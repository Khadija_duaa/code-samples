import React, { Component, Fragment, lazy, Suspense } from "react";
import { Redirect } from "react-router-dom";
import queryString from "query-string";
import { Helmet } from "react-helmet";
import { withSnackbar } from "notistack";
import Grid from "@material-ui/core/Grid";
import { compose } from "redux";
import { withRouter, Link, Route } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import InputAdornment from "@material-ui/core/InputAdornment";
import DefaultTextField from "../../components/Form/TextField";
import ReactHtmlParser from 'react-html-parser';
import Paper from "@material-ui/core/Paper";
import loginStyles from "./styles";
import CustomButton from "../../components/Buttons/CustomButton";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import IconButton from "@material-ui/core/IconButton";
// import SignUpForm from "./SignupForm";
import InputLabel from "@material-ui/core/InputLabel";
import EditIcon from "@material-ui/icons/Edit";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
// import ForgotPassword from "./ForgotPassword";
// import EnterNewPassword from "./EnterNewPassword";
import helper from "../../helper";
import Hotkeys from "react-hot-keys";
import Icons from "../../components/Icons/Icons";
import InvitationEmail from "./InvitationEmail";
import DuplicateExternalAccount from "./DuplicateExternalAccount";
// import TwoFactorAuth from "./TwoFactorAuth";
import {
  authenticateEmailRequest,
  authenticatePassword,
  getOnBoardingData,
} from "../../redux/actions/authentication";
import SsoForm from "./Sso";
import { AcceptTeamInvitation } from "../../redux/actions/workspace";

import { forgotPasswordRequest, resetPassword } from "../../redux/actions/forgotPassword";
import { FetchUserInfo } from "../../redux/actions/profile";
import { validatePasswordField, validateUserNameAndEmailField } from "../../utils/formValidations";
import getErrorMessages from "../../utils/constants/errorMessages";
import SocialLoginButtons from "../../components/SocialLoginButtons/SocialLoginButtons";

import { PopulateTempData, DeleteTempData } from "../../redux/actions/tempdata";
import { ValidateToken } from "../../redux/actions/onboarding";
import CustomTooltip from "../../components/Tooltip/Tooltip";
import axios from "axios";
import constants from "../../redux/constants/types";
import nTaskIcon from "../../assets/images/nTask32x32.png";
import LogoV2 from "../../assets/images/nTask-Logo-Black.svg";
import TeamsPermission from "../TeamsPermission/TeamsPermission";
import loadable from '@loadable/component'
import { PopulateWhiteLabelInfo } from "../../redux/actions/teamMembers";
import { store } from "../..";
import mixpanel from "mixpanel-browser";
import { MixPanelEvents } from "../../mixpanel";

// import faviconIcon from "../../assets/images/favicon.ico";
const ForgotPassword = loadable(() =>
  import(/*webpackChunkName: "forgotpassword" */ "./ForgotPassword")
);
const EnterNewPassword = loadable(() =>
  import(/* webpackChunkName: "newpassword" */ "./EnterNewPassword")
);
const TwoFactorAuth = loadable(() => import(/* webpackChunkName: "twofactor" */ "./TwoFactorAuth"));
const SignUpForm = loadable(() => import(/* webpackChunkName: "signup" */ "./SignupForm"));

class LoginView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userNameErrorMessage: "",
      userNameError: false,
      passwordErrorMessage: "",
      userPassword: "",
      passwordError: false,
      enterPassword: false,
      signUp: false,
      loginView: false,
      showPassword: false,
      emailDisabled: false,
      userEmail: "",
      userEmailCapsLock: false,
      rememberMe: false,
      forgotPassword: false,
      enterNewPassword: false,
      code: "",
      inviteEmail: false,
      duplicateExternalAccount: false,
      twoFactorAuth: false,
      twoFAFlag: false,
      tokenAPIResponse: null,
      forgotPasswordCode: 0,
      btnQuery: "",
      newPasswordBtnQuery: "",
      forgotPasswordBtnQuery: "",
      signInBtnQuery: "",
      forgetPasswordErrorMessage: "",
      companyData: null,
      showLogo: true,
      showForgotImage: false,
      forgotPasswordImage: null,
      forgotComapnyName: null,
      microsofTeams: { access_token: "", redirect_uri: "", showPermissions: false, userName: "" },
    };
    this.handleSignUp = this.handleSignUp.bind(this);
    this.handleEmailInput = this.handleEmailInput.bind(this);
    this.handleEditUserEmail = this.handleEditUserEmail.bind(this);
    this.handleRememberMe = this.handleRememberMe.bind(this);
    this.handleForgotPasswordClick = this.handleForgotPasswordClick.bind(this);
    this.handleLoginClick = this.handleLoginClick.bind(this);
    this.handleEmail_ForgotPassword = this.handleEmail_ForgotPassword.bind(this);
    this.showSnackBar = this.showSnackBar.bind(this);
    this.handleSignInClick = this.handleSignInClick.bind(this);
    this.handlePasswordInput = this.handlePasswordInput.bind(this);
    this.resetPassword_GetInitialInfo = this.resetPassword_GetInitialInfo.bind(this);
    this.resetPassword_NewPass = this.resetPassword_NewPass.bind(this);
    this.RedirectToInvitationEmail = this.RedirectToInvitationEmail.bind(this);
    this.setPasswordClick = this.setPasswordClick.bind(this);
    this.navigateToLogin = this.navigateToLogin.bind(this);
    this.navigateToDuplicateExtAcc = this.navigateToDuplicateExtAcc.bind(this);
    this.autheticateEmail_Login = this.autheticateEmail_Login.bind(this);
  }

  componentDidMount() {
    mixpanel.time_event(MixPanelEvents.LoginEvent);
    let companyName =
      localStorage.getItem("companyName") != null
        ? localStorage.getItem("companyName")
        : this.props.match.params.companyName;
    if ((companyName || window.location.host == 'cf.naxxa.io' || window.location.host == 'roboleadmanager.co.uk' || window.location.host == 'app.roboleadmanager.co.uk' || window.location.host == 'localhost:3001') && !localStorage.hasOwnProperty("token")) {
      //localStorage.removeItem("companyName");
      this.getCompanyData(companyName);
    } else if (companyName && localStorage.hasOwnProperty("token")) {
      this.props.history.push("/tasks");
    } else {
      const params = queryString.parse(location.search);
      // this.setState({microsofTeams:{isMicrosoftTeams: true, redirectUrl: params.redirect_uri+"#/authend"}});
      this.setFaviconIcon(nTaskIcon);
      this.ValidateToken(params);
    }
  }
  getCompanyData = (companyName, isAfterSuccess = false) => {
    this.setState({ showLogo: false });
    axios
      .get(`${constants.WHITELABELURL}?brandUrl=${companyName}`)
      .then(response => {
        if (response.data.success) {
          this.setState({ companyData: response.data.data });
          this.setFaviconIcon(response.data.data.faviconImageUrl);
          // here
          store.dispatch(PopulateWhiteLabelInfo(response.data.data));
        } else {
          this.setState({ companyData: {} });
          this.setFaviconIcon(nTaskIcon);
        }

        this.setState({ showLogo: true });
        localStorage.removeItem("companyName");
      })
      .catch(err => {
        this.setState({ companyData: {} });
        this.setState({ showLogo: true });
        this.setFaviconIcon(nTaskIcon);
        localStorage.removeItem("companyName");
      });
  };
  setFaviconIcon = icon => {
    var favicon = document.querySelector('link[rel="shortcut icon"]');
    if (favicon) favicon.remove();
    favicon = document.createElement("link");
    favicon.setAttribute("rel", "shortcut icon");
    var head = document.querySelector("head");
    head.appendChild(favicon);
    favicon.setAttribute("href", `${icon}?v=${new Date().getTime()}`);
  };
  resetPassword_GetInitialInfo() {
    this.setState({
      userEmail: helper.HELPER_GETPARAMS(window.location.href)
        ? helper.HELPER_GETPARAMS(window.location.href).email
        : "",
    });
    this.setState({
      code: helper.HELPER_GETPARAMS(window.location.href)
        ? helper.HELPER_GETPARAMS(window.location.href).code
        : "",
    });
  }

  resetPassword_NewPass(password, confirmPassword, code, eMail) {
    let self = this;
    let data = {
      eMail,
      code,
      password,
      confirmPassword,
    };
    this.setState({ newPasswordBtnQuery: "progress" });
    resetPassword(
      data,
      response => {
        this.setState({ newPasswordBtnQuery: "" });
        if (response.status === 200) {
          self.props.history.push("/account/login");
        }
      },
      error => {
        this.setState({ newPasswordBtnQuery: "" });
        if (error.status === 500) {
          self.showSnackBar("Server throws error", "error");
        } else {
          self.showSnackBar(error.data.message, "error");
        }
      }
    );
  }

  redirectWithStatus(self, data, _status) {
    mixpanel.track(MixPanelEvents.LoginEvent,data);
    let companyObj = {
      passwordError: "",
      usernameError: "",
      lastNameError: "",
      firstNameError: "",
      otherCompanyIndustry: "",
      passwordErrorMessage: "",
      usernameErrorMessage: "",
      lastNameErrorMessage: "",
      firstNameErrorMessage: "",
      showPassword: false,
      companyName: "",
      jobTitle: "",
      companyIndustry: "",
      companySize: "",
      otherIndustry: false,
      errorString_companyName: null,
      otherIndustry: false,

      otherCompanyIndustryError: false,
      otherCompanyIndustryErrorMessage: "",

      otherJobTitle: "",
      otherJobTitleErrorMessage: "",
      otherJobTitleError: false,
      OtherJob: false,
    };
    let workspaceObj = {
      workspaceName: "",
      workspaceUrl: "",
      workspaceUseOther: "",
      workspaceError: false,
      workspaceUrlError: false,
      workspaceErrorMessage: null,
      workspaceUrlErrorMessage: null,
      workspaceUse: "",
      otherWorkspaceOption: false,
    };
    let inviteMemberObj = {
      emailAddressErrorMessage: "",
      emailAddressError: false,
      inviteEmailAddress: "",
      inviteMemberList: [],
      teamId: "",
    };

    companyObj.companyName = data.companyName || "";
    companyObj.jobTitle = { value: data.jobTitle, label: data.jobTitle } || "";
    companyObj.otherIndustry = false;
    companyObj.companyIndustry = { value: data.industry, label: data.industry } || "";
    companyObj.otherCompanyIndustry = "";
    companyObj.companySize = data.teamSize || "";

    if (
      companyObj.companyIndustry &&
      companyObj.companyIndustry.value &&
      companyObj.companyIndustry.value.toLowerCase() === "other"
    ) {
      companyObj.otherIndustry = true;
      companyObj.otherCompanyIndustry = data.industryDetails;
    }

    if (
      companyObj.jobTitle &&
      companyObj.jobTitle.value &&
      companyObj.jobTitle.value.toLowerCase() === "other"
    ) {
      companyObj.OtherJob = true;
      companyObj.otherJobTitle = data.jobTitleDetails;
    }

    workspaceObj.workspaceName = data.teamName || "";
    workspaceObj.workspaceUrl = data.teamName || "";
    workspaceObj.workspaceUse = { value: data.usage, label: data.usage } || "";
    if (
      companyObj.workspaceUse &&
      companyObj.workspaceUse.value &&
      companyObj.workspaceUse.value.toLowerCase() === "other"
    ) {
      workspaceObj.workspaceUseOther = data.usage;
      workspaceObj.otherWorkspaceOption = true;
    }
    localStorage.setItem("CompanyInfo", JSON.stringify(companyObj));
    localStorage.setItem("CreateWorkspace", JSON.stringify(workspaceObj));
    localStorage.setItem("InviteMembers", JSON.stringify(inviteMemberObj));
    switch (_status.state) {
      case "4": //Company Information
        localStorage.setItem(
          "Onboarding",
          JSON.stringify({
            activeStep: 1,
            updatesCheckbox: false,
          })
        );
        self.props.history.push("/welcome/company");
        break;
      case "3": //WorkSpace Information
        localStorage.setItem(
          "Onboarding",
          JSON.stringify({
            activeStep: 2,
            updatesCheckbox: false,
          })
        );
        self.props.history.push("/welcome/create-workspace");
        break;
      case "2": //Invite Members
        localStorage.setItem(
          "Onboarding",
          JSON.stringify({
            activeStep: 3,
            updatesCheckbox: false,
          })
        );
        self.props.history.push("/welcome/invite-members");
        break;
      case "1": //Youtube
        self.props.history.replace("/welcome/getstarted");
      default:
        self.props.authAction(true);
        self.props.history.push("/tasks");
        break;
    }
  }

  OnBoardingData_Login = (self, _status) => {
    if (_status && _status.access_token) {
      localStorage.setItem("token", `Bearer ${_status.access_token}`);
      sessionStorage.setItem("token", `Bearer ${_status.access_token}`);
      if (_status.state === "1") {
        self.props.history.replace("/welcome/getstarted");
      } else {
        self.props.getOnBoardingData(data => {
          if (data.status === 200) {
            self.redirectWithStatus(self, data.data, _status);
          }
        });
      }
    }
  };

  UpdatetokenAPIResponse = obj => {
    this.setState({
      tokenAPIResponse: obj,
    });
  };

  UpdateTwoFAFlag = flag => {
    if (flag === false)
      this.setState({
        twoFactorAuth: false,
        twoFAFlag: false,
        loginView: true,
        forgotPassword: false,
        signUp: false,
        enterNewPassword: false,
        inviteEmail: false,
      });
  };

  // ZapierRedirect(token, email, zapkey) {
  //   // var client_id = helper.HELPER_GETPARAMS(window.location.href).client_id;
  //   // var response_type = helper.HELPER_GETPARAMS(window.location.href).response_type;
  //   var state = helper.HELPER_GETPARAMS(window.location.href).state;
  //   var redirect_uri = helper.HELPER_GETPARAMS(window.location.href).redirect_uri;
  //   localStorage.setItem("token", `Bearer ${token}`);
  //   localStorage.setItem("zapkey", zapkey);
  //   let rd = redirect_uri +
  //   "?code=" +
  //   encodeURIComponent(email + "," + zapkey) +
  //   "&state=" +
  //   state;
  //
  //   window.location.href = rd;
  //

  addUserToWorkSpace = (teamID, token) => {
    this.props.AcceptTeamInvitation(
      teamID,
      data => {
        sessionStorage.removeItem("teamID");
        this.props.history.replace("/tasks");
      },
      () => {}
    );
  };

  setToken = token => {
    const { rememberMe } = this.state;
    if (rememberMe) { /** if user have selected the option for remember me then set the token in the local stoarge  */
      localStorage.setItem("token", `Bearer ${token}`);
    } else {
      sessionStorage.setItem("token", `Bearer ${token}`);
    }
  };

  autheticatePassword_Login(param, event) {
    let postData = {
      grant_type: "password",
      username: this.state.userEmail.trim(),
      password: this.state.userPassword,
    };
    this.setState({ signInBtnQuery: "progress" });
    this.props.authenticatePassword(
      postData,
      data => {
        this.setState({ signInBtnQuery: "", enterPassword: false });
        if (data && data.access_token) {
          mixpanel.track(MixPanelEvents.LoginEvent, data);
          const parsedUrl = queryString.parse(this.props.location.search);
          const { microsoftteams, redirect_uri } = parsedUrl;
          if (microsoftteams) {
            /// Code for microsoft teams login
            sessionStorage["isMicrosftTeamsAuth"] = true;
            this.setState({
              microsofTeams: {
                showPermissions: true,
                redirect_uri: redirect_uri,
                access_token: data.access_token,
                userName: this.state.userEmail,
              },
            });
            return;
          }
          // if(this.state.microsofTeams.isMicrosoftTeams) {
          //   sessionStorage["isMicrosftTeamsAuth"] = true;
          //   this.setState({microsofTeams:{showPermissions:true}});
          //   // window.location = this.state.microsofTeams.redirectUrl+"?access_token="+data.access_token;
          //   return;
          // }
          data.grant_type = "password";
          data.email = this.state.userEmail;
          data.password = this.state.userPassword;
          localStorage.setItem("userName", this.state.userEmail);
          localStorage.setItem("zapkey", data.zapKey);
          let teamID = sessionStorage.getItem("teamID") || null;
          if (teamID) {
            this.setToken(data.access_token);
            this.addUserToWorkSpace(teamID, `Bearer ${data.access_token}`);
          } else {
            let zapAuth = helper.ISZAPIERCALL(this.state.userEmail, data.zapKey);
            if (zapAuth === 0) {
              this.setState({
                isDerived: true,
                tokenAPIResponse: data,
              });
              if (data.state > 0) {
                this.OnBoardingData_Login(this, data);
              } else if (data.requireOTP === "true" || data.requireOTP === "True") {
                if (this.state.companyData) {
                  this.setState({
                    companyData: null,
                    forgotPasswordImage: this.state.companyData.signinLogoUrl,
                    forgotComapnyName: this.state.companyData.companyName,
                  }); //empty company data and set oamge to show on whitelabel wfa screen
                }
                this.props.PopulateTempData(data);
                this.props.history.push("/account/userauthentication");
              } else {
                this.setToken(data.access_token);
                if (queryString.parseUrl(this.props.location.search).query.redirectUrl) {
                  if (queryString.parseUrl(this.props.location.search).query.redirect_uri) {
                    // In case of zapier
                    this.props.history.push(
                      `/zapierAuth?${queryString.stringify(
                        queryString.parseUrl(this.props.location.search).query
                      )}`
                    );
                  } else {
                    this.props.history.push(
                      `/${queryString.parseUrl(this.props.location.search).query.redirectUrl}`
                    );
                  }
                } else {
                  if (this.props.location.state && this.props.location.state.support) {
                    this.props.history.push({
                      pathname: "/tasks",
                      state: { support: true },
                    });
                  } else {
                    this.props.history.push("/teams/");
                  }
                }
              }
            } else if (zapAuth === 1) {
              this.props.history.push("/account/login");
            }
            // else if (zapAuth === 2) {
            //   helper.ZAPIERREDIRECT(
            //     data.access_token,
            //     this.state.userEmail,
            //     data.zapKey
            //   );
            // }
          }
        }
      },
      error => {
        this.setState({ signInBtnQuery: "" });
        this.setState({
          passwordError: true,
          passwordErrorMessage:
            error && error.response && error.response.data
              ? error.response.data.error_description
              : null,
        });
      }
    );
  }
  autheticateEmail_Login(event) {
    let inputValue = this.state.userEmail.trim();
    let validationObj = validateUserNameAndEmailField("email/username", inputValue);
    if (validationObj["isError"]) {
      this.setState({
        userNameError: validationObj["isError"],
        userNameErrorMessage: validationObj["message"],
      });
    } else {
      this.setState({ btnQuery: "progress" });
      this.props.authenticateEmailRequest(
        { email: inputValue },
        data => {
          this.setState({ btnQuery: "" });
          if (data.status === 200) {
            this.setState({
              userNameErrorMessage: "",
              userNameError: false,
              enterPassword: true,
            });
          }
        },
        error => {
          this.setState({ btnQuery: "" });
          if (error) {
            if (error.status === 406) {
              this.setState({
                userNameErrorMessage: getErrorMessages(inputValue, this.setPasswordClick)
                  .VERIFY_EMAIL,
                userNameError: true,
              });
            } else if (error.status === 407) {
              this.setState({
                userNameErrorMessage: error.data.message,
                userNameError: true,
              });
            } else if (error.status === 302) {
              this.setState({
                userNameErrorMessage: getErrorMessages(() => this.setPasswordClick(inputValue))
                  .SIGNIN_ACCOUNT_EXISTS,
                userNameError: true,
              });
            } else if (error.status === 403) {
              this.setState({
                userNameErrorMessage: error.data.message,
                userNameError: true,
              });
            } else if (error.status === 500) {
              this.showSnackBar("Server throws error", "error");
            } else {
              if (this.state.companyData) {
                this.setState({
                  userNameErrorMessage: getErrorMessages(inputValue).WL_SIGN_UP,
                  userNameError: true,
                });
              } else {
                this.setState({
                  userNameErrorMessage: getErrorMessages(inputValue).LETS_SIGN_UP,
                  userNameError: true,
                });
              }
            }
          }
        }
      );
    }
  }
  handleEmail_ForgotPassword(email_fp) {
    let self = this;
    this.setState({ forgotPasswordBtnQuery: "progress" });
    self.props.forgotPasswordRequest(
      { eMail: email_fp },
      data => {
        this.setState({ forgotPasswordBtnQuery: "" });
        if (data.payload.data && data.payload.data.statusCode === 200) {
          self.showSnackBar(`${data.payload.data.data}`, "info");
          self.setState({
            forgotPasswordCode: 0,
          });
          self.props.history.push("/account/login");
        }
      },
      error => {
        let errMessage = error.payload.data.data;
        self.setState({
          forgotPasswordCode: error.payload.data.statusCode,
          forgotPasswordBtnQuery: "",
          forgetPasswordErrorMessage: errMessage.message ? errMessage.message : errMessage,
        });
      }
    );
  }

  setPasswordClick(eMail) {
    let self = this;
    const email = typeof eMail === "string" ? eMail : self.state.userEmail.trim();
    self.props.forgotPasswordRequest(
      { eMail: email },
      data => {
        if (data.payload.data && data.payload.data.statusCode === 200) {
          self.showSnackBar(`${data.payload.data.data}`, "success");
          self.RedirectToInvitationEmail();
        }
      },
      err => {}
    );
  }

  RedirectToInvitationEmail() {}

  handleSignUp() {
    if (this.state.signUp) {
      this.setState({ signUp: false, loginView: true, forgotPassword: false, companyData: null });
      this.props.history.push("/account/login");
    } else {
      this.setState({
        signUp: true,
        loginView: false,
        forgotPassword: false,
        userNameErrorMessage: "",
        userNameError: false,
        companyData: null,
      });
      this.props.history.push("/account/register");
    }
  }

  handleClickShowPassword = () => {
    this.setState(state => ({ showPassword: !state.showPassword }));
  };
  handleEmailInput(event) {
    this.setState({
      userEmail: event.currentTarget.value,
      userNameErrorMessage: "",
      userNameError: false,
    });
  }
  handleEmailKey = e => {
    var caps_lock_on = e.getModifierState("CapsLock");

    if (caps_lock_on == true) this.setState({ userEmailCapsLock: true });
    else this.setState({ userEmailCapsLock: false });
  };
  handleForgotPasswordClick() {
    if (this.state.companyData) {
      // this.getCompanyData(this.state.companyData.companyName);
      this.setState({
        forgotPasswordImage: this.state.companyData.signinLogoUrl,
        forgotComapnyName: this.state.companyData.companyName,
      });
    }
    this.setState({
      forgotPassword: true,
      enterPassword: false,
      loginView: false,
      companyData: null,
    });
    this.props.history.push("/account/forgotPassword");
  }
  handleEditUserEmail() {
    this.setState({ enterPassword: false });
  }
  handleRememberMe(event) {
    this.setState({ rememberMe: event.target.checked });
  }
  handleLoginClick() {
    this.setState({ forgotPassword: false, loginView: true });
    this.props.history.push("/account/login");
  }

  handleSignInClick(param, event) {
    let passwordValue = this.state.userPassword;
    let validationObj = validatePasswordField(passwordValue);
    if (validationObj["isError"]) {
      this.setState({
        passwordError: validationObj["isError"],
        passwordErrorMessage: validationObj["message"],
      });
    } else {
      this.autheticatePassword_Login(param, event);
    }
  }
  handlePasswordInput(event) {
    this.setState({
      userPassword: event.password || event.target.value,
      passwordError: false,
      passwordErrorMessage: "",
    });
  }
  handleAuthAction() {
    this.props.authAction(true);
    this.props.history.push("/tasks");
  }
  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  navigateToLogin = () => {
    this.setState({
      userNameErrorMessage: "",
      userNameError: false,
      enterPassword: false,
      userPassword: "",
    });
    this.props.history.push("/account/login");
  };
  navigateToDuplicateExtAcc = () => {
    this.props.history.push("/account/duplicateaccount");
  };

  ValidateToken = params => {
    if (params.code) {
      this.props.ValidateToken(
        {
          email: params.email,
          code: params.code,
        },
        data => {
          if (data.data) {
            this.props.history.push("/account/login");
          }
        }
      );
    }
  };
  navigateToSso = () => {
    this.props.history.push("/account/sso-signin");
  };
  render() {
    const {
      userNameErrorMessage,
      userPassword,
      userNameError,
      passwordErrorMessage,
      passwordError,
      enterPassword,
      showPassword,
      userEmail,
      userEmailCapsLock,
      rememberMe,
      enterNewPassword,
      btnQuery,
      newPasswordBtnQuery,
      forgotPasswordBtnQuery,
      signInBtnQuery,
      tokenAPIResponse,
      twoFAFlag,
      companyData,
      showLogo,
      forgotPasswordImage,
      forgotComapnyName,
    } = this.state;
    const { classes, theme, authAction } = this.props;
    const { view } = this.props.match.params;
    const companyTitle = companyData
      ? companyData.brandName
        ? companyData.brandName
        : forgotComapnyName
        ? forgotComapnyName
        : "nTask"
      : forgotComapnyName
      ? forgotComapnyName
      : "nTask";
    const showSocialLoginButtons =
      view == "register" ||
      view == "login" ||
      companyData ||
      (view !== "userauthentication" &&
        view !== "forgotPassword" &&
        view !== "verifyemail" &&
        view !== "resetpassword");
    const userNameErrorCmp =
      typeof userNameErrorMessage == "object"
        ? userNameErrorMessage
        : ReactHtmlParser(userNameErrorMessage);
    const showPermissions = this.state.microsofTeams.showPermissions;
    return (
      <>
        {showPermissions ? (
          <TeamsPermission microsoftTeam={this.state.microsofTeams} />
        ) : (
          <div className={classes.varticalAlignParent}>
            <div className={classes.varticalAlignChild}>
              <div className={classes.loginMainCnt}>
                <Helmet>
                  <title>
                    {`${companyTitle} - Free Online Task & Project Management Software for Teams`}
                  </title>
                </Helmet>
                <div
                  style={{
                    width: companyData && companyData.signinLogoUrl ? (companyData.signinLogoWidth || 200) : 160,
                    height: companyData && companyData.signinLogoUrl ? (companyData.signinLogoHeight || 60) : 45,
                    marginLeft: "auto",
                    marginRight: "auto",
                  }}
                  className={classes.nTaskLogo}>
                  {showLogo ? (
                    <img
                      className={classes.ntaskLogoImg}
                      src={
                        companyData
                          ? companyData.signinLogoUrl
                            ? companyData.signinLogoUrl
                            : forgotPasswordImage
                            ? forgotPasswordImage
                            : LogoV2
                          : forgotPasswordImage
                          ? forgotPasswordImage
                          : LogoV2
                      }
                    />
                  ) : null}
                </div>

                <Paper classes={{ root: classes.loginFormCnt }}>
                  <Typography
                    variant="h2"
                    align="center"
                    style={{ marginBottom: 10, lineHeight: "normal" }}>
                    {view == "register"
                      ? "Fill in the information and click Sign Up"
                      : view === "login" || (companyData && !location.pathname.includes('resetpassword'))
                      ? `Sign in to your ${companyTitle} account`
                      : view === "forgotPassword"
                      ? "Forgot password?"
                      : view === "resetpassword" || location.pathname.includes('resetpassword')
                      ? "Enter your new password"
                      : view === "setpassword" || location.pathname.includes('setpassword')
                      ? "Enter your new password"
                      : view === "sso-signin"
                      ? "Single sign on"
                      : null}
                  </Typography>
                  {!companyData && (view === "login" || view === "resetpassword") ? (
                    <Typography variant="body1" align="center">
                      Organize, streamline and make your tasks smarter!
                    </Typography>
                  ) : view === "forgotPassword" ? (
                    <Typography variant="body1" align="center">
                      Enter your email or username to reset your password
                    </Typography>
                  ) : null}

                  {(view === "login" || (companyData && !location.pathname.includes('sso-signin'))) ? (
                    <form
                      className={classes.LoginForm}
                      onSubmit={e => {
                        e.preventDefault();
                      }}>
                      {enterPassword ? (
                        <Fragment>
                          <div style={{ textAlign: "left" }}>
                            <InputLabel classes={{ root: classes.disableEmailLabel }}>
                              Email / Username
                            </InputLabel>
                            <p className={classes.disableEmailValue}>
                              {userEmail}
                              <IconButton
                                disableRipple={true}
                                classes={{ root: classes.editEmailButton }}
                                onClick={this.handleEditUserEmail}>
                                <EditIcon fontSize="small" />
                              </IconButton>
                            </p>
                          </div>
                          <Hotkeys keyName="enter" onKeyDown={this.handleSignInClick}>
                            {/* <PasswordTextField
                          label="Password"
                          defaultProps={{
                            id: "passwordInput",
                            type: this.state.showPassword ? "text" : "password",
                            onChange: this.handlePasswordInput
                          }}
                        /> */}
                            <DefaultTextField
                              label="Password"
                              fullWidth={true}
                              errorState={passwordError}
                              errorMessage={passwordErrorMessage}
                              defaultProps={{
                                id: "passwordInput",
                                type: this.state.showPassword ? "text" : "password",
                                onChange: this.handlePasswordInput,
                                onKeyDown: e => {
                                  if (e.keyCode === 13) {
                                    /** Enter Click */
                                    this.handleSignInClick();
                                  }
                                },
                                value: userPassword,
                                autoFocus: true,
                                placeholder: "Enter password",
                                inputProps: { maxLength: 40 },
                                endAdornment: (
                                  <InputAdornment position="end">
                                    <IconButton
                                      disableRipple={true}
                                      classes={{
                                        root: classes.passwordVisibilityBtn,
                                      }}
                                      onClick={this.handleClickShowPassword}>
                                      {showPassword ? <Visibility /> : <VisibilityOff />}
                                    </IconButton>
                                  </InputAdornment>
                                ),
                              }}
                            />
                            <CustomButton
                              onClick={this.handleSignInClick}
                              btnType="success"
                              variant="contained"
                              query={signInBtnQuery}
                              disabled={signInBtnQuery == "progress"}
                              id="signInBtn">
                              {`Sign In`}
                            </CustomButton>
                          </Hotkeys>
                          <div className={classes.rememberMeCnt}>
                            <FormControlLabel
                              classes={{ root: classes.rememberCheckboxCnt }}
                              control={
                                <Checkbox
                                  checked={rememberMe}
                                  onChange={this.handleRememberMe}
                                  classes={{
                                    root: classes.updatesCheckbox,
                                    checked: classes.checked,
                                  }}
                                />
                              }
                              label="Remember Me"
                            />
                            <span
                              className={classes.forgotPasswordLink}
                              onClick={this.handleForgotPasswordClick}>
                              Forgot Password?
                            </span>
                          </div>
                        </Fragment>
                      ) : (
                        <Fragment>
                          <Hotkeys
                            keyName="enter"
                            onKeyDown={this.autheticateEmail_Login.bind(this, "")}>
                            <DefaultTextField
                              label={
                                <>
                                  {"Email / Username"}
                                  {userEmailCapsLock ? (
                                    <CustomTooltip
                                      helptext={"Caps Lock is On"}
                                      iconType="warning"
                                      placement="right-end"
                                    />
                                  ) : null}
                                </>
                              }
                              fullWidth={true}
                              errorMessage={
                                userNameErrorMessage !== "error" ? (
                                  userNameErrorCmp
                                ) : (
                                  <Fragment>
                                    {" "}
                                    Email/Username '{this.state.userEmail}' does not exists.{" "}
                                    <Link to={`/account/register?email=${this.state.userEmail}`}>
                                      Click here
                                    </Link>{" "}
                                    to create an account
                                  </Fragment>
                                )
                              }
                              errorState={userNameError}
                              defaultProps={{
                                id: "userNameInput",
                                onChange: this.handleEmailInput,
                                onKeyUp: this.handleEmailKey,
                                onMouseDown: this.handleEmailKey,
                                onKeyDown: e => {
                                  if (e.keyCode === 13) {
                                    /** Enter Click */
                                    this.autheticateEmail_Login();
                                  }
                                },
                                value: userEmail,
                                autoFocus: true,
                                placeholder: "Enter email or username",
                                inputProps: { maxLength: 250 },
                              }}
                            />
                          </Hotkeys>
                          <CustomButton
                            onClick={this.autheticateEmail_Login}
                            btnType="success"
                            variant="contained"
                            query={btnQuery}
                            disabled={btnQuery == "progress"}
                            id="signInNextBtn">
                            Next
                          </CustomButton>
                        </Fragment>
                      )}
                    </form>
                  ) : null}
                  <Suspense fallback={<></>}>
                    <Route
                      path="/account/register"
                      render={() => {
                        return (
                          <SignUpForm
                            classes={classes}
                            initialData={{ email: userEmail }}
                            RedirectToInvitationEmail={this.RedirectToInvitationEmail}
                            newPasswordAction={this.handleEmail_ForgotPassword}
                            setPasswordClick={this.setPasswordClick}
                            showSnackBar={this.showSnackBar}
                          />
                        );
                      }}
                    />
                  </Suspense>
                  <Suspense fallback={<></>}>
                    <Route
                      path="/account/sso-signin"
                      render={() => {
                        return (
                          <SsoForm
                            classes={classes}
                            initialData={{ email: userEmail }}
                            RedirectToInvitationEmail={this.RedirectToInvitationEmail}
                            newPasswordAction={this.handleEmail_ForgotPassword}
                            setPasswordClick={this.setPasswordClick}
                            showSnackBar={this.showSnackBar}
                          />
                        );
                      }}
                    />
                  </Suspense>
                  {/* {this.props.match.params.match == "forgotpassword" ? ( */}
                  <Suspense fallback={<></>}>
                    <Route
                      path="/account/forgotpassword"
                      render={() => {
                        return (
                          <ForgotPassword
                            classes={classes}
                            loginAction={this.handleLoginClick}
                            email={userEmail}
                          />
                        );
                      }}
                    />
                  </Suspense>
                  {/* // ) : null} */}

                  <Route
                    path="/account/duplicateaccount"
                    render={() => {
                      return <DuplicateExternalAccount theme={theme} classes={classes} />;
                    }}
                  />
                  <Suspense fallback={<></>}>
                    <Route
                      exact
                      path="/wl/:companyName/resetpassword"
                      render={() => {
                        return (
                          <EnterNewPassword
                            email={this.state.userEmail}
                            btnQuery={newPasswordBtnQuery}
                            resetPassword={this.resetPassword_NewPass}
                            classes={classes}
                          />
                        );
                      }}
                    />
                  </Suspense>
                  <Suspense fallback={<></>}>
                    <Route
                      path="/account/resetpassword"
                      render={() => {
                        return (
                          <EnterNewPassword
                            email={this.state.userEmail}
                            btnQuery={newPasswordBtnQuery}
                            resetPassword={this.resetPassword_NewPass}
                            classes={classes}
                          />
                        );
                      }}
                    />
                  </Suspense>
                  <Suspense fallback={<></>}>
                    <Route
                      path="/account/setpassword"
                      render={() => {
                        return (
                          <EnterNewPassword
                            email={this.state.userEmail}
                            btnQuery={newPasswordBtnQuery}
                            resetPassword={this.resetPassword_NewPass}
                            classes={classes}
                          />
                        );
                      }}
                    />
                  </Suspense>
                  <Suspense fallback={<></>}>
                    <Route
                      path="/account/userauthentication"
                      render={() => {
                        return (
                          <TwoFactorAuth
                            classes={classes}
                            theme={theme}
                            showSnackBar={this.showSnackBar}
                            navigateToLogin={this.navigateToLogin}
                            UpdateTwoFAFlag={this.UpdateTwoFAFlag}
                          />
                        );
                      }}
                    />
                  </Suspense>
                  <Suspense fallback={<></>}>
                    <Route
                      path="/account/verifyemail"
                      render={() => {
                        return <InvitationEmail theme={theme} classes={classes} />;
                      }}
                    />
                  </Suspense>
                  {showSocialLoginButtons && view !== "sso-signin" && !location.pathname.includes('resetpassword') ? (
                    <Fragment>
                      {view == "register" ? (
                        <div className={classes.signInOptionText}>
                          <p>or sign up with</p>
                        </div>
                      ) : (
                        <>
                          <div className={classes.signInOptionText}>
                            <p>or continue with</p>
                          </div>
                        </>
                      )}
                      {view !== "sso-signin" && !location.pathname.includes('resetpassword') && (
                        <SocialLoginButtons
                          duplicateAccount={this.navigateToDuplicateExtAcc}
                          showSnackBar={this.showSnackBar}
                        />
                      )}
                    </Fragment>
                  ) : null}
                  {view === "sso-signin" && !location.pathname.includes('resetPassword') && (
                    <CustomButton
                      onClick={this.navigateToLogin}
                      style={{
                        marginTop: 20,
                        margin: "10px auto 0 auto",
                        padding: "15px 0 10px 0",
                        // borderTop: `1px solid ${theme.palette.border.lightBorder}`,
                        borderRadius: 0,
                      }}
                      btnType="plain"
                      variant="text">
                      Sign in without SSO
                    </CustomButton>
                  )}
                  {view !== "sso-signin" && view !== "verifyemail" && (
                    <CustomButton
                      onClick={this.navigateToSso}
                      style={{
                        marginTop: 20,
                        margin: "10px auto 0 auto",
                        padding: "15px 0 10px 0",
                        // borderTop: `1px solid ${theme.palette.border.lightBorder}`,
                        borderRadius: 0,
                      }}
                      btnType="plain"
                      variant="text">
                      Sign in with SSO
                    </CustomButton>
                  )}
                  {view == "register" ? (
                    <p className={classes.agreementText}>
                      By signing up, you agree to our{" "}
                      <a
                        className={classes.termsLink}
                        target="_blank"
                        href="https://www.ntaskmanager.com/terms-conditions/">
                        Terms
                      </a>{" "}
                      and{" "}
                      <a
                        className={classes.privacyPolicyLink}
                        target="_blank"
                        href="https://www.ntaskmanager.com/privacy-policy/">
                        Privacy Policy
                      </a>
                    </p>
                  ) : null}
                  {view == "verifyemail" || view == "duplicateaccount" ? (
                    <CustomButton
                      style={{ marginTop: 40 }}
                      onClick={this.navigateToLogin}
                      btnType="success"
                      variant="contained">
                      {`Go Back Home`}
                    </CustomButton>
                  ) : null}
                </Paper>
                {showSocialLoginButtons || view === "forgotPassword" ? (
                  <p className={classes.signUpText}>
                    {view == "register"
                      ? `Already have an account ?`
                      : companyData || forgotPasswordImage
                      ? ``
                      : `Don't have an account ?`}
                    <span className={classes.signUpLink} onClick={this.handleSignUp}>
                      {view == "register"
                        ? " Sign In"
                        : companyData || forgotPasswordImage
                        ? ""
                        : " Sign Up"}
                    </span>
                  </p>
                ) : null}
              </div>
            </div>
          </div>
        )}
      </>
    );
  }
}

export default compose(
  withRouter,
  withSnackbar,
  withStyles(loginStyles, { withTheme: true }),
  connect(null, {
    ValidateToken,
    authenticateEmailRequest,
    authenticatePassword,
    getOnBoardingData,
    FetchUserInfo,
    forgotPasswordRequest,
    // resetPassword,
    PopulateTempData,
    DeleteTempData,
    AcceptTeamInvitation,
  })
)(LoginView);
