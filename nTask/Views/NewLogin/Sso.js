import React, { Component } from "react";
import { Helmet } from "react-helmet";
import DefaultTextField from "../../components/Form/TextField";
import CustomButton from "../../components/Buttons/CustomButton";
import { compose } from "redux";
import { withSnackbar } from "notistack";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { validateEmailAddress } from "../../redux/actions/teamMembers";
import { signinWithSso } from "../../redux/actions/authentication";
import { validEmail } from "../../utils/validator/common/email";
import { forgotPasswordRequest, resetPassword } from "../../redux/actions/forgotPassword";
import queryString from 'query-string'
import Hotkeys from "react-hot-keys";
let captcha;

class SsoForm extends Component {
  constructor(props) {
    super(props);
    const { email } = this.props.initialData;
    this.state = {
      email: "",
      emailError: false,
      emailErrorMessage: "",
      checkbox: true,
      btnQuery: "",
    };
  }
  componentDidMount() {
    const { location } = this.props;
    const params = queryString.parse(location.search);
    //set email error when error is in url query string
    if (params.error) {
      this.setState({ emailError: true, emailErrorMessage: params.error, email: params.email });
    }
  }

  handleChange = event => {
    const inputValue = event.currentTarget.value;
    this.setState({ email: inputValue, emailError: false, emailErrorMessage: "" });
  };

  validateEmailAddress = () => {
    /**Function for checking if the email is valid or not */
    let emailValue = this.state.signUpEmailInput;
    let validationObj = validEmail(emailValue);
    if (!validationObj.validated) {
      this.setState({
        emailError: true,
        emailMessage: validationObj.errorMessage,
      });
    }
    return validationObj.validated;
  };

  signinWithSso = () => {
    const { email } = this.state;
    window.location =
      ENV == "dev"
        ? `https://dauth.naxxa.io/api/v1/User/AuthenticateEmail?userName=${email}`
        : `https://auth.ntaskmanager.com/api/v1/User/AuthenticateEmail?userName=${email}`;
  };
  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };

  render() {
    const { emailError, emailErrorMessage, email, btnQuery } = this.state;
    const { classes } = this.props;

    return (
      <>
        <Helmet>
          <title>sso-signin | nTask</title>
        </Helmet>
        <Hotkeys keyName="enter" onKeyDown={this.signinWithSso}>
        <div className={classes.LoginForm} style={{ marginTop: 30 }}>
          <DefaultTextField
            label="Email"
            fullWidth={true}
            errorState={emailError}
            errorMessage={emailErrorMessage}
            defaultProps={{
              id: "signUpEmailInput",
              onChange: this.handleChange,
              value: email,
              placeholder: "Enter email address",
              inputProps: { maxLength: 250 },
            }}
          />

          <CustomButton
            onClick={this.signinWithSso}
            btnType="success"
            variant="contained"
            query={btnQuery}
            disabled={btnQuery == "progress"}>
            Sign In
          </CustomButton>
        </div>
        </Hotkeys>
      </>
    );
  }
}
export default compose(
  withRouter,
  withSnackbar,
  connect(null, {
    forgotPasswordRequest,
    validateEmailAddress,
  })
)(SsoForm);
