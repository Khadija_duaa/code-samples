import React, { Component } from 'react';
import SvgIcon from "@material-ui/core/SvgIcon";
import Typography from "@material-ui/core/Typography";
import InviteIcon from "../../components/Icons/InviteIcon";
import { withRouter } from "react-router-dom";

class DuplicateExternalAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const { theme, classes } = this.props;
    return (
      <div className={classes.invitationEmailInnerCnt}>
        <SvgIcon
          viewBox="0 0 458.6 434.9"
          className={classes.mailIcon}
          htmlColor={theme.palette.secondary.light}
        >
          <InviteIcon />
        </SvgIcon>
        <Typography variant="h1" classes={{ h1: classes.mainHeadingText }}>
          You have already signed in using another method.
          </Typography>

        <Typography variant="body2" align="center">
          Please sign in with other method to attach this account.
          </Typography>
      </div>
    );
  }
}

export default withRouter(DuplicateExternalAccount);