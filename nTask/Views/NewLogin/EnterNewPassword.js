import React, { Component } from "react";
import queryString from 'query-string';
import DefaultTextField from "../../components/Form/TextField";
import CustomButton from "../../components/Buttons/CustomButton";
import { validatePasswordField } from '../../utils/formValidations';
import getErrorMessages from '../../utils/constants/errorMessages';
import Tooltip from "@material-ui/core/Tooltip";
import LinearProgress from "@material-ui/core/LinearProgress";

const passwordStrenghts =
  [
    { item: 'with lowercase alphabets', rgx: new RegExp("[a-z]") },
    { item: 'with uppercase alphabets', rgx: new RegExp("[A-Z]") },
    { item: 'with numbers', rgx: new RegExp("[0-9]") },
    { item: 'with special characters', rgx: new RegExp("[$@$!%*#?&]") },
    { item: '8-40 characters', rgx: new RegExp("^[\\S|\\s]{8,40}$") }
  ]

class EnterNewPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      code: '',
      newPassError: false,
      confirmPassError: false,

      newPassErrorMessage: "",
      confirmPassErrorMessage: "",

      newPass: "",
      confirmPass: "",

      showTooltip: false,
      passwordStrength: { progress: 0 }
    };

    this.onSaveClick = this.onSaveClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  componentWillMount() {
    const parsed = queryString.parse(location.search);
    this.setState({ email: parsed.email, code: parsed.code });
  }
  validatePassword = (password) => {

    let passStrengthObj = { progress: 0 }

    // Check the conditions
    var ctr = 0;
    // Do not show anything when the length of password is zero.
    if (password.length > 0) {
      for (var i = 0; i < passwordStrenghts.length; i++) {
        if (passwordStrenghts[i]['rgx'].test(password)) {
          ctr++;
          passStrengthObj[passwordStrenghts[i]['item']] = true;
        }
      }

      passStrengthObj.progress = (100 / passwordStrenghts.length) * ctr;
    }

    return passStrengthObj;
  }
  handleChange(event) {
    let fieldName = event.currentTarget.id,
      inputValue = event.currentTarget.value
    switch (fieldName) {
      //////////////////////////////////// PASSWORD ///////////////////////////////////
      case 'newPass':
        const passwordValidationObj = this.validatePassword(inputValue);
        (inputValue === '') ? this.setState({ [fieldName]: inputValue, passwordStrength: passwordValidationObj }) :
          this.setState({ [fieldName]: inputValue, newPassError: false, newPassErrorMessage: '', passwordStrength: passwordValidationObj });
        break;
      //////////////////////////////////// CONFIRM PASSWORD ////////////////////////////////////
      case 'confirmPass':
        (inputValue === '') ? this.setState({ [fieldName]: inputValue }) :
          this.setState({ [fieldName]: inputValue, confirmPassError: false, confirmPassErrorMessage: '' });
        break;
    };
  }
  onSaveClick() {

    let noErrorExists = true;
    let newPasswordValue = this.state.newPass;
    let confirmNewPasswordValue = this.state.confirmPass;

    //////////////////////////////////// NEW PASSWORD ///////////////////////////////////
    let validationObj = validatePasswordField(newPasswordValue)
    if (validationObj['isError']) {
      noErrorExists = false;
      this.setState({
        newPassError: validationObj['isError'],
        newPassErrorMessage: validationObj['message']
      });
    }
    //////////////////////////////////// CONFIRM NEW PASSWORD ////////////////////////////////////
    if (newPasswordValue !== confirmNewPasswordValue) {
      noErrorExists = false;
      this.setState({
        confirmPassError: true,
        confirmPassErrorMessage: getErrorMessages().CONFIRM_PASSWORD_FIELD
      });
    }

    // SAVE NEW PASSWORD IF NO ERROR EXISTS
    if (noErrorExists) {
      const { newPass, confirmPass, email, code } = this.state;
      this.props.resetPassword(newPass, confirmPass, code, email);
    }
  }
  showTooltip = () => {
    this.setState({ showTooltip: true })
  }
  hideTooltip = () => {
    this.setState({ showTooltip: false })
  }
  render() {
    const {
      email,
      newPassError,
      confirmPassError,
      newPassErrorMessage,
      confirmPassErrorMessage,
      showTooltip,
      passwordStrength
    } = this.state;
    const { classes, btnQuery } = this.props;
    return (
      <div className={classes.LoginForm}>
        <DefaultTextField
          label="Email"
          fullWidth={true}
          defaultProps={{
            id: "newPasswordEmail",
            value: email,
            disabled: true
          }}
        />
        <Tooltip
          interactive
          open={showTooltip}
          disableFocusListener
          disableHoverListener
          disableTouchListener
          classes={{
            tooltip: classes.tooltip
          }}
          title={
            <div className="panel-body">
              <LinearProgress
                classes={{
                  root: classes.progressBar,
                  bar: `${passwordStrength.progress <= 40 ? classes.redBar : passwordStrength.progress <= 80 ? classes.orangeBar : classes.greenBar}`
                }}
                variant="determinate"
                value={passwordStrength.progress}
              />
              <div>A good password is :</div>
              <ul>
                {
                  passwordStrenghts.map((pStrength, i) => {
                    return <li key={i} className={`${passwordStrength[pStrength.item] ? classes.optionChecked : ''}`}><small>{pStrength.item}</small></li>
                  })
                }
              </ul>
            </div>
          }
          placement="right"
        >
          <DefaultTextField
            label="New Password"
            fullWidth={true}
            errorState={newPassError}
            errorMessage={newPassErrorMessage}
            defaultProps={{
              id: "newPass",
              onChange: this.handleChange,
              onFocus: this.showTooltip,
              onBlur: this.hideTooltip,
              value: this.state.newPass,
              type: "password",
              autoFocus: true,
              placeholder: "Enter new password",
              inputProps: { maxLength: 40 }
            }}
          />
        </Tooltip>
        <DefaultTextField
          label="Confirm Password"
          fullWidth={true}
          errorState={confirmPassError}
          errorMessage={confirmPassErrorMessage}
          defaultProps={{
            id: "confirmPass",
            onChange: this.handleChange,
            value: this.state.confirmPass,
            type: "password",
            placeholder: "Re-type new password",
            inputProps: { maxLength: 40 }
          }}
        />
        <CustomButton
          onClick={this.onSaveClick}
          btnType="success"
          variant="contained"
          query={btnQuery}
          disabled={btnQuery == "progress"}
        >
          Save
            </CustomButton>
      </div>
    );
  }
}

export default EnterNewPassword;
