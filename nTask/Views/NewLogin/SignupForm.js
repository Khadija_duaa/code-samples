import React, { Component } from "react";
import { Helmet } from "react-helmet";
import Grid from "@material-ui/core/Grid";
import DefaultTextField from "../../components/Form/TextField";
import CustomButton from "../../components/Buttons/CustomButton";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import { compose } from "redux";
import { withSnackbar } from "notistack";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { validateEmailAddress } from "../../redux/actions/teamMembers";

import { SignUpRequest } from "../../redux/actions/signUp";
import { SITE_KEY_CAPTCHA } from "../../utils/constants/externalConfig";
import ReCAPTCHA from "react-google-recaptcha";

import {
  validateEmailField,
  validateMultiWordNameField,
  filterNameInputValue,
} from "../../utils/formValidations";
import { validEmail } from "../../utils/validator/common/email";
import { validName } from "../../utils/validator/common/name";
import {
  forgotPasswordRequest,
  resetPassword,
} from "../../redux/actions/forgotPassword";
import queryString from "query-string";

let captcha;

class SignUpForm extends Component {
  constructor(props) {
    super(props);
    const { email } = this.props.initialData;
    this.state = {
      firstNameError: false,
      lastNameError: false,
      emailError: false,

      captchaToken: "",
      firstNameMessage: "",
      lastNameMessage: "",
      emailMessage: "",

      firstNameInput: "",
      lastNameInput: "",
      signUpEmailInput: email,

      checkbox: true,
      btnQuery: "",
    };
    this.handleCheckbox = this.handleCheckbox.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.SignUpBtnClick = this.SignUpBtnClick.bind(this);
    this.captchaOnChange = this.captchaOnChange.bind(this);
  }
  componentDidMount() {
    const { location } = this.props;
    let parseUrl = queryString.parseUrl(location.search);
    if (parseUrl.query && parseUrl.query.invitedBy) {
      this.setState({
        invitedBy: parseUrl.query.invitedBy,
        invitedTeamId: parseUrl.query.invitedTeamId,

      });
    } if (parseUrl.query && parseUrl.query.email) {
      this.setState({
        signUpEmailInput: parseUrl.query.email

      });
    }
  }

  captchaOnChange(value) {
    this.setState({ captchaToken: value });
  }
  handleChange(event) {
    let fieldName = event.currentTarget.id,
      inputValue = event.currentTarget.value;
    switch (fieldName) {
      //////////////////////////////////// FIRSTNAME ///////////////////////////////////
      case "firstNameInput":
        // inputValue = filterNameInputValue(inputValue);
        inputValue === ""
          ? this.setState({ [fieldName]: inputValue })
          : this.setState({
              [fieldName]: inputValue,
              firstNameError: false,
              firstNameMessage: "",
            });
        break;
      //////////////////////////////////// LASTNAME ////////////////////////////////////
      case "lastNameInput":
        // inputValue = filterNameInputValue(inputValue);
        inputValue === ""
          ? this.setState({ [fieldName]: inputValue })
          : this.setState({
              [fieldName]: inputValue,
              lastNameError: false,
              lastNameMessage: "",
            });
        break;
      ////////////////////////////////////// EMAIL /////////////////////////////////////
      case "signUpEmailInput":
        inputValue === ""
          ? this.setState({ [fieldName]: inputValue })
          : this.setState({
              [fieldName]: inputValue,
              emailError: false,
              emailMessage: "",
            });
        break;
    }
  }
  handleCheckbox(event) {
    this.setState({ checkbox: event.target.checked });
  }

  validateEmailAddress = () => {
    /**Function for checking if the email is valid or not */
    let emailValue = this.state.signUpEmailInput;
    let validationObj = validEmail(emailValue);
    if (!validationObj.validated) {
      this.setState({
        emailError: true,
        emailMessage: validationObj.errorMessage,
      });
    }
    return validationObj.validated;
  };

  validNameInput = (key, value) => {
    /**Function for checking if the first name and last name is valid or not */
    let validationObj = validName(value, key);
    if (!validationObj.validated) {
      switch (key) {
        case "firstName":
          this.setState({
            firstNameError: true,
            firstNameMessage: validationObj.errorMessage,
          });
          break;
        case "lastName":
          this.setState({
            lastNameError: true,
            lastNameMessage: validationObj.errorMessage,
          });
          break;

        default:
          break;
      }
    }
    return validationObj.validated;
  };

  onClickValidationsChecker = () => {
    /**Function that checks the validations of email , first name and last name input */

    let emailValidate = this.validateEmailAddress();
    if (emailValidate)
      /** returns true if all the values of validation gives true */
      return true;
  };
  setPasswordClick = () => {
    let self = this;
    const { signUpEmailInput } = self.state;
    this.props.forgotPasswordRequest(
      { eMail: signUpEmailInput },
      (data) => {
        if (data.payload.data && data.payload.data.statusCode === 200) {
          this.showSnackBar(`${data.payload.data.data}`, "success");
          this.RedirectToInvitationEmail();
        }
      },
      (error) => {}
    );
  };

  SignUpBtnClick() {
    /**Function for submitting form values of sign up page to backend */
    let self = this;
    const { signUpEmailInput, invitedBy, invitedTeamId } = this.state;
    const { location } = this.props;
    this.setState({
      btnQuery: "progress",
    });
    let parsedUrl = queryString.parse(location.search);
    if (
      self.onClickValidationsChecker() &&
      this.state.captchaToken &&
      this.state.captchaToken.length != 0
    ) {
      SignUpRequest(
        this.state.signUpEmailInput,
        parsedUrl.plan ? parsedUrl.plan : 'free',
        //Success

        (response) => {
          this.props.history.push("/account/verifyemail");
          // this.props.history.push(
          //   `/welcome/createprofile?email=${signUpEmailInput}&plan=${
          //     parsedUrl.plan ? parsedUrl.plan : ""
          //   }`
          // );
        },
        //Failure
        (error) => {
          this.setState({
            btnQuery: "",
            emailMessage: error.data.message,
            emailError: true,
          });
        }
      );
      // let searchQuery = this.props.history.location.search; // targeting url
      // let plan = queryString.parseUrl(searchQuery).query.plan; //Parsing the url and extracting the plan type using query string

      // let obj = {
      //   firstName: self.state.firstNameInput.trim(),
      //   lastName: self.state.lastNameInput.trim(),
      //   email: self.state.signUpEmailInput.trim(),
      //   IsSubscriptionEnabled: self.state.checkbox,
      //   trialType: plan ? plan : null,
      //   invitedTeamId,
      //   invitedBy
      // };
      // const {
      //   SignUpRequest,
      //   RedirectToInvitationEmail,
      //   showSnackBar
      // } = self.props;
      // this.setState({ btnQuery: "progress" });
      // SignUpRequest(
      //   obj,
      //   data => {
      //     this.setState({ btnQuery: "" });
      //     RedirectToInvitationEmail();
      //     this.props.history.push("/account/verifyemail");
      //   },
      //   error => {
      //     if (error.message === "Email not verified") {
      //       this.setState({
      //         btnQuery: "",
      //         emailMessage: (
      //           <span>
      //             Email {signUpEmailInput} is not verified. Please{" "}
      //             <a onClick={this.setPasswordClick}>click here</a> to verify
      //             email address.
      //           </span>
      //         ),
      //         emailError: true
      //       });
      //     } else {
      //       this.setState({
      //         btnQuery: "",
      //         emailMessage: error.message,
      //         emailError: true
      //       });
      //     }
      //   }
      // );
    }
  }
  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };

  render() {
    const {
      firstNameError,
      lastNameError,
      emailError,
      firstNameMessage,
      lastNameMessage,
      emailMessage,
      checkbox,
      btnQuery,
      signUpEmailInput
    } = this.state;
    const { classes, location } = this.props;
    const CaptchaClass = {
      width: "100%",
      marginBottom: "10px",
    };
    let parseUrl = queryString.parseUrl(location.search);
    return (
      <>
        <Helmet>
          <title>Register | nTask</title>
        </Helmet>
        <form className={classes.LoginForm} style={{ marginTop: 30 }}>
          {/* <Grid container spacing={24}>
            <Grid item xs={6}>
              <DefaultTextField
                label="First Name"
                fullWidth={true}
                errorState={firstNameError}
                errorMessage={firstNameMessage}
                defaultProps={{
                  id: "firstNameInput",
                  onChange: this.handleChange,
                  value: this.state.firstNameInput,
                  autoFocus: true,
                  placeholder: "Enter first name",
                  inputProps: { maxLength: 40 }
                }}
              />
            </Grid>
            <Grid item xs={6}>
              <DefaultTextField
                label="Last Name"
                fullWidth={true}
                errorState={lastNameError}
                errorMessage={lastNameMessage}
                defaultProps={{
                  id: "lastNameInput",
                  onChange: this.handleChange,
                  value: this.state.lastNameInput,
                  placeholder: "Enter last name",
                  inputProps: { maxLength: 40 }
                }}
              />
            </Grid>
          </Grid> */}
          <DefaultTextField
            label="Email"
            fullWidth={true}
            errorState={emailError}
            errorMessage={emailMessage}
            defaultProps={{
              id: "signUpEmailInput",
              onChange: this.handleChange,
              value: this.state.signUpEmailInput,
              placeholder: "Enter email address",
              inputProps: { maxLength: 50 },
            }}
          />
          <FormControlLabel
            classes={{ root: classes.updatesCheckboxCnt }}
            control={
              <Checkbox
                checked={checkbox}
                onChange={this.handleCheckbox}
                classes={{
                  root: classes.updatesCheckbox,
                  checked: classes.checked,
                }}
              />
            }
            label="I agree to recieve product updates, offers and promotions"
          />

          <div style={CaptchaClass}>
            <form
              onSubmit={() => {
                captcha.execute();
              }}
            >
              <ReCAPTCHA
                ref={(el) => {
                  captcha = el;
                }}
                sitekey={SITE_KEY_CAPTCHA}
                onChange={this.captchaOnChange}
              />
            </form>
          </div>
          <CustomButton
            onClick={this.SignUpBtnClick}
            btnType="success"
            variant="contained"
            query={btnQuery}
            disabled={btnQuery == "progress"}
          >
            Sign Up
          </CustomButton>
        </form>
      </>
    );
  }
}
export default compose(
  withRouter,
  withSnackbar,
  connect(null, {
    forgotPasswordRequest,
    validateEmailAddress,
  })
)(SignUpForm);
