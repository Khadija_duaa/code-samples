import React, { Component, Fragment } from "react";
import DefaultTextField from "../../components/Form/TextField";
import DefaultButton from "../../components/Buttons/DefaultButton";
import CustomButton from "../../components/Buttons/CustomButton";
import Typography from "@material-ui/core/Typography";
import ShieldIcon from "@material-ui/icons/VerifiedUser";
import MailIcon from "../../components/Icons/MailIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import CustomAvatar from "../../components/Avatar/Avatar";

import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

import {
  sendcode,
  sendcodetoverify,
  verifycode,
  authenticateUser
} from "../../redux/actions/authentication";

import { DeleteTempData } from "../../redux/actions/tempdata";

class TwoFactorAuth extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectMethod: true,
      sendcodeAPI: null,
      email: "",
      username: "",
      finalObj: "",
      authCode: "",
      profileImg: "",
      verifyBtnQuery: '',
      btnQuery: '',
      sendEmailBtnQuery: '',
      sendCodeBtnQuery: '',
      authCodeMessage: '',
      authCodeState: false
    };
    this.sendCode = this.sendCode.bind(this);
    this.handleEmailOptionClick = this.handleEmailOptionClick.bind(this);
    this.handleAppOptionClick = this.handleAppOptionClick.bind(this);
    this.handleVerifyCode = this.handleVerifyCode.bind(this);
  }

  handleInput = name => event => {
    this.setState({ [name]: event.target.value, authCodeState: false, authCodeMessage: '' });
  };

  sendCode() {
    this.handleEmailOptionClick(true);
  }
  componentDidMount = () => {
    if (
      this.props.tempDataState.data === "" ||
      this.props.tempDataState.data === null ||
      this.props.tempDataState.data === undefined
    ) {
      this.props.history.push("/account/login");
    } else {
      this.props.sendcode(
        {
          userName: this.props.tempDataState.data.userName,
          returnUrl: null,
          rememberMe: false
        },
        response => {
          if (response.status === 200) {
            this.setState({
              sendcodeAPI: response.data,
              email: this.props.tempDataState.data.email,
              username: this.props.tempDataState.data.userName,
              profileImg: response.data.imageUrl
            });
          } else {
            this.props.UpdateTwoFAFlag(false);
            this.props.history.push("/account/login");
          }
        }
      );
    }
  };

  handleAppOptionClick() {
    let obj = this.state.sendcodeAPI;
    obj.password = this.props.tempDataState.data.password;
    obj.userName = this.props.tempDataState.data.userName;
    obj.selectedProvider = "Authy Token";
    const { sendcodetoverify, showSnackBar } = this.props;
    this.setState({ sendCodeBtnQuery: 'progress' });
    sendcodetoverify(obj, response => {
      this.setState({ sendCodeBtnQuery: '' });
      if (response.status === 200) {
        this.setState({
          selectMethod: false,
          finalObj: obj,
        });
      } else {
        showSnackBar("Opps! Something went wrong", 'error');
      }
    });
  }

  redirectToLogin = () => {
    this.props.navigateToLogin();
  };
  handleVerifyCode() {

    let self = this;
    if (this.state.authCode) {
      let obj = this.state.finalObj;
      obj.code = this.state.authCode;
      const { verifycode, showSnackBar } = this.props;
      this.setState({ verifyBtnQuery: 'progress' });
      verifycode(obj, response => {
        this.setState({ verifyBtnQuery: '' });
        if (
          response &&
          response.status === 200
        ) {
          localStorage.setItem(
            "token",
            `Bearer ${this.props.tempDataState.data.access_token}`
          );
          sessionStorage.setItem(
            "token",
            `Bearer ${this.props.tempDataState.data.access_token}`
          );
          this.props.DeleteTempData();
          this.props.authenticateUser(true)
          this.props.history.push("/tasks");
        } else {
          showSnackBar("Oops! Your verification code is expired.", 'error');
        }
      });
    } else {
      this.setState({ authCodeState: true, authCodeMessage: 'Oops! Please enter the verification code!' })
      // showSnackBar("Please fill the Authentication Code", 'error');
    }
  }
  handleEmailOptionClick(flag) {
    let obj = this.state.sendcodeAPI;
    obj.password = this.props.tempDataState.data.password;
    obj.userName = this.props.tempDataState.data.userName;
    obj.selectedProvider = "Email Code";
    const { sendcodetoverify, showSnackBar } = this.props;
    this.setState({ sendEmailBtnQuery: 'progress' });
    sendcodetoverify(obj, response => {
      this.setState({ sendEmailBtnQuery: '' });
      if (response.status === 200) {
        this.setState({
          selectMethod: false,
          finalObj: obj,
        });
        if (flag)
          showSnackBar("Authentication code is sent to you on your email address. Please check.", 'success');
      } else {
        if (flag)
          showSnackBar("Unable to send Authentication code", 'error');
      }
    });
  }
  render() {
    const { authCode, selectMethod, email, username, profileImg, verifyBtnQuery, btnQuery, sendEmailBtnQuery, authCodeState, sendCodeBtnQuery, authCodeMessage } = this.state;
    const { classes, theme } = this.props;
    let convertedEmail = email.substr(email.indexOf("@"));
    convertedEmail = String.prototype.padStart(email.length - convertedEmail.length, '*') + convertedEmail;
    return (
      <div className={classes.twoFactorAuthForm}>
        <div className={classes.twoFactorAuthProfileCnt}>
          <Typography variant="h2" align="center" style={{ marginBottom: 10 }}>
            {selectMethod ? "Select Authentication Method" : "Verify Code"}
          </Typography>
          {!selectMethod ? (
            <Typography variant="body1" align="center">
              "Verification code has been sent to you."
            </Typography>
          ) : null}

          <div className={classes.twoFactorAuthProfilePic}>
            <CustomAvatar
              otherMember={{
                imageUrl: profileImg,
                fullName: username || ' ',
                lastName: "",
                email: email || ' ',
                isOnline: false,
                isOwner: false,
              }}
              disableCard={true}
              size="xxlarge"
            />
            {/* <img src={profileImg} alt="Profile Picture" /> */}
          </div>
          <Typography variant="h4">{username}</Typography>
        </div>
        {selectMethod ? (
          <Fragment>
            <CustomButton
              onClick={this.handleAppOptionClick}
              style={{ marginBottom: 20, fontSize: "14px" }}
              btnType="white"
              variant="contained"
              query={sendCodeBtnQuery}
              disabled={sendCodeBtnQuery == "progress" || sendEmailBtnQuery == "progress"}
            >
              <ShieldIcon
                fontSize="small"
                htmlColor={theme.palette.secondary.light}
                style={{ position: 'absolute', left: 16 }}
              />
              {`Approve a request on my authenticator app`}
            </CustomButton>
            <CustomButton
              onClick={() => { this.handleEmailOptionClick(false) }}
              style={{ marginBottom: 20, fontSize: "14px" }}
              btnType="white"
              variant="contained"
              query={sendEmailBtnQuery}
              disabled={sendEmailBtnQuery == "progress" || sendCodeBtnQuery == "progress"}
            >
              <SvgIcon
                viewBox="0 0 485.211 485.211"
                htmlColor={theme.palette.secondary.light}
                fontSize="small"
                style={{ position: 'absolute', left: 16 }}
              >
                <MailIcon />
              </SvgIcon>
              {`Send Email to ${convertedEmail}`}
            </CustomButton>
            <DefaultButton text="Cancel" buttonType="dangerBtn" onClick={this.redirectToLogin} />
          </Fragment>
        ) : (
            <Fragment>
              <DefaultTextField
                fullWidth={true}
                errorState={authCodeState}
                error={authCodeState}
                errorMessage={authCodeMessage}
                labelProps={{ shrink: true }}
                defaultProps={{
                  id: "authCode",
                  autoFocus: true,
                  placeholder: "Enter code here",
                  onChange: this.handleInput("authCode")
                }}
              />
              <CustomButton
                style={{ marginBottom: 10 }}
                onClick={this.handleVerifyCode}
                btnType="success"
                variant="contained"
                query={verifyBtnQuery}
                disabled={verifyBtnQuery == "progress" || btnQuery == "progress"}
              >
                Verify
            </CustomButton>
              <CustomButton
                style={{ marginBottom: 10 }}
                onClick={this.sendCode}
                btnType="success"
                variant="contained"
                query={btnQuery}
                disabled={verifyBtnQuery == "progress" || btnQuery == "progress"}
              >
                Resend Code
            </CustomButton>
              <DefaultButton text="Cancel" buttonType="dangerBtn" onClick={this.redirectToLogin} />
            </Fragment>
          )}
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    tempDataState: state.tempData
  }
}
//export default TwoFactorAuth;
export default compose(
  withRouter,
  connect(
    mapStateToProps,
    {
      sendcode,
      sendcodetoverify,
      verifycode,
      DeleteTempData,
      authenticateUser
    }
  )
)(TwoFactorAuth);
