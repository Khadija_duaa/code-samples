import React, { Component } from "react";
import SvgIcon from "@material-ui/core/SvgIcon";
import Typography from "@material-ui/core/Typography";
import InviteIcon from "../../components/Icons/InviteIcon";
import { withRouter } from "react-router-dom";

class InvitationEmail extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.goToHome = this.goToHome.bind(this);
  }
  goToHome() {
    this.props.history.replace(`/account/login`);
  }
  render() {
    const { theme, classes } = this.props;
    return (
      <div className={classes.invitationEmailInnerCnt}>
        <SvgIcon
          viewBox="0 0 458.6 434.9"
          className={classes.mailIcon}
          htmlColor={theme.palette.secondary.light}
        >
          <InviteIcon />
        </SvgIcon>
        <Typography variant="h1" classes={{ h1: classes.mainHeadingText }}>
          You've got mail!
        </Typography>

        <Typography variant="body2" align="center">
          You've been successfully registered. Now check your email to complete
          the signup.
        </Typography>
        <br />
        <Typography variant="body2" align="center">
          Check your spam / junk folder if you don't see the email in your
          inbox.
        </Typography>


      </div>
    );
  }
}

export default withRouter(InvitationEmail);
