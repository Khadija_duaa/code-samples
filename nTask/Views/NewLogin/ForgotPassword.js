import React, { Component } from "react";
import Typography from "@material-ui/core/Typography";
import DefaultTextField from "../../components/Form/TextField";
import CustomButton from "../../components/Buttons/CustomButton";
import Hotkeys from "react-hot-keys";
import DefaultButton from "../../components/Buttons/DefaultButton";
import { compose } from "redux";
import { withSnackbar } from "notistack";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";


import { validateUserNameAndEmailField } from '../../utils/formValidations';
import { forgotPasswordRequest } from "../../redux/actions/forgotPassword";

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      forgotEmail: this.props.email,
      forgotEmailMessage: "",
      forgotEmailError: false,
      forgotPasswordBtnQuery: '',
      open: false
    };
    this.setPassword = this.setPassword.bind(this);
    this.handleEmail = this.handleEmail.bind(this);
  }
  handleClick = () => {
    this.setState({ open: true });
  };

  handleEmail_ForgotPassword(email_fp) { /** function for calling forgot pass api and then navigate to sign in page */
    let self = this;
    self.setState({ forgotPasswordBtnQuery: "progress" });
    self.props.forgotPasswordRequest(
      { eMail: email_fp },
      data => {
        this.setState({ forgotPasswordBtnQuery: "" });
        if (data.payload.data && data.payload.data.statusCode === 200) {
          self.showSnackBar(`${data.payload.data.data}`, "info");
          self.props.history.push("/account/login");
        }
      },
      error => {
        let errMessage = error.payload.data.data;
        self.setState({
          forgotEmailError: true,
          forgotPasswordBtnQuery: "",
          forgotEmailMessage: errMessage.message ? errMessage.message : errMessage
        });
      }
    );
  }
  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right"
        },
        variant: type ? type : "info"
      }
    );
  };

  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };
  handleEmail(event) {
    this.setState({
      [event.currentTarget.id]: event.currentTarget.value, forgotEmailMessage: "",
      forgotEmailError: false,
    });
  }
  setPassword() { /** function checks if email is valid */
    const inputValue = this.state.forgotEmail.trim();
    const validationObj = validateUserNameAndEmailField('email/username', inputValue);
    if (validationObj['isError']) {
      this.setState({
        forgotEmailError: validationObj['isError'],
        forgotEmailMessage: validationObj['message']
      });
    }
    else {
      this.handleEmail_ForgotPassword(inputValue); /** function calling if email is valid */
      this.setState({
        forgotEmailMessage: "",
        forgotEmailError: false,
        open: true
      });
    }
  }
  render() {
    const { forgotEmail, forgotEmailError, forgotEmailMessage, forgotPasswordBtnQuery } = this.state;
    const { classes, loginAction } = this.props;


    return (
      <div className={classes.LoginForm}>
        <Hotkeys keyName="enter" onKeyDown={loginAction}>
          <DefaultTextField
            label="Email / Username"
            fullWidth={true}
            errorState={forgotEmailError}
            errorMessage={forgotEmailMessage}
            defaultProps={{
              id: "forgotEmail",
              onChange: this.handleEmail,
              value: forgotEmail,
              placeholder: "Enter email or username",
              autoFocus: true,
              inputProps: { maxLength: 250 }
            }}
          />
          <CustomButton
            onClick={this.setPassword}  /** function calling on click reset password */
            style={{ marginBottom: 10 }}
            btnType="success"
            variant="contained"
            query={forgotPasswordBtnQuery}
            disabled={forgotPasswordBtnQuery == "progress"}
          >
            {"Reset"}
          </CustomButton>
          <CustomButton
            onClick={loginAction} /** */
            btnType="success"
            variant="contained"
            disabled={forgotPasswordBtnQuery == "progress"}
          >
            {"Sign In"}
          </CustomButton>
        </Hotkeys>
      </div>
    );
  }
}

export default compose(
  withRouter,
  withSnackbar,
  connect(
    null,
    {
      forgotPasswordRequest,
    }
  )
)(ForgotPassword);

