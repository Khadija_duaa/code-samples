import React, { Component } from "react";
import SvgIcon from "@material-ui/core/SvgIcon";
import Typography from "@material-ui/core/Typography";
import InviteIcon from "../../components/Icons/InviteIcon";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import invalidInviteStyles from "./style";
import CustomButton from "../../components/Buttons/CustomButton";
import Paper from "@material-ui/core/Paper";
import { resendEmailVerification } from "../../redux/actions/profile";
import { Logout } from "../../redux/actions/logout";
import { connect } from "react-redux";

class InvalidInvite extends Component {
  constructor(props) {
    super(props);
    this.state = { btnQuery: "", step: 0 };
  }
  handleEmailSend = () => {
    this.setState({ btnQuery: "progress" });
    resendEmailVerification(() => {
      this.setState({ btnQuery: "", step: 1 });
    });
  };
  logoutUser = () => {
    this.props.Logout(null, this.props.history);
  };
  render() {
    const { theme, classes } = this.props;
    const { btnQuery, step } = this.state;

    return (
      <div className={classes.varticalAlignParent}>
        <div className={classes.varticalAlignChild}>
          <div className={classes.loginMainCnt}>
            <Paper className={classes.verifyEmailCnt}>
              <SvgIcon
                viewBox="0 0 458.6 434.9"
                className={classes.mailIcon}
                htmlColor={theme.palette.secondary.light}
              >
                <InviteIcon />
              </SvgIcon>
              <Typography
                variant="h1"
                classes={{ h1: classes.mainHeadingText }}
              >
                There seems to be an issue with the invite.
              </Typography>
              <Typography
                variant="body1"
                align="center"
                style={{
                  color: theme.palette.text.darkGray,
                  marginBottom: 5,
                }}
              >
                Please contact the team owner to reinvite you to the team.
              </Typography>
            </Paper>
          </div>
        </div>
      </div>
    );
  }
}

export default compose(
  connect(null, { Logout }),
  withStyles(invalidInviteStyles, { withTheme: true }),
  withRouter
)(InvalidInvite);
