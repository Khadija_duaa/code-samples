import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import React, { useMemo, useState, useContext } from "react";
import { connect, useDispatch } from "react-redux";
import { compose } from "redux";
import CustomSelectDropdown from "../../../components/Dropdown/CustomSelectDropdown/Dropdown";
import { filtersData } from "../../../helper/generateIssuesOverviewDropdownData";
import issueOverviewHeaderStyles from "./issuesOverviewHeader.style";
import IssueOverviewContext from "../Context/issueOverview.context";
import { getWidget, updateWidgetSetting } from "../../../redux/actions/overviews";
import {
  dispatchIssueWidgetData,
  dispatchWidgetSetting,
  dispatchColumns,
} from "../Context/actions";
import CustomMultiSelectDropdown from "../../../components/Dropdown/CustomMultiSelectWithSearchBarDropdown/Dropdown";
import { generateWorkspaceData } from "../../../helper/generateTasksOverviewDropdownData";
import { dispatchIssues, exportBulkIssue } from "../../../redux/actions/issues";
import { dispatchProjects } from "../../../redux/actions/projects";
import { dispatchTasks } from "../../../redux/actions/tasks";
import TeamColumnSelectionDropdown from "../../../components/Dropdown/SelectedItemsDropDown/teamColumnsDropdown";
import DefaultTextField from "../../../components/Form/TextField";
import { grid } from "../../../components/CustomTable2/gridInstance";
import debounce from "lodash/debounce";
import { doesFilterPass } from "../../../components/ReportingList/reportFilter.utils";
import CustomButton from "../../../components/Buttons/CustomButton";
import CustomIconButton from "../../../components/Buttons/IconButton";
import ImportExport from "@material-ui/icons/ImportExport";
import fileDownload from "js-file-download";
import { exportOverviewIssues } from "../../../redux/actions/reports";
import IconRefresh from "../../../components/Icons/IconRefresh";
import SvgIcon from "@material-ui/core/SvgIcon";
import CustomTooltip from "../../../components/Tooltip/Tooltip";
import { addPendingHandler } from "../../../redux/actions/backProcesses";

function IssuesOverviewHeader(props) {
  const {
    classes,
    theme,
    profileState: { workspace },
    feature,
    groupType,
    issues,
    quickFilteredData,
    isQuickFilterApplied
  } = props;
  const {
    state: { widgetSettings, issueWidgetData, filteredIssues, allColumns },
    dispatch,
  } = useContext(IssueOverviewContext);
  const dispatchFn = useDispatch();
  const [btnQuery, setBtnQuery] = useState("");
  const [exportBtnQuery, setExportBtnQuery] = useState("");
  const [refreshBtnQuery, setRefreshBtnQuery] = useState("");
  const [disableRefreshBtn, setDisableRefreshBtn] = useState(false);
  const [disableExportBtn, setDisableExportBtn] = useState(false);
  const [refreshInterval, setRefreshInterval] = useState(30);
  // Handle workspace selection in dropdown
  const onWorkspaceSelect = options => {
    let selectAll = options.find(o => o.value == "1") || false;
    let workspaceIds = options.map(o => o.obj.teamId);
    if (selectAll) {
      if (selectAll && options.length > 0) {
        if (widgetSettings.selectedWorkspaces.length > 0 && selectAll) {
          workspaceIds = workspaceIds;
        } else {
          workspaceIds = workspaceIds.filter(w => w !== "1");
        }
      } else {
        workspaceIds = [];
      }
    }

    dispatchWidgetSetting(dispatch, { selectedWorkspaces: workspaceIds });
  };
  const handleFetchData = () => {
    props.handleIsLoading(true);
    setBtnQuery("progress");
    updateWidgetSetting(
      "issue",
      { ...widgetSettings },
      // Success
      res => {
        getWidget(
          "issue",
          //Success
          resp => {
            props.handleIsLoading(false);
            setBtnQuery("");
            props.dispatchIssues(resp.data.entity.issues);
            props.dispatchProjects(resp.data.entity.projects);
            props.dispatchTasks(resp.data.entity.tasks);
            dispatchIssueWidgetData(dispatch, resp.data.entity);
            props.handleIsLoading(false);
          },
          err => {
            props.handleIsLoading(false);
            setBtnQuery("");

            dispatchIssueWidgetData(dispatch, {
              issues: [],
              tasks: [],
              projects: [],
              permissions: [],
            });
          }
        );
      }
    );
  };
  // Handle Filter Select
  const handleFilterSelect = option => {
    dispatchWidgetSetting(dispatch, { selectedFilter: option.id });
    updateWidgetSetting(
      "issue",
      { ...widgetSettings, selectedFilter: option.id },
      // Success
      res => { }
    );
  };

  // Generate workspace dropdown data
  const workspacesDropdownData = useMemo(() => {
    return generateWorkspaceData(workspace);
  }, workspace);

  const selectedFilterOption = filtersData.find(f => f.id === widgetSettings.selectedFilter);

  const selectedWorkspaceOptions = workspacesDropdownData.filter(w => {
    return widgetSettings.selectedWorkspaces.includes(w.obj.teamId);
  });

  const updateColumnCallback = updatedColumns => {
    dispatchColumns(dispatch, updatedColumns);
  };
  const throttleHandleSearch = debounce(data => {
    grid.grid && grid.grid.setQuickFilter(data);
  }, 1000);

  const getTotalIssues = () => {
    const filteredIssues = isQuickFilterApplied
      ? quickFilteredData.filter(t => doesFilterPass({ data: t }, "issueoverview"))
      : issues.filter(t => doesFilterPass({ data: t }, "issueoverview"));
    return filteredIssues.length;
  };
  //handle task search
  const handleSearch = e => {
    throttleHandleSearch(e.target.value);
  };
  const handleExportIssues = () => {
    setDisableExportBtn(true);
    const obj = {
      type: `issues`,
      apiType: 'post',
      data: [],
      fileName: 'issues.xlsx',
      apiEndpoint: `api/export/issueoverview`,
    }
    addPendingHandler(obj, dispatchFn);
    let tempInterval = 30;
    const updateTimeInterval = setInterval(() => {
      tempInterval = tempInterval - 1; 
      setRefreshInterval(tempInterval);
      if (!tempInterval) {
        setDisableExportBtn(false);
        // console.log('destroy interval here ', tempInterval);
        clearInterval(updateTimeInterval);
      }
    }, 1000);
    
    // setExportBtnQuery('progress')
    // exportOverviewIssues(res => {
    //   fileDownload(res.data, "issues.xlsx");
    //   setExportBtnQuery('')
    // });
  }
  const handleRefreshGrid = () => {
    props.handleIsLoading(true);
    setRefreshBtnQuery("progress");
    updateWidgetSetting(
      "issue",
      { ...widgetSettings },
      // Success
      res => {
        getWidget(
          "issue",
          //Success
          resp => {
            props.handleIsLoading(false); 
            props.dispatchIssues(resp.data.entity.issues);
            props.dispatchProjects(resp.data.entity.projects);
            props.dispatchTasks(resp.data.entity.tasks);
            dispatchIssueWidgetData(dispatch, resp.data.entity);


            setRefreshBtnQuery("");
            setDisableRefreshBtn(true);
            let tempInterval = 30;
            const updateTimeInterval = setInterval(() => {
              tempInterval = tempInterval - 1;
              console.log(tempInterval)
              setRefreshInterval(tempInterval);
              if (!tempInterval) {
                setDisableRefreshBtn(false);
                // console.log('destroy interval here ', tempInterval);
                clearInterval(updateTimeInterval);
              }
            }, 1000);
          },
          err => {
            props.handleIsLoading(false);
            setRefreshBtnQuery(""); 
            dispatchIssueWidgetData(dispatch, {
              issues: [],
              tasks: [],
              projects: [],
              permissions: [],
          });
          }
        );
      }
    );
  }
  return (
    <div className={classes.headerCnt}>
      <div className={classes.headerLeftCnt}>
        <Typography variant="h1" className={classes.mainHeadingIssues}>
          Issues
          <span
            style={{
              color: "rgba(126, 126, 126, 1)",
              fontSize: "12px",
              fontWeight: 500,
              lineHeight: 1.5,
            }}>
            ({getTotalIssues()})
          </span>
        </Typography>

        <DefaultTextField
          fullWidth={false}
          // errorState={forgotEmailError}
          error={false}
          // errorMessage={forgotEmailMessage}
          formControlStyles={{ width: 250, marginBottom: 0, marginRight: 10 }}
          defaultProps={{
            id: "issueListSearch",
            onChange: handleSearch,
            placeholder: `Search ${groupType}`,
            autoFocus: true,
            inputProps: { maxLength: 150, style: { padding: "7px 14px" } },
          }}
        />
        <CustomSelectDropdown
          options={() => filtersData}
          option={selectedFilterOption}
          onSelect={handleFilterSelect}
          isDashboard={true}
          height="140px"
          scrollHeight={180}
          buttonProps={{
            variant: "contained",
            btnType: "white",
            labelAlign: "left",
            style: { minWidth: 140, padding: "3px 4px 3px 8px", marginRight: 8 },
          }}
        />
        <CustomMultiSelectDropdown
          placeholder={"Workspces"}
          options={() => workspacesDropdownData}
          option={selectedWorkspaceOptions}
          onSelect={onWorkspaceSelect}
          scrollHeight={400}
          size={"large"}
          buttonProps={{
            variant: "contained",
            btnType: "white",
            labelAlign: "left",
            style: { minWidth: "100%", padding: "3px 4px 3px 8px" },
          }}
        />
        <CustomButton
          btnType="blue"
          variant="contained"
          className={classes.customButtonBlue}
          onClick={handleFetchData}
          query={btnQuery}
          disabled={btnQuery == "progress"}>
          Apply
        </CustomButton>
        <div className={classes.importExportButtonCnt}>
          <CustomButton
            onClick={handleExportIssues}
            variant={'outlined'}
            btnType="gray"
            query={exportBtnQuery}
            disabled={disableExportBtn}
            style={{ padding: "5px 7px", height: 32 ,
            background: disableExportBtn ? theme.palette.border.lightBorder : 'transparent'
          }}
          >
            <ImportExport />
            <span className={classes.labelImportExport}>Export Issues</span>
          </CustomButton>
        </div>
        <CustomTooltip
          helptext={
            disableRefreshBtn ? `Refresh will enables in ${refreshInterval} sec` : 'Refresh Grid'
          }
          placement="top"
          style={{ color: theme.palette.common.white }}>
          <CustomButton
            onClick={event => disableRefreshBtn ? () => { } : handleRefreshGrid()}
            query={refreshBtnQuery}
            style={{
              padding: "8px 8px",
              borderRadius: "4px",
              display: "flex",
              justifyContent: "space-between",
              minWidth: "auto",
              whiteSpace: "nowrap",
              marginLeft: 10,
              height: 30,
              background: disableRefreshBtn ? theme.palette.border.lightBorder : 'transparent'
            }}
            btnType={"white"}
            variant="contained">
            <SvgIcon
              viewBox="0 0 12 12.015"
              className={classes.qckFfilterIconRefresh}>
              <IconRefresh />
            </SvgIcon>
          </CustomButton>
        </CustomTooltip>
        <TeamColumnSelectionDropdown
          feature={feature}
          groupType={groupType}
          hideColumns={["matrix"]}
          allColumns={allColumns}
          updateColumn={updateColumnCallback}
          btnProps={{
            style: {
              border: "1px solid #dddddd",
              padding: "5px 10px",
              borderRadius: 4,
              marginLeft: 10,
            },
          }}
        />
      </div>
    </div>
  );
}
const mapStateToProps = state => {
  return {
    profileState: state.profile.data,
    issues: state.issues.data,
    reportingFilters: state.reportingFilters?.issueoverview,
  };
};
IssuesOverviewHeader.defaultProps = {
  issues: [],
};
export default compose(
  withStyles(issueOverviewHeaderStyles, { withTheme: true }),
  connect(mapStateToProps, {
    dispatchIssues,
    dispatchProjects,
    dispatchTasks,
  })
)(IssuesOverviewHeader);
