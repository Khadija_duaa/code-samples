const taskOverviewHeaderStyles = theme => ({
  headerCnt: {
    padding: "11px 10px 11px 16px",
    // borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  customButtonBlue: {
    fontSize: "13px !important",
    height: 30,
    marginLeft: 10,
  },
  mainHeading: {
    marginRight: 'auto',
    fontSize: "18px !important"
  },
  mainHeadingIssues: {
    marginRight: 'auto',
    fontSize: "18px !important",
    fontFamily: theme.typography.fontFamilyLato
  },
  subTaskSwitchCnt: {
    display: 'flex',
    alignItems: 'center',
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: 4,
    padding: '5px 0 4px 8px',
    marginLeft: 8,
    width: 100
  },
  subTaskSwitchLabel: {
    fontSize: "12px !important",
  },
  headerLeftCnt: {
    display: 'flex',
    alignItems: 'center',
    flex: 1,
  },
  importExportButtonCnt: {
    margin: "0 0 0 8px",
    '& svg':{
      color: theme.palette.icon.gray600
    }
  },
  labelImportExport:{
    paddingRight: 4,
    fontSize: "12px !important",
    color: "#7e7e7e",
    fontWeight: 500
  },
  qckFfilterIconRefresh: {
    fontSize: "12px !important",
    margin: "0px 5px",
  },
});

export default taskOverviewHeaderStyles;
