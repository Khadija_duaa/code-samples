import React, { useContext, useEffect, useState } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import IssueDetails from "../../../Views/Issue/IssueDetails/IssueDetails";
import CustomTable from "../../../components/CustomTableIssue/CustomTable.cmp";
import IssueOverviewContext from "../Context/issueOverview.context";
import {
  dispatchIssueGroupByAssignee,
  dispatchIssue,
  dispatchIssueWidgetData,
  dispatchIssueGroupByTask
} from "../Context/actions";
import { UpdateIssue } from "../../../redux/actions/issues";
import { getWidget } from "../../../redux/actions/overviews";
import { issueDetailDialogState } from "../../../redux/actions/allDialogs";
import { dispatchIssues } from "../../../redux/actions/issues";
import { dispatchProjects } from "../../../redux/actions/projects";
import { dispatchTasks } from "../../../redux/actions/tasks";

function IssueList(props) {
  const {
    state: { widgetSettings, issueWidgetData, filteredIssues, groupedIssues, selectedIssue },
    dispatch,
  } = useContext(IssueOverviewContext);

  //Getting selected columns
  const { selectedColumns } = widgetSettings;
  const [IssueData, setIssueData] = useState([]);

  //Getting Issue data based upon filter
  const getIssueData = () => {
    const { selectedFilter } = widgetSettings;
    if (groupedIssues.length) {
      return groupedIssues;
    }
    if (selectedFilter === "viewAllIssues") {
      return issueWidgetData.issues;
    }
    return filteredIssues[selectedFilter];
  };
  const isUpdated = data => {
    props.UpdateIssue(
      data,
      data => {},
      error => {}
    );
  };
  const closeIssueDetailsPopUp = () => {
    props.handleIsLoading(true);
    dispatchIssue(dispatch, null);
    getWidget(
      "issue",
      //Success
      res => {
        props.handleIsLoading(false);
        props.dispatchIssues(res.data.entity.issues);
        props.dispatchProjects(res.data.entity.projects);
        props.dispatchTasks(res.data.entity.tasks);
        dispatchIssueWidgetData(dispatch, res.data.entity);
      },
      err => {
        props.handleIsLoading(false);
        dispatchIssueWidgetData(dispatch, { projects: [], issues: [], risks: [], permissions: [] });
      }
    );
  };

  useEffect(() => {
    setIssueData(getIssueData());
  }, [groupedIssues]);

  useEffect(() => {
    const { selectedFilter } = widgetSettings;
    if (widgetSettings.groupBy.includes("assignee")) {
      const issues =
        selectedFilter == "viewAllIssues" ? issueWidgetData.issues : filteredIssues[selectedFilter];
      dispatchIssueGroupByAssignee(dispatch, issues);
    } else if (widgetSettings.groupBy.includes("task")) {
      const issues =
        selectedFilter == "viewAllIssues" ? issueWidgetData.issues : filteredIssues[selectedFilter];
      dispatchIssueGroupByTask(dispatch, issues);
    } else {
      setIssueData(getIssueData());
    }
  }, [issueWidgetData.issues, widgetSettings.selectedFilter, widgetSettings.filteredIssues]);

  let workspaceIssuePermission = selectedIssue && issueWidgetData.permissions.find(
    p => p.workSpaceId === selectedIssue.teamId
  ); /** workspace issue permission */
  return (
    <>
      <CustomTable
        data={IssueData}
        cols={selectedColumns}
        dispatch={dispatch}
        widgetSettings={widgetSettings}
        isLoading={props.isLoading}
      />
      {selectedIssue && (
        props.issueDetailDialogState(null,{
          id: selectedIssue.id,
          afterCloseCallBack: () => {
            closeIssueDetailsPopUp();
          },
          isUpdated: isUpdated,
          closeActionMenu: closeIssueDetailsPopUp,
          issueWorkspacePer : workspaceIssuePermission.issue
        })
      )}
    </>
  );
}

export default compose(
  connect(null, {
    UpdateIssue,
    issueDetailDialogState,
    dispatchIssues,
    dispatchProjects,
    dispatchTasks
  })
)(IssueList);
