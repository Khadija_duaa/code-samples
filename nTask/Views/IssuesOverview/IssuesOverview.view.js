import React, { useContext, useReducer, useEffect, useState } from "react";
import { compose } from "redux";
import { connect, useDispatch, useSelector } from "react-redux";

import withStyles from "@material-ui/core/styles/withStyles";
import IssuesOverviewHeader from "./IssuesOverviewHeader/IssuesOverviewHeader.view";
import issuesOverview from "./issuesOverview.style";
import IssueOverviewContext from "./Context/issueOverview.context";
import reducer from "./Context/reducer";
import initialState from "./Context/initialState";
import { getWidgetSetting, getWidget } from "../../redux/actions/overviews";
import { dispatchIssues } from "../../redux/actions/issues";
import { getTeamColumn } from "../../redux/actions/columns";
import { dispatchProjects } from "../../redux/actions/projects";
import { dispatchTasks } from "../../redux/actions/tasks";
import { dispatchWidgetSetting, dispatchIssueWidgetData, dispatchColumns } from "./Context/actions";
import RepostingList from "../../components/ReportingList/ReportingList.cmp";
import IssueList from "./IssueList/IssueList.view";
import TeamColumnSelectionDropdown from "../../components/Dropdown/SelectedItemsDropDown/teamColumnsDropdown";
import instance from "../../redux/instance";
import moment from "moment";
import { issueDetailDialogState } from "../../redux/actions/allDialogs";
import { FetchWorkspaceInfo } from "../../redux/actions/workspace";
import { grid } from "../../components/CustomTable2/gridInstance";
import isEmpty from "lodash/isEmpty";
import Loader from "../../components/Loader/Loader";

function IssuesOverview({ classes, dispatchIssues, dispatchProjects, dispatchTasks }) {
  const [state, dispatch] = useReducer(reducer, initialState);
  const dispatchFn = useDispatch();
  const { profileState } = useSelector(state => {
    return {
      profileState: state.profile.data,
    };
  });
  const [isLoading, setIsLoading] = useState(true);
  const [IssueData, setIssueData] = useState([]);
  const handleIsLoading = value => {
    setIsLoading(value);
  };

  useEffect(() => {
    getWidgetSetting(
      "Issue",
      //success
      res => {
        dispatchWidgetSetting(dispatch, res);
      }
    );
    getWidget(
      "issue",
      //Success
      res => {
        handleIsLoading(false);
        dispatchIssueWidgetData(dispatch, res.data.entity);
        dispatchIssues(res.data.entity.issues);
        dispatchProjects(res.data.entity.projects);
        dispatchTasks(res.data.entity.tasks);
      },
      err => {
        handleIsLoading(false);
        dispatchIssueWidgetData(dispatch, { projects: [], issues: [], tasks: [], permissions: [] });
      }
    );
    // all columns api from actions
    getTeamColumn("issueoverview", "issue", res => {
      dispatchColumns(dispatch, res.data.entity);
    });
    return () => {
      dispatchIssues([]);
      dispatchProjects([]);
      dispatchTasks([]);
      if (!window.location.pathname.includes("/reports")) {
        FetchWorkspaceInfo("", () => { }, () => { }, dispatchFn);
      }
      grid.grid = null;
    }
  }, []);

  const {
    widgetSettings,
    issueWidgetData,
    filteredIssues,
    groupedIssues,
    selectedIssue,
    allColumns,
    customColumns,
  } = state;
  const getIssueData = () => {
    const { selectedFilter } = widgetSettings;
    const today = new Date();
    if (groupedIssues.length) {
      return groupedIssues;
    }
    switch (selectedFilter) {
      case "viewAllIssues":
        return issueWidgetData.issues;
        break;
      case "assignedToMe":
        return issueWidgetData.issues.filter(issue => issue.assignee.includes(profileState.userId));
        break;
      case "dueToday":
        return issueWidgetData.issues.filter(
          issue =>
            issue.dueDate && moment(issue.dueDate).isSame(today, "day") && issue.status !== "Closed"
        );
        break;
      case "dueInFiveDays":
        return issueWidgetData.issues.filter(
          issue =>
            issue.dueDate &&
            !moment(issue.dueDate).isSame(today, "day") &&
            moment(issue.dueDate).diff(today, "day") <= 4 &&
            moment(issue.dueDate).diff(today, "day") >= 0 &&
            issue.status !== "Closed"
        );
        break;
      case "unassignedIssues":
        return issueWidgetData.issues.filter(t => isEmpty(t.assignee));
         
        break;
      case "overDueIssues":
        return issueWidgetData.issues.filter(
          issue =>
            issue.dueDate &&
            moment(issue.dueDate).isBefore(today, "day") &&
            issue.status !== "Closed"
        );
        break;
      case "unscheduledIsses":
        return issueWidgetData.issues.filter(issue => !issue.dueDate && !issue.startDate);
        break;

      default:
        break;
    }

    return filteredIssues[selectedFilter];
  };
  const columnChangeCallback = updatedColumns => {
    dispatchColumns(dispatch, updatedColumns);
  };
  useEffect(() => {
    setIssueData(getIssueData());
  }, [issueWidgetData.issues, widgetSettings.selectedFilter, widgetSettings.filteredIssues]);

  //Close task detail
  const closeIssueDetailsPopUp = () => {
    issueDetailDialogState(dispatchFn, {
      id: "",
      issueWorkspacePer: null,
    });
    setTimeout(() => {
      getWidget(
        "issue",
        //Success
        res => {
          handleIsLoading(false);
          dispatchIssueWidgetData(dispatch, res.data.entity);
          dispatchIssues(res.data.entity.issues);
          dispatchProjects(res.data.entity.projects);
          dispatchTasks(res.data.entity.tasks);
        },
        err => {
          handleIsLoading(false);
          dispatchIssueWidgetData(dispatch, { projects: [], issues: [], tasks: [], permissions: [] });
        }
      );
    }, 1000)
  };

  //Open task details on tasks row click
  const handleRowClick = row => {
    const isRowClicked = row.event.target.closest("*[data-rowClick='cell']");
    if (isRowClicked) return;
    if (row.data && row.data.id && row.data.uniqueId !== "-") {
      let workspaceIssuePermission =
        row.data &&
        issueWidgetData.permissions.find(
          p => p.workSpaceId === row.data.workSpaceId
        ); /** workspace issue permission */
      issueDetailDialogState(dispatchFn, {
        id: row.data.id,
        afterCloseCallBack: () => {
          closeIssueDetailsPopUp();
        },
        issueWorkspacePer: workspaceIssuePermission.issue,
      });
    }
  };
  const isQuickFilterApplied = widgetSettings.selectedFilter !== "viewAllTasks";

  return (
    <>
      <IssueOverviewContext.Provider value={{ dispatch, state }}>
        <div className={classes.tasksOverview}>
          {allColumns && allColumns.length ? (
            <IssuesOverviewHeader
              feature={"issueoverview"}
              groupType={"issue"}
              handleIsLoading={handleIsLoading}
              isQuickFilterApplied={isQuickFilterApplied}
              quickFilteredData={IssueData}
            />
          ) : null}
          {allColumns && allColumns.length && !isLoading ? (
            <>
              <RepostingList
                feature={"issueoverview"}
                groupType={"issue"}
                exportConfig={{
                  accessorKey: "issueIds",
                  accessorId: "id",
                  apiKey: "export/bulk/issueoverview",
                  documentName: "issues.xlsx"
                }}
                columnChangeCallback={columnChangeCallback}
                columns={allColumns}
                customColumns={customColumns}
                data={IssueData}
                emptyHeading={"No Issue found"}
                handleRowClick={handleRowClick}
                emptyText={
                  widgetSettings.selectedWorkspaces.length
                    ? "You do not have any issue yet."
                    : "Please select a workspace."
                }
              />
            </>
          ) : (
            
          <Loader />
          )}
          {/* <IssueList isLoading={isLoading} handleIsLoading={handleIsLoading} /> */}
        </div>
      </IssueOverviewContext.Provider>
    </>
  );
}

export default compose(
  withStyles(issuesOverview, { withTheme: true }),
  connect(null, {
    dispatchIssues,
    dispatchProjects,
    dispatchTasks,
    getTeamColumn,
  })
)(IssuesOverview);
