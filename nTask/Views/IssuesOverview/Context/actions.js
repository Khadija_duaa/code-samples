import {
  SET_WIDGET_SETTINGS,
  SET_GROUPBY_ASSIGNEE,
  SET_WIDGET_ISSUE_DATA,
  SET_SELECTED_ISSUE,
  SET_GROUPBY_TASK,
  SET_ALL_COLUMNS,
  UPDATE_TASK_DATA
} from "./constants";

export const dispatchWidgetSetting = (dispatch, obj) => {
  dispatch({ type: SET_WIDGET_SETTINGS, payload: obj });
};
export const dispatchIssueWidgetData = (dispatch, obj) => {
  dispatch({ type: SET_WIDGET_ISSUE_DATA, payload: obj });
};
export const dispatchIssueGroupByAssignee = (dispatch, data) => {
  dispatch({ type: SET_GROUPBY_ASSIGNEE, payload: data });
};
export const dispatchIssueGroupByTask = (dispatch, data) => {
  dispatch({ type: SET_GROUPBY_TASK, payload: data });
};
export const dispatchIssue = (dispatch, data) => {
  dispatch({ type: SET_SELECTED_ISSUE, payload: data });
};
export const dispatchColumns = (dispatch, data) => {
  dispatch({ type: SET_ALL_COLUMNS, payload: data });
};
export const dispatchUpdateTask = (dispatch, data) => {
  dispatch({ type: UPDATE_TASK_DATA, payload: data });
};
