const initialState = {
  widgetSettings: {
    selectedFilter: "viewAllIssues",
    groupBy: [],
    selectedWorkspaces: [],
    subTasks: false,
    selectedColumns: ["issueId", "title"],
  },
  issueWidgetData: { issues: [], tasks: [], projects: [], permissions:[] },
  filteredIssues: {
    assignedToMe: [],
    dueToday: [],
    dueInFiveDays: [],
    overDueIssues: [],
    unscheduledIsses: [],
  },
  groupedIssues: [],
  selectedIssue:null,
  allColumns: [],
  customColumns: []
};

export default initialState;
