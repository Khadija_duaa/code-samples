import { createContext } from "react";

const IssueOverviewContext = createContext({});

export default IssueOverviewContext;
