import moment from "moment";
import {
  SET_WIDGET_SETTINGS,
  SET_WIDGET_ISSUE_DATA,
  SET_GROUPBY_ASSIGNEE,
  SET_SELECTED_ISSUE,
  SET_GROUPBY_TASK,
  SET_ALL_COLUMNS,
  UPDATE_TASK_DATA
} from "./constants";
import { store } from "../../../index";
import cloneDeep from "lodash/cloneDeep";

function reducer(state, action) {
  switch (action.type) {
    case SET_WIDGET_SETTINGS: {
      const updatedState = { ...state.widgetSettings, ...action.payload };

      return { ...state, widgetSettings: updatedState };
    }
    break;
    case SET_GROUPBY_ASSIGNEE: {
      const storeState = store.getState();
      const newIssueList = [];
      action.payload.forEach(t => {
        if (t.assignee && t.assignee.length) {
          t.assignee.forEach(a => {
            const assignee = storeState.profile.data.teamMember.find(m => m.userId === a);
            if (assignee) {
              // If assignee exist in  team
              // this check is useful user is assigned an issue but the user does not exist in team any more
              const issue = { ...t, assignee: assignee.fullName };
              newIssueList.push(issue);
            } else {
              const issue = { ...t, assignee: "Others" };
              newIssueList.push(issue);
            }
          });
        } else {
          const issue = { ...t, assignee: "Others" };
          newIssueList.push(issue);
        }
      });
      return { ...state, groupedIssues: newIssueList };
    }
    break;

    case SET_GROUPBY_TASK: {
      const newIssueList = [];
      action.payload.forEach(t => {
        if (t.linkedTasks && t.linkedTasks.length) {
          t.linkedTasks.forEach(a => {
            const task = state.issueWidgetData.tasks.find(m => m.taskId === a);
            const issue = { ...t, task: task.taskTitle };
            newIssueList.push(issue);
          });
        } else {
          const issue = { ...t, task: "Unassigned" };
          newIssueList.push(issue);
        }
      });
      return { ...state, groupedIssues: newIssueList };
    }
    break;

    case SET_WIDGET_ISSUE_DATA: {
      const storeState = store.getState();
      const filteredIssues = {
        assignedToMe: [],
        dueToday: [],
        dueInFiveDays: [],
        overDueIssues: [],
        unscheduledIsses: [],
      };
      const today = new Date();

      action.payload.issues.forEach(t => {
        if (t.assignee && t.assignee.includes(storeState.profile.data.userId)) {
          filteredIssues.assignedToMe.push(t);
        }

        if (t.dueDate && moment(t.dueDate).isSame(today, "day") && t.status !== "Closed") {
          filteredIssues.dueToday.push(t);
        }
        if (
          t.dueDate &&
          moment(t.dueDate).diff(today, "day") <= 4 &&
          moment(t.dueDate).diff(today, "day") >= 0 &&
          t.status !== "Closed"
        ) {
          filteredIssues.dueInFiveDays.push(t);
        }
        if (t.dueDate && moment(t.dueDate).isBefore(today, "day") && t.status !== "Closed") {
          filteredIssues.overDueIssues.push(t);
        }
        if (!t.dueDate && !t.startDate) {
          filteredIssues.unscheduledIsses.push(t);
        }
      });
      return { ...state, issueWidgetData: action.payload };
    }
    break;

    case SET_SELECTED_ISSUE: {
      return { ...state, selectedIssue: action.payload };
    }
    break;

    case SET_ALL_COLUMNS: {
      const customColumns = action.payload.filter(
        column =>
          !column.isSystem &&
          column.fieldType !== "textarea" &&
          column.fieldType !== "section"
      );
      const allColumnUpdated = action.payload.filter(
        column => column.fieldType !== "textarea" && column.fieldType !== "section"
      );
      window.issueoverviewColumns = allColumnUpdated
      return { ...state, allColumns: allColumnUpdated, customColumns };
    }
    break;

    case UPDATE_TASK_DATA:{
      let stateData = cloneDeep(state);
      stateData.taskWidgetData.userTasks = stateData.taskWidgetData.userTasks.map(item => {
        if(item.taskId === action.payload.taskId){
          return {...item, ...action.payload};
        }
        else return item;
      });
      stateData.taskWidgetData.taskDetails = stateData.taskWidgetData.taskDetails.map(item => {
        if(item.taskId === action.payload.taskId){
          return {...item, ...action.payload};
        }
        else return item;
      });
      return {...stateData };
    }
    break;

    default:
      throw new Error();
  }
}

export default reducer;
