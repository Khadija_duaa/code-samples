const issuesOverviewStyles = theme => ({
    tasksOverview: {
     background: theme.palette.common.white,
   //   border: `1px solid ${theme.palette.border.lightBorder}`,
     borderRadius: 8,
     height: "100%",
     width: '100%',
     '& *': {
        fontFamily: "'lato', sans-serif",
     }
    },
  });
  
  export default issuesOverviewStyles;
  