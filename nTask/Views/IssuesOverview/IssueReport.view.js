import React, { useEffect, useMemo, useRef, useState } from "react";
import { withSnackbar } from "notistack";

// import CustomTable from "../../../components/CustomTable2/ReportingList.cmp";
import CustomTable from "../../components/CustomTable2/ReportingList.cmp";
import { useDispatch, useSelector } from "react-redux";
import isEqual from "lodash/isEqual";
import { AgGridColumn } from "@ag-grid-community/react";
import withStyles from "@material-ui/core/styles/withStyles";
import {
  getSavedFilters,
  updateTaskData,
} from "../../redux/actions/tasks";
import taskListStyles from "./taskReport.style";
import { FormattedMessage, injectIntl } from "react-intl";
import { compose } from "redux";
import isEmpty from "lodash/isEmpty";
// import taskColumnDefs from "./taskColumns";
import { taskDetailDialogState } from "../../redux/actions/allDialogs";
import ColumnSelector from "../../redux/selectors/columnSelector";
import isNull from "lodash/isNull";
import { headerProps } from "./constants";
import EmptyState from "../../components/EmptyStates/EmptyState";
// import TaskFilter from "./TaskFilter/taskFilter.view";
import { grid } from "../../components/CustomTable2/gridInstance";
import TemplatesSelector from "../../redux/selectors/templatesSelector";
// import { doesFilterPass } from "./TaskFilter/taskFilter.utils";

// const defaultColDef = { minWidth: 200, headerClass: "customHeader" };
const TaskList = React.memo(({ classes, theme, intl, enqueueSnackbar, style }) => {
  const state = useSelector(state => {
    return {
      taskColumns: ColumnSelector(state).task.columns,
      sections: ColumnSelector(state).task.sections,
      nonSectionFields: ColumnSelector(state).task.nonSectionFields,
      tasks: state.tasks.data,
      projects: state.projects.data,
      members: state.profile.data.member.allMembers,
      workspaceTemplates: state.workspaceTemplates.data,
      globalTimerTaskState: state.globalTimerTask,
      workspaceStatus: state.workspaceTemplates.data.defaultWSTemplate,
      profileState: state.profile.data,
      quickFilters: state.tasks.quickFilters,
      templates: TemplatesSelector(state),
    };
  });
  const sectionGroup = localStorage.getItem("sectiongrouping");

  const dispatch = useDispatch();
  const [selectedTasks, setSelectedTasks] = useState([]);
  const [mappingDialogState, setMappingDialogState] = useState({
    tempProject: null,
    openStatusDialog: false,
    newTemplateItem: {},
    oldStatusItem: {},
    tempTask: {},
  });
  const timerState = useRef(null);
  const statusTemplates = useRef(null);
  const [sectionGrouping, setSectionGrouping] = useState(sectionGroup === "true" ? true : false);
  const {
    taskColumns = [],
    tasks = [],
    projects,
    members,
    sections = [],
    nonSectionFields = [],
    globalTimerTaskState,
    workspaceTaskPer,
    workspaceStatus,
    profileState,
    quickFilters,
    templates,
  } = state;

  useEffect(() => {
    getSavedFilters("task", dispatch);
    return () => {
      grid.grid = null;
    };
  }, []);
  useEffect(() => {
    statusTemplates.current = templates;
  }, [templates]);
  const updateTask = (task, obj) => {
    updateTaskData(
      { task, obj },
      dispatch,
      //Success
      task => {
        if (grid.grid) {
          const rowNode = grid.grid && grid.grid.getRowNode(task.id);
          rowNode.setData(task);
        }
      },
      err => {
        if (err && err.data) showSnackBar(err.data.message, "error");
      }
    );
  };

  //Update Row if  Task Timer value is changed
  useEffect(() => {
    if (!isEmpty(grid.grid)) {
      const task = timerState.current
        ? timerState.current.task
        : globalTimerTaskState
          ? globalTimerTaskState.task
          : null;
      const rowNode = task && grid.grid.getRowNode(task.id);
      rowNode && rowNode.setData(task);
      timerState.current = globalTimerTaskState;
    }
  }, [globalTimerTaskState, grid.grid]);

  const isExternalFilterPresent = () => {
    return true;
  };

  //Clear selection if archived view is selected
  useEffect(() => {
    if (quickFilters && quickFilters.Archived) {
      grid.grid && grid.grid.deselectAll();
    }
  }, [quickFilters]);
  const handleTaskSelection = tasks => {
    setSelectedTasks(tasks);
  };

  //Close task detail
  const closeTaskDetailsPopUp = () => {
    taskDetailDialogState(dispatch, {
      id: "",
      afterCloseCallBack: null,
      type: "",
    });
  };
  //Open task details on tasks row click
  const handleTaskRowClick = row => {
    const isRowClicked = row.event.target.closest("*[data-rowClick='cell']");
    if (isRowClicked) return;
    if (row.data && row.data.taskId && row.data.uniqueId !== "-") {
      taskDetailDialogState(dispatch, {
        id: row.data.taskId,
        afterCloseCallBack: () => {
          closeTaskDetailsPopUp();
        },
        type: "comment",
      });
    }
  };
  //Snackbar function
  const showSnackBar = (snackBarMessage = "", type = null) => {
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "top",
          horizontal: "centerdp",
        },
        variant: type ? type : "info",
      }
    );
  };
  const taskTitleColumn = (taskColumns && taskColumns.find(c => c.columnKey == "taskTitle")) || {};
  let headProps = headerProps;
  const nonSectionsFieldskeys = nonSectionFields
    .filter(
      item =>
        item.fieldType == "textarea" ||
        item.fieldType == "formula" ||
        item.fieldType == "people" ||
        item.fieldType == "filesAndMedia" ||
        item.fieldType == "location" ||
        (item.fieldType == "dropdown" && item.settings.multiSelect)
    )
    .map(nsf => nsf.columnKey);
  const sectionsFieldskeys = sections.reduce((res, cv) => {
    let keys = cv.fields
      .filter(
        item =>
          item.fieldType == "textarea" ||
          item.fieldType == "formula" ||
          item.fieldType == "people" ||
          item.fieldType == "filesAndMedia" ||
          item.fieldType == "location" ||
          (item.fieldType == "dropdown" && item.settings.multiSelect)
      )
      .map(cv => cv.columnKey);
    res = [...res, ...keys];
    return res;
  }, []);
  headProps.columnGroupingDisabled = [
    ...headProps.columnGroupingDisabled,
    ...nonSectionsFieldskeys,
    ...sectionsFieldskeys,
  ];

  const handleChangeGrouping = value => {
    let g = grid.grid;
    let sectionGroup = localStorage.getItem("sectiongrouping");
    if (sectionGroup == "true") localStorage.setItem("sectiongrouping", value);
    else {
      localStorage.setItem("sectiongrouping", value);
    }
    setSectionGrouping(value);
  };
  return (
    <>
      {tasks.length == 0 ? (
        <div className={classes.emptyContainer} style={style}>
          <EmptyState
            screenType="task"
            heading={
              <FormattedMessage
                id="common.create-first.task.label"
                defaultMessage="Create your first task"
              />
            }
            message={
              <FormattedMessage
                id="common.create-first.task.messageb"
                defaultMessage='You do not have any tasks yet. Press "Alt + T" or click on button below.'
              />
            }
            button
          />
        </div>
      ) : (
        <div className={classes.taskListViewCnt} style={style}>
          <CustomTable
            columns={state.taskColumns}
            defaultColDef={{ lockPinned: true }}
            isExternalFilterPresent={isExternalFilterPresent}
            // doesExternalFilterPass={doesFilterPass}
            onSelectionChanged={handleTaskSelection}
            frameworkComponents={{
              // removed all custom renderer functions
            }}
            data={tasks}
            selectedTasks={selectedTasks}
            type="task"
            onRowGroupChange={(obj, key) => updateTask(obj, { [key]: obj[key] })}
            // createableProps={{
            //   placeholder: "Enter title for new task",
            //   id: "quickAddTask",
            //   btnText: "Add new task",
            //   addAction: handleAddTask,
            // }}
            headerProps={headProps}
            gridProps={{
              groupDisplayType: "groupRows",
              onRowClicked: handleTaskRowClick,
              reactUi: true,
              // groupIncludeFooter: true,
              // groupIncludeTotalFooter: true,
              maintainColumnOrder: true,
              // autoGroupColumnDef: { minWidth: 300 },
            }}>
            <AgGridColumn
              headerName=""
              field="colorCode"
              cellRenderer="color"
              filter={false}
              cellClass={classes.taskColorCell}
              maxWidth={6}
              resizeable={false}
              pinned="left"
              lockPosition={true}
              suppressMovable={true}
            />
            <AgGridColumn
              headerName={taskTitleColumn.columnName}
              field={taskTitleColumn.columnKey}
              resizable={true}
              suppressMovable={true}
              rowDrag={false}
              sortable={true}
              filter={true}
              lockPosition={true}
              wrapText={taskTitleColumn.wrapText}
              cellRenderer={"taskTitleCmp"}
              cellClass={`${classes.taskTitleCell} ag-textAlignLeft`}
              pinnedRowCellRenderer="customPinnedRowRenderer"
              pinned={"left"}
              autoHeight={taskTitleColumn.autoHeight}
              width={taskTitleColumn.width || 300}
              minWidth={200}
              align="left"
              sortIndex={taskTitleColumn.sortIndex == -1 ? null : taskTitleColumn.sortIndex}
              cellStyle={{ padding: 0 }}
              rowDrag={true}
            />
            <AgGridColumn
              headerName=""
              field="recurrence"
              suppressMovable={true}
              cellRenderer="recurrence"
              cellClass={classes.taskRecurrenceCell}
              filter={false}
              minWidth={22}
              maxWidth={22}
              pinned={"left"}
            />
            {/*</AgGridColumn>*/}

            {sectionGroup == "true" && (
              <AgGridColumn
                visible={false}
                headerName="System Fields"
                headerClass={classes.systemFieldGroup}>
                {taskColumns.map((c, i) => {
                  // const defaultColDef = taskColumnDefs(classes)[c.columnKey];
                  const {
                    hide,
                    pinned,
                    rowGroup,
                    rowGroupIndex,
                    sort,
                    sortIndex,
                    width,
                    wrapText,
                    autoHeight,
                  } = c;
                  return (
                    c.columnKey !== "taskTitle" &&
                    isNull(c.fieldType) &&
                    isNull(c.fieldId) && (
                      <AgGridColumn
                        key={c.id}
                        headerName={c.columnName}
                        field={c.columnKey}
                        hide={hide}
                        pinned={pinned}
                        rowGroup={rowGroup}
                        sort={sort}
                        sortIndex={sortIndex == -1 ? null : sortIndex}
                        wrapText={wrapText}
                        rowGroupIndex={rowGroup && parseFloat(rowGroupIndex)}
                        autoHeight={autoHeight}
                        width={width || (defaultColDef && defaultColDef.minWidth)}
                        cellStyle={{ justifyContent: "center" }}
                        {...defaultColDef}
                      />
                    )
                  );
                })}
              </AgGridColumn>
            )}
            {sectionGroup == "true" &&
              sections.map(section => {
                return (
                  section.fields.length > 0 && (
                    <AgGridColumn
                      headerName={section.columnName}
                      resizeable={false}
                      headerGroupComponent="customHeaderGroupComponent"
                      color={section.color}
                      headerClass={classes.sectionFieldsGroup}>
                      {section.fields.map((c, i) => {
                        // const defaultColDef = taskColumnDefs(classes)[c.fieldType];
                        const {
                          hide,
                          pinned,
                          rowGroup,
                          sort,
                          sortIndex,
                          width,
                          wrapText,
                          autoHeight,
                        } = c;
                        return (
                          <AgGridColumn
                            key={c.id}
                            headerName={c.columnName}
                            field={c.columnKey}
                            suppressKeyboardEvent={true}
                            hide={hide}
                            fieldType={c.fieldType}
                            pinned={pinned}
                            rowGroup={rowGroup}
                            fieldType={c.fieldType}
                            sort={sort}
                            sortIndex={sortIndex == -1 ? null : sortIndex}
                            wrapText={wrapText}
                            autoHeight={autoHeight}
                            width={width || (defaultColDef && defaultColDef.minWidth)}
                            fieldId={c.fieldId}
                            cellStyle={{
                              justifyContent:
                                c.fieldType === "dropdown" || c.fieldType === "filesAndMedia"
                                  ? "flex-start"
                                  : "center",
                            }}
                            {...defaultColDef}
                          />
                        );
                      })}
                    </AgGridColumn>
                  )
                );
              })}
            {sectionGroup == "true" && nonSectionFields.length ? (
              <AgGridColumn resizeable={false} headerClass={classes.taskTitleField}>
                {nonSectionFields.map((c, i) => {
                  // const defaultColDef = taskColumnDefs(classes)[c.fieldType];
                  const {
                    hide,
                    pinned,
                    rowGroup,
                    sort,
                    sortIndex,
                    width,
                    wrapText,
                    autoHeight,
                  } = c;
                  return (
                    <AgGridColumn
                      key={c.id}
                      headerName={c.columnName}
                      field={c.columnKey}
                      fieldType={c.fieldType}
                      hide={hide}
                      suppressKeyboardEvent={true}
                      pinned={pinned}
                      fieldType={c.fieldType}
                      rowGroup={rowGroup}
                      sort={sort}
                      sortIndex={sortIndex == -1 ? null : sortIndex}
                      wrapText={wrapText}
                      autoHeight={autoHeight}
                      fieldId={c.fieldId}
                      width={width || (defaultColDef && defaultColDef.minWidth)}
                      cellStyle={{
                        justifyContent:
                          c.fieldType === "dropdown" || c.fieldType === "filesAndMedia"
                            ? "flex-start"
                            : "center",
                      }}
                      {...defaultColDef}
                    />
                  );
                })}
              </AgGridColumn>
            ) : null}

            <AgGridColumn
              headerName={""}
              field={"columnDropdown"}
              resizable={false}
              sortable={false}
              lockPinned={true}
              filter={false}
              width={40}
              headerClass={"columnSelectHeader"}
              pinned={"right"}
              suppressMovable={true}
              cellRenderer={"taskActionCellRenderer"}
              cellStyle={{ padding: 0, textAlign: "center" }}
            />
          </CustomTable>
          {/* <TaskFilter
            sectionGrouping={sectionGrouping}
            handleChangeGrouping={handleChangeGrouping}
          /> */}
        </div>
      )}

      {/*<Footer*/}
      {/*  total={tasks && tasks.length}*/}
      {/*  display={true}*/}
      {/*  type="Task"*/}
      {/*  // quote={this.props.quote}*/}
      {/*/>*/}
    </>
  );
}, areEqual);

function areEqual(prevProps, nextProps) {
  return isEqual(prevProps, nextProps);
  /*
  return true if passing nextProps to render would return
  the same result as passing prevProps to render,
  otherwise return false
  */
}


export default compose(
  injectIntl,
  withSnackbar,
  withStyles(taskListStyles, { withTheme: true })
)(TaskList);
