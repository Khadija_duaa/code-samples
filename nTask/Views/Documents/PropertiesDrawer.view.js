import React, { useContext, useEffect, useState } from "react";
import { compose } from "redux";
import { useSelector } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./documents.style";
import { FileIcon, defaultStyles } from "react-file-icon";

import DocumentsContext from "./Context/documents.context";
import CustomDrawer from "../../components/Drawer/CustomDrawer";
import CloseIcon from "@material-ui/icons/Close";
import { updateDocumentsState } from "./Context/actions";
import imgSrc from "./img.jpg";
import DefaultTextField from "../../components/Form/TextField";
import helper from "../../helper/index";
import moment from "moment";
import CustomButton from "../../components/Buttons/CustomButton";
import Visibility from "@material-ui/icons/Visibility";
import SvgIcon from "@material-ui/core/SvgIcon";
import DownloadIcon from "../../components/Icons/DownloadIcon";
import IconFolder from "../../components/Icons/IconFolder";
import { CopyToClipboard } from "react-copy-to-clipboard";
import { getMemberById } from "../../helper/getMembersById";
import {
  updateFileDescription,
  updateFolderDescription,
  downloadFile,
  previewFile,
  getFileDescription,
  getFolderDescription,
  getThumbnail,
} from "../../redux/actions/sidebarPannel";

function PropertiesDrawer(params) {
  const { state, dispatch } = useContext(DocumentsContext);
  const {
    drawer: { open, data },
  } = state;
  const [fileDescription, setFileDescription] = useState("");
  const [downloadBtnQuery, setDownloadBtnQuery] = useState("");
  const [previewBtnQuery, setPreviewBtnQuery] = useState("");
  const [thumbnailUrl, setThumbnailUrl] = useState(null);

  const {
    name,
    size,
    filterPath,
    isDefault,
    dateCreated,
    updatedBy,
    createdBy,
    dateModified,
    isFile,
    description,
    fileId,
    type,
    path,
    groupType,
    folderId,
    thumbnail
  } = data;

  const {} = useSelector(state => {
    return {};
  });

  const { theme, classes } = params;

  const handleCloseDrawer = () => {
    updateDocumentsState(dispatch, { drawer: { data: null, open: false } });
  };
  const handleDescription = e => {
    setFileDescription(e.target.value);
  };
  const handleBlurDescription = e => {
    if (description !== fileDescription) {
      let obj = {
        description: fileDescription,
      };
      if (isFile) {
        obj.fileId = fileId;
        updateFileDescription(
          DOCUMENTS_BASE_URL,
          obj,
          succ => {},
          fail => {}
        );
      } else {
        obj.folderId = folderId;
        obj.groupType = groupType;
        updateFolderDescription(
          DOCUMENTS_BASE_URL,
          obj,
          succ => {},
          fail => {}
        );
      }
    }
  };

  const download = (link = "", filename = "") => {
    // Create a new link
    const anchor = document.createElement("a");
    anchor.href = link;
    anchor.download = filename;
    anchor.target = "_blank";

    // Append to the DOM
    document.body.appendChild(anchor);

    // Trigger `click` event
    anchor.click();

    // Remove element from DOM
    document.body.removeChild(anchor);
  };

  const handleDownloadFile = e => {
    setDownloadBtnQuery("progress");
    let obj = {
      path,
    };
    downloadFile(
      DOCUMENTS_BASE_URL,
      obj,
      succ => {
        console.log(succ);
        download(succ?.data?.data?.url, name);
        setDownloadBtnQuery("");
      },
      fail => {
        console.log(fail);
        setDownloadBtnQuery("");
      }
    );
  };
  const handlePreviewFile = e => {
    setPreviewBtnQuery("progress");
    let obj = {
      path,
    };
    previewFile(
      DOCUMENTS_BASE_URL,
      obj,
      succ => {
        console.log(succ);
        setPreviewBtnQuery("");
      },
      fail => {
        console.log(fail);
        setPreviewBtnQuery("");
      }
    );
  };

  const handleFileDescription = () => {
    getFileDescription(
      DOCUMENTS_BASE_URL,
      fileId,
      data => {
        setFileDescription(data.description || "");
      },
      fail => {}
    );
  };
  const handleFolderDescription = () => {
    getFolderDescription(
      DOCUMENTS_BASE_URL,
      folderId,
      data => {
        setFileDescription(data.description || "");
      },
      fail => {}
    );
  };
  const handleFileThumbnail = () => {
    getThumbnail(
      DOCUMENTS_BASE_URL,
      path,
      url => {
        setThumbnailUrl(url);
      },
      fail => {}
    );
  };

  useEffect(() => {
    if (isFile) {
      handleFileDescription();
      thumbnail && handleFileThumbnail();
    } else if (!isDefault && !isFile) {
      handleFolderDescription();
    }
  }, [data]);

  return data ? (
    <CustomDrawer
      open={open}
      closeDrawer={handleCloseDrawer}
      hideHeader={true}
      drawerWidth={400}
      hideCloseIcon={true}
      drawerProps={{
        PaperProps: {
          height: "100%",
        },
      }}>
      <div className={classes.header}>
        <span className={classes.drawerTitle}>Properties</span>
        <CloseIcon
          htmlColor={theme.palette.secondary.medDark}
          style={{ fontSize: "18px" }}
          onClick={handleCloseDrawer}
        />
      </div>
      <div className={classes.drawerContent}>
        <div>
          {thumbnailUrl ? (
            <img src={thumbnailUrl} className={classes.imgCnt} />
          ) : (
            <div className={classes.iconCnt}>
              {!isFile ? (
                <SvgIcon viewBox="0 0 18 15.246">
                  <IconFolder />
                </SvgIcon>
              ) : (
                <div className={classes.defaultIcon}>
                  <FileIcon extension={type} color="#bcbc" {...defaultStyles[type]} />
                </div>
              )}
            </div>
          )}

          <div className={classes.detailsCnt}>
            <div className={classes.label}>
              <span>Name</span>
            </div>
            <div className={classes.info}>
              <span>{name}</span>
            </div>
          </div>
          <div className={classes.detailsCnt}>
            <div className={classes.label}>
              <span>Size</span>
            </div>
            <div className={classes.info}>
              <span>{helper.formatBytes(size, 2, true)}</span>
            </div>
          </div>
          <div className={classes.detailsCnt}>
            <div className={classes.label}>
              <span>Location</span>
            </div>
            <CopyToClipboard text={filterPath}>
              <div className={classes.info}>
                <span>
                  {filterPath} <br />
                  <span className={classes.setAction}>Copy</span>
                </span>
              </div>
            </CopyToClipboard>
          </div>
          <div className={classes.detailsCnt}>
            <div className={classes.label}>
              <span>Modified</span>
            </div>
            <div className={classes.info}>
              <span>
                {getMemberById(updatedBy).fullName} <br />
                {dateModified &&
                  `${moment(dateModified).format("ll")} • ${moment(dateModified).format("LT")}`}
              </span>
            </div>
          </div>
          <div className={classes.detailsCnt}>
            <div className={classes.label}>
              <span>Created</span>
            </div>
            <div className={classes.info}>
              <span>
                {getMemberById(createdBy).fullName} <br />
                {dateCreated &&
                  `${moment(dateCreated).format("ll")} • ${moment(dateCreated).format("LT")}`}
              </span>
            </div>
          </div>
          {/* <div className={classes.detailsCnt}>
            <div className={classes.label}>
              <span>Automation</span>
            </div>
            <div className={classes.info}>
              <span className={classes.setAction}>Set Action</span>
            </div>
          </div> */}
          {!isDefault && (
            <DefaultTextField
              label={""}
              fullWidth={true}
              errorState={false}
              formControlStyles={{ marginTop: 15, marginBottom: 0 }}
              defaultProps={{
                onChange: handleDescription,
                onBlur: handleBlurDescription,
                value: fileDescription,
                multiline: true,
                rows: 5,
                autoFocus: false,
                placeholder: "Add description",
                inputProps: { maxLength: 1500 },
              }}
            />
          )}
        </div>

        {!isDefault && isFile && (
          <div className={classes.btnContainer}>
            <CustomButton
              onClick={handlePreviewFile}
              className={classes.btnStyles}
              btnType={"white"}
              variant="contained"
              query={previewBtnQuery}
              disabled={previewBtnQuery === "progress"}>
              <Visibility className={classes.icon} />
              Preview
            </CustomButton>
            <CustomButton
              onClick={handleDownloadFile}
              className={classes.btnStyles}
              btnType={"green"}
              variant="contained"
              query={downloadBtnQuery}
              disabled={downloadBtnQuery === "progress"}>
              <SvgIcon
                viewBox="0 0 16 14"
                htmlColor={theme.palette.secondary.medDark}
                className={classes.downloadIcon}>
                <DownloadIcon />
              </SvgIcon>
              Download
            </CustomButton>
          </div>
        )}
      </div>
    </CustomDrawer>
  ) : null;
}
PropertiesDrawer.defaultProps = {
  theme: {},
  classes: {},
};

export default compose(withStyles(styles, { withTheme: true }))(PropertiesDrawer);
