import React from "react";
import { compose } from "redux";
import { useSelector } from "react-redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import UnPlanned from '../billing/UnPlanned/UnPlanned';
import { FormattedMessage, injectIntl } from "react-intl";
import documentStyle from "./documents.style";
import DocumentManager from "../../assets/images/unplanned/documentManager.png";
import { isPlanNonPaid, TRIALPERIOD } from '../../components/constants/planConstant';
import Constants, { isAnyBusinessPlan, isBusinessPlan, isBusinessTrial, isEnterprisePlan } from "../../components/constants/planConstant";
import Documnets from "./Documnets.view";


function Documents(props) {
    const { classes } = props;
    const {
        company: { subscriptionDetails },
        workspaces,
    } = useSelector(state => {
        return {
            company: state.profile.data.teams.find(
                item => item.companyId == state.profile.data.activeTeam
            ),
            workspaces: state.profile.data.workspace
        };
    });
    const { paymentPlanTitle } = subscriptionDetails;

    return (
        <>
            {(isPlanNonPaid(paymentPlanTitle)) ?
                <div className={classes.unplannedMain}>
                    <div className={classes.unplannedCnt}>
                        <UnPlanned
                            feature="premium"
                            titleTxt={
                                <FormattedMessage
                                    id="common.discovered-dialog.premium-title"
                                    defaultMessage="Wow! You've discovered a Premium feature!"
                                />
                            }
                            boldText={
                                "Documents"
                                //     intl.formatMessage({
                                //   id: "project.label",
                                // defaultMessage: "Documents",
                                //   })

                            }
                            descriptionTxt={
                                <FormattedMessage
                                    id="common.discovered-dialog.list.project.label"
                                    defaultMessage={
                                        "are available on our Premium Plan. Upgrade now or start a {TRIALPERIOD}-day free trial to find out all nTask Premium features."
                                    }
                                    values={{ TRIALPERIOD: TRIALPERIOD }}
                                />
                            }
                            showBodyImg={true}
                            showDescription={true}
                            imgUrl={DocumentManager}
                        />
                    </div>
                </div> :
                <Documnets workspaces={workspaces.length} />
            }
        </>
    );
}

export default compose(withRouter, withStyles(documentStyle, { withTheme: true }))(Documents);
