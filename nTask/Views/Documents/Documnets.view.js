import React, { useReducer, useRef, useEffect, useState } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import DocumentContext from "./Context/documents.context";
import initialState from "./Context/initialState";
import reducer from "./Context/reducer";
import { updateDocumentsState } from "./Context/actions";
import PropertiesDrawer from "./PropertiesDrawer.view";
import {
  FileManagerComponent,
  Inject,
  NavigationPane,
  DetailsView,
  Toolbar,
} from "@syncfusion/ej2-react-filemanager";
import documentStyle from "./documents.style";
import Typography from "@material-ui/core/Typography";
import { Circle } from "rc-progress";
import helper from "../../helper/index";
import { getStorageLimit, getThumbnail, downloadFile } from "../../redux/actions/sidebarPannel";
import axios from "axios";
import { calculateContentHeight } from "../../utils/common";
import { shallowEqual, useSelector } from "react-redux";
import { registerLicense } from '@syncfusion/ej2-base';
import imgSrc from "./img.jpg";
// import { DropDownButton } from '@syncfusion/ej2-splitbuttons';

const GBValue = 1000000000;

function Documents(params) {
  const { classes, workspaces } = params;
  const [state, dispatch] = useReducer(reducer, initialState);
  const [workspaceCount, setWorkspaceCount] = useState(workspaces);
  const [storageLimit, setStorageLimit] = useState({
    totalStorage: 0,
    usedStorage: 0,
    percentageUsed: 0,
  });
  const {
    drawer: { open, data },
  } = state;
  // const hostUrl = "https://fm.naxxa.io/";
  // const hostUrl = DOCUMENTS_BASE_URL; 
  registerLicense('Mgo+DSMBaFt/QHRqVVhlXlpAaV5dX2NLfUN0T2dedVx0ZCQ7a15RRnVfQV1hSX1Sf0ViXnxfdw==;Mgo+DSMBPh8sVXJ0S0J+XE9BdFRGQmJWfFN0RnNbdVp3fldCcC0sT3RfQF5jSn5RdkZiUH9fd3BRQQ==;ORg4AjUWIQA/Gnt2VVhkQlFac11JWXxIYVF2R2BJfVR0c19EZEwgOX1dQl9gSX1TdEVgWHZecXNQQGc=;MTIzNjU0NkAzMjMwMmUzNDJlMzBPV0FYWUFMd0N2NDE0SVI3cWd5UHV0UjB5MHNWSE1BQllQSzlOdkJMdnVvPQ==;MTIzNjU0N0AzMjMwMmUzNDJlMzBIdkhUaFNneEVmTGtLZHNOTnBFSHRDTVN1WHZKNlZ6aE9RazN3TzhrRnZFPQ==;NRAiBiAaIQQuGjN/V0Z+WE9EaFpAVmdWf0x0RWFab1x6cFJMYlhBNQtUQF1hSn5QdkdjWn5XcHVdR2Fb;MTIzNjU0OUAzMjMwMmUzNDJlMzBIZ1k3dlJpRTJ0dzJRSzVhbk9Zd1VKamFWZVBqeXFaRWtPbHZjTS94WWZZPQ==;MTIzNjU1MEAzMjMwMmUzNDJlMzBGeCtQM2hBZWtsb2tBZlVCN0R3OERxWm9rRm5SNCtnK1FNb25xOXpmanBVPQ==;Mgo+DSMBMAY9C3t2VVhkQlFac11JWXxIYVF2R2BJfVR0c19EZEwgOX1dQl9gSX1TdEVgWHZecXxdQWc=;MTIzNjU1MkAzMjMwMmUzNDJlMzBLeE1VSk1CUUdDL2FDS20va21POWtaNFQrNlVMSkc2aGxPMlVNTHk0OFIwPQ==;MTIzNjU1M0AzMjMwMmUzNDJlMzBOcjR5YWpHN0lhQURPaExhUTNPZFZpM3VrTEphUXpWb29Ga3Y1a0l6RWZFPQ==;MTIzNjU1NEAzMjMwMmUzNDJlMzBIZ1k3dlJpRTJ0dzJRSzVhbk9Zd1VKamFWZVBqeXFaRWtPbHZjTS94WWZZPQ==')
  const fileObj = useRef(null);
  // refresh on files on add new workspace
  useEffect(() => {
    if (workspaces != workspaceCount) {
      fileObj.current.refreshFiles();
      setWorkspaceCount(workspaces)
    }
  }, [workspaces]);

  const handleFileSelection = args => {
    if (args.action == 'select') {
      fileObj.current.enableToolbarItems(["Properties"]);
    } else {
      fileObj.current.disableToolbarItems(["Properties"]);
    }
    if (open) {
      updateDocumentsState(dispatch, { drawer: { data: args.fileDetails, open: true } });
      console.log(args.fileDetails.name + " has been " + args.action + "ed");
    }

  };

  const handleMenuClick = args => {
    if (args.item.text === "Properties") {
      handleOpenDrawer(args.fileDetails[0]);
    }
  };
  const handleMenuOpen = args => {
    const { items } = args;
    for (const i in items) {
      if (items[i].id === fileObj.current.element.id + "_cm_properties") {
        items[i].iconCss = "e-icons e-fe-details";
      }
    }
  };

  const handleOpenDrawer = data => {
    updateDocumentsState(dispatch, { drawer: { data, open: true } });
  };
  const handleToolbarCreate = args => {
    for (var i = 0; i < args.items.length; i++) {
      if (args.items[i].text === "Properties") {
        args.items[i].align = "Right";
        args.items[i].overflow = "Show";
        args.items[i].prefixIcon = "e-icons e-fe-details";
        args.items[i].showTextOn = "Overflow";
        args.items[i].tooltipText = "Properties";
      }
    }
  };
  const handleToolbarClick = args => {
    if (args.item.text === "Properties" && args.fileDetails.length) {
      handleOpenDrawer(args.fileDetails[0]);
    }
  };

  const handleBeforeSend = args => {
    const token = helper.getToken();
    args.ajaxSettings.beforeSend = function (args) {
      args.httpRequest.setRequestHeader("Authorization", token);
    };
  };
  const disableMenuItems = () => { };
  const handleSuccess = params => {
    if (params.action == "Upload") {
      getStorage();
    }
  };
  const getStorage = () => {
    getStorageLimit(
      DOCUMENTS_BASE_URL,
      succ => {
        setStorageLimit(calculatePercentage(succ.data.totalStorage, succ.data.usedStorage));
        // let test = calculatePercentage(succ.data.totalStorage, succ.data.usedStorage);
        // console.log(test)
      },
      fail => {
        setStorageLimit({
          totalStorage: 0,
          usedStorage: 0,
        });
      }
    );
  };

  useEffect(() => {
    getStorage();
  }, []);

  useEffect(() => {
    if (fileObj) {
      fileObj.current.disableToolbarItems(["Properties"]);
    }
  }, [fileObj]);


  const calculatePercentage = (total, used) => {
    let percentageUsed = (used / total) * 100;
    percentageUsed = percentageUsed.toFixed(1);
    return {
      percentageUsed,
      usedStorage: (used / GBValue).toFixed(2),
      totalStorage: (total / GBValue).toFixed(2)
    }
  }
  // const usedStorage = (storageLimit.usedStorage / GBValue).toFixed(2);
  // const totalStorage = (storageLimit.totalStorage / GBValue).toFixed(2);
  // const storagePercent = Math.round((parseFloat(usedStorage) / parseFloat(totalStorage)) * 100) && '0';
  const storageType = "GB";

  const handleImageLoad = async (params,) => {
    // params.imageUrl = 'https://cdn.pixabay.com/photo/2018/08/26/23/55/woman-3633737__480.jpg';
    // const {
    //   fileDetails: [item],
    // } = params;
    // const response = await axios.get(`${DOCUMENTS_BASE_URL}api/file/${item.thumbnail}`, {
    //   headers: {
    //     accept: "application/json",
    //     Authorization: helper.getToken(),
    //   },
    // });
    // if (response.status == 200) {
    //   params.imageUrl = 'https://cdn.pixabay.com/photo/2018/08/26/23/55/woman-3633737__480.jpg';
    // }
    const {
      fileDetails: [item],
    } = params;
    const response = await axios.get(`https://fm.naxxa.io/api/file/${item.thumbnail}`, {
      headers: {
        accept: "application/json",
        Authorization: helper.getToken(),
      },
    });
    if (response.status == 200) {
      params.imageUrl = response.data;
    }
  };

  const handleDownload = (params) => {
    params.cancel = true;
    let obj = {
      path: params.data.data[0].path,
    };
    downloadFile(
      DOCUMENTS_BASE_URL,
      obj,
      succ => {
        // download("GFG.txt", 'welcome to geeks for geeks');
        download(succ?.data?.data?.url, params.data.names[0]);
      },
      fail => {
        console.log(fail);
      }
    );
  };

  // Start file download.

  const download = (link = "", filename = "") => {
    // Create a new link
    // const downloadFileRef = useRef(null);
    // const [downloadFileUrl, setDownloadFileUrl] = useState;
    // const [downloadFileName, setDownloadFileName] = useState();
    // const onLinkClick = (filename) => {
    //   axios.get("/presigned-url")
    //     .then((response) => {
    //       setDownloadFileUrl(response.url); setDownloadFileName(filename);
    //       // downloadFileRef.current?.click();
    //     })
    //     .catch((err) => {
    //       console.log(err);
    //     });
    // };
    // return (
    //   <><a onClick={() => onLinkClick("document.pdf")} aria-label="Download link">
    //     Download</a>
    //     <a href={downloadFileUrl} download={downloadFileName} className="hidden" ref={downloadFileRef}
    //     />
    //   </>)
    // e.stopPropagation();


    // axios.get(link)
    //   .then((response) => {
    //     // setDownloadFileUrl(response.url);
    //     // setDownloadFileName(filename); 
    //     let a = document.createElement("a");
    //     a.href = response.url; 
    //     a.setAttribute("download", filename);
    //     a.click();
    //     document.body.removeChild(anchor);
    //     // downloadFileRef.current?.click();
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   });
    let a = document.createElement("a");
    // a.href = link;
    a.href = imgSrc;
    a.setAttribute("download", 'img.jpg');
    a.click();
    document.body.removeChild(anchor);


    // const anchor = document.createElement("a");
    // anchor.href = 'https://thumbs.dreamstime.com/b/spring-natur-spring-natur-184361891.jpg';
    // anchor.download = filename;
    // anchor.target = "_blank";

    // Append to the DOM
    // document.body.appendChild(anchor);

    // Trigger `click` event
    // anchor.click();

    // Remove element from DOM
    // document.body.removeChild(anchor);
  };

  return (
    <DocumentContext.Provider
      value={{
        dispatch,
        state,
      }}>
      <div className={classes.mainContainer} style={{ height: calculateContentHeight() + 120 }}>
        <span className={classes.title}>Documents</span>
        <div className={classes.controlSection}>
          <FileManagerComponent
            ref={
              s => {
                fileObj.current = s;
              }}
            id="overview_file"
            view="LargeIcons"
            ajaxSettings={{
              url: DOCUMENTS_BASE_URL + "api/FileManager/FileOperations",
              url: DOCUMENTS_BASE_URL + "api/filemanager/fileoperations/team",
              getImageUrl: DOCUMENTS_BASE_URL + "api/file/",
              uploadUrl: DOCUMENTS_BASE_URL + "api/file/upload",
              downloadUrl: DOCUMENTS_BASE_URL + "api/FileManager/Download",
            }}
            contextMenuSettings={{
              file: [
                // "Open",
                "|",
                "Cut",
                "Copy",
                "Paste",
                "|",
                "Rename",
                "Download",
                "Delete",
                "Properties",
              ],
              folder: [
                // "Open",
                "|",
                "Cut",
                "Copy",
                "Paste",
                "|",
                "Rename",
                "Delete",
                "Properties"
              ],
              layout: [
                "SortBy",
                "View",
                "Refresh",
                "|",
                "Paste",
                "|",
                "NewFolder",
                "Upload",
                "|",
                "Properties",
                "|",
                "SelectAll",
              ],
            }}
            toolbarSettings={{
              items: [
                "NewFolder",
                "Upload",
                "SortBy",
                "Refresh",
                "Cut",
                "Copy",
                "Delete",
                "View",
                "Properties",
              ],
            }}
            menuClick={handleMenuClick}
            menuOpen={handleMenuOpen}
            toolbarClick={handleToolbarClick}
            toolbarCreate={handleToolbarCreate}
            showThumbnail={false}
            fileSelect={handleFileSelection}
            beforeSend={handleBeforeSend}
            allowMultiSelection={false}
            disableMenuItems={disableMenuItems}
            success={handleSuccess}
            beforeImageLoad={handleImageLoad}
            beforeDownload={handleDownload}
            uploadSettings={{ maxFileSize: 2333322222, minFileSize: 120, autoUpload: true }}
          >
            <Inject services={[NavigationPane, DetailsView, Toolbar]} />
          </FileManagerComponent>
          <div className={classes.teamStorageCnt}>
            <div className={classes.progressCnt}>
              <div className={classes.progressBar}>
                <Circle
                  percent={storageLimit.percentageUsed}
                  strokeWidth="10"
                  trailWidth=""
                  trailColor="#dedede"
                  strokeColor={"#00CC90"}
                />
                <Typography variant="h6" align="center" className={classes.progressText}>
                  {`${storageLimit.percentageUsed}%`}
                </Typography>
              </div>
              <div className={classes.teamStorageInfo}>
                <span className={classes.storageTitle}>Team Storage</span>
                <span
                  className={
                    classes.storageLabel
                  }>{`${storageLimit.usedStorage} ${storageType} / ${storageLimit.totalStorage} ${storageType} used`}</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      {data && <PropertiesDrawer />}
    </DocumentContext.Provider>
  );
}


// total size / used size 

Documents.defaultProps = {
  classes: {},
  theme: {},
};

export default compose(withRouter, withStyles(documentStyle, { withTheme: true }))(Documents);
