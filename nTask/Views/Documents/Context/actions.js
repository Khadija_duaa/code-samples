import {
    UPDATE_DOCUMENTS_STATE
} from "./constants";

export const updateDocumentsState = (dispatch, obj) => {
    dispatch({ type: UPDATE_DOCUMENTS_STATE, payload: obj });
};