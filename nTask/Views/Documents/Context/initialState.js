const initialState = {
  drawer: {
    open: false,
    data: null,
  },
};

export default initialState;
