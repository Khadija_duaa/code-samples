import bgPattern from "../../assets/images/unplanned/dots_pattern.png"
const documentsStyles = theme => ({
  mainContainer: {
    margin: '0 12px',
    height: '100%',
  },
  title: {
    fontSize: "20px !important",
    fontWeight: theme.typography.fontWeightLarge,
    fontFamily: theme.typography.fontFamilyLato,
    color: theme.palette.common.black,
    width: "100%",
    display: "inline-block",
    margin: "10px 0px 15px 0px",
  },
  controlSection: {
    position: "relative",
    height: 'calc(100% - 72px)',
  },
  teamStorageCnt: {
    height: 60,
    position: "absolute",
    bottom: 0,
    left: 0,
  },
  progressCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "100%",
    padding: "0 10px",
  },
  progressBar: {
    width: 44,
    height: 44,
    position: "relative",
  },
  progressText: {
    position: "absolute",
    top: "50%",
    left: "50%",
    fontSize: "12px !important",
    transform: "translate(-50%, -50%)",
    fontWeight: theme.typography.fontWeightExtraLight,
    fontFamily: theme.typography.fontFamilyLato,
    color: theme.palette.common.black,
  },
  teamStorageInfo: {
    display: "flex",
    flexDirection: "column",
    marginLeft: 10,
    lineHeight: 1.5,
  },
  storageTitle: {
    fontSize: "12px !important",
    color: "#7E7E7E",
    fontWeight: theme.typography.fontWeightExtraLight,
    fontFamily: theme.typography.fontFamilyLato,
  },
  storageLabel: {
    fontSize: "12px !important",
    color: theme.palette.common.black,
    fontWeight: theme.typography.fontWeightMedium,
    fontFamily: theme.typography.fontFamilyLato,
  },
  header: {
    height: 36,
    borderBottom: "1px solid #EAEAEA",
    display: "flex",
    justifyContent: "space-between",
    padding: "0 15px",
    alignItems: "center",
    cursor: "pointer",
  },
  drawerTitle: {
    fontSize: "14px !important",
    color: theme.palette.common.black,
    fontWeight: theme.typography.fontWeightMedium,
    fontFamily: theme.typography.fontFamilyLato,
  },
  drawerContent: {
    padding: "10px 15px",
    height: "calc(100% - 90px)",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
  },
  imgCnt: {
    width: "100%",
    borderRadius: "8px",
  },
  detailsCnt: {
    display: "flex",
    margin: "10px 0",
    lineHeight: 1.5,
  },
  label: {
    width: 100,
    display: "flex",
    flexDirection: "column",
    "& span": {
      fontSize: "14px !important",
      color: "#646464",
      fontWeight: theme.typography.fontWeightExtraLight,
      fontFamily: theme.typography.fontFamilyLato,
      // marginBottom: 15
    },
  },
  info: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    "& span": {
      fontSize: "14px !important",
      color: theme.palette.common.black,
      fontWeight: theme.typography.fontWeightExtraLight,
      fontFamily: theme.typography.fontFamilyLato,
    },
  },
  setAction: {
    color: `${theme.palette.status.success} !important`,
    textDecoration: "underline",
    cursor: "pointer",
  },
  btnContainer: {
    height: 36,
    display: "flex",
    justifyContent: "space-between",
    marginBottom: 10,
  },
  icon: {
    fontSize: "20px !important",
    marginRight: 5,
  },
  downloadIcon: {
    fontSize: "16px !important",
    marginRight: 8,
    color: "white",
  },
  btnStyles: {
    padding: "5px 10px",
    borderRadius: "4px",
    display: "flex",
    width: "calc(50% - 5px)",
  },
  iconCnt: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    "& svg": {
      margin: "30px 0",
      fontSize: "80px !important",
    },
  },
  defaultIcon: {
    width: 80,
  },
  unplannedMain: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: '15px 0 23px 0',
    background: `url(${bgPattern})`,
    // height: 'calc(100vh - 130px)',
    height: '100vh',
    overflowY: "auto",
  },
  unplannedCnt: {
    width: 550,
  },
  "@global": {
    '.e-footer-content .e-btn.e-primary.e-flat:not([DISABLED])': {
      backgroundColor:theme.palette.background.green,
      borderColor:theme.palette.background.green,
      border: 'none',
      boxShadow: 'rgba(17, 12, 46, 0.15) 0px 48px 100px 0px',
      color: theme.palette.common.white,
    },
    '.e-filemanager .e-address .e-search-wrap': {
      display: 'none',
    },
    '.e-footer-content .e-btn:hover.e-primary.e-flat:not([DISABLED])':{
      backgroundColor:theme.palette.background.green,
    },
    '.e-spinner-pane .e-spinner-inner .e-spin-bootstrap5':{
      stroke: theme.palette.common.green,
    },

  },
});

export default documentsStyles;
