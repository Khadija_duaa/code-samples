import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import {
  UpdateMeeting,
  UpdateMeetingFollowupAction,
  InReviewMeeting,
  PublishMeeting,
  UnPublishMeeting,
  UpdateAgenda,
  DeleteDiscussionPoint,
  RemoveMeetingDecision,
  DeleteMeetingAgenda,
  UpdateMeetingFollowUpAction,
  DeleteMeetingFollowUpAction,
  UpdateMeetingDecision,
  UnArchiveMeeting,
  // pathc action
  updateMeetingData,
} from "../../../redux/actions/meetings";
import { createTaskFromFollowup } from "../../../redux/actions/tasks";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import CloseIcon from "@material-ui/icons/Close";
import IconButton from "../../../components/Buttons/IconButton";
import Grid from "@material-ui/core/Grid";
import { grid } from "../../../components/CustomTable2/gridInstance";
import taskDetailStyles from "./styles";
import { withStyles } from "@material-ui/core/styles";
import EmailIcon from "@material-ui/icons/Email";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import { withSnackbar } from "notistack";
import ClockIcon from "@material-ui/icons/AccessTime";
import CalendarIcon from "@material-ui/icons/DateRange";
import SandTimerIcon from "@material-ui/icons/HourglassEmpty";
import LocationIcon from "@material-ui/icons/LocationOn";
import StaticDatePicker from "../../../components/DatePicker/StaticDatePicker";
import TimeInput from "../../../components/DatePicker/TimeInput";
import cloneDeep from "lodash/cloneDeep";
import DurationInput from "../../../components/DatePicker/DurationInput";
import DefaultTextField from "../../../components/Form/TextField";
import DefaultButton from "../../../components/Buttons/DefaultButton";
import Typography from "@material-ui/core/Typography";
import Agenda from "./Agenda";
import DiscussionNotes from "./DiscussionNotes";
import FollowUp from "./FollowUp";
import Decision from "./Decision";
import MemberListMenu from "../../../components/Menu/TaskMenus/MemberListMenu";
import helper from "../../../helper";
import moment from "moment";
import RepeatMeetingForm from "./RepeatMeetingForm/RepeatMeetingForm";
import MomEmailDialog from "../../../components/Dialog/MomEmailDialog";
import CustomButton from "../../../components/Buttons/CustomButton";
import {
  getReadOnlyPermissions,
} from "../permissions";
import SvgIcon from "@material-ui/core/SvgIcon";
import RecurrenceIcon from "../../../components/Icons/RecurrenceIcon";
import UpArrow from "@material-ui/icons/ArrowDropUp";
import DropdownArrow from "@material-ui/icons/ArrowDropDown";
import SearchDropdown from "../../../components/Dropdown/SearchDropdown";
import SelectSearchDropdown from "../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import CustomAvatar from "../../../components/Avatar/Avatar";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import ToggleButton from "@material-ui/lab/ToggleButton";
import CustomIconButton from "../../../components/Buttons/CustomIconButton";
import CancelIcon from "@material-ui/icons/Cancel";
import {
  generateAssigneeData,
  generateTaskData,
  generateProjectData,
} from "../../../helper/generateSelectData";
import TimeSelect from "../../../components/TimePicker/TimeSelect";
import RepeatMeeting from "../RepeatMeeting/RepeatMeeting";
import DropdownMenu from "../../../components/Dropdown/DropdownMenu";
import { isEmpty } from "validator";
import AssigneeSelectDropdown from "../../../components/Dropdown/AssigneeSelectDropdown";
import ZoomDescription from "./ZoomDescription";
import MicrosoftTeamDescription from "./MicrosoftTeamDescription";
import VideoMoreAction from "../../../components/VideoCall/VideoMoreAction";
import CreateableSelectDropdown from "../../../components/Dropdown/SelectCreateableDropdown/SelectCreateableDropdown";
import { FormattedMessage, injectIntl } from "react-intl";
import { Scrollbars } from "react-custom-scrollbars";
import { calculateHeight } from "../../../utils/common";
import ProjectPermissionSelector from "../../../redux/selectors/projectPermissionSelector";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";
import isUndefined from "lodash/isUndefined";
import { ProjectMandatory } from "../../../helper/config.helper";
import GoogleMeetDescription from "./GoogleMeetDescription";

class MeetingDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: true,
      name: [],
      meetingNameError: false,
      editMeetingName: false,
      meetingTitle: props.currentMeeting ? props.currentMeeting.meetingDisplayName : "Meeting No.1",
      meetingTitleOriginal: props.currentMeeting
        ? props.currentMeeting.meetingDisplayName
        : "Meeting No.1",
      currentMeeting: props.currentMeeting ? props.currentMeeting : {},
      meetingLocation: props.currentMeeting ? props.currentMeeting.meetingLocation : "",
      locationError: false,
      editMeetingDetails: false,
      addAgenda: false,
      agendaArray:
        props.currentMeeting &&
          props.currentMeeting.meetingAgendas &&
          props.currentMeeting.meetingAgendas.length
          ? props.currentMeeting.meetingAgendas
          : [],
      agenda: "",
      agendaError: "",
      discussion: "",
      discussionError: "",
      discussionArray:
        props.currentMeeting &&
          props.currentMeeting.discussionPoints &&
          props.currentMeeting.discussionPoints.length
          ? props.currentMeeting.discussionPoints
          : [],
      followUpArray:
        props.currentMeeting &&
          props.currentMeeting.followUpActions &&
          props.currentMeeting.followUpActions.length
          ? props.currentMeeting.followUpActions
          : [],
      followUp: "",
      followUpError: "",
      decisionArray:
        props.currentMeeting &&
          props.currentMeeting.decisions &&
          props.currentMeeting.decisions.length
          ? props.currentMeeting.decisions
          : [],
      decision: "",
      decisionError: "",
      date: props.currentMeeting
        ? props.currentMeeting.startDate
        : helper.RETURN_CUSTOMDATEFORMAT(moment()),
      // taskId: props.currentMeeting ? props.currentMeeting.taskId : "",
      duration: props.currentMeeting
        ? `${props.currentMeeting.durationHours}:${props.currentMeeting.durationMins}`
        : "01:00",
      time: props.currentMeeting ? props.currentMeeting.startTime : "",
      isPublished: false,
      isUnpublished: false,
      isReview: false,
      isPublishedNow: false,
      isCancelled: false,
      isUnpublishedNow: false,
      isReviewNow: false,
      showEmailPopup: false,
      requiredInputDuration: false,
      requiredInputTime: false,
      publishBtnQuery: "",
      submitReviewBtnQuery: "",
      unpublishBtnQuery: "",
      meetingExpanded: true,
      participantsExpanded: true,
      removeAttend: false,
      isChange: false,
      hours: props.currentMeeting ? props.currentMeeting.durationHours : "",
      mins: props.currentMeeting ? props.currentMeeting.durationMins : "",
      timehours: "",
      timemins: "",
      timeFormat: "",
      repeatDropdown: false,
      unArchiveFlag: false,
      unarchiveBtnQuery: "",
      saveDetails: false,
      selectedLocation: [],
      newLocation: [],
      defaultLocation: [{ id: "@zoom", label: "@Zoom" }],
      participantAdding: false,
      selectedTasks: [],
    };
    this.handleMeetingNameEdit = this.handleMeetingNameEdit.bind(this);
    this.handleMeetingNameInput = this.handleMeetingNameInput.bind(this);
    this.handleMeetingNameSave = this.handleMeetingNameSave.bind(this);
    this.editMeetingDetails = this.editMeetingDetails.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.handleInput = this.handleInput.bind(this);
    this.saveAction = this.saveAction.bind(this);
    this.handleTimeChange = this.handleTimeChange.bind(this);
    this.handleDurationChange = this.handleDurationChange.bind(this);
    this.selectedSaveDate = this.selectedSaveDate.bind(this);
    this.updateAction = this.updateAction.bind(this);
    this.deleteAction = this.deleteAction.bind(this);
    this.convertFollowupToTask = this.convertFollowupToTask.bind(this);
    this.publishMeeting = this.publishMeeting.bind(this);
    this.publishMeetingAfterConfirmation = this.publishMeetingAfterConfirmation.bind(this);
    this.unPublishMeeting = this.unPublishMeeting.bind(this);
    this.unPublishMeetingAfterConfirmation = this.unPublishMeetingAfterConfirmation.bind(this);
    this.reviewMeeting = this.reviewMeeting.bind(this);
    this.reviewMeetingAfterConfirmation = this.reviewMeetingAfterConfirmation.bind(this);
    this.openMinutesOfMeeting = this.openMinutesOfMeeting.bind(this);
    this.onZoomHandler = this.onZoomHandler.bind(this);
    this.closeEmailPopup = this.closeEmailPopup.bind(this);
  }

  componentDidMount() {
    const { currentMeeting } = this.props;
    if (currentMeeting) {
      let timeHours = currentMeeting.startTime
        ? currentMeeting.startTime.substring(0, 2)
        : null; /** extracting hours from start time string  */
      let timeMins = currentMeeting.startTime
        ? currentMeeting.startTime.substring(3, 5)
        : null; /** extracting mins from start time string  */
      let timeFormat = currentMeeting.startTime
        ? currentMeeting.startTime.substring(6, 8)
        : null; /** extracting time format from start time string  */
      this.setState({
        currentMeeting,
        agendaArray: currentMeeting.meetingAgendas,
        discussionArray: currentMeeting.discussionPoints,
        decisionArray: currentMeeting.decisions,
        followUpArray: currentMeeting.followUpActions,
        isPublishedNow: currentMeeting.status === "Published" ? true : false,
        isCancelled: currentMeeting.status === "Cancelled" ? true : false,
        isReviewNow: currentMeeting.status === "InReview" ? true : false,
        timehours: timeHours
          ? timeHours
          : "12" /** added extra check if start time  is undefined or empty/null then default time hours value will be selected */,
        timemins: timeMins
          ? timeMins
          : "00" /** added extra check if start time  is undefined or empty/null then default time mins value will be selected */,
        timeFormat: timeFormat
          ? timeFormat
          : "AM" /** added extra check if start time is undefined or empty/null then default time format value will be selected */,
        selectedTasks: this.generateSelectedValue(),
      });
    }

    document.querySelector("#root").style.filter = "blur(8px)";
  }
  generateSelectedValue = () => {
    /** function for generating the selected task list objects from task id and return taskList for showing pre populated selected task if any => function call in componentDidMount */
    const { tasks, currentMeeting } = this.props;
    const selectedTaskList = tasks.filter(task => {
      return currentMeeting.taskId === task.taskId;
    });
    let taskList = generateTaskData(selectedTaskList);
    return taskList;
  };

  isValidChange = () => { };
  componentWillUnmount() {
    document.querySelector("#root").style.filter = "";
    if (this.props.listView) {
      // if task details is open from list view and url is specific of a task than route it back to list view
      this.props.history.push("/meetings");
    }
  }
  componentDidUpdate(prevProps, prevState) {
    const { currentMeeting, closeMeetingDetailsPopUp } = this.props;
    if (!currentMeeting) {
      closeMeetingDetailsPopUp();
      return;
    }
    if (JSON.stringify(prevProps.currentMeeting) !== JSON.stringify(currentMeeting)) {
      let timeHours = currentMeeting.startTime.substring(
        0,
        2
      ); /** extracting hours from start time string  */
      let timeMins = currentMeeting.startTime.substring(
        3,
        5
      ); /** extracting mins from start time string  */
      let timeFormat = currentMeeting.startTime.substring(
        6,
        8
      ); /** extracting time format from start time string  */

      this.setState({
        currentMeeting,
        agendaArray: currentMeeting.meetingAgendas,
        discussionArray: currentMeeting.discussionPoints,
        decisionArray: currentMeeting.decisions,
        followUpArray: currentMeeting.followUpActions,
        isPublishedNow: currentMeeting.status === "Published" ? true : false,
        isCancelled: currentMeeting.status === "Cancelled" ? true : false,
        isReviewNow: currentMeeting.status === "InReview" ? true : false,
        timehours: timeHours
          ? timeHours
          : "12" /** added extra check if start time  is undefined or empty/null then default time hours value will be selected */,
        timemins: timeMins
          ? timeMins
          : "00" /** added extra check if start time  is undefined or empty/null then default time mins value will be selected */,
        timeFormat: timeFormat
          ? timeFormat
          : "AM" /** added extra check if start time is undefined or empty/null then default time format value will be selected */,
      });
    }
  }

  showSnackBar = (snackBarMessage, type, url = null) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
        {url && (
          <span
            className={classes.snackBarBtn}
            onClick={() => {
              window.location.href = url;
            }}>
            Reauthorize Microsoft Teams
          </span>
        )}
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  openMinutesOfMeeting() {
    this.setState({ showEmailPopup: true });
  }

  closeEmailPopup() {
    this.setState({ showEmailPopup: false });
  }
  onZoomHandler(item) {
    const meeting = cloneDeep(this.state.currentMeeting);
    meeting.meetingLocation = "@zoom";
    this.setState({ currentMeeting: meeting }, () => {
      let obj = { location: "@zoom" };
      this.props.updateMeetingData(
        { meeting, obj },
        null,
        //Success
        data => {
          this.showSnackBar("Instant Zoom meeting has been created.", "success");
          this.setState({ currentMeeting: data });
        },
        err => {
          this.showSnackBar(err.data.message, "error");
        }
      );
    });
  }
  onMicrosoftTeamHandler = item => {
    const meeting = cloneDeep(this.state.currentMeeting);
    meeting.meetingLocation = "@MicrosoftTeams";
    this.setState({ currentMeeting: meeting }, () => {
      let obj = { location: "@MicrosoftTeams" };
      this.props.updateMeetingData(
        { meeting, obj },
        null,
        //Success
        data => {
          this.showSnackBar("Instant Microsoft Teams meeting has been created.", "success");
          this.setState({ currentMeeting: data });
        },
        err => {
          this.showSnackBar(err.data.message, "error");
        }
      );
    });
  };
  onGoogleMeetHandler = item => {
    const meeting = cloneDeep(this.state.currentMeeting);
    meeting.meetingLocation = "@googlemeet";
    this.setState({ currentMeeting: meeting }, () => {
      let obj = { location: "@googlemeet" };
      this.props.updateMeetingData(
        { meeting, obj },
        null,
        //Success
        data => {
          this.showSnackBar("Instant Google Meet meeting has been created.", "success");
          this.setState({ currentMeeting: data });
        },
        err => {
          this.showSnackBar(err.data.message, "error");
        }
      );
    });
  };

  publishMeetingAfterConfirmation() {
    this.setState({ publishBtnQuery: "progress" }, () => {
      this.props.PublishMeeting(
        this.props.currentMeeting,
        response => {
          this.setState({
            isPublished: false,
            isPublishedNow: true,
            publishBtnQuery: "",
          });
          if (grid.grid) {
            const rowNode = grid.grid && grid.grid.getRowNode(response.id);
            rowNode.setData(response);
          }
        },
        error => {
          this.setState({
            isPublished: false,
            publishBtnQuery: "",
          });
          if (error.status === 500) {
            this.showSnackBar("Server throws error", "error");
          } else {
            this.showSnackBar(error.data.message, "error");
          }
        }
      );
    });
  }
  publishMeetingClose = () => {
    this.setState({
      isPublished: false,
    });
  };
  publishMeeting() {
    this.setState({
      isPublished: true,
    });
  }

  reviewMeetingAfterConfirmation() {
    this.setState({ submitReviewBtnQuery: "progress" }, () => {
      this.props.InReviewMeeting(
        this.props.currentMeeting,
        response => {
          this.setState({
            isReview: false,
            isReviewNow: true,
            submitReviewBtnQuery: "",
          });
          if (grid.grid) {
            const rowNode = grid.grid && grid.grid.getRowNode(response.id);
            rowNode.setData(response);
          }
        },
        error => {
          this.setState({
            isReview: false,
            submitReviewBtnQuery: "",
          });
          if (error.status === 500) {
            this.showSnackBar("Server throws error", "error");
          } else {
            this.showSnackBar(error.data.message, "error");
          }
        }
      );
    });
  }
  reviewMeetingClose = () => {
    this.setState({
      isReview: false,
    });
  };
  reviewMeeting() {
    this.setState({
      isReview: true,
    });
  }

  unPublishMeetingAfterConfirmation() {
    this.setState({ unpublishBtnQuery: "progress" }, () => {
      this.props.UnPublishMeeting(
        this.props.currentMeeting,
        response => {
          this.setState({
            isUnpublished: false,
            isUnpublishedNow: true,
            unpublishBtnQuery: "",
          });
        },
        error => {
          this.setState({
            isUnpublished: false,
            unpublishBtnQuery: "",
          });
        }
      );
    });
  }
  unPublishMeetingClose = () => {
    this.setState({ isUnpublished: false });
  };
  unPublishMeeting() {
    this.setState({
      isUnpublished: true,
    });
  }

  convertFollowupToTask(id, followUpAction, successCallback, errorCallback) {
    let obj = {
      actualDueDateString:
        (followUpAction && followUpAction.dueDateString) ||
        helper.RETURN_CUSTOMDATEFORMAT(followUpAction ? moment(followUpAction.dueDate) : moment()),
      dueDateString:
        (followUpAction && followUpAction.dueDateString) ||
        helper.RETURN_CUSTOMDATEFORMAT(followUpAction ? moment(followUpAction.dueDate) : moment()),
      taskTitle: followUpAction && followUpAction.description,
      assigneeList: followUpAction && followUpAction.assigneeList,
    };

    this.props.createTaskFromFollowup(
      id,
      obj,
      resp => {
        // updating meeting followup
        const meetingId = this.props.currentMeeting.meetingId;
        let followupActionCopy = cloneDeep(followUpAction);
        followupActionCopy.isTaskCreated = true;
        this.props.UpdateMeetingFollowupAction(meetingId, followupActionCopy);
        successCallback(resp);
      },
      errorCallback
    );
  }
  selectedSaveDate(date) {
    this.setState({ date, isChange: true });
  }

  handleDurationChange = (hrs, mins) => {
    this.setState({
      hours: hrs,
      mins: mins,
      isChange: true,
      duration: `${hrs}:${mins}`,
    });
  };
  clearSelect = (key, value) => {
    this.setState({ [key]: value });
  };
  clearEffort = (key, value) => {
    this.setState({ [key]: value });
  };

  handleTimeChange(time) {
    this.setState({ time, isChange: true });
  }
  handleMeetingNameEdit() {
    this.setState({ editMeetingName: true });
  }
  meetingLocationValidator = () => {
    let response1 = helper.HELPER_EMPTY("Meeting location", this.state.meetingLocation);
    let response2 = helper.HELPER_CHARLIMIT80("Meeting location", this.state.meetingLocation);
    let response3 = helper.HELPER_ALPHANUMERIC_SPACE("Meeting location", this.state.meetingLocation);
    let error = null;
    if (response1) {
      error = response1;
    } else {
      if (response2) {
        error = response2;
      } else if (response3) {
        error = response3;
      }
    }
    if (error) {
      this.setState({
        locationError: true,
      });
      return false;
    } else {
      return true;
    }
  };

  meetingNameValidator = () => {
    let response1 = helper.HELPER_EMPTY("Meeting title", this.state.meetingTitle);
    let response2 = helper.HELPER_CHARLIMIT250("Meeting title", this.state.meetingTitle);
    let response3 = helper.RETURN_CECKSPACES(this.state.meetingTitle, "Meeting title");
    let error = null;
    if (response1) {
      error = response1;
    } else {
      if (response2) {
        error = response2;
      } else if (response3) {
        error = response3;
      }
    }
    if (error) {
      this.setState({
        meetingNameError: true,
      });
      return false;
    } else {
      return true;
    }
  };
  handleMeetingNameSave(event, isClick) {
    if (
      (isClick === true || (event && event.key === "Enter")) &&
      this.meetingNameValidator() &&
      this.state.meetingTitle &&
      this.state.meetingTitleOriginal !== this.state.meetingTitle
    ) {

      const meeting = cloneDeep(this.state.currentMeeting);
      let obj = { meetingDisplayName: this.state.meetingTitle };

      this.props.updateMeetingData(
        { meeting, obj },
        null,
        //Success
        succ => {
          this.setState({
            editMeetingName: false,
            meetingNameError: false,
            meetingTitleOriginal: this.state.meetingTitle,
          });
          if (grid.grid) {
            const rowNode = grid.grid && grid.grid.getRowNode(succ.id);
            rowNode.setData(succ);
          }
        },
        err => {
          this.setState({
            editMeetingName: false,
            meetingNameError: false,
            meetingTitleOriginal: this.state.meetingTitle,
          });
          this.showSnackBar(err.data.message, "error");
        }
      )

      // this.props.UpdateMeeting(obj, (err, data) => {
      //   if (data) {
      //     this.setState({
      //       editMeetingName: false,
      //       meetingNameError: false,
      //       meetingTitleOriginal: this.state.meetingTitle,
      //     });
      //   }
      //   if (err) {
      //     this.showSnackBar(err.data.message, "error");
      //   }
      // });
    }
  }
  handleMeetingNameInput(event) {
    this.setState({ meetingTitle: event.target.value });
  }
  editMeetingDetails() {
    this.setState({ editMeetingDetails: true });
  }
  handleCancel() {
    this.setState({ editMeetingDetails: false, isChange: false });
  }
  getParticipants = () => {
    const { currentMeeting } = this.state;
    let data = this.props.wsMembers.filter(item => {
      if (currentMeeting.attendeeList.indexOf(item.userId) != -1) return item;
    });
    return generateAssigneeData(data);
  };
  getMemberDropdownData = () => {
    // Members dropdown data
    const { currentMeeting } = this.state;
    let data = this.props.wsMembers.filter(item => {
      if (currentMeeting.attendeeList.indexOf(item.userId) != -1) return item;
    });
    return data;
  };
  handleSave() {
    let { meetingTitle, date, time, duration, meetingLocation, currentMeeting, hours, mins, projectId } = this.state;
    this.setState({ saveDetails: true });
    let object = {
      meetingTitle,
      date,
      time,
      duration,
      meetingLocation,
      taskId: currentMeeting.taskId,
      hours,
      mins,
      participant: this.getParticipants(),
      projectId: currentMeeting.projectId,
    },
      flag = 0;
    const timeHours = this.state.time ? parseInt(this.state.time.split(":")[0]) : "";
    const timeMins = this.state.time ? parseInt(this.state.time.split(":")[1].split(" ")[0]) : "";
    if (!this.state.time || !(timeMins || timeHours)) {
      flag = 1;
      this.setState({
        requiredInputTime: true,
      });
    } else {
      this.setState({
        requiredInputTime: false,
      });
    }
    if (
      !this.state.duration ||
      this.state.duration === "0:0" ||
      this.state.duration === "00:0" ||
      this.state.duration === "00:00" ||
      this.state.duration === "0:00"
    ) {
      flag = 1;
      this.setState({
        requiredInputDuration: true,
      });
    } else {
      this.setState({
        requiredInputDuration: false,
      });
    }
    if ((hours == "0" && mins == "0") || (hours == null && mins == null)) {
      this.showSnackBar("Oops! Please select duration hours or minute.", "error");
    }
    if (flag) {
      return;
    }
    const { meetingDisplayName, endDateString, endTime, meetingAgendasList,
      attendeeList, ...obj } = helper.CALCULATEDURATIONDATETIME(object);
    currentMeeting = { ...currentMeeting, ...obj };
    const meeting = cloneDeep(this.state.currentMeeting);
    // remove project and task from meeting
    if (this.props.currentMeeting.projectId && obj.projectId == undefined) {
      obj.projectId = '';
    }
    if (this.props.currentMeeting.taskId && obj.taskId == undefined) {
      obj.taskId = '';
    }
    this.props.updateMeetingData(
      { meeting, obj },
      null,
      //Success
      succ => {
        this.setState({
          editMeetingDetails: false,
          saveDetails: false,
          isChange: false,
        });
        if (grid.grid) {
          const rowNode = grid.grid && grid.grid.getRowNode(succ.id);
          rowNode.setData(succ);
        }
      },
      err => {
        if (err && err.data.url) {
          this.showSnackBar(err.data.message, "error", err.data.url);
        } else this.showSnackBar(err.data.message, "error");
        this.setState({
          saveDetails: false,
        });
      }
    );
  }
  handleInput(event, name) {
    this.setState({
      [name]: event.target.value,
      agendaError: "",
      decisionError: "",
      followUpError: "",
    });
  }
  handleLocationInput = (event, name) => {
    this.setState({
      [name]: event.target.value,
      agendaError: "",
      decisionError: "",
      followUpError: "",
      isChange: true,
    });
  };
  saveAction(value, data, callback) {
    // const object = cloneDeep(this.state.currentMeeting);
    const meeting = cloneDeep(this.state.currentMeeting);

    if (value === "agenda") {
      if (!this.state.agenda) {
        callback();
        this.setState({ agendaError: "Please enter agenda value" }, () => {
          callback();
        });
        return;
      }
      if (helper.RETURN_CECKSPACES(this.state.agenda)) {
        this.setState({ agendaError: "" }, () => {
          callback();
        });
        return;
      }
      if (this.state.agenda.length > 250) {
        this.setState({ agendaError: "Please enter less than 250 characters" }, () => {
          callback();
        });
        return;
      }
      let obj = { meetingAgendasList: [this.state.agenda] };
      this.setState({ agenda: "", agendaError: '' }, () => {
        this.props.updateMeetingData(
          { meeting, obj },
          null,
          //Success
          succ => {
            this.setState({ editMeetingDetails: false, currentMeeting: succ });
            if (grid.grid) {
              const rowNode = grid.grid && grid.grid.getRowNode(succ.id);
              rowNode.setData(succ);
            }
            callback();
          },
          err => {
            this.showSnackBar(err.data.message, "error");
            callback();
          }
        );
        // this.props.UpdateMeeting(object, (err, data) => {
        //   if (data) {
        //     // this.setState({ editMeetingDetails: false });
        //   }
        //   if (err) {
        //     this.showSnackBar(err.data.message, "error");
        //   }
        //   callback();
        // });
      });
    } else if (value === "discussion") {
      if (data) {
        this.setState({ agenda: "" })
        let obj = { discussionPointsList: [data] };
        this.props.updateMeetingData(
          { meeting, obj },
          null,
          //Success
          succ => {
            if (grid.grid) {
              const rowNode = grid.grid && grid.grid.getRowNode(succ.id);
              rowNode.setData(succ);
            }
            callback();
          },
          err => {
            this.showSnackBar(err.data.message, "error");
            callback();
          }
        );
        // object.discussionPointsList = [data];
        // this.props.UpdateMeeting(object, (err, data) => {
        //   callback();
        //   if (err) {
        //     this.showSnackBar(err.data.message, "error");
        //   }
        // });
      }
    } else if (value === "decision") {
      if (!this.state.decision) {
        this.setState({ decisionError: "Please enter decision." }, () => {
          callback();
        });
        return;
      }
      if (helper.RETURN_CECKSPACES(this.state.decision)) {
        this.setState({ decisionError: "" }, () => {
          callback();
        });
        return;
      }
      if (this.state.decision.length > 250) {
        this.setState(
          {
            decisionError: "Please enter less than 250 characters",
          },
          () => {
            callback();
          }
        );
        return;
      }
      let obj = { decisionsList: [this.state.decision] };
      this.props.updateMeetingData(
        { meeting, obj },
        null,
        //Success
        succ => {
          this.setState({ decision: "", decisionError: "" });
          if (grid.grid) {
            const rowNode = grid.grid && grid.grid.getRowNode(succ.id);
            rowNode.setData(succ);
          }
          callback();
        },
        err => {
          this.showSnackBar(err.data.message, "error");
          callback();
        }
      );
      // object.decisionsList = [this.state.decision];
      // this.props.UpdateMeeting(object, (err, data) => {
      //   if (data) {
      //     this.setState({ decision: "", decisionError: "" });
      //   }
      //   if (err) {
      //     this.showSnackBar(err.data.message, "error");
      //   }
      //   callback();
      // });
    } else if (value === "followUp") {
      if (!this.state.followUp) {
        this.setState({ followUpError: "Please enter follow up actions." }, () => {
          callback();
        });
        return;
      }
      if (helper.RETURN_CECKSPACES(this.state.followUp)) {
        this.setState({ followUpError: "" }, () => {
          callback();
        });
        return;
      }
      if (this.state.followUp.length > 250) {
        this.setState(
          {
            followUpError: "Please enter less than 250 characters",
          },
          () => {
            callback();
          }
        );
        return;
      }
      // object.followUpActionsList = [
      //   {
      //     description: this.state.followUp,
      //     meetingId: object.meetingId,
      //     dueDateString: helper.RETURN_CUSTOMDATEFORMAT(data.date),
      //     assignee: data.assignees,
      //   },
      // ];
      let obj = {
        followUpActionsList: [{
          description: this.state.followUp,
          meetingId: meeting.meetingId,
          dueDateString: helper.RETURN_CUSTOMDATEFORMAT(data.date),
          assignee: data.assignees,
        }]
      };
      if (!isEmpty(this.state.followUp)) {
        this.setState({ followUp: "", followUpError: "" });
        this.props.updateMeetingData(
          { meeting, obj },
          null,
          //Success
          succ => {
            if (grid.grid) {
              const rowNode = grid.grid && grid.grid.getRowNode(succ.id);
              rowNode.setData(succ);
            }
            callback();
          },
          err => {
            this.showSnackBar(err.data.message, "error");
            callback();
          }
        );
        // this.props.UpdateMeeting(object, (err, data) => {
        //   if (data) {
        //     // this.setState({ followUp: "", followUpError: "" });
        //   }
        //   if (err) {
        //     this.showSnackBar(err.data.message, "error");
        //   }
        //   callback();
        // });
      }
    }
  }

  updateAction(value, key, meetingData) {
    if (key === "agenda") {
      if (!value.description) {
        this.setState({ agendaError: "" });
        return;
      }
      if (helper.RETURN_CECKSPACES(value.description)) {
        this.setState({
          agendaError: helper.RETURN_CECKSPACES(value.description),
        });
        return;
      }
      if (value.description.length > 250) {
        this.setState({ agendaError: "Please enter less than 250 characters" });
        return;
      }
      this.setState({ agenda: "" }, () => {
        this.props.UpdateAgenda(value, meetingData, (err, data) => {
          if (data) {
            this.setState({ agendaError: "" });
          }
        });
      });
    }

    if (key === "decision") {
      if (!value.description) {
        this.setState({ decisionError: "" });
        return;
      }
      if (helper.RETURN_CECKSPACES(value.description)) {
        this.setState({
          decisionError: helper.RETURN_CECKSPACES(value.description),
        });
        return;
      }
      if (value.description.length > 80) {
        this.setState({
          decisionError: "Please enter less than 80 characters",
        });
        return;
      }
      this.setState({ decision: "" }, () => {
        this.props.UpdateMeetingDecision(value, meetingData, (err, data) => {
          if (data) {
            this.setState({ decisionError: "" });
          }
        });
      });
    }
    if (key === "followUp") {
      if (!value.description) {
        this.setState({ followUpError: "" });
        return;
      }
      if (helper.RETURN_CECKSPACES(value.description)) {
        this.setState({
          followUpError: helper.RETURN_CECKSPACES(value.description),
        });
        return;
      }
      if (value.description.length > 250) {
        this.setState({
          followUpError: "Please enter less than 250 characters",
        });
        return;
      }
      this.setState({ followUp: "" }, () => {
        this.props.UpdateMeetingFollowUpAction(value, meetingData, (err, data) => {
          if (data) {
            this.setState({ followUpError: "" });
          }
        });
      });
    }
  }

  deleteAction(object, type, current, callback) {
    if (type === "discussion") {
      this.props.DeleteDiscussionPoint(object, current, callback);
    }
    if (type === "decision") {
      this.props.RemoveMeetingDecision(object, current, callback);
    }

    if (type === "agenda") {
      this.props.DeleteMeetingAgenda(object, current, callback);
    }

    if (type === "followUp") {
      this.props.DeleteMeetingFollowUpAction(object, current, callback);
    }
  }

  handleMeetingsClickAway = e => {
    e.stopPropagation();
    if (this.meetingNameValidator())
      this.setState({ editMeetingName: false }, () => {
        this.handleMeetingNameSave(null, true);
      });
  };
  handleMeetingExpandClick = () => {
    this.setState((prevState, props) => {
      return {
        meetingExpanded: !prevState.meetingExpanded,
      };
    });
  };
  handleParticipantsExpandClick = () => {
    this.setState((prevState, props) => {
      return {
        participantsExpanded: !prevState.participantsExpanded,
      };
    });
  };
  //Function that updates Meeting Task
  updateTask = (selectedTask, selectedObj) => {
    let newMeetingObj = {
      ...this.state.currentMeeting,
      taskId: selectedObj.id ? selectedObj.id : "",
      projectId: selectedObj.obj.projectId,
    };
    this.setState({ currentMeeting: newMeetingObj, isChange: true, selectedTasks: [selectedObj] });
    // this.props.UpdateMeeting(newMeetingObj, () => { }, () => { });
  };
  updateProject = (selectedTask, selectedObj) => {
    const { currentMeeting, selectedTasks } = this.state;

    let linkTask = selectedTasks.length
      ? selectedTasks[0].obj.projectId == "" || !selectedTasks[0].obj.projectId
        ? []
        : currentMeeting.taskId
      : currentMeeting.taskId;

    let newMeetingObj = {
      ...this.state.currentMeeting,
      projectId: selectedObj.obj.projectId,
      taskId: "",
    };
    this.setState({ currentMeeting: newMeetingObj, isChange: true });
    // this.props.UpdateMeeting(newMeetingObj, () => { }, () => { });
  };
  /**
   * For clear task from meeting
   */
  handleClearTask = () => {
    const { currentMeeting } = this.state;
    let newMeetingObj = {
      ...currentMeeting,
      taskId: "",
    };
    this.setState({ currentMeeting: newMeetingObj, isChange: true, selectedTasks: [] });
    // this.props.UpdateMeeting(newMeetingObj, () => { }, () => { });
  };
  handleClearProject = () => {
    const { currentMeeting } = this.state;
    let newMeetingObj = {
      ...currentMeeting,
      taskId: "",
      projectId: "",
    };
    this.setState({ currentMeeting: newMeetingObj, isChange: true, selectedTasks: [] });
    // this.props.UpdateMeeting(newMeetingObj, () => { }, () => { });
  };
  // Generate list of all projects for dropdown understandable form
  generateTaskDropdownData = meeting => {
    const { tasks } = this.props;
    let tasksData =
      meeting.projectId
        ? tasks.filter(t => t.projectId === meeting.projectId)
        : tasks;

    let taskList = generateTaskData(tasksData);
    return taskList;
  };
  generateProjectDropdownData = () => {
    const { currentMeeting } = this.state;
    const { projects } = this.props;
    let selectedProject =
      currentMeeting.projectId && currentMeeting.projectId !== ""
        ? projects.filter(t => t.projectId != currentMeeting.projectId)
        : projects;
    selectedProject = selectedProject.length ? selectedProject : projects;

    return generateProjectData(
      selectedProject.filter(p => p.projectId !== currentMeeting.projectId)
    );
  };
  //Generate list of selected options for project dropdown understandable form
  getSelectedTask = meeting => {
    const { tasks } = this.props;
    let selectedTask = tasks.find(task => {
      return task.taskId == meeting.taskId;
    });

    return selectedTask
      ? [
        {
          label: selectedTask.taskTitle,
          id: selectedTask.taskId,
          obj: meeting,
        },
      ]
      : [];
  };

  getSelectedProject = meeting => {
    const { currentMeeting, selectedTasks } = this.state;

    if (meeting.projectId) {
      const selectedProject = this.props.projects.find(project => {
        return project.projectId == meeting.projectId;
      });

      return selectedProject
        ? [
          {
            label: selectedProject.projectName,
            id: selectedProject.projectId,
            obj: {},
          },
        ]
        : [];
    } else return [];
  };
  attendenceHandle = (event, value, item) => {
    if (value) {
      const { currentMeeting } = this.state;
      let index = currentMeeting.attendees.findIndex(x => x.attendeeId == item.userId);
      let attend = { ...currentMeeting.attendees[index] };
      attend.isPresent = value == "absent" ? false : true;
      currentMeeting.attendees.splice(index, 1, attend);
      this.setState({ currentMeeting });
      // this.props.UpdateMeeting(currentMeeting, (err, response) => {
      //   if (response.data) {
      //     this.setState({ currentMeeting });
      //   }
      //   if (err) {
      //     this.showSnackBar(err.data.message, "error");
      //   }
      // });
      let obj = { attendees: currentMeeting.attendees };
      let meeting = currentMeeting;
      this.props.updateMeetingData(
        { meeting, obj },
        null,
        //Success
        succ => {
          this.setState({ currentMeeting })
          if (grid.grid) {
            const rowNode = grid.grid && grid.grid.getRowNode(succ.id);
            rowNode.setData(succ);
          }
        },
        err => {
          this.showSnackBar(err.data.message, "error");
        }
      );
    }
  };
  removeAttendenceHandle = item => {
    const { currentMeeting } = this.state;
    if (currentMeeting.createdById == item.userId) return;
    this.setState({
      removeAttend: true,
      removeAttendItem: item,
    });
  };
  removeAttendAfterConfirmation = () => {
    const meeting = cloneDeep(this.state.currentMeeting);
    const { currentMeeting, removeAttendItem } = this.state;
    let index = currentMeeting.attendeeList.findIndex(x => x == removeAttendItem.userId);
    currentMeeting.attendeeList.splice(index, 1);
    this.setState({ currentMeeting, submitReviewBtnQuery: "progress" });
    let obj = { attendeeList: currentMeeting.attendeeList };
    this.props.updateMeetingData(
      { meeting, obj },
      null,
      //Success
      succ => {
        this.setState({
          currentMeeting,
          removeAttend: false,
          removeAttendItem: null,
          submitReviewBtnQuery: "",
        });
        if (grid.grid) {
          const rowNode = grid.grid && grid.grid.getRowNode(succ.id);
          rowNode.setData(succ);
        }
      },
      err => {
        this.setState({ participantAdding: false });
        this.showSnackBar(err.data.message, "error");
      }
    );
    // this.props.UpdateMeeting(currentMeeting, (err, response) => {
    //   if (response.data) {
    //     this.setState({
    //       currentMeeting,
    //       removeAttend: false,
    //       removeAttendItem: null,
    //       submitReviewBtnQuery: "",
    //     });
    //   }
    //   if (err) {
    //     this.showSnackBar(err.data.message, "error");
    //   }
    // });
  };
  removeAttendClose = () => {
    this.setState({
      removeAttend: false,
    });
  };

  clearSelect = (key, value) => {
    this.setState({ [key]: value, isChange: true });
  };
  updateTime = (hours, minutes, timeFormat) => {
    const time = `${hours}:${minutes} ${timeFormat}`;
    this.setState({ time, isChange: true });
  };
  //Open Repeat Dialog Function
  repeatDropdownOpen = (e) => {
    e.stopPropagation();
    this.setState({ repeatDropdown: true });
  };
  //Close Repeat Dialog Function
  closeRepeatDropdown = () => {
    this.setState({ repeatDropdown: false });
  };
  // It is called to open unarchived dialog
  openPopUP = event => {
    this.setState({ unArchiveFlag: true });
  };
  // It is called to unarchived task
  handleUnArchived = e => {
    if (e) e.stopPropagation();
    const { currentMeeting } = this.state;
    const { meetingId, parentId } = currentMeeting || "";
    this.setState({ unarchiveBtnQuery: "progress" }, () => {

      this.props.UnArchiveMeeting(
        { meetingId: meetingId, parentId: parentId },
        result => {
          this.setState({ unarchiveBtnQuery: "", unArchiveFlag: false });
          this.props.closeMeetingDetailsPopUp();
          this.props.filterMeeting([result.meetingId]);
        },
        error => {
          this.setState({ unarchiveBtnQuery: "", unArchiveFlag: false });
          this.showSnackBar("Meeting Cannot be UnArchived", "error");
        },
      );
    });
  };
  // It is called to close unarchived dialog
  handleDialogClose = event => {
    this.setState({ unArchiveFlag: false });
  };

  addMeetingAttendee = (assignedTo, meeting) => {
    let assignedToArr = assignedTo.map(ass => {
      return ass.userId;
    });
    let newMeetingObj = {
      ...meeting,
      attendeeList: [...meeting.attendeeList, ...assignedToArr],
    };
    this.props.UpdateMeeting(newMeetingObj, () => { });
  };

  getArchivedUserName = () => {
    let members = this.props.profileState.data.member.allMembers || [];
    const { currentMeeting } = this.state;
    let user = members.find(
      m => m.userId === (currentMeeting.updatedById || currentMeeting.createdById)
    );
    return user ? user.fullName || user.userName || user.email : "";
  };

  updateParticipants = (assignedTo, meeting, add) => {
    this.setState({ participantAdding: true }, () => {
      const { currentMeeting } = this.state;
      let assignee = currentMeeting.attendeeList;
      assignee.push(assignedTo[0].userId);
      let obj = { attendeeList: assignee };
      this.props.updateMeetingData(
        { meeting, obj },
        null,
        //Success
        succ => {
          this.setState({ participantAdding: false });
          if (grid.grid) {
            const rowNode = grid.grid && grid.grid.getRowNode(succ.id);
            rowNode.setData(succ);
          }
        },
        err => {
          this.setState({ participantAdding: false });
          this.showSnackBar(err.data.message, "error");
        }
      );
      // this.props.updateMeetingAttendee(assignedTo, meeting, add, success => {
      //   this.setState({ participantAdding: false });
      // });
    });
  };

  render() {
    const {
      classes,
      theme,
      isArchivedSelected,
      wsMembers,
      profileState,
      workspaceMeetingPer,
      isZoomLinked,
      permissionObject,
      isTeamLinked,
      isGoogleLinked,
    } = this.props;
    const intl = this.props.intl;
    const {
      open,
      currentMeeting,
      editMeetingName,
      meetingTitle,
      meetingLocation,
      locationError,
      editMeetingDetails,
      agenda,
      agendaArray,
      discussion,
      discussionArray,
      followUp,
      followUpArray,
      decision,
      decisionError,
      agendaError,
      followUpError,
      discussionError,
      decisionArray,
      isPublished,
      isUnpublished,
      isReview,
      isPublishedNow,
      isCancelled,
      isReviewNow,
      showEmailPopup,
      publishBtnQuery,
      unpublishBtnQuery,
      submitReviewBtnQuery,
      requiredInputDuration,
      requiredInputTime,
      meetingExpanded,
      participantsExpanded,
      removeAttend,
      removeAttendItem,
      isChange,
      hours,
      mins,
      timeFormat,
      timehours,
      timemins,
      repeatDropdown,
      unArchiveFlag,
      unarchiveBtnQuery,
      saveDetails = false,
      newLocation,
      participantAdding = false,
    } = this.state;

    const meetingPermission =
      currentMeeting && currentMeeting.projectId
        ? isUndefined(permissionObject[currentMeeting.projectId])
          ? workspaceMeetingPer
          : permissionObject[currentMeeting.projectId].meeting
        : workspaceMeetingPer;


    const readOnly = getReadOnlyPermissions(currentMeeting);
    let panelBodyStatus = [classes.panelBody];
    if (isPublishedNow || isCancelled) {
      panelBodyStatus.push(classes.panelBodyDisable);
    }
    let members = profileState.data.member.allMembers || [];
    let meetingOwner;
    let zoomMeetingUrl = "";
    let teamMeetingUrl = "";
    let googleMeetUrl = "";
    let createrName = "";
    if (currentMeeting.zoomMeetingId) {
      meetingOwner = profileState.data.userId == currentMeeting.createdById;
      zoomMeetingUrl = meetingOwner
        ? currentMeeting.zoomHostUrl
        : currentMeeting.zoomParticipantUrl;
      createrName = profileState.data.member.allMembers.find(
        item => item.userId == currentMeeting.createdById
      ).fullName;
    }
    if (currentMeeting.teamMeetingId) {
      meetingOwner = profileState.data.userId == currentMeeting.createdById;
      teamMeetingUrl = meetingOwner
        ? currentMeeting.teamHostUrl
        : currentMeeting.teamParticipantUrl;
      createrName = profileState.data.member.allMembers.find(
        item => item.userId == currentMeeting.createdById
      ).fullName;
    }
    // google meet integration
    if (currentMeeting.googleMeetingId) {
      meetingOwner = profileState.data.userId == currentMeeting.createdById;
      googleMeetUrl = meetingOwner
        ? currentMeeting.googleMeetingUrl
        : currentMeeting.teamParticipantUrl;
      createrName = profileState.data.member.allMembers.find(
        item => item.userId == currentMeeting.createdById
      ).fullName;
    }

    return (
      <Dialog
        classes={{
          root: classes.taskDetailsDialog,
          paper: classes.dialogPaperCnt,
          scrollBody: classes.dialogCnt,
        }}
        aria-labelledby="form-dialog-title"
        fullWidth={true}
        open={open}
        onEscapeKeyDown={this.props.closeMeetingDetailsPopUp}>
        {/* <RepeatMeetingForm
          currentMeeting={currentMeeting}
          open={repeatDialogOpen}
          closeAction={this.repeatDialogClose}
          showSnackBar={this.showSnackBar}
        /> */}
        {open && (
          <DropdownMenu
            // open={open}
            anchorEl={this.anchorEl}
            placement="bottom-start"
            style={{
              width: 580,
            }}
            id="meetingRepeatDropdown"
            closeAction={this.closeRepeatDropdown}
            open={repeatDropdown}>
            <div>
              <RepeatMeeting
                closeAction={this.closeRepeatDropdown}
                meeting={currentMeeting}
                label={true}
                meetingPer={meetingPermission}
              />
            </div>
          </DropdownMenu>
        )}

        <DialogTitle classes={{ root: classes.defaultDialogTitle }}>
          {isArchivedSelected ? (
            <div className={classes.unArchiveTaskCnt}>
              <Typography variant="h6" className={classes.unArchiveTaskTxt}>
                This meeting is archived. You can't edit this meeting.
              </Typography>
              {meetingPermission.unarchiveMeetings.cando ? <CustomButton
                btnType="success"
                variant="contained"
                style={{ marginLeft: 10 }}
                onClick={event => {
                  this.openPopUP(event);
                }}>
                Unarchive
              </CustomButton> : null
              }

            </div>
          ) : null}
          <Grid
            container
            direction="row"
            justify="space-between"
            alignItems="center"
            style={{ padding: "10px 15px" }}>
            <Grid item sm={12} md={6} lg={6}>
              {editMeetingName &&
                meetingPermission.meetingDetail.canEditName.isAllowEdit ? (
                <ClickAwayListener onClickAway={this.handleMeetingsClickAway}>
                  <OutlinedInput
                    labelWidth={150}
                    notched={false}
                    value={meetingTitle}
                    onKeyDown={this.handleMeetingNameSave}
                    autoFocus
                    error={this.state.meetingNameError}
                    onChange={this.handleMeetingNameInput}
                    placeholder={"Add Meeting Title"}
                    style={{ width: 500 }}
                    inputProps={{ maxLength: 250 }}
                    classes={{
                      root: classes.outlinedInputCnt,
                      input: classes.outlinedTaskInput,
                      notchedOutline: classes.notchedOutlineCnt,
                      focused: classes.outlineInputFocus,
                    }}
                  />
                </ClickAwayListener>
              ) : (
                <Typography
                  classes={{ h2: classes.taskTitle }}
                  variant="h2"
                  onClick={this.handleMeetingNameEdit}>
                  {meetingTitle}
                </Typography>
              )}
            </Grid>
            <Grid item sm={12} md={6} lg={6}>
              <Grid container direction="row" justify="flex-end" alignItems="center">
                <div className="flex_center_start_row">
                  {isPublishedNow ? (
                    <>
                      <Typography variant="body2" style={{ marginRight: 10 }}>
                        <FormattedMessage
                          id="meeting.detail-dialog.publish-by.label"
                          defaultMessage="Published by:"
                        />
                      </Typography>
                      {members.find(
                        m => m.userId === (currentMeeting.updatedById || currentMeeting.createdById)
                      ).fullName ||
                        members.find(
                          m => m.userId === (currentMeeting.updatedById || currentMeeting.createdById)
                        ).userName ||
                        members.find(
                          m => m.userId === (currentMeeting.updatedById || currentMeeting.createdById)
                        ).email}{" "}
                      - {helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(currentMeeting.updatedDate)} -{" "}
                      {helper.RETURN_CUSTOMDATEFORMATFORTIME(currentMeeting.updatedDate)}
                    </>
                  ) : currentMeeting.status === "Cancelled" ? (
                    <>
                      <Typography variant="body2" style={{ marginRight: 10 }}>
                        Cancelled by:
                      </Typography>
                      {members.find(
                        m => m.userId === (currentMeeting.updatedById || currentMeeting.createdById)
                      ).fullName ||
                        members.find(
                          m => m.userId === (currentMeeting.updatedById || currentMeeting.createdById)
                        ).userName ||
                        members.find(
                          m => m.userId === (currentMeeting.updatedById || currentMeeting.createdById)
                        ).email}{" "}
                      - {helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(currentMeeting.updatedDate)} -{" "}
                      {helper.RETURN_CUSTOMDATEFORMATFORTIME(currentMeeting.updatedDate)}
                    </>
                  ) : currentMeeting.isDeleted ? (
                    `Archived by: ${this.getArchivedUserName()}
                     - ${helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(
                      currentMeeting.updatedDate
                    )} - ${helper.RETURN_CUSTOMDATEFORMATFORTIME(currentMeeting.updatedDate)}`
                  ) : isReviewNow &&
                    meetingPermission.meetingDetail.canPublishMeeting.cando ? (
                    <DefaultButton
                      onClick={this.publishMeeting}
                      text="PUBLISH"
                      buttonType="Plain"
                    />
                  ) : (
                    <div
                      style={{
                        paddingRight: 20,
                        borderRight: `1px solid ${theme.palette.border.lightBorder}`,
                      }}>
                      {meetingPermission.meetingDetail.canSubmitMeeting.cando &&
                        currentMeeting.isSubmitted == false ? (
                        <DefaultButton
                          onClick={this.reviewMeeting}
                          text={
                            <FormattedMessage
                              id="meeting.confirmations-dialog.submit-for-review.button.label"
                              defaultMessage="SUBMIT FOR REVIEW"
                            />
                          }
                          buttonType="Plain"
                          style={{
                            marginRight: 20,
                            backgroundColor: theme.palette.background.orange,
                          }}
                        />
                      ) : null}
                      {currentMeeting.status === "Published" && (
                        <DefaultButton
                          onClick={this.unPublishMeeting}
                          text="UN PUBLISH"
                          buttonType="Plain"
                        />
                      )}
                      {meetingPermission.meetingDetail.canPublishMeeting.cando &&
                        currentMeeting.status !== "Published" ? (
                        <DefaultButton
                          onClick={this.publishMeeting}
                          text={
                            <FormattedMessage
                              id="meeting.confirmations-dialog.publish-mom.button.label"
                              defaultMessage="PUBLISH"
                            />
                          }
                          buttonType="Publish"
                        />
                      ) : null}
                    </div>
                  )}
                  {((isZoomLinked && !currentMeeting.zoomMeetingId) ||
                    (isTeamLinked && !currentMeeting.teamMeetingId) ||
                    (isGoogleLinked && !currentMeeting.googleMeetingId)) &&
                    !isArchivedSelected &&
                    !readOnly ? (
                    <VideoMoreAction
                      item={currentMeeting}
                      onZoom={this.onZoomHandler}
                      onTeam={this.onMicrosoftTeamHandler}
                      onGoogle={this.onGoogleMeetHandler}
                    >

                    </VideoMoreAction>
                  ) : null}

                  {currentMeeting.status !== "Published" ? null : (
                    <IconButton btnType="transparent" onClick={this.openMinutesOfMeeting}>
                      <EmailIcon htmlColor={theme.palette.secondary.medDark} />
                    </IconButton>
                  )}
                  {/* repete meeting button */}
                  {teamCanView("repeatMeetingAccess") && !isArchivedSelected && !readOnly ? (
                    <CustomIconButton
                      onClick={this.repeatDropdownOpen}
                      btnType="filledWhite"
                      style={{
                        borderRadius: "50%",
                        border: "none",
                        marginLeft: "10px",
                      }}
                      buttonRef={node => {
                        this.anchorEl = node;
                      }}
                      disabled={
                        !meetingPermission.meetingDetail.repeatMeeting.isAllowAdd
                      }
                    // disabled={false}
                    >
                      <SvgIcon
                        viewBox="0 0 14 12.438"
                        htmlColor={
                          currentMeeting.meetingSchedule
                            ? theme.palette.primary.light
                            : theme.palette.secondary.medDark
                        }
                        className={classes.recurrenceIcon}>
                        <RecurrenceIcon />
                      </SvgIcon>
                    </CustomIconButton>
                  ) : null}

                  <IconButton btnType="transparent" onClick={this.props.closeMeetingDetailsPopUp}>
                    <CloseIcon htmlColor={theme.palette.secondary.medDark} />
                  </IconButton>
                </div>
              </Grid>
            </Grid>
          </Grid>
        </DialogTitle>

        <DialogContent classes={{ root: classes.defaultDialogContent }}>
          {/* <div style={{display: 'flex'}}>           */}
          <div style={{ flex: 1, padding: "20px 0 0 20px" }}>
            <Scrollbars autoHide autoHeightMax={250}>
              <Agenda
                agenda={agenda}
                agendaArray={agendaArray}
                isError={agendaError}
                currentMeeting={currentMeeting}
                classes={classes}
                theme={theme}
                inputAction={this.handleInput}
                saveAction={this.saveAction}
                updateAction={this.updateAction}
                deleteAction={this.deleteAction}
                meetingPer={meetingPermission}
              />
              <DiscussionNotes
                discussion={discussion}
                discussionArray={discussionArray}
                isError={discussionError}
                currentMeeting={currentMeeting}
                deleteAction={this.deleteAction}
                members={members}
                classes={classes}
                theme={theme}
                inputAction={this.handleInput}
                saveAction={this.saveAction}
                updateAction={this.updateAction}
                meetingPer={meetingPermission}
              />
              <FollowUp
                followUp={followUp}
                followUpArray={followUpArray}
                isError={followUpError}
                members={members}
                classes={classes}
                theme={theme}
                currentMeeting={currentMeeting}
                inputAction={this.handleInput}
                saveAction={this.saveAction}
                updateAction={this.updateAction}
                deleteAction={this.deleteAction}
                convertFollowupToTask={this.convertFollowupToTask}
                meetingPer={meetingPermission}
              />
              <Decision
                decision={decision}
                decisionArray={decisionArray}
                isError={decisionError}
                members={members}
                currentMeeting={currentMeeting}
                classes={classes}
                theme={theme}
                inputAction={this.handleInput}
                saveAction={this.saveAction}
                updateAction={this.updateAction}
                deleteAction={this.deleteAction}
                meetingPer={meetingPermission}
              />
            </Scrollbars>

            <ActionConfirmation
              open={isPublished}
              closeAction={this.publishMeetingClose}
              cancelBtnText={
                <FormattedMessage
                  id="meeting.confirmations-dialog.publish-mom.button-no.label"
                  defaultMessage="No"
                />
              }
              successBtnText={
                <FormattedMessage
                  id="meeting.confirmations-dialog.publish-mom.button-yes.label"
                  defaultMessage="Yes"
                />
              }
              alignment="center"
              headingText={
                <FormattedMessage
                  id="meeting.confirmations-dialog.publish-mom.title"
                  defaultMessage="Publish Minutes of Meeting"
                />
              }
              iconType="archive1"
              msgText={
                <FormattedMessage
                  id="meeting.confirmations-dialog.publish-mom.label"
                  defaultMessage="Are you sure you want to publish minutes of meeting?"></FormattedMessage>
              }
              note={
                <FormattedMessage
                  id="meeting.confirmations-dialog.publish-mom.note.label"
                  defaultMessage="You cannot edit meeting once minutes of meeting is published."></FormattedMessage>
              }
              successAction={this.publishMeetingAfterConfirmation}
              btnQuery={publishBtnQuery}
            />

            <ActionConfirmation
              open={isUnpublished}
              closeAction={this.unPublishMeetingClose}
              cancelBtnText="Cancel"
              successBtnText="UnPublish Meeting"
              alignment="center"
              headingText="UnPublish"
              iconType="archive"
              msgText={`Are you sure you want to unpublish minutes of meeting?`}
              successAction={this.unPublishMeetingAfterConfirmation}
              btnQuery={unpublishBtnQuery}
            />

            <MomEmailDialog
              open={showEmailPopup}
              theme={theme}
              closeAction={this.closeEmailPopup}
              currentObject={currentMeeting}
              showSnackBar={this.showSnackBar}
            />

            <ActionConfirmation
              open={isReview}
              closeAction={this.reviewMeetingClose}
              cancelBtnText={
                <FormattedMessage
                  id="meeting.confirmations-dialog.submit-for-review.button-no.label"
                  defaultMessage="No"
                />
              }
              successBtnText={
                <FormattedMessage
                  id="meeting.confirmations-dialog.submit-for-review.button-yes.label"
                  defaultMessage="Yes"
                />
              }
              alignment="center"
              headingText={
                <FormattedMessage
                  id="meeting.confirmations-dialog.submit-for-review.title"
                  defaultMessage="Submit for Review"
                />
              }
              iconType="archive"
              msgText={
                <FormattedMessage
                  id="meeting.confirmations-dialog.submit-for-review.label"
                  defaultMessage="Are you sure you want to submit minutes of meeting for review?"></FormattedMessage>
              }
              note={
                <FormattedMessage
                  id="meeting.confirmations-dialog.submit-for-review.note.label"
                  defaultMessage="Minutes of meeting will be submitted for review to all participants via email."></FormattedMessage>
              }
              successAction={this.reviewMeetingAfterConfirmation}
              btnQuery={submitReviewBtnQuery}
            />
            {removeAttend && (
              <ActionConfirmation
                open={removeAttend}
                closeAction={this.removeAttendClose}
                cancelBtnText={intl.formatMessage({
                  id: "common.action.cancel.label",
                  defaultMessage: "Cancel",
                })}
                successBtnText={intl.formatMessage({
                  id: "workspace-settings.user-management.add-members.remove-user.remove",
                  defaultMessage: "Remove",
                })}
                alignment="center"
                headingText={
                  <FormattedMessage
                    id="meeting.detail-dialog.remove-participant.title"
                    defaultMessage="Remove Participant"
                  />
                }
                iconType="none"
                msgText=""
                successBtnType="danger"
                successAction={this.removeAttendAfterConfirmation}
                btnQuery={submitReviewBtnQuery}>
                <CustomAvatar
                  otherMember={{
                    imageUrl: removeAttendItem.imageUrl,
                    fullName: removeAttendItem.fullName,
                    lastName: "",
                    email: removeAttendItem.email,
                    isOnline: removeAttendItem.isOnline,
                    isOwner: removeAttendItem.isOwner,
                  }}
                  size="small"
                  disableCard={true}
                />
                <Typography
                  variant="h5"
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    marginTop: 20,
                  }}>
                  {" "}
                  <FormattedMessage
                    id="workspace-settings.user-management.add-members.remove-user.message"
                    defaultMessage={`Are you sure you want to remove "${removeAttendItem.fullName}" from this meeting?`}
                    values={{ name: removeAttendItem.fullName }}
                  />
                </Typography>
              </ActionConfirmation>
            )}
          </div>
          {isArchivedSelected ? <div className={classes.disablePanel}></div> : null}
          <div className={classes.panels}>
            <Scrollbars autoHide={false}>
              {/* {editMeetingDetails && ( */}
              <div className={classes.panel}>
                <div className={classes.panelHeader} style={{ borderTop: 0 }}>
                  <div className={classes.panelHeaderCnt}>
                    <IconButton btnType="transparent" onClick={this.handleMeetingExpandClick}>
                      {meetingExpanded ? (
                        <UpArrow htmlColor={theme.palette.secondary.medDark} />
                      ) : (
                        <DropdownArrow htmlColor={theme.palette.secondary.medDark} />
                      )}
                    </IconButton>
                    <span style={{ marginLeft: 5, fontSize: "16px" }}>
                      <FormattedMessage
                        id="meeting.detail-dialog.title"
                        defaultMessage="Meeting Details"
                      />
                    </span>
                  </div>
                  {isChange && (
                    <div>
                      {/* <DefaultButton
                                text="Discard Changes"
                                buttonType="Transparent"
                                style={{ marginRight: 20 }}
                                onClick={this.handleCancel}
                              /> */}
                      <DefaultButton
                        onClick={this.handleSave}
                        text="Save"
                        buttonType="Plain"
                        disabled={saveDetails}
                      />
                    </div>
                  )}
                </div>
                {meetingExpanded && (
                  <div
                    className={panelBodyStatus.join(" ")}
                    style={{ padding: "10px 25px 0px 25px" }}>
                    <Grid
                      container
                      direction="row"
                      justify="flex-end"
                      alignItems="center"
                      spacing={2}>
                      <Grid item xs={12}>
                        <StaticDatePicker
                          label={
                            <FormattedMessage
                              id="meeting.detail-dialog.date.label"
                              defaultMessage="Date"
                            />
                          }
                          placeholder={
                            <FormattedMessage
                              id="meeting.detail-dialog.date.placeholder"
                              defaultMessage="Select Date"
                            />
                          }
                          isInput={true}
                          selectedSaveDate={this.selectedSaveDate}
                          isCreation={true}
                          data={currentMeeting}
                          isDisabled={
                            !meetingPermission.meetingDetail.meetingDate.isAllowEdit
                          }
                          margin={"20px 0px 0px 0px"}
                        />
                      </Grid>

                      <Grid item xs={6}>
                        <TimeSelect
                          clearSelect={this.clearSelect}
                          updateTime={this.updateTime}
                          label={
                            <FormattedMessage
                              id="meeting.detail-dialog.time.label"
                              defaultMessage="Time"
                            />
                          }
                          hours={timehours}
                          mins={timemins}
                          timeFormat={timeFormat}
                          style={{ marginLeft: 0, width: "auto" }}
                          labelAlign="top"
                          isDisabled={
                            !meetingPermission.meetingDetail.canEditMeetingTime
                              .isAllowEdit
                          }
                        />
                      </Grid>
                      <Grid item xs={6}>
                        <div className={classes.durationInputCnt} style={{ marginLeft: 0 }}>
                          <DurationInput
                            label={
                              <FormattedMessage
                                id="meeting.detail-dialog.duration.label"
                                defaultMessage="Duration"
                              />
                            }
                            updateTime={this.handleDurationChange}
                            hours={hours}
                            mins={mins}
                            date={currentMeeting.startDate}
                            data={currentMeeting}
                            error={false}
                            view="meetingList"
                            requiredInput={requiredInputDuration}
                            clearSelect={this.clearSelect}
                            disabled={
                              !meetingPermission.meetingDetail.meetingDuration
                                .isAllowEdit
                            }
                            styles={{
                              marginTop: 6,
                              marginBottom: 0
                            }}
                            hourClock={24}
                          />
                        </div>
                      </Grid>

                      <Grid item xs={teamCanView("projectAccess") ? 6 : 12}>
                        <div>
                          <SelectSearchDropdown
                            data={() => {
                              return this.generateTaskDropdownData(currentMeeting);
                            }}
                            styles={{
                              margin: 0,
                              marginRight: 20,
                              flex: 1,
                              marginTop: 20,
                              marginBottom: 10,
                            }}
                            isClearable={true}
                            selectClear={this.handleClearTask}
                            label={
                              <FormattedMessage
                                id="meeting.detail-dialog.task.label"
                                defaultMessage="Task"
                              />
                            }
                            selectChange={this.updateTask}
                            type="id"
                            selectedValue={this.getSelectedTask(currentMeeting)}
                            placeholder={""}
                            isMulti={false}
                            isDisabled={
                              !meetingPermission.meetingDetail.meetingTask
                                .isAllowEdit
                            }
                          />
                        </div>
                      </Grid>
                      {teamCanView("projectAccess") && (
                        <Grid item xs={6}>
                          <div>
                            <SelectSearchDropdown
                              data={() => {
                                return this.generateProjectDropdownData(currentMeeting);
                              }}
                              styles={{
                                margin: 0,
                                marginRight: 20,
                                flex: 1,
                                marginTop: 20,
                                marginBottom: 10,
                              }}
                              isClearable={!ProjectMandatory()}
                              selectClear={this.handleClearProject}
                              label={"Project"}
                              selectChange={this.updateProject}
                              type="id"
                              selectedValue={this.getSelectedProject(currentMeeting)}
                              placeholder={""}
                              isMulti={false}
                              isDisabled={
                                !meetingPermission.meetingDetail.meetingProject.isAllowEdit ||
                                isArchivedSelected
                              }
                            />
                          </div>
                        </Grid>
                      )}

                      <Grid item xs={12}>
                        {currentMeeting.zoomMeetingId ? (
                          <>
                            <div style={{ marginBottom: 10, marginLeft: 1 }}>
                              <Typography variant="h6" classes={{ h6: classes.zoomLabel }}>
                                Zoom Location
                              </Typography>
                              <div className={classes.zoomLink}>
                                <a href={zoomMeetingUrl} target="_blank">
                                  <span
                                    style={{
                                      display: "flex",
                                      overflow: "hidden",
                                    }}>
                                    {zoomMeetingUrl}
                                  </span>
                                </a>
                              </div>
                            </div>
                            <ZoomDescription
                              zoomDescription={currentMeeting.descriptionZoom}
                              createrName={createrName}></ZoomDescription>
                          </>
                        ) : null}
                        {currentMeeting.teamMeetingId ? (
                          <>
                            <div style={{ marginBottom: 10, marginLeft: 1 }}>
                              <Typography variant="h6" classes={{ h6: classes.zoomLabel }}>
                                Microsoft Teams Location
                              </Typography>
                              <div className={classes.zoomLink}>
                                <a href={teamMeetingUrl} target="_blank">
                                  <span
                                    style={{
                                      display: "flex",
                                      overflow: "hidden",
                                    }}>
                                    {teamMeetingUrl}
                                  </span>
                                </a>
                              </div>
                            </div>
                            <MicrosoftTeamDescription
                              teamDescription={currentMeeting.descriptionTeam}
                              createrName={createrName}></MicrosoftTeamDescription>
                          </>
                        ) : null}
                        {currentMeeting.googleMeetingId ? (
                          <>
                            <div style={{ marginBottom: 10, marginLeft: 1 }}>
                              <Typography variant="h6" classes={{ h6: classes.zoomLabel }}>
                                Google Meet Location
                              </Typography>
                              <div className={classes.zoomLink}>
                                <a href={googleMeetUrl} target="_blank">
                                  <span
                                    style={{
                                      display: "flex",
                                      overflow: "hidden",
                                    }}>
                                    {googleMeetUrl}
                                  </span>
                                </a>
                              </div>
                            </div>
                            <GoogleMeetDescription
                              meetDescription={currentMeeting.descriptionGoogleMeet}
                              createrName={createrName}></GoogleMeetDescription>
                          </>
                        ) : null}
                        {!currentMeeting.zoomMeetingId && !currentMeeting.teamMeetingId && !currentMeeting.googleMeetingId ? (
                          <DefaultTextField
                            label={intl.formatMessage({
                              id: "meeting.detail-dialog.location.label",
                              defaultMessage: "Location",
                            })}
                            fullWidth={true}
                            error={false}
                            errorState={locationError}
                            defaultProps={{
                              id: "location",
                              type: "text",
                              onChange: e => this.handleLocationInput(e, "meetingLocation"),
                              value: meetingLocation,
                              placeholder: intl.formatMessage({
                                id: "meeting.detail-dialog.location.placeholder",
                                defaultMessage: "Enter location",
                              }),
                              inputProps: { maxLength: 250 },
                              disabled: !meetingPermission.meetingDetail.meetingLocation
                                .isAllowEdit,
                            }}
                            formControlStyles={{
                              marginTop: 10,
                            }}
                          />
                        ) : null}
                      </Grid>
                    </Grid>
                  </div>
                )}
              </div>
              <div className={classes.panel}>
                <div className={classes.panelHeader}>
                  <div className={classes.panelHeaderCnt}>
                    <IconButton btnType="transparent" onClick={this.handleParticipantsExpandClick}>
                      {participantsExpanded ? (
                        <UpArrow htmlColor={theme.palette.secondary.medDark} />
                      ) : (
                        <DropdownArrow htmlColor={theme.palette.secondary.medDark} />
                      )}
                    </IconButton>
                    <span style={{ marginLeft: 5, fontSize: "16px" }}>
                      {
                        <FormattedMessage
                          id="meeting.detail-dialog.participants.label"
                          defaultMessage="Participants"
                        />
                      }
                    </span>
                  </div>
                  <div style={{ marginRight: 20 }}>
                    {meetingPermission.meetingDetail.meetingAttendee.isAllowAdd &&
                      (participantAdding ? (
                        <div className={`loader ${classes.miniLoader}`}></div>
                      ) : (
                        <AssigneeSelectDropdown
                          assignedTo={this.getMemberDropdownData()}
                          updateAction={this.updateParticipants}
                          btnText={
                            <FormattedMessage
                              id="meeting.detail-dialog.participants.add-button.label"
                              defaultMessage="Add Participants"
                            />
                          }
                          // isArchivedSelected={isArchivedSelected}
                          obj={currentMeeting}
                          // closeIssueDetailsPopUp={this.props.closeIssueDetailsPopUp}
                          disabled={currentMeeting.status == "Published" || currentMeeting.status == "Cancelled"}
                        />
                      ))}

                    {/* <MemberListMenu
                    id={currentMeeting.meetingId}
                    assigneeList={currentMeeting.assigneeLists || []}
                    MenuData={currentMeeting}
                    placementType="top-start"
                    isUpdated={this.props.isUpdated}
                    viewType="meeting"
                    checkAssignee={true}
                    checkedType="multi"
                    minimumSelections={1}
                    listType="assignee"
                    keyType=""
                    showListOnly={
                      readOnly ||
                      (permission && !permission.meetingAttendee.edit)
                    }
                    isArchivedSelected={isArchivedSelected}
                    mode='parentList'
                    sendList={this.assignedListHandler}
                    disabled={currentMeeting.status == 'Published' ? true : null}
                  /> */}
                  </div>
                </div>
                {participantsExpanded && (
                  <div className={panelBodyStatus.join(" ")} style={{ padding: "10px 5px" }}>
                    <ul className={classes.attendenceList}>
                      {/* <Scrollbars autoHide={false} autoHeight autoHeightMin={0} autoHeightMax={400}> */}
                      {currentMeeting.attendeeList &&
                        currentMeeting.attendeeList.map(userId => {
                          let item = wsMembers.find(m => m.userId == userId);
                          let attendee =
                            currentMeeting.attendees.find(x => x.attendeeId == userId) || {};
                          let attendence = attendee.isPresent ? "present" : "absent";
                          return (
                            <li>
                              <div className={classes.attendListDiv}>
                                <div
                                  style={{
                                    width: "90%",
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "space-between",
                                  }}>
                                  <div
                                    style={{
                                      display: "flex",
                                      alignItems: "center",
                                    }}>
                                    <CustomAvatar
                                      otherMember={{
                                        imageUrl: item.imageUrl,
                                        fullName: item.fullName,
                                        lastName: "",
                                        email: item.email,
                                        isOnline: item.isOnline,
                                        isOwner: item.isOwner,
                                      }}
                                      size="small"
                                      disableCard={true}
                                    />
                                    <div style={{ marginLeft: 10 }}>
                                      <Typography
                                        variant="h6"
                                        style={{
                                          fontWeight: theme.typography.fontWeightMedium,
                                          overflow: "hidden",
                                          whiteSpace: "nowrap",
                                          textOverflow: "ellipsis",
                                          maxWidth: "160px",
                                        }}
                                        title={item.fullName}>
                                        {item.fullName}
                                      </Typography>
                                      <Typography
                                        variant="caption"
                                        style={{
                                          fontSize: "11px",
                                          overflow: "hidden",
                                          whiteSpace: "nowrap",
                                          textOverflow: "ellipsis",
                                          maxWidth: "160px ",
                                        }}
                                        title={item.email}>
                                        {item.email}
                                      </Typography>
                                    </div>
                                  </div>
                                  {!item.isDeleted && (
                                    <div>
                                      <ToggleButtonGroup
                                        size="small"
                                        exclusive
                                        onChange={(event, value) =>
                                          this.attendenceHandle(event, value, item)
                                        }
                                        value={attendence}
                                        classes={{ root: classes.toggleBtnGroup }}>
                                        <ToggleButton
                                          key={1}
                                          value="present"
                                          classes={{
                                            root: classes.toggleButton,
                                            selected: classes.toggleButtonSelected,
                                          }}
                                          disabled={
                                            !meetingPermission.meetingDetail
                                              .meetingAttendee.isAllowAdd
                                          }>
                                          <FormattedMessage
                                            id="meeting.detail-dialog.participants.present-button.label"
                                            defaultMessage="Present"
                                          />
                                        </ToggleButton>
                                        <ToggleButton
                                          key={2}
                                          value="absent"
                                          classes={{
                                            root: classes.toggleButton,
                                            selected: classes.toggleButtonSelected,
                                          }}
                                          disabled={
                                            !meetingPermission.meetingDetail
                                              .meetingAttendee.isAllowAdd
                                          }>
                                          <FormattedMessage
                                            id="meeting.detail-dialog.participants.absent.label"
                                            defaultMessage="Absent"
                                          />
                                        </ToggleButton>
                                      </ToggleButtonGroup>
                                    </div>
                                  )}
                                </div>
                                {currentMeeting.createdById != item.userId &&
                                  meetingPermission.meetingDetail.meetingAttendee
                                    .isAllowDelete /** if user have no persion to delete assignee then remove icon would not be shown */ && (
                                    <CustomIconButton
                                      btnType="transparent"
                                      onClick={() => this.removeAttendenceHandle(item)}
                                      style={{ padding: 0 }}
                                      disabled={false}>
                                      <CancelIcon htmlColor={theme.palette.secondary.light} />
                                    </CustomIconButton>
                                  )}
                              </div>
                            </li>
                          );
                        })}
                      {/* </Scrollbars> */}
                    </ul>
                    {/* <div className={classes.publishBtn}></div> */}
                    {/* <div></div> */}
                  </div>
                )}
              </div>
            </Scrollbars>
          </div>
          {/* </div> */}
          {unArchiveFlag ? (
            <ActionConfirmation
              open={unArchiveFlag}
              closeAction={this.handleDialogClose}
              cancelBtnText="Cancel"
              successBtnText="Unarchive"
              alignment="center"
              iconType="unarchive"
              headingText="Unarchive"
              msgText={`Are you sure you want to unarchive this meeting?`}
              successAction={this.handleUnArchived}
              btnQuery={unarchiveBtnQuery}
            />
          ) : null}
        </DialogContent>
        <div className={classes.meetingDetailsBottomMsg}>
          <Typography variant="body2">
            {currentMeeting.status === "UpComing"
              ? `Scheduled by: ${members.find(
                m => m.userId === (currentMeeting.updatedById || currentMeeting.createdById)
              ).fullName ||
              members.find(
                m => m.userId === (currentMeeting.updatedById || currentMeeting.createdById)
              ).userName ||
              members.find(
                m => m.userId === (currentMeeting.updatedById || currentMeeting.createdById)
              ).email} - ${helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(
                currentMeeting.updatedDate
              )} - ${helper.RETURN_CUSTOMDATEFORMATFORTIME(currentMeeting.updatedDate)}`
              : null}
            {isReviewNow
              ? `Documented by: ${members.find(
                m => m.userId === (currentMeeting.updatedById || currentMeeting.createdById)
              ).fullName ||
              members.find(
                m => m.userId === (currentMeeting.updatedById || currentMeeting.createdById)
              ).userName ||
              members.find(
                m => m.userId === (currentMeeting.updatedById || currentMeeting.createdById)
              ).email} - ${helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(
                currentMeeting.updatedDate
              )} - ${helper.RETURN_CUSTOMDATEFORMATFORTIME(currentMeeting.updatedDate)}`
              : null}
            {isPublishedNow
              ? `This meeting took place on: ${helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(
                currentMeeting.startDateString
              )}   ${currentMeeting.startTime
                ? `from ${currentMeeting.startTime} till ${currentMeeting.endTime}`
                : ""
              }`
              : null}
            {currentMeeting.status === "Cancelled"
              ? `Cancelled by: ${members.find(
                m => m.userId === (currentMeeting.updatedById || currentMeeting.createdById)
              ).fullName ||
              members.find(
                m => m.userId === (currentMeeting.updatedById || currentMeeting.createdById)
              ).userName ||
              members.find(
                m => m.userId === (currentMeeting.updatedById || currentMeeting.createdById)
              ).email} - ${helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(
                currentMeeting.updatedDate
              )} - ${helper.RETURN_CUSTOMDATEFORMATFORTIME(currentMeeting.updatedDate)}`
              : null}
          </Typography>
        </div>
      </Dialog>
    );
  }
}

const mapStateToProps = state => {
  return {
    profileState: state.profile,
    tasks: state.tasks.data,
    projects: state.projects.data,
    wsMembers: state.profile.data.member.allMembers,
    isZoomLinked: state.profile.data.isZoomLinked,
    isTeamLinked: state.profile.data.isTeamLinked,
    isGoogleLinked: state.profile.data.isGoogleLinked,
    workspaceMeetingPer: state.workspacePermissions.data.meeting,
    permissionObject: ProjectPermissionSelector(state)
  };
};

export default compose(
  withRouter,
  withSnackbar,
  injectIntl,
  withStyles(taskDetailStyles, { withTheme: true }),
  connect(mapStateToProps, {
    UpdateMeeting,
    updateMeetingData,
    UpdateMeetingFollowupAction,
    InReviewMeeting,
    PublishMeeting,
    UnPublishMeeting,
    UpdateAgenda,
    DeleteDiscussionPoint,
    RemoveMeetingDecision,
    DeleteMeetingAgenda,
    UpdateMeetingFollowUpAction,
    DeleteMeetingFollowUpAction,
    UpdateMeetingDecision,
    UnArchiveMeeting,
    createTaskFromFollowup,
  })
)(MeetingDetails);
