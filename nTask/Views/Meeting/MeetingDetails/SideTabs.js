import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import taskDetailsStyles from "./styles";
import CommentsTab from "./CommentsTab";
import { Scrollbars } from "react-custom-scrollbars";

class SideTabs extends React.Component {
  state = {
    value: 0
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { classes } = this.props;
    const { value } = this.state;

    return (
      <Fragment>
        <Tabs
          value={value}
          onChange={this.handleChange}
          variant="fullWidth"
          classes={{ root: classes.TabsRoot, indicator: classes.tabIndicator }}
        >
          <Tab
            disableRipple={true}
            classes={{root: classes.tab,  wrapper:  classes.tabLabelCnt }}
            label="Comments"
          />
          <Tab
            disableRipple={true}
            classes={{ root: classes.tab, wrapper:  classes.tabLabelCnt }}
            label="Meetings"
          />
          <Tab
            disableRipple={true}
            classes={{ root: classes.tab, wrapper:  classes.tabLabelCnt }}
            label="Issues"
          />
          <Tab
            disableRipple={true}
            classes={{ root: classes.tab, wrapper:  classes.tabLabelCnt }}
            label="Risks"
          />
        </Tabs>
        {value === 0 && (
          <div className={classes.TabContentCnt}>
            <CommentsTab currentTask={this.props.taskData} />
          </div>
        )}
        {value === 1 && <div className={classes.TabContentCnt}>Item Two</div>}
        {value === 2 && <div className={classes.TabContentCnt}>Item Three</div>}
        {value === 3 && <div className={classes.TabContentCnt}>{JSON.stringify(this.props.taskData.risks, null, 3)}</div>}
      </Fragment>
    );
  }
}

export default withStyles(taskDetailsStyles)(SideTabs);
