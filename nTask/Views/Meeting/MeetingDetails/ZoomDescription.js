import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import classes from "./styles";
import Typography from "@material-ui/core/Typography";

class ZoomDescription extends Component {
    constructor(props){
        super();
    }
    render(){
        const { classes, zoomDescription, createrName} = this.props;
        return(
            <Fragment>
                <div style={{marginLeft: 1 }}>
                    <Typography
                        variant="h6"
                        classes={{h6: classes.zoomLabel}}
                        >
                       Zoom Description
                    </Typography>
                </div>
                <div className={classes.meetingDescriptionCnt}>
                    <Typography variant="h6" >
                        {createrName} is inviting you to a scheduled Zoom meeting.
                    </Typography>
                    <div className={classes.joinSection}>
                        <Typography variant="h6" style={{ fontWeight: 700 }}>
                            Join Zoom Meeting
                        </Typography>
                        <a href={zoomDescription.join_url} target="_blank" >{zoomDescription.join_url}</a>
                    </div>
                    <span className={classes.subSection}>
                        <Typography variant="h6" style={{ fontWeight: 700 }} >
                            Meeting ID:
                        </Typography>
                        <Typography variant="h6" classes={{h6: classes.subSecValue}}>
                            {zoomDescription.id}
                        </Typography>
                    </span>
                    <span className={classes.subSection}>
                        <Typography variant="h6" style={{ fontWeight: 700 }}>
                            Passcode:
                        </Typography>
                        <Typography variant="h6" classes={{h6: classes.subSecValue}}>
                        {zoomDescription.password}
                        </Typography>
                    </span>
                </div>
            </Fragment>
        );
    }
}
const mapStateToProps = state => {
    return {
    };
  };
export default compose(
    withRouter,
    connect(
      mapStateToProps,{}
    ),
    withStyles(classes, { withTheme: true })
  )(ZoomDescription);
