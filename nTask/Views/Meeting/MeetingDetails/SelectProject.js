import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { SaveProject } from "../../../redux/actions/projects";
import InputLabel from "@material-ui/core/InputLabel";
import Avatar from "@material-ui/core/Avatar";
import DoneIcon from "@material-ui/icons/Done";
import Select from 'react-select';
import { components } from "react-select";
import autoCompleteStyles from "../../../assets/jss/components/autoComplete";
import Grid from "@material-ui/core/Grid";
import DropdownArrow from "@material-ui/icons/ArrowDropDown"
import { UpdateTask, updateTaskData } from "../../../redux/actions/tasks";
import { UpdateCalenderTask } from "../../../redux/actions/calenderTasks";
class SelectProject extends Component {
  constructor(props) {
    super(props);
    this.state = {
      AssignedToLabel: false,
      AddToProjectLabel: false,
      value: [],
      projectOptions: [],
      AssignEmailError: false,
      taskTitle: undefined,
      selectedProjectOption: {
        Active: "true",
        value: "Select Project",
        label: "Select Project"
      }
    };
    this.handleSelectChange = this.handleSelectChange.bind(this);
  }
  componentDidMount() {
    let projectOptions =
      this.props.projectsState &&
        this.props.projectsState.data &&
        this.props.projectsState.data.length
        ? this.props.projectsState.data
        : [];
    let selectedProjectOption = {
      Active: "true",
      value: "Select Project",
      label: "Select Project"
    };
    if (projectOptions.length)
      projectOptions.forEach(function (obj) {
        obj.Active = "false";
        obj.value = obj.projectName;
        obj.label = obj.projectName;
      });
    if (this.props.projectId)
      selectedProjectOption = projectOptions.find(x => x.projectId === this.props.projectId)
    this.setState({ projectOptions, selectedProjectOption });
  }
  handleSelectChange(newValue, actionMeta) {
    this.setState({ selectedProjectOption: newValue }, () => {
      let updatedTask = this.props.taskData;
      const obj = {projectId : newValue.projectId}
      this.props.updateTaskData({task : updatedTask, obj}, response=>{
        response = response.data.CalenderDetails;
    });
    });
  }


  render() {
    const { classes, theme } = this.props;
    const DropdownIndicator = (props) => {
      return components.DropdownIndicator && (
        <components.DropdownIndicator {...props}>
          <DropdownArrow htmlColor={theme.palette.secondary.light} />
        </components.DropdownIndicator>
      );
    };
    // Styles of react-select autocomplete
    const customStyles = {
      control: (base, state) => ({
        ...base,
        background: "transparent",
        padding: "5.5px 10px 2px 10px",
        borderRadius: 4,
        borderColor: theme.palette.border.lightBorder,
        boxShadow: 'none',
        ":focus": {
          border: `1px solid ${theme.palette.border.darkBorder}`
        },
        ":hover": {
          border: `1px solid ${theme.palette.border.darkBorder}`
        }
      }),
      dropdownIndicator: (base, state) => ({
        padding: 0
      }),
      valueContainer: (base, state) => ({
        ...base,
        padding: "5px 0"
      })
    };
    const Option = props => {
     
      return (
        <components.Option {...props}>
          <Grid
            container
            direction="row"
            justify="flex-start"
            alignItems="center"
          >

            <Avatar classes={{ root: classes.dropDownAvatar }}>H</Avatar>

            <span>{props.children}</span>
          </Grid>
        </components.Option>
      );
    };

    return (
      <FormControl classes={{ root: classes.formControl }} fullWidth={true}>
        <InputLabel
          required
          htmlFor="AddToProject"
          classes={{
            root: classes.autoCompleteLabel,
            shrink: classes.shrinkLabel
          }}
          shrink={false}
        >
          Select Project
        </InputLabel>
        <Select
          styles={customStyles}

          onChange={this.handleSelectChange}
          inputId="AddToProject"
          components={{ DropdownIndicator, IndicatorSeparator: false }}
          options={this.state.projectOptions}
          value={this.state.selectedProjectOption}
          placeholder=""
        />
      </FormControl>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    projectsState: state.projects
  }
}

export default compose(
  withRouter,
  withStyles(autoCompleteStyles, {
    withTheme: true
  }),
  connect(
    mapStateToProps,
    { UpdateTask, UpdateCalenderTask, updateTaskData }
  )
)(SelectProject);