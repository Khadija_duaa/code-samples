import React, { Component, Fragment } from "react";
import DefaultTextField from "../../../components/Form/TextField";
import Typography from "@material-ui/core/Typography";
// import AgendaIcon from "@material-ui/icons/AssignmentOutlined";
import AddIcon from "@material-ui/icons/Add";
import RoundIcon from "@material-ui/icons/Brightness1";
import Hotkeys from "react-hot-keys";
import helper from "../../../helper";
import DeleteConfirmDialog from "../../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import CloseIcon from "@material-ui/icons/Close";
import IconButton from "../../../components/Buttons/IconButton";
import { getReadOnlyPermissions } from "../permissions";
// import AgendaIcon from "../../../assets/images/meeting_key_decisions.svg";
import { GetPermission } from "../../../components/permissions";
import { FormattedMessage, injectIntl } from "react-intl";
import SvgIcon from "@material-ui/core/SvgIcon";
import MeetingDecisionIcon from "../../../components/Icons/MeetingDecisionIcon";
class Decision extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDelete: false,
      currentObject: {},
      deleteBtnQuery: "",
      disableField: false
    };
    this.onAddDecision = this.onAddDecision.bind(this);
    this.onKeyDown = this.onKeyDown.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.handleEditBlur = this.handleEditBlur.bind(this);
    this.handleDecisionClick = this.handleDecisionClick.bind(this);
    this.onEditKeyDown = this.onEditKeyDown.bind(this);
    this.handleInput = this.handleInput.bind(this);
    this.deleteDecision = this.deleteDecision.bind(this);
    this.deleteDecisionAfterConfirmation = this.deleteDecisionAfterConfirmation.bind(this);
  }

  deleteDecisionAfterConfirmation() {
    const { currentObject } = this.state;
    const { currentMeeting } = this.props;
    this.setState({ deleteBtnQuery: "progress" }, () => {
      this.props.deleteAction(currentObject, "decision", currentMeeting, () => {
        this.setState({ deleteBtnQuery: "", isDelete: false });
      });
    });
  }
  handleDeleteDialogClose = () => {
    this.setState({ isDelete: false });
  };

  deleteDecision(event, value) {
    event.stopPropagation();
    this.setState({
      isDelete: true,
      currentObject: value,
    });
  }
  handleBlur(event, name) {
    this.setState({ addFollowup: false });
  }
  onAddDecision() {
    this.setState({ addFollowup: true });
  }
  onKeyDown(event) {
    const { decision } = this.props;
    if (event == "enter") {
      if (decision == "") this.props.saveAction("decision", {}, () => { });
      else this.setState({ disableField: true }, () => {
        this.props.saveAction("decision", {}, () => {
          this.setState({ disableField: false, addFollowup: false })
        });
      })
    } else if (event == "esc") {
      this.setState({ addFollowup: false });
    }
  }
  handleEditBlur(event, name) {
    this.setState({ [`editDecision${name}`]: false });
  }
  handleInput(event, name) {
    this.setState({ [name]: event.target.value });
  }
  onEditKeyDown(event, name, data) {
    if (event.key === "Enter") {
      if (data) {
        data.description = this.state[name];
      }
      this.setState({ [`editDecision${name}`]: false });
      this.props.updateAction(data, "decision", this.props.currentMeeting);
      // this.setState({ [`editDecision${name}`]: false });
    } else if (event.key == "Escape") {
      this.setState({ [`editDecision${name}`]: false });
    }
  }
  handleDecisionClick(event, value, name) {
    this.setState({ [name]: value, [`editDecision${name}`]: true });
  }
  render() {
    const {
      classes,
      theme,
      inputAction,
      decision,
      decisionArray,
      members,
      currentMeeting,
      isError,
      meetingPer,
    } = this.props;
    const { addFollowup, isDelete, currentObject, deleteBtnQuery, disableField } = this.state;
    const readOnly = getReadOnlyPermissions(currentMeeting);
    const deleteDecision = meetingPer.meetingDetail.meetingKeyDecisions.isAllowDelete;
    const edit = meetingPer.meetingDetail.meetingKeyDecisions.isAllowEdit;
    const add = meetingPer.meetingDetail.meetingKeyDecisions.isAllowAdd;
    const intl = this.props.intl;

    return (
      <Fragment>
        {!readOnly ? (
          <Fragment>
            <div className={`${classes.MeetingDetailsHeading} flex_center_start_row`}>
              {/* <img src={AgendaIcon} alt="Meeting Key Discussion" /> */}
              {/* <AgendaIcon htmlColor={theme.palette.primary.light} /> */}
              <SvgIcon
                viewBox="0 0 17.188 17.188"
                className={classes.meetingDetailsHeadingIcon}
              >
                <MeetingDecisionIcon />
              </SvgIcon>
              <Typography variant="h5" classes={{ root: classes.meetingDetailsHeadingText }}>
                <FormattedMessage
                  id="meeting.detail-dialog.key-decisions.label"
                  defaultMessage="Key Decisions"></FormattedMessage>
              </Typography>
            </div>
            {addFollowup && (add === undefined || add) ? (
              <Hotkeys keyName="enter,esc" onKeyDown={this.onKeyDown}>
                <div>
                  <DefaultTextField
                    fullWidth={true}
                    formControlStyles={{ marginBottom: 0, marginTop: 0 }}
                    errorState={isError}
                    errorMessage={isError}
                    noBorderRadius={true}
                    label={false}
                    defaultProps={{
                      type: "text",
                      onChange: event => inputAction(event, "decision"),
                      onBlur: event => this.handleBlur(event, "addDecision"),
                      onKeyDown: (event) => {
                        event.keyCode == 13 && this.onKeyDown("enter");
                      },
                      autoFocus: true,
                      value: decision,
                      placeholder: intl.formatMessage({
                        id: "meeting.detail-dialog.key-decisions.innerplaceholder",
                        defaultMessage: "Enter Key Decision",
                      }),
                      inputProps: {
                        maxLength: 250,
                      },
                      disabled: disableField
                    }}
                  />
                </div>
              </Hotkeys>
            ) : (
              <div
                onClick={this.onAddDecision}
                className={`${classes.addMeetingDetailBtn} flex_center_start_row`}>
                <AddIcon
                  htmlColor={theme.palette.text.primary}
                  className={classes.addMeetingDetailPlus}
                />
                <Typography variant="body1">
                  <FormattedMessage
                    id="meeting.detail-dialog.key-decisions.placeholder"
                    defaultMessage="Add Decision"
                  />
                </Typography>
              </div>
            )}

            <ul className={classes.addMeetingDetailList}>
              {decisionArray.map((x, i) => {
                return this.state[`editDecision${x.decisionId}`] ? (
                  <li className={classes.inputListItem} key={x.decisionId}>
                    <Hotkeys
                      keyName="enter,esc"
                      onKeyDown={event => this.onEditKeyDown(event, x.decisionId, x)}>
                      <DefaultTextField
                        fullWidth={true}
                        formControlStyles={{ marginBottom: 0, marginTop: 0 }}
                        error={false}
                        noBorderRadius={true}
                        label={false}
                        defaultProps={{
                          type: "text",
                          onChange: event => this.handleInput(event, x.decisionId),
                          onBlur: event => this.handleEditBlur(event, x.decisionId),
                          onKeyDown: event => this.onEditKeyDown(event, x.decisionId, x),
                          autoFocus: true,
                          value: this.state[x.decisionId],
                          placeholder: intl.formatMessage({
                            id: "meeting.detail-dialog.key-decisions.innerplaceholder",
                            defaultMessage: "Enter Decision",
                          }),
                        }}
                      />
                    </Hotkeys>
                  </li>
                ) : (
                  <li
                    className="flex_center_space_between_row"
                    onClick={event =>
                      (edit === undefined || edit) &&
                      this.handleDecisionClick(event, x.description, x.decisionId)
                    }
                    key={x.decisionId}>
                    <div>
                      <RoundIcon className={classes.listIcon} />
                      {x.description}
                      <Typography
                        classes={{
                          body2: classes.followUpActionDetails,
                        }}
                        variant="body2">
                        {`${members.find(m => m.userId === (x.updatedBy || x.createdBy)).fullName ||
                          members.find(m => m.userId === (x.updatedBy || x.createdBy)).userName ||
                          members.find(m => m.userId === (x.updatedBy || x.createdBy))
                            .email} - ${helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(
                              x.updatedDate
                            )} - ${helper.RETURN_CUSTOMDATEFORMATFORTIME(x.updatedDate)}`}
                      </Typography>
                    </div>
                    <div className={classes.CloseIconBtn}>
                      {deleteDecision ? (
                        <IconButton
                          btnType="smallFilledGray"
                          style={{ padding: 0 }}
                          onClick={event => {
                            this.deleteDecision(event, x);
                          }}>
                          <CloseIcon fontSize="small" htmlColor={theme.palette.common.white} />
                        </IconButton>
                      ) : null}
                    </div>
                  </li>
                );
              })}
            </ul>
            <DeleteConfirmDialog
              open={isDelete}
              closeAction={this.handleDeleteDialogClose}
              cancelBtnText={
                <FormattedMessage
                  id="common.action.cancel.label"
                  defaultMessage="Cancel"></FormattedMessage>
              }
              successBtnText={
                <FormattedMessage
                  id="meeting.detail-dialog.key-decisions.delete-confirmation.delete-button.label"
                  defaultMessage="Delete"></FormattedMessage>
              }
              alignment="center"
              headingText={
                <FormattedMessage
                  id="meeting.detail-dialog.key-decisions.delete-confirmation.title"
                  defaultMessage="Delete"></FormattedMessage>
              }
              successAction={this.deleteDecisionAfterConfirmation}
              msgText={
                <FormattedMessage
                  id="meeting.detail-dialog.key-decisions.delete-confirmation.label"
                  defaultMessage="Are you sure you want to delete this key decision point?"
                />
              }
              btnQuery={deleteBtnQuery}
            />
          </Fragment>
        ) : (
          <Fragment>
            <div className={`${classes.MeetingDetailsHeading} flex_center_start_row`}>
              {/* <img src={AgendaIcon} alt="Meeting Key Discussion" /> */}
              {/* <AgendaIcon htmlColor={theme.palette.primary.light} /> */}
              <SvgIcon
                viewBox="0 0 17.188 17.188"
                className={classes.meetingDetailsHeadingIcon}
              >
                <MeetingDecisionIcon />
              </SvgIcon>
              <Typography variant="h5" classes={{ root: classes.meetingDetailsHeadingText }}>
                <FormattedMessage
                  id="meeting.detail-dialog.key-decisions.label"
                  defaultMessage="Key Decisions"></FormattedMessage>
              </Typography>
            </div>
            <ul className={classes.addMeetingDetailList}>
              {decisionArray.map((x, i) => {
                return (
                  <li className="flex_center_space_between_row" key={x.decisionId}>
                    <div>
                      <RoundIcon className={classes.listIcon} />
                      {x.description}
                      <Typography
                        classes={{
                          body2: classes.followUpActionDetails,
                        }}
                        variant="body2">
                        {`${members.find(m => m.userId === (x.updatedBy || x.createdBy)).fullName ||
                          members.find(m => m.userId === (x.updatedBy || x.createdBy)).userName ||
                          members.find(m => m.userId === (x.updatedBy || x.createdBy))
                            .email} - ${helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(
                              x.updatedDate
                            )} - ${helper.RETURN_CUSTOMDATEFORMATFORTIME(x.updatedDate)}`}
                      </Typography>
                    </div>
                  </li>
                );
              })}
            </ul>
          </Fragment>
        )}
      </Fragment>
    );
  }
}

export default injectIntl(Decision);
