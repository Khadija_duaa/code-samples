import React, { Component, Fragment } from "react";
import { UpdateTask, updateTaskData } from "../../../redux/actions/tasks";
import { connect } from "react-redux";
import { Circle } from "rc-progress";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Grid from "@material-ui/core/Grid";
import CloseIcon from "@material-ui/icons/Close";
import IconButton from "../../../components/Buttons/IconButton";
import Typography from "@material-ui/core/Typography";
import DefaultTextField from "../../../components/Form/TextField";
import helper from "../../../helper";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import DragIndicator from "@material-ui/icons/DragIndicator";
import SvgIcon from "@material-ui/core/SvgIcon";
import CheckBoxIcon from "../../../components/Icons/CheckBoxIcon";

const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

class CheckList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checkListInput: "",
      checkList: [],
      taskData: {},
      checkListMouseOver: false
    };
    this.generateCheckList = this.generateCheckList.bind(this);
    this.handleClInputChange = this.handleClInputChange.bind(this);
    this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
    this.checkListItemDelete = this.checkListItemDelete.bind(this);
    this.onDragEnd = this.onDragEnd.bind(this);
    this.handleCheckListMouseOver = this.handleCheckListMouseOver.bind(this);
  }
  onDragEnd(result) {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    const items = reorder(
      this.state.checkList,
      result.source.index,
      result.destination.index
    );
    this.setState({
      checkList: items
    }, () => {
      let currentTask = this.props.taskData;
      const obj = {checkList : this.state.checkList};
      this.props.updateTaskData({task : currentTask, obj}, response=>{
        response = response.data.CalenderDetails;
          this.componentDidMount();
      });     
    });
  }
  checkListItemDelete(id) {
    // Event to delete checklist item
    let self = this;
    let checkList = this.state.checkList; //.filter((x) => x.id !== id);

    var index = this.state.checkList
      .map(x => {
        return x.id;
      })
      .indexOf(id);

    checkList.splice(index, 1);

    let updatedData = checkList.map((x, i) => {
      if (x.isDone) this.setState({ [i]: true });
      return {
        checkedDate: x.checkedDate,
        description: x.description,
        isDone: x.isDone,
        userId: x.userId
      };
    });
    let currentTask = this.props.taskData;
    const obj = {checkList : updatedData};
    this.setState({ checkList }, function () {
      self.props.updateTaskData({task : currentTask, obj}, response=>{
        response = response.data.CalenderDetails;
          self.props.UpdateCalenderTask(response);
          self.componentDidMount();
      });
    });
  }
  handleCheckListMouseOver() {
    this.setState({ checkListMouseOver: true });
  }
  static getDerivedStateFromProps(props, prevState) {
   // 
    let tasks = props.tasksState.data
      ? props.tasksState.data.filter(x => x.taskId === props.taskData.taskId)
      : [];
    let members =
      props.profileState && props.profileState.data && props.profileState.data.member
        ? props.profileState.data.member.allMembers
        : [];
    let completeDetails = tasks.map(x => {
      x.assigneeLists = members.filter(m => {
        return x.assigneeList.indexOf(m.userId) >= 0;
      });
      return x;
    });
    if (completeDetails && completeDetails.length)
      return { taskData: completeDetails[0] };
  }
  componentDidMount() {
    let members =
      this.props.profileState &&
        this.props.profileState.data &&
        this.props.profileState.data.member
        ? this.props.profileState.data.member.allMembers
        : [],
      userName = members.find(m => {
        return this.props.profileState.data.userId === m.userId;
      }).userName;
    let task = this.props.tasksState.data.find(
      x => x.taskId === this.props.taskData.taskId
    );

    let checkList = task.checkList.map((x, i) => {
      if (x.userId)
        userName = members.find(m => {
          return x.userId === m.userId;
        }).userName;
      return {
        id: i,
        value: x.description,
        description: x.description,
        isDone: x.isDone,
        checkedDate: x.checkedDate,
        userId: x.userId || this.props.profileState.data.userId,
        userName: userName
      };
    });
    this.setState({ taskData: this.props.taskData, checkList });
  }
  //Adding record in  Checklist on Enter key
  generateCheckList(event) {
   // 
    if (
      event.key === "Enter" &&
      !helper.RETURN_CECKSPACES(this.state.checkListInput)
    ) {
      let members =
        this.props.profileState &&
          this.props.profileState.data &&
          this.props.profileState.data.member
          ? this.props.profileState.data.member.allMembers
          : [];
      this.setState(
        prevState => ({
          checkList: prevState.checkList.concat({
            id: prevState.checkList.length,
            value: prevState.checkListInput,
            description: prevState.checkListInput,
            isDone: false,
            checkedDate: new Date().toISOString(),
            userId: this.props.profileState.data.userId,
            userName: members.find(m => {
              return this.props.profileState.data.userId === m.userId;
            }).userName
          }),
          checkListInput: ""
        }),
        () => {
          let self = this;
          let currentTask = this.props.taskData;
          const obj = {checkList : this.state.checkList};
          self.props.updateTaskData({task : currentTask, obj}, response=>{
              self.componentDidMount();
          });
        }
      );
    }
  }
  //handling checklist input field change by setting it's value to state
  handleClInputChange(event) {
    this.setState({ checkListInput: event.target.value });
  }
  //Checkbox toggle state
  handleCheckboxChange = name => event => {
    let self = this;
    let checkList = this.state.checkList.map(x => {
      if (name === x.id) {
        x.isDone = event.target.checked;
        x.checkedDate = new Date().toISOString();
      }
      return x;
    });
    let updatedData = checkList.map(x => {
      if (name === x.id) {
        return {
          checkedDate: new Date().toISOString(),
          description: x.description,
          isDone: event.target.checked,
          userId: x.userId
        };
      }
      return {
        checkedDate: x.checkedDate,
        description: x.description,
        isDone: x.isDone,
        userId: x.userId
      };
    });
    let currentTask = this.props.taskData;
    const obj = { checkList : updatedData}
    this.setState({ [name]: event.target.checked, checkList }, function () {
      self.props.updateTaskData({task : currentTask, obj}, response=>{
        self.componentDidMount();
    });
    });
  };
  render() {
    const { classes, theme } = this.props;
    const {
      checkListInput,
      checkList,
      taskData,
      checkListMouseOver
    } = this.state;
    return (
      <Fragment>
        <Grid item xs={12} classes={{ item: classes.checklistInputCnt }}>
          <div className={classes.clInputProgressCnt}>
            <DefaultTextField
              label="Checklist"
              fullWidth={true}
              formControlStyles={{ marginBottom: 0, marginRight: 20 }}
              error={false}
              defaultProps={{
                id: "firstNameInput",
                onChange: this.handleClInputChange,
                onKeyUp: this.generateCheckList,
                value: checkListInput,
                placeholder: "Add a checklist item"
              }}
            />

            <div style={{ width: 74, position: "relative" }}>
              <Circle
                percent={taskData.progress}
                strokeWidth="7"
                trailWidth=""
                trailColor="#dedede"
                strokeColor="#30d56e"
              />
              <span className={classes.clProgressValue}>
                {taskData.progress}%
              </span>
            </div>
          </div>
        </Grid>
        <Grid xs={12} item classes={{ item: classes.checklistCnt }}>
          <DragDropContext onDragEnd={this.onDragEnd}>
            <Droppable droppableId="droppable">
              {(provided, snapshot) => (
                <ul className={classes.checkList} ref={provided.innerRef}>
                  {checkList.map((list, index) => (
                    <Draggable
                      key={list.id}
                      draggableId={list.id}
                      index={index}
                    >
                      {(provided, snapshot) => (
                        <li
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          className={classes.checkListItem}
                        >
                          <Grid
                            container
                            direction="row"
                            justify="flex-start"
                            alignItems="center"
                          >
                            <span
                              {...provided.dragHandleProps}
                              className={classes.dragHandleCnt}
                            >
                              <DragIndicator
                                htmlColor={theme.palette.secondary.light}
                                className={classes.dragHandle}
                              />
                            </span>
                            <div className={classes.checkListItemInner}>
                              <FormControlLabel
                                classes={{ root: classes.checklistCheckboxCnt }}
                                control={
                                  <Checkbox
                                    checked={list.isDone}
                                    style={{ padding: "10px 10px 10px 0" }}
                                    onClick={this.handleCheckboxChange(list.id)}
                                    // classes={{
                                    //   root: classes.checkListCheckBox
                                    // }}
                                    value={list.value}
                                    checkedIcon={
                                      <SvgIcon
                                        viewBox="0 0 426.667 426.667"
                                        htmlColor={
                                          theme.palette.status.completed
                                        }
                                        classes={{ root: classes.checkedIcon }}
                                      >
                                        <CheckBoxIcon />
                                      </SvgIcon>
                                    }
                                    icon={
                                      <Fragment>
                                        <SvgIcon
                                          viewBox="0 0 426.667 426.667"
                                          htmlColor={
                                            theme.palette.background.items
                                          }
                                          classes={{
                                            root: classes.unCheckedIcon
                                          }}
                                        >
                                          <CheckBoxIcon />
                                        </SvgIcon>
                                        <span className={classes.emptyCheckbox}></span>
                                      </Fragment>
                                    }
                                    color="primary"
                                  />
                                }
                              />
                              <span
                                style={
                                  list.isDone
                                    ? { textDecoration: "line-through" }
                                    : null
                                }
                              >
                                {list.value}
                              </span>
                              {(list.isDone) ? (
                                <Typography
                                  classes={{
                                    body2: classes.checklistItemDetails
                                  }}
                                  variant="body2"
                                >
                                  - {list.userName} -{" "}
                                  {list.checkedDate
                                    ? helper.RETURN_CUSTOMDATEFORMATFORCHECKLIST(
                                      list.checkedDate
                                    )
                                    : helper.RETURN_CUSTOMDATEFORMATFORCHECKLIST(
                                      new Date().toISOString()
                                    )}
                                </Typography>
                              ) : null}
                            </div>
                          </Grid>
                          <div className={classes.CloseIconBtn}>
                            <IconButton
                              id={list.id}
                              btnType="smallFilledGray"
                              onClick={this.checkListItemDelete.bind(
                                this,
                                list.id
                              )}
                            >
                              <CloseIcon
                                fontSize="small"
                                htmlColor={theme.palette.common.white}
                              />
                            </IconButton>
                          </div>
                        </li>
                      )}
                    </Draggable>
                  ))}
                  {provided.placeholder}
                </ul>
              )}
            </Droppable>
          </DragDropContext>
        </Grid>
        {/* </Hotkeys> */}
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    tasksState: state.tasks,
    profileState: state.profile

  }
}

  
export default connect(
  mapStateToProps,
  { UpdateTask, updateTaskData }
)(CheckList);
