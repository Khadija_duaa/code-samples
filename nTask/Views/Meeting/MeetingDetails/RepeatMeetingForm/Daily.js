import React, { Component, Fragment } from "react";
import TimeInput from "../../../../components/DatePicker/TimeInput";
import StaticDatePicker from "../../../../components/DatePicker/StaticDatePicker";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormLabel from "@material-ui/core/FormLabel";
import FormControl from "@material-ui/core/FormControl";
import Grid from "@material-ui/core/Grid";
import SelectMeetingCount from "./SelectMeetingCount";
import MeetingOccurSelect from "./MeetingOccurSelect";
import Typography from "@material-ui/core/Typography";
import NotificationMessage from "../../../../components/NotificationMessages/NotificationMessages";
import CustomButton from "../../../../components/Buttons/CustomButton";
import helper from "../../../../helper";
import moment from "moment";
class Daily extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time: "",
      endValue: "EndBy",
      date: helper.RETURN_CUSTOMDATEFORMAT(moment()),
      advanceOption: false,
      meetingsCount: 0,
      advanceValue: "",
      requiredInput: false
    };
    this.handleTimeChange = this.handleTimeChange.bind(this);
    this.handleAdvanceOption = this.handleAdvanceOption.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.selectedSaveDate = this.selectedSaveDate.bind(this);
    this.handleMeetingChange = this.handleMeetingChange.bind(this);
    this.handleAdvanceMeetingChange = this.handleAdvanceMeetingChange.bind(
      this
    );
  }
  componentDidMount() {
    if (this.props.currentMeeting) {
      this.setState({
        endValue:
          this.props.currentMeeting.endBy === "Date" ? "EndBy" : "After",
        time: this.props.currentMeeting.startTime,
        date: helper.RETURN_CUSTOMDATEFORMAT(
          moment(this.props.currentMeeting.endDateString)
        )
      });
    }
  }

  handleMeetingChange(value) {
    this.setState({
      meetingsCount: this.state.endValue === "After" ? value.value || 10 : 0,
      requiredInput: false
    });
  }
  handleAdvanceMeetingChange(value) {
    this.setState({ advanceValue: value.value, requiredInput: false });
  }
  handleSubmit() {
    let flag = 0;
    if (!this.state.date) {
      this.setState({
        requiredInput: true
      });
      flag = 1;
    } else {
      this.setState({
        requiredInput: false
      });
    }
    if (
      !this.state.time ||
      this.state.time === "0:0 " ||
      this.state.time === "0:0 PM" ||
      this.state.time === "0:0 AM"
    ) {
      flag = 1;
      this.setState({
        requiredInput: true
      });
    } else {
      this.setState({
        requiredInput: false
      });
    }
    if (flag) {
      return;
    }
    this.props.saveData(this.state);
  }
  selectedSaveDate(date) {
    this.setState({
      date: helper.RETURN_CUSTOMDATEFORMAT(date),
      requiredInput: false
    });
  }
  handleChange = event => {
    this.setState({ endValue: event.target.value, requiredInput: false });
  };
  handleAdvanceOption() {
    this.setState(prevState => ({
      advanceOption: !prevState.advanceOption,
      requiredInput: false
    }));
  }
  handleTimeChange(time) {
    let timeData = time.toLowerCase().includes("am")
      ? time
      : time.toLowerCase().includes("pm")
      ? time
      : time + " AM";
    this.setState({ time: timeData, requiredInput: false });
  }
  render() {
    const {
      classes,
      theme,
      repeatMeetingBtnQuery,
      currentMeeting
    } = this.props;
    const {
      endValue,
      advanceOption,
      time,
      date,
      meetingsCount,
      advanceValue,
      requiredInput
    } = this.state;
    return (
      <Fragment>
        <div className={classes.inputWrapCnt}>
          <div className={classes.timeInputCnt}>
            <TimeInput
              label="Daily at"
              handleTimeChange={this.handleTimeChange}
              requiredInput={requiredInput}
              data={currentMeeting}
            />
          </div>
        </div>
        <div className={`${classes.inputWrapCnt} flex_center_center_row`}>
          <Grid
            item
            xs={7}
            className={`${classes.endByCnt} flex_center_start_row`}
          >
            <span className={classes.radioLabel}>End By</span>
            <Radio
              checked={endValue === "EndBy"}
              onChange={this.handleChange}
              value="EndBy"
              className={classes.radioBtn}
            />
            <StaticDatePicker
              isInput={true}
              requiredInput={requiredInput}
              selectedSaveDate={this.selectedSaveDate}
              isDisabled={endValue !== "EndBy" ? true : false}
              isCreation={true}
              minDate={new Date()}
              view="meeting-endBy"
              currentMeeting={currentMeeting}
            />
          </Grid>
          <Grid
            item
            xs={5}
            className={`${classes.afterCnt} flex_center_start_row`}
          >
            <span className={classes.radioLabel}>After</span>
            <Radio
              checked={endValue === "After"}
              onChange={this.handleChange}
              value="After"
              className={classes.radioBtn}
            />
            {endValue === "After" ? (
              <SelectMeetingCount
                handleMeetingChange={this.handleMeetingChange}
                disabled={false}
                currentMeeting={currentMeeting}
              />
            ) : (
              <SelectMeetingCount
                handleMeetingChange={this.handleMeetingChange}
                disabled={true}
                currentMeeting={currentMeeting}
              />
            )}
            <span className={classes.radioLabelRight}>meetings</span>
          </Grid>
        </div>
        <div className={classes.advanceOptionCnt}>
          <Typography
            variant="body2"
            className={classes.advanceOption}
            color="primary"
            onClick={this.handleAdvanceOption}
          >
            {advanceOption ? "Clear Advanced Options" : "Advanced Options"}
          </Typography>
          {advanceOption ? (
            <div className="flex_center_start_row">
              <span className={classes.selectLabel}>
                Meeting occurs when last meeting is:
              </span>
              <MeetingOccurSelect
                handleAdvanceMeetingChange={this.handleAdvanceMeetingChange}
              />
            </div>
          ) : null}
        </div>
        {time ? (
          <NotificationMessage type="sucess" iconType="calendar">
            {`Meeting occurs daily at ${time}`}
            {meetingsCount
              ? " and ends after " + meetingsCount + " meetings"
              : " till " + helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(date)}

            {advanceOption && advanceValue
              ? " when last meeting is " + advanceValue
              : "."}
          </NotificationMessage>
        ) : null}
        <div
          className={`flex_center_space_between_row ${classes.actionBtnCnt}`}
        >
          {/* <DefaultButton text="Discard Changes" buttonType="Transparent" /> */}

          <div className="flex_center_end_row">
            {/* <DefaultButton
            text="Delete Schedule"
            buttonType="dangerBtn"
            style={{ marginRight: 20 }}
          /> */}
            <CustomButton
              onClick={this.handleSubmit}
              style={{ marginRight: 20 }}
              btnType="success"
              variant="contained"
              query={repeatMeetingBtnQuery}
              disabled={repeatMeetingBtnQuery == "progress"}
            >
              Repeat Meeting
            </CustomButton>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Daily;
