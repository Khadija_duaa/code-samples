import React, { Component, Fragment } from "react";
import TimeInput from "../../../../components/DatePicker/TimeInput";
import StaticDatePicker from "../../../../components/DatePicker/StaticDatePicker";
import Radio from "@material-ui/core/Radio";
import Grid from "@material-ui/core/Grid";
import SelectMeetingCount from "./SelectMeetingCount";
import MeetingOccurSelect from "./MeetingOccurSelect";
import Typography from "@material-ui/core/Typography";
import NotificationMessage from "../../../../components/NotificationMessages/NotificationMessages";
import CustomButton from "../../../../components/Buttons/CustomButton";
import SelectIconMenu from "../../../../components/Menu/TaskMenus/SelectIconMenu";
import helper from "../../../../helper";
import moment from "moment";

const days = ["M", "T", "W", "T", "F", "S", "S"];
const initialState = {
  time: "",
  endValue: "EndBy",
  advanceOption: false,
  selectedDay: []
};
class Weekly extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time: "",
      endValue: "EndBy",
      advanceOption: false,
      advanceValue: "",
      date: moment(),
      meetingsCount: 0,
      selectedDay: [0],
      selectedFullDay: ["Monday"],
      selectedWeek: "",
      requiredInput: false
    };
    this.handleTimeChange = this.handleTimeChange.bind(this);
    this.handleAdvanceOption = this.handleAdvanceOption.bind(this);
    this.handleDayClick = this.handleDayClick.bind(this);
    this.resetState = this.resetState.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.selectedSaveDate = this.selectedSaveDate.bind(this);
    this.handleMeetingChange = this.handleMeetingChange.bind(this);
    this.handleAdvanceMeetingChange = this.handleAdvanceMeetingChange.bind(
      this
    );
    this.selectedRecursiveValue = this.selectedRecursiveValue.bind(this);
  }
  componentDidMount() {
    if (this.props.currentMeeting)
      this.setState({
        selectedDay: helper.RETURN_DAYS_INDEX(
          this.props.currentMeeting.selectedWeekDays
        ),
        selectedFullDay: this.props.currentMeeting.selectedWeekDays,
        time: this.props.currentMeeting.startTime,
        date: helper.RETURN_CUSTOMDATEFORMAT(
          moment(this.props.currentMeeting.endDateString)
        )
      });
  }

  selectedRecursiveValue(value) {
    this.setState({ selectedWeek: value });
  }
  handleMeetingChange(value) {
    this.setState({ meetingsCount: value.value });
  }
  handleAdvanceMeetingChange(value) {
    this.setState({ advanceValue: value.value });
  }
  handleSubmit() {
    let flag = 0;
    if (!this.state.date) {
      this.setState({
        requiredInput: true
      });
      flag = 1;
    } else {
      this.setState({
        requiredInput: false
      });
    }
    if (
      !this.state.time ||
      this.state.time === "0:0 " ||
      this.state.time === "0:0 PM" ||
      this.state.time === "0:0 AM"
    ) {
      flag = 1;
      this.setState({
        requiredInput: true
      });
    } else {
      this.setState({
        requiredInput: false
      });
    }
    if (flag) {
      return;
    }
    this.props.saveData(this.state);
  }
  selectedSaveDate(date) {
    this.setState({
      date: helper.RETURN_CUSTOMDATEFORMAT(date),
      requiredInput: false
    });
  }
  resetState() {
    this.setState(initialState);
  }
  handleChange = event => {
    this.setState({ endValue: event.target.value, requiredInput: false });
  };
  handleAdvanceOption() {
    this.setState(prevState => ({
      advanceOption: !prevState.advanceOption,
      requiredInput: false
    }));
  }
  handleTimeChange(time) {
    let timeData = time.toLowerCase().includes("am")
      ? time
      : time.toLowerCase().includes("pm")
      ? time
      : time + " AM";
    this.setState({ time: timeData, requiredInput: false });
  }
  handleDayClick(event, day, i) {
    const dayArr = [i];
    const dayIndex = this.state.selectedDay.indexOf(i);
    const dayState = [...this.state.selectedDay];
    dayState.splice(dayIndex, 1);
    if (dayIndex >= 0) {
      this.setState({ selectedDay: dayState });
    } else if (dayIndex < 0) {
      this.setState(prevState => ({
        selectedDay: [...prevState.selectedDay, ...dayArr],
        selectedFullDay: helper.RETURN_DAYS_WITH_ARRAY([
          ...prevState.selectedDay,
          ...dayArr
        ]),
        requiredInput: false
      }));
    }
  }

  render() {
    const {
      classes,
      theme,
      repeatMeetingBtnQuery,
      currentMeeting
    } = this.props;
    const {
      endValue,
      advanceOption,
      selectedDay,
      advanceValue,
      time,
      date,
      meetingsCount,
      selectedFullDay,
      selectedWeek,
      requiredInput
    } = this.state;
    return (
      <Fragment>
        <div className={classes.inputWrapCnt}>
          <div className="flex_center_start_row mb20">
            <span className={classes.selectLabel}>Every:</span>
            <SelectIconMenu
              iconType="status"
              isSimpleList={false}
              heading="Select week"
              menuType="weeks"
              isSingle={true}
              currentMeeting={currentMeeting}
              selectedRecursiveValue={this.selectedRecursiveValue}
            />
            <span className={classes.selectLabelMid}>on</span>
            <ul className={classes.daysSelect}>
              {days.map((day, i) => {
                return (
                  <li
                    key={i}
                    onClick={event => this.handleDayClick(event, day, i)}
                    style={{
                      background:
                        selectedDay.indexOf(i) >= 0
                          ? theme.palette.common.white
                          : null
                    }}
                  >
                    {day}
                  </li>
                );
              })}
            </ul>
          </div>
          <div className="flex_center_start_row">
            <span className={classes.selectLabel}>Occurs at:</span>
            <div className={classes.timeInputCnt}>
              <TimeInput
                label={false}
                handleTimeChange={this.handleTimeChange}
                requiredInput={requiredInput}
                data={currentMeeting}
              />
            </div>
          </div>
        </div>
        <div className={`${classes.inputWrapCnt} flex_center_center_row`}>
          <Grid
            item
            xs={7}
            className={`${classes.endByCnt} flex_center_start_row`}
          >
            <span className={classes.radioLabel}>End By</span>
            <Radio
              checked={endValue === "EndBy"}
              onChange={this.handleChange}
              value="EndBy"
              className={classes.radioBtn}
            />
            <StaticDatePicker
              isInput={true}
              selectedSaveDate={this.selectedSaveDate}
              isDisabled={endValue !== "EndBy" ? true : false}
              isCreation={true}
              requiredInput={requiredInput}
              view="meeting-endBy"
              currentMeeting={currentMeeting}
            />
          </Grid>
          <Grid
            item
            xs={5}
            className={`${classes.afterCnt} flex_center_start_row`}
          >
            <span className={classes.radioLabel}>After</span>
            <Radio
              checked={endValue === "After"}
              onChange={this.handleChange}
              value="After"
              className={classes.radioBtn}
            />
            {endValue === "After" ? (
              <SelectMeetingCount
                handleMeetingChange={this.handleMeetingChange}
                disabled={false}
              />
            ) : (
              <SelectMeetingCount
                handleMeetingChange={this.handleMeetingChange}
                disabled={true}
              />
            )}
            <span className={classes.radioLabelRight}>meetings</span>
          </Grid>
        </div>
        <div className={classes.advanceOptionCnt}>
          <Typography
            variant="body2"
            className={classes.advanceOption}
            color="primary"
            onClick={this.handleAdvanceOption}
          >
            {advanceOption ? "Clear Advanced Options" : "Advanced Options"}
          </Typography>
          {advanceOption ? (
            <div className="flex_center_start_row">
              <span className={classes.selectLabel}>
                Meeting occurs when last meeting is:
              </span>
              <MeetingOccurSelect
                handleAdvanceMeetingChange={this.handleAdvanceMeetingChange}
              />
            </div>
          ) : null}
        </div>
        {}
        {time || selectedWeek ? (
          <NotificationMessage type="sucess" iconType="calendar">
            {`Meeting occurs every ${selectedWeek ||
              "1st week"} on ${selectedFullDay.join(", ")} at ${time}`}
            {meetingsCount
              ? " and ends after " + meetingsCount + " meetings"
              : " till " + helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(date)}

            {advanceOption && advanceValue
              ? " when last meeting is " + advanceValue
              : "."}
          </NotificationMessage>
        ) : null}
        <div
          className={`flex_center_space_between_row ${classes.actionBtnCnt}`}
        >
          {/* <DefaultButton
            text="Discard Changes"
            onClick={this.resetState}
            buttonType="Transparent"
          /> */}

          <div className="flex_center_end_row">
            {/* <DefaultButton
              text="Delete Schedule"
              buttonType="dangerBtn"
              style={{ marginRight: 20 }}
            /> */}
            <CustomButton
              onClick={this.handleSubmit}
              btnType="success"
              variant="contained"
              query={repeatMeetingBtnQuery}
              disabled={repeatMeetingBtnQuery == "progress"}
            >
              Repeat Meeting
            </CustomButton>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Weekly;
