import React, { Component } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { CreateMeetingSchedule } from "../../../../redux/actions/meetings";
import CustomDialog from "../../../../components/Dialog/CustomDialog";
import dialogStyles from "../../../../assets/jss/components/dialog";
import { withStyles } from "@material-ui/core/styles";
import repeatMeetingStyle from "./style";
import combineStyles from "../../../../utils/mergeStyles";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import Daily from "./Daily";
import Weekly from "./Weekly";
import Monthly from "./Monthly";
import helper from "../../../../helper";
import {FormattedMessage} from 'react-intl';

class RepeatMeetingForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "Daily",
      repeatMeetingBtnQuery: ""
    };
    this.saveData = this.saveData.bind(this);
  }
  componentDidMount() {
    if (this.props.currentMeeting)
      this.setState({
        value:
          this.props.currentMeeting.meetingFrequency &&
          this.props.currentMeeting.meetingFrequency !== "NoRecurrence"
            ? this.props.currentMeeting.meetingFrequency
            : "Daily"
      });
    else
      this.setState({
        value: "Daily"
      });
  }
  handleChange = event => {
    this.setState({ value: event.target.value });
  };
  saveData(value) {
    let currentMeeting = this.props.currentMeeting;
    if (this.state.value === "Daily") {
      const object = helper.CALCULATEDURATIONDATETIME(
        value.time,
        currentMeeting,
        true,
        null,
        null,
        value.date
      );
      object.meetingFrequency = "Daily";
      object.dailyIntervalType = "everyNumberOfDays";
      object.advanceMeeting = value.advanceOption
        ? value.advanceValue === "Submitted for Review"
          ? "Submitted"
          : value.advanceValue || "Published"
        : null;
      if (value.meetingsCount) {
        object.NumberOfMeetings = parseInt(value.meetingsCount);
        object.endBy = "After";
      } else {
        object.endBy = "Date";
      }

      this.setState({ repeatMeetingBtnQuery: "progress" }, () => {
        this.props.CreateMeetingSchedule(
          object,
          response => {
            this.setState({ repeatMeetingBtnQuery: "" });
            this.props.closeAction();
          },
          error => {
            this.setState({ repeatMeetingBtnQuery: "" });
            if (error && error.status === 500) {
              this.props.showSnackBar("Server throws error", "error");
            } else {
              this.props.showSnackBar(
                error ? error.data.message : "error",
                "error"
              );
            }
          }
        );
      });
    } else if (this.state.value === "Weekly") {
      let weeks = ["1st week", "2nd week", "3rd week", "4th week"];
      const object = helper.CALCULATEDURATIONDATETIME(
        value.time,
        currentMeeting,
        true,
        null,
        null,
        value.date
      );
      object.meetingFrequency = "Weekly";
      object.weeklyInterval = weeks.indexOf(value.selectedWeek) + 1 || 1;
      object.selectedWeekDays = value.selectedFullDay;
      object.advanceMeeting = value.advanceOption
        ? value.advanceValue === "Submitted for Review"
          ? "Submitted"
          : value.advanceValue || "Published"
        : null;
      if (value.meetingsCount) {
        object.NumberOfMeetings = parseInt(value.meetingsCount);
        object.endBy = "After";
      } else {
        object.endBy = "Date";
      }
      this.setState({ repeatMeetingBtnQuery: "progress" }, () => {
        this.props.CreateMeetingSchedule(
          object,
          response => {
            this.setState({ repeatMeetingBtnQuery: "" });
            this.props.closeAction();
          },
          error => {
            this.setState({ repeatMeetingBtnQuery: "" });
            if (error && error.status === 500) {
              this.props.showSnackBar("Server throws error", "error");
            } else {
              this.props.showSnackBar(
                error ? error.data.message : "error",
                "error"
              );
            }
          }
        );
      });
    } else if (this.state.value === "Monthly") {
      let day = "";
      const object = helper.CALCULATEDURATIONDATETIME(
        value.time,
        currentMeeting,
        true,
        null,
        null,
        value.date
      );
      object.meetingFrequency = "Monthly";
      object.monthlyInterval = value.selectedMonth
        ? parseInt(value.selectedMonth[0])
        : 1;
      object.advanceMeeting = value.advanceOption
        ? value.advanceValue === "Submitted for Review"
          ? "Submitted"
          : value.advanceValue || "Published"
        : null;
      if (value.monthOption === "Day") {
        object.dayOfMonthlyInterval = value.selectedDay
          ? parseInt(value.selectedDay[0])
          : 1;
        object.dailyIntervalType = "everyNumberOfDays";
        object.monthlyIntervalType = "SelectDay";
      } else if (value.monthOption === "Week") {
        object.dayOfWeek =
          value.selectedFullDay && value.selectedFullDay.length > 0
            ? value.selectedFullDay[0]
            : "Monday";
        object.weeklyInterval = value.selectedWeek;
        object.monthlyIntervalType = "SelectWeek";
      }
      if (value.meetingsCount) {
        object.NumberOfMeetings = parseInt(value.meetingsCount);
        object.endBy = "After";
      } else {
        object.endBy = "Date";
      }

      this.setState({ repeatMeetingBtnQuery: "progress" }, () => {
        this.props.CreateMeetingSchedule(
          object,
          response => {
            this.setState({ repeatMeetingBtnQuery: "" });
            this.props.closeAction();
          },
          error => {
            this.setState({ repeatMeetingBtnQuery: "" });
            if (error && error.status === 500) {
              this.props.showSnackBar("Server throws error", "error");
            } else {
              this.props.showSnackBar(
                error ? error.data.message : "error",
                "error"
              );
            }
          }
        );
      });
    }
  }
  render() {
    const { classes, theme, open, closeAction, currentMeeting } = this.props;
    const { value, repeatMeetingBtnQuery } = this.state;
    return (
      <CustomDialog
        title={<FormattedMessage id="meeting.detail-dialog.repeat-meeting.label"  defaultMessage="Repeat Meeting"/>}
        dialogProps={{
          open: open,
          onClose: closeAction,
          PaperProps: {
            className: classes.dialogPaperCmp
          }
        }}
      >
        <FormControl component="fieldset" className={classes.formControl}>
          <RadioGroup
            name="Layout"
            className={classes.group}
            value={this.state.value}
            onChange={this.handleChange}
            row
          >
            <FormControlLabel
              value="Daily"
              control={<Radio color="primary" />}
      label={<FormattedMessage id="meeting.detail-dialog.repeat-meeting.daily.label"  defaultMessage="Daily"/> }
            />
            <FormControlLabel
              value="Weekly"
              control={<Radio color="primary" />}
              label={<FormattedMessage id="meeting.detail-dialog.repeat-meeting.weekly.label"  defaultMessage="Weekly"/> }
            />
            <FormControlLabel
              value="Monthly"
              control={<Radio color="primary" />}
              label= {<FormattedMessage id="meeting.detail-dialog.repeat-meeting.monthly.label"  defaultMessage="Monthly"/>}
            />
          </RadioGroup>
        </FormControl>
        <div className={classes.dialogContentCnt}>
          {value == "Daily" ? (
            <Daily
              theme={theme}
              classes={classes}
              saveData={this.saveData}
              repeatMeetingBtnQuery={repeatMeetingBtnQuery}
              currentMeeting={currentMeeting}
            />
          ) : value == "Weekly" ? (
            <Weekly
              theme={theme}
              classes={classes}
              saveData={this.saveData}
              repeatMeetingBtnQuery={repeatMeetingBtnQuery}
              currentMeeting={currentMeeting}
            />
          ) : (
            <Monthly
              theme={theme}
              classes={classes}
              saveData={this.saveData}
              repeatMeetingBtnQuery={repeatMeetingBtnQuery}
              currentMeeting={currentMeeting}
            />
          )}
        </div>
      </CustomDialog>
    );
  }
}

export default compose(
  withRouter,
  withStyles(combineStyles(dialogStyles, repeatMeetingStyle), {
    withTheme: true
  }),
  connect(
    null,
    {
      CreateMeetingSchedule
    }
  )
)(RepeatMeetingForm);
