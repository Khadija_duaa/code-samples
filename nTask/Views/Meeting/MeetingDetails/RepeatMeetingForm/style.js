const repeatMeetingStyle = theme => ({
  dialogContentCnt: {
    padding: 20,
    paddingTop: 0
  },
  dialogPaperCmp: {
    // maxWidth: 700
  },
  radioBtnGroupLabel: {
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular,
    marginLeft: 3
  },
  formControl: {
    width: "100%",
    padding: "10px 50px",
    background: theme.palette.background.items
  },
  dailyFormControl: {
    width: "100%"
  },
  group: {
    width: "100%",
    display: "flex",
    justifyContent: "space-between"
  },
  dailyViewGroup: {
    width: "100%",
    display: "flex"
  },
  radioLabel: {
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightLight,
    lineHeight: 1.5,
    display: "inline-block"
  },
  selectLabel:{
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightLight,
    lineHeight: 1.5,
    display: "inline-block",
    minWidth: 75
  },
  selectLabelMid: {
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightLight,
    lineHeight: 1.5,
    display: "inline-block",
    minWidth: 17,
    margin: "0 15px"
  },
  selectLabelAdvanced: {
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightLight,
    lineHeight: 1.5,
    width: 425
  },
  radioLabelRight: {
    color: theme.palette.text.secondary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightLight,
    lineHeight: 1.5,
    display: "inline-block",
    marginLeft: 10
  },
  endByCnt: {
      marginRight: 20,
      '& $radioLabel':{
          minWidth: 70
      },
  },
  afterCnt: {
      '& $radioLabel':{
          minWidth: 35
      },
  },
  radioBtn: {
      padding: 0,
      margin: "0 5px"
  },
  timeInputCnt:{
      width: 210
  },
  inputWrapCnt:{
    padding: "20px 0",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`
  },
  advanceOptionCnt: {
    padding: "10px 0"
  },
  advanceOption:{
    textDecoration: "underline",
    cursor: "pointer"
  },
  actionBtnCnt:{
    marginTop: 40
  },
  daysSelect:{
    listStyleType: 'none',
    display: "flex",
    padding: 0,
    margin: 0,
    '& li': {
      padding: "12px 14px",
      border: `1px solid ${theme.palette.border.lightBorder}`,
      borderLeft: "none",
      margin: 0,
      background: theme.palette.background.items,
      cursor: "pointer"

    },
    '& li:first-child': {
      borderRadius: "30px 0 0 30px",
      borderLeft: `1px solid ${theme.palette.border.lightBorder}`
    },
    '& li:last-child': {
      borderRadius: "0 30px 30px 0"
    }
  }
});

export default repeatMeetingStyle;
