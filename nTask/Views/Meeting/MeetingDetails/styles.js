const taskDetailStyles = (theme) => ({
  fontSize: {
    fontSize: "11px !important"
  },
  taskDetailsDialog: {
    paddingRight: "0 !important",
  },
  dialogCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  dialogPaperCnt: {
    background: theme.palette.common.white,
    maxWidth: "95%",
    minHeight: "90%",
  },
  defaultDialogTitle: {
    padding: "0",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
  },
  defaultDialogContent: {
    padding: 0,
    display: "flex",
    overflow: "hidden"
  },
  defaultDialogAction: {
    padding: " 25px 25px",
  },
  assigneeListCnt: {
    padding: "0 20px",
    margin: "0 10px",
    borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
    borderRight: `1px solid ${theme.palette.border.lightBorder}`,
  },
  mainTaskDetailsCnt: {
    marginBottom: 40,
  },

  taskDetailsLeftCnt: {
    borderRight: `1px solid ${theme.palette.border.lightBorder}`,
    padding: 30,
    height: "100%",
  },
  dropdownsLabel: {
    transform: "translate(6px, -7px) scale(1)",
    display: "block",
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular,
  },
  btnDropdownsLabel: {
    transform: "translate(0, -7px) scale(1)",
    display: "block",
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular,
  },
  taskDetailsFieldCnt: {},
  descriptionCnt: {
    height: 120,
    padding: 10,
    resize: "vertical",
    borderRadius: "4px",
    overflowY: "auto",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    fontSize: "12px !important",
    "& *": {
      margin: 0,
    },
  },
  tabLabelCnt: {
    padding: 0,
    fontSize: "12px !important",
    textTransform: "capitalize",
    fontWeight: theme.typography.fontWeightRegular,
  },
  TabsRoot: {
    background: theme.palette.background.paper,
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
  },
  tab: {
    minWidth: "unset",
  },

  TabContentCnt: {
    background: theme.palette.background.light,
    padding: 20,
    height: "calc(100% - 49px)",
    position: "relative",
  },
  tabIndicator: {
    background: theme.palette.primary.light,
  },
  outlinedInputCnt: {
    marginRight: 15,
  },
  outlinedTimeInputCnt: {
    margin: 0,
    background: theme.palette.common.white,
    borderRadius: 4,
  },
  outlinedMinInputCnt: {
    "&:hover $notchedOutlineMinCnt": {
      border: `1px solid ${theme.palette.border.lightBorder} !important`,
    },
  },
  outlinedHoursInputCnt: {
    "&:hover $notchedOutlineHoursCnt": {
      border: `1px solid ${theme.palette.border.lightBorder} !important`,
      borderRight: "none !important",
    },
  },
  outlineInputFocus: {
    "& $notchedOutlineCnt": {
      border: `1px solid ${theme.palette.border.lightBorder}`,
    },
    "& $notchedOutlineMinCnt": {
      border: `1px solid ${theme.palette.border.lightBorder}`,
    },
    "& $notchedOutlineHoursCnt": {
      border: `1px solid ${theme.palette.border.lightBorder}`,
      borderRight: 0,
    },
    "& $notchedOutlineAMCnt": {
      border: `1px solid ${theme.palette.border.lightBorder}`,
    },
  },
  outlinedInput: {
    padding: "13.5px 14px",
    "&:hover": {
      cursor: "auto",
    },
  },
  outlinedDateInput: {
    padding: "13.5px 14px",
    background: theme.palette.background.default,
    borderRadius: 4,
  },
  outlinedTaskInput: {
    padding: "10px 14px",
    background: theme.palette.background.default,
    borderRadius: 4,
    fontSize: "22px !important",
    fontWeight: theme.typography.fontWeightRegular,
  },
  outlinedInputDisabled: {
    background: "transparent",
  },
  outlinedTimeInput: {
    padding: 14,
    textAlign: "center",
  },
  outlinedHoursInput: {
    background: theme.palette.background.default,
    textAlign: "center",
    borderRadius: "30px 0 0 30px",
    padding: 14,
  },
  outlinedMinInput: {
    padding: 14,
    background: theme.palette.background.default,
    textAlign: "center",
  },
  outlinedAmInput: {
    padding: 14,
    borderRadius: "0 30px 30px 0",
    borderLeft: 0,
    textAlign: "center",
    textTransform: "capitalize",
    "&:hover": {
      cursor: "pointer",
    },
  },
  notchedOutlineCnt: {
    borderRadius: 4,
    border: `1px solid ${theme.palette.border.lightBorder}`,
  },
  notchedOutlineHoursCnt: {
    borderRadius: "30px 0 0 30px",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRight: 0,
  },
  notchedOutlineMinCnt: {
    borderRadius: 0,
    border: `1px solid ${theme.palette.border.lightBorder}`,
  },
  notchedOutlineAMCnt: {
    borderRadius: "0 30px 30px 0",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderLeft: 0,
  },
  addTimeBtnCnt: {
    display: "flex",
    background: theme.palette.background.medium,
    padding: "5px 5px",
    cursor: "pointer",
    borderRadius: 20,
  },
  addTimeActionBtnCnt: {
    textAlign: "right",
    marginTop: 10,
  },
  addTimeBtnText: {
    color: theme.palette.text.primary,
    fontSize: "16px !important",
    lineHeight: "20px",
    marginLeft: 5,
    textDecoration: "underline",
  },
  checklistInputCnt: {
    marginTop: 25,
  },
  clInputProgressCnt: {
    display: "flex",
    alignItems: "center",
    marginBottom: 10,
  },
  clProgressValue: {
    position: "absolute",
    right: 13,
    top: 24,
    fontSize: "15px !important",
  },
  checklistCnt: {},
  checkList: {
    listStyleType: "none",
    margin: "0 0 0 -20px",
    padding: 0,
  },
  checkListItem: {
    fontSize: "17px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightLight,
    marginBottom: 5,
    paddingRight: 23,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    "&:hover $CloseIconBtn": {
      visibility: "visible",
    },
    "&:hover $dragHandleCnt": {
      visibility: "visible",
    },
    "&:hover $emptyCheckbox": {
      display: "none",
    },
    "&:hover $unCheckedIcon": {
      display: "block",
    },
  },
  dragHandleCnt: {
    visibility: "hidden",
  },
  dragHandle: {
    fontSize: "28px !important",
  },
  checkedIcon: {
    fontSize: "26px !important",
  },
  emptyCheckbox: {
    width: 26,
    height: 26,
    background: theme.palette.background.paper,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "100%",
  },
  unCheckedIcon: {
    fontSize: "26px !important",
    display: "none",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "100%",
  },
  checklistItemDetails: {
    marginLeft: 10,
  },
  checkListItemInner: {
    display: "flex",
    alignItems: "center",
    width: "90%",
    "& span": {
      fontSize: "12px !important",
      color: theme.palette.text.primary,
      lineHeight: "normal",
    },
  },
  checklistCheckboxCnt: {
    margin: 0,
  },
  checkListCheckBox: {
    padding: 6,
  },
  CloseIconBtn: {
    visibility: "hidden",
  },
  checkboxCheckedIcon: {
    color: theme.palette.primary.light,
  },
  checkboxIcon: {},
  datePickerPopper: {
    zIndex: 2,
  },

  menuTab: {
    background: theme.palette.background.light,
    borderRadius: "10px 10px 0 0",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderBottom: "none",
    opacity: 1,
    minWidth: 152,
  },
  menuTabSelected: {
    background: theme.palette.common.white,
    borderRadius: "10px 10px 0 0",
    border: `1px solid ${theme.palette.border.lightBorder}`,
  },
  menuTabIndicator: {
    background: "transparent",
    height: 1,
  },
  menuTimeCnt: {
    background: theme.palette.background.light,
    marginTop: 48,
    marginBottom: 4,
    border: "1px solid rgba(221,221,221,1)",
    borderRadius: "0 12px 12px 0",
    borderLeft: 0,
    width: 250,
  },
  dateTimeHeading: {
    background: theme.palette.background.default,
    padding: "10px 20px",
    margin: 0,
    textTransform: "uppercase",
    fontSize: "0.944rem !important",
    fontWeight: theme.typography.fontWeightRegular,
    borderRadius: "0 12px 0 0",
    color: theme.palette.text.secondary,
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
  },
  menuDateTimeInnerCnt: {
    padding: "15px 20px",
  },
  timeFieldsCnt: {
    padding: "0 20px",
  },
  otherCommentAvatar: {
    marginRight: 10,
  },
  selfCommentAvatar: {
    marginLeft: 10,
  },
  commentContentCnt: {
    borderRadius: 4,
    fontSize: "13px !important",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    padding: "10px 15px",
    fontSize: "13px !important",
    background: theme.palette.background.default,
    wordBreak: "break-all",
    "& p": {
      margin: 0,
      padding: 0,
      lineHeight: 1.5,
      wordBreak: "break-word",
    },
  },
  commentAttachContentCnt: {
    borderRadius: 10,
    fontSize: "13px !important",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    padding: "10px 15px",
    background: theme.palette.background.default,
    "& p": {
      margin: 0,
      padding: 0,
    },
  },
  commentBoxMention: {
    color: theme.palette.primary.light,
  },
  otherTimeStamp: {
    fontSize: "11px !important",
    marginLeft: 12,
    color: theme.palette.text.secondary,
    textAlign: "left",
    margin: 0,
    marginTop: 3,
  },
  selfTimeStamp: {
    fontSize: "11px !important",
    marginRight: 12,
    color: theme.palette.text.secondary,
    textAlign: "right",
    margin: 0,
    marginTop: 3,
  },
  attachmentCnt: {
    background: theme.palette.background.light,
    borderRadius: 5,
    marginTop: 10,
    padding: "10px 15px 10px 15px",
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
  },
  commentsAttachmentIcon: {
    marginRight: 10,
  },
  commentsFileName: {
    fontSize: "12px !important",
    paddingRight: "20px !important",
    color: theme.palette.text.primary,
    marginBottom: "3px !important",
  },
  commentsFileSize: {
    fontSize: "10px !important",
    color: theme.palette.text.secondary,
  },
  attachmentDownloadIcon: {
    background: theme.palette.secondary.medDark,
    borderRadius: "100%",
  },
  // Notification Menu Styles
  NotifAvatar: {
    borderRadius: "100%",
    width: 56,
    height: 56,
  },
  notifMenuItem: {
    height: "auto",
    padding: "15px 15px",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
  },
  notifMenuHeadingItem: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    cursor: "unset",
    "&:hover": {
      background: "transparent",
    },
  },
  menuHeaderReadAllText: {
    color: theme.palette.text.primary,
    cursor: "pointer",
  },
  menuHeadingText: {
    textTransform: "uppercase",
  },
  notifMenuItemText: {
    padding: 0,
    "& p": {
      fontSize: "12px !important",
      fontWeight: theme.typography.fontWeightLight,
      color: theme.palette.text.secondary,
      margin: 0,
      "& b": {
        fontWeight: theme.typography.fontWeightMedium,
        color: theme.palette.text.primary,
      },
    },
    "& $notifMenuDate": {
      fontSize: "10px !important",
    },
  },
  notifMenuDate: {},
  activityCardCnt: {
    padding: "8px 10px",
    background: theme.palette.common.white,
    borderRadius: 4,
    border: `1px solid ${theme.palette.border.lightBorder}`,
  },
  activityMsg: {
    color: theme.palette.text.primary,
    "& b": {
      color: theme.palette.text.primary,
      fontWeight: theme.typography.fontWeightMedium,
    },
  },
  activityDate: {
    textTransform: "uppercase",
    marginBottom: 10,
    display: "flex",
    alignItems: "center",
  },
  calendarIcon: {
    marginRight: 5,
  },
  activityTime: {
    fontSize: "12px !important",
    margin: "0 0 5px 0",
    color: theme.palette.text.secondary,
  },
  singleActivityCnt: {
    marginBottom: 20,
  },
  taskTitle: {
    lineHeight: 1.6,
    padding: "0 15px",
    transition: "ease background 1s",
    maxWidth: '100%',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    borderRadius: 4,
    "&:hover": {
      background: theme.palette.background.items,
      transition: "ease background 1s",
      cursor: "text",
    },
  },
  taskCreationText: {
    fontSize: "10px !important",
    color: theme.palette.text.light,
    position: "absolute",
    padding: 0,
    bottom: 0,
    left: 10,
    right: 1,
    background: theme.palette.common.white,
  },
  meetingDetailsIcon: {
    width: 42,
    height: 42,
    border: `1px solid ${theme.palette.border.greenBorder}`,
    padding: 8,
    borderRadius: "50%",
    marginRight: 5,
    display: "inline-block",
  },
  meetingDetailsIconCnt: {
    display: "flex",
  },
  MeetingDetailsHeading: {
    background: theme.palette.background.items,
    padding: 8,
    borderRadius: 4,
  },
  meetingDetailsHeadingText: {
    marginLeft: 5,
  },
  meetingDetailsHeadingIcon: {
    color: theme.palette.background.green,
    fontSize: "17px !important"
  },
  addMeetingDetailList: {
    listStyleType: "none",
    marginBottom: 20,
    padding: 0,
    "& > li": {
      padding: "8px 10px",
      borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
      fontSize: "12px !important",
      color: theme.palette.text.primary,
      cursor: "text",
      "& p": {
        margin: 0,
        padding: 0,
      },
      "& $CloseIconBtn": {
        marginLeft: 10,
      },
      "&:hover $CloseIconBtn": {
        visibility: "visible",
      },
    },
    "& > $inputListItem": {
      padding: 0,
    },
  },
  inputListItem: {},
  listIcon: {
    fontSize: "8px !important",
    color: theme.palette.secondary.light,
    marginRight: 10,
  },
  followUpActionDetails: {
    fontSize: "10px !important",
    display: "block",
  },
  convertToTask: {
    color: theme.palette.primary.light,
    borderRight: `1px solid ${theme.palette.border.lightBorder}`,
    marginRight: "10px !important",
    margin: "0 !important",
    padding: "3px 10px 3px 10px !important",
    cursor: "pointer",
  },
  staticDatePickerCnt: {
    borderRight: `1px solid ${theme.palette.border.lightBorder}`,
    paddingRight: 10,
    display: "flex",
    alignItems: "center",
    marginRight: 10,
  },
  timeInputCnt: {
    // width: 210,
    // marginRight: 20,
    marginLeft: 20,
    marginTop: 8,
  },
  durationInputCnt: {
    // width: 200,
    flex: 1,
    // marginRight: 20
  },
  publishBtn: {
    borderRight: `1px solid ${theme.palette.border.lightBorder}`,
    borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
    paddingRight: 15,
    paddingLeft: 10,
    margin: "0px 10px 0 20px",
  },
  addFollowUpExtraCnt: {
    position: "absolute",
    right: 0,
    top: 8,
    zIndex: 1,
  },
  meetingDetailsBottomMsg: {
    padding: "5px 20px",
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    background: theme.palette.background.items,
    borderRadius: "0 0 4px 4px",
  },
  recurrenceIcon: {
    fontSize: "18px !important",
  },
  addMeetingDetailPlus: {
    marginRight: 10,
    fontSize: "16px !important",
    color: theme.palette.primary.light,
  },
  addMeetingDetailBtn: {
    padding: "11px 10px",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    cursor: "pointer",
  },
  panels: {
    width: 470,
    marginLeft: 10,
    borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
    backgroundColor: theme.palette.background.light,
  },
  panel: {},
  panelHeader: {
    justifyContent: "space-between",
    display: "flex",
    alignItems: "center",
    backgroundColor: theme.palette.background.items,
    padding: "10px",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    borderTop: `1px solid ${theme.palette.border.lightBorder}`,
  },
  panelHeaderCnt: {
    display: "flex",
    alignItems: "center",
  },
  attendenceList: {
    listStyleType: "none",
    padding: 0,
    margin: 0,
    marginBottom: 25,
    "& > li": {
      display: "flex",
      flexDirection: "column",
      // borderBottom: `1px solid ${theme.palette.border.extraLightBorder}`,
    },
    "& li:last-child": {
      borderBottom: "none",
      // borderBottom: `3px solid ${theme.palette.border.extraLightBorder}`,
    },
  },
  attendListDiv: {
    padding: "4px 10px",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },

  toggleBtnGroup: {
    textAlign: "center",
    borderRadius: 4,
    background: theme.palette.common.white,
    border: "none",
    boxShadow: "none",
    display: "flex",
    "& $toggleButtonSelected[value = 'present']": {
      color: theme.palette.text.brightGreen,
      backgroundColor: theme.palette.background.extraLightGreen,
      "&:after": {
        background: theme.palette.common.white,
      },
      "&:hover": {
        background: theme.palette.background.extraLightGreen,
        color: theme.palette.text.brightGreen,
      },
    },
    "& $toggleButtonSelected[value = 'absent']": {
      color: theme.palette.text.danger,
      backgroundColor: theme.palette.background.dangerLight,
      "&:after": {
        background: theme.palette.common.white,
      },
      "&:hover": {
        color: theme.palette.text.danger,
        background: theme.palette.background.dangerLight,
      },
    },
  },
  toggleButton: {
    border: `1px solid ${theme.palette.border.lightBorder} !important`,
    borderRadius: 4,
    // height: 54,
    padding: "5px 20px",
    fontSize: "12px !important",
    textTransform: "capitalize",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightLight,
    background: theme.palette.common.white,
    "&:hover": {
      background: theme.palette.background.medium,
    },
    "&[value = 'present']": {
      borderRight: 0,
    },
  },
  toggleButtonSelected: {},
  panelBodyDisable: {
    pointerEvents: "none",
  },
  panelBody: {
    // marginRight: 20
  },
  unArchiveTaskCnt: {
    background: theme.palette.background.black,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    padding: 10,
  },
  unArchiveTaskTxt: {
    color: theme.palette.common.white,
  },
  disablePanel: {
    position: "absolute",
    pointerEvents: "unset",
    width: 470,
    marginLeft: "70%",
    height: "80%",
    backgroundColor: theme.palette.background.light,
    zIndex: 1,
    opacity: 0.3,
  },
  zoomLabel: {
    fontWeight: theme.typography.fontWeightRegular,
    color: '#202020',
  },
  zoomLink: {
    border: `1px solid ${theme.palette.border.lightBorder}`,
    backgroundColor: theme.palette.background.items,
    "& a": {
      color: theme.palette.text.azure,
    },
    padding: '12px 14px',
    borderRadius: 4,
    marginTop: 5,
    fontSize: "12px !important",
  },
  meetingDescriptionCnt: {
    border: `1px solid ${theme.palette.border.lightBorder}`,
    backgroundColor: theme.palette.common.white,
    padding: '12px 14px',
    borderRadius: 4,
    marginTop: 5,
    marginBottom: 10,
  },
  joinSection: {
    marginTop: 10,
    wordBreak: 'break-all',
    "& a": {
      color: theme.palette.text.azure,
      fontSize: "12px !important",
    },
  },
  subSection: {
    marginTop: 10,
    display: 'flex',
    flexDirection: "column"
  },
  subSecValue: {
    marginLeft: 5,
    wordBreak: 'break-all'
  },
  miniLoader: {
    width: 20,
    height: 20,
    border: `1px solid white`,
    borderTop: `1px solid #0090ff`,
    marginRight: 20
  },
  snackBarBtn: {
    color: "white",
    background: "#0090ff",
    padding: 8,
    borderRadius: 4,
    cursor: "pointer",
    fontSize: "13px !important"
  }
});

export default taskDetailStyles;
