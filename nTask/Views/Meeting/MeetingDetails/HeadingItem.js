import React from 'react';
import Typography from '@material-ui/core/Typography';
import AgendaIcon from "@material-ui/icons/Assignment"

function MeetingDetailHeading(props){
    return (
        <div>
            <AgendaIcon htmlColor={theme.palette.primary.light} />
            <Typography>Agenda</Typography>
        </div>
    )
}