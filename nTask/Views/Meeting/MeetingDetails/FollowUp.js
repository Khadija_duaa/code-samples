import React, { Component, Fragment } from "react";
import DefaultTextField from "../../../components/Form/TextField";
import Typography from "@material-ui/core/Typography";
// import AgendaIcon from "@material-ui/icons/AssignmentOutlined";
import CalendarIcon from "@material-ui/icons/DateRange";
import AddIcon from "@material-ui/icons/Add";
import RoundIcon from "@material-ui/icons/Brightness1";
import findIndex from "lodash/findIndex";
import Hotkeys from "react-hot-keys";
import MemberListMenu from "../../../components/Menu/TaskMenus/MemberListMenu";
import StaticDatePicker from "../../../components/DatePicker/StaticDatePicker";
import CloseIcon from "@material-ui/icons/Close";
import IconButton from "../../../components/Buttons/IconButton";
import moment from "moment";
import helper from "../../../helper";
import DeleteConfirmDialog from "../../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import { getMeetingsPermissions, getReadOnlyPermissions } from "../permissions";
// import AgendaIcon from "../../../assets/images/meeting_followup_actions.svg";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import { GetPermission } from "../../../components/permissions";
import { FormattedMessage, injectIntl } from "react-intl";
import SvgIcon from "@material-ui/core/SvgIcon";
import MeetingFollowUpIcon from "../../../components/Icons/MeetingFollowUpIcon";
class FollowUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      assignees: [],
      date: moment(),
      followUp: "",
      isDelete: false,
      addToTask: false,
      currentObject: {},
      checkedAssignee: [],
      deleteBtnQuery: "",
      convertToTaskBtnQuery: "",
      disableField: false

    };
    this.onAddFollowup = this.onAddFollowup.bind(this);
    this.onKeyDown = this.onKeyDown.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.selectAssignee = this.selectAssignee.bind(this);
    this.selectedSaveDate = this.selectedSaveDate.bind(this);
    this.isUpdated = this.isUpdated.bind(this);
    this.deleteFollowUp = this.deleteFollowUp.bind(this);
    this.deleteFollowUpPoint = this.deleteFollowUpPoint.bind(this);
    this.addToTaskConfirmation = this.addToTaskConfirmation.bind(this);
    this.addToTaskFollowUp = this.addToTaskFollowUp.bind(this);
    this.handleFollowUpClick = this.handleFollowUpClick.bind(this);
    this.onEditKeyDown = this.onEditKeyDown.bind(this);
    this.handleInput = this.handleInput.bind(this);
  }
  deleteFollowUpPoint(event, value) {
    event.stopPropagation();
    this.setState({
      isDelete: true,
      currentObject: value,
    });
  }
  deleteFollowUp() {
    const { currentObject } = this.state;
    const { currentMeeting } = this.props;
    this.setState({ deleteBtnQuery: "progress" }, () => {
      this.props.deleteAction(currentObject, "followUp", currentMeeting, () => {
        this.setState({ deleteBtnQuery: "", isDelete: false });
      });
    });
  }
  handleDialogClose = () => {
    this.setState({ addToTask: false });
  };
  handleDeleteDialogClose = () => {
    this.setState({ isDelete: false });
  };

  addToTaskConfirmation(value) {
    this.setState({
      addToTask: true,
      currentObject: value,
    });
  }
  addToTaskFollowUp() {
    this.setState({ convertToTaskBtnQuery: "progress" }, () => {
      const { currentObject: followUpAction } = this.state;
      this.props.convertFollowupToTask(
        followUpAction.id,
        followUpAction,
        resp => {
          this.setState({ convertToTaskBtnQuery: "", addToTask: false });
        },
        err => { }
      );
    });
  }
  isUpdated(data, dateData, isDate) {
    let obj = data;
    if (isDate) {
      obj = dateData;
      obj.dueDateString = helper.RETURN_CUSTOMDATEFORMAT(data);
    } else {
      obj.assigneeList = obj.attendeeList;
      delete obj.attendeeList;
    }
    this.props.updateAction(obj, "followUp", this.props.currentMeeting);
  }

  followUpAssigneeUpdate = (list, isNewAdded, followUpAction) => {
    const { currentMeeting } = this.props;
    if (isNewAdded == "no") {
      let obj = followUpAction;
      let checkedAssignee = [];
      if (isNewAdded && list && list.length >= 0) {
        checkedAssignee = this.props.members.filter(m => {
          return (
            findIndex(list, function (o) {
              return o.userId === m.userId;
            }) >= 0
          );
        });
      }
      list = list.map(member => member.id);
      obj.assigneeList = list;
      // this.setState({ assignees: list, checkedAssignee });
      this.props.updateAction(obj, "followUp", currentMeeting);
    }
  };

  selectedSaveDate(date) {
    this.setState({ date });
  }
  selectAssignee(list, isNewAdded) {
    let checkedAssignee = [];
    if (isNewAdded && list && list.length >= 0) {
      checkedAssignee = this.props.members.filter(m => {
        return (
          findIndex(list, function (o) {
            return o.id === m.userId;
          }) >= 0
        );
      });
    }
    list = list.map(member => member.id);
    this.setState({ assignees: list, checkedAssignee });
  }
  handleBlur(event, name) {
    this.setState({ addFollowup: false });
  }
  onAddFollowup() {
    this.setState({ addFollowup: true });
  }
  onKeyDown(event) {
    const { followUp } = this.props;
    if (event.keyCode == 13) {
      if (followUp == "") this.props.saveAction("followUp", this.state, () => { });
      else this.setState({ disableField: true }, () => {
        this.props.saveAction("followUp", this.state, () => {
          this.setState({ disableField: false, addFollowup: false, assignees: [], checkedAssignee: [], date: moment() })
        });
      });
    } else if (event.keyCode == 27) {
      this.setState({ addFollowup: false });
    }
  }
  handleEditBlur(event, name) {
    this.setState({ [`editFollowUp${name}`]: false });
  }
  handleInput(event, name) {
    this.setState({ [name]: event.target.value });
  }
  onEditKeyDown(event, name, data) {
    if (event.key === "Enter") {
      this.setState({ [`editFollowUp${name}`]: false });
      if (data) {
        data.description = this.state[name];
      }
      this.props.updateAction(data, "followUp", this.props.currentMeeting);
    } else if (event.key == "Escape") {
      this.setState({ [`editFollowUp${name}`]: false });
    }
  }
  handleFollowUpClick(event, value, name) {
    this.setState({ [name]: value, [`editFollowUp${name}`]: true });
  }
  render() {
    const {
      classes,
      theme,
      inputAction,
      followUp,
      followUpArray,
      members,
      currentMeeting,
      isError,
      meetingPer,
    } = this.props;
    const intl = this.props.intl;
    const {
      addFollowup,
      isDelete,
      addToTask,
      checkedAssignee,
      deleteBtnQuery,
      convertToTaskBtnQuery,
      disableField
    } = this.state;
    let completeDetails = followUpArray.map(x => {
      x.assigneeLists = members.filter(m => {
        if (x.assigneeList && x.assigneeList.length >= 0)
          return x.assigneeList.indexOf(m.userId) >= 0;
      });
      return x;
    });

    const readOnly = getReadOnlyPermissions(currentMeeting);

    const deletefollowUp = meetingPer.meetingDetail.meetingFollowUpActions.isAllowDelete;
    const edit = meetingPer.meetingDetail ? meetingPer.meetingDetail.meetingFollowUpActions.isAllowEdit : true;
    const add = meetingPer.meetingDetail.meetingFollowUpActions.isAllowAdd;

    return (
      <Fragment>
        {!readOnly ? (
          <Fragment>
            <div className={`${classes.MeetingDetailsHeading} flex_center_start_row`}>
              {/* <img src={AgendaIcon} alt="Meeting Followup Action" /> */}
              {/* <AgendaIcon htmlColor={theme.palette.primary.light} /> */}
              <SvgIcon
                viewBox="0 0 14.2 14.2"
                className={classes.meetingDetailsHeadingIcon}
              >
                <MeetingFollowUpIcon />
              </SvgIcon>
              <Typography variant="h5" classes={{ root: classes.meetingDetailsHeadingText }}>
                <FormattedMessage
                  id="meeting.detail-dialog.follow-up-actions.label"
                  defaultMessage="Follow Up Actions"></FormattedMessage>
              </Typography>
            </div>
            {addFollowup && (add === undefined || add) ? (
              <ClickAwayListener onClickAway={event => this.handleBlur(event, "addFollowup")}>
                <div style={{ position: "relative" }}>
                  <DefaultTextField
                    fullWidth={true}
                    formControlStyles={{ marginBottom: 0, marginTop: 0, }}
                    errorState={isError}
                    errorMessage={isError}
                    noBorderRadius={true}
                    label={false}
                    defaultProps={{
                      type: "text",
                      onChange: event => inputAction(event, "followUp"),
                      // oBlur: event => this.handleBlur(event, "addFollowup"),
                      onKeyDown: this.onKeyDown,
                      autoFocus: true,
                      value: followUp,
                      placeholder: intl.formatMessage({
                        id: "meeting.detail-dialog.follow-up-actions.innerplaceholder",
                        defaultMessage: "Enter Follow Up Actions",
                      }),
                      inputProps: {
                        style: { paddingRight: 160 },
                        maxLength: 250,
                      },
                      disabled: disableField
                    }}
                  />
                  <div className={`${classes.addFollowUpExtraCnt} flex_center_start_row`}>
                    <div className={classes.staticDatePickerCnt}>
                      <StaticDatePicker
                        selectedSaveDate={this.selectedSaveDate}
                        isCreation={true}
                        // minDate={true}
                      />
                    </div>
                    <MemberListMenu
                      assigneeList={checkedAssignee}
                      placementType="top-start"
                      buttonStyles={{ padding: 3 }}
                      selectAssigneeList={this.selectAssignee}
                      viewType="meeting"
                      checkAssignee={true}
                      checkedType="single"
                      listType="assignee"
                      keyType=""
                      isFollowUp={true}
                      currentMeeting={currentMeeting}
                      isNewAdded={true}
                      iconSize="xsmall"
                      disablePortal={true}
                    />
                    <div className={classes.CloseIconBtn}>
                      <IconButton btnType="smallFilledGray" style={{ padding: 0 }}>
                        <CloseIcon fontSize="small" htmlColor={theme.palette.common.white} />
                      </IconButton>
                    </div>
                  </div>
                </div>
              </ClickAwayListener>
            ) : (
              <div
                onClick={this.onAddFollowup}
                className={`${classes.addMeetingDetailBtn} flex_center_start_row`}>
                <AddIcon
                  htmlColor={theme.palette.text.primary}
                  className={classes.addMeetingDetailPlus}
                />
                <Typography variant="body1">
                  <FormattedMessage
                    id="meeting.detail-dialog.follow-up-actions.placeholder"
                    defaultMessage="Add Follow Up Actions"
                  />
                </Typography>
              </div>
            )}
            <ul className={classes.addMeetingDetailList}>
              {completeDetails.map(x => {
                return this.state[`editFollowUp${x.followUpActionId}`] ? (
                  <li className={classes.inputListItem} key={x.followUpActionId}>
                    <Hotkeys
                      keyName="enter,esc"
                      onKeyDown={event => this.onEditKeyDown(event, x.followUpActionId, x)}>
                      <DefaultTextField
                        fullWidth={true}
                        formControlStyles={{ marginBottom: 0, marginTop: 0 }}
                        error={false}
                        noBorderRadius={true}
                        label={false}
                        defaultProps={{
                          type: "text",
                          onChange: event => this.handleInput(event, x.followUpActionId),
                          onBlur: event => this.handleEditBlur(event, x.followUpActionId),
                          onKeyDown: event => this.onEditKeyDown(event, x.followUpActionId, x),
                          autoFocus: true,
                          value: this.state[x.followUpActionId],
                          placeholder: "Enter Follow Up Action",
                        }}
                      />
                    </Hotkeys>
                  </li>
                ) : (
                  <li className="flex_center_space_between_row">
                    <div
                      className="flex_center_start_row"
                      onClick={event =>
                        edit &&
                        this.handleFollowUpClick(event, x.description, x.followUpActionId)
                      }>
                      <RoundIcon className={classes.listIcon} />
                      <div className="flex_center_start_col">
                        {x.description}
                        <Typography
                          classes={{
                            body2: classes.followUpActionDetails,
                          }}
                          variant="body2">
                          {`${members.find(m => m.userId === (x.updatedBy || x.createdBy))
                            .fullName ||
                            members.find(m => m.userId === (x.updatedBy || x.createdBy)).userName ||
                            members.find(m => m.userId === (x.updatedBy || x.createdBy))
                              .email} - ${helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(
                                x.updatedDate
                              )} - ${helper.RETURN_CUSTOMDATEFORMATFORTIME(x.updatedDate)}`}
                        </Typography>
                      </div>
                    </div>
                    <div className="flex_center_start_row">
                      {!x.isTaskCreated && edit && (
                        <Typography
                          classes={{ root: classes.convertToTask }}
                          variant="body2"
                          onClick={this.addToTaskConfirmation.bind(this, x)}>
                          <FormattedMessage
                            id="meeting.detail-dialog.convert-to-task.label"
                            defaultMessage="Convert to Task"
                          />
                        </Typography>
                      )}
                      <div className={classes.staticDatePickerCnt}>
                        <CalendarIcon
                          htmlColor={theme.palette.secondary.light}
                          style={{ marginRight: "5px", fontSize: "17px" }}
                        />
                        {edit ? (
                          <StaticDatePicker
                            updateDate={this.isUpdated}
                            data={x}
                            isFollowUp={true}
                            // minDate={true}
                          />
                        ) : (
                          <Typography variant="body2">
                            {helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(
                              x.dueDateString || x.dueDate
                            )}
                          </Typography>
                        )}
                      </div>
                      {x.assigneeLists.length > 0 ? (
                        <MemberListMenu
                          assigneeList={x.assigneeLists || []}
                          MenuData={x}
                          buttonStyles={{ padding: 3 }}
                          placementType="top-start"
                          isUpdated={this.isUpdated}
                          viewType="meeting"
                          action={true}
                          checkAssignee={true}
                          listType="assignee"
                          keyType=""
                          showListOnly={!edit}
                          isFollowUp={true}
                          currentMeeting={currentMeeting}
                          hideButton={true}
                          iconSize="xsmall"
                          disabled={!edit}
                        />
                      ) : (
                        <MemberListMenu
                          assigneeList={checkedAssignee}
                          placementType="top-start"
                          buttonStyles={{ padding: 3 }}
                          selectAssigneeList={this.followUpAssigneeUpdate}
                          viewType="meeting"
                          checkAssignee={true}
                          checkedType="single"
                          listType="assignee"
                          keyType=""
                          isFollowUp={true}
                          currentMeeting={currentMeeting}
                          isNewAdded={true}
                          iconSize="xsmall"
                          disablePortal={true}
                          followUpAction={x}
                          disabled={!edit}
                        />
                      )}

                      <div className={classes.CloseIconBtn}>
                        {deletefollowUp ? (
                          <IconButton
                            btnType="smallFilledGray"
                            style={{ padding: 0 }}
                            onClick={event => {
                              this.deleteFollowUpPoint(event, x);
                            }}>
                            <CloseIcon fontSize="small" htmlColor={theme.palette.common.white} />
                          </IconButton>
                        ) : null}
                      </div>
                    </div>
                  </li>
                );
              })}
            </ul>
            <DeleteConfirmDialog
              open={isDelete}
              closeAction={this.handleDeleteDialogClose}
              cancelBtnText={
                <FormattedMessage
                  id="common.action.cancel.label"
                  defaultMessage="Cancel"></FormattedMessage>
              }
              successBtnText={
                <FormattedMessage
                  id="meeting.detail-dialog.follow-up-actions.delete-confirmation.delete-button.label"
                  defaultMessage="Delete"></FormattedMessage>
              }
              alignment="center"
              headingText={
                <FormattedMessage
                  id="meeting.detail-dialog.follow-up-actions.delete-confirmation.title"
                  defaultMessage="Delete"></FormattedMessage>
              }
              successAction={this.deleteFollowUp}
              msgText={
                <FormattedMessage
                  id="meeting.detail-dialog.follow-up-actions.delete-confirmation.label"
                  defaultMessage="Are you sure you want to delete this follow up action?"></FormattedMessage>
              }
              btnQuery={deleteBtnQuery}
            />

          </Fragment>
        ) : (
          <Fragment>
            <div className={`${classes.MeetingDetailsHeading} flex_center_start_row`}>
              {/* <img src={AgendaIcon} alt="Meeting Followup Action" /> */}
              {/* <AgendaIcon htmlColor={theme.palette.primary.light} /> */}
              <SvgIcon
                viewBox="0 0 14.2 14.2"
                className={classes.meetingDetailsHeadingIcon}
              >
                <MeetingFollowUpIcon />
              </SvgIcon>
              <Typography variant="h5" classes={{ root: classes.meetingDetailsHeadingText }}>
                <FormattedMessage
                  id="meeting.detail-dialog.follow-up-actions.label"
                  defaultMessage="Follow Up Actions"></FormattedMessage>{" "}
              </Typography>
            </div>
            <ul className={classes.addMeetingDetailList}>
              {completeDetails.map(x => {
                return this.state[`editFollowUp${x.followUpActionId}`] ? (
                  <li className={classes.inputListItem} key={x.followUpActionId}>
                    <Hotkeys
                      keyName="enter,esc"
                      onKeyDown={event => this.onEditKeyDown(event, x.followUpActionId, x)}>
                      <DefaultTextField
                        fullWidth={true}
                        formControlStyles={{ marginBottom: 0, marginTop: 0 }}
                        error={false}
                        noBorderRadius={true}
                        label={false}
                        defaultProps={{
                          type: "text",
                          onChange: event => this.handleInput(event, x.followUpActionId),
                          onBlur: event => this.handleEditBlur(event, x.followUpActionId),
                          autoFocus: true,
                          value: this.state[x.followUpActionId],
                          placeholder: "Enter Follow Up Action",
                        }}
                      />
                    </Hotkeys>
                  </li>
                ) : (
                  <li className="flex_center_space_between_row">
                    <div className="flex_center_start_row">
                      <RoundIcon className={classes.listIcon} />
                      <div className="flex_center_start_col">
                        {x.description}
                        <Typography
                          classes={{
                            body2: classes.followUpActionDetails,
                          }}
                          variant="body2">
                          {`${members.find(m => m.userId === (x.updatedBy || x.createdBy))
                            .fullName ||
                            members.find(m => m.userId === (x.updatedBy || x.createdBy)).userName ||
                            members.find(m => m.userId === (x.updatedBy || x.createdBy))
                              .email} - ${helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(
                                x.updatedDate
                              )} - ${helper.RETURN_CUSTOMDATEFORMATFORTIME(x.updatedDate)}`}
                        </Typography>
                      </div>
                    </div>
                    <div className="flex_center_start_row">
                      {!x.isTaskCreated && edit && (
                        <Typography
                          classes={{ root: classes.convertToTask }}
                          variant="body2"
                          onClick={this.addToTaskConfirmation.bind(this, x)}>
                          <FormattedMessage
                            id="meeting.detail-dialog.convert-to-task.label"
                            defaultMessage="Convert to Task"
                          />
                        </Typography>
                      )}
                      <div className={classes.staticDatePickerCnt}>
                        <Typography variant="body2">
                          {x
                            ? helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(x.dueDate)
                            : helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(new Date())}
                        </Typography>
                      </div>
                      <div className={classes.staticDatePickerCnt}>
                        <MemberListMenu
                          //id={value.row ? value.row.id : ""}
                          assigneeList={x.assigneeLists || []}
                          MenuData={x}
                          buttonStyles={{ padding: 3 }}
                          placementType="bottom-start"
                          isUpdated={this.isUpdated}
                          viewType="meeting"
                          checkAssignee={true}
                          checkedType="multi"
                          listType="assignee"
                          showListOnly={true}
                        />
                      </div>
                    </div>
                  </li>
                );
              })}
            </ul>
          </Fragment>
        )}
        <ActionConfirmation
          open={addToTask}
          closeAction={this.handleDialogClose}
          cancelBtnText={
            <FormattedMessage
              id="common.action.cancel.label"
              defaultMessage="Cancel"></FormattedMessage>
          }
          successBtnText={
            <FormattedMessage
              id="common.action.create.label"
              defaultMessage="Create"></FormattedMessage>
          }
          alignment="center"
          headingText={
            <FormattedMessage
              id="meeting.detail-dialog.convert-to-task.confirmation.title"
              defaultMessage="Convert to Task"
            />
          }
          iconType="archive"
          msgText={
            <FormattedMessage
              id="meeting.detail-dialog.convert-to-task.confirmation.label"
              defaultMessage="Are you sure you want to convert follow up point to task?"
            />
          }
          successAction={this.addToTaskFollowUp}
          btnQuery={convertToTaskBtnQuery}
        />
      </Fragment>
    );
  }
}

export default injectIntl(FollowUp);
