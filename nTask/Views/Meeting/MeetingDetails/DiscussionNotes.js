import React, { Component, Fragment } from "react";
import DefaultTextField from "../../../components/Form/TextField";
import Typography from "@material-ui/core/Typography";
// import AgendaIcon from "@material-ui/icons/AssignmentOutlined";
import RoundIcon from "@material-ui/icons/Brightness1";
import Hotkeys from "react-hot-keys";
import StaticTextEditor from "../../../components/TextEditor/StaticTextEditor";
import helper from "../../../helper";
import ReactHtmlParser from 'react-html-parser';
import CloseIcon from "@material-ui/icons/Close";
import IconButton from "../../../components/Buttons/IconButton";
import DeleteConfirmDialog from "../../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import { getReadOnlyPermissions } from "../permissions";
import AgendaIcon from "../../../assets/images/meeting_discussion_notes.svg";
import { GetPermission } from "../../../components/permissions";
import { FormattedMessage, injectIntl } from "react-intl";
import SvgIcon from "@material-ui/core/SvgIcon";
import MeetingDiscussionIcon from "../../../components/Icons/MeetingDiscussionIcon";

class DiscussionNotes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDelete: false,
      currentObject: {},
      deleteBtnQuery: ""
    };
    this.onKeyDown = this.onKeyDown.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.deleteDiscussion = this.deleteDiscussion.bind(this);
    this.deleteDiscussionPoint = this.deleteDiscussionPoint.bind(this);
  }
  deleteDiscussionPoint(event, value) {
    event.stopPropagation();
    this.setState({
      isDelete: true,
      currentObject: value
    });
  }
  deleteDiscussion() {
    const { currentObject } = this.state;
    const { currentMeeting } = this.props;
    this.setState({ deleteBtnQuery: "progress" }, () => {
      this.props.deleteAction(
        currentObject,
        "discussion",
        currentMeeting,
        () => {
          this.setState({ deleteBtnQuery: "", isDelete: false });
        }
      );
    });
  }
  handleDeleteDialogClose = () => {
    this.setState({ isDelete: false });
  };
  handleBlur(event, name) {
    this.setState({ addDiscussion: false });
  }

  onKeyDown(event, comments) {
    this.setState({ addDiscussion: false });
  }

  render() {
    const {
      classes,
      theme,
      inputAction,
      discussion,
      discussionArray,
      members,
      currentMeeting,
      meetingPer
    } = this.props;
    const {
      addDiscussion,
      isDelete,
      currentObject,
      deleteBtnQuery
    } = this.state;
    const readOnly = getReadOnlyPermissions(currentMeeting);

    const deleteDiscussion = meetingPer.meetingDetail.meetingDiscussionNotes.isAllowDelete;
    const add = meetingPer.meetingDetail.meetingDiscussionNotes.isAllowAdd;

    return (
      <Fragment>
        {!readOnly ? (
          <Fragment>
            <div
              className={`${classes.MeetingDetailsHeading} flex_center_start_row`}
            >
              {/* <img src={AgendaIcon} alt="Meeting Discussion Notes" /> */}
              {/* <AgendaIcon htmlColor={theme.palette.primary.light} /> */}
              <SvgIcon
                viewBox="0 0 14.031 16"
                className={classes.meetingDetailsHeadingIcon}
              >
                <MeetingDiscussionIcon />
              </SvgIcon>
              <Typography
                variant="h5"
                classes={{ root: classes.meetingDetailsHeadingText }}
              >
                <FormattedMessage id="meeting.detail-dialog.discussion-notes.label" defaultMessage="Discussion Notes" ></FormattedMessage>
              </Typography>
            </div>

            {/* {addDiscussion && (add === undefined || add) ? ( */}
            <Hotkeys keyName="esc" onKeyDown={event => this.onKeyDown(event)}>
              <StaticTextEditor
                plainEditor={true}
                handleNewComments={this.onKeyDown}
                saveAction={this.props.saveAction}
                blurAction={this.handleBlur}
                addPermission={add}
              />
            </Hotkeys>
            {/* ) : (
             
            )} */}
            <ul className={classes.addMeetingDetailList}>
              {discussionArray.map(x => {
                return (
                  <li
                    className="flex_center_space_between_row"
                    key={x.discussionPointId}
                  >
                    <div className="flex_center_start_row">
                      <RoundIcon className={classes.listIcon} />

                      <div className="flex_center_start_col">
                        {ReactHtmlParser(x.description)}
                        <Typography
                          classes={{
                            body2: classes.followUpActionDetails
                          }}
                          variant="body2"
                        >
                          {`${members.find(
                            m => m.userId === (x.updatedBy || x.createdBy)
                          ).fullName ||
                            members.find(
                              m => m.userId === (x.updatedBy || x.createdBy)
                            ).userName ||
                            members.find(
                              m => m.userId === (x.updatedBy || x.createdBy)
                            )
                              .email} - ${helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(
                                x.updatedDate
                              )} - ${helper.RETURN_CUSTOMDATEFORMATFORTIME(
                                x.updatedDate
                              )}`}
                        </Typography>
                      </div>
                    </div>
                    <div className={classes.CloseIconBtn}>
                      {deleteDiscussion ? (
                        <IconButton
                          btnType="smallFilledGray"
                          style={{ padding: 0 }}
                          onClick={event => {
                            this.deleteDiscussionPoint(event, x);
                          }}
                        >
                          <CloseIcon
                            fontSize="small"
                            htmlColor={theme.palette.common.white}
                          />
                        </IconButton>
                      ) : null}
                    </div>
                  </li>
                );
              })}
            </ul>
            <DeleteConfirmDialog
              open={isDelete}
              closeAction={this.handleDeleteDialogClose}
              cancelBtnText={<FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel"></FormattedMessage>}
              successBtnText={<FormattedMessage id="meeting.detail-dialog.discussion-notes.delete-confirmation.delete-button.label" defaultMessage="Delete"></FormattedMessage>}
              alignment="center"
              headingText={<FormattedMessage id="meeting.detail-dialog.discussion-notes.delete-confirmation.title" defaultMessage="Delete"></FormattedMessage>}
              successAction={this.deleteDiscussion}
              msgText={<FormattedMessage id="meeting.detail-dialog.discussion-notes.delete-confirmation.label" defaultMessage="Are you sure you want to delete this discussion point?"></FormattedMessage>}
              btnQuery={deleteBtnQuery}
            />
          </Fragment>
        ) : (
          <Fragment>
            <div
              className={`${classes.MeetingDetailsHeading} flex_center_start_row`}
            >
              {/* <img src={AgendaIcon} alt="Meeting Discussion Notes" /> */}
              {/* <AgendaIcon htmlColor={theme.palette.primary.light} /> */}
              <SvgIcon
                viewBox="0 0 14.031 16"
                className={classes.meetingDetailsHeadingIcon}
              >
                <MeetingDiscussionIcon />
              </SvgIcon>
              <Typography
                variant="h5"
                classes={{ root: classes.meetingDetailsHeadingText }}
              >
                <FormattedMessage id="meeting.detail-dialog.discussion-notes.label" defaultMessage="Discussion Notes"></FormattedMessage>
              </Typography>
            </div>

            <ul className={classes.addMeetingDetailList}>
              {discussionArray.map(x => {
                return (
                  <li
                    className="flex_center_space_between_row"
                    key={x.discussionPointId}
                  >
                    <div className="flex_center_start_row">
                      <RoundIcon className={classes.listIcon} />

                      <div className="flex_center_start_col">
                        {ReactHtmlParser(x.description)}
                        <Typography
                          classes={{
                            body2: classes.followUpActionDetails
                          }}
                          variant="body2"
                        >
                          {`${members.find(
                            m => m.userId === (x.updatedBy || x.createdBy)
                          ).fullName ||
                            members.find(
                              m => m.userId === (x.updatedBy || x.createdBy)
                            ).userName ||
                            members.find(
                              m => m.userId === (x.updatedBy || x.createdBy)
                            )
                              .email} - ${helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(
                                x.updatedDate
                              )} - ${helper.RETURN_CUSTOMDATEFORMATFORTIME(
                                x.updatedDate
                              )}`}
                        </Typography>
                      </div>
                    </div>
                  </li>
                );
              })}
            </ul>
          </Fragment>
        )}
      </Fragment>
    );
  }
}

export default DiscussionNotes;
