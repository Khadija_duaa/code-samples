import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import classes from "./styles";
import Typography from "@material-ui/core/Typography";

class ZoomDescription extends Component {
    constructor(props){
        super();
    }
    render(){
        const { classes, teamDescription, createrName} = this.props;
        return(
            <Fragment>
                <div style={{marginLeft: 1 }}>
                    <Typography
                        variant="h6"
                        classes={{h6: classes.zoomLabel}}
                        >
                        Microsoft Team Description
                    </Typography>
                </div>
                <div className={classes.meetingDescriptionCnt}>
                    <Typography variant="h6" >
                        {createrName} is inviting you to a scheduled Microsoft Team meeting.
                    </Typography>
                    <div className={classes.joinSection}>
                        <Typography variant="h6" style={{ fontWeight: 700 }}>
                            Join Microsoft Team Meeting
                        </Typography>
                        <a href={teamDescription.onlineMeeting.joinUrl} target="_blank" >{teamDescription.onlineMeeting.joinUrl}</a>
                    </div>
                    {/* <span className={classes.subSection}>
                        <Typography variant="h6" style={{ fontWeight: 700 }}>
                            Meeting ID:
                        </Typography>
                        <Typography variant="h6" classes={{h6: classes.subSecValue}}>
                            {teamDescription.id}
                        </Typography>
                    </span> */}
                </div>
            </Fragment>
        );
    }
}
const mapStateToProps = state => {
    return {
    };
  };
export default compose(
    withRouter,
    connect(
      mapStateToProps,{}
    ),
    withStyles(classes, { withTheme: true })
  )(ZoomDescription);
