import React, { Component, Fragment } from "react";
import DefaultTextField from "../../../components/Form/TextField";
import Typography from "@material-ui/core/Typography";
// import AgendaIcon from "@material-ui/icons/AssignmentOutlined";
import AddIcon from "@material-ui/icons/Add";
import RoundIcon from "@material-ui/icons/Brightness1";
import Hotkeys from "react-hot-keys";
import CloseIcon from "@material-ui/icons/Close";
import IconButton from "../../../components/Buttons/IconButton";
import DeleteConfirmDialog from "../../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import { getReadOnlyPermissions } from "../permissions";
// import AgendaIcon from "../../../assets/images/meeting_agenda.svg";
import { GetPermission } from "../../../components/permissions";
import { injectIntl, FormattedMessage } from "react-intl";
import MeetingAgendaIcon from "../../../components/Icons/MeetingAgendaIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
class Agenda extends Component {
  constructor(props) {
    super(props);
    this.state = {
      addAgenda: true,
      isDelete: false,
      currentObject: {},
      deleteBtnQuery: "",
      disableField: false
    };
    this.onAddAgenda = this.onAddAgenda.bind(this);
    this.onKeyDown = this.onKeyDown.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.handleEditBlur = this.handleEditBlur.bind(this);
    this.handleAgendaClick = this.handleAgendaClick.bind(this);
    this.onEditKeyDown = this.onEditKeyDown.bind(this);
    this.handleInput = this.handleInput.bind(this);
    this.deleteAgenda = this.deleteAgenda.bind(this);
    this.deleteAgendaAfterConfirmation = this.deleteAgendaAfterConfirmation.bind(
      this
    );
  }
  deleteAgendaAfterConfirmation() {
    const { currentObject } = this.state;
    const { currentMeeting } = this.props;
    this.setState({ deleteBtnQuery: "progress" }, () => {
      this.props.deleteAction(currentObject, "agenda", currentMeeting, () => {
        this.setState({ deleteBtnQuery: "", isDelete: false });
      });
    });
  }
  handleDeleteDialogClose = () => {
    this.setState({ isDelete: false });
  };
  deleteAgenda(event, value) {
    event.stopPropagation();
    this.setState({ isDelete: true, currentObject: value });
  }
  handleBlur(event, name) {
    this.setState({ [name]: false, addAgenda: false });
  }

  onAddAgenda() {
    this.setState({ addAgenda: true });
  }
  onKeyDown(event) {
    const { agenda } = this.props;
    if (event === "enter") {
      if (agenda == "") this.props.saveAction("agenda", {}, () => { });
      else this.setState({ disableField: true }, () => {
        this.props.saveAction("agenda", {}, () => {
          this.setState({ disableField: false, addAgenda: false })
        });
      })
    } else if (event == "esc") {
      this.setState({ addAgenda: false });
    }
  }
  handleEditBlur(event, name) {
    this.setState({ [`editAgenda${name}`]: false });
  }
  handleInput(event, name) {
    this.setState({ [name]: event.target.value });
  }
  onEditKeyDown(event, name, data) {
    if (event.key === "Enter") {
      if (data) {
        data.description = this.state[name];
      }
      this.setState({ [`editAgenda${name}`]: false });
      this.props.updateAction(data, "agenda", this.props.currentMeeting);
    } else if (event.key == "Escape") {
      this.setState({ [`editAgenda${name}`]: false });
    }
  }
  handleAgendaClick(event, value, name) {
    this.setState({ [name]: value, [`editAgenda${name}`]: true });
  }
  render() {
    const {
      classes,
      theme,
      inputAction,
      agenda,
      agendaArray,
      isError,
      currentMeeting,
      meetingPer
    } = this.props;
    const intl = this.props.intl;
    const { addAgenda, isDelete, deleteBtnQuery, disableField } = this.state;
    const readOnly = getReadOnlyPermissions(currentMeeting);
    const deleteAgenda = meetingPer.meetingDetail.meetingAgenda.isAllowDelete;
    const edit = meetingPer.meetingDetail.meetingAgenda.isAllowEdit;
    const add = meetingPer.meetingDetail.meetingAgenda.isAllowAdd;
    return (
      <Fragment>
        {!readOnly ? (
          <Fragment>
            <div
              className={`${classes.MeetingDetailsHeading} flex_center_start_row`}
            >
              <SvgIcon
                viewBox="0 0 13.5 16.938"
                className={classes.meetingDetailsHeadingIcon}
              >
                <MeetingAgendaIcon />
              </SvgIcon>
              <Typography
                variant="h5"
                classes={{ root: classes.meetingDetailsHeadingText }}
              >
                <FormattedMessage id="meeting.detail-dialog.meeting-agenda.label" defaultMessage="Meeting Agenda" ></FormattedMessage>
              </Typography>
            </div>
            {addAgenda && (add === undefined || add) ? (
              <Hotkeys keyName="enter,esc" onKeyDown={this.onKeyDown}>
                <div>
                  <DefaultTextField
                    fullWidth={true}
                    formControlStyles={{ marginBottom: 0, marginTop: 0 }}
                    errorState={isError}
                    errorMessage={isError}
                    noBorderRadius={true}
                    label={false}
                    defaultProps={{
                      disabled: disableField,
                      type: "text",
                      onChange: event => inputAction(event, "agenda"),
                      onBlur: event => this.handleBlur(event, "addAgenda"),
                      onKeyDown: (event) => {
                        event.keyCode == 13 && this.onKeyDown("enter");
                      },
                      autoFocus: true,
                      value: agenda,
                      placeholder: intl.formatMessage({ "id": "meeting.detail-dialog.meeting-agenda.innerplaceholder", defaultMessage: "Enter Agenda" }),
                      inputProps: {
                        maxLength: 250,

                      }
                    }}
                  />
                </div>
              </Hotkeys>
            ) : (
              <div
                onClick={this.onAddAgenda}
                className={`${classes.addMeetingDetailBtn} flex_center_start_row`}
              >
                <AddIcon
                  htmlColor={theme.palette.text.primary}
                  className={classes.addMeetingDetailPlus}
                />
                <Typography variant="body1"><FormattedMessage id="meeting.detail-dialog.meeting-agenda.placeholder" defaultMessage="Add Agenda" ></FormattedMessage> </Typography>
              </div>
            )}

            <ul className={classes.addMeetingDetailList}>
              {agendaArray.map((x, i) => {
                return this.state[`editAgenda${x.agendaId}`] ? (
                  <li className={classes.inputListItem} key={x.agendaId}>
                    <Hotkeys
                      keyName="enter,esc"
                      onKeyDown={event => this.onEditKeyDown(event, x.agendaId, x)}
                    >
                      <div>
                        <DefaultTextField
                          fullWidth={true}
                          formControlStyles={{ marginBottom: 0, marginTop: 0 }}
                          error={false}
                          noBorderRadius={true}
                          label={false}
                          defaultProps={{
                            type: "text",
                            onChange: event => this.handleInput(event, x.agendaId),
                            onBlur: event => this.handleEditBlur(event, x.agendaId),
                            onKeyDown: (event) => this.onEditKeyDown(event, x.agendaId, x),
                            autoFocus: true,
                            value: this.state[x.agendaId],
                            placeholder: intl.formatMessage({ "id": "meeting.detail-dialog.meeting-agenda.innerplaceholder", defaultMessage: "Enter Agenda" }),
                          }}
                        />
                      </div>
                    </Hotkeys>
                  </li>
                ) : (
                  <li
                    className="flex_center_space_between_row"
                    onClick={event =>
                      (edit === undefined || edit) &&
                      this.handleAgendaClick(event, x.description, x.agendaId)
                    }
                    key={x.agendaId}
                  >
                    <div>
                      <RoundIcon className={classes.listIcon} />
                      {x.description}
                    </div>
                    <div className={classes.CloseIconBtn}>
                      {deleteAgenda ? (
                        <IconButton
                          btnType="smallFilledGray"
                          style={{ padding: 0 }}
                          onClick={event => {
                            this.deleteAgenda(event, x);
                          }}
                        >
                          <CloseIcon
                            fontSize="small"
                            htmlColor={theme.palette.common.white}
                          />
                        </IconButton>
                      ) : null}
                    </div>
                  </li>
                );
              })}
            </ul>
            <DeleteConfirmDialog
              open={isDelete}
              closeAction={this.handleDeleteDialogClose}
              cancelBtnText={<FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel"></FormattedMessage>}
              successBtnText={<FormattedMessage id="meeting.detail-dialog.meeting-agenda.delete-confirmation.delete-button.label" defaultMessage="Delete"></FormattedMessage>}
              alignment="center"
              headingText={<FormattedMessage id="meeting.detail-dialog.meeting-agenda.delete-confirmation.title" defaultMessage="Delete"></FormattedMessage>}
              successAction={this.deleteAgendaAfterConfirmation}
              msgText={<FormattedMessage id="meeting.detail-dialog.meeting-agenda.delete-confirmation.label" defaultMessage="Are you sure you want to delete this agenda?"></FormattedMessage>}
              btnQuery={deleteBtnQuery}
            />
          </Fragment>
        ) : (
          <Fragment>
            <div
              className={`${classes.MeetingDetailsHeading} flex_center_start_row`}
            >
              {/* <img src={AgendaIcon} alt="Meeting Agenda Image" /> */}
              {/* <AgendaIcon htmlColor={theme.palette.primary.light} /> */}
              <SvgIcon
                viewBox="0 0 13.5 16.938"
                htmlColor={theme.palette.secondary.medDark}
                className={classes.meetingDetailsHeadingIcon}
              >
                <MeetingAgendaIcon />
              </SvgIcon>
              <Typography
                variant="h5"
                classes={{ root: classes.meetingDetailsHeadingText }}
              >
                {intl.formatMessage({ "id": "meeting.detail-dialog.meeting-agenda.label", defaultMessage: "Meeting Agenda" })}
              </Typography>
            </div>
            <ul className={classes.addMeetingDetailList}>
              {agendaArray.map((x, i) => {
                return (
                  <li
                    className="flex_center_space_between_row"
                    key={x.agendaId}
                  >
                    <div>
                      <RoundIcon className={classes.listIcon} />
                      {x.description}
                    </div>
                  </li>
                );
              })}
            </ul>
          </Fragment>
        )}
      </Fragment>
    );
  }
}

export default injectIntl(Agenda);
