const calendarStyles = theme => ({
  calendarToolbar: {
    marginBottom: 20
  },
  navigationArrow: {
    fontSize: "28px !important"
  },
  toolbarCurrentDate: {
    fontSize: "18px !important",
    fontWeight: 500,
    verticalAlign: "middle",
    display: "inline-block",
    width: 160,
    textAlign: "center"
  },
  NextBackBtnCnt: {
    margin: "4px 0 0 20px"
  },
  toggleContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start"
  },
  toggleBtnGroup: {
    borderRadius: 4,
    background: theme.palette.common.white,
    // border: `1px solid ${theme.palette.border.lightBorder}`,
    boxShadow: "none",
    "& $toggleButtonSelected": {
      color: theme.palette.text.secondary,
      backgroundColor: theme.palette.common.white,
      "&:after": {
        background: theme.palette.common.white
      },
      "&:hover": {
        background: theme.palette.common.white
      }
    }
  },
  groupedHorizontal:{
    borderLeft: `1px solid ${theme.palette.border.lightBorder} !important`
  },
  toggleButton: {
    height: "auto",
    padding: "9px 20px",
    fontSize: "12px !important",
    fontWeight: theme.palette.fontWeightMedium,
    textTransform: "capitalize",
    "&:hover": {
      background: theme.palette.common.white
    },
    "&[value = 'center']": {
      borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
      borderRight: `1px solid ${theme.palette.border.lightBorder}`
    }
  },
  toggleButtonSelected: {},
  recurrenceIcon: {
    fontSize: "23px !important",
    position: "absolute",
    right: 5,
    top: -5,
    background: 'white',
    padding: 3,
    border: '1px solid #cecece',
    borderRadius: 15

  },
  repeatOccuranceStyle: {
    padding: '6px !important',
    '&:hover': {
      color: "white !important",
      background: '#0090ff !important'
    }
  },
  calenderMeetingRender: {
    display: 'flex', 
    alignItems: 'center', 
    justifyContent: 'space-between'
  },
  meetNowIcon:{
    paddingLeft: 5,
    color: theme.palette.common.white
  }
});

export default calendarStyles;
