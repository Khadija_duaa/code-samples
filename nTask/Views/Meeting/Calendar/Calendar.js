import React from "react";
import {Calendar, Views, momentLocalizer} from "react-big-calendar";
import { withStyles, withTheme } from "@material-ui/core/styles";
import withDragAndDrop from "react-big-calendar/lib/addons/dragAndDrop";
import moment from "moment";
// import "react-big-calendar/lib/addons/dragAndDrop/styles.less";
import 'react-big-calendar/lib/css/react-big-calendar.css';
import "../../../assets/css/react-big-calendar.css";
import LeftArrow from "@material-ui/icons/ArrowLeft";
import RightArrow from "@material-ui/icons/ArrowRight";
import CustomIconButton from "../../../components/Buttons/IconButton";
import DefaultButton from "../../../components/Buttons/DefaultButton";
import calendarStyles from "./styles";
import Grid from "@material-ui/core/Grid";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import helper from "../../../helper";
import {
  UpdateMeetingCalender,
  createNextOccurrence,
} from "../../../redux/actions/meetings";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import StaticDatePicker from "./DatePicker/StaticDatePicker";
import CustomButton from "../../../components/Buttons/CustomButton";
import Typography from "@material-ui/core/Typography";
import SvgIcon from "@material-ui/core/SvgIcon";
import RecurrenceIcon from "../../../components/Icons/RecurrenceIcon";
import MeetNowIcon from "../../../components/Icons/MeetNowIcon";
import MeetingDetails from "../MeetingDetails/MeetingDetails";
import DefaultDialog from "../../../components/Dialog/Dialog";
import AddMeetingForm from "../../AddNewForms/AddMeetingForm";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import { withSnackbar } from "notistack";
import { canAdd } from "../permissions";
import { FormattedMessage } from "react-intl";

const DragAndDropCalendar = withDragAndDrop(Calendar);
const localizer = momentLocalizer(moment);

class TaskCalendar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      events: this.meetingEvents(this.props.meetingsState.data),
      view: Views.MONTH,
      date: new Date(),
      btnClicked: "", //this.props.calenderTasks.calenderTasks
      alignment: "right",
      currentMeeting: null,
      currentDate: moment(),
      clickableMeeting: false,
      meetingDate: new Date(),
      meetingStartTime: {},
      meetingDuration: {},
      openRecurrenceConfirm: false,
      createReccurrenceDate: null,
      activeMeetingDisplayName: "",
      currentMeetingOccuranceId: null,
      btnQuery: "",
    };
  }
  meetingEvents(meetings) {
    const { theme } = this.props;
    const meetingStatusColor = {
      Upcoming: theme.palette.meetingStatus.Upcoming,
      Cancelled: theme.palette.meetingStatus.Cancelled,
      OverDue: theme.palette.meetingStatus.OverDue,
      InReview: theme.palette.meetingStatus.InReview,
      Published: theme.palette.meetingStatus.Published,
    };
    const filteredMettings = meetings.map((m) => {
      let meeting = this.getMeetingPer(m)
      let z = m.zoomMeetingId;
      return {
        id: meeting.meetingId,
        meetingOccurance: false,
        title: meeting.meetingDisplayName || "-",
        meetingZoom: z,
        allDay: false,
        start: new Date(
          helper.RETURN_CUSTOMDATEFORMAT(
            moment(new Date(meeting.startDateString)),
            meeting.startTime
          )
        ),
        end: new Date(
          helper.RETURN_CUSTOMDATEFORMAT(
            moment(new Date(meeting.startDateString)),
            meeting.endTime
          )
        ),
        parentId: meeting.parentId,
        desc: meeting.description || "",
        meetingsColor: meeting.status
          ? meetingStatusColor[meeting.status]
          : "#5185fc",
        meetingObj: meeting,
        borderRadius: "0px",
      };
    });
    return filteredMettings;
  }
  getMeetingPer=(m)=>{
    const { projects, tasks, meetingPer } = this.props;

    if (m.taskId) {
      let attachedTask = tasks.find((t) => t.taskId == m.taskId);
      if (attachedTask) {
        let attachedProject = projects.find(
          (p) => p.projectId == attachedTask.projectId
        );
        if (attachedProject) {
          m.meetingPermission =
            attachedProject.projectPermission.permission.meeting;
          return m;
        } else {
          m.meetingPermission = meetingPer;
          return m;
        }
      } else {
        m.meetingPermission = meetingPer;
        return m;
      }
    } else {
      m.meetingPermission = meetingPer;
      return m;
    }
  }
  clearBtnState = () => {
    this.setState({ btnClicked: "" });
  };
  componentDidUpdate(prevProps, prevState) {
    if (
      JSON.stringify(prevProps.meetingsState.data) !==
        JSON.stringify(this.props.meetingsState.data) &&
      prevState.currentMeeting != null
    ) {
      let meeting = this.props.meetingsState.data.find((meeting) => {
        return prevState.currentMeeting.id == meeting.id;
      });
      this.setState({ currentMeeting: meeting });
    }

    if (
      JSON.stringify(prevState.events) !==
      JSON.stringify(this.meetingEvents(this.props.meetingsState.data))
    ) {
      this.setState({
        events: this.meetingEvents(this.props.meetingsState.data),
      });
    }
  }
  updateCalenderMeetings(start, end, id) {
    let index = this.props.meetingsState.data
      .map((meeting) => {
        return meeting.meetingId;
      })
      .indexOf(id);
    let obj = { ...this.props.meetingsState.data[index] };
    obj.startDateString = helper.RETURN_CUSTOMDATEFORMAT(moment(start));
    obj.startTime = moment(start).format("hh:mm A");
    obj.endTime = moment(end).format("hh:mm A");
    obj.endDateString = helper.RETURN_CUSTOMDATEFORMAT(moment(end));
    this.props.UpdateMeetingCalender(obj, () => {});
  }

  onEventDrop = ({ event, start, end, isAllDay: droppedOnAllDaySlot }) => {
    if (!event.meetingOccurance) {
      const { events } = this.state;
      const idx = events.indexOf(event);
      const updatedEvent = { ...event, start, end };
      const nextEvents = [...events];
      nextEvents.splice(idx, 1, updatedEvent);
      this.setState({
        events: nextEvents,
      });
      this.updateCalenderMeetings(start, end, event.id);
    }
  };

  eventStyle(event) {
    if (event.meetingsColor) {
      if (event.borderColor) {
        return {
          style: {
            background: event.meetingsColor,
            border: event.borderColor,
            color: event.color,
            borderRadius: event.borderRadius,
          },
          className: event.className,
        };
      } else {
        return {
          style: {
            background: event.meetingsColor,
            borderRadius: event.borderRadius,
          },
        };
      }
    } else {
      return {};
    }
  }
  monthView = () => {
    this.setState({ view: "month", alignment: "right" });
  };
  weekView = () => {
    this.setState({ view: "week", alignment: "center" });
  };
  dayView = () => {
    this.setState({ view: "day", alignment: "left" });
  };
  handleAlignment = (event, alignment) => this.setState({ alignment });

  getFilteredMeetings = () => {
    return this.props.meetingsState.data.filter((x) => {
      if (x.isDelete === false) {
        return x;
      }
    });
  };
  onDoubleClickEvent = (event, e) => {
    if (event.meetingOccurance) {
      let permission = event.meetingObj.meetingPermission.meetingDetail.canCreateRepeatMeeting.cando;
      permission
        ? this.setState({
            currentMeetingOccuranceId: event.meetingObj.meetingId,
            createReccurrenceDate: event.start,
            openRecurrenceConfirm: true,
            activeMeetingDisplayName: event.title,
          })
        : null;
    } else {
      const meetingDetailPer = event.meetingObj.meetingPermission.meetingDetail.cando;
      meetingDetailPer
        ? this.setState({ currentMeeting: event.meetingObj })
        : null;
    }
  };
  closeMeetingDetailsPopUp = () => {
    this.setState({ currentMeeting: null });
  };

  addNewMeeting = ({ start, end }) => {
    if (this.props.meetingPer.createMeeting.cando) {
      this.setState({
        meetingStartTime: helper.startTime(start),
        meetingDuration: helper.getDuration(start, end),
        events: [
          ...this.state.events,
          {
            start: start,
            end: end,
          },
        ],
        clickableMeeting: true,
        meetingDate: new Date(start),
      });
    }
  };
  Toolbar = (toolbar) => {
    let datePickerDate = toolbar.label;
    const { theme, classes } = this.props;
    const { view } = this.state;
    const goToBack = () => {
      if (view === "month") {
        toolbar.date.setMonth(toolbar.date.getMonth() - 1);
        toolbar.onNavigate("prev");
        this.setState({ currentDate: toolbar.date });
      } else if (view === "week") {
        toolbar.date.setDate(toolbar.date.getDate() - 7);
        toolbar.onNavigate("prev");
        this.setState({ currentDate: toolbar.date });
      } else if (view === "day") {
        toolbar.date.setDate(toolbar.date.getDate() - 1);
        toolbar.onNavigate("prev");
        this.setState({ currentDate: toolbar.date });
      }
      this.setState({ btnClicked: "Back" });
      datePickerDate = toolbar.label;
    };

    const goToNext = () => {
      if (view === "month") {
        toolbar.date.setMonth(toolbar.date.getMonth() + 1);
        toolbar.onNavigate("next");
        this.setState({ currentDate: toolbar.date });
      } else if (view === "week") {
        toolbar.date.setDate(toolbar.date.getDate() + 7);
        toolbar.onNavigate("next");
        this.setState({ currentDate: toolbar.date });
      } else if (view === "day") {
        toolbar.date.setDate(toolbar.date.getDate() + 1);
        toolbar.onNavigate("next");
        this.setState({ currentDate: toolbar.date });
      }
      this.setState({ btnClicked: "Next" });
      datePickerDate = toolbar.label;
    };

    const onDateSelect = (_date) => {
      if (_date) {
        const previousDate = new moment(this.state.date);
        previousDate.diff(_date, "months") !== 0
          ? toolbar.date.setMonth(_date.month())
          : null;
        previousDate.diff(_date, "years") !== 0
          ? toolbar.date.setYear(_date.year())
          : null;
        previousDate.diff(_date, "day") !== 0
          ? toolbar.date.setDate(_date.date())
          : null;
        this.setState({ date: new Date(_date.format()) });
      }
    };

    const goToCurrent = () => {
      const now = new Date();
      toolbar.date.setDate(now.getDate());
      toolbar.date.setMonth(now.getMonth());
      toolbar.date.setYear(now.getFullYear());

      DefaultButton;
      toolbar.onNavigate("current");
      this.setState({ currentDate: toolbar.date });
    };
    return (
      <div className={classes.calendarToolbar}>
        <Grid container>
          <div className={classes.toggleContainer}>
            <ToggleButtonGroup
              value={this.state.alignment}
              exclusive
              classes={{ root: classes.toggleBtnGroup, groupedHorizontal: classes.groupedHorizontal }}
              onChange={this.handleAlignment}
            >
              <ToggleButton
                value="left"
                classes={{
                  root: classes.toggleButton,
                  selected: classes.toggleButtonSelected,
                }}
                onClick={this.dayView}
              >
              <FormattedMessage id="common.action.day.label" defaultMessage="Day"/>  
              </ToggleButton>
              <ToggleButton
                value="center"
                classes={{
                  root: classes.toggleButton,
                  selected: classes.toggleButtonSelected,
                }}
                onClick={this.weekView}
              >
               <FormattedMessage id="common.action.week.label" defaultMessage="Week"/>    
              </ToggleButton>
              <ToggleButton
                value="right"
                classes={{
                  root: classes.toggleButton,
                  selected: classes.toggleButtonSelected,
                }}
                onClick={this.monthView}
              >
                <FormattedMessage id="common.action.month.label" defaultMessage="Month"/>   
              </ToggleButton>
            </ToggleButtonGroup>
          </div>

          <Grid item>
            <CustomButton
              onClick={goToCurrent}
              btnType="graydashed"
              variant="contained"
              style={{ padding: "9px 20px", marginLeft: 20 }}
            >
              <FormattedMessage id="common.action.today.label" defaultMessage="Today"/>
            </CustomButton>
          </Grid>
          <Grid item classes={{ item: classes.NextBackBtnCnt }}>
            <CustomIconButton btnType="condensed" onClick={goToBack}>
              <LeftArrow
                htmlColor={theme.palette.secondary.dark}
                classes={{ root: classes.navigationArrow }}
              />
            </CustomIconButton>
            <span className={classes.toolbarCurrentDate}>
              {this.state.view === "month" ? (
                <Typography variant="body2">{toolbar.label}</Typography>
              ) : (
                <StaticDatePicker
                  date={toolbar.date}
                  onDateSelect={onDateSelect}
                  children={datePickerDate}
                  btnState={this.state.btnClicked}
                  clearBtnState={this.clearBtnState}
                />
              )}
            </span>
            <CustomIconButton btnType="condensed" onClick={goToNext}>
              <RightArrow
                htmlColor={theme.palette.secondary.dark}
                classes={{ root: classes.navigationArrow }}
              />
            </CustomIconButton>
          </Grid>
        </Grid>
      </div>
    );
  };
  handleClose = () => {
    this.setState({ clickableMeeting: false });
  };
  MonthEvent = ({ event }) => {
    const { theme, classes } = this.props;

    return (
      <div className={classes.calenderMeetingRender}>
        {event.title}
        {event.meetingOccurance ? (
          <SvgIcon
            viewBox="0 0 14 12.438"
            htmlColor={"#0090ff"}
            className={classes.recurrenceIcon}
          >
            <RecurrenceIcon />
          </SvgIcon>
        ) : null}
        {event.meetingZoom ? (
          <SvgIcon
            viewBox="0 0 20.87 14.602"
            htmlColor={theme.palette.secondary.medDark}
            className={classes.meetNowIcon}
          >
            <MeetNowIcon />
          </SvgIcon>
        ) : null}
      </div>
    );
  };
  renderNewMeeting = () => {
    const {
      events,
      view,
      currentDate,
      clickableMeeting,
      meetingDate,
    } = this.state;
    const calenderCurrentDate = new Date(moment(currentDate));
    let components = {
      toolbar: this.Toolbar,
      event: this.MonthEvent,
    };
    return (
      <div style={{ height: 700, width: "100%" }}>
        <DefaultDialog
          title="Add Meeting"
          open={clickableMeeting}
          onClose={this.handleClose}
        >
          <AddMeetingForm
            handleCloseMeeting={this.handleClose}
            isNew={true}
            isDialog={true}
            preFilledData={{
              meetingDate,
              startTime: this.state.meetingStartTime,
              calenderView: { view },
              meetingDuration: this.state.meetingDuration,
            }}
            maxLength={250}
          />
        </DefaultDialog>
        <DragAndDropCalendar
          popup
          selectable
          localizer={localizer}
          components={components}
          events={events}
          onEventDrop={this.onEventDrop}
          eventPropGetter={this.eventStyle}
          resizable
          onEventResize={() => {}}
          onSelectSlot={this.addNewMeeting}
          //defaultView={this.state.view === 1 ? Views.MONTH : this.state.view === 2 ? Views.WEEK : this.state.view === 3 ? Views.DAY : null }
          view={view}
          defaultDate={calenderCurrentDate}
          timeslots={1}
          onDoubleClickEvent={this.onDoubleClickEvent}
        />
      </div>
    );
  };

  getMeetingOccurances = (meetings) => {
    const dates = [];
    const meeting = meetings.filter((m) => {
      return m.repeatDates ? m : null;
    });
    // const newArr = dates.concat(...meeting.map(item => item.repeatDates))
    meeting.map((m) => {
      m.repeatDates.map((r) => {
        const obj = {
          startDate: r,
          title: m.meetingDisplayName,
          id: m.meetingId,
          owner: m.isOwner,
          meetingObj: m,
        };
        dates.push(obj);
      });
    });
    const repeatOccurance = this.getOccuranceobj(dates);
    return repeatOccurance;
  };

  getOccuranceobj = (dates) => {
    const { classes } = this.props;
    const className = classes.repeatOccuranceStyle;
    const occuranceObj = dates.map((d) => {
      return {
        id: d.startDate,
        meetingOccurance: true,
        title: d.title,
        allDay: false,
        start: new Date(moment(d.startDate).format("MM/DD/YYYY hh:mm A")),
        end: new Date(moment(d.startDate).format("MM/DD/YYYY hh:mm A")),
        parentId: null,
        desc: "",
        meetingsColor: "#f1f1f1",
        borderColor: "1px solid #cecece",
        color: "#7e7e7e",
        // meetingObj: { startDate: d.startDate, occuranceId: d.id, owner: d.owner }
        meetingObj: d.meetingObj,
        borderRadius: "0px",
        className: className,
      };
    });
    return occuranceObj;
  };
  handleDialogClose = (name) => {
    this.setState({
      openRecurrenceConfirm: false,
      createReccurrenceDate: "",
      activeMeetingDisplayName: "",
      currentMeetingOccuranceId: "",
    });
  };

  handleCreateReccureMeeting = () => {
    const { createReccurrenceDate, currentMeetingOccuranceId } = this.state;
    this.setState({ btnQuery: "progress" });

    const data = {
      MeetingId: currentMeetingOccuranceId,
      date: moment(createReccurrenceDate).format("MM/DD/YYYY hh:mm:ss A"),
    };

    // Create Next Meeting occurence
    this.props.createNextOccurrence(
      data,
      //Success
      () => {
        this.setState({ btnQuery: "" });
        this.handleDialogClose();
      },
      //Failure
      (error) => {
        this.setState({ btnQuery: "" });
        this.showSnackBar(error.data.message, "error");
      }
    );
  };
  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  render() {
    const {
      currentMeeting,
      events,
      view,
      currentDate,
      clickableMeeting,
      openRecurrenceConfirm,
      activeMeetingDisplayName,
      btnQuery,
    } = this.state;
    const calenderCurrentDate = new Date(moment(currentDate));

    // const MonthEvent = ({ event }) => {
    //   return (
    //     <>
    //       {event.title}
    //       {event.parentId ? (
    //         <SvgIcon
    //           viewBox="0 0 14 12.438"
    //           htmlColor={theme.palette.common.white}
    //           className={classes.recurrenceIcon}
    //         >
    //           <RecurrenceIcon />
    //         </SvgIcon>
    //       ) : null}
    //     </>
    //   );
    // };
    let components = {
      toolbar: this.Toolbar,
      event: this.MonthEvent,
    };

    const MeetingOccurances = this.getMeetingOccurances(
      this.props.meetingsState.data
    );

    return (
      <div style={{ height: 700, width: "100%" }}>
        {openRecurrenceConfirm && (
          <ActionConfirmation
            open={openRecurrenceConfirm}
            closeAction={this.handleDialogClose}
            cancelBtnText="Cancel"
            successBtnText="Yes, Create Next Occurrence"
            alignment="center"
            headingText="Create Next Occurrence"
            iconType="recurrence"
            msgText={
              <>
                This meeting is part of "{activeMeetingDisplayName}" schedule
                and does not exist yet.
                <br />
                <br />
                Would you like to create next occurrence?
              </>
            }
            successAction={this.handleCreateReccureMeeting}
            btnQuery={btnQuery}
          />
        )}
        {currentMeeting ? (
          <MeetingDetails
            closeMeetingDetailsPopUp={this.closeMeetingDetailsPopUp}
            currentMeeting={currentMeeting}
          />
        ) : clickableMeeting ? (
          this.renderNewMeeting()
        ) : (
          <DragAndDropCalendar
            popup
            selectable
            localizer={localizer}
            components={components}
            events={events.concat(MeetingOccurances)}
            onEventDrop={this.onEventDrop}
            eventPropGetter={this.eventStyle}
            resizable={false}
            onSelectSlot={this.addNewMeeting}
            //defaultView={this.state.view === 1 ? Views.MONTH : this.state.view === 2 ? Views.WEEK : this.state.view === 3 ? Views.DAY : null }
            view={view}
            defaultDate={calenderCurrentDate}
            timeslots={1}
            onDoubleClickEvent={this.onDoubleClickEvent}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    meetingsState: state.meetings,
    meetingPer: state.workspacePermissions.data.meeting,
    projects: state.projects.data || [],
    tasks: state.tasks.data || [],

  };
};

export default compose(
  withRouter,
  withSnackbar,
  withStyles(calendarStyles, { withTheme: true }),
  connect(mapStateToProps, {
    UpdateMeetingCalender,
    createNextOccurrence,
  })
)(TaskCalendar);
