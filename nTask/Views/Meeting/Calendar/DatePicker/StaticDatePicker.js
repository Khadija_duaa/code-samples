import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import DatePicker from "react-datepicker";
import moment from "moment";
import "../../../../assets/css/react-datepicker.css";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import Popper from "@material-ui/core/Popper";
import datePickerStyles from "../../../../assets/jss/components/datePicker";
import CustomButton from "../../../../components/Buttons/CustomButton";

class StaticDatePicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedDate: moment(),
      open: false,
      isEmpty: false,
      isChanged: true
    };
    this.dateSelect = this.dateSelect.bind(this);
  }
 static getDerivedStateFromProps(nextProps, prevState) {
  // if(prevState.isChanged)
    return ({ selectedDate: moment(nextProps.date) });
  }

  dateSelect(date) {
   // const prevDate = this.state.selectedDate
    this.setState({ selectedDate: date, isChanged: false });
    this.props.onDateSelect(date);
  }
  handleToggle = () => {
    this.setState(state => ({ open: !state.open, isChanged: false }));
  };
  handleClose = event => {
    this.setState({ open: false }, () => {
      if (this.props.selectedSaveDate) {
        if (this.props.isCreation)
          this.props.selectedSaveDate(this.state.selectedDate);
        else
          this.props.selectedSaveDate(this.state.selectedDate, this.props.data);
      }
    });
  };
  render() {
    const { classes, theme, children } = this.props;
    const { open, selectedDate, isEmpty } = this.state;
   
    return (
      <div className="staticDatePicker">
          <CustomButton 
          onClick={this.handleToggle} 
          btnType="plain"
          disableRipple
          buttonRef={node => {
            this.anchorEl = node;
          }}
          variant="text">
          {selectedDate.format("MMMM DD, YYYY")}
      </CustomButton>
        <Popper
          open={open}
          transition
          disablePortal
          anchorEl={this.anchorEl}
          className={classes.RangeDatePickerPopper}
        >
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              id="menu-list-grow"
              style={{
                transformOrigin:
                  placement === "bottom" ? "center top" : "center bottom"
              }}
            >
              <div>
                <ClickAwayListener onClickAway={this.handleClose}>
                  <DatePicker
                    inline
                    selected={selectedDate}
                    onSelect={this.dateSelect}
                  />
                </ClickAwayListener>
              </div>
            </Grow>
          )}
        </Popper>
      </div>
    );
  }
}

export default withStyles(datePickerStyles, {
  withTheme: true
})(StaticDatePicker);
