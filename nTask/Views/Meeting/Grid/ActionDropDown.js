import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles, withTheme } from "@material-ui/core/styles";
import itemStyles from "./styles";
import menuStyles from "../../../assets/jss/components/menu";
import CustomIconButton from "../../../components/Buttons/IconButton";
import SelectionMenu from "../../../components/Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import combineStyles from "../../../utils/mergeStyles";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import Checkbox from "@material-ui/core/Checkbox";
import DoneIcon from "@material-ui/icons/Done";
import { Scrollbars } from "react-custom-scrollbars";

let checkedItems = ["Title", "Status", "Task", "Date", "Time", "Location", "Attendee"];
class TableActionDropDown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      pickerOpen: false,
      pickerPlacement: "",
      checked: ["Task"],
      isLoaded: false,
      loggedInTeam: "",
      isFirstLoad: false
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
  }
  handleClose(event) {
    event.stopPropagation();
    this.setState({ open: false, pickerOpen: false });
  }
  handleClick(event, placement) {

    const { currentTarget } = event;
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement
    }));
  }
  componentDidMount() {
    this.setState({
      checked: checkedItems,
      loggedInTeam: this.props.profileState.data.loggedInTeam,
      isFirstLoad: true
    })
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if ((nextProps.itemOrderState.data.meetingColumnOrder && nextProps.itemOrderState.data.meetingColumnOrder.length)) {
      checkedItems = nextProps.itemOrderState.data.meetingColumnOrder;
    }
    if ((prevState.loggedInTeam !== nextProps.profileState.data.loggedInTeam)) {
      if (!prevState.checked.every(item => checkedItems.includes(item))) {
        nextProps.selectedAction(checkedItems, true)
        return ({
          isLoaded: true,
          checked: checkedItems,
          loggedInTeam: nextProps.profileState.data.loggedInTeam,
          isFirstLoad: false
        })
      }

    }

    if (!prevState.isLoaded && prevState.isFirstLoad) {
      nextProps.selectedAction(checkedItems, prevState.isFirstLoad)
      return ({
        isLoaded: true,
        checked: checkedItems,
        isFirstLoad: false
      })
    }
    return { checked: prevState.checked }
  }

  handleToggle = value => () => {
    const { checked } = this.state;
    const { selectedAction } = this.props;
    this.setState({
      isLoaded: true
    });
    if (checked.length <= 7) {

      const currentIndex = checked.indexOf(value);
      const newChecked = [...checked];

      if (currentIndex === -1 && checked.length < 7) {
        newChecked.push(value);
      } else if (currentIndex !== -1) {
        if (currentIndex === 0) {
          return;
        }
        newChecked.splice(currentIndex, 1);
      }
      if (this.state.checked.length !== newChecked.length) {
        this.setState({
          checked: newChecked
        }, () => {
          selectedAction(this.state.checked)
        });
      }

    } else {
      return false;
    }

  };
  render() {
    const { classes, theme, selectedColor, colorChange } = this.props;
    const { open, placement, checked } = this.state;
    const ddData = ["Title", "Status", "Task", "Date", "Time", "Location", "Attendee", "Duration"];
    return (
      <ClickAwayListener onClickAway={this.handleClose}>
        <div>
          <CustomIconButton
            btnType="condensed"
            onClick={event => {
              this.handleClick(event, "bottom-end");
            }}
            buttonRef={node => {
              this.anchorEl = node;
            }}
          >
            <MoreVerticalIcon htmlColor={theme.palette.secondary.medDark} style={{ fontSize: "24px" }} />
          </CustomIconButton>

          <SelectionMenu
            open={open}
            closeAction={this.handleClose}
            placement={placement}
            checkedType="multi"
            anchorRef={this.anchorEl}
            list={
              <Scrollbars autoHide style={{ height: 250 }}>
                <List>
                  <ListItem
                    disableRipple={true}
                    classes={{ root: classes.menuHeadingItem }}
                  >
                    <ListItemText
                      primary={`${checked.length - 1}/6 selected`}
                      classes={{ primary: classes.menuHeadingListItemText }}
                    />
                  </ListItem>
                  {ddData.slice(1).map(
                    value =>
                      <ListItem
                        key={value}
                        button
                        disableRipple={true}
                        classes={{ selected: classes.statusMenuItemSelected }}
                        onClick={this.handleToggle(value)}
                      >
                        <ListItemText
                          primary={value}
                          classes={{
                            primary: classes.statusItemText
                          }}
                        />
                        <ListItemSecondaryAction>
                          <Checkbox
                            checkedIcon={
                              <DoneIcon
                                htmlColor={theme.palette.primary.light}
                              />
                            }
                            onChange={this.handleToggle(value)}
                            checked={
                              this.state.checked.indexOf(value) !==
                              -1
                            }
                            icon={false}
                          />
                        </ListItemSecondaryAction>
                      </ListItem>

                  )}
                </List>
              </Scrollbars>
            }
          />
        </div>
      </ClickAwayListener>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    profileState: state.profile,
    itemOrderState: state.itemOrder

  }
}

export default compose(
  withRouter,
  withStyles(combineStyles(itemStyles, menuStyles), {
    withTheme: true
  }),
  connect(
    mapStateToProps,
    {}
  )
)(TableActionDropDown);

