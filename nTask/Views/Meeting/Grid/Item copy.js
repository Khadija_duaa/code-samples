import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import helper from "../../../helper";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import itemStyles from "./styles";
import Typography from "@material-ui/core/Typography";
import DefaultButton from "../../../components/Buttons/DefaultButton";
import LocationIcon from "@material-ui/icons/LocationOn";
import MenuList from "../../../components/Menu/TaskMenus/MemberListMenu";
import AssigneeDropdown from "../../../components/Dropdown/AssigneeDropdown";
import {FormattedMessage,injectIntl} from 'react-intl';

import {
  UpdateTask,
  FetchTasksInfo,
  CopyTask,
  DeleteTask,
  ArchiveTask,
  UnArchiveTask,
} from "../../../redux/actions/tasks";
import { SaveItemOrder } from "../../../redux/actions/itemOrder";
import { UpdateCalenderTask } from "../../../redux/actions/calenderTasks";
import {
  UpdateMeeting,
  CreateInLineMeeting,
  BulkUpdateMeeting,
  DeleteMeeting,
  ArchiveMeeting,
  UnArchiveMeeting,
  DeleteMeetingSchedule,
  ArchiveMeetingSchedule,
  UnArchiveMeetingSchedule,
  CancelMeeting,
  CancelMeetingSchedule,
  MarkMeetingAsStarted,
} from "../../../redux/actions/meetings";
import AddMeetingForm from "../../AddNewForms/AddMeetingForm";
import MeetingDetails from "../MeetingDetails/MeetingDetails";
import DefaultDialog from "../../../components/Dialog/Dialog";
import MeetingActionDropdown from "../List/MeetingActionDropDown";
import { getMeetingsPermissions, getReadOnlyPermissions } from "../permissions";
import SvgIcon from "@material-ui/core/SvgIcon";
import RecurrenceIcon from "../../../components/Icons/RecurrenceIcon";
import StarredCheckBox from "../../../components/Form/Starred";
import IconButton from "../../../components/Buttons/IconButton";
import SideDrawer from "../Drawer/Drawer";
import SearchDropdown from "../../../components/Dropdown/SearchDropdown";
import ProjectPermissionSelector from "../../../redux/selectors/projectPermissionSelector";
import isUndefined from "lodash/isUndefined";

class TaskGridItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      starChecked: false,
      taskProgress: 30,
      selectedColor: "#fff",
      completeDetails: [],
      userId: "",
      meetingId: "",
      progress: 0,
      isColored: false,
      isProgress: false,
      showMeetings: false,
      currentMeeting: {},
      meetingDateTimeDialog: false,
      isArchived: false,
      instantData: [],
      isInstanceChange: false,
      repeatMeetingId: null,
    };
    this.handleColorChange = this.handleColorChange.bind(this);
    this.handleCalculateProgress = this.handleCalculateProgress.bind(this);
    this.showMeetingsPopUp = this.showMeetingsPopUp.bind(this);
    this.closeMeetingsPopUp = this.closeMeetingsPopUp.bind(this);
    this.isUpdated = this.isUpdated.bind(this);
    this.showDateTimeDetailsPopUp = this.showDateTimeDetailsPopUp.bind(this);
    this.closeDateTimeDetailsPopUp = this.closeDateTimeDetailsPopUp.bind(this);
  }

  componentDidMount() {
    this.props.resetCount();
    let userId = this.props.profileState.data.userId;
    let details = this.getDetailedMeetings();
    this.setState({ completeDetails: this.props.filteredMeetings, userId });
  }

  componentDidUpdate(prevProps) {
    const { currentMeeting } = this.state;
    const { filteredMeetings } = this.props;

    const meetingDetailsMeeting = filteredMeetings.find((m) => {
      // Checking if the meeting details meeting exist in the list or not
      return currentMeeting && m.meetingId == currentMeeting.meetingId;
    });
    if (currentMeeting && !meetingDetailsMeeting) {
      // if meeting does not exist in the filtered list remove the meeting details from state
      //This scenario can be produced if you apply filter on a meeting with assignee from sidebar and open meeting details and change assignee and than toggle filter from sidebar again
      this.setState({ showMeetings: false, currentMeeting: null });
    }

    if (
      JSON.stringify(prevProps.filteredMeetings) !==
      JSON.stringify(this.props.filteredMeetings)
    ) {
      //Remove this check, due to this check archive/unarchive view not renderd
      let details = this.getDetailedMeetings();
      this.setState({ completeDetails: details.completeDetails });
    }
    if (
      JSON.stringify(prevProps.sortObj) !== JSON.stringify(this.props.sortObj)
    ) {
      this.setState({ completeDetails: this.props.filteredMeetings });
    }
  }

  static getDerivedStateFromProps(props, state) {
    let { instantData } = state;
    let meetings =
      instantData.length > 0 ? instantData : props.filteredMeetings || [];
    let members =
      props.profileState &&
      props.profileState.data &&
      props.profileState.data.member
        ? props.profileState.data.member.allMembers
        : [];
    let completeDetails = meetings.map((x) => {
      x.assigneeLists = members.filter((m) => {
        if (x.attendeeList && x.attendeeList.length >= 0)
          return x.attendeeList.indexOf(m.userId) >= 0;
      });
      return x;
    });
    // completeDetails = completeDetails
    //   .map(x => {
    //     let position = helper.RETURN_ITEMORDER(
    //       props.itemOrderState.data.itemOrder,
    //       x.id
    //     );
    //     x.taskOrder = position || 0;
    //     return x;
    //   })
    //   .sort((a, b) => a.taskOrder - b.taskOrder);

    props.returnCount(completeDetails.length, props.showRecords);
    if (props.isChanged) {
      props.handleChangeState();
    }
    return {
      completeDetails: completeDetails,
    };
  }

  getDetailedMeetings = () => {
    const props = this.props;
    let meetings = props.filteredMeetings || [];
    let members =
      props.profileState &&
      props.profileState.data &&
      props.profileState.data.member
        ? props.profileState.data.member.allMembers
        : [];
    let completeDetails = meetings.map((x) => {
      x.assigneeLists = members.filter((m) => {
        if (x.attendeeList && x.attendeeList.length >= 0)
          return x.attendeeList.indexOf(m.userId) >= 0;
      });
      return x;
    });
    return {
      completeDetails,
      members,
    };
  };

  showDateTimeDetailsPopUp() {
    this.setState({ meetingDateTimeDialog: true });
  }
  closeDateTimeDetailsPopUp() {
    this.setState({ meetingDateTimeDialog: false });
  }

  isUpdated(data) {
    let completeDetails = this.props.filteredMeetings.map((x) => {
      if (data.meetingId === x.meetingId) {
        x = data;
      }
      return x;
    });
    this.setState(
      {
        instantData: completeDetails,
        isInstanceChange: true,
        currentMeeting: data,
      },
      () => {
        this.props.UpdateMeeting(data, (err, data) => {
          if (this.props.meetingState.indexOf("Archived") >= 0)
            this.setState({ isArchived: true, isInstanceChange: false });
        });
      }
    );
  }
  handleCalculateProgress(data, isProgressUpdated) {
    if (isProgressUpdated) {
      this.setState({ progress: 0, meetingId: "", isProgress: false });
    } else {
      let result = helper.RETURN_PROGRESS(data.checkList, data.status);
      this.setState({
        progress: result,
        meetingId: data.meetingId,
        isProgress: true,
      });
    }
  }
  handleColorChange = (data, color) => {
    let self = this;
    this.setState(
      { selectedColor: color.hex, meetingId: data.meetingId, isColored: true },
      function() {
        let obj = Object.assign({}, data);
        obj.colorCode = color.hex;
        delete obj.CalenderDetails;
        self.props.UpdateTaskColorFromGridItem(obj, (data) => {
          self.props.UpdateCalenderTask(data, () => {
            self.setState({
              selectedColor: "#fff",
              meetingId: "",
              isColored: false,
            });
          });
        });
      }
    );
  };
  //Function that updates assignee of respective document (e.g. task/risk/issue/meeting)
  updateMeetingAttendee = (assignedTo, meeting) => {
    let assignedToArr = assignedTo.map((ass) => {
      return ass.userId;
    });
    let newMeetingObj = { ...meeting, attendeeList: assignedToArr };
    this.props.UpdateMeeting(newMeetingObj, () => {});
  };
  showMeetingsPopUp(data, event) {
    const readOnly = getReadOnlyPermissions(data);
    this.setState({
      showMeetings: !data.startTime || !data.startDateString ? false : true,
      meetingDateTimeDialog:
        data &&
        !data.isDelete &&
        (!data.startTime || !data.startDateString) &&
        !readOnly
          ? true
          : false,
      currentMeeting: data,
      meetingId: data.meetingId,
    });
    let newMeetingObj = { ...meeting, attendeeList: assignedToArr };
    this.props.UpdateMeeting(newMeetingObj, () => {});
  }
  showMeetingsPopUp(data, event) {
    const { permissionObject, meetingPer} = this.props;
    const meetingPermission =
        data && data.projectId
            ? isUndefined(permissionObject[data.projectId])
              ? meetingPer
              : permissionObject[data.projectId].meeting
            : meetingPer;
    if (meetingPermission.meetingDetail.cando) {
      const readOnly = getReadOnlyPermissions(data);
      this.setState({
        showMeetings: !data.startTime || !data.startDateString ? false : true,
        meetingDateTimeDialog:
          data &&
          !data.isDelete &&
          (!data.startTime || !data.startDateString) &&
          !readOnly
            ? true
            : false,
        currentMeeting: data,
        meetingId: data.meetingId,
      });
    }
  }
  closeMeetingsPopUp() {
    this.setState({ showMeetings: false, meetingId: "" });
  }
  handleStaredChange = (event, meeting) => {
    let isStared = event.target.checked;
    if (meeting) {
      let obj = {
        Id: meeting.id,
        MarkStar: isStared,
      };
      this.props.MarkMeetingAsStarted(obj, (err, data) => {});
    }
  };
  openRepeatMeetingDrawer = (event, meeting) => {
    event.stopPropagation();
    this.setState({
      repeatMeetingDrawer: true,
      repeatMeetingId: meeting.meetingId,
    });
  };
  closeRepeatMeetingDrawer = () => {
    this.setState({ repeatMeetingDrawer: false, repeatMeetingDrawer: "" });
  };

  generateTaskDropdownData = (meeting) => {
    const { tasks } = this.props;
    let tasksArr = tasks.map((task) => {
      return { label: task.taskTitle, id: task.taskId, obj: meeting };
    });
    return tasksArr;
  };

  getSelectedTask = (meeting) => {
    const { tasks } = this.props;
    let selectedTask = tasks.find((task) => {
      return task.taskId == meeting.taskId;
    });

    return selectedTask
      ? [
          {
            label: selectedTask.taskTitle,
            id: selectedTask.taskId,
            obj: meeting,
          },
        ]
      : [];
  };

  updateMeetingTask = (selectedTask, meeting) => {
    let newMeetingObj = {
      ...meeting,
      taskId: selectedTask.length ? selectedTask[0].id : "",
    };
    this.props.UpdateMeeting(
      newMeetingObj,
      () => {},
      () => {}
    );
  };

  render() {
    const { classes, theme, isArchivedSelected, meetingPer, intl, permissionObject } = this.props;
    const {
      selectedColor,
      meetingId,
      isProgress,
      isColored,
      currentMeeting,
      meetingDateTimeDialog,
      isInstanceChange,
      repeatMeetingDrawer,
      repeatMeetingId,
    } = this.state;
    const profileUserId = this.props.profileState.data.userId;
    let selectedColorValue = "#fff",
      progress = 0;

    const gridData = this.state.completeDetails
      .slice(0, this.props.showRecords)
      .map((x, i) => {
        if (meetingId === x.meetingId) {
          if (isColored) selectedColorValue = selectedColor;
          else selectedColorValue = x.colorCode;
          if (isProgress) progress = this.state.progress;
          else progress = x.progress;
        } else {
          selectedColorValue = x.colorCode;
          progress = x.progress;
        }
        const permission = getMeetingsPermissions(x, this.props);
        const readOnly = getReadOnlyPermissions(x);
        const taskTitle =
          x && this.props.tasksState.data
            ? this.props.tasksState.data.find((x) => x.taskId === x.taskId)
              ? this.props.tasksState.data.find((x) => x.taskId === x.taskId)
                  .taskTitle
              : "              -"
            : "              -";
        const meetingPermission =
        x && x.projectId
            ? isUndefined(permissionObject[x.projectId])
              ? meetingPer
              : permissionObject[x.projectId].meeting
            : meetingPer;
        return (
          <Grid item xs={12} sm={6} md={6} lg={4} xl={3} key={x.meetingId}>
            {repeatMeetingId && repeatMeetingDrawer && (
              <SideDrawer
                closeAction={this.closeRepeatMeetingDrawer}
                open={repeatMeetingDrawer}
                repeatMeetingId={repeatMeetingId}
                meetingPer={meetingPermission}
              />
            )}
            <div
              style={{ borderLeft: `5px solid ${selectedColorValue}` }}
              className={classes.taskItemCnt}
              onClick={this.showMeetingsPopUp.bind(this, x)}
            >
              <Typography
                classes={{ h6: classes.taskGridTaskList }}
                variant="h6"
              >
                {x.uniqueId}
              </Typography>

              <div className={classes.gridItemHeadingCnt}>
                <Typography
                  classes={{ h5: classes.taskGridTaskList }}
                  variant="h5"
                >
                  {x.meetingDisplayName || "Un Named"}

                  {!x.isDelete && x.meetingSchedule && (
                    <IconButton
                      btnType="transparent"
                      onClick={(event) =>
                        this.openRepeatMeetingDrawer(event, x)
                      }
                      // style={{ padding: 15 }}
                    >
                      <SvgIcon
                        viewBox="0 0 14 12.438"
                        htmlColor={this.props.theme.palette.primary.light}
                        className={this.props.classes.recurrenceIcon}
                      >
                        <RecurrenceIcon />
                      </SvgIcon>
                    </IconButton>
                  )}
                  {/* {x.parentId ? (
                    <SvgIcon viewBox="0 0 14 12.438" htmlColor={theme.palette.primary.light} className={classes.recurrenceIcon}>
                      <RecurrenceIcon />
                    </SvgIcon>
                  )
                    : null} */}
                </Typography>

                <div className="flex_center_start_row">
                  <StarredCheckBox
                    checked={x.isStared}
                    onChange={(e) => this.handleStaredChange(e, x)}
                  />
                  <MeetingActionDropdown
                    meeting={x}
                    CopyTask={this.props.CopyTask}
                    filterMeeting={this.props.filterMeeting}
                    DeleteMeeting={this.props.DeleteMeeting}
                    ArchiveMeeting={this.props.ArchiveMeeting}
                    UnArchiveMeeting={this.props.UnArchiveMeeting}
                    DeleteMeetingSchedule={this.props.DeleteMeetingSchedule}
                    ArchiveMeetingSchedule={this.props.ArchiveMeetingSchedule}
                    UnArchiveMeetingSchedule={
                      this.props.UnArchiveMeetingSchedule
                    }
                    CancelMeeting={this.props.CancelMeeting}
                    CancelMeetingSchedule={this.props.CancelMeetingSchedule}
                    CopyCalenderTask={this.props.CopyCalenderTask}
                    UpdateCalenderTask={this.props.UpdateCalenderTask}
                    selectedColor={x && x.colorCode ? x.colorCode : { hex: "" }}
                    isUpdated={this.isUpdated}
                    closeActionMenu={this.closeActionMenu}                    
                    isArchivedSelected={isArchivedSelected}
                    meetingPer={meetingPermission}
                    view='grid'
                  />
                </div>
              </div>

              <div>
                {!readOnly &&
                meetingPermission.meetingDetail.meetingTask.isAllowEdit ? (
                  <SearchDropdown
                    initSelectedOption={this.getSelectedTask(x)}
                    buttonProps={{
                      disabled: isArchivedSelected,
                    }}
                    obj={x}
                    labelAlign="left"
                    optionsList={this.generateTaskDropdownData(x)}
                    singleSelect={true}
                    updateAction={this.updateMeetingTask}
                    selectedOptionHead={
                      <FormattedMessage
                        id="common.meeting.label2"
                        defaultMessage="Meeting Task"
                      />
                    }
                    allOptionsHead={
                      <FormattedMessage
                        id="profile-settings-dialog.apps-integrations.common.task.label"
                        defaultMessage="Tasks"
                      />
                    }
                    buttonPlaceholder={intl.formatMessage({
                      id: "meeting.creation-dialog.form.task.placeholder",
                      defaultMessage: "Select Task",
                    })}
                    isMulti={false}
                    multipleOptionsPlaceHolder="Multiple Tasks"
                  />
                ) : (
                  <Typography variant="body2">{taskTitle}</Typography>
                )}
              </div>

              <Grid
                container
                direction="row"
                justify="space-between"
                alignItems="center"
                classes={{ container: classes.gridItemDropdownCnt }}
              >
                {/* <AssigneeList meetingId={x.meetingId} assigneeList={x.assigneeLists} allMembers={this.props.profile.data.member.allMembers} /> */}
                <div>
                  <AssigneeDropdown
                    assignedTo={x ? x.assigneeLists : []}
                    updateAction={this.updateMeetingAttendee}
                    isArchivedSelected={isArchivedSelected}
                    obj={x}
                    assigneeHeading={
                      <FormattedMessage
                        id="meeting.detail-dialog.participants.label"
                        defaultMessage="Participants"
                      />
                    }
                    minOneSelection={true}
                    disabled={
                      x.status == "Published" ||
                      !meetingPermission.meetingDetail.meetingAttendee.isAllowAdd
                        ? true
                        : null
                    }
                    deletePermission={
                      meetingPermission.meetingDetail.meetingAttendee
                        .isAllowDelete
                    }
                  />
                </div>
                {x.startDateString ? (
                  <div
                    className={`${classes.meetingDateCnt} flex_center_center_col`}
                  >
                    <span className={classes.meetingDate}>
                      {helper.RETURN_DAY_DATE(x.startDateString)}
                    </span>
                    <Typography variant="body2" align="center">
                      {x.startDateString
                        ? helper.RETURN_MONTH_NAME(x.startDateString)
                        : this.props.intl.formatMessage({id:"common.date.placeholder",defaultMessage:"Select Date"})}
                    </Typography>
                  </div>
                ) : null}
              </Grid>
              <Grid item>
                <Grid
                  container
                  direction="row"
                  justify="space-between"
                  alignItems="center"
                  classes={{ container: classes.itemBottomBtns }}
                >
                  <DefaultButton
                    buttonType="smallIconBtn"
                    onClick={this.showMeetingsPopUp.bind(this, x)}
                  >
                    <LocationIcon classes={{ root: classes.smallBtnIcon }} />
                    <Typography variant="body2" align="center">
                      {x.location || " - "}
                    </Typography>
                  </DefaultButton>
                </Grid>
              </Grid>
            </div>
          </Grid>
        );
      });
    return (
      <Fragment>
        <DefaultDialog
          title={
            <FormattedMessage
              id="meeting.creation-dialog.title"
              defaultMessage="Add Meeting Details"
            ></FormattedMessage>
          }
          open={meetingDateTimeDialog}
          onClose={this.closeDateTimeDetailsPopUp}
        >
          <AddMeetingForm
            handleCloseMeeting={this.closeDateTimeDetailsPopUp}
            isNew={true}
            isDialog={true}
            isAddMeetingDetails={true}
            currentMeeting={currentMeeting}
            isUpdated={this.isUpdated}
            maxLength={250}
          />
        </DefaultDialog>
        {this.state.showMeetings ? (
          <MeetingDetails
            isUpdated={this.isUpdated}
            closeMeetingDetailsPopUp={this.closeMeetingsPopUp}
            currentMeeting={
              this.props.meetings.filter(function(x) {
                return x.meetingId == meetingId;
              })[0]
            }
            isArchivedSelected={isArchivedSelected}
            isInstanceChange={isInstanceChange}
            filterMeeting={this.props.filterMeeting}
            UnArchiveMeeting={this.props.UnArchiveMeeting}
          />
        ) : null}
        <Grid container spacing={2} pointerEvents={"none"}>
          {gridData}
        </Grid>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profileState: state.profile,
    tasksState: state.tasks,
    workspacePermissionsState: state.workspacePermissions,
    itemOrderState: state.itemOrder,
    tasks: state.tasks.data,
    meetingPer: state.workspacePermissions.data.meeting,
    permissionObject : ProjectPermissionSelector(state)
  };
};

export default compose(
  withRouter,
  injectIntl,
  withStyles(itemStyles, { withTheme: true }),
  connect(mapStateToProps, {
    UpdateTask,
    UpdateCalenderTask,
    SaveItemOrder,
    FetchTasksInfo,
    CopyTask,
    DeleteTask,
    ArchiveTask,
    UnArchiveTask,

    ////////////////////////

    UpdateMeeting,
    CreateInLineMeeting,
    BulkUpdateMeeting,
    DeleteMeeting,
    ArchiveMeeting,
    UnArchiveMeeting,
    DeleteMeetingSchedule,
    ArchiveMeetingSchedule,
    UnArchiveMeetingSchedule,
    CancelMeeting,
    CancelMeetingSchedule,
    MarkMeetingAsStarted,
  })
)(TaskGridItem);
