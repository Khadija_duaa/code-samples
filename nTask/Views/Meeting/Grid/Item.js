import React, { Component, Fragment, useEffect, useState } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import helper from "../../../helper";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import { grid } from "../../../components/CustomTable2/gridInstance";
import itemStyles from "./styles";
import Typography from "@material-ui/core/Typography";
import DefaultButton from "../../../components/Buttons/DefaultButton";
import LocationIcon from "@material-ui/icons/LocationOn";
import MenuList from "../../../components/Menu/TaskMenus/MemberListMenu";
import AssigneeDropdown from "../../../components/Dropdown/AssigneeDropdown";
import { FormattedMessage, injectIntl } from 'react-intl';
import { doesFilterPass } from "../List/MeetingFilter/meetingFilter.utils";

import {
  UpdateTask,
  FetchTasksInfo,
  CopyTask,
  DeleteTask,
  ArchiveTask,
  UnArchiveTask,
} from "../../../redux/actions/tasks";
import { SaveItemOrder } from "../../../redux/actions/itemOrder";
import { UpdateCalenderTask } from "../../../redux/actions/calenderTasks";
import {
  updateMeetingData,
} from "../../../redux/actions/meetings";
import AddMeetingForm from "../../AddNewForms/AddMeetingForm";
import MeetingDetails from "../MeetingDetails/MeetingDetails";
import DefaultDialog from "../../../components/Dialog/Dialog";
import MeetingActionDropdown from "../MeetingActionDropdown/MeetingActionDropdown";
import { getMeetingsPermissions, getReadOnlyPermissions } from "../permissions";
import SvgIcon from "@material-ui/core/SvgIcon";
import RecurrenceIcon from "../../../components/Icons/RecurrenceIcon";
import StarredCheckBox from "../../../components/Form/Starred";
import Stared from "../../../components/Starred/Starred";
import IconButton from "../../../components/Buttons/IconButton";
import SideDrawer from "../Drawer/Drawer";
import SearchDropdown from "../../../components/Dropdown/SearchDropdown";
import ProjectPermissionSelector from "../../../redux/selectors/projectPermissionSelector";
import isUndefined from "lodash/isUndefined";
import { generateTaskData } from "../../../helper/generateSelectData";

import { generateTaskDropdownData, statusData } from "../../../helper/meetingDropdownData";
import useUpdateMeeting from "../../../helper/customHooks/meetings/updateMeeting";
// class MeetingGridItem extends Component {
const MeetingGridItem = (props) => {
  const {
    classes,
    theme,
    resetCount,
    isArchivedSelected,
  } = props;
  const state = useSelector(state => {
    return {
      tasksState: state.tasks,
      workspaceMeetingPer: state.workspacePermissions.data.meeting,
      members: state.profile.data.member.allMembers,
      meetings: state.meetings.data || [],
      quickFilters: state.meetings.quickFilters,
      tasks: state.tasks.data,
      permissionObject: ProjectPermissionSelector(state)
    };
  });
  const {
    tasksState,
    workspaceMeetingPer,
    members,
    meetings,
    quickFilters,
    tasks,
    permissionObject,
  } = state;
  const { editMeeting } = useUpdateMeeting();
  const dispatch = useDispatch();
  const [completeDetails, setCompleteDetails] = useState([]);
  const [meetingDetail, setMeetingDetail] = useState('');
  useEffect(() => {
    resetCount();
    // let userId = profileState.data.userId;
    let showMeetings = meetings.filter(m => doesFilterPass({ data: m }));
    if (quickFilters.Archived) {
      showMeetings = showMeetings.filter(m => m.isDeleted)
    }
    setCompleteDetails(showMeetings);
  }, [meetings, quickFilters]);
  
  useEffect(() => {
    if (meetings.length && meetingDetail != '') {
      const selectedMeetingObj = meetings.find(m => meetingDetail.data.meetingId == m.meetingId);
      let obj;
      if (meetingDetail.detailsView) {
        obj = { data: selectedMeetingObj, detailsView: true };
      }
      else {
        obj = { data: selectedMeetingObj, detailsView: false };
      }
      setMeetingDetail(obj);
    }
  }, [meetings]);

  // meeting update functions
  const updateMeeting = (meeting, obj) => {
    updateMeetingData(
      { meeting, obj },
      dispatch,
      //Success
      meeting => {
        if (grid.grid) {
          const rowNode = grid.grid && grid.grid.getRowNode(meeting.id);
          rowNode.setData(meeting);
        }
      },
      err => {
        if (err && err.data) showSnackBar(err.data.message, "error");
      }
    );
  };
  // Handle straed
  const handleMeetingStared = (stared, obj) => {
    editMeeting(obj, "isStared", stared)
  };
  // Handle task change
  const handleTaskChange = (task, obj) => {
    // editMeeting(obj, 'taskId', task.length ? task[0].id : '');
    let object = {
      taskId: task.length ? task[0].id : '',
    }
    if (obj.projectId && task.length) object.projectId = task[0].obj.projectId;
    updateMeeting(obj, object);
  };
  // Handle Attendee Update 
  const handleUpdateAssignee = (assignees, obj) => {
    const attendeeList = assignees.map(a => a.userId);
    updateMeeting(obj, { attendeeList: attendeeList });
  };
  // show Meetings PopUp 
  const showMeetingsPopUp = row => {
    const permission = workspaceMeetingPer.meetingDetail.cando;
    if (permission && row && row.id && row.uniqueId !== "-") {
      if (!row.startTime || !row.startDate) {
        let obj = { data: row, detailsView: false };
        setMeetingDetail(obj)
      } else {
        let obj = { data: row, detailsView: true };
        setMeetingDetail(obj)
      }
    }
  };
  const handleCloseMeeting = () => {
    setMeetingDetail('')
  };
  const closeDateTimeDetailsPopUp = () => {
    setMeetingDetail('')
  }
  const isUpdated = (data) => {

  }
  const getSelectedTask = (meeting) => {
    let selectedTask = tasks.filter(t => {
      return meeting.taskId && meeting.taskId.indexOf(t.taskId) > -1;
    });
    return generateTaskData(selectedTask);
  };
  return (
    <>
      {meetingDetail &&
        <DefaultDialog
          title={
            <FormattedMessage
              id="meeting.creation-dialog.title"
              defaultMessage="Add Meeting Details"></FormattedMessage>
          }
          open={!meetingDetail.detailsView ? true : false}
          onClose={closeDateTimeDetailsPopUp}>
          <AddMeetingForm
            handleCloseMeeting={closeDateTimeDetailsPopUp}
            isNew={true}
            isDialog={true}
            isAddMeetingDetails={true}
            currentMeeting={meetingDetail.data}
            maxLength={250}
          />
        </DefaultDialog>
      }
      {meetingDetail.detailsView ?
        <MeetingDetails
          isUpdated={isUpdated}
          closeMeetingDetailsPopUp={handleCloseMeeting}
          currentMeeting={meetingDetail.data}
          listView={true}
          isArchivedSelected={isArchivedSelected}
          isInstanceChange={false}
          filterMeeting={[]}
          UnArchiveMeeting={() => { }}
          updateMeetingAttendee={() => { }}
        /> : null}

      <Grid className={classes.gridContainer} container spacing={2}>
        {completeDetails.map((x, i) => {
          const readOnly = getReadOnlyPermissions(x);
          const taskTitle =
            x && tasksState.data
              ? tasksState.data.find((x) => x.taskId === x.taskId)
                ? tasksState.data.find((x) => x.taskId === x.taskId)
                  .taskTitle
                : "-"
              : "-";
          const membersObjArr = members && members.filter(m => x.attendeeList.includes(m.userId));
          const meetingPermission =
            x && x.projectId
              ? isUndefined(permissionObject[x.projectId])
                ? workspaceMeetingPer
                : permissionObject[x.projectId].meeting
              : workspaceMeetingPer;
          return (<Grid item xs={12} sm={6} md={6} lg={4} xl={3} key={x.meetingId}>
            <div
              style={{ borderLeft: `5px solid ${x.colorCode}` }}
              className={classes.taskItemCnt}
              onClick={() => showMeetingsPopUp(x)}
            >
              <Typography
                classes={{ h6: classes.taskGridTaskList }}
                variant="h6"
              >
                {x.uniqueId}
              </Typography>
              <div className={classes.gridItemHeadingCnt}>
                <Typography
                  classes={{ h5: classes.taskGridTaskList }}
                  variant="h5"
                >
                  {x.meetingDisplayName || "Un Named"}
                  {!x.isDelete && x.meetingSchedule && (
                    <IconButton
                      btnType="transparent"
                      onClick={(event) =>
                        this.openRepeatMeetingDrawer(event, x)
                      }
                    // style={{ padding: 15 }}
                    >
                      <SvgIcon
                        viewBox="0 0 14 12.438"
                        htmlColor={theme.palette.primary.light}
                        className={classes.recurrenceIcon}
                      >
                        <RecurrenceIcon />
                      </SvgIcon>
                    </IconButton>
                  )}
                </Typography>

                <div className="flex_center_start_row">
                  <Stared
                    isStared={x.isStared}
                    handleCallback={(e) => handleMeetingStared(e, x)}
                    diabled={x.isDeleted}
                  />
                  <MeetingActionDropdown
                    btnProps={{ style: { width: 37 } }}
                    data={x}
                    handleActivityLog={() => { }}
                    handleCloseCallBack={() => { }}
                    meetingPermission={meetingPermission}
                    isArchived={x.isDeleted}
                  />
                </div>
              </div>
              <div>
                {!readOnly && meetingPermission.meetingDetail.meetingTask.isAllowEdit ? (
                  <SearchDropdown
                    initSelectedOption={getSelectedTask(x)}
                    obj={x}
                    buttonProps={{
                      disabled: isArchivedSelected,
                    }}
                    tooltipText={
                      <FormattedMessage
                        id="task.detail-dialog.project.hint1"
                        defaultMessage="You cannot change project once user effort is added to task or you don't have permission to change project"
                      />
                    }
                    labelAlign="left"
                    optionsList={generateTaskDropdownData(x)}
                    singleSelect={true}
                    updateAction={(option) => handleTaskChange(option, x)}
                    selectedOptionHead={
                      <FormattedMessage
                        id="common.meeting.label2"
                        defaultMessage="Meeting Task"
                      />
                    }
                    allOptionsHead={
                      <FormattedMessage
                        id="profile-settings-dialog.apps-integrations.common.task.label"
                        defaultMessage="Tasks"
                      />
                    }
                    buttonPlaceholder={
                      <FormattedMessage
                        id="meeting.creation-dialog.form.task.placeholder"
                        defaultMessage="Select Task"
                      />}
                    isMulti={false}
                    multipleOptionsPlaceHolder="Multiple Tasks"
                  />
                ) : (
                  <Typography variant="body2" classes={{ body2: classes.taskTitleText }}>{taskTitle}</Typography>
                )}
              </div>
              <Grid
                container
                direction="row"
                justify="space-between"
                alignItems="center"
                classes={{ container: classes.gridItemDropdownCnt }}
              >
                <div>
                  <AssigneeDropdown
                    assignedTo={membersObjArr}
                    updateAction={handleUpdateAssignee}
                    isArchivedSelected={isArchivedSelected}
                    obj={x}
                    assigneeHeading={
                      <FormattedMessage
                        id="meeting.detail-dialog.participants.label"
                        defaultMessage="Participants"
                      />
                    }
                    minOneSelection={true}
                    disabled={
                      isArchivedSelected ||
                      (!meetingPermission.meetingDetail.meetingAttendee.isAllowDelete &&
                        !meetingPermission.meetingDetail.meetingAttendee.isAllowAdd) ||
                      x.status == "Published" || x.status == 'Cancelled'
                    }
                    addPermission={meetingPermission.meetingDetail.meetingAttendee.isAllowAdd}
                    deletePermission={meetingPermission.meetingDetail.meetingAttendee.isAllowDelete}
                  />
                </div>
                {x.startDate ? (
                  <div
                    className={`${classes.meetingDateCnt} flex_center_center_col`}
                  >
                    <span className={classes.meetingDate}>
                      {helper.RETURN_DAY_DATE(x.startDate)}
                    </span>
                    <Typography variant="body2" align="center">
                      {x.startDate
                        ? helper.RETURN_MONTH_NAME(x.startDate)
                        : <FormattedMessage
                          id="common.date.placeholder"
                          defaultMessage="Select Date"
                        />}
                    </Typography>
                  </div>
                ) : null}
              </Grid>
              <Grid item>
                <Grid
                  container
                  direction="row"
                  justify="space-between"
                  alignItems="center"
                  classes={{ container: classes.itemBottomBtns }}
                >
                  <DefaultButton
                    buttonType="smallIconBtn"
                    onClick={() => showMeetingsPopUp(x)}
                  >
                    <LocationIcon classes={{ root: classes.smallBtnIcon }} />
                    <Typography variant="body2" align="center">
                      {x.meetingLocation || " - "}
                    </Typography>
                  </DefaultButton>
                </Grid>
              </Grid>
            </div>
          </Grid>)
        })}
      </Grid>
    </>
  );
}


export default compose(
  withRouter,
  injectIntl,
  withStyles(itemStyles, { withTheme: true }),

)(MeetingGridItem);
