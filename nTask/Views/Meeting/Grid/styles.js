const itemStyles = theme => ({

  gridContainer: {
    alignContent: 'flex-start',
    paddingTop: '8px',
  },
  taskTitleText: {
    width: "100px",
    overflow: "hidden",
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
    textAlign: "left",
    textTransform: "none",
    fontSize: "12px",
    fontFamily: theme.typography.fontFamilyLato
  },
  AssigneeAvatar: {
    border: `2px solid ${theme.palette.common.white}`
  },
  addAssigneeBtn: {
    fontSize: "20px !important"
  },
  taskGridTaskList: {
    width: 260,
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    display: 'flex'
  },
  TotalAssignee: {
    background: theme.palette.secondary.lightBlue,
    color: theme.palette.secondary.main
  },
  taskItemCnt: {
    background: theme.palette.common.white,
    padding: "10px 10px 10px 15px",
    borderRadius: "5px",
    boxShadow: "0 2px 6px 0px rgb(0 0 0 / 20%)"
  },
  statusIcon: {
    fontSize: "11px !important"
  },
  statusItemText: {
    fontSize: "12px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightRegular
  },
  statusMenuItemCnt: {
    padding: "2px 0 2px 16px",
    fontSize: "12px !important",
    height: 20
  },
  statusMenuItemSelected: {
    background: `transparent !important`,
    "&:hover": {
      background: `transparent !important`
    }
  },
  progressCountCnt: {
    marginBottom: 5
  },
  progressBar: {
    borderRadius: 20,
    marginBottom: 15
  },
  smallBtnIcon: {
    fontSize: "20px !important",
    color: theme.palette.secondary.light
  },
  smallIconBtnCount: {
    fontSize: "16px !important",
    marginLeft: 3,
    lineHeight: "normal"
  },
  itemBottomBtns: {
    padding: "10px 5px 0 5px",
    borderTop: `1px solid ${theme.palette.border.lightBorder}`
  },
  gridItemHeadingCnt: {
    display: "flex",
    justifyContent: "space-between"

  },
  gridItemDropdownCnt: {
    marginBottom: 20,
    marginTop: 10,
    minHeight: 50
  },
  meetingDateCnt: {
    background: theme.palette.background.items,
    borderRadius: 4,
    padding: "6px 15px",
  },
  meetingDate: {
    fontSize: "18px !important",
    textTransform: "uppercase",
    fontWeight: theme.typography.fontWeightRegular
  },
  meetingMonth: {
    fontSize: "12px !important",
    color: theme.palette.text.light
  },
  recurrenceIcon: {
    fontSize: "18px !important",
    marginLeft: 5
  },
});

export default itemStyles;
