import React, { Component } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { withSnackbar } from "notistack";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import dashboardStyles from "./styles";
import differenceWith from "lodash/differenceWith";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import MenuItem from "@material-ui/core/MenuItem";
import LeftArrow from "@material-ui/icons/ChevronLeft";
import FormControl from "@material-ui/core/FormControl";
import selectStyles from "../../assets/jss/components/select";
import combineStyles from "../../utils/mergeStyles";
import ListItemText from "@material-ui/core/ListItemText";
import RoundIcon from "@material-ui/icons/Brightness1";
import ArchivedIcon from "@material-ui/icons/Archive";
import GroupingDropdown from "../../components/Dropdown/GroupingDropdown/GroupingDropdown";
import SortingDropdown from "../../components/Dropdown/SortingDropdown/SortingDropdown";
import MeetingGridItem from "./Grid/Item";
import MeetingCalendar from "./Calendar/Calendar";
import MeetingList from "./List/MeetingList.view";
import SvgIcon from "@material-ui/core/SvgIcon";
import AdvFilterIcon from "../../components/Icons/AdvFilterIcon";
import QuickFilterIcon from "../../components/Icons/QuickFilterIcon";
import CustomButton from "../../components/Buttons/CustomButton";
import ImportExportDD from "../../components/Dropdown/ImportExportDD";
import ColumnSelectionDropdown from "../../components/Dropdown/SelectedItemsDropDown";
import classNames from "classnames";
import Footer from "../../components/Footer/Footer";
import Divider from "@material-ui/core/Divider";
import { getSortOrder, sortListData } from "../../helper/sortListData";
import { sortingDropdownData, groupingDropdownData } from "./dropdownData";
import { calculateContentHeight, isScrollBottom } from "../../utils/common";
import { FetchWorkspaceInfo } from "../../redux/actions/workspace";
import { saveSortingProjectList } from "../../redux/actions/projects";
import helper from "../../helper";
import moment from "moment";
import EmptyState from "../../components/EmptyStates/EmptyState";
import { getFilteredMeetings } from "./MeetingFilter/FilterUtils";
import { setAppliedFilters } from "../../redux/actions/appliedFilters";
import SelectedRangeDatePicker from "../../components/DatePicker/SelectedRangeDatePicker";
import { teamCanView } from "../../components/PlanPermission/PlanPermission";
import PlainMenu from "../../components/Menu/menu";
import MenuList from "@material-ui/core/MenuList";
import { FormattedMessage, injectIntl } from "react-intl";
import CustomCalendar from "../../components/Calendar/Calendar.cmp";
import DefaultDialog from "../../components/Dialog/Dialog";
import AddMeetingForm from "../AddNewForms/AddMeetingForm";
import ActionConfirmation from "../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import MeetingDetails from "./MeetingDetails/MeetingDetails";
import ProjectPermissionSelector from "../../redux/selectors/projectPermissionSelector";
import RecurrenceIcon from "../../components/Icons/RecurrenceIcon";
import MeetNowIcon from "../../components/Icons/MeetNowIcon";
import isEmpty from "lodash/isEmpty";

import { grid } from "../../components/CustomTable2/gridInstance";
import debounce from "lodash/debounce";
import DefaultTextField from "../../components/Form/TextField";
import {
  getMeetings,
  UpdateMeetingCalender,
  createNextOccurrence,
} from "../../redux/actions/meetings";
import {
  getArchivedData,
  removeMeetingQuickFilter,
  updateQuickFilter,
  showAllMeetings,
  updateMeetingData,
} from "../../redux/actions/meetings";
import { getTasks } from "../../redux/actions/tasks";
import { getProjects } from "../../redux/actions/projects";
import { statusData } from "../../helper/meetingDropdownData";
import IconRefresh from "../../components/Icons/IconRefresh";
import ListLoader from "../../components/ContentLoader/List";
import { doesFilterPass } from "./List/MeetingFilter/meetingFilter.utils";


let increment = 20;
class MeetingDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      alignment: "left",
      filteredMeetings: [],
      quickFilters: [],
      dateRangeFilter: null,
      open: false,
      placement: "bottom-start",
      anchorEl: "",
      listView: true,
      gridView: false,
      calendarView: false,
      isArchived: false,
      exportedMessage: "",
      openFilterSidebar: false,
      multipleFilters: "",
      clearlastState: false,
      total: 0,
      loadMore: 0,
      isChanged: false,
      showRecords: increment,
      sortObj: this.getSortValues(),
      sortingDropDownData: sortingDropdownData,
      groupingDropdownData,
      selectedGroup: {},
      openRecurrenceConfirm: false,
      createReccurrenceDate: null,
      activeMeetingDisplayName: "",
      currentMeetingOccuranceId: null,
      currentMeeting: null,
      clickableMeeting: false,
      meetingDate: new Date(),
      meetingStartTime: {},
      meetingDuration: {},
      refreshBtnQuery: "",
      isLoading: true,
      btnQuery: "",
      selectedCalendarView: "month",
      meetingDetail: '',
    };
    this.allFilterList = [
      { key: "Show All", value: "Show All" },
      { key: "Today", value: "Today" },
      { key: "This Week", value: "This Week" },
      { key: "This Month", value: "This Month" },
      { key: "Upcoming", value: "Upcoming" },
      { key: "In Review", value: "In Review" },
      { key: "Published", value: "Published" },
      { key: "Cancelled", value: "Cancelled" },
      { key: "Overdue", value: "Overdue" },
      { key: "Archived", value: "Archived" },
    ];
    this.child = React.createRef();
    this.renderListView = this.renderListView.bind(this);
    this.renderCalendarView = this.renderCalendarView.bind(this);
    this.renderGridView = this.renderGridView.bind(this);
    this.handleArchiveChange = this.handleArchiveChange.bind(this);
    this.handleExportType = this.handleExportType.bind(this);
    this.closeSnakBar = this.closeSnakBar.bind(this);
    this.searchFilter = this.searchFilter.bind(this);
    this.searchFilterApplied = this.searchFilterApplied.bind(this);
    this.clearFilter = this.clearFilter.bind(this);
    this.returnCount = this.returnCount.bind(this);
    this.handleChangeState = this.handleChangeState.bind(this);
    this.resetCount = this.resetCount.bind(this);
    this.throttleHandleSearch = debounce(this.throttleHandleSearch, 1000);
  }
  getSortValues = () => {
    const { workspaces } = this.props;
    let loggedInTeam = this.props.loggedInTeam;

    if (loggedInTeam) {
      let { meetingColumn, meetingDirection } = getSortOrder(workspaces, loggedInTeam, 3);

      if (meetingColumn && meetingDirection) {
        return {
          column: meetingColumn,
          direction: meetingDirection,
          cancelSort: "",
        };
      } else {
        return { column: "", direction: "", cancelSort: "NONE" };
      }
    }
  };
  getAllMeetings = () => {
    this.props.getMeetings(
      null,
      null,
      succ => {
        this.setState({ refreshBtnQuery: "", isLoading: false });
      },
      () => {
        this.setState({ refreshBtnQuery: "", isLoading: false });
      }
    );
  };
  setInitialGrouping = () => {
    // Function called to set the initial value of group by in meeting
    const { itemOrderState } = this.props;
    const meetingGroupBy = itemOrderState.groupByColumn.meetingGroupBy;
    const selectedGroupByValue = groupingDropdownData.find(g => g.key == meetingGroupBy) || {}; // Finding Column object based on the column key saved on backend for grouping
    this.setState({ selectedGroup: selectedGroupByValue });
  };
  componentDidMount() {
    this.getAllMeetings();
    this.setInitialGrouping();
    this.setState({ loggedInTeam: this.props.profileState.data.loggedInTeam });

    // Setting up default selected filter ["This Month"].
    // this.handleChange({'target':{'value': ["This Month"]}});

    // For handling default meeting filter
    this.setState({
      filteredMeetings: getFilteredMeetings(
        this.props.meetings,
        this.props.appliedFiltersState.Meeting,
        [],
        null,
        this.props.tasksState,
        this.props.projects,
        this.props.meetingPer
      ),
    });
  }
  componentWillUnmount() {
    this.handleShowAllMeetings();
  }
  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.loggedInTeam !== nextProps.profileState.data.loggedInTeam)
      return { loggedInTeam: nextProps.profileState.data.loggedInTeam };
    else return { loggedInTeam: prevState.loggedInTeam };
  }
  resetCount() {
    this.setState({ showRecords: increment, loadMore: increment });
  }
  returnCount(total, loadMore) {
    this.setState({ total, loadMore });
  }
  handleScrollDown = e => {
    const bottom = isScrollBottom(e);
    if (bottom) {
      if (this.state.total > this.state.showRecords)
        this.setState({ showRecords: this.state.showRecords + increment }, () => {
          this.setState({ loadMore: this.state.showRecords }, () => {
            if (this.state.total < this.state.showRecords) {
              this.setState({
                showRecords: this.state.total,
                loadMore: this.state.total,
              });
            }
          });
        });
    }
  };
  handleChangeState = () => {
    this.setState({ isChanged: false });
  };
  clearFilter() {
    this.setState({ multipleFilters: "", clearlastState: false });
  }
  searchFilterApplied() {
    this.setState({ multipleFilters: "" });
  }
  searchFilter(data) {
    this.setState({ multipleFilters: data, isChanged: true });
  }
  closeSnakBar() {
    this.setState({ showSnakBar: false });
  }
  handleDrawerOpen = () => {
    this.setState({ openFilterSidebar: true });
  };
  handleDrawerClose = () => {
    this.setState({ openFilterSidebar: false });
  };
  handleExportType(snackBarMessage, type) {
    this.setState({ exportedMessage: snackBarMessage });
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  }
  handleArchiveChange() {
    this.setState({ isArchived: !this.state.isArchived });
  }
  renderListView() {
    this.setState({
      listView: true,
      gridView: false,
      calendarView: false,
      ganttView: false,
      alignment: "left",
    });
  }
  renderGridView() {
    this.setState({
      listView: false,
      gridView: true,
      calendarView: false,
      ganttView: false,
      alignment: "center",
    });
  }
  renderCalendarView() {
    this.setState({
      listView: false,
      gridView: false,
      calendarView: true,
      ganttView: false,
      alignment: "right",
    });
  }
  handleClick(event, placement) {
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }
  handleClose = () => {
    if (this.anchorEl.contains(event.target)) {
      return;
    }
    this.setState({ open: false });
  };
  handleSelectClick = (value, type) => {
    const { quickFilters } = this.props;
    if (quickFilters.Archived) {
      this.props.removeMeetingQuickFilter(type);
      setTimeout(() => {
        !isEmpty(grid.grid) && grid.grid.redrawRows();
      }, 0);
      return;
    }
    if (type == "Archived") {
      const filterObj = { [value]: { type: value, selectedValues: [] } };
      this.props.getArchivedData(
        3,
        succ => {
          this.props.updateQuickFilter(filterObj);
          setTimeout(() => {
            !isEmpty(grid.grid) && grid.grid.redrawRows();
          }, 0);
        },
        err => { }
      );
    } else {
      const filterObj = !quickFilters[type]
        ? {
          ...quickFilters,
          [type]: {
            type: "",
            selectedValues: [value],
          },
        }
        : {
          ...quickFilters,
          [type]: {
            type: "",
            selectedValues: quickFilters[type]["selectedValues"].includes(value)
              ? quickFilters[type]["selectedValues"].filter(item => item !== value)
              : [...quickFilters[type]["selectedValues"], ...[value]],
          },
        };
      const objKeys = Object.keys(filterObj);
      const isFilterEmpty = objKeys.every(item => filterObj[item]["selectedValues"].length == 0);
      if (isFilterEmpty) {
        this.props.showAllMeetings({});
      } else {
        this.props.updateQuickFilter(filterObj);
      }
    }
  };
  // show all task function
  handleShowAllMeetings = () => {
    this.props.showAllMeetings({});
    setTimeout(() => {
      !isEmpty(grid.grid) && grid.grid.redrawRows();
    }, 0);
  };
  filterMeeting = (meetingIds, type = "") => {
    this.setState(({ filteredMeetings }) => {
      return {
        filteredMeetings: differenceWith(filteredMeetings, meetingIds, (meeting, meetingId) => {
          return type == 5
            ? meetingId === meeting.meetingId && meeting.status !== "Published"
            : meetingId === meeting.meetingId;
        }),
      };
    });
  };
  handleClearArchived = () => {
    this.setState({ open: false });
  };
  filterUnArchiveMeetingDelete = meetingId => {
    this.setState(({ filteredMeetings }) => {
      return {
        filteredMeetings: filteredMeetings.filter(meeting => {
          return meeting.meetingId !== meetingId;
        }),
      };
    });
  };
  sortingMeetingList = (column, direction, cancelSort = "") => {
    /** function passing to sorting drop down for getting the selected value  */
    this.setState({
      sortObj: { column: column, direction: direction, cancelSort: cancelSort },
    }); /** updating state object and passing sortObj to meeting List */
    let obj = {
      meeting: [column],
      type: 3,
      MeetingOrder: direction,
    };
    this.props.saveSortingProjectList(
      obj,
      succ => { },
      err => { }
    );
  };
  filteredMeetings = () => {
    /** function for sorting if there is already saved value of sorted column and sort order */
    const { sortObj, filteredMeetings } = this.state;
    if (sortObj && sortObj.column && sortObj.direction) {
      let data = sortListData(filteredMeetings, sortObj.column, sortObj.direction);
      return data;
    } else if (sortObj && sortObj.cancelSort == "NONE") {
      return getFilteredMeetings(
        this.props.meetings,
        this.props.appliedFiltersState.Meeting,
        [],
        null,
        this.props.tasksState,
        this.props.projects,
        this.props.meetingPer
      );
    } else {
      return getFilteredMeetings(
        this.props.meetings,
        this.props.appliedFiltersState.Meeting,
        [],
        null,
        this.props.tasksState,
        this.props.projects,
        this.props.meetingPer
      );
    }
  };
  //Lifecycle method on group select
  onGroupSelect = option => {
    this.setState({ selectedGroup: option });
  };
  getTranslatedId(name) {
    switch (name) {
      case "Upcoming":
        name = "meeting.status.upcoming";
        break;
      case "In Review":
        name = "meeting.status.in-review";
        break;
      case "Published":
        name = "meeting.status.publish";
        break;
      case "Cancelled":
        name = "meeting.status.cancel";
        break;
      case "Overdue":
        name = "meeting.status.overdue";
        break;
      case "Today":
        name = "meeting.status.today";
        break;
      case "This Week":
        name = "meeting.status.week";
        break;
      case "This Month":
        name = "meeting.status.month";
        break;
    }
    return name;
  }
  componentDidUpdate() {
    const { currentMeeting } = this.state;
    const { meetings } = this.props;
    if (currentMeeting && currentMeeting != null && meetings.length) {
      const selectedMeetingObj = meetings.find(m => currentMeeting.meetingId == m.meetingId);
      if (JSON.stringify(selectedMeetingObj) !== JSON.stringify(currentMeeting)) {
        // console.log('working ')
        this.setState({ currentMeeting: selectedMeetingObj });
      }
    }
  }
  onDoubleClickEvent = (event, e) => {
    if (event.meetingOccurance) {
      let permission = this.props.meetingPer.meetingDetail.canCreateRepeatMeeting.cando;
      permission
        ? this.setState({
          currentMeetingOccuranceId: event.obj.meetingId,
          createReccurrenceDate: event.start,
          openRecurrenceConfirm: true,
          activeMeetingDisplayName: event.title,
        })
        : null;
    } else {
      const meetingDetailPer = this.props.meetingPer.meetingDetail.cando;
      meetingDetailPer ? this.setState({ currentMeeting: event.obj }) : null;
    }
  };
  showSnackBar = (snackBarMessage, type, url = null) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
        {url && (
          <span
            className={classes.snackBarBtn}
            onClick={() => {
              window.location.href = url;
            }}>
            Reauthorize Microsoft Teams
          </span>
        )}
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  handleUpdateMeetingDates = params => {
    const { start, end, id, meetingOccurance = false } = params;
    const { meetings } = this.props;
    if (!meetingOccurance) {
      let index = meetings
        .map(meeting => {
          return meeting.meetingId;
        })
        .indexOf(id);
      let meeting = { ...meetings[index] };
      let obj = {};
      obj.startDate = helper.RETURN_CUSTOMDATEFORMAT(moment(start));
      obj.startTime = moment(start).format("hh:mm A");
      obj.endTime = moment(end).format("hh:mm A");
      obj.endDateString = helper.RETURN_CUSTOMDATEFORMAT(moment(end));
      (obj.durationHours = helper.getDuration(start, end).hours),
        (obj.durationMins = helper.getDuration(start, end).minutes),
        this.props.updateMeetingData(
          { meeting, obj },
          null,
          //Success
          succ => {
            // this.setState({
            //   editMeetingDetails: false,
            //   saveDetails: false,
            //   isChange: false,
            // });
            if (grid.grid) {
              const rowNode = grid.grid && grid.grid.getRowNode(succ.id);
              rowNode.setData(succ);
            }
          },
          err => {
            if (err && err.data.url) {
              this.showSnackBar(err.data.message, "error", err.data.url);
            } else this.showSnackBar(err.data.message, "error");
            this.setState({
              saveDetails: false,
            });
          }
        );
    }
  };
  getMeetingOccurances = meetings => {
    const dates = [];
    const meeting = meetings.filter(m => {
      return m.repeatDates ? m : null;
    });
    // const newArr = dates.concat(...meeting.map(item => item.repeatDates))
    meeting.map(m => {
      m.repeatDates.map(r => {
        const obj = {
          startDate: r,
          title: m.meetingDisplayName,
          id: m.meetingId,
          owner: m.isOwner,
          meetingObj: m,
        };
        dates.push(obj);
      });
    });
    const repeatOccurance = this.getOccuranceobj(dates);
    return repeatOccurance;
  };
  getOccuranceobj = dates => {
    const { classes, theme } = this.props;
    const className = classes.repeatOccuranceStyle;
    const occuranceObj = dates.map(d => {
      return {
        id: d.startDate,
        meetingOccurance: true,
        title: d.title,
        allDay: false,
        start: new Date(moment(d.startDate).format("MM/DD/YYYY hh:mm A")),
        end: new Date(moment(d.startDate).format("MM/DD/YYYY hh:mm A")),
        meetingsColor: theme.palette.border.extraLightBorder,
        borderColor: "1px solid #cecece",
        obj: d.meetingObj,
        borderRadius: "0px",
        className: className,
        color: theme.palette.text.light,
      };
    });
    return occuranceObj;
  };
  addNewMeeting = ({ start, end }) => {
    if (this.props.meetingPer.createMeeting.cando) {
      this.setState({
        meetingStartTime: helper.startTime(start),
        meetingDuration: helper.getDuration(start, end),
        clickableMeeting: true,
        meetingDate: new Date(start),
      });
    }
  };
  handleCloseClickable = () => {
    this.setState({ clickableMeeting: false });
  };
  handleDialogClose = name => {
    this.setState({
      openRecurrenceConfirm: false,
      createReccurrenceDate: "",
      activeMeetingDisplayName: "",
      currentMeetingOccuranceId: "",
    });
  };
  handleCreateReccureMeeting = () => {
    const { createReccurrenceDate, currentMeetingOccuranceId } = this.state;
    this.setState({ btnQuery: "progress" });

    const data = {
      MeetingId: currentMeetingOccuranceId,
      date: moment(createReccurrenceDate).format("MM/DD/YYYY hh:mm:ss A"),
    };

    // Create Next Meeting occurence
    this.props.createNextOccurrence(
      data,
      //Success
      () => {
        this.setState({ btnQuery: "" });
        this.handleDialogClose();
      },
      //Failure
      error => {
        this.setState({ btnQuery: "" });
        this.props.showSnackBar(error.data.message, "error");
      }
    );
  };
  meetingEvents(meetings) {
    const { theme } = this.props;
    const meetingStatusColor = {
      Upcoming: theme.palette.meetingStatus.Upcoming,
      Cancelled: theme.palette.meetingStatus.Cancelled,
      OverDue: theme.palette.meetingStatus.OverDue,
      InReview: theme.palette.meetingStatus.InReview,
      Published: theme.palette.meetingStatus.Published,
    };
    const filteredMettings = meetings.map(m => {
      let meeting = this.getMeetingPer(m);
      return {
        id: meeting.meetingId,
        title: meeting.meetingDisplayName || "-",
        allDay: false,
        start: new Date(
          helper.RETURN_CUSTOMDATEFORMAT(moment(new Date(meeting.startDate)), meeting.startTime)
        ),
        end: new Date(
          helper.RETURN_CUSTOMDATEFORMAT(moment(new Date(meeting.endDate)), meeting.endTime)
        ),
        meetingsColor:
          meeting.colorCode && !meeting.colorCode.includes("fff")
            ? meeting.colorCode
            : meetingStatusColor[meeting.status],
        obj: meeting,
        meetingZoom: meeting.zoomMeetingId,
        meetingOccurance: false,
      };
    });
    return filteredMettings;
  }
  getMeetingPer = m => {
    const { meetingPer, permissionObject } = this.props;

    m.meetingPermission = m?.projectId
      ? !permissionObject[m.projectId]
        ? meetingPer
        : permissionObject[m.projectId].meeting
      : meetingPer;
    return m;
  };
  closeMeetingDetailsPopUp = () => {
    this.setState({ currentMeeting: null });
  };
  throttleHandleSearch = data => {
    grid.grid && grid.grid.setQuickFilter(data);
  };
  //handle task search
  handleSearch = e => {
    this.throttleHandleSearch(e.target.value);
    // this.setState({searchQuery: e.target.value});
  };
  handleRefreshList = () => {
    this.setState({ refreshBtnQuery: "progress", isLoading: true }, () => {
      this.props.getMeetings(
        null,
        null,
        succ => {
          this.setState({ refreshBtnQuery: "", isLoading: false });
          this.handleExportType("Grid Refreshed Successfully!", "success");
        },
        () => {
          this.setState({ refreshBtnQuery: "", isLoading: false });
          this.handleExportType("Oops! Server throws Error.", "error");
        }
      );
    });
  };
  // refresh grid after import meetings
  handleImportedMeetings = () => {
    this.setState({ refreshBtnQuery: "progress", isLoading: true }, () => {
      this.props.getProjects(null, null, succ => {
        this.props.getTasks(null, null, succ => {
          this.props.getMeetings(
            null,
            null,
            succ => {
              this.setState({ refreshBtnQuery: "", isLoading: false });
              this.handleExportType("Grid Refreshed Successfully!", "success");
            },
            () => {
              this.setState({ refreshBtnQuery: "", isLoading: false });
              this.handleExportType("Oops! Server throws Error.", "error");
            }
          );
        })
      })
    });
  };
  render() {
    const { classes, theme, quote, workspaces, loggedInTeam, intl, quickFilters } = this.props;
    const {
      alignment,
      calendarView,
      gridView,
      openFilterSidebar,
      exportedMessage,
      filteredMeetings,
      multipleFilters,
      showRecords,
      refreshBtnQuery,
      isLoading,
      dateRangeFilter,
      selectedGroup = {},
    } = this.state;

    const EventRenderer = ({ event }) => {
      return (
        <>
          {event.title}
          {event.meetingOccurance && (
            <SvgIcon
              viewBox="0 0 14 12.438"
              htmlColor={"#0090ff"}
              className={classes.recurrenceIcon}>
              <RecurrenceIcon />
            </SvgIcon>
          )}
          {event.meetingZoom && (
            <SvgIcon
              viewBox="0 0 20.87 14.602"
              htmlColor={theme.palette.secondary.medDark}
              style={{ right: event.meetingOccurance ? 35 : 5 }}
              className={classes.meetNowIcon}>
              <MeetNowIcon />
            </SvgIcon>
          )}
        </>
      );
    };
    const eventStyle = event => {
      if (event.meetingsColor) {
        if (event.borderColor) {
          return {
            style: {
              background: event.meetingsColor,
              border: event.borderColor,
              color: event.color,
              borderRadius: event.borderRadius,
            },
            className: event.className,
          };
        } else {
          return {
            style: {
              background: event.meetingsColor,
              borderRadius: event.borderRadius,
            },
          };
        }
      } else {
        return {};
      }
    };
    const Status = statusData(theme, classes, intl);
    let meetingData = this.meetingEvents(this.props.meetings);
    const MeetingOccurances = this.getMeetingOccurances(this.props.meetings);
    meetingData = meetingData.concat(MeetingOccurances);
    const Date = [
      {
        label: intl.formatMessage({ id: this.getTranslatedId("Today"), defaultMessage: "Today" }),
        name: "today",
      },
      {
        label: intl.formatMessage({ id: this.getTranslatedId("This Week"), defaultMessage: "This Week", }),
        name: "currentWeek",
      },
      {
        label: intl.formatMessage({ id: this.getTranslatedId("This Month"), defaultMessage: "This Month", }),
        name: "currentMonth",
      },
    ];
    const isArchived = quickFilters.length && quickFilters[quickFilters.length - 1] === "Archived";
    let selectedItems = [];
    let activeQuickFilterKeys = Object.keys(quickFilters).length
      ? Object.keys(quickFilters)
      : "All";
    if (Array.isArray(activeQuickFilterKeys)) {
      if (activeQuickFilterKeys[0] == "Archived") {
        activeQuickFilterKeys = "Archived";
      } else {
        activeQuickFilterKeys.map(item => {
          if (quickFilters[item]) {
            selectedItems = [...selectedItems, ...quickFilters[item]["selectedValues"]];
          } else {
            selectedItems = [...selectedItems, item];
          }
        });
        activeQuickFilterKeys = selectedItems.length
          ? `${selectedItems[0]} ${selectedItems.length > 1 ? `& +${selectedItems.length - 1}` : ""
          }`
          : "All";
      }
    }
    return (
      <div className={classes.root}>
        <main
          className={
            classNames(classes.content, {
              [classes.contentShift]: openFilterSidebar,
            })
          }>
          <div className={classes.drawerHeader}>
            <Grid container classes={{ container: classes.taskDashboardCnt }}>
              <Grid
                container
                direction="row"
                justify="space-between"
                alignItems="center"
                classes={{ container: classes.taskDashboardHeader }}>
                <div className="flex_center_start_row">
                  {isArchived ? (
                    <>
                      <LeftArrow
                        onClick={this.handleClearArchived}
                        className={classes.backArrowIcon}
                      />
                      <Typography variant="h1">
                        <FormattedMessage
                          id="common.archived.meeting.label"
                          defaultMessage="Archived Meetings"
                        />
                      </Typography>
                    </>
                  ) : (
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        marginRight: "16px",
                      }}>
                      <Typography variant="h1" className={classes.listViewHeading}>
                        Meetings
                      </Typography>
                      <span className={classes.count}>
                        {this.props.meetings.filter(t => doesFilterPass({ data: t })).length}
                      </span>
                    </div>
                  )}
                  {/* {!quickFilters["Archived"] ? ( */}
                  <div className={classes.toggleContainer}>
                    <ToggleButtonGroup
                      value={alignment}
                      exclusive
                      classes={{ root: classes.toggleBtnGroup, groupedHorizontal: classes.groupedHorizontal }}>
                      <ToggleButton
                        value="left"
                        onClick={this.renderListView}
                        classes={{
                          root: classes.toggleButton,
                          selected: classes.toggleButtonSelected,
                        }}>
                        <FormattedMessage id="common.list.label" defaultMessage="List" />
                      </ToggleButton>
                      <ToggleButton
                        value="center"
                        onClick={this.renderGridView}
                        classes={{
                          root: classes.toggleButton,
                          selected: classes.toggleButtonSelected,
                        }}>
                        <FormattedMessage id="common.grid.label" defaultMessage="Grid" />
                      </ToggleButton>

                      <ToggleButton
                        value="right"
                        onClick={this.renderCalendarView}
                        classes={{
                          root: classes.toggleButton,
                          selected: classes.toggleButtonSelected,
                        }}>
                        <FormattedMessage id="common.calendar.label" defaultMessage="Calendar" />
                      </ToggleButton>
                    </ToggleButtonGroup>
                  </div>
                  {/* ) : null} */}
                </div>

                {!isArchived ? (
                  <div className="flex_center_start_row">
                    {this.state.listView /** Advanced sorting only display in premium and bussiness plan */ && (
                      <>
                      </>
                    )}

                    {calendarView ? null : (
                      <React.Fragment>
                        {/* search filed */}
                        {this.state.listView ?
                          <>
                            <DefaultTextField
                              fullWidth={false}
                              // errorState={forgotEmailError}
                              error={false}
                              // errorMessage={forgotEmailMessage}
                              formControlStyles={{ width: 250, marginBottom: 0, marginRight: 10 }}
                              defaultProps={{
                                id: "meetingListSearch",
                                onChange: this.handleSearch,
                                placeholder: "Search Meeting",
                                autoFocus: true,
                                inputProps: { maxLength: 150, style: { padding: "9px 14px" } },
                              }}
                            />
                          </>
                          : null}
                        {/* refresh btn */}
                        <CustomButton
                          onClick={this.handleRefreshList}
                          query={refreshBtnQuery}
                          style={{
                            // padding: customFilterSelect ? "4px 8px 4px 4px" : "3px 8px 3px 4px",
                            padding: "9px 8px",
                            borderRadius: "4px",
                            display: "flex",
                            justifyContent: "space-between",
                            minWidth: "auto",
                            whiteSpace: "nowrap",
                            marginRight: 10,
                            height: 32,
                          }}
                          btnType={"white"}
                          variant="contained">
                          <SvgIcon
                            viewBox="0 0 12 12.015"
                            className={classes.qckFfilterIconRefresh}>
                            <IconRefresh />
                          </SvgIcon>
                        </CustomButton>
                        <FormControl className={classes.formControl}>
                          <CustomButton
                            onClick={event => {
                              this.handleClick(event, "bottom-end");
                            }}
                            buttonRef={node => {
                              this.anchorEl = node;
                            }}
                            style={{
                              // padding: customFilterSelect ? "4px 8px 4px 4px" : "3px 8px 3px 4px",
                              padding: "3px 8px 3px 4px",
                              borderRadius: "4px",
                              display: "flex",
                              justifyContent: "space-between",
                              minWidth: "auto",
                              whiteSpace: "nowrap",
                            }}
                            btnType={!isEmpty(quickFilters) ? "lightBlue" : "white"}
                            variant="contained">
                            <SvgIcon
                              classes={{root: classes.quickFilterIconSize}}
                              viewBox="0 0 24 24"
                              className={
                                true ? classes.qckFfilterIconSelected : classes.qckFilterIconSvg
                              }>
                              <QuickFilterIcon />
                            </SvgIcon>
                            <span
                              className={
                                !isEmpty(quickFilters)
                                  ? classes.qckFilterLblSelected
                                  : classes.qckFilterLbl
                              }>
                              <FormattedMessage id="common.show.label" defaultMessage="Show" /> :{" "}
                            </span>
                            {/* <span className={classes.checkname}>{selectedFiltersLbl}</span> */}
                            <span className={classes.checkname}>{activeQuickFilterKeys}</span>
                          </CustomButton>
                          <PlainMenu
                            open={this.state.open}
                            closeAction={this.handleClose}
                            placement="bottom-start"
                            anchorRef={this.anchorEl}
                            style={{ width: 280 }}
                            offset="0 15px">
                            <MenuList disablePadding>
                              {/* showl all */}
                              <MenuItem
                                className={`${classes.statusMenuItemCnt} ${isEmpty(quickFilters) ? classes.selectedValue : ""
                                  }`}
                                classes={{
                                  root: classes.customRootMenuItem,
                                  selected: classes.statusMenuItemSelected,
                                }}
                                value="Show All"
                                onClick={this.handleShowAllMeetings}>
                                <ListItemText
                                  primary={
                                    <FormattedMessage
                                      id="common.show.all"
                                      defaultMessage="Show All"
                                    />
                                  }
                                  classes={{ primary: classes.plainItemText }}
                                />
                              </MenuItem>
                              {!quickFilters["Archived"] ? (
                                <>
                                  <Divider />
                                  <MenuItem
                                    disableRipple={true}
                                    classes={{
                                      root: classes.menuHeadingItem,
                                    }}>
                                    {this.props.intl.formatMessage({
                                      id: "meeting.creation-dialog.form.date.label",
                                      defaultMessage: "Date",
                                    })}
                                  </MenuItem>
                                  {Date.map(item => (
                                    <MenuItem
                                      key={item.name}
                                      value={item.name}
                                      className={`
                                      ${classes.statusMenuItemCnt}
                                   ${!isEmpty(quickFilters) && quickFilters[item.name] && quickFilters[item.name].selectedValues.length
                                          ? classes.selectedValue
                                          : ""
                                        }`}
                                      classes={{
                                        root: classes.customRootMenuItem,
                                        selected: classes.statusMenuItemSelected,
                                      }}
                                      onClick={() => this.handleSelectClick(item.name, item.name)}>
                                      <ListItemText
                                        primary={item.label}
                                        classes={{
                                          primary: classes.statusItemTextMeeting,
                                        }}
                                      />
                                    </MenuItem>
                                  ))}
                                  <Divider />
                                  <MenuItem classes={{ root: classes.menuHeadingItem }}>
                                    <FormattedMessage
                                      id="task.creation-dialog.form.status.label"
                                      defaultMessage="Status"
                                    />
                                  </MenuItem>
                                  {/* status quick filters */}
                                  {Status.map(item => (
                                    <MenuItem
                                      key={item.name}
                                      value={item.name}
                                      className={`${classes.statusMenuItemCnt} 
                                  ${!isEmpty(quickFilters) &&
                                          quickFilters?.status?.selectedValues.includes(item.value)
                                          ? classes.selectedValue
                                          : ""
                                        }`}
                                      classes={{
                                        root: classes.customRootMenuItem,
                                        selected: classes.statusMenuItemSelected,
                                      }}
                                      onClick={() => this.handleSelectClick(item.value, "status")}>
                                      <RoundIcon
                                        htmlColor={item.color}
                                        classes={{ root: classes.statusIcon }}
                                      />
                                      <ListItemText
                                        primary={item.label}
                                        classes={{
                                          primary: classes.statusItemText,
                                        }}
                                      />
                                    </MenuItem>
                                  ))}
                                </>
                              ) : null}

                              <MenuItem
                                value={"Archived"}
                                classes={{ root: classes.highlightItem }}
                                onClick={() => this.handleSelectClick("Archived", "Archived")}>
                                <ArchivedIcon
                                  htmlColor={theme.palette.secondary.light}
                                  classes={{
                                    root: classes.selectHighlightItemIcon,
                                  }}
                                />
                                <FormattedMessage
                                  id="common.archived.archived"
                                  defaultMessage="Archived"
                                />
                              </MenuItem>
                            </MenuList>
                          </PlainMenu>
                          {/* </Select> */}
                        </FormControl>
                      </React.Fragment>
                    )}
                    <ImportExportDD
                      handleExportType={this.handleExportType}
                      filterList={quickFilters}
                      multipleFilters={multipleFilters}
                      handleRefresh={this.handleImportedMeetings}
                      ImportExportType={"meeting"}
                      data={filteredMeetings}
                    />
                    {this.state.listView && (
                      <ColumnSelectionDropdown
                        feature={"meeting"}
                        onColumnHide={this.onColumnHide}
                        hideColumns={["matrix"]}
                        btnProps={{
                          style: {
                            border: "1px solid #dddddd",
                            padding: "5px 10px",
                            borderRadius: 4,
                            marginLeft: 10,
                          },
                        }}
                      />
                    )}
                  </div>
                ) : null}
              </Grid>
              <Grid container classes={{ container: classes.dashboardContentCnt }}>
                {calendarView ? (
                  <Grid
                    item
                    classes={{ item: classes.taskCalendarCnt }}
                    style={{ height: calculateContentHeight() }}>
                    {/* <div className={classes.calendarCnt}> */}
                    <CustomCalendar
                      onDoubleClickEvent={this.onDoubleClickEvent}
                      updateDates={this.handleUpdateMeetingDates}
                      data={meetingData}
                      EventRenderer={EventRenderer}
                      eventPropGetter={eventStyle}
                      onSelectSlot={this.addNewMeeting}
                      resizable={!this.state.selectedCalendarView.includes("month")}
                      monthView={true}
                      weekView={true}
                      dayView={true}
                      draggableAccessor={event => {
                        return !event.meetingOccurance;
                      }}
                      updateView={view => {
                        this.setState({
                          selectedCalendarView: view,
                        });
                      }}
                    />
                    {/* </div> */}
                  </Grid>
                ) : gridView ? (
                  <>
                    <Grid
                      container
                      onScroll={this.handleScrollDown}
                      classes={{ container: classes.taskGridCnt }}
                      style={{ height: calculateContentHeight() }}>
                      {this.props.meetings.length > 0 ? (
                        <MeetingGridItem
                          meetingState={quickFilters}
                          returnCount={this.returnCount}
                          handleChangeState={this.handleChangeState}
                          isChanged={this.state.isChanged}
                          showRecords={showRecords}
                          resetCount={this.resetCount}
                          isArchivedSelected={isArchived}
                          sortObj={this.state.sortObj}
                          meetings={filteredMeetings}
                        />
                      ) : isArchived ? (
                        <EmptyState
                          screenType="Archived"
                          heading={
                            <FormattedMessage
                              id="common.archived.label"
                              defaultMessage="No archived items found"
                            />
                          }
                          message={
                            <FormattedMessage
                              id="common.archived.message"
                              defaultMessage="You haven't archived any items yet."
                            />
                          }
                          button={false}
                        />
                      ) : this.props.meetingPer.createMeeting.cando ? (
                        <EmptyState
                          screenType="meeting"
                          heading={
                            <FormattedMessage
                              id="common.create-first.meeting.label"
                              defaultMessage="Create your first meeting"
                            />
                          }
                          message={
                            <FormattedMessage
                              id="common.create-first.meeting.messagea"
                              defaultMessage='You do not have any meetings yet. Press "Alt + M" or click on button below.'
                            />
                          }
                          button={true}
                        />
                      ) : (
                        <EmptyState
                          screenType="meeting"
                          heading={
                            <FormattedMessage
                              id="common.create-first.meeting.messageb"
                              defaultMessage="You do not have any meetings yet."
                            />
                          }
                          message={
                            <FormattedMessage
                              id="common.create-first.meeting.messagec"
                              defaultMessage="Request your team member(s) to assign you a meeting."
                            />
                          }
                          button={false}
                        />
                      )}
                    </Grid>
                  </>
                ) : (
                  <Grid item classes={{ item: classes.taskListCnt }}>
                    {this.state.isLoading ? <ListLoader style={{ paddingLeft: 32 }} /> : <MeetingList />}

                  </Grid>
                )}
              </Grid>
            </Grid>
          </div>
        </main>
        {this.state.openRecurrenceConfirm && (
          <ActionConfirmation
            open={this.state.openRecurrenceConfirm}
            closeAction={this.handleDialogClose}
            cancelBtnText="Cancel"
            successBtnText="Yes, Create Next Occurrence"
            alignment="center"
            headingText="Create Next Occurrence"
            iconType="recurrence"
            msgText={
              <>
                This meeting is part of "{this.state.activeMeetingDisplayName}" schedule
                and does not exist yet.
                <br />
                <br />
                Would you like to create next occurrence?
              </>
            }
            successAction={this.handleCreateReccureMeeting}
            btnQuery={this.state.btnQuery}
          />
        )}
        <DefaultDialog
          title={
            <FormattedMessage
              id="meeting.creation-dialog.title"
              defaultMessage="Add Meeting Details"></FormattedMessage>
          }
          open={this.state.clickableMeeting}
          onClose={this.handleCloseClickable}>
          <AddMeetingForm
            handleCloseMeeting={this.handleCloseClickable}
            isNew={true}
            isDialog={true}
            preFilledData={{
              meetingDate: this.state.meetingDate,
              startTime: this.state.meetingStartTime,
              calenderView: {
                view: this.state.selectedCalendarView,
              },
              meetingDuration: this.state.meetingDuration,
            }}
            maxLength={250}
          />
        </DefaultDialog>

        {this.state.currentMeeting ?
          <MeetingDetails
            closeMeetingDetailsPopUp={this.closeMeetingDetailsPopUp}
            currentMeeting={this.state.currentMeeting}
          /> : null}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    meetingsState: state.meetings,
    meetings: state.meetings.data || [],
    profileState: state.profile,
    appliedFiltersState: state.appliedFilters,
    itemOrderState: state.itemOrder.data,
    tasksState: state.tasks.data,
    workspaces: state.profile.data.workspace,
    loggedInTeam: state.profile.data.loggedInTeam,
    meetingPer: state.workspacePermissions.data.meeting,
    projects: state.projects.data || [],
    permissionObject: ProjectPermissionSelector(state),
    quickFilters: state.meetings.quickFilters || {},
    meetingFilters: state.meetings.meetingFilter,
  };
};

export default compose(
  withRouter,
  withSnackbar,
  injectIntl,
  withStyles(combineStyles(dashboardStyles, selectStyles), {
    withTheme: true,
  }),
  connect(mapStateToProps, {
    getMeetings,
    getProjects,
    getTasks,
    setAppliedFilters,
    FetchWorkspaceInfo,
    saveSortingProjectList,
    UpdateMeetingCalender,
    createNextOccurrence,
    getArchivedData,
    removeMeetingQuickFilter,
    updateQuickFilter,
    showAllMeetings,
    updateMeetingData,
  })
)(MeetingDashboard);
