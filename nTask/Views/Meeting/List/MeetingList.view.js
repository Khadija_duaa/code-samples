import React, { useCallback, useEffect, useMemo, useRef, useState } from "react";
import { withSnackbar } from "notistack";
import { withRouter } from "react-router-dom";
import loadable from '@loadable/component'
const CustomTable = loadable(() => import("../../../components/CustomTable2/listViewTable.cmp"));
import { useDispatch, useSelector } from "react-redux";
import isEqual from "lodash/isEqual";
import { AgGridColumn } from "@ag-grid-community/react";
import MeetingActionDropdown from "../MeetingActionDropdown/MeetingActionDropdown";
import StatusDropdown from "../../../components/Dropdown/StatusDropdown/Dropdown";
import withStyles from "@material-ui/core/styles/withStyles";
import AddIcon from "@material-ui/icons/Add";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";
import { updateTaskData } from "../../../redux/actions/tasks";
import { FormattedMessage, injectIntl } from "react-intl";
import SideDrawer from "../Drawer/Drawer";

import CustomTooltip from "../../../components/Tooltip/Tooltip";
import CustomIconButton from "../../../components/Buttons/CustomIconButton";
import RecurrenceBtn from "../../../components/Buttons/RecurrenceBtn";
import SvgIcon from "@material-ui/core/SvgIcon";
import RecurrenceIcon from "../../../components/Icons/RecurrenceIcon";
import { compose } from "redux";
import { sortListData, getSortOrder } from "../../../helper/sortListData";
import { generateTaskData, generateProjectData } from "../../../helper/generateSelectData";
import helper from "../../../helper";
import SearchDropdown from "../../../components/Dropdown/SearchDropdown";
import AssigneeDropdown from "../../../components/Dropdown/AssigneeDropdown";
import isEmpty from "lodash/isEmpty";
import moment from "moment";
import Typography from "@material-ui/core/Typography";
import { Circle } from "rc-progress";
import meetingColumnDefs, { sortAlphabetically } from "./meetingColumns";
import Meetingcmp from "../../../components/BulkActionsMeeting/Meeting.cmp";
import GroupByComponents from "../../../components/CustomTable2/GroupByComponents/GroupByComponents";
import CustomDatePicker from "../../../components/DatePicker/DatePicker/ListViewDatePicker";
import TimePicker from "../../../components/DatePicker/TimePicker";
import Stared from "../../../components/Starred/Starred";
import { issueDetailDialogState } from "../../../redux/actions/allDialogs";
import ColumnSelector from "../../../redux/selectors/columnSelector";
import isNull from "lodash/isNull";
import CustomFieldRenderer from "../../../components/CustomTable2/CustomFieldsColumn/CustomFieldRenderer";
import { headerProps } from "./constants";
import { getMeetingsPermissions, getReadOnlyPermissions, canDo } from "../permissions";
import queryString from "query-string";

import { generateTaskDropdownData, statusData } from "../../../helper/meetingDropdownData";
import EmptyState from "../../../components/EmptyStates/EmptyState";
import { grid } from "../../../components/CustomTable2/gridInstance";
import isUndefined from "lodash/isUndefined";
import ProjectPermissionSelector from "../../../redux/selectors/projectPermissionSelector";
import { doesFilterPass } from "./MeetingFilter/meetingFilter.utils";
import MeetingFilter from "./MeetingFilter/meetingFilter.view";
import DefaultDialog from "../../../components/Dialog/Dialog";
import {
  createIssue,
  updateIssueData,
  UpdateIssueCustomField,
  updateIssueObject,
  exportBulkIssue,
  // getSavedFilters,
} from "../../../redux/actions/issues";

import { emptyMeeting } from "../../../utils/constants/emptyMeeting";
import meetingListStyles from "./meetingList.style";
import fileDownload from "js-file-download";
import {
  createMeeting,
  updateMeetingData,
  getSavedFilters,
  BulkUpdateMeeting,
  DeleteMeeting,
  ArchiveMeeting,
  UnArchiveMeeting,
  DeleteMeetingSchedule,
  ArchiveMeetingSchedule,
  UnArchiveMeetingSchedule,
  exportBulkMeeting,
  CancelMeeting,
  CancelMeetingSchedule,
  MarkMeetingAsStarted,
} from "../../../redux/actions/meetings";
import MeetingDetails from "../MeetingDetails/MeetingDetails";
import useUpdateIssueHook from "../../../helper/customHooks/issues/updateIssue";
import useUpdateMeeting from "../../../helper/customHooks/meetings/updateMeeting";
import AddMeetingForm from "../../AddNewForms/AddMeetingForm";
import { addPendingHandler } from "../../../redux/actions/backProcesses";
import mixpanel from 'mixpanel-browser';
import { MixPanelEvents } from '../../../mixpanel';

// const defaultColDef = { minWidth: 200, headerClass: "customHeader" };
const MeetingList = React.memo(({ classes, theme, intl, enqueueSnackbar, style, history }) => {
  const state = useSelector(state => {
    return {
      meetingColumns: ColumnSelector(state).meeting.columns,
      sections: ColumnSelector(state).meeting.sections,
      nonSectionFields: ColumnSelector(state).meeting.nonSectionFields,
      // taskColumns: ColumnSelector(state).task.columns,
      // sections: ColumnSelector(state).task.sections,
      // nonSectionFields: ColumnSelector(state).task.nonSectionFields, 
      issues: state.meetings.data,

      meetings: state.meetings.data,
      workspaces: state.profile.data.workspace,
      workspaceMeetingPer: state.workspacePermissions.data.meeting,
      projects: state.projects.data,
      allTasks: state.tasks.data,
      members: state.profile.data.member.allMembers,
      workspaceTemplates: state.workspaceTemplates.data,
      globalTimerTaskState: state.globalTimerTask,
      permissionObject: ProjectPermissionSelector(state),
      workspaceIssuePer: state.workspacePermissions.data.issue,
      workspaceStatus: state.workspaceTemplates.data.defaultWSTemplate,
      profileState: state.profile.data,
      quickFilters: state.meetings.quickFilters,
      meetingFilter: state.meetings.meetingFilter,
    };
  });
  const sectionGroup = localStorage.getItem("issueSectionGrouping");
  const dispatch = useDispatch();
  const [selectedMeetings, setSelectedMeetings] = useState([]);
  const [repeatMeetingDrawer, setTepeatMeetingDrawer] = useState(false);
  const [repeatMeetingId, setRepeatMeetingId] = useState(null);
  const [mappingDialogState, setMappingDialogState] = useState({
    tempProject: null,
    openStatusDialog: false,
    newTemplateItem: {},
    oldStatusItem: {},
    tempTask: {},
  });

  const [meetingDetail, setMeetingDetail] = useState('');
  const timerState = useRef(null);
  const statusTemplates = useRef(null);
  const [sectionGrouping, setSectionGrouping] = useState(sectionGroup === "true" ? true : false);
  const { editMeeting } = useUpdateMeeting()
  const {
    meetingColumns = [],
    issues = [],
    meetings = [],
    projects,
    members,
    sections = [],
    nonSectionFields = [],
    globalTimerTaskState,
    workspaceIssuePer,
    permissionObject,
    workspaceStatus,
    profileState,
    quickFilters,
    meetingFilter,
    allTasks = [],
    workspaces,
    workspaceMeetingPer
  } = state;

  useEffect(() => {
    getSavedFilters("meeting", dispatch);
    return () => {
      grid.grid = null;
    };
  }, []);
  const updateMeetingObj = (meeting, type, value) => {
    editMeeting(meeting, type, value);
  };

  const updateMeeting = (meeting, obj) => {
    updateMeetingData(
      { meeting, obj },
      dispatch,
      //Success
      meeting => {
        if (grid.grid) {
          const rowNode = grid.grid && grid.grid.getRowNode(meeting.id);
          rowNode.setData(meeting);
        }
      },
      err => {
        if (err && err.data) showSnackBar(err.data.message, "error");
      }
    );
  };
  //Handle planned/actual date save
  const handleMeetingStared = (stared, obj) => {
    updateMeeting(obj, { isStared: stared });
  };
  const CustomRenderer = ({ meeting, member, iconChildren }) => {
    if (meeting) {
      let assigneeLists = meeting.createdById == member.userId
      if (assigneeLists) return <> </>;
      else return iconChildren;
    }
  };
  const handleTaskChange = (task, obj) => {
    // Handle task change
    let object = {
      taskId: task.length ? task[0].id : '',
    }
    if (obj.projectId && task.length) object.projectId = task[0].obj.projectId;
    updateMeeting(obj, object);
    // updateMeeting(obj, {
    //   taskId: task.length ? task[0].id : '',
    //   // projectId:
    //   //   !obj.projectId && task.length
    //   //     ? task[0].obj.projectId
    //   //     : obj.projectId,
    // });
  };
  const handleUpdateAssignee = (assignees, obj) => {
    // Handle Assignee change
    const attendeeList = assignees.map(a => a.userId);
    updateMeeting(obj, { attendeeList: attendeeList });
  };

  const handleStatusChange = (status, obj) => {
    // updateMeeting(obj, { status: status.value });
  };

  const handleProjectChange = (newValue, meeting) => {
    const projectId = newValue.length ? newValue[0].id : "";
    // const selectedMeetings = allTasks.filter(task => {
    //   return meeting.taskId.indexOf(task.taskId) > -1;
    // });

    // let linkTask = selectedMeetings.length
    //   ? selectedMeetings[0].meetingId == "" || !selectedMeetings[0].meetingId
    //     ? []
    //     : meeting.taskId
    //   : meeting.taskId;

    const obj = {
      projectId: projectId,
      // taskId: newValue.length == 0 ? [] : linkTask,
    };
    updateMeeting(meeting, obj);
  };
  //Handle Add Task
  const handleAddMeeting = (data, callback) => {
    // if (!data.value.replace(/\s/g, '').length) {
    //   showSnackBar('Meeting Name is incorrect please try with other name', "error");
    //   return;
    // }
    //Post Obj is object to be posted to backend for task creation
    const postObj = {
      meetingDisplayName: data.value,
      title: data.value,
      clientId: data.clientId
    };

    //Dispatch Obj is object that is dispatched before the api call for quick entry
    const dispatchObj = {
      ...emptyMeeting,
      meetingDisplayName: data.value,
      title: data.value,
      clientId: data.clientId,
      id: data.clientId,
      meetingId: data.clientId,
    };
    //Calling grid add method to add task
    // callback(dispatchObj, "add");
    createMeeting(postObj, dispatch, dispatchObj, res => {
      mixpanel.track(MixPanelEvents.MeetingCreationEvent, res);
      //Calling callback to edit the added record with the actual data
      // callback(res, "edit");
    }, fail => {
      DeleteMeeting(dispatchObj.meetingId, (response) => { }, err => { }, dispatch);
      showSnackBar('Meeting Name already exist please try with other name', "error");
    }
    );
    // CreateInLineMeeting(data, (err, x) => {
    //   if (x) {
    //     grid.selectAllCheckbox.checked = false;
    //     //  setState({ addMeeting: false });
    //   } else {
    //     setState({
    //       TaskError: false,
    //       TaskErrorMessage: x ? x.data : "",
    //     });
    //   }
    // }) 
  };

  // Generate list of all projects for dropdown understandable form
  const generateProjectDropdownData = meetings => {
    let filteredProjects = projects.filter(p => p.projectId !== meetings.projectId);
    const projectsArr = filteredProjects.map(project => {
      return { label: project.projectName, id: project.projectId, obj: meetings };
    });
    return projectsArr;
  };
  //Handle planned/actual date save
  const handleDateSave = (date, meeting, time) => {
    const formatDate = helper.RETURN_CUSTOMDATEFORMAT(moment(date), meeting.startTime);

    updateMeetingObj(meeting, 'startDate', formatDate)
  };
  //Update Row if  Meeting Timer value is changed
  useEffect(() => {
    if (!isEmpty(grid.grid)) {
      const task = timerState.current
        ? timerState.current.task
        : globalTimerTaskState
          ? globalTimerTaskState.task
          : null;
      const rowNode = task && grid.grid.getRowNode(task.id);
      rowNode && rowNode.setData(task);
      timerState.current = globalTimerTaskState;
    }
  }, [globalTimerTaskState, grid.grid]);

  // export issue api function
  const handleExportMeeting = props => {
    const selectedNodes = grid.grid.getSelectedNodes();
    const selectedNodesLength = selectedNodes?.length || 1
    const postObj = selectedNodes.length
      ? { meetingIds: selectedNodes.map(i => i.data.meetingId) }
      : { meetingIds: [props.node.data.id] };

    const obj = {
      type: 'meetings',
      apiType: 'post',
      data: postObj,
      fileName: 'meetings.xlsx',
      apiEndpoint: 'api/export/bulkmeeting',
    }
    addPendingHandler(obj, dispatch);

    // exportBulkMeeting(postObj, dispatch,
    //   res => {
    //     fileDownload(res.data, "meetings.xlsx");
    //     showSnackBar(`${selectedNodesLength} meetings exported successfully`, "success");
    //   },
    //   (err) => {
    //     if (err.response.status == 405) {
    //       showSnackBar("You don't have sufficient rights to export meeting", "error");
    //     } else {
    //       showSnackBar("oops! There seems to be an issue, please contact support@ntaskmanager.com", "error");
    //     }
    //   });
  };
  // export all issue api function
  const handleExportAllMeetings = props => {
    let allNodes = [];
    grid.grid.forEachNodeAfterFilter(node => allNodes.push(node.data));
    const postObj = allNodes.length && { meetingIds: allNodes.map(i => i.meetingId) }

    const obj = {
      type: 'meetings',
      apiType: 'post',
      data: postObj,
      fileName: 'meetings.xlsx',
      apiEndpoint: 'api/export/bulkmeeting',
    }
    addPendingHandler(obj, dispatch);
    
    // exportBulkMeeting(postObj, dispatch,
    //   res => {
    //     fileDownload(res.data, "meetings.xlsx");
    //     showSnackBar(`${allNodes.length} meetings exported successfully`, "success");
    //   },
    //   (err) => {
    //     if (err.response.status == 405) {
    //       showSnackBar("You don't have sufficient rights to export meetings", "error");
    //     } else {
    //       showSnackBar("oops! There seems to be an issue, please contact support@ntaskmanager.com", "error");
    //     }
    //   });
  };

  const selectedSaveTime = (time, data) => {
    const object = helper.CALCULATEDURATIONDATETIME(time, data, true);
    updateMeetingObj(data, 'startTime', time)
  }
  // handle repete meeting drawer 
  const openRepeatMeetingDrawer = (event, meeting) => {
    event.stopPropagation();
    setTepeatMeetingDrawer(true);
    setRepeatMeetingId(meeting.meetingId);
  };
  const closeRepeatMeetingDrawer = () => {
    setTepeatMeetingDrawer(false);
    setRepeatMeetingId(null);
  };
  const StatusDropdownCmp = (row = { data: {} }) => {
    const rowData = row.data ? row.data : { data: {} };
    const meetingStatusData = statusData(theme, classes, intl);
    let selectedStatus = rowData
      ? meetingStatusData.find(item => item.value == rowData.status) || meetingStatusData[0]
      : meetingStatusData[0];
    return (
      <StatusDropdown
        onSelect={status => {
          handleStatusChange(status, rowData);
        }}
        buttonType={"listButton"}
        option={selectedStatus}
        options={meetingStatusData}
        toolTipTxt={selectedStatus.label}
        writeFirst={false}
        disabled={true}
        preSelection={false}
        dropdownProps={{ disablePortal: false, }}
      />
    );
  };
  const MeetingTitleCellRenderer = (row = { data: {} }) => {
    const rowData = row.data ? row.data : { data: {} };

    const isArchive = rowData.isDeleted;
    let reOccurence = rowData.meetingSchedule != null;

    return (
      <div className={classes.taskTitleCnt}>
        <div className={`${classes.taskTitleTextCnt} wrapText`} title={rowData.meetingDisplayName}>
          <span className={classes.taskTitle}>{rowData.meetingDisplayName} </span>
        </div>
        {rowData.isNew && <div style={{ minWidth: 54 }}>
          <span className={classes.newTag}>New</span>
        </div>}
        {RecurrenceRenderer(row)}
        <Stared
          isStared={rowData.isStared}
          handleCallback={isStared => handleMeetingStared(isStared, rowData)}
          diabled={rowData.isDeleted}
        />
      </div>
    );
  };

  const RecurrenceRenderer = row => {
    const rowData = row.data || {};
    return (
      rowData.meetingSchedule != null ? (
        <RecurrenceBtn
          theme={theme}
          onClick={e => !rowData.isDeleted && openRepeatMeetingDrawer(e, rowData)}
        />
      )
        : null
    );
  };
  const meetingLocationRenderer = (row = { data: {} }) => {
    const rowData = row.data ? row.data : { data: {} };
    return (
      <div className={`${classes.taskTitleTextCnt} wrapText`} title={rowData.meetingLocation}>
        <span className={classes.taskTitle}>{!isEmpty(rowData.meetingLocation) ? rowData.meetingLocation : '-'} </span>
      </div>
    );
  };
  const ProjectDropdownRenderer = row => {
    const rowData = row.data || {};
    const selectedProject = rowData.projectId
      ? [
        {
          label: rowData.projects ? rowData.projects.projectName : "",
          id: rowData.projectId,
          obj: rowData,
        },
      ]
      : [];
    let meetingPermission =
      rowData && rowData.projectId
        ? !permissionObject[rowData.projectId]
          ? workspaceMeetingPer
          : permissionObject[rowData.projectId].meeting
        : workspaceMeetingPer;
    const isArchive = rowData.isDeleted;
    return (
      <SearchDropdown
        initSelectedOption={selectedProject}
        obj={rowData}
        disabled={isArchive || !meetingPermission.meetingDetail.meetingProject.isAllowEdit || rowData.status == "Published" || rowData.status == "Cancelled"}
        // tooltip={isDisabled}
        key={rowData.projectId}
        popperProps={{ disablePortal: false }}
        tooltipText={
          <FormattedMessage
            id="task.detail-dialog.project.hint1"
            defaultMessage="You cannot change project once user effort is added to task or you don't have permission to change project"
          />
        }
        optionsList={generateProjectDropdownData(rowData)}
        singleSelect
        updateAction={handleProjectChange}
        selectedOptionHead={"Meeting Project"}
        allOptionsHead={intl.formatMessage({
          id: "project.label",
          defaultMessage: "Projects",
        })}
        buttonPlaceholder={
          <FormattedMessage
            id="task.creation-dialog.form.project.placeholder"
            defaultMessage="Select Project"
          />
        }
        buttonProps={{
          style: {
            width: "100%",
          },
        }}
        btnTextProps={{
          style: {
            width: "90%",
            overflow: "hidden",
            whiteSpace: "nowrap",
            textOverflow: "ellipsis",
          },
        }}
      />
    );
  };
  const TaskDropdownRenderer = row => {
    const rowData = row.data || {};
    let meetingPermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceMeetingPer
          : permissionObject[rowData.projectId].meeting
        : workspaceMeetingPer;
    const isArchive = rowData.isDeleted;

    const tasksData = generateTaskDropdownData(rowData);
    let selectedTask = allTasks.filter(t => {
      return rowData.taskId && rowData.taskId.indexOf(t.taskId) > -1;
    });
    selectedTask = generateTaskData(selectedTask);

    return (
      <SearchDropdown
        initSelectedOption={selectedTask}
        obj={rowData}
        disabled={isArchive || !meetingPermission.meetingDetail.meetingTask.isAllowEdit || rowData.status == "Published" || rowData.status == "Cancelled"}
        popperProps={{ disablePortal: false }}
        tooltipText={
          <FormattedMessage
            id="task.detail-dialog.project.hint1"
            defaultMessage="You cannot change project once user effort is added to task or you don't have permission to change project"
          />
        }
        optionsList={tasksData}
        singleSelect
        updateAction={(option) => handleTaskChange(option, rowData)}
        selectedOptionHead="Tasks"
        allOptionsHead={"Tasks"}
        buttonPlaceholder="Select Task"
        buttonProps={{
          style: {
            width: "100%",
          },
        }}
        btnTextProps={{
          style: {
            width: "90%",
            overflow: "hidden",
            whiteSpace: "nowrap",
            textOverflow: "ellipsis",
          },
        }}
      />
    );
  };
  const AttendeeDropdownRenderer = row => {
    const rowData = row.data || {};
    const membersObjArr = members && members.filter(m => rowData.attendeeList.includes(m.userId));
    let meetingPermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceMeetingPer
          : permissionObject[rowData.projectId].meeting
        : workspaceMeetingPer;
    const isArchive = rowData.isDeleted;
    return (
      <span data-rowClick="cell">
        <AssigneeDropdown
          popperProps={{ disablePortal: false }}
          assignedTo={membersObjArr || []}
          updateAction={handleUpdateAssignee}
          isArchivedSelected={isArchive}
          minOneSelection={true}
          obj={rowData}
          avatarSize={"xsmall"}
          buttonVariant={"small"}
          CustomRenderer={(member, iconChildren) => (
            <CustomRenderer meeting={rowData} member={member} iconChildren={iconChildren} />
          )}
          customIconButtonProps={{
            classes: {
              root: classes.iconBtnStyles,
            },
          }}
          totalAssigneeBtnProps={{
            style: {
              height: 29,
              width: 29,
              fontSize: "14px !important",
              fontWeight: theme.typography.fontWeightLarge,
            },
          }}
          customIconRenderer={<AddIcon htmlColor={theme.palette.text.dark} />}
          style={{ height: "100%" }}

          disabled={
            isArchive ||
            (!meetingPermission.meetingDetail.meetingAttendee.isAllowDelete &&
              !meetingPermission.meetingDetail.meetingAttendee.isAllowAdd) || rowData.status == "Published" || rowData.status == "Cancelled"
          }
          addPermission={meetingPermission.meetingDetail.meetingAttendee.isAllowAdd}
          deletePermission={meetingPermission.meetingDetail.meetingAttendee.isAllowDelete}
        />
      </span>
    );
  };
  const StartDateRenderer = row => {
    const rowData = row.data ? row.data : { data: {} };
    const { startDate, dueDate } = rowData;
    const meetingPermission =
      row && rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId]) ? workspaceMeetingPer : permissionObject[rowData.projectId].meeting
        : workspaceMeetingPer;
    const permission = meetingPermission.meetingDetail.meetingDate.isAllowEdit;
    const readOnly = getReadOnlyPermissions(rowData);
    const isArcheive = rowData.isDeleted;
    return !readOnly && permission ? (
      <CustomDatePicker
        date={startDate && moment(startDate)}
        dateFormat="MMM DD, YYYY"
        timeInput={false}
        onSelect={(date, time) => {
          handleDateSave(date, rowData, time);
        }}
        // selectedTime={startDate} 
        timeInputLabel={intl.formatMessage({
          id: "meeting.creation-dialog.form.start-time.label",
          defaultMessage: "Start Time",
        })}
        datePickerProps={{
          filterDate: date => {
            return dueDate
              ? moment(date).isBefore(dueDate, "day") || moment(date).isSame(dueDate, "day")
              : true;
          },
        }}
        // btnProps={{ className: classes.datePickerCustomStyle }}
        disabled={isArcheive || !rowData.startTime || !rowData.startDate || rowData.status == "Published"}
        filterDate={moment()}
        style={{ display: "flex", alignItems: "center", height: "100%" }}
        PopperProps={{ disablePortal: false }}
        btnType="transparent"
        placeholder={"Select"}
        icon={false}
      />
    ) : (
      <Typography variant="h6">
        {rowData && rowData.startDate
          ? helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(rowData.startDate)
          : " -"}
      </Typography>
    )
  };
  const StartTimeRenderer = row => {
    const rowData = row.data ? row.data : { data: {} };
    const meetingPermission =
      row && row.data && row.data.projectId
        ? isUndefined(permissionObject[row.data.projectId]) ? workspaceMeetingPer : permissionObject[row.data.projectId].meeting
        : workspaceMeetingPer;
    const permission = meetingPermission.meetingDetail.canEditMeetingTime.isAllowEdit;
    const readOnly = getReadOnlyPermissions(row.data);
    const isArcheive = rowData.isDeleted;
    return (
      <div>
        {!readOnly && permission && !isArcheive ? (
          <span data-rowClick="cell">
            <TimePicker
              isAm={true}
              selectedSaveTime={selectedSaveTime}
              data={row.data}
              view="meetingList"
              // showDateTimeDetailsPopUp={showMeetingsPopUp}
              isDateNull={
                row.data &&
                  !row.data.isDelete &&
                  (!row.data.startTime || !row.data.startTime)
                  ? true
                  : false
              }
            />
          </span>
        ) : (
          <Typography variant="h6">
            {row.data && row.data.startTime
              ? helper.RETURN_CUSTOMDATEFORMATFORTIME(row.data.startTime)
              : "-"}
          </Typography>
        )}
      </div>
    );
  };
  const CreatedByRenderer = (row = { data: {} }) => {
    const rowData = row.data ? row.data : { data: {} };
    return (
      <div className={`${classes.taskTitleTextCnt} ${classes.textCenter} wrapText`} title={rowData.createdBy}>
        <span className={classes.taskTitle}>
          {!isEmpty(rowData.createdBy) ? rowData.createdBy : "-"}{" "}
          {/* Test Thissssssss lorem ispum Test Thissssssss lorem ispum */}
        </span>
      </div>
    );
  };
  const DateRenderer = row => {
    const rowData = row.data || {};
    const formatedDate = moment(rowData.createdDate).format("MMM DD, YY");
    return formatedDate;
  };
  const UpdatedDateRenderer = row => {
    const rowData = row.data || {};
    const formatedDate = moment(rowData.updatedDate).format("MMM DD, YY");
    return formatedDate;
  };
  const UpdatedByRenderer = (row = { data: {} }) => {
    const rowData = row.data ? row.data : { data: {} };
    return (
      <div className={`${classes.taskTitleTextCnt} ${classes.textCenter} wrapText`} title={rowData.updatedBy}>
        <span className={classes.taskTitle}>
          {!isEmpty(rowData.updatedBy) ? rowData.updatedBy : "-"}{" "}
        </span>
      </div>
    );
  };
  const ColorRenderer = row => {
    const rowData = row.data || {};
    return (
      <span
        style={{ background: rowData.colorCode || "transparent" }}
        className={classes.taskColor}></span>
    );
  };
  const isExternalFilterPresent = () => {
    return true;
  };

  //Clear selection if archived view is selected
  useEffect(() => {
    if (quickFilters && quickFilters.Archived) {
      grid.grid && grid.grid.deselectAll();
    }
  }, [quickFilters]);

  useEffect(() => {
    grid.grid && grid.grid.onFilterChanged();
  }, [meetingFilter, quickFilters]);
  // update meeting agenda run time
  useEffect(() => {
    if (meetings.length && meetingDetail != '') {
      const selectedMeetingObj = meetings.find(m => meetingDetail.data.meetingId == m.meetingId);
      let obj;
      if (meetingDetail.detailsView) {
        obj = { data: selectedMeetingObj, detailsView: true };
      }
      else {
        obj = { data: selectedMeetingObj, detailsView: false };
      }
      setMeetingDetail(obj);
    }
  }, [meetings]);

  useEffect(() => {
    let searchQuery = history.location.search
    if (searchQuery) {
      let meetingId = queryString.parseUrl(searchQuery).query.meetingId;
      let meetingFound = meetings.find(item => item.meetingId == meetingId);
      if (meetingId && meetingFound) {
        let obj;
        if (meetingFound.startTime || meetingFound.startDate) {
          obj = { data: meetingFound, detailsView: true };
        } else {
          obj = { data: meetingFound, detailsView: false };
        }
        setMeetingDetail(obj);
      } else {
        this.handleExportType("Oops! Meeting not found.", "error");
        history.push("/meetings");
      }
    }
  }, [history.location.search]);

  const handleTaskSelection = meeting => {
    setSelectedMeetings(meeting);
  };

  const handleClearSelection = () => {
    setSelectedMeetings([]);
    grid.grid && grid.grid.deselectAll();
  };
  //Open meeting details on meetings row click
  const handleMeeitingRowClick = row => {
    let rowData = row.data;
    const isRowClicked = row.event.target.closest("*[data-rowClick='cell']");
    let permission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceMeetingPer
          : permissionObject[rowData.projectId].meeting
        : workspaceMeetingPer;
    if (isRowClicked) {
      return false;
    }
    if (permission.meetingDetail.cando && rowData && rowData.id && rowData.uniqueId !== "-") {
      if (!rowData.startTime || !rowData.startDate) {
        let obj = { data: rowData, detailsView: false };
        setMeetingDetail(obj)
      } else {
        let obj = { data: rowData, detailsView: true };
        setMeetingDetail(obj)
      }
    }
  };
  const MeetingActionDropdownRenderer = row => {
    const rowData = row.data || {};
    let permission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceMeetingPer
          : permissionObject[rowData.projectId].meeting
        : workspaceMeetingPer;
    return (
      <MeetingActionDropdown
        btnProps={{ style: { width: 37 } }}
        data={rowData}
        handleActivityLog={() => { }}
        handleCloseCallBack={() => { }}
        meetingPermission={permission}
        isArchived={rowData.isDeleted}
      />
    );
  };
  const getContextMenuItems = useCallback((params) => {
    const selectedNodes = grid.grid.getSelectedNodes();
    let result = [
      {
        // custom item
        name: selectedNodes.length ? 'Export Selected Meeting(s)' : 'Export Meeting',
        action: () => handleExportMeeting(params),
        // cssClasses: ['redFont', 'bold'],
      },
      {
        // custom item
        name: 'Export all Meetings',
        action: () => handleExportAllMeetings(params),
        // cssClasses: ['redFont', 'bold'],
      },
    ];
    return result;
  }, []);
  //Snackbar function
  const showSnackBar = (snackBarMessage = "", type = null) => {
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "top",
          horizontal: "centerdp",
        },
        variant: type ? type : "info",
      }
    );
  };
  const meetingTitleColumn = (meetingColumns && meetingColumns.find(c => c.columnKey == "meetingDisplayName")) || {};
  let headProps = headerProps;
  const nonSectionsFieldskeys = nonSectionFields
    .filter(
      item =>
        item.fieldType == "textarea" ||
        item.fieldType == "formula" ||
        item.fieldType == "filesAndMedia" ||
        item.fieldType == "location" ||
        (item.fieldType == "dropdown" && item.settings.multiSelect) ||
        (item.fieldType == "people" && item.settings.multiplePeople)
    )
    .map(nsf => nsf.columnKey);
  const sectionsFieldskeys = sections.reduce((res, cv) => {
    let keys = cv.fields
      .filter(
        item =>
          item.fieldType == "textarea" ||
          item.fieldType == "formula" ||
          item.fieldType == "filesAndMedia" ||
          item.fieldType == "location" ||
          (item.fieldType == "dropdown" && item.settings.multiSelect) ||
          (item.fieldType == "people" && item.settings.multiplePeople)
      )
      .map(cv => cv.columnKey);
    res = [...res, ...keys];
    return res;
  }, []);
  headProps.columnGroupingDisabled = [
    ...headProps.columnGroupingDisabled,
    ...nonSectionsFieldskeys,
    ...sectionsFieldskeys,
  ];

  const handleChangeGrouping = value => {
    let sectionGroup = localStorage.getItem("issueSectionGrouping");
    if (sectionGroup == "true") localStorage.setItem("issueSectionGrouping", value);
    else {
      localStorage.setItem("issueSectionGrouping", value);
    }
    setSectionGrouping(value);
  };
  const RenderColumnsWithoutSectionGrouping = () => {
    const systemColumns = meetingColumns
      .map((c, i) => {
        const defaultColDef = meetingColumnDefs(classes, members)[c.columnKey];
        const {
          hide,
          pinned,
          rowGroup,
          rowGroupIndex,
          sort,
          sortIndex,
          width,
          wrapText,
          autoHeight,
          position,
        } = c;
        return (
          c.columnKey !== "meetingDisplayName" && c.isSystem && (
            <AgGridColumn
              key={c.id}
              headerName={c.columnName}
              field={c.columnKey}
              hide={hide}
              suppressKeyboardEvent={true}
              pinned={pinned}
              align="left"
              rowGroup={rowGroup}
              sort={sort}
              sortIndex={sortIndex == -1 ? null : sortIndex}
              wrapText={wrapText}
              rowGroupIndex={rowGroup && parseFloat(rowGroupIndex)}
              autoHeight={autoHeight}
              width={width || (defaultColDef && defaultColDef.minWidth)}
              cellStyle={{ justifyContent: "center" }}
              position={position}
              {...defaultColDef}
            />
          )
        );
      })
      .filter(c => c !== false);
    const nonSectionColumns = nonSectionFields.map((c, i) => {
      const defaultColDef = meetingColumnDefs(classes, members)[c.fieldType];
      const { hide, pinned, rowGroup, sort, sortIndex, width, wrapText, autoHeight, position } = c;
      return (
        <AgGridColumn
          key={c.id}
          headerName={c.columnName}
          field={c.columnKey}
          hide={hide}
          pinned={pinned}
          align="left"
          fieldType={c.fieldType}
          rowGroup={rowGroup}
          suppressKeyboardEvent={true}
          sort={sort}
          sortIndex={sortIndex == -1 ? null : sortIndex}
          wrapText={wrapText}
          autoHeight={autoHeight}
          fieldId={c.fieldId}
          position={position}
          width={width || (defaultColDef && defaultColDef.minWidth)}
          cellStyle={{
            justifyContent:
              c.fieldType === "dropdown" || c.fieldType === "filesAndMedia"
                ? "flex-start"
                : "center",
          }}
          {...defaultColDef}
        />
      );
    });
    const sectionColumns = sections
      .map(section => {
        return (
          section.fields.length > 0 &&
          section.fields.map((c, i) => {
            const defaultColDef = meetingColumnDefs(classes, members)[c.fieldType];
            const {
              hide,
              pinned,
              rowGroup,
              sort,
              sortIndex,
              width,
              wrapText,
              autoHeight,
              position,
            } = c;
            return (
              <AgGridColumn
                key={c.id}
                headerName={c.columnName}
                field={c.columnKey}
                hide={hide}
                pinned={pinned}
                align="left"
                suppressKeyboardEvent={true}
                position={position}
                rowGroup={rowGroup}
                fieldType={c.fieldType}
                sort={sort}
                sortIndex={sortIndex == -1 ? null : sortIndex}
                wrapText={wrapText}
                autoHeight={autoHeight}
                width={width || (defaultColDef && defaultColDef.minWidth)}
                fieldId={c.fieldId}
                cellStyle={{
                  justifyContent:
                    c.fieldType === "dropdown" || c.fieldType === "filesAndMedia"
                      ? "flex-start"
                      : "center",
                }}
                {...defaultColDef}
              />
            );
          })
        );
      })
      .filter(c => c !== false);
    const spreadSectionCols = sectionColumns.reduce((r, cv) => {
      r = [...r, ...cv];
      return r;
    }, []);
    const allColumns = [...systemColumns, ...nonSectionColumns, ...spreadSectionCols];
    const sortedColumns = allColumns.sort((pv, cv) => {
      return pv.props.position - cv.props.position;
    });
    return sortedColumns;
  };

  // meeting detail dialog
  const isUpdated = data => {
    // updateMeeting(data, (err, data) => {
    //   if (this.props.meetingState.indexOf("Archived") >= 0)
    //     this.setState({ isArchived: true, isInstanceChange: false });
    // });
  }
  const handleCloseMeeting = () => {
    setMeetingDetail('')
  };
  const closeDateTimeDetailsPopUp = () => {
    setMeetingDetail('')
  }
  return (
    <>
      {selectedMeetings.length ? (
        <Meetingcmp selectedMeetings={selectedMeetings} clearSelection={handleClearSelection} />
      ) : null}
      {repeatMeetingId && repeatMeetingDrawer && (
        <SideDrawer
          closeAction={closeRepeatMeetingDrawer}
          open={repeatMeetingDrawer}
          repeatMeetingId={repeatMeetingId}
          meetingPer={workspaceMeetingPer}
          allMeetings={meetings}
        />
      )}
      {meetings.length == 0 ? (
        <div className={classes.emptyContainer} style={style}>
          <EmptyState
            screenType="meeting"
            heading={
              <FormattedMessage
                id="common.create-first.meeting.label"
                defaultMessage="Create your first meeting"
              />
            }
            message={
              <FormattedMessage
                id="common.create-first.meeting.messagea"
                defaultMessage='You do not have any meeting yet. Press "Alt + M" or click on button below.'
              />
            }
            button={workspaceMeetingPer.createMeeting.cando}
          />
        </div>
      ) : (
        <div className={classes.taskListViewCnt} style={style}>
          <CustomTable
            columns={state.meetingColumns}
            exportCallback={handleExportMeeting}
            defaultColDef={{ lockPinned: true }}
            isExternalFilterPresent={isExternalFilterPresent}
            doesExternalFilterPass={doesFilterPass}
            onSelectionChanged={handleTaskSelection}
            idKey={'meetingId'}
            frameworkComponents={{
              MeetingActionCellRenderer: MeetingActionDropdownRenderer,
              meetingTitleCmp: MeetingTitleCellRenderer,
              meetingLocationCmp: meetingLocationRenderer,
              statusDropdown: StatusDropdownCmp,
              meetingProject: ProjectDropdownRenderer,
              attendee: AttendeeDropdownRenderer,
              createdByRenderer: CreatedByRenderer,
              updatedByRenderer: UpdatedByRenderer,
              createdDateRenderer: DateRenderer,
              updatedDateCmp: UpdatedDateRenderer,
              color: ColorRenderer,
              startDate: StartDateRenderer,
              startTime: StartTimeRenderer,
              taskRenderer: TaskDropdownRenderer,
              groupRowInnerRenderer: GroupRowInnerRenderer,
              // recurrence: RecurrenceRenderer,
            }}
            data={meetings}
            selectedTasks={selectedMeetings}
            type="meeting"
            onRowGroupChange={(obj, key) => updateMeeting(obj, { [key]: obj[key] })}
            createableProps={{
              placeholder: "Enter title for new meeting",
              id: "quickAddMeeting",
              btnText: "Add new Meeting",
              addAction: handleAddMeeting,
              addHelpTask: "Press enter to add new Meeting",
              emptyErrorMessage: 'Please Enter Meeting title',
              disabled: !workspaceMeetingPer.createMeeting.cando
            }}
            headerProps={headProps}
            gridProps={{
              getContextMenuItems: getContextMenuItems,
              groupDisplayType: "groupRows",
              onRowClicked: handleMeeitingRowClick,
              reactUi: true,
              // groupIncludeFooter: true,
              // groupIncludeTotalFooter: true,
              maintainColumnOrder: true,
              // autoGroupColumnDef: { minWidth: 300 },
            }}>
            {/*<AgGridColumn headerClass={classes.taskTitleField} >*/}
            <AgGridColumn
              headerName=""
              field="colorCode"
              cellRenderer="color"
              filter={false}
              cellClass={classes.taskColorCell}
              maxWidth={6}
              resizeable={false}
              pinned="left"
              lockPosition={true}
              suppressMovable={true}
            />
            <AgGridColumn
              headerName={meetingTitleColumn.columnName}
              field={meetingTitleColumn.columnKey}
              resizable={true}
              suppressMovable={true}
              checkboxSelection={teamCanView("bulkActionAccess")}
              // rowDrag={false}
              sortable={true}
              filter={true}
              lockPosition={true}
              wrapText={meetingTitleColumn.wrapText}
              cellRenderer={"meetingTitleCmp"}
              cellClass={`${classes.taskTitleCell} ag-textAlignLeft`}
              pinnedRowCellRenderer="customPinnedRowRenderer"
              pinned={"left"}
              autoHeight={meetingTitleColumn.autoHeight}
              width={meetingTitleColumn.width || 300}
              minWidth={200}
              align="left"
              sort={meetingTitleColumn.sort}
              sortIndex={meetingTitleColumn.sortIndex == -1 ? null : meetingTitleColumn.sortIndex}
              cellStyle={{ padding: 0 }}
              rowDrag={true}
              comparator={sortAlphabetically}
            />

            <AgGridColumn
              headerName=""
              field="recurrence"
              suppressMovable={true}
              cellRenderer="recurrence"
              cellClass={classes.taskRecurrenceCell}
              filter={false}
              minWidth={22}
              maxWidth={22}
              pinned={"left"}
            />
            {!sectionGroup || sectionGroup == "false"
              ? RenderColumnsWithoutSectionGrouping()
              : null}

            {sectionGroup == "true" && (
              <AgGridColumn
                visible={false}
                headerName="System Fields"
                headerClass={classes.systemFieldGroup}>
                {meetingColumns.map((c, i) => {
                  const defaultColDef = meetingColumnDefs(classes, members)[c.columnKey];
                  const {
                    hide,
                    pinned,
                    rowGroup,
                    rowGroupIndex,
                    sort,
                    sortIndex,
                    width,
                    wrapText,
                    autoHeight,
                  } = c;
                  return (
                    c.columnKey !== "meetingDisplayName" &&
                    isNull(c.fieldType) &&
                    isNull(c.fieldId) && (
                      <AgGridColumn
                        key={c.id}
                        headerName={c.columnName}
                        field={c.columnKey}
                        hide={hide}
                        pinned={pinned}
                        align="left"
                        rowGroup={rowGroup}
                        sort={sort}
                        sortIndex={sortIndex == -1 ? null : sortIndex}
                        wrapText={wrapText}
                        rowGroupIndex={rowGroup && parseFloat(rowGroupIndex)}
                        autoHeight={autoHeight}
                        width={width || (defaultColDef && defaultColDef.minWidth)}
                        cellStyle={{ justifyContent: "center" }}
                        {...defaultColDef}
                      />
                    )
                  );
                })}
              </AgGridColumn>
            )}
            <AgGridColumn
              headerName={""}
              field={"columnDropdown"}
              resizable={false}
              sortable={false}
              lockPinned={true}
              filter={false}
              width={40}
              headerClass={"columnSelectHeader"}
              pinned={"right"}
              suppressMovable={true}
              cellRenderer={"MeetingActionCellRenderer"}
              cellStyle={{ padding: 0, textAlign: "center" }}
            />
          </CustomTable>
          <MeetingFilter
            sectionGrouping={sectionGrouping}
            handleChangeGrouping={handleChangeGrouping}
          />


          {meetingDetail &&
            <DefaultDialog
              title={
                <FormattedMessage
                  id="meeting.creation-dialog.title"
                  defaultMessage="Add Meeting Details"></FormattedMessage>
              }
              open={!meetingDetail.detailsView ? true : false}
              onClose={closeDateTimeDetailsPopUp}>
              <AddMeetingForm
                handleCloseMeeting={closeDateTimeDetailsPopUp}
                isNew={true}
                isDialog={true}
                isAddMeetingDetails={true}
                currentMeeting={meetingDetail.data}
                maxLength={250}
              />
            </DefaultDialog>
          }
          {meetingDetail.detailsView ?
            <MeetingDetails
              isUpdated={isUpdated}
              closeMeetingDetailsPopUp={handleCloseMeeting}
              currentMeeting={meetingDetail.data}
              listView={true}
              isArchivedSelected={quickFilters && quickFilters.Archived}
              isInstanceChange={false}
              filterMeeting={[]}
              UnArchiveMeeting={() => { }}
              updateMeetingAttendee={() => { }}
            /> : null}
        </div>
      )}

      {/*<Footer*/}
      {/*  total={tasks && tasks.length}*/}
      {/*  display={true}*/}
      {/*  type="Task"*/}
      {/*  // quote={this.props.quote}*/}
      {/*/>*/}
    </>
  );
}, areEqual);

function areEqual(prevProps, nextProps) {
  return isEqual(prevProps, nextProps);
  /*
  return true if passing nextProps to render would return
  the same result as passing prevProps to render,
  otherwise return false
  */
}

const GroupRowInnerRenderer = props => {
  return <GroupByComponents data={props} feature='meeting' />;
};

export default compose(
  withRouter,
  injectIntl,
  withSnackbar,
  withStyles(meetingListStyles, { withTheme: true })
)(MeetingList);
