import React from "react"
import getMeetingByStatusType from "../../../helper/getMeetingByStatusType";
import getMeetingByLocationType from "../../../helper/getMeetingByLocationType";
import getMeetingByTaskType from "../../../helper/getMeetingByTaskType";
import Typography from "@material-ui/core/Typography";
function getTranslatedId(name) {
  switch(name) {
     case "Upcoming":
      name = "meeting.status.upcoming";
       break;
       case "In Review":
        name = "meeting.status.in-review";
        break;
        case "Published":
          name = "meeting.status.publish";
          break;
          case "Cancelled":
            name = "meeting.status.cancel";
            break;
            case "Overdue":
              name = "meeting.status.overdue";
              break;
              case "Today":
                name = "meeting.status.today";
                break;
                case "This Week":
                  name = "meeting.status.week";
                  break;
                  case "This Month":
                    name = "meeting.status.month";
                    break;
                    case "null":
                      name = "common.others.label";
                      break;
                      case "undefined":
                      name = "common.others.label";
                      break;
  }
  return name;
}
export function GroupingRowRenderer(param, meetings, classes, intl=null) { // Grouping row renderer
  const { name, columnGroupName } = param;
  let meetingCount;
  let value;
  switch (columnGroupName) { // Switch statement used to calculate countes of the meetings according to the grouping selected
    case ('status'):
      meetingCount = getMeetingByStatusType(meetings, name) //Function to return meetings based on priority
      value = intl != null ? intl.formatMessage({id: getTranslatedId(name), defaultMessage: name }) : name;
      break;
    case ('location'):
      meetingCount = getMeetingByLocationType(meetings, name) //Function to return meetings based on priority
      value = intl != null ? intl.formatMessage({id: getTranslatedId(name), defaultMessage: name }) : name;
      break;
    case ('taskTitle'): {
      meetingCount = getMeetingByTaskType(meetings, name); //Function to return meetings based on issue project
      value = intl != null ? intl.formatMessage({id: getTranslatedId(name), defaultMessage: name }) : name;
    }
    default:
      break;
  }
  return <div className={classes.groupingRow} {...param.renderer}>
    <div className={classes.groupingRowTextCnt}>
      <Typography variant="body1" className={classes.groupingRowText}>{value == "null" || value == "undefined" || !value ? "Others" : value}</Typography>
      <span className={classes.groupingCount}>{meetingCount && meetingCount > 999 ? "999+" : meetingCount ? meetingCount.length : null}</span>
    </div>
  </div>
}