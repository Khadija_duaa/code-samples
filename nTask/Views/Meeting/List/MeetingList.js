import React, { Component, Fragment, useState } from "react";
import ReactDataGrid from "react-data-grid";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import BulkActions from "./BulkActions";
import AddMeeting from "./AddMeeting";
import Hotkeys from "react-hot-keys";
import cloneDeep from "lodash/cloneDeep";
import { columns } from "../constants";
import Grid from "@material-ui/core/Grid";
import listStyles from "./meetingList.style";
import AddIcon from "@material-ui/icons/Add";
import CancelIcon from "@material-ui/icons/Cancel";
import IconButton from "../../../components/Buttons/IconButton";
import TableActionDropDown from "./ActionDropDown";
import helper from "../../../helper";
import MeetingDetails from "../MeetingDetails/MeetingDetails";
import TimePicker from "../../../components/DatePicker/TimePicker";
import StarredCheckBox from "../../../components/Form/Starred";
import DefaultDialog from "../../../components/Dialog/Dialog";
import AddMeetingForm from "../../AddNewForms/AddMeetingForm";
import { calculateContentHeight } from "../../../utils/common";
import find from "lodash/find";
import queryString from "query-string";
import AssigneeDropdown from "../../../components/Dropdown/AssigneeDropdown";
import SearchDropdown from "../../../components/Dropdown/SearchDropdown";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";
import SideDrawer from "../Drawer/Drawer";
import { headerTranslations } from "../../../utils/headerTranslations";
import {TRIALPERIOD} from '../../constants/planConstant';

import {
  UpdateTask,
  FetchTasksInfo,
  CopyTask,
  DeleteTask,
  ArchiveTask,
  UnArchiveTask,
} from "../../../redux/actions/tasks";
import { SaveItemOrder } from "../../../redux/actions/itemOrder";
import { UpdateCalenderTask } from "../../../redux/actions/calenderTasks";
import { sortListData, getSortOrder } from "../../../helper/sortListData";
import {
  UpdateMeeting,
  CreateInLineMeeting,
  BulkUpdateMeeting,
  DeleteMeeting,
  ArchiveMeeting,
  UnArchiveMeeting,
  DeleteMeetingSchedule,
  ArchiveMeetingSchedule,
  UnArchiveMeetingSchedule,
  CancelMeeting,
  CancelMeetingSchedule,
  MarkMeetingAsStarted,
} from "../../../redux/actions/meetings";
import StaticDatePicker from "../../../components/DatePicker/StaticDatePicker";
import MeetingActionDropdown from "./MeetingActionDropDown";
import Typography from "@material-ui/core/Typography";
import MenuList from "../../../components/Menu/TaskMenus/MemberListMenu";
import EmptyState from "../../../components/EmptyStates/EmptyState";
import { getMeetingsPermissions, getReadOnlyPermissions, canDo } from "../permissions";
import SvgIcon from "@material-ui/core/SvgIcon";
import RecurrenceIcon from "../../../components/Icons/RecurrenceIcon";
import UnPlanned from "../../billing/UnPlanned/UnPlanned";
import Modal from "@material-ui/core/Modal";
import { GroupingRowRenderer } from "./GroupingRowRenderer";
import { Data } from "react-data-grid-addons";
import { FormattedMessage, injectIntl } from "react-intl";
import isEqual from "lodash/isEqual";
import ColumnSelectionDropdown from "../../../components/Dropdown/SelectedItemsDropDown";
import RemoveIcon from "@material-ui/icons/Close";
import { generateTaskData, generateProjectData } from "../../../helper/generateSelectData";
import ProjectPermissionSelector from "../../../redux/selectors/projectPermissionSelector";
import isUndefined from "lodash/isUndefined";
import {TRIALPERIOD} from '../../../components/constants/planConstant';

const {
  id_C,
  meetingTitle_C,
  meetingStatus_C,
  taskTitle_C,
  startDate_C,
  startTime_C,
  location_C,
  projectName,
  attendee_C,
  optionList_C,
  duration_C,
} = columns;

const {
  Draggable: { Container: DraggableContainer, RowActionsCell, DropTargetRowContainer },
  Data: { Selectors },
  Toolbar,
} = require("react-data-grid-addons");

const RowRenderer = DropTargetRowContainer(ReactDataGrid.Row);

let increment = 20;
const CustomRenderer = ({ row, member, iconChildren }) => {
  if (row) {
    let assigneeLists = row.createdBy.indexOf(member.userId) >= 0;
    if (assigneeLists) return <> </>;
    else return iconChildren;
  }
};

class MeetingList extends React.Component {
  static defaultProps = { rowKey: "meetingId" };
  constructor(props, context) {
    super(props, context);
    this.state = {
      repeatMeetingDrawer: false,
      repeatMeetingId: null,
      _columns: [],
      rows: [],
      output: "",
      selectedIds: [],
      addMeeting: false,
      addNewForm: false,
      completeDetails: [],
      userId: "",
      value: "",
      progress: 0,
      issueId: "",
      isProgress: false,
      TaskError: false,
      TaskErrorMessage: "",
      isReorder: false,
      startDateOpen: false,
      dueDateOpen: false,
      newCol: [],
      showMeetings: false,
      currentMeeting: {},
      detailsMeetingId: "",
      loggedInTeam: "",
      isWorkspaceChanged: false,
      showRecords: increment,
      totalRecords: 0,
      members: [],
      saveDate: new Date(),
      saveTime: "12:00 AM",
      isArchived: false,
      meetingDialog: false,
      meetingDateTimeDialog: false,
      instantData: [],
      isInstanceChange: false,
      sortColumn: null,
      sortDirection: "NONE",
      showUnplanned: false,
      selectedMeetingObj: [],
    };
    this.addMeeting = this.addMeeting.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.addMeetingInput = React.createRef();
    this.onKeyDown = this.onKeyDown.bind(this);
    this.cancelAddMeeting = this.cancelAddMeeting.bind(this);
    this.isUpdated = this.isUpdated.bind(this);
    this.showMeetingsPopUp = this.showMeetingsPopUp.bind(this);
    this.closeMeetingsPopUp = this.closeMeetingsPopUp.bind(this);
    this.handleScrollDown = this.handleScrollDown.bind(this);
    this.scrollListener = this.scrollListener.bind(this);
    this.BulkUpdate = this.BulkUpdate.bind(this);
    this.selectedSaveDate = this.selectedSaveDate.bind(this);
    this.selectedSaveTime = this.selectedSaveTime.bind(this);
    this.closeMeetingDetailsPopUp = this.closeMeetingDetailsPopUp.bind(this);
    this.closeActionMenu = this.closeActionMenu.bind(this);
    this.handleDialog = this.handleDialog.bind(this);
    this.handleDialogClose = this.handleDialogClose.bind(this);
    this.showDateTimeDetailsPopUp = this.showDateTimeDetailsPopUp.bind(this);
    this.closeDateTimeDetailsPopUp = this.closeDateTimeDetailsPopUp.bind(this);
  }
  getTranslatedId = name => {
    var trans = { id: "", message: "" };
    switch (name) {
      case "Upcoming":
        trans = { id: "meeting.status.upcoming", message: name };
        break;
      case "In Review":
        trans = { id: "meeting.status.in-review", message: name };
        break;
      case "Published":
        trans = { id: "meeting.status.publish", message: name };
        break;
      case "Cancelled":
        trans = { id: "meeting.status.cancel", message: name };
        break;
      case "In Review":
        trans = { id: "meeting.status.in-review", message: name };
        break;
      case "Published":
        trans = { id: "meeting.status.publish", message: name };
        break;
      case "Cancelled":
        trans = { id: "meeting.status.cancel", message: name };
        break;
      case "OverDue":
        trans = { id: "meeting.status.overdue", message: name };
        break;
      case "OverDue":
        trans = { id: "meeting.status.overdue", message: name };
    }
  };
  getSelectedProject = meeting => {
    const { projects } = this.props;
    const selectedProject = projects.find(project => {
      return project.projectId == meeting.projectId;
    });

    return selectedProject
      ? [
          {
            label: selectedProject.projectName,
            id: selectedProject.projectId,
            obj: meeting,
          },
        ]
      : [];
  };
  // Generate list of all projects for dropdown understandable form
  generateProjectDropdownData = meeting => {
    const { projects } = this.props;

    let selectedProject = meeting.projectId
      ? projects.filter(t => t.projectId != meeting.projectId)
      : projects;
    selectedProject = selectedProject.length ? selectedProject : projects;

    return generateProjectData(selectedProject.filter(p => p.projectId !== meeting.projectId));
  };

  generateSelectedValue = meeting => {
    /** function for generating the selected task list objects from task id and return taskList for showing pre populated selected task if any => function call in componentDidMount */

    const selectedTaskList = this.props.tasks.filter(task => {
      return meeting.taskId == task.taskId;
    });
    let taskList = generateTaskData(selectedTaskList);
    return taskList;
  };

  handleSelectChange = (newValue, project, meeting) => {
    const projectId = newValue.length ? newValue[0].id : "";
    let selectedTasks = this.generateSelectedValue(meeting);

    let linkTask = selectedTasks.length
      ? selectedTasks[0].obj.projectId == "" || !selectedTasks[0].obj.projectId
        ? []
        : meeting.taskId
      : meeting.taskId;

    let newMeetingObj = {
      ...meeting,
      projectId: projectId,
      taskId: "",
    };
    this.props.UpdateMeeting(newMeetingObj, () => {});
  };

  getTranslatedId = name => {
    var trans = { id: "", message: "" };
    switch (name) {
      case "Upcoming":
        trans = { id: "meeting.status.upcoming", message: name };
        break;
    }
    return trans.id != ""
      ? this.props.intl.formatMessage({ id: trans.id, defaultMessage: trans.message })
      : name;
  };
  onColumnResize = (columnIndex, size) => {
    const { _columns } = this.state;
    if (size < 0) return;
    let columnWidth = JSON.parse(localStorage.getItem("columnWidthMeeting"));
    if (columnWidth) {
      columnWidth[_columns[columnIndex - 1].key] = {
        width: size,
      };
      localStorage.setItem("columnWidthMeeting", JSON.stringify(columnWidth));
    } else {
      let obj = {};
      obj[_columns[columnIndex - 1].key] = {
        width: size,
      };
      localStorage.setItem("columnWidthMeeting", JSON.stringify(obj));
    }
  };
  getColumns = () => {
    let columnWidth = JSON.parse(localStorage.getItem("columnWidthMeeting"));

    let columns = [
      {
        key: id_C.key,
        name: id_C.name,
        width: columnWidth && columnWidth[id_C.key] ? columnWidth[id_C.key].width : 150,
        cellClass: "firstColoumn",
        resizable: true,
        formatter: value => {
          return (
            <div
              className={this.props.classes.taskListTitleTextCnt}
              style={{
                borderLeft: `6px solid ${value.row ? value.row.colorCode : ""}`,
              }}>
              <span className={this.props.classes.taskListTitleText}>
                {value.row ? value.row.uniqueId : ""}
              </span>
            </div>
          );
        },
        columnOrder: 0,
        sortable: teamCanView("advanceSortAccess") ? false : true,
      },
      {
        key: meetingTitle_C.key,
        name: meetingTitle_C.name,
        cellClass: "secondColoumn",
        visible: true,
        width:
          columnWidth && columnWidth[meetingTitle_C.key]
            ? columnWidth[meetingTitle_C.key].width
            : 400,
        sortDescendingFirst: true,
        resizable: true,
        formatter: value => {
          return (
            <>
              <div style={{ display: "flex", alignItems: "center" }}>
                {!this.props.isArchivedSelected ? (
                  <StarredCheckBox
                    checkboxStyles={{ padding: 0 }}
                    // styles={{ position: "absolute", left: -25 }}
                    checked={value.row ? value.row.isStared : false}
                    onChange={e => this.handleStaredChange(e, value.row)}
                  />
                ) : null}

                <span
                  title={value.row ? value.row.meetingDisplayName : ""}
                  className={this.props.classes.taskListTitleText}>
                  {!this.props.isArchivedSelected && value.row && value.row.meetingSchedule && (
                    <IconButton
                      btnType="transparent"
                      onClick={event => this.openRepeatMeetingDrawer(event, value.row)}
                      style={{ padding: 15, marginRight: 2 }}>
                      <SvgIcon
                        viewBox="0 0 14 12.438"
                        htmlColor={this.props.theme.palette.primary.light}
                        className={this.props.classes.recurrenceIcon}>
                        <RecurrenceIcon />
                      </SvgIcon>
                    </IconButton>
                  )}
                  {value.row ? value.row.meetingDisplayName : ""}
                </span>
              </div>
            </>
          );
        },
        columnOrder: 1,
        sortable: teamCanView("advanceSortAccess") ? false : true,
      },
      {
        key: meetingStatus_C.key,
        name: meetingStatus_C.name,
        width:
          columnWidth && columnWidth[meetingStatus_C.key]
            ? columnWidth[meetingStatus_C.key].width
            : 130,
        resizable: true,
        formatter: value => {
          const { theme, classes } = this.props;
          let meetingStatusColor = {
            Upcoming: theme.palette.meetingStatus.Upcoming,
            Cancelled: theme.palette.meetingStatus.Cancelled,
            OverDue: theme.palette.meetingStatus.OverDue,
            InReview: theme.palette.meetingStatus.InReview,
            Published: theme.palette.meetingStatus.Published,
          };
          return (
            <span
              className={classes.meetingStatus}
              style={{
                color: value.row && value.row.status ? meetingStatusColor[value.row.status] : "",
                border:
                  value.row && value.row.status
                    ? `1px solid ${meetingStatusColor[value.row.status]}`
                    : "",
              }}>
              {value.row && value.row.status
                ? value.row.status.toUpperCase() === "INREVIEW"
                  ? this.getTranslatedId("In Review").toUpperCase()
                  : this.getTranslatedId(value.row.status).toUpperCase()
                : ""}
            </span>
          );
        },
        cellStyle: { background: "red" },
        cellClass: "dropDownCell",
        columnOrder: 2,
        sortable: teamCanView("advanceSortAccess") ? false : true,
      },
      {
        key: projectName.key,
        name: projectName.name,
        resizable: true,
        width: 240,
        visible: teamCanView("projectAccess"),
        formatter: value => {
          const meetingPermission =
            value && value.row && value.row.projectId
              ? isUndefined(this.props.permissionObject[value.row.projectId])
                ? this.props.workspaceMeetingPer
                : this.props.permissionObject[value.row.projectId].meeting
              : this.props.workspaceMeetingPer;
          const meeting = value.row ? value.row : [];
          // some times task effort comes in string and some times empty array so to check that below line of code is added
          const canEditProject = meetingPermission.meetingDetail.meetingProject.isAllowEdit;
          return (
            <SearchDropdown
              initSelectedOption={meeting.projectId ? this.getSelectedProject(meeting) : []}
              obj={meeting}
              disableDropdown={!canEditProject || this.props.isArchivedSelected}
              key={meeting.projectId}
              tooltipText={
                <FormattedMessage
                  id="task.detail-dialog.project.hint1"
                  defaultMessage="You cannot change project once user effort is added to task or you don't have permission to change project"
                />
              }
              optionsList={this.generateProjectDropdownData(meeting)}
              singleSelect
              updateAction={(newValue, project) =>
                this.handleSelectChange(newValue, project, meeting)
              }
              selectedOptionHead={this.props.intl.formatMessage({
                id: "common.task-project.label",
                defaultMessage: "Task Project",
              })}
              allOptionsHead={this.props.intl.formatMessage({
                id: "project.label",
                defaultMessage: "Projects",
              })}
              buttonPlaceholder={
                <FormattedMessage
                  id="task.creation-dialog.form.project.placeholder"
                  defaultMessage="Select Project"
                />
              }
              buttonProps={{
                disabled: !canEditProject || this.props.isArchivedSelected,
              }}
              IssueView={true}
            />
          );
        },
        cellClass: "dropDownCell",
        columnOrder: 6,

        sortable: !teamCanView("advanceSortAccess"),
      },
      {
        key: taskTitle_C.key,
        name: taskTitle_C.name,
        width:
          columnWidth && columnWidth[taskTitle_C.key] ? columnWidth[taskTitle_C.key].width : 240,
        resizable: true,
        formatter: value => {
          const meetingPermission =
            value && value.row && value.row.projectId
              ? isUndefined(this.props.permissionObject[value.row.projectId])
                ? this.props.workspaceMeetingPer
                : this.props.permissionObject[value.row.projectId].meeting
              : this.props.workspaceMeetingPer;
          const meetingTaskPermission = meetingPermission.meetingDetail.meetingTask.isAllowEdit;
          const readOnly = getReadOnlyPermissions(value.row);
          let meeting = value.row ? value.row : [];
          return (
            <>
              {!readOnly && meetingTaskPermission ? (
                <SearchDropdown
                  initSelectedOption={this.getSelectedTask(meeting)}
                  buttonProps={{
                    disabled: this.props.isArchivedSelected,
                  }}
                  obj={meeting}
                  optionsList={this.generateTaskDropdownData(meeting)}
                  singleSelect={true}
                  updateAction={(selectedTasks, task) =>
                    this.updateTask(selectedTasks, task, meeting)
                  }
                  selectedOptionHead="Meeting Task"
                  allOptionsHead={<FormattedMessage id="task.label" defaultMessage="Tasks" />}
                  buttonPlaceholder={
                    <FormattedMessage
                      id="common.bulk-action.selectTask"
                      defaultMessage="Select Task"
                    />
                  }
                  //  isMulti={true}
                  multipleOptionsPlaceHolder="Multiple Tasks"
                />
              ) : (
                <div className={this.props.classes.taskTitle}>
                  <Typography variant="body2" align="center">
                    {" "}
                    {value.row.taskTitle}{" "}
                  </Typography>
                </div>
              )}
            </>
          );
        },
        cellClass: "dropDownCell",
        columnOrder: 3,
      },
      {
        key: startDate_C.key,
        name: startDate_C.name,
        width:
          columnWidth && columnWidth[startDate_C.key] ? columnWidth[startDate_C.key].width : 150,
        resizable: true,
        formatter: value => {
          const meetingPermission =
            value && value.row && value.row.projectId
              ? isUndefined(this.props.permissionObject[value.row.projectId])
                ? this.props.workspaceMeetingPer
                : this.props.permissionObject[value.row.projectId].meeting
              : this.props.workspaceMeetingPer;
          const permission = meetingPermission.meetingDetail.meetingDate.isAllowEdit;
          const readOnly = getReadOnlyPermissions(value.row);

          return (
            <div>
              {!readOnly && permission ? (
                <StaticDatePicker
                  selectedSaveDate={this.selectedSaveDate}
                  data={value.row}
                  view="meetingList"
                  showDateTimeDetailsPopUp={this.showMeetingsPopUp}
                  isDateNull={
                    value.row &&
                    !value.row.isDelete &&
                    (!value.row.startTime || !value.row.startDateString)
                      ? true
                      : false
                  }
                />
              ) : (
                // <></>
                <Typography variant="h6">
                  {value.row && value.row.startDateString
                    ? helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(value.row.startDateString)
                    : " -"}
                </Typography>
              )}
            </div>
          );
        },
        cellClass: "dropDownCell",
        columnOrder: 4,
        sortable: teamCanView("advanceSortAccess") ? false : true,
      },

      {
        key: startTime_C.key,
        name: startTime_C.name,
        width:
          columnWidth && columnWidth[startTime_C.key] ? columnWidth[startTime_C.key].width : 125,
        resizable: true,
        formatter: value => {
          const meetingPermission =
            value && value.row && value.row.projectId
              ? isUndefined(this.props.permissionObject[value.row.projectId])
                ? this.props.workspaceMeetingPer
                : this.props.permissionObject[value.row.projectId].meeting
              : this.props.workspaceMeetingPer;
          const permission = meetingPermission.meetingDetail.canEditMeetingTime.isAllowEdit;
          const readOnly = getReadOnlyPermissions(value.row);
          return (
            <div>
              {!readOnly && permission ? (
                <TimePicker
                  isAm={true}
                  selectedSaveTime={this.selectedSaveTime}
                  data={value.row}
                  view="meetingList"
                  showDateTimeDetailsPopUp={this.showMeetingsPopUp}
                  isDateNull={
                    value.row &&
                    !value.row.isDelete &&
                    (!value.row.startTime || !value.row.startDateString)
                      ? true
                      : false
                  }
                />
              ) : (
                <Typography variant="h6">
                  {value.row && value.row.startDateString
                    ? helper.RETURN_CUSTOMDATEFORMATFORTIME(value.row.startDateString)
                    : " -"}
                </Typography>
              )}
            </div>
          );
        },
        cellClass: "dropDownCell",
        columnOrder: 5,
        sortable: teamCanView("advanceSortAccess") ? false : true,
      },
      {
        key: location_C.key,
        name: location_C.name,
        width: columnWidth && columnWidth[location_C.key] ? columnWidth[location_C.key].width : 240,
        resizable: true,
        formatter: value => {
          return (
            <Typography variant="h6">
              {value.row && value.row.location
                ? value.row.zoomMeetingId
                  ? "@Zoom"
                  : value.row.location
                : " - "}
            </Typography>
            // <PiriorityMenu
            // // priority={value.row && value.row.priority ? value.row.priority : 1}
            // // MenuData={value.row}
            // // searchFilterApplied={this.props.searchFilterApplied}
            // // isUpdated={this.isUpdated}
            // />
          );
        },
        cellClass: "dropDownCell",
        columnOrder: 6,
        sortable: teamCanView("advanceSortAccess") ? false : true,
      },
      {
        key: attendee_C.key,
        name: attendee_C.name,
        width: columnWidth && columnWidth[attendee_C.key] ? columnWidth[attendee_C.key].width : 240,
        resizable: true,
        formatter: value => {
          const meetingPermission =
            value && value.row && value.row.projectId
              ? isUndefined(this.props.permissionObject[value.row.projectId])
                ? this.props.workspaceMeetingPer
                : this.props.permissionObject[value.row.projectId].meeting
              : this.props.workspaceMeetingPer;
          const permission = meetingPermission.meetingDetail.meetingAttendee.isAllowAdd;
          return (
            <AssigneeDropdown
              assignedTo={value.row ? value.row.assigneeLists : []}
              updateAction={this.updateMeetingAttendee}
              isArchivedSelected={this.props.isArchivedSelected}
              obj={value.row}
              assigneeHeading="Participants"
              minOneSelection={true}
              disabled={value.row.status == "Published" || !permission ? true : null}
              deletePermission={meetingPermission.meetingDetail.meetingAttendee.isAllowDelete}
              CustomRenderer={(member, iconChildren) => (
                <CustomRenderer row={value.row} member={member} iconChildren={iconChildren} />
              )}
            />
            // <MenuList
            //   key={value.row ? value.row.meetingId : ""}
            //   assigneeList={value.row ? value.row.assigneeLists : []}
            //   MenuData={value.row}
            //   placementType="bottom-end"
            //   isUpdated={this.isUpdated}
            //   viewType="meeting"
            //   checkAssignee={true}
            //   checkedType={"multi"}
            //   minimumSelections={1}
            //   listType="assignee"
            //   showListOnly={
            //     readOnly || (permission && !permission.meetingAttendee.edit)
            //   }
            //   showDateTimeDetailsPopUp={this.showDateTimeDetailsPopUp}
            //   isArchivedSelected={this.props.isArchivedSelected}
            // />
          );
        },
        cellClass: "dropDownCell",
        columnOrder: 7,
      },
      {
        key: duration_C.key,
        name: duration_C.name,
        width: columnWidth && columnWidth[duration_C.key] ? columnWidth[duration_C.key].width : 165,
        resizable: true,
        formatter: value => {
          return (
            <Typography variant="h6">
              {value.row && value.row.durationHours >= 0 && value.row.durationMins >= 0
                ? `${value.row.durationHours} hr ${value.row.durationMins} min`
                : "00:00"}
            </Typography>
          );
        },
        cellClass: "dropDownCell",
        columnOrder: 8,
        sortable: teamCanView("advanceSortAccess") ? false : true,
      },
      {
        key: "optionList",
        name: "",
        formatter: value => {
          const meetingPermission =
            value && value.row && value.row.projectId
              ? isUndefined(this.props.permissionObject[value.row.projectId])
                ? this.props.workspaceMeetingPer
                : this.props.permissionObject[value.row.projectId].meeting
              : this.props.workspaceMeetingPer;
          return (
            <MeetingActionDropdown
              filterUnArchiveMeetingDelete={this.props.filterUnArchiveMeetingDelete}
              meeting={value.row}
              CopyTask={this.props.CopyTask}
              filterMeeting={this.props.filterMeeting}
              DeleteMeeting={this.props.DeleteMeeting}
              ArchiveMeeting={this.props.ArchiveMeeting}
              UnArchiveMeeting={this.props.UnArchiveMeeting}
              DeleteMeetingSchedule={this.props.DeleteMeetingSchedule}
              ArchiveMeetingSchedule={this.props.ArchiveMeetingSchedule}
              UnArchiveMeetingSchedule={this.props.UnArchiveMeetingSchedule}
              CancelMeeting={this.props.CancelMeeting}
              CancelMeetingSchedule={this.props.CancelMeetingSchedule}
              CopyCalenderTask={this.props.CopyCalenderTask}
              UpdateCalenderTask={this.props.UpdateCalenderTask}
              selectedColor={value.row && value.row.colorCode ? value.row.colorCode : { hex: "" }}
              isUpdated={this.isUpdated}
              closeActionMenu={this.closeActionMenu}
              isArchivedSelected={this.props.isArchivedSelected}
              meetingPer={meetingPermission}
              view="list"
            />
            // <CustomIconButton
            //   btnType="condensed"

            // >
            //   <MoreVerticalIcon htmlColor={props.theme.palette.secondary.dark} style={{ fontSize: "24px !important" }} />
            // </CustomIconButton>
          );
        },
        cellClass: "dropDownCell lastColoumn",
        columnOrder: 9,
        width: columnWidth && columnWidth["optionList"] ? columnWidth["optionList"].width : 50,
      },
    ];
    return columns;
  };
  //Function that updates assignee of respective document (e.g. task/risk/issue/meeting)
  updateMeetingAttendee = (assignedTo, meeting, add = false, succFun = () => {}) => {
    let assignedToArr = assignedTo.map(ass => {
      return ass.userId;
    });
    let newMeetingObj;
    if (add) {
      newMeetingObj = {
        ...meeting,
        attendeeList: [...meeting.attendeeList, ...assignedToArr],
        // attendeeList: assignedToArr /** added updated attendee id's to attendeelist (array)   */,
      };
    } else {
      newMeetingObj = {
        ...meeting,
        // attendeeList: [...meeting.attendeeList, ...assignedToArr]
        attendeeList: assignedToArr /** added updated attendee id's to attendeelist (array)   */,
      };
    }
    this.props.UpdateMeeting(newMeetingObj, (success, data) => {
      succFun();
    });
  };

  componentWillUnmount() {
    if (this.canvas) {
      this.canvas.removeEventListener("scroll", this.scrollListener);
    }
  }
  selectedColumnArr = () => {
    const { meetingColumns = [] } = this.props;

    const meetingColumnOrder = meetingColumns.filter(c => !c.hide).map(c => c.columnName);

    let newCols = this.getColumns();

    let columns = newCols.filter(
      (x, i) =>
        meetingColumnOrder.indexOf(x.name) >= 0 ||
        i === newCols.length - 1 ||
        x.key == "meetingDisplayName"
    );

    /** containing unique values */
    this.setState({
      _columns: columns,
    });
  };
  componentDidMount() {
    const { workspaces } = this.props;
    let loggedInTeam = this.props.profileState.data.loggedInTeam;
    let userId = this.props.profileState.data.userId;

    if (loggedInTeam) {
      let { meetingColumn, meetingDirection } = getSortOrder(workspaces, loggedInTeam, 3);

      if (meetingColumn && meetingDirection) {
        let rows = this.getDetailedMeetings();
        this.sortRows(rows.completeDetails, meetingColumn, meetingDirection);
      } else {
        this.setState({ rows: this.getDetailedMeetings().completeDetails });
      }
    }
    // let details = this.getDetailedMeetings();

    this.setState({
      // rows: details.completeDetails,
      userId,
      newCol: this.state._columns,
      // _columns: this.state._columns.slice(0, 7),
      showMeetings: false,
      loggedInTeam: loggedInTeam,
      // members: details.members
    });
  }
  queryMeetingUrl = () => {
    let searchQuery = this.props.history.location.search; //getting meeting id in the url
    const { filteredMeetings } = this.props;
    const { showMeetings } = this.state;
    if (searchQuery && !showMeetings) {
      // checking if there is meeting Id in the url or not
      let meetingId = queryString.parseUrl(searchQuery).query.meetingId; //Parsing the url and extracting the meeting id using query string
      let validateMeeting = this.props.filteredMeetings.find(meeting => {
        // Checking if the meeting id is valid or not
        return meetingId == meeting.meetingId;
      });
      if (validateMeeting && (validateMeeting.startTime || validateMeeting.startDateString)) {
        //If the meeting id is valid
        this.setState({ showMeetings: true, detailsMeetingId: meetingId });
      } else {
        this.props.showSnackBar("Oops! Item not found", "info");
        //In case the meeting id in the url is wrong user is redirected to meetings Page
        this.props.history.push("/meetings");
      }
    }
  };
  columnChangeCallback = () => {
    this.selectedColumnArr();
  };
  componentDidUpdate(prevProps, prevState) {
    const { addMeeting, isReorder, sortColumn, sortDirection, detailsMeetingId } = this.state;
    const { sortObj, filteredMeetings } = this.props;

    const meetingDetailsMeeting = filteredMeetings.find(m => {
      // Checking if the meeting details meeting exist in the list or not
      return m.meetingId == detailsMeetingId;
    });
    if (detailsMeetingId && !meetingDetailsMeeting) {
      // if meeting does not exist in the filtered list remove the meeting details from state
      //This scenario can be produced if you apply filter on a meeting with assignee from sidebar and open meeting details and change assignee and than toggle filter from sidebar again
      this.setState({ showMeetingDetails: false, detailsMeetingId: "" });
    }
    if (addMeeting == true) {
      this.addMeetingInput.current.focus();
    }
    if (
      JSON.stringify(prevProps.filteredMeetings) !== JSON.stringify(this.props.filteredMeetings)
    ) {
      let details = this.getDetailedMeetings();
      if (sortColumn) this.sortRows(details.completeDetails, sortColumn, sortDirection);
      else
        this.setState({
          rows: details.completeDetails,
          members: details.members,
        });
    }
    // Clearing bulk selection on archive mode selection
    else if (JSON.stringify(prevProps.meetingState) !== JSON.stringify(this.props.meetingState)) {
      this.setState({ selectedIds: [] });
    } else if (isReorder) {
      this.setItemsOrder();
    } else if (
      this.props.profileState.data.loggedInTeam !== prevProps.profileState.data.loggedInTeam
    ) {
      this.setState({
        loggedInTeam: this.props.profileState.data.loggedInTeam,
      });
    }

    if (
      sortObj &&
      sortObj.column &&
      (sortObj.column !== prevProps.sortObj.column ||
        sortObj.direction !== prevProps.sortObj.direction)
    ) {
      /** when user click on selected sort button for changing the sort order, this condtion will execute */
      let details = this.getDetailedMeetings();
      this.sortRows(
        details.completeDetails,
        sortObj.column,
        sortObj.direction
      ); /** function call for reseting the state meting list into selected sort manner */
    } else if (
      sortObj &&
      sortObj.cancelSort == "NONE" &&
      sortObj.cancelSort !== prevProps.sortObj.cancelSort
    ) {
      /** when user click on cancel sort this condtion will execute */
      let details = this.getDetailedMeetings();
      this.setState({
        rows: details.completeDetails,
      }); /** updating state meeting list  */
    }
    if (!isEqual(prevProps.meetingColumns, this.props.meetingColumns)) {
      this.selectedColumnArr();
    }
    this.queryMeetingUrl();
    this.renderHeaders();
  }
  renderHeaders = () => {
    var elements = document.getElementsByClassName("widget-HeaderCell__value");
    var sortableElements = document.getElementsByClassName("react-grid-HeaderCell-sortable");
    for (var i = 0; i < elements.length; i++) {
      elements[i].innerText = headerTranslations(elements[i].innerText, this.props.intl);
    }
    for (var i = 0; i < sortableElements.length; i++) {
      sortableElements[i].innerText = headerTranslations(
        sortableElements[i].innerText,
        this.props.intl
      );
    }
  };
  //Function that updates Meeting Task
  updateTask = (selectedTask, task, meeting) => {
    let newMeetingObj = {
      ...meeting,
      taskId: selectedTask.length ? selectedTask[0].id : "",
      projectId: selectedTask.length ? selectedTask[0].obj.projectId : meeting.projectId,
    };
    this.props.UpdateMeeting(
      newMeetingObj,
      () => {},
    );
  };
  // Generate list of all projects for dropdown understandable form
  generateTaskDropdownData = meeting => {
    const { tasks } = this.props;
    let tasksData =
      meeting.projectId && meeting.projectId !== ""
        ? tasks.filter(t => t.projectId === meeting.projectId)
        : tasks;

    let tasksArr = generateTaskData(tasksData);
    return tasksArr;
  };
  //Generate list of selected options for project dropdown understandable form
  getSelectedTask = meeting => {
    const { tasks } = this.props;
    let selectedTask = tasks.find(task => {
      return task.taskId == meeting.taskId;
    });

    return selectedTask
      ? [
          {
            label: selectedTask.taskTitle,
            id: selectedTask.taskId,
            obj: meeting,
          },
        ]
      : [];
  };
  handleStaredChange = (event, meeting) => {
    let isStared = event.target.checked;
    if (meeting) {
      let obj = {
        Id: meeting.id,
        MarkStar: isStared,
      };
      this.props.MarkMeetingAsStarted(obj, (err, data) => {});
    }
  };

  setItemsOrder = () => {
    let props = this.props;
    let rows = cloneDeep(this.state.rows)
      .map(x => {
        let position = helper.RETURN_ITEMORDER(props.itemOrderState.data.itemOrder, x.id);
        x.taskOrder = position || 0;
        return x;
      })
      .sort((a, b) => a.taskOrder - b.taskOrder);

    this.setState({ rows, isReorder: false });
  };

  getDetailedMeetings = () => {
    const props = this.props;
    let meetings = props.filteredMeetings || [];
    let members =
      props.profileState && props.profileState.data && props.profileState.data.member
        ? props.profileState.data.member.allMembers
        : [];
    let completeDetails = meetings.map(x => {
      x.assigneeLists = members.filter(m => {
        if (x.attendeeList && x.attendeeList.length >= 0)
          return x.attendeeList.indexOf(m.userId) >= 0;
      });
      return x;
    });
    return {
      completeDetails,
      members,
    };
  };

  showDateTimeDetailsPopUp(data) {
    this.setState({
      detailsMeetingId: data.meetingId,
      meetingDateTimeDialog: true,
    });
  }
  closeDateTimeDetailsPopUp() {
    this.setState({ meetingDateTimeDialog: false });
  }
  closeActionMenu() {
    if (this.props.meetingState.indexOf("Archived") >= 0) this.setState({ isArchived: true });
    else this.setState({ isArchived: false });
  }
  selectedSaveDate(date, data) {
    if (data && date) {
      const object = helper.CALCULATEDURATIONDATETIME(date, data);
      this.props.UpdateMeeting(object, (err, data) => {
        //
      });
    }
    this.setState({ saveDate: new Date(date._d) });
  }
  selectedSaveTime(time, data) {
    if (data) {
      if (data) {
        const object = helper.CALCULATEDURATIONDATETIME(time, data, true);
        this.props.UpdateMeeting(object, (err, data) => {});
      }
    }
    this.setState({ saveTime: time });
  }
  handleDialogClose(event, name) {
    this.setState({ [name]: false });
  }

  handleDialog(event, name) {
    this.setState({ [name]: true, open: false });
  }
  scrollListener() {
    if (this.canvas.scrollHeight - (this.canvas.scrollTop + this.canvas.clientHeight) < 1) {
      this.handleScrollDown();
    }
  }

  handleScrollDown = () => {
    this.setState({ showRecords: this.state.showRecords + increment }, () => {
      if (this.state.showRecords >= this.state.rows.length) {
        this.setState({ showRecords: this.state.rows.length });
      }
    });
  };
  showMeetingsPopUp(e, data) {
    const { workspaceMeetingPer, permissionObject } = this.props;
    const meetingPermission =
      data && data.projectId
        ? isUndefined(permissionObject[data.projectId])
          ? workspaceMeetingPer
          : permissionObject[data.projectId].meeting
        : workspaceMeetingPer;
    if (e > -1 && meetingPermission.meetingDetail.cando) {
      const readOnly = getReadOnlyPermissions(data);
      this.setState({
        showMeetings: data && (!data.startTime || !data.startDateString) ? false : true,
        detailsMeetingId: data.meetingId,
        addMeeting: false,
        meetingDateTimeDialog:
          data && !data.isDelete && (!data.startTime || !data.startDateString) && !readOnly
            ? true
            : false,
      });
    }
  }
  closeMeetingsPopUp() {
    this.setState({ showMeetings: false });
  }

  isUpdated(data) {
    this.props.UpdateMeeting(data, (err, data) => {
      if (this.props.meetingState.indexOf("Archived") >= 0)
        this.setState({ isArchived: true, isInstanceChange: false });
    });
  }
  BulkUpdate(data, callback) {
    this.props.BulkUpdateMeeting(data, (err, resp) => {
      if (callback) {
        this.setState({ selectedIds: [] });
        callback();
      }
      if (resp) {
        // Unarchive scenerio
        if (this.props.meetingState.indexOf("Archived") >= 0) {
          this.setState({ isArchived: true, selectedIds: [] });
          this.props.filterMeeting(data.meetingIds, data.type);
        } else this.setState({ isArchived: false });
      }
    });
  }

  addMeeting(keyName) {
    if (keyName && keyName === "enter") {
      this.setState({ addNewForm: true });
    } else if (keyName && keyName === "inline") {
      this.setState({ addMeeting: true });
    } else {
      this.setState({ addNewForm: true });
    }
  }
  cancelAddMeeting() {
    this.setState({
      addMeeting: false,
      addNewForm: false,
      TaskError: false,
      TaskErrorMessage: "",
    });
  }
  handleChange(e) {
    if (e.target.value.length > 250) {
      this.setState({
        TaskError: true,
        TaskErrorMessage: this.props.intl.formatMessage({
          id: "meeting.creation-dialog.form.validation.meeting-title.length",
          defaultMessage: "Please enter less than 250 characters",
        }),
        value: e.target.value,
      });
    } else {
      this.setState({
        TaskError: false,
        TaskErrorMessage: "",
        value: e.target.value,
      });
    }
  }
  onKeyDown(event) {
    if (event.keyCode === 13) {
      let checkEmpty = helper.RETURN_CECKSPACES(this.state.value);
      if (!this.state.value || checkEmpty) {
        this.setState({
          TaskError: true,
          TaskErrorMessage:
            checkEmpty ||
            this.props.intl.formatMessage({
              id: "meeting.creation-dialog.form.validation.meeting-title.list-title",
              defaultMessage: "Please enter meeting title",
            }),
        });

        return;
      }

      if (this.state.value && this.state.value.length > 250) {
        this.setState({
          TaskError: true,
          TaskErrorMessage: this.props.intl.formatMessage({
            id: "meeting.creation-dialog.form.validation.meeting-title.length",
            defaultMessage: "Please enter less than 250 characters",
          }),
        });
        return;
      }

      let splittedData = this.state.saveTime.split(":");
      let startDate = parseInt(splittedData[0]) + 1;
      splittedData[0] = startDate;
      let data = {
        // endDate: this.state.saveDate,
        // endTime: splittedData.join(":"),
        // //startDate: this.state.saveDate,
        // startTime: this.state.saveTime,
        // durationHours: 1,
        // durationMins: 0,
        title: this.state.value,
        meetingDisplayName: this.state.value,
      };

      this.props.CreateInLineMeeting(data, (err, x) => {
        if (x) {
          this.grid.selectAllCheckbox.checked = false;
          // this.setState({ addMeeting: false });
        } else {
          this.setState({
            TaskError: false,
            TaskErrorMessage: x ? x.data : "",
          });
        }
      });
      this.setState({ value: "" });
    } else if (event.keyCode == 27) {
      this.setState({ addMeeting: false });
    } else {
      this.setState({ addMeeting: true });
    }
  }

  getRandomDate = (start, end) => {
    return new Date(
      start.getTime() + Math.random() * (end.getTime() - start.getTime())
    ).toLocaleDateString();
  };

  rowGetter = i => {
    if (i >= 0) return this.state.rows[i];
  };

  isDraggedRowSelected = (selectedRows, rowDragSource) => {
    if (selectedRows && selectedRows.length > 0) {
      let key = this.props.rowKey;
      return selectedRows.filter(r => r[key] === rowDragSource.data[key]).length > 0;
    }
    return false;
  };

  reorderRows = e => {
    let selectedRows = Selectors.getSelectedRowsByKey({
      rowKey: this.props.rowKey,
      selectedKeys: this.state.selectedIds,
      rows: this.state.rows,
    });
    let draggedRows = this.isDraggedRowSelected(selectedRows, e.rowSource)
      ? selectedRows
      : [e.rowSource.data];
    let undraggedRows = this.state.rows.filter(function(r) {
      return draggedRows.indexOf(r) === -1;
    });
    let args = [e.rowTarget.idx, 0].concat(draggedRows);
    Array.prototype.splice.apply(undraggedRows, args);

    undraggedRows = undraggedRows.map((x, i) => {
      x.taskOrder = i;
      return x;
    });
    let orderToBeSaved = undraggedRows.map(x => {
      return x.id;
    });
    let itemOrder = this.props.itemOrderState.data;
    itemOrder.itemOrder = itemOrder.itemOrder
      .map((x, i) => {
        if (orderToBeSaved.indexOf(x.itemId) >= 0) {
          return {
            itemId: x.itemId,
            position: orderToBeSaved.indexOf(x.itemId),
          };
        } else return x;
      })
      .sort((a, b) => a.position - b.position);
    this.props.SaveItemOrder(
      itemOrder,
      data => {
        this.setState({
          rows: undraggedRows,
          isReorder: true,
          showMeetings: false,
        });
      },
      () => {},
      undraggedRows,
      "meeting"
    );
  };
  onRowsSelected = rows => {
    // let row = rows.row;
    const filterRows = rows.filter(r => !r.row.__metaData); // Removing Grouping Rows from the selected list
    let selectedIds = this.state.selectedIds.concat(filterRows.map(r => r.row[this.props.rowKey]));
    let selectedMeetingObjArr = [...this.state.selectedMeetingObj, ...rows];

    if (selectedIds.length > 1 && !teamCanView("bulkActionAccess")) {
      this.setState({ showUnplanned: true });
      return;
    }
    this.setState(
      {
        selectedIds: selectedIds,
        showMeetings: false,
        // selectedRowList: { ...this.state.selectedRowList, row }
        selectedMeetingObj: selectedMeetingObjArr,
      },
      () => {
        if (this.state.rows.length === this.state.selectedIds.length) {
          this.grid.selectAllCheckbox.checked = true;
        }
      }
    );
  };

  onRowsDeselected = rows => {
    let rowIds = rows.map(r => r.row[this.props.rowKey]);
    this.setState({
      selectedIds: this.state.selectedIds.filter(i => rowIds.indexOf(i) === -1),
      showMeetings: false,
      selectedMeetingObj: this.state.selectedMeetingObj.filter(
        s => rowIds.indexOf(s.row[this.props.rowKey]) === -1
      ),
    });
  };
  closeMeetingDetailsPopUp() {
    this.setState({ showMeetings: false });
  }

  sortRows = (initialRows, sortColumn, sortDirection) => {
    if (sortDirection === "NONE") {
      let details = this.getDetailedMeetings();
      this.setState({
        rows: details.completeDetails,
        sortColumn: null,
        sortDirection,
      });
    } else {
      let rows = sortListData(initialRows, sortColumn, sortDirection);
      this.setState({ rows, sortColumn, sortDirection });
    }
  };
  handleClose = event => {
    /** function call when close drop down */
    this.setState({ showUnplanned: false });
  };

  openRepeatMeetingDrawer = (event, meeting) => {
    event.stopPropagation();
    this.setState({
      repeatMeetingDrawer: true,
      repeatMeetingId: meeting.meetingId,
    });
  };
  closeRepeatMeetingDrawer = () => {
    this.setState({ repeatMeetingDrawer: false, repeatMeetingDrawer: "" });
  };

  render() {
    const {
      addMeeting,
      addNewForm,
      selectedIds,
      detailsMeetingId,
      isWorkspaceChanged,
      meetingDateTimeDialog,
      isInstanceChange,
      rows,
      repeatMeetingId,
      repeatMeetingDrawer,
      showUnplanned,
    } = this.state;
    const {
      classes,
      theme,
      meetingDialog,
      meetingState,
      filteredMeetings,
      appliedFiltersState,
      isArchivedSelected,
      selectedGroup,
      workspaceMeetingPer,
      intl,
      permissionObject,
    } = this.props;
    if (this.state.rows.length > 0 && this.state.rows.length === this.state.selectedIds.length) {
      if (this.grid && this.grid.selectAllCheckbox) this.grid.selectAllCheckbox.checked = true;
    } else {
      if (this.grid && this.grid.selectAllCheckbox) this.grid.selectAllCheckbox.checked = false;
    }
    if (isWorkspaceChanged) {
      if (this.grid && this.grid.selectAllCheckbox) this.grid.selectAllCheckbox.checked = false;
    }
    const EmptyRowsView = () => {
      return isArchivedSelected ? (
        <EmptyState
          screenType="Archived"
          heading={
            <FormattedMessage id="common.archived.label" defaultMessage="No archived items found" />
          }
          message={
            <FormattedMessage
              id="common.archived.message"
              defaultMessage="You haven't archived any items yet."
            />
          }
          button={false}
        />
      ) : (meetingState.length > 0 || appliedFiltersState.Meeting) &&
        (filteredMeetings || filteredMeetings.length <= 0) ? (
        <EmptyState
          screenType="search"
          heading={
            <FormattedMessage id="common.search-list.label" defaultMessage="No Results Found" />
          }
          message={
            <FormattedMessage
              id="common.search-list.message"
              defaultMessage="No matching results found against your filter criteria."
            />
          }
          button={false}
        />
      ) : workspaceMeetingPer.createMeeting.cando ? (
        <EmptyState
          screenType="meeting"
          heading={
            <FormattedMessage
              id="common.create-first.meeting.label"
              defaultMessage="Create your first meeting"
            />
          }
          message={
            <FormattedMessage
              id="common.create-first.meeting.messagea"
              defaultMessage='You do not have any meetings yet. Press "Alt + M" or click on button below.'
            />
          }
          button={true}
        />
      ) : (
        <EmptyState
          screenType="meeting"
          heading={
            <FormattedMessage
              id="common.create-first.meeting.messageb"
              defaultMessage="You do not have any meetings yet."
            />
          }
          message={
            <FormattedMessage
              id="common.create-first.meeting.messagec"
              defaultMessage="Request your team member(s) to assign you a meeting."
            />
          }
          button={false}
        />
      );
    };
    const groupBy = selectedGroup.key && [selectedGroup.key];
    const groupedRows = groupBy ? Data.Selectors.getRows({ rows, groupBy }) : rows;

    return (
      <Fragment>
        {repeatMeetingId && repeatMeetingDrawer && (
          <SideDrawer
            closeAction={this.closeRepeatMeetingDrawer}
            open={repeatMeetingDrawer}
            repeatMeetingId={repeatMeetingId}
            meetingPer={workspaceMeetingPer}
            allMeetings={rows}
          />
        )}
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={showUnplanned}
          onClose={this.handleClose}
          onBackdropClick={this.handleClose}
          BackdropProps={{
            classes: {
              root: classes.unplannedRoot,
            },
          }}>
          <div className={classes.unplannedMain} style={{ top: 400, left: 200 }}>
            <UnPlanned
              feature="premium"
              titleTxt={
                <FormattedMessage
                  id="common.discovered-dialog.business-title"
                  defaultMessage="Wow! You've discovered a Premium feature!"
                />
              }
              boldText={this.props.intl.formatMessage({
                id: "common.discovered-dialog.list.bulk-action.title",
                defaultMessage: "Bulk Actions",
              })}
              descriptionTxt={
                <FormattedMessage
                  id="common.discovered-dialog.list.bulk-action.label"
                  defaultMessage={"is available on our Premium Plan. Upgrade now or start a {TRIALPERIOD}-day free trial to find out all nTask Premium features."}
                  values={{TRIALPERIOD: TRIALPERIOD}}
                />
              }
              showBodyImg={false}
              showDescription={true}
              selectUpgrade={this.handleClose}
              titleStyle={{ fontSize: "18px" }}
            />
          </div>
        </Modal>
        <DefaultDialog
          title={
            <FormattedMessage
              id="meeting.creation-dialog.form.title"
              defaultMessage="Add Meeting"></FormattedMessage>
          }
          open={addNewForm}
          onClose={this.cancelAddMeeting}>
          <AddMeetingForm
            handleCloseMeeting={this.cancelAddMeeting}
            isNew={true}
            isDialog={true}
            maxLength={250}
          />
        </DefaultDialog>
        <DefaultDialog
          title={
            <FormattedMessage
              id="meeting.creation-dialog.title"
              defaultMessage="Add Meeting Details"></FormattedMessage>
          }
          open={meetingDateTimeDialog}
          onClose={event => this.handleDialogClose(event, "meetingDateTimeDialog")}>
          <AddMeetingForm
            handleCloseMeeting={this.closeDateTimeDetailsPopUp}
            isNew={true}
            isDialog={true}
            isAddMeetingDetails={true}
            currentMeeting={this.props.filteredMeetings.find(function(x) {
              return x.meetingId == detailsMeetingId;
            })}
            isUpdated={this.isUpdated}
            maxLength={250}
          />
        </DefaultDialog>

        {this.state.showMeetings && detailsMeetingId && filteredMeetings.length > 0 ? (
          <MeetingDetails
            isUpdated={this.isUpdated}
            closeMeetingDetailsPopUp={this.closeMeetingDetailsPopUp}
            currentMeeting={
              rows.filter(function(x) {
                return x.meetingId == detailsMeetingId;
              })[0]
            }
            listView={true}
            isArchivedSelected={isArchivedSelected}
            isInstanceChange={isInstanceChange}
            filterMeeting={this.props.filterMeeting}
            UnArchiveMeeting={this.props.UnArchiveMeeting}
            updateMeetingAttendee={this.updateMeetingAttendee}
          />
        ) : null}

        <Hotkeys keyName="alt+m" onKeyDown={this.onKeyDown} />
        <div style={{ position: "relative" }}>
          <div
            style={{
              width: "100%",
              position: "absolute",
              paddingLeft: 0,
            }}
            className={selectedIds.length > 1 ? "multiChecked" : null}>
            {selectedIds.length > 1 ? (
              <BulkActions
                selectedIdsList={this.state.selectedIds}
                BulkUpdate={this.BulkUpdate}
                isArchivedSelected={isArchivedSelected}
                workspaceMeetingPer={workspaceMeetingPer}
                selectedMeetingObj={this.state.selectedMeetingObj}
                permissionObject={permissionObject}
              />
            ) : null}
            <div className={classes.TableActionBtnDD}>
              <ColumnSelectionDropdown
                feature={"meeting"}
                columnChangeCallback={this.columnChangeCallback}
              />
            </div>

            <DraggableContainer>
              <ReactDataGrid
                onRowClick={this.showMeetingsPopUp.bind(this)}
                enableCellSelection={teamCanView("bulkActionAccess")}
                ref={node => (this.grid = node)}
                rowActionsCell={RowActionsCell}
                columns={this.state._columns}
                emptyRowsView={EmptyRowsView}
                onGridSort={(sortColumn, sortDirection) =>
                  this.sortRows(rows, sortColumn, sortDirection)
                }
                rowGetter={i => {
                  return groupedRows[i];
                }}
                rowsCount={groupedRows.length}
                rowHeight={60}
                headerRowHeight={
                  isArchivedSelected || !workspaceMeetingPer.createMeeting.cando ? 50 : 105
                }
                minHeight={calculateContentHeight()}
                rowGroupRenderer={param => {
                  return GroupingRowRenderer(param, rows, classes, intl);
                }}
                selectAllCheckbox={true}
                rowRenderer={<RowRenderer onRowDrop={this.reorderRows} />}
                rowSelection={{
                  showCheckbox: true,
                  enableShiftSelect: true,
                  onRowsSelected: this.onRowsSelected,
                  onRowsDeselected: this.onRowsDeselected,
                  selectBy: {
                    keys: {
                      rowKey: this.props.rowKey,
                      values: this.state.selectedIds,
                    },
                  },
                }}
                onColumnResize={this.onColumnResize}
              />
            </DraggableContainer>
            {/* </BottomScrollListener> */}
            {isArchivedSelected ? null : workspaceMeetingPer.createMeeting.cando ? (
              <AddMeeting
                className={addMeeting ? "addTaskInputCnt" : "addTaskCnt"}
                style={{ left: 36 }}
                onClick={!addMeeting ? () => this.addMeeting("inline") : undefined}>
                {addMeeting ? (
                  <Fragment>
                    {this.state.TaskError ? (
                      <div htmlFor="TaskError" className={classes.selectError}>
                        {this.state.TaskErrorMessage ? this.state.TaskErrorMessage : ""}
                      </div>
                    ) : null}
                    {/* <Hotkeys keyName="enter,esc" onKeyDown={this.onKeyDown}> */}
                    <input
                      ref={this.addMeetingInput}
                      onKeyDown={event => this.onKeyDown(event)}
                      id="addTaskInput"
                      style={{
                        border: this.state.TaskError
                          ? `1px solid ${theme.palette.border.redBorder}`
                          : null,
                      }}
                      autoFocus={this.state.focus}
                      onBlur={this.cancelAddMeeting}
                      type="text"
                      onChange={this.handleChange}
                      value={this.state.value}
                    />
                    {/* </Hotkeys> */}

                    <Grid
                      container
                      direction="row"
                      justify="flex-end"
                      alignItems="center"
                      classes={{ container: classes.addTaskInputClearCnt }}>
                      {/* <StaticDatePicker
                    selectedSaveDate={this.selectedSaveDate}
                    isCreation={true}
                  />
                  <TimePicker
                    selectedSaveTime={this.selectedSaveTime}
                    isCreation={true}
                    isAm={true}
                  /> */}
                      <span className={classes.addTaskInputClearText}>
                        <FormattedMessage
                          id="common.press.message"
                          defaultMessage="Press Enter to Add"
                        />
                      </span>
                      <IconButton btnType="condensed" onClick={this.cancelAddMeeting}>
                        <CancelIcon
                          fontSize="default"
                          htmlColor={theme.palette.secondary.light}
                        />
                      </IconButton>
                    </Grid>
                  </Fragment>
                ) : (
                  <Grid container direction="row" justify="space-between" alignItems="center">
                    <Grid item>
                      <Grid container direction="row" justify="flex-start" alignItems="center">
                        <AddIcon htmlColor={theme.palette.primary.light} />
                        <span className={classes.addNewTaskText}>
                          <FormattedMessage
                            id="meeting.add-new.label"
                            defaultMessage="Add New Meeting"
                          />
                        </span>
                      </Grid>
                    </Grid>
                    <Grid item>
                      <p className={classes.shortcutText}>
                        {" "}
                        {`${this.props.intl.formatMessage({
                          id: "common.press.label",
                          defaultMessage: "Press",
                        })} Alt + M`}
                      </p>
                    </Grid>
                  </Grid>
                )}
              </AddMeeting>
            ) : null}
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    workspacePermissionsState: state.workspacePermissions,
    profileState: state.profile,
    itemOrderState: state.itemOrder,
    appliedFiltersState: state.appliedFilters,
    tasks: state.tasks.data,
    workspaces: state.profile.data.workspace,
    workspaceMeetingPer: state.workspacePermissions.data.meeting,
    meetingColumns: state.columns.meeting,
    projects: state.projects.data,
    permissionObject: ProjectPermissionSelector(state),
  };
};

export default compose(
  withRouter,
  injectIntl,
  withStyles(listStyles, { withTheme: true }),
  connect(mapStateToProps, {
    UpdateTask,
    UpdateCalenderTask,
    SaveItemOrder,
    FetchTasksInfo,
    CopyTask,
    DeleteTask,
    ArchiveTask,
    UnArchiveTask,

    ////////////////////////

    UpdateMeeting,
    CreateInLineMeeting,
    BulkUpdateMeeting,
    DeleteMeeting,
    ArchiveMeeting,
    UnArchiveMeeting,
    DeleteMeetingSchedule,
    ArchiveMeetingSchedule,
    UnArchiveMeetingSchedule,
    CancelMeeting,
    CancelMeetingSchedule,
    MarkMeetingAsStarted,
  })
)(MeetingList);
