import React, { Component, Fragment } from "react";
import { withStyles, withTheme } from "@material-ui/core/styles";
import itemStyles from "./meetingList";
import menuStyles from "../../../assets/jss/components/menu";
import CustomIconButton from "../../../components/Buttons/IconButton";
import SelectionMenu from "../../../components/Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import combineStyles from "../../../utils/mergeStyles";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import RightArrow from "@material-ui/icons/ArrowRight";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import DeleteConfirmDialog from "../../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import ColorPicker from "../../../components/Dropdown/ColorPicker/ColorPicker";
import { FormattedMessage } from "react-intl";

class MeetingActionDropDown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      pickerOpen: false,
      pickerPlacement: "",
      archiveFlag: false,
      unArchiveFlag: false,
      deleteFlag: false,
      archiveScheduleFlag: false,
      unArchiveScheduleFlag: false,
      deleteScheduleFlag: false,
      cancelFlag: false,
      cancelScheduleFlag: false,
      deleteMeetingBtnQuery: "",
      deleteScheduleMeetingBtnQuery: "",
      archiveBtnQuery: "",
      archiveScheduleBtnQuery: "",
      unarchiveBtnQuery: "",
      unarchiveScheduledBtnQuery: "",
      cancelBtnQuery: "",
      cancelScheduledBtnQuery: "",
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleColorClick = this.handleColorClick.bind(this);
    this.handlePickerClose = this.handlePickerClose.bind(this);
    this.handleDialogClose = this.handleDialogClose.bind(this);
  }

  componentDidMount = () => {
    this.setState({
      selectedColor: this.props.selectedColor,
    });
  };

  handleOperations = (event, value) => {
    event.stopPropagation();
    switch (value) {
      case "Copy":
        this.props.CopyTask(this.props.meeting);
        break;
      case "Public Link":
        break;
      case "Archive Meeting":
        this.setState({ archiveFlag: true, popupFlag: true });
        break;
      case "Unarchive Meeting":
        this.setState({ unArchiveFlag: true, popupFlag: true });
        break;
      case "Delete Meeting":
        this.setState({ deleteFlag: true, popupFlag: true });
        break;
      case "Unarchive Meeting Schedule":
        this.setState({ unArchiveScheduleFlag: true, popupFlag: true });
        break;
      case "Archive Meeting Schedule":
        this.setState({ archiveScheduleFlag: true, popupFlag: true });
        break;

      case "Delete Meeting Schedule":
        this.setState({ deleteScheduleFlag: true, popupFlag: true });
        break;
      case "Cancel Meeting":
        this.setState({ cancelFlag: true, popupFlag: true });
        break;
      case "Cancel Meeting Schedule":
        this.setState({ cancelScheduleFlag: true, popupFlag: true });
        break;
    }

    this.setState({ open: false, pickerOpen: false });
  };
  handleClose(event) {
    event.stopPropagation();
    this.setState({ open: false, pickerOpen: false });
  }
  handlePickerClose(event) {
    this.setState({ pickerOpen: false });
  }
  handleDialogClose(e, isChanged) {
    if (e) e.stopPropagation();
    this.setState(
      {
        popupFlag: false,
        archiveFlag: false,
        unArchiveFlag: false,
        deleteFlag: false,
        archiveScheduleFlag: false,
        unArchiveScheduleFlag: false,
        deleteScheduleFlag: false,
        cancelFlag: false,
        cancelScheduleFlag: false,
      },
      () => {
        if (isChanged && this.props.closeActionMenu) this.props.closeActionMenu();
      }
    );
  }

  handleClick(event, placement) {
    event.stopPropagation();
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }
  handleColorClick(event) {
    event.stopPropagation();
    if (event.target.closest("#colorPickerCnt") == null) {
      this.setState(prevState => ({ pickerOpen: !prevState.pickerOpen }));
    }
  }

  colorChange = (color, task) => {
    let self = this;
    this.setState({ selectedColor: color }, function () {
      let obj = Object.assign({}, task);
      this.setState({
        selectedColor: color,
      });
      obj.colorCode = color;
      this.props.isUpdated(obj, this.props.meeting);
    });
  };

  deleteMeeting = () => {
    const { meetingId } = this.props.meeting;
    this.setState({ deleteMeetingBtnQuery: "progress" });
    this.props.DeleteMeeting(
      meetingId,
      response => {
        this.setState({ deleteMeetingBtnQuery: "", deleteFlag: false });
        // this.props.filterMeeting(meetingId);
        this.props.filterUnArchiveMeetingDelete(meetingId);
      },
      error => {
        this.setState({ deleteMeetingBtnQuery: "", deleteFlag: false });
      }
    );
  };

  deleteMeetingSchedule = () => {
    const { meetingId, parentId } = this.props.meeting;
    this.setState({ deleteScheduleMeetingBtnQuery: "progress" });
    this.props.DeleteMeetingSchedule(
      meetingId,
      parentId,
      response => {
        this.setState({
          deleteScheduleMeetingBtnQuery: "",
          deleteScheduleFlag: false,
        });
      },
      error => {
        this.setState({
          deleteScheduleMeetingBtnQuery: "",
          deleteScheduleFlag: false,
        });
      }
    );
  };

  handleArchive = (e, isScheduled) => {
    if (e) e.stopPropagation();
    if (isScheduled) {
      this.setState({ archiveScheduleBtnQuery: "progress" }, () => {
        this.props.ArchiveMeetingSchedule(
          this.props.meeting.meetingId,
          this.props.meeting.parentId,
          () => {
            this.setState({
              archiveScheduleBtnQuery: "",
              archiveScheduleFlag: false,
            });
          }
        );
      });
    } else
      this.setState({ archiveBtnQuery: "progress" }, () => {
        this.props.ArchiveMeeting(this.props.meeting.meetingId, result => {
          this.setState({ archiveBtnQuery: "", archiveFlag: false });
          this.props.filterMeeting([result.meetingId]);
        });
      });
  };

  handleUnArchive = (e, isScheduled) => {
    if (e) e.stopPropagation();
    if (isScheduled) {
      this.setState({ unarchiveScheduledBtnQuery: "progress" }, () => {
        this.props.UnArchiveMeetingSchedule(
          this.props.meeting.meetingId,
          this.props.meeting.parentId,
          () => {
            this.setState({
              unarchiveScheduledBtnQuery: "",
              unArchiveScheduleFlag: false,
            });
          }
        );
      });
    } else
      this.setState({ unarchiveBtnQuery: "progress" }, () => {
        this.props.UnArchiveMeeting(this.props.meeting.meetingId, result => {
          this.setState({ unarchiveBtnQuery: "", unArchiveFlag: false });
          this.props.filterMeeting([result.meetingId]);
        });
      });
  };

  handleCancelMeeting = (e, isScheduled) => {
    if (e) e.stopPropagation();
    if (isScheduled) {
      this.setState({ cancelScheduledBtnQuery: "progress" }, () => {
        this.props.CancelMeetingSchedule(
          this.props.meeting.meetingId,
          this.props.meeting.parentId,
          () => {
            this.setState({
              cancelScheduledBtnQuery: "",
              cancelScheduleFlag: false,
            });
          }
        );
      });
    } else
      this.setState({ cancelBtnQuery: "progress" }, () => {
        this.props.CancelMeeting(this.props.meeting.meetingId, () => {
          this.setState({ cancelBtnQuery: "", cancelFlag: false });
        });
      });
  };
  getTranslatedId(value) {
    switch (value) {
      case "Color":
        return "common.action.color.label";
        break;
      case "Cancel Meeting":
        return "common.action.cancel.meeting.label";
        break;
      case "Archive Meeting":
        return "common.archived.meeting.archived-meeting-label";
        break;
      case "Archive Meeting Schedule":
        return "common.archived.meeting.messageb";
        break;
      case "Unarchive Meeting":
        return "common.action.un-archive.confirmation.title";
        break;
      case "Unarchive Meeting Schedule":
        return "common.un-archived.meetings.messageb";
        break;
      case "Delete Meeting":
        return "common.action.delete.meeting.messageb";
        break;
      case "Delete Meeting Schedule":
        return "common.action.delete.meeting.messagea";
        break;
      default:
        return value;
        break;
    }
  }

  render() {
    const {
      classes,
      meeting,
      theme,
      isArchivedSelected,
      meetingPer,
      view,
    } = this.props;
    const {
      open,
      placement,
      pickerOpen,
      selectedColor,
      deleteFlag,
      unArchiveFlag,
      archiveFlag,
      archiveScheduleFlag,
      unArchiveScheduleFlag,
      deleteScheduleFlag,
      cancelFlag,
      cancelScheduleFlag,
      deleteMeetingBtnQuery,
      deleteScheduleMeetingBtnQuery,
      archiveBtnQuery,
      archiveScheduleBtnQuery,
      unarchiveBtnQuery,
      unarchiveScheduledBtnQuery,
      cancelBtnQuery,
      cancelScheduledBtnQuery,
    } = this.state;
    const ddData = isArchivedSelected ? [] : ["Color"];

    ////////////////////////////////////////////////////////////////Cancel//////////////////////////////////
    if (meeting.isDelete === false && meetingPer.cancelMeeting.cando) {
      ddData.push("Cancel Meeting");
    }
    if (
      meeting.isDelete === false &&
      meeting.parentId &&
      (this.props.meeting.isOwner || meetingPer.cancelMeeting.cando)
    ) {
      ddData.push("Cancel Meeting Schedule");
    }

    //////////////////////////////////////////////////Archive//////////////////////////////////////////////

    if (meeting.isDelete === false && meetingPer.archiveMeetings.cando) {
      ddData.push("Archive Meeting");
    }
    if (meeting.isDelete === false && meeting.parentId && meetingPer.archiveMeetings.cando) {
      ddData.push("Archive Meeting Schedule");
    }
    if (this.props.meeting.isDelete === true && meetingPer.unarchiveMeetings.cando) {
      ddData.push("Unarchive Meeting");
    }
    if (meeting.isDelete === true && meeting.parentId && meetingPer.unarchiveMeetings.cando) {
      ddData.push("Unarchive Meeting Schedule");
    }
    ///////////////////////////////////////// Delete ///////////////////////////////////

    if (meetingPer.deleteMeeting.cando) {
      ddData.push("Delete Meeting");
    }
    if (meeting.parentId && (meeting.isOwner || meetingPer.deleteMeeting.cando)) {
      ddData.push("Delete Meeting Schedule");
    }

    return (
      <Fragment>
        <ClickAwayListener onClickAway={this.handleClose}>
          <div>
            <CustomIconButton
              btnType="condensed"
              onClick={event => {
                this.handleClick(event, "bottom-end");
              }}
              buttonRef={node => {
                this.anchorEl = node;
              }}>
              <MoreVerticalIcon
                htmlColor={theme.palette.secondary.medDark}
                style={{ fontSize: "24px" }}
              />
            </CustomIconButton>
            <SelectionMenu
              open={open}
              closeAction={this.handleClose}
              placement={placement}
              anchorRef={this.anchorEl}
              list={
                <List>
                  <ListItem disableRipple={true} classes={{ root: classes.menuHeadingItem }}>
                    <ListItemText
                      primary={
                        <FormattedMessage id="common.action.label" defaultMessage="Select Action" />
                      }
                      classes={{ primary: classes.menuHeadingListItemText }}
                    />
                  </ListItem>
                  {ddData.map(value =>
                    value == "Color" ? (
                      <ListItem
                        key={value}
                        button
                        disableRipple={true}
                        classes={{
                          root: classes.selectColorMenuItem,
                          selected: classes.statusMenuItemSelected,
                        }}
                        onClick={event => {
                          this.handleColorClick(event);
                        }}>
                        <ListItemText
                          primary={
                            <FormattedMessage id={this.getTranslatedId(value)} defaultMessage={value} />
                          }
                          classes={{
                            primary: classes.statusItemText,
                          }}
                        />
                        <RightArrow
                          htmlColor={theme.palette.secondary.dark}
                          classes={{ root: classes.submenuArrowBtn }}
                        />
                        <div
                          id="colorPickerCnt"
                          className={
                            view == "grid" ? classes.colorPickerCnt : classes.colorPickerCntLeft
                          }
                          style={pickerOpen ? { display: "block" } : { display: "none" }}>
                          <ColorPicker
                            triangle="hide"
                            onColorChange={color => {
                              this.colorChange(color, this.props.meeting);
                            }}
                            selectedColor={selectedColor}
                          />
                        </div>
                      </ListItem>
                    ) : value == "Delete Meeting" ? ( //organizerId
                      // this.props.meeting.organizerId !== null ()
                      this.props.meeting.status !== "Published" &&
                        this.props.meeting.status !== "InReview" ? (
                        <ListItem
                          key={value}
                          button
                          disableRipple={true}
                          classes={{ selected: classes.statusMenuItemSelected }}
                          onClick={event => this.handleOperations(event, value)}>
                          <ListItemText
                            primary={
                              <FormattedMessage
                                id={this.getTranslatedId(value)}
                                defaultMessage={value}
                              />
                            }
                            classes={{
                              primary: classes.statusItemText,
                            }}
                          />
                        </ListItem>
                      ) : null
                    ) : value == "Cancel Meeting" ? ( //organizerId
                      // this.props.meeting.organizerId !== null ()
                      this.props.meeting.status !== "Cancelled" &&
                        this.props.meeting.status !== "Published" &&
                        this.props.meeting.status !== "InReview" ? (
                        <ListItem
                          key={value}
                          button
                          disableRipple={true}
                          classes={{ selected: classes.statusMenuItemSelected }}
                          onClick={event => this.handleOperations(event, value)}>
                          <ListItemText
                            primary={
                              <FormattedMessage
                                id={this.getTranslatedId(value)}
                                defaultMessage={value}
                              />
                            }
                            classes={{
                              primary: classes.statusItemText,
                            }}
                          />
                        </ListItem>
                      ) : null
                    ) : value == "Delete Meeting Schedule" ? (
                      this.props.meeting.status !== "Published" &&
                        this.props.meeting.status !== "InReview" &&
                        this.props.meeting.status !== "Cancelled" ? (
                        <ListItem
                          key={value}
                          button
                          disableRipple={true}
                          classes={{ selected: classes.statusMenuItemSelected }}
                          onClick={event => this.handleOperations(event, value)}>
                          <ListItemText
                            primary={
                              <FormattedMessage
                                id={this.getTranslatedId(value)}
                                defaultMessage={value}
                              />
                            }
                            classes={{
                              primary: classes.statusItemText,
                            }}
                          />
                        </ListItem>
                      ) : null
                    ) : value == "Cancel Meeting Schedule" ? ( //organizerId
                      // this.props.meeting.organizerId !== null ()
                      this.props.meeting.status !== "Published" &&
                        this.props.meeting.status !== "InReview" &&
                        this.props.meeting.status !== "Cancelled" ? (
                        <ListItem
                          key={value}
                          button
                          disableRipple={true}
                          classes={{ selected: classes.statusMenuItemSelected }}
                          onClick={event => this.handleOperations(event, value)}>
                          <ListItemText
                            primary={
                              <FormattedMessage
                                id={this.getTranslatedId(value)}
                                defaultMessage={value}
                              />
                            }
                            classes={{
                              primary: classes.statusItemText,
                            }}
                          />
                        </ListItem>
                      ) : null
                    ) : (
                      <ListItem
                        key={value}
                        button
                        disableRipple={true}
                        classes={{ selected: classes.statusMenuItemSelected }}
                        onClick={event => this.handleOperations(event, value)}>
                        <ListItemText
                          primary={
                            <FormattedMessage id={this.getTranslatedId(value)} defaultMessage={value} />
                          }
                          classes={{
                            primary: classes.statusItemText,
                          }}
                        />
                      </ListItem>
                    )
                  )}
                </List>
              }
            />
          </div>
        </ClickAwayListener>
        <React.Fragment>
          <ActionConfirmation
            open={archiveFlag}
            closeAction={this.handleDialogClose}
            cancelBtnText={
              <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
            }
            successBtnText={
              <FormattedMessage
                id="common.archived.meeting.archived-meeting-label"
                defaultMessage="Archive Meeting"
              />
            }
            alignment="center"
            headingText={
              <FormattedMessage
                id="common.action.archive.confirmation.archive-button.label"
                defaultMessage="Archive"
              />
            }
            iconType="archive"
            msgText={
              <FormattedMessage
                id="common.archived.meeting.messagec"
                defaultMessage="Are you sure you want to archive this meeting?"
              />
            }
            successAction={this.handleArchive}
            btnQuery={archiveBtnQuery}
          />
          <ActionConfirmation
            open={archiveScheduleFlag}
            closeAction={this.handleDialogClose}
            cancelBtnText={
              <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
            }
            successBtnText={
              <FormattedMessage
                id="common.archived.meeting.messaged"
                defaultMessage="Archive Schedule"
              />
            }
            alignment="center"
            headingText={
              <FormattedMessage
                id="common.action.archive.confirmation.archive-button.label"
                defaultMessage="Archive"
              />
            }
            iconType="archive"
            msgText={
              <FormattedMessage
                id="common.archived.meeting.messagee"
                defaultMessage="Are you sure you want to archive this meeting schedule?"
              />
            }
            successAction={e => this.handleArchive(e, true)}
            btnQuery={archiveScheduleBtnQuery}
          />
        </React.Fragment>
        <React.Fragment>
          <ActionConfirmation
            open={unArchiveFlag}
            closeAction={this.handleDialogClose}
            cancelBtnText={
              <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
            }
            successBtnText={
              <FormattedMessage
                id="common.un-archived.meetings.label"
                defaultMessage="Unarchive Meetings"
              />
            }
            alignment="center"
            iconType="unarchive"
            headingText={
              <FormattedMessage
                id="common.action.un-archive.confirmation.title"
                defaultMessage="Unarchive"
              />
            }
            msgText={
              <FormattedMessage
                id="common.un-archived.meetings.messagec"
                defaultMessage="Are you sure you want to unarchive this meeting?"
              />
            }
            successAction={this.handleUnArchive}
            btnQuery={unarchiveBtnQuery}
          />
          <ActionConfirmation
            open={unArchiveScheduleFlag}
            closeAction={this.handleDialogClose}
            cancelBtnText={
              <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
            }
            successBtnText={
              <FormattedMessage
                id="common.un-archived.meetings.messaged"
                defaultMessage="Unarchive Schedule"
              />
            }
            alignment="center"
            iconType="unarchive"
            headingText={
              <FormattedMessage
                id="common.action.un-archive.confirmation.title"
                defaultMessage="Unarchive"
              />
            }
            msgText={
              <FormattedMessage
                id="common.un-archived.meetings.messagee"
                defaultMessage="Are you sure you want to unarchive this meeting schedule?"
              />
            }
            successAction={e => this.handleUnArchive(e, true)}
            btnQuery={unarchiveScheduledBtnQuery}
          />
        </React.Fragment>

        <React.Fragment>
          <DeleteConfirmDialog
            open={deleteFlag}
            closeAction={this.handleDialogClose}
            cancelBtnText={
              <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
            }
            successBtnText={
              <FormattedMessage
                id="common.action.delete.meeting.messageb"
                defaultMessage="Delete meeting"
              />
            }
            alignment="center"
            headingText={
              <FormattedMessage
                id="common.action.delete.meeting.messageb"
                defaultMessage="Delete meeting"
              />
            }
            successAction={this.deleteMeeting}
            msgText={
              <FormattedMessage
                id="common.action.delete.meeting.message"
                defaultMessage="Are you sure you want to delete this meetings?"
              />
            }
            btnQuery={deleteMeetingBtnQuery}
          />

          <DeleteConfirmDialog
            open={deleteScheduleFlag}
            closeAction={this.handleDialogClose}
            cancelBtnText={
              <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
            }
            successBtnText={
              <FormattedMessage
                id="common.action.delete.meeting.messagec"
                defaultMessage="Delete Schedule"
              />
            }
            alignment="center"
            headingText={
              <FormattedMessage
                id="common.action.delete.meeting.messagec"
                defaultMessage="Delete Schedule"
              />
            }
            successAction={this.deleteMeetingSchedule}
            msgText={
              <FormattedMessage
                id="common.action.delete.meeting.messaged"
                defaultMessage="Are you sure you want to delete this meeting schedule?"
              />
            }
            btnQuery={deleteScheduleMeetingBtnQuery}
          />
        </React.Fragment>

        <React.Fragment>
          <DeleteConfirmDialog
            open={cancelFlag}
            closeAction={this.handleDialogClose}
            cancelBtnText={
              <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
            }
            successBtnText={
              <FormattedMessage
                id="common.action.cancel.meeting.label"
                defaultMessage="Cancel Meeting"
              />
            }
            alignment="center"
            headingText={
              <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
            }
            iconType="archive"
            msgText={
              <FormattedMessage
                id="common.action.cancel.meeting.messageg"
                defaultMessage="Are you sure you want to cancel this meeting?"
              />
            }
            successAction={this.handleCancelMeeting}
            btnQuery={cancelBtnQuery}
          />

          <DeleteConfirmDialog
            open={cancelScheduleFlag}
            closeAction={this.handleDialogClose}
            cancelBtnText={
              <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
            }
            successBtnText={
              <FormattedMessage
                id="common.action.cancel.meeting.messagee"
                defaultMessage="Cancel schedule"
              />
            }
            alignment="center"
            headingText={
              <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
            }
            iconType="archive"
            msgText={
              <FormattedMessage
                id="common.action.cancel.meeting.messagef"
                defaultMessage="Are you sure you want to cancel this meeting schedule?"
              />
            }
            successAction={e => this.handleCancelMeeting(e, true)}
            btnQuery={cancelBtnQuery}
          />
        </React.Fragment>
      </Fragment>
    );
  }
}

export default withStyles(combineStyles(itemStyles, menuStyles), {
  withTheme: true,
})(MeetingActionDropDown);
