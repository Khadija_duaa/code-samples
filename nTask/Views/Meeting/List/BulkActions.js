import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button/";
import listStyles from "./meetingList.style";
import IconMenu from "../../../components/Menu/TaskMenus/IconMenu";
import MenuList from "../../../components/Menu/TaskMenus/MemberListMenu";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Popper from "@material-ui/core/Popper";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import helper from "../../../helper";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import DeleteConfirmDialog from "../../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import Typography from "@material-ui/core/Typography";
import ColorPicker from "../../../components/Dropdown/ColorPicker/ColorPicker";
import { compose } from "redux";
import { FormattedMessage, injectIntl } from "react-intl";

class BulkActions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDateOpen: false,
      dueDateOpen: false,
      selectedColor: "#D9E3F0",
      anchorEl: null,
      open: false,
      showConfirmation: false,
      buttonName: "",
      message: "",
      inputType: "",
      prevColor: "#D9E3F0",
      deleteMeeting: false,
      archiveBtnQuery: "",
      unarchiveBtnQuery: "",
      statusBtnQuery: "",
    };

    this.handleStartDateClose = this.handleStartDateClose.bind(this);
    this.handleStartDateToggle = this.handleStartDateToggle.bind(this);
    this.handleDueDateClose = this.handleDueDateClose.bind(this);
    this.handleDueDateToggle = this.handleDueDateToggle.bind(this);
    this.handleColorPickerBtnClick = this.handleColorPickerBtnClick.bind(this);
    this.handleClickAway = this.handleClickAway.bind(this);
    this.showConfirmationPopup = this.showConfirmationPopup.bind(this);
    this.closeConfirmationPopup = this.closeConfirmationPopup.bind(this);
  }
  closeDialog = (name) => {
    this.setState({ [name]: false });
  };
  openDialog = (event, name) => {
    this.setState({ [name]: true });
  };
  bulkDeleteMeeting = () => {
    const { selectedIdsList, selectedMeetingObj, permissionObject, workspaceMeetingPer } = this.props;

    let filterMeetingHaveDeletePer = selectedMeetingObj.reduce((res, cv) => { /** filter those meetings which user have the permission to delete */
      if (cv.projectId) {
        if (permissionObject[cv.projectId].meeting.deleteMeeting.cando) res.push(cv);
      } else if (workspaceMeetingPer.deleteMeeting.cando) {
        res.push(cv);
      }
      return res;
    }, []);

    //Generating array of meetingIds
    let meetingIdsArr = filterMeetingHaveDeletePer.map((r) => {
      return r.row.meetingId;
    });

    const type = helper.MEETING_BULK_TYPES("Delete");
    let data = {
      type: type,
      meetingIds: meetingIdsArr,
    };
    this.setState({ btnQuery: "progress" });
    this.props.BulkUpdate(data, () => {
      this.setState({ btnQuery: "" }, () => {
        this.setState({ deleteMeeting: false, showConfirmation: false });
      });
    });
  };
  showConfirmationPopup(inputType) {
    this.setState({
      showConfirmation: true,
      inputType: inputType || "",
    });
  }

  closeConfirmationPopup() {
    this.setState({ showConfirmation: false });
  }

  handleBulkStatusClick = () => {
    let inputType = this.state.inputType;
    const type = helper.MEETING_BULK_TYPES(inputType);
    let data = {
      type: type || inputType,
      meetingIds: this.props.selectedIdsList,
      status: "Cancelled",
    };
    this.setState({ statusBtnQuery: "progress" }, () => {
      this.props.BulkUpdate(data, () => {
        this.setState({
          statusBtnQuery: "",
          inputType: "",
          showConfirmation: false,
        });
      });
    });
  };
  handleStartDateClose(event) {
    this.setState({ startDateOpen: false });
  }
  handleStartDateToggle() {
    this.setState((state) => ({ startDateOpen: !state.startDateOpen }));
  }
  handleDueDateClose(event) {
    this.setState({ dueDateOpen: false });
  }
  handleDueDateToggle() {
    this.setState((state) => ({ dueDateOpen: !state.dueDateOpen }));
  }
  handleColorPickerBtnClick(event) {
    const { currentTarget } = event;
    this.setState((state) => ({
      anchorEl: currentTarget,
      open: !state.open,
    }));
  }
  handleColorChange = (color) => {
    this.setState({ selectedColor: color }, () => {
      if (this.state.selectedColor !== this.state.prevColor) {
        const type = helper.MEETING_BULK_TYPES("Color");
        let data = {
          type,
        };
        data["colorCode"] = this.state.selectedColor;
        data.meetingIds = this.props.selectedIdsList;
        this.props.BulkUpdate(data);
        this.setState({ prevColor: this.state.selectedColor });
      }
    });
  };
  handleClickAway = () => {
    this.setState({ open: false });
  };
  handleArchive = (e) => {
    if (e) e.stopPropagation();
    let inputType = this.state.inputType;
    const type = helper.MEETING_BULK_TYPES(inputType);
    let data = {
      type: type || inputType,
      meetingIds: this.props.selectedIdsList,
    };
    this.setState({ archiveBtnQuery: "progress" }, () => {
      this.props.BulkUpdate(data, () => {
        this.setState({
          archiveBtnQuery: "",
          inputType: "",
          showConfirmation: false,
        });
      });
    });
  };
  handleUnArchive = (e) => {
    if (e) e.stopPropagation();
    let inputType = this.state.inputType;
    const type = helper.MEETING_BULK_TYPES(inputType);
    let data = {
      type: type || inputType,
      meetingIds: this.props.selectedIdsList,
    };
    this.setState({ unarchiveBtnQuery: "progress" }, () => {
      this.props.BulkUpdate(data, () => {
        this.setState({
          unarchiveBtnQuery: "",
          inputType: "",
          showConfirmation: false,
        });
      });
    });
  };
  getTranslatedId(value) {
    switch (value) {
      case "Color":
        value = "common.action.color.label";
        break;
      case "Archive":
        value = "common.action.archive.confirmation.archive-button.label";
        break;
      case "Delete":
        value = "common.action.delete.confirmation.delete-button.label";
        break;
      case "UnArchive":
        value = "common.action.un-archive.confirmation.title";
        break;
      case "Add Participants":
        value = "common.bulk-action.addParticipant";
        break;
      case "Cancel":
        value = "common.action.cancel.label";
        break;
    }
    return value;
  }
  render() {
    const {
      classes,
      theme,
      selectedIdsList,
      isArchivedSelected,
      workspaceMeetingPer,
      selectedMeetingObj,
      intl
    } = this.props;
    const {
      dueDateOpen,
      startDateOpen,
      selectedColor,
      anchorEl,
      open,
      showConfirmation,
      inputType,
      btnQuery,
      deleteMeeting,
      archiveBtnQuery,
      unarchiveBtnQuery,
      statusBtnQuery,
    } = this.state;

    const id = open ? "simple-popper" : null;
    const styles = (action, i, arr) => {
      return i == 0
        ? { borderRadius: "4px 0 0 4px" }
        : i == arr.length - 1
          ? { borderRadius: "0 4px 4px 0", borderLeft: "none" }
          : { borderLeft: "none", borderRadius: 0 };
    };
    const BulkActionsType = isArchivedSelected
      ? [selectedIdsList.length + " " + intl.formatMessage({ id: "common.item-selected.label", defaultMessage: "items Selected" }), "Unarchive", "Delete"]
      : [
        selectedIdsList.length + " " + intl.formatMessage({ id: "common.item-selected.label", defaultMessage: "items Selected" }),
        "Color",
        "Add Participants",
        "Cancel",
        "Archive",
        "Delete",
      ];

    let filterMeetingHaveDeletePer = selectedMeetingObj.reduce((res, cv) => { /** filter those meetings which user have not the permission to delete */
      if (cv.projectId) {
        if (!permissionObject[cv.projectId].meeting.deleteMeeting.cando) res.push(cv);
      } else if (!workspaceMeetingPer.deleteMeeting.cando) {
        res.push(cv);
      }
      return res;
    }, []);

    return (
      <div className={classes.BulkActionsCnt}>
        {inputType === "Archive" ? (
          <ActionConfirmation
            open={showConfirmation && workspaceMeetingPer.archiveMeetings.cando}
            closeAction={this.closeConfirmationPopup}
            cancelBtnText={
              <FormattedMessage
                id="common.action.cancel.label"
                defaultMessage="Cancel"
              />
            }
            successBtnText={
              <FormattedMessage
                id="common.archived.meeting.label2"
                defaultMessage="Archive Meetings"
              />
            }
            alignment="center"
            headingText={
              <FormattedMessage
                id="common.action.archive.confirmation.archive-button.label"
                defaultMessage="Archive"
              />
            }
            iconType="archive"
            msgText={
              <FormattedMessage
                id="common.archived.meeting.messagea"
                defaultMessage={`Are you sure you want to archive these ${selectedIdsList.length} meetings?`}
                values={{ m: selectedIdsList.length }}
              />
            }
            successAction={this.handleArchive}
            btnQuery={archiveBtnQuery}
          />
        ) : inputType === "Unarchive" ? (
          <ActionConfirmation
            open={showConfirmation && workspaceMeetingPer.unarchiveMeetings.cando}
            closeAction={this.closeConfirmationPopup}
            cancelBtnText={
              <FormattedMessage
                id="common.action.cancel.label"
                defaultMessage="Cancel"
              />
            }
            successBtnText={
              <FormattedMessage
                id="common.un-archived.meetings.label"
                defaultMessage="Unarchive Meetings"
              />
            }
            alignment="center"
            iconType="unarchive"
            headingText={
              <FormattedMessage
                id="common.action.un-archive.confirmation.title"
                defaultMessage="Unarchive"
              />
            }
            msgText={
              <FormattedMessage
                id="common.un-archived.meetings.messagea"
                defaultMessage={`Are you sure you want to unarchive these ${selectedIdsList.length} meetings?`}
                values={{ m: selectedIdsList.length }}
              />
            }
            successAction={this.handleUnArchive}
            btnQuery={unarchiveBtnQuery}
          />
        ) : inputType === "Status" ? (
          <ActionConfirmation
            open={showConfirmation && workspaceMeetingPer.cancelMeeting.cando}
            closeAction={this.closeConfirmationPopup}
            cancelBtnText={
              <FormattedMessage
                id="common.action.cancel.label"
                defaultMessage="Cancel"
              />
            }
            successBtnText={
              <FormattedMessage
                id="Cancel Meetings"
                defaultMessage="Cancel Meetings"
              />
            }
            alignment="center"
            iconType="unarchive"
            headingText={
              <FormattedMessage
                id="common.action.cancel.label"
                defaultMessage="Cancel"
              />
            }
            msgText={
              <FormattedMessage
                id="common.action.cancel.meeting.message"
                defaultMessage={`Are you sure you want to cancel these ${selectedIdsList.length} meetings?`}
                values={{ m: selectedIdsList.length }}
              />
            }
            successAction={this.handleBulkStatusClick}
            btnQuery={statusBtnQuery}
          />
        ) : inputType === "Delete" ? (
          <DeleteConfirmDialog
            open={showConfirmation && workspaceMeetingPer.deleteMeeting.cando}
            // closeAction={() => { this.closeDialog("deleteMeeting") }}
            closeAction={this.closeConfirmationPopup}
            cancelBtnText={
              <FormattedMessage
                id="common.action.cancel.label"
                defaultMessage="Cancel"
              />
            }
            successBtnText={
              <FormattedMessage
                id="common.action.delete.confirmation.delete-button.label"
                defaultMessage="Delete"
              />
            }
            alignment="center"
            headingText={
              <FormattedMessage
                id="common.action.delete.confirmation.delete-button.label"
                defaultMessage="Delete"
              />
            }
            successAction={this.bulkDeleteMeeting}
            btnQuery={btnQuery}
            disabled={
              selectedIdsList.length - filterMeetingHaveDeletePer.length == 0
            }
          >
            <>
              {filterMeetingHaveDeletePer.length > 0 && (
                <Typography
                  variant="h5"
                  style={{
                    userSelect: "none",
                    marginBottom: 20,
                    // textAlign: "center",
                  }}
                >
                  <FormattedMessage
                    id="common.action.delete.meeting.label"
                    defaultMessage={`Are you sure you want to delete these ${selectedIdsList.length} meetings?`}
                    values={{ m: selectedIdsList.length }}
                  />
                </Typography>
              )}
              <Typography variant="h5" style={{ userSelect: "none", width: '100%' }}>
                <FormattedMessage id="common.action.delete.meeting.label" values={{
                  m: selectedIdsList.length -
                    filterMeetingHaveDeletePer.length
                }} defaultMessage={`Are you sure you want to delete these ${selectedIdsList.length -
                  filterMeetingHaveDeletePer.length} meetings?`} />
              </Typography>
            </>
          </DeleteConfirmDialog>
        ) : null}

        {BulkActionsType.map((action, i, arr) => {
          return i == 1 && !isArchivedSelected ? (
            <>
              <Button
                disabled={i == 0 ? true : false}
                style={styles(action, i, arr)}
                variant="outlined"
                onClick={this.handleColorPickerBtnClick}
                classes={{ outlined: classes.BulkActionBtn }}
                selectedIdsList={this.state.selectedIds}
              >
                <FormattedMessage
                  id={this.getTranslatedId(action)}
                  defaultMessage={action}
                />
              </Button>
              <Popper
                id={id}
                open={open}
                anchorEl={anchorEl}
                disablePortal
                transition
              >
                {({ TransitionProps, placement }) => (
                  <Grow
                    {...TransitionProps}
                    id="menu-list-grow"
                    style={{
                      transformOrigin:
                        placement === "bottom" ? "center top" : "left bottom",
                    }}
                  >
                    <ClickAwayListener onClickAway={this.handleClickAway}>
                      <div id="colorPickerCnt">
                        <ColorPicker
                          triangle="hide"
                          onColorChange={(color) => {
                            this.handleColorChange(color);
                          }}
                          selectedColor={selectedColor}
                        />
                      </div>
                    </ClickAwayListener>
                  </Grow>
                )}
              </Popper>
            </>
          ) : i == 2 &&
            !isArchivedSelected &&
            workspaceMeetingPer.meetingDetail.meetingAttendee.isAllowAdd ? (
            <MenuList
              assigneeList={[]}
              placementType="bottom-end"
              selectedIdsList={this.props.selectedIdsList}
              BulkUpdate={this.props.BulkUpdate}
              viewType="meeting"
              buttonStyles={styles(action, i, arr)}
              checkAssignee={true}
              checkedType="multi"
              listType="assignee"
              buttonText={"Add Participants"}
              isBulk={true}
              keyType="Participant"
              showDateTimeDetailsPopUp={() => { }}
            />
          ) : i == 3 && !isArchivedSelected ? (
            <Button
              disabled={i == 0 ? true : false}
              style={styles(action, i, arr)}
              variant="outlined"
              classes={{ outlined: classes.BulkActionBtn }}
              onClick={this.showConfirmationPopup.bind(this, "Status")}
            >
              <FormattedMessage
                id={this.getTranslatedId(action)}
                defaultMessage={action}
              />
            </Button>
          ) : i == 4 && !isArchivedSelected ? (
            <Button
              disabled={i == 0 ? true : false}
              style={styles(action, i, arr)}
              variant="outlined"
              classes={{ outlined: classes.BulkActionBtn }}
              onClick={this.showConfirmationPopup.bind(this, action)}
            >
              <FormattedMessage
                id={this.getTranslatedId(action)}
                defaultMessage={action}
              />
            </Button>
          ) : i == 5 && !isArchivedSelected ? (
            <Button
              disabled={i == 0 ? true : false}
              style={styles(action, i, arr)}
              variant="outlined"
              classes={{ outlined: classes.BulkActionBtn }}
              onClick={this.showConfirmationPopup.bind(this, "Delete")}
            // onClick={event => this.openDialog(event, "deleteMeeting")}
            >
              <FormattedMessage
                id={this.getTranslatedId(action)}
                defaultMessage={action}
              />
            </Button>
          ) : (
            <Button
              disabled={i == 0 ? true : false}
              style={styles(action, i, arr)}
              variant="outlined"
              classes={{ outlined: classes.BulkActionBtn }}
              onClick={this.showConfirmationPopup.bind(this, action)}
            >
              <FormattedMessage
                id={this.getTranslatedId(action)}
                defaultMessage={action}
              />
            </Button>
          );
        })}
      </div>
    );
  }
}

export default compose(injectIntl, withStyles(listStyles, { withTheme: true }))(BulkActions);
