import React from "react";
import moment from "moment";
import { store } from "../../../index";
import { generateNumberValue } from "../../../helper/generateNumberFieldData";

import helper from "../../../helper";
import isEmpty from "lodash/isEmpty";
import { handleCalculateValue } from "../../../helper/listView/listView.helper";
let createdDateValueGetter = function (params) {
  return params.data && params.data.createdDate
    ? moment(params.data.createdDate).format("MMM DD, YYYY")
    : "";
};
let updatedDateValueGetter = function (params) {
  return params.data && params.data.updatedDate
    ? moment(params.data.updatedDate).format("MMM DD, YYYY")
    : "";
};
let startDateValueGetter = function (params) {
  return params.data && params.data.startDate
    ? moment(params.data.startDate).format("MMM DD, YYYY")
    : "";
};
let startTimeValueGetter = function (params) {
  return params.data && params.data.startTime
    ? params.data.startTime
    : "";
};
let projectValueGetter = function (params) {
  return params.data && params.data.projectId
    ? params.data.projects?.projectName || ""
    : "";
};
let taskValueGetter = function (params) {
  return params.data && params.data.tasks
    ? params.data.tasks.taskTitle
    : "";
};
let locationValueGetter = function (params) {
  return params.data && !isEmpty(params.data.meetingLocation)
    ? params.data.meetingLocation
    : "-";
};

let numberFieldGetter = function (params) {
  const customFields = store.getState().customFields.data;
  if (!params.data) return "";
  let numberField = customFields.find(item => item.fieldId === params.colDef.fieldId);
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (numberField && rowData) {
    // value = `${
    //   numberField.settings.direction === "left" && numberField.settings.unit !== "none"
    //     ? numberField.settings.unit
    //     : ""
    // } ${generateNumberValue(rowData.fieldData.data, numberField)} ${
    //   numberField.settings.direction === "right" && numberField.settings.unit !== "none"
    //     ? numberField.settings.unit
    //     : ""
    // }`;
    value = {
      unit: numberField.settings.unit,
      direction: numberField.settings.direction,
      number: generateNumberValue(rowData.fieldData.data, numberField),
    };
  }
  return value;
};
let numberFieldKeyCreator = function (params) {
  const customFields = store.getState().customFields.data;
  if (!params.data) return "";
  let numberField = customFields.find(item => item.fieldId === params.colDef.fieldId);
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (numberField && rowData) {
    value = `${numberField.settings.direction === "left" && numberField.settings.unit !== "none"
      ? numberField.settings.unit
      : ""
      } ${generateNumberValue(rowData.fieldData.data, numberField)} ${numberField.settings.direction === "right" && numberField.settings.unit !== "none"
        ? numberField.settings.unit
        : ""
      }`;

  }
  return value;
};

let moneyFieldGetter = function (params) {
  if (!params.data) return "";
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (rowData) {
    value = `${rowData.fieldData.data} `;
  }
  return value;
};

let countryFieldGetter = function (params) {
  if (!params.data) return "";
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (rowData) {
    value = rowData.fieldData.data;
  }
  return value;
};

let ratingFieldGetter = function (params) {
  if (!params.data) return "";
  const customFields = store.getState().customFields.data;
  let ratingField = customFields.find(item => item.fieldId === params.colDef.fieldId);
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (rowData) {
    for (let i = 0; i < rowData.fieldData.data; i++) {
      value = `${value} ${ratingField.settings.emoji}`;
    }
  }
  return value;
};

let websiteurlFieldGetter = function (params) {
  if (!params.data) return "";
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (rowData) {
    value = rowData.fieldData.data;
  }
  return value;
};

let emailFieldGetter = function (params) {
  if (!params.data) return "";
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (rowData) {
    value = rowData.fieldData.data;
  }
  return value;
};

let phoneFieldGetter = function (params) {
  if (!params.data) return "";
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (rowData) {
    value = rowData.fieldData.data;
  }
  return value;
};

let fileMediaFieldGetter = function (params) {
  if (!params.data) return "";
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (rowData) {
    value = rowData.fieldData.data.map(item => item.fileName).join(",");
  }
  return value;
};
let dateValueGetter = function (params) {
  if (!params.data) return "";
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (rowData) {
    value = moment(rowData.fieldData.data.date).format("lll");
  }
  return value;
};

let textFieldGetter = function (params) {
  if (!params.data) return "";
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (rowData) {
    value = rowData.fieldData.data;
  }
  return value;
};

let dropdownFieldGetter = function (params) {
  if (!params.data) return "";
  let value = "";
  if (params.data) {
    const customFields = store.getState().customFields.data;
    let dropdownField = customFields.find(item => item.fieldId === params.colDef.fieldId);
    let rowData = params.data.customFieldData
      ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
      : null;
    if (rowData && !dropdownField.settings.multiSelect) {
      let dataValue =
        dropdownField.values.data.find(opt => opt.id === rowData.fieldData.data.id) || {};
      value = dataValue.value || "";
    }
    if (rowData && dropdownField.settings.multiSelect) {
      value = rowData.fieldData.data.map(item => item.value).join(",");
    }
  }
  return value;
};
let peopleFieldGetter = function (params) {
  if (!params.data) return "";
  let value = "";
  if (params.data) {
    const customFields = store.getState().customFields.data;
    const allMembers = store.getState().profile.data.member.allMembers;
    let peopleField = customFields.find(item => item.fieldId === params.colDef.fieldId);
    let rowData = params.data.customFieldData
      ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
      : null;
    if (rowData && !peopleField.settings.multiplePeople) {
      let contentValue = rowData.fieldData.data
        ? allMembers.find(m => rowData.fieldData.data.includes(m.userId)) || {}
        : "";
      value = contentValue.fullName || "";
    }
  }
  return value;
};
let formulaFieldGetter = function (params) {
  if (!params.data) return "";
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (rowData) {
    value = value = handleCalculateValue(params.data.customFieldData, rowData.fieldData.data);
  }
  return value;
};

const durationValueGetter = (params) => {
  return `${params.data.durationHours}h ${params.data.durationMins}m`;
}
const meetingColumnDefs = classes => ({
  uniqueId: {
    sortable: true,
    filter: true,
    minWidth: 100,
    resizable: true,
  },
  timeDuration: {
    sortable: true,
    filter: true,
    minWidth: 100,
    resizable: true,
    valueGetter: durationValueGetter
  },
  meetingDisplayName: {
    sortable: true,
    filter: true,
    resizable: true,
    width: 150,
    checkboxSelection: true,
    cellRenderer: "meetingDisplayName",
    cellClass: classes.taskTitleCell,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    rowDrag: true,
  },
  status: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 150,
    cellRenderer: "statusDropdown",
    cellClass: classes.fullSpannedCell,
    valueGetter: "data.status",
    comparator: sortStatus,
  },
  startDate: {
    sortable: true,
    filter: true,
    minWidth: 120,
    resizable: true,
    comparator: dateComparator,
    valueGetter: startDateValueGetter,
    cellRenderer: "startDate",
  },
  startTime: {
    sortable: true,
    filter: true,
    minWidth: 100,
    resizable: true,
    cellRenderer: "startTime",
    comparator: timeComparator,
    valueGetter: startTimeValueGetter,
  },
  projects: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 180,
    valueGetter: projectValueGetter,
    cellRenderer: "meetingProject",
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    comparator: sortProject,
  },
  tasks: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 180,
    valueGetter: taskValueGetter,
    cellRenderer: "taskRenderer",
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    comparator: sortTask,
  },
  progress: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 130,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "progressRenderer",
  },
  meetingLocation: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 200,
    cellRenderer: "meetingLocationCmp",
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    valueGetter: locationValueGetter,
    comparator: sortAlphabetically,
  },
  attendee: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 200,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "attendee",
  },
  createdDate: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 180,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "createdDateRenderer",
    comparator: dateComparator,
    valueGetter: createdDateValueGetter,
  },
  updatedDate: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 180,
    cellRenderer: "updatedDateCmp",
    comparator: dateComparator,
    valueGetter: updatedDateValueGetter,
  },
  createdBy: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 180,
    cellRenderer: "createdByRenderer",
    pinnedRowCellRenderer: "customPinnedRowRenderer",
  },
  updatedBy: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 180,
    cellRenderer: "updatedByRenderer",
    pinnedRowCellRenderer: "customPinnedRowRenderer",
  },
  // custom fields starts here
  textfield: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    valueGetter: textFieldGetter,
    cellClass: "ag-textAlignLeft",
    cellRenderer: "textfield",
    align: "left",
  },
  textarea: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "textarea",
    cellClass: "ag-textAlignLeft",
    valueGetter: "data.customFieldData",
  },
  location: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "location",
    valueGetter: "data.customFieldData",
    cellClass: "ag-textAlignLeft",
  },
  country: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "country",
    valueGetter: countryFieldGetter,
    cellClass: "ag-textAlignLeft",
    comparator: sortAlphabetically
  },
  number: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "number",
    comparator: sortNumber,
    valueGetter: numberFieldGetter,
    keyCreator: numberFieldKeyCreator,
    cellClass: "ag-textAlignLeft",
  },
  money: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "money",
    comparator: sortMoney,
    valueGetter: moneyFieldGetter,
    cellClass: "ag-textAlignLeft",
  },
  email: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 220,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "email",
    comparator: sortAlphabetically,
    valueGetter: emailFieldGetter,
    cellClass: "ag-textAlignLeft",
  },
  websiteurl: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "websiteurl",
    valueGetter: websiteurlFieldGetter,
    comparator: sortAlphabetically,
    cellClass: "ag-textAlignLeft",
  },
  date: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 150,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "date",
    comparator: dateComparator,
    valueGetter: dateValueGetter,
    cellClass: "ag-textAlignLeft",
  },
  phone: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "phone",
    valueGetter: phoneFieldGetter,
    cellClass: "ag-textAlignLeft",
  },
  rating: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 200,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "rating",
    valueGetter: ratingFieldGetter,
  },
  formula: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "formula",
    comparator: sortFormula,
    valueGetter: formulaFieldGetter,
    cellClass: "ag-textAlignLeft",
  },
  people: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "people",
    valueGetter: peopleFieldGetter,
    comparator: sortAlphabetically,
  },
  dropdown: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "dropdown",
    valueGetter: dropdownFieldGetter,
    comparator: sortAlphabetically,
    cellClass: `${classes.fullSpannedCell} ag-textAlignLeft`,
  },
  filesAndMedia: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "filesAndMedia",
    valueGetter: fileMediaFieldGetter,
    comparator: sortAlphabetically,
    cellClass: "ag-textAlignLeft",
  },
});

function dateComparator(date1, date2) {
  const newDate = date1 ? new Date(date1) : "";
  const prevDate = date2 ? new Date(date2) : "";
  if (newDate == prevDate) return 0;

  return newDate - prevDate;
}
function timeComparator(time1, time2) {
  const newTime = time1 ? moment(time1, 'HH:mm: A').diff(moment().startOf('day'), 'seconds') : "";
  const prevTime = time2 ? moment(time2, 'HH:mm: A').diff(moment().startOf('day'), 'seconds') : "";
  if (newTime == prevTime) return 0;
  return newTime - prevTime;
}

function sortProject(prevProject, nextProject, prevRow, nextRow) {
  const prevProjectName = prevProject.toLowerCase();
  const nextProjectName = nextProject.toLowerCase();
  if (prevProjectName == nextProjectName) return 0;
  if (prevProjectName < nextProjectName) return -1;
  if (prevProjectName > nextProjectName) return 1;
}
function sortTask(prevTask, nextTask, prevRow, nextRow) {
  const prevTaskName = prevTask.toLowerCase();
  const nextTaskName = nextTask.toLowerCase();
  if (prevTaskName == nextTaskName) return 0;
  if (prevTaskName < nextTaskName) return -1;
  if (prevTaskName > nextTaskName) return 1;
}

function sortStatus(prevStatus, nextStatus, prevRow, nextRow) {
  const prevStatusName = prevRow.data ? prevRow.data.status : "";
  const nextStatusName = nextRow.data ? nextRow.data.status : "";
  if (prevStatusName == nextStatusName) return 0;
  if (prevStatusName < nextStatusName) return -1;
  if (prevStatusName > nextStatusName) return 1;
}

function sortNumber(nextObj, prevObj, nextRow, prevRow) {
  const prevNumberVal = prevObj.number ? parseFloat(prevObj.number) : "";
  const nextNumberVal = nextObj.number ? parseFloat(nextObj.number) : "";
  if (nextNumberVal == prevNumberVal) return 0;
  if (nextNumberVal < prevNumberVal) return -1;
  if (nextNumberVal > prevNumberVal) return 1;
}
function sortMoney(nextObj, prevObj, nextRow, prevRow) {
  const prevNumberVal = prevObj ? parseFloat(prevObj) : "";
  const nextNumberVal = nextObj ? parseFloat(nextObj) : "";
  if (nextNumberVal == prevNumberVal) return 0;
  if (nextNumberVal < prevNumberVal) return -1;
  if (nextNumberVal > prevNumberVal) return 1;
}
function sortFormula(nextObj, prevObj, nextRow, prevRow) {
  const prevNumberVal = prevObj ? parseFloat(prevObj) : "";
  const nextNumberVal = nextObj ? parseFloat(nextObj) : "";
  if (nextNumberVal == prevNumberVal) return 0;
  if (nextNumberVal < prevNumberVal) return -1;
  if (nextNumberVal > prevNumberVal) return 1;
}
export function sortAlphabetically(nextObj, prevObj, nextRow, prevRow) {
  const prevtext = prevObj ? prevObj.trim().toLowerCase() : "";
  const nextText = nextObj ? nextObj.trim().toLowerCase() : "";
  if (nextText == prevtext) return 0;
  if (nextText < prevtext) return -1;
  if (nextText > prevtext) return 1;
}

export default meetingColumnDefs;
