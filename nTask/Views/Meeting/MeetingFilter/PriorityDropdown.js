import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import ListItemText from "@material-ui/core/ListItemText";
import DoneIcon from "@material-ui/icons/Done";
import RoundIcon from "@material-ui/icons/Brightness1";
import selectStyles from "../../../assets/jss/components/select";
import Checkbox from "@material-ui/core/Checkbox";
import SelectMenu from "../../../components/Menu/SelectMenu";
import FlagIcon from "@material-ui/icons/Flag";
let object = {};
class PriorityDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: [],
      clear: false
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.isClear) {
      object = {
        name: [],
        clear: true
      };
    } else {
      object = {
        clear: false
      };
    }
    return (object)
  }

  handleChange = event => {
    this.setState({ name: event.target.value }, () => {
      this.props.handleFilterChange("priority", this.state.name);
    });
  };
  render() {
    const { classes, theme } = this.props;
    const { name, clear } = this.state;
    const statusColor = theme.palette.status;
    const names = [
      { name: "Critical", color: statusColor.completed },
      { name: "High", color: statusColor.review },
      { name: "Medium", color: statusColor.inProgress },
      { name: "Low", color: statusColor.notStarted }
    ];
    if (clear) {
      this.props.handleClearState(true);
    }
    return (
      <SelectMenu
        label="Priority"
        multiple={true}
        value={name}
        onChange={this.handleChange}
      >
        {names.map(status => (
          <MenuItem
            key={status.name}
            value={status.name}
            classes={{
              root: classes.statusMenuItemCnt,
              selected: classes.statusMenuItemSelected
            }}
          >
            <FlagIcon
              htmlColor={status.color}
              classes={{ root: classes.flagIcon }}
            />
            <ListItemText
              primary={status.name}
              classes={{ primary: classes.statusItemText }}
            />

            <Checkbox
              checkedIcon={
                <DoneIcon
                  htmlColor={theme.palette.primary.light}
            
                />
              }
              checked={this.state.name.indexOf(status.name) > -1}
              icon={false}
            />
          </MenuItem>
        ))}
      </SelectMenu>
    );
  }
}

export default withStyles(selectStyles, {
  withTheme: true
})(PriorityDropdown);
