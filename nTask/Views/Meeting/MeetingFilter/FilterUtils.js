import helper from "../../../helper";
import intersectionWith from "lodash/intersectionWith";
import findIndex from "lodash/findIndex";
import cloneDeep from "lodash/cloneDeep";
import moment from "moment";

export function getFilteredMeetings(MeetingArray, multipleFilters, meetingState = [], dow, tasks, projects, meetingPer) {
    let meeting = cloneDeep(MeetingArray) || [];
    const StatusFilters = ["Upcoming", "In Review", "Published", "Cancelled", "Overdue", "Starred"];
    const DateFilters = ["Today", "This Week", "This Month"];
    let isStatusSelected = false;
    let isDateSelected = false;
    let isDateRangeFilter = -1;

    if (meetingState && meetingState.length) {
        meetingState.forEach((item, index) => {
            if (StatusFilters.indexOf(item) >= 0)
                isStatusSelected = true;
            if (DateFilters.indexOf(item) >= 0)
                isDateSelected = true;
            if (item.dateRangeFilter)
                isDateRangeFilter = index;
        })
    }

    const isArchived = (meetingState && meetingState.length) ? meetingState.indexOf("Archived") > -1 : false;

    // Filtering Archived
    if (!isArchived) {
        const filtered = meeting.filter(x => {
            if (x.isDelete === false) {
                return x;
            }
        });
        meeting = filtered;
    }

    // meeting = meeting.map((item) => {
    //     item.createdDate = moment(item.createdDate);
    //     item.updatedDate = moment(item.updatedDate);
    //     return item;
    // })

    let flag = 0;
    let start = "",
        end = "",
        compare = false;
    if (multipleFilters) {
        let value = meeting || [];
        if (multipleFilters.participants.length > 0) {
            flag = 1;
            value = meeting.filter(x => {
                // if (intersectionWith(multipleFilters.participants, x.attendees ? x.attendees.map(x => x.attendeeId) : [], (x, y) => x.id === y).length > 0) {
                if (intersectionWith(multipleFilters.participants, x.attendees ? x.attendees.map(x => x.attendeeId) : [], (x, y) => x === y).length > 0) {
                    return x;
                }
            });
        }
        if (multipleFilters.createdBy.length > 0) {
            value = (value.length >= 0 && flag === 1) ? value : meeting;
            flag = 1;
            value = value.filter(x => {
                // if (findIndex(multipleFilters.createdBy, (o) => o.id === x.createdBy) > -1)
                if (multipleFilters.createdBy.indexOf(x.createdBy) > -1)
                    return x;
            });
        }

        if (multipleFilters.status.length > 0) {
            value = (value.length >= 0 && flag === 1) ? value : meeting;
            flag = 1;
            value = value.filter(x => {
                let meetingStatusCheck = multipleFilters.status.findIndex(item => {
                    return item == (x.status.toUpperCase() === "INREVIEW"
                        ? "In Review"
                        : x.status.toUpperCase() === "UPCOMING"
                            ? "Upcoming"
                            : x.status === "OverDue"
                                ? "Overdue"
                                : x.status);
                });
                if (meetingStatusCheck >= 0) {
                    return x;
                }
            });
        }

        if (multipleFilters.task.length > 0) {
            value = (value.length >= 0 && flag === 1) ? value : meeting;
            flag = 1;
            value = value.filter(x => {
                // if (findIndex(multipleFilters.task, (o) => o.id === x.taskId) > -1)
                if (multipleFilters.task.indexOf(x.taskId) > -1)
                    return x;
            });
        }

        if (multipleFilters.date.startDate || multipleFilters.date.endDate) {
            value = (value.length >= 0 && flag === 1) ? value : meeting;
            flag = 1;
            value = value.filter(x => {
                start = new Date(helper.RETURN_CUSTOMDATEFORMAT(
                    multipleFilters.date.startDate,
                    "12:00:00 AM"
                ));
                end = (multipleFilters.date.endDate) ?
                    new Date(helper.RETURN_CUSTOMDATEFORMAT(
                        multipleFilters.date.endDate,
                        "12:00:00 AM"
                    )) : '';
                compare = helper.CHECKDATEIFPRESENTINRANGE(
                    start,
                    end,
                    new Date(x.startDateString)
                );
                if (compare) return x;
            });
        }

        if (multipleFilters.recurrence) {
            value = (value.length >= 0 && flag === 1) ? value : meeting;
            flag = 1;
            value = value.filter(x => {
                if (x.meetingFrequency !== "NoRecurrence") {
                    return x;
                }
            });
        }
        flag = 0;
        meeting = value;
    } else if (meetingState && meetingState.length) {

        if (isStatusSelected)
            meeting = meeting.filter(x => {
                if (meetingState.indexOf((x.status && x.status.toUpperCase() === "INREVIEW") ? "In Review" :
                    x.status.toUpperCase() === "UPCOMING" ? "Upcoming" : x.status === "OverDue" ? "Overdue" : x.status) >= 0) {
                    return x;
                }
            });

        if (isDateSelected) {
            meeting = meeting.filter(x => {
                if (meetingState.indexOf("Today") >= 0) {
                    let days;
                    if (x.startDateString)
                        days = helper.RETURN_REMAINING_DAYS_DIFFERENCE(x.startDateString);
                    if (days === 0) {
                        return x;
                    }
                }
                if (meetingState.indexOf("This Week") >= 0) {
                    let startDate = helper.RETURN_CUSTOMDATEFORMAT(moment().day(dow), "12:00:00 AM"), compare = false;
                    let endDate = helper.RETURN_CUSTOMDATEFORMAT(moment().day(dow + 6), "11:59:59 PM");
                    if (x.startDateString) {
                        compare = helper.CHECKDATEIFPRESENTINRANGE(new Date(startDate), new Date(endDate), new Date(x.startDateString))
                        if (compare)
                            return x;
                    }
                }
                if (meetingState.indexOf("This Month") >= 0) {
                    let startDate = helper.RETURN_CUSTOMDATEFORMAT(moment().startOf('month'), "12:00:00 AM"), compare = false;
                    let endDate = helper.RETURN_CUSTOMDATEFORMAT(moment().endOf('month'), "11:59:59 PM");
                    if (x.startDateString) {
                        compare = helper.CHECKDATEIFPRESENTINRANGE(new Date(startDate), new Date(endDate), new Date(x.startDateString))
                        if (compare)
                            return x;
                    }
                }
            });
        }

        if (isDateRangeFilter > -1) {
            meeting = meeting.filter(x => {
                start = new Date(helper.RETURN_CUSTOMDATEFORMAT(
                    meetingState[isDateRangeFilter].dateRangeFilter.startDate,
                    "12:00:00 AM"
                ));
                end = (meetingState[isDateRangeFilter].dateRangeFilter.endDate) ?
                    new Date(helper.RETURN_CUSTOMDATEFORMAT(
                        meetingState[isDateRangeFilter].dateRangeFilter.endDate,
                        "12:00:00 AM"
                    )) : '';
                compare = helper.CHECKDATEIFPRESENTINRANGE(
                    start,
                    end,
                    new Date(x.startDateString)
                );
                if (compare) return x;
            });
        }

        // if (isArchived) {
        //     meeting = meeting.filter(x => {
        //         if (x.isDelete)
        //             return x;
        //     });
        // }
    }

    meeting = meeting ? meeting.map((item) => {
        if (item.taskId) {
            let task = tasks.filter(x => x.taskId === item.taskId);
            if (task && task.length)
                item.taskTitle = task[0].taskTitle || '';
        }
        item.timeDuration = `${item.durationHours}:${item.durationMins}`
        item.location = item.location ? item.location : null
        return item;
    }) : []

    return meeting;
}
