const repeatMeetingStyles = theme => ({
  contentCnt: {
    padding: 15
  },
  topGrayBar: {
    background: theme.palette.background.paper,
    padding: "15px",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`
  },
  repeatMeetingHeading: {
    marginBottom: 5,
    fontSize: "12px !important",
    color: theme.palette.text.light,
    textTransform: "uppercase"
  },
  toggleContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start"
  },
  toggleBtnGroup: {
    borderRadius: 4,
    background: theme.palette.common.paper,
    // border: `1px solid ${theme.palette.border.lightBorder}`,
    boxShadow: "none",
    "& $toggleButtonSelected": {
      color: theme.palette.text.secondary,
      backgroundColor: theme.palette.common.white,
      boxShadow: "none !important",
      "&:after": {
        background: theme.palette.common.white
      },
      "&:hover": {
        background: theme.palette.common.white
      }
    }
  },
  groupedHorizontal:{
    borderLeft: `1px solid ${theme.palette.border.lightBorder} !important`
  },
  toggleButton: {
    height: "auto",
    padding: "8px 20px",
    lineHeight: "normal",
    fontSize: "12px !important",
    fontWeight: theme.palette.fontWeightMedium,
    textTransform: "capitalize",
    "&:hover": {
      background: theme.palette.common.white
    },
    "&[value = 'center']": {
      borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
      borderRight: `1px solid ${theme.palette.border.lightBorder}`
    }
  },
  toggleButtonSelected: {},
    selectLabel: {
        fontSize: "12px !important",
        minWidth: 77
    },
    inputCenterLabel: {
      fontSize: "12px !important",
      marginLeft: 10,
      marginRight: 10
    },
    stopByCnt:{
      display: "flex",
      alignItems: "center",
      width: 350
    },
    intervalCnt:{
      display: "flex",
      alignItems: "center",
      margin: "0 0 20px 0"
    },
    repeatEveryCnt:{
      display: "flex",
      alignItems: "center",
      marginBottom: 15
    },
    repeatMonthEveryCnt:{
      marginBottom: 15
    },
    weekendCheckCnt: {
      borderTop: `1px solid ${theme.palette.border.lightBorder}`
    },
    daySelectCnt: {
      display: "inline-block",
      width: 240
    },
    daysSelect:{
      listStyleType: 'none',
      display: "flex",
      padding: 0,
      margin: 0,
      '& li': {
        padding: "11.5px 12px",
        border: `1px solid ${theme.palette.border.lightBorder}`,
        borderLeft: "none",
        margin: 0,
        background: theme.palette.background.items,
        cursor: "pointer",
        fontSize: "12px !important"
  
      },
      '& li:first-child': {
        borderRadius: "4px 0 0 4px",
        borderLeft: `1px solid ${theme.palette.border.lightBorder}`
      },
      '& li:last-child': {
        borderRadius: "0 4px 4px 0"
      }
    },
    monthDropdownOutCnt:{
      display: "inline-block",
      width: 260
    },
    monthDropdownCnt:{
      display: "flex",
      alignItems: "center"
    },
    advanceOptionCnt: {
      padding: "10px 0"
    },
    advanceOption:{
      textDecoration: "underline",
      cursor: "pointer"
    },
    selectLabel:{
      color: theme.palette.text.primary,
      fontSize: "12px !important",
      fontWeight: theme.typography.fontWeightLight,
      lineHeight: 1.5,
      display: "inline-block",
      minWidth: 75,
     
    },
});

export default repeatMeetingStyles;
