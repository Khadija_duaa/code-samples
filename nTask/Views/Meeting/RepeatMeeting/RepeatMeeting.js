import React, { Component } from "react";
import CustomDialog from "../../../components/Dialog/CustomDialog";
import repeatMeetingStyles from "./styles";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import ButtonActionsCnt from "../../../components/Dialog/ConfirmationDialogs/ButtonActionsCnt";
import Typography from "@material-ui/core/Typography";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import Daily from "./Daily";
import Weekly from "./Weekly";
import Monthly from "./Monthly";
import DefaultCheckbox from "../../../components/Form/Checkbox";
import { repeatMeetingInitObj, repeatMeetingOptions } from "./repeatMeetingObj";
import cloneDeep from "lodash/cloneDeep";
import {
  repeatMeeting,
  deleteMeetingSchedule,
} from "../../../redux/actions/meetings";
import NotificationMessage from "../../../components/NotificationMessages/NotificationMessages";
import moment from "moment";
import getInfoMessage from "./getInfoMessage";
import { withSnackbar } from "notistack";
import { debug } from "util";
import { generateAdvancedOptionData } from "../../../helper/generateSelectData";
import CustomRadioMultiDimentional from "../../../components/Form/Radio/RadioMultiDimentional";
import SelectSearchDropdown from "../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { FormattedMessage } from 'react-intl';
import DefaultSwitch from "../../../components/Form/Switch";
import CustomTooltip from "../../../components/Tooltip/Tooltip";
import InputLabel from "@material-ui/core/InputLabel";
class RepeatMeeting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      repeatTypeTab: "left",
      weekendMeetingTab: "center",
      weekEndMeetingCheck: true,
      daily: repeatMeetingInitObj.daily,
      weekly: repeatMeetingInitObj.weekly,
      monthly: repeatMeetingInitObj.monthly,
      weekEndMeeting: '0',
      advanceOption: false,
      AdvanceOption: [{ value: "Published", label: "Published" }],
    };
  }
  componentDidMount() {
    const { meeting } = this.props;
    const { daily, weekly, monthly } = this.state;

    if (meeting.meetingSchedule) {
      //Pre filling form values, if repeat Meeting already exist, especially selected tabs
      const repeatMeetingType = meeting.meetingSchedule.repeatType;
      const repeatTypeTab =
        repeatMeetingType == "Daily"
          ? "left"
          : repeatMeetingType == "Weekly"
            ? "center"
            : "right";
      const weekendOptionSelected =
        repeatMeetingType == "Daily"
          ? meeting.meetingSchedule.repeatDaily.weekendMeeting
          : repeatMeetingType == "Monthly"
            ? meeting.meetingSchedule.repeatMonthly.weekendMeeting
            : "";
      const weekendMeetingTab =
        weekendOptionSelected == "3"
          ? "left"
          : weekendOptionSelected == "2"
            ? "center"
            : "right";
      const weekEndMeetingCheck = weekendOptionSelected ? true : false;
      const advanceValue = generateAdvancedOptionData().filter((a) => {
        return a.value == meeting.meetingSchedule.advanceMeeting;
      });
      this.setState({
        weekendMeetingTab,
        repeatTypeTab,
        weekEndMeetingCheck,
        AdvanceOption: advanceValue,
        advanceOption: advanceValue ? true : false,
      });
      // Conditions applied to set repeat details object according to selected tab
      if (repeatMeetingType == "Daily") {
        //Daily
        this.setState({
          daily: { ...daily, repeatDetails: meeting.meetingSchedule },
          weekEndMeeting: meeting.meetingSchedule.repeatDaily?.weekendMeeting,
        });
      } else if (repeatMeetingType == "Weekly") {
        // Weekly
        this.setState({
          weekly: { ...weekly, repeatDetails: meeting.meetingSchedule },
          weekEndMeeting: meeting.meetingSchedule.repeatWeekly?.weekendMeeting,
        });
      } else {
        // Monthly
        this.setState({
          monthly: { ...monthly, repeatDetails: meeting.meetingSchedule },
          weekEndMeeting: meeting.meetingSchedule.repeatMonthly?.weekendMeeting,
        });
      }
    }
  }
  componentDidUpdate(prevProps, prevState) {
    // const { meeting } = this.props;
    // if (JSON.stringify(prevState.repeatTypeTab) !== JSON.stringify(this.state.repeatTypeTab)) {
    //   this.setState({ advanceOption: false, AdvanceOption: [{ value: "Published", label: "Published" }] });
    // }
    // else {
    //   const repeatMeetingType = meeting.meetingSchedule.repeatType;
    //   const repeatTypeTab =
    //     repeatMeetingType == "Daily"
    //       ? "left"
    //       : repeatMeetingType == "Weekly"
    //         ? "center"
    //         : "right";
    //   let advanceOpt = meeting.meetingSchedule.advanceMeeting
    //   this.setState({ advanceOption: true, AdvanceOption: advanceOpt });
    // }
  }

  handleWeekendMeetingChange = (event) => {
    const { weekEndMeetingCheck, repeatTypeTab } = this.state;
    // function to toggle checked and unchecked state of checkbox
    this.setState({ weekEndMeetingCheck: !weekEndMeetingCheck });
    if (!weekEndMeetingCheck) {
      // Incase the weekend checkbox is unchecked the weekendMeeting Value in state will be empty
      if (repeatTypeTab == "right") {
        this.updateDailyRepeatDetails("repeatMonthly", "weekendMeeting", "3");
      } else {
        this.updateDailyRepeatDetails("repeatDaily", "weekendMeeting", "3");
      }
    } else {
      if (repeatTypeTab == "right") {
        this.updateDailyRepeatDetails("repeatMonthly", "weekendMeeting", "");
      } else {
        this.updateDailyRepeatDetails("repeatDaily", "weekendMeeting", "");
      }
    }
  };
  handleTabSelect = (event, tab) => {
    if (tab) {
      this.setState({ repeatTypeTab: tab });
    }
  };
  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  handleWeekendMeetingTabSelect = (event, tab) => {
    const { repeatTypeTab } = this.state;
    if (tab) {
      //Setting the ENUMS based on the tab selected
      let selectedWeekendValue =
        tab == "left" ? "3" : tab == "center" ? "2" : "1";
      this.setState({ weekendMeetingTab: tab });
      if (repeatTypeTab == "right") {
        this.updateDailyRepeatDetails(
          "repeatMonthly",
          "weekendMeeting",
          selectedWeekendValue
        );
      } else {
        this.updateDailyRepeatDetails(
          "repeatDaily",
          "weekendMeeting",
          selectedWeekendValue
        );
      }
    }
  };

  //Updating Repeat Type in repeat meeting object
  updateRepeatType = () => { };
  //Updating RepeatDetails
  updateDailyRepeatDetails = (key, type, value) => {
    const { repeatTypeTab } = this.state;
    const selectedPeriod = // Saving reference to object depending on the selected Tab
      repeatTypeTab == "left"
        ? "daily"
        : repeatTypeTab == "center"
          ? "weekly"
          : "monthly";

    let clonedObj = cloneDeep(this.state[selectedPeriod]); // Cloning Object

    if (type == "root") {
      clonedObj["repeatDetails"][key] = value; // Updating Value on clone object
      this.setState({ [selectedPeriod]: clonedObj }); // Updating state with the new object of daily repeat meeting
    } else {
      clonedObj["repeatDetails"][key][type] = value; // Updating Value on clone object
      this.setState({ [selectedPeriod]: clonedObj }); // Updating state with the new object of daily repeat meeting
    }
  };
  handleSubmitRepeatMeeting = () => {
    const { repeatTypeTab } = this.state;
    const { meeting } = this.props;
    const updateRepeatMeeting = meeting.meetingSchedule ? true : false;
    this.setState({ btnQuery: "progress" });
    const selectedPeriod = // Saving reference to object depending on the selected Tab
      repeatTypeTab == "left"
        ? "daily"
        : repeatTypeTab == "center"
          ? "weekly"
          : "monthly";
    const postObj = {
      ...this.state[selectedPeriod],
      meetingId: meeting.meetingId,
      // weekendMeeting: this.state.weekEndMeeting ? '2' : '0',
    };
    this.props.repeatMeeting(
      postObj,
      //success
      () => {
        this.setState({ btnQuery: "" });
        this.props.closeAction();
      },
      //failure
      (error) => {
        this.showSnackBar(error.data.message, "error");
        this.setState({ btnQuery: "" });
      },
      updateRepeatMeeting
    );
  };

  deleteMeetingSchedule = () => {
    /** function calls when user delete schedule  */
    const { meeting } = this.props;
    this.props.deleteMeetingSchedule(
      meeting.meetingId,
      (success) => {
        this.props.closeAction();
      },
      (err) => { }
    );
  };
  handleAdvanceOption = () => {
    this.setState((prevState) => ({
      advanceOption: !prevState.advanceOption,
    }));
  };

  handleOptionsSelect = (type, option) => {
    /** function call when user select advanced option */
    const { repeatTypeTab } = this.state;
    this.setState({ [type]: option });
    if (repeatTypeTab == "left") {
      /** if tab is left then update object of daily */
      this.updateDailyRepeatDetails(type, "root", option.value);
    } else if (repeatTypeTab == "center") {
      /** if tab is center then update object of weekly */
      this.updateDailyRepeatDetails(type, "root", option.value);
    } else {
      this.updateDailyRepeatDetails(
        type,
        "root",
        option.value
      ); /** if tab is right then update object of monthly */
    }
  };

  handleSelectWeekendMeeting = (event, value) => {
    this.setState({ weekEndMeeting: value.value });
    const { repeatTypeTab } = this.state;
    if (repeatTypeTab == "left") {
      this.updateDailyRepeatDetails("repeatDaily", "weekendMeeting", value.value);
    } else if (repeatTypeTab == "center") {
      this.updateDailyRepeatDetails("repeatWeekly", "weekendMeeting", value.value);
    } else {
      this.updateDailyRepeatDetails("repeatMonthly", "weekendMeeting", value.value);
    }
  }
  render() {
    const {
      classes,
      theme,
      open,
      closeAction,
      meeting,
      label,
      meetingPer,
    } = this.props;
    const {
      btnQuery,
      alignment,
      weekEndMeetingCheck,
      repeatTypeTab,
      weekendMeetingTab,
      daily,
      weekly,
      monthly,
      weekEndMeeting,
      advanceOption,
      AdvanceOption,
    } = this.state;

    return (
      <div
        onClick={(event) => {
          event.stopPropagation();
        }}
      >
        {/* // <DetailsDialog
      //   title="Repeat Meeting"
      //   dialogProps={{
      //     open: open,
      //     onClick: e => {
      //       e.stopPropagation();
      //     },
      //     onClose: closeAction,
      //     PaperProps: {
      //       style: { maxWidth: 580 }
      //     }
      //   }}
      // > */}
        <div className={classes.topGrayBar}>
          <Typography variant="body1" className={classes.repeatMeetingHeading}>
            {label ? <FormattedMessage id="meeting.detail-dialog.repeat-meeting.label" defaultMessage="Repeat Meeting"></FormattedMessage> : ""}
          </Typography>
          <div className={classes.toggleContainer}>
            <ToggleButtonGroup
              value={repeatTypeTab}
              exclusive
              onChange={this.handleTabSelect}
              classes={{ root: classes.toggleBtnGroup, groupedHorizontal: classes.groupedHorizontal }}
            >
              <ToggleButton
                value="left"
                classes={{
                  root: classes.toggleButton,
                  selected: classes.toggleButtonSelected,
                }}
              >
                <FormattedMessage id="meeting.detail-dialog.repeat-meeting.daily.label" defaultMessage="Daily" />

              </ToggleButton>
              <ToggleButton
                value="center"
                classes={{
                  root: classes.toggleButton,
                  selected: classes.toggleButtonSelected,
                }}
              >
                <FormattedMessage id="meeting.detail-dialog.repeat-meeting.weekly.label" defaultMessage="Weekly" />

              </ToggleButton>

              <ToggleButton
                value="right"
                classes={{
                  root: classes.toggleButton,
                  selected: classes.toggleButtonSelected,
                }}
              >
                <FormattedMessage id="meeting.detail-dialog.repeat-meeting.monthly.label" defaultMessage="Monthly" />

              </ToggleButton>
            </ToggleButtonGroup>
          </div>
        </div>
        <div className={classes.contentCnt}>
          {repeatTypeTab == "left" ? (
            <Daily
              data={daily}
              updateRepeatData={this.updateDailyRepeatDetails}
              meeting={meeting}
            />
          ) : repeatTypeTab == "center" ? (
            <Weekly
              data={weekly}
              updateRepeatData={this.updateDailyRepeatDetails}
              meeting={meeting}
            />
          ) : (
            <Monthly
              data={monthly}
              updateRepeatData={this.updateDailyRepeatDetails}
              meeting={meeting}
            />
          )}
          {/* Weekend Check Container */}
          <div className={classes.weekendCheckCnt}>
            <InputLabel
              classes={{
                root: classes.selectLabel,
              }}
              style={{ marginTop: 15, display: 'flex', alignItems: 'center' }}
              shrink={false}>
              Non-Working days meeting creation
              <CustomTooltip
                size="Large"
                helptext={
                  <>Repeat meetings that falls on non working days could be skipped or may also be created before/on/after non working days</>
                }
                iconType="help"
                position="static"
              />
            </InputLabel>
            <CustomRadioMultiDimentional
              options={repeatMeetingOptions}
              value={weekEndMeeting}
              onRadioChange={this.handleSelectWeekendMeeting}
              iconProps={{ style: { fontSize: "18px" } }}
              radioGroupProps={{
                style: { display: "flex", alignItems: "center", flexDirection: "row" },
              }}
            />
            {/* {(repeatTypeTab == "left" ||
              (repeatTypeTab == "right" &&
                monthly.repeatDetails.repeatMonthly.timePeriod == "Day")) && ( */}
            {(false &&
              <>
                <DefaultCheckbox
                  checked={weekEndMeetingCheck}
                  onChange={this.handleWeekendMeetingChange}
                  label={<FormattedMessage id="meeting.detail-dialog.repeat-meeting.common.create-week-meeting.label" defaultMessage="Create weekend meeting"></FormattedMessage>}
                  styles={{ display: "flex", alignItems: "center" }}
                  checkboxStyles={{ paddingLeft: 0 }}
                />
                {/* Weekend Meeting Toggle Buttons */}
                {weekEndMeetingCheck && (
                  <div className={classes.toggleContainer}>
                    <ToggleButtonGroup
                      value={weekendMeetingTab}
                      exclusive
                      onChange={this.handleWeekendMeetingTabSelect}
                      classes={{ root: classes.toggleBtnGroup }}
                    >
                      <ToggleButton
                        value="left"
                        //   onClick={this.renderListView}
                        classes={{
                          root: classes.toggleButton,
                          selected: classes.toggleButtonSelected,
                        }}
                      >
                        <FormattedMessage id="meeting.detail-dialog.repeat-meeting.common.before-week.label" defaultMessage="Before weekend"></FormattedMessage>                         </ToggleButton>
                      <ToggleButton
                        value="center"
                        //   onClick={this.renderGridView}
                        classes={{
                          root: classes.toggleButton,
                          selected: classes.toggleButtonSelected,
                        }}
                      >
                        <FormattedMessage id="meeting.detail-dialog.repeat-meeting.common.on-weekend.label" defaultMessage="On weekend"></FormattedMessage>
                      </ToggleButton>

                      <ToggleButton
                        value="right"
                        //   onClick={this.renderCalendarView}
                        classes={{
                          root: classes.toggleButton,
                          selected: classes.toggleButtonSelected,
                        }}
                      >
                        <FormattedMessage id="meeting.detail-dialog.repeat-meeting.common.after-weekend.label" defaultMessage="After weekend"></FormattedMessage>
                      </ToggleButton>
                    </ToggleButtonGroup>
                  </div>
                )}
              </>
            )}
          </div>

          {/* <div className={classes.advanceOptionCnt}>
            <Typography                                
              variant="body2"
              className={classes.advanceOption}
              color="primary"
              onClick={this.handleAdvanceOption}
            >
              {advanceOption ? "Clear Advanced Options" : "Advanced Options"}
            </Typography>
            {advanceOption ? (
              <div className="flex_center_start_row">
                <span className={classes.selectLabel} style={{ width: '100%' }} >
                  Meeting occurs when last meeting is:
              </span>
                <SelectSearchDropdown  
                  data={() => {
                    return generateAdvancedOptionData(); 
                  }}
                  isClearable={false}
                  label=""
                  selectChange={this.handleOptionsSelect}
                  selectClear={() => { }}
                  type="AdvanceOption"
                  selectedValue={AdvanceOption}
                  placeholder={""}
                  isMulti={false}
                />
              </div>
            ) : null}
          </div> */}

          <NotificationMessage
            type="info"
            iconType="info"
            style={{ marginTop: 10 }}
          >
            {getInfoMessage(this.state)}
          </NotificationMessage>
        </div>
        <ButtonActionsCnt
          // cancelAction={closeAction}
          successAction={this.handleSubmitRepeatMeeting}
          successBtnText={meeting.meetingSchedule ? <FormattedMessage id="meeting.detail-dialog.repeat-meeting.save-button.update.label" defaultMessage="Update" /> : <FormattedMessage id="meeting.detail-dialog.repeat-meeting.save-button.save.label" defaultMessage="Repeat Meeting" />}
          deleteBtnText={meeting.meetingSchedule ? <FormattedMessage id="meeting.detail-dialog.repeat-meeting.delete-button.label" defaultMessage="Delete Schedule" /> : ""}
          deleteAction={this.deleteMeetingSchedule}
          // cancelBtnText="Discard Changes"
          btnType="success"
          btnQuery={btnQuery}
          disabled={
            meeting.meetingSchedule == null
              ? false
              : meetingPer.meetingDetail.repeatMeeting.isAllowEdit
                ? false
                : true
          }
          deletePer={!meetingPer.meetingDetail.repeatMeeting.isAllowDelete}
        />
      </div>
    );
  }
}

export default compose(
  withSnackbar,
  withStyles(repeatMeetingStyles, { withTheme: true }),
  connect((state) => state, {
    repeatMeeting,
    deleteMeetingSchedule,
  })
)(RepeatMeeting);
