import React, { Component } from "react";
import TimeSelect from "../../../components/TimePicker/TimeSelect";
import SelectSearchDropdown from "../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import {
  generateWeekNoData,
  generateRepeatStopByData
} from "../../../helper/generateSelectData";
import InputLabel from "@material-ui/core/InputLabel";
import repeatMeetingStyles from "./styles";
import { withStyles } from "@material-ui/core/styles";
import StaticDatePicker from "../../../components/DatePicker/StaticDatePicker";
import MultiSelectDropdown from "../../../components/Dropdown/MultiSelectDropdown/MultiSelectDropdown";
import DefaultTextField from "../../../components//Form/TextField";
import moment from "moment"
import { FormattedMessage } from "react-intl";

//Array of Days
const days = [
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
  "Sunday"
];

const weekData = generateWeekNoData();
const repeatStopData = generateRepeatStopByData();

class Weekly extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: repeatStopData[0],
      weekNo: weekData[0],
      selectedDay: [days[0]],
      afterMeetingCount: 5,
      weekInterval: 1,
      date: moment(),
      hours: "12",
      minutes: "00",
      timeFormat: 'AM'
    };
  }
  updateTime = (hours, minutes, timeFormat) => {
    const timeObj = { hours: hours, minutes, timeFormat };
    this.setState({ hours, minutes, timeFormat });
    this.props.updateRepeatData("repeatAt", "root", timeObj);
  };
  clearSelect = (key, value) => {
    this.setState({ [key]: value });
  };
  //Local state is update for dropdown so object can be made to send update to backend with API
  //Updating local state when option is selected in assignee dropdown
  handleOptionsSelect = (type, option, field) => {
    this.setState({ [type]: option }, () => {
      this.props.updateRepeatData(field, type, option.value);
    });
  };
  //Function which updates week number in repeat week object
  updateWeekNo = value => {
    this.props.updateRepeatData(
      "repeatWeekly",
      "weekNo",
      value.map(v => v.obj.number)
    );
  };
  //Saving date to local so later on can be used to send to backend
  updateDate = date => {
    this.setState({
      date
    });
    this.props.updateRepeatData("stopBy", "date", date.format("MM/DD/YYYY"));
  };
  //updating week day select in state
  handleDaySelect = value => {
    const { selectedDay } = this.state;
    if (selectedDay.includes(value)) {
      // if the day is already selected than remove it
      const newArray = selectedDay.filter(d => d !== value);
      if (newArray.length >= 1) {
        this.setState({ selectedDay: newArray });
        this.props.updateRepeatData("repeatWeekly", "dayInitial", newArray);
      }
    } else {
      // if the day is not already selected than add it
      this.setState({ selectedDay: [...selectedDay, value] });
      this.props.updateRepeatData("repeatWeekly", "dayInitial", [
        ...selectedDay,
        value
      ]);
    }
  };
  //Function handles on change of number field of stopBy Meeting Count
  handleInputChange = (event, name) => {
    let value = event.target.value;
    switch (name) {
      case "afterMeetingCount":
        //Update value only if value is greater than 0 and less than 1000
        if (value > 0 && value < 1000 && value.indexOf('.') == -1) {
          this.setState({ [name]: value });
          this.props.updateRepeatData("stopBy", "value", value);
        }
        break;
      case "weekInterval":
        //Update value only if value is greater than 0 and less than 1000
        if (value > 0 && value < 100 && value.indexOf('.') == -1) {
          this.setState({ [name]: value });
          this.props.updateRepeatData("repeatWeekly", "interval", value);
        }
        break;
      default:
        break;
    }
  };
  componentDidMount() {
    const { meeting } = this.props;
    let repeatType = meeting.meetingSchedule ? meeting.meetingSchedule.repeatType : null

    // Checking the repeat Type of the meeting if it already exist on weekly than prefill the values of input fields accordingly
    if (repeatType == "Weekly") {

      const stopByValue = generateRepeatStopByData().find(s => {
        return s.value == meeting.meetingSchedule.stopBy.type
      });
      const afterMeetingCount = meeting.meetingSchedule.stopBy.value;
      const date = moment(meeting.meetingSchedule.stopBy.date);
      const interval = meeting.meetingSchedule.repeatWeekly.interval;
      const dayInitial = meeting.meetingSchedule.repeatWeekly.dayInitial;
      const hours = meeting.meetingSchedule.repeatAt.hours;
      const minutes = meeting.meetingSchedule.repeatAt.minutes;
      const timeFormat = meeting.meetingSchedule.repeatAt.timeFormat;

      this.setState({
        type: stopByValue,
        afterMeetingCount: afterMeetingCount ? afterMeetingCount : 5,
        weekInterval: interval,
        date,
        selectedDay: dayInitial,
        hours,
        minutes,
        timeFormat
      });
    }
  }

  render() {
    const {
      type,
      selectedDay,
      weekNo,
      afterMeetingCount,
      weekInterval,
      date,
      hours,
      minutes,
      timeFormat
    } = this.state;
    const { classes, theme } = this.props;
    return (
      <>
        <div className={classes.repeatEveryCnt}>
          <InputLabel
            classes={{
              root: classes.selectLabel
            }}
            shrink={false}
          >
                   <FormattedMessage id="meeting.detail-dialog.repeat-meeting.weekly.every.label" defaultMessage="Every:"></FormattedMessage> 

          </InputLabel>
          <DefaultTextField
            label={false}
            fullWidth={true}
            error={false}
            errorState={false}
            formControlStyles={{ marginBottom: 0, width: 80 }}
            defaultProps={{
              id: "weekInterval",
              type: "number",
              onChange: e => this.handleInputChange(e, "weekInterval"),
              value: weekInterval,
              inputProps: {
                style: { paddingTop: 11, paddingBottom: 11 }
              }
            }}
          />
          {/* Stop By Dropdown  */}
          {/* <MultiSelectDropdown
            data={() => {
              return weekData;
            }}
            selectedValueInit="wk"
            defaultValue={[weekData[0]]}
            updateAction={this.updateWeekNo}
          /> */}
          {/* <SelectSearchDropdown
            data={() => {
              return weekData;
            }}
            isClearable={false}
            label=""
            selectChange={(type, option) => this.handleOptionsSelect(type, option, "repeatWeekly")}
            selectClear={this.handleClearSelect}
            styles={{ flex: 1, marginTop: 0, marginBottom: 0 }}
            type="weekNo"
            selectedValue={weekNo}
            placeholder={"Week"}
            isMulti={false}
          /> */}
          <InputLabel
            classes={{
              root: classes.inputCenterLabel
            }}
            shrink={false}
          >
                  <FormattedMessage id="meeting.detail-dialog.repeat-meeting.weekly.week.label" defaultMessage="week(s) on"></FormattedMessage>    

          </InputLabel>
          {/* Day Select Control */}
          <ul className={classes.daysSelect}>
            {days.map((day, i) => {
              return (
                <li
                  key={i}
                  onClick={event => this.handleDaySelect(day)}
                  style={{
                    background: selectedDay.includes(day)
                      ? theme.palette.common.white
                      : null
                  }}
                >
                  {day[0]}
                </li>
              );
            })}
          </ul>
        </div>
        {/* Time Select Control */}
        <TimeSelect
          clearSelect={this.clearSelect}
          updateTime={this.updateTime}
          label={<FormattedMessage id="meeting.detail-dialog.repeat-meeting.common.repeat-at.label" defaultMessage="Repeat at:"></FormattedMessage>}
          hours={hours}
          mins={minutes}
          timeFormat={timeFormat}
        />
        {/* StopBy Container */}
        <div className={classes.stopByCnt}>
          <InputLabel
            classes={{
              root: classes.selectLabel
            }}
            shrink={false}
          >
        <FormattedMessage id="meeting.detail-dialog.repeat-meeting.common.stop-by.label" defaultMessage="Stop by:"></FormattedMessage> 
          </InputLabel>
          {/* Stop By Dropdown  */}
          <SelectSearchDropdown
            data={() => {
              return generateRepeatStopByData();
            }}
            isClearable={false}
            label=""
            selectChange={(type, option) =>
              this.handleOptionsSelect(type, option, "stopBy")
            }
            selectClear={this.handleClearSelect}
            styles={{ flex: 1, marginRight: 10 }}
            type="type"
            selectedValue={type}
            placeholder={"type"}
            isMulti={false}
          />
          {/* Date picker */}
          {type.value == "Date" ? (
            <StaticDatePicker
              label=""
              placeholder="Select Date"
              isInput={true}
              selectedSaveDate={this.updateDate}
              isCreation={true}
              style={{ width: 170 }}
              date={date}
              formControlStyles={{ margin: 0 }}
              minDate={new Date()}
            />
          ) : (
              <DefaultTextField
                label={false}
                fullWidth={true}
                error={false}
                errorState={false}
                formControlStyles={{ marginBottom: 0, width: 80 }}
                defaultProps={{
                  id: "afterMeetingCount",
                  type: "number",
                  onChange: e => this.handleInputChange(e, "afterMeetingCount"),
                  value: afterMeetingCount
                }}
              />
            )}
        </div>
      </>
    );
  }
}

export default withStyles(repeatMeetingStyles, { withTheme: true })(Weekly);
