const workspaceSetting = theme => ({
  // New Styles 
  workspaceSettingCnt: {
    padding: "20px 20px 20px 60px"
  },
  workspaceSettingContentCnt: {
  },
  workspaceSettingListCnt: {
    width: 260,
    marginRight: 20
  },
  editIcon: {
    marginBottom: -2,
    marginLeft: 2,
    fontSize: "17px !important",
    cursor: 'pointer'
  },
  workspaceSettingSideList: {
    padding: 0,
    "& $listItem": {
      background: theme.palette.common.white,
      border: `1px solid ${theme.palette.border.lightBorder}`,
      "& $workspaceSideListText": {
        color: theme.palette.text.grayDarker,
        fontSize: "13px !important"
      },
      "&:hover": {
        background: theme.palette.common.white
      }
    },
    "& $listItemSelected": {
      background: theme.palette.common.white,
      border: `1px solid ${theme.palette.border.lightBorder}`,
      "& $workspaceSideListText": {
        color: `${theme.palette.text.primary} !important`,
        fontSize: "13px !important",
        fontWeight: theme.typography.fontWeightMedium,
      }
    }
  },

  listItem: {},
  listItemSelected: {},
  workspaceSideListText: {},
  personalInfoCnt: {
    padding: 20,
    borderRadius: 4,
    background: theme.palette.common.white,
    border: `1px solid ${theme.palette.border.lightBorder}`
  },
  workspaceNameCnt: {
    marginTop: 20
  },
  wsRightCntHeading: {
    marginBottom: 20
  },
  inviteTeamCnt: {
    padding: 20,
    borderRadius: 4,
    background: theme.palette.common.white,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    marginBottom: 10
  },
  inviteHeadingText: {
    marginBottom: 10
  },
  workspaceMembersCnt: {
    padding: 20,
    borderRadius: 4,
    background: theme.palette.common.white,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    marginBottom: 10
  },
  manageMembersCnt: {
    width: 594,
  },
  userList: {
    listStyleType: "none",
    padding: 0,
    margin: 0,
    "& > li": {
      borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
      padding: "10px 0",
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between"
    },
    "& li:last-child": {
      borderBottom: "none"
    }
  },
  headingItem: {
    padding: "0 20px",
    outline: "none",
    "&:hover": {
      backgroundColor: "transparent",
      cursor: "unset"
    }
  },

  headingText: {
    // Dropdown Heading Item Text
    fontSize: "12px !important",
    marginTop: 5,
    fontWeight: theme.typography.fontWeightLight,
    textTransform: "capitalize",
    color: theme.palette.secondary.light
  },
  emailIcon: {
    fontSize: "20px !important"
  },
  workspaceHeading: {
    marginRight: 20
  },
  backArrowIcon: {
    fontSize: "28px !important",
    marginRight: 10,
    cursor: "pointer"
  },
  workspaceSettingHeader: {
    margin: "0 0 10px 0",
    paddingRight: 40
  },
  crownIcon: {
    marginRight: 5
  },
  permissionTableCnt: {
    display: "flex",
    marginTop: 10,
    overflowX: "auto",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: 4
  },
  permissionTableList: {
    listStyleType: "none",
    padding: 0,
    margin: 0,
    width: "auto"
  },
  headingCell: {
    background: theme.palette.background.paper,
    padding: "10px 0 10px 10px",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    minHeight: 39
  },
  headerFirstCell: {
    background: theme.palette.common.white,
    padding: "15px 0 15px 10px",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`
  },
  bodyFirstCell: {
    background: theme.palette.common.white,
    padding: "5px 0 5px 10px",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`
  },
  headerCell: {
    background: theme.palette.common.white,
    padding: "15px 5px 15px 5px",
    textAlign: "center",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    minWidth: 150
  },
  bodyCell: {
    background: theme.palette.common.white,
    padding: "3px 5px 2px 5px",
    textAlign: "center",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    minWidth: 150
  },
  doneIcon: {
    fontSize: "19px !important"
  },
  // workspace setting container
  dialogCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    minHeight: 685
  },
  dialogPaperCnt: {
    overflowY: "visible",
    background: theme.palette.common.white,
    maxWidth: 800
  },
  defaultDialogTitle: {
    padding: "25px",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`
  },
  defaultDialogContent: {
    padding: 0,
    overflowY: "visible"
  },
  defaultDialogAction: {
    padding: "0 25px 25px"
  },


  profileSideListText: {
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular,
    color: theme.palette.text.secondary
  },

  profileSettingContentCnt: {
    // padding: "40px 20px 20px 20px"
    flex: 1
  },

  accountPrefFormCnt: {
    padding: 20
  },
  selectFormControl: {
    marginBottom: 40
  },
  formLabel: {
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular
  },
  selectLabel: {
    transform: "translate(6px, -18px) scale(1)",
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular
  },
  profileDialogActionBtns: {
    padding: "15px 20px"
  },
  profilePicCnt: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    borderRight: `1px solid ${theme.palette.border.lightBorder}`,
    paddingRight: 20,
    marginRight: 20,
    "&:hover $deleteProfileImageIcon": {
      display: "block"
    }
  },
  profilePic: {
    position: "relative",
    width: 60,
    height: 60,
    marginBottom: 20,
    borderRadius: "100%",
    background: theme.palette.background.light,
    '& img': {
      height: '100%',
      width: '100%',
      borderRadius: '50%'
    }
  },
  profileInfoList: {
    margin: 0,
    padding: 0,
    listStyleType: "none",

    "& li": {
      marginBottom: 10,
      lineHeight: "normal",
      color: theme.palette.text.secondary,
      fontSize: "12px !important",
      display: "flex"
    }
  },
  profileInfoIcon: {
    marginRight: 10
  },

  deleteProfileImageIcon: {
    position: "absolute",
    right: -2,
    top: -6,
    background: theme.palette.background.default,
    borderRadius: "100%",
    display: "none",
    cursor: "pointer"
  },
  uploadPhotoBtn: {
    color: theme.palette.text.secondary,
    textDecoration: "underline",
    fontSize: "12px !important",
    margin: 0,
    marginTop: 10,
    cursor: "pointer"
  },
  accountPrefHeading: {
    padding: "15px 20px",
    borderRadius: 5,
    marginBottom: 40,
    fontWeight: theme.typography.fontWeightRegular,
    background: theme.palette.background.light,
    fontSize: "16px !important",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between"
  },
  accountPrefHeadingInner: {
    display: "flex",
    alignItems: "center"
  },
  accountPrefHeadingIcon: {
    marginRight: 5
  },
  accountPrefContentStaticList: {
    margin: "0 0 30px 0",
    padding: 0,
    listStyleType: "none",
    "& li": {
      padding: "0 0 15px 10px",
      lineHeight: "normal",
      color: theme.palette.text.secondary,
      fontSize: "12px !important",
      borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      "& p": {
        margin: 0
      }
    },
    "& $noFlex": {
      display: "block"
    }
  },
  accountAttachMethod: {
    width: "100px",
    "& img": {
      width: "20px",
      verticalAlign: "bottom",
      marginRight: "10px"
    }
  },
  accountAttachEmail: {
    width: "200px",
    textOverflow: "ellipsis",
    overflow: "hidden",
    whiteSpace: "nowrap",
    color: "rgb(152, 193, 74)"
  },
  accountPrefContentList: {
    padding: 0,
    listStyleType: "none",
    "& li": {
      lineHeight: "normal",
      color: theme.palette.text.secondary,
      fontSize: "12px !important",
      "& ul li": {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
        padding: "15px 0 15px 10px",
        marginLeft: 15
      },
      "& p": {
        margin: 0
      },
      "& $mainSwitchCnt": {
        display: "flex",
        justifyContent: "space-between",
        borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
        alignItems: "center",
        padding: "15px 0 15px 10px",
        "& p": {
          color: theme.palette.text.primary,
          fontWeight: theme.typography.fontWeightRegular
        }
      }
    }
  },
  mainSwitchCnt: {},
  noFlexInner: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center"
  },
  noFlex: {},
  helpIconBtn: {
    padding: 0,
    "&:hover": {
      background: "transparent"
    }
  },
  twoFactorAuthInputCnt: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "flex-start",
    marginTop: 40
  },
  appIntegrationList: {
    margin: 0,
    padding: 0,
    listStyleType: "none",
    minHeight: 600,
    padding: 20,
    "& li": {
      padding: "15px 8px 15px 20px",
      borderRadius: 5,
      border: `1px solid ${theme.palette.border.lightBorder}`,
      display: "flex",
      marginBottom: 20,
      justifyContent: "space-between",
      alignItems: "center",
      "& p": {
        margin: "0 0 0 10px",
        fontWeight: theme.typography.fontWeightRegular
      },
      "& div": {
        display: "flex",
        alignItems: "center"
      }
    }
  },
  usageDetailCnt: {
    borderTop: `1px solid ${theme.palette.border.lightBorder}`,
    padding: "20px 20px 0 20px"
  },
  usageDetailList: {
    listStyleType: "none",
    padding: 0,
    "& li": {
      borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
      padding: "20px 0",
      display: "flex",
      justifyContent: "space-between"
    }
  },
  upgradeTextCnt: {
    fontSize: "12px !important",
    color: theme.palette.primary.light,
    textDecoration: "underline",
    margin: 0
  },
  greeText: {
    fontSize: "12px !important",
    color: theme.palette.primary.light,
    textDecoration: "underline",
    margin: 0
  },
  upgradeRightCnt: {
    textAlign: "right"
  },

  sendInviteEmailCnt: {
    display: 'flex',
    marginTop: 10,
  },
  inviteDropdownsCnt: {
    // flex: 1,
    display: "flex",
    // marginTop: 10,
  },
  userListCnt: {
    // marginTop: 10
  },
  memberPic: {
    height: 36,
    width: 36,
    marginRight: 10
  },
  disabledUserText: {
    color: theme.palette.text.danger,
    fontSize: "11px !important"
  },
  EnabledUserText: {
    color: theme.palette.text.green,
    fontSize: "10px !important",
    textDecoration: 'underline',
    cursor: 'pointer',
  },
  enableUserLink: {
    color: theme.palette.primary.light,
    textTransform: "uppercase",
    fontSize: "10px !important",
    textDecoration: "underline",
    marginRight: 10
  },
  disableUserLink: {
    color: theme.palette.text.danger,
    textTransform: "uppercase",
    fontSize: "10px !important",
    textDecoration: "underline",
    marginRight: 10
  },
  sendInviteBtnCnt: {
    // marginBottom: 20,
    // textAlign: "right",
    marginTop: 10,
    display: 'flex',
  },
  dropdownArrow: {
    marginLeft: 20,
    fontSize: "24px !important"
  },
  addNewRoleMenuItem: {
    background: theme.palette.background.items,
    borderTop: `1px solid ${theme.palette.border.lightBorder}`
  },
  addNewRoleMenuItemText: {
    padding: "0 4px",
    fontSize: "12px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightRegular
  },
  errorText: {
    color: 'red',
    marginTop: -10,
    marginLeft: 7,
    '& span a': {
      color: theme.palette.primary.light,
      cursor: 'pointer'
    },
  },
});

export default workspaceSetting;
