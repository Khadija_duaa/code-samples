export const columns = {
  meetingNone: {
    key: '',
    name: 'None'
  },
  id_C: {
    key: "uniqueId",
    name: "ID"
  },
  meetingTitle_C: {
    key: "meetingDisplayName",
    name: "Title"
  },
  meetingStatus_C: {
    key: "status",
    name: "Status"
  },
  taskTitle_C:{
    key: "taskTitle",
    name: "Task"
  },
  startDate_C: {
    key: "startDate",
    name: "Date"
  },
  startTime_C:{
    key: "startTime",
    name: "Time"
  },
  location_C:{
    key: "location",
    name: "Location"
  },
  attendee_C: {
    key: "attendee",
    name: "Attendee"
  },
  duration_C: {
    key: "timeDuration",
    name: "Duration"
  },
  projectName:{
    key:"project",
    name:"Project"
  }
}