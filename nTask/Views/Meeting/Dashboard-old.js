import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { withSnackbar } from "notistack";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import dashboardStyles from "./styles";
import differenceWith from "lodash/differenceWith";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import MenuItem from "@material-ui/core/MenuItem";
import LeftArrow from "@material-ui/icons/ChevronLeft";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import selectStyles from "../../assets/jss/components/select";
import combineStyles from "../../utils/mergeStyles";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import InputLabel from "@material-ui/core/InputLabel";
import Checkbox from "@material-ui/core/Checkbox";
import ListItemText from "@material-ui/core/ListItemText";
import DoneIcon from "@material-ui/icons/Done";
import RoundIcon from "@material-ui/icons/Brightness1";
import ArchivedIcon from "@material-ui/icons/Archive";
import GroupingDropdown from "../../components/Dropdown/GroupingDropdown/GroupingDropdown";
import SortingDropdown from "../../components/Dropdown/SortingDropdown/SortingDropdown";
import MeetingGridItem from "./Grid/Item";
import MeetingCalendar from "./Calendar/Calendar";
import MeetingList from "./List/MeetingList";
import SvgIcon from "@material-ui/core/SvgIcon";
import AdvFilterIcon from "../../components/Icons/AdvFilterIcon";
import QuickFilterIcon from "../../components/Icons/QuickFilterIcon";
import CustomButton from "../../components/Buttons/CustomButton";
import ImportExportDD from "../../components/Dropdown/ImportExportDD";
import MeetingFilter from "./MeetingFilter/MeetingFilter";
import classNames from "classnames";
import Footer from "../../components/Footer/Footer";
import Divider from "@material-ui/core/Divider";
import { getSortOrder, sortListData } from "../../helper/sortListData";
import { sortingDropdownData, groupingDropdownData } from "./dropdownData";
import { calculateContentHeight, isScrollBottom } from "../../utils/common";
import {
  getArchivedData,
  FetchWorkspaceInfo,
} from "../../redux/actions/workspace";
import { saveSortingProjectList } from "../../redux/actions/projects";

import EmptyState from "../../components/EmptyStates/EmptyState";
import { getFilteredMeetings } from "./MeetingFilter/FilterUtils";
import { setAppliedFilters } from "../../redux/actions/appliedFilters";
import SelectedRangeDatePicker from "../../components/DatePicker/SelectedRangeDatePicker";
import { teamCanView } from "../../components/PlanPermission/PlanPermission";
import PlainMenu from "../../components/Menu/menu";
import MenuList from "@material-ui/core/MenuList";
import { FormattedMessage, injectIntl } from "react-intl";

let increment = 20;
class MeetingDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      alignment: "left",
      filteredMeetings: [],
      quickFilters: [],
      dateRangeFilter: null,
      open: false,
      placement: "bottom-start",
      anchorEl: "",
      listView: true,
      gridView: false,
      calendarView: false,
      isArchived: false,
      exportedMessage: "",
      openFilterSidebar: false,
      multipleFilters: "",
      clearlastState: false,
      total: 0,
      loadMore: 0,
      isChanged: false,
      showRecords: increment,
      sortObj: this.getSortValues(),
      sortingDropDownData: sortingDropdownData,
      groupingDropdownData,
      selectedGroup: {},
    };
    this.allFilterList = [
      { key: "Show All", value: "Show All" },
      { key: "Today", value: "Today" },
      { key: "This Week", value: "This Week" },
      { key: "This Month", value: "This Month" },
      { key: "Upcoming", value: "Upcoming" },
      { key: "In Review", value: "In Review" },
      { key: "Published", value: "Published" },
      { key: "Cancelled", value: "Cancelled" },
      { key: "Overdue", value: "Overdue" },
      { key: "Archived", value: "Archived" },
    ];
    this.child = React.createRef();
    this.renderListView = this.renderListView.bind(this);
    this.renderCalendarView = this.renderCalendarView.bind(this);
    this.renderGridView = this.renderGridView.bind(this);
    this.handleArchiveChange = this.handleArchiveChange.bind(this);
    this.handleExportType = this.handleExportType.bind(this);
    this.closeSnakBar = this.closeSnakBar.bind(this);
    this.searchFilter = this.searchFilter.bind(this);
    this.searchFilterApplied = this.searchFilterApplied.bind(this);
    this.clearFilter = this.clearFilter.bind(this);
    this.returnCount = this.returnCount.bind(this);
    this.handleChangeState = this.handleChangeState.bind(this);
    this.resetCount = this.resetCount.bind(this);
  }
  getSortValues = () => {
    const { workspaces } = this.props;
    let loggedInTeam = this.props.loggedInTeam;

    if (loggedInTeam) {
      let { meetingColumn, meetingDirection } = getSortOrder(
        workspaces,
        loggedInTeam,
        3
      );

      if (meetingColumn && meetingDirection) {
        return {
          column: meetingColumn,
          direction: meetingDirection,
          cancelSort: "",
        };
      } else {
        return { column: "", direction: "", cancelSort: "NONE" };
      }
    }
  };

  setInitialGrouping = () => {
    // Function called to set the initial value of group by in meeting
    const { itemOrderState } = this.props;
    const meetingGroupBy = itemOrderState.groupByColumn.meetingGroupBy;
    const selectedGroupByValue =
      groupingDropdownData.find((g) => g.key == meetingGroupBy) || {}; // Finding Column object based on the column key saved on backend for grouping
    this.setState({ selectedGroup: selectedGroupByValue });
  };
  componentDidMount() {
    this.setInitialGrouping();
    this.setState({ loggedInTeam: this.props.profileState.data.loggedInTeam });

    // Setting up default selected filter ["This Month"].
    // this.handleChange({'target':{'value': ["This Month"]}});

    // For handling default meeting filter
    this.setState({
      filteredMeetings: getFilteredMeetings(
        this.props.meetingsState.data,
        this.props.appliedFiltersState.Meeting,
        [],
        null,
        this.props.tasksState.data,
        this.props.projects,
        this.props.meetingPer
      ),
    });
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.loggedInTeam !== nextProps.profileState.data.loggedInTeam)
      return { loggedInTeam: nextProps.profileState.data.loggedInTeam };
    else return { loggedInTeam: prevState.loggedInTeam };
  }

  componentDidUpdate(prevProps, prevState) {
    const { Meeting } = this.props.appliedFiltersState;
    const advanceFiltersChanged =
      JSON.stringify(prevProps.appliedFiltersState.Meeting) !==
      JSON.stringify(Meeting);
    const quickFiltersChanged =
      JSON.stringify(prevState.quickFilters) !==
      JSON.stringify(this.state.quickFilters);
    const isDateRangeChanged =
      JSON.stringify(prevState.dateRangeFilter) !==
      JSON.stringify(this.state.dateRangeFilter);
    const meetingsChanged =
      JSON.stringify(prevProps.meetingsState.data) !==
      JSON.stringify(this.props.meetingsState.data);

    const isArchiveSelected =
      this.state.quickFilters.length &&
      this.state.quickFilters[this.state.quickFilters.length - 1] ===
        "Archived";

    if (meetingsChanged && !isArchiveSelected) {
      this.setState({
        filteredMeetings: getFilteredMeetings(
          this.props.meetingsState.data,
          Meeting,
          this.state.dateRangeFilter
            ? [{ dateRangeFilter: this.state.dateRangeFilter }]
            : this.state.quickFilters,
          this.props.profileState.data.profile.startDayOfWeek,
          this.props.tasksState.data,
          this.props.projects,
          this.props.meetingPer
        ),
      });
    } else if (quickFiltersChanged) {
      if (isArchiveSelected) {
      } else {
        this.setState({
          filteredMeetings: getFilteredMeetings(
            this.props.meetingsState.data,
            Meeting,
            this.state.quickFilters,
            this.props.profileState.data.profile.startDayOfWeek,
            this.props.tasksState.data,
            this.props.projects,
            this.props.meetingPer
          ),
        });
      }
    } else if (advanceFiltersChanged) {
      // If advance filter is set than resetting quick filters
      if (Meeting) {
        this.setState({
          quickFilters: [],
          filteredMeetings: getFilteredMeetings(
            this.props.meetingsState.data,
            Meeting,
            null,
            null,
            this.props.tasksState.data,
            this.props.projects,
            this.props.meetingPer
          ),
        });
      } else {
        this.setState({
          filteredMeetings: getFilteredMeetings(
            this.props.meetingsState.data,
            null,
            this.state.dateRangeFilter
              ? [{ dateRangeFilter: this.state.dateRangeFilter }]
              : this.state.quickFilters,
            this.props.profileState.data.profile.startDayOfWeek,
            this.props.tasksState.data,
            this.props.projects,
            this.props.meetingPer
          ),
        });
      }
    } else if (isDateRangeChanged) {
      this.setState({
        filteredMeetings: getFilteredMeetings(
          this.props.meetingsState.data,
          Meeting,
          [{ dateRangeFilter: this.state.dateRangeFilter }],
          this.props.profileState.data.profile.startDayOfWeek,
          this.props.tasksState.data,
          this.props.projects,
          this.props.meetingPer
        ),
      });
    }
  }

  resetCount() {
    this.setState({ showRecords: increment, loadMore: increment });
  }
  returnCount(total, loadMore) {
    this.setState({ total, loadMore });
  }
  handleScrollDown = (e) => {
    const bottom = isScrollBottom(e);
    if (bottom) {
      if (this.state.total > this.state.showRecords)
        this.setState(
          { showRecords: this.state.showRecords + increment },
          () => {
            this.setState({ loadMore: this.state.showRecords }, () => {
              if (this.state.total < this.state.showRecords) {
                this.setState({
                  showRecords: this.state.total,
                  loadMore: this.state.total,
                });
              }
            });
          }
        );
    }
  };
  handleChangeState = () => {
    this.setState({ isChanged: false });
  };
  clearFilter() {
    this.setState({ multipleFilters: "", clearlastState: false });
  }
  searchFilterApplied() {
    this.setState({ multipleFilters: "" });
  }
  searchFilter(data) {
    this.setState({ multipleFilters: data, isChanged: true });
  }
  closeSnakBar() {
    this.setState({ showSnakBar: false });
  }

  handleDrawerOpen = () => {
    this.setState({ openFilterSidebar: true });
  };

  handleDrawerClose = () => {
    this.setState({ openFilterSidebar: false });
  };

  handleExportType(snackBarMessage, type) {
    this.setState({ exportedMessage: snackBarMessage });
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  }
  handleArchiveChange() {
    this.setState({ isArchived: !this.state.isArchived });
  }
  renderListView() {
    this.setState({
      listView: true,
      gridView: false,
      calendarView: false,
      ganttView: false,
      alignment: "left",
    });
  }
  renderGridView() {
    this.setState({
      listView: false,
      gridView: true,
      calendarView: false,
      ganttView: false,
      alignment: "center",
    });
  }

  renderCalendarView() {
    this.setState({
      listView: false,
      gridView: false,
      calendarView: true,
      ganttView: false,
      alignment: "right",
    });
  }
  handleClick(event, placement) {
    this.setState((state) => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }
  handleClose = () => {
    if (this.anchorEl.contains(event.target)) {
      return;
    }
    this.setState({ open: false });
  };
  dataRangeCllickHandler = (event, date) => {
    const { value } = event.target;
    // Preventing from default open dialog click
    if (typeof value === "object" && value !== "dateRange") {
      let items = value;
      if (items[items.length - 1] !== "Archived") {
        let showAllIndex = value.indexOf("Show All");
        if (value.length && showAllIndex > -1) {
          if (showAllIndex === value.length - 1) items = ["Show All"];
          else items.splice(showAllIndex, 1);
        }
      } else {
        items = ["Archived"];
        getArchivedData(
          3,
          (resp) => {
            this.setState({
              filteredMeetings: getFilteredMeetings(
                resp.data,
                null,
                this.state.quickFilters,
                this.props.profileState.data.profile.startDayOfWeek,
                this.props.tasksState.data,
                this.props.projects,
                this.props.meetingPer
              ),
            });
          },
          (error) => {}
        );
      }
      this.setState({ dateRangeFilter: null, quickFilters: items }, () => {
        if (this.props.appliedFiltersState.Meeting)
          this.props.setAppliedFilters(null, "Meeting");
      });
    } else if (value === "dateRange") {
      this.setState({ quickFilters: [] }, () => {
        this.setState({ dateRangeFilter: date.startDate ? date : null }, () => {
          if (this.props.appliedFiltersState.Meeting)
            this.props.setAppliedFilters(null, "Meeting");
        });
      });
    }
  };
  handleSelectClick = (value) => {
    // const { value } = event.target;
    // Preventing from default open dialog click
    // if (typeof value === "object" && value !== "dateRange") {
    let items = value;
    let { quickFilters } = this.state;
    if (value !== "Archived") {
      if (value == "Show All") items = ["Show All"];
      else {
        if (quickFilters.indexOf("Show All") > -1) {
          quickFilters = quickFilters.filter((item) => item != "Show All");
        }
        quickFilters.indexOf(value) == -1
          ? (items = [...quickFilters, value])
          : (items = quickFilters.filter((item) => item !== value));
      }
      // let showAllIndex = value.indexOf("Show All");
      // if (value.length && showAllIndex > -1) {
      //   if (showAllIndex === value.length - 1) items = ["Show All"];
      //   else items.splice(showAllIndex, 1);
      // }
    } else {
      items = ["Archived"];
      getArchivedData(
        3,
        (resp) => {
          this.setState({
            filteredMeetings: getFilteredMeetings(
              resp.data,
              null,
              this.state.quickFilters,
              this.props.profileState.data.profile.startDayOfWeek,
              this.props.tasksState.data,
              this.props.projects,
              this.props.meetingPer
            ),
          });
        },
        (error) => {}
      );
    }
    this.setState({ dateRangeFilter: null, quickFilters: items }, () => {
      if (this.props.appliedFiltersState.Meeting)
        this.props.setAppliedFilters(null, "Meeting");
    });
    // }
  };
  handleRefresh = () => {
    this.props.FetchWorkspaceInfo("", () => {});
  };
  filterMeeting = (meetingIds, type = "") => {
    this.setState(({ filteredMeetings }) => {
      return {
        filteredMeetings: differenceWith(
          filteredMeetings,
          meetingIds,
          (meeting, meetingId) => {
            return type == 5
              ? meetingId === meeting.meetingId &&
                  meeting.status !== "Published"
              : meetingId === meeting.meetingId;
          }
        ),
      };
    });
  };

  handleClearArchived = () => {
    this.setState({ quickFilters: [], open: false });
  };

  filterUnArchiveMeetingDelete = (meetingId) => {
    this.setState(({ filteredMeetings }) => {
      return {
        filteredMeetings: filteredMeetings.filter((meeting) => {
          return meeting.meetingId !== meetingId;
        }),
      };
    });
  };

  sortingMeetingList = (column, direction, cancelSort = "") => {
    /** function passing to sorting drop down for getting the selected value  */
    this.setState({
      sortObj: { column: column, direction: direction, cancelSort: cancelSort },
    }); /** updating state object and passing sortObj to meeting List */
    let obj = {
      meeting: [column],
      type: 3,
      MeetingOrder: direction,
    };
    this.props.saveSortingProjectList(
      obj,
      (succ) => {},
      (err) => {}
    );
  };

  filteredMeetings = () => {
    /** function for sorting if there is already saved value of sorted column and sort order */
    const { sortObj, filteredMeetings } = this.state;
    if (sortObj && sortObj.column && sortObj.direction) {
      let data = sortListData(
        filteredMeetings,
        sortObj.column,
        sortObj.direction
      );
      return data;
    } else if (sortObj && sortObj.cancelSort == "NONE") {
      return getFilteredMeetings(
        this.props.meetingsState.data,
        this.props.appliedFiltersState.Meeting,
        [],
        null,
        this.props.tasksState.data,
        this.props.projects,
        this.props.meetingPer
      );
    } else {
      return getFilteredMeetings(
        this.props.meetingsState.data,
        this.props.appliedFiltersState.Meeting,
        [],
        null,
        this.props.tasksState.data,
        this.props.projects,
        this.props.meetingPer
      );
    }
  };
  //Lifecycle method on group select
  onGroupSelect = (option) => {
    this.setState({ selectedGroup: option });
  };
  getTranslatedId(name) {
    switch(name) {
       case "Upcoming":
        name = "meeting.status.upcoming";
         break;
         case "In Review":
          name = "meeting.status.in-review";
          break;
          case "Published":
            name = "meeting.status.publish";
            break;
            case "Cancelled":
              name = "meeting.status.cancel";
              break;
              case "Overdue":
                name = "meeting.status.overdue";
                break;
                case "Today":
                  name = "meeting.status.today";
                  break;
                  case "This Week":
                    name = "meeting.status.week";
                    break;
                    case "This Month":
                      name = "meeting.status.month";
                      break;
    }
    return name;
  }
  render() {
    const { classes, theme, quote, workspaces, loggedInTeam, intl } = this.props;
    const {
      alignment,
      calendarView,
      gridView,
      openFilterSidebar,
      exportedMessage,
      quickFilters,
      filteredMeetings,
      multipleFilters,
      showRecords,
      dateRangeFilter,
      selectedGroup = {},
    } = this.state;
    const color = theme.palette;
    const Status = [
      { label: intl.formatMessage({id: this.getTranslatedId("Upcoming"),defaultMessage:"Upcoming"}), name: "Upcoming", color: color.meetingStatus.Upcoming },
      { label: intl.formatMessage({id: this.getTranslatedId("In Review"),defaultMessage:"In Review"}), name: "In Review", color: color.meetingStatus.InReview },
      { label: intl.formatMessage({id: this.getTranslatedId("Published"),defaultMessage:"Published"}), name: "Published", color: color.meetingStatus.Published },
      { label: intl.formatMessage({id: this.getTranslatedId("Cancelled"),defaultMessage:"Cancelled"}), name: "Cancelled", color: color.meetingStatus.Cancelled },
      { label: intl.formatMessage({id: this.getTranslatedId("Overdue"),defaultMessage:"Overdue"}), name: "Overdue", color: color.meetingStatus.OverDue }
    ];
    const Date = [
      { label:intl.formatMessage({id:this.getTranslatedId("Today"),defaultMessage:"Today"}), name: "Today" },
      { label:intl.formatMessage({id:this.getTranslatedId("This Week"),defaultMessage:"This Week"}), name: "This Week" },
      { label:intl.formatMessage({id:this.getTranslatedId("This Month"),defaultMessage:"This Month"}), name: "This Month" }
    ];

    let statusData = [];
    let meetingDay = [];
    let archived = false,
      starred = false;
    let filterData = {};

    const isArchived =
      quickFilters.length &&
      quickFilters[quickFilters.length - 1] === "Archived";

    statusData = Status.filter((x) => {
      return quickFilters.indexOf(x.name) >= 0;
    }).map((x) => x.name);

    meetingDay = Date.filter((x) => {
      return quickFilters.indexOf(x.name) >= 0;
    }).map((x) => x.name);

    archived = quickFilters.indexOf("Archived") >= 0;
    starred = quickFilters.indexOf("Starred") >= 0;

    filterData = {
      statusData,
      meetingDay,
      archived,
      starred,
    };

    // const MenuProps = {
    //   MenuListProps: {
    //     disablePadding: true,
    //   },
    //   PaperProps: {
    //     style: {
    //       overflow: "visible",
    //       transform: "translateY(40px)",
    //       overflow: "hidden",
    //       borderRadius: 4,
    //     },
    //   },
    //   anchorEl: this.anchorEl,
    // };
    const saveFilterApplied = this.props.appliedFiltersState.Meeting
      ? true
      : false;
    const selectedFilter = [];
    this.allFilterList.map((item) => {
      if (quickFilters.indexOf(item.key) != -1) selectedFilter.push(item.value);
    });
    const customFilterSelect =
      selectedFilter.length == 0 ||
      (selectedFilter.length == 1 && selectedFilter.indexOf("Show All") > -1)
        ? false
        : true;
    const selectedFiltersLbl = customFilterSelect
      ? selectedFilter.length > 1
        ? intl.formatMessage({id:this.getTranslatedId(selectedFilter[0]),defaultMessage: selectedFilter[0]}) + " & +" + (selectedFilter.length - 1)
        : intl.formatMessage({id:this.getTranslatedId(selectedFilter[0]),defaultMessage: selectedFilter[0]})
      : intl.formatMessage({id:"common.all.label",defaultMessage:"All"});

    return (
      <div className={classes.root}>
        {/* <MeetingDetails /> */}
        <main
          className={
            // teamCanView("advanceFilterAccess") ?
            classNames(classes.content, {
              [classes.contentShift]: openFilterSidebar,
            })
            // : classes.noFiltercontent
          }
        >
          <div className={classes.drawerHeader}>
            <Grid container classes={{ container: classes.taskDashboardCnt }}>
              <Grid
                container
                direction="row"
                justify="space-between"
                alignItems="center"
                classes={{ container: classes.taskDashboardHeader }}
              >
                <div className="flex_center_start_row">
                  {isArchived ? (
                    <>
                      <LeftArrow
                        onClick={this.handleClearArchived}
                        className={classes.backArrowIcon}
                      />
                      <Typography variant="h1">
                        <FormattedMessage
                          id="common.archived.meeting.label"
                          defaultMessage="Archived Meetings"
                        />
                      </Typography>
                    </>
                  ) : <div
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    marginRight: "16px",
                  }}>
                  <Typography variant="h1" className={classes.listViewHeading}>
                    Meetings
                  </Typography>
                </div>}
                  {!isArchived ? (
                    <div className={classes.toggleContainer}>
                      <ToggleButtonGroup
                        value={alignment}
                        exclusive
                        classes={{ root: classes.toggleBtnGroup }}
                      >
                        <ToggleButton
                          value="left"
                          onClick={this.renderListView}
                          classes={{
                            root: classes.toggleButton,
                            selected: classes.toggleButtonSelected,
                          }}
                        >
                          <FormattedMessage
                            id="common.list.label"
                            defaultMessage="List"
                          />
                        </ToggleButton>
                        <ToggleButton
                          value="center"
                          onClick={this.renderGridView}
                          classes={{
                            root: classes.toggleButton,
                            selected: classes.toggleButtonSelected,
                          }}
                        >
                          <FormattedMessage
                            id="common.grid.label"
                            defaultMessage="Grid"
                          />
                        </ToggleButton>

                        <ToggleButton
                          value="right"
                          onClick={this.renderCalendarView}
                          classes={{
                            root: classes.toggleButton,
                            selected: classes.toggleButtonSelected,
                          }}
                        >
                          <FormattedMessage
                            id="common.calendar.label"
                            defaultMessage="Calendar"
                          />
                        </ToggleButton>
                      </ToggleButtonGroup>
                    </div>
                  ) : null}
                </div>

                {!isArchived ? (
                  <div className="flex_center_start_row">
                    {this.state.listView /** Advanced sorting only display in premium and bussiness plan */ && <>
                    <GroupingDropdown
                      selectedGroup={selectedGroup}
                      view="meeting"
                      data={this.state.groupingDropdownData}
                      onGroupSelect={this.onGroupSelect}
                      height={160}
                    />
                      <SortingDropdown
                        data={this.state.sortingDropDownData}
                        sortingList={this.sortingMeetingList}
                        getSortOrder={getSortOrder(workspaces, loggedInTeam, 3)}
                        height={!teamCanView("advanceSortAccess") ? 220 : null}
                        width={!teamCanView("advanceSortAccess") ? 305 : null}
                      />
                    </>}

                    {calendarView ? null : (
                      <React.Fragment>
                        <SelectedRangeDatePicker
                          placeholder="Date Filter"
                          handleFilterChange={(val, date) =>
                            this.dataRangeCllickHandler(
                              { target: { value: "dateRange" } },
                              date
                            )
                          }
                          type="date"
                          selectedDate={dateRangeFilter}
                          inputProps={{
                            error: false,
                          }}
                          classes={{
                            outlinedInput: classes.outlinedInput,
                            placeholderLbl: classes.placeholderLbl,
                            dateSelection: classes.dateSelection,
                            dateFilterIconSvg: classes.dateFilterIconSvg,
                            dateFilterIconSelected:
                              classes.dateFilterIconSelected,
                          }}
                        />

                        <FormControl
                          className={classes.formControl}
                          style={{ marginLeft: 8 }}
                        >
                          {/* <Select
                            multiple
                            displayEmpty
                            value={quickFilters}
                            renderValue={selected => {
                              if (selected.length === 0) {
                                return (
                                  <span
                                    style={{ color: theme.palette.text.light }}
                                  >
                                    Filter
                                  </span>
                                );
                              }
                              return selected.join(", ");
                            }}
                            onClick={this.handleSelectClick}
                            input={
                              <OutlinedInput
                                labelWidth={150}
                                notched={false}
                                inputRef={node => {
                                  this.anchorEl = node;
                                }}
                                classes={{
                                  root: classes.outlinedSelectCnt,
                                  notchedOutline: classes.outlinedSelect
                                }}
                                id="select-multiple-checkbox"
                              />
                            }
                            MenuProps={MenuProps}
                            classes={{
                              root: classes.selectCnt,
                              select: classes.selectCmp,
                              selectMenu: classes.selectMenu,
                              icon: classes.selectIcon
                            }}
                          > */}
                          <CustomButton
                            onClick={(event) => {
                              this.handleClick(event, "bottom-end");
                            }}
                            buttonRef={(node) => {
                              this.anchorEl = node;
                            }}
                            style={{
                              padding: customFilterSelect
                                ? "4px 8px 4px 4px"
                                : "3px 8px 3px 4px",
                              borderRadius: "4px",
                              display: "flex",
                              justifyContent: "space-between",
                              minWidth: "auto",
                              whiteSpace: "nowrap",
                            }}
                            btnType={customFilterSelect ? "lightBlue" : "white"}
                            variant="contained"
                          >
                            <SvgIcon
                              viewBox="0 0 24 24"
                              className={
                                customFilterSelect
                                  ? classes.qckFfilterIconSelected
                                  : classes.qckFilterIconSvg
                              }
                            >
                              <QuickFilterIcon />
                            </SvgIcon>
                            <span
                              className={
                                customFilterSelect
                                  ? classes.qckFilterLblSelected
                                  : classes.qckFilterLbl
                              }
                            >
                              <FormattedMessage
                                id="common.show.label"
                                defaultMessage="Show"
                              />{" "}
                              :{" "}
                            </span>
                            <span className={classes.checkname}>
                              {selectedFiltersLbl}
                            </span>
                          </CustomButton>
                          <PlainMenu
                            open={this.state.open}
                            closeAction={this.handleClose}
                            placement="bottom-start"
                            anchorRef={this.anchorEl}
                            style={{ width: 280 }}
                            offset="0 15px"
                          >
                            <MenuList disablePadding>
                              <MenuItem
                                className={`${classes.statusMenuItemCnt} ${
                                  !customFilterSelect
                                    ? classes.selectedValue
                                    : ""
                                }`}
                                classes={{
                                  root: classes.statusMenuItemCnt,
                                  selected: classes.statusMenuItemSelected,
                                }}
                                value="Show All"
                                onClick={this.handleSelectClick.bind(
                                  this,
                                  "Show All"
                                )}
                              >
                                <ListItemText
                                  primary={<FormattedMessage id="common.show.all" defaultMessage="Show All" />}
                                  classes={{ primary: classes.plainItemText }}
                                />
                              </MenuItem>
                              <Divider />
                              <MenuItem
                                disableRipple={true}
                                classes={{
                                  root: classes.menuHeadingItem,
                                }}
                              >
                                {this.props.intl.formatMessage({
                                  id: "meeting.creation-dialog.form.date.label",
                                  defaultMessage: "Date",
                                })}
                              </MenuItem>
                              {Date.map((status) => (
                                <MenuItem
                                  key={status.name}
                                  value={status.name}
                                  className={`${classes.statusMenuItemCnt} ${
                                    quickFilters.indexOf(status.name) > -1
                                      ? classes.selectedValue
                                      : ""
                                  }`}
                                  classes={{
                                    selected: classes.statusMenuItemSelected,
                                  }}
                                  onClick={this.handleSelectClick.bind(
                                    this,
                                    status.name
                                  )}
                                >
                                  <ListItemText
                                    primary={status.label}
                                    classes={{
                                      primary: classes.statusItemText,
                                    }}
                                  />
                                </MenuItem>
                              ))}
                              <Divider />
                              <MenuItem
                                classes={{ root: classes.menuHeadingItem }}
                              >
                                <FormattedMessage
                                  id="task.creation-dialog.form.status.label"
                                  defaultMessage="Status"
                                />
                              </MenuItem>
                              {Status.map((item) => (
                                <MenuItem
                                  key={item.name}
                                  value={item.name}
                                  className={`${classes.statusMenuItemCnt} ${
                                    quickFilters.indexOf(item.name) > -1
                                      ? classes.selectedValue
                                      : ""
                                  }`}
                                  classes={{
                                    selected: classes.statusMenuItemSelected,
                                  }}
                                  onClick={this.handleSelectClick.bind(
                                    this,
                                    item.name
                                  )}
                                >
                                  <RoundIcon
                                    htmlColor={item.color}
                                    classes={{ root: classes.statusIcon }}
                                  />
                                  <ListItemText
                                    primary={item.label}
                                    classes={{
                                      primary: classes.statusItemText,
                                    }}
                                  />
                                </MenuItem>
                              ))}
                              <MenuItem
                                value={"Archived"}
                                classes={{ root: classes.highlightItem }}
                                onClick={this.handleSelectClick.bind(
                                  this,
                                  "Archived"
                                )}
                              >
                                <ArchivedIcon
                                  htmlColor={theme.palette.secondary.light}
                                  classes={{
                                    root: classes.selectHighlightItemIcon,
                                  }}
                                />
                                <FormattedMessage
                                  id="common.archived.archived"
                                  defaultMessage="Archived"
                                />
                              </MenuItem>
                            </MenuList>
                          </PlainMenu>
                          {/* </Select> */}
                        </FormControl>
                      </React.Fragment>
                    )}
                    {!calendarView ? (
                      <div className={classes.advFilterButtonCnt}>
                        <CustomButton
                          onClick={this.handleDrawerOpen}
                          style={{
                            padding: saveFilterApplied ? "4px" : "3px 4px",
                            minWidth: "auto",
                          }}
                          btnType={saveFilterApplied ? "lightBlue" : "white"}
                          variant="contained"
                        >
                          <SvgIcon
                            viewBox="0 0 24 24"
                            className={
                              saveFilterApplied
                                ? classes.advFilterIconSelected
                                : classes.advFilterIconSvg
                            }
                          >
                            <AdvFilterIcon />
                          </SvgIcon>
                          <span
                            className={
                              saveFilterApplied
                                ? classes.advFilterLblSelected
                                : classes.advFilterLbl
                            }
                          >
                            <FormattedMessage
                              id="common.filter.label"
                              defaultMessage="Filters"
                            />
                          </span>
                        </CustomButton>
                      </div>
                    ) : null}
                    <ImportExportDD
                      handleExportType={this.handleExportType}
                      filterList={filterData}
                      multipleFilters={multipleFilters}
                      handleRefresh={this.handleRefresh}
                      ImportExportType={"meeting"}
                      data={filteredMeetings}
                    />
                  </div>
                ) : null}
              </Grid>
              <Grid
                container
                classes={{ container: classes.dashboardContentCnt }}
              >
                {calendarView ? (
                  <Grid
                    item
                    classes={{ item: classes.taskCalendarCnt }}
                    style={{ height: calculateContentHeight() }}
                  >
                    <MeetingCalendar
                      meetingState={quickFilters}
                      handleExportType={this.handleExportType}
                      exportedMessage={exportedMessage}
                      multipleFilters={this.state.multipleFilters}
                      searchFilterApplied={this.searchFilterApplied}
                      returnCount={this.returnCount}
                      handleChangeState={this.handleChangeState}
                      isChanged={this.state.isChanged}
                    />
                  </Grid>
                ) : gridView ? (
                  <Grid
                    container
                    onScroll={this.handleScrollDown}
                    classes={{ container: classes.taskGridCnt }}
                    style={{ height: calculateContentHeight() - 90 }}
                  >
                    {this.props.meetingsState.data.length > 0 ? (
                      <MeetingGridItem
                        meetingState={quickFilters}
                        filteredMeetings={filteredMeetings}
                        filterMeeting={this.filterMeeting}
                        handleExportType={this.handleExportType}
                        exportedMessage={exportedMessage}
                        multipleFilters={this.state.multipleFilters}
                        searchFilterApplied={this.searchFilterApplied}
                        returnCount={this.returnCount}
                        handleChangeState={this.handleChangeState}
                        isChanged={this.state.isChanged}
                        showRecords={showRecords}
                        resetCount={this.resetCount}
                        isArchivedSelected={isArchived}
                        sortObj={this.state.sortObj}
                        meetings={filteredMeetings}
                      />
                    ) : isArchived ? (
                      <EmptyState
                        screenType="Archived"
                        heading={
                          <FormattedMessage
                            id="common.archived.label"
                            defaultMessage="No archived items found"
                          />
                        }
                        message={
                          <FormattedMessage
                            id="common.archived.message"
                            defaultMessage="You haven't archived any items yet."
                          />
                        }
                        button={false}
                      />
                    ) : this.props.meetingPer.createMeeting.cando ? (
                      <EmptyState
                        screenType="meeting"
                        heading={
                          <FormattedMessage
                            id="common.create-first.meeting.label"
                            defaultMessage="Create your first meeting"
                          />
                        }
                        message={
                          <FormattedMessage
                            id="common.create-first.meeting.messagea"
                            defaultMessage='You do not have any meetings yet. Press "Alt + M" or click on button below.'
                          />
                        }
                        button={true}
                      />
                    ) : (
                      <EmptyState
                        screenType="meeting"
                        heading={
                          <FormattedMessage
                            id="common.create-first.meeting.messageb"
                            defaultMessage="You do not have any meetings yet."
                          />
                        }
                        message={
                          <FormattedMessage
                            id="common.create-first.meeting.messagec"
                            defaultMessage="Request your team member(s) to assign you a meeting."
                          />
                        }
                        button={false}
                      />
                    )}
                  </Grid>
                ) : (
                  <Grid item classes={{ item: classes.taskListCnt }}>
                    <MeetingList
                      meetingState={quickFilters}
                      filteredMeetings={filteredMeetings}
                      filterMeeting={this.filterMeeting}
                      isArchivedSelected={isArchived}
                      handleExportType={this.handleExportType}
                      exportedMessage={exportedMessage}
                      multipleFilters={this.state.multipleFilters}
                      searchFilterApplied={this.searchFilterApplied}
                      returnCount={this.returnCount}
                      handleChangeState={this.handleChangeState}
                      isChanged={this.state.isChanged}
                      filterUnArchiveMeetingDelete={
                        this.filterUnArchiveMeetingDelete
                      }
                      sortObj={this.state.sortObj}
                      selectedGroup={selectedGroup}
                      showSnackBar={this.props.showSnackBar}
                    />
                  </Grid>
                )}
              </Grid>
            </Grid>
          </div>
        </main>
        {/* {teamCanView("advanceFilterAccess") && ( */}
        <>
          <MeetingFilter
            open={openFilterSidebar}
            closeAction={this.handleDrawerClose}
            searchFilter={this.searchFilter}
            clearFilter={this.clearFilter}
            clearlastState={this.state.clearlastState}
          />
        </>
        {/* )} */}
        <Footer
          total={this.state.filteredMeetings.length}
          display={true}
          type="Meeting"
          quote={this.props.quote}
          open={this.props.open}
        />
        {/* )} */}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    meetingsState: state.meetings,
    profileState: state.profile,
    appliedFiltersState: state.appliedFilters,
    itemOrderState: state.itemOrder.data,
    tasksState: state.tasks,
    workspaces: state.profile.data.workspace,
    loggedInTeam: state.profile.data.loggedInTeam,
    meetingPer: state.workspacePermissions.data.meeting,
    projects: state.projects.data || [],
  };
};

export default compose(
  withRouter,
  withSnackbar,
  injectIntl,
  withStyles(combineStyles(dashboardStyles, selectStyles), {
    withTheme: true,
  }),
  connect(mapStateToProps, {
    setAppliedFilters,
    FetchWorkspaceInfo,
    saveSortingProjectList,
  })
)(MeetingDashboard);
