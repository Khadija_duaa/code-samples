import React, { useState, useRef, useEffect, memo } from "react";
import { useDispatch, useSelector } from "react-redux";
import DropdownMenu from "../../../components/Dropdown/DropdownMenu";
import CustomIconButton from "../../../components/Buttons/CustomIconButton";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import CustomListItem from "../../../components/ListItem/CustomListItem";
import withStyles from "@material-ui/core/styles/withStyles";
import meetingActionDropdownStyles from "./meetingActionDropdown.style";
import CustomMenuList from "../../../components/MenuList/CustomMenuList";

import {
  createMeeting,
  updateMeetingData,
  BulkUpdateMeeting,
  DeleteMeeting,
  ArchiveMeeting,
  UnArchiveMeeting,
  DeleteMeetingSchedule,
  ArchiveMeetingSchedule,
  UnArchiveMeetingSchedule,
  CancelMeeting,
  CancelMeetingSchedule,
  MarkMeetingAsStarted,
} from "../../../redux/actions/meetings";
import { emptyTask } from "../../../utils/constants/emptyTask";
import { v4 as uuidv4 } from "uuid";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import { FormattedMessage } from "react-intl";
import { withSnackbar } from "notistack";
import { compose } from "redux";
import { publickLink, setDeleteDialogueState } from "../../../redux/actions/allDialogs";
import ColorPicker from "../../../components/Dropdown/ColorPicker/ColorPicker";
import isEqual from "lodash/isEqual";
import "react-toastify/dist/ReactToastify.css";
import IconActivity from "../../../components/Icons/TaskActionIcons/IconActivity";
import IconArchive from "../../../components/Icons/TaskActionIcons/IconArchive";
import IconColor from "../../../components/Icons/TaskActionIcons/IconColor";
import IconDelete from "../../../components/Icons/TaskActionIcons/IconDelete";
import SvgIcon from "@material-ui/core/SvgIcon";
import IconCopy from "../../../components/Icons/TaskActionIcons/IconCopy";
import IconLink from "../../../components/Icons/TaskActionIcons/IconLink";
import CancelIcon from "@material-ui/icons/Cancel";


// import constants from "../constants/types";
function areEqual(prevProps, nextProps) {
  return isEqual(prevProps, nextProps);
  /*
  return true if passing nextProps to render would return
  the same result as passing prevProps to render,
  otherwise return false
  */
}
const MeetingActionDropdown = memo(props => {
  const [open, setOpen] = useState(null);
  const [archiveConfirmation, setArchiveConfirmation] = useState(false);
  const [unArchiveConfirmation, setUnArchiveConfirmation] = useState(false);
  const [archiveBtnQuery, setArchiveBtnQuery] = useState("");
  const [unarchiveBtnQuery, setUnarchiveBtnQuery] = useState("");

  const anchorEl = useRef(null);
  const dispatch = useDispatch();
  const {
    classes,
    data,
    btnProps,
    handleActivityLog,
    handleCloseCallBack,
    meetingPermission,
    isArchived = false,
    activityLogOption = false,
  } = props;
  const handleClose = event => {
    // Function closes dropdown
    setOpen(null);
  };
  const handleArchiveConfirmClose = () => {
    setArchiveConfirmation(false);
    setUnArchiveConfirmation(false);
  };
  const handleArchiveConfirmation = () => {
    setArchiveConfirmation(true);
  };
  const handleUnArchiveConfirmation = () => {
    setUnArchiveConfirmation(true);
  };
  const handleDropdownOpen = event => {
    // Function Opens the dropdown
    setOpen(state => (state ? null : event.currentTarget));
  };
  const showSnackBar = (snackBarMessage = "", type = null) => {
    const { classes, enqueueSnackbar } = props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  useEffect(() => {
    anchorEl.current &&
      anchorEl.current.addEventListener("click", e => {
        e.stopPropagation();
        handleDropdownOpen(e);
      });
  }, []);
  //Delete single task
  const deleteTask = () => {
    handleCloseCallBack();
    dispatch(setDeleteDialogueState({ btnQuery: "progress" }));

    DeleteMeeting(
      data.meetingId,
      (data, res) => {
        closeDeleteConfirmation();
      },
      err => {
        closeDeleteConfirmation();
        if (err.data && err.data.message) showSnackBar(err.data.message, "error");
      },
      dispatch
    );
  };
  //Cancel single meeting
  const cancelMeeting = () => {
    handleCloseCallBack();
    dispatch(setDeleteDialogueState({ btnQuery: "progress" }));
    CancelMeeting({ meetingId: data.meetingId, parentId: data.parentId }, () => {
      closeDeleteConfirmation();
    }, dispatch);
  };
  //archive a Meeting
  const archiveTask = () => {
    setArchiveBtnQuery('progress');
    ArchiveMeeting(
      { meetingId: data.meetingId, parentId: data.parentId },
      () => {
        // Success Callback
        handleArchiveConfirmClose();
        setArchiveBtnQuery("");
      },
      err => {
        // failure callback
        setArchiveBtnQuery("");
        handleArchiveConfirmClose();
        if (err.data && err.data.message) showSnackBar(err.data.message, "error");
      },
      dispatch
    );
    handleClose();
  };
  const handleUnArchive = e => {
    if (e) e.stopPropagation();
    setUnarchiveBtnQuery('progress');
    handleCloseCallBack();
    UnArchiveMeeting(
      { meetingId: data.meetingId, parentId: data.parentId },
      //success
      () => {
        handleArchiveConfirmClose();
        setUnarchiveBtnQuery('');
      },
      //failure
      error => {
        setUnarchiveBtnQuery('');
        showSnackBar("Meeting Cannot be UnArchived", "error");
      },
      dispatch
    );
  };
  //Confirmation Dialog close
  const closeDeleteConfirmation = () => {
    dispatch(setDeleteDialogueState({ open: false, btnQuery: "" }));
  };
  //Open delete confirmation dialog
  const handleDeleteConfirmation = success => {
    const dialogObj = {
      open: true,
      cancelBtnText: "Cancel",
      successBtnText: "Delete",
      headingText: "Delete",
      msgText: `Are you sure you want to delete this Meeting?`,
      successAction: deleteTask,
      closeAction: closeDeleteConfirmation,
      btnQuery: "",
    };
    dispatch(setDeleteDialogueState(dialogObj));
    handleClose();
  };
  //Open Cancel confirmation dialog
  const handleCancelConfirmation = () => {
    const dialogObj = {
      open: true,
      cancelBtnText: "Cancel",
      successBtnText: "Cancel Meeting",
      headingText: "Cancel",
      msgText: `Are you sure you want to Cancel this Meeting?`,
      successAction: cancelMeeting,
      closeAction: closeDeleteConfirmation,
      btnQuery: "",
    };
    dispatch(setDeleteDialogueState(dialogObj));
    handleClose();
  };
  // change color
  const colorChange = color => {
    const obj = { colorCode: color };
    handleClose();
    updateMeetingData(
      { meeting: data, obj },
      dispatch,
      //Success
      meeting => { },
      err => {
        if (err && err.data) showSnackBar(err.data.message, "error");
      }
    );
  };
  // permissions allowed 
  let cancelPer = meetingPermission ? meetingPermission.cancelMeeting.cando : true;
  let archivePer = meetingPermission ? meetingPermission.archiveMeetings.cando : true;
  let unarchivePer = meetingPermission ? meetingPermission.unarchiveMeetings.cando : true;
  let deletePer = meetingPermission ? meetingPermission.deleteMeeting.cando : true;

  const isOpen = Boolean(open);
  return (
    <>
      <CustomIconButton
        btnType="transparent"
        buttonRef={node => (anchorEl.current = node)}
        style={{ padding: 0 }}
        {...btnProps}>
        <MoreVerticalIcon className={classes.ellipsesIcon} />
      </CustomIconButton>
      <DropdownMenu
        open={isOpen}
        closeAction={handleClose}
        anchorEl={anchorEl.current}
        size={"small"}
        placement="bottom-end"
        disablePortal={false}>
        <CustomMenuList>
          <CustomListItem
            subNav={true}
            popperProps={{
              placement: "left-start",
            }}
            subNavRenderer={
              <>
                <ColorPicker
                  triangle="hide"
                  onColorChange={colorChange}
                  selectedColor={data.colorCode || ""}
                  width={190}
                />
              </>
            }>
            <SvgIcon viewBox="0 0 16.5 16.5" className={classes.icon}>
              <IconColor />
            </SvgIcon>
            Color
          </CustomListItem>
          {cancelPer && !isArchived && data.status != "Cancelled" && data.status != "InReview" && data.status != "Published" && (
            <CustomListItem rootProps={{ onClick: handleCancelConfirmation }}>
              <SvgIcon viewBox="0 0 14 14" className={classes.icon}>
                <CancelIcon />
              </SvgIcon>
              Cancel Meeting
            </CustomListItem>
          )}
          {archivePer && !isArchived && (
            <CustomListItem rootProps={{ onClick: handleArchiveConfirmation }}>
              <SvgIcon viewBox="0 0 14 14.014" className={classes.icon}>
                <IconArchive />
              </SvgIcon>
              Archive Meeting
            </CustomListItem>
          )}
          {unarchivePer && isArchived && (
            <CustomListItem rootProps={{ onClick: handleUnArchiveConfirmation }}>
              <SvgIcon viewBox="0 0 14 14.014" className={classes.icon}>
                <IconArchive />
              </SvgIcon>
              Un Archive
            </CustomListItem>
          )}
          {deletePer && data.status != "InReview" && data.status != "Published" && (
            <CustomListItem rootProps={{ onClick: handleDeleteConfirmation }}>
              <SvgIcon viewBox="0 0 14 14.002" className={classes.icon}>
                <IconDelete />
              </SvgIcon>
              Delete Meeting
            </CustomListItem>
          )}
        </CustomMenuList>
      </DropdownMenu>
      {archiveConfirmation ? (
        <ActionConfirmation
          open={true}
          closeAction={handleArchiveConfirmClose}
          cancelBtnText={
            <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
          }
          successBtnText={
            <FormattedMessage
              id="common.action.archive.confirmation.archive-button.label"
              defaultMessage="Archive"
            />
          }
          alignment="center"
          headingText={
            <FormattedMessage
              id="common.action.archive.confirmation.archive-button.label"
              defaultMessage="Archive"
            />
          }
          iconType="archive"
          msgText={
            <FormattedMessage
              id="common.archived.tasks.messageb"
              defaultMessage="Are you sure you want to archive this {label}?"
              values={{
                label: 'meeting'
              }}
            />
          }
          successAction={archiveTask}
          btnQuery={archiveBtnQuery}
        />
      ) : null}
      {unArchiveConfirmation ? (
        <ActionConfirmation
          open={unArchiveConfirmation}
          closeAction={handleArchiveConfirmClose}
          cancelBtnText={
            <FormattedMessage
              id="common.action.un-archive.confirmation.cancel-button.label"
              defaultMessage="Cancel"
            />
          }
          successBtnText={
            <FormattedMessage
              id="common.action.un-archive.confirmation.un-archive-button.label"
              defaultMessage="Unarchive"
            />
          }
          alignment="center"
          iconType="unarchive"
          headingText={
            <FormattedMessage
              id="common.action.un-archive.confirmation.title"
              defaultMessage="Unarchive"
            />
          }
          msgText={
            <FormattedMessage
              id="common.action.un-archive.confirmation.task.label"
              defaultMessage="Are you sure you want to unarchive this {label}?"
              values={{
                label: 'meeting'
              }}
            />
          }
          successAction={handleUnArchive}
          btnQuery={unarchiveBtnQuery}
        />
      ) : null}
    </>
  );
}, areEqual);

export default compose(
  withSnackbar,
  withStyles(meetingActionDropdownStyles, { withTheme: true })
)(MeetingActionDropdown);

///new
