const getMeetingsPermissions = (currentMeeting, props) => {
  const permission = currentMeeting
    ? currentMeeting.isOwner
      ? false
      : props.workspacePermissionsState.data &&
        props.workspacePermissionsState.data.meeting
      ? props.workspacePermissionsState.data.meeting
      : ""
    : "";
  return permission;
};
const getEditPermissions = (permission, readOnly) => {
  if (permission) {
    const {
      meetingDate,
      meetingTime,
      meetingLocation,
      meetingDuration
    } = permission;
    return (
      meetingDate.edit &&
      meetingTime.edit &&
      meetingLocation.edit &&
      meetingDuration.edit &&
      !readOnly
    );
  }
  return readOnly ? false : true;
};

const getReadOnlyPermissions = currentMeeting => {
  let readOnly = false;
  if (
    currentMeeting &&
    (currentMeeting.status === "Cancelled" ||
      currentMeeting.isDeleted ||
      currentMeeting.status === "Published")
  ) {
    readOnly = true;
  }
  return readOnly;
};
function canDo(permissions, key){
  return permissions[key]
 }
 export function canAdd(meeting, permissions, key){
  return meeting.isOwner ? true : permissions[key].add
  
  }
export { getEditPermissions, getMeetingsPermissions, getReadOnlyPermissions, canDo };
