import { columns } from "./constants";

export const sortingDropdownData = [
  { key: "", name: "None" },
  { key: "uniqueId", name: "ID", },
  { key: "meetingDisplayName", name: "Title" },
  { key: "startDate", name: "Date" },
  { key: "startTime", name: "Time" },
  { key: "status", name: "Status" },
  { key: "timeDuration", name: "Duration" },
  { key: "location", name: "Location"}
]
export const groupingDropdownData = [columns.meetingNone, columns.meetingStatus_C, columns.location_C, columns.taskTitle_C]