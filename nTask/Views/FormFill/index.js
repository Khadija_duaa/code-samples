import { withStyles } from "@material-ui/core/styles";
import SvgIcon from "@material-ui/core/SvgIcon";
import AddIcon from "@material-ui/icons/AddCircleRounded";
import moment from "moment";
import queryString from "query-string";
import React, { useEffect, useState } from "react";
import Recaptcha from "react-google-recaptcha";
import { Helmet } from "react-helmet";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
import * as Survey from "survey-core";
import "survey-core/defaultV2.css";
import * as SurveyReact from "survey-react-ui";
import PoweredByNTask from "../../components/Icons/PoweredByNTask";
import { getFormById, getLocationAPI, saveFormFillData } from "../../redux/actions/forms";
import AccessDenied from "./AccessDenied";
import NotFoundScreen from "./NotFoundScreen";
import styles from "./styles";
import TimerScreen from "./timerScreen";
let TempForm = null;
Survey.StylesManager.applyTheme("defaultV2");
let recaptchaState = null;
function SurveyView(params) {
  const { classes, location } = params;
  const searchQuery = location.search;
  const formId = queryString.parse(searchQuery).id;
  const [formJSON, setFormJSON] = useState(null);
  const [tempData, setTempData] = useState(null);
  const [locAccScreen, setLocAccScreen] = useState(null);
  const [locAccErr, setLocAccErr] = useState(null);
  const [getAccessbtn, setGetAccessbtn] = useState(null);
  const [submitAnotherRes, setSubmitAnotherRes] = useState(null);
  const [model, setModel] = useState(null);

  const onValueChanged = (result) => {
    console.log("value changed!", result.data);
  };

  const verifyCallback = (res) => {
    if (res) recaptchaState = true;
    else recaptchaState = false;
  };

  const handleSubmitAnotherRes = () => {
    setSubmitAnotherRes(null);
    createSurveyObj(formJSON);
  }

  const onComplete = (result, options) => {
    let formdata = { ...result.data, formId: formId };
    console.log("Complete! ", result.data, options);
    if (formJSON?.formSettings?.multipleSubmission) setSubmitAnotherRes(true);

    saveFormFillData(
      formdata,
      (succ) => {
        console.log("Complete! ", succ);
      },
      (fail) => {
        console.log("error! ", fail);
      }
    );
  };
  const onCompleting = (result, options) => {
    const {
      formSettings: { showCaptcha },
    } = formJSON;
    if (showCaptcha) {
      if (recaptchaState) {
        options.allowComplete = true;
        return;
      }
      alert("Please verify if you are a human!");
      options.allowComplete = false;
    } else {
      options.allowComplete = true;
    }
  };
  const checkStateCity = (value, comparison) => {
    if (value) return comparison.includes(value);
    return true;
  };
  const handleGetForm = () => {
    getFormById(
      formId,
      (succ) => {
        // console.log(succ.data);
        const {
          formSettings: { locationSettings, locationInfo },
        } = succ.data;

        if (locationSettings && locationInfo?.length) {
          TempForm = succ.data;
          getLocation()
          /** if user have selected the location access then check the country ans city */



        } else {
          setFormJSON(succ.data);
          createSurveyObj(succ.data);
        }
      },
      (failure) => {
        let errMessage = failure?.data?.message || "Cannot able to access the form details!"
        setLocAccErr(errMessage);
        setGetAccessbtn(false);
        setLocAccScreen(true);
      }
    );
  };
  const displayLocation = (latitude, longitude) => {
    getLocationAPI(
      latitude,
      longitude,
      (locData) => {
        const {
          formSettings: { locationSettings, locationInfo },
        } = TempForm;

        const isExistLocation = locationInfo.find(
          (item) =>
            locData.country.includes(item.country) &&
            checkStateCity(item.state, locData.region) &&
            checkStateCity(item.city, locData.locality)
        );

        if (isExistLocation) {
          setFormJSON(TempForm);
          createSurveyObj(TempForm);
        } else {
          setLocAccErr("The request page requires proper authorization for access!");
          setLocAccScreen(true);
          setGetAccessbtn(false);
        }
        // handleGetForm(locData);
      },
      (err) => {
        setLocAccErr("Cannot able to access the location details!");
        setLocAccScreen(true);
        setGetAccessbtn(false);
      }
    );
  };

  const showPosition = (position) => {
    let lat = position.coords.latitude;
    let lon = position.coords.longitude;
    displayLocation(lat, lon);
  };

  const showError = (error) => {
    switch (error.code) {
      case error.PERMISSION_DENIED:
        setLocAccErr(
          "This page is region-specific. Please give your location permission to continue!"
        );
        setGetAccessbtn(true);
        break;
      case error.POSITION_UNAVAILABLE:
        setLocAccErr("Location information is unavailable!");
        break;
      case error.TIMEOUT:
        setLocAccErr("The request to get user location timed out!");
        setGetAccessbtn(true);
        break;
      case error.UNKNOWN_ERROR:
        setLocAccErr("An unknown error occurred!");
        setGetAccessbtn(true);

        break;
    }
    setLocAccScreen(true);
  };

  const getLocation = () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition, showError);
    } else {
      setLocAccErr("Geolocation is not supported by this browser!");
      setLocAccScreen(true);
    }
  };

  useEffect(() => {
    // getLocation();
    handleGetForm();
  }, []);


  const createSurveyObj = (obj) => {
    if (obj) {
      let model = new Survey.Model(obj);
      setModel(model);
    }
  }

  useEffect(() => {
    if (formJSON) {
      document.title = `${formJSON.title} - nTask`;
    }
    if (model) {
      var element = document.getElementsByClassName("sd-body__page");
      const captcha = document.getElementById("captcha");
      element.length && captcha && element[0].appendChild(captcha);
    }
  }, [model])

  if (formJSON && model) {
    const {
      formSettings: { publishSettings, publishDate, unPublishSettings, unPublishDate, showCaptcha },
    } = formJSON;

    const obj = {
      publishDate: publishSettings && publishDate,
      unPublishDate: unPublishSettings && unPublishDate,
      preAccessPublish: function () {
        return moment(this.publishDate).isAfter(new Date());
      },
      postAccessPublish: function () {
        return moment(this.publishDate).isBefore(new Date());
      },
      preAccessUnPublish: function () {
        return moment(this.unPublishDate).isAfter(new Date());
      },
      postAccessUnPublish: function () {
        return moment(this.unPublishDate).isBefore(new Date());
      },
      timer: function () {
        return this.publishDate && this.preAccessPublish();
      },
      inValid: function () {
        return this.postAccessUnPublish();
      },
    };

    if (obj.timer()) {
      return <TimerScreen formData={formJSON} />;
    }
    if (obj.inValid()) {
      return <NotFoundScreen formData={formJSON} />;
    }
    if (!obj.timer() && !obj.inValid()) {
      return (
        <div className={classes.container}>
          {/* <Helmet>
            <title>
              {`${formJSON.title} - nTask`}
            </title>
          </Helmet> */}
          <SurveyReact.Survey
            model={model}
            onComplete={onComplete}
            onValueChanged={onValueChanged}
            onCompleting={onCompleting}
          />
          {showCaptcha && (
            <div style={{ backgroundColor: "#f3f3f3", marginTop: 16 }} id="captcha">
              <Recaptcha
                sitekey={"6LdOlx4gAAAAANXtTLCLKeukSdfOn_kembQYNiil"}
                render={"explicit"}
                onChange={verifyCallback}
              />
            </div>
          )}
          <div>
            {submitAnotherRes && <span className={classes.anotherResTxt} onClick={handleSubmitAnotherRes}>Submit another response</span>}
            <div className={classes.poweredCnt} style={{ margin: "0 auto", marginTop: 30 }}>
              <div
                className={classes.itemCnt}
                onClick={() => window.open("https://www.ntaskmanager.com/", "_blank")}>
                <AddIcon /> <span>Start creating your own form using nTask</span>
              </div>
              <SvgIcon viewBox="0 0 157.01 20.14">
                <PoweredByNTask />
              </SvgIcon>
            </div>
          </div>
        </div>
      );
    }
  } else if (locAccScreen) {
    return (
      <AccessDenied errMessage={locAccErr} getAccessbtn={getAccessbtn} getLocation={getLocation} />
    );
  } else return <>
    <div className="loaderContainer">
      <div className="loader"></div>
    </div>
  </>;
}

SurveyView.defaultProps = {
  classes: {},
  theme: {},
};

export default compose(withRouter, withStyles(styles, { withTheme: true }))(SurveyView);
