import { withStyles } from "@material-ui/core/styles";
import CancelIcon from "@material-ui/icons/Cancel";
import React from "react";
import { compose } from "redux";
import styles from "./styles";
import LockIcon from '@material-ui/icons/Lock';
import LocationImg1 from "../../assets/images/locationImg1.svg";
import LocationImg2 from "../../assets/images/locationImg2.svg";

function AccessDenied(params) {
  const { theme, classes, errMessage, getAccessbtn, getLocation } = params;
  return (
    <div className={classes.deniedCnt}>
      <CancelIcon className={classes.cancelIcon} />
      <span className={classes.accessLabel}>Access Denied</span>
      <span className={classes.accessSubLabel}>
        You don't have permission to view this page. <br /> {errMessage}
      </span>
      {getAccessbtn && (

        <div>
          <span className={classes.accessPromtLabel} onClick={getLocation}>
            Allow access
          </span>


          <div className={classes.locationLimit}>
            <h2>How to enable location access on your browser</h2>
            <p>On your browser:</p>
            <div className={classes.locationListTag}>
              <span>1</span>
              To the left of the address bar, click Padlock
              <LockIcon />
              icon.
            </div>

            <img src={LocationImg1}
              style={{
                transform: 'translateX(-9%)',
              }}
              alt="ArrowCondition" />

            <div className={classes.locationListTag}>
              <span>2</span>
              Switch on the Location toggle to enable location access.
            </div>

            <img src={LocationImg2} alt="ArrowCondition" />
          </div>
        </div>
      )}
    </div>
  );
}
AccessDenied.defaultProps = {
  classes: {},
  theme: {},
  errMessage: "",
  getAccessbtn: false,
  getLocation: () => { },
};

export default compose(withStyles(styles, { withTheme: true }))(AccessDenied);
