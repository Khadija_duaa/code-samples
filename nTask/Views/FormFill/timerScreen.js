import { withStyles } from "@material-ui/core/styles";
import SvgIcon from "@material-ui/core/SvgIcon";
import AddIcon from "@material-ui/icons/AddCircleRounded";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { compose } from "redux";
import LogoV2 from "../../assets/images/nTask-Logo-Black.svg";
import CustomButton from "../../components/Buttons/CustomButton";
import PoweredByNTask from "../../components/Icons/PoweredByNTask";
import styles from "./styles";

function TimerScreen(params) {
  const { theme, classes, formData } = params;
  const {
    formSettings: {
      publishCustomBranding,
      publishDate,
      publishDescription,
      publishRedirectBtnText,
      publishRedirectUrl,
      publishBgColor,
      publishFontColor,
      publishRedirectBtnColor,
      publishLogoUrl,
      publishLogoTagline,
      publishBgImg,
    },
  } = formData;
  const [days, setDays] = useState(0);
  const [hours, setHours] = useState(0);
  const [min, setMin] = useState(0);
  const [sec, setSec] = useState(0);

  useEffect(() => {
    const eventTime = moment(publishDate).valueOf();
    const currentTime = moment(new Date()).valueOf();
    const diffTime = eventTime - currentTime;
    let duration = moment.duration(diffTime, "milliseconds");
    const interval = 1000;
    setInterval(() => {
      duration = moment.duration(duration - interval, "milliseconds");
      setDays(duration.days());
      setHours(duration.hours());
      setMin(duration.minutes());
      setSec(duration.seconds());
      if(duration.days() == 0 && duration.hours() == 0 && duration.minutes() == 0 && duration.seconds() == 0){
        window.location.reload(false);
      }
    }, 1000);

    return () => { };
  }, []);

  return (
    <div
      className={classes.formContent}
      style={{
        backgroundColor: publishCustomBranding && publishBgColor ? publishBgColor : "white",
        color: publishCustomBranding && publishFontColor ? publishFontColor : "#2d2d2d",
        backgroundImage: publishCustomBranding && publishBgImg ? `url(${publishBgImg})` : "",
      }}>
      <div className={classes.generalContent}>
        <div className={classes.pageWrapper}>
          <div className={classes.brandLogo}>
            <a href={"https://www.ntaskmanager.com/"} target={"_blank"}>
              <img className={classes.ntaskLogoImg} src={publishCustomBranding && publishLogoUrl ? publishLogoUrl : LogoV2} alt={"Logo"} />
            </a>
          </div>
          <span className={classes.label}>{publishCustomBranding && publishLogoTagline ? publishLogoTagline : `Get More Done with nTask`}</span>
          <div className={classes.pageLayout}>
            <div className={classes.intermediatePage}>
              <div>
                <div className={classes.countForm}>
                  <div
                    className={classes.countItem}
                    style={{ borderColor: publishCustomBranding && publishFontColor ? publishFontColor : "#00cc90" }}>
                    {days} <span>Days</span>
                  </div>
                  <div
                    className={classes.countItem}
                    style={{ borderColor: publishCustomBranding && publishFontColor ? publishFontColor : "#00cc90" }}>
                    {hours} <span>hours</span>
                  </div>
                  <div
                    className={classes.countItem}
                    style={{ borderColor: publishCustomBranding && publishFontColor ? publishFontColor : "#00cc90" }}>
                    {min} <span>minutes</span>
                  </div>
                  <div
                    className={classes.countItem}
                    style={{ borderColor: publishCustomBranding && publishFontColor ? publishFontColor : "#00cc90" }}>
                    {sec} <span>seconds</span>
                  </div>
                </div>
              </div>
              <div class={classes.indermediateText}>
                {publishCustomBranding && publishDescription ? publishDescription : "This form will be available soon."}
              </div>
              {publishCustomBranding && publishRedirectBtnText && (
                <CustomButton
                  onClick={() => {
                    window.open(publishRedirectUrl, "_blank");
                  }}
                  btnType="green"
                  style={{
                    marginTop: "1em",
                    color: publishFontColor ? publishFontColor : "white",
                    backgroundColor: publishRedirectBtnColor ? publishRedirectBtnColor : "#00CC90",
                  }}
                  variant="contained"
                  query={""}
                  disabled={false}>
                  {publishRedirectBtnText}
                </CustomButton>
              )}
            </div>
            <div className={classes.poweredCnt}>
              <div
                className={classes.itemCnt}
                onClick={() => window.open("https://www.ntaskmanager.com/", "_blank")}>
                <AddIcon /> <span>Start creating your own form using nTask</span>
              </div>
              <SvgIcon viewBox="0 0 157.01 20.14">
                <PoweredByNTask />
              </SvgIcon>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
TimerScreen.defaultProps = {
  classes: {},
  theme: {},
};

export default compose(withStyles(styles, { withTheme: true }))(TimerScreen);
