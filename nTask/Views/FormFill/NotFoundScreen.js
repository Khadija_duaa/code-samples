import { withStyles } from "@material-ui/core/styles";
import React from "react";
import { compose } from "redux";
import NotfoundImg from "../../assets/images/404.svg";
import LogoV2 from "../../assets/images/nTask-Logo-Black.svg";
import CustomButton from "../../components/Buttons/CustomButton";
import styles from "./styles";
import SvgIcon from "@material-ui/core/SvgIcon";
import AddIcon from "@material-ui/icons/AddCircleRounded";
import PoweredByNTask from "../../components/Icons/PoweredByNTask";

function NotFoundScreen(params) {
  const { theme, classes, formData } = params;
  const {
    formSettings: {
      unPublishCustomBranding,
      unPublishDescription,
      unPublishRedirectBtnText,
      unPublishRedirectUrl,
      unPublishBgColor,
      unPublishFontColor,
      unPublishBgImg,
      unPublishLogoUrl,
      unPublishLogoTagline,
      unPublishRedirectBtnColor,
      unPublishIllustration,
    },
  } = formData;
  return (
    <div
      className={classes.formContent}
      style={{
        backgroundColor: unPublishCustomBranding && unPublishBgColor ? unPublishBgColor : "white",
        color: unPublishCustomBranding && unPublishFontColor ? unPublishFontColor : "#2d2d2d",
        backgroundImage: unPublishCustomBranding && unPublishBgImg ? `url(${unPublishBgImg})` : "",
      }}>
      <div className={classes.generalContent}>
        <div className={classes.pageWrapper}>
          <div className={classes.brandLogo}>
            <a href={"https://www.ntaskmanager.com/"} target={"_blank"}>
              <img
                className={classes.ntaskLogoImg}
                src={unPublishCustomBranding && unPublishLogoUrl ? unPublishLogoUrl : LogoV2}
                alt={"Logo"}
              />
            </a>
          </div>
          <span className={classes.label}>
            {unPublishCustomBranding && unPublishLogoTagline ? unPublishLogoTagline : "Get More Done with nTask"}
          </span>
          <div className={classes.pageLayout}>
            <div className={classes.intermediatePage}>
              <div>
                <img
                  className={classes.ntaskLogoImg}
                  src={unPublishCustomBranding && unPublishIllustration ? unPublishIllustration : NotfoundImg}
                  alt={"404 Image"}
                />
              </div>
              <div class={classes.indermediateText}>
                {unPublishCustomBranding && unPublishDescription ? unPublishDescription : "This form is not available."}
              </div>
              {unPublishCustomBranding && unPublishRedirectBtnText && (
                <CustomButton
                  onClick={() => {
                    window.open(unPublishRedirectUrl, "_blank");
                  }}
                  btnType="green"
                  style={{
                    marginTop: "1em",
                    color: unPublishFontColor ? unPublishFontColor : "white",
                    backgroundColor: unPublishRedirectBtnColor
                      ? unPublishRedirectBtnColor
                      : "#00CC90",
                  }}
                  variant="contained"
                  query={""}
                  disabled={false}>
                  {unPublishRedirectBtnText}
                </CustomButton>
              )}
            </div>
            <div className={classes.poweredCnt}>
              <div
                className={classes.itemCnt}
                onClick={() => window.open("https://www.ntaskmanager.com/", "_blank")}>
                <AddIcon /> <span>Start creating your own form using nTask</span>
              </div>
              <SvgIcon viewBox="0 0 157.01 20.14">
                <PoweredByNTask />
              </SvgIcon>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
NotFoundScreen.defaultProps = {
  classes: {},
  theme: {},
};

export default compose(withStyles(styles, { withTheme: true }))(NotFoundScreen);
