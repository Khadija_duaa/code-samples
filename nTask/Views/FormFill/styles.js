const surveyPageViewStyles = (theme) => ({
  container: {
    // padding: "0 15px 15px",
    height: "100%",
    overflow: "auto",
  },
  formContent: {
    // minHeight: "calc(100vh - 185px)",
    height: "100vh",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    position: "relative",
    transition: "padding-right .3s linear",
    paddingTop: "2.8rem",
    paddingBottom: "2.8rem",
    backgroundSize: "cover",
  },
  generalContent: {
    maxWidth: "800px",
    minHeight: "200px",
    margin: "0 auto",
  },
  pageWrapper: {
    minHeight: "90vh",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
  },
  brandLogo: {
    maxWidth: "181px",
    width: "100%",
  },
  ntaskLogoImg: {
    width: "100%",
    maxWidth: "300px",
  },
  pageLayout: {
    margin: "auto",
    textAlign: "center",
  },
  intermediatePage: {
    maxWidth: "480px",
    textAlign: "center",
    margin: "0 auto",
  },
  countForm: {
    display: "flex",
    justifyContent: "center",
    "& $countItem": {
      fontSize: "3em !important",
      fontWeight: 600,
      textAlign: "center",
      padding: "0.5em",
      border: "2px solid",
      borderRadius: "14px",
      margin: "0.2em",
      width: "3em",
    },
    "& span": {
      display: "block",
      fontSize: "1rem !important",
      marginTop: "1em",
    },
  },
  countItem: {
    borderColor: "#00cc90",
    color: "inherit",
  },
  indermediateText: {
    fontSize: "1.6em !important",
    fontWeight: "600",
    marginTop: "1em",
    color: "inherit",
  },
  label: {
    marginTop: "1em",
    textAlign: "center",
    alignSelf: "center",
  },
  cancelIcon: {
    display: "block",
    width: "auto",
    fontSize: "120px !important",
    marginBottom: "15px",
    color: "#c92127",
  },
  deniedCnt: {
    textAlign: "center",
    height: "100vh",
    display: "flex",
    flexWrap: "nowrap",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    width: "710px",
    margin: "auto",
    maxWidth: "100%",
    lineHeight: "initial",
  },
  accessLabel: {
    fontSize: "44px !important",
    fontWeight: 900,
    color: "#717171",
  },
  accessSubLabel: {
    fontSize: "18px !important",
    fontWeight: 400,
    color: "#717171",
    marginTop: 15,
  },
  accessPromtLabel: {
    fontSize: "18px !important",
    fontWeight: 400,
    color: "#717171",
    marginTop: 15,
    textDecoration: "underline",
    cursor: "pointer",
  },
  poweredCnt: {
    width: 680,
    height: 56,
    border: "1px solid #E8E8E8",
    borderRadius: 8,
    background: "white",
    marginTop: 100,

    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",

    fontSize: "12px !important",
    fontWeight: theme.palette.fontWeightMedium,
    fontFamily: theme.typography.fontFamilyLato,

    "& svg": {
      fontSize: "16px !important",
      color: "#2d2d2d",
    },
  },
  itemCnt: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    fontSize: "16px !important",
    cursor: "pointer",
    color: "#2d2d2d",
    "& svg": {
      fontSize: "20px !important",
      color: "#00cc90",
      marginRight: "5px",
    },
  },
  anotherResTxt: {
    display: "block",
    textAlign: "center",
    marginTop: 20,
    textDecoration: "underline",
    color: "#009d6e",
    fontFamily: theme.typography.fontFamilyLato,
    cursor: "pointer"
  },
  locationLimit: {
    textAlign: 'left',
    fontSize: 14,
    '& h2': {
      fontSize: 16,
    }
  },
  locationListTag: {
    margin: '20px 0',
    '& span': {
      fontSize: 12,
      background: theme.palette.border.extraLightBorder,
      borderRadius: '50%',
      marginRight: 6,
      width: 20,
      height: 20,
      display: 'inline-flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    '& svg': {
      color: "#c92127",
      fontSize: 19,
      transform: 'translateY(4px)',
    }
  },

});

export default surveyPageViewStyles;
