import React from "react";
import { withStyles } from "@material-ui/core/styles";
import TeamsPermissionStyle from "./Styles";
import ntaskLogo from "../../assets/images/nTask32x32.png";
import CustomButton from "../../components/Buttons/CustomButton";
const TeamsPermission = ({ theme,classes,microsoftTeam }) => {
const close = () => {
 alert("do not authorize user");
};
const success = () => {
  window.location = decodeURIComponent(microsoftTeam.redirect_uri)+"?isAuthSuccess=true&code="+microsoftTeam.access_token+"&userName="+microsoftTeam.userName;
};
  return (
    <>
      <div className={classes.mainBg}>
        <div className={classes.innerBox}>
          <div className={classes.textIcon}>
            <span>
              <img src={ntaskLogo} alt="Ntask_Logo" />
            </span>
            <label>Ntask</label>
          </div>
        </div>
        <div className={classes.centerBox}>
            <h2>Grant permission</h2>

<p>NTask for Microsoft Teams is requesting permission to use NTask Connect to access your NTask account.</p>

<b>If you authorize this app</b>, <span style={{color: 'darkblue'}}>you will give it permission to:</span>

<b>The app is already authorized to:</b>

<ul>
    <li>Access your name and email address.</li>
    <li>Create and modify tasks, projects, and comments on your behalf.</li>
    <li>You can deauthorize the app completely from the Apps tab under My Profile Settings.</li>
</ul>


<div style={{display:"flex",justifyContent: "center"}}>
<CustomButton
                btnType="success"
                variant="contained"
                onClick={(e) => success()
                }
                text=""
                style={{
                  height: 35,
                  fontWeight: 400,
                  fontFamily: theme.typography.fontFamilyLato,
                  fontSize: "13px",
                  marginRight:"10px",
                }}
>
Allow</CustomButton>
<CustomButton
                btnType="cancel"
                variant="contained"
                style={{
                  height: 35,
                  fontWeight: 400,
                  fontFamily: theme.typography.fontFamilyLato,
                  fontSize: "13px",
                }}
>Cancel</CustomButton>
</div>
<br/>
Logged in as {microsoftTeam.userName} (Not you?)
        </div>
      </div>
    </>
  );
};

export default withStyles(TeamsPermissionStyle, { withTheme: true })(TeamsPermission);
