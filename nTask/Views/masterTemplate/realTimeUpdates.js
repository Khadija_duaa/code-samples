import defaultPush from "push.js";
import nTaskDesktopIcon from "../../assets/images/nTask-logo32x32.png";
import { getTaskById } from "../../helper/getTaskFromTasksData";
import { getAnyTeamIdExcept } from "../../helper/getTeams";
import { store } from "../../index";
import { updateCurrentWorkspaceCalender } from '../../redux/actions/workspace';
import { nTaskSignalConnectionOpen } from "../../utils/ntaskSignalR";

function realTimeUpdates(
  props,
  switchToWorkspace,
  fetchWorkspaceData,
  FetchWorkspaceInfo,
  removedWorkspace
) {
  const hub = $.connection.notificationHub;
  // var chatHub = $.connection.notificationHub;
  // $.connection.hub.url = `${window.baseUrl}/signalr/hubs`;
  // $.connection.hub.url = `${ENV == "production1" || ENV == "beta1" ? BASE_URL_API : BASE_URL
  //   }signalr/hubs`;
  $.connection.hub.logging = true;
  if (hub) {
    hub.client.onlineUser = (userDetails) => {
      props.UpdateUserIsOnline(userDetails);
    };
  }

  const desktopNotification = (notificationType, payload) => {
    const state = store.getState();
    const companyInfo = state.whiteLabelInfo.data;
    defaultPush.create(notificationType, {
      body: payload.body,
      icon: companyInfo ? companyInfo.notificationlogoUrl : nTaskDesktopIcon,
      onClick() {
        window.focus();
        if (payload.notificationUrl && payload.notificationUrl != "#")
          window.open(payload.notificationUrl, "_blank");
        this.close();
      },
    });
  };

  hub.client.receive = (type, obj) => {
    switch (type.type) {
      case "Task": // This will trigger if the action needs to be performed on Task
        {
          if (location.pathname.indexOf("/reports") == 0) {
            return;
          }
          if (type.action == "Update") {
            // This condition checks what kind of actions needs to be performed, update, edit or delete
            props.dispatchTask(obj, () => { });
          }
          if (type.action == "InAppUpdate") {
            props.AddNotificationsInfo(obj); /** update notifications in App header */
            props.UpdateTaskNotifications(obj); /** update task specific notifications */
          }
          if (type.action == "DesktopNotification") {
            desktopNotification("Task Notification", obj);
          }
          if (type.action == "Delete") {
            // This condition checks what kind of actions needs to be performed, update, edit or delete
            props.dispatchTaskDelete(obj);
          }
          if (type.action == "Create") {
            // This condition checks what kind of actions needs to be performed, update, edit or delete
            props.dispatchNewTask({ ...obj, isNew: true });
          }
          if (type.action == "Comment") {
            // This condition checks what kind of actions needs to be performed, update, edit or delete
            props.AddNewTaskCommentData(obj);
          }
          if (type.action == "StartTaskTimer") {
            //
            const currentTask = getTaskById(obj.taskId);
            props.dispatchStartTimer(obj, currentTask);
          }
          if (type.action == "StopTaskTimer") {
            //
            props.dispatchStopTimer(obj);
          }
          if (type.action == "BulkUpdate") {
            // This condition checks  whether to update tasks in bulk from store
            props.UpdateBulkStoreTasks(obj, false);
          }
          if (type.action == "BulkDelete") {
            // This condition checks  whether to delete tasks in bulk from store
            props.DeleteBulkStoreTask(obj);
          }
          if (type.action == "BulkCreate") {
            // This condition checks  whether to delete tasks in bulk from store
            props.BulkAddTask(obj);
          }
          if (type.action == "Archive") {
            // This condition checks  whether to update tasks Archive from the store
            props.dispatchArchiveTask(obj);
          }
          if (type.action == "CopyTask") {
            props.dispatchCopyTask(obj);
          }
        }
        break;
      case "TaskProgress":
        {
          if (location.pathname.indexOf("/reports") == 0) {
            return;
          }
          if (type.action == "Update") {
            props.updateTask(obj);
          }
        }
        break;
      case "Issue": // This will trigger if the action needs to be performed on Issue
        {
          if (location.pathname.indexOf("/reports") == 0) {
            return;
          }
          if (type.action == "InAppUpdate") {
            props.AddNotificationsInfo(obj);
            props.UpdateIssueNotifications(obj);
          }
          if (type.action == "DesktopNotification") {
            desktopNotification("Issue Notification", obj);
          }
          if (type.action == "Update") {
            // This condition checks what kind of actions needs to be performed, update, edit or delete
            props.dispatchIssue(obj);
          }
          if (type.action == "Delete") {
            // This condition checks what kind of actions needs to be performed, update, edit or delete
            props.dispatchIssueDelete(obj);
          }
          if (type.action == "Create") {
            // This condition checks what kind of actions needs to be performed, update, edit or delete
            props.dispatchNewIssue({ ...obj, isNew: true });
          }
          if (type.action == "Comment") {
            // This condition checks what kind of actions needs to be performed, update, edit or delete
            props.AddNewIssueCommentData(obj);
          }
          if (type.action == "BulkUpdate") {
            // This condition checks  whether to update issues in bulk from store
            props.UpdateBulkStoreIssue(obj, false);
          }
          if (type.action == "BulkDelete") {
            // This condition checks whether to delete issues in bulk from store
            props.UpdateBulkStoreIssue(obj, true);
          }
          if (type.action == "Archive") {
            // This condition checks whether to delete issues in bulk from store
            props.dispatchArchiveIssue(obj);
          }
        }
        break;
      case "Meeting": // This will trigger if the action needs to be performed on Issue
        {
          if (type.action == "InAppUpdate") {
            props.AddNotificationsInfo(obj);
          }
          if (type.action == "DesktopNotification") {
            desktopNotification("Meeting Notification", obj);
          }
          if (type.action == "Update") {
            // This condition checks what kind of actions needs to be performed, update, edit or delete
            props.dispatchMeeting(obj);
          }
          if (type.action == "Delete") {
            // This condition checks what kind of actions needs to be performed, update, edit or delete
            props.dispatchMeetingDelete(obj);
          }
          if (type.action == "Archive") {
            // This condition checks what kind of actions needs to be performed, update, edit or delete
            props.dispatchMeetingDelete(obj.meetingId);
          }
          if (type.action == "Create") {
            // || type.action == "Unarchive"
            // This condition checks what kind of actions needs to be performed, update, edit or delete
            props.dispatchNewMeeting(obj);
          }
          if (type.action == "PublishMeeting" || type.action == "SubmitMeeting" || type.action == "CancelMeeting") {
            // This condition checks what kind of actions needs to be performed, update
            props.dispatchMeeting(obj);
          }
          if (type.action == "BulkUpdate") {
            // This condition checks what kind of actions needs to be performed, update, edit
            props.UpdateBulkStoreMeeting(obj, false);
          }
          if (type.action == "BulkDelete") {
            // This condition checks what kind of actions needs to be performed, update, edit
            props.UpdateBulkStoreMeeting(obj, true);
          }
        }
        break;

      case "Project": // This will trigger if the action needs to be performed on Issue
        {
          if (type.action == "InAppUpdate") {
            props.AddNotificationsInfo(obj);
          }
          if (type.action == "DesktopNotification") {
            desktopNotification("Project Notification", obj);
          }
          if (type.action == "Archive") {
            // This condition checks what kind of actions needs to be performed, update, edit or delete
            props.dispatchArchiveProject(obj.projectId);
            FetchWorkspaceInfo("", () => { });
          }
          if (type.action == "Update") {
            // This condition checks what kind of actions needs to be performed, update, edit or delete
            props.dispatchProject(obj);
          }
          if (type.action == "Delete") {
            // This condition checks what kind of actions needs to be performed, update, edit or delete
            props.dispatchProjectDelete(obj);
          }
          if (type.action == "ProjectSetting") {
            // This condition checks what kind of actions needs to be performed, update, edit or delete or Project Setting
            const projectManager = [];
            const resources = [];
            obj.resources.forEach((resource) => {
              resources.push(resource.userId);
              if (resource.isProjectManager) projectManager.push(resource.userId);
            });
            props.dispactchProjectPartially({
              projectId: obj.projectId,
              projectManager,
              resources,
            });
          }
          if (type.action == "Duplicate") {
            // This condition checks what kind of actions needs to be performed, update, edit , delete or Duplicate
            props.dispatchProjectCopy(obj.project);
            FetchWorkspaceInfo(obj.project.teamId, (response) => { });
          }
          if (type.action == "MoveProject") {
            // This condition checks what kind of actions needs to be performed, update, edit , delete , Duplicate or Move
            FetchWorkspaceInfo("", () => { });
          }
          if (type.action == "EditProjectName") {
            // This condition checks what kind of actions needs to be performed, update, edit , delete , Duplicate or Move
            props.dispatchRenameProject(obj);
          }
          if (type.action == "Create") {
            // This condition checks what kind of actions needs to be performed, update, edit or delete
            props.dispatchNewProject({ ...obj, isNew: true });
          }
          if (type.action == "BulkUpdate") {
            // This condition checks  whether to update projects in bulk from store
            props.UpdateBulkStoreProjects(obj);
          }
          if (type.action == "UpdateProjectInTask") {
            // This condition checks  whether to update projects in bulk from store
            props.toggleProjectInStore(obj);
          }
          if (type.action == "RemoveProject") {
            // This condition checks  whether to update projects in bulk from store
            obj = { projectId: obj };
            props.toggleProjectInStore(obj);
          }
        }
        // code block
        break;

      case "Risk": // This will trigger if the action needs to be performed on Risk
        {
          if (type.action == "InAppUpdate") {
            props.AddNotificationsInfo(obj);
            props.UpdateRiskNotifications(obj);
          }
          if (type.action == "DesktopNotification") {
            desktopNotification("Risk Notification", obj);
          }
          if (type.action == "Update") {
            // This condition checks what kind of actions needs to be performed, update, edit or delete
            props.dispatchRisk(obj);
          }
          if (type.action == "Archive") {
            // This condition checks what kind of actions needs to be performed, update, edit or delete or Archive
            props.dispatchRiskArchive(obj);
          }
          if (type.action == "Delete") {
            // This condition checks what kind of actions needs to be performed, update, edit or delete
            props.dispatchRiskDelete(obj);
          }
          if (type.action == "Create") {
            // This condition checks what kind of actions needs to be performed, update, edit or delete
            props.dispatchNewRisk({ ...obj, isNew: true });
          }
          if (type.action == "Comment") {
            // This condition checks what kind of actions needs to be performed, update, edit or delete
            props.AddNewRiskCommentData(obj);
          }
          if (type.action == "BulkUpdate") {
            // This condition checks  whether to update risks in bulk from store
            props.UpdateBulkStoreRisk(obj, false);
          }
          if (type.action == "BulkDelete") {
            // This condition checks whether to delete risks in bulk from store
            props.UpdateBulkStoreRisk(obj, true);
          }
        }
        break;

      case "Gantt": // This will trigger if the action needs to be performed on Gantt
        {
          if (type.action == "Update") {
            // This condition checks what kind of actions needs to be performed, update, edit or delete
            props.dispatchGanttUpdate(obj);
          }
          if (type.action == "TaskComplete") {
            // This condition checks what kind of actions needs to be performed, update, edit or delete
            props.dispatchTaskComplete(obj);
          }
          if (type.action == "SaveTaskLink") {
            // This condition checks what kind of actions needs to be performed, update, edit or delete
            props.dispatchTasksLink(obj);
          }
          if (type.action == "DeleteTaskLink") {
            // This condition checks what kind of actions needs to be performed, update, edit or delete
            props.dispatchDeleteTasksLink(obj);
          }
        }
        break;
      case "Team":
        {
          if (type.action == "InviteMember") {
            props.dispatchNotification(obj[0]);
          }
          if (type.action == "updateTeamMember") {
            props.dispatchTeamMember(obj, "update");
          }
        }
        break;
      case "Workspace":
        {
          // This will trigger if the action needs to be performed on Workspace & Its Members
          if (type.action == "UpdateRole") {
            // This condition checks what kind of actions needs to be performed, update Member & Permissions
            // const { member, permission } = obj;
            // props.UpdateMember(member, 'update');
            // props.PopulatePermissions(permission);
            $.connection.hub.stop();
            fetchWorkspaceData();
          } else if (type.action == "InviteMember") {
            // This condition checks what kind of actions needs to be performed, update Member & Permissions
            const member = obj[0];
            props.UpdateMember(member, "insert");
          } else if (type.action == "DeleteMember") {
            // This condition checks what kind of actions needs to be performed, delete Member
            const member = { userId: obj };
            props.UpdateMember(member, "delete");
          } else if (type.action == "EnableMember") {
            // This condition checks what kind of actions needs to be performed, update Member & Permissions
            const { team } = obj;
            props.AddNewTeam(team);
          } else if (type.action == "OtherMemberEnable") {
            const { userId } = obj;
            // Just update status of enabled member
            const member = { userId, isActive: true };
            props.UpdateMember(member, "updatePartially");
          } else if (type.action == "DisableMember") {
            const { oldTeamId } = obj;
            removedWorkspace(true, oldTeamId); /** callback to master template */
            // Switch workspace of this user
            // $.connection.hub.stop();
            // switchToWorkspace(newTeamId);
          } else if (type.action == "OtherMemberDisable") {
            const { userId } = obj;
            // Just update status of disabled member
            const member = { userId, isActive: false };
            props.UpdateMember(member, "updatePartially");
          } else if (type.action == "DisableMemberToOtherWorkspace") {
            const { workspaceId } = obj;
            props.RemoveTeamFromList(workspaceId);
          } else if (type.action == "Delete") {
            const teamId = obj;
            if (props.profileState.data.loggedInTeam === teamId) {
              const newTeamId = getAnyTeamIdExcept(teamId);
              if (newTeamId) {
                // Switch workspace of this user
                $.connection.hub.stop();
                switchToWorkspace(newTeamId);
              } else {
                // navigate user to create workspace page
                this.props.history.push("/no-workspace-found");
              }
            }
            // Remove workspace from workspace list
            else props.RemoveTeamFromList(teamId);
          } else if (type.action == "WorkSpaceUpdate") {
            const { workSpace } = obj;
            props.UpdateTeam(workSpace);
          } else if (type.action == "ChangeWorkspace") {
            const teamId = obj;
            // Switch workspace of this user
            $.connection.hub.stop();
            switchToWorkspace(teamId);
          } else if (type.action == "ChangeItemOrder") {
            const { order } = obj;
            props.PopulateItemOrder(order);
          } else if (type.action == "Update") {
            store.dispatch(updateCurrentWorkspaceCalender(obj.calendar));
          }
        }
        break;
      case "TodoItem":
        {
          props.dispatchAddTodoItems(obj, type.action);
        }
        break;
      case "NTaskTemplate":
        {
          props.dispatchTemplatesActions(type.action, obj);
          // if (type.action == "CreateTemplate") {
          // }
          // if (type.action == "SetDefaultTemplate") {
          // }
        }
        break;
      case "Board":
        {
          props.singalRBoards(type.action, obj);
        }
        break;
      case "Kanban":
        {
          props.singalRKanban(type.action, obj);
        }
        break;
      case "KanbanTask":
        {
          props.singalRKanbanTask(type.action, obj);
        }
        break;
      case "TaskStatus":
        {
          props.UpdateTasksInstore(type.action, obj);
        }
        break;
      case "KanbanLogs":
        {
          props.UpdateKanbanLogsInstore(type.action, obj);
        }
        break;
      case "CustomField":
        {
          props.UpdateCustomFieldsSignalR(type.action, obj);
        }
        break;
      case "CustomFieldData":
        {
          if (obj.groupType === "risk") {
            props.UpdateRiskCustomFieldData({ type: type.action, obj });
          }
          if (obj.groupType === "issue") {
            props.UpdateIssueCustomFieldData({ type: type.action, obj });
          }
          if (obj.groupType === "task") {
            props.UpdateTaskCustomFieldData({ type: type.action, obj });
          }
          if (obj.groupType === "project") {
            props.UpdateProjectCustomFieldData({ type: type.action, obj });
          }
        }
        break;
      case "GridColumns":
        {
          props.addGridColumns(type.action, obj.newColumns);
        }
        break;
      case "BoardBackground":
        {
          props.updateBoardBackgroundSettings(type.action, { ...obj, itemKey: "background" });
        }
        break;
      case "BoardCardSettings":
        {
          props.updateBoardBackgroundSettings(type.action, { ...obj, itemKey: "cardSetting" });
        }
        break;

      case "Calender":
        {
          if (type.action === "Update") {
            const profile = store.getState().profile.data
            const activeWorkspace = profile.workspace.find((item) => item.teamId === profile.loggedInTeam)

            if (activeWorkspace.calendar.calenderId === obj.calenderId) store.dispatch(updateCurrentWorkspaceCalender(obj))
          }
        }

      default:
        break;
    }
    if (obj && obj.type) {
      switch (obj.type.context) {
        case "chat":
          {
            if (obj.type.action == "Updated") {
              // This condition checks what kind of actions needs to be performed, update, edit or delete
              props.dispatchChatUpdate(obj.groupId, obj.update, () => { });
            }
            if (obj.type.action == "Deleted") {
              // This condition checks what kind of actions needs to be performed, update, edit or delete
              props.dispatchChatDelete(obj.groupId, obj.update);
              props.updateTaskCommentCountKanban("Delete", obj.groupId);
            }
            if (obj.type.action == "Created") {
              // This condition checks what kind of actions needs to be performed, update, edit or delete
              props.dispatchChatNew(obj.groupId, obj.update);
              props.updateTaskCommentCountKanban("Add", obj.groupId);
            }
            if (obj.type.action == "ChatTypingStatus") {
              // This condition checks what kind of actions needs to be performed, update, edit or delete
              props.dispatchChatTyping(obj.groupId, obj);
            }
          }
          break;
        case "documents":
          {
            if (obj.type.action == "UpdateFolder") {
              // This condition checks what kind of actions needs to be performed, update, edit or delete
              props.dispatchFolderUpdate(obj.update, () => { });
            }
            if (obj.type.action == "DeleteFolder") {
              // This condition checks what kind of actions needs to be performed, update, edit or delete
              props.dispatchFolderDelete(obj.update);
            }
            if (obj.type.action == "CreateFolder") {
              // This condition checks what kind of actions needs to be performed, update, edit or delete
              props.dispatchFolderNew(obj.update);
            }
            if (obj.type.action == "DeleteFile") {
              // This condition checks what kind of actions needs to be performed, update, edit or delete
              props.dispatchFileDelete(obj.update);
            }
            if (obj.type.action == "CreateFile") {
              // This condition checks what kind of actions needs to be performed, update, edit or delete
              props.dispatchFileNew(obj.update);
            }
          }
          break;
        default:
          break;
      }
    }
  };
  nTaskSignalConnectionOpen(props.profileState.data.userId);
}

export default realTimeUpdates;
