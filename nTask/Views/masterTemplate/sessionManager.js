import React, { Component } from "react";
import { connect } from "react-redux";
import IdleTimer from "react-idle-timer";
import helper from "../../helper/index";

class SessionManager extends Component {
  constructor(props) {
    super(props);
    this.idleTimer = null;
  }

  componentDidUpdate(prevProps, prevState) {
    const { isUserIdle, autoLogoutTime, profile } = this.props;
    if (
      profile.profile &&
      (isUserIdle !== prevProps.isUserIdle ||
        autoLogoutTime !== prevProps.autoLogoutTime ||
        profile.profile.autoLogoutEnabled !==
          prevProps.profile.profile.autoLogoutEnabled)
    ) {
      this.idleTimer.reset();
    }
  }

  render() {
    return (
      <IdleTimer
        ref={(ref) => {
          this.idleTimer = ref;
        }}
        element={document.body}
        onActive={this.onActive}
        onIdle={this.onIdle}
        onAction={this.onAction}
        debounce={250}
        timeout={1000 * 60 * this.props.autoLogoutTime}
      />
    );
  }

  onAction = (e) => {};

  onActive = (e) => {};

  onIdle = (e) => {
    const { profile } = this.props;
    if (helper.getToken() && profile.profile.autoLogoutEnabled)
      this.props.handleUserIdleState(
        true,
        Date.now() - this.idleTimer.getLastActiveTime()
      );
  };
}

const mapStateToProps = (state) => ({
  autoLogoutTime: state.profile.data.profile
    ? state.profile.data.profile.autoLogoutTime
    : 45,
  profile: state.profile.data,
});

export default connect(mapStateToProps)(SessionManager);
