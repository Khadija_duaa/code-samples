import React, { Component, Suspense } from "react";
import isEmpty from "lodash/isEmpty";
import { withSnackbar } from "notistack";
import { connect } from "react-redux";
import { store } from "../../index";
import helper from "../../helper/index";

import SidebarNew from "../../Views/Sidebar/Sidebar.view";
import SidebarOld from "../../components/Sidebar/Sidebar";
import appStyles from "../../appStyles";
import { withRouter, Route, Redirect, Switch } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import { GetFeatureAccessPermissions } from "../../redux/actions/team"
import {
  FetchUserInfo,
  isTocAccepted,
  SwitchTeamWorkspace,
  LinkSlackInfo,
  LinkZoomInfo,
  UpdateMember,
  LinkMicrosoftTeamsInfo,
} from "../../redux/actions/profile";
import {
  FetchWorkspaceInfo,
  AddNewTeam,
  UpdateTeam,
  RemoveTeamFromList,
  UpdateUserIsOnline,
  PopulateItemOrder,
  WorkspaceExists,
  PopulatePermissions,
} from "../../redux/actions/workspace";
import { GetUserPreferences } from "../../redux/actions/profileSettings";
import moment from "moment";
import { compose } from "redux";
import { sessionLogout } from "../../redux/actions/logout";
import { FormattedMessage } from "react-intl";

import {
  dispatchTask,
  dispatchTaskDelete,
  dispatchNewTask,
  dispatchArchiveTask,
  dispatchCopyTask,
  dispatchStopTimer,
  updateTask,
  UpdateTaskCustomFieldData,
  getTasks,
  BulkAddTask,
} from "../../redux/actions/tasks";
import { addGridColumns } from "../../redux/actions/columns";
import { PopulateNotificationsInfo, AddNotificationsInfo } from "../../redux/actions/notifications";
import { nTaskSignalConnectionOpen } from "../../utils/ntaskSignalR";
import { AddNewTaskCommentData, UpdateTaskNotifications } from "../../redux/actions/taskComments";
import {
  AddNewIssueCommentData,
  UpdateIssueNotifications,
} from "../../redux/actions/issueComments";
import { AddNewRiskCommentData, UpdateRiskNotifications } from "../../redux/actions/riskComments";
import CustomAvatar from "../../components/Avatar/Avatar";

import {
  dispatchIssue,
  dispatchIssueDelete,
  dispatchNewIssue,
  dispatchArchiveIssue,
  UpdateIssueCustomFieldData,
} from "../../redux/actions/issues";
import {
  singalRBoards,
  singalRKanban,
  singalRKanbanTask,
  updateTaskCommentCountKanban,
  UpdateKanbanLogsInstore,
  updateBoardBackgroundSettings,
} from "../../redux/actions/boards";
import {
  dispatchRisk,
  dispatchNewRisk,
  dispatchRiskDelete,
  dispatchRiskArchive,
  UpdateRiskCustomFieldData,
} from "../../redux/actions/risks";
import {
  dispatchMeeting,
  dispatchNewMeeting,
  dispatchMeetingDelete,
} from "../../redux/actions/meetings";
import {
  dispatchProject,
  dispatchNewProject,
  dispatchProjectDelete,
  dispatchProjectCopy,
  dispatchRenameProject,
  dispatchArchiveProject,
  dispactchProjectPartially,
  UpdateProjectCustomFieldData,
} from "../../redux/actions/projects";
import {
  dispatchGanttUpdate,
  dispatchNewGanttTask,
  dispatchTaskComplete,
  dispatchTasksLink,
  dispatchDeleteTasksLink,
} from "../../redux/actions/gantt";
import {
  dispatchChatUpdate,
  dispatchChatDelete,
  dispatchChatNew,
  dispatchChatTyping,
} from "../../redux/actions/chat";
import ListLoader from "../../components/ContentLoader/List";
import realTimeUpdates from "./realTimeUpdates";
import Forms from "../Forms/index";
import CreateTeamIntroDialog from "../../components/Dialog/IntorducingTeamDialog/CreateTeamIntroDialog";
import instance from "../../redux/instance";
import RestrictedAccessWorkspaceDialogue from "../../components/Dialog/restrictedAccessWorkspaceDialog";
import ConstantsPlan from "../../components/constants/planConstant";
import ActionConfirmation from "../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import { showUpgradePlan, UpdatePromoCodeValue } from "../../redux/actions/userBillPlan";
import AllDialogs from "../../components/Dialog/AllDialogs";
import CollaborationView from "../Collaboration/CollaborationView";
import { PopulateWhiteLabelInfo, DefaultWhiteLabelInfo } from "../../redux/actions/teamMembers";
import loadable from "@loadable/component";
import { LicenseManager } from "@ag-grid-enterprise/core";
LicenseManager.setLicenseKey("CompanyName=nTask Inc,LicensedApplication=app.ntaskmanager.com,LicenseType=SingleApplication,LicensedConcurrentDeveloperCount=1,LicensedProductionInstancesCount=1,AssetReference=AG-021514,ExpiryDate=25_October_2022_[v2]_MTY2NjY1MjQwMDAwMA==8e5e9a6e6e11d3f9556b8d4bb68539e6");
const ProjectDashboard = loadable(() => import("../Project/Dashboard"));
const TaskDashboard = loadable(() => import("../Task/Dashboard"));

const TrialExpire = loadable(() => import("../billing/TrialExpire/TrialExpire"));
const PaymentOverdue = loadable(() => import("../billing/PaymentOverdue/PaymentOverdue"));
const MappingStatusModal = loadable(() => import("../billing/MappingCustomStatus"));
const PromoPlan = loadable(() => import("../billing/PromoPlan/PromoPlan"));
const PaymentPlan = loadable(() => import("../billing/PromoPlan/PaymentPlan"));

const Header = loadable(() => import("../../components/Header/Header"));
const MeetingDashboard = loadable(() => import("../Meeting/Dashboard"));
const RiskDashboard = loadable(() => import("../Risk/Dashboard"));
const IssueDashboard = loadable(() => import("../Issue/Dashboard"));
// const ReportsDashboard = loadable(() => import(/* webpackChunkName: "reportsdashboard" */ "../Reports/Dashboard"));
const TeamTimeSheetDashboard = loadable(() => import("../TeamTimesheet/Dashboard"));
const TimeSheetDashboard = loadable(() => import("../Timesheet/Dashboard"));
const Boards = loadable(() => import("../Boards"));
const NotificationDashboard = loadable(() => import("../Notification/Dashboard"));
const ActivityDashboard = loadable(() => import("../ActivityLog/Dashboard"));
const WorkspaceSetting = loadable(() => import("../WorkspaceSetting/WorkspaceSetting"));
const TeamSetting = loadable(() => import("../TeamSetting/TeamSetting"));
const IssueOverview = loadable(() => import("../IssueOverview/Overviews.view"));
const Analytics = loadable(() => import("../Analytics/analytics.view"));
const AnalyticsTwo = loadable(() => import("../Analytics2/AnalyticsTwo.view"));
const ResourcesDashboard = loadable(() => import("../ResourcesDashboard/resources.view"));
const MembersOverview = loadable(() => import("../MembersOverview/MembersOverview.view"));
const BoldibiDashboard = loadable(() => import("../IwmsTimeSheet/TimeSheetTabs"));
const StatusReports = loadable(() => import("../TeamSetting/TeamSetting"));
const Overviews = loadable(() => import("../Overviews/Overviews.view"));
const SessionDialog = loadable(() => import("../../components/Dialog/SessionDialog"));
const SessionManager = loadable(() => import("./sessionManager"));
const TeamDashboard = loadable(() => import("../Teams/Dashboard"));
const Documents = loadable(() => import("../Documents"));
import Resources from "../Resources";
// import Documents from "../Documents/Documnets.view";
import {
  authenticateUser,
  getUserState,
  getRandomQuotes,
} from "../../redux/actions/authentication";

import { LinkGoogleCalendarInfo } from "../../redux/actions/googleCalendar";
import { dispatchNotification } from "../../redux/actions/teamInvitations";
import { dispatchTeamMember } from "../../redux/actions/team";
import { PopulateTempData } from "../../redux/actions/tempdata";
import { getGlobalTaskTime } from "../../redux/actions/globalTimerTask";
import { UpdateBulkStoreRisk } from "../../redux/actions/risks";
import { UpdateBulkStoreIssue } from "../../redux/actions/issues";
import {
  UpdateBulkStoreTasks,
  dispatchStartTimer,
  UpdateTasksInstore,
  DeleteBulkStoreTask,
} from "../../redux/actions/tasks";
import { UpdateBulkStoreProjects, toggleProjectInStore } from "../../redux/actions/projects";
import { UpdateBulkStoreMeeting } from "../../redux/actions/meetings";
import MasterLoader from "../../components/ContentLoader/Master";
import queryString from "query-string";
import { StartFreePlanBilling } from "../../redux/actions/userBillPlan";
import {
  acceptTeamInvitation,
  rejectTeamInvitation,
  removeTeamInvitation,
  updateTeamCount,
} from "../../redux/actions/teamInvitations";
import { dispatchAddTodoItems } from "../../redux/actions/todoItems";
import axios from "axios";
import constants from "../../redux/constants/types";
import { injectIntl } from "react-intl";

import { dispatchTemplatesActions } from "../../redux/actions/workspaceTemplates";
import { getCustomField, UpdateCustomFieldsSignalR } from "../../redux/actions/customFields";

import clsx from "clsx";
const ReportingMasterView = loadable(() => import("../Reporting/ReportingMaster.view"));
import ActionLoader from "../../components/ActionLoader/ActionLoader";
import { registerLocale, setDefaultLocale } from "react-datepicker";
// [@Kazim-@TODO-@PENDING] : need to change moment and date fns with naxxa date lib. (Date : 2 Feb 2023)
import enUS from 'date-fns/locale/en-US';

// Views
class MasterTemplate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: this.props.sidebarPannel.open,
      isUserIdle: false,
      sessionDialog: false,
      loadingState: true,
      accessFlag: false,
      timerTask: null,
      introTeamDialog: false,
      isTrialPlanExpired: false,
      isPaymentPlanExpired: false,
      restrictedAccessDialogue: false,
      removedWorkspace: null,
      isPromoPlan: false,
      isPaymentPlan: false,
      quote: "",
      planDialog3: false,
      showTeamInvite: true,
      loading: false,
      isMappingStatusRequired: false,
    };
    this.handleSidebarToggle = this.handleSidebarToggle.bind(this);
  }

  handleSessionDialogOpen = () => {
    this.setState({ sessionDialog: true });
  };

  handleSessionDialogClose = () => {
    this.setState({ isUserIdle: false, sessionDialog: false });
  };

  showLoadingState = () => {
    this.setState({ loadingState: true });
  };

  hideLoadingState = () => {
    this.setState({ loadingState: false });
  };

  showUpgradeDialog = () => {
    const data = {
      show: true,
      mode: ConstantsPlan.UPGRADEPLANMODE,
    };
    this.props.showUpgradePlan(data);
  };

  getWorkspaceData = () => {
    this.getQuotes();
    this.interval = setInterval(this.getQuotes, 7200000);
    // Init freshchat

    this.props.authenticateUser(true);
    this.setState({
      accessFlag: true,
    });
    localStorage.removeItem("CompanyInfo");
    localStorage.removeItem("CreateProfile");
    localStorage.removeItem("CreateWorkspace");
    localStorage.removeItem("InviteMembers");
    localStorage.removeItem("Onboarding");
    localStorage.removeItem("teamId");
    const searchQuery = this.props.history.location.search;
    if (searchQuery) {
      const objectType = this.props.history.location.pathname.split("/")[1]; // Parsing the url and extracting the object type using query string
      let objectId = "";
      if (objectType == "projects") {
        objectId = queryString.parseUrl(searchQuery).query.projectId; // Parsing the url and extracting the project id using query string
      } else if (objectType == "tasks") {
        objectId = queryString.parseUrl(searchQuery).query.taskId; // Parsing the url and extracting the task id using query string
      } else if (objectType == "boards") {
        objectId = queryString.parseUrl(searchQuery).query.taskId; // Parsing the url and extracting the task id using query string

      }
      else if (objectType == "meetings") {
        objectId = queryString.parseUrl(searchQuery).query.meetingId; // Parsing the url and extracting the meeting id using query string
      } else if (objectType == "issues") {
        objectId = queryString.parseUrl(searchQuery).query.issueId; // Parsing the url and extracting the issue id using query string
      } else if (objectType == "risks") {
        objectId = queryString.parseUrl(searchQuery).query.riskId; // Parsing the url and extracting the risk id using query string
      }
      if (objectType && objectId) {
        this.props.SwitchTeamWorkspace(
          objectId,
          objectType,
          (success) => {
            this.loadFetchUserInfo();
          },
          (fail) => {
            this.loadFetchUserInfo();
          }
        );
      } else {
        this.loadFetchUserInfo();
      }
    } else {
      this.loadFetchUserInfo();
    }
  };

  loadFetchUserInfo = () => {
    const { location } = this.props;
    const match = this.props.match.params.view;
    const parsedUrl = queryString.parse(location.search);
    // call api here access feature

    // this.props.GetFeatureAccessPermissions();
    this.props.FetchUserInfo((data) => {
      if (data.data.activeTeam == "4c027962d01e4bf1bcdd5e0486556edc") {
        document.querySelector("body").classList.add("hideChatBox");
      }
      let activeTeam = null;
      if (data.data.teams.length > 0) {
        activeTeam = data.data.teams.find((t) => {
          return t.companyId == data.data.activeTeam;
        });
      }
      const oldSidebarSeen = localStorage.getItem("oldSidebarSeen") == "true" ? true : false;
      const userCreatedDate = moment(data?.data?.createdDate);
      const isOldUser = userCreatedDate.isBefore("2022-03-03T11:26:29.219Z");
      localStorage.setItem("oldSidebarView", false);
      // if (!isOldUser) {
      //   localStorage.setItem("oldSidebarSeen", false);
      //   localStorage.setItem("oldSidebarView", false);
      // } else if (!oldSidebarSeen) {
      //   localStorage.setItem("oldSidebarSeen", true);
      //   localStorage.setItem("oldSidebarView", true);
      // }
      if (activeTeam && activeTeam.isEnableWhiteLabeling) {
        // Code will execute in case of white labeling enabled
        this.getCompanyData(data.data.activeTeam);
      } else {
        store.dispatch(DefaultWhiteLabelInfo());
      }

      if (data.data.emailVerficationLock) {
        // When user is locked out due to not verifying his email
        this.props.history.push("/verify-email");
        return;
      }

      if (parsedUrl.promoCode) {
        this.props.UpdatePromoCodeValue(parsedUrl.promoCode, () => { });
      }

      if (parsedUrl.upgradeplan) {
        const activeTeam = data.data.teams.find((t) => {
          return t.companyId == data.data.activeTeam;
        });
        this.showUpgradeDialog();
        this.props.history.push(`/teams/${activeTeam.companyUrl}`);
      }

      if (data.data.teams.length) {
        if (ENV == "production" && profitwell) {
          profitwell("start", { user_email: data.data.email });
        }
      }

      /** fetching all user created custom fields */
      this.props.getCustomField(
        (succ) => { },
        (fail) => { }
      );
      this.props.getTasks(
        null,
        null,
        () => { },
        () => { }
      );
      this.props.GetUserPreferences(
        () => {
          // if (!localStorage.getItem("businessPlanNotif3")) {
          //   this.setState({ planDialog3: true });
          //   document.querySelector("#root").style.filter = "blur(8px)";
          //   localStorage.setItem("businessPlanNotif3", true);
          // }
          let dayOfWeek = data.data && data.data.profile ? data.data.profile.startDayOfWeek : 0;

          moment.updateLocale("en", {
            week: {
              dow: dayOfWeek == 7 ? 0 : data.data.profile.startDayOfWeek,
            },
          });

          registerLocale("en-US", {
            ...enUS, options: {
              ...enUS.options,
              weekStartsOn: dayOfWeek == 7 ? 0 : data.data.profile.startDayOfWeek
            }
          });
          setDefaultLocale("en-US");
          // Code block for the raygun connection for error reporting details of user
          // rg4js("setUser", {
          //   identifier: data.data.userId,
          //   isAnonymous: false,
          //   email: data.data.email,
          //   firstName: data.data.fullName || "",
          //   fullName: data.data.fullName,
          // });
          if (ENV == "production" && window.userpilot) {
            window.userpilot.identify(
              data.data.userId, // Used to identify users
              {
                name: data.data.fullName, // Full name
                email: data.data.email, // Email address
                created_at: moment(data.data.createdDate, "YYYY-MM-DDThh:mm:ss").format("x"),
                plan: data.data.userPlan,
                // Additional user properties
                // plan: "free",
                // trialEnds: '2019-10-31T09:29:33.401Z'"
              }
            );
          }
          if (!data.data.teams.length) {
            this.props.history.push("/no-team-found");
            return;
          }
          if (ENV == "production" && activeTeam && window.userpilot) {
            window.userpilot.identify(
              data.data.userId, // Used to identify users
              {
                name: data.data.fullName, // Full name
                email: data.data.email, // Email address
                created_at: moment(data.data.createdDate, "YYYY-MM-DDThh:mm:ss").format("x"),
                whiteLabel: activeTeam.isEnableWhiteLabeling,
                // Additional user properties
                // plan: "free",
                // trialEnds: '2019-10-31T09:29:33.401Z'"
              }
            );
          }

          //Checking if team exists against user and if exist fetch - Code needs to be removed
          if (data.data.runMigration) {
            this.props.history.push("/no-team-found");
            return;
          }
          const { isOwner } = this.props;
          const { paymentPlanTitle, currentPeriondEndDate } = data.data.teams.find(
            (item) => item.companyId == data.data.activeTeam
          ).subscriptionDetails;
          const activedTeam =
            data.data.teams.find((item) => item.companyId == data.data.activeTeam) || null;
          // Comment below to disable promo popup
          // if (
          //   isOwner &&
          //   ConstantsPlan.isPlanNonPaid(paymentPlanTitle) &&
          //   !localStorage.getItem("promoPlanChina")
          // ) {
          //   this.setState({ isPromoPlan: true });
          //   document.querySelector("#root").style.filter = "blur(8px)";
          //   localStorage.setItem("promoPlanChina", true);
          //   return;
          // }
          const { gracePeriod } = this.props;
          // const {paymentPlanTitle, currentPeriondEndDate} = data.data.teams.find(item => item.companyId == data.data.activeTeam).subscriptionDetails;
          if (!ConstantsPlan.isPlanFree(paymentPlanTitle)) {
            if (
              ConstantsPlan.isPlanTrial(paymentPlanTitle) &&
              ConstantsPlan.isPlanExpire(currentPeriondEndDate)
            ) {
              this.setState({ isTrialPlanExpired: true });
              return;
            }
            if (
              ConstantsPlan.isPlanPaid(paymentPlanTitle) &&
              ConstantsPlan.isPlanExpire(currentPeriondEndDate, gracePeriod)
            ) {
              this.setState({ isPaymentPlanExpired: true });
              return;
            }
          }
          const newSidebarView = localStorage.getItem("oldSidebarView") == "true" ? false : true;
          if (
            (data.data.activeTeam &&
              data.data.loggedInTeam &&
              data.data.teams.filter((t) => {
                return t.companyId !== "000000";
              }).length &&
              location.pathname.indexOf("/teams/") !== 0) ||
            (newSidebarView && data.data.activeTeam)
          ) {
            // Incase there is no team and there are workspaces available
            this.fetchWorkspace();
          } else {
            const activeTeam = data.data.teams.find((t) => {
              return t.companyId == data.data.activeTeam;
            });
            // checks if there is no active workspace and redirects to active team url
            this.props.history.push(`/teams/${activeTeam.companyUrl}`);
            {
              activeTeam.companyUrl
                ? this.props.history.push(`/teams/${activeTeam.companyUrl}`)
                : this.props.history.push(`/teams/`);
            }
            this.hideLoadingState();
          }
          if (
            activedTeam &&
            activedTeam.isStatusMappingRequired &&
            ConstantsPlan.DisplayPlanName(activedTeam.subscriptionDetails.paymentPlanTitle) !==
            "Business"
          ) {
            // if (activedTeam && activedTeam.isStatusMappingRequired) {
            this.setState({ isMappingStatusRequired: true });
          }
          // Function that triggers signalR websocket connection
          realTimeUpdates(
            this.props,
            this.switchToWorkspace,
            this.fetchWorkspace,
            this.props.FetchWorkspaceInfo,
            this.removedWorkspace
          );
        },
        (err) => { }
      );
    });
  };
  getCompanyData = (teamId) => {
    this.setState({ loading: true });
    axios
      .get(`${constants.WHITELABELURL}?teamId=${teamId}`)
      .then((response) => {
        if (response.data.success) {
          localStorage.setItem("companyName", response.data.data.companyName);
          store.dispatch(PopulateWhiteLabelInfo(response.data.data));
        } else {
          store.dispatch(PopulateWhiteLabelInfo(null));
          localStorage.removeItem("companyName");
        }
        var favicon = document.querySelector('link[rel="shortcut icon"]');
        if (favicon) favicon.remove();
        favicon = document.createElement("link");
        favicon.setAttribute("rel", "shortcut icon");
        var head = document.querySelector("head");
        head.appendChild(favicon);
        favicon.setAttribute(
          "href",
          `${response.data.data.faviconImageUrl}?v=${new Date().getTime()}`
        );
        this.setState({ loading: false });
      })
      .catch((response) => {
        store.dispatch(PopulateWhiteLabelInfo(null));
        localStorage.removeItem("companyName");
        this.setState({ loading: false });
      });
  };
  removedWorkspace = (remove, removedWorkspaceId) => {
    const { profileState } = this.props;
    const deletedWorkspace = profileState.data.workspace.find((x) => {
      /** fetching deleted workspace object from workspaces list */
      return x.teamId == removedWorkspaceId;
    });
    this.setState({
      restrictedAccessDialogue: remove,
      removedWorkspace: deletedWorkspace,
    }); /** Setting deleted object into state and passing further into restrict access workspace dialogue  */
  };

  closeModal = () => {
    /** function for clossing the restrict access workspace dialogue */
    this.setState({ restrictedAccessDialogue: false });
  };

  workspaceRemoveSuccess = () => {
    /** function call on succes button click of restricted access modal of disble invited workspace */
    const { removedWorkspace } = this.state;
    const profileState = this.props.profileState.data;

    this.props.RemoveTeamFromList(
      removedWorkspace.teamId
    ); /** Action call for removing deleted workspace from profile state workspaces */

    let activeTeam;
    if (
      profileState &&
      profileState.workspace &&
      profileState.workspace.length &&
      profileState.teams.length
    ) {
      activeTeam = profileState.teams.find((t) => {
        /** fetching active team from profile state team */
        return t.companyId == profileState.activeTeam;
      });
    }
    const state = store.getState();
    if (state.profile.data.loggedInTeam === removedWorkspace.teamId) {
      this.props.history.push(
        `/teams/${activeTeam.companyUrl}`
      ); /** redirecting to team dashboard */
    }
    this.setState({ restrictedAccessDialogue: false });
  };

  fetchWorkspace = () => {
    const { data } = this.props.profileState;
    const url = this.props.location.search;
    const { pathname } = this.props.location;
    const params = queryString.parse(url);
    if (pathname == "/zoom" && params.code) {
      this.props.LinkZoomInfo(params.code, () => {
        this.props.history.push("/tasks");
      }, err => {
        let errMessage = err.data?.message || "Oops! Zoom authorization failed."
        this.showSnackBar(errMessage, "error");
        this.props.history.push("/tasks");
      });
    } else if (pathname == "/microsoftteams" && params.code) {
      this.props.LinkMicrosoftTeamsInfo(params.code, () => {
        this.props.history.push("/tasks");
      }, err => {
        let errMessage = err.data?.message || "Oops! Microsoft Teams authorization failed."
        this.showSnackBar(errMessage, "error");
        this.props.history.push("/tasks");
      });
    } else if (pathname == "/googlecalendar" && params.code) {
      this.props.history.push("/tasks");
      this.props.LinkGoogleCalendarInfo(
        params.code,
        (succ) => { },
        (fail) => {
          this.showSnackBar("Oops! Google Calender authorization failed", "error");
        }
      );
    } else if (params.code) {
      this.props.LinkSlackInfo(params.code, () => {
        this.props.history.push("/tasks");
      }, err => {
        let errMessage = err.data?.message || "Oops! Slack authorization failed."
        this.showSnackBar(errMessage, "error");
        this.props.history.push("/tasks");
      });
    }
    this.props.FetchWorkspaceInfo(
      "",
      (wspace) => {
        if (wspace && wspace.status === 200) {
          const { data } = wspace;
          this.props.GetUserPreferences(
            () => {
              nTaskSignalConnectionOpen(this.props.profileState.data.userId);
              setTimeout(() => this.setState({ loadingState: false }), 1000);
              const { profileState } = this.props;

              // Only enable freshchat on beta
              if (ENV == "production" && window.fcWidget) {
                //  To set user properties for freshchat
                const currentUser = profileState.data.member.allMembers.find((member) => {
                  return member.userId == profileState.data.profile.userId;
                });
                // Code for freshchat widget
                window.fcWidget.user.setProperties({
                  externalId: currentUser.userId, // user’s id unique to your system
                  firstName: currentUser.fullName, // user’s first name
                  email: currentUser.email,
                });
              }
            },
            () => { }
          );
        } else if (wspace && wspace.status === 300 && data.workspace.length === 0) {
          // this.props.PopulateTempData("Enter-WorkSpace");
          const activeTeam = profileState.data.teams.find((t) => {
            return t.companyId == profileState.data.activeTeam;
          });
          // checks if there is no active workspace and redirects to active team url
          this.props.history.push(`/teams/${activeTeam.companyUrl}`);
        } else if (wspace && wspace.status === 403) {
          this.hideLoadingState();
          const activeTeam = profileState.data.teams.find((t) => {
            return t.companyId == profileState.data.activeTeam;
          });
          // checks if there is no active workspace and redirects to active team url
          this.props.history.push(`/teams/${activeTeam.companyUrl}`);
        }
        this.hideLoadingState();
        const newSidebarView = localStorage.getItem("oldSidebarView") == "true" ? false : true;
        if (newSidebarView) {
          this.handleActiveTeam();
        }
      },
      () => {
        const { profileState } = this.props;
        const activeTeam = profileState.data.teams.find((t) => {
          return t.companyId == profileState.data.activeTeam;
        });
        this.props.history.push(`/teams/${activeTeam.companyUrl}`);
        this.hideLoadingState();
      }
    );
  };

  handleActiveTeam = () => {
    const { profileState } = this.props;
    const { pathname } = this.props.location;
    const activeTeam = profileState.data.teams.find((t) => {
      return t.companyId == profileState.data.activeTeam;
    });
    if (pathname.includes("/teams/")) this.props.history.push(`/teams/${activeTeam.companyUrl}`);
    this.hideLoadingState();
  };

  introTeamDialogClose = () => {
    this.setState({ introTeamDialog: false });
  };

  switchToWorkspace = (teamId) => {
    this.showLoadingState();
    this.props.FetchWorkspaceInfo(teamId, () => {
      this.props.history.push("/tasks");
      this.props.FetchUserInfo(() => {
        this.hideLoadingState();
      });
    });
  };

  componentDidUpdate(prevProps) {
    if (
      JSON.stringify(this.props.globalTimerTaskState) !==
      JSON.stringify(prevProps.globalTimerTaskState)
    ) {
      this.setState({
        timerTask: isEmpty(this.props.globalTimerTaskState)
          ? null
          : this.props.globalTimerTaskState,
      });
    }
    if (JSON.stringify(this.props.sidebarPannel) !== JSON.stringify(prevProps.sidebarPannel)) {
      this.setState({ open: this.props.sidebarPannel.open });
    }
  }

  componentDidMount() {
    if (this.props.location.state && this.props.location.state.support) {
      instance()
        .get("api/ntask/GetSsoUrl")
        .then((response) => {
          window.location.href = response.data;
          this.props.location.state = "";
        });
    }

    this.getWorkspaceData();
    // The listener
    // this.props.history.listen((location, action) => {
    //   rg4js("trackEvent", { type: "pageView", path: location.pathname });
    // });
  }

  getQuotes = () => {
    const token = helper.getToken();
    if (token) {
      getRandomQuotes((res) => {
        this.setState({ quote: res.data });
      });
    }
  };

  handleSidebarToggle() {
    this.setState({
      open: !this.state.open,
    });
  }

  handleUserIdleState = (isIdle, sessionTime) => {
    this.props.sessionLogout(() => {
      this.setState({ isUserIdle: isIdle, sessionDialog: true });
    });
  };

  continueFreePlan = (data) => {
    this.props.StartFreePlanBilling(
      data,
      (success) => {
        this.setState({ isTrialPlanExpired: false, isPaymentPlanExpired: false }, () => {
          this.loadFetchUserInfo();
        });
      },
      (error) => {
        this.showSnackBar(error.data);
        this.setState({ btnPromoQuery: "" });
      }
    );
  };

  trialPaidDone = (data) => {
    this.setState({ isTrialPlanExpired: false }, () => {
      this.loadFetchUserInfo();
    });
  };

  overduePaidDone = (data) => {
    this.setState({ isPaymentPlanExpired: false }, () => {
      this.loadFetchUserInfo();
    });
  };

  // handlePlanDialogClose = () => {
  //   this.setState({ planDialog3: false });
  //   document.querySelector("#root").style.filter = "";
  // };
  componentWillUnmount() {
    clearInterval(this.interval);
  }

  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type || "info",
      }
    );
  };

  closeTrialExpirePopUp = () => {
    this.setState({ isTrialPlanExpired: false });
  };

  closePaymentOverdue = () => {
    this.setState({ isPaymentPlanExpired: false });
  };

  closeMappingStatus = () => {
    this.setState({ isMappingStatusRequired: false });
  };

  trialCreateTeamDone = () => {
    this.setState({ isTrialPlanExpired: false }, () => {
      this.hideLoadingState();
    });
  };

  overdueCreateTeamDone = () => {
    this.setState({ isPaymentPlanExpired: false, isMappingStatusRequired: false }, () => {
      this.hideLoadingState();
    });
  };

  planExpire = (mode) => {
    if (mode == "Trial") this.setState({ isTrialPlanExpired: true });
    else if (mode == "StatusMapping") {
      this.setState({ isMappingStatusRequired: true });
    } else {
      this.setState({ isPaymentPlanExpired: true });
    }
  };

  switchExpirePlan = (mode) => {
    this.setState({ isTrialPlanExpired: false, isPaymentPlanExpired: true });
  };

  closePromoPopUp = () => {
    this.setState({ isPromoPlan: false });
    document.querySelector("#root").style.filter = "";
    this.loadFetchUserInfo();
  };

  closePlanPopUp = () => {
    this.setState({ isPaymentPlan: false });
  };

  openPlanPopUp = () => {
    this.setState({ isPaymentPlan: true });
  };

  closeInviteConfirmation = () => {
    this.setState({ showTeamInvite: false });
  };

  // When user click on accept invite button
  handleAcceptInvite = (teamId) => {
    this.setState({ inviteBtnQuery: "progress" });
    this.props.acceptTeamInvitation(
      teamId,
      () => {
        this.props.removeTeamInvitation();
        this.setState({ inviteBtnQuery: "" });
      },
      () => { },
      false
    );
  };

  // When user click on reject invite button
  handleRejectInvite = (teamId) => {
    // this.setState({ inviteBtnQuery: "progress" });
    this.props.rejectTeamInvitation(
      teamId,
      () => {
        this.props.removeTeamInvitation();
        // this.setState({ inviteBtnQuery: "" });
      }, // Failure
      () => { },
      false
    );
  };

  // When user click on i will decide later button
  handleDecideLater = (teamId) => {
    this.props.updateTeamCount();
    this.props.removeTeamInvitation();
  };

  render() {
    const { classes, theme, promoBar, notificationBar, profileState, intl, location: { pathname } } = this.props;
    const { quote } = this.state;
    const {
      isUserIdle,
      inviteBtnQuery,
      sessionDialog,
      loadingState,
      accessFlag,
      timerTask,
      introTeamDialog,
      isTrialPlanExpired,
      isPaymentPlanExpired,
      planDialog3,
      restrictedAccessDialogue,
      removedWorkspace,
      planDialog2,
      isPromoPlan,
      isPaymentPlan,
      showTeamInvite,
      loading,
      isMappingStatusRequired,
    } = this.state;
    const workspaceSettingsPermissions = !isEmpty(
      this.props.workspacePermissionsState.data.workSpace
    )
      ? this.props.workspacePermissionsState.data.workSpace.workspaceSetting.cando
      : false;
    const pendingInvites =
      profileState.data &&
        profileState.data.pendingInvites &&
        profileState.data.pendingInvites.length
        ? profileState.data.pendingInvites
        : [];
    const userCreatedDate = moment(profileState?.data?.createdDate);
    const isOldUser = userCreatedDate.isBefore("2022-03-03T11:26:29.219Z");
    const newSidebarView = localStorage.getItem("oldSidebarView") == "false" ? true : false;
    return accessFlag ? (
      <>
        {pendingInvites.length ? (
          <ActionConfirmation
            open={showTeamInvite}
            disableBackdropClick
            disableEscapeKeyDown
            closeAction={this.closeInviteConfirmation}
            cancelBtnText={<FormattedMessage id="timesheet.reject.label" defaultMessage="Reject" />}
            successBtnText={<FormattedMessage id="common.accept.label" defaultMessage="Accept" />}
            alignment="center"
            headingText={
              <FormattedMessage
                id="team-invitation-dialog.label"
                defaultMessage="Team Invitation"
              />
            }
            btnProps={{
              deleteBtnText: (
                <u>
                  {" "}
                  <FormattedMessage
                    id="team-invitation-dialog.action"
                    defaultMessage="I'll decide later"
                  />
                </u>
              ),
              deleteBtnProps: {
                btnType: "plain",
                variant: "text",
              },
            }}
            customIcon={
              <CustomAvatar
                otherMember={{
                  imageUrl: pendingInvites[0].invitedBy.imageUrl,
                  fullName: pendingInvites[0].invitedBy.fullName,
                  lastName: "",
                  email: pendingInvites[0].invitedBy.fullName,
                }}
                styles={{ marginBottom: 16 }}
                size="large"
                disableCard
              />
            }
            msgText={
              <>
                <span style={{ fontSize: "16px" }}>{pendingInvites[0].invitedBy.fullName} </span>{" "}
                <FormattedMessage
                  id="team-invitation-dialog.message"
                  defaultMessage="invited you to join team"
                />{" "}
                <span style={{ color: theme.palette.text.azure }}>
                  {pendingInvites[0].team.teamName}.
                </span>
              </>
            }
            successAction={() => this.handleAcceptInvite(pendingInvites[0].team.teamId)}
            rejectInvitation={() => {
              this.handleRejectInvite(pendingInvites[0].team.teamId);
            }}
            teamInvitationDialog
            deleteAction={() => {
              this.handleDecideLater(pendingInvites[0].team.teamId);
            }}
            btnQuery={inviteBtnQuery}
          />
        ) : null}
        {isPromoPlan && (
          <Suspense fallback={<></>}><PromoPlan showSnackBar={this.showSnackBar} closePromoPopUp={this.closePromoPopUp} /></Suspense>
        )}
        {isPaymentPlan && (
          <Suspense fallback={<></>}><PaymentPlan showSnackBar={this.showSnackBar} closeTrialPopUp={this.closePlanPopUp} /></Suspense>
        )}
        {isTrialPlanExpired && (
          <Suspense fallback={<></>}> <TrialExpire
            showSnackBar={this.showSnackBar}
            closeTrialExpirePopUp={this.closeTrialExpirePopUp}
            continueFreePlan={this.continueFreePlan}
            paymentPaidDone={this.trialPaidDone}
            createTeam={this.trialCreateTeamDone}
            hideLoadingState={this.hideLoadingState}
            showLoadingState={this.showLoadingState}
            planExpire={this.planExpire}
            switchExpirePlan={this.switchExpirePlan}
          /></Suspense>
        )}
        {isPaymentPlanExpired && (
          <Suspense fallback={<></>}> <PaymentOverdue
            showSnackBar={this.showSnackBar}
            closePaymentOverdue={this.closePaymentOverdue}
            continueFreePlan={this.continueFreePlan}
            paymentPaidDone={this.overduePaidDone}
            createTeam={this.overdueCreateTeamDone}
            hideLoadingState={this.hideLoadingState}
            showLoadingState={this.showLoadingState}
            planExpire={this.planExpire}
            switchExpirePlan={this.switchExpirePlan}
          /></Suspense>
        )}
        {isMappingStatusRequired && (
          <Suspense fallback={<></>}><MappingStatusModal
            showSnackBar={this.showSnackBar}
            closeMappingStatus={this.closeMappingStatus}
            createTeam={this.overdueCreateTeamDone}
            hideLoadingState={this.hideLoadingState}
            showLoadingState={this.showLoadingState}
            planExpire={this.planExpire}
            switchExpirePlan={this.switchExpirePlan}
          /></Suspense>
        )}

        <RestrictedAccessWorkspaceDialogue
          closeAction={this.closeModal}
          onSuccess={this.workspaceRemoveSuccess}
          open={restrictedAccessDialogue}
          removedWorkspace={removedWorkspace}
        />

        <CreateTeamIntroDialog
          closeAction={this.introTeamDialogClose}
          open={introTeamDialog}
          fetchWorkspace={this.fetchWorkspace}
          showLoadingState={this.showLoadingState}
          hideLoadingState={this.hideLoadingState}
        />
        <div className={classes.root}>
          <Suspense fallback={<></>}>  <SessionManager handleUserIdleState={this.handleUserIdleState} isUserIdle={isUserIdle} /></Suspense>
          <Suspense fallback={<></>}><SessionDialog closeAction={this.handleSessionDialogClose} open={sessionDialog} /></Suspense>
          {loadingState ? (
            <>
              {newSidebarView && profileState.data.teams.length && profileState.data.activeTeam ? (
                <>
                  <SidebarNew
                    showLoadingState={this.showLoadingState}
                    hideLoadingState={this.hideLoadingState}
                    planExpire={this.planExpire}
                  />
                  <MasterLoader sidebar={false} />
                </>
              ) : (
                <MasterLoader />
              )}
            </>
          ) : (
            <>
              <Suspense fallback={<></>}>
                <Header
                  open={this.state.open}
                  showLoadingState={this.showLoadingState}
                  hideLoadingState={this.hideLoadingState}
                  timerTask={timerTask}
                  planExpire={this.planExpire}
                />
              </Suspense>
              {(newSidebarView || !isOldUser) && profileState.data.teams.length && profileState.data.activeTeam ? (
                <SidebarNew
                  showLoadingState={this.showLoadingState}
                  hideLoadingState={this.hideLoadingState}
                  planExpire={this.planExpire}
                />
              ) : (
                <SidebarOld
                  open={this.state.open}
                  toggleAction={this.handleSidebarToggle}
                  key={this.props.match.params.view}
                  loading={loading}
                />
              )}

              {/*<Footer quote={quote} open={this.state.open} />{" "}*/}
            </>
          )}
          <main
            id="mainContentCnt"
            style={{
              marginTop:
                newSidebarView && (promoBar || notificationBar) && pathname !== "/collaboration"
                  ? "100px"
                  : !newSidebarView && (promoBar || notificationBar)
                    ? "140px"
                    : !notificationBar && pathname === "/collaboration" ? "41px" : "",
            }}
            className={clsx({
              [classes.contentShift]: !this.state.open,
              [classes.content]: pathname !== "/collaboration",
              [classes.contentCollab]: pathname === "/collaboration",
              [classes.newPaddingAdjustment]: newSidebarView && pathname !== "/collaboration",
              [classes.sidebarClose]: !this.props.sidebarPannel.open,
              [classes.surveyPageAdjustments]: pathname == "/forms" && !(promoBar || notificationBar),
              [classes.surveyPageAdjustmentsWithHeight]: pathname == "/forms" && (promoBar || notificationBar),
            })}>
            {loadingState ? (
              <ListLoader />
            ) : (
              <>
                <Suspense fallback={<></>}>
                  <Route
                    path="/boards"
                    render={() => {
                      return <Boards open={this.state.open} quote={quote} />;
                    }}
                  />
                </Suspense>
                <Suspense fallback={<></>}>
                  <Route
                    path="/teams/:teamname"
                    render={() => {
                      return (
                        <TeamDashboard
                          showLoadingState={this.showLoadingState}
                          hideLoadingState={this.hideLoadingState}
                        />
                      );
                    }}
                  /></Suspense>
                <Route
                  path="/tasks"
                  exact
                  render={() => {
                    return (
                      <Suspense fallback={<></>}>
                        <TaskDashboard
                          hideLoadingState={this.hideLoadingState}
                          showLoadingState={this.showLoadingState}
                          showSnackBar={this.showSnackBar}
                          quote={quote}
                        />
                      </Suspense>
                    );
                  }}
                />
                {/* {teamCanView("projectAccess") && ( */}
                <Suspense fallback={<></>}>
                  <Route
                    path="/risks"
                    render={() => {
                      return (
                        <RiskDashboard
                          quote={quote}
                          showSnackBar={this.showSnackBar}
                          open={this.state.open}
                        />
                      );
                    }}
                  />
                </Suspense>
                {/* )} */}
                <Suspense fallback={<></>}>
                  <Route
                    path="/issues"
                    render={() => {
                      return (
                        <IssueDashboard
                          quote={quote}
                          showSnackBar={this.showSnackBar}
                          open={this.state.open}
                        />
                      );
                    }}
                  />
                </Suspense>
                <Suspense fallback={<></>}>
                  <Route
                    path="/meetings"
                    render={() => {
                      return (
                        <MeetingDashboard
                          quote={quote}
                          showSnackBar={this.showSnackBar}
                          open={this.state.open}
                        />
                      );
                    }}
                  />
                </Suspense>
                {/* {teamCanView("projectAccess") && ( */}

                <Route
                  path="/projects"
                  render={() => {
                    return (
                      <Suspense fallback={<></>}>
                        {" "}
                        <ProjectDashboard quote={quote} open={this.state.open} />
                      </Suspense>
                    );
                  }}
                />
                <Suspense fallback={<></>}>
                  <Route
                    path="/gantt"
                    render={() => {
                      return <ProjectDashboard quote={quote} />;
                    }}
                  />
                </Suspense>
                {/* )} */}

                {/* <Route
                  path="/reports"
                  render={() => {
                    return <ReportsDashboard />;
                  }}
                />

                <Route
                  path="/reports"
                  render={() => {
                    return <ReportsDashboard quote={quote} />;
                  }}
                /> */}
                <Suspense fallback={<></>}>
                  <Route
                    path="/timesheet"
                    render={() => {
                      return <TimeSheetDashboard showSnackBar={this.showSnackBar} />;
                    }}
                  />
                </Suspense>
                <Suspense fallback={<></>}>
                  <Route
                    path="/notifications"
                    render={() => {
                      return <NotificationDashboard />;
                    }}
                  />
                </Suspense>
                {ENV === "dev" &&
                  <Route
                    path="/documents"
                    render={() => {
                      return <Documents />;
                    }}
                  />
                }
                {ENV === "dev" &&
                  <Suspense fallback={<></>}>
                    <Route
                      path="/resources"
                      render={() => {
                        return (
                          <Resources
                            showSnackBar={this.showSnackBar}
                          />
                        );
                      }}
                    />

                  </Suspense>
                }
                <Suspense fallback={<></>}>
                  {[
                    "8c1d63c294c5496c910e4c2b405ab2a9",
                    "9aa989d4f15247c586de45c174d2094b",
                    "9892d3cf5f5e4a9786172841dc4646f3",
                    "cf8b974c5ae54c1eac6ab05af83cdc59",
                    "28662a47fc10441fbf07a6fefd0fe428",].includes(profileState.data.activeTeam) ?
                    <Route
                      path="/forms"
                      render={() => {
                        return <Forms />;
                      }}
                    /> : null}
                  <Route
                    path="/team-settings"
                    render={() => {
                      return (
                        <TeamSetting
                          showLoadingState={this.showLoadingState}
                          hideLoadingState={this.hideLoadingState}
                          planExpire={this.planExpire}
                        />
                      );
                    }}
                  />
                </Suspense>
                {/* <Suspense fallback={<></>}>
                  <Route
                    exact
                    path="/reports/timesheet"
                    render={() => {
                      return <TeamTimeSheetDashboard showSnackBar={this.showSnackBar} />;
                    }}
                  />
                </Suspense>
                <Suspense fallback={<></>}>
                  <Route
                    exact
                    path="/teams/IwmsAnalytics"
                    render={() => {
                      return <BoldibiDashboard />;
                    }}
                  />
                </Suspense>
                <Suspense fallback={<></>}>
                  <Route
                    exact
                    path="/teams/StatusReports"
                    render={() => {
                      return <StatusReports />;
                    }}
                  />
                </Suspense>
                <Suspense fallback={<></>}>
                  <Route
                    path="/reports/members-overview"
                    exact
                    render={() => {
                      return <MembersOverview />;
                    }}
                  /></Suspense>
                <Suspense fallback={<></>}>
                  <Route
                    path="/reports/overview"
                    exact
                    render={() => {
                      return <Overviews />;
                    }}
                  /></Suspense>
                <Suspense fallback={<></>}>
                  <Route
                    path="/reports/IssueOverview"
                    exact
                    render={() => {
                      return <IssueOverview />;
                    }}
                  /></Suspense>
                <Suspense fallback={<></>}>
                  <Route
                    path="/reports/analytics"
                    exact
                    render={() => {
                      return <Analytics />;
                    }}
                  /></Suspense> */}

                <Suspense fallback={<></>}>
                  <Route
                    path="/reports/:id"
                    exact
                    render={() => {
                      return <ReportingMasterView />;
                    }}
                  /></Suspense>

                {/* <Suspense fallback={<></>}>
                  <Route
                    path="/teams/resources"
                    exact
                    render={() => {
                      return <ResourcesDashboard />;
                    }}
                  /></Suspense> */}


                <Suspense fallback={<></>}>
                  <Route
                    path="/activity"
                    render={() => {
                      return <ActivityDashboard showSnackBar={this.showSnackBar} />;
                    }}
                  />
                </Suspense>
                {ENV !== "production" && (
                  <Route
                    path="/collaboration"
                    render={() => {
                      return <CollaborationView />;
                    }}
                  />
                )}
                {/* {ENV !== "production" && ( */}
                {/* <Route
                    path="/overview"
                    render={() => {
                      return <Overviews />;
                    }}
                  /> */}
                {/* {this.isOverViewEnabled()  &&    ( */}

                {/* )}     */}
                {/* )} */}
                {/* <Route
                  path="/notifications"
                  render={() => {
                    return <NotificationDashboard />;
                  }}
                /> */}

                {workspaceSettingsPermissions ? (
                  <Suspense fallback={<></>}>
                    <Route
                      path="/workspace-settings"
                      render={() => {
                        return <WorkspaceSetting />;
                      }}
                    />
                  </Suspense>
                ) : (
                  <>
                    {this.props.location.pathname === "/workspace-settings" ? (
                      <Redirect from="/workspace-settings" to="/tasks" />
                    ) : null}
                  </>
                )}
              </>
            )}
            <AllDialogs intl={intl} showSnackBar={this.showSnackBar} />
          </main>
          {/* general loading components for all imports */}
          <ActionLoader />
        </div>
      </>
    ) : null;
  }
}

const mapStateToProps = (state) => {
  return {
    sidebarPannel: state.sidebarPannel,
    profileState: state.profile,
    workspacePermissionsState: state.workspacePermissions,
    promoBar: state.promoBar.promoBar,
    notificationBar: state.notificationBar.notificationBar,
    globalTimerTaskState: state.globalTimerTask,
    gracePeriod: state.profile.data.gracePeriod,
    isOwner: state.profile.data.teamMember.length
      ? state.profile.data.teamMember.find((item) => item.userId == state.profile.data.userId)
        .isOwner
      : false,
  };
};

export default compose(
  withStyles(appStyles, { withTheme: true }),
  withRouter,
  injectIntl,
  withSnackbar,
  connect(mapStateToProps, {
    acceptTeamInvitation,
    rejectTeamInvitation,
    removeTeamInvitation,
    FetchWorkspaceInfo,
    AddNewTeam,
    UpdateTeam,
    RemoveTeamFromList,
    FetchUserInfo,
    SwitchTeamWorkspace,
    GetUserPreferences,
    dispatchNotification,
    isTocAccepted,
    sessionLogout,
    getUserState,
    updateTeamCount,
    showUpgradePlan,
    // getOnBoardingData,
    authenticateUser,
    WorkspaceExists,
    PopulatePermissions,
    PopulateTempData,
    getGlobalTaskTime,
    UpdateBulkStoreRisk,
    UpdateBulkStoreIssue,
    UpdateBulkStoreTasks,
    dispatchStartTimer,
    UpdateBulkStoreProjects,
    toggleProjectInStore,
    UpdateBulkStoreMeeting,
    LinkSlackInfo,
    LinkZoomInfo,
    LinkGoogleCalendarInfo,
    UpdateMember,
    dispatchTask,
    dispatchIssue,
    dispatchRisk,
    dispatchMeeting,
    dispatchProject,
    dispatchTaskDelete,
    dispatchNewTask,
    dispatchIssueDelete,
    dispatchNewIssue,
    dispatchArchiveIssue,
    dispatchRiskDelete,
    dispatchNewRisk,
    AddNewTaskCommentData,
    dispatchNewMeeting,
    dispatchTeamMember,
    dispatchMeetingDelete,
    dispatchRenameProject,
    dispatchProjectDelete,
    dispatchProjectCopy,
    dispatchArchiveProject,
    dispactchProjectPartially,
    dispatchNewProject,
    dispatchGanttUpdate,
    dispatchNewGanttTask,
    dispatchTaskComplete,
    AddNewRiskCommentData,
    AddNewIssueCommentData,
    dispatchTasksLink,
    dispatchDeleteTasksLink,
    UpdateUserIsOnline,
    PopulateItemOrder,
    dispatchArchiveTask,
    dispatchCopyTask,
    dispatchStopTimer,
    dispatchRiskArchive,
    StartFreePlanBilling,
    UpdatePromoCodeValue,
    PopulateNotificationsInfo,
    AddNotificationsInfo,
    dispatchAddTodoItems,
    UpdateTaskNotifications,
    UpdateIssueNotifications,
    UpdateRiskNotifications,
    PopulateWhiteLabelInfo,
    DefaultWhiteLabelInfo,
    dispatchChatDelete,
    dispatchChatNew,
    dispatchChatTyping,
    dispatchTemplatesActions,
    singalRBoards,
    singalRKanban,
    singalRKanbanTask,
    updateTaskCommentCountKanban,
    UpdateTasksInstore,
    DeleteBulkStoreTask,
    UpdateKanbanLogsInstore,
    updateBoardBackgroundSettings,
    dispatchChatUpdate,
    updateTask,
    getCustomField,
    UpdateCustomFieldsSignalR,
    UpdateRiskCustomFieldData,
    UpdateIssueCustomFieldData,
    UpdateTaskCustomFieldData,
    UpdateProjectCustomFieldData,
    getTasks,
    addGridColumns,
    BulkAddTask,
    GetFeatureAccessPermissions,
    LinkMicrosoftTeamsInfo,
  })
)(MasterTemplate);
