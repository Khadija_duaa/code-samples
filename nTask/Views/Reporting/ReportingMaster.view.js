import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import loadable from "@loadable/component";
const IssueOverview = loadable(() => import("../IssueOverview/Overviews.view"));
const Overviews = loadable(() => import("../Overviews/Overviews.view"));
const TeamTimeSheetDashboard = loadable(() => import("../TeamTimesheet/Dashboard"));
const Analytics = loadable(() => import("../Analytics/analytics.view"));
const MembersOverview = loadable(() => import("../MembersOverview/MembersOverview.view"));
const AnalyticsTwo = loadable(() => import("../Analytics2/AnalyticsTwo.view"));

const ReportingMasterView = ({ match }) => {
  const { analyticsMenu   } = useSelector(state => {
    return {
      analyticsMenu: state.sidebarPannel.data.analyticsMenu,
    };
  });

  const [menu ,setMenu]= useState({});
  const [flag ,setFlag]= useState(false);

  useEffect(() => {
    setFlag(false)
    const sidePanelMenu = analyticsMenu.filter((ele)=>{
      if(ele.key === match.params.id) {
        return ele
      } 
    })
    setMenu(sidePanelMenu[0]);
    
    setTimeout(() => {
      setFlag(true)
    }, 100);

  }, [match.params.id])

  
  return (
    <>
    {flag ? 
    <>
    {   menu &&  menu.key == 'taskOverview' ? <Overviews/> 
      : menu && menu.key == 'issueOverview'? <IssueOverview/>
      : menu && menu.key == 'teamtimesheet'? <TeamTimeSheetDashboard/>
      : menu && menu.key == 'analytics'? <Analytics/>
      : menu && menu.key == 'membersOverview'? <MembersOverview/>
      : menu ? <AnalyticsTwo dashboardId={menu.url}/> : null 
    }
    </>
    : ''}
    </>
  )
}

export default withRouter(ReportingMasterView)