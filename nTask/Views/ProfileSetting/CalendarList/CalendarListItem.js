import React, { useState } from "react";
import Typography from "@material-ui/core/Typography";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import CalendarListStyles from "./styles";
import CustomButton from "../../../components/Buttons/CustomButton";
import SvgIcon from "@material-ui/core/SvgIcon";
import DefaultSwitch from "../../../components/Form/Switch";
import DeleteListIcon from "../../../components/Icons/DeleteListIcon";
import SettingListIcon from "../../../components/Icons/SettingListIcon";
import Truncate from "react-truncate";
import { FormattedMessage } from "react-intl";

function CalendarListItem(props) {
  const {
    classes,
    updateSync,
    urlText,
    item,
    stopSync,
    deleteCalendar,
    deleteCalendarWithData
  } = props;

  const [isListView, setListView] = useState(true);
  const [isStopSync, setStopSync] = useState(false);

  const [isDeleteSync, setDeleteSync] = useState(false);
  const [disableSyncBtnQuery, setDisableSyncBtnQuery] = useState("");
  const [deleteBtnQuery, setDeleteBtnQuery] = useState("");
  const [deleteBtnDataQuery, setDeleteBtnDataQuery] = useState("");
  const [enableSyncBtnQuery, setEnableSyncBtnQuery] = useState("");
  const [editedItem, setEditedItem] = useState({});

  const toggleSyncHandler = (i) => {
    setEditedItem(i);
    setStopSync((prevState) => !prevState);
    setListView((prevState) => !prevState);
  };

  const deleteSyncHandler = (i) => {
    setEditedItem(i);
    setStopSync(false);
    setListView(false);
    setDeleteSync((prevState) => !prevState);
  };

  const deleteCal = (item) => {
    setDeleteBtnQuery("progress");
    deleteCalendar(
      item,
      (succ) => {
        setListView(true);
        setStopSync(false);
        setDeleteSync(false);
        setDeleteBtnQuery("");
      },
      (fail) => {
        setDeleteBtnQuery("");
      }
    );
  };
  const deleteCalWithData = (item) => {
    setDeleteBtnDataQuery("progress");
    deleteCalendarWithData(
      item,
      (succ) => {
        setListView(true);
        setStopSync(false);
        setDeleteSync(false);
        setDeleteBtnQuery("");
      },
      (fail) => {
        setDeleteBtnQuery("");
      }
    );
  };

  const cancelSyncHandler = () => {
    setListView(true);
    setStopSync(false);
    setDeleteSync(false);
  };

  const disableSyncing = (item) => {
    setDisableSyncBtnQuery("progress");
    stopSync(item, (succCallback) => {
      setListView(true);
      setStopSync(false);
      setDeleteSync(false);
      setDisableSyncBtnQuery("");
    });
  };

  const enableSyncing = (item) => {
    setEnableSyncBtnQuery("progress");
    stopSync(item, (succCallback) => {
      setListView(true);
      setStopSync(false);
      setDeleteSync(false);
      setEnableSyncBtnQuery("");
    });
  };

  return (
    <>
      {isListView && (
        <div className={classes.listItemWrap} id={item.id} key={item.id}>
          <div className={classes.listItemLeft}>
            <Typography variant="body2" className={classes.accountSubTitle}>
              {` Calendar: ${item.calendarTitle}`}
            </Typography>
            <Typography
              title={item.accountId}
              variant="h5"
              className={classes.titleText}
            >
              <Truncate
                trimWhitespace={true}
                width={250}
                ellipsis={<span>...</span>}
              >
                {item.accountId.split("/").pop()}
              </Truncate>
            </Typography>
            {urlText ? (
              <Typography
                variant="h5"
                className={classes.copyURL}
                onClick={() => {
                  navigator.clipboard.writeText(mail);
                }}
              >
                {" "}
                <FormattedMessage
                  id="profile-settings-dialog.apps-integrations.apple-calendar.copy-calender.title"
                  defaultMessage="Copy URL"
                />
              </Typography>
            ) : (
              <Typography
                variant="body2"
                title={item.selectedTeamName}
                className={classes.accountSubTitle}
              >
                <Truncate
                  trimWhitespace={true}
                  width={250}
                  ellipsis={<span>...</span>}
                >
                  {`Team : ${
                    item.selectedTeamName
                      ? item.selectedTeamName.split("/").pop()
                      : "-"
                  }`}
                </Truncate>
              </Typography>
            )}
          </div>
          <div className={classes.listItemRight}>
            <div className={classes.iconWrap}>
              {/* Delete Icon */}
              <SvgIcon
                viewBox="0 0 18 20.001"
                className={classes.deleteIcon}
                onClick={() => {
                  deleteSyncHandler(item);
                }}
              >
                <DeleteListIcon />
              </SvgIcon>

              {/* Setting Icon */}
              {item.syncEnabled && (
                <SvgIcon
                  viewBox="0 0 20 19.999"
                  className={classes.settingIcon}
                  onClick={() => {
                    updateSync(item);
                  }}
                >
                  <SettingListIcon />
                </SvgIcon>
              )}

              <DefaultSwitch
                checked={item.syncEnabled}
                onChange={() => {
                  toggleSyncHandler(item);
                }}
                color={item.syncEnabled ? undefined : "Dark"}
              />
            </div>
          </div>
        </div>
      )}

      {/* Stop sync View */}
      {isStopSync
        ? editedItem.id == item.id && (
            <div className={classes.listItemStopSyncWrap}>
              <div className={classes.listItemLeft}>
                <Typography variant="body2" className={classes.accountSubTitle}>
                  {" "}
                  Are you sure you want to{" "}
                  {item.syncEnabled ? "stop " : "enable"} syncing the
                </Typography>
                <Typography variant="h5" className={classes.titleText}>
                  {" "}
                  <Truncate
                    trimWhitespace={true}
                    width={250}
                    ellipsis={<span>...</span>}
                  >
                    {item.accountId.split("/").pop()}
                  </Truncate>{" "}
                  <span>calendar?</span>{" "}
                </Typography>
              </div>
              <div className={classes.listItemRight}>
                <div className={classes.listItemStopSyncBtns}>
                  <CustomButton
                    onClick={cancelSyncHandler}
                    variant="contained"
                    disabled={false}
                    btnType={"gray"}
                    style={{ padding: `5px`, fontSize: "12px" }}
                  >
                    <FormattedMessage
                      id="common.action.cancel.label"
                      defaultMessage="Cancel"
                    />
                  </CustomButton>
                  &nbsp;
                  <CustomButton
                    onClick={() => {
                      item.syncEnabled
                        ? disableSyncing(item)
                        : enableSyncing(item);
                    }}
                    variant="contained"
                    disabled={  
                      item.syncEnabled
                        ? false
                        : enableSyncBtnQuery
                      }
                    btnType={item.syncEnabled ? "danger" : "success"}
                    style={{ padding: `5px 9px`, fontSize: "12px" }}
                    query={ 
                      item.syncEnabled
                        ? disableSyncBtnQuery
                        : enableSyncBtnQuery
                    }
                  >
                    {item.syncEnabled ? (
                      <FormattedMessage
                        id="profile-settings-dialog.apps-integrations.common.confirmation.stop-button.label"
                        defaultMessage="Stop Sync"
                      />
                    ) : (
                      <FormattedMessage
                        id="profile-settings-dialog.apps-integrations.outlook.confirmation.enable-button.label"
                        defaultMessage="Enable"
                      />
                    )}
                  </CustomButton>
                </div>
              </div>
            </div>
          )
        : null}

      {/* Delete sync View */}
      {isDeleteSync
        ? editedItem.id == item.id && (
            <>
              <div className={classes.listItemStopSyncWrap}>
                <div className={classes.listItemLeft}>
                  <Typography
                    variant="body2"
                    className={classes.accountSubTitle}
                  >
                    {" "}
                    Would you like to delete only the sync or delete all data with the
                  </Typography>
                  <Typography variant="h5" className={classes.titleText}>
                    <Truncate
                      trimWhitespace={true}
                      width={250}
                      ellipsis={<span>...</span>}
                    >
                      {item.accountId.split("/").pop()}
                    </Truncate>{" "}
                    <span>calendar?</span>{" "}
                  </Typography>
                </div>
                <div className={classes.listItemRight}></div>
              </div>
              <div className={classes.listItemStopSyncBtns}>
                <CustomButton
                  onClick={cancelSyncHandler}
                  variant="contained"
                  disabled={false}
                  btnType={"gray"}
                  style={{ padding: `5px`, fontSize: "12px" }}
                >
                  <FormattedMessage
                    id="common.action.cancel.label"
                    defaultMessage="Cancel"
                  />
                </CustomButton>
                &nbsp;
                &nbsp;
                <CustomButton
                  onClick={() => {
                    deleteCalWithData(item);
                  }}
                  variant="contained"
                  disabled={false}
                  btnType={"danger"}
                  style={{ padding: `5px 10px`, fontSize: "12px" }}
                  query={deleteBtnDataQuery}
                >
                  <FormattedMessage
                    id="common.action.delete.label3"
                    defaultMessage=" Delete Sync with Data"
                  />
                </CustomButton>
                &nbsp;
                &nbsp;
                <CustomButton
                  onClick={() => {
                    deleteCal(item);
                  }}
                  variant="contained"
                  disabled={false}
                  btnType={"danger"}
                  style={{ padding: `5px 10px`, fontSize: "12px" }}
                  query={deleteBtnQuery}
                >
                  <FormattedMessage
                    id="common.action.delete.label2"
                    defaultMessage="Delete Sync Only"
                  />
                </CustomButton>
              </div>
            </>
          )
        : null}
    </>
  );
}

export default withStyles(CalendarListStyles, { withTheme: true })(
  CalendarListItem
);
