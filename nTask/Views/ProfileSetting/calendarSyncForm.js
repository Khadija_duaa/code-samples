import React, { useState, useEffect } from "react";
import Typography from "@material-ui/core/Typography";
import FilledRadio from "../../components/Form/Radio/FilledRadio";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import CalendarSyncFormStyles from "./styles";
import CustomButton from "../../components/Buttons/CustomButton";
import CreateableSelectDropdown from "../../components/Dropdown/SelectCreateableDropdown/SelectCreateableDropdown";

import {
  generateTeamsData,
  generateMeetingData,
  generateTaskData,
  generateIssueData,
  generateProjectsData,
  generateWorkspacesData,
  generateSelectData,
} from "../../helper/generateGoogleCalendarData";
import isEmpty from "lodash/isEmpty";
import cloneDeep from "lodash/cloneDeep";
import { FormattedMessage, injectIntl } from "react-intl";

function CalendarSyncForm(props) {
  const {
    options,
    classes,
    theme,
    onRadioChange,
    value,
    btnText,
    syncWithCalendar,
    data,
    setDefaultParentState,
    AddCalendarSettings,
    intl
  } = props;

  const [selectedRadioOpt, setSelectedRadioOpt] = useState(
    "Planned start/end dates"
  );
  const [selectedTeam, setSelectedTeam] = useState(null);
  const [selectedWorkspaces, setSelectedWorkspaces] = useState(null);
  const [workspaces, setWorkspaces] = useState(null);
  const [projects, setProjects] = useState(null);
  const [selectedMeeting, setSelectedMeeting] = useState(null);
  const [selectedTask, setSelectedTask] = useState(null);
  const [selectedIssue, setSelectedIssue] = useState(null);
  const [selectedProjects, setSelectedProjects] = useState(null);
  const [btnQuery, setBtnQuery] = useState("");
  const radioOptions = ["Planned start/end dates", "Actual start/end dates"]; //Options user will select to tell the reason of cancellation

  useEffect(() => {
    let selectedTeamIfAny = getSelectedTeam();
    setSelectedTeam(selectedTeamIfAny);
    selectedTeamIfAny && getWorkspaces(selectedTeamIfAny);

    let selectedWorkspacesIfAny = getSelectedWorkspaces();
    setSelectedWorkspaces(selectedWorkspacesIfAny);
    selectedWorkspacesIfAny && getProjects(selectedWorkspacesIfAny);

    let selectedProjectsIfAny = getSelectedProjects();
    setSelectedProjects(selectedProjectsIfAny);

    let selectedRadioOpt = getRadioOpt();
    setSelectedRadioOpt(selectedRadioOpt);

    let meetingTypeSelected = getMeetingType();
    setSelectedMeeting(meetingTypeSelected);

    let issueTypeSelected = getIssueType();
    setSelectedIssue(issueTypeSelected);

    let taskTypeSelected = getTaskType();
    setSelectedTask(taskTypeSelected);
  }, []);

  const getRadioOpt = () => {
    if (data && data.syncCalender.dateType == 1) {
      return "Planned start/end dates";
    } else return "Actual start/end dates";
  };

  const getMeetingType = () => {
    let meetingArr = generateMeetingData(intl);
    if (data.syncCalender.meetings == 1) {
      return meetingArr[0];
    } else if (data.syncCalender.meetings == 2) {
      return meetingArr[1];
    } else return null;
  };

  const getIssueType = () => {
    let issueArr = generateIssueData(intl);
    if (data.syncCalender.issues == 1) {
      return issueArr[0];
    } else if (data.syncCalender.issues == 2) {
      return issueArr[1];
    } else return null;
  };

  const getTaskType = () => {
    let taskArr = generateTaskData(intl);
    if (data.syncCalender.tasks == 1) {
      return taskArr[0];
    } else if (data.syncCalender.tasks == 2) {
      return taskArr[1];
    } else return null;
  };

  const radioHandler = (event) => {
    setSelectedRadioOpt(event.target.value);
  };
  const getSelectedTeam = () => {
    if (data && data.syncCalender.selectedTeamId) {
      let teams = generateTeamsData(data.teams);
      let selectedTeam = teams.find(
        (t) => t.id == data.syncCalender.selectedTeamId
      );
      return selectedTeam;
    } else return null;
  };
  const getSelectedWorkspaces = () => {
    if (data && data.syncCalender.workspaceIds) {
      let workspaces = generateWorkspacesData(data.workspaces, 'GOOGLE', intl);
      let selectedWorkspaces = [];
      data.syncCalender.workspaceIds.map((w1) => {
        workspaces.map((w2) => {
          if (w2.id == w1) selectedWorkspaces.push(w2);
        });
      });
      return selectedWorkspaces;
    } else return null;
  };
  const getSelectedProjects = () => {
    if (data && data.syncCalender.projectIds) {
      let projects = generateProjectsData(data.projects, intl);
      let selectedProjects = [];
      data.syncCalender.projectIds.map((p1) => {
        projects.map((p2) => {
          if (p2.id == p1) selectedProjects.push(p2);
        });
      });
      return selectedProjects;
    } else return null;
  };

  const getTeams = () => {
    if (!isEmpty(data.teams)) {
      let teams = generateTeamsData(data.teams);
      return teams;
    } else return [];
  };

  const selectTeam = (key, value) => {
    setSelectedTeam(value);
    setSelectedWorkspaces(null);
    setSelectedProjects(null);
    getWorkspaces(value);
  };

  const handleSelectWorkspace = (key, value) => {
    setSelectedWorkspaces(value);
    setSelectedProjects([]);
    getProjects(value);
  };

  const getWorkspaces = (value) => {
    let relatedWorkspaces = data.workspaces.filter(
      (w) => w.companyId == value.id
    );
    let desiredWorkspaceObj = generateWorkspacesData(relatedWorkspaces, 'GOOGLE', intl);
    setWorkspaces(desiredWorkspaceObj);
  };

  const getProjects = (value) => {
    let projects = [];
    value.map((v) => {
      data.projects.map((p) => {
        if (p.teamId == v.id) {
          projects.push(p);
        }
      });
    });
    let desiredProjectObj = generateProjectsData(projects, intl);
    setProjects(desiredProjectObj);
  };

  const handleSelectMeeting = (key, value) => {
    setSelectedMeeting(value);
  };
  const handleSelectTask = (key, value) => {
    setSelectedTask(value);
  };
  const handleSelectIssue = (key, value) => {
    setSelectedIssue(value);
  };
  const setDefaultState = (key, value) => {
    setSelectedIssue(null);
    setSelectedTask(null);
    setSelectedMeeting(null);
    setSelectedTeam(null);
    setDefaultParentState();
  };
  const handleSelectProjects = (key, value) => {
    setSelectedProjects(value);
  };

  const handleClearProject = (p) => {
    let projects = selectedProjects.filter((s) => s.id !== p.id);
    setSelectedProjects(projects);
  };
  const handleRemoveWorkspace = (p) => {
    let workspaces = selectedWorkspaces.filter((s) => s.id !== p.id);
    setSelectedWorkspaces(workspaces);
    setSelectedProjects([]);
    getProjects(workspaces);
  };

  const saveCalendarDetails = () => {
    setBtnQuery("progress");
    let obj = cloneDeep(data.syncCalender);
    obj.dateType = selectedRadioOpt == "Planned start/end dates" ? 1 : 2;
    obj.selectedTeamId = selectedTeam ? selectedTeam.id : null;
    obj.selectedTeamName = selectedTeam ? selectedTeam.label : null;
    obj.projectIds = !isEmpty(selectedProjects)
      ? selectedProjects.map((p) => p["id"])
      : null;
    obj.workspaceIds = !isEmpty(selectedWorkspaces)
      ? selectedWorkspaces.map((w) => w["id"])
      : null;
    obj.issues = selectedIssue ? selectedIssue.value : 0;
    obj.tasks = selectedTask ? selectedTask.value : 0;
    obj.meetings = selectedMeeting ? selectedMeeting.value : 0;
    AddCalendarSettings(obj, (callback) => {
      setBtnQuery("");
    });
  };
  return (
    <>
      {/* Calendar team Form! */}
      <div className={classes.teamForm}>
        <CreateableSelectDropdown
          data={() => {
            return getTeams();
          }}
          label={<FormattedMessage id="profile-settings-dialog.apps-integrations.common.team.label" defaultMessage="Team" />}
          placeholder={<FormattedMessage id="profile-settings-dialog.apps-integrations.common.team.placeholder" defaultMessage="Select Team" />}
          name="team"
          selectOptionAction={selectTeam}
          createOptionAction={null}
          removeOptionAction={null}
          type="task"
          selectedValue={selectedTeam}
          avatar={false}
          createLabelValidation={null}
          isMulti={false}
          disableIndicator={true}
          noOptionMessage={<FormattedMessage id="profile-settings-dialog.apps-integrations.common.team.error" defaultMessage="No teams found." />}
          isSearchable={true}
          acceptDataInFormOfFun={true}
        />

        <CreateableSelectDropdown
          data={workspaces}
          label={<FormattedMessage id="profile-settings-dialog.apps-integrations.common.workspace.label" defaultMessage="Workspace(s)" />}
          placeholder={<FormattedMessage id="profile-settings-dialog.apps-integrations.common.workspace.placeholder" defaultMessage="Select one or more workspaces" />}
          name="workspaces"
          selectOptionAction={handleSelectWorkspace}
          createOptionAction={null}
          removeOptionAction={handleRemoveWorkspace}
          type="workspaces"
          // createText="Invite Member"
          selectedValue={selectedWorkspaces}
          avatar={false}
          createLabelValidation={null}
          noOptionMessage={<FormattedMessage id="profile-settings-dialog.apps-integrations.common.workspace.error" defaultMessage="No workspaces found." />}
          acceptDataInFormOfFun={false}
          isMulti={true}
          isSearchable={true}
          disableIndicator={true}
        />
      </div>

      {/* Calendar sync Form! */}
      <div className={classes.syncForm}>
        <Typography variant={"h5"} className={classes.titleText}>
          <FormattedMessage id="profile-settings-dialog.apps-integrations.common.select.label" defaultMessage="Select what you would like to sync" />        </Typography>
        <div className={classes.calendarInputWrap}>
          <CreateableSelectDropdown
            data={projects}
            label={<FormattedMessage id="profile-settings-dialog.apps-integrations.common.project.label" defaultMessage="Project(s)" />}
            placeholder={<FormattedMessage id="profile-settings-dialog.apps-integrations.common.project.placeholder" defaultMessage="Select one or more projects" />}
            name="projects"
            selectOptionAction={handleSelectProjects}
            removeOptionAction={handleClearProject}
            type="projects"
            isMulti={true}
            // createText="Invite Member"
            selectedValue={selectedProjects}
            avatar={false}
            createLabelValidation={null}
            noOptionMessage={<FormattedMessage id="profile-settings-dialog.apps-integrations.common.project.error" defaultMessage="No projects found." />}
            acceptDataInFormOfFun={false}
          />
        </div>
        <div className={classes.calendarInputWrap}>
          <CreateableSelectDropdown
            data={() => {
              return generateTaskData(intl);
            }}
            isClearable={true}
            label={<FormattedMessage id="profile-settings-dialog.apps-integrations.common.task.label" defaultMessage="Tasks" />}
            placeholder={<FormattedMessage id="profile-settings-dialog.apps-integrations.common.task.placeholder" defaultMessage="Select option" />}
            name="taskType"
            selectOptionAction={handleSelectTask}
            selectClear={handleSelectTask}
            selectedValue={selectedTask}
            type="taskType"
            isMulti={false}
            isDisabled={null}
            styles={{ marginLeft: "4px" }}
          />

          <CreateableSelectDropdown
            data={() => {
              return generateMeetingData(intl);
            }}
            isClearable={true}
            label={<FormattedMessage id="profile-settings-dialog.apps-integrations.common.meeting.label" defaultMessage="Meetings" />}
            placeholder={<FormattedMessage id="profile-settings-dialog.apps-integrations.common.meeting.placeholder" defaultMessage="Select option" />}
            name="meetingType"
            selectOptionAction={handleSelectMeeting}
            selectClear={handleSelectMeeting}
            selectedValue={selectedMeeting}
            type="meetingType"
            isMulti={false}
            isDisabled={null}
            styles={{ marginRight: "4px" }}
          />
        </div>

        <div className={classes.calendarInputWrap}>
          <CreateableSelectDropdown
            data={() => {
              return generateIssueData(intl);
            }}
            isClearable={true}
            label={<FormattedMessage id="profile-settings-dialog.apps-integrations.common.issue.label" defaultMessage="Issues" />}
            placeholder={<FormattedMessage id="profile-settings-dialog.apps-integrations.common.issue.placeholder" defaultMessage="Select option" />}
            name="IssueType"
            selectOptionAction={handleSelectIssue}
            selectClear={handleSelectIssue}
            selectedValue={selectedIssue}
            type="IssueType"
            isMulti={false}
            isDisabled={null}
            styles={{ marginRight: "4px", maxWidth: "50%" }}
          />

        </div>
      </div>
      <div className={classes.syncDateSection}>
        <Typography variant={"h5"} className={classes.titleText}>
          <FormattedMessage id="profile-settings-dialog.apps-integrations.common.select-date.label" defaultMessage="Select dates on which you would like to sync the calendar" />
        </Typography>
        <Typography variant={"body2"} className={classes.subText}>
          <FormattedMessage id="profile-settings-dialog.apps-integrations.common.sync.label" defaultMessage="Sync calendar with" />
        </Typography>
        <div className={classes.radioWrapper}>
          <FilledRadio
            options={radioOptions}
            value={selectedRadioOpt}
            onRadioChange={radioHandler}
            intl={intl}
          />
        </div>
      </div>

      {/* Footer */}
      <div className={classes.calendarDropDownBtns}>
        <CustomButton
          onClick={setDefaultState}
          variant="contained"
          disabled={false}
          btnType={"gray"}
        >
          Cancel
        </CustomButton>
        &nbsp;
        <CustomButton
          onClick={saveCalendarDetails}
          variant="contained"
          // disabled={btnQuery}
          btnType={"blue"}
          query={btnQuery}
          disabled={
            isEmpty(selectedWorkspaces) ||
            isEmpty(selectedTeam) ||
            (isEmpty(selectedProjects) &&
              isEmpty(selectedMeeting) &&
              isEmpty(selectedTask) &&
              isEmpty(selectedIssue)) || btnQuery
          }
        >
          {btnText}
        </CustomButton>
      </div>
    </>
  );
}
CalendarSyncForm.defaultProps = {
  data: {},
  setDefaultParentState: () => { },
  AddCalendarSettings: () => { },
};

export default withStyles(CalendarSyncFormStyles, { withTheme: true })(
  CalendarSyncForm
);
