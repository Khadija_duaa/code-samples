import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import DropdownArrow from "@material-ui/icons/ArrowDropDown";
import { components } from "react-select";
import DefaultSwitch from "../../components/Form/Switch";
import NotificationIcon from "@material-ui/icons/Notifications";

import {
  SaveLocalization,
  UpdateUserPreferences,
} from "../../redux/actions/profileSettings";
import { UpdateLocalization } from "../../redux/actions/profile";
import {
  convertDayToWeekday,
  convertWeekdayToDay,
} from "../../utils/convertTime";
import { profileSettingsHelpText } from "../../components/Tooltip/helptext";
import CustomTooltip from "../../components/Tooltip/Tooltip";
import { FormattedMessage,injectIntl } from "react-intl";
import {translateNotification} from "../../utils/notificationTranslation";
class NotificationPreferences extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profileModified: false,
      userPreference: [],
      isChanged: false,
      isDisabled: false,
      disableId: "",
    };

    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount = () => {
    this.setState({
      userPreference: this.props.userPreferenceState.data.preferences,
    });
  };

  componentDidUpdate(prevProps, prevState) {
    const { isChanged } = this.state;
    const { userPreferenceState } = this.props;
    if (isChanged) {
      this.setState({
        userPreference: userPreferenceState.data.preferences,
        isChanged: false,
      });
    }
  }

  handleChange(id, event) {
    let temparr = [];
    let mainObj = this.props.userPreferenceState.data.preferences || null;
    mainObj
      ? mainObj.map((obj) => {
          let flag = false;
          let totalSubTasks = 0;
          let iterator = -1;

          if (obj.notificationId === id) {
            let arr = [];
            arr.push({
              NotificationId: obj.notificationId,
              isTurnedOn: !obj.isTurnedOn,
            });
            obj.subNotifications.map((iobj) => {
              arr.push({
                NotificationId: iobj.id,
                isTurnedOn: !obj.isTurnedOn,
                parrentId: obj.notificationId,
              });
            });
            temparr = arr;
          } else {
            if (obj.subNotifications.length > 0) {
              totalSubTasks = obj.subNotifications.length;
              let arr = [];
              obj.subNotifications.map((iobj) => {
                if (iobj.id === id) {
                  if (iterator === -1) iterator = 0;
                  iterator = iterator + 1;
                  flag = true;
                  arr.push({
                    NotificationId: iobj.id,
                    isTurnedOn: !iobj.isTurnedOn,
                    parrentId: obj.notificationId,
                  });
                }
              });
              if (flag) {
                if (
                  totalSubTasks === iterator &&
                  iterator === obj.subNotifications.length
                ) {
                  arr.push({
                    NotificationId: obj.notificationId,
                    isTurnedOn: !obj.isTurnedOn,
                  });
                }
                temparr = arr;
                flag = false;
              } else {
                arr = [];
              }
            }
          }
        })
      : null;
    mainObj.map((obj) => {
      let counter = 0;
      let total = 0;
      let checkflag = false;
      let parentId = obj.notificationId;

      if (obj.notificationId !== id) {
        //Checks whether with this check the preference will active all preferences or not
        total = obj.subNotifications.length;
        obj.subNotifications.map((iobj) => {
          if (iobj.id === id) {
            checkflag = true;
          }
          if (event.target.checked === iobj.isTurnedOn) {
            counter = counter + 1;
          }
        });
        if (checkflag && counter === total - 1) {
          temparr.push({
            NotificationId: parentId,
            isTurnedOn: event.target.checked,
          });
        }
      }

      if (obj.notificationId !== id) {
        //Checks whether with this check the preference will deActive all preferences or not
        counter = 0;
        total = obj.subNotifications.length;
        obj.subNotifications.map((iobj) => {
          if (iobj.id === id) {
            checkflag = true;
          }
          if (event.target.checked === !iobj.isTurnedOn) {
            counter = counter + 1;
          }
        });
        if (checkflag && counter === total && event.target.checked === false) {
          temparr.push({
            NotificationId: parentId,
            isTurnedOn: event.target.checked,
          });
        }
      }
    });
    let temp = {
      updatePreferencesList: temparr,
      updatePreferencesListString: null,
    };
    this.setState({ isDisabled: true, disableId: id });
    this.props.UpdateUserPreferences(temp, (data) => {
      if (data.response.status === 200) {
        this.setState({
          isChanged: true,
          isDisabled: false,
          disableId: id,
          profileModified: false,
        });
        this.props.showSnackBar(
          "Notification Preferences Updated",
          false,
          "success"
        );
      } else {
        this.setState({
          isChanged: true,
          isDisabled: false,
          disableId: id,
        });
        this.props.showSnackBar(
          "Unable to save Notification Preferences",
          false,
          "error"
        );
      }
    });
  }

  render() {
    const { userPreference } = this.state;
    const { classes, theme, intl } = this.props;

    return (
      <>
        <div className={classes.accountPrefFormCnt}>
          <div
            className={classes.accountPrefHeading}
            style={{ marginBottom: 10 }}
          >
            <div className={classes.accountPrefHeadingInner}>
              <NotificationIcon
                classes={{ root: classes.accountPrefHeadingIcon }}
                fontSize="small"
                htmlColor={theme.palette.secondary.light}
              />
              <FormattedMessage
                id="profile-settings-dialog.notification-preferences.title"
                defaultMessage="Notification Preferences"
              />
            </div>
            <div>
              <CustomTooltip
                helptext={
                  <FormattedMessage
                    id="profile-settings-dialog.notification-preferences.message"
                    defaultMessage={
                      profileSettingsHelpText.notificationPreferencesHelpText
                    }
                  />
                }
                iconType="help"
                position="static"
              />
            </div>
          </div>

          <React.Fragment>
            <ul className={classes.accountPrefContentList}>
              <React.Fragment>
                {userPreference
                  ? userPreference.map((obj) => {
                      return (
                        <li key={obj.notificationId}>
                          <div className={classes.switchInnerCnt}>
                            <div className={classes.mainSwitchCnt}>
                              <p>{translateNotification(obj.notificationPreferenceType, intl)}</p>
                              <div>
                                <DefaultSwitch
                                  checked={obj.isTurnedOn}
                                  onChange={(event) => {
                                    this.handleChange(
                                      obj.notificationId,
                                      event
                                    );
                                  }}
                                  size={"medium"}
                                  value={obj.isTurnedOn}
                                />
                              </div>
                            </div>
                            {obj.subNotifications.length > 0 ? (
                              <ul className={classes.accountPrefContentList}>
                                {obj.subNotifications.map((iObj) => {
                                  return (
                                    <li>
                                      <p>{translateNotification(iObj.name, intl)}</p>
                                      <div>
                                        <DefaultSwitch
                                          checked={iObj.isTurnedOn}
                                          onChange={(event) => {
                                            this.handleChange(iObj.id, event);
                                          }}
                                          value={iObj.isTurnedOn}
                                        />
                                      </div>
                                    </li>
                                  );
                                })}
                              </ul>
                            ) : null}
                          </div>
                        </li>
                      );
                    })
                  : null}
              </React.Fragment>
            </ul>
          </React.Fragment>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    constantsState: state.constants,
    profileState: state.profile,
    userPreferenceState: state.userPreference,
  };
};

//export default AccountPreferences;
export default compose(
  withRouter,
  injectIntl,
  connect(mapStateToProps, {
    SaveLocalization,
    UpdateLocalization,
    UpdateUserPreferences,
  })
)(NotificationPreferences);
