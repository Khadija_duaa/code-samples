const profileSetting = (theme) => ({
  dialogCnt: {},
  dialogPaperCnt: {
    // overflowY: "visible",
    background: theme.palette.common.white,
    maxWidth: 820,
    borderRadius: 6
  },
  defaultDialogTitle: {
    padding: "25px",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
  },
  defaultDialogContent: {
    padding: 0,
    overflowY: "visible",
  },
  defaultDialogAction: {
    padding: "0 25px 25px",
  },
  profileSettingListCnt: {
    background: theme.palette.background.items,
    borderRight: `1px solid ${theme.palette.border.lightBorder}`,
  },
  profileSettingSideList: {
    "& $listItemSelected": {
      background: "rgba(0, 0, 0, 0.14)",
      boxShadow: "none",
      // border: `1px solid ${theme.palette.border.lightBorder}`,
      borderLeft: "none",
      borderRight: "none",

      "& $profileSideListText": {
        color: theme.palette.text.primary,
      },
    },
  },
  listItem: {},
  profileSideListText: {
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular,
    color: theme.palette.text.secondary,
  },
  listItemSelected: {
  },
  profileSettingContentCnt: {
    // padding: "40px 20px 20px 20px"
  },
  personalInfoFormCnt: {
    padding: "40px 20px 20px 20px",
  },
  accountPrefFormCnt: {
    padding: 20,
  },
  selectFormControl: {
    marginBottom: 40,
  },
  formLabel: {
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular,
  },
  selectLabel: {
    transform: "translate(6px, -18px) scale(1)",
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular,
  },
  selectLabelLanguage: {
    transform: "translate(6px, -23px) scale(1)",
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular,
  },
  profileDialogActionBtns: {
    boxShadow: "0 -6px 19px -10px #d0d0d0",
    padding: "15px 20px",
  },
  profilePicCnt: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    borderRight: `1px solid ${theme.palette.border.lightBorder}`,
    paddingRight: 20,
    marginRight: 20,
    "&:hover $deleteProfileImageIcon": {
      display: "block",
    },
  },
  profilePic: {
    position: "relative",
    width: 60,
    height: 60,
    marginBottom: 20,
    borderRadius: "100%",
    background: theme.palette.background.light,
    "& img": {
      borderRadius: "50%",
    },
  },
  profileInfoList: {
    margin: 0,
    padding: 0,
    listStyleType: "none",

    "& li": {
      marginBottom: 10,
      lineHeight: "normal",
      color: theme.palette.text.secondary,
      fontSize: "12px !important",
      display: "flex",
    },
  },
  profileInfoIcon: {
    marginRight: 10,
  },
  personalInfoTopCnt: {
    padding: 20,
  },
  deleteProfileImageIcon: {
    position: "absolute",
    right: -2,
    top: -6,
    background: theme.palette.background.default,
    borderRadius: "100%",
    display: "none",
    cursor: "pointer",
  },
  uploadPhotoBtn: {
    color: theme.palette.text.secondary,
    textDecoration: "underline",
    fontSize: "12px !important",
    margin: 0,
    cursor: "pointer",
  },
  accountPrefHeading: {
    padding: "15px 20px",
    borderRadius: 5,
    marginBottom: 40,
    fontWeight: theme.typography.fontWeightRegular,
    background: theme.palette.background.light,
    fontSize: "16px !important",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  sessionManagementHeading: {
    padding: "4px 20px",
    borderRadius: 5,
    marginBottom: 40,
    fontWeight: theme.typography.fontWeightRegular,
    background: theme.palette.background.light,
    fontSize: "16px !important",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  accountPrefHeadingInner: {
    display: "flex",
    alignItems: "center",
  },
  accountPrefHeadingIcon: {
    marginRight: 5,
  },
  accountPrefContentStaticList: {
    // margin: "0 0 30px 0",
    padding: 0,
    listStyleType: "none",
    "& li": {
      padding: "15px 0 15px 10px",
      lineHeight: "normal",
      color: theme.palette.text.secondary,
      fontSize: "12px !important",
      borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      "& $noFlexInner p": {
        margin: 0,
      },
    },
    "& $noFlex": {
      display: "block",
    },
  },
  accountAttachMethod: {
    width: "100px",
    "& img": {
      width: "20px",
      verticalAlign: "bottom",
      marginRight: "10px",
    },
  },
  accountAttachEmail: {
    width: "200px",
    textOverflow: "ellipsis",
    overflow: "hidden",
    whiteSpace: "nowrap",
    color: "rgb(152, 193, 74)",
  },
  accountPrefContentList: {
    padding: 0,
    listStyleType: "none",
    "& li": {
      lineHeight: "normal",
      color: theme.palette.text.secondary,
      fontSize: "12px !important",
      "& ul li": {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
        padding: "15px 0 15px 10px",
        marginLeft: 15,
      },
      "& p": {
        margin: 0,
      },
      "& $mainSwitchCnt": {
        display: "flex",
        justifyContent: "space-between",
        borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
        alignItems: "center",
        padding: "15px 0 15px 10px",
        "& p": {
          color: theme.palette.text.primary,
          fontWeight: theme.typography.fontWeightRegular,
        },
      },
    },
  },
  mainSwitchCnt: {},
  noFlexInner: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  noFlex: {},
  helpIconBtn: {
    padding: 0,
    "&:hover": {
      background: "transparent",
    },
  },
  twoFactorAuthInputCnt: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "baseline",
  },
  twoFactorAuthEmailInputCnt: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "baseline",
    marginTop: 20,
  },
  appIntegrationList: {
    margin: 0,
    padding: 0,
    listStyleType: "none",
    minHeight: 600,
    padding: 20,
    "& li": {
      padding: "11px 8px 11px 12px",
      borderRadius: 5,
      border: `1px solid ${theme.palette.border.lightBorder}`,
      display: "flex",
      marginBottom: 12,
      justifyContent: "space-between",
      alignItems: "center",
      "& p": {
        margin: "0 0 0 10px",
        fontSize:"16px !important",
        fontWeight: theme.typography.fontWeightRegular,
      },
    },
  },
  iconNameCnt: {
    display: "flex",
    alignItems: "center",
  },
  logoutAllDevicesText: {
    color: theme.palette.text.green,
    margin: "1px 0 0 10px",
    textDecoration: "underline",
    cursor: "pointer",
  },
  optionChecked: {},
  tooltip: {
    fontSize: "1rem !important",
    backgroundColor: "#f8f8f8",
    "& .panel-body": {
      padding: 10,
      color: "#000",
      "& ul": {
        padding: "6px 0 0 24px",
        "& $optionChecked": {
          color: "#01a52f",
          listStyle: "none",
          "&:before": {
            position: "absolute",
            left: 22,
            content: '"✓"',
          },
        },
        "& li": {
          color: "#656565",
        },
      },
    },
  },
  progressBar: {
    height: 12,
    marginBottom: 10,
    backgroundColor: "#dbdbdb",
  },
  redBar: {
    backgroundColor: "#d9534f",
  },
  orangeBar: {
    backgroundColor: "#f0ad4e",
  },
  greenBar: {
    backgroundColor: "#00CC90",
  },
  confirmEmailText: {
    color: theme.palette.secondary.main,
    cursor: "pointer",
    textDecoration: "underline",
  },
  cancelEmailText: {
    color: theme.palette.text.danger,
    cursor: "pointer",
    textDecoration: "underline",
  },
  zoomIcon: {
    color: theme.palette.text.azure
    },
  dayLightSaving: {
    color: "#333333",
    padding: "0 0 10px 10px",
    marginBottom: 15,
    display: "flex",
    fontSize: "12px !important",
    lineHeight: "normal",
    alignItems: "center",
    borderBottom: "1px solid rgba(221, 221, 221, 1)",
    justifyContent: "space-between",
  },
  zoomIcon: {
    color: theme.palette.text.azure,
  },
  googleMeetIcon: {
    width:'auto',
  },
  outlookIcon: {
    color: theme.palette.text.azure,
  },
  appleIcon: {
    color: theme.palette.common.primary,
  },
  learnMore: {
    color: theme.palette.common.primary,
    margin: 0,
    marginRight: `16px !important`,
    padding: 5,
    textDecoration: "underline",
    "&:hover": {
      cursor: "pointer",
    },
  },
  accountSubTitle: {
    color: theme.palette.common.primary,
    margin: 0,
    marginRight: `14px !important`,
  },
  googleCalendarTitles: {
    borderBottom: `1px solid ${theme.palette.border.grayLighter}`,
  },
  titleText: {
    color: theme.palette.common.black,
    margin: `0 0 10px 0`,
  },
  calendarWrapper: {
    background: theme.palette.background.airy,
    marginTop: -23,
    marginBottom: 12,
    borderTop: `3px solid ${theme.palette.background.grayLighter}`,
    border: `1px solid ${theme.palette.background.grayLighter}`,
    borderRadius: `0px 0px 4px 4px`,
    padding: `15px 20px`,
    "& listItemWrap:last-child": {
      border: `none`,
    },
  },
  dropDownTitle: {
    marginBottom: 20,
  },
  calendarDropDownBtns: {
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "center",
  },
  calendarInputWrap: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  radioWrapper: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  syncDateSection: {
    borderTop: `1px solid ${theme.palette.background.grayLighter}`,
    borderBottom: `1px solid ${theme.palette.background.grayLighter}`,
    padding: `20px 0 `,
    marginBottom: 20,
  },
  teamForm: {
    borderBottom: `1px solid ${theme.palette.border.grayLighter}`,
    marginBottom: 10,
    padding: `10px 0`,
  },
  tooltip: {
    fontSize: "12px !important",
    backgroundColor: theme.palette.common.black,
    },
  betaIcon:{
    backgroundColor : "#FE6363",
    marginLeft: 10,
    borderRadius: 4,
    color: "white",
    padding: "3px 7px 3px 6px",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
  },
  infoIcon:{
    marginBottom: "-2.5px",
    fontSize: "13px !important",
    marginLeft: 4
  },
   optionText: {
    // width: 260,
    overflow: "hidden",
    whiteSpace: "nowrap",
    lineHeight: "normal",
    textOverflow: "ellipsis",
  },
  betaIconDropDown:{
    background: "#fe63632e",
    // marginRight: 10,
    borderRadius: 4,
    color: "#F24C4C",
    padding: "3px 7px 3px 6px",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
    fontSize: "11px !important"
  },
    socialIconcontainer: {
      display: "flex",
      flexDirection: "row",
      alignItems: "center",
    },
    socialTitle: {
      fontSize: "14px !important",
      color: "#0000008A",
      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: 500,
      marginLeft: 5
    },
    signInOption: {
      border: `1px solid ${theme.palette.border.lightBorder}`,
      borderRadius: "4px",
      boxSizing: "border-box",
      whiteSpace: 'nowrap',
      // width: 52,
      // height: 52,
      // padding: "5px 10px 5px 12px",
      padding: "7px 14px",
      cursor: "pointer",
      boxShadow: "0px 2px 6px -5px rgba(0,0,0,0.71)",
      webkitBoxShadow: "0px 2px 6px -5px rgba(0,0,0,0.71)",
      mozBoxShadow:"0px 2px 6px -5px rgba(0,0,0,0.71)",
      marginLeft: 8,
      "&:hover":{
        background:"#EEEEEE"
      },
      "& img": {
        width: 20,
        height: 20,
      },
      "& .btn-linkedin": {
        width: "inherit",
        height: "inherit",
        border: "none",
        cursor: "inherit",
        outline: "none",
        background: "none",
        margin: "-12px",
        borderRadius: "50%",
      },
    },
    signInOptionCnt: {
      display: "flex",
      listStyleType: "none",
      justifyContent: "center",
      padding: 0,
    },
});

export default profileSetting;
