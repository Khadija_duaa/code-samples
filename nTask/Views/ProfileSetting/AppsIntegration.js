import React, { Component } from "react";
import Icons from "../../components/Icons/Icons";
import DefaultSwitch from "../../components/Form/Switch";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import { withSnackbar } from "notistack";

import { connect } from "react-redux";
import styles from "./styles";
import { SaveProfile } from "../../redux/actions/profileSettings";
import {
  UnlinkSlackInfo,
  FetchUserInfo,
  UnlinkZoomInfo,
  UnlinkTeamsInfo,
} from "../../redux/actions/profile";

import {
  connectGoogleCalender,
  syncCalendar,
  saveCalendarDetails,
  GetGoogleAccounts,
  EnableSyncCalendar,
  GoogleDeauthorization,
  GetCalendarSettings,
  clearGoogleCalendarState,
  GoogleDeauthorizationWithData,
} from "../../redux/actions/googleCalendar";
import {
  getCalendarDetails,
  generateUrl,
  CalendarDeauthorize,
  EnableDisableSyncCalendar,
} from "../../redux/actions/outlookCalendar";
import SlackIcon from "../../components/Icons/SlackIcon";
import ZoomIcon from "../../components/Icons/ZoomIcon";
import TeamsIcon from "../../components/Icons/TeamsIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import MoveWorkspaceData from "../../components/Dialog/NewWorkspaceDialog/MoveWorkspaceData";
import CustomButton from "../../components/Buttons/CustomButton";
import OutlookCalendarIcon from "../../components/Icons/OutlookCalendarIcon";
import AppleCalendarIcon from "../../components/Icons/AppleCalendarIcon";
import GoogleCalendarIcon from "../../components/Icons/GoogleCalendarIcon";
import Typography from "@material-ui/core/Typography";
import CreateableSelectDropdown from "../../components/Dropdown/SelectCreateableDropdown/SelectCreateableDropdown";
import CalendarSyncForm from "./calendarSyncForm";
import CalendarDetails from "./CalendarDetails";
import CalendarListItem from "./CalendarList/CalendarListItem";
import IntegratedCalendarList from "./IntegratedCalendarList/CalendarListItem";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import {
  generateAccountsData,
  generateCalendarsData,
} from "../../helper/generateGoogleCalendarData";
import DropdownMenu from "../../components/Dropdown/DropdownMenu";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import isEmpty from "lodash/isEmpty";
import DownArrow from "@material-ui/icons/ArrowDropDown";
import cloneDeep from "lodash/cloneDeep";
import NotificationMessage from "../../components/NotificationMessages/NotificationMessages";
import { FormattedMessage, injectIntl } from "react-intl";
import Tooltip from "@material-ui/core/Tooltip";
// import { GOOGLE_ICON } from "../..//constants/icons";
import { GOOGLE_ICON } from "../../utils/constants/icons";
import GoogleCalendarMeetIcon from "../../components/Icons/GoogleCalendar&MeetIcon";
// import GoogleLoginButton from "../../components/SocialLoginButtons/GoogleButton";
const url =
  "https://accounts.google.com/o/oauth2/v2/auth/oauthchooseaccount?access_type=offline&response_type=code&client_id=206198469770-540h8io3d8u1kuo93qu4q5ir93h4ks4a.apps.googleusercontent.com&redirect_uri=http%3A%2F%2Flocalhost%3A3001%2Fgooglecalendar&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fcalendar&flowName=GeneralOAuthFlow";

class AppIntegration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      slackChecked: false,
      zapierChecked: false,
      personalWorkspaceDialog: false,
      zoomChecked: false,
      teamsChecked: false,
      hasAccount: "google",
      outlookSyncList: true,
      selectedGoogleAccount: null,
      selectedGoogleCalendar: null,
      calendars: this.getCalendars([], false),
      accounts: [],
      openAccountsDd: false,
      btnQuery: "",
      btnQueryGoogle: "",
      btnQueryOutlook: "",
      btnQueryApple: "",

      syncGoogleAccount: false,
      googleSyncList: false,
      accountDetails: false,
      connectOutlookAcc: false,
      calendarList: true,
      connectAppleAcc: false,
      calendarListApple: true,
    };
  }

  componentDidMount() {
    this.setState({
      legacyDataChecked: this.props.profile.profile.enableLegacyData,
      slackChecked: this.props.profile.isSlackLinked,
      zoomChecked: this.props.profile.isZoomLinked,
      teamsChecked: this.props.profile.isTeamLinked,
    });
    this.getGoogleCalendarState();
    this.getAccounts();
  }

  handleSlackSwitch = () => {
    this.setState(
      {
        slackChecked: !this.state.slackChecked,
      },
      () => {
        if (this.state.slackChecked) window.location.href = this.props.profile.slackRedirectURL;
        else this.props.UnlinkSlackInfo(() => {});
      }
    );
  };

  handleZapierSwitch = () => {
    this.setState(prevState => ({
      zapierChecked: !prevState.zapierChecked,
    }));
  };

  handleZoomSwitch = () => {
    this.setState(
      prevState => {
        return {
          zoomChecked: !prevState.zoomChecked,
        };
      },
      () => {
        if (this.state.zoomChecked) window.location.href = this.props.profile.zoomRedirectURL;
        else this.props.UnlinkZoomInfo(() => {});
      }
    );
  };

  handleTeamsSwitch = () => {
    this.setState(
      prevState => {
        return {
          teamsChecked: !prevState.teamsChecked,
        };
      },
      () => {
        if (this.state.teamsChecked) window.location.href = this.props.profile.teamRedirectURL;
        else this.props.UnlinkTeamsInfo(() => {});
      }
    );
  };

  getGoogleCalendarState = () => {
    const { googleCalendar, profile } = this.props;
    this.setState({
      googleSyncList: googleCalendar.googleIntegration
        ? false
        : googleCalendar.syncCalenderList && googleCalendar.syncCalenderList.length > 0
        ? true
        : false,
      accountDetails: false,
      syncGoogleAccount: googleCalendar.googleIntegration,
    });
  };

  componentDidUpdate(prevProps, prevState) {
    if (
      JSON.stringify(this.props.googleCalendar.accountsAndCalendar) !==
      JSON.stringify(prevProps.googleCalendar.accountsAndCalendar)
    ) {
      this.setState({
        accounts: this.props.googleCalendar.accountsAndCalendar,
      });
    }
  }

  componentWillUnmount() {
    this.props.clearGoogleCalendarState(false);
  }

  getAccounts = () => {
    const { googleCalendar } = this.props;
    if (!isEmpty(googleCalendar.accountsAndCalendar)) {
      let accounts = generateAccountsData(googleCalendar.accountsAndCalendar);
      return accounts;
    } else return [];
  };

  handlePersonalWorkspaceBtn = () => {
    this.setState({
      personalWorkspaceDialog: true,
    });
  };

  clearValues = () => {
    this.setState({
      selectedGoogleAccount: null,
      selectedGoogleCalendar: null,
    });
  };

  connectGoogleHandler = () => {
    const { profile } = this.props;
    window.location.href = this.props.profile.googleCalendarURL;
  };

  connectOutlookHandler = () => {
    this.setState(
      {
        btnQueryOutlook: "progress",
      },
      () => {
        this.props.getCalendarDetails(
          /** 1 is for outlook and 2 is for Apple */
          0 /** first parameter is code */,
          1,
          succ => {
            this.setState({
              connectOutlookAcc: true,
              btnQueryOutlook: "",
              calendarList: false,
            });
          },
          fail => {
            this.setState({
              connectOutlookAcc: true,
              btnQueryOutlook: "",
            });
          }
        );
      }
    );
  };

  connectAppleHandler = () => {
    this.setState(
      {
        btnQueryApple: "progress",
      },
      () => {
        this.props.getCalendarDetails(
          /** 1 is for outlook and 2 is for Apple */
          0,
          2,
          succ => {
            this.setState({
              connectAppleAcc: true,
              btnQueryApple: "",
              calendarListApple: false,
            });
          },
          fail => {
            this.setState({
              connectAppleAcc: false,
              btnQueryApple: "",
              calendarListApple: true,
            });
          }
        );
      }
    );
  };

  syncGoogleCalendar = () => {
    const { selectedGoogleAccount, selectedGoogleCalendar } = this.state;
    let obj = {
      CalendarId: selectedGoogleCalendar.id,
      CalendarTitle: selectedGoogleCalendar.value,
      AccountId: selectedGoogleAccount.id,
    };
    this.setState(
      {
        btnQuery: "progress",
      },
      () => {
        this.props.syncCalendar(
          obj,
          succ => {
            this.setState({
              googleSyncList: false,
              syncGoogleAccount: false,
              accountDetails: true,
              btnQuery: "",
            });
          },
          fail => {}
        );
      }
    );

    // this.setState((preState) => {
    //   return {
    //     syncGoogleAccount: !preState.syncGoogleAccount,
    //   };
    // });
  };

  syncWithGoogle = () => {
    this.setState({ googleSyncList: true, syncGoogleAccount: false });
  };

  syncWithOutlook = () => {
    this.setState({ outlookSyncList: true, syncOutlookAccount: false });
  };

  syncWithApple = () => {
    // this.setState({googleSyncList: true, syncGoogleAccount: false})
  };

  updateGoogleDetailsHandler = item => {
    this.props.GetCalendarSettings(
      item.accountId,
      succ => {
        this.setState({
          googleSyncList: false,
          syncGoogleAccount: false,
          accountDetails: true,
        });
      },
      fail => {}
    );
  };
  updateDetailsHandler = item => {
    this.props.getCalendarDetails(
      /** 1 is for outlook and 2 is for Apple */
      item.code,
      item.mailServerType,
      succ => {
        this.setState({
          connectOutlookAcc: true,
          calendarList: false,
        });
      },
      fail => {
        this.setState({
          connectOutlookAcc: true,
          calendarList: true,
        });
      }
    );
  };
  updateDetailsHandleraApple = item => {
    this.props.getCalendarDetails(
      /** 1 is for outlook and 2 is for Apple */
      item.code,
      item.mailServerType,
      succ => {
        this.setState({
          connectAppleAcc: true,
          calendarListApple: false,
        });
      },
      fail => {
        this.setState({
          connectAppleAcc: true,
          calendarListApple: true,
        });
      }
    );
  };

  handleMoveWorkspaceDialogClose = () => {
    this.setState({ personalWorkspaceDialog: false });
  };

  getCalendars = (calendarsArr, stateCall = false) => {
    if (calendarsArr && !isEmpty(calendarsArr)) {
      let calendars = generateCalendarsData(calendarsArr);
      if (stateCall) {
        this.setState({
          calendars: calendars,
        });
      } else return calendars;
    } else return [];
  };

  selectAccount = (key, value) => {
    this.setState(
      {
        [key]: value,
        selectedGoogleCalendar: {},
      },
      () => {
        this.getCalendars(value.obj.gCalendar, true);
      }
    );
  };

  selectCalendar = (key, value) => {
    this.setState({
      [key]: value,
    });
  };

  handleAccountClick = (event, placement) => {
    this.setState({
      openAccountsDd: true,
    });
  };

  handleAccountClose = () => {
    this.setState({
      openAccountsDd: false,
    });
  };

  setDefaultState = () => {
    const { googleCalendar } = this.props;

    this.setState({
      googleSyncList:
        googleCalendar.syncCalenderList && googleCalendar.syncCalenderList.length > 0
          ? true
          : false,
      syncGoogleAccount:
        googleCalendar.syncCalenderList && googleCalendar.syncCalenderList.length > 0
          ? false
          : true,
      accountDetails: false,
      selectedGoogleCalendar: {},
      selectedGoogleAccount: {},
    });
  };

  AddCalendarSettings = (obj, callback) => {
    this.props.saveCalendarDetails(
      obj,
      succ => {
        this.setState({
          googleSyncList: true,
          accountDetails: false,
          syncGoogleAccount: false,
        });
        callback();
      },
      fail => {
        if (fail.response == undefined) {
          this.props.FetchUserInfo(callback => {});
          this.showSnackBar("Success! Your sync is in progress", "success");
          this.props.onDialogueClose();
        } else if (fail.response.status == 504) {
          this.props.FetchUserInfo(callback => {});
          this.showSnackBar("Success! Your sync is in progress", "success");
          this.props.onDialogueClose();
        } else {
          this.showSnackBar("Oops! Saving calendar settings failed", "error");
          callback();
        }
      }
    );
  };

  stopSync = (item, succCallback) => {
    let obj = cloneDeep(item);
    obj.syncEnabled = !obj.syncEnabled;
    this.props.EnableSyncCalendar(
      obj,
      succ => {
        succCallback();
      },
      fail => {}
    );
  };

  enableDisableSync = (item, succCallback) => {
    let obj = cloneDeep(item);
    obj.syncEnabled = !obj.syncEnabled;
    this.props.EnableDisableSyncCalendar(
      obj,
      succ => {
        succCallback();
      },
      fail => {}
    );
  };

  deleteCalendar = (item, succCallback, fail) => {
    const { googleCalendar } = this.props;
    this.props.GoogleDeauthorization(
      item,
      succ => {
        this.setState({
          googleSyncList:
            googleCalendar.syncCalenderList && googleCalendar.syncCalenderList.length > 1
              ? true
              : false,
          accountDetails: false,
          syncGoogleAccount: false,
        });
        succCallback();
      },
      fail => {
        this.showSnackBar("Oops! Something went wrong", "error");
        fail();
      }
    );
  };

  deleteCalendarWithData = (item, succCallback, fail) => {
    const { googleCalendar } = this.props;
    this.props.GoogleDeauthorizationWithData(
      item,
      succ => {
        this.setState({
          googleSyncList:
            googleCalendar.syncCalenderList && googleCalendar.syncCalenderList.length > 1
              ? true
              : false,
          accountDetails: false,
          syncGoogleAccount: false,
        });
        succCallback();
      },
      fail => {
        this.showSnackBar("Oops! Something went wrong", "error");
        fail();
      }
    );
  };

  deleteOutlookCalendar = (item, succCallback, fail) => {
    const { googleCalendar } = this.props;
    this.props.CalendarDeauthorize(
      item,
      succ => {
        succCallback();
      },
      fail => {
        this.showSnackBar("Oops! Something went wrong", "error");
        fail();
      }
    );
  };

  deleteAppleCalendar = (item, succCallback, fail) => {
    this.props.CalendarDeauthorize(
      item,
      succ => {
        succCallback();
      },
      fail => {
        this.showSnackBar("Oops! Something went wrong", "error");
        fail();
      }
    );
  };

  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };

  addOutlookDetails = (data, callBack) => {
    this.props.generateUrl(
      data,
      succ => {
        this.setState({
          connectOutlookAcc: false,
          calendarList: true,
        });
        callBack();
      },
      fail => {
        callBack();
      }
    );
  };

  addAppleDetails = (data, callBack) => {
    this.props.generateUrl(
      data,
      succ => {
        this.setState({
          connectAppleAcc: false,
          calendarListApple: true,
        });
        callBack();
      },
      fail => {
        callBack();
      }
    );
  };

  setClearState = () => {
    const {} = this.state;
    const { outlookCalendar } = this.props;
    this.setState({
      connectOutlookAcc: false,
      calendarList: true,
    });
  };
  setClearStateApple = () => {
    this.setState({
      connectAppleAcc: false,
      calendarListApple: true,
    });
  };

  render() {
    const {
      classes,
      profile,
      googleCalendar,
      theme = {},
      outlookCalendar,
      appleCalendar,
    } = this.props;
    const {
      slackChecked,
      zapierChecked,
      personalWorkspaceDialog,
      zoomChecked,
      teamsChecked,
      hasAccount,
      syncGoogleAccount,
      googleSyncList,
      outlookSyncList,
      selectedGoogleAccount,
      calendars,
      selectedGoogleCalendar,
      accounts,
      openAccountsDd,
      accountDetails,
      btnQuery,
      btnQueryGoogle,
      connectOutlookAcc,
      btnQueryOutlook,
      calendarList,
      btnQueryApple,
      connectAppleAcc,
      calendarListApple,
    } = this.state;
    return (
      <>
        <MoveWorkspaceData
          closeAction={this.handleMoveWorkspaceDialogClose}
          open={personalWorkspaceDialog}
        />
        <ul className={classes.appIntegrationList}>
          <li>
            <div className={classes.iconNameCnt}>
              <SvgIcon viewBox="70 70 160 160" className={classes.slackIcon}>
                <SlackIcon />
              </SvgIcon>
              {/* <p><FormattedMessage id="profile-settings-dialog.apps-integrations.slack.label"   defaultMessage="Slack" /></p> */}
              <p>Slack</p>
            </div>
            <DefaultSwitch size={"medium"} checked={slackChecked} onChange={this.handleSlackSwitch} />
          </li>
          <li>
            <div className={classes.iconNameCnt}>
              <SvgIcon viewBox="0 0 2228.833 2073.333" className={classes.teamsIcon}>
                <TeamsIcon />
              </SvgIcon>
              <p>Microsoft Teams</p>
            </div>
            <Tooltip
              classes={{
                tooltip: classes.tooltip,
              }}
              title={
                <div style={{ width: "100%" }}>
                  <div style={{ whiteSpace: "wrap" }}>
                    <strong>nTask Integration with Microsoft Teams!</strong>
                  </div>
                  <span style={{ display: "flex", marginTop: 5, marginBottom: 5 }}>
                    Collaborate on nTask and stay connected with Microsoft Teams.
                  </span>
                  <div style={{ whiteSpace: "wrap" }}>
                    <strong>Some limitations by Microsoft Teams may apply:</strong>
                  </div>
                  <span style={{ display: "flex", marginTop: 5, marginBottom: 5 }}>
                    If a Microsoft Teams user without a Teams for Businesses account sets a meeting
                    in nTask, then the user’s account is considered Skype for Consumers account. Due
                    to which the link generated in nTask meetings might be skype or outlook url.
                  </span>
                </div>
              }
              placement="bottom">
              <DefaultSwitch size={"medium"} checked={teamsChecked} onChange={this.handleTeamsSwitch} />
            </Tooltip>
          </li>
          <li>
            <div className={classes.iconNameCnt}>
              <img src={Icons.ZapierIcon} width={20} height={20} />
              {/* <p><FormattedMessage id="profile-settings-dialog.apps-integrations.zapier.label"   defaultMessage="Zapier" /></p> */}
              <p>Zapier</p>
            </div>

            <Tooltip
              classes={{
                tooltip: classes.tooltip,
              }}
              title={
                <div style={{ width: "100%" }}>
                  <div style={{ whiteSpace: "wrap" }}>
                    <strong>Everything on one platform with Zapier and nTask!</strong>
                  </div>
                  <span style={{ display: "flex", marginTop: 5 }}>
                    WFH is a reality for all of us. We are trying to make this as easy for you as
                    possible. We are going to help you integrate all the apps you need on one
                    platform, keeping you and your teams super organized and efficient.
                    <br />
                    To enable us to do this, we are growing our community of integration partners.
                    At the moment we are in review with Zapier. We need your cooperation and
                    patience as due to the pandemic Zapier is overwhelmed with requests.
                    <br />
                    We will update you on a regular basis and send you a notification the moment it
                    is active
                  </span>
                </div>
              }
              placement="bottom">
              <DefaultSwitch size={"medium"} checked={zapierChecked} onChange={this.handleZapierSwitch} />
            </Tooltip>
          </li>
          <li>
            <div className={classes.iconNameCnt}>
              <SvgIcon viewBox="0 0 22 22" className={classes.zoomIcon}>
                <ZoomIcon />
              </SvgIcon>
              <p>
                {/* <FormattedMessage
                  id="profile-settings-dialog.apps-integrations.zoom.label"
                  defaultMessage="Zoom"
                /> */}
                Zoom
              </p>{" "}
            </div>
            <DefaultSwitch size={"medium"} checked={zoomChecked} onChange={this.handleZoomSwitch} />
          </li>
          {/* Googel Calendar  */}
          <li>
            <div className={classes.iconNameCnt}>
              <SvgIcon viewBox="0 0 70.67 22" className={classes.googleMeetIcon}>
                <GoogleCalendarMeetIcon />
              </SvgIcon>
              <p>
                {/* <FormattedMessage
                  id="profile-settings-dialog.apps-integrations.google-calender.label"
                  defaultMessage="Google Calendar"
                /> */}
                Google Calendar & Google Meet
              </p>
            </div>
            <div>
              {/* <Typography variant="body2" className={classes.learnMore}>
                Learn more
              </Typography>
              &nbsp; */}
              {/* <Tooltip
              classes={{
                tooltip: classes.tooltip
              }}
              title={
                <div style={{width: '100%'}}>
                  <div style={{whiteSpace: 'wrap'}}><strong>One calendar to rule them all!!</strong></div>
                  <span style={{display: 'flex', marginTop: 5}}>We are going to help you manage your schedule the right way – with just one calendar. You will soon be able to merge your nTask Calender with Google to help you see your teams and your schedule all in one calender.<br/>
                    We are currently in review with our integration partner, Google. We need your cooperation and patience, as due to the pandemic Google has more requests to process.</span>
                    We will update you on a regular basis and send you a notification the moment this is active
                </div>}
              placement="bottom"
            > */}
              {/* <CustomButton
                onClick={this.connectGoogleHandler}
                variant="white"
                disabled={false}
                btnType={"blue"}
                query={btnQueryGoogle}
              >
                <FormattedMessage
                  id="profile-settings-dialog.apps-integrations.common.connect-button.label"
                  defaultMessage="Connect"
                />
              </CustomButton> */}

              <div className={classes.signInOption} onClick={this.connectGoogleHandler}>
                <div className={classes.socialIconcontainer}>
                  <img className={classes.googleIcon} src={GOOGLE_ICON} />
                  <span className={classes.socialTitle}> Connect with Google </span>
                </div>
              </div>

              {/* </Tooltip>               */}
            </div>
          </li>
          {googleSyncList || syncGoogleAccount || accountDetails ? (
            <>
              <div className={classes.calendarWrapper}>
                {googleSyncList && (
                  <>
                    {googleCalendar.syncCalenderList.map(item => (
                      <>
                        <CalendarListItem
                          updateSync={item => {
                            this.updateGoogleDetailsHandler(item);
                          }}
                          urlText={null}
                          item={item}
                          stopSync={this.stopSync}
                          deleteCalendar={this.deleteCalendar}
                          deleteCalendarWithData={this.deleteCalendarWithData}
                        />
                      </>
                    ))}
                  </>
                )}
                {syncGoogleAccount && (
                  <>
                    <Typography variant="h5" className={classes.dropDownTitle}>
                      <FormattedMessage
                        id="profile-settings-dialog.apps-integrations.google-calender.connect.label"
                        defaultMessage="Connect to Google Calendar"
                      />
                    </Typography>

                    <CreateableSelectDropdown
                      data={() => {
                        return this.getAccounts();
                      }}
                      label={
                        <span className={classes.label}>
                          <FormattedMessage
                            id="profile-settings-dialog.apps-integrations.google-calender.connect.account.label"
                            defaultMessage="Account"
                          />
                        </span>
                      }
                      placeholder={
                        <FormattedMessage
                          id="profile-settings-dialog.apps-integrations.google-calender.connect.account.placeholder"
                          defaultMessage="Select google account"
                        />
                      }
                      id="selectedGoogleAccount"
                      name="selectedGoogleAccount"
                      selectOptionAction={(key, value) => {
                        this.selectAccount("selectedGoogleAccount", value);
                      }}
                      createOptionAction={null}
                      disableIndicator={true}
                      type="task"
                      createText=""
                      isMulti={false}
                      selectedValue={selectedGoogleAccount}
                      createLabelValidation={() => {
                        return true;
                      }}
                      clearOptionAfterSelect={true}
                      isDisabled={false}
                      noOptionsMessage={
                        <FormattedMessage
                          id="profile-settings-dialog.apps-integrations.google-calender.connect.account.error"
                          defaultMessage="No google accounts found."
                        />
                      }
                      isSearchable={false}
                    />

                    <CreateableSelectDropdown
                      data={calendars}
                      label={
                        <span className={classes.label}>
                          <FormattedMessage
                            id="profile-settings-dialog.apps-integrations.google-calender.connect.calendar.label"
                            defaultMessage="Calendar"
                          />
                        </span>
                      }
                      placeholder={
                        <FormattedMessage
                          id="profile-settings-dialog.apps-integrations.google-calender.connect.calendar.placeholder"
                          defaultMessage="Select a calendar"
                        />
                      }
                      id="selectedGoogleCalendar"
                      name="selectedGoogleCalendar"
                      selectOptionAction={(key, value) => {
                        this.selectCalendar("selectedGoogleCalendar", value);
                      }}
                      createOptionAction={null}
                      disableIndicator={true}
                      type="task"
                      createText=""
                      isMulti={false}
                      selectedValue={selectedGoogleCalendar}
                      createLabelValidation={() => {
                        return true;
                      }}
                      clearOptionAfterSelect={true}
                      isDisabled={false}
                      noOptionsMessage={
                        <FormattedMessage
                          id="profile-settings-dialog.apps-integrations.google-calender.connect.calendar.error"
                          defaultMessage="No calendars found."
                        />
                      }
                      acceptDataInFormOfFun={false}
                      isSearchable={false}
                    />

                    <div className={classes.calendarDropDownBtns}>
                      <CustomButton
                        onClick={() => {
                          this.clearValues();
                        }}
                        variant="contained"
                        disabled={false}
                        btnType={"gray"}
                        >
                        <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
                      </CustomButton>
                      &nbsp;
                      <CustomButton
                        onClick={this.syncGoogleCalendar}
                        variant="contained"
                        // disabled={false}
                        btnType={"blue"}
                        query={btnQuery}
                        disabled={
                          isEmpty(selectedGoogleCalendar) && isEmpty(selectedGoogleCalendar) || btnQuery
                        }>
                        <FormattedMessage
                          id="profile-settings-dialog.apps-integrations.google-calender.connect.sync-button.label"
                          defaultMessage="Sync Calendar"
                        />
                      </CustomButton>
                    </div>
                  </>
                )}{" "}
                {accountDetails && (
                  <>
                    <div className={classes.googleCalendarTitles}>
                      <Typography variant="body2" className={classes.accountSubTitle}>
                        {" "}
                        {`Calendar: ${googleCalendar.syncCalender.calendarTitle}`}
                      </Typography>
                      <Typography variant="h5" className={classes.titleText}>
                        {" "}
                        {googleCalendar.syncCalender.accountId || ""}
                      </Typography>
                    </div>

                    <CalendarSyncForm
                      btnText="Save"
                      syncWithCalendar={() => this.syncWithGoogle()}
                      data={googleCalendar}
                      setDefaultParentState={this.setDefaultState}
                      AddCalendarSettings={this.AddCalendarSettings}
                      intl={this.props.intl}
                    />
                  </>
                )}
              </div>
            </>
          ) : null}

          <li>
            <div className={classes.iconNameCnt}>
              <SvgIcon viewBox="0 0 22 22.243" className={classes.outlookIcon}>
                <OutlookCalendarIcon />
              </SvgIcon>
              <p>
                {/* <FormattedMessage
                  id="profile-settings-dialog.apps-integrations.outlook.label"
                  defaultMessage="Outlook"
                /> */}
                Outlook
              </p>
            </div>
            {/* <Typography variant="body2" className={classes.learnMore}>
                Learn more
              </Typography>
              &nbsp; */}
            <CustomButton
              onClick={this.connectOutlookHandler}
              variant="contained"
              disabled={false}
              btnType={"blue"}
              query={btnQueryOutlook}>
              <FormattedMessage
                id="profile-settings-dialog.apps-integrations.common.connect-button.label"
                defaultMessage="Connect"
              />
            </CustomButton>
          </li>
          {connectOutlookAcc ? (
            <>
              <div className={classes.calendarWrapper}>
                <CalendarDetails
                  btnText={
                    outlookCalendar.syncOtherCalendar ? (
                      <FormattedMessage
                        id="profile-settings-dialog.apps-integrations.common.save-button.label"
                        defaultMessage="Save"
                      />
                    ) : (
                      <FormattedMessage
                        id="profile-settings-dialog.apps-integrations.common.generate-url-buton.label"
                        defaultMessage="Generate URL"
                      />
                    )
                  }
                  syncWithCalendar={null}
                  mailServerType={1}
                  data={outlookCalendar}
                  setDefaultParentState={this.setClearState}
                  AddCalendarSettings={this.addOutlookDetails}
                  intl={this.props.intl}
                />
              </div>
            </>
          ) : null}
          {!isEmpty(outlookCalendar.syncedCalendar) && calendarList && (
            <div className={classes.calendarWrapper}>
              {outlookCalendar.syncedCalendar.map(s => {
                return (
                  <IntegratedCalendarList
                    updateSync={data => {
                      this.updateDetailsHandler(data);
                    }}
                    data={s}
                    stopSync={this.enableDisableSync}
                    deleteCalendar={this.deleteOutlookCalendar}
                  />
                );
              })}
              <NotificationMessage
                type="NewInfo"
                iconType="info"
                style={{
                  width: "100%",
                  marginTop: 19,
                  textAlign: "start",
                  fontSize: "11px",
                  padding: "6px",
                  backgroundColor: "#EAEAEA",
                  borderRadius: 4,
                  fontFamily: theme.typography.fontFamilyLato,
                  fontWeight: 400,
                }}>
                <FormattedMessage
                  id="profile-settings-dialog.apps-integrations.outlook.copy-calender.label"
                  defaultMessage="Copy calendar subscription URL and paste it in your Outlook's
                calendar URL field to sync nTask's calendar with Outlook's
                calendar."
                />
              </NotificationMessage>
            </div>
          )}

          <li>
            <div className={classes.iconNameCnt}>
              <SvgIcon viewBox="0 0 26 25.999" className={classes.appleIcon}>
                <AppleCalendarIcon />
              </SvgIcon>
              <p>
                {/* <FormattedMessage
                  id="profile-settings-dialog.apps-integrations.apple-calendar.label"
                  defaultMessage="Apple Calendar"
                /> */}
                Apple Calendar
              </p>
            </div>
            {/* <Typography variant="body2" className={classes.learnMore}>
                Learn more
              </Typography> */}
            <CustomButton
              onClick={this.connectAppleHandler}
              variant="contained"
              disabled={false}
              btnType={"blue"}
              query={btnQueryApple}>
              <FormattedMessage
                id="profile-settings-dialog.apps-integrations.common.connect-button.label"
                defaultMessage="Connect"
              />
            </CustomButton>
          </li>
          {connectAppleAcc ? (
            <>
              <div className={classes.calendarWrapper}>
                <CalendarDetails
                  btnText={
                    appleCalendar.syncOtherCalendar ? (
                      <FormattedMessage
                        id="profile-settings-dialog.apps-integrations.common.save-button.label"
                        defaultMessage="Save"
                      />
                    ) : (
                      <FormattedMessage
                        id="profile-settings-dialog.apps-integrations.common.generate-url-buton.label"
                        defaultMessage="Generate URL"
                      />
                    )
                  }
                  mailServerType={2}
                  syncWithCalendar={null}
                  data={appleCalendar}
                  setDefaultParentState={this.setClearStateApple}
                  AddCalendarSettings={this.addAppleDetails}
                  intl={this.props.intl}
                />
              </div>
            </>
          ) : null}
          {!isEmpty(appleCalendar.syncedCalendar) && calendarListApple && (
            <div className={classes.calendarWrapper}>
              {appleCalendar.syncedCalendar.map(a => {
                return (
                  <IntegratedCalendarList
                    updateSync={data => {
                      this.updateDetailsHandleraApple(data);
                    }}
                    data={a}
                    stopSync={this.enableDisableSync}
                    deleteCalendar={this.deleteAppleCalendar}
                  />
                );
              })}
              <NotificationMessage
                type="NewInfo"
                iconType="info"
                style={{
                  width: "100%",
                  marginTop: 19,
                  textAlign: "start",
                  fontSize: "11px",
                  padding: "6px",
                  backgroundColor: "#EAEAEA",
                  borderRadius: 4,
                  fontFamily: theme.typography.fontFamilyLato,
                  fontWeight: 400,
                }}>
                <FormattedMessage
                  id="profile-settings-dialog.apps-integrations.apple-calendar.copy-calender.label"
                  defaultMessage="Copy calendar subscription URL and paste it in your Apple's
                calendar URL field to sync nTask's calendar with Apple's
                calendar."
                />
              </NotificationMessage>
            </div>
          )}

          {profile.hasPersonalData && (
            <li style={{ margin: 0 }}>
              <div>
                <p>
                  <FormattedMessage
                    id="profile-settings-dialog.apps-integrations.personal-data.label"
                    defaultMessage="Personal Data"
                  />
                </p>
              </div>
              <CustomButton
                onClick={this.handlePersonalWorkspaceBtn}
                btnType="success"
                variant="contained"
                style={{ marginRight: 10 }}>
                <FormattedMessage
                  id="profile-settings-dialog.apps-integrations.personal-data.migrate-button.label"
                  defaultMessage="Migrate Data"
                />
              </CustomButton>
            </li>
          )}
        </ul>
      </>
    );
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    profile: state.profile.data,
    googleCalendar: state.googleCalendar.data,
    outlookCalendar: state.outlookCalendar.data,
    appleCalendar: state.appleCalendar.data,
  };
};
export default compose(
  withStyles(styles, {
    withTheme: true,
  }),
  injectIntl,
  withSnackbar,
  connect(mapStateToProps, {
    SaveProfile,
    UnlinkSlackInfo,
    UnlinkZoomInfo,
    UnlinkTeamsInfo,
    FetchUserInfo,
    connectGoogleCalender,
    syncCalendar,
    saveCalendarDetails,
    GetGoogleAccounts,
    EnableSyncCalendar,
    GoogleDeauthorization,
    GetCalendarSettings,
    clearGoogleCalendarState,
    getCalendarDetails,
    generateUrl,
    CalendarDeauthorize,
    EnableDisableSyncCalendar,
    GoogleDeauthorizationWithData,
  })
)(AppIntegration);
