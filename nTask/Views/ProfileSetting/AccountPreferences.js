import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Select from "react-select";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import DefaultButton from "../../components/Buttons/DefaultButton";
import CustomButton from "../../components/Buttons/CustomButton";
import Grid from "@material-ui/core/Grid";
import DropdownArrow from "@material-ui/icons/ArrowDropDown";
import { components } from "react-select";
import DefaultSwitch from "../../components/Form/Switch";
import NotificationIcon from "@material-ui/icons/Notifications";
import LocationIcon from "@material-ui/icons/LocationOn";
import PersonalizationIcon from "@material-ui/icons/ColorLens";
import merge from "lodash/merge";
import { setSideBarTheme } from "../../redux/actions/sidebar";
import moment from "moment";
import { SaveLocalization, UpdateUserPreferences } from "../../redux/actions/profileSettings";
import { UpdateLocalization } from "../../redux/actions/profile";
import { getModifiedState } from "../../utils/common";
import { convertDayToWeekday, convertWeekdayToDay } from "../../utils/convertTime";
import { profileSettingsHelpText } from "../../components/Tooltip/helptext";
import CustomTooltip from "../../components/Tooltip/Tooltip";
import { FormattedMessage, injectIntl } from "react-intl";
import InfoIcon from "@material-ui/icons/Info";
import Typography from "@material-ui/core/Typography";

class AccountPreferences extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profileModified: false,
      language: "",
      firstWeekDay: "",
      timezone: "",
      userPreference: [],
      isDisabled: false,
      disableId: "",
      darkModeChecked: false,
      fieldsChanged: {},
      btnQuery: "",
      dls: false,
      oldSidebarView: false,
    };
    this.handleLanguageSelect = this.handleLanguageSelect.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleFirstWeekDaySelect = this.handleFirstWeekDaySelect.bind(this);
    this.handleTimeZoneChange = this.handleTimeZoneChange.bind(this);
    this.handleNotificationChange = this.handleNotificationChange.bind(this);
    this.handleLocalization = this.handleLocalization.bind(this);
    this.changeModificationStatus = this.changeModificationStatus.bind(this);
  }

  componentDidMount = () => {
    const TimeZoneLabel =
      (this.props.constantsState.data &&
        this.props.constantsState.data.timeZones.find(obj => {
          return obj.value === this.props.profileState.data.profile.timeZone;
        })) ||
      {};
    this.setState({
      darkModeChecked: localStorage.getItem("sidebarMode") == "dark" ? true : false, // For Theme Dark Mode
      oldSidebarView: localStorage.getItem("oldSidebarView") == "true" ? true : false, // For oldSidebarView Mode
      firstWeekDay: {
        value: convertDayToWeekday(this.props.profileState.data.profile.startDayOfWeek),
        label: this.translate(
          convertDayToWeekday(this.props.profileState.data.profile.startDayOfWeek)
        ),
      },
      timezone: {
        value: this.props.profileState.data.profile.timeZone,
        label: TimeZoneLabel.label,
        obj: TimeZoneLabel,
      },
      dls: this.props.profileState.data.profile.isDayLightSavingEnable,
      // language: {
      //   value: this.props.profileState.data.profile.language,
      //   label: this.props.profileState.data.profile.language,
      // },
      language:
        this.props.constantsState.data.languages.find(
          l => l.value == this.props.profileState.data.profile.language
        ) || {},
      userPreference: this.props.userPreferenceState.data.preferences,
    });
  };

  componentDidUpdate(prevProps, prevState) {
    const { startDayOfWeek } = this.props.profileState.data.profile;
    if (startDayOfWeek !== prevProps.profileState.data.profile.startDayOfWeek) {
      moment.updateLocale("en", {
        week: {
          dow: this.props.profileState.data.profile.startDayOfWeek,
        },
      });
    }
  }
  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.props.onDialogueClose();
  };
  handleClick = () => {
    // this.setState({ open: true });
  };
  handleLanguageSelect(value) {
    this.setState(
      merge(
        { language: value },
        this.changeModificationStatus(
          "language",
          this.props.profileState.data.profile["language"],
          value.value
        )
      )
    );
  }
  handleFirstWeekDaySelect(value) {
    this.setState(
      merge(
        { firstWeekDay: value },
        this.changeModificationStatus(
          "startDayOfWeek",
          convertDayToWeekday(this.props.profileState.data.profile["startDayOfWeek"]),
          value.value
        )
      )
    );
  }
  handleTimeZoneChange(value) {
    this.setState(
      merge(
        { timezone: value },
        this.changeModificationStatus(
          "timeZone",
          this.props.profileState.data.profile["timeZone"],
          value.value
        )
      )
    );
  }
  handleNotificationChange(state, event) {
    this.setState({ [state]: event.target.checked });
  }
  handleDarkMode = () => {
    this.setState(
      prevState => ({ darkModeChecked: !prevState.darkModeChecked }),
      () => {
        localStorage.setItem("sidebarMode", this.state.darkModeChecked ? "dark" : "light");
        this.props.setSideBarTheme(this.state.darkModeChecked ? "dark" : "light");
      }
    );
  };
  oldSidebarView = () => {
    this.setState(
      prevState => ({ oldSidebarView: !prevState.oldSidebarView }),
      () => {
        localStorage.setItem("oldSidebarView", this.state.oldSidebarView);
      }
    );
  };
  handleLocalization() {
    let self = this;
    let localizationObj = {
      language: self.state.language.value,
      timeZone: self.state.timezone.value,
      startDayOfWeek: convertWeekdayToDay(self.state.firstWeekDay.value),
      IsDayLightSavingEnable: this.state.dls,
    };
    this.setState({ btnQuery: "progress" });
    self.props.SaveLocalization(localizationObj, response => {
      if (response.status === 200) {
        self.setState({ profileModified: false, btnQuery: "" });
        // self.props.UpdateLocalization(localizationObj);
        // self.props.showSnackBar("Localization Saved", true, "success");
        location.reload();
      } else {
        self.setState({ btnQuery: "" });
        self.props.showSnackBar("Unable to save Localization.", false, "error");
      }
    });
  }
  changeModificationStatus = (fieldname, propsValue, value) => {
    return getModifiedState(this.state.fieldsChanged, fieldname, propsValue, value);
  };
  handleDlsChange = () => {
    this.setState({ dls: !this.state.dls, profileModified: true });
  };
  translate = value => {
    let id = "";
    switch (value) {
      case "Monday":
        id = "profile-settings-dialog.account-preferences.localization.day-of-week.list.monday";
        break;
      case "Tuesday":
        id = "profile-settings-dialog.account-preferences.localization.day-of-week.list.tuesday";
        break;
      case "Wednesday":
        id = "profile-settings-dialog.account-preferences.localization.day-of-week.list.wednesday";
        break;
      case "Thursday":
        id = "profile-settings-dialog.account-preferences.localization.day-of-week.list.thursday";
        break;
      case "Friday":
        id = "profile-settings-dialog.account-preferences.localization.day-of-week.list.friday";
        break;
      case "Saturday":
        id = "profile-settings-dialog.account-preferences.localization.day-of-week.list.saturday";
        break;
      case "Sunday":
        id = "profile-settings-dialog.account-preferences.localization.day-of-week.list.sunday";
        break;
    }
    return id == "" ? value : this.props.intl.formatMessage({ id: id, defaultMessage: value });
  };
  render() {
    const customStyles = {
      option: base => ({
        ...base,
        fontSize: "16px",
      }),
      control: (base, state) => ({
        ...base,
        background: "transparent",
        padding: "2px 10px 2px 15px",
        borderRadius: 4,
        borderColor: theme.palette.border.lightBorder,
        boxShadow: "none",
        ":focus": {
          border: `1px solid ${theme.palette.border.darkBorder}`,
        },
        ":hover": {
          border: `1px solid ${theme.palette.border.darkBorder}`,
        },
      }),
      valueContainer: (base, state) => ({
        ...base,
        padding: "5px 0 5px 0px",
        fontSize: "12px",
        color: theme.palette.text.light,
      }),
    };
    const DropdownIndicator = props => {
      return (
        components.DropdownIndicator && (
          <components.DropdownIndicator {...props}>
            <DropdownArrow htmlColor={theme.palette.secondary.light} />
          </components.DropdownIndicator>
        )
      );
    };
    const CustomOptionComponent = ({ innerRef, innerProps, children }) => {
      return (
        <div
          ref={innerRef}
          {...innerProps}
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            // height: 100,
            padding: "6px 10px",
            cursor: "pointer",
          }}>
          <Typography variant="h6" style={{}} className={classes.optionText}>
            {children}
          </Typography>
          {children !== "English - United Kingdom" && children !== "English" ? (
            <span className={classes.betaIconDropDown}>Beta</span>
          ) : null}
        </div>
      );
    };
    const {
      language,
      firstWeekDay,
      timezone,
      activitiesChecked,
      desktopChecked,
      darkModeChecked,
      applicationChecked,
      disableId,
      isDisabled,
      profileModified,
      btnQuery,
      dls,
    } = this.state;
    const { classes, theme, profileState } = this.props;
    const userCreatedDate = moment(profileState?.data?.createdDate)
    const isOldUser = userCreatedDate.isBefore('2022-03-03T11:26:29.219Z');
    return (
      <>
        <div className={classes.accountPrefFormCnt}>
          {/* Localization----------------- */}
          <div className={classes.accountPrefHeading}>
            <div className={classes.accountPrefHeadingInner}>
              <LocationIcon
                classes={{ root: classes.accountPrefHeadingIcon }}
                htmlColor={theme.palette.secondary.light}
                fontSize="small"
              />
              <FormattedMessage
                id="profile-settings-dialog.account-preferences.localization.title"
                defaultMessage="Localization"
              />
            </div>
            <div>
              <CustomTooltip
                helptext={
                  <FormattedMessage
                    id="profile-settings-dialog.account-preferences.localization.hint"
                    defaultMessage={profileSettingsHelpText.localizationHelpText}
                  />
                }
                iconType="help"
                position="static"
              />
            </div>
          </div>
          <FormControl fullWidth={true} classes={{ root: classes.selectFormControl }}>
            <InputLabel
              htmlFor="language"
              classes={{
                root: classes.selectLabelLanguage,
              }}
              shrink={false}>
              <FormattedMessage
                id="profile-settings-dialog.account-preferences.localization.form.language.label"
                defaultMessage="Language"
              />
              <span className={classes.betaIcon}>
                {" "}
                Beta
                <CustomTooltip
                  helptext={
                    "A beta version allows us to share our excitement with you and makes you part of the process, truly becoming an app that works for you and with you. We would love for you to use the new language options that are now available for you to further personalize nTask as your go to Task Manager. Please share your feedback and concerns with our support team!"
                  }
                  iconType="help"
                  placement="top"
                  style={{ color: theme.palette.common.white }}>
                  <InfoIcon className={classes.infoIcon} />
                </CustomTooltip>
              </span>
            </InputLabel>
            <Select
              styles={customStyles}
              onChange={this.handleLanguageSelect}
              inputId="language"
              options={this.props.constantsState.data.languages}
              value={language}
              components={{
                DropdownIndicator,
                IndicatorSeparator: false,
                Option: CustomOptionComponent,
              }}
            />
          </FormControl>

          <FormControl fullWidth={true} classes={{ root: classes.selectFormControl }}>
            <InputLabel
              htmlFor="firstWeekDay"
              classes={{
                root: classes.selectLabel,
              }}
              shrink={false}>
              <FormattedMessage
                id="profile-settings-dialog.account-preferences.localization.form.first-day-of-week.label"
                defaultMessage="First day of the week"
              />
            </InputLabel>
            <Select
              styles={customStyles}
              onChange={this.handleFirstWeekDaySelect}
              inputId="firstWeekDay"
              options={this.props.constantsState.data.dayOfWeek.map(p => {
                return { value: p.value, label: this.translate(p.label) };
              })}
              value={firstWeekDay}
              components={{ DropdownIndicator, IndicatorSeparator: false }}
            />
          </FormControl>

          <FormControl
            fullWidth={true}
            classes={{ root: classes.selectFormControl }}
            style={{ marginBottom: 20 }}>
            <InputLabel
              htmlFor="timeZone"
              classes={{
                root: classes.selectLabel,
              }}
              shrink={false}>
              <FormattedMessage
                id="profile-settings-dialog.account-preferences.localization.form.time-zone.label"
                defaultMessage="Time Zone"
              />
            </InputLabel>
            <Select
              styles={customStyles}
              onChange={this.handleTimeZoneChange}
              inputId="timeZone"
              options={this.props.constantsState.data.timeZones}
              components={{ DropdownIndicator, IndicatorSeparator: false }}
              value={timezone}
            />
          </FormControl>
          <div className={classes.dayLightSaving}>
            <p style={{ display: "flex" }}>
              <FormattedMessage
                id="profile-settings-dialog.account-preferences.localization.form.daylight-saving.label"
                defaultMessage="Adjust for daylight saving automatically"
              />
              <CustomTooltip
                size="Large"
                helptext={
                  <>
                    <p>
                      <FormattedMessage
                        id="profile-settings-dialog.account-preferences.localization.form.daylight-saving.hint1"
                        defaultMessage="Assume that you live in a region that does not observe daylight saving time (DST), but the setting for that time zone does adjust for daylight saving time. In this situation, you should not turnoff the Automatically Daylight Saving Time Adjustment. We recommend that you select a time zone that does not observe daylight saving time and that has the same Coordinated Universal Time (UTC) offset as the current time zone."
                      />{" "}
                      <br />
                    </p>
                    <p>
                      <FormattedMessage
                        id="profile-settings-dialog.account-preferences.localization.form.daylight-saving.hint2"
                        defaultMessage="For example, assume that your current time zone is (UTC-06:00) Central Time (US and Canada). There are two time zones that do not observe daylight saving time and that have the same UTC offset (-06:00):"
                      />
                    </p>
                    <ul style={{ paddingLeft: 20 }}>
                      <li>
                        <FormattedMessage
                          id="profile-settings-dialog.account-preferences.localization.form.daylight-saving.hint3"
                          defaultMessage="(UTC-06:00) Central America"
                        />
                      </li>
                      <li>
                        <FormattedMessage
                          id="profile-settings-dialog.account-preferences.localization.form.daylight-saving.hint4"
                          defaultMessage="(UTC-06:00) Saskatchewan"
                        />
                      </li>
                    </ul>
                  </>
                }
                iconType="help"
                position="static"
              />
            </p>
            <DefaultSwitch checked={dls} onChange={this.handleDlsChange} />
          </div>
          <Grid
            container
            direction="row"
            justify="flex-end"
            alignItems="center"
            style={{ marginBottom: 20 }}>
            <DefaultButton
              style={{ marginRight: 20 }}
              text={<FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />}
              buttonType="Transparent"
              onClick={this.handleClose}
            />
            <CustomButton
              onClick={this.handleLocalization}
              btnType="success"
              variant="contained"
              query={btnQuery}
              disabled={profileModified ? btnQuery == "progress" : true}>
              <FormattedMessage
                id="profile-settings-dialog.account-preferences.localization.form.submit-button.label"
                defaultMessage="Save Localization"
              />
            </CustomButton>
          </Grid>
          {/* Personalization----------------- */}
          {/* <div className={classes.accountPrefHeading} style={{ marginBottom: 10 }}>
            <div className={classes.accountPrefHeadingInner}>
              <PersonalizationIcon
                classes={{ root: classes.accountPrefHeadingIcon }}
                fontSize="small"
                htmlColor={theme.palette.secondary.light}
              />

              <FormattedMessage
                id="profile-settings-dialog.account-preferences.personalization.title"
                defaultMessage="Personalization"
              />
            </div>
            <div>
              <CustomTooltip
                helptext={
                  <FormattedMessage
                    id="profile-settings-dialog.account-preferences.personalization.hint"
                    defaultMessage={profileSettingsHelpText.personalizationHelpText}
                  />
                }
                iconType="help"
                position="static"
              />
            </div>
          </div>
          <ul className={classes.accountPrefContentStaticList}>
            <li>
              <p>
                <FormattedMessage
                  id="profile-settings-dialog.account-preferences.personalization.label"
                  defaultMessage="Select a background pattern"
                />
              </p>
              <div />
            </li>
            <li>
              <p>
                <FormattedMessage
                  id="profile-settings-dialog.account-preferences.personalization.dark-sidebar-mode.label"
                  defaultMessage="Dark Sidebar Mode"
                />
              </p>
              <div>
                <DefaultSwitch
                  checked={darkModeChecked}
                  onChange={event => {
                    this.handleDarkMode("darkModeChecked", event);
                  }}
                  value="darkModeChecked"
                />
              </div>
            </li>
             {isOldUser && <li>
              <p>Old Sidebar View</p>
              <div>
                <DefaultSwitch
                  checked={this.state.oldSidebarView}
                  onChange={event => {
                    this.oldSidebarView("oldSidebarView", event);
                  }}
                  value="oldSidebarView"
                />
              </div>
            </li>}
          </ul> */}
        </div>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    profileState: state.profile,
    constantsState: state.constants,
    userPreferenceState: state.userPreference,
  };
};

//export default AccountPreferences;
export default compose(
  withRouter,
  injectIntl,
  connect(mapStateToProps, {
    SaveLocalization,
    UpdateLocalization,
    UpdateUserPreferences,
    setSideBarTheme,
  })
)(AccountPreferences);
