import React, { Component } from "react";
import findIndex from "lodash/findIndex";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import LockIcon from "@material-ui/icons/Lock";
import EmailIcon from "@material-ui/icons/Email";
import SecurityIcon from "@material-ui/icons/Security";
import Grid from "@material-ui/core/Grid";
import Hotkeys from "react-hot-keys";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import {
  ChangePassword,
  DisableTwoFactorAuth,
  EnableTwoFactorAuth,
  VerifyAuthyCode,
  GetAuthyQrCode,
  SaveProfile,
  changeEmailAddress,
  cancelEmailRequest
} from "../../redux/actions/profileSettings";
import { FetchUserInfo, UpdateUserProfile, resendEmailVerification } from "../../redux/actions/profile";
import {
  handleAccountAttachment,
  handleAccountDeattachment
} from "../../redux/actions/socialAuthentication";
import DefaultTextField from "../../components/Form/TextField";
import ActionConfirmation from "../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import DefaultButton from "../../components/Buttons/DefaultButton";
import CustomButton from "../../components/Buttons/CustomButton";
import DefaultSwitch from "../../components/Form/Switch";
import {
  getSocialLoginInfo,
  facebookProvider,
  googleProvider,
  twitterProvider
} from "../../utils/socialLogin";
import { validatePasswordField } from "../../utils/formValidations";
import {
  FACEBOOK_ICON,
  GOOGLE_ICON,
  LINKEDIN_ICON,
  TWITTER_ICON
} from "../../utils/constants/icons";
import getErrorMessages from "../../utils/constants/errorMessages";
import { profileSettingsHelpText } from "../../components/Tooltip/helptext";
import CustomTooltip from "../../components/Tooltip/Tooltip";
import Tooltip from "@material-ui/core/Tooltip";
import LinearProgress from "@material-ui/core/LinearProgress";
import NotificationMessage from "../../components/NotificationMessages/NotificationMessages";
import { FormattedMessage, injectIntl } from "react-intl";


const passwordStrenghts = [
  { item: "with lowercase alphabets", rgx: new RegExp("[a-z]") },
  { item: "with uppercase alphabets", rgx: new RegExp("[A-Z]") },
  { item: "with numbers", rgx: new RegExp("[0-9]") },
  { item: "with special characters", rgx: new RegExp("[$@$!%*#?&]") },
  { item: "8-40 characters", rgx: new RegExp("^[\\S|\\s]{8,40}$") }
];

class SignInSecurity extends Component {
  constructor(props) {
    super(props);
    this.state = {
      oldPassword: "",
      newPassword: "",
      confirmNewPassword: "",
      SessionTimeout: 0,
      authCode: "",
      authCodeError: false,
      authCodeErrorMessage: "",
      QRCode: "",
      authEmail: false,
      authAccountSetup: false,
      FA2Check: false,

      oldPasswordError: false,
      newPasswordError: false,
      confirmNewPasswordError: false,
      SessionTimeoutError: false,

      confirmNewPasswordErrorMessage: "",
      newPasswordErrorMessage: "",
      oldPasswordErrorMessage: "",
      SessionTimeoutErrorMessage: "",

      profileModified: false,

      deattachDialogueOpen: false,
      deattachProvider: "",
      btnQuery: "",
      timeoutBtnQuery: "",
      detachBtnQuery: "",

      showTooltip: false,
      passwordStrength: { progress: 0 },
      changeEmailButton: true,
      changeEmailInput: null,
      emailConfirmationMessage: this.props.profileState.data.changeEmailRequest.isConfirm,

      changeEmailInputError: false,
      changeEmailInputMessage: "",
      showOldPassword: false,
      showConfirmPassword: false,
      showNewPassword: false,
      autoLogoutEnabled: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleInput = this.handleInput.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
    this.handleChangeSessionTimeout = this.handleChangeSessionTimeout.bind(this);
    this.enableAuthyAuth = this.enableAuthyAuth.bind(this);
    this.enableEmailAuth = this.enableEmailAuth.bind(this);
    this.setConnectedAccounts = this.setConnectedAccounts.bind(this);
    this.is2FAMethodEnabled = this.is2FAMethodEnabled.bind(this);
    this.onKeyDown = this.onKeyDown.bind(this);
    this.initState = this.state;
    this.translatePasswordStrengths(props.intl);
  }
  translatePasswordStrengths(intl) {
    let t1 = 8;
    let t2 = 40;
    passwordStrenghts[0].item = intl.formatMessage({
      id: "profile-settings-dialog.signin-security.change-password.form.password.hint1",
      defaultMessage: "with lowercase alphabets",
    });
    passwordStrenghts[1].item = intl.formatMessage({
      id: "profile-settings-dialog.signin-security.change-password.form.password.hint2",
      defaultMessage: "with uppercase alphabets",
    });
    passwordStrenghts[2].item = intl.formatMessage({
      id: "profile-settings-dialog.signin-security.change-password.form.password.hint3",
      defaultMessage: "with numbers",
    });
    passwordStrenghts[3].item = intl.formatMessage({
      id: "profile-settings-dialog.signin-security.change-password.form.password.hint4",
      defaultMessage: "with special characters",
    });
    passwordStrenghts[4].item = intl.formatMessage(
      {
        id: "profile-settings-dialog.signin-security.change-password.form.password.hint5",
        defaultMessage: "8-40 characters",
      },
      { t1, t2 }
    );
  }
  componentDidMount = () => {
    const { email, twoFactorEnabled, profile, changeEmailRequest } = this.props.profileState.data;
    this.setState(
      {
        authEmailInput: email,
        FA2Check: twoFactorEnabled,
        SessionTimeout: profile.autoLogoutTime,
        changeEmailInput: email,
        emailConfirmationMessage: changeEmailRequest.isConfirm,
        autoLogoutEnabled: profile.autoLogoutEnabled,
      },
      () => {
        this.initState = this.state;
      }
    );

    this.props.GetAuthyQrCode(response => {
      if (response.status === 200) {
        this.setState({
          QRCode: response.data.qrCode,
        });
      } else {
      }
    });
  };

  validatePassword = password => {
    let passStrengthObj = { progress: 0 };

    // Check the conditions
    var ctr = 0;
    // Do not show anything when the length of password is zero.
    if (password.length > 0) {
      for (var i = 0; i < passwordStrenghts.length; i++) {
        if (passwordStrenghts[i]["rgx"].test(password)) {
          ctr++;
          passStrengthObj[passwordStrenghts[i]["item"]] = true;
        }
      }

      passStrengthObj.progress = (100 / passwordStrenghts.length) * ctr;
    }

    return passStrengthObj;
  };

  handleInput = fieldName => event => {
    let inputValue = event.target.value;
    const props = this.props;

    if (fieldName !== "SessionTimeout") this.setState({ profileModified: true });

    switch (fieldName) {
      //////////////////////////////////// AUTHY CODE ////////////////////////////////////
      case "authCode":
        this.setState({ [fieldName]: inputValue });
        break;
      ////////////////////////////////////CHANGE EMAIL INPUT////////////////////////////////
      case "changeEmailInput":
        this.setState({
          [fieldName]: inputValue,
          changeEmailInputError: false,
          changeEmailInputMessage: "",
        });
        break;
      //////////////////////////////////// OLD PASSWORD ////////////////////////////////////
      case "oldPassword":
        inputValue === ""
          ? this.setState({ [fieldName]: inputValue })
          : this.setState({
            [fieldName]: inputValue,
            oldPasswordError: false,
            oldPasswordErrorMessage: "",
          });
        break;
      //////////////////////////////////// NEW PASSWORD ///////////////////////////////////
      case "newPassword":
        const passwordValidationObj = this.validatePassword(inputValue);
        inputValue === ""
          ? this.setState({
            [fieldName]: inputValue,
            passwordStrength: passwordValidationObj,
          })
          : this.setState({
            [fieldName]: inputValue,
            newPasswordError: false,
            newPasswordErrorMessage: "",
            passwordStrength: passwordValidationObj,
          });
        break;
      //////////////////////////////////// CONFIRM NEW PASSWORD ////////////////////////////////////
      case "confirmNewPassword":
        inputValue === ""
          ? this.setState({ [fieldName]: inputValue })
          : this.setState({
            [fieldName]: inputValue,
            confirmNewPasswordError: false,
            confirmNewPasswordErrorMessage: "",
          });
        break;
      //////////////////////////////////// SESSION TIMEOUT ////////////////////////////////////
      case "SessionTimeout": {
        inputValue = inputValue * 1;
        if (inputValue <= 10080) {
          inputValue = ""
            ? this.setState({
              [fieldName]: inputValue,
            })
            : this.setState(
              {
                [fieldName]: inputValue,
                SessionTimeoutError: false,
                SessionTimeoutErrorMessage: "",
              },
              () => {
                this.initState = this.state;
              }
            );
        }
        break;
      }
      default:
        this.setState({ [fieldName]: inputValue });
    }
  };
  handleChange(provider, event) {
    const state = event.target.checked;
    const { SessionTimeout } = this.state;
    const {
      jobTitle,
      jobTitleDetails,
      industry,
      industryDetails,
      firstName,
      lastName,
      country,
      phoneNumber,
      pictureUrl,
    } = this.props.profileState.data.profile;
    const intl = this.props.intl;
    switch (provider) {
      case "facebookAccount":
        this.handleExternalUserAccAttachment(facebookProvider, "Facebook", state);
        break;
      case "googleAccount":
        this.handleExternalUserAccAttachment(googleProvider, "Google", state);
        break;
      case "twitterAccount":
        this.handleExternalUserAccAttachment(twitterProvider, "Twitter", state);
        break;
      case "linkedInAccount":
        this.handleExternalUserAccAttachment("LinkedIn", "LinkedIn", state);
        break;
      case "authAccountSetup":
        this.disable2FAMethods(
          "authAccountSetup",
          "Authy",
          0,
          state,
          this.is2FAMethodEnabled("Authy")
        );
        break;
      case "authEmail":
        this.disable2FAMethods("authEmail", "Email", 1, state, this.is2FAMethodEnabled("Email"));
        break;
      case "autoLogoutEnabled":
        this.setState({ [provider]: state });
        let postObj = {
          jobTitle,
          jobTitleDetails,
          industry,
          industryDetails,
          firstName,
          lastName,
          country,
          phoneNumber,
          pictureUrl,
          autoLogoutTime: this.state.SessionTimeout,
          autoLogoutEnabled: state,
        };
        this.props.SaveProfile(postObj, () => {
          this.props.UpdateUserProfile(postObj);
        });
        break;
      default:
        this.setState({ [provider]: state });
    }
  }
  handleExternalUserAccAttachment = (loginProvider, providerName, state) => {
    const self = this;
    // ATTACH
    if (state) {
      getSocialLoginInfo(loginProvider).then(result => {
        const { accessToken, secret } = result.credential;
        this.props.handleAccountAttachment(
          { token: accessToken, provider: providerName, tokenSecret: secret },
          response => {
            if (response.data.email) {
              this.props.FetchUserInfo(response => { });
            } else if (response.data.message === "Account already attached") {
              this.props.showSnackBar(
                "Oops! This account has already been attached.",
                false,
                "success"
              );
            } else {
              this.props.showSnackBar(
                "Oops! This account has different profile details.",
                false,
                "error"
              );
            }
          },
          error => {
            const { response } = error;
            if (response.status === 409) {
              this.props.showSnackBar(
                "Oops! This account is already associated with another nTask account.",
                false,
                "info"
              );
            }
          }
        );
      });
    }
    // DE-ATTACH
    else {
      self.setState({
        deattachDialogueOpen: true,
        deattachProvider: providerName,
      });
    }
  };
  handleDeAttachment = () => {
    const self = this;
    self.setState({ detachBtnQuery: "progress" }, () => {
      this.props.handleAccountDeattachment(
        this.state.deattachProvider,
        response => {
          self.setState({ detachBtnQuery: "", deattachDialogueOpen: false });
          if (response && response.status === 200) {
            if (response.data.message === "Unable to deattach account.") {
              self.props.showSnackBar("Oops! You must have one account.", false, "info");
            } else {
              self.props.FetchUserInfo(response => { });
            }
          }
        },
        error => {
          self.setState({ detachBtnQuery: "", deattachDialogueOpen: false });
        }
      );
    });
  };
  disable2FAMethods = (methodFieldName, methodName, authEnum, state, isEnabled) => {
    let self = this;
    if (!state && isEnabled) {
      self.props.DisableTwoFactorAuth(response => {
        if (response && response.status === 200) {
          self.setState({
            [methodFieldName]: state,
            authCode: "",
            authCodeError: false,
            authCodeErrorMessage: "",
          });
          self.props.FetchUserInfo(response => { });
          self.props.showSnackBar(`${methodName} Authentication Disabled.`, false, "success");
        } else {
          self.props.showSnackBar("Failed to Disable", false, "error");
        }
      }, authEnum);
    } else {
      self.setState({
        [methodFieldName]: state,
        authCodeError: false,
        authCodeErrorMessage: "",
      });
    }
  };
  APIChangePassword = () => {
    let self = this;
    let changePassObj = {
      oldPassword: self.state.oldPassword,
      newPassword: self.state.newPassword,
      confirmPassword: self.state.confirmNewPassword,
    };
    self.setState({ btnQuery: "progress" });
    self.props.ChangePassword(
      changePassObj,
      data => {
        self.setState({
          profileModified: false,
          btnQuery: "",
          oldPassword: "",
          newPassword: "",
          confirmNewPassword: "",
        });
        this.props.showSnackBar("Password Changed Successfully", false, "success");
      },
      error => {
        self.setState({ profileModified: true, btnQuery: "" });
        if (error.status === 422) {
          this.props.showSnackBar(error.data.message, false, "error");
          this.setState({
            oldPasswordError: true,
            oldPasswordErrorMessage: error.data.message,
          });
        } else if (error.status === 500) {
          this.props.showSnackBar("Server throws error", false, "error");
        } else {
          this.props.showSnackBar(error.data.message, false, "error");
        }
      }
    );
  };
  enableAuthyAuth() {
    let self = this;
    if (self.state.authCode) {
      self.props.VerifyAuthyCode(self.state.authCode, response => {
        if (response.data && response.status === 200) {
          self.setState({
            authAccountSetup: false,
            authCodeError: false,
            authCodeErrorMessage: "",
          });
          self.props.showSnackBar("Verified", false, "success");
        } else {
          self.setState({
            authCodeError: true,
            authCodeErrorMessage: self.props.intl.formatMessage({
              id:
                "profile-settings-dialog.signin-security.two-factor-auth.setup-authy.form.validation.code.invalidcode",
              defaultMessage: getErrorMessages("code").INVALID,
            }),
          });
          self.props.showSnackBar("Unable to Verify", false, "error");
        }
      });
    } else {
      self.setState({
        authCodeError: true,
        authCodeErrorMessage: self.props.intl.formatMessage({
          id:
            "profile-settings-dialog.signin-security.two-factor-auth.setup-authy.form.validation.code.empty",
          defaultMessage: getErrorMessages("code").EMPTY_FIELD,
        }),
      });
    }
  }
  enableEmailAuth() {
    let self = this;
    self.props.EnableTwoFactorAuth(response => {
      if (response.status === 200) {
        self.props.FetchUserInfo(response => { });
        self.props.showSnackBar("Email Authentication Enabled.", false, "success");
      } else {
        self.props.showSnackBar("Email Authentication Disabled.", false, "error");
      }
    });
  }
  handleChangePassword() {
    let noErrorExists = true;
    let oldPasswordValue = this.state.oldPassword;
    let newPasswordValue = this.state.newPassword;
    let confirmNewPasswordValue = this.state.confirmNewPassword;

    //////////////////////////////////// OLD PASSWORD ////////////////////////////////////
    let validationObj = validatePasswordField(oldPasswordValue, "old password", this.props.intl);
    if (validationObj["isError"]) {
      noErrorExists = false;
      this.setState({
        oldPasswordError: validationObj["isError"],
        oldPasswordErrorMessage: validationObj["message"],
      });
    }
    //////////////////////////////////// NEW PASSWORD ///////////////////////////////////
    validationObj = validatePasswordField(newPasswordValue, "new password", this.props.intl);
    if (validationObj["isError"]) {
      noErrorExists = false;
      this.setState({
        newPasswordError: validationObj["isError"],
        newPasswordErrorMessage: validationObj["message"],
      });
    }
    //////////////////////////////////// CONFIRM NEW PASSWORD ////////////////////////////////////
    validationObj = validatePasswordField(
      confirmNewPasswordValue,
      "confirm password",
      this.props.intl
    );
    if (validationObj["isError"]) {
      noErrorExists = false;
      this.setState({
        confirmNewPasswordError: validationObj["isError"],
        confirmNewPasswordErrorMessage: validationObj["message"],
      });
    } else if (oldPasswordValue === newPasswordValue) {
      noErrorExists = false;
      this.setState({
        newPasswordError: true,
        newPasswordErrorMessage: this.props.intl.formatMessage({
          id:
            "profile-settings-dialog.signin-security.change-password.form.validation.new-password.different",
          defaultMessage: `Oops! new password must be different from old password.`,
        }),
        oldPasswordError: false,
        oldPasswordErrorMessage: "",
        confirmNewPasswordError: false,
        confirmNewPasswordErrorMessage: "",
      });
    } else if (newPasswordValue !== confirmNewPasswordValue) {
      noErrorExists = false;
      this.setState({
        confirmNewPasswordError: true,
        confirmNewPasswordErrorMessage: this.props.intl.formatMessage({
          id:
            "profile-settings-dialog.signin-security.change-password.form.validation.confirm-password.not-same",
          defaultMessage: getErrorMessages().CONFIRM_PASSWORD_FIELD,
        }),
      });
    }
    // if (this.state.passwordStrength.progress < 100) {
    //   this.setState({
    //     newPasswordError: true,
    //     newPasswordErrorMessage: "Oops! Password should be more complex"
    //   });
    //   return;
    // }
    // SAVE NEW PASSWORD IF NO ERROR EXISTS
    if (noErrorExists) {
      this.APIChangePassword();
    }
  }
  handleChangeSessionTimeout() {
    const { SessionTimeout } = this.state;
    const {
      jobTitle,
      jobTitleDetails,
      industry,
      industryDetails,
      firstName,
      lastName,
      country,
      phoneNumber,
      pictureUrl,
    } = this.props.profileState.data.profile;

    this.setState({ timeoutBtnQuery: "progress" });
    if (SessionTimeout > 10080 || SessionTimeout < 15) {
      let t1 = 15;
      let t2 = 10080;
      this.setState({
        SessionTimeoutError: true,
        SessionTimeoutErrorMessage: this.props.intl.formatMessage(
          {
            id:
              "profile-settings-dialog.signin-security.session-management.form.validation.time.min-max",
            defaultMessage: "Oops! Maximum and minimum time limit is 15 mins to 10080 mins.",
          },
          { t1, t2 }
        ),
        timeoutBtnQuery: "",
      });
    } else {
      let obj = {
        jobTitle,
        jobTitleDetails,
        industry,
        industryDetails,
        firstName,
        lastName,
        country,
        phoneNumber,
        pictureUrl,
        autoLogoutTime: this.state.SessionTimeout,
      };
      SaveProfile(obj, data => {
        if (data.status === 200) {
          this.props.UpdateUserProfile(obj);
          this.setState({
            SessionTimeoutError: false,
            SessionTimeoutErrorMessage: "",
            timeoutBtnQuery: "",
          });
        } else {
          this.setState({ timeoutBtnQuery: "" });
        }
      });
    }
  }

  setConnectedAccounts = provider => {
    return (
      findIndex(this.props.profileState.data.externalLogins, function (o) {
        return o.login.loginProvider === provider;
      }) !== -1
    );
  };
  getConnectedAccountMail = provider => {
    const { externalLogins } = this.props.profileState.data;
    const index = findIndex(externalLogins, function (o) {
      return o.login.loginProvider === provider;
    });
    return index !== -1 ? externalLogins[index].email : "";
  };
  is2FAMethodEnabled = method => {
    return (
      findIndex(this.props.profileState.data.twoFactorTypes, function (o) {
        return o.name === method;
      }) !== -1
    );
  };
  onKeyDown(event) {
    if (event === "enter") {
      if (!this.is2FAMethodEnabled("Authy")) {
        this.enableAuthyAuth();
      }
    }
  }
  handleDialogClose = () => {
    this.setState({ deattachDialogueOpen: false });
  };
  showTooltip = () => {
    this.setState({ showTooltip: true });
  };
  hideTooltip = () => {
    this.setState({ showTooltip: false });
  };
  enableEmailInput = () => {
    this.setState({ changeEmailButton: false, emailConfirmationMessage: true });
  };
  submitNewEmail = () => {
    /** function for updating the new email address */
    const prevValOfChangeEmailIput = this.props.profileState.data.email;
    const newValueofChangeEmailInput = this.state.changeEmailInput;
    if (prevValOfChangeEmailIput !== newValueofChangeEmailInput) {
      /** checks if the previous email is equal to new one or not */
      this.props.changeEmailAddress(
        newValueofChangeEmailInput,
        data => {
          /** API call with call back for changing the email address / send invitation email to new email */
          this.setState({
            changeEmailButton: true,
            changeEmailInput: newValueofChangeEmailInput,
            emailConfirmationMessage: false,
          });
        },
        error => {
          let errorText =
            error.data.message == "Please provide email."
              ? this.props.intl.formatMessage({
                id: "profile-settings-dialog.signin-security.change-email.form.validation.empty",
                defaultMessage: "Please provide email.",
              })
              : error.data.message;
          this.setState({
            changeEmailInputError: true,
            changeEmailInputMessage: errorText,
          });
        }
      );
    } else {
      this.setState({ changeEmailButton: true });
    }
  };
  cancelEmailButtonClick = () => {
    /** It discards the changings in change email input field and set default email address */
    this.setState({
      changeEmailButton: true,
      changeEmailInput: this.props.profileState.data.email,
      changeEmailInputError: false,
      changeEmailInputMessage: "",
    });
  };
  resendConfirmationEmail = () => {
    const email = this.props.profileState.data.changeEmailRequest.email;
    // this.props.changeEmailAddress(email, () => {});
    this.props.resendEmailVerification(() => {
      this.props.showSnackBar("Email sent successfully", "success");
    });
  };
  cancelEmailClick = () => {
    this.props.cancelEmailRequest(
      () => {
        this.setState({
          changeEmailButton: true,
          emailConfirmationMessage: true,
          changeEmailInput: this.props.profileState.data.email,
        });
      },
      () => { }
    );
  };
  handleClickShowOldPassword = () => {
    this.setState(state => ({ showOldPassword: !state.showOldPassword }));
  };
  handleClickShowNewPassword = () => {
    this.setState(state => ({ showNewPassword: !state.showNewPassword }));
  };
  handleClickShowConfirmPassword = () => {
    this.setState(state => ({
      showConfirmPassword: !state.showConfirmPassword,
    }));
  };
  render() {
    const {
      authEmail,
      authAccountSetup,
      oldPassword,
      oldPasswordError,
      oldPasswordErrorMessage,
      newPassword,
      newPasswordError,
      newPasswordErrorMessage,
      confirmNewPassword,
      confirmNewPasswordError,
      confirmNewPasswordErrorMessage,
      SessionTimeout,
      SessionTimeoutError,
      SessionTimeoutErrorMessage,
      authCode,
      authCodeError,
      authCodeErrorMessage,
      authEmailInput,
      authEmailInputError,
      authEmailInputErrorMessage,
      profileModified,
      deattachDialogueOpen,
      btnQuery,
      timeoutBtnQuery,
      detachBtnQuery,
      showTooltip,
      passwordStrength,
      changeEmailButton,
      changeEmailInput,
      emailConfirmationMessage,
      changeEmailInputError,
      changeEmailInputMessage,
      showOldPassword,
      showNewPassword,
      showConfirmPassword,
      autoLogoutEnabled,
    } = this.state;
    const { classes, theme } = this.props;
    const intl = this.props.intl;
    const { hasLocalAccount, changeEmailRequest } = this.props.profileState.data;
    const prevValOfChangeEmailIput = this.props.profileState.data.email;
    return (
      <div className={classes.accountPrefFormCnt}>
        {/* Two Factor Authentication----------------- */}
        <div className={classes.accountPrefHeading} style={{ marginBottom: 10 }}>
          <div className={classes.accountPrefHeadingInner}>
            <SecurityIcon
              classes={{ root: classes.accountPrefHeadingIcon }}
              fontSize="small"
              htmlColor={theme.palette.secondary.light}
            />
            <FormattedMessage
              id="profile-settings-dialog.signin-security.two-factor-auth.label"
              defaultMessage="Two Factor Authentication"
            />
          </div>
          <div>
            <CustomTooltip
              helptext={
                <FormattedMessage
                  id="profile-settings-dialog.signin-security.two-factor-auth.hint"
                  defaultMessage={profileSettingsHelpText.twoFactorAuthenticationHelpText}
                />
              }
              iconType="help"
              position="static"
            />
          </div>
        </div>
        <ul className={classes.accountPrefContentStaticList}>
          <li className={classes.noFlex}>
            <div className={classes.noFlexInner}>
              <p>
                {
                  <FormattedMessage
                    id="profile-settings-dialog.signin-security.two-factor-auth.setup-authy.label"
                    defaultMessage="Set up an Authy account"
                  />
                }
              </p>
              <div>
                <DefaultSwitch
                  size={"medium"}
                  disabled={!hasLocalAccount}
                  checked={authAccountSetup || this.is2FAMethodEnabled("Authy")}
                  onChange={event => this.handleChange("authAccountSetup", event)}
                  value="authAccountSetup"
                />
              </div>
            </div>
            {authAccountSetup && !this.is2FAMethodEnabled("Authy") ? (
              <React.Fragment>
                <div>
                  <img src={this.state.QRCode} />
                </div>
                <div className={classes.twoFactorAuthInputCnt}>
                  <Hotkeys keyName="enter" onKeyDown={this.onKeyDown}>
                    <DefaultTextField
                      label={
                        <FormattedMessage
                          id="profile-settings-dialog.signin-security.two-factor-auth.setup-authy.form.code.label"
                          defaultMessage="Code"
                        />
                      }
                      fullWidth={true}
                      errorState={authCodeError}
                      errorMessage={authCodeErrorMessage}
                      defaultProps={{
                        id: "authCode",
                        autoFocus: false,
                        autoComplete: false,
                        placeholder: intl.formatMessage({
                          id:
                            "profile-settings-dialog.signin-security.two-factor-auth.setup-authy.form.code.placeholder",
                          defaultMessage: "Enter code here",
                        }),
                        value: authCode,
                        onChange: this.handleInput("authCode"),
                      }}
                    />
                  </Hotkeys>
                  <DefaultButton
                    onClick={this.enableAuthyAuth}
                    style={{
                      marginLeft: 20,
                      width: "115px",
                      marginTop: "16px",
                    }}
                    text={
                      <FormattedMessage
                        id="profile-settings-dialog.signin-security.two-factor-auth.setup-authy.form.verify-button.label"
                        defaultMessage="Verify"
                      />
                    }
                    buttonType="Plain"
                  />
                </div>
              </React.Fragment>
            ) : null}
          </li>
          <li className={classes.noFlex}>
            <div className={classes.noFlexInner}>
              <p>
                <FormattedMessage
                  id="profile-settings-dialog.signin-security.two-factor-auth.email.label"
                  defaultMessage="Email Address"
                />
              </p>
              <div>
                <DefaultSwitch
                  size={"medium"}
                  disabled={!hasLocalAccount}
                  checked={authEmail || this.is2FAMethodEnabled("Email")}
                  onChange={event => {
                    this.handleChange("authEmail", event);
                  }}
                  value="authEmail"
                />
              </div>
            </div>
            {authEmail && !this.is2FAMethodEnabled("Email") ? (
              <div className={classes.twoFactorAuthEmailInputCnt}>
                <DefaultTextField
                  errorState={authEmailInputError}
                  errorMessage={authEmailInputErrorMessage}
                  defaultProps={{
                    id: "authEmailInput",
                    value: authEmailInput,
                    disabled: true,
                    onChange: this.handleInput("authEmailInput"),
                  }}
                />
                <DefaultButton
                  onClick={this.enableEmailAuth}
                  style={{
                    marginLeft: 20,
                    width: "165px",
                    padding: "10px 15px",
                  }}
                  text={
                    <FormattedMessage
                      id="profile-settings-dialog.signin-security.two-factor-auth.email.enable-button.label"
                      defaultMessage="Enable 2FA"
                    />
                  }
                  buttonType="Plain"
                />
              </div>
            ) : null}
          </li>
        </ul>

        {/* ======================== Change Email Address Section========================== */}
        <div className={classes.accountPrefHeading}>
          {" "}
          {/*  header of change email section ===*/}
          <div className={classes.accountPrefHeadingInner}>
            <EmailIcon
              classes={{ root: classes.accountPrefHeadingIcon }}
              htmlColor={theme.palette.secondary.light}
              fontSize="small"
            />
            <FormattedMessage
              id="profile-settings-dialog.signin-security.change-email.label"
              defaultMessage="Change Email Address"
            />
          </div>
          <div>
            <CustomTooltip
              helptext={
                <FormattedMessage
                  id="profile-settings-dialog.signin-security.change-email.hint"
                  defaultMessage={profileSettingsHelpText.changeEmailHelpText}
                />
              }
              iconType="help"
              position="static"
            />
          </div>
        </div>
        <div className={classes.twoFactorAuthEmailInputCnt}>
          {" "}
          {/*  Email Address input and buttons ===*/}
          <DefaultTextField
            errorState={changeEmailInputError}
            errorMessage={changeEmailInputMessage}
            defaultProps={{
              id: "changeEmailInput",
              value: changeEmailInput,
              disabled: changeEmailButton ? true : false,
              onChange: this.handleInput("changeEmailInput"),
            }}
          />
        </div>
        {!emailConfirmationMessage ? (
          <NotificationMessage type="info" iconType="info" style={{ marginBottom: 20 }}>
            {
              <FormattedMessage
                id="profile-settings-dialog.signin-security.change-email.form.success.label"
                values={{ email: changeEmailRequest.email }}
                defaultMessage={`Your new email "${changeEmailRequest.email}" has not been confirmed. `}
              />
            }
            <br />
            <span className={classes.confirmEmailText} onClick={this.resendConfirmationEmail}>
              <FormattedMessage
                id="profile-settings-dialog.signin-security.change-email.form.success.resend-button.label"
                defaultMessage="Resend"
              />
            </span>
            {` | `}
            <span className={classes.cancelEmailText} onClick={this.cancelEmailClick}>
              <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
            </span>
          </NotificationMessage>
        ) : null}
        <div style={{ marginBottom: 30 }}>
          {" "}
          {/*  Grid of cancel / change email and update email button  ===*/}
          <Grid
            container
            direction="row"
            justify="flex-end"
            alignItems="center"
            marginBottom="30px">
            {!changeEmailButton ? (
              <DefaultButton
                text={<FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />}
                buttonType="Transparent"
                style={{ marginRight: 20 }}
                disabled={changeEmailButton ? true : false}
                onClick={
                  this.cancelEmailButtonClick
                } /** function for discarding any change in input field */
              />
            ) : null}
            {emailConfirmationMessage ? (
              changeEmailButton ? (
                <CustomButton
                  onClick={
                    this.enableEmailInput
                  } /** function calling, when user click on change email button it enables the input field and switch itself to update email button */
                  btnType="success"
                  variant="contained"
                  query={btnQuery}
                  disabled={false}>
                  <FormattedMessage
                    id="profile-settings-dialog.signin-security.change-email.form.change-email-button.label"
                    defaultMessage="Change Email"
                  />
                </CustomButton>
              ) : (
                <CustomButton
                  onClick={this.submitNewEmail} /** submit new entered email function calling  */
                  btnType="success"
                  variant="contained"
                  query={btnQuery}
                  disabled={changeEmailInput != prevValOfChangeEmailIput ? false : true}>
                  <FormattedMessage
                    id="profile-settings-dialog.signin-security.change-email.form.update-button.label"
                    defaultMessage="Update Email"
                  />
                </CustomButton>
              )
            ) : null}
          </Grid>
        </div>

        {/* Password----------------- */}
        <div className={classes.accountPrefHeading}>
          <div className={classes.accountPrefHeadingInner}>
            <LockIcon
              classes={{ root: classes.accountPrefHeadingIcon }}
              htmlColor={theme.palette.secondary.light}
              fontSize="small"
            />
            <FormattedMessage
              id="profile-settings-dialog.signin-security.change-password.label"
              defaultMessage="Change Password"
            />
          </div>
          <div>
            <CustomTooltip
              helptext={
                <FormattedMessage
                  id="profile-settings-dialog.signin-security.change-password.hint"
                  defaultMessage={profileSettingsHelpText.changePasswordHelpText}
                />
              }
              iconType="help"
              position="static"
            />
          </div>
        </div>

        <DefaultTextField
          label={
            <FormattedMessage
              id="profile-settings-dialog.signin-security.change-password.form.old-password.label"
              defaultMessage="Old Password"
            />
          }
          fullWidth={true}
          errorState={oldPasswordError}
          errorMessage={oldPasswordErrorMessage}
          defaultProps={{
            id: "oldPassword",
            value: oldPassword,
            onChange: this.handleInput("oldPassword"),
            type: showOldPassword ? "text" : "password",
            endAdornment: (
              <InputAdornment position="end">
                <IconButton
                  disableRipple={true}
                  classes={{
                    root: classes.passwordVisibilityBtn,
                  }}
                  onClick={this.handleClickShowOldPassword}>
                  {showOldPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            ),
          }}
        />


        <DefaultTextField
          label={
            <FormattedMessage
              id="profile-settings-dialog.signin-security.change-password.form.password.label"
              defaultMessage="New Password"
            />
          }
          fullWidth={true}
          errorState={newPasswordError}
          errorMessage={newPasswordErrorMessage}
          defaultProps={{
            id: "newPassword",
            value: newPassword,
            onChange: this.handleInput("newPassword"),
            onClick: this.showTooltip,
            onBlur: this.hideTooltip,
            type: showNewPassword ? "text" : "password",
            endAdornment: (
              <InputAdornment position="end">
                <Tooltip
                  interactive
                  open={showTooltip}
                  disableFocusListener
                  disableHoverListener
                  disableTouchListener
                  classes={{
                    tooltip: classes.tooltip,
                  }}
                  title={
                    <div className="panel-body">
                      <LinearProgress
                        classes={{
                          root: classes.progressBar,
                          bar: `${passwordStrength.progress <= 40
                            ? classes.redBar
                            : passwordStrength.progress <= 80
                              ? classes.orangeBar
                              : classes.greenBar
                            }`,
                        }}
                        variant="determinate"
                        value={passwordStrength.progress}
                      />
                      <div>
                        <FormattedMessage
                          id="profile-settings-dialog.signin-security.change-password.form.password.hint"
                          defaultMessage="A good password is :"
                        />{" "}
                      </div>
                      <ul>
                        {passwordStrenghts.map((pStrength, i) => {
                          return (
                            <li
                              key={i}
                              className={`${passwordStrength[pStrength.item] ? classes.optionChecked : ""
                                }`}>
                              <small>{pStrength.item}</small>
                            </li>
                          );
                        })}
                      </ul>
                    </div>
                  }
                  placement="right">
                  <IconButton
                    disableRipple={true}
                    classes={{
                      root: classes.passwordVisibilityBtn,
                    }}
                    onClick={this.handleClickShowNewPassword}>
                    {showNewPassword ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </Tooltip>
              </InputAdornment>
            ),
          }}
        />

        <DefaultTextField
          label={
            <FormattedMessage
              id="profile-settings-dialog.signin-security.change-password.form.confirm-password.label"
              defaultMessage="Confirm New Password"
            />
          }
          // formControlStyles={{ marginBottom: 15 }}
          fullWidth={true}
          errorState={confirmNewPasswordError}
          errorMessage={confirmNewPasswordErrorMessage}
          defaultProps={{
            id: "confirmNewPassword",
            value: confirmNewPassword,
            onChange: this.handleInput("confirmNewPassword"),
            type: showConfirmPassword ? "text" : "password",
            endAdornment: (
              <InputAdornment position="end">
                <IconButton
                  disableRipple={true}
                  classes={{
                    root: classes.passwordVisibilityBtn,
                  }}
                  onClick={this.handleClickShowConfirmPassword}>
                  {showConfirmPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            ),
          }}
        />

        <Grid container direction="row" justify="flex-end" alignItems="center">
          <DefaultButton
            text={<FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />}
            buttonType="Transparent"
            onClick={() => this.props.onDialogueClose()}
          />
          <CustomButton
            onClick={this.handleChangePassword}
            btnType="success"
            variant="contained"
            query={btnQuery}
            disabled={profileModified ? btnQuery == "progress" : true}>
            <FormattedMessage
              id="profile-settings-dialog.signin-security.change-password.form.save-button.label"
              defaultMessage="Save Changes"
            />
          </CustomButton>
        </Grid>

        <div className={classes.accountPrefHeading} style={{ marginTop: 30, paddingRight: 10 }}>
          <div className={classes.accountPrefHeadingInner}>
            <LockIcon
              classes={{ root: classes.accountPrefHeadingIcon }}
              htmlColor={theme.palette.secondary.light}
              fontSize="small"
            />
            <FormattedMessage
              id="profile-settings-dialog.signin-security.session-management.label"
              defaultMessage="Session Management"
            />
          </div>
          <div>
            <DefaultSwitch
              size={"medium"}
              checked={autoLogoutEnabled}
              color={autoLogoutEnabled ? undefined : "Dark"}
              onChange={event => this.handleChange("autoLogoutEnabled", event)}
              value="autoLogoutEnabled"
            />
          </div>
        </div>
        {autoLogoutEnabled && (
          <>
            <DefaultTextField
              label={
                <FormattedMessage
                  id="profile-settings-dialog.signin-security.session-management.form.time.label"
                  defaultMessage="Timeout In Minutes"
                />
              }
              formControlStyles={{ marginBottom: 0 }}
              fullWidth={true}
              errorState={SessionTimeoutError}
              errorMessage={SessionTimeoutErrorMessage}
              defaultProps={{
                id: "sessionTimeout",
                value: SessionTimeout,
                onChange: this.handleInput("SessionTimeout"),
                type: "text",
              }}
            />

            <Grid container direction="row" justify="flex-end" alignItems="center">
              <DefaultButton
                text={<FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />}
                buttonType="Transparent"
                onClick={() => this.props.onDialogueClose()}
              />
              <CustomButton
                onClick={this.handleChangeSessionTimeout}
                btnType="success"
                variant="contained"
                query={timeoutBtnQuery}
                disabled={this.initState.SessionTimeout == SessionTimeout}>
                <FormattedMessage
                  id="profile-settings-dialog.signin-security.session-management.form.update-button.label"
                  defaultMessage="Update Time"
                />
              </CustomButton>
            </Grid>
          </>
        )}
        <div
          className={classes.accountPrefHeading}
          style={{ marginBottom: 10, paddingRight: 0, marginTop: 30 }}>
          <div className={classes.accountPrefHeadingInner}>
            <SecurityIcon
              classes={{ root: classes.accountPrefHeadingIcon }}
              fontSize="small"
              htmlColor={theme.palette.secondary.light}
            />
            <FormattedMessage
              id="profile-settings-dialog.signin-security.connected-accounts.label"
              defaultMessage="Connected Accounts"
            />
          </div>
        </div>

        <ul className={classes.accountPrefContentStaticList}>
          <li className={classes.noFlex} >
            <div className={classes.noFlexInner}>
              <p className={classes.accountAttachMethod}>
                <img src={FACEBOOK_ICON} />
                <FormattedMessage
                  id="profile-settings-dialog.signin-security.connected-accounts.facebook.label"
                  defaultMessage="Facebook"
                />
              </p>
              {this.setConnectedAccounts("Facebook") ? (
                <span className={classes.accountAttachEmail}>
                  {this.getConnectedAccountMail("Facebook")}
                </span>
              ) : null}
              <div>
                <DefaultSwitch
                  size={"medium"}
                  checked={this.setConnectedAccounts("Facebook")}
                  onChange={event => this.handleChange("facebookAccount", event)}
                  value="facebookAccount"
                />
              </div>
            </div>
          </li>
          <li className={classes.noFlex} >
            <div className={classes.noFlexInner}>
              <p className={classes.accountAttachMethod}>
                <img src={GOOGLE_ICON} />
                <FormattedMessage
                  id="profile-settings-dialog.signin-security.connected-accounts.google.label"
                  defaultMessage="Google"
                />
              </p>
              {this.setConnectedAccounts("Google") ? (
                <span className={classes.accountAttachEmail}>
                  {this.getConnectedAccountMail("Google")}
                </span>
              ) : null}
              <div>
                <DefaultSwitch
                  size={"medium"}
                  checked={this.setConnectedAccounts("Google")}
                  onChange={event => this.handleChange("googleAccount", event)}
                  value="googleAccount"
                />
              </div>
            </div>
          </li>
          <li className={classes.noFlex} >
            <div className={classes.noFlexInner}>
              <p className={classes.accountAttachMethod}>
                <img src={TWITTER_ICON} />
                <FormattedMessage
                  id="profile-settings-dialog.signin-security.connected-accounts.twitter.label"
                  defaultMessage="Twitter"
                />
              </p>
              {this.setConnectedAccounts("Twitter") ? (
                <span className={classes.accountAttachEmail}>
                  {this.getConnectedAccountMail("Twitter")}
                </span>
              ) : null}
              <div>
                <DefaultSwitch
                  size={"medium"}
                  checked={this.setConnectedAccounts("Twitter")}
                  onChange={event => this.handleChange("twitterAccount", event)}
                  value="twitterAccount"
                />
              </div>
            </div>
          </li>
          <li className={classes.noFlex} >
            <div className={classes.noFlexInner}>
              <p className={classes.accountAttachMethod}>
                <img src={LINKEDIN_ICON} />
                <FormattedMessage
                  id="profile-settings-dialog.signin-security.connected-accounts.linkedin.label"
                  defaultMessage="LinkedIn"
                />
              </p>
              {this.setConnectedAccounts("LinkedIn") ? (
                <span className={classes.accountAttachEmail}>
                  {this.getConnectedAccountMail("LinkedIn")}
                </span>
              ) : null}
              <div>
                <DefaultSwitch
                  size={"medium"}
                  checked={this.setConnectedAccounts("LinkedIn")}
                  onChange={event => this.handleChange("linkedInAccount", event)}
                  value="linkedInAccount"
                />
              </div>
            </div>
          </li>
        </ul>
        <ActionConfirmation
          open={deattachDialogueOpen}
          closeAction={this.handleDialogClose}
          cancelBtnText="Cancel"
          successBtnText="Disconnect"
          alignment="center"
          headingText="Disconnect Account"
          iconType="archive"
          msgText={`Are you sure you want to disconnect this account?`}
          successAction={this.handleDeAttachment}
          btnQuery={detachBtnQuery}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    profileState: state.profile
  };
};

export default compose(
  withRouter,
  injectIntl,
  connect(mapStateToProps, {
    ChangePassword,
    DisableTwoFactorAuth,
    EnableTwoFactorAuth,
    VerifyAuthyCode,
    GetAuthyQrCode,
    FetchUserInfo,
    handleAccountAttachment,
    handleAccountDeattachment,
    UpdateUserProfile,
    changeEmailAddress,
    cancelEmailRequest,
    SaveProfile,
    resendEmailVerification
  })
)(SignInSecurity);
