import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import DefaultTextField from "../../components/Form/TextField";
import Select from "react-select";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import DefaultButton from "../../components/Buttons/DefaultButton";
import CustomButton from "../../components/Buttons/CustomButton";
import Grid from "@material-ui/core/Grid";
import DeleteIcon from "@material-ui/icons/Cancel";
import Icons from "../../components/Icons/Icons";
import DropdownArrow from "@material-ui/icons/ArrowDropDown";
import { components } from "react-select";
import { generateUsername, getModifiedState } from "../../utils/common";
import SignOutIcon from "@material-ui/icons/PowerSettingsNew";
import helper from "../../helper";
import autoCompleteStyles from "../../assets/jss/components/autoComplete";
import combineStyles from "../../utils/mergeStyles";
import profileSetting from "./styles";
import { withStyles } from "@material-ui/core/styles";
import ActionConfirmation from "../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import isEqual from "lodash/isEqual";
import {
  validatePhoneField,
  validateTitleField,
  validateMultiWordNameField,
  filterNameInputValue,
} from "../../utils/formValidations";
import getErrorMessages from "../../utils/constants/errorMessages";
import { uploadprofileimage } from "../../redux/actions/onboarding";
import { Logout } from "../../redux/actions/logout";
import {
  SaveLocalization,
  SaveProfile,
  RemoveProfileImage,
} from "../../redux/actions/profileSettings";
import {
  UpdateUserProfile,
  UpdateProfileImage,
} from "../../redux/actions/profile";
import CustomAvatar from "../../components/Avatar/Avatar";
import { validName } from "../../utils/validator/common/name";
import { validPhone } from "../../utils/validator/common/phone";
import { getImageExtensions } from "../../helper/getImageExtensions";
import { FormattedMessage, FormattedDate } from "react-intl";
import { injectIntl } from "react-intl";

// import translate from '../../i18n/translate';

class PersonalInformation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fullName: "",
      fullNameError: false,
      fullNameErrorMessage: "",
      companyIndustry: "",
      otherIndustry: false,
      otherCompanyIndustry: false,
      jobRole: "",
      otherJobRole: "",
      otherJobRoleSelected: false,
      country: {
        label: "United States",
        value: "US",
      },
      phoneNumber: "",
      phoneNumberError: "",
      phoneNumberErrorMessage: "",
      pictureUrlFull: "",
      imageUrl: "",
      profileModified: false,
      industryList: [],
      jobList: [],
      countryList: [],
      otherJobRoleError: false,
      otherJobRolerErrorMessage: "",
      otherCompanyIndustryError: false,
      otherCompanyIndustryErrorMessage: "",
      fieldsChanged: {},
      btnQuery: "",
      logoutDialogueOpen: false,
      logoutBtnQuery: "",
    };
    this.handleInput = this.handleInput.bind(this);
    this.handleCountryChange = this.handleCountryChange.bind(this);
    this.handleIndustrySelect = this.handleIndustrySelect.bind(this);
    this.handleJobRoleSelect = this.handleJobRoleSelect.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleSaveChanges = this.handleSaveChanges.bind(this);
    this.clickFile = this.clickFile.bind(this);
    this._handleImageChange = this._handleImageChange.bind(this);
    this.removeProfilePicture = this.removeProfilePicture.bind(this);
    this.changeModificationStatus = this.changeModificationStatus.bind(this);
  }
  componentDidMount = () => {
    let countryName = this.state.country;
    if (this.props.constantsState.data) {
      this.props.constantsState.data.countriesList.map((obj) => {
        if (obj.value === this.props.profileState.data.profile.country) {
          countryName = obj;
        }
      });
      this.setState({
        industryList: this.props.constantsState.data.industriesList,
        jobList: this.props.constantsState.data.jobTitlesList,
        countryList: this.props.constantsState.data.countriesList,

        fullName: this.props.profileState.data.profile.fullName,
        jobRole:
          this.props.profileState.data.profile.jobTitle !== null
            ? {
                value: this.props.profileState.data.profile.jobTitle,
                label: this.translate(this.props.profileState.data.profile.jobTitle),
              }
            : null,
        otherJobRole:
          this.props.profileState.data.profile.jobTitleDetails || null,
        otherJobRoleSelected:
          this.props.profileState.data.profile.jobTitle &&
          this.props.profileState.data.profile.jobTitle.toLowerCase() ===
            "other"
            ? true
            : false,
        companyIndustry:
          this.props.profileState.data.profile.industry !== null
            ? {
                value: this.props.profileState.data.profile.industry,
                label: this.translate(this.props.profileState.data.profile.industry),
              }
            : null,
        otherIndustry:
          this.props.profileState.data.profile.industry &&
          this.props.profileState.data.profile.industry.toLowerCase() ===
            "other"
            ? true
            : false,
        otherCompanyIndustry:
          this.props.profileState.data.profile.industryDetails || null,

        country: countryName,
        phoneNumber: this.props.profileState.data.profile.phoneNumber,
        pictureUrlFull: this.props.profileState.data.imageUrl,
        imageUrl: this.props.profileState.data.imageUrl,
      });
    }
  };

  componentDidUpdate = (prevProps) => {
    const { constantsState } = this.props;
    let countryName = this.state.country;
    if (
      constantsState.data &&
      !isEqual(constantsState.data, prevProps.constantsState.data)
    ) {
      this.props.constantsState.data.countriesList.map((obj) => {
        if (obj.value === this.props.profileState.data.profile.country) {
          countryName = obj;
        }
      });
      this.setState({
        industryList: this.props.constantsState.data.industriesList,
        jobList: this.props.constantsState.data.jobTitlesList,
        countryList: this.props.constantsState.data.countriesList,

        fullName: this.props.profileState.data.fullName,
        jobRole:
          this.props.profileState.data.profile.jobTitle !== null
            ? {
                value: this.props.profileState.data.profile.jobTitle,
                label: this.props.profileState.data.profile.jobTitle,
              }
            : null,
        otherJobRole:
          this.props.profileState.data.profile.jobTitleDetails || null,
        otherJobRoleSelected:
          this.props.profileState.data.profile.jobTitle &&
          this.props.profileState.data.profile.jobTitle.toLowerCase() ===
            "other"
            ? true
            : false,
        companyIndustry:
          this.props.profileState.data.profile.industry !== null
            ? {
                value: this.props.profileState.data.profile.industry,
                label: this.props.profileState.data.profile.industry,
              }
            : null,
        otherIndustry:
          this.props.profileState.data.profile.industry &&
          this.props.profileState.data.profile.industry.toLowerCase() ===
            "other"
            ? true
            : false,
        otherCompanyIndustry:
          this.props.profileState.data.profile.industryDetails || null,

        country: countryName,
        phoneNumber: this.props.profileState.data.profile.phoneNumber,
        pictureUrlFull: this.props.profileState.data.imageUrl,
        imageUrl: this.props.profileState.data.imageUrl,
      });
    }
  };
  validNameInput = (key, value) => {
    /**Function for checking if the first name and last name is valid or not */
    let validationObj = validName(value, key);
    if (!validationObj.validated) {
      switch (key) {
        case "fullName":
          this.setState({
            fullNameError: true,
            fullNameErrorMessage: validationObj.errorMessage,
          });
          break;

        default:
          break;
      }
    }
    return validationObj.validated;
  };

  validPhoneNumber = (phoneValue) => {
    /**Function for checking if the phone is valid or not */
    let validationObj = validPhone(phoneValue);
    if (!validationObj.validated) {
      this.setState({
        phoneNumberError: true,
        phoneNumberErrorMessage: validationObj.errorMessage,
      });
    }
    return validationObj.validated;
  };

  onClickValidationsChecker() {
    let errorCheck_fullName = false;
    let errorCheck_PhoneNum = false;

    //////////////////////////////////// FIRST NAME ////////////////////////////////////
    let fullNameValue = this.state.fullName;
    errorCheck_fullName = this.validNameInput("fullName", fullNameValue);

    ////////////////////////////////////// PHONE NUMBER //////////////////////////////////////
    let phoneValue = this.state.phoneNumber;
    if (phoneValue) errorCheck_PhoneNum = this.validPhoneNumber(phoneValue);
    else errorCheck_PhoneNum = true;
    return errorCheck_fullName && errorCheck_PhoneNum;
  }

  clickFile() {
    let fileinputbtn = document.getElementById("profileImageUpload");
    fileinputbtn.click();
  }
  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.props.onDialogueClose();
  };
  handleInput = (fieldName) => (event) => {
    let inputValue = event.target.value;
    if (inputValue) {
      if (fieldName === "phoneNumber") {
        // inputValue.replace(/[\(|\)|+|-]/gi, '')
        this.changeModificationStatus(
          fieldName,
          this.props.profileState.data.profile[fieldName],
          inputValue.trim()
        );
      } else
        this.changeModificationStatus(
          fieldName,
          this.props.profileState.data[fieldName],
          inputValue.trim()
        );
    }

    switch (fieldName) {
      //////////////////////////////////// FIRST NAME ////////////////////////////////////
      case "fullName":
        // inputValue = filterNameInputValue(inputValue);
        inputValue === ""
          ? this.setState({ [fieldName]: inputValue })
          : this.setState({
              [fieldName]: inputValue,
              fullNameError: false,
              fullNameErrorMessage: "",
            });
        break;
      //////////////////////////////////// LAST NAME ////////////////////////////////////

      //////////////////////////////////// OTHER INDUSTRY ////////////////////////////////////
      case "otherCompanyIndustry":
        inputValue === ""
          ? this.setState({ [fieldName]: inputValue })
          : this.setState({
              [fieldName]: inputValue,
              otherCompanyIndustryError: false,
              otherCompanyIndustryErrorMessage: "",
            });
        break;
      //////////////////////////////////// OTHER JOB ////////////////////////////////////
      case "otherJobRole":
        inputValue === ""
          ? this.setState({ [fieldName]: inputValue })
          : this.setState({
              [fieldName]: inputValue,
              otherJobRoleError: false,
              otherJobRolerErrorMessage: "",
            });
        break;
      //////////////////////////////////// PHONE NUMBER ////////////////////////////////////
      case "phoneNumber":
        const patt = new RegExp(
          /^([\+]?|[\+]?[\d]+|[\+]?[\d]+[\-]?|[\+]?[\d]+[\-]?[\d]+)$/
        );
        if (inputValue === "" || patt.test(inputValue))
          this.setState({
            [fieldName]: inputValue,
            phoneNumberError: false,
            phoneNumberErrorMessage: "",
            profileModified: true,
          });
        break;
      default:
        this.setState({ [fieldName]: inputValue });
    }
  };
  handleCountryChange(value) {
    this.changeModificationStatus(
      "country",
      this.props.profileState.data.profile["country"],
      value.value
    );
    this.setState({ country: value });
  }
  handleIndustrySelect(value) {
    this.changeModificationStatus(
      "industry",
      this.props.profileState.data.profile["industry"],
      value.value
    );
    if (value.label.toLowerCase() == "other") {
      this.setState({
        companyIndustry: value,
        otherIndustry: true,
      });
    } else {
      this.setState({
        companyIndustry: value,
        otherIndustry: false,
        otherCompanyIndustry: "",
      });
    }
  }
  handleSaveChanges(e) {
    e.preventDefault();
    let isValidationSuccessfull = this.onClickValidationsChecker();
    if (isValidationSuccessfull) {
      let obj = {
        jobTitle: this.state.jobRole ? this.state.jobRole.value : null,
        jobTitleDetails: this.state.jobRole
          ? this.state.jobRole.value === "Other" ||
            this.state.jobRole.value === "other"
            ? this.state.otherJobRole
            : null
          : null,
        industry: this.state.companyIndustry
          ? this.state.companyIndustry.value
          : null,
        industryDetails: this.state.companyIndustry
          ? this.state.companyIndustry.value === "Other" ||
            this.state.companyIndustry.value === "other"
            ? this.state.otherCompanyIndustry
            : null
          : null,
        fullName: this.state.fullName.trim(),
        country: this.state.country.value,
        phoneNumber: this.state.phoneNumber,
        pictureUrl: this.state.imageUrl,
        autoLogoutTime: this.props.profileState.data.profile.autoLogoutTime,
      };
      this.setState({ btnQuery: "progress" });
      this.props.SaveProfile(obj, (data) => {
        if (data.status === 200) {
          this.props.UpdateUserProfile(obj);
          this.setState({
            otherJobRoleError: false,
            otherJobRolerErrorMessage: "",
            otherCompanyIndustryError: false,
            otherCompanyIndustryErrorMessage: "",
            profileModified: false,
            btnQuery: "",
          });
          this.props.showSnackBar("Personal Information Updated", 'info');
        } else {
          this.setState({ btnQuery: "" });
          this.props.showSnackBar("Unable to Updated", 'info');
        }
      });
    }
  }
  handleJobRoleSelect(value) {
    this.changeModificationStatus(
      "jobTitle",
      this.props.profileState.data.profile["jobTitle"],
      value.value
    );
    if (value.label == "other" || value.label == "Other") {
      this.setState({
        jobRole: value,
        otherJobRoleSelected: true,
      });
    } else {
      this.setState({
        jobRole: value,
        otherJobRoleSelected: false,
        otherJobRole: "",
      });
    }
  }
  removeProfilePicture() {
    this.props.RemoveProfileImage("?email=", (data) => {
      if (data && data.status === 200) {
        this.props.UpdateProfileImage("default-male-image.png");
        this.setState({
          imageUrl: "default-male-image.png",
          pictureUrlFull:
            this.props.constantsState.data.pictureBaseUrl +
            "default-male-image.png",
        });
      }
    });
  }
  _handleImageChange(e) {
    e.preventDefault();
    // let fileExt = ["jpg", "png", "gif", "jpeg", "PNG", "JPG", "JPEG", "GIF", "bmp", "BMP"];

    var data = new FormData();
    let reader = new FileReader();
    let file = e.target;
    let self = this;
    if (file.files.length > 0) {
      let type = file.files[0].name.toLowerCase().match(/[0-9a-z]{1,5}$/gm)[0];
      if (type != null && getImageExtensions(type)) {
        reader.onloadend = () => {
          this.setState({
            file: file.files[0],
            imagePreviewUrl: reader.result,
          });
        };
        reader.readAsDataURL(file.files[0]);
        data.append("PrevImageName", this.state.imageUrl);
        data.append("UploadedImage", file.files[0]);
        self.props.uploadprofileimage(
          data,
          (response) => {
            self.setState({
              pictureUrlFull: response.data,
              imageUrl: response.data,
            });
            this.props.showSnackBar(
              "Profile image has been uploaded successfully.",
              false,
              "success"
            );
          },
          (err) => {
            this.props.showSnackBar(err.data.message, false, "error");
          }
        );
      } else {
        this.props.showSnackBar(
          getErrorMessages().INVALID_IMAGE_EXTENSION,
          false
        );
      }
    }
  }

  changeModificationStatus = (fieldname, propsValue, value) => {
    this.setState(
      getModifiedState(this.state.fieldsChanged, fieldname, propsValue, value)
    );
  };

  openLogoutDialogue = () => {
    this.setState({ logoutDialogueOpen: true });
  };

  closeLogoutDialogue = () => {
    this.setState({ logoutDialogueOpen: false });
  };

  handleLogoutFromAllDevices = () => {
    this.props.Logout(
      "?logOutAll=true",
      null,
      (resp) => {},
      (err) => {
        this.setState({ logoutDialogueOpen: false });
      }
    );
  };
  translate = (value) => {
    let id = "";
   switch(value) {
    case "Accounts and Finance": 
      id = "profile-settings-dialog.personal-information.job-title.list.accounts-finance";
      break;
    case "Administration":
      id = "profile-settings-dialog.personal-information.job-title.list.administration";
      break;
    case "Advisory":
      id = "profile-settings-dialog.personal-information.job-title.list.advisory";
      break;  
    case "Auditing":
      id = "profile-settings-dialog.personal-information.job-title.list.auditing";
      break; 
    case "Creative":
        id = "profile-settings-dialog.personal-information.job-title.list.creative";
        break;
    case "Design and UI/UX":
        id = "profile-settings-dialog.personal-information.job-title.list.design-ui-ux";
        break;
    case "Human Resources":
        id = "profile-settings-dialog.personal-information.job-title.list.human-resources";
        break;   
    case "Management":
        id = "profile-settings-dialog.personal-information.job-title.list.management";
        break;
    case "Marketing":
        id = "profile-settings-dialog.personal-information.job-title.list.marketing";
        break;
    case "Performance Management":
        id = "profile-settings-dialog.personal-information.job-title.list.performance-management";
        break;  
    case "Project Management":
        id = "profile-settings-dialog.personal-information.job-title.list.project-management";
        break;
    case "Quality Control and Assurance":
        id = "profile-settings-dialog.personal-information.job-title.list.quality-assurance";
        break;
    case "Sales and Business Development":
        id = "profile-settings-dialog.personal-information.job-title.list.sales-business";
        break;
    case "Software Development":
        id = "profile-settings-dialog.personal-information.job-title.list.software-development";
        break;  
    case "Support and Customer Service":
        id = "profile-settings-dialog.personal-information.job-title.list.support-service";
        break;
    case "Systems Administration":
        id = "profile-settings-dialog.personal-information.job-title.list.systems-administration";
        break;
    case "Talent Acquisition":
        id = "profile-settings-dialog.personal-information.job-title.list.talent-acquisition";
        break;
    case "Talent Acquisition":
        id = "profile-settings-dialog.personal-information.job-title.list.talent-acquisition";
        break; 
    case "Teacher":
        id = "profile-settings-dialog.personal-information.job-title.list.teacher";
        break;
    case "Other":
        id ="profile-settings-dialog.personal-information.job-title.list.other";
        break;
    case "SME":
      id = "profile-settings-dialog.personal-information.industry.list.sme";
      break;
    case "SME":
      id = "profile-settings-dialog.personal-information.industry.list.sme";
      break;
    case "Automotive":
      id = "profile-settings-dialog.personal-information.industry.list.automative";
      break;
    case "Business Services":
        id = "profile-settings-dialog.personal-information.industry.list.business-services";
        break;
    case "Education":
          id = "profile-settings-dialog.personal-information.industry.list.education";
          break;
    case "Energy":
       id = "profile-settings-dialog.personal-information.industry.list.energy";
       break;
    case "Energy":
        id = "profile-settings-dialog.personal-information.industry.list.energy";
        break;   
    case "FSI":  
        id = "profile-settings-dialog.personal-information.industry.list.fsi";
        break;
    case "Public Sector":
        id = "profile-settings-dialog.personal-information.industry.list.public-sector";
        break;    
    case "Realty":
        id = "profile-settings-dialog.personal-information.industry.list.realty";
        break;  
    // case "Realty":
    //       id = "profile-settings-dialog.personal-information.industry.list.realty";
    //       break;  
    case "Retail and Manufacturing":
          id = "profile-settings-dialog.personal-information.industry.list.retail-manufacturing";
          break;   
    case "Telecom":
        id ="profile-settings-dialog.personal-information.industry.list.telecom";
        break;    
   }
   return id == "" ? value : this.props.intl.formatMessage({id : id, defaultMessage: value});
  };
  render() {
    const customStyles = {
      option: base => ({
        ...base,
        fontSize: "16px",
      }),
      control: (base, state) => ({
        ...base,
        background: "transparent",
        padding: 0,
        fontSize: "12px",
        minHeight: 41,
        borderRadius: 4,
        borderColor: theme.palette.border.lightBorder,
        boxShadow: "none",
        ":focus": {
          border: `1px solid ${theme.palette.border.darkBorder}`,
        },
        ":hover": {
          border: `1px solid ${theme.palette.border.darkBorder}`,
        },
      }),
      input: (base, state) => ({
        color: theme.palette.text.secondary,
      }),
      valueContainer: (base, state) => ({
        ...base,
        padding: "0 0 0 14px",
      }),
      multiValue: (base, state) => ({
        ...base,
        background: theme.palette.background.light,
        padding: "5px 5px",
        borderRadius: 20,
      }),
      multiValueLabel: (base, state) => ({
        ...base,
        marginRight: 10,
        //background: "red"
      }),
      multiValueRemove: (base, state) => ({
        ...base,
        background: theme.palette.background.contrast,
        borderRadius: "50%",
        color: theme.palette.common.white,
        ":hover": {
          background: theme.palette.error.light,
          color: theme.palette.common.white,
        },
      }),
    };
    const DropdownIndicator = (props) => {
      return (
        components.DropdownIndicator && (
          <components.DropdownIndicator {...props}>
            <DropdownArrow htmlColor={theme.palette.secondary.light} />
          </components.DropdownIndicator>
        )
      );
    };
    const {
      fullName,
      fullNameError,
      fullNameErrorMessage,

      companyIndustry,
      otherIndustry,
      otherCompanyIndustry,
      jobRole,
      otherJobRole,
      otherJobRoleSelected,
      country,
      phoneNumber,
      phoneNumberError,
      phoneNumberErrorMessage,
      profileModified,
      otherJobRoleError,
      otherJobRolerErrorMessage,
      otherCompanyIndustryError,
      otherCompanyIndustryErrorMessage,
      btnQuery,
      logoutDialogueOpen,
      logoutBtnQuery,
    } = this.state;
    const { classes, theme, profileState } = this.props;
    const intl = this.props.intl;
    // const intl = useIntl();
    // const {intl} = this.props;
    const hideBtn = {
      display: "none",
    };
    const { userName, email } = profileState.data;
    let uName = profileState.data.profile.fullName;
    // let u =   {<FormattedMessage id="profile-settings-dialog.personal-information.form.phone-number.label"  defaultMessage="message with email: {email}">

    // </FormattedMessage>};

    return (
      <>
        <Grid
          container
          direction="row"
          justify="flex-start"
          alignItems="center"
          classes={{ container: classes.personalInfoTopCnt }}
        >
          <ActionConfirmation
            open={logoutDialogueOpen}
            closeAction={this.closeLogoutDialogue}
            cancelBtnText={
              <FormattedMessage
                id="common.action.delete.confirmation.cancel-button.label"
                defaultMessage="Cancel"
              />
            }
            successBtnText={
              <FormattedMessage
                id="meeting.confirmations-dialog.publish-mom.button-yes.label"
                defaultMessage="Yes"
              />
            }
            alignment="center"
            iconType="signOutIcon"
            headingText={
              <FormattedMessage
                id="common.logout.title"
                defaultMessage="Logout"
              />
            }
            msgText={
              <FormattedMessage
                id="common.logout.action"
                defaultMessage="Are you sure you want to logout from all devices?"
              />
            }
            successAction={this.handleLogoutFromAllDevices}
            btnQuery={logoutBtnQuery}
          />
          <div className={classes.profilePicCnt}>
            <div className={classes.profilePic}>
              <CustomAvatar personal size="large" hideBadge={true} />
              {this.props.profileState.data.imageUrl ? (
                <DeleteIcon
                  htmlColor={theme.palette.error.main}
                  classes={{ root: classes.deleteProfileImageIcon }}
                  onClick={this.removeProfilePicture}
                />
              ) : null}
            </div>
            <p className={classes.uploadPhotoBtn} onClick={this.clickFile}>
              <FormattedMessage
                id="profile-settings-dialog.upload-photo.label"
                defaultMessage="Upload Photo"
              ></FormattedMessage>
            </p>
            <input
              style={hideBtn}
              className="fileInput"
              type="file"
              id="profileImageUpload"
              onChange={this._handleImageChange}
              accept={"png, jpeg, jpg, gif"}
            />
          </div>
          <div>
            <ul className={classes.profileInfoList}>
              <li>
                <img
                  src={Icons.PersonIconLight}
                  width={18}
                  height={18}
                  className={classes.profileInfoIcon}
                />
                {uName}
              </li>
              <li>
                <img
                  src={Icons.EnvelopIconLight}
                  width={18}
                  height={18}
                  className={classes.profileInfoIcon}
                />
                {this.props.profileState.data.email}
              </li>
              <li>
                <img
                  src={Icons.ActivityIcon}
                  width={18}
                  height={18}
                  className={classes.profileInfoIcon}
                />
                <FormattedMessage
                  id="profile-settings-dialog.last-login.label"
                  defaultMessage="Last Login:"
                ></FormattedMessage>
                &nbsp;
                <FormattedDate
                  value={this.props.profileState.data.lastLoginTime}
                  month="short"
                  day="numeric"
                  year="numeric"
                  hour="numeric"
                  minute="numeric"
                />
              </li>
              <li>
                <SignOutIcon
                  htmlColor={theme.palette.secondary.light}
                  classes={{ root: classes.plainMenuItemIcon }}
                  style={{ fontSize: "18px" }}
                />
                <p
                  className={classes.logoutAllDevicesText}
                  onClick={this.openLogoutDialogue}
                >
                  <FormattedMessage
                    id="profile-settings-dialog.log-out.label"
                    defaultMessage="Sign out from all devices"
                  ></FormattedMessage>
                </p>
              </li>
            </ul>
          </div>
        </Grid>
        <div className={classes.personalInfoFormCnt}>
          <DefaultTextField
            label={
              <FormattedMessage
                id="profile-settings-dialog.personal-information.form.full-name.label"
                defaultMessage="Full Name"
              ></FormattedMessage>
            }
            fullWidth={true}
            errorState={fullNameError}
            errorMessage={fullNameErrorMessage}
            defaultProps={{
              id: "fullName",
              onChange: this.handleInput("fullName"),
              value: fullName,
              placeholder: "Full Name",
              autoFocus: true,
              inputProps: { maxLength: 40 },
            }}
          />

          {/* <DefaultTextField
            label="Last Name"
            fullWidth={true}
            errorState={lastNameError}
            errorMessage={lastNameErrorMessage}
            defaultProps={{
              id: "lastName",
              value: lastName,
              onChange: this.handleInput("lastName"),
              placeholder: "Last name",
              inputProps: { maxLength: 40 }
            }}
          /> */}

          <FormControl
            fullWidth={true}
            classes={{ root: classes.reactSelectFormControl }}
          >
            <InputLabel
              htmlFor="jobRole"
              classes={{
                root: classes.selectLabel,
              }}
              shrink={false}
            >
              <FormattedMessage
                id="profile-settings-dialog.personal-information.form.job-title.label"
                defaultMessage="Job Title"
              ></FormattedMessage>
            </InputLabel>
            <Select
              styles={customStyles}
              onChange={this.handleJobRoleSelect}
              inputId="jobRole"
              options={this.state.jobList.map((job) => {return {label: this.translate(job.label), value: job.value}})}
              value={jobRole}
              components={{ DropdownIndicator, IndicatorSeparator: false }}
            />
          </FormControl>
          {otherJobRoleSelected ? (
            <DefaultTextField
              label={
                <FormattedMessage
                  id="profile-settings-dialog.personal-information.form.specify-job-title.label"
                  defaultMessage="Please specify your job title"
                ></FormattedMessage>
              }
              fullWidth={true}
              errorState={otherJobRoleError}
              errorMessage={otherJobRolerErrorMessage}
              defaultProps={{
                id: "otherJobRole",
                onChange: this.handleInput("otherJobRole"),
                value: otherJobRole,
                inputProps: { maxLength: 80 },
              }}
            />
          ) : null}

          <FormControl
            fullWidth={true}
            classes={{ root: classes.reactSelectFormControl }}
          >
            <InputLabel
              htmlFor="jobTitle"
              classes={{
                root: classes.selectLabel,
              }}
              shrink={false}
            >
              <FormattedMessage
                id="profile-settings-dialog.personal-information.form.companys-industry.label"
                defaultMessage="Company's Industry"
              ></FormattedMessage>{" "}
            </InputLabel>
            <Select
              styles={customStyles}
              onChange={this.handleIndustrySelect}
              inputId="companyIndustry"
              options={this.state.industryList.map((ind) => {return {label: this.translate(ind.label), value: ind.value}})}
              value={companyIndustry}
              components={{ DropdownIndicator, IndicatorSeparator: false }}
            />
          </FormControl>
          {otherIndustry ? (
            <DefaultTextField
              label="Please specify your industry"
              fullWidth={true}
              errorState={otherCompanyIndustryError}
              errorMessage={otherCompanyIndustryErrorMessage}
              defaultProps={{
                id: "otherCompanyIndustry",
                onChange: this.handleInput("otherCompanyIndustry"),
                value: otherCompanyIndustry,
                inputProps: { maxLength: 80 },
              }}
            />
          ) : null}

          <FormControl
            fullWidth={true}
            classes={{ root: classes.reactSelectFormControl }}
          >
            <InputLabel
              htmlFor="country"
              classes={{
                root: classes.selectLabel,
              }}
              shrink={false}
            >
              <FormattedMessage
                id="profile-settings-dialog.personal-information.form.country.label"
                defaultMessage="Country"
              ></FormattedMessage>
            </InputLabel>
            <Select
              styles={customStyles}
              onChange={this.handleCountryChange}
              inputId="country"
              options={this.state.countryList}
              components={{ DropdownIndicator, IndicatorSeparator: false }}
              value={country}
            />
          </FormControl>
          <DefaultTextField
            label={
              <FormattedMessage
                id="profile-settings-dialog.personal-information.form.phone-number.label"
                defaultMessage="Phone Number"
              ></FormattedMessage>
            }
            fullWidth={true}
            errorState={phoneNumberError}
            errorMessage={phoneNumberErrorMessage}
            defaultProps={{
              id: "phoneNumber",
              onChange: this.handleInput("phoneNumber"),
              value: phoneNumber,
              placeholder: intl.formatMessage({
                id:
                  "profile-settings-dialog.personal-information.form.phone-number.placeholder",
                defaultMessage: "Phone number",
              }),
              inputProps: { maxLength: 25 },
              // inputComponent: PhoneNumberMask,
            }}
          />
        </div>

        <Grid
          container
          direction="row"
          justify="flex-end"
          alignItems="center"
          classes={{ container: classes.profileDialogActionBtns }}
        >
          <DefaultButton
            text={
              <FormattedMessage
                id="common.action.cancel.label"
                defaultMessage="Cancel"
              ></FormattedMessage>
            }
            buttonType="Transparent"
            onClick={this.handleClose}
          />
          <CustomButton
            onClick={this.handleSaveChanges}
            btnType="success"
            variant="contained"
            query={btnQuery}
            disabled={profileModified ? btnQuery == "progress" : true}
          >
            {
              <FormattedMessage
                id="profile-settings-dialog.personal-information.form.save-button.label"
                defaultMessage="Save Changes"
              ></FormattedMessage>
            }
          </CustomButton>
        </Grid>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    constantsState: state.constants,
    profileState: state.profile,
  };
};

PersonalInformation = injectIntl(PersonalInformation, { withRef: true });
// export default PersonalInformation;
export default compose(
  withStyles(combineStyles(profileSetting, autoCompleteStyles), {
    withTheme: true,
  }),
  withRouter,
  connect(mapStateToProps, {
    uploadprofileimage,
    SaveLocalization,
    UpdateUserProfile,
    SaveProfile,
    RemoveProfileImage,
    UpdateProfileImage,
    Logout,
  })
)(PersonalInformation);
