import React, { Component } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import CloseIcon from "@material-ui/icons/Close";
import IconButton from "../../components/Buttons/IconButton";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import profileSetting from "./styles";
import { withStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import PersonalInformation from "./PersonalInformation";
import AccountPreferences from "./AccountPreferences";
import SignInSecurity from "./SignInAndSecurity";
import AppIntegration from "./AppsIntegration";
import NotificationPreferences from "./NotificationPreferences";
import MoveWorkspaceData from "../../components/Dialog/NewWorkspaceDialog/MoveWorkspaceData";
import translate from "../../i18n/translate";
import { FormattedMessage } from "react-intl";
import { Scrollbars } from "react-custom-scrollbars";

class ProfileSetting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: this.props.selectedIndex ? this.props.selectedIndex : 0,
    };
  }

  handleListItemClick = (event, index) => {
    this.setState({ selectedIndex: index });
  };

  render() {
    const { classes, theme, closeAction, showSnackBar, googleCalendar, zoom } = this.props;
    // const { selectedIndex } = this.state;
    const selectedIndex =
      this.state.selectedIndex == 0
        ? googleCalendar.googleIntegration || zoom
          ? 4
          : this.state.selectedIndex
        : this.state.selectedIndex;

    return (
      <Dialog
        classes={{
          paper: classes.dialogPaperCnt,
          scrollBody: classes.dialogCnt,
        }}
        aria-labelledby="form-dialog-title"
        fullWidth={true}
        open={this.props.open}
        scroll="body"
        onClose={closeAction}
        disableBackdropClick={true}
        disableEscapeKeyDown={true}>
        <DialogTitle id="form-dialog-title" classes={{ root: classes.defaultDialogTitle }}>
          <Grid container direction="row" justify="space-between" alignItems="center">
            <Grid item>
              <Typography variant="h2">
                {" "}
                <FormattedMessage
                  id="profile-settings-dialog.title"
                  defaultMessage="Profile Settings"
                />
              </Typography>
            </Grid>
            <Grid item>
              <IconButton btnType="transparent" onClick={this.props.closeAction}>
                <CloseIcon htmlColor={theme.palette.secondary.medDark} />
              </IconButton>
            </Grid>
          </Grid>
        </DialogTitle>
        <DialogContent classes={{ root: classes.defaultDialogContent }}>
          <Grid container direction="row" justify="flex-start" alignItems="stretch">
            <Grid item xs={4} classes={{ item: classes.profileSettingListCnt }}>
              <List component="nav" classes={{ root: classes.profileSettingSideList }}>
                <ListItem
                  button
                  selected={selectedIndex === 0}
                  onClick={event => this.handleListItemClick(event, 0)}
                  classes={{
                    root: classes.listItem,
                    selected: classes.listItemSelected,
                  }}>
                  <ListItemText
                    primary={
                      <FormattedMessage
                        id="profile-settings-dialog.personal-information.title"
                        defaultMessage="Personal Information"></FormattedMessage>
                    }
                    classes={{ primary: classes.profileSideListText }}
                  />
                </ListItem>
                <ListItem
                  button
                  selected={selectedIndex === 1}
                  onClick={event => this.handleListItemClick(event, 1)}
                  classes={{
                    root: classes.listItem,
                    selected: classes.listItemSelected,
                  }}>
                  <ListItemText
                    primary={
                      <FormattedMessage
                        id="profile-settings-dialog.account-preferences.title"
                        defaultMessage="Account Preferences"></FormattedMessage>
                    }
                    classes={{ primary: classes.profileSideListText }}
                  />
                </ListItem>
                <ListItem
                  button
                  selected={selectedIndex === 2}
                  onClick={event => this.handleListItemClick(event, 2)}
                  classes={{
                    root: classes.listItem,
                    selected: classes.listItemSelected,
                  }}>
                  <ListItemText
                    primary={
                      <FormattedMessage
                        id="profile-settings-dialog.notification-preferences.title"
                        defaultMessage="Notification Preferences"></FormattedMessage>
                    }
                    classes={{ primary: classes.profileSideListText }}
                  />
                </ListItem>
                <ListItem
                  button
                  selected={selectedIndex === 3}
                  onClick={event => this.handleListItemClick(event, 3)}
                  classes={{
                    root: classes.listItem,
                    selected: classes.listItemSelected,
                  }}>
                  <ListItemText
                    primary={
                      <FormattedMessage
                        id="profile-settings-dialog.signin-security.title"
                        defaultMessage="Sign In & Security"></FormattedMessage>
                    }
                    classes={{ primary: classes.profileSideListText }}
                  />
                </ListItem>

                <ListItem
                  button
                  selected={selectedIndex === 4}
                  onClick={event => this.handleListItemClick(event, 4)}
                  classes={{
                    root: classes.listItem,
                    selected: classes.listItemSelected,
                  }}>
                  <ListItemText
                    primary={
                      <FormattedMessage
                        id="profile-settings-dialog.apps-integrations.title"
                        defaultMessage="Apps & integrations"></FormattedMessage>
                    }
                    classes={{ primary: classes.profileSideListText }}
                  />
                </ListItem>
              </List>
            </Grid>

            <Grid item xs={8} classes={{ item: classes.profileSettingContentCnt }}>
              <Scrollbars autoHide={true} autoHeight autoHeightMin={700} autoHeightMax={700}>
                {selectedIndex === 0 ? (
                  <PersonalInformation onDialogueClose={closeAction} showSnackBar={showSnackBar} />
                ) : selectedIndex === 1 ? (
                  <AccountPreferences
                    classes={classes}
                    theme={theme}
                    onDialogueClose={closeAction}
                    showSnackBar={showSnackBar}
                  />
                ) : selectedIndex === 2 ? (
                  <NotificationPreferences
                    classes={classes}
                    theme={theme}
                    onDialogueClose={closeAction}
                    showSnackBar={showSnackBar}
                  />
                ) : selectedIndex === 3 ? (
                  <SignInSecurity
                    classes={classes}
                    theme={theme}
                    onDialogueClose={closeAction}
                    showSnackBar={showSnackBar}
                  />
                ) : selectedIndex === 4 ? (
                  <AppIntegration
                    classes={classes}
                    theme={theme}
                    onDialogueClose={closeAction}
                    showSnackBar={showSnackBar}
                  />
                ) : null}
              </Scrollbars>
            </Grid>
          </Grid>
        </DialogContent>
      </Dialog>
    );
  }
}
ProfileSetting.defaultProps = {
  googleCalendar: {},
};

export default withStyles(profileSetting, { withTheme: true })(ProfileSetting);
