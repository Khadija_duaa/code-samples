const listStyle = (theme) => ({
  listItemWrap: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "flex-start",
    borderBottom: `1px solid ${theme.palette.border.grayLighter}`,
    padding: `10px 0`,
  },
  listItemLeft: {
    flex: 1,
  },
  listItemStopSyncWrap: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: `10px 0`,
  },
  accountSubTitle: {
    color: theme.palette.common.primary,
    margin: 0,
    marginRight: `14px !important`,
  },
  copyURL: {
    color: theme.palette.background.btnBlue,
    textDecoration: "underline",
    margin: 0,
    "&:hover": {
      cursor: "pointer",
    },
  },
  titleText: {
    color: theme.palette.common.black,
    margin: `0 0 2px 0`,
  },
  iconWrap: {
    display: "flex",
    alignItems: "center",
  },
  deleteIcon: {
    fontSize: "20px !important",
    color: theme.palette.text.grayDarker,
    marginRight: 16,
    cursor: "pointer",
    "&:hover": {
      color: theme.palette.background.danger,
    },
  },
  settingIcon: {
    fontSize: "20px !important",
    marginRight: 4,
    color: theme.palette.text.grayDarker,
    cursor: "pointer",
  },
  listItemStopSyncBtns: {
    display: "flex",
    alignItems: "center",
  },
});

export default listStyle;
