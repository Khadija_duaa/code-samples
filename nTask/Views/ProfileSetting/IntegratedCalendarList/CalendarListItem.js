import React, { useState } from "react";
import Typography from "@material-ui/core/Typography";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import CalendarListStyles from "./styles";
import CustomButton from "../../../components/Buttons/CustomButton";
import SvgIcon from "@material-ui/core/SvgIcon";
import DefaultSwitch from "../../../components/Form/Switch";
import DeleteListIcon from "../../../components/Icons/DeleteListIcon";
import SettingListIcon from "../../../components/Icons/SettingListIcon";
import Truncate from "react-truncate";
import { FormattedMessage } from "react-intl";

function CalendarListItem(props) {
  const {
    classes,
    updateSync,
    urlText,
    data,
    stopSync,
    deleteCalendar,
    theme,
  } = props;

  const [isListView, setListView] = useState(true);
  const [isStopSync, setStopSync] = useState(false);

  const [isDeleteSync, setDeleteSync] = useState(false);
  const [disableSyncBtnQuery, setDisableSyncBtnQuery] = useState("");
  const [deleteBtnQuery, setDeleteBtnQuery] = useState("");
  const [enableSyncBtnQuery, setEnableSyncBtnQuery] = useState("");
  const [editedItem, setEditedItem] = useState({});

  const toggleSyncHandler = (i) => {
    setEditedItem(i);
    setStopSync((prevState) => !prevState);
    setListView((prevState) => !prevState);
  };

  const deleteSyncHandler = (i) => {
    setEditedItem(i);
    setStopSync(false);
    setListView(false);
    setDeleteSync((prevState) => !prevState);
  };

  const deleteCal = (item) => {
    setDeleteBtnQuery("progress");
    deleteCalendar(
      item,
      (succ) => {
        setListView(true);
        setStopSync(false);
        setDeleteSync(false);
        setDeleteBtnQuery("");
      },
      (fail) => {
        setDeleteBtnQuery("");
      }
    );
  };

  const cancelSyncHandler = () => {
    setListView(true);
    setStopSync(false);
    setDeleteSync(false);
  };

  const disableSyncing = (item) => {
    setDisableSyncBtnQuery("progress");
    stopSync(item, (succCallback) => {
      setListView(true);
      setStopSync(false);
      setDeleteSync(false);
      setDisableSyncBtnQuery("");
    });
  };

  const enableSyncing = (item) => {
    setEnableSyncBtnQuery("progress");
    stopSync(item, (succCallback) => {
      setListView(true);
      setStopSync(false);
      setDeleteSync(false);
      setEnableSyncBtnQuery("");
    });
  };

  return (
    <>
      {isListView && (
        <div className={classes.listItemWrap} key={data.code}>
          <div className={classes.listItemLeft}>
            <Typography variant="body2" className={classes.accountSubTitle}>
              <FormattedMessage
                id="profile-settings-dialog.apps-integrations.common.subscription.label"
                defaultMessage="Calendar Subscription URL:"
              />
            </Typography>
            <Typography
              title={`${ENV == "production1" || ENV == "beta1"  ? BASE_URL_API : BASE_URL }api/Calendar/GetCalendarAsync?fileName=${data.code}.ics`}
              variant="h5"
              className={classes.titleText}
            >
              <Truncate
                trimWhitespace={true}
                width={300}
                ellipsis={<span>...</span>}
              >
                {/* {data.code.split("/").pop()} */}
                {`${ENV == "production1" || ENV == "beta1"  ? BASE_URL_API : BASE_URL }api/Calendar/GetCalendarAsync?fileName=${data.code}.ics`}
              </Truncate>
            </Typography>
            <Typography
              variant="h5"
              className={classes.copyURL}
              onClick={() => {
                navigator.clipboard.writeText(
                  `${ENV == "production1" || ENV == "beta1"  ? BASE_URL_API : BASE_URL }api/Calendar/GetCalendarAsync?fileName=${data.code}.ics`
                );
              }}
            >
              <FormattedMessage
                id="profile-settings-dialog.apps-integrations.apple-calendar.copy-calender.title"
                defaultMessage="Copy URL"
              />
            </Typography>
          </div>
          <div className={classes.listItemRight}>
            <div className={classes.iconWrap}>
              {/* Delete Icon */}
              <SvgIcon
                viewBox="0 0 18 20.001"
                className={classes.deleteIcon}
                onClick={() => {
                  deleteSyncHandler(data);
                }}
              >
                <DeleteListIcon />
              </SvgIcon>

              {/* Setting Icon */}
              {data.syncEnabled && (
                <SvgIcon
                  viewBox="0 0 20 19.999"
                  className={classes.settingIcon}
                  onClick={() => {
                    updateSync(data);
                  }}
                >
                  <SettingListIcon />
                </SvgIcon>
              )}

              <DefaultSwitch
                checked={data.syncEnabled}
                onChange={() => {
                  toggleSyncHandler(data);
                }}
                color={data.syncEnabled ? undefined : "Dark"}
              />
            </div>
          </div>
        </div>
      )}

      {/* Stop sync View */}
      {isStopSync ? (
        <div className={classes.listItemStopSyncWrap} key={data.code}>
          <div className={classes.listItemLeft}>
            <Typography variant="body2" className={classes.accountSubTitle}>
              {" "}
              Are you sure you want to {data.syncEnabled
                ? "stop "
                : "enable"}{" "}
              syncing the calendar?
            </Typography>
          </div>
          <div className={classes.listItemRight}>
            <div className={classes.listItemStopSyncBtns}>
              <CustomButton
                onClick={cancelSyncHandler}
                variant="contained"
                disabled={false}
                btnType={"gray"}
                style={{ padding: `5px`, fontSize: "12px" }}
              >
                <FormattedMessage
                  id="common.action.cancel.label"
                  defaultMessage="Cancel"
                />
              </CustomButton>
              &nbsp;
              <CustomButton
                onClick={() => {
                  data.syncEnabled ? disableSyncing(data) : enableSyncing(data);
                }}
                variant="contained"
                disabled={false}
                btnType={data.syncEnabled ? "danger" : "success"}
                style={{ padding: `5px 9px`, fontSize: "12px" }}
                query={
                  data.syncEnabled ? disableSyncBtnQuery : enableSyncBtnQuery
                }
              >
                {data.syncEnabled ? (
                  <FormattedMessage
                    id="profile-settings-dialog.apps-integrations.common.confirmation.stop-button.label"
                    defaultMessage="Stop Sync"
                  />
                ) : (
                  <FormattedMessage
                    id="profile-settings-dialog.apps-integrations.outlook.confirmation.enable-button.label"
                    defaultMessage="Enable"
                  />
                )}
              </CustomButton>
            </div>
          </div>
        </div>
      ) : null}

      {/* Delete sync View */}
      {isDeleteSync ? (
        <div className={classes.listItemStopSyncWrap} key={data.code}>
          <div className={classes.listItemLeft}>
            <Typography variant="body2" className={classes.accountSubTitle}>
              <FormattedMessage
                id="profile-settings-dialog.apps-integrations.common.delete.action"
                defaultMessage="Are you sure you want to delete the calendar?"
              />
            </Typography>
          </div>
          <div className={classes.listItemRight}>
            <div className={classes.listItemStopSyncBtns}>
              <CustomButton
                onClick={cancelSyncHandler}
                variant="contained"
                disabled={false}
                btnType={"gray"}
                style={{ padding: `5px`, fontSize: "12px" }}
              >
                <FormattedMessage
                  id="common.action.cancel.label"
                  defaultMessage="Cancel"
                />
              </CustomButton>
              &nbsp;
              <CustomButton
                onClick={() => {
                  deleteCal(data);
                }}
                variant="contained"
                disabled={false}
                btnType={"danger"}
                style={{ padding: `5px`, fontSize: "12px" }}
                query={deleteBtnQuery}
              >
                <FormattedMessage
                  id="common.action.delete.label"
                  defaultMessage="Delete"
                />
              </CustomButton>
            </div>
          </div>
        </div>
      ) : null}
    </>
  );
}

export default withStyles(CalendarListStyles, { withTheme: true })(
  CalendarListItem
);
