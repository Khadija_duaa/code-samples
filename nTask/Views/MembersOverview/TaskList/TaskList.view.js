import React, { useContext, useEffect, useState } from "react";
import CustomTable from "../MembersOverviewTable/CustomTable.cmp";
import MembersOverviewContext from "../Context/membersOverview.context";
import { dispatchTaskGroupByAssignee } from "../Context/actions";
import { grid } from "../../../components/CustomTable2/gridInstance";
import { exportBulkIssue } from "../../../redux/actions/issues";
import fileDownload from "js-file-download";
function TaskList(props) {
  const {
    state: { widgetSettings, taskWidgetData, filteredTasks, groupedTasks },
    dispatch,
  } = useContext(MembersOverviewContext);
  //Getting selected columns
  const { selectedColumns } = widgetSettings;
  const [taskData, setTaskData] = useState([]);
  //Getting task data based upon filter
  const getTasksData = () => {
    const { selectedFilter } = widgetSettings;
    if (groupedTasks.length) {
      return groupedTasks;
    }
    if (selectedFilter === "viewAllTasks") {
      return taskWidgetData.userTasks;
    }
    return filteredTasks[selectedFilter];
  };
  useEffect(() => {
    setTaskData(getTasksData());
  }, [groupedTasks]);

  useEffect(() => {
    const { selectedFilter } = widgetSettings;
    if (widgetSettings.groupBy.includes("assignee")) {
      const tasks = selectedFilter == "viewAllTasks" ?  taskWidgetData.userTasks : filteredTasks[selectedFilter]
      dispatchTaskGroupByAssignee(dispatch, tasks);
    } else {
    setTaskData(getTasksData());
    }
  }, [taskWidgetData.userTasks, widgetSettings.selectedFilter, widgetSettings.filteredTasks]);
  return (
    <CustomTable
      data={taskData}
      cols={selectedColumns}
      dispatch={dispatch}
      widgetSettings={widgetSettings}
      isLoading={props.isLoading}
    />
  );
}

export default TaskList;
