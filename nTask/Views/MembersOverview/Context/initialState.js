const initialState = {
    widgetSettings: {selectedFilter: 'viewAllTasks', selectedTaskStatus: [],selectedDelaySpan: [], groupBy: [], selectedWorkspaces: [],selectedMembers: [], subTasks: false, selectedColumns : ['taskTitle', 'status', 'plannedEndDate', 'priority', 'progress', 'assignee']},
    taskWidgetData: {userTasks: [], issues: [], risks: [],projects:[]},
    filteredTasks : {
        assignedToMe: [],
        dueToday: [],
        dueInFiveDays: [],
        overDueTasks: [],
        unscheduledTasks: [],
        },  
        groupedTasks: []
};

export default initialState;