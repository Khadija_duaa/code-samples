import { SET_WIDGET_SETTINGS, SET_GROUPBY_ASSIGNEE, SET_WIDGET_TASK_DATA } from "./constants";

export const dispatchWidgetSetting = (dispatch, obj) => {
  dispatch({ type: SET_WIDGET_SETTINGS, payload: obj });
};
export const dispatchTaskWidgetData = (dispatch, obj) => {
  dispatch({ type: SET_WIDGET_TASK_DATA, payload: obj });
};
export const dispatchTaskGroupByAssignee = (dispatch, data) => {
  dispatch({ type: SET_GROUPBY_ASSIGNEE, payload: data });
};
