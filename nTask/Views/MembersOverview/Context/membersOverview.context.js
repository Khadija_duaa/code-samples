import { createContext } from "react";

const MembersOverviewContext = createContext({});

export default MembersOverviewContext;
