import React, { useContext, useEffect, useState } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import chartStyles from "./Chart.style";
import Grid from "@material-ui/core/Grid";
import SvgIcon from "@material-ui/core/SvgIcon";
import MembersOverviewContext from "../Context/membersOverview.context";

import IconDonut from "../../../components/Icons/IconDonut";
import { ResponsivePie } from "@nivo/pie";
import { dispatchWidgetSetting } from "../Context/actions";
import { updateWidgetSetting } from "../../../redux/actions/overviews";
import { CanAccessFeature } from "../../../components/AccessFeature/AccessFeature.cmp";
const statuses = [
  {
    id: "Not Started",
    lable: "Not Started",
    statusId: 0,
  },
  {
    id: "In Progress",
    lable: "In Progress",
    statusId: 1,
  },
  {
    id: "On Hold",
    lable: "On Hold",
    statusId: 2,
  },
  {
    id: "Completed",
    lable: "Completed",
    statusId: 3,
  },
  {
    id: "Cancelled",
    lable: "Cancelled",
    statusId: 4,
  },
];

function Charts({ classes }) {
  const {
    state: { taskWidgetData, widgetSettings },
    dispatch,
  } = useContext(MembersOverviewContext);

  const [projects, setProjects] = useState([]);
  const [tasks, setTasks] = useState([]);
  const [totalTasks, setTotalTasks] = useState([]);
  const [delayedTasks, setDelayedTasks] = useState([]);
  const [delayedProjects, setDelayedProjects] = useState([]);

  const getProjectByTask = () => {
    let projectData = [];
    projectData = statuses.map(s => {
      return {
        id: s.id,
        label: s.label,
        value: taskWidgetData.projects && taskWidgetData.projects.filter(p => p.projectStatus === s.statusId).length,
      };
    });
    setProjects(projectData);
  };
  const getTaskByStatus = () => {
    const tasks = taskWidgetData.userTasks;
    let uniqueStatuses = filterUniqueStatuses(tasks);
    let taskStatusData = uniqueStatuses.filter(item => item.statusTitle).map(us => {
      return {
        id: `${us.workspaceName}: ${us.statusTitle}`,
        label: us.statusTitle,
        value: tasks.filter(ele => ele.statusId === us.statusId).length,
        color: us.statusColor,
        statusId: us.statusId,
      };
    });
    setTasks(taskStatusData);
    setTotalTasks(tasks.length);
  };

  const getTaskByDelay = () => {
    let delayedTasks = taskWidgetData.userTasks.filter(t => t.delaySpan > 0);
    let onTimeTasks = taskWidgetData.userTasks.filter(t => !t.delaySpan);
    const tasksList = [{
      id: 'On Track',
      label: 'On Track',
      value: onTimeTasks.length,
    }, {
      id: 'Delayed',
      label: 'Delayed',
      value: delayedTasks.length,
    }]
    setDelayedTasks(tasksList);
  };
  const getProjectByDelay = () => {
    let delayedProjects = taskWidgetData.projects.filter(p => p.isProjectDelay);
    let onTimeProjects = taskWidgetData.projects.filter(p => !p.isProjectDelay);

    let projectDelayData = [{
      id: 'On Track',
      label: 'On Track',
      value: onTimeProjects.length,
    },
    {
      id: 'Delayed',
      label: 'Delayed',
      value: delayedProjects.length,
    }]
    setDelayedProjects(projectDelayData);
  };


  const filterUniqueStatuses = items => {
    let uniqueStatuses = items.reduce((res, cv) => {
      let isExist = res.find(r => r.statusId === cv.statusId);
      if (!isExist) res.push(cv);
      return res;
    }, []);
    return uniqueStatuses;
  };
  const handleTaskByStatusClick = (obj) => {

    if (!widgetSettings.selectedTaskStatus.includes(obj.data.statusId)) {
      let statusIdArr = [obj.data.statusId];


      dispatchWidgetSetting(dispatch, { selectedTaskStatus: statusIdArr });
      updateWidgetSetting(
        "memberoverview",
        { ...widgetSettings, selectedTaskStatus: statusIdArr },
        // Success
        res => {
        },
      );
    }
  };
  const handleTaskDelayClick = (obj) => {
    dispatchWidgetSetting(dispatch, { selectedDelaySpan: [obj.label] });
    updateWidgetSetting(
      "memberoverview",
      { ...widgetSettings, selectedDelaySpan: [obj.label] },
      // Success
      res => {
      },
    );
  };
  useEffect(() => {
    getProjectByTask();
    getTaskByStatus();
    getTaskByDelay();
    getProjectByDelay();
  }, [taskWidgetData]);

  const noDelayedTasks = delayedTasks.every(e => e.value == 0);
  const noDelayedProjects = delayedProjects.every(e => e.value == 0);
  const chartArcProps = {
    arcLinkLabelsSkipAngle: 27,
    arcLinkLabelsTextOffset: 5,
    arcLinkLabelsTextColor: "#333333",
    arcLinkLabelsOffset: 15,
    arcLinkLabelsDiagonalLength: 8,
    arcLinkLabelsStraightLength: 10,
    arcLinkLabelsThickness: 2,
    arcLinkLabelsColor: { from: "color" },
    arcLabelsSkipAngle: 19,
    arcLabelsTextColor: { from: "color", modifiers: [["darker", 2]] },
  };
  console.log(tasks)
  return (
    <>
      <Grid
        container
        direction="row"
        // justify="space-between"
        // alignItems="center"
        className={classes.widgetCnt}>
        <Grid item sm={6} md={6} lg={6} className={classes.gridStyle}>
          <div style={{ background: "#2EBAE6" }} className={classes.content}>
            <label className={classes.contentHeading}>Projects</label>
            <label>{`${taskWidgetData.projects ? taskWidgetData.projects.length : 0}`}</label>
            <span className={classes.time}>(Count)</span>
          </div>
        </Grid>
        <Grid item sm={6} md={6} lg={6} className={classes.gridStyle}>
          <div style={{ background: "#29C8A2" }} className={classes.content}>
            <label className={classes.contentHeading}>Tasks</label>
            <label>{`${totalTasks}`}</label>
            <span className={classes.time}>(Count)</span>
          </div>
        </Grid>
      </Grid>
      <Grid container direction="row" justify="space-between" alignItems="center">
        <CanAccessFeature group='project' feature='status'>

          <Grid item sm={12} md={6} lg={3} spacing={2} className={classes.gridStyle}>
            <div className={classes.graphContent}>
              <label className={classes.graphHeading}>Projects By Status</label>
              {projects.length == 0 ? (
                <div className={classes.emptyParentCnt}>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center",
                    }}>
                    <SvgIcon viewBox="0 0 140 140" className={classes.svgIcon}>
                      <IconDonut />
                    </SvgIcon>
                    <span className={classes.noFoundTxt}> No Record Available</span>
                  </div>
                </div>
              ) : (
                <ResponsivePie
                  data={projects}
                  margin={{ top: 40, right: 80, bottom: 80, left: 80 }}
                  innerRadius={0.5}
                  colors={[
                    "#fc7150", // Not Started
                    "#5185fc", // In Progress
                    "#faa948", // On Hold
                    "#4dd778", // Completed
                    "#ff5b5b", // Cancelled
                  ]}
                  padAngle={0}
                  cornerRadius={0}
                  activeOuterRadiusOffset={8}
                  borderWidth={0}
                  borderColor={{
                    from: 'color',
                    modifiers: [
                      [
                        'darker',
                        0
                      ]
                    ]
                  }}
                  arcLinkLabelsSkipAngle={10}
                  arcLinkLabelsTextColor="#333333"
                  arcLinkLabelsThickness={2}
                  arcLinkLabelsColor={{ from: 'color' }}
                  arcLabelsSkipAngle={10}
                  arcLabelsTextColor={{
                    from: 'color',
                    modifiers: [
                      [
                        'darker',
                        2
                      ]
                    ]
                  }}
                />
              )}
            </div>
          </Grid>
        </CanAccessFeature>
        <Grid item sm={12} md={6} lg={3} spacing={2} className={classes.gridStyle}>
          <div className={classes.graphContent}>
            <label className={classes.graphHeading}>
              Projects By Progress
            </label>
            {noDelayedProjects ? (
              <div className={classes.emptyParentCnt}>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                  }}>
                  <SvgIcon viewBox="0 0 140 140" className={classes.svgIcon}>
                    <IconDonut />
                  </SvgIcon>
                  <span className={classes.noFoundTxt}> No Record Available</span>
                </div>
              </div>
            ) : (
              <ResponsivePie
                data={delayedProjects}
                margin={{ top: 40, right: 80, bottom: 80, left: 80 }}
                innerRadius={0.5}
                colors={[
                  "#fc7150", // Not Started
                  "#5185fc", // In Progress
                  "#faa948", // On Hold
                  "#4dd778", // Completed
                  "#ff5b5b", // Cancelled
                ]}
                padAngle={0}
                cornerRadius={0}
                activeOuterRadiusOffset={8}
                borderWidth={0}
                borderColor={{
                  from: 'color',
                  modifiers: [
                    [
                      'darker',
                      0
                    ]
                  ]
                }}
                arcLinkLabelsSkipAngle={10}
                arcLinkLabelsTextColor="#333333"
                arcLinkLabelsThickness={2}
                arcLinkLabelsColor={{ from: 'color' }}
                arcLabelsSkipAngle={10}
                arcLabelsTextColor={{
                  from: 'color',
                  modifiers: [
                    [
                      'darker',
                      2
                    ]
                  ]
                }}
              />
            )}
          </div>
        </Grid>

        <Grid item sm={12} md={6} lg={3} spacing={2} className={classes.gridStyle}>
          <div className={classes.graphContent}>
            <label className={classes.graphHeading}>Tasks By Status</label>
            {tasks.length == 0 ? (
              <div className={classes.emptyParentCnt}>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                  }}>
                  <SvgIcon viewBox="0 0 140 140" className={classes.svgIcon}>
                    <IconDonut />
                  </SvgIcon>
                  <span className={classes.noFoundTxt}> No Record Available</span>
                </div>
              </div>
            ) : (
              <ResponsivePie
                data={tasks}
                margin={{ top: 40, right: 80, bottom: 80, left: 80 }}
                innerRadius={0.5}
                onClick={handleTaskByStatusClick}
                padAngle={0}
                cornerRadius={0}
                activeOuterRadiusOffset={8}
                borderWidth={0}
                borderColor={{
                  from: 'color',
                  modifiers: [
                    [
                      'darker',
                      0
                    ]
                  ]
                }}
                arcLinkLabelsSkipAngle={10}
                arcLinkLabelsTextColor="#333333"
                arcLinkLabelsThickness={2}
                arcLinkLabelsColor={{ from: 'color' }}
                arcLabelsSkipAngle={10}
                arcLabelsTextColor={{
                  from: 'color',
                  modifiers: [
                    [
                      'darker',
                      2
                    ]
                  ]
                }}
              />
            )}
          </div>
        </Grid>
        <Grid item sm={12} md={6} lg={3} spacing={2} className={classes.gridStyle}>
          <div className={classes.graphContent}>
            <label className={classes.graphHeading}>Tasks By Progress </label>
            {noDelayedTasks ? (
              <div className={classes.emptyParentCnt}>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                  }}>
                  <SvgIcon viewBox="0 0 140 140" className={classes.svgIcon}>
                    <IconDonut />
                  </SvgIcon>
                  <span className={classes.noFoundTxt}> No Record Available</span>
                </div>
              </div>
            ) : (
              <ResponsivePie
                data={delayedTasks}
                margin={{ top: 40, right: 80, bottom: 80, left: 80 }}
                innerRadius={0.5}
                onClick={handleTaskDelayClick}
                padAngle={0}
                cornerRadius={0}
                activeOuterRadiusOffset={8}
                borderWidth={0}
                borderColor={{
                  from: 'color',
                  modifiers: [
                    [
                      'darker',
                      0
                    ]
                  ]
                }}
                arcLinkLabelsSkipAngle={10}
                arcLinkLabelsTextColor="#333333"
                arcLinkLabelsThickness={2}
                arcLinkLabelsColor={{ from: 'color' }}
                arcLabelsSkipAngle={10}
                arcLabelsTextColor={{
                  from: 'color',
                  modifiers: [
                    [
                      'darker',
                      2
                    ]
                  ]
                }}
              />
            )}
          </div>
        </Grid>

      </Grid>
    </>
  );
}

export default withStyles(chartStyles, { withTheme: true })(Charts);
