const chartStyles = theme => ({
  widgetCnt: {
    marginTop: 10,
  },
  gridStyle: {
    padding: "5px",
  },
  content: {
    padding: 10,
    background: "#ffffff",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    minHeight: "118px",
    borderRadius: "5px",
    color: "#FFFFFF !important",
    "& label": {
      fontSize: "25px !important",
      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: "400",
      marginBottom: "10px",
      color: "#ffffff",
    },
  },
  contentHeading: {
    textAlign: "center",
    fontFamily: theme.typography.fontFamilyLato,
    // letterSpacing: "0px",
    color: "#FFFFFF",
    opacity: 1,
    fontSize: "20px !important",
    marginBottom: 8,
    // lineHeight: "45px",
  },
  time: {
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: "400",
    color: "#ffffff !important",
    marginBottom: 10,
  },
  graphHeading: {
    // textAlign: "center",
    fontFamily: theme.typography.fontFamilyLato,
    // letterSpacing: "0px",
    color: "#00000",
    // opacity: 1,
    fontSize: "20px !important",
    fontWeight: 600,
    height: 50,
    display: 'block'
    // lineHeight: "45px",
  },
  graphContent: {
    height: "300px",
    background: " #FFFFFF",
    border: "1px solid rgba(221, 221, 221, 1)",
    borderRadius: "5px",
    // opacity: "1",
    // textAlign: "center",
    padding: "10px",
  },

  svgIcon: {
    fontSize: "70px !important",
  },


  emptyParentCnt: {
    width: "100%",
    height: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
  },noFoundTxt: {
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: "600",
    color: "#374649",
    padding: 15,
    fontSize: "18px !important",
  },

  /* Extra small devices (phones, 600px and down) */
  "@media only screen and (max-width: 768px)": {
    content: {
      marginBottom: "15px",
    },
  },
  /* Medium devices (landscape tablets, 768px and up) */
  "@media only screen and (min-width: 768px)": {
    content: {
      marginBottom: "15px",
    },
  },
});

export default chartStyles;
