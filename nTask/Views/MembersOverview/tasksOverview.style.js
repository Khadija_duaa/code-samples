const tasksOverviewStyles = theme => ({
  tasksOverview: {
    background: theme.palette.common.white,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: 8,
    width: "100%",
    padding: "0px 5px 12px 31px",
    "& *": {
      fontFamily: theme.typography.fontFamilyLato,
    },
  },
});

export default tasksOverviewStyles;
