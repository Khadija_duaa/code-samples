import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import React, { useMemo, useCallback, useEffect, useState, useContext } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import CustomSelectDropdown from "../../../components/Dropdown/CustomSelectDropdown/Dropdown";
import CustomMultiSelectDropdown from "../../../components/Dropdown/CustomMultiSelectWithSearchBarDropdown/Dropdown";
import {
  filtersData,
  generateWorkspaceData,
} from "../../../helper/generateTasksOverviewDropdownData";
import taskOverviewHeaderStyles from "./tasksOverviewHeader.style";
import DefaultSwitch from "../../../components/Form/Switch";
import MoreActionDropDown from "./moreActionDropdown/MoreActionDropDown";
import MembersOverviewContext from "../Context/membersOverview.context";
import { getWidget, updateWidgetSetting } from "../../../redux/actions/overviews";
import { dispatchTaskWidgetData, dispatchWidgetSetting } from "../Context/actions";
import { generateAssigneeData } from "../../../helper/generateSelectData";
import CustomButton from "../../../components/Buttons/CustomButton";
import isEqual from 'lodash/isEqual'
function MembersOverviewHeader(props) {
  const {
    classes,
    profileState: { workspace, teamMember, activeTeam },
  } = props;
  const [subTaskChecked, setSubTaskChecked] = useState(false);
  const [isChanged, setIsChanged] = useState(false);
  const {
    state: { widgetSettings, taskWidgetData, filteredTasks },
    dispatch,
  } = useContext(MembersOverviewContext);

  // Handle workspace selection in dropdown
  const onWorkspaceSelect = options => {
    // setSelectedWorkspace(option);
    const workspaceIds = options.map(o => o.obj.teamId);
    dispatchWidgetSetting(dispatch, { selectedWorkspaces: workspaceIds });
    setIsChanged(true)
  };
  // Handle member selection in dropdown
  const onMemberSelected = options => {
    // setSelectedWorkspace(option);
    const memberIds = options.map(o => o.id);
    dispatchWidgetSetting(dispatch, { selectedMembers: memberIds });
    setIsChanged(true)

  };
  const handleSaveWidgetSetting = () => {
    setIsChanged(false)
    updateWidgetSetting(
      "memberoverview",
      { ...widgetSettings },
      // Success
      res => {
        getWidget(
          "memberoverview",
          //Success
          resp => {
            dispatchTaskWidgetData(dispatch, resp.data.entity);
          },
          err => {
            dispatchTaskWidgetData(dispatch, { userTasks: [], issues: [], risks: [] });
          }
        );
      }
    );
  }
  // Handle Filter Select
  const handleFilterSelect = option => {
    dispatchWidgetSetting(dispatch, { selectedFilter: option.id });
    updateWidgetSetting(
      "memberoverview",
      { ...widgetSettings, selectedFilter: option.id },
      // Success
      res => {}
    );
  };
  // Handle subtasks switch check/uncheck
  const handleSubtaskSwitch = () => {
    setSubTaskChecked(prevValue => setSubTaskChecked(!prevValue));
  };
  // Generate workspace dropdown data
  const workspacesDropdownData = useMemo(() => {
    return generateWorkspaceData(workspace);
  }, workspace);

  const selectedFilterOption = filtersData.find(f => f.id === widgetSettings.selectedFilter);

  const selectedWorkspaceOptions = workspacesDropdownData.filter(w => {
    return widgetSettings.selectedWorkspaces.includes(w.obj.teamId);
  });
  const getTotalTasks = () => {
    const { selectedFilter } = widgetSettings;
    if (selectedFilter === "viewAllTasks") {
      return taskWidgetData.userTasks.length;
    }
    return filteredTasks[selectedFilter].length;
  };
  const generateMembersData = useMemo(() => {
    return generateAssigneeData(teamMember)
  }, [teamMember]);

  const getSelectedMembersData = useMemo(() => {
    const filterMembers = teamMember.filter(m => widgetSettings.selectedMembers.includes(m.userId));
    return generateAssigneeData(filterMembers)
  }, [generateMembersData]);
  return (
    <div className={classes.headerCnt}>
      <div className={classes.headerLeftCnt}>
        <Typography variant="h1" className={classes.mainHeading}>
          {activeTeam === "013f092268fb464489129c5ea19b89d3" ? "Members Overview" : "Status Report"}
        </Typography>

        {/*<CustomSelectDropdown*/}
        {/*  options={() => filtersData}*/}
        {/*  option={selectedFilterOption}*/}
        {/*  onSelect={handleFilterSelect}*/}
        {/*  isDashboard={true}*/}
        {/*  height="140px"*/}
        {/*  scrollHeight={180}*/}
        {/*  buttonProps={{*/}
        {/*    variant: "contained",*/}
        {/*    btnType: "white",*/}
        {/*    labelAlign: "left",*/}
        {/*    style: { minWidth: 140, padding: "3px 4px 3px 8px", marginRight: 8 },*/}
        {/*  }}*/}
        {/*  // disabled={!permission.detailsTabs.billingMethode.cando}*/}
        {/*/>*/}


        {/* <div className={classes.subTaskSwitchCnt}>
          <Typography variant="h6" className={classes.subTaskSwitchLabel}>
            Subtasks
          </Typography>

          <DefaultSwitch checked={subTaskChecked} onChange={handleSubtaskSwitch} size="small" />
        </div> */}
      </div>
      <div className={classes.headerRightCnt}>

      <CustomMultiSelectDropdown
        label="Workspaces"
        hideLabel={true}
        options={() => workspacesDropdownData}
        option={selectedWorkspaceOptions}
        onSelect={onWorkspaceSelect}
        scrollHeight={400}
        size={"large"}
        buttonProps={{
          variant: "contained",
          btnType: "white",
          labelAlign: "left",
          style: { width: 250, padding: "3px 4px 3px 8px", height: 35 },
        }}
      />
        <CustomMultiSelectDropdown
          label="Members"
          hideLabel={true}
          options={() => generateMembersData}
          option={getSelectedMembersData}
          onSelect={onMemberSelected}
          scrollHeight={400}
          size={"large"}
          btnCntStyles={{marginRight: 10}}
          buttonProps={{
            variant: "contained",
            btnType: "white",
            labelAlign: "left",
            style: { width: 250, padding: "3px 4px 3px 8px", height: 35, marginLeft: 10 },
          }}
        />
      <CustomButton disabled={!isChanged} variant='contained' btnType={'blue'} onClick={handleSaveWidgetSetting}>
        Apply
      </CustomButton>
      </div>
      {/* <MoreActionDropDown /> */}
    </div>
  );
}
const mapStateToProps = state => {
  return {
    profileState: state.profile.data,
  };
};

export default compose(
  withStyles(taskOverviewHeaderStyles, { withTheme: true }),
  connect(mapStateToProps)
)(MembersOverviewHeader);
