const taskOverviewHeaderStyles = theme => ({
  headerCnt: {
    padding: "11px 10px 11px 16px",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  
  mainHeading: {
      marginRight: 16,
      fontSize: "18px !important"
  },
  subTaskSwitchCnt: {
    display: 'flex',
    alignItems: 'center',
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: 4,
    padding: '5px 0 4px 8px',
    marginLeft: 8,
    width: 100
  },
  subTaskSwitchLabel: {
      fontSize: "12px !important",
  },
  headerLeftCnt: {
      display: 'flex',
      alignItems: 'center'
  },
  headerRightCnt: {
      display: 'flex',
      alignItems: 'center'
  }
});

export default taskOverviewHeaderStyles;
