const overviewStyles = theme => ({
  overviewCnt: {
    // padding: "12px 12px 12px 34px",
    padding: "0px 12px 0px 5px",
    // background: theme.palette.common.white,
    // border: `1px solid ${theme.palette.border.lightBorder}`,
    // borderRadius: 8,
    width: "100%",
    height: "100%",
    overflowX: "clip",
    "& *": {
      fontFamily: theme.typography.fontFamilyLato,
    },
  },
});

export default overviewStyles;
