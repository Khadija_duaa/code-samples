const customTableStyles = (theme) => ({
    TotalAssignee: {
        background: theme.palette.secondary.lightBlue,
        color: theme.palette.secondary.main,
        width: 30,
        height: 30
      },
      customFieldValue:{
        whiteSpace: "nowrap",
        overflow: "hidden",
        textOverflow: "ellipsis",
        display : "inline-block !important"
      }
})

export default customTableStyles;