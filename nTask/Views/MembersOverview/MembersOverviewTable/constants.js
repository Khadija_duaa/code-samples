const generateTaskColumnsData = [
  {
    label: "Workspace",
    value: "workspaceName",
  },
  {
    label: "Project",
    value: "projectName",
  },
  {
    label: "Task Id",
    value: "uniqueId",
  },
  {
    label: "Delay Span",
    value: "delaySpan",
  },
  {
    label: "Status",
    value: "statusTitle",
  },
  {
    label: "Priority",
    value: "priority",
  },
  {
    label: "Planned Start Date",
    value: "startDate",
  },
  {
    label: "Planned End Date",
    value: "dueDate",
  },
  {
    label: "Planned Effort",
    value: "plannedEffort",
  },
  {
    label: "Effort To Date",
    value: "effortToDate",
  },
  {
    label: "Actual Start Date",
    value: "actualStartDate",
  },
  {
    label: "Actual End Date",
    value: "actualDueDate",
  },
  {
    label: "Progress",
    value: "progress",
  },
  {
    label: "Assignee",
    value: "assignee",
  },
  {
    label: "Comments",
    value: "comments",
  },
  {
    label: "Time Logged",
    value: "timeLogged",
  },
  {
    label: "Attachments",
    value: "attachments",
  },
  {
    label: "Meetings",
    value: "meetings",
  },
  {
    label: "Issues",
    value: "issues",
  },
  {
    label: "Risks",
    value: "risks",
  },
  {
    label: "Creation Date",
    value: "creationDate",
  },
  {
    label: "Created By",
    value: "createdBy",
  },
];

export default generateTaskColumnsData;
