import React, { useReducer, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import withStyles from "@material-ui/core/styles/withStyles";
import TasksOverviewHeader from "./TasksOverviewHeader/MembersOverviewHeader.view";
import MembersOverviewContext from "./Context/membersOverview.context";
import reducer from "./Context/reducer";
import initialState from "./Context/initialState";
import { getWidgetSetting, getWidget } from "../../redux/actions/overviews";
import { dispatchWidgetSetting, dispatchTaskWidgetData } from "./Context/actions";
import TaskList from "./TaskList/TaskList.view";
import overviewStyles from "./overview.style";
import ChartsView from "./Charts/Charts.view";
import { calculateContentHeight } from "../../utils/common";
import { FetchWorkspaceInfo } from "../../redux/actions/workspace";


function MembersOverview({ classes }) {
  const [state, dispatch] = useReducer(reducer, initialState);
  const [isLoading, setIsLoading] = useState(true);
  const dispatchFn = useDispatch();

  useEffect(() => {
    getWidgetSetting(
      "memberoverview",
      //success
      res => {
        dispatchWidgetSetting(dispatch, res);
      }
    );
    getWidget(
      "memberoverview",
      //Success
      res => {
        setIsLoading(false);
        dispatchTaskWidgetData(dispatch, res.data.entity);
      },
      err => {
        setIsLoading(false);
        dispatchTaskWidgetData(dispatch, { userTasks: [], issues: [], risks: [] });
      }
    );
    return ()=>{
      if (!window.location.pathname.includes("/reports")) {
        FetchWorkspaceInfo(
          "",
          () => { },
          () => { },
          dispatchFn
        );
      }
    }
  }, []);
  
  return (
    <MembersOverviewContext.Provider value={{ dispatch, state }}>
      <div className={classes.overviewCnt} style={{ height: calculateContentHeight() + 77 }}>
        <TasksOverviewHeader />
        <ChartsView />
        <TaskList isLoading={isLoading} />
      </div>
    </MembersOverviewContext.Provider>
  );
}

export default withStyles(overviewStyles, { withTheme: true })(MembersOverview);
