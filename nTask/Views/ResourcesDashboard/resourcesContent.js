import React, { useMemo, useEffect, useState, useContext, useRef } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import PropTypes from "prop-types";
import styles from "./resources.style.js";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import CustomMultiSelectDropdown from "../../components/Dropdown/CustomMultiSelectWithSearchBarDropdown/Dropdown";
import ntaskLogo from "../../assets/images/svgs/nTaskLogo.svg";
import { ResponsivePie } from "@nivo/pie";
import { ResponsiveBar } from "@nivo/bar";
import ImportExportDD from "../../components/Dropdown/ImportExportDD";
import CustomButton from "../../components/Buttons/CustomButton";
import ImportExport from "@material-ui/icons/ImportExport";
import ProjectOverview from "./ProjectOverview/ProjectOverview.view";
import {
  getWidgetAnalyticsData,
  updateProjectAnalyticsData,
} from "../../redux/actions/resourcesDashboard";
import ProjectOverviewContext from "./ProjectOverview/Context/ProjectOverview.context";
import { generateWorkspaceData } from "../../helper/generateTasksOverviewDropdownData";
import {
  dispatchTaskWidgetData,
  dispatchWidgetSetting,
  dispatchWidgetData,
} from "./ProjectOverview/Context/actions";
import isUndefined from "lodash/isUndefined";
import SvgIcon from "@material-ui/core/SvgIcon";

import IconDonut from "../../components/Icons/IconDonut";
import IconBar from "../../components/Icons/IconBar";
import IconPie from "../../components/Icons/IconPie";

function ResourcesContent(props) {
  const {
    classes,
    profileState: { workspace },
  } = props;
  const {
    state: { widgetSettings, projectWidgetData, filteredTasks },
    dispatch,
  } = useContext(ProjectOverviewContext);
  const {
    projectResource,
    resourceProjects,
    tasksByStatus,
    projectSummary,
    projectsbyProgress,
    effortByProject,
    projectByProjectManagers,
  } = projectWidgetData;

  const {
    projects,
    resources,
    selectedProject,
    selectedResource,
    selectedStatus,
    selectedWorkspaces,
    taskStatus,
  } = widgetSettings.widgetSettings;

  const [donutData, setDonutData] = useState([]);
  const [bardata, setBardata] = useState([]);
  const [pieData, setPieData] = useState([]);
  const [defaultVal, setDefaultVal] = useState([{ label: "Select All", value: "1" }]);
  const [defaultValStatus, setDefaultValStatus] = useState([{ label: "Select All", value: "5" }]);
  const [btnDisabled, setBtnDisabled] = useState(true);
  const [btnQuery, setBtnQuery] = useState("");

  const prevSelectedWorkspaceRef = useRef();
  const prevSelectedStatusRef = useRef();
  const prevSelectedPmRef = useRef();

  const settingTaskStatuses = () => {
    //generating all task status data for the status dropdown menu
    let allStatus = taskStatus.map(ele => {
      return {
        label: ele.title,
        value: ele.statusId,
      };
    });
    return allStatus;
  };

  // Generate tasks status dropdown data
  const taskStatuses = useMemo(() => {
    let statuses = settingTaskStatuses();
    return statuses;
  }, taskStatus);

  // Generate workspace dropdown data
  const workspacesDropdownData = useMemo(() => {
    let activeWorkspaces = workspace.filter(w => w.isActive); /** only show active worksapces  */
    let wd = generateWorkspaceData(activeWorkspaces);
    // return [{ label: "Select All", value: "1", obj: { teamId: "1" } }, ...wd];
    return wd;
  }, workspace);

  const settingProjects = () => {
    //generating all projects data for the project dropdown menu
    let allProjects = projects.map(ele => {
      return {
        label: ele.projectName,
        value: ele.projectId,
      };
    });
    return allProjects;
  };

  // Generate projects dropdown data
  const allProjects = useMemo(() => {
    let projects = settingProjects();
    return projects;
  }, projects);

  //Generate resources Data
  const resourcesDdata = () => {
    let r = [];
    resources.map(p => {
      r.push({
        label: p.fullName,
        value: p.userId,
      });
    });
    return r;
  };

  // fetching selected workspace
  const selectedWorkspaceOptions = workspacesDropdownData.filter(w => {
    return selectedWorkspaces.includes(w.obj.teamId);
  });

  // fetching selected statuses
  const selectedStatusOptions = settingTaskStatuses().filter(s => {
    return selectedStatus.includes(s.value);
  });

  // fetching selected projects
  const selectedProjectOptions = settingProjects().filter(s => {
    return selectedProject.includes(s.value);
  });

  // fetching resources Pms
  const selectedResourcesOptions = resourcesDdata().filter(p => {
    return selectedResource.includes(p.value);
  });

  // Handle workspace selection in dropdown
  const onWorkspaceSelect = options => {
    let selectAll = options.find(o => o.value == "1") || false;
    let workspaceIds = options.map(o => o.obj.teamId);
    if (selectAll) {
      if (selectAll && options.length > 1) {
        if (selectedWorkspaces.length > 0 && selectAll) {
          workspaceIds = [];
        } else {
          workspaceIds = workspaceIds.filter(w => w !== "1");
        }
      } else {
        workspaceIds = [];
      }
    }

    let object = {
      ...widgetSettings,
      widgetSettings: {
        ...widgetSettings.widgetSettings,
        selectedWorkspaces: workspaceIds,
      },
    };
    dispatchWidgetSetting(dispatch, object);
    // updateProjectAnalyticsData(
    //   object,
    //   // Success
    //   res => {
    //     dispatchWidgetSetting(dispatch, res);
    //     getWidgetAnalyticsData("", res => {
    //       dispatchWidgetData(dispatch, res.data.entity);
    //     });
    //   }
    // );
  };

  // Handle status selection in dropdown
  const onStatusSelect = options => {
    let selectAll = options.find(o => o.value == "5") || false;
    let statusIds = options.map(o => o.value);
    if (selectAll) {
      if (selectAll && options.length > 1) {
        if (selectedStatusOptions.length > 0 && selectAll) {
          statusIds = [];
        } else {
          statusIds = statusIds.filter(w => w !== "5");
        }
      } else {
        statusIds = [];
      }
    }

    let object = {
      ...widgetSettings,
      widgetSettings: {
        ...widgetSettings.widgetSettings,
        selectedStatus: statusIds,
      },
    };
    dispatchWidgetSetting(dispatch, object);
  };

  // Handle project selection in dropdown
  const onProjectSelect = options => {
    let selectAll = options.find(o => o.value == "5") || false;
    let projectIds = options.map(o => o.value);
    if (selectAll) {
      if (selectAll && options.length > 1) {
        if (selectedProject.length > 0 && selectAll) {
          projectIds = [];
        } else {
          projectIds = projectIds.filter(w => w !== "5");
        }
      } else {
        projectIds = [];
      }
    }

    let object = {
      ...widgetSettings,
      widgetSettings: {
        ...widgetSettings.widgetSettings,
        selectedProject: projectIds,
      },
    };
    dispatchWidgetSetting(dispatch, object);
  };

  // Handle resource selection in dropdown
  const onResourceSelect = options => {
    let selectAll = options.find(o => o.value == "1") || false;
    let resourcesIds = options.map(o => o.value);
    if (selectAll) {
      if (selectAll && options.length > 1) {
        if (selectedResource.length > 0 && selectAll) {
          resourcesIds = [];
        } else {
          resourcesIds = resourcesIds.filter(w => w !== "1");
        }
      } else {
        resourcesIds = [];
      }
    }

    let object = {
      ...widgetSettings,
      widgetSettings: {
        ...widgetSettings.widgetSettings,
        selectedResource: resourcesIds,
      },
    };
    dispatchWidgetSetting(dispatch, object);
  };

  const compilingEffortByProject = param => {
    let data = [];
    param.map(element => {
      data.push({
        id: element.id,
        "Effort Completed": element.completedEfforts,
        "Effort Remaining": element.remainingEfforts,
        completedEfforts: element.completedEfforts,
        remainingEfforts: element.remainingEfforts,
      });
    });
    setBardata(data);
  };

  const compilingProjectByProjectManagers = param => {
    let data = [];
    param.map(element => {
      data.push({
        id: element.id,
        label: element.label,
        value: element.value,
      });
    });
    setPieData(data);
  };

  const handleClickApply = () => {
    setBtnQuery("progress");
    let object = {
      ...widgetSettings,
    };
    updateProjectAnalyticsData(
      object,
      // Success
      res => {
        // dispatchWidgetSetting(dispatch, res);
        getWidgetAnalyticsData("", res => {
          dispatchWidgetData(dispatch, res.data.entity);
          setBtnQuery("");
          setBtnDisabled(true);
        });
      }
    );
  };

  // useEffect(() => {
  //   if (
  //     !isUndefined(prevSelectedWorkspaceRef.current) &&
  //     !isUndefined(prevSelectedStatusRef.current) &&
  //     !isUndefined(prevSelectedPmRef.current)
  //   ) {
  //     if (
  //       JSON.stringify(prevSelectedWorkspaceRef.current) !==
  //         JSON.stringify(selectedWorkspaceOptions) ||
  //       JSON.stringify(prevSelectedStatusRef.current) !== JSON.stringify(selectedStatusOptions) ||
  //       JSON.stringify(prevSelectedPmRef.current) !== JSON.stringify(selectedResourcesOptions)
  //     ) {
  //       setBtnDisabled(false);
  //     }
  //   }
  //   prevSelectedWorkspaceRef.current = selectedWorkspaceOptions;
  //   prevSelectedStatusRef.current = selectedStatusOptions;
  //   prevSelectedPmRef.current = selectedResourcesOptions;
  // });

  useEffect(() => {
    // let a = prevSelectedWorkspaceRef.current;
    // let b = prevSelectedStatusRef.current;
    // let c = prevSelectedPmRef.current;

    setDonutData(resourceProjects);
    setPieData(tasksByStatus);
    // compilingEffortByProject(effortByProject);
    // compilingProjectByProjectManagers(projectByProjectManagers);
  }, [resourceProjects, tasksByStatus]);

  const noRecordProgress = donutData.every(e => e.value == 0);
  const noRecordTask = pieData.every(e => e.value == 0);
  // const noRecordProjectEffort = bardata.every(
  //   e => e.completedEfforts == 0 && e.remainingEfforts == 0
  // );

  return (
    /*heading*/

    <Grid container spacing={2}>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
        className={classes.topContainer}>
        <Grid item sm={12} md={11} lg={3} className={classes.gridStyle}>
          <div>
            {/* <img src={ntaskLogo} className={classes.headerLogo} /> */}
            <label className={classes.dashboardTitle}>Resources Dashboard</label>
          </div>
        </Grid>
        {/* first select dropdown */}
        <Grid item sm={12} md={3} lg={2} className={classes.gridStyle}>
          <CustomMultiSelectDropdown
            label="Workspace"
            options={() => workspacesDropdownData}
            option={selectedWorkspaceOptions}
            onSelect={onWorkspaceSelect}
            scrollHeight={220}
            size={"large"}
            buttonProps={{
              variant: "contained",
              btnType: "white",
              labelAlign: "left",
              style: { minWidth: "100%", padding: "3px 4px 3px 8px", height: 35 },
            }}
          />
        </Grid>
        {/* second select dropdown */}
        <Grid item sm={12} md={3} lg={2} className={classes.gridStyle}>
          <CustomMultiSelectDropdown
            label="Projects"
            options={() => allProjects}
            option={selectedProjectOptions}
            onSelect={onProjectSelect}
            scrollHeight={220}
            size={"large"}
            buttonProps={{
              variant: "contained",
              btnType: "white",
              labelAlign: "left",
              style: { minWidth: "100%", padding: "3px 4px 3px 8px", height: 35 },
            }}
          />
        </Grid>
        {/* third select dropdown */}
        <Grid item sm={12} md={3} lg={2} className={classes.gridStyle}>
          <CustomMultiSelectDropdown
            label="Task Status"
            options={() => taskStatuses}
            option={selectedStatusOptions}
            onSelect={onStatusSelect}
            scrollHeight={220}
            size={"large"}
            buttonProps={{
              variant: "contained",
              btnType: "white",
              labelAlign: "left",
              style: { minWidth: "100%", padding: "3px 4px 3px 8px", height: 35 },
            }}
          />
        </Grid>
        {/* 4th select dropdown */}
        <Grid item sm={12} md={3} lg={2} className={classes.gridStyle}>
          <CustomMultiSelectDropdown
            label="Resources"
            options={resourcesDdata}
            option={selectedResourcesOptions}
            onSelect={onResourceSelect}
            scrollHeight={220}
            size={"large"}
            buttonProps={{
              variant: "contained",
              btnType: "white",
              labelAlign: "left",
              style: { minWidth: "100%", padding: "3px 4px 3px 8px", height: 35 },
            }}
          />
          <div className={classes.horizontalDivider}></div>
        </Grid>
        {/* apply button */}
        <Grid item sm={12} md={1} lg={1} className={classes.gridStyle}>
          <CustomButton
            onClick={event => {
              handleClickApply();
            }}
            ref={null}
            style={{
              marginTop: 15,
              height: 33,
            }}
            btnType={"blue"}
            variant="contained"
            disabled={btnDisabled || btnQuery === "progress"}
            query={btnQuery}>
            {/* <ImportExport classes={{ root: classes.importExportIcon }} /> */}
            <span className={classes.sortLbl}>Apply</span>
          </CustomButton>
        </Grid>
      </Grid>

      {/*colored boxes*/}
      {/* <Grid container direction="row" justify="space-between" alignItems="center">
        <Grid item sm={6} md={4} lg={2} className={classes.gridStyle} spacing={2}>
          <div style={{ background: "#2EBAE6", padding: 10 }} className={classes.content}>
            <label className={classes.contentHeading}>Resources</label>
            <label>{`${projectSummary.totalProjects}`}</label>
            <span className={classes.time}>Total</span>
            <span className={classes.time}>(Count)</span>
          </div>
        </Grid>
        <Grid item sm={6} md={4} lg={2} className={classes.gridStyle} spacing={2}>
          <div style={{ background: "#29C8A2", padding: 10 }} className={classes.content}>
            <label className={classes.contentHeading}>Tasks</label>
            <label>{`${projectSummary.completedTasks}/${projectSummary.totalTasks}`}</label>
            <span className={classes.time}>Completed / Total</span>
            <span className={classes.time}>(Count)</span>
          </div>
        </Grid>
        <Grid item sm={6} md={4} lg={2} className={classes.gridStyle} spacing={2}>
          <div style={{ background: "#29C8A2", padding: 10 }} className={classes.content}>
            <label className={classes.contentHeading}>Tasks</label>
            <label>{`${projectSummary.overDueTasks}/${projectSummary.lateTasks}/${projectSummary.onTrackTasks}`}</label>
            <span className={classes.time}>Not Started / In Progress / Total</span>
            <span className={classes.time}>(Count)</span>
          </div>
        </Grid>
        <Grid item sm={6} md={4} lg={2} className={classes.gridStyle} spacing={2}>
          <div style={{ background: "#465457", padding: 10 }} className={classes.content}>
            <label className={classes.contentHeading}>Effort</label>
            <label>{`${projectSummary.completedEfforts}/${projectSummary.remainingEfforts}/${projectSummary.totalEfforts}`}</label>
            <span className={classes.time}>Completed / Remaining / Total</span>
            <span className={classes.time}>(Hours)</span>
          </div>
        </Grid>
        <Grid item sm={6} md={4} lg={2} className={classes.gridStyle} spacing={2}>
          <div style={{ background: "#E64E4E", padding: 10 }} className={classes.content}>
            <label className={classes.contentHeading}>Issues</label>
            <label>{`${projectSummary.issuesOpen}/${projectSummary.issuesClosed}`}</label>
            <span className={classes.time}>Open / Closed</span>
            <span className={classes.time}>(Count)</span>
          </div>
        </Grid>
        <Grid item sm={6} md={4} lg={2} className={classes.gridStyle} spacing={2}>
          <div style={{ background: "#FC844F", padding: 10 }} className={classes.content}>
            <label className={classes.contentHeading}>Risks</label>
            <label>{projectSummary.risks}</label>
            <span className={classes.time}>Total</span>
            <span className={classes.time}>(Count)</span>
          </div>
        </Grid>
      </Grid> */}
      {/*Donut Graphs*/}
      <Grid container direction="row" justify="space-between" alignItems="center">
        <Grid item sm={12} md={6} lg={4} spacing={2} className={classes.gridStyle}>
          <div className={classes.graphContent}>
            <label className={classes.graphHeading}>Resource Tasks by Project</label>
            {noRecordProgress ? (
              <div className={classes.emptyParentCnt}>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                  }}>
                  <SvgIcon viewBox="0 0 140 140" className={classes.svgIcon}>
                    <IconDonut />
                  </SvgIcon>
                  <span className={classes.noFoundTxt}> No Record Available</span>
                </div>
              </div>
            ) : (
              <ResponsivePie
                data={donutData}
                margin={{ top: 40, right: 80, bottom: 80, left: 80 }}
                innerRadius={0.5}
                padAngle={0}
                cornerRadius={0}
                colors={{ scheme: "category10" }}
                borderWidth={0}
                borderColor={{ from: "color", modifiers: [["darker", 0]] }}
                radialLabelsSkipAngle={10}
                radialLabelsTextColor="#333333"
                radialLabelsLinkColor={{ from: "color" }}
                sliceLabelsSkipAngle={10}
                sliceLabelsTextColor="#333333"
                defs={[
                  {
                    id: "dots",
                    type: "patternDots",
                    background: "inherit",
                    color: "rgba(255, 255, 255, 0.3)",
                    size: 4,
                    padding: 1,
                    stagger: true,
                  },
                  {
                    id: "lines",
                    type: "patternLines",
                    background: "inherit",
                    color: "rgba(255, 255, 255, 0.3)",
                    rotation: -45,
                    lineWidth: 6,
                    spacing: 10,
                  },
                ]}
                fill={[
                  {
                    match: {
                      id: "ruby",
                    },
                    id: "dots",
                  },
                  {
                    match: {
                      id: "c",
                    },
                    id: "dots",
                  },
                  {
                    match: {
                      id: "go",
                    },
                    id: "dots",
                  },
                  {
                    match: {
                      id: "python",
                    },
                    id: "dots",
                  },
                  {
                    match: {
                      id: "scala",
                    },
                    id: "lines",
                  },
                  {
                    match: {
                      id: "lisp",
                    },
                    id: "lines",
                  },
                  {
                    match: {
                      id: "elixir",
                    },
                    id: "lines",
                  },
                  {
                    match: {
                      id: "javascript",
                    },
                    id: "lines",
                  },
                ]}
                radialLabelsLinkOffset={-5}
                radialLabelsLinkHorizontalLength={15}
                radialLabelsTextXOffset={2}
              />
            )}
          </div>
        </Grid>

        {/*bar chart*/}
        {/* <Grid item sm={12} md={12} lg={8} spacing={2} className={classes.gridStyle}>
          <div className={classes.bargraphContent} id="custom-scroll">
            <label className={classes.graphHeading}>
              Effort Completed and Remaining by Resource
            </label>
            {noRecordProjectEffort ? (
              <div className={classes.emptyParentCnt}>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                  }}>
                  <SvgIcon viewBox="0 0 140 140.117" className={classes.svgIcon}>
                    <IconBar />
                  </SvgIcon>
                  <span className={classes.noFoundTxt}> No Record Available</span>
                </div>
              </div>
            ) : (
              <ResponsiveBar
                data={bardata}
                keys={["Effort Completed", "Effort Remaining"]}
                layout="horizontal"
                indexBy="id"
                margin={{ top: 40, right: 30, bottom: 100, left: 60 }}
                padding={0}
                valueScale={{ type: "linear" }}
                indexScale={{ type: "band", round: true }}
                colors={["#29c8a2", "#465457"]}
                defs={[
                  {
                    id: "dots",
                    type: "patternDots",
                    background: "inherit",
                    color: "#38bcb2",
                    size: 4,
                    padding: 1,
                    stagger: true,
                  },
                  {
                    id: "lines",
                    type: "patternLines",
                    background: "inherit",
                    color: "#eed312",
                    rotation: -45,
                    lineWidth: 6,
                    spacing: 10,
                  },
                ]}
                fill={[
                  {
                    match: {
                      id: "fries",
                    },
                    id: "dots",
                  },
                  {
                    match: {
                      id: "sandwich",
                    },
                    id: "lines",
                  },
                ]}
                borderColor={{ from: "color", modifiers: [["darker", 1.6]] }}
                axisTop={null}
                axisRight={null}
                axisBottom={{
                  tickSize: 6,
                  tickPadding: 5,
                  tickRotation: 90,
                  legend: "",
                  legendPosition: "middle",
                  legendOffset: 60,
                }}
                axisLeft={{
                  tickSize: 5,
                  tickPadding: 5,
                  tickRotation: 0,
                  // legend: "Effort",
                  legendPosition: "middle",
                  legendOffset: -40,
                }}
                axisBottom={{
                  legend: "Hours",
                  legendPosition: "middle",
                  legendOffset: 40,
                }}
                labelSkipWidth={12}
                labelSkipHeight={12}
                labelTextColor={{ from: "color", modifiers: [["darker", 1.6]] }}
                legends={[
                  {
                    dataFrom: ["Effort Completed", "Effort Remaining"],
                    anchor: "top",
                    direction: "row",
                    justify: false,
                    translateX: 46,
                    translateY: -46,
                    itemsSpacing: 40,
                    itemWidth: 75,
                    itemHeight: 44,
                    itemDirection: "left-to-right",
                    itemOpacity: 0.85,
                    symbolSize: 10,
                    effects: [
                      {
                        on: "hover",
                        style: {
                          itemOpacity: 1,
                        },
                      },
                    ],
                  },
                ]}
                animate={true}
                motionStiffness={90}
                motionDamping={15}
              />
            )}
          </div>
        </Grid> */}

        {/*Pie Graph*/}
        <Grid item sm={12} md={6} lg={4} spacing={2} className={classes.gridStyle}>
          <div className={classes.graphContent}>
            <label className={classes.graphHeading}>Tasks by Status</label>
            {noRecordTask ? (
              <div className={classes.emptyParentCnt}>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                  }}>
                  <SvgIcon viewBox="0 0 140 140.009" className={classes.svgIcon}>
                    <IconPie />
                  </SvgIcon>
                  <span className={classes.noFoundTxt}> No Record Available</span>
                </div>
              </div>
            ) : (
              <ResponsivePie
                data={pieData}
                margin={{ top: 40, right: 80, bottom: 80, left: 80 }}
                innerRadius={0}
                padAngle={0}
                cornerRadius={0}
                colors={{ scheme: "paired" }}
                borderWidth={0}
                borderColor={{ from: "color", modifiers: [["darker", 0]] }}
                radialLabelsSkipAngle={10}
                radialLabelsTextColor="#333333"
                radialLabelsLinkColor={{ from: "color" }}
                sliceLabelsSkipAngle={10}
                sliceLabelsTextColor="#333333"
                defs={[
                  {
                    id: "dots",
                    type: "patternDots",
                    background: "inherit",
                    color: "rgba(255, 255, 255, 0.3)",
                    size: 4,
                    padding: 1,
                    stagger: true,
                  },
                  {
                    id: "lines",
                    type: "patternLines",
                    background: "inherit",
                    color: "rgba(255, 255, 255, 0.3)",
                    rotation: -45,
                    lineWidth: 6,
                    spacing: 10,
                  },
                ]}
                fill={[
                  {
                    match: {
                      id: "ruby",
                    },
                    id: "dots",
                  },
                  {
                    match: {
                      id: "c",
                    },
                    id: "dots",
                  },
                  {
                    match: {
                      id: "go",
                    },
                    id: "dots",
                  },
                  {
                    match: {
                      id: "python",
                    },
                    id: "dots",
                  },
                  {
                    match: {
                      id: "scala",
                    },
                    id: "lines",
                  },
                  {
                    match: {
                      id: "lisp",
                    },
                    id: "lines",
                  },
                  {
                    match: {
                      id: "elixir",
                    },
                    id: "lines",
                  },
                  {
                    match: {
                      id: "javascript",
                    },
                    id: "lines",
                  },
                ]}
                radialLabelsLinkOffset={-5}
                radialLabelsLinkHorizontalLength={15}
                radialLabelsTextXOffset={2}
              />
            )}
          </div>
        </Grid>
      </Grid>
      {/*Project OverView*/}
      {/* <ProjectOverview /> */}
    </Grid>
  );
}

ResourcesContent.propTypes = {
  classes: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => {
  return {
    profileState: state.profile.data,
  };
};

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps)
)(ResourcesContent);
