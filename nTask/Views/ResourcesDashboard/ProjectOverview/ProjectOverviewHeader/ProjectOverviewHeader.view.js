import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import React, { useMemo, useEffect, useState, useContext } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import CustomSelectDropdown from "../../../../components/Dropdown/CustomSelectDropdown/Dropdown";
import CustomMultiSelectDropdown from "../../../../components/Dropdown/CustomMultiSelectDropdown/Dropdown";
import {
  filtersData,
  generateWorkspaceData,
} from "../../../../helper/generateTasksOverviewDropdownData";
import projectOverviewHeaderStyles from "./ProjectOverviewHeader.style";
import ProjectOverviewContext from "../Context/ProjectOverview.context";
import { getWidget, updateWidgetSetting } from "../../../../redux/actions/overviews";
import { dispatchTaskWidgetData, dispatchWidgetSetting } from "../Context/actions";

function ProjectOverviewHeader(props) {
  const {
    classes,
    profileState: { workspace, activeTeam },
  } = props;
  const [subTaskChecked, setSubTaskChecked] = useState(false);
  const {
    state: { widgetSettings, projectWidgetData, filteredTasks },
    dispatch,
  } = useContext(ProjectOverviewContext);
  const { selectedWorkspaces } = widgetSettings.widgetSettings;

  // Handle workspace selection in dropdown
  const onWorkspaceSelect = options => {
    // setSelectedWorkspace(option);
    const workspaceIds = options.map(o => o.obj.teamId);
    dispatchWidgetSetting(dispatch, {selectedWorkspaces: workspaceIds })
    updateWidgetSetting(
      "task",
      {...widgetSettings, selectedWorkspaces: workspaceIds },
      // Success
      (res) => {  
        getWidget(
          "Task",
          //Success
          resp => {
            dispatchTaskWidgetData(dispatch, resp.data.entity);
          },
          err => {
            dispatchTaskWidgetData(dispatch, {userTasks: [], issues: [], risks: []});
          }
        );
      }
    );
  };
// Handle Filter Select
  const handleFilterSelect = option => {
    dispatchWidgetSetting(dispatch, {selectedFilter: option.id })
      updateWidgetSetting(
      "task",
      {...widgetSettings, selectedFilter: option.id },
      // Success
      (res) => {
       
      }
    );
  }
  // Handle subtasks switch check/uncheck
  const handleSubtaskSwitch = () => {
    setSubTaskChecked(prevValue => setSubTaskChecked(!prevValue));
  };
  // Generate workspace dropdown data
  const workspacesDropdownData = useMemo(() => {
    return generateWorkspaceData(workspace);
  }, workspace);

  const selectedFilterOption = filtersData.find(f => f.id === widgetSettings.selectedFilter);

  const selectedWorkspaceOptions = workspacesDropdownData.filter(w => {
    return selectedWorkspaces.includes(w.obj.teamId);
  });
  const getTotalTasks = () => {
    const { selectedFilter } = widgetSettings;
    if (selectedFilter === "viewAllTasks") {
      return projectWidgetData.userTasks.length;
    }
    return filteredTasks[selectedFilter].length;
  }
  return (
    <div className={classes.headerCnt}>
      <div className={classes.headerLeftCnt}>
        <span className={classes.mainHeading}>
        {activeTeam === "013f092268fb464489129c5ea19b89d3" ? "Executive Report" : "Projects Overview"}
          {/* <span style={{color: 'rgba(126, 126, 126, 1)',
            fontSize: "12px !important",
            fontWeight: 500,
            lineHeight: 1.5 }}>({getTotalTasks()})</span> */}
        </span>

        {/* <CustomSelectDropdown
          options={() => filtersData}
          option={selectedFilterOption}
          onSelect={handleFilterSelect}
          isDashboard={true}
          height="140px"
          scrollHeight={180}
          buttonProps={{
            variant: "contained",
            btnType: "white",
            labelAlign: "left",
            style: { minWidth: 140, padding: "3px 4px 3px 8px", marginRight: 8 },
          }}
          // disabled={!permission.detailsTabs.billingMethode.cando}
        /> */}
        {/* <CustomMultiSelectDropdown
          label="Workspace:"
          options={() => workspacesDropdownData}
          option={selectedWorkspaceOptions}
          onSelect={onWorkspaceSelect}
          height="140px"
          scrollHeight={180}
          buttonProps={{
            variant: "contained",
            btnType: "white",
            labelAlign: "left",
            style: { minWidth: 180, padding: "3px 4px 3px 8px" },
          }}
          // disabled={!permission.detailsTabs.billingMethode.cando}
        /> */}

        {/* <div className={classes.subTaskSwitchCnt}>
          <Typography variant="h6" className={classes.subTaskSwitchLabel}>
            Subtasks
          </Typography>

          <DefaultSwitch checked={subTaskChecked} onChange={handleSubtaskSwitch} size="small" />
        </div> */}
      </div>
      {/* <MoreActionDropDown /> */}
    </div>
  );
}
const mapStateToProps = state => {
  return {
    profileState: state.profile.data,
  };
};

export default compose(
  withStyles(projectOverviewHeaderStyles, { withTheme: true }),
  connect(mapStateToProps)
)(ProjectOverviewHeader);
