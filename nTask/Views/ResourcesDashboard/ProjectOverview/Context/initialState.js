const initialState = {
  widgetSettings: {
    groupBy: [],
    widgetSettings: {
      projects:[],
      resources:[],
      selectedProject: [],
      selectedResource: [],
      selectedStatus: [],
      selectedWorkspaces: [],
      taskStatus:[]
    },
  },
  projectWidgetData: {
    projectResource: [],
    resourceProjects: [],
    tasksByStatus: [],
    projectSummary: {},
    projects: [],
    projectsbyProgress: [],
    effortByProject: [],
    // effortByProject: [
    //   {
    //     id: "Gantt Access1",
    //     completedEfforts: 2,
    //     remainingEfforts: 4,
    //   },
    //   {
    //     id: "Manual Roller1",
    //     completedEfforts: 6,
    //     remainingEfforts: 8,
    //   },
    //   {
    //     id: "Gantt Access2",
    //     completedEfforts: 9,
    //     remainingEfforts: 12,
    //   },
    //   {
    //     id: "Giveaways1",
    //     completedEfforts: 14,
    //     remainingEfforts: 18,
    //   },
    //   {
    //     id: "Test-APEX01",
    //     completedEfforts: 20,
    //     remainingEfforts: 22,
    //   },
    //   {
    //     id: "Flora 2.00",
    //     completedEfforts: 22,
    //     remainingEfforts: 30,
    //   },
    //   {
    //     id: "Gantt Access3",
    //     completedEfforts: 35,
    //     remainingEfforts: 45,
    //   },
    //   {
    //     id: "Manual Roller2",
    //     completedEfforts: 50,
    //     remainingEfforts: 75,
    //   },
    //   {
    //     id: "Gantt Access4",
    //     completedEfforts: 80,
    //     remainingEfforts: 55,
    //   },
    //   {
    //     id: "Giveaways2",
    //     completedEfforts: 90,
    //     remainingEfforts: 65,
    //   },
    //   {
    //     id: "Test-APEX02",
    //     completedEfforts: 30,
    //     remainingEfforts: 50,
    //   },
    //   {
    //     id: "Flora 2.01",
    //     completedEfforts: 40,
    //     remainingEfforts: 18,
    //   },
    //   {
    //     id: "Gantt Access5",
    //     completedEfforts: 84,
    //     remainingEfforts: 30,
    //   },
    //   {
    //     id: "Manual Roller3",
    //     completedEfforts: 55,
    //     remainingEfforts: 66,
    //   },
    //   {
    //     id: "Gantt Access6",
    //     completedEfforts: 77,
    //     remainingEfforts: 88,
    //   },
    //   {
    //     id: "Giveaways3",
    //     completedEfforts: 10,
    //     remainingEfforts: 25,
    //   },
    //   {
    //     id: "Test-APEX03",
    //     completedEfforts: 30,
    //     remainingEfforts: 39,
    //   },
    //   {
    //     id: "Flora 2.02",
    //     completedEfforts: 10,
    //     remainingEfforts: 52,
    //   },
    //   {
    //     id: "Gantt Access7",
    //     completedEfforts: 26,
    //     remainingEfforts: 53,
    //   },
    //   {
    //     id: "Manual Roller4",
    //     completedEfforts: 22,
    //     remainingEfforts: 33,
    //   },
    //   {
    //     id: "Gantt Access8",
    //     completedEfforts: 80,
    //     remainingEfforts: 93,
    //   },
    //   {
    //     id: "Giveaways4",
    //     completedEfforts: 25,
    //     remainingEfforts: 36,
    //   },
    //   {
    //     id: "Test-APEX04",
    //     completedEfforts: 44,
    //     remainingEfforts: 35,
    //   },
    //   {
    //     id: "Flora 2.03",
    //     completedEfforts: 65,
    //     remainingEfforts: 83,
    //   },
    //   {
    //     id: "Gantt Access9",
    //     completedEfforts: 75,
    //     remainingEfforts: 83,
    //   },
    //   {
    //     id: "Manual Roller5",
    //     completedEfforts: 92,
    //     remainingEfforts: 100,
    //   },
    //   {
    //     id: "Gantt Access10",
    //     completedEfforts: 46,
    //     remainingEfforts: 76,
    //   },
    //   {
    //     id: "Giveaways5",
    //     completedEfforts: 68,
    //     remainingEfforts: 85,
    //   },
    //   {
    //     id: "Test-APEX05",
    //     completedEfforts: 85,
    //     remainingEfforts: 98,
    //   },
    //   {
    //     id: "Flora 2.04",
    //     completedEfforts: 99,
    //     remainingEfforts: 122,
    //   },
    //   {
    //     id: "Gantt Access11",
    //     completedEfforts: 25,
    //     remainingEfforts: 35,
    //   },
    //   {
    //     id: "Manual Roller6",
    //     completedEfforts: 36,
    //     remainingEfforts: 56,
    //   },
    //   {
    //     id: "Gantt Access12",
    //     completedEfforts: 45,
    //     remainingEfforts: 65,
    //   },
    //   {
    //     id: "Giveaways6",
    //     completedEfforts: 13,
    //     remainingEfforts: 32,
    //   },
    //   {
    //     id: "Test-APEX06",
    //     completedEfforts: 46,
    //     remainingEfforts: 65,
    //   },
    //   {
    //     id: "Flora 2.05",
    //     completedEfforts: 79,
    //     remainingEfforts: 98,
    //   },
    //   {
    //     id: "Gantt Access13",
    //     completedEfforts: 14,
    //     remainingEfforts: 74,
    //   },
    //   {
    //     id: "Manual Roller7",
    //     completedEfforts: 25,
    //     remainingEfforts: 85,
    //   },
    //   {
    //     id: "Gantt Access14",
    //     completedEfforts: 65,
    //     remainingEfforts: 69,
    //   },
    //   {
    //     id: "Giveaways7",
    //     completedEfforts: 56,
    //     remainingEfforts: 68,
    //   },
    //   {
    //     id: "Test-APEX07",
    //     completedEfforts: 46,
    //     remainingEfforts: 59,
    //   },
    //   {
    //     id: "Flora 2.06",
    //     completedEfforts: 68,
    //     remainingEfforts: 89,
    //   },
    //   {
    //     id: "Gantt Access15",
    //     completedEfforts: 45,
    //     remainingEfforts: 65,
    //   },
    //   {
    //     id: "Manual Roller8",
    //     completedEfforts: 75,
    //     remainingEfforts: 95,
    //   },
    //   {
    //     id: "Gantt Access16",
    //     completedEfforts: 65,
    //     remainingEfforts: 95,
    //   },
    //   {
    //     id: "Giveaways8",
    //     completedEfforts: 66,
    //     remainingEfforts: 88,
    //   },
    //   {
    //     id: "Test-APEX08",
    //     completedEfforts: 55,
    //     remainingEfforts: 68,
    //   },
    //   {
    //     id: "Flora 2.07",
    //     completedEfforts: 59,
    //     remainingEfforts: 69,
    //   },
    //   {
    //     id: "Gantt Access17",
    //     completedEfforts: 75,
    //     remainingEfforts: 83,
    //   },
    //   {
    //     id: "Manual Roller9",
    //     completedEfforts: 92,
    //     remainingEfforts: 100,
    //   },
    // ],
    projectByProjectManagers: [],
  },
  filteredTasks: {
    assignedToMe: [],
    dueToday: [],
    dueInFiveDays: [],
    overDueTasks: [],
    unscheduledTasks: [],
  },
  groupedTasks: [],
};

export default initialState;
