const generateTaskColumnsData = [
    // {
    //   label: "Project",
    //   value: "projectName",
    // },
    {
      label: "Project Manager",
      value: "projectManager",
    },
    {
      label: "Project Start",
      value: "startDate",
    },
    {
      label: "Project Finish",
      value: "dueDate",
    },
    {
      label: "Progress",
      value: "progress",
    },
    {
      label: "Effort(Hours)",
      value: "completedEffortInHours",
    },
    {
      label: "Task Count",
      value: "totalTasks",
    },
    {
      label: "Overdue Tasks",
      value: "overDueTasks",
    },
    {
      label: "Late Tasks",
      value: "lateTasks",
    },
    {
      label: "On Track Tasks",
      value: "onTrackTask",
    },
    {
      label: "Future Tasks",
      value: "futureTasks",
    },
    {
      label: "Completed Tasks",
      value: "completedTasks",
    },
  ];

  export default generateTaskColumnsData