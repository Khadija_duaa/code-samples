import React, { useReducer, useState, useEffect } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
//import TasksOverview from "../TasksOverview/TasksOverview.view";
import resourcesStyles from "./resources.style";
import ResourcesContent from "./resourcesContent.js";
// import ResourceDashboard from "./resourceDashboard";
import { calculateContentHeight } from "../../utils/common";
import { dispatchWidgetSetting, dispatchWidgetData } from "./ProjectOverview/Context/actions";
import ProjectOverviewContext from "./ProjectOverview/Context/ProjectOverview.context";
import initialState from "./ProjectOverview/Context/initialState";
import reducer from "./ProjectOverview/Context/reducer";
import { getWidgetAnalyticsData, getProjectAnalyticsData } from "../../redux/actions/resourcesDashboard";
import Loader from "../../components/Loader/Loader";

function Resources({ classes }) {
  const [state, dispatch] = useReducer(reducer, initialState);
  const [isLoading, setIsLoading] = useState(true);

  //Getting project data
  useEffect(() => {
    getProjectAnalyticsData(res => {
      dispatchWidgetSetting(dispatch, res);
      getWidgetAnalyticsData("", res => {
        dispatchWidgetData(dispatch, res.data.entity);
        setIsLoading(false);
      });
    });
  }, []);

  return (
    <ProjectOverviewContext.Provider value={{ dispatch, state }}>
      <div className={classes.overviewCnt} style={{ height: calculateContentHeight() + 77 }}>
        {isLoading && (
          
          <Loader />
        )}
        {!isLoading && <ResourcesContent />}
      </div>
    </ProjectOverviewContext.Provider>
  );
}

export default withStyles(resourcesStyles, { withTheme: true })(Resources);
