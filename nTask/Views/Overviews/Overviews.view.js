import React from "react";
import withStyles from '@material-ui/core/styles/withStyles';
import TasksOverview from "../TasksOverview/TasksOverview.view";
import overviewStyles from './overview.style';

function Overviews({classes}) {

  return (
    <div className={classes.overviewCnt}>
      <TasksOverview />
    </div>
  );
}

export default withStyles(overviewStyles, {withTheme: true})(Overviews);
