import { withStyles } from "@material-ui/core/styles";
import RestartAltIcon from "@material-ui/icons/RotateLeft";
import VisibilityIcon from "@material-ui/icons/Visibility";
import clsx from "clsx";
import React, { useContext } from "react";
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
import { connect, useSelector } from "react-redux";
import { compose } from "redux";
import CustomListItem from "../../../components/ListItem/CustomListItem";
import CustomMenuList from "../../../components/MenuList/CustomMenuList";
import CustomTooltip from "../../../components/Tooltip/Tooltip.js";
import { getMenuIcons } from "../constants";
import { updateSettings } from "../Context/actions/sidebar.actions";
import SidebarContext from "../Context/sidebar.context";
import sidebarStyles from "./CustomizeMenu.style";

function CustomizeMenu(params) {
  const {
    state: { menuItems },
    dispatch,
  } = useContext(SidebarContext);
  const { classes, theme, whiteLabelTeam } = params;

  const { sidebarPannel } = useSelector((state) => {
    return {
      sidebarPannel: state.sidebarPannel,
    };
  });

  const reorder = (list, startIndex, endIndex) => {
    const result = [...list];
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
  };

  const onDragEnd = (result, totalItems, key) => {
    const { source, destination } = result;
    const items = reorder(totalItems, source.index, destination.index);
    if (key == "team") updateSettings(dispatch, { menuItems: { ...menuItems, teamMenu: items } });
    if (key == "workspace")
      updateSettings(dispatch, { menuItems: { ...menuItems, workspaceMenu: items } });
  };

  const handleVisible = (selectedItem, totalItems, key) => {
    const updatedData = totalItems.map((item) => {
      if (item.key === selectedItem.key) {
        return { ...item, isHidden: !item.isHidden };
      } else {
        return item;
      }
    });
    if (key == "team")
      updateSettings(dispatch, { menuItems: { ...menuItems, teamMenu: updatedData } });
    if (key == "workspace") {
      let atLeastOneSelected = updatedData.some((item) => !item.isHidden);
      atLeastOneSelected &&
        updateSettings(dispatch, { menuItems: { ...menuItems, workspaceMenu: updatedData } });
    }
  };

  const handleResetAll = () => {
    updateSettings(dispatch, {
      menuItems: {
        ...menuItems,
        teamMenu: sidebarPannel.data.teamMenu,
        workspaceMenu: sidebarPannel.data.workspaceMenu,
      },
    });
  };
  return (
    <>
      <div className={classes.heading}>
        <span>Customize Menu</span>
        <CustomTooltip
          helptext={"Reset Changes"}
          iconType="help"
          placement="top"
          //   classes={classes}
          style={{ color: theme.palette.common.white }}>
          <RestartAltIcon className={classes.restAllIcon} onClick={handleResetAll} />
        </CustomTooltip>
      </div>
      <DragDropContext onDragEnd={(result) => onDragEnd(result, menuItems.teamMenu, "team")}>
        <Droppable droppableId="droppable">
          {(provided, snapshot) => (
            <CustomMenuList>
              {/* <div ref={provided.innerRef} style={{ minHeight: 180 }}> */}
              <div ref={provided.innerRef}>
                {menuItems.teamMenu && menuItems.teamMenu.map((menu, index) => {
                  return (
                    menu.key != 'documents' && ENV != 'dev' ?
                      <Draggable key={menu.key} draggableId={menu.key} index={index}>
                        {(provided, snapshot) => (
                          <div ref={provided.innerRef} {...provided.draggableProps}>
                            <CustomListItem
                              compactMode={false}
                              draggable={true}
                              draggableProps={{ ...provided.dragHandleProps }}
                              darkmode={true}
                              endIcon={
                                <div title={menu.isHidden ? "Show" : "Hide"}>
                                  {menu.key !== "tasks" && (
                                    <VisibilityIcon
                                      onClick={() => handleVisible(menu, menuItems.teamMenu, "team")}
                                      className={clsx({
                                        [classes.visibilityIconOn]: !menu.isHidden,
                                        [classes.visibilityIconOff]: menu.isHidden,
                                      })}
                                    />
                                  )}
                                </div>
                              }
                              isSelected={false}
                              icon={getMenuIcons(menu.key, classes)}
                              rootProps={{
                                onClick: (e) => { },
                              }}>
                              {menu.name}
                            </CustomListItem>
                          </div>
                        )}
                      </Draggable> : null
                  )
                })}
              </div>
            </CustomMenuList>
          )}
        </Droppable>
      </DragDropContext>
      <div
        className={clsx({
          [classes.quickAccess]: true,
        })}>
        <CustomListItem
          compactMode={false}
          darkmode={true}
          isSelected={false}
          icon={whiteLabelTeam && whiteLabelTeam.quickAccessImageUrl ?
            <img
              src={whiteLabelTeam.quickAccessImageUrl}
              alt={whiteLabelTeam.companyName}
              className={classes.sidebarIcon}
            /> : getMenuIcons("quickAccess", classes)}
          rootProps={{
            onClick: (e) => { },
          }}>
          {`Quick Access`}
        </CustomListItem>
      </div>
      <DragDropContext
        onDragEnd={(result) => onDragEnd(result, menuItems.workspaceMenu, "workspace")}>
        <Droppable droppableId="droppable">
          {(provided, snapshot) => (
            <CustomMenuList>
              {/* <div ref={provided.innerRef} style={{ minHeight: 252 }}> */}
              <div ref={provided.innerRef}>
                {menuItems.workspaceMenu && menuItems.workspaceMenu.map((menu, index) => (
                  <Draggable key={menu.key} draggableId={menu.key} index={index}>
                    {(provided, snapshot) => (
                      <div ref={provided.innerRef} {...provided.draggableProps}>
                        <CustomListItem
                          compactMode={false}
                          draggable={true}
                          draggableProps={{ ...provided.dragHandleProps }}
                          darkmode={true}
                          endIcon={
                            <div title={menu.isHidden ? "Show" : "Hide"}>
                              {menu.key != "tasks" && (
                                <VisibilityIcon
                                  onClick={() =>
                                    handleVisible(menu, menuItems.workspaceMenu, "workspace")
                                  }
                                  className={clsx({
                                    [classes.visibilityIconOn]: !menu.isHidden,
                                    [classes.visibilityIconOff]: menu.isHidden,
                                  })}
                                />
                              )}
                            </div>
                          }
                          isSelected={false}
                          icon={getMenuIcons(menu.key, classes)}
                          rootProps={{
                            onClick: (e) => { },
                          }}>
                          {menu.name}
                        </CustomListItem>
                      </div>
                    )}
                  </Draggable>
                ))}
              </div>
            </CustomMenuList>
          )}
        </Droppable>
      </DragDropContext>
    </>
  );
}
CustomizeMenu.defaultProps = {
  theme: {},
  classes: {},
};
const mapStateToProps = state => {
  return {
    whiteLabelTeam: state.whiteLabelInfo.data,
  };
};

export default compose(
  connect(mapStateToProps, {}),
  withStyles(sidebarStyles, { withTheme: true })
)(CustomizeMenu);
