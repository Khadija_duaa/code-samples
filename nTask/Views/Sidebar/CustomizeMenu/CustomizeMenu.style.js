const CustomizeMenu = theme => ({
  heading: {
    fontSize: "13px !important",
    fontWeight: theme.typography.fontWeightExtraLight,
    fontFamily: theme.typography.fontFamilyLato,
    padding: 10,
    color: "white",
  },
  sidebarIcon: {
    marginLeft: 4,
  },
  quickAccess: {
    borderBottom: `1px solid ${theme.palette.sidebar.elementsBackgroundColor}`,
    borderTop: `1px solid ${theme.palette.sidebar.elementsBackgroundColor}`,
    opacity: "0.5",
    pointerEvents: "none",
    paddingLeft: 10
  },
  dragHandleCnt: {
    visibility: "visible",
  },
  dragHandle: {
    fontSize: "24px !important",
  },
  checkListItemSpacer: {
    marginLeft: 28,
  },
  visibilityIconOn: {
    fontSize: "18px !important",
    color: `${theme.palette.background.green} !important`,
  },
  visibilityIconOff: {
    fontSize: "18px !important",
    color: "#7F8F9A !important",
  },
  restAllIcon: {
    fontSize: "22px !important",
    position: "absolute",
    right: 8,
    color: "#7F8F9A",
    cursor: "pointer",
    "&:hover": {
      color: `${theme.palette.background.green} !important`,
    },
  },
  bootstrapTooltip: {
    backgroundColor: `${theme.palette.common.white} !important`,
    color: "black !important",
  },
});

export default CustomizeMenu;
