import React, { useReducer, useState, useEffect, useRef, useCallback } from "react";
import { compose } from "redux";
import { connect, useDispatch } from "react-redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import sidebarStyles from "./style";
import SidebarContext from "./Context/sidebar.context";
import reducer from "./Context/reducer";
import initialState from "./Context/initialState";
import SidebarHeader from "./Header/SidebarHeader.view";
import ProfileMenu from "./ProfileMenu/ProfileMenu.view";
import { updateSidebarState, getMenuItems } from "../../redux/actions/sidebarPannel";
import clsx from "clsx";
import TeamInfoView from "./Team/TeamInformation/TeamInfo.view";
import WorkspaceInfoView from "./Workspace/WorkspaceInformation/WorkspaceInfo.view";
import SidebarMenuView from "./SidebarMenu/SidebarMenu.view";
import CustomizeMenuView from "./CustomizeMenu/CustomizeMenu.view";
import { updateSettings } from "./Context/actions/sidebar.actions";
import TeamListView from "./Team/TeamList/TeamList.view";
import WorkspaceList from "./Workspace/WorkspaceList/WorkspaceList.view";
import ReportingView from "./Reporting/Reporting.view";
import QuickAccessView from "./QuickAccess/QuickAccess.view";

function Sidebar(params) {
  const {
    classes,
    sidebarPannel,
    showLoadingState,
    hideLoadingState,
    planExpire,
    history,
    notificationBar,
    promoBar,
    whiteLabelTeam,
  } = params;
  const dispatchFun = useDispatch();
  const [state, dispatch] = useReducer(reducer, initialState);
  const [open, setOpen] = useState(sidebarPannel.open);
  const sidebarRef = useRef(null);
  const [isResizing, setIsResizing] = useState(false);
  const [sidebarWidth, setSidebarWidth] = useState(sidebarPannel.width);
  const [pointerEventClass, setPointerEventClass] = useState(false);

  const {
    teamView,
    innerSidebar,
    sidebarFullWidth,
    customizeMenu,
    workspaceView,
    quickAccess,
  } = state;

  const handleSidebarClose = () => {
    setPointerEventClass(true);
    setTimeout(() => {
      setPointerEventClass(false);
    }, 500);

    updateSidebarState(dispatchFun, { open: false });
    let sideBarState = { ...JSON.parse(localStorage.getItem("sidebar")), collape: false };
    localStorage.setItem("sidebar", JSON.stringify(sideBarState));
  };

  const startResizing = useCallback(mouseDownEvent => {
    setIsResizing(true);
  }, []);

  const stopResizing = useCallback(() => {
    setIsResizing(false);
  }, []);

  const resize = useCallback(
    mouseMoveEvent => {
      if (isResizing) {
        const width = mouseMoveEvent.clientX - sidebarRef.current.getBoundingClientRect().left;
        if (width >= 230 && width <= 400) {
          setSidebarWidth(width);
          updateSidebarState(dispatchFun, { width: parseInt(width) });
          let sideBarState = { ...JSON.parse(localStorage.getItem("sidebar")), width: width };
          localStorage.setItem("sidebar", JSON.stringify(sideBarState));
          // localStorage.setItem("sidebarWidth", width);
        }
      }
    },
    [isResizing]
  );

  useEffect(() => {
    window.addEventListener("mousemove", resize);
    window.addEventListener("mouseup", stopResizing);
    return () => {
      window.removeEventListener("mousemove", resize);
      window.removeEventListener("mouseup", stopResizing);
    };
  }, [resize, stopResizing]);
  // default width and collaps setting here
  useEffect(() => {
    const sideBarState = JSON.parse(localStorage.getItem("sidebar"));
    if (sideBarState != null) {
      if (!sideBarState.collape) {
        updateSidebarState(dispatchFun, { open: false });
      }
      if (sideBarState.width) {
        updateSidebarState(dispatchFun, { width: sideBarState.width });
        setSidebarWidth(sideBarState.width);
      }
    }
  }, []);
  useEffect(() => {
    setOpen(sidebarPannel.open);
  }, [sidebarPannel]);

  useEffect(() => {
    let searchQuery = history.location.pathname; //getting url
    getMenuItems(
      dispatchFun,
      succ => {
        updateSettings(dispatch, {
          menuItems: succ.data.entity,
          innerSidebar: searchQuery.startsWith("/reports") ? true : false,
        });
      },
      fail => {}
    );
  }, []);

  return (
    <>
      <SidebarContext.Provider
        value={{
          dispatch,
          state,
          showLoadingState,
          hideLoadingState,
          planExpire,
        }}>
        <div
          className={clsx({
            [classes.parentContainer]: true,
            [classes.roundedEdge]: !whiteLabelTeam,
            [classes.pointerEventClass]: pointerEventClass,
          })}
          ref={sidebarRef}
          style={
            open
              ? {
                  width: sidebarWidth,
                  marginTop: promoBar || notificationBar ? 50 : "",
                  height: promoBar || notificationBar ? `calc(100vh - 50px)` : "",
                  paddingRight: 0,
                }
              : {
                  width: 0,
                  minWidth: 0,
                  marginTop: promoBar || notificationBar ? 50 : "",
                  height: promoBar || notificationBar ? `calc(100vh - 50px)` : "",
                  paddingRight: 1,
                }
          }
          // onMouseDown={e => e.preventDefault()}
        >
          <div
            className={clsx({
              [classes.sidebarContainerOpen]: open,
              [classes.sidebarContainerClose]: !open,
            })}>
            <div className={classes.sidebarElements}>
              <SidebarHeader
                handleSidebarClose={handleSidebarClose}
                sidebarPannel={sidebarPannel}
              />
              <div>
                {!customizeMenu /** selected team menu */ && <TeamInfoView />}
                <div style={{ display: "flex", flexDirection: "row" }}>
                  <div
                    className={clsx({
                      [classes.teamPannel]: teamView,
                      [classes.teamPannelDefault]: !teamView,
                    })}>
                    {!customizeMenu /** selected workspace menu */ && <WorkspaceInfoView />}
                    <div className={classes.sidebarInnerPannel}>
                      <div
                        className={clsx({
                          [classes.sidebarPannel]: true,
                          [classes.workspaceOpenStyle]: workspaceView,
                          [classes.sidebarPannelOpen]: innerSidebar && sidebarFullWidth,
                          [classes.sidebarPannelClose]: innerSidebar && !sidebarFullWidth,
                        })}>
                        {!customizeMenu /** side bar menu view containing the route of team and workspce menus */ && (
                          <SidebarMenuView />
                        )}
                        {customizeMenu /**  */ && <CustomizeMenuView />}
                        <ProfileMenu />
                      </div>
                      {workspaceView /** calls when workspace view is opened */ && (
                        <div className={clsx({ [classes.teamPannelOpen]: true })}>
                          <WorkspaceList />
                        </div>
                      )}
                      {innerSidebar /** calls when reporting view/quick access is open */ && (
                        <div
                          className={clsx({
                            [classes.analyticsPannel]: sidebarFullWidth,
                            [classes.analyticsPannelHalf]: !sidebarFullWidth,
                          })}>
                          {quickAccess ? <QuickAccessView /> : <ReportingView />}
                        </div>
                      )}
                    </div>
                  </div>
                  {teamView /** calls when team menu is open */ && (
                    <div className={clsx({ [classes.teamPannelOpen]: true })}>
                      <TeamListView />
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
          {/* <div className={classes.appSidebarResizer} onMouseDown={startResizing} /> */}
        </div>
      </SidebarContext.Provider>
    </>
  );
}
Sidebar.defaultProps = {
  classes: {},
};

const mapStateToProps = state => {
  return {
    sidebarPannel: state.sidebarPannel,
    promoBar: state.promoBar.promoBar,
    notificationBar: state.notificationBar.notificationBar,
    whiteLabelTeam: state.whiteLabelInfo.data,
  };
};

export default compose(
  withRouter,
  connect(mapStateToProps, {}),
  withStyles(sidebarStyles, { withTheme: true })
)(Sidebar);
