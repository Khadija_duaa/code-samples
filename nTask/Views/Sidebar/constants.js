import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";

import IconProjects from "../../components/Icons/IconProjects";
import IconBoards from "../../components/Icons/IconBoards";
import IconTasks from "../../components/Icons/IconTasks";
import IconMeetings from "../../components/Icons/IconMeetings";
import IconTimesheet from "../../components/Icons/IconTimesheet";
import IconIssues from "../../components/Icons/IconIssues";
import IconRisks from "../../components/Icons/IconRisks";
import IconStar from "../../components/Icons/IconStar";

import IconAnalytics from "../../components/Icons/IconAnalytics";
import IconHome from "../../components/Icons/IconHome";
import IconNotes from "../../components/Icons/IconNotes";
import IconChat from "../../components/Icons/IconChat";
import IconSurveys from "../../components/Icons/IconSurveys";
import QuickAccessIcon from "../../components/Icons/nTaskLogo";
import IconResources from "../../components/Icons/IconResources";
import IconDelete from "../../components/Icons/CollaborationIcons/DeleteIcon";

export const getMenuIcons = (type, classes) => {
  const Components = {
    delete: (
      <SvgIcon style={{ fontSize: "18px" }} className={classes.sidebarIcon} viewBox="0 0 18 19.849">
        <IconDelete />
      </SvgIcon>),
    home: (
      <SvgIcon className={classes.sidebarIcon} viewBox="0 0 16 16.002">
        <IconHome />
      </SvgIcon>
    ),
    reporting: (
      <SvgIcon className={classes.sidebarIcon} viewBox="0 0 16 16">
        <IconAnalytics />
      </SvgIcon>
    ),
    collaboration: (
      <SvgIcon className={classes.sidebarIcon} viewBox="0 0 16 16.297">
        <IconChat />
      </SvgIcon>
    ),
    forms: (
      <SvgIcon className={classes.sidebarIcon} viewBox="0 0 15 15">
        <IconSurveys />
      </SvgIcon>
    ),
    resources: (
      <SvgIcon className={classes.sidebarIcon} viewBox="0 0 18 15.597">
        <IconResources />
      </SvgIcon>
    ),
    notes: (
      <SvgIcon className={classes.sidebarIcon} viewBox="0 0 14 17.684">
        <IconNotes />
      </SvgIcon>
    ),
    documents: (
      <SvgIcon className={classes.sidebarIcon} viewBox="0 0 14 17.684">
        <IconNotes />
      </SvgIcon>
    ),
    quickAccess: (
      <SvgIcon className={classes.sidebarIcon} viewBox="0 0 18 15.282">
        <QuickAccessIcon />
      </SvgIcon>
    ),
    projects: (
      <SvgIcon className={classes.sidebarIcon} viewBox="0 0 16 13.516">
        <IconProjects />
      </SvgIcon>
    ),
    boards: (
      <SvgIcon className={classes.sidebarIcon} viewBox="0 0 13.995 13.19">
        <IconBoards />
      </SvgIcon>
    ),
    tasks: (
      <SvgIcon className={classes.sidebarIcon} viewBox="0 0 14 15.556">
        <IconTasks />
      </SvgIcon>
    ),
    meetings: (
      <SvgIcon className={classes.sidebarIcon} viewBox="0 0 15 15.004">
        <IconMeetings />
      </SvgIcon>
    ),
    timesheets: (
      <SvgIcon className={classes.sidebarIcon} style={{ fontSize: "18px" }} viewBox="0 0 16 18.496">
        <IconTimesheet />
      </SvgIcon>
    ),
    issues: (
      <SvgIcon className={classes.sidebarIcon} viewBox="0 0 14 15.256">
        <IconIssues />
      </SvgIcon>
    ),
    risks: (
      <SvgIcon className={classes.sidebarIcon} viewBox="0 0 15 13.127">
        <IconRisks />
      </SvgIcon>
    ),
  };

  const ExportCmp = () => Components[type];
  return <>{ExportCmp()}</>;
};
