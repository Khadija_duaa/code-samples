import React, { useContext } from "react";
import { compose } from "redux";
import { useSelector } from "react-redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import reportingStyles from "./Reporting.style";
import SidebarContext from "../Context/sidebar.context";
import CustomMenuList from "../../../components/MenuList/CustomMenuList";
import CustomListItem from "../../../components/ListItem/CustomListItem";
import IconChart from "../../../components/Icons/IconChart";
import SvgIcon from "@material-ui/core/SvgIcon";

function Reporting(params) {
  const {
    state: { sidebarFullWidth },
    dispatch,
  } = useContext(SidebarContext);
  const { theme, classes, history } = params;
  const { sidebarPannel, activeTeam } = useSelector(state => {
    return {
      sidebarPannel: state.sidebarPannel,
      activeTeam: state.profile.data.activeTeam,
    };
  });
  let searchQuery = history.location.pathname; //getting url
  return (
    <div className={classes.reportingContainer}>
      {sidebarFullWidth && <span className={classes.teamLabel}>{`All Reports`}</span>}
      <CustomMenuList style={{ marginTop: 5 }}>
        {sidebarPannel.data.analyticsMenu.map(menu => {
          const menuName =
            activeTeam === "013f092268fb464489129c5ea19b89d3" && menu.key == "membersOverview"
              ? "Members Overview"
              : menu.key == "membersOverview"
              ? "Status Report"
              : menu.name;
          return (
            !menu.isHidden && (
              <CustomListItem
                compactMode={false}
                darkmode={true}
                isSelected={searchQuery == '/reports/' + menu.key}
                icon={
                  sidebarFullWidth ? (
                    false
                  ) : (
                    <div title={menu.name}>
                      <SvgIcon viewBox="0 0 16 16" className={classes.reportingIcon}>
                        <IconChart />
                      </SvgIcon>
                    </div>
                  )
                }
                rootProps={{
                  onClick: e => {
                    if (menu.key) history.push('/reports/' + menu.key);
                  },
                }}>
                {sidebarFullWidth && `${menuName}`}
              </CustomListItem>
            )
          );
        })}
      </CustomMenuList>
    </div>
  );
}
Reporting.defaultProps = {
  theme: {},
  classes: {},
  handleSidebarClose: () => {},
};

export default compose(withRouter, withStyles(reportingStyles, { withTheme: true }))(Reporting);
