const reportingStyles = theme => ({
  teamLabel: {
    fontSize: "13px !important",
    fontWeight: theme.typography.fontWeightExtraLight,
    fontFamily: theme.typography.fontFamilyLato,
    color: theme.palette.sidebar.labelColor,
    lineHeight: "normal",
    marginLeft: 10,
  },
  reportingContainer: {
    height: "100vh",
    paddingTop: 5,
  },
  reportingIcon: {
    marginLeft: 6,
    marginRight: "0px !important",
    fontSize: "17px !important",
  },
});

export default reportingStyles;
