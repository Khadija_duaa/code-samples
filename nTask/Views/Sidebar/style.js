const sidebarStyles = theme => ({
  parentContainer: {
    height: "100vh",
    flexGrow: 0,
    flexShrink: 0,
    minWidth: 230,
    maxWidth: 400,
    display: "flex",
    zIndex: 1111,
    position: "relative",
    overflow: 'hidden',
    "&:hover $sidebarContainerClose": {
      width: 230,
      position: "relative",
      height: "100vh",
      backgroundColor: `${theme.palette.sidebar.background}`,
      position: "fixed",
      top: 0,
      bottom: 0,
      zIndex: 1111,
      transform: "translateX(0px)",
      transition: "ease all 0.5s",
    },
  },
  roundedEdge: {
    "&:before": {
      content: '""',
      position: "absolute",
      backgroundColor: "transparent",
      top: 0,
      right: "-13px",
      height: "57px",
      width: "14px",
      borderTopLeftRadius: 25,
      boxShadow: `0 -12px 0 0 ${theme.palette.sidebar.background}`,
    },
  },
  sidebarContainerOpen: {
    // width: 220,
    position: "relative",
    height: "100%",
    backgroundColor: `${theme.palette.sidebar.background}`,
    transform: "translateX(0px)",
    flex: 1,
  },
  sidebarContainerClose: {
    width: 0,
    overflow: "hidden",
    height: "100%",
    backgroundColor: `${theme.palette.sidebar.background}`,
    position: "fixed",
    top: 0,
    bottom: 0,
    zIndex: 1111,
    transform: "translateX(-220px)",
  },
  sidebarElements: {
    height: "100%",
    width: "100%",
  },
  transitionClass: {
    transition: "ease all 0.5s",
  },
  teamViewOpenClass: {
    backgroundColor: theme.palette.sidebar.elementsBackgroundColor,
    height: "100%",
  },
  sidebarInnerPannel: {
    height: "100vh",
    width: "100%",
    display: "flex",
  },
  analyticsPannel: {
    backgroundColor: theme.palette.sidebar.elementsBackgroundColor,
    flex: 1,
    height: "100vh",
    borderRadius: "6px 0px 0px 0px",
    marginTop: 5,
  },
  analyticsPannelHalf: {
    backgroundColor: theme.palette.sidebar.elementsBackgroundColor,
    width: 48,
    height: "100vh",
    borderRadius: "6px 0px 0px 0px",
    marginTop: 5,
  },
  sidebarPannelOpen: {
    width: "42px !important",
    flex: "unset !important",
  },
  sidebarPannelClose: {
    flex: 1,
  },
  sidebarPannel: {
    width: "100%",
  },
  teamPannel: {
    width: 47,
    height: "100vh",
  },
  teamPannelOpen: {
    flex: 1,
    height: "100vh",
    backgroundColor: theme.palette.sidebar.elementsBackgroundColor,
  },
  teamPannelDefault: {
    width: "100%",
    height: "100vh",
  },
  workspaceOpenStyle: {
    width: 47,
    height: "100vh",
  },
  appSidebarResizer: {
    flexGrow: 0,
    flexShrink: 0,
    flexBasis: 6,
    justifySelf: "flex-end",
    cursor: "e-resize",
    resize: "horizontal",
    transition: "0.4s ease all",
    "&:hover": {
      backgroundColor: "#576d83",
    },
  },
  pointerEventClass: {
    pointerEvents: "none",
  },
});

export default sidebarStyles;
