const quickaccessStyles = theme => ({
  collapseWraper: {
    borderBottom: `1px solid ${theme.palette.sidebar.background}`,
    height: 'auto',
    maxHeight: '100vh',
  },
  headerTitle: {
    fontSize: "13px !important",
    color: theme.palette.sidebar.labelColor,
    fontWeight: theme.typography.fontWeightMedium,
    fontFamily: theme.typography.fontFamilyLato,
  },
  toggleCollapseIcon: {
    fontSize: "20px !important",
    color: "#fff",
    transform: "rotate(270deg)",
  },
  collapseHeader: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: 10,
    cursor: "pointer",
  },
  rotateIcon: {
    transform: "rotate(0deg)",
  },
  emptyContentCnt: {
    minHeight: 148,
    textAlign: "center",
    padding: 10,
    "& span": {
      color: "#fff",
      fontSize: "12px !important",
      fontWeight: theme.typography.fontWeightExtraLight,
      fontFamily: theme.typography.fontFamilyLato,
      marginTop: 15,
      display: "block",
      lineHeight: "normal",
      padding: "0px 7px",
    },
  },
  emptyIcon: {
    margin: "auto",
    borderRadius: "50%",
    height: 70,
    width: 70,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    background: `${theme.palette.background.green}2B`,
    "& svg": {
      fontSize: "42px !important",
      color: theme.palette.background.green,
    },
  },
  starIcon: {
    fontSize: "28px !important",
  },
  starIconSmall: {
    fontSize: "10px !important",
    marginBottom: -1,
    "& path": {
      fill: `${theme.palette.filter.starred} !important`,
    },
  },

  menuItems: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: "5px 12px",
    cursor: "pointer",
    background: "transparent",
    transition: "0.4s ease all",
    "& span": {
      color: "#fff",
      fontSize: "13px !important",
      fontWeight: theme.typography.fontWeightExtraLight,
      fontFamily: theme.typography.fontFamilyLato,
    },
    "&:hover svg": {
      visibility: "visible",
    },
    "&:hover": {
      background: theme.palette.sidebar.elementsHoverColor,
    },
  },
  optionIcon: {
    fontSize: "20px !important",
    color: "#fff",
    transform: "rotate(90deg)",
    visibility: "visible",
  },
  paperRoot: {
    backgroundColor: "#2C3C4D",
    borderRadius: 10,
  },
  favouriteItem: {
    display: "flex",
    flexDirection: "column",
    lineHeight: "normal",
    flex: 1
  },
  subHeading: {
    color: `${theme.palette.sidebar.labelColor} !important`,
  },
  sidebarFilterListItem: {
    padding: "5px 12px",
    display: "flex",
    justifyContent: "space-between",
    background: "transparent",
    transition: "0.4s ease all",
    "&:hover": {
      background: `${theme.palette.sidebar.elementsHoverColor} !important`,
    },
  },
  selectedFilter: {
    background: `${theme.palette.sidebar.elementsHoverColor} !important`,
  },
  selectedValueIcon: {
    //Adds left border to selected list item
    fontSize: "10px !important",
    position: "absolute",
    left: 10,
  },
  savedFilterListItemText: {
    fontSize: "13px !important",
    fontWeight: theme.typography.fontWeightExtraLight,
    fontFamily: theme.typography.fontFamilyLato,
    color: theme.palette.common.white,
    // width: 100,
    // overflow: "hidden",
    // whiteSpace: "nowrap",
    // lineHeight: "normal",
    // textOverflow: "ellipsis",
  },
  pushPinIcon: {
    fontSize: "14px !important",
    transform: "rotate(45deg)",
    marginRight: 2,
    color: theme.palette.filter.starred,
  },
});

export default quickaccessStyles;
