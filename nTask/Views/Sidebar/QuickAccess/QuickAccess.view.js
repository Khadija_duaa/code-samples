import React, { useContext, useState } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import styles from "./QuickAccess.style";
import SidebarContext from "../Context/sidebar.context";
import Collapse from "@material-ui/core/Collapse";
import ArrowRightIcon from "@material-ui/icons/KeyboardArrowDown";
import FilterIcon from "../../../components/Icons/AdvFilterIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import clsx from "clsx";
import FavoritesItems from "./FavoritesItems.cmp";
import SavedFiltersCmp from "./SavedFilters.cmp";

function QuickAccess(params) {
  const {
    state: { },
    dispatch,
  } = useContext(SidebarContext);

  const { theme, classes, history } = params;
  const [open, setOpen] = useState("favorite");
  const [favoriteOpen, setFavoriteOpen] = useState(true);
  const [filterOpen, setFilterOpen] = useState(true);

  // const handleItemClick = value => {
  //   if (open == value) setOpen("");
  //   else setOpen(value);
  // };

  return (
    <>
      <div className={classes.collapseWraper}>
        {/* <div className={classes.collapseHeader} onClick={e => handleItemClick("filter")}> */}
        <div className={classes.collapseHeader} onClick={e => setFilterOpen(!filterOpen)}>
          <span className={classes.headerTitle}>Quick Filters</span>
          <ArrowRightIcon
            className={clsx({
              [classes.toggleCollapseIcon]: true,
              [classes.rotateIcon]: filterOpen,
            })}
          />
        </div>
        <Collapse in={filterOpen} timeout="auto" unmountOnExit>
          <SavedFiltersCmp />
        </Collapse>
      </div>
      <div className={classes.collapseWraper}>
        {/* <div className={classes.collapseHeader} onClick={e => handleItemClick("favorite")}> */}
        <div className={classes.collapseHeader} onClick={e => setFavoriteOpen(!favoriteOpen)}>
          <span className={classes.headerTitle}>Favorites</span>
          <ArrowRightIcon
            className={clsx({
              [classes.toggleCollapseIcon]: true,
              [classes.rotateIcon]: favoriteOpen,
            })}
          />
        </div>
        <Collapse in={favoriteOpen} timeout="auto" unmountOnExit>
          <FavoritesItems />
        </Collapse>
      </div>
    </>
  );
}
QuickAccess.defaultProps = {
  theme: {},
  classes: {},
};

export default compose(withRouter, withStyles(styles, { withTheme: true }))(QuickAccess);
