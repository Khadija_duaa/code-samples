import React, { useContext } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./QuickAccess.style";
import SidebarContext from "../Context/sidebar.context";
import FilterIcon from "../../../components/Icons/AdvFilterIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Typography from "@material-ui/core/Typography";
import PushPin from "../../../components/Icons/PushPin";
import {
  setAppliedFilters,
  deleteUserFilter,
  setDefaultfilter,
  deleteUserCustomFilter,
} from "../../../redux/actions/appliedFilters";
import { SaveItemOrder } from "../../../redux/actions/itemOrder";
import MoreActionsView from "./MoreActions.view";
import cloneDeep from "lodash/cloneDeep";
import { updateTaskFilter, deleteTaskFilter, clearTaskFilter } from "../../../redux/actions/tasks";
import {
  updateIssueFilter,
  deleteIssueFilter,
  clearIssueFilter,
} from "../../../redux/actions/issues";
import {
  updateMeetingFilter,
  deleteMeetingFilter,
  clearMeetingFilter,
} from "../../../redux/actions/meetings";
import {
  updateProjectFilter,
  deleteProjectFilter,
  clearProjectFilter,
} from "../../../redux/actions/projects";
import { updateRiskFilter, clearRiskFilter } from "../../../redux/actions/risks";
import clsx from "clsx";
import { Scrollbars } from "react-custom-scrollbars";

function QuickAccess(params) {
  const {
    state: { },
    dispatch,
  } = useContext(SidebarContext);

  const dispatchFun = useDispatch();
  const storeState = useSelector(state => {
    return {
      itemOrder: state.itemOrder.data,
      appliedFilters: state.appliedFilters,
      savedIssueFilters: state.issues.savedFilters || [],
      savedMeetingFilters: state.meetings.savedFilters || [],
      savedTaskFilters: state.tasks.savedFilters || [],
      savedRiskFilters: state.risks.savedFilters || [],
      savedProjectFilters: state.projects.savedFilters || [],
      taskFilter: state.tasks.taskFilter || {},
      issueFilter: state.issues.issueFilter || {},
      meetingFilter: state.meetings.meetingFilter || {},
      projectFilter: state.projects.projectFilter || {},
      riskFilter: state.risks.riskFilter || {},
    };
  });
  const {
    itemOrder,
    appliedFilters,
    savedIssueFilters,
    savedMeetingFilters,
    savedTaskFilters,
    taskFilter,
    issueFilter,
    meetingFilter,
    projectFilter,
    riskFilter,
    savedRiskFilters,
    savedProjectFilters,
  } = storeState;
  const { theme, classes, history, match } = params;

  const getViewType = () => {
    let viewType = match.params.view;
    let viewFilter = [];
    switch (viewType) {
      case "projects":
        viewType = `Project`;
        viewFilter =
          itemOrder.userProjectFilter &&
            itemOrder.userProjectFilter.length &&
            itemOrder.userProjectFilter[0].projectFilter
            ? itemOrder.userProjectFilter[0].projectFilter
            : [];
        break;
      case "meetings":
        viewType = `Meeting`;
        viewFilter =
          itemOrder.userMeetingFilter &&
            itemOrder.userMeetingFilter.length &&
            itemOrder.userMeetingFilter[0].meetingFilter
            ? itemOrder.userMeetingFilter[0].meetingFilter
            : [];
        break;
      case "issues":
        viewType = `issue`;
        viewFilter = [];
        break;
      case "risks":
        viewType = `Risk`;
        viewFilter = [];
        break;
      case "tasks":
        viewType = `task`;
        viewFilter = [];
        break;
      default:
        viewType = ``;
        viewFilter = [];
    }
    return { viewType, viewFilter };
  };

  const handleFilterClick = (filter, type) => {
    if (type == "Meeting") applyMeetingFilter(filter);
    else setAppliedFilters(filter, type, dispatchFun);
  };

  const makeThisFilterDefault = obj => {
    let view = obj.type.toLowerCase();
    let Item = cloneDeep(obj.itemOrder);
    Item[`${view}Filter`] = { ...obj.filter, defaultFilter: true };
    SaveItemOrder(
      Item,
      resp => { },
      error => { },
      null,
      "",
      dispatchFun
    );
  };

  const handleSelectOption = (option, obj) => {
    switch (option) {
      case "deleteFilter":
        if (obj.type == "ddd") {
          deleteUserFilter(
            obj.filter,
            obj.type,
            resp => { },
            error => { },
            dispatchFun
          );
        } else {
          deleteUserCustomFilter(
            obj.filter,
            obj.type,
            resp => { },
            error => { },
            dispatchFun
          );
        }
        break;
      case "default":
        if (obj.type == "ddd") {
          makeThisFilterDefault(obj);
        } else {
          setDefaultfilter(
            obj.filter,
            obj.type,
            true,
            resp => { },
            error => { },
            dispatchFun
          );
        }
        break;
      case "unDefault":
        if (obj.type == "ddd") {
          makeThisFilterDefault(obj);
        } else {
          setDefaultfilter(
            obj.filter,
            obj.type,
            false,
            resp => { },
            error => { },
            dispatchFun
          );
        }
        break;
      default:
        break;
    }
  };

  const renderThumb = ({ style, ...props }) => {
    const thumbStyle = {
      backgroundColor: `#dbdbdb`,
      borderRadius: 6,
    };
    return <div style={{ ...style, ...thumbStyle }} {...props} />;
  };
  //Return  Active filter according to the view
  const getActiveFilter = () => {
    switch (match.params.view) {
      case "tasks":
        return savedTaskFilters;
        break;
      case "issues":
        return savedIssueFilters;
        break;
      case "meetings":
        return savedMeetingFilters;
        break;
      case "risks":
        return savedRiskFilters;
        break;
      case "projects":
        return savedProjectFilters;
        break;
      default:
        return [];
        break;
    }
  };
  //Handle Apply Task Filter
  const applyTaskFilter = filter => {
    if (taskFilter.filterName === filter.filterName) clearTaskFilter(dispatchFun);
    else updateTaskFilter(filter, dispatchFun);
  };
  //Handle Apply Issues Filter
  const applyIssueFilter = filter => {
    if (issueFilter.filterName === filter.filterName) clearIssueFilter(dispatchFun);
    else updateIssueFilter(filter, dispatchFun);
  };
  // Handle Apply Risk Filter
  const applyRiskFilter = filter => {
    if (riskFilter.filterName === filter.filterName) clearRiskFilter(dispatchFun);
    else updateRiskFilter(filter, dispatchFun);
  };
  // Handle Apply Meetings Filter
  const applyMeetingFilter = filter => {
    if (meetingFilter.filterName === filter.filterName) clearMeetingFilter(dispatchFun);
    else updateMeetingFilter(filter, dispatchFun);
  };
  // Handle Apply Project Filter
  const applyProjectFilter = filter => {
    if (projectFilter.filterName === filter.filterName) clearProjectFilter(dispatchFun);
    else updateProjectFilter(filter, dispatchFun);
  };
  const applyFilter = filter => {
    switch (match.params.view) {
      case "tasks":
        applyTaskFilter(filter);
        return;
        break;
      case "issues":
        applyIssueFilter(filter);
        return;
      case "risks":
        applyRiskFilter(filter);
        return;
        break;
      case "meetings":
        applyMeetingFilter(filter);
        return;
        break;
      case "projects":
        applyProjectFilter(filter);
        return;
        break;
    }
  };

  const view = getViewType();
  const currentViewFilter = appliedFilters[getViewType().viewType] || {};
  const options = [
    {
      label: "Delete Filter",
      value: "deleteFilter",
    },
    {
      label: "Make this Default View",
      value: "default",
    }
  ];
  const optionsUndefault = [
    {
      label: "Delete Filter",
      value: "deleteFilter",
    },
    {
      label: "Remove From Default View",
      value: "unDefault",
    }
  ];
  const getFilter = getActiveFilter();
  return (
    <>
      <Scrollbars
        autoHide
        style={{ height: "calc(50vh - 119px)" }}
        renderThumbVertical={renderThumb}>
        {view.viewFilter.length == 0 && getFilter && getFilter.length == 0 && (
          <div className={classes.emptyContentCnt}>
            <div className={classes.emptyIcon}>
              <SvgIcon htmlColor={theme.palette.icon.gray300} viewBox="0 0 24 24">
                <FilterIcon />
              </SvgIcon>
            </div>
            <span>Your saved filters will be listed here.</span>
          </div>
        )}
        {getFilter && getFilter.length && teamCanView("advanceFilterAccess") ? (
          <div>
            <List className={classes.sidebarList} disablePadding>
              {getFilter.map(filter => {
                return (
                  <ListItem
                    key={filter.filterId}
                    button
                    disableRipple={true}
                    onClick={() => applyFilter(filter)}
                    className={clsx({
                      [classes.sidebarFilterListItem]: true,
                      [classes.selectedFilter]:
                        storeState[`${view.viewType.toLowerCase()}Filter`][`filterId`] ===
                        filter.filterId,
                    })}>
                    <Typography variant="body2" className={classes.savedFilterListItemText}>
                      {filter.filterName}
                    </Typography>
                    <div className="flex_center_start_row">
                      {filter.defaultFilter ? (
                        <SvgIcon
                          viewBox="0 0 8 11.562"
                          className={classes.pushPinIcon}
                          htmlColor={theme.palette.common.white}>
                          <PushPin />
                        </SvgIcon>
                      ) : null}
                      <MoreActionsView
                        data={filter.defaultFilter ? optionsUndefault : options}
                        handleSelectOption={handleSelectOption}
                        defaultFilter={filter.defaultFilter}
                        obj={{
                          filter: filter,
                          type: view.viewType,
                          itemOrder: itemOrder,
                        }}
                      />
                    </div>
                  </ListItem>
                );
              })}
            </List>
          </div>
        ) : null}
      </Scrollbars>
    </>
  );
}
QuickAccess.defaultProps = {
  theme: {},
  classes: {},
};

export default compose(withRouter, withStyles(styles, { withTheme: true }))(QuickAccess);
