import React, { useContext, useState } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./QuickAccess.style";
import SidebarContext from "../Context/sidebar.context";
import IconBasicGreen from "../../../components/Icons/PlanIcons/IconBasicGreen";
import MoreActions from "./MoreActions.view";
import { UpdateProject, updateProjectData } from "../../../redux/actions/projects";
import { grid } from "../../../components/CustomTable2/gridInstance";
import { Scrollbars } from "react-custom-scrollbars";
import {
  taskDetailDialogState,
  issueDetailDialogState,
  riskDetailDialog,
} from "../../../redux/actions/allDialogs";
import useUpdateTask from "../../../helper/customHooks/tasks/updateTask";
import MeetingDetails from "../../Meeting/MeetingDetails/MeetingDetails";
import { UpdateMeeting, MarkMeetingAsStarted } from "../../../redux/actions/meetings";
import useUpdateIssueHook from "../../../helper/customHooks/issues/updateIssue";
import useUpdateProject from "../../../helper/customHooks/projects/updateProject";
import useUpdateMeeting from "../../../helper/customHooks/meetings/updateMeeting";
import useUpdateRisk from "../../../helper/customHooks/Risks/updateRisk";
import { MarkRiskAsStarred } from "../../../redux/actions/risks";

function FavoritesItems(params) {
  const {
    state: { },
    dispatch,
  } = useContext(SidebarContext);

  const { editTask } = useUpdateTask();
  const { editIssue } = useUpdateIssueHook();
  const { editProject } = useUpdateProject();
  const { editMeeting } = useUpdateMeeting();
  const { editRisk } = useUpdateRisk();

  const [selectedMeeting, setSelectedMeeting] = useState(false);
  const { classes, history } = params;
  let searchQuery = history.location.pathname; //getting url
  const dispatchFn = useDispatch();

  const { projects, tasks, meetings, issues, risks } = useSelector(state => {
    return {
      projects: state.projects.data,
      tasks: state.tasks.data || [],
      meetings: state.meetings.data || [],
      issues: state.issues.data || [],
      risks: state.risks.data || [],
    };
  });

  const renderThumb = ({ style, ...props }) => {
    const thumbStyle = {
      backgroundColor: `#dbdbdb`,
      borderRadius: 6,
    };
    return <div style={{ ...style, ...thumbStyle }} {...props} />;
  };

  const favProjects = projects.filter(starProj => {
    return starProj.isStared && !starProj.isDeleted;
  });

  const favTasks = tasks.filter(starTask => {
    return starTask.isStared && !starTask.isDeleted;
  });

  const favMeeting = meetings.filter(starMeeting => {
    return starMeeting.isStared && !starMeeting.isDelete;
  });

  const favIssue = issues.filter(starIssue => {
    return starIssue.isStared && !starIssue.isArchive;
  });

  const favRisk = risks.filter(starRisk => {
    return starRisk.isStared && !starRisk.isArchive;
  });

  const handleFavProjectClick = project => {
    history.push({
      pathname: "/projects",
      search: `?projectId=${project.projectId}`,
    });
  };

  const handleFavTaskClick = task => {
    taskDetailDialogState(dispatchFn, {
      id: task.taskId,
      afterCloseCallBack: () => { },
      type: "comment",
    });
  };

  const handleFavIssueClick = issue => {
    issueDetailDialogState(dispatchFn, {
      id: issue.id,
      afterCloseCallBack: () => { },
    });
  };

  const handleFavMeetingClick = meeting => {
    setSelectedMeeting(meeting);
  };

  const handleFavRiskClick = risk => {
    riskDetailDialog(
      {
        id: risk.id,
        afterCloseCallBack: () => { },
      },
      dispatchFn
    );
  };

  const handleSelectOption = (option, obj, type) => {
    if (type == "project") {
      editProject(obj, "isStared", false);
    } else if (type == "task") {
      editTask(obj, "isStared", false);
    } else if (type == "meeting") {
      editMeeting(obj, "isStared", false)
    } else if (type == "issue") {
      editIssue(obj, "isStared", false);
    } else if (type == "risk") {
      editRisk(obj, "isStared", false);
    }
  };

  const handleCloseMeeting = () => {
    setSelectedMeeting(null);
  };

  const isUpdated = data => {
    UpdateMeeting(data, (err, data) => { }, dispatchFn);
  };

  const updateMeetingAttendee = (assignedTo, meeting, add = false, succFun = () => { }) => {
    let assignedToArr = assignedTo.map(ass => {
      return ass.userId;
    });
    let newMeetingObj;
    if (add) {
      newMeetingObj = {
        ...meeting,
        attendeeList: [...meeting.attendeeList, ...assignedToArr],
      };
    } else {
      newMeetingObj = {
        ...meeting,
        attendeeList: assignedToArr /** added updated attendee id's to attendeelist (array)   */,
      };
    }
    UpdateMeeting(newMeetingObj, (success, data) => {
      succFun();
    });
  };
  const selectedView =
    searchQuery == "/projects" || searchQuery == "/boards"
      ? "project"
      : searchQuery == "/tasks"
        ? "task"
        : searchQuery == "/meetings"
          ? "meeting"
          : searchQuery == "/issues"
            ? "issue"
            : searchQuery == "/risks"
              ? "risk"
              : "item";

  return (
    <>
      <Scrollbars
        autoHide
        style={{ height: "calc(50vh - 119px)" }}
        renderThumbVertical={renderThumb}>
        {favProjects.length && selectedView == "project" ? (
          favProjects.map(project => {
            return (
              <div className={classes.menuItems}>
                <div
                  className={classes.favouriteItem}
                  onClick={e => handleFavProjectClick(project)}>
                  <span>{project.projectName}</span>
                  <span className={classes.subHeading}>Project</span>
                </div>
                <MoreActions
                  data={[{ label: "Remove Favorite", value: "removeFvrt" }]}
                  handleSelectOption={(option, obj) => handleSelectOption(option, obj, "project")}
                  obj={project}
                />
              </div>
            );
          })
        ) : favTasks.length && selectedView == "task" ? (
          favTasks.map(task => {
            return (
              <div className={classes.menuItems}>
                <div className={classes.favouriteItem} onClick={e => handleFavTaskClick(task)}>
                  <span>{task.taskTitle}</span>
                  <span className={classes.subHeading}>Task</span>
                </div>
                <MoreActions
                  data={[{ label: "Remove Favorite", value: "removeFvrt" }]}
                  handleSelectOption={(option, obj) => handleSelectOption(option, obj, "task")}
                  obj={task}
                />
              </div>
            );
          })
        ) : favMeeting.length && selectedView == "meeting" ? (
          favMeeting.map(meeting => {
            return (
              <div className={classes.menuItems}>
                <div
                  className={classes.favouriteItem}
                  onClick={e => handleFavMeetingClick(meeting)}>
                  <span>{meeting.meetingDisplayName}</span>
                  <span className={classes.subHeading}>Meeting</span>
                </div>
                <MoreActions
                  data={[{ label: "Remove Favorite", value: "removeFvrt" }]}
                  handleSelectOption={(option, obj) => handleSelectOption(option, obj, "meeting")}
                  obj={meeting}
                />
              </div>
            );
          })
        ) : favRisk.length && selectedView == "risk" ? (
          favRisk.map(risk => {
            return (
              <div className={classes.menuItems}>
                <div className={classes.favouriteItem} onClick={e => handleFavRiskClick(risk)}>
                  <span>{risk.title}</span>
                  <span className={classes.subHeading}>Risk</span>
                </div>
                <MoreActions
                  data={[{ label: "Remove Favorite", value: "removeFvrt" }]}
                  handleSelectOption={(option, obj) => handleSelectOption(option, obj, "risk")}
                  obj={risk}
                />
              </div>
            );
          })
        ) : favIssue.length && selectedView == "issue" ? (
          favIssue.map(issue => {
            return (
              <div className={classes.menuItems}>
                <div className={classes.favouriteItem} onClick={e => handleFavIssueClick(issue)}>
                  <span>{issue.title}</span>
                  <span className={classes.subHeading}>Issue</span>
                </div>
                <MoreActions
                  data={[{ label: "Remove Favorite", value: "removeFvrt" }]}
                  handleSelectOption={(option, obj) => handleSelectOption(option, obj, "issue")}
                  obj={issue}
                />
              </div>
            );
          })
        ) : (
          <div className={classes.emptyContentCnt}>
            <div className={classes.emptyIcon}>
              <IconBasicGreen className={classes.starIcon} />
            </div>
            <span>
              {`Add any ${selectedView} to Favorites by clicking the`}{" "}
              <IconBasicGreen className={classes.starIconSmall} /> icon.
            </span>
          </div>
        )}
      </Scrollbars>
      {selectedMeeting ? (
        <MeetingDetails
          isUpdated={isUpdated}
          closeMeetingDetailsPopUp={handleCloseMeeting}
          currentMeeting={selectedMeeting}
          listView={true}
          isArchivedSelected={false}
          isInstanceChange={false}
          filterMeeting={[]}
          UnArchiveMeeting={() => { }}
          updateMeetingAttendee={updateMeetingAttendee}
        />
      ) : null}
    </>
  );
}
FavoritesItems.defaultProps = {
  theme: {},
  classes: {},
};

export default compose(withRouter, withStyles(styles, { withTheme: true }))(FavoritesItems);
