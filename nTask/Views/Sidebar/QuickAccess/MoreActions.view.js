import React, { useState } from "react";
import dropdownStyle from "./QuickAccess.style";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import DropdownMenu from "../../../components/Dropdown/DropdownMenu";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";

import CustomIconButton from "../../../components/Buttons/IconButton";
import CustomMenuList from "../../../components/MenuList/CustomMenuList";
import CustomListItem from "../../../components/ListItem/CustomListItem";

function MoreActionDropDown(props) {
  const { classes, theme, data = [], handleSelectOption, obj = {} } = props;
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = event => {
    // Function Opens the dropdown
    event.stopPropagation();
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };
  const handleClose = event => {
    // Function closes dropdown
    if(event){
      const isExist = event.target.id == "moreActionBtn" || event.target.closest("#moreActionBtn");
      if (isExist) {
        return;
      }
      setAnchorEl(null);
    }
  };

  const open = Boolean(anchorEl);

  return (
    <>
      <CustomIconButton
        btnType="condensed"
        style={{ padding: 0 }}
        onClick={handleClick}
        buttonRef={anchorEl}
        id="moreActionBtn">
        <MoreVerticalIcon className={classes.optionIcon} />
      </CustomIconButton>
      <DropdownMenu
        open={open}
        closeAction={handleClose}
        anchorEl={anchorEl}
        size={"small"}
        disablePortal={false}
        paperProps={{
          classes: {
            root: classes.paperRoot,
          },
        }}
        placement="bottom-start">
        <CustomMenuList>
          {data.map(item => {
            return (
              <CustomListItem
                compactMode={false}
                darkmode={true}
                rootProps={{
                  onClick: e => {
                    handleSelectOption(item.value, obj);
                    handleClose();
                  },
                }}>
                {item.label}
              </CustomListItem>
            );
          })}
        </CustomMenuList>
      </DropdownMenu>
    </>
  );
}

export default compose(withStyles(dropdownStyle, { withTheme: true }))(MoreActionDropDown);
