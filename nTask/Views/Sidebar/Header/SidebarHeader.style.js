const sidebarStyles = theme => ({
  sidebarHeaderCnt: {
    height: 40,
    width: "100%",
    boxShadow: `0px 1px 0px ${theme.palette.sidebar.shadow}`,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  logoDefault: {
    marginLeft: 10,
    cursor: "pointer",
  },
  whiteLabelLogo: {
    width: 90,
    // backgroundColor: "white",
  },
  iconMenu: {
    marginRight: 15,
    cursor: "pointer",
  },
  bootstrapTooltip: {
    backgroundColor: `${theme.palette.common.white} !important`,
    color: "black !important",
  },
});

export default sidebarStyles;
