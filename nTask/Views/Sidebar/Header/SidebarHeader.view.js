import React, { useContext } from "react";
import { compose } from "redux";
import { useSelector } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import sidebarStyles from "./SidebarHeader.style";
import SidebarContext from "../Context/sidebar.context";
import nTaskLogo from "../../../assets/images/nTask_Logo_v3.svg";
import IconMenu from "../../../assets/images/iconMenu.svg";
import IconMenuGrey from "../../../assets/images/iconMenuGrey.svg";
import CustomTooltip from "../../../components/Tooltip/Tooltip.js";
import clsx from "clsx";

function SidebarHeader(params) {
  const {
    state: { },
    dispatch,
  } = useContext(SidebarContext);
  const { whiteLabelTeam } = useSelector(state => {
    return {
      whiteLabelTeam: state.whiteLabelInfo.data,
    };
  });

  const { theme, classes, handleSidebarClose, sidebarPannel } = params;
  return (
    <div className={classes.sidebarHeaderCnt} style={whiteLabelTeam ? { backgroundColor: whiteLabelTeam.backgroudColor } : {}}>
      {whiteLabelTeam /** if user is enterprise then show its logo */ ? (
        <img
          src={whiteLabelTeam.headerImageUrl}
          alt={whiteLabelTeam.companyName}
          style={whiteLabelTeam.sideBarLogoWidth ? { width: whiteLabelTeam.sideBarLogoWidth } : {}}
          className={clsx({ [classes.logoDefault]: true,[classes.whiteLabelLogo]: true })}
        />
      ) : (
        <img src={nTaskLogo} alt="nTask Logo" className={classes.logoDefault} />
      )}

      {sidebarPannel.open && (
        <CustomTooltip
          helptext={"Close Sidebar"}
          iconType="help"
          placement="top"
          // classes={classes}
          style={{ color: theme.palette.common.white }}>
          <img
            src={whiteLabelTeam ? IconMenuGrey : IconMenu}
            alt="Menu Icon"
            className={classes.iconMenu}
            onClick={handleSidebarClose}
          />
        </CustomTooltip>
      )}
    </div>
  );
}
SidebarHeader.defaultProps = {
  theme: {},
  classes: {},
  handleSidebarClose: () => { },
};

export default compose(withStyles(sidebarStyles, { withTheme: true }))(SidebarHeader);
