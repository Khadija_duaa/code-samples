const sidebarMenuStyles = theme => ({
  quickAccess: {
    borderBottom: `1px solid ${theme.palette.sidebar.elementsBackgroundColor}`,
    // borderTop: `1px solid ${theme.palette.sidebar.elementsBackgroundColor}`,
  },
  sidebarIcon: {
    marginLeft: 4,
  },
  menuCnt:{
    borderBottom: `1px solid ${theme.palette.sidebar.elementsBackgroundColor}`,
  }
});

export default sidebarMenuStyles;
