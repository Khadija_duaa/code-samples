import React, { useContext, useState, useEffect } from "react";
import { compose } from "redux";
import { useSelector, useDispatch } from "react-redux";

import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import sidebarMenuStyles from "./SidebarMenu.style";
import SidebarContext from "../Context/sidebar.context";
import CustomListItem from "../../../components/ListItem/CustomListItem";
import CustomMenuList from "../../../components/MenuList/CustomMenuList";
import { updateSettings } from "../Context/actions/sidebar.actions";
import { getMenuIcons } from "../constants";
import clsx from "clsx";
import isEmpty from "lodash/isEmpty";
import { clearKanbanStoreData } from "../../../redux/actions/boards";

function SidebarMenu(params) {
  const {
    state: { innerSidebar, sidebarFullWidth, teamView, workspaceView, quickAccess },
    dispatch,
  } = useContext(SidebarContext);
  const dispatchFn = useDispatch();

  const { classes, history } = params;
  const [quickAccessState, setQuickAccess] = useState(false);
  const { sidebarPannel, profile, board, whiteLabelTeam } = useSelector(state => {
    return {
      sidebarPannel: state.sidebarPannel,
      profile: state.profile.data,
      board: state.boards,
      whiteLabelTeam: state.whiteLabelInfo.data,
    };
  });

  const handleCollaborationClick = () => {
    history.push("/collaboration")
  }

  const handleClickTeamMenu = menu => {
    /** function call on selection of team menus */
    if (menu.url && menu.key == "reporting") {
      // history.push("/reports/overview");
    } else if (menu.url) {
      history.push(menu.url);
    }
    updateSettings(dispatch, {
      innerSidebar: menu.key == "reporting" ? true : false,
      quickAccess: false,
      workspaceView: false,
      teamView: false,
    });
    setQuickAccess(false);
  };
  const handleWorkspaceMenu = menuItem => {
    /** function calls on selection of workspace menu */
    if (menuItem.url.includes("/boards") && !isEmpty(board.data.kanban)) {
      clearKanbanStoreData(dispatchFn);
    }
    history.push(menuItem.url);
    updateSettings(dispatch, {
      innerSidebar: false,
      workspaceView: false,
      teamView: false,
      quickAccess: false,
      sidebarFullWidth: true,
    });
    setQuickAccess(false);
  };
  const hanldeClickQuickAccess = () => {
    /** function calls on selection of quick access */
    setQuickAccess(!quickAccessState);
    updateSettings(dispatch, {
      innerSidebar: !quickAccessState,
      workspaceView: false,
      teamView: false,
      quickAccess: !quickAccessState,
      sidebarFullWidth: true,
    });
  };
  const handleSurvey = () => {
    /** function calls on selection of surveys */
    history.push("/forms");
  };
  const handleResources = () => {
    /** function calls on selection of Resource Management */
    history.push("/resources");
  };
  useEffect(() => {
    setQuickAccess(quickAccess);
  }, [quickAccess]);

  let searchQuery = history.location.pathname; //getting url
  const extendedView =
    teamView || workspaceView
      ? false
      : !innerSidebar
        ? true
        : innerSidebar && sidebarFullWidth
          ? false
          : true;
  return profile.workspace?.length ? (
    <CustomMenuList style={{ marginTop: 5 }}>
      {[
        "8c1d63c294c5496c910e4c2b405ab2a9",
        "9aa989d4f15247c586de45c174d2094b",
        "9892d3cf5f5e4a9786172841dc4646f3",
        "cf8b974c5ae54c1eac6ab05af83cdc59",
        "28662a47fc10441fbf07a6fefd0fe428",
      ].includes(profile.activeTeam) && <div
        className={clsx({
          [classes.quickAccess]: true,
        })}
        style={{ border: "none" }}
        title={"Forms"}>
          <CustomListItem
            compactMode={false}
            darkmode={true}
            isSelected={!quickAccessState && searchQuery.includes("/forms")}
            icon={getMenuIcons("forms", classes)}
            rootProps={{
              onClick: handleSurvey,
            }}>
            {extendedView && `Forms`}
          </CustomListItem>
        </div>}
      {true && <div
        className={clsx({
          [classes.quickAccess]: true,
        })}
        style={{ border: "none" }}
        title={"Resource Workload"}>
        <CustomListItem
          compactMode={false}
          darkmode={true}
          isSelected={!quickAccessState && searchQuery.includes("/resources")}
          icon={getMenuIcons("resources", classes)}
          rootProps={{
            onClick: handleResources,
          }}>
          {extendedView && `Resource Workload`}
        </CustomListItem>
      </div>}
      <div title={"Collaboration"}
        className={clsx({
          [classes.quickAccess]: true,
        })}
        style={{ border: "none" }}
      >
        <CustomListItem
          compactMode={false}
          darkmode={true}
          isSelected={!quickAccessState && searchQuery.includes("/collaboration")}
          icon={getMenuIcons("resources", classes)}
          rootProps={{
            onClick: e => {
              handleCollaborationClick();
            },
          }}>
          {extendedView && "Collaboration"}
        </CustomListItem>
      </div>
      <div
        className={clsx({
          [classes.menuCnt]: sidebarPannel.data?.teamMenu.length,
        })}>
        {sidebarPannel.data.teamMenu.map(menu => {
          return (
            !menu.isHidden && menu.key != 'documents' ? (
              <div title={menu.name}>
                <CustomListItem
                  compactMode={false}
                  darkmode={true}
                  isSelected={!quickAccessState && menu.url && searchQuery.startsWith(menu.url)}
                  icon={getMenuIcons(menu.key, classes)}
                  rootProps={{
                    onClick: e => handleClickTeamMenu(menu),
                  }}>
                  {extendedView && `${menu.name}`}
                </CustomListItem>
              </div>
            )
              : menu.key == 'documents' && ENV === 'dev' // && false 
                ? <div title={menu.name}>
                  <CustomListItem
                    compactMode={false}
                    darkmode={true}
                    isSelected={!quickAccessState && menu.url && searchQuery.startsWith(menu.url)}
                    icon={getMenuIcons(menu.key, classes)}
                    rootProps={{
                      onClick: e => handleClickTeamMenu(menu),
                    }}>
                    {extendedView && `${menu.name}`}
                  </CustomListItem>
                </div>
                : ''
          );
        })}
      </div>

      <div
        className={clsx({
          [classes.quickAccess]: true,
        })}
        title={"Quick Access"}>
        <CustomListItem
          compactMode={false}
          darkmode={true}
          isSelected={quickAccessState}
          icon={
            whiteLabelTeam && whiteLabelTeam.quickAccessImageUrl ?
              <img
                src={whiteLabelTeam.quickAccessImageUrl}
                alt={whiteLabelTeam.companyName}
                className={classes.sidebarIcon}
              /> : getMenuIcons("quickAccess", classes)
          }
          rootProps={{
            onClick: hanldeClickQuickAccess,
          }}>
          {extendedView && `Quick Access`}
        </CustomListItem>
      </div>

      <div className={classes.menuCnt}>
        {sidebarPannel.data.workspaceMenu.map(menuItem => {
          return (
            !menuItem.isHidden && (
              <div title={menuItem.name}>
                <CustomListItem
                  compactMode={false}
                  darkmode={true}
                  isSelected={!quickAccessState && searchQuery.startsWith(menuItem.url)}
                  icon={getMenuIcons(menuItem.key, classes)}
                  rootProps={{
                    onClick: e => {
                      handleWorkspaceMenu(menuItem);
                    },
                  }}>
                  {extendedView && menuItem.name}
                </CustomListItem>
              </div>
            )
          );
        })}
      </div>
    </CustomMenuList>
  ) : null;
}
SidebarMenu.defaultProps = {
  theme: {},
  classes: {},
};

export default compose(withRouter, withStyles(sidebarMenuStyles, { withTheme: true }))(SidebarMenu);
