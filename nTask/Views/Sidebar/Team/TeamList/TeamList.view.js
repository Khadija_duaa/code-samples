import React, { useState, useContext } from "react";
import { compose } from "redux";
import { connect, useDispatch } from "react-redux";
import { withRouter } from "react-router";
import { withSnackbar } from "notistack";

import { withStyles } from "@material-ui/core/styles";
import teamListStyles from "./TeamList.style";
import DefaultTextField from "../../../../components/Form/TextField";

import DefaultDialog from "../../../../components/Dialog/Dialog";

import CustomAvatar from "../../../../components/Avatar/Avatar";
import ConstantsPlan from "../../../../components/constants/planConstant";
import SidebarContext from "../../Context/sidebar.context";
import SearchInput, { createFilter } from "react-search-input";
import {
  switchTeam,
  whiteLabelInfo,
  DefaultWhiteLabelInfo,
} from "./../../../../redux/actions/teamMembers";
import { clearBoardsData } from "./../../../../redux/actions/boards";
import { FetchUserInfo } from "./../../../../redux/actions/profile";
import WorkspaceIcon from "../../../../components/Icons/WorkspaceIcon";
import { Scrollbars } from "react-custom-scrollbars";
import SettingsIcon from "@material-ui/icons/Settings";
import AddIcon from "@material-ui/icons/AddCircleRounded";
import { FormattedMessage } from "react-intl";
import CreateTeamForm from "../../../../Views/AddNewForms/addNewTeamFrom";
import SvgIcon from "@material-ui/core/SvgIcon";
import { getTasks } from "./../../../../redux/actions/tasks";
import { getCustomField } from "./../../../../redux/actions/customFields";
import { FetchWorkspaceInfo } from "./../../../../redux/actions/workspace";
import { clearAllAppliedFilters } from "./../../../../redux/actions/appliedFilters";

import Basic from "../../../../assets/images/plans/icon_Basic_Green.svg";
import Business from "../../../../assets/images/plans/icon_Business_yellow.svg";
import Enterprise from "../../../../assets/images/plans/icon_Enterpise_Yellow.svg";
import Premium from "../../../../assets/images/plans/icon_Premium_yellow.svg";
import Basic_Badge from "../../../../assets/images/plans/Basic_Badge.svg";
import Business_Badge from "../../../../assets/images/plans/Business_Badge.svg";
import Enterprise_Badge from "../../../../assets/images/plans/Enterprise_Badge.svg";
import Premium_Badge from "../../../../assets/images/plans/Premium_Badge.svg";

function TeamList(params) {
  const {
    state: { },
    dispatch,
    showLoadingState,
    hideLoadingState,
    planExpire,
  } = useContext(SidebarContext);
  const {
    classes,
    theme,
    profile,
    history,
    clearBoardsData,
    FetchUserInfo,
    gracePeriod,
    allMembers,
    getTasks,
    getCustomField,
    FetchWorkspaceInfo,
    clearAllAppliedFilters,
    enqueueSnackbar,
  } = params;
  const [searchTerm, setSearchTerm] = useState("");
  const [createTeamDialog, setCreateTeamDialog] = useState(false);
  const dispatchFun = useDispatch();

  const searchUpdated = e => {
    setSearchTerm(e.target.value);
  };
  const getPlanImage = (plan, whiteLabelTeam) => {
    if (whiteLabelTeam) {
      return Enterprise;
    } else if (ConstantsPlan.isPlanFree(plan)) {
      return Basic;
    } else if (ConstantsPlan.isPremiumPlan(plan) || ConstantsPlan.isPremiumTrial(plan)) {
      return Premium;
    } else if (ConstantsPlan.isBusinessPlan(plan) || ConstantsPlan.isBusinessTrial(plan)) {
      return Business;
    } else if (ConstantsPlan.isPlanTrial(plan)) {
      return Basic;
    }
  };
  const getPlanLabel = (plan, whiteLabelTeam) => {
    if (whiteLabelTeam) {
      return Enterprise_Badge;
    } else if (ConstantsPlan.isPlanFree(plan)) {
      return Basic_Badge;
    } else if (ConstantsPlan.isPremiumPlan(plan) || ConstantsPlan.isPremiumTrial(plan)) {
      return Premium_Badge;
    } else if (ConstantsPlan.isBusinessPlan(plan) || ConstantsPlan.isBusinessTrial(plan)) {
      return Business_Badge;
    } else if (ConstantsPlan.isPlanTrial(plan)) {
      return Basic_Badge;
    }
  };

  const selectedTeam = teamItem => {
    showLoadingState();
    switchTeam(teamItem.companyId, 
      response => {
        if (teamItem.isEnableWhiteLabeling) {
          whiteLabelInfo(teamItem.companyId, succ => { }, dispatchFun);
        } else {
          DefaultWhiteLabelInfo(dispatchFun);
        }
        history.push(`/teams/${response.data.team.companyUrl}`);
        clearBoardsData(false);

        const { workspaces } = response.data;

        if (workspaces.length) {
          FetchWorkspaceInfo(
            workspaces[0].teamId,
            () => {
              /** fetching all user created custom fields */
              getTasks(
                null,
                null,
                () => { },
                () => { }
              );
              getCustomField(
                succ => { },
                fail => { }
              );
              clearAllAppliedFilters(null);
            },
            error => { }
          );
        }

        FetchUserInfo(() => {
          const { paymentPlanTitle, currentPeriondEndDate } = response.data.team.subscriptionDetails;
          if (
            response.data.team &&
            response.data.team.isStatusMappingRequired &&
            ConstantsPlan.DisplayPlanName(paymentPlanTitle) !== "Business"
          ) {
            showLoadingState();
            planExpire("StatusMapping");
          }
          if (!ConstantsPlan.isPlanFree(paymentPlanTitle)) {
            if (
              ConstantsPlan.isPlanTrial(paymentPlanTitle) &&
              ConstantsPlan.isPlanExpire(currentPeriondEndDate)
            ) {
              showLoadingState();
              planExpire("Trial");
              return;
            } else if (
              ConstantsPlan.isPlanPaid(paymentPlanTitle) &&
              ConstantsPlan.isPlanExpire(currentPeriondEndDate, gracePeriod)
            ) {
              showLoadingState();
              planExpire("Paid");
              return;
            }
          }
        });
      }, err => {
        hideLoadingState();
        if (err && err.data) showSnackBar(err.data, "error");
      });
  };
  const renderThumb = ({ style, ...props }) => {
    const thumbStyle = {
      backgroundColor: `#dbdbdb`,
      borderRadius: 6,
    };
    return <div style={{ ...style, ...thumbStyle }} {...props} />;
  };
  const handleTeamDialogOpen = e => {
    setCreateTeamDialog(true);
  };
  const handleTeamDialogClose = e => {
    setCreateTeamDialog(false);
  };
  const teamSetting = () => {
    if (history.location.pathname.includes('team-settings')) {
      return
    } history.push("/team-settings");
  }
  const showSnackBar = (snackBarMessage = "", type = null, options) => {
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "top",
          horizontal: "centerdp",
        },
        variant: type ? type : "info",
        ...options
      }
    );
  };
  const filteredData = profile.teams.filter(createFilter(searchTerm, ["companyName"]));
  let loggedInOrganization = profile.teams.find(n => {
    return profile.activeTeam == n.companyId;
  });
  const currentUser = profile.teamMember.find(m => {
    return profile.userId == m.userId;
  });
  return (
    <>
      <div className={classes.teamContent}>
        <Scrollbars
          autoHide
          style={{ height: "calc(100vh - 95px)" }}
          renderThumbVertical={renderThumb}>
          <div style={{ padding: "5px 0px" }} className={classes.opendState}>
            <div
              className={classes.teamSettingsLabel}
              onClick={() => history.push(`/teams/${loggedInOrganization.companyUrl}`)}>
              <SvgIcon
                viewBox="0 0 16 15.188"
                htmlColor={theme.palette.secondary.light}
                className={classes.workspaceIcon}>
                <WorkspaceIcon />
              </SvgIcon>
              <span className={classes.settingLabel}>Workspace Dashboard</span>
            </div>
            {(currentUser.role == "Owner" || currentUser.role == "Admin") && (
              <div
                className={classes.teamSettingsLabel}
                onClick={teamSetting}>
                <SettingsIcon />
                <span className={classes.settingLabel}>Team Settings</span>
              </div>
            )}
            <div className={classes.teamSettingsLabel} onClick={handleTeamDialogOpen}>
              <AddIcon style={{ color: theme.palette.background.green }} />
              <span className={classes.settingLabel}>Create Team</span>
            </div>
          </div>
          <div>
            <div className={classes.teamSettingsContainer}>
              <span className={classes.teamLabel} style={{ marginLeft: 10 }}>{`Others (${profile.teams.length == 0 ? 0 : profile.teams.length - 1
                })`}</span>
            </div>
            <div>
              <DefaultTextField
                error={false}
                errorState={false}
                errorMessage={""}
                formControlStyles={{ marginBottom: 0, minHeight: 37 }}
                styles={classes.searchInput}
                transparentInput={true}
                defaultProps={{
                  type: "text",
                  placeholder: `Search`,
                  value: searchTerm,
                  autoFocus: true,
                  inputProps: { maxLength: 80 },
                  onChange: e => {
                    searchUpdated(e);
                  },
                }}
              />

              {filteredData.length ? (
                filteredData.map((team, index) => {
                  if (team.companyId == loggedInOrganization.companyId || team.isDeleted) return;
                  return (
                    <div className={classes.teamElements} onClick={() => selectedTeam(team)} id={`teamSidebarMenu${index}${team.companyId}`}>
                      <CustomAvatar
                        otherMember={{
                          imageUrl: team.companyImage ? team.companyImage.pictureUrl : "",
                          fullName: team.companyName,
                          lastName: "",
                          email: team.companyUrl,
                          isOnline: false,
                        }}
                        size="xsmall"
                        hideBadge={true}
                        styles={{ marginLeft: 5 }}
                      />
                      <div className={classes.teamInfo}>
                        <span
                          className={classes.teamName}
                          title={team.companyName}>{`${team.companyName || "-"}`}</span>
                        <span className={classes.teamLabel} title={team.teamOwner}>{team.teamOwner || "-"}</span>
                      </div>
                      <div className={classes.arrowsCnt}>
                        <img
                          src={getPlanImage(
                            team.subscriptionDetails.paymentPlanTitle,
                            team.isEnableWhiteLabeling
                          )}
                          onMouseEnter={e => {
                            e.currentTarget.src = getPlanLabel(
                              team.subscriptionDetails.paymentPlanTitle,
                              team.isEnableWhiteLabeling
                            );
                          }}
                          onMouseOut={e => {
                            e.currentTarget.src = getPlanImage(
                              team.subscriptionDetails.paymentPlanTitle,
                              team.isEnableWhiteLabeling
                            );
                          }}
                        />
                      </div>
                    </div>
                  );
                })
              ) : filteredData.length == 0 && profile.teams.length ? (
                <div className={classes.emptyState}>
                  <span className={classes.teamLabel}>{"No Results Found!"}</span>
                </div>
              ) : null}
            </div>
          </div>
        </Scrollbars>
      </div>
      <DefaultDialog
        title={
          <FormattedMessage
            id="team-settings.team-creation-dialog.title"
            defaultMessage="Create Team"
          />
        }
        sucessBtnText="Create Team"
        open={createTeamDialog}
        onClose={handleTeamDialogClose}>
        <CreateTeamForm handleDialogClose={handleTeamDialogClose} />
      </DefaultDialog>
    </>
  );

}
TeamList.defaultProps = {
  classes: {},
  theme: {},
};

const mapStateToProps = state => {
  return {
    profile: state.profile.data,
    allMembers: state.profile.data.teamMember,
  };
};

export default compose(
  withRouter,
  withSnackbar,
  connect(mapStateToProps, {
    clearBoardsData,
    FetchUserInfo,
    FetchWorkspaceInfo,
    getTasks,
    getCustomField,
    clearAllAppliedFilters,
  }),
  withStyles(teamListStyles, { withTheme: true })
)(TeamList);
