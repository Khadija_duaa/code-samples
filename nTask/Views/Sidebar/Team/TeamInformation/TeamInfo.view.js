import React, { useContext } from "react";
import { compose } from "redux";
import { withRouter } from "react-router";

import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import teamInfoStyles from "./TeamInfo.style";
import clsx from "clsx";
import CustomAvatar from "../../../../components/Avatar/Avatar";
import ArrowRightIcon from "@material-ui/icons/KeyboardArrowDown";
import SidebarContext from "../../Context/sidebar.context";
import { updateSettings } from "../../Context/actions/sidebar.actions";

function TeamInfo(params) {
  const {
    state: { teamView },
    dispatch,
  } = useContext(SidebarContext);
  const { classes, theme, profile, history } = params;

  let team = profile.teams.find(n => {
    return profile.activeTeam == n.companyId;
  });

  const handleOpenSettings = () => {
    updateSettings(dispatch, {
      teamView: !teamView,
      workspaceView: false,
      innerSidebar: false,
      sidebarFullWidth: true,
      quickAccess: false,
    });
  };

  return (
    <>
      <div
        className={clsx({ [classes.teamCont]: true, [classes.openView]: teamView })}
        onClick={handleOpenSettings}>
        <div className={classes.teamElements}>
          <CustomAvatar size="small" userActiveTeam={true} styles={{ marginLeft: 5 }} />
          <div className={classes.teamInfo}>
            <span className={classes.teamLabel}>{"Team"}</span>
            <span
              className={classes.teamName}
              title={team.companyName}>{`${team.companyName}`}</span>
          </div>
          <div className={classes.arrowsCnt}>
            <ArrowRightIcon
              className={clsx({
                [classes.arrowIconRight]: !teamView,
                [classes.arrowIconLeft]: teamView,
              })}
            />
          </div>
        </div>
      </div>
    </>
  );
}
TeamInfo.defaultProps = {
  classes: {},
  theme: {},
};

const mapStateToProps = state => {
  return {
    profile: state.profile.data,
  };
};

export default compose(
  withRouter,
  connect(mapStateToProps, {}),
  withStyles(teamInfoStyles, { withTheme: true })
)(TeamInfo);
