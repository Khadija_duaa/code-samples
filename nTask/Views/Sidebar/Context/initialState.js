const initialState = {
  teamView: false,
  workspaceView: false,
  innerSidebar: false,
  sidebarFullWidth: true,
  customizeMenu: false,
  quickAccess: false,
  menuItems: {},
};
export default initialState;
