import { UPDATESETTINGS } from "./constants";
import initialState from "./initialState";

function reducer(state, action) {
  switch (action.type) {
    case UPDATESETTINGS:
      return { ...state, ...action.payload };
      break;

    default:
      return { ...state };
      break;
  }
}
export default reducer;
