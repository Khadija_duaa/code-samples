import { UPDATESETTINGS } from "../constants";

export const updateSettings = (dispatch, obj) => {
  dispatch({ type: UPDATESETTINGS, payload: obj });
};
