import React, { useContext } from "react";
import { compose } from "redux";
import { withRouter } from "react-router";

import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import workspaceInfoStyles from "./WorkspaceInfo.style";
import clsx from "clsx";
import CustomAvatar from "../../../../components/Avatar/Avatar";
import ArrowRightIcon from "@material-ui/icons/KeyboardArrowDown";

import WorkspaceList from "../WorkspaceList/WorkspaceList.view";
import SidebarContext from "../../Context/sidebar.context";
import { updateSettings } from "../../Context/actions/sidebar.actions";

function WorkspaceInfo(params) {
  const {
    state: { workspaceView, teamView },
    dispatch,
  } = useContext(SidebarContext);
  const { classes, theme, profile, history } = params;

  let team =
    profile?.workspace?.find(n => {
      return profile.loggedInTeam == n.teamId;
    }) || {};

  const handleOpenSettings = () => {
    updateSettings(dispatch, {
      workspaceView: !workspaceView,
      teamView: false,
      innerSidebar: false,
      sidebarFullWidth: true,
      quickAccess: false,
    });
  };

  return (
    <>
      <div
        className={clsx({
          [classes.teamCont]: true,
          [classes.openView]: workspaceView,
          [classes.teamViewOpen]: teamView,
        })}
        onClick={handleOpenSettings}>
        <div className={classes.teamElements}>
          <CustomAvatar
            size="small"
            loggedInTeam={true}
            styles={teamView ? { marginLeft: 3 } : { marginLeft: 5 }}
          />
          <div className={classes.teamInfo}>
            <span className={classes.teamLabel}>{"Workspace"}</span>
            <span className={classes.teamName} title={team.teamName}>{`${team.teamName ||
              "Select Workspace"}`}</span>
          </div>
          <div className={classes.arrowsCnt}>
            <ArrowRightIcon
              className={clsx({
                [classes.arrowIconRight]: !workspaceView,
                [classes.arrowIconLeft]: workspaceView,
              })}
            />
          </div>
        </div>
      </div>

      {/* {workspaceView && (
        <div className={classes.workspaceContent}>
          <Scrollbars
            autoHide
            style={{ height: "calc(100vh - 200px)" }}
            renderThumbVertical={renderThumb}>
            <div style={{ padding: "5px 0px" }} className={classes.opendState}>
              <div
                className={classes.teamSettingsLabel}
                onClick={() => history.push("/workspace-settings")}>
                <SettingsIcon />
                <span className={classes.settingLabel}>Workspace Settings</span>
              </div>
            </div>
            <WorkspaceList />
          </Scrollbars>
        </div>
      )} */}
    </>
  );
}
WorkspaceInfo.defaultProps = {
  classes: {},
  theme: {},
};

const mapStateToProps = state => {
  return {
    profile: state.profile.data,
  };
};

export default compose(
  withRouter,
  connect(mapStateToProps, {}),
  withStyles(workspaceInfoStyles, { withTheme: true })
)(WorkspaceInfo);
