const workspaceInfoStyles = theme => ({
  teamCont: {
    margin: "5px auto 0px",
    height: 48,
    backgroundColor: theme.palette.sidebar.elementsBackgroundColor,
    borderRadius: "6px",
    cursor: "pointer",
    width: "calc(100% - 20px)"

  },
  teamSettingsContainer: {
    color: theme.palette.common.white,
    display: "flex",
    alignItems: "center",
    minHeight: 36,
    cursor: "pointer",
    borderBottom: `1px solid ${theme.palette.sidebar.elementsBackgroundColor}`,
    "& $expandIcon": {
      color: theme.palette.sidebar.labelColor,
    },
    "& $expandMoreIcon": {
      color: theme.palette.sidebar.labelColor,
    },
  },
  openView: {
    margin: "5px 0px 0px auto !important",
    width: "calc(100% - 10px) !important"
    // padding: "0px 10px",
    // borderRadius: 0,
    // borderBottom: `1px solid transparent`,
  },
  
  teamElements: {
    display: "flex",
    color: theme.palette.common.white,
    height: "100%",
    width: "100%",
    alignItems: "center",
    padding: "0px 5px",
  },
  teamInfo: {
    display: "flex",
    flexDirection: "column",
    fontSize: "12px",
    marginLeft: 10,
    alignItems: "flex-start",
    flex: 1,
    width: 0,
  },
  teamLabel: {
    fontSize: "13px !important",
    fontWeight: theme.typography.fontWeightExtraLight,
    fontFamily: theme.typography.fontFamilyLato,
    color: theme.palette.sidebar.labelColor,

    // width: 170,
    // whiteSpace: "nowrap",
    // overflow: "hidden",
    // textOverflow: "ellipsis",
  },
  teamOwnerLabel: {
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightExtraLight,
    fontFamily: theme.typography.fontFamilyLato,
    color: theme.palette.sidebar.labelColor,
  },
  teamName: {
    fontSize: "15px !important",
    fontWeight: theme.typography.fontWeightExtraLight,
    fontFamily: theme.typography.fontFamilyLato,
    width: "100%",
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    height: 19
  },
  teamOwnerName: {
    fontSize: "14px !important",
    fontWeight: theme.typography.fontWeightExtraLight,
    fontFamily: theme.typography.fontFamilyLato,
    width: 120,
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
  },
  arrowIconRight: {
    transform: "rotate(270deg)",
    // position: "absolute",
    // right: "23px",
    fontSize: "20px",
    // top: "13px",
  },
  arrowIconLeft: {
    transform: "rotate(90deg)",
    // position: "absolute",
    // right: "23px",
    fontSize: "20px",
    // top: "13px",
  },
  arrowsCnt: {
    position: "relative",
    height: "100%",
    display: "flex",
    alignItems: "center"
  },
  expandIcon: {
    fontSize: "20px !important",
    margin: "0px 5px",
  },
  expandMoreIcon: {
    fontSize: "20px !important",
    margin: "0px 5px",
    transform: "rotate(270deg)",
  },
  
 
  collapseCnt: {
    borderBottom: `1px solid ${theme.palette.sidebar.background}`,
  },
  storageCnt: {
    margin: "5px 10px 10px 10px",
    minHeight: "188px",
    borderRadius: 10,
    backgroundColor: theme.palette.sidebar.elementsHoverColor,
    color: theme.palette.common.white,
    padding: "6px 8px",
  },
  infoCnt: {
    display: "flex",
    flexDirection: "column",
    marginBottom: 10,
    lineHeight: 1.3,
  },
  infoMainCnt: {
    display: "flex",
    justifyContent: "space-between",
  },
  planCnt: {
    minWidth: 60,
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "center",
    paddingRight: 5,
  },
  inviteLabel: {
    color: "#00CC90",
    textDecoration: "underline",
    fontSize: "13px !important",
    fontWeight: theme.typography.fontWeightExtraLight,
    fontFamily: theme.typography.fontFamilyLato,
    cursor: "pointer",
    paddingTop: 12,
  },
  storageLabel: {
    fontSize: "13px !important",
    fontWeight: theme.typography.fontWeightExtraLight,
    fontFamily: theme.typography.fontFamilyLato,
    paddingTop: 12,
  },
  progressBar: {
    backgroundColor: theme.palette.sidebar.elementsBackgroundColor,
    borderRadius: 2,
  },
  greenBar: {
    backgroundColor: theme.palette.background.barGreen,
  },
  searchIcon: {
    color: theme.palette.sidebar.labelColor,
    fontSize: "14px !important",
    marginRight: 10,
  },
  workspaceIcon: {
    fontSize: "17px !important",
    marginBottom: 0,
    marginLeft: 3,
  },
  
  teamViewOpen: {
    margin: "5px 0px",
    background: "transparent",
    overflow: "hidden",
    width: "100% !important"
  },
});

export default workspaceInfoStyles;
