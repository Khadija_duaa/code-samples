const workspaceListStyles = theme => ({
  searchInput: {
    // padding: "4px 8px 5px 8px",
    // width: "100%",
    outline: "none",
    border: "none",
    fontSize: "14px !important",
    // lineHeight: "18px",
    fontWeight: theme.typography.fontWeightMedium,
    fontFamily: "Lato, sans-serif",
    borderRadius: 0,
    cursor: "pointer",
    height: 35,
    color: theme.palette.sidebar.labelColor,
    margin: "1px 8px",
    background: theme.palette.sidebar.elementsHoverColor,
    borderRadius: 6,
    "& fieldset": {
      border: "none !important",
    },
    "& input": {
      color: `${theme.palette.common.white} !important`,
    },
  },
  teamSettingsContainer: {
    color: theme.palette.common.white,
    display: "flex",
    alignItems: "center",
    minHeight: 36,
    cursor: "pointer",
    "& $expandIcon": {
      color: theme.palette.sidebar.labelColor,
    },
    "& $expandMoreIcon": {
      color: theme.palette.sidebar.labelColor,
    },
  },
  expandIcon: {
    fontSize: "20px !important",
    margin: "0px 5px",
  },
  expandMoreIcon: {
    fontSize: "20px !important",
    margin: "0px 5px",
    transform: "rotate(270deg)",
  },
  searchIcon: {
    color: theme.palette.sidebar.labelColor,
    fontSize: "14px !important",
    marginRight: 10,
  },
  collapseCnt: {},
  wrapper: {
    height: "calc(100vh - 170px) !important",
  },
  teamName: {
    fontSize: "14px !important",
    fontWeight: theme.typography.fontWeightExtraLight,
    fontFamily: theme.typography.fontFamilyLato,
    width: "90%",
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
  },
  teamInfo: {
    display: "flex",
    flexDirection: "column",
    fontSize: "12px",
    marginLeft: 10,
    alignItems: "flex-start",
    flex: 1,
    width: 0,
    width: "90%",
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
  },
  teamLabel: {
    fontSize: "13px !important",
    fontWeight: theme.typography.fontWeightExtraLight,
    fontFamily: theme.typography.fontFamilyLato,
    color: theme.palette.sidebar.labelColor,
    lineHeight: "normal",

    // width: 170,
    // whiteSpace: "nowrap",
    // overflow: "hidden",
    // textOverflow: "ellipsis",
  },
  teamElements: {
    display: "flex",
    color: theme.palette.common.white,
    height: "100%",
    width: "100%",
    alignItems: "center",
    padding: "5px 5px",
    lineHeight: 1.3,
    cursor: "pointer",
    marginTop: "5px",
    "&:hover": {
      backgroundColor: theme.palette.sidebar.elementsHoverColor,
    },
  },
  arrowsCnt: {
    // position: "relative",
    height: "100%",
  },
  planIcon: {
    // position: "absolute",
    // top: "-8px",
    fontSize: "15px",
    marginRight: 4
    // bottom: 0,
    // right: 7,
  },
  workspaceContent: {
    backgroundColor: theme.palette.sidebar.elementsBackgroundColor,
    // paddingBottom: 60,
  },
  opendState: {
    borderBottom: `1px solid ${theme.palette.sidebar.background}`,
  },
  teamSettingsLabel: {
    color: theme.palette.sidebar.headingColor,
    display: "flex",
    alignItems: "center",
    minHeight: 34,
    cursor: "pointer",
    paddingLeft: 8,
    cursor: "pointer",
    "& svg": {
      fontSize: "18px !important",
      marginRight: 8,
      color: theme.palette.sidebar.headingColor,
    },
    "&:hover": {
      backgroundColor: theme.palette.sidebar.elementsHoverColor,
    },
    "&:hover svg": {
      color: `${theme.palette.background.green} !important`,
    },
    "&:hover $settingLabel": {
      color: theme.palette.common.white,
    },
  },
  settingLabel: {
    color: theme.palette.sidebar.headingColor,
    fontSize: "13px !important",
    fontWeight: theme.typography.fontWeightExtraLight,
    fontFamily: theme.typography.fontFamilyLato,
  },
  emptyState: {
    display: "flex",
    flexDirection: "column",
    fontSize: "12px",
    marginLeft: 10,
    alignItems: "flex-start",
    marginTop: 5,
  },
});

export default workspaceListStyles;
