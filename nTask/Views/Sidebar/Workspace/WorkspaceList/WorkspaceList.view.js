import React, { useState, useContext } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router";

import { withStyles } from "@material-ui/core/styles";
import workspaceListStyles from "./WorkspaceList.style";
import DefaultTextField from "../../../../components/Form/TextField";
import CustomAvatar from "../../../../components/Avatar/Avatar";

import SidebarContext from "../../Context/sidebar.context";
import SearchInput, { createFilter } from "react-search-input";
import DefaultDialog from "../../../../components/Dialog/Dialog";
import AddWorkspaceForm from "../../../../components/Header/addWorkSpaceForm";

import { clearBoardsData } from "./../../../../redux/actions/boards";
import { FetchUserInfo } from "./../../../../redux/actions/profile";
import { getTasks } from "./../../../../redux/actions/tasks";
import { getCustomField } from "./../../../../redux/actions/customFields";
import { clearAllAppliedFilters } from "./../../../../redux/actions/appliedFilters";
import { FetchWorkspaceInfo } from "./../../../../redux/actions/workspace";
import { nTaskSignalConnectionOpen } from "../../../../utils/ntaskSignalR";
import SettingsIcon from "@material-ui/icons/Settings";
import { Scrollbars } from "react-custom-scrollbars";
import AddIcon from "@material-ui/icons/AddCircleRounded";
import { FormattedMessage } from "react-intl";

function WorkspaceList(params) {
  const {
    state: {},
    dispatch,
    showLoadingState,
    hideLoadingState,
  } = useContext(SidebarContext);
  const {
    classes,
    theme,
    profile,
    history,
    clearBoardsData,
    FetchUserInfo,
    getTasks,
    getCustomField,
    clearAllAppliedFilters,
    FetchWorkspaceInfo,
    allMembers,
    workSpacePer,
    companyInfo
  } = params;
  const [searchTerm, setSearchTerm] = useState("");
  const [createWorkspaceDialog, setCreateWorkspaceDialog] = useState(false);

  const searchUpdated = e => {
    setSearchTerm(e.target.value);
  };

  const selectedTeam = workspace => {

    showLoadingState();
    clearBoardsData(false);
    $.connection.hub.stop();
    FetchWorkspaceInfo(
      workspace.teamId,
      () => {
        /** fetching all user created custom fields */
        getTasks(
          null,
          null,
          () => {},
          () => {}
        );
        getCustomField(
          succ => {},
          fail => {}
        );
        FetchUserInfo(() => {
          nTaskSignalConnectionOpen(profile.userId);
            if(companyInfo?.workspaceLandingPageUrl) {
              history.push(companyInfo?.workspaceLandingPageUrl);
            } else {
              history.push("/tasks");
            }
          clearAllAppliedFilters(null);
          hideLoadingState();
        });
      },
      error => {
        FetchUserInfo(response => {
          let activeTeam = response.data.teams.find(t => {
            return t.companyId == response.data.activeTeam;
          });
          history.push(`/teams/${activeTeam.companyUrl}`);
          hideLoadingState();
        });
      }
    );
  };
  const renderThumb = ({ style, ...props }) => {
    const thumbStyle = {
      backgroundColor: `#dbdbdb`,
      borderRadius: 6,
    };
    return <div style={{ ...style, ...thumbStyle }} {...props} />;
  };
  const handleWorkspaceDialogClose = e => {
    setCreateWorkspaceDialog(false);
  };
  const handleWorkspaceDialogOpen = e => {
    setCreateWorkspaceDialog(true);
  };
  const filteredData = profile.workspace.filter(createFilter(searchTerm, ["teamName"]));
  return (
    <>
      <div className={classes.workspaceContent}>
        <Scrollbars
          autoHide
          style={{ height: "calc(100vh - 152px)" }}
          renderThumbVertical={renderThumb}>
          <div style={{ padding: "5px 0px" }} className={classes.opendState}>
            {profile.workspace.length &&
            workSpacePer.workspaceSetting &&
            workSpacePer.workspaceSetting.cando ? (
              <div
                className={classes.teamSettingsLabel}
                onClick={() => history.push("/workspace-settings")}>
                <SettingsIcon />
                <span className={classes.settingLabel}>Workspace Settings</span>
              </div>
            ) : null}
            <div className={classes.teamSettingsLabel} onClick={handleWorkspaceDialogOpen}>
              <AddIcon style={{ color: theme.palette.background.green }} />
              <span className={classes.settingLabel}>Create Workspace</span>
            </div>
          </div>
          <div>
            <div className={classes.teamSettingsContainer}>
              <span className={classes.teamLabel} style={{ marginLeft: 10 }}>{`Others (${
                profile.workspace.length == 0 ? 0 : profile.workspace.length - 1
              })`}</span>
            </div>
            <div>
              <DefaultTextField
                error={false}
                errorState={false}
                errorMessage={""}
                formControlStyles={{ marginBottom: 0, minHeight: 37 }}
                styles={classes.searchInput}
                transparentInput={true}
                defaultProps={{
                  type: "text",
                  placeholder: `Search`,
                  value: searchTerm,
                  autoFocus: true,
                  inputProps: { maxLength: 80 },
                  onChange: e => {
                    searchUpdated(e);
                  },
                }}
              />

              {filteredData.length ? (
                filteredData.map((team, index) => {
                  const createdBy = allMembers.find(member => member.userId === team.teamOwner);
                  if (profile.loggedInTeam === team.teamId || !team.isActive) return;
                  return (
                    <div className={classes.teamElements} onClick={() => selectedTeam(team)} id={`WSSidebarMenu${index}${team.teamId}`}>
                      <CustomAvatar
                        otherMember={{
                          imageUrl: team.pictureUrl
                            ? `${team.imageBasePath}${team.pictureUrl}`
                            : "",
                          fullName: team.teamName,
                          lastName: "",
                          email: team.companyUrl,
                          isOnline: false,
                        }}
                        size="xsmall"
                        hideBadge={true}
                        styles={{ marginLeft: 5 }}
                      />
                      <div className={classes.teamInfo}>
                        <span
                          className={classes.teamName}
                          title={team.teamName}>{`${team.teamName}`}</span>
                        <span className={classes.teamLabel}>{createdBy?.fullName || "-"}</span>
                      </div>
                    </div>
                  );
                })
              ) : filteredData.length == 0 && profile.workspace.length ? (
                <div className={classes.emptyState}>
                  <span className={classes.teamLabel}>{"No Results Found!"}</span>
                </div>
              ) : null}
            </div>
          </div>
        </Scrollbars>
      </div>
      <DefaultDialog
        title={
          <FormattedMessage
            id="workspace-settings.create-workspace.label"
            defaultMessage="Create Workspace"
          />
        }
        sucessBtnText="Create Workspace"
        open={createWorkspaceDialog}
        onClose={handleWorkspaceDialogClose}>
        <AddWorkspaceForm closeDialog={handleWorkspaceDialogClose} />
      </DefaultDialog>
    </>
  );
}
WorkspaceList.defaultProps = {
  classes: {},
  theme: {},
};

const mapStateToProps = state => {
  return {
    profile: state.profile.data,
    allMembers: state.profile.data.teamMember,
    workSpacePer: state.workspacePermissions.data.workSpace,
    companyInfo: state.whiteLabelInfo.data,
  };
};

export default compose(
  withRouter,
  connect(mapStateToProps, {
    clearBoardsData,
    FetchUserInfo,
    getTasks,
    getCustomField,
    clearAllAppliedFilters,
    FetchWorkspaceInfo,
  }),
  withStyles(workspaceListStyles, { withTheme: true })
)(WorkspaceList);
