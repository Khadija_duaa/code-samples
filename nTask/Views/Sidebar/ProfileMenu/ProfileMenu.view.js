import Badge from "@material-ui/core/Badge";
import { withStyles } from "@material-ui/core/styles";
import SvgIcon from "@material-ui/core/SvgIcon";
import clsx from "clsx";
import isEmpty from "lodash/isEmpty";
import moment from "moment";
import { withSnackbar } from "notistack";
import React, { useContext, useRef, useState } from "react";
import { FormattedMessage } from "react-intl";
import { connect, useDispatch } from "react-redux";
import { withRouter } from "react-router";
import { compose } from "redux";
import CustomAvatar from "../../../components/Avatar/Avatar";
import CustomButton from "../../../components/Buttons/CustomButton";
import Constants from "../../../components/constants/planConstant";
import FeedbackDialog from "../../../components/Dialog/FeedbackDialog";
import DropdownMenu from "../../../components/Dropdown/DropdownMenu";
import DefaultSwitch from "../../../components/Form/Switch";
import { ActivityIcon } from "../../../components/Icons/ActivityIcon";
import DoubleArrow from "../../../components/Icons/DoubleArrow";
import IconCallCalling from "../../../components/Icons/IconCallCalling";
import IconCustomizeMenu from "../../../components/Icons/IconCustomizeMenu";
import IconFeedback from "../../../components/Icons/IconFeedback";
import IconInvitation from "../../../components/Icons/IconInvitation";
import IconLogout from "../../../components/Icons/IconLogout";
import IconMessageText from "../../../components/Icons/IconMessageText";
import IconProfileSettings from "../../../components/Icons/IconProfileSettings";
import Love from "../../../components/Icons/Love";
import CustomListItem from "../../../components/ListItem/CustomListItem";
import CustomMenuList from "../../../components/MenuList/CustomMenuList";
import CustomTooltip from "../../../components/Tooltip/Tooltip.js";
import { profileSettingState } from "../../../redux/actions/allDialogs";
import { Logout } from "../../../redux/actions/logout";
import { saveMenuItems, showHideSideBar } from "../../../redux/actions/sidebarPannel";
import InvitationsDialog from "../../InvitationsDialog/InvitationsDialog";
import { updateSettings } from "../Context/actions/sidebar.actions";
import SidebarContext from "../Context/sidebar.context";
import profileMenuStyles from "./ProfileMenu.style";

function ProfileMenu(params) {
  const {
    state: {
      teamView,
      workspaceView,
      innerSidebar,
      sidebarFullWidth,
      customizeMenu,
      menuItems,
      quickAccess,
    },
    dispatch,
  } = useContext(SidebarContext);

  const anchorEl = useRef(null);
  const dispatchFun = useDispatch();
  const [open, setOpen] = useState(false);
  const [openFeedback, setOpenFeedback] = useState(false);
  const [openInvitationDialog, setOpenInvitationDialog] = useState(false);
  const [saveMenuQuery, setSaveMenuQuery] = useState("");
  const {
    theme,
    classes,
    profile,
    enqueueSnackbar,
    Logout,
    history,
    profileSettingState,
    saveMenuItems,
    sidebarPannel,
    subscriptionDetails,
    whiteLabelTeam,
  } = params;
  const { teamInvitationCount, teamMember, userId, workspace } = profile;

  const handleClick = (event) => {
    event.stopPropagation();
    setOpen(!open);
  };
  const handleClose = (event) => {
    const isExist = event.target.id == "profileMenuBtn" || event.target.closest("#profileMenuBtn");
    if (isExist) {
      return;
    }
    setOpen(false);
  };
  const handleProfileSetting = (e) => {
    setOpen(false);
    profileSettingState({ open: true });
  };
  const handleInvitation = (e) => {
    setOpen(false);
    setOpenInvitationDialog(true);
  };
  const handleFeedback = (e) => {
    setOpen(false);
    setOpenFeedback(true);
  };
  const handleDialogClose = (e) => {
    setOpenFeedback(false);
  };

  const handleInviteDialogClose = (e) => {
    setOpenInvitationDialog(false);
  };
  const showSnackBar = (snackBarMessage, closeDialog, type) => {
    if (closeDialog) {
      handleDialogCloseAction();
    }
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type || "info",
      }
    );
  };
  const handleGuidedTour = (event, code) => {
    // When click on any guided tour from the list to run
    event.stopPropagation();
    handleClose();
    if (window.userpilot) {
      userpilot.trigger(code); // Triggering userpilot action
    }
  };
  const handleOpenCustomizeMenu = () => {
    updateSettings(dispatch, {
      customizeMenu: true,
      sidebarFullWidth: true,
      innerSidebar: false,
      teamView: false,
      workspaceView: false,
      quickAccess: false,
    });
  };
  const handleSaveOrder = () => {
    setSaveMenuQuery("progress");
    const teamMenu = menuItems.teamMenu.map((item, index) => {
      return { ...item, index };
    });
    const workspaceMenu = menuItems.workspaceMenu.map((item, index) => {
      return { ...item, index };
    });
    const obj = {
      userId: menuItems.userId,
      teamMenu,
      workspaceMenu,
    };
    saveMenuItems(
      dispatchFun,
      obj,
      (success) => {
        updateSettings(dispatch, { customizeMenu: false });
        setSaveMenuQuery("");
      },
      (failure) => {
        setSaveMenuQuery("");
      }
    );
  };

  const handleActivityLog = () => {
    history.push("/activity");
    handleClose();
  };

  const loggedInUser = teamMember.find((user) => user.userId === userId) || {};
  const isChange = JSON.stringify(menuItems) !== JSON.stringify(sidebarPannel.data);
  const paidUser = Constants.isPlanPaid(subscriptionDetails.paymentPlanTitle);
  const freePlan = Constants.isPlanFree(subscriptionDetails.paymentPlanTitle);
  const trialPlan = Constants.isPlanTrial(subscriptionDetails.paymentPlanTitle);
  const isOldSidebarVisible = localStorage.getItem("oldSidebarView") == "true" ? true : false;
  const userCreatedDate = moment(profile?.createdDate);
  const isOldUser = userCreatedDate.isBefore("2022-03-03T11:26:29.219Z");
  return (
    <>
      {customizeMenu ? (
        <div className={classes.customizeDoneBtn}>
          <CustomButton
            onClick={() => {
              isChange && !isEmpty(menuItems)
                ? handleSaveOrder()
                : updateSettings(dispatch, {
                  customizeMenu: false,
                  teamView: false,
                  workspaceView: false,
                  innerSidebar: false,
                  sidebarFullWidth: true,
                  quickAccess: false,
                });
            }}
            btnType="green"
            style={{
              width: "100%",
              height: 34,
            }}
            variant="contained"
            query={saveMenuQuery}
            disabled={saveMenuQuery == "progress"}>
            {isChange && !isEmpty(menuItems) ? "Save" : "Go Back"}
          </CustomButton>
        </div>
      ) : innerSidebar && sidebarFullWidth ? (
        <>
          <div
            className={clsx({
              [classes.customizeMenuContClose]: true,
              [classes.bottomPosition]: quickAccess,
            })}
            onClick={handleOpenCustomizeMenu}>
            <SvgIcon viewBox="0 0 18 18">
              <IconCustomizeMenu />
            </SvgIcon>
          </div>
          <div
            className={classes.profileMenuCntClose}
            ref={(node) => {
              anchorEl.current = node;
            }}>
            {teamInvitationCount > 0 ? (
              <div className={classes.profileCntClose}>
                <div onClick={handleClick} id="profileMenuBtn">
                  <Badge
                    badgeContent={teamInvitationCount}
                    classes={{ badge: classes.profileAvatarBadge }}>
                    <CustomAvatar personal size="small" hideBadge={true} />
                  </Badge>
                </div>
                {!quickAccess && (
                  <div
                    className={classes.arrowCnt}
                    onClick={(e) => {
                      e.stopPropagation();
                      updateSettings(dispatch, { sidebarFullWidth: false });
                    }}>
                    <CustomTooltip
                      helptext={"Show less information"}
                      iconType="help"
                      placement="top"
                      //   classes={classes}
                      style={{ color: theme.palette.common.white }}>
                      <SvgIcon viewBox="0 0 12 11.999">
                        <DoubleArrow />
                      </SvgIcon>
                    </CustomTooltip>
                  </div>
                )}
              </div>
            ) : (
              <div className={classes.profileCntClose}>
                <div onClick={handleClick} id="profileMenuBtn">
                  <CustomAvatar personal size="small" hideBadge={true} />
                </div>
                {!quickAccess && (
                  <div
                    className={classes.arrowCnt}
                    onClick={(e) => {
                      e.stopPropagation();
                      updateSettings(dispatch, { sidebarFullWidth: false });
                    }}>
                    <CustomTooltip
                      helptext={"Show less information"}
                      iconType="help"
                      placement="top"
                      //   classes={classes}
                      style={{ color: theme.palette.common.white }}>
                      <SvgIcon viewBox="0 0 12 11.999">
                        <DoubleArrow />
                      </SvgIcon>
                    </CustomTooltip>
                  </div>
                )}
              </div>
            )}
          </div>
        </>
      ) : (
        <>
          {workspace?.length ? (
            <>
              {/* {!teamView &&
                !workspaceView &&
                isOldUser &&
                profile.activeTeam !== "4c027962d01e4bf1bcdd5e0486556edc" && (
                  <div
                    style={{
                      bottom: (trialPlan || freePlan) && !teamView && !workspaceView && 140,
                    }}
                    className={classes.newSidebarSwitchCnt}>
                    <p>
                      New Menu <span className={classes.betaTag}>beta</span>
                    </p>
                    <div style={{ marginTop: -3 }}>
                      <DefaultSwitch
                        checked={!isOldSidebarVisible}
                        onChange={(event) => {
                          params.showHideSideBar(!isOldSidebarVisible);
                          localStorage.setItem("oldSidebarView", !isOldSidebarVisible);
                          window.location.reload();
                        }}
                        color="dark"
                        size="small"
                        // value="oldSidebarView"
                      />
                    </div>
                  </div>
                )} */}
              <div
                className={clsx({
                  [classes.customizeMenuCont]: true,
                  [classes.teamMenuOpen]: teamView || workspaceView,
                  [classes.bottom]: (trialPlan || freePlan) && !teamView && !workspaceView,
                })}
                onClick={handleOpenCustomizeMenu}>
                <SvgIcon viewBox="0 0 18 18">
                  <IconCustomizeMenu />
                </SvgIcon>
                {!teamView && !workspaceView && (
                  <span className={classes.customizeLabel}>Customize Menu</span>
                )}
              </div>
            </>
          ) : null}
          {(trialPlan || freePlan) && !teamView && !workspaceView ? (
            <div
              className={clsx({
                [classes.bookDemoBtn]: true,
                [classes.compactBtn]: !sidebarFullWidth,
              })}>
              <CustomButton
                onClick={() => {
                  window.open("https://www.ntaskmanager.com/talk-to-sales/", "_blank");
                }}
                btnType="yellow"
                style={{
                  height: 34,
                }}
                variant="contained"
                query={""}
                disabled={false}>
                <IconCallCalling style={{ fontSize: "16px", marginRight: 7 }} />
                Talk to Sales
              </CustomButton>
            </div>
          ) : null}
          <div
            className={classes.profileMenuCnt}
            ref={(node) => {
              anchorEl.current = node;
            }}
            onClick={handleClick}
            id="profileMenuBtn">
            {teamInvitationCount > 0 ? (
              <div className={classes.profileCnt}>
                <Badge
                  badgeContent={teamInvitationCount}
                  classes={{ badge: classes.profileAvatarBadge }}>
                  <CustomAvatar personal size="small" hideBadge={true} styles={{ marginLeft: 3 }} />
                </Badge>
                {!teamView && !workspaceView && (
                  <div className={classes.profileInfo}>
                    <span className={classes.userName} title={loggedInUser.fullName}>
                      {loggedInUser.fullName}
                    </span>
                    <span className={classes.userRole}>{`Team ${loggedInUser.role}`}</span>
                  </div>
                )}
                {!sidebarFullWidth && (
                  <CustomTooltip
                    helptext={"Show more information"}
                    iconType="help"
                    placement="top"
                    //   classes={classes}
                    style={{ color: theme.palette.common.white }}>
                    <div
                      className={classes.arrowCntOpen}
                      onClick={(e) => {
                        e.stopPropagation();
                        updateSettings(dispatch, { sidebarFullWidth: true });
                      }}>
                      <SvgIcon viewBox="0 0 12 11.999">
                        <DoubleArrow />
                      </SvgIcon>
                    </div>
                  </CustomTooltip>
                )}
              </div>
            ) : (
              <div className={classes.profileCnt}>
                <CustomAvatar personal size="small" hideBadge={true} styles={{ marginLeft: 3 }} />
                {!teamView && !workspaceView && (
                  <div className={classes.profileInfo}>
                    <span className={classes.userName} title={loggedInUser.fullName}>
                      {loggedInUser.fullName}
                    </span>
                    <span className={classes.userRole}>{`Team ${loggedInUser.role}`}</span>
                  </div>
                )}
                {!sidebarFullWidth && (
                  <CustomTooltip
                    helptext={"Show more information"}
                    iconType="help"
                    placement="top"
                    //   classes={classes}
                    style={{ color: theme.palette.common.white }}>
                    <div
                      className={classes.arrowCntOpen}
                      onClick={(e) => {
                        e.stopPropagation();
                        updateSettings(dispatch, { sidebarFullWidth: true });
                      }}>
                      <SvgIcon viewBox="0 0 12 11.999">
                        <DoubleArrow />
                      </SvgIcon>
                    </div>
                  </CustomTooltip>
                )}
              </div>
            )}
          </div>
        </>
      )}

      <DropdownMenu
        open={open}
        anchorEl={anchorEl.current}
        placement="top-end"
        size="medium"
        closeAction={handleClose}
        disablePortal={false}
        paperProps={{
          classes: {
            root: classes.paperRoot,
          },
        }}>
        <CustomMenuList>
          <div className={classes.menuCnt}>
            <div className={classes.profileInfoCnt}>
              <CustomAvatar personal size="small36" hideBadge={true} styles={{ marginLeft: 5 }} />
              <div className={classes.profileInfo}>
                <span className={classes.fullName} title={loggedInUser.fullName}>
                  {loggedInUser.fullName}
                </span>
                <span
                  className={classes.userRole}
                  title={loggedInUser.email}>{`${loggedInUser.email}`}</span>
              </div>
            </div>
            <CustomListItem
              compactMode={false}
              darkmode={true}
              icon={<IconProfileSettings />}
              rootProps={{ onClick: (e) => handleProfileSetting(e) }}>
              <FormattedMessage
                id="profile-settings-dialog.title"
                defaultMessage="Profile Settings"
              />
            </CustomListItem>
            <CustomListItem
              compactMode={false}
              darkmode={true}
              icon={<IconInvitation />}
              rootProps={{ onClick: (e) => handleInvitation(e) }}>
              {teamInvitationCount > 0 ? (
                <div className={classes.invitationCnt}>
                  <FormattedMessage
                    id="header.user-menu.invite.label"
                    defaultMessage="Invitations"
                  />
                  <span className={classes.notificationCount}>{teamInvitationCount}</span>
                </div>
              ) : (
                <FormattedMessage id="header.user-menu.invite.label" defaultMessage="Invitations" />
              )}
            </CustomListItem>
            {/* <CustomListItem
              compactMode={false}
              darkmode={true}
              icon={<Flag />}
              subNav={true}
              popperProps={{
                paperProps: {
                  classes: {
                    root: classes.subNav,
                  },
                  disablePortal: false,
                },
              }}
              subNavRenderer={
                <>
                  <CustomListItem
                    rootProps={{ onClick: e => handleGuidedTour(e, "1588693494aWrq6403") }}
                    compactMode={false}
                    darkmode={true}>
                    <FormattedMessage
                      id="header.user-menu.guided-tour.menus-list.create-task.label"
                      defaultMessage="Create a Task"
                    />
                  </CustomListItem>
                  <CustomListItem
                    rootProps={{ onClick: e => handleGuidedTour(e, "1588694169gMpl9497") }}
                    compactMode={false}
                    darkmode={true}>
                    <FormattedMessage
                      id="header.user-menu.guided-tour.menus-list.group-by.label"
                      defaultMessage="Task: Group By"
                    />
                  </CustomListItem>
                  <CustomListItem
                    rootProps={{ onClick: e => handleGuidedTour(e, "1588694300xDur7637") }}
                    compactMode={false}
                    darkmode={true}>
                    <FormattedMessage
                      id="header.user-menu.guided-tour.menus-list.group-by.label"
                      defaultMessage="Task: Group By"
                    />
                  </CustomListItem>
                  <CustomListItem
                    rootProps={{ onClick: e => handleGuidedTour(e, "1588694348zEhe8959") }}
                    compactMode={false}
                    darkmode={true}>
                    <FormattedMessage
                      id="header.user-menu.guided-tour.menus-list.customize-list.label"
                      defaultMessage="Customize List View"
                    />
                  </CustomListItem>
                </>
              }>
              <FormattedMessage
                id="header.user-menu.guided-tour.label"
                defaultMessage="Guided Tour"
              />
            </CustomListItem> */}
            <CustomListItem
              compactMode={false}
              darkmode={true}
              icon={<IconFeedback />}
              rootProps={{ onClick: (e) => handleFeedback(e) }}>
              <FormattedMessage
                id="header.user-menu.send-feedback.label"
                defaultMessage="Send Feedback"
              />
            </CustomListItem>

            {/* activity log  */}
            <CustomListItem
              compactMode={false}
              darkmode={true}
              icon={
                <SvgIcon viewBox="0 0 512 512" classes={{ root: classes.plainMenuItemIcon }}>
                  <path d={ActivityIcon} fill={theme.palette.secondary.light} />
                </SvgIcon>
              }
              rootProps={{ onClick: (e) => handleActivityLog() }}>
              <FormattedMessage id="activity-log.label" defaultMessage="Activity Log" />
            </CustomListItem>

            <CustomListItem
              compactMode={false}
              darkmode={true}
              icon={<IconLogout />}
              rootProps={{ onClick: (e) => Logout("", history) }}>
              <FormattedMessage id="header.user-menu.sign-out.label" defaultMessage="Sign Out" />
            </CustomListItem>

            {paidUser && !whiteLabelTeam ? (
              <div style={{ padding: "5px 15px 10px 15px" }}>
                <CustomButton
                  onClick={() => {
                    window.open("https://www.ntaskmanager.com/get-support/", "_blank");
                  }}
                  btnType="yellow"
                  style={{
                    height: 34,
                  }}
                  variant="contained"
                  query={""}
                  disabled={false}>
                  <IconMessageText style={{ fontSize: "17px", marginRight: 5 }} />
                  Get Support
                </CustomButton>
              </div>
            ) : null}
            {!whiteLabelTeam && (
              <a className={classes.fotter}
                href="httpshttps://ntaskmanager.tapfiliate.com/publisher/signup/ntask-affiliate-program-1/"
                target={'_blank'} >
                <Love style={{ fontSize: "38px" }} />
                <div className={classes.profileInfo}>
                  <span className={classes.referFriend}>
                    {/* <FormattedMessage
                      id="header.user-menu.love.label"
                      defaultMessage="Love using nTask?"
                    /> */}
                    Refer a friend
                  </span>
                  <span className={classes.userRole}>
                    {/* <FormattedMessage
                      id="header.user-menu.love.placeholder"
                      defaultMessage="Recommend it to a friend!"
                    /> */}
                    Get 30% Commission
                  </span>
                </div>
              </a>
            )}
          </div>
        </CustomMenuList>
      </DropdownMenu>
      <FeedbackDialog
        open={openFeedback}
        closeDialog={handleDialogClose}
        showSnackBar={showSnackBar}
      />
      <InvitationsDialog open={openInvitationDialog} closeAction={handleInviteDialogClose} />
    </>
  );
}
ProfileMenu.defaultProps = {
  theme: {},
  classes: {},
  profile: {},
  subscriptionDetails: {},
};

const mapStateToProps = (state) => {
  return {
    whiteLabelTeam: state.whiteLabelInfo.data,
    profile: state.profile.data,
    sidebarPannel: state.sidebarPannel,
    subscriptionDetails: state.profile.data.teams.find(
      (item) => item.companyId == state.profile.data.activeTeam
    )?.subscriptionDetails,
  };
};

export default compose(
  withSnackbar,
  withRouter,
  connect(mapStateToProps, { Logout, profileSettingState, saveMenuItems, showHideSideBar }),
  withStyles(profileMenuStyles, { withTheme: true })
)(ProfileMenu);
