const profileMenuStyles = theme => ({
  profileMenuCnt: {
    height: 52,
    width: "100%",
    boxShadow: `0px -1px 0px ${theme.palette.sidebar.shadow}`,
    position: "absolute",
    bottom: 0,
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
  },
  newSidebarSwitchCnt: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '0 20px 0 10px',
    position: 'absolute',
    width: '100%',
    bottom: '90px',
    boxShadow: '0px 1px 0px #1d3544',
    '& p': {
      color: theme.palette.common.white,
      fontSize: "12px !important",
      fontWeight: theme.typography.fontWeightExtraLight
    }
  },
  betaTag: {
    padding: '2px 3px',
    borderRadius: 4,
    background: '#00ABED',
    color: theme.palette.common.white,
    fontSize: "10px !important",
    display: 'inline-block',
    marginLeft: 4
  },
  noPaddedButton: {
    padding: 0,
    marginLeft: 10,
    "&:hover": {
      backgroundColor: "transparent",
    },
  },
  noPaddedBtnText: {
    textTransform: "capitalize",
    fontSize: "1.1em !important",
    fontWeight: theme.typography.fontWeightRegular,
  },
  profileAvatarBadge: {
    background: theme.palette.background.danger,
    color: theme.palette.common.white,
    height: 16,
    transform: "scale(1) translate(33%, -35%)",
    minWidth: 16,
  },
  profileCnt: {
    display: "flex",
    color: theme.palette.common.white,
  },
  profileInfoCnt: {
    display: "flex",
    color: theme.palette.common.white,
    padding: "15px 5px",
    boxShadow: `0px 1px 0px ${theme.palette.sidebar.shadowLight}`,
  },
  fotter: {
    display: "flex",
    color: theme.palette.common.white,
    padding: "15px 5px",
    boxShadow: `0px -1px 0px ${theme.palette.sidebar.shadowLight}`,
    background: theme.palette.sidebar.shadowLight,
    borderRadius: "0px 0px 10px 10px",
    textDecoration: 'none',
  },
  profileInfo: {
    display: "flex",
    flexDirection: "column",
    fontSize: "12px !important",
    marginLeft: 10,
    lineHeight: 1.4,
    alignItems: "flex-start",
  },
  userName: {
    fontSize: "13px !important",
    fontWeight: theme.typography.fontWeightExtraLight,
    fontFamily: theme.typography.fontFamilyLato,
    overflow: "hidden",
    textOverflow: "ellipsis",
    whiteSpace: "nowrap",
    width: 150,
  },
  fullName: {
    fontSize: "15px !important",
    fontWeight: theme.typography.fontWeightExtraLight,
    fontFamily: theme.typography.fontFamilyLato,
    overflow: "hidden",
    textOverflow: "ellipsis",
    whiteSpace: "nowrap",
    width: 150,
  },
  referFriend: {
    fontSize: "15px !important",
    fontWeight: theme.typography.fontWeightMedium,
    width: 150,
  },
  userRole: {
    overflow: "hidden",
    textOverflow: "ellipsis",
    whiteSpace: "nowrap",
    width: 150,

    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightExtraLight,
    fontFamily: theme.typography.fontFamilyLato,
    color: "#DBE2E6",
  },
  paperRoot: {
    backgroundColor: "#2C3C4D",
    borderRadius: 10,
  },
  subNav: {
    backgroundColor: "#2C3C4D",
    // borderRadius: 10,
  },
  menuCnt: {
    width: "100%",
    // height: 400
  },
  notificationCount: {
    fontSize: "10px !important",
    color: theme.palette.common.white,
    background: theme.palette.background.danger,
    height: 16,
    width: 16,
    lineHeight: "normal",
    borderRadius: "50%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  invitationCnt: {
    display: "flex",
    width: "100%",
    justifyContent: "space-between",
  },
  btnContainer: {
    position: "absolute",
    bottom: 0,
    background: theme.palette.sidebar.elementsBackgroundColor,
    width: "100%",
    padding: "10px 10px",
  },
  customizeDoneBtn: {
    position: "absolute",
    bottom: 0,
    // background: theme.palette.sidebar.elementsBackgroundColor,
    width: "100%",
    padding: "10px 10px",
  },
  addIcon: {
    marginRight: 5,
  },
  profileMenuCntClose: {
    // height: 52,
    width: "42px",
    boxShadow: `0px -1px 0px ${theme.palette.sidebar.shadow}`,
    position: "absolute",
    bottom: 0,
    // display: "flex",
    // alignItems: "center",
    cursor: "pointer",
  },
  profileCntClose: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 10,
    color: "white",
    paddingBottom: 8,
  },
  arrowCnt: {
    width: "100%",
    height: 34,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 3,
    "&:hover": {
      backgroundColor: theme.palette.sidebar.elementsBackgroundColor,
    },
    "& svg": {
      fontSize: "14px !important",
    },
  },
  arrowCntOpen: {
    position: "absolute",
    bottom: 0,
    right: 48,
    height: "100%",
    width: 28,
    padding: "20px 7px",
    "& svg": {
      transform: "rotate(180deg)",
      fontSize: "14px !important",
    },
    "&:hover": {
      backgroundColor: theme.palette.sidebar.elementsBackgroundColor,
    },
  },
  customizeMenuCont: {
    // height: 52,
    // width: "100%",
    // boxShadow: `0px -1px 0px ${theme.palette.sidebar.shadow}`,
    position: "absolute",
    bottom: 62,
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
    color: theme.palette.sidebar.labelColor,
    paddingLeft: 10,
    "& svg": {
      fontSize: "18px !important",
    },
    "&:hover svg": {
      color: theme.palette.background.green,
    },
    "&:hover $customizeLabel": {
      color: "white",
    },
  },
  teamMenuOpen: {
    paddingLeft: 12,
  },
  customizeLabel: {
    fontSize: "13px !important",
    fontWeight: theme.typography.fontWeightExtraLight,
    fontFamily: theme.typography.fontFamilyLato,
    marginLeft: 5,
  },
  customizeMenuContClose: {
    position: "absolute",
    bottom: 95,
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
    color: theme.palette.sidebar.labelColor,
    paddingLeft: 11,
    "& svg": {
      fontSize: "20px !important",
    },
    "&:hover svg": {
      color: theme.palette.background.green,
    },
  },
  bottomPosition: {
    bottom: "58px !important",
  },
  headSet: {
    fontSize: "20px !important",
    marginRight: 5,
    marginBottom: -1,
  },
  bookDemoBtn: {
    width: "100%",
    bottom: 52,
    padding: "5px 15px 10px 15px",
    position: "absolute",
  },
  compactBtn: {
    width: "calc(100% - 47px)",
  },
  bottom: {
    bottom: 108,
  },
});

export default profileMenuStyles;
