import React, { Component } from "react";
import CustomDialog from "../../components/Dialog/CustomDialog";
import { Scrollbars } from "react-custom-scrollbars";
import { withSnackbar } from "notistack";
import { withStyles } from "@material-ui/core/styles";
import ButtonActionsCnt from "../../components/Dialog/ConfirmationDialogs/ButtonActionsCnt";
import { compose } from "redux";
import { connect } from "react-redux";
import timesheetStyles from "./styles";
import DefaultTextField from "../../components/Form/TextField";
import moment from "moment";
import DurationInput from "../../components/DatePicker/DurationInput";
import TimerIconGray from "../../assets/images/timer_gray.svg";
import {
  addTeamTaskDuration,
  getTeamDateTaskEffort,
} from "../../redux/actions/timesheet";
import { addUserEffort } from "../../redux/actions/tasks";
import CustomIconButton from "../../components/Buttons/CustomIconButton";
import TimerIcon from "@material-ui/icons/AvTimer";
import KeyboardIcon from "@material-ui/icons/Keyboard";
import InputLabel from "@material-ui/core/InputLabel";
import cloneDeep from "lodash/cloneDeep";
import CloseIcon from "@material-ui/icons/Close";
import IconButton from "../../components/Buttons/IconButton";
import NotificationMessage from "../../components/NotificationMessages/NotificationMessages";
import {injectIntl,FormattedMessage} from "react-intl";
class AddDurationDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isProceedClicked: false,
      checkedTeam: null,
      hours: "",
      mins: "",
      effortNotes: "",
      durationError: false,
      btnQuery: '',
      showNotes: false,
      timeEntries: { timeLogs: [] },
      saveButtonDisable: true,
      addDurationDialogState: {
        open: false,
        hours: "",
        mins: "",
        effortObj: "",
        timeStamp: "",
        project: {},
      },
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.initState &&
      (JSON.stringify(this.initState.timeEntries) !==
        JSON.stringify(this.state.timeEntries) ||
        this.state.effortNotes !== this.initState.effortNotes)
    ) {
      this.setState({ saveButtonDisable: false }, () => {
        this.initState = this.state;
      });
    }
  }
  componentDidMount() {
    const { timeEntryProps } = this.props;
    const { project, effortObj, timeStamp } = timeEntryProps;
    const postObj = { TaskId: effortObj.taskId, Date: timeStamp.date };
    this.props.getTeamDateTaskEffort(
      postObj,
      //Success
      (response) => {
        this.setState(
          { timeEntries: response.data, effortNotes: response.data.notes },
          () => {
            if (!response.data.timeLogs.length) {
              // In case there is no entry, we will add 1 entry by default with no values so user can input
              this.addNewTimeEntry();
            }
            this.initState = this.state;
          }
        );
      }
    );
  }
  // Function calls actions to save value to backend
  handleSaveTimeEntry = () => {
    const { timeEntries, effortNotes } = this.state;
    const { date, timeEntryProps } = this.props;
    const cloneObj = cloneDeep(timeEntries);
    cloneObj.notes = effortNotes;
    cloneObj.timeLogs.forEach((t) => {
      //removing dummy unique id's of new records so it may not cause error on backend while saving
      if (t.isNew) {
        t.id = "";
      }
    });
    cloneObj.projectId = timeEntryProps.project.projectId;
    cloneObj.workspaceId = timeEntryProps.project.workspaceId;
    this.setState({ btnQuery: "progress" }); // showing progress on save changes button
    this.props.addTeamTaskDuration(
      cloneObj,
      (response) => {
        // this.setState({ timesheetData: cloneDeep(this.props.timesheetState.timeEntry) });
        // this.props.addUserEffort(response.data.taskEffort);
        this.props.loadTimeSheet(
          `&currentDate=${date.format("YYYY-MM-DDTHH:mm:ss")}`
        );
        this.setState({ btnQuery: "" });
        this.props.closeAction();
      },
      (error) => {
        this.props.loadTimeSheet(
          `&currentDate=${date.format("YYYY-MM-DDTHH:mm:ss")}`
        );
        this.setState({ btnQuery: "" });
        this.props.closeAction();
        if(error) this.showSnackBar(error.data.message, "error");
      }
    );
  };
  //Function updates the values of hours and mins in input dropdowns
  updateEffortTime = (hrs, mins, obj) => {
    const { timeEntries } = this.state; // Getting all times entries object
    const clonedObj = { ...obj, effortHours: hrs, effortMinutes: mins }; //Cloning the existing single time entry object and updating new value in it
    const filteredObj = timeEntries.timeLogs.map((t) => {
      // returning new array of timelogs with the new updated entry user made
      if (t.id == clonedObj.id) {
        return clonedObj;
      } else {
        return t;
      }
    });
    //Updating the final object
    this.setState({ timeEntries: { ...timeEntries, timeLogs: filteredObj } });
  };
  addNewTimeEntry = () => {
    const { timeEntries } = this.state; // Getting all times entries object
    const newLogObject = [
      ...timeEntries.timeLogs,
      {
        id: Math.random(),
        effortHours: null,
        effortMinutes: null,
        timer: false,
        isNew: true,
      },
    ];
    this.setState({ timeEntries: { ...timeEntries, timeLogs: newLogObject } });
  };
  handleNotesInput = (event) => {
    this.setState({ effortNotes: event.target.value });
  };
  //The function will be called by the time entry dropdowns when the clear button in dropdown is clicked
  clearEffort = (key, value) => {
    this.setState({ [key]: value });
  };
  //Function triggers to show notes text area
  handleShowNotes = () => {
    this.setState({ showNotes: true });
  };
  deleteTimeLog = (timeLog) => {
    const { timeEntries } = this.state;
    let deletedLog = timeEntries.timeLogs.filter((t) => {
      //Removing the entry from the list by using filter method and returns the remaining entries
      return t.id != timeLog.id;
    });
    let existingDeletedIds =
      timeEntries.DeletedIds && timeEntries.DeletedIds.length
        ? timeEntries.DeletedIds
        : []; // checking if there is property of DeletedId exist in time entry object or not if not than assign empty array to avoid errors
    let newTimeEntryObj = { ...timeEntries, timeLogs: deletedLog }; //Creating new time entry object by updating the timelogs in object with the new time logs object
    let newDeletedIdsArr = [...existingDeletedIds, timeLog.id]; // Creating new deleted ids array by adding deleted time into existing state
    if (timeLog.isNew) {
      //Deleted ID's are saved in state so they can be sent to backend to tell which records are deleted, time logs which are not yet saved and only added to frontend exist with the property of isNew so in that case it's not added to deleted id's list
      this.setState({
        timeEntries: newTimeEntryObj,
      });
    } else {
      this.setState({
        timeEntries: { ...newTimeEntryObj, DeletedIds: newDeletedIdsArr }, // Updating time entry object with the deleted id's *note deleted id's are used to tell server which time logs needs to be deleted when user press save changes
      });
    }
  };

  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };

  render() {
    const {
      classes,
      theme,
      open,
      headingText,
      closeAction,
      timeEntryProps,
      timesheetPer,
      globalTimeIncrement = {},
    } = this.props;
    const { project, effortObj, timeStamp } = timeEntryProps;
    const {
      effortNotes,
      showNotes,
      timeEntries,
      saveButtonDisable,
    } = this.state;
    const hoursSum =
      timeEntries.timeLogs
        .map((t) => t.effortHours)
        .reduce((a, b) => parseInt(a) + parseInt(b), 0) || 0;
    const minsSum =
      timeEntries.timeLogs
        .map((t) => t.effortMinutes)
        .reduce((a, b) => parseInt(a) + parseInt(b), 0) || 0;
    const duration = moment.duration(`${hoursSum}:${minsSum}`);
    const hours = duration.hours();
    const minutes = duration.minutes();
    const days = duration.days();
    const totalMin = hoursSum * 60 + minsSum;
    const lastTimeLog = timeEntries.timeLogs[timeEntries.timeLogs.length - 1]; // Getting the last entry of the time logged
    const showAddEntry =
      (lastTimeLog && (lastTimeLog.effortHours || lastTimeLog.effortMinutes)) ||
      !lastTimeLog; // Check if true will show Add Entry button and if false will hide
    const disableSaveButton = saveButtonDisable || !showAddEntry;
    return (
      <CustomDialog
        title={
          <>
            <FormattedMessage id="timesheet.time-entry.label" defaultMessage="Time Entry"/> :{" "}
            <span className={classes.dialogHeaderDate}>
              {moment(timeStamp.date).format("ddd Do, MMMM, YYYY")}
            </span>
          </>
        }
        dialogProps={{
          open: timeEntryProps.open,
          onClick: (e) => {
            e.stopPropagation();
          },
          onClose: closeAction,
          PaperProps: {
            style: { maxWidth: 480 },
          },
        }}
      >
        <div className={classes.dialogContentCnt}>
          <DefaultTextField
            label={<FormattedMessage id="header.add-new-button.list.task.label" defaultMessage="Task"/>}
            error={false}
            defaultProps={{
              disabled: true,
              value: effortObj.taskTitle,
            }}
          />
          <DefaultTextField
            label={<FormattedMessage id="header.add-new-button.list.project.label" defaultMessage="Project"/>}
            error={false}
            defaultProps={{
              disabled: true,
              value: project.projectName
                ? project.projectName
                : ("--" + this.props.intl.formatMessage({id:"timesheet.time-entry.creation-dialog.project.not-assigned.label", defaultMessage:"No project assigned"})+"--"),
            }}
          />
          {timeEntries.timeLogs.length ? (
            <InputLabel classes={{ root: classes.dropdownsLabel }}>
<FormattedMessage id="timesheet.time-entry.label" defaultMessage="Time Entry"/>             
</InputLabel>
          ) : null}
          {timeEntries.timeLogs.map((t, i) => {
            return (
              <>
                <div key={t.id} style={{ display: "flex", marginBottom: 15 }}>
                  <DurationInput
                    updateTime={(hours, mins) =>
                      this.updateEffortTime(hours, mins, t)
                    }
                    hours={t.effortHours}
                    disabled={project["status"] == "Approved" || project["status"] == "In Review" || !timesheetPer.addTimeEntry.cando}
                    mins={t.effortMinutes}
                    error={false}
                    styles={{ flex: 1, margin: 0 }}
                    clearSelect={this.clearEffort}
                    customMins={globalTimeIncrement?.isCustomTime ? [0, 30] : []}
                  />
                  <div>
                    {t.timer ? ( // Render timer icon if entry is entered through timer else render manual entry icon of keyboard
                      <CustomIconButton
                        btnType="gray"
                        // onClick={this.handleAddBtnClick}
                        style={{
                          padding: 6,
                          marginLeft: 15,
                          marginRight: 15,
                          border: "none",
                        }}
                      >
                        <TimerIcon
                          htmlColor={theme.palette.secondary.light}
                          className={classes.timerBtnIcon}
                        />
                      </CustomIconButton>
                    ) : (
                      <CustomIconButton
                        btnType="gray"
                        // onClick={this.handleAddBtnClick}
                        style={{
                          padding: 6,
                          marginLeft: 15,
                          marginRight: 15,
                          border: "none",
                        }}
                      >
                        <KeyboardIcon
                          htmlColor={theme.palette.secondary.light}
                          className={classes.timerBtnIcon}
                        />
                      </CustomIconButton>
                    )}
                    {project["status"] !== "Approved" && project["status"] !== "In Review" &&
                      timesheetPer.deleteTimeEntry.cando && (
                        <IconButton
                          style={{
                            padding: 2,
                            right: 0,
                            background: "#de133e",
                            borderRadius: "50%",
                          }}
                          onClick={() => this.deleteTimeLog(t)}
                        >
                          <CloseIcon
                            className={classes.closeIcon}
                            htmlColor={theme.palette.common.white}
                          />
                        </IconButton>
                      )}
                  </div>
                </div>
              </>
            );
          })}
          <div style={{ display: "flex" }}>
            {project["status"] !== "Approved" && project["status"] !== "In Review" && showAddEntry && !days == 1 && timesheetPer.addTimeEntry.cando && (
              <p
                className={classes.showNotesBtn}
                onClick={this.addNewTimeEntry} // sending index to assign a dummy unique id to newly created time log
              >
               <FormattedMessage id="timesheet.time-entry.creation-dialog.add-entry.label" defaultMessage="Add Entry"/> 
              </p>
            )}
            {!showNotes &&
            !timeEntries.notes &&
            project["status"] !== "Approved" && project["status"] !== "In Review" &&
            timesheetPer.addNotes.cando && ( // show link to add notes, clicking on link shows the text area to add notes
                <p
                  className={classes.addMoreDetailsBtn}
                  onClick={this.handleShowNotes}
                >
                <FormattedMessage id="timesheet.time-entry.creation-dialog.add-notes.labl" defaultMessage="Add Notes"/>   
                </p>
              )}
          </div>
          {(showNotes || timeEntries.notes) && (
            <DefaultTextField
              label={<FormattedMessage id="timesheet.time-entry.creation-dialog.notes.label" defaultMessage="Notes"/>}
              error={false}
              defaultProps={{
                value: effortNotes,
                onChange: this.handleNotesInput,
                rows: 5,
                multiline: true,
                disabled: (project["status"] == "Approved" && project["status"] == "In Review") && !timesheetPer.addNotes.cando
              }}
            />
          )}
          <div className={classes.totalTimeCnt}>
            <img src={TimerIconGray} className={classes.timerIconGray} />{" "}
            <span>
              <FormattedMessage id="timesheet.time-entry.creation-dialog.total-time.label" values={{h:days == 1 ? "24" : hours, m: minutes }}  defaultMessage={"Total Time : " + (days == 1 ? "24" : hours)+ " hours " +minutes+ " minutes"} /> 
            </span>
          </div>
          <NotificationMessage
            type="NewInfo"
            iconType="info"
            style={{
              marginTop: "12px",
              padding: "10px 15px 10px 15px",
              backgroundColor: theme.palette.background.light,
              borderRadius: 4,
            }}
          >
           <FormattedMessage id="timesheet.time-entry.creation-dialog.notes.placeholder"   defaultMessage="Note: Time entry cannot exceed 24 hours limit per day."/> 
          </NotificationMessage>
        </div>
        {project["status"] !== "Approved" && project["status"] !== "In Review" && (
          <ButtonActionsCnt
            cancelAction={closeAction}
            successAction={this.handleSaveTimeEntry}
            successBtnText={<FormattedMessage id="timesheet.time-entry.creation-dialog.save-button.label" defaultMessage="Save Changes" />}
            cancelBtnText={<FormattedMessage id="common.action.cancel.label"  defaultMessage="Cancel"/>}
            btnType="success"
            btnQuery={this.state.btnQuery}
            successBtnProps={{
              disabled: disableSaveButton || totalMin > 1440,
            }}
          />
        )}
      </CustomDialog>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    profile: state.profile.data,
  };
};

export default compose(
  withSnackbar,
  injectIntl,
  withStyles(timesheetStyles, {
    withTheme: true,
  }),
  connect(mapStateToProps, {
    addTeamTaskDuration,
    addUserEffort,
    getTeamDateTaskEffort,
  })
)(AddDurationDialog);
