import React, { Component } from "react";
import sortBy from "lodash/sortBy";
import find from "lodash/find";
import findIndex from "lodash/findIndex";
import filter from "lodash/filter";
import cloneDeep from "lodash/cloneDeep";
import CustomIconButton from "../../components/Buttons/CustomIconButton";
import RightArrow from "@material-ui/icons/ArrowRight";
import LeftArrow from "@material-ui/icons/ArrowLeft";
import CustomButton from "../../components/Buttons/CustomButton";
import timesheetStyles from "./styles";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
import { connect } from "react-redux";
import moment from "moment";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import TaskIcon from "@material-ui/icons/ViewList";
import Typography from "@material-ui/core/Typography";
import RejectDialog from "./RejectDialog";
import MessageIcon from "@material-ui/icons/Message";
import EmptyState from "../../components/EmptyStates/EmptyState";
import SvgIcon from "@material-ui/core/SvgIcon";
import CustomTooltip from "../../components/Tooltip/Tooltip";
import { timesheetsHelpText } from "../../components/Tooltip/helptext";
import helper from "../../helper";
import ProjectsIcon from "../../components/Icons/ProjectIcon";
import CustomAvatar from "../../components/Avatar/Avatar";
import {
  getWeekDates,
  calRowsTotalTime,
  generateWeekDates,
  getDate,
  calColsTotalTime,
  calTotalTime,
} from "./timingFunctions";
import { generateUsername } from "../../utils/common";
import {
  getPendingTeamTimesheetWeekDurations,
  rejectTeamPendingTimesheet,
  approvePendingTeamTimesheet,
  selectTimesheetWorkspaces,
  SaveTotalTimeTitle,
  withdrawTeamTimesheet,
} from "../../redux/actions/timesheet";
import { updateTasksTimesheetStatus } from "../../redux/actions/tasks";
import { calculateContentHeight } from "../../utils/common";
import { FormattedMessage } from "react-intl";
import CustomMultiSelectDropdown from "../../components/Dropdown/CustomMultiSelectWithSearchBarDropdown/Dropdown";
import { generateWorkspaceData } from "../../helper/generateTasksOverviewDropdownData";
import DefaultTextField from "../../components/Form/TextField";
import AddDurationDialog from "./AddDurationDialog";
import AddCustomTotalTime from "./AddCustomTotalTime";
import classNames from "classnames";
import isEqual from "lodash/isEqual";
import ImportExport from "@material-ui/icons/ImportExport";
import apiInstance from "../../redux/instance";
import fileDownload from "js-file-download";
import LockIcon from "@material-ui/icons/Lock";
import WithdrawDialog from "../Timesheet/WithdrawDialog";
import TimesheetComments from "../../components/TimesheetComments/TimesheetComments.view";
import BillableIcon from "../../components/Icons/Billable";
import NonBillableIcon from "../../components/Icons/NonBillable";
import CustomDatePicker from "../../components/DatePicker/DatePicker/ListViewDatePicker";
import Loader from "../../components/Loader/Loader";
import StaticDatePicker from "../../components/DatePicker/StaticDatePicker";
import { inDateRange } from "../../helper/dates/dates";
import ExportDialog from "./ExportDialog";

class PendingTimesheet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: null,
      rejectDialog: false,
      weekDates: [],
      timesheetData: {},
      selectedUserId: null,
      selectedProjectId: null,
      rejectBtnQuery: "",
      approveBtnQuery: "",
      totalTimeLogged: "",
      selectedWorkspaces: [],
      exportDialog: false,
      fromDate: this.props.date,
      toDate: this.props.date,
      editTitle: false,
      totalTimeTitle: "",
      addDurationDialogState: {
        open: false,
        hours: "",
        mins: "",
        effortObj: "",
        timeStamp: "",
        project: {},
        workspaceId: "",
      },
    };
    this.handleThisWeek = this.handleThisWeek.bind(this);
  }
  componentDidMount() {
    this.showLoader();
    const { date } = this.props;
    if (date.toString() === moment().startOf("isoWeek").toString()) {
      this.loadWeekData("");
    } else {
      this.loadWeekData(`&currentDate=${date.format("YYYY-MM-DDTHH:mm:ss")}`)
    }
    this.setState({
      fromDate: date.format("YYYY-MM-DDTHH:mm:ss"),
      toDate: date.clone().endOf("isoWeek").format("YYYY-MM-DDTHH:mm:ss"),
    })
    // this.loadWeekData("");
  };
  handleThisWeek() {
    const { date } = this.props;
    const currentDate = moment().startOf("isoWeek");
    if (date.toString() !== currentDate.toString()) {
      this.showLoader();
      this.props.setTimesheetSelectedWeek(currentDate);
    }
  }
  workspacesDropdownData = () => {
    const { workspace } = this.props.profileState.data;
    return generateWorkspaceData(workspace);
  };
  onWorkspaceSelect = workspace => {
    const { date } = this.props;
    this.setState({ selectedWorkspaces: workspace });
    const workspaceIds = workspace.map(w => w.obj.teamId);
    this.props.selectTimesheetWorkspaces({ workspaceIds: workspaceIds }, () => {
      this.showLoader();
      this.loadWeekData(`&currentDate=${moment(date).format("YYYY-MM-DDTHH:mm:ss")}`);
    });
  };
  handleNextWeek = () => {
    const { date } = this.props
    const nextWeek = date.clone().add(7, "days");
    this.showLoader();
    this.props.setTimesheetSelectedWeek(nextWeek);
    // this.setState({ date: nextWeek });
    // this.loadWeekData(`&currentDate=${nextWeek.format("YYYY-MM-DDTHH:mm:ss")}`);
  };
  handlePrevWeek = () => {
    const { date } = this.props;
    const prevWeek = date.clone().subtract(7, "days");
    this.showLoader();
    this.props.setTimesheetSelectedWeek(prevWeek);
    // this.setState({ date: prevWeek });
    // this.loadWeekData(`&currentDate=${prevWeek.format("YYYY-MM-DDTHH:mm:ss")}`);
  };
  handleSelectDate = (date) => {
    if (date) {
      const startDate = moment(date).startOf('isoWeek');
      this.props.setTimesheetSelectedWeek(startDate);
      // this.setState({ date: startDate });
      // this.loadWeekData(`&currentDate=${startDate.format("YYYY-MM-DDTHH:mm:ss")}`);
    }
  }
  handleChange = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false,
    });
  };
  loadWeekData = parms => {
    this.props.getPendingTeamTimesheetWeekDurations(parms, (error, response) => {
      if (error) {
        this.hideLoader();
      } else {
        if (response) {
          const selectedWorkspaceData = this.workspacesDropdownData().filter(wd =>
            this.props.timesheetState.timeEntry.selectedWorkspaces.includes(wd.obj.teamId)
          );

          this.setState({
            timesheetData: cloneDeep(this.props.timesheetState.pendingApproved),
            selectedWorkspaces: selectedWorkspaceData,
          });
          this.hideLoader();
        } else {
          this.hideLoader();
        }
      }
    });
  };
  withdrawDialogOpen = (event, selectedProjectId, totalTime, workspaceId) => {
    event.stopPropagation();
    this.setState({
      withdrawDialog: true,
      selectedProjectId,
      totalTimeLogged: totalTime,
      workspaceId,
    });
  };
  withdrawDialogClose = () => {
    this.setState({ withdrawDialog: false });
  };
  handleWithdraw = comment => {
    const {
      selectedProjectId,
      timesheetData,
      selectedUserId,
      totalTimeLogged,
      workspaceId,
    } = this.state;
    const { date } = this.props;
    const data = {
      projectId: selectedProjectId,
      workspaceId: workspaceId,
      submitBy: selectedUserId,
      other: selectedProjectId ? false : true,
      startDate: date.format("YYYY-MM-DDTHH:mm:ss"),
      weeklyTime: totalTimeLogged,
      endDate: date
        .clone()
        .add(6, "days")
        .format("YYYY-MM-DDTHH:mm:ss"),
      comment,
    };
    if (!selectedProjectId) {
      let otherTasks = [];
      const userIndex = findIndex(timesheetData["userSubmittedTimesheet"] || [], {
        submitUser: selectedUserId,
      });
      const projects =
        timesheetData["userSubmittedTimesheet"][userIndex]["projectTimesheetVM"] || [];
      const projectIndex = findIndex(projects, {
        projectId: selectedProjectId,
      });
      const tasks =
        timesheetData["userSubmittedTimesheet"][userIndex]["projectTimesheetVM"][projectIndex][
        "taskEffortVM"
        ] || [];
      tasks.map(obj => {
        otherTasks.push(obj.taskId);
      });
      data.OtherTaskIds = otherTasks;
    }
    this.setState({ rejectBtnQuery: "progress" });
    this.props.withdrawTeamTimesheet(data, (error, response) => {
      this.setState({ rejectBtnQuery: "" });
      if (error) {
        this.setState({
          rejectDialog: false,
          withdrawDialog: false,
          workspaceId: "",
        });
      } else {
        if (response) {
          this.setState({
            timesheetData: cloneDeep(this.props.timesheetState.pendingApproved),
            rejectDialog: false,
            withdrawDialog: false,
            workspaceId: "",
          });
        }
      }
    });
  };
  rejectDialogOpen = (event, selectedProjectId, totalTime, worksapce) => {
    event.stopPropagation();
    this.setState({
      rejectDialog: true,
      selectedProjectId,
      totalTimeLogged: totalTime,
      submitTimesheetWorkspace: worksapce,
    });
  };
  rejectDialogClose = () => {
    this.setState({ rejectDialog: false });
  };
  handleTotalTimeInputChange = e => {
    this.setState({ totalTimeTitle: e.target.value });
  };

  handleReject = comment => {
    const {
      selectedProjectId,
      timesheetData,
      selectedUserId,
      totalTimeLogged,
      submitTimesheetWorkspace,
    } = this.state;
    const { date } = this.props;
    const data = {
      projectId: selectedProjectId,
      submitBy: selectedUserId,
      other: selectedProjectId ? false : true,
      startDate: date.format("YYYY-MM-DDTHH:mm:ss"),
      weeklyTime: totalTimeLogged,
      workspaceId: submitTimesheetWorkspace,
      endDate: date
        .clone()
        .add(6, "days")
        .format("YYYY-MM-DDTHH:mm:ss"),
      comment,
    };
    if (!selectedProjectId) {
      let otherTasks = [];
      const userIndex = findIndex(timesheetData["userSubmittedTimesheet"] || [], {
        submitUser: selectedUserId,
      });
      const projects =
        timesheetData["userSubmittedTimesheet"][userIndex]["projectTimesheetVM"] || [];
      const projectIndex = findIndex(projects, {
        projectId: selectedProjectId,
        workspaceId: submitTimesheetWorkspace,
      });
      const tasks =
        timesheetData["userSubmittedTimesheet"][userIndex]["projectTimesheetVM"][projectIndex][
        "taskEffortVM"
        ] || [];
      tasks.map(obj => {
        otherTasks.push(obj.taskId);
      });
      data.OtherTaskIds = otherTasks;
    }
    this.setState({ rejectBtnQuery: "progress" });
    this.props.rejectTeamPendingTimesheet(data, (error, response) => {
      this.setState({ rejectBtnQuery: "" });
      if (error) {
      } else {
        if (response) {
          this.setState({
            timesheetData: cloneDeep(this.props.timesheetState.pendingApproved),
            rejectDialog: false,
          });
        }
      }
    });
  };

  componentDidUpdate(prevProps, prevState) {
    if (
      !isEqual(prevProps.timesheetState.pendingApproved, this.props.timesheetState.pendingApproved)
    ) {
      this.setState({ timesheetData: cloneDeep(this.props.timesheetState.pendingApproved) });
    }
    const { date, timesheetState } = this.props;
    if (prevProps.profileState.data.loggedInTeam !== this.props.profileState.data.loggedInTeam) {
      this.showLoader();
      this.loadWeekData("");
    } else if (prevProps.date.toString() !== date.toString()) {
      if (
        date.toString() ===
        moment()
          .startOf("isoWeek")
          .toString()
      ) {
        this.showLoader();
        this.loadWeekData("");
      } else {
        this.showLoader();
        this.loadWeekData(`&currentDate=${date.format("YYYY-MM-DDTHH:mm:ss")}`);
      }
      this.setState({
        fromDate: this.props.date.format("YYYY-MM-DDTHH:mm:ss"),
        toDate: this.props.date.clone().endOf("isoWeek").format("YYYY-MM-DDTHH:mm:ss"),
      })
    } else if (
      JSON.stringify(timesheetState.timeEntry) !==
      JSON.stringify(prevProps.timesheetState.timeEntry)
    ) {
      this.setState({ timesheetData: cloneDeep(timesheetState.timeEntry) });
    }
  }

  handleApprove = (event, projectId, time, workspace) => {
    event.stopPropagation();
    const { timesheetData, selectedUserId } = this.state;
    const { date } = this.props;
    const data = {
      projectId: projectId,
      submitBy: selectedUserId,
      workspaceId: workspace,
      other: projectId ? false : true,
      startDate: date.format("YYYY-MM-DDTHH:mm:ss"),
      endDate: date
        .clone()
        .add(6, "days")
        .format("YYYY-MM-DDTHH:mm:ss"),
      weeklyTime: time,
    };
    if (!projectId) {
      let otherTasks = [];
      const userIndex = findIndex(timesheetData["userSubmittedTimesheet"] || [], {
        submitUser: selectedUserId,
      });
      const projects =
        timesheetData["userSubmittedTimesheet"][userIndex]["projectTimesheetVM"] || [];
      const projectIndex = findIndex(projects, { projectId: projectId });
      const tasks =
        timesheetData["userSubmittedTimesheet"][userIndex]["projectTimesheetVM"][projectIndex][
        "taskEffortVM"
        ] || [];
      tasks.map(obj => {
        otherTasks.push(obj.taskId);
      });
      data.OtherTaskIds = otherTasks;
    }
    this.setState({ approveBtnQuery: "progress" });
    this.props.approvePendingTeamTimesheet(data, (error, response) => {
      this.setState({ approveBtnQuery: "" });
      if (error) {
        this.props.showSnackBar(
          error.data.message,
          "error"
        );
      } else {
        if (response) {
          const resData = response.data;
          // In Progress -> Approved
          if (resData.status) {
            const statusProjectId = projectId;
            const timesheetData = this.props.timesheetState.pendingApproved;
            let taskIds = [];
            const userIndex = findIndex(timesheetData["userSubmittedTimesheet"] || [], {
              submitUser: selectedUserId,
            });
            const projects =
              timesheetData["userSubmittedTimesheet"][userIndex]["projectTimesheetVM"] || [];
            const projectIndex = findIndex(projects, {
              projectId: statusProjectId,
            });
            const tasks = projects[projectIndex]["taskEffortVM"] || [];
            tasks.map(obj => {
              taskIds.push(obj.taskId);
            });
            // this.props.updateTasksTimesheetStatus(taskIds, "Approved");
          }
          this.setState({
            timesheetData: cloneDeep(this.props.timesheetState.pendingApproved),
          });
        }
      }
    });
  };
  handleTimesheetClick = (userName, selectedUserId) => {
    this.setState({ selectedUserId }, () => {
      this.props.handleChangeView(userName);
    });
  };
  renderCurrentDate = (stamp, i) => {
    const date = this.props.date.clone().add(i, "days");
    const formatedDate = date.format("MM/DD/YYYY");
    if (moment(formatedDate).isSame(stamp.date)) {
      return stamp.duration || helper.standardTimeFormat("0:0");
    }
  };
  filterSelectedUserTimesheetData = () => {
    const { selectedUserId, timesheetData } = this.state;
    let filtered = cloneDeep(timesheetData);
    filtered =
      filter(timesheetData["userSubmittedTimesheet"], {
        submitUser: selectedUserId,
      }) || [];
    if (filtered.length) return filtered[0]["projectTimesheetVM"].filter(p => p.status != "Rejected");
    // return filtered[0]["projectTimesheetVM"]
    return filtered;
  };
  calculateUserBasedTimeLoggedTime = (user, weekDays) => {
    const timesheets = user["projectTimesheetVM"];
    let rowsTotal = [];
    timesheets.map(timesheet => {
      rowsTotal = rowsTotal.concat(calRowsTotalTime(weekDays, timesheet["taskEffortVM"]));
    });
    return calTotalTime(rowsTotal);
  };
  calculateUserBasedTimeOffsetTime = (user, weekDays) => {
    const timesheets = user["projectTimesheetVM"];
    let rowsTotal = timesheets.reduce((r, cv) => {
      cv.taskEffortVM.forEach(x => r.push(x.totalOffset));
      return r;
    }, []);
    return calTotalTime(rowsTotal);
  };
  calculateUserBasedApprovedTime = (user, weekDays) => {
    const timesheets = user["projectTimesheetVM"];
    let rowsTotal = timesheets.reduce((r, cv) => {
      cv.taskEffortVM.forEach(x => r.push(x.approvedWeekTotalEffort));
      return r;
    }, []);
    return calTotalTime(rowsTotal);
  };
  //Function that open task duration popup
  handleAddTaskDuration = (project, effortObj, timeStamp, flag) => {
    this.setState({
      addDurationDialogState: {
        open: true,
        effortObj: effortObj,
        timeStamp: timeStamp,
        project: project,
        flag,
      },
    });
  };
  //Function that open task duration popup
  closeAddTaskDuration = () => {
    this.setState({
      addDurationDialogState: {
        open: false,
        effortObj: {},
        timeStamp: {},
        project: {},
      },
    });
  };
  showTranslatedStatus = status => {
    let intl = { id: "", message: "" };
    switch (status) {
      case "In Progress":
        intl.id = "task.common.status.dropdown.in-progress";
        intl.message = "In Progress";
        break;
      case "In Review":
        intl.id = "task.common.status.dropdown.in-review";
        intl.message = "In Review";
        break;
      case "Rejected":
        intl.id = "timesheet.status.reject";
        intl.message = "Rejected";
        break;
      case "Approved":
        intl.id = "timesheet.status.approve";
        intl.message = "Approved";
        break;
      case "In Progress - Withdrawn Approval":
        intl.id = "timesheet.status.withDraw";
        intl.message = "In Progress - Withdrawn Approval";
        break;
      default:
        intl.id = "";
        intl.message = status;
        break;
    }
    return <FormattedMessage id={intl.id} defaultMessage={intl.message} />;
  };
  getWorkspace = workspaceId => {
    const { profileState } = this.props;

    return profileState.data.workspace.find(w => {
      return w.teamId == workspaceId;
    });
  };
  handleTitleClick = () => {
    const { timesheetData } = this.state;
    this.setState({ editTitle: true, totalTimeTitle: timesheetData.lableTimeLogged });
  };
  handleSaveTitle = (e, ws) => {
    if (e.keyCode == 13) {
      this.setState({ editTitle: false });
      const postObj = { LabelLoggedHours: this.state.totalTimeTitle };
      this.props.SaveTotalTimeTitle(postObj, res => {
        // this.loadWeekData(`&currentDate=${moment(date).format("YYYY-MM-DDTHH:mm:ss")}`);
      });
    }
  };
  showLoader = () => {
    this.setState({ isLoading: true });
  };
  hideLoader = () => {
    this.setState({ isLoading: false });
  };
  handleExportExcel = () => {
    this.setState({ exportBtnQuery: "progress" });

    const fileName = `timesheet.xlsx`;
    apiInstance()
      .get(
        BASE_URL +
        `api/TeamTimeSheet/GetTeamTimeSheetPendingDownload?timeSheetType=Weekly&currentDate=${moment(
          this.props.date
        ).format("YYYY-MM-DDTHH:mm:ss")}`,
        {
          responseType: "blob",
        }
      )
      .then(res => {
        this.setState({ exportBtnQuery: "" });
        fileDownload(res.data, fileName);
        // showMessage(`File downloaded successfully.`, "success");
      });
    return;
  };

  // ******* simple export function starts here *******

  // It is called to close Export dialog
  handleExportOpen = () => {
    this.setState({ exportDialog: true });
    return;
  };
  // It is called to close Export dialog
  handleExportClose = event => {
    this.setState({ exportDialog: false });
  };

  handleSelectExportDate = (date, type) => {
    const { fromDate, toDate } = this.state
    if (type == 'from') {
      if (date) {
        const inRangeCheck = inDateRange(date,
          moment(toDate).add(-1, "month").format('l'),
          moment(toDate).format('l'));
        if (inRangeCheck || toDate == null) {
          this.setState({ fromDate: moment(date).format("YYYY-MM-DD") });
        }
        else {
          this.setState({ fromDate: moment(date).format("YYYY-MM-DD"), toDate: moment(date).add(1, "month").format("YYYY-MM-DD") });
        }
      } else {
        this.setState({ fromDate: null });
      }
    }
    if (type == 'to') {
      if (date) {
        this.setState({ toDate: moment(date).format("YYYY-MM-DD") });
      } else {
        this.setState({ toDate: null });
      }
    }
  }
  exportAfterConfirmation = () => {
    this.setState({ exportBtnQuery: "progress" });
    const { fromDate, toDate } = this.state

    const fileName = `timesheet.xlsx`;
    apiInstance()
      .get(
        BASE_URL +
        `api/TeamTimeSheet/GetTeamTimeSheetPendingDownloadClassic?status=approved&timeSheetType=Weekly&currentDate=${moment(fromDate).format("YYYY-MM-DDTHH:mm:ss")}&lastDate=${moment(toDate).format("YYYY-MM-DDTHH:mm:ss")}`,
        {
          responseType: "blob",
        }
      )
      .then(res => {
        this.setState({ exportBtnQuery: "", exportDialog: false });
        fileDownload(res.data, fileName);
        // showMessage(`File downloaded successfully.`, "success");
      });
    return;
  };
  getMemberName = id => {
    return this.props.profileState.data.teamMember.find(m => m.userId == id).fullName.toLowerCase();
  };
  render() {
    const { classes, theme, activeView, timesheetPer, date } = this.props;
    const allMembers = this.props.profileState.data.teamMember;
    const {
      expanded,
      rejectDialog,
      timesheetData,
      rejectBtnQuery,
      approveBtnQuery,
      selectedWorkspaces,
      addDurationDialogState,
      editTitle,
      totalTimeTitle,
      isLoading,
      exportDialog,
      fromDate,
      toDate,
      exportBtnQuery,
      withdrawDialog,
    } = this.state;
    let users = [],
      projects = [];
    if (activeView === 0) {
      users = timesheetData["userSubmittedTimesheet"] || [];
      if (users.length) {
        users = users.reduce((r, cv) => {
          const filterdTimesheet = cv.projectTimesheetVM.filter(p => p.status != "Rejected");
          let tempObj;
          if (filterdTimesheet.length) {
            tempObj = { ...cv, projectTimesheetVM: filterdTimesheet }
            r.push(tempObj);
          }
          return r;
        }, []);
      }
      users = users.sort((a, b) => {
        if (this.getMemberName(a.submitUser) < this.getMemberName(b.submitUser)) {
          return -1;
        }
        if (this.getMemberName(a.submitUser) > this.getMemberName(b.submitUser)) {
          return 1;
        }
        return 0;
      });
      users = users.filter(o => {
        return o.submitUser;
      });
    } else {
      projects = this.filterSelectedUserTimesheetData();
      if (projects.length) {
        // Sort data
        projects = sortBy(projects, [
          function (o) {
            if (o.projectName === "Other Tasks") return "";
            else return o.projectName;
          },
        ]);
      } else {
        this.props.handleChangeView(0);
      }
    }
    const weekDays = getWeekDates(date);
    const usedWorkspaces = projects.reduce((r, cv) => {
      if (!r.includes(cv.workspaceId)) {
        r.push(cv.workspaceId);
      }
      return r;
    }, []);
    const workspacesToShow = selectedWorkspaces.reduce((r, cv) => {
      const isWorkspaceExist = projects.find(p => p.workspaceId == cv.obj.teamId);
      if (isWorkspaceExist) {
        r.push(cv);
      }
      return r;
    }, []);

    return (
      <div
        className={classNames({ [classes.timeSheetCnt]: true, [classes.loaderCnt]: isLoading })}
        style={{ height: calculateContentHeight() }}>
        {isLoading ? (
          <Loader />
        ) : (
          <>
            {activeView === 0 ? (
              <>
                {/* Next Prev Buttons Starts */}
                <div className="flex_center_space_between_row mb20">
                  <div className={classes.NextBackBtnCnt}>
                    <CustomIconButton
                      btnType="condensed"
                      style={{ padding: 0 }}
                      onClick={this.handlePrevWeek}>
                      <LeftArrow
                        htmlColor={theme.palette.secondary.dark}
                        classes={{ root: classes.navigationArrow }}
                      />
                    </CustomIconButton>
                    <div className={classes.selectDate}>
                      <span className={classes.toolbarCurrentDate}>{getDate(date)}</span>
                      <CustomDatePicker
                        dateFormat="MMM DD, YYYY"
                        timeInput={false}
                        datePickerProps={{
                        }}
                        onSelect={(date) => {
                          this.handleSelectDate(date)
                        }}
                        filterDate={moment()}
                        btnProps={{ className: classes.datePickerCustomStyle }}
                        style={{
                          display: "flex",
                          alignItems: "center",
                          opacity: 0,
                          position: 'absolute',
                          top: 0,
                          left: 0,
                          width: '100%',
                          height: '100%',
                          zIndex: 2,
                        }}
                        PopperProps={{ disablePortal: false }}
                        showDates={false}
                        placeholder={"Select Date"}
                        icon={true}
                      />
                    </div>

                    <CustomIconButton
                      btnType="condensed"
                      style={{ padding: 0 }}
                      onClick={this.handleNextWeek}>
                      <RightArrow
                        htmlColor={theme.palette.secondary.dark}
                        classes={{ root: classes.navigationArrow }}
                      />
                    </CustomIconButton>
                  </div>
                  <div style={{ display: "flex", alignItems: "center" }}>
                    {([
                      "b286b41822d845ae96489a60b06280ae",
                      "630c29e8a2a84ff887f6d3ea1a2907f7",
                      "e26f65f31d2546b5bb9bf31f8e581d74",
                    ].includes(this.props.profileState.data.activeTeam) || ENV === "dev" || ENV === "uat") && (
                        <CustomButton
                          variant={"outlined"}
                          btnType={"success"}
                          onClick={this.handleExportOpen}
                          btnQuery={exportBtnQuery}
                          style={{ padding: "3px 12px", marginRight: 10 }}>
                          <ImportExport /> Simple Export to Excel
                        </CustomButton>
                      )}
                    <CustomButton
                      variant={"outlined"}
                      btnType={"success"}
                      onClick={this.handleExportExcel}
                      btnQuery={exportBtnQuery}
                      style={{ padding: "3px 12px", marginRight: 10 }}>
                      <ImportExport /> Export to Excel
                    </CustomButton>
                    <CustomMultiSelectDropdown
                      label="Workspaces"
                      hideLabel={true}
                      options={() => this.workspacesDropdownData()}
                      option={selectedWorkspaces}
                      onSelect={this.onWorkspaceSelect}
                      scrollHeight={400}
                      size={"large"}
                      buttonProps={{
                        variant: "contained",
                        btnType: "white",
                        labelAlign: "left",
                        style: {
                          width: 250,
                          padding: "3px 4px 3px 8px",
                          height: 35,
                          marginRight: 15,
                        },
                      }}
                    />
                    {!date.isSame(moment().startOf("isoWeek")) ? (
                      <CustomButton
                        onClick={this.handleThisWeek}
                        btnType="graydashed"
                        variant="contained">
                        <FormattedMessage
                          id="timesheet.this-week.label"
                          defaultMessage="This Week"
                        />
                      </CustomButton>
                    ) : null}
                  </div>
                </div>
                {/* Next Prev Buttton Ends */}
                {users.length ? null : (
                  <div className={classes.emptyStateCnt}>
                    <EmptyState
                      screenType="timesheet"
                      heading={
                        <FormattedMessage
                          id="timesheet.no-timesheet-submitted.label"
                          defaultMessage="No Timesheets Submitted"
                        />
                      }
                      message={
                        <FormattedMessage
                          id="timesheet.no-timesheet-submitted.message"
                          defaultMessage="There are no timesheets pending to be approved."
                        />
                      }
                      button={false}
                    />
                  </div>
                )}
                {users.map((user, index) => {
                  const totalTime = this.calculateUserBasedTimeLoggedTime(user, weekDays);
                  const totalOffsetTime = this.calculateUserBasedTimeOffsetTime(user, weekDays);
                  const totalUserApprovedEffort = this.calculateUserBasedApprovedTime(
                    user,
                    weekDays
                  );
                  let localUser = find(allMembers, { userId: user.submitUser });
                  let name = "";
                  if (localUser)
                    name = generateUsername(
                      localUser.email,
                      localUser.userName,
                      localUser.fullName
                    );
                  else {
                    localUser = { imageUrl: `` };
                  }
                  return (
                    <ExpansionPanel
                      onChange={() => this.handleTimesheetClick(name, user["submitUser"])}
                      classes={{ root: classes.accordPanelRootMember }}
                      key={index}>
                      <ExpansionPanelSummary
                        classes={{
                          root: classes.accordSummaryPanelPending,
                          content: classes.accordSummaryPanelContent2,
                        }}
                        expandIcon={<RightArrow style={{ right: 0 }} />}
                        key={index}>
                        <div
                          className={classes.accordSummaryInnerCnt}>
                          <div className="flex_center_start_row">
                            <CustomAvatar
                              otherMember={{
                                imageUrl: localUser.imageUrl,
                                fullName: localUser.fullName,
                                lastName: "",
                                email: localUser.email,
                                isOnline: localUser.isOnline,
                                isOwner: localUser.isOwner,
                              }}
                              size="small"
                            />
                            <Typography variant="h5" className={classes.accorheading}>
                              {name}
                            </Typography>
                          </div>
                          <div className="flex_center_start_row">
                            <div
                              className={`flex_center_center_col ${classes.timeLoggedCnt}`}
                              style={{ borderRight: "none", paddingRight: 0 }}>
                              <Typography variant="caption">Time Logged</Typography>
                              <p>
                                {helper.standardTimeFormat(totalTime)}
                                <span>
                                  <FormattedMessage
                                    id="timesheet.hours.label"
                                    defaultMessage="hours"
                                  />
                                </span>
                              </p>
                            </div>
                            <div className={`flex_center_center_col ${classes.timeLoggedCnt}`}
                              style={{ borderRight: "none", paddingRight: 0 }}>
                              <Typography variant="caption">
                                {timesheetData && timesheetData.lableTimeLogged}
                              </Typography>
                              <p>
                                {helper.standardTimeFormat(totalUserApprovedEffort)}
                                <span>
                                  <FormattedMessage
                                    id="timesheet.hours.label"
                                    defaultMessage=" hours"
                                  />
                                </span>
                              </p>
                            </div>
                            {/* offset of complete worksapce */}
                            {([
                              "b286b41822d845ae96489a60b06280ae",
                              "630c29e8a2a84ff887f6d3ea1a2907f7",
                              "e26f65f31d2546b5bb9bf31f8e581d74",
                            ].includes(this.props.profileState.data.activeTeam) || ENV === "dev" || ENV === "uat") &&
                              <div
                                className={`flex_center_center_col ${classes.timeLoggedCnt}`}
                                style={{ borderRight: "none", paddingRight: 0 }}>
                                <Typography variant="caption">Offset</Typography>
                                <p>
                                  {helper.standardTimeFormat(totalOffsetTime)}
                                  <span>
                                    <FormattedMessage
                                      id="timesheet.hours.label"
                                      defaultMessage="hours"
                                    />
                                  </span>
                                </p>
                              </div>
                            }
                          </div>
                        </div>
                      </ExpansionPanelSummary>
                    </ExpansionPanel>
                  );
                })}
              </>
            ) : (
              <>
                <RejectDialog
                  open={rejectDialog}
                  closeAction={this.rejectDialogClose}
                  action={this.handleReject}
                  btnQuery={rejectBtnQuery}
                />
                <WithdrawDialog
                  open={withdrawDialog}
                  closeAction={this.withdrawDialogClose}
                  action={this.handleWithdraw}
                  btnQuery={rejectBtnQuery}
                />
                {workspacesToShow.map(w => {
                  return (
                    <ExpansionPanel
                      defaultExpanded={true}
                      // expanded={expanded === `${index}`}
                      // onChange={this.handleChange(`${index}`)}
                      classes={{
                        root: classes.workspaceAccordPanelRoot,
                        // expanded: classes.accordPanelRootExpand,
                      }}
                    // key={index}
                    >
                      <ExpansionPanelSummary
                        classes={
                          {
                            // root: classes.workspaceAccordSummaryPanel,
                            // content: classes.accordSummaryPanelContent,
                            // expanded: classes.accordSummaryPanelExpand,
                          }
                        }
                        expandIcon={<ExpandMoreIcon />}>
                        <Typography variant="h2" className={classes.workspaceAccorheading}>
                          Workspace name: {this.getWorkspace(w.obj.teamId).teamName}
                        </Typography>
                      </ExpansionPanelSummary>
                      <ExpansionPanelDetails classes={{ root: classes.expansionDetailsCnt }}>
                        {projects.map((project, index) => {
                          const data = project["taskEffortVM"] || [];
                          const rowsTotalTime = calRowsTotalTime(weekDays, data);
                          const colsTotalTime = calColsTotalTime(weekDays, data);
                          const totalTime = calTotalTime(data.map(d => d.weekTotalEffort));
                          const totalApprovedEffort = calTotalTime(data.map(d => d.approvedWeekTotalEffort));
                          const totalOffset = calTotalTime(data.map(d => d.totalOffset));
                          const statusClass =
                            project["status"] == "In Review"
                              ? classes.inReview
                              : project["status"] == "Rejected"
                                ? classes.rejected
                                : project["status"] == "Approved"
                                  ? classes.approved
                                  : project["status"] == "In Progress - Withdrawn Approval"
                                    ? classes.withdraw
                                    : classes.inProgress;
                          return (
                            w.obj.teamId == project.workspaceId && (
                              <ExpansionPanel
                                expanded={expanded === `${index}`}
                                onChange={this.handleChange(`${index}`)}
                                classes={{ root: classes.accordPanelRoot }}
                                key={index}>
                                <ExpansionPanelSummary
                                  classes={{
                                    root: classes.accordSummaryPanel,
                                    content: classes.accordSummaryPanelContent2,
                                  }}
                                  expandIcon={<ExpandMoreIcon />}>
                                  <div className={classes.accordSummaryInnerCnt}>
                                    <div className="flex_center_start_row">
                                      {project["projectName"] == "Other Tasks" ? (
                                        <SvgIcon
                                          viewBox="0 0 18 15.188"
                                          className={classes.smallBtnIcon}>
                                          <TaskIcon />
                                        </SvgIcon>
                                      ) : (
                                        <SvgIcon
                                          viewBox="0 0 18 15.188"
                                          className={classes.smallBtnIcon}>
                                          <ProjectsIcon />
                                        </SvgIcon>
                                      )}
                                      <Typography variant="h5" className={classes.accorheading}>
                                        {project["projectName"] == "Other Tasks" ||
                                          project["projectName"] == "" ? (
                                          "Other Tasks"
                                        ) : (
                                          <>
                                            <b>Project Name: </b>
                                            {project["projectName"]}
                                          </>
                                        )}
                                      </Typography>
                                      {project["projectName"] == "Other Tasks" ? (
                                        <CustomTooltip
                                          helptext={timesheetsHelpText.otherTasksHelpText}
                                          iconType="help"
                                          position="static"
                                        />
                                      ) : null}
                                      <span className={`${classes.statusTag} ${statusClass}`}>
                                        {this.showTranslatedStatus(project["status"])}
                                      </span>
                                      <TimesheetComments comments={project["rejectReason"]} />
                                    </div>
                                    <div className="flex_center_start_row">
                                      {project["status"] == "Approved" ? (
                                        <CustomButton
                                          btnType="gray"
                                          variant="contained"
                                          onClick={e =>
                                            this.withdrawDialogOpen(
                                              e,
                                              project["projectId"],
                                              helper.standardTimeFormat(totalTime),
                                              w.obj.teamId
                                            )
                                          }>
                                          <LockIcon
                                            htmlColor={theme.palette.secondary.light}
                                            className={classes.lockIcon}
                                          />
                                          <FormattedMessage
                                            id="timesheet.withdraw-approval-button.label"
                                            defaultMessage="Withdraw Approval"
                                          />
                                        </CustomButton>
                                      ) : null}
                                      {project["status"] == "In Review" ? (
                                        <>
                                          <CustomButton
                                            btnType="dangerText"
                                            style={{ marginRight: 10 }}
                                            variant="contained"
                                            onClick={e =>
                                              this.rejectDialogOpen(
                                                e,
                                                project["projectId"],
                                                helper.standardTimeFormat(totalTime),
                                                w.obj.teamId
                                              )
                                            }
                                            disabled={!timesheetPer.rejectTimesheet.cando}>
                                            <FormattedMessage
                                              id="timesheet.reject.label"
                                              defaultMessage="Reject"
                                            />
                                          </CustomButton>
                                          <CustomButton
                                            btnType="success"
                                            variant="outlined"
                                            onClick={event =>
                                              this.handleApprove(
                                                event,
                                                project["projectId"],
                                                helper.standardTimeFormat(totalTime),
                                                w.obj.teamId
                                              )
                                            }
                                            query={approveBtnQuery}
                                            disabled={
                                              approveBtnQuery == "progress" ||
                                              !timesheetPer.acceptTimesheet.cando
                                            }>
                                            <FormattedMessage
                                              id="timesheet.approve-button.label"
                                              defaultMessage="Approve Timesheet"
                                            />
                                          </CustomButton>
                                        </>
                                      ) : null}
                                      <div
                                        className={`flex_center_center_col ${classes.timeLoggedCnt}`}
                                        style={{ borderRight: "none", paddingRight: 0 }}>
                                        <Typography variant="caption">Time Logged</Typography>
                                        <p>
                                          {helper.standardTimeFormat(totalTime)}
                                          <span>
                                            <FormattedMessage
                                              id="timesheet.hours.label"
                                              defaultMessage="hours"
                                            />
                                          </span>
                                        </p>
                                      </div>
                                      <div
                                        className={`flex_center_center_col ${classes.timeLoggedCnt}`}>
                                        <Typography variant="caption">
                                          {timesheetData && timesheetData.lableTimeLogged}
                                        </Typography>
                                        <p>
                                          {helper.standardTimeFormat(totalApprovedEffort)}
                                          <span>
                                            <FormattedMessage
                                              id="timesheet.hours.label"
                                              defaultMessage="hours"
                                            />
                                          </span>
                                        </p>
                                      </div>
                                      {([
                                        "b286b41822d845ae96489a60b06280ae",
                                        "630c29e8a2a84ff887f6d3ea1a2907f7",
                                        "e26f65f31d2546b5bb9bf31f8e581d74",
                                      ].includes(this.props.profileState.data.activeTeam) || ENV === "dev" || ENV === "uat") &&
                                        <div
                                          className={`flex_center_center_col ${classes.timeLoggedCnt}`}
                                          style={{ borderLeft: "none", paddingLeft: 0 }}>
                                          <Typography variant="caption">
                                            Offset
                                          </Typography>
                                          <p>
                                            {/* calculate offset hours here hhock */}
                                            {helper.standardTimeFormat(totalOffset)}
                                            <span>
                                              <FormattedMessage
                                                id="timesheet.hours.label"
                                                defaultMessage="hours"
                                              />
                                            </span>
                                          </p>
                                        </div>
                                      }
                                    </div>
                                  </div>
                                </ExpansionPanelSummary>
                                <ExpansionPanelDetails
                                  classes={{ root: classes.expansionDetailsCnt }}>
                                  {/* Time Sheet Header Starts */}
                                  <div className={classes.timesheetHeaderCnt}>
                                    <div className={classes.addTaskBtnCnt}>Task Title</div>
                                    <div className={classes.weekDatesCnt}>
                                      {generateWeekDates(weekDays, classes)}
                                      {editTitle ? (
                                        <DefaultTextField
                                          label={false}
                                          error={false}
                                          formControlStyles={{
                                            marginTop: 0,
                                            marginBottom: 0,
                                          }}
                                          fullWidth={false}
                                          defaultProps={{
                                            onChange: this.handleTotalTimeInputChange,
                                            onKeyDown: this.handleSaveTitle,
                                            inputProps: {
                                              style: {
                                                padding: "8px 14px",
                                                textAlign: "center",
                                                fontSize: "12px",
                                                transition: "0.15s padding ease",
                                                cursor: "text",
                                              },
                                            },
                                            value: totalTimeTitle,
                                          }}
                                        />
                                      ) : (
                                        <>
                                          <div
                                            className={classes.headerTotalCell}
                                            style={{ paddingRight: 0 }}>
                                            Time Logged
                                          </div>
                                          <div
                                            className={classes.headerTotalCellEditable}
                                            style={{ paddingRight: 0 }}
                                            onClick={e => this.handleTitleClick(e)}>
                                            {timesheetData.lableTimeLogged}
                                          </div>
                                          {([
                                            "b286b41822d845ae96489a60b06280ae",
                                            "630c29e8a2a84ff887f6d3ea1a2907f7",
                                            "e26f65f31d2546b5bb9bf31f8e581d74",
                                          ].includes(this.props.profileState.data.activeTeam) || ENV === "dev" || ENV === "uat") &&
                                            <div
                                              className={classes.headerTotalCell}
                                              style={{ paddingRight: 0 }}>
                                              Offset
                                            </div>
                                          }
                                        </>
                                      )}
                                    </div>
                                  </div>
                                  {/* Time Sheet Header Ends */}
                                  {data.map((ele, i) => {
                                    return (
                                      <div
                                        key={ele.taskId}
                                        className={classes.weekDatesRow}
                                        style={{ borderBottom: i == 0 ? "none" : "" }}>
                                        <p className={classes.taskTitle}>
                                          {/* billable/ none billable icons  */}
                                          {ele.isBillable ? <CustomTooltip
                                            helptext={
                                              'Billable Task'
                                            }
                                            placement="top"
                                            style={{ color: theme.palette.common.white }}>
                                            <SvgIcon
                                              viewBox="0 0 17.99 17.99"
                                              className={classes.billableIcon}>
                                              <BillableIcon />
                                            </SvgIcon>
                                          </CustomTooltip> :
                                            <CustomTooltip
                                              helptext={
                                                'Non-Billable Task'
                                              }
                                              placement="top"
                                              style={{ color: theme.palette.common.white }}>
                                              <SvgIcon
                                                viewBox="0 0 18.39 18.39"
                                                className={classes.nonBillableIcon}>
                                                <NonBillableIcon />
                                              </SvgIcon>
                                            </CustomTooltip>}
                                          {ele.taskTitle}
                                        </p>
                                        <div className={classes.weekDatesInputCnt}>
                                          {ele.timestamp.map((stamp, index) => {
                                            const renderUI = (
                                              <div
                                                key={stamp.id}
                                                className={classes.weekDatesCellPending}>
                                                {stamp.notes && (
                                                  <div className={classes.triangleTopRight}></div>
                                                )}
                                                <span>{this.renderCurrentDate(stamp, index)}</span>
                                              </div>
                                            );
                                            return stamp.notes ? (
                                              <CustomTooltip
                                                helptext={
                                                  <div>
                                                    <span className={classes.notesLabel}>
                                                      Notes:
                                                    </span>
                                                    <div className={classes.notes}>
                                                      {stamp.notes}
                                                    </div>
                                                  </div>
                                                }
                                                iconType="help"
                                                position="static"
                                                placement="right"
                                                hideArrow={true}
                                                classes={classes}>
                                                {renderUI}
                                              </CustomTooltip>
                                            ) : (
                                              renderUI
                                            );
                                          })}
                                          <div
                                            className={classes.totalTaskTimeLogCnt}
                                            style={{ paddingRight: 0 }}>
                                            {helper.standardTimeFormat(ele.weekTotalEffort)}
                                          </div>
                                          {/* nc hours */}
                                          <div
                                            className={classes.totalTaskTimeCnt}
                                            style={{ paddingRight: 0 }}
                                            onClick={
                                              project["status"].toLowerCase() == "in review" ||
                                                (ENV === "dev" || ENV === "uat") ||
                                                (["b286b41822d845ae96489a60b06280ae",
                                                  "630c29e8a2a84ff887f6d3ea1a2907f7",
                                                  "e26f65f31d2546b5bb9bf31f8e581d74",
                                                ].includes(this.props.profileState.data.activeTeam) && project["status"].toLowerCase() == "approved")
                                                ? e =>
                                                  this.handleAddTaskDuration(
                                                    project,
                                                    ele,
                                                    helper.standardTimeFormat(
                                                      ele.approvedWeekTotalEffort
                                                    ),
                                                    'hours'
                                                  )
                                                : null
                                            }>
                                            {helper.standardTimeFormat(ele.approvedWeekTotalEffort)}
                                          </div>
                                          {/* offset hours */}
                                          {([
                                            "b286b41822d845ae96489a60b06280ae",
                                            "630c29e8a2a84ff887f6d3ea1a2907f7",
                                            "e26f65f31d2546b5bb9bf31f8e581d74",
                                          ].includes(this.props.profileState.data.activeTeam) || ENV === "dev" || ENV === "uat") &&
                                            <div
                                              className={classes.totalTaskTimeCnt}
                                              style={{ paddingRight: 0 }}
                                              onClick={
                                                project["status"].toLowerCase() == "in review" || project["status"].toLowerCase() == "approved"
                                                  ? e =>
                                                    this.handleAddTaskDuration(
                                                      project,
                                                      ele,
                                                      helper.standardTimeFormat(
                                                        ele.approvedWeekTotalEffort
                                                      ),
                                                      'offset'
                                                    )
                                                  : null
                                              }>
                                              {/* bhock */}
                                              {ele.totalOffset}
                                            </div>
                                          }
                                        </div>
                                      </div>
                                    );
                                  })}
                                  <div className={classes.timesheetFooterCnt}>
                                    <p className={classes.footerTotalLabel}>
                                      <FormattedMessage
                                        id="common.total.label"
                                        defaultMessage="Total"
                                      />
                                    </p>
                                    <div className={classes.weekDatesCnt}>
                                      {colsTotalTime.map((time, i) => {
                                        return (
                                          <div
                                            key={i}
                                            className={this.props.classes.footerTotalCell}>
                                            {helper.standardTimeFormat(time)}
                                          </div>
                                        );
                                      })}
                                      {/* footer total time */}
                                      <div
                                        className={classes.footerTotalCell}
                                        style={{ paddingRight: 0 }}>
                                        {helper.standardTimeFormat(totalTime)}
                                      </div>
                                      <div
                                        className={classes.footerTotalCell}
                                        style={{ paddingRight: 0 }}>
                                        {helper.standardTimeFormat(totalApprovedEffort)}
                                      </div>
                                      {/* footer offset hours fhock */}
                                      {([
                                        "b286b41822d845ae96489a60b06280ae",
                                        "630c29e8a2a84ff887f6d3ea1a2907f7",
                                        "e26f65f31d2546b5bb9bf31f8e581d74",
                                      ].includes(this.props.profileState.data.activeTeam) || ENV === "dev" || ENV === "uat")
                                        &&
                                        <div
                                          className={classes.footerTotalCell}
                                          style={{ paddingRight: 0 }}>
                                          {helper.standardTimeFormat(totalOffset)}
                                        </div>
                                      }
                                    </div>
                                  </div>
                                </ExpansionPanelDetails>
                              </ExpansionPanel>
                            )
                          );
                        })}
                      </ExpansionPanelDetails>
                    </ExpansionPanel>
                  );
                })}
              </>
            )}
            <AddCustomTotalTime
              timeEntryProps={addDurationDialogState}
              closeAction={this.closeAddTaskDuration}
              date={date}
              timesheetPer={timesheetPer}
              loadWeekData={this.loadWeekData}
            />
            <ExportDialog
              open={exportDialog}
              handleClose={this.handleExportClose}
              fromDate={fromDate}
              toDate={toDate}
              handleDateChange={this.handleSelectExportDate}
              successAction={this.exportAfterConfirmation}
              btnQuery={exportBtnQuery}
            />
          </>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    timesheetState: state.timesheet,
    profileState: state.profile,
  };
};

export default compose(
  withRouter,
  withStyles(timesheetStyles, { withTheme: true }),
  connect(mapStateToProps, {
    getPendingTeamTimesheetWeekDurations,
    rejectTeamPendingTimesheet,
    approvePendingTeamTimesheet,
    updateTasksTimesheetStatus,
    selectTimesheetWorkspaces,
    SaveTotalTimeTitle,
    withdrawTeamTimesheet,
  })
)(PendingTimesheet);
