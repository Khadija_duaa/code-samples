import React, { Component } from "react";
import CustomDialog from "../../components/Dialog/CustomDialog";
import { Scrollbars } from "react-custom-scrollbars";
import { withSnackbar } from "notistack";
import { withStyles } from "@material-ui/core/styles";
import ButtonActionsCnt from "../../components/Dialog/ConfirmationDialogs/ButtonActionsCnt";
import { compose } from "redux";
import { connect } from "react-redux";
import timesheetStyles from "./styles";
import DefaultTextField from "../../components/Form/TextField";
import moment from "moment";
import DurationInput from "../../components/DatePicker/DurationInput";
import TimerIconGray from "../../assets/images/timer_gray.svg";
import {
  addTeamTaskDuration,
  getTeamDateTaskEffort, SaveTotalTime,
} from "../../redux/actions/timesheet";
import { addUserEffort } from "../../redux/actions/tasks";
import CustomIconButton from "../../components/Buttons/CustomIconButton";
import TimerIcon from "@material-ui/icons/AvTimer";
import KeyboardIcon from "@material-ui/icons/Keyboard";
import InputLabel from "@material-ui/core/InputLabel";
import cloneDeep from "lodash/cloneDeep";
import CloseIcon from "@material-ui/icons/Close";
import IconButton from "../../components/Buttons/IconButton";
import NotificationMessage from "../../components/NotificationMessages/NotificationMessages";
import { injectIntl, FormattedMessage } from "react-intl";
import SelectSearchDropdown from "../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { generateDuration100HoursData } from "../../helper/generateSelectData";
class AddCustomTotalTime extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isProceedClicked: false,
      checkedTeam: null,
      hours: "",
      mins: "",
      effortNotes: "",
      durationError: false,
      showNotes: false,
      timeEntries: { timeLogs: [] },
      saveButtonDisable: true,
      offsetHours: 0,
      errorMessage: '',
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.timeEntryProps.open && prevProps.timeEntryProps.open !== this.props.timeEntryProps.open) {

      const { effortObj } = this.props.timeEntryProps;
      const effortHrs = effortObj.approvedWeekTotalEffortHours
      const effortMins = effortObj.approvedWeekTotalEffortMin
      const offset = effortObj.offset

      const totalTime = { effortHours: effortHrs, effortMinutes: effortMins };
      this.setState({
        totalTime,
        hours: effortHrs, mins: effortMins,
        offsetHours: offset && offset > -1 ? offset : "",
        errorMessage: '',
      })
    }
  }
  componentDidMount() {
    // const { timeEntryProps } = this.props;
    // const { project, effortObj, timeStamp } = timeEntryProps;
    // const totalTime = { effortHours: effortObj.approvedWeekTotalEffortHours, effortMinutes: effortObj.approvedWeekTotalEffortMin };
    // this.setState({ totalTime });

  }
  // Function calls actions to save value to backend
  handleSaveTimeEntry = () => {
    this.setState({ btnQuery: 'progress', errorMessage: '' })
    const { totalTime, offsetHours } = this.state;
    const { timeEntryProps, date } = this.props;
    const postObj = {
      taskId: timeEntryProps.effortObj.taskId,
      workspaceId: timeEntryProps.project.workspaceId,
      hours: totalTime?.effortHours,
      Minutes: totalTime?.effortMinutes,
      offset: offsetHours,
      submittedby: timeEntryProps.effortObj.submitBy,
      startDate: date.format("YYYY-MM-DDTHH:mm:ss"),
      endDate: date.clone().add(7, "days")
    }
    if (this.ValidateTime((totalTime?.effortHours * 60) + Number(totalTime?.effortMinutes))) {
      this.props.SaveTotalTime(postObj, (res) => {
        this.setState({ btnQuery: '' });
        this.props.loadWeekData(`&currentDate=${moment(date).format("YYYY-MM-DDTHH:mm:ss")}`);
        this.props.closeAction();
      })
    } else {
      this.setState({
        btnQuery: '',
        errorMessage: 'Time entered cannot be greater than total time logged/Offset hours.'
      })
    }
  };

  ValidateTime = (givenTime) => {
    const { timeEntryProps: { effortObj: { totalOffSetInMins, weekTotalEffort }, flag } } = this.props;
    // if (isBillable) { 

    const totalWeekEfforts = moment.duration(weekTotalEffort).asMinutes();
    console.log(totalWeekEfforts)
    if ((totalWeekEfforts - totalOffSetInMins) < givenTime && flag == 'hours') {
      return false
    } else {
      return true
    }
    // } else {
    //   if (totalBillableEffortsInMins < givenTime) {
    //     this.setState({
    //       btnQuery: '',
    //       errorMessage: 'Show error'
    //     })
    //     return false
    //   } else {
    //     return true
    //   }
    // }
  }
  //Function updates the values of hours and mins in input dropdowns
  updateEffortTime = (hrs, mins, obj) => {
    const totalTime = { effortHours: hrs, effortMinutes: mins };
    this.setState({ totalTime });
  };
  processValue = (val) => {
    if (val <= 100 && 0 <= val)
      return val.replace(/[^.\d]/g, "").replace(/^(\d*\.?)|(\d*)\.?/g, "$1$2")
    else return this.state.offsetHours;
  };
  handleOffsetChange = (e) => {
    this.setState({ offsetHours: this.processValue(e.target.value) });
  };
  render() {
    const {
      classes,
      theme,
      open,
      headingText,
      closeAction,
      btnQuery,
      timeEntryProps,
      timesheetPer,
    } = this.props;
    const { project, effortObj, timeStamp, flag } = timeEntryProps;
    const {
      effortNotes,
      showNotes,
      timeEntries,
      saveButtonDisable,
    } = this.state;

    return (
      <CustomDialog
        // flag == 'offset' ? 'Total Weekly Effort with offset' :
        title={'Total Weekly Effort'}
        dialogProps={{
          open: timeEntryProps.open,
          onClick: (e) => {
            e.stopPropagation();
          },
          onClose: closeAction,
          PaperProps: {
            style: { maxWidth: 480 },
          },
        }}
      >

        <div className={classes.dialogContentCnt}>
          <DefaultTextField
            label={<FormattedMessage id="header.add-new-button.list.task.label" defaultMessage="Task" />}

            defaultProps={{
              disabled: true,
              value: effortObj.taskTitle,
            }}
          />
          <DefaultTextField
            label={<FormattedMessage id="header.add-new-button.list.project.label" defaultMessage="Project" />}
            error={false}
            formControlStyles={{ marginBottom: 42 }}
            defaultProps={{
              disabled: true,
              value: project.projectName
                ? project.projectName
                : ("--" + this.props.intl.formatMessage({ id: "timesheet.time-entry.creation-dialog.project.not-assigned.label", defaultMessage: "No project assigned" }) + "--"),
            }}
          />
          {timeEntries.timeLogs.length ? (
            <InputLabel classes={{ root: classes.dropdownsLabel }}>
              <FormattedMessage id="timesheet.time-entry.label" defaultMessage="Time Entry" />
            </InputLabel>
          ) : null}

          {flag == 'hours' ? <DurationInput
            updateTime={(hours, mins) =>
              this.updateEffortTime(hours, mins)
            }
            hourClock={168}
            showLabel={true}
            hours={this.state.hours}
            disabled={flag == 'offset'}
            // disabled={project["status"] == "Approved" || !timesheetPer.addTimeEntry.cando}
            mins={this.state.mins}
            error={this.state.errorMessage}
            errorMessage={this.state.errorMessage}
            styles={{ flex: 1, margin: 0 }}
          />
            :
            <DefaultTextField
              defaultProps={{
                type: "text",
                id: "numberValue",
                placeholder: "Enter value between 0 to 100",
                value: this.state.offsetHours,
                autoComplete: "off",
                onChange: e => this.handleOffsetChange(e),
                endAdornment: "%",
              }}
            />
          }
        </div>

        <ButtonActionsCnt
          cancelAction={closeAction}
          successAction={this.handleSaveTimeEntry}
          successBtnText={<FormattedMessage id="timesheet.time-entry.creation-dialog.save-button.label" defaultMessage="Save Changes" />}
          cancelBtnText={<FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />}
          btnType="success"
          btnQuery={this.state.btnQuery}
          successBtnProps={{
            // disabled: disableSaveButton || totalMin > 1440,
          }}
        />

      </CustomDialog>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    profile: state.profile.data,
  };
};

export default compose(
  withSnackbar,
  injectIntl,
  withStyles(timesheetStyles, {
    withTheme: true,
  }),
  connect(mapStateToProps, {
    addTeamTaskDuration,
    addUserEffort,
    getTeamDateTaskEffort,
    SaveTotalTime
  })
)(AddCustomTotalTime);
