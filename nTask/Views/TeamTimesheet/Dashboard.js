import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import moment from "moment";
import TimeSheet from "./TimeSheet";
import ToggleButton from "@material-ui/lab/ToggleButton";
import LeftArrow from "@material-ui/icons/ChevronLeft";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import { withSnackbar } from "notistack";
import timesheetStyles from "./styles";
import Typography from "@material-ui/core/Typography";
import PendingTimesheet from "./PendingTimesheet";
import PrintIcon from "@material-ui/icons/Print";
import CustomIconButton from "../../components/Buttons/CustomIconButton";
import TimesheetEmailDialog from "../../components/Dialog/TimesheetEmailDialog";
import { canDo } from "../WorkspacePermission/Permissions";
import GetUserProjects from "../../helper/getUserProjects";
import { FormattedMessage } from "react-intl";
import TimeSheetLoader from "../../components/ContentLoader/TimeSheet";
import { FetchWorkspaceInfo } from "../../redux/actions/workspace";

const timesheetPer = {
  "label": "Timesheet",
  "description": "Establish accountability of project members through enforcing weekly timesheet entries",
  "cando": true,
  "addTask": {
    "label": "Add Task",
    "cando": true
  },
  "addTimeEntry": {
    "label": "Add time entry",
    "cando": true
  },
  "deleteTimeEntry": {
    "label": "Delete time entry",
    "cando": true
  },
  "deleteTimesheet": {
    "label": "Delete timesheet",
    "cando": true
  },
  "submitTimesheet": {
    "label": "Submit timesheet for approval",
    "cando": true
  },
  "resubmitTimeSheet": {
    "label": "Resubmit timesheet for approval",
    "cando": true
  },
  "sendTimesheet": {
    "label": "Send timesheet",
    "cando": true
  },
  "addNotes": {
    "label": "Add notes",
    "cando": true
  },
  "acceptTimesheet": {
    "label": "Accept timesheet",
    "cando": true
  },
  "rejectTimesheet": {
    "label": "Reject timesheet",
    "cando": true
  }
}
class TeamTimeSheetDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      alignment: "left",
      activeView: 0,
      activeViewPendingList: 0,
      openTimesheetEmail: false,
      timesheetSelectedWeekDate: moment().startOf("isoWeek"),
      isLoading: true
    };
    this.renderView = this.renderView.bind(this);
    this.TimesheetEmailDialogClose = this.TimesheetEmailDialogClose.bind(this);
    this.TimesheetEmailDialogOpen = this.TimesheetEmailDialogOpen.bind(this);
  }

  renderView(view, selectedTab) {
    this.setState({
      activeView: view,
      alignment: selectedTab,
      activeViewPendingList: 0,
      // timesheetSelectedWeekDate: moment().startOf("isoWeek"),
    });
  }
  handlePendingListView = (type) => {
    this.setState({ activeViewPendingList: type });
  };
  TimesheetEmailDialogClose() {
    this.setState({ openTimesheetEmail: false });
  }
  TimesheetEmailDialogOpen() {
    this.setState({ openTimesheetEmail: true });
  }
  setTimesheetSelectedWeek = (timesheetSelectedWeekDate) => {
    this.setState({ timesheetSelectedWeekDate });
  };

  componentWillUnmount() {
    if (!window.location.pathname.includes("/reports")) {
      this.props.FetchWorkspaceInfo(
        "",
        () => { },
        () => { },
        null
      );
    }
  }

  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  render() {
    const { classes, theme, timesheetState } = this.props;

    const {
      alignment,
      activeView,
      activeViewPendingList,
      openTimesheetEmail,
      timesheetSelectedWeekDate,
    } = this.state;
    return (
    <div className={classes.timeSheetDashboard}>

        <TimesheetEmailDialog
          date={timesheetSelectedWeekDate}
          open={openTimesheetEmail}
          closeAction={this.TimesheetEmailDialogClose}
        />

        <div
          className={`${classes.timeSheetDashboardHeader} flex_center_space_between_row`}
        >
          <div className="flex_center_start_row">
            {activeViewPendingList !== 0 ? (
              <>
                <LeftArrow
                  onClick={() => this.handlePendingListView(0)}
                  className={classes.backArrowIcon}
                />
                {/* <Typography variant="h1" classes={{ h1: classes.timeSheetHeadingDimmed }}>
                    {
                      `Timesheet: `
                    }
                  </Typography> */}
                <Typography
                  variant="h1"
                  classes={{ h1: classes.timeSheetHeading }}
                >
                  {activeViewPendingList}
                </Typography>
              </>
            ) : (null
              // <Typography
              //   variant="h1"
              //   classes={{ h1: classes.timeSheetHeading }}
              // >
              //   {`Timesheet`}
              // </Typography>
            )}
            {/* #cbcbcb */}
            {activeViewPendingList === 0 ? (
              <div className={classes.toggleContainer}>
                <ToggleButtonGroup
                  value={alignment}
                  exclusive
                  onChange={this.handleAlignment}
                  classes={{
                    root: classes.toggleBtnGroup,
                    selected: classes.toggleButtonSelected,
                    groupedHorizontal: classes.groupedHorizontal
                  }}
                >
                  <ToggleButton
                    value="left"
                    onClick={(event) => this.renderView(0, "left")}
                    classes={{
                      root: classes.toggleButton,
                      selected: classes.toggleButtonSelected,
                    }}
                  >
                   <FormattedMessage id="timesheet.time-entry.label" defaultMessage="Time Entry"/> 
                  </ToggleButton>
                  {timesheetPer.acceptTimesheet.cando || timesheetPer.rejectTimesheet.cando  ||
                    GetUserProjects(
                      this.props.projectState.data,
                      this.props.profileState.data.userId
                    ).length > 0 ? (
                    <ToggleButton
                      value="right"
                      onClick={(event) => this.renderView(1, "right")}
                      style={{
                        borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
                      }}
                      classes={{
                        root: classes.toggleButton,
                        selected: classes.toggleButtonSelected,
                      }}
                    >
                    {/* <FormattedMessage id="timesheet.pending-approved.label" defaultMessage="Pending/Approved"/>    */}
                      {/* <FormattedMessage id="task.common.status.dropdown.in-review" defaultMessage="In Review"/> */}
                      In Review/Approved
                    </ToggleButton>
                  ):null}
                </ToggleButtonGroup>
              </div>
            ) : null}
          </div>
        </div>
        {activeView == 0 ? (
          <TimeSheet
            date={timesheetSelectedWeekDate}
            setTimesheetSelectedWeek={this.setTimesheetSelectedWeek}
            timesheetPer={timesheetPer}
            showLoader={this.showLoader}
            hideLoader={this.hideLoader}
            showSnackBar={this.showSnackBar}
          />
        ) : activeView == 1 ? (
          <PendingTimesheet
          date={timesheetSelectedWeekDate}
          setTimesheetSelectedWeek={this.setTimesheetSelectedWeek}
            activeView={activeViewPendingList}
            handleChangeView={this.handlePendingListView}
            timesheetPer={timesheetPer}
            showLoader={this.showLoader}
            hideLoader={this.hideLoader}
            showSnackBar={this.showSnackBar}
          />
        ) : null}

      </div>
        )
  }
}

const mapStateToProps = (state) => {
  return {
    timesheetState: state.timesheet,
    projectState: state.projects,
    profileState: state.profile,
    timesheetPer: state.workspacePermissions.data.timesheet,
  };
};

export default compose(
  withSnackbar,
  withStyles(timesheetStyles, { withTheme: true }),
  connect(mapStateToProps, {
    FetchWorkspaceInfo
  })
)(TeamTimeSheetDashboard);
