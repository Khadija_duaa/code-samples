import React, { Component } from 'react';
import moment from "moment";
import find from "lodash/find";

//Takes First day of week and return corresponding week dates
export const getWeekDates = (startDate) => {
  let endOfWeek = startDate.clone().endOf("isoWeek");
  let days = [];
  let day = startDate;
  while (day <= endOfWeek) {
    days.push(day.toDate());
    day = day.clone().add(1, "d");
  }
  return days;
}

//Takes an array of timesheet data object, retrieve timestamps and return total duration
export const addDuration = timeStamp => {
  let durations = "";
  if (timeStamp.length > 0) {
    timeStamp.forEach(stamp => {
      durations = moment.duration(stamp.duration).add(durations);
    });
    const totalTime = `${durations.hours() +
      (durations.days() * 24 +
        durations.months() * 30 * 24)}:${durations.minutes()}`;

    return totalTime;
  } else return "0:0";
};

// Takes an array of current week dates and week timesheet data object and returns array of task/row total durations
export const calRowsTotalTime = (weekDays, data) => {
  let totalTime = [];
  data.map((obj) => {
    let combinedColumns = [];
    weekDays.map(day => {
      const currentDate = moment(day).format("M/D/YYYY");
      const columnObject = find(obj.timestamp, {
        date: currentDate
      });
      if (columnObject) combinedColumns.push(columnObject);
    });
    totalTime.push(addDuration(combinedColumns));
  });
  return totalTime;
};

//Takes array of week dates and return formated array of date for time sheet header
export const generateWeekDates = (weekDays, classes, dayName = true) => {
  return weekDays.map((date, i) => {
    return (
      <div key={i}>
        {dayName && <div className={classes.headerDay}>
          {moment(date).format("ddd")}
        </div>}
        <div className={classes.headerDate}>
          {moment(date).format("DD MMM")}
        </div>
      </div>
    );
  });
};

//Takes start of current week and returns start and end date of the week as a range for week navigation
export const getDate = (date) => {
  const weekStart = date.clone().format("MMM DD");
  const weekEnd = date
    .clone()
    .endOf("isoWeek")
    .format("MMM DD");
  const year = date.clone().format("YYYY");
  const finalDateRange = `${weekStart} - ${weekEnd}, ${year}`;
  return finalDateRange;
};

// Takes array of current week dates and week timesheet data object and returns array of task/column total durations
export const calColsTotalTime = (weekDays, data) => {
  let totalTime = [];
  weekDays.map(date => {
    let combinedColumns = [];
    data.map((obj) => {
      const columnObject = find(obj.timestamp, {
        date: moment(date).format("M/D/YYYY")
      });
      if (columnObject) combinedColumns.push(columnObject);
    });
    totalTime.push(addDuration(combinedColumns));
  });
  return totalTime;
};

// Takes array of durations and return sum of all durations
export const calTotalTime = (rowsTime) => {
  let durations = "0:0";
  if (rowsTime.length) {
    rowsTime.map(time => {
      durations = moment.duration(time).add(durations);
    });
    const rowsTotalTime = `${durations.hours() +
      (durations.days() * 24 +
        durations.months() * 30 * 24)}:${durations.minutes()}`;
    return `${rowsTotalTime}`;
  }
  return durations;
};