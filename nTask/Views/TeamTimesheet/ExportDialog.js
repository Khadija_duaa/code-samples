import React, { useEffect, useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import moment from "moment";
import { FormattedMessage } from "react-intl";
import ActionConfirmation from "../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import timesheetStyles from "./styles";
import CustomDatePicker from "../../components/DatePicker/DatePicker/DatePicker";
import InputLabel from "@material-ui/core/InputLabel";
import Typography from "@material-ui/core/Typography";

function ExportDialog({
  classes,
  open,
  handleClose,
  fromDate,
  toDate,
  handleDateChange,
  successAction,
  btnQuery,
}) {
  const [startDate, setStartDate] = useState(fromDate);
  const [endDate, setEndDate] = useState(toDate);

  useEffect(() => {
    setStartDate(fromDate);
    setEndDate(toDate);
  }, [fromDate, toDate]);


  return (
    <ActionConfirmation
      open={open}
      cancelBtnText={
        <FormattedMessage
          id="common.action.cancel.label"
          defaultMessage="Cancel"
        />
      }
      successBtnText={'Export'}
      alignment="left"
      headingText={'Simple Export to Excel'}
      iconType="none"
      msgText=""
      successBtnType="success"
      closeAction={handleClose}
      successAction={successAction}
      btnQuery={btnQuery}
      btnProps={{
        disabled: startDate == null || endDate == null
      }}
    >

      <Typography variant="h4" className={classes.headingText}>
        Select date range to export
      </Typography>
      <div className={classes.multipleDropdownCnt}>
        <div
          className={classes.datePickerWrapper}
          style={{
            marginRight: 20,
          }}>
          <InputLabel classes={{ root: classes.inputLable }}>
            From
          </InputLabel>
          <CustomDatePicker
            date={startDate && moment(startDate)}
            dateFormat="MMM DD, YYYY"
            timeInput={false}
            onSelect={(date) => handleDateChange(date, 'from')}
            label={null}
            icon={true}
            PopperProps={{ size: null }}
            filterDate={moment()}
            btnProps={{ className: classes.datePickerCustomStyle }}
            deleteIcon={true}
            placeholder={"Select Date"}
            containerProps={{ style: { alignItems: "center" } }}
            datePickerProps={endDate && {
              maxDate: new Date(endDate) || '',
            }}
            deleteIconWrapperProps={{
              style: {
                marginLeft: 'auto',
              },
            }}
            deleteIconProps={{
              style: {
                marginRight: 10,
              },
            }}
          />
        </div>
        <div className={classes.datePickerWrapper}>
          <InputLabel classes={{ root: classes.inputLable }}>
            To
          </InputLabel>
          <CustomDatePicker
            date={endDate && moment(endDate)}
            dateFormat="MMM DD, YYYY"
            timeInput={false}
            onSelect={(date) => handleDateChange(date, 'to')}
            label={null}
            icon={true}
            PopperProps={{ size: null }}
            filterDate={moment()}
            deleteIcon={true}
            btnProps={{ className: classes.datePickerCustomStyle }}
            placeholder={"Select Date"}
            datePickerProps={startDate && {
              maxDate: new Date(moment(startDate).add(1, "month")) || '',
              minDate: new Date(startDate) || '',
            }}
            deleteIconWrapperProps={{
              style: {
                marginLeft: 'auto',
              },
            }}
            deleteIconProps={{
              style: {
                marginRight: 10,
              },
            }}
          />
        </div>
      </div>
    </ActionConfirmation>
  );
}



export default compose(
  withStyles(timesheetStyles, { withTheme: true }),
)(ExportDialog);
