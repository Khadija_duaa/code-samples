import React, { Component, useMemo } from "react";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
import { connect } from "react-redux";
import moment from "moment";
import findIndex from "lodash/findIndex";
import sortBy from "lodash/sortBy";
import cloneDeep from "lodash/cloneDeep";
import CloseIcon from "@material-ui/icons/Close";
import IconButton from "../../components/Buttons/IconButton";
import LeftArrow from "@material-ui/icons/ArrowLeft";
import RightArrow from "@material-ui/icons/ArrowRight";
import { withStyles } from "@material-ui/core/styles";
import timesheetStyles from "./styles";
import DefaultTextField from "../../components/Form/TextField";
import CustomIconButton from "../../components/Buttons/CustomIconButton";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ChevronRight";
import ExpandLessIcon from "@material-ui/icons/ExpandMore";
import CustomButton from "../../components/Buttons/CustomButton";
import LockIcon from "@material-ui/icons/LockOpen";
import ActionConfirmation from "../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import TaskListMenu from "../../components/Menu/TaskMenus/TaskListMenu2";
import EmptyState from "../../components/EmptyStates/EmptyState";
import CustomTooltip from "../../components/Tooltip/Tooltip";
import AddIcon from "@material-ui/icons/Add";
import AddDurationDialog from "./AddDurationDialog";
import { calculateContentHeight, setCaretPosition, maskInputValue } from "../../utils/common";
import { addUserEffort } from "../../redux/actions/tasks";
import CustomAvatar from "../../components/Avatar/Avatar";
import classNames from "classnames";

import {
  getWeekDates,
  calRowsTotalTime,
  generateWeekDates,
  getDate,
  calColsTotalTime,
  calTotalTime,
} from "./timingFunctions";
import TaskIcon from "../../components/Icons/TaskIcon";
import helper from "../../helper";
import ProjectsIcon from "../../components/Icons/ProjectIcon";
import MessageIcon from "@material-ui/icons/Message";
import SvgIcon from "@material-ui/core/SvgIcon";
import {
  addTeamTaskDuration,
  submitTeamTimesheet,
  removeTaskLogInTeam,
  getTimesheetTasks,
  selectTimesheetWorkspaces,
  getTeamTimesheetWeekDurations,
  addNewTaskInTeamTimesheet,
} from "../../redux/actions/timesheet";
import { updateTasksTimesheetStatus } from "../../redux/actions/tasks";
import { timesheetsHelpText } from "../../components/Tooltip/helptext";
import DeleteConfirmDialog from "../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import TimerIconWhite from "../../assets/images/timer_white.svg";
import { FormattedMessage } from "react-intl";
import CustomMultiSelectDropdown from "../../components/Dropdown/CustomMultiSelectWithSearchBarDropdown/Dropdown";
import { generateWorkspaceData } from "../../helper/generateTasksOverviewDropdownData";
import ImportExport from "@material-ui/icons/ImportExport";
import apiInstance from "../../redux/instance";
import fileDownload from "js-file-download";
import TimesheetComments from "../../components/TimesheetComments/TimesheetComments.view";
import NonBillableIcon from "../../components/Icons/NonBillable";
import BillableIcon from "../../components/Icons/Billable";
import CustomDatePicker from "../../components/DatePicker/DatePicker/ListViewDatePicker";
import Loader from "../../components/Loader/Loader";
import StaticDatePicker from "../../components/DatePicker/StaticDatePicker";
import { inDateRange } from "../../helper/dates/dates";
import ExportDialog from "./ExportDialog";
import { GlobalTimeCustom } from "../../helper/config.helper";

const timesheetPer = {
  label: "Timesheet",
  description:
    "Establish accountability of project members through enforcing weekly timesheet entries",
  cando: true,
  addTask: {
    label: "Add Task",
    cando: true,
  },
  addTimeEntry: {
    label: "Add time entry",
    cando: true,
  },
  deleteTimeEntry: {
    label: "Delete time entry",
    cando: true,
  },
  deleteTimesheet: {
    label: "Delete timesheet",
    cando: true,
  },
  submitTimesheet: {
    label: "Submit timesheet for approval",
    cando: true,
  },
  resubmitTimeSheet: {
    label: "Resubmit timesheet for approval",
    cando: true,
  },
  sendTimesheet: {
    label: "Send timesheet",
    cando: true,
  },
  addNotes: {
    label: "Add notes",
    cando: true,
  },
  acceptTimesheet: {
    label: "Accept timesheet",
    cando: true,
  },
  rejectTimesheet: {
    label: "Reject timesheet",
    cando: true,
  },
};
class Timesheet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      weekDates: [],
      tooltipRights: false,
      approvalDialog: false,
      timesheetData: {},
      taskListTrigger: null,
      selectedProjectId: null,
      submitApprovalBtnQuery: "",
      deleteModal: false,
      openModal: false,
      exportDialog: false,
      fromDate: this.props.date,
      toDate: this.props.date,
      deleteBtnQuery: "",
      selectedProjecIdDelete: null,
      selectedTaskIdDelete: null,
      selectedWeekDateDelete: null,
      isLoading: true,
      addDurationDialogState: {
        open: false,
        hours: "",
        mins: "",
        effortObj: "",
        timeStamp: "",
        project: {},
        workspaceConfig: {},
      },
      totalTimeLogged: "",
      selectedWorkspaces: [],
      expanded: [],
    };
    this.handleInput = this.handleInput.bind(this);
    this.handleThisWeek = this.handleThisWeek.bind(this);
    this.approvalDialogClose = this.approvalDialogClose.bind(this);
    this.approvalDialogOpen = this.approvalDialogOpen.bind(this);
  }

  componentDidMount() {
    const { date } = this.props;
    // this.loadWeekData("");
    if (date.toString() === moment().startOf("isoWeek").toString()) {
      this.showLoader();
      this.loadWeekData("");
    } else {
      this.showLoader();
      this.loadWeekData(`&currentDate=${date.format("YYYY-MM-DDTHH:mm:ss")}`)
    }
    this.setState({
      fromDate: date.format("YYYY-MM-DDTHH:mm:ss"),
      toDate: date.clone().endOf("isoWeek").format("YYYY-MM-DDTHH:mm:ss"),
    })
  }

  componentDidUpdate(prevProps) {
    const { date, timesheetState } = this.props;
    if (prevProps.profileState.data.loggedInTeam !== this.props.profileState.data.loggedInTeam) {
      this.showLoader();
      this.loadWeekData("");
    } else if (prevProps.date.toString() !== date.toString()) {
      if (
        date.toString() ===
        moment()
          .startOf("isoWeek")
          .toString()
      ) {
        this.showLoader();
        this.loadWeekData("");
      } else {
        this.setState({
          fromDate: this.props.date.format("YYYY-MM-DDTHH:mm:ss"),
          toDate: this.props.date.clone().endOf("isoWeek").format("YYYY-MM-DDTHH:mm:ss"),
        })
        this.showLoader();
        this.loadWeekData(`&currentDate=${date.format("YYYY-MM-DDTHH:mm:ss")}`);
      }
    } else if (
      JSON.stringify(timesheetState.timeEntry) !==
      JSON.stringify(prevProps.timesheetState.timeEntry)
    ) {
      this.setState({ timesheetData: cloneDeep(timesheetState.timeEntry) });
    }
  }

  setTaskListTrigger = elem => {
    this.setState({ taskListTrigger: elem });
  };
  handleExpand = id => {
    const { expanded } = this.state;
    const isExist = expanded.includes(id);
    if (isExist) {
      this.setState({ expanded: expanded.filter(w => w !== id) });
    } else {
      this.setState({ expanded: [...this.state.expanded, id] });
    }
  };

  handleInput(event, oldValue, projectId, taskId, day) {
    const inputRef = event.target;
    let cursorPosition = event.target.selectionStart;
    let newValue = event.target.value;
    if (oldValue === "24:00" && (cursorPosition == 4 || cursorPosition == 5)) {
      oldValue = "24:00";
      newValue = "24:0";
    }

    let inputData = maskInputValue(newValue, oldValue, cursorPosition);
    let value = inputData.value;
    cursorPosition = inputData.cursorPosition;

    let prevData = { ...this.state.timesheetData };
    const projectIndex = findIndex(prevData["projectTimesheetVM"], {
      projectId: projectId,
    });
    const taskIndex = findIndex(prevData["projectTimesheetVM"][projectIndex]["taskEffortVM"], {
      taskId: taskId,
    });
    const fieldIndex = findIndex(
      prevData["projectTimesheetVM"][projectIndex]["taskEffortVM"][taskIndex]["timestamp"],
      { date: day }
    );
    prevData["projectTimesheetVM"][projectIndex]["taskEffortVM"][taskIndex]["timestamp"][
      fieldIndex
    ].duration = value;

    this.setState({ timesheetData: prevData }, () => {
      setCaretPosition(inputRef, cursorPosition);
    });
  }

  onKeyPress = (event, projectId, taskId, day) => {
    if (event.key === "Enter") {
      const value = event.target.value;
      this.updateFieldValue(value, projectId, taskId, day);
    }
  };
  handleBlur = (projectId, taskId, day) => {
    let data = this.state.timesheetData;
    const projectIndex = findIndex(data["projectTimesheetVM"], {
      projectId: projectId,
    });
    const taskIndex = findIndex(data["projectTimesheetVM"][projectIndex]["taskEffortVM"], {
      taskId: taskId,
    });
    const fieldIndex = findIndex(
      data["projectTimesheetVM"][projectIndex]["taskEffortVM"][taskIndex]["timestamp"],
      { date: day }
    );
    const value =
      data["projectTimesheetVM"][projectIndex]["taskEffortVM"][taskIndex]["timestamp"][fieldIndex]
        .duration;
    this.updateFieldValue(value, projectId, taskId, day);
  };
  updateFieldValue = (value, projectId, taskId, day) => {
    const { timeEntry } = this.props.timesheetState;
    let prevData = { ...timeEntry };

    const projectIndex = findIndex(prevData["projectTimesheetVM"], {
      projectId: projectId,
    });
    const taskIndex = findIndex(prevData["projectTimesheetVM"][projectIndex]["taskEffortVM"], {
      taskId: taskId,
    });
    const fieldIndex = findIndex(
      prevData["projectTimesheetVM"][projectIndex]["taskEffortVM"][taskIndex]["timestamp"],
      { date: day }
    );
    const prevValue =
      prevData["projectTimesheetVM"][projectIndex]["taskEffortVM"][taskIndex]["timestamp"][
        fieldIndex
      ].duration;

    if (prevValue !== value) {
      const values = value.split(":");
      const hours = values[0] * 1;
      const mins = values[1] * 1;
      const timeStamp = moment(day).format("YYYY-MM-DDTHH:mm:ss");

      const data = {
        effortHours: hours,
        effortMinutes: mins,
        taskId,
        projectId,
        weekDate: timeStamp,
      };

      this.props.addTeamTaskDuration(
        data,
        response => {
          // this.setState({ timesheetData: cloneDeep(this.props.timesheetState.timeEntry) });
          this.props.addUserEffort(response.data);
        },
        error => {
          if (error.status === 500) {
            // self.showSnackBar('Server throws error','error');
          }
        }
      );
    }
  };

  renderCurrentDate = (stamp, i) => {
    const date = this.props.date.clone().add(i, "days");
    const formatedDate = date.format("MM/DD/YYYY");
    if (moment(formatedDate).isSame(stamp.date)) {
      return stamp.duration ? stamp.duration : "00:00";
    }
  };

  handleThisWeek() {
    const { date } = this.props;
    const currentDate = moment().startOf("isoWeek");
    if (date.toString() !== currentDate.toString()) {
      this.props.setTimesheetSelectedWeek(currentDate);
    }
  }

  loadWeekData = parms => {
    this.props.getTeamTimesheetWeekDurations(parms, (error, response) => {
      if (error) {
        this.hideLoader();
      } else {
        if (response) {
          this.props.getTimesheetTasks();
          const selectedWorkspaceData = this.workspacesDropdownData().filter(
            wd =>
              response.data.selectedWorkspaces &&
              response.data.selectedWorkspaces.includes(wd.obj.teamId)
          );
          this.setState({
            timesheetData: cloneDeep(this.props.timesheetState.timeEntry),
            selectedWorkspaces: selectedWorkspaceData,
          });
          this.hideLoader();
        } else {
          this.hideLoader();
        }
      }
    });
  };
  handleNextWeek = () => {
    const { date } = this.props;
    const nextWeek = date.clone().add(7, "days");
    this.props.setTimesheetSelectedWeek(nextWeek);
  };
  handlePrevWeek = () => {
    const { date } = this.props;
    const prevWeek = date.clone().subtract(7, "days");
    this.props.setTimesheetSelectedWeek(prevWeek);
  };
  handleSelectDate = (date) => {
    if (date) {
      const startDate = moment(date).startOf('isoWeek');
      this.props.setTimesheetSelectedWeek(startDate);
    }
  }
  approvalDialogOpen(event, selectedProjectId, totalTime, ws) {
    event.stopPropagation();
    this.setState({
      approvalDialog: true,
      selectedProjectId,
      totalTimeLogged: totalTime,
      submitTimesheetWorkspace: ws,
    });
  }

  approvalDialogClose() {
    this.setState({ approvalDialog: false });
  }

  handleSubmitForApproval = () => {
    const {
      selectedProjectId,
      timesheetData,
      totalTimeLogged,
      submitTimesheetWorkspace,
    } = this.state;
    const { date } = this.props;
    let data = {
      projectId: selectedProjectId,
      other: selectedProjectId ? false : true,
      workspaceId: submitTimesheetWorkspace,
      startDate: date.format("YYYY-MM-DDTHH:mm:ss"),
      endDate: date
        .clone()
        .add(6, "days")
        .format("YYYY-MM-DDTHH:mm:ss"),
      weeklyTime: totalTimeLogged,
    };

    if (!selectedProjectId) {
      let otherTasks = [];
      const projects = timesheetData["projectTimesheetVM"] || [];
      const projectIndex = findIndex(projects, {
        projectId: selectedProjectId,
        workspaceId: submitTimesheetWorkspace,
      });
      const tasks = projects[projectIndex]["taskEffortVM"] || [];
      tasks.map(obj => {
        otherTasks.push(obj.taskId);
      });
      data.OtherTaskIds = otherTasks;
    }
    this.setState({ submitApprovalBtnQuery: "progress" });
    this.props.submitTeamTimesheet(
      data,
      response => {
        this.setState({
          submitApprovalBtnQuery: "",
          timesheetData: cloneDeep(this.props.timesheetState.timeEntry),
          approvalDialog: false,
          submitTimesheetWorkspace: "",
        });
      },
      error => {
        this.setState({
          submitApprovalBtnQuery: "",
          approvalDialog: false,
          submitTimesheetWorkspace: "",
        });
        const err = error.data && error.data.message;
        const message =
          err || "Oops! There seems to be an issue, please contact support@ntaskmanager.com";
        this.props.showSnackBar(message, "error");
      }
    );
    this.props.getTeamTimesheetWeekDurations(
      `&currentDate=${this.props.date.format("YYYY-MM-DDTHH:mm:ss")}`,
      (error, response) => {
        if (error) {
          this.props.showSnackBar(
            error.data?.message,
            "error"
          );
        } else {
          if (response) {
            //this.setState({ timesheetData: cloneDeep(this.props.timesheetState.timeEntry) })
          }
        }
      }
    );
  };

  handleAddTask = (taskId, projectId, workspaceId) => {
    const { date } = this.props;

    const data = {
      effortHours: 0,
      effortMinutes: 0,
      taskId: taskId,
      projectId: projectId,
      date: date.format("YYYY-MM-DDTHH:mm:ss"),
      weekStartDate: date.format("YYYY-MM-DDTHH:mm:ss"),
      workspaceId,
    };
    const suppressInReviewProjectTask = this.props.timesheetState.timeEntry.projectTimesheetVM.some(
      t => t.projectId == projectId && t.workspaceId == workspaceId && t.status == "In Review"
    );
    const suppressInReviewUnSortedTask = this.props.timesheetState.timeEntry.projectTimesheetVM.some(
      t => !t.projectId && t.workspaceId == workspaceId && t.status == "In Review"
    );
    const suppressApprovedProjectTask = this.props.timesheetState.timeEntry.projectTimesheetVM.some(
      t => t.projectId == projectId && t.workspaceId == workspaceId && t.status == "Approved"
    );
    const suppressApprovedUnSortedTask = this.props.timesheetState.timeEntry.projectTimesheetVM.some(
      t => !t.projectId && t.workspaceId == workspaceId && t.status == "In Review"
    );

    if (suppressInReviewProjectTask) {
      this.props.showSnackBar(
        "Oops, cannot add task! Timesheet is already in review for current week for this project.",
        "error"
      );
      return;
    }
    if (suppressInReviewUnSortedTask && !projectId) {
      this.props.showSnackBar(
        "Oops, cannot add task! Timesheet is already in review for current week for tasks not assigned to projects.",
        "error"
      );
      return;
    }
    if (suppressApprovedProjectTask) {
      this.props.showSnackBar(
        "Oops, cannot add task! Timesheet is already approved for current week for this project.",
        "error"
      );
      return;
    }
    if (suppressApprovedUnSortedTask && !projectId) {
      this.props.showSnackBar(
        "Oops, cannot add task! Timesheet is already approved for current week for tasks not assigned to projects.",
        "error"
      );
      return;
    }

    this.props.addNewTaskInTeamTimesheet(data, (error, response) => {
      if (error) {
        this.props.showSnackBar(
          error.data.message,
          "error"
        );
      } else {
        if (response) {
          const resData = response.data;
          // Approved -> In Progress
          if (resData.status) {
            const statusProjectId = resData.status.projectId;
            const timesheetData = this.props.timesheetState.timeEntry;
            let taskIds = [];
            const projects = timesheetData["projectTimesheetVM"] || [];
            const projectIndex = findIndex(projects, {
              projectId: statusProjectId,
            });
            const tasks = projects[projectIndex]["taskEffortVM"] || [];
            tasks.map(obj => {
              taskIds.push(obj.taskId);
            });
            // this.props.updateTasksTimesheetStatus(taskIds, "In Progress");
          }
          this.setState({
            timesheetData: cloneDeep(this.props.timesheetState.timeEntry),
            approvalDialog: false,
          });
        }
      }
    });
  };
  getAddedTasks = ws => {
    const { timesheetData } = this.state;
    const projects = timesheetData["projectTimesheetVM"] || [];
    let timesheetTasks = [];
    projects.map(project => {
      if (project.workspaceId == ws) {
        const tasks = project["taskEffortVM"] || [];
        tasks.map(task => {
          timesheetTasks.push({
            projectId: project["projectId"],
            taskId: task["taskId"],
            workspaceId: project.workspaceId,
          });
        });
      }
    });
    return timesheetTasks;
  };
  openTaskListMenu = () => {
    this.state.taskListTrigger.click();
  };
  //Function that open task duration popup
  handleAddTaskDuration = (project, effortObj, timeStamp) => {
    // workspaceId
    // get workspace data form workspaceId 


    // const thisWorkspace = workspaces.find(w => w.teamId === currentTask.teamId) || {};
    this.setState({
      addDurationDialogState: {
        open: true,
        effortObj,
        timeStamp,
        project,
        workspaceConfig: GlobalTimeCustom(project.workspaceId),
      },
    });
  };
  //Function that open task duration popup
  closeAddTaskDuration = () => {
    this.setState({
      addDurationDialogState: {
        open: false,
        effortObj: {},
        timeStamp: {},
        project: {},
        workspaceConfig: {},
      },
    });
  };
  removeTaskFromSheet = (projectId, taskId, weekDate) => {
    /** function calls when user clicks on close Icon */
    this.setState({
      deleteModal: true,
      openModal: true,
      selectedProjecIdDelete: projectId,
      selectedTaskIdDelete: taskId,
      selectedWeekDateDelete: weekDate,
    });
  };

  handleDialogClose = e => {
    /** function that closes the modal */
    if (e) e.stopPropagation();
    this.setState({
      openModal: false,
      deleteModal: false,
    });
  };

  handleDelete = e => {
    /** function when click on delete button in modal */
    e.stopPropagation();
    const { selectedProjecIdDelete, selectedTaskIdDelete, selectedWeekDateDelete } = this.state;
    this.setState({ openModal: false, deleteModal: false });
    this.props.removeTaskLogInTeam(
      selectedProjecIdDelete,
      selectedTaskIdDelete,
      selectedWeekDateDelete.startOf("week").format("MM/DD/YYYY HH:mm:ss a"),
      () => {
        this.loadWeekData(`&currentDate=${this.props.date.format("YYYY-MM-DDTHH:mm:ss")}`);
      },
      error => {
        this.props.showSnackBar(
          error.data.message,
          "error"
        );
      }
    );
  };
  showTranslatedStatus = status => {
    let intl = { id: "", message: "" };
    switch (status) {
      case "In Progress":
        intl.id = "task.common.status.dropdown.in-progress";
        intl.message = "In Progress";
        break;
      case "In Review":
        intl.id = "task.common.status.dropdown.in-review";
        intl.message = "In Review";
        break;
      case "Rejected":
        intl.id = "timesheet.status.reject";
        intl.message = "Rejected";
        break;

      case "Approved":
        intl.id = "timesheet.status.approve";
        intl.message = "Approved";
        break;
      case "In Progress - Withdrawn Approval":
        intl.id = "timesheet.status.withDraw";
        intl.message = "In Progress - Withdrawn Approval";
        break;
      default:
        intl.id = "";
        intl.message = status;
        break;
    }
    return <FormattedMessage id={intl.id} defaultMessage={intl.message} />;
  };
  // Generate workspace dropdown data
  workspacesDropdownData = () => {
    const { workspace } = this.props.profileState.data;
    return generateWorkspaceData(workspace);
  };
  onWorkspaceSelect = workspace => {
    this.setState({ selectedWorkspaces: workspace });
    const workspaceIds = workspace.map(w => w.obj.teamId);
    this.props.selectTimesheetWorkspaces({ workspaceIds: workspaceIds }, () => {
      this.props.getTimesheetTasks();
      this.loadWeekData(`&currentDate=${this.props.date.format("YYYY-MM-DDTHH:mm:ss")}`);
    });
  };
  getWorkspace = workspaceId => {
    const { profileState } = this.props;

    return profileState.data.workspace.find(w => {
      return w.teamId == workspaceId;
    });
  };
  showLoader = () => {
    this.setState({ isLoading: true });
  };
  hideLoader = () => {
    this.setState({ isLoading: false });
  };
  handleExportExcel = () => {
    this.setState({ exportBtnQuery: "progress" });

    const fileName = `timesheet.xlsx`;
    apiInstance()
      .get(
        BASE_URL +
        `api/TeamTimeSheet/GetTeamTimeSheetDownload?status=Approved&timeSheetType=Weekly&currentDate=${moment(
          this.props.date
        ).format("YYYY-MM-DDTHH:mm:ss")}`,
        {
          responseType: "blob",
        }
      )
      .then(res => {
        this.setState({ exportBtnQuery: "" });
        fileDownload(res.data, fileName);
        // showMessage(`File downloaded successfully.`, "success");
      });
    return;
  };


  // ******* simple export function starts here *******

  // It is called to close Export dialog
  handleExportOpen = () => {
    this.setState({ exportDialog: true });
    return;
  };
  // It is called to close Export dialog
  handleExportClose = event => {
    this.setState({ exportDialog: false });
  };

  handleSelectExportDate = (date, type) => {
    const { fromDate, toDate } = this.state
    if (type == 'from') {
      if (date) {
        const inRangeCheck = inDateRange(date,
          moment(toDate).add(-1, "month").format('l'),
          moment(toDate).format('l'));
        if (inRangeCheck || toDate == null) {
          this.setState({ fromDate: moment(date).format("YYYY-MM-DD") });
        }
        else {
          this.setState({ fromDate: moment(date).format("YYYY-MM-DD"), toDate: moment(date).add(1, "month").format("YYYY-MM-DD") });
        }
      } else {
        this.setState({ fromDate: null });
      }
    }
    if (type == 'to') {
      if (date) {
        this.setState({ toDate: moment(date).format("YYYY-MM-DD") });
      } else {
        this.setState({ toDate: null });
      }
    }
  }
  exportAfterConfirmation = () => {
    this.setState({ exportBtnQuery: "progress" });
    const { fromDate, toDate } = this.state
    const fileName = `timesheet.xlsx`;
    apiInstance()
      .get(
        BASE_URL +
        `api/TeamTimeSheet/GetTeamTimeSheetDownloadClassic?timeSheetType=Weekly&currentDate=${moment(fromDate).format("YYYY-MM-DDTHH:mm:ss")}&lastDate=${moment(toDate).format("YYYY-MM-DDTHH:mm:ss")}`,
        {
          responseType: "blob",
        }
      )
      .then(res => {
        this.setState({ exportBtnQuery: "", exportDialog: false });
        fileDownload(res.data, fileName);
        // showMessage(`File downloaded successfully.`, "success");
      });
    return;
  };
  generateWeekAggregation = () => {
    let result = this.props.timesheetState.timeEntry["projectTimesheetVM"].reduce((time, r, i) => {
      const totalTimeEfforts = r.taskEffortVM.reduce((taskEffort, v, i) => {
        const weekTimeStamps = v.timestamp.map((e, i) => {
          const existingDurations = taskEffort.length ? taskEffort : time;
          return e.duration
            ? calTotalTime([existingDurations[i], e.duration])
            : calTotalTime([existingDurations[i], "00:00"]);
        });
        taskEffort = weekTimeStamps;
        return taskEffort;
      }, []);
      time = totalTimeEfforts;
      return time;
    }, []);
    return result;
  };

  render() {
    const { classes, theme, date, profileState } = this.props;

    const {
      expanded,
      approvalDialog,
      timesheetData,
      submitApprovalBtnQuery,
      openModal,
      exportDialog,
      fromDate,
      toDate,
      deleteBtnQuery,
      deleteModal,
      addDurationDialogState,
      selectedWorkspaces,
      isLoading,
      exportBtnQuery,
    } = this.state;

    const addedTasks = ws => this.getAddedTasks(ws);
    let projects = [];
    if (addedTasks.length) projects = timesheetData["projectTimesheetVM"] || [];

    // Sort data
    projects = sortBy(projects, [
      function (o) {
        if (!o.projectId) return "";
        else return o.projectName;
      },
    ]);

    const weekDays = getWeekDates(date);
    // const selectedWorkspaceOptions = this.workspacesDropdownData().filter(w => {
    //   return widgetSettings.selectedWorkspaces.includes(w.obj.teamId);
    // });

    // const usedWorkspaces = projects.reduce((r, cv) => {
    //   if(!r.includes(cv.workspaceId)){
    //     r.push(cv.workspaceId);
    //   }
    //   return r
    // },[]);
    const allProjectsData = this.props.timesheetState.timeEntry["projectTimesheetVM"]
      ? this.generateWeekAggregation()
      : [];
    const aggregatedSum = calTotalTime(allProjectsData);
    return (
      <div>
        <div className={`flex_center_space_between_row ${classes.timesheetHeaderNavCnt}`}>
          <div className={classes.NextBackBtnCnt}>
            <CustomIconButton
              btnType="condensed"
              style={{ padding: 0 }}
              onClick={this.handlePrevWeek}>
              <LeftArrow
                htmlColor={theme.palette.secondary.dark}
                classes={{ root: classes.navigationArrow }}
              />
            </CustomIconButton>
            <div className={classes.selectDate}>
              <span className={classes.toolbarCurrentDate}>{getDate(date)}</span>
              <CustomDatePicker
                dateFormat="MMM DD, YYYY"
                timeInput={false}
                onSelect={(date) => {
                  this.handleSelectDate(date)
                }}
                filterDate={moment()}
                btnProps={{ className: classes.datePickerCustomStyle }}
                style={{
                  display: "flex",
                  alignItems: "center",
                  opacity: 0,
                  position: 'absolute',
                  top: 0,
                  left: 0,
                  width: '100%',
                  height: '100%',
                  zIndex: 2,
                }}
                PopperProps={{ disablePortal: false }}
                showDates={false}
                placeholder={"Select Date"}
                icon={true}
              />
            </div>

            <CustomIconButton
              btnType="condensed"
              style={{ padding: 0, marginRight: 20 }}
              onClick={this.handleNextWeek}>
              <RightArrow
                htmlColor={theme.palette.secondary.dark}
                classes={{ root: classes.navigationArrow }}
              />
            </CustomIconButton>
          </div>
          <div style={{ display: "flex", alignItems: "center" }}>
            {([
              "b286b41822d845ae96489a60b06280ae",
              "630c29e8a2a84ff887f6d3ea1a2907f7",
              "e26f65f31d2546b5bb9bf31f8e581d74",
            ].includes(profileState.data.activeTeam) || ENV === "dev" || ENV === "uat") && (
                <CustomButton
                  variant={"outlined"}
                  btnType={"success"}
                  onClick={this.handleExportOpen}
                  btnQuery={exportBtnQuery}
                  style={{ padding: "3px 12px", marginRight: 10 }}>
                  <ImportExport /> Simple Export to Excel
                </CustomButton>
              )}
            <CustomButton
              variant={"outlined"}
              btnType={"success"}
              onClick={this.handleExportExcel}
              btnQuery={exportBtnQuery}
              style={{ padding: "3px 12px", marginRight: 10 }}>
              <ImportExport /> Export to Excel
            </CustomButton>
            <CustomMultiSelectDropdown
              label="Workspaces"
              hideLabel={true}
              options={() => this.workspacesDropdownData()}
              option={selectedWorkspaces}
              onSelect={this.onWorkspaceSelect}
              scrollHeight={400}
              size={"large"}
              buttonProps={{
                variant: "contained",
                btnType: "white",
                labelAlign: "left",
                style: { width: 250, padding: "3px 4px 3px 8px", height: 35 },
              }}
            />
            {!date.isSame(moment().startOf("isoWeek")) ? (
              <CustomButton
                onClick={this.handleThisWeek}
                btnType="graydashed"
                variant="contained"
                style={{ marginLeft: 10 }}>
                <FormattedMessage id="timesheet.this-week.label" defaultMessage="This Week" />
              </CustomButton>
            ) : null}
          </div>
        </div>
        {/* Next Prev Buttton Ends */}
        {projects && projects.length ? (
          <div className={classes.timeAggregationCnt}>
            {/* timehseet */}
            <p className={classes.footerTotalLabel}>Timesheets Total</p>
            <div className={classes.weekDatesCnt}>
              {allProjectsData.map((d, i) => {
                const weekDaysCmp = generateWeekDates(weekDays, classes, false);
                return (
                  <div className={this.props.classes.footerTotalCell}>
                    {weekDaysCmp[i]}
                    {helper.standardTimeFormat(d)}
                  </div>
                );
              })}
              <div className={classes.footerTotalCell} style={{ paddingRight: 0 }}>
                {helper.standardTimeFormat(aggregatedSum)}
              </div>
            </div>
          </div>
        ) : null}
        <div
          className={classNames({ [classes.timeSheetCnt]: true, [classes.loaderCnt]: isLoading })}
          style={{ height: calculateContentHeight() - 67 }}>
          {isLoading ? (
            <Loader />
          ) : (
            <>
              {addDurationDialogState.open && (
                <AddDurationDialog
                  loadTimeSheet={this.loadWeekData}
                  timeEntryProps={addDurationDialogState}
                  closeAction={this.closeAddTaskDuration}
                  date={date}
                  timesheetPer={timesheetPer}
                  globalTimeIncrement={addDurationDialogState.workspaceConfig}
                />
              )}
              <ActionConfirmation
                open={approvalDialog}
                closeAction={this.approvalDialogClose}
                cancelBtnText={
                  <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
                }
                successBtnText={
                  <FormattedMessage
                    id="timesheet.submit-for-approval-confirmation.confirm-button.label"
                    defaultMessage="Confirm"
                  />
                }
                alignment="center"
                iconType="timerIcon"
                headingText={
                  <FormattedMessage
                    id="timesheet.submit-for-approval-confirmation.title"
                    defaultMessage="Submit For Approval"
                  />
                }
                msgText={
                  <FormattedMessage
                    id="timesheet.submit-for-approval-confirmation.label"
                    defaultMessage="Are you sure you want to submit this timesheet for approval?"
                  />
                }
                note={
                  <FormattedMessage
                    id="timesheet.submit-for-approval-confirmation.note"
                    defaultMessage="Timesheet will be sent to workspace owners, admins and project managers for approval."
                  />
                }
                successAction={this.handleSubmitForApproval}
                btnQuery={submitApprovalBtnQuery}
              />

              {/* Next Prev Buttons Starts */}

              {selectedWorkspaces.map(w => {
                const isExpanded = expanded.includes(w.obj.teamId);
                return (
                  <ExpansionPanel
                    defaultExpanded={true}
                    expanded={isExpanded}
                    onChange={() => this.handleExpand(w.obj.teamId)}
                    classes={{
                      root: classes.workspaceAccordPanelRoot,
                      // expanded: classes.accordPanelRootExpand,
                    }}
                    key={w.obj.teamId}>
                    <ExpansionPanelSummary
                      classes={{
                        root: classes.workspaceAccordSummaryPanel,
                        content: classes.accordSummaryPanelContent,
                        expanded: classes.workspaceAccordSummaryPanelExpand,
                        focused: classes.workspaceAccordSummaryPanelExpandFocused,
                      }}
                      expandIcon={null}>
                      <div className={classes.workspaceHeadingCnt}>
                        <div style={{ display: "flex", alignItems: "center" }}>
                          <CustomAvatar
                            styles={{ marginLeft: 15 }}
                            otherTeam={{ ...w.obj, companyName: w.obj.teamName }}
                            size="xsmall"
                          />
                          <Typography variant="h2" className={classes.workspaceAccorheading}>
                            Workspace name: {this.getWorkspace(w.obj.teamId).teamName}
                          </Typography>
                        </div>
                        <div style={{ display: "flex", alignItems: "center" }}>
                          <TaskListMenu
                            filteredList={this.getAddedTasks(w.obj.teamId)}
                            setTaskListTrigger={this.setTaskListTrigger}
                            handleItemSelect={(taskId, projectId) =>
                              this.handleAddTask(taskId, projectId, w.obj.teamId)
                            }
                            label="Task"
                            keyType="task"
                            disabled={!timesheetPer.addTask.cando}
                            workspaceId={w.obj.teamId}
                          />
                          <div className={classes.vl}></div>
                          {isExpanded ? <ExpandLessIcon /> : <ExpandMoreIcon />}
                        </div>
                      </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails classes={{ root: classes.expansionDetailsCnt }}>
                      {projects.length ? (
                        projects.map((project, index) => {
                          const data = project["taskEffortVM"] || [];
                          const rowsTotalTime = calRowsTotalTime(weekDays, data);
                          const colsTotalTime = calColsTotalTime(weekDays, data);
                          const totalTime = calTotalTime(rowsTotalTime);
                          const statusClass =
                            project["status"] == "In Review"
                              ? classes.inReview
                              : project["status"] == "Rejected"
                                ? classes.rejected
                                : project["status"] == "Approved"
                                  ? classes.approved
                                  : project["status"] == "In Progress - Withdrawn Approval"
                                    ? classes.withdraw
                                    : classes.inProgress;
                          return (
                            w.obj.teamId == project.workspaceId && (
                              <ExpansionPanel
                                defaultExpanded={true}
                                // expanded={expanded === `${index}`}
                                // onChange={this.handleChange(`${index}`)}
                                classes={{
                                  root: classes.accordPanelRoot,
                                  expanded: classes.accordPanelRootExpand,
                                }}
                                key={index}>
                                <ExpansionPanelSummary
                                  classes={{
                                    root: classes.accordSummaryPanel,
                                    content: classes.accordSummaryPanelContent2,
                                    expanded: classes.accordSummaryPanelExpand,
                                  }}
                                  expandIcon={
                                    <div className={classes.expandIcon}>
                                      <ExpandMoreIcon />
                                    </div>
                                  }>
                                  <div className={classes.accordSummaryInnerCnt}>
                                    <div className="flex_center_start_row">
                                      {!project["projectName"] ? (
                                        <SvgIcon
                                          viewBox="0 0 18 15.188"
                                          className={classes.smallBtnIcon}>
                                          <TaskIcon />
                                        </SvgIcon>
                                      ) : (
                                        <SvgIcon
                                          viewBox="0 0 18 15.188"
                                          className={classes.smallBtnIcon}>
                                          <ProjectsIcon />
                                        </SvgIcon>
                                      )}
                                      <Typography variant="h5" className={classes.accorheading}>
                                        {!project["projectName"] ? (
                                          <FormattedMessage
                                            id="timesheet.not-assigned.label"
                                            defaultMessage="Not assigned to projects"
                                          />
                                        ) : (
                                          <>
                                            {" "}
                                            <b>Project Name: </b>
                                            {project["projectName"]}
                                          </>
                                        )}
                                      </Typography>
                                      {!project["projectName"] ? (
                                        <CustomTooltip
                                          helptext={
                                            <FormattedMessage
                                              id="timesheet.not-assigned.hint"
                                              defaultMessage={timesheetsHelpText.otherTasksHelpText}
                                            />
                                          }
                                          iconType="help"
                                          position="static"
                                        />
                                      ) : null}
                                      <span className={`${classes.statusTag} ${statusClass}`}>
                                        {this.showTranslatedStatus(project["status"])}
                                      </span>
                                      <TimesheetComments comments={project["rejectReason"]} />
                                    </div>
                                    <div className="flex_center_start_row">
                                      {project["status"] == "In Progress" ? (
                                        <CustomButton
                                          btnType="gray"
                                          variant="contained"
                                          onClick={e =>
                                            this.approvalDialogOpen(
                                              e,
                                              project["projectId"],
                                              helper.standardTimeFormat(totalTime),
                                              w.obj.teamId
                                            )
                                          }
                                          disabled={
                                            !timesheetPer.submitTimesheet.cando ||
                                            totalTime === "0:0"
                                          }>
                                          <FormattedMessage
                                            id="timesheet.submit-approval-button.label"
                                            defaultMessage="Submit for Approval"
                                          />
                                        </CustomButton>
                                      ) : null}
                                      {project["status"] == "Rejected" ||
                                        project["status"] == "In Progress - Withdrawn Approval" ? (
                                        <CustomButton
                                          btnType="gray"
                                          variant="contained"
                                          onClick={e =>
                                            this.approvalDialogOpen(
                                              e,
                                              project["projectId"],
                                              helper.standardTimeFormat(totalTime),
                                              w.obj.teamId
                                            )
                                          }
                                          disabled={
                                            !timesheetPer.resubmitTimeSheet.cando ||
                                            totalTime === "0:0"
                                          }>
                                          <FormattedMessage
                                            id="timesheet.resubmit-for-approval.label"
                                            defaultMessage="Resubmit for Approval"
                                          />
                                        </CustomButton>
                                      ) : null}
                                      <div
                                        className={`flex_center_center_col ${classes.timeLoggedCnt}`}>
                                        <Typography variant="caption">
                                          {
                                            <FormattedMessage
                                              id="timesheet.time-logged.label"
                                              defaultMessage="Time Logged"
                                            />
                                          }
                                        </Typography>
                                        <p>
                                          {helper.standardTimeFormat(totalTime)}
                                          <span>
                                            {` `}
                                            <FormattedMessage
                                              id="timesheet.hours.label"
                                              defaultMessage="hours"
                                            />
                                          </span>
                                        </p>
                                      </div>
                                    </div>
                                  </div>
                                </ExpansionPanelSummary>
                                <ExpansionPanelDetails
                                  classes={{ root: classes.expansionDetailsCnt }}>
                                  {/* Time Sheet Header Starts */}
                                  <div className={classes.timesheetHeaderCnt}>
                                    <div className={classes.addTaskBtnCnt}>Task Title</div>
                                    <div className={classes.weekDatesCnt}>
                                      {generateWeekDates(weekDays, classes)}
                                      <div
                                        className={classes.headerTotalCell}
                                        style={{ paddingRight: 0 }}>
                                        <FormattedMessage
                                          id="common.total.label"
                                          defaultMessage="Total"
                                        />
                                      </div>
                                    </div>
                                  </div>
                                  {/* Time Sheet Header Ends */}
                                  {data.map((ele, i) => {
                                    return (
                                      <div
                                        key={ele.taskId}
                                        className={classes.weekDatesRow}
                                        style={{ borderBottom: i == 0 ? "none" : "" }}>
                                        <p className={classes.taskTitle}>
                                          {/* billable/ none billable icons  */}
                                          {ele.isBillable ? <CustomTooltip
                                            helptext={
                                              'Billable Task'
                                            }
                                            placement="top"
                                            style={{ color: theme.palette.common.white }}>
                                            <SvgIcon
                                              viewBox="0 0 17.99 17.99"
                                              className={classes.billableIcon}>
                                              <BillableIcon />
                                            </SvgIcon>
                                          </CustomTooltip> :
                                            <CustomTooltip
                                              helptext={
                                                'Non-Billable Task'
                                              }
                                              placement="top"
                                              style={{ color: theme.palette.common.white }}>
                                              <SvgIcon
                                                viewBox="0 0 18.39 18.39"
                                                className={classes.nonBillableIcon}>
                                                <NonBillableIcon />
                                              </SvgIcon>
                                            </CustomTooltip>}
                                          <div>{ele.taskTitle}</div></p>
                                        <div className={classes.weekDatesInputCnt}>
                                          {ele.timestamp.map((stamp, index) => {
                                            let val = this.renderCurrentDate(stamp, index);
                                            const renderUI = (
                                              <div key={stamp.id} className={classes.weekDatesCell}>
                                                {stamp.notes && (
                                                  <div className={classes.triangleTopRight}></div>
                                                )}
                                                <span className={classes.addBtnCnt}>
                                                  <CustomIconButton
                                                    btnType="success"
                                                    onClick={event =>
                                                      this.handleAddTaskDuration(
                                                        project,
                                                        ele,
                                                        stamp,
                                                      )
                                                    }
                                                    style={{
                                                      borderRadius: "4px 0 0 4px",
                                                    }}>
                                                    <img
                                                      src={TimerIconWhite}
                                                      className={classes.whiteTimerIcon}
                                                    />
                                                  </CustomIconButton>
                                                </span>
                                                <DefaultTextField
                                                  label={false}
                                                  error={false}
                                                  formControlStyles={{
                                                    marginTop: 0,
                                                    marginBottom: 0,
                                                  }}
                                                  fullWidth={false}
                                                  defaultProps={{
                                                    id: `${stamp.id}`,
                                                    onClick: event =>
                                                      this.handleAddTaskDuration(
                                                        project,
                                                        ele,
                                                        stamp,
                                                      ),
                                                    // disabled:
                                                    //   project["status"] == "Approved"
                                                    // ? true
                                                    //     : false,
                                                    readOnly: true,
                                                    inputProps: {
                                                      style: {
                                                        padding: "8px 14px",
                                                        textAlign: "center",
                                                        fontSize: "12px",
                                                        transition: "0.15s padding ease",
                                                        cursor: "pointer",
                                                      },
                                                    },
                                                    // onKeyDown: event =>
                                                    //   this.onKeyPress(
                                                    //     event,
                                                    //     project["projectId"],
                                                    //     ele.taskId,
                                                    //     stamp["date"]
                                                    //   ),
                                                    // onBlur: () =>
                                                    //   this.handleBlur(
                                                    //     project["projectId"],
                                                    //     ele.taskId,
                                                    //     stamp["date"]
                                                    //   ),
                                                    // onChange: event =>
                                                    //   this.handleInput(
                                                    //     event,
                                                    //     val,
                                                    //     project["projectId"],
                                                    //     ele.taskId,
                                                    //     stamp["date"]
                                                    //   ),
                                                    value: val == "00:00" ? "-" : val,
                                                  }}
                                                />
                                              </div>
                                            );
                                            return stamp.notes ? (
                                              <CustomTooltip
                                                helptext={
                                                  <div>
                                                    <span className={classes.notesLabel}>
                                                      Notes:
                                                    </span>
                                                    <div className={classes.notes}>
                                                      {stamp.notes}
                                                    </div>
                                                  </div>
                                                }
                                                iconType="help"
                                                position="static"
                                                placement="right"
                                                hideArrow={true}
                                                classes={classes}>
                                                {renderUI}
                                              </CustomTooltip>
                                            ) : (
                                              renderUI
                                            );
                                          })}
                                          <div className={classes.totalTaskTime}>
                                            {helper.standardTimeFormat(rowsTotalTime[i])}
                                          </div>
                                          {project["status"] !== "Approved" &&
                                            project["status"] !== "In Review" &&
                                            timesheetPer.deleteTimesheet.cando ? (
                                            <IconButton
                                              btnType="smallFilledGray"
                                              style={{
                                                padding: 2,
                                                position: "absolute",
                                                right: 0,
                                                background: "#de133e",
                                              }}
                                              onClick={() =>
                                                this.removeTaskFromSheet(
                                                  project["projectId"],
                                                  ele.taskId,
                                                  date
                                                )
                                              }>
                                              <CloseIcon
                                                className={classes.closeIcon}
                                                htmlColor={theme.palette.common.white}
                                              />
                                            </IconButton>
                                          ) : null}
                                        </div>
                                      </div>
                                    );
                                  })}
                                  <div className={classes.timesheetFooterCnt}>
                                    {/* timehseet */}
                                    <p className={classes.footerTotalLabel}>
                                      <FormattedMessage
                                        id="common.total.label"
                                        defaultMessage="Total"
                                      />
                                    </p>
                                    <div className={classes.weekDatesCnt}>
                                      {colsTotalTime.map((time, i) => {
                                        return (
                                          <div
                                            key={i}
                                            className={this.props.classes.footerTotalCell}>
                                            {helper.standardTimeFormat(time)}
                                          </div>
                                        );
                                      })}
                                      <div
                                        className={classes.footerTotalCell}
                                        style={{ paddingRight: 0 }}>
                                        {helper.standardTimeFormat(totalTime)}
                                      </div>
                                    </div>
                                  </div>
                                </ExpansionPanelDetails>
                              </ExpansionPanel>
                            )
                          );
                        })
                      ) : (
                        <div className={classes.emptyStateCnt}>
                          <EmptyState
                            screenType="timesheet"
                            heading={
                              <FormattedMessage
                                id="timesheet.no-time.label"
                                defaultMessage="No Time Logged Yet"
                              />
                            }
                            message={
                              <FormattedMessage
                                id="timesheet.no-time.message"
                                defaultMessage="This is your weekly timesheet view. All your time entries made  in the tasks will be shown here."
                              />
                            }
                            button={false}
                          />
                        </div>
                      )}
                    </ExpansionPanelDetails>
                  </ExpansionPanel>
                );
              })}

              <React.Fragment>
                {deleteModal /** delete modal */ ? (
                  <DeleteConfirmDialog
                    open={openModal}
                    closeAction={this.handleDialogClose}
                    cancelBtnText={
                      <FormattedMessage
                        id="common.action.delete.confirmation.cancel-button.label"
                        defaultMessage="Cancel"
                      />
                    }
                    successBtnText={
                      <FormattedMessage
                        id="common.action.delete.confirmation.delete-button.label"
                        defaultMessage="Delete"
                      />
                    }
                    alignment="center"
                    headingText={
                      <FormattedMessage
                        id="timesheet.delete-confirmation.title"
                        defaultMessage="Delete"
                      />
                    }
                    successAction={this.handleDelete}
                    msgText={
                      <FormattedMessage
                        id="timesheet.delete-confirmation.label"
                        defaultMessage="Are you sure you want to delete this timesheet?"
                      />
                    }
                    btnQuery={deleteBtnQuery}
                  />
                ) : null}
              </React.Fragment>
            </>
          )}
        </div>
        <ExportDialog
          open={exportDialog}
          handleClose={this.handleExportClose}
          fromDate={fromDate}
          toDate={toDate}
          handleDateChange={this.handleSelectExportDate}
          successAction={this.exportAfterConfirmation}
          btnQuery={exportBtnQuery}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    timesheetState: state.timesheet,
    profileState: state.profile,
    tasks: state.tasks.data,
  };
};

export default compose(
  withRouter,
  withStyles(timesheetStyles, { withTheme: true }),
  connect(mapStateToProps, {
    getTeamTimesheetWeekDurations,
    addUserEffort,
    addNewTaskInTeamTimesheet,
    addTeamTaskDuration,
    submitTeamTimesheet,
    updateTasksTimesheetStatus,
    removeTaskLogInTeam,
    getTimesheetTasks,
    selectTimesheetWorkspaces
  })
)(Timesheet);
