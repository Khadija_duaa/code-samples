

const PlanStyles = theme => ({
    mainPlan: {

    },
    planDetailsCnt: {
        padding: '12px 18px',
        borderRadius: 4,
        background: theme.palette.common.white,
        border: `1px solid ${theme.palette.border.lightBorder}`,
        marginBottom: 10,
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    planTitle: {
        display: 'flex',
        alignItems: 'center',
    },
    proPlanTxt: {
        '& span': {
            color: '#552DC5',
        }
    },
    lifeTimePlanName: {
        fontSize: "14px !important",
        '& span': {
            color: theme.palette.text.darkGray,
        }
    },
    planPackage: {
        padding: '4px 8px',
        backgroundColor: theme.palette.background.lightBlue,
        color: theme.palette.text.primary,
        fontSize: "11px !important",
        marginLeft: 5,
        borderRadius: 4,
        color: theme.palette.text.azure,
        marginLeft: 5,
        textTransform: 'uppercase',
        fontWeight: theme.typography.fontWeightMedium,
    },
    planLimitation: {
        marginTop: 7,
        fontSize: "11px !important",
    },
    planDueTimeCnt: {
        display: 'flex',
        alignItems: 'center',
        marginTop: 8,
    },
    paymentDueIcon: {
        fontSize: "14px !important",
        color: theme.palette.secondary.light,
        marginRight: 8
    },
    paymentDueDate: {
        fontSize: "11px !important",
    },
    expiryClass: {
        color: theme.palette.text.danger,
    },
    billingHistoryCnt: {
        borderRadius: 4,
        background: theme.palette.common.white,
        border: `1px solid ${theme.palette.border.lightBorder}`,
    },

});
export default PlanStyles;