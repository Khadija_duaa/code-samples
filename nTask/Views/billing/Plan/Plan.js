import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import classes from "./style";
import Typography from "@material-ui/core/Typography";
import CustomButton from "../../../components/Buttons/CustomButton";
import MeetingsIcon from "../../../components/Icons/MeetingIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import StickyHeadTable from "../../../components/Table/Table";
import CancelSubscription from "../CancelSubscription/CancelSubscription";
import {
  FetchPaymentHistory,
  CancelCustomerSubscriptionState,
} from "../../../redux/actions/userBillPlan";
import { refundSubscription } from "../../../redux/actions/team"
import moment from "moment";
import TableLoader from "../../../components/ContentLoader/Table";
import AddBillDetail from "../AddBillDetail/AddBillDetail";
import { StripeProvider, Elements } from "react-stripe-elements";
import PlanPackages from "../PlanPackages/PlanPackages";
import Constants from "../../../components/constants/planConstant";
import MakePayment from "../MakePayment/MakePayment";
import NoBilling from "../NoBilling/NoBilling";
import { FormattedMessage, injectIntl } from "react-intl";
import CustomTooltip from "../../../components/Tooltip/Tooltip";

class Plan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openCancelSub: false,
      loadingState: true,
      openBillDetail: false,
      openPackages: false,
      packageDetails: null,
      makePayment: false,
      billingHistory: false,
    };
    this.rows = [];
    this.columns = [
      {
        id: "id",
        label: this.props.intl.formatMessage({
          id: "common.table.id.label",
          defaultMessage: "ID",
        }),
        minWidth: 100,
        align: "left",
        padding: "4px 6px 4px 20px",
        lineHeight: "15px",
      },
      {
        id: "date",
        label: this.props.intl.formatMessage({
          id: "common.date.label",
          defaultMessage: "Date",
        }),
        minWidth: 100,
        align: "left",
        padding: "4px 6px 4px 20px",
        lineHeight: "15px",
      },
      {
        id: "plan",
        label: this.props.intl.formatMessage({
          id: "common.table.plan.label",
          defaultMessage: "Plan",
        }),
        minWidth: 100,
        align: "left",
        padding: "4px 6px 4px 20px",
        whiteSpace: "pre-wrap",
        lineHeight: "15px",
      },
      {
        id: "amount",
        label: this.props.intl.formatMessage({
          id: "common.table.Amount.label",
          defaultMessage: "Amount",
        }),
        minWidth: 100,
        align: "left",
        padding: "4px 6px 4px 20px",
        lineHeight: "15px",
      },
      {
        id: "status",
        label: this.props.intl.formatMessage({
          id: "common.table.Status.label",
          defaultMessage: "Status",
        }),
        minWidth: 100,
        align: "left",
        padding: "4px 6px 4px 20px",
        whiteSpace: "pre-wrap",
        lineHeight: "15px",
      },
      // {
      //     id: 'expiry',
      //     label: 'Valid Till',
      //     minWidth: 100,
      //     align: 'left',
      //     padding: '4px 6px 4px 20px',
      //     lineHeight: '15px',
      // },
      {
        id: "method",
        label: this.props.intl.formatMessage({
          id: "common.table.Method.label",
          defaultMessage: "Method",
        }),
        minWidth: 100,
        align: "left",
        padding: "4px 6px 4px 20px",
        whiteSpace: "pre-wrap",
        lineHeight: "15px",
      },
      {
        id: "export",
        label: "",
        minWidth: 100,
        align: "left",
        padding: "4px 6px 4px 20px",
        lineHeight: "15px",
      },
    ];
  }



  componentDidMount() {
    this.loadPaymentHistory();
  }

  /**
   * it is called to get all payment history of the current user related to team from Stripe
   */
  loadPaymentHistory = () => {
    const { teamId } = this.props;
    const { paymentPlanTitle } = this.props.subscriptionDetails;
    this.props.FetchPaymentHistory(
      teamId,
      result => {
        if (result.data) {
          this.rows = result.data.map((item, index) => {
            let obj = {};
            obj.id = item.id ? item.id : "";
            obj.date = item.paymentDate
              ? moment(item.paymentDate).format("MMM DD, YYYY")
              : moment(new Date()).format("MMM DD, YYYY");
            obj.plan = `nTask ${item.planName ? item.planName : paymentPlanTitle
              } \n${this.capitalizeFLetter(item.planType ? item.planType : "")}`;
            obj.amount = `$ ${(item.amount / 100).toFixed(2)}`;
            obj.expiry = item.expiryDate
              ? moment(item.expiryDate).format("MMM DD, YYYY")
              : moment(new Date()).format("MMM DD, YYYY");
            obj.method = `${this.capitalizeFLetter(item.cardBrand)} ${this.capitalizeFLetter(
              item.cardType,
            )} \n${this.props.intl.formatMessage({
              id: "team-settings.billing.ends-in.label",
              defaultMessage: "Ends in",
            })} ${item.cardEnds}`;
            obj.status = item.paid ? this.props.intl.formatMessage({
              id: "team-settings.billing.status.success",
              defaultMessage: "Success",
            }) : this.props.intl.formatMessage({
              id: "team-settings.billing.status.fail",
              defaultMessage: "Fail",
            });
            obj.export = item.viewDetailsUrl ? item.viewDetailsUrl : "";

            return obj;
          });
          this.setState({ loadingState: false, billingHistory: true });
        } else {
          // this.props.showSnackBar('No Payment History');
          this.setState({ loadingState: false, billingHistory: false });
        }
      },
      fail => {
        this.props.showSnackBar(fail.data);
        this.setState({ loadingState: false });
      },
    );
  };
  componentDidUpdate(prevProps, prevState) {
    if (this.props.mode == Constants.BUSINESSPAYMENT) {
      this.makePayment();
    }
  }
  /**
   * to capitalize the plan label
   * @param {string} inputValue
   */
  capitalizeFLetter = inputValue => {
    return inputValue ? inputValue[0].toUpperCase() + inputValue.slice(1) : inputValue;
  };
  /**
   * it is called to open cancel subscription dialog
   */
  openCancelSubscription = () => {
    this.setState({ openCancelSub: true });
  };
  editBill = () => {
  };
  /**
   * it is called to close cancel subscription dialog
   */
  exitCancelSubscription = () => {
    this.setState({ openCancelSub: false });
  };
  /**
   * it is called when cancel subscription successfully done
   * @param {object} data
   */
  successCancelSubscription = data => {
    this.props.CancelCustomerSubscriptionState(
      data,
      result => {
        this.setState({ openCancelSub: false });
      },
      error => {
      },
    );

  };
  /**
   * it is called to open bill detail dialog
   */
  showPaymentHandler = () => {
    this.setState({ openBillDetail: true });
  };
  /**
   * it is called to open package plan dialog
   */
  showPackages = () => {
    this.setState({ openPackages: true });
  };
  /**
   * it is called when user select the package plan for further action
   * @param {object} packageInfo
   */
  selectPackage = packageInfo => {
    this.setState({ openPackages: false, packageDetails: packageInfo, openBillDetail: true });
  };
  /**
   * it is called when payment of plan successfully done
   * @param {object} planData
   */
  paymentPaidHandler = planData => {
    if (this.props.paymentPaidDone) this.props.paymentPaidDone(planData);
    this.setState({ openBillDetail: false });
    this.loadPaymentHistory();
  };
  /**
   * it is called when user exit from bill detail dialog and don't want any change
   */
  exitBillDetail = () => {
    this.setState({ openBillDetail: false });
  };
  /**
   * it is called when doesn't select any package and exit from dialog
   */
  exitPackageDialog = () => {
    this.setState({ openPackages: false });
  };
  /**
   * it is called for open makepayment dialog
   */
  makePayment = () => {
    this.setState({ makePayment: true });
  };
  /**
   * it is called when user doesn't payment and exit from payment dialog
   */
  exitMakePayment = () => {
    this.setState({ makePayment: false });
  };
  /**
   * it is called when user pay his dues andexit from payment dialog
   */
  makePaymentHandler = () => {
    this.setState({ makePayment: false });
  };
  upgradePlan = () => {
    this.props.history.push("/team-settings/upgrade-plan");
  }
  render() {
    const {
      classes,
      subscriptionDetails,
      theme,
      showSnackBar,
      minimumUser,
      mode,
      gracePeriod,
      company,
    } = this.props;
    let {
      currentPeriondEndDate,
      isCancled,
      packageType,
      paymentPlanTitle,
      isTrialUsed,
    } = subscriptionDetails;
    const {
      openBillDetail,
      openPackages,
      packageDetails,
      openCancelSub,
      loadingState,
      makePayment,
      billingHistory,
    } = this.state;
    let expDate = moment(currentPeriondEndDate).format("MMMM DD, YYYY");
    let expMomentDate = moment(new Date(currentPeriondEndDate), "MMMM DD, YYYY");
    let nowMomentDate = moment(new Date(), "MMMM DD, YYYY");
    let diff = expMomentDate.diff(nowMomentDate, "days");
    let paymentDueIconClass = [classes.paymentDueIcon];
    let paymentDueDateClass = [classes.paymentDueDate];
    if (diff < 0) {
      paymentDueIconClass.push(classes.expiryClass);
      paymentDueDateClass.push(classes.expiryClass);
    }
    let planInfo = "";
    if (openBillDetail) {
      planInfo = { ...packageDetails };
      planInfo.pricePlanType =
        packageDetails.planType == Constants.PREMIUMPLAN
          ? Constants.PREMIUMPLANPRICE
          : Constants.BUSINESSPLANPRICE;
    }
    let panelTitle = Constants.DisplayPlanName(paymentPlanTitle);
    let isBusinessPlan = Constants.isBusinessPlan(paymentPlanTitle);
    const getPlanName = () => {
      if (packageType) {
        if (packageType.includes('Tier 5')) { return 'Supreme Saver' }
        else if (packageType.includes('Tier 4')) { return 'Ultimate Saver' }
        else if (packageType.includes('Tier 3')) { return 'Big Saver' }
        else if (packageType.includes('Tier 2')) { return 'Standard Saver' }
        else if (packageType.includes('Tier 1')) { return 'Basic Saver' }
        else {
          return panelTitle
        }
      }
    }
    const isLifetimeDeal = () => {
      if (company.promotion == 'Lifetime Deal 2020') {
        return true
      } else {
        return false
      }
    }

    const showCancelBtn = () => {
      const today = new Date();
      const subscriptionDate = company.subscriptionDetails.currentPeriodStartDate;
      const dueDate = moment(subscriptionDate).add(30, "days").format("MM/DD/YYYY");
      return moment(dueDate).diff(today, "day") >= 1
    }

    return (
      <div className={classes.mainPlan}>
        {loadingState ? (
          <TableLoader />
        ) : openCancelSub ? (
          <CancelSubscription
            exitCancelSubscription={this.exitCancelSubscription}
            successCancelSubscription={this.successCancelSubscription}
            showSnackBar={showSnackBar}
            intl={this.props.intl}></CancelSubscription>
        ) : (
          <Fragment>
            <div
              className={classes.planDetailsCnt}
              fxLayout="row"
              fxLayoutAlign="space-around center">
              {mode == Constants.UPGRADEFROMTRIAL && (
                <Fragment>
                  {!isLifetimeDeal() ?
                    <div>
                      <div className={classes.planTitle}>
                        <Typography variant="h2" className={classes.proPlanTxt}>
                          nTask {panelTitle} Plan
                        </Typography>
                        <span className={classes.planPackage}>{packageType}</span>
                      </div>
                      <Typography variant="body2" className={classes.planLimitation}>
                        <FormattedMessage
                          id="team-settings.billing.unlimited.label"
                          values={{ t: panelTitle }}
                          defaultMessage={`Unlimited access to all ${panelTitle} features of nTask`}
                        />
                      </Typography>
                      <div className={classes.planDueTimeCnt}>
                        <SvgIcon viewBox="0 0 17.031 17" className={classes.paymentDueIcon}>
                          <MeetingsIcon />
                        </SvgIcon>
                        <Typography variant="h5" className={classes.paymentDueDate}>
                          <FormattedMessage
                            id="team-settings.billing.payment-due.label"
                            values={{ p: expDate }}
                            defaultMessage={`Next payment due on ${expDate}`}
                          />
                        </Typography>
                      </div>
                    </div>
                    : <div>
                      <div className={classes.planTitle}>
                        <Typography variant="h2" className={classes.proPlanTxt}>
                          Lifetime Deal by <span>Prime</span>
                        </Typography>
                      </div>
                      <Typography variant="body2" className={classes.lifeTimePlanName}>
                        Current Plan: <span>{getPlanName(panelTitle)}</span>
                      </Typography>
                    </div>}

                  <div>
                    {false ? (
                      /** if is business plan then disable cancel button and show tooltip  */
                      <CustomTooltip
                        helptext="Please contact support at support@ntaskmanager.com to cancel your subscription."
                        iconType="help"
                        placement="top"
                        style={{ color: theme.palette.common.white }}>
                        <CustomButton
                          btnType="danger"
                          variant="contained"
                          style={{
                            marginRight: 4,
                            backgroundColor: `rgba(243, 243, 243, 1)`,
                            color: `rgba(0, 0, 0, 0.26)`,
                          }}
                          onClick={() => {
                          }}
                          disabled={isCancled}>
                          <FormattedMessage
                            id="team-settings.billing.cancel-subscription-button.label"
                            defaultMessage="Cancel Subscription"
                          />
                        </CustomButton>
                      </CustomTooltip>
                    ) : null}
                    {isLifetimeDeal() ?
                      <>
                        {showCancelBtn() ?
                          <CustomButton
                            btnType="success"
                            variant="contained"
                            style={{
                              marginRight: 4,
                              backgroundColor: `${theme.palette.background.dangerLight}`,
                              color: `${theme.palette.text.danger}`,
                            }}
                            onClick={this.openCancelSubscription}
                            disabled={isCancled}>
                            Cancel & Request Refund
                          </CustomButton> : null}
                        <CustomButton
                          btnType="green"
                          variant="contained"
                          style={{
                            marginRight: 4,
                          }}
                          onClick={this.upgradePlan}
                        // disabled={isCancled}
                        >
                          Upgrade
                        </CustomButton>
                      </>
                      :
                      <CustomButton
                        btnType="success"
                        variant="contained"
                        style={{
                          marginRight: 4,
                          backgroundColor: `${theme.palette.background.dangerLight}`,
                          color: `${theme.palette.text.danger}`,
                        }}
                        onClick={this.openCancelSubscription}
                        disabled={isCancled}>
                        <FormattedMessage
                          id="team-settings.billing.cancel-subscription-button.label"
                          defaultMessage="Cancel Subscription"
                        />
                      </CustomButton>
                    }
                  </div>
                </Fragment>
              )}
              {mode == Constants.UPGRADEFROMFREE && (
                <Fragment>
                  <div>
                    <div className={classes.planTitle}>
                      <Typography variant="h2" className={classes.proPlanTxt}>
                        nTask Free Plan
                      </Typography>
                    </div>
                    <Typography variant="body2" className={classes.planLimitation}>
                      <FormattedMessage
                        id="team-settings.billing.unlimited.label"
                        values={{ t: "Free" }}
                        defaultMessage="Unlimited access to all Free features of nTask"
                      />
                    </Typography>
                  </div>
                  <div>
                    <CustomButton
                      btnType="blue"
                      variant="contained"
                      style={{ marginBottom: 0 }}
                      onClick={this.showPackages}>
                      <FormattedMessage
                        id="team-settings.billing.upgrade-button.label"
                        defaultMessage="Upgrade"
                      />
                    </CustomButton>
                  </div>
                </Fragment>
              )}
              {mode == Constants.GRACEPERIODPLAN && (
                <Fragment>
                  <div>
                    <div className={classes.planTitle}>
                      <Typography variant="h2" className={classes.proPlanTxt}>
                        nTask {panelTitle} Plan
                      </Typography>
                    </div>
                    <div className={classes.planDueTimeCnt}>
                      <SvgIcon viewBox="0 0 17.031 17" className={paymentDueIconClass.join(" ")}>
                        <MeetingsIcon />
                      </SvgIcon>
                      <Typography variant="body2" className={paymentDueDateClass.join(" ")}>
                        Your {packageType} payment is past due as of {expDate}
                      </Typography>
                    </div>
                  </div>
                  <div>
                    <CustomButton
                      btnType="blue"
                      variant="contained"
                      style={{ marginBottom: 0 }}
                      onClick={this.makePayment}>
                      Make Payment
                    </CustomButton>
                  </div>
                </Fragment>
              )}
            </div>
            {billingHistory ? (
              <div className={classes.billingHistoryCnt}>
                <StickyHeadTable
                  classes={classes}
                  rows={this.rows}
                  columns={this.columns}></StickyHeadTable>
              </div>
            ) : (
              <NoBilling></NoBilling>
            )}
          </Fragment>
        )}
        {openBillDetail && (
          <StripeProvider apiKey={STRIPE_PUBLISH_KEY}>
            <Elements>
              <AddBillDetail
                showSnackBar={this.props.showSnackBar}
                paymentPaidHandler={planData => this.paymentPaidHandler(planData)}
                exitBillDetail={this.exitBillDetail}
                minimumUser={minimumUser}
                selectedPackageDetails={planInfo}></AddBillDetail>
            </Elements>
          </StripeProvider>
        )}
        {openPackages && (
          <PlanPackages
            selectPackage={this.selectPackage}
            showSnackBar={this.props.showSnackBar}
            exitPackageDialog={this.exitPackageDialog}
            mode={Constants.UPGRADEPLANMODE}></PlanPackages>
        )}
        {makePayment && (
          <StripeProvider apiKey={STRIPE_PUBLISH_KEY}>
            <Elements>
              <MakePayment
                showSnackBar={this.props.showSnackBar}
                makePaymentHandler={this.makePaymentHandler}
                exitMakePayment={this.exitMakePayment}></MakePayment>
            </Elements>
          </StripeProvider>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    subscriptionDetails: state.profile.data.teams.find(
      item => item.companyId == state.profile.data.activeTeam,
    ).subscriptionDetails,
    company: state.profile.data.teams.find(
      item => item.companyId == state.profile.data.activeTeam,
    ),
    teamId: state.profile.data.activeTeam,
    minimumUser: state.profile.data.teamMember.filter(
      item => item.isDeleted == false && (item.isActive == null || item.isActive == true),
    ).length,
    gracePeriod: state.profile.data.gracePeriod,
  };
};
export default compose(
  withRouter,
  injectIntl,
  connect(mapStateToProps, {
    FetchPaymentHistory,
    CancelCustomerSubscriptionState,
  }),
  withStyles(classes, { withTheme: true }),
)(Plan);
