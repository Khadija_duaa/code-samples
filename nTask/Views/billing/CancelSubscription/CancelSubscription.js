import React, { Component, Fragment, useState, useEffect } from "react";
import { compose } from "redux";
import { connect, useSelector } from "react-redux";
import { withRouter, Link } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import classes from "./style";
import Typography from "@material-ui/core/Typography";
import CustomButton from "../../../components/Buttons/CustomButton";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import CustomAvatar from "../../../components/Avatar/Avatar";
import NotificationMessage from "../../../components/NotificationMessages/NotificationMessages";
import SelectSearchDropdown from "../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { generateAssigneeData } from "../../../helper/generateSelectData";
import ButtonActionsCnt from "../../../components/Dialog/ConfirmationDialogs/ButtonActionsCnt";
import DefaultTextField from "../../../components/Form/TextField";
import CheckCircle from "@material-ui/icons/CheckCircle";
import IconButton from "../../../components/Buttons/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import moment from "moment";
import InputAdornment from "@material-ui/core/InputAdornment";
import { CancelCustomerSubscription } from "../../../redux/actions/userBillPlan";
import { authenticatePassword } from "../../../redux/actions/authentication";
import { getTeamProjectStatus, refundSubscription } from "../../../redux/actions/team";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import Constants from "../../../components/constants/planConstant";
import SvgIcon from "@material-ui/core/SvgIcon";
import AdvanceFilterIcon from "../../../components/Icons/AdvanceFilterIcon";
import IconCustomFields from "../../../components/Icons/IconCustomFields";
import AttachmentIcon from "../../../components/Icons/AttachmentIcon";
import BasicIcon from "../../../components/Icons/BasicIcon";
import IconPermissionAlert from "../../../components/Icons/IconPermissionAlert";
import BulkActionsIcon from "../../../components/Icons/BulkActionsIcon";
import GanttIcon from "../../../components/Icons/GanttIcon";
import IconKanban from "../../../components/Icons/IconKanban";
import IconCustomTaskStatus from "../../../components/Icons/IconCustomTaskStatus";
import IntegrationsIcon from "../../../components/Icons/IntegrationsIcon";
import KanbanIcon from "../../../components/Icons/KanbanIcon";
import ProjectsIcon from "../../../components/Icons/ProjectsIcon";
import ReportingIcon from "../../../components/Icons/ReportingIcon";
import RiskManagmentIcon from "../../../components/Icons/RiskManagmentIcon";
import RolesPermissionsIcon from "../../../components/Icons/RolesPermissionsIcon";
import StorageIcon from "../../../components/Icons/StorageIcon";
import PremiumIcon from "../../../components/Icons/PremiumIcon";
import PremiumCrownIcon from "../../../components/Icons/PremiumCrownIcon";
import BusinessBreifcaseIcon from "../../../components/Icons/BusinessBreifcaseIcon";
import SadFaceIcon from "../../../assets/images/sadFace.svg";
import CustomRadio from "../../../components/Form/Radio/Radio";
import { FormattedMessage, injectIntl } from "react-intl";
import Grid from "@material-ui/core/Grid";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import Divider from "@material-ui/core/Divider";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import ToggleButton from "@material-ui/lab/ToggleButton";
import Brightness1Icon from "@material-ui/icons/Brightness1";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import cloneDeep from "lodash/cloneDeep";
import { Scrollbars } from "react-custom-scrollbars";
import isEmpty from "lodash/isEmpty";
import { validEmail } from "../../../utils/validator/common/email";
const getSteps = () => {
  return ["Cancellation Notice", "Select Active Users", "Confirmation"];
};

const cancelOptions = [
  "Missing features I need",
  "Switching to another product",
  "Too expensive",
  "Technical issues",
  "Other",
]; //Options user will select to tell the reason of cancellation

function convertedTextOptions(intl) {
  let convertedOptions = [];
  cancelOptions.map(label => {
    convertedOptions.push(getTranslatedText(intl, label));
  });
  return convertedOptions;
}
function getTranslatedText(intl, label) {
  let t = { id: "", defaultMessage: label };
  switch (label) {
    case "Missing features I need":
      t.id = "team-settings.billing.cancel-reason-confirmations.type.list.missing";
      break;
    case "Switching to another product":
      t.id = "team-settings.billing.cancel-reason-confirmations.type.list.switching";
      break;

    case "Too expensive":
      t.id = "team-settings.billing.cancel-reason-confirmations.type.list.expensive";
      break;
    case "Technical issues":
      t.id = "team-settings.billing.cancel-reason-confirmations.type.list.technical";
      break;
    case "Other":
      t.id = "team-settings.billing.cancel-reason-confirmations.type.list.other";
      break;
  }
  return intl.formatMessage(t);
}
function CancelSubscription(props) {
  const {
    classes,
    closeTrialExpirePopUp,
    companyId,
    members,
    theme,
    subscriptionDetails,
    companyName,
    ownerName,
    intl,
  } = props;
  const { currentPeriondEndDate, packageType, paymentPlanTitle } = subscriptionDetails;
  const planDisplayName = Constants.DisplayPlanName(paymentPlanTitle);

  const [activeStep, setActiveStep] = useState(0);
  const [assignees, setAassignees] = useState([]);
  const [teamName, setTeamName] = useState("");
  const [paypalEmail, setPaypalEmail] = useState("");
  const [userFeedback, setuserFeedback] = useState("");
  const [cancelDialog, setCancelDialog] = useState(1);
  // const [showteamName, setShowteamName] = useState(false);
  const [teamNameError, setTeamNameError] = useState(false);
  const [teamNameErrorMsg, setTeamNameErrorMsg] = useState("");
  const [paypalEmailError, setPaypalEmailError] = useState(false);
  const [paypalEmailErrorMsg, setPaypalEmailErrorMsg] = useState("");
  const [feedbackError, setFeedbackError] = useState(false);
  const [feedbackErrorMsg, setFeedbackErrorMsg] = useState("");
  const [btnQuery, setBtnQuery] = useState("");
  const [cancelSubData, setCancelSubData] = useState("");
  const [cancelOption, setCancelOption] = useState("");
  const [currentView, setView] = useState("mappManually");
  const [projectsArr, setProjectsArr] = useState([]);
  const [totalTaskCount, setTotalTaskCount] = useState(0);
  const statusColor = props.theme.palette.taskStatusNew;
  const steps = getSteps();
  /**
   * it is for move to next screen
   */
  const { profileState } = useSelector(state => {
    return {
      profileState: state.profile.data,
    };
  });
  const { activeTeam, teams } = profileState;
  const company = teams.find(t => t.companyId === activeTeam) || {};
  const handleNext = () => {
    if (activeStep == 2) {
      submitCancelSubscription();
    } else {
      if (planDisplayName !== "Premium") setActiveStep(prevActiveStep => prevActiveStep + 1);
      else setActiveStep(prevActiveStep => prevActiveStep + 2);
    }
  };
  //  const handleNext = () => {
  //   if (activeStep == 2) {
  //     submitCancelSubscription();
  //   } else if (activeStep == 0) {
  //     setActiveStep(prevActiveStep => prevActiveStep + 2);
  //   } else setActiveStep(prevActiveStep => prevActiveStep + 1);
  // };
  const handleBack = () => {
    if (planDisplayName !== "Premium") setActiveStep(prevActiveStep => prevActiveStep - 1);
    else setActiveStep(prevActiveStep => prevActiveStep - 2);
  };
  const handleChangeTab = (event, nextView) => {
    if (nextView) setView(nextView);
  };
  /**
   * it is for go to 3rd screen of cancel dialog
   */
  const updateCancelDialog = () => {
    setCancelDialog(3);
  };
  /**
   * it is called when user cancel his plan, that they have taken
   */
  const cancelCompleted = () => {
    props.successCancelSubscription(cancelSubData);
  };
  /**
   * it is called when user select thier members for downgrade plan
   * @param {*} key
   * @param {string} value
   */
  const handleActiveUserChange = (key, value) => {
    //Local state is update for dropdown so object can be made to send update to backend with API
    //Updating local state when option is selected in assignee dropdown
    if (value.length < 11) setAassignees(value);
  };
  /**
   * it is called when don't want to do any thing and exit from dialog
   */
  const exitCancelSubscription = () => {
    props.exitCancelSubscription();
  };

  // const cancelSubscription = () => {
  //     if(props.cancelSubscription)
  //         props.cancelSubscription();
  // }
  /**
   * it is called when user enter teamName for cancel his team subscription
   * @param {event} e
   */
  const handleChange = e => {
    setTeamName(e.target.value);
  };
  const handleChangeEmail = e => {
    setPaypalEmail(e.target.value);
  };
  const handleUserFeedbackChange = e => {
    // when user enter cancel subscription feedback, it updates the value of textarea
    setuserFeedback(e.target.value);
  };
  /**
   * it is called for submit when user fullfill all params
   */
  const submitCancelSubscription = () => {
    // let pwsswordAuthData = {
    //     grant_type: "teamName",
    //     username: props.profileState.data.email,
    //     teamName: teamName
    //   };
    const { companyName } = props;
    if (userFeedback.trim().length < 1) {
      setFeedbackError(true);
      setFeedbackErrorMsg(
        this.props.intl.formatMessage({
          id: "feedback.messagea",
          defaultMessage: "Please enter your feedback.",
        })
      );
      return;
    } else {
      setFeedbackError(false);
      setFeedbackErrorMsg("");
    }
    // let validateEmail = validEmail(paypalEmail);
    // if (isLifetimeDeal() && !validateEmail.validated) {
    //   setBtnQuery("");
    //   setPaypalEmailError(true)
    //   setPaypalEmailErrorMsg(validateEmail.errorMessage);
    //   return
    // } else {
    //   setPaypalEmailError(false);
    //   setPaypalEmailErrorMsg("");
    // }
    setBtnQuery("progress");
    if (teamName.trim().toLowerCase() == companyName.trim().toLowerCase()) {
      const { companyId } = props;
      if (isLifetimeDeal()) {
        const cancelPostObj = {
          TeamId: companyId,
          paypalEmail: 'Not Provided',
        }
        refundSubscription(
          cancelPostObj,
          result => {
            setCancelSubData({ companyInfo: companyId });
            // props.showSnackBar(result.message);
            setCancelDialog(2);
          },
          error => {
            props.showSnackBar(error.message);
            setBtnQuery("");
          },
        );
      } else {
        const cancelPostObj = {
          TeamId: companyId,
          Description: userFeedback.trim(),
          Reasons: [cancelOption],
          isStatusMappingRequired: planDisplayName == "Premium" ? false : true
        }
        props.CancelCustomerSubscription(
          cancelPostObj,
          response => {
            setCancelSubData({ companyInfo: response.data.team });
            props.showSnackBar(response.data.message);
            setCancelDialog(2);
          },
          fail => {
            props.showSnackBar(fail.data.message);
            setBtnQuery("");
          }
        );
      }

      // let mapsProjectStatus = [];
      // let mappingStatus = [];
      // if (!isEmpty(projectsArr)) {
      //   projectsArr.map(p => {
      //     p.statuses.map(s => {
      //       mappingStatus.push({
      //         statusIDFrom: s.status.statusId,
      //         statusIDTo:
      //           currentView == "mappManually" ? (s.selectedStatus ? s.selectedStatus.value : 0) : 0,
      //       });
      //     });
      //     mapsProjectStatus.push({
      //       projectId: p.item.projectId,
      //       workspaceId: p.item.projectStatus.workSpaceId,
      //       projectTitle: p.item.projectName,
      //       mappingStatus: mappingStatus,
      //     });
      //   });
      // }

      // cancelPostObj.mapsProjectStatus = mapsProjectStatus;

    } else {
      setTeamNameError(true);
      setTeamNameErrorMsg("Invalid Team Name");
      setBtnQuery("");
    }


    // props.authenticateteamName(
    //     pwsswordAuthData,
    //     //Success Callback
    //     () => {
    //         const {companyId} = props;
    //         props.CancelCustomerSubscription(companyId,response => {
    //             if(response.status == 200){
    //                 setCancelSubData({companyInfo: response.data.company});
    //                 props.showSnackBar(response.data.message);
    //                 setCancelDialog(2);
    //             }else{
    //                 props.showSnackBar(response.data.message);
    //                 setBtnQuery('')
    //             }
    //         });
    //     },

    //     error => {
    //         setteamNameError(true);
    //         setteamNameErrorMsg(error.data.error_description);
    //         setBtnQuery('');
    //     }
    //   );
  };

  const handleSubscReasonCheck = event => {
    setCancelOption(event.target.value);
  };
  // const handleSelectStatus = (key, opt, id, status) => {
  //   let updatedState = projectsArr.map(p => {
  //     if (p.item.projectId == id) {
  //       p.statuses = p.statuses.map(s => {
  //         if (s.status.statusCode == status.statusCode) {
  //           s.selectedStatus = opt;
  //         }
  //         return s;
  //       });
  //     }
  //     return p;
  //   });
  //   setProjectsArr(updatedState);
  // }; 
  // const handleViewClose = ele => {
  //   let updatedState = projectsArr.map(p => {
  //     if (p.item.projectId == ele.item.projectId) {
  //       p.isView = !p.isView;
  //       return p;
  //     } else return p;
  //   });
  //   setProjectsArr(updatedState);
  // };
  // const generateStatusArr = () => {
  //   return [
  //     {
  //       label: "Not Started",
  //       value: 0,
  //       icon: (
  //         <Brightness1Icon htmlColor={statusColor.NotStarted} className={classes.statusIcon} />
  //       ),
  //     },
  //     {
  //       label: "In Progress",
  //       value: 1,
  //       icon: (
  //         <Brightness1Icon htmlColor={statusColor.InProgress} className={classes.statusIcon} />
  //       ),
  //     },
  //     {
  //       label: "In Review",
  //       value: 2,
  //       icon: <Brightness1Icon htmlColor={statusColor.InReview} className={classes.statusIcon} />,
  //     },
  //     {
  //       label: "Completed",
  //       value: 3,
  //       icon: (
  //         <Brightness1Icon htmlColor={statusColor.Completed} className={classes.statusIcon} />
  //       ),
  //     },
  //     {
  //       label: "Cancelled",
  //       value: 4,
  //       icon: (
  //         <Brightness1Icon htmlColor={statusColor.Cancelled} className={classes.statusIcon} />
  //       ),
  //     },
  //   ];
  // };
  // const mappingProjectArr = itemArr => {
  //   let projectInfo = [];
  //   itemArr.map(element => {
  //     element.projectInfo.map(p => {
  //       let arr = p.projectStatus.statusList.map(s => {
  //         return {
  //           status: s,
  //           selectedStatus: null,
  //         };
  //       });
  //       projectInfo.push({
  //         item: p,
  //         statuses: arr,
  //         isView: true,
  //         selectedStatus: null,
  //       });
  //     });
  //   });
  //   setProjectsArr(projectInfo);
  // };
  useEffect(() => {
    props.getTeamProjectStatus(
      succ => {
        // mappingProjectArr(succ.teamsInfo);
        setTotalTaskCount(succ.taskCounts);
      },
      fail => {
      }
    );
  }, []);
  const isLifetimeDeal = () => {
    if (company.promotion == 'Lifetime Deal 2020') {
      return true
    } else {
      return false
    }
  }

  let expiryDate = moment(currentPeriondEndDate).format("MMMM DD, YYYY");
  const selectLbl = `You have Selected ${assignees.length}/10`;
  const successBtnTitle =
    activeStep == steps.length - 1 && !isLifetimeDeal() ? (
      <FormattedMessage
        id="team-settings.billing.cancel-subscription-button.label"
        defaultMessage="Cancel Subscription"
      />
    ) : activeStep == steps.length - 1 && isLifetimeDeal() ? ('Cancel & Request Refund') : (
      <FormattedMessage
        id="team-settings.billing.cancel-subscription-confirmation.proceed.label"
        defaultMessage="Proceed"
      />
    );
  const successBtnType =
    activeStep == steps.length - 1
      ? // <FormattedMessage id="team-settings.billing.status.danger" defaultMessage="danger" />
      "danger"
      : // <FormattedMessage id="team-settings.billing.status.success" defaultMessage="success" />
      "danger";
  const companyNameCheck = teamName.trim().toLowerCase() == companyName.trim().toLowerCase();
  const paypalEmailCheck = validEmail(paypalEmail);

  const successBtnDisableCheck = btnQuery === "progress" || (activeStep === 2 && (!cancelOption || !companyNameCheck));
  const cancelOptionsLabel = convertedTextOptions(intl);
  return cancelDialog == 1 ? (
    <Dialog
      classes={{
        paper: classes.dialogPaperCnt,
        scrollBody: classes.dialogCnt,
      }}
      fullWidth={true}
      scroll="body"
      disableBackdropClick={true}
      disableEscapeKeyDown={true}
      open={true}
      onClose={exitCancelSubscription}>
      <DialogTitle
        id="form-dialog-title"
        classes={{
          root: activeStep == 2 ? classes.defaultDialogTitle2 : classes.defaultDialogTitle,
        }}>
        <Grid container direction="row" justify="space-between" alignItems="center">
          <Grid item style={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
            {activeStep !== 0 && (
              <div className={classes.arrowBackCnt} onClick={handleBack}>
                <ArrowBackIosIcon className={classes.arrowBackIcon} />
              </div>
            )}
            {activeStep == 0 && (
              <Typography variant="h2" className={classes.title}>
                <FormattedMessage
                  id="team-settings.billing.cancel-subscription-button.label"
                  defaultMessage="Cancel Subscription"
                />
              </Typography>
            )}
            {activeStep == 1 && (
              // <Typography variant="h2" className={classes.title2}>
              //   {`"${intl.formatMessage({ id:"team-settings.billing.cancel-subscription-button.label", defaultMessage: "Cancel Subscription"})}"`} 
              //   <span style={{ color: "#202020" }}> <FormattedMessage id="team-settings.billing.status-mapping-confirmation.title" defaultMessage="Status Mapping"/> </span>
              // </Typography>
              <Typography variant="h2" className={classes.title}>
                <FormattedMessage
                  id="team-settings.billing.cancel-subscription-button.label"
                  defaultMessage="Cancel Subscription"
                />
              </Typography>
            )}
          </Grid>
          {activeStep !== 2 && (
            <Grid item>
              <IconButton btnType="transparent" onClick={exitCancelSubscription}>
                <CloseIcon htmlColor={theme.palette.secondary.medDark} />
              </IconButton>
            </Grid>
          )}
        </Grid>
      </DialogTitle>
      <DialogContent className={classes.defaultDialogContent}>
        <div className={classes.defaultDialogBody}>
          <div className={classes.defaultDialogStepper}>
            {steps.map((label, index) => {
              const ctrsClasses = `${classes.stepsCtrls} + ${activeStep == index ? classes.stepsCtrlsActive : classes.stepsCtrlsInActive
                }`;
              const arrowClasses = `${steps.length - 1 == index ? classes.arrowRightRemove : classes.arrowRight
                } + ${activeStep == index ? classes.arrowRightActive : classes.arrowRightInActive}`;
              const chkCrlClass = `${activeStep - 1 >= index ? classes.checkCircleActive : classes.checkCircleInActive
                }`;
              const stepLblMain = `${classes.stepLblMain} + ${activeStep < index ? classes.stepLblMainUnSelected : ""
                }`;
              return (
                <div className={classes.stepsHeader}>
                  <div className={ctrsClasses}>
                    <div className={classes.stepLbl}>
                      <span className={stepLblMain}>Step {index + 1}</span>
                      <span className={classes.stepLblSub}>{label}</span>
                    </div>
                    <CheckCircle className={chkCrlClass}></CheckCircle>
                  </div>
                  <div className={arrowClasses}></div>
                </div>
              );
            })}
          </div>
          <div>
            {activeStep === 0 ? (
              <div className={classes.stepOneCnt}>
                {/* <CustomAvatar
                                        userActiveTeam={true}
                                        size="large"
                                    /> */}
                {Constants.isBusinessPlan(paymentPlanTitle) ? (
                  <Fragment>
                    {!isLifetimeDeal() ?
                      <NotificationMessage
                        type="NewInfo"
                        iconType="info"
                        style={{
                          width: "100%",
                          marginTop: 19,
                          textAlign: "start",
                          fontSize: "13px",
                          padding: "10px 15px 15px 15px",
                          backgroundColor: theme.palette.background.light,
                          borderRadius: 4,
                          fontWeight: 400,
                          fontFamily: theme.typography.fontFamilyLato,
                          color: "#333333",
                        }}>
                        <FormattedMessage id="common.hi.label" defaultMessage="Hi" /> {ownerName},{" "}
                        <FormattedMessage
                          id="team-settings.billing.cancel-subscription-confirmation.title"
                          defaultMessage="your current Business Plan will remain active till the next billing cycle"
                        />{" "}
                        <span style={{ color: theme.palette.text.green }}>{expiryDate}</span>,{" "}
                        <FormattedMessage
                          id="team-settings.billing.cancel-subscription-confirmation.title2"
                          defaultMessage="if you choose to cancel now."
                        />
                      </NotificationMessage> : null}
                    <Typography
                      variant="h6"
                      className= {classes.fontSize}
                      style={{
                        marginTop: 20,
                        textAlign: "start",
                        width: "100%",
                        fontFamily: theme.typography.fontFamilyLato,
                        fontSize: "13px",
                      }}>
                      {/* <FormattedMessage
                        id="team-settings.billing.cancel-subscription-confirmation.label"
                        values={{
                          t: (
                            <span
                              style={{
                                color: theme.palette.text.azure,
                                fontWeight: theme.typography.fontWeightMedium,
                              }}>
                              nTask Business Plan
                            </span>
                          ),
                        }}
                        defaultMessage={`You and your team will not have access to the ${(
                          <span
                            style={{
                              color: theme.palette.text.azure,
                              fontWeight: theme.typography.fontWeightMedium,
                            }}>
                            nTask Business Plan
                          </span>
                        )} features after cancellation. The team will continue to be able to access the existing workspaces, tasks and meetings.`}
                      /> */}
                      You and your team will not have access to the {(
                        <span
                          style={{
                            color: theme.palette.text.azure,
                            fontWeight: theme.typography.fontWeightMedium,
                          }}>
                          nTask Business Plan
                        </span>
                      )} features after cancellation. The team will continue to be able to access the existing workspaces, tasks and meetings.
                    </Typography>
                    <Typography
                      variant="h6"
                      className= {classes.fontSize}
                      style={{
                        marginTop: 20,
                        textAlign: "start",
                        width: "100%",
                        fontFamily: theme.typography.fontFamilyLato,
                        fontSize: "13px",
                      }}>
                      <FormattedMessage
                        id="team-settings.billing.cancel-subscription-confirmation.heres-missing.label"
                        defaultMessage="Here's what you'll be missing:"
                      />
                    </Typography>
                    <ul className={classes.packages}>
                      {!isLifetimeDeal() ?
                        <li>
                          <div className={classes.packageItem}>
                            <SvgIcon viewBox="0 0 20 20" className={classes.svgIcon}>
                              <StorageIcon />
                            </SvgIcon>
                            <span className={classes.packageItemTxt}>
                              <FormattedMessage
                                id="team-settings.billing.cancel-subscription-confirmation.heres-missing.list.storage"
                                values={{ n: 10 }}
                                defaultMessage="10 GB Storage"
                              />
                            </span>
                          </div>
                        </li> : null}
                      <li>
                        <div className={classes.packageItem}>
                          <SvgIcon viewBox="0 0 20 20" className={classes.svgIcon}>
                            <ProjectsIcon />
                          </SvgIcon>
                          <span className={classes.packageItemTxt}>
                            <FormattedMessage id="project.label" defaultMessage="Projects" />
                          </span>
                        </div>
                      </li>
                      <li>
                        <div className={classes.packageItem}>
                          <SvgIcon viewBox="0 0 20 20" className={classes.svgIcon}>
                            <GanttIcon />
                          </SvgIcon>
                          <span className={classes.packageItemTxt}>
                            <FormattedMessage
                              id="team-settings.billing.cancel-subscription-confirmation.heres-missing.list.Gantt"
                              defaultMessage="Gantt"
                            />
                          </span>
                        </div>
                      </li>
                      <li>
                        <div className={classes.packageItem}>
                          <SvgIcon viewBox="0 0 22 22" className={classes.svgIcon}>
                            <IconKanban />
                          </SvgIcon>
                          <span className={classes.packageItemTxt}>{"Boards"}</span>
                        </div>
                      </li>
                      <li>
                        <div className={classes.packageItem}>
                          <SvgIcon viewBox="0 0 22 22" className={classes.svgIcon}>
                            <IconCustomTaskStatus />
                          </SvgIcon>
                          <span className={classes.packageItemTxt}>{"Custom Task Status"}</span>
                        </div>
                      </li>
                      <li>
                        <div className={classes.packageItem}>
                          <SvgIcon viewBox="0 0 20 20" className={classes.svgIcon}>
                            <RiskManagmentIcon />
                          </SvgIcon>
                          <span className={classes.packageItemTxt}>
                            <FormattedMessage
                              id="team-settings.billing.cancel-subscription-confirmation.heres-missing.list.risk-mangement"
                              defaultMessage="Risk Management"
                            />
                          </span>
                        </div>
                      </li>
                      <li>
                        <div className={classes.packageItem}>
                          <SvgIcon viewBox="0 0 20 20" className={classes.svgIcon}>
                            <BulkActionsIcon />
                          </SvgIcon>
                          <span className={classes.packageItemTxt}>
                            <FormattedMessage
                              id="team-settings.billing.cancel-subscription-confirmation.heres-missing.list.bulk-actions"
                              defaultMessage="Bulk Actions"
                            />
                          </span>
                        </div>
                      </li>
                      <li>
                        <div className={classes.packageItem}>
                          <SvgIcon viewBox="0 0 20 20" className={classes.svgIcon}>
                            <AdvanceFilterIcon />
                          </SvgIcon>
                          <span className={classes.packageItemTxt}>
                            <FormattedMessage
                              id="team-settings.billing.cancel-subscription-confirmation.heres-missing.list.custom-filters"
                              defaultMessage="Create/Save Custom Filters"
                            />{" "}
                          </span>
                        </div>
                      </li>
                      <li>
                        <div className={classes.packageItem}>
                          <SvgIcon viewBox="0 0 22 22" className={classes.svgIcon}>
                            <IconCustomFields />
                          </SvgIcon>
                          <span className={classes.packageItemTxt}>
                            Custom Fields
                          </span>
                        </div>
                      </li>
                    </ul>
                  </Fragment>
                ) : null}
                {Constants.isPremiumPlan(paymentPlanTitle) ? (
                  <Fragment>
                    <NotificationMessage
                      type="NewInfo"
                      iconType="info"
                      style={{
                        width: "100%",
                        marginTop: 19,
                        textAlign: "start",
                        fontSize: "13px",
                        padding: "10px 15px 15px 15px",
                        backgroundColor: theme.palette.background.light,
                        borderRadius: 4,
                        fontWeight: 400,
                        fontFamily: theme.typography.fontFamilyLato,
                        color: "#333333",
                      }}>
                      <FormattedMessage id="common.hi.label" defaultMessage="Hi" /> {ownerName}, <FormattedMessage id="team-settings.billing.cancel-subscription-confirmation.titlepremium" defaultMessage="your current Premium Plan will remain active till the next
                      billing cycle"/>{" "}
                      <span style={{ color: theme.palette.text.green }}>{expiryDate}</span>,{" "}
                      <FormattedMessage
                        id="team-settings.billing.cancel-subscription-confirmation.title2"
                        defaultMessage="if you choose to cancel now."
                      />
                      {/* Your {packageType} subscription is paid until {expiryDate}.<br />You will still be able to access all of our {planDisplayName} features till that date. */}
                    </NotificationMessage>
                    <Typography
                      variant="h6"
                      style={{ marginTop: 20, textAlign: "start", width: "100%" }}>
                      {/* <FormattedMessage
                        id="team-settings.billing.cancel-subscription-confirmation.label"
                        values={{
                          t: (
                            <span
                              style={{
                                color: theme.palette.text.azure,
                                fontWeight: theme.typography.fontWeightMedium,
                              }}>
                              nTask Premium Plan
                            </span>
                          ),
                        }}
                        defaultMessage={`You and your team will not have access to the ${(
                          <span
                            style={{
                              color: theme.palette.text.azure,
                              fontWeight: theme.typography.fontWeightMedium,
                            }}>
                            nTask Premium Plan
                          </span>
                        )} features after cancellation. The team will continue to be able to access the existing workspaces, tasks and meetings.`}
                      /> */}
                      You and your team will not have access to the {(
                        <span
                          style={{
                            color: theme.palette.text.azure,
                            fontWeight: theme.typography.fontWeightMedium,
                          }}>
                          nTask Premium Plan
                        </span>
                      )} features after cancellation. The team will continue to be able to access the existing workspaces, tasks and meetings.
                    </Typography>
                    <Typography
                      variant="h6"
                      style={{ marginTop: 20, textAlign: "start", width: "100%" }}>
                      <FormattedMessage
                        id="team-settings.billing.cancel-subscription-confirmation.heres-missing.label"
                        defaultMessage="Here's what you'll be missing:"
                      />{" "}
                    </Typography>
                    <ul className={classes.packages}>
                      <li>
                        <div className={classes.packageItem}>
                          <SvgIcon viewBox="0 0 20 20" className={classes.svgIcon}>
                            <StorageIcon />
                          </SvgIcon>
                          <span className={classes.packageItemTxt}>
                            <FormattedMessage
                              id="team-settings.billing.cancel-subscription-confirmation.heres-missing.list.storage"
                              values={{ n: 5 }}
                              defaultMessage="5 GB Storage"
                            />
                          </span>
                        </div>
                      </li>
                      <li>
                        <div className={classes.packageItem}>
                          <SvgIcon viewBox="0 0 20 20" className={classes.svgIcon}>
                            <ProjectsIcon />
                          </SvgIcon>
                          <span className={classes.packageItemTxt}>
                            <FormattedMessage id="project.label" defaultMessage="Projects" />
                          </span>
                        </div>
                      </li>
                      <li>
                        <div className={classes.packageItem}>
                          <SvgIcon viewBox="0 0 20 20" className={classes.svgIcon}>
                            <GanttIcon />
                          </SvgIcon>
                          <span className={classes.packageItemTxt}>
                            <FormattedMessage
                              id="team-settings.billing.cancel-subscription-confirmation.heres-missing.list.Gantt"
                              defaultMessage="Gantt"
                            />
                          </span>
                        </div>
                      </li>
                      <li>
                        <div className={classes.packageItem}>
                          <SvgIcon viewBox="0 0 22 22" className={classes.svgIcon}>
                            <IconKanban />
                          </SvgIcon>
                          <span className={classes.packageItemTxt}>{"Boards"}</span>
                        </div>
                      </li>
                      <li>
                        <div className={classes.packageItem}>
                          <SvgIcon viewBox="0 0 20 20" className={classes.svgIcon}>
                            <BulkActionsIcon />
                          </SvgIcon>
                          <span className={classes.packageItemTxt}>
                            <FormattedMessage
                              id="team-settings.billing.cancel-subscription-confirmation.heres-missing.list.bulk-actions"
                              defaultMessage="Bulk Actions"
                            />
                          </span>
                        </div>
                      </li>
                      <li>
                        <div className={classes.packageItem}>
                          <SvgIcon viewBox="0 0 20 20" className={classes.svgIcon}>
                            <AdvanceFilterIcon />
                          </SvgIcon>
                          <span className={classes.packageItemTxt}>
                            <FormattedMessage
                              id="team-settings.billing.cancel-subscription-confirmation.heres-missing.list.custom-filters"
                              defaultMessage="Create/Save Custom Filters"
                            />
                          </span>
                        </div>
                      </li>
                      <li>
                        <div className={classes.packageItem}>
                          <SvgIcon viewBox="0 0 22 22" className={classes.svgIcon}>
                            <IconCustomFields />
                          </SvgIcon>
                          <span className={classes.packageItemTxt}>
                            Custom Fields
                          </span>
                        </div>
                      </li>
                    </ul>
                  </Fragment>
                ) : null}
              </div>
            ) : activeStep === 1 ? (
              <div className={classes.stepTwoCnt}>
                <div className={classes[`centerAlignContent`]}>
                  <div>
                    <SvgIcon viewBox="0 0 80 80" className={classes.IconPermissionAlert}>
                      <IconPermissionAlert />
                    </SvgIcon>
                  </div>
                  <div className={classes.messageCnt}>
                    <span className={classes.message}>
                      <span style={{ fontWeight: 700 }}> {`${totalTaskCount} ${intl.formatMessage({ id: "task.label", defaultMessage: "tasks" }).toLowerCase()}`} </span>
                      {/* {` ${intl.formatMessage({id:"team-settings.billing.status-mapping-confirmation.label", defaultMessage:"will be effected if you cancel"})}`}{" "} */}
                      will be affected, If you cancel your {" "}
                      <span
                        style={{
                          color: theme.palette.text.azure,
                          fontWeight: theme.typography.fontWeightMedium,
                        }}>

                        {!isLifetimeDeal() ? `nTask ${planDisplayName}` : null}
                      </span>{" "}
                      {/* {intl.formatMessage({id:"common.subscription.label",defaultMessage:"subscription"})}. */}
                      plan subscription. These custom task statuses will be deleted permanently and cannot be recovered.
                    </span>
                  </div>
                  <div className={classes.messageCnt}>
                    <span className={classes.message}>
                      {/* {<FormattedMessage id="team-settings.billing.status-mapping-confirmation.label2" defaultMessage="How should we handle these tasks?"/>} */}
                      Are you sure you want to cancel your {" "}
                      <span
                        style={{
                          color: theme.palette.text.azure,
                          fontWeight: theme.typography.fontWeightMedium,
                        }}>
                        {!isLifetimeDeal() ? `nTask ${planDisplayName}` : null}
                      </span>{" "}
                      plan subscription?
                    </span>
                  </div>
                </div>
                {/* <Divider style={{ margin: "20px 0" }} /> */}
                {/* <div className={classes.toggleContainer}>
                    <ToggleButtonGroup
                      value={currentView}
                      exclusive
                      onChange={handleChangeTab}
                      classes={{ root: classes.toggleBtnGroup }}>
                      <ToggleButton
                        value="mappManually"
                        classes={{
                          root: classes.toggleButton,
                          selected: classes.toggleButtonSelected,
                        }}>
                        Map Status Manually
                      </ToggleButton>
                      <ToggleButton
                        value="allOpen"
                        classes={{
                          root: classes.toggleButton,
                          selected: classes.toggleButtonSelected,
                        }}>
                        Change all to{" "}
                        <Brightness1Icon
                          style={{ color: "#FC7150" }}
                          className={classes.openIcon}
                        />{" "}
                        {`Not Started`}
                      </ToggleButton>
                    </ToggleButtonGroup>
                  </div> */}

                {/* {currentView == "mappManually" && (
                    <>
                      {projectsArr.length > 0 &&
                        projectsArr.map((p, index) => {
                          return (
                            <>
                              <div className={classes.labelCnt}>
                                {" "}
                                <span
                                  className={classes.labelTitleLeft}
                                  onClick={() => handleViewClose(p)}>
                                  {" "}
                                  {p.isView ? (
                                    <ArrowDropDownIcon className={classes.iconShowHide} />
                                  ) : (
                                    <ArrowRightIcon className={classes.iconShowHide} />
                                  )}
                                  {p.item.projectName}
                                </span>
                                {index == 0 && (
                                  <span className={classes.labelTitleRight}>
                                    nTask Standard Status
                                  </span>
                                )}
                              </div>
                              {p.isView &&
                                p.statuses.map((s, indexKey) => {
                                  return (
                                    <>
                                      <div className={classes.statusesCnt} key={indexKey}>
                                        <CustomButton
                                          btnType="white"
                                          variant="contained"
                                          style={{
                                            fontSize: "14px !important",
                                            color: "#202020",
                                            border: "none",
                                            background: "#F6F6F6",
                                            marginLeft: 0,
                                            fontFamily: theme.typography.fontFamilyLato,
                                            fontWeight: theme.palette.fontWeightMedium,
                                            padding: "7px 50px 7px 9px",
                                            width: 170,
                                            display: "flex",
                                            flexDirection: "row",
                                            justifyContent: "end",
                                          }}>
                                          <Brightness1Icon
                                            className={classes.statusesIcon}
                                            style={{ color: s.status.statusColor }}
                                          />
                                          <span
                                            className={classes.statusTitle}
                                            title={s.statusTitle}>
                                            {s.status.statusTitle}{" "}
                                          </span>
                                        </CustomButton>
                                        <span className={classes.taskCountTitle}>
                                          {" "}
                                          {s.status.taskCount} tasks{" "}
                                          <ArrowForwardIcon className={classes.arrowRight} />{" "}
                                        </span>

                                        <SelectSearchDropdown
                                          data={generateStatusArr}
                                          label={null}
                                          selectChange={(key, opt) => {
                                            handleSelectStatus(
                                              key,
                                              opt,
                                              p.item.projectId,
                                              s.status
                                            );
                                          }}
                                          type="status"
                                          selectedValue={s.selectedStatus}
                                          placeholder={"Select status"}
                                          icon={true}
                                          isMulti={false}
                                          isDisabled={false}
                                          styles={{ width: 170, margin: 0 }}
                                        />
                                      </div>
                                    </>
                                  );
                                })}
                            </>
                          );
                        })}
                    </>
                  )} */}
              </div>
            ) : activeStep === 2 ? (
              <div className={classes.stepThreeCnt}>
                {/* <CustomAvatar
                                                userActiveTeam={true}
                                                size="large"
                                            /> */}

                <div className={classes.cancelSubscFeedbackCnt}>
                  <img src={SadFaceIcon} className={classes.sadImage} />
                  <Typography
                    variant="h1"
                    style={{
                      fontFamily: theme.typography.fontFamilyLato,
                    }}>
                    <FormattedMessage
                      id="team-settings.billing.cancel-reason-confirmations.title"
                      defaultMessage="We're sad to see you go..."
                    />
                  </Typography>
                  <Typography variant="body1" className={classes.cancelSubTitle}>
                    <FormattedMessage
                      id="team-settings.billing.cancel-reason-confirmations.label"
                      defaultMessage="Before you cancel, please let us know the reason you are leaving. Every bit of feedback helps!"
                    />
                  </Typography>
                  <div className={classes.cancelOptionsCnt}>
                    <CustomRadio
                      options={cancelOptionsLabel}
                      value={cancelOption}
                      onRadioChange={handleSubscReasonCheck}
                    />
                  </div>

                  <DefaultTextField
                    label=""
                    errorState={feedbackError}
                    errorMessage={feedbackErrorMsg}
                    defaultProps={{
                      id: "userFeedback",
                      onChange: handleUserFeedbackChange,
                      placeholder: intl.formatMessage({
                        id:
                          "team-settings.billing.cancel-reason-confirmations.feedback.placeholder",
                        defaultMessage: "Type your feedback here",
                      }),
                      type: "text",
                      value: userFeedback,
                      multiline: true,
                      rows: 5,
                      inputProps: {
                        maxLength: 500,
                      },
                    }}
                  />
                  <Typography
                    variant="h6"
                    style={{ textAlign: "start", width: "100%", userSelect: 'none' }}>


                    {/* <FormattedMessage
                      id="team-settings.billing.cancel-reason-confirmations.type.label"
                      defaultMessage={`Type ${(
                        <span style={{ fontWeight: theme.typography.fontWeightLarge }}>
                          {companyName}
                        </span>
                      )} in the field below to cancel your nTask subscription.`}
                      values={{
                        t: (
                          <span style={{ fontWeight: theme.typography.fontWeightLarge }}>
                            {companyName}
                          </span>
                        ),
                      }}
                    /> */}
                    Type "{(
                      <span style={{ fontWeight: theme.typography.fontWeightLarge }}>
                        {companyName}
                      </span>
                    )}" in the field below to cancel your nTask subscription.
                  </Typography>
                  <DefaultTextField
                    label={""}
                    errorState={teamNameError}
                    errorMessage={teamNameErrorMsg}
                    defaultProps={{
                      id: "teamName",
                      onChange: handleChange,
                      placeholder: intl.formatMessage({
                        id: "team-settings.billing.cancel-reason-confirmations.team-name.label",
                        defaultMessage: "Enter Team Name",
                      }),
                      type: "text",
                      value: teamName,
                      // inputProps: {
                      //     maxLength: 40
                      // },
                    }}
                  />
                  {isLifetimeDeal() ? <>
                    <Typography
                      variant="h6"
                      style={{ textAlign: "start", width: "100%", userSelect: 'none' }}>
                      Please enter your PayPal ID if you wish to receive your refund there.
                    </Typography>
                    <DefaultTextField
                      label={""}
                      // errorState={paypalEmailError}
                      // errorMessage={paypalEmailErrorMsg}
                      defaultProps={{
                        id: "teamName",
                        onChange: handleChangeEmail,
                        placeholder: "Enter Your Paypal ID (Optional)",
                        type: "email",
                        value: paypalEmail,
                        // inputProps: {
                        //     maxLength: 40
                        // },
                      }}
                    /></> : null}
                </div>
              </div>
            ) : (
              <div></div>
            )}
          </div>
        </div>
        <ButtonActionsCnt
          cancelAction={exitCancelSubscription}
          successAction={handleNext}
          successBtnText={successBtnTitle}
          cancelBtnText={
            <FormattedMessage
              id="team-settings.billing.cancel-subscription-confirmation.nevermind-button.label"
              defaultMessage="Nevermind, I don't want to cancel"
            />
          }
          cancelBtnProps={{ style: { color: theme.palette.text.azure, marginRight: 15 } }}
          btnType={successBtnType}
          btnQuery={btnQuery}
          disabled={successBtnDisableCheck}
        />
      </DialogContent>
    </Dialog>
  ) : cancelDialog == 2 ? (
    <Dialog
      classes={{
        paper: classes.dialogPaperCnt,
        scrollBody: classes.dialogCnt,
      }}
      fullWidth={true}
      scroll="body"
      disableBackdropClick={true}
      disableEscapeKeyDown={true}
      open={true}
      onClose={cancelCompleted}>
      {isLifetimeDeal() ?
        <DialogContent className={classes.cancelDialog3Content}>
          <div className={classes.cancelDialog2Body}>
            <Typography variant="h4" style={{ fontSize: "20px !important", color: '#000' }}>
              We're sorry to see you go.
            </Typography>
            <Typography variant="body2" style={{ marginTop: 16, fontSize: "14px", color: '#000' }}>
              Cancel subscription request has been submitted. A customer support agent will get in touch with you shortly with an update.
            </Typography>
            <div className={classes.cancelDialog2FooterBtns} style={{ marginTop: 30 }}>
              <CustomButton btnType="success" variant="contained" onClick={cancelCompleted}>
                <FormattedMessage id="common.action.Continue.label" defaultMessage="Continue" />
              </CustomButton>
            </div>
          </div>
        </DialogContent>
        :
        <DialogContent className={classes.cancelDialog2Content}>
          <div className={classes.cancelDialog2Body}>
            <CustomAvatar userActiveTeam={true} size="large" />
            <Typography variant="h4" style={{ marginTop: 32 }}>
              <FormattedMessage
                id="common.cancel-subscription.message"
                defaultMessage=" Subscription Details Updated"
              />
            </Typography>
            <Typography variant="body2" style={{ marginTop: 16 }}>
              <FormattedMessage
                id="common.cancel-subscription.messageb"
                defaultMessage="Your team is now on nTask Free Plan."
              />
            </Typography>
            <div className={classes.cancelDialog2FooterBtns}>
              <CustomButton btnType="success" variant="contained" onClick={cancelCompleted}>
                <FormattedMessage id="common.action.Continue.label" defaultMessage="Continue" />
              </CustomButton>
            </div>
          </div>
        </DialogContent>
      }

    </Dialog>
  ) : cancelDialog == 3 ? (
    <Dialog
      classes={{
        paper: classes.cancelDialog3,
        scrollBody: classes.dialogCnt,
      }}
      style={{ padding: 150 }}
      aria-labelledby="form-dialog-title"
      fullWidth={true}
      scroll="body"
      open={true}
      onEscapeKeyDown={exitCancelSubscription}>
      <DialogContent className={classes.cancelDialog3Content}>
        <div className={classes.cancelDialog2Body}>
          <CustomAvatar userActiveTeam={true} size="large" />
          <Typography variant="h4" style={{ marginTop: 32 }}>
            <FormattedMessage
              id="common.cancel-subscription.message"
              defaultMessage="Subscription Details Updated"
            />
          </Typography>
          <Typography variant="body2" style={{ marginTop: 16 }}>
            <FormattedMessage
              id="common.cancel-subscription.messagec"
              defaultMessage="Your workspace is now on nTask Free Plan."
            />
          </Typography>
          <Typography variant="body2" style={{ marginTop: 34, textAlign: "center" }}>
            Your payment will be refunded within 7 working days.
            <br />
            If you have any query.{" "}
            <a href="mailto: support@ntaskmanager.com" className={classes.linkBtnStyle}>
              Contact Us.
            </a>
          </Typography>
          <div className={classes.cancelDialog3FooterBtns}>
            <CustomButton btnType="success" variant="contained" onClick={cancelCompleted}>
              <FormattedMessage id="common.action.Continue.label" defaultMessage="Continue" />
            </CustomButton>
          </div>
        </div>
      </DialogContent>
    </Dialog>
  ) : null;
}
const mapStateToProps = state => {
  let company = state.profile.data.teams.find(
    item => item.companyId == state.profile.data.activeTeam
  );
  return {
    profileState: state.profile,
    members: state.profile.data ? state.profile.data.teamMember : [],
    companyId: state.profile.data.activeTeam,
    subscriptionDetails: company ? company.subscriptionDetails : "",
    companyName: company ? company.companyName : "",
    ownerName: state.profile.data.teamMember.find(item => item.userId == state.profile.data.userId)
      .fullName,
  };
};
export default compose(
  withRouter,
  injectIntl,
  connect(mapStateToProps, {
    CancelCustomerSubscription,
    authenticatePassword,
    getTeamProjectStatus,
  }),
  withStyles(classes, { withTheme: true })
)(CancelSubscription);
