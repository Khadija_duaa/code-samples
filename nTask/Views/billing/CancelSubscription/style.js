import planBgImg from "../../../assets/images/bg_1.png";
import stepArrowGrey from "../../../assets/images/stepArrow_grey.png";
import stepArrowWhite from "../../../assets/images/stepArrow_white.png";
const CancelSubscriptionStyles = theme => ({
  dialogPaperCnt: {
    overflowY: "visible",
    background: theme.palette.common.white,
    width: "570px !important",
    maxWidth: 750,
  },
  fontSize: {
    fontSize: "13px !important"
  },
  fontSizeBody: {
    fontSize: "14px !important"
  },
  fontSizeHead: {
    fontSize: "20px !important"
  },
  cancelDefaultDialogTitle: {
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    padding: "20px 18px 20px 25px",
    backgroundColor: theme.palette.background.light,
    borderTopRightRadius: "inherit",
    borderTopLeftRadius: "inherit",
  },
  cancelTitle: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    fontWeight: theme.typography.fontWeightRegular,
  },
  defaultDialogContent: {
    padding: "0!important",
  },
  defaultDialogBody: {
    // padding: "32px 0"
    paddingTop: 10,
    paddingBottom: 32,
  },
  defaultDialogStepper: {
    display: "none",
    alignContent: "space-between",
    border: `2px solid ${theme.palette.border.lightBorder}`,
    borderRadius: 4,
    height: 48,
  },
  stepsHeader: {
    display: "flex",
    fontSize: "10px !important",
    width: "100%",
    position: "relative",
    backgroundColor: theme.palette.background.light,
  },
  stepsCtrls: {
    display: "flex",
    width: "100%",
    justifyContent: "space-around",
    padding: "9px 0 10px 0",
    alignItems: "center",
  },
  stepsCtrlsActive: {
    backgroundColor: theme.palette.background.default,
  },
  stepsCtrlsInActive: {
    backgroundColor: theme.palette.background.light,
  },
  stepLbl: {
    display: "flex",
    flexDirection: "column",
    paddingLeft: 10,
  },
  stepLblMain: {
    fontSize: "13px !important",
  },
  stepLblMainUnSelected: {
    color: theme.palette.text.light,
  },
  stepLblSub: {
    fontSize: "12px !important",
    paddingTop: 6,
    color: theme.palette.text.light,
  },
  arrowRightRemove: {
    display: "none",
  },
  arrowRight: {
    width: 15,
    right: -12,
    height: 45,
    zIndex: 1,
    top: -1,
    position: "absolute",
  },
  arrowRightActive: {
    background: `url(${stepArrowWhite})`,
    backgroundRepeat: "no-repeat",
  },
  arrowRightInActive: {
    background: `url(${stepArrowGrey})`,
    backgroundRepeat: "no-repeat",
  },
  checkCircleActive: {
    fontSize: "14px !important",
    color: theme.palette.primary.light,
  },
  checkCircleInActive: {
    visibility: "collapse",
  },
  stepOneCnt: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    textAlign: "center",
    padding: "0 25px",
  },
  stepTwoCnt: {
    padding: "0 25px",
  },
  stepThreeCnt: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    textAlign: "center",
  },
  cancelSubscFeedbackCnt: {
    padding: "0px 25px 0 25px",
    width: "100%",
    textAlign: "center",
  },
  dlgFooter: {
    marginTop: 40,
    justifyContent: "flex-end",
    display: "flex",
  },

  cancelDialog2: {
    overflowY: "visible",
    background: theme.palette.common.white,
    width: "520px!important",
  },
  cancelDialog2Content: {
    padding: "145px 64px 147px 64px!important",
    background: `url(${planBgImg})`,
    backgroundRepeat: "no-repeat",
  },
  cancelDialog3Content: {
    padding: "60px !important",
    // background: `url(${planBgImg})`,
    backgroundRepeat: "no-repeat",
    textAlign: 'center',
  },
  cancelDialog2Body: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  cancelDialog2FooterBtns: {
    marginTop: 60,
  },

  cancelDialog3: {
    overflowY: "visible",
    background: theme.palette.common.white,
    width: "410px!important",
  },
  cancelDialog3Content: {
    padding: "35px 40px!important",
  },
  cancelDialog3Body: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  linkBtnStyle: {
    color: theme.palette.text.azure,
    fontSize: "12px !important",
  },
  cancelDialog3FooterBtns: {
    marginTop: 50,
  },

  cancelOptionsCnt: {
    width: "100%",
    textAlign: "left",
    marginBottom: 15,
  },
  active: {},
  cancelSubTitle: {
    color: theme.palette.text.medGray,
    fontSize: "16px !important",
    width: "80%",
    margin: "10px auto 30px auto",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 500,
  },
  packages: {
    fontSize: "14px !important",
    color: theme.palette.text.darkGray,
    fontWeight: theme.typography.fontWeightLight,
    paddingInlineStart: 0,
    listStyleType: "none",
    marginTop: 10,
    "& li": {
      paddingBottom: 10,
      display: "inline",
      float: "left",
      width: "50%",
    },
    "& li:nth-child(even)": {
      marginLeft: 10,
      width: "calc(50% - 10px)",
    },
  },
  svgIcon: {
    fontSize: "20px !important",
  },
  packageItem: {
    display: "flex",
    alignItems: "center",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: 4,
    padding: 10,
  },
  packageItemTxt: {
    marginLeft: 16,
    fontFamily: theme.typography.fontFamilyLato,
    fontSize: "13px !important",
  },
  defaultDialogTitle: {
    padding: "15px 20px",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    userSelect: "none",
  },
  defaultDialogTitle2: {
    padding: "15px 20px",
    // borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    userSelect: "none",
  },
  arrowBackIcon: {
    fontSize: "12px !important",
    marginLeft: 5,
  },
  arrowBackCnt: {
    background: "#F3F3F3",
    height: "30px",
    width: "31px",
    borderRadius: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    marginRight: "10px",
    alignItems: "center",
    cursor: "pointer",
  },
  title: {
    fontWeight: 700,
    fontFamily: theme.typography.fontFamilyLato,
    color: "#202020",
    fontSize: "18px !important",
  },
  title2: {
    fontWeight: 700,
    fontFamily: theme.typography.fontFamilyLato,
    color: "#7E7E7E",
    fontSize: "18px !important",
  },
  centerAlignContent: {
    // padding: "20px 60px",
    marginTop: 15,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  IconPermissionAlert: {
    fontSize: "8px !important",
    marginBottom: 10,
  },
  message: {
    color: "#202020",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
    fontSize: "15px !important",
    lineHeight: 1.6,
  },
  messageCnt: {
    textAlign: "center",
  },
  toggleBtnGroup: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    flexWrap: "nowrap",
    width: "100%",
    borderRadius: 4,
    background: theme.palette.common.white,
    // border: `1px solid ${theme.palette.border.lightBorder}`,
    height: 40,
    boxShadow: "none",
    "& $toggleButtonSelected": {
      color: theme.palette.text.secondary,
      backgroundColor: "white",
      "&:after": {
        background: theme.palette.common.white,
      },
      "&:hover": {
        background: theme.palette.common.white,
      },
    },
  },
  toggleButton: {
    height: "auto",
    padding: "4px 20px 5px",
    fontSize: "13px !important",
    fontWeight: theme.palette.fontWeightMedium,
    fontFamily: theme.typography.fontFamilyLato,
    textTransform: "capitalize",
    width: "100%",
    background: "#F6F6F6",
    "&:hover": {
      background: theme.palette.common.white,
    },
    "&[value = 'mappManually']": {
      //  borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
      borderRight: `1px solid ${theme.palette.border.lightBorder}`,
    },
  },
  toggleButtonSelected: {},
  toggleContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
    marginBottom: 25,
  },
  openIcon: {
    fontSize: "11px !important",
    color: "#ABB4BB",
    margin: "2px 3px 0px 3px",
  },
  statusesCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-around",
    marginBottom: 10,
  },
  arrowRight: {
    marginBottom: "-4px",
    fontSize: "18px !important",
    marginLeft: 10,
  },
  taskCountTitle: {
    fontSize: "14px !important",
    color: "#202020",
    fontWeight: theme.palette.fontWeightMedium,
    fontFamily: theme.typography.fontFamilyLato,
  },
  statusesIcon: {
    marginRight: 5,
    fontSize: "12px !important",
  },
  labelCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    fontSize: "13px !important",
    color: "#202020",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
    marginBottom: 10,
  },
  labelTitleLeft: {
    marginLeft: 12,
  },
  iconShowHide: {
    color: "#7E7E7E",
    marginBottom: -7,
    cursor: "pointer",
  },
  labelTitleRight: {
    marginRight: 65,
    color: "#969696",
  },
  statusIcon: {
    fontSize: "11px !important",
    marginRight: 5,
  },
  statusTitle: {
    // maxWidth : 50,
    overflow: "hidden",
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
  },
});
export default CancelSubscriptionStyles;
