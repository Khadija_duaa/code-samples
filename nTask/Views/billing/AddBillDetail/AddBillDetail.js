import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter, Link } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import classes from "./style";
import Typography from "@material-ui/core/Typography";
import CustomButton from "../../../components/Buttons/CustomButton";
import {
  CardNumberElement,
  CardExpiryElement,
  CardCVCElement,
  injectStripe,
} from "react-stripe-elements";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import creditCardLogo from "../../../assets/images/cc_logos.png";
import DefaultTextField from "../../../components/Form/TextField";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import ToggleButton from "@material-ui/lab/ToggleButton";
import Divider from "@material-ui/core/Divider";
import Input from "@material-ui/core/Input";
import {
  UpgradePremiumPlan,
  FetchPricePlans,
  FetchPaymentMethod,
  FetchPromotionCode,
} from "../../../redux/actions/userBillPlan";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import IconButton from "../../../components/Buttons/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import SelectSearchDropdown from "../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { generateSelectData } from "../../../helper/generateSelectData";
import AddBillSuccess from "./AddBillSuccess";
import AddBillDenied from "./AddBillDenied";
import isEmpty from "lodash/isEmpty";
import Constants from "../../../components/constants/planConstant";
import moment from "moment";
import { FormattedMessage, injectIntl } from "react-intl";
import MappingStatusModal from "../MappingCustomStatus";

class AddBillDetail extends Component {
  constructor(props) {
    super(props);
    this.labelRef = React.createRef();
    this.state = {
      paymentType: "creditcard",
      promocodeState: props.promocode ? "inProgress" : "initial",
      promocodeValue: props.promocode ? props.promocode : "",
      promocodePriceValue: {},
      // totalMembers:
      //   props.minimumUser < 3 && Constants.isPremiumPlan(props.selectedPackageDetails.planType)
      //     ? 3
      //     : props.minimumUser,
      totalMembers: props.minimumUser,
      labelWidth: 0,
      subscription: props.selectedPackageDetails.subscription,
      // pricePlanType: props.selectedPackageDetails.pricePlanType,
      planType: props.selectedPackageDetails.planType,
      mode: "bill",
      errorMessage: "",
      btnSubmitQuery: "",
      planPrice: {},
      loading: true,
      cards: [],
      selectedCard: {
        label: props.intl.formatMessage({
          id: "team-settings.billing.bill-dialog.payment-details.placeholder",
          defaultMessage: "New Payment Method",
        }),
        id: "0000",
      },
      btnPromoQuery: "",
      showBill: false,
      isStatusMappingRequiredModal: false,
    };
    this.subscriptionTypes = {
      monthly: "unitPriceMonthly",
      annually: "unitPriceAnually",
    };
    this.maximumUser = 300;
    this.minimumUser = props.minimumUser;
    // this.minimumUser =
    //   props.minimumUser < 3 && Constants.isPremiumPlan(props.selectedPackageDetails.planType)
    //     ? 3
    //     : props.minimumUser;
  }
  /**
   * It is for fetch priceplans for our paid plans and payments method if user already have
   */
  componentDidMount() {
    this.props.FetchPricePlans(
      result => {
        this.setState({ planPrice: result.data, loading: false });
      },
      fail => {
        this.props.showSnackBar("Payment Price Plans not available");
        this.setState({ loading: false });
      }
    );
    this.props.FetchPaymentMethod(
      response => {
        let cardsData = this.readyCardData(response.data ? response.data.sources.data : []);
        this.setState({
          cards: cardsData,
        });
      },
      fail => {
        this.props.showSnackBar(fail.data);
      }
    );
    if (this.props.promocode) {
      this.promocodeHandler();
    }
  }
  /**
   * It is for generate cards details for selection control and 1 entry for new card
   * @param {Array} cardsArr
   */
  readyCardData = cardsArr => {
    let data = [];
    data = cardsArr.map(item => {
      return {
        label:
          item.brand +
          this.props.intl.formatMessage({
            id: "team-settings.billing.bill-dialog.cardsendin",
            defaultMessage: "card ends in",
          }) +
          item.last4,
        id: item.id,
      };
    });
    data.push({
      label: this.props.intl.formatMessage({
        id: "team-settings.billing.bill-dialog.payment-details.placeholder",
        defaultMessage: "New Payment Method",
      }),
      id: "0000",
    });
    return data;
  };
  /**
   * It is to change state if card selected
   * @param {Event} event
   */
  cardTypeChange = event => {
    this.setState({ paymentType: event.target.value });
  };
  /**
   * It is to detect change in card number, expiry card , and CVC number
   * @param {Event} error
   */
  handleChange = ({ error }) => {
    if (error) {
      this.setState({ errorMessage: error.message });
    } else this.setState({ errorMessage: "" });
  };
  /**
   * It is to submit cards details and generate token from Stripe
   * @param {Event} evt
   */
  handleSubmit = evt => {
    evt.preventDefault();
    const { selectedCard } = this.state;
    if (this.props.stripe) {
      this.setState({ btnSubmitQuery: "progress" }, () => {
        if (selectedCard.id == "0000") {
          this.props.stripe
            .createToken()
            .then(result => {
              if (result.error) {
                result.error.message =
                  result.error.message == "Your card number is incomplete."
                    ? this.props.intl.formatMessage({
                      id: "team-settings.payment-method.creation.form.validation.card-no.empty",
                      defaultMessage: result.error.message,
                    })
                    : result.error.message;
                this.setState({
                  errorMessage: result.error.message,
                  btnSubmitQuery: "",
                });
              } else if (result.token) {
                this.sendData(result.token.card.brand, result.token.id, "");
              }
            })
            .catch(err => {
              this.setState({ errorMessage: err.message, btnSubmitQuery: "" });
            });
        } else {
          this.sendData("", "", selectedCard.id);
        }
      });
    } else {
    }
  };
  /**
   * It is to send card details and payment details to save in server
   * @param {string} brand
   * @param {string} evt
   * @param {string} evt
   */
  sendData = (brand, tokenId, cardId) => {
    const { teamId, profile } = this.props;
    const { subscription, totalMembers, promocodePriceValue, planType } = this.state;

    const {
      teams,
      activeTeam,
    } = profile; /** if user is upgrading from bussiness trial plan to premium then task status mapping modal should open, so the user can updade task statuses without any status conflits */
    const { subscriptionDetails } = teams.find(t => t.companyId == activeTeam) || {};
    const { paymentPlanTitle } = subscriptionDetails;
    let isStatusMappingRequired =
      paymentPlanTitle === "BusinessTrial" && planType === "Premium" ? true : false;

    let data = {};
    data.companyId = teamId;
    data.cardName = brand;
    data.email = "";
    data.phone = "";
    data.userPlan = subscription;
    data.stripeToken = tokenId;
    data.promoCode = isEmpty(promocodePriceValue) ? "" : promocodePriceValue.id;
    data.planType = planType;
    data.amount = this.getTotalPayment("PromoCode").toFixed(2);
    data.users = totalMembers;
    data.paymentMethodId = cardId;
    data.isStatusMappingRequired = isStatusMappingRequired;

    this.props.UpgradePremiumPlan(
      data,
      result => {
        if (result.data.status === "Success") {
          const { unitPriceAnually, unitPriceMonthly } = result.data.company.paymentPlan;
          const { packageType, paymentPlanTitle, numberOfUsers } = result.data.company.subscriptionDetails;
          const coupon = result.data.paymentMethodId?.discount?.coupon;
          let invoice = result.data.paymentMethodId.latest_invoice;

          gtag("event", "purchase", {
            transaction_id: invoice.charge,
            value: this.getTotalPayment("PromoCode").toFixed(2),
            currency: "USD",
            coupon: coupon ? coupon.name : '',
            items: [
              // If someone purchases more than one item, 
              // you can add those items to the items array
              {
                item_name: `${paymentPlanTitle} ${packageType}`,
                coupon: coupon ? coupon.name : '',
                discount: coupon ? (this.getTotalPayment("all").toFixed(2) * (coupon.percent_off / 100)).toFixed(2) : '',
                item_category: paymentPlanTitle,
                item_category2: packageType,
                price: this.getTotalPayment("all").toFixed(2) / numberOfUsers,
                quantity: numberOfUsers
              }]
          });
          if (ENV == "production") {
            (function (t, a, p) {
              t.TapfiliateObject = a;
              t[a] =
                t[a] ||
                function () {
                  (t[a].q = t[a].q || []).push(arguments);
                };
            })(window, "tap");

            tap("create", "15762-607f0b", { integration: "stripe" });
            // tap("detect", { cookie_domain: "ntaskmanager.com" });
            tap("conversion", invoice.charge, data.amount, { customer_id: invoice.customer });
            // if (window.userpilot) {
            //   window.userpilot.identify(
            //     profile.userId, // Used to identify users
            //     {
            //       name: profile.fullName, // Full name
            //       email: profile.email, // Email address
            //       created_at: moment(profile.createdDate, "YYYY-MM-DDThh:mm:ss").format("x"),
            //       plan: result.data.company.paymentPlan.planTitle,
            //     }
            //   );
            // }
          }
          this.setState({
            planData: {
              companyInfo: result.data.company,
            },
            isStatusMappingRequiredModal: isStatusMappingRequired,
            mode: "success",
          });
        } else {
          this.setState({ mode: "fail" });
        }
      },
      fail => {
        this.props.showSnackBar(fail.data);
        this.setState({ mode: "bill", btnSubmitQuery: "" });
      }
    );
  };
  /**
   * It is to change on click promo button
   */
  clickToAddPromo = () => {
    this.setState({ promocodeState: "inProgress" });
  };
  /**
   * It is for add promo value that user enter
   */
  changePromocode = event => {
    this.setState({ promocodeValue: event.target.value });
  };
  /**
   * It is for promo verification and get value against user added promocode
   * @param {event} event
   * @param {string} btnLbl
   */
  promocodeHandler = (event, btnLbl) => {
    const { promocodeState, promocodeValue, subscription, planType, totalMembers } = this.state;
    let data = {};
    data.userPlan = subscription;
    data.promoCode = promocodeValue;
    data.planType = planType
    data.quantity = totalMembers;
    data.email = this.props.profile.email;
    if (promocodeState == "inProgress") {
      this.setState({ btnPromoQuery: "progress" }, () => {
        this.props.FetchPromotionCode(
          data,
          response => {
            if (response.data.valid) {
              this.setState({
                btnPromoQuery: "",
                promocodeState: "done",
                promocodePriceValue: response.data,
              });
            } else {
              this.props.showSnackBar("PromoCode not valid");
              this.setState({ btnPromoQuery: "" });
            }
          },
          fail => {
            this.props.showSnackBar(fail.data);
            this.setState({ btnPromoQuery: "" });
          }
        );
      });
    } else if (promocodeState == "done") {
      this.setState({
        promocodeState: "initial",
        promocodeValue: "",
        promocodePriceValue: {},
      });
    }
  };
  /**
   * It is for remove promo code
   * @param {event} event
   */
  clickToRemovePromo = event => {
    this.setState({ promocodeState: "inProgress" });
  };
  /**
   * It is to change on plan either annully or monthly
   * @param {event} event
   * @param {string} newPaymentPlan
   */
  handleSubscriptionChange = (event, newPaymentPlan) => {
    if (newPaymentPlan) {
      this.setState({ subscription: newPaymentPlan });
      this.removePromoOnPlanChange();
    }
  };
  /**
   * it is called when user change plan from toggle or dropdown
   */
  removePromoOnPlanChange = () => {
    this.setState({
      promocodeState: "initial",
      promocodeValue: "",
      promocodePriceValue: {},
    });
  };
  /**
   * It is for when user change members
   * @param {event} event
   */
  teamMembersChangeHandler = event => {
    this.setState({ totalMembers: event.target.value });
  };
  /**
   * It is for when user change members on focusout and set valu according to minimum value
   * @param {event} event
   */
  teamMembersBlurHandler = event => {
    if (event.target.value > this.maximumUser) {
      this.setState({ totalMembers: this.maximumUser });
    }
    else if (event.target.value < this.minimumUser) {
      this.setState({ totalMembers: this.minimumUser });
    }
    else {
      this.setState({ totalMembers: event.target.value });
    }
  };
  /**
   * It is for when user enter greater value in input field of members controll
   * @param {event} event
   */
  teamMembersInputHandler = event => {
    if (event.target.value.length > event.target.maxLength)
      event.target.value = event.target.value.slice(0, event.target.maxLength);
  };
  /**
   * It is for getting total payment  according to tiers plans with include promo value
   * @param {string} mode
   */
  getTotalPayment = mode => {
    const { subscription, totalMembers, planPrice, planType, promocodePriceValue } = this.state;
    let pricePlanType = planType.toLowerCase();
    let billCalculatePriceValue = 0;
    const monthMultiplier = subscription === "annually" ? 12 : 1;
    if (totalMembers <= 3) {
      if (false) {
        billCalculatePriceValue =
          (planPrice[pricePlanType][subscription]["tiers"][0]["unit_amount"] / 100) *
          totalMembers *
          monthMultiplier;
      } else if (pricePlanType == "business" || pricePlanType == "premium") {
        billCalculatePriceValue =
          (planPrice[pricePlanType][subscription]["amount"] / 100) * totalMembers * monthMultiplier;
      }
    } else {
      if (false) {
        billCalculatePriceValue =
          (planPrice[pricePlanType][subscription]["tiers"][0]["unit_amount"] / 100) *
          3 *
          monthMultiplier +
          (planPrice[pricePlanType][subscription]["tiers"][1]["unit_amount"] / 100) *
          (totalMembers - 3) *
          monthMultiplier;
      } else if (pricePlanType == "business" || pricePlanType == "premium") {
        billCalculatePriceValue =
          (planPrice[pricePlanType][subscription]["amount"] / 100) * 3 * monthMultiplier +
          (planPrice[pricePlanType][subscription]["amount"] / 100) *
          (totalMembers - 3) *
          monthMultiplier;
      }
    }
    if (mode == "PromoCode" && !isEmpty(promocodePriceValue)) {
      return promocodePriceValue.amountOff
        ? billCalculatePriceValue - (promocodePriceValue.amountOff / 100)
        : billCalculatePriceValue -
        billCalculatePriceValue * (promocodePriceValue.percentOff / 100);
    } else {
      return billCalculatePriceValue;
    }
  };
  /**
   * It is for getting total payment  according to tiers plans with include promo value
   * @param {string} mode
   */
  getActualAnnual = mode => {
    const { subscription, totalMembers, planPrice, planType, promocodePriceValue } = this.state;
    let pricePlanType = planType.toLowerCase();
    let billCalculatePriceValue = 0;
    const monthMultiplier = 12;
    if (totalMembers <= 3) {
      if (false) {
        billCalculatePriceValue =
          (planPrice[pricePlanType]["monthly"]["tiers"][0]["unit_amount"] / 100) *
          totalMembers *
          monthMultiplier;
      } else if (pricePlanType == "business" || pricePlanType == "premium") {
        billCalculatePriceValue =
          (planPrice[pricePlanType]["monthly"]["amount"] / 100) * totalMembers * monthMultiplier;
      }
    } else {
      if (false) {
        billCalculatePriceValue =
          (planPrice[pricePlanType]["monthly"]["tiers"][0]["unit_amount"] / 100) *
          3 *
          monthMultiplier +
          (planPrice[pricePlanType]["monthly"]["tiers"][1]["unit_amount"] / 100) *
          (totalMembers - 3) *
          monthMultiplier;
      } else if (pricePlanType == "business" || pricePlanType == "premium") {
        billCalculatePriceValue =
          (planPrice[pricePlanType]["monthly"]["amount"] / 100) * 3 * monthMultiplier +
          (planPrice[pricePlanType]["monthly"]["amount"] / 100) *
          (totalMembers - 3) *
          monthMultiplier;
      }
    }
    return billCalculatePriceValue;
  };
  /**
   * It is for getting total payment  according to tiers plans without include promo value for display
   * @param {string} tierFor
   */
  getPayment = tierFor => {
    const { subscription, totalMembers, planPrice, planType, promocodePriceValue } = this.state;
    let pricePlanType = planType.toLowerCase();
    let billCalculatePriceValue = 0;
    const monthMultiplier = subscription === "annually" ? 12 : 1;
    if (tierFor == 3) {
      if (false) {
        billCalculatePriceValue =
          (planPrice[pricePlanType][subscription]["tiers"][0]["unit_amount"] / 100) *
          (totalMembers <= 3 ? totalMembers : 3) *
          monthMultiplier;
      } else if (pricePlanType == "business") {
        billCalculatePriceValue =
          (planPrice[pricePlanType][subscription]["amount"] / 100) *
          (totalMembers <= 3 ? totalMembers : 3) *
          monthMultiplier;
      }
    } else if (tierFor == "all") {
      billCalculatePriceValue =
        (planPrice[pricePlanType][subscription]["amount"] / 100) * totalMembers * monthMultiplier;
    } else {
      if (false) {
        billCalculatePriceValue =
          (planPrice[pricePlanType][subscription]["tiers"][1]["unit_amount"] / 100) *
          (totalMembers - 3) *
          monthMultiplier;
      } else if (pricePlanType == "business") {
        billCalculatePriceValue =
          (planPrice[pricePlanType][subscription]["amount"] / 100) *
          (totalMembers - 3) *
          monthMultiplier;
      }
    }
    return billCalculatePriceValue;
  };
  /**
   * It is for successfull payment done then update the store
   */
  successfullyExit = () => {
    this.setState({ mode: "" });
    if (this.props.paymentPaidHandler) this.props.paymentPaidHandler(this.state.planData);
  };
  /**
   * It is for unsuccessfull payment done then reopen the bill mode
   */
  unSuccessfullyExit = () => {
    this.setState({ mode: "bill" });
  };
  /**
   * It is for when user change the plan from permium/business or viseversa
   * @param {string} type
   * @param {Object} item
   */
  handlePlanTypeSelect = (type, item) => {
    let { totalMembers } = this.state;
    if (Constants.isPremiumPlan(item.id) && this.minimumUser < 3) {
      this.minimumUser = this.props.minimumUser;
      totalMembers = totalMembers < this.minimumUser ? this.minimumUser : totalMembers;
    } else {
      this.minimumUser = this.props.minimumUser;
    }
    this.setState({ [type]: item.id, totalMembers: totalMembers });
    // this.removePromoOnPlanChange();
  };
  /**
   * It is for when you select the card form selection control
   * @param {string} type
   * @param {string} item
   */
  handleCardSelect = (type, item) => {
    this.setState({ [type]: item });
  };
  /**
   * It is for arrange the cards
   */
  generatePlans = () => {
    let plans = [
      {
        label: this.props.intl.formatMessage({
          id: "team-settings.billing.biling-plan-dialog.premimum-label",
          defaultMessage: "Premium",
        }),
        id: "premium",
      },
      {
        label: this.props.intl.formatMessage({
          id: "team-settings.billing.biling-plan-dialog.business-label",
          defaultMessage: "Business",
        }),
        id: "business",
      },
    ];
    let ddData = plans.map((r, i) => {
      return generateSelectData(r.label, r.id, r.id, null, r);
    });
    return ddData;
  };
  /**
   * It is for create the cards data according to selection control
   */
  generateCards = () => {
    let { cards } = { ...this.state };
    let ddData = cards.map((item, index) => {
      return generateSelectData(item.label, item.id, item.id, null, item);
    });
    return ddData;
  };
  /**
   * It is called when button label change according to mode define
   * @param {string} mode
   */
  toggleBtnPriceLbl = mode => {
    const { planPrice, planType } = this.state;
    let pricePlanType = planType.toLowerCase();
    // if (Constants.isPremiumPlan(planType)) {
    if (false) {
      return planPrice[pricePlanType][mode]["tiers"][0]["unit_amount"] / 100;
    } else {
      return planPrice[pricePlanType][mode]["amount"] / 100;
    }
  };
  /**
   * It is for display label for price details
   */
  calculationLbl = () => {
    const { planPrice, planType, subscription, totalMembers } = this.state;
    let pricePlanType = planType.toLowerCase();
    const monthMultiplier = subscription === "annually" ? 12 : 1;
    const monthlbl = this.props.intl.formatMessage({
      id: "team-settings.billing.bill-dialog.months-label",
      defaultMessage: "months",
    });
    // if (Constants.isPremiumPlan(planType)) {
    if (false) {
      return `($${planPrice[pricePlanType][subscription]["tiers"][0]["unit_amount"] /
        100} x ${totalMembers} x ${monthMultiplier} ${monthlbl})`;
    } else {
      return `($${planPrice[pricePlanType][subscription]["amount"] /
        100} x ${totalMembers} x ${monthMultiplier} ${monthlbl}`;
    }
  };
  /**
   * It is to show/hide bill details
   */
  showBillHandler = () => {
    this.setState((prevState, props) => {
      return {
        showBill: !prevState.showBill,
      };
    });
  };
  /**
   * It is for display calculation for user according to plan tiers
   */
  calculationDisplay = () => {
    const { subscription, totalMembers, planPrice, planType } = this.state;
    const { classes } = this.props;
    let pricePlanType = planType.toLowerCase();
    const monthMultiplier = subscription === "annually" ? 12 : 1;
    const months =
      subscription === "annually"
        ? this.props.intl.formatMessage({
          id: "team-settings.billing.bill-dialog.months-label",
          defaultMessage: "months",
        })
        : this.props.intl.formatMessage({
          id: "team-settings.billing.bill-dialog.month-label",
          defaultMessage: "month",
        });
    // if (totalMembers <= 3) {
    if (false) {
      return (
        <div className={classes.showBillDetails}>
          <p className={classes.showBillDetailsLbl}>
            <Typography variant="body2">
              ${" "}
              {Constants.isPremiumPlan(planType)
                ? planPrice[pricePlanType][subscription]["tiers"][0]["unit_amount"] / 100
                : planPrice[pricePlanType][subscription]["amount"] / 100}{" "}
              x {totalMembers <= 3 ? totalMembers : 3} x {monthMultiplier} {months}
            </Typography>
            <Typography variant="body2">${this.getPayment(3).toFixed(2)}</Typography>
          </p>
        </div>
      );
    } else if (false) {
      // } else if (Constants.isPremiumPlan(planType)) {
      return (
        <div className={classes.showBillDetails}>
          <p className={classes.showBillDetailsLbl}>
            <Typography variant="body2">
              $ {planPrice[pricePlanType][subscription]["tiers"][0]["unit_amount"] / 100} x{" "}
              {totalMembers <= 3 ? totalMembers : 3} x {monthMultiplier} {months}
            </Typography>
            <Typography variant="body2">${this.getPayment(3).toFixed(2)}</Typography>
          </p>
          <p className={classes.showBillDetailsLbl}>
            <Typography variant="body2">
              $ {planPrice[pricePlanType][subscription]["tiers"][1]["unit_amount"] / 100} x{" "}
              {totalMembers - 3} x {monthMultiplier} {months}
            </Typography>
            <Typography variant="body2">${this.getPayment().toFixed(2)}</Typography>
          </p>
        </div>
      );
    } else {
      return (
        <div className={classes.showBillDetails}>
          <p className={classes.showBillDetailsLbl}>
            <Typography variant="body2">
              $ {planPrice[pricePlanType][subscription]["amount"] / 100} x {totalMembers} x{" "}
              {monthMultiplier} {months}
            </Typography>
            <Typography variant="body2">${this.getPayment("all").toFixed(2)}</Typography>
          </p>
        </div>
      );
    }
  };
  closeMappingStatus = () => {
    this.setState({ isMappingStatusRequired: false, mode: "" });
  };
  planExpire = mode => {
    if (mode == "StatusMapping") {
      this.setState({ isMappingStatusRequired: true });
    }
  };

  render() {
    const { classes, theme } = this.props;
    const {
      paymentType,
      promocodeState,
      promocodeValue,
      promocodePriceValue,
      subscription,
      totalMembers,
      mode,
      btnSubmitQuery,
      loading,
      selectedCard,
      btnPromoQuery,
      planType,
      showBill,
      isStatusMappingRequiredModal,
    } = this.state;
    const promoCodeInput = promocodeState == "done" ? true : false;
    const promoCodeBtnType = promocodeState == "done" ? "dangerText" : "gray";
    const showActualAnnual =
      subscription === "annually" &&
        ((planType.toLowerCase() == "premium") ||
          planType.toLowerCase() == "business")
        ? true
        : false;
    // const showActualAnnual =
    //   subscription === "annually" &&
    //   ((planType.toLowerCase() == "premium" && totalMembers > 3) ||
    //     planType.toLowerCase() == "business")
    //     ? true
    //     : false;

    const promoCodeBtnLbl =
      promocodeState == "done" ? (
        <FormattedMessage
          id="team-settings.billing.bill-dialog.remove-button.label"
          defaultMessage="Remove"
        />
      ) : (
        <FormattedMessage
          id="team-settings.billing.bill-dialog.apply-button.label"
          defaultMessage="Apply"
        />
      );
    const submitBtnLbl =
      paymentType == "creditcard" ? (
        <FormattedMessage
          id="team-settings.billing.upgrade-button.label"
          defaultMessage="Upgrade"
        />
      ) : (
        <FormattedMessage
          id="team-settings.billing.bill-dialog.paypal-button.label"
          defaultMessage="Continue to PayPal"
        />
      );
    const billDispLbl =
      subscription === "annually" ? (
        <FormattedMessage
          id="team-settings.billing.biling-plan-dialog.anual-button.label"
          defaultMessage="Billed Annually"
        />
      ) : (
        <FormattedMessage
          id="team-settings.billing.biling-plan-dialog.monthly-button.label"
          defaultMessage="Billed Monthly"
        />
      );
    let billCalculateLbl = "-";
    let billCalculatePriceValue = 0;
    let billActualAnnualPriceValue = 0;
    let annuallPriceLbl = "-";
    let monthlyPriceLbl = "-";
    let totalPriceValue = 0;
    if (!loading) {
      billCalculateLbl = this.calculationLbl();
      billActualAnnualPriceValue = this.getActualAnnual();
      billCalculatePriceValue = this.getTotalPayment("Price");
      totalPriceValue = this.getTotalPayment("PromoCode");
      annuallPriceLbl = this.toggleBtnPriceLbl("annually");
      monthlyPriceLbl = this.toggleBtnPriceLbl("monthly");
    }
    let promoValue = isEmpty(promocodePriceValue)
      ? `-`
      : promocodePriceValue.amountOff
        ? `-($${promocodePriceValue.amountOff / 100})`
        : `-(${promocodePriceValue.percentOff}%)`;
    let savePercentage = isEmpty(promocodePriceValue) ? `33` :
      promocodePriceValue.amountOff ? `${promocodePriceValue.amountOff / 100}`
        : `${promocodePriceValue.percentOff}`;
    return (
      <Fragment>
        {mode == "success" &&
          (isStatusMappingRequiredModal ? (
            <MappingStatusModal
              showSnackBar={this.props.showSnackBar}
              closeMappingStatus={this.closeMappingStatus}
              createTeam={this.closeMappingStatus}
              hideLoadingState={this.closeMappingStatus}
              showLoadingState={this.closeMappingStatus}
              planExpire={this.planExpire}
              switchExpirePlan={() => { }}
            />
          ) : (
            <AddBillSuccess
              successfullyExit={this.successfullyExit}
              mode={planType}></AddBillSuccess>
          ))}
        {mode === "fail" && <AddBillDenied unSuccessfullyExit={this.unSuccessfullyExit} />}
        {mode == "bill" && (
          <Dialog
            classes={{
              paper: classes.dialogPaperCnt,
              scrollBody: classes.dialogCnt,
            }}
            fullWidth={true}
            scroll="body"
            disableBackdropClick={true}
            disableEscapeKeyDown={true}
            open={true}
            onClose={() => { }}>
            <DialogTitle
              id="form-dialog-title"
              classes={{ root: classes.addBillDefaultDialogTitle }}>
              <div className={classes.addBillTitle}>
                <Typography variant="h2">
                  <FormattedMessage
                    id="team-settings.billing.bill-dialog.title"
                    defaultMessage="Add Bill Details"
                  />
                </Typography>
                <IconButton btnType="transparent" onClick={this.props.exitBillDetail}>
                  <CloseIcon htmlColor={theme.palette.secondary.medDark} />
                </IconButton>
              </div>
            </DialogTitle>
            <DialogContent className={classes.addBillContent}>
              <div className={classes.paymentSummary}>
                <ToggleButtonGroup
                  size="small"
                  exclusive
                  onChange={this.handleSubscriptionChange}
                  value={subscription}
                  classes={{ root: classes.toggleBtnGroup }}>
                  {/* <ToggleButton
                                            key={1}
                                            value="annually"
                                            classes={{
                                                root: classes.toggleButton,
                                                selected: classes.toggleButtonSelected
                                            }}
                                            >
                                                <div className={classes.toggleButtonMainDiv}>
                                                    <div className={classes.toggleButtonOfferDiv}>Limited Time</div>
                                                    <span className={classes.planTypeLbl}>Billed Annually<div className={classes.saleOff} style={{marginLeft: 5}}><span>Save 33%</span></div></span>
                                                    <div className={classes.planTypeCnt} style={{display: 'none'}}>
                                                        <span className={classes.planPriceStyle}>
                                                            <span style={{fontSize: "14px !important"}}>$</span>{annuallPriceLbl}<span style={{fontSize: "13px !important", marginLeft: 5}}>User/Month</span></span>
                                                    </div>
                                                </div>
                                        </ToggleButton>

                                        <ToggleButton
                                            key={2}
                                            value="monthly"
                                            classes={{
                                                root: classes.toggleButton,
                                                selected: classes.toggleButtonSelected
                                            }}
                                            >
                                                <div className={classes.toggleButtonMainDiv}>
                                                    <span className={classes.planTypeLbl}>Billed Monthly</span>
                                                    <span  className={classes.planPriceStyle} style={{display: 'none'}}>
                                                        <span style={{fontSize: "14px !important"}}>$</span>{monthlyPriceLbl}<span style={{fontSize: "13px !important", marginLeft: 5}}>User/Month</span></span>
                                                </div>
                                        </ToggleButton> */}
                  <ToggleButton
                    key={1}
                    value="monthly"
                    classes={{
                      root: classes.toggleButton,
                      selected: classes.toggleButtonSelected,
                    }}>
                    <div className={classes.toggleButtonMainDiv}>
                      {/* <div className={classes.toggleButtonOfferDiv}>Limited Time</div> */}
                      <span className={classes.planTypeLbl}>
                        <FormattedMessage
                          id="team-settings.billing.biling-plan-dialog.monthly-button.label"
                          defaultMessage="Billed Monthly"
                        />
                      </span>
                    </div>
                  </ToggleButton>

                  <ToggleButton
                    key={2}
                    value="annually"
                    classes={{
                      root: classes.toggleButton,
                      selected: classes.toggleButtonSelected,
                    }}>
                    <div className={classes.toggleButtonMainDiv}>
                      <span className={classes.planTypeLbl}>
                        <FormattedMessage
                          id="team-settings.billing.biling-plan-dialog.anual-button.label"
                          defaultMessage="Billed Annually"
                        />
                      </span>
                      <div className={classes.saleOff} style={{ marginLeft: 5 }}>
                        <span>

                          {/* {savePercentage != "-" && subscription != "monthly" ? (
                            `Save ${savePercentage}%`
                          ) : null} */}

                          <FormattedMessage
                            id="team-settings.billing.biling-plan-dialog.anual-button.sub-label"
                            defaultMessage="Save 33%"
                          />
                        </span>
                      </div>
                    </div>
                  </ToggleButton>
                </ToggleButtonGroup>
                <div className={classes.teammemberCnt}>
                  <Typography variant="h5"
                    className={classes.fontSize}
                    style={{ fontSize: "13px" }}>
                    <FormattedMessage
                      id="team-settings.billing.bill-dialog.subscription-plan-label"
                      defaultMessage="Subscription Plan"
                    />
                  </Typography>
                  <div style={{ width: 110 }}>
                    <SelectSearchDropdown
                      data={() => {
                        return this.generatePlans();
                      }}
                      styles={{ margin: 0, marginRight: 20, flex: 1 }}
                      isClearable={false}
                      label=""
                      selectChange={this.handlePlanTypeSelect}
                      type="planType"
                      selectedValue={this.generatePlans().find(d => {
                        return d.id.toLowerCase() == planType.toLowerCase();
                      })}
                      placeholder={""}
                      isMulti={false}
                    />
                  </div>
                </div>
                <div className={classes.teammemberCnt}>
                  <Typography variant="h5"
                    className={classes.fontSize}
                    style={{ fontSize: "13px" }}>
                    <FormattedMessage
                      id="team-settings.billing.bill-dialog.no-teams-label"
                      defaultMessage="No. of team members"
                    />
                  </Typography>
                  <Input
                    type="number"
                    className={classes.teammembers}
                    disableUnderline={true}
                    onInput={this.teamMembersInputHandler}
                    inputProps={{
                      min: this.minimumUser,
                      max: this.maximumUser,
                      maxLength: 3,
                      step: 1,
                    }}
                    value={totalMembers}
                    onBlur={this.teamMembersBlurHandler}
                    onChange={this.teamMembersChangeHandler}
                    style={{ width: 110 }}
                  />
                </div>
                <div className={classes.annualBillCnt}>
                  <div style={{ display: "flex", alignItems: "baseline" }}>
                    <Typography variant="h5"
                      className={classes.fontSize}
                      style={{ fontSize: "13px" }}>
                      {billDispLbl}{" "}
                    </Typography>
                    {showActualAnnual ? (
                      <Typography
                        variant="h6"
                        className={classes.fontSize}
                        style={{
                          fontSize: "13px",
                          color: theme.palette.text.brightGreen,
                          marginLeft: 5,
                        }}>
                        {" "}
                        (Save 33%)
                      </Typography>
                    ) : null}
                    {!loading && (
                      <a
                        href="#"
                        onClick={this.showBillHandler}
                        className={classes.linkStyle}
                        style={{ marginLeft: 5, paddingTop: 3 }}>
                        <FormattedMessage
                          id="team-settings.billing.bill-dialog.show-detail.label"
                          defaultMessage="Show Detail"
                        />
                      </a>
                    )}
                    {/* <Typography  variant="body2">&nbsp;{billCalculateLbl}</Typography> */}
                  </div>
                  <div style={{ display: "flex" }}>
                    {showActualAnnual ? (
                      <Typography variant="h5" style={{ textDecoration: "line-through" }}>
                        ${billActualAnnualPriceValue.toFixed(2)}
                      </Typography>
                    ) : null}
                    <Typography variant="h5" style={{ marginLeft: 10 }}>
                      ${billCalculatePriceValue.toFixed(2)}
                    </Typography>
                  </div>
                </div>
                {showBill && this.calculationDisplay()}
                <div className={classes.promocodeDiscoutnCnt}>
                  <Typography variant="h5"
                    className={classes.fontSize}
                    style={{ fontSize: "13px" }}>
                    <FormattedMessage
                      id="team-settings.billing.bill-dialog.promo-code-label"
                      defaultMessage="Promo Code"
                    />{" "}
                    <span className={classes.promoPriceLbl}>
                      (
                      <FormattedMessage
                        id="team-settings.billing.bill-dialog.applicable-label"
                        defaultMessage="Applicable for the first year only"
                      />
                      )
                    </span>
                  </Typography>
                  <div style={{ display: "flex" }}>
                    {promoValue != "-" && subscription != "monthly" ? (
                      <Typography variant="h5">Additional</Typography>
                    ) : null}
                    <Typography
                      variant="h5"
                      style={{ color: theme.palette.text.danger, marginLeft: 5 }}>
                      {`${promoValue}`}
                    </Typography>
                  </div>
                </div>
                <Divider style={{ marginTop: 22 }} />
                <div className={classes.userBillCnt}>
                  <Typography
                    variant="h6"
                    className={classes.fontSize}
                    style={{
                      fontSize: "13px",
                      color: theme.palette.text.brightGreen,
                    }}>
                    <FormattedMessage
                      id="team-settings.billing.bill-dialog.pay-label"
                      defaultMessage="You will pay"
                    />
                  </Typography>
                  <Typography variant="h5" style={{ color: theme.palette.text.brightGreen }}>
                    ${totalPriceValue.toFixed(2)}
                  </Typography>
                </div>
              </div>
              <Divider />
              <div className={classes.paymentDetail}>
                <Typography variant="h4">
                  <FormattedMessage
                    id="team-settings.billing.bill-dialog.payment-details.label"
                    defaultMessage="Payment Details"
                  />
                </Typography>
                <div style={{ marginTop: 10 }}>
                  <SelectSearchDropdown
                    data={() => {
                      return this.generateCards();
                    }}
                    styles={{ margin: 0, marginRight: 20, flex: 1 }}
                    isClearable={false}
                    label=""
                    selectChange={this.handleCardSelect}
                    type="selectedCard"
                    selectedValue={selectedCard}
                    placeholder={""}
                    isMulti={false}
                  />
                </div>
                {selectedCard.id == "0000" ? (
                  <Fragment>
                    <FormControl component="fieldset" className={classes.formControl}>
                      <RadioGroup
                        className={classes.cardRadiogroup}
                        aria-label="card"
                        name="card"
                        value={paymentType}
                        onChange={this.cardTypeChange}>
                        <FormControlLabel
                          value="creditcard"
                          control={<Radio />}
                          label={
                            <div className={classes.creditCardRadioDiv}>
                              <span className={classes.radioBtnLbl} style={{ paddingRight: 10 }}>
                                <FormattedMessage
                                  id="team-settings.billing.bill-dialog.credit-card-label"
                                  defaultMessage="Credit Card"
                                />
                              </span>
                              <img
                                src={creditCardLogo}
                                alt="credit card details"
                                className={classes.creditCard}
                              />
                            </div>
                          }></FormControlLabel>
                        {/* <FormControlLabel value="paypal" disabled control={<Radio />} label={<div><span className={classes.radioBtnLbl} style={{display: 'inline'}}>PayPal</span><span className={classes.comingsoon}>coming soon...</span></div>} /> */}
                      </RadioGroup>
                    </FormControl>
                    <form>
                      <div
                        className={this.state.errorMessage.length ? classes.error : ""}
                        role="alert">
                        {this.state.errorMessage}
                      </div>
                      {paymentType == "creditcard" ? (
                        <Fragment>
                          <label>
                            <span className={classes.lblStyle}>
                              <FormattedMessage
                                id="team-settings.payment-method.creation.form.card-no.label"
                                defaultMessage="Card number"
                              />
                            </span>
                            <CardNumberElement
                              className={classes.cardNumberCnt}
                              onChange={this.handleChange}
                            />
                          </label>
                          <div className={classes.container}>
                            <label>
                              <span className={classes.lblStyle}>
                                <FormattedMessage
                                  id="team-settings.payment-method.creation.form.expiry-date.label"
                                  defaultMessage="Expiration date"
                                />
                              </span>
                              <CardExpiryElement
                                className={classes.expiryElement}
                                onChange={this.handleChange}
                              />
                            </label>
                            <label>
                              <span className={classes.lblStyle}>
                                <FormattedMessage
                                  id="team-settings.payment-method.creation.form.cvc.label"
                                  defaultMessage="CVC"
                                />
                              </span>
                              <CardCVCElement
                                className={classes.cvcElement}
                                onChange={this.handleChange}
                              />
                            </label>
                          </div>
                        </Fragment>
                      ) : null}
                    </form>
                  </Fragment>
                ) : null}
                {promocodeState == "initial" ? (
                  <div style={{ marginTop: 15 }}>
                    <a href="#" onClick={this.clickToAddPromo} className={classes.linkStyle}>
                      <FormattedMessage
                        id="team-settings.billing.bill-dialog.add-promo-label"
                        defaultMessage="Add promo code"
                      />
                    </a>
                  </div>
                ) : (
                  <div className={classes.promocodeValueCnt}>
                    <div style={{ width: "45%" }}>
                      <span className={classes.lblStyle} style={{ marginBottom: 9 }}>
                        <FormattedMessage
                          id="team-settings.billing.bill-dialog.promo-code-label"
                          defaultMessage="Promo Code"
                        />
                      </span>
                      <DefaultTextField
                        label={false}
                        fullWidth={true}
                        error={false}
                        errorState={false}
                        formControlStyles={{ marginBottom: 0 }}
                        defaultProps={{
                          id: "promocode",
                          type: "text",
                          onChange: e => this.changePromocode(e),
                          value: promocodeValue,
                          placeholder: "xxxx",
                          inputProps: {
                            maxLength: 80,
                            disabled: promoCodeInput,
                          },
                        }}
                      />
                    </div>
                    <CustomButton
                      btnType={promoCodeBtnType}
                      variant="contained"
                      style={{
                        marginBottom: 0,
                        width: 110,
                        height: 40,
                        marginLeft: "10%",
                      }}
                      onClick={event => this.promocodeHandler(event, promoCodeBtnLbl)}
                      query={btnPromoQuery}
                      disabled={btnPromoQuery === "progress"}>
                      {promoCodeBtnLbl}
                    </CustomButton>
                  </div>
                )}
                {paymentType == "paypal" ? (
                  <Typography variant="body2" style={{ marginTop: 10 }}>
                    <FormattedMessage
                      id="team-settings.billing.bill-dialog.connect-to-label"
                      defaultMessage="Please click the button below to connect your account to
                    Paypal."
                    />
                    <br />
                    <FormattedMessage
                      id="team-settings.billing.bill-dialog.no-money-label"
                      defaultMessage="No money is being transferred at this point."
                    />
                  </Typography>
                ) : null}
              </div>
              <div style={{ marginLeft: 20 }}>
                <div>
                  <CustomButton
                    btnType="blue"
                    variant="contained"
                    style={{ marginBottom: 0, marginRight: 15, minWidth: 120 }}
                    onClick={this.handleSubmit}
                    query={btnSubmitQuery}
                    disabled={btnSubmitQuery === "progress"}>
                    {submitBtnLbl}
                  </CustomButton>
                  <Link
                    className={classes.linkStyle}
                    to="/"
                    onClick={e => e.preventDefault()}
                    style={{ display: "none" }}>
                    <FormattedMessage
                      id="team-settings.billing.bill-dialog.do-it.label"
                      defaultMessage="Do it later"
                    />
                  </Link>
                </div>
                <Typography variant="body2" style={{ marginTop: 10 }}>
                  <FormattedMessage
                    id="team-settings.billing.bill-dialog.by-cont-label"
                    defaultMessage="By continuing, you agree to our"
                  />{" "}
                  <a
                    href="https://www.ntaskmanager.com/terms-conditions/"
                    className={classes.linkStyle}
                    target="_blank">
                    <FormattedMessage
                      id="team-settings.billing.bill-dialog.term-cond-label"
                      defaultMessage="terms and conditions."
                    />
                  </a>
                </Typography>
              </div>
            </DialogContent>
          </Dialog>
        )}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    teamId: state.profile.data.activeTeam,
    promocode: state.profile.data.promocode,
    profile: state.profile.data,
  };
};
export default compose(
  withRouter,
  injectIntl,
  connect(mapStateToProps, {
    UpgradePremiumPlan,
    FetchPricePlans,
    FetchPaymentMethod,
    FetchPromotionCode,
  }),
  withStyles(classes, { withTheme: true })
)(injectStripe(AddBillDetail));
