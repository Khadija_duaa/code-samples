import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import classes from "./style";
import Typography from "@material-ui/core/Typography";
import CustomButton from "../../../components/Buttons/CustomButton";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import successIcon from "../../../assets/images/success_icon_tick.png";
import clsx from "clsx";
import PremiumCrownIcon from "../../../components/Icons/PremiumCrownIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import Constants from "../../../components/constants/planConstant";
import { FormattedMessage } from "react-intl";

class UpdateSuccess extends Component {
  constructor(props) {
    super(props);
  }
  /**
   * It is for if user bill payment successfull then this dialog appears
   */
  handleContineBtn = () => {
    const { successfullyExit = () => { } } = this.props;
    successfullyExit();
  }

  render() {
    let { classes } = this.props;

    return (
      <Dialog
        classes={{
          paper: classes.dialogSuccessPaperCnt,
          scrollBody: classes.dialogCnt,
        }}
        fullWidth={true}
        scroll="body"
        disableBackdropClick={true}
        disableEscapeKeyDown={true}
        open={true}
        onClose={() => { }}
      >
        <DialogContent
          className={clsx(
            classes.defaultDialogContent,
            classes.SuccessContentBack
          )}
        >
          <div className={classes.dialogBody}>
            <div className={classes.successIconContainer}>
              {/* <SvgIcon
                viewBox="0 0 27.998 23.35"
                style={{ width: 40, height: 34 }}
              >
                <BusinessBreifcaseIcon />
              </SvgIcon> */}
              <img
                src={successIcon}
                alt="Plan Image"
                className={classes.statusIcon}
              />
            </div>
            <Typography variant="h2" className={classes.successHeaderTitle}>
              Thank you for your request
            </Typography>
            <Typography variant="h2" className={classes.successHeaderCnt}>
              A customer support agent will reach out to you shortly for next steps.
            </Typography>

            <Typography variant="body2" className={classes.successBodyPlan}>
              For technical support, you can contact us at
            </Typography>
            <Typography variant="body2" className={classes.successSupportCnt}>
              <FormattedMessage
                id="common.team.team-subscription.support"
                defaultMessage="For technical support, you can contact us at"
              />{" "}
              <a
                href="mailto: support@ntaskmanager.com"
                className={classes.linkBtnStyle}
              >
                support@ntaskmanager.com.
              </a>
            </Typography>
            <div className={classes.successDialogFooter}>
              <CustomButton
                btnType="blue"
                variant="contained"
                style={{
                  textTransform: "none",
                  fontSize: "13px !important",
                  padding: "10px 24px",
                }}
                onClick={this.handleContineBtn}
              >
                <FormattedMessage
                  id="common.continue.label"
                  defaultMessage="Continue"
                />
              </CustomButton>
            </div>
          </div>
        </DialogContent>
      </Dialog>
    );
  }
}
export default compose(
  withRouter,
  withStyles(classes, { withTheme: true })
)(UpdateSuccess);
