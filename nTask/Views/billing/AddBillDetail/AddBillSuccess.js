import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import classes from "./style";
import Typography from "@material-ui/core/Typography";
import CustomButton from "../../../components/Buttons/CustomButton";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import successIcon from "../../../assets/images/success_icon_tick.png";
import clsx from "clsx";
import PremiumCrownIcon from "../../../components/Icons/PremiumCrownIcon";
import BusinessBreifcaseIcon from "../../../components/Icons/BusinessBreifcaseIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import Constants from "../../../components/constants/planConstant";
import { FormattedMessage } from "react-intl";
import { getColumnsList } from "../../../redux/actions/columns";

class AddBillSuccess extends Component {
  constructor(props) {
    super(props);
  }
  /**
   * It is for if user bill payment successfull then this dialog appears
   */
  handleContineBtn = () => {
    const { successfullyExit = () => { }, getColumnsList = () => { } } = this.props;
    getColumnsList(() => { /** calling columns API to get all the columns of the project and risk module if user have upgraded the team/workspace */
      successfullyExit();
    }, null,
      (err) => {
        successfullyExit();
      }
    );
  }

  render() {
    let { classes, mode } = this.props;

    let planType =
      Constants.PREMIUMTRIALPLAN == mode || Constants.BUSINESSTRIALPLAN == mode
        ? Constants.isPremiumTrial(mode)
          ? "Premium Trial"
          : "Business Trial"
        : Constants.isPremiumPlan(mode)
          ? "Premium"
          : "Business";
    return (
      <Dialog
        classes={{
          paper: classes.dialogSuccessPaperCnt,
          scrollBody: classes.dialogCnt,
        }}
        fullWidth={true}
        scroll="body"
        disableBackdropClick={true}
        disableEscapeKeyDown={true}
        open={true}
        onClose={() => { }}
      >
        <DialogContent
          className={clsx(
            classes.defaultDialogContent,
            classes.SuccessContentBack
          )}
        >
          <div className={classes.dialogBody}>
            <div className={classes.successIconContainer}>
              {planType == "Premium" ? (
                <SvgIcon
                  viewBox="0 0 34.001 22.879"
                  style={{ width: 46, height: 30 }}
                >
                  <PremiumCrownIcon />
                </SvgIcon>
              ) : (
                <SvgIcon
                  viewBox="0 0 27.998 23.35"
                  style={{ width: 40, height: 34 }}
                >
                  <BusinessBreifcaseIcon />
                </SvgIcon>
              )}
              {/* <img
                                        src={successIcon}
                                        alt="Plan Image"
                                        className={classes.statusIcon}
                                    /> */}
            </div>
            <Typography variant="h2" className={classes.successHeaderTitle}>
              <FormattedMessage
                id="common.team.team-subscription.title"
                defaultMessage=" Congratulations!"
              />
            </Typography>
            <Typography variant="h2" className={classes.successHeaderCnt}>
              <FormattedMessage
                id="common.team.team-subscription.message"
                defaultMessage="Your team subscription is successfully updated to"
              />
            </Typography>
            <Typography variant="h2" className={classes.successHeaderCntPlan}>
              <FormattedMessage
                id="common.team.team-subscription.plan"
                defaultMessage={`nTask ${planType} Plan`}
                values={{ planType: planType }}
              />
            </Typography>
            <Typography variant="body2" className={classes.successBodyPlan}>
              <FormattedMessage
                id="common.team.team-subscription.label"
                defaultMessage={`Enjoy unlimited access to all nTask ${planType.toLowerCase()} features.`}
                values={{ planType: planType }}
              />
            </Typography>
            <Typography variant="body2" className={classes.successSupportCnt}>
              <FormattedMessage
                id="common.team.team-subscription.support"
                defaultMessage="For technical support, you can contact us at"
              />{" "}
              <a
                href="mailto: support@ntaskmanager.com"
                className={classes.linkBtnStyle}
              >
                support@ntaskmanager.com.
              </a>
            </Typography>
            <div className={classes.successDialogFooter}>
              <CustomButton
                btnType="blue"
                variant="contained"
                style={{
                  textTransform: "none",
                  fontSize: "13px",
                  padding: "10px 24px",
                }}
                onClick={this.handleContineBtn}
              >
                <FormattedMessage
                  id="common.continue.label"
                  defaultMessage="Continue"
                />
              </CustomButton>
            </div>
          </div>
        </DialogContent>
      </Dialog>
    );
  }
}
const mapStateToProps = (state) => {
  return {};
};
export default compose(
  withRouter,
  connect(mapStateToProps, {
    getColumnsList
  }),
  withStyles(classes, { withTheme: true })
)(AddBillSuccess);
