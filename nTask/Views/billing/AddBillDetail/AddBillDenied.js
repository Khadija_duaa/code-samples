import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter, Link } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import classes from "./style";
import Typography from "@material-ui/core/Typography";
import CustomButton from "../../../components/Buttons/CustomButton";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import deniedIcon from "../../../assets/images/denied_icon_cross.png";

class AddBillDenied extends Component {
    constructor(props) {
        super(props);
      }
      /**
       * It is for if user bill payment unsuccessfull then this dialog appears
       */
      render(){
        let {classes} = this.props;
          return (
                <Dialog
                    classes={{
                        paper: classes.dialogPaperCnt,
                        scrollBody: classes.dialogCnt
                        }}
                        fullWidth={true}
                        scroll="body"
                        disableBackdropClick={true}
                        disableEscapeKeyDown={true}
                        open={true}
                        onClose={()=>{}}
                    >
                        <DialogContent className={classes.defaultDialogContent }>
                            <div className={classes.dialogBody} >
                                <div className={classes.iconContainer}>
                                    <img
                                        src={deniedIcon}
                                        alt="Pro Plan Image"
                                        className={classes.statusIcon}
                                    />
                                </div>
                                <Typography variant="h2" className={classes.bodyTitle}>
                                    Payment Denied
                                </Typography>
                                <Typography variant="body2" className={classes.bodyCnt}>
                                    Unfortunately, your payment could not be processed. Please contact your<br/>
                                    card issuer to resolve the issue.
                                </Typography>
                                <Typography variant="body2" className={classes.bodyCnt}>
                                    To contact our billing services department, email us at <Link className={classes.linkBtnStyle} to="/" onClick={e => e.preventDefault()}>billing@ntaskmanager.com</Link>
                                </Typography>
                                <div className={classes.dialogFooter}>
                                    <CustomButton
                                        btnType="blue"
                                        variant="contained"
                                        style={{ margin: '20px 0', textTransform: 'none', fontSize: "14px"}}
                                        onClick={this.props.unSuccessfullyExit}
                                    >
                                    Go Back
                                    </CustomButton>
                                </div>
                            </div>
                        </DialogContent>
                    </Dialog>
          );
      }
    }
    const mapStateToProps = state => {
        return {
         
        };
      };
    export default compose(
        withRouter,
        connect(
          mapStateToProps,{
          }
        ),
        withStyles(classes, { withTheme: true })
      )(AddBillDenied);