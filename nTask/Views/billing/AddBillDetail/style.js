import SuccessBill from "../../../assets/images/plans/Bg_Success_Bill.png"
const AddBillDetailStyles = theme => ({
    fontSize: {
        fontSize: "13px !important"
      },
    addBillDetailsDialog: {

    },
    // billDialog: {
    //     overflowY: "auto",        
    // },
    dialogPaperCnt: {
        overflowY: "visible",
        background: theme.palette.common.white,
        width: "600px!important"
    },
    dialogSuccessPaperCnt: {
        overflowY: "visible",
        background: theme.palette.common.white,
        width: "530px!important"
    },
    // addBillDetailPaperCnt: {
    //   overflowY: "auto",
    //   background: theme.palette.common.white,
    // },
    addBillDefaultDialogTitle: {
      padding: '20px 20px 20px 25px',
      borderTopRightRadius: 'inherit',
      borderTopLeftRadius: 'inherit',
      marginBottom: 10,
      backgroundColor: theme.palette.background.light,
      borderBottom: `1px solid ${theme.palette.background.contrast}`,
    },
    addBillTitle: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      fontWeight: theme.typography.fontWeightRegular,
    },
    addBillContent: {
  
    },

    // billDetail: {
    //     width: 550,
    // },
    paymentSummary: {
        // padding: '36px 46px',
        padding: 20,
        borderRadius: 4,
        background: theme.palette.common.white,
        // border: `1px solid ${theme.palette.border.lightBorder}`,
        // width: 550,
        marginBottom: 10
    },
    toggleBtnGroup: {
        textAlign: 'center',
        borderRadius: 4,
        background: theme.palette.common.white,
        border: "none",
        boxShadow: "none",
        "& $toggleButtonSelected": {
          color: theme.palette.common.black,
          backgroundColor: theme.palette.background.paper,
          '& $planPriceStyle':{
            color: theme.palette.text.azure,
          },
          '& $planTypeLbl': {
            color: theme.palette.common.black,
          },
          '& $saleOff': {
            // backgroundColor: theme.palette.background.green,
            color: theme.palette.text.brightGreen,
            fontWeight: theme.typography.fontWeightMedium,
          },
          "&:after": {
            background: theme.palette.common.white
          },
          "&:hover":{
            background: theme.palette.background.light,
            color: theme.palette.common.black
          },
        },
      },

      toggleButton: {
        border: `1px solid ${theme.palette.border.lightBorder}`,
        borderRadius: 4,
        // width: '50%',
        // height: "auto",
        height: 54,
         padding: "0px 20px",
         fontSize: "12px !important",
         textTransform: "capitalize",
         color: theme.palette.text.primary,
         fontWeight: theme.typography.fontWeightLight,
         background: theme.palette.background.items,
         "&:hover":{
           background: theme.palette.background.medium
         },
         "&[value = 'monthly']": {
           borderRight: 0,
         }
       },
       toggleButtonSelected:{},
    toggleButtonMainDiv: {
        position: 'relative',
        // paddingTop: 10,
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'column',
    },
    toggleButtonOfferDiv: {
        display: 'none',
        position: 'absolute',
        top: '-34%',
        left: '10%',
        background: 'white',
        border:`1px solid ${theme.palette.border.lightBorder}`,
        padding: '5px 10px',
        borderRadius: 4,
        fontSize: "12px !important",
        color: theme.palette.text.light,
        fontWeight: theme.typography.fontWeightLarge,
    },
    planTypeLbl: {
        display: 'flex',
        // marginTop: 5,
        fontSize: "14px !important",
        fontWeight: 600,
        color: theme.palette.text.light,
    },
    planTypeCnt: {
        display: 'flex', 
        alignItems: 'center',
    },
    planPriceStyle: {
        marginRight: 8,
        fontSize: "22px !important",
        color: theme.palette.text.light,
        fontWeight: theme.typography.fontWeightMedium,
    },
    saleOff: {
        // padding: '2px 6px',
        // fontSize: "11px !important",
        // borderRadius: 4,
        // backgroundColor: theme.palette.background.dark,
        // color: theme.palette.common.white,
        color: theme.palette.text.light,
    },
    teammemberCnt: {
        display: 'flex', 
        marginTop: 25, 
        justifyContent: 'space-between', 
        alignItems: 'center',
    },
    teammembers: {
        '& input[type=number]::-webkit-inner-spin-button': {
            '-webkit-appearance': 'none',
            background: `#FFF url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAPCAYAAADZCo4zAAAArUlEQVQoU82RsRHDIAxF+RVtNkla48ZMEmcDj+ARskE8QjYgDdAmm7ilsXziEKcRQsFJ+u/0JYBRJ+c8Hcexj+P4lTIkyDnPRPTiHMBjGIatxnw18UlEUwM+ABaGoEVpHWO8AagQUkpvIlq1L3dp0Npn0MPq+C+AlNJSStm897seLoRwsdbeeU1+sWspxQvUxGCM+dUtNMS5tbaKzrm5rylQs6li/wvxbpARkesnsYNeejnucREAAAAASUVORK5CYII=') no-repeat center center;`,
            width: 15,
            height: 30,
            opacity: 1, /* shows Spin Buttons per default (Chrome >= 39) */
            position: 'absolute',
            top: 0,
            right: 10,
            bottom: 0,
        },
        // '& input[type=number]::-webkit-outer-spin-button': {
        //     '-webkit-appearance': 'none',
        //     background: `#FFF url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADgAAAA4CAIAAAAn5KxJAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAxRpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQyIDc5LjE2MDkyNCwgMjAxNy8wNy8xMy0wMTowNjozOSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6RjkzRDgyMjhGQzY5MTFFODgzMUFGMjJCRjcxMUZDRDkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6RjkzRDgyMjdGQzY5MTFFODgzMUFGMjJCRjcxMUZDRDkiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTggV2luZG93cyI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSIzNDc3MkRFNDBDNEE2NUQ1NjNBQkM2RTQ2MjEzQ0QwQSIgc3RSZWY6ZG9jdW1lbnRJRD0iMzQ3NzJERTQwQzRBNjVENTYzQUJDNkU0NjIxM0NEMEEiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5G+6mSAAAWX0lEQVR42nRaWYwc13V9W+1Vvc5GzsLhOtxFSaRMSQy8wQsQ5cuGYSRfyYeRL/8EAZIg+QiQxAmQAP4JYDiJHTgwDNlxFMuxbFm2RMe2YFmhKJLiOjMcbrNyprunt9rr5d5X3T1N0i72NKurq+rdd9+955x7q+k//sOfMbVRSgkhtL/BPhwcvA+2wWn5R0kySiXLUombYFQkGaOZL2VKCZUEj8JOChuN8LJUwF5CicwSSdMshTPwJLgVnII3hCszmmUSPma4wZEMjgv62zcwEYcZMvoxc9UOk1lKGYWvU5nBEFzjnHlwYZalSZLGMQ7PdMaZDtbBnCjLuKTwH5rHiDJS5raiXT1nkdwA2FG2SsHhtn2PDttI+97NzcXBOKekN/vcRvhAMwlfELCTUa6xIEpq9db6VrvRaNZqW0mSRVECIziOW/bs2emJYsngPOHgSuU2SpXTyM4q4f1xTPyoZqsmAIaCpTQ3VA2eW5ybnXuRDiZIBnb2/tR5CdxKci2I0ls371y6fHV1bXOj0Ykzkvsc7IAxNE0vWvb1G7dPntx38viBLAlYLNEXJM0dAVs+NNoNLgTjIYJwChScgUsvwB+4Aj2zwMG9taZsEKy9d7XQ6EiYKkyScvgTOk0pf/fijfM///XyeiMD93GTCEfQnjO4mlKYyofNVstvbbcbEA9PnzpK0kCDsGRwE6k8l+W25rOTEPeMphDGGB5oMi79sH2Dnd8QpgODKUkkTISZuvWw1nz9J2+9+/7VTiSJ5jKwG76SEpaJMxgwU+smcWBNgwVvdbJf/PJywSkcOjBF4g7eNeu5M0+mfNw8WDEKMCPxoGD9LT9pYOjg42CfZZi/uPyU6rYRRPHlK5de+Z+3lzda8JloTAqwJ2VZhFYKDWI/jjM1KkZMxsD/IiMijMjlSwvTu0fLHg9i9Dd8Sfuj5H6hah1gcIggnsePJsRO9gzl1CBgB0uvSUiaNBEJFea1pbXXfnj+1sJSQnRumhRMw/nH6BsMJi4gqsA2BiiV5YNzQCZKOAPnaSsbtfn55WdPzMo0EJoGJ6YIHRwgATCJqoDJenCQqjurpX8k3QepNBSgvR3JM0Z02/7Z2xdefuWtbkCo8AAYIZcg/znMlIHDUnQ9ExG4kTJhWIBIKWIqY4BJsL4Eg1Kj6dLi4rE9Zd2zYAqEYWLBYqvATDE7c1hCp8ocDARsw9j+JJoOYpRqTDesi1fnv/PdNzoBuNVOSKIAVLkeEETlAzAADJzCOIBA8HWS6gTnj7eByWZpmkQJyR7WNjea2/urJfgSo5NxJAGMSshS0gsANXoeeyLPmAEzDe3gN4+wEeObzdY3X361E3FmaCn4EjJeDQ9oqhFmgD2cQmimMoYggQBzOBMay9lF5UrGDB7IOIyzRidd9+VeYQCrwTlobJ7+BHgrHSD/wE2PAP4AzHJD82XfSSxDvP2TCxv1LtEKKYspjSEWSZYYjNlCWBq+NE4sA94iU1BT5xWv4Bh2N/TBVVG3zYTcvWfm/mrt0o27m356+ca97Ubz5Nz+ybEqZArYCt5TmU8SNSgYn2czMpOmaQMnD8frEE322MyPkqvXbiF1swQchhNKpEF5wdRtXStZuiekZ+mVglP18N01dVPXLdvSdB0Sqt3uep5pW4Z/ZP+uSuXNCzeWbi7cv8OWFpc+89KnZ3aPJsRHOEVExGTKh89d2/PoYyrkNzK7pomN7WZju025APKDpYLbacwoGLqjMc8UliCuoCOOPlE0RgoeuNXWeMGznKJrObbQdWE4MDMaB7D+ju0JoX/vzV+sJmT+7uqv3r2466VPIMIwJkgO1Y+n8iOGPmnxYIOkBYUUBhHEUAbJAlmeJKaWWloGq6xTSGSua8A1EPWapQvLBNwTuuPqtkt1nes60xAbKDHBmAnTO3f6yOLSzdVryxFz767W0rDLDUAVnRJf9JQE2pvQ3tKD0uB5mA6CdbDz2JYmKVG5A8wGWALzswxhG9w2hKEJA8zSQOYRQ9Ms09B1TTcN23Vtx+HI00JRG+dADVxLsnhy18i5554pWULn/GGt0W63TdPExOHoWaEYnCuk7Nkj1KZpYrDx37IpN6uEI5Dj0tI11wTHwYpkGpUGCDkqHQvWGKgohVvalg3mIqyiSqKIolwXGkzEgEnqnDxz4ujRPVMiieI4Wbr3AIeGCTEcieYjqlcPlQRXljHRO4wOzW0VvRcQBlzPRZzghrTBEfQMEJgM1Si8a5zCwhoGkBRcQiBBe3OjDK4Cr+joCV03IFgNIFLDNAE0q0X7zLE5HWGeG7atpodW4YhIPWCvGhu9jHYBKQOOiZ0XuByoDl5UvRicAHDLDNPSwW9wX2BeUAk01klmoG/gFugNNBcJGZdJ0zWWq0qVAjhDSRT6ILhQTedCBwR9em52vFzo+t1Gq2mYcDFIMTQMUFkQvFRA8lLU4mIgO3oxywYgnyu7XIEiqlcqZcMAwwLUMpTo+Zxgwmo1cNEAuNULbgpnMqVgwSthGAIi2i7kI0fk6dMfBMREuTA5Vr5Tq/tBxNQa43AcKXRI+GNK9Ci0d3DI0AEhwRh5hkGoRRFQDlyjmRorFmwHMgEFGN44S1DlJjFANS40bJBOwAB+EoOhcAcdglMTuJgQtQqJ4N1kadExwZix8fFcR+Kg6PucpiRTYh3hSQkd0tPFA5SX2cBWiE7FbLJUKo6MlhvtdZPGu4r2mK0LrItoEJFGinZ6IdVIc7wdtIT2tOl5lohNnsVcp1ES+5CBacQlCCxNpAJmS7OU6a69a8SZ8LQjBw7AsgvUhBQ1DkPFANlJoPxTrsMkI0+UbSBzh4+hZMzSkucdOXhgcf5eoeBUCq7BSRqGNUI2241msxlDKYcqnfB75ty95eDowbPPHC45AAI68CCQw9XlzrsXLsiQ7No1WhkxZsbL+yYniUZHy4XjB2bHKmUpA6Uq0UasbGGHoNWoy0DK8scM7UcqHfqYI76Mwl2VMnCOBwEoDEKgvuStzabs+ienpqrlYjuO/YQubzbWVjd+nd2sVr0jrgXgD9oukfy9Cxfnbyx329HC7TsFl9hadu706d/58AvVordn17ghtDQJVeCxXEtL1otSIFE4LAZs1NdTciiRdjZA8FtXLv3qZ28VAMw5hdETH+qKsEDl6RPHJyenbi0sxB0fhMj+oh15+6KouVJr7fcjk2bdMA0kP3d49sXDBxJm2J4DjLC1sXbhwvsT05OWxqsFS9NAO6OJilJUBaJ00yBnRK6V+ybulOy9oMaAwToN9gqmtW98IgtWXVPo2HcgtqG7ZdAj/M0r719ZuDM2Xvzi535/Zf7G9SuL9YQsPNiYm56sHJwyCsKLtbud+KfvvLN7etraJCOeffaZZ8DidtCBvCyZIPxjYGVgBlUTYBXJlCIZlG6Y9fniDqtUNRs8lvWhAIAy2u6MucVoFOgcypIU2Nuzi5wk3cZ2t7lVsfUPn3rmI0+fiPeMfG15fau1tvSgcevO6MmnjlTGCvcXl//1W6+utIOv/clfVW3+g2985c7CzUNzBztxsnJ3xQSNQrFclCh3GO3H3yAOs7wKlb0cf8TQARRggUCUBiC0s70NMWfYWuI3bcOwXBfxqtE85Hij0zOzFrv6s/OCyP0T43fq292taH2zVd9ueRUHGP/TZ46VpmZ2ewAC2eljx+MoAA7xPK9SDhLQO1kCWc4UPGYkL8iz3Je59BOqIuV99Mp68hlXO8vls1QVLXxfa/orG5vc1GdHZ5phG4SXxoFlCC+w3SO7IHYLBuk0W0yYzSTttn3Psrphp93s0ogYtvGxjz2vm3ZUX+euOzE3F4Q+QI4GitjUIwkqScOqRia5hsN1f7QcGopRMBiBIQdUgBQUEyQ/gAkor1y7ttWoHzx8SGDVS8MoTsy0CHkAKAA1PvITeuTh5sPFO8ugncBPfoD/gLKAGxJQV5ppmDYoNuE4kJEyi0kc6pbZiaC8AtJWwIguo/0ia6cLttN1UiWfElc8P5jLGLxcxSifmp0yXDOTiVAVZxxDmYbNF8+1AYXgBn4YbNXrtUYDC14GIiaLkzQIAt/vYtqaqPNBKAF7Rd0AvgMwx+IYeBh14KA1s1MFD4vMPEYH2UNyl2PVrzoyNId+qIIzeebcC/Pzt1bu359hkwyvwkhOIzRHA+GkA90bRGghrN520OlCnsRQN0HB3u10GKhrTYNoCjrNpJnBtIFQsYJmPEyCTGINTVVjqdcZUe4c1KKqxUSVjFJiSnlcKVyQxSC2cR+rFw6kJ6VbLVd2TwDlphmBYghsBZ9lQIkRjJKaBvcKVqFYgJmtbdY6YQyK7ujRo8dPnCiXykxVzDhyGoNGFRDZuFpA4DATTAFGeN6kG1SUj3W+hnt3j7md9juUNGcKKNNm9+61XSdOEwxHzErUpplqyIA+dz1PM/XV9bVGc9sreOCiMIquXLrcabfBlm67FUW+jkIb9QPBtggmeIYoj54ZdA2fbCkrru/D1U7js4/7ykQEfKKWAETQC+debNe3796az5ce4k0qQID40YFXHbsddaBagBWwHOfBg80L7110w4Pd+vrswX37jxzsdrtwvlMoIBBRECEYpUEUMsckfZaRQ02oYXMx6/tFvlT9rxzCkt55GAZ9HUhTMGJ0YuLyxfdt0NI6rJr0ZeRITUVrFnehgvdHR0ZnptP1zdUJh5w5eeT5kycdkAYYRsyyrDQlgJhYN8C4BKYqoZpHsFQlFTqXIyLm9iqrstzNuaE5fGZ9V0sFZLI/LdY3FCcEJRsIpXq341g2SKqOH9mm0ej4cPNuFNXa8f31WhIlZdN4+tixM6eOAjTa1RLDZgq1TAdinWK7M1PIngJ4hVFicE2tTE7YCqL6BqjgxFaMyBtj5JHIxVYheWKTqo9VqlYKxdJq875U/YIkIo7lxlF3+WG9HcSL99ebQVZ1vRMHDz937Pi459TbdUDPzVZj7fbmSKU6Mj6CFgIVpSggWp0uZCRVjSeQ6HlBLxWH7vQ/cllMlNjOe469MKU77fThs5H6s8zzChyQRQg/wiYjIGkLRLzfCSK/k8i1esut7HY9x2B8efG23HJ3TU1okdwzMblBN69c+WBXfXzfgX0ACCm21Gmtvm2XyinqcqJ82JfwfWzaaZLlEZt3dXsNatQBKlwekykKUh3Hc7wClPV+HMHBMKMb222TS8cr+tttoNV2s3mvvcW6NVmtRDWj02ppnsMhPkqFu3fvLK/fm9g9VjQ03+9AodHpRtaIqbgvwdYBDJLtSLlHKPTRdkjfqTId9mhvBx8spJZtTk5NXb96Q9ONOIq7cWAkYqRcqRY8C+gxlaCRbcFmJkaOzO3bPzVRsh1u6GGaxFy+8OLzN+evQTJA2RmGURgBKmmW5ybYK+AD4yR5ZEkHWU/JY2I+1607ydRzLTb+cdJy34ED2es/9cMIS3dDh0J4c2uzXduyDL0I6lIYkExgyOLK/YXlhZHRkUMHD45UR3TB9+3dUx0thqEfxlG7Hdfq3cLENNN0ksQqKMkACuUAH4f6ozs42kMkvAgrQlUC4SvvkEAFmRDsB0/NTJeqxfWNhynjHviVkDpWq7HpxyOeU9CpU7Ag7K7fWtxs1A4c3FMZhSxyY6oHwP0J8Szb98OHW7XVeufs08/CIQQaBQUYqZlS6/3nDYMAEH2rldAbWmtlsdx58KX4l6knB7brnvnQ6Q+uXE853e5GSFGcwXg8DQH6R0are/eMwYyAhM4+e2psfGT/1LQtjPX6ersdbK7XwTXCtBeWHx557kNQ/ydB0F9FFX4KWx4rhEgO5n203+GrwYrnH1SAgl9TBbYc9NXp55771re++7DRyqgVx3GmnnganK61uk6taWqy6loHpidHR6rAjWHHh2vCbvJgZXV1Yztj1lZ7eXbu0OS+fVDz72BKHnF08P8jxooBEOQFypNEOihNAFuxU6PbD1aXF67fOHrkyFs//2XA9CjD54zIOLBknUDb3KJpkI1Xx6rV2tYGFEbU0Le6/kajfe326uZ2yypW3eroqdPP0tgHPFVPU7O84TNEooMFfdRQ2X90Omxf/zhVT7Uy03GXHmz8yzf++Y3Xf+SJ7PMv/d7czNSlpTVCtYTIKFUFDZMrjTZHf4BK3pRhZ2pqKvXr3ZQsrG9dn1/rNGuju5OPP/u85RVTv6bAX/azqIdLvTbJo6Qvhtv6w65UhZQk/bIVxNGlGwtf/NO/mL/9wLUMr6BHne6E5y575kY7SLCnyCLIAwAFwla2w+3OxmYnEYy8s/iwVK22ut07682ouXXy8NTHPvXJL/3Tl0+dOfuFP/yDkifU6sueQ3d2nojRYawasrUPAQybSqDl/G7nL//mSwsrW2ZxFA65liOj2OFs79hIlm1udUL16AWUCShp0kiTjuBba1sQCwIE0oMNfLpDxFMzu7/wuc92GG3H6cuvvbmwvP73f/7HZddSxV3We24vf0Mm5Q84OWKCUhw5mGFNQlN8QeaoZ/G67X39m9++fOWmZTuEJpbIpseqQbtFWFK2+f6J8kzFHbGYTVNdtTlAHgUp6cYpZFkQh1BS7656556a/qPPfGR23Om2t9uRbxSKVz9Y+Nq/f1dyLyPIhLyH1BrtcSVGHFRv+EJRQgYdkn6YQmZI1dgEQhQ6t+z//K9Xv/r1bzq2CwODJaahQbnU9YOCQy0Qn0XXAVKqbzudIIjSMFElk8wAnkydT5QnDu2bntsHVFWcLrskTXZVSidmZy4uPTQd8/zb73zqdz956vgBv1WDGKD4xDqTA1gHSyg+20Cw2Vh70PvJBXpfPdOFsOXcNO12u/vee+9/77XX3zz/izjFaI8h+Jl0WXYY0LHklQsaNm91g+n6VrO9trXd9kNgJtdkY9Xi7J6ZqV3joyNFF+gqS+JUllCZsiAkv7p672+/+h8bsOSZPD63//Offeno3GzR0cOuD+WfyibQgJmKeWy0q3I5r/fx+R7uCNXarLX9V1959eWXv3P1+q0oJabtSaaxNGKqpwr+bkOxrBiAk9QSpFCwR4tuxbPvLa/GGR0rWVOjhcPTYxMTY4atyywBdQUlAJRa+Iw0DZ/aM3V87/SPby5ys3xt/sFf/92XZ2fGP/HRcx9+8YVqsRx3OwncHrsLEH4RxVI3Q/WUqZ9baKYFRfG9+8v/89oPX3vjx1dv3ITKS7NcnWBlh3NjOZdykI1bIJixmgZ0S8FLroYtZtcSIKMazY6h636ns12vjY9VYLwQvAmBbmggQdI01jTDZRxcza7fgdgD11BiLd1rfOXfvv39H5x/8eyZj7747J7JCZrKKOjC+VzJDKHbRYjHrh9enb/9ox+98d/f//7yypqmC9NEL6Z5Zzr/fQBqKuzvQ6HUSdJ6uz1b8QCWIHUkZ6CqXC40YThWC7xfKjimZQRBl2GdbAhhCN3Ex8iwjoJlptGF2Sf580PIJfCJCRp3vdb59iuvv/HT8yeOzZ174exTRw/ZFo8DHwhZ/PjN/73ywQf/d/H9q9eu17e7pm2bhQrJn0WDCMSQiJWVSc5u+S8qOknYCcO2H4xUPAYqLo6h3DNtU7NsAZoj6JZKxVLJtSwDO/pCw8IT4hlBFgSXWNqsXV28rVOLk1gyROFU/bpESKim9EaYnv/1B29fuDIzOfbxF587e/pUwdT/X4ABAA/hEEX0bgqxAAAAAElFTkSuQmCC') no-repeat center center;`,
        //     width: 15,
        //     height: 30,
        //     // width: '1em',
        //     // borderLeft: '1px solid #BBB',
        //     opacity: 1, /* shows Spin Buttons per default (Chrome >= 39) */
        //     position: 'absolute',
        //     top: 0,
        //     right: 0,
        //     bottom: 0,
        // },
        '& input': {
            border: `1px solid ${theme.palette.border.lightBorder}`,
            paddingLeft: 15,
            paddingRight: 26,
            borderRadius: 4,
            fontSize: "14px !important",
        },
    },
    annualBillCnt:{
        marginTop: 18,
        display: 'flex', 
        justifyContent: 'space-between', 
    },
    promocodeDiscoutnCnt: {
        marginTop: 20,
        display: 'flex', 
        justifyContent: 'space-between',
    },
    promoPriceLbl : {
        color: theme.palette.secondary.medDark,
        fontWeight: theme.typography.fontWeightRegular,
    },
    userBillCnt: {
        marginTop: 22, 
        display: 'flex', 
        justifyContent: 'space-between',
        backgroundColor: theme.palette.background.extraLightGreen,
        padding: 10,
        bordeRadius: 4,
    },
    sepratorHr: {
        width: '100%',
        border: `0.1px solid ${theme.palette.border.lightBorder}`,
    },
    promocodeValueCnt: {
        display: 'flex', 
        marginTop: 15, 
        alignItems: 'flex-end',
    },
    paymentDetail: {
        padding: 20,
        borderRadius: 4,
        background: theme.palette.common.white,
        // border: `1px solid ${theme.palette.border.lightBorder}`,
        // width: 550,
    },
    formControl: {
        width: '100%',
    },
    cardRadiogroup : {
        flexDirection: 'row',
        width: '100%',
        // justifyContent: 'space-around',
    },
    creditCardRadioDiv : {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingRight: 50,
    },
    radioBtnLbl : {
        display: 'flex',
        color: theme.palette.text.primary,
    },
    comingsoon: {
        fontSize: "7px !important",
        letterSpacing: 1,
        marginLeft: 5
    },
    cardNumberCnt: {
        padding: 10,
        border: `1px solid ${theme.palette.border.lightBorder}`,
        borderRadius: 4,
        marginTop: 9,
    },
    container: {
        display: 'flex',
        marginTop: 15,
        width: '100%',
        justifyContent: 'space-between',
        '& label': {
            width: '45%',
        }
    },
    lblStyle: {
        display: 'flex',
        marginTop: 9,
        fontSize: "10px !important",
        color: theme.palette.secondary.light,
    },
    expiryElement: {
        padding: 10,
        border: `1px solid ${theme.palette.border.lightBorder}`,
        borderRadius: 4,
        marginTop: 9,
    },
    cvcElement: {
        padding: 10,
        border: `1px solid ${theme.palette.border.lightBorder}`,
        borderRadius: 4,
        marginTop: 9,
    },
    linkStyle : {
        color: theme.palette.secondary.light,
        fontSize: "12px !important",
    },
    linkBtnStyle: {
        color: theme.palette.text.green,
        fontSize: "12px !important",
    },
    error : {
        // marginTop: 10,
        fontSize: "13px !important",
        color: `${theme.palette.text.danger}!important`,
        padding: 10,
        backgroundColor: `${theme.palette.background.dangerLight}!important`,
        border: '1px!important',
        borderRadius: 4,
    },
    promocodeEdit: {
        width: '100%',
        paddingRight: 20,
        '& label': {
            color: theme.palette.secondary.light,
        },
        '& input':{
            paddingTop: 10,
            paddingBottom: 10,
        }
    },
    promocodeDone: {

        width: 260,
        border: `1px solid ${theme.palette.border.lightBorder}`,
        borderRadius: 4,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: 14,
        paddingRight: 14,
        paddingTop: 9,
        paddingBottom: 9,
    },
    promocodeRemove: {
        color: theme.palette.text.danger,
    },
    showBillDetails: {
        margin: '5px 0',
        padding: 10,
        backgroundColor: theme.palette.background.items,
        borderRadius: 4,
        display: 'flex',
        flexDirection: 'column',
    },
    showBillDetailsLbl: {
        justifyContent: 'space-between',
        display: 'flex', 
        margin: '5px 0',
        color: theme.palette.text.darkGray,
    },

    
    //Dialog Styles
    
    defaultDialogContent: {
        padding: '50px 28px 30px 28px!important',
    },
    SuccessContentBack: {
        background: `url(${SuccessBill})`,
        backgroundRepeat: "no-repeat",
    },
    dialogBody: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    iconContainer: {
        // height: 105,
        // marginTop: 14
    },
    successIconContainer: {
        width: 100,
        height: 100,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: theme.palette.background.lightOrange,
        borderRadius: '50%',
    },
    statusIcon: {
        width: 130
    },
    bodyTitle: {
        fontSize: "32px !important",
        marginTop: 40,
    },
    successHeaderTitle: {
        fontSize: "22px !important",
        marginTop: 19,
    },
    bodyCnt: {
        marginTop: 28,
        fontSize: "14px !important",
        textAlign: 'center',
    },
    successBodyPlan: {
        marginTop: 16,
        fontSize: "12px !important",
        textAlign: 'center',
    },
    successSupportCnt: {
        marginTop: 3,
        fontSize: "12px !important",
        textAlign: 'center',
    },
    successHeaderCnt: {
        marginTop: 8,
        fontSize: "14px !important",
        textAlign: 'center',
    },
    successHeaderCntPlan: {
        marginTop: 10,
        color: theme.palette.text.azure,
    },
    dialogFooter: {
        padding: '70px 100px 0px 100px',
        width: '100%',
        display: 'flex',
        justifyContent: 'space-around',
    },
    successDialogFooter: {
        padding: '26px 100px 0px 100px',
        width: '100%',
        display: 'flex',
        justifyContent: 'space-around',
    }
      
});
export default AddBillDetailStyles;