import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import classes from "./style";
import Typography from "@material-ui/core/Typography";
import billingIcon from "../../../assets/images/billing_icon.png"
import {FormattedMessage} from "react-intl";
class NoBilling extends Component {
    constructor(props){
        super();
    }
    render(){
        const { classes} = this.props;
        return(
            <div className={classes.billingHistoryCnt}>
                <img
                    src={billingIcon}
                    alt="No Billing history Image"
                    className={classes.noBillHistoryIcon}
                />
                <Typography variant="h4" className={classes.noBillingTitle}>
                    <FormattedMessage id="team-settings.billing.no-billing-history.title" defaultMessage="No Billing History"/>
                </Typography>
                <Typography variant="body2" className={classes.noBillingCnt}>
                   <FormattedMessage id="team-settings.billing.no-billing-history.label"  defaultMessage="There is no billing history available at this moment"/> 
                </Typography>
            </div>
        );
    }
}
const mapStateToProps = state => {
    return {
    };
  };
export default compose(
    withRouter,
    connect(
      mapStateToProps,{}
    ),
    withStyles(classes, { withTheme: true })
  )(NoBilling);
