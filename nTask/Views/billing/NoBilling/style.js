const NoBillStyles = theme => ({
    billingHistoryCnt:{
        paddingTop: 126,
        paddingBottom: 228,
        borderRadius: 4,
        background: theme.palette.common.white,
        border: `1px solid ${theme.palette.border.lightBorder}`,
        display: 'flex',
        alignItems: 'center',
        textAlign: 'center',
        flexDirection: 'column',
    },
    noBillHistoryIcon: {
        width: 77,
        height: 88
    },
    noBillingTitle: {
        marginTop: 30,
    },
    noBillingCnt: {
        marginTop: 9, 
        fontSize: "13px !important"
    }

});
export default NoBillStyles;