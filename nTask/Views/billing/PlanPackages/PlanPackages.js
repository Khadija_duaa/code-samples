import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import classes from "./style";
import Typography from "@material-ui/core/Typography";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import CustomButton from "../../../components/Buttons/CustomButton";
import IconButton from "../../../components/Buttons/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Divider from "@material-ui/core/Divider";
import premiumPlan from "../../../assets/images/icon_premium_plan.png";
import businessPlan from "../../../assets/images/icon_business_plan.png";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import ToggleButton from "@material-ui/lab/ToggleButton";
import { FetchPackagePlans } from "../../../redux/actions/userBillPlan";
import Constants from "../../../components/constants/planConstant";
import SvgIcon from "@material-ui/core/SvgIcon";
import AdvanceFilterIcon from "../../../components/Icons/AdvanceFilterIcon";
import AttachmentIcon from "../../../components/Icons/AttachmentIcon";
import BasicIcon from "../../../components/Icons/BasicIcon";
import BulkActionsIcon from "../../../components/Icons/BulkActionsIcon";
import CustomStatusesIcon from "../../../components/Icons/IconCustomTaskStatus";
import GanttIcon from "../../../components/Icons/GanttIcon";
import IntegrationsIcon from "../../../components/Icons/IntegrationsIcon";
import KanbanIcon from "../../../components/Icons/KanbanIcon";
import ProjectsIcon from "../../../components/Icons/ProjectsIcon";
import ReportingIcon from "../../../components/Icons/ReportingIcon";
import RiskManagmentIcon from "../../../components/Icons/RiskManagmentIcon";
import RolesPermissionsIcon from "../../../components/Icons/RolesPermissionsIcon";
import StorageIcon from "../../../components/Icons/StorageIcon";
import PremiumIcon from "../../../components/Icons/PremiumIcon";
import PremiumCrownIcon from "../../../components/Icons/PremiumCrownIcon";
import BusinessBreifcaseIcon from "../../../components/Icons/BusinessBreifcaseIcon";
import { FormattedMessage } from "react-intl";
import {TRIALPERIOD} from '../../../components/constants/planConstant';

class PlanPackages extends Component {
  constructor(props) {
    super(props);
    this.state = {
      subscription: "annually",
      planPrice: {},
      loading: false,
    };
    this.subscriptionTypes = {
      monthly: "unitPriceMonthly",
      annually: "unitPriceAnually",
    };
  }
  /**
   * it is called to get plan packeges from server
   */
  componentDidMount() {
    this.props.FetchPackagePlans(
      result => {
        if (result.status == 200) {
          this.setState({ planPrice: result.data, loading: true });
        } else this.setState({ loading: false });
      },
      fail => {
        this.props.showSnackBar(fail.data);
      }
    );
  }
  /**
   * it is called when user change its package plan annully/ monthly
   * @param {event} event
   * @param {string} newPaymentPlan
   */
  handlePlanChange = (event, newPaymentPlan) => {
    if (newPaymentPlan) {
      this.setState({ subscription: newPaymentPlan });
    }
  };
  /**
   * it is called when user change its package plan premium/business
   * @param {string} selectedPackage
   */
  selectPackage = (selectedPackage, isTrial) => {
    this.props.selectPackage({
      subscription: this.state.subscription,
      planType: selectedPackage,
      trailPaid: isTrial,
    });
  };

  render() {
    const { classes, theme, mode } = this.props;
    const { subscription, planPrice, loading } = this.state;
    let premiumPrice = "-";
    let businessPrice = "-";
    if (loading) {
      premiumPrice = planPrice[Constants.PREMIUMPLANPRICE][this.subscriptionTypes[subscription]];
      businessPrice = planPrice[Constants.BUSINESSPLANPRICE][this.subscriptionTypes[subscription]];
    }
    let premiumBtnLbl =
      mode == Constants.TRIALPLANMODE ? (
        <FormattedMessage
          id="team-settings.billing.biling-plan-dialog.trial-button.label"
          defaultMessage={"Start {TRIALPERIOD}-day free trial"}
          values={{TRIALPERIOD: TRIALPERIOD}}
        />
      ) : mode == Constants.UPGRADEPLANMODE ? (
        <FormattedMessage
          id="team-settings.billing.biling-plan-dialog.upgrade-premium-button.label"
          defaultMessage="Upgrade to Premium"
        />
      ) : (
        <FormattedMessage
          id="team-settings.billing.biling-plan-dialog.update.label"
          defaultMessage="Update"
        />
      );
    let businessBtnLbl =
      mode == Constants.TRIALPLANMODE ? (
        <FormattedMessage
          id="team-settings.billing.biling-plan-dialog.trial-button.label"
          defaultMessage={"Start {TRIALPERIOD}-day free trial"}
          values={{TRIALPERIOD: TRIALPERIOD}}
        />
      ) : mode == Constants.UPGRADEPLANMODE ? (
        <FormattedMessage
          id="team-settings.billing.biling-plan-dialog.upgrade-business-button.label"
          defaultMessage="Upgrade to Business"
        />
      ) : (
        <FormattedMessage
          id="team-settings.billing.biling-plan-dialog.update.label"
          defaultMessage="Update"
        />
      );

    const { isTrialUsed } = this.props.subscriptionDetails;
    let showTrial = false; //(mode == Constants.UPGRADEPLANMODE && !isTrialUsed);
    return (
      <Dialog
        classes={{
          paper: classes.dialogPaperCnt,
          scrollBody: classes.dialogCnt,
        }}
        fullWidth={true}
        scroll="body"
        disableBackdropClick={true}
        disableEscapeKeyDown={true}
        open={true}
        onClose={this.props.exitPackageDialog}>
        <DialogTitle id="form-dialog-title" classes={{ root: classes.packagesDialogTitle }}>
          <div className={classes.packagesTitle}>
            <Typography variant="h2">
              <FormattedMessage
                id="team-settings.billing.biling-plan-dialog.title"
                defaultMessage="Select nTask Subscription to Continue"
              />
            </Typography>
            <IconButton btnType="transparent" onClick={this.props.exitPackageDialog}>
              <CloseIcon htmlColor={theme.palette.secondary.medDark} />
            </IconButton>
          </div>
        </DialogTitle>
        <DialogContent className={classes.defaultDialogContent}>
          <div className={classes.packageSubscription}>
            <ToggleButtonGroup
              size="small"
              exclusive
              onChange={this.handlePlanChange}
              value={subscription}
              classes={{ root: classes.toggleBtnGroup }}>
              <ToggleButton
                key={2}
                value="annually"
                classes={{
                  root: classes.toggleButton,
                  selected: classes.toggleButtonSelected,
                }}>
                <div className={classes.toggleButtonMainDiv}>
                  <span className={classes.planTypeLbl}>
                    <FormattedMessage
                      id="team-settings.billing.biling-plan-dialog.anual-button.label"
                      defaultMessage="Billed Annually"
                    />
                  </span>
                  <div className={classes.saleOffCnt}>
                    <span className={classes.saleOffTxt}>
                      <FormattedMessage
                        id="team-settings.billing.biling-plan-dialog.anual-button.sub-label"
                        defaultMessage="Save 33%"
                      />
                    </span>
                  </div>
                </div>
              </ToggleButton>
              <ToggleButton
                key={1}
                value="monthly"
                classes={{
                  root: classes.toggleButton,
                  selected: classes.toggleButtonSelected,
                }}>
                <div className={classes.toggleButtonMainDiv}>
                  {/* <div className={classes.toggleButtonOfferDiv}>Limited Time</div> */}
                  <span className={classes.planTypeLbl}>
                    <FormattedMessage
                      id="team-settings.billing.biling-plan-dialog.monthly-button.label"
                      defaultMessage="Billed Monthly"
                    />
                  </span>
                </div>
              </ToggleButton>
            </ToggleButtonGroup>
          </div>
          <div className={classes.packagesBody}>
            <div className={[classes.packages, classes.premium].join(" ")}>
              <div className={classes.packagePlan}>
                <SvgIcon viewBox="0 0 34.001 22.879" style={{ width: 34, height: 24 }}>
                  <PremiumCrownIcon />
                </SvgIcon>
                {/* <img
                                    src={premiumPlan}
                                    alt="Premium Plan Image"
                                    className={classes.planImage}
                                /> */}
                <Typography variant="h2" className= {classes.fontSize} style={{ fontSize: "28px", marginTop: 9, lineHeight: "1.3" }}>
                  <FormattedMessage
                    id="team-settings.billing.biling-plan-dialog.premimum-label"
                    defaultMessage="Premium"
                  />
                </Typography>
                <Typography variant="body2" className= {classes.fontSizeBody} style={{ fontSize: "15px" }}>
                  <FormattedMessage
                    id="team-settings.billing.biling-plan-dialog.sme-label"
                    defaultMessage="For SME's, Startups"
                  />
                </Typography>
                <Typography
                  variant="h6"
                  className= {classes.fontSizeHeadLarge}
                  style={{
                    fontSize: "36px",
                    marginTop: 15,
                    color: theme.palette.text.azure,
                    fontWeight: theme.typography.fontWeightMedium,
                    lineHeight: "1.3",
                  }}>
                  <sup>$</sup>
                  {premiumPrice}
                </Typography>
                <Typography variant="h6" className={classes.fontSizeHeadSmall} style={{ fontSize: "13px", lineHeight: "1.3" }}>
                  <FormattedMessage
                    id="team-settings.billing.biling-plan-dialog.per-month-label"
                    defaultMessage="per month, per user"
                  />
                </Typography>
                <div className={classes.packageTrialUpgradeBtns}>
                  <CustomButton
                    btnType="blue"
                    variant="contained"
                    onClick={() => this.selectPackage(Constants.PREMIUMPLAN, mode)}
                    style={{ padding: "12px 20px" }}>
                    {premiumBtnLbl}
                  </CustomButton>
                  {showTrial ? (
                    <a
                      href="javascript:void(0)"
                      className={classes.linkStyleTrial}
                      onClick={() =>
                        this.selectPackage(Constants.PREMIUMPLAN, Constants.TRIALPLANMODE)
                      }>
                      Start {constants.TRIALPERIOD}-day free trial
                    </a>
                  ) : null}
                </div>
              </div>
              <Divider></Divider>
              <div className={classes.packageDetails}>
                <ul>
                  <li>
                    <div className={classes.packageItem}>
                      <SvgIcon viewBox="0 0 20 20" className={classes.svgIcon}>
                        <BasicIcon />
                      </SvgIcon>
                      <span className={classes.packageItemTxt}>
                        {/* Everything in{" "}
                        <span
                          style={{
                            color: theme.palette.common.black,
                            fontWeight: theme.typography.fontWeightMedium,
                          }}
                        >
                          
                        </span>
                        , Plus */}
                        <FormattedMessage
                          id="team-settings.billing.biling-plan-dialog.list.everything"
                          values={{
                            t: <span style={{color: theme.palette.common.black,fontWeight: theme.typography.fontWeightMedium,}}> 
                              <FormattedMessage id="team-settings.billing.biling-plan-dialog.basic" defaultMessage="Basic"/>
                              </span>
                          }}
                          // values={{ name: <b>{activeTeam.companyName}</b> }}
                          defaultMessage={`Everything in {t}, Plus`}
                        />
                      </span>
                    </div>
                  </li>
                  <li>
                    <div className={classes.packageItem}>
                      <SvgIcon viewBox="0 0 20 20" className={classes.svgIcon}>
                        <StorageIcon />
                      </SvgIcon>
                      <span className={classes.packageItemTxt}>
                        <FormattedMessage
                          id="team-settings.billing.cancel-subscription-confirmation.heres-missing.list.storage"
                          values={{ n: 5 }}
                          defaultMessage="5 GB Storage"
                        />
                      </span>
                    </div>
                  </li>
                  <li>
                    <div className={classes.packageItem}>
                      <SvgIcon viewBox="0 0 20 20" className={classes.svgIcon}>
                        <AttachmentIcon />
                      </SvgIcon>
                      <span className={classes.packageItemTxt}>
                        <FormattedMessage
                          id="team-settings.billing.biling-plan-dialog.list.attachement"
                          values={{ n: 150 }}
                          defaultMessage="150 MB Attachment Size"
                        />
                      </span>
                    </div>
                  </li>
                  <li>
                    <div className={classes.packageItem}>
                      <SvgIcon viewBox="0 0 20 20" className={classes.svgIcon}>
                        <ProjectsIcon />
                      </SvgIcon>
                      <span className={classes.packageItemTxt}>
                        <FormattedMessage id="project.label" defaultMessage="Projects" />
                      </span>
                    </div>
                  </li>
                  <li>
                    <div className={classes.packageItem}>
                      <SvgIcon viewBox="0 0 20 20" className={classes.svgIcon}>
                        <GanttIcon />
                      </SvgIcon>
                      <span className={classes.packageItemTxt}>
                        <FormattedMessage
                          id="team-settings.billing.cancel-subscription-confirmation.heres-missing.list.Gantt"
                          defaultMessage="Gantt"
                        />
                      </span>
                    </div>
                  </li>
                  <li>
                    <div className={classes.packageItem}>
                      <SvgIcon viewBox="0 0 20 20" className={classes.svgIcon}>
                        <KanbanIcon />
                      </SvgIcon>
                      <span className={classes.packageItemTxt}>
                        <FormattedMessage
                          id="team-settings.billing.biling-plan-dialog.list.kanban"
                          defaultMessage="Boards"
                        />
                      </span>
                    </div>
                  </li>
                  <li>
                    <div className={classes.packageItem}>
                      <SvgIcon viewBox="0 0 20 20" className={classes.svgIcon}>
                        <BulkActionsIcon />
                      </SvgIcon>
                      <span className={classes.packageItemTxt}>
                        <FormattedMessage
                          id="team-settings.billing.cancel-subscription-confirmation.heres-missing.list.bulk-actions"
                          defaultMessage="Bulk Actions"
                        />
                      </span>
                    </div>
                  </li>
                  <li>
                    <div className={classes.packageItem}>
                      <SvgIcon viewBox="0 0 20 20" className={classes.svgIcon}>
                        <AdvanceFilterIcon />
                      </SvgIcon>
                      <span className={classes.packageItemTxt}>
                        <FormattedMessage
                          id="team-settings.billing.biling-plan-dialog.list.advance"
                          defaultMessage="Advanced Filters"
                        />
                      </span>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div className={[classes.packages, classes.business].join(" ")}>
              <div className={classes.packagePlan}>
                <SvgIcon viewBox="0 0 27.998 23.35" style={{ width: 28, height: 24 }}>
                  <BusinessBreifcaseIcon />
                </SvgIcon>
                {/* <img
                                    src={businessPlan}
                                    alt="Business Plan Image"
                                    className={classes.planImage}
                                /> */}
                <Typography variant="h2" className= {classes.fontSize} style={{ fontSize: "28px", marginTop: 9, lineHeight: "1.3" }}>
                  <FormattedMessage
                    id="team-settings.billing.biling-plan-dialog.business-label"
                    defaultMessage="Business"
                  />
                </Typography>
                <Typography variant="body2" className={classes.fontSizeHeadMed} style={{ fontSize: "15px" }}>
                  <FormattedMessage
                    id="team-settings.billing.biling-plan-dialog.for-large-label"
                    defaultMessage="For Larger Corporates"
                  />
                </Typography>
                <Typography
                  variant="h6"
                  className= {classes.fontSizeHeadLarge}
                  style={{
                    fontSize: "36px",
                    marginTop: 15,
                    color: theme.palette.text.azure,
                    fontWeight: theme.typography.fontWeightMedium,
                    lineHeight: "1.3",
                  }}>
                  <sup>$</sup>
                  {businessPrice}
                </Typography>
                <Typography variant="h6" 
                className={classes.fontSizeHeadSmall}
                style={{ fontSize: "13px", lineHeight: "1.3" }}>
                  <FormattedMessage
                    id="team-settings.billing.biling-plan-dialog.per-month-label"
                    defaultMessage="per month, per user"
                  />
                </Typography>
                <div className={classes.packageTrialUpgradeBtns}>
                  <CustomButton
                    btnType="blue"
                    variant="contained"
                    onClick={() => this.selectPackage(Constants.BUSINESSPLAN, mode)}
                    style={{ padding: "12px 20px" }}>
                    {businessBtnLbl}
                  </CustomButton>
                  {showTrial ? (
                    <a
                      href="javascript:void(0)"
                      className={classes.linkStyleTrial}
                      onClick={() =>
                        this.selectPackage(Constants.BUSINESSPLAN, Constants.TRIALPLANMODE)
                      }>
                      Start {constants.TRIALPERIOD}-day free trial
                    </a>
                  ) : null}
                </div>
              </div>
              <Divider></Divider>
              <div className={classes.packageDetails}>
                <ul>
                  <li>
                    <div className={classes.packageItem}>
                      <SvgIcon viewBox="0 0 22 22" className={classes.svgIcon}>
                        <PremiumIcon />
                      </SvgIcon>
                      <span className={classes.packageItemTxt}>
                        <FormattedMessage
                          id="team-settings.billing.biling-plan-dialog.list.everything"
                          values={{
                            t: <span style={{color: theme.palette.common.black,fontWeight: theme.typography.fontWeightMedium,}}> 
                              <FormattedMessage id="team-settings.billing.biling-plan-dialog.premimum-label" defaultMessage="Premium"/>
                              </span>
                          }}
                          defaultMessage={`Everything in {t}, Plus`}
                        />
                      </span>
                    </div>
                  </li>
                  <li>
                    <div className={classes.packageItem}>
                      <SvgIcon viewBox="0 0 20 20" className={classes.svgIcon}>
                        <StorageIcon />
                      </SvgIcon>
                      <span className={classes.packageItemTxt}>
                        <FormattedMessage
                          id="team-settings.billing.cancel-subscription-confirmation.heres-missing.list.storage"
                          values={{ n: 10 }}
                          defaultMessage="10 GB Storage"
                        />
                      </span>
                    </div>
                  </li>
                  <li>
                    <div className={classes.packageItem}>
                      <SvgIcon viewBox="0 0 20 20" className={classes.svgIcon}>
                        <AttachmentIcon />
                      </SvgIcon>
                      <span className={classes.packageItemTxt}>
                        <FormattedMessage
                          id="team-settings.billing.biling-plan-dialog.list.attachement"
                          values={{ n: 250 }}
                          defaultMessage="250 MB Attachment Size"
                        />
                      </span>
                    </div>
                  </li>
                  <li>
                    <div className={classes.packageItem}>
                      <SvgIcon viewBox="0 0 20 20" className={classes.svgIcon}>
                        <AdvanceFilterIcon />
                      </SvgIcon>
                      <span className={classes.packageItemTxt}>
                        <FormattedMessage
                          id="team-settings.billing.cancel-subscription-confirmation.heres-missing.list.custom-filters"
                          defaultMessage="Create & Save Custom Filters"
                        />
                      </span>
                    </div>
                  </li>
                  <li>
                    <div className={classes.packageItem}>
                      <SvgIcon viewBox="0 0 20 20" className={classes.svgIcon}>
                        <RiskManagmentIcon />
                      </SvgIcon>
                      <span className={classes.packageItemTxt}>
                        <FormattedMessage
                          id="team-settings.billing.cancel-subscription-confirmation.heres-missing.list.risk-mangement"
                          defaultMessage="Risk Management"
                        />
                      </span>
                    </div>
                  </li>
                  <li>
                    <div className={classes.packageItem}>
                      <SvgIcon viewBox="0 0 20 20" className={classes.svgIcon}>
                        <KanbanIcon />
                      </SvgIcon>
                      <span className={classes.packageItemTxt}>
                        <FormattedMessage
                          id="team-settings.billing.biling-plan-dialog.list.kanban"
                          defaultMessage="Boards"
                        />
                      </span>
                    </div>
                  </li>
                  <li>
                    <div className={classes.packageItem}>
                      <SvgIcon viewBox="0 0 22 22" className={classes.svgIcon}>
                        <CustomStatusesIcon />
                      </SvgIcon>
                      <span className={classes.packageItemTxt}>
                        <FormattedMessage
                          id="team-settings.billing.biling-plan-dialog.list.customstatus"
                          defaultMessage="Custom Statuses"
                        />
                      </span>
                    </div>
                  </li>
                  <li>
                    <div className={classes.packageItem}>
                      <SvgIcon viewBox="0 0 20 20" className={classes.svgIcon}>
                        <RolesPermissionsIcon />
                      </SvgIcon>
                      <span className={classes.packageItemTxt}>
                        <FormattedMessage
                          id="team-settings.billing.biling-plan-dialog.list.role-permission"
                          defaultMessage="Roles & Permissions"
                        />
                      </span>
                    </div>
                  </li>
                  {/* <li>
                    <div className={classes.packageItem}>
                      <SvgIcon viewBox="0 0 20 20" className={classes.svgIcon}>
                        <ReportingIcon />
                      </SvgIcon>
                      <span className={classes.packageItemTxt}>
                        <FormattedMessage id="team-settings.billing.biling-plan-dialog.list.reporting" defaultMessage="Advanced Reporting"/>{" "}
                        <span className={classes.comingSoonCnt}>
                          (<FormattedMessage id="team-settings.billing.biling-plan-dialog.list.comming-soon" defaultMessage="Comming Soon" />)
                        </span>
                      </span>
                    </div>
                  </li> */}
                </ul>
              </div>
            </div>
          </div>
        </DialogContent>
      </Dialog>
    );
  }
}
const mapStateToProps = state => {
  return {
    profile: state.profile.data,
    subscriptionDetails: state.profile.data.teams.find(
      t => t.companyId == state.profile.data.activeTeam
    ).subscriptionDetails,
  };
};
export default compose(
  withRouter,
  connect(mapStateToProps, {
    FetchPackagePlans,
  }),
  withStyles(classes, { withTheme: true })
)(PlanPackages);
