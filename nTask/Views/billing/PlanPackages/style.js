import planBgImg from "../../../assets/images/plans/BgGeometricPattern.png"
import PlanPackageBG from "../../../assets/images/svgs/PlanPackageBG.svg"
const PackagesStyles = theme => ({

    dialogPaperCnt: {
        overflowY: "visible",
        background: theme.palette.common.white,
        width: "706px!important",
        maxWidth: '800px!important',
    },
    fontSize: {
        fontSize: "28px !important"
    },
    fontSizeBody: {
        fontSize: "15px !important"
    },
    packagesDialogTitle: {
        borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
        padding: '20px 18px 20px 25px',
        // backgroundColor: theme.palette.background.light,
        borderTopRightRadius: 'inherit',
        borderTopLeftRadius: 'inherit',
    },
    packagesTitle: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        fontWeight: theme.typography.fontWeightRegular,
    },
    packageSubscription: {
        // padding: '0px 144px',
        // borderRadius: 4,
        // background: theme.palette.common.white,
        // border: `1px solid ${theme.palette.border.lightBorder}`,
        // width: 550,
        // marginBottom: 10
    },
    toggleBtnGroup: {
        textAlign: 'center',
        borderRadius: 4,
        background: theme.palette.common.white,
        border: "none",
        boxShadow: "none",
        "& $toggleButtonSelected": {
            //   color: theme.palette.common.white,
            backgroundColor: theme.palette.background.lightGreen,
            border: `1px solid ${theme.palette.background.lightGreen}`,
            //   '& $planPriceStyle':{
            //     color: theme.palette.text.azure,
            //   },
            '& $planTypeLbl': {
                color: theme.palette.common.white,
            },
            '& $saleOffTxt': {
                color: theme.palette.common.white,
                fontWeight: theme.typography.fontWeightMedium,
            },
            '& $saleOffCnt': {
                backgroundColor: theme.palette.background.darkGreen,
            },
            "&:after": {
                background: theme.palette.common.white
            },
            "&:hover": {
                background: theme.palette.background.lightGreen,
                // color: theme.palette.common.black
            },
        },
    },

    toggleButton: {
        border: `1px solid ${theme.palette.border.lightBorder}`,
        borderRadius: 4,
        // height: 54,
        // width: 180,
        // width: '50%',
        // height: "auto",
        // padding: "0px 20px",
        fontSize: "12px !important",
        textTransform: "capitalize",
        color: theme.palette.common.white,
        fontWeight: theme.typography.fontWeightLight,
        background: theme.palette.background.paper,
        padding: '10px 27px',
        height: '100%',
        "&:hover": {
            // background: theme.palette.background.medium
        },
        "&[value = 'annually']": {
            borderRight: 0,
            padding: 10,
        }
    },
    toggleButtonSelected: {},
    toggleButtonMainDiv: {
        position: 'relative',
        // paddingTop: 10,
        alignItems: 'center',
        display: 'flex',
        // flexDirection: 'column',
    },
    planTypeLbl: {
        display: 'flex',
        fontSize: "13px !important",
        // fontWeight: 700,
        color: theme.palette.text.light,
        whiteSpace: 'nowrap'
    },
    saleOffCnt: {
        padding: '0px 7px 1px 7px',
        borderRadius: 4,
        backgroundColor: theme.palette.background.dark,
        marginLeft: 5
    },
    saleOffTxt: {
        color: theme.palette.common.white,
        fontSize: "11px !important",
        whiteSpace: 'nowrap',
    },
    defaultDialogContent: {
        padding: '20px 45px',
        background: `url(${PlanPackageBG})`,
        backgroundRepeat: "no-repeat",
        backgroundPositionX: 15,
        backgroundPositionY: 20,
    },
    packagesBody: {
        display: 'flex',
        justifyContent: 'space-between',
        marginTop: 16
    },
    packages: {
        border: `1px solid ${theme.palette.border.lightBorder}`,
        width: '50%',
        borderRadius: 10,
    },
    premium: {
        // borderTopLeftRadius: 4,
        // borderBottomLeftRadius: 4,
    },
    business: {
        // borderTopRightRadius: 4,
        // borderBottomRightRadius: 4,
        marginLeft: 16,
    },
    packagePlan: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: '15px 0 25px 0',
        minHeight: 265,
    },
    packageDetails: {
        padding: `23px 0 10px 23px`,
        background: `url(${planBgImg})`,
        backgroundColor: theme.palette.background.paper,
        borderRadius: 'inherit',
        '& ul': {
            fontSize: "14px !important",
            // marginTop: 20,
            color: theme.palette.text.darkGray,
            fontWeight: theme.typography.fontWeightLight,
            // lineHeight: 2, 
            paddingInlineStart: 0,
            listStyleType: 'none',
            // whiteSpace: 'nowrap',
            '& li': {
                paddingBottom: 10,
                '&:before': {
                    color: theme.palette.common.white,
                },
            },
        }
    },
    packagesBtns: {
        marginTop: 20,
    },
    planImage: {
        // width: 100
    },
    svgIcon: {
        fontSize: "20px !important",
    },
    packageItem: {
        display: 'flex',
        alignItems: 'center'
    },
    packageItemTxt: {
        marginLeft: 16
    },
    comingSoonCnt: {
        fontSize: "11px !important",
        color: theme.palette.text.darkGreen,
    },
    linkStyleTrial: {
        color: theme.palette.text.linkBlue,
        fontSize: "13px !important",
        // marginRight: 14
        // marginTop: 15,
    },
    packageTrialUpgradeBtns: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: 67,
        marginTop: 20,
        flexDirection: 'column'
    }
});
export default PackagesStyles;