
import planBgImg from "../../../assets/images/bg_pattern.png"
const PaymentOverdueStyles = theme => ({ 

    // paymentOverdueDetailsDialog: {
        // overflowY: "auto", 
    // },
    dialogPaperCnt: {
        overflowY: "visible",
        background: theme.palette.common.white,
        width: "530px!important"
    },
    defaultDialogContent: {
        padding: '40px 40px 40px 40px!important',
        background: `url(${planBgImg})`,
        backgroundRepeat: "no-repeat",
        backgroundPositionY: -100,
    },
    paymentOverdueBody: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    bodyHeaderMain: {
        marginTop: 24,
    },
    bodyHeaderTopContent : {
        display: 'flex',
        textAlign: 'center',
        flexDirection: 'column',
        textTransform: 'capitalize',
    },
    workspaceName: {
        marginTop: 5,
        color: theme.palette.secondary.main,
    },
    bodyHeaderContent: {
        marginTop: 18,
        display: 'flex',
        textAlign: 'center',
    },
    bodyHeaderBtns: {
        marginTop: 30,
        display: 'flex',
        justifyContent: 'space-evenly',
        width: '100%',
        paddingLeft: 40,
        paddingRight: 40,
    },
    seprator: {
        marginTop: 30,
        display: 'flex',
        width: '100%',
        alignItems: 'center',
    },
    sepratorText: {
        fontSize: "12px !important",
        width: 36,
        height: 36,
        border: `1px solid ${theme.palette.border.lightBorder}`,
        borderRadius: '50%',
        alignItems: 'center',
        display: 'flex',
        placeContent: 'center',
    },
    sepratorHrLeft: {
        marginRight: 5,
        marginLeft: 0,
        width: 'calc(50% - 24px)',
    },
    sepratorHrRight : {
        marginLeft: 5,
        marginRight: 0,
        width: 'calc(50% - 24px)',
    },
    paymentOverdueBodyBottom: {
        // marginTop: 30,
        width: '100%',
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
    },
    paymentOverdueBodyBottomBtn: {
        paddingTop: 30,
        width: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },
    selectionDiv: {
        width: '60%',
        textAlign: 'left'
    }

});
export default PaymentOverdueStyles;