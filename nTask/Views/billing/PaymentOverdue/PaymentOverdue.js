import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import classes from "./style";
import Typography from "@material-ui/core/Typography";
import CustomAvatar from "../../../components/Avatar/Avatar";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import SelectSearchDropdown from "../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { generateCompaniesData } from "../../../helper/generateSelectData";
import CustomButton from "../../../components/Buttons/CustomButton";
import Divider from "@material-ui/core/Divider";
import { StripeProvider, Elements } from "react-stripe-elements";
import {
  switchTeam,
  whiteLabelInfo,
  DefaultWhiteLabelInfo,
} from "../../../redux/actions/teamMembers";
import { FetchUserInfo } from "../../../redux/actions/profile";
import moment from "moment";
import Constants from "../../../components/constants/planConstant";
import RestrictedAccess from "../RestrictedAccess/RestrictedAccess";
import DefaultDialog from "../../../components/Dialog/Dialog";
import CreateTeamForm from "../../AddNewForms/addNewTeamFrom";
import PlanPackages from "../PlanPackages/PlanPackages";
import MakePayment from "../MakePayment/MakePayment";
import ConstantsPlan from "../../../components/constants/planConstant";

class PaymentOverdue extends Component {
  constructor(props) {
    super();
    this.state = {
      makePayment: false,
      selectedTeam: [],
      btnSwitchQuery: "",
    };
  }
  /**
   * it is called when user exit from overdue dialog
   */
  exitDialog = () => {
    if (this.props.closePaymentOverdue) this.props.closePaymentOverdue();
  };
  /**
   * it is called when user change his team when payment overdue of his current team
   * @param {string} key
   * @param {string} value
   */
  handleTeamChange = (key, value) => {
    this.setState({ [key]: value });
  };
  /**
   * it is called when want to move on free plan
   */
  continueFree = () => {
    let { companyId } = this.props;
    if (this.props.continueFreePlan) this.props.continueFreePlan({ companyId: companyId });
  };
  /**
   * it is called when user pay his dues and move on with current team
   */
  makePaymentHandler = () => {
    if (this.props.paymentPaidDone) this.props.paymentPaidDone();
  };
  /**
   * it is called when user exit from make payment dialog
   */
  exitMakePayment = () => {
    this.setState({ makePayment: false });
  };
  /**
   * it is called when user switch to other team and its check that other team is either
   * on free plan or trial/paid valid
   * @param {string} type
   * @param {string} item
   */
  switchToOtherTeam = (type, item) => {
    const { selectedTeam } = this.state;
    if (!selectedTeam.id) return;
    // this.props.switchExpirePlan();
    this.props.closePaymentOverdue();
    this.props.showLoadingState();
    this.setState({ btnSwitchQuery: "progress" });
    switchTeam(selectedTeam.id, response => {
      if (selectedTeam.isEnableWhiteLabeling && selectedTeam.isEnableWhiteLabeling == true)
        this.props.whiteLabelInfo(selectedTeam.id);
      else this.props.DefaultWhiteLabelInfo();
      this.props.FetchUserInfo(userApiResponse => {
        // this.setState({btnSwitchQuery: ''});
        this.props.history.push(`/teams/${response.data.team.companyUrl}`);

        const { gracePeriod } = this.props;
        const { paymentPlanTitle, currentPeriondEndDate } = response.data.team.subscriptionDetails;
        if (!ConstantsPlan.isPlanFree(paymentPlanTitle)) {
          if (
            ConstantsPlan.isPlanTrial(paymentPlanTitle) &&
            ConstantsPlan.isPlanExpire(currentPeriondEndDate)
          ) {
            this.setState({ btnSwitchQuery: "" });
            this.props.showLoadingState();
            this.props.planExpire("Trial");
            return;
          } else if (
            ConstantsPlan.isPlanPaid(paymentPlanTitle) &&
            ConstantsPlan.isPlanExpire(currentPeriondEndDate, gracePeriod)
          ) {
            this.setState({ btnSwitchQuery: "" });
            this.props.showLoadingState();
            this.props.planExpire("Paid");
            return;
          }
        }
      });
      this.props.hideLoadingState();
    });
  };
  /**
   * it is called when on overdue user want to open a create new team dialog
   */
  openCreateTeam = () => {
    this.setState({ openDialog: true });
  };
  /**
   * it is called when user close the create new team dialog exit
   */
  handleDialogClose = () => {
    this.setState({ openDialog: false });
  };
  /**
   * it is called when on overdue user select to create new team
   */
  createTeam = () => {
    this.props.createTeam();
  };
  /**
   * it is called when user open a make payment dialog
   */
  makePayment = () => {
    this.setState({ makePayment: true });
  };

  render() {
    const { classes, teams, isOwner, companyName, minimumUser } = this.props;
    const { makePayment, selectedTeam, openDialog, btnSwitchQuery } = this.state;
    const { packageType, currentPeriondEndDate, paymentPlanTitle } = this.props.subscriptionDetails;
    let expiryDate = moment(currentPeriondEndDate).format("MMMM DD, YYYY");
    let planName = Constants.DisplayPlanName(paymentPlanTitle);
    return (
      <Fragment>
        <DefaultDialog
          title="Create Team"
          sucessBtnText="Create Team"
          open={openDialog}
          onClose={this.handleDialogClose}>
          <CreateTeamForm
            handleDialogClose={this.handleDialogClose}
            createTeamHandler={this.createTeam}
          />
        </DefaultDialog>
        {makePayment == false && isOwner == true && (
          <Dialog
            classes={{
              paper: classes.dialogPaperCnt,
              scrollBody: classes.dialogCnt,
            }}
            fullWidth={true}
            scroll="body"
            disableBackdropClick={true}
            disableEscapeKeyDown={true}
            open={true}
            onClose={this.exitDialog}>
            <DialogContent className={classes.defaultDialogContent}>
              <div className={classes.paymentOverdueBody}>
                <CustomAvatar userActiveTeam={true} size="large" />
                <Typography variant="h2" className={classes.bodyHeaderMain}>
                  <span className={classes.bodyHeaderTopContent}>
                    {packageType} Payment Overdue
                    <br />
                    <span className={classes.workspaceName}>{companyName}</span>
                  </span>
                </Typography>
                <Typography variant="h6" className={classes.bodyHeaderContent}>
                  Your {packageType} payment is past due as of {expiryDate}. Please make payment to
                  continue using all our {planName} features.
                </Typography>
                <div className={classes.bodyHeaderBtns}>
                  {/* <CustomButton
                                            btnType="white"
                                            variant="contained"
                                            onClick={this.continueFree}
                                        >
                                        Continue with Free Plan
                                    </CustomButton> */}
                  <CustomButton
                    btnType="blue"
                    variant="contained"
                    onClick={this.makePayment}
                    style={{ padding: "7px 20px" }}>
                    Make Payment
                  </CustomButton>
                </div>
                <div className={classes.seprator}>
                  <Divider className={classes.sepratorHrLeft} />
                  <div className={classes.sepratorText}>
                    <span>OR</span>
                  </div>
                  <Divider className={classes.sepratorHrRight} />
                </div>
                <div className={classes.paymentOverdueBodyBottom}>
                  {teams.length ? (
                    <Fragment>
                      <Typography variant="h6" className={classes.bodyHeaderContent}>
                        Meanwhile you can continue your work by switching to one of the appended
                        options.
                      </Typography>
                      <div className={classes.paymentOverdueBodyBottomBtn}>
                        <div className={classes.selectionDiv}>
                          <SelectSearchDropdown
                            data={() => generateCompaniesData(teams)}
                            label=""
                            isMulti={false}
                            selectChange={this.handleTeamChange}
                            type="selectedTeam"
                            selectedValue={selectedTeam}
                            placeholder="Switch to:"
                            styles={{ marginTop: 0, marginBottom: 0 }}
                          />
                        </div>
                        <CustomButton
                          btnType="success"
                          variant="contained"
                          onClick={this.switchToOtherTeam}
                          query={btnSwitchQuery}
                          disabled={btnSwitchQuery === "progress"}>
                          Switch
                        </CustomButton>
                      </div>
                    </Fragment>
                  ) : (
                    <div style={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
                      <Typography variant="h6" className={classes.bodyHeaderContent}>
                        Meanwhile you can continue working by creating a new team.
                      </Typography>
                      <CustomButton
                        btnType="success"
                        variant="contained"
                        onClick={this.openCreateTeam}
                        style={{ marginTop: 20 }}>
                        Create Your Team
                      </CustomButton>
                    </div>
                  )}
                </div>
              </div>
            </DialogContent>
          </Dialog>
        )}
        {makePayment == false && isOwner == false && (
          <RestrictedAccess
            closeRestrictedDialog={this.props.closePaymentOverdue}
            createTeam={this.createTeam}
            mode={"Plan"}
            hideLoadingState={this.props.hideLoadingState}
            showLoadingState={this.props.showLoadingState}
            planExpire={this.props.planExpire}
            switchExpirePlan={this.props.switchExpirePlan}></RestrictedAccess>
        )}
        {makePayment && (
          <StripeProvider apiKey={STRIPE_PUBLISH_KEY}>
            <Elements>
              <MakePayment
                showSnackBar={this.props.showSnackBar}
                makePaymentHandler={this.makePaymentHandler}
                exitMakePayment={this.exitMakePayment}></MakePayment>
            </Elements>
          </StripeProvider>
        )}
      </Fragment>
    );
  }
}
PaymentOverdue.defaultProps = {
  subscriptionDetails: {},
  isOwner: true,
  companyName: "",
  minimumUser: 0,
  companyId: "",
  gracePeriod: null,
};

const mapStateToProps = state => {
  return {
    subscriptionDetails:
      state.profile.data.teams.find(item => item.companyId == state.profile.data.activeTeam)
        .subscriptionDetails || {},
    isOwner: state.profile.data.teamMember.find(item => item.userId == state.profile.data.userId)
      .isOwner,
    teams: state.profile.data.teams.filter(item => item.companyId != state.profile.data.activeTeam),
    companyName: state.profile.data.teams.find(
      item => item.companyId == state.profile.data.activeTeam
    ).companyName,
    minimumUser: state.profile.data.teamMember.length,
    companyId: state.profile.data.activeTeam,
    gracePeriod: state.profile.data.gracePeriod,
  };
};
export default compose(
  withRouter,
  connect(mapStateToProps, {
    FetchUserInfo,
    whiteLabelInfo,
    DefaultWhiteLabelInfo,
  }),
  withStyles(classes, { withTheme: true })
)(PaymentOverdue);
