import planBgImg from "../../../assets/images/plans/BgGeometricPattern.png"

const NoPlanStyles = theme => ({
    main:{
        // padding: 20,
        borderRadius: 4,
        background: theme.palette.common.white,
        border: `1px solid ${theme.palette.border.lightBorder}`,
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'column',
        // background: `url(${planBgImg})`,
        // backgroundRepeat: "no-repeat",
        maxWidth: 736,
    },
    mainHeader: {
        background: theme.palette.common.white,
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
        padding: 20,
    },
    // mainBottom: {
    //     background: `url(${planBgImg})`,
    // },
    iconContainer: {
        height: 105,
        marginTop: 14
    },
    noPlanIcon: {
        width: 100
    },
    noPlanInfoCnt: {
        paddingTop: 19,
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'column',
        paddingBottom: 20,
    },
    benefitslistCont: {
        flexDirection: 'column',
        alignItems: 'center',
        display: 'flex',
        background: `url(${planBgImg})`,
        width: '100%',
        borderTop: `1px solid ${theme.palette.border.lightBorder}`,
        padding: 20,
        backgroundColor: theme.palette.background.paper,
        borderRadius: 'inherit',
    },
    benefitsHeadCnt:{
        // fontSize: "15px !important"
    },
    benefitslist: {
        marginTop: 29,
        display: 'flex',
        whiteSpace: 'nowrap',
        // '& ul': {
        //     listStyle: 'none',
        //     paddingRight: 20,
        //     "& li": {
        //         display: 'flex',
        //         color: '#0090ff',
        //         alignItems: 'center',
        //         '& svg': {
        //             marginRight: 5, 
        //             fontSize: "18px !important",
        //             color: theme.palette.background.blue
        //         },
        //         '& p':{
        //             lineHeight: 2.5,
        //         }
        //     }
        // }
    },
    noPlanTxt: {
        marginTop: 11,
        fontSize: "13px !important"
    },
    basicPlan: {
        fontSize: "22px !important",
        marginTop: 5,
    },
    upgradeNoPlanCnt: {
        marginTop: 17,
        fontSize: "14px !important",
    },
    seprator: {
        display: 'flex',
        // width: '100%',
        alignItems: 'center',
        margin: '0 10px',
    },
    sepratorHrLeft: {
        marginRight: 5,
        marginLeft: 0,
        width: 12,
    },
    sepratorHrRight : {
        marginLeft: 5,
        marginRight: 0,
        width: 12,
    },
    sepratorText: {
        fontSize: "15px !important",
    },
    benefitCnt: {
        height: 'fit-content',
        padding: 8,
        border: `1px solid ${theme.palette.border.grayLighter}`,
        borderRadius: 4,
        display: 'flex',
        alignItems: 'center',
        background: theme.palette.common.white,
        // width: 210
    },
    benefitCntImg: {
        fontSize: "22px !important",
    },
    benefitCntTxt: {
        fontSize: "13px !important",
        marginLeft: 8,
    },
    linkStyle : {
        color: theme.palette.text.primary,
        fontSize: "12px !important",
        marginTop: 20,
    },
});
export default NoPlanStyles;