import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import classes from "./style";
import Typography from "@material-ui/core/Typography";
import CustomButton from "../../../components/Buttons/CustomButton";
import upgradePro from "../../../assets/images/upgrade_pro.png";
import CheckIcon from "@material-ui/icons/Check";
import PlanPackages from "../PlanPackages/PlanPackages";
import Constants from "../../../components/constants/planConstant";
import AddBillDetail from "../AddBillDetail/AddBillDetail";
import { StripeProvider, Elements } from "react-stripe-elements";
import Divider from "@material-ui/core/Divider";
import Business from "../../../assets/images/plans/Business.svg";
import Premium from "../../../assets/images/plans/Premium.svg";
import PremiumIcon from "../../../components/Icons/PremiumIcon";
import BusinessIcon from "../../../components/Icons/BusinessIcon";
import Grid from "@material-ui/core/Grid";
import SvgIcon from "@material-ui/core/SvgIcon";
import AddBillSuccess from "../AddBillDetail/AddBillSuccess";
import {UpgradeTrailInStore } from "../../../redux/actions/userBillPlan";
import {TRIALPERIOD} from '../../../components/constants/planConstant';

import moment from "moment";
import { FormattedMessage,injectIntl } from "react-intl";

class NoPlan extends Component {
  constructor(props) {
    super();
    this.state = {
      openBillDetail: false,
      openPackages: false,
      packageDetails: null,
      packageMode: "",
      isTrialSuccess:false,
      mode:"",
      response: null
    };
    this.benefitsList = [
      {
        icon: "Premium",
        txt: props.intl.formatMessage({id:"team-settings.billing.no-plan.list.unlineted-proj-label" ,defaultMessage:"Unlimited Projects"}),
        tooltip: "Premium Features",
      },
      {
        icon: "Premium",
        txt: props.intl.formatMessage({id:"team-settings.billing.no-plan.list.unlock-gants-label" ,defaultMessage:"Unlock Gantt Charts"}),
        tooltip: "Premium Features",
      },
      {
        icon: "Premium",
        txt: props.intl.formatMessage({id:"team-settings.billing.no-plan.list.unlock-bulk" ,defaultMessage:"Unlock Boards"}),
        tooltip: "Premium Features",
      },
      {
        icon: "Premium",
        txt: props.intl.formatMessage({id:"team-settings.billing.no-plan.list.unlock-sort-label" ,defaultMessage:"Unlock Custom Statuses"}),
        tooltip: "Premium Features",
      },
      {
        icon: "Premium",
        txt: props.intl.formatMessage({id:"team-settings.billing.no-plan.list.unlock-custom-label" ,defaultMessage:"Unlock Custom Filters"}),
        tooltip: "Premium Features",
      },
      {
        icon: "Premium",
        txt: props.intl.formatMessage({id:"team-settings.billing.no-plan.list.priority-label" ,defaultMessage:"Priority Customer Support"}),
        tooltip: "Premium Features",
      },
      {
        icon: "Business",
        txt: props.intl.formatMessage({id:"team-settings.billing.cancel-subscription-confirmation.heres-missing.list.risk-mangement" ,defaultMessage:"Risk Management"}),
        tooltip: "Business Features",
      },
      {
        icon: "Business",
        txt: props.intl.formatMessage({id:"team-settings.billing.no-plan.list.storage" ,defaultMessage:"Upto 10 GB Storage"}),
        tooltip: "Business Features",
      },
    ];
  }
  /**
   * it is called to show package plan depending upon mode eithe upgrade or trial
   * @param {string} mode
   */
  showPackages = (mode) => {
    this.setState({ openPackages: true, packageMode: mode });
  };
  /**
   * it is called when select packge and in which mode the package was
   * @param {object} packageInfo
   */
  selectPackage = (packageInfo) => {
    const { packageMode } = this.state;
    const { profile } = this.props;
    if (Constants.TRIALPLANMODE == packageInfo.trailPaid) {
      this.setState({ openPackages: false });
      this.props.startTrialPlanBilling(packageInfo, (success) => {
        //Updating trail plan on userpilot code for production only
        this.setState({isTrialSuccess:true,mode:mode, response: success} );
        if (ENV == "production" && window.userpilot) {
          window.userpilot.identify(
            profile.userId, // Used to identify users
            {
              name: profile.fullName, // Full name
              email: profile.email, // Email address
              created_at: moment(
                profile.createdDate,
                "YYYY-MM-DDThh:mm:ss"
              ).format("x"),
              plan: success.data.company.paymentPlan.planTitle,
            }
          );
        }
      });
    } else if (Constants.UPGRADEPLANMODE == packageMode) {
      this.setState({
        openPackages: false,
        packageDetails: packageInfo,
        openBillDetail: true,
      });
    }
  };
  /**
   * it is called when upgrade plan and exit from add bill details dialog
   * @param {object} planData
   */
  paymentPaidHandler = (planData) => {
    if (this.props.paymentPaidDone) this.props.paymentPaidDone(planData);
  };
  /**
   * it is called when don't upgrade plan and exit from add bill details dialog
   */
  exitBillDetail = () => {
    this.setState({ openBillDetail: false });
  };
  /**
   * it is called when don't select package and exit from package dialog
   */
  exitPackageDialog = () => {
    this.setState({ openPackages: false });
  };
  successfullyExit = () => {
    this.props.UpgradeTrailInStore(this.state.response, success => {
      this.setState({ isTrialSuccess: false });
    })

  };
  render() {
    const { classes, minimumUser, theme } = this.props;
    const {
      openBillDetail,
      openPackages,
      packageDetails,
      packageMode
      ,isTrialSuccess,mode
    } = this.state;
    let planInfo = "";
    if (openBillDetail) {
      planInfo = { ...packageDetails };
      planInfo.pricePlanType =
        packageDetails.planType == Constants.PREMIUMPLAN
          ? Constants.PREMIUMPLANPRICE
          : Constants.BUSINESSPLANPRICE;
    }
    return (
      <div style={{ backgroundColor: "white", width: "fit-content" }}>
        <div className={classes.main}>
          <div className={classes.mainHeader}>
            <div className={classes.iconContainer}>
              <img
                src={upgradePro}
                alt="Pro Plan Image"
                className={classes.noPlanIcon}
              />
            </div>
            <div className={classes.noPlanInfoCnt}>
              <Typography variant="body2" className={classes.noPlanTxt}>
                <FormattedMessage id="team-settings.billing.no-plan.current-sub-label" defaultMessage="Current Subscription"/>
              </Typography>
              <Typography variant="h2" className={classes.basicPlan}>
               <FormattedMessage id="team-settings.billing.no-plan.title" defaultMessage="Basic Plan"/>
              </Typography>
              <Typography variant="body2" className={classes.upgradeNoPlanCnt}>
               <FormattedMessage id="team-settings.billing.no-plan.unlimited-label" defaultMessage="Upgrade to get unlimited access to all nTask features"/> 
              </Typography>
              <div style={{ marginTop: 16, display: "flex" }}>
                <CustomButton
                  btnType="blue"
                  variant="outlined"
                  onClick={() => this.showPackages(Constants.TRIALPLANMODE)}
                >
                  <FormattedMessage id="team-settings.billing.biling-plan-dialog.trial-button.label" defaultMessage={"Start {TRIALPERIOD}-day free trial"} values={{TRIALPERIOD: TRIALPERIOD}}/>
                </CustomButton>
                <div className={classes.seprator}>
                  <Divider className={classes.sepratorHrLeft} />
                  <div className={classes.sepratorText}>
                    <span><FormattedMessage id="team-settings.billing.trial-expired-modal.or-label" defaultMessage="OR"/></span>
                  </div>
                  <Divider className={classes.sepratorHrRight} />
                </div>
                {/* <span style={{padding: '0px 5px', fontSize: "15px !important"}}>OR</span> */}
                <CustomButton
                  btnType="blue"
                  variant="contained"
                  style={{ marginLeft: 0 }}
                  onClick={() => this.showPackages(Constants.UPGRADEPLANMODE)}
                >
                  <FormattedMessage id="common.action.upgrade.label" defaultMessage="Upgrade Now"/>
                </CustomButton>
              </div>
            </div>
          </div>
          <div className={classes.benefitslistCont}>
            <Typography variant="h2" className={classes.benefitsHeadCnt}>
              <FormattedMessage id="team-settings.billing.no-plan.benefits-label" defaultMessage="Benefits of upgrading to nTask Premium or Business Plan"/>
            </Typography>
            <div className={classes.benefitslist}>
              <Grid
                container
                spacing={2}
                direction="row"
                justify="space-between"
                alignItems="center"
              >
                {this.benefitsList.map((item) => {
                  return (
                    <Grid
                      item
                      classes={{ item: classes.workspaceListItem }}
                      xs={6}
                      lg={4}
                      xl={4}
                    >
                      <div className={classes.benefitCnt}>
                        {/* <img title={item.tooltip} src={item.icon == 'Premium' ? PremiumIcon22x22: BusinessIcon22x22} className={classes.benefitCntImg}/> */}
                        {item.icon == "Premium" ? (
                          <SvgIcon
                            viewBox="0 0 22 22"
                            className={classes.benefitCntImg}
                          >
                            <PremiumIcon />
                          </SvgIcon>
                        ) : (
                          <SvgIcon
                            viewBox="0 0 22 22"
                            className={classes.benefitCntImg}
                          >
                            <BusinessIcon />
                          </SvgIcon>
                        )}
                        <Typography
                          variant="h6"
                          className={classes.benefitCntTxt}
                        >
                          {item.txt}
                        </Typography>
                      </div>
                    </Grid>
                  );
                })}

                <Grid
                  item
                  classes={{ item: classes.workspaceListItem }}
                  xs={6}
                  lg={4}
                  xl={4}
                >
                  <div className={classes.benefitCnt}>
                    <SvgIcon
                      viewBox="0 0 22 22"
                      className={classes.benefitCntImg}
                    >
                      <BusinessIcon />
                    </SvgIcon>
                    <Typography variant="h6" className={classes.benefitCntTxt}>
                      <FormattedMessage id="team-settings.billing.no-plan.list.reporting" defaultMessage="Reporting"/>
                    </Typography>
                    <Typography
                      variant="h6"
                      style={{
                        color: theme.palette.text.brightGreen,
                        marginLeft: 3,
                      }}
                    >
                      {" "}
                      (<FormattedMessage id="team-settings.billing.biling-plan-dialog.list.comming-soon" defaultMessage="Comming Soon" />)
                    </Typography>
                  </div>
                </Grid>
              </Grid>
              {/* <ul >
                                <li><CheckIcon/><Typography variant="body2">Upto 10GB Storage</Typography></li>
                                <li><CheckIcon/><Typography variant="body2">Unlimited Projects</Typography></li>
                                <li><CheckIcon/><Typography variant="body2">Unlimited Issues & Risks</Typography></li>
                                <li><CheckIcon/><Typography variant="body2">Unlock Custom Statuses</Typography></li>
                            </ul>
                            <ul >
                                <li><CheckIcon/><Typography variant="body2">Unlock Gantt Charts</Typography></li>
                                <li><CheckIcon/><Typography variant="body2">Unlock Boards</Typography></li>
                                <li><CheckIcon/><Typography variant="body2">Unlock Custom Filters</Typography></li>
                                <li><CheckIcon/><Typography variant="body2">Priority Customer Support</Typography></li>
                            </ul> */}
            </div>
            <a
              href="https://www.ntaskmanager.com/pricing/"
              className={classes.linkStyle}
              target="_blank"
            >
              <FormattedMessage id="common.action.learn-more.label" defaultMessage="Learn More About Plans"/>
            </a>
          </div>
        </div>
        { isTrialSuccess &&
          <AddBillSuccess
            successfullyExit={this.successfullyExit}
            mode={mode+"Trial"}
          ></AddBillSuccess>
        }
        {openPackages && (
          <PlanPackages
            selectPackage={this.selectPackage}
            exitPackageDialog={this.exitPackageDialog}
            showSnackBar={this.props.showSnackBar}
            mode={packageMode}
          ></PlanPackages>
        )}
        {openBillDetail && (
          <StripeProvider apiKey={STRIPE_PUBLISH_KEY}>
            <Elements>
              <AddBillDetail
                showSnackBar={this.props.showSnackBar}
                paymentPaidHandler={(planData) =>
                  this.paymentPaidHandler(planData)
                }
                exitBillDetail={this.exitBillDetail}
                minimumUser={minimumUser}
                selectedPackageDetails={planInfo}
              ></AddBillDetail>
            </Elements>
          </StripeProvider>
        )}
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    profile: state.profile.data,
    minimumUser: state.profile.data.teamMember.filter(
      (item) =>
        item.isDeleted == false &&
        (item.isActive == null || item.isActive == true)
    ).length,
  };
};
export default compose(
  withRouter,
  injectIntl,
  connect(mapStateToProps, {UpgradeTrailInStore}),
  withStyles(classes, { withTheme: true })
)(NoPlan);
