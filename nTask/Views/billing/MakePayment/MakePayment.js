import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter, Link } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import classes from "./style";
import Typography from "@material-ui/core/Typography";
import CustomButton from "../../../components/Buttons/CustomButton";
import {
  CardNumberElement,
  CardExpiryElement,
  CardCVCElement,
  injectStripe,
} from "react-stripe-elements";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import creditCardLogo from "../../../assets/images/cc_logos.png";
import Divider from "@material-ui/core/Divider";
import Input from "@material-ui/core/Input";
import {
  MakePaymentPlan,
  FetchDueInvoice,
  FetchPaymentMethod,
} from "../../../redux/actions/userBillPlan";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import IconButton from "../../../components/Buttons/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import SelectSearchDropdown from "../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { generateSelectData } from "../../../helper/generateSelectData";
import isEmpty from "lodash/isEmpty";
import Constants from "../../../components/constants/planConstant";

class MakePayment extends Component {
  constructor(props) {
    super(props);
    this.labelRef = React.createRef();
    this.state = {
      paymentType: "creditcard",
      errorMessage: "",
      btnSubmitQuery: "",
      amountDue: 0,
      invoiceId: "",
      loading: true,
      cards: [],
      selectedCard: {},
      btnPromoQuery: "",
    };
  }

  /**
   * it is called to get due invoics  and payment methods of user
   */
  componentDidMount() {
    const { teamId } = this.props;
    this.props.FetchDueInvoice(
      teamId,
      result => {
        this.setState({
          amountDue: result.data.amountDue,
          invoiceId: result.data.id,
          loading: false,
        });
      },
      fail => {
        this.props.showSnackBar(fail.data);
        this.setState({ loading: false });
      }
    );
    this.props.FetchPaymentMethod(
      response => {
        this.readyCardData(response);
      },
      fail => {
        this.props.showSnackBar(fail.data);
      }
    );
  }
  /**
   * it is called for prepared data for cards
   * @param {object} response
   */
  readyCardData = response => {
    let cardsArr = response.data ? response.data.sources.data : [];
    let data = [];
    data = cardsArr.map(item => {
      return {
        label: item.brand + " card ends in " + item.last4,
        id: item.id,
      };
    });
    data.push({ label: "New Payment Method", id: "0000" });
    let selectedCard = { label: "New Payment Method", id: "0000" };
    if (response.data && response.data.default_source) {
      selectedCard = data.find(item => item.id == response.data.default_source);
    }
    this.setState({ cards: data, selectedCard });
  };
  /**
   * it is called when user change the card
   * @param {event} event
   */
  cardTypeChange = event => {
    this.setState({ paymentType: event.target.value });
  };
  /**
   * it is called when user change card number, card expire year and cvc and set error state
   * @param {Object} error
   */
  handleChange = ({ error }) => {
    if (error) {
      this.setState({ errorMessage: error.message });
    } else this.setState({ errorMessage: "" });
  };
  /**
   * it is called when user submit data and get token
   * @param {event} evt
   */
  handleSubmit = evt => {
    evt.preventDefault();
    const { selectedCard } = this.state;
    if (this.props.stripe) {
      this.setState({ btnSubmitQuery: "progress" }, () => {
        if (selectedCard.id == "0000") {
          this.props.stripe
            .createToken()
            .then(result => {
              if (result.error) {
                this.setState({ errorMessage: result.error.message, btnSubmitQuery: "" });
              } else if (result.token) {
                this.sendData(result.token.card.brand, result.token.id, "");
              }
            })
            .catch(err => {
              this.setState({ errorMessage: err.message, btnSubmitQuery: "" });
            });
        } else {
          this.sendData("", "", selectedCard.id);
        }
      });
    } else {
    }
  };
  /**
   * it is called when user send data for submit payment
   * @param {string} brand
   * @param {string} tokenId
   * @param {string} cardId
   */
  sendData = (brand, tokenId, cardId) => {
    const { invoiceId } = this.state;
    let data = {};
    data.invoiceId = invoiceId;
    data.cardId = cardId;
    data.stripeToken = tokenId;

    this.props.MakePaymentPlan(
      data,
      result => {
        this.props.showSnackBar(result.data);
        if (this.props.makePaymentHandler) this.props.makePaymentHandler();
      },
      fail => {
        this.props.showSnackBar(fail.data);
        this.setState({ btnSubmitQuery: "" });
      }
    );
  };
  /**
   * it is called to get total payment
   */
  getTotalPayment = () => {
    const { amountDue } = this.state;
    return amountDue / 100;
  };
  /**
   * it is called when user change card
   * @param {string} type
   * @param {string} item
   */
  handleCardSelect = (type, item) => {
    this.setState({ [type]: item });
  };
  /**
   * it is called to generate cards for selection control
   */
  generateCards = () => {
    let { cards } = { ...this.state };
    let ddData = cards.map((item, index) => {
      return generateSelectData(item.label, item.id, item.id, null, item);
    });
    return ddData;
  };
  /**
   * it is called to open bill details dialog
   */
  makePayment = () => {
    this.setState({ openBillDetail: true });
  };

  render() {
    const { classes, theme, teamMembers } = this.props;
    const { packageType, paymentPlanTitle } = this.props.subscriptionDetails;
    const planTitle = Constants.DisplayPlanName(paymentPlanTitle);
    const { paymentType, btnSubmitQuery, selectedCard } = this.state;
    const submitBtnLbl = "Make Payment";
    const amount = this.getTotalPayment().toFixed(2);
    return (
      <Dialog
        classes={{
          paper: classes.dialogPaperCnt,
          scrollBody: classes.dialogCnt,
        }}
        fullWidth={true}
        scroll="body"
        disableBackdropClick={true}
        disableEscapeKeyDown={true}
        open={true}
        onClose={() => {}}>
        <DialogTitle
          id="form-dialog-title"
          classes={{ root: classes.makePaymentDefaultDialogTitle }}>
          <div className={classes.makePaymentTitle}>
            <Typography variant="h2">Make Payment</Typography>
            <IconButton btnType="transparent" onClick={this.props.exitMakePayment}>
              <CloseIcon htmlColor={theme.palette.secondary.medDark} />
            </IconButton>
          </div>
        </DialogTitle>
        <DialogContent>
          <div className={classes.makePaymentDetail}>
            <div className={classes.teammemberCnt}>
              <Typography variant="h5" style={{ fontSize: "13px" }}>
                No. of team members
              </Typography>
              <Typography variant="h5" style={{ fontSize: "13px" }}>
                {teamMembers}
              </Typography>
            </div>
            <div className={classes.teammemberCnt}>
              <Typography variant="h5" style={{ fontSize: "13px" }}>
                Subscription Plan
              </Typography>
              <Typography variant="h5" style={{ fontSize: "13px" }}>
                {planTitle}
              </Typography>
            </div>
            <div className={classes.teammemberCnt}>
              <Typography variant="h5" style={{ fontSize: "13px !important" }}>
                Billing Cycle
              </Typography>
              <Typography variant="h5" style={{ textTransform: "capitalize" }}>
                {packageType}
              </Typography>
            </div>
            <div className={classes.teammemberCnt}>
              <Typography variant="h5" style={{ fontSize: "13px !important" }}>
                Bill Due Amount
              </Typography>
              <Typography variant="h5" style={{ textTransform: "capitalize" }}>
                ${amount}
              </Typography>
            </div>
            <div style={{ marginTop: 10 }}>
              <SelectSearchDropdown
                data={() => {
                  return this.generateCards();
                }}
                styles={{ margin: 0, marginRight: 20, flex: 1 }}
                isClearable={false}
                label=""
                selectChange={this.handleCardSelect}
                type="selectedCard"
                selectedValue={selectedCard}
                placeholder={""}
                isMulti={false}
              />
            </div>
            {selectedCard.id == "0000" ? (
              <Fragment>
                <FormControl component="fieldset" className={classes.formControl}>
                  <RadioGroup
                    className={classes.cardRadiogroup}
                    aria-label="card"
                    name="card"
                    value={paymentType}
                    onChange={this.cardTypeChange}>
                    <FormControlLabel
                      value="creditcard"
                      control={<Radio />}
                      label={
                        <div className={classes.creditCardRadioDiv}>
                          <span className={classes.radioBtnLbl} style={{ paddingRight: 10 }}>
                            Credit Card
                          </span>
                          <img
                            src={creditCardLogo}
                            alt="credit card details"
                            className={classes.creditCard}
                          />
                        </div>
                      }></FormControlLabel>
                    {/* <FormControlLabel value="paypal" disabled control={<Radio />} label={<div><span className={classes.radioBtnLbl} style={{display: 'inline'}}>PayPal</span><span className={classes.comingsoon}>coming soon...</span></div>} /> */}
                  </RadioGroup>
                </FormControl>
                <form>
                  <div className={this.state.errorMessage.length ? classes.error : ""} role="alert">
                    {this.state.errorMessage}
                  </div>
                  {paymentType == "creditcard" ? (
                    <Fragment>
                      <label>
                        <span className={classes.lblStyle}>Card number</span>
                        <CardNumberElement
                          className={classes.cardNumberCnt}
                          onChange={this.handleChange}
                        />
                      </label>
                      <div className={classes.container}>
                        <label>
                          <span className={classes.lblStyle}>Expiration date</span>
                          <CardExpiryElement
                            className={classes.expiryElement}
                            onChange={this.handleChange}
                          />
                        </label>
                        <label>
                          <span className={classes.lblStyle}>CVC</span>
                          <CardCVCElement
                            className={classes.cvcElement}
                            onChange={this.handleChange}
                          />
                        </label>
                      </div>
                    </Fragment>
                  ) : null}
                </form>
              </Fragment>
            ) : null}
          </div>
          <div style={{ marginLeft: 20 }}>
            <div>
              <CustomButton
                btnType="blue"
                variant="contained"
                style={{ marginBottom: 0, marginRight: 15, minWidth: 120 }}
                onClick={this.handleSubmit}
                query={btnSubmitQuery}
                disabled={btnSubmitQuery === "progress"}>
                {submitBtnLbl}
              </CustomButton>
              <Link
                className={classes.linkStyle}
                to="/"
                onClick={e => e.preventDefault()}
                style={{ display: "none" }}>
                Do it later
              </Link>
            </div>
            <Typography variant="body2" style={{ marginTop: 10 }}>
              By continuing, you agree to our{" "}
              <a
                href="https://www.ntaskmanager.com/terms-conditions/"
                className={classes.linkStyle}
                target="_blank">
                terms and conditions.
              </a>
            </Typography>
          </div>
        </DialogContent>
      </Dialog>
    );
  }
}

const mapStateToProps = state => {
  return {
    subscriptionDetails: state.profile.data.teams.find(
      item => item.companyId == state.profile.data.activeTeam
    ).subscriptionDetails,
    teamId: state.profile.data.activeTeam,
    teamMembers: state.profile.data.teamMember.length,
  };
};
export default compose(
  withRouter,
  connect(mapStateToProps, {
    MakePaymentPlan,
    FetchDueInvoice,
    FetchPaymentMethod,
  }),
  withStyles(classes, { withTheme: true })
)(injectStripe(MakePayment));
