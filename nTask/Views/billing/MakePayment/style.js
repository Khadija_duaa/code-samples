import planBgImg from "../../../assets/images/bg_pattern.png"
const MakePayment = theme => ({

    dialogPaperCnt: {
        overflowY: "visible",
        background: theme.palette.common.white,
        width: "600px!important"
    },
    fontSize: {
        fontSize: "13px !important"
      },
    makePaymentDefaultDialogTitle: {
      padding: '20px 20px 20px 25px',
      borderTopRightRadius: 'inherit',
      borderTopLeftRadius: 'inherit',
      marginBottom: 10,
      backgroundColor: theme.palette.background.light,
      borderBottom: `1px solid ${theme.palette.background.contrast}`,
    },
    makePaymentTitle: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      fontWeight: theme.typography.fontWeightRegular,
    },
    teammemberCnt: {
        display: 'flex', 
        marginTop: 25, 
        justifyContent: 'space-between', 
        alignItems: 'center',
    },
    teammembers: {
        '& input[type=number]::-webkit-inner-spin-button': {
            '-webkit-appearance': 'none',
            background: `#FFF url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAPCAYAAADZCo4zAAAArUlEQVQoU82RsRHDIAxF+RVtNkla48ZMEmcDj+ARskE8QjYgDdAmm7ilsXziEKcRQsFJ+u/0JYBRJ+c8Hcexj+P4lTIkyDnPRPTiHMBjGIatxnw18UlEUwM+ABaGoEVpHWO8AagQUkpvIlq1L3dp0Npn0MPq+C+AlNJSStm897seLoRwsdbeeU1+sWspxQvUxGCM+dUtNMS5tbaKzrm5rylQs6li/wvxbpARkesnsYNeejnucREAAAAASUVORK5CYII=') no-repeat center center;`,
            width: 15,
            height: 30,
            opacity: 1, /* shows Spin Buttons per default (Chrome >= 39) */
            position: 'absolute',
            top: 0,
            right: 10,
            bottom: 0,
        },
        '& input': {
            border: `1px solid ${theme.palette.border.lightBorder}`,
            paddingLeft: 15,
            paddingRight: 26,
            borderRadius: 4,
            fontSize: "14px !important",
        },
    },
	makePaymentDetail: {
        padding: 20,
        borderRadius: 4,
        background: theme.palette.common.white,
    },
    formControl: {
        width: '100%',
    },
    cardRadiogroup : {
        flexDirection: 'row',
        width: '100%',
        // justifyContent: 'space-around',
    },
    creditCardRadioDiv : {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingRight: 50,
    },
    radioBtnLbl : {
        display: 'flex',
        color: theme.palette.text.primary,
    },
    comingsoon: {
        fontSize: "7px !important",
        letterSpacing: 1,
        marginLeft: 5
    },
    cardNumberCnt: {
        padding: 10,
        border: `1px solid ${theme.palette.border.lightBorder}`,
        borderRadius: 4,
        marginTop: 9,
    },
    container: {
        display: 'flex',
        marginTop: 15,
        width: '100%',
        justifyContent: 'space-between',
        '& label': {
            width: '45%',
        }
    },
    lblStyle: {
        display: 'flex',
        marginTop: 9,
        fontSize: "10px !important",
        color: theme.palette.secondary.light,
    },
    expiryElement: {
        padding: 10,
        border: `1px solid ${theme.palette.border.lightBorder}`,
        borderRadius: 4,
        marginTop: 9,
    },
    cvcElement: {
        padding: 10,
        border: `1px solid ${theme.palette.border.lightBorder}`,
        borderRadius: 4,
        marginTop: 9,
    },
    linkStyle : {
        color: theme.palette.secondary.light,
        fontSize: "12px !important",
    },
    error : {
        // marginTop: 10,
        fontSize: "13px !important",
        color: `${theme.palette.text.danger}!important`,
        padding: 10,
        backgroundColor: `${theme.palette.background.dangerLight}!important`,
        border: '1px!important',
        borderRadius: 4,
    },
});
export default MakePayment;