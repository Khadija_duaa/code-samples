
// import planBgImg from "../../../assets/images/bg_pattern.png"
const PaymentOverdueStyles = theme => ({ 

    // paymentOverdueDetailsDialog: {
        // overflowY: "auto", 
    // },
    dialogPaperCnt: {
        overflowY: "visible",
        background: theme.palette.common.white,
        width: "530px!important"
    },
    defaultDialogContent: {
        padding: '40px 40px 40px 40px !important',
        // background: `url(${planBgImg})`,
        backgroundRepeat: "no-repeat !important",
        backgroundPositionY: "-100px !important",
    },
    paymentOverdueBody: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    bodyHeaderMain: {
        marginTop: 24,
    },
    bodyHeaderTopContent : {
        display: 'flex',
        textAlign: 'center',
        flexDirection: 'column',
        textTransform: 'capitalize',
    },
    workspaceName: {
        marginTop: 5,
        color: theme.palette.secondary.main,
    },
    bodyHeaderContent: {
        marginTop: 18,
        display: 'flex',
        textAlign: 'center',
    },
    bodyHeaderContentChild: {
        marginTop: 2,
        display: 'flex',
        textAlign: 'center',
        padding : "0 5px"
    },
    bodyHeaderBtns: {
        marginTop: 30,
        display: 'flex',
        justifyContent: 'space-evenly',
        width: '100%',
        paddingLeft: 40,
        paddingRight: 40,
    },
    seprator: {
        marginTop: 30,
        display: 'flex',
        width: '100%',
        alignItems: 'center',
    },
    sepratorText: {
        fontSize: "12px !important",
        width: 36,
        height: 36,
        border: `1px solid ${theme.palette.border.lightBorder}`,
        borderRadius: '50%',
        alignItems: 'center',
        display: 'flex',
        placeContent: 'center',
    },
    sepratorHrLeft: {
        marginRight: 5,
        marginLeft: 0,
        width: 'calc(50% - 24px)',
    },
    sepratorHrRight : {
        marginLeft: 5,
        marginRight: 0,
        width: 'calc(50% - 24px)',
    },
    paymentOverdueBodyBottom: {
        marginTop: 10,
        width: '100%',
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
    },
    paymentOverdueBodyBottomBtn: {
        paddingTop: 30,
        width: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },
    selectionDiv: {
        width: '60%',
        textAlign: 'left'
    },
     defaultDialogTitle: {
    padding: "15px 20px",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    userSelect: "none",
  },
  arrowBackCnt: {
    background: "#F3F3F3",
    height: "30px",
    width: "31px",
    borderRadius: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    marginRight: "10px",
    alignItems: "center",
    cursor: "pointer",
  },
  arrowBackIcon: {
    fontSize: "12px !important",
    marginLeft: 5,
  },
  title: {
    fontWeight: 700,
    fontFamily: theme.typography.fontFamilyLato,
    color: "#202020",
    fontSize: "18px !important",
  },
  messageCnt: {
    textAlign: "center",
    display: "flex",
    flexDirection: "column"
  },
  message: {
    color: "#202020",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
    fontSize: "15px !important",
    lineHeight: 1.6,
  },
  toggleContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
    marginBottom: 25,
  },
  toggleBtnGroup: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    flexWrap: "nowrap",
    width: "100%",
    borderRadius: 4,
    background: theme.palette.common.white,
    // border: `1px solid ${theme.palette.border.lightBorder}`,
    height: 40,
    boxShadow: "none",
    "& $toggleButtonSelected": {
      color: theme.palette.text.secondary,
      backgroundColor: "white",
      "&:after": {
        background: theme.palette.common.white,
      },
      "&:hover": {
        background: theme.palette.common.white,
      },
    },
  },
  toggleButton: {
    height: "auto",
    padding: "4px 20px 5px",
    fontSize: "13px !important",
    fontWeight: theme.palette.fontWeightMedium,
    fontFamily: theme.typography.fontFamilyLato,
    textTransform: "capitalize",
    width: "100%",
    background: "#F6F6F6",
    "&:hover": {
      background: theme.palette.common.white,
    },
    "&[value = 'mappManually']": {
      //  borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
      borderRight: `1px solid ${theme.palette.border.lightBorder}`,
    },
  },
  toggleButtonSelected: {},
  toggleContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
    marginBottom: 25,
  },
  openIcon: {
    fontSize: "11px !important",
    color: "#ABB4BB",
    margin: "2px 3px 0px 3px",
  },
  labelCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    fontSize: "13px !important",
    color: "#202020",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
    marginBottom: 10,
  },
  labelTitleLeft: {
    marginLeft: 12,
  },
  iconShowHide: {
    color: "#7E7E7E",
    marginBottom: -7,
    cursor: "pointer",
  },
  labelTitleRight: {
    marginRight: 65,
    color: "#969696",
  },
  statusesCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-around",
    marginBottom: 10,
  },
   statusTitle: {
    // maxWidth : 50,
    overflow: "hidden",
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
  },
  statusesIcon: {
    marginRight: 5,
    fontSize: "12px !important",
  },
   taskCountTitle: {
    fontSize: "14px !important",
    color: "#202020",
    fontWeight: theme.palette.fontWeightMedium,
    fontFamily: theme.typography.fontFamilyLato,
  },
  arrowRight: {
    width: 15,
    marginBottom: -7
    // right: -12,
    // height: 45,
    // zIndex: 1,
    // top: -1,
    // position: "absolute",
  },
  statusIcon: {
    fontSize: "11px !important",
    marginRight: 5,
  },

});
export default PaymentOverdueStyles;