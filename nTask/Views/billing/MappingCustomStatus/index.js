import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import classes from "./style";
import Typography from "@material-ui/core/Typography";
import CustomAvatar from "../../../components/Avatar/Avatar";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import SelectSearchDropdown from "../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import ButtonActionsCnt from "../../../components/Dialog/ConfirmationDialogs/ButtonActionsCnt";
import { generateCompaniesData } from "../../../helper/generateSelectData";
import CustomButton from "../../../components/Buttons/CustomButton";
import Divider from "@material-ui/core/Divider";
import { StripeProvider, Elements } from "react-stripe-elements";
import {
  switchTeam,
  whiteLabelInfo,
  DefaultWhiteLabelInfo,
  mapTeamTaskStatus
} from "../../../redux/actions/teamMembers";
import { FetchUserInfo } from "../../../redux/actions/profile";
import moment from "moment";
import Constants from "../../../components/constants/planConstant";
import RestrictedAccess from "../RestrictedAccess/RestrictedAccess";
import DefaultDialog from "../../../components/Dialog/Dialog";
import CreateTeamForm from "../../AddNewForms/addNewTeamFrom";
import PlanPackages from "../PlanPackages/PlanPackages";
import ConstantsPlan from "../../../components/constants/planConstant";
import planBgImg from "../../../assets/images/bg_pattern.png";
import Grid from "@material-ui/core/Grid";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import { FormattedMessage, injectIntl } from "react-intl";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import ToggleButton from "@material-ui/lab/ToggleButton";
import Brightness1Icon from "@material-ui/icons/Brightness1";
import { getTeamProjectStatus } from "../../../redux/actions/team";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import { Scrollbars } from "react-custom-scrollbars";
import isEmpty from "lodash/isEmpty";

class Index extends Component {
  constructor(props) {
    super();
    this.state = {
      selectedTeam: [],
      btnSwitchQuery: "",
      activeStep: 0,
      currentView: "mappManually",
      projectsArr:[],
      taskCount: 0
    };
  }
  /**
   * it is called when user exit from overdue dialog
   */
  exitDialog = () => {
    if (this.props.closeMappingStatus) this.props.closeMappingStatus();
  };
  /**
   * it is called when user change his team when payment overdue of his current team
   * @param {string} key
   * @param {string} value
   */
  handleTeamChange = (key, value) => {
    this.setState({ [key]: value });
  };
  /**
   * it is called when user exit from make payment dialog
   */
  exitMakePayment = () => {
    this.setState({ makePayment: false });
  };
  /**
   * it is called when user switch to other team and its check that other team is either
   * on free plan or trial/paid valid
   * @param {string} type
   * @param {string} item
   */
  switchToOtherTeam = (type, item) => {
    const { selectedTeam } = this.state;
    if (!selectedTeam.id) return;
    // this.props.switchExpirePlan();
    // this.props.closeMappingStatus();
    // this.props.showLoadingState();
    this.setState({ btnSwitchQuery: "progress" });
    switchTeam(selectedTeam.id, response => {
      if (selectedTeam.isEnableWhiteLabeling && selectedTeam.isEnableWhiteLabeling == true)
        this.props.whiteLabelInfo(selectedTeam.id);
      else this.props.DefaultWhiteLabelInfo();
      this.props.FetchUserInfo(userApiResponse => {
        // this.setState({btnSwitchQuery: ''});
        this.props.history.push(`/teams/${response.data.team.companyUrl}`);

        const { gracePeriod } = this.props;
        const { paymentPlanTitle, currentPeriondEndDate } = response.data.team.subscriptionDetails;
        if (!ConstantsPlan.isPlanFree(paymentPlanTitle)) {
          if (
            ConstantsPlan.isPlanTrial(paymentPlanTitle) &&
            ConstantsPlan.isPlanExpire(currentPeriondEndDate)
          ) {
            this.setState({ btnSwitchQuery: "" });
            this.props.showLoadingState();
            this.props.planExpire("Trial");
            return;
          } else if (
            ConstantsPlan.isPlanPaid(paymentPlanTitle) &&
            ConstantsPlan.isPlanExpire(currentPeriondEndDate, gracePeriod)
          ) {
            this.setState({ btnSwitchQuery: "" });
            this.props.showLoadingState();
            this.props.planExpire("Paid");
            return;
          }
        }
        this.exitDialog();
      });
      this.props.hideLoadingState();
    });
  };
  /**
   * it is called when on overdue user want to open a create new team dialog
   */
  openCreateTeam = () => {
    this.setState({ openDialog: true });
  };
  /**
   * it is called when user close the create new team dialog exit
   */
  handleDialogClose = () => {
    this.setState({ openDialog: false });
  };
  /**
   * it is called when on overdue user select to create new team
   */
  createTeam = () => {
    this.props.createTeam();
  };
  /**
   * it is called when user open a make payment dialog
   */
  makePayment = () => {
    this.setState({ makePayment: true });
  };

 handleChangeTab = (event, nextView) => {
    this.setState({ currentView: nextView });
  };
  successAction=()=>{
    const {projectsArr, currentView} = this.state;
     let mapsProjectStatus = [];
      let mappingStatus = [];
      if (!isEmpty(projectsArr)) {
        projectsArr.map(p => {
          p.statuses.map(s => {
            mappingStatus.push({
              statusIDFrom: s.status.statusId,
              statusIDTo:
                currentView == "mappManually" ? (s.selectedStatus ? s.selectedStatus.value : 0) : 0,
            });
          });
          mapsProjectStatus.push({
            projectId: p.item.projectId,
            workspaceId: p.item.workspaceId,
            projectTitle: p.item.projectName,
            mappingStatus: mappingStatus,
          });
        });
      }
      this.props.mapTeamTaskStatus(
        {mapsProjectStatus , isStatusMappingRequired: false},
        succ => {
          window.location.reload();
        },
        fail => {
          window.location.reload();
          this.props.showSnackBar("Oops! Something went wrong.", "error")
        }
      );
  }
    generateStatusArr = () => {
        const {statusIcon,classes, theme} = this.props;
        const statusColor = theme.palette.taskStatusNew;
    return [
      {
        label: "Not Started",
        value: 0,
        icon: (
          <Brightness1Icon htmlColor={statusColor.NotStarted} className={classes.statusIcon} />
        ),
      },
      {
        label: "In Progress",
        value: 1,
        icon: (
          <Brightness1Icon htmlColor={statusColor.InProgress} className={classes.statusIcon} />
        ),
      },
      {
        label: "In Review",
        value: 2,
        icon: <Brightness1Icon htmlColor={statusColor.InReview} className={classes.statusIcon} />,
      },
      {
        label: "Completed",
        value: 3,
        icon: (
          <Brightness1Icon htmlColor={statusColor.Completed} className={classes.statusIcon} />
        ),
      },
      {
        label: "Cancelled",
        value: 4,
        icon: (
          <Brightness1Icon htmlColor={statusColor.Cancelled} className={classes.statusIcon} />
        ),
      },
    ];
  };
  mappingProjectArr = (itemArr, taskCounts) => {
    let projectInfo = [];
    itemArr.map(element => {
      element.projectInfo.map(p => {
        p.workspaceId = element.workspaceId;
        p.workspaceName = element.workspaceName;
        let arr = p.projectStatus.map(s => {
          return {
            status: s,
            selectedStatus: null,
          };
        });
        projectInfo.push({
          item: p,
          statuses: arr,
          isView: true,
          selectedStatus: null,
        });
      });
    });
    this.setState({projectsArr : projectInfo, taskCount : taskCounts});
  };
  handleSelectStatus = (key, opt, id, status) => {
    let updatedState = this.state.projectsArr.map(p => {
      if (p.item.projectId == id) {
        p.statuses = p.statuses.map(s => {
          if (s.status.statusId == status.statusId) {
            s.selectedStatus = opt;
          }
          return s;
        });
      }
      return p;
    });
    this.setState({projectsArr : updatedState});
  };
  handleViewClose = ele => {
    let updatedState = this.state.projectsArr.map(p => {
      if (p.item.projectId == ele.item.projectId) {
        p.isView = !p.isView;
        return p;
      } else return p;
    });
      this.setState({projectsArr : updatedState});
  };

  componentDidMount(){
    this.props.getTeamProjectStatus(
      succ => {
        this.mappingProjectArr(succ.teamsInfo, succ.taskCounts);
        // setTotalTaskCount(succ.taskCounts);
      },
      fail => {
      }
    );
  }

  render() {
    const { classes, teams, isOwner, companyName, minimumUser, intl, theme } = this.props;
    const {
      makePayment,
      selectedTeam,
      openDialog,
      btnSwitchQuery,
      activeStep,
      currentView,
      projectsArr,
      taskCount
    } = this.state;
    const { packageType, currentPeriondEndDate, paymentPlanTitle, trialType } = this.props.subscriptionDetails;
    let expiryDate = moment(currentPeriondEndDate).format("MMMM DD, YYYY");
    let planName = Constants.DisplayPlanName(paymentPlanTitle);
    let trailExpire = trialType == "BusinessTrial" ? true : false;
    return (
      <Fragment>
        <DefaultDialog
          title="Create Team"
          sucessBtnText="Create Team"
          open={openDialog}
          onClose={this.handleDialogClose}>
          <CreateTeamForm
            handleDialogClose={this.handleDialogClose}
            createTeamHandler={this.createTeam}
          />
        </DefaultDialog>
        {true && (
          <Dialog
            classes={{
              paper: classes.dialogPaperCnt,
              scrollBody: classes.dialogCnt,
            }}
            fullWidth={true}
            scroll="body"
            disableBackdropClick={true}
            disableEscapeKeyDown={true}
            open={true}
            onClose={this.exitDialog}>
            {activeStep == 1 && (
              <DialogTitle
                id="form-dialog-title"
                classes={{
                  root: classes.defaultDialogTitle,
                }}>
                <Grid container direction="row" justify="space-between" alignItems="center">
                  <Grid
                    item
                    style={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
                    <div
                      className={classes.arrowBackCnt}
                      onClick={() => {
                        this.setState({ activeStep: 0 });
                      }}>
                      <ArrowBackIosIcon className={classes.arrowBackIcon} />
                    </div>
                    <Typography variant="h2" className={classes.title}>
                      {"Task Status Mapping"}
                    </Typography>
                  </Grid>
                </Grid>
              </DialogTitle>
            )}

            <DialogContent
              className={classes.defaultDialogContent}
              style={{ background: activeStep == 0 ? `url(${planBgImg})` : "" }}>
              {activeStep == 0 && (
                <div className={classes.paymentOverdueBody}>
                  <CustomAvatar userActiveTeam={true} size="large" />
                  <Typography variant="h2" className={classes.bodyHeaderMain}>
                    <span className={classes.bodyHeaderTopContent}>
                      Task Status Mapping
                      <br />
                      <span className={classes.workspaceName}>{companyName}</span>
                    </span>
                  </Typography>
                  <Typography variant="h6" className={classes.bodyHeaderContent}>
                    {trailExpire
                      ? "Looks like your nTask Business Trial has expired"
                      : isOwner
                      ? "Looks like you recently cancelled your nTask Business subscription of your team"
                      : "Looks like team owner recently cancelled nTask Business subscription of your team"}
                  </Typography>
                  <Typography variant="h6" className={classes.bodyHeaderContentChild}>
                    {
                      "In result of that, status mapping of custom task status to Standard task status is required. "
                    }
                    {isOwner
                      ? "Click on the button below to start status mapping."
                      : "Please contact your team owner to perform this activity."}
                  </Typography>
                  {isOwner && (
                    <>
                      <div className={classes.bodyHeaderBtns}>
                        <CustomButton
                          btnType="blue"
                          variant="contained"
                          onClick={() => {
                            this.setState({ activeStep: 1 });
                          }}
                          style={{ padding: "7px 20px" }}>
                          Map Custom Status
                        </CustomButton>
                      </div>
                      <div className={classes.seprator}>
                        <Divider className={classes.sepratorHrLeft} />
                        <div className={classes.sepratorText}>
                          <span>OR</span>
                        </div>
                        <Divider className={classes.sepratorHrRight} />
                      </div>
                    </>
                  )}
                  <div className={classes.paymentOverdueBodyBottom}>
                    {teams.length ? (
                      <Fragment>
                        {/* <Typography variant="h6" className={classes.bodyHeaderContent}>
                        Meanwhile you can continue your work by switching to one of the appended
                        options.
                      </Typography> */}
                        <div className={classes.paymentOverdueBodyBottomBtn}>
                          <div className={classes.selectionDiv}>
                            <SelectSearchDropdown
                              data={() => generateCompaniesData(teams)}
                              label="Switch to Another Team"
                              isMulti={false}
                              selectChange={this.handleTeamChange}
                              type="selectedTeam"
                              selectedValue={selectedTeam}
                              placeholder="Switch to:"
                              styles={{ marginTop: 0, marginBottom: 0 }}
                            />
                          </div>
                          <CustomButton
                            btnType="success"
                            variant="contained"
                            onClick={this.switchToOtherTeam}
                            query={btnSwitchQuery}
                            disabled={btnSwitchQuery === "progress"}>
                            Switch
                          </CustomButton>
                        </div>
                      </Fragment>
                    ) : (
                      <div
                        style={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
                        <Typography variant="h6" className={classes.bodyHeaderContent}>
                          Meanwhile you can continue working by creating a new team.
                        </Typography>
                        <CustomButton
                          btnType="success"
                          variant="contained"
                          onClick={this.openCreateTeam}
                          style={{ marginTop: 20 }}>
                          Create Your Team
                        </CustomButton>
                      </div>
                    )}
                  </div>
                </div>
              )}
              {activeStep == 1 && (
                <>
                  <div className={classes.messageCnt}>
                    <span className={classes.message}>
                      <span style={{ fontWeight: 700 }}>
                        {" "}
                        {`${taskCount} ${intl
                          .formatMessage({ id: "task.label", defaultMessage: "tasks" })
                          .toLowerCase()}`}{" "}
                      </span>
                      require mapping against nTask Standard status.
                    </span>
                    <span className={classes.message}>How should we handle these tasks?</span>
                  </div>
                  <Divider style={{ margin: "20px 0" }} />

                  <div className={classes.toggleContainer}>
                    <ToggleButtonGroup
                      value={currentView}
                      exclusive
                      onChange={this.handleChangeTab}
                      classes={{ root: classes.toggleBtnGroup }}>
                      <ToggleButton
                        value="mappManually"
                        classes={{
                          root: classes.toggleButton,
                          selected: classes.toggleButtonSelected,
                        }}>
                        Map Status Manually
                      </ToggleButton>
                      <ToggleButton
                        value="allOpen"
                        classes={{
                          root: classes.toggleButton,
                          selected: classes.toggleButtonSelected,
                        }}>
                        Change all to{" "}
                        <Brightness1Icon
                          style={{ color: "#FC7150" }}
                          className={classes.openIcon}
                        />{" "}
                        {`Not Started`}
                      </ToggleButton>
                    </ToggleButtonGroup>
                  </div>
                  <Scrollbars autoHide={true} autoHeight autoHeightMin={0} autoHeightMax={300}>
                    {currentView == "mappManually" && (
                      <>
                        {projectsArr.length > 0 &&
                          projectsArr.map((p, index) => {
                            return (
                              <>
                                <div className={classes.labelCnt}>
                                  {" "}
                                  <span
                                    className={classes.labelTitleLeft}
                                    onClick={() => this.handleViewClose(p)}>
                                    {" "}
                                    {p.isView ? (
                                      <ArrowDropDownIcon className={classes.iconShowHide} />
                                    ) : (
                                      <ArrowRightIcon className={classes.iconShowHide} />
                                    )}
                                    {`${p.item.projectName} (${p.item.workspaceName})`}
                                  </span>
                                  {index == 0 && (
                                    <span className={classes.labelTitleRight}>
                                      Standard Status
                                    </span>
                                  )}
                                </div>
                                {p.isView &&
                                  p.statuses.map((s, indexKey) => {
                                    return (
                                      <>
                                        <div className={classes.statusesCnt} key={indexKey}>
                                          <CustomButton
                                            btnType="white"
                                            variant="contained"
                                            style={{
                                              fontSize: "14px",
                                              color: "#202020",
                                              border: "none",
                                              background: "#F6F6F6",
                                              marginLeft: 0,
                                              fontFamily: theme.typography.fontFamilyLato,
                                              fontWeight: theme.palette.fontWeightMedium,
                                              padding: "7px 50px 7px 9px",
                                              width: 170,
                                              display: "flex",
                                              flexDirection: "row",
                                              justifyContent: "end",
                                            }}>
                                            <Brightness1Icon
                                              className={classes.statusesIcon}
                                              style={{ color: s.status.colorCode }}
                                            />
                                            <span
                                              className={classes.statusTitle}
                                              title={s.statusName}>
                                              {s.status.statusName}{" "}
                                            </span>
                                          </CustomButton>
                                          <span className={classes.taskCountTitle}>
                                            {" "}
                                            {s.status.taskCount} tasks{" "}
                                            <ArrowForwardIcon className={classes.arrowRight} />{" "}
                                          </span>

                                          <SelectSearchDropdown
                                            data={this.generateStatusArr}
                                            label={null}
                                            selectChange={(key, opt) => {
                                              this.handleSelectStatus(
                                                key,
                                                opt,
                                                p.item.projectId,
                                                s.status
                                              );
                                            }}
                                            type="status"
                                            selectedValue={s.selectedStatus}
                                            placeholder={"Select status"}
                                            icon={true}
                                            isMulti={false}
                                            isDisabled={false}
                                            styles={{ width: 170, margin: 0 }}
                                          />
                                        </div>
                                      </>
                                    );
                                  })}
                              </>
                            );
                          })}
                      </>
                    )}
                  </Scrollbars>
                </>
              )}
            </DialogContent>

            {activeStep == 1 && (
              <ButtonActionsCnt
                cancelAction={() => {
                  this.setState({ activeStep: 0 });
                }}
                successAction={this.successAction}
                successBtnText={"Confirm"}
                cancelBtnText={"Go Back"}
                // cancelBtnProps={{ style: { color: theme.palette.text.azure, marginRight: 15 } }}
                btnType={"blue"}
                btnQuery={""}
                disabled={false}
              />
            )}
          </Dialog>
        )}
        {makePayment == false && isOwner == false && (
          <RestrictedAccess
            closeRestrictedDialog={this.props.closeMappingStatus}
            createTeam={this.createTeam}
            mode={"Plan"}
            hideLoadingState={this.props.hideLoadingState}
            showLoadingState={this.props.showLoadingState}
            planExpire={this.props.planExpire}
            switchExpirePlan={this.props.switchExpirePlan}></RestrictedAccess>
        )}
      </Fragment>
    );
  }
}
Index.defaultProps = {
  subscriptionDetails: {},
  isOwner: true,
  companyName: "",
  minimumUser: 0,
  companyId: "",
  gracePeriod: null,
};

const mapStateToProps = state => {
  return {
    subscriptionDetails:
      state.profile.data.teams.find(item => item.companyId == state.profile.data.activeTeam)
        .subscriptionDetails || {},
    isOwner: state.profile.data.teamMember.find(item => item.userId == state.profile.data.userId)
      .isOwner,
    teams: state.profile.data.teams.filter(item => item.companyId != state.profile.data.activeTeam),
    companyName: state.profile.data.teams.find(
      item => item.companyId == state.profile.data.activeTeam
    ).companyName,
    minimumUser: state.profile.data.teamMember.length,
    companyId: state.profile.data.activeTeam,
    gracePeriod: state.profile.data.gracePeriod,
  };
};
export default compose(
  withRouter,
  injectIntl,
  connect(mapStateToProps, {
    FetchUserInfo,
    whiteLabelInfo,
    DefaultWhiteLabelInfo,
    getTeamProjectStatus,
    mapTeamTaskStatus
  }),
  withStyles(classes, { withTheme: true })
)(Index);
