import betaDialogBgImg from "../../../assets/images/bg_pattern.png";
const UpdateSubscriptionStyles = theme => ({

    updateDlgPaperCnt: {
        overflowY: "visible",
        background: theme.palette.common.white,
        width: "520px!important",
    },
    fontSize: {
        fontSize: "13px !important"
      },
    confirmeDlgPaperCnt: {
        overflowY: "visible",
        background: theme.palette.common.white,
        width: "520px!important",
        background: `url(${betaDialogBgImg})`,
        backgroundRepeat: "no-repeat",
        backgroundColor: theme.palette.common.white,
        backgroundPositionY: '-100px',
    },
    updateDefaultDialogTitle: {
        borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
        padding: '20px 18px 20px 25px',
        borderTopLeftRadius: 'inherit',
        borderTopRightRadius: 'inherit',
        backgroundColor: theme.palette.background.light,
    },
    confirmDefaultDialogTitle: {
        // borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
        padding: '10px 10px 0px 0px',
        display: 'flex',
        justifyContent: 'flex-end',
    },
    updateSubscriptionCnt: {
        padding: '18px 26px',
    },
    confimationSubscriptionCnt: {
        padding: '40px 36px',
    },
    teamImage: {
        marginBottom: 40
    },
    updateTitle: {
        // padding: '15px 0px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    confirmationTitle: {
        padding: '0px 0px',
        display: 'flex',
        justifyContent: 'space-between',
    },
    updateCommonStyle: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        // margin: '20px 0px',
    },
    updateTeammember: {
        
    },
    dividerStyle: {
        margin: '18px 0',
    },
    
    teammembers: {
        '& input[type=number]::-webkit-inner-spin-button': {
            '-webkit-appearance': 'none',
            background: `#FFF url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAPCAYAAADZCo4zAAAArUlEQVQoU82RsRHDIAxF+RVtNkla48ZMEmcDj+ARskE8QjYgDdAmm7ilsXziEKcRQsFJ+u/0JYBRJ+c8Hcexj+P4lTIkyDnPRPTiHMBjGIatxnw18UlEUwM+ABaGoEVpHWO8AagQUkpvIlq1L3dp0Npn0MPq+C+AlNJSStm897seLoRwsdbeeU1+sWspxQvUxGCM+dUtNMS5tbaKzrm5rylQs6li/wvxbpARkesnsYNeejnucREAAAAASUVORK5CYII=') no-repeat center center;`,
            width: 15,
            height: 30,
            opacity: 1, /* shows Spin Buttons per default (Chrome >= 39) */
            position: 'absolute',
            top: 0,
            right: 10,
            bottom: 0,
        },
        '& input': {
            border: `1px solid ${theme.palette.border.lightBorder}`,
            paddingLeft: 15,
            paddingRight: 26,
            borderRadius: 4,
            fontSize: "14px !important",
        },
    },
    updateCurrentSub: {
    },
    updateBillingCycle: {
    },
    updateBillingAmuont: {
    },
    updateTotalBill: {
        borderRadius: 4,
        padding: 10,
        backgroundColor: theme.palette.background.extraLightGreen,
    },
    updateNote: {
        marginTop: 18,
    },
    updateBtn: {
        marginTop: 18,
        justifyContent: 'flex-end',
    },
    ConfirmationBtn: {
        display: 'flex',
        marginTop: 50,
        flexDirection: 'column',
        alignItems: 'center',
    },
    linkStyle : {
        color: theme.palette.common.black,
        fontSize: "12px !important",
        marginTop: 20,
        textDecoration: 'underline',
        cursor: 'pointer',
    },

});
export default UpdateSubscriptionStyles;