import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter, Link } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import classes from "./style";
import Typography from "@material-ui/core/Typography";
import CustomButton from "../../../components/Buttons/CustomButton";
import Divider from "@material-ui/core/Divider";
import Input from "@material-ui/core/Input";
import clsx from "clsx";
import NotificationMessage from "../../../components/NotificationMessages/NotificationMessages";
import {
  FetchCustomerSubscription,
  UpdateCustomerSubscription,
} from "../../../redux/actions/userBillPlan";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import IconButton from "../../../components/Buttons/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import CustomTooltip from "../../../components/Tooltip/Tooltip";
import teamImages from "../../../assets/images/team.png";
import moment from "moment";
import { FormattedMessage, injectIntl } from "react-intl";

class UpdateSubscription extends Component {
  constructor(props) {
    super(props);
    this.state = {
      totalMembers: props.minimumUser || 1,
      totalDays: 0,
      remainingDays: 0,
      amount: 0,
      confirmationDlg: props.currentTeamUserRole == "Owner" ? true : false,
      updateDlg: false,
      btnQuery: "",
      loading: true,
    };
    this.minimumUser = props.minimumUser || 1;
    this.maximumUser = 300;
  }
  /**
   * it is called to get existing subscription of the user
   */
  componentDidMount() {
    const { companyId } = this.props;
    let data = { companyId: companyId, promoCode: "", users: 0 };
    this.props.FetchCustomerSubscription(
      data,
      (result) => {
        this.setState({
          remainingDays: result.data.remainingDays,
          amount: result.data.amount,
          totalDays: result.data.totalDays,
          loading: false,
        });
      },
      (fail) => {
        this.props.showSnackBar(fail.data);
      }
    );
  }
  /**
   * it is called when team members change
   * @param {event} event
   */
  teamMembersChangeHandler = (event) => {
    this.setState({ totalMembers: event.target.value });
  };
  /**
   * it is called check thant members not exceed from maximum members count on fucus out
   */
  teamMembersBlurHandler = () => {
    if (event.target.value > this.maximumUser) {
      this.setState({ totalMembers: this.maximumUser });
    } else if (event.target.value < this.minimumUser) {
      this.setState({ totalMembers: this.minimumUser });
    } else {
      this.setState({ totalMembers: event.target.value });
    }
  };
  /**
   * it is called check thant members not exceed from maximum members count if user enter self
   */
  teamMembersInputHandler = (event) => {
    if (event.target.value.length > event.target.maxLength)
      event.target.value = event.target.value.slice(0, event.target.maxLength);
  };
  /**
   * it is called when click on update subscription and user subscription update
   */
  updateSubscription = () => {
    let { totalMembers, amount, totalDays, remainingDays } = this.state;
    let users = Number(totalMembers);
    let perDayAmount = amount / totalDays;
    let discountedAmount = remainingDays * (perDayAmount / 100);

    // const {multiplier} = this.state;
    const { companyId } = this.props;
    let data = {
      companyId: companyId,
      users: users,
      amount: Number((users * discountedAmount).toFixed(2)),
    };
    this.setState({ btnQuery: "progress" }, () => {
      this.props.UpdateCustomerSubscription(
        data,
        (success) => {
          this.props.subscriptionSuccessfull();
        },
        (fail) => {
          this.setState({ btnQuery: "" });
        }
      );
    });
  };
  /**
   * it is called when confirmation done from confirmation dialog and open updatetion dialog
   */
  confirmUpdateSubscription = () => {
    this.setState({ confirmationDlg: false, updateDlg: true });
  };

  render() {
    const {
      classes,
      theme,
      subscriptionDetails,
      currentTeamUserRole,
      company,
    } = this.props;
    const {
      currentPeriondEndDate,
      packageType,
      paymentPlanTitle,
    } = subscriptionDetails;
    let expDate = moment(currentPeriondEndDate).format("MMMM DD, YYYY");
    const {
      totalMembers,
      confirmationDlg,
      updateDlg,
      btnQuery,
      remainingDays,
      amount,
      totalDays,
      loading,
    } = this.state;
    const helpText = this.props.intl.formatMessage(
      {
        id: "common.team.team-subscription.helptext",
        defaultMessage: `You will be charged only for selected team members for remaining days of your team subscription billing cycle. Your team subscription renews on ${expDate}. If you have any question, contact us at support@ntaskmanager.com`,
      },
      { expDate }
    );
    let perDayAmount = 0;
    let actualAmount = 0;
    let discountedAmount = 0;
    if (!loading) {
      perDayAmount = amount / totalDays;
      actualAmount = totalDays * (perDayAmount / 100);
      discountedAmount = remainingDays * (perDayAmount / 100);
    }
    return (
      <Fragment>
        {updateDlg && (
          <Dialog
            classes={{
              paper: classes.updateDlgPaperCnt,
              scrollBody: classes.dialogCnt,
            }}
            fullWidth={true}
            scroll="body"
            disableBackdropClick={true}
            disableEscapeKeyDown={true}
            open={true}
            onClose={() => { }}
          >
            <DialogTitle
              id="form-dialog-title"
              classes={{ root: classes.updateDefaultDialogTitle }}
            >
              <div className={classes.updateTitle}>
                <Typography variant="h2"><FormattedMessage id="team-settings.user-management.confirmation.member-limit.update-subscription-button.label" defaultMessage="Update Subscription" /></Typography>
                <IconButton
                  btnType="transparent"
                  onClick={this.props.exitUpdateSubscription}
                >
                  <CloseIcon htmlColor={theme.palette.secondary.medDark} />
                </IconButton>
              </div>
            </DialogTitle>
            <DialogContent className={classes.updateSubscriptionCnt}>
              <div>
                <div
                  className={clsx(
                    classes.updateCommonStyle,
                    classes.updateTeammember
                  )}
                >
                  <Typography variant="body2"
                    className={classes.fontSize} style={{ fontSize: "13px" }}>
                    <FormattedMessage id="team-settings.user-management.update-subscription.team-member" defaultMessage="Select No. of team members" />
                  </Typography>
                  <Input
                    type="number"
                    className={classes.teammembers}
                    disableUnderline={true}
                    onInput={this.teamMembersInputHandler}
                    inputProps={{
                      min: this.minimumUser,
                      max: this.maximumUser,
                      maxLength: 3,
                      step: 1,
                    }}
                    value={totalMembers}
                    onBlur={this.teamMembersBlurHandler}
                    onChange={this.teamMembersChangeHandler}
                    style={{ width: 110 }}
                  />
                </div>
                <Divider className={classes.dividerStyle} />
                <div
                  className={clsx(
                    classes.updateCommonStyle,
                    classes.updateCurrentSub
                  )}
                >
                  <Typography variant="body2" className= {classes.fontSize} style={{ fontSize: "13px" }}>
                    <FormattedMessage id="team-settings.billing.no-plan.current-sub-label" defaultMessage="Current Subscription" />
                  </Typography>
                  <Typography variant="h5">nTask {paymentPlanTitle}</Typography>
                </div>
                <Divider className={classes.dividerStyle} />
                <div
                  className={clsx(
                    classes.updateCommonStyle,
                    classes.updateBillingCycle
                  )}
                >
                  <Typography variant="body2" className= {classes.fontSize} style={{ fontSize: "13px" }}>
                    <FormattedMessage id="team-settings.user-management.update-subscription.billing-cycle" defaultMessage="Billing Cycle" />
                  </Typography>
                  <Typography
                    variant="h5"
                    style={{ textTransform: "capitalize" }}
                  >
                    {packageType}
                  </Typography>
                </div>
                <Divider className={classes.dividerStyle} />
                <div
                  className={clsx(
                    classes.updateCommonStyle,
                    classes.updateBillingAmuont
                  )}
                >
                  <Typography variant="body2" className= {classes.fontSize} style={{ fontSize: "13px" }}>
                    <FormattedMessage id="team-settings.user-management.update-subscription.billing-amount" defaultMessage="Billing Amount" /> (${actualAmount.toFixed(2)} x {totalMembers})
                  </Typography>
                  <Typography variant="h5">
                    ${(actualAmount * totalMembers).toFixed(2)}
                  </Typography>
                </div>
                <Divider className={classes.dividerStyle} />
                <div
                  className={clsx(
                    classes.updateCommonStyle,
                    classes.updateTotalBill
                  )}
                >
                  <div>
                    <Typography
                      variant="h5"
                      style={{
                        //color: theme.palette.text.extraLightGreen,
                        position: "relative",
                        display: "inline",
                      }}
                    >
                      <FormattedMessage id="team-settings.billing.bill-dialog.pay-label" defaultMessage="You will pay" />
                      <CustomTooltip
                        helptext={helpText}
                        iconType="help"
                      //style={{ color: theme.palette.text.extraLightGreen }}
                      />
                    </Typography>
                    <Typography
                      variant="h6"
                    //style={{ color: theme.palette.text.extraLightGreen }}
                    >
                      <FormattedMessage id="team-settings.user-management.update-subscription.remaining-days" values={{ rd: remainingDays }} defaultMessage={`For ${remainingDays} remaining days of this billing cycle.`} />
                    </Typography>
                    <Typography
                      variant="h6"
                    //style={{ color: theme.palette.text.extraLightGreen }}
                    >
                      <FormattedMessage id="team-settings.user-management.update-subscription.primary-text" defaultMessage="Your payment will be deducted from your primary card." />
                    </Typography>
                  </div>
                  <Typography
                    variant="h5"
                    style={{ color: theme.palette.text.extraLightGreen }}
                  >
                    ${(discountedAmount * totalMembers).toFixed(2)}
                  </Typography>
                </div>
                <div
                  className={clsx(
                    classes.updateCommonStyle,
                    classes.updateNote
                  )}
                >
                  <Typography variant="body2" className= {classes.fontSize} style={{ fontSize: "13px" }}>
                    <FormattedMessage id="team-settings.user-management.update-subscription.note" defaultMessage="Note. Your team subscription renews on" />{" "}
                    <span
                      style={{
                        fontSize: "15px",
                        color: theme.palette.common.black,
                      }}
                    >
                      {expDate}
                    </span>
                  </Typography>
                  <Typography variant="h5"></Typography>
                </div>
                <div
                  className={clsx(classes.updateCommonStyle, classes.updateBtn)}
                >
                  {company.promotion != 'Lifetime Deal 2020' ?
                    <CustomButton
                      btnType="blue"
                      variant="contained"
                      style={{ marginBottom: 0, minWidth: 120 }}
                      onClick={this.updateSubscription}
                      query={btnQuery}
                      disabled={btnQuery === "progress"}
                    >
                      <FormattedMessage id="team-settings.user-management.confirmation.member-limit.update-subscription-button.label" defaultMessage="Update Subscription" />
                    </CustomButton>
                    : null}
                </div>
              </div>
            </DialogContent>
          </Dialog>
        )}

        {confirmationDlg && (
          <Dialog
            classes={{
              paper: classes.confirmeDlgPaperCnt,
              scrollBody: classes.dialogCnt,
            }}
            fullWidth={true}
            scroll="body"
            disableBackdropClick={true}
            disableEscapeKeyDown={true}
            open={true}
            onClose={() => { }}
          >
            <DialogTitle
              id="form-dialog-title"
              classes={{ root: classes.confirmDefaultDialogTitle }}
            >
              {/* <div className={classes.confirmationTitle}>
                            <Typography  variant="h1">
                               
                            </Typography>
                        </div> */}
              <IconButton
                btnType="transparent"
                style={{ padding: 0 }}
                onClick={this.props.exitUpdateSubscription}
              >
                <CloseIcon htmlColor={theme.palette.secondary.medDark} />
              </IconButton>
            </DialogTitle>
            <DialogContent className={classes.confimationSubscriptionCnt}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                }}
              >
                <img
                  src={teamImages}
                  alt="teamImage"
                  className={classes.teamImage}
                />
                <Typography variant="h2" style={{ marginBottom: 20 }}>
                  <FormattedMessage id="team-settings.user-management.confirmation.member-limit.title" defaultMessage="Team Members Limit Consumed" />
                </Typography>
                {company.promotion != 'Lifetime Deal 2020' ?
                  <Typography variant="body2" style={{ textAlign: " center" }}>
                    <FormattedMessage id="team-settings.user-management.confirmation.member-limit.label" defaultMessage="Your team has reached to its maximum member(s) limit. To
                  continue inviting more members to your team, please update
                  your team member subscription."/>
                  </Typography>
                  :
                  <Typography variant="body2" style={{ textAlign: " center" }}>
                    Maximum team limit reached. To add more members, please conact nTask Support</Typography>
                }

              </div>
              <div className={classes.ConfirmationBtn}>
                {company.promotion != 'Lifetime Deal 2020' ?
                  <CustomButton
                    btnType="blue"
                    variant="contained"
                    style={{ marginBottom: 0, width: 160 }}
                    onClick={this.confirmUpdateSubscription}
                  >
                    <FormattedMessage id="team-settings.user-management.confirmation.member-limit.update-subscription-button.label" defaultMessage="Update Subscription" />
                  </CustomButton> : null}
                <a
                  className={classes.linkStyle}
                  onClick={this.props.exitUpdateSubscription}
                >
                  <FormattedMessage id="team-settings.user-management.confirmation.member-limit.do-it-button.label" defaultMessage="I'll do it later" />
                </a>
              </div>
            </DialogContent>
          </Dialog>
        )}

        {currentTeamUserRole == "Admin" && (
          <Dialog
            classes={{
              paper: classes.confirmeDlgPaperCnt,
              scrollBody: classes.dialogCnt,
            }}
            fullWidth={true}
            scroll="body"
            disableBackdropClick={true}
            disableEscapeKeyDown={true}
            open={true}
            onClose={() => { }}
          >
            <DialogTitle
              id="form-dialog-title"
              classes={{ root: classes.confirmDefaultDialogTitle }}
            >
              <IconButton
                btnType="transparent"
                style={{ padding: 0 }}
                onClick={this.props.exitUpdateSubscription}
              >
                <CloseIcon htmlColor={theme.palette.secondary.medDark} />
              </IconButton>
            </DialogTitle>
            <DialogContent className={classes.confimationSubscriptionCnt}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                }}
              >
                <img
                  src={teamImages}
                  alt="teamImage"
                  className={classes.teamImage}
                />
                <Typography variant="h2" style={{ marginBottom: 20 }}>
                  Team Members Limit Consumed
                </Typography>
                <Typography variant="body2" style={{ textAlign: " center" }}>
                  Your team has reached to its maximum member(s) limit. To
                  continue inviting more members to your team, please contact
                  your team owner for subscription update.
                </Typography>
              </div>
              <div className={classes.ConfirmationBtn}>
                <CustomButton
                  btnType="blue"
                  variant="contained"
                  style={{ marginBottom: 0, width: 160 }}
                  onClick={this.props.exitUpdateSubscription}
                >
                  Continue
                </CustomButton>
              </div>
            </DialogContent>
          </Dialog>
        )}
      </Fragment>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    companyId: state.profile.data.activeTeam,
    subscriptionDetails: state.profile.data.teams.find(
      (item) => item.companyId == state.profile.data.activeTeam
    ).subscriptionDetails,
    company: state.profile.data.teams.find(
      item => item.companyId == state.profile.data.activeTeam,
    ),
  };
};
export default compose(
  withRouter,
  injectIntl,
  connect(mapStateToProps, {
    FetchCustomerSubscription,
    UpdateCustomerSubscription,
  }),
  withStyles(classes, { withTheme: true })
)(UpdateSubscription);
