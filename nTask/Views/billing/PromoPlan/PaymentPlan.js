import React, { Component, Fragment } from "react";
import { withSnackbar } from "notistack";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import classes from "./style";
import Typography from "@material-ui/core/Typography";
import CustomButton from "../../../components/Buttons/CustomButton";
import { StripeProvider, Elements } from 'react-stripe-elements';
import AddBillDetail from '../AddBillDetail/AddBillDetail';
import Constants from "../../../components/constants/planConstant";
import PlanPackages from "../PlanPackages/PlanPackages";
import { switchTeam } from "../../../redux/actions/teamMembers";
import { FetchUserInfo } from "../../../redux/actions/profile";
import promoImage from "../../../assets/images/promo_image.svg";
import nTaskLogo from "../../../assets/images/nTask-Logo.svg";
import IconButton from "../../../components/Buttons/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import DefaultTextField from "../../../components/Form/TextField";
import { UpgradePremiumPlanState, UpdatePromoCodeValue } from "../../../redux/actions/userBillPlan";


class PaymentPlan extends Component {

    constructor(props) {
        super();
        this.state = {
            openBillDetail: false,
            selectedTeam: [],
            packageDetails: null,
            openPackages: true,
            openDialog: false,
            btnSwitchQuery: '',
        };
    }
    // closePopUp = () => {
    //     if(this.props.closePromoPopUp)
    //       this.props.closePromoPopUp();
    // }
    /**
     * it is called when team change
     * @param {string} key
     * @param {string} value
     */
    handleTeamChange = (key, value) => {
        this.setState({ [key]: value });
    }
    /**
     * it is called to open a bill dialog
     */
    upgradePlan = () => {
        this.setState({ openBillDetail: true });
    }
    /**
     * it is called when payment is done and upgrade store state
     * @param {object} planData
     */
    paymentPaidHandler = (planData) => {
        this.removePromocode();
        this.props.UpgradePremiumPlanState(planData);
        if (this.props.closeTrialPopUp)
            this.props.closeTrialPopUp();
    }
    /**
     * it is called when exit from add bill detail dialog and close trial popup
     */
    exitBillDetail = () => {
        this.removePromocode();
        this.setState({ openBillDetail: false });
        if (this.props.closeTrialPopUp)
            this.props.closeTrialPopUp();
    }
    /**
     * it is called to open package plans dialog
     */
    showPackages = () => {
        this.setState({ openPackages: true });
    }
    /**
     * it is called when user select package and move to add bill dialog for payment
     * @param {object} packageInfo
     */
    selectPackage = (packageInfo) => {
        this.setState({ openPackages: false, packageDetails: packageInfo, openBillDetail: true });
    }
    /**
     * it is called to exit from package plan dialog without selecting plan
     */
    exitPackageDialog = () => {
        this.removePromocode();
        this.setState({ openPackages: false });
        if (this.props.closeTrialPopUp)
            this.props.closeTrialPopUp();
    }
    /**
     * it is called to remove promocode form state
     */
    removePromocode = () => {
        this.props.UpdatePromoCodeValue(null, response => {

        },
            fail => {

            });
    }
    // copyPromo = () => {
    //     this.inputEl.select();
    //     document.execCommand("copy");
    // }
    componentDidMount() {
        if (this.props.showBilling) {
            this.selectPackage({
                subscription: 'annually',
                planType: Constants.BUSINESSPLAN,
                trailPaid: Constants.TRIALPLANMODE,
            })
        }
    }
    render() {
        const { classes, teams, isOwner, minimumUser, theme } = this.props;
        const { paymentPlanTitle, packageType } = this.props.subscriptionDetails;
        let plan = Constants.DisplayPlanName(paymentPlanTitle);
        const { openBillDetail, openPackages, packageDetails, selectedTeam, openDialog, btnSwitchQuery } = this.state;
        let planInfo = '';
        if (openBillDetail) {
            planInfo = { ...packageDetails }
            planInfo.pricePlanType = (packageDetails.planType == Constants.PREMIUMPLAN) ? Constants.PREMIUMPLANPRICE : Constants.BUSINESSPLANPRICE;
        }

        return (
            <Fragment>
                {openBillDetail && <StripeProvider apiKey={STRIPE_PUBLISH_KEY}>
                    <Elements>
                        <AddBillDetail
                            showSnackBar={this.props.showSnackBar}
                            paymentPaidHandler={(planData) => this.paymentPaidHandler(planData)}
                            exitBillDetail={this.exitBillDetail}
                            minimumUser={minimumUser}
                            selectedPackageDetails={planInfo}
                        >
                        </AddBillDetail>
                    </Elements>
                </StripeProvider>}
                {openPackages && <PlanPackages
                    selectPackage={this.selectPackage}
                    showSnackBar={this.props.showSnackBar}
                    exitPackageDialog={this.exitPackageDialog}
                    mode={Constants.UPGRADEPLANMODE} ></PlanPackages>
                }
            </Fragment>
        )
    }
}
const mapStateToProps = state => {
    return {
        subscriptionDetails: state.profile.data.teams.find(item => item.companyId == state.profile.data.activeTeam).subscriptionDetails,
        isOwner: state.profile.data.teamMember.find(item => item.userId == state.profile.data.userId).isOwner,
        teams: state.profile.data.teams.filter(item => item.companyId != state.profile.data.activeTeam),
        minimumUser: state.profile.data.teamMember.filter(item => (item.isDeleted == false) && (item.isActive == null || item.isActive == true)).length,
        companyId: state.profile.data.activeTeam,
    };
};
export default compose(
    withRouter,
    withSnackbar,
    withStyles(classes, { withTheme: true }),
    connect(
        mapStateToProps,
        {
            FetchUserInfo,
            UpgradePremiumPlanState,
            UpdatePromoCodeValue
        }
    )
)(PaymentPlan);