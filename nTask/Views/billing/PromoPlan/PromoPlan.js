import React, { Component, Fragment } from "react";
import { withSnackbar } from "notistack";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import classes from "./style";
import Typography from "@material-ui/core/Typography";
import CustomButton from "../../../components/Buttons/CustomButton";
import { StripeProvider, Elements } from "react-stripe-elements";
import AddBillDetail from "../AddBillDetail/AddBillDetail";
import Constants from "../../../components/constants/planConstant";
import PlanPackages from "../PlanPackages/PlanPackages";
import { switchTeam } from "../../../redux/actions/teamMembers";
import { FetchUserInfo } from "../../../redux/actions/profile";
import promoImage from "../../../assets/images/promo_image.svg";
import nTaskLogo from "../../../assets/images/nTask-White-logo.svg";
import IconButton from "../../../components/Buttons/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import DefaultTextField from "../../../components/Form/TextField";
import { UpdatePromoCodeValue } from "../../../redux/actions/userBillPlan";
import TextImage from "../../../assets/images/HCNY2020.svg";

class PromoPlan extends Component {
  constructor(props) {
    super();
    this.state = {
      openBillDetail: false,
      selectedTeam: [],
      packageDetails: null,
      openPackages: false,
      openDialog: false,
      btnSwitchQuery: "",
    };
  }
  /**
   * it is called to exit from current dialog
   */
  closePopUp = () => {
    if (this.props.closePromoPopUp) this.props.closePromoPopUp();
  };
  /**
   * it is called when user change the team
   * @param {string} key
   * @param {string} value
   */
  handleTeamChange = (key, value) => {
    this.setState({ [key]: value });
  };
  /**
   * it is called when user want to open add bill dialog
   */
  upgradePlan = () => {
    this.setState({ openBillDetail: true });
  };
  /**
   * it is called when payment done
   * @param {object} planData
   */
  paymentPaidHandler = planData => {
    if (this.props.closePromoPopUp) this.props.closePromoPopUp();
  };
  /**
   * it is called when user doesn't pay and exit from add bill dialog
   */
  exitBillDetail = () => {
    this.setState({ openBillDetail: false });
    if (this.props.closePromoPopUp) this.props.closePromoPopUp();
  };
  /**
   * it is called to open package plan dialog
   */
  showPackages = code => {
    this.props.UpdatePromoCodeValue(code, response => {
      // this.props.openPromoPopUp();
      this.setState({ openPackages: true });
    });
  };
  /**
   * it is called when user select any package plan
   */
  selectPackage = packageInfo => {
    this.setState({
      openPackages: false,
      packageDetails: packageInfo,
      openBillDetail: true,
    });
  };
  /**
   * it is called when user doesn't select any package and exit from package plan dialog
   */
  exitPackageDialog = () => {
    this.setState({ openPackages: false });
    if (this.props.closePromoPopUp) this.props.closePromoPopUp();
  };
  /**
   * it is called to copy promocode to clipboard
   */
  copyPromo = () => {
    this.inputEl.select();
    document.execCommand("copy");
  };

  render() {
    const { classes, teams, isOwner, minimumUser, theme } = this.props;
    const { paymentPlanTitle, packageType } = this.props.subscriptionDetails;
    let plan = Constants.DisplayPlanName(paymentPlanTitle);
    const {
      openBillDetail,
      openPackages,
      packageDetails,
      selectedTeam,
      openDialog,
      btnSwitchQuery,
    } = this.state;
    let planInfo = "";
    if (openBillDetail) {
      planInfo = { ...packageDetails };
      planInfo.pricePlanType =
        packageDetails.planType == Constants.PREMIUMPLAN
          ? Constants.PREMIUMPLANPRICE
          : Constants.BUSINESSPLANPRICE;
    }

    return (
      <Fragment>
        {openBillDetail == false && (
          <Dialog
            classes={{
              paper: classes.trialExpirePaperCnt,
              scrollBody: classes.dialogCnt,
            }}
            fullWidth={true}
            scroll="body"
            disableBackdropClick={true}
            disableEscapeKeyDown={true}
            open={true}
            onClose={this.closePopUp}>
            <DialogContent className={classes.promoPlanContent}>
              <div
                className={classes.updateTitle}
                style={{ display: "flex", justifyContent: "flex-end" }}>
                <IconButton
                  btnType="transparent"
                  onClick={this.closePopUp}
                  style={{ background: "rgba(255, 255, 255, 0.4)" }}>
                  <CloseIcon
                    htmlColor={theme.palette.common.white}
                    className={classes.closeIcon}
                  />
                </IconButton>
              </div>
              <div className={classes.promoPlanBody}>
                <div className={classes.iconContainer}>
                  <img src={TextImage} alt="Pro Plan Image" className={classes.promoFeatureImg} />
                </div>
                <Typography variant="h2" className={classes.promoPlanTitle}>
                  Enjoy nTask Paid Plans at an unbelievable{" "}
                  <span style={{ fontWeight: 700, fontSize: "24px" }}>10%</span> discount!
                </Typography>
                <Typography variant="body2" className={classes.promoPlanCnt}>
                  New Year Promo Code
                </Typography>

                <div className={classes.promocodeValueCnt}>
                  <Typography variant="h6" className={classes.promoCodeTitle}>
                    Copy the code to use this discount at checkout.
                  </Typography>
                  <div style={{ display: "flex", marginTop: 30 }}>
                    <DefaultTextField
                      label={false}
                      fullWidth={true}
                      error={false}
                      errorState={false}
                      formControlStyles={{
                        marginBottom: 0,
                        width: 220,
                        borderRadius: "0",
                      }}
                      defaultProps={{
                        id: "promocode",
                        type: "text",
                        style: { borderRadius: "0", border: "none" },
                        onChange: e => {},
                        readOnly: true,
                        value: "COVID",
                        inputProps: {
                          maxLength: 80,
                          style: {
                            borderRadius: "0",
                            fontSize: "26px",
                            border: "none",
                            padding: "7px 14px",
                            fontFamily: theme.typography.fontFamilyLato,
                          },
                        },
                        inputRef: node => {
                          this.inputEl = node;
                        },
                      }}
                    />
                    <CustomButton
                      btnType="white"
                      variant="contained"
                      style={{
                        marginBottom: 20,
                        padding: "13.5px 0px 13.5px",
                        borderRadius: "0",
                        fontSize: "14px",
                        color: "white",
                        border: "none",
                        background: "transparent linear-gradient(180deg, #FFDC65 0%, #D68326 100%)",
                        marginLeft: 0,
                      }}
                      onClick={this.copyPromo}>
                      Copy
                    </CustomButton>
                  </div>
                </div>
                <div className={classes.promoPlanBtns}>
                  <CustomButton
                    btnType="white"
                    variant="contained"
                    style={{
                      color: "white",
                      border: "none",
                      background: "transparent linear-gradient(180deg, #FFDC65 0%, #D68326 100%)",
                      padding: "15px 20px",
                      width: 190,
                    }}
                    onClick={() => this.showPackages("COVID")}>
                    Upgrade Now
                  </CustomButton>
                </div>
              </div>
            </DialogContent>
          </Dialog>
        )}
        {openBillDetail && (
          <StripeProvider apiKey={STRIPE_PUBLISH_KEY}>
            <Elements>
              <AddBillDetail
                showSnackBar={this.props.showSnackBar}
                paymentPaidHandler={planData => this.paymentPaidHandler(planData)}
                exitBillDetail={this.exitBillDetail}
                minimumUser={minimumUser}
                selectedPackageDetails={planInfo}></AddBillDetail>
            </Elements>
          </StripeProvider>
        )}
        {openPackages && (
          <PlanPackages
            selectPackage={this.selectPackage}
            showSnackBar={this.props.showSnackBar}
            exitPackageDialog={this.exitPackageDialog}
            mode={Constants.UPGRADEPLANMODE}></PlanPackages>
        )}
      </Fragment>
    );
  }
}
const mapStateToProps = state => {
  return {
    subscriptionDetails: state.profile.data.teams.find(
      item => item.companyId == state.profile.data.activeTeam
    ).subscriptionDetails,
    isOwner: state.profile.data.teamMember.find(item => item.userId == state.profile.data.userId)
      .isOwner,
    teams: state.profile.data.teams.filter(item => item.companyId != state.profile.data.activeTeam),
    minimumUser: state.profile.data.teamMember.filter(
      item => item.isDeleted == false && (item.isActive == null || item.isActive == true)
    ).length,
    companyId: state.profile.data.activeTeam,
  };
};

export default compose(
  withRouter,
  withSnackbar,
  withStyles(classes, { withTheme: true }),
  connect(mapStateToProps, {
    FetchUserInfo,
    UpdatePromoCodeValue,
  })
)(PromoPlan);
