import backgroundPattern from "../../../assets/images/bgPattern.png";
import baloon from "../../../assets/images/balloonsSmallRight.svg";

const PromoPlanStyles = theme => ({
  trialExpireDetailsDialog: {

  },
  trialExpirePaperCnt: {
    overflowY: "visible",
    background: 'radial-gradient(ellipse at center, #de1515 1%,#980000 100%)',
    width: "550px !important",
    // backgroundImage: `url(${newYearLeftBg}), url(${newYearRightBg})`,
    backgroundRepeat: "no-repeat"
  },
  promoPlanContent: {
    padding: '20px 20px 60px 20px !important',
    borderRadius: 4,
    backgroundImage: `url(${backgroundPattern}), url(${baloon}), url(${baloon})`,
    backgroundPosition: "0 0, 15px 0px, calc(100% - 15px) 0",
    backgroundSize: 'cover, auto, auto',
    backgroundRepeat: "no-repeat"
  },
  promoPlanBody: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    textAlign: 'center',
  },
  promoPlanTitle: {
    fontSize: "24px !important",
    fontWeight: theme.typography.fontWeightRegular,
    fontFamily: theme.typography.fontFamilyLato,
    width: 432,
    marginTop: 25,
    lineHeight: "27px",
    color: theme.palette.common.white
  },
  promoPlanCnt: {
    fontSize: "23px !important",
    marginTop: 13,
    fontFamily: theme.typography.fontFamilyLato,
    color: theme.palette.common.white
  },
  promocodeValueCnt: {
    // display: 'flex', 
    display: "flex",
    alignItems: "center",
    flexDirection: "column"
    // alignItems: 'flex-end',
  },
  promoCodeTitle: {
    color: theme.palette.common.white,
    fontSize: "18px !important",
    fontFamily: theme.typography.fontFamilyLato,
  },
  closeIcon: {
    fontSize: "15px !important"
  },
  promoFeatureImg: {
    width: 412,
    marginTop: 20
  },
  //   lblStyle: {
  //     display: 'flex',
  //     fontSize: "12px !important",
  // },
  promoPlanBtns: {
    marginTop: 10
  },
});
export default PromoPlanStyles;