import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import classes from "./style";
import Typography from "@material-ui/core/Typography";
import CustomAvatar from "../../../components/Avatar/Avatar";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import SelectSearchDropdown from "../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { generateCompaniesData } from "../../../helper/generateSelectData";
import CustomButton from "../../../components/Buttons/CustomButton";
import DefaultDialog from "../../../components/Dialog/Dialog";
import CreateTeamForm from "../../AddNewForms/addNewTeamFrom";
import { switchTeam, whiteLabelInfo, DefaultWhiteLabelInfo } from "../../../redux/actions/teamMembers";
import { FetchUserInfo } from "../../../redux/actions/profile";
import ConstantsPlan from "../../../components/constants/planConstant";
import moment from "moment";

function RestrictedAccess(props) {
    const [selectedTeam, setSelectedTeam] = React.useState({});
    const [openDialog, setOpenDialog] = React.useState(false);
    const [btnSwitchQuery, setBtnSwitchQuery] = React.useState('');
    const trialText = `You have been temporarily locked out of this team because the free trial expired.
                        Please ask the team owner to either continue with our Free forever plan or upgrade to
                        the paid plan to enjoy all our nTask features.`;
    const planText = `You have been temporarily locked out of this team due to payment that is overdue.
                        Don't worry - we will restore your access once the payment has been made.`;

    /**
     * it is called when user want to exit from this dialog
     */
    const exitDialog = () => {
        if (props.closeRestrictedDialog)
            props.closeRestrictedDialog();
    }
    /**
     * it is called when user change the team from selection contorl
     * @param {string} key 
     * @param {string} value 
     */
    const handleTeamsChange = (key, value) => {
        setSelectedTeam(value)
        // setWworkspace(value);
    }
    /**
     * it is called when user select team and called api for switching
     * @param {object} type 
     * @param {string} item 
     */
    const switchToOtherTeam = (type, item) => {

        if (!selectedTeam.id) return;
        // props.switchExpirePlan();
        props.closeRestrictedDialog();
        props.showLoadingState();
        setBtnSwitchQuery('progress');
        switchTeam(selectedTeam.id, response => {
            if (selectedTeam.isEnableWhiteLabeling && selectedTeam.isEnableWhiteLabeling == true)
                props.whiteLabelInfo(selectedTeam.id);
            else
                props.DefaultWhiteLabelInfo();
            props.FetchUserInfo((userApiResponse) => {
                // setBtnSwitchQuery('');
                props.history.push(`/teams/${response.data.team.companyUrl}`);

                const { gracePeriod } = props;
                const { paymentPlanTitle, currentPeriondEndDate } = response.data.team.subscriptionDetails;
                if (!ConstantsPlan.isPlanFree(paymentPlanTitle)) {
                    if (ConstantsPlan.isPlanTrial(paymentPlanTitle) && ConstantsPlan.isPlanExpire(currentPeriondEndDate)) {
                        setBtnSwitchQuery('');
                        props.showLoadingState();
                        props.planExpire('Trial');
                        return;
                    } else if (ConstantsPlan.isPlanPaid(paymentPlanTitle) && ConstantsPlan.isPlanExpire(currentPeriondEndDate, gracePeriod)) {
                        setBtnSwitchQuery('');
                        props.showLoadingState();
                        props.planExpire('Paid');
                        return;
                    }
                }
            });
            props.hideLoadingState();
        });
    }
    /**
     * it is called to open create new team dialog
     */
    const openCreateTeam = () => {
        setOpenDialog(true);
    }
    /**
     * it is called to close create new team dialog
     */
    const handleDialogClose = () => {
        setOpenDialog(false);
    }
    /**
     * it is called when user create new team and move further
     */
    const createTeam = () => {
        props.createTeam();
    }

    const { classes, teams, mode } = props;
    const showText = mode == 'Trial' ? trialText : planText;

    return (
        <Fragment>
            <DefaultDialog
                title="Create Team"
                sucessBtnText="Create Team"
                open={openDialog}
                onClose={handleDialogClose}
            >
                <CreateTeamForm
                    handleDialogClose={handleDialogClose}
                    createTeamHandler={createTeam}
                />
            </DefaultDialog>
            <Dialog
                classes={{
                    paper: classes.dialogPaperCnt,
                    scrollBody: classes.dialogCnt
                }}
                fullWidth={true}
                scroll="body"
                disableBackdropClick={true}
                disableEscapeKeyDown={true}
                open={true}
                onClose={exitDialog}
            >
                <DialogContent className={classes.defaultDialogContent}>
                    <div className={classes.restrictedAccessBody} >
                        <CustomAvatar
                            userActiveTeam={true}
                            size="large"
                        />
                        <Typography variant="h2" className={classes.bodyTitle}>
                            Restricted Access
                        </Typography>
                        <Typography variant="h6" className={classes.bodyCnt}>
                            {showText}
                        </Typography>
                        <div className={classes.restrictedAccessBodyBottom}>
                            {teams.length ?
                                <Fragment>
                                    <Typography variant="h6" className={classes.bodyCnt}>
                                        Meanwhile you can continue your work by switching to one of the appended options.
                                    </Typography>
                                    <div className={classes.restrictedAccessBodyBottomBtn}>
                                        <div className={classes.selectionDiv}>
                                            <SelectSearchDropdown
                                                data={() => generateCompaniesData(teams)}
                                                label=""
                                                isMulti={false}
                                                selectChange={handleTeamsChange}
                                                type="selectedTeam"
                                                selectedValue={selectedTeam}
                                                placeholder="Switch to:"
                                                styles={{ marginTop: 0, marginBottom: 0 }}
                                            />
                                        </div>
                                        <CustomButton
                                            btnType="success"
                                            variant="contained"
                                            onClick={switchToOtherTeam}
                                            query={btnSwitchQuery}
                                            disabled={btnSwitchQuery === "progress"}
                                        >
                                            Switch
                                        </CustomButton>
                                    </div>
                                </Fragment>
                                : <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }} >
                                    <Typography variant="h6" className={classes.bodyCnt}>
                                        Meanwhile you can continue working by creating a new team.
                                    </Typography>
                                    <CustomButton
                                        btnType="success"
                                        variant="contained"
                                        onClick={openCreateTeam}
                                        style={{ marginTop: 20 }}
                                    >
                                        Create Your Team
                                    </CustomButton>
                                </div>}
                        </div>
                    </div>
                </DialogContent>
            </Dialog>
        </Fragment>

    )
}
const mapStateToProps = state => {
    return {
        teams: state.profile.data.teams.filter(item => item.companyId != state.profile.data.activeTeam),
        gracePeriod: state.profile.data.gracePeriod,
    };
};
export default compose(
    withRouter,
    connect(
        mapStateToProps, {
        FetchUserInfo,
        whiteLabelInfo,
        DefaultWhiteLabelInfo,
    }
    ),
    withStyles(classes, { withTheme: true })
)(RestrictedAccess);