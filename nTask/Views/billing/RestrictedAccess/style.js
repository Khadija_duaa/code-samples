import planBgImg from "../../../assets/images/bg_pattern.png"
const RestrictedAccessStyles = theme => ({ 

  restrictedDetailsDialog: {
    
  },
  dialogPaperCnt: {
    overflowY: "visible",
    background: theme.palette.common.white,
    width: "600px!important"
  },
  defaultDialogContent: {
    padding: '40px 28px 30px 28px!important',
    background: `url(${planBgImg})`,
    backgroundRepeat: "no-repeat",
  },
  restrictedAccessBody: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  bodyTitle: {
    marginTop: 26
  },
  bodyCnt: {
    marginTop: 20, 
    textAlign: 'center'
  },
  // restrictedAccessFooter: {
  //   padding: '40px 100px 0px 100px',
  //   width: '100%',
  //   display: 'flex',
  //   justifyContent: 'space-around',
  // },
  selectionDiv: {
    width: '60%',
    textAlign: 'left'
  },
  restrictedAccessBodyBottom: {
    // marginTop: 30,
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
},
restrictedAccessBodyBottomBtn: {
    paddingTop: 30,
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-evenly',
},

});
export default RestrictedAccessStyles;