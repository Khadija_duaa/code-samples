import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import classes from "./style";
import Typography from "@material-ui/core/Typography";
import CustomButton from "../../../components/Buttons/CustomButton";
import upgradePro from "../../../assets/images/upgrade_pro.png";
import CheckIcon from "@material-ui/icons/Check";
import MeetingsIcon from "../../../components/Icons/MeetingIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import billingIcon from "../../../assets/images/billing_icon.png";
import moment from "moment";
import AddBillDetail from "../AddBillDetail/AddBillDetail";
import { StripeProvider, Elements } from "react-stripe-elements";
import PlanPackages from "../PlanPackages/PlanPackages";
import Constants from "../../../components/constants/planConstant";
import NoBilling from "../NoBilling/NoBilling";
import { FormattedMessage } from "react-intl";

class TrialPlan extends Component {
  constructor() {
    super();
    this.state = {
      openBillDetail: false,
      openPackages: false,
      packageDetails: null,
      isTrialUser: true,
    };
  }
  /**
   * it is called to open add bill details dialog
   */
  showPaymentHandler = () => {
    this.setState({ openBillDetail: true });
  };
  /**
   * it is called to opne package plan
   */
  showPackages = () => {
    this.setState({ openPackages: true });
  };
  /**
   * it is called when user select package plan and move further
   * @param {object} packageInfo
   */
  selectPackage = packageInfo => {
    this.setState({ openPackages: false, packageDetails: packageInfo, openBillDetail: true });
  };
  /**
   * it is called when user pay his dues and moveon
   * @param {object} planData
   */
  paymentPaidHandler = planData => {
    if (this.props.paymentPaidDone) this.props.paymentPaidDone(planData);
  };
  /**
   * it is called when user exit from add bill detail dialog without pay
   */
  exitBillDetail = () => {
    this.setState({ openBillDetail: false });
  };
  /**
   * it is called when user exit from package plan without select any package plan
   */
  exitPackageDialog = () => {
    this.setState({ openPackages: false });
  };
  render() {
    const { classes, subscriptionDetails, theme, minimumUser } = this.props;
    const { currentPeriondEndDate, paymentPlanTitle } = subscriptionDetails;
    const { openBillDetail, openPackages, packageDetails } = this.state;
    let expiryFormattedDate = moment(currentPeriondEndDate).format("MMMM DD, YYYY");
    let expMomentDate = moment(new Date(currentPeriondEndDate), "MMMM DD, YYYY");
    let nowMomentDate = moment(new Date(), "MMMM DD, YYYY");
    let diff = expMomentDate.diff(nowMomentDate, "days");
    let trialPlanDueIconClass = [classes.trialPlanDueIcon];
    let trialPlanDueDateClass = [classes.trialPlanDueDate];
    if (diff <= 5) {
      trialPlanDueIconClass.push(classes.expiryClass);
      trialPlanDueDateClass.push(classes.expiryClass);
    }
    let planInfo = "";
    if (openBillDetail) {
      planInfo = { ...packageDetails };
      planInfo.pricePlanType =
        packageDetails.planType == Constants.PREMIUMPLAN
          ? Constants.PREMIUMPLANPRICE
          : Constants.BUSINESSPLANPRICE;
    }
    let panelTitle = Constants.DisplayPlanName(paymentPlanTitle);
    return (
      <Fragment>
        <div className={classes.mainPlan}>
          <div
            className={classes.planDetailsCnt}
            fxLayout="row"
            fxLayoutAlign="space-around center">
            <div>
              <div className={classes.planTitle}>
                <Typography variant="h2" className={classes.trialPlanTxt}>
                  {`nTask ${panelTitle}`}
                </Typography>
              </div>
              <div className={classes.trialPlanLimitationDiv}>
                <SvgIcon viewBox="0 0 17.031 17" className={trialPlanDueIconClass}>
                  <MeetingsIcon />
                </SvgIcon>
                <Typography variant="h5" className={trialPlanDueDateClass}>
                  <FormattedMessage
                    id="team-settings.billing.expire.label"
                    values={{ t1: panelTitle, t2: expiryFormattedDate }}
                    defaultMessage={`Your ${panelTitle} will expire on ${expiryFormattedDate}`}
                  />
                </Typography>
              </div>
            </div>
            <div>
              <CustomButton
                btnType="blue"
                variant="contained"
                style={{ marginBottom: 0 }}
                onClick={this.showPackages}>
                <FormattedMessage
                  id="team-settings.billing.upgrade-button.label"
                  defaultMessage="Upgrade"
                />
              </CustomButton>
            </div>
          </div>
          <NoBilling></NoBilling>
        </div>
        {openBillDetail && (
          <StripeProvider apiKey={STRIPE_PUBLISH_KEY}>
            <Elements>
              <AddBillDetail
                showSnackBar={this.props.showSnackBar}
                paymentPaidHandler={planData => this.paymentPaidHandler(planData)}
                exitBillDetail={this.exitBillDetail}
                minimumUser={minimumUser}
                selectedPackageDetails={planInfo}></AddBillDetail>
            </Elements>
          </StripeProvider>
        )}
        {openPackages && (
          <PlanPackages
            showSnackBar={this.props.showSnackBar}
            selectPackage={this.selectPackage}
            exitPackageDialog={this.exitPackageDialog}
            mode={Constants.UPGRADEPLANMODE}></PlanPackages>
        )}
      </Fragment>
    );
  }
}
const mapStateToProps = state => {
  return {
    subscriptionDetails: state.profile.data.teams.find(
      item => item.companyId == state.profile.data.activeTeam
    ).subscriptionDetails,
    minimumUser: state.profile.data.teamMember.filter(
      item => item.isDeleted == false && (item.isActive == null || item.isActive == true)
    ).length,
  };
};
export default compose(
  withRouter,
  connect(mapStateToProps),
  withStyles(classes, { withTheme: true })
)(TrialPlan);
