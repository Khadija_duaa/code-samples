

const TrialPlanStyles = theme => ({

    planDetailsCnt:{
        padding: '25px 18px 22px 18px',
        borderRadius: 4,
        background: theme.palette.common.white,
        border: `1px solid ${theme.palette.border.lightBorder}`,
        marginBottom: 10,
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    planTitle: {
        display: 'flex',
    },
    trialPlanLimitationDiv: {
        display: 'flex', 
        alignItems: 'center', 
        paddingTop: 9,
        
    },
    trialPlanDueIcon:{
        fontSize: "14px !important",
        color: theme.palette.secondary.light,
        marginRight: 8
    },
    trialPlanDueDate: {
        fontSize: "11px !important",
    },
    expiryClass: {
        color: theme.palette.text.danger,
    },    
});
export default TrialPlanStyles;