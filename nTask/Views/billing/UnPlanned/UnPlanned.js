import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import classes from "./style";
import Typography from "@material-ui/core/Typography";
import CustomButton from "../../../components/Buttons/CustomButton";
import UpgradePlan from "../UpgradePlan/UpgradePlan";
import Basic from "../../../assets/images/plans/Basic.svg";
import Premium from "../../../assets/images/unplanned/PremiumIcon.svg";
import Business from "../../../assets/images/unplanned/BusinessIcon.svg";
import PlanConstant from "../../../components/constants/planConstant";
import Gantt from "../../../assets/images/unplanned/Gantt.png";
import {
  showUpgradePlan,
  StartTrialPlanBilling,
  UpgradeTrailInStore,
} from "../../../redux/actions/userBillPlan";
// import NoPlan from "../NoPlan/NoPlan";
import PlanPackages from "../PlanPackages/PlanPackages";
import Constants from "../../../components/constants/planConstant";
import { withSnackbar } from "notistack";
import AddBillSuccess from "../AddBillDetail/AddBillSuccess";
import moment from "moment";
import { FormattedMessage } from "react-intl";
import {TRIALPERIOD} from '../../../components/constants/planConstant';

class UnPlanned extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openPackages: false,
      packageMode: "",
      isTrialSuccess: false,
      response: null,
      mode: "",
    };
  }
  startTrialPlanHandler = selectedPackage => {
    const { teamId, profile } = this.props;
    let mode = selectedPackage.planType;
    let companyInfo = {
      mode: mode,
      companyId: this.props.profile.activeTeam,
    };
    this.props.StartTrialPlanBilling(
      companyInfo,
      success => {
        this.setState({ isTrialSuccess: true, mode: mode, response: success,openPackages: false });
        if (ENV == "production" && window.userpilot) {
          window.userpilot.identify(
            profile.userId, // Used to identify users
            {
              name: profile.fullName, // Full name
              email: profile.email, // Email address
              created_at: moment(profile.createdDate, "YYYY-MM-DDThh:mm:ss").format("x"),
              plan: success.data.company.paymentPlan.planTitle,
            }
          );
        }
      },
      fail => {
        this.showSnackBar(fail.data);
      }
    );
  };
  /**
   * it is called to show upgrade dialog plan depending upon mode
   */
  exitPackageDialog = () => {
    // this.removePromocode();
    this.setState({ openPackages: false });
    if (this.props.closeTrialPopUp) this.props.closeTrialPopUp();
  };
  /**
   * it is called to remove promocode form state
   */
  removePromocode = () => {
    this.props.UpdatePromoCodeValue(
      null,
      response => {},
      fail => {}
    );
  };
  showPackages = mode => {
    // this.setState({ openPackages: true, packageMode: mode });
    let data = {
      show: true,
      mode: PlanConstant.TRIALPLANMODE,
    };
    this.props.showUpgradePlan(data);
    if (this.props.selectUpgrade) this.props.selectUpgrade();
    // this.props({ openPackages: true, packageMode: mode });
  };
  showUpgradeDialog = () => {
    let data = {
      show: true,
      mode: PlanConstant.UPGRADEPLANMODE,
    };
    if(window.dataLayer){
      window.dataLayer.push({'event':'upgrade-feature-not-in-plan-click'});

    }
    this.props.showUpgradePlan(data);
    if (this.props.selectUpgrade) this.props.selectUpgrade();
  };
  selectPackage = packageInfo => {
    this.startTrialPlanHandler(packageInfo);
  };
  successfullyExit = () => {
    this.props.UpgradeTrailInStore(this.state.response, success => {
      this.setState({ isTrialSuccess: false });
    });
  };
  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  render() {
    const {
      openBillDetail,
      openPackages,
      packageDetails,
      selectedTeam,
      openDialog,
      btnSwitchQuery,
      packageMode,
      isTrialSuccess,
      response,
      mode,
    } = this.state;
    const {
      classes,
      feature,
      titleTxt,
      showDescription,
      boldText,
      descriptionTxt,
      showBodyImg,
      imgUrl,
      isOwner,
      titleStyle,
    } = this.props;
    const { paymentPlanTitle, isTrialUsed } = this.props.subscriptionDetails;
    const TitleStyle = titleStyle ? { ...titleStyle } : {};

    return (
      // <div className={classes.main}>
      <Fragment>
        <div className={classes.mainCnt}>
          <img src={feature == "premium" ? Premium : Business} className={classes.planIcon} />
          <Typography variant="h2" className={classes.planTitle} style={TitleStyle}>
            {titleTxt}
          </Typography>
          {showDescription && (
            <Typography variant="body2" className={classes.planCnt}>
              <b className={classes.boldText}>{`${boldText}`}</b>&nbsp;{descriptionTxt}
            </Typography>
          )}
          {isOwner &&
            !PlanConstant.isPremiumTrial(paymentPlanTitle) &&
            !PlanConstant.isPremiumPlan(paymentPlanTitle) &&
            !isTrialUsed && (
              <div className={classes.linkButtonDiv}>
                <a
                  href="https://www.ntaskmanager.com/pricing/"
                  className={classes.linkStyle}
                  target="_blank">
                  <FormattedMessage
                    id="common.action.learn-more.label"
                    defaultMessage="Learn More About Plans"
                  />
                </a>
              </div>
            )}
          {showBodyImg && <img src={imgUrl} className={classes.planImage} />}
          <div className={classes.actionCnt}>
            {isOwner &&
              (PlanConstant.isPlanFree(paymentPlanTitle) ||
                PlanConstant.isPlanTrial(paymentPlanTitle)) && (
                <CustomButton
                  btnType="blue"
                  variant="contained"
                  style={{ marginBottom: 0, marginTop: 0 }}
                  onClick={this.showUpgradeDialog}>
                  <FormattedMessage id="common.action.upgrade.label" defaultMessage="Upgrade Now" />
                </CustomButton>
              )}
              <span className={classes.linkBtw} style={{marginTop: 10, marginRight: 0}}>or</span>
            {!isOwner &&
              (PlanConstant.isPlanFree(paymentPlanTitle) ||
                PlanConstant.isPlanTrial(paymentPlanTitle) ||
                PlanConstant.isPremiumPlan(paymentPlanTitle)) && (
                <span className={classes.memberCnt}>
                  <FormattedMessage
                    id="common.discovered-dialog.contact-label"
                    defaultMessage="Contact your team owner to upgrade"
                  />
                </span>
              )}
            {isOwner && PlanConstant.isPremiumPlan(paymentPlanTitle) && (
              <span className={classes.memberCnt}>
                <FormattedMessage
                  id="common.discovered-dialog.contact-upgrade-label"
                  values={{
                    a: (
                      <a href="mailto: support@ntaskmanager.com" className={classes.linkBtnStyle}>
                        {" "}
                        support@ntaskmanager.com{" "}
                      </a>
                    ),
                  }}
                  defaultMessage={`Contact ${(
                    <a href="mailto: support@ntaskmanager.com" className={classes.linkBtnStyle}>
                      {" "}
                      support@ntaskmanager.com{" "}
                    </a>
                  )} to upgrade to Business Plan`}
                />
              </span>
            )}
          </div>
          {isOwner &&
            !PlanConstant.isPremiumTrial(paymentPlanTitle) &&
            !PlanConstant.isPremiumPlan(paymentPlanTitle) &&
            !isTrialUsed && (
              <div className={classes.linkButtonDiv}>
                <a
                  href="javascript:void(0)"
                  className={classes.linkStyleTrial}
                  style={{paddingTop:10}}
                  onClick={() => this.showPackages(Constants.TRIALPLANMODE)}>
                  <FormattedMessage
                    id="common.action.trial.label"
                    defaultMessage={"Start {TRIALPERIOD}-day free trial"}
                    values={{TRIALPERIOD: TRIALPERIOD}}
                  />
                </a>
                {/* <span className={classes.linkBtw}>or</span>
                <a
                  href="https://www.ntaskmanager.com/pricing/"
                  className={classes.linkStyle}
                  target="_blank">
                  <FormattedMessage
                    id="common.action.learn-more.label"
                    defaultMessage="Learn More About Plans"
                  />
                </a> */}
              </div>
            )}
          {(PlanConstant.isPremiumPlan(paymentPlanTitle) ||
            PlanConstant.isPremiumTrial(paymentPlanTitle) ||
            isTrialUsed ||
            !isOwner) && (
            <a
              href="https://www.ntaskmanager.com/pricing/"
              className={classes.linkStyle}
              style={{ marginTop: 13 }}
              target="_blank">
              <FormattedMessage
                id="common.action.learn-more.label"
                defaultMessage="Learn More About Plans"
              />
            </a>
          )}
        </div>
        {/* <NoPlan
        showSnackBar={this.props.showSnackBar}
        startTrialPlanBilling={this.startTrialPlanBilling}
        paymentPaidDone={(data) => this.paymentPaidDone(data)}
      ></NoPlan> */}
        {openPackages && (
          <PlanPackages
            selectPackage={this.selectPackage}
            showSnackBar={this.props.showSnackBar}
            exitPackageDialog={this.exitPackageDialog}
            mode={packageMode}></PlanPackages>
        )}
        {isTrialSuccess && (
          <AddBillSuccess successfullyExit={this.successfullyExit} mode={mode + "Trial"} />
        )}
      </Fragment>
    );
  }
}
UnPlanned.defaultProps = {
  classes: {},
  feature: "",
  titleTxt: "",
  showDescription: "",
  descriptionTxt: "",
  showBodyImg: "",
  imgUrl: "",
  isOwner: false,
  titleStyle: {},
};

const mapStateToProps = state => {
  return {
    profile: state.profile.data,
    isOwner: state.profile.data.teamMember.find(item => item.userId == state.profile.data.userId)
      .isOwner,
    subscriptionDetails: state.profile.data.teams.find(
      item => item.companyId == state.profile.data.activeTeam
    ).subscriptionDetails,
  };
};
export default compose(
  withRouter,
  withSnackbar,
  connect(mapStateToProps, {
    showUpgradePlan,
    StartTrialPlanBilling,
    UpgradeTrailInStore,
  }),
  withStyles(classes, { withTheme: true })
)(UnPlanned);
