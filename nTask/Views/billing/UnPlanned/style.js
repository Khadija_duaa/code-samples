import bgPattern from "../../../assets/images/unplanned/dots_pattern.png"

const UnPlannedStyles = theme => ({
    // main: {
    //     display:'flex',
    //     flexDirection:'column',
    //     alignItems:'center',
    //     height: '100%',
    //     padding: '15px 0 23px 0',
    //     background: `url(${bgPattern})`,
    // },
    mainCnt: {
        display:'flex',
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'flex-start',
        height:"100%"
    },
    planIcon: {

    },
    planTitle: {
        fontSize: "20px !important",
        marginTop: 10,
        textAlign: 'center',
    },
    planCnt: {
        marginTop: 10,
        textAlign: 'center',
    },
    planImage: {
        // marginTop: 24,
    },
    actionCnt: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        marginTop: 15,
    },
    memberCnt: {
        fontSize: "12px !important",
        textAlign: 'center',
        lineHeight: 1.5,
    },
    linkStyle : {
        color: theme.palette.text.primary,
        fontSize: "13px !important",
        // marginTop: 15,
    },
    linkBtnStyle: {
        color: theme.palette.text.green,
        fontSize: "12px !important",
    },
    linkStyleTrial : {
        color: theme.palette.text.linkBlue,
        fontSize: "13px !important",
        // marginRight: 14
        // marginTop: 15,
    },
    linkBtw : {
        color: theme.palette.text.primary,
        fontSize: "13px !important",
        marginRight: 10

    },
    linkButtonDiv : {
        padding: "15px 0"
    },
    boldText: {
        color: theme.palette.text.primary,
        fontSize: "12px !important",
        fontWeight: 500
      }
});
export default UnPlannedStyles;