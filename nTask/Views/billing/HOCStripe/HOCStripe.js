import React from 'react';
import {StripeProvider, Elements} from 'react-stripe-elements';

const hocStripe = (props) => (
    <StripeProvider apiKey={STRIPE_PUBLISH_KEY}>
        <Elements>
            {props.children}
        </Elements>
    </StripeProvider>
)
export default hocStripe;