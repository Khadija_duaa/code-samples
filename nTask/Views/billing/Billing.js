import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import NoPlan from "./NoPlan/NoPlan";
import classes from "./style";
import Plan from "./Plan/Plan";
import TrialPlan from "./TrialPlan/TrialPlan";
import {
  StartTrialPlanBilling,
  UpgradePremiumPlanState
  , UpgradeTrailInStore
} from "../../redux/actions/userBillPlan";
import Typography from "@material-ui/core/Typography";
import Constants from "../../components/constants/planConstant";
import moment from "moment";
import AddBillSuccess from "./AddBillDetail/AddBillSuccess";

class Billing extends Component {
  constructor() {
    super();
    this.state = {
      isTrialSuccess: false,
      response: null,
      mode: ""
    }
  }
  /**
   * it is called when user tial start
   */
  startTrialPlanBilling = (selectedPackage) => {
    const { teamId, profile } = this.props;
    let mode = selectedPackage.planType;
    let companyInfo = {
      mode: mode,
      companyId: teamId,
    };
    this.props.StartTrialPlanBilling(
      companyInfo,
      (success) => {
        // this.props.showSnackBar(success.data.status);
        this.setState({ isTrialSuccess: true, mode: mode, response: success });
        if (ENV == "production" && window.userpilot) {
          window.userpilot.identify(
            profile.userId, // Used to identify users
            {
              name: profile.fullName, // Full name
              email: profile.email, // Email address
              created_at: moment(
                profile.createdDate,
                "YYYY-MM-DDThh:mm:ss"
              ).format("x"),
              plan: success.data.company.paymentPlan.planTitle,
            }
          );
        }
      },
      (fail) => {
        this.props.showSnackBar(fail.data);
      }
    );
  };
  /**
   * it is called when user pay his dues and we change the state of the store
   */
  paymentPaidDone = (data) => {
    this.props.UpgradePremiumPlanState(data);
  };
  successfullyExit = () => {
    this.props.UpgradeTrailInStore(this.state.response, success => {
      this.setState({ isTrialSuccess: false });
    })

  };
  render() {
    const { classes, theme, subscriptionDetails, gracePeriod } = this.props;
    const { isTrialSuccess, response, mode } = this.state;
    let {
      paymentPlanTitle,
      isTrialUsed,
      currentPeriondEndDate,
    } = subscriptionDetails;
    let planMode = Constants.UPGRADEFROMFREE;
    let openPlan = false;
    if (
      Constants.isPremiumPlan(paymentPlanTitle) ||
      Constants.isBusinessPlan(paymentPlanTitle)
    ) {
      let expMomentDate = moment(
        new Date(currentPeriondEndDate),
        "MMMM DD, YYYY"
      );
      let nowMomentDate = moment(new Date(), "MMMM DD, YYYY");
      let diff = expMomentDate.diff(nowMomentDate, "days");
      if (diff < 0 && diff > -gracePeriod) {
        planMode = Constants.GRACEPERIODPLAN;
      } else {
        planMode = Constants.UPGRADEFROMTRIAL;
      }
      openPlan = true;
    } else if (isTrialUsed && Constants.isPlanFree(paymentPlanTitle)) {
      planMode = Constants.UPGRADEFROMFREE;
      openPlan = true;
    }
    return <>
      {Constants.isPlanFree(paymentPlanTitle) && !isTrialUsed ? (
        <NoPlan
          showSnackBar={this.props.showSnackBar}
          startTrialPlanBilling={this.startTrialPlanBilling}
          paymentPaidDone={(data) => this.paymentPaidDone(data)}
        ></NoPlan>
      ) : paymentPlanTitle == Constants.PREMIUMTRIALPLAN ||
        paymentPlanTitle == Constants.BUSINESSTRIALPLAN ? (
            <TrialPlan
              showSnackBar={this.props.showSnackBar}
              paymentPaidDone={(data) => this.paymentPaidDone(data)}
            ></TrialPlan>
          ) : openPlan ? (
            <Plan
              showSnackBar={this.props.showSnackBar}
              paymentPaidDone={(data) => this.paymentPaidDone(data)}
              mode={planMode}
            ></Plan>
          ) : (
              <div className={classes.notPlan}>
                <Typography variant="body2">Billing Is Coming Soon ...</Typography>
              </div>
            )}
      {isTrialSuccess &&
        <AddBillSuccess
          successfullyExit={this.successfullyExit}
          mode={mode + "Trial"}
        />
      }
    </>

  }
}
const mapStateToProps = (state) => {
  return {
    subscriptionDetails: state.profile.data.teams.find(
      (item) => item.companyId == state.profile.data.activeTeam
    ).subscriptionDetails,
    teamId: state.profile.data.activeTeam,
    gracePeriod: state.profile.data.gracePeriod,
    profile: state.profile.data,
  };
};
export default compose(
  withRouter,
  connect(mapStateToProps, {
    StartTrialPlanBilling,
    UpgradePremiumPlanState,
    UpgradeTrailInStore
  }),
  withStyles(classes, { withTheme: true })
)(Billing);
