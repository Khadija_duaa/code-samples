import planBgImg from "../../../assets/images/bg_pattern.png"

const TrialExpireStyles = theme => ({ 
  trialExpireDetailsDialog: {
    
  },
  trialExpirePaperCnt: {
    overflowY: "visible",
    background: theme.palette.common.white,
    width: "470px!important",
  },
  trialExpireContent: {
    padding: '40px 30px 40px 30px!important',
    background: `url(${planBgImg})`,
    backgroundRepeat: "no-repeat",
    backgroundPositionY: -100,
  },
  expireCnt: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    textAlign: 'center',
  },
  proTrialTitle: {
    marginTop: 26
  },
  proTrialCnt: {
    marginTop: 20
  },
  proTrialBtns: {
    marginTop: 40
  },
  seprator: {
    marginTop: 30,
    display: 'flex',
    width: '100%',
    alignItems: 'center',
  },
  sepratorText: {
    fontSize: "12px !important",
    width: 36,
    height: 36,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: '50%',
    alignItems: 'center',
    display: 'flex',
    placeContent: 'center',
  },
  sepratorHrLeft: {
    marginRight: 5,
    marginLeft: 0,
    width: 'calc(50% - 24px)',
  },
  sepratorHrRight : {
    marginLeft: 5,
    marginRight: 0,
    width: 'calc(50% - 24px)',
  },
  trialExpireBodyBottom: {
    // marginTop: 30,
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
  },
  trialExpireBodyBottomBtn: {
    paddingTop: 30,
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  selectionDiv: {
    width: '60%',
    textAlign: 'left'
  },
});
export default TrialExpireStyles;