import React, { Component, Fragment } from "react";
import { withSnackbar } from "notistack";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import classes from "./style";
import Typography from "@material-ui/core/Typography";
import CustomButton from "../../../components/Buttons/CustomButton";
import { StripeProvider, Elements } from "react-stripe-elements";
import AddBillDetail from "../AddBillDetail/AddBillDetail";
import { generateCompaniesData } from "../../../helper/generateSelectData";
import CustomAvatar from "../../../components/Avatar/Avatar";
import Divider from "@material-ui/core/Divider";
import SelectSearchDropdown from "../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import Constants from "../../../components/constants/planConstant";
import PlanPackages from "../PlanPackages/PlanPackages";
import RestrictedAccess from "../RestrictedAccess/RestrictedAccess";
import {
  switchTeam,
  whiteLabelInfo,
  DefaultWhiteLabelInfo,
} from "../../../redux/actions/teamMembers";
import { FetchUserInfo } from "../../../redux/actions/profile";
import DefaultDialog from "../../../components/Dialog/Dialog";
import CreateTeamForm from "../../AddNewForms/addNewTeamFrom";
import ConstantsPlan from "../../../components/constants/planConstant";
import moment from "moment";
import { FormattedMessage } from "react-intl";

class TrialExpire extends Component {
  constructor(props) {
    super();
    this.state = {
      openBillDetail: false,
      selectedTeam: [],
      packageDetails: null,
      openPackages: false,
      openDialog: false,
      btnSwitchQuery: "",
      paymentPlanTitle: props.subscriptionDetails.paymentPlanTitle,
    };
  }
  /**
   * it is called to close current dialog
   */
  closePopUp = () => {
    if (this.props.closeTrialExpirePopUp) this.props.closeTrialExpirePopUp();
  };
  /**
   * it is called to set team in selection contorl
   * @param {string} key
   * @param {string} value
   */
  handleTeamChange = (key, value) => {
    this.setState({ [key]: value });
  };
  /**
   * it is called when user move to free team
   */
  continueFree = () => {
    let { companyId, subscriptionDetails, tasks } = this.props;
    // const mappingRequired = tasks.length == 0 ? false : tasks.some(task => task.status > 4);
    let obj = {
      companyId: companyId,
      // isStatusMappingRequired : subscriptionDetails.paymentPlanTitle == "BusinessTrial" && mappingRequired ?  true : false
      isStatusMappingRequired : subscriptionDetails.paymentPlanTitle == "BusinessTrial" ?  true : false
    };
    if (this.props.continueFreePlan) this.props.continueFreePlan(obj);  
  };
  /**
   * it is called to open add bill details dialog
   */
  upgradePlan = () => {
    this.setState({ openBillDetail: true });
  };
  /**
   * it is called when user pay his dues
   * @param {object} planData
   */
  paymentPaidHandler = planData => {
    if (this.props.paymentPaidDone) this.props.paymentPaidDone(planData);
  };
  /**
   * it is called when user exit from add bill dialog withour pay
   */
  exitBillDetail = () => {
    this.setState({ openBillDetail: false });
  };
  /**
   * it is called when user select team from selection control and called API to moveon
   * @param {string} type
   * @param {string} item
   */
  switchToOtherTeam = (type, item) => {
    const { selectedTeam } = this.state;
    if (!selectedTeam.id) return;
    // this.props.switchExpirePlan();
    this.props.closeTrialExpirePopUp();
    this.props.showLoadingState();
    this.setState({ btnSwitchQuery: "progress" });
    switchTeam(selectedTeam.id, response => {
      if (selectedTeam.isEnableWhiteLabeling && selectedTeam.isEnableWhiteLabeling == true)
        this.props.whiteLabelInfo(selectedTeam.id);
      else this.props.DefaultWhiteLabelInfo();
      this.props.FetchUserInfo(userApiResponse => {
        // this.setState({btnSwitchQuery: ''});
        this.props.history.push(`/teams/${response.data.team.companyUrl}`);

        const { gracePeriod } = this.props;
        const { paymentPlanTitle, currentPeriondEndDate } = response.data.team.subscriptionDetails;
        if (!ConstantsPlan.isPlanFree(paymentPlanTitle)) {
          if (
            ConstantsPlan.isPlanTrial(paymentPlanTitle) &&
            ConstantsPlan.isPlanExpire(currentPeriondEndDate)
          ) {
            this.setState({ btnSwitchQuery: "", paymentPlanTitle: paymentPlanTitle });
            this.props.showLoadingState();
            this.props.planExpire("Trial");
            return;
          } else if (
            ConstantsPlan.isPlanPaid(paymentPlanTitle) &&
            ConstantsPlan.isPlanExpire(currentPeriondEndDate, gracePeriod)
          ) {
            this.setState({ btnSwitchQuery: "", paymentPlanTitle: paymentPlanTitle });
            this.props.showLoadingState();
            this.props.planExpire("Paid");
            return;
          }
        }
      });
      this.props.hideLoadingState();
    });
  };
  /**
   * it is caled to open package plan dialog
   */
  showPackages = () => {
    this.setState({ openPackages: true });
  };
  /**
   * it is called when user select package plan
   */
  selectPackage = packageInfo => {
    this.setState({ openPackages: false, packageDetails: packageInfo, openBillDetail: true });
  };
  /**
   * it is called when user exit from package plan wihtout select any plan
   */
  exitPackageDialog = () => {
    this.setState({ openPackages: false });
  };
  /**
   * it is called to open new create team dialog
   */
  openCreateTeam = () => {
    this.setState({ openDialog: true });
  };
  /**
   * it is called to close new create team dialog
   */
  handleDialogClose = () => {
    this.setState({ openDialog: false });
  };
  /**
   * it is called when user enter new team and want to move on
   */
  createTeam = () => {
    this.props.createTeam();
  };

  render() {
    const { classes, teams, isOwner, minimumUser, subscriptionDetails } = this.props;
    const { paymentPlanTitle } = this.state;
    let plan = Constants.DisplayPlanName(subscriptionDetails.paymentPlanTitle);
    const {
      openBillDetail,
      openPackages,
      packageDetails,
      selectedTeam,
      openDialog,
      btnSwitchQuery,
    } = this.state;
    let planInfo = "";
    if (openBillDetail) {
      planInfo = { ...packageDetails };
      planInfo.pricePlanType =
        packageDetails.planType == Constants.PREMIUMPLAN
          ? Constants.PREMIUMPLANPRICE
          : Constants.BUSINESSPLANPRICE;
    }

    return (
      <Fragment>
        <DefaultDialog
          title="Create Team"
          sucessBtnText="Create Team"
          open={openDialog}
          onClose={this.handleDialogClose}>
          <CreateTeamForm
            handleDialogClose={this.handleDialogClose}
            createTeamHandler={this.createTeam}
          />
        </DefaultDialog>
        {openBillDetail == false && isOwner == true && (
          <Dialog
            classes={{
              paper: classes.trialExpirePaperCnt,
              scrollBody: classes.dialogCnt,
            }}
            fullWidth={true}
            scroll="body"
            disableBackdropClick={true}
            disableEscapeKeyDown={true}
            open={true}
            onClose={this.closePopUp}>
            <DialogContent className={classes.trialExpireContent}>
              <div className={classes.expireCnt}>
                <CustomAvatar userActiveTeam={true} size="large" />
                <Typography variant="h2" className={classes.proTrialTitle}>
                  {plan}{" "}
                  <FormattedMessage
                    id="team-settings.billing.trial-expired-modal.title"
                    defaultMessage="Expired"
                  />
                </Typography>
                <Typography variant="h6" className={classes.proTrialCnt}>
                  <FormattedMessage
                    id="team-settings.billing.trial-expired-modal.label"
                    values={{ plan: plan }}
                    defaultMessage={`Your nTask ${plan} has now expired. Please upgrade  to continue using all Premium features or choose the Free plan if you would like to continue as a Free user.`}
                  />
                </Typography>
                <div className={classes.proTrialBtns}>
                  <CustomButton
                    btnType="white"
                    variant="contained"
                    style={{ marginRight: 12 }}
                    onClick={this.continueFree}>
                    <FormattedMessage
                      id="team-settings.billing.trial-expired-modal.continue-button.label"
                      defaultMessage="Continue with Free Plan"
                    />
                  </CustomButton>
                  <CustomButton
                    btnType="blue"
                    variant="contained"
                    style={{ marginBottom: 0, padding: "7px 20px" }}
                    onClick={this.showPackages}>
                    <FormattedMessage
                      id="team-settings.billing.upgrade-button.label"
                      defaultMessage="Upgrade"
                    />
                  </CustomButton>
                </div>
                <div className={classes.seprator}>
                  <Divider className={classes.sepratorHrLeft} />
                  <div className={classes.sepratorText}>
                    <span>
                      <FormattedMessage
                        id="team-settings.billing.trial-expired-modal.or-label"
                        defaultMessage="OR"
                      />
                    </span>
                  </div>
                  <Divider className={classes.sepratorHrRight} />
                </div>

                <div className={classes.trialExpireBodyBottom}>
                  {teams.length ? (
                    <Fragment>
                      <Typography variant="h6" className={classes.proTrialCnt}>
                        <FormattedMessage
                          id="team-settings.billing.trial-expired-modal.mean-label"
                          defaultMessage="Meanwhile you can continue your work by switching to one of the appended options."
                        />
                      </Typography>
                      <div className={classes.trialExpireBodyBottomBtn}>
                        <div className={classes.selectionDiv}>
                          <SelectSearchDropdown
                            data={() => generateCompaniesData(teams)}
                            label=""
                            isMulti={false}
                            selectChange={this.handleTeamChange}
                            type="selectedTeam"
                            selectedValue={selectedTeam}
                            placeholder={
                              <FormattedMessage
                                id="team-settings.billing.trial-expired-modal.switch-to-label"
                                defaultMessage="Switch to:"
                              />
                            }
                            styles={{ marginTop: 0, marginBottom: 0 }}
                          />
                        </div>
                        <CustomButton
                          btnType="success"
                          variant="contained"
                          onClick={this.switchToOtherTeam}
                          query={btnSwitchQuery}
                          disabled={btnSwitchQuery === "progress"}>
                          <FormattedMessage
                            id="team-settings.billing.trial-expired-modal.switch.label"
                            defaultMessage="Switch"
                          />
                        </CustomButton>
                      </div>
                    </Fragment>
                  ) : (
                    <div style={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
                      <Typography variant="h6" className={classes.proTrialCnt}>
                        <FormattedMessage
                          id="team-settings.billing.trial-expired-modal.mean-while-team-label"
                          defaultMessage="Meanwhile you can continue working by creating a new team."
                        />
                      </Typography>
                      <CustomButton
                        btnType="success"
                        variant="contained"
                        onClick={this.openCreateTeam}
                        style={{ marginTop: 20 }}>
                        <FormattedMessage
                          id="team-settings.billing.trial-expired-modal.create-team.label"
                          defaultMessage="Create Your Team "
                        />
                      </CustomButton>
                    </div>
                  )}
                </div>
              </div>
            </DialogContent>
          </Dialog>
        )}
        {openBillDetail == false && isOwner == false && (
          <RestrictedAccess
            closeRestrictedDialog={this.props.closeTrialExpirePopUp}
            createTeam={this.createTeam}
            mode={"Trial"}
            hideLoadingState={this.props.hideLoadingState}
            showLoadingState={this.props.showLoadingState}
            planExpire={this.props.planExpire}
            switchExpirePlan={this.props.switchExpirePlan}></RestrictedAccess>
        )}
        {openBillDetail && (
          <StripeProvider apiKey={STRIPE_PUBLISH_KEY}>
            <Elements>
              <AddBillDetail
                showSnackBar={this.props.showSnackBar}
                paymentPaidHandler={planData => this.paymentPaidHandler(planData)}
                exitBillDetail={this.exitBillDetail}
                minimumUser={minimumUser}
                selectedPackageDetails={planInfo}></AddBillDetail>
            </Elements>
          </StripeProvider>
        )}
        {openPackages && (
          <PlanPackages
            selectPackage={this.selectPackage}
            showSnackBar={this.props.showSnackBar}
            exitPackageDialog={this.exitPackageDialog}
            mode={Constants.UPGRADEPLANMODE}></PlanPackages>
        )}
      </Fragment>
    );
  }
}
const mapStateToProps = state => {
  return {
    subscriptionDetails: state.profile.data.teams.find(
      item => item.companyId == state.profile.data.activeTeam
    ).subscriptionDetails,
    isOwner: state.profile.data.teamMember.find(item => item.userId == state.profile.data.userId)
      .isOwner,
    teams: state.profile.data.teams.filter(item => item.companyId != state.profile.data.activeTeam),
    minimumUser: state.profile.data.teamMember.filter(
      item => item.isDeleted == false && (item.isActive == null || item.isActive == true)
    ).length,
    companyId: state.profile.data.activeTeam,
    gracePeriod: state.profile.data.gracePeriod,
    tasks: state.tasks.data || [],

  };
};
export default compose(
  withRouter,
  withSnackbar,
  withStyles(classes, { withTheme: true }),
  connect(mapStateToProps, {
    FetchUserInfo,
    whiteLabelInfo,
    DefaultWhiteLabelInfo,
  })
)(TrialExpire);
