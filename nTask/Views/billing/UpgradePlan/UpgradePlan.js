import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import classes from "./style";
import PlanPackages from "../PlanPackages/PlanPackages";
import Constants from "../../../components/constants/planConstant";
import AddBillDetail from "../AddBillDetail/AddBillDetail";
import { StripeProvider, Elements } from "react-stripe-elements";
import {
  UpgradePremiumPlanState,
  showUpgradePlan,
  StartTrialPlanBilling,
  UpgradeTrailInStore,
} from "../../../redux/actions/userBillPlan";
import { withSnackbar } from "notistack";
import HOCStripe from "../HOCStripe/HOCStripe";
import AddBillSuccess from "../AddBillDetail/AddBillSuccess";
import moment from "moment";

class UpgradePlan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openBillDetail: false,
      openPackages: props.openPackages,
      packageDetails: null,
      packageMode: props.mode,
      isTrialSuccess: false,
      response: null,
      mode: "",
    };
  }
  componentDidMount() {
    const { openBillDetail, mode } = this.props;
    if (mode === Constants.BUSINESSPAYMENT) {
      this.setState({
        openPackages: false,
        packageDetails: {
          subscription: "annually",
          planType: "Business",
          trailPaid: "Upgrade",
        },
        openBillDetail: true,
      });
    } else if (mode === Constants.PREMIUMPAYMENT) {
      this.setState({
        openPackages: false,
        packageDetails: {
          subscription: "annually",
          planType: "Premium",
          trailPaid: "Upgrade",
        },
        openBillDetail: true,
      });
    }
  }
  startTrialHandler = selectedPackage => {
    let mode = selectedPackage.planType;
    let companyInfo = {
      mode: mode,
      companyId: this.props.teamId,
    };
    const { profile } = this.props;
    this.props.StartTrialPlanBilling(
      companyInfo,
      success => {
        // this.setState({packageMode: "Premium"});
        this.setState({ isTrialSuccess: true, mode: mode, response: success, openPackages: false });
        if (ENV == "production" && window.userpilot) {
          window.userpilot.identify(
            profile.userId, // Used to identify users
            {
              name: profile.fullName, // Full name
              email: profile.email, // Email address
              created_at: moment(profile.createdDate, "YYYY-MM-DDThh:mm:ss").format("x"),
              plan: success.data.company.paymentPlan.planTitle,
            }
          );
        }
        // this.showSnackBar(success.data.status);
      },
      fail => {
        this.showSnackBar(fail.data);
      }
    );
  };
  /**
   * it is called when select packge and in which mode the package was
   * @param {object} packageInfo
   */
  selectPackage = packageInfo => {
    if (Constants.TRIALPLANMODE == packageInfo.trailPaid) {
      this.setState({ openPackages: false });
      this.startTrialHandler(packageInfo);
    } else if (Constants.UPGRADEPLANMODE == packageInfo.trailPaid) {
      this.setState({ openPackages: false, packageDetails: packageInfo, openBillDetail: true });
    }
  };

  /**
   * it is called when upgrade plan and exit from add bill details dialog
   * @param {object} planData
   */
  paymentPaidHandler = planData => {
    this.props.UpgradePremiumPlanState(planData);
    this.props.showUpgradePlan(false);
  };
  /**
   * it is called when don't upgrade plan and exit from add bill details dialog
   */
  exitBillDetail = () => {
    this.setState({ openBillDetail: false });
    this.exitUpgradePlan();
  };
  /**
   * it is called when don't select package and exit from package dialog
   */
  exitPackageDialog = () => {
    this.setState({ openPackages: false });
    this.exitUpgradePlan();
  };
  /**
   * it is called when don't upgrade his plan to premium plan
   */
  exitUpgradePlan = () => {
    this.props.exitUpgrade();
  };

  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  successfullyExit = () => {
    this.props.UpgradeTrailInStore(this.state.response, success => {
      this.setState({ isTrialSuccess: false });
    });
  };
  render() {
    const { classes, minimumUser } = this.props;
    const {
      openBillDetail,
      openPackages,
      packageDetails,
      packageMode,
      isTrialSuccess,
      mode,
    } = this.state;
    let planInfo = "";
    if (openBillDetail) {
      planInfo = { ...packageDetails };
      planInfo.pricePlanType =
        packageDetails.planType == Constants.PREMIUMPLAN
          ? Constants.PREMIUMPLANPRICE
          : Constants.BUSINESSPLANPRICE;
    }
    return (
      <div style={{ backgroundColor: "white" }}>
        {isTrialSuccess && (
          <AddBillSuccess
            successfullyExit={this.successfullyExit}
            mode={mode + "Trial"}></AddBillSuccess>
        )}
        {openPackages && (
          <PlanPackages
            selectPackage={this.selectPackage}
            exitPackageDialog={this.exitPackageDialog}
            showSnackBar={this.showSnackBar}
            mode={packageMode}></PlanPackages>
        )}
        {openBillDetail && (
          <HOCStripe>
            <AddBillDetail
              showSnackBar={this.showSnackBar}
              paymentPaidHandler={planData => this.paymentPaidHandler(planData)}
              exitBillDetail={this.exitBillDetail}
              minimumUser={minimumUser}
              selectedPackageDetails={planInfo}></AddBillDetail>
          </HOCStripe>
        )}
      </div>
    );
  }
}
UpgradePlan.defaultProps = {
  openBillDetail: false,
  packageDetails: null,
};
const mapStateToProps = state => {
  return {
    minimumUser: state.profile.data.teamMember.filter(
      item => item.isDeleted == false && (item.isActive == null || item.isActive == true)
    ).length,
    teamId: state.profile.data.activeTeam,
    profile: state.profile.data,
    mode: state.userBillPlan.modeUpgradePlan,
  };
};
export default compose(
  withRouter,
  withSnackbar,
  connect(mapStateToProps, {
    UpgradePremiumPlanState,
    showUpgradePlan,
    StartTrialPlanBilling,
    UpgradeTrailInStore,
  }),
  withStyles(classes, { withTheme: true })
)(UpgradePlan);
