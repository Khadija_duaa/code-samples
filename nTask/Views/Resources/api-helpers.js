import { groupBy, flatMap, orderBy } from "lodash";
import moment from "moment";
import { dayComparisonDateFormat } from "./constants";
import { generateDateRange, parseISODateString } from "../../helper/dates/dates";
import { formatMomentToDate } from "./temp-utils";

export const dayWorkloadObjectTemplate = {
  moment: null,
  date: null,
  id: null,
  load: null,
};

export const parseEstimates = (estimatesList, calendarDates) => {
  let parsedHashMap;

  if (estimatesList && estimatesList.length > 0) {
    const simplifiedList = estimatesList.map((k) => ({
      hours: k.estimate,
      date: moment(k.estimationDate).format(dayComparisonDateFormat),
    }));

    const groupedByDate = groupBy(simplifiedList, (k) => k.date);

    const summed = flatMap(Object.values(groupedByDate), (k) => {
      if (k.length <= 0) {
        return;
      }
      return {
        date: k.at(0).date,
        hours: k.reduce((acc, curr) => acc + curr.hours, 0),
      };
    });

    // const dateSortFn = (a, b) => moment(a.date).isBefore(b.date);
    // const sortedList = summed.sort(dateSortFn);
    const sortedList = orderBy(summed, (k) => k.date, ["asc"]);

    const mapped = new Map(sortedList.map((k) => [k.date, k.hours]));

    parsedHashMap = mapped;
  }

  // const calendarDates = getState().resources.data.calendarDatesFlattened;

  const outputArr = Object.values(calendarDates).map((k) => {
    const date = k.momentObj.format(dayComparisonDateFormat);

    const load = parsedHashMap?.has(date) ? parsedHashMap.get(date) : null;

    return {
      ...dayWorkloadObjectTemplate,

      moment: k,
      date,
      load,
      // capacity,
      // offDay: false,
    };
  });

  return outputArr;
};

export const parseResourcesEstimates = (estimatesList, capacity, calendarDates) => {
  return parseEstimates(estimatesList, calendarDates).map((k) => ({
    ...k,
    load: k.load ?? 0,
    capacity,
    // offDay: false
  }));
};

export const parseProjectsEstimates = (estimatesList, calendarDates) => {
  return parseEstimates(estimatesList, calendarDates);
};

export const parseTaskEstimates = (estimatesList, taskDates) => {
  let parsedHashMap;

  if (estimatesList && estimatesList.length > 0) {
    const simplifiedList = estimatesList.map((k) => ({
      id: k._id,
      hours: k.estimate,
      date: moment(k.estimationDate).format(dayComparisonDateFormat),
    }));

    const groupedByDate = groupBy(simplifiedList, (k) => k.date);

    const summed = flatMap(Object.values(groupedByDate), (k) => {
      if (k.length <= 0) {
        return;
      }
      return {
        id: k.at(0).id,
        date: k.at(0).date,
        hours: k.reduce((acc, curr) => acc + curr.hours, 0),
      };
    });

    // const dateSortFn = (a, b) => moment(a.date).isBefore(b.date);
    // const sortedList = summed.sort(dateSortFn);
    const sortedList = orderBy(summed, (k) => k.date, ["asc"]);

    const mapped = new Map(sortedList.map((k) => [k.date, { id: k.id, hours: k.hours }]));

    parsedHashMap = mapped;
  }

  // const calendarDates = getState().resources.data.calendarDatesFlattened;

  const outputArr = taskDates.map((k) => {
    const date = formatMomentToDate(k);
    const dateData = parsedHashMap?.has(date) ? parsedHashMap.get(date) : null;

    return {
      ...dayWorkloadObjectTemplate,

      moment: k,
      date,
      id: dateData?.id,
      load: dateData?.hours,
      // load: 3,
      // capacity,
      // offDay: false,
    };
  });

  return outputArr;
};

export const parseResourceOrUnassignedTaskEstimates = (estimatesList, calendarDates) => {
  return parseTaskEstimates(estimatesList, calendarDates).map((k) => ({
    ...k,
    // load: k.load ?? 0,
    // capacity,
    // offDay: false
  }));
};

export const parseProjectsApiResponse = (projectsList, calendarDates) => {
  const parsed = projectsList.map((k) => ({
    id: k._id,
    title: k.projectName,

    dailyWorkload: parseProjectsEstimates(k.estimates, calendarDates),
    taskIds: [],
  }));

  return parsed;
};

export const parseResourcesApiResponse = (resourcesList, calendarDates) => {
  const parsed = resourcesList.map((k) => ({
    id: k.resourceId,
    name: k.fullName,
    email: k.email,
    // designation: "Product Manager" || "Missing Designation",
    designation: k.designation,
    // imageUrl: "https://picsum.photos/100",
    imageUrl: k.pictureUrl,

    dailyWorkload: parseResourcesEstimates(
      k.estimates,
      // k.capacity // TODO: here is hardcoded value for daily capacity
      8,
      calendarDates
    ),
    taskIds: [],
    totalCapacity: k.capacity,
  }));

  return parsed;
};

export const parseApiTaskRecord = (task) => {
  const plannedStartDate = task.startDate;
  const plannedEndDate = task.endDate;

  const taskDates = generateDateRange(plannedStartDate, plannedEndDate);

  const plannedStartDateParsed = parseISODateString(task.startDate);
  const plannedEndDateParsed = parseISODateString(task.endDate);

  return {
    id: task.taskId,
    name: task.taskTitle,

    // plannedStartDate: "2022-12-15T02:00:00",
    // plannedEndDate: "2022-12-27T23:55:00",

    // plannedStartDate: plannedStartDate,
    // plannedEndDate: plannedEndDate,

    // plannedStartDate: plannedStartDateParsed.isValid() ? plannedStartDate : null,
    // plannedEndDate: plannedEndDateParsed.isValid() ? plannedEndDate : null,

    // plannedStartDate: plannedStartDateParsed.isValid() ? formatMomentToDate (plannedStartDateParsed) : null,
    // plannedEndDate: plannedEndDateParsed.isValid() ? formatMomentToDate(plannedEndDateParsed) : null,

    plannedStartDate: plannedStartDateParsed?.isValid() ? plannedStartDateParsed : null,
    plannedEndDate: plannedEndDateParsed?.isValid() ? plannedEndDateParsed : null,

    estimatedHours: 0,
    estimatedMinutes: 0,
    // parentTaskId: null,
    // parentTask: null,
    parentTask: task.parentTaskId &&
      task.parentTaskTitle && {
        id: task.parentTaskId,
        name: task.parentTaskTitle,
      },

    // projectId: task.projectId,
    // project: null,
    project: task.projectId &&
      task.projectName && {
        id: task.projectId,
        name: task.projectName,
      },

    workspaceId: task.workspaceId,
    // NOTE: this field will be array bcz one task will be assignable to multiple resources
    assignedToResourceId: task.resourceId || null,
    // totalEffort: [
    //   {
    //     date: "2022-12-20",
    //     minutes: 5 * 60,
    //   },
    //   {
    //     date: "2022-12-21",
    //     minutes: 1 * 60,
    //   },
    //   {
    //     date: "2022-12-22",
    //     minutes: 3 * 60,
    //   },
    //   {
    //     date: "2022-12-23",
    //     minutes: 3 * 60,
    //   },
    // ],
    totalEffort: [],
    dailyWorkload: parseResourceOrUnassignedTaskEstimates(task.estimates, taskDates),

    color: task.taskColor,

    priority: task.priority,

    status: task.statusName && {
      name: task.statusName,
      color: task.statusColor,
    },
  };
};

export const parseResourceOrUnassignedTasksApiResponse = (tasksList) => {
  const parsed = tasksList.map(parseApiTaskRecord);

  return parsed;
};
