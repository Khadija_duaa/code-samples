// @flow
// [@Rizwan-@TODO-@PENDING] : remove unused imports (Date : 2-23-2023)
// [@Rizwan-@TODO-@PENDING] : remove commented code (Date : 2-23-2023)
// [@Rizwan-@TODO-@PENDING] : add code commenting (Date : 2-23-2023)
import React, { useEffect } from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import ResourcesGrid from "./components/ResourcesGrid/ResourcesGrid";
import BaseGrid from "./components/BaseGrid/BaseGrid";
import { useDispatch, useSelector } from "react-redux";
import styles from "./styles";
import { ResourceWorkloadViewType } from "../../utils/constants/ResourceWorkloadViewType";
import ProjectsGrid from "./components/ProjectsGrid/ProjectsGrid";
import FiltersRow from "./components/FiltersRow/FiltersRow";
import { resetGridDataActionCreator } from "../../redux/actionCreators/resources";
import { DragDropContext } from "react-beautiful-dnd";
import {
  assignUnassignedTaskToResourceThunk,
  reassignResourceTaskThunk,
} from "../../redux/thunks/resources";
import { withSnackbarNotifs } from "./../../hoc";
import PropTypes from "prop-types";

function ResourcesWorkloadPage(props) {
  const { classes, showSnackbar } = props;

  const dispatch = useDispatch();

  const workloadViewType = useSelector((s) => s.resources.uiGrid.workloadViewType);

  useEffect(() => {
    dispatch(resetGridDataActionCreator());
  }, []);

  const onDragStart = (result) => {
    console.info("onDragStart:", result);
    // SAMPLE OUTPUT
    // {
    //   draggableId: 'task-1',
    //   type: 'TYPE',
    //   source: {
    //     droppableId: 'column-1',
    //     index: 0
    //   }
    // }
  };

  const onDragUpdate = (result) => {
    console.info("onDragUpdate:", result);
    // SAMPLE OUTPUT
    // {
    //   draggableId: 'task-1',
    //   type: 'TYPE',
    //   source: {
    //     droppableId: 'column-1',
    //     index: 0
    //   },
    //   destination: {
    //     droppableId: 'column-1',
    //     index: 1
    //   }
    // }
  };

  const onDragEnd = (result) => {
    // Info: available droppables: droppable-tasks-unassigned, droppable-resource-ID_STRING

    console.info("onDragEnd:", result);
    // SAMPLE OUTPUT
    // {
    //   draggableId: 'task-1',
    //   type: 'TYPE',
    //   source: {
    //     droppableId: 'column-1',
    //     index: 0
    //   },
    //   destination?: {
    //     droppableId: 'column-1',
    //     index: 1
    //   },
    //   reason: 'DROP'
    // }

    const { destination, source } = result;

    if (!destination) {
      // Note: do nothing
      return;
    }

    // // dragged and dropped on same list as well as same position
    // if (destination.droppableId === source.droppableId && destination.index === source.index) {
    //   return;
    // }

    // dragged and dropped in same list
    if (destination.droppableId === source.droppableId) {
      // dropped on same position

      if (destination.index === source.index) {
        console.info("onDragEnd - Info: same list: same position");
        // TODO: do nothing
        return;
      }

      // position changed within same list
      console.info("onDragEnd - Info: same list: different position");

      if ((destination.droppableId || source.droppableId) === "droppable-tasks-unassigned") {
        console.info("onDragEnd - Info: same list: different position - unassigned tasks");
        // TODO: here update the tasks order within unassigned tasks list

        dispatch({
          type: "TEMP_REORDER_UNASSIGNED_TASK",
          payload: {
            fromIndex: source.index,
            toIndex: destination.index,
          },
        });

        return;
      }

      console.info("onDragEnd - Info: same list: different position - resource tasks");
      // TODO: here update the tasks order within resource's tasks list

      const resourceId = (source || destination).droppableId.split("-").at(-1);

      dispatch({
        type: "TEMP_REORDER_RESOURCE_TASK",
        payload: {
          resourceId,
          fromIndex: source.index,
          toIndex: destination.index,
        },
      });

      return;
    }

    // different list

    // check if unassigned tasks list is involved

    // dragged from resource and dropped on unassigned
    if (destination.droppableId === "droppable-tasks-unassigned") {
      console.info("onDragEnd - Info: dropped on unassigned tasks");
      // TODO: remove the task from resource's tasks list
      // TODO: add in unassigned tasks list

      const fromResourceId = source.droppableId.split("-").at(-1);

      dispatch({
        type: "TEMP_UNASSIGN_RESOURCE_TASK",
        payload: {
          fromResourceId,
          fromIndex: source.index,
          toIndex: destination.index,
        },
      });

      return;
    }

    // dragged from unassigned and dropped on resource
    if (source.droppableId === "droppable-tasks-unassigned") {
      console.info("onDragEnd - Info: dragged from unassigned tasks");
      // TODO: remove the task from unassigned tasks list
      // TODO: add in resource's tasks list

      const toResourceId = destination.droppableId.split("-").at(-1);

      // dispatch({
      //   type: "TEMP_ASSIGN_TASK_TO_RESOURCE",
      //   payload: {
      //     toResourceId,
      //     fromIndex: source.index,
      //     toIndex: destination.index,
      //   },
      // });

      dispatch(
        assignUnassignedTaskToResourceThunk({
          toResourceId,
          fromIndex: source.index,
          toIndex: destination.index,
          successCallback: () => {},
          failureCallback: apiCallFailure,
          finallyCallback: () => {},
        })
      );

      return;
    }

    console.info("onDragEnd - Info: dragged from a resource and dropped on other");
    // TODO: remove the task from resource's tasks list
    // TODO: add in other resource's tasks list

    const fromResourceId = source.droppableId.split("-").at(-1);
    const toResourceId = destination.droppableId.split("-").at(-1);

    // TODO: handle the case, dragged task is already assigned to destination resource

    // dispatch({
    //   type: "TEMP_RESOURCE_TASK_REASSIGN",
    //   payload: {
    //     fromResourceId,
    //     toResourceId,
    //     fromIndex: source.index,
    //     toIndex: destination.index,
    //   },
    // });

    dispatch(
      reassignResourceTaskThunk({
        fromResourceId,
        toResourceId,
        fromIndex: source.index,
        toIndex: destination.index,
        successCallback: () => {},
        failureCallback: apiCallFailure,
        finallyCallback: () => {},
      })
    );

    return;

    // // const _tasks = cloneDeep(tasksDataList || []);
    // const _tasks = tasksDataList || [];
    // const sourceInx = _tasks.findIndex(k => k.id === draggableId);
    // // const destinationInx = _tasks.findIndex(k => k.id === draggableId);

    // // TODO: here we'll update indexes in redux store and it'll automatically re-render
    // const newTasksList = cloneDeep(resourceTasksMarkup);
    // console.info("sssssssssss------11111:", Array.from(newTasksList));

    // const sourceItem = newTasksList[source.index];
    // const destinationItem = newTasksList[destination.index];
    // console.info("sssssssssss------22222:", sourceItem, destinationItem);

    // newTasksList.splice(source.index, 1, destinationItem);
    // newTasksList.splice(destination.index, 1, sourceItem);

    // console.info("sssssssssss------33333:", Array.from(newTasksList));
    // setResourceTasksMarkup(newTasksList);
  };

  const apiCallFailure = (error) =>
    showSnackbar(error.data?.message || ((error.status || error.statusText) && `${error.status}: ${error.statusText}`) || "Backend service seems offline!", "error");

  const dragDropContextProps = {
    onDragStart,
    onDragUpdate,
    onDragEnd,
  };

  return (
    <>
      <DragDropContext {...dragDropContextProps}>
        <div className={classes.mainContainer}>
          <div className={classes.filtersRow}>
            <FiltersRow />
          </div>
          <BaseGrid>
            {workloadViewType === ResourceWorkloadViewType.resources.value && <ResourcesGrid />}
            {workloadViewType === ResourceWorkloadViewType.projects.value && <ProjectsGrid />}
          </BaseGrid>
        </div>
      </DragDropContext>
    </>
  );
}

ResourcesWorkloadPage.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
  showSnackbar: PropTypes.func,
};

ResourcesWorkloadPage.defaultProps = { classes: {}, theme: {} };

export default compose(
  withRouter,
  withSnackbarNotifs,
  withStyles(styles, { withTheme: true })
)(ResourcesWorkloadPage);
