import { CalendarTimelinePeriods } from "../../utils/constants/CalendarTimelinePeriods";
import { getCurrentMonthDaysCount } from "./../../helper/dates/dates";
import { dayComparisonDateFormat } from "./constants";

export const generateRandomNumber = (min, max) => {
  const _min = Math.ceil(min);
  const _max = Math.floor(max);

  return Math.floor(Math.random() * (_max - _min + 1) + _min); // The maximum is inclusive and the minimum is inclusive
};

export const generateRandomNumberPair = (min, max) => {
  const one = generateRandomNumber(min, max);
  const two = generateRandomNumber(min, max);

  return one <= two ? [one, two] : [two, one];
};

export const daysList = {
  [CalendarTimelinePeriods.week.value]: 7,
  [CalendarTimelinePeriods.twoWeeks.value]: 14,
  [CalendarTimelinePeriods.month.value]: getCurrentMonthDaysCount(),
};

export const generateOneDayLoadAndCapacity = () => {
  return {
    load: generateRandomNumber(0, 12),
    capacity: generateRandomNumber(4, 8),
    offDay: false,
  };
};

export const generateDailyWorkload = (isSatSunOff = true) => {
  return Array(31)
    .fill(0)
    .map((k, inx) => {
      const i = inx + 1;

      return isSatSunOff && (i % 6 === 0 || i % 7 === 0)
        ? { load: 0, capacity: 0, offDay: true }
        : generateOneDayLoadAndCapacity();
    });
};

export const isValidFunction = (ftn) => ftn && typeof ftn === "function";

export const getCombinedHashMapKey = (firstId, secondId) => `${firstId}+${secondId}`;

export const formatMomentToDate = (moment) => moment?.format(dayComparisonDateFormat) || "";

export const isValidUrl = (urlString) => {
  var urlPattern = new RegExp("(https://|http://|//)", "g"); // validate fragment locator
  return !!urlPattern.test(urlString);
};

export function hexToRgb(hex) {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result
    ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16),
      }
    : null;
}

export const DEFAULT_BACKGROUND_COLOR = "#ffffff";

export function contrastColorFromHex(hex) {
  const { r, g, b } = hexToRgb(hex || DEFAULT_BACKGROUND_COLOR);

  return contrastColor(r, g, b, 186);
}

export function contrastColor(r, g, b, threshold = 149) {
  // ref: https://stackoverflow.com/questions/3942878/how-to-decide-font-color-in-white-or-black-depending-on-background-color
  // ref: https://jsfiddle.net/q781qx52/
  return r * 0.299 + g * 0.587 + b * 0.114 > threshold ? "#000000" : "#ffffff";
}
