import React from "react";
import { compose } from "redux";
import { useSelector } from "react-redux";
import { defaultProps, propTypes } from "../RowWithColumns/props";
import RowWithColumns from "../RowWithColumns";
import { withStyles } from "@material-ui/core/styles";
import styles from "./styles";
import { withRouter } from "react-router-dom";

const GridRow = (props) => {
  const leftColumnCollapsed = useSelector((s) => s.resources.uiGrid.sidebarCollapsed);

  return <RowWithColumns {...{ ...props, leftColumnCollapsed }} />;
};

GridRow.propTypes = { ...propTypes };

GridRow.defaultProps = {
  ...defaultProps,

  leftColumnMinWidth: 250,
  leftColumnMinWidthCollapsed: 60,
  leftColumnMinWidthUnit: "px",

  leftColumnFlexWidth: 250,
  leftColumnFlexWidthCollapsed: 60,
  leftColumnFlexWidthUnit: "px",
};

export default compose(withRouter, withStyles(styles, { withTheme: true }))(GridRow);
