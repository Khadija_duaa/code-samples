// @flow

import React, { memo, useContext } from "react";
import { useSelector } from "react-redux";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import styles from "./styles";
import clsx from "clsx";
import TaskTimeline from "../TaskTimeline";
import TaskCard from "../TaskCard";
import GridRow from "../GridRow";
import { Draggable } from "react-beautiful-dnd";
import PropTypes from "prop-types";
import { ResourceContext, UnassignedTasksContext } from "../../contexts";
import { TaskContext } from "../../contexts/resources";

const TaskRow = (props) => {
  const { classes, taskIndex } = props;

  const { isUnassignedTask = false } = useContext(UnassignedTasksContext) || {};
  const resourceId = useContext(ResourceContext);
  const taskId = useContext(TaskContext);

  const calendarDatesFlattened = useSelector((s) =>
    Object.values(s.resources.data.calendarDatesFlattened)
  );
  const dayBoxWidth = useSelector((s) => s.resources.uiGrid.dayProgressHourglassWidth);

  return (
    <>
      <Draggable
        draggableId={`draggable-${
          isUnassignedTask ? "unassigned" : `resource-${resourceId}`
        }-task-${taskId}`}
        index={taskIndex}>
        {(provided, snapshot) => {
          // console.info("drag-provided:", provided);

          return (
            <div
              ref={provided.innerRef}
              {...provided.draggableProps}
              // {...provided.dragHandleProps}
              // style={{ backgroundColor: snapshot.isDragging ? "lightgreen" : "white" }}
            >
              <GridRow
                leftColumn={<TaskCard reorderHandleProps={provided.dragHandleProps} />}
                rightColumn={
                  <div className={clsx(classes.taskTimelinePath, "task-timeline-container")}>
                    <div>
                      {calendarDatesFlattened.map((k, i) => {
                        const dayOfWeek = k.momentObj.day();
                        const isWeekend = dayOfWeek === 0 || dayOfWeek === 6;

                        // using 'index' will cause re-rendering on resize
                        return (
                          <>
                            <div
                              key={i}
                              style={{
                                height: "100%",
                                position: "absolute",
                                top: 0,
                                left: 0,
                                width: dayBoxWidth,
                                transform: `translateX(${dayBoxWidth * i}px)`,
                                ...(isWeekend && { opacity: 0.5, backgroundColor: "lightgray" }),
                                // backgroundColor: isWeekend ? "lightgray" : "transparent",
                                // zIndex: '-1',
                              }}></div>
                          </>
                        );
                      })}
                    </div>
                    <div className={classes.timelineContainer}>
                      <TaskTimeline />
                    </div>
                  </div>
                }
                isTaskRow
              />
            </div>
          );
        }}
      </Draggable>
    </>
  );
};

TaskRow.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
  taskIndex: PropTypes.number,
};

TaskRow.defaultProps = {
  classes: {},
  theme: {},
};

const WithInjectedHOCs = compose(withRouter, withStyles(styles, { withTheme: true }))(TaskRow);

const Memoized = memo(WithInjectedHOCs);

export default Memoized;
