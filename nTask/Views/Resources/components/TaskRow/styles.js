const taskRowStyles = (theme) => ({
  // here objects of styles
  taskCard: {
    // no styles
    padding: 9,
  },
  taskTimelinePath: {
    position: "relative",
    height: "100%",
    minHeight: 50,
    padding: "0px 6px",
  },
  timelineContainer: {
    // display: "flex",
    display: "inline-block",
    position: "relative",
    top: "50%",
    transform: "translateY(-50%)",
    // background: "#00ABED",
    padding: "7px 0",
    boxSizing: "border-box",
    // borderRadius: 5,
    color: "#fff",
    // width: '100vh',
  },
});

export default taskRowStyles;
