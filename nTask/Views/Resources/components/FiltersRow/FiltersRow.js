import React, { useLayoutEffect } from "react";
import { Grid, SvgIcon, Typography } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import CustomButton from "../../../../components/Buttons/CustomButton";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import CalendarResource from "../../../../components/Icons/CalendarResource";
import { setInitialFiltersActionCreator } from "../../../../redux/actionCreators/resources";
import { ResourceWorkloadViewType } from "../../../../utils/constants/ResourceWorkloadViewType";
import { EffortsDropdown, ZoomDropdown } from "../Dropdowns";
import { SettingsDrawer, UnscheduledTasksDrawer } from "../Drawers";
import { Filters } from "../Filters";
// import Filters from "../Filters";
import { withStyles } from "@material-ui/core";
import styles from "./styles";
import PropTypes from "prop-types";

const FiltersRow = (props) => {
  const { classes } = props;
  const dispatch = useDispatch();

  const workloadViewType = useSelector((s) => s.resources.uiGrid.workloadViewType);

  useLayoutEffect(() => {
    dispatch(setInitialFiltersActionCreator());
  }, []);

  return (
    <>
      <div className={classes.filtersRow}>
        <Grid
          container
          direction="row"
          justify="space-between"
          alignItems="center"
          classes={{ container: classes.taskDashboardHeader }}>
          <div className="flex_center_start_row">
            <div className={classes.projectHeaderLeftCnt}>
              <Typography variant="h1" className={classes.listViewHeading}>
                {workloadViewType === ResourceWorkloadViewType.resources.value &&
                  "Resource Workloads"}
                {workloadViewType === ResourceWorkloadViewType.projects.value &&
                  "Project Workloads"}
              </Typography>
            </div>
          </div>
          <div className="flex_center_start_row">
            {/* week view dropdown */}
            <ZoomDropdown />

            {/* today button here */}
            {false && (
              <CustomButton
                // onClick={() => setOpen(!open)}
                className={classes.borderBtn}
                style={{
                  // padding: customFilterSelect ? "4px 8px 4px 4px" : "3px 8px 3px 4px",
                  padding: "9px 8px",
                  borderRadius: "4px",
                  display: "flex",
                  justifyContent: "space-between",
                  minWidth: "74px",
                  whiteSpace: "nowrap",
                  marginRight: 10,
                  height: 32,
                }}
                btnType={"white"}
                variant="contained">
                <SvgIcon viewBox="0 0 16 17.778">
                  <CalendarResource />
                </SvgIcon>
                <span className={classes.qckFilterLbl}>Today</span>
              </CustomButton>
            )}
            {/* Filter dropdown here */}
            <Filters />
            {/* effort type starts here */}
            <EffortsDropdown />
            {/* un-scheduled tasks starts here */}
            <UnscheduledTasksDrawer />
            {/* settings drawer starts here */}
            <SettingsDrawer />
          </div>
        </Grid>
      </div>
    </>
  );
};

FiltersRow.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
};

FiltersRow.defaultProps = {
  classes: {},
  theme: {},
};

export default compose(withRouter, withStyles(styles, { withTheme: true }))(FiltersRow);
