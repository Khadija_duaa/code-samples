const filtersRowStyles = (theme) => ({
  // style objects here
  filtersRow: {
    margin: "1rem 1rem 1rem 0",
  },
  borderBtn: {
    padding: "3px 8px 3px 4px",
    borderRadius: "4px",
    height: 32,
    marginRight: 8,
    fontFamily: "lato",
    color: "#171717",
    fontSize: "13px",
    "& svg": {
      fontSize: 18,
      marginRight: 5,
      color: "#7F8F9A",
    },
  },
  taskDashboardHeader: {},

  projectHeaderLeftCnt: {},

  listViewHeading: {
    fontSize: "20px",
    fontWeight: "bold",
    fontFamily: "'lato'",
    color: "#171717",
  },

  qckFilterLbl: {},
});

export default filtersRowStyles;
