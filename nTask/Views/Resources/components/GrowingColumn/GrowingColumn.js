// @flow

import React from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import styles from "./styles";
import PropTypes from "prop-types";

const GrowingColumn = (props) => {
  const { classes } = props;

  return (
    <>
      <div className={classes.rightColumn}>{props.children}</div>
    </>
  );
};

GrowingColumn.propTypes = {
  children: PropTypes.element.isRequired,
};

GrowingColumn.defaultProps = {
  classes: {},
  theme: {},
};

export default compose(withRouter, withStyles(styles, { withTheme: true }))(GrowingColumn);
