const growingColumnStyles = (theme) => ({
  rightColumn: {
    flexGrow: 1,
    overflow: "hidden",
  },
});

export default growingColumnStyles;
