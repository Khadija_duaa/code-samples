// @flow

import React, { useRef, memo } from "react";
import { useSelector } from "react-redux";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import styles from "./styles";
import clsx from "clsx";
import Collapsible from "../Collapsible";
import Typography from "@material-ui/core/Typography";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import GridRow from "../GridRow";
import ResourcesGrid from "../ResourcesGrid";
import { ProjectContext } from "../../contexts";
import PropTypes from "prop-types";

const ProjectRow = (props) => {
  const { classes, projectInfo } = props;

  const collapsed = useSelector((s) => s.resources.uiGrid.sidebarCollapsed);

  const rowCollapseToggleBtnRef = useRef(null);

  return (
    <>
      <Collapsible
        collapseBtnRef={rowCollapseToggleBtnRef}
        summaryRow={(isExpanded) => {
          return (
            <GridRow
              leftColumn={
                <div className={classes.rcCard} ref={rowCollapseToggleBtnRef}>
                  <div
                    className={clsx(
                      classes.toggleIndicator,
                      collapsed && classes.collapsed_sideBar
                    )}
                    style={collapsed ? { width: "0px", display: "none" } : {}}>
                    <ChevronRightIcon className={isExpanded ? "open" : ""} />
                  </div>
                  <Typography className={classes.projectName} variant="h5">
                    {projectInfo.title}
                  </Typography>
                </div>
              }
              rightColumn={
                <div className={classes.rcTimeline}>
                  <div className={classes.progressWrapper}>
                    {projectInfo.dailyWorkload.map((workload) => (
                      <div
                        className={clsx({
                          [classes.dayProgress]: true,
                          [classes.offDay]: workload.offDay,
                        })}>
                        {workload.offDay || !workload.load ? (
                          <></>
                        ) : (
                          <span>{workload.load?.toFixed()}h</span>
                        )}
                      </div>
                    ))}
                  </div>
                </div>
              }
            />
          );
        }}
        detailsPanel={(isExpanded) => {
          if (!isExpanded) {
            return <></>;
          }

          return (
            <ProjectContext.Provider value={projectInfo.id}>
              <ResourcesGrid />
            </ProjectContext.Provider>
          );
        }}
      />
    </>
  );
};

ProjectRow.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
  projectInfo: PropTypes.object,
};

ProjectRow.defaultProps = {
  classes: {},
  theme: {},
};

const WithInjectedHOCs = compose(withRouter, withStyles(styles, { withTheme: true }))(ProjectRow);

const Memoized = memo(WithInjectedHOCs);

export default Memoized;
