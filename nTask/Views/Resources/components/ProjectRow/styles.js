const resourceRowStyles = (theme) => ({
  rcCard: {
    cursor: "pointer",
    width: "250px",
    height: "98px",
    position: "relative",
    padding: "13px",
    display: "flex",
    maxHeight: "58px",
    gap: "5px",
    alignItems: 'center',
    "& p": {
      margin: 0,
      padding: 10,
    },
  },
  rcTimeline: {
    // no styles
    height: "100%",
  },
  progressWrapper: {
    display: "flex",
    height: "100%",
  },
  dayProgress: {
    width: "100%",
    // minWidth: 100,
    padding: 5,
    boxSizing: "border-box",
    borderLeft: "1px solid #DDDDDD",
    marginLeft: "-1px",

    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  offDay: {
    cursor: "auto",
    backgroundColor: "#F3F3F3",
  },
  projectCard: {},
  projectName: {
    color: "#171717",
    fontSize: "14px",
    fontFamily: "Lato, Regular",
    fontWeight: 400,
    lineHeight: 1.5,
    textTransform: 'capitalize'
  },
  toggleIndicator: {
    "& svg": {
      color: "#969696",
      position: "relative",
      top: "5px",
      transition: "0.4s all ease",
      fontSize: '22px'
    },
    "& svg.open": {
      transform: "rotate(90deg)",
    },
  },
  collapsed_sideBar: {
    opacity: 0,
    transition: "0.2s opacity ease",
    pointerEvents: "none",
  },

});

export default resourceRowStyles;
