const resourceCardStyles = (theme) => ({
  resourceName: {
    fontFamily: "Lato, Regular",
    fontSize: "14px",
    fontWeight: "400",
    color: "#171717",
    lineHeight: "1.5",
    wordBreak: "break-word",
  },
  resourceDesignation: {
    color: "#646464",
    fontSize: "13px",
    fontFamily: "Lato, Regular",
    lineHeight: "1.5",
    fontWeight: 400,
    wordBreak: "break-word",
  },
  resourceCapacity: {
    color: "#7E7E7E",
    fontSize: "13px",
    fontFamily: "Lato, Regular",
    lineHeight: "1.5",
    "& span": {
      color: "#171717",
    },
  },
  rscMediaParent: {
    display: "flex",
    gap: "10px",
    alignItems: "flex-start",
  },
  resourceUtilizationBar: {
    marginTop: 10,
  },
  workLoadProgress: {
    height: "5px",
    borderRadius: "50px",
  },
  toggleIndicator: {
    "& svg": {
      color: "#969696",
      position: "relative",
      top: "5px",
      transition: "0.4s all ease",
    },
    "& svg.open": {
      transform: "rotate(90deg)",
    },
  },
  resourcesDetailsParent: {
    flex: "1",
  },
  rscRightSide: {
    transition: "1.5s opacity ease",
    opacity: 1,
  },
  collapsed_sideBar: {
    opacity: 0,
    transition: "0.2s opacity ease",
    pointerEvents: "none",
  },
  barFilledColor: {
    backgroundColor: theme.palette.workload.userLinearProgressFilledColor,
    borderRadius: "50px",
  },
  barUnfilledColor: {
    backgroundColor: theme.palette.workload.userLinearProgressEmptyColor,
  },
  barOverloadColor: {
    backgroundColor: theme.palette.workload.overload,
  },
  capacityParent: {},
  alignCenter: {
    alignItems: "center",
  },
});

export default resourceCardStyles;
