// @flow

import React, { memo } from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import styles from "./styles";
import clsx from "clsx";
import CustomAvatar from "../../../../components/Avatar/Avatar";
import Typography from "@material-ui/core/Typography";
import LinearProgress from "@material-ui/core/LinearProgress";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import { useSelector } from "react-redux";
import {
  selectResourceTotalCapacityForSelectedTimelinePeriod,
  selectResourceTotalWorkloadForSelectedTimelinePeriod,
} from "./../../../../redux/selectors/resources";
import { ResourcesDrawer } from "../Drawers";
import { isValidUrl } from "../../temp-utils";
import PropTypes from "prop-types";

const ResourceCard = (props) => {
  const { classes, data, isExpanded } = props;

  const { id: resourceId, totalCapacity } = data;

  const collapsed = useSelector((s) => s.resources.uiGrid.sidebarCollapsed);

  const totalWorkloadForSelectedTimelinePeriod = useSelector((s) =>
    selectResourceTotalWorkloadForSelectedTimelinePeriod(s, { resourceId })
  );
  // const totalCapacityForSelectedTimelinePeriod = useSelector((s) =>
  //   selectResourceTotalCapacityForSelectedTimelinePeriod(s, { resourceId })
  // );

  const workloadPercentage = (
    (totalWorkloadForSelectedTimelinePeriod / totalCapacity) *
    100
  ).toFixed();

  const isOverload = workloadPercentage > 100;

  return (
    <>
      <div
        className={clsx(classes.toggleIndicator, collapsed && classes.collapsed_sideBar)}
        style={collapsed ? { width: "0px", display: "none" } : {}}>
        <ChevronRightIcon className={isExpanded ? "open" : ""} />
      </div>
      <div className={classes.resourcesDetailsParent}>
        <div className={clsx(classes.rscMediaParent)}>
          <div className={classes.rscLeftSide}>
            <CustomAvatar
              otherMember={{
                imageUrl: isValidUrl(data.imageUrl) ? data.imageUrl : "",
                fullName: data.name,
                lastName: "",
                email: data.email,
                isOnline: false,
                isOwner: false,
              }}
              size="small"
            />
          </div>
          <div className={clsx(classes.rscRightSide, collapsed && classes.collapsed_sideBar)}>
            <Typography className={classes.resourceName} variant="h5">
              {data.name || data.email}
            </Typography>

            {data.designation && (
              <Typography className={classes.resourceDesignation} variant="h6">
                {data.designation}
              </Typography>
            )}
            <div className={classes.capacityParent}>
              {/* <Button
                    onClick={(event) => {
                      // alert("outer btn");
                      console.log("event; outer btn click", cloneDeep(event));
                      event.stopPropagation();
                    }}>
                    <Typography className={classes.resourceCapacity} variant="p">
                      Capacity:&nbsp;
                      <span>
                        {totalWorkloadForSelectedTimelinePeriod}h /{" "}
                        {totalCapacityForSelectedTimelinePeriod}h
                      </span>
                    </Typography>
                  </Button> */}
              <ResourcesDrawer resource={data} />
            </div>
          </div>
        </div>

        <div
          className={clsx(classes.resourceUtilizationBar, collapsed && classes.collapsed_sideBar)}>
          <LinearProgress
            // color="primary"
            variant="determinate"
            classes={{
              barColorPrimary: isOverload ? classes.barOverloadColor : classes.barFilledColor,
            }}
            className={clsx(classes.workLoadProgress, classes.barUnfilledColor)}
            value={isOverload ? 100 : workloadPercentage}
          />
        </div>
      </div>
    </>
  );
};

ResourceCard.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
  data: PropTypes.object,
  isExpanded: PropTypes.bool,
};

ResourceCard.defaultProps = {
  classes: {},
  theme: {},
  data: {},
  isExpanded: false,
};

const WithInjectedHOCs = compose(withRouter, withStyles(styles, { withTheme: true }))(ResourceCard);

const Memoized = memo(WithInjectedHOCs);

export default Memoized;
