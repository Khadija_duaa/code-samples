const unassignedTasksRowCardStyles = (theme) => ({
  resourceName: {
    fontFamily: "Lato, Regular",
    fontSize: "14px",
    fontWeight: "400",
    color: "#171717",
    lineHeight: "1.5",
  },
  rscMediaParent: {
    display: "flex",
    gap: "10px",
    alignItems: "flex-start",
  },
  toggleIndicator: {
    "& svg": {
      color: "#969696",
      position: "relative",
      top: "5px",
      transition: "0.4s all ease",
    },
    "& svg.open": {
      transform: "rotate(90deg)",
    },
  },
  resourcesDetailsParent: {
    flex: "1",
  },
  rscRightSide: {
    transition: "1.5s opacity ease",
    opacity: 1,
  },
  collapsed_sideBar: {
    opacity: 0,
    transition: "0.2s opacity ease",
    pointerEvents: "none",
  },
  alignCenter: {
    alignItems: "center",
  },
  sidebarCollapsed: {
    width: 0,
    display: "none",
  },
  sidebarExpanded: {},
});

export default unassignedTasksRowCardStyles;
