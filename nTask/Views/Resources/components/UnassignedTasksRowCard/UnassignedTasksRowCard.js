// @flow

import React, { memo } from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import styles from "./styles";
import clsx from "clsx";
import CustomAvatar from "../../../../components/Avatar/Avatar";
import Typography from "@material-ui/core/Typography";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import { useSelector } from "react-redux";
import PropTypes from "prop-types";

const UnassignedTasksRowCard = (props) => {
  const { classes, isExpanded } = props;

  const sidebarCollapsed = useSelector((s) => s.resources.uiGrid.sidebarCollapsed);
  const dataExists = useSelector((s) => s.resources.dataFlags.unassignedTasksDataListExists);
  const unassignedTasksDataCount = useSelector(
    (s) => s.resources.data.unassignedTasksDataList.length
  );

  return (
    <>
      <div
        className={clsx(
          classes.toggleIndicator,
          sidebarCollapsed && classes.collapsed_sideBar,
          sidebarCollapsed ? classes.sidebarCollapsed : classes.sidebarExpanded
        )}>
        <ChevronRightIcon className={isExpanded ? "open" : ""} />
      </div>
      <div className={classes.resourcesDetailsParent}>
        <div className={clsx(classes.rscMediaParent, classes.alignCenter)}>
          <div className={classes.rscLeftSide}>
            <CustomAvatar
              otherMember={{
                fullName: "?",
                lastName: "",
                email: "",
                isOnline: false,
                isOwner: false,
              }}
              size="small"
              disableCard
              styles={{ backgroundColor: "#EAEAEA", borderRadius: "50%", color: "gray" }}
            />
          </div>
          <div
            className={clsx(classes.rscRightSide, sidebarCollapsed && classes.collapsed_sideBar)}>
            <Typography className={classes.resourceName} variant="h5">
              Unassigned
              {/* if dataExists, then show, otherwise we'll miss the '0' records even after API call returns 0 results */}
              {dataExists && <span>&nbsp;({unassignedTasksDataCount} Tasks)</span>}
            </Typography>
          </div>
        </div>
      </div>
    </>
  );
};

UnassignedTasksRowCard.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
  isExpanded: PropTypes.bool,
};

UnassignedTasksRowCard.defaultProps = {
  classes: {},
  theme: {},
  isExpanded: false,
};

const WithInjectedHOCs = compose(
  withRouter,
  withStyles(styles, { withTheme: true })
)(UnassignedTasksRowCard);

const Memoized = memo(WithInjectedHOCs);

export default Memoized;
