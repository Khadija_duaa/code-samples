// @flow

import React, { useState, useEffect, memo } from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import styles from "./styles";
import clsx from "clsx";
import { Droppable } from "react-beautiful-dnd";
import TasksList from "../TasksList";
import { useSelector } from "react-redux";
import LoadingRecordsRow from "../LoadingRecordsRow";
import { useDispatch } from "react-redux";
import { fetchUnassignedTasksDataListThunk } from "../../../../redux/thunks/resources";
import { withSnackbarNotifs } from "./../../../../hoc";
import PropTypes from "prop-types";

const UnassignedTasks = (props) => {
  const { classes, showSnackbar } = props;

  const dispatch = useDispatch();

  const [tasksList, setTasksList] = useState([]);
  const [loading, setLoading] = useState(false);

  const dataExists = useSelector((s) => s.resources.dataFlags.unassignedTasksDataListExists);
  const unassignedTasksDataList = useSelector((s) => s.resources.data.unassignedTasksDataList);
  const timelinePeriod = useSelector((s) => s.resources.filters.calendarTimelinePeriod);

  useEffect(() => {
    setLoading(true);
    setTasksList(unassignedTasksDataList);
    setLoading(false);
  }, [unassignedTasksDataList]);

  const apiCallFailure = (error) =>
    showSnackbar(error.data?.message || ((error.status || error.statusText) && `${error.status}: ${error.statusText}`) || "Backend service seems offline!", "error");

  const apiCallFinally = () => setLoading(false);

  const fetchDataFromApi = () => {
    setLoading(true);
    dispatch(
      fetchUnassignedTasksDataListThunk({
        failureCallback: apiCallFailure,
        finallyCallback: apiCallFinally,
      })
    );
  };

  useEffect(() => {
    if (!dataExists) {
      fetchDataFromApi();
    }
  }, [timelinePeriod, dataExists]);

  if (loading) {
    return <LoadingRecordsRow />;
  }

  return (
    <>
      <Droppable droppableId={"droppable-tasks-unassigned"}>
        {(provided, snapshot) => {
          // console.info("drop-provided:", provided);

          return (
            <div
              className={clsx("unassigned-tasks-container", classes.resourceTasksContainer)}
              ref={provided.innerRef}
              {...provided.droppableProps}
              style={{
                backgroundColor: snapshot.isDraggingOver ? "skyblue" : "white",
              }}>
              <TasksList tasks={tasksList} isDraggingOver={snapshot.isDraggingOver} />
              {provided.placeholder}
            </div>
          );
        }}
      </Droppable>
    </>
  );
};

UnassignedTasks.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
  showSnackbar: PropTypes.func,
};

UnassignedTasks.defaultProps = {
  classes: {},
  theme: {},
};

const WithInjectedHOCs = compose(
  withRouter,
  withSnackbarNotifs,
  withStyles(styles, { withTheme: true })
)(UnassignedTasks);

const Memoized = memo(WithInjectedHOCs);

export default Memoized;
