import TasksList from "./TasksList";

export { default as TasksList } from "./TasksList";
export * from "./TasksList";

export default TasksList;
