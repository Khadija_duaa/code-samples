// @flow

import React, { memo } from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import styles from "./styles";
import TaskRow from "../TaskRow";
import { useSelector } from "react-redux";
import NoRecordsRow from "../NoRecordsRow";
import PropTypes from "prop-types";
import { TaskContext } from "../../contexts/resources";

const TasksList = (props) => {
  const { tasks, isDraggingOver = false } = props;

  const gridColumnsCount = useSelector((s) => s.resources.uiGrid.gridColumnsCount);

  if (!gridColumnsCount || gridColumnsCount.length <= 0) {
    return <></>;
  }

  if (!tasks || tasks.length <= 0) {
    return isDraggingOver ? <></> : <NoRecordsRow />;
  }

  return tasks.map((task, inx) => {
    // TODO: remove the line below to show all tasks of a resource
    // if (inx > 0) {
    //   return;
    // }

    return (
      <TaskContext.Provider value={task.id}>
        <TaskRow key={task.id} taskIndex={inx} />
      </TaskContext.Provider>
    );
  });
};

TasksList.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
  tasks: PropTypes.array,
  isDraggingOver: PropTypes.bool,
};

TasksList.defaultProps = {
  classes: {},
  theme: {},
  tasks: [],
  isDraggingOver: true,
};

const WithInjectedHOCs = compose(withRouter, withStyles(styles, { withTheme: true }))(TasksList);

const Memoized = memo(WithInjectedHOCs);

export default Memoized;
