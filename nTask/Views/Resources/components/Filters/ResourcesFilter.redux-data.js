import React, { memo, useEffect, useState } from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./styles";
import { withRouter } from "react-router-dom";
import DefaultCheckbox from "../../../../components/Form/Checkbox";
import CustomAvatar from "../../../../components/Avatar/Avatar";
import SearchInput from "react-search-input";
import { useDispatch, useSelector } from "react-redux";
import { selectTeamResourcesList } from "../../../../redux/selectors/resources";
import { setFilterResourcesAction } from "../../../../redux/actions/resources";
import PropTypes from "prop-types";

const ResourcesFilter = (props) => {
  const { classes, theme } = props;

  const dispatch = useDispatch();

  const teamResourcesList = useSelector(selectTeamResourcesList);
  const selectedResources = useSelector((s) => s.resources.filters.resources);

  const [hideSelectAll, setHideSelectAll] = useState(false);
  const [selectedList, setSelectedList] = useState(initializeSelectedList);
  const [visibleList, setVisibleList] = useState([]);

  useEffect(() => {
    // TODO: here select current resources
  }, []);

  useEffect(() => {
    setVisibleList(teamResourcesList);

    teamResourcesList.forEach((k) => {
      if (!selectedList.has(k.id)) {
        selectedList.set(k.id, false);
      }
    });
  }, [teamResourcesList]);

  // NOTE: this goes for infinite re-renders and crashes
  // useEffect(() => {
  //   const map = new Map();
  //   selectedWorkspaces.forEach((k) => map.set(k, true));
  //   setSelectedList(map);
  // }, [selectedWorkspaces]);

  useEffect(() => {
    const selectedIds = Array.from(selectedList.entries())
      .filter(([_, v]) => !!v)
      .map(([k]) => k);

    dispatch(setFilterResourcesAction(selectedIds));
  }, [selectedList]);

  function initializeSelectedList() {
    const map = new Map();

    selectedResources.forEach((k) => map.set(k, true));

    return map;
  }

  const handleCheckboxChange = (id, checked) =>
    setSelectedList((map) => new Map(map.set(id, checked)));

  const onSearch = (term) => {
    setHideSelectAll(term?.length > 0);

    if (!term || term.length <= 0) {
      setVisibleList(teamResourcesList);
      return;
    }

    const termLower = term.toLowerCase();
    const filteredList = teamResourcesList.filter((k) =>
      k.name?.toLowerCase()?.includes(termLower)
    );

    setVisibleList(filteredList);
  };

  return (
    <>
      <div className={classes.tabContent}>
        <div className={classes.dropdownSearchCnt}>
          <SearchInput
            className="HtmlInput"
            maxLength="80"
            onChange={onSearch}
            placeholder={"Search by name, email or job title"}
          />
        </div>
        {!hideSelectAll && (
          <label className={classes.checkBoxWrapper} key={0}>
            <DefaultCheckbox
              checked={
                Array(...selectedList.values()).every((k) => !!k)
                // selectedList.size === 1 ? false : Array(...selectedList.values()).every((k) => !!k)
              }
              onChange={(_, checked) => {
                const map = new Map();
                selectedList.forEach((_, k) => map.set(k, checked));
                setSelectedList(map);
              }}
              checkboxStyles={{ padding: "0 8px 0 0" }}
              color={theme.palette.primary.main}
            />
            <span>Select All</span>
          </label>
        )}
        {visibleList.map((k, i) => (
          <label className={classes.checkBoxWrapper} key={i + 1}>
            <DefaultCheckbox
              checked={selectedList.has(k.id) && selectedList.get(k.id)}
              onChange={(_, checked) => handleCheckboxChange(k.id, checked)}
              checkboxStyles={{ padding: "0 8px 0 0" }}
              color={theme.palette.primary.main}
            />
            <CustomAvatar
              otherMember={{
                imageUrl: k.imageUrl,
                fullName: k.fullName,
                lastName: "",
                email: k.email,
                isOnline: k.isOnline,
                isOwner: k.isOwner,
              }}
              size="small"
            />
            <span style={{ marginLeft: 8 }}>{k.fullName || k.email}</span>
          </label>
        ))}
      </div>
    </>
  );
};

ResourcesFilter.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
};

ResourcesFilter.defaultProps = { classes: {}, theme: {} };

const WithInjectedHOCs = compose(
  withRouter,
  withStyles(styles, { withTheme: true })
)(ResourcesFilter);
const Memoized = memo(WithInjectedHOCs);

export default Memoized;
