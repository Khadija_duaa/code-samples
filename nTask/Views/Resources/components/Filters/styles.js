const collapsibleStyles = (theme) => ({
  tab: {
    minHeight: "unset",
    fontSize: 14,
    minWidth: 20,
    whiteSpace: "nowrap",
    textTransform: "none",
    marginRight: "1rem !important",
  },
  tabLabel: {
    color: theme.palette.text.primary,
    padding: "6px !important",
    fontFamily: "Lato",
    fontWeight: "normal",
    fontSize: "15px",
  },
  tabContent: {
    padding: "5px 1rem 1rem",
  },
  tabs: {
    "& > div": {
      padding: "0 1rem",
      borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    },
  },
  count: {
    fontSize: 12,
    fontWeight: 400,
    borderRadius: 36,
    margin: "0 2px 0 4px",
    background: theme.palette.background.brightBlue,
    color: theme.palette.common.white,
    width: "1.4em",
    height: "1.4em",
    display: "inline-flex",
    justifyContent: "center",
    alignItems: "center",
  },
  checkBoxWrapper: {
    padding: "6px 0",
    fontSize: 13,
    display: "flex",
    alignItems: "center",

    "& span": {
      cursor: "pointer",
      userSelect: "none",
      "--webkit-user-select": "none",
      "--moz-user-select": "none",
      "--ms-user-select": "none",
    },
  },

  borderBtn: {
    padding: "3px 8px 3px 4px",
    borderRadius: "4px",
    height: 32,
    fontFamily: "lato",
    color: "#171717",
    fontSize: "13px",
    marginRight: 8,
    minWidth: "77px",
    "& svg": {
      fontSize: 18,
      color: "#7F8F9A",
    },
  },
  applyBtn: {
    padding: "1rem",
    display: "flex",
    justifyContent: "end",
  },
  emptyProjects: {
    padding: "20px 0px",
    textAlign: "center",
    textTransform: "Capitalize",
    fontFamily: "Lato",
    fontWeight: "normal",
    fontSize: "16px",
  },
  filterLoader: {
    padding: "20px 0px",
    display: "flex",
    justifyContent: "center",
  },
  dropdownSearchCnt: {
    marginBottom: "10px",
  },
  tabBody: {
    // maxHeight: "200px",
    // overflowY: "auto",
  },
});

export default collapsibleStyles;
