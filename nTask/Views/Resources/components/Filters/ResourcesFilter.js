import React, { memo, useEffect, useState } from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./styles";
import { withRouter } from "react-router-dom";
import DefaultCheckbox from "../../../../components/Form/Checkbox";
import CustomAvatar from "../../../../components/Avatar/Avatar";
import Scrollbars from "react-custom-scrollbars";
import SearchInput, { createFilter } from "react-search-input";
import { useDispatch, useSelector } from "react-redux";
import { selectResourcesOfWorkspaces } from "../../../../redux/selectors/resources";
import { setFilterResourcesAction } from "../../../../redux/actions/resources";
import { fetchFilterResourcesOfWorkspacesDataListThunk } from "../../../../redux/thunks/resources";
import { CircularProgress } from "@material-ui/core";
import { isValidUrl } from "../../temp-utils";
import { withSnackbarNotifs } from "../../../../hoc";
import PropTypes from "prop-types";

const ResourcesFilter = (props) => {
  const { classes, theme, showSnackbar } = props;

  const dispatch = useDispatch();

  const resourcesOfWorkspacesList = useSelector(selectResourcesOfWorkspaces);
  const selectedResources = useSelector((s) => s.resources.filters.resources);
  const selectedWorkspaces = useSelector((s) => s.resources.filters.workspaces);

  const [hideSelectAll, setHideSelectAll] = useState(false);
  const [selectedList, setSelectedList] = useState(initializeSelectedList);
  const [visibleList, setVisibleList] = useState([]);
  const [accumulatedList, setAccumulativeList] = useState([]);
  const [loading, setLoading] = useState(false);

  const apiCallFailure = (error) =>
    showSnackbar(error.data?.message || ((error.status || error.statusText) && `${error.status}: ${error.statusText}`) || "Backend service seems offline!", "error");

  useEffect(() => {
    // TODO: here select current resources

    const keysArr = Array.from(resourcesOfWorkspacesList.keys());
    // const alreadyFetchedAll = selectedWorkspaces.every(keysArr.includes);
    const alreadyFetchedAll = selectedWorkspaces.every((k) => keysArr.includes(k));

    if (resourcesOfWorkspacesList.size <= 0 || !alreadyFetchedAll) {
      setLoading(true);

      dispatch(
        fetchFilterResourcesOfWorkspacesDataListThunk({
          finallyCallback: () => setLoading(false),
          failureCallback: apiCallFailure,
        })
      );
    }
  }, []);

  useEffect(() => {
    if (resourcesOfWorkspacesList.size <= 0) {
      return;
    }
    const list = Array.from(resourcesOfWorkspacesList.values());
    // accumulated reducer to combine sub-arrays
    const accumulatedReducer = (acc, cur) => [...acc, ...cur];
    const accumulated = list.reduce(accumulatedReducer, []);
    const sorted = accumulated.sort((a, b) => a.name - b.name);

    setAccumulativeList(sorted);
  }, [resourcesOfWorkspacesList]);

  useEffect(() => {
    setVisibleList(accumulatedList);

    accumulatedList.forEach((k) => {
      if (!selectedList.has(k.id)) {
        selectedList.set(k.id, false);
      }
    });

    setSelectedList(new Map(selectedList));
    setHideSelectAll(accumulatedList.length <= 0);
  }, [accumulatedList]);

  useEffect(() => {
    const selectedIds = Array.from(selectedList.entries())
      .filter(([_, v]) => !!v)
      .map(([k]) => k);

    dispatch(setFilterResourcesAction(selectedIds));
  }, [selectedList]);

  function initializeSelectedList() {
    const map = new Map();

    selectedResources.forEach((k) => map.set(k, true));

    return map;
  }

  const handleCheckboxChange = (id, checked) =>
    setSelectedList((map) => new Map(map.set(id, checked)));

  const onSearch = (term) => {
    setHideSelectAll(term?.length > 0);

    if (!term || term.length <= 0) {
      setVisibleList(accumulatedList);
      return;
    }

    const termLower = term.toLowerCase();

    const filteredList = accumulatedList.filter(
      (k) =>
        k.fullName?.toLowerCase()?.includes(termLower) ||
        k.email?.toLowerCase()?.includes(termLower) ||
        k.designation?.toLowerCase()?.includes(termLower)
    );

    setVisibleList(filteredList);
  };

  if (loading) {
    return (
      <>
        <div className={classes.filterLoader}>
          <CircularProgress className={classes.progress} />
        </div>
      </>
    );
  }

  return (
    <>
      <div className={classes.tabContent}>
        <div className={classes.dropdownSearchCnt}>
          <SearchInput
            className="HtmlInput"
            maxLength="80"
            onChange={onSearch}
            placeholder={"Search by name, email or job title"}
          />
        </div>
        <Scrollbars
          autoHide={false}
          autoHeight
          autoHeightMin={100}
          autoHeightMax={180}
          className="scrollCon">
          <div className={classes.tabBody}>
            {!hideSelectAll && (
              <label className={classes.checkBoxWrapper} key={0}>
                <DefaultCheckbox
                  checked={Array(...selectedList.values()).every((k) => !!k)}
                  onChange={(_, checked) => {
                    const map = new Map();
                    selectedList.forEach((_, k) => map.set(k, checked));
                    setSelectedList(map);
                  }}
                  checkboxStyles={{ padding: "0 8px 0 0" }}
                  color={theme.palette.primary.main}
                />
                <span>Select All</span>
              </label>
            )}

            {!loading && visibleList.length === 0 && (
              <>
                <div className={classes.emptyProjects}>No resources</div>
              </>
            )}

            {!loading &&
              visibleList.map((k, i) => (
                <label className={classes.checkBoxWrapper} key={i + 1}>
                  <DefaultCheckbox
                    checked={selectedList.has(k.id) && selectedList.get(k.id)}
                    onChange={(_, checked) => handleCheckboxChange(k.id, checked)}
                    checkboxStyles={{ padding: "0 8px 0 0" }}
                    color={theme.palette.primary.main}
                  />
                  <CustomAvatar
                    otherMember={{
                      imageUrl: isValidUrl(k.imageUrl) ? k.imageUrl : "",
                      fullName: k.fullName,
                      email: k.email,
                    }}
                    size="small"
                  />
                  <span style={{ marginLeft: 8 }}>{k.fullName || k.email}</span>
                </label>
              ))}
          </div>
        </Scrollbars>
      </div>
    </>
  );
};

ResourcesFilter.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
  showSnackbar: PropTypes.func,
};

ResourcesFilter.defaultProps = { classes: {}, theme: {} };

const WithInjectedHOCs = compose(
  withRouter,
  withSnackbarNotifs,
  withStyles(styles, { withTheme: true })
)(ResourcesFilter);
const Memoized = memo(WithInjectedHOCs);

export default Memoized;
