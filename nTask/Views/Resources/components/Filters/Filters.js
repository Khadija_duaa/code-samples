import React, { memo, useEffect, useRef, useState } from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./styles";
import CustomButton from "../../../../components/Buttons/CustomButton";
import { withRouter } from "react-router-dom";
import QuickFilterIcon from "../../../../components/Icons/QuickFilterIcon";
import DropdownMenu from "../../../../components/Dropdown/DropdownMenu";
import { Divider, Tab, Tabs, SvgIcon } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { ResourceManagerFiltersTabs } from "../../../../utils/constants/ResourceManagerFiltersTabs";
// import WorkspacesFilter from "./WorkspacesFilter";
// import ResourcesFilter from "./ResourcesFilter";
// import ProjectsFilter from "./ProjectsFilter";
import { ProjectsFilter, ResourcesFilter, WorkspacesFilter } from "./index";
import { setFilterWorkspacesAction } from "../../../../redux/actions/resources";
import { ResourceWorkloadViewType } from "./../../../../utils/constants/ResourceWorkloadViewType";
import {
  resetGridDataActionCreator,
  resetGridTasksDataActionCreator,
} from "../../../../redux/actionCreators/resources";
import { fetchGridResourcesDataListThunk } from "../../../../redux/thunks/resources";
import { PAGE_SIZE } from "../ResourcesGrid";
import PropTypes from 'prop-types';

const Filters = (props) => {
  const { classes, theme } = props;

  const dispatch = useDispatch();

  const workloadViewType = useSelector((s) => s.resources.uiGrid.workloadViewType);
  const currentWorkspaceId = useSelector((s) => s.profile.data.loggedInTeam);

  const [open, setOpen] = useState(false);
  const [tab, setTab] = useState(ResourceManagerFiltersTabs.workspaces.value);

  const anchorEl = useRef(null);

  useEffect(() => {
    dispatch(setFilterWorkspacesAction([currentWorkspaceId]));
  }, []);

  const handleClick = (event) => {
    event.stopPropagation();

    setOpen(!open);
  };

  const handleClose = () => setOpen(false);

  const handleChange = (event, newValue) => {
    setTab(newValue);
  };

  const renderTabContentMarkup = () => {
    switch (tab) {
      case ResourceManagerFiltersTabs.workspaces.value:
        return <WorkspacesFilter />;

      case ResourceManagerFiltersTabs.projects.value:
        return <ProjectsFilter />;

      case ResourceManagerFiltersTabs.resources.value:
        return <ResourcesFilter />;

      default:
        return <div className={classes.tabContent}>No content</div>;
    }
  };

  const onApplyFilters = () => {
    // filters applied here

    // here call the resources/projects API thunk

    dispatch(resetGridDataActionCreator());
    dispatch(resetGridTasksDataActionCreator());

    if (workloadViewType === ResourceWorkloadViewType.resources.value) {
      // reset resources list
      dispatch(
        fetchGridResourcesDataListThunk({
          pageNo: 1,
          pageSize: PAGE_SIZE,
          // finallyCallback: apiCallFinally,
        })
      );

      // reset resource tasks list
    }

    handleClose();
  };

  return (
    <>
      <CustomButton
        onClick={handleClick}
        className={classes.borderBtn}
        buttonRef={(node) => {
          anchorEl.current = node;
        }}
        btnType={"white"}
        variant="contained">
        <SvgIcon viewBox="0 0 24 24" className={classes.qckFilterIconSvg}>
          <QuickFilterIcon
            nativeColor={theme.palette.secondary.light}
            classes={{ root: classes.selectHighlightItemIcon }}
          />
        </SvgIcon>
        <span className={classes.qckFilterLbl}>Filters</span>
      </CustomButton>

      <DropdownMenu
        // style={{ width: 700 }}
        open={open}
        closeAction={handleClose}
        anchorEl={anchorEl.current}
        size="xxlarge"
        placement="bottom-start">
        {/* <Scrollbars autoHide autoHeightMax={250} autoHeight={false} style={{ height: 250 }}> */}
        <div
          style={
            {
              // position: "absolute",
              // right: "0",
              // marginTop: 5,
              // background: theme.palette.common.white,
              // boxShadow:
              //   "0px 1px 5px 0px rgb(0 0 0 / 20%), 0px 2px 2px 0px rgb(0 0 0 / 14%), 0px 3px 1px -2px rgb(0 0 0 / 12%)",
              // borderRadius: "4px",
              // width: 530
            }
          }>
          <Tabs
            value={tab}
            indicatorColor="primary"
            textColor="primary"
            onChange={handleChange}
            className={classes.tabs}>
            {Object.values(ResourceManagerFiltersTabs).map((k) => (
              <Tab
                className={classes.tab}
                classes={{ labelContainer: classes.tabLabel }}
                value={k.value}
                label={k.uiText}
              />
            ))}
          </Tabs>
          {renderTabContentMarkup()}

          <Divider variant="middle" />

          <div className={classes.applyBtn}>
            <CustomButton
              btnType="success"
              variant="contained"
              style={{ borderRadius: 6, padding: "4px 11px" }}
              onClick={onApplyFilters}>
              <span>Apply Filters</span>
            </CustomButton>
          </div>
        </div>

        {/* </Scrollbars> */}
      </DropdownMenu>
    </>
  );
};

Filters.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
};

Filters.defaultProps = {
  classes: {},
  theme: {},
};

const WithInjectedHOCs = compose(withRouter, withStyles(styles, { withTheme: true }))(Filters);
const Memoized = memo(WithInjectedHOCs);

export default Memoized;
