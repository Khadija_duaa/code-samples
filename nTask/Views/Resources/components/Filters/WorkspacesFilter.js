import React, { memo, useEffect, useRef, useState } from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./styles";
import { withRouter } from "react-router-dom";
import DefaultCheckbox from "../../../../components/Form/Checkbox";
import Scrollbars from "react-custom-scrollbars";
import SearchInput from "react-search-input";
import { useDispatch, useSelector } from "react-redux";
import { selectTeamWorkspacesList } from "../../../../redux/selectors/resources";
import { setFilterWorkspacesAction } from "../../../../redux/actions/resources";
import PropTypes from "prop-types";

const WorkspacesFilter = (props) => {
  const { classes, theme } = props;

  const dispatch = useDispatch();

  const teamWorkspacesList = useSelector(selectTeamWorkspacesList);
  const selectedWorkspaces = useSelector((s) => s.resources.filters.workspaces);

  const [hideSelectAll, setHideSelectAll] = useState(false);
  const [selectedList, setSelectedList] = useState(initializeSelectedList);
  const [visibleList, setVisibleList] = useState([]);

  useEffect(() => {
    // TODO: here select current workspace
  }, []);

  useEffect(() => {
    setVisibleList(teamWorkspacesList);

    teamWorkspacesList.forEach((k) => {
      if (!selectedList.has(k.id)) {
        selectedList.set(k.id, false);
      }
    });
    setSelectedList(new Map(selectedList));
  }, [teamWorkspacesList]);

  useEffect(() => {
    const selectedIds = Array.from(selectedList.entries())
      .filter(([_, v]) => !!v)
      .map(([k]) => k);

    dispatch(setFilterWorkspacesAction(selectedIds));
  }, [selectedList]);

  function initializeSelectedList() {
    const map = new Map();

    selectedWorkspaces.forEach((k) => map.set(k, true));

    return map;
  }

  const handleCheckboxChange = (id, checked) =>
    setSelectedList((map) => new Map(map.set(id, checked)));

  const onSearch = (term) => {
    setHideSelectAll(term?.length > 0);

    if (!term || term.length <= 0) {
      setVisibleList(teamWorkspacesList);
      return;
    }

    const termLower = term.toLowerCase();
    const filteredList = teamWorkspacesList.filter((k) =>
      k.name?.toLowerCase()?.includes(termLower)
    );

    setVisibleList(filteredList);
  };

  return (
    <>
      <div className={classes.tabContent}>
        <div className={classes.dropdownSearchCnt}>
          <SearchInput
            className="HtmlInput"
            maxLength="80"
            onChange={onSearch}
            placeholder={"Search by workspace name"}
          />
        </div>
        <Scrollbars
          autoHide={false}
          autoHeight
          autoHeightMin={100}
          autoHeightMax={180}
          className="scrollCon">
          <div className={classes.tabBody}>
            {!hideSelectAll && (
              <label className={classes.checkBoxWrapper} key={0}>
                <DefaultCheckbox
                  checked={
                    // Array(...selectedWorkspaces.values()).reduce((acc, cur)=> acc && cur, true)
                    Array(...selectedList.values()).every((k) => !!k)
                    // selectedList.size === 1 ? false : Array(...selectedList.values()).every((k) => !!k)
                  }
                  onChange={(_, checked) => {
                    const map = new Map();
                    selectedList.forEach((_, k) => map.set(k, checked));
                    setSelectedList(map);
                  }}
                  checkboxStyles={{ padding: "0 8px 0 0" }}
                  color={theme.palette.primary.main}
                />
                <span>Select All</span>
              </label>
            )}

            {visibleList.map((k, i) => (
              <label className={classes.checkBoxWrapper} key={i + 1}>
                <DefaultCheckbox
                  checked={selectedList.has(k.id) && selectedList.get(k.id)}
                  onChange={(_, checked) => handleCheckboxChange(k.id, checked)}
                  checkboxStyles={{ padding: "0 8px 0 0" }}
                  color={theme.palette.primary.main}
                />
                <span>{k.name}</span>
              </label>
            ))}
          </div>
        </Scrollbars>
      </div>
    </>
  );
};

WorkspacesFilter.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
};

WorkspacesFilter.defaultProps = { classes: {}, theme: {} };

const WithInjectedHOCs = compose(
  withRouter,
  withStyles(styles, { withTheme: true })
)(WorkspacesFilter);
const Memoized = memo(WithInjectedHOCs);

export default Memoized;
