// @flow

import React, { useState, useEffect } from "react";
import DefaultTextField from "../../../../components/Form/TextField";
import { useSelector } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import weeklyCapacityStyles from "./style";
import Hotkeys from "react-hot-keys";
import { validateWeeklyCapacity } from "../../../../utils/validator/common/weeklyCapacity";
import { validateDailyWorkloadCapacity } from "../../../../utils/validator/common/dailyWorkloadCapacity";
import { CalendarTimelinePeriods } from "../../../../utils/constants/CalendarTimelinePeriods";
import clsx from "clsx";
import PropTypes from "prop-types";

type DropdownProps = {
  classes: Object,
  theme: Object,
  label: String,
  onInputChange: Function,
  actualCapacity: String,
  onInputBlur: Function,
  placeholder: String,
  permission: Boolean,
};

//Onboarding Main Component
function TaskTimelineDayWorkloadInput(props: DropdownProps) {
  const {
    classes,
    theme,
    label,
    onInputChange,
    onInputBlur,
    actualCapacity,
    permission,
    boxWidth,
    onEdit,
    textStyles,
  } = props;

  const [capacity, setCapacity] = useState(actualCapacity);
  const [editable, setEditable] = useState(false);

  useEffect(() => {
    setCapacity(actualCapacity);
  }, [actualCapacity]);

  const calendarTimelinePeriod = useSelector((s) => s.resources.filters.calendarTimelinePeriod);

  const isMonthView = calendarTimelinePeriod === CalendarTimelinePeriods.month.value;
  const monthBoxWidth = boxWidth - 10;

  const handleInput = (event) => {
    event.preventDefault();
    event.stopPropagation();

    // Function handles input
    let inputValue = event.target.value;
    if (validateDailyWorkloadCapacity(inputValue).validated) {
      // Function handles input

      // let value = inputValue ? parseFloat(inputValue) : "";
      let value = inputValue ?? "";

      setCapacity(value); // set capacity value
      onInputChange(value); // input change in parent
    }
  };

  const handleCapacityClick = (event) => {
    event.preventDefault();
    event.stopPropagation();

    //Edit capacity of a resource
    setEditable(true);

    onEdit?.(event);
  };

  const handleInputBlur = (event) => {
    event.preventDefault();
    event.stopPropagation();

    //Handle blur funtion of input
    if (capacity == "") setCapacity(0); // set capacity value 0 if emty string
    setEditable(false);
    onInputBlur(capacity);
  };

  const onKeyDown = (keyName, event, handle) => {
    event.preventDefault();
    event.stopPropagation();

    if (keyName == "enter") {
      onInputChange(capacity);
      onInputBlur(capacity);
      setEditable(false);
      if (capacity == "") setCapacity(0); // set capacity value 0 if emty string
    }
  };

  return (
    <div style={{ position: "relative", width: isMonthView ? monthBoxWidth : 70, height: 28 }}>
      <span
        onClick={!editable && permission ? handleCapacityClick : null}
        className={editable ? classes.cpacityValueMiniCnt : classes.cpacityValueCnt}
        style={isMonthView ? { minWidth: monthBoxWidth, width: monthBoxWidth } : {}}>
        {!editable ? (
          <span className={classes.capacityValue} style={textStyles}>
            {capacity}
          </span>
        ) : null}
        <span className={classes.hoursLabel} style={textStyles}>
          {editable && isMonthView ? null : "h"}
        </span>
      </span>

      {editable ? (
        <Hotkeys keyName="enter" onKeyDown={onKeyDown}>
          <DefaultTextField
            label={label}
            size="small"
            error={false}
            formControlStyles={{
              marginBottom: 0,
              maxWidth: isMonthView ? monthBoxWidth : 70,
              width: isMonthView ? monthBoxWidth : 70,
            }}
            customInputClass={{ notchedOutline: isMonthView ? classes.monthViewFieldset : null }}
            defaultProps={{
              id: "dailyTaskLoad",
              onChange: handleInput,
              autoFocus: true,
              onBlur: handleInputBlur,
              value: capacity,
              inputProps: {
                style: {
                  fontFamily: theme.typography.fontFamilyLato,
                  fontWeight: 500,
                  fontSize: 13,
                },
              },
            }}
          />
        </Hotkeys>
      ) : null}
    </div>
  );
}

TaskTimelineDayWorkloadInput.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
  label: PropTypes.string,
  onInputChange: PropTypes.func,
  onInputBlur: PropTypes.func,
  actualCapacity: PropTypes.oneOfType(PropTypes.string, PropTypes.number),
  permission: PropTypes.bool,
  boxWidth: PropTypes.number,
  onEdit: PropTypes.func,
  textStyles: PropTypes.object,
};

TaskTimelineDayWorkloadInput.defaultProps = {
  classes: {},
  theme: {},
  label: "",
  onInputChange: () => {},
  actualCapacity: 40,
  onInputBlur: () => {},
  permission: false,
  onEdit: () => {},
  textStyles: {},
};

export default withStyles(weeklyCapacityStyles, { withTheme: true })(TaskTimelineDayWorkloadInput);
