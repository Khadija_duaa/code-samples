const weeklyCapacityStyles = (theme) => ({
  hoursLabel: {
    fontSize: 13,
    fontWeight: 400,
    color: "#FFF",
    fontFamily: theme.typography.fontFamilyLato,
  },
  placeholder: {
    fontSize: 13,
    fontWeight: 400,
    color: theme.palette.text.light,
    fontFamily: theme.typography.fontFamilyLato,
  },
  cpacityValueCnt: {
    display: "inline-block",
    color: theme.palette.text.primary,
    fontSize: 12,
    fontFamily: theme.typography.fontFamilyLato,
    borderRadius: 4,
    position: "absolute",
    right: 0,
    // zIndex: 111,
    textAlign: "left",
    cursor: "pointer",
    "&:hover": {
      background: theme.palette.background.items,
    },
    "&:hover *": {
      color: "#000",
    },
    padding: 7,
    minWidth: 70,
  },
  cpacityValueMiniCnt: {
    display: "inline-block",
    color: theme.palette.text.primary,
    fontSize: 12,
    transition: "ease all 0.2s",
    fontFamily: theme.typography.fontFamilyLato,
    borderRadius: 4,
    position: "absolute",
    right: 0,
    zIndex: 111,
    textAlign: "left",
    padding: 7,
    minWidth: 20,
  },
  capacityValue: {
    fontSize: 13,
    fontWeight: 500,
    color: "#fff",
    fontFamily: theme.typography.fontFamilyLato,
    marginRight: 2,
  },
  monthViewFieldset: { paddingLeft: "0 !important" },
});

export default weeklyCapacityStyles;
