import TaskTimelineDayWorkloadInput from "./TaskTimelineDayWorkloadInput";

export { default as TaskTimelineDayWorkloadInput } from "./TaskTimelineDayWorkloadInput";
export * from "./TaskTimelineDayWorkloadInput";

export default TaskTimelineDayWorkloadInput;
