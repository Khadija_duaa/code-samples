import NoRecordsRow from "./NoRecordsRow";

export { default as NoRecordsRow } from "./NoRecordsRow";
export * from "./NoRecordsRow";

export default NoRecordsRow;
