const noRecordsRowStyles = (theme) => ({
  // style objects here
  noRecords: {
    height: "58px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    borderBottom: "1px solid #DDDDDD",
    fontFamily: "'lato'",
    fontSize: "14px",
  },
});

export default noRecordsRowStyles;
