// @flow

import React from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import styles from "./styles";
import EmptyRow from "../EmptyRow";
import PropTypes from "prop-types";

const NoRecordsRow = (props) => {
  const { classes, children } = props;

  return (
    <EmptyRow>
      <div className={classes.noRecords}>{children || <>No records</>}</div>
    </EmptyRow>
  );
};

NoRecordsRow.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
  children: PropTypes.element,
};

NoRecordsRow.defaultProps = {
  classes: {},
  theme: {},
};

export default compose(withRouter, withStyles(styles, { withTheme: true }))(NoRecordsRow);
