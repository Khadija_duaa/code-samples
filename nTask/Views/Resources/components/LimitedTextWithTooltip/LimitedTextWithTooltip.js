// @flow

import React, { memo } from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import styles from "./styles";
import PropTypes from "prop-types";
import ConditionalTooltip from "../ConditionalTooltip";

const LimitedTextWithTooltip = (props) => {
  const { classes, children, text, limit, lineLimit } = props;

  const styles = {
    WebkitLineClamp: lineLimit,
  };

  return (
    <>
      <ConditionalTooltip title={text} placement="top" condition={text?.split(" ").length > limit}>
        <div style={styles} className={classes.lineLimit}>
          {children}
        </div>
      </ConditionalTooltip>
    </>
  );
};

LimitedTextWithTooltip.propTypes = {
  children: PropTypes.element,
  text: PropTypes.string,
  limit: PropTypes.number,
  lineLimit: PropTypes.number,
};

LimitedTextWithTooltip.defaultProps = {
  text: null,
  limit: 10,
  lineLimit: 1,
};

const WithInjectedHOCs = compose(
  withRouter,
  withStyles(styles, { withTheme: true })
)(LimitedTextWithTooltip);

const Memoized = memo(WithInjectedHOCs);

export default Memoized;
