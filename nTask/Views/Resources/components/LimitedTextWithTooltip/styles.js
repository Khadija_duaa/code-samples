const styles = (theme) => ({
  // here objects of styles
  lineLimit: {
    display: "-webkit-box",
    WebkitBoxOrient: "vertical",
    overflow: "hidden",
    textOverflow: "ellipsis",
    wordBreak: "break-word",
  },
});

export default styles;
