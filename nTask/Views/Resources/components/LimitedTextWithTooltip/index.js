import LimitedTextWithTooltip from "./LimitedTextWithTooltip";

export { default as LimitedTextWithTooltip } from "./LimitedTextWithTooltip";
export * from "./LimitedTextWithTooltip";

export default LimitedTextWithTooltip;
