import React, { memo, useCallback, useContext, useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import DragHandleIcon from "@material-ui/icons/DragHandle";
import styles from "./styles";
import { Resizable } from "react-resizable";
import classNames from "classnames";
import { cloneDeep, debounce } from "lodash";
import { parseISODateString } from "../../../../helper/dates/dates";
import { formatMomentToDate } from "./../../temp-utils";
import ErrorBoundary from "../ErrorBoundary";
import moment from "moment";
import { ResourceContext, UnassignedTasksContext } from "../../contexts";
import {
  updateResourceTaskPlannedDatesAction,
  updateUnassignedTaskPlannedDatesAction,
  updateUnassignedTaskWorkloadsAction,
} from "../../../../redux/actions/resources";
import { updateResourceOrUnassignedTaskPlannedDatesThunk } from "../../../../redux/thunks/resources";
import { updateResourceTaskWorkloadsAction } from "./../../../../redux/actions/resources";
import { dayWorkloadObjectTemplate } from "../../api-helpers";
import { Rnd } from "react-rnd";
import PropTypes from "prop-types";
import { selectTaskData } from "../../../../redux/selectors/resources";
import { TaskContext } from "../../contexts/resources";

const USE_REACT_RESIZABLE = true;
const USE_REACT_RND = false;

const USE_DISCRETE_EXTEND_RIGHT = true;
const USE_DISCRETE_EXTEND_LEFT = true;
const USE_DISCRETE_SHRINK_RIGHT = true;
const USE_DISCRETE_SHRINK_LEFT = true;

const USE_SMOOTHE_EXTEND_RIGHT = false;
const USE_SMOOTHE_EXTEND_LEFT = false;
const USE_SMOOTHE_SHRINK_RIGHT = false;
const USE_SMOOTHE_SHRINK_LEFT = false;

const TaskTimelineResizable = (props) => {
  const {
    classes,
    theme,
    children,

    minTimelineWidth,
    startsBeforeTimespan,
    endsAfterTimespan,
    setTimelineFixedWidth,
    setNewDayWidth,
    boxTemplate,
    setTimelineDragPosition,
    timelineDragPositionBackup,
    // dayBoxWidth,
    setDailyWorkloadSortedByDayIndex,
    dailyWorkloadSortedByDayIndexBackup: oldArr,
    setDailyWorkloadSortedByDayIndexBackup: setOldArr,
    timelineFixedWidth,
    dailyWorkloadSortedByDayIndex,
    timelineDragPosition,
    timelineInitialPosition,
    getBoxWidth,
    // taskId,
    // plannedStartDate,
    // plannedEndDate,
    taskDailyWorkload,
  } = props;

  // console.info("rendered: TaskTimelineResizable");

  const dispatch = useDispatch();

  const { isUnassignedTask = false } = useContext(UnassignedTasksContext) || {};
  const resourceId = useContext(ResourceContext);
  const taskId = useContext(TaskContext);

  const dayBoxWidth = useSelector((s) => s.resources.uiGrid.dayProgressHourglassWidth);
  const taskData = useSelector((s) =>
    selectTaskData(s, {
      isUnassignedTask,
      resourceId,
      taskId,
    })
  );

  let { plannedStartDate, plannedEndDate } = taskData || {};

  // let timelineWidth = minTimelineWidth;
  const [timelineWidth, setTimelineWidth] = useState(
    0
    // dailyWorkload.length * dayBoxWidth
    // dailyWorkloadSortedByDayIndex.length * dayBoxWidth
  );

  const [sortedWorkloads, setSortedWorkloads] = useState();
  const [isExtending, setIsExtending] = useState();
  const [isRightDirection, setIsRightDirection] = useState();

  const minTimelineHeight = 42;
  const oldBoxesWidthPlusWithNewBoxesAdded = oldArr.reduce((acc, cur) => acc + cur.boxWidth, 0);

  useEffect(() => {
    // setTimelineWidth(
    //   // .slice(
    //   //   0,
    //   //   endsBeforeTimespan || startsAfterTimespan
    //   //     ? 0
    //   //     : endDateIndex < 0
    //   //     ? daysList[selectedCalendarTimeline] - startInx
    //   //     : endDateIndex + 1
    //   // )
    //   dailyWorkloadSortedByDayIndex.length * dayBoxWidth // adding 1 to 'endDateIndex' to include it
    // );
    setTimelineWidth(minTimelineWidth);
  }, []);

  useEffect(() => {
    // setTimelineWidth(
    //   // .slice(
    //   //   0,
    //   //   endsBeforeTimespan || startsAfterTimespan
    //   //     ? 0
    //   //     : endDateIndex < 0
    //   //     ? daysList[selectedCalendarTimeline] - startInx
    //   //     : endDateIndex + 1
    //   // )
    //   dailyWorkloadSortedByDayIndex.length * dayBoxWidth // adding 1 to 'endDateIndex' to include it
    // );
    setTimelineWidth(dailyWorkloadSortedByDayIndex.reduce((acc, cur) => acc + cur.boxWidth, 0));
  }, [
    dailyWorkloadSortedByDayIndex,
    // selectedCalendarTimeline,
    // endDateIndex,
    // startInx,
    // dayBoxWidth,
    // startsAfterTimespan,
    // endsBeforeTimespan,
  ]);

  useEffect(() => {
    setSortedWorkloads(dailyWorkloadSortedByDayIndex);
  }, [dailyWorkloadSortedByDayIndex]);

  const updateBackend = (newStartDate, newEndDate) => {
    // return;

    dispatch(
      updateResourceOrUnassignedTaskPlannedDatesThunk({
        taskId,
        startDate: newStartDate.toISOString(),
        endDate: newEndDate.toISOString(),
      })
    );
  };

  // let debouncedApiCall;
  const debouncedApiCall = useCallback(
    debounce(updateBackend, 0, {
      // maxWait: 2000,
    }),
    []
  );

  const updateState = ({ rightExtendDays = 0, leftExtendDays = 0 }) => {
    let plannedStartDateParsed = parseISODateString(plannedStartDate);
    let plannedEndDateParsed = parseISODateString(plannedEndDate);

    let newStartDate = plannedStartDateParsed.clone();
    let newEndDate = plannedEndDateParsed.clone();

    const newWorkloads = cloneDeep(taskDailyWorkload);

    // console.info('kkkk--11:', formatMomentToDate(newStartDate), cloneDeep(newStartDate));

    if (rightExtendDays > 0) {
      // newEndDate = newEndDate.clone().add(rightExtendDays, "days").clone();
      newEndDate.add(rightExtendDays, "days");

      const workloadsToAdd = Array(rightExtendDays)
        .fill(0)
        .map((_, i) => {
          const currentMoment = plannedEndDateParsed.clone().add(i + 1, "day");

          return {
            ...dayWorkloadObjectTemplate,

            date: formatMomentToDate(currentMoment),
            load: 0,
            moment: currentMoment,
          };
        });

      newWorkloads.push(...workloadsToAdd);
    }

    if (leftExtendDays > 0) {
      // newStartDate = newStartDate.clone().add(-1 * leftExtendDays, "days").clone();
      newStartDate.add(-1 * leftExtendDays, "days");

      const workloadsToAdd = Array(leftExtendDays)
        .fill(0)
        .map((_, i, selfArr) => {
          const currentMoment = plannedStartDateParsed.clone().add(i - selfArr.length, "day");

          return {
            ...dayWorkloadObjectTemplate,

            date: formatMomentToDate(currentMoment),
            load: 0,
            moment: currentMoment,
          };
        });

      newWorkloads.unshift(...workloadsToAdd);
    }

    // console.info('kkkk--22:', formatMomentToDate(newStartDate), cloneDeep(newStartDate));

    if (isUnassignedTask) {
      dispatch(updateUnassignedTaskPlannedDatesAction(taskId, newStartDate, newEndDate));
      dispatch(updateUnassignedTaskWorkloadsAction(taskId, newWorkloads));
    } else {
      dispatch(updateResourceTaskPlannedDatesAction(resourceId, taskId, newStartDate, newEndDate));
      dispatch(updateResourceTaskWorkloadsAction(resourceId, taskId, newWorkloads));
    }

    if (rightExtendDays <= 0 && leftExtendDays <= 0) {
      return;
    }
    return;
    debouncedApiCall?.(newStartDate, newEndDate);
  };

  const ffRef = useRef(0);

  const handleOnResizeRightExtend = (width) => {
    const clonedArr = cloneDeep(dailyWorkloadSortedByDayIndex);

    // // const oldArr = clonedArr.filter((k) => !k.isNewBox);
    // const isLastNew = clonedArr.at(-1)?.isNewLastBox;

    // if (!isLastNew) {
    //   debugger;

    //   const boxesToAddNew = 1;
    //   // const boxesToAddNew = Math.ceil((width - boxesMaxWidth) / dayBoxWidth);
    //   const newBoxes = Array(boxesToAddNew)
    //     // 'undefined' value set to hide any value while dragging/resizing
    //     .fill(undefined)
    //     .map((k, i) => ({
    //       ...boxTemplate,
    //       dayIndex: oldArr.length,
    //       moment: null,
    //       effort: k,
    //       offDay: false,
    //       isNewLastBox: boxesToAddNew - 1 === i,
    //       isNewBox: true,
    //       boxWidth: dayBoxWidth,
    //     }));

    //   clonedArr.splice(0, oldArr.length, ...oldArr);

    //   // TODO: here decide how many days to add, may be required two or more days, if diff width is 400px, (double of day box size)
    //   clonedArr.push(...newBoxes);
    //   console.info("eee-eee - new box added at the end:", clonedArr);

    //   setDailyWorkloadSortedByDayIndex(clonedArr);
    // }
    // debugger;
    // const boxesMaxWidth = clonedArr.reduce((acc, cur) => acc + cur.boxWdith, 0);
    const oldBoxesWidth = oldArr.reduce((acc, cur) => acc + cur.boxWidth, 0);

    const isSame = oldBoxesWidth === minTimelineWidth;

    const lastBoxWidth = width - oldBoxesWidth;
    // we need to set the width increased or decreased of new box
    // setNewDayWidth(width - boxesMaxWidth);
    // setNewDayWidth(lastBoxWidth);

    const lastBox = clonedArr.at(-1);

    console.info("eee-eee - last box width -- 1:", lastBoxWidth, cloneDeep(lastBox));

    if (!lastBox || !lastBox.isNewLastBox) {
      return;
    }

    // debugger;
    lastBox.boxWidth = lastBoxWidth;
    // lastBox.boxWidth = dayBoxWidth + lastBoxWidth;

    console.info("eee-eee - last box width -- 2:", lastBoxWidth, cloneDeep(lastBox));

    clonedArr.splice(-1, 1, lastBox);

    setDailyWorkloadSortedByDayIndex(clonedArr);
  };

  const handleOnResizeLeftExtend = (width) => {
    const clonedArr = cloneDeep(dailyWorkloadSortedByDayIndex);

    // const boxesMaxWidth = clonedArr.reduce((acc, cur) => acc + cur.boxWdith, 0);
    const oldBoxesWidth = oldArr.reduce((acc, cur) => acc + cur.boxWidth, 0);

    const isSame = oldBoxesWidth === minTimelineWidth;

    const firstBoxWidth = Math.ceil(width - oldBoxesWidth);

    const firstBox = clonedArr.at(0);

    if (!firstBox || !firstBox.isNewFirstBox || firstBoxWidth < 0) {
      return;
    }

    // debugger;
    firstBox.boxWidth = firstBoxWidth;

    clonedArr.splice(0, 1, firstBox);

    // debugger;
    // setTimelineDragPosition(timelineDragPositionBackup - firstBoxWidth);
    // setTimelineDragPosition(tlDragPosition => {
    //   const sss = tlDragPosition - firstBoxWidth;
    //   // debugger;
    //   console.info('tlDragPosition:', tlDragPosition,firstBoxWidth, sss);
    //   return sss;
    // });
    // console.info('boxes--111:', firstBoxWidth);

    const firstNewBoxes = [];

    for (let bx of clonedArr) {
      if (bx.isNewBox) {
        firstNewBoxes.push(bx);
      } else {
        break;
      }
    }

    const firstNewBoxesWidth = firstNewBoxes.reduce((acc, curr) => acc + curr.boxWidth, 0);

    const newDragPosition = timelineDragPositionBackup - firstNewBoxesWidth;

    // console.info('boxes--222:', firstNewBoxes, firstNewBoxesWidth, timelineDragPositionBackup, newDragPosition);

    setTimelineDragPosition(newDragPosition);
    // setTimelineDragPosition(timelineInitialPosition - lastBoxWidth);
    // setTimelineDragPosition(timelineDragPosition - lastBoxWidth);
    // setTimelineDragPosition(() => timelineDragPosition - lastBoxWidth);
    // setTimelineDragPosition((w) => w - lastBoxWidth);

    setDailyWorkloadSortedByDayIndex(clonedArr);
  };

  const handleOnResizeRightShrink = (width) => {
    // ONE

    // const clonedArr = cloneDeep(dailyWorkloadSortedByDayIndex);

    //     const changeInWidth = width - minTimelineWidth;

    //     const lastBox = clonedArr.at(-1);
    //     // debugger;
    //     if (changeInWidth > dayBoxWidth) {
    //       return;
    //     }

    //     lastBox.boxWidth = dayBoxWidth - changeInWidth;

    //     clonedArr.splice(-1, 1, lastBox);

    //     setDailyWorkloadSortedByDayIndex(clonedArr);

    // TWO

    // console.info('right side shrink:',  minTimelineWidth, width);
    // debugger;
    const clonedArr = cloneDeep(dailyWorkloadSortedByDayIndex);

    // debugger;
    // const boxesMaxWidth = clonedArr.reduce((acc, cur) => acc + cur.boxWdith, 0);
    const oldBoxesWidth = parseInt(oldArr.reduce((acc, cur) => acc + cur.boxWidth, 0));

    const lastBoxWidth = oldBoxesWidth - width;

    const lastBox = clonedArr.at(-1);

    console.info("eee-eee - last box width -- 1:", lastBoxWidth, oldBoxesWidth, cloneDeep(lastBox));

    if (!lastBox) {
      return;
    }

    // debugger;
    lastBox.boxWidth = dayBoxWidth - lastBoxWidth;
    // lastBox.boxWidth = dayBoxWidth + lastBoxWidth;

    console.info(
      "eee-eee - last box width -- 2:",
      lastBoxWidth,
      dayBoxWidth - lastBoxWidth,
      cloneDeep(lastBox)
    );

    clonedArr.splice(-1, 1, lastBox);

    setDailyWorkloadSortedByDayIndex(clonedArr);
  };

  const handleOnResizeLeftShrink = (width) => {};

  const onResize = (event, resizeData) => {
    event.stopPropagation();

    const {
      handle,
      size: { width, height },
    } = resizeData;
    // console.info("eee-eee onResize:", resizeData);
    console.info("eee-eee onResize:", handle, width);

    const isRightSideResize = handle === "e";
    const isLeftSideResize = handle === "w";

    const isExtending = width > minTimelineWidth;
    const isShrinking = width < minTimelineWidth;

    // return;

    // resize with right handle
    if (isRightSideResize) {
      // debugger;
      // setTimelineWidth(width);

      // width is increased
      if (isExtending) {
        handleOnResizeRightExtend(width);
      }
      // width is decreased
      else if (isShrinking) {
        handleOnResizeRightShrink(width);
      }

      // setTimelineWidth(width);
    }
    // resize with right handle
    else if (isLeftSideResize) {
      // debugger;
      // setTimelineWidth(width);

      // width is increased
      if (isExtending) {
        handleOnResizeLeftExtend(width);
      }
      // width is decreased
      else if (isShrinking) {
        handleOnResizeLeftShrink(width);
      }
      // setTimelineWidth(width);
    }
  };

  // backup of 'onResize' function
  // const onResize = (event, resizeData) => {
  //   event.stopPropagation();

  //   const {
  //     handle,
  //     size: { width, height },
  //   } = resizeData;
  //   // console.info("eee-eee onResize:", resizeData);
  //   console.info("eee-eee onResize:", handle, width, minTimelineWidth, width - minTimelineWidth);

  //   const isRightSideResize = handle === "e";
  //   const isLeftSideResize = handle === "w";

  //   // return;
  //   // setTimelineWidth(width);
  //   // debugger;
  //   // const clonedArr = cloneDeep(dailyWorkloadSortedByDayIndex);
  //   // const oldBoxesWidth = oldArr.reduce((acc, cur) => acc + cur.boxWidth, 0);

  //   // const lastBoxWidth = width - oldBoxesWidth;
  //   // // we need to set the width increased or decreased of new box
  //   // // setNewDayWidth(width - boxesMaxWidth);
  //   // // setNewDayWidth(lastBoxWidth);

  //   // const lastBox = clonedArr.at(-1);

  //   // if (!lastBox || !lastBox.isNewLastBox) {
  //   //   return;
  //   // }

  //   // lastBox.boxWidth = lastBoxWidth;

  //   // clonedArr.splice(-1, 1, lastBox);

  //   // setDailyWorkloadSortedByDayIndex(clonedArr);
  //   // return;

  //   // resize with right handle
  //   if (isRightSideResize) {
  //     // debugger;
  //     // setTimelineWidth(width);

  //     // width is increased than minTimelineWidth
  //     if (width > minTimelineWidth) {
  //       const clonedArr = cloneDeep(dailyWorkloadSortedByDayIndex);

  //       // // const oldArr = clonedArr.filter((k) => !k.isNewBox);
  //       // const isLastNew = clonedArr.at(-1)?.isNewLastBox;

  //       // if (!isLastNew) {
  //       //   debugger;

  //       //   const boxesToAddNew = 1;
  //       //   // const boxesToAddNew = Math.ceil((width - boxesMaxWidth) / dayBoxWidth);
  //       //   const newBoxes = Array(boxesToAddNew)
  //       //     // 'undefined' value set to hide any value while dragging/resizing
  //       //     .fill(undefined)
  //       //     .map((k, i) => ({
  //       //       ...boxTemplate,
  //       //       dayIndex: oldArr.length,
  //       //       moment: null,
  //       //       effort: k,
  //       //       offDay: false,
  //       //       isNewLastBox: boxesToAddNew - 1 === i,
  //       //       isNewBox: true,
  //       //       boxWidth: dayBoxWidth,
  //       //     }));

  //       //   clonedArr.splice(0, oldArr.length, ...oldArr);

  //       //   // TODO: here decide how many days to add, may be required two or more days, if diff width is 400px, (double of day box size)
  //       //   clonedArr.push(...newBoxes);
  //       //   console.info("eee-eee - new box added at the end:", clonedArr);

  //       //   setDailyWorkloadSortedByDayIndex(clonedArr);
  //       // }
  //       // debugger;
  //       // const boxesMaxWidth = clonedArr.reduce((acc, cur) => acc + cur.boxWdith, 0);
  //       const oldBoxesWidth = oldArr.reduce((acc, cur) => acc + cur.boxWidth, 0);

  //       const isSame = oldBoxesWidth === minTimelineWidth;

  //       const lastBoxWidth = width - oldBoxesWidth;
  //       // we need to set the width increased or decreased of new box
  //       // setNewDayWidth(width - boxesMaxWidth);
  //       // setNewDayWidth(lastBoxWidth);

  //       const lastBox = clonedArr.at(-1);

  //       console.info("eee-eee - last box width -- 1:", lastBoxWidth, cloneDeep(lastBox));

  //       if (!lastBox || !lastBox.isNewLastBox) {
  //         return;
  //       }

  //       // debugger;
  //       lastBox.boxWidth = lastBoxWidth;
  //       // lastBox.boxWidth = dayBoxWidth + lastBoxWidth;

  //       console.info("eee-eee - last box width -- 2:", lastBoxWidth, cloneDeep(lastBox));

  //       clonedArr.splice(-1, 1, lastBox);

  //       setDailyWorkloadSortedByDayIndex(clonedArr);
  //     } else if (width < minTimelineWidth) {
  //       const clonedArr = cloneDeep(dailyWorkloadSortedByDayIndex);

  //       const changeInWidth = width - minTimelineWidth;

  //       const lastBox = clonedArr.at(-1);
  //       // debugger;
  //       if(changeInWidth > dayBoxWidth) {
  //         return;
  //       }

  //       lastBox.boxWidth = dayBoxWidth - changeInWidth;

  //       clonedArr.splice(-1, 1, lastBox);

  //       setDailyWorkloadSortedByDayIndex(clonedArr);

  //     }
  //     // width is decreased than minTimelineWidth
  //     else if (width < minTimelineWidth) {
  //       // console.info('right side shrink:',  minTimelineWidth, width);
  //       // debugger;
  //       const clonedArr = cloneDeep(dailyWorkloadSortedByDayIndex);

  //       // debugger;
  //       // const boxesMaxWidth = clonedArr.reduce((acc, cur) => acc + cur.boxWdith, 0);
  //       const oldBoxesWidth = parseInt(oldArr.reduce((acc, cur) => acc + cur.boxWidth, 0));

  //       const lastBoxWidth = oldBoxesWidth - width;

  //       const lastBox = clonedArr.at(-1);

  //       console.info(
  //         "eee-eee - last box width -- 1:",
  //         lastBoxWidth,
  //         oldBoxesWidth,
  //         cloneDeep(lastBox)
  //       );

  //       if (!lastBox) {
  //         return;
  //       }

  //       // debugger;
  //       lastBox.boxWidth = dayBoxWidth - lastBoxWidth;
  //       // lastBox.boxWidth = dayBoxWidth + lastBoxWidth;

  //       console.info(
  //         "eee-eee - last box width -- 2:",
  //         lastBoxWidth,
  //         dayBoxWidth - lastBoxWidth,
  //         cloneDeep(lastBox)
  //       );

  //       clonedArr.splice(-1, 1, lastBox);

  //       setDailyWorkloadSortedByDayIndex(clonedArr);
  //     }
  //     // width is decreased than minTimelineWidth
  //     else if (width < minTimelineWidth) {
  //       // console.info('right side shrink:',  minTimelineWidth, width);
  //       // debugger;
  //       const clonedArr = cloneDeep(dailyWorkloadSortedByDayIndex);

  //       // debugger;
  //       // const boxesMaxWidth = clonedArr.reduce((acc, cur) => acc + cur.boxWdith, 0);
  //       const oldBoxesWidth = parseInt(oldArr.reduce((acc, cur) => acc + cur.boxWidth, 0));

  //       const lastBoxWidth = oldBoxesWidth - width;

  //       const lastBox = clonedArr.at(-1);

  //       console.info(
  //         "eee-eee - last box width -- 1:",
  //         lastBoxWidth,
  //         oldBoxesWidth,
  //         cloneDeep(lastBox)
  //       );

  //       if (!lastBox) {
  //         return;
  //       }

  //       // debugger;
  //       lastBox.boxWidth = dayBoxWidth - lastBoxWidth;
  //       // lastBox.boxWidth = dayBoxWidth + lastBoxWidth;

  //       console.info(
  //         "eee-eee - last box width -- 2:",
  //         lastBoxWidth,
  //         dayBoxWidth - lastBoxWidth,
  //         cloneDeep(lastBox)
  //       );

  //       clonedArr.splice(-1, 1, lastBox);

  //       setDailyWorkloadSortedByDayIndex(clonedArr);
  //     }
  //     // setTimelineWidth(width);
  //   } else if (isLeftSideResize) {
  //     // debugger;
  //     // setTimelineWidth(width);

  //     // width is increased than minTimelineWidth
  //     if (width > minTimelineWidth) {
  //       const clonedArr = cloneDeep(dailyWorkloadSortedByDayIndex);

  //       // const boxesMaxWidth = clonedArr.reduce((acc, cur) => acc + cur.boxWdith, 0);
  //       const oldBoxesWidth = oldArr.reduce((acc, cur) => acc + cur.boxWidth, 0);

  //       const isSame = oldBoxesWidth === minTimelineWidth;

  //       const firstBoxWidth = Math.ceil(width - oldBoxesWidth);

  //       const firstBox = clonedArr.at(0);

  //       if (!firstBox || !firstBox.isNewFirstBox || firstBoxWidth < 0) {
  //         return;
  //       }

  //       // debugger;
  //       firstBox.boxWidth = firstBoxWidth;

  //       clonedArr.splice(0, 1, firstBox);

  //       // debugger;
  //       // setTimelineDragPosition(timelineDragPositionBackup - firstBoxWidth);
  //       // setTimelineDragPosition(tlDragPosition => {
  //       //   const sss = tlDragPosition - firstBoxWidth;
  //       //   // debugger;
  //       //   console.info('tlDragPosition:', tlDragPosition,firstBoxWidth, sss);
  //       //   return sss;
  //       // });
  //       // console.info('boxes--111:', firstBoxWidth);

  //       const firstNewBoxes = [];

  //       for (let bx of clonedArr) {
  //         if (bx.isNewBox) {
  //           firstNewBoxes.push(bx);
  //         } else {
  //           break;
  //         }
  //       }

  //       const firstNewBoxesWidth = firstNewBoxes.reduce((acc, curr) => acc + curr.boxWidth, 0);

  //       const newDragPosition = timelineDragPositionBackup - firstNewBoxesWidth;

  //       // console.info('boxes--222:', firstNewBoxes, firstNewBoxesWidth, timelineDragPositionBackup, newDragPosition);

  //       setTimelineDragPosition(newDragPosition);
  //       // setTimelineDragPosition(timelineInitialPosition - lastBoxWidth);
  //       // setTimelineDragPosition(timelineDragPosition - lastBoxWidth);
  //       // setTimelineDragPosition(() => timelineDragPosition - lastBoxWidth);
  //       // setTimelineDragPosition((w) => w - lastBoxWidth);

  //       setDailyWorkloadSortedByDayIndex(clonedArr);
  //     }
  //     // setTimelineWidth(width);

  //     // setTimelineWidth(width);
  //     // const clonedArr = cloneDeep(
  //     //   dailyWorkloadSortedByDayIndex
  //     //   // .slice(
  //     //   //   0,
  //     //   //   endsBeforeTimespan || startsAfterTimespan
  //     //   //     ? 0
  //     //   //     : endDateIndex < 0
  //     //   //     ? daysList[selectedCalendarTimeline] - startInx
  //     //   //     : endDateIndex + 1
  //     //   // )
  //     // );
  //     // // debugger;

  //     // const boxesMaxWidth = clonedArr.length * dayBoxWidth;

  //     // width is increased than minTimelineWidth
  //     // if (width > minTimelineWidth) {
  //     //   const isLastNew = clonedArr.at(0)?.isNewLastBox;

  //     //   if (!isLastNew) {
  //     //     const boxesToAddNew = Math.ceil((width - boxesMaxWidth) / dayBoxWidth);

  //     //     const newBoxes = Array(boxesToAddNew)
  //     //       // 'undefined' value set to hide any value while dragging/resizing
  //     //       .fill(undefined)
  //     //       .map((k, i) => ({
  //     //         dayIndex: i,
  //     //         moment: null,
  //     //         effort: k,
  //     //         offDay: false,
  //     //         isNewLastBox: i === 0,
  //     //         isNewBox: true,
  //     //       }));

  //     //     // TODO: here decide how many days to add, may be required two or more days, if diff width is 400px, (double of day box size)
  //     //     clonedArr.unshift(...newBoxes);
  //     //     const updatedClonedArr = clonedArr.map((k, i) => ({
  //     //       ...k,
  //     //       dayIndex: i,
  //     //       isNewLastBox: i === 0,
  //     //     }));
  //     //     clonedArr.splice(0, clonedArr.length, ...updatedClonedArr);

  //     //     console.info("eee-eee - new box added at the first:", clonedArr);

  //     //     setDailyWorkloadSortedByDayIndex(clonedArr);
  //     //   }

  //     //   const lastBoxWidth = width - (clonedArr.length - 1) * dayBoxWidth;
  //     //   // we need to set the width increased or decreased of new box
  //     //   // setNewDayWidth(width - boxesMaxWidth);
  //     //   setNewDayWidth(lastBoxWidth);
  //     //   console.info(
  //     //     "eee-eee timelineDragPosition XXXXX",
  //     //     timelineInitialPosition,
  //     //     timelineDragPosition,
  //     //     lastBoxWidth,
  //     //     timelineDragPosition - lastBoxWidth,
  //     //     timelineInitialPosition - lastBoxWidth
  //     //   );
  //     //   setTimelineDragPosition(timelineDragPositionBackup - lastBoxWidth);
  //     //   // setTimelineDragPosition(timelineInitialPosition - lastBoxWidth);
  //     //   // setTimelineDragPosition(timelineDragPosition - lastBoxWidth);
  //     //   // setTimelineDragPosition(() => timelineDragPosition - lastBoxWidth);
  //     //   // setTimelineDragPosition((w) => w - lastBoxWidth);
  //     // }
  //   }
  // };

  const handleOnResizeStartRightExtend = (width) => {
    // const lastBoxWidth = width - oldBoxesWidth;
    const lastBoxWidth = width - oldBoxesWidthPlusWithNewBoxesAdded;

    const clonedArr = cloneDeep(dailyWorkloadSortedByDayIndex);

    const isLastNew = clonedArr.at(-1)?.isNewLastBox;
    if (!isLastNew) {
      // note: adding a new box in the right-end so user can drag and increase timeline width
      // TODO: handle the decrease side, we need to resize the last timeline day box instead of new
      // dailyWorkloadSortedByDayIndex.push(8);

      // clonedArr.at(-1).boxWidth = getBoxWidth(clonedArr.length - 1, clonedArr.length);
      clonedArr.at(-1).boxWidth = dayBoxWidth; // TODO: commenting this line

      // TODO: here decide how many days to add, may be required two or more days, if diff width is 400px, (double of day box size)
      clonedArr.push({
        ...boxTemplate,
        dayIndex: clonedArr.length,
        effort: 0,
        offDay: false,
        isNewLastBox: true,
        isNewBox: true,
        // boxWidth: getBoxWidth(clonedArr.length, clonedArr.length + 1),
        boxWidth: lastBoxWidth,
      });

      console.info("eee-eee - new box added at the end:", clonedArr);
    } else {
      clonedArr.at(-1).boxWidth = lastBoxWidth;

      // // dailyWorkloadSortedByDayIndex.push(8);
      // const clonedArr = cloneDeep(dailyWorkloadSortedByDayIndex);

      // // TODO: here decide how many days to add, may be required two or more days, if diff width is 400px, (double of day box size)
      // clonedArr.push({ dayIndex: clonedArr.length, effort: 0, offDay: false, isNewBox: true });

      // console.info("eee-eee - new box added at the end:", clonedArr);

      // setDailyWorkloadSortedByDayIndex(clonedArr);
    }

    setDailyWorkloadSortedByDayIndex(clonedArr);
  };

  const handleOnResizeStartLeftExtend = (width) => {
    // const lastBoxWidth = width - oldBoxesWidth;
    const firstBoxWidth = width - oldBoxesWidthPlusWithNewBoxesAdded;

    const clonedArr = cloneDeep(dailyWorkloadSortedByDayIndex);

    // TODO: handle case when no day in the list
    const isFirstNew = clonedArr.at(0)?.isNewFirstBox;
    if (!isFirstNew) {
      // clonedArr.at(-1).boxWidth = getBoxWidth(clonedArr.length - 1, clonedArr.length);
      clonedArr.at(0).boxWidth = dayBoxWidth; // TODO: commenting this line

      clonedArr.unshift({
        ...boxTemplate,
        dayIndex: 0,
        effort: 0,
        offDay: false,
        isNewLastBox: false,
        isNewBox: true,
        // boxWidth: getBoxWidth(clonedArr.length, clonedArr.length + 1),
        boxWidth: firstBoxWidth,
        isNewFirstBox: true,
      });

      const updatedClonedArr = clonedArr.map((k, i) => ({
        ...k,
        dayIndex: i,
        isNewFirstBox: i === 0,
      }));
      // debugger;
      clonedArr.splice(0, clonedArr.length, ...updatedClonedArr);

      // console.info("eee-eee - new box added at the start:", clonedArr);
    } else {
      clonedArr.at(0).boxWidth = firstBoxWidth;

      // // dailyWorkloadSortedByDayIndex.push(8);
      // const clonedArr = cloneDeep(dailyWorkloadSortedByDayIndex);

      // // TODO: here decide how many days to add, may be required two or more days, if diff width is 400px, (double of day box size)
      // clonedArr.push({ dayIndex: clonedArr.length, effort: 0, offDay: false, isNewBox: true });

      // console.info("eee-eee - new box added at the end:", clonedArr);

      // setDailyWorkloadSortedByDayIndex(clonedArr);
    }

    setDailyWorkloadSortedByDayIndex(clonedArr);
  };

  const handleOnResizeStartRightShrink = (width) => {};

  const handleOnResizeStartLeftShrink = (width) => {};

  const onResizeStart = (event, resizeData) => {
    event.stopPropagation();

    const {
      handle,
      size: { width, height },
    } = resizeData;
    // console.info("eee-eee onResizeStart:", resizeData);
    console.info("eee-eee onResizeStart:", handle, width, height, minTimelineWidth);

    const isRightSideResizeStart = handle === "e";
    const isLeftSideResizeStart = handle === "w";

    const isExtending = width >= minTimelineWidth;
    const isShrinking = width < minTimelineWidth;

    // return;

    // resize with right handle
    if (isRightSideResizeStart) {
      // debugger;
      // width is increased
      if (width > minTimelineWidth) {
        handleOnResizeStartRightExtend(width);
      }
      // width is decreased
      else if (width < minTimelineWidth) {
        handleOnResizeStartRightShrink(width);
      }
    }
    // resize with left handle
    else if (isLeftSideResizeStart) {
      // debugger;
      // width is increased
      if (isExtending) {
        handleOnResizeStartLeftExtend(width);
      }
      // width is decreased
      else if (isShrinking) {
        handleOnResizeStartLeftShrink(width);
      }
    }
  };

  // backup of 'onResizeStart' function
  // const onResizeStart = (event, resizeData) => {
  //   event.stopPropagation();

  //   const {
  //     handle,
  //     size: { width, height },
  //   } = resizeData;
  //   // console.info("eee-eee onResizeStart:", resizeData);
  //   console.info("eee-eee onResizeStart:", handle, width, height, minTimelineWidth);

  //   const isRightSideResizeStart = handle === "e";
  //   const isLeftSideResizeStart = handle === "w";

  //   // return;
  //   // resize with right handle
  //   if (isRightSideResizeStart) {
  //     // debugger;
  //     // increase timeline on right side
  //     if (width > minTimelineWidth) {
  //       // const lastBoxWidth = width - oldBoxesWidth;
  //       const lastBoxWidth = width - oldBoxesWidthPlusWithNewBoxesAdded;

  //       const clonedArr = cloneDeep(dailyWorkloadSortedByDayIndex);

  //       const isLastNew = clonedArr.at(-1)?.isNewLastBox;
  //       if (!isLastNew) {
  //         // note: adding a new box in the right-end so user can drag and increase timeline width
  //         // TODO: handle the decrease side, we need to resize the last timeline day box instead of new
  //         // dailyWorkloadSortedByDayIndex.push(8);

  //         // clonedArr.at(-1).boxWidth = getBoxWidth(clonedArr.length - 1, clonedArr.length);
  //         clonedArr.at(-1).boxWidth = dayBoxWidth; // TODO: commenting this line

  //         // TODO: here decide how many days to add, may be required two or more days, if diff width is 400px, (double of day box size)
  //         clonedArr.push({
  //           ...boxTemplate,
  //           dayIndex: clonedArr.length,
  //           effort: 0,
  //           offDay: false,
  //           isNewLastBox: true,
  //           isNewBox: true,
  //           // boxWidth: getBoxWidth(clonedArr.length, clonedArr.length + 1),
  //           boxWidth: lastBoxWidth,
  //         });

  //         console.info("eee-eee - new box added at the end:", clonedArr);
  //       } else {
  //         clonedArr.at(-1).boxWidth = lastBoxWidth;

  //         // // dailyWorkloadSortedByDayIndex.push(8);
  //         // const clonedArr = cloneDeep(dailyWorkloadSortedByDayIndex);

  //         // // TODO: here decide how many days to add, may be required two or more days, if diff width is 400px, (double of day box size)
  //         // clonedArr.push({ dayIndex: clonedArr.length, effort: 0, offDay: false, isNewBox: true });

  //         // console.info("eee-eee - new box added at the end:", clonedArr);

  //         // setDailyWorkloadSortedByDayIndex(clonedArr);
  //       }

  //       setDailyWorkloadSortedByDayIndex(clonedArr);
  //     }
  //     // decrease timeline from right side
  //     else if (width < minTimelineWidth) {
  //       // debugger;
  //     }
  //   } else if (isLeftSideResizeStart) {
  //     // debugger;
  //     // increase timeline on left side
  //     if (width >= minTimelineWidth) {
  //       // const lastBoxWidth = width - oldBoxesWidth;
  //       const firstBoxWidth = width - oldBoxesWidthPlusWithNewBoxesAdded;

  //       const clonedArr = cloneDeep(dailyWorkloadSortedByDayIndex);

  //       // TODO: handle case when no day in the list
  //       const isFirstNew = clonedArr.at(0)?.isNewFirstBox;
  //       if (!isFirstNew) {
  //         // clonedArr.at(-1).boxWidth = getBoxWidth(clonedArr.length - 1, clonedArr.length);
  //         clonedArr.at(0).boxWidth = dayBoxWidth; // TODO: commenting this line

  //         clonedArr.unshift({
  //           ...boxTemplate,
  //           dayIndex: 0,
  //           effort: 0,
  //           offDay: false,
  //           isNewLastBox: false,
  //           isNewBox: true,
  //           // boxWidth: getBoxWidth(clonedArr.length, clonedArr.length + 1),
  //           boxWidth: firstBoxWidth,
  //           isNewFirstBox: true,
  //         });

  //         const updatedClonedArr = clonedArr.map((k, i) => ({
  //           ...k,
  //           dayIndex: i,
  //           isNewFirstBox: i === 0,
  //         }));
  //         // debugger;
  //         clonedArr.splice(0, clonedArr.length, ...updatedClonedArr);

  //         // console.info("eee-eee - new box added at the start:", clonedArr);
  //       } else {
  //         clonedArr.at(0).boxWidth = firstBoxWidth;

  //         // // dailyWorkloadSortedByDayIndex.push(8);
  //         // const clonedArr = cloneDeep(dailyWorkloadSortedByDayIndex);

  //         // // TODO: here decide how many days to add, may be required two or more days, if diff width is 400px, (double of day box size)
  //         // clonedArr.push({ dayIndex: clonedArr.length, effort: 0, offDay: false, isNewBox: true });

  //         // console.info("eee-eee - new box added at the end:", clonedArr);

  //         // setDailyWorkloadSortedByDayIndex(clonedArr);
  //       }

  //       setDailyWorkloadSortedByDayIndex(clonedArr);
  //     }
  //     // decrease timeline from left side
  //     else if (width < minTimelineWidth) {
  //     }
  //   }
  // };

  const handleOnResizeStopRightExtend = (width) => {
    // const clonedArr = cloneDeep(dailyWorkload);
    const clonedArr = cloneDeep(dailyWorkloadSortedByDayIndex);

    const boxesMaxWidth = clonedArr.length * dayBoxWidth;

    const isSame = timelineFixedWidth === minTimelineWidth;

    // const widthAdded = width - timelineFixedWidth;
    const widthAdded = width - oldBoxesWidthPlusWithNewBoxesAdded;

    let boxesToAddNew = Math.floor(widthAdded / dayBoxWidth);

    // NOTE: add a or skip box on 50% pixels of box
    const boxSnapping = widthAdded % dayBoxWidth >= dayBoxWidth / 2;

    // boxesToAddNew += boxSnapping ? 1 : 0;
    boxesToAddNew += boxSnapping;

    // // remove all temporary boxes
    // const updatedCloned = clonedArr.filter((k) => !k.isNewBox);
    // clonedArr.splice(0, clonedArr.length, ...updatedCloned);

    const lastItem = clonedArr.at(-1);
    if (/*lastItem.isNewBox*/ lastItem.isNewLastBox) {
      // clonedArr.splice(clonedArr.length - 1, 1);
      // clonedArr.splice(-1, 1);
      clonedArr.pop();
    }

    let newBoxes = [];
    if (boxesToAddNew > 0) {
      newBoxes = Array(boxesToAddNew)
        // TODO/NOTE: here fill the array with full day effort, i.e. usually '8' hours
        .fill(0)
        .map((k, i) => ({
          ...boxTemplate,
          dayIndex: clonedArr.length + i,
          moment: clonedArr
            .at(-1)
            .moment.clone()
            .add(i + 1, "day")
            // .add(1, "day")
            .clone(),
          effort: k,
          offDay: false,
          isNewLastBox: false,
          isNewBox: true,
          boxWidth: dayBoxWidth,
        }));
      // debugger;

      // TODO: here decide how many days to add, may be required two or more days, if diff width is 400px, (double of day box size)
      clonedArr.push(...newBoxes);
    }

    console.info("eee-eee - new box added at the end:", clonedArr);

    setDailyWorkloadSortedByDayIndex(clonedArr);
    setOldArr(clonedArr);

    // if (boxesToAddNew > 0) {
    updateState({
      // rightExtendBoxes: newBoxes,
      rightExtendDays: boxesToAddNew,
    });
    // }

    // const fullWidthSnapped = clonedArr.length * dayBoxWidth;

    // setTimelineWidth(fullWidthSnapped);

    // setNewDayWidth(0);

    // timelineFixedWidth = fullWidthSnapped;
    // setTimelineFixedWidth(fullWidthSnapped);
  };

  const handleOnResizeStopLeftExtend = (width) => {
    // const clonedArr = cloneDeep(dailyWorkload);
    const clonedArr = cloneDeep(dailyWorkloadSortedByDayIndex);

    const boxesMaxWidth = clonedArr.length * dayBoxWidth;

    const isSame = timelineFixedWidth === minTimelineWidth;

    // const widthAdded = width - timelineFixedWidth;
    const widthAdded = width - oldBoxesWidthPlusWithNewBoxesAdded;

    let boxesToAddNew = Math.floor(widthAdded / dayBoxWidth);

    // NOTE: add a or skip box on 50% pixels of box
    const boxSnapping = widthAdded % dayBoxWidth >= dayBoxWidth / 2;

    // boxesToAddNew += boxSnapping ? 1 : 0;
    boxesToAddNew += boxSnapping;

    // // remove all temporary boxes
    // const updatedCloned = clonedArr.filter((k) => !k.isNewBox);
    // clonedArr.splice(0, clonedArr.length, ...updatedCloned);

    const firstItem = clonedArr.at(0);
    if (/*lastItem.isNewBox*/ firstItem.isNewFirstBox) {
      // clonedArr.splice(clonedArr.length - 1, 1);
      // clonedArr.splice(-1, 1);
      clonedArr.shift();
    }

    let newBoxes = [];
    if (boxesToAddNew > 0) {
      newBoxes = Array(boxesToAddNew)
        // TODO/NOTE: here fill the array with full day effort, i.e. usually '8' hours
        .fill(0)
        .map((k, i, selfArr) => ({
          ...boxTemplate,
          dayIndex: i,
          moment: clonedArr
            .at(0)
            .moment.clone()
            .add(i - selfArr.length, "day")
            .clone(),
          effort: k,
          isNewBox: true,
          boxWidth: dayBoxWidth,
        }));
      // debugger;

      clonedArr.unshift(...newBoxes);
      const updatedClonedArr = clonedArr.map((k, i) => ({
        ...k,
        dayIndex: i,
      }));
      clonedArr.splice(0, clonedArr.length, ...updatedClonedArr);
    }

    console.info("eee-eee - new box added at the first:", clonedArr);

    const boxesWidth = boxesToAddNew * dayBoxWidth;
    // const boxesWidth = newBoxes.reduce((acc, cur) => acc + cur.boxWidth, 0);

    const firstNewBoxes = [];

    for (let bx of clonedArr) {
      if (bx.isNewBox) {
        firstNewBoxes.push(bx);
      } else {
        break;
      }
    }

    const firstNewBoxesWidth = firstNewBoxes.reduce((acc, curr) => acc + curr.boxWidth, 0);

    const newDragPosition = timelineDragPositionBackup - firstNewBoxesWidth;

    setTimelineDragPosition(newDragPosition);

    // setTimelineDragPosition(timelineDragPositionBackup - boxesWidth);

    setDailyWorkloadSortedByDayIndex(clonedArr);
    setOldArr(clonedArr);

    // Note: commented out the following IF condition because to restore the original width
    // if (boxesToAddNew > 0) {
    updateState({
      // leftExtendBoxes: newBoxes,
      leftExtendDays: boxesToAddNew,
    });
    // }

    // const fullWidthSnapped = clonedArr.length * dayBoxWidth;

    // setTimelineWidth(fullWidthSnapped);

    // setNewDayWidth(0);

    // timelineFixedWidth = fullWidthSnapped;
    // setTimelineFixedWidth(fullWidthSnapped);
  };

  const handleOnResizeStopRightShrink = (width) => {};

  const handleOnResizeStopLeftShrink = (width) => {};

  const onResizeStop = (event, resizeData) => {
    event.stopPropagation();

    const {
      handle,
      size: { width, height },
    } = resizeData;
    console.info("eee-eee onResizeStop:", resizeData);
    // console.info("eee-eee onResizeStop:", handle, width);

    const isRightSideResizeStop = handle === "e";
    const isLeftSideResizeStop = handle === "w";

    const isExtending = width >= oldBoxesWidthPlusWithNewBoxesAdded;
    const isShrinking = width < minTimelineWidth;

    // return;

    // resize with right handle
    if (isRightSideResizeStop) {
      // debugger;

      // width is increased
      // if (width > timelineWidth) {
      // if (width > minTimelineWidth) {
      // if (width >= timelineFixedWidth) {
      if (isExtending) {
        handleOnResizeStopRightExtend(width);
      }
      // width is decreased
      else if (isShrinking) {
      }
    } else if (isLeftSideResizeStop) {
      // debugger;

      // width is increased
      // if (width > timelineWidth) {
      // if (width > minTimelineWidth) {
      // if (width >= timelineFixedWidth) {
      if (isExtending) {
        handleOnResizeStopLeftExtend(width);
      }
      // width is decreased
      else if (isShrinking) {
      }
    }
  };

  // backup of 'onResizeStop' function
  // const onResizeStop = (event, resizeData) => {
  //   event.stopPropagation();

  //   const {
  //     handle,
  //     size: { width, height },
  //   } = resizeData;
  //   console.info("eee-eee onResizeStop:", resizeData);
  //   // console.info("eee-eee onResizeStop:", handle, width);

  //   const isRightSideResizeStop = handle === "e";
  //   const isLeftSideResizeStop = handle === "w";

  //   return;
  //   // resize with right handle
  //   if (isRightSideResizeStop) {
  //     // debugger;
  //     // const clonedArr = cloneDeep(dailyWorkload);
  //     const clonedArr = cloneDeep(dailyWorkloadSortedByDayIndex);

  //     const boxesMaxWidth = clonedArr.length * dayBoxWidth;

  //     const isSame = timelineFixedWidth === minTimelineWidth;

  //     // width is increased than minTimelineWidth
  //     // if (width > timelineWidth) {
  //     // if (width > minTimelineWidth) {
  //     // if (width >= timelineFixedWidth) {
  //     if (width >= oldBoxesWidthPlusWithNewBoxesAdded) {
  //       // const widthAdded = width - timelineFixedWidth;
  //       const widthAdded = width - oldBoxesWidthPlusWithNewBoxesAdded;

  //       let boxesToAddNew = Math.floor(widthAdded / dayBoxWidth);

  //       // NOTE: add a or skip box on 50% pixels of box
  //       const boxSnapping = widthAdded % dayBoxWidth >= dayBoxWidth / 2;

  //       // boxesToAddNew += boxSnapping ? 1 : 0;
  //       boxesToAddNew += boxSnapping;

  //       // // remove all temporary boxes
  //       // const updatedCloned = clonedArr.filter((k) => !k.isNewBox);
  //       // clonedArr.splice(0, clonedArr.length, ...updatedCloned);

  //       const lastItem = clonedArr.at(-1);
  //       if (/*lastItem.isNewBox*/ lastItem.isNewLastBox) {
  //         // clonedArr.splice(clonedArr.length - 1, 1);
  //         // clonedArr.splice(-1, 1);
  //         clonedArr.pop();
  //       }

  //       let newBoxes = [];
  //       if (boxesToAddNew > 0) {
  //         newBoxes = Array(boxesToAddNew)
  //           // TODO/NOTE: here fill the array with full day effort, i.e. usually '8' hours
  //           .fill(0)
  //           .map((k, i) => ({
  //             ...boxTemplate,
  //             dayIndex: clonedArr.length + i,
  //             moment: clonedArr
  //               .at(-1)
  //               .moment.clone()
  //               .add(i + 1, "day")
  //               // .add(1, "day")
  //               .clone(),
  //             effort: k,
  //             offDay: false,
  //             isNewLastBox: false,
  //             isNewBox: true,
  //             boxWidth: dayBoxWidth,
  //           }));
  //         // debugger;

  //         // TODO: here decide how many days to add, may be required two or more days, if diff width is 400px, (double of day box size)
  //         clonedArr.push(...newBoxes);
  //       }

  //       console.info("eee-eee - new box added at the end:", clonedArr);

  //       setDailyWorkloadSortedByDayIndex(clonedArr);
  //       setOldArr(clonedArr);

  //       // if (boxesToAddNew > 0) {
  //       updateState({
  //         // rightExtendBoxes: newBoxes,
  //         rightExtendDays: boxesToAddNew,
  //       });
  //       // }

  //       // const fullWidthSnapped = clonedArr.length * dayBoxWidth;

  //       // setTimelineWidth(fullWidthSnapped);

  //       // setNewDayWidth(0);

  //       // timelineFixedWidth = fullWidthSnapped;
  //       // setTimelineFixedWidth(fullWidthSnapped);
  //     }
  //   } else if (isLeftSideResizeStop) {
  //     // return;
  //     // debugger;
  //     // const clonedArr = cloneDeep(dailyWorkload);
  //     const clonedArr = cloneDeep(dailyWorkloadSortedByDayIndex);

  //     const boxesMaxWidth = clonedArr.length * dayBoxWidth;

  //     const isSame = timelineFixedWidth === minTimelineWidth;

  //     // width is increased than minTimelineWidth
  //     // if (width > timelineWidth) {
  //     // if (width > minTimelineWidth) {
  //     // if (width >= timelineFixedWidth) {
  //     if (width >= oldBoxesWidthPlusWithNewBoxesAdded) {
  //       // const widthAdded = width - timelineFixedWidth;
  //       const widthAdded = width - oldBoxesWidthPlusWithNewBoxesAdded;

  //       let boxesToAddNew = Math.floor(widthAdded / dayBoxWidth);

  //       // NOTE: add a or skip box on 50% pixels of box
  //       const boxSnapping = widthAdded % dayBoxWidth >= dayBoxWidth / 2;

  //       // boxesToAddNew += boxSnapping ? 1 : 0;
  //       boxesToAddNew += boxSnapping;

  //       // // remove all temporary boxes
  //       // const updatedCloned = clonedArr.filter((k) => !k.isNewBox);
  //       // clonedArr.splice(0, clonedArr.length, ...updatedCloned);

  //       const firstItem = clonedArr.at(0);
  //       if (/*lastItem.isNewBox*/ firstItem.isNewFirstBox) {
  //         // clonedArr.splice(clonedArr.length - 1, 1);
  //         // clonedArr.splice(-1, 1);
  //         clonedArr.shift();
  //       }

  //       let newBoxes = [];
  //       if (boxesToAddNew > 0) {
  //         newBoxes = Array(boxesToAddNew)
  //           // TODO/NOTE: here fill the array with full day effort, i.e. usually '8' hours
  //           .fill(0)
  //           .map((k, i, selfArr) => ({
  //             ...boxTemplate,
  //             dayIndex: i,
  //             moment: clonedArr
  //               .at(0)
  //               .moment.clone()
  //               .add(i - selfArr.length, "day")
  //               .clone(),
  //             effort: k,
  //             isNewBox: true,
  //             boxWidth: dayBoxWidth,
  //           }));
  //         // debugger;

  //         clonedArr.unshift(...newBoxes);
  //         const updatedClonedArr = clonedArr.map((k, i) => ({
  //           ...k,
  //           dayIndex: i,
  //         }));
  //         clonedArr.splice(0, clonedArr.length, ...updatedClonedArr);
  //       }

  //       console.info("eee-eee - new box added at the first:", clonedArr);

  //       const boxesWidth = boxesToAddNew * dayBoxWidth;
  //       // const boxesWidth = newBoxes.reduce((acc, cur) => acc + cur.boxWidth, 0);

  //       const firstNewBoxes = [];

  //       for (let bx of clonedArr) {
  //         if (bx.isNewBox) {
  //           firstNewBoxes.push(bx);
  //         } else {
  //           break;
  //         }
  //       }

  //       const firstNewBoxesWidth = firstNewBoxes.reduce((acc, curr) => acc + curr.boxWidth, 0);

  //       const newDragPosition = timelineDragPositionBackup - firstNewBoxesWidth;

  //       setTimelineDragPosition(newDragPosition);

  //       // setTimelineDragPosition(timelineDragPositionBackup - boxesWidth);

  //       setDailyWorkloadSortedByDayIndex(clonedArr);
  //       setOldArr(clonedArr);

  //       // Note: commented out the following IF condition because to restore the original width
  //       // if (boxesToAddNew > 0) {
  //       updateState({
  //         // leftExtendBoxes: newBoxes,
  //         leftExtendDays: boxesToAddNew,
  //       });
  //       // }

  //       // const fullWidthSnapped = clonedArr.length * dayBoxWidth;

  //       // setTimelineWidth(fullWidthSnapped);

  //       // setNewDayWidth(0);

  //       // timelineFixedWidth = fullWidthSnapped;
  //       // setTimelineFixedWidth(fullWidthSnapped);
  //     }

  //     // // const clonedArr = cloneDeep(dailyWorkload);
  //     // const clonedArr = cloneDeep(
  //     //   dailyWorkloadSortedByDayIndex
  //     //   // .slice(
  //     //   //   0,
  //     //   //   endsBeforeTimespan || startsAfterTimespan
  //     //   //     ? 0
  //     //   //     : endDateIndex < 0
  //     //   //     ? daysList[selectedCalendarTimeline] - startInx
  //     //   //     : endDateIndex + 1
  //     //   // )
  //     // );
  //     // // debugger;
  //     // const boxesMaxWidth = clonedArr.length * dayBoxWidth;

  //     // // width is increased than minTimelineWidth
  //     // if (width > timelineFixedWidth) {
  //     //   // const widthAdded = width - minTimelineWidth;
  //     //   const widthAdded = width - timelineFixedWidth;

  //     //   let boxesToAddNew = Math.floor(widthAdded / dayBoxWidth);
  //     //   // note: add a or skip box on 50% pixels of box
  //     //   const boxSnapping = widthAdded % dayBoxWidth >= dayBoxWidth / 2;
  //     //   boxesToAddNew += boxSnapping ? 1 : 0;

  //     //   // // remove all temporary boxes
  //     //   // const updatedCloned = clonedArr.filter((k) => !k.isNewBox);
  //     //   // clonedArr.splice(0, clonedArr.length, ...updatedCloned);

  //     //   const firstItem = clonedArr.at(0);
  //     //   if (firstItem.isNewBox) {
  //     //     clonedArr.splice(0, 1);
  //     //   }

  //     //   const newBoxes = Array(boxesToAddNew)
  //     //     // here fill the array with full day effort, i.e. usually '8' hours
  //     //     .fill(0)
  //     //     .map((k, i, selfArr) => {
  //     //       const prevMoment = clonedArr
  //     //         .at(0)
  //     //         .moment.clone()
  //     //         .add(i - selfArr.length, "day")
  //     //         // .add(-1, "day")
  //     //         .clone();
  //     //       // debugger;

  //     //       return {
  //     //         dayIndex: i,
  //     //         moment: prevMoment.clone(),
  //     //         effort: k,
  //     //         offDay: false,
  //     //         isNewLastBox: false,
  //     //         isNewBox: true,
  //     //       };
  //     //     });
  //     //   // debugger;

  //     //   // TODO: here decide how many days to add, may be required two or more days, if diff width is 400px, (double of day box size)
  //     //   clonedArr.unshift(...newBoxes);
  //     //   const updatedClonedArr = clonedArr.map((k, i) => ({
  //     //     ...k,
  //     //     dayIndex: i,
  //     //     isNewLastBox: false,
  //     //   }));
  //     //   clonedArr.splice(0, clonedArr.length, ...updatedClonedArr);
  //     //   console.info("eee-eee - new box added at the first:", clonedArr);

  //     //   setDailyWorkloadSortedByDayIndex(clonedArr);

  //     //   const resizeLeftWidth = boxesToAddNew * dayBoxWidth;
  //     //   setTimelineDragPosition(timelineDragPositionBackup - resizeLeftWidth);

  //     //   const fullWidthSnapped = clonedArr.length * dayBoxWidth;

  //     //   setTimelineWidth(fullWidthSnapped);
  //     //   setNewDayWidth(0);

  //     //   // timelineFixedWidth = fullWidthSnapped;
  //     //   setTimelineFixedWidth(fullWidthSnapped);
  //     // }
  //   }
  // };

  const rndResizeStart = (...args) => {
    // arguments = (
    //   e: SyntheticMouseEvent<HTMLDivElement> | SyntheticTouchEvent<HTMLDivElement>,
    //   dir: ResizeDirection,
    //   refToElement: React.ElementRef<'div'>,
    // ) => void;

    console.info("rndResizeStart:", args);
  };

  // let lastBoxWidth = 0;
  let lastBoxWidth = useRef(0);

  if (!taskData) {
    return <>{children}</>;
  }

  const rndResize = (...args) => {
    // arguments = (
    //   e: MouseEvent | TouchEvent,
    //   dir: ResizeDirection,
    //   refToElement: React.ElementRef<'div'>,
    //   delta: ResizableDelta,
    //   position: Position,
    // ) => void;

    console.info("rndResize:", args);

    const [event, direction, refToElement, delta, position] = args;

    const isRightResize = direction === "right";

    if (isRightResize) {
      const isExtending = delta.width > 0;
      const isShrinking = delta.width < 0;

      const clonedArr = cloneDeep(dailyWorkloadSortedByDayIndex);

      const lastBox = clonedArr.at(-1);

      console.info("eee-eee - change in width -- 1:", delta.width, cloneDeep(lastBox));

      if (isExtending) {
        const newWidth = dayBoxWidth + delta.width;
        lastBox.boxWidth = newWidth;
        lastBoxWidth.current = newWidth;
        console.info("eee-eee - last box width -- 1:", lastBoxWidth.current);
      } else if (isShrinking) {
        const newWidth = lastBoxWidth.current + delta.width;
        // lastBox.boxWidth = lastBox.boxWidth + delta.width;
        lastBox.boxWidth = newWidth;
        lastBoxWidth.current = newWidth;

        console.info("eee-eee - last box width -- 2:", lastBoxWidth.current);
      }

      console.info("eee-eee - change in width -- 2:", delta.width, cloneDeep(lastBox));

      clonedArr.splice(-1, 1, lastBox);

      setDailyWorkloadSortedByDayIndex(clonedArr);
    }
  };

  const rndResizeStop = (...args) => {
    // arguments = (
    //   e: MouseEvent | TouchEvent,
    //   dir: ResizeDirection,
    //   refToElement: React.ElementRef<'div'>,
    //   delta: ResizableDelta,
    //   position: Position,
    // ) => void;

    console.info("rndResizeStop:", args);
  };

  const taskTimelineResizableSettings = {
    axis: "x",
    height: minTimelineHeight,
    // minConstraints: [minTimelineWidth, minTimelineHeight],
    // maxConstraints: [],
    resizeHandles: ["e", "w"],
    className: classNames(
      "task-timeline-resize-container-wrapper",
      classes.taskTimelineResizeContainer
    ),
    // width: parseFloat(parseFloat(timelineWidth).toFixed(1)),
    // width: parseFloat(timelineWidth.toFixed(1)),
    // width: timelineWidth.toFixed(1),
    // width: parseFloat(timelineWidth),
    // width: timelineWidth,
    width: dailyWorkloadSortedByDayIndex.reduce((acc, cur) => acc + cur.boxWidth, 0),
    // width: dailyWorkloadSortedByDayIndex.length * dayBoxWidth,
    // width: parseInt(timelineWidth.toFixed()),
    handle: (handleDirection, handleRef) => {
      const isRight = handleDirection === "e";
      const isLeft = handleDirection === "w";

      const hideHandle = (isLeft && startsBeforeTimespan) || (isRight && endsAfterTimespan);

      return (
        <button
          className={classNames(
            classes.expandButton,
            "resize-handle",
            // handleDirection === "e" && ["resize-handle-right", [classes.expandButtonRight], !startsBeforeTimespan && 'resize-handle-hide'],
            isRight && [
              "resize-handle-right",
              [classes.expandButtonRight],
              // NOTE: this is not working because CSS hover sets to 'block', so, inline style has priority and we're using it here below
              // endsAfterTimespan && [classes.hideHandle],
            ],
            isLeft && [
              "resize-handle-left",
              [classes.expandButtonLeft],
              // startsBeforeTimespan && [classes.hideHandle],
            ]
            // {
            //   [`${classes.expandButtonRight} resize-handle-right`]: handleDirection === "e",
            //   [`${classes.expandButtonLeft} resize-handle-left`]: handleDirection === "w",
            // }
          )}
          style={hideHandle ? { display: "none" } : {}}
          ref={handleRef}>
          <DragHandleIcon className={classes.expandSVG} />
        </button>
      );
    },
  };

  const taskTimelineResizableHandlers = {
    onResize: onResize,
    onResizeStart: onResizeStart,
    onResizeStop: onResizeStop,
  };

  const rndSettings = {
    size: {
      width: dailyWorkloadSortedByDayIndex.reduce((acc, cur) => acc + cur.boxWidth, 0),
      height: minTimelineHeight,
    },
    className: classNames(
      "task-timeline-resize-container-wrapper",
      classes.taskTimelineResizeContainer
    ),
    resizeGrid: [1, 0],
    resizeHandleComponent: {
      right: (
        <button className={classNames(classes.expandButton, "resize-handle")}>
          <DragHandleIcon className={classes.expandSVG} />
        </button>
      ),
    },
    enableResizing: {
      top: false,
      right: true,
      bottom: false,
      left: true,
      topRight: false,
      topBottom: false,
      leftBottom: false,
      leftTop: false,
    },
    disableDragging: true,
    dragAxis: "none",
  };

  const rndHandlers = {
    onResizeStart: rndResizeStart,
    onResize: rndResize,
    onResizeStop: rndResizeStop,
  };

  if (USE_REACT_RND) {
    return (
      <>
        {/* <Resizable {...taskTimelineResizableSettings} {...taskTimelineResizableHandlers}> */}
        <Rnd {...rndSettings} {...rndHandlers}>
          <div
            className={classNames(
              "task-timeline-resize-container",
              classes.timelineStrip,
              classes.draggable
            )}
            // style={{ width: `${timelineWidth - 6}px`, height: `${minTimelineHeight}px` }}
            style={{ width: `${timelineWidth}px`, height: `${minTimelineHeight}px` }}
            // aria-owns={open ? 'mouse-over-popover' : undefined}
            // aria-haspopup="true"
            // onMouseEnter={handlePopoverOpen}
            // onMouseLeave={handlePopoverClose}
          >
            {children}
          </div>
        </Rnd>
        {/* </Resizable> */}
      </>
    );
  }

  if (USE_REACT_RESIZABLE) {
    return (
      <>
        <Resizable {...taskTimelineResizableSettings} {...taskTimelineResizableHandlers}>
          <div
            className={classNames(
              "task-timeline-resize-container",
              classes.timelineStrip,
              classes.draggable
            )}
            // style={{ width: `${timelineWidth - 6}px`, height: `${minTimelineHeight}px` }}
            style={{ width: `${timelineWidth}px`, height: `${minTimelineHeight}px` }}
            // aria-owns={open ? 'mouse-over-popover' : undefined}
            // aria-haspopup="true"
            // onMouseEnter={handlePopoverOpen}
            // onMouseLeave={handlePopoverClose}
          >
            {children}
          </div>
        </Resizable>
      </>
    );
  }

  // return (
  //   <>
  //     {/* <Resizable {...taskTimelineResizableSettings} {...taskTimelineResizableHandlers}> */}
  //     <Rnd {...rndSettings} {...rndHandlers}>
  //       <div
  //         className={classNames(
  //           "task-timeline-resize-container",
  //           classes.timelineStrip,
  //           classes.draggable
  //         )}
  //         // style={{ width: `${timelineWidth - 6}px`, height: `${minTimelineHeight}px` }}
  //         style={{ width: `${timelineWidth}px`, height: `${minTimelineHeight}px` }}
  //         // aria-owns={open ? 'mouse-over-popover' : undefined}
  //         // aria-haspopup="true"
  //         // onMouseEnter={handlePopoverOpen}
  //         // onMouseLeave={handlePopoverClose}
  //       >
  //         {children}
  //       </div>
  //     </Rnd>
  //     {/* </Resizable> */}
  //   </>
  // );
};

TaskTimelineResizable.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
  children: PropTypes.element.isRequired,
  minTimelineWidth: PropTypes.number,
  startsBeforeTimespan: PropTypes.bool,
  endsAfterTimespan: PropTypes.bool,
  setTimelineFixedWidth: PropTypes.func,
  setNewDayWidth: PropTypes.func,
  boxTemplate: PropTypes.any,
  setTimelineDragPosition: PropTypes.func,
  timelineDragPositionBackup: PropTypes.number,
  // dayBoxWidth: PropTypes.number,
  setDailyWorkloadSortedByDayIndex: PropTypes.func,
  dailyWorkloadSortedByDayIndexBackup: PropTypes.array,
  setDailyWorkloadSortedByDayIndexBackup: PropTypes.func,
  timelineFixedWidth: PropTypes.number,
  dailyWorkloadSortedByDayIndex: PropTypes.array,
  timelineDragPosition: PropTypes.number,
  timelineInitialPosition: PropTypes.number,
  getBoxWidth: PropTypes.func,
  // taskId: PropTypes.string,
  // plannedStartDate: PropTypes.any,
  // plannedEndDate: PropTypes.any,
  taskDailyWorkload: PropTypes.array,
};

TaskTimelineResizable.defaultProps = {
  classes: {},
  theme: {},
};

const WithInjectedHOCs = compose(
  withRouter,
  withStyles(styles, { withTheme: true })
)(TaskTimelineResizable);

const Memoized = memo(WithInjectedHOCs);

export default Memoized;
