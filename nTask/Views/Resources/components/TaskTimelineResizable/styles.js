const taskTimelineResizableStyles = (theme) => ({
  // here objects of styles
  timelineStrip: {
    display: "flex",
    alignItems: "center",
    position: "relative",
    // "& div": {
    //   background: "#00ABED",
    //   minWidth: 99,
    //   padding: 5,
    //   // borderRadius: 5,
    //   textAlign: "center",
    //   boxSizing: "border-box",
    // },
    // "& .task-timeline-day:first-of-type": {
    //   borderRadius: "5px 0 0 5px",
    // },
    // "& .task-timeline-day:last-of-type": {
    //   borderRadius: "0 5px 5px 0",
    // },
    // "& .task-timeline-day:only-of-type": {
    //   borderRadius: 5,
    // },
    // "& .task-timeline-day:first-child": {
    //   borderRadius: "5px 0 0 5px",
    // },
    // "& .task-timeline-day:last-child": {
    //   borderRadius: "0 5px 5px 0",
    // },
    // "& .task-timeline-day:only-child": {
    //   borderRadius: 5,
    // },
    // // backup
    // "&:first-child": {
    //   borderRadius: "5px 0 0 5px",
    // },
    // "&:last-child": {
    //   borderRadius: "0 5px 5px 0",
    // },
    // "&:only-child": {
    //   borderRadius: 5,
    // },
  },
  taskTimelineResizeContainer: {
    "&:hover .resize-handle": {
      display: "block",
    },
  },
  draggable: {
    cursor: "move",
  },
  expandButton: {
    display: "none",
    border: "none",
    background: "rgba(255, 255, 255, 0.2)",
    position: "absolute",
    height: "100%",
    padding: 0,
    cursor: "ew-resize",
    top: 0,
    zIndex: 2,
    // padding: '0 10px',
    "& svg": {
      transform: "rotate(90deg)",
    },
  },
  expandButtonRight: {
    left: "initial",
    right: 0,
    borderRadius: "0 5px 5px 0px",
  },
  expandButtonLeft: {
    left: 0,
    right: "initial",
    borderRadius: "5px 0 0 5px",
  },
  hideHandle: {
    display: "none",
  },
  expandSVG: {
    fontSize: 14,
  },
});

export default taskTimelineResizableStyles;
