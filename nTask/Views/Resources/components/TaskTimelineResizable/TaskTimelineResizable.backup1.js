import React, { memo, useEffect, useRef, useState } from "react";
import { connect, useSelector } from "react-redux";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import clsx from "clsx";
import { withRouterAndMaterialThemeStyles } from "../../../../hoc";
import TaskTimelineDay from "../TaskTimelineDay";
import { sortBy } from "lodash";
import Draggable from "react-draggable";
import DragHandleIcon from "@material-ui/icons/DragHandle";
import styles from "./styles";
import { Resizable } from "react-resizable";
import classNames from "classnames";
import { cloneDeep } from "lodash";
import { generateDateRange } from "../../../../helper/dates/dates";
import { daysList } from "./../../temp-utils";
import Popover from "@material-ui/core/Popover";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import { dayComparisonDateFormat } from "./../../constants";
import ErrorBoundary from "../ErrorBoundary";
import TaskTimelineDraggable from "../TaskTimelineDraggable";

const TaskTimelineResizable = (props) => {
  const {
    classes,
    theme,
    children,

    minTimelineWidth,
    startsBeforeTimespan,
    endsAfterTimespan,
    setTimelineFixedWidth,
    setNewDayWidth,

    setTimelineDragPosition,
    timelineDragPositionBackup,
    dayBoxWidth,
    setDailyWorkloadSortedByDayIndex,
    timelineFixedWidth,
    dailyWorkloadSortedByDayIndex,
    timelineDragPosition,
    timelineInitialPosition,
  } = props;

  console.info("rendered: TaskTimelineResizable");

  // let timelineWidth = minTimelineWidth;
  const [timelineWidth, setTimelineWidth] = useState(
    0
    // dailyWorkload.length * dayBoxWidth
    // dailyWorkloadSortedByDayIndex.length * dayBoxWidth
  );

  const [sortedWorkloads, setSortedWorkloads] = useState();
  const [isExtending, setIsExtending] = useState();
  const [isRightDirection, setIsRightDirection] = useState();

  const minTimelineHeight = 42;

  useEffect(() => {
    // setTimelineWidth(
    //   // .slice(
    //   //   0,
    //   //   endsBeforeTimespan || startsAfterTimespan
    //   //     ? 0
    //   //     : endDateIndex < 0
    //   //     ? daysList[selectedCalendarTimeline] - startInx
    //   //     : endDateIndex + 1
    //   // )
    //   dailyWorkloadSortedByDayIndex.length * dayBoxWidth // adding 1 to 'endDateIndex' to include it
    // );
    setTimelineWidth(minTimelineWidth);
  }, []);

  useEffect(() => {
    // setTimelineWidth(
    //   // .slice(
    //   //   0,
    //   //   endsBeforeTimespan || startsAfterTimespan
    //   //     ? 0
    //   //     : endDateIndex < 0
    //   //     ? daysList[selectedCalendarTimeline] - startInx
    //   //     : endDateIndex + 1
    //   // )
    //   dailyWorkloadSortedByDayIndex.length * dayBoxWidth // adding 1 to 'endDateIndex' to include it
    // );
    setTimelineWidth(dailyWorkloadSortedByDayIndex.reduce((acc, cur) => acc + cur.boxWidth, 0));
  }, [
    dailyWorkloadSortedByDayIndex,
    // selectedCalendarTimeline,
    // endDateIndex,
    // startInx,
    // dayBoxWidth,
    // startsAfterTimespan,
    // endsBeforeTimespan,
  ]);

  useEffect(() => {
    setSortedWorkloads(dailyWorkloadSortedByDayIndex);
  }, [dailyWorkloadSortedByDayIndex]);

  const onResize = (event, resizeData) => {
    event.stopPropagation();

    const {
      handle,
      size: { width, height },
    } = resizeData;
    // console.info("eee-eee onResize:", resizeData);
    console.info("eee-eee onResize:", handle, width, minTimelineWidth, width - minTimelineWidth);

    // setTimelineWidth(width);
    // return;

    // resize with right handle
    if (handle === "e") {
      debugger;
      setTimelineWidth(width);

      // width is increased than minTimelineWidth
      if (width > minTimelineWidth) {
        const clonedArr = cloneDeep(dailyWorkloadSortedByDayIndex);

        const oldArr = clonedArr.filter((k) => !k.isNewBox);
        const isLastNew = clonedArr.at(-1)?.isNewLastBox;

        if (!isLastNew) {
          debugger;

          const boxesToAddNew = 1;
          // const boxesToAddNew = Math.ceil((width - boxesMaxWidth) / dayBoxWidth);
          const newBoxes = Array(boxesToAddNew)
            // 'undefined' value set to hide any value while dragging/resizing
            .fill(undefined)
            .map((k, i) => ({
              dayIndex: oldArr.length,
              moment: null,
              effort: k,
              offDay: false,
              isNewLastBox: boxesToAddNew - 1 === i,
              isNewBox: true,
              boxWidth: dayBoxWidth,
            }));

          clonedArr.splice(0, oldArr.length, ...oldArr);

          // TODO: here decide how many days to add, may be required two or more days, if diff width is 400px, (double of day box size)
          clonedArr.push(...newBoxes);
          console.info("eeeeee - new box added at the end:", clonedArr);

          setDailyWorkloadSortedByDayIndex(clonedArr);
        }
        debugger;
        // const boxesMaxWidth = clonedArr.reduce((acc, cur) => acc + cur.boxWdith, 0);
        const oldBoxesWidth = oldArr.reduce((acc, cur) => acc + cur.boxWidth, 0);

        const lastBoxWidth = width - oldBoxesWidth;
        // we need to set the width increased or decreased of new box
        // setNewDayWidth(width - boxesMaxWidth);
        // setNewDayWidth(lastBoxWidth);

        const lastBox = clonedArr.at(-1);

        if (!lastBox || !lastBox.isNewLastBox) {
          return;
        }

        lastBox.boxWidth = lastBoxWidth;

        clonedArr.splice(-1, 1, lastBox);

        setDailyWorkloadSortedByDayIndex(clonedArr);
      }
      // setTimelineWidth(width);
    } else if (handle === "w") {
      setTimelineWidth(width);
      const clonedArr = cloneDeep(
        dailyWorkloadSortedByDayIndex
        // .slice(
        //   0,
        //   endsBeforeTimespan || startsAfterTimespan
        //     ? 0
        //     : endDateIndex < 0
        //     ? daysList[selectedCalendarTimeline] - startInx
        //     : endDateIndex + 1
        // )
      );
      // debugger;

      const boxesMaxWidth = clonedArr.length * dayBoxWidth;

      // width is increased than minTimelineWidth
      if (width > minTimelineWidth) {
        const isLastNew = clonedArr.at(0)?.isNewLastBox;

        if (!isLastNew) {
          const boxesToAddNew = Math.ceil((width - boxesMaxWidth) / dayBoxWidth);

          const newBoxes = Array(boxesToAddNew)
            // 'undefined' value set to hide any value while dragging/resizing
            .fill(undefined)
            .map((k, i) => ({
              dayIndex: i,
              moment: null,
              effort: k,
              offDay: false,
              isNewLastBox: i === 0,
              isNewBox: true,
            }));

          // TODO: here decide how many days to add, may be required two or more days, if diff width is 400px, (double of day box size)
          clonedArr.unshift(...newBoxes);
          const updatedClonedArr = clonedArr.map((k, i) => ({
            ...k,
            dayIndex: i,
            isNewLastBox: i === 0,
          }));
          clonedArr.splice(0, clonedArr.length, ...updatedClonedArr);

          console.info("eeeeee - new box added at the first:", clonedArr);

          setDailyWorkloadSortedByDayIndex(clonedArr);
        }

        const lastBoxWidth = width - (clonedArr.length - 1) * dayBoxWidth;
        // we need to set the width increased or decreased of new box
        // setNewDayWidth(width - boxesMaxWidth);
        setNewDayWidth(lastBoxWidth);
        console.info(
          "eee-eee timelineDragPosition XXXXX",
          timelineInitialPosition,
          timelineDragPosition,
          lastBoxWidth,
          timelineDragPosition - lastBoxWidth,
          timelineInitialPosition - lastBoxWidth
        );
        setTimelineDragPosition(timelineDragPositionBackup - lastBoxWidth);
        // setTimelineDragPosition(timelineInitialPosition - lastBoxWidth);
        // setTimelineDragPosition(timelineDragPosition - lastBoxWidth);
        // setTimelineDragPosition(() => timelineDragPosition - lastBoxWidth);
        // setTimelineDragPosition((w) => w - lastBoxWidth);
      }
    }
  };

  const onResizeStart = (event, resizeData) => {
    event.stopPropagation();

    const {
      handle,
      size: { width, height },
    } = resizeData;
    // console.info("eee-eee onResizeStart:", resizeData);
    console.info("eee-eee onResizeStart:", handle, width, height, minTimelineWidth);

    return;
    // resize with right handle
    if (handle === "e") {
      // debugger;
      // increase timeline on right side
      if (width > minTimelineWidth) {
        const isLastNew = dailyWorkloadSortedByDayIndex.at(-1)?.isNewLastBox;
        if (!isLastNew) {
          // note: adding a new box in the right-end so user can drag and increase timeline width
          // TODO: handle the decrease side, we need to resize the last timeline day box instead of new
          // dailyWorkloadSortedByDayIndex.push(8);
          const clonedArr = cloneDeep(dailyWorkloadSortedByDayIndex);

          // TODO: here decide how many days to add, may be required two or more days, if diff width is 400px, (double of day box size)
          clonedArr.push({
            dayIndex: clonedArr.length,
            effort: 0,
            offDay: false,
            isNewLastBox: true,
            isNewBox: true,
            boxWidth: width - minTimelineWidth,
          });

          console.info("eeeeee - new box added at the end:", clonedArr);

          setDailyWorkloadSortedByDayIndex(clonedArr);
        }

        // // dailyWorkloadSortedByDayIndex.push(8);
        // const clonedArr = cloneDeep(dailyWorkloadSortedByDayIndex);

        // // TODO: here decide how many days to add, may be required two or more days, if diff width is 400px, (double of day box size)
        // clonedArr.push({ dayIndex: clonedArr.length, effort: 0, offDay: false, isNewBox: true });

        // console.info("eeeeee - new box added at the end:", clonedArr);

        // setDailyWorkloadSortedByDayIndex(clonedArr);
      }
      // decrease timeline from right side
      else if (width < minTimelineWidth) {
      }
    } else if (handle === "w") {
    }
  };

  const onResizeStop = (event, resizeData) => {
    event.stopPropagation();
    const {
      handle,
      size: { width, height },
    } = resizeData;
    console.info("eee-eee onResizeStop:", resizeData);
    // console.info("eee-eee onResizeStop:", handle, width);

    return;
    // resize with right handle
    if (handle === "e") {
      // const clonedArr = cloneDeep(dailyWorkload);
      const clonedArr = cloneDeep(dailyWorkloadSortedByDayIndex);

      const boxesMaxWidth = clonedArr.length * dayBoxWidth;

      // width is increased than minTimelineWidth
      // if (width > timelineWidth) {
      // if (width > minTimelineWidth) {
      if (width > timelineFixedWidth) {
        const widthAdded = width - timelineFixedWidth;

        let boxesToAddNew = Math.floor(widthAdded / dayBoxWidth);

        // note: add a or skip box on 50% pixels of box
        const boxSnapping = widthAdded % dayBoxWidth >= dayBoxWidth / 2;

        boxesToAddNew += boxSnapping ? 1 : 0;

        // // remove all temporary boxes
        // const updatedCloned = clonedArr.filter((k) => !k.isNewBox);
        // clonedArr.splice(0, clonedArr.length, ...updatedCloned);

        const lastItem = clonedArr.at(-1);
        if (lastItem.isNewBox) {
          // clonedArr.splice(clonedArr.length - 1, 1);
          clonedArr.splice(-1, 1);
        }

        const newBoxes = Array(boxesToAddNew)
          // TODO/NOTE: here fill the array with full day effort, i.e. usually '8' hours
          .fill(8)
          .map((k, i) => ({
            dayIndex: clonedArr.length,
            moment: clonedArr
              .at(-1)
              .moment.clone()
              .add(i + 1, "day")
              // .add(1, "day")
              .clone(),
            effort: k,
            offDay: false,
            isNewLastBox: false,
            isNewBox: true,
          }));
        // debugger;

        // TODO: here decide how many days to add, may be required two or more days, if diff width is 400px, (double of day box size)
        clonedArr.push(...newBoxes);
        console.info("eeeeee - new box added at the end:", clonedArr);

        setDailyWorkloadSortedByDayIndex(clonedArr);

        const fullWidthSnapped = clonedArr.length * dayBoxWidth;

        setTimelineWidth(fullWidthSnapped);

        setNewDayWidth(0);

        // timelineFixedWidth = fullWidthSnapped;
        setTimelineFixedWidth(fullWidthSnapped);
      }
    } else if (handle === "w") {
      // const clonedArr = cloneDeep(dailyWorkload);
      const clonedArr = cloneDeep(
        dailyWorkloadSortedByDayIndex
        // .slice(
        //   0,
        //   endsBeforeTimespan || startsAfterTimespan
        //     ? 0
        //     : endDateIndex < 0
        //     ? daysList[selectedCalendarTimeline] - startInx
        //     : endDateIndex + 1
        // )
      );
      // debugger;
      const boxesMaxWidth = clonedArr.length * dayBoxWidth;

      // width is increased than minTimelineWidth
      if (width > timelineFixedWidth) {
        // const widthAdded = width - minTimelineWidth;
        const widthAdded = width - timelineFixedWidth;

        let boxesToAddNew = Math.floor(widthAdded / dayBoxWidth);
        // note: add a or skip box on 50% pixels of box
        const boxSnapping = widthAdded % dayBoxWidth >= dayBoxWidth / 2;
        boxesToAddNew += boxSnapping ? 1 : 0;

        // // remove all temporary boxes
        // const updatedCloned = clonedArr.filter((k) => !k.isNewBox);
        // clonedArr.splice(0, clonedArr.length, ...updatedCloned);

        const firstItem = clonedArr.at(0);
        if (firstItem.isNewBox) {
          clonedArr.splice(0, 1);
        }

        const newBoxes = Array(boxesToAddNew)
          // here fill the array with full day effort, i.e. usually '8' hours
          .fill(0)
          .map((k, i, selfArr) => {
            const prevMoment = clonedArr
              .at(0)
              .moment.clone()
              .add(i - selfArr.length, "day")
              // .add(-1, "day")
              .clone();
            // debugger;

            return {
              dayIndex: i,
              moment: prevMoment.clone(),
              effort: k,
              offDay: false,
              isNewLastBox: false,
              isNewBox: true,
            };
          });
        // debugger;

        // TODO: here decide how many days to add, may be required two or more days, if diff width is 400px, (double of day box size)
        clonedArr.unshift(...newBoxes);
        const updatedClonedArr = clonedArr.map((k, i) => ({
          ...k,
          dayIndex: i,
          isNewLastBox: false,
        }));
        clonedArr.splice(0, clonedArr.length, ...updatedClonedArr);
        console.info("eeeeee - new box added at the first:", clonedArr);

        setDailyWorkloadSortedByDayIndex(clonedArr);

        const resizeLeftWidth = boxesToAddNew * dayBoxWidth;
        setTimelineDragPosition(timelineDragPositionBackup - resizeLeftWidth);

        const fullWidthSnapped = clonedArr.length * dayBoxWidth;

        setTimelineWidth(fullWidthSnapped);
        setNewDayWidth(0);

        // timelineFixedWidth = fullWidthSnapped;
        setTimelineFixedWidth(fullWidthSnapped);
      }
    }
  };

  const taskTimelineResizableSettings = {
    axis: "x",
    height: minTimelineHeight,
    minConstraints: [minTimelineWidth, minTimelineHeight],
    // maxConstraints: [],
    resizeHandles: ["e", "w"],
    className: classNames(
      "task-timeline-resize-container-wrapper",
      classes.taskTimelineResizeContainer
    ),
    // width: parseFloat(parseFloat(timelineWidth).toFixed(1)),
    // width: parseFloat(timelineWidth.toFixed(1)),
    // width: timelineWidth.toFixed(1),
    // width: parseFloat(timelineWidth),
    // width: timelineWidth,
    width: parseInt(timelineWidth.toFixed()),
    handle: (handleDirection, handleRef) => {
      const isRight = handleDirection === "e";
      const isLeft = handleDirection === "w";

      const hideHandle = (isLeft && startsBeforeTimespan) || (isRight && endsAfterTimespan);

      return (
        <button
          className={classNames(
            classes.expandButton,
            "resize-handle",
            // handleDirection === "e" && ["resize-handle-right", [classes.expandButtonRight], !startsBeforeTimespan && 'resize-handle-hide'],
            isRight && [
              "resize-handle-right",
              [classes.expandButtonRight],
              // NOTE: this is not working because CSS hover sets to 'block', so, inline style has priority and we're using it here below
              // endsAfterTimespan && [classes.hideHandle],
            ],
            isLeft && [
              "resize-handle-left",
              [classes.expandButtonLeft],
              // startsBeforeTimespan && [classes.hideHandle],
            ]
            // {
            //   [`${classes.expandButtonRight} resize-handle-right`]: handleDirection === "e",
            //   [`${classes.expandButtonLeft} resize-handle-left`]: handleDirection === "w",
            // }
          )}
          style={hideHandle ? { display: "none" } : {}}
          ref={handleRef}>
          <DragHandleIcon className={classes.expandSVG} />
        </button>
      );
    },
  };

  const taskTimelineResizableHandlers = {
    onResize: onResize,
    onResizeStart: onResizeStart,
    onResizeStop: onResizeStop,
  };

  return (
    <>
      <Resizable {...taskTimelineResizableSettings} {...taskTimelineResizableHandlers}>
        <div
          className={classNames(
            "task-timeline-resize-container",
            classes.timelineStrip,
            classes.draggable
          )}
          style={{ width: `${timelineWidth - 6}px`, height: `${minTimelineHeight}px` }}
          // aria-owns={open ? 'mouse-over-popover' : undefined}
          // aria-haspopup="true"
          // onMouseEnter={handlePopoverOpen}
          // onMouseLeave={handlePopoverClose}
        >
          {children}
        </div>
      </Resizable>
    </>
  );
};

TaskTimelineResizable.defaultProps = {
  /** default props , calls when props are not coming from parent class undefined */
  classes: {},
  theme: {},
};

const WithInjectedHOCs = compose(
  withRouter,
  withStyles(styles, { withTheme: true })
)(TaskTimelineResizable);

const Memoized = memo(WithInjectedHOCs);

export default Memoized;
