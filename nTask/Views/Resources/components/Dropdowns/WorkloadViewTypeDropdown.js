import React, { useRef, useState } from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./styles";
import { SvgIcon, MenuItem, MenuList, ListItemText } from "@material-ui/core";
import CustomButton from "../../../../components/Buttons/CustomButton";
import PlainMenu from "../../../../components/Menu/menu";
import { withRouter } from "react-router-dom";
import DoubleArrowIcon from "../../../../components/Icons/DoubleArrow";
import { useDispatch, useSelector } from "react-redux";
import classNames from "classnames";
import { setResourceWorkloadViewTypeAction } from "../../../../redux/actions/resources";
import { ExpandMore as ExpandMoreIcon, Done as DoneIcon } from "@material-ui/icons";
import { ResourceWorkloadViewType } from "../../../../utils/constants/ResourceWorkloadViewType";
import { selectDataListCount } from "./../../../../redux/selectors/resources";
import clsx from "clsx";
import PropTypes from "prop-types";

function WorkloadViewTypeDropdown(props) {
  const { classes } = props;

  const dispatch = useDispatch();

  const [open, setOpen] = useState(false);

  const anchorEl = useRef(null);

  const dataListCount = useSelector(selectDataListCount);
  const workloadViewType = useSelector((s) => s.resources.uiGrid.workloadViewType);
  const openCollapsiblesCount = useSelector((s) => s.resources.uiGrid.openCollapsiblesCount);

  const selectedItem = Object.values(ResourceWorkloadViewType).find(
    (k) => k.value === workloadViewType
  );

  const handleClick = (event) => {
    event.stopPropagation();

    setOpen((s) => !s);
  };

  const handleClose = () => setOpen(false);

  const onWorkloadViewTypeChange = (e, newValue) => {
    handleClose();
    dispatch(setResourceWorkloadViewTypeAction(newValue));
  };

  return (
    <>
      <CustomButton
        onClick={handleClick}
        className={clsx(classes.borderBtn, classes.resourceToggleButton)}
        buttonRef={(node) => {
          anchorEl.current = node;
        }}
        btnType={"white"}
        variant="text">
        <SvgIcon
          viewBox="0 0 16 14"
          className={clsx(classes.doubleArrowSVG, {
            [classes.openCollapsables]: openCollapsiblesCount > 0,
          })}>
          <DoubleArrowIcon className={classes.qckFilterIconSvg} />
        </SvgIcon>
        &nbsp;
        <span className={classes.wvtDDLabel}>
          {selectedItem?.uiText} ({dataListCount || 0})
        </span>
        <ExpandMoreIcon />
      </CustomButton>
      <PlainMenu
        open={open}
        closeAction={handleClose}
        placement="bottom-start"
        anchorRef={anchorEl.current}
        style={{ width: 143 }}
        disablePortal>
        <MenuList disablePadding>
          <div
            className={classes.groupContainer}
            // style={{ width: 140 }}
          >
            {Object.values(ResourceWorkloadViewType).map((item) => {
              const isSelectedItem = item.value === (selectedItem?.value || workloadViewType);

              // TODO: remove this when projects' view available
              if (item.value !== workloadViewType) {
                return;
              }

              return (
                <MenuItem
                  key={item.value}
                  value={item.value}
                  className={classNames(
                    {
                      [classes.itemSelected]: isSelectedItem,
                      [classes.selectedValue]: isSelectedItem,
                    },
                    classes.menuListItem
                  )}
                  classes={{
                    selected: classes.menuItemSelected,
                  }}
                  onClick={(e) => onWorkloadViewTypeChange(e, item.value)}>
                  <ListItemText primary={item.uiText} classes={{ primary: classes.itemText }} />
                  {isSelectedItem ? <DoneIcon className={classes.doneIcon} /> : ""}
                </MenuItem>
              );
            })}
          </div>
        </MenuList>
      </PlainMenu>
    </>
  );
}

WorkloadViewTypeDropdown.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
};

WorkloadViewTypeDropdown.defaultProps = { classes: {}, theme: {} };

export default compose(
  withRouter,
  withStyles(styles, { withTheme: true })
)(WorkloadViewTypeDropdown);
