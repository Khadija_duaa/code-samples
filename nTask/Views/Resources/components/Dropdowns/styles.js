const collapsibleStyles = (theme) => ({
  // collapse side bar styles starts here
  itemText: {
    fontSize: 13,
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightRegular,
  },
  itemSelected: {
    padding: "5px 0 5px 16px",
    marginBottom: 2,
  },
  selectedText: {
    "& span": {
      color: theme.palette.secondary.main,
    },
  },
  selectedValue: {
    //Adds left border to selected list item
    "&:before": {
      top: "50%",
      left: "0",
      right: "0",
      width: "4px",
      bottom: "0",
      content: "''",
      position: "absolute",
      borderRadius: "0 10px 10px 0px",
      height: "30px",
      transform: "translateY(-50%)",
      background: theme.palette.secondary.main,
    },
  },
  menuItemSelected: {
    background: `${theme.palette.common.white} !important`,
    "&:hover": {
      background: `${theme.palette.background.items}`,
    },
    "&:focus": {
      background: `${theme.palette.background.items} !important`,
    },
  },
  borderBtn: {
    padding: "3px 8px 3px 4px",
    borderRadius: "4px",
    height: 32,
    fontFamily: "lato !important",
    color: "#171717",
    fontSize: "13px",
    minWidth: "87px",
    marginRight: 8,

    "& svg": {
      fontSize: 18,
    },

    "& span": {
      textTransform: "none",
    },
  },
  wvtDDLabel: {
    paddingLeft: "10px",
    fontWeight: 400,
    fontFamily: "'lato'",
    textTransform: "capitalize",
    color: "#171717",
    fontSize: "15px",
    paddingRight: "6px",
  },
  resourceToggleButton: {
    color: "#000",
  },
  doubleArrowSVG: {
    transform: "rotate(90deg)  translateX(2px)",
    fontSize: "16px !important",
    color: "#969696",
    transition: "450ms all ease-out",
    transformOrigin: "center center",
    transformBox: "fill-box",
  },
  doneIcon: {
    color: theme.palette.secondary.main,
    transform: "translateX(15px)",
  },
  groupContainer: {
    padding: "13px 0",

    "& *": {
      fontFamily: "lato !important",
    },
  },
  menuListItem: {
    padding: "3px 26px",
  },
  groupTitle: {
    fontSize: 14,
    color: "#7E7E7E",
    padding: "2px 0 5px 16px",
  },
  openCollapsables: {
    transform: "rotate(-90deg)",
  },
});

export default collapsibleStyles;
