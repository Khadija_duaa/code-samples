import React, { useRef, useState } from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./styles";
import { ListItemText, MenuList, MenuItem, SvgIcon } from "@material-ui/core";
import CustomButton from "../../../../components/Buttons/CustomButton";
import PlainMenu from "../../../../components/Menu/menu";
import { withRouter } from "react-router-dom";
import { CalendarTimelinePeriods } from "../../../../utils/constants/CalendarTimelinePeriods";
import { useDispatch, useSelector } from "react-redux";
import MagnifyGlassResources from "../../../../components/Icons/MagnifyGlassResources";
import DoneIcon from "@material-ui/icons/Done";
import clsx from "clsx";
import classNames from "classnames";
import { setFilterCalendarTimelinePeriodAction } from "../../../../redux/actions/resources";
import PropTypes from "prop-types";

function ZoomDropdown(props) {
  const { classes } = props;

  const dispatch = useDispatch();

  const [open, setOpen] = useState(false);

  const anchorEl = useRef(null);

  const calendarTimelinePeriod = useSelector((s) => s.resources.filters.calendarTimelinePeriod);

  const selectedItem = Object.values(CalendarTimelinePeriods).find(
    (k) => k.value === calendarTimelinePeriod
  );

  const handleClick = (event) => {
    event.stopPropagation();

    setOpen((s) => !s);
  };

  const handleClose = () => setOpen(false);

  const onCalendarSpanChange = (e, newValue) => {
    handleClose();
    dispatch(setFilterCalendarTimelinePeriodAction(newValue));
  };

  return (
    <>
      <CustomButton
        onClick={handleClick}
        className={classes.borderBtn}
        buttonRef={(node) => {
          anchorEl.current = node;
        }}
        btnType={"white"}
        variant="contained">
        <SvgIcon viewBox="0 0 16 17.778">
          <MagnifyGlassResources className={classes.qckFilterIconSvg} />
        </SvgIcon>
        &nbsp;
        <span className={classes.qckFilterLbl}>{selectedItem?.uiText}</span>
      </CustomButton>
      <PlainMenu
        open={open}
        closeAction={handleClose}
        placement="bottom-start"
        anchorRef={anchorEl.current}
        style={{ width: 130 }}>
        <MenuList disablePadding>
          <div
            className={classes.groupContainer}
            // style={{ width: 180 }}
          >
            {Object.values(CalendarTimelinePeriods).map((item) => {
              const isSelectedItem = item.value === (selectedItem?.value || calendarTimelinePeriod);

              return (
                <MenuItem
                  key={item.value}
                  value={item.value}
                  className={classNames(
                    {
                      [classes.itemSelected]: isSelectedItem,
                      [classes.selectedValue]: isSelectedItem,
                    },
                    classes.menuListItem
                  )}
                  classes={{
                    selected: classes.menuItemSelected,
                  }}
                  onClick={(e) => onCalendarSpanChange(e, item.value)}>
                  <ListItemText
                    primary={item.uiText}
                    classes={{ primary: classes.itemText }}
                    className={clsx(isSelectedItem && classes.selectedText)}
                  />
                  {isSelectedItem ? <DoneIcon className={classes.doneIcon} /> : ""}
                </MenuItem>
              );
            })}
          </div>
        </MenuList>
      </PlainMenu>
    </>
  );
}

ZoomDropdown.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
};

ZoomDropdown.defaultProps = { classes: {}, theme: {} };

export default compose(withRouter, withStyles(styles, { withTheme: true }))(ZoomDropdown);
