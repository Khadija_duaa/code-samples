// @flow

import React, { useRef, memo } from "react";
import { useSelector } from "react-redux";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import styles from "./styles";
import clsx from "clsx";
import Collapsible from "../Collapsible";
import UnassignedTasksRowCard from "../UnassignedTasksRowCard";
import GridRow from "../GridRow";
import UnassignedTasks from "../UnassignedTasks";
import { UnassignedTasksContext } from "../../contexts";
import PropTypes from "prop-types";

const UnassignedTasksRow = (props) => {
  const { classes } = props;

  const rowCollapseToggleBtnRef = useRef(null);

  const gridColumnsCount = useSelector((s) => s.resources.uiGrid.gridColumnsCount);
  const dayBoxWidth = useSelector((s) => s.resources.uiGrid.dayProgressHourglassWidth);

  return (
    <>
      <Collapsible
        collapseBtnRef={rowCollapseToggleBtnRef}
        summaryRow={(isExpanded) => (
          <GridRow
            leftColumn={
              <div
                className={clsx(classes.rcCard, classes.rcCardUnassigned)}
                ref={rowCollapseToggleBtnRef}>
                <UnassignedTasksRowCard isExpanded={isExpanded} />
              </div>
            }
            rightColumn={
              <div className={classes.rcTimeline}>
                <div className={classes.progressWrapper}>
                  {Array(gridColumnsCount)
                    .fill(0)
                    .map(() => (
                      <div
                        className={clsx(classes.dayProgress, classes.offDay)}
                        style={{
                          width: `${dayBoxWidth}px`,
                          maxWidth: `${dayBoxWidth}px`,
                          minWidth: `${dayBoxWidth}px`,
                        }}
                      />
                    ))}
                </div>
              </div>
            }
          />
        )}
        detailsPanel={(isExpanded) => {
          if (!isExpanded) {
            return <></>;
          }

          return (
            <UnassignedTasksContext.Provider value={{ isUnassignedTask: true }}>
              <UnassignedTasks />
            </UnassignedTasksContext.Provider>
          );
        }}
      />
    </>
  );
};

UnassignedTasksRow.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
};

UnassignedTasksRow.defaultProps = {
  classes: {},
  theme: {},
};

const WithInjectedHOCs = compose(
  withRouter,
  withStyles(styles, { withTheme: true })
)(UnassignedTasksRow);

const Memoized = memo(WithInjectedHOCs);

export default Memoized;
