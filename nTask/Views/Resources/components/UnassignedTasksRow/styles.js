const unassignedTasksRowStyles = (theme) => ({
  rcCard: {
    cursor: "pointer",
    width: "250px",
    height: "98px",
    position: "relative",
    padding: "13px",
    display: "flex",
    maxHeight: "98px",
    gap: "5px",
    "& p": {
      margin: 0,
      padding: 10,
    },
  },
  rcTimeline: {
    // no styles
    height: "100%",
  },
  progressWrapper: {
    display: "flex",
    height: "100%",
  },
  dayProgress: {
    padding: 5,
    boxSizing: "border-box",
    borderLeft: "1px solid #DDDDDD",
    "&:first-child": {
      borderLeft: "transparent",
    },
  },
  rcCardUnassigned: {
    height: "58px",
    maxHeight: "58px",
  },
  offDay: {
    cursor: "auto",
    backgroundColor: "#F3F3F3",
  },
});

export default unassignedTasksRowStyles;
