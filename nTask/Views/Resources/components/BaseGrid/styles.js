const baseGridStyles = (theme) => ({
  // here objects of styles
  rcContainer: {
    border: "1px solid #DDDDDD",
    borderRadius: 6,
    position: "relative",
    overflow: "auto",
    overflowY: "hidden",
    maxHeight: "calc(100vh - 180px)",
  },
  rcHeader: {
    width: "100%",
    position: "sticky",
    zIndex: "99",
    top: "0",
  },
  sideBarHeader: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    padding: "10px",
    height: "100%",
    "& p": {
      margin: 0,
      color: "#171717",
      fontSize: "15px",
      fontFamily: "'lato'",
    },
  },
  arrowForward: {
    padding: "4px 4px 4px 7px",
    backgroundColor: "white",
    border: "1px solid #E7E9EB",
    borderRadius: "50px",
    cursor: "pointer",
    color: "#646464",
    width: "24px",
    transition: "0.4s all ease",
    zIndex: 1,
    "&:hover": {
      backgroundColor: "#0090ff",
      border: "1px solid #0090ff",
      color: "white",
      width: "36px",
      paddingLeft: "0",
    },
  },
  collapsedArrow: {
    "&:hover": {
      paddingRight: "0",
      paddingLeft: 7,
    },
  },
  sideBarHeaderCollapsed: {
    justifyContent: "center",
  },
  sideBarDDParent: {
    width: "160px",
    opacity: 1,
    transition: "0.4s opacity ease",
    display: "flex",
    alignItems: "center",
    fontFamily: "'lato'",
    fontSize: "15px",
    color: "#171717",
    whiteSpace: "nowrap",
  },
  sidebarDDParentCollapsed: {
    opacity: 0,
    width: 0,
    display: "none",
  },
  slotsWrapper: {
    display: "flex",
  },
  monthSlots: {
    width: "100%",
    borderBottom: "1px solid #DDDDDD",
    marginBottom: "-1px",
    borderLeft: "1px solid #DDDDDD",
    marginLeft: "-1px",
  },
  monthNamesRow: {
    display: "flex",
    flexDirection: "row",
  },
  month: {
    padding: 5,
    borderBottom: "1px solid #DDDDDD",
    borderLeft: "1px solid #DDDDDD",
    boxSizing: "border-box",
    fontFamily: "Lato",
    color: "#171717",
    fontSize: "14px",
    "&:first-child": {
      borderLeft: "transparent",
    },
  },
  daySlots: {
    display: "flex",
  },
  slot: {
    textAlign: "center",
    padding: 5,
    boxSizing: "border-box",
    borderLeft: "1px solid #DDDDDD",
    fontFamily: "Lato",
    color: "#646464",
    fontSize: "14px",
    "&:first-child": {
      borderLeft: "transparent",
    },
  },
  currentDayHeaderCell: {
    background: "antiquewhite",
  },
  // currentDayDataCell: {
  //   background: 'antiquewhite'
  // },
  // TODO: we'll use this to disable full height/width off as same in XD design
  offDay: {
    // background: 'antiquewhite'
  },
  resourcePanel: {
    width: "100%",
  },
});

export default baseGridStyles;
