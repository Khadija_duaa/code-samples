import React, { useLayoutEffect } from "react";
import { compose } from "redux";
import GridRow from "../GridRow/GridRow";
import ArrowForward from "@material-ui/icons/ArrowForwardIos";
import ArrowBackIcon from "@material-ui/icons/ArrowBackIos";
import { useDispatch, useSelector } from "react-redux";
import styles from "./styles";
import { CalendarTimelinePeriods } from "../../../../utils/constants/CalendarTimelinePeriods";
import {
  getCurrent2WeekDates,
  getCurrentMonthDates,
  getCurrentWeekDates,
} from "../../../../helper/dates/dates";
import {
  resetGridCalendarDatesAction,
  resetGridCalendarDatesFlattenedAction,
  setDayProgressHourglassWidthAction,
  setGridCalendarDatesAction,
  setUiSidebarCollapsedAction,
} from "../../../../redux/actions/resources";
import { daysList } from "../../temp-utils";
import { WorkloadViewTypeDropdown } from "../Dropdowns";
import { clsx } from "clsx";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core";
import { useMeasure } from "react-use";
import Scrollbars from "react-custom-scrollbars";
import PropTypes from "prop-types";

const BaseGrid = (props) => {
  const { classes, children } = props;

  const dispatch = useDispatch();

  const selectedCalendarTimeline = useSelector(
    (state) => state.resources.filters.calendarTimelinePeriod
  );
  const sidebarCollapsed = useSelector((s) => s.resources.uiGrid.sidebarCollapsed);
  const calendarDates = useSelector((s) => s.resources.data.calendarDates);
  const calendarDatesFlattened = useSelector((s) => s.resources.data.calendarDatesFlattened);
  const dayBoxWidth = useSelector((s) => s.resources.uiGrid.dayProgressHourglassWidth);

  const [headerRowGrowingColumnRef, { width: colsTotalWidth }] = useMeasure();

  useLayoutEffect(() => {
    dispatch(resetGridCalendarDatesAction());
    dispatch(resetGridCalendarDatesFlattenedAction());

    const gridDates = generateCalendarGrid(selectedCalendarTimeline);
    dispatch(setGridCalendarDatesAction(gridDates));

    calculateGridBoxSize();
  }, [selectedCalendarTimeline]);

  useLayoutEffect(() => {
    calculateGridBoxSize();
  }, [colsTotalWidth]);

  const calculateGridBoxSize = () => {
    const dayWidth = parseFloat((colsTotalWidth / daysList[selectedCalendarTimeline]).toFixed(2));

    dispatch(setDayProgressHourglassWidthAction(dayWidth));
  };

  const generateCalendarGrid = (period) => {
    let generatedDates = [];

    switch (period) {
      case CalendarTimelinePeriods.week.value:
        generatedDates = getCurrentWeekDates();
        break;
      case CalendarTimelinePeriods.twoWeeks.value:
        generatedDates = getCurrent2WeekDates();
        break;
      case CalendarTimelinePeriods.month.value:
        generatedDates = getCurrentMonthDates();
        break;
      default:
        generatedDates = getCurrentWeekDates();
        break;
    }

    return generatedDates;
  };

  const handleToggleSidebar = () => dispatch(setUiSidebarCollapsedAction(!sidebarCollapsed));

  const isMonthView = selectedCalendarTimeline === CalendarTimelinePeriods.month.value;

  return (
    <>
      <div className={classes.rcContainer}>
        {/* header starts here */}
        <div className={classes.rcHeader}>
          <GridRow
            leftColumn={
              <div
                className={clsx(
                  classes.sideBarHeader,
                  sidebarCollapsed && classes.sideBarHeaderCollapsed
                )}>
                <div
                  className={clsx(
                    classes.sideBarDDParent,
                    sidebarCollapsed && classes.sidebarDDParentCollapsed
                  )}>
                  <WorkloadViewTypeDropdown />
                </div>

                {sidebarCollapsed ? (
                  <ArrowForward
                    className={clsx(classes.arrowForward, classes.collapsedArrow)}
                    onClick={handleToggleSidebar}
                  />
                ) : (
                  <ArrowBackIcon className={classes.arrowForward} onClick={handleToggleSidebar} />
                )}
              </div>
            }
            rightColumn={
              <div className={classes.slotsWrapper} ref={headerRowGrowingColumnRef}>
                <div className={classes.monthSlots}>
                  <div className={classes.monthNamesRow}>
                    {calendarDates.map((month) => (
                      <div
                        className={classes.month}
                        style={{
                          width: `${month.dates.length * dayBoxWidth}px`,
                          maxWidth: `${month.dates.length * dayBoxWidth}px`,
                          minWidth: `${month.dates.length * dayBoxWidth}px`,
                          // flex: month.dates.length,
                        }}>
                        {month.monthName}&nbsp;{month.monthYear}
                      </div>
                    ))}
                  </div>
                  <div className={classes.daySlots}>
                    {Object.entries(calendarDatesFlattened).map(([kDay, kObj]) => (
                      <div
                        className={clsx(
                          classes.slot,
                          kObj.isToday && [classes.currentDayHeaderCell]
                        )}
                        style={{
                          width: `${dayBoxWidth}px`,
                          maxWidth: `${dayBoxWidth}px`,
                          minWidth: `${dayBoxWidth}px`,
                          // flex: month.dates.length,
                        }}>
                        {isMonthView ? kObj.dateOfMonth : kDay}
                      </div>
                    ))}
                  </div>
                </div>
              </div>
            }
          />
        </div>
        {/* Resource panel starts here  */}
        <Scrollbars
          autoHide={false}
          autoHeight
          autoHeightMin={100}
          autoHeightMax={"75vh"}
          className="scrollCon">
          <div className={classes.resourcePanel}>{children}</div>
        </Scrollbars>
      </div>
    </>
  );
};

BaseGrid.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
  children: PropTypes.element.isRequired,
};

BaseGrid.defaultProps = { classes: {}, theme: {} };

export default compose(withRouter, withStyles(styles, { withTheme: true }))(BaseGrid);
