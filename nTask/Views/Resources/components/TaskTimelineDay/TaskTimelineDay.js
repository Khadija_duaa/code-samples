// @flow

import React, { memo, useContext } from "react";
import { useSelector } from "react-redux";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import styles from "./styles";
import clsx from "clsx";
import TaskTimelineDayWorkloadInput from "../TaskTimelineDayWorkloadInput";
import { ResourceContext, UnassignedTasksContext } from "../../contexts";
import { CalendarTimelinePeriods } from "../../../../utils/constants/CalendarTimelinePeriods";
import ConditionalTooltip from "../ConditionalTooltip";
import PropTypes from "prop-types";
import { contrastColorFromHex } from "../../temp-utils";
import { selectTaskData, selectTaskTimelineColor } from "../../../../redux/selectors/resources";
import { TaskContext } from "../../contexts/resources";

const TaskTimelineDay = (props) => {
  const {
    classes,
    theme,
    effortInHours,
    isOffDay = false,
    boxWidth,
    isNewLastDay = false,
    newLastDayWidth = 0,
    resetBorderRadius = false,
    // dayIndex,
    updateDayWorkload,
    enableTimelineDrag,
    disableTimelineDrag,
    isNewBox = false,
  } = props;

  const { isUnassignedTask = false } = useContext(UnassignedTasksContext) || {};
  const resourceId = useContext(ResourceContext);
  const taskId = useContext(TaskContext);

  const calendarTimelinePeriod = useSelector((s) => s.resources.filters.calendarTimelinePeriod);
  const dayBoxWidth = useSelector((s) => s.resources.uiGrid.dayProgressHourglassWidth);

  const taskData = useSelector((s) =>
    selectTaskData(s, {
      isUnassignedTask,
      resourceId,
      taskId,
    })
  );
  const { priority: taskPriority, status: taskStatus, color: taskColor } = taskData || {};

  const taskTimelineColor = useSelector((s) =>
    selectTaskTimelineColor(s, {
      theme,
      taskPriority,
      taskStatusColor: taskStatus?.color,
      taskColor,
    })
  );

  if (!taskData) {
    return <></>;
  }

  const isMonthView = calendarTimelinePeriod === CalendarTimelinePeriods.month.value;

  const isMaturedBox = (boxWidth / dayBoxWidth) * 100 > (isMonthView ? 80 : 50);

  // const newBoxWidthPercentage = !isNewLastDay
  //   ? 0
  //   : Math.round((newLastDayWidth / dayBoxWidth) * 100);

  // console.info("eeeee newBoxWidthPercentage:", newBoxWidthPercentage, dayBoxWidth);

  const onWorkloadChange = (newHoursValue) => {
    const hours = Number(newHoursValue);

    if (newHoursValue.length === 0 || newHoursValue === effortInHours) {
      return;
    }
    updateDayWorkload(hours);
  };

  const onWorkloadBlur = (newHoursValue) => {
    const hours = Number(newHoursValue);

    enableTimelineDrag();

    // if (newHoursValue === effortInHours) {
    //   return;
    // }
  };

  const onWorkloadEdit = () => disableTimelineDrag();

  // TODO: off day is not editable
  return (
    <>
      <div
        className={clsx([classes.timelineDay], {
          // [`${classes.workDay} ${classes.editable}`]: !isOffDay,
          [classes.workDay]: !isOffDay,
          [classes.offDay]: isOffDay,
          // [classes.defaultTaskTimelineColor]: !taskTimelineColor,
        })}
        style={{
          ...(taskTimelineColor && { background: taskTimelineColor }),
          // minWidth: dayBoxWidth,
          // width: isNewLastDay ? newLastDayWidth : dayBoxWidth,
          width: boxWidth,
          ...(resetBorderRadius && { borderRadius: "inherit !important" }),
        }}>
        <span
          className={clsx({
            [classes.editable]: !isOffDay && !isUnassignedTask,
          })}>
          {/* {isNewDay && newBoxWidthPercentage > 30 && <>{!isOffDay && effortInHours}</>} */}
          {/* {!isOffDay && effortInHours} */}
          {/* NOTE: below is backup line */}
          {/* {!isNewLastDay && !isOffDay && effortInHours} */}
          {/* {console.info('effortInHours:', effortInHours)} */}
          {isMaturedBox && !isOffDay && !isUnassignedTask && (
            <>
              <ConditionalTooltip
                condition={isUnassignedTask}
                title="Assign task to a resource to update workload">
                <TaskTimelineDayWorkloadInput
                  textStyles={{
                    color: contrastColorFromHex(taskTimelineColor),
                  }}
                  placeholder="Day hours"
                  actualCapacity={effortInHours || 0}
                  onInputChange={onWorkloadChange}
                  onInputBlur={onWorkloadBlur}
                  permission={!isUnassignedTask}
                  boxWidth={boxWidth}
                  onEdit={onWorkloadEdit}
                />
              </ConditionalTooltip>
            </>
          )}
          {/* {isNewLastDay && newBoxWidthPercentage > 30 && effortInHours} */}
          {!isMaturedBox && (effortInHours === undefined || isUnassignedTask) && effortInHours}
          {/* {((!isMaturedBox && effortInHours === undefined) || !isUnassignedTask) && (
            <ConditionalTooltip
              condition={isUnassignedTask}
              title="Assign task to a resource to update workload">
              {effortInHours || ''}
            </ConditionalTooltip>
          )} */}
          {/* {!isMaturedBox && effortInHours === undefined && (
            <ConditionalTooltip
              condition={isUnassignedTask}
              title="Assign task to a resource to update workload">
              {effortInHours || 0}
            </ConditionalTooltip>
          )} */}
          {/* {((!isMaturedBox && effortInHours === undefined) || isUnassignedTask) && effortInHours} */}
        </span>
      </div>
    </>
  );
};

TaskTimelineDay.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
  effortInHours: PropTypes.number,
  isOffDay: PropTypes.bool,
  boxWidth: PropTypes.number,
  isNewLastDay: PropTypes.bool,
  newLastDayWidth: PropTypes.number,
  resetBorderRadius: PropTypes.bool,
  // dayIndex,
  updateDayWorkload: PropTypes.func,
  enableTimelineDrag: PropTypes.func,
  disableTimelineDrag: PropTypes.func,
  isNewBox: PropTypes.bool,
};

TaskTimelineDay.defaultProps = {
  /** default props , calls when props are not coming from parent class undefined */
  classes: {},
  theme: {},
  effortInHours: 0,
  isOffDay: false,
  isNewLastDay: false,
  updateDayWorkload: () => {},
  enableTimelineDrag: () => {},
  disableTimelineDrag: () => {},
  isNewBox: false,
};

// const propsAreEqual = (prevProps, nextProps) => {
//   return prevProps.effortInHours === nextProps.effortInHours;
// };

const WithInjectedHOCs = compose(
  withRouter,
  withStyles(styles, { withTheme: true })
)(TaskTimelineDay);

const Memoized = memo(WithInjectedHOCs);

export default Memoized;
