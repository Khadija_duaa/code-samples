const taskTimelineDayStyles = (theme) => ({
  // here objects of styles
  timelineDay: {
    textAlign: "center",
    boxSizing: "border-box",
    height: "42px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",

    // transition: "250ms all ease-in-out",
    transition: "background-color 250ms ease-in-out",

    // border: "1px solid",
    // borderLeft: 0,
    // borderRight: 0,

    // Note: sequence mattered here, (browser checks inverse of the below sequence), i.e. browser checks only-of-type at first and last-of-type at end
    "&:last-of-type": {
      borderRadius: "0 6px 6px 0",
      // border: '1px solid'
      // borderRight: "1px solid",
      // borderLeft: 0,
    },
    "&:first-of-type": {
      borderRadius: "6px 0 0 6px",
      // borderRight: 0,
      // borderLeft: "1px solid",
    },
    "&:only-of-type": {
      borderRadius: 6,
      // border: '1px solid'
    },
  },
  workDay: {
    // background: "#00ABED",
  },
  offDay: {
    // background: "#00ABED",
    // minWidth: 99,
    // padding: 5,
    // borderRadius: 5,
  },
  editable: {
    cursor: "text",
  },
  defaultTaskTimelineColor: {
    background: theme.palette.workload.taskTimelineDefaultColor,
    borderColor: theme.palette.border.lightBorder,
    borderStyle: 'solid',
    borderWidth: 1,
    // border: '1px solid',
    // set font color to white/black
  },
});

export default taskTimelineDayStyles;
