// @flow

import React from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import styles from "./styles";
import PropTypes from "prop-types";

const EmptyRow = (props) => {
  const { classes, children } = props;

  return <div className={classes.emptyRow}>{children}</div>;
};

EmptyRow.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
  children: PropTypes.element.isRequired,
};

EmptyRow.defaultProps = { classes: {}, theme: {} };

export default compose(withRouter, withStyles(styles, { withTheme: true }))(EmptyRow);
