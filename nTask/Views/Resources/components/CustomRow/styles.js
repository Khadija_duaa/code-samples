const customRowStyles = (theme) => ({
  customRow: {
    display: "flex",
    borderBottom: "1px solid #DDDDDD",
    // "zIndex": "2",
    position: "relative",
    background: "#fff",
  },
  taskRowHeight: {
    minHeight: 60,
  },
});

export default customRowStyles;
