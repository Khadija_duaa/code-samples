// @flow

import React from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import styles from "./styles";
import PropTypes from "prop-types";

const CustomRow = (props) => {
  const { classes, theme, isTaskRow } = props;

  return (
    <>
      <div className={`${classes.customRow} ${isTaskRow ? classes.taskRowHeight : ""}`}>
        {props.children}
      </div>
    </>
  );
};

CustomRow.propTypes = {
  children: PropTypes.element.isRequired,
};

CustomRow.defaultProps = {
  classes: {},
  theme: {},
};

export default compose(withRouter, withStyles(styles, { withTheme: true }))(CustomRow);
