import ConditionalTooltip from "./ConditionalTooltip";

export { default as ConditionalTooltip } from "./ConditionalTooltip";
export * from "./ConditionalTooltip";

export default ConditionalTooltip;
