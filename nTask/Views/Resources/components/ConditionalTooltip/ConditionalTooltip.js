// @flow

import React, { memo, useState, useEffect, Fragment, useRef } from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import styles from "./styles";
import PropTypes from "prop-types";
import ConditionalGenericWrapper from "./../ConditionalGenericWrapper";
import CustomTooltip from "../../../../components/Tooltip/Tooltip";
import { Tooltip } from "@material-ui/core";

function ConditionalTooltip(props) {
  const { classes, theme, children, condition, placement, title } = props;

  // return <Tooltip {...{ placement, title }}>{children}</Tooltip>;

  return (
    <ConditionalGenericWrapper
      condition={condition}
      wrapper={(content) => (
        <CustomTooltip {...{ placement, helptext: title }}>{content}</CustomTooltip>
        // <Tooltip {...{ placement, helptext: title }}>{content}</Tooltip>
        // <div className='ffff' >{content}</div>
      )}>
      {children}
    </ConditionalGenericWrapper>
  );
}

ConditionalTooltip.propTypes = {
  children: PropTypes.node,
  condition: PropTypes.bool,
  placement: PropTypes.string,
  title: PropTypes.string,
};

ConditionalTooltip.defaultProps = {
  classes: {},
  theme: {},
  children: <></>,
  condition: false,
  placement: "top",
  title: "",
};

const WithInjectedHOCs = compose(
  withRouter,
  withStyles(styles, { withTheme: true })
)(ConditionalTooltip);
const Memoized = memo(WithInjectedHOCs);

export default Memoized;
