// @flow

import React, { Component } from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import styles from "./styles";
import PropTypes from "prop-types";

class ErrorBoundary extends Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false, error: null };
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.

    return { hasError: true, error };
  }

  componentDidCatch(error, errorInfo) {
    console.error("error-boundary error:", error, errorInfo);
  }

  render() {
    // const { classes, theme, children } = this.props;

    if (this.state.hasError) {
      // You can render any custom fallback UI
      return <h1 className={this.props.classes.font}>Something went wrong.</h1>;
    }

    return this.props.children;
  }
}

ErrorBoundary.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
  children: PropTypes.element.isRequired,
};

ErrorBoundary.defaultProps = { classes: {}, theme: {} };

export default compose(withRouter, withStyles(styles, { withTheme: true }))(ErrorBoundary);
