import ErrorBoundary from "./ErrorBoundary";

export { default as ErrorBoundary } from "./ErrorBoundary";
export * from "./ErrorBoundary";

export default ErrorBoundary;
