import React from "react";
import PropTypes from "prop-types";

const propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
  
  leftColumn: PropTypes.element.isRequired,
  rightColumn: PropTypes.element.isRequired,
  leftColumnCollapsed: PropTypes.bool,

  leftColumnMinWidth: PropTypes.number,
  leftColumnMinWidthCollapsed: PropTypes.number,
  leftColumnMinWidthUnit: PropTypes.string,

  leftColumnFlexWidth: PropTypes.number,
  leftColumnFlexWidthCollapsed: PropTypes.number,
  leftColumnFlexWidthUnit: PropTypes.string,
};

const defaultProps = {
  /** default props , calls when props are not coming from parent class undefined */
  classes: {},
  theme: {},
  leftColumn: <></>,
  rightColumn: <></>,
  leftColumnCollapsed: false,

  // rightColumnWidth: 0,
  // rightColumnWidthCollapsed: 0,
};

export { propTypes, defaultProps };
