// @flow

import React from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import styles from "./styles";
import StickyColumn from "../StickyColumn";
import GrowingColumn from "../GrowingColumn";
import CustomRow from "../CustomRow";
import { defaultProps, propTypes } from "./props";

const RowWithColumns = (props) => {
  const {
    leftColumnCollapsed,

    leftColumnMinWidth,
    leftColumnMinWidthCollapsed,
    leftColumnMinWidthUnit,

    leftColumnFlexWidth,
    leftColumnFlexWidthCollapsed,
    leftColumnFlexWidthUnit,
  } = props;

  return (
    <>
      <CustomRow isTaskRow={props.isTaskRow}>
        <StickyColumn
          {...{
            collapsed: leftColumnCollapsed,

            expandedMinWidth: leftColumnMinWidth,
            collapsedMinWidth: leftColumnMinWidthCollapsed,
            minWidthUnit: leftColumnMinWidthUnit,

            expandedFlexWidth: leftColumnFlexWidth,
            collapsedFlexWidth: leftColumnFlexWidthCollapsed,
            flexWidthUnit: leftColumnFlexWidthUnit,
          }}>
          {props.leftColumn}
        </StickyColumn>
        <GrowingColumn>{props.rightColumn}</GrowingColumn>
      </CustomRow>
    </>
  );
};

RowWithColumns.propTypes = propTypes;

RowWithColumns.defaultProps = defaultProps;

export default compose(withRouter, withStyles(styles, { withTheme: true }))(RowWithColumns);
