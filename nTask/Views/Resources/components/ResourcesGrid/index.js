import ResourcesGrid from "./ResourcesGrid";

export { default as ResourcesGrid } from "./ResourcesGrid";
export * from "./ResourcesGrid";

export default ResourcesGrid;
