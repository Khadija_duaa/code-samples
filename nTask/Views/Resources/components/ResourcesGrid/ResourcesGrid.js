// @flow
// [@Rizwan-@TODO-@PENDING] : add code commenting (Date : 2-23-2023)
import React, { useState, useEffect, memo, useContext } from "react";
import { useSelector } from "react-redux";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import styles from "./styles";
import ResourceRow from "../ResourceRow";
import { withSnackbarNotifs } from "../../../../hoc";
import NoRecordsRow from "../NoRecordsRow";
import {
  resetGridResourcesDataListExistsAction,
  unsetGridProjectResourcesDataListExistsAction,
  resetGridResourceTasksDataListExistsAction,
  resetGridProjectResourceTasksDataListExistsAction,
} from "./../../../../redux/actions/resources";
import {
  fetchGridProjectResourcesDataListThunk,
  fetchGridResourcesDataListThunk,
} from "./../../../../redux/thunks/resources";
import { useDispatch } from "react-redux";
import LoadingRecordsRow from "../LoadingRecordsRow";
import {
  selectIfProjectsView,
  selectGridResourcesListExists,
  selectGridResourcesList,
  selectGridResourcesListRecordsCount,
} from "../../../../redux/selectors/resources";
import { ProjectContext } from "../../contexts";
import UnassignedTasksRow from "./../UnassignedTasksRow";
import { resetUnassignedTasksDataListExistsAction } from "./../../../../redux/actions/resources";
import { DragDropContext } from "react-beautiful-dnd";
import PropTypes from "prop-types";

export const PAGE_SIZE = 40;

const ResourcesGrid = (props) => {
  const { showSnackbar } = props;

  const dispatch = useDispatch();

  const projectId = useContext(ProjectContext);

  const [resourcesList, setResourcesList] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPageNo, setCurrentPageNo] = useState(1);

  const dataExists = useSelector((s) => selectGridResourcesListExists(s, { projectId }));
  const totalRecordsCount = useSelector((s) =>
    selectGridResourcesListRecordsCount(s, { projectId })
  );
  // const resourcesDataList = useSelector((s) => s.resources.data.resourcesDataList);
  const resourcesDataList = useSelector((s) => selectGridResourcesList(s, { projectId }));
  const timelinePeriod = useSelector((s) => s.resources.filters.calendarTimelinePeriod);
  const workloadViewType = useSelector((s) => s.resources.uiGrid.workloadViewType);
  const showProjectResources = useSelector(selectIfProjectsView);

  const totalPagesCount = Math.ceil(totalRecordsCount / PAGE_SIZE);
  // const nextPageNo = pageNo < totalPages ? pageNo + 1 : totalPages;

  useEffect(() => {
    // setLoading(true);
    // dispatch(resetGridDataActionCreator());
    // dispatch(resetGridResourcesDataListAction());
  }, []);

  useEffect(() => {
    // setLoading(!resourcesDataList || resourcesDataList.length <= 0);

    // setLoading(true);
    setResourcesList(resourcesDataList);
    // setLoading(false);
  }, [resourcesDataList]);

  const apiCallFailure = (error) =>
    showSnackbar(error.data?.message || ((error.status || error.statusText) && `${error.status}: ${error.statusText}`) || "Backend service seems offline!", "error");

  const apiCallFinally = () => setLoading(false);

  const fetchResourcesNextPage = () => {
    if (currentPageNo < totalPagesCount) {
      const nextPageNumber = currentPageNo + 1;
      setCurrentPageNo(nextPageNumber);

      if (showProjectResources) {
        // dispatch(
        //   fetchGridProjectResourcesDataListThunk({ projectId, finallyCallback: apiCallFinally })
        // );
      } else {
        dispatch(
          fetchGridResourcesDataListThunk({
            pageNo: nextPageNumber,
            pageSize: PAGE_SIZE,
            failureCallback: apiCallFailure,
            finallyCallback: apiCallFinally,
          })
        );
      }
    }
  };

  const fetchDataFromApi = () => {
    if (!dataExists) {
      setLoading(true);
      if (showProjectResources) {
        dispatch(
          fetchGridProjectResourcesDataListThunk({
            projectId,
            finallyCallback: apiCallFinally,
          })
        );
      } else {
        dispatch(
          fetchGridResourcesDataListThunk({
            pageNo: currentPageNo,
            pageSize: PAGE_SIZE,
            failureCallback: apiCallFailure,
            finallyCallback: apiCallFinally,
          })
        );
      }
    }
  };

  useEffect(() => {
    if (showProjectResources) {
      dispatch(unsetGridProjectResourcesDataListExistsAction(projectId));
      dispatch(resetGridProjectResourceTasksDataListExistsAction(projectId));
    } else {
      dispatch(resetGridResourcesDataListExistsAction());
      dispatch(resetGridResourceTasksDataListExistsAction());

      dispatch(resetUnassignedTasksDataListExistsAction());
    }
  }, [timelinePeriod]);

  useEffect(() => {
    // dispatch(resetGridResourcesDataListAction());

    fetchDataFromApi();
  }, [workloadViewType, timelinePeriod, projectId, showProjectResources, dataExists]);

  if (loading) {
    return <LoadingRecordsRow />;
  }

  if (!resourcesList || resourcesList.length <= 0) {
    return <NoRecordsRow />;
  }

  return (
    <>
      {resourcesList.map((resource, i) => {
        // TODO: comment this out, this is to render only 1 resource record
        // if (i > 0) {
        //   return;
        // }
        return resource && <ResourceRow resourceInfo={resource} />;
      })}
      <UnassignedTasksRow />
    </>
  );
};

ResourcesGrid.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
  showSnackbar: PropTypes.func,
};

ResourcesGrid.defaultProps = {
  classes: {},
  theme: {},
};

const WithInjectedHOCs = compose(
  withRouter,
  withSnackbarNotifs,
  withStyles(styles, { withTheme: true })
)(ResourcesGrid);

const Memoized = memo(WithInjectedHOCs);

export default Memoized;
