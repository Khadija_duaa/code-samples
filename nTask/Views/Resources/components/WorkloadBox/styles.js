const workloadStyles = (theme) => ({
  workloadParent: {
    height: "100%",
    // height: "90px",
    // minWidth: "54px",
    width: "100%",
    borderRadius: "4px",
    padding: "4px",
    position: "relative",
    overflow: "hidden",
  },
  hoursUtilized: {
    fontFamily: "lato",
    fontWeight: "bold",
    fontSize: "14px",
    position: "absolute",
    top: "4px",
    left: "4px",
    zIndex: 2,
  },
  loadUtilized: {
    position: "absolute",
    bottom: "0",
    left: "0",
    width: "100%",
    transition: "300ms all ease-in-out",
  },
  unitText: {
    display: "inline-block",
  },
  oneMonth: {
    fontSize: 12,
  },
});

export default workloadStyles;
