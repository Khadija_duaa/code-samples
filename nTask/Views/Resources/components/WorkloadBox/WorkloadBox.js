// @flow

import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import PropTypes from "prop-types";
import styles from "./styles";
import { ResourceWorkloadUnits } from "../../../../utils/constants/ResourceWorkloadUnits";
import { CalendarTimelinePeriods } from "../../../../utils/constants/CalendarTimelinePeriods";
import { clsx } from "clsx";

const WorkloadBox = (props) => {
  const { classes, theme, capacity, load, isOffDay } = props;

  const [progress, setProgress] = useState(0);
  const [fillColor, setFillColor] = useState(theme.palette.workload.capacityUtilized);
  const [textColor, setTextColor] = useState(theme.palette.workload.fontColor);
  const [textValue, setTextValue] = useState("");
  const [unitText, setUnitText] = useState(ResourceWorkloadUnits.hour.value);

  const unit = useSelector((s) => s.resources.filters.progressHourglassUnit);
  const calendarTimelinePeriod = useSelector((s) => s.resources.filters.calendarTimelinePeriod);

  const isMonthView = calendarTimelinePeriod === CalendarTimelinePeriods.month.value;

  useEffect(() => {
    makeCalculationsByUnit();
  }, [load, capacity, unit, isOffDay]);

  const makeCalculationsByUnit = () => {
    const workload = load / parseFloat(capacity) || 0;
    const _progressPercentage = workload * 100;

    setProgress(_progressPercentage >= 100 ? 100 : _progressPercentage);

    switch (unit) {
      case ResourceWorkloadUnits.hour.value:
        setTextValue(load.toFixed(1));
        setUnitText(ResourceWorkloadUnits.hour.symbol);
        break;
      case ResourceWorkloadUnits.percentage.value:
        setTextValue(Math.round(_progressPercentage));
        setUnitText(ResourceWorkloadUnits.percentage.symbol);
        break;
      case ResourceWorkloadUnits.fullTimeEquivalents.value:
        setUnitText(ResourceWorkloadUnits.fullTimeEquivalents.symbol);
        setTextValue(workload.toFixed(1));
        break;
      default:
        setTextValue(load);
        setUnitText(ResourceWorkloadUnits.hour.symbol);
        break;
    }

    const selectedFillColor =
      _progressPercentage > 100
        ? theme.palette.workload.overload
        : theme.palette.workload.capacityUtilized;
    setFillColor(selectedFillColor);

    const selectedFontColor =
      _progressPercentage > 100
        ? theme.palette.workload.overloadFontColor
        : theme.palette.workload.fontColor;
    setTextColor(selectedFontColor);
  };

  return (
    <>
      <div
        className={classes.workloadParent}
        style={{
          backgroundColor: isOffDay
            ? theme.palette.workload.offDay
            : theme.palette.workload.availableCapacity,
        }}>
        {progress !== 0 && (
          <Typography className={classes.hoursUtilized} variant="h5" style={{ color: textColor }}>
            {textValue}
            <Typography
              variant="span"
              className={clsx(classes.unitText, isMonthView && classes.oneMonth)}>
              {unitText}
            </Typography>
          </Typography>
        )}
        <div
          className={classes.loadUtilized}
          style={{ backgroundColor: fillColor, height: `${progress}%` }}></div>
      </div>
    </>
  );
};

WorkloadBox.propTypes = {
  classes: PropTypes.any,
  theme: PropTypes.any,
  capacity: PropTypes.number.isRequired,
  load: PropTypes.number.isRequired,
  isOffDay: PropTypes.bool,
};

WorkloadBox.defaultProps = {
  classes: {},
  theme: {},
  isOffDay: false,
};

export default compose(withRouter, withStyles(styles, { withTheme: true }))(WorkloadBox);
