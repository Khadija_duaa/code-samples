import WorkloadBox from "./WorkloadBox";

export { default as WorkloadBox } from "./WorkloadBox";
export * from "./WorkloadBox";

export default WorkloadBox;
