import ResourceTasks from "./ResourceTasks";

export { default as ResourceTasks } from "./ResourceTasks";
export * from "./ResourceTasks";

export default ResourceTasks;
