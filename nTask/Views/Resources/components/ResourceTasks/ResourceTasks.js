// @flow

import React, { useState, useEffect, memo, useContext } from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import styles from "./styles";
import clsx from "clsx";
import { withSnackbarNotifs } from "../../../../hoc";
import { Droppable } from "react-beautiful-dnd";
import TasksList from "../TasksList";
import { useSelector } from "react-redux";
import LoadingRecordsRow from "../LoadingRecordsRow";
import {
  selectIfProjectsView,
  selectGridResourceTasksListExists,
  selectGridResourceTasksList,
} from "../../../../redux/selectors/resources";
import { useDispatch } from "react-redux";
import { fetchResourceTasksDataListThunk } from "../../../../redux/thunks/resources";
import { ProjectContext, ResourceContext } from "../../contexts";
import PropTypes from "prop-types";

const ResourceTasks = (props) => {
  const { classes, showSnackbar } = props;

  const dispatch = useDispatch();

  const projectId = useContext(ProjectContext);
  const resourceId = useContext(ResourceContext);

  const [tasksList, setTasksList] = useState([]);
  const [loading, setLoading] = useState(false);

  const dataExists = useSelector((s) =>
    selectGridResourceTasksListExists(s, { resourceId, projectId })
  );
  const resourceTasksDataList = useSelector((s) =>
    selectGridResourceTasksList(s, { resourceId, projectId })
  );
  const timelinePeriod = useSelector((s) => s.resources.filters.calendarTimelinePeriod);
  const showProjectTasks = useSelector(selectIfProjectsView);

  useEffect(() => {
    // setLoading(!resourcesDataList || resourcesDataList.length <= 0);

    setLoading(true);
    setTasksList(resourceTasksDataList);
    setLoading(false);
  }, [resourceTasksDataList]);

  const apiCallFailure = (error) =>
    showSnackbar(error.data?.message || ((error.status || error.statusText) && `${error.status}: ${error.statusText}`) || "Backend service seems offline!", "error");

  const apiCallFinally = () => setLoading(false);

  const fetchDataFromApi = () => {
    if (!dataExists) {
      setLoading(true);
      if (showProjectTasks) {
        // dispatch(
        //   fetchResourceTasksDataListThunk(
        //     "6375df71d15b9077d98c98a4",
        //     apiCallSuccess,
        //     apiCallFailure,
        //     apiCallFinally
        //   )
        // );
      } else {
        dispatch(
          fetchResourceTasksDataListThunk({
            resourceId,
            failureCallback: apiCallFailure,
            finallyCallback: apiCallFinally,
          })
        );
      }
    }
  };

  // useEffect(() => {
  //   if (showProjectTasks) {
  //     dispatch(resetGridProjectResourceTasksDataListExistsAction(projectId));
  //   } else {
  //     dispatch(resetGridResourceTasksDataListExistsAction());
  //   }
  // }, [timelinePeriod]);

  useEffect(() => {
    fetchDataFromApi();
  }, [resourceId, timelinePeriod, projectId, dataExists]);

  if (loading) {
    return <LoadingRecordsRow />;
  }

  return (
    <>
      <Droppable droppableId={`droppable-resource-${resourceId}`}>
        {(provided, snapshot) => {
          return (
            <div
              className={clsx("resource-tasks-container", classes.resourceTasksContainer)}
              ref={provided.innerRef}
              {...provided.droppableProps}
              style={{
                backgroundColor: snapshot.isDraggingOver ? "skyblue" : "white",
              }}>
              <TasksList tasks={tasksList} isDraggingOver={snapshot.isDraggingOver} />
              {provided.placeholder}
            </div>
          );
        }}
      </Droppable>
    </>
  );
};

ResourceTasks.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
  showSnackbar: PropTypes.func,
};

ResourceTasks.defaultProps = {
  classes: {},
  theme: {},
};

const WithInjectedHOCs = compose(
  withRouter,
  withSnackbarNotifs,
  withStyles(styles, { withTheme: true })
)(ResourceTasks);

const Memoized = memo(WithInjectedHOCs);

export default Memoized;
