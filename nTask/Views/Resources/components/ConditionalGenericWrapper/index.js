import ConditionalGenericWrapper from "./ConditionalGenericWrapper";

export { default as ConditionalGenericWrapper } from "./ConditionalGenericWrapper";
export * from "./ConditionalGenericWrapper";

export default ConditionalGenericWrapper;
