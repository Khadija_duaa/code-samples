// @flow

import React, { memo } from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import styles from "./styles";
import PropTypes from "prop-types";

function ConditionalGenericWrapper(props) {
  const { children, condition, wrapper } = props;

  if (condition && wrapper) {
    return wrapper(children);
  }
  return children;
}

ConditionalGenericWrapper.propTypes = {
  children: PropTypes.node,
  condition: PropTypes.bool,
  wrapper: PropTypes.func,
};

ConditionalGenericWrapper.defaultProps = {
  classes: {},
  theme: {},
  children: <></>,
  condition: false,
  wrapper: null,
};

const WithInjectedHOCs = compose(
  withRouter,
  withStyles(styles, { withTheme: true })
)(ConditionalGenericWrapper);
const Memoized = memo(WithInjectedHOCs);

export default Memoized;
