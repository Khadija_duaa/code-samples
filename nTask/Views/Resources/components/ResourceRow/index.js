import ResourceRow from "./ResourceRow";

export { default as ResourceRow } from "./ResourceRow";
export * from "./ResourceRow";

export default ResourceRow;
