const resourceRowStyles = (theme) => ({
  rcCard: {
    cursor: "pointer",
    width: "250px",
    minHeight: "98px",
    position: "relative",
    padding: "13px",
    display: "flex",
    maxHeight: "100%",
    gap: "5px",
    "& p": {
      margin: 0,
      padding: 10,
    },
  },
  rcTimeline: {
    // no styles
    height: "100%",
  },
  progressWrapper: {
    display: "flex",
    height: "100%",
  },
  dayProgress: {
    padding: 5,
    boxSizing: "border-box",
    borderLeft: "1px solid #DDDDDD",
    "&:first-child": {
      borderLeft: "transparent",
    },
  },
  resourceName: {
    fontFamily: "Lato, Regular",
    fontSize: "14px",
    fontWeight: "400",
    color: "#171717",
    lineHeight: "1.5",
  },
  resourceDesignation: {
    color: "#646464",
    fontSize: "13px",
    fontFamily: "Lato, Regular",
    lineHeight: "1.5",
  },
  resourceCapacity: {
    color: "#7E7E7E",
    fontSize: "13px",
    fontFamily: "Lato, Regular",
    lineHeight: "1.5",
    "& span": {
      color: "#171717",
    },
  },
  rscMediaParent: {
    display: "flex",
    gap: "10px",
    alignItems: "flex-start",
  },
  resourceUtilizationBar: {
    marginTop: 10,
  },
  workLoadProgress: {
    height: "5px",
    borderRadius: "50px",
  },
  toggleIndicator: {
    "& svg": {
      color: "#969696",
      position: "relative",
      top: "5px",
    },
  },
  resourcesDetailsParent: {
    flex: "1",
  },
  rcCardUnassigned: {
    height: "58px",
    maxHeight: "58px",
  },
  loadingWorkloads: {
    display: "inline-flex",
    alignSelf: "center",
    paddingLeft: 30,
    color: theme.palette.text.grayDarker,
  },
});

export default resourceRowStyles;
