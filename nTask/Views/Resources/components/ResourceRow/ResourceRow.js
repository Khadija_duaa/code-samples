// @flow

import React, { useRef, memo, useContext } from "react";
import { useSelector } from "react-redux";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import styles from "./styles";
import clsx from "clsx";
import Collapsible from "../Collapsible";
import WorkloadBox from "../WorkloadBox";
import ResourceCard from "../ResourceCard";
import GridRow from "../GridRow";
import ResourceTasks from "../ResourceTasks";
import { ProjectContext, ResourceContext } from "../../contexts";
import { selectGridResourceDailyWorkloads } from "../../../../redux/selectors/resources";
import PropTypes from "prop-types";

// TODO: we should implement lazy loading for speedy app, i.e. loading components on-demand (when need to display)
// const ResourceTasks = React.lazy(() => import("../ResourceTasks/ResourceTasks"));

const ResourceRow = (props) => {
  const { classes, resourceInfo } = props;
  const { dailyWorkload, ...resource } = resourceInfo;

  const projectId = useContext(ProjectContext);

  const rowCollapseToggleBtnRef = useRef(null);

  const gridColumnsCount = useSelector((s) => s.resources.uiGrid.gridColumnsCount);
  const timelinePeriod = useSelector((s) => s.resources.filters.calendarTimelinePeriod);
  const dayBoxWidth = useSelector((s) => s.resources.uiGrid.dayProgressHourglassWidth);

  const computedDailyWorkload = useSelector((s) =>
    selectGridResourceDailyWorkloads(s, { resourceId: resource.id, projectId })
  );

  return (
    <>
      <Collapsible
        collapseBtnRef={rowCollapseToggleBtnRef}
        summaryRow={(isExpanded) => (
          <GridRow
            leftColumn={
              <div className={clsx(classes.rcCard)} ref={rowCollapseToggleBtnRef}>
                <ResourceCard data={resource} isExpanded={isExpanded} />
              </div>
            }
            rightColumn={
              <div className={classes.rcTimeline}>
                <div className={classes.progressWrapper}>
                  {gridColumnsCount === computedDailyWorkload.length ? (
                    computedDailyWorkload.map((day) => {
                      const { load, capacity, offDay } = day;

                      return (
                        <div
                          className={classes.dayProgress}
                          style={{
                            width: `${dayBoxWidth}px`,
                            maxWidth: `${dayBoxWidth}px`,
                            minWidth: `${dayBoxWidth}px`,
                            // flex: month.dates.length,
                          }}>
                          <WorkloadBox load={load} capacity={capacity} isOffDay={offDay} />
                        </div>
                      );
                    })
                  ) : (
                    <div className={classes.loadingWorkloads}>Loading...</div>
                  )}
                </div>
              </div>
            }
          />
        )}
        detailsPanel={(isExpanded) => {
          if (!isExpanded) {
            return <></>;
          }

          return (
            <ResourceContext.Provider value={resourceInfo.id}>
              <ResourceTasks />
            </ResourceContext.Provider>
          );
        }}
      />
    </>
  );
};

ResourceRow.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
  resourceInfo: PropTypes.object,
};

ResourceRow.defaultProps = {
  classes: {},
  theme: {},
  resourceInfo: {},
};

const WithInjectedHOCs = compose(withRouter, withStyles(styles, { withTheme: true }))(ResourceRow);

const Memoized = memo(WithInjectedHOCs);

export default Memoized;
