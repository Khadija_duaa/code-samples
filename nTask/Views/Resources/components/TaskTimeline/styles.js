const taskTimelineStyles = (theme) => ({
  // here objects of styles
  timelineDaysWrapper: {
    display: "flex",
    flexDirection: "row",

    borderRadius: 6,
    borderWidth: 0,
    borderStyle: "solid",

    transition: "border-width 70ms ease-in-out, border-color 70ms ease-in-out",

    "&:hover": {
      borderWidth: "2px !important",
    },
  },
  defaultBorder: {
    borderColor: theme.palette.border.lightBorder,
    borderWidth: 1,

    "&:hover": {
      borderColor: `${theme.palette.workload.taskTimelineDefaultBorderColor} !important`,
    },
  },
});

export default taskTimelineStyles;
