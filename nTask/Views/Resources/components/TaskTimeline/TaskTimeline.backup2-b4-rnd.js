import React, { memo, useContext, useEffect, useRef, useState } from "react";
import { connect, useDispatch, useSelector } from "react-redux";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import clsx from "clsx";
import { withRouterAndMaterialThemeStyles } from "../../../../hoc";
import TaskTimelineDay from "../TaskTimelineDay";
import { sortBy } from "lodash";
import Draggable from "react-draggable";
import DragHandleIcon from "@material-ui/icons/DragHandle";
import styles from "./styles";
import { Resizable } from "react-resizable";
import classNames from "classnames";
import { cloneDeep } from "lodash";
import { generateDateRange } from "../../../../helper/dates/dates";
import { daysList, formatMomentToDate } from "./../../temp-utils";
import Popover from "@material-ui/core/Popover";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import { dayComparisonDateFormat } from "./../../constants";
import ErrorBoundary from "../ErrorBoundary";
import TaskTimelineDraggable from "../TaskTimelineDraggable";
import TaskTimelineResizable from "../TaskTimelineResizable";
import useTraceUpdate from "../../../../hooks/useTraceUpdate";
import { ResourceContext } from "../../../../contexts";
import { updateResourceTaskDayWorkloadAction } from "../../../../redux/actions/resources";
import { selectResourceTaskWorkloads } from "../../../../redux/selectors/resources";
import { updateResourceTaskDayWorkloadThunk } from "../../../../redux/thunks/resources";
import { CalendarTimelinePeriods } from "./../../../../utils/constants/CalendarTimelinePeriods";

// // const plannedStartDate = "2022-12-15T02:00:00";
// // const plannedEndDate = "2022-12-27T23:55:00";
// const plannedStartDate = "2022-12-22T02:00:00";
// const plannedEndDate = "2022-12-27T23:55:00";
// const plannedStartDate = "2023-01-10T02:00:00";
// const plannedEndDate = "2023-01-11T23:55:00";

// // case 1:
// const plannedStartDate = "2023-01-10T02:00:00";
// const plannedEndDate = "2023-01-11T23:55:00";

// // case 2:
// const plannedStartDate = "2023-01-10T02:00:00";
// const plannedEndDate = "2023-01-15T23:55:00";

// // case 3:
// const plannedStartDate = "2023-01-09T02:00:00";
// const plannedEndDate = "2023-01-14T23:55:00";

// // case 4:
// const plannedStartDate = "2023-01-07T02:00:00";
// const plannedEndDate = "2023-01-10T23:55:00";

// // case 5:
// const plannedStartDate = "2023-01-13T02:00:00";
// const plannedEndDate = "2023-01-18T23:55:00";

// // case 6:
// const plannedStartDate = "2023-01-06T02:00:00";
// const plannedEndDate = "2023-01-17T23:55:00";

// temporary format
// const format = "YYYY-MM-DD";

export const boxTemplate = {
  dayIndex: -1,
  moment: null,
  effort: 0,
  id: null,
  offDay: false,
  isNewLastBox: false,
  isNewBox: false,
  boxWidth: 0,
  isNewFirstBox: false,
};

const TaskTimeline = (props) => {
  const { classes, theme, startInx, dailyWorkload, taskData } = props;

  const resourceId = useContext(ResourceContext);

  const dispatch = useDispatch();

  // useTraceUpdate(props);
  // console.info("rendered: TaskTimeline");

  const [lockTimelineDrag, setLockTimelineDrag] = useState(false);

  const [anchorEl, setAnchorEl] = useState(null);

  const handlePopoverOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handlePopoverClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);

  const selectedCalendarTimeline = useSelector((s) => s.resources.filters.calendarTimelinePeriod);
  const calendarDatesFlattened = useSelector((s) => s.resources.data.calendarDatesFlattened);
  const dayBoxWidth = useSelector((s) => s.resources.uiGrid.dayProgressHourglassWidth);
  const taskTimelineWorkloads = useSelector((s) =>
    selectResourceTaskWorkloads(s, { resourceId, taskId: taskData.id })
  );

  const isMonthView = selectedCalendarTimeline === CalendarTimelinePeriods.month.value;

  // console.log("eee-eee dayBoxWidth:", dayBoxWidth);
  // console.log("eee-eee start and end dates:", taskData.plannedStartDate, taskData.plannedEndDate);

  const getBoxWidth = (index, listLen) => {
    // const listLen = dailyWorkloadSortedByDayIndex.length;
    // TODO: this is temporarily returning same width for each box even on corners'.
    return dayBoxWidth;
    const isOnly = listLen === 1;
    if (isOnly) {
      return dayBoxWidth - 6;
    }

    const isFirst = index === 0;
    const isLast = listLen - 1 === index;

    if (isFirst || isLast) {
      return dayBoxWidth - 3;
    }
    return dayBoxWidth;
  };

  const timelineDateRange = generateDateRange(taskData.plannedStartDate, taskData.plannedEndDate);
  // const timelineDateRange = generateDateRange(plannedStartDate, plannedEndDate);

  // console.info('timelineDateRange:', timelineDateRange);

  // const taskDailyWorkload = taskData.dailyWorkload;
  // const taskDailyWorkload = taskTimelineWorkloads;
  const taskDailyWorkload =
    taskTimelineWorkloads?.length <= 0 ? taskData.dailyWorkload : taskTimelineWorkloads;

  // console.info('taskDailyWorkload:', taskDailyWorkload);

  const taskDailyWorkloadHashMap = new Map(
    taskDailyWorkload.map((k) => {
      return [k.date, k];
    })
  );

  // console.info('taskDailyWorkloadHashMap:', taskDailyWorkloadHashMap);

  const timelineDateRangeMapped = timelineDateRange.map((k, inx, selfArr) => {
    const date = formatMomentToDate(k);
    const dateData = taskDailyWorkloadHashMap?.has(date)
      ? taskDailyWorkloadHashMap?.get(date)
      : null;

    return {
      ...boxTemplate,

      dayIndex: inx,
      moment: k,
      // effort: 0,
      effort: dateData?.load || 0,
      id: dateData?.id || null,
      offDay: false,
      // NOTE: we can comment out below lines because we need them in only new boxes
      isNewLastBox: false,
      isNewBox: false,
      // boxWidth: dayBoxWidth
      boxWidth: getBoxWidth(inx, selfArr.length),
    };
  });
  // console.info("timelineDateRangeMapped:", cloneDeep(timelineDateRangeMapped));
  // debugger;

  // const dailyWorkloadSortedByDayIndex = sortBy(dailyWorkload, (k) => k.dayIndex);

  const [newLastDayWidth, setNewDayWidth] = useState(0);
  const [timelineDragPosition, setTimelineDragPosition] = useState(0);
  const [timelineDragPositionBackup, setTimelineDragPositionBackup] = useState(0);

  let mapRangeToTimelineSpan = null;
  // const calendarDatesFlattenedSorted = Object.values(calendarDatesFlattened).sort(
  //   (a, b) => a.dateOfMonth - b.dateOfMonth
  // );
  // alread sorted when creating flattened dates, otherwise use moment to sort
  const [calendarDatesFlattenedSorted] = useState(Object.values(calendarDatesFlattened));

  // const dateIndex = calendarDatesFlattenedSorted.findIndex((k) =>
  //   k.momentObj.isSame(timelineDateRangeMapped.at(0), "day")
  // );

  let startDateIndex =
    timelineDateRangeMapped.length > 0 &&
    calendarDatesFlattenedSorted.findIndex((k) =>
      k.momentObj.isSame(timelineDateRangeMapped.at(0).moment, "day")
    );

  if (startDateIndex === -1 && timelineDateRangeMapped.length > 0) {
    startDateIndex = timelineDateRangeMapped.findIndex((k) =>
      k.moment.isSame(calendarDatesFlattenedSorted.at(0).momentObj, "day")
    );

    startDateIndex *= -1;
  }
  let endDateIndex =
    timelineDateRangeMapped.length > 0 &&
    calendarDatesFlattenedSorted.findIndex((k) =>
      k.momentObj.isSame(timelineDateRangeMapped.at(-1).moment, "day")
    );

  // if (endDateIndex === -1 && timelineDateRangeMapped.length > 0) {
  //   debugger;
  //   endDateIndex = timelineDateRangeMapped.findIndex((k) =>
  //     k.moment.isSame(calendarDatesFlattenedSorted.at(-1).momentObj, "day")
  //   );

  //   endDateIndex *= -1;
  // }

  const startsBeforeTimespan =
    timelineDateRangeMapped.length > 0 &&
    calendarDatesFlattenedSorted.every((k) =>
      timelineDateRangeMapped.at(0).moment.isBefore(k.momentObj, "day")
    );
  const endsAfterTimespan =
    timelineDateRangeMapped.length > 0 &&
    calendarDatesFlattenedSorted.every((k) =>
      timelineDateRangeMapped.at(-1).moment.isAfter(k.momentObj, "day")
    );
  const startsAfterTimespan =
    timelineDateRangeMapped.length > 0 &&
    calendarDatesFlattenedSorted.every((k) =>
      timelineDateRangeMapped.at(0).moment.isAfter(k.momentObj, "day")
    );
  const endsBeforeTimespan =
    timelineDateRangeMapped.length > 0 &&
    calendarDatesFlattenedSorted.every((k) =>
      timelineDateRangeMapped.at(-1).moment.isBefore(k.momentObj, "day")
    );
  // console.info("gggg---8888", taskData.name, endsBeforeTimespan, startsAfterTimespan);

  // const timelineInitialPosition = startInx < 0 ? 0 : startInx * dayBoxWidth;
  // const timelineInitialPosition = startDateIndex < 0 ? 0 : startDateIndex * dayBoxWidth;
  const timelineInitialPosition = startDateIndex * dayBoxWidth;

  const [dailyWorkloadSortedByDayIndexBackup, setDailyWorkloadSortedByDayIndexBackup] = useState(
    timelineDateRangeMapped.length <= 0 || endsBeforeTimespan || startsAfterTimespan
      ? []
      : timelineDateRangeMapped
  );

  const [dailyWorkloadSortedByDayIndex, setDailyWorkloadSortedByDayIndex] = useState(
    // sortBy(dailyWorkload, (k) => k.dayIndex)
    // timelineDateRangeMapped

    // timelineDateRangeMapped.length <= 0
    //   ? []
    //   : timelineDateRangeMapped.slice(
    //       endsBeforeTimespan || startsAfterTimespan || startsBeforeTimespan
    //         ? 0
    //         : // : // : dailyWorkloadSortedByDayIndex.at(0)?.isNewLastBox
    //           // timelineDateRangeMapped.length > 0
    //           // ? startDateIndex - dailyWorkloadSortedByDayIndex.filter((k) => k.isNewBox).length
    //           startDateIndex,
    //       endsBeforeTimespan || startsAfterTimespan
    //         ? 0
    //         : endDateIndex < 0
    //         ? // ? daysList[selectedCalendarTimeline] - startInx
    //           daysList[selectedCalendarTimeline] - startDateIndex
    //         : endDateIndex + 1
    //     )

    // 2nd mechanism
    timelineDateRangeMapped.length <= 0 || endsBeforeTimespan || startsAfterTimespan
      ? []
      : timelineDateRangeMapped
  );

  useEffect(() => {
    console.info("dailyWorkloadSortedByDayIndex:", dailyWorkloadSortedByDayIndex);
  }, [dailyWorkloadSortedByDayIndex]);

  // useEffect(() => {
  //   if (dailyWorkloadSortedByDayIndex?.length > 0) {
  //     const cloned = cloneDeep(dailyWorkloadSortedByDayIndex);

  //     cloned = cloned.filter((k) => k.isNewBox);

  //     cloned.push({
  //       dayIndex: cloned.length,
  //       moment: null,
  //       effort: 0,
  //       offDay: true,
  //       isNewLastBox: true,
  //       isNewBox: true,
  //       // boxWidth: 0
  //       // boxWidth: getBoxWidth(inx, selfArr.length),
  //     });

  //     setDailyWorkloadSortedByDayIndex();
  //   }
  // }, [dailyWorkloadSortedByDayIndex]);

  const [timelineDateRangeWithoutWeekends] = useState(
    timelineDateRangeMapped
      // const timelineDateRangeWithoutWeekends = timelineDateRangeMapped
      // const timelineDateRangeWithoutWeekends = dailyWorkloadSortedByDayIndex
      // const [timelineDateRangeWithoutWeekends] = useState(dailyWorkloadSortedByDayIndex
      .filter((k) => {
        const dayOfWeek = k.moment?.day();
        // const dayOfWeek = k.day();
        return dayOfWeek !== 0 && dayOfWeek !== 6;
      })
  );

  // console.info(
  //   "timelineDayssss",
  //   timelineDateRangeMapped,
  //   timelineDateRangeMapped.map((k) => k.moment.format(dayComparisonDateFormat)),
  //   timelineDateRangeMapped.map((k) => k.moment.day())
  // );
  // console.info(
  //   "timelineDayssss without weekends ------ 222222",
  //   timelineDateRangeWithoutWeekends,
  //   timelineDateRangeWithoutWeekends.map((k) => k.moment?.format(dayComparisonDateFormat)),
  //   timelineDateRangeWithoutWeekends.map((k) => k.moment?.day())
  // );

  // const [avgDailyLoadHours, setAvgDailyLoadHours] = useState(
  //   0
  //   // (
  //   //   (taskData.estimatedHours * 60 + taskData.estimatedMinutes) /
  //   //   60 /
  //   //   (timelineDateRangeWithoutWeekends.length +
  //   //     dailyWorkloadSortedByDayIndex.filter((k) => k.isNewBox).length)
  //   // ).toFixed(1)
  // );

  // const minTimelineWidth = dailyWorkloadSortedByDayIndex.filter((k) => !k.isNewBox).length * dayBoxWidth;
  // const minTimelineWidth =
  //   (dailyWorkload.length || daysList[selectedCalendarTimeline]) * dayBoxWidth;
  // const minTimelineWidth = dailyWorkloadSortedByDayIndex.length * dayBoxWidth;
  const minTimelineWidth = dailyWorkloadSortedByDayIndex
    .filter((k) => !k.isNewBox)
    .reduce((acc, cur) => acc + cur.boxWidth, 0);

  // let timelineFixedWidth = minTimelineWidth;
  const [timelineFixedWidth, setTimelineFixedWidth] = useState(minTimelineWidth);
  // debugger;
  // useEffect(() => {
  //   console.info("average daily hour", avgDailyLoadHours);
  //   setAvgDailyLoadHours(
  //     (
  //       (taskData.estimatedHours * 60 + taskData.estimatedMinutes) /
  //       60 /
  //       (timelineDateRangeWithoutWeekends.length +
  //         dailyWorkloadSortedByDayIndex.filter((k) => k.isNewBox).length)
  //     ).toFixed(1)
  //   );
  // }, [
  //   avgDailyLoadHours,
  //   dailyWorkloadSortedByDayIndex,
  //   taskData.estimatedHours,
  //   taskData.estimatedMinutes,
  //   timelineDateRangeWithoutWeekends,
  // ]);

  useEffect(() => {
    setTimelineDragPosition(timelineInitialPosition);
    setTimelineDragPositionBackup(timelineInitialPosition);
  }, []);

  // useEffect(() => {
  //   setTimelineDragPosition(timelineInitialPosition);
  // }, [timelineInitialPosition]);

  const updateWorkloadOfDay = (updatedHours, dayIndex, taskDayId = null, isNewBox = false) => {
    const taskId = taskData.id;

    dispatch(updateResourceTaskDayWorkloadAction(resourceId, taskId, dayIndex, updatedHours));

    if (!updatedHours && updatedHours !== 0) {
      return;
    }

    if (isNewBox) {
      // TODO: integrate the API to create estimates
      // dispatch(
      //   updateResourceTaskDayWorkloadThunk({
      //     resourceId,
      //     taskId,
      //     dayIndex,
      //     updatedHours,
      //   })
      // );
    } else {
      // integrate the API to update task day estimates
      dispatch(
        updateResourceTaskDayWorkloadThunk({
          // taskId,
          taskDayId,
          updatedWorkloadHours: updatedHours,
        })
      );
    }
  };

  const timelineDraggableProps = {
    setTimelineDragPosition,
    setTimelineDragPositionBackup,
    timelineInitialPosition,
    dayBoxWidth,
    timelineDragPosition,
    taskId: taskData.id,
    plannedStartDate: taskData.plannedStartDate,
    plannedEndDate: taskData.plannedEndDate,
    lockTimelineDrag,
    noDaysInTimeline: !dailyWorkloadSortedByDayIndex || dailyWorkloadSortedByDayIndex.length <= 0,
  };

  const timelineResizableProps = {
    minTimelineWidth,
    startsBeforeTimespan,
    endsAfterTimespan,
    setTimelineFixedWidth,
    setNewDayWidth,
    boxTemplate,
    setTimelineDragPosition,
    timelineDragPositionBackup,
    dayBoxWidth,
    setDailyWorkloadSortedByDayIndex,
    dailyWorkloadSortedByDayIndexBackup,
    setDailyWorkloadSortedByDayIndexBackup,
    timelineFixedWidth,
    dailyWorkloadSortedByDayIndex,
    timelineDragPosition,
    timelineInitialPosition,
    getBoxWidth,
    taskId: taskData.id,
    plannedStartDate: taskData.plannedStartDate,
    plannedEndDate: taskData.plannedEndDate,
    taskDailyWorkload,
  };

  return (
    <>
      <ErrorBoundary>
        <TaskTimelineDraggable {...timelineDraggableProps}>
          <div className={classes.timelinePlusLabelsContainer}>
            {/* NOTE: this caused timeline slided right, updates the progress houglass 1 box behind the actual on day value change */}
            {/* <div
            style={{
              color: "black",
              // width: "200px"
              minWidth: `${dayBoxWidth}px`,
              maxWidth: `${dayBoxWidth * 2}px`,
            }}>
            {taskData.name}
          </div> */}
            <TaskTimelineResizable {...timelineResizableProps}>
              {/* <Popover
              id="mouse-over-popover"
              open={open}
              anchorEl={anchorEl}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "left",
              }}
              transformOrigin={{
                vertical: "top",
                horizontal: "left",
              }}
              onClose={handlePopoverClose}
              disableRestoreFocus
              className={classes.timeLinePopoverParent}>
              <div className={classes.timelinePopoverBody}>
                <div className={classes.tlPopoverHeader}>
                  <Typography variant="h6" className={classes.projectName}>
                    Research & Analysis
                  </Typography>
                </div>
                <div className={classes.tlPopoverBody}>
                  <Grid container>
                    <Grid item style={{ marginBottom: "16px" }} xs={12}>
                      <ul className={classes.projectHoursList}>
                        <li className={classes.projectHoursItem}>
                          <div className={classes.leftSide}>
                            <Typography variant="h6" className={classes.listLabel}>
                              Duration
                            </Typography>
                          </div>
                          <div className={classes.rightSide}>
                            <input type="text" className={classes.projectHours} value={12} />
                            <Typography variant="h6" className={classes.unit}>
                              day(s)
                            </Typography>
                          </div>
                        </li>
                        <li className={classes.projectHoursItem}>
                          <div className={classes.leftSide}>
                            <Typography variant="h6" className={classes.listLabel}>
                              Effort
                            </Typography>
                          </div>
                          <div className={classes.rightSide}>
                            <input type="text" className={classes.projectHours} value={12} />
                            <Typography variant="h6" className={classes.unit}>
                              hour(s)
                            </Typography>
                          </div>
                        </li>
                      </ul>
                    </Grid>
                  </Grid>
                </div>
              </div>
            </Popover> */}
              {/* {console.info(
              "111",
              endsBeforeTimespan || startsAfterTimespan
                ? 0
                : // : dailyWorkloadSortedByDayIndex.at(0)?.isNewLastBox
                dailyWorkloadSortedByDayIndex.filter((k) => k.isNewBox).length > 0
                ? startInx - dailyWorkloadSortedByDayIndex.filter((k) => k.isNewBox).length
                : startInx
            )} */}
              {/* {console.info(
              "222",
              endsBeforeTimespan || startsAfterTimespan
                ? 0
                : endDateIndex < 0
                ? daysList[selectedCalendarTimeline]
                : endDateIndex + 1
            )} */}
              {/* {dailyWorkloadSortedByDayIndex.length > 0 &&
            Object.values(calendarDatesFlattened)
              .slice(
                endsBeforeTimespan || startsAfterTimespan
                  ? 0
                  : // : dailyWorkloadSortedByDayIndex.at(0)?.isNewLastBox
                  dailyWorkloadSortedByDayIndex.filter((k) => k.isNewBox).length > 0
                  ? startInx - dailyWorkloadSortedByDayIndex.filter((k) => k.isNewBox).length
                  : startInx,
                endsBeforeTimespan || startsAfterTimespan
                  ? 0
                  : endDateIndex < 0
                  ? daysList[selectedCalendarTimeline]
                  : endDateIndex + 1
              ) // adding 1 to 'endDateIndex' to include it
              .map((k, i, selfArr) => {
                console.info(
                  "breakkkkk",
                  selfArr,
                  endDateIndex,
                  endsBeforeTimespan,
                  startsAfterTimespan,
                  startInx,
                  i,
                  daysList[selectedCalendarTimeline],
                  endDateIndex < 0 && startInx + i + 1 === daysList[selectedCalendarTimeline]
                );
                // debugger;

                // if (endDateIndex < 0 && startInx + i + 1 === daysList[selectedCalendarTimeline]) {
                //   return;
                // }
                const dayOfWeek = k.momentObj.day();
                const isWeekend = dayOfWeek === 0 || dayOfWeek === 6;

                // index will cause re-rendering on resize
                return (
                  <>
                    <div
                      key={i}
                      style={{
                        height: "100%",
                        position: "absolute",
                        top: 0,
                        left: 0,
                        width: dayBoxWidth,
                        transform: `translateX(${dayBoxWidth * i}px)`,
                        ...(isWeekend && { opacity: 0.5, backgroundColor: "lightgray" }),
                        // backgroundColor: isWeekend ? "lightgray" : "transparent",
                        // zIndex: '-1',
                      }}></div>
                  </>
                );
              })} */}
              {dailyWorkloadSortedByDayIndex

                // .slice(
                //   0,
                //   endsBeforeTimespan || startsAfterTimespan
                //     ? 0
                //     : endDateIndex < 0
                //     ? daysList[selectedCalendarTimeline] - startInx
                //     : endDateIndex + 1
                // ) // adding 1 to 'endDateIndex' to include it

                // .filter((k, i) => {
                //   console.info(
                //     "ggggg",
                //     endDateIndex,
                //     startInx,
                //     i,
                //     startInx + i < daysList[selectedCalendarTimeline],
                //     endDateIndex >= 0 || startInx + i < daysList[selectedCalendarTimeline]
                //   );
                //   return endDateIndex >= 0 || startInx + i < daysList[selectedCalendarTimeline];
                // })
                .map((k, i, arrSelf) => {
                  // if (endDateIndex < 0 && startInx + i + 1 === daysList[selectedCalendarTimeline]) {
                  //   return;
                  // }
                  // console.info("gggg----7777", arrSelf.length, arrSelf);

                  const dayOfWeek = k.moment?.day() ?? -1;
                  const isWeekend = dayOfWeek === 0 || dayOfWeek === 6;

                  const resetBorderRadius =
                    (i === 0 && startsBeforeTimespan) ||
                    (i === arrSelf.length - 1 && endsAfterTimespan);
                  // console.info(
                  //   "gggg-----444444",
                  //   i,
                  //   startsBeforeTimespan,
                  //   endsAfterTimespan,
                  //   i === 0 && startsBeforeTimespan,
                  //   i === arrSelf.length - 1 && endsAfterTimespan
                  // );

                  // index will cause re-rendering on resize
                  return (
                    <>
                      <TaskTimelineDay
                        key={i}
                        {...{
                          effortInHours: k.effort,
                          // isOffDay: isWeekend,
                          // dayBoxWidth: getBoxWidth(i),
                          dayBoxWidth,
                          boxWidth: k.boxWidth,
                          resetBorderRadius,
                          // dayIndex: i,
                          // taskId: taskData.id
                          updateDayWorkload: (updatedHours) =>
                            updateWorkloadOfDay(updatedHours, i, k.id, k.isNewBox),
                          enableTimelineDrag: () => setLockTimelineDrag(false),
                          disableTimelineDrag: () => setLockTimelineDrag(true),
                          isNewBox: k.isNewBox,

                          // ...(k.isNewBox && { isNewDay: k.isNewBox, newDayWidth: newDayWidth })
                          ...(k.isNewLastBox && { isNewLastDay: k.isNewLastBox, newLastDayWidth }),
                        }}
                      />
                    </>
                  );
                })}
            </TaskTimelineResizable>
            <div
              className="task-label"
              style={{
                paddingLeft: 10,
                // width: "200px"
                minWidth: `${isMonthView ? dayBoxWidth * 3 : dayBoxWidth}px`,
                maxWidth: `${isMonthView ? dayBoxWidth * 6 : dayBoxWidth * 2}px`,
              }}>
              {taskData.name}
            </div>
          </div>
        </TaskTimelineDraggable>
      </ErrorBoundary>
    </>
  );
};

TaskTimeline.defaultProps = {
  /** default props , calls when props are not coming from parent class undefined */
  classes: {},
  theme: {},
};

const WithInjectedHOCs = compose(withRouter, withStyles(styles, { withTheme: true }))(TaskTimeline);
const Memoized = memo(WithInjectedHOCs);

export default Memoized;
