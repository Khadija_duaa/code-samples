import React, {
  memo,
  useCallback,
  useContext,
  useEffect,
  useLayoutEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import { connect, useDispatch, useSelector } from "react-redux";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import clsx from "clsx";
import TaskTimelineDay from "../TaskTimelineDay";
import { debounce, sortBy } from "lodash";
import styles from "./styles";
import classNames from "classnames";
import { cloneDeep } from "lodash";
import { generateDateRange } from "../../../../helper/dates/dates";
import { daysList, formatMomentToDate } from "./../../temp-utils";
import ErrorBoundary from "../ErrorBoundary";
import TaskTimelineDraggable from "../TaskTimelineDraggable";
import TaskTimelineResizable from "../TaskTimelineResizable";
import { ResourceContext, UnassignedTasksContext } from "../../contexts";
import {
  updateResourceTaskDayWorkloadAction,
  updateUnassignedTaskDayWorkloadAction,
} from "../../../../redux/actions/resources";
import {
  selectResourceTaskWorkloads,
  selectResourceTask,
  selectUnassignedTask,
  selectUnassignedTasksDataListExists,
  selectUnassignedTaskWorkloads,
  selectTaskColorSetting,
  selectTaskTimelineColor,
  selectTaskData,
} from "../../../../redux/selectors/resources";
import {
  createResourceTaskDayWorkloadThunk,
  updateResourceTaskDayWorkloadThunk,
  updateResourceOrUnassignedTaskPlannedDatesThunk,
} from "../../../../redux/thunks/resources";
import TaskTimelineLabelsPlusPopover from "../TaskTimelineLabelsPlusPopover";
import { dayWorkloadObjectTemplate } from "../../api-helpers";
import PropTypes from "prop-types";
import { TaskTimelineColor } from "../../../../utils/constants/TaskTimelineColor";
import { priorityData } from "./../../../../helper/taskDropdownData";
import { TaskContext } from "../../contexts/resources";

// // const plannedStartDate = "2022-12-15T02:00:00";
// // const plannedEndDate = "2022-12-27T23:55:00";
// const plannedStartDate = "2022-12-22T02:00:00";
// const plannedEndDate = "2022-12-27T23:55:00";
// const plannedStartDate = "2023-01-10T02:00:00";
// const plannedEndDate = "2023-01-11T23:55:00";

// // case 1:
// const plannedStartDate = "2023-01-10T02:00:00";
// const plannedEndDate = "2023-01-11T23:55:00";

// // case 2:
// const plannedStartDate = "2023-01-10T02:00:00";
// const plannedEndDate = "2023-01-15T23:55:00";

// // case 3:
// const plannedStartDate = "2023-01-09T02:00:00";
// const plannedEndDate = "2023-01-14T23:55:00";

// // case 4:
// const plannedStartDate = "2023-01-07T02:00:00";
// const plannedEndDate = "2023-01-10T23:55:00";

// // case 5:
// const plannedStartDate = "2023-01-13T02:00:00";
// const plannedEndDate = "2023-01-18T23:55:00";

// // case 6:
// const plannedStartDate = "2023-01-06T02:00:00";
// const plannedEndDate = "2023-01-17T23:55:00";

// temporary format
// const format = "YYYY-MM-DD";

export const boxTemplate = {
  dayIndex: -1,
  moment: null,
  effort: 0,
  id: null,
  offDay: false,
  isNewLastBox: false,
  isNewBox: false,
  boxWidth: 0,
  isNewFirstBox: false,
};

const TaskTimeline = (props) => {
  const { classes, theme } = props;

  const { isUnassignedTask = false } = useContext(UnassignedTasksContext) || {};
  const resourceId = useContext(ResourceContext);
  const taskId = useContext(TaskContext);

  const dispatch = useDispatch();

  const [lockTimelineDrag, setLockTimelineDrag] = useState(false);

  const unassignedTasksDataListExists = useSelector(selectUnassignedTasksDataListExists);
  const selectedCalendarTimeline = useSelector((s) => s.resources.filters.calendarTimelinePeriod);
  const calendarDatesFlattened = useSelector((s) => s.resources.data.calendarDatesFlattened);
  const dayBoxWidth = useSelector((s) => s.resources.uiGrid.dayProgressHourglassWidth);

  const _taskData = useSelector((s) =>
    selectTaskData(s, {
      isUnassignedTask,
      resourceId,
      taskId,
    })
  );

  // let _taskData = useSelector(
  //   (s) =>
  //     (isUnassignedTask
  //       ? selectUnassignedTask(s, { taskId })
  //       : selectResourceTask(s, { resourceId, taskId })) || taskData
  // );

  const taskData = _taskData;

  // if (!_taskData) {
  //   return <></>;
  // }
  let {
    plannedStartDate,
    plannedEndDate,
    dailyWorkload = [],
    workspaceId,
    priority: taskPriority,
    status: taskStatus,
    color: taskColor,
  } = _taskData || {};

  const taskTimelineColor = useSelector((s) =>
    selectTaskTimelineColor(s, {
      theme,
      taskPriority,
      taskStatusColor: taskStatus?.color,
      taskColor,
    })
  );

  // console.info('taskData:', cloneDeep(_taskData));

  // const taskTimelineWorkloads = useSelector(
  //   (s) =>
  //     (isUnassignedTask
  //       ? // ? selectUnassignedTask(s, { taskId })?.dailyWorkload
  //         selectUnassignedTaskWorkloads(s, { taskId })
  //       : selectResourceTaskWorkloads(s, { resourceId, taskId })) || []
  // );

  // Note: without memoization, it's causing infinite rendering loop
  const taskTimelineWorkloads = useMemo(() => taskData?.dailyWorkload || [], [taskData]);
  // const { plannedStartDate, plannedEndDate, dailyWorkload } = useSelector((s) =>
  //   selectResourceTask(s, { resourceId, taskId })
  // );

  // plannedStartDate = "2023-02-20";
  // plannedEndDate = "2023-02-20";

  // console.log("eee-eee dayBoxWidth:", dayBoxWidth);
  // console.log("eee-eee start and end dates:", taskData.plannedStartDate, taskData.plannedEndDate);

  const getBoxWidth = (index, listLen) => {
    // console.info('box:', dayBoxWidth, index, listLen)
    // const listLen = dailyWorkloadSortedByDayIndex.length;
    // TODO: this is temporarily returning same width for each box even on corners'.
    // return dayBoxWidth;
    const isOnly = listLen === 1;
    if (isOnly) {
      return dayBoxWidth - 6;
    }

    const isFirst = index === 0;
    const isLast = listLen - 1 === index;

    if (isFirst || isLast) {
      return dayBoxWidth - 5;
    }
    return dayBoxWidth;
  };

  const [timelineDateRange, setTimelineDateRange] = useState(
    generateDateRange(plannedStartDate, plannedEndDate) || []
  );
  // const timelineDateRange = generateDateRange(plannedStartDate, plannedEndDate);

  useEffect(() => {
    // Note: this updates the timelineDateRange state variable whenever the date ranges change (increased / decreased)
    setTimelineDateRange(generateDateRange(plannedStartDate, plannedEndDate));
  }, [plannedStartDate, plannedEndDate]);

  // const taskDailyWorkload = taskData.dailyWorkload;
  // const taskDailyWorkload = taskTimelineWorkloads;
  const loads = taskTimelineWorkloads?.length <= 0 ? dailyWorkload : taskTimelineWorkloads;
  const [taskDailyWorkload, setTaskDailyWorkload] = useState(loads);

  // console.info('taskDailyWorkload:', taskDailyWorkload);

  const getWorkloadsDataForHashMap = () =>
    (taskTimelineWorkloads?.length <= 0 ? dailyWorkload : taskTimelineWorkloads).map((k) => {
      return [k.date, k];
    });

  const [taskDailyWorkloadHashMap, setTaskDailyWorkloadHashMap] = useState(
    new Map(getWorkloadsDataForHashMap())
  );

  useEffect(() => {
    const newMap = new Map(getWorkloadsDataForHashMap());
    // Note: this updates local hash map of day workloads whenever the loads array updated in redux for the task
    setTaskDailyWorkloadHashMap(newMap);
  }, [taskTimelineWorkloads]);

  // console.info('taskDailyWorkloadHashMap:', taskDailyWorkloadHashMap);

  const getMappedDates = () =>
    timelineDateRange.map((k, inx, selfArr) => {
      const date = formatMomentToDate(k);
      const dateData = taskDailyWorkloadHashMap?.has(date)
        ? taskDailyWorkloadHashMap?.get(date)
        : null;

      return {
        ...boxTemplate,

        dayIndex: inx,
        moment: k,
        // effort: 0,
        effort: dateData?.load || 0,
        id: dateData?.id || null,
        offDay: false,
        // NOTE: we can comment out below lines because we need them in only new boxes
        isNewLastBox: false,
        isNewBox: false,
        // boxWidth: dayBoxWidth
        boxWidth: getBoxWidth(inx, selfArr.length),
      };
    });

  const [timelineDateRangeMapped, setTimelineDateRangeMapped] = useState(getMappedDates());
  // console.info("timelineDateRangeMapped:", timelineDateRangeMapped);
  // console.info("timelineDateRangeMapped:", timelineDateRangeMapped.map(formatMomentToDate));

  useEffect(() => {
    // Note: This is required to pass the taskDurationDays to popover component, this will update the state variable, whenever timelineDateRange state variable changes
    setTimelineDateRangeMapped(getMappedDates());
  }, [timelineDateRange]);

  // const dailyWorkloadSortedByDayIndex = sortBy(dailyWorkload, (k) => k.dayIndex);

  const [newLastDayWidth, setNewDayWidth] = useState(0);
  const [timelineDragPosition, setTimelineDragPosition] = useState(0);
  const [timelineDragPositionBackup, setTimelineDragPositionBackup] = useState(0);

  let mapRangeToTimelineSpan = null;
  // const calendarDatesFlattenedSorted = Object.values(calendarDatesFlattened).sort(
  //   (a, b) => a.dateOfMonth - b.dateOfMonth
  // );
  // alread sorted when creating flattened dates, otherwise use moment to sort
  const [calendarDatesFlattenedSorted] = useState(Object.values(calendarDatesFlattened));

  // const dateIndex = calendarDatesFlattenedSorted.findIndex((k) =>
  //   k.momentObj.isSame(timelineDateRangeMapped.at(0), "day")
  // );

  // let startDateIndex =
  //   timelineDateRangeMapped.length > 0 &&
  //   calendarDatesFlattenedSorted.findIndex((k) =>
  //     k.momentObj.isSame(timelineDateRangeMapped.at(0).moment, "day")
  //   );

  // if (startDateIndex === -1 && timelineDateRangeMapped.length > 0) {
  //   startDateIndex = timelineDateRangeMapped.findIndex((k) =>
  //     k.moment.isSame(calendarDatesFlattenedSorted.at(0).momentObj, "day")
  //   );

  //   startDateIndex *= -1;
  // }

  const calculateStartDateIndex = () => {
    if (!timelineDateRangeMapped) {
      return null;
    }
    let startIndex =
      timelineDateRangeMapped.length > 0 &&
      calendarDatesFlattenedSorted.findIndex((k) =>
        k.momentObj.isSame(timelineDateRangeMapped.at(0).moment, "day")
      );

    if (startIndex === -1 && timelineDateRangeMapped.length > 0) {
      startIndex = timelineDateRangeMapped.findIndex((k) =>
        k.moment.isSame(calendarDatesFlattenedSorted.at(0).momentObj, "day")
      );

      startIndex *= -1;
    }
    return startIndex;
  };
  const [startDateIndex, setStartDateIndex] = useState(calculateStartDateIndex());

  // console.info("startDateIndex:", startDateIndex);

  const calculateEndDateIndex = () =>
    timelineDateRangeMapped.length > 0 &&
    calendarDatesFlattenedSorted.findIndex((k) =>
      k.momentObj.isSame(timelineDateRangeMapped.at(-1).moment, "day")
    );
  let [endDateIndex, setEndDateIndex] = useState(calculateEndDateIndex());

  // if (endDateIndex === -1 && timelineDateRangeMapped.length > 0) {

  //   endDateIndex = timelineDateRangeMapped.findIndex((k) =>
  //     k.moment.isSame(calendarDatesFlattenedSorted.at(-1).momentObj, "day")
  //   );

  //   endDateIndex *= -1;
  // }

  const calculateStartsBeforeTimespan = () =>
    timelineDateRangeMapped.length > 0 &&
    calendarDatesFlattenedSorted.every((k) =>
      timelineDateRangeMapped.at(0).moment.isBefore(k.momentObj, "day")
    );
  const [startsBeforeTimespan, setStartsBeforeTimespan] = useState(calculateStartsBeforeTimespan());

  // useEffect(() => {
  //   setStartsBeforeTimespan(calculateStartsBeforeTimespan());
  // }, [startsBeforeTimespan]);

  const calculateEndsAfterTimespan = () =>
    timelineDateRangeMapped.length > 0 &&
    calendarDatesFlattenedSorted.every((k) =>
      timelineDateRangeMapped.at(-1).moment.isAfter(k.momentObj, "day")
    );
  // const [endsAfterTimespan, setEndsAfterTimespan] = useState(calculateEndsAfterTimespan());
  const endsAfterTimespan = calculateEndsAfterTimespan();

  // useEffect(() => {
  //   setEndsAfterTimespan(calculateEndsAfterTimespan());
  // }, [endsAfterTimespan]);

  const calculateStartsAfterTimespan = () =>
    timelineDateRangeMapped.length > 0 &&
    calendarDatesFlattenedSorted.every((k) =>
      timelineDateRangeMapped.at(0).moment.isAfter(k.momentObj, "day")
    );
  const [startsAfterTimespan, setStartsAfterTimespan] = useState(calculateStartsAfterTimespan());

  // useEffect(() => {
  //   setStartsAfterTimespan(calculateStartsAfterTimespan());
  // }, [startsAfterTimespan]);

  const calculateEndsBeforeTimespan = () =>
    timelineDateRangeMapped.length > 0 &&
    calendarDatesFlattenedSorted.every((k) =>
      timelineDateRangeMapped.at(-1).moment.isBefore(k.momentObj, "day")
    );
  // const [endsBeforeTimespan, setEndsBeforeTimespan] = useState(calculateEndsBeforeTimespan());
  const endsBeforeTimespan = calculateEndsBeforeTimespan();

  // useEffect(() => {
  //   setEndsBeforeTimespan(calculateEndsBeforeTimespan());
  // }, [endsBeforeTimespan]);

  // useLayoutEffect(() => {
  // useEffect(() => {
  //   setStartDateIndex(calculateStartDateIndex());
  //   // setEndDateIndex(calculateEndDateIndex());

  //   // setStartsBeforeTimespan(calculateStartsBeforeTimespan());
  //   // setEndsAfterTimespan(calculateEndsAfterTimespan());
  //   // setStartsAfterTimespan(calculateStartsAfterTimespan());
  //   // setEndsBeforeTimespan(calculateEndsBeforeTimespan());
  // }, [timelineDateRangeMapped]);

  // console.info("gggg---8888", taskData.name, endsBeforeTimespan, startsAfterTimespan);

  // const timelineInitialPosition = startInx < 0 ? 0 : startInx * dayBoxWidth;
  // const timelineInitialPosition = startDateIndex < 0 ? 0 : startDateIndex * dayBoxWidth;
  const [timelineInitialPosition, setTimelineInitialPosition] = useState(
    startDateIndex * dayBoxWidth
  );

  const getSortedListByDay = () =>
    timelineDateRangeMapped.length <= 0 || endsBeforeTimespan || startsAfterTimespan
      ? []
      : timelineDateRangeMapped;

  const [dailyWorkloadSortedByDayIndexBackup, setDailyWorkloadSortedByDayIndexBackup] = useState(
    getSortedListByDay()
  );

  const [dailyWorkloadSortedByDayIndex, setDailyWorkloadSortedByDayIndex] = useState(
    // sortBy(dailyWorkload, (k) => k.dayIndex)
    // timelineDateRangeMapped

    // timelineDateRangeMapped.length <= 0
    //   ? []
    //   : timelineDateRangeMapped.slice(
    //       endsBeforeTimespan || startsAfterTimespan || startsBeforeTimespan
    //         ? 0
    //         : // : // : dailyWorkloadSortedByDayIndex.at(0)?.isNewLastBox
    //           // timelineDateRangeMapped.length > 0
    //           // ? startDateIndex - dailyWorkloadSortedByDayIndex.filter((k) => k.isNewBox).length
    //           startDateIndex,
    //       endsBeforeTimespan || startsAfterTimespan
    //         ? 0
    //         : endDateIndex < 0
    //         ? // ? daysList[selectedCalendarTimeline] - startInx
    //           daysList[selectedCalendarTimeline] - startDateIndex
    //         : endDateIndex + 1
    //     )

    // 2nd mechanism
    getSortedListByDay()
  );

  // useEffect(() => {
  //   console.info("dailyWorkloadSortedByDayIndex:", dailyWorkloadSortedByDayIndex);
  // }, [dailyWorkloadSortedByDayIndex]);

  useEffect(() => {
    setDailyWorkloadSortedByDayIndex(getSortedListByDay());
    setStartDateIndex(calculateStartDateIndex());
  }, [timelineDateRangeMapped]);

  // useEffect(() => {
  //   if (dailyWorkloadSortedByDayIndex?.length > 0) {
  //     const cloned = cloneDeep(dailyWorkloadSortedByDayIndex);

  //     cloned = cloned.filter((k) => k.isNewBox);

  //     cloned.push({
  //       dayIndex: cloned.length,
  //       moment: null,
  //       effort: 0,
  //       offDay: true,
  //       isNewLastBox: true,
  //       isNewBox: true,
  //       // boxWidth: 0
  //       // boxWidth: getBoxWidth(inx, selfArr.length),
  //     });

  //     setDailyWorkloadSortedByDayIndex();
  //   }
  // }, [dailyWorkloadSortedByDayIndex]);

  const [timelineDateRangeWithoutWeekends] = useState(
    timelineDateRangeMapped
      // const timelineDateRangeWithoutWeekends = timelineDateRangeMapped
      // const timelineDateRangeWithoutWeekends = dailyWorkloadSortedByDayIndex
      // const [timelineDateRangeWithoutWeekends] = useState(dailyWorkloadSortedByDayIndex
      .filter((k) => {
        const dayOfWeek = k.moment?.day();
        // const dayOfWeek = k.day();
        return dayOfWeek !== 0 && dayOfWeek !== 6;
      })
  );

  // console.info(
  //   "timelineDayssss",
  //   timelineDateRangeMapped,
  //   timelineDateRangeMapped.map((k) => k.moment.format(dayComparisonDateFormat)),
  //   timelineDateRangeMapped.map((k) => k.moment.day())
  // );
  // console.info(
  //   "timelineDayssss without weekends ------ 222222",
  //   timelineDateRangeWithoutWeekends,
  //   timelineDateRangeWithoutWeekends.map((k) => k.moment?.format(dayComparisonDateFormat)),
  //   timelineDateRangeWithoutWeekends.map((k) => k.moment?.day())
  // );

  // const [avgDailyLoadHours, setAvgDailyLoadHours] = useState(
  //   0
  //   // (
  //   //   (taskData.estimatedHours * 60 + taskData.estimatedMinutes) /
  //   //   60 /
  //   //   (timelineDateRangeWithoutWeekends.length +
  //   //     dailyWorkloadSortedByDayIndex.filter((k) => k.isNewBox).length)
  //   // ).toFixed(1)
  // );

  // const minTimelineWidth = dailyWorkloadSortedByDayIndex.filter((k) => !k.isNewBox).length * dayBoxWidth;
  // const minTimelineWidth =
  //   (dailyWorkload.length || daysList[selectedCalendarTimeline]) * dayBoxWidth;
  // const minTimelineWidth = dailyWorkloadSortedByDayIndex.length * dayBoxWidth;
  const [minTimelineWidth] = useState(
    dailyWorkloadSortedByDayIndex
      .filter((k) => !k.isNewBox)
      .reduce((acc, cur) => acc + cur.boxWidth, 0)
  );

  // let timelineFixedWidth = minTimelineWidth;
  const [timelineFixedWidth, setTimelineFixedWidth] = useState(minTimelineWidth);

  // useEffect(() => {
  //   console.info("average daily hour", avgDailyLoadHours);
  //   setAvgDailyLoadHours(
  //     (
  //       (taskData.estimatedHours * 60 + taskData.estimatedMinutes) /
  //       60 /
  //       (timelineDateRangeWithoutWeekends.length +
  //         dailyWorkloadSortedByDayIndex.filter((k) => k.isNewBox).length)
  //     ).toFixed(1)
  //   );
  // }, [
  //   avgDailyLoadHours,
  //   dailyWorkloadSortedByDayIndex,
  //   taskData.estimatedHours,
  //   taskData.estimatedMinutes,
  //   timelineDateRangeWithoutWeekends,
  // ]);

  useEffect(() => {
    setTimelineDragPosition(timelineInitialPosition);
    setTimelineDragPositionBackup(timelineInitialPosition);
  }, []);

  // Note: This effect triggers when day-box-width changed in redux to update some state variables
  useEffect(() => {
    // Note: This updates widths of task days (within timeline)
    setTimelineDateRangeMapped((list) =>
      list.map((k, inx, selfArr) => ({
        ...k,
        boxWidth: getBoxWidth(inx, selfArr.length),
      }))
    );

    // Note: This updades starting offset according to updated day-box-width
    const newStartingOffset = startDateIndex * dayBoxWidth;
    setTimelineInitialPosition(newStartingOffset);
    setTimelineDragPosition(newStartingOffset);
    setTimelineDragPositionBackup(newStartingOffset);
  }, [dayBoxWidth]);

  useEffect(() => {
    // Note: This updades starting offset according to updated start-data-index
    const newStartingOffset = startDateIndex * dayBoxWidth;
    setTimelineInitialPosition(newStartingOffset);
    setTimelineDragPosition(newStartingOffset);
    setTimelineDragPositionBackup(newStartingOffset);
  }, [startDateIndex]);

  // useEffect(() => {
  //   setTimelineDragPosition(timelineInitialPosition);
  // }, [timelineInitialPosition]);

  const updateBackendCreateOrUpdateDayEstimate = (
    workspaceId,
    updatedHours,
    taskDayId,
    dayMoment,
    isNewBox
  ) => {
    if (isNewBox || !taskDayId) {
      // TODO: integrate the API to create estimates
      dispatch(
        createResourceTaskDayWorkloadThunk({
          workspaceId,
          resourceId,
          taskId,
          dateMoment: dayMoment,
          updatedWorkloadHours: updatedHours,
          isUnassignedTask,
        })
      );
    } else {
      // integrate the API to update task day estimates
      dispatch(
        updateResourceTaskDayWorkloadThunk({
          // taskId,
          taskDayId,
          updatedWorkloadHours: updatedHours,
        })
      );
    }
  };

  const debouncedApiCallUpdateBackendCreateOrUpdateDayEstimate = useCallback(
    debounce(updateBackendCreateOrUpdateDayEstimate, 3000),
    []
  );

  const enableTimelineDrag = () => setLockTimelineDrag(false),
    disableTimelineDrag = () => setLockTimelineDrag(true);

  const updateWorkloadOfDay = (
    updatedHours,
    dayIndex = -1,
    taskDayId = null,
    dayMoment = null,
    isNewBox = false
  ) => {
    if (isNaN(updatedHours) || dayIndex <= -1) {
      return;
    }

    // here update local workloads
    const newWorkloads = cloneDeep(taskDailyWorkload);

    let dayLoad = newWorkloads.at(dayIndex);
    if (!dayLoad || Object.keys(dayLoad) <= 0) {
      dayLoad = {
        ...dayWorkloadObjectTemplate,
        moment: dayMoment,
        date: formatMomentToDate(dayMoment),
      };
    }
    dayLoad.load = updatedHours || 0;

    newWorkloads.splice(dayIndex, 1, dayLoad);

    setTaskDailyWorkload(newWorkloads);

    if (isUnassignedTask) {
      dispatch(updateUnassignedTaskDayWorkloadAction(taskId, dayIndex, updatedHours));
    } else {
      dispatch(updateResourceTaskDayWorkloadAction(resourceId, taskId, dayIndex, updatedHours));
    }

    debouncedApiCallUpdateBackendCreateOrUpdateDayEstimate(
      workspaceId,
      updatedHours,
      taskDayId,
      dayMoment,
      isNewBox
    );
  };

  // const updateTaskPlannedDatesBackend = useCallback((newStartDateMoment, newEndDateMoment) => {
  //   if (
  //     !newStartDateMoment ||
  //     !newStartDateMoment.isValid() ||
  //     !newEndDateMoment ||
  //     !newEndDateMoment.isValid()
  //   ) {
  //     return;
  //   }

  //   console.info("API called to update planned dates");

  //   // dispatch(
  //   //   updateResourceTaskPlannedDatesThunk({
  //   //     resourceId,
  //   //     taskId,
  //   //     startDate: newStartDateMoment.toISOString(),
  //   //     endDate: newEndDateMoment.toISOString(),
  //   //   })
  //   // );
  // }, []);

  // const updateTaskPlannedDatesBackendDebounced = useCallback(
  //   debounce(updateTaskPlannedDatesBackend, 5000, {
  //     // maxWait: 2000,
  //   }),
  //   []
  // );

  if (!_taskData) {
    return <></>;
  }

  const timelineDraggableProps = {
    setTimelineDragPosition,
    setTimelineDragPositionBackup,
    timelineInitialPosition,
    setTimelineInitialPosition,
    timelineDragPosition,
    lockTimelineDrag,
    noDaysInTimeline: !dailyWorkloadSortedByDayIndex || dailyWorkloadSortedByDayIndex.length <= 0,
    // updateTaskPlannedDatesBackendDebounced,
    taskDailyWorkload,
    setTaskDailyWorkload,
  };

  const timelineResizableProps = {
    minTimelineWidth,
    startsBeforeTimespan,
    endsAfterTimespan,
    setTimelineFixedWidth,
    setNewDayWidth,
    boxTemplate,
    setTimelineDragPosition,
    timelineDragPositionBackup,
    // dayBoxWidth,
    setDailyWorkloadSortedByDayIndex,
    dailyWorkloadSortedByDayIndexBackup,
    setDailyWorkloadSortedByDayIndexBackup,
    timelineFixedWidth,
    dailyWorkloadSortedByDayIndex,
    timelineDragPosition,
    timelineInitialPosition,
    getBoxWidth,
    // taskId,
    // plannedStartDate,
    // plannedEndDate,
    taskDailyWorkload,
  };

  const timelineLabelsPlusPopoverProps = {
    // taskDurationDays: dailyWorkloadSortedByDayIndex?.length || 0,
    taskDurationDays: timelineDateRangeMapped?.length || 0,
    // disableEditingDuration: endsBeforeTimespan || startsAfterTimespan || dailyWorkloadSortedByDayIndex?.length === 0,
    disableEditingDuration: dailyWorkloadSortedByDayIndex?.length === 0,
    taskDailyWorkload,
    dailyWorkloadSortedByDayIndex,
    setDailyWorkloadSortedByDayIndex,
    setTaskDailyWorkload,
    enableTimelineDrag,
    disableTimelineDrag,
  };

  return (
    <>
      <ErrorBoundary>
        <TaskTimelineDraggable {...timelineDraggableProps}>
          <TaskTimelineLabelsPlusPopover {...timelineLabelsPlusPopoverProps}>
            <TaskTimelineResizable {...timelineResizableProps}>
              <div
                className={clsx(classes.timelineDaysWrapper, {
                  [classes.defaultBorder]: !taskTimelineColor,
                })}
                style={{ borderColor: taskTimelineColor || theme.palette.border.lightBorder }}>
                {/* {console.info(
              "111",
              endsBeforeTimespan || startsAfterTimespan
                ? 0
                : // : dailyWorkloadSortedByDayIndex.at(0)?.isNewLastBox
                dailyWorkloadSortedByDayIndex.filter((k) => k.isNewBox).length > 0
                ? startInx - dailyWorkloadSortedByDayIndex.filter((k) => k.isNewBox).length
                : startInx
            )} */}
                {/* {console.info(
              "222",
              endsBeforeTimespan || startsAfterTimespan
                ? 0
                : endDateIndex < 0
                ? daysList[selectedCalendarTimeline]
                : endDateIndex + 1
            )} */}
                {/* {dailyWorkloadSortedByDayIndex.length > 0 &&
            Object.values(calendarDatesFlattened)
              .slice(
                endsBeforeTimespan || startsAfterTimespan
                  ? 0
                  : // : dailyWorkloadSortedByDayIndex.at(0)?.isNewLastBox
                  dailyWorkloadSortedByDayIndex.filter((k) => k.isNewBox).length > 0
                  ? startInx - dailyWorkloadSortedByDayIndex.filter((k) => k.isNewBox).length
                  : startInx,
                endsBeforeTimespan || startsAfterTimespan
                  ? 0
                  : endDateIndex < 0
                  ? daysList[selectedCalendarTimeline]
                  : endDateIndex + 1
              ) // adding 1 to 'endDateIndex' to include it
              .map((k, i, selfArr) => {
                console.info(
                  "breakkkkk",
                  selfArr,
                  endDateIndex,
                  endsBeforeTimespan,
                  startsAfterTimespan,
                  startInx,
                  i,
                  daysList[selectedCalendarTimeline],
                  endDateIndex < 0 && startInx + i + 1 === daysList[selectedCalendarTimeline]
                );

                // if (endDateIndex < 0 && startInx + i + 1 === daysList[selectedCalendarTimeline]) {
                //   return;
                // }
                const dayOfWeek = k.momentObj.day();
                const isWeekend = dayOfWeek === 0 || dayOfWeek === 6;

                // index will cause re-rendering on resize
                return (
                  <>
                    <div
                      key={i}
                      style={{
                        height: "100%",
                        position: "absolute",
                        top: 0,
                        left: 0,
                        width: dayBoxWidth,
                        transform: `translateX(${dayBoxWidth * i}px)`,
                        ...(isWeekend && { opacity: 0.5, backgroundColor: "lightgray" }),
                        // backgroundColor: isWeekend ? "lightgray" : "transparent",
                        // zIndex: '-1',
                      }}></div>
                  </>
                );
              })} */}
                {/* {console.info("kk length:", dailyWorkloadSortedByDayIndex.length)} */}
                {dailyWorkloadSortedByDayIndex

                  // .slice(
                  //   0,
                  //   endsBeforeTimespan || startsAfterTimespan
                  //     ? 0
                  //     : endDateIndex < 0
                  //     ? daysList[selectedCalendarTimeline] - startInx
                  //     : endDateIndex + 1
                  // ) // adding 1 to 'endDateIndex' to include it

                  // .filter((k, i) => {
                  //   console.info(
                  //     "ggggg",
                  //     endDateIndex,
                  //     startInx,
                  //     i,
                  //     startInx + i < daysList[selectedCalendarTimeline],
                  //     endDateIndex >= 0 || startInx + i < daysList[selectedCalendarTimeline]
                  //   );
                  //   return endDateIndex >= 0 || startInx + i < daysList[selectedCalendarTimeline];
                  // })
                  .map((k, i, arrSelf) => {
                    // if (endDateIndex < 0 && startInx + i + 1 === daysList[selectedCalendarTimeline]) {
                    //   return;
                    // }
                    // console.info("gggg----7777", arrSelf.length, arrSelf);

                    const dayOfWeek = k.moment?.day() ?? -1;
                    const isWeekend = dayOfWeek === 0 || dayOfWeek === 6;

                    const resetBorderRadius =
                      (i === 0 && startsBeforeTimespan) ||
                      (i === arrSelf.length - 1 && endsAfterTimespan);
                    // console.info(
                    //   "gggg-----444444",
                    //   i,
                    //   startsBeforeTimespan,
                    //   endsAfterTimespan,
                    //   i === 0 && startsBeforeTimespan,
                    //   i === arrSelf.length - 1 && endsAfterTimespan
                    // );

                    // index will cause re-rendering on resize
                    return (
                      <>
                        <TaskTimelineDay
                          key={i}
                          {...{
                            effortInHours: Number(k.effort).toFixed(1),
                            // isOffDay: isWeekend,
                            boxWidth: k.boxWidth,
                            resetBorderRadius,
                            // dayIndex: i,
                            updateDayWorkload: (updatedHours) =>
                              updateWorkloadOfDay(updatedHours, i, k.id, k.moment, k.isNewBox),
                            enableTimelineDrag,
                            disableTimelineDrag,
                            isNewBox: k.isNewBox,

                            // ...(k.isNewBox && { isNewDay: k.isNewBox, newDayWidth: newDayWidth })
                            ...(k.isNewLastBox && {
                              isNewLastDay: k.isNewLastBox,
                              newLastDayWidth,
                            }),
                          }}
                        />
                      </>
                    );
                  })}
              </div>
            </TaskTimelineResizable>
          </TaskTimelineLabelsPlusPopover>
        </TaskTimelineDraggable>
      </ErrorBoundary>
    </>
  );
};

TaskTimeline.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
};

TaskTimeline.defaultProps = {
  /** default props , calls when props are not coming from parent class undefined */
  classes: {},
  theme: {},
};

const WithInjectedHOCs = compose(withRouter, withStyles(styles, { withTheme: true }))(TaskTimeline);
const Memoized = memo(WithInjectedHOCs);

export default Memoized;
