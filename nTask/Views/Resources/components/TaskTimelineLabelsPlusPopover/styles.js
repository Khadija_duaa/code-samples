const taskTimelineLabelsPlusPopoverStyles = (theme) => ({
  // here objects of styles
  timelinePlusLabelsContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    flexWrap: "nowrap",

    "& .task-label": {
      color: "black",
      fontFamily: "'lato'",
      fontSize: "14px",
      fontWeight: 400,
      lineHeight: "20px",
      textOverflow: "ellipsis",
      textTransform: "capitalize",
      WebkitLineClamp: "3",
      WebkitBoxOrient: "vertical",
      display: "-webkit-box",
      overflow: "hidden",
    },
  },
  timeLinePopoverParent: {
    boxShadow: "0px 2px 8px #0000001F",
    borderRadius: "6px",
  },
  timelinePopoverBody: {
    minWidth: "270px",
    minHeight: "167px",
    width: "100%",
    height: "100%",
    maxWidth: "334.49px",
    padding: "12px 16px",
    backgroundColor: "#FFFFFF",
  },
  tlPopoverBody: {},
  tlPopoverHeader: {
    borderBottom: "1px solid #EAEAEA",
    paddingBottom: "8px",
    marginBottom: "8px",
  },
  projectName: {
    color: "#171717",
    fontSize: "14px",
    fontWeight: "bold",
    fontFamily: "lato",
    textOverflow: "ellipsis",
    textTransform: "capitalize",
    WebkitLineClamp: "3",
    WebkitBoxOrient: "vertical",
    display: "-webkit-box",
    overflow: "hidden",
  },
  projectHoursList: {
    padding: "0",
    listStyle: "none",
  },
  projectHoursItem: {
    display: "flex",
    alignItems: "center",
    marginBottom: "8px",
    width: "190px",
    justifyContent: "space-between",
  },
  rightSide: {
    display: "flex",
    alignItems: "center",
    gap: "5px",
    position: "relative",
    width: "105px",
  },
  leftSide: {
    // width: "60%",
    flex: "1",
  },
  projectHours: {
    border: "1px solid #D8DEE1",
    width: "60px",
    borderRadius: "4px",
    fontSize: "13px",
    color: "#171717",
    padding: "5px",
  },
  unit: {
    color: "#171717",
    fontSize: "13px",
    fontFamily: "'Lato'",
    lineHeight: "20px",
    fontWeight: 400,
    position: "absolute",
    top: "50%",
    transform: "translateY(-50%)",
    right: "0px",
    textAlign: "left",
    width: "40px",
  },
  listLabel: {
    color: "#646464",
    fontSize: "13px",
    fontFamily: "lato",
  },
  popoverPaper: {
    backgroundColor: "#FFFFFF",
    borderRadius: "6px",
    boxShadow: "0px 2px 8px #0000001F",
  },
  popOverInput: {
    "& > fieldset": {
      display: "none",
    },
  },
  popOverActualInput: {
    border: "1px solid #D8DEE1",
    borderRadius: "4px",
    width: "60px",
    "&::-webkit-outer-spin-button": {
      WebkitAppearance: "none",
      margin: "0",
    },
    "&::-webkit-inner-spin-button": {
      WebkitAppearance: "none",
      margin: "0",
    },
    "& [type=number]": {
      MozAppearance: "textfield",
    },
  },
  popOverText: {
    color: "#171717",
    fontFamily: "Lato, Regular",
    fontSize: "13px",
    textTransform: "Capitalize",
    lineHeight: "20px",
  },
  popOverTooltip: {
    color: "#969696",
    fontSize: "15px",
  },
  projectTag: {
    color: "#7E7E7E",
    fontSize: "13px",
    lineHeight: "20px",
    fontFamily: "Lato,Regular",
    marginBottom: "6px",
  },
  subTaskIcon: {
    fontSize: 15,
    marginRight: 5,
    transform: "translateY(5px)",
  },
  taskLabelParent: {
    display: "flex",
    alignItems: "center",
    gap: "5px",
    marginTop: 5,
    "& svg": {
      color: "#7E7E7E",
      fontSize: "14px",
    },
  },
  taskLabel: {
    fontSize: "13px",
    color: "#7E7E7E",
    fontFamily: "lato",
    textTransform: "capitalize",
  },
  taskTitleParent: {
    display: "flex",
    alignItems: "flex-start",
  },
});

export default taskTimelineLabelsPlusPopoverStyles;
