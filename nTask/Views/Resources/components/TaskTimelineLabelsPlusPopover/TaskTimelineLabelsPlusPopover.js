import React, { memo, useCallback, useContext, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import clsx from "clsx";
import { debounce } from "lodash";
import styles from "./styles";
import { cloneDeep } from "lodash";
import { parseISODateString } from "../../../../helper/dates/dates";
import { formatMomentToDate } from "./../../temp-utils";
import { Grid, Popover, SvgIcon, Typography } from "@material-ui/core";
import { ResourceContext, UnassignedTasksContext } from "../../contexts";
import {
  deleteResourceTaskDayWorkloadByDateAction,
  deleteUnassignedTaskDayWorkloadByDateAction,
  updateResourceTaskPlannedDatesAction,
  updateUnassignedTaskPlannedDatesAction,
  updateUnassignedTaskWorkloadsAction,
} from "../../../../redux/actions/resources";
import { deleteResourceTaskDayWorkloadThunk } from "../../../../redux/thunks/resources";
import { CalendarTimelinePeriods } from "./../../../../utils/constants/CalendarTimelinePeriods";
import { updateResourceOrUnassignedTaskPlannedDatesThunk } from "./../../../../redux/thunks/resources";
import { updateResourceTaskWorkloadsAction } from "./../../../../redux/actions/resources";
import DefaultTextField from "../../../../components/Form/TextField";
import { dayWorkloadObjectTemplate } from "./../../api-helpers";
import { boxTemplate } from "../TaskTimeline";
import CustomTooltip from "../../../../components/Tooltip/Tooltip";
import InfoIcon from "@material-ui/icons/Info";
import SubTaskIcon from "../../../../components/Icons/SubTaskIcon";
import ResourceProjectIcon from "../../../../components/Icons/ResourceProjectIcon";
import PropTypes from "prop-types";
import LimitedTextWithTooltip from "../LimitedTextWithTooltip";
import {
  selectShowTaskNameWithTimelineSetting,
  selectTaskData,
} from "./../../../../redux/selectors/resources";
import { TaskContext } from "../../contexts/resources";

const TaskTimelineLabelsPlusPopover = (props) => {
  const {
    classes,
    theme,
    children,
    taskDurationDays,
    disableEditingDuration,
    taskDailyWorkload,
    dailyWorkloadSortedByDayIndex,
    setDailyWorkloadSortedByDayIndex,
    setTaskDailyWorkload,
    enableTimelineDrag,
    disableTimelineDrag,
  } = props;

  const dispatch = useDispatch();

  const { isUnassignedTask = false } = useContext(UnassignedTasksContext) || {};
  const resourceId = useContext(ResourceContext);
  const taskId = useContext(TaskContext);

  const taskData = useSelector((s) =>
    selectTaskData(s, {
      isUnassignedTask,
      resourceId,
      taskId,
    })
  );

  const [durationDays, setDurationDays] = useState(taskDurationDays);
  const [anchorEl, setAnchorEl] = useState(null);
  const [popoverPosition, setPopoverPosition] = useState({ x: null, y: null });

  const showTaskNameWithTimeline = useSelector(selectShowTaskNameWithTimelineSetting);
  const selectedCalendarTimeline = useSelector((s) => s.resources.filters.calendarTimelinePeriod);
  const dayBoxWidth = useSelector((s) => s.resources.uiGrid.dayProgressHourglassWidth);

  let {
    name: taskName,
    plannedStartDate,
    plannedEndDate,
    parentTask,
    project,
    // Note: this should be working, but not working, not picking default value i.e. {}
    // parentTask: { name: taskParentName } = {},
    // project: { name: taskProjectName } = {},
  } = taskData || {};
  const { name: taskParentName } = parentTask || {};
  const { name: taskProjectName } = project || {};

  const open = Boolean(anchorEl);

  const isMonthView = selectedCalendarTimeline === CalendarTimelinePeriods.month.value;

  useEffect(() => {
    setDurationDays(taskDurationDays);
  }, [taskDurationDays]);

  useEffect(() => {
    open ? disableTimelineDrag() : enableTimelineDrag();
  }, [open]);

  const handlePopoverOpen = (event) => {
    // popover focus in

    setPopoverPosition({ x: event.clientX, y: event.clientY });
    setAnchorEl(event.currentTarget);
  };

  const handlePopoverClose = () => {
    // popover focus out
    setAnchorEl(null);
  };

  const updateLocalState = (newVal) => {
    const daysDiff = newVal - taskDurationDays;
    const days = Math.abs(daysDiff);

    const extend = daysDiff > 0;
    const shrink = daysDiff < 0;

    if (daysDiff === 0) {
      return;
    }

    const clonedArr = cloneDeep(dailyWorkloadSortedByDayIndex);
    const newWorkloads = cloneDeep(taskDailyWorkload);

    if (extend) {
      const newBoxes = Array(days)
        // TODO/NOTE: here fill the array with full day effort, i.e. usually '8' hours
        .fill(0)
        .map((k, i) => ({
          ...boxTemplate,
          dayIndex: clonedArr.length + i,
          moment: clonedArr
            .at(-1)
            .moment.clone()
            .add(i + 1, "day")
            .clone(),
          effort: k,
          offDay: false,
          isNewLastBox: false,
          isNewBox: true,
          boxWidth: dayBoxWidth,
        }));

      clonedArr.push(...newBoxes);

      const workloadsToAdd = Array(days)
        .fill(0)
        .map((_, i) => {
          const currentMoment = parseISODateString(plannedEndDate)
            .clone()
            .add(i + 1, "day");

          return {
            ...dayWorkloadObjectTemplate,

            date: formatMomentToDate(currentMoment),
            load: 0,
            moment: currentMoment,
          };
        });

      newWorkloads.push(...workloadsToAdd);
    }

    let daysToDelete = [];

    if (shrink) {
      clonedArr.splice(-1 * days);

      daysToDelete = newWorkloads.slice(newWorkloads.length - days);

      newWorkloads.splice(-1 * days);
    }

    setDailyWorkloadSortedByDayIndex(clonedArr);
    setTaskDailyWorkload(newWorkloads);

    const newStartDate = clonedArr.at(0).moment;
    const newEndDate = clonedArr.at(-1).moment;

    if (isUnassignedTask) {
      dispatch(updateUnassignedTaskPlannedDatesAction(taskId, newStartDate, newEndDate));
      dispatch(updateUnassignedTaskWorkloadsAction(taskId, newWorkloads));
    } else {
      dispatch(updateResourceTaskPlannedDatesAction(resourceId, taskId, newStartDate, newEndDate));
      dispatch(updateResourceTaskWorkloadsAction(resourceId, taskId, newWorkloads));
    }

    // return;
    debouncedApiCall(newStartDate, newEndDate, daysToDelete);
  };

  const debouncedStateUpdate = useCallback(
    debounce(updateLocalState, 3000, {
      // maxWait: 2000,
    })
    // Note: disabling this empty array, bcz with this, updateLocalState preserves previous/old props (values), e.g. taskDurationDays
    // otherwise, we need to pass all the props and states as parameters explicitly while calling this function.
    //, []
  );

  const handleDurationChange = (e) => {
    const newVal = parseInt(e.target.value);

    setDurationDays(newVal);

    debouncedStateUpdate(newVal);
  };

  const handleDurationChangeBlur = (e) => {
    // e.preventDefault();
    // e.stopPropagation();
    // const newVal = parseInt(e.target.value);
    // console.info("duration blur:", newVal);
    // setDurationDays(newVal);
  };

  const updateBackend = /*useCallback(*/ (
    newStartDateMoment,
    newEndDateMoment,
    daysToDelete = []
  ) => {
    // console.info("called API to update", addDays);

    // const newStartDate = moment(plannedStartDate).add(addDays, "days");
    // const newEndDate = moment(plannedEndDate).add(addDays, "days");

    // dispatch(updateResourceTaskPlannedDatesAction(resourceId, taskId, newStartDate, newEndDate));

    // console.info(
    //   "API called to update planned dates",
    //   formatMomentToDate(newStartDateMoment),
    //   formatMomentToDate(newEndDateMoment),
    //   daysToDelete.length
    // );

    dispatch(
      updateResourceOrUnassignedTaskPlannedDatesThunk({
        taskId,
        startDate: newStartDateMoment.toISOString(),
        endDate: newEndDateMoment.toISOString(),
      })
    );

    // Call API to delete all these workload estimates
    daysToDelete.forEach((d) => {
      // Note: this below update is moved in below's thunk function's success block
      // if (isUnassignedTask) {
      //   dispatch(deleteUnassignedTaskDayWorkloadByDateAction(taskId, d.moment));
      // } else {
      //   dispatch(deleteResourceTaskDayWorkloadByDateAction(resourceId, taskId, d.moment));
      // }

      if (!d.id) {
        return;
      }

      dispatch(
        deleteResourceTaskDayWorkloadThunk({
          resourceId,
          taskId,
          taskDayId: d.id,
          dayDate: d.moment,
          isUnassignedTask,
        })
      );
    });
  };

  const debouncedApiCall = useCallback(debounce(updateBackend, 5000), []);

  if (!taskData) {
    return <>{children}</>;
  }

  return (
    <>
      <div
        className={classes.timelinePlusLabelsContainer}
        aria-owns={open ? "mouse-over-popover" : undefined}
        aria-haspopup="true"
        onMouseEnter={handlePopoverOpen}
        onMouseLeave={handlePopoverClose}>
        {/* NOTE: this caused timeline slided right, updates the progress houglass 1 box behind the actual on day value change */}
        {/* <div
            style={{
              color: "black",
              // width: "200px"
              minWidth: `${dayBoxWidth}px`,
              maxWidth: `${dayBoxWidth * 2}px`,
            }}>
            {taskData.name}
          </div> */}
        {children}
        <Popover
          id="mouse-over-popover"
          open={open}
          anchorEl={anchorEl}
          // anchorOrigin={{
          //   vertical: 'bottom',
          //   horizontal: 'left',
          // }}
          // transformOrigin={{
          //   vertical: 'bottom',
          //   horizontal: 'left',
          // }}
          // style={{  pointerEvents: 'none' }}
          // style={!open ? {  pointerEvents: 'none' } : {}}
          anchorReference="anchorPosition"
          anchorPosition={{ top: popoverPosition.y, left: popoverPosition.x }}
          onClose={handlePopoverClose}
          disableRestoreFocus
          PaperProps={{ classes: { root: classes.popoverPaper } }}
          className={clsx("mouse-over-popover", classes.timeLinePopoverParent)}>
          <div className={classes.timelinePopoverBody}>
            <div className={classes.tlPopoverHeader}>
              {taskParentName && (
                <LimitedTextWithTooltip text={taskParentName} limit={4} lineLimit={1}>
                  <Typography variant="span" className={classes.projectTag}>
                    {taskParentName}
                  </Typography>
                </LimitedTextWithTooltip>
              )}
              <div className={classes.taskTitleParent}>
                {taskParentName && (
                  <SvgIcon viewBox="0 0 10.939 14.372" className={classes.subTaskIcon}>
                    <SubTaskIcon />
                  </SvgIcon>
                )}
                <LimitedTextWithTooltip text={taskName} limit={12} lineLimit={3}>
                  <Typography variant="h6" className={classes.projectName}>
                    {taskName}
                  </Typography>
                </LimitedTextWithTooltip>
              </div>
              {taskProjectName && !taskParentName && (
                <div className={clsx(classes.taskLabelParent)}>
                  <SvgIcon viewBox="0 0 14 14">
                    <ResourceProjectIcon />
                  </SvgIcon>{" "}
                  <LimitedTextWithTooltip text={taskProjectName} limit={4} lineLimit={1}>
                    <span className={classes.taskLabel}>{taskProjectName}</span>
                  </LimitedTextWithTooltip>
                </div>
              )}
            </div>
            <div className={classes.tlPopoverBody}>
              <Grid container>
                <Grid item style={{ marginBottom: "0px" }} xs={12}>
                  <ul className={classes.projectHoursList}>
                    <li className={classes.projectHoursItem}>
                      <div className={classes.leftSide}>
                        <Typography variant="h6" className={classes.listLabel}>
                          Duration:
                        </Typography>
                      </div>
                      <div className={classes.rightSide}>
                        <DefaultTextField
                          // type="number"
                          size="small"
                          error={false}
                          // NOTE: disable direct edit
                          // onKeyDown={() => false}
                          formControlStyles={{
                            marginBottom: 0,
                            width: 60,
                            // maxWidth: isMonthView ? monthBoxWidth : 70,
                            // width: isMonthView ? monthBoxWidth : 70,
                          }}
                          // styles={[classes.projectHours]}
                          customInputClass={{ root: classes.popOverInput }}
                          // onChange={handleDurationChange}
                          // value={durationDays}
                          disabled={disableEditingDuration}
                          defaultProps={{
                            id: "taskDuration",
                            onChange: disableEditingDuration ? null : handleDurationChange,
                            autoFocus: true,
                            onBlur: disableEditingDuration ? null : handleDurationChangeBlur,
                            value: durationDays,
                            inputProps: {
                              type: "number",
                              step: 1,
                              min: 0,
                              className: `${classes.popOverActualInput}`,
                              // disabled: disableEditingDuration,
                              style: {
                                fontFamily: theme.typography.fontFamilyLato,
                                fontWeight: 500,
                                fontSize: 13,
                              },
                            },
                          }}
                        />
                        <Typography variant="h6" className={classes.unit}>
                          day(s)
                        </Typography>
                      </div>
                    </li>
                    <li className={classes.projectHoursItem}>
                      <div className={classes.leftSide}>
                        <Typography variant="h6" className={classes.listLabel}>
                          Effort:
                        </Typography>
                      </div>
                      <div className={classes.rightSide}>
                        <DefaultTextField
                          // type="number"
                          size="small"
                          error={false}
                          classes={classes.popoverInput}
                          // NOTE: disable direct edit
                          // onKeyDown={() => false}
                          formControlStyles={{
                            marginBottom: 0,
                            width: 60,
                            // maxWidth: isMonthView ? monthBoxWidth : 70,
                            // width: isMonthView ? monthBoxWidth : 70,
                          }}
                          // styles={[classes.projectHours]}
                          customInputClass={{ root: classes.popOverInput }}
                          // onChange={handleDurationChange}
                          // value={durationDays}
                          disabled={true}
                          value={(taskDailyWorkload || []).reduce(
                            (acc, cur) => acc + (cur.load || 0),
                            0
                          )}
                          // value={(taskDailyWorkload || []).filter(k => !isNaN(k)).reduce((acc, cur) => acc + cur.load, 0)}
                          defaultProps={{
                            id: "taskEffort",
                            inputProps: {
                              type: "number",
                              step: 1,
                              min: 0,
                              className: `${classes.popOverActualInput}`,
                              // disabled: disableEditingDuration,
                              style: {
                                fontFamily: theme.typography.fontFamilyLato,
                                fontWeight: 500,
                                fontSize: 13,
                              },
                            },
                          }}
                        />
                        <Typography variant="h6" className={classes.unit}>
                          hour(s)
                        </Typography>
                      </div>
                    </li>

                    {false && (
                      <>
                        <li className={classes.projectHoursItem}>
                          <div className={classes.leftSide}>
                            <Typography variant="h6" className={classes.listLabel}>
                              Successor:
                            </Typography>
                          </div>
                          <div className={classes.rightSide}>
                            <Typography variant="h6" className={classes.popOverText}>
                              Create User Flows
                            </Typography>
                          </div>
                        </li>
                        <li className={classes.projectHoursItem}>
                          <div className={classes.leftSide}>
                            <Typography variant="h6" className={classes.listLabel}>
                              Dependency:
                            </Typography>
                          </div>
                          <div className={classes.rightSide}>
                            <Typography variant="h6" className={classes.popOverText}>
                              Start-to-Start
                            </Typography>
                            <CustomTooltip
                              placement="right"
                              helptext={
                                "The successor task can only start after the predecessor task has started."
                              }>
                              <InfoIcon className={classes.popOverTooltip} />
                            </CustomTooltip>
                          </div>
                        </li>
                      </>
                    )}
                  </ul>
                </Grid>
              </Grid>
            </div>
          </div>
        </Popover>
        {showTaskNameWithTimeline && dailyWorkloadSortedByDayIndex.length > 0 && (
          <div
            className="task-label"
            style={{
              paddingLeft: 10,
              // width: "200px"
              minWidth: `${isMonthView ? dayBoxWidth * 3 : dayBoxWidth}px`,
              maxWidth: `${isMonthView ? dayBoxWidth * 6 : dayBoxWidth * 2}px`,
            }}>
            <LimitedTextWithTooltip text={taskName} limit={12} lineLimit={3}>
              {taskName}
            </LimitedTextWithTooltip>
          </div>
        )}
      </div>
    </>
  );
};

TaskTimelineLabelsPlusPopover.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
  children: PropTypes.element.isRequired,
  taskDurationDays: PropTypes.number,
  disableEditingDuration: PropTypes.bool,
  taskDailyWorkload: PropTypes.any,
  dailyWorkloadSortedByDayIndex: PropTypes.array,
  setDailyWorkloadSortedByDayIndex: PropTypes.func,
  setTaskDailyWorkload: PropTypes.func,
  enableTimelineDrag: PropTypes.bool,
  disableTimelineDrag: PropTypes.bool,
};

TaskTimelineLabelsPlusPopover.defaultProps = {
  classes: {},
  theme: {},
};

const WithInjectedHOCs = compose(
  withRouter,
  withStyles(styles, { withTheme: true })
)(TaskTimelineLabelsPlusPopover);
const Memoized = memo(WithInjectedHOCs);

export default Memoized;
