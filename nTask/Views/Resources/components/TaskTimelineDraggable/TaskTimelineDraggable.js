import React, { memo, useCallback, useContext, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import clsx from "clsx";
import { debounce } from "lodash";
import Draggable from "react-draggable";
import styles from "./styles";
import { cloneDeep } from "lodash";
import { formatMomentToDate } from "./../../temp-utils";
import {
  updateResourceTaskPlannedDatesAction,
  updateUnassignedTaskPlannedDatesAction,
  updateUnassignedTaskWorkloadsAction,
} from "../../../../redux/actions/resources";
import { ResourceContext, UnassignedTasksContext } from "../../contexts";
import moment from "moment";
import { updateResourceOrUnassignedTaskPlannedDatesThunk } from "../../../../redux/thunks/resources";
import { updateResourceTaskWorkloadsAction } from "./../../../../redux/actions/resources";
import PropTypes from "prop-types";
import { TaskContext } from "../../contexts/resources";
import { selectTaskData } from "../../../../redux/selectors/resources";

const TaskTimelineDraggable = (props) => {
  const {
    children,
    setTimelineDragPosition,
    setTimelineDragPositionBackup,
    timelineInitialPosition,
    setTimelineInitialPosition,
    timelineDragPosition,
    lockTimelineDrag,
    noDaysInTimeline = true,
    taskDailyWorkload,
    setTaskDailyWorkload,
  } = props;

  const dispatch = useDispatch();

  const { isUnassignedTask = false } = useContext(UnassignedTasksContext) || {};
  const resourceId = useContext(ResourceContext);
  const taskId = useContext(TaskContext);

  const dayBoxWidth = useSelector((s) => s.resources.uiGrid.dayProgressHourglassWidth);
  const taskData = useSelector((s) =>
    selectTaskData(s, {
      isUnassignedTask,
      resourceId,
      taskId,
    })
  );

  const [movingForward, setMovingFarward] = useState(true);
  const [isDragging, setIsDragging] = useState(false);

  let { plannedStartDate, plannedEndDate } = taskData || {};

  // Snapping: 50%
  const snappingPercentage = dayBoxWidth / 2;

  const updateTaskPlannedDatesBackend = (newStartDateMoment, newEndDateMoment) => {
    if (
      !newStartDateMoment ||
      !newStartDateMoment.isValid() ||
      !newEndDateMoment ||
      !newEndDateMoment.isValid()
    ) {
      return;
    }

    dispatch(
      updateResourceOrUnassignedTaskPlannedDatesThunk({
        taskId,
        startDate: newStartDateMoment.toISOString(),
        endDate: newEndDateMoment.toISOString(),
      })
    );
  };

  const debouncedApiCall = useCallback(debounce(updateTaskPlannedDatesBackend, 5000), []);

  if (!taskData) {
    return <>{children}</>;
  }

  const onStartTimeline = (event, startParams) => {
    event.preventDefault();
    event.stopPropagation();
  };

  const onDragTimeline = (event, dragParams) => {
    event.preventDefault();
    event.stopPropagation();

    // SAMPLE RESPONSE
    // {
    //   deltaX: 0,
    //   deltaY: NaN,
    //   lastX: 6,
    //   lastY: NaN,
    //   node: null,
    //   x: 6,
    //   y: NaN,
    // };

    if (lockTimelineDrag || noDaysInTimeline) {
      return;
    }
    // setTimelineDragPosition((currPosition) => currPosition + dragParams.deltaX);
    // setTimelineDragPositionBackup((currPosition) => currPosition + dragParams.deltaX);

    setMovingFarward(dragParams.x >= dragParams.lastX);

    setTimelineDragPosition(dragParams.x);
    setTimelineDragPositionBackup(dragParams.x);
  };

  const updateState = (addDays = 0) => {
    const newStartDate = moment(plannedStartDate).add(addDays, "days");
    const newEndDate = moment(plannedEndDate).add(addDays, "days");

    if (isUnassignedTask) {
      dispatch(updateUnassignedTaskPlannedDatesAction(taskId, newStartDate, newEndDate));
    } else {
      dispatch(updateResourceTaskPlannedDatesAction(resourceId, taskId, newStartDate, newEndDate));
    }

    // here slide day-wise workloads too
    const newWorkloads = cloneDeep(taskDailyWorkload);

    const loads = newWorkloads.map((k) => {
      const newMoment = k.moment.clone().add(addDays, "day");

      return {
        ...k,
        moment: newMoment,
        date: formatMomentToDate(newMoment),
      };
    });

    newWorkloads.splice(0, loads.length, ...loads);

    setTaskDailyWorkload(newWorkloads);

    if (isUnassignedTask) {
      dispatch(updateUnassignedTaskWorkloadsAction(taskId, newWorkloads));
    } else {
      dispatch(updateResourceTaskWorkloadsAction(resourceId, taskId, newWorkloads));
    }

    // TODO: avoid API call if same position after dragging as before
    debouncedApiCall(newStartDate, newEndDate);
  };

  const onDropTimeline = (event, dropParams) => {
    event.preventDefault();
    event.stopPropagation();

    // SAMPLE RESPONSE
    // {
    //   deltaX: 0,
    //   deltaY: NaN,
    //   lastX: 6,
    //   lastY: NaN,
    //   node: null,
    //   x: 6,
    //   y: NaN,
    // };

    if (lockTimelineDrag || noDaysInTimeline) {
      return;
    }

    const movedRightFromInitialPosition = dropParams.x > timelineInitialPosition;

    if (movedRightFromInitialPosition) {
      const pixelsMoved = dropParams.x - timelineInitialPosition;
      let boxesMoved = pixelsMoved / dayBoxWidth;

      boxesMoved = Math.floor(boxesMoved);

      // snapping applies here
      const boxSnapping = pixelsMoved % dayBoxWidth >= snappingPercentage;
      boxesMoved += boxSnapping ? 1 : 0;

      const newPositionOffset = boxesMoved * dayBoxWidth;

      const newPosition = timelineInitialPosition + newPositionOffset;

      // setTimelineDragPosition(() => newPosition);
      // setTimelineDragPositionBackup(() => newPosition);

      setTimelineDragPosition(newPosition);
      setTimelineDragPositionBackup(newPosition);
      setTimelineInitialPosition(newPosition);

      if (boxesMoved > 0) {
        updateState(boxesMoved);
      }
    } else {
      const pixelsMoved = timelineInitialPosition - dropParams.x;
      let boxesMoved = pixelsMoved / dayBoxWidth;

      boxesMoved = Math.floor(boxesMoved);

      // snapping applies here
      const boxSnapping = pixelsMoved % dayBoxWidth >= snappingPercentage;
      boxesMoved += boxSnapping ? 1 : 0;

      const newPositionOffset = boxesMoved * dayBoxWidth;

      const newPosition = timelineInitialPosition - newPositionOffset;

      // setTimelineDragPosition(() => newPosition);
      // setTimelineDragPositionBackup(() => newPosition);

      setTimelineDragPosition(newPosition);
      setTimelineDragPositionBackup(newPosition);
      setTimelineInitialPosition(newPosition);

      if (boxesMoved > 0) {
        updateState(-1 * boxesMoved);
      }
    }
  };

  const dragConfigOptions = {
    axis: "x",
    grid: lockTimelineDrag ? [0, 0] : [1, 0],
    // bounds: ".task-timeline-container", // disabled this line to clip the extensive timeline to display a part of it
    ...(timelineInitialPosition && { defaultPosition: { x: timelineInitialPosition, y: 0 } }),
    // position: { x: startInx * dayBoxWidth, y: 0 },
    position: { x: noDaysInTimeline ? 0 : timelineDragPosition, y: 0 },
    // "data-timelineDragSource": "timeline",
    enableUserSelectHack: false,

    // // NOTE: extra props to test/tryout
    // // REF: https://www.npmjs.com/package/react-draggable
    // cancel: string,
    // disabled: boolean,
    // handle: string,
    // offsetParent: HTMLElement,
    // onMouseDown: (e: MouseEvent) => void,
    // nodeRef: React.Ref<typeof React.Component>,
    // positionOffset: {x: number | string, y: number | string}
  };

  const dragHandlers = {
    onStart: onStartTimeline,
    onDrag: onDragTimeline,
    onStop: onDropTimeline,
  };

  return (
    <>
      <Draggable {...dragConfigOptions} {...dragHandlers}>
        <div className={clsx("task-timeline-draggable-container")}>{children}</div>
      </Draggable>
    </>
  );
};

TaskTimelineDraggable.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
  children: PropTypes.element.isRequired,
  setTimelineDragPosition: PropTypes.func,
  setTimelineDragPositionBackup: PropTypes.func,
  timelineInitialPosition: PropTypes.number,
  setTimelineInitialPosition: PropTypes.func,
  timelineDragPosition: PropTypes.number,
  lockTimelineDrag: PropTypes.bool,
  noDaysInTimeline: PropTypes.number,
  taskDailyWorkload: PropTypes.any,
  setTaskDailyWorkload: PropTypes.func,
};

TaskTimelineDraggable.defaultProps = {
  /** default props , calls when props are not coming from parent class undefined */
  classes: {},
  theme: {},
  noDaysInTimeline: true,
};

const WithInjectedHOCs = compose(
  withRouter,
  withStyles(styles, { withTheme: true })
)(TaskTimelineDraggable);

const Memoized = memo(WithInjectedHOCs);

export default Memoized;
