// @flow

import React from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import clsx from "clsx";
import styles from "./styles";

const StickyColumn = (props) => {
  const {
    classes,
    children,
    collapsed,

    expandedMinWidth,
    collapsedMinWidth,
    minWidthUnit,

    expandedFlexWidth,
    collapsedFlexWidth,
    flexWidthUnit,
  } = props;

  return (
    <>
      <div
        className={clsx({
          [classes.leftColumn]: true,
          [classes.collapsed]: collapsed,
        })}
        style={
          collapsed
            ? {
                width: `${collapsedMinWidth}${minWidthUnit}`,
                flexBasis: `${collapsedFlexWidth}${flexWidthUnit}`,
              }
            : {
                width: `${expandedMinWidth}${minWidthUnit}`,
                flexBasis: `${expandedFlexWidth}${flexWidthUnit}`,
              }
        }>
        {children}
      </div>
    </>
  );
};

StickyColumn.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
  children: PropTypes.element.isRequired,
  collapsed: PropTypes.bool,

  expandedMinWidth: PropTypes.number,
  collapsedMinWidth: PropTypes.number,
  minWidthUnit: PropTypes.string,

  expandedFlexWidth: PropTypes.number,
  collapsedFlexWidth: PropTypes.number,
  flexWidthUnit: PropTypes.string,
};

StickyColumn.defaultProps = {
  classes: {},
  theme: {},
  collapsed: false,
};

export default compose(withRouter, withStyles(styles, { withTheme: true }))(StickyColumn);
