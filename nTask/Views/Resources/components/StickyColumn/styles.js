const stickyColumnStyles = (theme) => ({
  leftColumn: {
    flex: "0 0",
    borderRight: "1px solid #DDDDDD",
    position: "sticky",
    left: 0,
    zIndex: 222,
    background: "#fff",
    transition: "0.4s ease all",
  },
  collapsed: {
    flex: "0 0",
    overflow: 'hidden'
  },
});

export default stickyColumnStyles;
