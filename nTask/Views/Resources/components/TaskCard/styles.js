const taskCardStyles = (theme) => ({
  // here objects of styles
  taskCard: {
    // no styles
    padding: 9,
    "min-height": "60px",
    display: "flex",
    alignItems: "center",
    gap: "5px",
    height: "100%",
    width: "250px",
    "&:hover .taskDragger": {
      opacity: "1",
    },
  },
  noMargin: {
    margin: "0px !important",
  },
  taskTitle: {
    color: "#171717",
    margin: "0px",
    fontSize: "14px",
    fontWeight: 400,
    lineHeight: "20px",
    fontFamily: "'lato'",
    textTransform: "capitalize",
    // display: "-webkit-box",
    // WebkitLineClamp: "3",
    // WebkitBoxOrient: "vertical",
    // overflow: "hidden",
    // textOverflow: "ellipsis",
    // wordBreak: "break-word",
  },
  taskLabelParent: {
    display: "flex",
    alignItems: "center",
    gap: "5px",
    "& svg": {
      color: "#7E7E7E",
      fontSize: "14px",
    },
  },
  taskLabel: {
    color: "#7E7E7E",
    fontSize: "13px",
    fontFamily: "'lato'",
    textOverflow: "ellipsis",
    textTransform: "capitalize",
    WebkitLineClamp: "1",
    WebkitBoxOrient: "vertical",
    overflow: "hidden",
    display: "-webkit-box",
  },
  taskCardLeft: {},
  taskCardRight: {
    flex: "1",
  },
  taskDragger: {
    padding: "0",
    background: "no-repeat",
    border: "0",
    color: "#969696",
    opacity: "0",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  hasParentTask: {
    // margin: "5px 0 0",
    // display: "flex",
    // gap: "4px",
    "& svg": {},
  },
  collapsedSidebarHide: {
    display: "none",
  },
  taskTitleParent: {
    display: "flex",
    alignItems: "flex-start",
    gap: "5px",
    margin: "0px",
    "& svg": {
      color: "#7E7E7E",
      fontSize: "14px",
      transform: "translateY(2px)",
    },
  },
});

export default taskCardStyles;
