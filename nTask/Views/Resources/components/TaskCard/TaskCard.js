// @flow

import React, { memo, useContext } from "react";
import { useEffect, useRef, useState } from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import styles from "./styles";
import clsx from "clsx";
import DragIndicatorIcon from "@material-ui/icons/DragIndicator";
import { useSelector } from "react-redux";
import { SvgIcon } from "@material-ui/core";
import ResourceProjectIcon from "../../../../components/Icons/ResourceProjectIcon";
import SubTaskIcon from "../../../../components/Icons/SubTaskIcon";
import PropTypes from "prop-types";
import LimitedTextWithTooltip from "../LimitedTextWithTooltip";
import { TaskContext } from "../../contexts/resources";
import { selectTaskData } from "../../../../redux/selectors/resources";
import { ResourceContext, UnassignedTasksContext } from "../../contexts";

const TaskCard = (props) => {
  const { classes, reorderHandleProps } = props;

  const { isUnassignedTask = false } = useContext(UnassignedTasksContext) || {};
  const resourceId = useContext(ResourceContext);
  const taskId = useContext(TaskContext);

  const sidebarCollapsed = useSelector((s) => s.resources.uiGrid.sidebarCollapsed);
  const taskInfo = useSelector((s) =>
    selectTaskData(s, {
      isUnassignedTask,
      resourceId,
      taskId,
    })
  );

  // Note: this is causing console warning of msg "Can't find draggable handle for Draggable with ID"
  // if (!taskInfo) {
  //   return <></>;
  // }

  return (
    <>
      <div className={classes.taskCard}>
        <div
          className={clsx(classes.taskCardLeft, {
            [classes.collapsedSidebarHide]: sidebarCollapsed,
          })}>
          <button
            type="button"
            className={clsx(classes.taskDragger, "taskDragger")}
            {...reorderHandleProps}>
            <DragIndicatorIcon />
          </button>
        </div>
        {taskInfo && (
          <div className={classes.taskCardRight}>
            {taskInfo.parentTask && (
              <div className={classes.taskLabelParent} style={{ marginBottom: 5 }}>
                <LimitedTextWithTooltip text={taskInfo.parentTask.name} limit={4} lineLimit={1}>
                  <span className={classes.taskLabel}>{taskInfo.parentTask.name}</span>
                </LimitedTextWithTooltip>
              </div>
            )}
            <div className={classes.taskTitleParent}>
              {taskInfo.parentTask && (
                <SvgIcon viewBox="0 0 10.939 14.372" className={classes.subTaskIcon}>
                  <SubTaskIcon />
                </SvgIcon>
              )}
              <LimitedTextWithTooltip text={taskInfo.name} limit={12} lineLimit={3}>
                <h5
                  className={clsx(classes.taskTitle, {
                    [classes.noMargin]: !taskInfo.project && !taskInfo.parentTask,
                    [classes.hasParentTask]: !!taskInfo.parentTask,
                  })}>
                  {taskInfo.name}
                </h5>
              </LimitedTextWithTooltip>
            </div>

            {taskInfo.project && !taskInfo.parentTask && (
              <div className={classes.taskLabelParent} style={{ marginTop: 6 }}>
                <SvgIcon viewBox="0 0 14 14">
                  <ResourceProjectIcon />
                </SvgIcon>{" "}
                <LimitedTextWithTooltip text={taskInfo.project.name} limit={4} lineLimit={1}>
                  <span className={classes.taskLabel}>{taskInfo.project.name}</span>
                </LimitedTextWithTooltip>
              </div>
            )}
          </div>
        )}
      </div>
    </>
  );
};

TaskCard.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
  taskInfo: PropTypes.object,
  reorderHandleProps: PropTypes.object,
};

TaskCard.defaultProps = {
  classes: {},
  theme: {},
  taskInfo: {},
};

const WithInjectedHOCs = compose(withRouter, withStyles(styles, { withTheme: true }))(TaskCard);

const Memoized = memo(WithInjectedHOCs);

export default Memoized;
