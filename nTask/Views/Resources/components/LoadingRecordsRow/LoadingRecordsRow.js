// @flow

import React from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import styles from "./styles";
import EmptyRow from "../EmptyRow";
import LinearProgress from "@material-ui/core/LinearProgress";
import PropTypes from "prop-types";

const LoadingRecordsRow = (props) => {
  const { children } = props;

  return (
    <>
      <EmptyRow>
        <div>
          {children || (
            <>
              <LinearProgress />
            </>
          )}
        </div>
      </EmptyRow>
    </>
  );
};

LoadingRecordsRow.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
  children: PropTypes.element,
};

LoadingRecordsRow.defaultProps = {
  classes: {},
  theme: {},
};

export default compose(withRouter, withStyles(styles, { withTheme: true }))(LoadingRecordsRow);
