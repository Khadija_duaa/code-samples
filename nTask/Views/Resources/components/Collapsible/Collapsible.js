// @flow

import React, { memo, useState, useEffect, useRef } from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import styles from "./styles";
import PropTypes from "prop-types";
import {
  decreaseOpenCollapsiblesCountAction,
  increaseOpenCollapsiblesCountAction,
} from "../../../../redux/actions/resources";
import { useDispatch } from "react-redux";

function Collapsible(props) {
  const { classes, summaryRow, detailsPanel, collapseBtnRef } = props;

  const [expanded, setExpanded] = useState(false);
  const [accordionBodyHeight, setAccordionBodyHeight] = useState(0);

  const accordionBodyRef = useRef(null);
  const dispatch = useDispatch();

  const summaryRowMarkup = typeof summaryRow === "function" ? summaryRow(expanded) : summaryRow;
  const detailsPanelMarkup =
    typeof detailsPanel === "function" ? detailsPanel(expanded) : detailsPanel;

  useEffect(() => {
    // console.info("event; rerendered", cloneDeep(props));
    if (collapseBtnRef?.current) {
      // collapseBtnRef.current.onclick = toggleAccordion;
      // collapseBtnRef.current.onclickcapture = toggleAccordion;
      // collapseBtnRef.current.addEventListener("click", toggleAccordion, false);
      collapseBtnRef.current.addEventListener("click", toggleAccordion);
      // collapseBtnRef.current.addEventListener("click", toggleAccordion, { capture: false });

      // console.info("event; props", { ...collapseBtnRef.current });

      return () => {
        // collapseBtnRef.current.onclick = null;
        // collapseBtnRef.current.onclickcapture = toggleAccordion;
        // collapseBtnRef.current.onclickcapture = null;
        // collapseBtnRef.current.removeEventListener("click", toggleAccordion, false);
        collapseBtnRef.current.removeEventListener("click", toggleAccordion);
        // collapseBtnRef.current.removeEventListener("click", toggleAccordion, { capture: false });
      };
    }
  }, [collapseBtnRef, collapseBtnRef.current]);

  useEffect(() => {
    calculatePanelHeight();
  }, [expanded, accordionBodyRef?.current?.innerHTML]);

  useEffect(() => {
    expanded
      ? dispatch(increaseOpenCollapsiblesCountAction())
      : dispatch(decreaseOpenCollapsiblesCountAction());
  }, [expanded]);

  const calculatePanelHeight = () => {
    setAccordionBodyHeight(accordionBodyRef.current.clientHeight);
    // setAccordionBodyHeight(() => accordionBodyRef.current.clientHeight);
  };

  const toggleAccordion = (event) => {
    // event.stopPropagation();
    // console.log("event; collapsible click", event.target.closest);

    if (event.target.closest("*[data-resourceDrawer='cell']")) {
      return;
    }

    // return false;
    // event.preventDefalt();
    // event.stopPropagation();

    setExpanded((exp) => !exp);

    // dispatch(setOpenCollapsiblesCount(showTaskNameWithTimeline));

    // calculatePanelHeight();
  };

  return (
    <>
      {/* summary row */}
      {summaryRowMarkup}
      {/* summary row end */}

      {/* details panel */}
      <div
        className={classes.taskPanel}
        style={
          expanded
            ? { height: /* accordionBodyHeight */ "100%", overflow: "inherit", opacity: 1 }
            : // ? { height: accordionBodyHeight, overflow: "inherit", opacity: 1 }
              {}
        }>
        <div className={classes.taskList} ref={accordionBodyRef}>
          {detailsPanelMarkup}
        </div>
      </div>
      {/* details panel */}
    </>
  );
}

Collapsible.propTypes = {
  collapseBtnRef: PropTypes.shape({ current: PropTypes.instanceOf(Element) }).isRequired,
  summaryRow: PropTypes.oneOfType([PropTypes.func, PropTypes.node]),
  detailsPanel: PropTypes.oneOfType([PropTypes.func, PropTypes.node]),
};

Collapsible.defaultProps = {
  classes: {},
  theme: {},
  collapseBtnRef: null,
  summaryRow: <></>,
  detailsPanel: <></>,
};

const WithInjectedHOCs = compose(withRouter, withStyles(styles, { withTheme: true }))(Collapsible);
const Memoized = memo(WithInjectedHOCs);

export default Memoized;
