const collapsibleStyles = (theme) => ({
  // collapse side bar styles starts here
  taskPanel: {
    height: 0,
    overflow: "hidden",
    transition: "0.4s ease all",
    opacity: 0,
  },
  taskList: {
    // minHeight: 99,
    transition: "0.4s ease all",
  },
});

export default collapsibleStyles;
