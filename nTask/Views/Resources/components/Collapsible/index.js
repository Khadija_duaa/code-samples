import Collapsible from "./Collapsible";

export { default as Collapsible } from "./Collapsible";
export * from "./Collapsible";

export default Collapsible;
