const styles = (theme) => ({
  drawer: {
    position: "fixed",
    zIndex: 1000,
  },
  drawerContentRD: {
    height: "100%",
    position: "relative",
  },
  capacityButton: {
    padding: 0,
    border: "none",
  },
  resourceCapacity: {
    color: "#7E7E7E",
    fontSize: "13px",
    fontFamily: "Lato, Regular",
    lineHeight: "1.5",
    "& span": {
      color: "#171717",
    },
  },
  capacityParent: {
    display: "flex",
    alignItems: "center",
    "& *": {
      // transition: "0.4s all ease-in-out",
    },
    "&:hover *": {
      color: "#0096FF",
      fill: "#0096FF",
    },
  },
  openPanelIcon: {
    fontSize: 14,
    color: "#7E7E7E",
    marginLeft: 5,
  },
  drawerHeading: {
    fontFamily: "Lato",
    fontWeight: "Bold",
    fontSize: "16px",
    lineHeight: "19px",
  },
  borderBottomDH: {
    borderBottom: "1px solid #dddddd",
  },
  drawerHeader: {
    padding: "10px 20px",
  },
  drawerBody: {
    padding: "12px 20px",
    // overflowY: "scroll",
    maxHeight: "85vh",
  },
  drawerRcCard: {
    display: "flex",
    alignItems: "center",
    gap: "10px",
    padding: "12px",
    border: "1px solid #DCE1E4",
    borderRadius: "8px",
    marginBottom: "16px",
  },
  resourceName: {
    fontFamily: "Lato, Regular",
    fontSize: "14px",
    fontWeight: "400",
    color: "#171717",
    lineHeight: "1.5",
  },
  resourceDesignation: {
    color: "#646464",
    fontSize: "13px",
    fontFamily: "Lato, Regular",
    lineHeight: "1.5",
  },
  activeProjects: {
    fontFamily: "'Lato'",
    fontSize: "14px",
    color: "#171717",
    "& span": {
      fontWeight: "bold",
    },
  },
  projectHoursList: {
    padding: "0",
    listStyle: "none",
  },
  projectHoursItem: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    boxShadow: "0px 1px 0px #EAEAEA",
    paddingBottom: "15px",
    marginBottom: "15px",
  },
  leftSide: {
    display: "flex",
    alignItems: "center",
    color: "#646464",
    gap: "10px",
    "& svg": {
      fontSize: "14px",
    },
  },
  projectName: {
    fontFamily: "'lato'",
    fontSize: "14px",
    color: "#171717",
  },
  rightSide: {},
  projectHours: {
    width: "80px",
    height: "32px",
    border: "1px solid #DDDDDD",
    borderRadius: "4px",
    padding: "0px 8px",
    fontFamily: "'lato'",
    fontSize: "14px",
  },
  flexContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  capacityLoad: {
    color: "#171717",
    fontFamily: "'lato'",
    fontSize: "14px",
    fontWeight: "bold",
  },
  flexEnd: {
    justifyContent: "flex-end",
  },
  cancelButton: {
    textTransform: "capitalize",
    fontFamily: "'lato'",
    fontSize: "13px",
  },
  greenButton: {
    boxShadow: "none",
    background: "#00CC90",
    color: "#fff",
    textTransform: "capitalize",
    fontSize: "13px",
  },
  loaderContainer: {
    width: "100%",
    padding: "20px 0",
    display: "flex",
    justifyContent: "center",
  },
  borderBtn: {
    padding: "3px 8px 3px 4px",
    borderRadius: "4px",
    height: 32,
    marginRight: 8,
    fontFamily: "lato",
    color: "#171717",
    fontSize: "13px",
    "& svg": {
      fontSize: 18,
      color: "#7F8F9A",
    },
  },
});

export default styles;
