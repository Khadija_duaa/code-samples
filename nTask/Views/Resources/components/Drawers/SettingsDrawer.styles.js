const styles = (theme) => ({
  root: {
    color: theme.palette.secondary.dark,
    position: "relative",
    "& *": {
      fontFamily: theme.typography.fontFamilyLato,
    },
  },
  drawer: {
    position: "fixed",
    zIndex: 1000,
  },
  drawerContentRD: {
    height: "100%",
    position: "relative",
  },
  borderBtn: {
    padding: "3px 8px 3px 4px",
    borderRadius: "4px",
    height: 32,
    marginRight: 8,
    fontFamily: "lato",
    color: "#171717",
    fontSize: "13px",
    "& svg": {
      fontSize: 18,
      color: "#7F8F9A",
    },
  },
  drawerHeading: {
    fontFamily: "Lato",
    fontWeight: "Bold",
    fontSize: "16px",
    lineHeight: "19px",
  },
  borderBottomDH: {
    borderBottom: "1px solid #dddddd",
  },
  drawerHeader: {
    padding: "10px 20px",
  },
  drawerBody: {
    padding: "12px 20px",
    // overflowY: "scroll",
    maxHeight: "85vh",
  },
  projectHoursList: {
    padding: "0",
    listStyle: "none",
  },
  projectHoursItem: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    boxShadow: "0px 1px 0px #EAEAEA",
    paddingBottom: "15px",
    marginBottom: "15px",
  },
  leftSide: {
    display: "flex",
    alignItems: "center",
    color: "#646464",
    gap: "10px",
    "& svg": {
      fontSize: "14px",
    },
  },
  projectName: {
    fontFamily: "'lato'",
    fontSize: "14px",
    color: "#171717",
  },
  rightSide: {},

  drawerSelect: {
    border: "1px solid #D8DEE1",
    height: "32px",
    width: "83px",
    color: "#171717",
    fontSize: "13px",
    borderRadius: "4px",
    padding: "0 4px",
  },
  downloadButton: {
    color: "#171717",
    fontSize: "13px",
    textTransform: "capitalize",
    "& span": {
      display: "flex",
      gap: "5px",
    },
  },
});

export default styles;
