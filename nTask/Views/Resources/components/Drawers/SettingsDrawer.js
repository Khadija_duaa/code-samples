import React, { memo, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Grid, SvgIcon, Typography } from "@material-ui/core";
import CustomDrawer from "../../../../components/Drawer/CustomDrawer";
import { IconSlidePanel } from "../../../../components/Icons/IconSlidePanel";
import styles from "./ResourcesDrawer.styles";
import CustomIconButton from "../../../../components/Buttons/CustomIconButton";
import withStyles from "@material-ui/core/styles/withStyles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import SettingsIcon from "@material-ui/icons/Settings";
import CustomButton from "../../../../components/Buttons/CustomButton";
import DefaultSwitch from "../../../../components/Form/Switch";
import ResourceDownloadIcon from "../../../../components/Icons/ResourceDownloadIcon";
import { clsx } from "clsx";
import {
  setSettingsShowTaskNameWithTimelineAction,
  setSettingsTaskColorAction,
} from "../../../../redux/actions/resources";
import {
  createOrUpdateUserSettingsThunk,
  fetchUserSettingsThunk,
} from "../../../../redux/thunks/resources";
import PropTypes from "prop-types";
import { TaskColorDropdown } from "../Dropdowns";
import {
  selectShowTaskNameWithTimelineSetting,
  selectTaskColorSetting,
} from "../../../../redux/selectors/resources";
import { debounce } from "lodash";

const SettingsDrawer = (props) => {
  const { classes } = props;

  const dispatch = useDispatch();

  const showTaskNameWithTimeline = useSelector(selectShowTaskNameWithTimelineSetting);
  const taskTimelineColor = useSelector(selectTaskColorSetting);

  const [flag, setFlag] = useState(false);
  const [open, setOpen] = useState(false);

  useEffect(() => {
    dispatch(fetchUserSettingsThunk());
    setFlag(false);
  }, []);

  useEffect(() => {
    debouncedApiCall();
  }, [showTaskNameWithTimeline, taskTimelineColor]);

  const debouncedApiCall = debounce(() => {
    dispatch(createOrUpdateUserSettingsThunk());
  }, 2000);

  const onTaskColorChange = (newValue) => {
    dispatch(setSettingsTaskColorAction(newValue));
  };

  const onTaskSwitchChange = () => {
    dispatch(setSettingsShowTaskNameWithTimelineAction(!showTaskNameWithTimeline));
  };

  return (
    <div className={classes.root}>
      <CustomButton
        onClick={() => setOpen(!open)}
        style={{
          padding: "4px 8px 4px 8px",
          borderRadius: "4px",
          display: "flex",
          justifyContent: "space-between",
          minWidth: "auto",
          whiteSpace: "nowrap",
          marginRight: 10,
        }}
        className={classes.borderBtn}
        btnType={"white"}
        variant="contained">
        <SettingsIcon />
      </CustomButton>

      <CustomDrawer
        hideCloseIcon
        drawerProps={{
          className: classes.drawer,
          PaperProps: {
            height: "100%",
            border: " 1px solid #E7E9EB",
          },
        }}
        open={open}
        closeDrawer={() => setOpen(false)}
        hideHeader={true}
        drawerWidth={400}>
        <div className={classes.drawerContentRD}>
          <Grid
            container
            justify="space-between"
            alignItems="center"
            className={clsx(classes.borderBottomDH, classes.drawerHeader)}>
            <Grid item>
              <Typography variant="h3" className={classes.drawerHeading}>
                Settings
              </Typography>
            </Grid>
            <Grid item>
              <CustomIconButton
                style={{ padding: 5, borderRadius: 0 }}
                iconBtn
                onClick={() => setOpen(false)}>
                <IconSlidePanel />
              </CustomIconButton>
            </Grid>
          </Grid>

          <Grid container className={classes.drawerBody}>
            <Grid item xs={12} style={{ marginBottom: "16px" }}>
              <div className={classes.projectHoursListParent}>
                <ul className={classes.projectHoursList}>
                  <li className={classes.projectHoursItem}>
                    <div className={classes.leftSide}>
                      <Typography variant="h6" className={classes.projectName}>
                        Color Tasks by
                      </Typography>
                    </div>
                    <div className={classes.rightSide}>
                      <TaskColorDropdown {...{ taskTimelineColor, onChange: onTaskColorChange }} />
                    </div>
                  </li>
                  <li className={classes.projectHoursItem}>
                    <div className={classes.leftSide}>
                      <Typography variant="h6" className={classes.projectName}>
                        Show Task Name on Timeline
                      </Typography>
                    </div>
                    <div className={classes.rightSide}>
                      <DefaultSwitch
                        size={"small"}
                        checked={showTaskNameWithTimeline}
                        onChange={onTaskSwitchChange}
                        rootProps={{
                          button: false,
                          disableRipple: true,
                          style: { cursor: "default" },
                        }}
                        // value="darkModeChecked"
                      />
                    </div>
                  </li>
                  {false && (
                    <li className={classes.projectHoursItem}>
                      <div className={classes.leftSide}>
                        <Typography variant="h6" className={classes.projectName}>
                          Export Workload in Excel
                        </Typography>
                      </div>
                      <div className={classes.rightSide}>
                        <Button variant="outlined" className={classes.downloadButton}>
                          <SvgIcon viewBox="0 0 16 17.778" style={{ fontSize: 18 }}>
                            <ResourceDownloadIcon className={classes.qckFilterIconSvg} />
                          </SvgIcon>
                          Download
                        </Button>
                      </div>
                    </li>
                  )}
                </ul>
              </div>
            </Grid>
          </Grid>
        </div>
      </CustomDrawer>
    </div>
  );
};

SettingsDrawer.propTypes = {
  classes: PropTypes.object,
};

SettingsDrawer.defaultProps = { classes: {}, theme: {} };

const WithInjectedHOCs = compose(
  withRouter,
  withStyles(styles, { withTheme: true })
)(SettingsDrawer);

const Memoized = memo(WithInjectedHOCs);

export default Memoized;
