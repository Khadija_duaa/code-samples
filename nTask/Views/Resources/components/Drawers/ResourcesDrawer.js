import React, { memo, useEffect, useState } from "react";
import { Button, CircularProgress, Grid, SvgIcon, Typography } from "@material-ui/core";
import CustomDrawer from "../../../../components/Drawer/CustomDrawer";
import { IconSlidePanel } from "../../../../components/Icons/IconSlidePanel";
import styles from "./ResourcesDrawer.styles";
import CustomIconButton from "../../../../components/Buttons/CustomIconButton";
import withStyles from "@material-ui/core/styles/withStyles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import CustomButton from "../../../../components/Buttons/CustomButton";
import LayersIcon from "@material-ui/icons/Layers";
import FolderIcon from "@material-ui/icons/Folder";
import CustomAvatar from "../../../../components/Avatar/Avatar";
import OpenPanelIcon from "../../../../components/Icons/OpenPanelIcon";
import CustomTooltip from "../../../../components/Tooltip/Tooltip";
import clsx from "clsx";
import {
  selectResourceTotalCapacityForSelectedTimelinePeriod,
  selectResourceTotalWorkloadForSelectedTimelinePeriod,
} from "./../../../../redux/selectors/resources";
import { isValidFunction, isValidUrl } from "../../temp-utils";
import { useDispatch, useSelector } from "react-redux";
import instance from "../../../../redux/instance";
import { baseURL } from "../../constants";
import { withSnackbarNotifs } from "../../../../hoc";
import PropTypes from "prop-types";
import DefaultTextField from "../../../../components/Form/TextField";
import { cloneDeep } from "lodash";
import { isFloat, isEmpty } from "validator";
import { updateResourceTotalCapacityAction } from "../../../../redux/actions/resources";

const ResourcesDrawer = (props) => {
  const { classes, theme, showSnackbar, resource } = props;

  const dispatch = useDispatch();

  const totalWorkloadForSelectedTimelinePeriod = useSelector((s) =>
    selectResourceTotalWorkloadForSelectedTimelinePeriod(s, { resourceId: resource.id })
  );
  // const totalCapacityForSelectedTimelinePeriod = useSelector((s) =>
  //   selectResourceTotalCapacityForSelectedTimelinePeriod(s, { resourceId: resource.id })
  // );

  const [flag, setFlag] = useState(false);
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [projectsList, setProjectsList] = useState([]);
  const [projectsListBackup, setProjectsListBackup] = useState([]);
  const [weeklyCapacity, setWeeklyCapacity] = useState(0);
  const [dailyCapacity, setDailyCapacity] = useState(0);

  useEffect(() => {
    const total = projectsList.reduce((acc, curr) => acc + Number(curr.capacityHours), 0);

    setWeeklyCapacity(total);
  }, [projectsList]);

  useEffect(() => {
    setFlag(false);

    if (!open) {
      return;
    }

    setLoading(true);

    fetchDataFromApi({
      successCallback: (data) => {
        setProjectsList(data.list);
        setProjectsListBackup(data.list);

        setWeeklyCapacity(data.weeklyCapacity);
        setDailyCapacity(data.dailyCapacity);
      },
      failureCallback: (error) =>
        showSnackbar(error.data?.message || ((error.status || error.statusText) && `${error.status}: ${error.statusText}`) || "Backend service seems offline!", "error"),
      finallyCallback: () => setLoading(false),
    });
  }, [open]);

  const fetchDataFromApi = ({
    successCallback = null,
    failureCallback = null,
    finallyCallback = null,
  } = {}) => {
    if (!resource.id) {
      return;
    }

    const body = Object.assign({}, { resourceId: [resource.id] });
    const params = {};

    return instance(baseURL)
      .post(`/api/resources/capacity`, body, { params })
      .then((axiosResponse) => axiosResponse.status === 200 && axiosResponse.data)
      .then((response) => {
        // Note: API missing these properties
        // if (!response.success || !response.message?.toLowerCase().includes("created")) {
        //   return;
        // }

        // return response?.data;
        return response;
      })
      .then((data = {}) => {
        // sample response
        // {
        //   data: [
        //     {
        //       projectId: "a7139b3917a9475da862b65250759bb9",
        //       capacity: "40",
        //       projectName: "sample project - 1",
        //     },
        //     {
        //       projectId: "08406abfa1da4d4eba3de45fcb2cc005",
        //       capacity: "40",
        //       projectName: "Project for test - 2",
        //     },
        //   ],
        // }

        const parsed = data.data?.map((k) => ({
          id: k.projectId,
          name: k.projectName,
          capacityHours: Number(k.capacity) || 0,
        }));

        return {
          list: parsed,
          weeklyCapacity: data.weeklyCapacity || 0,
          dailyCapacity: data.dailyCapacity || 0,
        };
      })
      .then((data) => {
        if (isValidFunction(successCallback)) {
          successCallback(data);
        }
      })
      .catch((error) => {
        console.error("exception error:", error);
        if (isValidFunction(failureCallback)) {
          failureCallback(error.response);
        }
      })
      .finally(() => {
        if (isValidFunction(finallyCallback)) {
          finallyCallback();
        }
      });
  };

  const toggleDrawer = () => setOpen((o) => !o);

  const handleDurationChange = (projectId) => (e) => {
    const inputValue = e.target.value;

    let typeCheck = isFloat(inputValue, {
      min: 0,
      // max: 24,
      allow_leading_zeroes: false,
    });
    const empty = isEmpty(inputValue);
    let validated = typeCheck || empty;
    if (!validated) {
      return;
    }

    // update local state
    updateLocalState(inputValue, projectId);

    // update backend
    // debouncedApiCall({ projectId, updatedHours: Number(inputValue) });
  };

  const handleDurationChangeBlur = (projectId) => (e) => {};

  function updateLocalState(inputValue, projectId) {
    const list = cloneDeep(projectsList);

    const inx = list.findIndex((k) => k.id === projectId);

    const [removed] = list.splice(inx, 1);

    removed.capacityHours = inputValue;

    list.splice(inx, 0, removed);

    setProjectsList(list);
  }

  function updateBackend({
    successCallback = null,
    failureCallback = null,
    finallyCallback = null,
  } = {}) {
    if (!resource.id) {
      return;
    }

    const oldMap = new Map(projectsListBackup.map((k) => [k.id, k.capacityHours]));
    const newMap = new Map(projectsList.map((k) => [k.id, Number(k.capacityHours)]));

    const changed = projectsList.filter((k) => newMap.get(k.id) !== oldMap.get(k.id));

    changed.forEach((k) => {
      const body = Object.assign(
        {
          resourceId: resource.id,
          projectId: [k.id],
        },
        // Note: diagnosed the issue with Rizwan khan and adil bhai on 2023.03.07.Tue - need to send string instead of number
        //       bcz there is string in 'saveProjectSetting' API call while adding resource to project
        { hours: Number(k.capacityHours).toString() }
      );
      const params = {};

      return instance(baseURL)
        .put(`/api/resources/project/estimatedhours`, body, { params })
        .then((axiosResponse) => axiosResponse.status === 200 && axiosResponse.data)
        .then((response) => {
          // SAMPLE RESPONSE
          //   {
          //     "statusCode": "S001",
          //     "message": "Updated",
          //     "success": true,
          //     "data": null
          // }
          if (!response.success || !response.message?.toLowerCase().includes("updated")) {
            return;
          }

          dispatch(updateResourceTotalCapacityAction(resource.id, weeklyCapacity));

          return response?.data;
        })
        .then((data) => {
          if (isValidFunction(successCallback)) {
            successCallback(data);
          }
        })
        .catch((error) => {
          console.error("exception error:", error);
          if (isValidFunction(failureCallback)) {
            failureCallback(error.response);
          }
        })
        .finally(() => {
          if (isValidFunction(finallyCallback)) {
            finallyCallback();
          }
        });
    });
  }

  return (
    <div className={classes.ResourcesDrawerMain}>
      {/* <CustomButton
                onClick={() => setOpen(!open)}
                className={classes.capacityButton}
                >
                <LayersIcon  />
            </CustomButton> */}
      <CustomTooltip placement="top" helptext={"View Capacity"}>
        <CustomButton
          onClick={toggleDrawer}
          // style={{ pointerEvents: "none" }}
          className={classes.capacityButton}
          btnType={"white"}
          variant="contained"
          data-resourceDrawer="cell">
          <div className={classes.capacityParent}>
            <Typography className={classes.resourceCapacity} variant="p">
              Capacity:&nbsp;
              <span>
                {totalWorkloadForSelectedTimelinePeriod}h / {resource.totalCapacity}h
              </span>
            </Typography>
            <SvgIcon viewBox="0 0 13.5 13.499" className={classes.openPanelIcon}>
              <OpenPanelIcon />
            </SvgIcon>
          </div>
        </CustomButton>
      </CustomTooltip>

      <CustomDrawer
        hideCloseIcon
        drawerProps={{
          className: classes.drawer,
          PaperProps: {
            height: "100%",
            border: " 1px solid #E7E9EB",
          },
          "data-resourceDrawer": "cell",
        }}
        open={open}
        closeDrawer={() => setOpen(false)}
        hideHeader={true}
        drawerWidth={400}>
        <div className={classes.drawerContentRD}>
          <Grid
            container
            justify="space-between"
            alignItems="center"
            className={clsx(classes.borderBottomDH, classes.drawerHeader)}>
            <Grid item>
              <Typography variant="h3" className={classes.drawerHeading}>
                Resource Weekly Capacity
              </Typography>
            </Grid>
            <Grid item>
              <CustomIconButton
                style={{ padding: 5, borderRadius: 0 }}
                iconBtn
                onClick={() => setOpen(false)}>
                <IconSlidePanel />
              </CustomIconButton>
            </Grid>
          </Grid>
          <Grid container className={classes.drawerBody}>
            <Grid item xs={12}>
              <div className={classes.drawerRcCard}>
                <div>
                  <CustomAvatar
                    otherMember={{
                      imageUrl: isValidUrl(resource.imageUrl) ? resource.imageUrl : "",
                      fullName: resource.name,
                      lastName: "",
                      email: resource.email,
                      isOnline: false,
                      isOwner: false,
                    }}
                    size="small"
                  />
                </div>
                <div>
                  <Typography className={classes.resourceName} variant="h5">
                    {resource.name || resource.email}
                  </Typography>
                  <Typography className={classes.resourceDesignation} variant="h6">
                    {resource.designation}
                  </Typography>
                </div>
              </div>
            </Grid>
            {loading && (
              <>
                <div className={classes.loaderContainer}>
                  <CircularProgress className={classes.progress} />
                </div>
              </>
            )}
            {!loading && (
              <>
                <Grid item style={{ marginBottom: "16px" }} xs={12}>
                  <Typography variant="h6" className={classes.activeProjects}>
                    <span>{projectsList.length || 0}</span> active projects
                  </Typography>
                </Grid>
                <Grid item style={{ marginBottom: "16px" }} xs={12}>
                  <div className={classes.projectHoursListParent}>
                    <ul className={classes.projectHoursList}>
                      {projectsList.map((k, inx) => (
                        <li className={classes.projectHoursItem}>
                          <div className={classes.leftSide}>
                            <FolderIcon />
                            <Typography variant="h6" className={classes.projectName}>
                              {k.name}
                            </Typography>
                          </div>
                          <div className={classes.rightSide}>
                            <DefaultTextField
                              size="small"
                              error={false}
                              formControlStyles={{
                                marginBottom: 0,
                                // width: 60,
                              }}
                              // styles={[classes.projectHours]}
                              // customInputClass={{ root: classes.popOverInput }}
                              onChange={handleDurationChange(k.id)}
                              onBlur={handleDurationChangeBlur(k.id)}
                              value={k.capacityHours}
                              defaultProps={{
                                id: "resourceCapacityForProject",
                                // onChange: handleDurationChange(k.id),
                                autoFocus: inx === 0,
                                // onBlur: handleDurationChangeBlur(k.id),
                                // value: k.capacityHours,
                                inputProps: {
                                  type: "number",
                                  step: 1,
                                  // min: 0,
                                  className: `${classes.projectHours}`,
                                  style: {
                                    fontFamily: theme.typography.fontFamilyLato,
                                    fontWeight: 500,
                                    fontSize: 13,
                                  },
                                },
                              }}
                            />
                          </div>
                        </li>
                      ))}
                    </ul>
                  </div>
                </Grid>

                <Grid item style={{ marginBottom: "16px" }} xs={12}>
                  <div className={classes.flexContainer}>
                    <Typography variant="h6" className={classes.capacityLoad}>
                      Weekly Capacity
                    </Typography>
                    <Typography variant="h6" className={classes.capacityLoad}>
                      {weeklyCapacity} hours
                    </Typography>
                  </div>
                </Grid>
                {false && (
                  <Grid item style={{ marginBottom: "20px" }} xs={12}>
                    <div className={classes.flexContainer}>
                      <Typography variant="h6" className={classes.capacityLoad}>
                        Daily Capacity
                      </Typography>
                      <Typography variant="h6" className={classes.capacityLoad}>
                        {dailyCapacity} hours
                      </Typography>
                    </div>
                  </Grid>
                )}
                <Grid item xs={12}>
                  <div className={clsx(classes.flexContainer, classes.flexEnd)}>
                    <Button
                      variant="outlined"
                      className={classes.cancelButton}
                      style={{ marginRight: "20px" }}
                      onClick={() => setOpen(false)}>
                      Cancel
                    </Button>
                    <Button
                      variant="contained"
                      className={classes.greenButton}
                      disabled={loading}
                      onClick={() => {
                        updateBackend();
                        setOpen(false);
                      }}>
                      Save Changes
                    </Button>
                  </div>
                </Grid>
              </>
            )}
          </Grid>
        </div>
      </CustomDrawer>
    </div>
  );
};

ResourcesDrawer.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
  showSnackbar: PropTypes.func,
  resource: PropTypes.object,
};

ResourcesDrawer.defaultProps = { classes: {}, theme: {} };

const WithInjectedHOCs = compose(
  withRouter,
  withSnackbarNotifs,
  withStyles(styles, { withTheme: true })
)(ResourcesDrawer);

const Memoized = memo(WithInjectedHOCs);

export default Memoized;
