import React, { Fragment, memo } from "react";
import {
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
  Grid,
  SvgIcon,
  Typography,
} from "@material-ui/core";
import styles from "./UnscheduledTasksList.styles";
import withStyles from "@material-ui/core/styles/withStyles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { ExpandMore as ExpandMoreIcon } from "@material-ui/icons";
import ResourceProjectIcon from "../../../../../components/Icons/ResourceProjectIcon";
import PropTypes from "prop-types";
import NoRecordsRow from "../../NoRecordsRow";
import { Draggable } from "react-beautiful-dnd";

const UnscheduledTasksList = (props) => {
  const { classes, list } = props;

  if (list.length === 0) {
    return (
      <div className={classes.noRecordsContainer}>
        <NoRecordsRow />
      </div>
    );
  }

  return list.map((k) => (
    <Fragment key={k.id}>
      <Grid item xs={12} style={{ marginBottom: "16px" }}>
        <ExpansionPanel className={classes.projectExpansionPanel} defaultExpanded>
          <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon />}
            style={{ padding: 0, minHeight: "40px", height: "40px" }}>
            <Typography variant="h5" className={classes.expHeading}>
              {k.id && (
                <>
                  <SvgIcon viewBox="0 0 14 14">
                    <ResourceProjectIcon />
                  </SvgIcon>
                  &nbsp;
                </>
              )}
              {k.id ? k.name : "Non-Project Tasks"}&nbsp;(
              {k.tasks.length || 0})
            </Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails className={classes.expPanelDetails}>
            <div className={classes.unsTaskParent}>
              {k.tasks.length === 0 ? (
                <>
                  <NoRecordsRow />
                </>
              ) : (
                k.tasks.map((t, tInx) => (
                  <Draggable
                    draggableId={`draggable-unscheduled-task-${t.id}`}
                    index={tInx}
                    key={t.id}>
                    {(provided, snapshot) => {
                      return (
                        <div
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}>
                          <div className={classes.unsTaskCard}>
                            <Typography variant="p" className={classes.unsText}>
                              {t.title}
                            </Typography>
                          </div>
                        </div>
                      );
                    }}
                  </Draggable>
                ))
              )}
            </div>
          </ExpansionPanelDetails>
        </ExpansionPanel>
      </Grid>
    </Fragment>
  ));
};

UnscheduledTasksList.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
};

UnscheduledTasksList.defaultProps = { classes: {}, theme: {} };

const WithInjectedHOCs = compose(
  withRouter,
  withStyles(styles, { withTheme: true })
)(UnscheduledTasksList);

const Memoized = memo(WithInjectedHOCs);

export default Memoized;
