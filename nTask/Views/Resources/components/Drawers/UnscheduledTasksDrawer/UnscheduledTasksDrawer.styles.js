const styles = (theme) => ({
  root: {
    color: theme.palette.secondary.dark,
    position: "relative",
    "& *": {
      fontFamily: theme.typography.fontFamilyLato,
    },
  },
  borderBtn: {
    padding: "3px 8px 3px 4px",
    borderRadius: "4px",
    height: 32,
    marginRight: 8,
    fontFamily: "lato",
    color: "#171717",
    fontSize: "13px",
    "& svg": {
      fontSize: 18,
      color: "#7F8F9A",
    },
  },
});

export default styles;
