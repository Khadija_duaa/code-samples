const styles = (theme) => ({
  searchContainer: {
    position: "relative",
    width: "100%",
  },
  searchIcon: {
    color: "#969696",
    fontSize: "20px",
    position: "absolute",
    zIndex: 1,
    top: "50%",
    transform: "translateY(-50%)",
    left: "10px",
  },
  searchInput: {
    "& input": {
      paddingLeft: "32px",
      color: "#7E7E7E",
      height: "40px",
    },
  },
  greyBox: {
    borderRadius: "6px",
    backgroundColor: "#EAEAEA",
    display: "flex",
    alignItems: "center",
    gap: "5px",
    padding: "10px 10px",
  },
  greyBoxText: {
    fontSize: "13px",
    "& span": {
      fontWeight: "bold",
    },
  },
  drawerBody: {
    padding: "12px 20px",
    // overflowY: "scroll",
    maxHeight: "85vh",
  },
  loaderContainer: {
    width: "100%",
    padding: "20px 0",
    display: "flex",
    justifyContent: "center",
  },
});

export default styles;
