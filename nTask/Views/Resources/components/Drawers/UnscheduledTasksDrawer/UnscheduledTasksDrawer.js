import React, { memo, useEffect, useState } from "react";
import styles from "./UnscheduledTasksDrawer.styles";
import withStyles from "@material-ui/core/styles/withStyles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import CustomButton from "../../../../../components/Buttons/CustomButton";
import PropTypes from "prop-types";
import ErrorBoundary from "../../ErrorBoundary";
import UnscheduledTasksDrawerContent from "./UnscheduledTasksDrawerContent";

const UnscheduledTasksDrawer = (props) => {
  const { classes } = props;

  const [flag, setFlag] = useState(false);
  const [open, setOpen] = useState(false);

  useEffect(() => {
    setFlag(false);
  }, []);

  function openDrawer() {
    setOpen(true);
  }

  function closeDrawer() {
    setOpen(false);
  }

  return (
    <ErrorBoundary>
      <div className={classes.root}>
        <CustomButton
          onClick={openDrawer}
          className={classes.borderBtn}
          btnType={"white"}
          variant="contained">
          <span className={classes.qckFilterLbl}>Unscheduled</span>
        </CustomButton>

        {open && <UnscheduledTasksDrawerContent {...{ closeDrawer, open }} />}
      </div>
    </ErrorBoundary>
  );
};

UnscheduledTasksDrawer.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
};

UnscheduledTasksDrawer.defaultProps = { classes: {}, theme: {} };

const WithInjectedHOCs = compose(
  withRouter,
  withStyles(styles, { withTheme: true })
)(UnscheduledTasksDrawer);

const Memoized = memo(WithInjectedHOCs);

export default Memoized;
