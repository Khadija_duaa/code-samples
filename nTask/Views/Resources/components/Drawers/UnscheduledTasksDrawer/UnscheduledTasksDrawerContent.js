import React, { memo } from "react";
import { Grid, Typography } from "@material-ui/core";
import CustomDrawer from "../../../../../components/Drawer/CustomDrawer";
import { IconSlidePanel } from "../../../../../components/Icons/IconSlidePanel";
import styles from "./UnscheduledTasksDrawerContent.styles";
import CustomIconButton from "../../../../../components/Buttons/CustomIconButton";
import withStyles from "@material-ui/core/styles/withStyles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { clsx } from "clsx";
import PropTypes from "prop-types";
import { Droppable } from "react-beautiful-dnd";
import UnscheduledTasks from "./UnscheduledTasks";

const UnscheduledTasksDrawerContent = (props) => {
  const { classes, open, closeDrawer } = props;

  return (
    <CustomDrawer
      hideCloseIcon
      drawerProps={{
        className: classes.drawer,
        PaperProps: {
          height: "100%",
          border: " 1px solid #E7E9EB",
        },
      }}
      open={open}
      closeDrawer={closeDrawer}
      hideHeader={true}
      drawerWidth={400}>
      <Droppable droppableId={"droppable-tasks-unscheduled"}>
        {(provided, snapshot) => {
          console.info("drag:", provided, snapshot);

          return (
            <div
              className={clsx("unscheduled-tasks-container")}
              ref={provided.innerRef}
              {...provided.droppableProps}
              style={{
                backgroundColor: snapshot.isDraggingOver ? "skyblue" : "white",
              }}>
              <div className={classes.drawerContentRD}>
                <Grid
                  container
                  justify="space-between"
                  alignItems="center"
                  className={clsx(classes.borderBottomDH, classes.drawerHeader)}>
                  <Grid item>
                    <Typography variant="h3" className={classes.drawerHeading}>
                      Unscheduled Tasks
                    </Typography>
                  </Grid>
                  <Grid item>
                    <CustomIconButton
                      style={{ padding: 5, borderRadius: 0 }}
                      iconBtn
                      onClick={closeDrawer}>
                      <IconSlidePanel />
                    </CustomIconButton>
                  </Grid>
                </Grid>

                <UnscheduledTasks />
              </div>
              {provided.placeholder}
            </div>
          );
        }}
      </Droppable>
    </CustomDrawer>
  );
};

UnscheduledTasksDrawerContent.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
  closeDrawer: PropTypes.func,
  open: PropTypes.bool,
};

UnscheduledTasksDrawerContent.defaultProps = { classes: {}, theme: {} };

const WithInjectedHOCs = compose(
  withRouter,
  withStyles(styles, { withTheme: true })
)(UnscheduledTasksDrawerContent);

const Memoized = memo(WithInjectedHOCs);

export default Memoized;
