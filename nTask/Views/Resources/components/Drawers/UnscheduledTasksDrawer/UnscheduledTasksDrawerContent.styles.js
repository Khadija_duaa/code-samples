const styles = (theme) => ({
  drawer: {
    position: "fixed",
    zIndex: 1000,
  },
  drawerContentRD: {
    height: "100%",
    position: "relative",
  },
  drawerHeading: {
    fontFamily: "Lato",
    fontWeight: "Bold",
    fontSize: "16px",
    lineHeight: "19px",
  },
  borderBottomDH: {
    borderBottom: "1px solid #dddddd",
  },
  drawerHeader: {
    padding: "10px 20px",
  },
});

export default styles;
