import React, { memo, useEffect, useState } from "react";
import { Grid, Typography, CircularProgress } from "@material-ui/core";
import styles from "./UnscheduledTasks.styles";
import withStyles from "@material-ui/core/styles/withStyles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { Info as InfoIcon, Search as SearchIcon } from "@material-ui/icons";
import SearchInput from "react-search-input";
import Scrollbars from "react-custom-scrollbars";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import { fetchUnscheduledTasksDataListThunk } from "../../../../../redux/thunks/resources";
import { withSnackbarNotifs } from "../../../../../hoc";
import { debounce } from "lodash";
import UnscheduledTasksList from "./UnscheduledTasksList";

const UnscheduledTasks = (props) => {
  const { classes, showSnackbar } = props;

  const dispatch = useDispatch();

  const unscheduledTasksDataList = useSelector((s) => s.resources.data.unscheduledTasksDataList);
  const dataExists = useSelector((s) => s.resources.dataFlags.unscheduledTasksDataListExists);

  const [visibleList, setVisibleList] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (!dataExists) {
      setLoading(true);

      dispatch(
        fetchUnscheduledTasksDataListThunk({
          // successCallback:
          failureCallback: apiCallFailure,
          finallyCallback: () => setLoading(false),
        })
      );
    }
  }, [dataExists]);

  useEffect(() => {
    setVisibleList(unscheduledTasksDataList);
  }, [unscheduledTasksDataList]);

  const onSearch = (term) => {
    if (!term || term.length === 0) {
      setVisibleList(unscheduledTasksDataList);
      return;
    }

    const termLower = term.toLowerCase();

    const filteredList = unscheduledTasksDataList
      .map((k) => ({
        ...k,
        tasks: k.tasks.filter((t) => t.title?.toLowerCase()?.includes(termLower)),
      }))
      .filter((k) => k.tasks.length > 0);

    setVisibleList(filteredList);
  };

  const onDebouncedSearch = debounce(onSearch, 100);

  function apiCallFailure(error) {
    showSnackbar(error.data?.message || ((error.status || error.statusText) && `${error.status}: ${error.statusText}`) || "Backend service seems offline!", "error");
  }

  if (loading) {
    return (
      <div className={classes.loaderContainer}>
        <CircularProgress className={classes.progress} />
      </div>
    );
  }

  return (
    <Scrollbars
      autoHide={false}
      autoHeight
      autoHeightMin={100}
      autoHeightMax={"100%"}
      className="scrollCon">
      <Grid container className={classes.drawerBody}>
        <Grid item xs={12} style={{ marginBottom: "16px" }}>
          <div className={classes.greyBox}>
            <InfoIcon style={{ fontSize: "14px" }} />
            <Typography variant="h6" className={classes.greyBoxText}>
              <span>Drag & drop</span> task to the workload calendar.
            </Typography>
          </div>
        </Grid>

        <Grid item xs={12} style={{ marginBottom: "16px" }}>
          <div className={classes.searchContainer}>
            <SearchIcon className={classes.searchIcon} />
            <SearchInput
              className={`${classes.searchInput} HtmlInput`}
              maxLength="80"
              onChange={onDebouncedSearch}
              placeholder={"Search or create new task"}
            />
          </div>
        </Grid>

        <UnscheduledTasksList list={visibleList} />
      </Grid>
    </Scrollbars>
  );
};

UnscheduledTasks.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
};

UnscheduledTasks.defaultProps = { classes: {}, theme: {} };

const WithInjectedHOCs = compose(
  withRouter,
  withSnackbarNotifs,
  withStyles(styles, { withTheme: true })
)(UnscheduledTasks);

const Memoized = memo(WithInjectedHOCs);

export default Memoized;
