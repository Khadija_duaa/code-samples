const styles = (theme) => ({
  expPanelDetails: {
    padding: "0",
    width: "100%",
  },
  unsTaskParent: {
    width: "100%",
  },
  unsTaskCard: {
    width: "100%",
    height: "39px",
    border: "1px solid #DDDDDD",
    borderRadius: "4px",
    display: "flex",
    alignItems: "center",
    padding: "0 10px",
    cursor: "pointer",
    boxShadow: "0px 1px 2px #00000014",
    marginBottom: "10px",
    "&:last-child": {
      marginBottom: 0,
    },
  },
  unsText: {
    fontSize: "14px",
    color: "#202020",
  },
  expHeading: {
    color: "#171717",
    fontSize: "14px",
    display: "flex",
    alignItems: "center",
    "& svg": {
      fontSize: "18px",
      color: "#969696",
      marginRight: "8px",
    },
  },
  projectExpansionPanel: {
    boxShadow: "none",
    background: "transparent",
  },
  noRecordsContainer: {
    width: "100%",
  },
});

export default styles;
