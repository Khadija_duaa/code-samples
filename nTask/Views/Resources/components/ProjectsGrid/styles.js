const projectsGridStyles = (theme) => ({
  // here objects of styles
  rcContainer: {
    border: "1px solid #DDDDDD",
    borderRadius: 6,
    position: "relative",
    overflowX: "scroll",
  },
  rcHeader: {
    width: "100%",
  },
  arrowForward: {
    //   position: "absolute",
    //   top: 35,
    //   right: -14,
    padding: 4,
    backgroundColor: "white",
    border: "1px solid #E7E9EB",
    borderRadius: "50%",
    cursor: "pointer",
    color: "#646464",
    zIndex: 1,
    "&:hover": {
      backgroundColor: "#0090ff",
      border: "1px solid #0090ff",
      color: "white",
    },
  },
  slotsWraper: {
    display: "flex",
  },
  monthSlots: {
    width: "100%",
    borderBottom: "1px solid #DDDDDD",
    marginBottom: "-1px",
    borderLeft: "1px solid #DDDDDD",
    marginLeft: "-1px",
  },
  monthNamesRow: {
    display: 'flex',
    flexDirection: 'row'
  },
  month: {
    padding: 7,
    borderBottom: "1px solid #DDDDDD",
    borderRight: "1px solid #DDDDDD",
    boxSizing: "border-box",
    fontFamily: "Lato",
    color: "#171717",
    fontSize: "14px"
  },
  daySlots: {
    display: "flex",
  },
  slot: {
    width: "100%",
    // minWidth: 100,
    textAlign: "center",
    padding: 3,
    boxSizing: "border-box",
    borderLeft: "1px solid #DDDDDD",
    marginLeft: "-1px",
    fontFamily: "Lato",
    color: "#646464",
    fontSize: "14px"
  },
  resourcePanel: {
    width: "100%",
  },
  sideBarHeader: {
    "display": "flex",
    "alignItems": "center",
    "justifyContent": "space-between",
    "padding": "10px",
    "height": "100%",
    "& p": {
      margin: 0,
      "color": "#171717",
      "fontSize": "15px",
      "fontFamily": "'lato'"
    }
  }
});

export default projectsGridStyles;
