// @flow

import React, { useState, useEffect, memo } from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import styles from "./styles";
import { withSnackbarNotifs } from "../../../../hoc";
import ProjectRow from "../ProjectRow";
import { useSelector } from "react-redux";
import NoRecordsRow from "../NoRecordsRow";
import { resetGridProjectsDataListExistsAction } from "./../../../../redux/actions/resources";
import { useDispatch } from "react-redux";
import { fetchGridProjectsDataListThunk } from "./../../../../redux/thunks/resources";
import LoadingRecordsRow from "../LoadingRecordsRow";
import PropTypes from "prop-types";

const ProjectsGrid = (props) => {
  const { showSnackbar } = props;

  const dispatch = useDispatch();

  const [projectsList, setProjectsList] = useState([]);
  const [loading, setLoading] = useState(false);

  const dataExists = useSelector((s) => s.resources.dataFlags.projectsDataListExists);
  const workloadViewType = useSelector((s) => s.resources.uiGrid.workloadViewType);
  const projectsDataList = useSelector((s) => s.resources.data.projectsDataList);
  const timelinePeriod = useSelector((s) => s.resources.filters.calendarTimelinePeriod);

  // useEffect(() => {
  //   // dispatch(resetGridDataActionCreator());
  //   // dispatch(resetGridProjectsDataListAction());
  // }, []);

  useEffect(() => {
    //   setLoading(!projectsDataList || projectsDataList.length <= 0);

    setLoading(true);
    setProjectsList(projectsDataList);
    setLoading(false);
  }, [projectsDataList]);

  const apiCallFailure = (error) =>
    showSnackbar(error.data?.message || ((error.status || error.statusText) && `${error.status}: ${error.statusText}`) || "Backend service seems offline!", "error");

  const apiCallFinally = () => setLoading(false);

  const fetchDataFromApi = () => {
    if (!dataExists) {
      setLoading(true);
      dispatch(
        fetchGridProjectsDataListThunk({
          failureCallback: apiCallFailure,
          finallyCallback: apiCallFinally,
        })
      );
    }
  };

  useEffect(() => {
    // dispatch(resetGridProjectsDataListAction());

    fetchDataFromApi();
  }, [workloadViewType, timelinePeriod, dataExists]);

  useEffect(() => {
    dispatch(resetGridProjectsDataListExistsAction());
  }, [timelinePeriod]);

  if (loading) {
    return <LoadingRecordsRow />;
  }

  if (!projectsList || projectsList.length <= 0) {
    return <NoRecordsRow />;
  }

  return projectsList.map((project, inx) => <ProjectRow key={inx} projectInfo={project} />);
};

ProjectsGrid.propTypes = {
  classes: PropTypes.object,
  theme: PropTypes.object,
  showSnackbar: PropTypes.func,
};

ProjectsGrid.defaultProps = {
  classes: {},
  theme: {},
};

const WithInjectedHOCs = compose(
  withRouter,
  withSnackbarNotifs,
  withStyles(styles, { withTheme: true })
)(ProjectsGrid);

const Memoized = memo(WithInjectedHOCs);

export default Memoized;
