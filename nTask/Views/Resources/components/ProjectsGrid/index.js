import ProjectsGrid from "./ProjectsGrid";

export { default as ProjectsGrid } from "./ProjectsGrid";
export * from "./ProjectsGrid";

export default ProjectsGrid;
