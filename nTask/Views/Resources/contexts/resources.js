import { createContext } from "react";

const ResourceContext = createContext();
ResourceContext.displayName = "ResourceContext";

const ProjectContext = createContext();
ProjectContext.displayName = "ProjectContext";

const UnassignedTasksContext = createContext();
UnassignedTasksContext.displayName = "UnassignedTasksContext";

const TaskContext = createContext();
TaskContext.displayName = "TaskContext";

export { ResourceContext, ProjectContext, UnassignedTasksContext, TaskContext };
