const sidebarStyles = (theme) => ({
  //   time line section starts here
  timeLineRow: {
    position: "relative",
    height: 40,
    borderTop: "1px solid red",
    borderBottom: "1px solid red",
  },
  cellOuter: {},
  cell: {},
  // prototype starts here
  mainContainer: {
    padding: "15px 0px 0px 15px",
  },
  /* prototype starts here */
  rcContainer: {
    border: "1px solid #DDDDDD",
    borderRadius: 6,
    position: "relative",
    overflowX: "scroll",
    marginTop: 25
  },


  daySlots: {
    display: "flex",
  },
  /* header setings starts here */
  rcHeader: {
    width: "100%",
  },
  arrowForward: {
    //   position: "absolute",
    //   top: 35,
    //   right: -14,
    fontSize: "12px",
    "position": "relative",
    "right": "-2px"
    // backgroundColor: "white",
    // border: "1px solid #E7E9EB",
    // borderRadius: "50%",
    // cursor: "pointer",
    // color: "#929292",
    // zIndex: 1,
    // "&:hover": {
    //   backgroundColor: "#0090ff",
    //   border: "1px solid #0090ff",
    //   color: "white",
    // },
  },
  slotsWraper: {
    display: "flex",
  },
  month: {
    padding: 7,
    borderBottom: "1px solid #DDDDDD",
    boxSizing: "border-box",
  },
  slot: {
    width: "100%",
    // minWidth: 100,
    textAlign: "center",
    padding: 3,
    boxSizing: "border-box",
    borderLeft: "1px solid #DDDDDD",
    marginLeft: "-1px",
  },
  monthSlots: {
    width: "100%",
    borderBottom: "1px solid #DDDDDD",
    marginBottom: "-1px",
    borderLeft: "1px solid #DDDDDD",
    marginLeft: "-1px",
  },
  /* progress starts here */

  progressCnt: {
    backgroundColor: "#99EAD2",
    height: 90,
    borderRadius: 5,
    padding: 5,
    boxSizing: "border-box",
  },
  /* resource panel starts here */
  resourcePanel: {
    width: "100%",
  },
  /* task timeline starts here */  // collapse side bar styles starts here
  /* task timeline starts here */ // collapse side bar styles starts here
  expandedBody: {
    height: 200,
  },
  // header starts here
 
});

export default sidebarStyles;
