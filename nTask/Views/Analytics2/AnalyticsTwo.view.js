import React, { useState, useEffect, useMemo, useContext, useRef } from "react";
import CustomDatePicker from "../../components/DatePicker/DatePicker/DatePicker";
import withStyles from "@material-ui/core/styles/withStyles";
import { compose } from "redux";
import { connect } from "react-redux";
import moment from "moment";
import CustomButton from "../../components/Buttons/CustomButton";
import AnalyticsStyles from "./Analytics.style";
import CustomMultiSelectDropdown from "../../components/Dropdown/CustomMultiSelectWithSearchBarDropdown/Dropdown";
import { generateWorkspaceData } from "../../helper/generateTasksOverviewDropdownData";
import CustomListItem from "../../../src/components/ListItem/CustomListItem";
import ArrowDropDown from "@material-ui/icons/ArrowDropDown";
import DropdownMenu from "../../components/Dropdown/DropdownMenu";
import helper from "../../helper";
import IconRefresh from "../../components/Icons/IconRefresh";
import SvgIcon from "@material-ui/core/SvgIcon";
// import CustomTooltip from "../../components/Tooltip/Tooltip";
import CustomIconButton from "../../components/Buttons/IconButton";
import SelectionMenu from "../../components/Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import DownloadIcon from "../../components/Icons/DownloadIcon";
import PdfProjectIcon from "../../components/Icons/PdfProjectIcon";
import ExcelProjectIcon from "../../components/Icons/ExcelProjectIcon";
import ImageIcon from "../../components/Icons/ImageIcon";


const createdDateFilterTypes = [
  {
    label: "today",
    value: "Today",
  },
  {
    label: "yesterday",
    value: "Yesterday",
  },
  {
    label: "currentWeek",
    value: "This Week",
  },
  {
    label: "currentMonth",
    value: "This Month",
  },
  {
    label: "custom",
    value: "Custom",
  },
];

function AnalyticsTwo(props) {
  const {
    classes,
    theme,
    profileState: { workspace, activeTeam },
    dashboardId
  } = props;

  
  const userToken = helper.getToken();
  // const userToken =
  //   "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZWFtSWQiOiJiMjg2YjQxODIyZDg0NWFlOTY0ODlhNjBiMDYyODBhZSJ9.PpMJjl6nwaSG_23nAlE3hG2PXM2-Cp_JcyyqBNFz-2E"; //localStorage.getItem("token");

  let startDate = moment().format("YYYY-MM-DD");
  let endDate = moment().format("YYYY-MM-DD");

  const [testStartDate, setTestStartDate] = useState(startDate);
  const [testEndDate, setTestEndDate] = useState(endDate);
  const [firstStartDate, setFirstStartDate] = useState("");
  const [firstEndDate, setFirstEndDate] = useState("");
  const [wSpaceData, setWSpaceData] = useState([]);
  const [filterOption, setFilterOption] = useState(createdDateFilterTypes[0].value);
  const [openDropDown, setOpenDropDown] = useState(false);
  const [openExportDropDown, setOpenExportDropDown] = useState(false);

  const anchorEl = useRef(null);
  const anchorRef = useRef(null);

  const onSelectDateItems = (e, option) => {
    e.stopPropagation();
    setFilterOption(option);
    setOpenDropDown(true);
    creatingDates(option);
  };

  const handleStartDate = date => {
    if (date) {
      const fDate = moment(date).format("YYYY-MM-DD");
      setTestStartDate(fDate);
      setFirstStartDate(fDate);
    }
  };

  const handleEndDate = date => {
    if (date) {
      const eDate = moment(date).format("YYYY-MM-DD");
      setTestEndDate(eDate);
      setFirstEndDate(eDate);
    }
  };

  const handleUpdateFilter = () => {
    loadBoldBI(testStartDate,testEndDate,wSpaceData);
  };

  const handleSelectedData = option => {
    setWSpaceData(option);
  };

  const handleClear = () => {
    // reset all filter values and send null to boldBI
    setFirstStartDate('');
    setFirstEndDate('');
    // setWSpaceData([]);
    setTestStartDate(startDate);
    setTestEndDate(endDate);
    setFilterOption(createdDateFilterTypes[0].label);
    // const workspaceSelectedItem = workspacesDropdownData[0];
    // setWSpaceData([workspaceSelectedItem]);
    loadBoldBI(startDate,endDate,wSpaceData);
  };
  const creatingDates = item => {
    let startDate = "";
    let endDate = "";

    switch (item) {
      case "Today":
        startDate = moment().format("YYYY-MM-DD");
        endDate = moment().format("YYYY-MM-DD");
        setTestStartDate(startDate);
        setTestEndDate(endDate);
        setFirstStartDate('');
        setFirstEndDate('');
        break;
      case "Yesterday":
        var date = new Date();
        date.setDate(date.getDate() - 1);
        startDate = moment(date).format("YYYY-MM-DD");
        endDate = moment(date).format("YYYY-MM-DD");
        setTestStartDate(startDate);
        setTestEndDate(endDate);
        setFirstStartDate('');
        setFirstEndDate('');
        break;
      case "This Week":
        const weekstartDate = new Date(moment().startOf("week"));
        const weekEndDate = new Date(moment().endOf("week"));
        startDate = moment(weekstartDate).format("YYYY-MM-DD");
        endDate = moment(weekEndDate).format("YYYY-MM-DD");
        setTestStartDate(startDate);
        setTestEndDate(endDate);
        setFirstStartDate('');
        setFirstEndDate('');
        break;
      case "This Month":
        const monthStartDate = new Date(moment().startOf("month"));
        const monthEndDate = new Date(moment().endOf("month"));
        startDate = moment(monthStartDate).format("YYYY-MM-DD");
        endDate = moment(monthEndDate).format("YYYY-MM-DD");
        setTestStartDate(startDate);
        setTestEndDate(endDate);
        setFirstStartDate('');
        setFirstEndDate('');
        break;
      default:
        console.log("No  Information Found");
        break;
    }
  };
  // Generate workspace dropdown data
  const workspacesDropdownData = useMemo(() => {
    let activeWorkspaces = workspace.filter(w => w.isActive); /** only show active worksapces  */
    let wd = generateWorkspaceData(activeWorkspaces);
    return wd;
  }, workspace);

  const loadBoldBI = (sDate=startDate, eDate=endDate, wsData=[]) => {
    let workspaceIds;

    workspaceIds = wsData.map(x => {
      return x.obj.teamId;
    }).toString();

    // Task startdate and endDate and token overwrite lines
    const reportTokenTask =
      userToken != null && userToken != "" ? "&&jwtTokenTask=" + userToken : "";
    const reportStartDateTask =
      testStartDate != null && testStartDate != "" ? "&&startDateTask=" + testStartDate : "";
    const reportEndDateTask =
      testEndDate != null && testEndDate != "" ? "&&endDateTask=" + testEndDate : "";
    const reportworkSpaceIdTask =
      workspaceIds != null && workspaceIds != "" ? "&&workspaceIdTask=" + workspaceIds : "";

    // project startdate and endDate and token overwrite lines
    const reportTokenProject =
      userToken != null && userToken != "" ? "&&jwtTokenProject=" + userToken : "";
    const reportStartDateProject =
      testStartDate != null && testStartDate != "" ? "&&startDateProject=" + testStartDate : "";
    const reportEndDateProject =
      testEndDate != null && testEndDate != "" ? "&&endDateProject=" + testEndDate : "";
    const reportworkSpaceIdProject =
      workspaceIds != null && workspaceIds != "" ? "&&workspaceIdProject=" + workspaceIds : "";

    // Issues startdate and endDate and token overwrite lines
    const reportTokenIssue =
      userToken != null && userToken != "" ? "&&jwtTokenIssue=" + userToken : "";
    const reportStartDateIssue =
      testStartDate != null && testStartDate != "" ? "&&startDateIssue=" + testStartDate : "";
    const reportEndDateIssue =
      testEndDate != null && testEndDate != "" ? "&&endDateIssue=" + testEndDate : "";
    const reportworkSpaceIdIssue =
      workspaceIds != null && workspaceIds != "" ? "&&workspaceIdIssue=" + workspaceIds : "";

    // Timesheetdaily startdate and endDate and token overwrite lines

    const reportTokenTimesheetDaily =
      userToken != null && userToken != "" ? "&&jwtTokenTimesheetDaily=" + userToken : "";
    const reportStartDateTimesheetDaily =
      testStartDate != null && testStartDate != "" ? "&&startDateDaily=" + testStartDate : "";
    const reportEndDateTimesheetDaily =
      testEndDate != null && testEndDate != "" ? "&&endDateDaily=" + testEndDate : "";
    const reportworkSpaceIdTimesheetDaily =
      workspaceIds != null && workspaceIds != "" ? "&&workspaceIdDaily=" + workspaceIds : "";

    // Risk startdate and endDate and token overwrite lines

    const reportStartDateRisk =
      testStartDate != null && testStartDate != "" ? "&&startDateRisk=" + testStartDate : "";

    const reportEndDateRisk =
      testEndDate != null && testEndDate != "" ? "&&endDateRisk=" + testEndDate : "";

    const reportworkSpaceIdRisk =
      workspaceIds != null && workspaceIds != "" ? "&&workspaceIdRisk=" + workspaceIds : "";

    const reportTokenRisk =
      userToken != null && userToken != "" ? "&&jwtTokenRisk=" + userToken : "";

    // actual startDate and endDate and token
    const reportStartDate =
      testStartDate != null && testStartDate != "" ? "&&startDate=" + sDate : "";
    const reportEndDate =
      testEndDate != null && testEndDate != "" ? "&&endDate=" + eDate : "";
    const reportworkSpaceId =
      workspaceIds != null && workspaceIds != "" ? "&&workspaceId=" + workspaceIds : "&&workspaceId=";
    const reportjwtToken = userToken != null && userToken != "" ? "&&token=" + userToken : "";

     let boldbiEmbedInstance = BoldBI.create({
      serverUrl: "https://bip.ntaskmanager.com/bi/site/1",
      // dashboardId: "8637b43b-1d27-4505-b812-5cb85a431067",
      dashboardId: dashboardId,
      embedContainerId: "dashboard-container", // This should be the container id where you want to embed the dashboard
      filterParameters: `${reportStartDate}${reportEndDate}${reportworkSpaceId}${reportjwtToken}`,
      dashboardSettings: {
        showDashboardParameter: false,
        enableTheme: false,
        enableFilterOverview: false,
        showHeader: false,
      },
      embedType: BoldBI.EmbedType.Component,
      environment: BoldBI.Environment.Enterprise,
      mode: BoldBI.Mode.View,
      height: "95%",
      width: "100%",
      authorizationServer: {
        url: `${REPORTING_BASE_URL}/embeddetail/get/`,
      },
      expirationTime: "100000",
    });
    boldbiEmbedInstance.loadDashboard();
    console.log(boldbiEmbedInstance);
  };

  useEffect(() => {
    const workspaceSelectedItem = workspacesDropdownData[0];
    setWSpaceData([workspaceSelectedItem]);
    loadBoldBI(startDate,endDate,[workspaceSelectedItem]);
  }, [dashboardId]);

  const isDescendant = (parent, child) => {
    let node = child.parentNode;
    while (node != null) {
      if (node == parent) {
        return true;
      }
      node = node.parentNode;
    }
    return false;
  };

//BoldBi Exports 
const handleOpenExport=(e)=>{
  setOpenExportDropDown(!openExportDropDown);
}
const exportDashboardAsExcel=()=>{
    let instance = BoldBI.getInstance("dashboard-container"); 
    let exportInformation ={'dashboardId': dashboardId};
    instance.exportDashboardAsExcel(exportInformation);
    handleOpenExport()
}
  const exportDashboardAsPdf=()=>{
    let instance = BoldBI.getInstance("dashboard-container"); 
    let exportInformation ={'dashboardId':dashboardId}
    instance.exportDashboardAsPdf(exportInformation)
    handleOpenExport()
  }
  const exportDashboardAsImage=()=>{
    let instance = BoldBI.getInstance("dashboard-container"); 
    let exportInformation ={'dashboardId':dashboardId};
    instance.exportDashboardAsImage(exportInformation);
    handleOpenExport()
  }

//Refresh Dashboard 

  const refreshDashboard=()=>{
    let instance = BoldBI.getInstance("dashboard-container");
    instance.refreshDashboard();   
  }

  return (
    <>
      {openDropDown && (
        <DropdownMenu
          open={openDropDown}
          closeAction={e => {
            let parent = document.querySelector("#dateFilterSelectionMenu");
            let children = e.target;
            const checkIsDescendant = isDescendant(parent, children);
            if (checkIsDescendant) {
              return;
            } else {
              setOpenDropDown(false);
            }
          }}
          anchorEl={anchorEl.current}
          placement="bottom-start"
          disablePortal={false}
          id={"dateFilterSelectionMenu"}
          size={"medium"}>
          {createdDateFilterTypes.map(t => {
            const isSelected = filterOption == t.value;
            return (
              <CustomListItem
                isSelected={isSelected}
                rootProps={{ onClick: e => onSelectDateItems(e, t.value) }}>
                <span>{t.value}</span>
              </CustomListItem>
            );
          })}

          {filterOption && filterOption === "Custom" ? (
            <>
              <div
                style={{
                  margin: "2px 6px",
                  border: "1px solid #DDDDDD",
                  borderRadius: "4px",
                  padding: 6,
                }}>
                <CustomDatePicker
                  // date={testStartDate}
                  date={firstStartDate}
                  label={"From:"}
                  PopperProps={{ disablePortal: true, size: null }}
                  icon={false}
                  timeInput={false}
                  onSelect={handleStartDate}
                  disabled={false}
                  deleteIcon={false}
                  placeholder={"Select Date"}
                  containerProps={{ style: { alignItems: "center" } }}
                  btnProps={{
                    style: {
                      background: "transparent",
                      border: "none",
                      padding: 0,
                      textAlign: "left",
                    },
                  }}
                  labelProps={{
                    style: {
                      width: "auto",
                      marginTop: 0,
                      marginRight: 5,
                      fontSize: "14px",
                    },
                  }}
                  closeOnDateSelect={true}
                />
              </div>
              <div
                style={{
                  margin: "4px 6px",
                  border: "1px solid #DDDDDD",
                  borderRadius: "4px",
                  padding: 6,
                }}>
                <CustomDatePicker
                  // date={testEndDate}
                  date={firstEndDate}
                  label={"To:"}
                  PopperProps={{ disablePortal: true, size: null }}
                  icon={false}
                  timeInput={false}
                  onSelect={handleEndDate}
                  disabled={false}
                  deleteIcon={false}
                  placeholder={"Select Date"}
                  containerProps={{ style: { alignItems: "center" } }}
                  btnProps={{
                    style: {
                      background: "transparent",
                      border: "none",
                      padding: 0,
                      textAlign: "left",
                    },
                  }}
                  labelProps={{
                    style: {
                      width: "auto",
                      marginTop: 0,
                      marginRight: 5,
                      fontSize: "14px",
                    },
                  }}
                  closeOnDateSelect={true}
                />
              </div>
            </>
          ) : (
            <></>
          )}
        </DropdownMenu>
      )}
      <div className={classes.mainContainer}>
        <div className={classes.innerSection}>
          <div className={classes.analyticsDatePicker}>
            <div className={classes.dateDropdown}>
              {/* <label>Date Range</label> */}
              <CustomButton
                onClick={e => {
                  setOpenDropDown(prev => !prev);
                }}
                buttonRef={node => {
                  anchorEl.current = node;
                }}
                variant="contained"
                style={{
                  height: 31,
                  fontWeight: 400,
                  fontFamily: theme.typography.fontFamilyLato,
                  fontSize: "13px",
                  border: "1px solid rgba(234,234,234,1)",
                  padding: "5px 10px",
                  color: "black",
                  background: "white",
                  boxShadow: " none",
                  borderRadius: "4px",
                  textTransform: "capitalize",
                  marginRight: "15px",
                }}>
                {filterOption} <ArrowDropDown style={{ color: "#BFBFBF" }} />
              </CustomButton>
            </div>
            <CustomMultiSelectDropdown
              label="Workspace"
              hideLabel={true}
              options={() => workspacesDropdownData}
              option={wSpaceData}
              onSelect={handleSelectedData}
              scrollHeight={400}
              size={"large"}
              buttonProps={{
                variant: "contained",
                btnType: "white",
                labelAlign: "left",
                style: {
                  minWidth: "100%",
                  padding: "3px 4px 3px 8px",
                  height: 31,
                  marginTop: 5,
                  border: "1px solid rgba(234, 234, 234, 1)",
                },
              }}
            />
            <CustomButton
              variant="contained"
              btnType="blue"
              style={{
                marginLeft: "15px",
                padding: "5px 10px",
              }}
              onClick={handleUpdateFilter}>
              Apply Filter
            </CustomButton>
            <CustomButton
              variant="contained"
              btnType="danger"
              style={{
                marginLeft: "15px",
                padding: "5px 10px",
              }}
              onClick={handleClear}>
              Clear
            </CustomButton>

            <div className={classes.exportButtonCnt}>
              <CustomIconButton
                onClick={(event) => {handleOpenExport(event)}}
                buttonRef={node => {
                  anchorRef.current = node;
                }}
                btnType="filledWhite"
                style={{ padding: "5px 7px", height: 31 }}
                >
                <SvgIcon
                    viewBox="0 0 16 14"
                    htmlColor={theme.palette.secondary.medDark}
                    className={classes.downloadIcon}>
                    <DownloadIcon />
                </SvgIcon>
                <span className={classes.labelExport}>
                  Export
                </span>
              </CustomIconButton>
            </div>

            <SelectionMenu
              open={openExportDropDown}
              closeAction={handleOpenExport}
              placement={'bottom-end'}
              anchorRef={anchorRef.current}
              list={
                <List>   
                    <ListItem 
                      button
                      disableRipple={true}
                      classes={{selected: classes.exportMenuItemSelected}}
                      onClick={exportDashboardAsPdf}>
                      <SvgIcon style={{ fontSize: "18px" }} viewBox="0 0 80 98.5">
                        <PdfProjectIcon htmlColor={theme.palette.secondary.medDark} />
                      </SvgIcon>
                      <ListItemText
                        primary={'Export As PDF'}
                        classes={{
                          primary: classes.exportMenuText,
                        }}
                      />
                    </ListItem>
                    <ListItem 
                      button
                      disableRipple={true}
                      classes={{ selected: classes.exportMenuItemSelected }}
                      onClick={exportDashboardAsExcel}>
                      <SvgIcon style={{ fontSize: "18px" }} viewBox="0 0 226.8 226.8">
                        <ExcelProjectIcon htmlColor={theme.palette.secondary.medDark} />
                      </SvgIcon>
                      <ListItemText
                        primary={'Export As Excel'}
                        classes={{
                          primary: classes.exportMenuText,
                        }}
                      />
                    </ListItem>
                    <ListItem 
                      button
                      disableRipple={true}
                      classes={{ selected: classes.exportMenuItemSelected }}
                      onClick={exportDashboardAsImage}>
                      <SvgIcon style={{ fontSize: "18px" }} viewBox="0 0 60 60">
                        <ImageIcon/>
                      </SvgIcon>
                      <ListItemText
                        primary={'Export As Image'}
                        classes={{
                          primary: classes.exportMenuText,
                        }}
                      />
                    </ListItem>
                </List>
              }
            />

            {/* <CustomTooltip
              helptext={
                disableRefreshBtn ? `Refresh will enables in ${refreshInterval} sec` : 'Refresh Grid'
              }
              placement="top"
              style={{ color: theme.palette.common.white }}>
            </CustomTooltip> */}
            <CustomButton
              // onClick={event => disableRefreshBtn ? () => { } : handleRefreshGrid()}
              // query={refreshBtnQuery}
              onClick={refreshDashboard}
              style={{
                padding: "8px 8px",
                borderRadius: "4px",
                display: "flex",
                justifyContent: "space-between",
                minWidth: "auto",
                whiteSpace: "nowrap",
                marginLeft: 15,
                height: 31,
                // background: disableRefreshBtn ? theme.palette.border.lightBorder : 'transparent'
              }}
              btnType={"white"}
              variant="contained">
              <SvgIcon
                viewBox="0 0 12 12.015"
                className={classes.iconRefresh}>
                <IconRefresh />
              </SvgIcon>
            </CustomButton>

          </div>
        </div>
        <div id="dashboard-container"></div>
      </div>
    </>
  );
}

const mapStateToProps = state => {
  return {
    profileState: state.profile.data,
  };
};

export default compose(
  withStyles(AnalyticsStyles, { withTheme: true }),
  connect(mapStateToProps)
)(AnalyticsTwo);
