const AnalyticsStyle = theme => ({
  mainContainer: {
    overflow: " hidden",
    height: "calc(100vh - 60px)",
  },
  innerSection: {
    background: "#fff",
    padding: "10px 10px",
  },
  analyticsDatePicker: {
    display: "flex",
    alignItems: "end",
    justifyContent: 'end',
  },
  filterBtn: {
    border: "1px solid rgba(234, 234, 234, 1)",
    borderRadius: 50,
    fontFamily: "lato",
    fontWeight: 400,
    textTransform: "none",
    background: "rgba(241, 241, 241, 1)",
    padding: " 3px 12px",
  },
  progressSlider: {
    padding: "15px 8px",
  },
  priorityIcon: {
    paddingRight: 3,
  },
  horizontalSlider: {
    width: "100%",
    maxWidth: "500px",
    height: "50px",
    border: "1px solid grey",
  },
  exampleThumb: {
    fontSize: "0.9em !important",
    textAlign: "center",
    backgroundColor: "black",
    color: "white",
    cursor: "pointer",
    border: "5px solid gray",
    boxSizing: "border-box",
  },
  exampleTrack: {
    top: "20px",
    height: 10,
    background: "rgb(221, 221, 221)",
  },
  exampleTrack1: {
    background: "rgb(255, 0, 0)",
  },
  exampleTrack: {
    background: " rgb(0, 255, 0)",
  },
  dateDropdown:{
    display: "flex",
    alignItems: "flex-start",
    flexDirection: "column",
  "& label":{
    color: "#7E7E7E",
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: theme.typography.fontWeightRegular,
    marginBottom: 7,
  }
  },
  iconRefresh: {
    fontSize: "12px !important",
    margin: "0px 5px",
  },
  labelExport: {
    paddingRight: 4,
    fontSize: "12px !important",
    color: "#7e7e7e",
    fontWeight: 500,
  },
  exportMenuItemSelected: {
    background: `transparent !important`,
    "&:before": {
      content: "''",
      position: "absolute",
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      width: 5,
      background: theme.palette.secondary.main,
    },
    "&:hover": {
      background: `transparent !important`,
    },
  },
  exportMenuText: {
    fontSize: "12px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightRegular,
  },
  exportButtonCnt: {
    margin: "0 0 0 15px"
  },
  downloadIcon: {
    fontSize: '14px',
    marginRight: '6px',
},
});

export default AnalyticsStyle;
