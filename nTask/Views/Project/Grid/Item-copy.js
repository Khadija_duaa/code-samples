import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import helper from "../../../helper";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import itemStyles from "./styles";
import Typography from "@material-ui/core/Typography";
import MenuList from "../../../components/Menu/TaskMenus/MemberListMenu";
import DefaultButton from "../../../components/Buttons/DefaultButton";
import LinearProgress from "@material-ui/core/LinearProgress";
import SvgIcon from "@material-ui/core/SvgIcon";
import TaskIcon from "../../../components/Icons/TaskIcon";
import MeetingsIcon from "../../../components/Icons/MeetingIcon";
import IssuesIcon from "../../../components/Icons/IssueIcon";
import RiskIcon from "../../../components/Icons/RiskIcon";
import ProjectActionDropdown from "../List/ProjectActionDropDown";
import cloneDeep from "lodash/cloneDeep";
import { getProjectAssosiatedData } from "../../../helper/getProjectAssosiatedData";
import { sortListData, getSortOrder } from "../../../helper/sortListData";
import CustomTooltip from "../../../components/Tooltip/Tooltip";
import CustomIconButton from "../../../components/Buttons/CustomIconButton";
import IconGantt from "../../../components/Icons/IconGantt";

import {
  UpdateTask,
  FetchTasksInfo,
  CopyTask,
  DeleteTask,
  ArchiveTask,
  UnArchiveTask,
} from "../../../redux/actions/tasks";
import { UpdateCalenderTask, CopyCalenderTask } from "../../../redux/actions/calenderTasks";

import { FetchWorkspaceInfo } from "../../../redux/actions/workspace";
import { FormattedMessage } from "react-intl";
import {
  SaveProject,
  DeleteProject,
  CopyProject,
  EditProject,
  ArchiveProject,
  UnarchiveProject,
  UpdateProject,
  projectDetails
} from "../../../redux/actions/projects";
import flattenDeep from "lodash/flattenDeep";
import { UpdateTaskColorFromGridItem } from "../../../redux/actions/workspace";
import StarredCheckBox from "../../../components/Form/Starred";

class ProjectGridItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      starChecked: false,
      taskProgress: 30,
      selectedColor: "#fff",
      completeDetails: [],
      userId: this.props.userId,
      projectId: "",
      progress: 0,
      isColored: false,
      isProgress: false,
      showProjects: false,
      currentTask: {},
      sortColumn: [],
      sortDirection: "",
    };
    this.handleColorChange = this.handleColorChange.bind(this);
    this.handleCalculateProgress = this.handleCalculateProgress.bind(this);
    this.showProjectsPopUp = this.showProjectsPopUp.bind(this);
    this.closeProjectsPopUp = this.closeProjectsPopUp.bind(this);
  }

  componentDidMount() {
    const { workspaces, loggedInTeam, filteredProjects } = this.props;
    if (loggedInTeam) {
      let test = getSortOrder(workspaces, loggedInTeam, 1);
      let { sortColumn, sortDirection } = test;
      if (sortColumn && sortDirection) {
        let projects = filteredProjects();
        // let rows = sortListData(projects, sortColumn, sortDirection);
        this.setState({ completeDetails: projects, sortColumn, sortDirection });
      } else {
        this.setFormattedProjectRows();
      }
    }
    this.props.resetCount();
    // this.setFormattedProjectRows();
  }
  sortRows = (initialRows, sortColumn, sortDirection) => {
    if (sortDirection === "NONE") {
      this.setFormattedProjectRows();
      this.setState({ sortColumn: null, sortDirection: "" });
    } else {
      let rows = sortListData(initialRows, sortColumn, sortDirection);
      this.setState({ completeDetails: rows, sortColumn, sortDirection });
    }
  };

  componentDidUpdate(prevProps, prevState) {
    const { filteredProjects, tasks, issues, risks, meetings, sortObj, projects } = this.props;
    const dataChanged =
      JSON.stringify(prevProps.filteredProjects()) !== JSON.stringify(filteredProjects()) ||
      JSON.stringify(prevProps.tasks) !== JSON.stringify(tasks) ||
      JSON.stringify(prevProps.issues) !== JSON.stringify(issues) ||
      JSON.stringify(prevProps.risks) !== JSON.stringify(risks) ||
      JSON.stringify(prevProps.meetings) !== JSON.stringify(meetings) ||
      JSON.stringify(prevProps.sortObj) !== JSON.stringify(sortObj);

    if (dataChanged) {
      if (
        sortObj &&
        sortObj.column &&
        (sortObj.column !== prevProps.sortObj.column ||
          sortObj.direction !== prevProps.sortObj.direction)
      ) {
        /** when user click on selected sort button for changing the sort order, this condtion will execute */
        this.sortRows(
          filteredProjects(),
          sortObj.column,
          sortObj.direction
        ); /** function call for reseting the state project list into selected sort manner */
      } else if (
        sortObj &&
        sortObj.cancelSort == "NONE" &&
        sortObj.cancelSort !== prevProps.sortObj.cancelSort
      ) {
        /** when user click on cancel sort this condtion will execute */
        this.setFormattedProjectRows();
      }
    }
    if (JSON.stringify(prevProps.projects) !== JSON.stringify(projects)) {
      this.setFormattedProjectRows();
    }
  }

  setFormattedProjectRows = () => {
    let members = this.props.allMembers;
    const { filteredProjects, tasks, issues, risks, meetings, projects } = this.props;

    let completeDetails =
      cloneDeep(projects).map(x => {
        // Inserting count values in data
        let associatedData = getProjectAssosiatedData(x.projectId, tasks, issues, risks, meetings);
        x.linkedTasksCount = associatedData.tasks.length;
        x.linkedIssuesCount = associatedData.issues.length;
        x.linkedRisksCount = associatedData.risks.length;
        x.linkedMeetingsCount = associatedData.meetings.length;

        x.projectManagers = members.filter(m => {
          if (x.projectManager && x.projectManager.length >= 0)
            return x.projectManager.indexOf(m.userId) >= 0;
        });
        return x;
      }) || [];

    completeDetails = completeDetails
      .map(x => {
        let position = helper.RETURN_ITEMORDER(this.props.itemOrder.itemOrder, x.id);
        x.taskOrder = position || 0;
        return x;
      })
      .sort((a, b) => a.taskOrder - b.taskOrder);

    this.props.returnCount(completeDetails.length, this.props.showRecords);
    if (this.props.isChanged) {
      this.props.handleChangeState();
    }

    this.setState({
      members,
      completeDetails,
    });
  };

  handleCalculateProgress(data, isProgressUpdated) {
    if (isProgressUpdated) {
      this.setState({ progress: 0, projectId: "", isProgress: false });
    } else {
      let result = helper.RETURN_PROGRESS(data.checkList, data.status);
      this.setState({
        progress: result,
        projectId: data.projectId,
        isProgress: true,
      });
    }
  }
  handleColorChange = (data, color) => {
    let self = this;
    this.setState(
      { selectedColor: color.hex, projectId: data.projectId, isColored: true },
      function () {
        let obj = Object.assign({}, data);
        obj.colorCode = color.hex;
        delete obj.CalenderDetails;
        self.props.UpdateTaskColorFromGridItem(obj, data => {
          self.props.UpdateCalenderTask(data, () => {
            self.setState({
              selectedColor: "#fff",
              projectId: "",
              isColored: false,
            });
          });
        });
      }
    );
  };
  showProjectsPopUp(data) {
    const {
      projectPer,
      projectDetailsDialog: { docked, fullView },
    } = this.props;

    // if (projectPer.accessGantt.cando) {
    //   this.props.projectClick(data);
    // }

    this.props.projectDetails({
      projectDetails: data,
      fullView: fullView ? true : false,
      docked: docked ? true : false,
      dialog: true
    });

    // this.setState({ showProjects: true, currentTask: data });
  }
  closeProjectsPopUp() {
    this.setState({ showProjects: false });
  }
  isUpdated = data => {
    // this.props.UpdateProject(data, (err, data) => {
    //
    // });
  };
  handleStaredChange = (event, project) => {
    let isMark = event.target.checked;
    event.stopPropagation();
    this.setState({ [project.projectId]: isMark }, () => {
      let obj = {
        Id: project.id,
        MarkStar: isMark,
      };
      this.props.UpdateProject(obj);
    });
  };

  render() {
    const { classes, theme, sortObj, projectPer } = this.props;
    const { starChecked, selectedColor, projectId, isProgress, isColored, } = this.state;
    const profileUserId = this.props.userId;
    let selectedColorValue = "#fff",
      progress = 0;
    // const gridData = this.state.completeDetails.slice(0, this.props.showRecords).map((x, i) => { /** for on demand load when scroll down */
    const gridData = this.state.completeDetails.map((x, i) => {
      let meetingsCount = 0,
        riskCount = 0,
        issueCount = 0,
        list = [];
      if (projectId === x.projectId) {
        if (isColored) selectedColorValue = selectedColor;
        else selectedColorValue = x.colorCode;
        if (isProgress) progress = this.state.progress;
        else progress = x.progress;
      } else {
        selectedColorValue = x.colorCode;
        progress = x.projectProgress;
      }
      let tasksData = this.props.tasks
        ? this.props.tasks.filter(t => t.projectId === (x && x.projectId ? x.projectId : ""))
        : [];
      tasksData.map(t => {
        meetingsCount += this.props.meetings
          ? this.props.meetings.filter(meeting => meeting.taskId === t.taskId).length
          : 0;
      });

      tasksData.map(t => {
        riskCount += this.props.risks
          ? this.props.risks.filter(risk => risk.linkedTasks.indexOf(t.taskId) >= 0).length
          : 0;
      });
      tasksData.map(t => {
        issueCount += this.props.issues
          ? this.props.issues.filter(issue => issue.linkedTasks.indexOf(t.taskId) >= 0).length
          : 0;
      });
      let progressData = this.props.tasks
        ? this.props.tasks
          .filter(t => t.projectId === (x && x.projectId ? x.projectId : ""))
          .map(p => p.progress)
        : [],
        percentage = 0;
      if (progressData.length)
        percentage = progressData.reduce((sum, x) => sum + x) / progressData.length;

      tasksData.map(t => {
        list.push(t.assigneeList);
        list.push(
          this.props.meetings
            ? this.props.meetings
              .filter(meeting => meeting.taskId === t.taskId)
              .map(x => x.attendees)
              .map(x => x.attendeeId)
              .filter(Boolean)
            : []
        );
        list.push(
          this.props.risks
            ? this.props.risks
              .filter(risk => risk.linkedTasks.indexOf(x.taskId) >= 0)
              .map(x => x.riskOwner)
              .filter(Boolean)
            : []
        );
        list.push(
          this.props.issues
            ? this.props.issues
              .filter(issue => issue.linkedTasks.indexOf(x.taskId) >= 0)
              .map(x => x.issueAssingnees)
              .filter(Boolean)
            : []
        );
      });
      let assignees = [...new Set(flattenDeep(list))],
        assigneeList = [];
      let members = this.props.allMembers;
      assigneeList = members.filter(m => {
        if (assignees.length >= 0) return assignees.indexOf(m.userId) >= 0;
      });

      let effortsData = this.props.tasks
        ? this.props.tasks
          .filter(t => t.projectId === (x && x.projectId ? x.projectId : ""))
          .map(p => p.totalEffort)
        : [];
      effortsData = effortsData ? helper.SUMDURATIONS(effortsData).split(":") : "0:0";
      let gantAccess = x.projectPermission ? x.projectPermission.permission.project.accessGantt.cando : projectPer.accessGantt.cando
      return (
        <Grid item xs={12} sm={6} md={6} lg={4} xl={3} key={x.projectId}>
          <div
            style={{
              borderLeft: `5px solid ${selectedColorValue !== "" ? selectedColorValue : "#fff"}`,
            }}
            className={classes.taskItemCnt}
            onClick={this.showProjectsPopUp.bind(this, x)}>
            <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="center"
              classes={{ container: classes.gridItemHeadingCnt }}>
              <Typography classes={{ h6: classes.taskGridTaskList }} variant="h6">
                {x.uniqueId}
              </Typography>
              <Typography classes={{ h5: classes.taskGridTaskList }} variant="h5">
                {x.projectName || "Un Named"}
              </Typography>

              <Grid item>
                <Grid container direction="row" justify="space-between" alignItems="flex-end">
                  {gantAccess && (
                    <CustomTooltip
                      helptext={<FormattedMessage id="project.tooltip.project-gantt" defaultMessage="Project Gantt" />}
                      iconType="help"
                      placement="top"
                      style={{ color: theme.palette.common.white }}>
                      <CustomIconButton
                        onClick={e => {
                          e.stopPropagation();
                          this.props.projectClick(x);
                        }}
                        btnType="transparent"
                        variant="contained"
                        style={{
                          width: 26,
                          height: 26,
                          borderRadius: 4,
                          marginRight: 4,
                          // border: "1px solid #c2c2c2"
                        }}
                        disabled={false}>
                        <SvgIcon
                          viewBox="0 0 14 11"
                          htmlColor={theme.palette.secondary.medDark}
                          className={classes.IconGantt}>
                          <IconGantt />
                        </SvgIcon>
                      </CustomIconButton>
                    </CustomTooltip>
                  )}
                  <StarredCheckBox
                    checked={x.isStared}
                    onChange={e => this.handleStaredChange(e, x)}
                  />
                  <ProjectActionDropdown
                    selectedColor={selectedColor}
                    FetchWorkSpaceData={this.props.FetchWorkspaceInfo}
                    project={x}
                    EditProject={this.props.EditProject}
                    CopyProject={this.props.CopyProject}
                    DeleteProject={this.props.DeleteProject}
                    ArchiveProject={this.props.ArchiveProject}
                    UnarchiveProject={this.props.UnarchiveProject}
                    SaveProject={this.props.SaveProject}
                    CopyCalenderTask={this.props.CopyCalenderTask}
                    UpdateCalenderTask={this.props.UpdateCalenderTask}
                    openProjectSetting={this.props.openProjectSetting}
                    showSnackbar={this.props.handleExportType}
                    isUpdated={this.isUpdated}
                  />
                </Grid>
              </Grid>
            </Grid>

            <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="center"
              classes={{ container: classes.gridItemDropdownCnt }}>
              {assigneeList.length > 0 ? (
                <MenuList
                  id={x ? x.id : ""}
                  assigneeList={assigneeList}
                  MenuData={x}
                  placementType="bottom-end"
                  viewType="project"
                  checkAssignee={true}
                  listType="assignee"
                />
              ) : (
                <div style={{ height: 40 }} />
              )}
            </Grid>
            <Grid item>
              <Grid
                container
                direction="row"
                justify="space-between"
                alignItems="center"
                classes={{ container: classes.progressCountCnt }}>
                <Typography variant="body1">{`${Math.round(percentage)}%`}</Typography>
                <Typography variant="body1">
                  {/* {helper.RETURN_REMAINING_DAYS_STATUS(x.dueDate, progress)} */
                    effortsData[0] + "h : " + effortsData[1] + "m"}
                </Typography>
              </Grid>
              <LinearProgress
                variant="determinate"
                value={Math.round(percentage)}
                classes={{ root: classes.progressBar }}
              />
              <Grid
                container
                direction="row"
                justify="space-between"
                alignItems="center"
                classes={{ container: classes.itemBottomBtns }}>
                <DefaultButton
                  buttonType="smallIconBtn"
                  onClick={this.showProjectsPopUp.bind(this, x)}>
                  <SvgIcon viewBox="0 0 18 15.188" className={classes.smallBtnIcon}>
                    <TaskIcon />
                  </SvgIcon>
                  <Typography variant="body2" align="center">
                    {tasksData.length}
                  </Typography>
                </DefaultButton>

                <DefaultButton
                  buttonType="smallIconBtn"
                  onClick={this.showProjectsPopUp.bind(this, x)}>
                  <SvgIcon viewBox="0 0 16 17.375" className={classes.smallBtnIcon}>
                    <IssuesIcon />
                  </SvgIcon>
                  <Typography variant="body2" align="center">
                    {issueCount}
                  </Typography>
                </DefaultButton>

                <DefaultButton
                  buttonType="smallIconBtn"
                  onClick={this.showProjectsPopUp.bind(this, x)}>
                  <SvgIcon viewBox="0 0 18 15.75" className={classes.smallBtnIcon}>
                    <RiskIcon />
                  </SvgIcon>
                  <Typography variant="body2" align="center">
                    {riskCount}
                  </Typography>
                </DefaultButton>

                <DefaultButton
                  buttonType="smallIconBtn"
                  onClick={this.showProjectsPopUp.bind(this, x)}>
                  <SvgIcon viewBox="0 0 17.031 17" className={classes.smallBtnIcon}>
                    <MeetingsIcon />
                  </SvgIcon>
                  <Typography variant="body2" align="center">
                    {meetingsCount}
                  </Typography>
                </DefaultButton>
              </Grid>
            </Grid>
          </div>
        </Grid>
      );
    });
    return (
      <Fragment>
        <Grid container spacing={2}>
          {gridData}
        </Grid>
      </Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    userId: state.profile.data.userId || "",
    allMembers: state.profile.data.member.allMembers || [],
    itemOrder: cloneDeep(state.itemOrder.data) || [],

    // This data is for calculating count of respective object linked with Project
    tasks: state.tasks.data || [],
    issues: state.issues.data || [],
    risks: state.risks.data || [],
    meetings: state.meetings.data || [],
    loggedInTeam: state.profile.data.loggedInTeam,
    workspaces: state.profile.data.workspace,
    projectPer: state.workspacePermissions.data.project,
    projectDetailsDialog: state.projectDetailsDialog,
  };
};

export default compose(
  withRouter,
  withStyles(itemStyles, { withTheme: true }),
  connect(mapStateToProps, {
    UpdateTask,
    FetchTasksInfo,
    UpdateTaskColorFromGridItem,
    UpdateCalenderTask,
    CopyTask,
    CopyCalenderTask,
    DeleteTask,
    ArchiveTask,
    UnArchiveTask,
    projectDetails,
    SaveProject,
    DeleteProject,
    CopyProject,
    EditProject,
    ArchiveProject,
    UnarchiveProject,
    FetchWorkspaceInfo,
    UpdateProject,
  })
)(ProjectGridItem);
