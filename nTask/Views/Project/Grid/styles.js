const itemStyles = theme => ({
  gridContainer: {
    alignContent: 'flex-start',
    paddingTop: '8px',
  },
  AssigneeAvatar: {
    border: `2px solid ${theme.palette.common.white}`
  },
  addAssigneeBtn: {
    fontSize: "20px !important"
  },
  taskGridTaskList: {
    width: 260,
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
  },
  TotalAssignee: {
    background: theme.palette.secondary.lightBlue,
    color: theme.palette.secondary.main
  },
  taskGridCnt: {
    padding: "0 30px 0 60px",
    flex: 1,
    overflowY: "auto"
  },
  taskItemCnt: {
    background: theme.palette.common.white,
    padding: "10px 10px 10px 15px",
    borderRadius: "5px",
    boxShadow: "0 2px 6px 0px rgb(0 0 0 / 20%)"
  },
  statusIcon: {
    fontSize: "11px !important"
  },
  statusItemText: {
    fontSize: "12px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightRegular
  },
  statusMenuItemCnt: {
    padding: "2px 0 2px 16px",
    fontSize: "12px !important",
    height: 20
  },
  statusMenuItemSelected: {
    background: `transparent !important`,
    "&:hover": {
      background: `transparent !important`
    }
  },
  progressCountCnt: {
    marginBottom: 5
  },
  progressBar: {
    borderRadius: 20,
    marginBottom: 15
  },
  smallBtnIcon: {
    fontSize: "18px !important",
    color: theme.palette.secondary.light,
    marginRight: 5
  },
  smallIconBtnCount: {
    fontSize: "16px !important",
    marginLeft: 3,
    lineHeight: "normal"
  },
  itemBottomBtns: {
    marginBottom: 10,
  },
  gridItemHeadingCnt: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 10,
  },
  taskGridActions: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  gridItemDropdownCnt: {
    marginBottom: 10
  },
  IconGantt: {
    fontSize: "18px !important"
  }
});

export default itemStyles;
