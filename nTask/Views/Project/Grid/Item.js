import React, { Component, Fragment, useEffect, useState } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import helper from "../../../helper";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import { grid } from "../../../components/CustomTable2/gridInstance";
import itemStyles from "./styles";
import Typography from "@material-ui/core/Typography";
import MenuList from "../../../components/Menu/TaskMenus/MemberListMenu";
import DefaultButton from "../../../components/Buttons/DefaultButton";
import LinearProgress from "@material-ui/core/LinearProgress";
import SvgIcon from "@material-ui/core/SvgIcon";
import TaskIcon from "../../../components/Icons/TaskIcon";
import MeetingsIcon from "../../../components/Icons/MeetingIcon";
import IssuesIcon from "../../../components/Icons/IssueIcon";
import RiskIcon from "../../../components/Icons/RiskIcon";
// import ProjectActionDropdown from "../List/ProjectActionDropDown";
import ProjectActionDropdown from "../ProjectActionDropdown/ProjectActionDropdown";
import cloneDeep from "lodash/cloneDeep";
import { getProjectAssosiatedData } from "../../../helper/getProjectAssosiatedData";
import { sortListData, getSortOrder } from "../../../helper/sortListData";
import CustomTooltip from "../../../components/Tooltip/Tooltip";
import CustomIconButton from "../../../components/Buttons/CustomIconButton";
import IconGantt from "../../../components/Icons/IconGantt";
import { doesFilterPass } from "../List/ProjectFilter/projectFilter.utils";


import { FormattedMessage, injectIntl } from "react-intl";
import {
  projectDetails,
  updateProjectData,
} from "../../../redux/actions/projects";
import ProjectPermissionSelector from "../../../redux/selectors/projectPermissionSelector";

import flattenDeep from "lodash/flattenDeep";
import Stared from "../../../components/Starred/Starred";

// class ProjectGridItem extends Component {
const ProjectGridItem = (props) => {
  const {
    classes,
    theme,
    resetCount,
    isArchivedSelected,
    projectClick,
  } = props;
  const dispatch = useDispatch();

  const state = useSelector(state => {
    return {
      userId: state.profile.data.userId || "",
      allMembers: state.profile.data.member.allMembers || [],
      itemOrder: cloneDeep(state.itemOrder.data) || [],
      tasks: state.tasks.data || [],
      issues: state.issues.data || [],
      risks: state.risks.data || [],
      meetings: state.meetings.data || [],
      projects: state.projects.data || [],
      quickFilters: state.projects.quickFilters,
      loggedInTeam: state.profile.data.loggedInTeam,
      workspaces: state.profile.data.workspace,
      projectPer: state.workspacePermissions.data.project,

      permissionObject: ProjectPermissionSelector(state),
      workspaceProjectPer: state.workspacePermissions.data.project,

      projectDetailsDialog: state.projectDetailsDialog,
    };
  });
  const {
    userId,
    allMembers,
    itemOrder,
    tasks,
    issues,
    risks,
    meetings,
    projects,
    quickFilters,
    loggedInTeam,
    workspaces,
    projectPer,
    permissionObject,
    workspaceProjectPer,
    projectDetailsDialog,
  } = state;
  const [starChecked, setStarChecked] = useState(false);
  const [taskProgress, setTaskProgress] = useState(30);
  const [selectedColor, setSelectedColor] = useState('#fff');
  const [completeDetails, setCompleteDetails] = useState([]);
  const [projectId, setProjectId] = useState('');
  const [isColored, setIsColored] = useState(false);
  const [isProgress, setIsProgress] = useState(false);
  const [showProjects, setShowProjects] = useState(false);
  const [currentTask, setCurrentTask] = useState({});


  useEffect(() => {
    resetCount();
    // let userId = profileState.data.userId;
    let showProjects = projects.filter(m => doesFilterPass({ data: m }));
    if (quickFilters.Archived) {
      showProjects = showProjects.filter(m => m.isDeleted)
    }
    setCompleteDetails(showProjects);
  }, [projects, quickFilters]);


  const updateProject = (project, obj) => {
    updateProjectData(
      { project, obj },
      dispatch,
      //Success
      project => {
        if (grid.grid) {
          const rowNode = grid.grid && grid.grid.getRowNode(project.id);
          rowNode.setData(project);
        }
      },
      err => {
        if (err && err.data) showSnackBar(err.data.message, "error");
      }
    );
  };

  const handleProjectStared = (stared, obj) => {
    updateProject(obj, { isStared: stared });
  };

  const isUpdated = (data) => {
  };
  const showProjectsPopUp = (data) => {
    //   const {
    //     projectPer,
    //   } = props;
    const { docked, fullView } = projectDetailsDialog;
    projectDetails(
      {
        projectDetails: data,
        fullView: fullView ? true : false,
        docked: docked ? true : false,
        dialog: true
      },
      dispatch
    );
    setShowProjects(true);
    setCurrentTask(data);
    // this.setState({ showProjects: true, currentTask: data });
  }
  return (
    <>
      <Grid className={classes.gridContainer} container spacing={2}>
        {completeDetails.map((x, i) => {
          // let list = [];
          // let assignees = [...new Set(flattenDeep(list))],
          //   assigneeList = [];
          // let members = allMembers;
          // assigneeList = members.filter(m => {
          //   if (assignees.length >= 0) return assignees.indexOf(m.userId) >= 0;
          // });

          const membersObjArr = allMembers && allMembers.filter(m => x.resources && x.resources.includes(m.userId));
          let projectPermission = x?.projectId
            ? permissionObject[x.projectId] ?
              permissionObject[x.projectId].project
              : workspaceProjectPer
            : workspaceProjectPer;

          let timeLoged = helper.CHANGETIMEFORMAT(x.totalTimeLogged)

          let gantAccess = x.projectPermission ? x.projectPermission.permission.project.accessGantt.cando : projectPer.accessGantt.cando
          return (
            <Grid item xs={12} sm={6} md={6} lg={4} xl={3} key={x.projectId}>
              <div
                style={{
                  borderLeft: `5px solid ${x.colorCode !== "" ? x.colorCode : "#fff"}`,
                }}
                className={classes.taskItemCnt}
                onClick={() => showProjectsPopUp(x)}>
                <div className={classes.gridItemHeadingCnt}>
                  <div>
                    <Typography classes={{ h6: classes.taskGridTaskList }} variant="h6">
                      {x.uniqueId}
                    </Typography>
                    <Typography classes={{ h5: classes.taskGridTaskList }} variant="h5">
                      {x.projectName || "Un Named"}
                    </Typography>
                  </div>
                  <div className={classes.taskGridActions}>
                    {gantAccess && (
                      <CustomTooltip
                        helptext={<FormattedMessage id="project.tooltip.project-gantt" defaultMessage="Project Gantt" />}
                        iconType="help"
                        placement="top"
                        style={{ color: theme.palette.common.white }}>
                        <CustomIconButton
                          onClick={e => {
                            e.stopPropagation();
                            projectClick(x);
                          }}
                          btnType="transparent"
                          variant="contained"
                          style={{
                            width: 26,
                            height: 26,
                            borderRadius: 4,
                            marginRight: 4,
                            // border: "1px solid #c2c2c2"
                          }}
                          disabled={false}>
                          <SvgIcon
                            viewBox="0 0 14 11"
                            htmlColor={theme.palette.secondary.medDark}
                            className={classes.IconGantt}>
                            <IconGantt />
                          </SvgIcon>
                        </CustomIconButton>
                      </CustomTooltip>
                    )}
                    <Stared
                      isStared={x.isStared}
                      handleCallback={isStared => handleProjectStared(isStared, x)}
                      diabled={x.isDeleted}
                    />
                    <ProjectActionDropdown
                      btnProps={{ style: { width: 24 } }}
                      data={x}
                      handleActivityLog={() => { }}
                      handleCloseCallBack={() => { }}
                      projectPermission={projectPermission}
                      isArchived={x.isDeleted}
                    />
                  </div>
                </div>
                <Grid
                  container
                  direction="row"
                  justify="space-between"
                  alignItems="center"
                  classes={{ container: classes.gridItemDropdownCnt }}>
                  {membersObjArr.length > 0 ? (
                    <MenuList
                      id={x ? x.id : ""}
                      assigneeList={membersObjArr}
                      MenuData={x}
                      placementType="bottom-end"
                      viewType="project"
                      checkAssignee={true}
                      listType="assignee"
                    />
                  ) : (
                    <div style={{ height: 40 }} />
                  )}
                </Grid>
                <Grid item>
                  <Grid
                    container
                    direction="row"
                    justify="space-between"
                    alignItems="center"
                    classes={{ container: classes.progressCountCnt }}>
                    <Typography variant="body1">{`${x.progress}%`}</Typography>
                    <Typography variant="body1">
                      {/* {helper.RETURN_REMAINING_DAYS_STATUS(x.dueDate, progress)} */
                        timeLoged}
                    </Typography>
                  </Grid>
                  <LinearProgress
                    variant="determinate"
                    value={x.progress}
                    classes={{ root: classes.progressBar }}
                  />
                  <Grid
                    container
                    direction="row"
                    justify="space-between"
                    alignItems="center"
                    classes={{ container: classes.itemBottomBtns }}>
                    <DefaultButton
                      buttonType="smallIconBtn"
                      onClick={() => showProjectsPopUp(x)}>
                      <SvgIcon viewBox="0 0 18 15.188" className={classes.smallBtnIcon}>
                        <TaskIcon />
                      </SvgIcon>
                      <Typography variant="body2" align="center">
                        {x.tasks}
                      </Typography>
                    </DefaultButton>

                    <DefaultButton
                      buttonType="smallIconBtn"
                      onClick={() => showProjectsPopUp(x)}>
                      <SvgIcon viewBox="0 0 16 17.375" className={classes.smallBtnIcon}>
                        <IssuesIcon />
                      </SvgIcon>
                      <Typography variant="body2" align="center">
                        {x.issues}
                      </Typography>
                    </DefaultButton>

                    <DefaultButton
                      buttonType="smallIconBtn"
                      onClick={() => showProjectsPopUp(x)}>
                      <SvgIcon viewBox="0 0 18 15.75" className={classes.smallBtnIcon}>
                        <RiskIcon />
                      </SvgIcon>
                      <Typography variant="body2" align="center">
                        {x.risks}
                      </Typography>
                    </DefaultButton>

                    <DefaultButton
                      buttonType="smallIconBtn"
                      onClick={() => showProjectsPopUp(x)}>
                      <SvgIcon viewBox="0 0 17.031 17" className={classes.smallBtnIcon}>
                        <MeetingsIcon />
                      </SvgIcon>
                      <Typography variant="body2" align="center">
                        {x.meetings}
                      </Typography>
                    </DefaultButton>
                  </Grid>
                </Grid>
              </div>
            </Grid>
          );
        })}
      </Grid>
    </>
  );
}

export default compose(
  withRouter,
  injectIntl,
  withStyles(itemStyles, { withTheme: true }),

)(ProjectGridItem); 
