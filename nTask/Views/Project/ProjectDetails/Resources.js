import React, { Component, Fragment } from "react";
import newProjectStyles from "./styles";
import SelectSearchDropdown from "../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import ProjectResourcesTable from "../../../components/Table/ProjectResourcesTable";
import Hotkeys from "react-hot-keys";
import NotificationMessage from "../../../components/NotificationMessages/NotificationMessages";

function Resources(props) {
  const {
    classes,
    theme,
    rateType = "",
    resourcesListValues = [],
    rows = [],
    ddData,
    currencySymbol
  } = props;

  return (
    <Fragment>
      <div className={classes.dialogCnt}>
        <div className={classes.resourceCnt}>
          <Hotkeys keyName="enter" onKeyDown={props.onKeyDown}>
            <SelectSearchDropdown
              data={() => ddData}
              label="Add Resource(s)"
              styles={{ margin: 0 }}
              selectChange={props.handleCreateOptionsSelect}
              type="assignees"
              selectedValue={resourcesListValues}
              placeholder={"Add resource(s) to the project"}
              avatar={true}
              isMulti={false}
            />
          </Hotkeys>
        </div>
        <ProjectResourcesTable
          classes={classes}
          theme={theme}
          rows={rows}
          currencySymbol={currencySymbol}
          rateType={rateType}
          handleChangePermission={props.handleChangePermission}
          removeResource={props.removeResource}
          handleInputTable={props.handleInputTable}
        />
        <NotificationMessage
          type="info"
          iconType="info"
          style={{
            //   width: "calc(100% - 176px)",
            padding: "10px 15px 10px 15px",
            backgroundColor: "rgba(243, 243, 243, 1)",
            borderRadius: 4,
            bottom : 40,
            position: 'absolute'
          }}
        >
          All Project Managers (PMs) have edit project/timesheet
          approval/rejection rights.
        </NotificationMessage>
      </div>
    </Fragment>
  );
}

export default Resources;
