const newProjectStyles = (theme) => ({
  TabsRoot: {
    background: theme.palette.background.airy,
    // borderBottom: `1px solid #F6F6F6`,
    boxShadow: `0px 1px 0px ${theme.palette.background.grayLighter}`,
    padding: "0 20px",
    minHeight: 40,
  },
  tabIndicator: {
    background: theme.palette.border.blue,
    borderRadius: "10px 10px 0px 0px",
    height: 3,
  },
  tab: {
    minWidth: "unset",
    minHeight: 40,
  },
  tabLabel: {
    fontSize: "13px !important",
  },
  tabLabelCnt: {
    padding: 0,
    fontSize: "12px !important",
    textTransform: "capitalize",
    "& $h6": {
      fontWeight: theme.typography.fontWeightMedium,
    },
    // fontWeight: theme.typography.fontWeightRegular,
  },
  dialogCnt: {
    padding: 20,
    minHeight: 575,
  },
  tabSelected: {
    color: theme.palette.border.blue,
    "& $h6": {
      color: theme.palette.border.blue,
    },
  },
  toggleBtnGroup: {
    // textAlign: "center",
    borderRadius: 4,
    marginBottom: 26,
    background: theme.palette.common.white,
    border: "none",
    boxShadow: "none",
    "& $toggleButtonSelected": {
      //   color: theme.palette.common.white,
      backgroundColor: theme.palette.border.blue,
      // border: `1px solid ${theme.palette.border.blue}`,
      //   '& $planPriceStyle':{
      //     color: theme.palette.text.azure,
      //   },
      "& $planTypeLbl": {
        color: theme.palette.common.white,
      },
      "&:after": {
        background: theme.palette.common.white,
      },
      "&:hover": {
        background: theme.palette.border.blue,
        // color: theme.palette.common.black
      },
    },
  },
  toggleButton: {
    border: `1px solid #F6F6F6`,
    borderRadius: 4,
    height: 32,
    // width: 180,
    // width: '50%',
    // height: "auto",
    // padding: "0px 20px",
    fontSize: "12px !important",
    textTransform: "capitalize",
    color: theme.palette.common.white,
    fontWeight: theme.typography.fontWeightLight,
    background: "#F6F6F6",
    padding: "8px 12px",
    // height: "100%",
    "&:hover": {
      // background: theme.palette.background.medium
    },
    "&[value = 'annually']": {
      borderRight: 0,
      padding: 10,
    },
  },
  toggleButtonSelected: {},
  toggleButtonMainDiv: {
    position: "relative",
    // paddingTop: 10,
    alignItems: "center",
    display: "flex",
    // flexDirection: 'column',
  },
  planTypeLbl: {
    display: "flex",
    fontSize: "13px !important",
    // fontWeight: 700,
    color: theme.palette.text.light,
    whiteSpace: "nowrap",
  },
  note: {
    marginTop: 5,
    color: "#7E7E7E",
  },
  link: {
    color: theme.palette.secondary.main,
    cursor: "pointer",
    textDecoration: "underline",
  },
  selectFormControl: {
    width: "100%",
    marginTop: 22,
  },
  defaultInputLabel: {
    transform: "translate(2px, -17px) scale(1)",
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular,
  },
  outlinedSelectCnt: {
    "&:hover $outlinedSelect": {
      border: `1px solid #0000003b !important`,
    },
  },
  outlinedSelect: {
    borderRadius: 4,
    background: theme.palette.common.white,
  },
  outlineInputFocus: {
    "& $outlinedSelect": {
      border: `1px solid ${theme.palette.border.blue} !important`,
    },
  },
  singleSelectCmp: {
    borderRadius: 4,
    padding: "12px 28px 12px 12px",
    color: theme.palette.text.secondary,
    fontSize: theme.typography.pxToRem(16),
    "&:hover": {
      border: "none",
    },
    "&:focus": {
      background: "transparent",
    },
  },
  selectIcon: {
    color: theme.palette.secondary.light,
    right: 11,
    color: "rgba(191, 191, 191, 1)",
    fontSize: "31px !important",
    top: "calc(50% - 15px)",
  },
  headingItem: {
    padding: "0 20px",
    outline: "none",
    "&:hover": {
      backgroundColor: "transparent",
      cursor: "unset",
    },
  },
  headingText: {
    // Dropdown Heading Item Text
    fontSize: "12px !important",
    marginTop: 5,
    fontWeight: theme.typography.fontWeightLight,
    textTransform: "capitalize",
    color: theme.palette.secondary.light,
  },
  menuItem: {
    padding: "8px 16px 8px 16px",
    marginBottom: 2,
  },
  selectedValue: {
    //Adds left border to selected list item
    "&:before": {
      content: "''",
      position: "absolute",
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      width: 5,
      background: theme.palette.secondary.main,
    },
  },
  itemSelected: {
    // Selected Item
    background: `${theme.palette.common.white} !important`,
    "&:hover": {
      background: `${theme.palette.background.items}`,
    },
    "&:focus": {
      background: `${theme.palette.background.items} !important`,
    },
  },
  itemText: {
    fontSize: "12px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightLight,
    width: 134,
    overflow: "hidden",
    whiteSpace: "nowrap",
    lineHeight: "normal",
    textOverflow: "ellipsis",
  },
  projectIcon: {
    fontSize: "14px !important",
    // marginRight: 6
  },
  resourceCnt: {
    margin: "20px 0",
  },

  root: {
    width: "100%",
    background: theme.palette.background.default,
    boxShadow: "none",
    marginTop: "30px",
  },
  tableWrapper: {
    overflow: "auto",
    minHeight: 517,
  },
  tableHeader: {
    background: theme.palette.background.light,
  },
  tableHeaderRow: {
    height: 36,
  },
  tableHeaderCell: {
    padding: "9px 8px",
  },
  tableRow: {
    verticalAlign: "middle",
  },
  linkStyle: {
    color: theme.palette.text.green,
    fontWeight: theme.typography.fontWeightMedium,
  },
  currencyDropdownCnt: {
    width: 234,
    marginLeft: 20,
    marginTop: 23,
  },
  importBulkCnt: {
    border: "1px solid lightgray",
    borderStyle: "dashed",
    borderRadius: 8,
    marginBottom: 10,
  },
  heading: {
    fontSize: "13px !important",
    fontWeight: theme.typography.fontWeightMedium,
    marginBottom: 5,
  },
  heading2: {
    right: 0,
    position: "absolute",
    fontSize: "13px !important",
    fontWeight: theme.typography.fontWeightMedium,
    marginBottom: 5,
  },
  milestoneTitle: {
    fontSize: "13px !important",
    fontWeight: theme.typography.fontWeightMedium,
    color: "#333333",
  },
  milestoneDate: {
    color: "#7E7E7E",
  },
  detailsStartIA: {
    width: 12,
    marginLeft: -7,
    marginRight: 5,
    marginBottom: 2,
  },
  detailsEndIA: {
    width: 12,
    marginRight: 9,
    marginLeft: 0,
  },
  PrEpsRoot: {
    backgroundColor: "#F6F6F6",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: 4,
    boxShadow: "none",
    "&$expanded": {
      minHeight: "0 !important",
    },
  },
  PEpdroot: {
    background: "white",
  },
  PEpsExpanded: {},
  PEpsContent: {
    "&$expanded": {
      margin: "12px 0",
    },
  },
  PEpsRoot: {
    minHeight: "0 !important",
    padding: "0 24px 0 15px",
    "&$expanded": {
      minHeight: "0 !important",
    },
  },
  PEpExpanded: {
    margin: "auto",
  },
  arrowDown: {
    marginTop: -1,
  },
  financialList: {
    listStyleType: "none",
    padding: 0,
    width: "100%",
    margin: 0,
    "& li": {
      // padding: "2px 0 ",
      // margin: "0 20px",
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
      // borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    },
    "& li:last-child": {
      border: "none",
    },
  },
  financialListItemTitle: {
    flex: 1,
  },
  taskIcon: {
    fontSize: "17px !important",
    marginRight: 5,
  },
  timeIcon: {
    fontSize: "19px !important",
    marginRight: 5,
  },
  resourcesIcon: {
    marginTop: -7,
    fontSize: "19px !important",
    marginRight: 5,
  },
  costIcon: {
    marginTop: -2,
    fontSize: "19px !important",
    marginRight: 5,
  },
  billingIcon: {
    // marginTop: -2,
    fontSize: "50px !important",
    marginRight: 5,
  },
  actualCostCnt: {
    marginTop: 15,
  },
  listItemText: {
    right: 35,
    fontSize: "13px !important",
    fontWeight: theme.typography.fontWeightMedium,
    marginBottom: 5,
  },
  sendEmailCnt: {
    display: "flex",
    marginTop: 15,
  },
  actualCostTitleCnt: {
    right: 0,
    position: "absolute",
    marginTop: -7,
  },
  heading3: {
    fontSize: "13px !important",
    fontWeight: theme.typography.fontWeightMedium,
    textAlign: "right",
  },
  billingMethodCnt: {
    backgroundColor: "#F6F6F6",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: 4,
    marginBottom: 15,
    width: "fit-content",
  },
  feeCnt: {
    marginLeft: 30,
    padding: "5px 0 0 25px",
    borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
  },
  projectColorIcon: {
    fontSize: "19px !important",
    // marginBottom: 2,
  },
  projectStatusStartIA: {
    marginRight: 0,
  },
  projectColorPicker: {
    position: "absolute",
    zIndex: 1,
    marginTop: 30,
  },
  currencySymbol: {
    fontSize: "11px !important",
    fontWeight: theme.typography.fontWeightRegular,
    color: "#969696",
  },
  addProjectDiv: {
    width: "100%",
  },
  
  createdBy: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    padding: "5px 14px",
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    background: theme.palette.background.items,
    borderRadius: "0 0 4px 4px",
  },
  buttonCnt: {
    position: "absolute",
    bottom: 0,
    width: "100%",
  },
  notchedOutlineCntt: {
    border: "none",
  },
  outlinedProjectInput: {
    padding: "0px",
    background: theme.palette.background.default,
    borderRadius: 4,
    fontSize: "18px !important",
    fontWeight: theme.typography.fontWeightMedium,
  },
  projectDropdownsLabel: {
    transform: "translate(6px, -7px) scale(1)",
    display: "block",
    color: '#646464',
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
  },
  projectEditor: {
    maxHeight: "430px !important",
    height: "327px",
    padding: 20,
    // fontSize: "14px !important",
    // fontFamily: theme.typography.fontFamilyLato,
    // fontWeight: 400,
    overflowY: "auto",
    borderRadius: 4,
    color: '#646464',
  },
  descriptionCntt: {
    maxHeight: "430px !important",
    height: "400px",
    padding: 10,
    borderRadius: "4px",
    overflowY: "auto",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    resize: "vertical",
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
    color: '#646464',
    "& *": {
      margin: 0,
    },
  },
  errorText: {
    color: theme.palette.text.danger,
    marginBottom: -7,
  },
  limitExceedCnt: {
    // background: "#ff4a4a3b",
    display: "flex",
    borderRadius: "5px",
    // width: " 193px",
    marginBottom: "5px",
    padding: "2px 5px",
  },
  limitExceedTxt: {
    // color: "#FF4A4A"
  },
  lockIcon: {
    // color: "#FF4A4A",
    fontSize: "16px !important",
    paddingTop: "3px",
  },
  commentsTabCnt: {
      display: "flex",
      flexDirection: "column",
      // height: "calc(100% - 80px)",
      flex: 1,
  },
  detailsDiv: {
    // width: "70%", 
    display: 'flex', 
    flexDirection: 'column',
    background: theme.palette.background.airy,
    // borderLeft: `1px solid ${theme.palette.background.contrast}`,
    width: '45%',
  },
});

export default newProjectStyles;
