import React from "react";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import SvgIcon from "@material-ui/core/SvgIcon";
import TaskIcon from "../../../components/Icons/TaskIcon";
import TimeIcon from "../../../components/Icons/IconTime";
import ResourcesIcon from "../../../components/Icons/ResourcesIcon";
import CostIcon from "../../../components/Icons/CostIcon";
import ApproveIconfrom from "../../../components/Icons/ApprovedTimeIcon";
import BillingIcon from "../../../components/Icons/BillingMethodIcon";
import DefaultCheckbox from "../../../components/Form/Checkbox";
import DefaultTextField from "../../../components/Form/TextField";

function FinancialSummary(props) {
  const {
    theme,
    classes,
    counts = {},
    totalHours = "",
    totalMins = "",
    exceededPerValue = 80,
    billingMethodVal = "",
    selectedFeeMethod = "",
    sendEmailCheck = true,
    estimatedCost = "",
    fee = "",
    completedTasks = "-",
    pendingApprovalTime = "-",
    approvedTimeLogged = "-",
  } = props;
  const [expanded, setExpanded] = React.useState("estimatedCost");
  const [expandedActual, setExpandedActual] = React.useState("actualCost");

  const handleChange = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };
  const handleChangeActual = (panel) => (event, newExpanded) => {
    setExpandedActual(newExpanded ? panel : false);
  };
  const getLabel = () => {
    switch (billingMethodVal) {
      case "Fixed Fee":
        return intl.formatMessage({id:"project.dev.details.billable.billing-method.list.fixed",defaultMessage:"Fixed fee"});
        break;

      case "Fixed Fee per Task":
        if (fee == "sameEveryTask") {
          return intl.formatMessage({id:"project.dev.details.billable.billing-method.list.fixedpertask",defaultMessage:"Fee per task"});
        } else if (fee == "differentPerTask") {
          return intl.formatMessage({id:"project.dev.details.billable.billing-method.list.avgpertask",defaultMessage:"Avg. fee per task"});
        }
        break;

      case "Hourly Rate by Task":
        if (fee == "sameHourlyEveryTask") {
          return intl.formatMessage({id:"project.dev.details.billable.billing-method.list.hourlyrate",defaultMessage:"Hourly rate per task"});
        } else if (fee == "differentHourlyPerTask") {
          return intl.formatMessage({id:"project.dev.details.billable.billing-method.list.avghourly",defaultMessage:"Avg. hourly rate per task"});
        }
        break;

      case "Hourly rate by Resource":
        if (fee == "sameHourlyEveryResource") {
          return intl.formatMessage({id:"project.dev.details.billable.billing-method.list.hourlyperres",defaultMessage:"Hourly rate per resource"});
        } else if (fee == "differentHourlyPerResource") {
          return intl.formatMessage({id:"project.dev.details.billable.billing-method.list.avghourlyperres",defaultMessage:"Avg. hourly rate per resource"});
        }
        break;

      default:
        return "-";
        break;
    }
  };
  return (
    <>
      <div className={classes.dialogCnt}>
        <div className={classes.billingMethodCnt}>
          <div style={{ display: "flex", marginRight: 20 }}>
            <SvgIcon
              viewBox="0 0 45 45"
              htmlColor={theme.palette.secondary.light}
              className={classes.billingIcon}
            >
              <BillingIcon />
            </SvgIcon>
            <div style={{ padding: 5 }}>
              <Typography variant="h6" style={{ color: "#969696" }}>
                Billing Method
              </Typography>
              <Typography
                variant="h6"
                style={{ fontWeight: theme.typography.fontWeightMedium }}
              >
                {billingMethodVal}
              </Typography>
            </div>
            {billingMethodVal !== "Fixed Fee" && (
              <div className={classes.feeCnt}>
                <Typography variant="h6" style={{ color: "#969696" }}>
                  Fee
                </Typography>
                <Typography
                  variant="h6"
                  style={{ fontWeight: theme.typography.fontWeightMedium }}
                >
                  {selectedFeeMethod}
                </Typography>
              </div>
            )}
          </div>
        </div>
        <ExpansionPanel
          square
          expanded={expanded === "estimatedCost"}
          onChange={handleChange("estimatedCost")}
          classes={{ root: classes.PrEpsRoot, expanded: classes.PEpExpanded }}
        >
          <ExpansionPanelSummary
            aria-controls="estimatedCost-content"
            id="estimatedCost-header"
            classes={{
              root: classes.PEpsRoot,
              expanded: classes.PEpsExpanded,
              content: classes.PEpsContent,
            }}
          >
            <ArrowDropDownIcon
              className={classes.arrowDown}
              htmlColor={theme.palette.secondary.medDark}
            />
            <Typography variant="h6" className={classes.heading}>
              Estimated Cost
            </Typography>
            <Typography variant="h6" className={classes.heading2}>
              {props.getEstimatedCost()}
            </Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails classes={{ root: classes.PEpdroot }}>
            <ul className={classes.financialList}>
              <li>
                <SvgIcon
                  viewBox="0 0 20 15"
                  htmlColor={theme.palette.secondary.light}
                  className={classes.taskIcon}
                >
                  <TaskIcon />
                </SvgIcon>
                <Typography
                  variant="h6"
                  className={classes.financialListItemTitle}
                >
                  Total no. of tasks
                </Typography>
                <Typography variant="h6" className={classes.listItemText}>
                  {counts.taskCount}
                </Typography>
              </li>
              <li>
                <SvgIcon
                  viewBox="0 0 20 15"
                  htmlColor={theme.palette.secondary.light}
                  className={classes.timeIcon}
                >
                  <TimeIcon />
                </SvgIcon>
                <Typography
                  variant="h6"
                  className={classes.financialListItemTitle}
                >
                  Estimated time
                </Typography>
                <Typography variant="h6" className={classes.listItemText}>
                  {`${totalHours} hrs ${totalMins} mins`}
                </Typography>
              </li>
              <li>
                <SvgIcon
                  viewBox="1 0 20 15"
                  htmlColor={theme.palette.secondary.light}
                  className={classes.resourcesIcon}
                >
                  <ResourcesIcon />
                </SvgIcon>
                <Typography
                  variant="h6"
                  className={classes.financialListItemTitle}
                >
                  Total number of resources
                </Typography>
                <Typography variant="h6" className={classes.listItemText}>
                  {counts.resourcesCount}
                </Typography>
              </li>
              <li>
                <SvgIcon
                  viewBox="1 0 20 15"
                  htmlColor={theme.palette.secondary.light}
                  className={classes.costIcon}
                >
                  <CostIcon />
                </SvgIcon>
                <Typography
                  variant="h6"
                  className={classes.financialListItemTitle}
                >
                  {getLabel()}
                </Typography>
                <Typography variant="h6" className={classes.listItemText}>
                  {props.getrowLabelValue()}
                </Typography>
              </li>
            </ul>
          </ExpansionPanelDetails>
        </ExpansionPanel>

        <div className={classes.actualCostCnt}>
          <ExpansionPanel
            square
            expanded={expandedActual === "actualCost"}
            onChange={handleChangeActual("actualCost")}
            classes={{ root: classes.PrEpsRoot, expanded: classes.PEpExpanded }}
          >
            <ExpansionPanelSummary
              aria-controls="actualCost-content"
              id="actualCost-header"
              classes={{
                root: classes.PEpsRoot,
                expanded: classes.PEpsExpanded,
                content: classes.PEpsContent,
              }}
            >
              <ArrowDropDownIcon
                className={classes.arrowDown}
                htmlColor={theme.palette.secondary.medDark}
              />
              <Typography variant="h6" className={classes.heading}>
                Actual Cost
              </Typography>
              <div className={classes.actualCostTitleCnt}>
                <Typography variant="h6" className={classes.heading3}>
                  $ 0
                </Typography>
                <Typography variant="h6">
                  <span className={classes.heading3}>100%</span> estimated cost
                  remaining
                </Typography>
              </div>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails classes={{ root: classes.PEpdroot }}>
              <ul className={classes.financialList}>
                <li>
                  <SvgIcon
                    viewBox="0 0 20 15"
                    htmlColor={theme.palette.secondary.light}
                    className={classes.taskIcon}
                  >
                    <TaskIcon />
                  </SvgIcon>
                  <Typography
                    variant="h6"
                    className={classes.financialListItemTitle}
                  >
                    Total no. of completed tasks
                  </Typography>
                  <Typography variant="h6" className={classes.listItemText}>
                    {completedTasks}
                  </Typography>
                </li>
                <li>
                  <SvgIcon
                    viewBox="0 0 20 15"
                    htmlColor={theme.palette.secondary.light}
                    className={classes.timeIcon}
                  >
                    <ApproveIconfrom />
                  </SvgIcon>
                  <Typography
                    variant="h6"
                    className={classes.financialListItemTitle}
                  >
                    Approved time logged
                  </Typography>
                  <Typography variant="h6" className={classes.listItemText}>
                    {approvedTimeLogged}
                  </Typography>
                </li>
                <li>
                  <SvgIcon
                    viewBox="0 0 20 15"
                    htmlColor={theme.palette.secondary.light}
                    className={classes.timeIcon}
                  >
                    <TimeIcon />
                  </SvgIcon>
                  <Typography
                    variant="h6"
                    className={classes.financialListItemTitle}
                  >
                    Pending approval time
                  </Typography>
                  <Typography variant="h6" className={classes.listItemText}>
                    {pendingApprovalTime}
                  </Typography>
                </li>
                <li>
                  <SvgIcon
                    viewBox="1 0 20 15"
                    htmlColor={theme.palette.secondary.light}
                    className={classes.costIcon}
                  >
                    <CostIcon />
                  </SvgIcon>
                  <Typography
                    variant="h6"
                    className={classes.financialListItemTitle}
                  >
                    Total cost
                  </Typography>
                  <Typography variant="h6" className={classes.listItemText}>
                    {props.getTotalCost()}
                  </Typography>
                </li>
              </ul>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </div>

        <div className={classes.sendEmailCnt}>
          <DefaultCheckbox
            checkboxStyles={{ padding: 0, marginRight: 10 }}
            checked={sendEmailCheck}
            onChange={() => {
              props.handleChangeSendEmail(!sendEmailCheck);
            }}
            fontSize={20}
          />
          <Typography variant="h6" className={classes.financialListItemTitle}>
            Send email alerts if project actual cost exceeds{" "}
            <DefaultTextField
              label=""
              helptext=""
              errorState={false}
              errorMessage={""}
              formControlStyles={{ margin: "-8px 5px 0 0", maxWidth: 73 }}
              defaultProps={{
                type: "text",
                id: "exceededPer",
                placeholder: "",
                value: exceededPerValue,
                autoFocus: false,
                inputProps: { maxLength: 3 },
                onChange: props.handleChangePercentage,
              }}
            />
            % of estimated cost.
          </Typography>
        </div>
      </div>
    </>
  );
}
export default FinancialSummary;
