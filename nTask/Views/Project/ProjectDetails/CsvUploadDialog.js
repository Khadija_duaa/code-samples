import React, { Component, Fragment } from "react";
import SvgIcon from "@material-ui/core/SvgIcon";
import CircularProgress from "@material-ui/core/CircularProgress";
import ExcelIcon from "../../../components/Icons/ExcelIcon";
import dialogStyles from "../../../assets/jss/components/dialog";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import DefaultButton from "../../../components/Buttons/DefaultButton";
import Dropzone from "react-dropzone";
import Typography from "@material-ui/core/Typography";
import { Circle } from "rc-progress";
import helper from "../../../helper";
import { ImportFile } from "../../../redux/actions/ImportExport";
import apiInstance, { CancelToken } from "../../../redux/instance";
import NotificationMessage from "../../../components/NotificationMessages/NotificationMessages";
import getErrorMessages from "../../../utils/constants/errorMessages";

class DragDropCsv extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dnd: true,
      progress: 0,
      cancelRequest: null,
      successfullyImortedCount: 0,
      totalAdded: 0,
      errorMesseage: "",
    };
    this.initialState = this.state;
    this.handleDragOver = this.handleDragOver.bind(this);
  }
  onhandleDrop(files) {
    if (files.length) {
      this.manageImports(0, files);
      this.setState({ dnd: false });
    }
  }
  manageImports = (index, files) => {
    const self = this;
    const file = files[index];
    const { type, closeAction, showMessage, handleRefresh } = self.props;
    const successApiCallback = (response) => {
      if (response.status === 200) {
        const successfullyImortedCount = response.data;
        // closeAction();
        // showMessage(`${successfullyImortedCount} ${type}s have been added to System.`);
        handleRefresh();
        if (index === files.length - 1)
          self.setState({
            progress: 100,
            cancelRequest: null,
            successfullyImortedCount,
            totalAdded: self.state.totalAdded + successfullyImortedCount,
          });
        else {
          // self.manageImports(index + 1, files);
          self.setState({
            totalAdded: self.state.totalAdded + successfullyImortedCount,
          });
        }
        this.setState({ errorMesseage: "" });
      }
    };
    const failureApiCallback = (error) => {
      if (error.status && error.status === 500) {
        if (index === files.length - 1) self.setState(self.initialState);
        this.setState({ errorMesseage: error.data.message });
      } else if (
        (error.status && error.status === 422) ||
        error.status === 406
      ) {
        if (index === files.length - 1) self.setState(self.initialState);
        this.setState({ errorMesseage: error.data.message });
      } else {
        if (index === files.length - 1) self.setState(self.initialState);
        this.setState({ errorMesseage: error.data.message });
      }
    };

    let formdata = new FormData();
    formdata.append("file", file);
    const config = {
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      cancelToken: new CancelToken(function executor(c) {
        self.setState({ cancelRequest: c });
      }),
    };
    if (type === "task") {
      ImportFile(
        formdata,
        "api/UserTask/ImportTasks",
        config,
        successApiCallback,
        failureApiCallback
      );
    }
  };
  handleDragOver(event) {
    event.stopPropagation();
  }

  handleCancelRequest = () => {
    const { cancelRequest } = this.state;
    if (cancelRequest) cancelRequest("User cancelled upload.");
  };

  handleInvalidFileDrop = () => {
    this.props.showMessage(
      getErrorMessages().INVALID_IMPORT_EXTENSION,
      "error"
    );
  };
  render() {
    const { classes, theme, open, type } = this.props;
    const { progress, dnd, totalAdded, errorMesseage } = this.state;
    const dndStyles = {
      position: "relative",
      width: "100%",
      borderWidth: 2,
      borderColor: theme.palette.border.lightBorder,
      borderStyle: "dashed",
      borderRadius: 5,
      padding: 50,
      background: theme.palette.background.paper,
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      marginBottom: 30,
    };
    const dndActiveStyle = {
      borderColor: theme.palette.border.darkBorder,
    };

    const title = type.charAt(0).toUpperCase() + type.slice(1);
    return (
      <div className={classes.centerAlignDialogContent}>
        {errorMesseage ? (
          <NotificationMessage
            type="failure"
            iconType="failure"
            style={{ marginBottom: 20, width: "100%" }}
          >
            {errorMesseage}
          </NotificationMessage>
        ) : null}
        {dnd ? (
          <Dropzone
            onDrop={this.onhandleDrop.bind(this)}
            onDragOver={this.handleDragOver}
            onDropRejected={this.handleInvalidFileDrop}
            accept="application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, text/csv"
            activeStyle={dndActiveStyle}
            style={dndStyles}
            // disableClick
            onClick={(evt) => evt.preventDefault()}
          >
            {({ getRootProps, getInputProps, open }) => {
              return (
                <Fragment>
                  <div
                    className={classes.csvConfirmationIconCnt}
                    {...getRootProps()}
                  >
                    <SvgIcon
                      viewBox="0 0 26 26"
                      className={classes.archieveConfirmationIcon}
                      htmlColor={theme.palette.secondary.light}
                    >
                      <ExcelIcon />
                    </SvgIcon>
                  </div>
                  <Typography variant="h3">Drag & Drop</Typography>
                  <input {...getInputProps()} />
                  <p className={classes.dragnDropMessageText}>
                    your CSV file or{" "}
                    <u
                      style={{
                        color: theme.palette.border.blue,
                        cursor: "pointer",
                      }}
                      onClick={() => open()}
                    >
                      browse
                    </u>{" "}
                    from your computer
                  </p>
                </Fragment>
              );
            }}
          </Dropzone>
        ) : (
          <div className={classes.importBulkProgressCnt}>
            <div style={{ width: 100, position: "relative", marginBottom: 30,  display: "flex", flexDirection: "row", justifyContent: "center" }}>
              {progress < 100 ? (
                <CircularProgress className={classes.progress} />
              ) : (
                <>
                  <Circle
                    percent={progress}
                    strokeWidth="3"
                    trailWidth=""
                    trailColor="#dedede"
                    strokeColor="#0090ff"
                  />
                  <span className={classes.clProgressValue}>{progress}%</span>
                </>
              )}
            </div>

            <Typography variant="h3">
              {progress < 100 ? "Importing..." : "Congratulations!"}
            </Typography>
            <p className={classes.dragnDropMessageText}>
              {progress < 100
                ? "Please wait! It will take a moment."
                : `${totalAdded}/${totalAdded} ${type}s have been imported to nTask successfully.`}
            </p>
          </div>
        )}
      </div>
    );
  }
}

export default withStyles(dialogStyles, { withTheme: true })(DragDropCsv);
