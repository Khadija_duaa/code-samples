import React, { Component, Fragment } from "react";
import newProjectStyles from "./styles";
import DefaultTextField from "../../../components/Form/TextField";
import ProjectSelectIconMenu from "../../../components/Menu/TaskMenus/ProjectSelectIconMenu";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import ToggleButton from "@material-ui/lab/ToggleButton";
import AttachMoneyIcon from "@material-ui/icons/AttachMoney";
import Typography from "@material-ui/core/Typography";
import SelectCurrency from "../ProjectSetting/CurrencyDropdown";
import ProjectDescription from "../ProjectDetail/Details/ProjectDescription";
import InputAdornment from "@material-ui/core/InputAdornment";

function Details(props) {
  const getMessage = (m) => {
    const { classes } = props;
    switch (m) {
      case "differentPerTask":
        return (
          <Typography variant="h6" className={classes.note}>
            Set fee of eact task in{" "}
            <span
              className={classes.link}
              onClick={() => props.changeTab("taskTab")}
            >
              Tasks tab.
            </span>
          </Typography>
        );
        break;
      case "differentHourlyPerTask":
        return (
          <Typography variant="h6" className={classes.note}>
            Set hourly rate of eact task in{" "}
            <span
              className={classes.link}
              onClick={() => props.changeTab("taskTab")}
            >
              Tasks tab.
            </span>
          </Typography>
        );
        break;
      case "differentHourlyPerResource":
        return (
          <Typography variant="h6" className={classes.note}>
            Set hourly rate of eact resource in{" "}
            <span
              className={classes.link}
              onClick={() => props.changeTab("resourceTab")}
            >
              Resources tab.
            </span>
          </Typography>
        );
        break;

      default:
        return null;
        break;
    }
  };

  const {
    btnQuery = "",
    projectDescription,
    subscription,
    billingMethod,
    money,
    fee = "",
    classes,
    theme,
    billingMethodVal,
    selectedFeeMethod,
    selectedCurrency = {},
    currencyOptions = [],
    currencySymbol,
    newProject,
    setProjectDescription,intl
  } = props;

  return (
    <Fragment>
      <div className={classes.dialogCnt}>
        {!newProject && (
          <ToggleButtonGroup
            size="small"
            exclusive
            onChange={props.handlePlanChangePayment}
            value={subscription}
            classes={{ root: classes.toggleBtnGroup }}
          >
            <ToggleButton
              key={2}
              value="billable"
              classes={{
                root: classes.toggleButton,
                selected: classes.toggleButtonSelected,
              }}
            >
              <div className={classes.toggleButtonMainDiv}>
                <span className={classes.planTypeLbl}>Billable</span>
              </div>
            </ToggleButton>
            <ToggleButton
              key={1}
              value="non-billable"
              classes={{
                root: classes.toggleButton,
                selected: classes.toggleButtonSelected,
              }}
            >
              <div className={classes.toggleButtonMainDiv}>
                <span className={classes.planTypeLbl}>Non-Billable</span>
              </div>
            </ToggleButton>
          </ToggleButtonGroup>
        )}

        {/* <DefaultTextField
          label="Project Description"
          error={false}
          defaultProps={{
            value: projectDescription,
            onChange: props.handleDescriptionInput,
            disabled: false,
            rows: 25,
            multiline: true,
            // rowsMax: 10
          }}
        /> */}

        <ProjectDescription
          classes={classes}
          theme={theme}
          taskData={{}}
          permission={true}
          setProjectDescription={setProjectDescription}
          projectDescription={projectDescription}
        />

        {subscription == "billable" && !newProject && (
          <>
            <div style={{ display: "flex", marginTop: 16 }}>
              <ProjectSelectIconMenu
                iconType="status"
                view="billingMethod"
                isSingle={true}
                isSimpleList={true}
                isUpdated={props.selectBillingMethod}
                style={{ marginBottom: 0 }}
                transform="translateY(48px)"
                label="Billing Method"
                selectedValue={billingMethodVal}
              />
              <div className={classes.currencyDropdownCnt}>
                <SelectCurrency
                  onChange={props.handleSelectCurrencyChange}
                  options={currencyOptions}
                  value={selectedCurrency}
                  toolTip={false}
                />
              </div>
            </div>
            <div style={{ display: "flex", marginTop: 16 }}>
              {billingMethod !== "fixed" && (
                <ProjectSelectIconMenu
                  iconType="status"
                  view="fee"
                  isSingle={true}
                  isSimpleList={true}
                  isUpdated={props.handleFeeInput}
                  style={{
                    marginBottom: 0,
                    marginRight: 20,
                    maxWidth: "427px",
                  }}
                  transform="translateY(48px)"
                  label="Fee"
                  selectedValue={selectedFeeMethod}
                  billingMethod={billingMethod}
                />
              )}
              {fee.includes("same") ? (
                <DefaultTextField
                  label={
                    billingMethod == "fixed"
                      ? "Total Project Fee"
                      : billingMethod == "fixedPerTask"
                      ? "Per Task"
                      : "Per Hour"
                  }
                  // fullWidth={true}
                  errorState={false}
                  formControlStyles={
                    billingMethod == "fixed"
                      ? { width: "30%", marginBottom: 0 }
                      : { width: "170px", marginBottom: 0 }
                  }
                  errorMessage=""
                  error={false}
                  defaultProps={{
                    id: "money",
                    type: "text",
                    onChange: props.handleTypeMoneyInput,
                    value: money,
                    autoFocus: false,
                    placeholder: "",
                    inputProps: { maxLength: 85 },
                    startAdornment: (
                      <InputAdornment position="start">
                        <Typography
                          variant="h6"
                          className={classes.currencySymbol}
                        >
                          {currencySymbol}
                        </Typography>
                      </InputAdornment>
                    ),
                  }}
                />
              ) : null}
            </div>
            {getMessage(fee)}
          </>
        )}
      </div>
    </Fragment>
  );
}

export default Details;
