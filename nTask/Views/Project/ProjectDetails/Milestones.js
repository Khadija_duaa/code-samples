import React, { Component, Fragment } from "react";
import DefaultTextField from "../../../components/Form/TextField";
import Typography from "@material-ui/core/Typography";
import CustomButton from "../../../components/Buttons/CustomButton";
import moment from "moment";
import StaticDatePicker from "../../../components/DatePicker/StaticDatePicker";
import CrossIcon from "../../../components/Icons/CrossIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import DefaultButton from "../../../components/Buttons/DefaultButton";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import CheckBoxIcon from "../../../components/Icons/CheckBoxIcon";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import DefaultCheckbox from "../../../components/Form/Checkbox";
import Hotkeys from "react-hot-keys";

function Milestones(props) {
  const {
    classes,
    theme,
    milestoneTitle = "",
    milestoneDate,
    rows = [],
    milestoneTitleErrState = {},
    intl
  } = props;

  const onKeyDown = (keyName, e) => {
    if (keyName === "enter") {
      props.handleAddMileStone();
    }
  };

  return (
    <Fragment>
      <div className={classes.dialogCnt}>
        <div style={{ display: "flex" }}>
          <DefaultTextField
            label="Milestone Title"
            helptext=""
            errorState={milestoneTitleErrState.milestoneTitleErr}
            errorMessage={milestoneTitleErrState.milestoneTitleErrMess}
            formControlStyles={{
              marginBottom: 0,
              maxWidth: 215,
              marginRight: 15,
            }}
            defaultProps={{
              type: "text",
              id: "milestoneTitle",
              placeholder: "Enter milestone Title",
              value: milestoneTitle,
              autoFocus: false,
              inputProps: { maxLength: 80 },
              onChange: props.handleMilestoneTitle,
            }}
          />
          <StaticDatePicker
            label="Milestone Date"
            placeholder="Select Date"
            isInput={true}
            onDateSelect={(date) => {
              props.handleMilestoneDate(date);
            }}
            isCreation={true}
            style={{ marginTop: 2 }}
            date={moment(milestoneDate)}
          />
          <div style={{ alignSelf: "center" }}>
            <Hotkeys keyName="enter" onKeyDown={onKeyDown}>
              <CustomButton
                style={{ marginLeft: 15, minWidth: 128 }}
                variant="contained"
                btnType="success"
                disabled={false}
                onClick={props.handleAddMileStone}
              >
                Add Milestone
              </CustomButton>
            </Hotkeys>
          </div>
        </div>

        <div style={{ minHeight: 200 }}>
          <Table stickyHeader>
            <TableHead className={classes.tableHeader}>
              <TableRow className={classes.tableHeaderRow}>
                <TableCell className={classes.tableHeaderCell}>
                  {" "}
                  <Typography variant="h6" className={classes.tableHeaderTxt}>
                    Title
                  </Typography>
                </TableCell>
                <TableCell align="right" className={classes.tableHeaderCell}>
                  <Typography variant="h6" className={classes.tableHeaderTxt}>
                    Billable
                  </Typography>
                </TableCell>
                <TableCell
                  className={classes.tableHeaderCell}
                  align="right"
                ></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <TableRow key={row.milestoneId} className={classes.tableRow}>
                  <TableCell
                    component="th"
                    scope="row"
                    className={classes.tableHeaderCell}
                  >
                    <div className="flex_center_start_row">
                      <FormControlLabel
                        classes={{
                          root: classes.checklistCheckboxCnt,
                        }}
                        control={
                          <Checkbox
                            checked={row.milestoneStatus}
                            style={{ padding: "0 5px 0 0" }}
                            onClick={(e) => {
                              props.handleCheck(row, e, "checkMark");
                            }}
                            disableRipple
                            value={""}
                            checkedIcon={
                              <SvgIcon
                                viewBox="0 0 426.667 426.667"
                                htmlColor={theme.palette.status.completed}
                                classes={{
                                  root: classes.checkedIcon,
                                }}
                              >
                                <CheckBoxIcon />
                              </SvgIcon>
                            }
                            icon={
                              <>
                                <SvgIcon
                                  viewBox="0 0 426.667 426.667"
                                  htmlColor={theme.palette.background.items}
                                  classes={{
                                    root: classes.unCheckedIcon,
                                  }}
                                >
                                  <CheckBoxIcon />
                                </SvgIcon>
                                <span className={classes.emptyCheckbox} />
                              </>
                            }
                            color="primary"
                          />
                        }
                      />
                      <div>
                        <Typography
                          variant="h6"
                          className={classes.milestoneTitle}
                        >
                          {row.milestoneTitle}
                        </Typography>
                        <Typography
                          variant="caption"
                          className={classes.milestoneDate}
                        >
                          {moment(row.milestoneDate).format("LL")}
                        </Typography>
                      </div>
                    </div>
                  </TableCell>
                  <TableCell
                    align="right"
                    style={{ padding: "4px 16px 3px 4px" }}
                  >
                    <DefaultCheckbox
                      checkboxStyles={{ padding: 0, marginRight: 0 }}
                      checked={row.billable}
                      onChange={(e) => {
                        props.handleCheck(row, e, "billable");
                      }}
                      fontSize={20}
                    />
                  </TableCell>
                  <TableCell
                    align="right"
                    style={{ padding: "4px 0", width: 20 }}
                  >
                    <DefaultButton
                      buttonType="smallIconBtn"
                      disableRipple
                      onClick={() => {
                        props.removeMilestone(row);
                      }}
                    >
                      <SvgIcon viewBox="0 0 22 22" style={{ fontSize: "19px" }}>
                        <CrossIcon />
                      </SvgIcon>
                    </DefaultButton>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </div>
      </div>
    </Fragment>
  );
}

export default Milestones;
