import React, { useState, useEffect } from "react";
import { withSnackbar } from "notistack";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import combinedStyles from "../../../utils/mergeStyles";

import { withStyles } from "@material-ui/core/styles";
import newProjectStyles from "./styles";
import tableStyle from "../../../components/Table/style";
import classes from "../../../components/TodoList/style";
import addNewFormStyles from "../../AddNewForms/style";
import permissionStyles from "../../WorkspaceSetting/Permissions/styles";
import moment from "moment";
import SvgIcon from "@material-ui/core/SvgIcon";

import {
  saveNewProject,
  getProjectSetting,
  EditProject,
} from "../../../redux/actions/projects";

import DefaultTextField from "../../../components/Form/TextField";
import ProjectDialog from "../../../components/Dialog/projectDialog";
import ButtonActionsCnt from "../../../components/Dialog/ConfirmationDialogs/ButtonActionsCnt";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Details from "./Details";
import Resources from "./Resources";
import isNumeric from "lodash/isNumber";
import isEmpty from "lodash/isEmpty";
import { generateAssigneeData } from "../../../helper/generateSelectData";
import helper from "../../../helper";
import differenceWith from "lodash/differenceWith";
import Tasks from "./Tasks";
import apiInstance from "../../../redux/instance";
import Milestones from "./Milestones";
import FinancialSummary from "./FinancialSummary";
import NotificationMessage from "../../../components/NotificationMessages/NotificationMessages";
import SelectSearchDropdown from "../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import IconActivitySmall from "../../../components/Icons/IconActivitySmall";
import { statusData } from "../../../helper/projectDropdownData";
import {FormattedMessage} from "react-intl";
function ProjectDetails(props) {
  const [detailsTab, setDetailsTab] = useState({
    projectId: null,
    projectTitle: "",
    projectColor: "#F47373",
    projectStatus: "",
    projectDescription: "",
    billingType: "",
    billingMethod: "",
    currency: "",
    fee: "",
    fixedCost: 0 /** if user select fixed rate for per task, per hourly task rate, per resource rate or total project cost */,
    createdDate: null,
    updatedDate: null,
    createdBy: null,
    workSpaceId: null,
    teamId: null,
  });
  useEffect(() => {
    /** function call when component renders */
    const {
      profileState,
      constantsState,
      newProject,
      selectedProject = {},
      theme,
      classes,intl
    } = props;
    let allMembers = profileState.data.member.allMembers;
    let currentUser =
      allMembers.find((cu) => cu.userId === profileState.data.userId) || {};
    generateRowsData(
      currentUser
    ); /** function calling for pre added current user in the resources tab table */
    const resourcesddData = generateResourcesData(); /** function call for generating the drop down data of resources tab */

    let currencyOptions = [];
    constantsState.data.currencies.forEach(function(obj) {
      /** logic for generating the currency drop down data in detail tab */
      const name = `${obj.isoCurrencySymbol} - ${obj.currencyEnglishName}`;
      currencyOptions.push({
        Active: "false",
        value: obj.isoCurrencySymbol,
        label: name,
      });
    });

    setCurrencyOptions(currencyOptions);
    setDDData(resourcesddData);
    setNewProject(newProject);
    setSelectedProject(selectedProject);

    setProjectSatus(); /** function call which sets the project status drop down data and if already created project then assiging status value to the component else assign default values */

    if (!isEmpty(selectedProject)) {
      /** if already created project then assiging saved values to the components */
      setProjectTitle(selectedProject.projectName);
      setProjectDescription(selectedProject.description);
      setSelectedColor(selectedProject.colorCode);
    }

    setDetailsTab({
      ...detailsTab,
      createdBy: profileState.data.userId,
      createdDate: moment().format(),
      teamId: profileState.data.activeTeam,
      workSpaceId: profileState.data.loggedInTeam,
    });
  }, []);

  const setProjectSatus = () => {
    const { theme, classes, selectedProject = {} } = props;
    const projectStatusData = statusData(
      theme,
      classes,intl
    ); /** geeting project status drop down data and setting into state */
    setProjectStatusData(projectStatusData);
    if (!isEmpty(selectedProject)) {
      const selectedStatus =
        projectStatusData.find((p) => p.value == selectedProject.status) || {};
      setProjectStatus(selectedStatus);
    } else {
      setProjectStatus(projectStatusData[0]);
    }
  };

  const [projectStatusData, setProjectStatusData] = useState([]);

  const [newProject, setNewProject] = useState(true);
  const [selectedProject, setSelectedProject] = useState({});

  const [rows, setRows] = useState([]);
  const [ddData, setDDData] = useState([]);
  const generateRowsData = (data) => {
    /** function for creating resources table rows data as needed */
    const row = {
      user: data,
      weeklyCapacity: "40",
      hourlyRate: 0,
      PM: data.teamOwner ? true : false,
      permission: data.teamOwner ? "Project Manager" : "Contributor",
    };
    setRows([...rows, row]);
    filterResourcesDDlist(data.userId, "push");
  };

  const filterResourcesDDlist = (id, val) => {
    /** logic for including and excluding the added/removed member in the resources drop down  */
    const ddData = generateResourcesData();
    let updatedddData = [];
    const arrIds = rows.map((r) => r.user.userId);
    if (id && val == "push") {
      /** true when user add resource from table */
      arrIds.push(id);
      updatedddData = ddData.filter((d) => arrIds.indexOf(d.id) == -1);
    }
    if (id && val == "pop") {
      /** true when user remove resource from table */
      const updatedArrIds = arrIds.filter((a) => a.indexOf(id) == -1);
      updatedddData = ddData.filter((d) => updatedArrIds.indexOf(d.id) == -1);
    }

    setDDData(updatedddData);
  };

  const generateResourcesData = () => {
    /** function calls for creating dddata of resources */
    const { profileState } = props;
    let currentUserID = profileState.data.userId;
    let allMembers = profileState.data.member.allMembers;
    let updatedList = allMembers.filter((m) => {
      return !m.isDeleted && m.userId !== currentUserID;
    });
    const data = generateAssigneeData(updatedList);
    return data;
  };

  const [value, setValue] = useState(0);
  const handleChange = (event, value) => {
    /** function for toggleing the tabs */
    if (!newProject) {
      setValue(value);
      setBulkImport(false);
      setImportButtonText("Import Bulk Tasks");
    }
  };

  const changeTab = (tab = "") => {
    /** function call for toggling the tabs when user select different resource rate or task rate from detail tab view */
    if (tab == "taskTab") {
      setValue(2);
    } else if (tab == "resourceTab") {
      setValue(1);
    }
  };

  const [projectSettings, setProjectSettings] = useState({});

  const [btnQuery, setBtnQuery] = useState("");
  const handleSubmit = () => {
    setBtnQuery("progress");
    /** function call when user clicks create project button */
    if (isEmpty(projectTitle)) {
      setProjectErrorState({
        projectTitleErrorMessage: "Oops! Please enter title",
        projectTitleError: true,
      });
      setBtnQuery("");
    } else {
      let data = {
        projectName: projectTitle.trim(),
        description: projectDescription,
        status: projectStatus.value,
        colorCode: selectedColor,
      };
      props.saveNewProject(
        data,
        (success) => {
          let projectId = success.data.projectId;
          if (projectId) {
            setSelectedProject(success.data);
            props.getProjectSetting(
              projectId,
              (succ) => {
                setProjectSettings(succ);
              },
              (err) => {
                showSnackBar(
                  "Oops! Fetching project settings Failed.",
                  "error"
                );
              }
            );
          }

          setNewProject(false);
          showSnackBar("Hurrah! Project created successfully.", "success");
          setBtnQuery("");
        },
        (err) => {
          if (err.data) showSnackBar(err.data.message, "error");
          setBtnQuery("");
        }
      );
    }
  };

  const downloadTemplate = () => {
    /** function call when user click download template button */
    setBtnQuery("progress");
    const fileName = "nTask-Tasks-Template.xls";
    const exportType = 0; /** export type 0 is for downloading task template for bulk import tasks */
    apiInstance()
      .get(`${ENV == "production1" || ENV == "beta1"  ? BASE_URL_API : BASE_URL }api/base/DownloadTemplate`, {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
        },
        params: {
          type: exportType,
        },
        responseType: "blob",
      })
      .then((response) => {
        if (response.status === 200) {
          setBtnQuery("");
          helper.DOWNLOAD_TEMPLATE(
            response.data,
            fileName,
            "application/vnd.ms-excel"
          );
          showSnackBar(`File downloaded successfully.`, "success");
        }
      })
      .catch((error) => {
        if (error) {
          setBtnQuery("");
          showSnackBar(`Oops! File downloading failed.`, "error");
        }
      });
  };

  const [projectTitle, setProjectTitle] = useState("");
  const [projectErrorState, setProjectErrorState] = useState({
    projectTitleError: false,
    projectTitleErrorMessage: "",
  });
  const handleTextFieldChange = (event) => {
    /** function call when user enter project title */
    setProjectTitle(event.target.value);
    setDetailsTab({ ...detailsTab, projectTitle: event.target.value });
    setProjectErrorState({
      projectTitleError: false,
      projectTitleErrorMessage: "",
    });
  };

  const onKeyDownProjectName = (e) => {
    if (e.key == "Enter" && !newProject) {
      if (!isEmpty(selectedProject)) {
        const newName =
          selectedProject.projectName.trim() == projectTitle.trim();
        if (!newName)
          editProject(projectDescription, selectedColor, projectStatus);
      }
    }
  };

  const handleProjectNameClickAway = (e) => {
    e.preventDefault();

    if (e && !newProject) {
      if (!isEmpty(selectedProject)) {
        const newName =
          selectedProject.projectName.trim() == projectTitle.trim();
        if (!newName)
          editProject(projectDescription, selectedColor, projectStatus);
      }
    }
  };

  const editProject = (description = "", color = "", status = {}) => {
    if (isEmpty(projectTitle)) {
      setProjectErrorState({
        projectTitleErrorMessage: "Oops! Please enter title",
        projectTitleError: true,
      });
      setBtnQuery("");
    } else {
      setSaving(true);
      let data = {
        projectId: selectedProject.projectId,
        projectName: projectTitle.trim(),
        description: description,
        status: status.value,
        colorCode: color,
      };
      props.saveNewProject(
        data,
        (succ) => {
          setSelectedProject(succ.data);
          setSaving(false);
        },
        (err) => {
          setSaving(false);
          if (err.data) showSnackBar(err.data.message, "error");
        }
      );
    }
  };

  const [projectDescription, setProjectDescription] = useState("");
  const handleDescriptionInput = (e) => {
    /** function call when user enter project description */
    if (e) {
      setProjectDescription(e);
      setDetailsTab({ ...detailsTab, projectDescription: e });
      if (!newProject) editProject(e, selectedColor, projectStatus);
    }
  };

  const [subscription, setSubscription] = useState("non-billable");
  const [billingMethod, setBillingMethod] = useState("fixed");
  const [fee, setFee] = useState("sameEveryTask");
  const [selectedFeeMethod, setSelectedFeeMethod] = useState("");
  const [billingMethodVal, setBillingMethodVal] = useState("Fixed Fee");
  const [rateType, setRateType] = useState("");

  const handlePlanChangePayment = (event, paymentPlan) => {
    /** function call when user select subscription, is it billable or non billable (button) from detail tab view */
    if (paymentPlan) {
      setSubscription(paymentPlan);
      setBillingMethod("fixed");
      // setFee("");
      setSelectedFeeMethod("");
      setDetailsTab({ ...detailsTab, billingType: paymentPlan });
    }
  };
  const selectBillingMethod = (obj) => {
    /** function call when user select billing method (fixed, per task, hourly rate or per resource) from detail tab view */

    setBillingMethod(obj.key);
    setBillingMethodVal(obj.name);
    setSelectedFeeMethod("");
    setFee("sameEveryTask");
    setDetailsTab({ ...detailsTab, billingMethod: obj.name });
  };
  const handleFeeInput = (fee) => {
    /** function call when user select fee (Fee dropdown) from detail view*/
    setFee(fee.key);
    setRateType(fee.key);
    setSelectedFeeMethod(fee.name);
    setDetailsTab({ ...detailsTab, fee: fee.name });
  };

  const [selectedColor, setSelectedColor] = useState("#F47373");
  const colorChange = (color) => {
    /** function call when user select/change project color  */
    setSelectedColor(color);
    setDetailsTab({ ...detailsTab, projectColor: color });
    setColorPicker(false);
    if (!newProject) editProject(projectDescription, color, projectStatus);
  };

  const [colorPicker, setColorPicker] = useState(false);
  const handleClickAway = () => {
    /** function call when user opens color picker and then click anywhere else */
    setColorPicker(false);
  };

  const showColorPicker = () => {
    /** function call for showing and disappering the block picker */
    const newColorState = colorPicker ? false : true;
    setColorPicker(newColorState);
  };

  //
  const [currencyOptions, setCurrencyOptions] = useState([]);
  const [currencySymbol, setCurrencySymbol] = useState("USD");
  const [selectedCurrency, setSelectedCurrency] = useState({
    Active: "false",
    value: "USD",
    label: "USD - US Dollar",
  });
  const handleSelectCurrencyChange = (item) => {
    /** function call when user select the desired currency */
    setSelectedCurrency(item);
    setCurrencySymbol(item.value);
    setDetailsTab({ ...detailsTab, currency: item.value });
  };

  const [money, setMoney] = useState(0);
  const handleTypeMoneyInput = (event) => {
    /** function call when user input the money (total project fee/ per task/ per hour) */
    const val = event.target.value;
    if (isNumeric(val) || isEmpty(val)) {
      setMoney(event.target.value);
      setDetailsTab({ ...detailsTab, fixedCost: event.target.value });
    }
  };

  const [projectStatus, setProjectStatus] = useState({});
  const handleStatusSelect = (status) => {
    /** function call when user select the status of the project  */
    if (status) {
      setProjectStatus(status);
      if (!newProject) editProject(projectDescription, selectedColor, status);
    }
  };

  const [resourcesListValues, setResourcesListValues] = useState([]);
  const handleCreateOptionsSelect = (type, option) => {
    /** function for setting/showing the selected resources from drop down in drop down selected view  */
    if (option) setResourcesListValues(option);
  };

  const onKeyDown = (keyName, e, handle) => {
    /** function calls when user select the resource to add and press enter */
    const { profileState } = props;
    let allMembers = profileState.data.member.allMembers;
    if (keyName === "enter") {
      const selectedMember = allMembers.find(
        (a) => a.userId == resourcesListValues.id
      );
      generateRowsData(selectedMember);
      setResourcesListValues([]);
      setCounts({
        ...counts,
        resourcesCount: rows.length + 1,
      });
    }
  };

  const removeResource = (row) => {
    /** function call when user remove resource from the table */
    const updatedRows = rows.filter((r) => r.user.userId !== row.user.userId);
    setRows(updatedRows);
    setCounts({
      ...counts,
      resourcesCount: updatedRows.length,
    });
    filterResourcesDDlist(row.user.userId, "pop");
  };

  const handleChangePermission = (row, obj) => {
    /** function call when user change the permissions of added resources */
    const updatedRows = rows.map((r) => {
      if (r.user.userId == row.user.userId) {
        r.permission = obj.name;
        return r;
      } else return r;
    });
    setRows(updatedRows);
  };

  const handleInputTable = (e, row, val = "") => {
    /** function calls when user add the hourly rate or weekly capacity of the resource */
    if (isNumeric(e.target.value) || isEmpty(e.target.value)) {
      const updatedRows = rows.map((r) => {
        if (r.user.userId == row.user.userId) {
          if (val == "weeklyCapacity") {
            /** true when user enter weekly capacity */
            r.weeklyCapacity = e.target.value;
          }
          if (val == "hourlyRate") {
            /** true when user enter hourly rate */
            r.hourlyRate = e.target.value;
          }
          return r;
        } else return r;
      });
      setRows(updatedRows);
    }
  };

  const handleInputTaskTable = (e, row, val = "") => {
    /** function calls when user add the hourly rate of the task */
    if (isNumeric(e.target.value) || isEmpty(e.target.value)) {
      const updatedRows = taskRows.map((r) => {
        if (r.taskId == row.taskId) {
          if (val == "differentHourlyPerTask") {
            r.hourlyRate = e.target.value;
          }
          if (val == "differentPerTask") {
            r.fixedRate = e.target.value;
          }
          return r;
        } else return r;
      });
      setTaskRows(updatedRows);
    }
  };

  const [taskTitle, setTaskTitle] = useState("");
  const [taskTitleErrorState, setTaskTitleErrorState] = useState({
    taskTitleError: false,
    taskTitleErrorMessage: "",
  });
  const handleTaskTitleChange = (e) => {
    /** function calls when user enter task title */
    if (e) {
      setTaskTitle(e.target.value);
      setTaskTitleErrorState({
        taskTitleError: false,
        taskTitleErrorMessage: "",
      });
    }
  };

  const updateEstimatedTime = (row, hrs, mins) => {
    let totalHours = 0;
    let totalMins = 0;
    const updatedRows = taskRows.map((r) => {
      if (r.taskId == row.taskId) {
        r.hours = hrs;
        r.mins = mins;
        totalHours = parseInt(r.hours) + totalHours;
        totalMins = parseInt(r.mins) + totalMins;
        return r;
      } else {
        totalHours = parseInt(r.hours) + totalHours;
        totalMins = parseInt(r.mins) + totalMins;
        return r;
      }
    });
    setTaskRows(updatedRows);
    setTotalHours(totalHours);
    setTotalMins(totalMins);
  };

  const [taskRows, setTaskRows] = useState([]);
  const handleAddTask = () => {
    /** function calls when user enters task tile and press add task button */
    if (!isEmpty(taskTitle)) {
      const taskRow = {
        taskTitle: taskTitle,
        taskId: Math.floor(
          Math.random() * 100
        ) /** logic for generating a random number and assign to task so when user delete task the id matches for filtering */,
        hours: 0,
        mins: 0,
        hourlyRate: 0,
        fixedRate: 0,
      };
      const taskCount = taskRows.length == 0 ? 1 : taskRows.length + 1;
      setTaskRows([...taskRows, taskRow]);
      setCounts({
        ...counts,
        taskCount: taskCount,
      });
      setTaskTitle("");
    } else {
      /** true when user click add task button without entering task title */
      setTaskTitleErrorState({
        taskTitleErrorMessage: "Oops! Please enter title",
        taskTitleError: true,
      });
    }
  };

  const removeTask = (row) => {
    /** function calls when user delete added task from the table */
    const updatedRows = taskRows.filter((r) => r.taskId !== row.taskId);
    setTaskRows(updatedRows);
    setCounts({
      ...counts,
      taskCount: updatedRows.length,
    });
  };

  const [bulkImport, setBulkImport] = useState(false);
  const [importButtonText, setImportButtonText] = useState("Import Bulk Tasks");
  const handleImportBulkTask = () => {
    /** function for toggleing the import bulk task view and button text  */
    setBulkImport(true);
    setImportButtonText("Go Back");
  };

  const goBack = () => {
    /** function for toggleing back the task view and button text  */
    setBulkImport(false);
    setImportButtonText("Import Bulk Tasks");
  };

  const showSnackBar = (snackBarMessage, type) => {
    /** showing snack bar if case of any error/ success scenario  */
    const { classes, enqueueSnackbar } = props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type || "info",
      }
    );
  };

  const [milestoneTitle, setMilestoneTitle] = useState("");
  const [milestoneTitleErrState, setMilestoneTitleErrState] = useState({
    milestoneTitleErrMess: "",
    milestoneTitleErr: false,
  });
  const handleMilestoneTitle = (e) => {
    /** function call when user enter milestone title  */
    if (e) {
      setMilestoneTitle(e.target.value);
      setMilestoneTitleErrState({
        milestoneTitleErrMess: "",
        milestoneTitleErr: false,
      });
    }
  };

  const [milestoneDate, setMilestoneDate] = useState(new Date());
  const handleMilestoneDate = (date) => {
    /** function calls when user select milestone date */
    if (date) setMilestoneDate(date);
  };

  const [milestoneRows, setMilestoneRows] = useState([]);
  const handleAddMileStone = () => {
    /** function call when user click add milestone button  */
    if (!isEmpty(milestoneTitle)) {
      const milestoneRow = {
        /** logic for setting the row data in table as needed */
        milestoneTitle: milestoneTitle,
        milestoneId: Math.floor(Math.random() * 100),
        milestoneDate: milestoneDate,
        milestoneStatus: false,
        billable: false,
      };
      const milestonesCount /** logic for the count of added milestones */ =
        milestoneRows.length == 0 ? 1 : milestoneRows.length + 1;
      setMilestoneRows([...milestoneRows, milestoneRow]);
      setMilestoneTitle("");
      setMilestoneDate(new Date());
      setCounts({
        ...counts,
        milestonesCount: milestonesCount,
      });
    } else {
      /** true when user click add milestone button without entering the title  */
      setMilestoneTitleErrState({
        milestoneTitleErr: true,
        milestoneTitleErrMess: "Oops! Please enter tile",
      });
    }
  };

  const removeMilestone = (row) => {
    /** function calls when user remove milestone from the table  */
    const updatedRows = milestoneRows.filter(
      (m) => m.milestoneId !== row.milestoneId
    );
    setMilestoneRows(updatedRows);
    setCounts({
      ...counts,
      milestonesCount: updatedRows.length,
    });
  };

  const handleCheck = (row, e, val) => {
    /** function calls when user change the state of the checkboxes in milestone*/
    const updatedRows = milestoneRows.map((m) => {
      if (m.milestoneId == row.milestoneId) {
        if (val == "checkMark") {
          /** true when user change the state of the milestone status  */
          m.milestoneStatus = m.milestoneStatus == false ? true : false;
        }
        if (val == "billable") {
          /** true when user change the state of billable in milestone table */
          m.billable = m.billable == false ? true : false;
        }
        return m;
      } else return m;
    });
    setMilestoneRows(updatedRows);
  };

  const [totalHours, setTotalHours] = useState(0);
  const [totalMins, setTotalMins] = useState(0);
  const [sendEmailCheck, setSendEmailCheck] = useState(true);
  const handleChangeSendEmail = (value) => {
    setSendEmailCheck(value);
  };

  const [exceededPerValue, setExceededPerValue] = useState(80);
  const handleChangePercentage = (e) => {
    if (
      (isNumeric(e.target.value) || isEmpty(e.target.value)) &&
      parseInt(e.target.value) <= 100
    ) {
      setExceededPerValue(e.target.value);
    }
  };

  const getrowLabelValue = () => {
    let average = 0;
    let sum = 0;

    switch (billingMethodVal) {
      case "Fixed Fee":
        return `${currencySymbol} ${money}`; /** if fixed fee then return simply user entered fixed money */
        break;

      case "Fixed Fee per Task":
        if (fee == "sameEveryTask") {
          return `${currencySymbol} ${money}`; /** if same fee then return simply user entered money */
        } else if (fee == "differentPerTask") {
          taskRows.map((t) => {
            sum = parseInt(t.fixedRate) + sum;
          });
          average = sum / taskRows.length;
          return `${currencySymbol} ${average}`;
        }
        break;

      case "Hourly Rate by Task":
        if (fee == "sameHourlyEveryTask") {
          return `${currencySymbol} ${money}`; /** if same fee then return simply user entered money */
        } else if (fee == "differentHourlyPerTask") {
          taskRows.map((t) => {
            sum = parseInt(t.hourlyRate) + sum;
          });
          average = sum / taskRows.length;
          return `${currencySymbol} ${average}`;
        }
        break;

      case "Hourly rate by Resource":
        if (fee == "sameHourlyEveryResource") {
          return `${currencySymbol} ${money}`; /** if same fee then return simply user entered money */
        } else if (fee == "differentHourlyPerResource") {
          rows.map((t) => {
            sum = parseInt(t.hourlyRate) + sum;
          });
          average = sum / rows.length;
          return `${currencySymbol} ${average}`;
        }
        break;

      default:
        return "-";
        break;
    }
  };

  const [estimatedCost, setEstimatedCost] = useState(0);
  const getEstimatedCost = () => {
    let estimatedCost = 0;
    switch (billingMethodVal) {
      case "Fixed Fee":
        estimatedCost = money; /** total project fee / fixed fee */
        return `${currencySymbol} ${estimatedCost}`;
        break;

      case "Fixed Fee per Task":
        if (fee == "sameEveryTask") {
          estimatedCost =
            taskRows.length *
            parseInt(
              money
            ); /** if fixed fee per task then, fixed fee multiply by total no of tasks */
          return `${currencySymbol} ${estimatedCost}`;
        } else if (fee == "differentPerTask") {
          let sum = 0;
          taskRows.map((t) => {
            sum =
              parseInt(t.fixedRate) +
              sum; /** if different fee per task then add all task's fixed fee */
          });
          estimatedCost = sum;
          return `${currencySymbol} ${estimatedCost}`;
        }
        break;

      case "Hourly Rate by Task":
        if (fee == "sameHourlyEveryTask") {
          let hourlyRatePerMin =
            parseInt(money) /
            60; /** calculate per min cost because estimated time contains both hours and mins */
          let totalMins = 0;
          taskRows.map((t) => {
            totalMins =
              parseInt(t.hours) * 60 +
              parseInt(t.mins); /** calculate total mins of all tasks */
          });
          estimatedCost = totalMins * hourlyRatePerMin;
          return `${currencySymbol} ${estimatedCost.toFixed(
            2
          )}`; /** e.g 40.55555 return 40.55 */
        } else if (fee == "differentHourlyPerTask") {
          let totalmins = 0;
          let hourlyRatePerMin = 0;
          let sum = 0;
          taskRows.map((t) => {
            totalmins =
              parseInt(t.hours) * 60 +
              t.mins; /** calculate total mins of each task */
            hourlyRatePerMin =
              parseInt(t.hourlyRate) /
              60; /** calculate hourly min of each task */
            sum = totalmins * hourlyRatePerMin + sum;
          });
          estimatedCost = sum;
          return `${currencySymbol} ${estimatedCost.toFixed(
            2
          )}`; /** e.g 40.55555 return 40.55 */
        }
        break;

      case "Hourly rate by Resource":
        if (fee == "sameHourlyEveryResource") {
          let totalWeeklyCapacity = 0;
          rows.map((t) => {
            totalWeeklyCapacity =
              parseInt(t.weeklyCapacity) +
              totalWeeklyCapacity; /** calculate all resources weekly capacity */
          });
          estimatedCost =
            totalWeeklyCapacity *
            parseInt(
              money
            ); /** multiply fixed fee with total weekly capacity */
          return `${currencySymbol} ${estimatedCost}`;
        } else if (fee == "differentHourlyPerResource") {
          let sum = 0;
          rows.map((t) => {
            sum =
              parseInt(t.weeklyCapacity) * parseInt(t.hourlyRate) +
              sum; /** multiply each weekly capacity with its own hourly rate and add result  */
          });
          estimatedCost = sum;
          return `${currencySymbol} ${estimatedCost}`;
        }
        break;

      default:
        return "-";
        break;
    }
    setEstimatedCost(estimatedCost);
  };

  const [counts, setCounts] = useState({
    resourcesCount: 1,
    taskCount: 0,
    milestonesCount: 0,
  });

  const [completedTasks, setCompletedTasks] = useState("-");
  const [approvedTimeLogged, setApprovedTimeLogged] = useState("-");
  const [pendingApprovalTime, setPendingApprovalTime] = useState("-");
  const [totalCost, setTotalCost] = useState(0);

  const getTotalCost = () => {
    let totalCost = 0;
    switch (billingMethodVal) {
      case "Fixed Fee":
        let completedTasks = 0; /** count of completed tasks*/
        /** getting one task rate by dividing total no of billable tasks with total project fee and then multiply with no of billable completed tasks to get actual cost till today */
        totalCost =
          (parseInt(money) / taskRows.length) *
          completedTasks; /** money variable contains the total project fee */
        return `${currencySymbol} ${totalCost}`;
        break;

      case "Fixed Fee per Task":
        if (fee == "sameEveryTask") {
          let completedTasks = 0; /** count of completed tasks*/
          /** getting one task rate by dividing total no of billable tasks with fixed billable task fee and then multiply with no of billable completed tasks to get actual cost till today */
          totalCost =
            (parseInt(money) / taskRows.length) *
            completedTasks; /** money variable contains the fixed fee per task */
          return `${currencySymbol} ${totalCost}`;
        } else if (fee == "differentPerTask") {
          // totalCost = fee for billable completedTasks 1 + fee for billable completedTasks 2 + ... /** if different fee per task then only add billable completed tasks fee */
          return `${currencySymbol} ${totalCost}`;
        }
        break;

      case "Hourly Rate by Task":
        if (fee == "sameHourlyEveryTask") {
          // totalCost = [Sum of billable approved time of all Tasks (convert into minutes) X hourly rate for billable Task(convert into 1 min rate)]
          return `${currencySymbol} ${totalCost}`;
        } else if (fee == "differentHourlyPerTask") {
          // totalCost =  [Approved time of billable Task (1) x hourly rate for billable Task (1)]
          // + [Approved time of billable Task (2) x hourly rate for billable task (2)] + ...
          /** Note : convert time into minutes and hourly into per min rate and then multiply, and get billable approved time from timesheet */

          return `${currencySymbol} ${totalCost}`;
        }
        break;

      case "Hourly rate by Resource":
        if (fee == "sameHourlyEveryResource") {
          // totalCost = [Sum of approved time of all resources x hourly rate for Resources]
          return `${currencySymbol} ${totalCost}`;
        } else if (fee == "differentHourlyPerResource") {
          //  Total cost = [Approved time of billable Resource (1) x hourly rate for billable Resource (1)]
          // + [Approved time of billable Resource (2) x hourly rate for billable Resource (2)] + ...
          return `${currencySymbol} ${totalCost}`;
        }
        break;

      default:
        return "-";
        break;
    }
    setTotalCost(totalCost);
  };

  const [projectDetail, setProjectDetail] = useState(false);
  const openProjectDetailView = () => {
    /** function for displaying/ hiding the project details view */
    setProjectDetail(projectDetail ? false : true);
  };

  const [fullScreen, setFullScreen] = useState(false);
  const handleFullScreen = () => {
    setFullScreen(fullScreen ? false : true);
  };

  const getProjectCreatedBy = (userId) => {
    const { profileState } = props;
    if (userId) {
      const createdBy =
        profileState.data.member.allMembers.find((m) => m.userId == userId) ||
        {};
      return createdBy.fullName;
    } else return "";
  };

  const getLastSavedValues = () => {
    const { theme, classes } = props;
    let lastSaved;
    let updatedDate = moment(selectedProject.updatedDate);
    let currentDate = moment();
    if (currentDate.diff(updatedDate, "days") == 0) {
      /** 0 means if user updated project today */
      lastSaved = moment(selectedProject.updatedDate).calendar();
    } else if (currentDate.diff(updatedDate, "days") == 1) {
      /** 1 means if user updated project yesterday */
      lastSaved = moment(selectedProject.updatedDate)
        .subtract(1, "days")
        .calendar();
    } else if (currentDate.diff(updatedDate, "days") < 365) {
      /** 365 means if user updated project current year */
      lastSaved = `${moment(selectedProject.updatedDate)
        .format("LL")
        .substring(0, 7)} at ${moment(selectedProject.updatedDate).format(
        "LT"
      )}`;
    } else {
      /** else means if user updated project last year */
      lastSaved = `${moment(selectedProject.updatedDate).format(
        "LL"
      )} at ${moment(selectedProject.updatedDate).format("LT")}`;
    }
    return (
      <Typography
        variant="body2"
        style={{
          fontWeight: theme.typography.fontWeightMedium,
          color: "#202020",
        }}
      >
        {saving ? "Saving..." : `Last saved : ${lastSaved}`}
      </Typography>
    );
  };

  const [saving, setSaving] = useState(false);

  const { classes, theme, closeAction, profileState, projectDialog } = props;

  // let permissionAttachment = getCompletePermissionsWithArchieve(
  //   this.props.taskData,
  //   this.props.permission,
  //   "attachment"
  // );
  const deleteAttachment = true;
  // permissionAttachment === true ? true : permissionAttachment.delete;
  return (
    <ProjectDialog
      title={newProject ? `${projectTitle}` : selectedProject.projectName}
      newProject={newProject}
      fullScreen={fullScreen}
      dialogProps={{
        open: projectDialog,
        disableBackdropClick: true,
        onClose: (event) => props.handleDialogClose(event, "projectDialog"),
        onClick: (e) => {
          e.stopPropagation();
        },
        PaperProps: {
          style: {
            maxWidth: fullScreen ? "100%" : projectDetail ? 960 : 650,
            minHeight: 708,
          },
        },
      }}
      openProjectDetailView={
        openProjectDetailView
      } /** function on click detail view icon */
      handleFullScreen={
        handleFullScreen
      } /** function on click full screen view icon */
      borderRadius={"6px"}
      projectTitle={projectTitle}
      projectErrorState={projectErrorState}
      classes={classes}
      theme={theme}
      handleTextFieldChange={handleTextFieldChange}
      colorPicker={colorPicker}
      showColorPicker={showColorPicker}
      colorChange={colorChange}
      selectedColor={selectedColor}
      onKeyDownProjectName={onKeyDownProjectName}
      handleProjectNameClickAway={handleProjectNameClickAway}
      handleStatusSelect={handleStatusSelect}
      projectStatus={projectStatus}
      statusDropDownData={projectStatusData}
    >
      <div style={{ display: "flex" }}>
        <div className={classes.addProjectDiv}>
          {!newProject && (
            <Tabs
              value={value}
              // action={(obj) => {
              //   setTimeout(() => {
              //     obj.updateIndicator();
              //   }, 500);
              // }}
              onChange={handleChange}
              fullWidth
              classes={{
                root: classes.TabsRoot,
                indicator: classes.tabIndicator,
              }}
            >
              {/* <Tab
                disableRipple={true}
                classes={{
                  root: classes.tab,
                  labelContainer: classes.tabLabelCnt,
                  selected: classes.tabSelected,
                }}
                label={
                  <Typography variant="h6" className={classes.tabLabel}>
                    <FormattedMessage id="project.dev.details.title"  defaultMessage="Details"/>{" "}
                  </Typography>
                }
              />
              <Tab
                disableRipple={true}
                classes={{
                  root: classes.tab,
                  labelContainer: classes.tabLabelCnt,
                  selected: classes.tabSelected,
                }}
                label={
                  <Typography
                    variant="h6"
                    className={classes.tabLabel}
                  ><FormattedMessage id="project.dev.resources.title" defaultMessage="Resources"/>{` (${counts.resourcesCount}) `}</Typography>
                }
              />
              <Tab
                disableRipple={true}
                classes={{
                  root: classes.tab,
                  labelContainer: classes.tabLabelCnt,
                  selected: classes.tabSelected,
                }}
                label={
                  <Typography
                    variant="h6"
                    className={classes.tabLabel}
                  ><FormattedMessage id="project.dev.tasks.title" defaultMessage="Tasks"/>{` (${counts.taskCount}) `}</Typography>
                }
              /> */}
              <Tab
                disableRipple={true}
                classes={{
                  root: classes.tab,
                  wrapper:  classes.tabLabelCnt,
                  selected: classes.tabSelected,
                }}
                label={
                  <Typography
                    variant="h6"
                    className={classes.tabLabel}
                  ><FormattedMessage id="project.dev.milestones.title" defaultMessage="Milestones"/>{` (${counts.milestonesCount}) `}</Typography>
                }
              />
              <Tab
                disableRipple={true}
                classes={{
                  root: classes.tab,
                  wrapper:  classes.tabLabelCnt,
                  selected: classes.tabSelected,
                }}
                label={
                  <Typography variant="h6" className={classes.tabLabel}>
                    <FormattedMessage id="project.dev.fianancial-summary.title" defaultMessage="Financial Summary"/>
                  </Typography>
                }
              />
            </Tabs>
          )}
          {value === 0 && (
            <Details
              classes={classes}
              theme={theme}
              projectDescription={projectDescription}
              subscription={subscription}
              billingMethod={billingMethod}
              money={money}
              fee={fee}
              billingMethodVal={billingMethodVal}
              selectedFeeMethod={selectedFeeMethod}
              handleDescriptionInput={handleDescriptionInput}
              handlePlanChangePayment={handlePlanChangePayment}
              selectBillingMethod={selectBillingMethod}
              handleFeeInput={handleFeeInput}
              handleTypeMoneyInput={handleTypeMoneyInput}
              changeTab={changeTab}
              currencyOptions={currencyOptions}
              selectedCurrency={selectedCurrency}
              handleSelectCurrencyChange={handleSelectCurrencyChange}
              showSnackBar={showSnackBar}
              currencySymbol={currencySymbol}
              newProject={newProject}
              setProjectDescription={handleDescriptionInput}
              intl={intl}
            />
          )}
          {value === 1 && (
            <Resources
              classes={classes}
              theme={theme}
              profileState={profileState}
              rateType={rateType}
              resourcesListValues={resourcesListValues}
              rows={rows}
              ddData={ddData}
              handleCreateOptionsSelect={handleCreateOptionsSelect}
              onKeyDown={onKeyDown}
              removeResource={removeResource}
              handleInputTable={handleInputTable}
              showSnackBar={showSnackBar}
              handleChangePermission={handleChangePermission}
              currencySymbol={currencySymbol}
            />
          )}
          {value === 2 && (
            <Tasks
              classes={classes}
              theme={theme}
              bulkImport={bulkImport}
              rateType={rateType}
              taskTitle={taskTitle}
              handleTaskTitleChange={handleTaskTitleChange}
              taskRows={taskRows}
              handleAddTask={handleAddTask}
              handleInputTaskTable={handleInputTaskTable}
              removeTask={removeTask}
              showSnackBar={showSnackBar}
              updateEstimatedTime={updateEstimatedTime}
              currencySymbol={currencySymbol}
              taskTitleErrorState={taskTitleErrorState}
            />
          )}
          {value === 3 && (
            <Milestones
              classes={classes}
              theme={theme}
              milestoneTitle={milestoneTitle}
              handleMilestoneTitle={handleMilestoneTitle}
              milestoneDate={milestoneDate}
              handleMilestoneDate={handleMilestoneDate}
              rows={milestoneRows}
              handleAddMileStone={handleAddMileStone}
              removeMilestone={removeMilestone}
              handleCheck={handleCheck}
              milestoneTitleErrState={milestoneTitleErrState}
              intl={intl}
            />
          )}
          {value === 4 && (
            <FinancialSummary
              classes={classes}
              theme={theme}
              counts={counts}
              totalHours={totalHours}
              totalMins={totalMins}
              billingMethodVal={billingMethodVal}
              selectedFeeMethod={selectedFeeMethod}
              exceededPerValue={exceededPerValue}
              handleChangePercentage={handleChangePercentage}
              handleChangeSendEmail={handleChangeSendEmail}
              sendEmailCheck={sendEmailCheck}
              fee={fee}
              getrowLabelValue={getrowLabelValue}
              getEstimatedCost={getEstimatedCost}
              completedTasks={completedTasks}
              approvedTimeLogged={approvedTimeLogged}
              pendingApprovalTime={pendingApprovalTime}
              getTotalCost={getTotalCost}
              // estimatedCost={estimatedCost}
            />
          )}

          {!newProject ? (
            <div className={classes.createdBy}>
              <Typography variant="body2">
                <SvgIcon
                  viewBox="0 0 14 11.195"
                  style={{ fontSize: theme.typography.fontSize }}
                >
                  <IconActivitySmall />{" "}
                </SvgIcon>{" "}
                {/* <FormattedMessage id="project.dev.footer.created-on" defaultMessage="Created on:"/>{" "}
                {`${moment(projectSettings.createdDate).format(
                  "LL"
                )} ${intl.formatMessage({id:"project.dev.footer.at", defaultMessage:"at"})} ${moment(projectSettings.createdDate).format(
                  "LT"
                )} ${intl.formatMessage({id:"project.dev.footer.by", defaultMessage:"by"})} ${getProjectCreatedBy(projectSettings.createdBy)}`} */}
               <FormattedMessage id="project.dev.footer.leftlabel" values={{t1: moment(projectSettings.createdDate).format("LL"),t2: moment(
              projectSettings.createdDate
            ).format("LT"),t3: getProjectCreatedBy(
              projectSettings.createdBy
            )}} defaultMessage={`Created on:
            ${moment(projectSettings.createdDate).format("LL")} at ${moment(
              projectSettings.createdDate
            ).format("LT")} by ${getProjectCreatedBy(
              projectSettings.createdBy
            )}`}/>
              </Typography>
              {getLastSavedValues()}
            </div>
          ) : (
            <div className={classes.buttonCnt}>
              <ButtonActionsCnt
                cancelAction={closeAction}
                successAction={bulkImport ? downloadTemplate : handleSubmit}
                successBtnText={"Create Project"}
                cancelBtnText="Cancel"
                btnType="success"
                btnQuery={btnQuery}
                deleteBtnText={""}
                deleteAction={handleImportBulkTask}
                btnTypeDelete="plain"
                btnTypeVariant="outlined"
                disabled={projectTitle == "" ? true : false}
              />
            </div>
          )}
        </div>
      </div>
    </ProjectDialog>
  );
}
const mapStateToProps = (state) => {
  return {
    profileState: state.profile,
    constantsState: state.constants,
  };
};

export default compose(
  withRouter,
  withSnackbar,
  withStyles(
    combinedStyles(
      newProjectStyles,
      tableStyle,
      classes,
      addNewFormStyles,
      permissionStyles
    ),
    {
      withTheme: true,
    }
  ),
  connect(mapStateToProps, {
    saveNewProject,
    getProjectSetting,
    EditProject,
  })
)(ProjectDetails);
