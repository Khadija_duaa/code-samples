import React, { Component, Fragment } from "react";
import newProjectStyles from "./styles";
import DefaultTextField from "../../../components/Form/TextField";
import Typography from "@material-ui/core/Typography";
import CustomButton from "../../../components/Buttons/CustomButton";
import TaskTable from "../../../components/Table/ProjectTaskTable";
import NotificationMessage from "../../../components/NotificationMessages/NotificationMessages";
import ImportBulk from "./CsvUploadDialog";
import Hotkeys from "react-hot-keys";

function Tasks(props) {
  const {
    classes,
    theme,
    taskTitle = "",
    taskRows,
    rateType,
    bulkImport,
    currencySymbol,
    taskTitleErrorState={}
  } = props;

  const onKeyDown = (keyName, e) => {
    if (keyName === "enter") {
      props.handleAddTask();
    }
  };

  return (
    <Fragment>
      <div className={classes.dialogCnt}>
        {!bulkImport ? (
          <>
            <div style={{ display: "flex" }}>
              <DefaultTextField
                label="Add/Create New Task"
                helptext=""
                errorState={taskTitleErrorState.taskTitleError}
                errorMessage={taskTitleErrorState.taskTitleErrorMessage}
                formControlStyles={{
                  marginBottom: 0,
                }}
                defaultProps={{
                  type: "text",
                  id: "taskTitle",
                  placeholder: "Enter Task Title",
                  value: taskTitle,
                  autoFocus: false,
                  inputProps: { maxLength: 80 },
                  onChange: props.handleTaskTitleChange,
                }}
              />
              <div style={{ alignSelf: "center" }}>
                <Hotkeys keyName="enter" onKeyDown={onKeyDown}>
                  <CustomButton
                    style={{ marginLeft: 20, minWidth: 100 }}
                    variant="contained"
                    btnType="success"
                    disabled={false}
                    onClick={props.handleAddTask}
                  >
                    Add Task
                  </CustomButton>
                </Hotkeys>
              </div>
            </div>
            <TaskTable
              classes={classes}
              theme={theme}
              rows={taskRows}
              rateType={rateType}
              removeTask={props.removeTask}
              currencySymbol={currencySymbol}
              // handleInputTable={props.handleInputTaskTable}
              handleInputTaskTable={props.handleInputTaskTable}
              updateEstimatedTime={props.updateEstimatedTime}
            />
          </>
        ) : (
          <>
            <Typography variant="h6" className={classes.heading}>
              Import Bulk Tasks
            </Typography>
            <div className={classes.importBulkCnt}>
              <ImportBulk
                type={"task"}
                theme={theme}
                handleExportType={props.showSnackBar}
                showMessage={props.showSnackBar}
                handleRefresh={() => {}}
              />
            </div>
            <NotificationMessage
              type="info"
              iconType="info"
              style={{
                //   width: "calc(100% - 176px)",
                padding: "10px 15px 10px 15px",
                backgroundColor: theme.palette.background.light,
                borderRadius: 4,
              }}
            >
              Please make sure your csv file matches the format of nTask
              template. You can also download the template by clicking "Download
              Template" button.
            </NotificationMessage>
          </>
        )}
      </div>
    </Fragment>
  );
}
export default Tasks;
