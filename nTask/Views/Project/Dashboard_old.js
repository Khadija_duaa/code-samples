 import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import dashboardStyles from "./styles";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import MenuItem from "@material-ui/core/MenuItem";
import differenceWith from "lodash/differenceWith";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import selectStyles from "../../assets/jss/components/select";
import combineStyles from "../../utils/mergeStyles";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import InputLabel from "@material-ui/core/InputLabel";
import ArchivedIcon from "@material-ui/icons/Archive";
import TaskGridItem from "./Grid/Item";
import ProjectCalendar from "./Calendar/Calendar";
import ProjectList from "./List/ProjectList.view";
import ImportExportDD from "../../components/Dropdown/ImportExportDD";
import { withSnackbar } from "notistack";
import ProjectFilter from "./ProjectFilter/ProjectFilter";
import classNames from "classnames";
import Footer from "../../components/Footer/Footer";
import ProjectGantt from "./Gantt/GanttChart";
import LeftArrow from "@material-ui/icons/ChevronLeft";
import { getGanttTasks } from "../../redux/actions/gantt";
import { showLoading, hideLoading } from "react-redux-loading-bar";
import CustomIconButton from "../../components/Buttons/IconButton";
import queryString from "query-string";
import { getSortOrder, sortListData } from "../../helper/sortListData";
import {
  saveSortingProjectList,
  projectDetails, getProjects,
} from "../../redux/actions/projects";
import SortingDropdown from "../../components/Dropdown/SortingDropdown/SortingDropdown";
import {
  FetchWorkspaceInfo,
  getArchivedData,
} from "../../redux/actions/workspace";
import { calculateContentHeight, isScrollBottom } from "../../utils/common";
import ProjectSetting from "./ProjectSetting/ProjectSetting";
import EmptyState from "../../components/EmptyStates/EmptyState";
import { canEdit, canDo } from "./permissions";
import SvgIcon from "@material-ui/core/SvgIcon";
import FullScreenIcon from "../../components/Icons/FullScreen";
import DefaultSwitch from "../../components/Form/Switch";
import ViewDropdown from "./Gantt/ViewDropdown";
// import PlannedVsActualDropDown from "./Gantt/PlannedVsActualDropDown";
import PlannedVsActualDropDown from "./plannedVsActualDropDown";
import { teamCanView } from "../../components/PlanPermission/PlanPermission";
import UnPlanned from "../billing/UnPlanned/UnPlanned";
import Gantt from "../../assets/images/unplanned/Gantt.png";
import QuickFilterIcon from "../../components/Icons/QuickFilterIcon";
import PlainMenu from "../../components/Menu/menu";
import MenuList from "@material-ui/core/MenuList";
import CustomButton from "../../components/Buttons/CustomButton";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import { GetPermission } from "../../components/permissions";
import { FormattedMessage, injectIntl } from "react-intl";
import isEmpty from "lodash/isEmpty";
import GroupingDropdown from "../../components/Dropdown/GroupingDropdown/GroupingDropdown";
import {TRIALPERIOD} from '../../constants/planConstant';

let screenfull = require("screenfull");
let increment = 20;
class ProjectDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      alignment: "left",
      name: [],
      open: false,
      placement: "bottom-start",
      anchorEl: "",
      listView: true,
      gridView: false,
      calendarView: false,
      isArchived: false,
      openFilterSidebar: false,
      multipleFilters: "",
      clearlastState: false,
      filteredProjects: [],
      quickFilters: [],
      total: 0,
      loadMore: 0,
      isChanged: false,
      projectGantt: false,
      selectedProject: "",
      showRecords: increment,
      projectId: "",
      currency: "",
      ganttExpand: false,
      selectedView: "",
      plannedVsActual: "Planned",
      calenderDateType: "Planned Start/End",
      fullScreen: false,
      sortObj: this.getSortValues(),
      sortingDropDownData: [
        { key: "", name: "None" },
        { key: "uniqueId", name: "ID" },
        { key: "projectName", name: "Project Title" },
      ],
      groupingDropDownData: [
        { key: "", name: "None" },
        { key: "status", name: "Status" },
        { key: "projectManager", name: "Project Managers" },
      ],
      currentProject: {},
      selectedGroup:{}
    };
    this.allFilterList = [{ key: "Archived", value: "Archived" }];
    this.child = React.createRef();
    this.renderListView = this.renderListView.bind(this);
    this.renderCalendarView = this.renderCalendarView.bind(this);
    this.renderGridView = this.renderGridView.bind(this);
    this.handleArchiveChange = this.handleArchiveChange.bind(this);
    this.handleExportType = this.handleExportType.bind(this);
    this.searchFilter = this.searchFilter.bind(this);
    this.searchFilterApplied = this.searchFilterApplied.bind(this);
    this.clearFilter = this.clearFilter.bind(this);
    this.returnCount = this.returnCount.bind(this);
    this.handleChangeState = this.handleChangeState.bind(this);
    this.handleProjectGridClick = this.handleProjectGridClick.bind(this);
    this.handleProjectListClick = this.handleProjectListClick.bind(this);
    this.handleBackBtn = this.handleBackBtn.bind(this);
    this.resetCount = this.resetCount.bind(this);
  }
  // startTrialPlanBilling = (selectedPackage, callback) => {
  //   const { teamId } = this.props;
  //   let mode = selectedPackage.planType;
  //   let companyInfo = {
  //     mode: mode,
  //     companyId: teamId,
  //   };
  //   this.props.StartTrialPlanBilling(
  //     companyInfo,
  //     (success) => {
  //       this.props.showSnackBar(success.data.status);
  //       callback();
  //     },
  //     (fail) => {
  //       this.props.showSnackBar(fail.data);
  //     }
  //   );
  // };
  getSortValues = () => {
    const { workspaces } = this.props;
    let loggedInTeam = this.props.loggedInTeam;

    if (loggedInTeam) {
      let { projectColumn, projectDirection } = getSortOrder(workspaces, loggedInTeam, 3);

      if (projectColumn && projectDirection) {
        return {
          column: projectColumn,
          direction: projectDirection,
          cancelSort: "",
        };
      } else {
        return { column: "", direction: "", cancelSort: "NONE" };
      }
    }
  };

  resetCount() {
    this.setState({ showRecords: increment, loadMore: increment });
  }
  returnCount(total, loadMore) {
    this.setState({ total, loadMore });
  }
  handleScrollDown = e => {
    const bottom = isScrollBottom(e);
    if (bottom) {
      if (this.state.total > this.state.showRecords)
        this.setState({ showRecords: this.state.showRecords + increment }, () => {
          this.setState({ loadMore: this.state.showRecords }, () => {
            if (this.state.total < this.state.showRecords) {
              this.setState({
                showRecords: this.state.total,
                loadMore: this.state.total,
              });
            }
          });
        });
    }
  };
  handleChangeState = () => {
    this.setState({ isChanged: false });
  };
  clearFilter() {
    this.setState({ multipleFilters: "", clearlastState: false });
  }
  searchFilterApplied() {
    this.setState({ multipleFilters: "" });
  }
  searchFilter(data) {
    this.setState({ multipleFilters: data, isChanged: true }, () => {
      this.setState({ name: [] });
    });
  }

  setInitialGrouping = () => {
    // Function called to set the initial value of group by in task
    const { itemOrderState } = this.props;
    const taskGroupBy = itemOrderState.groupByColumn.projectGroupBy || [""];
    const selectedGroupByValue = this.state.groupingDropDownData.find(g => g.key == taskGroupBy) || {}; // Finding Column object based on the column key saved on backend for grouping
    this.setState({ selectedGroup: selectedGroupByValue });
  };

  componentDidMount() {
    const { workspaces, loggedInTeam } = this.props;
    this.props.getProjects(null, null,
      //success
      () => {

    })
    if (loggedInTeam) {
      let { sortColumn, sortDirection } = getSortOrder(workspaces, loggedInTeam, 1);
      if (sortColumn && sortDirection) {
        this.setState({
          sortObj: { column: sortColumn, direction: sortDirection },
        });
      } else {
        this.setState({
          sortObj: { column: "", direction: "", cancelSort: "NONE" },
        });
      }
    }

    this.setState({
      loggedInTeam: this.props.loggedInTeam,
      filteredProjects: this.props.projects,
    });
    this.redirectToGantt();
    this.queryProjectUrl();
    this.setInitialGrouping();
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.location.search !== this.props.location.search) {
      this.redirectToGantt();
    }
    const isArchiveSelected =
      this.state.quickFilters.length &&
      this.state.quickFilters[this.state.quickFilters.length - 1] === "Archived";
    const { projects } = this.props;
    if (JSON.stringify(prevProps.projects) !== JSON.stringify(projects) && !isArchiveSelected) {
      this.setState({ filteredProjects: projects });
    }
    this.queryProjectUrl();
  }
  redirectToGantt = () => {
    let projectId = queryString.parse(this.props.location.search).projectId;

    if (projectId) {
      this.setState({
        projectGantt: false,
      });
      this.props.showLoading();
      this.props.getGanttTasks(projectId, response => {
        const { currency } = response.data;
        this.setState({
          projectGantt: true,
          selectedProject: projectId,
          currency: currency ? currency : "USD",
        });
        this.props.hideLoading();
      });
    }
  };
  queryProjectUrl = () => {
    const {
      projects,
      projectDetailsDialog: { docked, fullView, dialog, projectDetails },
    } = this.props;
    const { filteredProjects } = this.state;
    const searchQuery = this.props.history.location.search;

    if (searchQuery) {
      const objectType = this.props.history.location.pathname.split("/")[1]; // Parsing the url and extracting the object type using query string
      let objectId = "";
      if (objectType == "projects") {
        let projectIdInUrl = queryString.parse(this.props.location.search).projectId;
        if (
          projectIdInUrl &&
          projectIdInUrl !== "" &&
          (projectDetails == undefined || isEmpty(projectDetails))
        ) {
          let validateProject = projects.findIndex(project => {
            // Checking if the issue id is valid or not
            return projectIdInUrl == project.projectId;
          });
          if (validateProject > -1) {
            let currentProject = projects.find(p => p.projectId == projectIdInUrl) || false;
            this.props.history.push("/projects");

            if (currentProject) {
              this.props.projectDetails({
                projectDetails: currentProject,
                fullView: fullView ? true : false,
                docked: docked ? true : false,
                dialog: true,
              });
            }
          } else {
            // this.props.showSnackBar("Oops! Item not found", 'info');
            //In case the issue id in the url is wrong user is redirected to issues Page
            this.props.history.push("/projects");
          }
        }
      }
    }
  };
  handleProjectListClick(project) {
    const { profileState, permissions } = this.props;
    if (
      GetPermission.canDo(project.isOwner, permissions, "accessGantt") ||
      (project.projectManager && project.projectManager.indexOf(profileState.userId) > -1)
    ) {
      this.props.showLoading();
      this.props.getGanttTasks(project.projectId, response => {
        const { currency } = response.data;
        this.setState({
          // projectGantt: true,
          selectedProject: project.projectId,
          currency: currency ? currency : "USD",
        });
        // this.props.history.push({
        //   to: "/gantt",
        //   search: `?projectId=${project.projectId}`,
        // });
        this.props.history.push(`/gantt?projectId=${project.projectId}`);
        this.props.hideLoading();
      });
    }
  }
  handleProjectGridClick(project) {
    this.props.showLoading();
    this.props.getGanttTasks(project.projectId, response => {
      const { currency } = response.data;
      this.setState({
        // projectGantt: true,
        selectedProject: project.projectId,
        currency: currency ? currency : "USD",
      });
      this.props.history.push({
        to: "/projects",
        search: `?projectId=${project.projectId}`,
      });
      this.props.hideLoading();
    });
  }
  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.loggedInTeam !== nextProps.loggedInTeam)
      return { loggedInTeam: nextProps.loggedInTeam, name: [] };
    else return { loggedInTeam: prevState.loggedInTeam, name: prevState.name };
  }
  handleDrawerOpen = () => {
    this.setState({ openFilterSidebar: true });
  };

  handleDrawerClose = () => {
    this.setState({ openFilterSidebar: false });
  };

  handleExportType(snackBarMessage, type) {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  }
  handleChange = event => {
    this.setState({ name: event.target.value, isChanged: true }, () => {
      this.setState({ multipleFilters: "", clearlastState: true });
    });
  };
  handleBackBtn() {
    this.props.history.goBack();
  }
  handleArchiveChange() {
    this.setState({ isArchived: !this.state.isArchived });
  }
  renderListView() {
    this.setState({
      listView: true,
      gridView: false,
      calendarView: false,
      alignment: "left",
    });
  }
  renderGridView() {
    this.setState({
      listView: false,
      gridView: true,
      calendarView: false,
      alignment: "center",
    });
  }
  renderCalendarView() {
    this.setState({
      listView: false,
      gridView: false,
      calendarView: true,
      alignment: "right",
    });
  }
  handleClick(event, placement) {
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }
  handleClose = () => {
    if (this.anchorEl.contains(event.target)) {
      return;
    }
    this.setState({ open: false });
  };
  handleRefresh = () => {
    this.props.FetchWorkspaceInfo("", () => {});
  };
  handleAlignment = (event, alignment) => this.setState({ alignment });
  openProjectSetting = (event, projectId, projectName, project) => {
    this.setState({ projectId, projectName, currentProject: project });
  };
  closeProjectSetting = () => {
    this.setState({ projectId: "", projectName: "" });
  };
  handleClearArchived = () => {
    this.setState({ quickFilters: [], filteredProjects: this.props.projects, open: false });
  };
  handleSelectClick = value => {
    // const { value } = event.target;
    // Preventing from default open dialog click
    let items = value;
    let { quickFilters } = this.state;
    if (value !== "Archived") {
      if (value == "Show All") items = ["Show All"];
      else {
        if (quickFilters.indexOf("Show All") > -1) {
          quickFilters = quickFilters.filter(item => item != "Show All");
        }
        quickFilters.indexOf(value) == -1
          ? (items = [...quickFilters, value])
          : (items = quickFilters.filter(item => item !== value));
      }
    } else {
      items = ["Archived"];
      getArchivedData(
        1,
        resp => {
          this.setState({
            quickFilters: [value],
            filteredProjects: resp.data,
          });
        },
        error => {}
      );
    }
    this.setState({ quickFilters: items || [] });
  };
  handleFullScreen = () => {
    this.setState(
      prevState => ({ fullScreen: !prevState.fullScreen }),
      () => {
        if (this.state.fullScreen) {
          let elem = document.getElementById("mainContentCnt");
          if (elem.requestFullscreen) {
            elem.requestFullscreen();
          } else if (elem.mozRequestFullScreen) {
            /* Firefox */
            elem.mozRequestFullScreen();
          } else if (elem.webkitRequestFullscreen) {
            /* Chrome, Safari and Opera */
            elem.webkitRequestFullscreen();
          } else if (elem.msRequestFullscreen) {
            /* IE/Edge */
            elem.msRequestFullscreen();
          }
          // document.documentElement.requestFullscreen();
          document.querySelector("#mainContentCnt").style.marginTop = 0;
          document.querySelector("#sidebar").style.display = "none";
          document.querySelector("#header").style.display = "none";
          // document.querySelector("#footer").style.display = "none";
        } else {
          if (document.exitFullscreen) {
            document.exitFullscreen();
          } else if (document.mozCancelFullScreen) {
            /* Firefox */
            document.mozCancelFullScreen();
          } else if (document.webkitExitFullscreen) {
            /* Chrome, Safari and Opera */
            document.webkitExitFullscreen();
          } else if (document.msExitFullscreen) {
            /* IE/Edge */
            document.msExitFullscreen();
          }
          // document.exitFullscreen();
        }
      }
    );
    screenfull.onchange(() => {
      if (!screenfull.isFullscreen) {
        this.setState({ fullScreen: false });
        document.querySelector("#mainContentCnt").style.marginTop = "";
        document.querySelector("#sidebar").style.display = "";
        document.querySelector("#header").style.display = "";
        // document.querySelector("#footer").style.display = "";
      }
    });
  };
  handleProjectsDateType = (event, type) => {
    this.setState({ calenderDateType: type });
  };
  handleGanttView = (event, view) => {
    this.setState(prevState => ({
      selectedView: prevState.selectedView == view ? "" : view,
    }));
  };
  handleTaskbarsType = (event, type) => {
    this.setState({ plannedVsActual: type });
  };

  handleSwitch = name => {
    this.setState({ [name]: !this.state[name] });
  };
  filterProject = ids => {
    this.setState(({ filteredProjects }) => {
      return {
        filteredProjects: differenceWith(filteredProjects, ids, (project, id) => {
          return id === project.projectId;
        }),
      };
    });
  };
  sortingProjectList = (column, direction, cancelSort = "") => {
    /** function passing to sorting drop down for getting the selected value  */
    this.setState({
      sortObj: { column: column, direction: direction, cancelSort: cancelSort },
    }); /** updating state object and passing sortObj to project List */

    let obj = {
      project: [column],
      type: 1,
      ProjectOrder: direction,
    };
    this.props.saveSortingProjectList(
      obj,
      succ => {},
      err => {}
    );
  };

  filteredProjects = () => {
    /** function for sorting if there is already saved value of sorted column and sort order */
    const { sortObj, filteredProjects } = this.state;
    if (sortObj && sortObj.column && sortObj.direction) {
      let data = sortListData(filteredProjects, sortObj.column, sortObj.direction);
      return data;
    } else if (sortObj && sortObj.cancelSort == "NONE") {
      return this.props.projects;
    } else {
      return this.props.projects;
    }
  };

  //Lifecycle method on group select
  onGroupSelect = option => {
    this.setState({ selectedGroup: option });
  };

  render() {
    const { classes, theme, ganttTasks, quote, workspaces, loggedInTeam, projectPer } = this.props;
    const {
      alignment,
      calendarView,
      gridView,
      openFilterSidebar,
      multipleFilters,
      projectGantt,
      selectedProject,
      filteredProjects,
      quickFilters,
      showRecords,
      projectId,
      projectName,
      currency,
      ganttExpand,
      plannedVsActual,
      calenderDateType,
      selectedView,
      sortingDropDownData,
      groupingDropDownData,
      selectedGroup
    } = this.state;

    // const MenuProps = {
    //   MenuListProps: {
    //     disablePadding: true,
    //   },
    //   PaperProps: {
    //     style: {
    //       overflow: "visible",
    //       transform: "translateY(40px)",
    //       borderRadius: 4,
    //       overflow: "hidden",
    //     },
    //   },
    //   anchorEl: this.anchorEl,
    // };
    const isArchived = quickFilters.length && quickFilters[quickFilters.length - 1] === "Archived";

    let projectIdParam = queryString.parse(this.props.location.search).projectId;
    const selectedFilter = [];
    this.allFilterList.map(item => {
      if (quickFilters.indexOf(item.key) != -1) selectedFilter.push(item.value);
    });
    const customFilterSelect =
      selectedFilter.length == 0 ||
      (selectedFilter.length == 1 && selectedFilter.indexOf("Show All") > -1)
        ? false
        : true;
    const selectedFiltersLbl = customFilterSelect
      ? selectedFilter.length > 1
        ? selectedFilter[0] + " & +" + (selectedFilter.length - 1)
        : selectedFilter[0]
      : this.props.intl.formatMessage({ id: "common.all.label", defaultMessage: "All" });

    return
    !teamCanView("projectAccess") ? (
      <div className={classes.unplannedMain}>
        <div className={classes.unplannedCnt}>
          <UnPlanned
            feature="premium"
            titleTxt={
              <FormattedMessage
                id="common.discovered-dialog.premium-title"
                defaultMessage="Wow! You've discovered a premium feature!"
              />
            }
            boldText={this.props.intl.formatMessage({
              id: "project.label",
              defaultMessage: "Projects",
            })}
            descriptionTxt={
              <FormattedMessage
                id="common.discovered-dialog.list.project.label"
                defaultMessage={"is available on our premium plan. Upgrade now or start a {TRIALPERIOD}-day free trial to find out all about nTask premium features."}
                values={{TRIALPERIOD: TRIALPERIOD}}
              />
            }
            showBodyImg={true}
            showDescription={true}
            imgUrl={Gantt}
          />
        </div>
      </div>
    ) : projectId ? (
      <ProjectSetting
        projectId={projectId}
        projectName={projectName}
        closeProjectSetting={this.closeProjectSetting}
      />
    ) : (
      <div className={classes.root}>
        {projectIdParam && projectGantt ? (
          <Grid container classes={{ container: classes.taskDashboardCnt }}>
            {/* <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="center"
              classes={{ container: classes.taskDashboardHeader }}
            >
              <div className="flex_center_start_row">
                <LeftArrow
                  onClick={this.handleBackBtn}
                  className={classes.backArrowIcon}
                />
                <Typography
                  className={classes.projectHeading}
                  title={ganttTasks.projectName}
                  variant="h1"
                >
                  {ganttTasks.projectName}
                </Typography>
              </div>
              <div className="flex_center_start_row">
                <ViewDropdown
                  handleGanttView={this.handleGanttView}
                  selectedView={selectedView}
                  taskBarsType={taskBarsType}
                />
                <PlannedVsActualDropDown
                  handleTaskbarsType={this.handleTaskbarsType}
                  taskBarsType={taskBarsType}
                />

                <CustomIconButton
                  onClick={this.handleFullScreen}
                  btnType="filledWhite"
                  style={{ padding: "9px" }}
                >
                  <SvgIcon
                    viewBox="0 0 13.97 13.812"
                    htmlColor={theme.palette.secondary.light}
                    className={classes.fullScreenIcon}
                  >
                    <FullScreenIcon />
                  </SvgIcon>
                </CustomIconButton>
              </div>
              <Grid
                container
                direction="row"
                justify="flex-end"
                alignItems="center"
              />
            </Grid> */}
            <Grid container classes={{ container: classes.dashboardContentCnt }}>
              <Grid item classes={{ item: classes.taskGanttCnt }}>
                <ProjectGantt
                  projectId={selectedProject}
                  selectedView={selectedView}
                  currency={currency}
                  ganttTasks={ganttTasks}
                  handleFullScreen={this.handleFullScreen}
                  plannedVsActual={plannedVsActual}
                  taskData={this.props.tasks}
                  handleTaskbarsType={this.handleTaskbarsType}
                  projectState={quickFilters}
                  ganttExpand={this.state.fullScreen}
                  handleGanttView={this.handleGanttView}
                />
              </Grid>
            </Grid>
          </Grid>
        ) : !projectIdParam ? (
          <>
            <main
              className={classNames(classes.content, {
                [classes.contentShift]: openFilterSidebar,
              })}>
              <div className={classes.drawerHeader}>
                <Grid container classes={{ container: classes.taskDashboardCnt }}>
                  <Grid
                    container
                    direction="row"
                    justify="space-between"
                    alignItems="center"
                    classes={{ container: classes.taskDashboardHeader }}>
                    <div className="flex_center_start_row">
                      {isArchived ? (
                        <>
                          <LeftArrow
                            onClick={this.handleClearArchived}
                            className={classes.backArrowIcon}
                          />
                          <Typography variant="h1">
                            {
                              <FormattedMessage
                                id="common.archived.projects.label"
                                defaultMessage="Archived Projects"
                              />
                            }
                          </Typography>
                        </>
                      ) : <div
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        marginRight: "16px",
                      }}>
                      <Typography variant="h1" className={classes.listViewHeading}>
                        Projects
                      </Typography>
                    </div>}
                      {!isArchived ? (
                        <div className={classes.toggleContainer}>
                          <ToggleButtonGroup
                            value={alignment}
                            exclusive
                            onChange={this.handleAlignment}
                            classes={{ root: classes.toggleBtnGroup }}>
                            <ToggleButton
                              value="left"
                              onClick={this.renderListView}
                              classes={{
                                root: classes.toggleButton,
                                selected: classes.toggleButtonSelected,
                              }}>
                              <FormattedMessage id="common.list.label" defaultMessage="List" />
                            </ToggleButton>
                            <ToggleButton
                              value="center"
                              onClick={this.renderGridView}
                              classes={{
                                root: classes.toggleButton,
                                selected: classes.toggleButtonSelected,
                              }}>
                              <FormattedMessage id="common.grid.label" defaultMessage="Grid" />
                            </ToggleButton>
                            <ToggleButton
                              value="right"
                              onClick={this.renderCalendarView}
                              classes={{
                                root: classes.toggleButton,
                                selected: classes.toggleButtonSelected,
                              }}>
                              <FormattedMessage
                                id="common.calendar.label"
                                defaultMessage="Calendar"
                              />
                            </ToggleButton>
                          </ToggleButtonGroup>
                        </div>
                      ) : null}
                    </div>
                    {!isArchived ? (
                      <div className="flex_center_start_row">
                        <GroupingDropdown
                            id="projectGroupingDropdown"
                            view="project"
                            selectedGroup={selectedGroup}
                            data={groupingDropDownData}
                            onGroupSelect={this.onGroupSelect}
                            height={160}
                          />
                        {!calendarView &&
                        teamCanView(
                          "advanceSortAccess"
                        ) /** Advanced sorting only display in premium and bussiness plan */ && (
                            <SortingDropdown
                              data={this.state.sortingDropDownData}
                              sortingList={this.sortingProjectList}
                              getSortOrder={getSortOrder(workspaces, loggedInTeam, 1)}
                              height={125}
                            />
                          )}
                        {calendarView && !projectIdParam ? null : (
                          <FormControl className={classes.formControl}>
                            {/* <Select
                              multiple
                              value={quickFilters}
                              onChange={this.handleChange}
                              renderValue={(selected) => {
                                if (selected.length === 0) {
                                  return (
                                    <span
                                      style={{
                                        color: theme.palette.text.light,
                                      }}
                                    >
                                      Filter
                                    </span>
                                  );
                                }
                                return selected.join(", ");
                              }}
                              onClick={this.handleSelectClick}
                              input={
                                <OutlinedInput
                                  labelWidth={150}
                                  notched={false}
                                  inputRef={(node) => {
                                    this.anchorEl = node;
                                  }}
                                  classes={{
                                    root: classes.outlinedSelectCnt,
                                    notchedOutline: classes.outlinedSelect,
                                  }}
                                  id="select-multiple-checkbox"
                                />
                              }
                              MenuProps={MenuProps}
                              classes={{
                                root: classes.selectCnt,
                                select: classes.selectCmp,
                                selectMenu: classes.selectMenu,
                                icon: classes.selectIcon,
                              }}
                            > */}
                            <CustomButton
                              onClick={event => {
                                this.handleClick(event, "bottom-end");
                              }}
                              buttonRef={node => {
                                this.anchorEl = node;
                              }}
                              style={{
                                padding: customFilterSelect ? "4px 8px 4px 4px" : "3px 8px 3px 4px",
                                borderRadius: "4px",
                                display: "flex",
                                justifyContent: "space-between",
                                minWidth: "auto",
                                whiteSpace: "nowrap",
                              }}
                              btnType={customFilterSelect ? "lightBlue" : "white"}
                              variant="contained">
                              <SvgIcon
                                viewBox="0 0 24 24"
                                className={
                                  customFilterSelect
                                    ? classes.qckFfilterIconSelected
                                    : classes.qckFilterIconSvg
                                }>
                                <QuickFilterIcon />
                              </SvgIcon>
                              <span
                                className={
                                  customFilterSelect
                                    ? classes.qckFilterLblSelected
                                    : classes.qckFilterLbl
                                }>
                                <FormattedMessage id="common.show.label" defaultMessage="Show" /> :{" "}
                              </span>
                              <span className={classes.checkname}>{selectedFiltersLbl}</span>
                            </CustomButton>
                            <PlainMenu
                              open={this.state.open}
                              closeAction={this.handleClose}
                              placement="bottom-start"
                              anchorRef={this.anchorEl}
                              style={{ width: 280 }}
                              offset="0 15px">
                              <MenuList disablePadding>
                                <MenuItem
                                  className={`${classes.statusMenuItemCnt} ${
                                    !customFilterSelect ? classes.selectedValue : ""
                                  }`}
                                  classes={{
                                    root: classes.statusMenuItemCnt,
                                    selected: classes.statusMenuItemSelected,
                                  }}
                                  value="Show All"
                                  onClick={this.handleSelectClick.bind(this, "Show All")}>
                                  <ListItemText
                                    primary={
                                      <FormattedMessage
                                        id="common.show.all"
                                        defaultMessage="Show All"
                                      />
                                    }
                                    classes={{ primary: classes.plainItemText }}
                                  />
                                </MenuItem>
                                <Divider />
                                <MenuItem
                                  value={"Archived"}
                                  classes={{ root: classes.highlightItem }}
                                  onClick={this.handleSelectClick.bind(this, "Archived")}>
                                  <ArchivedIcon
                                    htmlColor={theme.palette.secondary.light}
                                    classes={{
                                      root: classes.selectHighlightItemIcon,
                                    }}
                                  />
                                  <FormattedMessage
                                    id="common.archived.archived"
                                    defaultMessage="Archived"
                                  />
                                </MenuItem>
                              </MenuList>
                            </PlainMenu>
                            {/* </Select> */}
                          </FormControl>
                        )}
                        {calendarView ? (
                          <PlannedVsActualDropDown
                            handleTaskbarsType={this.handleProjectsDateType}
                            calenderDateType={calenderDateType}
                          />
                        ) : null}
                        <ImportExportDD
                          handleExportType={this.handleExportType}
                          filterList={quickFilters}
                          multipleFilters={multipleFilters}
                          handleRefresh={this.handleRefresh}
                          ImportExportType={"project"}
                          isArchived={this.state.quickFilters.indexOf("Archived") >= 0}
                        />
                      </div>
                    ) : null}
                  </Grid>
                  <Grid container classes={{ container: classes.dashboardContentCnt }}>
                    {calendarView && !projectIdParam ? (
                      <Grid
                        item
                        classes={{ item: classes.taskCalendarCnt }}
                        style={{ height: calculateContentHeight() }}>
                        <ProjectCalendar
                          taskState={quickFilters}
                          calenderDateType={calenderDateType}
                          multipleFilters={this.state.multipleFilters}
                          searchFilterApplied={this.searchFilterApplied}
                        />
                      </Grid>
                    ) : gridView && !projectIdParam ? (
                      <div
                        onScroll={this.handleScrollDown}
                        className={classes.taskGridCnt}
                        style={{ height: calculateContentHeight() }}>
                        {this.props.projects.length > 0 ? (
                          <TaskGridItem
                            projectState={quickFilters}
                            filteredProjects={this.filteredProjects}
                            isArchivedSelected={isArchived}
                            // projectClick={this.handleProjectListClick}
                            openProjectSetting={this.openProjectSetting}
                            handleExportType={this.handleExportType}
                            multipleFilters={this.state.multipleFilters}
                            searchFilterApplied={this.searchFilterApplied}
                            returnCount={this.returnCount}
                            handleChangeState={this.handleChangeState}
                            isChanged={this.state.isChanged}
                            projectClick={this.handleProjectListClick}
                            showRecords={showRecords}
                            resetCount={this.resetCount}
                            sortObj={this.state.sortObj}
                            projects={filteredProjects}
                          />
                        ) : projectPer.createProject.cando ? (
                          <EmptyState
                            screenType="project"
                            heading="Create your first project"
                            message='You do not have any projects yet. Press "Alt + P" or click on button below.'
                            button={true}
                          />
                        ) : (
                          <EmptyState
                            screenType="project"
                            heading="You do not have any projects yet."
                            message="Request your team member(s) to assign you a project."
                            button={false}
                          />
                        )}
                      </div>
                    ) : !projectIdParam ? (
                      <Grid item classes={{ item: classes.taskListCnt }}>
                        <ProjectList
                          projectState={quickFilters}
                          filteredProjects={filteredProjects}
                          filterProject={this.filterProject}
                          isArchivedSelected={isArchived}
                          projectClick={this.handleProjectListClick}
                          openProjectSetting={this.openProjectSetting}
                          handleExportType={this.handleExportType}
                          multipleFilters={this.state.multipleFilters}
                          searchFilterApplied={this.searchFilterApplied}
                          returnCount={this.returnCount}
                          handleChangeState={this.handleChangeState}
                          isChanged={this.state.isChanged}
                          sortObj={this.state.sortObj}
                          selectedGroup={selectedGroup}
                        />
                      </Grid>
                    ) : null}
                  </Grid>
                </Grid>
              </div>
            </main>
            <ProjectFilter
              open={openFilterSidebar}
              closeAction={this.handleDrawerClose}
              searchFilter={this.searchFilter}
              clearFilter={this.clearFilter}
              clearlastState={this.state.clearlastState}
            />
            <Footer
              quote={quote}
              total={this.state.filteredProjects.length}
              display={true}
              type="Project"
              open={this.props.open}
            />
          </>
        ) : null}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    loggedInTeam: state.profile.data.loggedInTeam,
    projects: state.projects.data || [],
    tasks: state.tasks.data || [],
    ganttTasks: state.ganttTasks,
    permissions: state.workspacePermissions.data.project,
    profileState: state.profile.data,
    workspaces: state.profile.data.workspace,
    projectPer: state.workspacePermissions.data.project,
    projectDetailsDialog: state.projectDetailsDialog,
    itemOrderState: state.itemOrder.data,

  };
};

export default compose(
  withRouter,
  withSnackbar,
  injectIntl,
  withStyles(combineStyles(dashboardStyles, selectStyles), {
    withTheme: true,
  }),
  connect(mapStateToProps, {
    getGanttTasks,
    showLoading,
    hideLoading,
    FetchWorkspaceInfo,
    saveSortingProjectList,
    projectDetails,
    getProjects
  })
)(ProjectDashboard);
