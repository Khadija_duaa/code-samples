import React, { Component, Fragment, useEffect, useState } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect, useDispatch } from "react-redux";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import dashboardStyles from "./styles";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import selectStyles from "../../assets/jss/components/select";
import combineStyles from "../../utils/mergeStyles";
import ArchivedIcon from "@material-ui/icons/Archive";
import loadable from '@loadable/component'
import Gantt from "../../assets/images/unplanned/Gantt.png";
const ProjectList = loadable(() => import("./List/ProjectList.view"))
const ProjectCalendar = loadable(() => import("./Calendar/Calendar"))
const ProjectGridItem = loadable(() => import("./Grid/Item"))

import ImportExportDD from "../../components/Dropdown/ImportExportDD";
import { withSnackbar } from "notistack";
import { getGanttTasks } from "../../redux/actions/gantt";
import { showLoading, hideLoading } from "react-redux-loading-bar";
import CustomIconButton from "../../components/Buttons/IconButton";
import IconRefresh from "../../components/Icons/IconRefresh";
import queryString from "query-string";
import ListLoader from "../../components/ContentLoader/List";
import { getSortOrder, sortListData } from "../../helper/sortListData";
import {
  saveSortingProjectList,
  projectDetails,
  getProjects,
} from "../../redux/actions/projects";
import { getTasks } from "../../redux/actions/tasks";
import { getIssues } from "../../redux/actions/issues";
import {
  FetchWorkspaceInfo,
} from "../../redux/actions/workspace";
import {
  updateQuickFilter,
  removeProjectQuickFilter,
  getArchivedData,
} from "../../redux/actions/projects";
import { calculateContentHeight, isScrollBottom } from "../../utils/common";
import SvgIcon from "@material-ui/core/SvgIcon";
import PlannedVsActualDropDown from "./plannedVsActualDropDown";
import CustomButton from "../../components/Buttons/CustomButton";
import { GetPermission } from "../../components/permissions";
import DefaultTextField from "../../components/Form/TextField";
import { FormattedMessage, injectIntl } from "react-intl";
import isEmpty from "lodash/isEmpty";
import ColumnSelectionDropdown from "../../components/Dropdown/SelectedItemsDropDown";
import { grid } from "../../components/CustomTable2/gridInstance";
import debounce from "lodash/debounce";
import { doesFilterPass } from "./List/ProjectFilter/projectFilter.utils";
const ProjectGantt = loadable(() => import("./Gantt/GanttChart"))

import searchQuery from "../../components/CustomTable2/ColumnSettingDropdown/searchQuery";
import { teamCanView } from '../../components/PlanPermission/PlanPermission';
import UnPlanned from '../billing/UnPlanned/UnPlanned';
import { TRIALPERIOD } from '../../components/constants/planConstant';

let screenfull = require("screenfull");
let increment = 20;
const ProjectDashboard = (props) => {
  const {
    classes,
    enqueueSnackbar,
    theme,
    history,
    profileState,
    permissions,
    projects,
    quickFilters,
    getGanttTasks,
    workspaces,
    tasks,
    location,
    loggedInTeam,
    intl,
    hideLoading,
    projectDetailsDialog: { docked, fullView, dialog, projectDetails },
  } = props;
  const [view, setView] = useState("list");
  const [multipleFilters, setMultipleFilters] = useState([]);
  // const [quickFilters, setQuickFilters] = useState([]);
  const [filteredProjects, setFilteredProjects] = useState([]);
  const [archiveBtnQuery, setArchiveBtnQuery] = useState([]);
  const [isChanged, setIsChanged] = useState(false);
  const [showRecords, setShowRecords] = useState("");
  const [calenderDateType, setCalenderDateType] = useState("Planned Start/End");
  const [projectId, setProjectId] = useState("");
  const [currentProject, setCurrentProject] = useState({});
  const [total, setTotal] = useState(0);
  const [loadMore, setLoadMore] = useState(0);
  const [sortObj, setSortObj] = useState({});
  const [ganttState, setGanttState] = useState({});
  const [selectedView, setSelectedView] = useState('');
  const [plannedVsActual, setPlannedVsActual] = useState('Planned');
  const [refreshBtnQuery, setRefreshBtnQuery] = useState("");
  const [isLoading, setIsLoading] = useState(true);
  const [fullScreen, setFullScreen] = useState(false);

  const dispatch = useDispatch();
  const projectAccess = teamCanView("projectAccess");
  //Set active Project view

  const queryProjectUrl = () => {
    const searchQuery = location.search;
    if (searchQuery) {
      const objectType = location.pathname.split("/")[1]; // Parsing the url and extracting the object type using query string
      if (objectType == "projects") {
        let projectIdInUrl = queryString.parse(location.search).projectId;
        if (
          projectIdInUrl &&
          projectIdInUrl !== "" &&
          (projectDetails == undefined || isEmpty(projectDetails))
        ) {
          let validateProject = projects.findIndex(project => {
            // Checking if the issue id is valid or not
            return projectIdInUrl == project.projectId;
          });
          if (validateProject > -1) {
            let currentProject = projects.find(p => p.projectId == projectIdInUrl) || false;
            history.push("/projects");

            if (currentProject) {
              props.projectDetails({
                projectDetails: currentProject,
                fullView: fullView ? true : false,
                docked: docked ? true : false,
                dialog: true,
              });
            }
          } else {
            // this.props.showSnackBar("Oops! Item not found", 'info');
            //In case the issue id in the url is wrong user is redirected to issues Page
            history.push("/projects");
          }
        }
      }
    }
  };

  // grid actions
  const getSortValues = () => {
    if (loggedInTeam) {
      let { projectColumn, projectDirection } = getSortOrder(workspaces, loggedInTeam, 3);

      if (projectColumn && projectDirection) {
        setSortObj({
          column: projectColumn,
          direction: projectDirection,
          cancelSort: "",
        });
      } else {
        setSortObj({ column: "", direction: "", cancelSort: "NONE" });
      }
    }
  };

  useEffect(() => {
    if (location.pathname == "/gantt") {
      redirectToGantt();
    }
    queryProjectUrl()
    //Clear all filters from redux if component is unmounted
    return (() => {
      if (searchQuery.quickFilters['Archived']) {
        removeProjectQuickFilter('Archived', dispatch);
      }
    })
  }, [location.search])

  useEffect(() => {
    if (projectAccess && location.pathname != "/gantt") {
      getProjects(
        null,
        dispatch,
        //success
        () => {
          setIsLoading(false);
          setFilteredProjects();
        },
        () => { }
      );
      getSortValues();
    }
  }, [projectAccess]);

  if (!projectAccess) {
    return (
      <div className={classes.unplannedMain}>
        <div className={classes.unplannedCnt}>
          <UnPlanned
            feature="premium"
            titleTxt={
              <FormattedMessage
                id="common.discovered-dialog.premium-title"
                defaultMessage="Wow! You've discovered a Premium feature!"
              />
            }
            boldText={intl.formatMessage({
              id: "project.label",
              defaultMessage: "Projects",
            })}
            descriptionTxt={
              <FormattedMessage
                id="common.discovered-dialog.list.project.label"
                defaultMessage={
                  "is available on our Premium Plan. Upgrade now or start a {TRIALPERIOD}-day free trial to find out all nTask Premium features."
                }
                values={{ TRIALPERIOD: TRIALPERIOD }}
              />
            }
            showBodyImg={true}
            showDescription={true}
            imgUrl={Gantt}
          />
        </div>
      </div>
    );
  }

  const handleSetView = (event, option) => {
    setView(option);
  };
  const handleTaskbarsType = (event, type) => {
    setPlannedVsActual(type)
  };
  const handleGanttView = (event, view) => {
    if (selectedView != view) {
      setSelectedView(view)
    } else {
      setSelectedView('')
    }
  };
  //column hide callback
  const onColumnHide = obj => {
    if (grid.grid) {
      grid.grid.columnModel.applyColumnState({
        state: [
          {
            colId: obj.id,
            hide: obj.hide,
            rowGroup: obj.rowGroup,
          },
        ],
      });
    }
  };
  const handleFullScreen = () => {
    setFullScreen(prev => !prev)
    if (!fullScreen) {
      let elem = document.getElementById("mainContentCnt");
      if (elem.requestFullscreen) {
        elem.requestFullscreen();
      } else if (elem.mozRequestFullScreen) {
        /* Firefox */
        elem.mozRequestFullScreen();
      } else if (elem.webkitRequestFullscreen) {
        /* Chrome, Safari and Opera */
        elem.webkitRequestFullscreen();
      } else if (elem.msRequestFullscreen) {
        /* IE/Edge */
        elem.msRequestFullscreen();
      }
      // document.documentElement.requestFullscreen();
      // document.querySelector("#mainContentCnt").style.marginTop = 0;
      // document.querySelector("#sidebar").style.display = "none";
      // document.querySelector("#header").style.display = "none";
      // document.querySelector("#footer").style.display = "none";
    } else {
      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.mozCancelFullScreen) {
        /* Firefox */
        document.mozCancelFullScreen();
      } else if (document.webkitExitFullscreen) {
        /* Chrome, Safari and Opera */
        document.webkitExitFullscreen();
      } else if (document.msExitFullscreen) {
        /* IE/Edge */
        document.msExitFullscreen();
      }
      // document.exitFullscreen();
    }

    screenfull.onchange(() => {
      if (!screenfull.isFullscreen) {
        setFullScreen(false)
        document.querySelector("#mainContentCnt").style.marginTop = "";
        document.querySelector("#sidebar").style.display = "";
        document.querySelector("#header").style.display = "";
        // document.querySelector("#footer").style.display = "";
      }
    });
  };

  const handleExportType = (snackBarMessage, type) => {
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      },
    );
  };

  const handleSelectClick = (value) => {
    const filterObj = { [value]: { type: value, selectedValue: [] } };
    if (quickFilters[value]) {
      removeProjectQuickFilter(value, dispatch);
      setTimeout(() => {
        !isEmpty(grid.grid) && grid.grid.redrawRows();
      }, 0);
      return;
    }
    if (value == "Archived") {
      setArchiveBtnQuery("progress");
      getArchivedData(
        1,
        resp => {
          // setQuickFilters([value])
          setArchiveBtnQuery("");
          updateQuickFilter(filterObj, dispatch);
          setTimeout(() => {
            !isEmpty(grid.grid) && grid.grid.redrawRows();
          }, 0);
        },
        dispatch,
      );
      return;
    }
    updateQuickFilter(filterObj, dispatch);
    // setQuickFilters({ quickFilters: items || [] });
  };

  const throttleHandleSearch = debounce(data => {
    grid.grid && grid.grid.setQuickFilter(data);
  }, 1000);

  //handle task search
  const handleSearch = e => {
    throttleHandleSearch(e.target.value);
  };
  const handleProjectListClick = (project) => {
    if (
      GetPermission.canDo(project.isOwner, permissions, "accessGantt") ||
      (project.projectManager && project.projectManager.indexOf(profileState.userId) > -1)
    ) {
      // showLoading();
      // getGanttTasks(project.projectId, response => {
      //   const { currency } = response.data;
      //   setGanttState(prev => ({
      //     ...prev,
      //     projectGantt: true,
      //     selectedProject: projectId,
      //     currency: currency ? currency : "USD",
      //   }));
      // const { currency } = response.data;

      // selectedProject(project.projectId)
      // this.setState({
      //   // projectGantt: true,
      //   selectedProject: project.projectId,
      //   currency: currency ? currency : "USD",
      // });
      // this.props.history.push({
      //   to: "/gantt",
      //   search: `?projectId=${project.projectId}`,
      // });
      history.push(`/gantt?projectId=${project.projectId}`);
      // hideLoading();
      // });
    }
  };
  // calander actions

  const handleProjectsDateType = (event, type) => {
    setCalenderDateType(type);
    // this.setState({ calenderDateType: type });
  };

  const searchFilterApplied = () => {
    setMultipleFilters("");
  };
  const handleChangeState = () => {
    setIsChanged(false);
  };
  const handleFilteredProjects = () => {
    /** function for sorting if there is already saved value of sorted column and sort order */
    if (sortObj && sortObj.column && sortObj.direction) {
      let data = sortListData(filteredProjects, sortObj.column, sortObj.direction);
      return data;
    } else if (sortObj && sortObj.cancelSort == "NONE") {
      return projects;
    } else {
      return projects;
    }
  };
  const openProjectSetting = (event, projectId, projectName, project) => {
    setProjectId(projectId);
    setCurrentProject(project);
    // this.setState({ projectId, projectName, currentProject: project });
  };
  const resetCount = () => {
    setShowRecords(increment);
    setLoadMore(increment);
    // this.setState({ showRecords: increment, loadMore: increment });
  };
  const returnCount = (total, loadMore) => {
    setTotal(total);
    setLoadMore(loadMore);
    // this.setState({ total, loadMore });
  };

  const handleRefreshList = () => {
    setRefreshBtnQuery("progress");
    setIsLoading(true);
    getProjects(
      null,
      dispatch,
      () => {
        setRefreshBtnQuery("");
        setIsLoading(false);
        handleExportType("Grid Refreshed Successfully!", "success");
      },

      () => {
        setRefreshBtnQuery("");
        setIsLoading(false);
        handleExportType("Oops! Server throws Error.", "error");
      },
    );
  };

  const handleRefresh = () => {
    setRefreshBtnQuery("progress");
    setIsLoading(true);
    getProjects(null, dispatch, succ => {
      getTasks(null, dispatch, succ => {
        getIssues(null, dispatch,
          //success
          () => {
            setRefreshBtnQuery("");
            setIsLoading(false);
          },
          () => {
            setRefreshBtnQuery("");
            setIsLoading(false);
            handleExportType("Oops! Server throws Error.", "error");
          }
        );
      })
    })
  }
  const redirectToGantt = () => {
    let projectId = queryString.parse(location.search).projectId;

    if (projectId) {
      setGanttState(prev => ({ ...prev, projectGantt: false }));
      // this.props.showLoading();
      props.getGanttTasks(projectId, response => {
        const { currency } = response.data;
        setGanttState(prev => ({
          ...prev,
          projectGantt: true,
          selectedProject: projectId,
          currency: currency ? currency : "USD",
        }));
        hideLoading();
      });
    }
  };
  const isArchived = quickFilters.length && quickFilters[quickFilters.length - 1] === "Archived";
  let projectIdParam = queryString.parse(location.search).projectId;


  return (
    <div>
      {projectIdParam && ganttState.projectGantt ? (
        <Grid container classes={{ container: classes.taskDashboardCnt }}>
          <Grid container classes={{ container: classes.dashboardContentCnt }}>
            <Grid item classes={{ item: classes.taskGanttCnt }}>
              <ProjectGantt
                projectId={ganttState.selectedProject}
                selectedView={selectedView}
                currency={ganttState.currency}
                ganttTasks={props.ganttTasks}
                handleFullScreen={handleFullScreen}
                plannedVsActual={plannedVsActual}
                taskData={tasks}
                handleTaskbarsType={handleTaskbarsType}
                projectState={projects}
                ganttExpand={fullScreen}
                handleGanttView={handleGanttView}
              />
            </Grid>
          </Grid>
        </Grid>
      ) : (
        <div>
          <div className={classes.projectDashboardHeader}>
            <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="center"
              classes={{ container: classes.taskDashboardHeader }}>
              <div className="flex_center_start_row">
                <div className={classes.projectHeaderLeftCnt}>
                  <Typography variant="h1" className={classes.listViewHeading} style={{fontSize: "18px"}}>
                    Projects
                  </Typography>
                  <span className={classes.count}>
                    {filteredProjects && filteredProjects.length ? filteredProjects.length : projects.filter(p => doesFilterPass({ data: p })).length}
                  </span>
                  <div className={classes.toggleContainer}>
                    <ToggleButtonGroup
                      value={view}
                      exclusive
                      // onChange={handleAlignment}
                      classes={{ root: classes.toggleBtnGroup, groupedHorizontal: classes.groupedHorizontal }}>
                      <ToggleButton
                        value="list"
                        onClick={(e) => handleSetView(e, "list")}
                        classes={{
                          root: classes.toggleButton,
                          selected: classes.toggleButtonSelected,
                        }}>
                        <FormattedMessage id="common.list.label" defaultMessage="List" />
                      </ToggleButton>
                      <ToggleButton
                        value="grid"
                        onClick={(e) => handleSetView(e, "grid")}
                        classes={{
                          root: classes.toggleButton,
                          selected: classes.toggleButtonSelected,
                        }}>
                        <FormattedMessage id="common.grid.label" defaultMessage="Grid" />
                      </ToggleButton>
                      <ToggleButton
                        value="calendar"
                        onClick={(e) => handleSetView(e, "calendar")}
                        classes={{
                          root: classes.toggleButton,
                          selected: classes.toggleButtonSelected,
                        }}>
                        <FormattedMessage
                          id="common.calendar.label"
                          defaultMessage="Calendar"
                        />
                      </ToggleButton>
                    </ToggleButtonGroup>
                  </div>
                </div>
              </div>
              <div className="flex_center_start_row">
                {/* search bar */}
                {view == "list" ?
                  <DefaultTextField
                    fullWidth={false}
                    // errorState={forgotEmailError}
                    error={false}
                    // errorMessage={forgotEmailMessage}
                    formControlStyles={{ width: 250, marginBottom: 0, marginRight: 10 }}
                    defaultProps={{
                      id: "projectListSearch",
                      onChange: handleSearch,
                      placeholder: "Search Project",
                      autoFocus: true,
                      inputProps: { maxLength: 150, style: { padding: "8px 14px" } },
                    }}
                  /> : ""}
                {/* refresh button */}
                {view !== "calendar" ?
                  <CustomButton
                    onClick={handleRefreshList}
                    query={refreshBtnQuery}
                    style={{
                      // padding: customFilterSelect ? "4px 8px 4px 4px" : "3px 8px 3px 4px",
                      padding: "9px 8px",
                      borderRadius: "4px",
                      display: "flex",
                      justifyContent: "space-between",
                      minWidth: "auto",
                      whiteSpace: "nowrap",
                      marginRight: 10,
                      height: 32
                    }}
                    btnType={"white"}
                    variant="contained">
                    <SvgIcon viewBox="0 0 12 12.015" className={classes.qckFfilterIconRefresh}>
                      <IconRefresh />
                    </SvgIcon>
                  </CustomButton> : ""}
                {/* archive button */}
                {view !== "calendar" ?
                  <CustomButton
                    onClick={event => {
                      handleSelectClick("Archived");
                    }}
                    style={{
                      padding: "3px 8px 3px 4px",
                      borderRadius: "4px",
                      height: 32,
                    }}
                    query={archiveBtnQuery}
                    disabled={archiveBtnQuery == "progress"}
                    // btnType={"white"}
                    btnType={!isEmpty(quickFilters) ? "lightBlue" : "white"}
                    variant="contained">

                    {!quickFilters["Archived"]}
                    <SvgIcon
                      classes={{root: classes.selectHighlightItemIcon}}
                      viewBox="0 0 24 24"
                      // className={classes.qckFilterIconSvg}
                      className={
                        !isEmpty(quickFilters)
                          ? classes.qckFfilterIconSelected
                          : classes.qckFilterIconSvg
                      }>
                      <ArchivedIcon
                        htmlColor={theme.palette.secondary.light}
                      />
                    </SvgIcon>
                    <span
                      className={
                        !isEmpty(quickFilters)
                          ? classes.qckFilterLblSelected
                          : classes.qckFilterLbl
                      }>
                      Archived
                    </span>
                  </CustomButton> : ""}
                <ImportExportDD
                  handleExportType={handleExportType}
                  filterList={quickFilters}
                  multipleFilters={multipleFilters}
                  id="projectImportExport"
                  handleRefresh={handleRefresh}
                  ImportExportType={"project"}
                  isArchived={quickFilters.length && quickFilters.indexOf("Archived") >= 0}
                />
                {view == "calendar" ? (
                  <PlannedVsActualDropDown
                    handleTaskbarsType={handleProjectsDateType}
                    calenderDateType={calenderDateType}
                  />
                ) : null}
                {view == "list" ?
                  <ColumnSelectionDropdown
                    feature={"project"}
                    onColumnHide={onColumnHide}
                    hideColumns={["matrix"]}
                    btnProps={{
                      style: {
                        border: "1px solid #dddddd",
                        padding: "5px 10px",
                        borderRadius: 4,
                        marginLeft: 10,
                      },
                    }}
                  /> : ""}
              </div>
            </Grid>
          </div>
          {/* {view} */}
          {view == "list" && location.pathname != "/gantt" ?
            <>
              {isLoading ? <ListLoader style={{ paddingLeft: 32 }} /> : <ProjectList
                projectState={projects}
                projectClick={handleProjectListClick}
              />}
            </>
            : view == "grid" ?
              <div
                // onScroll={this.handleScrollDown}
                className={classes.taskGridCnt}
                style={{ height: calculateContentHeight() }}>
                <ProjectGridItem
                  projectState={quickFilters}
                  returnCount={returnCount}
                  handleChangeState={handleChangeState}
                  isChanged={isChanged}
                  showRecords={showRecords}
                  resetCount={resetCount}
                  isArchivedSelected={isArchived}
                  sortObj={sortObj}
                  projectClick={handleProjectListClick}
                // filteredProjects={handleFilteredProjects}
                // openProjectSetting={openProjectSetting}
                // handleExportType={handleExportType}
                // multipleFilters={multipleFilters}
                // searchFilterApplied={searchFilterApplied}
                />
              </div>
              : view == "calendar" ?
                <Grid container classes={{ container: classes.dashboardContentCnt }}>
                  <Grid
                    item
                    classes={{ item: classes.taskCalendarCnt }}
                    style={{ height: "calc(100vh - 160px)" }}>
                    <ProjectCalendar
                      taskState={quickFilters}
                      calenderDateType={calenderDateType}
                      multipleFilters={multipleFilters}
                      searchFilterApplied={searchFilterApplied}
                      customCalendar={workspaces.find(w => w.teamId === loggedInTeam).calendar}
                    />
                  </Grid>
                </Grid> : ""}
        </div>)}
    </div>
  );
};

const mapStateToProps = (state, ownProps) => {
  return {
    loggedInTeam: state.profile.data.loggedInTeam,
    projects: state.projects.data || [],
    tasks: state.tasks.data || [],
    ganttTasks: state.ganttTasks,
    permissions: state.workspacePermissions.data.project,
    profileState: state.profile.data,
    workspaces: state.profile.data.workspace,
    quickFilters: state.projects.quickFilters || {},
    projectPer: state.workspacePermissions.data.project,
    projectDetailsDialog: state.projectDetailsDialog,
    itemOrderState: state.itemOrder.data,

  };
};

export default compose(
  withRouter,
  withSnackbar,
  injectIntl,
  withStyles(combineStyles(dashboardStyles, selectStyles), {
    withTheme: true,
  }),
  connect(mapStateToProps, {
    getGanttTasks,
    showLoading,
    hideLoading,
    FetchWorkspaceInfo,
    saveSortingProjectList,
    projectDetails,
    getProjects,
    updateQuickFilter,
    removeProjectQuickFilter,
  }),
)(ProjectDashboard);
