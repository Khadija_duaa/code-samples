import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import menuStyles from "../../assets/jss/components/menu";
import CustomButton from "../../components/Buttons/CustomButton"
import SelectionMenu from "../../components/Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import combineStyles from "../../utils/mergeStyles";
import DownArrow from "@material-ui/icons/ArrowDropDown";
import Typography from "@material-ui/core/Typography";
import { FormattedMessage } from "react-intl";

const items = ["Planned Start/End", "Actual Start/End"];

class PlannedVsActualDropDown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      pickerOpen: false,
      pickerPlacement: "",
      archiveFlag: false,
      unArchiveFlag: false,
      deleteFlag: false,
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handlePickerClose = this.handlePickerClose.bind(this);
  }
  handleDialogOpen = () => {
    this.setState({ renameRoleDialogOpen: true });
  };
  handleDialogClose = () => {
    this.setState({ renameRoleDialogOpen: false });
  };
  handleClose(event) {
    this.setState({ open: false, pickerOpen: false });
  }
  handlePickerClose(event) {
    this.setState({ pickerOpen: false });
  }
  componentWillUnmount() {
    this.props.handleTaskbarsType(null, "Planned Start/End")
  }
  handleClick(event, placement) {
    const { currentTarget } = event;
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement
    }));
  }
  getTranslatedText = (name) => {
    switch (name) {
      case "Creation Date":
        name = "common.select.calender-view.dropdown-label.creation-date";
        break;
      case "Planned Start/End":
        name = "common.select.calender-view.dropdown-label.plan-start-end";
        break;
      case "Actual Start/End":
        name = "common.select.calender-view.dropdown-label.actual-start-end";
        break;
    }
    return name;
  }
  render() {
    const { classes, theme, calenderDateType, handleTaskbarsType } = this.props;

    const { open, placement } = this.state;
    return (
      <ClickAwayListener onClickAway={this.handleClose}>
        <div>
          <CustomButton
            onClick={event => {
              this.handleClick(event, "bottom-end");
            }}
            buttonRef={node => {
              this.anchorEl = node;
            }}
            style={{
              minWidth: 'max-content',
              padding: "3px 8px 3px 14px",
              display: "flex",
              justifyContent: "space-between",
            }}
            btnType="white"
            variant="contained"
          >  <span><FormattedMessage id="common.select.calender-view.label" defaultMessage="View:" /> <FormattedMessage id={this.getTranslatedText(calenderDateType)} defaultMessage={calenderDateType} /></span>
            <DownArrow
              htmlColor={theme.palette.secondary.medDark}
              className={classes.dropdownArrow}
            />
          </CustomButton>

          <SelectionMenu
            open={open}
            closeAction={this.handleClose}
            placement={placement}
            style={{ width: 215 }}
            anchorRef={this.anchorEl}
            list={
              <List disablePadding={true}>
                <ListItem disableRipple
                  className={classes.headingItem}>
                  <ListItemText
                    primary={<FormattedMessage id="common.select.calender-view.placeholder" defaultMessage="Select View" />}
                    classes={{ primary: classes.headingText }}
                  />
                </ListItem>
                {
                  items.map((item, index) => {

                    return (
                      <ListItem
                        key={index}
                        button
                        disableRipple
                        selected={calenderDateType == item}
                        classes={{ selected: classes.statusMenuItemSelectedMUI4 }}
                        onClick={event => handleTaskbarsType(event, item)}
                      >
                        <ListItemText
                          primary={<FormattedMessage id={this.getTranslatedText(item)} defaultMessage={item} />}
                          classes={{
                            primary: classes.statusItemText
                          }}
                        />
                      </ListItem>
                    )
                  })
                }
              </List>
            }
          />
        </div>
      </ClickAwayListener >
    );
  }
}

export default withStyles(combineStyles(menuStyles), {
  withTheme: true
})(PlannedVsActualDropDown);
