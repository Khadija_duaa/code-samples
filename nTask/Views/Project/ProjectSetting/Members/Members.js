import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import projectSettingStyles from "../styles";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import CustomAvatar from "../../../../components/Avatar/Avatar";
import DefaultTextField from "../../../../components/Form/TextField";
import HelpIcon from "@material-ui/icons/HelpOutline";
import { connect } from "react-redux";
import { compose } from "redux";
import { getUser } from "../../../../utils/common";
import AddMemberDropdown from "../AddMemberDropdown";
import { Scrollbars } from "react-custom-scrollbars";
import { isUserPresent } from "../../../../redux/actions/authentication";

class ProjectMembers extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  handleCheckboxChange = name => event => {
    this.setState({ [name]: event.target.checked });
  };

  inviteNewMember = email => {
    const success = response => {

      this.props.AssignToProject("resources", {
        userId: response.data.userId,
        isProjectManager: false,
        isBillable: false,
        isAssignee: false,
        hourlyRate: 0
      });

    }
    const failure = response => {

    }
    this.props.isUserPresent({ email: email }, success, failure)
  }
  render() {
    const {
      classes,
      theme,
      psdata,
      updateResource,
      selectAllAction,
      AssignToProject
    } = this.props;
    const { resources, hourlyRateType, billingMethod, projectId, currency } = psdata;
    const members = this.props.profileState.data.member.allMembers;

    return (
      <>
        <Grid item xs={12} className={classes.projectTasksHeadingCnt}>
          <Typography variant="h2" className={classes.projectTasksHeading}>
            Resources
          </Typography>
          <div className="flex_center_start_row">
            <div className="flex_center_center_col" style={{ width: 110 }}>
              <HelpIcon
                htmlColor={theme.palette.secondary.light}
                className={classes.helpIcon}
              />
              <Typography variant="body2">Project Manager</Typography>
              <span
                className={classes.selectAllLink}
                onClick={event => {
                  selectAllAction("resources", "isProjectManager");
                }}
              >
                Select All
              </span>
            </div>
            {hourlyRateType == "Resource" &&
              billingMethod !== 0 &&
              billingMethod !== 2 ? (
                <div className="flex_center_center_col" style={{ width: 100 }}>
                  <HelpIcon
                    htmlColor={theme.palette.secondary.light}
                    className={classes.helpIcon}
                  />
                  <Typography variant="body2">Billable</Typography>
                  <span
                    className={classes.selectAllLink}
                    onClick={event => {
                      selectAllAction("resources", "isBillable");
                    }}
                  >
                    Select All
                </span>
                </div>
              ) : null}
            {hourlyRateType == "Resource" &&
              billingMethod !== 0 &&
              billingMethod !== 2 ? (
                <div className="flex_center_center_col" style={{ width: 100 }}>
                  <HelpIcon
                    htmlColor={theme.palette.secondary.light}
                    className={classes.helpIcon}
                  />
                  <Typography variant="body2">Hourly Rate</Typography>
                </div>
              ) : null}
          </div>
        </Grid>
        <Grid item xs={12} className={classes.projectTaskListCnt}>
          <AddMemberDropdown
            AssignToProject={AssignToProject}
            psdata={psdata}
            inviteNewMember={this.inviteNewMember}
          />
          <Scrollbars autoHide style={{ height: 230 }}>
            <ul className={classes.projectTaskList}>
              {resources.map(resource => {
                const user = getUser(resource.userId, members);
                return (
                  <li
                    className={classes.projectTaskListItem}
                    key={resource.userId}
                  >
                    {
                      (user) ?
                        <div className="flex_center_start_row">
                          <CustomAvatar
                            otherMember={{
                              imageUrl: user.imageUrl,
                              fullName: user.fullName,
                              lastName: '',
                              email: user.email,
                              isOnline: user.isOnline,
                              isOwner: user.isOwner
                            }}
                            size="small"
                          />
                          <Typography variant="body2">
                            {
                              user.fullName
                            }
                          </Typography>
                        </div>
                        : null
                    }
                    <div className="flex_center_start_row">
                      <div
                        className="flex_center_center_row"
                        style={{ width: 110 }}
                      >
                        <FormControlLabel
                          style={{ margin: 0 }}
                          classes={{
                            label: classes.checkboxLabel
                          }}
                          control={
                            <Checkbox
                              checked={resource.isProjectManager}
                              onChange={event =>
                                updateResource(
                                  resource.userId,
                                  "isProjectManager",
                                  event.currentTarget.checked
                                )
                              }
                              value="projectManager"
                              classes={{
                                root: classes.checkbox,
                                checked: classes.checked
                              }}
                            />
                          }
                        />
                      </div>
                      {hourlyRateType == "Resource" &&
                        billingMethod !== 0 &&
                        billingMethod !== 2 ? (
                          <div
                            className="flex_center_center_row"
                            style={{ width: 100 }}
                          >
                            <FormControlLabel
                              classes={{
                                label: classes.checkboxLabel
                              }}
                              style={{ margin: 0 }}
                              control={
                                <Checkbox
                                  checked={resource.isBillable}
                                  onChange={event =>
                                    updateResource(
                                      resource.userId,
                                      "isBillable",
                                      event.currentTarget.checked
                                    )
                                  }
                                  value="billableResource"
                                  classes={{
                                    root: classes.checkbox,
                                    checked: classes.checked
                                  }}
                                />
                              }
                            />
                          </div>
                        ) : null}
                      {hourlyRateType == "Resource" &&
                        billingMethod !== 0 &&
                        billingMethod !== 2 ? (
                          <DefaultTextField
                            label={false}
                            fullWidth={true}
                            formControlStyles={{ marginBottom: 0, width: 100 }}
                            error={false}
                            defaultProps={{
                              id: "memberRate",
                              disabled: resource.isBillable ? false : true,
                              onChange: event => {
                                updateResource(
                                  resource.userId,
                                  "hourlyRate",
                                  event.currentTarget.value
                                );
                              },
                              value: resource.hourlyRate,
                              startAdornment: currency ? currency : 'USD'
                            }}
                          />
                        ) : null}
                    </div>
                  </li>
                );
              })}
            </ul>
          </Scrollbars>
        </Grid>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profileState: state.profile
  }
}

export default compose(
  connect(
    mapStateToProps,
    { isUserPresent }
  ),
  withStyles(projectSettingStyles, { withTheme: true })
)(ProjectMembers);
