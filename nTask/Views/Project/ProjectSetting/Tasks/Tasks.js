import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import projectSettingStyles from "../styles";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import DefaultTextField from "../../../../components/Form/TextField";
import HelpIcon from "@material-ui/icons/HelpOutline";
import { connect } from "react-redux";
import { compose } from "redux";
import { getTaskName } from "../../../../utils/common";
import AddTaskDropdown from "../AddTaskDropdown";
import { Scrollbars } from "react-custom-scrollbars";
import { SaveTask } from "../../../../redux/actions/tasks";
class ProjectTasks extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  handleCheckboxChange = name => event => {
    this.setState({ [name]: event.target.checked });
  };
  componentDidUpdate() {}
  // shouldComponentUpdate(nextProps, nextState) {
  //   if(isEqual(this.props.psdata, nextProps.psdata)){
  //
  //     return false
  //   } else {
  //     return true
  //   }
  // }
  createTask = taskTitle => {
    const success = (err, response) => {
      this.props.AssignToProject("tasks", {
        taskId: response.id,
        isBillable: false,
        hourlyRate: 0
      });
    };
    const failure = () => {};
    this.props.SaveTask(
      {
        taskTitle: taskTitle,
        projectId: this.props.psdata.projectId
      },
      success,
      failure
    );
  };
  render() {
    const {
      classes,
      theme,
      psdata,
      updateTask,
      selectAllAction,
      AssignToProject,
      taskAssignToProject
    } = this.props;
    const { tasks, hourlyRateType, billingMethod, currency } = psdata;
    return (
      <>
        <Grid item xs={12} className={classes.projectTasksHeadingCnt}>
          <Typography variant="h2" className={classes.projectTasksHeading}>
            Tasks
          </Typography>
          <div className="flex_center_start_row">
            {billingMethod !== 0 ? (
              <div className="flex_center_center_col" style={{ width: 100 }}>
                <HelpIcon
                  htmlColor={theme.palette.secondary.light}
                  className={classes.helpIcon}
                />
                <Typography variant="body2">Billable</Typography>
                <span
                  className={classes.selectAllLink}
                  onClick={event => {
                    selectAllAction("tasks", "isBillable");
                  }}
                >
                  Select All
                </span>
              </div>
            ) : null}
            {hourlyRateType == "Task" &&
            billingMethod !== 0 &&
            billingMethod !== 2 ? (
              <div style={{ width: 100 }} className="flex_center_center_col">
                <HelpIcon
                  htmlColor={theme.palette.secondary.light}
                  className={classes.helpIcon}
                />
                <Typography variant="body2">Hourly Rate</Typography>
              </div>
            ) : null}
          </div>
        </Grid>
        <Grid item xs={12} className={classes.projectTaskListCnt}>
          <AddTaskDropdown
            AssignToProject={AssignToProject}
            psdata={psdata}
            createTask={this.createTask}
            taskAssignToProject={taskAssignToProject}
          />
          <Scrollbars autoHide style={{ height: 230 }}>
            <ul className={classes.projectTaskList}>
              {tasks.map((task, i, arr) => {
                return (
                  <li className={classes.projectTaskListItem} key={task.taskId}>
                    <Typography variant="body2">
                      {getTaskName(task.taskId, this.props.tasksState.data)}
                    </Typography>
                    <div className="flex_center_start_row">
                      {billingMethod !== 0 ? (
                        <div
                          className="flex_center_center_row"
                          style={{ width: 100 }}
                        >
                          <FormControlLabel
                            style={{ margin: 0 }}
                            classes={{
                              label: classes.checkboxLabel
                            }}
                            control={
                              <Checkbox
                                checked={task.isBillable}
                                onChange={event => {
                                  updateTask(
                                    task.taskId,
                                    "isBillable",
                                    event.currentTarget.checked
                                  );
                                }}
                                classes={{
                                  root: classes.checkbox,
                                  checked: classes.checked
                                }}
                              />
                            }
                          />
                        </div>
                      ) : null}
                      {hourlyRateType == "Task" &&
                      billingMethod !== 0 &&
                      billingMethod !== 2 ? (
                        <DefaultTextField
                          label={false}
                          fullWidth={true}
                          formControlStyles={{ marginBottom: 0, width: 100 }}
                          error={false}
                          defaultProps={{
                            id: task.taskId,
                            disabled: task.isBillable ? false : true,
                            onChange: event => {
                              updateTask(
                                task.taskId,
                                "hourlyRate",
                                event.currentTarget.value
                              );
                            },
                            value: task.hourlyRate,
                            startAdornment: currency ? currency : "USD"
                          }}
                        />
                      ) : null}
                    </div>
                  </li>
                );
              })}
            </ul>
          </Scrollbars>
        </Grid>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    tasksState: state.tasks
  };
};

export default compose(
  connect(
    mapStateToProps,
    { SaveTask }
  ),
  withStyles(projectSettingStyles, { withTheme: true })
)(ProjectTasks);
