import React, { Component, Fragment } from "react";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import Select from "react-select";
import { components } from "react-select";
import Avatar from "@material-ui/core/Avatar";
import autoCompleteStyles from "../../../assets/jss/components/autoComplete";
import combineStyles from "../../../utils/mergeStyles";
import FormControl from "@material-ui/core/FormControl";
import { connect } from "react-redux";
import { compose } from "redux";
import { generateActiveMembers } from '../../../helper/getActiveMembers';

class AddMemberDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      AssignedToLabel: false,
      AddToProjectLabel: false,
      value: [],
      AssignEmailError: false,
      taskTitle: undefined
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.onSelectFocus = this.onSelectFocus.bind(this);
    this.onSelectBlur = this.onSelectBlur.bind(this);
  }
  handleChange(event, name) {
    this.setState({
      [name]: event.target.value // Mapping value of textfield to local state
    });
  }
  onSelectFocus(event) {
    this.setState({ [event.target.id]: true });
  }
  onSelectBlur(event) {
    if (this.state.value.length < 1) {
      this.setState({ [event.target.id]: false });
    }
  }

  handleSelectChange(newValue, actionMeta) {
    //Getting the email entered by user from the new value object
    let validateValue =
      newValue.length > 1
        ? newValue.slice(-1)[0].label
        : newValue.length == 1
          ? newValue[0].label
          : null;

    // validate the new selected value against email if validated returns value else returns null
    if (actionMeta.action == "create-option") {
      //setting the value if the email is validated
      this.props.inviteNewMember(newValue.value);
      this.setState({ value: "" });
    } else if (actionMeta.action == "select-option") {
      // setting the value if user selected value from the dropdown list
      let newObj = {
        userId: newValue.id,
        isProjectManager: false,
        isBillable: false,
        isAssignee: false,
        hourlyRate: 0
      };

      this.props.AssignToProject("resources", newObj);
      this.setState({ value: "" });
    }
  }
  generateData = () => {
    let members = generateActiveMembers();
    let psMembers = this.props.psdata.resources;
    let psMemberIds = [];
    let newArr = [];
    psMembers.forEach(x => {
      psMemberIds.push(x.userId);
    });
    members.filter(m => !m.isDeleted).forEach(x => {
      if (psMemberIds.indexOf(x.userId) == -1) {
        newArr.push({ value: x.fullName, label: x.fullName, id: x.userId });
      }
    });
    return newArr;
  };
  render() {
    const { classes, theme, profile, projectId } = this.props;
    // Styles of react-select autocomplete
    const customStyles = {
      control: (base, state) => ({
        ...base,
        background: "transparent",
        padding: "4px 0 4px 0",
        minHeight: 41,
        borderRadius: 4,
        borderColor: theme.palette.border.lightBorder,
        boxShadow: "none",
        ":focus": {
          border: `1px solid ${theme.palette.border.darkBorder}`
        },
        ":hover": {
          border: `1px solid ${theme.palette.border.darkBorder}`
        }
      }),
      input: (base, state) => ({
        color: theme.palette.text.secondary
      }),
      valueContainer: (base, state) => ({
        ...base,
        padding: "0 0 0 14px"
      })
    };

    // Text for the select option when inviting new user
    const addEmailLabel = inputValue => {
      return `Invite "${inputValue}"`;
    };

    //Component to show avatar in the options list
    const Option = props => {
      return (
        <components.Option {...props}>
          <Grid
            container
            direction="row"
            justify="flex-start"
            alignItems="center"
          >
            {props.data.__isNew__ ? (
              //shows avatar for inviting new user by email
              <Avatar classes={{ root: classes.dropDownAddAvatar }}>+</Avatar>
            ) : //returns null so no icon is shown with tasks if exists in list
              null}
            <span>{props.children}</span>
          </Grid>
        </components.Option>
      );
    };

    const options = this.generateData();
    return (
      <FormControl
        style={{ marginBottom: 20 }}
        classes={{ root: classes.formControl }}
        fullWidth={true}
      >
        <Select
          isClearable={false}
          styles={customStyles}
          onFocus={this.onSelectFocus}
          onBlur={this.onSelectBlur}
          // formatCreateLabel={addEmailLabel}
          inputId="AssignedToLabel"
          components={{
            ClearIndicator: true,
            DropdownIndicator: null,
            Option: Option,
            makeAnimated: true
          }}
          onChange={this.handleSelectChange}
          options={options}
          value={this.state.value}
          placeholder="Add Resources"
        />
      </FormControl>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profileState: state.profile
  }
}

export default compose(
  connect(mapStateToProps),
  withStyles(combineStyles(autoCompleteStyles), {
    withTheme: true
  })
)(AddMemberDropdown);
