import React, { Component } from "react";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "react-select";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import HelpIcon from "@material-ui/icons/HelpOutline";
import Typography from "@material-ui/core/Typography";
import { connect } from "react-redux";
import { compose } from "redux";
import cloneDeep from "lodash/cloneDeep";
import LeftArrow from "@material-ui/icons/ChevronLeft";
import isEqual from "lodash/isEqual";
import ProjectTasks from "./Tasks/Tasks";
import ProjectMembers from "./Members/Members";
import {
  getProjectSetting,
  saveProjectSetting,
  UpdateProjectPartially,
} from "../../../redux/actions/projects";
import CustomButton from "../../../components/Buttons/CustomButton";
import { calculateContentHeight } from "../../../utils/common";
import projectSettingStyles from "./styles";
import BillingMethodTabs from "./BillingMethod/BillingMethodTabs";
import { BulkUpdateTask } from "../../../redux/actions/tasks";
import SelectCurrency from "./CurrencyDropdown";

class ProjectSetting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: 0,
      psdata: "",
      newPst: [],
      currencyOptions: [],
      selectedCurrency: {},
    };
    this.initState = "";
  }

  handleTabChange = (event, value) => {
    this.setState(prevState => ({
      psdata: { ...prevState.psdata, billingMethod: value },
    }));
  };

  updateTask = (taskId, key, value) => {
    // Function Takes taskId, key and value of the task object that needs to be updated
    const { psdata } = this.state; // current profile setting data from state
    const copy = cloneDeep(psdata); // making copy of profile setting data
    const index = copy.tasks.findIndex(ele => {
      // finding index of task that needs to be udpated
      return ele.taskId == taskId;
    });
    copy.tasks[index][key] = value; // setting the desired value change in task object in profile setting
    this.setState({ psdata: copy }); // updating object of profile setting stored in state
  };

  updateResource = (userId, key, value) => {
    // Function Takes userId, key and value of the user object that needs to be updated
    const { psdata } = this.state; // current profile setting data from state
    const copy = cloneDeep(psdata); // making copy of profile setting data
    const index = copy.resources.findIndex(ele => {
      // finding index of user that needs to be udpated
      return ele.userId == userId;
    });
    copy.resources[index][key] = value; // setting the desired value change in task object in profile setting
    this.setState({ psdata: copy }); // updating object of profile setting stored in state
  };

  AssignToProject = (key, object) => {
    // Function Takes userId, key and value of the user object that needs to be updated
    const { psdata } = this.state; // current profile setting data from state
    const copy = cloneDeep(psdata); // making copy of profile setting data

    copy[key].unshift(object); // setting the desired value change in task object in profile setting
    this.setState({ psdata: copy }); // updating object of profile setting stored in state
  };

  updateProjectSetting = (key, value) => {
    this.setState(prevState => ({
      psdata: { ...prevState.psdata, [key]: value },
    }));
  };

  taskAssignToProject = taskId => {
    // Adding Task id's into array to later on use for bulk update task's project
    this.setState({ newPst: [...this.state.newPst, taskId] });
  };

  componentDidUpdate(prevProps, prevState) {
    const stateEqCheck = isEqual(prevState.psdata, this.state.psdata);
    const initstateEqCheck = isEqual(this.state.psdata, this.initState);
    if (!prevState.psdata && !stateEqCheck) {
      this.setState({ changes: false });
    }
    if (prevState.psdata && !stateEqCheck && !initstateEqCheck) {
      this.setState({ changes: true });
    }
  }

  componentDidMount() {
    this.props.getProjectSetting(
      this.props.projectId,
      data => {
        // Success
        this.initState = data;
        let selectedCurrency = {};
        const currencyOptions = [];
        this.props.constantsState.data.currencies.forEach(function(obj) {
          const name = `${obj.isoCurrencySymbol} - ${obj.currencyEnglishName}`;
          currencyOptions.push({
            Active: "false",
            value: obj.isoCurrencySymbol,
            label: name,
          });
          if (data.currency === obj.isoCurrencySymbol)
            selectedCurrency = {
              Active: "false",
              value: obj.isoCurrencySymbol,
              label: name,
            };
        });
        this.setState({
          currencyOptions,
          psdata: { ...data, currency: data.currency ? data.currency : "USD" },
          selectedCurrency,
        });
      },
      () => {
        // Failure
      }
    );
  }

  selectAllAction = (updateType, key) => {
    // Function Takes userId, key and value of the user object that needs to be updated
    const { psdata } = this.state; // current profile setting data from state
    const copy = cloneDeep(psdata); // making copy of profile setting data
    copy[updateType].forEach(x => {
      x[key] = true;
    });
    this.setState({ psdata: copy }); // updating object of profile setting stored in state
  };

  handleDiscardChanges = event => {
    this.setState({ psdata: this.initState, changes: false });
  };

  handleSaveProjectSetting = () => {
    // function that save project setting on backend
    this.setState({ changes: false });
    const success = data => {
      // sucess logic goes here
      this.initState = data;
      if (this.state.newPst.length) {
        this.props.BulkUpdateTask({
          // Bulk Updating actual tasks by assigning them this project
          projectId: this.state.psdata.projectId,
          taskIds: this.state.newPst,
          type: 2,
        });
      }

      const projectManager = [];
      const resources = [];
      data.resources.forEach(resource => {
        resources.push(resource.userId);
        if (resource.isProjectManager) projectManager.push(resource.userId);
      });
      this.props.UpdateProjectPartially({
        projectId: this.props.projectId,
        projectManager,
        resources,
      });
    };

    const failure = data => {
      // failure logic goes here
      this.setState({ changes: true });
    };
    this.props.saveProjectSetting(this.state.psdata, success, failure);
  };

  handleSelectChange = item => {
    this.setState(
      prevState => ({ selectedCurrency: item }),
      () => {
        this.updateProjectSetting("currency", item.value);
      }
    );
  };

  render() {
    const { classes, theme, closeProjectSetting, projectName, projectPer } = this.props;
    const { psdata, changes, currencyOptions, selectedCurrency } = this.state;
    const { billingMethod } = psdata;

    return (
      <>
        <div className={`flex_center_space_between_row ${classes.projectSettingHeader}`}>
          <div className="flex_center_start_row">
            <LeftArrow onClick={closeProjectSetting} className={classes.backArrowIcon} />

            <Typography variant="h1">{projectName}</Typography>
          </div>
          <div>
            <CustomButton
              btnType="plain"
              disableRipple
              variant="text"
              disabled={!changes}
              style={{ marginRight: 20 }}
              onClick={event => this.handleDiscardChanges(event)}>
              Discard Changes
            </CustomButton>
            <CustomButton
              // onClick={event => this.handleToggle(event)}
              disabled={!changes}
              onClick={event => this.handleSaveProjectSetting(event)}
              btnType="success"
              disableRipple
              variant="contained">
              Save
            </CustomButton>
          </div>
        </div>

        <Grid
          container
          style={{ height: calculateContentHeight() }}
          classes={{ container: classes.projectSettingCnt }}>
          {psdata && this.props.tasksState.data ? (
            <>
              {projectPer.projectSetting.editBudget.isAllowEdit && (
                <>
                  <Grid item xs={12} className={classes.topHeading}>
                    <Typography variant="h2" className="mb20">
                      Billing Method
                    </Typography>
                  </Grid>
                  <Grid item xs={6} className={classes.BillingMethodsTabCnt}>
                    <BillingMethodTabs
                      handleTabChange={this.handleTabChange}
                      activeTab={psdata.billingMethod}
                      psdata={psdata}
                      updateProjectSetting={this.updateProjectSetting}
                    />
                  </Grid>
                </>
              )}
              <Grid item xs={6} className={classes.BillingMethodsInfoCnt}>
                {/* Not Billable Info Starts */}
                <div className={classes.infoCntRight}>
                  {billingMethod == 0 ? (
                    <div>
                      <div className={`flex_center_start_row ${classes.infoAreaHeadingCnt}`}>
                        <HelpIcon
                          htmlColor={theme.palette.secondary.light}
                          className={classes.helpIcon}
                        />
                        <Typography variant="h5">Not-Billable</Typography>
                      </div>
                      <Typography variant="body2" className={classes.infoSubHeading}>
                        Choose this if you do not wish to bill your client for this project. You may
                        use it for your internal projects.
                      </Typography>
                    </div>
                  ) : billingMethod == 1 ? (
                    <div>
                      {/* Not Billable Info Ends */}

                      {/* Time and Materials Info Starts */}
                      <div className={`flex_center_start_row ${classes.infoAreaHeadingCnt}`}>
                        <HelpIcon
                          htmlColor={theme.palette.secondary.light}
                          className={classes.helpIcon}
                        />
                        <Typography variant="h5">Time & Materials</Typography>
                      </div>
                      <Typography variant="body2" className={classes.infoSubHeading}>
                        Choose this option to bill your project by hourly rates.
                      </Typography>
                      <Typography variant="body2" className={classes.infoPoints}>
                        <span style={{ color: theme.palette.text.primary }}>By Project:</span>{" "}
                        Choose this option to add hourly rate for entire project. Hourly rate for
                        all tasks and resources will be same.
                      </Typography>
                      <Typography variant="body2" className={classes.infoPoints}>
                        <span style={{ color: theme.palette.text.primary }}>By Task:</span> Choose
                        this option to add hourly rates for tasks. You can add different hourly
                        rates for each task.
                      </Typography>
                      <Typography variant="body2">
                        <span
                          style={{ color: theme.palette.text.primary }}
                          className={classes.infoPoints}>
                          By Resource:
                        </span>{" "}
                        Choose this option to add hourly rate for resources. You can add different
                        hourly rates for each resource.
                      </Typography>
                    </div>
                  ) : /* Time and Materials Info Ends */
                  billingMethod == 2 ? (
                    // {/* Fixed Fee Info Starts */}
                    <div>
                      <div className={`flex_center_start_row ${classes.infoAreaHeadingCnt}`}>
                        <HelpIcon
                          htmlColor={theme.palette.secondary.light}
                          className={classes.helpIcon}
                        />
                        <Typography variant="h5">Fixed Fee</Typography>
                      </div>
                      <Typography variant="body2" className={classes.infoSubHeading}>
                        A fixed fee project means you bill at a set price. No matter how long it
                        takes to complete it.
                      </Typography>
                    </div>
                  ) : null}
                  {/* Fixed Fee Info Ends */}
                </div>
                <div className={classes.currencyDropdownCnt}>
                  <SelectCurrency
                    onChange={this.handleSelectChange}
                    options={currencyOptions}
                    value={selectedCurrency}
                    isDisabled={!projectPer.projectSetting.editCurrency.isAllowEdit}
                  />
                </div>
              </Grid>
              <Grid item xs={6} className={classes.projectTasksCnt}>
                <ProjectTasks
                  psdata={psdata}
                  updateTask={this.updateTask}
                  selectAllAction={this.selectAllAction}
                  AssignToProject={this.AssignToProject}
                  taskAssignToProject={this.taskAssignToProject}
                />
              </Grid>
              {projectPer.projectSetting.editResources.isAllowEdit && (
                <Grid item xs={6} className={classes.projectMembersCnt}>
                  <ProjectMembers
                    psdata={psdata}
                    updateResource={this.updateResource}
                    selectAllAction={this.selectAllAction}
                    AssignToProject={this.AssignToProject}
                  />
                </Grid>
              )}
            </>
          ) : null}
        </Grid>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    constantsState: state.constants,
    tasksState: state.tasks,
    projectPer: state.workspacePermissions.data.project,
  };
};

export default compose(
  connect(mapStateToProps, {
    getProjectSetting,
    saveProjectSetting,
    UpdateProjectPartially,
    BulkUpdateTask,
  }),
  withStyles(projectSettingStyles, { withTheme: true })
)(ProjectSetting);
