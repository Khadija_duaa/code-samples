import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import InputLabel from "@material-ui/core/InputLabel";
import Avatar from "@material-ui/core/Avatar";
import Select from 'react-select';
import { components } from "react-select";
import autoCompleteStyles from "../../../assets/jss/components/autoComplete";
import Grid from "@material-ui/core/Grid";
import DropdownArrow from "@material-ui/icons/ArrowDropDown"
import { taskDetailsHelpText } from "../../../components/Tooltip/helptext";
import CustomTooltip from "../../../components/Tooltip/Tooltip";


function SelectCurrency(props){
    const { classes, theme, onChange,options, value, isDisabled  } = props;
    const DropdownIndicator = (props) => {
      return components.DropdownIndicator && (
        <components.DropdownIndicator {...props}>
          <DropdownArrow htmlColor={theme.palette.secondary.light} fontSize="default" />
        </components.DropdownIndicator>
      );
    };
    // Styles of react-select autocomplete
    const customStyles = {
      control: (base, state) => ({
        ...base,
        background: "transparent",
        padding: "0 10px 0 15px",
        fontSize: theme.typography.pxToRem(16),
        borderRadius: 4,
        borderColor: theme.palette.border.lightBorder,
        boxShadow: 'none',
        ":focus": {
          border: `1px solid ${theme.palette.border.darkBorder}`
        },
        ":hover": {
          border: `1px solid ${theme.palette.border.darkBorder}`
        }
      }),
      dropdownIndicator: (base, state) => ({
        padding: 0
      }),
      option: (styles, { data, isDisabled, isFocused, isSelected }) => {
        return {...styles,
        fontSize: "12px",
        padding: "10px 12px"
        }
      },
      valueContainer: (base, state) => ({
        ...base,
        padding: "5px 0 5px 0px",
        fontSize: "12px",
        color: theme.palette.text.light
      })
    };
    

    return (
      <FormControl classes={{ root: classes.formControl }} fullWidth={true}>
        <InputLabel
          htmlFor="AddToProject"
          classes={{
            root: classes.autoCompleteLabel,
            shrink: classes.shrinkLabel
          }}
          shrink={false}
         
        >
          Currency
          <CustomTooltip helptext={taskDetailsHelpText.selectProjectHelpText} iconType="help" />
        </InputLabel>
        <Select
          styles={customStyles}
          onChange={onChange}
          inputId="ChangeCurrency"
          components={{ DropdownIndicator, IndicatorSeparator: false }}
          options={options}
          value={value}
          noOptionsMessage={() => {return "No Project"}}
          isDisabled ={isDisabled }
        />
      </FormControl>
    );
  
        }

export default compose(
  withRouter,
  withStyles(autoCompleteStyles, {
    withTheme: true
  }),
  
)(SelectCurrency);