const projectSettingStyles = theme => ({
    projectSettingCnt:{
        background: theme.palette.common.white,
        padding: "20px 40px 20px 60px",
        borderTop: `1px solid ${theme.palette.border.lightBorder}`,
        overflowY: "auto",
        position: "relative"
    },
    projectSettingHeader:{
        margin: "20px 0 30px 0",
        padding: "0 20px 0 62px"
    },
    BillingMethodsTabCnt:{
        borderRight: `1px solid ${theme.palette.border.lightBorder}`,
        padding: "0 30px 0 0"
    },
    projectTasksCnt:{
        padding: "0 30px 0 0",
        marginTop: 20
    },
    projectMembersCnt:{
        padding: "0 0 0 30px",
        marginTop: 20
    },
    BillingMethodsInfoCnt:{
        padding: "0 0 0 30px",
        display: "flex"
    },
    tabContainer:{
        border: `1px solid ${theme.palette.border.lightBorder}`,
        borderTop: "none",
        padding: 20,
        minHeight: 314
    },
    tabsRoot:{
      
    },
    tabLabel: {
        fontSize: "12px !important",
        color: theme.palette.text.primary,
        fontWeight: theme.typography.fontWeightLight,
        textTransform: "capitalize"
    },
    labelContainer:{
        padding: 0,
        textAlign: "left"
        fontSize: "12px !important",
        color: theme.palette.text.primary,
        fontWeight: theme.typography.fontWeightLight,
        textTransform: "capitalize"
    },
    tabRoot:{
        border: `1px solid ${theme.palette.border.lightBorder}`,
        background: theme.palette.background.paper,
        minHeight: 56,
        padding: "0 10px",
        maxWidth: "unset",
        flex: 1
    },
    tabIndicator:{
        display: "none"
    },
    tabSelected:{
        background: theme.palette.common.white,
        "& $checkedIcon":{
            visibility: "visible"
        }
    },
    checkedIcon:{
        fontSize: "20px !important",
        position: "absolute",
        right: 12,
        visibility: "hidden"
    },
    rateTypeSelectCnt:{
        paddingBottom: 20,
        marginBottom: 20,
        borderBottom: `1px solid ${theme.palette.border.lightBorder}`
    },
    inputLeftLabel:{
        width: 120,
        marginRight: 10
    },
    projectFeeLabel:{
        width: 130,
        marginRight: 10
    },
    inputRightLabel:{
        marginLeft: 10
    },
    checkbox: {
        color: theme.palette.primary.light,
        '&$checked': {
          color: theme.palette.primary.light,
        },
      },
      checkboxLabel:{
          color: theme.palette.text.primary
      },
      checked: {},
      helpIcon:{
          fontSize: "16px !important",
          marginRight: 5
      },
      infoAreaHeadingCnt:{
          marginBottom: 5
      },
      infoSubHeading:{
          marginLeft: 22,
          marginBottom: 10
      },
      infoPoints:{
          marginBottom: 10,
          marginLeft: 22
      },
      topHeading:{
          marginBottom: 20
      },
      projectTaskListCnt:{
          border:`1px solid ${theme.palette.border.lightBorder}`,
          borderRadius: 4,
          padding: "10px 15px",
          "& ul":{
              listStyleType: "none",
              padding: 0,
              margin: 0,
              "& li":{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  padding: "10px 5px",
                  borderBottom:`1px solid ${theme.palette.border.lightBorder}`,
                "& $checkbox":{
                    padding: 0,
                    margin: 0
                }
              }
          },
         
      },
      memberProfileAvatar:{
        width: 24,
        height: 24,
        marginRight: 10
    },
    projectTasksHeading:{
        flex: 1
    },
    projectTasksHeadingCnt:{
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        marginBottom: 10,
        padding: "0 20px",
        minHeight: 45
    },
    helpIcon:{
        fontSize: "16px !important"
    },
    selectAllLink:{
        fontSize: "10px !important",
        color: theme.palette.text.light,
        textDecoration: "underline",
        cursor: "pointer"
    },
    backArrowIcon: {
        fontSize: "28px !important",
        marginRight: 10,
        cursor: "pointer"
      },
      infoCntRight: {
        flex: 1
      },
      currencyDropdownCnt:{
          width: 200,
          marginLeft: 10,
          marginTop: 23

      }

})

export default projectSettingStyles;