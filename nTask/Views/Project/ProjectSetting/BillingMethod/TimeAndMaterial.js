import React, { Component } from "react";
import Typography from "@material-ui/core/Typography";
import SelectIconMenu from "../../../../components/Menu/TaskMenus/SelectIconMenu";
import NotificationMessage from "../../../../components/NotificationMessages/NotificationMessages";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import projectSettingStyles from "../styles";
import DefaultTextField from "../../../../components/Form/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import InputAdornment from "@material-ui/core/InputAdornment";

class TimeAndMaterial extends Component {
  constructor(props) {
    super(props);
    this.state = {
      totalBudget: "",
      projectPercent: "",
      budgetResetCheck: false,
      projectExceedCheck: false
    };
  }
  handleInputChange = (event, name) => {
    this.setState({ [name]: event.target.value });
  };
  handleCheckboxChange = name => event => {
    this.setState({ [name]: event.target.checked });
  };
  render() {
    const { classes, psdata, updateProjectSetting } = this.props;
    const {
      projectPercent,
    } = this.state;
    const {
      hourlyRateType,
      budget,
      budgetReset,
      sendEmailAlert,
      hourlyRate,
      alertLimit,
      currency
    } = psdata;
    
    return (
      <>
        <div className={classes.rateTypeSelectCnt}>
          <Typography variant="h5">Hourly Rate</Typography>
          <Grid item xs={12}>
            <div className="flex_center_start_row mb20">
              <Typography variant="body2" className={classes.inputLeftLabel}>
                Set hourly rate by:
              </Typography>

              <SelectIconMenu
                iconType="status"
                isSimpleList={false}
                handleHourlyRateDD={value => {
                  updateProjectSetting("hourlyRateType", value);
                }}
                projectRateType={hourlyRateType}
                heading="Select Rate"
                menuType="projectRate"
                isSingle={true}
                style={{ marginBottom: 0, width: 150, marginRight: 10 }}
              />
              {hourlyRateType == "Project" ?
              <DefaultTextField
                label={false}
                fullWidth={true}
                formControlStyles={{ marginBottom: 0, width: 100 }}
                error={false}
                defaultProps={{
                  id: "hourlyRate",
                  onChange: event =>
                    updateProjectSetting("hourlyRate", event.target.value),
                  value: hourlyRate,
                  startAdornment:currency
                }}
              />: null}
            </div>
          </Grid>
          {
            hourlyRateType !== "Project" ?
              <NotificationMessage iconType="info" type="info">
                {
                  `Set your rates in the ${hourlyRateType}s section below.`
                }
              </NotificationMessage> : null
          }
        </div>
        <div className={classes.budgetSelectCnt}>
          <Typography variant="h5">Budget</Typography>
          <Grid item xs={12}>
            <div className="flex_center_start_row">
              <Typography variant="body2" className={classes.inputLeftLabel}>
                Set total budget:
              </Typography>
              <DefaultTextField
                label={false}
                fullWidth={true}
                formControlStyles={{ marginBottom: 0, width: 150 }}
                error={false}
                defaultProps={{
                  id: "totalBudget",
                  onChange: event =>
                    updateProjectSetting("budget", event.target.value),
                  value: budget,
                  startAdornment:currency
                }}
              />
              <Typography
                variant="body2"
                className={classes.inputRightLabel}
                style={{ flex: 1 }}
              >
                per month
              </Typography>
            </div>
            <FormControlLabel
              classes={{
                label: classes.checkboxLabel
              }}
              control={
                <Checkbox
                  checked={budgetReset}
                  onChange={event => {
                    updateProjectSetting(
                      "budgetReset",
                      event.currentTarget.checked
                    );
                  }}
                  value="budgetResetCheck"
                  classes={{
                    root: classes.checkbox,
                    checked: classes.checked
                  }}
                />
              }
              label="Budget resets every month"
            />
            <div className="flex_center_start_row">
              <FormControlLabel
                classes={{
                  label: classes.checkboxLabel
                }}
                control={
                  <Checkbox
                    checked={sendEmailAlert}
                    onChange={event => {
                      updateProjectSetting(
                        "sendEmailAlert",
                        event.currentTarget.checked
                      );
                    }}
                    value="projectExceedCheck"
                    classes={{
                      root: classes.checkbox,
                      checked: classes.checked
                    }}
                  />
                }
                label="Send email alerts if project exceeds"
              />

              <DefaultTextField
                label={false}
                fullWidth={true}
                formControlStyles={{ marginBottom: 0, width: 70 }}
                error={false}
                defaultProps={{
                  id: "projectPercent",
                  disabled: !sendEmailAlert,
                  onChange: event =>
                  updateProjectSetting("alertLimit", event.target.value),
                  value: alertLimit
                  
                }}
              />
              <Typography variant="body2" className={classes.inputRightLabel}>
                % of project
              </Typography>
            </div>
          </Grid>
        </div>
      </>
    );
  }
}

export default withStyles(projectSettingStyles, { withTheme: true })(
  TimeAndMaterial
);
