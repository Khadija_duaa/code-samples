import React, {Component} from "react";
import { withStyles } from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import projectSettingStyles from "../styles";
import SvgIcon from "@material-ui/core/SvgIcon";
import CheckBoxIcon from "../../../../components/Icons/CheckBoxIcon";
import TimeAndMaterial from "./TimeAndMaterial";
import FixedFee from "./FixedFee";
import NotificationMessage from "../../../../components/NotificationMessages/NotificationMessages";

class BillingMethodTabs extends Component {
  render() {
    const { classes, theme, activeTab, handleTabChange, psdata, updateProjectSetting } = this.props;
    return (
      <>
        <Tabs
          value={activeTab}
          onChange={handleTabChange}
          classes={{
            root: classes.tabsRoot,
            indicator: classes.tabIndicator
          }}
        >
          <Tab
            label="Not-Billable"
            icon={
              <SvgIcon
                viewBox="0 0 426.667 426.667"
                htmlColor={theme.palette.status.completed}
                classes={{ root: classes.checkedIcon }}
              >
                <CheckBoxIcon />
              </SvgIcon>
            }
            classes={{
              // label: classes.tabLabel,
              wrapper: classes.labelContainer,
              root: classes.tabRoot,
              selected: classes.tabSelected
            }}
          />
          <Tab
            label="Time & Materials"
            style={{ borderLeft: 0, borderRight: 0 }}
            icon={
              <SvgIcon
                viewBox="0 0 426.667 426.667"
                htmlColor={theme.palette.status.completed}
                classes={{ root: classes.checkedIcon }}
              >
                <CheckBoxIcon />
              </SvgIcon>
            }
            classes={{
              // label: classes.tabLabel,
              root: classes.tabRoot,
              wrapper: classes.labelContainer,
              selected: classes.tabSelected
            }}
          />
          <Tab
            label="Fixed Fee"
            icon={
              <SvgIcon
                viewBox="0 0 426.667 426.667"
                htmlColor={theme.palette.status.completed}
                classes={{ root: classes.checkedIcon }}
              >
                <CheckBoxIcon />
              </SvgIcon>
            }
            classes={{
              // label: classes.tabLabel,
              wrapper: classes.labelContainer,
              root: classes.tabRoot,
              selected: classes.tabSelected
            }}
          />
        </Tabs>
        {activeTab === 0 && (
          <div className={classes.tabContainer}>
             <NotificationMessage iconType="info" type="info" psdata={psdata} updateProjectSetting={updateProjectSetting}>
             This project is not billable
        </NotificationMessage>
          </div>
        )}
        {activeTab === 1 && (
          <div className={classes.tabContainer}>
            <TimeAndMaterial psdata={psdata} updateProjectSetting={updateProjectSetting}/>
          </div>
        )}
        {activeTab === 2 && (
          <div className={classes.tabContainer} >
            <FixedFee psdata={psdata} updateProjectSetting={updateProjectSetting}/>
          </div>
        )}
      </>
    );
  }
}

export default withStyles(projectSettingStyles, { withTheme: true })(
  BillingMethodTabs
);
