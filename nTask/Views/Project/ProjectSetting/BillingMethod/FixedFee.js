import React, { Component } from "react";
import Typography from "@material-ui/core/Typography";
import SelectIconMenu from "../../../../components/Menu/TaskMenus/SelectIconMenu";
import NotificationMessage from "../../../../components/NotificationMessages/NotificationMessages";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import projectSettingStyles from "../styles";
import DefaultTextField from "../../../../components/Form/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";

class FixedFee extends Component {
  constructor(props) {
    super(props);
    this.state = {
      projectPercent: "",
      budgetResetCheck: false,
      projectExceedCheck: false,
      projectFee: ""
    };
  }

  handleInputChange = (event, name) => {
    this.setState({ [name]: event.target.value });
  };
  handleCheckboxChange = name => event => {
    this.setState({ [name]: event.target.checked });
  };
  render() {
    const { classes, psdata, updateProjectSetting } = this.props;
    const { budget, sendEmailAlert, alertLimit, currency } = psdata;

    return (
      <>
        <div className={classes.rateTypeSelectCnt}>
          <Typography variant="h5">Project Fee</Typography>
          <Grid item xs={6}>
            <div className="flex_center_start_row">
              <Typography variant="body2" className={classes.projectFeeLabel}>
                Set total project fee:
              </Typography>
              <DefaultTextField
                label={false}
                fullWidth={true}
                formControlStyles={{ marginBottom: 0, width: 150 }}
                error={false}
                defaultProps={{
                  id: "budget",
                  onChange: event =>
                    updateProjectSetting("budget", event.target.value),
                  value: budget,
                  startAdornment: currency
                }}
              />
            </div>
          </Grid>
        </div>
        <div className={classes.budgetSelectCnt}>
          <Typography variant="h5">Budget</Typography>
          <NotificationMessage
            style={{ marginTop: 10, marginBottom: 10 }}
            iconType="info"
            type="info"
          >
            Your project fee and budget will be the same.
          </NotificationMessage>
          {/* <Grid item xs={12}>
            <div className="flex_center_start_row">
              <FormControlLabel
                classes={{
                  label: classes.checkboxLabel
                }}
                control={
                  <Checkbox
                    checked={sendEmailAlert}
                    onChange={event => {
                      updateProjectSetting(
                        "sendEmailAlert",
                        event.currentTarget.checked
                      );
                    }}
                    value="projectExceedCheck"
                    classes={{
                      root: classes.checkbox,
                      checked: classes.checked
                    }}
                  />
                }
                label="Send email alerts if project exceeds"
              />
              <DefaultTextField
                label={false}
                fullWidth={true}
                formControlStyles={{ marginBottom: 0, width: 70 }}
                error={false}
                defaultProps={{
                  id: "projectPercent",
                  disabled: !sendEmailAlert,
                  onChange: event =>
                    updateProjectSetting("alertLimit", event.target.value),
                  value: alertLimit
                }}
              />
              <Typography variant="body2" className={classes.inputRightLabel}>
                % of project
              </Typography>
            </div>
          </Grid> */}
        </div>
      </>
    );
  }
}

export default withStyles(projectSettingStyles, { withTheme: true })(FixedFee);
