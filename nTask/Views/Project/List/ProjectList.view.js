import React, { useCallback, useEffect, useState } from "react";
import { withSnackbar } from "notistack";

import loadable from '@loadable/component'
const CustomTable = loadable(() => import("../../../components/CustomTable2/listViewTable.cmp"));
import { useDispatch, useSelector } from "react-redux";
import isEqual from "lodash/isEqual";
import { AgGridColumn } from "@ag-grid-community/react";
import ProjectActionDropdown from "../ProjectActionDropdown/ProjectActionDropdown";
import StatusDropdown from "../../../components/Dropdown/StatusDropdown/Dropdown";
import withStyles from "@material-ui/core/styles/withStyles";
import AddIcon from "@material-ui/icons/Add";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";
import cloneDeep from "lodash/cloneDeep";

import { FormattedMessage, injectIntl } from "react-intl";
import { compose } from "redux";
import helper from "../../../helper";
import AssigneeDropdown from "../../../components/Dropdown/AssigneeDropdown";
import moment from "moment";
import Typography from "@material-ui/core/Typography";
import { Circle } from "rc-progress";
import projectColumnDefs, { sortAlphabetically } from "./projectColumns";
import Projectcmp from "../../../components/BulkActionsProject/Project.cmp";
import GroupByComponents from "../../../components/CustomTable2/GroupByComponents/GroupByComponents";
import SvgIcon from "@material-ui/core/SvgIcon";
import ToggleUpdateIcon from "../../../components/Icons/ToggleUpdateIcon";
import MeetingsIcon from "../../../components/Icons/MeetingIcon";
import IssueIcon from "../../../components/Icons/IssueIcon";
import RiskIcon from "../../../components/Icons/RiskIcon";
import TaskIcon from "../../../components/Icons/TaskIcon";
import Stared from "../../../components/Starred/Starred";
import CustomTooltip from "../../../components/Tooltip/Tooltip";
import { isDateEqual, inDateRange } from "../../../helper/dates/dates";
import { taskDetailDialogState } from "../../../redux/actions/allDialogs";
import ColumnSelector from "../../../redux/selectors/columnSelector";
import isNull from "lodash/isNull";
import CustomFieldRenderer from "../../../components/CustomTable2/CustomFieldsColumn/CustomFieldRenderer";
import { headerProps } from "./constants";
import isArray from "lodash/isArray";
import { billingMethodData } from "../../../helper/projectDropdownData";

import EmptyState from "../../../components/EmptyStates/EmptyState";
import AttachmentIcon from "@material-ui/icons/Attachment";
import ProjectFilter from "./ProjectFilter/projectFilter.view";
import { grid } from "../../../components/CustomTable2/gridInstance";
import isUndefined from "lodash/isUndefined";
import ProjectPermissionSelector from "../../../redux/selectors/projectPermissionSelector";
import TemplatesSelector from "../../../redux/selectors/templatesSelector";
import { doesFilterPass } from "./ProjectFilter/projectFilter.utils";
import isEmpty from "lodash/isEmpty";

import { emptyProject } from "../../../utils/constants/emptyProject";
import {
  updateProjectData,
  getSavedFilters,
  createProject,
  DeleteProject,
  projectDetails,
  updateProjectCf,
  markProjectCompleted,
  dispatchProject,
  exportBulkProject
} from "../../../redux/actions/projects";
import fileDownload from "js-file-download";
import { statusData } from "../../../helper/projectDropdownData";
import IconGantt from "../../../components/Icons/IconGantt";
import CustomIconButton from "../../../components/Buttons/CustomIconButton";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import IconTasks from "../../../components/Icons/IconTasks";
import projectListStyles from "./projectList.style";
import { addPendingHandler } from "../../../redux/actions/backProcesses";

// const defaultColDef = { minWidth: 200, headerClass: "customHeader" };
const ProjectList = React.memo(({ classes, theme, intl, enqueueSnackbar, style, projectClick, filteredProjects }) => {
  const globalState = useSelector(state => {
    return {
      projectColumns: ColumnSelector(state).project.columns,
      sections: ColumnSelector(state).project.sections,
      nonSectionFields: ColumnSelector(state).project.nonSectionFields,
      tasks: state.tasks.data,
      projects: state.projects.data,
      members: state.profile.data.member.allMembers,
      workspaceTemplates: state.workspaceTemplates.data,
      globalTimerTaskState: state.globalTimerTask,
      permissionObject: ProjectPermissionSelector(state),
      workspaceProjectPer: state.workspacePermissions.data.project,
      workspaceStatus: state.workspaceTemplates.data.defaultWSTemplate,
      profileState: state.profile.data,
      quickFilters: state.projects.quickFilters,
      projectFilters: state.projects.projectFilter,
      templates: TemplatesSelector(state),
      projectDetailsDialog: state.projectDetailsDialog,
      loadingProjectDetails: state.loadingProjectDetails,
    };
  });
  const sectionGroup = localStorage.getItem("sectiongrouping");
  const dispatch = useDispatch();
  const [selectedProjects, setSelectedProjects] = useState([]);
  const [projectObj, setProjectObj] = useState(null);
  const [statusCompleteDialogOpen, setStatusCompleteDialogOpen] = useState(false);
  const [statusCompleteBtnQuery, setStatusCompleteBtnQuery] = useState(false);
  const [sectionGrouping, setSectionGrouping] = useState(sectionGroup === "true" ? true : false);
  const {
    projectColumns = [],
    projects,
    members,
    sections = [],
    nonSectionFields = [],
    workspaceProjectPer,
    permissionObject,
    quickFilters,
    projectFilters,
    loadingProjectDetails,
    projectDetailsDialog,
  } = globalState;

  useEffect(() => {
    getSavedFilters("project", dispatch);
    return () => {
      grid.grid = null;
    };
  }, []);

  useEffect(() => {
    grid.grid && grid.grid.onFilterChanged();
  }, [projectFilters, quickFilters]);

  const updateProject = (project, obj) => {
    updateProjectData(
      { project, obj },
      dispatch,
      //Success
      project => {
        setProjectObj(null);
        if (grid.grid) {
          const rowNode = grid.grid && grid.grid.getRowNode(project.id);
          rowNode.setData(project);
          projectCompleteConfirmClose();
        }
      },
      err => {
        setProjectObj(null);
        if (err && err.data) showSnackBar(err.data.message, "error");
      }
    );
  };

  const handleProjectStared = (stared, obj) => {
    updateProject(obj, { isStared: stared });
  };

  // Handle Manager change
  const handleUpdateManager = (assignees, obj) => {
    const projectManagerList = assignees.map(a => a.userId);
    updateProject(obj, { projectManager: projectManagerList });
  };

  // Handle Resources change
  const handleUpdateResources = (assignees, obj) => {
    const resourcesList = assignees.map(a => a.userId);
    updateProject(obj, { resources: resourcesList });
  };

  const handleStatusChange = (status, obj) => {
    if (status.value == 3) {
      projectCompleteConfirmOpen();
      let projectObj = { ...obj, status: status.value }
      setProjectObj(projectObj)
      return;
    }
    updateProject(obj, { status: status.value });
  };

  const projectCompleteConfirmOpen = () => {
    setStatusCompleteDialogOpen(true)
  };

  const projectCompleteConfirmClose = () => {
    setStatusCompleteDialogOpen(false);
    setProjectObj(null);
    setStatusCompleteBtnQuery('');
  };

  const handleMarkProjectAsCompleted = () => {
    setStatusCompleteBtnQuery('progress');
    // markProjectCompleted(projectObj, () => { }, dispatch);
    updateProject(projectObj, { status: projectObj.status });
  };

  const handleAddProject = (data, callback) => {
    //Post Obj is object to be posted to backend for project creation
    const postObj = { projectName: data.value, clientId: data.clientId };
    //Dispatch Obj is object that is dispatched before the api call for quick entry
    const dispatchObj = {
      ...emptyProject,
      projectName: data.value,
      clientId: data.clientId,
      id: data.clientId,
      projectId: data.clientId,
      isNew: true,
    };
    createProject(postObj, dispatch, dispatchObj, res => { }, fail => {
      DeleteProject(dispatchObj, (response) => { }, err => { }, dispatch);
      showSnackBar('Project Name already exist please try with other name', "error");
    });
  };

  const StatusDropdownCmp = (row = { data: {} }) => {
    const rowData = row.data ? row.data : { data: {} };
    const projectStatusData = statusData(theme, classes, intl);

    let selectedStatus = rowData
      ? projectStatusData.find(item => item.value == rowData.status) || projectStatusData[0]
      : projectStatusData[0];

    let projectPermission = rowData?.projectId
      ? permissionObject[rowData.projectId] ?
        permissionObject[rowData.projectId].project
        : workspaceProjectPer
      : workspaceProjectPer;

    const isArchive = rowData.isDeleted;

    return (
      <StatusDropdown
        onSelect={(status) => { handleStatusChange(status, rowData); }}
        buttonType={"listButton"}
        option={selectedStatus}
        options={projectStatusData}
        toolTipTxt={selectedStatus.label}
        writeFirst={true}
        disabled={
          isArchive || !projectPermission?.projectDetails?.editProjectStatus?.isAllowEdit
        }
        preSelection={false}
        dropdownProps={{
          disablePortal: false,
        }}
      />
    );
  };

  const ProjectTitleCellRenderer = (row = { data: {} }) => {
    const rowData = row.data ? row.data : { data: {} };

    let projectPermission = rowData?.projectId
      ? permissionObject[rowData.projectId] ?
        permissionObject[rowData.projectId].project
        : workspaceProjectPer
      : workspaceProjectPer;

    const isArchive = rowData.isDeleted;
    let gantAccess = projectPermission.accessGantt.cando;
    return (
      <div className={classes.taskTitleCnt}>
        <span className={classes.GantBtnWrapper}
        >
          {gantAccess && !isArchive && (
            <CustomTooltip
              helptext={
                <FormattedMessage
                  id="project.tooltip.project-gantt"
                  defaultMessage="Project Gantt"
                />
              }
              iconType="help"
              placement="top"
              style={{ color: theme.palette.common.white }}>

              <CustomIconButton
                data-rowClick="cell"
                onClick={e => {
                  projectClick(rowData);
                }}
                btnType="transparent"
                variant="contained"
                style={{
                  width: 26,
                  height: 26,
                  borderRadius: 4,
                  opacity: 1,
                  marginRight: 10,
                  // border: "1px solid #c2c2c2"
                }}
                disabled={isArchive || !gantAccess}>
                <SvgIcon
                  viewBox="0 0 14 11"
                  htmlColor={theme.palette.secondary.medDark}
                  className={classes.IconGantt}>
                  <IconGantt />
                </SvgIcon>
              </CustomIconButton>
            </CustomTooltip>
          )}
        </span>
        <div className={`${classes.taskTitleTextCnt} wrapText`} title={rowData.projectName}>
          <span className={classes.taskTitle}>{rowData.projectName} </span>
        </div>
        {rowData.isNew && <div style={{ minWidth: 54 }}>
          <span className={classes.newTag}>New</span>
        </div>}
        <Stared
          isStared={rowData.isStared}
          handleCallback={isStared => handleProjectStared(isStared, rowData)}
          diabled={rowData.isDeleted}
        />
      </div>
    );
  };

  const ProjectManagerRenderer = row => {
    const rowData = row.data || {};
    const membersObjArr =
      members &&
      members.filter(m => rowData.projectManager && rowData.projectManager.includes(m.userId));
    let projectPermission = rowData?.projectId
      ? permissionObject[rowData.projectId] ?
        permissionObject[rowData.projectId].project
        : workspaceProjectPer
      : workspaceProjectPer;
    const isArchive = rowData.isDeleted;
    return (
      <span data-rowClick="cell">
        <AssigneeDropdown
          popperProps={{ disablePortal: false }}
          assignedTo={membersObjArr || []}
          updateAction={handleUpdateManager}
          isArchivedSelected={isArchive}
          assigneeHeading={'Project Manager(s)'}
          obj={rowData}
          avatarSize={"xsmall"}
          buttonVariant={"small"}
          customIconButtonProps={{
            classes: {
              root: classes.iconBtnStyles,
            },
          }}
          totalAssigneeBtnProps={{
            style: {
              height: 29,
              width: 29,
              fontSize: 14,
              fontWeight: theme.typography.fontWeightLarge,
            },
          }}
          customIconRenderer={<AddIcon htmlColor={theme.palette.text.dark} />}
          style={{ height: "100%" }}
          disabled={
            isArchive ||
            (!projectPermission.projectDetails.resourceTabs.projectPermissions.cando &&
              !projectPermission.projectDetails.resourceTabs.deleteResource.cando)
          }
          addPermission={projectPermission.projectDetails.resourceTabs.projectPermissions.cando}
          // deletePermission={projectPermission.projectDetails.resourceTabs.deleteResource.cando &&
          //   rowData.projectManager.length > 1}
          deletePermission={false}
        />
      </span>
    );
  };

  const ResourcesDropdownRenderer = row => {
    const rowData = row.data || {};
    const membersObjArr =
      members && members.filter(m => rowData.resources && rowData.resources.includes(m.userId));

    let projectPermission = rowData?.projectId
      ? permissionObject[rowData.projectId] ?
        permissionObject[rowData.projectId].project
        : workspaceProjectPer
      : workspaceProjectPer;
    const isArchive = rowData.isDeleted;

    return (
      <span data-rowClick="cell">
        <AssigneeDropdown
          popperProps={{ disablePortal: false }}
          assignedTo={membersObjArr || []}
          updateAction={handleUpdateResources}
          isArchivedSelected={isArchive}
          obj={rowData}
          assigneeHeading={'Resource(s)'}
          avatarSize={"xsmall"}
          buttonVariant={"small"}
          customIconButtonProps={{
            classes: {
              root: classes.iconBtnStyles,
            },
          }}
          totalAssigneeBtnProps={{
            style: {
              height: 29,
              width: 29,
              fontSize: 14,
              fontWeight: theme.typography.fontWeightLarge,
            },
          }}
          customIconRenderer={<AddIcon htmlColor={theme.palette.text.dark} />}
          style={{ height: "100%" }}
          disabled={
            isArchive ||
            (!projectPermission.projectDetails.resourceTabs.addProjectResources.cando &&
              !projectPermission.projectDetails.resourceTabs.deleteResource.cando)
          }
          addPermission={projectPermission.projectDetails.resourceTabs.addProjectResources.cando}
          // deletePermission={projectPermission.projectDetails.resourceTabs.deleteResource.cando}
          deletePermission={false}
        />
      </span>
    );
  };

  const UpdatedDateRenderer = row => {
    const rowData = row.data || {};
    const formatedDate = moment(rowData.updatedDate).format("MMM DD, YY");
    return formatedDate;
  };
  const CreatedByRenderer = (row = { data: {} }) => {
    const rowData = row.data ? row.data : { data: {} };
    return (
      <div className={`${classes.taskTitleTextCnt} ${classes.textCenter} wrapText`} title={rowData.createdBy}>
        <span className={classes.taskTitle}>
          {!isEmpty(rowData.createdBy) ? rowData.createdBy : "-"}{" "}
        </span>
      </div>
    );
  };
  const UpdatedByRenderer = (row = { data: {} }) => {
    const rowData = row.data ? row.data : { data: {} };
    return (
      <div className={`${classes.taskTitleTextCnt} ${classes.textCenter} wrapText`} title={rowData.updatedBy}>
        <span className={classes.taskTitle}>
          {!isEmpty(rowData.updatedBy) ? rowData.updatedBy : "-"}{" "}
        </span>
      </div>
    );
  };
  const ProjectStatusRenderer = (row = { data: {} }) => {
    const rowData = row.data ? row.data : { data: {} };
    return (
      <div className={`${classes.taskTitleTextCnt} wrapText`} title={rowData.projectStatusTemplate}>
        <span className={classes.taskTitle}>
          {rowData.projectStatusTemplate}
        </span>
      </div>
    );
  };
  const BillingMethodRenderer = (row = { data: {} }) => {
    const rowData = row.data ? row.data : { data: {} };
    let billing = billingMethodData().find(b => b.value == rowData.billingMethod);
    const billingDetail = billing && rowData.billingType == 1 ? billing.label : '-';
    return (
      <div className={`${classes.taskTitleTextCnt} wrapText`} title={billingDetail}>
        <span className={classes.taskTitle}>
          {billingDetail}
        </span>
      </div>
    );
  };
  const DateRenderer = row => {
    const rowData = row.data || {};
    const formatedDate = moment(rowData.createdDate).format("MMM DD, YY");
    return formatedDate;
  };

  const ProgressRenderer = row => {
    const rowData = row.data || {};

    const overdue = helper.RETURN_OVER_DUE_DAYS_WITH_PROGRESS(
      rowData.actualDueDateString || "",
      rowData.progress || 0
    );

    return (
      <div style={{ display: "flex", alignItems: "center" }}>
        <div style={{ width: 30, height: 30, position: "relative" }}>
          <Circle
            percent={rowData.progress}
            strokeWidth="10"
            trailWidth=""
            trailColor="#dedede"
            strokeColor={overdue ? "#de133e" : "#30d56e"}
          />
          <Typography
            variant="h6"
            align="center"
            style={{
              position: "absolute",
              top: "50%",
              left: "50%",
              fontSize: 10,
              transform: "translate(-50%, -50%)",
            }}>
            {rowData.progress}
          </Typography>
        </div>
      </div>
    );
  };

  const CommentsRenderer = row => {
    const rowData = row.data || {};

    return (
      <div className={classes.commentsCell}>
        <SvgIcon
          viewBox="-1 2 24 21"
          className={`${classes.commentsIcon} ${rowData.totalUnreadComment ? classes.commentsIconColorRed : classes.commentsIconColorDim
            }`}>
          <ToggleUpdateIcon />
        </SvgIcon>
        <Typography
          variant="body2"
          align="center"
          className={`${rowData.totalUnreadComment ? classes.commentsIconColorRed : classes.commentsIconColorDim
            }`}>
          {rowData.comments ? rowData.comments : "-"}
        </Typography>
      </div>
    );
  };

  const AttachmentsRenderer = row => {
    const rowData = row.data || {};

    return (
      <div className={classes.commentsCell}>
        <AttachmentIcon classes={{ root: classes.attachmentIcon }} />
        {rowData.totalAttachment == 0 ? "-" : rowData.totalAttachment}
      </div>
    );
  };

  const ColorRenderer = row => {
    const rowData = row.data || {};

    return (
      <span
        style={{ background: rowData.colorCode || "transparent" }}
        className={classes.taskColor}></span>
    );
  };

  const isExternalFilterPresent = () => {
    return true;
  };

  const MeetingRenderer = row => {
    const rowData = row.data || {};

    return (
      <div className={classes.countsCnt}>
        <SvgIcon
          viewBox="0 0 15 16.667"
          classes={{ root: classes.meetingIcon }}
          htmlColor={theme.palette.icon.gray400}>
          <MeetingsIcon variant={"outlined"} />
        </SvgIcon>
        <Typography variant="body2" align="center" className={classes.countsText}>
          {rowData.meetings ? rowData.meetings : "-"}
        </Typography>
      </div>
    );
  };

  const IssuesRenderer = row => {
    const rowData = row.data || {};

    return (
      <div className={classes.countsCnt}>
        <SvgIcon
          viewBox="0 0 12.914 16.796"
          classes={{ root: classes.issueIcon }}
          htmlColor={theme.palette.icon.gray400}>
          <IssueIcon variant="outlined" />
        </SvgIcon>
        <Typography variant="body2" align="center" className={classes.countsText}>
          {rowData.issues ? rowData.issues : "-"}
        </Typography>
      </div>
    );
  };

  const TasksRenderer = row => {
    const rowData = row.data || {};
    return (
      <div className={classes.countsCnt}>
        <SvgIcon classes={{ root: classes.taskIcon }}
          htmlColor={theme.palette.icon.gray400} viewBox="0 0 14 15.556">
          <IconTasks />
        </SvgIcon>
        <Typography variant="body2" align="center" className={classes.countsText}>
          {rowData.tasks ? rowData.tasks : "-"}
        </Typography>
      </div>
    );
  };

  const RisksRenderer = row => {
    const rowData = row.data || {};

    return (
      <div className={classes.countsCnt}>
        <SvgIcon
          viewBox="0 0 17.355 15"
          classes={{ root: classes.riskIcon }}
          htmlColor={theme.palette.icon.gray400}>
          <RiskIcon variant={"outlined"} />
        </SvgIcon>
        <Typography variant="body2" align="center" className={classes.countsText}>
          {rowData.risks ? rowData.risks : "-"}
        </Typography>
      </div>
    );
  };

  //Clear selection if archived view is selected
  useEffect(() => {
    if (quickFilters && quickFilters.Archived) {
      grid.grid && grid.grid.deselectAll();
    }
  }, [quickFilters]);

  useEffect(() => {
    return () => {
      const { docked, fullView } = projectDetailsDialog;
      projectDetails(
        {
          projectDetails: {},
          fullView: fullView ? true : false,
          docked: docked ? true : false,
          dialog: false,
        },
        dispatch
      );

    }
  }, [])

  const handleProjectSelection = projects => {
    setSelectedProjects(projects);
  };

  const handleClearSelection = () => {
    setSelectedProjects([]);
    grid.grid && grid.grid.deselectAll();
  };
  // export project api function
  const handleExportProject = props => {
    const selectedNodes = grid.grid.getSelectedNodes();
    const selectedNodesLength = selectedNodes?.length || 1;
    const postObj = selectedNodes.length
      ? { projectIds: selectedNodes.map(i => i.data.projectId) }
      : { projectIds: [props.node.data.projectId] };

    const obj = {
      type: 'projects',
      apiType: 'post',
      data: postObj,
      fileName: 'projects.xlsx',
      apiEndpoint: 'api/export/bulkproject',
    }
    addPendingHandler(obj, dispatch);

    // exportBulkProject(postObj, dispatch, res => {
    //   fileDownload(res.data, "projects.xlsx");
    //   showSnackBar(`${selectedNodesLength} projects exported successfully`, "success");
    // },
    //   (err) => {
    //     if (err.response.status == 405) {
    //       showSnackBar("You don't have sufficient rights to export project", "error");
    //     } else {
    //       showSnackBar("oops! There seems to be an issue, please contact support@ntaskmanager.com", "error");
    //     }
    //   });
  };
  // export all project api function
  const handleExportAllProjects = props => {
    let allNodes = [];
    grid.grid.forEachNodeAfterFilter(node => allNodes.push(node.data));
    const postObj = allNodes.length && { projectIds: allNodes.map(i => i.projectId) };

    const obj = {
      type: 'projects',
      apiType: 'post',
      data: postObj,
      fileName: 'projects.xlsx',
      apiEndpoint: 'api/export/bulkproject',
    }
    addPendingHandler(obj, dispatch);

    // exportBulkProject(postObj, dispatch, res => {
    //   fileDownload(res.data, "projects.xlsx");
    //   showSnackBar(`${allNodes.length} projects exported successfully`, "success");
    // },
    //   (err) => {
    //     if (err.response.status == 405) {
    //       showSnackBar("You don't have sufficient rights to export projects", "error");
    //     } else {
    //       showSnackBar("oops! There seems to be an issue, please contact support@ntaskmanager.com", "error");
    //     }
    //   });
  };
  const getContextMenuItems = useCallback(params => {
    const selectedNodes = grid.grid.getSelectedNodes();
    let result = [
      {
        // custom item
        name: selectedNodes.length ? "Export Selected Project(s)" : "Export Project",
        action: () => handleExportProject(params),
        // cssClasses: ['redFont', 'bold'],
      },
      {
        // custom item
        name: "Export all Projects",
        action: () => handleExportAllProjects(params),
        // cssClasses: ['redFont', 'bold'],
      },
    ];
    return result;
  }, []);
  //Open task details on tasks row click
  const handleProjectRowClick = row => {
    const isRowClicked = row.event.target.closest("*[data-rowClick='cell']");
    if (isRowClicked) return;

    const {
      projectPermission: { permission },
    } = row.data;
    const { docked, fullView } = projectDetailsDialog;
    // const isRowClicked = row.event.target.closest("*[data-rowClick='cell']");
    // if (isRowClicked) return;
    if (quickFilters && quickFilters.Archived) {
      console.log('quickFilter');
    }
    if (!loadingProjectDetails && permission.project.projectDetails.cando && quickFilters && !quickFilters.Archived) {
      projectDetails(
        {
          projectDetails: row.data,
          fullView: fullView ? true : false,
          docked: docked ? true : false,
          dialog: true,
        },
        dispatch
      );
    }
  };

  const handleUpdateCustomField = (
    rowData,
    option,
    obj,
    settings,
    succ = () => { },
    fail = () => { }
  ) => {
    const projectCopy = cloneDeep(rowData);

    if (obj.fieldType == "matrix") {
      const selectedField =
        projectCopy.customFieldData &&
        projectCopy.customFieldData.find(x => x.fieldId == obj.fieldId);
      const isExist =
        selectedField &&
        selectedField.fieldData.data.findIndex(f => f.cellName == option.cellName) > -1;
      if (isExist === true) return;
    }
    const { fieldId } = obj;
    const newObj = {
      groupType: "project",
      groupId: projectCopy.projectId,
      fieldType: obj.fieldType,
      fieldId,
      fieldData: { data: option },
    };
    if (obj.fieldType === "dropdown") {
      if (isArray(newObj.fieldData.data))
        newObj.fieldData.data = newObj.fieldData.data.map(opt => {
          return { id: opt.id };
        });
      else newObj.fieldData.data = { id: newObj.fieldData.data.id };
    }
    if (obj.fieldType === "matrix")
      newObj.fieldData.data = newObj.fieldData.data.map(opt => {
        return { cellName: opt.cellName };
      });

    updateProjectCf(
      newObj,
      res => {
        succ();
        const resObj = res.data.entity[0];
        let customFieldsArr = [];
        // Updating Global state
        const isExist = projectCopy.customFieldData
          ? projectCopy.customFieldData.findIndex(c => c.fieldId === resObj.fieldId) > -1
          : false; /** if new risk created, its customFields are null so in null case scenario , compile the normal false flow, add custom field in risk object and save it */
        if (isExist) {
          customFieldsArr = projectCopy.customFieldData.map(c => {
            return c.fieldId === resObj.fieldId ? resObj : c;
          });
        } else {
          customFieldsArr = projectCopy.customFieldData
            ? [...projectCopy.customFieldData, resObj]
            : [
              resObj,
            ]; /** add custom field in risk object and save it, if newly created risk because its custom field is already null */
        }
        let newProjectObj = { ...projectCopy, customFieldData: customFieldsArr };
        if (grid.grid) {
          const rowNode = grid.grid.getRowNode(newProjectObj.id);
          rowNode?.setData(newProjectObj);
        }
        dispatchProject(newProjectObj, dispatch);
      },
      () => {
        fail();
      },
      dispatch
    );
  };

  const CustomfieldRendere = row => {
    /** custom fields columns rendere */
    const obj = {
      fieldId: row.colDef.fieldId,
      fieldType: row.colDef.fieldType,
      fieldData: { data: null },
    };
    const rowData = row.data || {};

    const data = rowData.customFieldData
      ? rowData.customFieldData.find(cf => cf.fieldId === row.colDef.fieldId) || obj
      : obj;

    let projectPermission = rowData?.projectId
      ? permissionObject[rowData.projectId] ?
        permissionObject[rowData.projectId].project
        : workspaceProjectPer
      : workspaceProjectPer;

    const canUpdateField =
      projectPermission && !rowData.isDeleted
        ? projectPermission.projectDetails.isUpdateField.cando
        : false;
    return (
      <CustomFieldRenderer
        field={data}
        fieldType={row.colDef.fieldType}
        rowData={rowData}
        handleUpdateCustomField={handleUpdateCustomField}
        permission={canUpdateField}
        groupType={"project"}
        placeholder={false}
      />
    );
  };

  const TaskActionDropdownRenderer = row => {
    const rowData = row.data || {};
    let projectPermission = rowData?.projectId
      ? permissionObject[rowData.projectId] ?
        permissionObject[rowData.projectId].project
        : workspaceProjectPer
      : workspaceProjectPer;
    return (
      <ProjectActionDropdown
        btnProps={{ style: { width: 37 } }}
        data={rowData}
        handleActivityLog={() => { }}
        handleCloseCallBack={() => { }}
        projectPermission={projectPermission}
        isArchived={rowData.isDeleted}
      />
    );
  };

  //Snackbar function
  const showSnackBar = (snackBarMessage = "", type = null) => {
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "top",
          horizontal: "centerdp",
        },
        variant: type ? type : "info",
      }
    );
  };

  const projectTitleColumn =
    (projectColumns && projectColumns.find(c => c.columnKey == "projectName")) || {};

  let headProps = headerProps;

  const nonSectionsFieldskeys = nonSectionFields
    .filter(
      item =>
        // item.fieldType == "textarea" ||
        // item.fieldType == "formula" ||
        // item.fieldType == "filesAndMedia" ||
        // item.fieldType == "location" ||
        (item.fieldType == "dropdown" && item.settings.multiSelect) ||
        (item.fieldType == "people" && item.settings.multiplePeople)
    )
    .map(nsf => nsf.columnKey);

  const sectionsFieldskeys = sections.reduce((res, cv) => {
    let keys = cv.fields
      .filter(
        item =>
          // item.fieldType == "textarea" ||
          // item.fieldType == "formula" ||
          // item.fieldType == "filesAndMedia" ||
          // item.fieldType == "location" ||
          (item.fieldType == "dropdown" && item.settings.multiSelect) ||
          (item.fieldType == "people" && item.settings.multiplePeople)
      )
      .map(cv => cv.columnKey);
    res = [...res, ...keys];
    return res;
  }, []);

  headProps.columnGroupingDisabled = [
    ...headProps.columnGroupingDisabled,
    ...nonSectionsFieldskeys,
    ...sectionsFieldskeys,
  ];
  headProps.columnSortingDisabled = [
    ...headProps.columnSortingDisabled,
    ...nonSectionsFieldskeys,
    ...sectionsFieldskeys,
  ];

  const handleChangeGrouping = value => {
    let sectionGroup = localStorage.getItem("sectiongrouping");
    if (sectionGroup == "true") localStorage.setItem("sectiongrouping", value);
    else {
      localStorage.setItem("sectiongrouping", value);
    }
    setSectionGrouping(value);
  };

  const RenderColumnsWithoutSectionGrouping = () => {
    const systemColumns = projectColumns
      .map((c, i) => {
        let filteredkey = c.columnKey === "estimated Time" ? "estimatedTime" : c.columnKey === "approved Time" ? "approvedTime" : c.columnKey;
        const defaultColDef = projectColumnDefs(classes)[filteredkey];
        const {
          hide,
          pinned,
          rowGroup,
          rowGroupIndex,
          sort,
          sortIndex,
          width,
          wrapText,
          autoHeight,
          position,
        } = c;
        return (
          c.columnKey !== "projectName" &&
          c.isSystem && (
            <AgGridColumn
              key={c.id}
              headerName={c.columnName}
              field={c.columnKey}
              hide={hide}
              suppressKeyboardEvent={true}
              pinned={pinned}
              align="left"
              rowGroup={rowGroup}
              sort={sort}
              sortIndex={sortIndex == -1 ? null : sortIndex}
              wrapText={wrapText}
              rowGroupIndex={rowGroup && parseFloat(rowGroupIndex)}
              autoHeight={autoHeight}
              width={width || (defaultColDef && defaultColDef.minWidth)}
              cellStyle={{ justifyContent: "center" }}
              position={position}
              resizable={defaultColDef ? defaultColDef.resizable : true}
              {...defaultColDef}
            />
          )
        );
      })
      .filter(c => c !== false);
    const nonSectionColumns = nonSectionFields.map((c, i) => {
      const defaultColDef = projectColumnDefs(classes)[c.fieldType];
      const { hide, pinned, rowGroup, sort, sortIndex, width, wrapText, autoHeight, position } = c;
      return (
        <AgGridColumn
          key={c.id}
          headerName={c.columnName}
          field={c.columnKey}
          hide={hide}
          pinned={pinned}
          align="left"
          fieldType={c.fieldType}
          rowGroup={rowGroup}
          suppressKeyboardEvent={true}
          sort={sort}
          sortIndex={sortIndex == -1 ? null : sortIndex}
          wrapText={wrapText}
          autoHeight={autoHeight}
          fieldId={c.fieldId}
          resizable={defaultColDef ? defaultColDef.resizable : true}
          position={position}
          width={width || (defaultColDef && defaultColDef.minWidth)}
          cellStyle={{
            justifyContent:
              c.fieldType === "dropdown" || c.fieldType === "filesAndMedia"
                ? "flex-start"
                : "center",
          }}
          {...defaultColDef}
        />
      );
    });
    const sectionColumns = sections
      .map(section => {
        return (
          section.fields.length > 0 &&
          section.fields.map((c, i) => {
            const defaultColDef = projectColumnDefs(classes)[c.fieldType];
            const {
              hide,
              pinned,
              rowGroup,
              sort,
              sortIndex,
              width,
              wrapText,
              autoHeight,
              position,
            } = c;
            return (
              <AgGridColumn
                key={c.id}
                headerName={c.columnName}
                field={c.columnKey}
                hide={hide}
                pinned={pinned}
                align="left"
                suppressKeyboardEvent={true}
                position={position}
                rowGroup={rowGroup}
                fieldType={c.fieldType}
                sort={sort}
                resizable={defaultColDef ? defaultColDef.resizable : true}
                sortIndex={sortIndex == -1 ? null : sortIndex}
                wrapText={wrapText}
                autoHeight={autoHeight}
                width={width || (defaultColDef && defaultColDef.minWidth)}
                fieldId={c.fieldId}
                cellStyle={{
                  justifyContent:
                    c.fieldType === "dropdown" || c.fieldType === "filesAndMedia"
                      ? "flex-start"
                      : "center",
                }}
                {...defaultColDef}
              />
            );
          })
        );
      })
      .filter(c => c !== false);
    const spreadSectionCols = sectionColumns.reduce((r, cv) => {
      r = [...r, ...cv];
      return r;
    }, []);
    const allColumns = [...systemColumns, ...nonSectionColumns, ...spreadSectionCols];
    const sortedColumns = allColumns.sort((pv, cv) => {
      return pv.props.position - cv.props.position;
    });
    return sortedColumns;
  };

  return (
    <>
      {selectedProjects.length >= 1 ? (
        <Projectcmp selectedProjects={selectedProjects} clearSelection={handleClearSelection} />
      ) : null}
      {projects.length == 0 ? (
        <div className={classes.emptyContainer} style={style}>
          <EmptyState
            screenType="project"
            heading={
              <FormattedMessage
                id="common.create-first.project.label"
                defaultMessage="Create your first Project"
              />
            }
            message={
              <FormattedMessage
                id="common.create-first.project.message"
                defaultMessage='You do not have any projects yet. Press "Alt + P" or click on button below.'
              />
            }
            button={workspaceProjectPer.createProject.cando}
          />
        </div>
      ) : (
        <div className={classes.taskListViewCnt} style={style}>
          <CustomTable
            columns={projectColumns}
            idKey={"projectId"}
            defaultColDef={{ lockPinned: true }}
            isExternalFilterPresent={isExternalFilterPresent}
            doesExternalFilterPass={doesFilterPass}
            onSelectionChanged={handleProjectSelection}
            frameworkComponents={{
              taskActionCellRenderer: TaskActionDropdownRenderer,
              statusDropdown: StatusDropdownCmp,
              projectTitleCmp: ProjectTitleCellRenderer,
              projectManager: ProjectManagerRenderer,
              resources: ResourcesDropdownRenderer,
              groupRowInnerRenderer: GroupRowInnerRenderer,
              createdDateRenderer: DateRenderer,
              createdByRenderer: CreatedByRenderer,
              updatedByRenderer: UpdatedByRenderer,
              projectStatusRenderer: ProjectStatusRenderer,
              billingMethodRenderer: BillingMethodRenderer,
              updatedDateCmp: UpdatedDateRenderer,
              progressRenderer: ProgressRenderer,
              comments: CommentsRenderer,
              documents: AttachmentsRenderer,
              meetings: MeetingRenderer,
              issues: IssuesRenderer,
              tasks: TasksRenderer,
              risks: RisksRenderer,
              color: ColorRenderer,
              textfield: CustomfieldRendere,
              textarea: CustomfieldRendere,
              location: CustomfieldRendere,
              country: CustomfieldRendere,
              number: CustomfieldRendere,
              money: CustomfieldRendere,
              email: CustomfieldRendere,
              websiteurl: CustomfieldRendere,
              date: CustomfieldRendere,
              phone: CustomfieldRendere,
              rating: CustomfieldRendere,
              formula: CustomfieldRendere,
              people: CustomfieldRendere,
              dropdown: CustomfieldRendere,
              filesAndMedia: CustomfieldRendere,
            }}
            data={projects}
            selectedProjects={selectedProjects}
            type="project"
            onRowGroupChange={(obj, key) => updateProject(obj, { [key]: obj[key] })}
            createableProps={{
              placeholder: "Enter title for new project",
              id: "quickAddProject",
              btnText: "Add new Project",
              addAction: handleAddProject,
              addHelpTask: "Press enter to add new Project",
              emptyErrorMessage: 'Please Enter Project title',
              disabled: !workspaceProjectPer.createProject.cando,
            }}
            headerProps={headProps}
            gridProps={{
              getContextMenuItems: getContextMenuItems,
              groupDisplayType: "groupRows",
              onRowClicked: handleProjectRowClick,
              reactUi: true,
              // groupIncludeFooter: true,
              // groupIncludeTotalFooter: true,
              maintainColumnOrder: true,
              // autoGroupColumnDef: { minWidth: 300 },
            }}>
            {/*<AgGridColumn headerClass={classes.taskTitleField} >*/}
            <AgGridColumn
              headerName=""
              field="colorCode"
              cellRenderer="color"
              filter={false}
              cellClass={classes.taskColorCell}
              maxWidth={6}
              resizable={false}
              pinned="left"
              lockPosition={true}
              suppressMovable={true}
            />
            <AgGridColumn
              headerName={projectTitleColumn.columnName}
              field={projectTitleColumn.columnKey}
              resizable={true}
              suppressMovable={true}
              checkboxSelection={teamCanView("bulkActionAccess")}
              // rowDrag={false}
              sortable={true}
              filter={true}
              lockPosition={true}
              wrapText={projectTitleColumn.wrapText}
              cellRenderer={"projectTitleCmp"}
              cellClass={`${classes.taskTitleCell} ag-textAlignLeft`}
              pinnedRowCellRenderer="customPinnedRowRenderer"
              pinned={"left"}
              autoHeight={projectTitleColumn.autoHeight}
              width={projectTitleColumn.width || 300}
              minWidth={200}
              align="left"
              sort={projectTitleColumn.sort}
              sortIndex={projectTitleColumn.sortIndex == -1 ? null : projectTitleColumn.sortIndex}
              cellStyle={{ padding: 0 }}
              rowDrag={true}
              comparator={sortAlphabetically}
            />
            {/*<AgGridColumn*/}
            {/*  headerName=""*/}
            {/*  field="drag"*/}
            {/*  suppressMovable={true}*/}
            {/*  filter={false}*/}
            {/*  */}
            {/*  cellClass={classes.taskDragCell}*/}
            {/*  maxWidth={20}*/}

            {/*  pinned={"left"}*/}
            {/*  // rowDragText={(params) => {*/}
            {/*  //   return params.rowNode.data.taskTitle*/}
            {/*  // }}*/}
            {/*/>*/}

            <AgGridColumn
              headerName=""
              field="recurrence"
              suppressMovable={true}
              cellRenderer="recurrence"
              cellClass={classes.taskRecurrenceCell}
              filter={false}
              minWidth={22}
              maxWidth={22}
              pinned={"left"}
            />
            {/*</AgGridColumn>*/}
            {!sectionGroup || sectionGroup == "false"
              ? RenderColumnsWithoutSectionGrouping()
              : null}

            {sectionGroup == "true" && (
              <AgGridColumn
                visible={false}
                headerName="System Fields"
                headerClass={classes.systemFieldGroup}>
                {projectColumns.map((c, i) => {
                  let filteredkey = c.columnKey === "estimated Time" ? "estimatedTime" : c.columnKey === "approved Time" ? "approvedTime" : c.columnKey;
                  const defaultColDef = projectColumnDefs(classes)[filteredkey];
                  const {
                    hide,
                    pinned,
                    rowGroup,
                    rowGroupIndex,
                    sort,
                    sortIndex,
                    width,
                    wrapText,
                    autoHeight,
                  } = c;
                  return (
                    c.columnKey !== "projectName" &&
                    c.isSystem && (
                      <AgGridColumn
                        key={c.id}
                        headerName={c.columnName}
                        field={c.columnKey}
                        hide={hide}
                        pinned={pinned}
                        align="left"
                        rowGroup={rowGroup}
                        sort={sort}
                        sortIndex={sortIndex == -1 ? null : sortIndex}
                        wrapText={wrapText}
                        rowGroupIndex={rowGroup && parseFloat(rowGroupIndex)}

                        autoHeight={autoHeight}
                        width={width || (defaultColDef && defaultColDef.minWidth)}
                        cellStyle={{ justifyContent: "center" }}
                        resizable={defaultColDef ? defaultColDef.resizable : true}
                        {...defaultColDef}
                      />
                    )
                  );
                })}
              </AgGridColumn>
            )}
            {sectionGroup == "true" &&
              sections.map(section => {
                return (
                  section.fields.length > 0 && (
                    <AgGridColumn
                      headerName={section.columnName}
                      resizable={false}
                      headerGroupComponent="customHeaderGroupComponent"
                      color={section.color}
                      headerClass={classes.sectionFieldsGroup}>
                      {section.fields.map((c, i) => {
                        const defaultColDef = projectColumnDefs(classes)[c.fieldType];
                        const {
                          hide,
                          pinned,
                          rowGroup,
                          sort,
                          sortIndex,
                          width,
                          wrapText,
                          autoHeight,
                        } = c;
                        return (
                          <AgGridColumn
                            key={c.id}
                            headerName={c.columnName}
                            field={c.columnKey}
                            suppressKeyboardEvent={true}
                            hide={hide}
                            pinned={pinned}
                            align="left"
                            rowGroup={rowGroup}
                            fieldType={c.fieldType}
                            sort={sort}
                            sortIndex={sortIndex == -1 ? null : sortIndex}
                            resizable={defaultColDef ? defaultColDef.resizable : true}
                            wrapText={wrapText}
                            autoHeight={autoHeight}
                            width={width || (defaultColDef && defaultColDef.minWidth)}
                            fieldId={c.fieldId}
                            cellStyle={{
                              justifyContent:
                                c.fieldType === "dropdown" || c.fieldType === "filesAndMedia"
                                  ? "flex-start"
                                  : "center",
                            }}
                            {...defaultColDef}
                          />
                        );
                      })}
                    </AgGridColumn>
                  )
                );
              })}
            {sectionGroup == "true" && nonSectionFields.length ? (
              <AgGridColumn resizable={false} headerClass={classes.taskTitleField}>
                {nonSectionFields.map((c, i) => {
                  const defaultColDef = projectColumnDefs(classes)[c.fieldType];
                  const {
                    hide,
                    pinned,
                    rowGroup,
                    sort,
                    sortIndex,
                    width,
                    wrapText,
                    autoHeight,
                  } = c;
                  return (
                    <AgGridColumn
                      key={c.id}
                      headerName={c.columnName}
                      field={c.columnKey}
                      hide={hide}
                      suppressKeyboardEvent={true}
                      pinned={pinned}
                      align="left"
                      fieldType={c.fieldType}
                      rowGroup={rowGroup}
                      sort={sort}
                      sortIndex={sortIndex == -1 ? null : sortIndex}
                      wrapText={wrapText}
                      autoHeight={autoHeight}
                      resizable={defaultColDef ? defaultColDef.resizable : true}
                      fieldId={c.fieldId}
                      width={width || (defaultColDef && defaultColDef.minWidth)}
                      cellStyle={{
                        justifyContent:
                          c.fieldType === "dropdown" || c.fieldType === "filesAndMedia"
                            ? "flex-start"
                            : "center",
                      }}
                      {...defaultColDef}
                    />
                  );
                })}
              </AgGridColumn>
            ) : null}

            <AgGridColumn
              headerName={""}
              field={"columnDropdown"}
              resizable={false}
              sortable={false}
              lockPinned={true}
              filter={false}
              width={40}
              headerClass={"columnSelectHeader"}
              pinned={"right"}
              suppressMovable={true}
              cellRenderer={"taskActionCellRenderer"}
              cellStyle={{ padding: 0, textAlign: "center" }}
            />
          </CustomTable>
          <ProjectFilter
            sectionGrouping={sectionGrouping}
            handleChangeGrouping={handleChangeGrouping}
          />
          <ActionConfirmation
            open={statusCompleteDialogOpen}
            closeAction={projectCompleteConfirmClose}
            cancelBtnText={<FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />}
            successBtnText={"Yes, Mark All Complete"}
            alignment="center"
            headingText={"Mark All Tasks Complete"}
            iconType="markAll"
            msgText={'Are you sure you want to mark all project tasks as completed'}
            successAction={handleMarkProjectAsCompleted}
            btnQuery={statusCompleteBtnQuery}
          />
        </div>
      )}
    </>
  );
}, areEqual);

function areEqual(prevProps, nextProps) {
  return isEqual(prevProps, nextProps);
  /*
  return true if passing nextProps to render would return
  the same result as passing prevProps to render,
  otherwise return false
  */
}

const GroupRowInnerRenderer = props => {
  return <GroupByComponents data={props} feature="project" />;
};

export default compose(
  injectIntl,
  withSnackbar,
  withStyles(projectListStyles, { withTheme: true })
)(ProjectList);
