import React, { Component, Fragment } from "react";
import Grid from "@material-ui/core/Grid";
import Hotkeys from "react-hot-keys";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import CustomDialog from "../../../components/Dialog/CustomDialog";
import autoCompleteStyles from "../../../assets/jss/components/autoComplete";
import combineStyles from "../../../utils/mergeStyles";
import DefaultButton from "../../../components/Buttons/DefaultButton";
import CustomButton from "../../../components/Buttons/CustomButton";
import dialogFormStyles from "../../../assets/jss/components/dialogForm";
import FormControl from "@material-ui/core/FormControl";
import DefaultTextField from "../../../components/Form/TextField";
import { withRouter } from "react-router-dom";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormLabel from "@material-ui/core/FormLabel";
import {
  SaveProject,
  FetchProjectsInfo,
} from "../../../redux/actions/projects";
import helper from "../../../helper";
import { FormattedMessage, injectIntl } from "react-intl";

class EditProjectForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "ganttView",
      projectTitle: "",
      projectTitleError: false,
      projectTitleErrorMessage: "",
      nameChanged: true,
      btnQuery: "",
    };
  }

  componentDidUpdate(prevProps) {
    const { open, project } = this.props;
    if (open !== prevProps.open) {
      this.setState({ projectTitle: project.projectName });
    }
  }

  closeDialog = (e) => {
    e.stopPropagation();
    this.props.closeAction();
  };

  handleTextFieldChange = (event) => {
    const value = event.target.value;
    this.setState({
      projectTitle: value,
      projectTitleError: false,
      projectTitleErrorMessage: "",
      nameChanged: value.trim() === this.props.project.projectName,
    });
  };

  validate = () => {
    let errorCheck_projectTitle = true;
    let errorCheck_projectTaskPrefix = true;
    if (helper.HELPER_EMPTY("Project Title", this.state.projectTitle)) {
      this.setState({
        projectTitleError: true,
        projectTitleErrorMessage: this.props.intl.formatMessage({id:"project.creation-dialog.validation.required",defaultMessage:"Project title is required."}),
      });
      errorCheck_projectTitle = false;
    } else {
      let checkMessage = helper.HELPER_CHARLIMIT80(
        "Project Title",
        this.state.projectTitle
      );
      if (checkMessage) {
        this.setState({
          projectTitleError: true,
          projectTitleErrorMessage: checkMessage,
        });
        errorCheck_projectTitle = false;
      }
    }
    if (!helper.HELPER_EMPTY("Project Title", this.state.projectTaskPrefix)) {
    }
    return errorCheck_projectTitle;
  };

  handleSubmit = (e) => {
    if (e) e.stopPropagation();
    if (this.validate()) {
      this.setState({ btnQuery: "progress" });
      this.props.EditProject(
        {
          projectId: this.props.project.projectId,
          editProjectName: this.state.projectTitle,
        },
        (response) => {
          if (response && response.status === 200) {
            this.setState({
              btnQuery: "",
              projectTitleError: false,
              projectTitleErrorMessage: "",
            });
            this.props.closeAction();
          }
        },
        fail => {
          if (fail.status === 409) {
            this.setState({ btnQuery: "", projectTitleError: true, projectTitleErrorMessage: "The project name is already exists"});
          }else{
            this.setState({btnQuery: ""});
            this.props.showSnackBar(fail.data.message, "error");
          }
        }
      );
    }
  };

  onKeyDown = (keyName, e, handle) => {
    if (keyName === "enter") {
      const { nameChanged } = this.state;
      if (this.state.btnQuery === "" && !nameChanged) this.handleSubmit();
    }
  };

  render() {
    const { classes, theme, open, closeAction } = this.props;
    const {
      projectTitleError,
      projectTitleErrorMessage,
      projectTitle,
      btnQuery,
      nameChanged,
    } = this.state;

    return (
      <Fragment>
        <CustomDialog
          title={
            <FormattedMessage
              id="project.rename.label"
              defaultMessage="Rename Project"
            />
          }
          dialogProps={{
            open: open,
            onClose: closeAction,
            onClick: (e) => e.stopPropagation(),
          }}
        >
          <div className={classes.dialogFormCnt} noValidate autoComplete="off">
            <Hotkeys keyName="enter" onKeyDown={this.onKeyDown}>
              <DefaultTextField
                label={
                  <FormattedMessage
                    id="project.creation-dialog.project-title.label"
                    defaultMessage="Project Title"
                  />
                }
                errorState={projectTitleError}
                errorMessage={projectTitleErrorMessage}
                defaultProps={{
                  type: "text",
                  id: "projectTitle",
                  placeholder: this.props.intl.formatMessage({
                    id: "project.creation-dialog.project-title.palceholder",
                    defaultMessage: "Add Project Title",
                  }),
                  value: projectTitle,
                  autoFocus: true,
                  inputProps: { maxLength: 80 },
                  onChange: (event) => {
                    this.handleTextFieldChange(event, "projectTitle");
                  },
                }}
              />
            </Hotkeys>
          </div>
          <Grid
            container
            direction="row"
            justify="flex-end"
            alignItems="center"
            classes={{ container: classes.dialogFormActionsCnt }}
          >
            <DefaultButton
              onClick={(e) => this.closeDialog(e)}
              text={
                <FormattedMessage
                  id="common.action.cancel.label"
                  defaultMessage="Cancel"
                />
              }
              buttonType="Transparent"
              style={{ marginRight: 20 }}
            />
            <CustomButton
              onClick={(e) => this.handleSubmit(e)}
              btnType="success"
              variant="contained"
              query={btnQuery}
              disabled={btnQuery == "progress" || nameChanged}
            >
              <FormattedMessage
                id="common.action.save.label"
                defaultMessage="Save"
              />
            </CustomButton>
          </Grid>
        </CustomDialog>
      </Fragment>
    );
  }
}

export default compose(
  withRouter,
  injectIntl,
  withStyles(combineStyles(autoCompleteStyles, dialogFormStyles), {
    withTheme: true,
  })
)(EditProjectForm);
