import React from "react";
import moment from "moment";
import { store } from "../../../index";
import { generateNumberValue } from "../../../helper/generateNumberFieldData";

import {
  currencyData,
  billingMethodData,
  feeMethodData,
} from "../../../helper/projectDropdownData";

import helper from "../../../helper";
import { handleCalculateValue } from "../../../helper/listView/listView.helper";
let createdDateValueGetter = function (params) {
  return params.data && params.data.createdDate
    ? moment(params.data.createdDate).format("MMM DD, YYYY")
    : "";
};
let startDateValueGetter = function (params) {
  return params.data && params.data.startDate
    ? moment(params.data.startDate).format("MMM DD, YYYY")
    : "";
};
let dueDateValueGetter = function (params) {
  return params.data && params.data.dueDate
    ? moment(params.data.dueDate).format("MMM DD, YYYY")
    : "";
};
let actualStartDateValueGetter = function (params) {
  return params.data && params.data.actualStartDate
    ? moment(params.data.actualStartDate).format("MMM DD, YYYY")
    : "";
};
let updatedDateValueGetter = function (params) {
  return params.data && params.data.updatedDate
    ? moment(params.data.updatedDate).format("MMM DD, YYYY")
    : "";
};
let actualDueDateValueGetter = function (params) {
  return params.data && params.data.actualDueDate
    ? moment(params.data.actualDueDate).format("MMM DD, YYYY")
    : "";
};
let projectValueGetter = function (params) {
  return params.data && params.data.projectId && params.data.projectInfor
    ? params.data.projectInfor.projectName
    : "";
};

let numberFieldGetter = function (params) {
  const customFields = store.getState().customFields.data;
  if (!params.data) return "";
  let numberField = customFields.find(item => item.fieldId === params.colDef.fieldId);
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (numberField && rowData) {
    value = {
      unit: numberField.settings.unit,
      direction: numberField.settings.direction,
      number: generateNumberValue(rowData.fieldData.data, numberField),
    };
  }
  return value;
};

let numberFieldKeyCreator = function (params) {
  const customFields = store.getState().customFields.data;
  if (!params.data) return "";
  let numberField = customFields.find(item => item.fieldId === params.colDef.fieldId);
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (numberField && rowData) {
    value = `${numberField.settings.direction === "left" && numberField.settings.unit !== "none"
      ? numberField.settings.unit
      : ""
      } ${generateNumberValue(rowData.fieldData.data, numberField)} ${numberField.settings.direction === "right" && numberField.settings.unit !== "none"
        ? numberField.settings.unit
        : ""
      }`;
  }
  return value;
};
let moneyFieldGetter = function (params) {
  if (!params.data) return "";
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (rowData) {
    value = `${rowData.fieldData.data} `;
  }
  return value;
};

let countryFieldGetter = function (params) {
  if (!params.data) return "";
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (rowData) {
    value = rowData.fieldData.data;
  }
  return value;
};
let locationFieldGetter = function (params) {
  if (!params.data) return "";
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (rowData) {
    value = `${rowData.fieldData.data.lineOne} ${rowData.fieldData.data.lineTwo} ${rowData.fieldData.data.city} ${rowData.fieldData.data.province} ${rowData.fieldData.data.zipCode} ${rowData.fieldData.data.countryCode}`;
  }
  return value;
};
let ratingFieldGetter = function (params) {
  if (!params.data) return "";
  const customFields = store.getState().customFields.data;
  let ratingField = customFields.find(item => item.fieldId === params.colDef.fieldId);
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (rowData) {
    for (let i = 0; i < rowData.fieldData.data; i++) {
      value = `${value} ${ratingField.settings.emoji}`;
    }
  }
  return value;
};

let websiteurlFieldGetter = function (params) {
  if (!params.data) return "";
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (rowData) {
    value = rowData.fieldData.data;
  }
  return value;
};

let emailFieldGetter = function (params) {
  if (!params.data) return "";
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (rowData) {
    value = rowData.fieldData.data;
  }
  return value;
};

let phoneFieldGetter = function (params) {
  if (!params.data) return "";
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (rowData) {
    value = rowData.fieldData.data;
  }
  return value;
};
let fileMediaFieldGetter = function (params) {
  if (!params.data) return "";
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (rowData) {
    value = rowData.fieldData.data.map(item => item.fileName).join(",");
  }
  return value;
};
let dateValueGetter = function (params) {
  if (!params.data) return ''
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (rowData) {
    value = moment(rowData.fieldData.data.date).format("lll");
  }
  return value;
};

let textFieldGetter = function (params) {
  if (!params.data) return ''
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (rowData) {
    value = rowData.fieldData.data;
  }
  return value;
};

let dropdownFieldGetter = function (params) {
  if (!params.data) return ''
  let value = "";
  if (params.data) {
    const customFields = store.getState().customFields.data;
    let dropdownField = customFields.find(item => item.fieldId === params.colDef.fieldId);
    let rowData = params.data.customFieldData
      ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
      : null;
    if (rowData && !dropdownField.settings.multiSelect) {
      let dataValue = dropdownField.values.data.find(opt => opt.id === rowData.fieldData.data.id) || {};
      value = dataValue.value || "";
    }
    if (rowData && dropdownField.settings.multiSelect) {
      value = rowData.fieldData.data.map(item => item.value).join(",");
    }
  }
  return value;
};
let peopleFieldGetter = function (params) {
  if (!params.data) return "";
  let value = "";
  if (params.data) {
    const customFields = store.getState().customFields.data;
    const allMembers = store.getState().profile.data.member.allMembers;
    let peopleField = customFields.find(item => item.fieldId === params.colDef.fieldId);
    let rowData = params.data.customFieldData
      ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
      : null;
    if (rowData && !peopleField.settings.multiplePeople) {
      let contentValue = rowData.fieldData.data
        ? allMembers.find(m => rowData.fieldData.data.includes(m.userId)) || {}
        : "";
      value = contentValue.fullName || "";
    }
  }
  return value;
};
let formulaFieldGetter = function (params) {
  if (!params.data) return "";
  let rowData = params.data.customFieldData
    ? params.data.customFieldData.find(item => item.fieldId === params.colDef.fieldId)
    : null;
  let value = "";
  if (rowData) {
    value = value = handleCalculateValue(params.data.customFieldData, rowData.fieldData.data);
  }
  return value;
};
const estimatedTimeValueGetter = (params) => {
  return helper.CHANGETIMEFORMAT(params.data.estimatedTime)
}
const totalTimeLoggedValueGetter = (params) => {
  return helper.CHANGETIMEFORMAT(params.data.totalTimeLogged)
}
const approvedTimeValueGetter = (params) => {
  return helper.CHANGETIMEFORMAT(params.data.approvedTime)
}
const billingMethodValueGetter = (params) => {
  let billing = billingMethodData().find(b => b.value == params.data.billingMethod);
  return billing && params.data.billingType == 1 ? billing.label : '-';
}
const billingTypeValueGetter = (params) => {
  return params.data.billingType ? 'Billable' : 'Non-Billable';
}
const currencyValueGetter = (params) => {
  return params.data.billingType ? params.data.currency : '-';
}
const projectColumnDefs = classes => ({
  billingType: {
    resizable: true,
    minWidth: 160,
    valueGetter: billingTypeValueGetter
  },
  billingMethod: {
    resizable: true,
    minWidth: 140,
    valueGetter: billingMethodValueGetter,
    cellRenderer: "billingMethodRenderer"
  },
  estimatedTime: {
    resizable: true,
    minWidth: 150,
    valueGetter: estimatedTimeValueGetter
  },
  totalTimeLogged: {
    resizable: true,
    minWidth: 170,
    valueGetter: totalTimeLoggedValueGetter
  },
  approvedTime: {
    minWidth: 150,
    resizable: true,
    valueGetter: approvedTimeValueGetter
  },
  currency: {
    resizable: true,
    minWidth: 120,
    valueGetter: currencyValueGetter
  },
  uniqueId: {
    sortable: true,
    filter: true,
    minWidth: 100,
    resizable: true,
  },
  tasks: {
    width: 150,
    minWidth: 100,
    resizable: true,
    cellRenderer: "tasks",
  },
  projectName: {
    sortable: true,
    filter: true,
    resizable: true,
    width: 150,
    checkboxSelection: true,
    cellRenderer: "projectTitleCmp",
    cellClass: classes.taskTitleCell,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    rowDrag: true,
    comparator: sortAlphabetically,
  },
  status: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 150,
    cellRenderer: "statusDropdown",
    cellClass: classes.fullSpannedCell,
    valueGetter: "data.status",
    comparator: sortStatus,
  },
  projectStatusTemplate: {
    minWidth: 180,
    resizable: true,
    cellRenderer: "projectStatusRenderer",
  },
  progress: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 130,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "progressRenderer",
  },
  resources: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 200,
    pinnedRowCellRenderer: "customPinnedaRowRenderer",
    cellRenderer: "resources",
  },
  projectManager: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 200,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "projectManager",
  },
  documents: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 150,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "totalAttachment",
  },
  comments: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 150,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "comments",
  },
  meetings: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 150,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "meetings",
  },
  Issues: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 150,
    // pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "issues",
  },
  risks: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 150,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "risks",
  },
  createdDate: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 180,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "createdDateRenderer",
    comparator: dateComparator,
    valueGetter: createdDateValueGetter,
  },
  updatedDate: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 180,
    cellRenderer: "updatedDateCmp",
    comparator: dateComparator,
    valueGetter: updatedDateValueGetter,
  },
  createdBy: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 180,
    cellRenderer: "createdByRenderer",
    pinnedRowCellRenderer: "customPinnedRowRenderer",
  },
  updatedBy: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 180,
    cellRenderer: "updatedByRenderer",
    pinnedRowCellRenderer: "customPinnedRowRenderer",
  },
  milestones: {
    minWidth: 130,
    resizable: true,
  },
  pendingTasks: {
    minWidth: 160,
    resizable: true,
  },
  completedTasks: {
    minWidth: 160,
    resizable: true,
  },
  budget: {
    minWidth: 120,
    resizable: true,
  },
  textfield: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    valueGetter: textFieldGetter,
    cellClass: "ag-textAlignLeft",
    cellRenderer: "textfield",
    align: "left",
    comparator: sortAlphabetically
  },
  textarea: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "textarea",
    cellClass: "ag-textAlignLeft",
    valueGetter: "data.customFieldData",
  },
  location: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "location",
    valueGetter: locationFieldGetter,
    comparator: sortAlphabetically,
    cellClass: "ag-textAlignLeft",
  },
  country: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "country",
    valueGetter: countryFieldGetter,
    cellClass: "ag-textAlignLeft",
    comparator: sortAlphabetically
  },
  number: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "number",
    comparator: sortNumber,
    valueGetter: numberFieldGetter,
    keyCreator: numberFieldKeyCreator,
    cellClass: "ag-textAlignLeft",
  },
  money: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "money",
    comparator: sortMoney,
    valueGetter: moneyFieldGetter,
    cellClass: "ag-textAlignLeft",
  },
  email: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 220,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "email",
    comparator: sortAlphabetically,
    valueGetter: emailFieldGetter,
    cellClass: "ag-textAlignLeft",
  },
  websiteurl: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "websiteurl",
    valueGetter: websiteurlFieldGetter,
    comparator: sortAlphabetically,
    cellClass: "ag-textAlignLeft",
  },
  date: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 150,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "date",
    valueGetter: dateValueGetter,
    comparator: dateComparator,
    cellClass: "ag-textAlignLeft",
  },
  phone: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "phone",
    valueGetter: phoneFieldGetter,
    cellClass: "ag-textAlignLeft",
  },
  rating: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 200,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "rating",
    valueGetter: ratingFieldGetter,
  },
  formula: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "formula",
    comparator: sortFormula,
    valueGetter: formulaFieldGetter,
    cellClass: "ag-textAlignLeft",
  },
  people: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "people",
    valueGetter: peopleFieldGetter,
    comparator: sortAlphabetically,
  },
  dropdown: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "dropdown",
    valueGetter: dropdownFieldGetter,
    comparator: sortAlphabetically,
    cellClass: `${classes.fullSpannedCell} ag-textAlignLeft`,
  },
  filesAndMedia: {
    sortable: true,
    filter: true,
    resizable: true,
    minWidth: 120,
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    cellRenderer: "filesAndMedia",
    valueGetter: "data.customFieldData",
    valueGetter: fileMediaFieldGetter,
    comparator: sortAlphabetically,
    cellClass: "ag-textAlignLeft",
  },
});

function dateComparator(date1, date2) {
  const newDate = date1 ? new Date(date1) : "";
  const prevDate = date2 ? new Date(date2) : "";
  if (newDate == prevDate) return 0;

  return newDate - prevDate;
}
function sortStatus(prevStatus, nextStatus, prevRow, nextRow) {
  const prevStatusName =
    prevRow.data ? prevRow.data.status : "";
  const nextStatusName =
    nextRow.data ? nextRow.data.status : "";
  if (prevStatusName == nextStatusName) return 0;
  if (prevStatusName < nextStatusName) return -1;
  if (prevStatusName > nextStatusName) return 1;
}
function sortNumber(nextObj, prevObj, nextRow, prevRow) {
  const prevNumberVal = prevObj.number ? parseFloat(prevObj.number.trim().replaceAll(',', '')) : "";
  const nextNumberVal = nextObj.number ? parseFloat(nextObj.number.trim().replaceAll(',', '')) : "";
  if (nextNumberVal == prevNumberVal) return 0;
  if (nextNumberVal < prevNumberVal) return -1;
  if (nextNumberVal > prevNumberVal) return 1;
}
function sortMoney(nextObj, prevObj, nextRow, prevRow) {
  const prevNumberVal = prevObj ? parseFloat(prevObj) : "";
  const nextNumberVal = nextObj ? parseFloat(nextObj) : "";
  if (nextNumberVal == prevNumberVal) return 0;
  if (nextNumberVal < prevNumberVal) return -1;
  if (nextNumberVal > prevNumberVal) return 1;
}
function sortFormula(nextObj, prevObj, nextRow, prevRow) {
  const prevNumberVal = prevObj ? parseFloat(prevObj) : "";
  const nextNumberVal = nextObj ? parseFloat(nextObj) : "";
  if (nextNumberVal == prevNumberVal) return 0;
  if (nextNumberVal < prevNumberVal) return -1;
  if (nextNumberVal > prevNumberVal) return 1;
}
export function sortAlphabetically(nextObj, prevObj, nextRow, prevRow) {
  const prevtext = prevObj ? prevObj.trim().toLowerCase() : "";
  const nextText = nextObj ? nextObj.trim().toLowerCase() : "";
  if (nextText == prevtext) return 0;
  if (nextText < prevtext) return -1;
  if (nextText > prevtext) return 1;
}

export default projectColumnDefs;
