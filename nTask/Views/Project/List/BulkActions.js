import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Button from "@material-ui/core/Button/";
import listStyles from "./styles";
import { BlockPicker } from "react-color";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Popper from "@material-ui/core/Popper";
import Grow from "@material-ui/core/Grow";
import {
  BulkUpdateProject,
  BulkDeleteProject,
} from "../../../redux/actions/projects";
import { FetchWorkspaceInfo } from "../../../redux/actions/workspace";
import helper from "../../../helper";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import DeleteConfirmDialog from "../../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import DeleteBulkProjectDialogContent from "../../../components/Dialog/Popups/bulkDeleteConfirmation";
import ColorPicker from "../../../components/Dropdown/ColorPicker/ColorPicker";
import { FormattedMessage, injectIntl } from "react-intl";
import PermissionAlert from "../../../components/Dialog/ConfirmationDialogs/PermissionAlert";

class BulkActions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDateOpen: false,
      dueDateOpen: false,
      selectedColor: "#D9E3F0",
      anchorEl: null,
      open: false,
      archiveBtnQuery: "",
      unarchiveBtnQuery: "",
      deleteBtnQuery: "",
      step: 0,
      disabled: true,
      permissionAlert : false
    };

    this.handleStartDateClose = this.handleStartDateClose.bind(this);
    this.handleStartDateToggle = this.handleStartDateToggle.bind(this);
    this.handleDueDateClose = this.handleDueDateClose.bind(this);
    this.handleDueDateToggle = this.handleDueDateToggle.bind(this);
    this.handleColorPickerBtnClick = this.handleColorPickerBtnClick.bind(this);
    this.handleClickAway = this.handleClickAway.bind(this);
    this.showConfirmationPopup = this.showConfirmationPopup.bind(this);
    this.closeConfirmationPopup = this.closeConfirmationPopup.bind(this);
  }
  showConfirmationPopup(inputType) {
    this.setState({
      showConfirmation: true,
      inputType: inputType || "",
    });
  }

  closeConfirmationPopup() {
    this.setState({ showConfirmation: false, step: 0 });
  }

  handleStartDateClose(event) {
    this.setState({ startDateOpen: false });
  }
  handleStartDateToggle() {
    this.setState(state => ({ startDateOpen: !state.startDateOpen }));
  }
  handleDueDateClose(event) {
    this.setState({ dueDateOpen: false });
  }
  handleDueDateToggle() {
    this.setState(state => ({ dueDateOpen: !state.dueDateOpen }));
  }
  handleColorPickerBtnClick(event) {
    const { currentTarget } = event;
    this.setState(state => ({
      anchorEl: currentTarget,
      open: !state.open,
    }));
  }
  handleColorChange = color => {
    const { selectedIdsList } = this.props;
    this.setState({ selectedColor: color }, () => {
      if (this.state.selectedColor !== this.state.prevColor) {
        const type = helper.PROJECT_BULK_TYPES("Color");
        let data = {
          type,
          projectIds: selectedIdsList,
        };
        data["colorCode"] = this.state.selectedColor;

        this.setState({ prevColor: this.state.selectedColor }, () => {
          this.props.BulkUpdateProject(data, () => {
            // this.FetchWorkSpace();
          });
        });
      }
    });
  };
  handleClickAway = () => {
    this.setState({ open: false });
  };
  FetchWorkSpace = () => {
    this.props.FetchWorkspaceInfo("", () => {});
  };
  handleBulkClick = type => {
    this.props.BulkUpdateProject(
      {
        type,
        projectIds: this.props.selectedIdsList,
      },
      () => {
        // this.FetchWorkSpace();
      }
    );
  };
  handleArchive = e => {
    /* function for Archive bulk projects */
    if (e) e.stopPropagation();
    let type = this.state.inputType;
    this.setState({ archiveBtnQuery: "progress" }, () => {
      this.props.BulkUpdateProject({ type, projectIds: this.props.selectedIdsList }, () => {
        this.props.FetchWorkspaceInfo("", () => {
          this.setState({
            archiveBtnQuery: "",
            inputType: "",
            showConfirmation: false,
          });
        });
      });
    });
  };
  handleUnArchive = e => {
    /* function for Unarchive bulk projects */
    if (e) e.stopPropagation();
    let type = 13;
    this.setState({ unarchiveBtnQuery: "progress" }, () => {
      this.props.BulkUpdateProject({ type, projectIds: this.props.selectedIdsList }, () => {
        this.props.handleUnarchive(this.props.selectedIdsList);
        this.props.FetchWorkspaceInfo("", () => {
          this.setState({
            unarchiveBtnQuery: "",
            inputType: "",
            showConfirmation: false,
          });
        });
      });
    });
  };

  handleDelete = e => {
    const {
      selectedRows,
      selectedIdsList,
      BulkDeleteProject,
      handleUnarchive,
      FetchWorkspaceInfo,
    } = this.props;

    let filterProjectsHaveDeletePer = selectedRows.filter(
      r => r.row.projectPermission.permission.project.delete.cando
    ); /** filter those projects which user have the permission to delete */

    //Generating array of projectIds
    let projectIdsArr = filterProjectsHaveDeletePer.map(r => {
      return r.row.projectId;
    });

    /* function for deleting bulk projects */
    if (e) e.stopPropagation();
    const type = helper.PROJECT_BULK_TYPES("Delete");
    this.setState({ deleteBtnQuery: "progress" }, () => {
      BulkDeleteProject({ type, projectIds: projectIdsArr }, () => {
        handleUnarchive(projectIdsArr);
        FetchWorkspaceInfo("", () => {
          this.setState({
            deleteBtnQuery: "",
            inputType: "",
            showConfirmation: false,
          });
        });
      });
    });
  };
  handleTypeProjectInput = event => {
    let value = event.target.value;
    value && value.trim().toLowerCase() === "delete all projects here"
      ? this.setState({ disabled: false })
      : this.setState({ disabled: true });
  };
  handleDeleteProjectStep = value => {
    this.setState({ step: value });
  };
  getTranslatedId(value) {
    switch (value) {
      case "Color":
        value = "common.action.color.label";
        break;
      case "Archive":
        value = "common.action.archive.confirmation.archive-button.label";
        break;
      case "Delete":
        value = "common.action.delete.confirmation.delete-button.label";
        break;
      case "Unarchive":
        value = "common.action.un-archive.confirmation.title";
        break;
    }
    return value;
  }
  render() {
    const { classes, theme, selectedIdsList, isArchivedSelected, projectPer, intl, profileState } = this.props;
    const {
      dueDateOpen,
      startDateOpen,
      selectedColor,
      anchorEl,
      open,
      inputType,
      showConfirmation,
      archiveBtnQuery,
      unarchiveBtnQuery,
      disabled,
      deleteBtnQuery,
      permissionAlert
    } = this.state;
    const id = open ? "simple-popper" : null;
    const styles = (action, i, arr) => {
      return i == 0
        ? { borderRadius: "4px 0 0 4px" }
        : i == arr.length - 1
        ? { borderRadius: "0 4px 4px 0", borderLeft: "none" }
        : { borderLeft: "none", borderRadius: 0 };
    };

    const BulkActionsType = isArchivedSelected
      ? [
          selectedIdsList.length +
            " " +
            intl.formatMessage({
              id: "common.item-selected.label",
              defaultMessage: "items Selected",
            }),
          "Unarchive",
          "Delete",
        ]
      : [
          selectedIdsList.length +
            " " +
            intl.formatMessage({
              id: "common.item-selected.label",
              defaultMessage: "items Selected",
            }),
          "Color",
          "Archive",
          "Delete",
        ]; /* Bulk actions name array */

      let currentUser = profileState.member.allMembers.find(m => {
        /** finding current user from team members array */
        return m.userId == profileState.userId;
      });
      let alertModal = currentUser.roleId == "000" || currentUser.roleId == "006" || currentUser.roleId == "002" ? true : false;
    return (
      <div className={classes.BulkActionsCnt}>
        {inputType === "Archive" /* Confirmation modal for Archive the bulk projects  */ ? (
          <ActionConfirmation
            open={showConfirmation && projectPer.archive.cando}
            closeAction={this.closeConfirmationPopup}
            cancelBtnText={
              <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
            }
            successBtnText={
              <FormattedMessage
                id="common.archived.projects.label2"
                defaultMessage="Archive Projects"
              />
            }
            alignment="center"
            headingText={
              <FormattedMessage
                id="common.action.archive.confirmation.archive-button.label"
                defaultMessage="Archive"
              />
            }
            iconType="archive"
            msgText={
              <FormattedMessage
                id="common.archived.projects.message"
                defaultMessage={`Are you sure you want to archive these ${selectedIdsList.length} projects?`}
                values={{ l: selectedIdsList.length }}
              />
            }
            successAction={this.handleArchive}
            btnQuery={archiveBtnQuery}
          />
        ) : inputType === "Unarchive" /* Confirmation modal for Unarchive the bulk projects  */ ? (
          <ActionConfirmation
            open={showConfirmation && projectPer.unarchive.cando}
            closeAction={this.closeConfirmationPopup}
            cancelBtnText={
              <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
            }
            successBtnText={
              <FormattedMessage
                id="common.un-archived.projects.label2"
                defaultMessage="Unarchive projects"
              />
            }
            alignment="center"
            iconType="unarchive"
            headingText={
              <FormattedMessage
                id="common.action.un-archive.confirmation.title"
                defaultMessage="Unarchive"
              />
            }
            // msgText={`Are you sure you want to unarchive these ${selectedIdsList.length} projects?`}
            msgText={
              <FormattedMessage
                id="common.un-archived.projects.message"
                defaultMessage={`Are you sure you want to unarchive these ${selectedIdsList.length} projects?`}
                values={{ l: selectedIdsList.length }}
              />
            }
            successAction={this.handleUnArchive}
            btnQuery={unarchiveBtnQuery}
          />
        ) : inputType === "Delete" /* Confirmation modal for deleting the bulk projects  */ ? ( 
            <DeleteConfirmDialog
              open={showConfirmation && projectPer.delete.cando}
              closeAction={this.closeConfirmationPopup}
              cancelBtnText={
                <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
              }
              successBtnText={
                this.state.step == 0 ? (
                  <FormattedMessage
                    id="common.action.delete.anyway.label"
                    defaultMessage="Delete Anyway"
                  />
                ) : (
                  <FormattedMessage
                    id="common.action.delete.confirmation.delete-button.label"
                    defaultMessage="Delete"
                  />
                )
              }
              alignment="center"
              headingText={
                <FormattedMessage
                  id="common.action.delete.confirmation.delete-button.label"
                  defaultMessage="Delete"
                />
              }
              disabled={this.state.step === 0 ? false : disabled}
              successAction={
                this.state.step === 0
                  ? () => {
                      this.handleDeleteProjectStep(1);
                    }
                  : () => this.handleDelete(event)
              }
              btnQuery={this.state.deleteBtnQuery}>
              {showConfirmation ? (
                <DeleteBulkProjectDialogContent
                  step={this.state.step}
                  handleTypeProjectInput={this.handleTypeProjectInput}
                />
              ) : null}
            </DeleteConfirmDialog> 
        ) : null}

        {BulkActionsType.map((action, i, arr) => {
          return i == 1 && !isArchivedSelected ? (
            <>
              {/* <Button
                disabled={i == 0 ? true : false}
                style={styles(action, i, arr)}
                variant="outlined"
                onClick={this.handleUnArchive}
                classes={{ outlined: classes.BulkActionBtn }}
                selectedIdsList={this.state.selectedIds}
              >
                {action}
              </Button> */}
              <Button
                disabled={i == 0 ? true : false}
                style={styles(action, i, arr)}
                variant="outlined"
                onClick={this.handleColorPickerBtnClick}
                classes={{ outlined: classes.BulkActionBtn }}
                selectedIdsList={this.state.selectedIds}>
                <FormattedMessage id={this.getTranslatedId(action)} defaultMessage={action} />
              </Button>
              <Popper id={id} open={open} anchorEl={anchorEl} disablePortal transition>
                {({ TransitionProps, placement }) => (
                  <Grow
                    {...TransitionProps}
                    id="menu-list-grow"
                    style={{
                      transformOrigin: placement === "bottom" ? "center top" : "left bottom",
                    }}>
                    <ClickAwayListener onClickAway={this.handleClickAway}>
                      <div id="colorPickerCnt">
                        <ColorPicker
                          triangle="hide"
                          onColorChange={color => {
                            this.handleColorChange(color);
                          }}
                          selectedColor={selectedColor}
                        />
                      </div>
                    </ClickAwayListener>
                  </Grow>
                )}
              </Popper>
            </>
          ) : (
            <Button
              disabled={i == 0 ? true : false}
              style={styles(action, i, arr)}
              variant="outlined"
              classes={{ outlined: classes.BulkActionBtn }}
              onClick={this.showConfirmationPopup.bind(this, action)}>
              <FormattedMessage id={this.getTranslatedId(action)} defaultMessage={action} />
            </Button>
          );
        })}
      </div>
    );
  }
}

// export default withStyles(listStyles, { withTheme: true })(BulkActions);
const mapStateToProps = (state, ownProps) => {
  return {
    projectPer: state.workspacePermissions.data.project,
    profileState: state.profile.data,
  };
};
export default compose(
  withRouter,
  injectIntl,
  withStyles(listStyles, { withTheme: true }),
  connect(mapStateToProps, { BulkUpdateProject, FetchWorkspaceInfo, BulkDeleteProject })
)(BulkActions);
