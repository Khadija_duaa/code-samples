import React, { Component, Fragment, useState } from "react";
import ReactDataGrid from "react-data-grid";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import BulkActions from "./BulkActions";
import { Circle } from "rc-progress";
import AddProject from "./AddProject";
import Hotkeys from "react-hot-keys";
import Grid from "@material-ui/core/Grid";
import listStyles from "./styles";
import AddIcon from "@material-ui/icons/Add";
import CancelIcon from "@material-ui/icons/Cancel";
import IconButton from "../../../components/Buttons/IconButton";
import TableActionDropDown from "./ActionDropDown";
import helper from "../../../helper";
import DefaultDialog from "../../../components/Dialog/Dialog";
import AddNewProject from "../../AddNewForms/AddNewProjectForm";
import Typography from "@material-ui/core/Typography";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";
import { headerTranslations } from "../../../utils/headerTranslations";
import StatusDropdown from "../../../components/Dropdown/StatusDropdown/Dropdown";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import {
  SaveProject,
  DeleteProject,
  CopyProject,
  EditProject,
  ArchiveProject,
  UnarchiveProject,
  moveProjectToWorkspace,
  UpdateProject,
  projectDetails,
  getSetLoadingProjectDetails,
  updateProject,
  markProjectCompleted
} from "../../../redux/actions/projects";

import {
  UpdateTask,
  FetchTasksInfo,
  CopyTask,
  DeleteTask,
  ArchiveTask,
  UnArchiveTask,
} from "../../../redux/actions/tasks";

import { FetchWorkspaceInfo } from "../../../redux/actions/workspace";

import { SaveItemOrder } from "../../../redux/actions/itemOrder";
import { UpdateCalenderTask } from "../../../redux/actions/calenderTasks";
import { sortListData, getSortOrder } from "../../../helper/sortListData";
import { statusData } from "../../../helper/projectDropdownData";

import ProjectActionDropdown from "./ProjectActionDropDown";
import DefaultButton from "../../../components/Buttons/DefaultButton";
import MenuList from "../../../components/Menu/TaskMenus/MemberListMenu";
import EmptyState from "../../../components/EmptyStates/EmptyState";
import SvgIcon from "@material-ui/core/SvgIcon";
import TaskIcon from "../../../components/Icons/TaskIcon";
import MeetingsIcon from "../../../components/Icons/MeetingIcon";
import IssuesIcon from "../../../components/Icons/IssueIcon";
import RiskIcon from "../../../components/Icons/RiskIcon";
import { calculateContentHeight } from "../../../utils/common";
import GetProjectTasks from "../../../helper/getProjectTasks";
import GetMeetingAttendees from "../../../helper/getMeetingAttendees";
import GetRiskOwner from "../../../helper/getRiskOwner";
import getIssueAssignees from "../../../helper/getIssueAssignees";
import cloneDeep from "lodash/cloneDeep";
import { getProjectAssosiatedData } from "../../../helper/getProjectAssosiatedData";
import Starred from "../Grid/Starred";
import { FormattedMessage, injectIntl } from "react-intl";
import CustomTooltip from "../../../components/Tooltip/Tooltip";
import CustomIconButton from "../../../components/Buttons/CustomIconButton";
import IconGantt from "../../../components/Icons/IconGantt";
import isEmpty from "lodash/isEmpty";
import find from "lodash/find";
import { GroupingRowRenderer } from "./GroupingRowRenderer";
import { Data } from "react-data-grid-addons";
import ColumnSelectionDropdown from "../../../components/Dropdown/SelectedItemsDropDown";
import isEqual from "lodash/isEqual";

let clicks = 0,
  timeout;

const {
  Draggable: { Container: DraggableContainer, RowActionsCell, DropTargetRowContainer },
  Data: { Selectors },
  Toolbar,
} = require("react-data-grid-addons");

const RowRenderer = DropTargetRowContainer(ReactDataGrid.Row);
const Progress = (progress, object) => {
  return (
    <div style={{ display: "flex", alignItems: "center" }}>
      <div style={{ width: 30, marginRight: 5, marginTop: 4 }}>
        <Circle
          percent={progress}
          strokeWidth="10"
          trailWidth=""
          trailColor="#dedede"
          strokeColor="#30d56e"
        />
      </div>
      <Typography variant="h6" align="center">
        {progress}%
      </Typography>
    </div>
  );
};

let increment = 20;
class ProjectListOld extends React.Component {
  // static defaultProps = { rowKey: "projectId" };
  constructor(props, context) {
    super(props, context);
    this.state = {
      _columns: this.getColumns(),
      rows: [],
      selectedRows: [],
      output: "",
      selectedIds: [],
      addProject: false,
      addNewForm: false,
      userId: props.userId,
      value: "",
      progress: 0,
      issueId: "",
      isProgress: false,
      TaskError: false,
      TaskErrorMessage: "",
      isReorder: false,
      startDateOpen: false,
      dueDateOpen: false,
      newCol: [],
      showProjects: false,
      currentTask: {},
      loggedInTeam: this.props.loggedInTeam,
      isWorkspaceChanged: false,
      showRecords: increment,
      totalRecords: 0,
      members: [],
      sortColumn: null,
      sortDirection: "NONE",
      selectedProject: {},
      projectDetailView: false,
      statusCompleteDialogOpen: false,
      statusCompleteBtnQuery: '',
      loadingProjectDetailsDialogue: this.props
        .loadingProjectDetails /** it set's true/false when api gives success to open other project details */,
    };
    this.addProject = this.addProject.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.addProjectInput = React.createRef();
    this.onKeyDown = this.onKeyDown.bind(this);
    this.cancelAddProject = this.cancelAddProject.bind(this);
    this.isUpdated = this.isUpdated.bind(this);
    this.selectedDate = this.selectedDate.bind(this);
    this.selectedAction = this.selectedAction.bind(this);
    this.showProjectsPopUp = this.showProjectsPopUp.bind(this);

    this.handleScrollDown = this.handleScrollDown.bind(this);
    this.scrollListener = this.scrollListener.bind(this);
  }

  componentDidMount() {
    const { workspaces, loggedInTeam } = this.props;
    if (loggedInTeam) {
      let { sortColumn, sortDirection } = getSortOrder(workspaces, loggedInTeam, 1);

      if (sortColumn && sortDirection) {
        /** sorting items if there is already saved sort order and column */
        let rows = this.setFormattedProjectRows();
        this.sortRows(rows, sortColumn, sortDirection);
      } else this.setFormattedProjectRows();
    }
  }
  columnChangeCallback = () => {
    this.selectedColumnArr();
  };
  onColumnResize = (columnIndex, size) => {
    const { _columns } = this.state;
    if(size < 0) return;
    let columnWidth = JSON.parse(localStorage.getItem("columnWidthProject"));
    if (columnWidth) {
      columnWidth[_columns[columnIndex - 1].key] = {
        width: size,
      };
      localStorage.setItem("columnWidthProject", JSON.stringify(columnWidth));
    } else {
      let obj = {};
      obj[_columns[columnIndex - 1].key] = {
        width: size,
      };
      localStorage.setItem("columnWidthProject", JSON.stringify(obj));
    }
  };
getColumns = () => {
  let columnWidth = JSON.parse(localStorage.getItem("columnWidthProject"));

  let columns = [
    {
      key: "uniqueId",
      name: "ID",
      width: columnWidth && columnWidth["uniqueId"] ? columnWidth["uniqueId"].width : 150,
      cellClass: "firstColoumn",
      sortDescendingFirst: true,
      visible: true,
      resizable: true,
      formatter: value => {
        return (
          <div
            className={this.props.classes.taskListTitleTextCnt}
            onClick={e => e.stopPropagation()}
            style={{
              borderLeft: `6px solid ${value.row &&
              (value.row.colorCode == "" ? "#fff" : value.row.colorCode)}`,
            }}>
                <span className={this.props.classes.taskListTitleText}>
                  {value.row ? value.row.uniqueId : ""}
                </span>
          </div>
        );
      },
      columnOrder: 0,
      sortable: teamCanView("advanceSortAccess") ? false : true,
    },

    {
      key: "projectName",
      name: "Project Title",
      cellClass: "secondColoumn",
      visible: true,
      width: columnWidth && columnWidth["projectName"] ? columnWidth["projectName"].width : 400,
      sortDescendingFirst: true,
      resizable: true,
      formatter: value => {
        let gantAccess = value.row
          ? value.row.projectPermission
            ? value.row.projectPermission.permission.project.accessGantt.cando
            : this.props.projectPer.accessGantt.cando
          : true;
        return (
          <div style={{ display: "flex", alignItems: "center" }}>
            {gantAccess && (
              <CustomTooltip
                helptext={
                  <FormattedMessage
                    id="project.tooltip.project-gantt"
                    defaultMessage="Project Gantt"
                  />
                }
                iconType="help"
                placement="top"
                style={{ color: this.props.theme.palette.common.white }}>
                <CustomIconButton
                  onClick={e => {
                    e.stopPropagation();
                    this.props.projectClick(value.row);
                  }}
                  btnType="transparent"
                  variant="contained"
                  style={{
                    width: 26,
                    height: 26,
                    borderRadius: 4,
                    // marginRight: 10,
                    // border: "1px solid #c2c2c2"
                  }}
                  disabled={!gantAccess}>
                  <SvgIcon
                    viewBox="0 0 14 11"
                    htmlColor={this.props.theme.palette.secondary.medDark}
                    className={this.props.classes.IconGantt}>
                    <IconGantt />
                  </SvgIcon>
                </CustomIconButton>
              </CustomTooltip>
            )}

            {!this.props.isArchivedSelected ? (
              <Starred
                userId={this.props.profileState.data.userId}
                currentProject={value.row}
              />
            ) : null}

            <span className={this.props.classes.taskListTitleText}>
                  {value.row ? value.row.projectName : ""}
                </span>
          </div>
        );
      },
      columnOrder: 1,
      sortable: teamCanView("advanceSortAccess") ? false : true,
    },
    {
      key: "status",
      name: "Status",
      width:  columnWidth && columnWidth["status"] ? columnWidth["status"].width : 155,
      resizable: true,
      formatter: value => {
        const projectStatusData = statusData(
          this.props.theme,
          this.props.classes,
          this.props.intl
        ); /** geeting project status drop down data and setting into state */
        const selectedStatus = value.row ? projectStatusData.find(p => p.value == value.row.status) || {} : {};
        let editProjectStatusPer = value.row && value.row.projectPermission  ? !value.row.projectPermission.permission.project.projectDetails.editProjectStatus.isAllowEdit : false; /** fetching project status permission from each project and passing it to the status componenet */

        return (
          <StatusDropdown
            onSelect={status => {
              this.handleStatusSelect(status, value.row);
            }}
            option={selectedStatus}
            writeFirst={true}
            options={projectStatusData}
            toolTipTxt={selectedStatus.label}
            disabled={editProjectStatusPer}
          />
        );
      },
      cellClass: "dropDownCell",
      columnOrder: 2,
      sortable: !teamCanView("advanceSortAccess"),
    },
    {
      key: "linkedTasksCount",
      name: "Tasks",
      width: columnWidth && columnWidth["linkedTasksCount"] ? columnWidth["linkedTasksCount"].width : 130,
      resizable: true,
      formatter: value => {
        return (
          <DefaultButton buttonType="smallIconBtn">
            <SvgIcon viewBox="0 0 18 15.188" className={this.props.classes.smallBtnIcon}>
              <TaskIcon />
            </SvgIcon>
            <Typography variant="body2" align="center">
              {value.row && value.row.tasks ? value.row.tasks : 0}
            </Typography>
          </DefaultButton>
        );
      },
      cellClass: "dropDownCell",
      columnOrder: 3,
      sortable: teamCanView("advanceSortAccess") ? false : true,
    },
    {
      key: "linkedIssuesCount",
      name: "Issues",
      width: columnWidth && columnWidth["linkedIssuesCount"] ? columnWidth["linkedIssuesCount"].width : 130,
      resizable: true,
      formatter: value => {
        return (
          <DefaultButton buttonType="smallIconBtn">
            <SvgIcon viewBox="0 0 16 17.375" className={this.props.classes.smallBtnIcon}>
              <IssuesIcon />
            </SvgIcon>
            <Typography variant="body2" align="center">
              {value.row && value.row.issues ? value.row.issues : 0}
            </Typography>
          </DefaultButton>
        );
      },
      cellClass: "dropDownCell",
      columnOrder: 4,
      sortable: teamCanView("advanceSortAccess") ? false : true,
    },

    {
      key: "linkedRisksCount",
      name: "Risks",
      width: columnWidth && columnWidth["linkedRisksCount"] ? columnWidth["linkedRisksCount"].width : 130,
      resizable: true,
      formatter: value => {
        return (
          <DefaultButton buttonType="smallIconBtn">
            <RiskIcon classes={{ root: this.props.classes.smallBtnIcon }} />
            <SvgIcon viewBox="0 0 18 15.75" className={this.props.classes.smallBtnIcon}>
              <RiskIcon />
            </SvgIcon>
            <Typography variant="body2" align="center">
              {value.row && value.row.risks ? value.row.risks : 0}
            </Typography>
          </DefaultButton>
        );
      },
      cellClass: "dropDownCell",
      columnOrder: 5,
      sortable: teamCanView("advanceSortAccess") ? false : true,
    },

    {
      key: "linkedMeetingsCount",
      name: "Meetings",
      width: columnWidth && columnWidth["linkedMeetingsCount"] ? columnWidth["linkedMeetingsCount"].width :  130,
      resizable: true,
      formatter: value => {
        return (
          <DefaultButton buttonType="smallIconBtn">
            <SvgIcon viewBox="0 0 17.031 17" className={this.props.classes.smallBtnIcon}>
              <MeetingsIcon />
            </SvgIcon>
            <Typography variant="body2" align="center">
              {value.row && value.row.meetings ? value.row.meetings : 0}
            </Typography>
          </DefaultButton>
        );
      },
      cellClass: "dropDownCell",
      columnOrder: 6,
      sortable: teamCanView("advanceSortAccess") ? false : true,
    },

    {
      key: "progress",
      name: "Progress",
      width: columnWidth && columnWidth["progress"] ? columnWidth["progress"].width : 155,
      resizable: true,
      formatter: value => {
        const row = value.row;
        const progress = row && value.row.status === 3 ? 100 : row && row.progress ? row.progress : 0;
        return Progress(Math.round(progress), this.state);
      },
      cellClass: "dropDownCell",
      columnOrder: 7,
      sortable: teamCanView("advanceSortAccess") ? false : true,
    },
    {
      key: "assignee",
      name: "Resources",
      width: columnWidth && columnWidth["assignee"] ? columnWidth["assignee"].width : 205,
      resizable: true,
      formatter: value => {
        const isArchive = this.props.projectState.indexOf("Archived") > -1;
        let tasksData = GetProjectTasks(this.props.tasks, value.row.projectId, isArchive),
          assignees = [];
        tasksData.map(x => {
          assignees = assignees
            .concat(x.assigneeList)
            .concat(GetMeetingAttendees(this.props.meetings, x.taskId))
            .concat(GetRiskOwner(this.props.risks, x.taskId))
            .concat(getIssueAssignees(this.props.issues, x.taskId));
        });
        let members = this.props.allMembers;
        // const resources = value.row.resources ? value.row.resour
        // Returns distinct values in array
        assignees = [...new Set([...assignees, ...value.row.resources])];
        let assigneeList = members.filter(m => {
          if (assignees.length >= 0) return assignees.indexOf(m.userId) >= 0;
        });

        return (
          <MenuList
            id={value.row ? value.row.projectId : ""}
            assigneeList={assigneeList}
            MenuData={value.row}
            placementType="bottom-end"
            isUpdated={this.isUpdated}
            viewType="project"
            checkAssignee={true}
            listType="assignee"
          />
        );
      },
      cellClass: "dropDownCell",
      columnOrder: 8,
    },
    {
      key: "projectManager",
      name: "Project Manager",
      width:  columnWidth && columnWidth["projectManager"] ? columnWidth["projectManager"].width : 205,
      resizable: true,
      formatter: value => {
        let assigneeList = value.row && value.row.projectManagers

        return (
          <MenuList
            id={value.row ? value.row.projectId : ""}
            assigneeList={assigneeList}
            MenuData={value.row}
            placementType="bottom-end"
            isUpdated={this.isUpdated}
            viewType="project"
            checkAssignee={true}
            listType="assignee"
          />
        );
      },
      cellClass: "dropDownCell",
      columnOrder: 9,
    },

    {
      key: "optionList",
      name: "",
      formatter: value => {
        return (
          <ProjectActionDropdown
            FetchWorkSpaceData={this.props.FetchWorkspaceInfo}
            project={value.row}
            handleUnarchive={this.handleUnarchive}
            EditProject={this.props.EditProject}
            CopyProject={this.props.CopyProject}
            openProjectSetting={this.props.openProjectSetting}
            DeleteProject={this.props.DeleteProject}
            ArchiveProject={this.props.ArchiveProject}
            CopyCalenderTask={this.props.CopyCalenderTask}
            UnarchiveProject={this.props.UnarchiveProject}
            moveProjectToWorkspace={moveProjectToWorkspace}
            SaveProject={this.props.SaveProject}
            UpdateCalenderTask={this.props.UpdateCalenderTask}
            showSnackbar={this.props.handleExportType}
            selectedColor={value.row && value.row.colorCode ? value.row.colorCode : { hex: "" }}
            isUpdated={this.isUpdated}
            isArchivedSelected={this.props.isArchivedSelected}
            projectPer={
              value.row.projectPermission
                ? value.row.projectPermission.permission.project
                : null
            }
          />
        );
      },
      cellClass: "dropDownCell lastColoumn",
      columnOrder: 10,
      width: columnWidth && columnWidth["optionList"] ? columnWidth["optionList"].width : 50,
    },
  ]
return columns
  }
  selectedColumnArr = () => {
    const { projectColumns = [] } = this.props;

    const projectColumnOrder = projectColumns.filter(c => !c.hide).map(c => c.columnName);

    let newCols = this.getColumns();


      let columns =


        newCols
          .filter((x, i) => projectColumnOrder.indexOf(x.name) >= 0 || i === newCols.length - 1 || x.key == "projectName");

      /** containing unique values */
      this.setState(
        {
          _columns: columns,
        },
      );

  };
  /* Project status handling */
updateProjectStatus = (status, project) => {
  this.props.updateProject(
    {...project, status: status.value},
    succ => {},
    err => {}
  );
}
handleStatusSelect = (status, project) => {
    if(status.value == 3){
      this.projectCompleteConfirmOpen();
      this.setState({selectedProject: {...project, status: status.value}})
      return;
    }
    this.updateProjectStatus(status, project)
  };
handleMarkProjectAsCompleted = () => {
  this.setState({statusCompleteBtnQuery: 'progress'})
  const {selectedProject} = this.state;
  this.props.markProjectCompleted(selectedProject);
  this.props.updateProject(
    selectedProject,
    succ => {
      this.setState({selectedProject: {}, statusCompleteBtnQuery: ''})
      this.projectCompleteConfirmClose();
    },
    err => {}
  );
}
  /* Project status handling */
  handleProjectDetailsDialogClose = e => {
    e.preventDefault();
    this.setState({ projectDetailView: false });
    this.props.projectDetails({});
  };

  componentDidUpdate(prevProps, prevState) {
    const { addProject, isReorder, sortColumn, sortDirection } = this.state;
    if (addProject == true) {
      this.addProjectInput.current.focus();
    }

    const {
      filteredProjects,
      tasks,
      issues,
      risks,
      meetings,
      loggedInTeam,
      isArchivedSelected,
      sortObj,
    } = this.props;
    const dataChanged =
      JSON.stringify(prevProps.filteredProjects) !== JSON.stringify(filteredProjects) ||
      JSON.stringify(prevProps.tasks) !== JSON.stringify(tasks) ||
      JSON.stringify(prevProps.issues) !== JSON.stringify(issues) ||
      JSON.stringify(prevProps.risks) !== JSON.stringify(risks) ||
      JSON.stringify(prevProps.meetings) !== JSON.stringify(meetings);

    if (loggedInTeam !== prevProps.loggedInTeam) {
      this.setState({ loggedInTeam, isWorkspaceChanged: true });
    }
    if (dataChanged) {
      if (sortColumn) this.sortRows(this.setFormattedProjectRows(), sortColumn, sortDirection);
      else this.setFormattedProjectRows();
    } else if (isReorder && isReorder !== prevState.isReorder) {
      this.setItemsOrder();
    }
    // Clearing bulk selection on archive mode selection
    else if (JSON.stringify(prevProps.projectState) !== JSON.stringify(this.props.projectState)) {
      this.setState({ selectedIds: [] });
    }

    if (
      sortObj &&
      sortObj.column &&
      (sortObj.column !== prevProps.sortObj.column ||
        sortObj.direction !== prevProps.sortObj.direction)
    ) {
      /** when user click on selected sort button for changing the sort order, this condtion will execute */
      this.sortRows(
        this.setFormattedProjectRows(),
        sortObj.column,
        sortObj.direction
      ); /** function call for reseting the state project list into selected sort manner */
    } else if (
      sortObj &&
      sortObj.cancelSort == "NONE" &&
      sortObj.cancelSort !== prevProps.sortObj.cancelSort
    ) {
      /** when user click on cancel sort this condtion will execute */
      this.setFormattedProjectRows();
    }
    if (
      JSON.stringify(prevProps.loadingProjectDetails) !==
      JSON.stringify(this.props.loadingProjectDetails)
    ) {
      this.setState({
        loadingProjectDetailsDialogue: this.props.loadingProjectDetails,
      });
    }
    // this.queryProjectUrl();
    this.renderHeaders();
    if(!isEqual(prevProps.taskColumns, this.props.taskColumns)){
      this.selectedColumnArr();
    }
  }

  renderHeaders = () => {
    var elements = document.getElementsByClassName("widget-HeaderCell__value");
    var sortableElements = document.getElementsByClassName("react-grid-HeaderCell-sortable");
    for (var i = 0; i < elements.length; i++) {
      elements[i].innerText = headerTranslations(elements[i].innerText, this.props.intl);
    }
    for (var i = 0; i < sortableElements.length; i++) {
      sortableElements[i].innerText = headerTranslations(
        sortableElements[i].innerText,
        this.props.intl
      );
    }
  };
  componentWillUnmount() {
    if (this.canvas) {
      this.canvas.removeEventListener("scroll", this.scrollListener);
    }
  }

  setFormattedProjectRows = () => {
    let members = this.props.allMembers;
    const { filteredProjects, tasks, issues, risks, meetings } = this.props;

    let rows =
      cloneDeep(filteredProjects).map(x => {
        // Inserting count values in data
        let associatedData = getProjectAssosiatedData(x.projectId, tasks, issues, risks, meetings);
        x.linkedTasksCount = associatedData.tasks.length;
        x.linkedIssuesCount = associatedData.issues.length;
        x.linkedRisksCount = associatedData.risks.length;
        x.linkedMeetingsCount = associatedData.meetings.length;

        let filteredTasks =
          tasks.filter(task => task.projectId === x.projectId); /** filter task which are related to the project */
          let tasksData = filteredTasks.reduce((result, cv) => { /** if task have its parent task then accepts its progress other wise accept own task progress */
            let parentTask = tasks.find(t => t.id == cv.parentId);
            if (parentTask) {
              result.push(parentTask.progress);
            } else {
              result.push(cv.progress);
            }
            return result;
          }, []);

        x.progress = tasksData.length
          ? tasksData.reduce((sum, x) => sum + x) / tasksData.length
          : 0;

        x.projectManagers = members.filter(m => {
          if (x.projectManager && x.projectManager.length >= 0)
            return x.projectManager.indexOf(m.userId) >= 0;
        });
        return x;
      }) || [];

    let allIds = rows.map(x => x.projectId);
    let selectedIds = allIds.filter(obj => {
      return this.state.selectedIds.indexOf(obj) >= 0;
    });

    this.setState({
      members,
      rows,
      selectedIds,
      totalRecords: filteredProjects.length,
      newCol: this.state._columns,
    });

    return rows;
  };

  setItemsOrder = () => {
    let props = this.props;
    let rows = cloneDeep(this.state.rows)
      .map(x => {
        let position = helper.RETURN_ITEMORDER(props.itemOrder.itemOrder, x.id);
        x.taskOrder = position || 0;
        x.tasks = props.tasks ? props.tasks.filter(z => z.projectId === x.projectId).length : 0;
        x.issues = props.tasks
          ? props.tasks
              .filter(z => z.projectId === x.projectId)
              .map(k => {
                let issueCount = props.issues
                  ? props.issues
                      .filter(y =>
                        props.projectState.indexOf("Archived") >= 0 ? y.isArchive : !y.isArchive
                      )
                      .filter(issue => issue.linkedTasks.indexOf(k.taskId) >= 0).length
                  : 0;
                return issueCount;
              })
          : 0;
        x.issues = x.issues.length
          ? x.issues.reduce((total, num) => {
              return total + num;
            })
          : 0;
        x.risks = props.tasks
          ? props.tasks
              .filter(z => z.projectId === x.projectId)
              .map(k => {
                let riskCount = props.issues
                  ? props.risks
                      .filter(y =>
                        props.projectState.indexOf("Archived") >= 0 ? y.isArchive : !y.isArchive
                      )
                      .filter(risk => risk.linkedTasks.indexOf(k.taskId) >= 0).length
                  : 0;
                return riskCount;
              })
          : 0;
        x.risks = x.risks.length
          ? x.risks.reduce((total, num) => {
              return total + num;
            })
          : 0;
        x.meetings = props.tasks
          ? props.tasks
              .filter(z => z.projectId === x.projectId)
              .map(k => {
                let meetingCount = props.meetings
                  ? props.meetings
                      .filter(y =>
                        props.projectState.indexOf("Archived") >= 0 ? y.isDelete : !y.isDelete
                      )
                      .filter(meeting => meeting.taskId === k.taskId).length
                  : 0;
                return meetingCount;
              })
          : 0;
        x.meetings = x.meetings.length
          ? x.meetings.reduce((total, num) => {
              return total + num;
            })
          : 0;
        let percentageData = props.tasks
            ? props.tasks.filter(z => z.projectId === x.projectId).map(k => k.progress)
            : [],
          percentage = 0;
        if (percentageData.length)
          percentage = percentageData.reduce((sum, val) => sum + val) / percentageData.length;
        x.progress = Math.round(percentage) || 0;
        return x;
      })
      .sort((a, b) => a.taskOrder - b.taskOrder);
    this.setState({ rows, isReorder: false });
  };

  handleColorChange = (data, color) => {
    //
    // let self = this;
    // this.setState({ selectedColor: color.hex, issueId: data.id, isColored: true }, function () {
    //   let obj = Object.assign({}, data);
    //   obj.colorCode = color.hex;
    //   self.props.UpdateProject(obj,(err,data)=>{
    //
    //   });
    //   self.setState({ selectedColor: '#fff', issueId: "", isColored: false })
    // });
  };
  handleStaredChange = (event, project) => {
    let isMark = event.target.checked;
    event.stopPropagation();
    this.setState({ [project.projectId]: isMark }, () => {
      let obj = {
        Id: project.id,
        MarkStar: isMark,
      };
      this.props.UpdateProject(obj);
    });
  };
  scrollListener() {
    if (this.canvas.scrollHeight - (this.canvas.scrollTop + this.canvas.clientHeight) < 1) {
      this.handleScrollDown();
    }
  }

  selectedAction(selectedColumns, isFirstLoad) {
    if (selectedColumns.length > 0) {
      let newCol = this.state.newCol.length ? this.state.newCol : this.state._columns;
      newCol = newCol
        .filter(x => selectedColumns.indexOf(x.name) >= 0 || x.columnOrder === 10)
        .sort((a, b) => a.columnOrder - b.columnOrder);
      this.setState({ _columns: newCol, showProjects: false }, () => {
        let itemOrder = this.props.itemOrder;
        itemOrder.projectColumnOrder = selectedColumns;
        if (!isFirstLoad) {
          this.props.SaveItemOrder(itemOrder, data => {});
        }
      });
    } else {
      this.setState({
        _columns: this.state.newCol.slice(0, 7),
        showProjects: false,
      });
    }
  }
  handleScrollDown = () => {
    this.setState({ showRecords: this.state.showRecords + increment }, () => {
      if (this.state.showRecords >= this.state.rows.length) {
        this.setState({ showRecords: this.state.rows.length });
      }
    });
  };
  showProjectsPopUp(e, data) {
    const {
      projectDetailsDialog: { docked, fullView },
      loadingProjectDetails,
    } = this.props;
    const { projectPermission : { permission } } = data;
    const { selectedProject } = this.state;
    if (!this.props.isArchivedSelected && data && data.projectId && !loadingProjectDetails && permission.project.projectDetails.cando) {
      if (selectedProject.projectId !== data.projectId) {
        this.props.getSetLoadingProjectDetails(true);
      }
      this.setState({
        selectedProject: data,
        projectDetailView: true,
        // loadingProjectDetailsDialogue: true,
      });
      // this.props.projectClick(data);
      this.props.projectDetails({
        projectDetails: data,
        fullView: fullView ? true : false,
        docked: docked ? true : false,
        dialog: true,
      });
    }
  }

  isUpdated(data) {
    // this.props.UpdateProject(data, (err, data) => {
    //
    // });
  }

  selectedDate(actualDate, isStart, plannedDate, currentTask, isPlanned, isActual) {
    if (isPlanned || isActual) {
      if (isStart) {
        currentTask.StartDateString = plannedDate;
        currentTask.actualStartDateString = actualDate;
        // this.props.UpdateProject(currentTask, (err, response) => {
        //   // response = response.data.CalenderDetails;
        //   // ;
        // });
      } else {
        currentTask.dueDateString = plannedDate;
        currentTask.actualDueDateString = actualDate;
        // this.props.UpdateProject(currentTask, (err, response) => {
        //   // response = response.data.CalenderDetails;
        //   // ;
        // });
      }
    }
  }

  addProject(keyName) {
    if (keyName && keyName === "enter") {
      this.setState({ addNewForm: true });
    } else if (keyName && keyName === "inline") {
      this.setState({ addProject: true });
    } else {
      this.setState({ addNewForm: true });
    }
  }
  cancelAddProject() {
    this.setState({
      addProject: false,
      addNewForm: false,
      TaskError: false,
      TaskErrorMessage: "",
    });
  }

  handleChange(e) {
    if (e.target.value.length > 80) {
      this.setState({
        TaskError: true,
        TaskErrorMessage: this.props.intl.formatMessage({
          id: "project.creation-dialog.validation.length",
          defaultMessage: "Please enter less than 80 characters",
        }),
        value: e.target.value,
      });
    } else {
      this.setState({
        TaskError: false,
        TaskErrorMessage: "",
        value: e.target.value,
      });
    }
  }
  onKeyDown(event) {
    if (event.keyCode === 13) {
      let checkEmpty = helper.RETURN_CECKSPACES(this.state.value);
      if (!this.state.value || checkEmpty) {
        this.setState({
          TaskError: true,
          TaskErrorMessage:
            checkEmpty ||
            this.props.intl.formatMessage({
              id: "project.creation-dialog.validation.title",
              defaultMessage: "Please enter project title",
            }),
        });

        return;
      }

      if (this.state.value && this.state.value.length > 80) {
        this.setState({
          TaskError: true,
          TaskErrorMessage: this.props.intl.formatMessage({
            id: "project.creation-dialog.validation.length",
            defaultMessage: "Please enter less than 80 characters",
          }),
        });
        return;
      }
      const { FetchWorkspaceInfo } = this.props;
      this.props.SaveProject(
        {
          projectName: this.state.value.replace(/\s\s+/g, " "),
          boardColorCode:
            "rgb(" +
            Math.round(Math.random() * 220) +
            "," +
            Math.round(Math.random() * 210) +
            "," +
            Math.round(Math.random() * 220) +
            ")",
        },
        x => {
          if (x && x.status === 200) {
            // FetchWorkspaceInfo(() => {});
            if (this.grid) this.grid.selectAllCheckbox.checked = false;
          } else if (x && x.status === 409) {
            this.setState({
              TaskError: true,
              TaskErrorMessage: this.props.intl.formatMessage({
                id: "project.creation-dialog.validation.exists",
                defaultMessage: "The project name is already exists",
              }),
            });
          } else if (x && x.status === 405) {
            this.setState({
              TaskError: true,
              TaskErrorMessage: this.props.intl.formatMessage({
                id: "project.creation-dialog.validation.limitexceed",
                defaultMessage:
                  "Oops! You have exceded the number of allowed projects. Please delete a project or upgrade to premium/business plan to remove the limit.",
              }),
            });
          } else {
            this.setState({
              TaskError: true,
              TaskErrorMessage: x.data.message,
            });
          }
        }
      );
      this.setState({ value: "" });
    } else if (event.keyCode == 27) {
      this.setState({ addProject: false });
    } else {
      this.setState({ addProject: true });
    }
  }

  getRandomDate = (start, end) => {
    return new Date(
      start.getTime() + Math.random() * (end.getTime() - start.getTime())
    ).toLocaleDateString();
  };

  rowGetter = i => {
    if (i >= 0) return this.state.rows[i];
  };

  isDraggedRowSelected = (selectedRows, rowDragSource) => {
    if (selectedRows && selectedRows.length > 0) {
      let key = "projectId";
      return selectedRows.filter(r => r[key] === rowDragSource.data[key]).length > 0;
    }
    return false;
  };

  reorderRows = e => {
    let selectedRows = Selectors.getSelectedRowsByKey({
      rowKey: "projectId",
      selectedKeys: this.state.selectedIds,
      rows: this.state.rows,
    });
    let draggedRows = this.isDraggedRowSelected(selectedRows, e.rowSource)
      ? selectedRows
      : [e.rowSource.data];
    let undraggedRows = this.state.rows.filter(function(r) {
      return draggedRows.indexOf(r) === -1;
    });
    let args = [e.rowTarget.idx, 0].concat(draggedRows);
    Array.prototype.splice.apply(undraggedRows, args);

    undraggedRows = undraggedRows.map((x, i) => {
      x.taskOrder = i;
      return x;
    });
    let orderToBeSaved = undraggedRows.map(x => {
      return x.id;
    });
    let itemOrder = this.props.itemOrder;
    itemOrder.itemOrder = itemOrder.itemOrder
      .map((x, i) => {
        if (orderToBeSaved.indexOf(x.itemId) >= 0) {
          return {
            itemId: x.itemId,
            position: orderToBeSaved.indexOf(x.itemId),
          };
        } else return x;
      })
      .sort((a, b) => a.position - b.position);
    this.props.SaveItemOrder(
      itemOrder,
      data => {
        this.setState({
          rows: undraggedRows,
          isReorder: true,
          showProjects: false,
        });
      },
      () => {},
      undraggedRows,
      "project"
    );
  };
  onRowsSelected = rows => {
    this.setState(
      {
        selectedIds: this.state.selectedIds.concat(rows.map(r => r.row["projectId"])),
        showProjects: false,
        selectedRows: rows,
      },
      () => {
        if (this.state.rows.length === this.state.selectedIds.length) {
          if (this.grid) this.grid.selectAllCheckbox.checked = true;
        }
      }
    );
  };

  onRowsDeselected = rows => {
    let rowIds = rows.map(r => r.row["projectId"]);
    this.setState({
      selectedIds: this.state.selectedIds.filter(i => rowIds.indexOf(i) === -1),
      showProjects: false,
    });
  };

  rowsLength = () => {
    return this.state.rows.length;
  };

  onGridRowsUpdated = ({ fromRow, toRow, updated }) => {
    this.setState(state => {
      const rows = state.rows.slice();
      for (let i = fromRow; i <= toRow; i++) {
        rows[i] = { ...rows[i], ...updated };
      }
      return { rows };
    });
  };

  handleUnarchive = ids => {
    const { selectedIds } = this.state;
    this.setState({ selectedIds: [] });
    this.props.filterProject(ids || selectedIds);
  };

  sortRows = (initialRows, sortColumn, sortDirection) => {
    if (sortDirection === "NONE") {
      this.setFormattedProjectRows();
      this.setState({ sortColumn: null, sortDirection });
    } else {
      let rows = sortListData(initialRows, sortColumn, sortDirection);
      this.setState({ rows, sortColumn, sortDirection });
    }
  };
groupByProjectManager = () => {
  return this.state.rows.reduce((r, cv) => {
    cv.projectManagers.forEach(x => {
        r.push({...cv, projectManager: x.fullName})
    })
    return r
    }, [])
}

projectCompleteConfirmOpen = () => {
  this.setState({statusCompleteDialogOpen: true})
}
projectCompleteConfirmClose = () => {
  this.setState({statusCompleteDialogOpen: false})
}

  render() {
    const {
      addProject,
      addNewForm,
      selectedIds,
      currentTask,
      isWorkspaceChanged,
      rows,
      selectedRows,
      statusCompleteBtnQuery,
      statusCompleteDialogOpen
    } = this.state;
    const { classes, theme, isArchivedSelected, projectPer, selectedGroup, intl } = this.props;
    if (this.state.rows.length > 0 && this.state.rows.length === this.state.selectedIds.length) {
      if (this.grid) this.grid.selectAllCheckbox.checked = true;
    } else {
      if (this.grid) this.grid.selectAllCheckbox.checked = false;
    }
    if (isWorkspaceChanged) {
      if (this.grid) this.grid.selectAllCheckbox.checked = false;
    }
    const EmptyRowsView = () => {
      return isArchivedSelected ? (
        <EmptyState
          screenType="Archived"
          heading={
            <FormattedMessage id="common.archived.label" defaultMessage="No archived items found" />
          }
          message={
            <FormattedMessage
              id="common.archived.message"
              defaultMessage="You haven't archived any items yet."
            />
          }
          button={false}
        />
      ) : projectPer.createProject.cando ? (
        <EmptyState
          screenType="project"
          heading={
            <FormattedMessage
              id="common.create-first.project.label"
              defaultMessage="Create your first project"
            />
          }
          message={
            <FormattedMessage
              id="common.create-first.project.message"
              defaultMessage='You do not have any projects yet. Press "Alt + P" or click on button below.'
            />
          }
          button={true}
        />
      ) : (
        <EmptyState
          screenType="project"
          heading={
            <FormattedMessage
              id="common.create-first.project.messageb"
              defaultMessage="You do not have any projects yet."
            />
          }
          message={
            <FormattedMessage
              id="common.create-first.project.messagec"
              defaultMessage="Request your team member(s) to assign you a project."
            />
          }
          button={false}
        />
      );
    };
    const pmGroupedRows = selectedGroup.key === 'projectManager' ? this.groupByProjectManager() : rows;
    const groupBy = selectedGroup.key && [selectedGroup.key];
    const groupedRows = groupBy ? Data.Selectors.getRows({ rows: pmGroupedRows, groupBy }) : rows;


    return (
      <Fragment>
          <ActionConfirmation
            open={statusCompleteDialogOpen}
            closeAction={this.projectCompleteConfirmClose}
            cancelBtnText={<FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel"/>}
            successBtnText={"Yes, Mark All Complete"}
            alignment="center"
            headingText={"Mark All Tasks Complete"}
            iconType="markAll"
            msgText={'Are you sure you want to mark all project tasks as completed'}
            successAction={() => this.handleMarkProjectAsCompleted()}
            btnQuery={statusCompleteBtnQuery}
          />
        <DefaultDialog title="Select Project" open={addNewForm} onClose={this.cancelAddProject}>
          <AddNewProject closeAction={this.cancelAddProject} />
        </DefaultDialog>
        <Hotkeys keyName="alt+p" onKeyDown={this.onKeyDown} />
        <div style={{ width: "100%" }} className={selectedIds.length > 1 ? "multiChecked" : null}>
          {selectedIds.length > 1 ? (
            <BulkActions
              selectedIdsList={this.state.selectedIds}
              handleUnarchive={this.handleUnarchive}
              isBulkUpdated={this.isUpdated}
              isArchivedSelected={isArchivedSelected}
              projects={this.props.projects}
              selectedRows={selectedRows}
            />
          ) : null}
          <div className={classes.TableActionBtnDD}>
            <ColumnSelectionDropdown
              feature={"project"}
              columnChangeCallback={this.columnChangeCallback}
            />
          </div>

          <DraggableContainer>
            <ReactDataGrid
              onRowClick={this.showProjectsPopUp}
              enableCellSelection={true}
              ref={node => (this.grid = node)}
              rowActionsCell={RowActionsCell}
              columns={this.state._columns}
              emptyRowsView={EmptyRowsView}
              onGridSort={(sortColumn, sortDirection) =>
                this.sortRows(rows, sortColumn, sortDirection)
              }
              // rowGetter={this.rowGetter}
              rowGetter={i => {
                return groupedRows[i];
              }}
              rowsCount={groupedRows.length}
              onGridRowsUpdated={this.onGridRowsUpdated}
              // rowsCount={this.rowsLength()}
              rowHeight={60}
              headerRowHeight={
                isArchivedSelected || !this.props.projectPer.createProject.cando ? 50 : 105
              }
              minHeight={calculateContentHeight()}
              selectAllCheckbox={true}
              rowRenderer={<RowRenderer onRowDrop={this.reorderRows} />}
              rowGroupRenderer={param => {
                return GroupingRowRenderer(param, pmGroupedRows, classes, intl);
              }}
              rowSelection={{
                showCheckbox: true,
                enableShiftSelect: true,
                onRowsSelected: this.onRowsSelected,
                onRowsDeselected: this.onRowsDeselected,
                selectBy: {
                  keys: {
                    rowKey: "projectId",
                    values: this.state.selectedIds,
                  },
                },
              }}
              onColumnResize={this.onColumnResize}
            />
          </DraggableContainer>
          {/* </BottomScrollListener> */}
          {isArchivedSelected ? null : this.props.projectPer.createProject.cando ? (
            <>
              <AddProject
                className={addProject ? "addTaskInputCnt" : "addTaskCnt"}
                onClick={!addProject ? () => this.addProject("inline") : undefined}>
                {addProject ? (
                  <Fragment>
                    {this.state.TaskError ? (
                      <div htmlFor="TaskError" className={classes.selectError}>
                        {this.state.TaskErrorMessage ? this.state.TaskErrorMessage : ""}
                      </div>
                    ) : null}
                    {/* <Hotkeys keyName="enter,esc" onKeyDown={this.onKeyDown}> */}
                    <input
                      ref={this.addProjectInput}
                      onKeyDown={event => this.onKeyDown(event)}
                      id="addTaskInput"
                      autoFocus={this.state.focus}
                      style={{
                        border: this.state.TaskError
                          ? `1px solid ${theme.palette.border.redBorder}`
                          : null,
                      }}
                      onBlur={this.cancelAddProject}
                      type="text"
                      onChange={this.handleChange}
                      value={this.state.value}
                    />
                    {/* </Hotkeys> */}
                    <Grid item classes={{ item: classes.addTaskInputClearCnt }}>
                      <span className={classes.addTaskInputClearText}>
                        <FormattedMessage
                          id="common.press.message"
                          defaultMessage="Press Enter to Add"
                        />
                      </span>
                      <IconButton btnType="condensed" onClick={this.cancelAddProject}>
                        <CancelIcon
                          fontSize="default"
                          htmlColor={theme.palette.secondary.light}
                        />
                      </IconButton>
                    </Grid>
                  </Fragment>
                ) : (
                  <Grid container direction="row" justify="space-between" alignItems="center">
                    <Grid item>
                      <Grid container direction="row" justify="flex-start" alignItems="center">
                        <AddIcon htmlColor={theme.palette.primary.light} />
                        <span className={classes.addNewTaskText}>
                          <FormattedMessage
                            id="project.add-new.label"
                            defaultMessage="Add New Project"
                          />
                        </span>
                      </Grid>
                    </Grid>
                    <Grid item>
                      <p className={classes.shortcutText}>
                        {" "}
                        {`${this.props.intl.formatMessage({
                          id: "common.press.label",
                          defaultMessage: "Press",
                        })} Alt + P`}
                      </p>
                    </Grid>
                  </Grid>
                )}
              </AddProject>
            </>
          ) : null}
        </div>
      </Fragment>
    );
  }
}

ProjectListOld.defaultProps={
  selectedGroup:{},
  rowKey:"projectId"
}

const mapStateToProps = (state, ownProps) => {
  return {
    loggedInTeam: state.profile.data.loggedInTeam,
    userId: state.profile.data.userId || "",
    allMembers: state.profile.data.member.allMembers || [],
    itemOrder: cloneDeep(state.itemOrder.data) || [],
    projects: state.projects.data || [],
    // This data is for calculating count of respective object linked with Project
    tasks: state.tasks.data || [],
    issues: state.issues.data || [],
    risks: state.risks.data || [],
    meetings: state.meetings.data || [],
    projectPer: state.workspacePermissions.data.project,
    workspaces: state.profile.data.workspace,
    profileState: state.profile,
    projectDetailsDialog: state.projectDetailsDialog,
    loadingProjectDetails: state.loadingProjectDetails,
    projectColumns: state.columns.project
  };
};

export default compose(
  withRouter,
  injectIntl,
  withStyles(listStyles, { withTheme: true }),
  connect(mapStateToProps, {
    UpdateTask,
    UpdateCalenderTask,
    SaveItemOrder,
    FetchTasksInfo,
    CopyTask,
    DeleteTask,
    ArchiveTask,
    UnArchiveTask,

    ////////////////////////

    UpdateProject,
    SaveProject,
    DeleteProject,
    CopyProject,
    EditProject,
    ArchiveProject,
    UnarchiveProject,
    FetchWorkspaceInfo,
    projectDetails,
    getSetLoadingProjectDetails,
    updateProject,
    markProjectCompleted
  })
)(ProjectListOld);
