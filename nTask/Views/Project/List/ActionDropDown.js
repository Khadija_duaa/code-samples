import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles, withTheme } from "@material-ui/core/styles";
import itemStyles from "./styles";
import menuStyles from "../../../assets/jss/components/menu";
import CustomIconButton from "../../../components/Buttons/IconButton";
import SelectionMenu from "../../../components/Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import combineStyles from "../../../utils/mergeStyles";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import { Scrollbars } from "react-custom-scrollbars";
import { FormattedMessage, injectIntl } from "react-intl";

let checkedItems = ["ID", "Project Title", "Tasks", "Issues", "Risks", "Meetings", "Progress"];
let Default_Column_Count = 5;
let total = Default_Column_Count;
const screenSize = 1380;
class TableActionDropDown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      pickerOpen: false,
      pickerPlacement: "",
      checked: [],
      isLoaded: false,
      loggedInTeam: "",
      isFirstLoad: false,
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
  }
  handleClose(event) {
    this.setState({ open: false, pickerOpen: false });
  }
  handleClick(event, placement) {
    const { currentTarget } = event;
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }
  componentDidMount() {
    this.setState({
      checked: checkedItems,
      loggedInTeam: this.props.profileState.data.loggedInTeam,
      isFirstLoad: true,
    });
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (
      nextProps.itemOrderState.data.projectColumnOrder &&
      nextProps.itemOrderState.data.projectColumnOrder.length
    ) {
      checkedItems = nextProps.itemOrderState.data.projectColumnOrder;
    }
    if (window.innerWidth <= screenSize) {
      checkedItems = checkedItems.slice(0, Default_Column_Count);
      total = 3;
    } else {
      total = Default_Column_Count;
    }
    if (prevState.loggedInTeam !== nextProps.profileState.data.loggedInTeam) {
      if (!prevState.checked.every(item => checkedItems.includes(item))) {
        nextProps.selectedAction(checkedItems, true);
        return {
          isLoaded: true,
          checked: checkedItems,
          loggedInTeam: nextProps.profileState.data.loggedInTeam,
          isFirstLoad: false,
        };
      }
    }

    if (!prevState.isLoaded && prevState.isFirstLoad) {
      nextProps.selectedAction(checkedItems, prevState.isFirstLoad);
      return {
        isLoaded: true,
        checked: checkedItems,
        isFirstLoad: false,
      };
    }
  }

  handleToggle = value => () => {
    const { checked } = this.state;
    const { selectedAction } = this.props;
    this.setState({
      isLoaded: true,
    });
    if (checked.length <= total + 2) {
      const currentIndex = checked.indexOf(value);
      const newChecked = [...checked];

      if (currentIndex === -1 && checked.length < total + 1) {
        newChecked.push(value);
      } else if (currentIndex !== -1) {
        // if (currentIndex === 0 ) {
        //   return;
        // }
        newChecked.splice(currentIndex, 1);
      }
      if (this.state.checked.length !== newChecked.length) {
        this.setState(
          {
            checked: newChecked,
          },
          () => {
            selectedAction(this.state.checked);
          }
        );
      }
    } else {
      return false;
    }
  };
  translate = value => {
    let id = "";
    switch (value) {
      case "ID":
        id = "common.ellipses-columns.id";
        break;
      case "Tasks":
        id = "common.ellipses-columns.tasks";
        break;
      case "Issues":
        id = "common.ellipses-columns.issues";
        break;
      case "Risks":
        id = "common.ellipses-columns.risks";
        break;
      case "Meetings":
        id = "common.ellipses-columns.meetings";
        break;
      case "Progress":
        id = "common.ellipses-columns.progress";
        break;
      case "Resources":
        id = "common.ellipses-columns.resources";
        break;
      case "Project Title":
        id = "common.ellipses-columns.projecttitle";
        break;
      case "Status":
        id = "common.ellipses-columns.status";
        break;
      default:
        id = value;
        break;
    }
    return <FormattedMessage id={id} defaultMessage={value} />;
  };
  render() {
    const { classes, theme, selectedColor, colorChange } = this.props;
    const { open, placement, checked } = this.state;
    const ddData = [
      "Project Title",
      "ID",
      "Status",
      "Tasks",
      "Issues",
      "Risks",
      "Meetings",
      "Progress",
      "Resources",
      "Project Manager",
    ];
    return (
      <ClickAwayListener onClickAway={this.handleClose}>
        <div>
          <CustomIconButton
            btnType="condensed"
            onClick={event => {
              this.handleClick(event, "bottom-end");
            }}
            buttonRef={node => {
              this.anchorEl = node;
            }}>
            <MoreVerticalIcon
              htmlColor={theme.palette.secondary.medDark}
              style={{ fontSize: "24px" }}
            />
          </CustomIconButton>

          <SelectionMenu
            open={open}
            closeAction={this.handleClose}
            placement={placement}
            checkedType="multi"
            anchorRef={this.anchorEl}
            list={
              <Scrollbars autoHide style={{ height: 250 }}>
                <List>
                  <ListItem disableRipple={true} classes={{ root: classes.menuHeadingItem }}>
                    <ListItemText
                      primary={`${checked.length - 1}/${total} ${this.props.intl.formatMessage({
                        id: "common.selected.label",
                        defaultMessage: "Selected",
                      })}`}
                      classes={{ primary: classes.menuHeadingListItemText }}
                    />
                  </ListItem>
                  {ddData.slice(1).map(value => (
                    <ListItem
                      key={value}
                      button
                      disableRipple={true}
                      className={`${classes.MenuItem} ${this.state.checked.indexOf(value) !== -1 ? classes.selectedValue : ""
                        }`}
                      classes={{ selected: classes.statusMenuItemSelected }}
                      onClick={this.handleToggle(value)}>
                      <ListItemText
                        primary={this.translate(value)}
                        classes={{
                          primary: classes.statusItemText,
                        }}
                      />
                    </ListItem>
                  ))}
                </List>
              </Scrollbars>
            }
          />
        </div>
      </ClickAwayListener>
    );
  }
}

const mapStateToProps = state => {
  return {
    profileState: state.profile,
    itemOrderState: state.itemOrder,
  };
};

export default compose(
  withRouter,
  injectIntl,
  withStyles(combineStyles(itemStyles, menuStyles), {
    withTheme: true,
  }),
  connect(mapStateToProps, {})
)(TableActionDropDown);
