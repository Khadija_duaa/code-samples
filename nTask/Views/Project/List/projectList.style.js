const projectListStyles = (theme) => ({
  taskTitleCnt: {
    display: "flex",
    alignItems: "center",
    flexDirection: "row",
    flex: 1,
    width: "100%"
  },
  newTag: {
    background: theme.palette.background.blue,
    padding: "1px 5px",
    color: theme.palette.common.white,
    fontSize: "10px !important",
    fontFamily: theme.typography.fontFamilyLato,
    marginLeft: 6,
    borderRadius: 4,
    minWidth: 35,
    textAlign: 'center'
  },
  taskTitleTextCnt: {
    // flex: 1,
    width: "calc(100% - 40px)",
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    // display: 'flex',
    // alignItems: 'center'
  },
  taskTitle: {
    fontSize: "14px !important",
    color: theme.palette.text.primary,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 500
  },
  textCenter: {
    textAlign: "center",
  },
  GantBtnWrapper: {
    marginRight: 10,
    width: 26,
    display: 'inline-block',
  },
  IconGantt: {
    fontSize: "16px !important"
  },
  taskTitleCell: {
    '& .ag-cell-value': {
      flex: 1
    },
    '& button': {
      opacity: 0
    },
    '&:hover button': {
      opacity: 1
    }
  },
  // New Styles

  fullSpannedCell: {
    padding: '0 !important',
    '& > div': {
      lineHeight: 'normal'
    }
  },
  iconBtnStyles: {
    padding: 4,
    background: theme.palette.common.white,
    border: `1px dashed #BFBFBF`,
    marginRight: 5,
    borderRadius: "100%",
    "& svg": {
      fontSize: "18px !important"
    }
  },
  systemFieldGroup: {
    background: theme.palette.background.lightGrey,
    color: 'white',
    borderRadius: '4px 0 0 0',
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 500,

  },
  sectionFieldsGroup: {
    background: theme.palette.background.lightGrey,
    color: 'white',
    borderRadius: '4px 0 0 0',
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 500,
    padding: '0 !important'
  },
  taskTitleField: {
    background: theme.palette.common.white
  },
  timeLoggedIcon: {
    fontSize: "18px !important",
    color: theme.palette.secondary.light,
    marginRight: 5,
  },
  timeLogCellCnt: {
    display: 'flex',
    alignItems: 'center',
    height: 40
  },
  meetingIcon: {
    fontSize: "17px !important",
    marginRight: 5,
  },
  issueIcon: {
    fontSize: "17px !important",
    marginRight: 5,
  },
  taskIcon: {
    fontSize: "17px !important",
    marginRight: 5,
  },
  riskIcon: {
    fontSize: "17px !important",
    marginRight: 5,
  },
  attachmentIcon: {
    fontSize: "18px !important",
    marginRight: 5,
    color: theme.palette.icon.gray400,
    transform: 'rotate(297deg)'
  },
  countsCnt: {
    height: 40,
    display: 'flex',
    alignItems: 'center',
  },
  countsText: {
    color: theme.palette.text.lightGray,
    fontFamily: theme.typography.fontFamilyLato,
    fontSize: "13px !important",
    fontWeight: theme.typography.fontWeightMedium
  },
  commentsIcon: {
    fontSize: "18px !important",
    marginRight: 5,
  },
  commentsIconColorDim: {
    color: theme.palette.icon.gray400
  },
  commentsIconColorRed: {
    color: theme.palette.error.dark,
  },
  commentsCell: {
    height: 40,
    display: 'flex',
    alignItems: 'center',
    '& p': {
      color: theme.palette.text.lightGray,
      fontFamily: theme.typography.fontFamilyLato
    }

  },
  taskColor: {
    width: 6,
    height: '100%',
    display: 'block'
  },
  taskColorCell: {
    padding: '0 !important'
  },
  taskDragCell: {
    padding: '0 !important'
  },
  taskCheckboxCell: {
    padding: '0 !important'
  },
  taskRecurrenceCell: {
    padding: '0 !important'
  },
  priorityLabel: {
    maxWidth: 70,
    justifyContent: 'flex-start'
  },
  emptyContainer: {
    height: "calc(100vh - 160px)",
    display: "flex",
  },
  taskListViewCnt: {
    display: 'flex',
    justifyContent: "flex-end"
  },
  playIcon: {
    fontSize: "14px !important"
  },
  stopIcon: {
    fontSize: "14px !important"
  },
  projectTitleLeftCnt: {
    display: 'flex',
    alignItems: 'flex-start'
  },
});

export default projectListStyles;