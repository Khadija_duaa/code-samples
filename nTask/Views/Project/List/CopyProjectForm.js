import React, { Component, Fragment } from "react";
import Grid from "@material-ui/core/Grid";
import { withSnackbar } from "notistack";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import CustomDialog from "../../../components/Dialog/CustomDialog";
import autoCompleteStyles from "../../../assets/jss/components/autoComplete";
import combineStyles from "../../../utils/mergeStyles";
import DefaultButton from "../../../components/Buttons/DefaultButton";
import CustomButton from "../../../components/Buttons/CustomButton";
import dialogFormStyles from "../../../assets/jss/components/dialogForm";
import DefaultTextField from "../../../components/Form/TextField";
import {
  SaveProject,
  FetchProjectsInfo,
} from "../../../redux/actions/projects";
import { FetchWorkspaceInfo } from "../../../redux/actions/workspace";

import helper from "../../../helper";
import { injectIntl, FormattedMessage } from "react-intl";

class CopyProjectForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "ganttView",
      projectTitle: "",
      projectTitleError: false,
      projectTitleErrorMessage: "",

      projectTaskPrefix: "Copy of",
      projectTaskPrefixError: false,
      projectTaskPrefixErrorMessage: "",
      btnQuery: "",
    };
  }

  closeDialog = (e) => {
    e.stopPropagation();
    this.props.closeAction();
  };

  handleChange = (event) => {
    this.setState({ value: event.target.value });
  };

  handleTextFieldChange = (event) => {
    if (event.target.value.length < 80)
      this.setState({ projectTitle: event.target.value });
  };

  handlePrefixTextFieldChange = (event) => {
    if (event.target.value.length < 80)
      this.setState({ projectTaskPrefix: event.target.value });
  };

  validate = () => {
    let errorCheck_projectTitle = true;
    let errorCheck_projectTaskPrefix = true;
    if (helper.HELPER_EMPTY("Project Title", this.state.projectTitle)) {
      this.setState({
        projectTitleError: true,
        projectTitleErrorMessage: this.props.intl.formatMessage({ id: "project.creation-dialog.validation.required", defaultMessage: "Project title is required." }),
      });
      errorCheck_projectTitle = false;
    } else {
      let checkMessage = helper.HELPER_CHARLIMIT80(
        "Project Title",
        this.state.projectTitle
      );
      if (checkMessage) {
        this.setState({
          projectTitleError: true,
          projectTitleErrorMessage: checkMessage,
        });
        errorCheck_projectTitle = false;
      }
    }
    if (!helper.HELPER_EMPTY("Project Title", this.state.projectTaskPrefix)) {
    }
    return errorCheck_projectTitle;
  };

  FetchWorkSpace = () => {
    this.props.FetchWorkspaceInfo("", () => {
      this.setState({
        projectTitle: "",
        projectTitleError: false,
        projectTitleErrorMessage: "",
      });
      this.props.closeAction();
      this.setState({ btnQuery: "" });
    });
  };

  handleSubmit = (e) => {
    e.stopPropagation();
    if (this.validate()) {
      this.setState({ btnQuery: "progress" });
      this.props.copyProject(
        {
          projectID: this.props.project.projectId,
          copiedProjectName: `${this.state.projectTaskPrefix} ${this.state.projectTitle}`,
          appendTask: this.state.projectTaskPrefix,
        },
        (response) => {
          // if (response.status === 200) {
          this.FetchWorkSpace();
          // } else if (response.status === 400) {
          // this.setState({ btnQuery: "", projectTitleError: true, projectTitleErrorMessage: "The project name is already exists"});
          // }
        },
        fail => {
          if (fail.status === 409) {
            this.setState({ btnQuery: "", projectTitleError: true, projectTitleErrorMessage: "The project name is already exists" });
          } else {
            this.setState({ btnQuery: "" });
            this.showSnackBar(fail.data.message, "error");
          }
        }
      );
    }
  };

  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };

  render() {
    const { classes, theme, open, closeAction } = this.props;
    const {
      projectTitleError,
      projectTitleErrorMessage,
      projectTitle,
      projectTaskPrefix,
      projectTaskPrefixError,
      projectTaskPrefixErrorMessage,
      btnQuery,
    } = this.state;

    return (
      <Fragment>
        <CustomDialog
          title={
            <FormattedMessage
              id="project.copy.label"
              defaultMessage="Copy Project"
            />
          }
          dialogProps={{
            open: open,
            onClose: closeAction,
            onClick: (e) => e.stopPropagation(),
          }}
        >
          <div className={classes.dialogFormCnt} noValidate autoComplete="off">
            <DefaultTextField
              label={
                <FormattedMessage
                  id="project.creation-dialog.project-title.label"
                  defaultMessage="Project Title"
                />
              }
              errorState={projectTitleError}
              errorMessage={projectTitleErrorMessage}
              defaultProps={{
                type: "text",
                id: "projectTitle",
                placeholder: this.props.intl.formatMessage({
                  id: "project.creation-dialog.project-title.palceholder",
                  defaultMessage: "Add Project Title",
                }),
                value: projectTitle,
                autoFocus: true,
                inputProps: { maxLength: 80 },
                onChange: (event) => {
                  this.handleTextFieldChange(event, "projectTitle");
                },
              }}
            />

            <DefaultTextField
              label={
                <FormattedMessage
                  id="project.prefix"
                  defaultMessage="Prefix"
                />
              }
              errorState={projectTaskPrefixError}
              errorMessage={projectTaskPrefixErrorMessage}
              defaultProps={{
                type: "text",
                id: "projectTaskPrefix",
                placeholder: "Copy of",
                value: projectTaskPrefix,
                inputProps: { maxLength: 80 },
                onChange: (event) => {
                  this.handlePrefixTextFieldChange(event, "projectTaskPrefix");
                },
              }}
            />
          </div>
          <Grid
            container
            direction="row"
            justify="flex-end"
            alignItems="center"
            classes={{ container: classes.dialogFormActionsCnt }}
          >
            <DefaultButton
              onClick={(e) => this.closeDialog(e)}
              text={
                <FormattedMessage
                  id="common.action.cancel.label"
                  defaultMessage="Cancel"
                />
              }
              buttonType="Transparent"
              style={{ marginRight: 20 }}
            />
            <CustomButton
              onClick={(e) => this.handleSubmit(e)}
              btnType="success"
              variant="contained"
              query={btnQuery}
              disabled={btnQuery == "progress"}
            >
              <FormattedMessage
                id="project.creation-dialog.create-button.label"
                defaultMessage="Create Project"
              />
            </CustomButton>
          </Grid>
        </CustomDialog>
      </Fragment>
    );
  }
}

export default compose(
  withRouter,
  injectIntl,
  withSnackbar,
  withStyles(combineStyles(autoCompleteStyles, dialogFormStyles), {
    withTheme: true,
  }),
  connect(null, {
    SaveProject,
    FetchProjectsInfo,
    FetchWorkspaceInfo,
  })
)(CopyProjectForm);
