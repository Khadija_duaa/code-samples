import React from "react";
import getTaskByStatusType from "../../../helper/getTaskByStatusType";
import getProjectByProjectManager from "../../../helper/getProjectByProjectManager";
import Typography from "@material-ui/core/Typography";

let status = ["Not Started", "In Progress", "On Hold", "Completed", "Cancelled"];
let priority = [
  "", // Set first value empty to get right index because priority values are 1-4 in backend
  "Critical",
  "High",
  "Medium",
  "Low",
];
function translatedText(name, intl) {
  switch (name) {
    case "Not Started":
      name = intl.formatMessage({
        id: "task.common.status.dropdown.not-started",
        defaultMessage: "Not Started",
      });
      break;
    case "In Progress":
      name = intl.formatMessage({
        id: "task.common.status.dropdown.in-progress",
        defaultMessage: "In Progress",
      });
      break;
    case "In Review":
      name = intl.formatMessage({
        id: "task.common.status.dropdown.in-review",
        defaultMessage: "In Review",
      });
      break;
    case "Completed":
      name = intl.formatMessage({
        id: "task.common.status.dropdown.completed",
        defaultMessage: "Completed",
      });
      break;
    case "Cancelled":
      name = intl.formatMessage({
        id: "task.common.status.dropdown.cancel",
        defaultMessage: "Cancelled",
      });
      break;
    case "Critical":
      name = intl.formatMessage({
        id: "task.common.priority.dropdown.critical",
        defaultMessage: "Critical",
      });
      break;
    case "High":
      name = intl.formatMessage({
        id: "task.common.priority.dropdown.high",
        defaultMessage: "High",
      });
      break;
    case "Low":
      name = intl.formatMessage({ id: "task.common.priority.dropdown.low", defaultMessage: "Low" });
      break;
    case "Medium":
      name = intl.formatMessage({
        id: "task.common.priority.dropdown.medium",
        defaultMessage: "Medium",
      });
      break;
    case "":
      name = intl.formatMessage({ id: "common.others.label", defaultMessage: "Others" });
      break;
  }
  return name;
}
export function GroupingRowRenderer(param, projects, classes, intl = null) {
  // Grouping row renderer
  const { name, columnGroupName } = param;
  let projectCount;
  let value;
  switch (
    columnGroupName // Switch statement used to calculate countes of the projects according to the grouping selected
  ) {
    case "status":
      {
        projectCount = getTaskByStatusType(projects, name); //Function to return projects based on task status
        value = intl != null ? translatedText(status[name],intl) : status[name]
      }
      break;
      case "projectManager":
        {
          projectCount = getProjectByProjectManager(projects, name)
          value = name
        }
    default:
      break;
  }
  return (
    <div className={classes.groupingRow} {...param.renderer}>
      <div className={classes.groupingRowTextCnt}>
        <Typography variant="body1" className={classes.groupingRowText}>
          {value == "null" || !value
            ? "Others"
            : `${value}`}
        </Typography>
        <span className={classes.groupingCount}>
          {projectCount && projectCount > 999 ? "999+" : projectCount ? projectCount.length : null}
        </span>
      </div>
    </div>
  );
}
