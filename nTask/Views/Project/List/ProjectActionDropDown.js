import React, { Component, Fragment } from "react";
import { withStyles, withTheme } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import itemStyles from "./styles";
import menuStyles from "../../../assets/jss/components/menu";
import CustomIconButton from "../../../components/Buttons/IconButton";
import SelectionMenu from "../../../components/Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import combineStyles from "../../../utils/mergeStyles";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import RightArrow from "@material-ui/icons/ArrowRight";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import DeleteConfirmation from "../../../components/Dialog/Popups/DeleteConfirmation";
import CopyProjectForm from "./CopyProjectForm";
import EditProjectForm from "./EditProjectForm";
import { canEdit, canDo } from "../permissions";
import DeleteConfirmDialog from "../../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import DeleteBulkProjectDialogContent from "../../../components/Dialog/Popups/bulkDeleteConfirmation";

import MoveProjectDialog from "../../../components/Dialog/MoveProjectDialog/MoveProjectDialog";
import { moveProjectToWorkspace } from "../../../redux/actions/projects";
import { FetchWorkspaceInfo } from "../../../redux/actions/workspace";
import { GetPermission } from "../../../components/permissions";
import ColorPicker from "../../../components/Dropdown/ColorPicker/ColorPicker";
import { injectIntl, FormattedMessage } from "react-intl";

class ProjectActionDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      pickerOpen: false,
      pickerPlacement: "",
      archiveFlag: false,

      unArchiveFlag: false,
      deleteFlag: false,
      copyFlag: false,
      renameFlag: false,
      archiveBtnQuery: "",
      deleteBtnQuery: "",
      unarchiveBtnQuery: "",
      moveProjectBtnQuery: "",
      moveProjectDialogue: false,
      step: 0,
      disabled: true,
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleColorClick = this.handleColorClick.bind(this);
    this.handlePickerClose = this.handlePickerClose.bind(this);
    this.handleDialogClose = this.handleDialogClose.bind(this);
  }

  componentDidMount = () => {
    this.setState({
      selectedColor: this.props.selectedColor,
    });
  };

  handlePopupDialogClose = (event, name) => {
    this.setState({ renameFlag: false, copyFlag: false });
  };

  handleOperations = (event, value) => {
    event.stopPropagation();
    switch (value) {
      case "Rename":
        //this.props.CopyTask(this.props.meeting);
        this.setState({ renameFlag: true });
        break;
      case "Copy":
        this.setState({ copyFlag: true });
        break;
      case "Public Link":
        break;
      case "Move":
        this.setState({ moveProjectDialogue: true });
        break;
      case "Archive":
        this.setState({ archiveFlag: true, popupFlag: true });
        break;
      case "Unarchive":
        this.setState({ unArchiveFlag: true, popupFlag: true });
        break;
      case "Delete":
        this.setState({ deleteFlag: true, popupFlag: true });
        break;
    }

    this.setState({ open: false, pickerOpen: false });
  };
  handleMoveProjectDialogueClose = () => {
    this.setState({ moveProjectDialogue: false });
  };
  handleClose(event) {
    this.setState({ open: false, pickerOpen: false });
  }
  handlePickerClose(event) {
    this.setState({ pickerOpen: false });
  }
  handleDialogClose(event) {
    if (event) event.stopPropagation();
    this.setState({
      popupFlag: false,
      archiveFlag: false,
      unArchiveFlag: false,
      deleteFlag: false,
    });
  }

  closeConfirmationPopup = () => {
    this.setState({ deleteFlag: false, step: 0 });
  };

  handleClick(event, placement) {
    event.stopPropagation();
    const { currentTarget } = event;
    this.setState((state) => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }
  handleColorClick(event) {
    event.stopPropagation();
    if (event.target.closest("#colorPickerCnt") == null) {
      this.setState((prevState) => ({ pickerOpen: !prevState.pickerOpen }));
    }
  }

  colorChange = (color, project) => {
    //event.stopPropagation();
    let self = this;
    this.setState({ selectedColor: color }, function () {
      let obj = Object.assign({}, project);
      // this.setState({
      //   selectedColor: color
      // });
      obj.colorCode = color;
      this.props.SaveProject(obj, () => { });
      //this.props.isUpdated(obj, this.props.meeting);
    });
  };

  handleArchive = (e) => {
    if (e) e.stopPropagation();
    this.setState({ archiveBtnQuery: "progress" }, () => {
      this.props.ArchiveProject(this.props.project.projectId, (response) => {
        this.setState({ archiveBtnQuery: "", archiveFlag: false });
        if (response.status === 200)
          this.props.FetchWorkSpaceData("", () => { });
      });
    });
  };

  handleUnArchive = (e) => {
    if (e) e.stopPropagation();
    this.setState({ unarchiveBtnQuery: "progress" }, () => {
      this.props.UnarchiveProject(this.props.project.projectId, (response) => {
        this.setState({ unarchiveBtnQuery: "", unArchiveFlag: false });
        if (response.status === 200) {
          this.props.handleUnarchive([this.props.project.projectId]);
          this.props.FetchWorkSpaceData("", () => { });
        }
      });
    });
  };

  handleMoveProject = (e, team) => {
    if (e) e.stopPropagation();
    this.setState({ moveProjectBtnQuery: "progress" }, () => {
      let data = {
        projectId: this.props.project.projectId,
        workspaceId: team.teamId,
      };

      this.props.moveProjectToWorkspace(
        data,
        (response) => {
          this.setState({
            moveProjectBtnQuery: "",
            moveProjectDialogue: false,
          });
          this.props.FetchWorkSpaceData("", () => { });
        },
        (error) => {
          this.setState({
            moveProjectBtnQuery: "",
            // exceptionText: error.data.message
          });
          this.props.showSnackbar(error.data.message, "error");
        }
      );
    });
  };
  handleDeleteProjectStep = (value) => {
    this.setState({ step: value });
  };

  handleDeleteProject = (event) => {
    event.stopPropagation();
    this.setState({ deleteBtnQuery: "progress" });
    this.props.DeleteProject(this.props.project.projectId, (response) => {
      this.setState({ deleteBtnQuery: "" });
      this.closeConfirmationPopup();
      if (response.status === 200)
        this.props.FetchWorkSpaceData("", () => {
          this.setState({ disabled: true, deleteBtnQuery: "" });
        });
      else {
        this.props.showSnackbar(response.data.message, "error");
      }
    });
  };
  handleTypeProjectInput = (event) => {
    let value = event.target.value;
    const { project } = this.props;
    project &&
      project.projectName.replace(/\s+/g, " ").toLowerCase() ===
      value.trim().toLowerCase()
      ? this.setState({ disabled: false })
      : this.setState({ disabled: true });
  };
  getTranslatedId(value) {
    switch (value) {
      case "Color":
        value = "common.action.color.label";
        break;
      case "Archive":
        value = "common.action.archive.confirmation.archive-button.label";
        break;
      case "Delete":
        value = "common.action.delete.confirmation.delete-button.label";
        break;
      case "Unarchive":
        value = "common.action.un-archive.confirmation.title";
        break;
      case "Settings":
        value = "common.action.settings.label";
        break;
      case "Move":
        value = "common.action.move.label";
        break;
      case "Copy":
        value = "common.action.copy.label";
        break;
      case "Rename":
        value = "common.action.rename.label";
        break;
    }
    return value;
  }
  render() {
    const {
      classes,
      theme,
      project,
      openProjectSetting,
      permissions,
      isArchivedSelected,
      userId,
      profileState,
      projectPer,
    } = this.props;
    const {
      open,
      placement,
      pickerOpen,
      popupFlag,
      deleteFlag,
      archiveFlag,
      unArchiveFlag,
      selectedColor,
      renameFlag,
      copyFlag,
      archiveBtnQuery,
      deleteBtnQuery,
      unarchiveBtnQuery,
      moveProjectDialogue,
      moveProjectBtnQuery,
      step,
      disabled,
    } = this.state;
    const ddData = isArchivedSelected
      ? ["Unarchive", "Delete"]
      : [
        "Rename",
        "Copy",
        this.props.teams.length ? "Move" : null,
        "Delete",
        "Archive",
        "Color",
        //"Public Link",
        // "Settings"
      ].filter(Boolean);

    const isProjectManager = project.projectManager
      ? project.projectManager.indexOf(userId) > -1
      : false;
    return (
      <Fragment>
        <ClickAwayListener onClickAway={this.handleClose}>
          <div>
            <CustomIconButton
              btnType="condensed"
              onClick={(event) => {
                this.handleClick(event, "bottom-end");
              }}
              buttonRef={(node) => {
                this.anchorEl = node;
              }}
            >
              <MoreVerticalIcon
                htmlColor={theme.palette.secondary.medDark}
                style={{ fontSize: "24px" }}
              />
            </CustomIconButton>

            <SelectionMenu
              open={open}
              closeAction={this.handleClose}
              placement={placement}
              anchorRef={this.anchorEl}
              list={
                <List
                  onClick={(event) => {
                    event.stopPropagation();
                  }}
                >
                  <ListItem
                    disableRipple={true}
                    classes={{ root: classes.menuHeadingItem }}
                  >
                    <ListItemText
                      primary={
                        <FormattedMessage
                          id="common.action.label"
                          defaultMessage="Select Action"
                        />
                      }
                      classes={{ primary: classes.menuHeadingListItemText }}
                    />
                  </ListItem>
                  {ddData.map((value) =>
                    value === "Color" ? (
                      <ListItem
                        key={value}
                        button
                        disableRipple={true}
                        classes={{
                          root: classes.selectColorMenuItem,
                          selected: classes.statusMenuItemSelected,
                        }}
                        onClick={(event) => {
                          this.handleColorClick(event);
                        }}
                      >
                        <ListItemText
                          primary={
                            <FormattedMessage
                              id={this.getTranslatedId(value)}
                              defaultMessage={value}
                            />
                          }
                          classes={{
                            primary: classes.statusItemText,
                          }}
                        />
                        <RightArrow
                          htmlColor={theme.palette.secondary.dark}
                          classes={{ root: classes.submenuArrowBtn }}
                        />
                        <div
                          id="colorPickerCnt"
                          className={classes.colorPickerCntLeft}
                          style={
                            pickerOpen ? { display: "block" } : { display: "none" }
                          }
                        >
                          <ColorPicker
                            triangle="hide"
                            onColorChange={(color) => {
                              this.colorChange(color, this.props.project);
                            }}
                            selectedColor={selectedColor}
                          />
                        </div>
                      </ListItem>
                    ) : value === "Settings" ? (
                      isProjectManager || projectPer && projectPer.projectDetails.cando ? (
                        <ListItem
                          key={value}
                          button
                          disableRipple={true}
                          classes={{
                            selected: classes.statusMenuItemSelected,
                          }}
                          onClick={(event) =>
                            openProjectSetting(
                              event,
                              project.projectId,
                              project.projectName,
                              project
                            )
                          }
                        >
                          <ListItemText
                            primary={
                              <FormattedMessage
                                id={this.getTranslatedId(value)}
                                defaultMessage={value}
                              />
                            }
                            classes={{
                              primary: classes.statusItemText,
                            }}
                          />
                        </ListItem>
                      ) : null
                    ) : value == "Rename" ? (
                      projectPer && projectPer.projectDetails.editProjectName.isAllowEdit ? (
                        <ListItem
                          key={value}
                          button
                          disableRipple={true}
                          classes={{ selected: classes.statusMenuItemSelected }}
                          onClick={(event) => this.handleOperations(event, value)}
                        >
                          <ListItemText
                            primary={
                              <FormattedMessage
                                id={this.getTranslatedId(value)}
                                defaultMessage={value}
                              />
                            }
                            classes={{
                              primary: classes.statusItemText,
                            }}
                          />
                        </ListItem>
                      ) : null
                    ) : value == "Copy" ? (
                      projectPer && projectPer.copyInWorkSpace.cando ? (
                        <ListItem
                          key={value}
                          button
                          disableRipple={true}
                          classes={{ selected: classes.statusMenuItemSelected }}
                          onClick={(event) => this.handleOperations(event, value)}
                        >
                          <ListItemText
                            primary={
                              <FormattedMessage
                                id={this.getTranslatedId(value)}
                                defaultMessage={value}
                              />
                            }
                            classes={{
                              primary: classes.statusItemText,
                            }}
                          />
                        </ListItem>
                      ) : null
                    ) : value == "Move" ? (
                      projectPer && projectPer.moveProject.cando && (
                        <ListItem
                          key={value}
                          button
                          disableRipple={true}
                          classes={{ selected: classes.statusMenuItemSelected }}
                          onClick={(event) => this.handleOperations(event, value)}
                        >
                          <ListItemText
                            primary={
                              <FormattedMessage
                                id={this.getTranslatedId(value)}
                                defaultMessage={value}
                              />
                            }
                            classes={{
                              primary: classes.statusItemText,
                            }}
                          />
                        </ListItem>
                      )
                    ) : value == "Archive" ? (
                      projectPer && projectPer.archive.cando ? (
                        <ListItem
                          key={
                            value === "Archive"
                              ? project.isDeleted === true
                                ? "Unarchive"
                                : "Archive"
                              : value
                          }
                          button
                          disableRipple={true}
                          classes={{ selected: classes.statusMenuItemSelected }}
                          onClick={(event) =>
                            this.handleOperations(
                              event,
                              value === "Archive"
                                ? project.isDeleted === true
                                  ? "Unarchive"
                                  : "Archive"
                                : value
                            )
                          }
                        >
                          <ListItemText
                            primary={
                              <FormattedMessage
                                id={this.getTranslatedId(value)}
                                defaultMessage={value}
                              />
                            }
                            classes={{
                              primary: classes.statusItemText,
                            }}
                          />
                        </ListItem>
                      ) : null
                    ) : value == "Delete" ? (
                      projectPer && projectPer.delete.cando ? (
                        <ListItem
                          key={value}
                          button
                          disableRipple={true}
                          classes={{ selected: classes.statusMenuItemSelected }}
                          onClick={(event) => this.handleOperations(event, value)}
                        >
                          <ListItemText
                            primary={
                              <FormattedMessage
                                id={this.getTranslatedId(value)}
                                defaultMessage={value}
                              />
                            }
                            classes={{
                              primary: classes.statusItemText,
                            }}
                          />
                        </ListItem>
                      ) : null
                    ) : value == "Unarchive" ? (
                      projectPer && projectPer.unarchive.cando && (
                        <ListItem
                          key={value}
                          button
                          disableRipple={true}
                          classes={{ selected: classes.statusMenuItemSelected }}
                          onClick={(event) => this.handleOperations(event, value)}
                        >
                          <ListItemText
                            primary={
                              <FormattedMessage
                                id={this.getTranslatedId(value)}
                                defaultMessage={value}
                              />
                            }
                            classes={{
                              primary: classes.statusItemText,
                            }}
                          />
                        </ListItem>
                      )
                    ) : (
                      <ListItem
                        key={value}
                        button
                        disableRipple={true}
                        classes={{ selected: classes.statusMenuItemSelected }}
                        onClick={(event) => this.handleOperations(event, value)}
                      >
                        <ListItemText
                          primary={
                            <FormattedMessage
                              id={this.getTranslatedId(value)}
                              defaultMessage={value}
                            />
                          }
                          classes={{
                            primary: classes.statusItemText,
                          }}
                        />
                      </ListItem>
                    )
                  )}
                </List>
              }
            />
          </div>
        </ClickAwayListener>

        <MoveProjectDialog
          open={moveProjectDialogue}
          projectName={this.props.project.projectName}
          successAction={this.handleMoveProject}
          closeAction={this.handleMoveProjectDialogueClose}
          btnQuery={moveProjectBtnQuery}
          cancelBtnText={
            <FormattedMessage
              id="common.action.cancel.label"
              defaultMessage="Cancel"
            />
          }
          exceptionText={this.state.exceptionText}
          headingText={
            <FormattedMessage
              id="project.move.label"
              defaultMessage="Move Project to Workspace"
            />
          }
        />

        <ActionConfirmation
          open={archiveFlag}
          closeAction={this.handleDialogClose}
          cancelBtnText={
            <FormattedMessage
              id="common.action.cancel.label"
              defaultMessage="Cancel"
            />
          }
          successBtnText={
            <FormattedMessage
              id="common.action.archive.confirmation.archive-button.label"
              defaultMessage="Archive"
            />
          }
          alignment="center"
          headingText={
            <FormattedMessage
              id="common.action.archive.confirmation.archive-button.label"
              defaultMessage="Archive"
            />
          }
          iconType="archive"
          msgText={
            <FormattedMessage
              id="common.archived.projects.messageb"
              defaultMessage="Are you sure you want to archive this project?"
            />
          }
          successAction={this.handleArchive}
          btnQuery={archiveBtnQuery}
        />

        <ActionConfirmation
          open={unArchiveFlag}
          closeAction={this.handleDialogClose}
          cancelBtnText={
            <FormattedMessage
              id="common.action.cancel.label"
              defaultMessage="Cancel"
            />
          }
          successBtnText={
            <FormattedMessage
              id="common.action.un-archive.confirmation.title"
              defaultMessage="Unarchive"
            />
          }
          alignment="center"
          iconType="unarchive"
          headingText={
            <FormattedMessage
              id="common.action.un-archive.confirmation.title"
              defaultMessage="Unarchive"
            />
          }
          msgText={
            <FormattedMessage
              id="common.un-archived.projects.messageb"
              defaultMessage="Are you sure you want to unarchive this project?"
            />
          }
          successAction={this.handleUnArchive}
          btnQuery={unarchiveBtnQuery}
        />

        <DeleteConfirmDialog
          open={deleteFlag}
          closeAction={this.closeConfirmationPopup}
          cancelBtnText={
            <FormattedMessage
              id="common.action.cancel.label"
              defaultMessage="Cancel"
            />
          }
          successBtnText={
            step == 0 ? (
              <FormattedMessage
                id="common.action.delete.anyway.label"
                defaultMessage="Delete Anyway"
              />
            ) : (
              <FormattedMessage
                id="common.action.delete.confirmation.delete-button.label"
                defaultMessage="Delete"
              />
            )
          }
          alignment="center"
          headingText={
            <FormattedMessage
              id="common.action.delete.confirmation.delete-button.label"
              defaultMessage="Delete"
            />
          }
          disabled={step === 0 ? false : disabled}
          btnQuery={deleteBtnQuery}
          successAction={
            step === 0
              ? () => {
                this.handleDeleteProjectStep(1);
              }
              : () => {
                this.handleDeleteProject(event);
              }
          }
        >
          {deleteFlag ? (
            <DeleteBulkProjectDialogContent
              step={step}
              project={project}
              handleTypeProjectInput={this.handleTypeProjectInput}
            />
          ) : null}
        </DeleteConfirmDialog>

        <CopyProjectForm
          open={copyFlag}
          closeAction={this.handlePopupDialogClose}
          project={project}
          CopyProject={this.props.CopyProject}
          FetchWorkSpaceData={this.props.FetchWorkSpaceData}
        />

        <EditProjectForm
          open={renameFlag}
          closeAction={this.handlePopupDialogClose}
          project={project}
          EditProject={this.props.EditProject}
          showSnackbar={this.props.showSnackbar}
        />
      </Fragment>
    );
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    permissions: state.workspacePermissions.data.project,
    userId: state.profile.data.userId,
    teams: state.profile.data.workspace || [],
    profileState: state.profile.data,
    // projectPer: state.workspacePermissions.data.project,
  };
};
export default compose(
  injectIntl,
  withStyles(combineStyles(itemStyles, menuStyles), { withTheme: true }),
  connect(mapStateToProps, {
    FetchWorkspaceInfo,
    moveProjectToWorkspace
  })
)(ProjectActionDropdown);
