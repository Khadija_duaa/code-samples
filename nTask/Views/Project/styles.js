import bgPattern from "../../assets/images/unplanned/dots_pattern.png"
const drawerWidth = 350;
const dashboardStyles = theme => ({
  projectDashboardHeader: {
    display: 'flex',
    margin: '14px 0 10px 0',
    padding: '0 20px 0 28px',
    alignItems: "center",
    justifyContent: 'space-between',
  },
  projectHeaderLeftCnt: {
    display: "flex",
    alignItems: "center",
  },
  selectHighlightItemIcon: {
    fontSize: "1.5rem !important"
  },
  toggleContainer: {
    marginLeft: 20
  },
  count: {
    fontSize: "12px !important",
    padding: "1px 6px 1px 5px",
    fontWeight: 400,
    borderRadius: 36,
    // minWidth: 26,
    minHeight: 16,
    border: "1px solid #BFBFBF"
  },
  taskDashboardCnt: {
    padding: '30px 0 0 0'
  },
  taskDashboardHeader: {
    // margin: "0 0 15px 0",
    // padding: "0 20px 0 62px",
    flexWrap: 'nowrap'
  },
  className: {},

  backArrowIcon: {
    fontSize: "28px !important",
    marginRight: 10,
    cursor: "pointer"
  },
  toggleBtnGroup: {
    display: 'flex',
    flexWrap: 'nowrap',
    borderRadius: 4,
    background: theme.palette.common.white,
    // border: `1px solid ${theme.palette.border.lightBorder}`,
    boxShadow: "none",
    "& $toggleButtonSelected": {
      color: theme.palette.text.secondary,
      backgroundColor: theme.palette.common.white,
      "&:after": {
        background: theme.palette.common.white
      },
      "&:hover": {
        background: theme.palette.common.white
      },
    },
  },
  groupedHorizontal:{
    borderLeft: `1px solid ${theme.palette.border.lightBorder} !important`
  },
  toggleButton: {
    height: "auto",
    padding: "4px 20px 5px",
    fontSize: "12px !important",
    fontWeight: theme.palette.fontWeightMedium,
    textTransform: "capitalize",
    "&:hover": {
      background: theme.palette.common.white
    },
    "&[value = 'grid']": {
      borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
      borderRight: `1px solid ${theme.palette.border.lightBorder}`
    }
  },
  toggleButtonSelected: {},
  advFilterButtonCnt: {},
  filterIcon: {
    fontSize: "18px !important"
  },
  qckFilterIconSvg: {
    color: theme.palette.secondary.medDark,
  },
  qckFilterIconSelected: {
    color: theme.palette.text.azure
  },
  qckFilterLbl: {
    paddingRight: 4,
    fontSize: "12px !important",
    color: '#7e7e7e',
  },
  qckFilterLblSelected: {
    paddingRight: 4
  },
  checkname: {
    paddingLeft: 0,
  },
  "@media (max-width: 1024px)": {
    qckFilterLbl: {
      display: 'none',
      paddingRight: 0,
    },
    qckFilterLblSelected: {
      display: 'none',
    },
    checkname: {
      paddingLeft: 0,
    },
  },
  importExportButtonCnt: {
    margin: "0 0 0 8px"
  },
  importExportIcon: {
    fontSize: "1.5rem !important"
    // fontSize: 29
  },
  fullScreenIcon: {
    fontSize: "19px !important"
  },
  filterDDText: {
    fontSize: "12px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightRegular
  },
  taskGridCnt: {
    padding: "0 30px 0 60px",
    flex: 1,
    overflowY: "auto",
    display: "flex"
  },
  taskCalendarCnt: {
    padding: "0 30px 0 60px",
    width: "100%",
    overflowY: "auto"
  },
  taskListCnt: {
    width: "100%"
  },
  listViewHeading: {
    marginRight: 5,
    fontSize: "18px !important",
    color: "#333333",
    fontWeight: 700,
    fontFamily: theme.typography.fontFamilyLato
  },
  content: {
    flexGrow: 1,
    // transition: theme.transitions.create("margin", {
    //   easing: theme.transitions.easing.sharp,
    //   duration: theme.transitions.duration.leavingScreen
    // }),
    marginRight: -drawerWidth
  },
  contentShift: {
    // transition: theme.transitions.create("margin", {
    //   easing: theme.transitions.easing.easeOut,
    //   duration: theme.transitions.duration.enteringScreen
    // }),
    // marginRight: 0
  },
  drawerHeader: {
    display: "flex",
    alignItems: "center",
    ...theme.mixins.toolbar,
    justifyContent: "flex-start"
  },
  hide: {
    display: "none"
  },
  root: {
    display: "flex"
  },
  taskGanttCnt: {
    padding: "0 2px 0 40px",
    width: "100%"
  },
  backArrowIcon: {
    fontSize: "28px !important",
    marginRight: 10,
    cursor: "pointer"
  },
  plannedVsActualSwtichCnt: {
    borderRadius: 4,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    background: theme.palette.common.white,
    padding: "4.5px 0px 4.5px 12px",
    marginRight: 20
  },
  projectHeading: {
    '@media (max-width: 1670px)': {
      maxWidth: 500,
      minWidth: 250,
      overflow: "hidden",
      fontSize: "18px !important",
      color: theme.palette.text.primary,
      whiteSpace: "nowrap",
      textOverflow: "ellipsis"
    },
    '@media (max-width: 1200px)': {
      maxWidth: 300,
      overflow: "hidden",
      color: theme.palette.text.primary,
      fontSize: "18px !important",
      whiteSpace: "nowrap",
      textOverflow: "ellipsis"
    }
  },
  unplannedMain: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: '15px 0 23px 0',
    background: `url(${bgPattern})`,
    // height: 'calc(100vh - 130px)',
    height: '100vh',
    overflowY: "auto",
  },
  unplannedCnt: {
    width: 550,
  },
  qckFfilterIconRefresh: {
    fontSize: "12px !important",
    margin: "0px 5px"
  },

});

export default dashboardStyles;
