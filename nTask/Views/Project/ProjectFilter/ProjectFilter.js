import React, { Component } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { SaveItemOrder } from "../../../redux/actions/itemOrder";
import Drawer from "@material-ui/core/Drawer";
import { withStyles } from "@material-ui/core/styles";
import StatusDropdown from "./StatusDropdown";
import PriorityDropdown from "./PriorityDropdown";
import SeverityDropdown from "./SeverityDropdown";
import AssignedToDropdown from "./AssignedToDropdown";
import TypeDropdown from "./TypeDropdown";
import RangeDatePicker from "../../../components/DatePicker/RangeDatePicker";
import Grid from "@material-ui/core/Grid";
import DefaultSwitch from "../../../components/Form/Switch";
import DefaultButton from "../../../components/Buttons/DefaultButton";
import { Scrollbars } from "react-custom-scrollbars";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import HelpIcon from "@material-ui/icons/HelpOutline";
import BackArrow from "@material-ui/icons/KeyboardArrowLeft";
import IconButton from "../../../components/Buttons/IconButton";
import ReactResizeDetector from "react-resize-detector";
import DefaultTextField from "../../../components/Form/TextField";
import SvgIcon from "@material-ui/core/SvgIcon";
import PushPin from "../../../components/Icons/PushPin";
import { FormattedMessage, injectIntl } from "react-intl";

const drawerWidth = 350;

const styles = (theme) => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
    padding: "43px 0 0 0",
    background: theme.palette.common.white,
    position: "fixed",
    top: 108,
    display: "flex",
    height: "100%",
    zIndex: 100,
    flexDirection: "row",
    justifyContent: "stretch",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    overflowX: "hidden",
    borderTop: 0,
    borderBottom: 0,
  },
  drawerHeader: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
  },
  filterFormCnt: {
    padding: "10px 20px 40px 20px",
  },
  switchCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    paddingLeft: 12,
  },
  switchLabel: {
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    margin: 0,
    marginRight: 30,
    fontWeight: theme.typography.fontWeightRegular,
    display: "flex",
  },
  filterBottomBtns: {
    padding: "10px 20px",
    borderTop: `1px solid ${theme.palette.border.lightBorder}`,
    marginTop: 40,
    position: "fixed",
    bottom: 130,
    background: theme.palette.common.white,
    zIndex: 1,
  },
  filterItem: {
    // marginTop: 20
  },
  filterCntHeader: {
    background: "#404040",
    padding: "12px 15px",
    position: "fixed",
    top: 0,
    left: 0,
    right: 0,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  filterHeaderClearBtn: {
    textDecoration: "underline",
    color: theme.palette.common.white,
    fontSize: "12px !important",
    cursor: "pointer",
  },
  filterHeaderTextCnt: {
    display: "flex",
    alignItems: "center",
  },
  filtersHeaderText: {
    marginLeft: 10,
    color: theme.palette.common.white,
    fontWeight: theme.typography.fontWeightLight,
    fontSize: "16px !important",
  },
  filtersHeaderCloseIcon: {
    fontSize: "11px !important",
    color: "#404040",
  },
  pushPinIcon: {
    fontSize: "12px !important",
    marginRight: 5,
  },
  filtersHeaderHelpIcon: {
    fontSize: "12px !important",
    marginLeft: 5,
  },
});
class MeetingFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterCntHeight: `${window.outerHeight - 400}px`,
      saveFilter: false,
      isClear: false,
      assignees: [],
      status: [],
      priority: [],
      createdBy: [],
      severity: [],
      actualStart: { endDate: "", startDate: "" },
      plannedStart: { endDate: "", startDate: "" },
      plannedEnd: { endDate: "", startDate: "" },
      actualEnd: { endDate: "", startDate: "" },
      isDisable: true,
      loggedInTeam: "",
    };

    this.handleSwitch = this.handleSwitch.bind(this);
    this.onResize = this.onResize.bind(this);
    this.handleInput = this.handleInput.bind(this);
    this.handleSaveFilter = this.handleSaveFilter.bind(this);
    this.handleBackFilter = this.handleBackFilter.bind(this);
    this.handleFilterChange = this.handleFilterChange.bind(this);
    this.handleClearState = this.handleClearState.bind(this);
    this.handleSearchFilter = this.handleSearchFilter.bind(this);
  }
  componentDidMount() {
    this.setState({ loggedInTeam: this.props.profileState.data.loggedInTeam });
  }
  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.clearlastState) {
      return { isClear: true };
    }
    if (prevState.loggedInTeam !== nextProps.profileState.data.loggedInTeam) {
      return {
        isClear: true,
        loggedInTeam: nextProps.profileState.data.loggedInTeam,
      };
    } else {
      return { loggedInTeam: prevState.loggedInTeam };
    }
  }

  handleSearchFilter() {
    const {
      filterCntHeight,
      saveFilter,
      isClear,
      isDisable,
      ...data
    } = this.state;

    this.setState(
      {
        isDisable: true,
      },
      () => {
        this.props.searchFilter(data);
      }
    );
  }
  handleClearState(isCleared) {
    if (isCleared) {
      this.setState({
        isClear: false,
      });
    } else {
      this.setState(
        {
          isClear: true,
          isDisable: true,
          severity: [],
          assignees: [],
          status: [],
          priority: [],
          createdBy: [],
          actualStart: { endDate: "", startDate: "" },
          plannedStart: { endDate: "", startDate: "" },
          plannedEnd: { endDate: "", startDate: "" },
          actualEnd: { endDate: "", startDate: "" },
        },
        () => {
          this.props.clearFilter();
        }
      );
    }
  }
  handleFilterChange(key, value) {
    this.setState(
      {
        [key]: value,
        isDisable: false,
      },
      () => {
        if (
          this.state.assignees.length === 0 &&
          this.state.status.length === 0 &&
          this.state.priority.length === 0 &&
          this.state.severity.length === 0 &&
          this.state.actualStart.startDate === "" &&
          this.state.actualStart.endDate === "" &&
          this.state.plannedStart.startDate === "" &&
          this.state.plannedStart.endDate === "" &&
          this.state.plannedEnd.startDate === "" &&
          this.state.plannedEnd.endDate === "" &&
          this.state.actualEnd.startDate === "" &&
          this.state.actualEnd.endDate === "" &&
          this.state.createdBy.length === 0
        ) {
          this.setState(
            {
              isDisable: true,
            },
            () => {
              this.props.clearFilter();
            }
          );
        }
      }
    );
  }
  handleSaveFilter() {
    const {
      filterCntHeight,
      saveFilter,
      isClear,
      isDisable,
      ...data
    } = this.state;
    let itemOrder = this.props.itemOrderState.data;
    let projectFilter = [];
    data.defaultFilter = false;
    data.filterName = "Filter--";
    projectFilter.push(data);
    itemOrder.projectFilter = projectFilter;
    this.props.SaveItemOrder(itemOrder, (data) => {});
    this.setState({ saveFilter: true });
  }
  handleBackFilter() {
    this.setState({ saveFilter: false });
  }
  onResize() {
    this.setState({ filterCntHeight: `${window.outerHeight - 400}px` });
  }
  handleInput(event, name) {
    this.setState({ filterName: event.target.value });
  }
  handleSwitch(name) {
    this.setState((prevState) => ({
      [name]: !prevState[name],
      isDisable: false,
    }));
  }

  render() {
    const { classes, theme, open, closeAction, multipleFilters } = this.props;
    const {
      filterCntHeight,
      saveFilter,
      defaultFilterChecked,
      filterName,
      filterNameError,
      filterNameErrorMessage,
      isDisable,
    } = this.state;
    if (this.state.isClear) this.props.clearFilter();
    return (
      <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="right"
        open={open}
        // PaperProps={{
        //   style:{paddingTop: topPosition}
        // }}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <ReactResizeDetector
          handleWidth
          handleHeight
          onResize={this.onResize}
        />
        <Scrollbars autoHide style={{ width: 415, height: filterCntHeight }}>
          <div className={classes.drawerHeader}>
            <div className={classes.filterCntHeader}>
              <div className={classes.filterHeaderTextCnt}>
                <IconButton
                  btnType="grayBg"
                  onClick={saveFilter ? this.handleBackFilter : closeAction}
                  style={{ padding: 4 }}
                >
                  {saveFilter ? (
                    <BackArrow
                      classes={{ root: classes.filtersHeaderCloseIcon }}
                      htmlColor={theme.palette.secondary.medDark}
                    />
                  ) : (
                    <CloseIcon
                      classes={{ root: classes.filtersHeaderCloseIcon }}
                      htmlColor={theme.palette.secondary.medDark}
                    />
                  )}
                </IconButton>

                <Typography
                  variant="h3"
                  classes={{ h3: classes.filtersHeaderText }}
                >
                  {saveFilter ? (
                    <FormattedMessage
                      id="filters.save.label"
                      defaultMessage="Save Filter"
                    />
                  ) : (
                    <FormattedMessage
                      id="filters.label"
                      defaultMessage="Filters"
                    />
                  )}
                </Typography>
              </div>
              <span
                className={classes.filterHeaderClearBtn}
                onClick={this.handleClearState.bind(this, false)}
              >
                <FormattedMessage id="common.action.Clear.label" defaultMessage="Clear" />
              </span>
            </div>
            {saveFilter ? (
              <Grid container classes={{ container: classes.filterFormCnt }}>
                <Grid
                  item
                  classes={{ item: classes.filterItem }}
                  style={{ marginBottom: 0 }}
                >
                  <DefaultTextField
                    label="Filter Name"
                    formControlStyles={{ marginBottom: 0 }}
                    fullWidth={true}
                    errorState={filterNameError}
                    errorMessage={filterNameErrorMessage}
                    defaultProps={{
                      id: "filterName",
                      placeholder: "Enter filter name",
                      onChange: (event) =>
                        this.handleInput(event, "filterName"),
                      value: filterName,
                    }}
                  />
                </Grid>
                <Grid
                  item
                  classes={{ item: classes.filterItem }}
                  style={{ marginTop: 0 }}
                >
                  <div className={classes.switchCnt} style={{ marginLeft: 0 }}>
                    <p className={classes.switchLabel}>
                      <SvgIcon
                        viewBox="0 0 8 11.562"
                        className={classes.pushPinIcon}
                        htmlColor={theme.palette.secondary.light}
                      >
                        <PushPin />
                      </SvgIcon>
                      Make this filter my default view
                      <HelpIcon
                        classes={{ root: classes.filtersHeaderHelpIcon }}
                        htmlColor={theme.palette.secondary.medDark}
                        fontSize="small"
                      />
                    </p>
                    <DefaultSwitch
                      checked={defaultFilterChecked}
                      onChange={(event) => {
                        this.handleSwitch("defaultFilterChecked", event);
                      }}
                    />
                  </div>
                </Grid>
              </Grid>
            ) : (
              <Grid container classes={{ container: classes.filterFormCnt }}>
                <Grid item xs={12} classes={{ item: classes.filterItem }}>
                  <AssignedToDropdown
                    iscreatedBy={false}
                    handleFilterChange={this.handleFilterChange}
                    style={{ marginBottom: 20 }}
                    handleClearState={this.handleClearState}
                    isClear={this.state.isClear}
                  />
                </Grid>
                <Grid item xs={12} classes={{ item: classes.filterItem }}>
                  <AssignedToDropdown
                    iscreatedBy={true}
                    handleFilterChange={this.handleFilterChange}
                    style={{ marginBottom: 20 }}
                    handleClearState={this.handleClearState}
                    isClear={this.state.isClear}
                  />
                </Grid>
                <Grid item xs={12} classes={{ item: classes.filterItem }}>
                  <StatusDropdown
                    handleFilterChange={this.handleFilterChange}
                    style={{ marginBottom: 20 }}
                    handleClearState={this.handleClearState}
                    isClear={this.state.isClear}
                  />
                </Grid>
                <Grid item xs={12} classes={{ item: classes.filterItem }}>
                  <PriorityDropdown
                    handleFilterChange={this.handleFilterChange}
                    style={{ marginBottom: 20 }}
                    handleClearState={this.handleClearState}
                    isClear={this.state.isClear}
                  />
                </Grid>
                <Grid item xs={12} classes={{ item: classes.filterItem }}>
                  <SeverityDropdown
                    handleFilterChange={this.handleFilterChange}
                    style={{ marginBottom: 20 }}
                    handleClearState={this.handleClearState}
                    isClear={this.state.isClear}
                  />
                </Grid>
                <Grid item xs={12} classes={{ item: classes.filterItem }}>
                  <TypeDropdown
                    handleFilterChange={this.handleFilterChange}
                    style={{ marginBottom: 20 }}
                    handleClearState={this.handleClearState}
                    isClear={this.state.isClear}
                  />
                </Grid>
                <Grid item xs={12} classes={{ item: classes.filterItem }}>
                  <RangeDatePicker
                    label="Start Date (Actual)"
                    placeholder="Select Start Date (Actual)"
                    handleFilterChange={this.handleFilterChange}
                    handleClearState={this.handleClearState}
                    isClear={this.state.isClear}
                    type="actualStart"
                  />
                </Grid>
                <Grid item xs={12} classes={{ item: classes.filterItem }}>
                  <RangeDatePicker
                    label="Start Date (Planned)"
                    placeholder="Select Start Date (Planned)"
                    handleFilterChange={this.handleFilterChange}
                    handleClearState={this.handleClearState}
                    isClear={this.state.isClear}
                    type="plannedStart"
                  />
                </Grid>
                <Grid item xs={12} classes={{ item: classes.filterItem }}>
                  <RangeDatePicker
                    label="Due Date (Actual)"
                    placeholder="Select Due Date (Actual)"
                    handleFilterChange={this.handleFilterChange}
                    handleClearState={this.handleClearState}
                    isClear={this.state.isClear}
                    type="actualEnd"
                  />
                </Grid>
                <Grid item xs={12} classes={{ item: classes.filterItem }}>
                  <RangeDatePicker
                    label="Due Date (Planned)"
                    placeholder="Select Due Date (Planned)"
                    handleFilterChange={this.handleFilterChange}
                    handleClearState={this.handleClearState}
                    isClear={this.state.isClear}
                    type="plannedEnd"
                  />
                </Grid>
              </Grid>
            )}

            <Grid
              container
              direction="row"
              justify="flex-end"
              alignItems="center"
              classes={{ container: classes.filterBottomBtns }}
            >
              <DefaultButton
                text="Save Filter"
                buttonType="Transparent"
                style={{ flex: 1, marginRight: 20 }}
                onClick={this.handleSaveFilter}
                disabled={isDisable}
              />
              <DefaultButton
                text="Search"
                buttonType="Plain"
                style={{ flex: 1 }}
                onClick={this.handleSearchFilter}
                disabled={isDisable}
              />
            </Grid>
          </div>
        </Scrollbars>
      </Drawer>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profileState: state.profile,
    itemOrderState: state.itemOrder,
  };
};

export default compose(
  withRouter,
  injectIntl,
  withStyles(styles, {
    withTheme: true,
  }),
  connect(mapStateToProps, { SaveItemOrder })
)(MeetingFilter);
