import React, { Component } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles, withTheme } from "@material-ui/core/styles";
import SearchInput, { createFilter } from "react-search-input";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import List from "@material-ui/core/List";
import CustomAvatar from "../../../components/Avatar/Avatar";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import Checkbox from "@material-ui/core/Checkbox";
import DoneIcon from "@material-ui/icons/Done";
import SelectMenu from "../../../components/Menu/SelectMenu";
import selectStyles from "../../../assets/jss/components/select";
import {FormattedMessage} from "react-intl";
let object = {};
class AssignedToDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      searchTerm: "",
      checked: [],
      checkedIds: [],
      allMembers: [],
      clear: false,
    };
    this.searchUpdated = this.searchUpdated.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
  }


  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.isClear) {
      object = {
        checked: [],
        checkedIds: [],
        clear: true
      };
    } else {
      object = {
        clear: false
      };
    }
    let allMembers = (nextProps.profileState && nextProps.profileState.data && nextProps.profileState.data.member) ? nextProps.profileState.data.member.allMembers : [];
    if (prevState.allMembers.length !== allMembers.length) {
      allMembers = allMembers.map(x => {
        return {
          id: x.userId,
          value: { name: x.userName, avatar: x.imageUrl, email: x.email, fullName: x.fullName, isOnline: x.isOnline, isOwner: x.isOwner }
        }
      });
      object.allMembers = allMembers;
      return (object);

    } else {
      object.allMembers = prevState.allMembers;
      return (object);
    }
  }
  searchUpdated(term) {
    this.setState({ searchTerm: term });
  }
  handleToggle = (value, id) => () => {
    const { checked, checkedIds } = this.state;
    const currentIndex = checked.indexOf(value);
    const idIndex = checkedIds.indexOf(id);
    const newChecked = [...checked];
    const newIds = [...checkedIds];

    if (currentIndex === -1) {
      newChecked.push(value);
      newIds.push(id);
    } else {
      newChecked.splice(currentIndex, 1);
      newIds.splice(idIndex, 1);
    }
    this.setState({
      checked: newChecked,
      checkedIds: newIds
    }, () => {

      this.props.handleFilterChange(this.props.iscreatedBy ? "createdBy" : "assignees", this.state.checkedIds);
    });
  };
  render() {
    const { theme, classes } = this.props;
    const { checked, allMembers, clear } = this.state;
    const data = allMembers;
    const filteredData = data.filter(
      createFilter(this.state.searchTerm, ["id", "value.name"])
    );
    if (clear) {
      this.props.handleClearState(true);
    }
    return (
      <SelectMenu
        label={this.props.iscreatedBy ? <FormattedMessage
          id="common.created-by.action"
          defaultMessage="Created By"
        /> : <FormattedMessage
        id="common.assigned.label"
        defaultMessage="Assigned To"
      />}
        multiple={true}
        value={checked}
      >
        <List classes={{ root: classes.searchMenuList }}>
          <ListItem disableRipple={true}>
            <SearchInput className="HtmlInput" onChange={this.searchUpdated} />
          </ListItem>
          {filteredData.map(data => {
            return (
              <ListItem
                key={data.id}
                value={data.value.name}
                onClick={this.handleToggle(data.value.name, data.id)}
                classes={{
                  root: classes.AvatarMenuItem
                }}
                button
                disableRipple={true}
              >
                <CustomAvatar
                  otherMember={{
                    imageUrl: data.value.avatar,
                    fullName: data.value.fullName,
                    lastName: '',
                    email: data.value.email,
                    isOnline: data.value.isOnline,
                    isOwner: data.value.isOwner
                  }}
                  size="small"
                />
                <ListItemText
                  primary={data.value.name}
                  classes={{ primary: classes.MenuItemText }}
                />
                <ListItemSecondaryAction>
                  <Checkbox
                    checkedIcon={
                      <DoneIcon
                        htmlColor={theme.palette.primary.light}

                      />
                    }
                    onChange={this.handleToggle(data.value.name, data.id)}
                    checked={this.state.checked.indexOf(data.value.name) !== -1}
                    icon={false}
                  />
                </ListItemSecondaryAction>
              </ListItem>
            );
          })}
        </List>
      </SelectMenu>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profileState: state.profile
  }
}


export default compose(
  withRouter,
  withStyles(selectStyles, {
    withTheme: true
  }),
  connect(
    mapStateToProps,
    {}
  )
)(AssignedToDropdown);