export function TaskEditorCmp(gantt, UpdateTaskName) {
  return {
    show: (id, column, config, placeholder) => {
      // called when input is displayed, put html markup of the editor into placeholder
      // and initialize your editor if needed:
      var txt = `${gantt.getTask(id).text} name=${column.name}`;
      // var html =
      //   "<div><input type='text' maxlength='80' autofocus class='ganttTaskEditInput' value='" + gantt.getTask(id).text + "' name='" + column.name + "'></div>";
      var html =
        "<div><input type='text' maxlength='80' autofocus class='ganttTaskEditInput' value='" +
        gantt.getTask(id).text.replace(/'/g, "&#39;") +
        "' name='" +
        column.name +
        "'></div>";
      placeholder.innerHTML = html;
      placeholder.querySelector("input").focus();
    },
    hide: () => {
      // called when input is hidden
      // destroy any complex editors or detach event listeners from here
    },

    set_value: (value, id, column, node) => {
      // ;
      // node.querySelector('input').value = value
      // set input value
    },

    get_value: (id, column, node) => {
      // return input value
    },

    is_changed: (value, id, column, node) => {
      let task = gantt.getTask(id); // Getting Task by id
      let input = node.querySelector("input");
      let emptyValueRegex = /([^\s])/;
      if (emptyValueRegex.test(input.value)) {
        task.text = node.querySelector("input").value;
        gantt.updateTask(id);
        UpdateTaskName(task);
      } else {
        return false;
      }
      // gantt.refreshData();

      // called before save/close. Return true if new value differs from the original one
      // returning true will trigger saving changes, returning false will skip saving
    },

    is_valid: (value, id, column, node) => {
      // validate, changes will be discarded if the method returns false
      return true;
    },

    save: (id, column, node) => {
      // only for inputs with map_to:auto. complex save behavior goes here
    },
    focus: (node) => {
      let input = node.querySelector("input");
      input.setSelectionRange(input.value.length, input.value.length);
    },
  };
}
