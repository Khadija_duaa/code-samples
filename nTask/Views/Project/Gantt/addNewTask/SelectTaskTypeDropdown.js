import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import InputLabel from "@material-ui/core/InputLabel";
import Avatar from "@material-ui/core/Avatar";
import DoneIcon from "@material-ui/icons/Done";
import Select from 'react-select';
import { components } from "react-select";
import autoCompleteStyles from "../../../../assets/jss/components/autoComplete";
import Grid from "@material-ui/core/Grid";
import DropdownArrow from "@material-ui/icons/ArrowDropDown"
import FormHelperText from "@material-ui/core/FormHelperText";


class SelectTaskType extends Component {
  constructor(props) {
    super(props);
    this.state = {
      AssignedToLabel: false,
      AddToProjectLabel: false,
      value: [],
      taskTypeOptions: [{value: "task", label: "Task"}, {value: "milestone", label: "Milestone"}],
      taskTitle: undefined,
    };
    this.handleSelectChange = this.handleSelectChange.bind(this);
  }

  handleSelectChange(newValue, actionMeta) {
    this.setState({ selectedProjectOption: newValue })
  }


  render() {
    const { classes, theme, taskTypeChangeAction, taskType, taskTypeError } = this.props;
    const {taskTypeOptions} = this.state;
    const DropdownIndicator = (props) => {
      return components.DropdownIndicator && (
        <components.DropdownIndicator {...props}>
          <DropdownArrow htmlColor={theme.palette.secondary.light} />
        </components.DropdownIndicator>
      );
    };
    // Styles of react-select autocomplete
    const customStyles = {
      control: (base, state) => ({
        ...base,
        background: "transparent",
        padding: "5.5px 10px 2px 10px",
        borderRadius: 4,
        borderColor: taskTypeError ? theme.palette.error.light : theme.palette.border.lightBorder,
        boxShadow: 'none',
        ":focus": {
          border: `1px solid ${theme.palette.border.darkBorder}`
        },
        ":hover": {
          border: `1px solid ${theme.palette.border.darkBorder}`
        }
      }),
      dropdownIndicator: (base, state) => ({
        padding: 0
      }),
      valueContainer: (base, state) => ({
        ...base,
        padding: "5px 0"
      })
    };
    const Option = props => {
     
      return (
        <components.Option {...props}>
          <Grid
            container
            direction="row"
            justify="flex-start"
            alignItems="center"
          >

            <Avatar classes={{ root: classes.dropDownAvatar }}>H</Avatar>

            <span>{props.children}</span>
          </Grid>
        </components.Option>
      );
    };

    return (
      <FormControl classes={{ root: classes.reactSelectFormControl }} fullWidth={true}>
        <InputLabel
          required
          htmlFor="SelectTaskType"
          classes={{
            root: classes.autoCompleteLabel,
            shrink: classes.shrinkLabel
          }}
          shrink={false}
        >
          Select Task Type
        </InputLabel>
        <Select
          styles={customStyles}
          onChange={taskTypeChangeAction}
          inputId="SelectTaskType"
          components={{ DropdownIndicator, IndicatorSeparator: false }}
          options={taskTypeOptions}
          value={taskType}
          placeholder=""
        />
        {taskTypeError ? (
              <FormHelperText
                classes={{ root: classes.selectError }}
              >
                Please select task type
              </FormHelperText>
            ) : null}
      </FormControl>
    );
  }
}
export default compose(
  withRouter,
  withStyles(autoCompleteStyles, {
    withTheme: true
  }),
  connect(
    null
  )
)(SelectTaskType);