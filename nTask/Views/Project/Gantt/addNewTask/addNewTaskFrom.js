import React, { Component, Fragment } from "react";
import { withSnackbar } from "notistack";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import uniqBy from "lodash/uniqBy";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import CreatableSelect from "react-select/lib/Creatable";
import { components } from "react-select";
import Avatar from "@material-ui/core/Avatar";
import autoCompleteStyles from "../../../../assets/jss/components/autoComplete";
import combineStyles from "../../../../utils/mergeStyles";
import DefaultButton from "../../../../components/Buttons/DefaultButton";
import CustomButton from "../../../../components/Buttons/CustomButton";
import dialogFormStyles from "../../../../assets/jss/components/dialogForm";
import DefaultTextField from "../../../../components/Form/TextField";
import { createNewMileStonesHelpText } from "../../../../components/Tooltip/helptext";
import { validateTitleField } from "../../../../utils/formValidations";
import CreateableSelectDropdown from "../../../../components/Dropdown/SelectCreateableDropdown/SelectCreateableDropdown";
import { generateSelectData } from "../../../../helper/generateSelectData";
import SelectSearchDropdown from "../../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import DeleteConfirmDialog from "../../../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";

import moment from "moment";
import {
  createBulkGanttTask,
  createGanttTask,
} from "../../../../redux/actions/gantt";
import StaticDatePicker from "../../../../components/DatePicker/StaticDatePicker";
import ButtonActionsCnt from "../../../../components/Dialog/ConfirmationDialogs/ButtonActionsCnt";
import FormHelperText from "@material-ui/core/FormHelperText";
import { DeleteArchiveTask } from "../../../../redux/actions/tasks";
import {FormattedMessage,injectIntl} from "react-intl";
import debounce from "lodash/debounce";
import NotificationMessage from "../../../../components/NotificationMessages/NotificationMessages";
import mixpanel from 'mixpanel-browser';
import { MixPanelEvents } from "../../../../mixpanel";

class AddNewTaskForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      AssignedToLabel: false,
      AddToProjectLabel: false,
      value: [],
      AssignEmailError: false,
      milestoneTitle:
        this.props.editMilestoneData.dialogType == "editMilestone"
          ? this.props.editMilestoneData.task.text
          : "",
      options: [],
      newTasks: [],
      invalidEmailMessage: false,
      selectedProjectOption: [],
      milestoneError: false,
      milestoneErrorMessage: "",
      taskType: { label: this.props.intl.formatMessage({id:"project.gant-view.task-dialog.form.type.list.task",defaultMessage:"Task"}) , value: "Task" },
      selectedMilestone: { label: this.props.intl.formatMessage({id:"project.gant-view.task-dialog.form.type.list.milestone",defaultMessage:"Milestone"}), value: "Milestone" },
      taskTypeError: false,
      selectedTasks: [],
      description:
        this.props.editMilestoneData.dialogType == "editMilestone"
          ? this.props.editMilestoneData.task.description
          : "",
      addTaskerror: false,
      actualDate:
        props.editMilestoneData.task &&
        props.editMilestoneData.task.actual_start,
      date:
        (props.editMilestoneData.task &&
          props.editMilestoneData.task.planned_start) ||
        new Date(),
    };

    this.handleChange = this.handleChange.bind(this);
  }
  componentDidMount(){
    mixpanel.time_event(MixPanelEvents.GantCreationEvent)
  }
  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  addTaskToGantt = (taskArr) => {
    const { plannedVsActual, projects, workspaceStatus } = this.props;
    if(taskArr.length){
      let taskTemplate;
      let attachedProject = projects.find((p) => p.projectId == taskArr[0].projectId);
          if (attachedProject && attachedProject.projectTemplate) {
            taskTemplate = attachedProject.projectTemplate;
          } else {
            taskTemplate = workspaceStatus;
          }
      let count = this.props.gantt.getTaskCount();
      taskArr.map((task,index) => {
        let startDate;
        let duration;
        let actualStartDate = task.start_date ? new Date(task.start_date) : null;
        let actualEndDate = task.start_date
          ? new Date(
              moment(task.start_date)
                .clone()
                .add(task.duration, "days")
            )
          : null;
        let plannedStartDate = task.plannedstartdate
          ? new Date(task.plannedstartdate)
          : null;
        let plannedEndDate = task.plannedstartdate
          ? new Date(
              moment(task.plannedstartdate)
                .clone()
                .add(task.plannedDuration, "days")
            )
          : null;
  
        if (
          plannedVsActual == "Planned" ||
          plannedVsActual == "Planned vs Actual"
        ) {
          startDate = task.plannedstartdate
            ? new Date(task.plannedstartdate)
            : null;
          duration = task.plannedDuration;
        } else {
          startDate = task.start_date ? new Date(task.start_date) : null;
          duration = task.duration;
        }
        let selectedStatus = taskTemplate.statusList.find(item => {
          return item.statusId == task.statusId;
        }) || false;
  
        this.props.gantt.addTask(
          task.type == "task"
            ? {
                id: task.taskId,
                sortorder: task.sortorder,
                text: task.text,
                start_date: startDate,
                // open: true,
                duration: duration,
                actual_start: actualStartDate,
                unscheduled:
                  plannedVsActual == "Planned" && task.plannedstartdate
                    ? false
                    : plannedVsActual == "Actual" && task.start_date
                    ? false
                    : plannedVsActual == "Planned vs Actual" &&
                      task.plannedstartdate
                    ? false
                    : true,
                actual_end: actualEndDate,
                progress: task.progress / 100,
                type: task.type,
                status: task.status,
                statusId: task.statusId,
                color: selectedStatus ? selectedStatus.statusColor : "",
                taskTemplate: taskTemplate,
                issues: task.issues,
                risks: task.risks,
                planned_start: plannedStartDate,
                planned_end: plannedEndDate,
                estiTime: task.estimatedEffort,
                actTime: task.taskEffort,
                assignee: [],
                priority: task.priority,
                attachments: task.taskAttachments,
                comments: task.comments,
                cost: task.cost,
                creationDate: new Date(task.createdDate),
                meetings: task.meetings,
              }
            : {
                id: task.taskId,
                sortorder: task.sortorder,
                text: task.text,
                start_date: startDate,
                planned_start: plannedStartDate,
                assignee: [],
                description: task.description,
                // open: true,
                duration: duration,
                type: task.type,
                taskTemplate: taskTemplate,
              },
          task.parent == "0" || !task.parent ? 0 : task.parent,
          count+index
        );
      });
    }
  };
   handleSubmit = debounce(() => {
    const { milestoneTitle, taskType, newTasks, selectedTasks } = this.state;
    const { createBulkGanttTask, projectId, intl } = this.props;
    //Validating Milestone title
    let validationObj = validateTitleField("milestone title", milestoneTitle, true,80, intl);
    if (validationObj["isError"]) {
      // Showing error incase there is error in milestone name entered
      this.setState({
        milestoneError: validationObj["isError"],
        milestoneErrorMessage: validationObj["message"],
      });
    }
    if (validationObj["isError"] == false && taskType.value == "Milestone") {
      this.setState({ btnQuery: "progress" }, () =>{
        //Action creator for creating Gantt Task as Milestone
        this.props.createGanttTask(
          {
            text: this.state.milestoneTitle.trim(),
            type: "milestone",
            projectId: this.props.projectId,
            parentId: this.props.addTaskActive,
            description: this.state.description,
            start_date: moment(this.state.date).format("L"),
          },
          //successddd
          (response) => {
            mixpanel.track(MixPanelEvents.GantCreationEvent, response)
            this.addTaskToGantt([response]);
            this.setState({ btnQuery: "" });
            this.props.closeAction();
          }
        );
      });
    }

    //The below condition will execute the code if user wants to add tasks with type Task (not-milestone)
    if (taskType.value == "Task") {
      //Making object for API request from new tasks list that user added with multi select
      const newTasksObj = newTasks.map((task) => {
        return {
          parentId: this.props.addTaskActive,
          taskId: "",
          taskTitle: task.value,
        };
      });
      //Making object for API request from existing tasks list that user added with multi select
      //Existing task list means user is added task to the project which are not linked with any project
      const selectedTasksObj = selectedTasks.map((task) => {
        return {
          parentId: this.props.addTaskActive,
          taskId: task.id,
          taskTitle: task.value,
        };
      });
      //Merging both new and existing tasks object and sending it to backend to add/link new tasks to project
      const bulkTaskObj = {
        projectId: projectId,
        taskTitles: [...newTasksObj, ...selectedTasksObj],
      };
      //Showing the loader on button on submit

      //Action of post AJAX call to add/link new task
      if (bulkTaskObj.taskTitles.length > 0) {
        this.setState({ btnQuery: "progress" }, () => {
          createBulkGanttTask(bulkTaskObj, (response) => {
            this.setState({ btnQuery: "" });
  
            //Extracting new task added for gantt task
            const newGanttTask = response.newTasks.map((task) => {
              return task.taskDto;
            });
  
            //Extracting linked tasks to project
            const updatedGanttTask = response.updatedTasks.map((task) => {
              return task.taskDto;
            });
            //Merging both new added and linked tasks to create one single array of tasks to be updated in gantt
            const newTaskArr = [...newGanttTask, ...updatedGanttTask];
  
            this.addTaskToGantt(newTaskArr);
            this.props.closeAction();
            this.props.updateTaskOrder();
          });
        });
      } else {
        this.setState({ addTaskerror: true });
      }
    }
  },800);
  handleChange(event, name) {
    this.setState({
      AssignEmailError: false,
      milestoneError: false,
      milestoneErrorMessage: "",
      ProjectError: false,
      ProjectErrorMessage: "",
    });
    this.setState({
      [name]: event.target.value, // Mapping value of textfield to local state
    });
  }
  onSelectFocus(event) {
    this.setState({ [event.target.id]: true });
  }
  onSelectBlur(event) {
    if (this.state.value.length < 1) {
      this.setState({ [event.target.id]: false });
    }
  }
  handleTaskTypeChange = (type, newValue) => {
    this.props.taskType(newValue.value);
    this.setState({ taskType: newValue });
  };

  generateTaskData = () => {
    const { tasks } = this.props;
    let noProjectTasks = tasks.filter((task) => {
      return !task.projectId;
    });
    let ddData = noProjectTasks.map((task, i) => {
      return generateSelectData(
        task.taskTitle,
        task.taskTitle,
        task.taskId,
        task.uniqueId,
        task
      );
    });
    return ddData;
  };
  //Local state is update for dropdown so object can be made to send update to backend with API
  //Updating local state when option is selected in task dropdown
  handleOptionsSelect = (type, option) => {
    let selTask = option.filter(item => item.__isNew__ ==  undefined);
    this.setState({ selectedTasks: selTask, addTaskerror: false });
  };
  //Updating local state when option is created in task dropdown
  handleCreateOption = (type, option) => {
    const { newTasks } = this.state;
    this.setState({ newTasks: [...newTasks, option], addTaskerror: false });
  };

  //Updating local state when option is removed from task dropdown
  handleRemoveOption = (type, option = {}) => {
    const { selectedTasks, newTasks } = this.state;
    if (type.__isNew__) {
      this.setState({
        newTasks: newTasks.filter((task) => {
          return task.label !== type.label;
        }),
      });
    } else {
      this.setState({
        selectedTasks: selectedTasks.filter((task) => {
          return task.id !== type.id;
        }),
      });
    }
  };
  //Saving milestone date to local so later on can be used to send to backend to create new milestone
  updateDate = (key, date) => {
    this.setState({ [key]: date });
  };
  handleMilestoneDescInput = (e) => {
    // update state of description of milestone
    this.setState({ description: e.target.value });
  };
  data = (editMilestone) => {
    if (editMilestone) {
      return [{ label: this.props.intl.formatMessage({id:"project.gant-view.task-dialog.form.type.list.milestone",defaultMessage:"Milestone"}), value: "Milestone" }];
    } else {
      return [
        { label: this.props.intl.formatMessage({id:"project.gant-view.task-dialog.form.type.list.task",defaultMessage:"Task"}), value: "Task" },
        { label: this.props.intl.formatMessage({id:"project.gant-view.task-dialog.form.type.list.milestone",defaultMessage:"Milestone"}), value: "Milestone" },
      ];
    }
  };
  //Called incase of mile stone is updated from the dialog
  updateMilestone = () => {
    const { editMilestoneData } = this.props;
    const { date, actualDate, description, milestoneTitle } = this.state;
    this.setState({ btnQuery: "progress" });
    const postObj = [
      {
        taskId: editMilestoneData.task.id,
        startDate: moment(date).format("MM-DD-YYYY"),
        actualStartDate: actualDate && moment(actualDate).format("MM-DD-YYYY"),
        description: description,
        type: "milestone",
        text: milestoneTitle.trim(),
      },
    ];
    this.props.updateTask(postObj, () => {
      this.setState({ btnQuery: "" });
      this.props.closeAction();
    });
  };
  deleteDialogMilseStone = () => {
    this.setState({ isDeleteMileStone: true });
  };
  handleDialogClose = () => {
    this.setState({ isDeleteMileStone: false });
  };
  deleteMilestone = () => {
    let taskId = this.props.editMilestoneData.task.id;
    this.setState({ btnQuery: "progress" });
    this.props.DeleteArchiveTask(
      taskId,
      //Success
      () => {
        this.props.gantt.deleteTask(taskId);
        this.setState({ btnQuery: "" });
        this.props.closeAction();
      }
    ),
      //Failure
      () => {};
  };
  render() {
    const {
      classes,
      theme,
      closeAction,
      selectedTask,
      editMilestoneData,
      intl
    } = this.props;
    const {
      btnQuery,
      milestoneError,
      milestoneErrorMessage,
      taskType,
      description,
      addTaskerror,
      selectedMilestone,
      milestoneTitle,
      actualDate,
      date,
      isDeleteMileStone,
    } = this.state;
    let editMilestone =
      editMilestoneData.dialogType == "editMilestone" ? true : false;
    return (
      <Fragment>
        <div className={classes.dialogFormCnt} noValidate autoComplete="off">
          <SelectSearchDropdown
            data={() => this.data(editMilestone)}
            label={<FormattedMessage id="project.gant-view.task-dialog.form.type.label" defaultMessage="Select Type"/>}
            id="selectTaskTypeGantt"
            isDisabled={editMilestone}
            selectChange={this.handleTaskTypeChange}
            type="type"
            selectedValue={
              editMilestoneData.dialogType == "editMilestone"
                ? selectedMilestone
                : taskType
            }
            isMulti={false}
          />
          {taskType.value == "Milestone" || editMilestone ? (
            <>
              <DefaultTextField
                label={<FormattedMessage id="project.gant-view.milestone-dialog.form.milestone-title.label" defaultMessage="Milestone Title"/>}
                errorState={milestoneError}
                errorMessage={milestoneErrorMessage}
                helptext={<FormattedMessage id="project.gant-view.milestone-dialog.form.milestone-title.hint" defaultMessage={createNewMileStonesHelpText.milestonesHelpText} />}
                defaultProps={{
                  type: "text",
                  id: "Milestone",
                  placeholder: intl.formatMessage({id:"project.gant-view.milestone-dialog.form.milestone-title.placeholder" , defaultMessage:"Add Milestone Title"}),
                  value: milestoneTitle,
                  autoFocus: true,
                  inputProps: { maxLength: 80 },
                  onChange: (event) => {
                    this.handleChange(event, "milestoneTitle");
                  },
                }}
              />
              <div style={{ display: "flex" }}>
                <StaticDatePicker
                  label={<FormattedMessage id="project.gant-view.milestone-dialog.form.plan-date.label" defaultMessage="Planned Date"/>}
                  placeholder={<FormattedMessage id="project.gant-view.milestone-dialog.form.plan-date.placeholder" defaultMessage="Select Date"/>}
                  isInput={true}
                  onDateSelect={(date) => this.updateDate("date", date)}
                  isCreation={true}
                  style={{ flex: 0.5, marginRight: 20 }}
                  date={moment(date)}
                />
                {/* <StaticDatePicker
                  label={<FormattedMessage id="project.gant-view.milestone-dialog.form.actual-date.label" defaultMessage="Actual Date"/>}
                  placeholder={<FormattedMessage id="project.gant-view.milestone-dialog.form.actual-date.label" defaultMessage="Select Date"/>}
                  isInput={true}
                  onDateSelect={(date) => this.updateDate("actualDate", date)}
                  isCreation={true}
                  style={{ flex: 0.5, marginRight: 20 }}
                  date={actualDate ? moment(actualDate) : null}
                /> */}
              </div>
              <DefaultTextField
                label={<FormattedMessage id="common.description.label" defaultMessage="Description"/>}
                fullWidth={true}
                defaultProps={{
                  id: "descriptionMilestone",
                  onChange: this.handleMilestoneDescInput,
                  value: description,
                  multiline: true,
                  rows: 7,
                  placeholder: intl.formatMessage({id:"common.description.placeholder" , defaultMessage: "Enter description here"}),
                  autoFocus: true,
                  inputProps: { maxLength: 1500 },
                }}
              />
            </>
          ) : (
            <>
              <CreateableSelectDropdown
                data={this.generateTaskData}
                label={<FormattedMessage id="project.gant-view.task-dialog.form.task-title.label" defaultMessage="Task Title"/>}
                placeholder={<FormattedMessage id="project.gant-view.task-dialog.form.task-title.label" defaultMessage="Select / Add Task"/>}
                id="selectTaskTitleDropdown"
                name="task"
                selectOptionAction={this.handleOptionsSelect}
                createOptionAction={this.handleCreateOption}
                removeOptionAction={this.handleRemoveOption}
                type="task"
                createText="Add Task"
                selectedValue={selectedTask}
                createLabelValidation={() => {
                  return true;
                }}
              />
              {addTaskerror ? (
                <FormHelperText classes={{ root: classes.errorText }}>
                  {" "}
                  {<FormattedMessage id="project.gant-view.task-dialog.form.validation.task-title.empty" defaultMessage="Oops! Please select / add task."/>}{" "}
                </FormHelperText>
              ) : null}
            </>
          )}
        {taskType.value !== "Milestone" && <NotificationMessage
          type="NewInfo"
          iconType="info"
          style={{
            width: "100%",
            textAlign: "start",
            fontSize: "11px",
            padding: "6px",
            backgroundColor: "#EAEAEA",
            borderRadius: 4,
            fontFamily: theme.typography.fontFamilyLato,
            fontWeight: 400,
          }}>
          Tasks having user effort will not be added in this project.
          </NotificationMessage>}
        </div>
        {editMilestone ? (
          <ButtonActionsCnt
            btnQuery={btnQuery}
            cancelBtnText={<FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel"/>}
            btnType="success"
            successBtnText={<FormattedMessage id="project.gant-view.milestone-dialog.form.save-button.label" defaultMessage="Save Changes"/>}
            deleteBtnText={<FormattedMessage id="common.action.delete.label" defaultMessage="Delete"/>}
            deleteBtn={true}
            deleteAction={this.deleteDialogMilseStone}
            cancelAction={closeAction}
            successAction={this.updateMilestone}
          />
        ) : (
          <ButtonActionsCnt
            cancelAction={closeAction}
            successAction={this.handleSubmit}
            successBtnText={
              taskType.value == "Milestone" ? <FormattedMessage id="project.gant-view.task-dialog.form.milestone-button.label" defaultMessage="Create Milestone"/> : <FormattedMessage id="project.gant-view.task-dialog.form.create-button.label" defaultMessage="Create Task"/>
            }
            cancelBtnText={<FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel"/>}
            btnType="success"
            btnQuery={btnQuery}
            successBtnProps={{ id: "addGanttTaskBtn" }}
          />
        )}
        {isDeleteMileStone ? (
          <DeleteConfirmDialog
            open={isDeleteMileStone}
            closeAction={this.handleDialogClose}
            cancelBtnText={<FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel"/>}
            successBtnText={<FormattedMessage id="common.action.delete.label" defaultMessage="Delete"/>}
            alignment="center"
            headingText={<FormattedMessage id="project.gant-view.milestone-dialog.delete-confirmation.title" defaultMessage="Delete"/>}
            successAction={this.deleteMilestone}
            msgText={<FormattedMessage id="project.gant-view.milestone-dialog.delete-confirmation.label" defaultMessage="Are you sure you want to delete this milestone?"/>}
            btnQuery={btnQuery}
          />
        ) : null}
      </Fragment>
    );
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    tasks: state.tasks.data,
    ganttTasks: state.ganttTasks.data,
    profileState: state.profile,
    projects: state.projects.data,
    workspaceStatus: state.workspaceTemplates.data.defaultWSTemplate,
  };
};
export default compose(
  withRouter,
  withSnackbar,
  injectIntl,
  withStyles(combineStyles(autoCompleteStyles, dialogFormStyles), {
    withTheme: true,
  }),
  connect(mapStateToProps, {
    createBulkGanttTask,
    DeleteArchiveTask,
    createGanttTask,
  })
)(AddNewTaskForm);
