import React from "react";
import { compose } from "redux";
import SvgIcon from "@material-ui/core/SvgIcon";
import CircularProgress from "@material-ui/core/CircularProgress";
import ExcelIcon from "../../../components/Icons/ExcelIcon";
import CustomDialog from "../../../components/Dialog/CustomDialog";
import dialogStyles from "../../../assets/jss/components/dialog";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import DefaultButton from "../../../components/Buttons/DefaultButton";
import Dropzone from "react-dropzone";
import Typography from "@material-ui/core/Typography";
import { Circle } from "rc-progress";
import helper from "../../../helper/index";
import { ImportFile } from "../../../redux/actions/ImportExport";
import apiInstance, { CancelToken } from "../../../redux/instance";
import NotificationMessage from "../../../components/NotificationMessages/NotificationMessages";
import getErrorMessages from "../../../utils/constants/errorMessages";
import { injectIntl, FormattedMessage } from "react-intl";
import { connect, useDispatch } from "react-redux";
import fileDownload from "js-file-download";
import { useState } from "react";
import { getProjects, ImportMsFile, ImportMsProject } from '../../../redux/actions/projects';
import MSImportIcon from "../../../components/Icons/MsImportIcon";

const fileType = [
  ".mpp",
  "",
];

export const MsUploadDialog = (props) => {
  const { classes, theme, open, companyInfo, type, closeAction, handleRefresh } = props;

  const [progress, setProgress] = useState(0);
  const [totalAdded, setTotalAdded] = useState(0);
  const [dnd, setDnd] = useState(true);
  const [errorMesseage, setErrorMesseage] = useState("");
  const dispatch = useDispatch()

  const onhandleDrop = (files) => {
    if (files.length) {
      manageImports(files[0]);
      setDnd(false);
    }
  };

  const manageImports = (file) => {
    const formdata = new FormData()
    formdata.append("file", file);
    // formdata.append('type', 'msproject-parse');
    // formdata.append('data', JSON.stringify({ "server": "https://rpi.ntaskmanager.com/gantt", "taskProperties": ["ID", 'Milestone'] }))
    ImportMsProject(formdata).then(() => {
      setDnd(true)
      handleCloseDialogue()
      handleRefresh()
    }).catch(() => console.error("Something happend!"))
    // ImportMsFile(
    //   formdata,
    //   (res) => {
    //     console.log(res);
    //     const { data } = res.data;
    //     ImportMsProject(
    //       { data, fileName: file.name },
    //       (res) => {
    //         setDnd(true);
    //         handleCloseDialogue();
    //         handleRefresh();
    //       },
    //       (err) => {
    //         setDnd(true);
    //         handleCloseDialogue();
    //         setErrorMesseage('somethis went wrong')
    //       });
    //   },
    //   (err) => {
    //     console.log(err);
    //     // setErrorMesseage('somethis went wrong')
    //   });
  };

  const handleDragOver = (event) => {
    event.stopPropagation();
  };

  const handleCloseDialogue = () => {
    if (dnd || progress === 100) {
      closeAction?.();
    }
  };

  const handleInvalidFileDrop = () => {
    props.showMessage(getErrorMessages().INVALID_MS_IMPORT_EXTENSION, "error");
  };

  const dndStyles = {
    position: "relative",
    width: "100%",
    borderWidth: 2,
    borderColor: theme.palette.border.lightBorder,
    borderStyle: "dashed",
    borderRadius: 5,
    padding: 50,
    background: theme.palette.background.paper,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginBottom: 30,
  };

  const dndActiveStyle = {
    borderColor: theme.palette.border.darkBorder,
  };

  return (
    <CustomDialog
      title="Import From MS Project"
      dialogProps={{
        open: open,
        onClose: handleCloseDialogue,
        onClick: (e) => e.stopPropagation(),
        disableBackdropClick: true,
        disableEscapeKeyDown: true,
      }}>
      <div className={classes.centerAlignDialogContent}>
        {errorMesseage ? (
          <NotificationMessage
            type="failure"
            iconType="failure"
            style={{ marginBottom: 20, width: "100%" }}>
            {errorMesseage}
          </NotificationMessage>
        ) : null}
        {dnd ? (
          <Dropzone
            onDrop={onhandleDrop}
            onDragOver={handleDragOver}
            onDropRejected={handleInvalidFileDrop}
            // accept=""
            accept={fileType}
            activeStyle={dndActiveStyle}
            style={dndStyles}
            // disableClick
            onClick={(evt) => evt.preventDefault()}>
            {({ getRootProps, getInputProps, open }) => {
              return (
                <>
                  <div className={classes.csvConfirmationIconCnt} {...getRootProps()}>
                    <SvgIcon
                      viewBox="0 0 50 43.571"
                      className={classes.archieveConfirmationIcon}
                      htmlColor={theme.palette.secondary.light}>
                      <MSImportIcon />
                    </SvgIcon>
                  </div>
                  <Typography variant="h3">
                    <FormattedMessage
                      id="common.import-dialog.label"
                      defaultMessage="Drag & Drop"
                    />
                  </Typography>
                  <input {...getInputProps()} />
                  <p className={classes.dragnDropMessageText}>
                    your MS Project file or{" "}
                    <u className={classes.browseText} onClick={() => open()}>
                      <FormattedMessage id="common.import-dialog.browse" defaultMessage="browse" />
                    </u>
                    <FormattedMessage
                      id="common.import-dialog.fromcomputer"
                      defaultMessage=" from your computer"
                    />
                  </p>
                </>
              );
            }}
          </Dropzone>
        ) : (
          <div className={classes.importBulkProgressCnt}>
            <div
              style={{
                width: 100,
                position: "relative",
                marginBottom: 30,
                display: "flex",
                flexDirection: "row",
                justifyContent: "center",
              }}>
              {progress < 100 ? (
                <CircularProgress className={classes.progress} />
              ) : (
                <>
                  <Circle
                    percent={progress}
                    strokeWidth="3"
                    trailWidth=""
                    trailColor="#dedede"
                    strokeColor="#0090ff"
                  />
                  <span className={classes.clProgressValue}>{progress}%</span>
                </>
              )}
            </div>

            <Typography variant="h3">
              {progress < 100 ? (
                <FormattedMessage id="common.importing.label" defaultMessage="Importing..." />
              ) : (
                <FormattedMessage
                  id="common.congratulations.label"
                  defaultMessage="Congratulations!"
                />
              )}
            </Typography>
            <p className={classes.dragnDropMessageText}>
              {progress < 100 ? (
                <FormattedMessage
                  id="common.import-dialog.wait.label"
                  defaultMessage="Please wait! It will take a moment."
                />
              ) : (
                `${totalAdded}/${totalAdded} ${type}${props.intl.formatMessage({
                  id: "common.import-dialog.added.label",
                  defaultMessage: "s have been imported to",
                })} ${companyInfo ? companyInfo.companyName : "nTask"} ${props.intl.formatMessage({
                  id: "common.import-dialog.added.success",
                  defaultMessage: "successfully.",
                })}.`
              )}
            </p>
          </div>
        )}
      </div>
      <Grid
        container
        direction="row"
        justify="flex-end"
        alignItems="center"
        classes={{
          container: classes.dialogActionsCnt,
        }}>
        {!dnd && progress < 100 ? null : ( // /> //   onClick={handleCancelRequest} //   buttonType="Plain" //   text="Cancel" // <DefaultButton
          <DefaultButton
            text={<FormattedMessage id="common.done.label" defaultMessage="Done" />}
            buttonType="Plain"
            onClick={handleCloseDialogue}
          />
        )}
      </Grid>
    </CustomDialog>
  );
};

function mapStateToProps(state) {
  return {
    companyInfo: state.whiteLabelInfo.data,
  };
}
export default compose(
  injectIntl,
  connect(mapStateToProps, {}),
  withStyles(dialogStyles, { withTheme: true })
)(MsUploadDialog);
