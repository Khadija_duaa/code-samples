import React, { Component } from "react";
import GanttComponent from "./Gantt";
import Toolbar from "./Toolbar";
import "./ganttStyles.css";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import moment from "moment";
import {
  saveTasksLink,
  taskComplete,
  removeGanttTask,
  deleteTasksLink,
  updateGanttTask,
  taskInComplete,
  updateSortOrder,
  getGanttTasks,
  updateMultipleGanttTask,
  updateGanttTaskPatch,
  updateGanttTaskOrder,
  dispatchGantt,
} from "../../../redux/actions/gantt";

import { debug } from "util";
import { withStyles, withTheme } from "@material-ui/core/styles";

class ProjectGantt extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentZoom: 0,
      messages: [],
      currentTask: null,

      showTaskDetails: false,
      deleteBtnQuery: "",
      columns: props.ganttTasksState.ganttTaskColumn
        ? props.ganttTasksState.ganttTaskColumn.ganttTaskColumnOrder
        : [],
    };

    this.handleZoomChange = this.handleZoomChange.bind(this);
    this.logLinkUpdate = this.logLinkUpdate.bind(this);
    this.handleTaskUpdate = this.handleTaskUpdate.bind(this);
    this.handleTaskComplete = this.handleTaskComplete.bind(this);
    this.handleTaskInComplete = this.handleTaskInComplete.bind(this);
  }

  logLinkUpdate(id, link) {
    const linkObj = {
      id: id,
      projectId: this.props.projectId,
      type: link.type,
      source: link.source,
      target: link.target,
    };
    this.props.saveTasksLink(linkObj);
  }
  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }
  componentDidUpdate(prevProps, prevState) { }

  handleTaskUpdate(obj) {
    this.props.updateGanttTask(obj);
  }
  handleBatchTaskUpdate = (obj, callback = () => { }) => {
    this.props.updateMultipleGanttTask(obj, succ => {
      console.log(succ);
    }, (err) => {
      console.log(err);
    });
  };
  handleGanttTaskPatch = (obj, callback) => {
    this.props.updateGanttTaskPatch(obj, succ => {
      // this.props.getGanttTasks(this.props.projectId, (res) => {
      // Gantt && Gantt.refreshData();
      // });
    });
  };
  handleTaskUpdateSortOrder = (obj, ganttTasks) => {
    this.props.updateSortOrder(obj, ganttTasks);
  };
  handleTaskComplete(taskId) {
    this.props.taskComplete(taskId);
  }
  handleTaskInComplete(taskId) {
    this.props.taskInComplete(taskId);
  }
  updateGanttTaskOrder = (payload) => {
    this.props.updateGanttTaskOrder(payload, succ => {
      this.props.getGanttTasks(this.props.projectId, () => { });
    });
  };
  handleZoomChange(zoom) {
    this.setState({
      currentZoom: zoom,
    });
  }
  getTaskPer = (t) => {
    const { projects, workspaceLevelTaskPer, workspaceStatus, workspaceLevelMeetingPer, workspaceLevelRiskPer, workspaceLevelIssuePer } = this.props;
    if (t) {
      if (t.projectId) {
        let attachedProject = projects.find((p) => p.projectId == t.projectId);
        if (attachedProject) {
          t.taskPermission = attachedProject.projectPermission.permission.task;
          t.meetingPermission = attachedProject.projectPermission.permission.meeting;
          t.riskPermission = attachedProject.projectPermission.permission.risk;
          t.issuePermission = attachedProject.projectPermission.permission.issue;
        }
        else {
          t.taskPermission = workspaceLevelTaskPer;
          t.meetingPermission = workspaceLevelMeetingPer;
          t.riskPermission = workspaceLevelRiskPer;
          t.issuePermission = workspaceLevelIssuePer;
        };
        if (attachedProject && attachedProject.projectTemplate) {
          t.taskStatus = attachedProject.projectTemplate;
        } else {
          t.taskStatus = workspaceStatus;
        }
        return t;
      } else {
        t.taskPermission = workspaceLevelTaskPer;
        t.taskStatus = workspaceStatus;
        t.meetingPermission = workspaceLevelMeetingPer;
        t.riskPermission = workspaceLevelRiskPer;
        t.issuePermission = workspaceLevelIssuePer;
        return t;
      }
    } else return {};
  };
  taskDetailsOpen = (taskData) => {
    let task = this.getTaskPer(taskData);
    this.setState({ showTaskDetails: true, currentTask: task }, () => {
      this.setState({
        showTaskDetails: false, currentTask: null
      })
    });
  };
  updateCurrentTask = (taskData) => {
    this.setState({ currentTask: taskData });
  };
  onDeleteTask = (taskId, Gantt, childIds, isChildDelete) => {
    //This must come here before delete of parent task so later on we add back the child task which gantt by default deletes
    const tasks = Gantt.getTaskByTime().filter(
      (t) => childIds.indexOf(t.id) > -1
    );
    this.props.removeGanttTask(taskId); // Removing Gantt Task
    Gantt.deleteTask(taskId);
    if (!isChildDelete) {
      //this will run in case when parent task is deleted with the option of not to delete the children tasks
      for (let i = 0; i < tasks.length; i++) {
        let task = tasks[i];
        task.parent = "0";
        Gantt.addTask(task);
      }
      Gantt.refreshData();
    }
  };
  closeTaskDetailsPopUp = (Gantt, taskId) => {
    this.setState({ showTaskDetails: false }, () => {
      this.props.getGanttTasks(this.props.projectId, (res) => {
        Gantt && Gantt.refreshData();
      });

    });
  };

  handleGanttColumn = (columns) => {
    this.setState({ columns });
  };
  render() {
    const {
      ganttTasksState,
      projectId,
      currency,
      selectedView,
      plannedVsActual,
      ganttExpand,
      handleGanttView,
      handleTaskbarsType,
      handleFullScreen,
      projects,
      workspaceStatus
    } = this.props;

    const { showTaskDetails, currentTask, columns } = this.state;
    let tasks = ganttTasksState.data || [];
    tasks = tasks.filter((x) => x.projectId === projectId);
    const taskData = () => {
      return tasks.map((x, i) => {
        let startDate;
        let duration;
        let attachedProject = projects.find((p) => p.projectId == x.projectId);
        if (attachedProject && attachedProject.projectTemplate) {
          x.taskTemplate = attachedProject.projectTemplate;
        } else {
          x.taskTemplate = workspaceStatus;
        }
        let selectedStatus = x.taskTemplate.statusList.find(item => {
          return item.statusId == x.statusId;
        }) || false;
        let actualStartDate = x.start_date ? new Date(x.start_date) : null;
        let actualEndDate = x.end_date ? new Date(x.end_date) : null;
        let plannedStartDate = x.plannedstartdate ? new Date(x.plannedstartdate) : null;

        let plannedEndDate = x.plannedstartdate && x.plannedenddate == null && x.plannedDuration
          ? new Date(
            moment(x.plannedstartdate)
              .clone()
              .add(x.plannedDuration, "days")
          )
          : x.plannedenddate ? x.plannedenddate : null;
        if (
          plannedVsActual == "Planned" ||
          plannedVsActual == "Planned vs Actual"
        ) {
          startDate = x.plannedstartdate ? new Date(x.plannedstartdate) : null;
          duration = x.plannedDuration;
        } else {
          startDate = x.start_date ? new Date(x.start_date) : null;
          duration = x.duration;
        }
        return x.type == "milestone"
          ? {
            id: x.id,
            text: x.text,
            start_date: startDate,
            type: x.type,
            duration: 1,
            planned_start: plannedStartDate,
            actual_start: actualStartDate,
            parent:
              x.parent == x.taskId
                ? 0
                : x.parent == "0" || x.parent == ""
                  ? 0
                  : x.parent,
            assignee: [],
            description: x.description,
            status: x.status,
            statusId: x.statusId,
            color: selectedStatus ? selectedStatus.statusColor : "",
            taskTemplate: x.taskTemplate,
          }
          : {
            id: x.taskId,
            uniqueId: x.uniqueId || "Not found",
            sortorder: x.sortorder,
            text: x.text,
            start_date: startDate,
            // open: true,
            actual_duration: x.duration,
            duration: duration,
            actual_start: actualStartDate,
            unscheduled:
              plannedVsActual == "Planned" && x.plannedstartdate
                ? false
                : plannedVsActual == "Actual" && x.start_date
                  ? false
                  : plannedVsActual == "Planned vs Actual" && x.plannedstartdate
                    ? false
                    : true,
            actual_end: actualEndDate,
            progress: x.progress / 100,
            // progress: x.progress,
            type: x.type,
            colorCode: x.colorCode,
            status: x.status,
            statusId: x.statusId,
            color: selectedStatus ? selectedStatus.statusColor : "",
            taskTemplate: x.taskTemplate,
            issues: x.issues,
            risks: x.risks,
            planned_start: plannedStartDate,
            planned_end: plannedEndDate,
            estiTime: x.estimatedTime,
            actTime: x.actualTime,
            assignee: x.assignees
              ? this.props.profileState.data.member.allMembers.filter(
                (member) => {
                  return x.assignees.indexOf(member.userId) > -1;
                }
              )
              : [],
            priority: x.priority,
            attachments: x.taskAttachments,
            comments: x.comments,
            cost: x.cost,
            creationDate: new Date(x.createdDate),
            meetings: x.meetings,
            parent:
              x.parent == x.taskId
                ? 0
                : x.parent == "0" || x.parent == ""
                  ? 0
                  : x.parent,
          };
      });
    };

    const data = {
      data: taskData() || [],
      links: ganttTasksState.links || [],
    };
    let tasksData = this.props.tasksState.data
      ? this.props.tasksState.data.filter(
        (x) => x.projectId === (this.props.projectId || "")
      )
      : [],
      riskCount = 0,
      issueCount = 0,
      percentage = 0;

    tasksData.map((x) => {
      riskCount += this.props.risksState.data
        ? this.props.risksState.data
          .filter((y) =>
            this.props.projects.indexOf("Archived") >= 0
              ? y.isArchive
              : !y.isArchive
          )
          .filter((risk) => risk.linkedTasks.indexOf(x.taskId) >= 0).length
        : 0;
    });
    tasksData.map((x) => {
      issueCount += this.props.issuesState.data
        ? this.props.issuesState.data
          .filter((y) =>
            this.props.projects.indexOf("Archived") >= 0
              ? y.isArchive
              : !y.isArchive
          )
          .filter((issue) => issue.linkedTasks.indexOf(x.taskId) >= 0).length
        : 0;
    });
    (tasksData = this.props.tasksState.data
      ? this.props.tasksState.data

        .filter((x) => x.projectId === (this.props.projectId || ""))
        .map((x) => x.progress)
      : []),
      (percentage = 0);
    if (tasksData.length)
      percentage = tasksData.reduce((sum, x) => sum + x) / tasksData.length;
    percentage = Math.round(percentage);
    return (
      <div>
        <Toolbar
          zoom={this.state.currentZoom}
          onZoomChange={this.handleZoomChange}
          handleFullScreen={handleFullScreen}
          tasks={
            this.props.ganttTasksState ? this.props.ganttTasksState : []
          }
          selectedColumns={columns}
          handleGanttView={handleGanttView}
          plannedVsActual={plannedVsActual}
          selectedView={selectedView}
          ganttData={ganttTasksState}
          profileData={this.props.profileState.data}
          projectId={this.props.projectId}
          handleTaskbarsType={handleTaskbarsType}
          issueCount={issueCount}
          riskCount={riskCount}
          percentage={percentage}
          currency={currency}
        />
        <div
          className="gantt-container"
          style={{
            height: ganttExpand
              ? window.innerHeight - 145
              : window.innerHeight - 285,
            width: "100%",
          }}
        >
          <GanttComponent
            ganttExpand={this.props.ganttExpand}
            tasks={data}
            addTaskAction={this.handleAddTask}
            taskDetailsOpen={this.taskDetailsOpen}
            closeTaskDetailsPopUp={this.closeTaskDetailsPopUp}
            showTaskDetails={showTaskDetails}
            selectedColumns={columns}
            selectedView={selectedView}
            dispatchGantt={this.props.dispatchGantt}
            projectId={projectId}
            plannedVsActual={plannedVsActual}
            currentTask={currentTask}
            zoom={this.state.currentZoom}
            handleGanttColumn={this.handleGanttColumn}
            location={this.props.location}
            onTaskUpdated={this.handleTaskUpdate}
            updateCurrentTask={this.updateCurrentTask}
            onDeleteTask={this.onDeleteTask}
            onLinkUpdated={this.logLinkUpdate}
            ganttTaskData={
              this.props.ganttTasksState ? this.props.ganttTasksState : []
            }
            updateGanttTaskOrder={this.updateGanttTaskOrder}
            handleTaskUpdateSortOrder={this.handleTaskUpdateSortOrder}
            handleBatchTaskUpdate={this.handleBatchTaskUpdate}
            handleGanttTaskPatch={this.handleGanttTaskPatch}
            deleteTasksLink={this.props.deleteTasksLink}
            taskData={this.props.tasksState ? this.props.tasksState : []}
            issueData={this.props.issuesState ? this.props.issuesState : []}
            riskData={this.props.risksState ? this.props.risksState : []}
            members={this.props.profileState.data.member.allMembers || []}
            handleTaskComplete={this.handleTaskComplete}
            handleTaskInComplete={this.handleTaskInComplete}
            getTaskPer={this.getTaskPer}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    ganttTasksState: state.ganttTasks,
    profileState: state.profile,
    tasksState: state.tasks,
    risksState: state.risks,
    issuesState: state.issues,
    ganttComponent: state.ganttComponent,
    workspaceLevelTaskPer: state.workspacePermissions.data.task,
    projects: state.projects.data,
    workspaceStatus: state.workspaceTemplates.data.defaultWSTemplate,
    workspaceLevelMeetingPer: state.workspacePermissions.data.meeting,
    workspaceLevelRiskPer: state.workspacePermissions.data.risk,
    workspaceLevelIssuePer: state.workspacePermissions.data.issue,
  };
};

export default compose(
  withRouter,
  connect(mapStateToProps, {
    removeGanttTask,
    saveTasksLink,
    taskComplete,
    deleteTasksLink,
    updateSortOrder,
    updateGanttTask,
    taskInComplete,
    getGanttTasks,
    updateMultipleGanttTask,
    updateGanttTaskPatch,
    updateGanttTaskOrder,
    dispatchGantt,
  })
)(ProjectGantt);
