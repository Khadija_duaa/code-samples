import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import menuStyles from "../../../assets/jss/components/menu";
import CustomButton from "../../../components/Buttons/CustomButton"
import gantStyles from "./style"
import SelectionMenu from "../../../components/Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import combineStyles from "../../../utils/mergeStyles";
import DownArrow from "@material-ui/icons/ArrowDropDown";
import Typography from "@material-ui/core/Typography";

class PlannedVsActualDropDown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      pickerOpen: false,
      pickerPlacement: "",
      archiveFlag: false,
      unArchiveFlag: false,
      deleteFlag: false,
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handlePickerClose = this.handlePickerClose.bind(this);
  }
  handleDialogOpen = () => {
    this.setState({ renameRoleDialogOpen: true });
  };
  handleDialogClose = () => {
    this.setState({ renameRoleDialogOpen: false });
  };
  handleClose(event) {
    this.setState({ open: false, pickerOpen: false });
  }
  handlePickerClose(event) {
    this.setState({ pickerOpen: false });
  }
  componentWillUnmount() {
    this.props.handleTaskbarsType(null, "Planned")
  }
  handleClick(event, placement) {
    const { currentTarget } = event;
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement
    }));
  }
  render() {
    const { classes, theme, taskBarsType, handleTaskbarsType } = this.props;
    const { open, placement } = this.state;
    return (
      <ClickAwayListener onClickAway={this.handleClose}>
        <div>
          <CustomButton
            onClick={event => {
              this.handleClick(event, "bottom-end");
            }}
            buttonRef={node => {
              this.anchorEl = node;
            }}
            style={{
              minWidth: 190,
              padding: "7px 8px 7px 14px",
              display: "flex",
              justifyContent: "space-between",
              marginRight: 20,
            }}
            btnType="white"
            variant="contained"
          >  <span>View: {taskBarsType}</span>
            <DownArrow
              htmlColor={theme.palette.secondary.medDark}
              className={classes.dropdownArrow}
            />
          </CustomButton>

          <SelectionMenu
            open={open}
            closeAction={this.handleClose}
            placement={placement}
            style={{ width: 215 }}
            anchorRef={this.anchorEl}
            list={
              <List disablePadding={true}>
                <ListItem disableRipple

                  className={classes.headingItem}>
                  <ListItemText
                    primary="Select View"
                    classes={{ primary: classes.headingText }}
                  />
                </ListItem>

                <ListItem
                  button
                  disableRipple
                  selected={taskBarsType == "Planned"}
                  classes={{ selected: classes.statusMenuItemSelected }}
                  onClick={event => handleTaskbarsType(event, "Planned")}
                >
                  <ListItemText
                    primary="Planned"
                    classes={{
                      primary: classes.statusItemText
                    }}
                  />
                </ListItem>

                <ListItem
                  button
                  disableRipple
                  classes={{ selected: classes.statusMenuItemSelected }}
                  selected={taskBarsType == "Actual"}
                  onClick={event => handleTaskbarsType(event, "Actual")}
                // onClick={deleteMember}
                >
                  <ListItemText
                    primary="Actual"
                    classes={{
                      primary: classes.statusItemText
                    }}
                  />
                </ListItem>
                <ListItem
                  button
                  disableRipple
                  classes={{ selected: classes.statusMenuItemSelected }}
                  selected={taskBarsType == "Planned vs Actual"}
                  onClick={event => handleTaskbarsType(event, "Planned vs Actual")}
                // onClick={deleteMember}
                >
                  <ListItemText
                    primary="Planned vs Actual"
                    classes={{
                      primary: classes.statusItemText
                    }}
                  />
                </ListItem>
              </List>
            }
          />
        </div>
      </ClickAwayListener>
    );
  }
}

export default withStyles(combineStyles(gantStyles, menuStyles), {
  withTheme: true
})(PlannedVsActualDropDown);
