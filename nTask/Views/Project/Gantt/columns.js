import moment from "moment";
import helper from "../../../helper";
import { store } from "../../../index";
let addButtonIcon =
  '<a class="taskAddIcon" title="Add Task"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 12 12">' +
  '  <path id="icon_add_copy" data-name="icon add copy" style="fill-rule: evenodd;" d="M997.349,216.163h-4.512v-4.512a0.864,0.864,0,0,0-1.675,0v4.512h-4.511a0.864,0.864,0,0,0,0,1.674h4.511v4.512a0.864,0.864,0,0,0,1.675,0v-4.512h4.512A0.864,0.864,0,0,0,997.349,216.163Z" transform="translate(-986 -211)"/>' +
  "</svg></a>";
let checklistIcon = `<a class="checklistIcon" title="Checklist"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 21 21">
  
<circle id="Ellipse_1918_copy_5" data-name="Ellipse 1918 copy 5" cx="10.5" cy="10.5" r="10"/>
<path id="Tick_copy_5" data-name="Tick copy 5" style="stroke-linejoin: round;
      stroke-width: 1px; fill: #fff;
      stroke: #fff;
      fill-rule: evenodd;" d="M189.864,431.2a0.527,0.527,0,0,0-.708,0l-5.994,5.612-2.3-2.157a0.524,0.524,0,0,0-.707,0,0.446,0.446,0,0,0,0,.662l2.657,2.489a0.526,0.526,0,0,0,.708,0l6.348-5.943A0.449,0.449,0,0,0,189.864,431.2Z" transform="translate(-174.5 -423.5)"/>
</svg>
</a>`
let taskDetailsIcon = `<a class="taskExpandIconCnt" title="Task Details" ><svg version="1.1" class="taskExpandIcon" width="14" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
<g>
<g>
 <path d="M507.296,4.704c-3.36-3.36-8.032-5.056-12.768-4.64L370.08,11.392c-6.176,0.576-11.488,4.672-13.6,10.496
   s-0.672,12.384,3.712,16.768l33.952,33.952L224.448,242.272c-6.24,6.24-6.24,16.384,0,22.624l22.624,22.624
   c6.272,6.272,16.384,6.272,22.656,0.032l169.696-169.696l33.952,33.952c4.384,4.384,10.912,5.824,16.768,3.744
   c2.24-0.832,4.224-2.112,5.856-3.744c2.592-2.592,4.288-6.048,4.608-9.888l11.328-124.448
   C512.352,12.736,510.656,8.064,507.296,4.704z"/>
</g>
</g>
<g>
<g>
 <path d="M448,192v256H64V64h256V0H32C14.304,0,0,14.304,0,32v448c0,17.664,14.304,32,32,32h448c17.664,0,32-14.336,32-32V192H448z
   "/>
</g>
</a>
</svg>`
const getRandomColor = str => {
  var hash = 0;
  for (var i = 0; i < str.length; i++) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash);
  }
  var h = hash % 360;
  return "hsl(" + h + ", " + 52 + "%, " + 65 + "%)";
};

const userAvatarChar = (firstName, lastName, email) => {
  const letters =
    firstName && lastName
      ? `${firstName[0].toUpperCase()}${lastName[0].toUpperCase()}`
      : firstName
        ? firstName[0].toUpperCase()
        : email[0].toUpperCase();
  return letters;
};

let textEditor = { type: "custom_editor", map_to: "text" };
let attachmentIcon =
  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAPCAYAAADUFP50AAABRElEQVQokYXSP0hWcRTG8Y8htEhzKE6FLjq4CQ6FgkH+AxUhQnEQNFykUWmRkBZRJwcXJx3CwAYdBOEFlXATGlrEVSEJXjAwpOQX5+rlvlpnufA753uec55zq0qlkv/EQzxDDQ5wmsof/JvRgm+YxjC+4k1KVFeU3kYbNjCOzXh9gi/Yuw9sxzpeYTf3fhzNXtw1ahfW0F+AsniMchEcwAq6cViBMIRWfMyDafmlUHyLmQKU8gtpTPzIdnyNWbyM72/M5aCJcLYjXP7rai0W8Rzz+I5RXAWU1CfjlidZpwT2YDucvMRIKKZ4F9N04iI/d9rxEc7wFPs56AMGo/Fq3PMmkuJRFKU9Psev1Yy6cDndMzV8X1Tcwc8YMTn2C1sYw6fIT+FPEUwPfagPxQb0xvGXw82KyM5xHnAjmmKCZEr5LgiuAS9lQ7r+p4kWAAAAAElFTkSuQmCC";
let issueIcon =
  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAANCAYAAACdKY9CAAAAwklEQVQokZWSuwrCMBhGT0WLWhWdXBUUik7OvpNv4eLm6vP4AA7FdhD0DcQrHbzx2RSEEKgHQkL4Tv7cvDiOZ8AEuAAeGY+fcU4D2JSBJX9QMisX5aQKNROeAjuHOATWQCDh9SMMrGhG1/RPCSngAwsrZnOXEJjpfhiGBysCJEnSA/ZAS9d6BpqmUmqlM6qmfQ+t7YgVcHQIHUDv5UtQy4WtQxgZoaLwTXsDogKHvurhFC5KWxXmwNhU0v95O/o6EH0Aej0ntFrelewAAAAASUVORK5CYII=";
let riskIcon =
  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAMCAYAAABSgIzaAAAA0klEQVQokX3SMUoEQRCF4U/XBTHwAoIoJmKwoeEGBsZ6FMHA1MVY1wPoVcREMDVwzIzMxUAEE6mhlm16xnlQ0F39v3pQMytN0+jRBq7wi0t818ha19NqhrM8j3BeA32J+3jBV943McFbCa12srjFGEdZ4+wZMp7iOM+fWbJ38p8xFjLv5C91k0zHeIHtauiouO8k02qxnF28Yr0AH+Md06L3gwO8LxKvK1PoIatUMMG2iTExptc6zMTnnrdp/ADxjfr0NLCoSRjvsIetAbDUB+7/AF4VJSMkKuKXAAAAAElFTkSuQmCC";
let commentIcon =
  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAABTklEQVQokYXSu0peURCG4cdTIygpbIKx9AKUNBZBMSCIVSBYJqKdkguIYG0jKIKdXkJA7AJp/sJKcrBQQ2LERhASDHhqRAyjs3Xn36gfbPZirXnXfDOzGmq1mjo1oRfP0Y5jfM7vshzaWAe+wXdM4wn+5v89tvL8Vs25aMByZhrAQb0ddGIBLzEeLorM03nB6D2g3B9N6+HkOnPYeoseXFWQ/xXn77CJpYCHsYazDFtBWwXjBBMZtxpcwF34WQqaQ2sF5by0/oVnAf/G09LBTjauqbR3mSMr1IHDgNfxAbNZU1j+iJZS8AWG0no09hVeB/wD37IRixnQVzF9p0lsY6+Y8xQ+pdX5SviNImMkiKbFW7h9JFHPIPYTjvF14yjrCydj+IIXRf0FHDrN5sXMZ3KW0bg/+Bo1YrdspQyHYkQj6H/gpd0Lx+0bj0HXwj9clETyPXukxQAAAABJRU5ErkJggg==";
let meetingIcon =
  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAOCAYAAAAfSC3RAAABF0lEQVQokXXSvS5EURTF8Z8bpQQFBd20IjodxUhM1N5AQiehpyY60UwhQqNQewQfBdUI8wgS8ZWYKJEd+8q4mVnJyd3Z/7XPWTnnDrTbbakZNHCEZ/81hhWc4z7IYOIBLGEHV7hDkewLs9hN3wO+A9ZxWDmhhddcrQoL70KRuw3jLY2driRlqpdkT+mtl3EaCRZzTXQNRj2X/U56i3LnIZzpr70q6Y60j8e+o7+axLqumwtt4BY1nCQrsq4l2y7NRWXHaaxiBMu5ol5L1jNq6ADHeMd89j4xio+8i54nbuazTOESF1lHL1jPE6O+QTPf6zT7UUcvWPw5f+aIErrOiKFyKBTvF9rKyKHnGIxfKL7jldj9dIrmD+7bOl5255OsAAAAAElFTkSuQmCC";
// var formatter = gantt.ext.formatters.durationFormatter({
//   enter: "day",
//   store: "day",
//   format: "auto"
// });
// var linksFormatter = gantt.ext.formatters.linkFormatter({ durationFormatter: formatter });


let editors = {
  successors: { type: "successor", map_to: "auto" },
  predecessors: { type: "predecessor", map_to: "auto" },
  progress: { type: "text", map_to: "progress" },
  duration: { type: "number", map_to: "duration", min: 0, max: 9999 },
};
const allWorkspaces = store.getState().profile.data;
const activeWorkspace = allWorkspaces.workspace.find(w => {
  return w.teamId == allWorkspaces.loggedInTeam
});

// function CanAccessFeature() {
//   const state = useSelector(state => {
//     return {
//       obj: state.featureAccessPermissions
//     }
//   });
//   const activeWorkspace = allWorkspaces.workspace.find(w => {
//     return w.teamId == allWorkspaces.loggedInTeam
//   });
//   return true
// }
const columns = [
  {
    name: "text",
    id: "text",
    label: "Task Name",
    value: "Task Name",
    tree: true,
    editor: textEditor,
    resize: true,
    min_width: 100,
    width: "*",
    template: function (obj) {
      return `<div class="ganttTaskDragHandle"></div><div class="taskColor" style="background: ${obj.colorCode}; opacity: ${obj.colorCode == "#ffffff" ? 0 : 1}"></div>${obj.text}`
    }
  },
  {
    name: "uniqueId",
    id: "uniqueId",
    value: "ID",
    align: "center",
    label: "ID",
    resize: true,
    min_width: 50,
    width: 50,
    hide: true,
    template: function (task) {
      let wbsCode = window.gantt.getWBSCode(task)
      return wbsCode;
      // return task.sortorder;
    }
  },
  {
    name: "progress",
    id: "progress",
    value: "Progress",
    label: "Progress",
    align: "center",
    resize: true,
    min_width: 80,
    hide: true,
    editor: activeWorkspace.taskProgressCalculation ? editors.progress : null,
    template: function (obj) {
      const progressValue = (obj.progress * 100).toFixed();
      if (obj.type == "milestone") {
        return "";
      } else {
        return (
          "<p class='cellInlineValue'>" + progressValue + "%" + "</p>"
        );
      }
    }
  },
  {
    name: "duration",
    id: "duration",
    value: "Duration",
    label: "Duration",
    align: "center",
    resize: true,
    min_width: 70,
    editor: editors.duration
  },
  {
    name: "actual_duration",
    id: "actual_duration",
    value: "Act. Duration",
    label: "Act. Duration",
    align: "center",
    resize: true,
    min_width: 70,
    // template: function (obj) {
    //   const actualDuration = obj.actual_duration ? moment(obj.actual_duration).format("MMM DD, YYYY") : "-";
    //   if (obj.type == "milestone") {
    //     return "";
    //   } else {
    //     return (
    //       // "<div class='headingValueCellCnt'><p class='cellInlineHeading'>Plan. End Date</p>" +
    //       "<p class='cellInlineValue'>" + actualDuration + "</p>"
    //       // </div>"
    //     );
    //   }
    // }
    // editor: editors.actual_duration
  },
  {
    name: "estiTime",
    id: "estiTime",
    label: "Estimated Effort",
    value: "Estimated Effort",
    width: 70,
    resize: true,
    min_width: 70,
    hide: true,
    align: "center",
    template: function (obj) {
      const estiTime = obj.estiTime ? obj.estiTime : "-";
      if (obj.type == "milestone") {
        return "";
      } else {
        return (
          // "<div class='headingValueCellCnt'><p class='cellInlineHeading'>Est. Time</p>" +
          "<p class='cellInlineValue'>" + estiTime + "</p>"
          // </div>"
        );
      }
    }
  },
  {
    name: "actTime",
    id: "actTime",
    label: "Actual Effort",
    value: "Actual Effort",
    width: 70,
    resize: true,
    min_width: 70,
    hide: true,
    align: "center",
    template: function (obj) {
      const actTime = obj.actTime ? obj.actTime : "-";
      if (obj.type == "milestone") {
        return "";
      } else {
        return (
          // "<div class='headingValueCellCnt'><p class='cellInlineHeading'>Act. Time</p>" +
          "<p class='cellInlineValue'>" + actTime + "</p>"
          // </div>"
        );
      }
    }
  },
  {
    name: "planned_start",
    id: "planned_start",
    label: "Plan. Start Date",
    value: "Plan. Start Date",
    width: 70,
    min_width: 70,
    resize: true,
    hide: true,
    type: 'date',
    align: "center",
    template: function (obj) {
      const plannedStart = obj.planned_start
        ? moment(obj.planned_start).format("MMM DD, YYYY")
        : "-";
      if (obj.type == "milestone") {
        return "";
      } else {
        return (
          // "<div class='headingValueCellCnt'><p class='cellInlineHeading'>Plan. Start Date</p>" +
          "<p class='cellInlineValue'>" + plannedStart + "</p>"
          // </div>"
        );
      }
    }
  },
  {
    name: "planned_end",
    id: "planned_end",
    label: "Plan. End Date",
    value: "Plan. End Date",
    width: 70,
    min_width: 70,
    resize: true,
    hide: true,
    type: 'date',
    align: "center",
    template: function (obj) {
      const plannedEnd = obj.planned_end
        ? moment(obj.planned_end).format("MMM DD, YYYY")
        : "-";
      if (obj.type == "milestone") {
        return "";
      } else {
        return (
          // "<div class='headingValueCellCnt'><p class='cellInlineHeading'>Plan. End Date</p>" +
          "<p class='cellInlineValue'>" + plannedEnd + "</p>"
          // </div>"
        );
      }
    }
  },
  {
    name: "start_date",
    id: "start_date",
    label: "Act. Start Date",
    value: "Act. Start Date",
    width: 70,
    resize: true,
    min_width: 70,
    type: 'date',
    hide: true,
    align: "center",
    template: function (obj) {

      const actualStartDate = obj.actual_start ? moment(obj.actual_start).format("MMM DD, YYYY") : "-";
      if (obj.type == "milestone") {
        return "";
      } else {
        return (
          // "<div class='headingValueCellCnt'><p class='cellInlineHeading'>Plan. End Date</p>" +
          "<p class='cellInlineValue'>" + actualStartDate + "</p>"
          // </div>"
        );
      }
    }
  },
  {
    name: "actual_end",
    id: "actual_end",
    label: "Act. End Date",
    value: "Act. End Date",
    width: 70,
    resize: true,
    min_width: 70,
    type: 'date',
    hide: true,
    align: "center",
    template: function (obj) {
      const actualEndDate = obj.actual_end ? moment(obj.actual_end).format("MMM DD, YYYY") : "-";
      if (obj.type == "milestone") {
        return "";
      } else {
        return (
          // "<div class='headingValueCellCnt'><p class='cellInlineHeading'>Plan. End Date</p>" +
          "<p class='cellInlineValue'>" + actualEndDate + "</p>"
          // </div>"
        );
      }
    }
  },
  { 
    name: "assignee",
    id: "assignee",
    align: "center",
    resize: true,
    label: "Assignees",
    value: "Assignees",
    width: 110,
    min_width: 110,
    hide: true,
    template: function (obj) {
      let assignee = [...obj.assignee]; // copying the assignee object
      assignee.length = 3; // trimming array size to 3 assignee
      let assigneeImages = `<div class="assigneeCellCnt">
    ${assignee.map((assignee, i) => {
        return assignee.imageUrl
          ? `<img class="assigneeAvatars" src=${assignee.imageUrl
          } alt="profileImage" />`
          : `<div class="letterAvatar" style=
        "background: ${getRandomColor(assignee.fullName)}">${userAvatarChar(
            assignee.fullName
          )}</div>`;
      })}
    ${obj.assignee.length > 3
          ? `<div class="countAvatar">+${obj.assignee.length - 3} </div>`
          : ""
        }
    </div>`;
      if (obj.type == "milestone") {
        return "";
      } else {
        return assigneeImages.split(",").join("");
      }
    }
  },
  {
    name: "priority",
    id: "priority",
    value: "Priority",
    align: "center",
    label: "Priority",
    resize: true,
    min_width: 70,
    width: 70,
    hide: true
  },
  {
    name: "issues",
    id: "issues",
    align: "center",
    label: "Issues",
    value: "Issues",
    resize: true,
    hide: true,
    width: 70,
    min_width: 70,
    template: function (obj) {
      if (obj.type == "milestone") {
        return "";
      } else {
        return (
          '<span class="cellInlineValueWithIcon"><img src="' +
          issueIcon +
          '" alt="IssueIcon"/>' +
          obj.issues +
          "</span>"
        );
      }
    }
  },
  {
    name: "risks",
    id: "risks",
    align: "center",
    label: "Risks",
    value: "Risks",
    resize: true,
    min_width: 70,
    width: 70,
    hide: true,
    template: function (obj) {
      if (obj.type == "milestone") {
        return "";
      } else {
        return (
          '<span class="cellInlineValueWithIcon"><img src="' +
          riskIcon +
          '" alt="RiskIcon"/>' +
          obj.risks +
          "</span>"
        );
      }
    }
  },
  {
    name: "attachments",
    id: "attachments",
    align: "center",
    width: 70,
    resize: true,
    min_width: 70,
    label: "Attachments",
    value: "Attachments",
    hide: true,
    template: function (obj) {
      if (obj.type == "milestone") {
        return "";
      } else {
        return (
          '<span class="cellInlineValueWithIcon"><img src="' +
          attachmentIcon +
          '" alt="attachmentIcon"/>' +
          obj.attachments +
          "</span>"
        );
      }
    }
  },
  {
    name: "comments",
    id: "comments",
    align: "center",
    width: 70,
    resize: true,
    min_width: 70,
    label: "Comments",
    value: "Comments",
    hide: true,
    template: function (obj) {
      if (obj.type == "milestone") {
        return "";
      } else {
        return (
          '<span class="cellInlineValueWithIcon"><img src="' +
          commentIcon +
          '" alt="attachmentIcon"/>' +
          obj.comments +
          "</span>"
        );
      }
    }
  },
  {
    name: "status",
    id: "status",
    resize: true, align: "center", width: 70, min_width: 70, label: "Status", value: "Status", hide: true
  },
  {
    name: "cost",
    id: "cost",
    resize: true, align: "center", width: 70, min_width: 70, label: "Cost", hide: true
  },
  {
    name: "creationDate",
    id: "creationDate",
    label: "Creation Date",
    value: "Creation Date",
    width: 70,
    resize: true,
    min_width: 70,
    align: "center",
    type: 'date',
    hide: true,
    template: function (obj) {
      const creationDate = obj.planned_end
        ? moment(obj.creationDate).format("MMM DD, YYYY")
        : "-";
      if (obj.type == "milestone") {
        return "";
      } else {
        return "<p class='cellInlineValue'>" + creationDate + "</p>";
      }
    }
  },
  {
    name: "meetings",
    id: "meetings",
    align: "center",
    label: "Meetings",
    value: "Meetings",
    width: 70,
    min_width: 70,
    resize: true,
    hide: true,
    template: function (obj) {
      if (obj.type == "milestone") {
        return "";
      } else {
        return (
          '<span class="cellInlineValueWithIcon"><img src="' +
          meetingIcon +
          '" alt="attachmentIcon"/>' +
          obj.meetings +
          "</span>"
        );
      }
    }
  },
  {
    name: "predecessors",
    label: "Predecessors",
    value: "Predecessors",
    // max_width: 100,
    min_width: 70,
    align: "center",
    editor: editors.predecessors,
    resize: true,
    template: function (task) {
      let links = task.$target;
      let labels = [];
      for (let i = 0; i < links.length; i++) {
        let link = gantt.getLink(links[i]);
        let pred = gantt.getTask(link.source);
        labels.push(gantt.getWBSCode(pred));
      }
      return labels.join(", ");
    },
  },
  {
    name: "successors",
    label: "Successors",
    value: "Successors",
    // max_width: 100,
    min_width: 70,
    // hide: true,
    align: "center",
    editor: editors.successors,
    resize: true,
    template: function (task) {
      let links = task.$source;
      let labels = [];
      for (let i = 0; i < links.length; i++) {
        let link = window.gantt.getLink(links[i]);
        let pred = window.gantt.getTask(link.target);
        labels.push(gantt.getWBSCode(pred));
      }
      return labels.join(", ");
    },
  },
  {
    name: "buttons",
    id: "buttons",
    label: addButtonIcon,
    align: "left",
    value: addButtonIcon,
    resize: true,
    max_width: 110,
    min_width: 110,
    template: function (obj) {
      if (obj.type == "milestone") {
        return "";
      } else {
        return `${addButtonIcon}${checklistIcon}${taskDetailsIcon}`;

      }
    }
  }
];

export default columns;
