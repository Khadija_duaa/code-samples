import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles, withTheme } from "@material-ui/core/styles";
import gantStyles from "./style";
import menuStyles from "../../../assets/jss/components/menu";
import CustomIconButton from "../../../components/Buttons/IconButton";
import SelectionMenu from "../../../components/Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import combineStyles from "../../../utils/mergeStyles";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import { Scrollbars } from "react-custom-scrollbars";
import { SaveColumnOrder } from "../../../redux/actions/gantt";
import { injectIntl } from "react-intl";
import { CanAccess } from "../../../components/AccessFeature/AccessFeature.cmp";

let total = 5;

class GanttColumnsDropDown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "top-start",
      pickerOpen: false,
      pickerPlacement: "",
      checked: [],
      isLoaded: false,
      loggedInTeam: "",
      isFirstLoad: false,
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
  }
  handleClose(event) {
    this.setState({ open: false, pickerOpen: false });

    this.props.SaveColumnOrder(this.props.projectId, this.state.checked);
  }
  handleClick(event, placement) {
    event.stopPropagation();
    this.setState((state) => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }

  componentDidMount() {
    this.setState({
      checked: this.props.ganttTasksState.ganttTaskColumn
        ? this.props.ganttTasksState.ganttTaskColumn.ganttTaskColumnOrder
        : [],
      loggedInTeam: this.props.profileState.data.loggedInTeam,
      isFirstLoad: true,
    });
  }

  handleToggle = (e, value) => {
    e.stopPropagation();
    const { checked } = this.state;
    const { handleGanttColumn } = this.props;
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex > -1) {
      newChecked.splice(currentIndex, 1);
    } else if (checked.length <= total - 1) {
      newChecked.push(value);
    }
    this.setState({ checked: newChecked }, () => {
      handleGanttColumn(this.state.checked);
    });
  };
  getTranslatedColumn = (label) => {
    switch (label) {
      case "Task Name":
        label = this.props.intl.formatMessage({
          id: "project.gant-view.columns.task-name",
          defaultMessage: "Task Name",
        });
        break;
      case "Actual Effort":
        label = this.props.intl.formatMessage({
          id: "project.gant-view.columns.actual-effort",
          defaultMessage: "Actual Effort",
        });
        break;
      case "Attachments":
        label = this.props.intl.formatMessage({
          id: "project.gant-view.columns.attachment",
          defaultMessage: "Attachments",
        });
        break;
      case "Assignees":
        label = this.props.intl.formatMessage({
          id: "project.gant-view.columns.assignees",
          defaultMessage: "Assignees",
        });
        break;
      case "Comments":
        label = this.props.intl.formatMessage({
          id: "common.comment.label",
          defaultMessage: "Comments",
        });
        break;
      case "Status":
        label = this.props.intl.formatMessage({
          id: "common.status.label",
          defaultMessage: "Status",
        });
        break;
      case "Priority":
        label = this.props.intl.formatMessage({
          id: "project.gant-view.columns.priority",
          defaultMessage: "Priority",
        });
        break;
      case "Act. End Date":
        label = this.props.intl.formatMessage({
          id: "project.gant-view.columns.act-end-date",
          defaultMessage: "Act. End Date"
        });
        break;
      case "Act. Start Date":
        label = this.props.intl.formatMessage({
          id: "project.gant-view.columns.act-start-date",
          defaultMessage: "Act. Start Date"
        });
        break;
      case "Act. Duration":
        label = "Act. Duration";
        break;
      case "Plan. Start Date":
        label = this.props.intl.formatMessage({
          id: "project.gant-view.columns.plan-start-date",
          defaultMessage: "Plan. Start Date"
        });
        break;
      case "Plan. End Date":
        label = this.props.intl.formatMessage({
          id: "project.gant-view.columns.plan-end-date",
          defaultMessage: "Plan. End Date"
        });
        break;
      case "Cost":
        label = this.props.intl.formatMessage({
          id: "project.gant-view.columns.cost", defaultMessage: "Cost"
        })
        break;
      case "Creation Date":
        label = this.props.intl.formatMessage({
          id: "project.gant-view.columns.creation-date", defaultMessage: "Creation Date"
        })
        break;
      case "Risks":
        label = this.props.intl.formatMessage({
          id: "risk.label", defaultMessage: "Risks"
        })
        break;
      case "Meetings":
        label = this.props.intl.formatMessage({
          id: "meeting.label", defaultMessage: "Meetings"
        })
        break;
      case "Issues":
        label = this.props.intl.formatMessage({
          id: "issue.label", defaultMessage: "Issues"
        })
        break;
    }
    return label;
  };
  render() {
    const { classes, theme, selectedColor, colorChange, style } = this.props;
    const { open, placement, checked } = this.state;
    let ddData = [
      "ID",
      "Progress",
      "Actual Effort",
      "Attachments",
      "Comments",
      "Creation Date",
      "Estimated Effort",
      "Issues",
      "Meetings",
      "Risks",
      "Predecessors",
      "Successors",
      "Duration"
    ];
    if (CanAccess({ group: 'task', feature: 'priority' })) {
      ddData.push("Priority");
    }
    if (CanAccess({ group: 'task', feature: 'statusTitle' })) {
      ddData.push("Status");
    }
    if (CanAccess({ group: 'task', feature: 'assigneeList' })) {
      ddData.push("Assignees");
    }
    if (CanAccess({ group: 'task', feature: 'startDate' })) {
      ddData.push("Plan. Start Date");
    }
    if (CanAccess({ group: 'task', feature: 'dueDate' })) {
      ddData.push("Plan. End Date");
    }
    if (CanAccess({ group: 'task', feature: 'actualStartDate' })) {
      ddData.push("Act. Start Date");
    }
    if (CanAccess({ group: 'task', feature: 'actualDueDate' })) {
      ddData.push("Act. End Date");
    }
    ddData.push("Act. Duration");


    return (
      // <ClickAwayListener onClickAway={this.handleClose}>
        <div>
          <CustomIconButton
            btnType="condensed"
            style={style}
            onClick={(event) => {
              this.handleClick(event, "bottom-end");
            }}
            buttonRef={(node) => {
              this.anchorEl = node;
            }}
          >
            <MoreVerticalIcon
              htmlColor={theme.palette.secondary.medDark}
              style={{ fontSize: "24px" }}
            />
          </CustomIconButton>
          <SelectionMenu
            isAddColSelectionClicked={true}
            open={open}
            closeAction={this.handleClose}
            placement={placement}
            checkedType="multi"
            anchorRef={this.anchorEl}
            list={
              <Scrollbars autoHide={false} style={{ height: 250 }}>
                <List>
                  <ListItem
                    disableRipple={true}
                    classes={{ root: classes.menuHeadingItem }}
                  >
                    <ListItemText
                      primary={`${checked.length
                        }/${total} ${this.props.intl.formatMessage({
                          id: "common.selected.label",
                          defaultMessage: "Selected",
                        })}`}
                      classes={{ primary: classes.menuHeadingListItemText }}
                    />
                  </ListItem>
                  {ddData.map((value) => (
                    <ListItem
                      key={value}
                      button
                      disableRipple={true}
                      className={`${classes.MenuItem} ${this.state.checked.indexOf(value) !== -1
                        ? classes.selectedValue
                        : ""
                        }`}
                      classes={{ selected: classes.statusMenuItemSelected }}
                      onClick={(e) => this.handleToggle(e, value)}
                    >
                      <ListItemText
                        primary={this.getTranslatedColumn(value)}
                        classes={{
                          primary: classes.statusItemText,
                        }}
                      />
                    </ListItem>
                  ))}
                </List>
              </Scrollbars>
            }
          />
        </div>
      // </ClickAwayListener>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profileState: state.profile,
    ganttTasksState: state.ganttTasks,
  };
};

export default compose(
  withRouter,
  injectIntl,
  withStyles(combineStyles(gantStyles, menuStyles), {
    withTheme: true,
  }),
  connect(mapStateToProps, { SaveColumnOrder })
)(GanttColumnsDropDown);
