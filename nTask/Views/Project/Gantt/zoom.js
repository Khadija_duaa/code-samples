export function setZoom(value, gantt) {
    switch (value) {
      case 0:
        gantt.config.min_column_width = 100;
        gantt.config.scale_unit = "week";
        gantt.config.date_scale = "%M, %Y";
        gantt.config.scale_height = 50;

        var daysStyle = function(date) {
          var dateToStr = gantt.date.date_to_str("%D");
          // if (dateToStr(date) == "Sun" || dateToStr(date) == "Sat")
          if (!gantt.isWorkTime(date))
            return "weekend";

          return "";
        };

        gantt.config.subscales = [
          { unit: "day", step: 1, date: "%D, %d", css: daysStyle }
        ];
        break;
      case 1:
        gantt.config.scale_unit = "month";
        gantt.config.step = 1;
        gantt.config.date_scale = "%Y";
        gantt.config.min_column_width = 150;

        gantt.config.scale_height = 50;

        var weekScaleTemplate = function(date) {
          var dateToStr = gantt.date.date_to_str("%d %M");
          var endDate = gantt.date.add(
            gantt.date.add(date, 1, "week"),
            -1,
            "day"
          );
          return dateToStr(date) + " - " + dateToStr(endDate);
        };

        var daysStyle = function(date) {
          var dateToStr = gantt.date.date_to_str("%D");
          // if (dateToStr(date) == "Sun" || dateToStr(date) == "Sat")
          if (!gantt.isWorkTime(date))
            return "weekend";

          return "";
        };

        gantt.config.subscales = [
          { unit: "week", step: 1, template: weekScaleTemplate }
        ];
        break;
      case 2:
        gantt.config.scale_unit = "month";
        gantt.config.step = 1;
        gantt.config.date_scale = "%Y";
        gantt.config.min_column_width = 100;

        gantt.config.scale_height = 50;

        var monthScaleTemplate = function(date) {
          var dateToStr = gantt.date.date_to_str("%d %M");
          var endDate = gantt.date.add(
            gantt.date.add(date, 1, "month"),
            -1,
            "day"
          );
          return dateToStr(date) + " - " + dateToStr(endDate);
        };

        var daysStyle = function(date) {
          var dateToStr = gantt.date.date_to_str("%D");
          // if (dateToStr(date) == "Sun" || dateToStr(date) == "Sat")
          if (!gantt.isWorkTime(date))
            return "weekend";

          return "";
        };

        gantt.config.subscales = [
          { unit: "month", step: 1, template: monthScaleTemplate }
        ];
        break;
      default:
        break;
    }
  }