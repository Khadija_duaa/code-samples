
import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from "@material-ui/core/ListItemText";
import { withStyles } from "@material-ui/core/styles";
import SvgIcon from "@material-ui/core/SvgIcon";
import DownArrow from "@material-ui/icons/ArrowDropDown";
import { cloneDeep } from "lodash";
import moment from "moment";
import React, { Component, Fragment } from "react";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import { compose } from "redux";
import menuStyles from "../../../assets/jss/components/menu";
import ViewIcon from "../../../components/Icons/ViewIcon";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import SelectionMenu from "../../../components/Menu/SelectionMenu";
import combineStyles from "../../../utils/mergeStyles";
import columns from "./columns";
import { exportToPdf, exportToExcel, exportToMSProject } from "../../../redux/actions/gantt";
import fileDownload from "js-file-download";
import { withSnackbar } from "notistack";

import { exportGanttStyle } from "./exportGanttStyle";
import gantStyles from "./style";
import ExportColumnGantt from "../../../components/Dialog/ExportColumn/ExportColumnGantt";
import MSProjectIcon from "../../../components/Icons/MSProjectIcon";
import PdfProjectIcon from "../../../components/Icons/PdfProjectIcon";
import ExcelProjectIcon from "../../../components/Icons/ExcelProjectIcon";
import CircularProgress from "@material-ui/core/CircularProgress";
import { addPendingHandler } from "../../../redux/actions/backProcesses";

class ViewDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      pickerOpen: false,
      pickerPlacement: "",
      archiveFlag: false,
      unArchiveFlag: false,
      deleteFlag: false,
      renameRoleDialogOpen: false,
      exportFlag: false,
      btnLoader: false,
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handlePickerClose = this.handlePickerClose.bind(this);
    // this.exportToMSProject = this.exportToMSProject.bind(this);
  }
  handleDialogOpen = () => {
    this.setState({ renameRoleDialogOpen: true });
  };
  handleDialogClose = () => {
    this.setState({ renameRoleDialogOpen: false });
  };
  handleClose(event) {
    this.setState({ open: false, pickerOpen: false });
  }
  handlePickerClose(event) {
    this.setState({ pickerOpen: false });
  }
  // componentWillUnmount() {
  //   this.props.handleGanttView(null, "Default")
  //    }
  sortDates = (a, b) => {
    return a.getTime() - b.getTime();
  };
  projectEnd = () => {
    const filterGanttData = this.props.ganttTaskData.data.filter((ele) => {
      if (ele.plannedstartdate) {
        return ele;
      }
    });
    let dates = [];
    filterGanttData.forEach((task) => {
      dates.push(
        new Date(moment(task.plannedstartdate).clone().add(task.plannedDuration, "days").format())
      );
    });
    let sorted = dates.sort(this.sortDates);
    return moment(sorted[sorted.length - 1]).format("DD-MM-YYYY");
  };
  handleClick(event, placement) {
    const { currentTarget } = event;
    this.setState((state) => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }

  // exportToExcel = () => {
  //   const assigneeFormatter = (col, task) => {

  //     const assigneeListString = task.assignee.reduce((r, cv) => {
  //       r = r ? ` ${r} ${cv.fullName},` : `${cv.fullName},`;
  //       return r;
  //     }, "");
  //     return assigneeListString;
  //   };
  //   const dateFormatter = (col, task) => {
  //     return task[col.name] ? moment(task[col.name]).format("LL") : "";
  //   };
  //   const formatters = {
  //     assignee: assigneeFormatter,
  //     date: dateFormatter,
  //   };
  //   const formatter = gantt.ext.formatters.durationFormatter({
  //     enter: "day",
  //     store: "day",
  //     format: "auto",
  //   });
  //   const linksFormatter = window.gantt.ext.formatters.linkFormatter({
  //     durationFormatter: formatter,
  //   });

  //   let backup_config = cloneDeep(gantt.config.columns);
  //   const visibleColumns = columns.filter(c => !c.hide);
  //   visibleColumns.forEach((x, i) => {

  //     visibleColumns[i] = {
  //       ...x,
  //       header: x.label,
  //       id: name,
  //       template: x.name == 'predecessors' || x.name == 'successors' ? x.template : formatters[x.type || x.name]
  //         ? (task) => formatters[x.type || x.name](x, task)
  //         : false,
  //     };
  //   });
  //   window.gantt.config.columns = visibleColumns;
  //   window.gantt.render();
  //   window.gantt.exportToExcel({ visual: true, cellColors: true });
  //   window.gantt.config.columns = backup_config;
  //   window.gantt.render();
  //   // window.gantt?.exportToExcel({
  //   //   name: "document.xlsx",
  //   //   columns:[
  //   //     { id:"successors",  header:"Successors", width:250, type:"date" }
  //   //   ],
  //   //     // [
  //   //     // { id: "text", header: "Title", width: 150 },
  //   //     // window.gantt.getGridColumns().filter(c => c.name !== 'buttons').map(c => { return {...c, header: c.label, $template: true}}),
  //   //     // { id:"start_date",  header:"Start date", width:250, type:"date" }
  //   //     // { id: "Predecessor", header: "Predecessor", width: 250, type: "predecessor" },
  //   //   // ],
  //   //   // window.gantt.getGridColumns().filter(c => c.name !== 'buttons').map(c => { return {...c, header: c.label, $template: true}}),
  //   //   // server:"https://export.dhtmlx.com/gantt",
  //   //   visual: true,
  //   //   cellColors: true,
  //   //   date_format: "dddd d, mmmm yyyy",
  //   // });
  // };
  // exportToMSProject = () => {
  //   window.gantt?.exportToMSProject({
  //     name: "document.xml",

  //   });
  // };
  //   exportToPdf = () => {
  //     window.gantt?.exportToPDF({
  //       header:
  //         exportGanttStyle +
  //         `<style>.ganttFeaturesList{
  //     list-style-type: none;
  //     display: flex;
  //     margin: 0;
  //     padding: 5px
  // }
  // .ganttFeaturesList li{
  //     display: flex;
  //     align-items: center;
  //     margin-left: 20px;
  // }
  // .ganttFeaturesList li:first-child {
  //     margin-left: 0
  // }
  // .ganttFeaturesList li p{
  //     font-family: sans-serif;
  //     line-height: normal;
  //     margin:0;
  //     padding: 0;
  //     font-size: 14px;
  //     display: block;
  //     width: 80px;
  // }
  // .ganttFeaturesList li img{
  //     marginLeft: 10px;
  // }
  // .ganttFeaturesCnt{
  //     width: 100%;
  //     display: flex;
  // }
  // .ganttFeaturesListCnt{
  //         display: flex;
  //     flex-direction: column;
  //     border: 1px solid #dfdfdf;
  //     padding: 0 10px;
  //     flex: 1;
  // }
  // .ganttFeaturesLeftCnt{
  //     border: 1px solid #dfdfdf;
  //     border-right: 0;
  //     display: flex;
  //     flex-direction: column;
  //     padding: 5px 20px;
  //     justify-content: center;
  //     width: 390px
  // }
  // .ganttFeaturesLeftCnt{
  // margin: 0;
  // padding: 0
  // }
  // </style>`,
  //       footer: `<div class=ganttFeaturesCnt><div class=ganttFeaturesLeftCnt><p>Project: Design Delivery Schedule</p> <p>Date: ${this.projectEnd()}</p></div><div class=ganttFeaturesListCnt><ul class=ganttFeaturesList><li><p>Task</p>
  // <img src=https://www.ntaskmanager.com/wp-content/uploads/2022/06/taskbar.png></li><li><p>Task Link</p> <img src=https://www.ntaskmanager.com/wp-content/uploads/2022/06/arrow.png></li><li><p>Milestone</p> <img src=https://www.ntaskmanager.com/wp-content/uploads/2022/06/milestone.png></li></ul><ul class=ganttFeaturesList><li><p>Today</p> <img src=https://www.ntaskmanager.com/wp-content/uploads/2022/06/today.png></li><li><p>Project End</p> <img src=https://www.ntaskmanager.com/wp-content/uploads/2022/06/project-end.png></li></ul></div></div></body>`,
  //       name: "ntask-gantt.pdf",
  //       raw: true,
  //     });
  //   };
  handleExports = (type) => {
    this.handleClose();
    const { projectId, projectName } = this.props;
    if (type == 'pdf') {
      this.setState({ exportFlag: true, exportType: 'pdf' })
      // this.props.exportToPdf(projectId,
      //   res => {
      //     fileDownload(res.data, `${projectName}.pdf`); 
      //   }
      // );
    } else if (type == 'excel') {
      this.setState({ exportFlag: true, exportType: 'excel' })
      // this.props.exportToExcel(projectId,
      //   res => {
      //     fileDownload(res.data, `${projectName}.xlsx`); 
      //   }
      // );
    } else if (type == 'project') {
      // this.setState({ btnLoader: true });
      const postObj = {
        projectId: projectId,
        columns: []
      }
      const obj = {
        type: 'projects',
        apiType: 'post',
        data: postObj,
        fileName: `${projectName}.mpp`,
        apiEndpoint: 'api/DocsFileUploader/ExportMSProjects',
      }
      this.props.addPendingHandler(obj, null);

      // this.props.exportToMSProject({
      //   projectId: projectId,
      //   columns: []
      // },
      //   res => {
      //     fileDownload(res.data, `${projectName}.mpp`);
      //     this.setState({ btnLoader: false })
      //   },
      //   err => {
      //     this.setState({ btnLoader: false });
      //     this.showSnackBar(`Unfortunately, we have encountered a problem. Please try again.`, "error");
      //   }
      // );
    } else if (type == 'primemera') {

      const postObj = {
        projectId: projectId,
        columns: []
      }
      const obj = {
        type: 'projects',
        apiType: 'post',
        data: postObj,
        fileName: `${projectName}.mpp`,
        apiEndpoint: 'api/DocsFileUploader/ExportMSProjects',
      }
      this.props.addPendingHandler(obj, null);

      // this.props.exportToMSProject({
      //   projectId: projectId,
      //   columns: []
      // },
      //   res => {
      //     fileDownload(res.data, `${projectName}.xer`);
      //     // this.props.showSnackBar('Activity Log exported successfully', "success");
      //   },
      //   err => {
      //     this.setState({ btnLoader: false });
      //     this.showSnackBar(`Unfortunately, we have encountered a problem. Please try again.`, "error");
      //   }
      // );
    }
  }
  handleCloseColumnPopup = () => {
    this.setState({ exportFlag: false });
  };

  handleBtnLoader = (state) => {
    this.setState({ btnLoader: state });
  }
  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right"
        },
        variant: type ? type : "info"
      }
    );
  };
  handelExportFailuer = (message) => {
    this.showSnackBar(`Unfortunately, we have encountered a problem. Please try again.`, "error");
  };
  render() {
    const { classes, theme, selectedView, handleGanttView, plannedVsActual, handleTaskbarsType } =
      this.props;
    const { open, placement, exportFlag, exportType, btnLoader } = this.state;

    return (
      <ClickAwayListener onClickAway={this.handleClose}>
        <div>
          <Button
            onClick={(event) => {
              this.handleClick(event, "bottom-end");
            }}
            // variant="text"
            buttonRef={(node) => {
              this.anchorEl = node;
            }}
            disabled={btnLoader}
            id="ganttViewSelectDropdown"
            disableRipple={true}
            className={classes.viewDropdownBtn}
            btnType="white"
            variant="contained">
            {btnLoader ?
              <CircularProgress
                classes={{ root: classes.circularProgress }}
              /> :
              <>
                <SvgIcon viewBox="0 0 18 18" className={classes.viewIcon}>
                  <ViewIcon htmlColor={theme.palette.secondary.medDark} />
                </SvgIcon>
                <DownArrow
                  htmlColor={theme.palette.secondary.medDark}
                  className={classes.dropdownArrow}
                />
              </>
            }
          </Button>

          <SelectionMenu
            open={open}
            closeAction={this.handleClose}
            placement={placement}
            style={{ width: 190 }}
            anchorRef={this.anchorEl}
            disablePortal={true}
            list={
              <List>
                <ListItem
                  button
                  disableRipple
                  selected={plannedVsActual == "Planned"}
                  classes={{ selected: classes.statusMenuItemSelected }}
                  onClick={(event) => handleTaskbarsType(event, "Planned")}>
                  <ListItemText
                    primary={
                      <FormattedMessage
                        id="project.gant-view.columns.planned"
                        defaultMessage="Planned"
                      />
                    }
                    classes={{
                      primary: classes.statusItemText,
                    }}
                  />
                </ListItem>

                <ListItem
                  button
                  disableRipple
                  classes={{ selected: classes.statusMenuItemSelected }}
                  selected={plannedVsActual == "Actual"}
                  onClick={(event) => handleTaskbarsType(event, "Actual")}
                // onClick={deleteMember}
                >
                  <ListItemText
                    primary={
                      <FormattedMessage
                        id="project.gant-view.columns.actual"
                        defaultMessage="Actual"
                      />
                    }
                    classes={{
                      primary: classes.statusItemText,
                    }}
                  />
                </ListItem>
                <ListItem
                  button
                  disableRipple
                  classes={{ selected: classes.statusMenuItemSelected }}
                  selected={plannedVsActual == "Planned vs Actual"}
                  onClick={(event) => handleTaskbarsType(event, "Planned vs Actual")}
                // onClick={deleteMember}
                >
                  <ListItemText
                    primary={
                      <FormattedMessage
                        id="project.gant-view.columns.plan-actual"
                        defaultMessage="Planned vs Actual"
                      />
                    }
                    classes={{
                      primary: classes.statusItemText,
                    }}
                  />
                </ListItem>
                <ListItem
                  button
                  disableRipple
                  classes={{ selected: classes.statusMenuItemSelected }}
                  style={{
                    borderTop: `1px solid ${theme.palette.border.lightBorder}`,
                  }}
                  selected={selectedView == "Progress Line"}
                  onClick={(event) => handleGanttView(event, "Progress Line")}
                // onClick={deleteMember}
                >
                  <ListItemText
                    primary={
                      <FormattedMessage
                        id="project.gant-view.columns.progress-line"
                        defaultMessage="Progress Line"
                      />
                    }
                    classes={{
                      primary: classes.statusItemText,
                    }}
                  />
                </ListItem>
                {plannedVsActual == "Actual" ? null : (
                  <ListItem
                    button
                    disableRipple
                    classes={{ selected: classes.statusMenuItemSelected }}
                    selected={selectedView == "Critical Path"}
                    onClick={(event) => handleGanttView(event, "Critical Path")}
                  // onClick={deleteMember}
                  >
                    <ListItemText
                      primary={
                        <FormattedMessage
                          id="project.gant-view.columns.critical-path"
                          defaultMessage="Critical Path"
                        />
                      }
                      classes={{
                        primary: classes.statusItemText,
                      }}
                    />
                  </ListItem>
                )}
                <ListItem
                  button
                  disableRipple
                  classes={{ selected: classes.statusMenuItemSelected }}
                  // onClick={this.exportToPdf}
                  onClick={() => { this.handleExports('pdf') }}
                // onClick={deleteMember}
                >
                  <ListItemIcon classes={{
                    root: classes.listItemIcon,
                  }}>
                    <SvgIcon style={{ fontSize: "18px" }} viewBox="0 0 80 98.5">
                      <PdfProjectIcon htmlColor={theme.palette.secondary.medDark} />
                    </SvgIcon>
                  </ListItemIcon>
                  <ListItemText
                    primary={"Export To PDF"}
                    classes={{
                      root: classes.textWithIcon,
                      primary: classes.statusItemText,
                    }}
                  />
                </ListItem>
                <ListItem
                  button
                  disableRipple
                  classes={{ selected: classes.statusMenuItemSelected }}
                  // onClick={this.exportToExcel} 
                  onClick={() => { this.handleExports('excel') }}
                // onClick={deleteMember}
                >
                  <ListItemIcon classes={{
                    root: classes.listItemIcon,
                  }}>
                    <SvgIcon style={{ fontSize: "18px" }} viewBox="0 0 226.8 226.8">
                      <ExcelProjectIcon htmlColor={theme.palette.secondary.medDark} />
                    </SvgIcon>
                  </ListItemIcon>
                  <ListItemText
                    primary={"Export To Excel"}
                    classes={{
                      root: classes.textWithIcon,
                      primary: classes.statusItemText,
                    }}
                  />
                </ListItem>
                <ListItem
                  button
                  disableRipple
                  classes={{ selected: classes.statusMenuItemSelected }}
                  // onClick={this.exportToMSProject}
                  onClick={() => { this.handleExports('project') }}
                // onClick={deleteMember}
                >
                  <ListItemIcon classes={{
                    root: classes.listItemIcon,
                  }}>
                    <SvgIcon style={{ fontSize: "18px" }} viewBox="0 0 80 69.8">
                      <MSProjectIcon htmlColor={theme.palette.secondary.medDark} />
                    </SvgIcon>
                  </ListItemIcon>
                  <ListItemText
                    primary={"Export To MS Project"}
                    classes={{
                      root: classes.textWithIcon,
                      primary: classes.statusItemText,
                    }}
                  />
                </ListItem>
              </List>
            }
          />

          {exportFlag && (
            <ExportColumnGantt
              open={exportFlag}
              closeAction={this.handleCloseColumnPopup}
              handelExportFailuer={this.handelExportFailuer}
              exportType={exportType}
              projectId={this.props.projectId}
              projectName={this.props.projectName}
              handleBtnLoader={this.handleBtnLoader}
            />
          )}
        </div>
      </ClickAwayListener>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    profileState: state.profile,
  };
};

export default compose(
  withSnackbar,
  connect(mapStateToProps, {
    exportToPdf,
    exportToExcel,
    exportToMSProject,
    addPendingHandler
  }),
  withStyles(combineStyles(gantStyles, menuStyles), {
    withTheme: true,
  })
)(ViewDropdown);
