const taskDetailStyles = (theme) => ({
  taskDetailsDialog: {
    paddingRight: "0 !important",
  },
  tabInnerContentCnt: {
    padding: 20,
    height: "100%",
  },
  activityCardCnt: {
    padding: "8px 10px",
    background: theme.palette.common.white,
    borderRadius: 4,
    border: `1px solid ${theme.palette.border.lightBorder}`,
  },
  activityMsg: {
    color: theme.palette.text.primary,
    "& b": {
      color: theme.palette.text.primary,
      fontWeight: theme.typography.fontWeightMedium,
    },
  },
  activityDate: {
    textTransform: "uppercase",
    marginBottom: 10,
    display: "flex",
    alignItems: "center",
  },
  dialogCnt: {
    // display: "flex",
    // alignItems: "center",
    // justifyContent: "center"
  },
  dialogPaperCnt: {
    // overflowY: "visible",
    background: theme.palette.common.white,
    maxWidth: "95%",
    minHeight: "90%",
  },
  defaultDialogTitle: {
    padding: "25px",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
  },
  defaultDialogContent: {
    padding: "0",
    overflowY: "visible",
    display: "flex",
    justifyContent: "stretch",
    alignItems: "stretch",
  },
  defaultDialogAction: {
    padding: " 25px 25px",
  },
  assigneeListCnt: {
    padding: "0 20px",
    margin: "0 10px 0 0",
    borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
    borderRight: `1px solid ${theme.palette.border.lightBorder}`,
  },
  mainTaskDetailsCnt: {
    marginBottom: 20,
  },

  taskDetailsLeftCnt: {
    borderRight: `1px solid ${theme.palette.border.lightBorder}`,
    padding: 30,
    height: "100%",
    position: "relative",
    // overflowY: "auto"
  },
  dropdownsLabel: {
    transform: "translate(6px, -7px) scale(1)",
    display: "block",
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular,
  },
  btnDropdownsLabel: {
    transform: "translate(0, -7px) scale(1)",
    display: "block",
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular,
  },
  taskDetailsFieldCnt: {},
  descriptionCnt: {
    height: 120,
    padding: 10,
    resize: "vertical",
    borderRadius: "4px",
    overflowY: "auto",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    fontSize: "12px !important",
    "& *": {
      margin: 0,
    },
  },
  tabLabelCnt: {
    padding: 0,
    fontSize: "12px !important",
    textTransform: "capitalize",
    fontWeight: theme.typography.fontWeightRegular,
  },
  TabsRoot: {
    background: theme.palette.background.paper,
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
  },
  tab: {
    minWidth: "unset",
  },
  TabContentCnt: {
    background: theme.palette.background.light,
    height: "calc(100% - 49px)",
    position: "relative",
  },
  tabContentGrid: {
    height: "100%",
  },
  tabContentFormCnt: {
    background: theme.palette.common.white,
    padding: 20,
    borderRadius: 4,
  },
  tabIndicator: {
    background: theme.palette.primary.light,
  },
  outlinedInputCnt: {
    marginRight: 15,
  },
  outlinedTimeInputCnt: {
    margin: 0,
    background: theme.palette.common.white,
    borderRadius: 4,
  },
  outlinedMinInputCnt: {
    "&:hover $notchedOutlineMinCnt": {
      border: `1px solid ${theme.palette.border.lightBorder} !important`,
    },
  },
  outlinedHoursInputCnt: {
    "&:hover $notchedOutlineHoursCnt": {
      border: `1px solid ${theme.palette.border.lightBorder} !important`,
      borderRight: "none !important",
    },
  },
  outlineInputFocus: {
    "& $notchedOutlineCnt": {
      border: `1px solid ${theme.palette.border.lightBorder}`,
    },
    "& $notchedOutlineMinCnt": {
      border: `1px solid ${theme.palette.border.lightBorder}`,
    },
    "& $notchedOutlineHoursCnt": {
      border: `1px solid ${theme.palette.border.lightBorder}`,
      borderRight: 0,
    },
    "& $notchedOutlineAMCnt": {
      border: `1px solid ${theme.palette.border.lightBorder}`,
    },
  },
  outlinedInput: {
    padding: "13.5px 14px",
    "&:hover": {
      cursor: "auto",
    },
  },
  outlinedDateInput: {
    padding: "13.5px 14px",
    background: theme.palette.background.default,
    borderRadius: 4,
  },
  outlinedTaskInput: {
    padding: "10px 14px",
    background: theme.palette.background.default,
    borderRadius: 4,
    fontSize: "22px !important",
    fontWeight: theme.typography.fontWeightRegular,
  },
  outlinedInputDisabled: {
    background: "transparent",
  },
  outlinedTimeInput: {
    padding: 14,
    textAlign: "center",
  },
  outlinedHoursInput: {
    background: theme.palette.background.default,
    textAlign: "center",
    borderRadius: "4px 0 0 4px",
    padding: 14,
  },
  outlinedMinInput: {
    padding: 14,
    background: theme.palette.background.default,
    textAlign: "center",
  },
  outlinedAmInput: {
    padding: 14,
    borderRadius: "0 4px 4px 0",
    borderLeft: 0,
    textAlign: "center",
    "&:hover": {
      cursor: "pointer",
    },
  },
  notchedOutlineCnt: {
    borderRadius: 4,
    border: `1px solid ${theme.palette.border.lightBorder}`,
  },
  notchedOutlineHoursCnt: {
    borderRadius: "4px 0 0 4px",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRight: 0,
  },
  notchedOutlineMinCnt: {
    borderRadius: 0,
    border: `1px solid ${theme.palette.border.lightBorder}`,
  },
  notchedOutlineAMCnt: {
    borderRadius: "0 4px 4px 0",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderLeft: 0,
  },
  addTimeBtnCnt: {
    display: "flex",
    background: theme.palette.background.medium,
    padding: "5px 5px",
    cursor: "pointer",
    borderRadius: 20,
  },
  addTimeActionBtnCnt: {
    textAlign: "right",
    marginTop: 10,
  },
  addTimeBtnText: {
    color: theme.palette.text.primary,
    fontSize: "16px !important",
    lineHeight: "20px",
    marginLeft: 5,
    textDecoration: "underline",
  },
  checklistInputCnt: {
    marginTop: 25,
  },
  checkListItemSpacer: {
    marginLeft: 28,
  },
  clInputProgressCnt: {
    display: "flex",
    alignItems: "center",
    marginBottom: 10,
    padding: 14,
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
  },
  clProgressValue: {
    position: "absolute",
    right: 0,
    left: 0,
    textAlign: "center",
    top: 20,
    fontSize: "15px !important",
  },
  checklistCnt: {
    height: "93%",
  },
  checkList: {
    listStyleType: "none",
    padding: 0,
    paddingBottom: 40,
  },
  checkListItem: {
    fontSize: "17px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightLight,
    top: "2px !important",
    left: "2px !important",
    position: "relative !important",
    marginBottom: 5,
    paddingRight: 23,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    "&:hover $CloseIconBtn": {
      visibility: "visible",
    },
    "&:hover $dragHandleCnt": {
      visibility: "visible",
    },
    "&:hover $emptyCheckbox": {
      display: "none",
    },
    "&:hover $unCheckedIcon": {
      display: "block",
    },
  },
  dragHandleCnt: {
    visibility: "hidden",
  },
  dragHandle: {
    fontSize: "28px !important",
  },
  checkedIcon: {
    fontSize: "20px !important",
  },
  emptyCheckbox: {
    width: 20,
    height: 20,
    background: theme.palette.background.paper,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "100%",
  },
  unCheckedIcon: {
    fontSize: "20px !important",
    display: "none",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "100%",
  },
  checkListItemControls: {
    display: "flex",
    padding: 14,
  },
  markAllCheck: {
    marginLeft: "auto",
    display: "flex",
    cursor: "pointer",
    "& $markAllCheckIcon": {
      color: theme.palette.status.completed,
      fontSize: "18px !important",
    },
    "& $markAllCheckText": {
      color: theme.palette.text.primary,
      marginLeft: 2,
    },
  },
  markAllCheckDisabled: {
    marginLeft: "auto",
    display: "flex",
    cursor: "default",
    "& $markAllCheckIcon": {
      color: theme.palette.text.light,
      fontSize: "18px !important",
    },
    "& $markAllCheckText": {
      color: theme.palette.text.light,
      marginLeft: 2,
    },
  },
  markAllCheckIcon: {},
  markAllCheckText: {},
  checklistItemDetails: {},
  checkListItemInner: {
    display: "flex",
    alignItems: "center",
    width: "90%",
    "& span": {
      fontSize: "12px !important",
      color: theme.palette.text.primary,
      lineHeight: "normal",
    },
  },
  checklistCheckboxCnt: {
    margin: 0,
  },
  checkListCheckBox: {
    padding: 6,
  },
  CloseIconBtn: {
    visibility: "hidden",
  },
  deleteItemIcon: {
    fontSize: "18px !important",
  },
  checkboxIcon: {},
  datePickerPopper: {
    zIndex: 2,
  },

  menuTab: {
    background: theme.palette.background.light,
    borderRadius: "10px 10px 0 0",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderBottom: "none",
    opacity: 1,
    minWidth: 141,
  },
  menuTabSelected: {
    background: theme.palette.common.white,
    borderRadius: "10px 10px 0 0",
    border: `1px solid ${theme.palette.border.lightBorder}`,
  },
  menuTabIndicator: {
    background: "transparent",
    height: 1,
  },
  menuTimeCnt: {
    background: theme.palette.background.light,
    marginTop: 48,
    marginBottom: 4,
    border: "1px solid rgba(221,221,221,1)",
    borderRadius: "0 12px 12px 0",
    borderLeft: 0,
    width: 250,
  },
  dateTimeHeading: {
    background: theme.palette.background.default,
    padding: "10px 20px",
    margin: 0,
    textTransform: "uppercase",
    fontSize: "0.944rem !important",
    fontWeight: theme.typography.fontWeightRegular,
    borderRadius: "0 12px 0 0",
    color: theme.palette.text.secondary,
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
  },
  menuDateTimeInnerCnt: {
    padding: "15px 20px",
  },
  timeFieldsCnt: {
    padding: "0 20px",
  },
  otherCommentAvatar: {
    marginRight: 20,
  },
  selfCommentAvatar: {
    marginLeft: 20,
  },
  commentContentCnt: {
    borderRadius: 4,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    padding: "10px 15px",
    fontSize: "13px !important",
    minWidth: 200,
    background: theme.palette.background.default,
    wordBreak: "break-all",
    "& p": {
      margin: 0,
      padding: 0,
      lineHeight: 1.5,
       wordBreak: "break-word"
    },
    "& ol": {
      padding: 0,
      margin: 0,
    },
    "& ul": {
      padding: 0,
      margin: 0,
    },
    "& li": {
      padding: 0,
      margin: "0 0 10px 15px",
    },
    "& li:last-child": {
      margin: "0 0 0 15px",
    },
    "& strong": {
      fontWeight: theme.palette.fontWeightMedium,
    },
  },
  commentAttachContentCnt: {
    borderRadius: 10,
    fontSize: "13px !important",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    padding: "10px 15px",
    background: theme.palette.background.default,
    "& p": {
      margin: 0,
      padding: 0,
    },
    "& p+$attachmentCnt": {
      marginTop: 10,
    },
  },
  commentBoxMention: {
    color: theme.palette.primary.light,
  },
  otherTimeStamp: {
    fontSize: "11px !important",
    marginLeft: 12,
    color: theme.palette.text.secondary,
    textAlign: "left",
    margin: 0,
    marginTop: 3,
  },
  selfTimeStamp: {
    fontSize: "11px !important",
    marginRight: 12,
    color: theme.palette.text.secondary,
    textAlign: "right",
    margin: 0,
    marginTop: 3,
  },
  attachmentCnt: {
    background: theme.palette.background.light,
    borderRadius: 5,
    marginTop: 10,
    padding: "10px 15px 10px 15px",
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
  },
  commentsAttachmentIcon: {
    marginRight: 10,
  },
  commentsFileName: {
    fontSize: "12px !important",
    paddingRight: "20px !important",
    color: theme.palette.text.primary,
    marginBottom: "3px !important",
  },
  commentsFileSize: {
    fontSize: "10px !important",
    color: theme.palette.text.secondary,
  },
  attachmentDownloadIcon: {
    marginRight: 10,
    background: theme.palette.secondary.medDark,
    borderRadius: "100%",
    fontSize: "20px !important",
  },
  attachmentDeleteIcon: {
    fontSize: "24px !important",
  },
  // Notification Menu Styles
  NotifAvatar: {
    borderRadius: "100%",
    width: 56,
    height: 56,
  },
  notifMenuItem: {
    height: "auto",
    padding: "15px 15px",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
  },
  notifMenuHeadingItem: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    cursor: "unset",
    "&:hover": {
      background: "transparent",
    },
  },
  menuHeaderReadAllText: {
    color: theme.palette.text.primary,
    cursor: "pointer",
  },
  menuHeadingText: {
    textTransform: "uppercase",
  },
  notifMenuItemText: {
    padding: 0,
    "& p": {
      fontSize: "12px !important",
      fontWeight: theme.typography.fontWeightLight,
      color: theme.palette.text.secondary,
      whiteSpace: "normal",
      margin: 0,
      "& b": {
        fontWeight: theme.typography.fontWeightMedium,
        color: theme.palette.text.primary,
      },
    },
    "& $notifMenuDate": {
      fontSize: "10px !important",
    },
  },
  notifMenuDate: {},
  commentsEditor: {
    // position: "absolute",
    // bottom: 12,
    // left: 17,
    // right: 17
  },
  issueTileCnt: {
    background: theme.palette.common.white,
    borderRadius: 4,
    marginBottom: 20,
  },
  issueTileContentCnt: {
    padding: "10px 15px 20px 15px",
  },
  issueTileFooter: {
    padding: 10,
    borderTop: `1px solid ${theme.palette.border.lightBorder}`,
  },
  footerLabel: {
    marginRight: 5,
  },
  footerIcon: {
    fontSize: "13px !important",
    marginRight: 3,
  },
  viewMoreCnt: {
    background: theme.palette.common.white,
    borderRadius: 4,
    textAlign: "center",
    padding: 5,
    cursor: "pointer",
  },
  statusText: {
    fontSize: "11px !important",
    fontWeight: theme.typography.fontWeightLight,
    padding: "5px 10px",
    color: theme.palette.common.white,
    textTransform: "uppercase",
    borderRadius: 4,
    letterSpacing: "1px",
  },
  meetingDateCnt: {
    background: theme.palette.background.items,
    borderRadius: 4,
    padding: 10,
    marginRight: 20,
  },
  meetingDate: {
    fontSize: "18px !important",
    textTransform: "uppercase",
    fontWeight: theme.typography.fontWeightRegular,
  },
  meetingMonth: {
    fontSize: "12px !important",
    color: theme.palette.text.light,
  },
  meetingTileCnt: {
    background: theme.palette.common.white,
    borderRadius: 4,
    marginBottom: 20,
  },
  meetingTileContentCnt: {
    padding: "10px 20px",
  },
  cancelText: {
    color: theme.palette.error.dark,
    fontSize: "12px !important",
  },
  commentsTabCnt: {
    display: "flex",
    flexDirection: "column",
  },
  commentsCnt: {
    flex: 1,
  },
  meetingBorderedIcon: {
    padding: 6,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "100%",
    width: 32,
    height: 32,
  },
  recurrenceIcon: {
    fontSize: "18px !important",
  },
  reminderIcon: {
    fontSize: "18px !important",
  },
  count: {
    minWidth: 22,
    height: 22,
    background: theme.palette.common.white,
    borderRadius: 4,
    display: "inline-block",
    lineHeight: "normal",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    boxSizing: "border-box",
    padding: 4,
    fontSize: "10px !important",
    color: theme.palette.text.secondary,
  },
  statusDropdownCnt: {
    padding: "0 20px",
    margin: "0 0 0 10px",
    borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
  },
  editor: {
    height: 120,
    padding: 20,
    fontSize: "12px !important",
    overflowY: "auto",
    borderRadius: 4,
  },
  createdBy: {
    padding: "5px 20px",
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    background: theme.palette.background.items,
    borderRadius: "0 0 4px 4px",
  },
  loadingListItemCnt: {
    height: 40,
    display: "flex",
    alignItems: "center",
    padding: "0 27px",
  },
  loadingListItemCheckbox: {
    height: 20,
    width: 20,
    background: "#fbfbfb",
    borderRadius: "50%",
  },
  loadingListItemTitleCnt: {
    marginLeft: 11,
    width: 267,
  },
  loadingListItemTitleTop: {
    height: 12,
    background: "#fbfbfb",
    marginBottom: 5,
  },
  loadingListItemTitleBottom: {
    height: 8,
    background: "#fbfbfb",
    width: "54%",
  },
});

export default taskDetailStyles;
