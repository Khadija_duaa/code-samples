import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import DoneIcon from "@material-ui/icons/Done";
import { UpdateTask, CreateCheckList, deleteCheckList,updateChecklist, updateTaskData } from "../../../../redux/actions/tasks";
import { connect } from "react-redux";
import { Circle } from "rc-progress";
import cloneDeep from "lodash/cloneDeep";
import debounce from "lodash/debounce";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Grid from "@material-ui/core/Grid";
import CloseIcon from "@material-ui/icons/Close";
import IconButton from "../../../../components/Buttons/IconButton";
import { Scrollbars } from "react-custom-scrollbars";
import Typography from "@material-ui/core/Typography";
import DefaultTextField from "../../../../components/Form/TextField";
import helper from "../../../../helper";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import DragIndicator from "@material-ui/icons/DragIndicator";
import SvgIcon from "@material-ui/core/SvgIcon";
import CheckBoxIcon from "../../../../components/Icons/CheckBoxIcon";
import { validateTitleField } from "../../../../utils/formValidations";
import { generateUsername } from "../../../../utils/common";
import styles from "./styles";
import { updateGanttTask } from "../../../../redux/actions/gantt";

const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

class CheckList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checkListInput: "",
      checkListInputError: false,
      checkListInputErrorMessage: "",
      checkList: [],
      checkListMouseOver: false
    };
    this.onDragEnd = this.onDragEnd.bind(this);
    this.handleCheckListMouseOver = this.handleCheckListMouseOver.bind(this);
  }

  componentDidMount() {
    this.populateChecklistData(this.props.task);
  }

  componentDidUpdate(prevProps, prevState) {
    const taskData = this.props.task;
    const prevTaskData = prevProps.task;

    if (taskData && JSON.stringify(taskData) !== JSON.stringify(prevTaskData)) {
      this.setState(this.populateChecklistData(taskData));
    }
  }

  populateChecklistData = task => {
    let members = this.props.members;
    let checkList = task
      ? task.checkList.map((x, i) => {
        let username = ``;
        if (x.userId) {
          let member = members.find(m => x.userId === m.userId);
          if (member) {
            const { userName, email, fullName } = member;
            username = generateUsername(email, userName, fullName);
          }
        }
        return {
          id: x.checkListId,
          value: x.description,
          description: x.description,
          isDone: x.isDone,
          checkedDate: x.checkedDate,
          userId: x.userId || this.props.userId,
          userName: username
        };
      }) || []
      : [];
    this.setState({ checkList });
  };

  updateTaskCheckList = debounce((checkList, updateStatus, isItemChecked) => {
    const currentTask = cloneDeep(this.props.task);
    const obj = {
      checkList : checkList
    }
    // currentTask.checkList = checkList;
    if (updateStatus) {
      if (isItemChecked)
        obj.status = currentTask.status === 0 ? 1 : currentTask.status;
      else
        obj.status = currentTask.status === 3 ? 1 : currentTask.status;
    }
      this.props.updateTaskData(
        { task: currentTask, obj },
        response => {
          this.props.updateGanttTask({
            id: response.data.taskId,
            text: response.data.taskTitle,
            progress: response.data.progress,
          });
        },
        err => {
          this.populateChecklistData(this.props.task);
        }
      );
  }, 800);

  onDragEnd(result) {
    // dropped outside the list
    const { permission, task } = this.props;
    const canEditCheckListItem = true;
    if (!result.destination || !canEditCheckListItem) {
      return;
    }

    const items = reorder(
      this.state.checkList,
      result.source.index,
      result.destination.index
    );
    this.setState({ checkList: items }, () => {
      this.updateTaskCheckList(this.state.checkList);
    });
  }

  handleDeleteItem(event, id) {
    event.stopPropagation();
    let newCheckList = this.state.checkList.filter(item => item.id !== id);
    this.setState({ checkList: newCheckList }, () => {
      this.updateTaskCheckList(newCheckList);
    });
  }

  handleCheckListMouseOver() {
    this.setState({ checkListMouseOver: true });
  }

  addNewItem = event => {
    if (event.key === "Enter") {
      let validationObj = validateTitleField(
        "Checklist",
        this.state.checkListInput,
        true,
        250
      );
      if (validationObj["isError"]) {
        this.setState({
          checkListInputError: validationObj["isError"],
          checkListInputErrorMessage: validationObj["message"]
        });
        return;
      }
      let newItem = {
        id: this.state.checkList.length,
        value: this.state.checkListInput.trim(),
        description: this.state.checkListInput.trim(),
        isDone: false,
        checkedDate: new Date().toISOString(),
        userId: this.props.userId
      };
      // copying state data
      let checkList = this.state.checkList.concat([]);
      // checkList.push(newItem);

      let addCheckListModel = {
        taskId: this.props.task.taskId,
        checklist: {
          Description: this.state.checkListInput.trim()
        }
      };

      this.setState(
        {
          checkListInput: "",
          checkListInputError: false,
          checkListInputErrorMessage: "",
          checkList
        },
        () => {
          // this.updateTaskCheckList(checkList, true);
          this.createCheckList(addCheckListModel);

        }
      );
    }
  };

  createCheckList = obj => {
    this.props.CreateCheckList(obj, response => {
      if (response && response.status === 200) {
        //
      }
    });
  };

  deleteCheckList = (event, id) => {
    event.stopPropagation();
    let deleteCheckListModel = {
      TaskId: this.props.task.taskId,
      CheckList: {
        CheckListId: id
      }
    };
    this.props.deleteCheckList(deleteCheckListModel, response => {
      if (response && response.status === 200) {
        let newCheckList = this.state.checkList.filter(item => item.id !== id);
        this.setState({ checkList: newCheckList });
      }
    });
  };

  handleItemNameInput = event => {
    this.setState(
      {
        checkListInput: event.target.value,
        checkListInputError: false,
        checkListInputErrorMessage: ""
      },
      () => {
        if (this.props.changeStatus && this.props.clearStatusChanged) {
          this.props.clearStatusChanged();
        }
      }
    );
  };

  // handleItemCheck = (id, event) => {
  //   const currentTask = cloneDeep(this.props.task);
  //   const permission = this.props.permission;
  //   const canEditCheckListItem = true;
  //   const condition = !canEditCheckListItem;
  //   if (condition) {
  //     return;
  //   }

  //   let checkList = this.state.checkList.map(item => {
  //     if (id === item.id) {
  //       // currentTask.status = currentTask.status === 0 ? 1 : currentTask.status;
  //       return {
  //         ...item,
  //         checkedDate: new Date().toISOString(),
  //         description: item.description,
  //         isDone: !item.isDone,
  //         userId: this.props.userId
  //       };
  //     }
  //     return item;
  //   });
  //   let formatedList = checkList.map(item => {
  //     return {
  //       checkedDate: item.checkedDate,
  //       description: item.description,
  //       isDone: item.isDone,
  //       userId: item.userId
  //     };
  //   });

  //   const isItemChecked = event.target ? event.target.checked : false;
  //   this.setState({ checkList }, () => {
  //     this.updateTaskCheckList(formatedList, isItemChecked, isItemChecked);
  //   });
  // };

  handleItemCheck = (list, event) => {
    event.stopPropagation();
    const { checkList, taskId } = this.props.task;
    const currentTask = cloneDeep(this.props.task);

    const selectedCheckList = checkList.find(c => { /** extracting single object of checklist from checklist array */
      return c.checkListId == list.id;
    });
    selectedCheckList.isDone = list.isDone ? false : true; /** switching bool isDone for selected checklist  */

    let updatedCheckList = checkList.map(c => { /** updating checklist array  */
      if (c.checkListId == list.id) {
        return selectedCheckList;
      }
      else { return c }
    });

    currentTask.checkList = updatedCheckList; /** assiging updated checklist array to current task object  */
    this.setState(this.populateChecklistData(currentTask)); /** calling function for updating checklist in state  */

    let obj = { /** creating obj for api */
      taskId: taskId,
      checklist: selectedCheckList
    };
    this.props.updateChecklist(
      obj,
      succ => { },
      err => { }
    );
  };

  handleMarkAllClick = () => {
    this.props.markAllAsChecked();
  };

  render() {
    const { classes, theme, task } = this.props;
    const {
      checkListInput,
      checkList,
      checkListInputError,
      checkListInputErrorMessage
    } = this.state;

    let progress = task ? task.progress : 0;

    const overdue = helper.RETURN_OVER_DUE_DAYS_WITH_PROGRESS(
      task.actualDueDateString,
      task.progress
    );

    let completed = 0, remaining = 0;

    checkList.forEach((list, index) => {
      list.isDone ? completed++ : remaining++;
    });

    return (
      <Fragment>
        <div className={classes.clInputProgressCnt}>
          <DefaultTextField
            label="Checklist"
            fullWidth={true}
            formControlStyles={{ marginBottom: 0, marginRight: 20 }}
            errorState={checkListInputError}
            errorMessage={checkListInputErrorMessage}
            defaultProps={{
              id: "firstNameInput",
              onChange: this.handleItemNameInput,
              onKeyUp: this.addNewItem,
              value: checkListInput,
              placeholder: "Add a checklist item"
              // disabled: isArchivedSelected
            }}
          />
          <div style={{ width: 74, position: "relative" }}>
            <Circle
              percent={progress}
              strokeWidth="7"
              trailWidth=""
              trailColor="#dedede"
              strokeColor={overdue ? "#de133e" : "#30d56e"}
            />
            <span className={classes.clProgressValue}>{progress}%</span>
          </div>
        </div>

        <div className={classes.checkListItemControls}>
          <Typography variant="body2">
            {" "}
            {`${completed} Completed - ${remaining} Remaining`}{" "}
          </Typography>
          <div
            className={
              remaining ? classes.markAllCheck : classes.markAllCheckDisabled
            }
            onClick={() => {
              remaining ? this.handleMarkAllClick() : null;
            }}
          >
            <DoneIcon className={classes.markAllCheckIcon} />
            <Typography
              classes={{
                body2: classes.markAllCheckText
              }}
              variant="body2"
            >
              Mark all as complete
            </Typography>
          </div>
        </div>

        <Grid xs={12} item classes={{ item: classes.checklistCnt }}>
          <DragDropContext onDragEnd={this.onDragEnd}>
            <Droppable droppableId="droppable">
              {(provided, snapshot) => (
                <Scrollbars style={{ height: "inherit" }}>
                  <ul className={classes.checkList} ref={provided.innerRef}>
                    {checkList.map((list, index) => (
                      <Draggable
                        key={list.id}
                        draggableId={list.id}
                        index={index}
                      >
                        {(provided, snapshot) => (
                          <li
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            className={classes.checkListItem}
                            onClick={e => this.handleItemCheck(list, e)}
                          >
                            <Grid
                              container
                              direction="row"
                              justify="flex-start"
                              alignItems="center"
                            >
                              {true ? (
                                <span
                                  {...provided.dragHandleProps}
                                  className={classes.dragHandleCnt}
                                >
                                  <DragIndicator
                                    htmlColor={theme.palette.secondary.light}
                                    className={classes.dragHandle}
                                  />
                                </span>
                              ) : (
                                  <span
                                    {...provided.dragHandleProps}
                                    className={classes.checkListItemSpacer}
                                  ></span>
                                )}
                              <div className={classes.checkListItemInner}>
                                <FormControlLabel
                                  classes={{
                                    root: classes.checklistCheckboxCnt
                                  }}
                                  control={
                                    <Checkbox
                                      checked={list.isDone}
                                      style={{ padding: "10px 10px 10px 0" }}
                                      onClick={e =>
                                        this.handleItemCheck(list, e)
                                      }
                                      value={list.value}
                                      checkedIcon={
                                        <SvgIcon
                                          viewBox="0 0 426.667 426.667"
                                          htmlColor={
                                            theme.palette.status.completed
                                          }
                                          classes={{
                                            root: classes.checkedIcon
                                          }}
                                        >
                                          <CheckBoxIcon />
                                        </SvgIcon>
                                      }
                                      icon={
                                        <Fragment>
                                          <SvgIcon
                                            viewBox="0 0 426.667 426.667"
                                            htmlColor={
                                              theme.palette.background.items
                                            }
                                            classes={{
                                              root: classes.unCheckedIcon
                                            }}
                                          >
                                            <CheckBoxIcon />
                                          </SvgIcon>
                                          <span
                                            className={classes.emptyCheckbox}
                                          />
                                        </Fragment>
                                      }
                                      color="primary"
                                    />
                                  }
                                />
                                <div>
                                  <span
                                    style={
                                      list.isDone
                                        ? { textDecoration: "line-through" }
                                        : null
                                    }
                                  >
                                    {list.value}
                                  </span>
                                  {list.isDone ? (
                                    <Typography
                                      classes={{
                                        body2: classes.checklistItemDetails
                                      }}
                                      variant="body2"
                                    >
                                      {list.userName} -{" "}
                                      {list.checkedDate
                                        ? helper.RETURN_CUSTOMDATEFORMATFORCHECKLIST(
                                          list.checkedDate
                                        )
                                        : helper.RETURN_CUSTOMDATEFORMATFORCHECKLIST(
                                          new Date().toISOString()
                                        )}
                                    </Typography>
                                  ) : null}
                                </div>
                              </div>
                            </Grid>
                            {true ? (
                              <div className={classes.CloseIconBtn}>
                                <IconButton
                                  id={list.id}
                                  btnType="smallFilledGray"
                                  onClick={event =>
                                    this.deleteCheckList(event, list.id)
                                  }
                                >
                                  <CloseIcon
                                    className={classes.deleteItemIcon}
                                    htmlColor={theme.palette.common.white}
                                  />
                                </IconButton>
                              </div>
                            ) : null}
                          </li>
                        )}
                      </Draggable>
                    ))}
                    {provided.placeholder}
                  </ul>
                </Scrollbars>
              )}
            </Droppable>
          </DragDropContext>
        </Grid>
        {/* </Hotkeys> */}
      </Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    userId: state.profile.data.userId,
    tasks: state.tasks.data || [],
    members: state.profile.data.member.allMembers || [],
    permissions: state.workspacePermissions.data.task
  };
};

export default compose(
  withRouter,
  withStyles(styles, {
    withTheme: true
  }),
  connect(
    mapStateToProps,
    {
      UpdateTask,
      updateGanttTask,
      CreateCheckList,
      deleteCheckList,
      updateChecklist,
      updateTaskData
    }
  )
)(CheckList);
