import moment from "moment";
let attachmentIcon =
  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAPCAYAAADUFP50AAABRElEQVQokYXSP0hWcRTG8Y8htEhzKE6FLjq4CQ6FgkH+AxUhQnEQNFykUWmRkBZRJwcXJx3CwAYdBOEFlXATGlrEVSEJXjAwpOQX5+rlvlpnufA753uec55zq0qlkv/EQzxDDQ5wmsof/JvRgm+YxjC+4k1KVFeU3kYbNjCOzXh9gi/Yuw9sxzpeYTf3fhzNXtw1ahfW0F+AsniMchEcwAq6cViBMIRWfMyDafmlUHyLmQKU8gtpTPzIdnyNWbyM72/M5aCJcLYjXP7rai0W8Rzz+I5RXAWU1CfjlidZpwT2YDucvMRIKKZ4F9N04iI/d9rxEc7wFPs56AMGo/Fq3PMmkuJRFKU9Psev1Yy6cDndMzV8X1Tcwc8YMTn2C1sYw6fIT+FPEUwPfagPxQb0xvGXw82KyM5xHnAjmmKCZEr5LgiuAS9lQ7r+p4kWAAAAAElFTkSuQmCC";
let issueIcon =
  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAANCAYAAACdKY9CAAAAwklEQVQokZWSuwrCMBhGT0WLWhWdXBUUik7OvpNv4eLm6vP4AA7FdhD0DcQrHbzx2RSEEKgHQkL4Tv7cvDiOZ8AEuAAeGY+fcU4D2JSBJX9QMisX5aQKNROeAjuHOATWQCDh9SMMrGhG1/RPCSngAwsrZnOXEJjpfhiGBysCJEnSA/ZAS9d6BpqmUmqlM6qmfQ+t7YgVcHQIHUDv5UtQy4WtQxgZoaLwTXsDogKHvurhFC5KWxXmwNhU0v95O/o6EH0Aej0ntFrelewAAAAASUVORK5CYII=";
let riskIcon =
  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAMCAYAAABSgIzaAAAA0klEQVQokX3SMUoEQRCF4U/XBTHwAoIoJmKwoeEGBsZ6FMHA1MVY1wPoVcREMDVwzIzMxUAEE6mhlm16xnlQ0F39v3pQMytN0+jRBq7wi0t818ha19NqhrM8j3BeA32J+3jBV943McFbCa12srjFGEdZ4+wZMp7iOM+fWbJ38p8xFjLv5C91k0zHeIHtauiouO8k02qxnF28Yr0AH+Md06L3gwO8LxKvK1PoIatUMMG2iTExptc6zMTnnrdp/ADxjfr0NLCoSRjvsIetAbDUB+7/AF4VJSMkKuKXAAAAAElFTkSuQmCC";
let commentIcon =
  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAABTklEQVQokYXSu0peURCG4cdTIygpbIKx9AKUNBZBMSCIVSBYJqKdkguIYG0jKIKdXkJA7AJp/sJKcrBQQ2LERhASDHhqRAyjs3Xn36gfbPZirXnXfDOzGmq1mjo1oRfP0Y5jfM7vshzaWAe+wXdM4wn+5v89tvL8Vs25aMByZhrAQb0ddGIBLzEeLorM03nB6D2g3B9N6+HkOnPYeoseXFWQ/xXn77CJpYCHsYazDFtBWwXjBBMZtxpcwF34WQqaQ2sF5by0/oVnAf/G09LBTjauqbR3mSMr1IHDgNfxAbNZU1j+iJZS8AWG0no09hVeB/wD37IRixnQVzF9p0lsY6+Y8xQ+pdX5SviNImMkiKbFW7h9JFHPIPYTjvF14yjrCydj+IIXRf0FHDrN5sXMZ3KW0bg/+Bo1YrdspQyHYkQj6H/gpd0Lx+0bj0HXwj9clETyPXukxQAAAABJRU5ErkJggg==";
let meetingIcon =
  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAOCAYAAAAfSC3RAAABF0lEQVQokXXSvS5EURTF8Z8bpQQFBd20IjodxUhM1N5AQiehpyY60UwhQqNQewQfBdUI8wgS8ZWYKJEd+8q4mVnJyd3Z/7XPWTnnDrTbbakZNHCEZ/81hhWc4z7IYOIBLGEHV7hDkewLs9hN3wO+A9ZxWDmhhddcrQoL70KRuw3jLY2driRlqpdkT+mtl3EaCRZzTXQNRj2X/U56i3LnIZzpr70q6Y60j8e+o7+axLqumwtt4BY1nCQrsq4l2y7NRWXHaaxiBMu5ol5L1jNq6ADHeMd89j4xio+8i54nbuazTOESF1lHL1jPE6O+QTPf6zT7UUcvWPw5f+aIErrOiKFyKBTvF9rKyKHnGIxfKL7jldj9dIrmD+7bOl5255OsAAAAAElFTkSuQmCC";

export function ToolTipCmp(start, end, task, props){
    const statusColor = props.theme.palette;
      let priorityData = [
        { name: "Critical", color: statusColor.taskPriority.Critical },
        { name: "High", color: statusColor.taskPriority.High },
        { name: "Medium", color: statusColor.taskPriority.Medium },
        { name: "Low", color: statusColor.taskPriority.Low }
      ];
      // let statusData = [
      //   { name: "Not Started", color: statusColor.taskStatus.NotStarted },
      //   { name: "In Progress", color: statusColor.taskStatus.InProgress },
      //   { name: "In Review", color: statusColor.taskStatus.InReview },
      //   { name: "Completed", color: statusColor.taskStatus.Completed },
      //   { name: "Cancelled", color: statusColor.taskStatus.Cancelled }
      // ];
      let selectedPriority = priorityData.filter(item => {
        return item.name == task.priority;
      });
      let selectedStatus = task.taskTemplate.statusList.find(item => {
        return item.statusId == task.statusId;
      });
      let dates = props.plannedVsActual == "Actual" ? 
      `${task.actual_start ? moment(task.actual_start).format("MMM, DD, YYYY") : ""} 
      - ${task.actual_end ? moment(task.actual_end).format("MMM, DD, YYYY") : ""}` : 
      // moment(task.actual_start).clone().add(task.duration - 1, "days").format("MMM, DD, YYYY") 
      `${task.planned_start ? moment(task.planned_start).format("MMM, DD, YYYY") : ""} 
      - ${task.planned_end ? moment(task.planned_end).format("MMM, DD, YYYY") : ""}`
      // moment(task.planned_start).clone().add(task.duration - 1, "days").format("MMM, DD, YYYY") 
      if (task.type == "milestone") return false;
      else {
        return `<div class="taskCard"><p class="taskCardHeading">${
          task.text
        }</p>
        <div class="progressCnt">
        <div class="progress_bar_before"></div>
        <div class="progress_bar_after" style="width:${task.progress * 100}%"></div>
        </div>
        <div class="flex_center_space_between_row taskStatusDateCnt">
        <div class="statusCircleCnt">
        <div class="statusCircle">
        <svg xmlns="http://www.w3.org/2000/svg" width="10" height="18.75" viewBox="0 0 24 32.75">

        <path id="Icon_Flag" style="fill: ${
          selectedPriority[0].color
        }; fill-rule: evenodd;" data-name="Icon Flag" class="cls-1" d="M625.679,200.959l-22.589-10.49V189.18a0.546,0.546,0,0,0-1.091,0v31.639a0.546,0.546,0,1,0,1.091,0v-9.993l22.559-8.865A0.545,0.545,0,0,0,625.679,200.959Z" transform="translate(-602 -188.625)"/>
        </svg>
        </div>
        <div class="statusCircle">
        <span class="statusIcon" style="background: ${
          selectedStatus ? selectedStatus.statusColor : ""
        }"></span>
        </div> 
        </div>
        <div><span class="taskDateTitle">Start/End Date</span><span class="taskDate">${dates}</span></div>

        </div>
        <div class="flex_center_space_between_row">
        <span class="taskDetailExtraIcons"><img src="${meetingIcon}" alt="attachmentIcon"/>${
          task.meetings
        }</span>
        <span class="taskDetailExtraIcons"><img src="${issueIcon}" alt="attachmentIcon"/>${
          task.issues
        }</span>
        <span class="taskDetailExtraIcons"><img src="${riskIcon}" alt="attachmentIcon"/>${
          task.risks
        }</span>
        <span class="taskDetailExtraIcons"><img src="${attachmentIcon}" alt="attachmentIcon"/>${
          task.attachments
        }</span>
        
        </div>
        `;
      }
}