export function SlackTime(gantt){
//Adding Layer for Slack time
gantt.addTaskLayer(function addSlack(task) {
    if (!gantt.config.show_slack) {
        return null;
    }

    var slack = gantt.getFreeSlack(task);

    if (!slack) {
        return null;
    }

    var state = gantt.getState().drag_mode;

    if (state == 'resize' || state == 'move') {
        return null;
    }

    var slackStart = new Date(task.end_date);
    var slackEnd = gantt.calculateEndDate(slackStart, slack);
    var sizes = gantt.getTaskPosition(task, slackStart, slackEnd);
    var el = document.createElement('div');

    el.className = 'slack';
    el.style.left = sizes.left + 'px';
    el.style.top = sizes.top + 2 + 21 + 'px';
    el.style.width = sizes.width + 'px';
    el.style.height = sizes.height + 'px';

    return el;
});
}