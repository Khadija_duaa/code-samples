export function SuccessorEditorCmp(gantt, UpdateTaskName) {
  return {
    show: function (id, column, config, placeholder) {
      // var txt = `${gantt.getTask(id).text} name=${column.name}`;
      // // var html =
      // //   "<div><input type='text' maxlength='80' autofocus class='ganttTaskEditInput' value='" + gantt.getTask(id).text + "' name='" + column.name + "'></div>";
      // var html =
      //   "<div><input type='text' maxlength='80' autofocus class='ganttTaskEditInput' value='" +
      //   gantt.getTask(id).text.replace(/'/g, "&#39;") +
      //   "' name='" +
      //   column.name +
      //   "'></div>";
      // placeholder.innerHTML = html;
      // placeholder.querySelector("input").focus();


      var html = "<div><input type='text' name='" + column.name + "'></div>";
      placeholder.innerHTML = html;
    },
    
    hide: function () {
      // called when input is hidden
      // destroy any complex editors or detach event listeners from here
    },

    set_value: function (value, id, column, node) {
      getInput(node).value = formatSuccessors(value, column.editor, gantt);
    },

    get_value: function (id, column, node) {
      return parseInputString((getInput(node).value || ""), column.editor);
    },

    is_changed: function (value, id, column, node) {
      var inputSuccessors = this.get_value(id, column, node);
      var taskSuccessors = parseInputString(formatSuccessors(value, column.editor, gantt), column.editor);

      return inputSuccessors.join() !== taskSuccessors.join();
    },

    is_valid: function (value, id, column, node) {
      // validate, changes will be discarded if the method returns false
      return true;
    },

    save: function (id, column, node) {
      var task = gantt.getTask(id);

      var linksDiff = getLinksDiff(task, this.get_value(id, column, node));

      if (linksDiff.add.length || linksDiff.remove.length) {
        gantt.batchUpdate(function () {
          linksDiff.add.forEach(function (link) {
            gantt.addLink(link);
          });
          linksDiff.remove.forEach(function (linkId) {
            gantt.deleteLink(linkId);
          });

          if (gantt.autoSchedule)
            gantt.autoSchedule();
        });
      }
    },

    focus: function (node) {
      gantt.config.editor_types.text.focus(node);
    }
  }
}

function getInput(node){
  return node.querySelector("input");
}

function formatSuccessors(task, config, gantt) {
  var links = task.$source;
  var labels = [];
  for (var i = 0; i < links.length; i++) {
    var link = gantt.getLink(links[i]);
    var pred = gantt.getTask(link.target);
    labels.push(gantt.getWBSCode(pred));
  }
  return labels.join((config.delimiter || ",") + " ");
}

function getSelectedLinks(taskId, successorsCodes) {
  var links = [];
  successorsCodes.forEach(function (code) {
    var successor = gantt.getTaskByWBSCode(code);
    if (successor) {
      var link = {
        source: taskId,
        target: successor.id,
        type: gantt.config.links.finish_to_start,
        lag: 0
      };
      if (gantt.isLinkAllowed(link)) {
        links.push(link);
      }
    }
  });
  return links;
}

function parseInputString(value, config) {
  var successors = (value || "").split(config.delimiter || ",");
  for (var i = 0; i < successors.length; i++) {
    var val = successors[i].trim();
    if (val) {
      successors[i] = val;
    } else {
      successors.splice(i, 1);
      i--;
    }
  }
  successors.sort();
  return successors;
}

function getLinksDiff(task, predecessorCodes) {
  var selectedLinks = getSelectedLinks(task.id, predecessorCodes);
  var existingLinksSearch = {};
  task.$target.forEach(function (linkId) {
    var link = gantt.getLink(linkId);
    existingLinksSearch[link.source + "_" + link.target] = link.id;
  });

  var linksToAdd = [];
  selectedLinks.forEach(function (link) {
    var linkKey = link.source + "_" + link.target;
    if (!existingLinksSearch[linkKey]) {
      linksToAdd.push(link);
    } else {
      delete existingLinksSearch[linkKey];
    }
  });

  var linksToDelete = [];
  for (var i in existingLinksSearch) {
    linksToDelete.push(existingLinksSearch[i]);
  }

  return {
    add: linksToAdd,
    remove: linksToDelete
  };
}