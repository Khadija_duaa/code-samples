import { TaskEditorCmp } from "./TaskEditor";
import { SuccessorEditorCmp } from "./SuccessorEditor";

export function ganttConfig(gantt, UpdateTaskName, UpdatePredecessor, UpdateSuccessor) {
    gantt.ganttEventsInitialized = true;
    gantt.config.open_tree_initially = true;
    gantt.config.order_branch = true;
    gantt.config.row_height = 45;
    gantt.config.row_line_height = 30;
    gantt.config.task_height = 18;
    gantt.config.order_branch = "marker";
    gantt.config.show_drag_vertical = true;
    gantt.config.show_drag_dates = true;
    gantt.config.drag_label_width = 70;
    gantt.config.drag_date = "%M %d";
    gantt.config.order_branch_free = true;
    gantt.config.drag_progress = true;
    gantt.config.initial_scroll = false;
    gantt.config.preserve_scroll = true;
    // gantt.config.min_column_width = 100;
    gantt.config.fit_tasks = true;
    gantt.config.show_errors = false;
    gantt.config.drag_progress = false;
    gantt.config.grid_resize = true;
    // gantt.config.grid_width = 600;
    gantt.config.sort = true;
    // gantt.config.auto_types = true;

    gantt.config.auto_scheduling = true;

    //Gantt Custom Task/milestone Editor Component
    gantt.config.editor_types.custom_editor = TaskEditorCmp(gantt, UpdateTaskName, UpdatePredecessor)
    gantt.config.editor_types.successor = SuccessorEditorCmp(gantt, UpdateTaskName, UpdateSuccessor)
    // skip off days / weekends
    // gantt.config.duration_unit = "hour";
    // gantt.config.skip_off_time = true;

    // this is calendar implementation
    gantt.config.work_time = true;
	gantt.config.duration_unit = "day";
    // gantt.setWorkTime({hours: ["8:00-17:00"]});
    // gantt.setWorkTime({day: 0, hours:  ["8:00-12:00"]});// make Sundays day-off
    // gantt.setWorkTime({day: 6, hours:  ["8:00-12:00"]});// make Saturdays day-off
    // gantt.setWorkTime({day: 1, hours: false});// make Mondays day-off
    // gantt.setWorkTime({day: 2, hours: false});// make Tuesdays day-off

    // //Parent Task bar renderer
    // gantt.config.type_renderers[gantt.config.types.project] = function (task) {
    // 	var main_el = document.createElement("div");
    // 	main_el.setAttribute(gantt.config.task_attribute, task.id);
    // 	var size = gantt.getTaskPosition(task);
    // 	main_el.innerHTML = [
    // 		"<div class='project-left'></div>",
    // 		"<div class='project-right'></div>"
    // 	].join('');
    // 	main_el.className = "custom-project";

    // 	main_el.style.left = size.left + "px";
    // 	main_el.style.top = size.top + 7 + "px";
    // 	main_el.style.width = size.width + "px";

    // 	return main_el;
    // };

}
