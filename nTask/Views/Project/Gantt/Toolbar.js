import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import AddIcon from "@material-ui/icons/AddCircle";
import CheckIcon from "@material-ui/icons/CheckCircle";
import MoneyIcon from "@material-ui/icons/MonetizationOn";
import Typography from "@material-ui/core/Typography";
import moment from "moment";
import Avatar from "@material-ui/core/Avatar";
import SvgIcon from "@material-ui/core/SvgIcon";
import CalendarToday from "@material-ui/icons/CalendarToday";
import { FormattedMessage } from "react-intl";
import gantStyles from "./style";
import DefaultTextField from "../../../components/Form/TextField";
import CustomTooltip from "../../../components/Tooltip/Tooltip";
import { projectGanttHelpText } from "../../../components/Tooltip/helptext";
import CustomAvatar from "../../../components/Avatar/Avatar";
import ViewDropdown from "./ViewDropdown";
import ZoomInIcon from "../../../components/Icons/ZoomInIcon";
import ZoomOutIcon from "../../../components/Icons/ZoomOutIcon";
import FullscreenIcon from "../../../components/Buttons/IconFullScreen";
import { CanAccessFeature } from "../../../components/AccessFeature/AccessFeature.cmp";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";

class Toolbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      addTask: false,
      addMileStone: false
    };
    this.handleZoomChange = this.handleZoomChange.bind(this);
    this.handleAddClick = this.handleAddClick.bind(this);
    this.handleInputBlur = this.handleInputBlur.bind(this);
  }

  handleZoomChange(e, value) {
    switch (this.props.zoom) {
      case 0:
        if (value == "zoomIn") {
          break;
        } else {
          this.props.onZoomChange(1);
          break;
        }

      case 1:
        if (value == "zoomIn") {
          this.props.onZoomChange(0);
          break;
        } else {
          this.props.onZoomChange(2);
        }
        break;
      case 2:
        if (value == "zoomIn") {
          this.props.onZoomChange(1);
        } else {
          break;
        }
      default:
      // code block
    }
  }

  handleInputBlur(event, name, inputValue) {
    this.setState({ [name]: false, [inputValue]: "" });
  }

  handleInputChange(event, name) {
    this.setState({ [name]: event.target.value });
  }

  handleAddClick(event, name) {
    this.setState({ [name]: true });
  }

  sortDates = (a, b) => {
    return a.getTime() - b.getTime();
  };

  getTotalCost = () => {
    /** function responsible for calculating the total cost */
    const { projectBillingMethod, feeType, tasks, resources, budget, hourlyRate } = this.props.ganttData.projectDetails;

    let totalCost = 0;
    const completedTasks = this.getCompletedTaskLength();
    const totalTasks = tasks.filter(t => t.isBillable).length;
    const resourcesArr = resources.filter(resourse => resourse.isBillable);
    const taskRows = tasks.filter(task => task.isBillable);

    switch (projectBillingMethod) {
      case 0 /** fixed fee */:
        totalCost =
          (budget / totalTasks) *
          completedTasks; /** [Total cost = [(Fixed fee / Total No. Of billable tasks) x Total No. Of billable Completed tasks] */
        return isNaN(totalCost) ? 0 : totalCost;
        break;

      case 1 /** fixed fee per task */:
        if (feeType == 0) {
          /** same fee for every task */
          totalCost =
            completedTasks *
            hourlyRate; /** (Total cost = Fee for billable tasks X Total No. Of billable Completed Tasks)  */
          return isNaN(totalCost) ? 0 : totalCost;
        } if (feeType == 1) {
          /** differnet fee for per task */
          let sum = 0;
          taskRows.map(t => {
            if (t.isDoneState) {
              sum =
                parseInt(t.fixedFee) +
                sum; /** (Total cost = [Fee for billable Completed Task (1)] + [Fee for billable Completed task (2)] + [ Fee for billable Completed Task(n)]) */
            }
          });
          totalCost = sum;
          return totalCost;
        }
        break;

      case 2 /** hourly rate per task */:
        if (feeType == 0) {
          /** same hourly rate for every task */
          let totalHours = 0;
          taskRows.map(t => {
            totalHours = Number(t.approvedTimeMin / 60) + totalHours;
          });
          totalCost =
            totalHours *
            hourlyRate; /** (Total cost = [Sum of approved time of all billable Tasks X hourly rate for billable Tasks]) */
          return totalCost;
        } if (feeType == 1) {
          /** diff hourly rate per task */
          let sum = 0;
          taskRows.map(t => {
            sum = Number(t.approvedTimeMin / 60) * t.hourlyRate + sum;
          });
          totalCost = sum; /** Total cost =  [Approved time of billable Task (1) x hourly rate for billable Task (1)] + [Approved time of billable Task (2) x hourly rate for billable task (2)] + [Approved d time of billable Task(n) x hourly rate for billable Task(n)]) */
          return totalCost;
        }
        break;

      case 3:
        if (feeType == 0) {
          let totalHours = 0;
          resourcesArr.map(t => {
            totalHours = Number(t.approvedTimeMin / 60) + totalHours;
          });
          totalCost =
            totalHours *
            hourlyRate; /** (Total cost = [Sum of approved time of all billable resources X hourly rate for billable resources]) */
          return totalCost;
        } if (feeType == 1) {
          let sum = 0;
          resourcesArr.map(t => {
            sum = Number(t.approvedTimeMin / 60) * parseInt(t.hourlyRate) + sum;
          });
          totalCost = sum; /** Total cost =  [Approved time of billable resource (1) x hourly rate for billable resource (1)] + [Approved time of billable resource (2) x hourly rate for billable resource (2)] + [Approved d time of billable resource(n) x hourly rate for billable resource(n)]) */
          return totalCost;
        }
        break;

      default:
        return null;
        break;
    }
  };

  getCompletedTaskLength = () => {
    /** fun calculate the total completed tasks */
    const { length } = this.props.ganttData.projectDetails.tasks.filter(t => t.isDoneState && t.isBillable);
    return length;
  };

  render() {
    const {
      classes,
      theme,
      zoom,
      tasks,
      ganttData,
      profileData,
      selectedView,
      handleGanttView,
      plannedVsActual,
      handleTaskbarsType,
      handleFullScreen
    } = this.props;
    const { addMileStone, addTask, taskTitle, mileStoneTitle } = this.state;
    const {
      completion,
      currency,
      cost,
      projectBudget,
      issues,
      risks,
      projectManager,
      projectName
    } = ganttData;

    const pmData = profileData.member.allMembers.filter(member => {
      return projectManager.indexOf(member.userId) > -1;
    });

    const filterGanttData = ganttData.data.filter(ele => {
      if (ele.plannedstartdate) {
        return ele;
      }
    });
    const projectStart = () => {
      const dates = [];
      filterGanttData.forEach(task => {
        dates.push(new Date(task.plannedstartdate));
      });
      const sorted = dates.sort(this.sortDates);
      return sorted[0];
    };

    // Getting the last task with respect to date
    const projectEnd = () => {
      const dates = [];
      filterGanttData.forEach(task => {
        dates.push(
          new Date(
            moment(task.plannedstartdate)
              .clone()
              .add(task.plannedDuration - 1, "days")
              .format()
          )
        );
      });
      const sorted = dates.sort(this.sortDates);
      return sorted[sorted.length - 1];
    };

    const GenerateList = () => {
      return pmData.slice(0, 3).map((member, i, t) => {
        return (
          // flex_center_center_row class is applied when there is only 1 user to style the user full name
          <li
            key={i}
            style={{ zIndex: i, marginLeft: !i == 0 ? -5 : null }}
            className={t.length == 1 ? "flex_center_center_row" : null}
          >
            {
              <CustomAvatar
                otherMember={{
                  imageUrl: member.imageUrl,
                  fullName: member.fullName,
                  lastName: "",
                  email: member.email,
                  isOnline: member.isOnline,
                  isOwner: member.isOwner
                }}
                size="xsmall"
              />
            }
            {t.length == 1 ? ( // Condition to show project manager full name if there is only 1 project manager
              <Typography
                variant="body2"
                className={classes.projectManagerName}
              >
                {member.fullName}
              </Typography>
            ) : null}
          </li>
        );
      });
    };
    const calcProjectBudget =
      projectBudget > 0 ? `${currency} ${projectBudget}` : "-";
    const costPercent = projectBudget > 0 ? `(${Math.round((cost / projectBudget) * 100)}%)` : "";
    const calcCost = cost > 0 ? `${currency} ${cost} ${costPercent}` : "-";


    const actualProjectCost = this.getTotalCost().toFixed(2);
    const actualCostPercent = projectBudget > 0 ? `(${Math.round((actualProjectCost / projectBudget) * 100)}%)` : "";
    const actualCalcCost = actualProjectCost > 0 ? `${currency} ${actualProjectCost} ${actualCostPercent}` : "-";

    return (
      <div className={classes.ganttHeaderCnt}>
        <div className={classes.ganttToolBarHeading}>
          <Typography variant="h1">{projectName}</Typography>
          <div className="flex_center_start_row">
            <CalendarToday style={{ fontSize: "20px", marginRight: 5 }} htmlColor={theme.palette.text.medGray} />
            <Typography variant="h5" className={classes.projectStartEndDates}>
              {projectStart() ? (
                <>
                  {moment(projectStart()).format("MMM DD, YYYY")}
                  {' '}
                  -
                  {" "}
                  {moment(projectEnd()).format("MMM DD, YYYY")}
                  {" "}
                </>
              ) : (
                <FormattedMessage id="project.gant-view.not-schedule.label" defaultMessage="Not scheduled" />
              )}
            </Typography>
          </div>
        </div>

        <div className={classes.toolbarCnt}>
          {/* Project Start End */}
          {/* <div className={`${classes.projectStartEndCnt} flex_center_center_col`}>
          <Typography variant="body2" className={classes.smallHeading}>
            PLANNED PROJECT START / END
          </Typography>
         
        </div> */}

          {/* Project Start End Ends */}

          {/* Project Budget Starts */}

          <CanAccessFeature group='project' feature='billingType'>
            <div className={classes.ganttHeaderToolbarTier}>
              <Typography variant="h1" className={classes.blackCount}>
                {calcProjectBudget}
              </Typography>
              <Typography variant="body2" className={classes.smallHeading}>
                <FormattedMessage id="project.gant-view.project-budget.label" defaultMessage="Project Budget" />
              </Typography>
            </div>
            {/* Project Budget Ends */}
            {/* Cost Starts */}
            <div className={classes.ganttHeaderToolbarTier}>
              {/* <Typography variant="h1" className={cost > projectBudget ? classes.redCount : classes.greenCount}> */}
              <Typography variant="h1" className={actualProjectCost > projectBudget ? classes.redCount : classes.greenCount}>
                {/* {calcCost} */}
                {actualCalcCost}
              </Typography>
              <Typography variant="body2" className={classes.smallHeading}>
                <FormattedMessage id="project.gant-view.project-cost.label" defaultMessage="Project Cost" />
                <CustomTooltip
                  helptext={<FormattedMessage id="project.gant-view.project-cost.hint" values={{ t1: <br /> }} defaultMessage={projectGanttHelpText.projectCost} />}
                  style={{ fontSize: "14px" }}
                  position="static"
                  iconType="help"
                />
              </Typography>
            </div>
            {/* Cost Ends */}
          </CanAccessFeature>

          {/* Progress Starts */}
          <div className={classes.ganttHeaderToolbarTier}>
            <Typography variant="h1" className={classes.greenCount}>
              {completion ? `${completion}%` : "0%"}
            </Typography>
            <Typography variant="body2" className={classes.smallHeading}>
              <FormattedMessage id="project.gant-view.completion.label" defaultMessage="completion" />
            </Typography>
          </div>
          {/* Progress End */}

          {/* Issue Starts */}
          <div className={classes.ganttHeaderToolbarTier}>
            <Typography variant="h1" className={classes.redCount}>
              {issues}
            </Typography>
            <Typography variant="body2" className={classes.smallHeading}>
              <FormattedMessage id="issue.label" defaultMessage="Issues" />
            </Typography>
          </div>
          {/* Issue Ends */}
          {/* Risks Starts */}
          {teamCanView("riskAccess") && (
            <div className={classes.ganttHeaderToolbarTier}>
              <div>
                <Typography variant="h1" className={classes.orangeCount}>
                  {risks}
                </Typography>
                <Typography variant="body2" className={classes.smallHeading}>
                  <FormattedMessage id="risk.label" defaultMessage="Risks" />

                </Typography>
              </div>
            </div>
          )}

          {/* Risks Ends */}

          {/* Project Managers Starts */}
          <div className={`${classes.ganttHeaderToolbarTier} ${classes.assigneeTier}`}>
            <div>
              <ul className="AssigneeAvatarList">
                <GenerateList />
                {pmData.length > 3 ? (
                  <li>
                    <Avatar classes={{ root: classes.TotalAssignee }}>
                      +
                      {pmData.length - 3}
                    </Avatar>
                  </li>
                ) : (
                  ""
                )}
              </ul>
              <Typography variant="body2" className={classes.smallHeading}>
                <FormattedMessage id="project.gant-view.proejct-manager.label" defaultMessage="Project managers" />
              </Typography>
            </div>
          </div>
          {/* View Dropdown Starts */}
          <div className={classes.ganttHeaderToolbarTier}>
            <ViewDropdown
              ganttTaskData={this.props.tasks}
              handleGanttView={handleGanttView}
              selectedView={selectedView}
              handleTaskbarsType={handleTaskbarsType}
              plannedVsActual={plannedVsActual}
              projectName={ganttData.projectName}
              projectId={ganttData.projectDetails.projectId}
            />
          </div>
          {/* View Dropdown Ends */}
          {/* Project Managers Ends */}


          {/* Risk Ends */}


        </div>
        {/* Zoom Starts */}
        <div className={`${classes.ganttExtraBtnsCnt} flex_center_center_col`}>
          <a
            className={classes.zoomOutCnt}
            onClick={e => this.handleZoomChange(e, "zoomOut")}
          >
            <SvgIcon
              viewBox="0 0 14 14"
              className={classes.zoomOutIcon}
            // htmlColor={theme.palette.error.main}
            >
              <ZoomOutIcon />
            </SvgIcon>
          </a>
          <a
            className={classes.zoomInCnt}
            onClick={e => this.handleZoomChange(e, "zoomIn")}
          >
            <SvgIcon
              viewBox="0 0 14 14"
              className={classes.zoomInIcon}
            // htmlColor={theme.palette.error.main}
            >
              <ZoomInIcon />
            </SvgIcon>
          </a>
          <a
            onClick={handleFullScreen}
            btnType="filledWhite"
            className={classes.fullScreenBtn}
          >
            <FullscreenIcon />
          </a>
        </div>
        {/* Zoom Ends */}
      </div>
    );
  }
}

export default withStyles(gantStyles, { withTheme: true })(Toolbar);
