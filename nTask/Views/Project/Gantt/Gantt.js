/*global gantt*/
import "dhtmlx-gantt";
import React, { Component } from "react";
import "dhtmlx-gantt/codebase/dhtmlxgantt.css";
// import "dhtmlx-gantt/codebase/dhtmlxgantt_tooltip";
// import "dhtmlx-gantt/codebase/dhtmlxgantt_auto_scheduling";
// import "dhtmlx-gantt/codebase/dhtmlxgantt_fullscreen";
// import "dhtmlx-gantt/codebase/dhtmlxgantt_tooltip";
// import "dhtmlx-gantt/codebase/dhtmlxgantt_critical_path";
// import "dhtmlx-gantt/codebase/dhtmlxgantt_overlay";
// import "dhtmlx-gantt/codebase/dhtmlxgantt_marker";
import { withStyles } from "@material-ui/core/styles";
import SvgIcon from "@material-ui/core/SvgIcon";
import isUndefined from "lodash/isUndefined";
import moment from "moment";
import { FormattedMessage, injectIntl } from "react-intl";
import { connect } from "react-redux";
import { compose } from "redux";
import DeleteConfirmDialog from "../../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import DefaultDialog from "../../../components/Dialog/Dialog";
import GanttToDoDrawer from "../../../components/Drawer/GanttToDoDrawer";
import HideColumnsIcon from "../../../components/Icons/HideColumnsIcon";
import ShowColumnsIcon from "../../../components/Icons/ShowColumnsIcon";
import TodoList from "../../../components/TodoList/TodoList";
import { taskDetailDialogState } from "../../../redux/actions/allDialogs";
import { getGanttTasks } from "../../../redux/actions/gantt";
import { getTaskComments, UpdateTaskProgressBar } from "../../../redux/actions/tasks";
import ProjectPermissionSelector from "../../../redux/selectors/projectPermissionSelector";
import AddNewTaskForm from "./addNewTask/addNewTaskFrom";
import columns from "./columns";
import GanttColumnsDropDown from "./GanttColumnsDropDown";
import { SlackTime } from "./ganttComponents/GanttSlackTime";
import { Scurve } from "./ganttComponents/Scurve";
import { ToolTipCmp } from "./ganttComponents/Tooltip";
import { ganttConfig } from "./ganttConfig";
import { onGanttReady } from "./ganttEvents/onGanttReady";
import { setZoom } from "./zoom";
let fixedColumns = ["text", "buttons"];

let EmptyStateArrow = `<svg
    id="Shape"
    xmlns="http://www.w3.org/2000/svg"
    width="86.906"
    height="224.344"
    viewBox="0 0 86.906 224.344"
  >
    <path
      style="fill: none;
      stroke: #ddd;
      stroke-linecap: round;
      stroke-linejoin: round;
      stroke-width: 2px;
      stroke-dasharray: 6 4;
      fill-rule: evenodd;"
      d="M492,465s137.745-72.77,57-211"
      transform="translate(-490.469 -243.531)"
    />
    <path
      id="Polygon_4412"
      data-name="Polygon 4412"
      style="fill-rule: evenodd; fill: #ddd;"
      d="M545.53,243.526l8.938,7.342-11.919,4.261Z"
      transform="translate(-490.469 -243.531)"
    />
  </svg>`;

class GanttComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      deleteLinkDialog: false,
      showGrid: true,
      taskDialog: false,
      openChecklist: false,
      checkListTask: null,
      gridWidth: 0,
      editMilestoneData: {},
      taskTypeTitle: "Task",
      todoListItems: [],
      deleteBtnQuery: "",
    };
    this.initGanttEvents = this.initGanttEvents.bind(this);
    this.gantt = window?.Gantt.getGanttInstance();
    window.gantt = this.gantt;
    this.gantt.plugins({
      auto_scheduling: true,
      marker: true,
      overlay: true,
      critical_path: true,
      tooltip: true,
      fullscreen: true,
    });
    (function () {
      var apiUrl = "https://rapi.ntaskmanager.com/gantt";
      // var apiUrl = "https://export.dhtmlx.com/gantt";

      var templates = [
        "leftside_text",
        "rightside_text",
        "task_text",
        "progress_text",
        "task_class",
      ];

      function xdr(url, pack, cb) {
        switch (true) {
          case gantt.env.isIE:
            gantt.env.isIE = false;
            gantt.ajax.post(url, pack, cb);
            gantt.env.isIE = true;
            break;
          case gantt.env.isNode:
            const http = require("http");

            var parts1 = url.split("://")[1];
            var parts2 = parts1.split("/")[0].split(":");
            var parts3 = parts1.split("/");

            var hostname = parts2[0];
            var port = parts2[1] || 80;
            var path = "/" + parts3.slice(1).join("/");

            const options = {
              hostname: hostname,
              port: port,
              path: path,
              method: "POST",
              headers: {
                "Content-Type": "application/json",
                "Content-Length": JSON.stringify(pack).length,
              },
            };

            const req = http.request(options, function (res) {
              let resData = [];
              res.on("data", function (d) {
                resData.push(d);
              });
              res.on("end", function (d) {
                cb(Buffer.concat(resData));
              });
            });

            req.on("error", function (error) {
              console.error(error);
            });

            req.write(JSON.stringify(pack));
            req.end();

            break;
          default:
            gantt.ajax.post(url, pack, cb);
        }
      }

      function defaults(obj, std) {
        for (var key in std) if (!obj[key]) obj[key] = std[key];
        return obj;
      }

      //compatibility for new versions of gantt
      if (!gantt.ajax) {
        if (window.dhtmlxAjax) {
          gantt.ajax = window.dhtmlxAjax;
        } else if (window.dhx4) {
          gantt.ajax = window.dhx4.ajax;
        }
      }

      function mark_columns(base) {
        let columns = base.config.columns;
        if (columns)
          for (var i = 0; i < columns.length; i++) {
            if (columns[i].template) columns[i].$template = true;
            if (columns[i].name == "buttons") {
              columns[i].hide = true;
            }
          }
        window.gantt.render();
      }

      function add_export_methods(gantt) {
        var color_box = null;
        var color_hash = {};

        function get_styles(css) {
          if (!color_box) {
            var color_box = document.createElement("DIV");
            color_box.style.cssText = "position:absolute; display:none;";
            document.body.appendChild(color_box);
          }
          if (color_hash[css]) return color_hash[css];

          color_box.className = css;
          return (color_hash[css] =
            get_color(color_box, "color") + ";" + get_color(color_box, "backgroundColor"));
        }

        function getMinutesWorktimeSettings(parsedRanges) {
          var minutes = [];
          parsedRanges.forEach(function (range) {
            minutes.push(range.startMinute);
            minutes.push(range.endMinute);
          });
          return minutes;
        }

        gantt._getWorktimeSettings = function () {
          var defaultWorkTimes = {
            hours: [0, 24],
            minutes: null,
            dates: { 0: true, 1: true, 2: true, 3: true, 4: true, 5: true, 6: true },
          };

          var time;
          if (!gantt.config.work_time) {
            time = defaultWorkTimes;
          } else {
            var wTime = gantt._working_time_helper;
            if (wTime && wTime.get_calendar) {
              time = wTime.get_calendar();
            } else if (wTime) {
              time = {
                hours: wTime.hours,
                minutes: null,
                dates: wTime.dates,
              };
            } else if (gantt.config.worktimes && gantt.config.worktimes.global) {
              var settings = gantt.config.worktimes.global;

              if (settings.parsed) {
                var minutes = getMinutesWorktimeSettings(settings.parsed.hours);
                time = {
                  hours: null,
                  minutes: minutes,
                  dates: {},
                };
                for (var i in settings.parsed.dates) {
                  if (Array.isArray(settings.parsed.dates[i])) {
                    time.dates[i] = getMinutesWorktimeSettings(settings.parsed.dates[i]);
                  } else {
                    time.dates[i] = settings.parsed.dates[i];
                  }
                }
              } else {
                time = {
                  hours: settings.hours,
                  minutes: null,
                  dates: settings.dates,
                };
              }
            } else {
              time = defaultWorkTimes;
            }
          }

          return time;
        };

        gantt.exportToPDF = function (config) {
          if (config && config.raw) {
            config = defaults(config, {
              name: "gantt.pdf",
              data: this._serialize_html(),
            });
          } else {
            config = defaults(config || {}, {
              name: "gantt.pdf",
              data: this._serialize_all(),
              config: this.config,
            });
            fix_columns(gantt, config.config.columns);
          }

          config.version = this.version;
          this._send_to_export(config, "pdf");
        };

        gantt.exportToPNG = function (config) {
          if (config && config.raw) {
            config = defaults(config, {
              name: "gantt.png",
              data: this._serialize_html(),
            });
          } else {
            config = defaults(config || {}, {
              name: "gantt.png",
              data: this._serialize_all(),
              config: this.config,
            });
            fix_columns(gantt, config.config.columns);
          }

          config.version = this.version;
          this._send_to_export(config, "png");
        };

        gantt.exportToICal = function (config) {
          config = defaults(config || {}, {
            name: "gantt.ical",
            data: this._serialize_plain().data,
            version: this.version,
          });
          this._send_to_export(config, "ical");
        };

        function eachTaskTimed(start, end) {
          return function (code, parent, master) {
            parent = parent || this.config.root_id;
            master = master || this;

            var branch = this.getChildren(parent);
            if (branch)
              for (var i = 0; i < branch.length; i++) {
                var item = this._pull[branch[i]];
                if ((!start || item.end_date > start) && (!end || item.start_date < end))
                  code.call(master, item);

                if (this.hasChild(item.id)) this.eachTask(code, item.id, master);
              }
          };
        }

        gantt.exportToExcel = function (config) {
          config = config || {};

          var tasks, dates;
          var state, scroll;
          if (config.start || config.end) {
            state = this.getState();
            dates = [this.config.start_date, this.config.end_date];
            scroll = this.getScrollState();
            var convert = this.date.str_to_date(this.config.date_format);
            tasks = this.eachTask;

            if (config.start) this.config.start_date = convert(config.start);
            if (config.end) this.config.end_date = convert(config.end);

            this.render();
            this.eachTask = eachTaskTimed(this.config.start_date, this.config.end_date);
          }

          this._no_progress_colors = config.visual === "base-colors";

          var data = null;
          if (!gantt.env.isNode) {
            data = this._serialize_table(config).data;
          }

          config = defaults(config, {
            name: "gantt.xlsx",
            title: "Tasks",
            data: data,
            columns: this._serialize_columns({ rawDates: true }),
            version: this.version,
          });

          if (config.visual) config.scales = this._serialize_scales(config);

          this._send_to_export(config, "excel");

          if (config.start || config.end) {
            this.config.start_date = state.min_date;
            this.config.end_date = state.max_date;
            this.eachTask = tasks;

            this.render();
            this.scrollTo(scroll.x, scroll.y);

            this.config.start_date = dates[0];
            this.config.end_date = dates[1];
          }
        };

        gantt.exportToJSON = function (config) {
          config = defaults(config || {}, {
            name: "gantt.json",
            data: this._serialize_all(),
            config: this.config,
            columns: this._serialize_columns(),
            worktime: gantt._getWorktimeSettings(),
            version: this.version,
          });
          this._send_to_export(config, "json");
        };

        function sendImportAjax(config) {
          var url = config.server || apiUrl;
          var store = config.store || 0;
          var formData = config.data;
          var callback = config.callback;

          formData.append("type", "excel-parse");
          formData.append(
            "data",
            JSON.stringify({
              sheet: config.sheet || 0,
              server: url,
            })
          );

          if (store) formData.append("store", store);

          var xhr = new XMLHttpRequest();
          xhr.onreadystatechange = function (e) {
            if (xhr.readyState == 4 && xhr.status == 0) {
              // network error
              if (callback) {
                callback(null);
              }
            }
          };

          xhr.onload = function () {
            var fail = xhr.status > 400;
            var info = null;

            if (!fail) {
              try {
                info = JSON.parse(xhr.responseText);
              } catch (e) { }
            }

            if (callback) {
              callback(info);
            }
          };

          xhr.open("POST", url, true);
          xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
          xhr.send(formData);
        }

        function nodejsImportExcel(config) {
          const http = require("http");
          var FormData = require("form-data");

          var url = config.server || apiUrl;

          var parts1 = url.split("://")[1];
          var parts2 = parts1.split("/")[0].split(":");
          var parts3 = parts1.split("/");

          var hostname = parts2[0];
          var port = parts2[1] || 80;
          var path = "/" + parts3.slice(1).join("/");

          const options = {
            hostname: hostname,
            port: port,
            path: path,
            method: "POST",
            headers: {
              "X-Requested-With": "XMLHttpRequest",
            },
          };

          var formData = new FormData();
          formData.append("file", config.data);
          formData.append("type", "excel-parse");
          formData.append(
            "data",
            JSON.stringify({
              sheet: config.sheet || 0,
            })
          );

          options.headers["Content-Type"] = formData.getHeaders()["content-type"];

          const req = http.request(options, function (res) {
            let resData = "";
            res.on("data", function (d) {
              resData += d;
            });
            res.on("end", function (d) {
              config.callback(resData.toString());
            });
          });

          req.on("error", function (error) {
            console.error(error);
          });
          formData.pipe(req);
        }

        gantt.importFromExcel = function (config) {
          try {
            var formData = config.data;
            if (formData instanceof FormData) {
            } else if (formData instanceof File) {
              var data = new FormData();
              data.append("file", formData);
              config.data = data;
            }
          } catch (error) { }
          if (gantt.env.isNode) {
            nodejsImportExcel(config);
          } else {
            sendImportAjax(config);
          }
        };

        gantt._msp_config = function (config) {
          if (config.project)
            for (var i in config.project) {
              if (!config._custom_data) config._custom_data = {};
              config._custom_data[i] = config.project[i](this.config);
            }

          if (config.tasks)
            for (var j = 0; j < config.data.length; j++) {
              var el = this.getTask(config.data[j].id);
              if (!el._custom_data) el._custom_data = {};
              for (var i in config.tasks) el._custom_data[i] = config.tasks[i](el, this.config);
            }

          delete config.project;
          delete config.tasks;

          config.time = gantt._getWorktimeSettings();

          var p_dates = this.getSubtaskDates();
          var format = this.date.date_to_str("%d-%m-%Y %H:%i:%s");
          config.start_end = {
            start_date: format(p_dates.start_date),
            end_date: format(p_dates.end_date),
          };
        };

        gantt._msp_data = function () {
          var old_xml_format = this.templates.xml_format;
          var old_format_date = this.templates.format_date;
          this.templates.xml_format = this.date.date_to_str("%d-%m-%Y %H:%i:%s");
          this.templates.format_date = this.date.date_to_str("%d-%m-%Y %H:%i:%s");

          var data = this._serialize_all();

          this.templates.xml_format = old_xml_format;
          this.templates.format_date = old_format_date;
          return data;
        };

        gantt._ajax_to_export = function (data, type, callback) {
          delete data.callback;

          var url = data.server || apiUrl;
          var pack = "type=" + type + "&store=1&data=" + encodeURIComponent(JSON.stringify(data));

          var cb = function (loader) {
            var xdoc = loader.xmlDoc || loader;
            var fail = xdoc.status > 400;
            var info = null;

            if (!fail) {
              try {
                info = JSON.parse(xdoc.responseText);
              } catch (e) { }
            }
            callback(info);
          };

          xdr(url, pack, cb);
        };

        gantt._send_to_export = function (data, type) {
          var convert = this.date.date_to_str(this.config.date_format || this.config.xml_date);
          if (data.config) {
            data.config = this.copy(data.config);
            mark_columns(data, type);

            if (data.config.start_date && data.config.end_date) {
              if (data.config.start_date instanceof Date) {
                data.config.start_date = convert(data.config.start_date);
              }
              if (data.config.end_date instanceof Date) {
                data.config.end_date = convert(data.config.end_date);
              }
            }
          }

          if (gantt.env.isNode) {
            var url = data.server || apiUrl;
            var pack = {
              type: type,
              store: 0,
              data: JSON.stringify(data),
            };
            var callbackFunction =
              data.callback ||
              function (response) {
                console.log(response);
              };

            return xdr(url, pack, callbackFunction);
          }

          if (data.callback) {
            return gantt._ajax_to_export(data, type, data.callback);
          }

          var form = this._create_hidden_form();
          form.firstChild.action = data.server || apiUrl;
          form.firstChild.childNodes[0].value = JSON.stringify(data);
          form.firstChild.childNodes[1].value = type;
          form.firstChild.submit();
        };

        gantt._create_hidden_form = function () {
          if (!this._hidden_export_form) {
            var t = (this._hidden_export_form = document.createElement("div"));
            t.style.display = "none";
            t.innerHTML =
              "<form method='POST' target='_blank'><textarea name='data' style='width:0px; height:0px;' readonly='true'></textarea><input type='hidden' name='type' value=''></form>";
            document.body.appendChild(t);
          }
          return this._hidden_export_form;
        };

        //patch broken json serialization in gantt 2.1
        var original = gantt.json._copyObject;
        function copy_object_base(obj) {
          var copy = {};
          for (var key in obj) {
            if (key.charAt(0) == "$") continue;
            copy[key] = obj[key];
          }

          var formatDate = gantt.templates.xml_format || gantt.templates.format_date;

          copy.start_date = formatDate(copy.start_date);
          if (copy.end_date) copy.end_date = formatDate(copy.end_date);

          return copy;
        }

        function copy_object_plain(obj) {
          var text = gantt.templates.task_text(obj.start_date, obj.end_date, obj);

          var copy = copy_object_base(obj);
          copy.text = text || copy.text;

          return copy;
        }

        function get_color(node, style) {
          var value = node.currentStyle
            ? node.currentStyle[style]
            : getComputedStyle(node, null)[style];
          var rgb = value.replace(/\s/g, "").match(/^rgba?\((\d+),(\d+),(\d+)/i);
          return (
            rgb && rgb.length === 4
              ? ("0" + parseInt(rgb[1], 10).toString(16)).slice(-2) +
              ("0" + parseInt(rgb[2], 10).toString(16)).slice(-2) +
              ("0" + parseInt(rgb[3], 10).toString(16)).slice(-2)
              : value
          ).replace("#", "");
        }

        // Excel interprets UTC time as local time in every timezone, send local time instead of actual UTC time.
        // https://github.com/SheetJS/js-xlsx/issues/126#issuecomment-60531614
        var toISOstring = gantt.date.date_to_str("%Y-%m-%dT%H:%i:%s.000Z");

        // excel serialization
        function copy_object_table(obj) {
          var copy = copy_object_columns(obj, copy_object_plain(obj));
          if (copy.start_date) copy.start_date = toISOstring(obj.start_date);
          if (copy.end_date) copy.end_date = toISOstring(obj.end_date);

          // private gantt._day_index_by_date was replaced by public gantt.columnIndexByDate in gantt 5.0
          var getDayIndex = gantt._day_index_by_date
            ? gantt._day_index_by_date
            : gantt.columnIndexByDate;

          copy.$start = getDayIndex.call(gantt, obj.start_date);
          copy.$end = getDayIndex.call(gantt, obj.end_date);
          copy.$level = obj.$level;
          copy.$type = obj.$rendered_type;

          var tmps = gantt.templates;
          copy.$text = tmps.task_text(obj.start, obj.end_date, obj);
          copy.$left = tmps.leftside_text ? tmps.leftside_text(obj.start, obj.end_date, obj) : "";
          copy.$right = tmps.rightside_text
            ? tmps.rightside_text(obj.start, obj.end_date, obj)
            : "";

          return copy;
        }

        function copy_object_colors(obj) {
          var copy = copy_object_table(obj);

          var node = gantt.getTaskNode(obj.id);
          if (node && node.firstChild) {
            var color = get_color(
              gantt._no_progress_colors ? node : node.firstChild,
              "backgroundColor"
            );
            if (color == "363636") color = get_color(node, "backgroundColor");

            copy.$color = color;
          } else if (obj.color) copy.$color = obj.color;

          return copy;
        }

        function copy_object_columns(obj, copy) {
          for (var i = 0; i < gantt.config.columns.length; i++) {
            var ct = gantt.config.columns[i].template;
            if (ct) {
              var val = ct(obj);
              if (val instanceof Date) val = gantt.templates.date_grid(val, obj);
              copy["_" + i] = val;
            }
          }
          return copy;
        }

        function copy_object_all(obj) {
          var copy = copy_object_base(obj);

          //serialize all text templates
          for (var i = 0; i < templates.length; i++) {
            var template = gantt.templates[templates[i]];
            if (template) copy["$" + i] = template(obj.start_date, obj.end_date, obj);
          }

          copy_object_columns(obj, copy);
          copy.open = obj.$open;
          return copy;
        }

        function fix_columns(gantt, columns) {
          for (var i = 0; i < columns.length; i++) {
            columns[i].label = columns[i].label || gantt.locale.labels["column_" + columns[i].name];
            if (typeof columns[i].width == "string") columns[i].width = columns[i].width * 1;
          }
        }

        gantt._serialize_html = function () {
          var smartScales = gantt.config.smart_scales;
          var smartRendering = gantt.config.smart_rendering;
          if (smartScales || smartRendering) {
            gantt.config.smart_rendering = false;
            gantt.config.smart_scales = false;
            gantt.render();
          }

          var html = this.$container.parentNode.innerHTML;

          if (smartScales || smartRendering) {
            gantt.config.smart_scales = smartScales;
            gantt.config.smart_rendering = smartRendering;
            gantt.render();
          }

          return html;
        };

        gantt._serialize_all = function () {
          gantt.json._copyObject = copy_object_all;
          var data = export_serialize();
          gantt.json._copyObject = original;
          return data;
        };

        gantt._serialize_plain = function () {
          var oldXmlFormat = gantt.templates.xml_format;
          var oldFormatDate = gantt.templates.format_date;
          gantt.templates.xml_format = gantt.date.date_to_str("%Y%m%dT%H%i%s", true);
          gantt.templates.format_date = gantt.date.date_to_str("%Y%m%dT%H%i%s", true);
          gantt.json._copyObject = copy_object_plain;

          var data = export_serialize();

          gantt.templates.xml_format = oldXmlFormat;
          gantt.templates.format_date = oldFormatDate;
          gantt.json._copyObject = original;

          delete data.links;
          return data;
        };

        function get_raw() {
          // support Gantt < 5.0
          if (gantt._scale_helpers) {
            var scales = gantt._get_scales(),
              min_width = gantt.config.min_column_width,
              autosize_min_width = gantt._get_resize_options().x
                ? Math.max(gantt.config.autosize_min_width, 0)
                : config.$task.offsetWidth,
              height = config.config.scale_height - 1;
            return gantt._scale_helpers.prepareConfigs(
              scales,
              min_width,
              autosize_min_width,
              height
            );
          } else {
            // Gantt >= 5.0
            var timeline = gantt.$ui.getView("timeline");
            if (timeline) {
              var availWidth = timeline.$config.width;
              if (gantt.config.autosize == "x" || gantt.config.autosize == "xy") {
                availWidth = Math.max(gantt.config.autosize_min_width, 0);
              }
              var state = gantt.getState(),
                scales = timeline._getScales(),
                min_width = gantt.config.min_column_width,
                height = gantt.config.scale_height - 1,
                rtl = gantt.config.rtl;
              return timeline.$scaleHelper.prepareConfigs(
                scales,
                min_width,
                availWidth,
                height,
                state.min_date,
                state.max_date,
                rtl
              );
            }
          }
        }

        gantt._serialize_table = function (config) {
          gantt.json._copyObject = config.visual ? copy_object_colors : copy_object_table;
          var data = export_serialize();
          gantt.json._copyObject = original;

          delete data.links;

          if (config.cellColors) {
            var css = this.templates.timeline_cell_class || this.templates.task_cell_class;
            if (css) {
              var raw = get_raw();
              var steps = raw[0].trace_x;
              for (var i = 1; i < raw.length; i++)
                if (raw[i].trace_x.length > steps.length) steps = raw[i].trace_x;

              for (var i = 0; i < data.data.length; i++) {
                data.data[i].styles = [];
                var task = this.getTask(data.data[i].id);
                for (var j = 0; j < steps.length; j++) {
                  var date = steps[j];
                  var cell_css = css(task, date);
                  if (cell_css)
                    data.data[i].styles.push({ index: j, styles: get_styles(cell_css) });
                }
              }
            }
          }
          return data;
        };

        gantt._serialize_scales = function (config) {
          var scales = [];
          var raw = get_raw();

          var min = Infinity;
          var max = 0;
          for (var i = 0; i < raw.length; i++) min = Math.min(min, raw[i].col_width);

          for (var i = 0; i < raw.length; i++) {
            var start = 0;
            var end = 0;
            var row = [];

            scales.push(row);
            var step = raw[i];
            max = Math.max(max, step.trace_x.length);
            var template =
              step.format ||
              step.template ||
              (step.date ? gantt.date.date_to_str(step.date) : gantt.config.date_scale);

            for (var j = 0; j < step.trace_x.length; j++) {
              var date = step.trace_x[j];
              end = start + Math.round(step.width[j] / min);

              var scale_cell = { text: template(date), start: start, end: end };

              if (config.cellColors) {
                var css = step.css || this.templates.scale_cell_class;
                if (css) {
                  var scale_css = css(date);
                  if (scale_css) scale_cell.styles = get_styles(scale_css);
                }
              }

              row.push(scale_cell);
              start = end;
            }
          }

          return { width: max, height: scales.length, data: scales };
        };

        gantt._serialize_columns = function (config) {
          gantt.exportMode = true;

          var columns = [];
          var cols = gantt.config.columns;

          var ccount = 0;
          for (var i = 0; i < cols.length; i++) {
            if (cols[i].name == "add" || cols[i].name == "buttons") continue;

            columns[ccount] = {
              id: cols[i].template ? "_" + i : cols[i].name,
              header: cols[i].label || gantt.locale.labels["column_" + cols[i].name],
              width: cols[i].width ? Math.floor(cols[i].width / 4) : "",
            };

            if (cols[i].name == "duration") columns[ccount].type = "number";
            if (cols[i].name == "start_date" || cols[i].name == "end_date") {
              columns[ccount].type = "date";
              if (config && config.rawDates) columns[ccount].id = cols[i].name;
            }

            ccount++;
          }

          gantt.exportMode = false;
          return columns;
        };

        function export_serialize() {
          gantt.exportMode = true;

          var xmlFormat = gantt.templates.xml_format;
          var formatDate = gantt.templates.format_date;

          // use configuration date format for serialization so date could be parsed on the export
          // required when custom format date function is defined
          gantt.templates.xml_format = gantt.templates.format_date = gantt.date.date_to_str(
            gantt.config.date_format || gantt.config.xml_date
          );

          var data = gantt.serialize();

          gantt.templates.xml_format = xmlFormat;
          gantt.templates.format_date = formatDate;
          gantt.exportMode = false;
          return data;
        }
      }

      add_export_methods(gantt);
      try {
        if (window && window.Gantt && Gantt.plugin) {
          Gantt.plugin(add_export_methods);
        }
      } catch (error) { }
    })();

    (function () {
      var apiUrl = "https://rapi.ntaskmanager.com/gantt";
      // var apiUrl = "https://export.dhtmlx.com/gantt";

      function set_level(data) {
        for (var i = 0; i < data.length; i++) {
          if (data[i].parent == 0) {
            data[i]._lvl = 1;
          }
          for (var j = i + 1; j < data.length; j++) {
            if (data[i].id == data[j].parent) {
              data[j]._lvl = data[i]._lvl + 1;
            }
          }
        }
      }

      function clear_level(data) {
        for (var i = 0; i < data.length; i++) {
          delete data[i]._lvl;
        }
      }

      function clear_rec_links(data) {
        set_level(data.data);
        var tasks = {};
        for (var i = 0; i < data.data.length; i++) {
          tasks[data.data[i].id] = data.data[i];
        }

        var links = {};

        for (i = 0; i < data.links.length; i++) {
          var link = data.links[i];
          if (
            gantt.isTaskExists(link.source) &&
            gantt.isTaskExists(link.target) &&
            tasks[link.source] &&
            tasks[link.target]
          ) {
            links[link.id] = link;
          }
        }

        for (i in links) {
          make_links_same_level(links[i], tasks);
        }

        var skippedLinks = {};
        for (i in tasks) {
          clear_circ_dependencies(tasks[i], links, tasks, {}, skippedLinks, null);
        }

        for (i in links) {
          clear_links_same_level(links, tasks);
        }

        for (i = 0; i < data.links.length; i++) {
          if (!links[data.links[i].id]) {
            data.links.splice(i, 1);
            i--;
          }
        }

        clear_level(data.data);
      }

      function clear_circ_dependencies(task, links, tasks, usedTasks, skippedLinks, prevLink) {
        var sources = task.$_source;
        if (!sources) return;

        if (usedTasks[task.id]) {
          on_circ_dependency_find(prevLink, links, usedTasks, skippedLinks);
        }

        usedTasks[task.id] = true;

        var targets = {};

        for (var i = 0; i < sources.length; i++) {
          if (skippedLinks[sources[i]]) continue;
          var curLink = links[sources[i]];
          var targetTask = tasks[curLink._target];
          if (targets[targetTask.id]) {
            // two link from one task to another
            on_circ_dependency_find(curLink, links, usedTasks, skippedLinks);
          }
          targets[targetTask.id] = true;
          clear_circ_dependencies(targetTask, links, tasks, usedTasks, skippedLinks, curLink);
        }
        usedTasks[task.id] = false;
      }

      function on_circ_dependency_find(link, links, usedTasks, skippedLinks) {
        if (link) {
          if (gantt.callEvent("onExportCircularDependency", [link.id, link])) {
            delete links[link.id];
          }

          delete usedTasks[link._source];
          delete usedTasks[link._target];
          skippedLinks[link.id] = true;
        }
      }

      function make_links_same_level(link, tasks) {
        var task,
          targetLvl,
          linkT = {
            target: tasks[link.target],
            source: tasks[link.source],
          };

        if (linkT.target._lvl != linkT.source._lvl) {
          if (linkT.target._lvl < linkT.source._lvl) {
            task = "source";
            targetLvl = linkT.target._lvl;
          } else {
            task = "target";
            targetLvl = linkT.source._lvl;
          }

          do {
            var parent = tasks[linkT[task].parent];
            if (!parent) break;
            linkT[task] = parent;
          } while (linkT[task]._lvl < targetLvl);

          var sourceParent = tasks[linkT.source.parent],
            targetParent = tasks[linkT.target.parent];

          while (sourceParent && targetParent && sourceParent.id != targetParent.id) {
            linkT.source = sourceParent;
            linkT.target = targetParent;
            sourceParent = tasks[linkT.source.parent];
            targetParent = tasks[linkT.target.parent];
          }
        }

        link._target = linkT.target.id;
        link._source = linkT.source.id;

        if (!linkT.target.$_target) linkT.target.$_target = [];
        linkT.target.$_target.push(link.id);

        if (!linkT.source.$_source) linkT.source.$_source = [];
        linkT.source.$_source.push(link.id);
      }

      function clear_links_same_level(links, tasks) {
        for (var link in links) {
          delete links[link]._target;
          delete links[link]._source;
        }

        for (var task in tasks) {
          delete tasks[task].$_source;
          delete tasks[task].$_target;
        }
      }

      function customProjectProperties(data, config) {
        if (config && config.project) {
          for (var i in config.project) {
            if (!gantt.config.$custom_data) gantt.config.$custom_data = {};
            gantt.config.$custom_data[i] =
              typeof config.project[i] == "function"
                ? config.project[i](gantt.config)
                : config.project[i];
          }
          delete config.project;
        }
      }

      function customTaskProperties(data, config) {
        if (config && config.tasks) {
          data.data.forEach(function (el) {
            for (var i in config.tasks) {
              if (!el.$custom_data) el.$custom_data = {};
              el.$custom_data[i] =
                typeof config.tasks[i] == "function"
                  ? config.tasks[i](el, gantt.config)
                  : config.tasks[i];
            }
          });
          delete config.tasks;
        }
      }

      function exportConfig(data, config) {
        var p_name = config.name || "gantt.xml";
        delete config.name;

        gantt.config.custom = config;

        var time = gantt._getWorktimeSettings();

        var p_dates = gantt.getSubtaskDates();
        if (p_dates.start_date && p_dates.end_date) {
          var formatDate = gantt.templates.format_date || gantt.templates.xml_format;
          gantt.config.start_end = {
            start_date: formatDate(p_dates.start_date),
            end_date: formatDate(p_dates.end_date),
          };
        }

        var manual = config.auto_scheduling === undefined ? false : !!config.auto_scheduling;

        var res = {
          callback: config.callback || null,
          config: gantt.config,
          data: data,
          manual: manual,
          name: p_name,
          worktime: time,
        };
        for (var i in config) res[i] = config[i];
        return res;
      }

      function add_export_methods(gantt) {
        gantt._ms_export = {};

        gantt.exportToMSProject = function (config) {
          config = config || {};
          config.skip_circular_links =
            config.skip_circular_links === undefined ? true : !!config.skip_circular_links;

          var oldXmlFormat = this.templates.xml_format;
          var oldFormatDate = this.templates.format_date;
          var oldXmlDate = this.config.xml_date;
          var oldDateFormat = this.config.date_format;

          var exportServiceDateFormat = "%d-%m-%Y %H:%i:%s";

          this.config.xml_date = exportServiceDateFormat;
          this.config.date_format = exportServiceDateFormat;
          this.templates.xml_format = this.date.date_to_str(exportServiceDateFormat);
          this.templates.format_date = this.date.date_to_str(exportServiceDateFormat);
          var data = this._serialize_all();

          customProjectProperties(data, config);

          customTaskProperties(data, config);

          if (config.skip_circular_links) {
            clear_rec_links(data);
          }

          config = exportConfig(data, config);

          this._send_to_export(config, config.type || "msproject");
          this.config.xml_date = oldXmlDate;
          this.config.date_format = oldDateFormat;
          this.templates.xml_format = oldXmlFormat;
          this.templates.format_date = oldFormatDate;

          this.config.$custom_data = null;
          this.config.custom = null;
        };

        gantt.exportToPrimaveraP6 = function (config) {
          config.type = "primaveraP6";
          return gantt.exportToMSProject(config);
        };

        function sendImportAjax(config) {
          var url = config.server || apiUrl;
          var store = config.store || 0;
          var formData = config.data;
          var callback = config.callback;

          var settings = { server: url };
          if (config.durationUnit) settings.durationUnit = config.durationUnit;
          if (config.projectProperties) settings.projectProperties = config.projectProperties;
          if (config.taskProperties) settings.taskProperties = config.taskProperties;

          formData.append("type", config.type || "msproject-parse");
          formData.append("data", JSON.stringify(settings));

          if (store) formData.append("store", store);

          var xhr = new XMLHttpRequest();
          xhr.onreadystatechange = function (e) {
            if (xhr.readyState == 4 && xhr.status == 0) {
              // network error
              if (callback) {
                callback(null);
              }
            }
          };

          xhr.onload = function () {
            var fail = xhr.status > 400;
            var info = null;

            if (!fail) {
              try {
                info = JSON.parse(xhr.responseText);
              } catch (e) { }
            }

            if (callback) {
              callback(info);
            }
          };

          xhr.open("POST", url, true);
          xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
          xhr.send(formData);
        }

        function nodejsImportMSP(config) {
          const http = require("http");
          var FormData = require("form-data");

          var url = config.server || apiUrl;

          var parts1 = url.split("://")[1];
          var parts2 = parts1.split("/")[0].split(":");
          var parts3 = parts1.split("/");

          var hostname = parts2[0];
          var port = parts2[1] || 80;
          var path = "/" + parts3.slice(1).join("/");

          const options = {
            hostname: hostname,
            port: port,
            path: path,
            method: "POST",
            headers: {
              "X-Requested-With": "XMLHttpRequest",
            },
          };

          var settings = {};
          if (config.durationUnit) settings.durationUnit = config.durationUnit;
          if (config.projectProperties) settings.projectProperties = config.projectProperties;
          if (config.taskProperties) settings.taskProperties = config.taskProperties;

          var formData = new FormData();
          formData.append("file", config.data);
          formData.append("type", config.type || "msproject-parse");
          formData.append("data", JSON.stringify(settings), options);

          options.headers["Content-Type"] = formData.getHeaders()["content-type"];

          const req = http.request(options, function (res) {
            let resData = "";
            res.on("data", function (d) {
              resData += d;
            });
            res.on("end", function (d) {
              config.callback(resData.toString());
            });
          });

          req.on("error", function (error) {
            console.error(error);
          });
          formData.pipe(req);
        }

        gantt.importFromMSProject = function (config) {
          var formData = config.data;

          try {
            if (formData instanceof FormData) {
            } else if (formData instanceof File) {
              var data = new FormData();
              data.append("file", formData);
              config.data = data;
            }
          } catch (error) { }
          if (gantt.env.isNode) {
            nodejsImportMSP(config);
          } else {
            sendImportAjax(config);
          }
        };

        gantt.importFromPrimaveraP6 = function (config) {
          config.type = "primaveraP6-parse";
          return gantt.importFromMSProject(config);
        };
      }

      add_export_methods(gantt);
      try {
        if (window && window.Gantt && Gantt.plugin) {
          Gantt.plugin(add_export_methods);
        }
      } catch (error) { }
    })();
  };

  shouldComponentUpdate(nextProps, nextState) {
    return true;
  };

  handleDialogClose(event, name) {
    this.setState({
      [name]: false,
      editMilestoneData: {},
      taskTypeTitle: "Task",
    });
  };

  handleDialog(event, name, id) {
    this.setState({ [name]: true, addTaskActive: id });
  };

  handleLinkDelete = () => {
    this.setState({ deleteBtnQuery: "progress" }, () => {
      this.gantt.deleteLink(this.state.deleteLinkId);
      // this.props.deleteTasksLink(this.state.deleteLinkId, () => {
      //   // this.gantt.deleteLink(this.state.deleteLinkId);
      //   this.setState({ deleteLinkDialog: false, deleteBtnQuery: "" });
      // });
    });
  };

  handleLinkDialogClose = () => {
    this.setState({ deleteLinkDialog: false });
  };

  projectEnd = () => {
    const filterGanttData = this.props.ganttTaskData.data.filter((ele) => {
      if (ele.plannedstartdate) {
        return ele;
      }
    });
    let dates = [];
    filterGanttData.forEach((task) => {
      dates.push(
        // new Date(moment(task.plannedstartdate).clone().add(task.plannedDuration, "days").format())
        new Date(moment(task.plannedenddate).add(1, "days").format())
      );
    });
    let sorted = dates.sort(this.sortDates);

    return sorted[sorted.length - 1];
  };

  sortDates = (a, b) => {
    return a.getTime() - b.getTime();
  };

  ganttMarker = () => {
    let gantt = this.gantt;
    let dateToStr = gantt.date.date_to_str("%F %j, %Y");
    let today = new Date();

    gantt.addMarker({
      start_date: today,
      css: "today",
      text: "Today",
      title: "Today: " + dateToStr(today),
      id: "todayMarker",
    });

    if (this.projectEnd()) {
      gantt.addMarker({
        start_date: this.projectEnd(),
        text: "Project end",
        id: "projectEndMarker",
      });
    }
  };

  renderEmptyState = () => {
    let emptyState = document.createElement("div");
    let t1 = '</p><p class="emptyStateDesc">';
    let t2 = "<br />";
    let t3 = '<span class="emptyStateAddBtn">+</span>';
    emptyState.classList.add("EmptyStateCnt");
    emptyState.innerHTML =
      `<div class="emptyStateArrow">${EmptyStateArrow}</div><div class="emptyStateContentCnt"><svg id="Icon" xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 100 100">

    <circle style="fill: #fafafa;" cx="50" cy="50" r="50"/>
    <path id="Icon_Tasks" style="fill: #e2e2e2;" data-name="Icon Tasks" class="cls-2" d="M420.349,393.608h-38.7a1.44,1.44,0,0,0-1.047.439,1.42,1.42,0,0,0-.441,1.041v5.92a1.418,1.418,0,0,0,.441,1.04,1.437,1.437,0,0,0,1.047.44h38.7a1.437,1.437,0,0,0,1.047-.44,1.415,1.415,0,0,0,.442-1.04v-5.92a1.418,1.418,0,0,0-.442-1.041A1.44,1.44,0,0,0,420.349,393.608Zm-1.488,5.92H403.977v-2.96h14.885v2.96h0Zm1.488-17.76h-38.7a1.437,1.437,0,0,0-1.047.44,1.418,1.418,0,0,0-.441,1.04v5.92a1.417,1.417,0,0,0,.441,1.04,1.437,1.437,0,0,0,1.047.44h38.7a1.437,1.437,0,0,0,1.047-.44,1.414,1.414,0,0,0,.442-1.04v-5.92a1.415,1.415,0,0,0-.442-1.04A1.437,1.437,0,0,0,420.349,381.768Zm-1.488,5.92H395.046v-2.96h23.815v2.96Zm2.535-17.32a1.437,1.437,0,0,0-1.047-.44h-38.7a1.437,1.437,0,0,0-1.047.44,1.42,1.42,0,0,0-.441,1.04v5.92a1.42,1.42,0,0,0,.441,1.041,1.436,1.436,0,0,0,1.047.439h38.7a1.436,1.436,0,0,0,1.047-.439,1.418,1.418,0,0,0,.442-1.041v-5.92A1.417,1.417,0,0,0,421.4,370.368Zm-2.535,5.481H409.93v-2.961h8.931v2.961Z" transform="translate(-351 -336)"/>
    <path id="Icon_Tasks_copy_3" style="fill: #c9c9c9;" data-name="Icon Tasks copy 3" class="cls-3" d="M420.349,381.768h-38.7a1.437,1.437,0,0,0-1.047.44,1.418,1.418,0,0,0-.441,1.04v5.92a1.417,1.417,0,0,0,.441,1.04,1.437,1.437,0,0,0,1.047.44h38.7a1.437,1.437,0,0,0,1.047-.44,1.414,1.414,0,0,0,.442-1.04v-5.92a1.415,1.415,0,0,0-.442-1.04A1.437,1.437,0,0,0,420.349,381.768Zm-1.488,5.92H395.046v-2.96h23.815v2.96Z" transform="translate(-351 -336)"/>
  </svg>
  <p class="emptyStateHeading">` +
      this.props.intl.formatMessage(
        {
          id: "project.gant-view.create-task.label",
          defaultMessage:
            'Create your first task</p><p class="emptyStateDesc">You do not have any tasks yet. Click <br /> "<span class="emptyStateAddBtn">+</span>" button to create a task.',
        },
        { t1, t2, t3 }
      ) +
      `</p></div>`;
    this.gantt.$grid.appendChild(emptyState);
  };

  componentWillUnmount() {
    //To cater the bug of tooltip in gantt, tooltip left displayed when gantt is unmounted
    let tooltip = document.querySelector(".gantt_tooltip");
    if (tooltip) {
      tooltip.remove();
    }
    this.gantt.destructor();
    this.gantt = null;
    window.gantt = null;
    const { allTasks, taskTodoItems } = this.props;
    if (taskTodoItems && taskTodoItems.length > 0) {
      let currentTask = allTasks.find((t) => t.id == taskTodoItems[0].taskId);
      this.updateTaskProgress(currentTask, taskTodoItems);
    }
  };

  componentDidMount() {
    let gantt = this.gantt;
    this.initGanttEvents(this.gantt);
    ganttConfig(gantt, this.UpdateTaskName); // Initializing all gantt configs
    this.ganttMarker();
    gantt.init(this.ganttContainer);
    gantt.parse(this.props.tasks);
    if (this.props.tasks.data.length <= 0) {
      this.renderEmptyState();
    }
    this.setState({ gridWidth: gantt.config.grid_width });
    this.props.dispatchGantt(this.gantt, this.props.plannedVsActual);
  };

  componentDidUpdate(prevProps, prevState) {
    const { selectedView } = this.props;
    const EmptyStateCnt = document.querySelector(".EmptyStateCnt");
    if (EmptyStateCnt && this.props.tasks.data.length > 0) {
      EmptyStateCnt.remove();
    }
    if (prevProps.location.search !== this.props.location.search) {
      // Checking if the selected project of gantt url is changed
      this.gantt.destructor(); // Destroying the existing instance of gantt chart
      this.gantt = null; // setting it's value to null to remove all empty values in object

      this.gantt = Gantt.getGanttInstance();
      window.gantt = Gantt.getGanttInstance();
      let gantt = this.gantt;
      this.initGanttEvents(this.gantt);
      ganttConfig(gantt, this.UpdateTaskName); // Initializing all gantt configs
      this.ganttMarker();




      gantt.init(this.ganttContainer);
      gantt.parse(this.props.tasks);
      if (this.props.tasks.data.length <= 0) {
        this.renderEmptyState();
      }
    }
    //This checks if current task for task details is changed or not and if changed update the state of current task in parent class, This needs to be redone
    if (JSON.stringify(prevProps.taskData) !== JSON.stringify(this.props.taskData)) {
      if (this.props.currentTask) {
        let taskData =
          this.props.taskData && this.props.taskData.data
            ? this.props.taskData.data.find((x) => x.taskId === this.props.currentTask.taskId)
            : {};
        this.props.updateCurrentTask(taskData);
      }
      // updateChecklist task if checklist is open
      else if (this.state.openChecklist) {
        let task =
          this.props.taskData && this.props.taskData.data
            ? this.props.taskData.data.find((x) => x.taskId === this.state.checkListTask.taskId)
            : null;
        this.setState({ checkListTask: task });
      }
    }
    if (this.props.plannedVsActual !== prevProps.plannedVsActual) {
      this.updateTasks();
      this.gantt.render();
    }
    // if (prevProps.ganttTaskData.data.length !== this.props.ganttTaskData.data.length) {

    // }

    if (
      JSON.stringify(prevProps.ganttTaskData.data) !==
      JSON.stringify(this.props.ganttTaskData.data) ||
      prevProps.zoom !== this.props.zoom ||
      JSON.stringify(prevProps.tasks.links) !== JSON.stringify(this.props.tasks.links)
    ) {
      let projectEndDate = this.projectEnd(); // Getting project end date
      let projectEndMarker = this.gantt.getMarker("projectEndMarker"); // Getting PRoject end date marker
      if (projectEndDate) {
        // Checking if there is project end date or not

        if (projectEndMarker) {
          projectEndMarker.start_date = projectEndDate;
          this.gantt.updateMarker("projectEndMarker");
        } else {
          let today = new Date();

          this.gantt.addMarker({
            start_date: today,
            css: "today",
            text: "Today",
            id: "todayMarker",
          });
          this.gantt.addMarker({
            start_date: projectEndDate,
            text: "Project end",
            id: "projectEndMarker",
          });
          // this.gantt.render()
        }
      }

      this.updateTasks();
    }
    if (JSON.stringify(this.props.selectedColumns) !== JSON.stringify(prevProps.selectedColumns)) {
      this.gantt.config.columns = this.hideShowColumn(columns);
      this.gantt.render();
      this.setState({ gridWidth: this.gantt.config.grid_width });
    }
    // if (
    //   prevProps.ganttExpand !== this.props.ganttExpand &&
    //   this.props.ganttExpand
    // ) {
    //   document.querySelector("#mainContentCnt").requestFullscreen();
    // } else if (
    //   prevProps.ganttExpand !== this.props.ganttExpand &&
    //   !this.props.ganttExpand
    // ) {
    //   document.exitFullscreen();
    // }
    //Switching Gantt View from dropdown
    if (selectedView == "" && prevProps.selectedView !== this.props.selectedView) {
      this.gantt.config.highlight_critical_path = false;
      this.gantt.config.show_slack = false;
      Scurve(this.gantt, this.props.selectedView, this.ganttContainer, this.props.tasks);
      this.reDrawGantt();
    } else if (
      selectedView == "Critical Path" &&
      prevProps.selectedView !== this.props.selectedView
    ) {
      this.gantt.config.highlight_critical_path = true;
      this.gantt.config.show_slack = true;
      this.reDrawGantt();
      Scurve(this.gantt, this.props.selectedView, this.ganttContainer, this.props.tasks);
    } else if (
      selectedView == "Progress Line" &&
      prevProps.selectedView !== this.props.selectedView
    ) {
      this.gantt.config.highlight_critical_path = false;
      this.gantt.config.show_slack = false;
      Scurve(this.gantt, this.props.selectedView, this.ganttContainer, this.props.tasks);
      this.reDrawGantt();
    }
  };

  updateTaskOrder = () => {
    let gantt = this.gantt;
    let tasksOrder = [];
    let i = 0;
    gantt.eachTask((task) => {
      tasksOrder.push({ taskId: task.id, index: i });
      i++;
    });
    let payload = {
      projectId: this.props.ganttTaskData.id,
      taskOrder: tasksOrder,
    };
    this.props.updateGanttTaskOrder(payload); // Action to update task order on backend
  };

  updateTasks = (task) => {
    this.gantt.batchUpdate(() => {
      var tasks = this.gantt.getTaskByTime();
      for (var i = 0; i < tasks.length; i++) {
        this.props.tasks.data.forEach((origTask) => {
          if (origTask.id === tasks[i].id) {
            var task = tasks[i];
            task.text = origTask.text;
            task.start_date = origTask.start_date ? origTask.start_date : task.start_date;
            task.end_date = origTask.start_date
              ? this.gantt.calculateEndDate(origTask.start_date, origTask.duration)
              : task.end_date;
            task.planned_start = origTask.planned_start;
            task.planned_end = origTask.planned_end;
            task.actual_start = origTask.actual_start;
            task.actual_end = origTask.actual_end;
            task.planned_end = origTask.planned_end;
            task.sortorder = origTask.sortorder;
            task.duration = origTask.duration;
            task.status = origTask.status;
            task.statusId = origTask.statusId;
            task.color = origTask.color;
            task.issues = origTask.issues;
            task.colorCode = origTask.colorCode;
            task.risks = origTask.risks;
            task.unscheduled = origTask.unscheduled;
            task.estiTime = origTask.estiTime;
            task.actTime = origTask.actTime;
            task.priority = origTask.priority;
            task.attachments = origTask.attachments;
            task.comments = origTask.comments;
            task.cost = origTask.cost;
            task.creationDate = origTask.creationDate;
            task.meetings = origTask.meetings;
            task.assignee = origTask.assignee;
            task.progress = origTask.progress;
            task.description = origTask.description;

            this.gantt.updateTask(task.id);
          }
        });
      }
    });
    this.gantt.refreshData();
  };

  reDrawGantt = () => {
    let projectEndDate = this.projectEnd(); // Getting project end date
    let projectEndMarker = this.gantt.getMarker("projectEndMarker"); // Getting PRoject end date marker
    if (projectEndDate) {
      // Checking if there is project end date or not

      if (projectEndMarker) {
        projectEndMarker.start_date = projectEndDate;
        this.gantt.updateMarker("projectEndMarker");
      } else {
        let today = new Date();

        this.gantt.addMarker({
          start_date: today,
          css: "today",
          text: "Today",
          id: "todayMarker",
        });
        this.gantt.addMarker({
          start_date: projectEndDate,
          text: "Project end",
          id: "projectEndMarker",
        });
      }
    }
    this.gantt.render();
  };

  initGanttEvents(gantt) {
    if (gantt.ganttEventsInitialized) {
      return;
    }
    gantt.templates.drag_date = null;
    gantt.templates.grid_file = function (item) {
      return "<div class='gantt_tree_icon gantt_file'></div>";
    };

    gantt.templates.grid_open = function (item) {
      return (
        "<div style='width:15px; float:left; height:100%'></div><div class='gantt_tree_icon gantt_" +
        (item.$open ? "close" : "open") +
        "'></div>"
      );
    };
    gantt.templates.grid_indent = function (task) {
      return "<div style='width:20px; float:left; height:100%'></div>";
    };
    gantt.templates.task_text = function (start, end, task) {
      return "";
    };
    gantt.templates.progress_text = (start, end, task) => {
      const { theme } = this.props;
      let progressColor = [
        { name: "Not Started", color: theme.palette.taskStatus.NotStarted },
        { name: "In Progress", color: theme.palette.taskStatus.InProgress },
        { name: "In Review", color: theme.palette.taskStatus.InReview },
        { name: "Completed", color: theme.palette.taskStatus.Completed },
        { name: "Cancelled", color: theme.palette.taskStatus.Cancelled },
      ];
      // let selectedStatus = progressColor.filter((item) => {
      //   return item.name == task.status;
      // });
      let selectedStatus =
        task.taskTemplate.statusList.find((item) => {
          return item.statusId == task.statusId;
        }) || false;
      return `<div class='taskProgressCnt' style='background-color: ${selectedStatus ? selectedStatus.statusColor : ""
        }'> ${Math.round(task.progress * 100)}% </div>`;
    };
    gantt.attachEvent("onGanttReady", () => {
      onGanttReady(gantt);
    });
    gantt.attachEvent("onGridResizeEnd", (old_width, new_width) => {
      //Setting state to force update the gantt component this is manadatory
      this.setState({ gridWidth: new_width });
      return true;
    });
    //Slack Time Component
    SlackTime(gantt);

    // set weekend
    if (this.props.ganttTaskData.calender) {
      const workingDays = this.props.ganttTaskData.calender.workingdays;
      const calendarExceptions = this.props.ganttTaskData.exceptionDates;
      for (var i = 0; i < calendarExceptions.length; i++) {
        gantt.setWorkTime({
          date: new Date(calendarExceptions[i]),
          hours: false
        });
      }
      // gantt.setWorkTime({ date: new Date('2022-12-18T06:05:00Z'), hours: false });
      [...Array(7)].map((item, index) => {
        if (workingDays.some(day => day.id == index)) {
          gantt.setWorkTime({ day: index, hours: true });// make Saturdays day-off
        } else {

          gantt.setWorkTime({ day: index, hours: false });// make Saturdays day-off
        }
      })
      // gantt.setWorkTime({ day: 0, hours: true });// make Sundays day-off
      // gantt.setWorkTime({ day: 1, hours: false });// make Mondays day-off
      // gantt.setWorkTime({ day: 2, hours: false });// make Tuesdays day-off
    }
    //   gantt.config.type_renderers[gantt.config.types.milestone]=function(task, defaultRender){
    //     let mileStoneEle =  document.createElement("div");
    //     mileStoneEle.setAttribute(gantt.config.task_attribute, task.id);
    //     let size = gantt.getTaskPosition(task);
    //     mileStoneEle.innerHTML = [
    //         '<div class="gantt_task_content"></div>' +
    //         '<div class="gantt_link_control task_left task_start_date" data-bind-property="start_date" style="height: 18px; line-height: 18px;">'+
    //         '<div class="gantt_link_point"></div>'+
    //         '</div><div class="gantt_link_control task_right task_end_date" data-bind-property="end_date" style="height: 18px; line-height: 18px;">'+
    //         '<div class="gantt_link_point"></div></div>',
    //     ].join('');
    //     mileStoneEle.className = "gantt_task_line gantt_milestone gantt_bar_milestone";
    //     mileStoneEle.style.left = size.left + "px";
    //     mileStoneEle.style.top = size.top - 3 + "px";
    //     mileStoneEle.style.width = 26 + "px";
    //     mileStoneEle.style.height = 26 + "px";

    //     return mileStoneEle;
    // };

    gantt.attachEvent("onTaskLoading", (task) => {
      if (moment(task.actual_start).isValid()) {
        task.actual_start = gantt.date.parseDate(task.actual_start, "xml_date");
        task.actual_end = gantt.date.parseDate(task.actual_end, "xml_date");
      }
      return true;
    });
    gantt.addTaskLayer((task) => {
      if (
        task.actual_start &&
        task.actual_end &&
        this.props.plannedVsActual == "Planned vs Actual" &&
        moment(task.actual_start).isValid()
      ) {
        let sizes = gantt.getTaskPosition(task, task.actual_start, moment(task.actual_end).add(1, "days"));
        let el = document.createElement("div");
        el.className = "baseline";
        el.style.left = sizes.left + "px";
        el.style.width = sizes.width + "px";
        el.style.top = sizes.top + gantt.config.task_height + 13 + "px";
        return el;
      }
      return false;
    });
    // gantt.config.sort = true;
    // here is off days settings
    gantt.templates.scale_cell_class = function (date) {
      // if (date.getDay() == 0 || date.getDay() == 6) {
      if (!gantt.isWorkTime(date)) {
        return "weekend";
      }
    };
    gantt.templates.task_cell_class = function (task, date) {
      // if (date.getDay() == 0 || date.getDay() == 6) {
      // console.log(gantt.isWorkTime({ date: date }))
      if (!gantt.isWorkTime(date)) {
        return "weekend";
      }
    };
    gantt.templates.task_class = function (start, end, task) {
      //   switch (task.status) {
      //     case "Not Started":
      //       return "notStarted";
      //       break;
      //     case "In Progress":
      //       return "inProgress";
      //       break;
      //     case "In Review":
      //       return "inReview";
      //       break;
      //     case "Completed":
      //       return "completed";
      //       break;
      //     case "Cancelled":
      //       return "cancelled";
      //       break;
      //   }
    };
    gantt.templates.rightside_text = function (start, end, task) {
      if (task.type == gantt.config.types.milestone) {
        return task.text;
      }
      return "";
    };
    gantt.attachEvent("onBeforeTaskChanged", (id, mode, task) => {
      if (mode == "move" || mode == "resize") {
        return true;
      }
      return false;
    });

    gantt.attachEvent("onTaskDrag", function (id, mode, task, original) {
      // gantt.config.auto_scheduling = false;
      let modes = gantt.config.drag_mode;
      if (mode == modes.move) {
        let diff = task.start_date - original.start_date;
        gantt.eachTask(function (child) {
          child.start_date = new Date(+child.start_date + diff);
          child.end_date = new Date(+child.end_date + diff);
          gantt.refreshTask(child.id, true);
        }, id);
      }
    });

    gantt.attachEvent("onAfterTaskDrag", (id, mode, e) => {
      let modes = gantt.config.drag_mode;
      // restrict autoscheduling
      // && this.state.autoScheduling
      if (mode == modes.move) {
        const { plannedVsActual } = this.props;
        let state = gantt.getState();
        let childs = [gantt.getTask(id)];
        gantt.eachTask((child) => {
          child.start_date = gantt.roundDate({
            date: child.start_date,
            unit: state.scale_unit,
            step: state.scale_step,
          });
          child.end_date = gantt.calculateEndDate(
            child.start_date,
            child.duration,
            gantt.config.duration_unit
          );
          gantt.updateTask(child.id);
          childs.push(child);
        }, id);
        const postObject = [];
        if (plannedVsActual == "Planned" || plannedVsActual == "Planned vs Actual") {
          childs.forEach((task) => {
            postObject.push({
              taskId: task.id,
              text: task.text,
              dueDate: task.planned_start ? moment(task.end_date).add(- 1, "days").format("MM-DD-YYYY") : null,
              startDate: task.planned_start ? moment(task.start_date).format("MM-DD-YYYY") : null,
            });
          });
        } else {
          childs.forEach((task) => {
            postObject.push({
              taskId: task.id,
              actualDueDate: task.actual_start ? moment(task.end_date).add(- 1, "days").format("MM-DD-YYYY") : null,
              actualStartDate: task.actual_start ? moment(task.start_date).format("MM-DD-YYYY") : null,
              text: task.text,
            });
          });
        }

        this.props.handleBatchTaskUpdate(postObject, () => { });
      }

      this.setState({ autoScheduling: false });
    });
    gantt.attachEvent("onBeforeAutoSchedule", function (taskId) {
      // any custom logic here
      if (taskId) {
        return true;
      } else {
        return false;
      }
    });
    // For position of tooltips
    gantt.attachEvent("onAfterAutoSchedule", (taskId, updatedTasks) => {
      this.setState({ autoScheduling: true });

      const filterTasks = this.gantt.getTaskByTime().filter((task) => {
        return updatedTasks.indexOf(task.id) > -1 || taskId == task.id;
      });

      const postObject = [];
      const { plannedVsActual } = this.props;
      filterTasks.forEach((task) => {
        if (plannedVsActual == "Planned" || plannedVsActual == "Planned vs Actual") {
          postObject.push({
            taskId: task.id,
            text: task.text,
            dueDate: task.planned_start ? moment(task.end_date).add(- 1, "days").format("MM-DD-YYYY") : null,
            startDate: task.planned_start ? moment(task.start_date).format("MM-DD-YYYY") : null,
          });
        } else {
          postObject.push({
            taskId: task.id,
            text: task.text,
            actualDueDate: task.actual_start ? moment(task.end_date).add(- 1, "days").format("MM-DD-YYYY") : null,
            actualStartDate: task.actual_start ? moment(task.start_date).format("MM-DD-YYYY") : null,
          });
        }
      });

      this.props.handleBatchTaskUpdate(postObject, () => { });
    });
    gantt.attachEvent("onBeforeTaskUpdate", (id, new_item) => {
      //any custom logic here
      return false;
    });
    gantt.attachEvent("onAfterTaskUpdate", (id, new_item) => { });

    gantt.attachEvent("onGanttReady", function () {
      let tooltips = gantt.ext.tooltips;
      tooltips.tooltip.setViewport(gantt.$task_data);
    });

    gantt.templates.tooltip_text = (start, end, task) => {
      return ToolTipCmp(start, end, task, this.props);
    };
    gantt.ext.inlineEditors.attachEvent("onBeforeEditStart", (state) => {

      return gantt.getTask(state.id).inline_allowed;
    });

    // inline progress and duration columns
    gantt.ext.inlineEditors.attachEvent("onEditStart", function (state) {
      if (state.columnName == "progress") {
        var task = gantt.getTask(state.id);
        var element = document.querySelectorAll('input[name="progress"]')[0];
        element.value = parseInt(element.value * 100);
      }
    });

    // gantt.attachEvent("onAfterAutoSchedule", (taskId, updatedTasks) => {
    gantt.ext.inlineEditors.attachEvent("onBeforeSave", (state) => {
      if (state.columnName == "progress") {
        state.newValue /= 100;
        const postObject = [{
          taskId: state.id,
          progress: state.newValue * 100
        }];
        this.props.handleBatchTaskUpdate(postObject, () => { });
        return false;
      }
      if (state.columnName == "duration") {
        // state.newValue /= 100;
        const postObject = [{
          taskId: state.id,
          duration: Number(state.newValue)
        }];
        this.props.handleBatchTaskUpdate(postObject, () => { });
        return false;
      }
    });

    gantt.attachEvent("onLinkDblClick", (id, e) => {
      this.setState({ deleteLinkId: id, deleteLinkDialog: true });
    });

    gantt.attachEvent("onAfterTaskDelete", (id) => {
      //After Deleting task there is no need to call EditTask API
      // if (this.props.onTaskUpdated) {
      //   this.props.onTaskUpdated(id, "deleted");
      // }
    });
    gantt.attachEvent("onTaskDblClick", (id, event) => {
      const targetClass = event.target.classList;
      const task = this.gantt.getTask(id);
      if (targetClass.contains("gantt_task_content") || targetClass.contains("gantt_task_drag")) {
        let taskData =
          this.props.taskData && this.props.taskData.data
            ? this.props.taskData.data.find((x) => x.taskId === id)
            : {};
        if (taskData && task.type == "task") {
          this.props.taskDetailsOpen(taskData);
        } else {
          this.setState({
            taskDialog: true,
            editMilestoneData: { task, dialogType: "editMilestone" },
          });
        }
      } else if (
        targetClass.contains("gantt_link_point") ||
        targetClass.contains("gantt_line_wrapper")
      ) {
        return true;
      } else {
        return false;
      }
    });
    gantt.attachEvent("onAfterLinkAdd", (id, link) => {
      this.props.onLinkUpdated(id, link);
    });
    gantt.attachEvent("onAfterLinkDelete", (id, link) => {
      //any custom logic here
      // this.props.deleteTasksLink(id, () => { 
      //   this.setState({ deleteLinkDialog: false, deleteBtnQuery: "" });
      // });
      this.props.deleteTasksLink(id, () => {
        this.setState({ deleteLinkDialog: false, deleteBtnQuery: "" });
      });
      // console.log('remove', id, link)
    });
    gantt.attachEvent("onGridHeaderClick", (id, e) => {
      e.stopPropagation();
      let addTaskTarget = e.target.closest(".taskAddIcon");
      if (addTaskTarget) {
        this.handleDialog(e, "taskDialog", "");
      }
    });
    // When task row is re-ordered

    gantt.attachEvent("onRowDragEnd", (id, tindex) => {
      const task = this.gantt.getTask(id);
      this.props.onTaskUpdated({
        id: task.id,
        text: task.text,
        type: task.type,
        parent: task.parent,
      });
      this.updateTaskOrder();
    });

    gantt.attachEvent("onTaskClick", (id, e) => {
      // //any custom logic here
      e.stopPropagation();
      let addTaskTarget = e.target.closest(".taskAddIcon");
      let checklistTarget = e.target.closest(".checklistIcon");
      let taskDetailsButton = e.target.closest(".taskExpandIconCnt");
      let target = e.target.parentNode.classList.contains("round");
      let inputChecked = e.target.parentNode.querySelector("input");

      if (addTaskTarget) {
        // alert("Add your task")
        this.handleDialog(e, "taskDialog", id);

        return false;
      } else if (checklistTarget) {
        // Checking if the target is checklist button
        let taskData =
          this.props.taskData && this.props.taskData.data
            ? this.props.taskData.data.find((x) => x.taskId === id)
            : {};
        if (taskData) {
          this.props.getTaskComments(
            { taskId: id, todoTaskId: taskData.id },
            (response) => {
              if (response && response.status === 200) {
                this.setState({
                  openChecklist: true,
                  checkListTask: taskData,
                  todoListItems: response.data.toDoList,
                });
              }
            },
            (error) => {
              if (error) {
                this.setState({ openChecklist: true, checkListTask: taskData });
              }
            }
          );
          // this.setState({ openChecklist: true, checkListTask: taskData });
        }
      } else if (target) {
        if (!inputChecked.checked) {
          inputChecked.checked = true;
          this.props.handleTaskComplete(id);
          return true;
        } else {
          inputChecked.checked = false;
          this.props.handleTaskInComplete(id);
          return true;
        }
      } else if (taskDetailsButton) {
        let taskData =
          this.props.taskData && this.props.taskData.data
            ? this.props.taskData.data.find((x) => x.taskId === id)
            : {};
        if (taskData) {
          this.props.taskDetailsOpen(taskData);
        }
      }
      return true;
    });

    gantt.config.columns = this.hideShowColumn(columns, this.props.selectedView);
    gantt.attachEvent("onColumnResizeEnd", (index, column, new_width) => {
      const gantt = this.gantt;
      this.setState({
        gridWidth: gantt.config.grid_width - column.width + new_width,
      });

      return true;
    });
  };

  UpdateTaskName = (task) => {
    const { plannedVsActual } = this.props;
    if (plannedVsActual == "Planned" || plannedVsActual == "Planned vs Actual") {
      this.props.onTaskUpdated({
        id: task.id,
        text: task.text,
        type: task.type,
        plannedDuration: task.duration,
        status: task.status,
        plannedstartdate: task.planned_start ? moment(task.start_date).format("DD-MM-YYYY") : null,
        sortorder: this.gantt.getGlobalTaskIndex(task.id),
        parent: task.parent,
        description: task.type == "milestone" && task.description,
      });
    } else {
      this.props.onTaskUpdated({
        id: task.id,
        text: task.text,
        type: task.type,
        duration: task.duration,
        status: task.status,
        start_date: task.actual_start ? moment(task.start_date).format("DD-MM-YYYY") : null,
        sortorder: this.gantt.getGlobalTaskIndex(task.id),
        parent: task.parent,
        description: task.type == "milestone" && task.description,
      });
    }
  };

  hideShowColumn = (columns) => {
    let newCols = columns.filter((item) => {
      return (
        this.props.selectedColumns.indexOf(item.value) > -1 ||
        item.value == " " ||
        fixedColumns.indexOf(item.name) > -1
      );
    });
    newCols.forEach((col) => {
      if (this.props.selectedColumns.indexOf(col.value) > -1) {
        col.hide = false;
      }
      col.label = this.getTranslatedColumn(col.label);
    });
    return newCols;
  };

  getTranslatedColumn = (label) => {
    switch (label) {
      case "Task Name":
        label = this.props.intl.formatMessage({
          id: "project.gant-view.columns.task-name",
          defaultMessage: "Task Name",
        });
        break;
      case "Actual Effort":
        label = this.props.intl.formatMessage({
          id: "project.gant-view.columns.actual-effort",
          defaultMessage: "Actual Effort",
        });
        break;
      case "Attachments":
        label = this.props.intl.formatMessage({
          id: "project.gant-view.columns.attachment",
          defaultMessage: "Attachments",
        });
        break;
      case "Assignees":
        label = this.props.intl.formatMessage({
          id: "project.gant-view.columns.assignees",
          defaultMessage: "Assignees",
        });
        break;
      case "Comments":
        label = this.props.intl.formatMessage({
          id: "common.comment.label",
          defaultMessage: "Comments",
        });
        break;
      case "Status":
        label = this.props.intl.formatMessage({
          id: "common.status.label",
          defaultMessage: "Status",
        });
        break;
      case "Priority":
        label = this.props.intl.formatMessage({
          id: "project.gant-view.columns.priority",
          defaultMessage: "Priority",
        });
        break;
      case "Act. End Date":
        label = this.props.intl.formatMessage({
          id: "project.gant-view.columns.act-end-date",
          defaultMessage: "Act. End Date",
        });
        break;
      case "Act. Start Date":
        label = this.props.intl.formatMessage({
          id: "project.gant-view.columns.act-start-date",
          defaultMessage: "Act. Start Date",
        });
        break;
      case "Plan. Start Date":
        label = this.props.intl.formatMessage({
          id: "project.gant-view.columns.plan-start-date",
          defaultMessage: "Plan. Start Date",
        });
        break;
      case "Plan. End Date":
        label = this.props.intl.formatMessage({
          id: "project.gant-view.columns.plan-end-date",
          defaultMessage: "Plan. End Date",
        });
        break;
      case "Cost":
        label = this.props.intl.formatMessage({
          id: "project.gant-view.columns.cost",
          defaultMessage: "Cost",
        });
        break;
      case "Creation Date":
        label = this.props.intl.formatMessage({
          id: "project.gant-view.columns.creation-date",
          defaultMessage: "Creation Date",
        });
        break;
      case "Risks":
        label = this.props.intl.formatMessage({
          id: "risk.label",
          defaultMessage: "Risks",
        });
        break;
      case "Meetings":
        label = this.props.intl.formatMessage({
          id: "meeting.label",
          defaultMessage: "Meetings",
        });
        break;
      case "Issues":
        label = this.props.intl.formatMessage({
          id: "issue.label",
          defaultMessage: "Issues",
        });
        break;
    }
    return label;
  };

  toggleColumns = () => {
    if (this.gantt.config.show_grid) {
      this.gantt.config.show_grid = false;
      this.setState({ showGrid: false });
    } else {
      this.gantt.config.show_grid = true;
      this.setState({ showGrid: true });
    }
    // Toggle function to expand and collapse extra columns
    // let showDetails = this.state.showDetails;
    // if (!showDetails) {
    //   this.gantt.config.grid_width = 650;
    //   this.setState({ showDetails: true }, () => {
    //     this.showSelectedColumn();
    //   });
    // } else {
    //   this.gantt.config.grid_width = 450;
    //   this.setState({ showDetails: false });

    //   this.gantt.getGridColumns().map(col => {
    //     if (
    //       (col.label !== " " && fixedColumns.indexOf(col.name) < 0) ||
    //       col.name == "columnSelect"
    //     ) {
    //       col.hide = true;
    //     }
    //   });
    // }
    this.gantt.render();
  };

  closeCheckList = () => {
    const { taskTodoItems, allTasks } = this.props;
    this.props.getGanttTasks(this.props.projectId, () => {
      this.setState({ openChecklist: false });
    });

    if (taskTodoItems && taskTodoItems.length > 0) {
      let currentTask = allTasks.find((t) => t.id == taskTodoItems[0].taskId);
      this.updateTaskProgress(currentTask, taskTodoItems);
    }
  };

  updateTaskProgress = (currentTask, totalItems) => {
    // const totalItems = props.todoItems.length;
    const cmpltdItems = totalItems.filter((item) => item.isComplete).length;
    const progressPercent = Math.round((cmpltdItems / totalItems.length) * 100) || 0;
    const newTaskObj = { ...currentTask, progress: progressPercent };

    this.props.UpdateTaskProgressBar(newTaskObj, () => { });
  };

  handleTaskType = (value) => {
    this.setState({ taskTypeTitle: value });
  };

  updateComments = (items = []) => {
    // this.setState({ todoListItems: items });
  };

  render() {
    setZoom(this.props.zoom, this.gantt);
    const {
      addTaskAction,
      projectId,
      plannedVsActual,
      taskData,
      currentTask,
      taskTodoItems,
      intl,
      getTaskPer,
      permissionObject,
      permissions,
    } = this.props;
    const {
      taskDialog,
      openChecklist,
      addTaskActive,
      deleteLinkDialog,
      gridWidth,
      editMilestoneData,
      taskTypeTitle,
      todoListItems,
    } = this.state;
    const gridShow = this.gantt.config.show_grid;
    const currentTaskChildren =
      currentTask &&
      this.props.tasks.data.filter((t) => {
        return t.parent == currentTask.taskId;
      });
    let checkListTask = getTaskPer(this.state.checkListTask);
    const permissionTask =
      checkListTask &&
        checkListTask.projectId /** if task link with project then assign project permissions */
        ? isUndefined(permissionObject[checkListTask.projectId])
          ? permissions
          : permissionObject[checkListTask.projectId].task
        : permissions; /** workspace level permissions */
    return (
      <>
        <GanttToDoDrawer
          open={openChecklist}
          closeDrawer={this.closeCheckList}
          headerTitle={checkListTask ? checkListTask.taskTitle : ""}>
          {openChecklist && (
            // <CheckList
            //   task={checkListTask}
            //   markAllAsChecked={() =>
            //     this.props.handleTaskComplete(checkListTask.taskId)
            //   }
            // />
            // <Scrollbars style={{ height: "100%" }}>
            <TodoList
              currentTask={checkListTask}
              todoItems={taskTodoItems}
              updateTodoItems={this.updateComments}
              permission={this.props.permissions}
              taskPer={permissionTask}
              intl={intl}
            />
            // </Scrollbars>
          )}
        </GanttToDoDrawer>

        <DefaultDialog
          title={
            editMilestoneData && editMilestoneData.dialogType == "editMilestone" ? (
              <FormattedMessage
                id="project.gant-view.edit-milestone.label"
                defaultMessage="Edit Milestone"
              />
            ) : taskTypeTitle === "Task" ? (
              <FormattedMessage
                id="project.gant-view.task-dialog.title"
                defaultMessage="Add Task"
              />
            ) : (
              <FormattedMessage
                id="project.gant-view.milestone-dialog.title"
                defaultMessage="Add Milestone"
              />
            )
          }
          open={taskDialog}
          onClose={(event) => this.handleDialogClose(event, "taskDialog")}>
          <AddNewTaskForm
            taskType={this.handleTaskType}
            addTaskAction={addTaskAction}
            addTaskActive={addTaskActive}
            plannedVsActual={plannedVsActual}
            gantt={this.gantt}
            updateTaskOrder={this.updateTaskOrder}
            projectId={projectId}
            editMilestoneData={editMilestoneData}
            closeAction={(event) => this.handleDialogClose(event, "taskDialog")}
            updateTask={this.props.handleBatchTaskUpdate}
          />
        </DefaultDialog>
        <div
          ref={(input) => {
            this.ganttContainer = input;
          }}
          style={{ width: "100%", height: "100%" }}
        />
        {this.state.showGrid ? (
          <GanttColumnsDropDown
            selectedColumns={this.props.selectedColumns}
            projectId={projectId}
            handleGanttColumn={this.props.handleGanttColumn}
            style={{
              position: "absolute",
              zIndex: 11111,
              top: 12,
              left: gridShow ? gridWidth - 28 : 0,
            }}
          />
        ) : null}

        {/* Checking if gantt is rendered */}
        {this.gantt.$container ? (
          <div onClick={this.toggleColumns}>
            {this.state.showGrid ? (
              <SvgIcon
                viewBox="0 0 17 49"
                style={{
                  position: "absolute",
                  zIndex: 11111,
                  top: this.gantt.$container.clientHeight / 2 + 38,
                  left: gridShow ? gridWidth : 0,
                  cursor: "pointer",
                  fontSize: "52px",
                  background: "transparent",
                  width: "auto",
                  width: 17,
                  height: 52,
                }}>
                <HideColumnsIcon />
              </SvgIcon>
            ) : (
              <SvgIcon
                viewBox="0 0 17 52"
                style={{
                  position: "absolute",
                  zIndex: 11111,
                  top: this.gantt.$container.clientHeight / 2 + 38,
                  left: gridShow ? gridWidth : 0,
                  cursor: "pointer",
                  fontSize: "52px",
                  background: "transparent",
                  width: 17,
                  height: 52,
                }}>
                <ShowColumnsIcon />
              </SvgIcon>
            )}
          </div>
        ) : null}

        {this.props.showTaskDetails ? (
          <>
            {this.props.taskDetailDialogState(null, {
              id: currentTask.taskId,
              onDeleteTask: this.props.onDeleteTask,
              isGantt: true,
              GanttCmp: this.gantt,
              childTasks: currentTaskChildren,
              afterCloseCallBack: (updatedTask) => {
                if (!updatedTask.current.projectId) {
                  /** if user have removed projectId from task */
                  const childIds = currentTaskChildren && currentTaskChildren.map((t) => t.id);
                  this.props.onDeleteTask(updatedTask.current.taskId, this.gantt, childIds, false);
                }
                this.props.closeTaskDetailsPopUp(this.gantt);
              },
            })}
          </>
        ) : null}
        <DeleteConfirmDialog
          open={deleteLinkDialog}
          closeAction={this.handleLinkDialogClose}
          cancelBtnText={
            <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
          }
          successBtnText={
            <FormattedMessage id="common.action.delete.label" defaultMessage="Delete" />
          }
          alignment="center"
          headingText={<FormattedMessage id="common.action.delete.label" defaultMessage="Delete" />}
          successAction={this.handleLinkDelete}
          msgText={
            <FormattedMessage
              id="project.gant-view.delete-link.action"
              defaultMessage="Are you sure you want to delete this link?"
            />
          }
          btnQuery={this.state.deleteBtnQuery}
        />
      </>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    permissions: state.workspacePermissions.data.task,
    taskTodoItems: state.todoItemsData.data.todoItems,
    allTasks: state.tasks.data || [],
    permissionObject: ProjectPermissionSelector(state),
  };
};

export default compose(
  injectIntl,
  withStyles({}, { withTheme: true }),
  connect(mapStateToProps, {
    getTaskComments,
    getGanttTasks,
    UpdateTaskProgressBar,
    taskDetailDialogState,
  })
)(GanttComponent);
