const gantStyles = theme => ({
  ganttHeaderCnt: {
    display: "flex",
    padding: "0 20px 20px 0",
    alignItems: "center"
  },
  ganttToolBarHeading: {
    flex: 1
  },
  toolbarCnt: {
    width: 900,
    display: "flex",
    background: theme.palette.common.white,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    padding: "10px 0",
    "& > div": {
      background: "transparent",
      padding: "0 10px",
      borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
      flex: 1
    },
    "& > div:first-child": {
      borderLeft: "none",
      flex: "none",
      width: 160
    },
    "& > div:last-child": {
      borderRight: "none",
      width: 80,
      flex: "none"
    },
    "& $zoomCnt": {
      flex: "none"
    }
  },
  ganttExtraBtnsCnt: {
    position: "absolute",
    bottom: 210,
    zIndex: 1,
    right: 25
  },
  viewDropdownBtn: {
    padding: 0,
    border: "none",
    boxShadow: "none",
    background: "none",
    minWidth: 54,
    height: 34,
    padding: 5
  },
  circularProgress: {
    width: "20px !important",
    height: "20px !important",
    marginRight: 5,
  },
  ganttHeaderToolbarTier: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
    "& > div": {
      textAlign: "center"
    }
  },
  projectStartEndCnt: {},
  addTaskInnerCnt: {
    flex: 1,
    padding: "0 10px",
    cursor: "pointer",
    "& $toolbarIcon": {
      marginRight: 5
    }
  },
  addMilestoneInnerCnt: {
    flex: 1,
    padding: "0 10px",
    borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
    cursor: "pointer",
    "& $toolbarIcon": {
      marginRight: 5
    }
  },
  toolbarIcon: {},
  greenCount: {
    color: theme.palette.text.sucess
  },
  redCount: {
    color: theme.palette.error.main
  },
  blackCount: {
    color: theme.palette.text.primary
  },
  orangeCount: {
    color: theme.palette.text.orange
  },
  costCnt: {
    "& $smallHeading": {
      display: "flex"
    }
  },
  zoomCnt: {
    padding: "8px 30px !important",
    flex: "none",
    width: 150
  },
  smallHeading: {
    textTransform: "capitalize",
    fontSize: "10px !important"
  },
  sliderHeading: {
    textTransform: "uppercase"
  },
  projectManagerName: {
    textTransform: "capitalize",
    marginLeft: 5,
    color: theme.palette.text.primary
  },
  statusMenuItemSelected: {
    background: `transparent !important`,
    "&:hover": {
      background: `transparent !important`
    }
  },
  statusItemText: {
    fontSize: "12px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightRegular
  },
  listItemIcon: {
    marginRight: 0,
    minWidth: "0px"
  },
  textWithIcon: {
    paddingLeft: 8
  },
  TotalAssignee: {
    background: theme.palette.secondary.lightBlue,
    color: theme.palette.secondary.main,
    width: 30,
    height: 30
  },
  assigneeTier: {
    flex: "none !important",
    width: 160
  },
  projectStartEndDates: {
    color: theme.palette.text.medGray,
    fontWeight: theme.typography.fontWeightRegular
  },
  dropdownArrow: {
    fontSize: "24px !important"
  },
  viewIcon: {
    fontSize: "20px !important"
  },
  addNewRoleMenuItem: {
    background: theme.palette.background.items,
    borderTop: `1px solid ${theme.palette.border.lightBorder}`
  },
  addNewRoleMenuItemText: {
    padding: "0 4px",
    fontSize: "12px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightRegular
  },
  zoomInCnt: {
    background: theme.palette.common.white,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "50%",
    padding: "5px 10px 3px 10px",
    cursor: "pointer",
    height: 36,
    width: 36,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 10,
    "&:hover": {
      background: "rgba(0, 0, 0, 0.08)"
    }
  },
  zoomOutCnt: {
    background: theme.palette.common.white,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "50%",
    padding: "5px 10px 3px 10px",
    cursor: "pointer",
    height: 36,
    width: 36,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 10,
    "&:hover": {
      background: "rgba(0, 0, 0, 0.08)"
    }
  },
  fullScreenBtn: {
    background: theme.palette.common.white,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "50%",
    padding: "5px 10px 3px 10px",
    cursor: "pointer",
    height: 36,
    width: 36,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",

    "&:hover": {
      background: "rgba(0, 0, 0, 0.08)"
    }
  },
  zoomOutIcon: {
    fontSize: "18px !important"
  },
  zoomInIcon: {
    fontSize: "18px !important"
  },
  fullScreenIcon: {
    fontSize: "16px !important"
  },
});

export default gantStyles;
