import React, { useState, useEffect } from "react";
import { withSnackbar } from "notistack";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import combinedStyles from "../../../utils/mergeStyles";
import { withStyles } from "@material-ui/core/styles";
import styles from "./styles";
import dialogStyles from "../../../assets/jss/components/dialog";

import Header from "../ProjectDetail/Header/Header";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { statusData } from "../../../helper/projectDropdownData";
import ButtonActionsCnt from "../../../components/Dialog/ConfirmationDialogs/ButtonActionsCnt";
import ProjectDescription from "../ProjectDetail/Details/ProjectDescription";
import isEmpty from "lodash/isEmpty";

import { saveNewProject, projectDetails } from "../../../redux/actions/projects";
import { grid } from "../../../components/CustomTable2/gridInstance";
import { CanAccessFeature } from "../../../components/AccessFeature/AccessFeature.cmp";
import mixpanel from 'mixpanel-browser';
import { MixPanelEvents } from '../../../mixpanel';

function CreateNewProject(props) {
  const { theme, classes, open = false, handleDialogClose, intl } = props;

  const [projectStatus, setProjectStatus] = useState({});
  const handleStatusSelect = status => {
    /** function call when user select the status of the project  */
    if (status) {
      setProjectStatus(status);
    }
  };

  const [projectStatusData, setProjectStatusData] = useState([]);

  useEffect(() => {
    const { theme, classes, intl } = props;
    const projectStatusData = statusData(
      theme,
      classes,
      intl
    ); /** geeting project status drop down data and setting into state */
    setProjectStatusData(projectStatusData);
    setProjectStatus(projectStatusData[0]);
    mixpanel.time_event(MixPanelEvents.ProjectCreationEvent);
  }, []);

  const [projectTitle, setProjectTitle] = useState("");
  const handleTextFieldChange = event => {
    /** function call when user enter project title */
    setProjectTitle(event.target.value);
    setError({
      errorState: false,
      errorMessage: "",
    });
  };

  const [error, setError] = useState({
    errorState: false,
    errorMessage: "",
  });

  const [selectedColor, setSelectedColor] = useState("#F47373");
  const colorChange = color => {
    /** function call when user select/change project color  */
    setSelectedColor(color);
  };

  const [btnQuery, setBtnQuery] = useState("");

  const handleSubmit = () => {
    /** function call when user clicks create project button */
    if (isEmpty(projectTitle)) {
      setError({
        errorState: true,
        errorMessage: intl.formatMessage({
          id: "project.creation-dialog.title-required",
          defaultMessage: "Oops! Please enter project title.",
        }),
      });
    } else {
      setBtnQuery("progress");
      let data = {
        projectName: projectTitle.replace(/\s\s+/g, " "),
        description: projectDescription,
        status: projectStatus.value,
        colorCode: selectedColor,
        boardColorCode:
          "rgb(" +
          Math.round(Math.random() * 220) +
          "," +
          Math.round(Math.random() * 210) +
          "," +
          Math.round(Math.random() * 220) +
          ")",
      };
      props.saveNewProject(
        data,
        success => {
          if (success && success.data) {
            mixpanel.track(MixPanelEvents.ProjectCreationEvent, success);
            props.projectDetails({ projectDetails: success.data });
            setBtnQuery("");
            showSnackBar("Hurrah! Project created successfully", "success");
            props.handleDialogClose({}, "projectDialog");
            setTimeout(() => {
              grid.grid && grid.grid.redrawRows();
            }, 0);
          }
        },
        err => {
          setBtnQuery("");
          if (err && err.data) showSnackBar(err.data.message, "error");
        }
      );
    }
  };

  const [projectDescription, setProjectDescription] = useState("");
  const handleDescriptionInput = e => {
    /** function call when user enter project description */
    if (e) {
      setProjectDescription(e);
    }
  };

  const showSnackBar = (snackBarMessage, type) => {
    /** showing snack bar if case of any error/ success scenario  */
    const { classes, enqueueSnackbar } = props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type || "info",
      }
    );
  };
  return (
    <>
      <Dialog
        open={open}
        disableBackdropClick={true}
        onClose={event => props.handleDialogClose(event, "projectDialog")}
        // onClick={e => {
        //   e.stopPropagation();
        // }}
        PaperProps={{
          style: {
            maxWidth: 650,
            minHeight: 708,
          },
        }}
        classes={{
          paper: classes.dialogPaperCnt,
          scrollBody: classes.dialogCnt,
        }}
        fullWidth={true}
        scroll="body">
        <DialogTitle id="form-dialog-title" classes={{ root: classes.defaultDialogTitle }}>
          <Header /** header component for the dialog containing the title, status, color etc modules */
            detailView={false}
            projectTitle={projectTitle}
            error={error}
            handleTextFieldChange={handleTextFieldChange}
            onKeyDownProjectName={() => {}}
            handleProjectNameClickAway={() => {}}
            projectStatusData={projectStatusData}
            selectedStatus={projectStatus}
            handleStatusSelect={handleStatusSelect}
            selectedColor={selectedColor}
            colorChange={colorChange}
            handleDialogClose={handleDialogClose}
            editProjectName={true}
            intl={intl}
          />
        </DialogTitle>
        <DialogContent classes={{ root: classes.defaultDialogContent }}>
        <CanAccessFeature group='project' feature='description'>
          <div className={classes.dialogContent}>
            <ProjectDescription
              permission={true}
              setProjectDescription={handleDescriptionInput}
              projectDescription={projectDescription}
              projectId={null}
            />
          </div>
        </CanAccessFeature>
          <div className={classes.buttonCnt}>
            <ButtonActionsCnt
              cancelAction={event => props.handleDialogClose(event, "projectDialog")}
              successAction={handleSubmit}
              successBtnText={intl.formatMessage({
                id: "project.creation-dialog.create-button.label",
                defaultMessage: "Create Project",
              })}
              cancelBtnText={intl.formatMessage({
                id: "common.action.cancel.label",
                defaultMessage: "Cancel",
              })}
              btnType="success"
              btnQuery={btnQuery}
              deleteBtnText={""}
              deleteAction={() => {}}
              btnTypeDelete="plain"
              btnTypeVariant="outlined"
              disabled={projectTitle == "" ? true : false}
            />
          </div>
        </DialogContent>
      </Dialog>
    </>
  );
}

const mapStateToProps = state => {
  return {};
};

export default compose(
  withRouter,
  withSnackbar,
  withStyles(combinedStyles(styles, dialogStyles), { withTheme: true }),
  connect(mapStateToProps, {
    saveNewProject,
    projectDetails,
  })
)(CreateNewProject);
