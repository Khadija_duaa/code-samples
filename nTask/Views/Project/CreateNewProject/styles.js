const projectStyles = (theme) => ({
  dialogContent: {
    padding: 20,
  },
  buttonCnt: {
    position: "absolute",
    bottom: 0,
    width: "100%",
  },
  statusIcon: {
    fontSize: "18px !important",
  },
});

export default projectStyles;
