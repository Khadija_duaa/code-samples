// @flow

import React, { useState, useEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import SvgIcon from "@material-ui/core/SvgIcon";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import isNaN from "lodash/isNaN";
import { FormattedMessage } from "react-intl";
import styles from "./style";

import TaskIcon from "../../../../components/Icons/TaskIcon";
import TimeIcon from "../../../../components/Icons/IconTime";
import ResourcesIcon from "../../../../components/Icons/ResourcesIcon";
import CostIcon from "../../../../components/Icons/CostIcon";
import ApproveIconfrom from "../../../../components/Icons/ApprovedTimeIcon";
import BillingIcon from "../../../../components/Icons/BillingMethodIcon";
import DefaultCheckbox from "../../../../components/Form/Checkbox";
import ArrowIncreasedRed from "../../../../components/Icons/ArrowIncreasedRed";

import {
  currencyData,
  billingMethodData,
  feeMethodData,
} from "../../../../helper/projectDropdownData";
import { validateAlertLimit } from "../../../../utils/validator/common/alertLimit";

type FinancialSummaryProps = {
  classes: Object,
  theme: Object,
  projectSettings: Object,
  billingMethod: Object,
  selectedFee: Object,
  updateProjectSettings: Function,
  permission: Object,
};

function FinancialSummary(props: FinancialSummaryProps) {
  /** Financial Summary main content  */
  const {
    theme,
    classes,
    projectSettings,
    billingMethod = {},
    selectedFee = {},
    updateProjectSettings,
    permission,
    intl,
  } = props;
  const { projectBillingMethod } = props.projectSettings;
  const [alertLimit, setAlertLimit] = useState(projectSettings.alertLimit);
  const [actualCostColor, setActualCostColor] = useState("");
  const [percentage, setPercentage] = useState("");

  const getLabel = () => {
    /** function call for returning the label in different scenarios */
    const { projectSettings = {} } = props;
    const method = projectSettings.projectBillingMethod;
    const fee = projectSettings.feeType;
    switch (method) {
      case 0:
        return intl.formatMessage({
          id: "project.dev.details.billable.billing-method.list.fixed",
          defaultMessage: "Fixed fee",
        });
        break;
      case 1:
        if (fee == 0) {
          return intl.formatMessage({
            id: "project.dev.details.billable.billing-method.list.fixedpertask",
            defaultMessage: "Fee per task",
          });
        }
        if (fee == 1) {
          return intl.formatMessage({
            id: "project.dev.details.billable.billing-method.list.avgpertask",
            defaultMessage: "Avg. fee per task",
          });
        }
        break;
      case 2:
        if (fee == 0) {
          return intl.formatMessage({
            id: "project.dev.details.billable.billing-method.list.hourlyrate",
            defaultMessage: "Hourly rate per task",
          });
        }
        if (fee == 1) {
          return intl.formatMessage({
            id: "project.dev.details.billable.billing-method.list.avghourly",
            defaultMessage: "Avg. hourly rate per task",
          });
        }
        break;
      case 3:
        if (fee == 0) {
          return intl.formatMessage({
            id: "project.dev.details.billable.billing-method.list.hourlyperres",
            defaultMessage: "Hourly rate per resource",
          });
        }
        if (fee == 1) {
          return intl.formatMessage({
            id: "project.dev.details.billable.billing-method.list.avghourlyperres",
            defaultMessage: "Avg. hourly rate per resource",
          });
        }
        break;
      default:
        return "-";
        break;
    }
  };

  const getrowLabelValue = () => {
    /** function calls for returing the fixed, average values of diff scenarios */
    const method = projectSettings.projectBillingMethod;
    const fee = projectSettings.feeType;
    let average = 0;
    let sum = 0;
    const currencySymbol = projectSettings.currency;
    switch (method) {
      case 0:
        return `${currencySymbol} ${projectSettings.budget}`; /** if fixed fee then return simply user entered fixed money */
        break;

      case 1:
        if (fee == 0) {
          return `${currencySymbol} ${projectSettings.hourlyRate}`; /** if same fee then return simply user entered money */
        }
        if (fee == 1) {
          projectSettings.tasks.map(t => {
            sum = parseInt(t.fixedFee) + sum;
          });
          average = sum / projectSettings.tasks.length;
          return `${currencySymbol} ${average.toFixed(2)}`;
        }
        break;

      case 2:
        if (fee == 0) {
          return `${currencySymbol} ${projectSettings.hourlyRate}`; /** if same fee then return simply user entered money */
        }
        if (fee == 1) {
          projectSettings.tasks.map(t => {
            sum = parseInt(t.hourlyRate) + sum;
          });
          average = sum / projectSettings.tasks.length;
          return `${currencySymbol} ${average.toFixed(2)}`;
        }
        break;

      case 3:
        if (fee == 0) {
          return `${currencySymbol} ${projectSettings.hourlyRate}`; /** if same fee then return simply user entered money */
        }
        if (fee == 1) {
          projectSettings.resources.map(t => {
            sum = parseInt(t.hourlyRate) + sum;
          });
          average = sum / projectSettings.resources.length;
          return `${currencySymbol} ${average.toFixed(2)}`;
        }
        break;

      default:
        return "-";
        break;
    }
  };

  const getEstimatedTime = () => {
    /** function calculates sum of all tasks in minutes */
    let sum = 0;
    const estimatedTime = projectSettings.tasks.map(t => {
      sum = parseInt(t.estimatedTimeMin) + sum;
    });

    return getTimeConvert(sum);
  };

  const getEstimatedCost = () => {
    /** it means Estimated Cost Function */
    /** function calls for calculating estimated cost */
    const method = projectSettings.projectBillingMethod;
    const fee = projectSettings.feeType;
    const taskRows = projectSettings.tasks.filter(task => task.isBillable);
    const rows = projectSettings.resources.filter(resourse => resourse.isBillable);

    let estimatedCost = 0;

    switch (method) {
      case 0:
        estimatedCost = projectSettings.budget; /** total project fee / fixed fee */
        return estimatedCost.toFixed(2);
        break;

      case 1:
        if (fee == 0) {
          estimatedCost =
            taskRows.length *
            projectSettings.hourlyRate; /** if fixed fee per task then, fixed fee multiply by total no of tasks */
          return estimatedCost.toFixed(2);
        }
        if (fee == 1) {
          let sum = 0;
          taskRows.map(t => {
            sum =
              parseInt(t.fixedFee) +
              sum; /** if different fee per task then add all task's fixed fee */
          });
          estimatedCost = sum;
          return estimatedCost.toFixed(2);
        }
        break;

      case 2:
        if (fee == 0) {
          let totalHours = 0;
          taskRows.map(t => {
            totalHours = Number(t.estimatedTimeMin / 60) + totalHours;
          });
          estimatedCost =
            totalHours *
            projectSettings.hourlyRate; /** if fixed fee per task then, fixed fee multiply by total no of tasks */
          return estimatedCost.toFixed(2);
        }
        if (fee == 1) {
          let sum = 0;
          taskRows.map(t => {
            sum = Number(t.estimatedTimeMin / 60) * t.hourlyRate + sum;
          });
          estimatedCost = sum;
          return estimatedCost.toFixed(2);
        }
        break;

      case 3:
        if (fee == 0) {
          let totalWeeklyCapacity = 0;
          rows.map(t => {
            totalWeeklyCapacity =
              parseInt(t.estimatedHour) +
              totalWeeklyCapacity; /** calculate all resources weekly capacity */
          });
          estimatedCost =
            totalWeeklyCapacity *
            projectSettings.hourlyRate; /** multiply fixed fee with total weekly capacity */
          return estimatedCost.toFixed(2);
        }
        if (fee == 1) {
          let sum = 0;
          rows.map(t => {
            sum =
              parseInt(t.estimatedHour) * parseInt(t.hourlyRate) +
              sum; /** multiply each weekly capacity with its own hourly rate and add result  */
          });
          estimatedCost = sum;
          return estimatedCost.toFixed(2);
        }
        break;

      default:
        return 0;
        break;
    }
  };
  const handleSendAlert = () => {
    /** selecting the email option */
    updateProjectSettings("sendEmailAlert", !projectSettings.sendEmailAlert, {});
  };

  const handleChangeAlertLimit = e => {
    /** function calls on change percentage of email alerts */
    const inputValue = e.target.value;
    if (validateAlertLimit(inputValue).validated) {
      // Function handles input
      const value = inputValue ? parseInt(inputValue) : "";
      if (value <= 100) setAlertLimit(value);
    }
  };
  const handleBlur = () => {
    /** saving email alert values */
    updateProjectSettings("alertLimit", alertLimit, {});
  };
  const onKeyDown = e => {
    if (e.key == "Enter") {
      handleBlur();
    }
  };

  const getTotalCost = () => {
    /** it means Actual Cost Function */
    /** function responsible for calculating the total cost */
    const { projectBillingMethod, feeType, tasks, resources, budget, hourlyRate } = projectSettings;

    let totalCost = 0;
    const completedTasks = getCompletedTaskLength();
    const totalTasks = tasks.filter(t => t.isBillable).length;
    const resourcesArr = resources.filter(resourse => resourse.isBillable);
    const taskRows = tasks.filter(task => task.isBillable);

    switch (projectBillingMethod) {
      case 0 /** fixed fee */:
        totalCost =
          (budget / totalTasks) *
          completedTasks; /** [Total cost = [(Fixed fee / Total No. Of billable tasks) x Total No. Of Completed billable tasks] */
        return isNaN(totalCost) ? 0 : totalCost.toFixed(2);
        break;

      case 1 /** fixed fee per task */:
        if (feeType == 0) {
          /** same fee for every task */
          totalCost =
            completedTasks *
            hourlyRate; /** (Total cost = Fee for billable tasks X Total No. Of billable Completed Tasks)  */
          return isNaN(totalCost) ? 0 : totalCost.toFixed(2);
        }
        if (feeType == 1) {
          /** differnet fee for per task */
          let sum = 0;
          taskRows.map(t => {
            if (t.isDoneState) {
              sum =
                parseInt(t.fixedFee) +
                sum; /** (Total cost = [Fee for Completed billable Task (1)] + [Fee for billable Completed task (2)] + [ Fee for billable Completed Task(n)]) */
            }
          });
          totalCost = sum;
          return totalCost.toFixed(2);
        }
        break;

      case 2 /** hourly rate per task */:
        if (feeType == 0) {
          /** same hourly rate for every task */
          let totalHours = 0;
          taskRows.map(t => {
            totalHours = Number(t.approvedTimeMin / 60) + totalHours;
          });
          totalCost =
            totalHours *
            hourlyRate; /** (Total cost = [Sum of approved time of all billable Tasks X hourly rate for billable Tasks]) */
          return totalCost.toFixed(2);
        }
        if (feeType == 1) {
          /** diff hourly rate per task */
          let sum = 0;
          taskRows.map(t => {
            sum = Number(t.approvedTimeMin / 60) * t.hourlyRate + sum;
          });
          totalCost = sum; /** Total cost =  [Approved time of billable Task (1) x hourly rate for billable Task (1)] + [Approved time of billable Task (2) x hourly rate for billable task (2)] + [Approved d time of billable Task(n) x hourly rate for billable Task(n)]) */
          return totalCost.toFixed(2);
        }
        break;

      case 3:
        if (feeType == 0) {
          let totalHours = 0;
          resourcesArr.map(t => {
            totalHours = Number(t.approvedTimeMin / 60) + totalHours;
          });
          totalCost =
            totalHours *
            hourlyRate; /** (Total cost = [Sum of approved time of all billable resources X hourly rate for billable resources]) */
          return totalCost.toFixed(2);
        }
        if (feeType == 1) {
          let sum = 0;
          resourcesArr.map(t => {
            sum = Number(t.approvedTimeMin / 60) * parseInt(t.hourlyRate) + sum;
          });
          totalCost = sum; /** Total cost =  [Approved time of billable resource (1) x hourly rate for billable resource (1)] + [Approved time of billable resource (2) x hourly rate for billable resource (2)] + [Approved d time of billable resource(n) x hourly rate for billable resource(n)]) */
          return totalCost.toFixed(2);
        }
        break;

      default:
        return "-";
        break;
    }
  };

  const getTimeConvert = num => {
    /** fun responsible for seprating the min into hours and mins */
    const hours = Math.floor(num / 60);
    const minutes = num % 60;
    return `${hours} ${intl.formatMessage({
      id: "project.dev.fianancial-summary.hrs-min.hrs",
      defaultMessage: "hrs",
    })} ${minutes} ${intl.formatMessage({
      id: "project.dev.fianancial-summary.hrs-min.min",
      defaultMessage: "mins",
    })}`;
  };

  const getCompletedTaskLength = () => {
    /** fun calculate the total completed tasks */
    const { length } = projectSettings.tasks.filter(t => t.isDoneState && t.isBillable);
    return length;
    // return projectSettings.completedTask;
  };

  const getPercentageOfActualCost = () => {
    /** fun responsible for calculating the percentage increase and descrease between actal and estiamted cost */
    const estimatedCost = parseFloat(getEstimatedCost());
    const actualCost = parseFloat(getTotalCost());

    const diff = estimatedCost - actualCost;
    const percentage = (diff / estimatedCost) * 100;
    if (Math.sign(percentage) == -1) {
      /** -1 means increase in percentage */
      setActualCostColor("#FF4A4A");
    } else {
      setActualCostColor("#1CB177");
    }
    setPercentage(isNaN(percentage) ? 0 : percentage.toFixed(0));
  };

  useEffect(() => {
    getPercentageOfActualCost();
  }, []);
  const translate = value => {
    let id = "";
    switch (value) {
      case "Fixed Fee":
        id = "project.dev.details.billable.billing-method.list.fixed";
        break;
      case "Fixed Fee per Task":
        id = "project.dev.details.billable.billing-method.list.fixedfeepertask";
        break;
      case "Hourly Rate by Task":
        id = "project.dev.details.billable.billing-method.list.hourlyratebytask";
        break;
      case "Hourly Rate by Resource":
        id = "project.dev.details.billable.billing-method.list.hourlyratebyresource";
        break;
      case "Same Fee for Every Task":
        id = "project.dev.details.billable.fee.list.same-fee-task";
        break;
      case "Different Fee per Task":
        id = "project.dev.details.billable.fee.list.different-fee-task";
        break;
      case "Same Hourly Rate for Every Task":
        id = "project.dev.details.billable.rate.list.same-hourly-rate-task";
        break;
      case "Different Hourly Rate per Task":
        id = "project.dev.details.billable.rate.list.different-hourly-rate-task";
        break;
      case "Same Hourly Rate for Every Resource":
        id = "project.dev.details.billable.rate.list.same-hourly-rate-resource";
        break;
      case "Different Hourly Rate per Resource":
        id = "project.dev.details.billable.rate.list.different-hourly-rate-resource";
        break;
    }
    return id == "" ? value : <FormattedMessage id={id} defaultMessage={value} />;
  };

  const sumApproveTimeLoggedTask = () => {
    const { tasks } = projectSettings;
    let sumApproveTimeTask = 0;
    const totalTasks = tasks.filter(t => t.isBillable);
    totalTasks.map(ele => {
      sumApproveTimeTask = ele.approvedTimeMin + sumApproveTimeTask;
    });
    return sumApproveTimeTask;
  };
  const sumPendingTimeLoggedTask = () => {
    const { tasks } = projectSettings;
    let sumPendingTimeTask = 0;
    const totalTasks = tasks.filter(t => t.isBillable);
    totalTasks.map(ele => {
      sumPendingTimeTask = ele.pendingApprovalMin + sumPendingTimeTask;
    });
    return sumPendingTimeTask;
  };
  const sumApproveTimeLoggedResource = () => {
    const { resources } = projectSettings;
    let sumApproveTimeResource = 0;
    const totalResources = resources.filter(t => t.isBillable);
    totalResources.map(ele => {
      sumApproveTimeResource = ele.approvedTimeMin + sumApproveTimeResource;
    });
    return sumApproveTimeResource;
  };
  const sumPendingTimeLoggedResource = () => {
    const { resources } = projectSettings;
    let sumPendingTimeResource = 0;
    const totalResources = resources.filter(t => t.isBillable);
    totalResources.map(ele => {
      sumPendingTimeResource = ele.pendingTimeMin + sumPendingTimeResource;
    });
    return sumPendingTimeResource;
  };

  let sumApproveLogged = projectBillingMethod == 3 ? sumApproveTimeLoggedResource() : sumApproveTimeLoggedTask();
  let sumPendingLogged = projectBillingMethod == 3 ? sumPendingTimeLoggedResource() : sumPendingTimeLoggedTask();

  return (
    <>
      {permission.financialSummaryTab.billingDetails.cando && projectSettings.billingMode !== 0 && (
        <div className={classes.billingMethodCnt}>
          <div style={{ display: "flex", marginRight: 20 }}>
            <SvgIcon
              viewBox="0 0 45 45"
              htmlColor={theme.palette.secondary.light}
              className={classes.billingIcon}>
              <BillingIcon />
            </SvgIcon>
            <div style={{ padding: 5 }}>
              <Typography variant="h6" className={classes.title}>
                <FormattedMessage
                  id="project.dev.details.billable.billing-method.label"
                  defaultMessage="Billing Method"
                />
              </Typography>
              <Typography variant="h6" className={classes.subTitle}>
                {translate(billingMethod.label)}
              </Typography>
            </div>
            {billingMethod.value != 0 /** 0 means fixed Fee */ && (
              <div className={classes.feeCnt}>
                <Typography variant="h6" className={classes.title}>
                  <FormattedMessage
                    id="project.dev.fianancial-summary.fee.label"
                    defaultMessage="Fee"
                  />
                </Typography>
                <Typography variant="h6" className={classes.subTitle}>
                  {translate(selectedFee.label)}
                </Typography>
              </div>
            )}
          </div>
        </div>
      )}

      {permission.financialSummaryTab.estimatedCost.cando && (
        <div className={classes.estimatedCostCnt}>
          <ul className={classes.financialList}>
            <li className={classes.header}>
              <ArrowDropDownIcon
                className={classes.arrowDown}
                htmlColor={theme.palette.secondary.medDark}
              />
              <Typography variant="h6" className={classes.heading}>
                <FormattedMessage
                  id="project.dev.fianancial-summary.estimated-cost.label"
                  defaultMessage="Estimated Cost"
                />
              </Typography>
              <Typography variant="h6" className={classes.heading2}>
                {`${projectSettings.currency} ${getEstimatedCost()}`}
              </Typography>
            </li>
            <li className={classes.child}>
              <SvgIcon viewBox="0 0 20 15" className={classes.taskIcon}>
                <TaskIcon />
              </SvgIcon>
              <Typography variant="h6" className={classes.financialListItemTitle}>
                <FormattedMessage
                  id="project.dev.fianancial-summary.estimated-cost.list.total-no-tasks"
                  defaultMessage="Total no. of tasks"
                />
              </Typography>
              <Typography variant="h6" className={classes.listItemText}>
                {projectSettings.tasks.length}
              </Typography>
            </li>
            <li className={classes.child}>
              <SvgIcon viewBox="0 0 20 15" className={classes.timeIcon}>
                <TimeIcon />
              </SvgIcon>
              <Typography variant="h6" className={classes.financialListItemTitle}>
                <FormattedMessage
                  id="project.dev.fianancial-summary.estimated-cost.list.estimated-time"
                  defaultMessage="Estimated time"
                />
              </Typography>
              <Typography variant="h6" className={classes.listItemText}>
                {getEstimatedTime()}
              </Typography>
            </li>
            <li className={classes.child}>
              <SvgIcon viewBox="1 0 20 15" className={classes.resourcesIcon}>
                <ResourcesIcon />
              </SvgIcon>
              <Typography variant="h6" className={classes.financialListItemTitle}>
                <FormattedMessage
                  id="project.dev.fianancial-summary.estimated-cost.list.total-no-resources"
                  defaultMessage="Total number of resources"
                />
              </Typography>
              <Typography variant="h6" className={classes.listItemText}>
                {projectSettings.resources.length}
              </Typography>
            </li>
            <li className={classes.child}>
              <SvgIcon viewBox="1 0 20 15" className={classes.costIcon}>
                <CostIcon />
              </SvgIcon>
              <Typography variant="h6" className={classes.financialListItemTitle}>
                {getLabel()}
              </Typography>
              <Typography variant="h6" className={classes.listItemText}>
                {getrowLabelValue()}
              </Typography>
            </li>
          </ul>
        </div>
      )}

      {permission.financialSummaryTab.actualCost.cando && (
        <div className={classes.actualCostCnt}>
          <ul className={classes.financialList}>
            <li className={classes.header}>
              <ArrowDropDownIcon
                className={classes.arrowDown}
                htmlColor={theme.palette.secondary.medDark}
              />
              <Typography variant="h6" className={classes.heading}>
                <FormattedMessage
                  id="project.dev.fianancial-summary.actual-cost.label"
                  defaultMessage="Actual Cost"
                />
              </Typography>
              <div style={{ textAlign: "right" }}>
                {" "}
                <Typography
                  variant="h6"
                  className={classes.heading2}
                  style={{ color: actualCostColor }}>
                  {`${projectSettings.currency} ${getTotalCost()}`}
                </Typography>
                <Typography variant="h6" className={classes.title}>
                  {Math.sign(percentage) == -1 && (
                    <SvgIcon viewBox="0 0 9 12" className={classes.arrowUp}>
                      <ArrowIncreasedRed />
                    </SvgIcon>
                  )}

                  <span className={classes.percentage}>{`${Math.abs(percentage)}%`}</span>

                  {Math.sign(percentage) == -1
                    ? ` ${intl.formatMessage({
                        id:
                          "project.dev.fianancial-summary.actual-cost.increased-remaining.increased",
                        defaultMessage: "increased from estimated cost",
                      })}`
                    : ` ${intl.formatMessage({
                        id:
                          "project.dev.fianancial-summary.actual-cost.increased-remaining.remaining",
                        defaultMessage: "estimated cost remaining",
                      })}`}
                </Typography>
              </div>
            </li>
            <li className={classes.child}>
              <SvgIcon viewBox="0 0 20 15" className={classes.taskIcon}>
                <TaskIcon />
              </SvgIcon>
              <Typography variant="h6" className={classes.financialListItemTitle}>
                <FormattedMessage
                  id="project.dev.fianancial-summary.actual-cost.list.total-completed-tasks"
                  defaultMessage="Total no. of completed tasks"
                />
              </Typography>
              <Typography variant="h6" className={classes.listItemText}>
                {getCompletedTaskLength()}
              </Typography>
            </li>
            {projectSettings.projectBillingMethod > 1 && (
              <>
                <li className={classes.child}>
                  <SvgIcon viewBox="0 0 20 15" className={classes.timeIcon}>
                    <ApproveIconfrom />
                  </SvgIcon>
                  <Typography variant="h6" className={classes.financialListItemTitle}>
                    <FormattedMessage
                      id="project.dev.fianancial-summary.actual-cost.list.approved-time-logged"
                      defaultMessage="Approved time logged"
                    />
                  </Typography>
                  <Typography variant="h6" className={classes.listItemText}>
                    {/* {getTimeConvert(projectSettings.projectApprovedMin)} */}
                    {getTimeConvert(sumApproveLogged)}
                  </Typography>
                </li>
                <li className={classes.child}>
                  <SvgIcon viewBox="0 0 20 15" className={classes.timeIcon}>
                    <TimeIcon />
                  </SvgIcon>
                  <Typography variant="h6" className={classes.financialListItemTitle}>
                    <FormattedMessage
                      id="project.dev.fianancial-summary.actual-cost.list.pending-approval"
                      defaultMessage="Pending approval time"
                    />
                  </Typography>
                  <Typography variant="h6" className={classes.listItemText}>
                    {/* {getTimeConvert(projectSettings.projectPendingApprovalMin)} */}
                    {getTimeConvert(sumPendingLogged)}
                  </Typography>
                </li>
              </>
            )}
            <li className={classes.child}>
              <SvgIcon viewBox="1 0 20 15" className={classes.costIcon}>
                <CostIcon />
              </SvgIcon>
              <Typography variant="h6" className={classes.financialListItemTitle}>
                <FormattedMessage
                  id="project.dev.fianancial-summary.actual-cost.list.total-cost"
                  defaultMessage="Total cost"
                />
              </Typography>
              <Typography variant="h6" className={classes.listItemText}>
                {`${projectSettings.currency} ${getTotalCost()}`}
              </Typography>
            </li>
          </ul>
        </div>
      )}

      {/* <div className={classes.sendEmailCnt}>
        <DefaultCheckbox
          checkboxStyles={{ padding: "5px 5px 0 0 " }}
          checked={projectSettings.sendEmailAlert}
          onChange={handleSendAlert}
          fontSize={20}
          color={"#0090ff"}
          disabled={!permission.financialSummaryTab.emailsAlerts.cando}
        />
        <Typography variant="h6" className={classes.heading}>
          <FormattedMessage
            id="project.dev.fianancial-summary.send-email.label"
            values={{
              t1: (
                <OutlinedInput
                  labelWidth={150}
                  notched={false}
                  value={alertLimit}
                  onKeyDown={onKeyDown}
                  autoFocus={false}
                  onChange={handleChangeAlertLimit}
                  onBlur={handleBlur}
                  placeholder=""
                  inputProps={{ maxLength: 3 }}
                  classes={{
                    root: classes.outlinedInputCntt,
                    input: classes.outlinedProjectInput,
                    notchedOutline: classes.notchedOutlineCntt,
                    focused: classes.outlineInputFocuss,
                  }}
                  disabled={
                    !projectSettings.sendEmailAlert ||
                    !permission.financialSummaryTab.emailsAlerts.cando
                  }
                />
              ),
            }}
            defaultMessage={`Send email alerts if project actual cost exceeds${" "}${(
              <OutlinedInput
                labelWidth={150}
                notched={false}
                value={alertLimit}
                onKeyDown={onKeyDown}
                autoFocus={false}
                onChange={handleChangeAlertLimit}
                onBlur={handleBlur}
                placeholder=""
                inputProps={{ maxLength: 3 }}
                classes={{
                  root: classes.outlinedInputCntt,
                  input: classes.outlinedProjectInput,
                  notchedOutline: classes.notchedOutlineCntt,
                  focused: classes.outlineInputFocuss,
                }}
                disabled={
                  !projectSettings.sendEmailAlert ||
                  !permission.financialSummaryTab.emailsAlerts.cando
                }
              />
            )}
          % of estimated cost.}`}
          />
        </Typography>
      </div> */}
    </>
  );
}

FinancialSummary.defaultProps = {
  theme: {},
  classes: {},
  projectSettings: {},
  billingMethod: {},
  selectedFee: {},
  updateProjectSettings: () => {},
  permission: {},
};

const mapStateToProps = state => {
  return {};
};

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, {})
)(FinancialSummary);
