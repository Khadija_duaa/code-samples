const FinancialSummaryStyles = (theme) => ({
  billingMethodCnt: {
    backgroundColor: "#F6F6F6",
    border: `1px solid #EAEAEA`,
    borderRadius: 6,
    marginBottom: 15,
    width: "fit-content",
  },
  billingIcon: {
    // marginTop: -2,
    fontSize: "50px !important",
    marginRight: 5,
  },
  title: {
    color: "#969696",
    fontSize: "13px !important",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
  },
  subTitle: {
    color: "#202020",
    fontSize: "13px !important",
    fontWeight: 700,
    fontFamily: theme.typography.fontFamilyLato,
  },
  feeCnt: {
    marginLeft: 30,
    padding: "5px 0 0 25px",
    borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
  },
  PrEpsRoot: {
    backgroundColor: "#F6F6F6",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: 4,
    boxShadow: "none",
    "&$expanded": {
      minHeight: "0 !important",
    },
  },
  PEpdroot: {
    background: "white",
  },
  PEpsExpanded: {},
  PEpsContent: {
    "&$expanded": {
      margin: "12px 0",
    },
  },
  PEpsRoot: {
    minHeight: "0 !important",
    padding: "0 24px 0 15px",
    "&$expanded": {
      minHeight: "0 !important",
    },
  },
  PEpExpanded: {
    margin: "auto",
  },
  arrowDown: {
    // marginTop: -1,
  },
  financialList: {
    listStyleType: "none",
    padding: 0,
    width: "100%",
    margin: 0,
    "& li": {
      // padding: "2px 0 ",
      // margin: "0 20px",
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
      // borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    },
    "& li:last-child": {
      border: "none",
    },
  },
  financialListItemTitle: {
    flex: 1,
    fontSize: "13px !important",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
    color: "#646464",
  },
  heading: {
    fontSize: "13px !important",
    fontWeight: 700,
    fontFamily: theme.typography.fontFamilyLato,
    // marginBottom: 5,
    flex: 1,
  },
  heading2: {
    // right: 0,
    // position: "absolute",
    fontSize: "13px !important",
    fontWeight: 700,
    fontFamily: theme.typography.fontFamilyLato,
    // marginBottom: 5,
    // right: 35,
  },
  actualCostCnt: {
    marginTop: 15,
    border: "1px solid #EAEAEA",
    borderRadius: 6,
    paddingBottom: 10,
  },
  listItemText: {
    // right: 35,
    fontSize: "13px !important",
    fontWeight: 700,
    fontFamily: theme.typography.fontFamilyLato,
    // marginBottom: 5,
  },
  taskIcon: {
    fontSize: "17px !important",
    marginRight: 5,
    color: "#BFBFBF",
  },
  timeIcon: {
    fontSize: "19px !important",
    marginRight: 5,
    color: "#BFBFBF",
  },
  resourcesIcon: {
    marginTop: -7,
    fontSize: "19px !important",
    marginRight: 5,
    color: "#BFBFBF",
  },
  costIcon: {
    marginTop: -2,
    fontSize: "19px !important",
    marginRight: 5,
    color: "#BFBFBF",
  },
  estimatedCostCnt: {
    // padding: 10,
    border: "1px solid #EAEAEA",
    borderRadius: 6,
    paddingBottom: 10,
  },
  header: {
    padding: "11px 14px 11px 6px",
    background: "#F6F6F6",
    borderBottom: "1px solid #EAEAEA",
  },
  child: {
    padding: "5px 14px 0 22px",
  },
  sendEmailCnt: {
    display: "flex",
    marginTop: 15,
  },
  outlinedInputCntt: {
    borderRadius: 4,
    maxHeight: 32,
    maxWidth: 60,
    marginRight: 15,
    marginLeft: 3,
    background: theme.palette.common.white,
    "&:hover $notchedOutlineCnt": {
      border: "none",
    },
  },
  outlineInputFocuss: {
    "& $notchedOutlineCnt": {
      border: "none",
    },
  },
  notchedOutlineCntt: {
    borderRadius: 4,
    border: `1px solid ${theme.palette.border.lightBorder} !important`,
  },
  outlinedProjectInput: {
    // padding: "0px",
    borderRadius: 4,
    fontSize: "13px !important",
    fontWeight: 600,
    fontFamily: theme.typography.fontFamilyLato,
  },
  arrowUp: {
    fontSize: "12px !important",
    marginRight: 2,
  },
  percentage: {
    color: "#202020",
    fontWeight: 600,
  },
});

export default FinancialSummaryStyles;
