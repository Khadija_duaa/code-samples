const ProjectDetailsDialogStyles = (theme) => ({
  dialogContent: {
    padding: 20,
  },
  dialogPaperCntt: {
    // background: theme.palette.common.white
    // border: "10px solid black",
    // width: 500
  },
  statusIcon: {
    fontSize: "18px !important",
  },
  dialogueWidth:{
    width: '90% !important'
  },

  // detailsDiv: {
  //   width: "70%",
  //   background: "#F6F6F6",
  //   borderLeft: `1px solid #dddddd`,
  // },
  "@media (min-width: 1440px )": {
    dialogPaperCntt: {
      width: "50%",
    },
  },
  "@media (max-width: 1024px )": {
    dialogPaperCntt: {
      width: "90%",
    },
  },
});

export default ProjectDetailsDialogStyles;
