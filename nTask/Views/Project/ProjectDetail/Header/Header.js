import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import { compose } from "redux";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import CustomIconButton from "../../../../components/Buttons/CustomIconButton";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import FormHelperText from "@material-ui/core/FormHelperText";
import StatusDropdown from "../../../../components/Dropdown/StatusDropdown/Dropdown";
import CustomTooltip from "../../../../components/Tooltip/Tooltip";
import IconButton from "../../../../components/Buttons/IconButton";
import ColorPickerDropdown from "../../../../components/Dropdown/ColorPicker/Dropdown";
import IconEditSmall from "../../../../components/Icons/IconEditSmall";
import IconActivityLog from "../../../../components/Icons/IconActivityLog";

import CloseIcon from "@material-ui/icons/Close";
import SvgIcon from "@material-ui/core/SvgIcon";
import DockIcon from "../../../../components/Icons/DockIcon";
import ToggleUpdateIcon from "../../../../components/Icons/ToggleUpdateIcon";
import IconDock from "../../../../components/Icons/IconDock";
import DocumentIcon from "../../../../components/Icons/DocumentIcon";
import DocumentIconBlue from "../../../../components/Icons/DocumentIconBlue";
import ColorIcon from "../../../../components/Icons/ColorIcon";
import isEmpty from "lodash/isEmpty";
import { statusData } from "../../../../helper/projectDropdownData";
import { toggleProjectView, projectDetails } from "../../../../redux/actions/projects";
import Typography from "@material-ui/core/Typography";
import { FormattedMessage } from "react-intl";
import { ActivityIcon } from "../../../../components/Icons/ActivityIcon";
import { CanAccessFeature } from "../../../../components/AccessFeature/AccessFeature.cmp";
function Header(props) {
  const {
    theme,
    classes,
    projectTitle = "",
    error = {},
    handleTextFieldChange,
    handleStatusSelect,
    selectedColor = "#F47373",
    colorChange,
    handleDialogClose,
    selectedStatus = {},
    projectStatusData = [],
    onKeyDownProjectName,
    handleProjectNameClickAway,
    detailView = false,
    handleFullScreen,
    projectDetails,
    projectSetting,
    editProjectName,
    handleNameEdit,
    permission,
    intl,
  } = props;

  const handleDockView = () => {
    props.toggleProjectView({ docked: true, fullView: false });
  };
  const handleDockViewClose = () => {
    props.toggleProjectView({ docked: false, fullView: true });
  };

  const openChatView = (check) => {
    // Opening chat view in project details
    if (projectSetting.chatView || projectSetting.docsView) {
      projectDetails({ chatView: check, docsView: false, activityLogView: false });
    } else {
      projectDetails({ chatView: !projectSetting.chatView, activityLogView: false });
    }
  };
  const openDocsView = (check) => {
    // Opening chat view in project details
    if (projectSetting.chatView || projectSetting.docsView) {
      projectDetails({ chatView: false, docsView: check, activityLogView: false });
    } else {
      projectDetails({ docsView: !projectSetting.docsView, activityLogView: false });
    }
  };
  const openActivityLogView = () => {
    // Opening chat view in project details
    projectDetails({
      chatView: false,
      docsView: false,
      activityLogView: !projectSetting.activityLogView,
    });
  };

  const getEditIcon = () => {
    return (
      <div className={classes.editIconCnt}>
        <CustomTooltip
          helptext="Edit Project Name"
          iconType="help"
          placement="top"
          style={{ color: theme.palette.common.white }}>
          <CustomIconButton
            onClick={event => {
              handleNameEdit(event);
            }}
            btnType="transparent"
            variant="contained"
            style={{
              padding: "3px 3px 0px 0px",
              backgroundColor: "transparent",
            }}
            disabled={false}>
            <SvgIcon
              viewBox="0 0 12 11.957"
              // htmlColor={theme.palette.secondary.medDark}
              className={classes.editIcon}>
              <IconEditSmall />
            </SvgIcon>
          </CustomIconButton>
        </CustomTooltip>
      </div>
    );
  };

  let editProjectNamePer = permission ? permission.editProjectName.isAllowEdit : true;
  let editProjectStatusPer = permission ? !permission.editProjectStatus.isAllowEdit : false;
  let editProjectColor = permission ? !permission.projectColor.cando : false;
  let editProjectUpdates = permission ? !permission.projectUpdates.cando : false;
  let editProjectDocuments = permission ? !permission.projectDocuments.cando : false;

  return (
    <>
      <Grid container direction="row" justify="space-between" alignItems="center">
        <Grid item>
          {editProjectName ? (
            <>

              <OutlinedInput
                labelWidth={150}
                notched={false}
                value={projectTitle}
                onKeyDown={onKeyDownProjectName}
                autoFocus={true} /** if already created project then autofocus false */
                onChange={handleTextFieldChange}
                onBlur={handleProjectNameClickAway}
                placeholder={intl.formatMessage({
                  id: "project.creation-dialog.project-title.placeholder",
                  defaultMessage: "Enter project title",
                })}
                style={{ width: detailView ? 340 : 430, height: 32 }}
                inputProps={{ maxLength: 80 }}
                classes={{
                  root: classes.outlinedInputCnt,
                  input: classes.outlinedProjectInput,
                  notchedOutline: classes.notchedOutlineCntt,
                  focused: classes.outlineInputFocus,
                }}
              />
              {error.errorState && (
                <FormHelperText classes={{ root: classes.errorText }}>
                  {error.errorMessage || ""}
                </FormHelperText>
              )}
            </>
          ) : (
            <div className={classes.projectTitleCnt}>
              <Typography
                style={{}}
                className={classes.projectTitle}
                variant="caption"
                // onClick={handleNameEdit}
                title={projectTitle}>
                {projectTitle}
              </Typography>
              {editProjectNamePer && getEditIcon()}
            </div>
          )}
        </Grid>

        <Grid item>
          <CanAccessFeature group='project' feature='status'>
            <StatusDropdown
              onSelect={handleStatusSelect}
              option={selectedStatus}
              options={projectStatusData}
              style={{ marginRight: 8 }}
              disabled={editProjectStatusPer}
              writeFirst={true}
            />
          </CanAccessFeature>
          <ColorPickerDropdown
            selectedColor={selectedColor}
            onSelect={colorChange}
            style={{ marginRight: 6 }}
            placement="bottom-end"
            disabled={editProjectColor}
          />
          {detailView /** if detail view true then show other icons */ && (
            <>
              <CustomIconButton
                onClick={() => openChatView(!projectSetting.chatView)}
                btnType="transparent"
                variant="contained"
                style={{
                  width: 26,
                  height: 26,
                  borderRadius: 4,
                  marginRight: 6,
                }}
                disabled={editProjectUpdates}>
                <CustomTooltip
                  helptext={
                    <FormattedMessage id="project.tooltips.updates" defaultMessage="Updates" />
                  }
                  iconType="help"
                  placement="top"
                  style={{ color: theme.palette.common.white }}>
                  <SvgIcon
                    viewBox="-1 2 24 21"
                    // htmlColor={theme.palette.secondary.medDark}
                    className={classes.updatesIcon}
                    style={{ color: projectSetting.chatView ? "#0090ff" : "#969696" }}>
                    <ToggleUpdateIcon />
                  </SvgIcon>
                </CustomTooltip>
              </CustomIconButton>



              <CustomIconButton
                onClick={() => openDocsView(!projectSetting.docsView)}
                btnType="transparent"
                variant="contained"
                style={{
                  width: 26,
                  height: 26,
                  borderRadius: 4,
                  marginRight: 5,
                }}
                disabled={editProjectDocuments}>
                <CustomTooltip
                  helptext={
                    <FormattedMessage id="project.tooltips.documents" defaultMessage="Documents" />
                  }
                  iconType="help"
                  placement="top"
                  style={{ color: theme.palette.common.white }}>
                  <SvgIcon viewBox="0 5 25 12" className={classes.documentIcon}>
                    {projectSetting.docsView ? <DocumentIconBlue /> : <DocumentIcon />}
                  </SvgIcon>
                </CustomTooltip>
              </CustomIconButton>






              <CustomIconButton
                onClick={openActivityLogView}
                btnType="transparent"
                variant="contained"
                style={{
                  width: 26,
                  height: 26,
                  borderRadius: 4,
                  marginRight: 2,
                }}
                disabled={editProjectDocuments}>
                <CustomTooltip
                  helptext={
                    <FormattedMessage id="activity-log.label" defaultMessage="Activity Log" />
                  }
                  iconType="help"
                  placement="top"
                  style={{ color: theme.palette.common.white }}>
                  <SvgIcon
                    viewBox="0 0 18 17.501"
                    classes={{ root: classes.plainMenuItemIcon }}
                    style={{ color: projectSetting.activityLogView ? "#0090ff" : "#969696" }}>
                    <IconActivityLog />
                  </SvgIcon>
                </CustomTooltip>
              </CustomIconButton>

              {projectSetting.fullView ? (
                <CustomIconButton
                  onClick={handleDockView}
                  btnType="transparent"
                  variant="contained"
                  style={{
                    width: 26,
                    height: 26,
                    borderRadius: 4,
                    marginRight: 6,
                  }}>
                  <CustomTooltip
                    helptext={
                      <FormattedMessage id="project.tooltips.dock-view" defaultMessage="Dock View" />
                    }
                    iconType="help"
                    placement="top"
                    style={{ color: theme.palette.common.white }}>
                    <SvgIcon viewBox="0 5 25 14" className={classes.dockViewIcon}>
                      <DockIcon />
                    </SvgIcon>
                  </CustomTooltip>
                </CustomIconButton>
              ) : (
                <CustomIconButton
                  onClick={handleDockViewClose}
                  btnType="transparent"
                  variant="contained"
                  style={{
                    width: 26,
                    height: 26,
                    borderRadius: 4,
                    marginRight: 6,
                  }}>
                  <CustomTooltip
                    helptext={<FormattedMessage id="project.tooltips.full-screen" defaultMessage="Full Screen View" />}
                    iconType="help"
                    placement="top"
                    style={{ color: theme.palette.common.white }}>
                    <SvgIcon viewBox="0 5 25 14" className={classes.dockViewIcon}>
                      <IconDock />
                    </SvgIcon>
                  </CustomTooltip>
                </CustomIconButton>

              )}
            </>
          )}
          <CustomIconButton
            onClick={event => handleDialogClose(event, "projectDialog")}
            btnType="transparent"
            variant="contained"
            style={{ width: 26, height: 26, borderRadius: 4 }}>
            <CloseIcon htmlColor={theme.palette.secondary.medDark} style={{ fontSize: "18px" }} />
          </CustomIconButton>
        </Grid>
      </Grid>
    </>
  );
}
Header.defaultProps = {
  permission: null,
};
const mapStateToProps = (state, ownProps) => {
  return {
    projectSetting: state.projectDetailsDialog,
  };
};
export default compose(
  connect(mapStateToProps, { toggleProjectView, projectDetails }),
  withStyles(styles, { withTheme: true })
)(Header);
