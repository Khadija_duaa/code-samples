const projectHeaderStyles = (theme) => ({
  outlinedProjectInput: {
    padding: "0px 5px",
    background: theme.palette.background.default,
    borderRadius: 4,
    fontSize: "18px !important",
    fontWeight: 700,
    fontFamily: theme.typography.fontFamilyLato,
  },
  notchedOutlineCntt: {
    border: `1px solid ${theme.palette.background.darkblue} !important`,
  },
  errorText: {
    color: theme.palette.text.danger,
    marginBottom: -7,
  },
  statusIcon: {
    fontSize: "20px !important",
  },
  dockViewIcon: {
    fontSize: "24px !important",
  },
  documentIcon: {
    fontSize: "24px !important",
  },
  updatesIcon: {
    fontSize: "22px !important",
  },
  projectTitle: {
    color: "#000000",
    fontSize: "18px !important",
    fontWeight: 700,
    fontFamily: theme.typography.fontFamilyLato,

    overflow: "hidden",
    whiteSpace: "nowrap",
    lineHeight: "normal",
    textOverflow: "ellipsis",
    maxWidth: 300,
  },
  editIcon: {
    fontSize: "16px !important",
    marginTop: -2,
    marginLeft: 5,
  },
  editIconCnt: {
    visibility: "hidden",
  },
  projectTitleCnt: {
    display: 'flex',
    "&:hover $editIconCnt": {
      visibility: "visible",
    },
  },
  plainMenuItemIcon: {
    fontSize: "18px !important",
    // color: "white",
    // marginBottom: 3
  },
});

export default projectHeaderStyles;
