import React, { Component, Fragment, useEffect, useState } from "react";
import { compose } from "redux";
import Typography from "@material-ui/core/Typography";
import helper from "../../../../helper";
import { withStyles } from "@material-ui/core/styles";
import activityStyles from "./styles";
import CalendarIcon from "@material-ui/icons/DateRange";
import ReactHtmlParser from 'react-html-parser';
import { projectActivityLogs } from "../../../../redux/actions/projects";
import { connect } from "react-redux";
import isEmpty from "lodash/isEmpty";
import cloneDeep from "lodash/cloneDeep";
import moment from "moment";
import CustomAvatar from "../../../../components/Avatar/Avatar";
import Avatar from "../../../../assets/images/1.png";
import EmptyState from "../../../../components/EmptyStates/EmptyState";
import { FormattedMessage, injectIntl } from "react-intl";
import { Scrollbars } from "react-custom-scrollbars";

function ActivityList(props) {
  const { classes, theme, members, allMembers = [] } = props;
  const [projectActivities, setProjectActivities] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    props.projectActivityLogs(
      props.projectId,
      response => {
        createActivityLog(response.data.activitylog);
        setLoading(false);
      },
      error => {
        if (error) {
          showSnackBar("Server throws error", "error");
        }
      }
    );
    return () => {};
  }, [0]);

  const createActivityLog = (activitylog, callback) => {
    /** function that creates the activity logs */
    if (!isEmpty(activitylog)) {
      let activiLogs = [];
      let desiredLogs = [];
      let logs = cloneDeep(activitylog.results);
      logs.map(l => {
        /** concatinate the arrays of activites of different days */
        l.activities.map(a => {
          activiLogs.push(a);
        });
      });

      activiLogs.map(el => {
        desiredLogs.push({
          /** creating the desired array structure for activity logs */
          id: el.activity.id,
          userName: el.userName,
          action: el.activity.message,
          ticketId: el.activity.entityId,
          datetime: moment(el.activity.updatedDate).format("LLL"),
          taskName: el.activity.taskTitle,
          taskId: el.activity.taskId,
          userId: el.activity.createdBy,
        });
      });
      setProjectActivities(desiredLogs);
    }
  };
  return (
    <div className={classes.mainContainer}>
      <div className={classes.headerTitle}>
        {<FormattedMessage id="activity-log.detail-label" defaultMessage="Activity" />}
      </div>
      <Scrollbars autoHide autoHeight autoHeightMin={725} autoHeightMax={725}>
        {loading ? (
          <div
            style={{
              height: "725px",
              width: "100%",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}>
            <div class="loader"></div>
          </div>
        ) : !loading && projectActivities.length === 0 ? (
          <div
            style={{
              height: "725px",
              width: "100%",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}>
            <EmptyState
              screenType="search"
              heading={
                <FormattedMessage id="common.activities.nologfound" defaultMessage="No Log Found" />
              }
              message={
                <FormattedMessage
                  id="common.search-list.message"
                  defaultMessage="No matching results found against your filter criteria."
                />
              }
              button={false}
            />
          </div>
        ) : (
          <>
            {projectActivities.map((data, i, arr) => {
              let selectedMember = allMembers.find(ele => ele.userId == data.userId) || null;
              return (
                <div className={classes.container}>
                  <div>
                    <CustomAvatar
                      otherMember={{
                        // imageUrl: Avatar,
                        imageUrl: selectedMember ? selectedMember.imageUrl : null,
                        fullName: selectedMember ? selectedMember.fullName : null,
                        lastName: "",
                        email: selectedMember ? selectedMember.email : null,
                        // isOnline: selectedMember ? selectedMember.isOnline : null,
                        // isOwner: selectedMember ? selectedMember.isOwner : null,
                      }}
                      size="xsmall"
                      disableCard
                    />
                  </div>
                  <div className={classes.detail}>
                    <div className={classes.description}>
                      <span className={classes.user}>{`${data.userName} `}</span>
                      <span className={classes.actionDescription}>{ReactHtmlParser(data.action)} </span>
                    </div>
                    <div className={classes.date}>{data.datetime}</div>
                  </div>
                </div>
              );
            })}
          </>
        )}
      </Scrollbars>
    </div>
  );
}
const mapStateToProps = state => {
  return {
    allMembers: state.profile.data.member.allMembers,
  };
};
export default compose(
  injectIntl,
  withStyles(activityStyles, { withTheme: true }),
  connect(mapStateToProps, { projectActivityLogs })
)(ActivityList);
