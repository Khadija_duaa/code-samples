const taskDetailStyles = theme => ({
  headerTitle: {
    background: theme.palette.background.light,
    padding: "17px 11px 10px 11px",
    borderBottom: `1px solid ${theme.palette.background.contrast}`,
    color: theme.palette.text.light,
    fontSize: "12px !important",
  },
  mainContainer: {
    width: "100%",
    borderLeft: `1px solid ${theme.palette.background.contrast}`,
  },
  container: {
    padding: "12px 10px 0px  10px",
    display: "flex",
    alignItems: "flex-start",
  },
  detail: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    padding: "0px 0px 6px 0px",
    margin: "0px 8px 0px 6px",
    borderBottom: `1px solid ${theme.palette.background.grayLighter}`,
    width: "100%",
  },
  description: {
    textAlign: "left",
    fontFamily: "Lato, sans-serif",
    fontWeight: theme.typography.fontWeightLight,
    fontSize: "13px !important",
    lineHeight: "16px",
    letterSpacing: 0,
    color: `${theme.palette.text.primay}`,
  },
  user: {
    color: `${theme.palette.common.black}`,
    fontSize: "13px !important",
    fontWeight: theme.typography.fontWeightLarge,
    fontFamily: theme.typography.fontFamilyLato,
  },
  actionDescription: {
    color: `#333333`,
    fontSize: "13px !important",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
  },
  ticket: {
    color: `${theme.palette.text.azure}`,
    fontSize: "13px !important",
    margin: 0,
    cursor: "pointer",
    fontFamily: theme.typography.fontFamilyLato,
  },
  date: {
    padding: "5px 0",
    textAlign: "left",
    fontFamily: "Lato, sans-serif",
    fontWeight: theme.typography.fontWeightLight,
    fontSize: "12px !important",
    lineHeight: "15px",
    letterSpacing: 0,
    color: `${theme.palette.text.medGray}`,
  },
});

export default taskDetailStyles;
