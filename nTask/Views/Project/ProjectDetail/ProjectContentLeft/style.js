const projectContentStyles = (theme) => ({
  projectContentCnt: {
    width: "100%",
    display: "flex",
    flex: 1,
  },
  projectContentLeft: {
    flex: 1,
    width: 650,
    display: "flex",
    flexDirection: "column",
    overflowY: "auto",
    borderRight: `1px solid ${theme.palette.background.contrast}`,

  },
  projectContentLeftDocked: {
    flex: 1,
    width: 650,
    display: "flex",
    flexDirection: "column",
    overflowY: "auto",
  },
  projectContentRightDock: {
    // width: "45%",
    overflow: "hidden",
    width: 520,
  },
  projectContentRight: {
    width: "45%",
    overflow: "hidden",
    // width: 520,
  },
  TabsRoot: {
    background: "#F6F6F6",
    "-webkit-box-shadow": "inset 0px 24px 5px -27px rgba(0,0,0,0.45)",
    "-moz-box-shadow": "inset 0px 24px 5px -27px rgba(0,0,0,0.45)",
    "box-shadow": "inset 0px 24px 5px -27px rgba(0,0,0,0.45)",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,

    minHeight: 40,
  },
  tabIndicator: {
    background: theme.palette.border.blue,
    borderRadius: "10px 10px 0px 0px",
    height: 3,
  },
  tab: {
    minWidth: "unset",
    minHeight: 40,
    flexGrow: 1,
    textTransform: "unset"
  },
  tabLabel: {
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 600,
  },
  tabLabelCnt: {
    padding: 0,
    // fontSize: "12px !important",
    textTransform: "capitalize",
    "& $h6": {
      fontWeight: theme.typography.fontWeightMedium,
    },
  },
  tabSelected: {
    background: "none",
    boxShadow: "none",
    color: theme.palette.border.blue,
    "& $h6": {
      color: theme.palette.border.blue,
    },
  },
  dialogContent: {
    padding: 25,
    // minHeight: 610,
    flex: 1,
    overflowY: "auto",
  },
  createdBy: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    padding: "10px 15px",
    // position: "absolute",
    bottom: 0,
    minHeight: 35,
    left: 0,
    right: 0,
    background: "#fff",
    borderRadius: "0 0 4px 4px",
    borderTop : "1px solid #EAEAEA"
  },
  createdOn: {
    fontSize: "12px",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
    color: "#7E7E7E",
  },
  lastSaved: {
    fontSize: "12px",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 700,
    color: "#202020",
  },
  taskDetailsCnt: {
    background: theme.palette.background.airy,
    height: "100%",
    boxShadow: "-2px 0px 0px rgba(234, 234, 234, 1)",
    borderLeft: "1px solid #EAEAEA",
    overflowY: "auto",
  },
  "@media (max-width: 1024px )": {
    projectContentLeftDocked: {
      width: 40,
    },
  },
});
export default projectContentStyles;
