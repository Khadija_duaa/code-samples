// @flow

import React, { useState, useEffect, Component, Fragment } from "react";
import { withSnackbar } from "notistack";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import SvgIcon from "@material-ui/core/SvgIcon";
import cloneDeep from "lodash/cloneDeep";
import ProjectResources from "../Resources/Resources";
import ProjectTasks from "../Tasks/Tasks";
import Typography from "@material-ui/core/Typography";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Details from "../Details/ProjectDetails";
import IconActivitySmall from "../../../../components/Icons/IconActivitySmall";
import moment from "moment";
import {
  saveProjectSettings,
  saveSplitProjectSettings,
  saveBillableSetting,
  projectDetails, UpdateProjectDetailListing,
} from "../../../../redux/actions/projects";
import { SaveTask, updateStoreTask, getTaskById } from "../../../../redux/actions/tasks";
import {
  billingMethodData,
  feeMethodData,
  currencyData,
} from "../../../../helper/projectDropdownData";
import FinancialSummary from "../FinancialSummary/FinancialSummary";
import MileStones from "../Milestones/Milestone";
import TaskDetails from "../Tasks/TaskDetails/TaskDetails";
import ListLoader from "../../../../components/ContentLoader/List";
import {
  CopyTask,
  ArchiveTask,
  DeleteTask,
  UpdateTask,
  saveSubTask,
  updateTaskData
} from "../../../../redux/actions/tasks";
import { publickLinkDialog } from "../../../../redux/actions/allDialogs";
import { generateSelectData } from "../../../../helper/generateSelectData";
import isUndefined from "lodash/isUndefined";
// import ProjectChat from "../../ProjectDetails/ProjectChat";
import ChatBuilder from "../../../../components/Chat/ChatBuilder";
import isEmpty from "lodash/isEmpty";
import NotificationMessage from "../../../../components/NotificationMessages/NotificationMessages";
import { FormattedMessage, injectIntl } from "react-intl";
import ProjectActivity from "../ActivityLog/ProjectActivity";
import { updateGlobalTimeTask, StoreDeleteTaskInfo } from "../../../../redux/actions/globalTimerTask";
import { DeleteArchiveTaskFromBoard } from "../../../../redux/actions/boards";
import { v4 as uuidv4 } from "uuid";
import { emptyTask } from "../../../../utils/constants/emptyTask";
import { CanAccessFeature, CanAccess } from "../../../../components/AccessFeature/AccessFeature.cmp";
import ProjectCalendar from '../ProjectCalendar/ProjectCalendar.view';
import { isAnyBusinessPlan, isEnterprisePlan } from "../../../../components/constants/planConstant";

type ProjectContentProps = {
  classes: Object,
  theme: Object,
  data: Object,
  selectedProject: Object,
  projectDescription: String,
  saving: Boolean,
  constantsState: Array,
  setSaving: Function,
  showSnackBar: Function,
  setProjectDescription: Function,
  publickLinkDialog: Function,
  CopyTask: Function,
  ArchiveTask: Function,
  DeleteTask: Function,
  UpdateTask: Function,
  SaveTask: Function,
  updateStoreTask: Function,
  tasks: Array,
  openChat: Boolean,
  saveSubTask: Function,
  projectDetails: Function,
  taskDetails: Object,
  setTaskDetails: Function,
  addTasksInProject: Function,
  setSelectedProject: Function,
  allProjects: Array,
  taskPer: Object,
  workspaceStatus: Object,
};

let sortedBreadCrumbArr = [];

class ProjectContentLeft extends Component<ProjectContentProps> {
  constructor(props) {
    super(props);
    this.state = {
      tabvalue: 0,
      selectedFee: {},
      amount: "",
      budget: 0,
      selectedCurrency: {},
      billingMethod: {},
      projectTasks: [],
      allTasks: [],
      breadCrumbArr: [],
      subscription: "",
      resources: [],
      isSubTaskCreated: false,
      onClickTask: {},
    };
  }

  updateContent = () => {
    const { theme, classes, data = {}, constantsState } = this.props;
    const { } = this.state;
    if (data && data.projectSettings) {
      this.initProjectTasks();
      this.initAllTasks();
      this.initProjectResources();

      let billinMethd /** selection billing method value when user open modal */ =
        billingMethodData().find(b => b.value == data.projectSettings.projectBillingMethod) || {};

      let selectedFee /** select fee type same fee/different fee scenario */ =
        feeMethodData(billinMethd).find(f => f.value == data.projectSettings.feeType) || {};

      const selectedCurr /** set selected curreny when pens modal */ =
        currencyData(constantsState).find(c => c.value == data.projectSettings.currency) || {};

      this.setState(
        {
          subscription: data.projectSettings.billingMode.toString(),
          billingMethod: billinMethd,
          selectedFee: selectedFee,
          amount:
            data.projectSettings.projectBillingMethod == 0
              ? data.projectSettings.budget
              : data.projectSettings
                .hourlyRate /** if billing method is fixed fee then assign total project fee other wise assign horly rate value coming from api */,
          budget: data.projectSettings.projectBudget,
          selectedCurrency: selectedCurr,
        },
        () => { }
      );
    }
  };

  componentDidMount() {
    this.updateContent();
  }

  componentDidUpdate(prevProps, prevState) {
    const { data, setTaskDetails } = this.props;
    const { } = this.state;
    if (JSON.stringify(data) !== JSON.stringify(prevProps.data)) {
      this.updateContent();
    }
    if (
      JSON.stringify(data.projectSettings.projectId) !==
      JSON.stringify(prevProps.data.projectSettings.projectId)
    ) {
      //Close Task detils view if project is changed, this will run in case of docked view
      setTaskDetails(false);
    }
    if (
      JSON.stringify(data.projectSettings.tasks) !==
      JSON.stringify(prevProps.data.projectSettings.tasks)
    ) {
      /** if project setting coming from props changes then tasks must be updated, this use effect work as component did update */
      this.initProjectTasks();
    }
  }

  handleTabChange = (event, value) => {
    /** function for toggleing the tabs according to permission given to user */
    const { taskDetails, setTaskDetails, permission } = this.props;
    switch (value) {
      case 0: {
        /** 0 means details tab */
        if (permission.detailsTabs.cando)
          this.setState({ tabvalue: value, onClickTask: {} }, () => { });
        break;
      }
      case 1: {
        /** 1 means resources tab */
        if (permission.resourceTabs.cando)
          this.setState({ tabvalue: value, onClickTask: {} }, () => { });
        break;
      }
      case 2: {
        /** 2 means task tab */
        if (permission.taskTabs.cando)
          this.setState({ tabvalue: value, onClickTask: {} }, () => { });
        break;
      }
      case 3: {
        /** 3 means milestone tab */
        if (permission.mileStoneTab.cando)
          this.setState({ tabvalue: value, onClickTask: {} }, () => { });
        break;
      }
      case 4: {
        /** 4 means financial summary tab */
        if (permission.financialSummaryTab.cando)
          this.setState({ tabvalue: value, onClickTask: {} }, () => { });
        break;
      }
      case 5: {
        /** 5 means Work Calendar tab */
        // if (permission.calenderTab.cando)
        this.setState({ tabvalue: value, onClickTask: {} }, () => { });
        break;
      }
    }
    if (!isEmpty(taskDetails)) {
      // close task details when user switch the tab
      setTaskDetails("");
    }
  };

  getProjectCreatedBy = userId => {
    const { profileState } = this.props;
    if (userId) {
      const createdBy = profileState.data.member.allMembers.find(m => m.userId == userId) || {};
      return createdBy.fullName;
    } else return "";
  };

  handleShowTaskDetails = task => {
    // Function that show task details
    this.props.setTaskDetails(task);
  };

  getLastSavedValues = () => {
    const { theme, classes, data, intl, saving } = this.props;
    let lastSaved;
    let date = data.projectSettings.updatedDate;
    let updatedDate = moment(date);
    let currentDate = moment();
    if (currentDate.diff(updatedDate, "days") == 0) {
      /** 0 means if user updated project today */
      lastSaved = moment(date).calendar();
    } else if (currentDate.diff(updatedDate, "days") == 1) {
      /** 1 means if user updated project yesterday */
      lastSaved = moment(date)
        .subtract(1, "days")
        .calendar();
    } else if (currentDate.diff(updatedDate, "days") < 365) {
      /** 365 means if user updated project current year */
      lastSaved = `${moment(date)
        .format("LL")
        .substring(0, 7)} at ${moment(date).format("LT")}`;
    } else {
      /** else means if user updated project last year */
      lastSaved = `${moment(date).format("LL")} at ${moment(date).format("LT")}`;
    }
    return (
      <span className={classes.lastSaved}>
        {saving ? (
          <FormattedMessage id="project.dev.footer.savinglabel" defaultMessage="Saving..." />
        ) : (
          `${intl.formatMessage({
            id: "project.dev.footer.lastsavedlabel",
            defaultMessage: "Last saved :",
          })} ${lastSaved}`
        )}
      </span>
    );
  };

  handlePlanChangePayment = (event, paymentPlan) => {
    const { data = {}, } = this.props;
    /** function call when user select subscription, is it billable or non billable (button) from detail tab view */
    if (paymentPlan) {
      this.setState(
        {
          subscription: paymentPlan,
        },
        () => {
          this.updateProjectSettings("billingMode", paymentPlan, null, () => {
            this.props.UpdateProjectDetailListing(null, { billingType: Number(paymentPlan) }, data.projectSettings.projectId)
          });
        }
      );
    }
  };

  updateProjectSettings = (e, val, otherParam = {}, updateStore) => {
    const {
      projectSettings,
      setSaving,
      data,
      saveProjectSettings,
      showSnackBar,
      selectedProject,
    } = this.props;
    const { } = this.state;
    setSaving(true);
    let settings = cloneDeep(data.projectSettings);
    switch (e) {
      case "billingMode":
        settings.billingMode = val;
        break;

      case "currency":
        settings.currency = val;
        break;

      case "projectBillingMethod":
        settings.projectBillingMethod = val;
        let feetype = feeMethodData(otherParam)[0];
        settings.feeType = feetype.value;
        settings.budget = "";
        settings.hourlyRate = "";
        this.setState({ amount: "", selectedFee: feetype }, () => { });
        break;

      case "feeType":
        settings.feeType = val;
        if (val == 1) {
          settings.budget = "";
          settings.hourlyRate = "";
          this.setState({ amount: "" }, () => { });
        }
        break;

      case "budget":
        settings.budget = val;
        break;

      case "projectBudget":
        settings.projectBudget = val;
        break;

      case "hourlyRate":
        settings.hourlyRate = val;
        break;

      case "resources":
        settings.resources = val;
        break;

      case "tasks":
        settings.tasks = val;
        break;

      case "sendEmailAlert":
        settings.sendEmailAlert = val;
        break;

      case "alertLimit":
        settings.alertLimit = val;
        break;

      case "customFieldData":
        settings.customFieldData = val;
        break;

      default:
        break;
    }
    saveProjectSettings(
      settings,
      succ => {
        setSaving(false);
        this.setState({ isSubTaskCreated: false });
        updateStore()
      },
      err => {
        setSaving(false);
        if (err && err.data) showSnackBar(err.data.message, "error");
        this.setState({ isSubTaskCreated: false });
      },
      selectedProject
    );
  };

  updateSplitProjectSettings = (val, updatedTaskArr, otherParam = {}) => {
    const { setSaving, data, saveBillableSetting, showSnackBar, selectedProject } = this.props;
    setSaving(true);
    let settings = cloneDeep(data.projectSettings);
    settings.tasks = updatedTaskArr;
    saveBillableSetting(
      val,
      settings,
      succ => {
        setSaving(false);
        if (succ && succ.data) showSnackBar(succ.data.message, "success")
      },
      err => {
        setSaving(false);
        if (err && err.data) showSnackBar(err.data.message, "error");
      },
      selectedProject
    );
  };

  handleShowHideTaskDetails = t => {
    const { setTaskDetails, projectDetails } = this.props;
    // Function that show/hide task details
    let task = this.getTaskPermission(t);
    setTaskDetails(task);
    projectDetails({
      chatView: false,
      docsView: false,
      activityLogView: false,
      loadingState: false,
    });
    // this.setState({onClickTask: {}  }, () => {});
  };
  hideTaskDetails = () => {
    const { setTaskDetails } = this.props;

    this.setState({ onClickTask: {} }, () => {
      setTaskDetails(null);
    });
  };

  getTaskPermission = t => {
    const { allProjects, taskPer } = this.props;
    /** pushing the task permission object in the task , if task has linked with any project i-e projectId !== null then find the project and fetch the task permission in project object */
    if (t) {
      if (t.entity.projectId) {
        let attachedProject = allProjects.find(p => p.projectId == t.entity.projectId);
        if (attachedProject) {
          t.entity.taskPermission = attachedProject.projectPermission.permission.task;
        } else t.entity.taskPermission = taskPer;
        return t;
      } else {
        t.entity.taskPermission = taskPer;
        return t;
      }
    }
  };

  getTaskDetails = task => {
    const { getTaskById, setTaskDetailsLoading } = this.props;

    setTaskDetailsLoading(true);
    this.props.projectDetails({ loadingState: true });

    this.setState({ breadCrumbArr: [], onClickTask: task }, () => { });
    this.getBreadCrumbData(task);
    getTaskById(
      task.taskId,
      //Success
      response => {
        this.handleShowHideTaskDetails(response);
        setTaskDetailsLoading(false);
      },
      //Failure
      err => {
        if (err) {
          setTaskDetailsLoading(false);
          this.handleShowHideTaskDetails("");
        }
      }
    );
  };

  getBreadCrumbData = (task = {}) => {
    const { data } = this.props;
    if (!isEmpty(task) && task.parentId) {
      let parentTask = data.projectSettings.tasks.find(t => t.taskId == task.parentId) || {};
      if (!isEmpty(parentTask)) {
        sortedBreadCrumbArr.unshift(parentTask);
        this.setState({ breadCrumbArr: sortedBreadCrumbArr }, () => {
          this.getBreadCrumbData(parentTask);
        });
      } else sortedBreadCrumbArr = [];
    } else sortedBreadCrumbArr = [];
  };

  initProjectResources = () => {
    //initialization state with project resources in list
    const { data } = this.props;
    const projectResources = data.projectSettings.resources;
    // const projectResourcesList = differenceWith(
    //   workspaceMembers,
    //   projectResources,
    //   (a, b) => {
    //     return a.userId != b.userId;
    //   }
    // );
    this.setState({ resources: projectResources });
  };

  /** task tab function starts from here  */
  initProjectTasks = () => {
    //initialization state with project tasks in list
    const { data } = this.props;
    const projectTasksList = data.projectSettings.tasks;
    this.sortedProjects(projectTasksList);
  };

  sortedProjects = (projectTasksList = []) => {
    /** sorting the task array for implementing parent and its child Tasks */
    let arr = [];
    const parentTasks = projectTasksList.filter(p => !p.parentId) || {};
    parentTasks.forEach(pT => {
      let childTasks = projectTasksList.filter(cT => cT.parentId == pT.taskId);
      if (childTasks.length > 0) {
        arr.push(pT);
        Array.prototype.push.apply(arr, childTasks);
      } else {
        arr.push(pT);
      }
    });
    this.setState({ projectTasks: arr });
  };

  initAllTasks = () => {
    this.setState({ allTasks: this.generateTaskData() });
  };

  generateTaskData = () => {
    const { tasks } = this.props;
    let noProjectTasks = tasks.filter(task => {
      return !task.projectId && !task.parentId;
    });
    let ddData = noProjectTasks.map((task, i) => {
      return generateSelectData(task.taskTitle, task.taskTitle, task.taskId, task.uniqueId, task);
    });
    return ddData;
  };

  handleActionsSelect = (opt: string, task: object) => {
    /** function calls on select of diff option from action drop down */
    const { publickLinkDialog } = this.props;

    switch (opt) {
      case "publickLink":
        publickLinkDialog({ taskId: task.taskId, taskTitle: task.taskTitle });
        break;
      case "copyTask":
        this.duplicateTask(task);
        break;
      case "archiveTask":
        this.archiveTask(task);
        break;
      case "deleteTask":
        this.deleteTask(task);
        break;
      case "unlinkTask":
        this.unLinkTask(task);
        break;

      default:
        break;
    }
  };

  duplicateTask = (obj: object) => {
    /** function calls when user select the copy task option */
    const { CopyTask, showSnackBar, tasks } = this.props;
    let task = tasks.find(f => f.id == obj.taskId) || {};
    const clientId = uuidv4();
    const postObj = {
      ...emptyTask,
      taskTitle: `Copy of ${task.taskTitle}`,
      clientId,
      id: clientId,
      isNew: true,
    };
    CopyTask(
      /** copy task APi */
      null,
      task.taskId,
      postObj,
      succ => {
        let duplicatedTask = { obj: succ };
        this.handleOptionsSelect("task", duplicatedTask); /** add copy task in the task Arr */
        showSnackBar("Task Copied Successfully", "success");
      },
      err => {
        if (!isUndefined(err.data)) showSnackBar(err.data.message, "error");
      }
    );
  };

  archiveTask = (obj: object) => {
    /** function calls when user want to archieve certain task */
    const { ArchiveTask, taskDetails, showSnackBar, tasks, setTaskDetails } = this.props;
    let task = tasks.find(f => f.id == obj.taskId) || {};
    ArchiveTask(
      /** Archive task API calls */
      task.taskId,
      response => {
        this.filterTasks(obj);
        if (!isEmpty(taskDetails.entity) && obj.taskId == taskDetails.entity.id) {
          /** if task is selected on left side and its details in open on right side UI on delete action the task details dialog must close */
          // dispatch(StoreDeleteTaskInfo(response.data.taskId));
          // dispatch(DeleteArchiveTaskFromBoard(response.data));
          // dispatch(updateGlobalTimeTask(response.data));
          setTaskDetails(false);
        }
        showSnackBar("Task Archived Successfully", "success");
      },
      err => {
        if (!isUndefined(err.data)) showSnackBar(err.data.message, "error");
      }
    );
  };

  deleteTask = (obj: object) => {
    /** function call when user wants to delete the task */
    const { DeleteTask, taskDetails, showSnackBar, tasks, setTaskDetails } = this.props;

    let task = tasks.find(f => f.id == obj.taskId) || {};
    DeleteTask(
      task,
      succ => {
        this.filterTasks(obj); /** filter task from state */
        if (!isEmpty(taskDetails) && obj.taskId == taskDetails.entity.id) {
          /** if task is selected on left side and its details in open on right side UI on delete action the task details dialog must close */
          setTaskDetails("");
        }
        showSnackBar("Task Deleted Successfully", "success");
      },
      err => {
        if (!isUndefined(err) || !isUndefined(err.data)) showSnackBar(err.data.message, "error");
      }
    );
  };

  unLinkTask = (obj: object) => {
    /** function call when user clicks the unlink option from action drop down */
    const { showSnackBar, tasks } = this.props;

    let task = tasks.find(f => f.id == obj.taskId) || {};
    if (task.timeLogged && task.timeLogged !== "00:00") {
      showSnackBar(
        this.props.intl.formatMessage({
          id: "task.detail-dialog.project.hint1",
          defaultMessage: "You cannot change project once user effort is added to task or you don't have permission to change project",
        }),
        "error"
      );
      return;
    }
    this.filterTasks(obj); /** it will filter the task */
    this.linkUnlinkGlobalTaskWithProject(
      task,
      "unlink"
    ); /** it will unlink the project from task by doing project ID null in task object */
    showSnackBar(
      this.props.intl.formatMessage({
        id: "common.successMessages.task-removed",
        defaultMessage: "Task successfully removed from project.",
      }),
      "success"
    );
  };

  handleColorSelect = (
    color: string,
    task: object,
    colorControlType = "taskListView" /** it indicates if user is changing color from task detail view or list view */
  ) => {
    /** function calls when user changes the color of a certain task from action drop down */
    const { showSnackBar, tasks, UpdateTask, updateTaskData } = this.props;
    const { projectTasks } = this.state;

    let object = {};
    if (colorControlType == "taskListView") {
      object = task;
    } else if (colorControlType == "taskDetailView") {
      object = projectTasks.find(t => t.taskId == task.id) || {};
    }
    object.colorCode = color;
    this.updateTaskDetailsIfOpen(
      object
    ); /** update task details if the task is open in task details view */
    this.UpdateStateAndCallApi(object);

    let updatedTask =
      tasks.find(t => t.id == task.taskId || t.id == task.id) ||
      {}; /** updating color in store also */
    const obj = { colorCode: color };
    updateTaskData({ task: updatedTask, obj }, () => { }, () => { });
    // UpdateTask(updatedTask, callback => {});
  };

  UpdateStateAndCallApi = (object: object) => {
    const { projectSettings, data } = this.props;

    let updatedArr = data.projectSettings.tasks.map(m => {
      if (m.taskId == object.taskId) {
        return object;
      } else return m;
    });
    this.sortedProjects(updatedArr);
  };

  filterTasks = (obj: object) => {
    const {
      projectSettings,
      saveSubTask,
      taskDetails,
      setTaskDetails,
      updateStoreTask,
      showSnackBar,
      data,
      tasks
    } = this.props;

    let filterTasksArr = data.projectSettings.tasks.filter(t => t.taskId != obj.taskId);
    this.updateProjectSettings("tasks", filterTasksArr, {}, () => {
      updateProject({ budget: amount })
    });

    if (obj.parentId) {
      /** if task has parent id it means it is a child task */

      /** if task detail view is open and user unlink subtask then it should remove from task detail view of sub task section */
      let object = {
        parentTaskId: obj.parentId,
        childTaskId: obj.taskId,
        IsToAdd: false,
      };
      saveSubTask(
        object,
        succ => {
          if (succ.data && succ.data.entity) {
            if (!isEmpty(taskDetails)) {
              let obj = cloneDeep(succ.data.entity);
              obj = this.getTaskPermission(succ.data.entity);
              setTaskDetails(obj);
            }
            updateStoreTask(succ.data.entity.entity);
            let subTaskObj = tasks.find(t => t.id == obj.taskId) || {};
            if (!isEmpty(subTaskObj)) this.linkUnlinkParentIdFromTask(subTaskObj, "unlink");
          }
        },
        err => {
          if (!isUndefined(err.data)) showSnackBar(err.data.message, "error");
        }
      );
    }
  };

  handleOptionsSelect = (type: string, value: object) => {
    const { data, selectedProject, showSnackBar } = this.props;
    const { allTasks, billingMethod } = this.state;
    let defaultProjectStatus = selectedProject.projectTemplate.statusList.find(
      item => item.isDefault
    );
    if (value.obj.timeLogged && value.obj.timeLogged !== "00:00") {
      /** if task have logged time then do not add to project */
      showSnackBar("Oops! You cannot assign a task to project once time has been logged against the task.",
        "error");
      return;
    }
    let object = {
      taskId: value.obj.id,
      isBillable: (billingMethod.value === 2 || billingMethod.value === 1) && data.projectSettings.billingMode == 1,
      hourlyRate: 0,
      fixedFee: 0,
      status: defaultProjectStatus.statusId,
      estimatedHrs: value.obj.estimatedHrs,
      estimatedMins: value.obj.estimatedMins,
      estimatedTimeMin: value.obj.estimatedHrs * 60 + value.obj.estimatedMins,
    };

    this.setState(
      {
        allTasks: allTasks.filter(t => {
          return t.obj.taskId != value.obj.taskId;
        }),
      },
      () => {
        this.updateProjectSettings("tasks", [object, ...data.projectSettings.tasks], {}, () => {
          updateProject({ budget: amount })
        });
        this.linkUnlinkGlobalTaskWithProject(value.obj, "link");
      }
    );
  };

  handleSelectSubtask = (type: string, value: object) => {
    const { allTasks } = this.state;
    const { saveSubTask, setTaskDetails, updateStoreTask, showSnackBar, taskDetails, selectedProject } = this.props;
    if (value.obj.timeLogged && value.obj.timeLogged !== "00:00") {
      showSnackBar(
        this.props.intl.formatMessage({
          id: "task.detail-dialog.project.hint1",
          defaultMessage: "You cannot change project once user effort is added to task or you don't have permission to change project",
        }),
        "error"
      );
      return;
    }
    let selectedTask = cloneDeep(value.obj);
    let defaultProjectStatus = selectedProject.projectTemplate.statusList.find(
      item => item.isDefault
    );
    selectedTask.status = defaultProjectStatus.statusId;
    selectedTask.statusColor = defaultProjectStatus.statusColor;
    selectedTask.statusTitle = defaultProjectStatus.statusTitle;
    let object = {
      parentTaskId: taskDetails.entity.id,
      childTaskId: value.obj.id,
      IsToAdd: true,
      projectId: taskDetails.entity.projectId,
    };
    let updatedd = allTasks.filter(t => {
      return t.obj.taskId != value.obj.taskId;
    });

    this.setState({ allTasks: updatedd, isSubTaskCreated: true }, () => {
      saveSubTask(
        object,
        succ => {
          if (succ.data) {
            let obj = this.getTaskPermission(succ.data.entity);
            setTaskDetails(obj);
            updateStoreTask(succ.data.entity.entity);
            /** assigning project id and parent id to the subtask */
            this.linkUnlinkParentIdFromTask(selectedTask, "link");
            // this.setState({isSubTaskCreated: false});
          }
        },
        err => {
          if (!isUndefined(err.data)) showSnackBar(err.data.message, "error");
          this.setState({ isSubTaskCreated: false });
        }
      );
    });
  };

  linkUnlinkParentIdFromTask = (sT, val) => {
    /** update parentId in subtask and update task globally */
    const { taskDetails, updateStoreTask, data } = this.props;

    let subTask = cloneDeep(sT);
    if (val == "link") {
      subTask.parentId = taskDetails.entity.id;
      subTask.projectId = data.projectSettings.projectId;
      this.handleOptionsSelect("task", {
        obj: subTask,
      }); /** saving subtask in projectSettings, task left side view */
    } else if (val == "unlink") {
      subTask.parentId = null;
      subTask.projectId = null;
    }

    updateStoreTask(subTask);
  };

  handleCreateSubTask = (type: string, option: object) => {
    const { SaveTask, showSnackBar, taskDetails } = this.props;
    this.setState({ isSubTaskCreated: true });
    SaveTask(
      {
        taskTitle: option.value.trim(),
        projectId: taskDetails.entity.projectId,
      },
      succ => {
        if (succ) {
          let task = { obj: succ.task };
          this.handleSelectSubtask("task", task);
        }
      },
      err => {
        if (!isUndefined(err.data)) showSnackBar(err.data.message, "error");
        this.setState({ isSubTaskCreated: false });
      },
      null
    );
  };

  linkUnlinkGlobalTaskWithProject = (object: object, k: string) => {
    const { updateStoreTask, data, workspaceStatus, selectedProject, updateTaskData } = this.props;
    let defaultWsStatus = workspaceStatus.statusList.find(s => s.isDefault) || {};
    let defaultProjectStatus = selectedProject.projectTemplate.statusList.find(
      item => item.isDefault
    );
    let obj = {};
    let updatedTask = cloneDeep(
      object
    ); /** updating project id in selected task so task can be linked to certain project and cannot show in dropdown again if selected */
    if (k == "link") {
      obj.projectId = data.projectSettings.projectId;
      obj.status = defaultProjectStatus.statusId;
    } else if (k == "unlink") {
      obj.projectId = "";
      obj.parentId = null;
      obj.status = defaultWsStatus.statusId;
      obj.statusTitle = defaultWsStatus.statusTitle;
    }
    updateTaskData({ task: updatedTask, obj }, null, res => {
      updateStoreTask({ ...res, ...obj });
    })
  };

  onChatMount = () => {
    this.props.projectDetails({ sideView: true });
  };
  onChatUnMount = () => {
    this.props.projectDetails({ sideView: false });
  };

  updateTaskDetailsIfOpen = task => {
    /** update task details if the task is open in task details view */
    const { taskDetails, setTaskDetails } = this.props;

    if (!isEmpty(taskDetails) && task.taskId == taskDetails.entity.id) {
      let updatetask = cloneDeep(taskDetails);
      updatetask.entity.taskTitle = task.taskTitle;
      updatetask.entity.colorCode = task.colorCode;
      setTaskDetails(updatetask);
    }
  };

  setBillingMethod = method => {
    this.setState({ billingMethod: method });
  };

  setSelectedFee = fee => {
    this.setState({ selectedFee: fee });
  };

  setAmount = amoun => {
    this.setState({ amount: amoun });
  };

  setSelectedCurrency = curr => {
    this.setState({ selectedCurrency: curr });
  };

  setBudget = budg => {
    this.setState({ budget: budg });
  };

  setResources = projectResources => {
    this.setState({ resources: projectResources });
  };

  setProjectTasks = pTs => {
    this.setState({ projectTasks: pTs });
  };

  setAllTasks = Tasks => {
    this.setState({ allTasks: Tasks });
  };

  updateTaskInProjectModal = data => {
    const { projectDetails, projectSetting } = this.props;
    let settings = cloneDeep(projectSetting);

    let updatedTasks = cloneDeep(this.state.projectTasks);
    let newMapping = data.mapping;
    if (newMapping.length > 0) {
      let modifiedTasks = updatedTasks
        ? updatedTasks.map(el => {
          let statusItem = newMapping.find(item => item.statusIDFrom == el.status);
          if (statusItem) {
            el.status = statusItem.statusIDTo;
            el.statusTitle = statusItem.toStatusTitle;
          }
          return el;
        })
        : [];
      settings.projectSettings.tasks = updatedTasks;
      this.setState({ projectTasks: modifiedTasks }, () => {
        projectDetails({ projectSettings: settings.projectSettings });
      });
    }
  };

  render() {
    const {
      tabvalue,
      selectedFee,
      amount,
      budget,
      selectedCurrency,
      billingMethod,
      projectTasks,
      allTasks,
      breadCrumbArr,
      subscription,
      resources,
      isSubTaskCreated,
      onClickTask,
    } = this.state;
    const {
      theme,
      classes,
      data = {},
      projectDescription,
      setProjectDescription,
      selectedProject = {},
      saving = false,
      constantsState,
      setSaving,
      showSnackBar,
      SaveTask,
      updateStoreTask,
      publickLinkDialog,
      CopyTask,
      ArchiveTask,
      DeleteTask,
      UpdateTask,
      tasks,
      openChat,
      saveSubTask,
      projectDetails,
      projectSetting,
      permission,
      taskDetails,
      setTaskDetails,
      allProjects,
      taskPer,
      addTasksInProject,
      taskDetailsLoading,
      setTaskDetailsLoading,
      intl,
      setSelectedProject,
      profileState,
      company,
    } = this.props;
    const { projectSettings } = this.props.data;
    const currentUser = profileState.data.teamMember.find(m => {
      return profileState.data.userId == m.userId;
    });

    const { paymentPlanTitle } = company.subscriptionDetails;
    const chatPermission = {
      addAttachment: permission.projectComments.projectattachment.isAllowAdd,
      deleteAttachment: permission.projectComments.projectattachment.isAllowDelete,
      downloadAttachment: permission.projectComments.downloadAttachment.cando,
      addComments: permission.projectComments.comments.isAllowAdd,
      editComment: permission.projectComments.comments.isAllowEdit,
      deleteComment: permission.projectComments.comments.isAllowDelete,
      convertToTask: permission.projectComments.convertToTask.cando,
      reply: permission.projectComments.reply.cando,
      replyLater: permission.projectComments.replyLater.cando,
      showReceipt: permission.projectComments.showReceipt.cando,
      clearAll: permission.projectComments.clearAllReplies.cando,
      clearReply: permission.projectComments.clearReply.cando,
    };
    const docPermission = {
      uploadDocument: permission.projectDocuments.uploadDocument.cando,
      addFolder: permission.projectDocuments.addFolder.cando,
      downloadDocument: permission.projectDocuments.downloadDocument.cando,
      viewDocument: permission.projectDocuments.cando,
      deleteDocument: permission.projectDocuments.deleteDocument.cando,
      openFolder: permission.projectDocuments.openFolder.cando,
      renameFolder: permission.projectDocuments.renameFolder.cando,
    };
    const chatConfig = {
      contextUrl: "communication",
      contextView: "Project",
      contextId: selectedProject.projectId,
      contextKeyId: "projectId",
      contextChat: "single",
      assigneeLists: profileState.data.member.allMembers.filter(
        member => resources.find(resrce => resrce.userId == member.userId) != undefined
      ),
    };



    return (
      <div className={classes.projectContentCnt}>
        <div
          className={
            projectSetting.docked ? classes.projectContentLeftDocked : classes.projectContentLeft
          }>
          <Tabs
            value={tabvalue}
            // action={obj => {
            //   setTimeout(() => {
            //     obj.updateIndicator();
            //   }, 500);
            // }}
            onChange={this.handleTabChange}
            fullWidth
            classes={{
              root: classes.TabsRoot,
              indicator: classes.tabIndicator,
            }}>
            <Tab
              disableRipple={true}
              classes={{
                root: classes.tab,
                wrapper: classes.tabLabelCnt,
                selected: classes.tabSelected,
              }}
              label={
                <Typography variant="h6" className={classes.tabLabel}>
                  <FormattedMessage id="project.dev.details.title" defaultMessage="Details" />{" "}
                </Typography>
              }
            />
            <Tab
              disableRipple={true}
              classes={{
                root: classes.tab,
                wrapper: classes.tabLabelCnt,
                selected: classes.tabSelected,
              }}
              label={
                <Typography variant="h6" className={classes.tabLabel}>
                  <FormattedMessage id="project.dev.resources.title" defaultMessage="Resources" />
                  {` (${data.projectSettings.resources.length}) `}
                </Typography>
              }
            />
            {/* {CanAccess({ group: 'task', feature: 'project' }) ? */}
            <Tab
              disableRipple={true}
              classes={{
                root: classes.tab,
                wrapper: classes.tabLabelCnt,
                selected: classes.tabSelected,
              }}
              label={
                <Typography variant="h6" className={classes.tabLabel}>
                  <FormattedMessage id="project.dev.tasks.title" defaultMessage="Tasks" />
                  {/* {` (${projectTasks.length}) `} */}
                  {` (${data.projectSettings.tasks.length}) `}
                </Typography>
              }
            />
            {/* : null} */}
            <Tab
              disableRipple={true}
              classes={{
                root: classes.tab,
                wrapper: classes.tabLabelCnt,
                selected: classes.tabSelected,
              }}
              label={
                <Typography variant="h6" className={classes.tabLabel}>
                  <FormattedMessage id="project.dev.milestones.title" defaultMessage="Milestones" />
                  {` (${data.projectSettings.mileStones.length})`}
                </Typography>
              }
            />

            {permission.financialSummaryTab.cando && CanAccess({ group: 'project', feature: 'billingType' }) && (
              <Tab
                disableRipple={true}
                classes={{
                  root: classes.tab,
                  wrapper: classes.tabLabelCnt,
                  selected: classes.tabSelected,
                }}
                label={
                  <Typography variant="h6" className={classes.tabLabel}>
                    <FormattedMessage
                      id="project.dev.fianancial-summary.title"
                      defaultMessage="Financial Summary"
                    />{" "}
                  </Typography>
                }
              />
            )}
            {(currentUser.role == "Owner" || currentUser.role == "Admin") && (isAnyBusinessPlan(paymentPlanTitle) || isEnterprisePlan(paymentPlanTitle)) && (
              <Tab
                disableRipple={true}
                classes={{
                  root: classes.tab,
                  labelContainer: classes.tabLabelCnt,
                  selected: classes.tabSelected,
                }}
                label={
                  <Typography variant="h6" className={classes.tabLabel} >
                    <FormattedMessage id="project.work-calendar.title" defaultMessage="Work Calendar" />
                  </Typography>
                }
              />
            )}

            {/* <Tab
              disableRipple={true}
              classes={{
                root: classes.tab,
                labelContainer: classes.tabLabelCnt,
                selected: classes.tabSelected,
              }}
              label={
                <Typography variant="h6" className={classes.tabLabel}>
                  Project Statuses
                </Typography>
              }
            /> */}
          </Tabs>

          {tabvalue === 0 && (
            <div className={classes.dialogContent}>
              <Details
                // data={data}
                updateProjectSettings={this.updateProjectSettings}
                projectDescription={projectDescription}
                setProjectDescription={setProjectDescription}
                subscription={subscription}
                handlePlanChangePayment={this.handlePlanChangePayment}
                billingMethod={billingMethod}
                setBillingMethod={this.setBillingMethod}
                selectedFee={selectedFee}
                setSelectedFee={this.setSelectedFee}
                amount={amount}
                setAmount={this.setAmount}
                changeTab={this.handleTabChange}
                selectedCurrency={selectedCurrency}
                setSelectedCurrency={this.setSelectedCurrency}
                permission={permission}
                intl={intl}
                budget={budget}
                setBudget={this.setBudget}
              />
            </div>
          )}
          {tabvalue === 1 && (
            <>
              <div className={classes.dialogContent}>
                <ProjectResources
                  projectSettings={data.projectSettings}
                  updateProjectSettings={this.updateProjectSettings}
                  permission={permission}
                  resources={resources}
                  setResources={this.setResources}
                  intl={intl}
                  billingMethod={billingMethod}
                />
              </div>
            </>
          )}
          {tabvalue === 2 && (
            <ProjectTasks
              projectSettings={data.projectSettings}
              selectedProject={selectedProject}
              setSelectedProject={setSelectedProject}
              updateProjectSettings={this.updateProjectSettings}
              updateSplitProjectSettings={this.updateSplitProjectSettings}
              createTask={SaveTask}
              updateTask={updateStoreTask}
              taskDetails={taskDetails}
              getTaskById={this.getTaskDetails}
              projectTasks={projectTasks}
              setProjectTasks={this.setProjectTasks}
              allTasks={allTasks}
              setAllTasks={this.setAllTasks}
              handleActionsSelect={this.handleActionsSelect}
              handleOptionsSelect={this.handleOptionsSelect}
              handleColorSelect={this.handleColorSelect}
              updateTaskDetailsIfOpen={this.updateTaskDetailsIfOpen}
              showSnackBar={showSnackBar}
              permission={permission}
              addTasksInProject={addTasksInProject}
              sortedProjects={this.sortedProjects}
              intl={intl}
              docked={data.docked}
              onClickTask={onClickTask}
              updateTaskInProjectModal={this.updateTaskInProjectModal}
              saving={this.props.saving}
            />
          )}
          {tabvalue === 3 && (
            <div className={classes.dialogContent}>
              <MileStones
                data={data}
                updateProjectSettings={this.updateProjectSettings}
                setSaving={setSaving}
                showSnackBar={showSnackBar}
                permission={permission}
                intl={intl}
              />
            </div>
          )}
          {tabvalue === 4 && (
            <div className={classes.dialogContent}>
              <FinancialSummary
                projectSettings={data.projectSettings}
                billingMethod={billingMethod}
                selectedFee={selectedFee}
                updateProjectSettings={this.updateProjectSettings}
                permission={permission}
                intl={intl}
              />
            </div>
          )}
          {tabvalue === 5 && (
            <div className={classes.dialogContent}>
              <ProjectCalendar
                value={projectSetting.projectSettings.calender}
                projectId={projectSetting.projectSettings.projectId}
              />
            </div>
          )}

          {tabvalue === 1 && (
            <NotificationMessage
              type="info"
              iconType="info"
              style={{
                // width: "calc(100% - 50px)",
                // padding: "10px 15px 10px 15px",
                backgroundColor: "rgba(243, 243, 243, 1)",
                borderRadius: 4,
                margin: "10px 20px",
              }}>
              <FormattedMessage
                id="project.dev.resources.note.label"
                defaultMessage="Team owner, team admin and workspace admin have full access of this
              project."
              />
            </NotificationMessage>
          )}

          <div
            className={classes.createdBy}
          // style={projectSetting.docked ? { position: "absolute" } : null}
          >
            <span className={classes.createdOn}>
              <SvgIcon
                viewBox="0 0 14 11.195"
                style={{ fontSize: theme.typography.fontSize, marginRight: 10 }}>
                <IconActivitySmall />
              </SvgIcon>
              <FormattedMessage
                id="project.dev.footer.leftlabel"
                values={{
                  t1: moment(selectedProject.createdDate).format("LL"),
                  t2: moment(selectedProject.createdDate).format("LT"),
                  // t3: this.getProjectCreatedBy(selectedProject.createdBy),
                  t3: selectedProject.createdBy,
                }}
                defaultMessage={`Created on:
              ${moment(selectedProject.createdDate).format("LL")} at ${moment(
                  selectedProject.createdDate
                ).format("LT")} by ${selectedProject.createdBy}`}
              />
            </span>
            {this.getLastSavedValues()}
          </div>
        </div>

        {taskDetailsLoading ? (
          <div
            className={
              projectSetting.docked ? classes.projectContentRightDock : classes.projectContentRight
            }>
            <div className={classes.taskDetailsCnt}>
              {/* <ListLoader style={{ padding: 20 }} /> */}
              <div
                style={{
                  height: "100%",
                  width: "100%",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}>
                <div className={`loader`}></div>
              </div>
            </div>
          </div>
        ) : projectSetting.chatView || projectSetting.docsView ? (
          <div style={{ width: "45%", height: "100%", display: "flex" }}>
            <ChatBuilder
              onMount={this.onChatMount}
              onUnmount={this.onChatUnMount}
              chatConfig={chatConfig}
              chatPermission={chatPermission}
              docConfig={chatConfig}
              docPermission={docPermission}
              selectedTab={projectSetting.docsView ? 2 : 0}
              intl={intl}
            />
          </div>
        ) : projectSetting.activityLogView ? (
          <div style={{ width: "45%", maxWidth: 528, height: "100%", display: "flex" }}>
            <ProjectActivity
              classes={classes}
              MenuData={projectSetting}
              theme={theme}
              members={this.props.profileState.data.member.allMembers}
              projectId={selectedProject.projectId}
            />
          </div>
        ) : taskDetails &&
          !projectSetting.chatView &&
          !projectSetting.docsView &&
          !projectSetting.activityLogView ? (
          <div
            className={
              projectSetting.docked ? classes.projectContentRightDock : classes.projectContentRight
            }>
            <div className={classes.taskDetailsCnt}>
              <TaskDetails
                taskDetails={taskDetails}
                handleShowHideTaskDetails={this.handleShowHideTaskDetails}
                updateProjectSettings={this.updateProjectSettings}
                projectSettings={data.projectSettings}
                handleActionsSelect={this.handleActionsSelect}
                setTaskDetails={setTaskDetails}
                allTasks={allTasks}
                handleSelectSubtask={this.handleSelectSubtask}
                handleCreateSubTask={this.handleCreateSubTask}
                isSubTaskCreated={isSubTaskCreated}
                UpdateTask={UpdateTask}
                showSnackBar={showSnackBar}
                handleColorSelect={this.handleColorSelect}
                getTaskById={this.getTaskDetails}
                breadCrumbArr={breadCrumbArr}
                UpdateStateAndCallApi={this.UpdateStateAndCallApi}
                projectTasks={projectTasks}
                permission={permission}
                setProjectTasks={this.setProjectTasks}
                intl={intl}
                hideTaskDetails={this.hideTaskDetails}
                selectedProject={selectedProject}
              />
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}

ProjectContentLeft.defaultProps = {
  classes: {},
  theme: {},
  data: {},
  selectedProject: {},
  projectDescription: "",
  saving: false,
  constantsState: [],
  setSaving: () => { },
  showSnackBar: () => { },
  setProjectDescription: () => { },
  publickLinkDialog: () => { },
  CopyTask: () => { },
  ArchiveTask: () => { },
  DeleteTask: () => { },
  UpdateTask: () => { },
  SaveTask: () => { },
  updateStoreTask: () => { },
  tasks: [],
  saveSubTask: () => { },
  openChat: false,
  permission: {},
  taskDetails: {},
  setTaskDetails: () => { },
  addTasksInProject: () => { },
  setSelectedProject: () => { },
  allProjects: [],
  taskPer: {},
  workspaceStatus: {},
};

const mapStateToProps = state => {
  let company = state.profile.data.teams.find(
    (item) => item.companyId == state.profile.data.activeTeam
  );
  return {
    profileState: state.profile,
    data: state.projectDetailsDialog,
    constantsState: state.constants,
    tasks: state.tasks.data,
    company: company,
    projectSetting: state.projectDetailsDialog,
    allProjects: state.projects.data || [],
    taskPer: state.workspacePermissions.data.task,
    workspaceStatus: state.workspaceTemplates.data.defaultWSTemplate,
  };
};

export default compose(
  withRouter,
  withSnackbar,
  injectIntl,
  withStyles(styles, {
    withTheme: true,
  }),
  connect(mapStateToProps, {
    saveProjectSettings,
    saveSplitProjectSettings,
    saveBillableSetting,
    SaveTask,
    updateStoreTask,
    CopyTask,
    ArchiveTask,
    DeleteTask,
    UpdateTask,
    publickLinkDialog,
    saveSubTask,
    projectDetails,
    getTaskById,
    UpdateProjectDetailListing,
    updateTaskData
  })
)(ProjectContentLeft);
