// @flow
import React, { useState, useEffect } from "react";
import taskStyles from "./style";
import Hotkeys from "react-hot-keys";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect, useDispatch } from "react-redux";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import AmountInput from "../../../../components/Form/AmountInput/AmountInput";
import DefaultTextField from "../../../../components/Form/TextField";
import moment from "moment";
import StatusDropdown from "../../../../components/Dropdown/StatusDropdown/Dropdown";
import { statusDataNew } from "../../../../helper/taskDropdownData";
import CreateableSelectDropdown from "../../../../components/Dropdown/SelectCreateableDropdown/SelectCreateableDropdown";
import ChevronDown from "@material-ui/icons/ArrowDropDown";
import ChevronUp from "@material-ui/icons/ArrowDropUp";
import TimeInput from "../../../../components/Form/TimeInput/TimeInput";
import cloneDeep from "lodash/cloneDeep";
import EmptyState from "../../../../components/EmptyStates/EmptyState";
import isEmpty from "lodash/isEmpty";
import MoreActionDropDown from "./MoreActionDropDown";
import { validTaskTitle } from "../../../../utils/validator/task/taskTitle";
import CustomButton from "../../../../components/Buttons/CustomButton";
import IconSubtask from "../../../../components/Icons/ProjectAddMoreIcons/IconSubtask";
import SvgIcon from "@material-ui/core/SvgIcon";
import BulkTaskImport from "./BulkTaskImport/BulkTaskImport";
import { Scrollbars } from "react-custom-scrollbars";
import { calculateHeight } from "../../../../utils/common";
import Truncate from "react-truncate";
import CustomTooltip from "../../../../components/Tooltip/Tooltip";
import CustomIconButton from "../../../../components/Buttons/CustomIconButton";
import IconEditSmall from "../../../../components/Icons/IconEditSmall";
import { UpdateTask, CheckAllTodoList, updateTaskData } from "../../../../redux/actions/tasks";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import DragIndicator from "@material-ui/icons/DragIndicator";
import { FormattedMessage } from "react-intl";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ProjectTemplates from "../ProjectTemplates/ProjectTemplates";
import CircularIcon from "@material-ui/icons/Brightness1";
import DefaultCheckbox from "../../../../components/Form/Checkbox";
import { teamCanView } from "../../../../components/PlanPermission/PlanPermission";
import UnPlanned from "../../../../Views/billing/UnPlanned/UnPlanned";
import Modal from "@material-ui/core/Modal";
import ActionConfirmation from "../../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import { CanAccess, CanAccessFeature } from "../../../../components/AccessFeature/AccessFeature.cmp";
import {TRIALPERIOD} from '../../../../components/constants/planConstant';

let priority = ["Critical", "High", "Medium", "Low"];

type DropdownProps = {
  classes: Object,
  theme: Object,
  label: String,
  onInputChange: Function,
  currency: String,
  placeholder: String,
  projectSettings: Object,
  updateProjectSettings: Function,
  updateSplitProjectSettings: Function,
  createTask: Function,
  updateTask: Function,
  taskDetails: Object,
  projectTasks: Array,
  setProjectTasks: Function,
  allTasks: Array,
  setAllTasks: Function,
  handleActionsSelect: Function,
  handleOptionsSelect: Function,
  handleColorSelect: Function,
  updateTaskDetailsIfOpen: Function,
  showSnackBar: Function,
  permission: Object,
  addTasksInProject: Function,
  UpdateTask: Function,
  sortedProjects: Function,
  setSelectedProject: Function,
  updateTaskInProjectModal: Function,
  selectedProject: Object,
  onClickTask: Object,
  workspaceStatus: Array,
  docked: Boolean,
};

//Onboarding Main Component

function ProjectTasks(props: DropdownProps) {
  const {
    classes,
    theme,
    projectSettings,
    updateProjectSettings,
    updateSplitProjectSettings,
    tasks,
    createTask,
    updateTask,
    taskDetails,
    projectTasks,
    setProjectTasks,
    allTasks,
    setAllTasks,
    handleActionsSelect,
    handleOptionsSelect,
    handleColorSelect,
    getTaskById,
    updateTaskDetailsIfOpen,
    permission,
    showSnackBar,
    addTasksInProject,
    sortedProjects,
    intl,
    selectedProject,
    setSelectedProject,
    workspaceStatus,
    docked,
    onClickTask,
    workspacePer,
    updateTaskInProjectModal,
    CheckAllTodoList,
    saving,
    companyInfo
  } = props;
  const dispatch = useDispatch();
  const [initial, setInitial] = useState("h");
  const [editTask, setEditTask] = useState({});
  const [importBulkTask, setImportBulkTask] = useState(false);
  const [tab, setTab] = useState("allTasks");
  const [unplanned, setUnplanned] = useState(false);
  const [selectedStatusObj, setSelectedStatusObj] = useState({});
  const [selectedTaskObj, setSelectedTaskObj] = useState({});
  const [markChecklistActionConf, setMarkChecklistActionConf] = useState(false);
  const [markChecklistActionBtnQuery, setMarkChecklistActionBtnQuery] = useState("");


  const handleTabChange = (event, tab) => {
    if (tab) {
      if (!teamCanView("customStatusAccess"))
        setUnplanned(true);
      else {
        setTab(tab);
      }
    }
  };

  const showImportBulkTask = () => {
    // Show Import Task View
    setImportBulkTask(true);
  };
  const hideImportBulkTask = () => {
    // Hide Import Task View
    setImportBulkTask(false);
  };

  const handleTaskClick = (event, task: object) => {
    // Function is called when single task is clicked
    event.stopPropagation();
    if (event.target.name !== "") getTaskById(task);
  };

  const markAllConfirmDialogClose = () => {
    setMarkChecklistActionConf(false);
    setSelectedTaskObj({})
    setSelectedStatusObj({})
  };

  const handleStatusChange = (status: object, obj: object) => {
    // let object = cloneDeep(obj);
    // object.status = status.value;
    // UpdateStateAndCallApi(object, true, 'taskStatus');

    // let updatedTask =
    //   tasks.find(t => t.id == obj.taskId) || {}; /** updating task status in store also */
    // updatedTask.status = status.value;
    // updatedTask.statusColor = status.statusColor
    // updatedTask.statusTitle = status.label;
    // updateTask.js(updatedTask);

    const { loggedInTeam, workspace } = props.profileState;
    const currentWorkspace = workspace.find(t => t.teamId === loggedInTeam);
    if (status.obj.isDoneState && currentWorkspace.config.isUserTasksEffortMandatory) {
      showSnackBar("Make sure you have added effort in the task", "info", {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
      });
    }
    if (status.obj.isDoneState) {
      setMarkChecklistActionConf(true);
      setSelectedTaskObj(obj)
      setSelectedStatusObj(status)
      return;
    }
    handleUpdateStatus(status, obj);
  };

  const handleUpdateStatus = (status, task) => {
    const updatedTask = cloneDeep(task);
    updatedTask.status = status.value;

    let updatedTaskObj =
      tasks.find(t => t.id == updatedTask.taskId) || {}; /** updating task status in store also */

    if (status.obj.isDoneState) {
      setMarkChecklistActionBtnQuery("progress");
      const checkData = {
        taskId: updatedTaskObj.id,
        checkAll: true,
      };
      CheckAllTodoList(checkData, succ => { }, fail => { }, props.profileState);
    }

    UpdateStateAndCallApi(updatedTask, true, 'taskStatus');


    updatedTaskObj.status = status.value;
    updatedTaskObj.statusColor = status.statusColor
    updatedTaskObj.statusTitle = status.label;
    updateTask(updatedTaskObj);

    setMarkChecklistActionConf(false);
    setSelectedTaskObj({})
    setSelectedStatusObj({})
    setMarkChecklistActionBtnQuery("");

  };

  const getPriority = (value: number = 0) => {
    switch (value) {
      case 1:
        return intl.formatMessage({
          id: "task.common.priority.dropdown.critical",
          defaultMessage: "Critical",
        });
        break;
      case 2:
        return intl.formatMessage({
          id: "task.common.priority.dropdown.high",
          defaultMessage: "High",
        });
        break;
      case 3:
        return intl.formatMessage({
          id: "task.common.priority.dropdown.medium",
          defaultMessage: "Medium",
        });
        break;
      case 4:
        return intl.formatMessage({
          id: "task.common.priority.dropdown.low",
          defaultMessage: "Low",
        });
        break;
      default:
        return "";
        break;
    }
  };
  //Updating local state when option is created in task dropdown
  const handleCreateOption = (type: string, option: object) => {
    if (!validTaskTitle(option.value)) {
      createTask(
        {
          taskTitle: option.value.trim(),
          projectId: projectSettings.projectId
        },
        succ => {
          let task = { obj: succ.task };
          handleOptionsSelect("task", task);
        },
        failure => { },
        null
      );
    }
  };

  const handleInputDuration = (hour, obj) => {
    let object = obj;
    let hours = hour == "" ? 0 : hour;
    let updatedMins = initial == "h" ? hours * 60 : hours;
    object.estimatedTimeMin = updatedMins;
    UpdateStateAndCallApi(object, true);

    updateEstimatedTimeInGlobalTask(updatedMins, obj);
  };
  const updateEstimatedTimeInGlobalTask = (num, task) => {
    let hours = Math.floor(num / 60);
    let minutes = num % 60;

    let updatedTask =
      tasks.find(t => t.id == task.taskId || t.id == task.id) ||
      {}; /** updating color in store also */
    const obj = {
      estimatedHrs: hours,
      estimatedMins: minutes
    };
    updateTaskData({ task: updatedTask, obj }, dispatch);
  };

  const UpdateStateAndCallApi = (object: object, apiCall: boolean = true, changeType: string = "") => {
    let updatedApiTasksArr = projectSettings.tasks.map(m => {
      if (m.taskId == object.taskId) {
        /** update the array of task in API*/
        return object;
      } else return m;
    });
    let updatedStateArr = projectTasks.map(m => {
      if (m.taskId == object.taskId) {
        /**  update state Arr of tasks */
        return object;
      } else return m;
    });
    if (changeType !== "taskStatus") setProjectTasks(updatedStateArr);
    if (apiCall) updateProjectSettings("tasks", updatedApiTasksArr, {});
  };

  const handleInputBlur = (v, obj: object) => {
    let object = obj;

    if (projectSettings.projectBillingMethod == 1) {
      /** 1 is for if user selected the fixed fee per task but different rate for every task */
      object.fixedFee = v;
    } else if (projectSettings.projectBillingMethod == 2) {
      /** 2 means if user selected hourly rate for every task but different hourly rate  */
      object.hourlyRate = v;
    }

    UpdateStateAndCallApi(object, true);
  };
  const handleUpdateBillable = (obj, val, event) => {
    /** function calls when user changed the billable state of the resources to true/false */
    event.stopPropagation();
    event.preventDefault();
    // let updatedObj = { ...obj, isBillable: val };
    let updatedObj = {
      projectId: projectSettings.projectId,
      taskId: obj.taskId,
      isBillable: val
    };
    UpdateSplitStateAndCallApi(updatedObj, true);
  };
  const UpdateSplitStateAndCallApi = (object: object, apiCall: boolean = true) => {
    let updatedApiTasksArr = projectSettings.tasks.map(m => {
      if (m.taskId == object.taskId) {
        /** update the array of task in API*/
        return { ...m, ...object };
      } else return m;
    });
    let updatedStateArr = projectTasks.map(m => {
      if (m.taskId == object.taskId) {
        /**  update state Arr of tasks */
        return { ...m, ...object };
      } else return m;
    });
    setProjectTasks(updatedStateArr);
    if (apiCall) updateSplitProjectSettings(object, updatedApiTasksArr, {});
  };

  const handleEstimateTitleClick = () => {
    if (initial == "h") { 
      setInitial("m");
    } else {
      setInitial("h");
    }
  };

  const handleEditTask = (event, task: object) => {
    event.stopPropagation();
    setEditTask(task);
  };

  const handleChangeTaskName = e => {
    setEditTask({ ...editTask, taskTitle: e.target.value });
  };

  const handleTaskNameBlur = () => {
    /** function calls when user changes task title */
    if (!validTaskTitle(editTask.taskTitle)) {
      /** task title validation */
      updateTaskDetailsIfOpen(
        editTask
      ); /** update task details if the task is open in task details view */
      UpdateStateAndCallApi(editTask, true);
      setEditTask({});
    } else {
      setEditTask({});
    }
  };
  const getSubtaskCount = (task, active = false) => {
    let count = projectSettings.tasks.filter(pt => pt.parentId == task.taskId).length;
    if (count > 0) {
      return (
        <>
          {" "}
          <CustomTooltip
            helptext="subtask"
            placement="bottom"
          >
            <SvgIcon
              viewBox="0 0 24 24"
              htmlColor={active ? theme.palette.border.brightBlue : theme.palette.secondary.light}
              className={classes.iconSubtask}>
              <IconSubtask />
            </SvgIcon>
          </CustomTooltip>
          <span
            className={classes.countTxt}
            style={{
              color: active ? theme.palette.border.brightBlue : "#969696",
            }}>
            {count}
          </span>{" "}
        </>
      );
    }
  };
  const getEditIcon = task => {
    return (
      <div className={classes.editIconCnt}>
        <CustomTooltip
          helptext="Edit Task Name"
          iconType="help"
          placement="top"
          style={{ color: theme.palette.common.white }}>
          <CustomIconButton
            onClick={event => {
              handleEditTask(event, task);
            }}
            btnType="transparent"
            variant="contained"
            style={{
              padding: "3px 3px 0px 0px",
              backgroundColor: "transparent",
            }}
            disabled={false}>
            <SvgIcon
              viewBox="0 0 12 11.957"
              // htmlColor={theme.palette.secondary.medDark}
              className={classes.editIcon}>
              <IconEditSmall />
            </SvgIcon>
          </CustomIconButton>
        </CustomTooltip>
      </div>
    );
  };

  const onDragEnd = result => {
    const { source, destination, draggableId, type } = result;
    const sInd = +source.droppableId;
    const dInd = +destination.droppableId;
    // Ticket is dragged through the same taskbar
    if (sInd === dInd) {
      const items = reorder(projectSettings, source.index, destination.index);
      const newState = items;
      setProjectTasks(newState.tasks);
      // sortedProjects(newState.tasks)
      updateProjectSettings("tasks", newState.tasks, {});
    }
  };

  const reorder = (list, startIndex, endIndex) => {
    const result = JSON.parse(JSON.stringify(list));
    const [removed] = result.tasks.splice(startIndex, 1);
    result.tasks.splice(endIndex, 0, removed);
    return result;
  };

  const statusData = statusArr => {
    if (statusArr && statusArr.statusList) {
      return statusArr.statusList.map(item => {
        return {
          label: item.statusTitle,
          value: item.statusId,
          icon: <CircularIcon htmlColor={item.statusColor} style={{ fontSize: "20px" }} />,
          statusColor: item.statusColor,
          obj: item
        };
      });
    }
  };

  let diffRate =
    (projectSettings.projectBillingMethod == 1 || projectSettings.projectBillingMethod == 2) &&
      projectSettings.feeType == 1
      ? true
      : false;

  const taskStatusData = statusDataNew(theme, classes);
  const taskStatusDataDropDown = statusData(
    selectedProject.projectTemplate ? selectedProject.projectTemplate : workspaceStatus
  );
  const windowHeight = calculateHeight();

  const updateCustomTemplate = template => {
    let updatedProject = cloneDeep(selectedProject);
    updatedProject.projectTemplate = template;
    setSelectedProject(updatedProject);
  };

  const taskStatusPermission = () => {
    /** if logged in user in Project Mnagaer then show task statuses other wise if contributor and viewr then do not show task statuses, but if role is not PM,Contributor and Viewr then use workspace level permission of task statuses */
    if (selectedProject.projectPermission) {
      if (selectedProject.projectPermission.roleId == "101") {
        return true;
      }
      if (
        selectedProject.projectPermission.roleId !== "101" &&
        selectedProject.projectPermission.roleId !== "102" &&
        selectedProject.projectPermission.roleId !== "103"
      ) {
        return workspacePer.workspaceSetting.tasksStatus.cando;
      }
    } else return true;
  };
  const handleClose = e => {
    setUnplanned(false);
  };
  const taskLabelSingle = companyInfo?.task?.sName;
  const taskLabelMulti = companyInfo?.task?.pName;
  return (
    <>
      {taskStatusPermission() && <div className={classes.toggleBtnCnt}>
        <ToggleButtonGroup
          size="small"
          exclusive
          onChange={handleTabChange}
          value={tab}
          classes={{ root: classes.toggleBtnGroup }}>
          <ToggleButton
            key={2}
            value="allTasks"
            classes={{
              root: classes.toggleButton,
              selected: classes.toggleButtonSelected,
            }}
            disabled={false}>
            <div className={classes.toggleButtonMainDiv}>
              <span className={classes.planTypeLbl}>
                <FormattedMessage
                  id="task.label"
                  defaultMessage="Tasks"
                />
              </span>
            </div>
          </ToggleButton>
            <ToggleButton
              key={1}
              value="taskStatus"
              classes={{
                root: classes.toggleButton,
                selected: classes.toggleButtonSelected,
              }}
              disabled={false}>
              <div className={classes.toggleButtonMainDiv}>
                <span className={classes.planTypeLbl}>
                  <FormattedMessage
                    id="profile-settings-dialog.notification-preferences.list.task-status"
                    defaultMessage="Status Templates"
                  />
                </span>
              </div>
            </ToggleButton>
        </ToggleButtonGroup>
      </div>}
      {unplanned && (
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={unplanned}
          onClose={handleClose}
          onBackdropClick={handleClose}
          BackdropProps={{
            classes: {
              root: classes.unplannedRoot,
            },
          }}
        >
          <div
            className={classes.unplannedMain}
            style={{ top: 205, left: 504 }}
          >
            <div className={classes.unplannedCnt}>
              <UnPlanned
                feature="business"
                titleTxt={
                  <FormattedMessage
                    id="common.discovered-dialog.business-title"
                    defaultMessage="Wow! You've discovered a Business feature!"
                  />
                }
                boldText={"Custom Task Status"}
                descriptionTxt={
                  <FormattedMessage
                    id="common.discovered-dialog.list.custom-status.label"
                    values={{ n: <span style={{ color: '#0090ff' }}>Business</span> }}
                    defaultMessage={"is available on our Business Plan. Upgrade now or start a {TRIALPERIOD}-day free trial to find out all nTask Business features."}
                    values={{TRIALPERIOD: TRIALPERIOD}}
                  />
                }
                showBodyImg={false}
                showDescription={true}
                selectUpgrade={handleClose}
              // imgUrl={template}
              />
            </div>
          </div>
        </Modal>
      )}
      {tab == "allTasks" && (
        <div className={classes.dialogContent}>
          {" "}
          {importBulkTask ? (
            <BulkTaskImport
              hideImportBulkTask={hideImportBulkTask}
              showSnackBar={showSnackBar}
              projectSettings={projectSettings}
              addTasksInProject={addTasksInProject}
            />
          ) : (
            <div className={classes.resourceCnt}>
              <div style={{ display: "flex" }}>
                <CreateableSelectDropdown
                  data={allTasks}
                  createCharacterLimit={250}
                  label={
                    <span className={classes.label}>
                      <FormattedMessage
                        id="project.dev.tasks.add-create-new-task.label"
                        defaultMessage="Add/Create New Task "
                      />
                    </span>
                  }
                  placeholder={intl.formatMessage({
                    id: "project.dev.tasks.add-create-new-task.placeholder",
                    defaultMessage: "Add/Create new task ",
                  })}
                  id="selectTaskTitleDropdown"
                  name="task"
                  selectOptionAction={handleOptionsSelect}
                  createOptionAction={handleCreateOption}
                  disableIndicator={true}
                  // removeOptionAction={this.handleRemoveOption}
                  type="task"
                  createText="Create Task"
                  isMulti={false}
                  selectedValue={[]}
                  createLabelValidation={() => {
                    return true;
                  }}
                  clearOptionAfterSelect={true}
                  // styles={{ width: 515 }}
                  isDisabled={!permission.taskTabs.addProjectTask.cando || saving}
                  noOptionMessage={"Create new task"}
                  acceptDataInFormOfFun={false}
                />
                <CustomButton
                  onClick={showImportBulkTask}
                  btnType="plain"
                  variant="text"
                  disabled={!permission.taskTabs.importBulkTasks.cando}
                  style={{
                    background: "#F6F6F6",
                    border: "1px solid #F6F6F6",
                    height: 39,
                    width: 169,
                    marginTop: 21,
                    marginLeft: 12,
                  }}>
                  <span className={classes.importTxt}>
                    <FormattedMessage
                      id="common.import.tasks.label"
                      defaultMessage={"Import Bulk Tasks"}
                      values={{
                        label: taskLabelMulti ? `Import Bulk ${taskLabelMulti}` : 'Import Bulk Tasks'
                      }}
                    />
                  </span>
                </CustomButton>
              </div>

              {projectTasks.length ? (
                // <Scrollbars
                //   autoHide
                //   style={{ height: windowHeight - windowHeight * 0.1 - 300 }}
                // >
                <ul className={classes.projectTaskList}>
                  <li className={classes.header}>
                    <div
                      className={classes.taskProfileInfoHead}
                      style={{ flex: "auto", width: 320 }}>
                      <span className={classes.headerTitle}>
                        <FormattedMessage id="task.label" defaultMessage="Tasks" />
                      </span>
                    </div>

                    <div className={classes.estimatedTimeHead}>
                      <span className={classes.headerTitle}>
                        <FormattedMessage
                          id="project.dev.tasks.headers.estimated-time"
                          defaultMessage="Estimated Time"
                        />
                      </span>
                      <span
                        style={{
                          display: "flex",
                          flexDirection: "column",
                          position: "relative",
                          cursor: "pointer",
                        }}
                        onClick={handleEstimateTitleClick}>
                        <ChevronUp
                          className={classes.arrowUp}
                          htmlColor={theme.palette.secondary.light}
                          style={{ top: -12, position: "absolute" }}
                        />
                        <ChevronDown
                          className={classes.arrowDown}
                          htmlColor={theme.palette.secondary.light}
                          style={{ top: -5, position: "absolute" }}
                        />
                      </span>
                    </div>
                    {diffRate && (
                      <div className={classes.hourlyRateHead}>
                        <span className={classes.headerTitle}>
                          {projectSettings.projectBillingMethod == 1 ? (
                            <FormattedMessage
                              id="project.dev.tasks.headers.fixedfee"
                              defaultMessage="Fixed Fee"
                            />
                          ) : projectSettings.projectBillingMethod == 2 ? (
                            <FormattedMessage
                              id="project.dev.tasks.headers.hourlyrate"
                              defaultMessage="Hourly Rate"
                            />
                          ) : (
                            ""
                          )}
                        </span>
                      </div>
                    )}
                    <CanAccessFeature group='project' feature='billingType'>

                      <div className={classes.columnThree}>
                        <span className={classes.headerTitle}>
                          <FormattedMessage
                            id="project.dev.milestones.header.billable"
                            defaultMessage="Billable"
                          />
                        </span>
                      </div>
                    </CanAccessFeature>
                    <div className={classes.actionColumn}></div>
                  </li>
                  {/* <DragDropContext onDragEnd={onDragEnd}>
                    <Droppable key={`taskDragable`} droppableId={1} type="task">
                      {(provided, snapshot) => ( */}
                  {/* <div ref={provided.innerRef} {...provided.droppableProps}> */}
                  {projectTasks.map((t, index) => {
                    let selectedStatus =
                      taskStatusDataDropDown.find(s => s.value == t.status) || {};

                    return (
                      <>
                        {/* <Draggable
                          key={t.taskId}
                          draggableId={t.taskId}
                          index={index}
                        > */}
                        {/* {(provided, snapshot) => (
                            <div
                              ref={provided.innerRef}
                              {...provided.draggableProps}
                              {...provided.dragHandleProps}
                              {...provided.draggableProps.style}
                            > */}
                        <li
                          className={classes.row}
                          name="task-List"
                          key={t.taskId}
                          onClick={event => handleTaskClick(event, t)}
                          //Selected task background should turn to light blue color
                          style={{
                            // borderLeft: `3px solid ${t.colorCode}`,
                            background:
                              !isEmpty(onClickTask) && onClickTask.taskId == t.taskId
                                ? theme.palette.background.lightBlue
                                : "",
                            paddingLeft: 0,
                            minHeight: !t.parentId ? 56 : 44,
                          }}>
                          <div style={{ padding: "2px 0px", width: "100%", display: "flex", flexDirection: "row", alignItems: "center" }}>
                            {/* <DragIndicator
                                  htmlColor={theme.palette.secondary.light}
                                  className={classes.dragHandle}
                                /> */}
                            {false ? (
                              // <div className={classes.taskProfileInfo}>
                              //   <Hotkeys keyName="enter" onKeyDown={handleTaskNameBlur}>
                              //     <DefaultTextField
                              //       label=""
                              //       error={false}
                              //       size="small"
                              //       formControlStyles={{
                              //         marginBottom: 0,
                              //         minwidth: 100,
                              //         paddingRight: 25,
                              //       }}
                              //       defaultProps={{
                              //         type: "text",
                              //         id: "taskTitle",
                              //         placeholder: "Enter task title",
                              //         value: editTask.taskTitle,
                              //         autoFocus: true,
                              //         onBlur: handleTaskNameBlur,
                              //         inputProps: { maxLength: 80 },
                              //         onChange: event => {
                              //           handleChangeTaskName(event);
                              //         },
                              //       }}
                              //     />
                              //   </Hotkeys>
                              // </div>
                              null
                            ) : (
                              <div
                                style={{
                                  paddingLeft: !t.parentId ? 0 : 30,
                                  display: "flex",
                                  flex: 1,
                                  width: 330,
                                  minHeight: !t.parentId ? 56 : 44,
                                  borderLeft: `3px solid`,
                                  borderLeftColor: t.colorCode == "" ? "transparent" : t.colorCode,
                                  alignItems: "center",
                                }}>
                                {/* <CanAccessFeature group='task' feature='statusTitle'> */}
                                  <div>
                                    <StatusDropdown
                                      onSelect={status => {
                                        handleStatusChange(status, t);
                                      }}
                                      iconButton={true}
                                      option={selectedStatus}
                                      style={{ marginRight: 8 }}
                                      // options={taskStatusData}
                                      options={taskStatusDataDropDown}
                                      toolTipTxt={selectedStatus.label}
                                      disabled={!permission.taskTabs.taskStatus.isAllowEdit}
                                      writeFirst={false}
                                      dropdownProps={{
                                        disablePortal: false,
                                      }}
                                    />
                                  </div>
                                {/* </CanAccessFeature> */}
                                <div className={classes.taskProfileInfo}>
                                  <div
                                    style={{
                                      display: "flex",
                                      flexDirection: "column",
                                    }}>
                                    <div style={{ display: "flex" }}>
                                      {!isEmpty(editTask) && editTask.taskId == t.taskId ? (
                                        <Hotkeys keyName="enter" onKeyDown={handleTaskNameBlur}>
                                          <DefaultTextField
                                            label=""
                                            error={false}
                                            size="small"
                                            formControlStyles={{
                                              marginBottom: 0,
                                              minwidth: 100,
                                              paddingRight: 25,
                                            }}
                                            defaultProps={{
                                              type: "text",
                                              id: "taskTitle",
                                              placeholder: "Enter task title",
                                              value: editTask.taskTitle,
                                              autoFocus: true,
                                              onBlur: handleTaskNameBlur,
                                              inputProps: { maxLength: 250 },
                                              onKeyDown: e => {
                                                if (e.keyCode == 13) handleTaskNameBlur()
                                              },
                                              onChange: event => {
                                                handleChangeTaskName(event);
                                              },
                                            }}
                                          />
                                        </Hotkeys>
                                      ) : (
                                        // <Typography
                                        //   variant="body2"
                                        //   className={classes.fullName}
                                        //   style={{
                                        //     width: diffRate ? 290 : 320,
                                        //     color:
                                        //       !isEmpty(taskDetails) &&
                                        //       taskDetails.entity.id == t.taskId
                                        //         ? theme.palette.text.azure
                                        //         : "",
                                        //     lineHeight: !t.parentId ? "normal" : 2.5,
                                        //   }}
                                        //   // onClick={(event) => {
                                        //   //   handleEditTask(event, t);
                                        //   // }}
                                        //   title={t.taskTitle}>
                                        //   <Truncate
                                        //     trimWhitespace={true}
                                        //     width={docked ? 180 : 300}
                                        //     ellipsis={<span>...</span>}>
                                        //     {t.taskTitle}
                                        //   </Truncate>

                                        //   {permission.taskTabs.taskName.isAllowEdit && getEditIcon(t)}
                                        //   {getSubtaskCount(
                                        //     t,
                                        //     !isEmpty(taskDetails) && taskDetails.entity.id == t.taskId
                                        //   )}
                                        // </Typography>
                                        <Typography
                                          variant="body2"
                                          className={classes.fullName}
                                          style={{
                                            color:
                                              !isEmpty(taskDetails) && taskDetails.entity.id == t.taskId
                                                ? theme.palette.text.azure
                                                : "",
                                            lineHeight: !t.parentId ? "normal" : 2.5,
                                          }}
                                          title={t.taskTitle}>
                                          {t.taskTitle}

                                          {permission.taskTabs.taskName.isAllowEdit && getEditIcon(t)}
                                          {getSubtaskCount(
                                            t,
                                            !isEmpty(taskDetails) && taskDetails.entity.id == t.taskId
                                          )}
                                        </Typography>
                                      )}
                                    </div>
                                    {!t.parentId && (
                                      <Typography variant="caption" className={classes.jobTitle}>
                                        {getPriority(t.priority)}{" "}
                                        {(t.startDate && t.endDate) || t.checklistItems > 0 ? (
                                          <span className={classes.dot}>•</span>
                                        ) : null}
                                        {t.startDate && t.endDate ? (
                                          <>
                                            {moment(t.startDate)
                                              .format("ll")
                                              .substring(0, 6)}

                                            <span className={classes.dot}>-</span>
                                            {moment(t.endDate)
                                              .format("ll")
                                              .substring(0, 6)}
                                            <span className={classes.dot}>•</span>
                                          </>
                                        ) : null}
                                        {t.checklistItems > 0 &&
                                          ` ${t.completedChecklistItems} of ${t.checklistItems} `}
                                        {t.totalAssignee > 0 && (
                                          <span className={classes.dot}>•</span>
                                        )}
                                        {t.totalAssignee > 0 && `${t.totalAssignee} Assignees `}
                                        {t.repeatType ? (
                                          <>
                                            {" "}
                                            <span className={classes.dot}>•</span>
                                            {` Repeat ${t.repeatType}`}
                                          </>
                                        ) : null}
                                      </Typography>
                                    )}
                                  </div>
                                </div>
                              </div>
                            )}
                            <div className={classes.estimatedTime}>
                              <TimeInput
                                placeholder="Add hours"
                                actualCapacity={
                                  initial == "h"
                                    ? Number((t.estimatedTimeMin / 60).toFixed(2))
                                    : t.estimatedTimeMin
                                }
                                onInputChange={() => { }}
                                initial={initial}
                                onInputBlur={hours => {
                                  handleInputDuration(hours, t);
                                }}
                                permission={permission.taskTabs.estimatedTime.isAllowEdit}
                              />
                            </div>
                            {diffRate && (
                              <div className={classes.hourlyRate}>
                                {" "}
                                <AmountInput
                                  label=""
                                  placeholder={intl.formatMessage({
                                    id: "project.dev.details.billable.per-hour.placeholder",
                                    defaultMessage: "Add Rate",
                                  })}
                                  onInputChange={() => { }}
                                  currency={projectSettings.currency}
                                  onBlur={amount => {
                                    handleInputBlur(amount, t);
                                  }}
                                  savedAmount={
                                    projectSettings.projectBillingMethod == 1
                                      ? t.fixedFee
                                      : projectSettings.projectBillingMethod == 2
                                        ? t.hourlyRate
                                        : 0
                                  }
                                  disabled={!permission.taskTabs.addRate.cando}
                                />{" "}
                              </div>
                            )}
                            <CanAccessFeature group='project' feature='billingType'>

                              <div className={classes.columnThree}>
                                <DefaultCheckbox
                                  checkboxStyles={{ padding: "5px 5px 4px 8px" }}
                                  checked={t.isBillable}
                                  unCheckedIconProps={{
                                    style: { fontSize: "20px" },
                                  }}
                                  checkedIconProps={{
                                    style: { fontSize: "20px" },
                                  }}
                                  onChange={e => {
                                    handleUpdateBillable(t, !t.isBillable, e);
                                  }}
                                  fontSize={20}
                                  color={"#0090ff"}
                                  disabled={!permission.taskTabs.taskBillable.cando}
                                />
                              </div>
                            </CanAccessFeature>
                            <div className={classes.actionColumn}>
                              <MoreActionDropDown
                                size="small"
                                handleOptionSelect={option => {
                                  handleActionsSelect(option, t);
                                }}
                                selectedColor={t.colorCode}
                                handleColorSelect={color =>
                                  handleColorSelect(color, t, "taskListView")
                                }
                                permission={permission}
                              />
                            </div>
                          </div>
                        </li>
                        {/* </div>
                          )} */}
                        {/* </Draggable> */}
                      </>
                    );
                  })}
                  {/* {provided.placeholder}
                  </div> */}
                  {/* )}
                    </Droppable>
                  </DragDropContext> */}
                </ul>
              ) : (
                // </Scrollbars>
                <div className={classes.emptyStateCnt}>
                  <EmptyState
                    screenType="task"
                    heading={
                      <FormattedMessage
                        id="project.dev.tasks.add-create-import-tasks.label"
                        defaultMessage="Add/Create/Import Tasks"
                      />
                    }
                    message={
                      <FormattedMessage
                        id="project.dev.tasks.add-create-import-tasks.placeholder"
                        defaultMessage="Create new task or add existing tasks. You can also import bulk tasks to the project."
                      />
                    }
                    button={false}
                  />{" "}
                </div>
              )}
            </div>
          )}
        </div>
      )}
      {tab == "taskStatus" && (
        <ProjectTemplates
          projectId={projectSettings.projectId}
          projectTemplate={selectedProject.projectTemplate}
          intl={intl}
          updateCustomTemplate={updateCustomTemplate}
          project={projectSettings}
          showSnackBar={showSnackBar}
          updateTaskInProjectModal={updateTaskInProjectModal}
        />
      )}
      <ActionConfirmation
        open={markChecklistActionConf}
        closeAction={markAllConfirmDialogClose}
        cancelBtnText={
          <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
        }
        successBtnText={
          <FormattedMessage
            id="task.detail-dialog.to-do-list.mark-all.action"
            defaultMessage="Yes, Mark All Complete"
          />
        }
        alignment="center"
        headingText={
          <FormattedMessage
            id="task.detail-dialog.to-do-list.mark-all.title"
            defaultMessage="Mark All Complete"
          />
        }
        iconType="markAll"
        msgText={
          <FormattedMessage
            id="task.detail-dialog.to-do-list.mark-all.message"
            defaultMessage="Are you sure you want to mark all to-do list items as completed?"
          />
        }
        successAction={() => handleUpdateStatus(selectedStatusObj, selectedTaskObj)}
        btnQuery={markChecklistActionBtnQuery}
      />
    </>
  );
}
ProjectTasks.defaultProps = {
  classes: {},
  theme: {},
  onInputChange: () => { },
  tasks: [],
  projectSettings: {},
  taskDetails: {},
  projectTasks: [],
  setProjectTasks: () => { },
  allTasks: [],
  setAllTasks: () => { },
  handleActionsSelect: () => { },
  handleOptionsSelect: () => { },
  handleColorSelect: () => { },
  updateTaskDetailsIfOpen: () => { },
  addTasksInProject: () => { },
  showSnackBar: () => { },
  permission: {},
  UpdateTask: () => { },
  sortedProjects: () => { },
  setSelectedProject: () => { },
  updateTaskInProjectModal: () => { },
  selectedProject: {},
  workspaceStatus: [],
  docked: false,
  workspacePer: {}
};

const mapStateToProps = (state, ownProps) => {
  return {
    tasks: state.tasks.data,
    workspaceStatus: state.workspaceTemplates.data.defaultWSTemplate,
    workspacePer: state.workspacePermissions.data.workSpace,
    profileState: state.profile.data,
    companyInfo: state.whiteLabelInfo.data,
  };
};
export default compose(
  withStyles(taskStyles, { withTheme: true }),
  connect(mapStateToProps, { UpdateTask, CheckAllTodoList })
)(ProjectTasks);
