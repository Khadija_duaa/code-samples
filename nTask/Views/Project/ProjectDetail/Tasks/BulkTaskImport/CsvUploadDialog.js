import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import SvgIcon from "@material-ui/core/SvgIcon";
import CircularProgress from "@material-ui/core/CircularProgress";
import bulkImportStyles from "./style";
import ExcelIcon from "../../../../../components/Icons/ExcelIcon";
import dialogStyles from "../../../../../assets/jss/components/dialog";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import DefaultButton from "../../../../../components/Buttons/DefaultButton";
import Dropzone from "react-dropzone";
import Typography from "@material-ui/core/Typography";
import { Circle } from "rc-progress";
import helper from "../../../../../helper";
import { ImportBulkTasksFile } from "../../../../../redux/actions/ImportExport";
import apiInstance, { CancelToken } from "../../../../../redux/instance";
import NotificationMessage from "../../../../../components/NotificationMessages/NotificationMessages";
import getErrorMessages from "../../../../../utils/constants/errorMessages";
import { calculateHeight } from "../../../../../utils/common";
import { FetchWorkspaceInfo } from "../../../../../redux/actions/workspace";
import { FormattedMessage } from "react-intl";
const fileType = [
  "application/vnd.ms-excel",
  "application/vnd.ms-excel.sheet.macroEnabled.12",
  "application/msexcel",
  "application/x-msexcel",
  "application/x-ms-excel",
  "application/x-excel",
  "application/x-dos_ms_excel",
  "application/xls",
  "application/x-xls",
  "application/x-msi",
  "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
  "text/csv",
  ".xlsx",
];

class DragDropCsv extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dnd: true,
      progress: 0,
      cancelRequest: null,
      successfullyImortedCount: 0,
      totalAdded: 0,
      errorMesseage: "",
    };
    this.initialState = this.state;
    this.handleDragOver = this.handleDragOver.bind(this);
  }
  onhandleDrop(files) {
    if (files.length) {
      this.manageImports(0, files);
      this.setState({ dnd: false });
    }
  }
  manageImports = (index, files) => {
    const self = this;
    const file = files[index];
    const { type, closeAction, showMessage, handleRefresh, projectId = "" } = self.props;
    const successApiCallback = response => {
      if (response.status === 200) {
        const successfullyImortedCount = response.data.count;
        // handleRefresh();

        this.props.addTasksInProject({
          taskIDs: response.data.taskIds,
          projectId: projectId,
        });
        this.props.FetchWorkspaceInfo("", () => {});
        // showMessage(`${successfullyImortedCount} ${type}s have been added to System.`);
        if (index === files.length - 1)
          self.setState({
            progress: 100,
            cancelRequest: null,
            successfullyImortedCount,
            totalAdded: self.state.totalAdded + successfullyImortedCount,
          });
        else {
          // self.manageImports(index + 1, files);
          self.setState({
            totalAdded: self.state.totalAdded + successfullyImortedCount,
          });
        }
        this.setState({ errorMesseage: "" });
      }
    };
    const failureApiCallback = error => {
      if (error.status && error.status === 500) {
        if (index === files.length - 1) self.setState(self.initialState);
        this.setState({ errorMesseage: error.data.message });
      } else if ((error.status && error.status === 422) || error.status === 406) {
        if (index === files.length - 1) self.setState(self.initialState);
        this.setState({ errorMesseage: error.data.message });
      } else {
        if (index === files.length - 1) self.setState(self.initialState);
        this.setState({ errorMesseage: error.data.message });
      }
    };

    let formdata = new FormData();
    formdata.append("file", file);
    // formdata.append("projectID", projectId);
    // let ApiData = {
    //   formdata,
    //   projectId
    // }
    const config = {
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      cancelToken: new CancelToken(function executor(c) {
        self.setState({ cancelRequest: c });
      }),
    };
    if (type === "task") {
       let url = "api/usertask/ImportTasksTemplateData";
      if(projectId)
      url = "api/usertask/ImportTasksTemplateData?projectId="+projectId;
      ImportBulkTasksFile(
        formdata,
        url,
        config,
        successApiCallback,
        failureApiCallback
      );
    }
  };
  handleDragOver(event) {
    event.stopPropagation();
  }

  handleCancelRequest = () => {
    const { cancelRequest } = this.state;
    if (cancelRequest) cancelRequest("User cancelled upload.");
  };

  handleInvalidFileDrop = () => {
    this.props.showMessage(getErrorMessages().INVALID_IMPORT_EXTENSION, "error");
  };
  render() {
    const { classes, theme, open, type } = this.props;
    const { progress, dnd, totalAdded, errorMesseage } = this.state;
    const dndStyles = {
      position: "relative",
      width: "100%",
      borderWidth: 2,
      borderColor: theme.palette.border.lightBorder,
      borderStyle: "dashed",
      borderRadius: 5,
      padding: 50,
      background: theme.palette.background.paper,
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      marginBottom: 30,
    };
    const dndActiveStyle = {
      borderColor: theme.palette.border.darkBorder,
    };

    const title = type.charAt(0).toUpperCase() + type.slice(1);
    return (
      <div className={classes.centerAlignDialogContent} style={{ height: calculateHeight() - 450 }}>
        {errorMesseage ? (
          <NotificationMessage
            type="failure"
            iconType="failure"
            style={{ marginBottom: 20, width: "100%" }}>
            {errorMesseage}
          </NotificationMessage>
        ) : null}
        {dnd ? (
          <Dropzone
            onDrop={this.onhandleDrop.bind(this)}
            onDragOver={this.handleDragOver}
            onDropRejected={this.handleInvalidFileDrop}
            accept={fileType}
            activeStyle={dndActiveStyle}
            style={dndStyles}
            // disableClick
            onClick={evt => {
              evt.preventDefault();
              evt.stopPropagation();
            }}>
            {({ getRootProps, getInputProps, open }) => {
              return (
                <Fragment>
                  <div
                    className={classes.csvConfirmationIconCnt}
                    onClick={e => e.stopPropagation()}
                    {...getRootProps()}>
                    <div style={{ textAlign: "center" }}  onClick={e => e.stopPropagation()}>
                      <div className={classes.excelIconCnt} onClick={e => e.stopPropagation()}>
                        <SvgIcon
                          viewBox="0 0 26 26"
                          className={classes.archieveConfirmationIcon}
                          htmlColor={theme.palette.secondary.light}>
                          <ExcelIcon />
                        </SvgIcon>
                      </div>
                      <Typography
                        variant="h3"
                        style={{
                          marginTop: 10,
                          fontFamily: theme.typography.fontFamilyLato,
                        }}>
                        <FormattedMessage
                          id="common.import-dialog.label"
                          defaultMessage="Drag & Drop"
                        />
                      </Typography>
                      <input {...getInputProps()} />
                      <p className={classes.dragnDropMessageText}>
                        <FormattedMessage
                          id="common.import-dialog.sentence"
                          defaultMessage="your CSV file or"
                        />{" "}
                        <u className={classes.browseText} onClick={() => open()}>
                          <FormattedMessage
                            id="common.import-dialog.browse"
                            defaultMessage="browse"
                          />
                        </u>{" "}
                        <FormattedMessage
                          id="common.import-dialog.fromcomputer"
                          defaultMessage=" from your computer"
                        />
                      </p>
                    </div>
                  </div>
                </Fragment>
              );
            }}
          </Dropzone>
        ) : (
          <div className={classes.importBulkProgressCnt}>
            <div style={{ width: 100, position: "relative", marginBottom: 30, display: "flex", flexDirection: "row", justifyContent: "center" }}>
              {progress < 100 ? (
                <CircularProgress style={{ marginLeft: 20 }} />
              ) : (
                <>
                  <Circle
                    percent={progress}
                    strokeWidth="3"
                    trailWidth=""
                    trailColor="#dedede"
                    strokeColor="#0090ff"
                  />
                  <span className={classes.clProgressValue}>{progress}%</span>
                </>
              )}
            </div>

            <Typography variant="h3">
              {progress < 100 ? "Importing..." : "Congratulations!"}
            </Typography>
            <p className={classes.dragnDropMessageText}>
              {progress < 100
                ? "Please wait! It will take a moment."
                : `${totalAdded}/${totalAdded} ${type}s have been imported to nTask successfully.`}
            </p>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {};
};

export default compose(
  withStyles(bulkImportStyles, { withTheme: true }),
  connect(mapStateToProps, { FetchWorkspaceInfo })
)(DragDropCsv);
