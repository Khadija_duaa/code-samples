// @flow
import React, { useState, useEffect } from "react";
import bulkImportStyles from "./style";
import { compose } from "redux";
import { connect, useSelector } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import BackArrow from "@material-ui/icons/KeyboardBackspace";
import Typography from "@material-ui/core/Typography";
import NotificationMessage from "../../../../../components/NotificationMessages/NotificationMessages";
import ImportBulk from "./CsvUploadDialog";
import helper from "../../../../../helper";
import CustomButton from "../../../../../components/Buttons/CustomButton";
import apiInstance from "../../../../../redux/instance";
import {FormattedMessage} from "react-intl";
type BulkImportProps = {
  classes: Object,
  theme: Object,
  hideImportBulkTask: Function,
  showSnackBar: Function,
  projectSettings: Object,
  addTasksInProject: Function,
};
function BulkTaskImport(props: BulkImportProps) {
  const {
    hideImportBulkTask,
    classes,
    theme,
    showSnackBar,
    projectSettings,
    addTasksInProject,
  } = props;
  const {companyInfo} = useSelector(state => {
    return {
      companyInfo: state.whiteLabelInfo.data,
    };
  });
  const downloadTemplate = () => {
    /** function call when user click download template button */
    const fileName = "nTask-Tasks-Template.xls";
    const exportType = 0; /** export type 0 is for downloading task template for bulk import tasks */
    apiInstance()
      .get(`${ENV == "production1" || ENV == "beta1"  ? BASE_URL_API : BASE_URL }api/export/tasktemplate`, {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
        },
        params: {
          type: exportType,
        },
        responseType: "blob",
      })
      .then((response) => {
        if (response.status === 200) {
          helper.DOWNLOAD_TEMPLATE(
            response.data,
            fileName,
            "application/vnd.ms-excel"
          );
          showSnackBar(`File downloaded successfully.`, "success");
        }
      })
      .catch((error) => {
        if (error) {
          setBtnQuery("");
          showSnackBar(`Oops! File downloading failed.`, "error");
        }
      });
  };
  const taskLabelSingle = companyInfo?.task?.sName;
  const taskLabelMulti = companyInfo?.task?.pName;
  return (
    <>
      <div className={classes.headingCnt}>
        <BackArrow
          onClick={hideImportBulkTask}
          className={classes.backBtnIcon}
        />
        <div
          style={{
            width: "100%",
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <div className={classes.headingTextCnt}>
            <Typography variant="h3" className={classes.mainHeadingText}>
              <FormattedMessage id="common.import.tasks.label"
                                defaultMessage="Import Bulk Tasks"
                                values={{label: taskLabelMulti || 'Tasks'}} />
            </Typography>
            <Typography variant="body2" className={classes.headingTagline}>
             <FormattedMessage id="project.dev.tasks.import-tasks.placeholder" defaultMessage="Import your tasks by uploading CSV file."/> 
            </Typography>
          </div>
          <CustomButton
            onClick={(e) => downloadTemplate(e)}
            btnType={"success"}
            variant="contained"
            query={""}
            disabled={false}
            style={{
              fontFamily: theme.typography.fontFamilyLato,
            }}
          >
           <FormattedMessage id="project.dev.tasks.download-template-button.label" defaultMessage="Download Template"/> 
          </CustomButton>
        </div>
      </div>
      <div className={classes.importBulkCnt}>
        <ImportBulk
          type={"task"}
          theme={theme}
          handleExportType={showSnackBar}
          showMessage={showSnackBar}
          handleRefresh={() => {}}
          projectId={projectSettings.projectId}
          addTasksInProject={addTasksInProject}
        />
      </div>
      <NotificationMessage
        type="info"
        iconType="info"
        style={{
          //   width: "100%",
          //   marginTop: 19,
          //   textAlign: "start",
          //   fontSize: "11px !important",
          padding: "10px 15px 10px 15px",
          backgroundColor: theme.palette.background.light,
          borderRadius: 4,
        }}
      >
        <FormattedMessage id="project.dev.tasks.import-tasks.label" defaultMessage={`"Please make sure your csv file matches the format of nTask template. You
        can also download the template by clicking on "Download Template"
        button."`}/>
      </NotificationMessage>
    </>
  );
}
BulkTaskImport.defaultProps = {
  showSnackBar: () => {},
  hideImportBulkTask: () => {},
  classes: {},
  theme: {},
  projectSettings: {},
  addTasksInProject: () => {},
};

export default withStyles(bulkImportStyles, { withTheme: true })(
  BulkTaskImport
);
