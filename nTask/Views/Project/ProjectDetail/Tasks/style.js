const taskStyles = theme => ({
  resourceCnt: {
    padding: "0px 0px 0px 5px",
    overflowY: "auto",
    height: "100%",
  },
  projectTaskList: {
    listStyleType: "none",
    padding: 0,
    margin: 0,

    "& li": {
      display: "flex",
      alignItems: "center",
      cursor: "pointer",
      borderBottom: `1px solid ${theme.palette.border.grayLighter}`,
      padding: "0 16px 0 8px",
      // margin: "0 0 5px 0",
      "&:hover $editIconCnt": {
        display: "inline !important",
      },
      "&:hover $dragHandle": {
        visibility: "visible",
      },
    },
  },
  taskProfileInfo: {
    flex: 1,
  },
  taskProfileInfoHead: {
    flex: 1,
    // paddingLeft: 30,
  },
  weeklyCapacity: {
    width: 100,
  },
  fullName: {
    color: "#202020",
    fontSize: "13px !important",
    fontWeight: 500,
    fontFamily: theme.typography.fontFamilyLato,
    lineHeight: "normal",
    cursor: "text",
    wordBreak: "break-word",
    maxWidth: 510
  },
  // fullName: {
  //   color: "#202020",
  //   fontSize: "13px !important",
  //   fontWeight: 700,
  //   fontFamily: theme.typography.fontFamilyLato,
  //   display: "inline-block",
  //   // overflow: "hidden",
  //   whiteSpace: "nowrap",
  //   lineHeight: "normal",
  //   textOverflow: "ellipsis",
  //   width: 290,
  //   cursor: "text",
  // },
  dot: {
    color: theme.palette.text.hint,
    fontSize: "14px !important",
  },
  iconSubtask: {
    fontSize: "18px !important",
    // marginLeft: 5,
    marginBottom: -3,
  },
  countTxt: {
    color: "#969696",
    fontSize: "13px !important",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
  },
  jobTitle: {
    color: "#7E7E7E",
    fontSize: "12px !important",
    fontWeight: 400,
    lineHeight: 0.9,
    fontFamily: theme.typography.fontFamilyLato,
  },
  headerTitle: {
    color: "#969696",
    fontSize: "12px !important",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
  },
  header: {
    borderBottom: "none !important",
  },
  weeklyCapacityHead: {
    width: 100,
    paddingLeft: 7,
  },
  hourlyRate: {
    width: 130,
  },
  hourlyRateHead: {
    width: 125,
    paddingLeft: 7,
  },
  estimatedTimeHead: {
    width: 130,
    paddingLeft: 10,
    display: "flex",
    alignItems: "center",
  },
  estimatedTime: {
    width: 115,
  },
  actionColumn: {
    width: 30,
  },
  row: {
    minHeight: 56,
  },
  statusIcon: {
    fontSize: "22px !important",
  },
  arrowUp: {
    fontSize: "22px !important",
  },
  arrowDown: {
    fontSize: "22px !important",
  },
  emptyStateCnt: {
    marginTop: 84,
  },
  actionColumn: {
    width: 10,
  },
  listItem: {
    padding: "7px 14px",
    fontSize: "13px !important",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
    "&:hover": {
      backgroundColor: "transparent !important",
    },
  },
  deleteTxt: {
    color: theme.palette.text.danger,
  },
  listItemColor: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    padding: "0px 6px 0px 14px",
    fontSize: "13px !important",
    fontWeight: 600,
    fontFamily: theme.typography.fontFamilyLato,
    "&:hover": {
      backgroundColor: "transparent !important",
    },
  },
  label: {
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
    color: "#646464",
  },
  colorPicker: {
    right: 195,
    top: 34,
    position: "fixed",
  },
  importTxt: {
    fontSize: "13px !important",
    fontWeight: 600,
    fontFamily: theme.typography.fontFamilyLato,
  },
  menuHeadingItem: {
    padding: "0 14px !important",
    border: "none !important",
    "&:hover": {
      backgroundColor: "transparent",
      cursor: "unset",
    },
  },
  menuHeadingListItemText: {
    fontSize: "12px !important",
    marginTop: 5,
    marginBottom: 2,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: theme.typography.fontWeightRegular,
    textTransform: "capitalize",
    color: theme.palette.secondary.light,
  },
  editIcon: {
    fontSize: "11px !important",
    marginTop: -4,
    marginLeft: 5,
  },
  editIconCnt: {
    display: "none",
  },
  dragHandle: {
    fontSize: "24px !important",
    cursor: "pointer",
    visibility: "hidden",
  },
  dialogContent: {
    padding: 20,
    // minHeight: 610,
    flex: 1,
    overflowY: "auto",
  },
  toggleBtnGroup: {
    // textAlign: "center",
    borderRadius: 4,
    background: theme.palette.common.white,
    border: "none",
    boxShadow: "none",
    "& $toggleButtonSelected": {
      //   color: theme.palette.common.white,
      backgroundColor: theme.palette.border.blue,
      // border: `1px solid ${theme.palette.border.blue}`,
      //   '& $planPriceStyle':{
      //     color: theme.palette.text.azure,
      //   },
      "& $planTypeLbl": {
        color: theme.palette.common.white,
      },
      "&:after": {
        background: theme.palette.common.white,
      },
      "&:hover": {
        background: theme.palette.border.blue,
        // color: theme.palette.common.black
      },
    },
  },
  toggleButton: {
    border: `1px solid #F6F6F6`,
    borderRadius: 4,
    height: 32,
    fontSize: "12px !important",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    textTransform: "capitalize",
    color: theme.palette.common.white,
    fontWeight: theme.typography.fontWeightLight,
    background: "#F6F6F6",
    padding: "8px 12px",
  },
  toggleButtonSelected: {},
  toggleButtonMainDiv: {
    position: "relative",
    alignItems: "center",
    display: "flex",
  },
  planTypeLbl: {
    display: "flex",
    fontSize: "13px !important",
    // fontWeight: 700,
    color: theme.palette.text.primary,
    whiteSpace: "nowrap",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
  },
   toggleBtnCnt: {
    display: "flex",
    justifyContent: "space-between",
    // marginBottom: 10,
    padding: "20px 0px 0px 24px"
  },
   columnThree: {
    width: 42,
  },
  unplannedRoot: {
    backgroundColor: 'rgba(0, 0, 0, 0)'
  },
  unplannedMain: {
    fontFamily : theme.typography.fontFamilyLato,
    background: theme.palette.common.white,
    width: 356,
    height: 'auto',
    top: 250,
    left: 200,
    padding: 20,
    position: "absolute",
    borderRadius: 4,
    boxShadow: `-1px 1px 5px -2px rgba(0,0,0,0.32)`,
  }
});

export default taskStyles;
