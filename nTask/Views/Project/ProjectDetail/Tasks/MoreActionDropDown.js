// @flow

import React, { useState, useEffect } from "react";
import dropdownStyle from "./style";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import DropdownMenu from "../../../../components/Dropdown/DropdownMenu";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import RightArrow from "@material-ui/icons/ArrowRight";
import ListItemText from "@material-ui/core/ListItemText";

import CustomIconButton from "../../../../components/Buttons/IconButton";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { Scrollbars } from "react-custom-scrollbars";
import ColorPicker from "../../../../components/Dropdown/ColorPicker/ColorPicker";
import {FormattedMessage} from "react-intl";
import { ProjectMandatory } from "../../../../helper/config.helper";

type DropdownProps = {
  classes: Object,
  theme: Object,
  size: String,
  handleOptionSelect: Function,
  selectedColor: String,
  handleColorSelect: Function,
  permission: Object,
};

//MoreActionDropDown Main Component
function MoreActionDropDown(props: DropdownProps) {
  const {
    classes,
    theme,
    size,
    handleOptionSelect,
    selectedColor,
    handleColorSelect,
    permission,
  } = props;
  const [anchorEl, setAnchorEl] = useState(null);
  const [colorPicker, setColorPicker] = useState(false);

  const handleClick = (event) => {
    // Function Opens the dropdown
    event.stopPropagation();
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };
  const handleClose = (event) => {
    // Function closes dropdown
    setAnchorEl(null);
    setColorPicker(false);
  };
  const handleItemClick = (event, option) => {
    //Fuction responsible for selecting item from the list
    handleOptionSelect(option); // Callback on item select used to lift up option to parent if needed

    if (option == "color") {
      /** if option is color then drop down will be open and color control will open */
      setColorPicker(true);
    } else {
      setAnchorEl(null);
      setColorPicker(false);
    }
  };
  const onColorChange = (color) => {
    /** function calls when user change color */
    handleColorSelect(color);
    setColorPicker(false);
  };
  const open = Boolean(anchorEl);

  return (
    <>
      <CustomIconButton
        btnType="condensed"
        style={{ padding: 0 }}
        onClick={handleClick}
        buttonRef={anchorEl}
      >
        <MoreVerticalIcon
          htmlColor={theme.palette.secondary.medDark}
          style={{ fontSize: "24px" }}
        />
      </CustomIconButton>
      <DropdownMenu
        open={open}
        closeAction={handleClose}
        anchorEl={anchorEl}
        size={size}
        placement="bottom-end"
      >
        {/* <Scrollbars autoHide autoHeight autoHeightMin={0} autoHeightMax={130}> */}
        <List>
          <ListItem
            disableRipple={true}
            classes={{ root: classes.menuHeadingItem }}
          >
            <ListItemText
              primary={<FormattedMessage id="common.action.label" defaultMessage="Select Action"/>}
              classes={{ primary: classes.menuHeadingListItemText }}
            />
          </ListItem>
          {permission.taskTabs.publicLink.cando && (
            <ListItem
              button
              className={classes.listItem}
              onClick={(event) => {
                handleItemClick(event, "publickLink");
              }}
            >
              <span><FormattedMessage id="common.action.public-link.label" defaultMessage="Public Link"/></span>
            </ListItem>
          )}
          {permission.taskTabs.color.cando && (
            <ListItem
              button
              className={classes.listItemColor}
              onClick={(event) => {
                handleItemClick(event, "color");
              }}
            >
              <span><FormattedMessage id="common.action.color.label" defaultMessage="Color"/></span>
              <RightArrow htmlColor={theme.palette.secondary.light} />
              <div
                className={classes.colorPicker}
                style={{ display: colorPicker ? "block" : "none" }}
              >
                <ColorPicker
                  triangle="hide"
                  onColorChange={onColorChange}
                  selectedColor={selectedColor}
                />
              </div>
            </ListItem>
          )}
          {permission.taskTabs.copyTask.cando && (
            <ListItem
              button
              className={classes.listItem}
              onClick={(event) => {
                handleItemClick(event, "copyTask");
              }}
            >
              <span><FormattedMessage id="common.action.copy-task.label" defaultMessage="Copy Task"/></span>
            </ListItem>
          )}
          {permission.taskTabs.archiveTask.cando && (
            <ListItem
              button
              className={classes.listItem}
              onClick={(event) => {
                handleItemClick(event, "archiveTask");
              }}
            >
              <span><FormattedMessage id="common.action.archive-task.label" defaultMessage="Archive Task"/></span>
            </ListItem>
          )}
          {permission.taskTabs.unlinkTask.cando && !ProjectMandatory() && (
            <ListItem
              button
              className={classes.listItem}
              onClick={(event) => {
                handleItemClick(event, "unlinkTask");
              }}
            >
              <span><FormattedMessage id="common.action.remove-task.label" defaultMessage="Remove Task"/></span>
            </ListItem>
          )}
          {permission.taskTabs.deleteTask.cando && (
            <ListItem
              button
              className={classes.listItem}
              onClick={(event) => {
                handleItemClick(event, "deleteTask");
              }}
            >
              <span className={classes.deleteTxt}><FormattedMessage id="common.action.delete-task.label" defaultMessage="Delete Task"/></span>
            </ListItem>
          )}
        </List>
        {/* </Scrollbars> */}
      </DropdownMenu>
    </>
  );
}
MoreActionDropDown.defaultProps = {
  classes: {},
  theme: {},
  size: "small",
  handleOptionSelect: () => {},
  selectedColor: "",
  handleColorSelect: () => {},
  permission: {},
};
export default compose(
  withRouter,
  withStyles(dropdownStyle, { withTheme: true })
)(MoreActionDropDown);
