// @flow

import React, { useState, useEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";

import DropdownMenu from "../../../../../components/Dropdown/DropdownMenu";

import Typography from "@material-ui/core/Typography";
import CustomButton from "../../../../../components/Buttons/CustomButton";
import { Scrollbars } from "react-custom-scrollbars";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import SvgIcon from "@material-ui/core/SvgIcon";
import RecurrenceIcon from "../../../../../components/Icons/RecurrenceIcon";
import AttachmentIcon from "../../../../../components/Icons/IconAttachment";
import RepeatIcon from "../../../../../components/Icons/ProjectAddMoreIcons/IconRepeatTask";
import ReminderIcon from "../../../../../components/Icons/ProjectAddMoreIcons/IconReminder";
import DependenciesIcon from "../../../../../components/Icons/ProjectAddMoreIcons/IconDependencies";
import CoverImage from "../../../../../components/Icons/ProjectAddMoreIcons/IconCoverImage";
import AddIcon from "../../../../../components/Icons/ProjectAddMoreIcons/IconAdd";
import IconImage from "../../../../../components/Icons/IconImage";
import IconSubtask from "../../../../../components/Icons/ProjectAddMoreIcons/IconSubtask";
import DefaultDialog from "../../../../../components/Dialog/Dialog";
import RepeatTask from "../../../../Task/RepeatTask/RepeatTask";
import NotificationMessage from "../../../../../components/NotificationMessages/NotificationMessages";
import getInfoMessage from "../../../../Task/RepeatTask/getInfoMessage";
import { repeatTaskInitObj } from "../../../../Task/RepeatTask/repeatTaskObj";
import SubTask from "./SubTask";
import AttachFile from "./AttachFile";
import isEmpty from "lodash/isEmpty";
import { FormattedMessage } from "react-intl";

type AddMoreOption = {
  classes: Object,
  theme: Object,
  taskPer: Object,
  tasks: Array,
  setCurrentTask: Function,
  currentTask: Object,
  allTasks: Array,
  handleSelectSubtask: Function,
  handleCreateSubTask: Function,
  taskDetails: Object,
  setTaskDetails: Function,
  showSnackBar: Function,
  projectSettings: Object,
  getTaskById: Function,
  permission: Object,
  selectedProject: Object,
};

function AddMore(props: AddMoreOption) {
  const {
    classes,
    theme,
    taskPer,
    tasks,
    currentTask,
    setCurrentTask,
    allTasks,
    handleSelectSubtask,
    handleCreateSubTask,
    taskDetails,
    setTaskDetails,
    showSnackBar,
    projectSettings,
    getTaskById,
    permission,
    intl,
    selectedProject
  } = props;
  const { repeatTask } = props.currentTask;
   const repeatObj = {
    repeatTypeTab:
      repeatTask && repeatTask.repeatType == "Daily"
        ? "left"
        : repeatTask && repeatTask.repeatType == "Weekly"
        ? "center"
        : "right",
    daily: { repeatDetails: repeatTask },
    weekly: { repeatDetails: repeatTask },
    monthly: { repeatDetails: repeatTask },
  };
  const repeatTaskPer = {
    /** creating desired object of permission for repeat task modal */
    taskDetail: {
      repeatTask: permission.taskTabs.addMore.repeatTask,
    },
  };
  const handleRepeatTaskModal = () => {
    setRepeatTaskDialog(true);
  };

  const [anchorEl, setAnchorEl] = useState(null);
  const [repeatTaskDialog, setRepeatTaskDialog] = useState(false);
  const [subTask, setSubTask] = useState(false);
  const [attachFile, setAttachFile] = useState(false);
  const [repeatTaskControl, setRepeatTaskControl] = useState(false);
  const [isSubTaskCreated, setIsSubTaskCreated] =  useState(false);
  const [addMoreArr, setAddMoreArr] = useState([]);
  useEffect(() => {
    setIsSubTaskCreated(props.isSubTaskCreated);
  }, [props.isSubTaskCreated]);
  const subtaskCmp = {
    key: 0,
    label: "subtask",
    children: (
      <SubTask
        allTasks={allTasks}
        handleSelectSubtask={handleSelectSubtask}
        currentTask={currentTask}
        handleCreateSubTask={handleCreateSubTask}
        isSubTaskCreated={isSubTaskCreated}
        projectSettings={projectSettings}
        getTaskById={getTaskById}
        permission={permission}
        intl={intl}
        selectedProject={selectedProject}
      />
    ),
  };
  const repeatTaskCmp = {
    key: 1,
    label: "repeatTask",
    children: (
      <CustomButton
        btnType="white"
        variant="contained"
        style={{
          display: "block",
          width: "100%",
          textAlign: "left",
          padding: "7px 5px",
          marginBottom: 10,
        }}
        iconBtn={true}
        onClick={handleRepeatTaskModal}
        // disabled={!permission.taskTabs.addMore.repeatTask.isAllowEdit}
        disabled={false}
      >
        <SvgIcon
          viewBox="0 0 14 12.438"
          htmlColor={theme.palette.border.brightBlue}
          className={classes.repeatedTaskIcon}
        >
          <RecurrenceIcon />
        </SvgIcon>
        <span className={classes.btnTxt}>
          {repeatTask ? getInfoMessage(repeatObj) : <FormattedMessage id="project.dev.tasks.detail.add-more.list.repeat-task" defaultMessage="Repeat task"/>}
        </span>
      </CustomButton>
    ),
  };
  const attachFileCmp = {
    key: 2,
    label: "attachment",
    children: (
      <AttachFile
        currentTask={currentTask}
        taskDetails={taskDetails}
        setTaskDetails={setTaskDetails}
        showSnackBar={showSnackBar}
        permission={permission}
        intl={intl}
      />
    ),
  };

  const handleClick = (event) => {
    // Function Opens the dropdown
    event.stopPropagation();
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };
  const handleClose = (event) => {
    // Function closes dropdown
    setAnchorEl(null);
  };
  const closeRepeatTaskDialog = () => {
    // Function closes repeat dialog
    setRepeatTaskDialog(false);
  };
  const handleRepeatTask = () => {
    // Function opens repeat dialog
    setAnchorEl(null);
    setRepeatTaskControl(true);
    if (!addMoreArr.some((el) => el["label"] == "repeatTask")) {
      setAddMoreArr([...addMoreArr, repeatTaskCmp]);
    }
  };
  
  const returnUpdatedTask = (updatedTask) => {
    /** fun calls after updating repeat task values in repeat dialog and then returns the updated repeat task */
    let updatedTaskData = {...updatedTask.data};
    updatedTaskData.taskPermission = currentTask.taskPermission;
    setCurrentTask(updatedTaskData);
    if (repeatTask) showSnackBar("Repeat Task Updated Successfully", "success");
    else showSnackBar("Repeat Task Scheduled Successfully", "success");
  };
  const handleAttachFile = (e) => {
    event.stopPropagation();
    setAttachFile(true);
    setAnchorEl(null);
    if (!addMoreArr.some((el) => el["label"] == "attachment")) {
      setAddMoreArr([...addMoreArr, attachFileCmp]);
    }
  };
  const handleSubTask = () => {
    setSubTask(true);
    setAnchorEl(null);
    if (!addMoreArr.some((el) => el["label"] == "subtask")) {
      setAddMoreArr([...addMoreArr, subtaskCmp]);
    }
  };

  const open = Boolean(anchorEl);
 
  const settingAddMoreOptions = () => {
    let arr = [];
    if (currentTask.subtasklist && currentTask.subtasklist.length > 0) {
      arr.push(subtaskCmp);
    }
    if (repeatTask) {
      arr.push(repeatTaskCmp);
    }
    if (!isEmpty(taskDetails) && taskDetails.attachments && taskDetails.attachments.length > 0) {
      arr.push(attachFileCmp);
    }
    setAddMoreArr(arr);
  };

  useEffect(() => {
    settingAddMoreOptions();
  }, [props.currentTask,props.repeatTask,props.taskDetails,isSubTaskCreated]);

  return (
    <div className={classes.addMoreCnt}>
      {/* {repeatTaskControl || repeatTask ? (
        <CustomButton
          btnType="white"
          variant="contained"
          style={{
            display: "block",
            width: "100%",
            textAlign: "left",
            padding: "7px 5px",
            marginBottom: 10,
          }}
          iconBtn={true}
          onClick={handleRepeatTaskModal}
          disabled={!permission.taskTabs.addMore.repeatTask.isAllowEdit}
        >
          <SvgIcon
            viewBox="0 0 14 12.438"
            htmlColor={theme.palette.border.brightBlue}
            className={classes.repeatedTaskIcon}
          >
            <RecurrenceIcon />
          </SvgIcon>
          <span className={classes.btnTxt}>
            {repeatTask ? getInfoMessage(repeatObj) : "Repeat task"}
          </span>
        </CustomButton>
      ) : null}

      {subTask ||
      (currentTask.subtasklist && currentTask.subtasklist.length > 0) ? (
        <SubTask
          allTasks={allTasks}
          handleSelectSubtask={handleSelectSubtask}
          currentTask={currentTask}
          handleCreateSubTask={handleCreateSubTask}
          projectSettings={projectSettings}
          getTaskById={getTaskById}
          permission={permission}
        />
      ) : null}

      {attachFile ||
      (!isEmpty(taskDetails) && taskDetails.taskDocs.length > 0) ? (
        <AttachFile
          currentTask={currentTask}
          taskDetails={taskDetails}
          setTaskDetails={setTaskDetails}
          showSnackBar={showSnackBar}
          permission={permission}
        />
      ) : null} */}
      {addMoreArr.length > 0 &&
        addMoreArr.map((element) => {
          return <div key={element.key}>{element.children}</div>;
        })}

      <CustomButton
        btnType="white"
        variant="contained"
        style={{
          display: "block",
          width: "100%",
          textAlign: "left",
          padding: "7px 5px",
        }}
        iconBtn={true}
        onClick={handleClick}
        buttonRef={anchorEl}
        disabled={!permission.taskTabs.addMore.cando}
      >
        {" "}
        <span className={classes.addMoreTxt}> <FormattedMessage id="project.dev.tasks.detail.add-more.label" defaultMessage="Add more"/></span>
      </CustomButton>
      <DropdownMenu
        open={open}
        closeAction={handleClose}
        anchorEl={anchorEl}
        size={"medium"}
        placement="bottom-start"
      >
        {/* <Scrollbars autoHide autoHeight autoHeightMin={0} autoHeightMax={130}> */}
        <List>
          {permission.taskTabs.addMore.createAddSubTask.cando && (
            <ListItem
              button
              className={classes.addMorelistItem}
              onClick={handleSubTask}
            >
              <SvgIcon
                viewBox="0 0 24 24"
                htmlColor={theme.palette.secondary.light}
                className={classes.icon}
              >
                <IconSubtask />
              </SvgIcon>
              <span><FormattedMessage id="project.dev.tasks.detail.add-more.list.subtask" defaultMessage="Subtask"/></span>
            </ListItem>
          )}
          {permission.taskTabs.addMore.attachment.isAllowAdd && (
            <ListItem
              button
              className={classes.addMorelistItem}
              onClick={handleAttachFile}
            >
              <SvgIcon
                viewBox="0 0 24 24"
                htmlColor={theme.palette.secondary.light}
                className={classes.icon}
              >
                <AttachmentIcon />
              </SvgIcon>

              <span><FormattedMessage id="project.dev.tasks.detail.add-more.list.attachement" defaultMessage="Attachment"/></span>
            </ListItem>
          )}
          {permission.taskTabs.addMore.repeatTask.isAllowAdd && (
            <ListItem
              button
              className={classes.addMorelistItem}
              onClick={handleRepeatTask}
            >
              <SvgIcon
                viewBox="0 0 24 24"
                htmlColor={theme.palette.secondary.light}
                className={classes.icon}
              >
                <RepeatIcon />
              </SvgIcon>
              <span><FormattedMessage id="project.dev.tasks.detail.add-more.list.repeat-task" defaultMessage="Repeat Task"/></span>
            </ListItem>
          )}
          <ListItem
            button
            className={classes.addMorelistItem}
            onClick={(event) => {}}
          >
            <SvgIcon
              viewBox="0 0 24 24"
              htmlColor={theme.palette.secondary.light}
              className={classes.icon}
            >
              <ReminderIcon />
            </SvgIcon>
            <span><FormattedMessage id="project.dev.tasks.detail.add-more.list.reminder" defaultMessage="Reminder"/></span>
            <span className={classes.comingSoonTxt}><FormattedMessage id="project.dev.tasks.detail.add-more.list.coming-soon" defaultMessage="Coming soon"/></span>
          </ListItem>
          <ListItem
            button
            className={classes.addMorelistItem}
            onClick={(event) => {}}
          >
            <SvgIcon
              viewBox="0 0 24 24"
              htmlColor={theme.palette.secondary.light}
              className={classes.icon}
            >
              <DependenciesIcon />
            </SvgIcon>
            <span><FormattedMessage id="project.dev.tasks.detail.add-more.list.dependencies" defaultMessage="Dependencies"/></span>
            <span className={classes.comingSoonTxt}><FormattedMessage id="project.dev.tasks.detail.add-more.list.coming-soon" defaultMessage="Coming soon"/></span>
          </ListItem>
          {/* <ListItem
            button
            className={classes.addMorelistItem}
            onClick={(event) => {}}
          >
            <SvgIcon
              viewBox="0 0 24 24"
              htmlColor={theme.palette.secondary.light}
              className={classes.icon}
            >
              <CoverImage />
            </SvgIcon>
            <span><FormattedMessage id="project.dev.tasks.detail.add-more.list.cover-image" defaultMessage="Cover Image" /></span>
            <span className={classes.comingSoonTxt}><FormattedMessage id="project.dev.tasks.detail.add-more.list.coming-soon" defaultMessage="Coming soon"/></span>
          </ListItem> */}
          <ListItem
            button
            className={classes.addMorelistItem}
            onClick={(event) => {}}
          >
            <SvgIcon
              viewBox="0 0 24 24"
              htmlColor={theme.palette.secondary.light}
              className={classes.icon}
            >
              <AddIcon />
            </SvgIcon>

            <span><FormattedMessage id="project.dev.tasks.detail.add-more.list.custom-field" defaultMessage="Custom Field"/></span>
            <span className={classes.comingSoonTxt}><FormattedMessage id="project.dev.tasks.detail.add-more.list.coming-soon" defaultMessage="Coming soon"/></span>
          </ListItem>
        </List>
        {/* </Scrollbars> */}
      </DropdownMenu>

      {repeatTaskDialog && (
        <DefaultDialog
          title="Repeat Task"
          sucessBtnText=""
          open={repeatTaskDialog}
          onClose={closeRepeatTaskDialog}
        >
          <RepeatTask
            closeAction={closeRepeatTaskDialog}
            task={currentTask}
            label={false}
            taskPer={repeatTaskPer}
            returnUpdatedTask={returnUpdatedTask}
          />
        </DefaultDialog>
      )}
    </div>
  );
}
AddMore.defaultProps = {
  classes: {},
  theme: {},
  taskPer: {},
  tasks: [],
  setCurrentTask: () => {},
  currentTask: {},
  allTasks: [],
  handleSelectSubtask: () => {},
  handleCreateSubTask: () => {},
  taskDetails: {},
  setTaskDetails: () => {},
  showSnackBar: () => {},
  projectSettings: {},
  getTaskById: () => {},
  permission: {},
  selectedProject: {},
};
const mapStateToProps = (state) => {
  return {
    taskPer: state.workspacePermissions.data.task,
    tasks: state.tasks.data,
  };
};

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, {})
)(AddMore);
