// @flow
import React, { useState, useEffect } from "react";
import taskDetailStyles from "./style";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import CloseDetailsIcon from "@material-ui/icons/MenuOutlined";
import CustomIconButton from "../../../../../components/Buttons/CustomIconButton";
import FlagIcon from "../../../../../components/Icons/FlagIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import ColorPickerDropdown from "../../../../../components/Dropdown/ColorPicker/Dropdown";
import StatusDropdown from "../../../../../components/Dropdown/StatusDropdown/Dropdown";
import { priorityData } from "../../../../../helper/taskDropdownData";
import RemoveIcon2 from "../../../../../components/Icons/RemoveIcon2";
import HideTaskDetails from "../../../../../components/Icons/HideTaskDetails";
import IconPublicLink from "../../../../../components/Icons/IconPublicLink";
import IconCopy from "../../../../../components/Icons/IconCopy";
import IconArchive from "../../../../../components/Icons/IconArchive";
import CustomTooltip from "../../../../../components/Tooltip/Tooltip";
import { CanAccessFeature } from "../../../../../components/AccessFeature/AccessFeature.cmp";
import cloneDeep from "lodash/cloneDeep";
import { FormattedMessage, injectIntl } from "react-intl";

type TaskDetailsHeaderProps = {
  classes: Object,
  theme: Object,
  handleShowHideTaskDetails: Function,
  handleActionsSelect: Function,
  taskDetails: Object,
  currentTask: Object,
  handleColorSelect: Function,
  handleSelectPriority: Function,
  setTaskDetails: Function,
  hideTaskDetails: Function,
  permission: Object,
};
function TaskDetailsHeader(props: TaskDetailsHeaderProps) {
  const {
    classes,
    theme,
    handleShowHideTaskDetails,
    handleActionsSelect,
    taskDetails,
    currentTask,
    handleColorSelect,
    handleSelectPriority,
    permission,
    setTaskDetails,
    hideTaskDetails
  } = props;

  const [selectedColor, setSelectedColor] = useState(currentTask.colorCode);
  const [taskPriority, setTaskPriority] = useState({});
  const taskPriorityData = priorityData(theme, classes, props.intl);

  useEffect(() => {
    let taskPriority = taskPriorityData.find(p => p.value == currentTask.priority) || {};
    setTaskPriority(taskPriority);
  }, [currentTask]);

  const onColorChange = color => {
    // Function handle to pull color selected in color picker
    setSelectedColor(color);
    handleColorSelect(color, currentTask, "taskDetailView");
  };
  const handlePriorityChange = (status, obj) => {
    // Handle Priority change
    handleSelectPriority(status, obj);
  };
  const handleItemClick = (e, option) => {
    let object = {
      taskId: taskDetails.entity.id,
      taskTitle: taskDetails.entity.taskTitle,
    };
    handleActionsSelect(option, object);
  };

  return (
    <div className={classes.taskDetailsHeaderCnt}>
      <div>
        <CustomTooltip
          helptext={<FormattedMessage id="common.action.hide.label" defaultMessage="Hide Detail View" />}
          iconType="help"
          placement="top"
          style={{ color: theme.palette.common.white }}>
          <CustomIconButton
            onClick={event => hideTaskDetails()}
            btnType="transparent"
            variant="contained"
            style={{ paddingLeft: 0 }}>
            <SvgIcon viewBox="0 0 18 14.261" className={classes.hideTaskDetailsIcon}>
              <HideTaskDetails color={theme.palette.text.grayDarker} />
            </SvgIcon>
          </CustomIconButton>
        </CustomTooltip>
      </div>
      <div>

        
      <CanAccessFeature group='task' feature='priority'>
        <StatusDropdown
          onSelect={priority => {
            handlePriorityChange(priority);
          }}
          iconButton={true}
          option={taskPriority}
          style={{ width: 26, height: 26, borderRadius: 4, marginRight: 6 }}
          options={taskPriorityData}
          toolTipTxt={
            <FormattedMessage id="common.action.priority.label" defaultMessage="Task Priority" />
          }
          disabled={!permission.taskTabs.taskPriority.isAllowEdit}
        />
        </CanAccessFeature>

        <CustomTooltip
          helptext={
            <FormattedMessage id="common.action.public-link.label" defaultMessage="Public Link" />
          }
          iconType="help"
          placement="top"
          style={{ color: theme.palette.common.white }}>
          <CustomIconButton
            onClick={event => {
              handleItemClick(event, "publickLink");
            }}
            btnType="transparent"
            variant="contained"
            style={{ width: 26, height: 26, borderRadius: 4, marginRight: 6 }}
            disabled={!permission.taskTabs.publicLink.cando}>
            <SvgIcon viewBox="0 0 24 24" className={classes.iconPublicLink}>
              <IconPublicLink color={theme.palette.text.grayDarker} />
            </SvgIcon>
          </CustomIconButton>
        </CustomTooltip>

        <ColorPickerDropdown
          selectedColor={selectedColor}
          onSelect={onColorChange}
          style={{ marginRight: 6 }}
          placement="bottom-end"
          disabled={!permission.taskTabs.color.cando}
        />

        <CustomTooltip
          helptext={<FormattedMessage id="common.action.copy-task.label" defaultMessage="Copy Task"/>}
          iconType="help"
          placement="top"
          style={{ color: theme.palette.common.white }}>
          <CustomIconButton
            onClick={event => {
              handleItemClick(event, "copyTask");
            }}
            btnType="transparent"
            variant="contained"
            style={{ width: 26, height: 26, borderRadius: 4, marginRight: 6 }}
            disabled={!permission.taskTabs.copyTask.cando}>
            <SvgIcon viewBox="0 0 24 24" className={classes.IconCopy}>
              <IconCopy color={theme.palette.text.grayDarker} />
            </SvgIcon>
          </CustomIconButton>
        </CustomTooltip>

        <CustomTooltip
          helptext={<FormattedMessage id="common.action.archive-task.label" defaultMessage="Archive Task"/>}
          iconType="help"
          placement="top"
          style={{ color: theme.palette.common.white }}>
          <CustomIconButton
            onClick={event => {
              handleItemClick(event, "archiveTask");
            }}
            btnType="transparent"
            variant="contained"
            style={{ width: 26, height: 26, borderRadius: 4, marginRight: 6 }}
            disabled={!permission.taskTabs.archiveTask.cando}>
            <SvgIcon viewBox="0 0 24 24" className={classes.IconArchive}>
              <IconArchive color={theme.palette.text.grayDarker} />
            </SvgIcon>
          </CustomIconButton>
        </CustomTooltip>

        <CustomTooltip
          helptext={<FormattedMessage id="common.action.delete-task.label" defaultMessage="Delete Task"/>}
          iconType="help"
          placement="top"
          style={{ color: theme.palette.common.white }}>
          <CustomIconButton
            onClick={event => {
              handleItemClick(event, "deleteTask");
            }}
            btnType="transparent"
            variant="contained"
            style={{
              width: 26,
              height: 26,
              borderRadius: 4,
              color: permission.taskTabs.deleteTask.cando ? "#de133e" : "#BFBFBF",
            }}
            disabled={!permission.taskTabs.deleteTask.cando}>
            <SvgIcon viewBox="0 0 24 24" className={classes.RemoveIcon2}>
              <RemoveIcon2 />
            </SvgIcon>
          </CustomIconButton>
        </CustomTooltip>
      </div>
    </div>
  );
}
TaskDetailsHeader.defaultProps = {
  classes: {},
  theme: {},
  handleActionsSelect: () => {},
  taskDetails: {},
  currentTask: {},
  handleColorSelect: () => {},
  handleSelectPriority: () => {},
  setTaskDetails: () => {},
  hideTaskDetails: () => {},
  permission: {},
};
const mapStateToProps = (state, ownProps) => {
  return {
    tasks: state.tasks.data,
  };
};

export default compose(
  injectIntl,
  withStyles(taskDetailStyles, { withTheme: true }),
  connect(mapStateToProps, {})
)(TaskDetailsHeader);
