// @flow
import React, { useState, useEffect, useRef } from "react";
import taskDetailStyles from "./style";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect, useDispatch } from "react-redux";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import TodoList from "../../../../../components/TodoList/TodoList";
import TaskDetailsHeader from "./TaskDetailsHeader";
import AssigneeDropdown from "../../../../../components/Dropdown/AssigneeDropdown2";
import { getMembersById } from "../../../../../helper/getMembersById";
import AddMore from "./AddMore.js";
import { projectDetails } from "../../../../../redux/actions/projects";
import { updateTaskData } from "../../../../../redux/actions/tasks";
import Grid from "@material-ui/core/Grid";
import CustomDatePicker from "../../../../../components/DatePicker/DatePicker/DatePicker";
import moment from "moment";
import TaskDetailsBreadCrumb from "../../../../../components/BreadCrumb/BreadCrumb";
import DescriptionEditor from "../../../../../components/TextEditor/CustomTextEditor/DescriptionEditor/DescriptionEditor";
import helper from "../../../../../helper";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import isEmpty from "lodash/isEmpty";
import cloneDeep from "lodash/cloneDeep";
import { FormattedMessage, injectIntl } from "react-intl";
import useUpdateTask from "../../../../../helper/customHooks/tasks/updateTask";
import { CanAccess, CanAccessFeature } from "../../../../../components/AccessFeature/AccessFeature.cmp";
import { excludeOffdays, getCalendar } from "../../../../../helper/dates/dates";
function TaskDetails(props) {
  const {
    classes,
    theme,
    handleShowHideTaskDetails,
    taskDetails,
    handleActionsSelect,
    setTaskDetails,
    allTasks,
    handleSelectSubtask,
    handleCreateSubTask,
    UpdateTask,
    projectDetails,
    showSnackBar,
    handleColorSelect,
    projectSettings,
    getTaskById,
    breadCrumbArr,
    tasks,
    UpdateStateAndCallApi,
    projectTasks,
    permission,
    setProjectTasks,
    intl,
    hideTaskDetails,
    selectedProject,
    updateTaskData
  } = props;
  const { entity: task = {}, toDoList = [] } = taskDetails;
  const dispatch = useDispatch();
  const { editTask } = useUpdateTask();
  const [currentTask, setCurrentTask] = useState(task);
  const [assignee, setAssignee] = useState(getMembersById(task.assigneeList));
  const [todoList, setTodoList] = useState([]);
  const [plannedStartDate, setPlannedStartDate] = useState("");
  const [plannedEndDate, setPlannedEndDate] = useState("");
  const [actualStartDate, setActualStartDate] = useState("");
  const [actualEndDate, setActualEndDate] = useState("");
  const [taskDescription, setTaskDescription] = useState(currentTask.description);
  const editorStateRef = useRef(currentTask.description || "");
  const [isSubTaskCreated, setIsSubTaskCreated] = useState(false);
  const { handleDateSave } = useUpdateTask();
  const updateAssignee = (assignedTo, selectedTask) => {
    /** function call when user add assigne */
    editTask(selectedTask, "assigneeList", assignedTo); /** calling update user task api to update assignees in task */
    setAssignee(assignedTo);
  };
  useEffect(() => {
    setIsSubTaskCreated(props.isSubTaskCreated);
  }, [props.isSubTaskCreated]);
  useEffect(() => {
    setCurrentTask(taskDetails.entity);
  }, [taskDetails]);

  useEffect(() => {
    setAssignee(getMembersById(task.assigneeList));
  }, [task.assigneeList]);

  useEffect(() => {
    setTodoList(toDoList);
  }, [toDoList]);

  useEffect(() => {
    setTodoList(toDoList);
    projectDetails({ sidePanel: true }); // Updating global state when the right side task details panel is opened
    return () => {
      projectDetails({ sidePanel: false }); // Updating global state when the right side task details panel is closed
    };
  }, []);
  const updateTodoList = (items = []) => {
    // Function that updates toDoList in parent state
    let updatedProjectTask = projectTasks.map(pt => {
      /** update to do item checked, deleted, added in task array */
      if (pt.taskId == currentTask.id) {
        pt.checklistItems = items.length;
        pt.completedChecklistItems = items.filter(i => i.isComplete).length;
        return pt;
      } else return pt;
    });
    setProjectTasks(updatedProjectTask);
    setTodoList(items);
    let updateTaskDetailsToDoItem = cloneDeep(taskDetails);
    updateTaskDetailsToDoItem.toDoList = items;
    updateTaskDetailsToDoItem.entity = currentTask;
    setTaskDetails(updateTaskDetailsToDoItem);
  };
  const handleUpdateDate = (key, date, time) => {
    let globalTask = tasks.find(t => t.taskId == taskDetails.entity.taskId) || {};
    let obj = {};
    switch (key) {
      case "pStart":
        obj = {
          startDate: date ? helper.RETURN_CUSTOMDATEFORMAT(date) : "",
          startTime: time,
        };
        break;
      case "pEnd":
        obj = {
          dueDate: date ? helper.RETURN_CUSTOMDATEFORMAT(date) : "",
          dueTime: time,
        };
        break;
      case "aStart":
        obj = {
          actualStartDate: date ? helper.RETURN_CUSTOMDATEFORMAT(date) : "",
          actualStartTime: time,
        };
        break;
      case "aEnd":
        obj = {
          actualDueDate: date ? helper.RETURN_CUSTOMDATEFORMAT(date) : "",
          actualDueTime: time,
        };
        break;
    }
    let updatedObject = { ...currentTask, ...obj }
    setCurrentTask(updatedObject);
    // setTaskDetails(updatedObject);
    updateTaskData(
      { task: globalTask, obj },
      dispatch,
      succ => { },
      fail => {
        showSnackBar(fail.data?.message, "error");
      }
    ); /** calling update user task api to updates in task */
  };
  const handleTaskDescription = data => {
    setTaskDescription(data.html); /** function set the description i the state */
    editorStateRef.current = data.html;
  };
  const handleClickAwayTaskDescription = () => {
    /** function responsible for updating the task description in the state and update in global task also */
    if (currentTask.description !== editorStateRef.current) {
      let globalTask = tasks.find(t => t.taskId == taskDetails.entity.taskId) || {};
      setCurrentTask({ ...currentTask, description: editorStateRef.current });
      editTask(globalTask, "description", editorStateRef.current);  /** calling update user task api to updates in task */
    }
  };
  const handlebreadCrumClick = task => {
    getTaskById(task);
  };
  const handleSelectPriority = (p, obj) => {
    setCurrentTask({
      ...currentTask,
      priority: p.value,
    }); /** updating the tassk detail view if priority change */
    let updateListViewTask = projectTasks.find(t => t.taskId == currentTask.id) || {};
    updateListViewTask.priority = p.value;
    UpdateStateAndCallApi(updateListViewTask); /** updating the task list view if priority change */
    let globalTask = tasks.find(t => t.taskId == taskDetails.entity.taskId) || {};
    editTask(globalTask, "priority", p.value); /** calling update user task api to updates in task */
  };

  const plannedMaxDate = currentTask.dueDate ? new Date(currentTask.dueDate) : null;
  const plannedMinDate = currentTask.startDate ? new Date(currentTask.startDate) : null;
  const actualMaxDate = currentTask.actualDueDate
    ? new Date(currentTask.actualDueDate)
    : null;
  const actualMinDate = currentTask.actualStartDate
    ? new Date(currentTask.actualStartDate)
    : null;
  const calendarWorkingdays = getCalendar({ projectId: currentTask.projectId }) || null;
  return (
    <div className={classes.taskDetailsCnt}>
      <TaskDetailsHeader
        handleShowHideTaskDetails={handleShowHideTaskDetails}
        handleActionsSelect={handleActionsSelect}
        taskDetails={taskDetails}
        currentTask={currentTask}
        handleColorSelect={handleColorSelect}
        handleSelectPriority={handleSelectPriority}
        permission={permission}
        setTaskDetails={setTaskDetails}
        hideTaskDetails={hideTaskDetails}
      />
      <TaskDetailsBreadCrumb
        endBreadCrumbName={currentTask.taskTitle}
        breadCrumArr={breadCrumbArr}
        handlebreadCrumClick={handlebreadCrumClick}
      />
      <TodoList
        currentTask={task}
        todoItems={todoList}
        updateTodoItemsInProjectTaskTab={updateTodoList}
        isArchivedSelected={task.isDeleted}
        progressBar={false}
        draggable={false}
        style={{ paddingRight: 220 }}
        permission={permission}
        taskPer={task.taskPermission}
        updateTodoItems={() => { }}
        intl={intl}
      />
      <CanAccessFeature group='task' feature='assigneeList'>
        <AssigneeDropdown
          assignedTo={assignee}
          updateAction={updateAssignee}
          isArchivedSelected={task.isDeleted}
          obj={currentTask}
          style={{
            marginTop: 10, marginBottom: 10,
            background: '#fff',
          }}
          permission={permission}
          customChipProps={{
            className: classes.chipRootClass,
          }}
          isAllowDelete={permission.taskTabs.taskAssignee.isAllowDelete}
          isAllowAdd={permission.taskTabs.taskAssignee.isAllowAdd}
          avatarSize={"xsmall"}
        />
      </CanAccessFeature>

      {CanAccess({ group: 'task', feature: 'startDate' })
        || CanAccess({ group: 'task', feature: 'dueDate' })
        || CanAccess({ group: 'task', feature: 'actualStartDate' })
        || CanAccess({ group: 'task', feature: 'actualDueDate' })
        ? <>
          <Grid container className={classes.datePickerCnt}>
            <CanAccessFeature group='task' feature='startDate'>
              <Grid item xs={6}>
                <CustomDatePicker
                  date={currentTask.startDate && moment(currentTask.startDate)}
                  filterDate={moment()}
                  style={{ borderRadius: 4, minWidth: "auto" }}
                  label={
                    <FormattedMessage id="project.dev.tasks.planstart" defaultMessage="Planned Start:" />
                  }
                  icon={false}
                  dateFormat="MMM DD, YYYY"
                  PopperProps={{ size: null }}
                  timeInput={true}
                  datePickerProps={{
                    maxDate: plannedMaxDate || undefined,
                    filterDate: date => {
                      return excludeOffdays(moment(date), calendarWorkingdays)
                    },
                  }}
                  onSelect={(date, time) => {
                    handleUpdateDate("pStart", date, time);
                  }}
                  selectedTime={currentTask.startTime}
                  timeInputLabel={intl.formatMessage({
                    id: "meeting.creation-dialog.form.start-time.label",
                    defaultMessage: "Start Time",
                  })}
                  btnProps={{ className: classes.datePickerCustomStyle }}
                  disabled={!permission.taskTabs.plannedStartDate.isAllowEdit}
                  deleteIcon={true}
                  placeholder={intl.formatMessage({
                    id: "meeting.creation-dialog.form.date.placeholder",
                    defaultMessage: "Select Date",
                  })}
                />
              </Grid>
            </CanAccessFeature>
            <CanAccessFeature group='task' feature='dueDate'>
              <Grid item xs={6}>
                <CustomDatePicker
                  date={currentTask.dueDate && moment(currentTask.dueDate)}
                  filterDate={moment()}
                  style={{ borderRadius: 4, minWidth: "auto" }}
                  icon={false}
                  label={
                    <FormattedMessage id="project.dev.tasks.planend" defaultMessage="Planned End:" />
                  }
                  dateFormat="MMM DD, YYYY"
                  PopperProps={{ size: null }}
                  datePickerProps={{
                    minDate: plannedMinDate || undefined,
                    filterDate: date => {
                      return excludeOffdays(moment(date), calendarWorkingdays)
                    },
                  }}
                  timeInput={true}
                  onSelect={(date, time) => {
                    handleUpdateDate("pEnd", date, time);
                  }}
                  selectedTime={currentTask.dueTime}
                  timeInputLabel={intl.formatMessage({
                    id: "common.time.action2",
                    defaultMessage: "End Time",
                  })}
                  btnProps={{ className: classes.datePickerCustomStyle }}
                  disabled={!permission.taskTabs.plannedEndDate.isAllowEdit}
                  deleteIcon={true}
                  placeholder={intl.formatMessage({
                    id: "meeting.creation-dialog.form.date.placeholder",
                    defaultMessage: "Select Date",
                  })}
                />
              </Grid>
            </CanAccessFeature>
            <Grid xs={12}>
              <div className="divider"></div>
            </Grid>
            <CanAccessFeature group='task' feature='actualStartDate'>
              <Grid item xs={6}>
                <CustomDatePicker
                  date={currentTask.actualStartDate && moment(currentTask.actualStartDate)}
                  filterDate={moment()}
                  style={{ borderRadius: 4, minWidth: "auto" }}
                  icon={false}
                  label={
                    <FormattedMessage id="project.dev.tasks.actualstart" defaultMessage="Actual Start:" />
                  }
                  dateFormat="MMM DD, YYYY"
                  datePickerProps={{
                    maxDate: actualMaxDate || undefined,
                  }}
                  PopperProps={{ size: null }}
                  timeInput={true}
                  onSelect={(date, time) => {
                    handleUpdateDate("aStart", date, time);
                  }}
                  selectedTime={currentTask.actualStartTime}
                  timeInputLabel={intl.formatMessage({
                    id: "meeting.creation-dialog.form.start-time.label",
                    defaultMessage: "Start Time",
                  })}
                  btnProps={{ className: classes.datePickerCustomStyle }}
                  disabled={!permission.taskTabs.actualStartDate.isAllowEdit}
                  deleteIcon={true}
                  placeholder={intl.formatMessage({
                    id: "meeting.creation-dialog.form.date.placeholder",
                    defaultMessage: "Select Date",
                  })}
                />
              </Grid>
            </CanAccessFeature>
            <CanAccessFeature group='task' feature='actualDueDate'>
              <Grid item xs={6}>
                <CustomDatePicker
                  date={currentTask.actualDueDate && moment(currentTask.actualDueDate)}
                  filterDate={moment()}
                  style={{ borderRadius: 4, minWidth: "auto" }}
                  icon={false}
                  label={
                    <FormattedMessage id="project.dev.tasks.actualend" defaultMessage="Actual End:" />
                  }
                  dateFormat="MMM DD, YYYY"
                  datePickerProps={{
                    minDate: actualMinDate || undefined,
                  }}
                  timeInput={true}
                  PopperProps={{ size: null }}
                  onSelect={(date, time) => {
                    handleUpdateDate("aEnd", date, time);
                  }}
                  selectedTime={currentTask.actualDueTime}
                  timeInputLabel={intl.formatMessage({
                    id: "common.time.action2",
                    defaultMessage: "End Time",
                  })}
                  btnProps={{ className: classes.datePickerCustomStyle }}
                  disabled={!permission.taskTabs.actualEndDate.isAllowEdit}
                  deleteIcon={true}
                  placeholder={intl.formatMessage({
                    id: "meeting.creation-dialog.form.date.placeholder",
                    defaultMessage: "Select Date",
                  })}
                />
              </Grid>
            </CanAccessFeature>
          </Grid>
        </> : null}

      {permission.taskTabs.taskDescription.cando && CanAccess({ group: 'task', feature: 'description' }) && (
        // <ClickAwayListener onClickAway={handleClickAwayTaskDescription}>
        <DescriptionEditor
          defaultValue={taskDescription}
          onChange={handleTaskDescription}
          placeholder={intl.formatMessage({
            id: "common.type.label",
            defaultMessage: "Type something",
          })}
          handleClickAway={handleClickAwayTaskDescription}
        />
        // </ClickAwayListener>
      )}
      <AddMore
        currentTask={currentTask}
        setCurrentTask={setCurrentTask}
        allTasks={allTasks}
        handleSelectSubtask={handleSelectSubtask}
        handleCreateSubTask={handleCreateSubTask}
        isSubTaskCreated={isSubTaskCreated}
        taskDetails={taskDetails}
        setTaskDetails={setTaskDetails}
        showSnackBar={showSnackBar}
        projectSettings={projectSettings}
        getTaskById={getTaskById}
        permission={permission}
        intl={intl}
        selectedProject={selectedProject}
      />
    </div>
  );
}
TaskDetails.defaultProps = {
  classes: {},
  theme: {},
  handleShowHideTaskDetails: () => { },
  handleActionsSelect: () => { },
  taskDetails: {},
  setTaskDetails: () => { },
  allTasks: [],
  handleSelectSubtask: () => { },
  handleCreateSubTask: () => { },
  UpdateTask: () => { },
  showSnackBar: () => { },
  handleColorSelect: () => { },
  projectSettings: {},
  getTaskById: () => { },
  breadCrumbArr: [],
  tasks: [],
  UpdateStateAndCallApi: () => { },
  projectTasks: [],
  permission: {},
  selectedProject: {},
  setProjectTasks: () => { },
  hideTaskDetails: () => { },
};
const mapStateToProps = (state, ownProps) => {
  return {
    tasks: state.tasks.data,
  };
};

export default compose(
  injectIntl,
  withStyles(taskDetailStyles, { withTheme: true }),
  connect(mapStateToProps, { projectDetails, updateTaskData })
)(TaskDetails);
