// @flow

import React, { useState, useEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import SvgIcon from "@material-ui/core/SvgIcon";
import CustomButton from "../../../../../components/Buttons/CustomButton";
import AttachmentIcon from "../../../../../components/Icons/IconAttachment";
import Dropzone from "react-dropzone";
import isEmpty from "lodash/isEmpty";
import {
  attachFile,
  attachMultipleFile,
  saveMultipleAttachment,
  saveAttachments,
  saveUserComment,
  deleteAttachmentFile,
} from "../../../../../redux/actions/projects";
import CustomIconButton from "../../../../../components/Buttons/IconButton";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import DropdownMenu from "../../../../../components/Dropdown/DropdownMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
// import { Document, Page } from "react-pdf";
// import PdfFile from "../../../../../assets/images/dummy.pdf";
import Dialog from "@material-ui/core/Dialog";
import helper from "../../../../../helper";
import { getIconByAttachmentType } from "../../../../../helper/getIconByAttachmentType";
import IconUploading from "../../../../../components/Icons/IconUploading";
import { FormattedMessage } from "react-intl";
import moment from "moment";

const acceptedFormats = [
  " application/msword",
  "application/x-zip-compressed",
  "application/vnd.ms-powerpoint",
  "application/vnd.ms-excel",
  "application/vnd.ms-excel",
  "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
  "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
  "text/csv",
  "text/plain",
  "application/pdf",
  "application/zip",
  "image/bmp",
  "image/gif",
  "image/jpeg",
  "image/png",
  "image/webp",
  "video/webm",
  "video/ogg",
  "video/mp4",
];
type AttachFileProps = {
  classes: Object,
  theme: Object,
  currentTask: Object,
  profileState: Object,
  tasks: [],
  attachFile: Function,
  attachMultipleFile: Function,
  saveAttachments: Function,
  saveUserComment: Function,
  taskDetails: Object,
  allMembers: Array,
  setTaskDetails: Function,
  deleteAttachmentFile: Function,
  showSnackBar: Function,
  permission: Object,
};

function AttachFile(props: AttachFileProps) {
  const {
    classes,
    theme,
    currentTask,
    profileState,
    tasks,
    attachFile,
    attachMultipleFile,
    saveMultipleAttachment,
    saveAttachments,
    saveUserComment,
    taskDetails,
    allMembers,
    setTaskDetails,
    deleteAttachmentFile,
    showSnackBar,
    permission,
    intl,
  } = props;

  const [anchorEl, setAnchorEl] = useState(null);
  const [numPages, setNumPages] = useState(null);
  const [pageNumber, setPageNumber] = useState(1);
  const [percentage, setPercentage] = useState(0);
  const [selectedAttachment, setSelectedAttachment] = useState({});

  const onDocumentLoadSuccess = ({ numPages }) => {
    setNumPages(numPages);
  };
  const onDrop = acceptedFiles => {
    /** function call when user select file from computer and add */
    setAnchorEl(null);
    if (!isEmpty(acceptedFiles)) {
      var data = new FormData();
      data.append("File", acceptedFiles[0]);
      let task = tasks.find(t => t.taskId == currentTask.taskId) || {};
      if (!isEmpty(task)) {
        const options = {
          onUploadProgress: progressEvent => {
            const { loaded, total } = progressEvent;
            let percent = Math.floor((loaded * 100) / total);
            if (percent < 100) {
              setPercentage(percent);
            }
          },
        };

        data.append("taskid", task.taskId);
        data.append("userid", profileState.userId);
        attachMultipleFile(
          /** uploading file to s3 bucket */
          data,
          succ => {
            if (succ) {
              saveAttachmentApi(
                acceptedFiles,
                succ[0],
                task
              ); /** on succ saving file information to db */

              setPercentage(100);
              setTimeout(() => {
                setPercentage(0);
              }, 1000);
            }
          },
          err => {
            if (err.data) showSnackBar(err.data.message, "error");
          },
          options
        );
      }
    } else {
      showSnackBar("Oops! File not supported.", "error");
    }
  };

  const saveAttachmentApi = (acceptedFiles, fileObj, task) => {
    /** function save the uploaded file info into db */
    let obj = [{
      // docsType: acceptedFiles[0].type,
      // fileName: acceptedFiles[0].name,
      // groupType: "Task",
      // isTaskFile: true,
      // taskId: task.taskId,
      // fileSize: acceptedFiles[0].size,
      // path: docPath,
      type: fileObj.fileType,
      name: fileObj.fileName,
      taskId: task.taskId,
      groupId: task.taskId,
      groupType: "Task",
      fileSize: fileObj.fileSize,
      path: fileObj.fileId,
      fileID: fileObj.fileId,
      fileName: fileObj.fileName,
      iconCategory: helper.getFileIconType(fileObj.fileExtension),
      fileExtension: fileObj.fileExtension,
    }];
    saveMultipleAttachment(
      /** db call */
      obj,
      succ => {
        if (succ){
           let taskAttachments = {
             ...taskDetails,
             attachments: [succ[0], ...taskDetails.attachments],
           };
           setTaskDetails(taskAttachments);
        }
          // saveUserCommentApi(
          //   acceptedFiles,
          //   succ[0].fileId,
          //   task
          // ); /** on succ db call , saving info of user who added the attachment in db */
      },
      err => {
        if (err.data) showSnackBar(err.data.message, "error");
      }
    );
  };
  // const saveUserCommentApi = (acceptedFiles, docId, task) => {
  //   /**function saving info of user who added the attachment in db */
  //   let object = {
  //     commentText: acceptedFiles[0].name,
  //     taskId: task.taskId,
  //     isTaskFile: true,
  //     commentType: 1,
  //     fileCommentId: docId,
  //   };
  //   const comment = {
  //     commentText: [0].name,
  //     taskId: task.taskId,
  //     commentType: 1,
  //     fileCommentId: docId,
  //     commentText: "",
  //     descriptionDelta: "",
  //     descriptionText: "",
  //     groupId: task.taskId,
  //     groupType: "Task",
  //     sharedWith: ["all"], // [getMessageMember().key],
  //   };
  //   saveUserComment(
  //     comment,
  //     data => {
  //       if (data) {
  //         /** on succ adding attachments into task details  */
  //         let taskAttachments = {
  //           ...taskDetails,
  //           attachments: [...taskDetails.attachments, data.comment.attachment]
  //         };
  //         setTaskDetails(taskAttachments);
  //         // showSnackBar("File added successfully", "success");
  //       }
  //     },
  //     err => {
  //       if (err.data) showSnackBar(err.data.message, "error");
  //     }
  //   );
  // };
  const convertBytes = bytes => {
    /** func that converts the bytes into diff file sizes */
    const sizes = ["Bytes", "KB", "MB", "GB", "TB"];
    if (bytes == 0) {
      return 0;
    }
    const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    if (i == 0) {
      return bytes + " " + sizes[i];
    }

    return (bytes / Math.pow(1024, i)).toFixed(1) + " " + sizes[i];
  };

  const handleActionClick = (event, item) => {
    // Function Opens the dropdown
    event.stopPropagation();
    setSelectedAttachment(item);
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };
  const handleClose = event => {
    // Function closes dropdown
    event.stopPropagation();
    setAnchorEl(null);
    setSelectedAttachment({});
  };
  const downloadFile = (event, obj) => {
    /** function call for downloading the desired attachment */
    event.stopPropagation();
    setAnchorEl(null);
    let data = {
      size: selectedAttachment.fileSize,
      type: selectedAttachment.type,
    };
    helper.DOWNLOAD_TEMPLATE(
      selectedAttachment.fileId,
      selectedAttachment.name,
      null,
      "attachment"
    );

    // let anchor = document.createElement("a");
    // anchor.href = `https://ntaskattachments.s3.amazonaws.com/ProfileImages/AttachedFiles/${obj.comment.attachment.docsName}`;
    // anchor.target = "_blank";
    // anchor.download = obj.comment.attachment.docsName;
    // anchor.click();
  };

  const handleDeleteAttachment = (event, obj) => {
    /** function for deleteing the desired attachment */
    event.stopPropagation();
    let id = selectedAttachment.id;
    deleteAttachmentFile(
      id,
      succ => { 
        if (succ) {
          let filterAttachmentsArr = taskDetails.attachments.filter(
            /** on succ filtering the deleted attachment from the taskDoc array and setting the state */
            a => a.id !== selectedAttachment.id
          );
          setTaskDetails({
            ...taskDetails,
            attachments: filterAttachmentsArr,
          });
          setAnchorEl(null);
          showSnackBar("File deleted successfully", "success");
          setSelectedAttachment({});
        }
      },
      err => {
        if (err.data) {
          showSnackBar(err.data.message, "error");
          setAnchorEl(null);
          setSelectedAttachment({});
          }
      }
    );
  };

  const attachedFiles =
    !isEmpty(taskDetails) && taskDetails.attachments && taskDetails.attachments.length > 0
      ? true
      : false; /** if attachment are not empty then show attached files and change respectively UI design */
  const open = Boolean(anchorEl);
  // const renderPages = () => {
  //   let PageCmp = [];
  //   let i;
  //   for (i = 0; i <= numPages; i++) {
  //     PageCmp.push(<Page pageNumber={i} />)
  //   }
  //   return PageCmp
  // }
  const getUserName = (userId) => {
    let selectedMember = allMembers.find(member => {
      return member.userId == userId;
    });
    return selectedMember.fullName;
  };
  return (
    <>
      {/* <Dialog
        open={true}
        disableBackdropClick={true}
        // onClose={(event) => props.handleDialogClose(event, "projectDialog")}
        onClick={(e) => {
          e.stopPropagation();
        }}
        PaperProps={{
          style: {
            maxWidth: "90%",
            minHeight: 708,
            background: theme.palette.common.white
            // minWidth: projectDetailView ? 1100 : 650,
          },
        }}
        classes={{
          // paper: classes.dialogPaperCnt,
          // scrollBody: classes.dialogCnt,
        }}
      >
        <Document
          // file={PdfFile}
          onLoadSuccess={onDocumentLoadSuccess}
        >
          {renderPages().map(p => {
            return (
              <>{p}</>
            )
          })}
        </Document>
        <p>Page {pageNumber} of {numPages}</p>


      </Dialog> */}
      <div
        className={
          attachedFiles || percentage > 0 ? classes.attachmentCnt : classes.attachmentEmptyCnt
        }>
        <Dropzone onDrop={onDrop} accept={acceptedFormats}>
          {({ getRootProps, getInputProps }) => (
            <div {...getRootProps()} className={classes.outlineHover}>
              {permission.taskTabs.addMore.attachment.isAllowAdd ? (
                <input {...getInputProps()} />
              ) : null}
              <CustomButton
                btnType="white"
                variant="contained"
                className={classes.attachmentBtn}
                style={
                  attachedFiles || percentage > 0
                    ? {
                        marginBottom: 0,
                        border: "none",
                      }
                    : {
                        marginBottom: 5,
                      }
                }
                iconBtn={true}
                disabled={!permission.taskTabs.addMore.attachment.isAllowAdd}
                // onClick={handleAttachFile}
              >
                <SvgIcon
                  viewBox="0 0 24 24"
                  htmlColor={theme.palette.border.brightBlue}
                  className={classes.AttachmentIcon}>
                  <AttachmentIcon />
                </SvgIcon>
                <span className={classes.btnTxt}>
                  <FormattedMessage
                    id="project.dev.tasks.detail.upload-file.label"
                    defaultMessage="Upload file"
                  />
                </span>
              </CustomButton>
            </div>
          )}
        </Dropzone>

        {attachedFiles || percentage > 0 ? (
          <ul className={classes.attachmentList}>
            {percentage > 0 && (
              <li key={1} style={{ minHeight: 47 }}>
                <div style={{ display: "flex" }}>
                  <SvgIcon viewBox="0 0 22.08 19.853">
                    <IconUploading />
                  </SvgIcon>
                  <div className={classes.attachFileHeader}>
                    <span className={classes.rowTxt} style={{ margin: "5px 10px" }}>
                      {`${intl.formatMessage({
                        id: "project.dev.tasks.detail.upload-file.uploading-label",
                        defaultMessage: "Uploading",
                      })} ${percentage}%`}
                    </span>
                  </div>
                </div>
              </li>
            )}
            {attachedFiles &&
              taskDetails.attachments.filter(item => item.folderId == "").map(a => {
                let item = {...a}
                return (
                  <>
                    <li key={item.id}>
                      <div style={{ display: "flex" }}>
                        {getIconByAttachmentType(theme, classes, item.type)}
                        <div className={classes.attachFileHeader}>
                          <span className={classes.rowTxt}> {item.name} </span>
                          <span className={classes.rowTxtSubtitle}>
                            {" "}
                            {`${convertBytes(item.fileSize)}`}
                            <span className={classes.dot}>•</span>
                            {`${intl.formatMessage({
                              id: "project.dev.tasks.detail.upload-file.added-by-label",
                              defaultMessage: "Added by",
                            })} ${getUserName(item.createdBy)}`}
                            <span className={classes.dot}>•</span>
                            {`${moment(item.createdDate).format('llll')}`}
                          </span>
                        </div>
                      </div>
                      <CustomIconButton
                        btnType="condensed"
                        style={{ padding: 0 }}
                        onClick={(event)=>handleActionClick(event, item)}
                        buttonRef={anchorEl}>
                        <MoreVerticalIcon
                          htmlColor={theme.palette.secondary.medDark}
                          style={{ fontSize: "24px" }}
                        />
                      </CustomIconButton>
                      <ClickAwayListener onClickAway={handleClose}>
                        <DropdownMenu
                          open={open}
                          closeAction={() => {}}
                          anchorEl={anchorEl}
                          size={"small"}
                          placement="bottom-end">
                          <List>
                            {/* <ListItem button className={classes.listItem} onClick={event => {}}>
                              <span>
                                <FormattedMessage
                                  id="project.dev.tasks.detail.upload-file.list.view"
                                  defaultMessage="View"
                                />
                              </span>
                            </ListItem> */}
                            <ListItem
                              button
                              className={classes.listItem}
                              onClick={event => {
                                downloadFile(event, item);
                              }}>
                              <span>
                                <FormattedMessage
                                  id="project.dev.tasks.detail.upload-file.list.download"
                                  defaultMessage="Download"
                                />
                              </span>
                            </ListItem>
                            {permission.taskTabs.addMore.attachment.isAllowDelete && (
                              <ListItem
                                button
                                className={classes.listItem}
                                onClick={event => {
                                  handleDeleteAttachment(event, item);
                                }}>
                                <span className={classes.deleteTxt}>
                                  <FormattedMessage
                                    id="common.action.delete.label"
                                    defaultMessage="Delete"
                                  />
                                </span>
                              </ListItem>
                            )}
                          </List>
                        </DropdownMenu>
                      </ClickAwayListener>
                    </li>{" "}
                  </>
                );
              })}
          </ul>
        ) : null}
      </div>
    </>
  );
}

AttachFile.defaultProps = {
  /** defaults props */
  classes: {},
  theme: {},
  currentTask: {},
  profileState: {},
  tasks: [],
  attachFile: () => {},
  attachMultipleFile: () => {},
  saveAttachments: () => {},
  saveMultipleAttachment: () => {},
  saveUserComment: () => {},
  taskDetails: {},
  allMembers: [],
  setTaskDetails: () => {},
  deleteAttachmentFile: () => {},
  showSnackBar: () => {},
  permission: {},
};
const mapStateToProps = state => {
  return {
    tasks: state.tasks.data,
    profileState: state.profile.data,
    allMembers: state.profile.data.member.allMembers,
  };
};

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, {
    attachFile,
    attachMultipleFile,
    saveAttachments,
    saveMultipleAttachment,
    saveUserComment,
    deleteAttachmentFile,
  })
)(AttachFile);
