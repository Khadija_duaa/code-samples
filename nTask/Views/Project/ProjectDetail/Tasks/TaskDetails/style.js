const taskDetailStyles = theme => ({
  taskDetailsCnt: {
    padding: "8px 10px",
  },
  taskDetailsHeaderCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  priorityIcon: {
    fontSize: "17px !important",
  },
  hideTaskDetailsIcon: {
    fontSize: "17px !important",
  },
  iconPublicLink: {
    fontSize: "20px !important",
  },
  IconCopy: {
    fontSize: "20px !important",
  },
  IconArchive: {
    fontSize: "20px !important",
  },
  RemoveIcon2: {
    fontSize: "22px !important",
    // color: theme.palette.text.danger,
  },
  addMoreTxt: {
    fontSize: "13px !important",
    fontWeight: 600,
    fontFamily: theme.typography.fontFamilyLato,
    color: "#0090ff",
    paddingLeft: 7,
  },
  addMoreCnt: {
    marginTop: 10,
  },
  listItem: {
    padding: "7px 14px",
    fontSize: "13px !important",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
    "&:hover": {
      backgroundColor: "transparent !important",
    },
  },
  dot: {
    color: theme.palette.text.hint,
    fontSize: "12px !important",
    margin: "0px 5px",
  },
  addMorelistItem: {
    padding: "5px 12px",
    fontSize: "13px !important",
    fontWeight: 600,
    color: "#202020",
    fontFamily: theme.typography.fontFamilyLato,
    "&:hover": {
      backgroundColor: "transparent !important",
    },
  },
  icon: {
    fontSize: "19px !important",
    marginRight: 10,
  },
  iconImage: {
    fontSize: "30px !important",
    marginRight: 10,
    color: theme.palette.text.grayDarker,
  },
  iconVideo: {
    fontSize: "30px !important",
    marginRight: 10,
    color: theme.palette.text.grayDarker,
  },
  iconSubtask: {
    fontSize: "15px !important",
    marginLeft: 5,
  },
  countTxt: {
    color: "#969696",
    fontSize: "13px !important",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
  },
  iconFile: {
    fontSize: "30px !important",
    marginRight: 8,
    marginLeft: 3,
    color: theme.palette.text.grayDarker,
  },
  iconZip: {
    fontSize: "30px !important",
    marginRight: 8,
    marginLeft: 3,
    color: theme.palette.text.grayDarker,
  },
  btnTxt: {
    fontSize: "13px !important",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
    // color: "#0090ff",
    paddingLeft: 7,
  },
  repeatedTaskIcon: {
    fontSize: "14px !important",
    // marginTop: 6,
    marginLeft: 7,
  },
  AttachmentIcon: {
    marginLeft: 7,
    fontSize: "18px !important",
  },
  subTaskIcon: {
    fontSize: "22px !important",
    marginTop: 8,
    // marginLeft: 2,
  },
  outlinedTaskInput: {
    padding: "0px",
    background: theme.palette.background.default,
    borderRadius: 4,
    fontSize: "13px !important",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
  },
  statusIcon: {
    fontSize: "22px !important",
  },
  notchedOutlineCntt: {
    border: "none",
  },
  addIconCnt: {
    margin: " 0px 0px 0 6px",
  },
  addIconSubtask: {
    padding: 3,
    border: `1px solid #0090ff`,
    "&:hover": {
      backgroundColor: "#0090ff",
      "& $addBtnIcon": {
        color: "#fff",
      },
    },
  },
  addBtnIcon: {
    fontSize: "16px !important",
    color: "#0090ff",
  },
  TaskList: {
    listStyleType: "none",
    padding: 0,
    margin: 0,
    position: "relative",

    "& li": {
      display: "flex",
      alignItems: "center",
      cursor: "pointer",
      borderBottom: `1px solid ${theme.palette.border.grayLighter}`,
      padding: "7px 6px",
    },
  },
  attachmentList: {
    listStyleType: "none",
    padding: 0,
    margin: 0,

    "& li": {
      display: "flex",
      alignItems: "center",
      cursor: "pointer",
      borderTop: `1px solid ${theme.palette.border.grayLighter}`,
      padding: "7px 6px 7px 15px",
      flexDirection: "row",
      justifyContent: "space-between",
    },
  },
  assigneeUl: {
    listStyleType: "none",
    padding: 0,
    margin: 0,
    display: "flex",
    position: "absolute",
    right: 35,
    "& li": {
      borderBottom: "none",
      padding: 0,
    },
  },
  rowTxt: {
    fontSize: "13px !important",
    fontWeight: 700,
    fontFamily: theme.typography.fontFamilyLato,
    color: "#202020",
    wordBreak: "break-word",
  },
  rowTxtSubTask: {
    fontSize: "13px !important",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
    color: "#202020",
  },
  subTaskCnt: {
    border: `1px solid ${theme.palette.border.lightBorder}`,
    // borderTop: "none",
    backgroundColor: "white",
    borderRadius: 6,
    marginBottom: 10,
    "&:hover": {
      border: `1px solid ${theme.palette.border.lightBlueBorder}`,
    },
  },
  subTaskEmptyCnt: {
    marginBottom: 10,
    // paddingBottom: 10,
    background: theme.palette.common.white,
    border: "1px solid rgba(221, 221, 221, 1)",
    borderRadius: 4,
    "&:hover": {
      border: `1px solid ${theme.palette.border.lightBlueBorder}`,
    },
  },
  attachmentEmptyCnt: {
    marginBottom: 5,
  },
  outlineHover: {
    "&:hover &containedwhite": {
      border: `1px solid ${theme.palette.border.lightBlueBorder} !important`,
    },
  },
  attachmentCnt: {
    border: `1px solid ${theme.palette.border.lightBorder}`,
    // borderTop: "none",
    backgroundColor: "white",
    borderRadius: 6,
    marginBottom: 5,
    "&:hover": {
      border: `1px solid ${theme.palette.border.lightBlueBorder}`,
    },
  },
  attachmentBtn: {
    display: "block",
    width: "100%",
    textAlign: "left",
    padding: "7px 5px",
    textTransform: "none",
  },
  arrowRight: {
    position: "absolute",
    right: 15,
    fontSize: "18px !important",
  },
  selectedIcon: {
    marginRight: 7,
    marginTop: 3,
    marginLeft: 3,
  },
  comingSoonTxt: {
    background: "#fcb829",
    color: "white",
    padding: 5,
    fontSize: "11px !important",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
    borderRadius: 2,
    position: "absolute",
    right: 7,
  },
  TotalAssignee: {
    background: theme.palette.secondary.lightBlue,
    color: theme.palette.secondary.main,
    width: 30,
    height: 30,
    fontSize: "12px !important",
    marginLeft: -7,
    fontWeight: 600,
    fontFamily: theme.typography.fontFamilyLato,
  },
  taskTitle: {
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 700,
    color: "#202020",
    fontSize: "18px !important",
    marginTop: 10,
  },
  rowTxtSubtitle: {
    color: "#969696",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
    fontSize: "12px !important",
    marginTop: 5,
  },
  attachFileHeader: {
    display: "grid",
  },
  deleteTxt: {
    color: theme.palette.text.danger,
  },
  datePickerCnt: {
    background: theme.palette.common.white,
    padding: 10,
    border: `1px solid ${theme.palette.border.grayLighter}`,
    borderRadius: 4,
    marginTop: 10,
    marginBottom: 10,
  },
  subTaskDropdownCnt: {
    padding: "0px 10px 0 10px",
    display: "flex",
  },
  datePickerCustomStyle: {
    background: "transparent",
    border: "none !important",
    borderRadius: "4px !important",
    padding: "0px 4px !important",
    "&:hover": {
      background: `${theme.palette.background.items} !important`,
    },
  },
  miniLoader: {
    width: 20,
    height: 20,
    border: `1px solid rgba(243, 243, 243, 1)`,
    borderTop: `1px solid #0090ff`,
  },
  loaderContainer: {
    height: '30px',
    width: '15px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 20,
    marginTop: 8
  },
  chipRootClass: {
    width: '100%',
    padding: 3,
    height: 30,
    background: theme.palette.background.medium,
    margin: "2px 6px 3px 0px",
    btnStylePeople: {
      marginTop: "0px !important",
      marginBottom: "0px !important",
      boxShadow: "none !important",
      border: "none !important",
      flex: "1 !important",
      background: "transparent !important",
      fontFamily: `${theme.typography.fontFamilyLato} !important`,
      fontWeight: `${theme.typography.fontWeightExtraLight} !important`,
      padding: "1px 8px !important",
      fontSize: "13px !important",
      display: "flex !important",
      justifyContent: "start !important",
      "&:hover": {
        background: "#F6F6F6 !important",
      },
    },
  },
});

export default taskDetailStyles;
