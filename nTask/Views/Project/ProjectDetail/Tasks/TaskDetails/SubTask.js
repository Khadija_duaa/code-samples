import React, { useState, useEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import CustomAvatar from "../../../../../components/Avatar/Avatar";
import Avatar from "@material-ui/core/Avatar";
import IconSubtask from "../../../../../components/Icons/ProjectAddMoreIcons/IconSubtask";
import SvgIcon from "@material-ui/core/SvgIcon";
import { statusDataNew } from "../../../../../helper/taskDropdownData";
// import { statusData } from "../../../../../helper/projectDropdownData";

import { Scrollbars } from "react-custom-scrollbars";
import ArrowRight from "@material-ui/icons/KeyboardArrowRight";
import CreateableSelectDropdown from "../../../../../components/Dropdown/SelectCreateableDropdown/SelectCreateableDropdown";
import Truncate from "react-truncate";
import CustomTooltip from "../../../../../components/Tooltip/Tooltip";
import CircularIcon from "@material-ui/icons/Brightness1";

type SubTaskProps = {
  classes: Object,
  theme: Object,
  allTasks: Array,
  handleSelectSubtask: Function,
  currentTask: Object,
  handleCreateSubTask: Function,
  allMembers: Array,
  workspaceStatus: Array,
  projectSettings: Object,
  getTaskById: Function,
  permission: Object,
  selectedProject: Object,
};

function SubTask(props: SubTaskProps) {
  const {
    classes,
    theme,
    allTasks,
    handleSelectSubtask,
    currentTask,
    handleCreateSubTask,
    allMembers,
    projectSettings,
    getTaskById,
    permission,intl,
    workspaceStatus,
    selectedProject
  } = props;
  const [isSubTaskCreated , setIsSubTaskCreated] = useState(false);
  useEffect(() => {
    setIsSubTaskCreated(props.isSubTaskCreated);
  },[props.isSubTaskCreated]);
  const generateAssigneeList = (assigneIdArr = []) => {
    /** function return the assignees in form of avatar to the subtask array */
    const selectedTaskList = allMembers.filter((m) => {
      return assigneIdArr.indexOf(m.userId) > -1;
    });
    if (selectedTaskList.length > 0) {
      return selectedTaskList.slice(0, 2).map((Assignee, i) => {
        return (
          <li key={i} style={{ marginLeft: !i == 0 ? -7 : null }}>
            <CustomAvatar
              otherMember={{
                imageUrl: Assignee.imageUrl,
                fullName: Assignee.fullName,
                lastName: "",
                email: Assignee.email,
                isOnline: false,
                isOwner: false,
              }}
              size="xsmall"
              disableCard
            />
          </li>
        );
      });
    } else return null;
  };
  const getSubtaskCount = (task) => {
    let count = projectSettings.tasks.filter((pt) => pt.parentId == task.taskId)
      .length;
    if (count > 0) {
      return (
        <>
          {" "}
          <SvgIcon
            viewBox="0 0 24 24"
            htmlColor={theme.palette.secondary.light}
            className={classes.iconSubtask}
          >
            <IconSubtask />
          </SvgIcon>
          <span
            className={classes.countTxt}
            style={{
              color: "#969696",
            }}
          >
            {count}
          </span>{" "}
        </>
      );
    }
  };
  const getTaskDetails = (t) => {
    let task = {
      taskId: t.taskId,
      parentId: currentTask.id,
    };
    getTaskById(task);
  };

  const statusData = statusArr => {
    if (statusArr) {
      return statusArr.statusList.map(item => {
        return {
          label: item.statusTitle,
          value: item.statusId,
          icon: <CircularIcon htmlColor={item.statusColor} style={{ fontSize: "20px" }} />,
          statusColor: item.statusColor,
        };
      });
    }
  };

  const taskStatusData = statusDataNew(theme, classes);
  const taskStatusDataDropDown = statusData(
    selectedProject.projectTemplate ? selectedProject.projectTemplate : workspaceStatus
  );
  const currentTaskCheck =
    currentTask.subtasklist && currentTask.subtasklist.length > 0;
  return (
    <>
      <div
        className={
          currentTaskCheck ? classes.subTaskCnt : classes.subTaskEmptyCnt
        }
      >
        <div
          className={classes.subTaskDropdownCnt}
          style={{
            borderBottom: currentTaskCheck ? "1px solid #EAEAEA" : "none",
            borderTop: currentTaskCheck ? "1px solid #EAEAEA" : "none",
          }}
        >
          <SvgIcon
            viewBox="0 0 24 24"
            htmlColor={theme.palette.border.brightBlue}
            className={classes.subTaskIcon}
          >
            <IconSubtask />
          </SvgIcon>
          {isSubTaskCreated ? <div className={classes.loaderContainer} >
              <div className={`loader ${classes.miniLoader}`}></div>
          </div> :
          <CreateableSelectDropdown
            data={() => {
              return allTasks;
            }}
            label={null}
            placeholder={intl.formatMessage({id:"project.dev.tasks.detail.create-subtask.placeholder",defaultMessage:"Select subtask or add new"})}
            id="selectsubTaskTitleDropdown"
            name="task"
            selectOptionAction={handleSelectSubtask}
            createOptionAction={handleCreateSubTask}
            disableIndicator={true}
            // removeOptionAction={this.handleRemoveOption}
            type="task"
            createText={intl.formatMessage({id:"project.dev.tasks.detail.create-subtask.label",defaultMessage:"Add subtask"})}
            isMulti={false}
            selectedValue={[]}
            createLabelValidation={() => {
              return true;
            }}
            clearOptionAfterSelect={true}
            styles={{
              marginTop: 0,
              marginBottom: 0,
            }}
            customStyles={{ control: { border: "none" } }}
            isDisabled={!permission.taskTabs.addMore.createAddSubTask.cando}
            noOptionMessage={"Create new subtask"}
          /> }
        </div>
        {currentTaskCheck && (
          // <Scrollbars autoHide autoHeight autoHeightMin={0} autoHeightMax={150}>
          <ul className={classes.TaskList}>
            {currentTask.subtasklist.map((s) => {
              let selectedStatus = taskStatusDataDropDown.find(
                (t) => t.value == s.status
              );
              return (
                <>
                  <li
                    key={s.taskId}
                    onClick={(event) => {
                      getTaskDetails(s);
                    }}
                  >
                    {selectedStatus && <span className={classes.selectedIcon}>
                      <CustomTooltip
                        helptext={selectedStatus.label}
                        iconType="help"
                        placement="top"
                        style={{ color: theme.palette.common.white }}
                      >
                        {selectedStatus.icon}
                      </CustomTooltip>
                    </span>}
                    <span className={classes.rowTxtSubTask} title={s.taskName}>
                      <Truncate
                        trimWhitespace={true}
                        width={250}
                        ellipsis={<span>...</span>}
                      >
                        {s.taskName}
                      </Truncate>{" "}
                    </span>
                    {getSubtaskCount(s)}
                    <ul className={classes.assigneeUl}>
                      {generateAssigneeList(s.assigneeList)}
                      {s.assigneeList.length > 2 ? (
                        <li>
                          <Avatar classes={{ root: classes.TotalAssignee }}>
                            +{s.assigneeList.length - 2}
                          </Avatar>
                        </li>
                      ) : null}
                    </ul>
                    <ArrowRight
                      className={classes.arrowRight}
                      htmlColor={theme.palette.secondary.medDark}
                    />
                  </li>{" "}
                </>
              );
            })}
          </ul>
          // </Scrollbars>
        )}
      </div>
    </>
  );
}

SubTask.defaultProps = {
  classes: {},
  theme: {},
  allTasks: [],
  handleSelectSubtask: () => {},
  currentTask: {},
  handleCreateSubTask: () => {},
  allMembers: [],
  projectSettings: {},
  getTaskById: () => {},
  permission: {},
  selectedProject: {},
};
const mapStateToProps = (state) => {
  return {
    tasks: state.tasks.data,
    allMembers: state.profile.data.member.allMembers,
    workspaceStatus: state.workspaceTemplates.data.defaultWSTemplate,
  };
};

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, {})
)(SubTask);
