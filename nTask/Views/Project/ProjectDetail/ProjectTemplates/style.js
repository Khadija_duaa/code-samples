const ProjectStatusStyles = theme => ({
  templateCnt: {
    display: "flex",
    height: "100%",
    backgroundColor: "#FAFAFA",
    borderTop: "1px solid #EAEAEA",
    marginTop: 19,
  },
  statusListCnt: {
    width: "75%",
    backgroundColor: "white",
    borderLeft: "1px solid #EAEAEA",
    display: 'flex',
  },
});

export default ProjectStatusStyles;
