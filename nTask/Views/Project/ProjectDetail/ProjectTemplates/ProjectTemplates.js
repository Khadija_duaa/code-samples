// @flow

import React, { useState, useEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import StatusList from "../../../../components/Templates/StatusList";
import {
  // SaveProjectTemplateStatusItem,
  // MapTaskWithSetDefaultStatus,
  // DeleteAndMapStatus,
  UseCustomTemplateInProject,
  UseTemplateInProject,
  UpdateProjectCustomTemplate
} from "../../../../redux/actions/projectTemplates";
import {
  CreateWSTemplate,
  // UpdateWSSavedTemplate
} from "../../../../redux/actions/workspaceTemplates";
import TaskStatus from "../../../../components/Templates/ProjectTemplatesList/TaskStatusTemplates";
import { getProjectTemplates } from "../../../../redux/actions/workspace";
import { FormattedMessage, injectIntl } from "react-intl";
import { getTemplateType } from "../../../../utils/common";
import templatesConstants from "../../../../utils/constants/templatesConstants";
import appConstant from "../../../../redux/constants/types";
import cloneDeep from "lodash/cloneDeep";

type ProjectTemplatesProps = {
  classes: Object,
  theme: Object,
  project: Object,
  projectId: String,
  UseCustomTemplateInProject: PropTypes.func.isRequired,
  UpdateProjectCustomTemplate: PropTypes.func.isRequired,
  UseTemplateInProject: PropTypes.func.isRequired,
};

function ProjectTemplates(props: ProjectTemplatesProps) {
  /** Financial Summary main content  */
  const {
    theme,
    classes,
    projectAllTemplates,
    projectPer,
    intl,
    projectId,
    projectTemplate,
    updateCustomTemplate,
    workspacePer,
    project,
    showSnackBar,
    updateTaskInProjectModal
  } = props;

  const [selectedTemplate, setSelectedTemplate] = useState(projectTemplate);
  const templatePermission = {
    isDeleted: false,
  };
  const templateConfig = {
    contextView: templatesConstants.PROJECT_VIEW,
    contextId: projectId,
    contextKeyId: "projectId",
    templateType: selectedTemplate && getTemplateType(selectedTemplate),
    projectTemplate: projectTemplate,
    workspaceDefaultTemplate: props.workspaceDefault,
    project: project
  };

  useEffect(() => {
    props.getProjectTemplates(
      templateConfig.contextId,
      res => {
        const selectedStatus = getDefaultStatusTemp(res.data.entity);
        setSelectedTemplate(selectedStatus);
      },
      error => {}
    );
    return () => {};
  }, []);

  const getDefaultStatusTemp = data => {
    let defaultStatus;
    if(projectTemplate) return projectTemplate;
    else{
      data.forEach(g => {
        g.templates.forEach(t => {
          if (t.isWorkspaceDefault) {
            defaultStatus = t;
          }
        });
      });
      return defaultStatus;
    }
  };

  const selectTemplateItem = templateItem => {
    if (projectTemplate && projectTemplate.templateId == templateItem.templateId) {
      setSelectedTemplate(projectTemplate);
    } else {
      setSelectedTemplate(templateItem);
    }
  };
  const useCustomTemplateHandler = (item,success,fail) => {
    item.projectId = templateConfig.contextId;
    props.UseCustomTemplateInProject(item,res => {
      success(res);
      updateCustomTemplate(res);
      selectTemplateItem(res);
    },err => {
      fail(err);
      showSnackBar(err.data.message,"error");
    }, (data)=>{
      updateTaskInProjectModal(data);
  })
  }
  const UpdateProjectCustomTemplateHandler = (item,success,fail) => {
    item.projectId = templateConfig.contextId;
    setSelectedTemplate(item.customstatus);
    props.UpdateProjectCustomTemplate(item,res => {
      success(res);
      updateCustomTemplate(res);
      // selectTemplateItem(res);
    },err => {
      fail(err);
      showSnackBar(err.data.message,"error");
    }, (data)=>{
        updateTaskInProjectModal(data);
    })
  }
  const CreateWSTemplateHandler = (item,success,fail) => {
    props.CreateWSTemplate(item,res => {
      success(res);
      showSnackBar(
        intl.formatMessage({
          id: "board.template.message.success",
          defaultMessage: "New template is saved",
        }),
        "success"
      ); 
    },err => {
      fail(err);
    }, (data)=>{
      updateTaskInProjectModal(data);
  })
  }
  const useTemplateHandler = (item,success,fail) => {
    item.projectId = templateConfig.contextId;
    props.UseTemplateInProject(item,res => {
      success(res);
      // if(res.templateId == props.workspaceDefault.templateId){
      //   updateCustomTemplate(null);
      // }else{
        updateCustomTemplate(res);
      // }
      setSelectedTemplate(res);
    },err => {
      fail(err);
    }, (data)=>{
        updateTaskInProjectModal(data);
    });
  }
  return (
    <>
      <div className={classes.templateCnt}>
        <div style={{ width: "250px", paddingTop : 15 }}>
          {projectAllTemplates ? (
            <TaskStatus
              data={cloneDeep(projectAllTemplates)}
              selectedTemplate={{customstatus: selectedTemplate, mappingStatus: []}}
              onTemplateSelect={selectTemplateItem}
              templatePermission={templatePermission}
              templateConfig={templateConfig}
              selectedIndex={0}
              intl={intl}
              workspaceDefaultTemplate = {props.workspaceDefault}
              permission={workspacePer.workspaceSetting.tasksStatus}
            />
          ) : null}
        </div>
        <div className={classes.statusListCnt}>
          {projectAllTemplates && selectedTemplate ? (
            <StatusList
              templateItem={{customstatus: selectedTemplate, mappingStatus: []}}
              draggable
              style={{ paddingRight: 220 }}
              // permission={projectPer}
              templatePermission={templatePermission}
              templateConfig={templateConfig}
              intl={intl}
              workSpaceStatus={projectAllTemplates}
              CreateTemplate={CreateWSTemplateHandler}
              UpdateSaveTemplate={UpdateProjectCustomTemplateHandler}
              UseCustomTemplate={useCustomTemplateHandler}
              UseStandardTemplate={useTemplateHandler}
              permission={workspacePer.workspaceSetting.tasksStatus}
            />
          ) : null}
        </div>
      </div>
    </>
  );
}

ProjectTemplates.defaultProps = {
  theme: {},
  classes: {},
  workspacePer:{},
  project:{},
  CreateWSTemplate(e) {
    return e;
  },
  UpdateProjectCustomTemplate(e) {
    return e;
  },
  UseCustomTemplateInProject(e) {
    return e;
  },
  UseTemplateInProject(e) {
    return e;
  },
  updateCustomTemplate(e) {
    return e;
  },
  updateTaskInProjectModal(e) {
    return e;
  }
};

const mapStateToProps = state => {
  return {
    projectPer: state.workspacePermissions.data.workSpace,
    projectAllTemplates: state.workspaceTemplates.data.allWSTemplates,
    workspaceDefault: state.workspaceTemplates.data.defaultWSTemplate,
    workspacePer: state.workspacePermissions.data.workSpace,
  };
};

export default compose(
  injectIntl,
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, {
    // SaveProjectTemplateStatusItem,
    CreateWSTemplate,
    UpdateProjectCustomTemplate,
    // MapTaskWithSetDefaultStatus,
    // DeleteAndMapStatus,
    UseCustomTemplateInProject,
    UseTemplateInProject,
    getProjectTemplates,
  })
)(ProjectTemplates);
