const resourcesStyles = (theme) => ({
  resourceCnt: {
    padding: "0px 0px 0px 5px",
  },
  projectResourcesList: {
    listStyleType: "none",
    padding: 0,
    margin: 0,

    "& li": {
      display: "flex",
      alignItems: "center",
      borderBottom: `1px solid ${theme.palette.border.grayLighter}`,
      padding: "0 4px",
      "&:hover $editIconCnt": {
        visibility: "visible",
      },
    },
  },
  resourceProfileInfo: {
    flex: 1,
  },
  weeklyCapacity: {
    width: 100,
  },
  fullName: {
    color: "#202020",
    fontSize: "13px !important",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,

    overflow: "hidden",
    whiteSpace: "nowrap",
    lineHeight: "normal",
    textOverflow: "ellipsis",
    width: 300,
  },
  jobTitle: {
    color: "#7E7E7E",
    fontSize: "12px !important",
    fontWeight: 400,
    lineHeight: "normal",
    fontFamily: theme.typography.fontFamilyLato,
    cursor: "pointer",

    overflow: "hidden",
    whiteSpace: "nowrap",
    lineHeight: "normal",
    textOverflow: "ellipsis",
    // width: 300,
  },
  headerTitle: {
    color: theme.palette.text.grayDarker,
    fontSize: "12px !important",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
  },
  header: {
    borderBottom: "none !important",
  },
  weeklyCapacityHead: {
    width: 100,
    paddingLeft: 7,
  },
  hourlyRate: {
    width: 90,
  },
  hourlyRateHead: {
    width: 90,
    paddingLeft: 7,
  },
  permissionHead: {
    width: 160,
    paddingLeft: 10,
  },
  permission: {
    width: 160,
  },
  actionColumn: {
    width: 30,
  },
  row: {
    minHeight: 57,
  },
  youText: {
    fontSize: "12px !important",
    color: theme.palette.text.medGray,
  },
  addResourceLabel: {
    color: "#646464",
    fontSize: "13px !important",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
  },
  addResourcePlaceholder: {
    color: "#BFBFBF",
    fontSize: "13px !important",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
  },
  jobRoleCnt: {
    display: "flex",
  },
  editIcon: {
    fontSize: "11px !important",
    marginTop: -4,
    marginLeft: 5,
  },
  editIconCnt: {
    visibility: "hidden",
  },
  columnThree: {
    width: 42,
  }
});

export default resourcesStyles;
