// @flow
import React, { useState, useEffect } from "react";
import resourcesStyles from "./style";
import SelectSearchDropdown from "../../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import Hotkeys from "react-hot-keys";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Typography from "@material-ui/core/Typography";
import {
  generateProjectAssigneeData,
  generateSelectData,
} from "../../../../helper/generateSelectData";
import { permissionData } from "../../../../helper/projectDropdownData";
import { withStyles } from "@material-ui/core/styles";
import WeeklyCapcityInput from "../../../../components/Form/WeeklyCapcityInput/WeeklyCapcityInput";
import differenceWith from "lodash/differenceWith";
import SvgIcon from "@material-ui/core/SvgIcon";
import RemoveIcon2 from "../../../../components/Icons/RemoveIcon2";
import CustomIconButton from "../../../../components/Buttons/CustomIconButton";
import CustomTooltip from "../../../../components/Tooltip/Tooltip";
import CustomAvatar from "../../../../components/Avatar/Avatar";
import AmountInput from "../../../../components/Form/AmountInput/AmountInput";
import CustomSelectDropdown from "../../../../components/Dropdown/CustomSelectDropdown/Dropdown";
import DefaultTextField from "../../../../components/Form/TextField";
import DeleteConfirmDialog from "../../../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import DefaultCheckbox from "../../../../components/Form/Checkbox";

import IconEditSmall from "../../../../components/Icons/IconEditSmall";
import { FormattedMessage } from "react-intl";
type DropdownProps = {
  classes: Object,
  theme: Object,
  label: String,
  onInputChange: Function,
  setResources: Function,
  currency: String,
  placeholder: String,
  workspaceMembers: Array<Object>,
  resources: Array<Object>,
  projectSettings: Object,
  selectedCurrency: Object,
  selectedFee: Object,
  profileState: Object,
  updateProjectSettings: Function,
  permission: Object,
  billingMethod: Object,
};

//Onboarding Main Component
function ProjectResources(props: DropdownProps) {
  const {
    classes,
    theme,
    workspaceMembers,
    projectSettings,
    selectedCurrency,
    selectedFee,
    updateProjectSettings,
    profileState,
    permission,
    resources,
    setResources,
    intl,
    billingMethod
  } = props;
  // const [resources, setResources] = useState([]);
  const [allMembers, setAllMembers] = useState([]);
  const [editJobRole, setEditJobRole] = useState();
  // const initProjectResources = () => {
  //   //initialization state with project resources in list
  //   const projectResources = projectSettings.resources;
  //   const projectResourcesList = differenceWith(
  //     workspaceMembers,
  //     projectResources,
  //     (a, b) => {
  //       return a.userId != b.userId;
  //     }
  //   );
  //   setResources(projectResources);
  // };
  const initAllMembers = () => {
    // initializing state with all members in dropdown
    const projectResources = projectSettings.resources;
    const AllMembers = workspaceMembers.filter(m => m.isActive && m.isDeleted == false);
    const allMembersList = differenceWith(AllMembers, projectResources, (a, b) => {
      return a.userId == b.userId;
    });

    setAllMembers(generateProjectAssigneeData(allMembersList));
  };

  useEffect(() => {
    // initProjectResources();
    initAllMembers();
  }, []);

  const [dialog, setDialog] = useState(false);
  const handleDialogClose = () => {
    setDialog(false);
    setDeleteResource({});
  };

  const [deletedResource, setDeleteResource] = useState({});
  const handleDelete = () => {
    setAllMembers([...allMembers, generateProjectAssigneeData([deletedResource])[0]]); // converting value to select control readable form
    let updatedObj = resources.filter(m => {
      return m.userId != deletedResource.userId;
    });
    setResources(updatedObj);
    updateProjectSettings("resources", updatedObj, {});
    setDialog(false);
  };

  //Local state is update for dropdown so object can be made to send update to backend with API
  //Updating local state when option is selected in assignee dropdown
  const handleOptionsSelect = (type, value) => {
    let val = { ...value.obj };
    let projectRoleId =
      val.roleId == "000" || val.roleId == "006" || val.roleId == "002"
        ? "101"
        : "102"; /** if selected member is team owner, team admin or workspace admin then by default it will appear as project manager 101 else 102 contributor */
    let obj = {
      designation: val.jobTitle,
      email: val.email,
      estimatedHour: 0,
      firstName: "",
      fullName: val.fullName,
      hourlyRate: 0,
      imageUrl: val.imageUrl,
      isAssignee: false,
      isBillable: billingMethod.value === 3 && projectSettings.billingMode == 1,
      isOnline: val.isOnline,
      isOwner: val.teamOwner,
      isProjectManager: projectRoleId == "101" ? true : false,
      lastName: "",
      projectRoleId: projectRoleId,
      userId: val.userId,
      workSpaceRoleId: val.roleId
    };
    let updatedObj = [...resources, obj];
    setResources(updatedObj);
    setAllMembers(
      allMembers.filter(m => {
        return m.obj.userId != value.obj.userId;
      })
    );
    //billingMethod.value
    updateProjectSettings("resources", updatedObj, {});
  };

  const handleRemove = value => {
    // Function responsible for removing a project resource from list
    setDeleteResource(value);
    setDialog(true);
  };

  const handleRoleChangeInput = event => {
    setEditJobRole({ ...editJobRole, designation: event.target.value });
  };
  const handleJobTitleEdit = (e, resource) => {
    e.stopPropagation();
    setEditJobRole(resource);
  };
  const handleJobTitleBlur = (resource = {}) => {
    let updatedObj = resources.map(m => {
      if (m.userId == editJobRole.userId) {
        return editJobRole;
      } else return m;
    });
    setResources(updatedObj);
    updateProjectSettings("resources", updatedObj, {});
    setEditJobRole({});
  };

  const handleInputBlur = (v, obj, value = "") => {
    let object = obj;
    let hours = v == "" ? 0 : v;
    switch (value) {
      case "weeklyCapacity":
        object.estimatedHour = hours;
        break;

      case "amount":
        object.hourlyRate = hours;
        break;

      default:
        break;
    }
    updatesResources(object);
  };

  const updatesResources = obj => {
    let updatedObj = resources.map(m => {
      if (m.userId == obj.userId) {
        return obj;
      } else return m;
    });
    updateProjectSettings("resources", updatedObj, {});
  };

  const handleUpdatePermission = (per, obj) => {
    if (per) {
      let updatedObj = obj;
      updatedObj.projectRoleId = per.value;
      updatedObj.isProjectManager = per.value == "101" ? true : false;
      updatesResources(updatedObj);
    }
  };
  const handleUpdateBillable = (obj, val) => {
    /** function calls when user changed the billable state of the resources to true/false */
    let updatedObj = { ...obj, isBillable: val };
    updatesResources(updatedObj);
  };
  const getEditIcon = resource => {
    return (
      <div className={classes.editIconCnt}>
        <CustomTooltip
          helptext="Edit Job Role"
          iconType="help"
          placement="top"
          style={{ color: theme.palette.common.white }}>
          <CustomIconButton
            onClick={event => {
              handleJobTitleEdit(event, resource);
            }}
            btnType="transparent"
            variant="contained"
            style={{
              padding: "3px 3px 0px 0px",
              backgroundColor: "transparent",
            }}
            disabled={false}>
            <SvgIcon
              viewBox="0 0 12 11.957"
              // htmlColor={theme.palette.secondary.medDark}
              className={classes.editIcon}>
              <IconEditSmall />
            </SvgIcon>
          </CustomIconButton>
        </CustomTooltip>
      </div>
    );
  };

  const disablePermission = item => {
    const { currentUser } = props;
    if (permission.resourceTabs.projectPermissions.cando) {
      /** if role is PM then check if there is more than 1 PM then enables permission ddown, other wise check if role is contributor or viewer then check if resources list have more than 1 resource then enables permission ddown other wise disable */
      let PMLength = resources.filter(r => r.projectRoleId == "101").length;
      if (currentUser.userId == item.userId && item.projectRoleId == "101") {
        if (PMLength == 1) return true;
        else return false;
      }
      //Team owner and PM
      else if (item.projectRoleId == "101" && (item.workSpaceRoleId == "000" ||
        item.workSpaceRoleId == "006" ||
        item.workSpaceRoleId == "002")
      ) {
        return true;
      } else if (item.projectRoleId != "101") {
        return false;
      }
    }
    else {
      return true;
    }
  };

  const showDeleteBtnHandler = item => {
    const { currentUser } = props;
    let PMLength = resources.filter(r => r.projectRoleId == "101").length;
    if (permission.resourceTabs.deleteResource.cando) {
      if (currentUser.userId == item.userId && item.projectRoleId == "101") {
        if (PMLength > 1) return true;
        if (PMLength == 1) return false;
      }
      if (currentUser.userId !== item.userId && item.projectRoleId == "101") {
        if (PMLength > 1) return true;
        if (PMLength == 1) return false;
      }
      if (item.projectRoleId !== "101") {
        return true;
      }
    } else {
      return false;
    }
  };
  const disableDeleteBtnHandler = () => {
    let PMLength = resources.filter(r => r.projectRoleId == "101");
    return PMLength.length > 0 ? false : true;
  };

  let diffRate =
    projectSettings.projectBillingMethod == 3 && projectSettings.feeType == 1 ? true : false;

  return (
    <div className={classes.resourceCnt}>
      <SelectSearchDropdown
        data={() => {
          return allMembers;
        }}
        label={
          <span className={classes.addResourceLabel}>
            <FormattedMessage
              id="project.dev.resources.add-resources.label"
              defaultMessage="Add Resource(s)"
            />
          </span>
        }
        selectChange={handleOptionsSelect}
        type="assignee"
        selectedValue={[]}
        placeholder={
          <span className={classes.addResourcePlaceholder}>
            <FormattedMessage
              id="project.dev.resources.add-resources.placeholder"
              defaultMessage="Add resource(s) to the project"
            />
          </span>
        }
        avatar={true}
        truncateOptionWidth={450}
        captionKey="email"
        isMulti={false}
        isDisabled={!permission.resourceTabs.addProjectResources.cando}
        noOptionsMessage={
          <span style={{fontSize: "15px"}}>
            <FormattedMessage
              id="project.dev.resources.action"
              defaultMessage="You do not have any resource in your workspace yet."
            />
          </span>
        }
      />

      <ul className={classes.projectResourcesList}>
        <li className={classes.header}>
          <div className={classes.resourceProfileInfo}>
            <span className={classes.headerTitle}>
              <FormattedMessage
                id="project.dev.resources.headers.teammebers"
                defaultMessage="Team Members"
              />
            </span>
          </div>
          <div className={classes.weeklyCapacityHead}>
            <span className={classes.headerTitle}>
              <FormattedMessage
                id="project.dev.resources.headers.weekcapacity"
                defaultMessage="Weekly Capacity"
              />
            </span>
          </div>
          {diffRate /** 1 means user selected the differrent rate per resource */ && (
            <div className={classes.hourlyRateHead}>
              <span className={classes.headerTitle}>
                <FormattedMessage
                  id="project.dev.resources.headers.hourlyrate"
                  defaultMessage="Hourly Rate"
                />
              </span>
            </div>
          )}
          <div className={classes.permissionHead}>
            <span className={classes.headerTitle}>
              <FormattedMessage
                id="project.dev.resources.headers.permissions"
                defaultMessage="Permissions"
              />
            </span>
          </div>
          {billingMethod.value == 3 && <div className={classes.columnThree}>
            <span className={classes.headerTitle}>
              <FormattedMessage
                id="project.dev.milestones.header.billable"
                defaultMessage="Billable"
              />
            </span>
          </div>}
          <div className={classes.actionColumn}></div>
        </li>
        {resources.map(r => {
          let selectedPermission =
            permissionData().find(p => p.value == parseInt(r.projectRoleId)) || {};
          let PMLength = resources.filter(r => r.projectRoleId == "101")
            .length; /** if PM is more than one then show delete icon */
          let disablePermissionDdown = disablePermission(r);
          let showDeleteBtn = showDeleteBtnHandler(r);
          let disableDeleteBtn =
            permission.resourceTabs.deleteResource.cando && disableDeleteBtnHandler();

          return (
            <li className={classes.row} key={r.userId}>
              <div className={classes.resourceProfileInfo} style={{ width: diffRate ? 200 : 300 }}>
                <div className="flex_center_start_row">
                  <CustomAvatar
                    styles={{ marginRight: 6 }}
                    otherMember={{
                      imageUrl: r.imageUrl,
                      firstName: r.fullName,
                      lastName: "",
                      email: r.email,
                      isOnline: r.isOnline,
                      isOwner: r.isOwner,
                    }}
                    size="small"
                  />
                  {editJobRole && editJobRole.userId == r.userId ? (
                    <Hotkeys keyName="enter" onKeyDown={handleJobTitleBlur}>
                      <DefaultTextField
                        label=""
                        error={false}
                        size="small"
                        formControlStyles={{ marginBottom: 0, width: 130 }}
                        defaultProps={{
                          type: "text",
                          id: "ResourceJobTitle",
                          placeholder: intl.formatMessage({
                            id: "project.dev.resources.add-job-role.placeholder",
                            defaultMessage: "Add Job Role",
                          }),
                          value: editJobRole.designation,
                          autoFocus: true,
                          onBlur: handleJobTitleBlur,
                          inputProps: { maxLength: 80 },
                          onChange: event => {
                            handleRoleChangeInput(event);
                          },
                        }}
                      />
                    </Hotkeys>
                  ) : (
                    <div>
                      <Typography
                        variant="body2"
                        title={r.fullName}
                        className={classes.fullName}
                        style={{ width: diffRate ? 150 : 200 }}>
                        {r.fullName}
                        <span className={classes.youText}>
                          &nbsp;
                          {r.userId == profileState.data.userId ? (
                            <FormattedMessage
                              id="project.dev.resources.you.label"
                              defaultMessage="(You)"
                            />
                          ) : null}
                        </span>
                      </Typography>
                      <div className={classes.jobRoleCnt}>
                        <Typography
                          variant="caption"
                          style={{ maxWidth: diffRate ? 150 : 200 }}
                          // onClick={() => handleJobTitleEdit(r)}
                          className={classes.jobTitle}
                          title={r.designation ? r.designation : null}>
                          {r.designation
                            ? r.designation
                            : intl.formatMessage({
                              id: "project.dev.resources.add-job-role.placeholder",
                              defaultMessage: "Add Job Role",
                            })}
                        </Typography>
                        {permission.resourceTabs.jobRole.cando && getEditIcon(r)}
                      </div>
                    </div>
                  )}
                </div>
              </div>
              <div className={classes.weeklyCapacity}>
                {" "}
                <WeeklyCapcityInput
                  placeholder="Add hours"
                  actualCapacity={r.estimatedHour || 0}
                  onInputChange={() => { }}
                  onInputBlur={hours => {
                    handleInputBlur(hours, r, "weeklyCapacity");
                  }}
                  permission={permission.resourceTabs.weeklyCapacity.cando}
                />{" "}
              </div>
              {diffRate /** 1 means user selected the differrent rate per resource */ && (
                <div className={classes.hourlyRate}>
                  {" "}
                  <AmountInput
                    label=""
                    placeholder={intl.formatMessage({
                      id: "project.dev.details.billable.per-hour.placeholder",
                      defaultMessage: "Add Rate",
                    })}
                    onInputChange={() => { }}
                    currency={projectSettings.currency}
                    savedAmount={r.hourlyRate}
                    onBlur={amount => {
                      handleInputBlur(amount, r, "amount");
                    }}
                    disabled={!permission.resourceTabs.addRate.cando}
                  />{" "}
                </div>
              )}
              <div className={classes.permission}>
                <CustomSelectDropdown
                  label=""
                  options={permissionData}
                  option={selectedPermission}
                  onSelect={per => {
                    handleUpdatePermission(per, r);
                  }}
                  icon={false}
                  height={"110px"}
                  scrollHeight={180}
                  disabled={disablePermissionDdown}
                />
              </div>
              {billingMethod.value == 3 && <div className={classes.columnThree}>
                <DefaultCheckbox
                  checkboxStyles={{ padding: "5px 5px 4px 8px" }}
                  checked={r.isBillable}
                  unCheckedIconProps={{
                    style: { fontSize: "20px" },
                  }}
                  checkedIconProps={{
                    style: { fontSize: "20px" },
                  }}
                  onChange={() => {
                    handleUpdateBillable(r, !r.isBillable);
                  }}
                  fontSize={20}
                  color={"#0090ff"}
                  disabled={!permission.resourceTabs.resourceBillable.cando}
                />
              </div>}
              <div className={classes.actionColumn}>
                {showDeleteBtn && (
                  <CustomTooltip
                    helptext={
                      <FormattedMessage
                        id="team-settings.billing.bill-dialog.remove-button.label"
                        defaultMessage="Remove"
                      />
                    }
                    iconType="help"
                    placement="top"
                    style={{ color: theme.palette.common.white }}>
                    <CustomIconButton
                      onClick={() => handleRemove(r)}
                      btnType="transparent"
                      variant="contained"
                      style={{
                        color: "#FF4A4A",
                      }}
                      disabled={false}>
                      <SvgIcon viewBox="0 0 24 24">
                        <RemoveIcon2 />
                      </SvgIcon>
                    </CustomIconButton>
                  </CustomTooltip>
                )}
              </div>
            </li>
          );
        })}
      </ul>

      {dialog && (
        <DeleteConfirmDialog
          open={dialog}
          closeAction={handleDialogClose}
          cancelBtnText={
            <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
          }
          successBtnText={
            <FormattedMessage id="common.action.delete.label" defaultMessage="Delete" />
          }
          alignment="center"
          headingText={
            <FormattedMessage
              id="project.dev.resources.delete-resource-confirmation.title"
              defaultMessage="Delete"
            />
          }
          successAction={handleDelete}
          msgText={
            <FormattedMessage
              id="project.dev.resources.delete-resource-confirmation.label"
              defaultMessage="Are you sure you want to delete this resource?"
            />
          }
          btnQuery={""}
        />
      )}
    </div>
  );
}
ProjectResources.defaultProps = {
  classes: {},
  theme: {},
  onInputChange: () => { },
  setResources: () => { },
  resources: [],
  workspaceMembers: [],
  projectSettings: {},
  profileState: {},
  permission: {},
};
const mapStateToProps = (state, ownProps) => {
  let profileState = state.profile;
  let workspaceMembers = profileState.data.member.allMembers;
  let currentUser = workspaceMembers.find(m => {
    /** finding current user from team members array */
    return m.userId == profileState.data.userId;
  });
  return {
    workspaceMembers: workspaceMembers,
    profileState: profileState,
    currentUser: currentUser,
  };
};
export default compose(
  withStyles(resourcesStyles, { withTheme: true }),
  connect(mapStateToProps, {})
)(ProjectResources);
