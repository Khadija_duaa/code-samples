import React, { useState, useEffect, useRef } from "react";
import { withStyles } from "@material-ui/core/styles";
import combineStyles from "../../../../utils/mergeStyles";
import styles from "./style";
import DatePicker from "react-datepicker";
import Grow from "@material-ui/core/Grow";
import Popper from "@material-ui/core/Popper";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import IconDate from "../../../../components/Icons/IconDate";
import SvgIcon from "@material-ui/core/SvgIcon";
import CustomButton from "../../../../components/Buttons/CustomButton";
import moment from "moment";

function DatePickerWithButton(props) {
  const {
    theme,
    classes,
    dateSelected = null,
    minDate = null,
    onSelect,
    icon = false,
    style = null,
  } = props;

  const [selectedDate, setSelectedDate] = useState(dateSelected);
  const anchorEl = useRef();

  const [open, setOpen] = useState(false);
  const handleDateBtnClick = () => {
    setOpen(!open);
  };
  const handleClose = () => {
    setOpen(false);
    onSelect(selectedDate);
  };
  const handleDateSelect = (date) => {
    setSelectedDate(date);
    setOpen(false);
  };

  return (
    <>
      <CustomButton
        onClick={handleDateBtnClick}
        btnType="plain"
        disableRipple
        variant="text"
        disabled={false}
        buttonRef={(node) => {
          anchorEl.current = node;
        }}
      >
        {icon && (
          <SvgIcon viewBox="0 0 24 24" className={classes.dateIcon}>
            <IconDate />
          </SvgIcon>
        )}
        <span className={classes.date}>
          {moment(selectedDate).format("ll")}
        </span>
      </CustomButton>
      <Popper
        open={open}
        transition
        disablePortal
        className={style}
        anchorRef={anchorEl.current}
      >
        {({ TransitionProps, placement }) => (
          <Grow
            {...TransitionProps}
            id="menu-list-grow"
            style={{
              transformOrigin: "center bottom",
            }}
          >
            <div
              onClick={(event) => {
                event.stopPropagation();
              }}
              className={classes.rangeDatePickerInner}
            >
              <ClickAwayListener onClickAway={handleClose}>
                <DatePicker
                  inline
                  // selected={moment(selectedDate)}
                  // onSelect={handleDateSelect}
                  minDate={minDate}
                  selected={selectedDate ? moment(selectedDate).toDate() : moment().toDate() }
                  onChange={handleDateSelect}
                />
              </ClickAwayListener>
            </div>
          </Grow>
        )}
      </Popper>{" "}
    </>
  );
}

export default withStyles(styles, { withTheme: true })(DatePickerWithButton);
