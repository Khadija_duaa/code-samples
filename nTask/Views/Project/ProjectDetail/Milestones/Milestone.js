// @flow

import React, { useState, useEffect, Fragment } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import CustomDatePicker from "../../../../components/DatePicker/DatePicker/DatePicker";
import DefaultTextField from "../../../../components/Form/TextField";
import Typography from "@material-ui/core/Typography";
import Hotkeys from "react-hot-keys";
import EmptyState from "../../../../components/EmptyStates/EmptyState";
import CustomButton from "../../../../components/Buttons/CustomButton";
import IconButton from "@material-ui/core/IconButton";
import moment from "moment";
import isEmpty from "lodash/isEmpty";
import SvgIcon from "@material-ui/core/SvgIcon";
import CheckBoxIcon from "../../../../components/Icons/CheckBoxIcon";
import Checkbox from "@material-ui/core/Checkbox";
import DefaultCheckbox from "../../../../components/Form/Checkbox";
import AddIcon from "@material-ui/icons/Add";
import RoundIcon from "@material-ui/icons/Brightness1Outlined";
import RemoveIcon2 from "../../../../components/Icons/RemoveIcon2";
import CustomIconButton from "../../../../components/Buttons/CustomIconButton";
import CustomTooltip from "../../../../components/Tooltip/Tooltip";
import { validMilestoneTitle } from "../../../../utils/validator/milestone/milestone";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { saveMilestone } from "../../../../redux/actions/projects";
import IconEditSmall from "../../../../components/Icons/IconEditSmall";
import { FormattedMessage } from "react-intl";
type MilestoneProps = {
  classes: Object,
  theme: Object,
  saveMilestone: Function,
  setSaving: Function,
  data: Object,
  showSnackBar: Function,
  permission: Object,
};

function Milestones(props: MilestoneProps) {
  const { classes, theme, saveMilestone, setSaving, showSnackBar, permission, intl } = props;
  const { projectDetails, projectSettings } = props.data;

  const [milestoneTitle, setMilestoneTitle] = useState("");
  const [date, setDate] = useState(moment());
  const [editMilestone, setEditMilestone] = useState({});
  const [milestoneList, setMilestoneList] = useState([]);

  const handleChangeTitle = e => {
    /** function call when user enter the milestone title to add */
    setMilestoneTitle(e.target.value);
  };

  const dateSelect = selectedDate => {
    /** function calls wheun user select date after/before entering title */
    setDate(selectedDate);
  };

  const handleAddMilestone = () => {
    /** function calls when user click add icon for addding the milestone */
    const obj = {
      projectId: projectSettings.projectId,
      mileStoneDate: moment(date).toISOString(true),
      mileStoneName: milestoneTitle.trim(),
    };
    milestoneApiCRUD(obj);
    setMilestoneTitle("");
  };
  const onKeyDown=(e)=>{
    if (e.key === "Enter" && milestoneTitle !== "") {
      handleAddMilestone();
    }
  }

  const handleDeleteMilestone = obj => {
    /** function calls when user delete the specific milestone from list */
    let updatedObj = { ...obj, isDeleted: true };
    updateStateList(updatedObj);
    updatedObj.projectId = projectSettings.projectId;
    milestoneApiCRUD(updatedObj);
    showSnackBar("Milestone Deleted Successfully", "error");
  };

  const handleCheckMilestone = (e, milestone) => {
    let updatedObj = { ...milestone, isCompleted: !milestone.isCompleted };
    updateStateList(updatedObj);
    updatedObj.projectId = projectSettings.projectId;
    milestoneApiCRUD(updatedObj);
  };

  const handleUpdateBillable = (obj, val) => {
    /** function calls when user changed the billable state of the milestone to true/false */
    let updatedObj = { ...obj, isBillable: val };
    updateStateList(updatedObj);
    updatedObj.projectId = projectSettings.projectId;
    milestoneApiCRUD(updatedObj);
  };

  const handleUpdateDate = (obj, dateVal) => {
    /** function updates the date of the selected/desired milestone */
    let formatedDate = moment(dateVal).toISOString(true);
    let updatedObj = {
      ...obj,
      mileStoneDate: formatedDate,
    };
    updateStateList(updatedObj);
    updatedObj.projectId = projectSettings.projectId;
    milestoneApiCRUD(updatedObj);
  };

  const milestoneApiCRUD = obj => {
    /** generic function which receives the updated milestone object and send it to API for create/update/delete operations */
    setSaving(true);
    setDate(moment());
    saveMilestone(
      obj,
      succ => {
        setSaving(false);
      },
      fail => {
        setSaving(false);
        initMilestoneList(); /** if API fails then whatever milestones coming from props will set into state */
      },
      projectDetails
    );
  };

  const handleEditMilestone = (e, m) => {
    /** function calls when user click on the desired milestone name to be changed , which set selected object into state for further changing in object */
    e.stopPropagation();
    setEditMilestone(m);
  };

  const handleChangeMilestoneName = event => {
    /** function calls when user change the desired milestone name  */
    setEditMilestone({ ...editMilestone, mileStoneName: event.target.value });
  };

  const handleMilestoneNameBlur = () => {
    /** function call when user is done with updating the milestone name and wants to save it */

    let updatedObj = editMilestone;
    if (!validMilestoneTitle(updatedObj.mileStoneName)) {
      /** if changed milestone is valid or not */
      updateStateList(updatedObj);
      updatedObj.projectId = projectSettings.projectId;
      milestoneApiCRUD(updatedObj);
      setEditMilestone({});
    } else {
      setEditMilestone({});
    }
  };
  const handleEditKeyDown = (e)=>{
    if (e.key === "Enter") {
      handleMilestoneNameBlur();
    }
  }

  const initMilestoneList = () => {
    //initialization state with milestone in list
    const milestones = projectSettings.mileStones;
    setMilestoneList(milestones);
  };

  const updateStateList = obj => {
    /** updating the state's milestone list array if user have updated anything  */
    let updatedMilestone;
    if (!obj.isDeleted) {
      /** if isDeleted false  */
      updatedMilestone = milestoneList.map(m => {
        if (m.id == obj.id) return obj;
        else return m;
      });
    } else {
      /** if isDeleted true  */
      updatedMilestone = milestoneList.filter(m => m.id !== obj.id);
    }
    setMilestoneList(updatedMilestone);
  };

  useEffect(() => {
    initMilestoneList();
  }, [projectSettings.mileStones]);

  const getEditIcon = milestone => {
    return (
      <div className={classes.editIconCnt}>
        <CustomTooltip
          helptext="Edit Milestone Name"
          iconType="help"
          placement="top"
          style={{ color: theme.palette.common.white }}>
          <CustomIconButton
            onClick={event => {
              handleEditMilestone(event, milestone);
            }}
            btnType="transparent"
            variant="contained"
            style={{
              padding: "3px 3px 0px 0px",
              backgroundColor: "transparent",
            }}
            disabled={false}>
            <SvgIcon
              viewBox="0 0 12 11.957"
              // htmlColor={theme.palette.secondary.medDark}
              className={classes.editIcon}>
              <IconEditSmall />
            </SvgIcon>
          </CustomIconButton>
        </CustomTooltip>
      </div>
    );
  };

  return (
    <>
      <div className={classes.milestoneHeader}>
        <DefaultTextField
          label={
            <span className={classes.label}>
              <FormattedMessage
                id="project.dev.milestones.create-milestone.label"
                defaultMessage="Create Milestone"
              />
            </span>
          }
          fullWidth
          errorState={false}
          errorMessage={""}
          defaultProps={{
            id: "milestoneInput",
            onChange: handleChangeTitle,
            onKeyDown: onKeyDown,
            value: milestoneTitle,
            placeholder: intl.formatMessage({
              id: "project.dev.milestones.create-milestone.placeholder",
              defaultMessage: "Enter milestone title",
            }),
            style: { paddingRight: 160 },
            autoFocus: false,
            disabled: !permission.mileStoneTab.addProjectMilestone.cando,
            inputProps: { maxLength: 80 },
          }}
        />
        <div className={classes.milestoneHeaderAction}>
          {!validMilestoneTitle(milestoneTitle) && (
            <>
              <Hotkeys keyName="enter" onKeyDown={handleAddMilestone}>
                <CustomDatePicker 
                  date={date} 
                  onSelect={dateSelect} 
                  PopperProps={{ size: null }}
                  filterDate={moment()}
                  timeInput={false}
                  dateFormat="MMM DD, YYYY"
                />

                <div className={classes.addIconCnt}>
                  <IconButton
                    classes={{ root: classes.addIcon }}
                    disabled={false}
                    onClick={handleAddMilestone}>
                    <AddIcon classes={{ root: classes.addMilestoneBtnIcon }} />
                  </IconButton>
                </div>
              </Hotkeys>
            </>
          )}
        </div>
      </div>
      <div>
        {!isEmpty(milestoneList) /** milestone list render UI */ ? (
          <ul className={classes.milestoneList}>
            <li className={classes.header}>
              <div className={classes.columnOne}>
                <span className={classes.headerTitle}>
                  <FormattedMessage
                    id="project.dev.milestones.header.milestone"
                    defaultMessage="Milestones"
                  />
                </span>
              </div>
              <div className={classes.columnTwo}>
                <span className={classes.headerTitle} style={{ paddingLeft: 8 }}>
                  <FormattedMessage id="project.dev.milestones.header.date" defaultMessage="Date" />
                </span>
              </div>
              <div className={classes.columnThree}>
                <span className={classes.headerTitle}>
                  <FormattedMessage
                    id="project.dev.milestones.header.billable"
                    defaultMessage="Billable"
                  />
                </span>
              </div>
              <div className={classes.actionColumn}></div>
            </li>
            {milestoneList.map(m => {
              /** milestone list body rendering ui */
              return (
                <li className={classes.row} key={m.id}>
                  <div className={classes.columnOne}>
                    <div className="flex_center_start_row">
                      {/* {m.isCompleted ? (
                        <SvgIcon
                          viewBox="0 0 426.667 426.667"
                          htmlColor={theme.palette.status.completed}
                          classes={{
                            root: classes.checkedIcon,
                          }}
                        >
                          <CheckBoxIcon />
                        </SvgIcon>
                      ) : (
                        <RoundIcon className={classes.roundIcon} />
                      )} */}
                      <FormControlLabel
                        classes={{
                          root: classes.checklistCheckboxCnt,
                        }}
                        control={
                          <Checkbox
                            checked={m.isCompleted}
                            disabled={!permission.mileStoneTab.completeMilestone.cando}
                            style={{ padding: "0 5px 0 0" }}
                            onClick={e => {
                              handleCheckMilestone(e, m);
                            }}
                            disableRipple
                            value={""}
                            checkedIcon={
                              <SvgIcon
                                viewBox="0 0 426.667 426.667"
                                htmlColor={theme.palette.status.completed}
                                classes={{
                                  root: classes.checkedIcon,
                                }}>
                                <CheckBoxIcon />
                              </SvgIcon>
                            }
                            icon={
                              <>
                                <SvgIcon
                                  viewBox="0 0 426.667 426.667"
                                  htmlColor={theme.palette.background.items}
                                  classes={{
                                    root: classes.unCheckedIcon,
                                  }}>
                                  <CheckBoxIcon />
                                </SvgIcon>
                                <span className={classes.emptyCheckbox} />
                              </>
                            }
                            color="primary"
                          />
                        }
                      />
                      {!isEmpty(editMilestone) && editMilestone.id == m.id ? (
                        <Hotkeys keyName="enter" onKeyDown={handleMilestoneNameBlur}>
                          <DefaultTextField
                            label=""
                            error={false}
                            size="small"
                            formControlStyles={{ marginBottom: 0, width: 330 }}
                            defaultProps={{
                              type: "text",
                              id: "ResourceJobTitle",
                              placeholder: intl.formatMessage({
                                id: "project.dev.milestones.create-milestone.placeholder",
                                defaultMessage: "Enter milestone title",
                              }),
                              value: editMilestone.mileStoneName,
                              autoFocus: true,
                              onBlur: handleMilestoneNameBlur,
                              onKeyDown: handleEditKeyDown,
                              inputProps: { maxLength: 80 },
                              onChange: event => {
                                handleChangeMilestoneName(event);
                              },
                            }}
                          />
                        </Hotkeys>
                      ) : (
                        <div>
                          <div className={classes.milestoneTitleCnt}>
                            <Typography
                              variant="caption"
                              // onClick={() => {
                              //   handleEditMilestone(m);
                              // }}
                              className={classes.milestoneTitle}
                              title={m.mileStoneName}>
                              {m.mileStoneName}
                            </Typography>
                            {!m.isCompleted &&
                              permission.mileStoneTab.milestoneName.isAllowEdit &&
                              getEditIcon(m)}
                          </div>
                          {m.isCompleted && (
                            <Typography
                              variant="body2"
                              className={classes.milestoneSubTitle}
                              title={null}>
                              {`Completed on: ${moment(m.updatedDate).format("LL")}`}
                            </Typography>
                          )}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className={classes.columnTwo}>
                    <CustomDatePicker
                      date={m.mileStoneDate}
                      icon={false}
                      PopperProps={{ size: null }}
                      onSelect={date => {
                        handleUpdateDate(m, date);
                      }}
                      filterDate={moment()}
                      timeInput={false}
                      dateFormat="MMM DD, YYYY"
                      btnProps={{ className: classes.datePickerCustomStyle }}
                      disabled={!permission.mileStoneTab.setMilestoneDate.cando || m.isCompleted}
                    />
                  </div>
                  <div className={classes.columnThree}>
                    <DefaultCheckbox
                      checkboxStyles={{ padding: "5px 5px 4px 8px" }}
                      checked={m.isBillable}
                      unCheckedIconProps={{
                        style: { fontSize: "20px" },
                      }}
                      checkedIconProps={{
                        style: { fontSize: "20px" },
                      }}
                      onChange={() => {
                        handleUpdateBillable(m, !m.isBillable);
                      }}
                      fontSize={20}
                      color={"#0090ff"}
                      disabled={!permission.mileStoneTab.billingMode.cando || m.isCompleted}
                    />
                  </div>
                  <div className={classes.actionColumn}>
                    <CustomTooltip
                      helptext={
                        <FormattedMessage
                          id="team-settings.billing.bill-dialog.remove-button.label"
                          defaultMessage="Remove"
                        />
                      }
                      iconType="help"
                      placement="top"
                      style={{ color: theme.palette.common.white }}>
                      <CustomIconButton
                        onClick={() => {
                          handleDeleteMilestone(m);
                        }}
                        btnType="transparent"
                        variant="contained"
                        style={{
                          width: 20,
                          height: 20,
                          borderRadius: 4,
                          marginLeft: 14,
                          color:
                            permission.mileStoneTab.deleteMilestone.cando && !m.isCompleted
                              ? "#de133e"
                              : "#BFBFBF",
                        }}
                        disabled={!permission.mileStoneTab.deleteMilestone.cando || m.isCompleted}>
                        <SvgIcon viewBox="0 0 24 24">
                          <RemoveIcon2 />
                        </SvgIcon>
                      </CustomIconButton>
                    </CustomTooltip>
                  </div>
                </li>
              );
            })}
          </ul>
        ) : (
          /** if milestone list is empty then show empty screen */
          <div className={classes.emptyStateCnt}>
            <EmptyState
              screenType="milestone"
              heading={
                <FormattedMessage
                  id="project.dev.milestones.emptystate.label"
                  defaultMessage="Milestones"
                />
              }
              message={
                <FormattedMessage
                  id="project.dev.milestones.emptystate.placeholder"
                  defaultMessage="Add milestones and set their dates for monitoring the overall progress/achievements of the project."
                />
              }
              button={false}
            />{" "}
          </div>
        )}
      </div>
    </>
  );
}

Milestones.defaultProps = {
  /** default props , calls when props are not coming from parent class undefined */
  classes: {},
  theme: {},
  saveMilestone: () => {},
  setSaving: () => {},
  data: {},
  showSnackBar: () => {},
  permission: {},
};

const mapStateToProps = state => {
  return {};
};

export default compose(
  withStyles(styles, {
    withTheme: true,
  }),
  connect(mapStateToProps, { saveMilestone })
)(Milestones);
