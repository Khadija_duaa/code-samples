const milestoneStyles = (theme) => ({
  milestoneHeader: {
    display: "flex",
    alignItems: "center",
    position: "relative",
  },
  milestoneHeaderAction: {
    position: "absolute",
    right: 10,
    display: "flex",
    alignItems: "center",
    display: "flex",
  },

  dateBtn: {
    background: "#F6F6F6",
    border: `1px solid #F6F6F6`,
    padding: "4px 10px",
    borderRadius: 18,
    "&:hover": {
      border: `1px solid #0090ff`,
      background: "#fff",
    },
  },
  listDateBtn: {
    background: "transparent",
    padding: "4px 10px",
    borderRadius: 6,
    width: "fit-content",
    "&:hover": {
      background: "#F6F6F6",
    },
  },
  dateIcon: {
    fontSize: "21px !important",
    marginRight: 2,
  },
  date: {
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 600,
  },
  label: {
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
    color: "#646464",
  },
  placeHolder: {
    color: "#BFBFBF",
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
  },
  addIconCnt: {
    margin: " 0px 0px 0 6px",
  },
  addIcon: {
    padding: 3,
    border: `1px solid #0090ff`,
    "&:hover": {
      backgroundColor: "#0090ff",
      "& $addMilestoneBtnIcon": {
        color: "#fff",
      },
    },
  },
  addMilestoneBtnIcon: {
    fontSize: "18px !important",
    color: "#0090ff",
  },
  StaticDatePickerPopper: {
    zIndex: 6,
    left: "-120px",
    top: 32,
  },
  staticPopperInList: {
    zIndex: 6,
    left: 358,
    top: 268,
  },
  rangeDatePickerInner: {
    border: "1px solid rgba(221,221,221,1)",
    background: theme.palette.common.white,
  },
  milestoneList: {
    listStyleType: "none",
    padding: 0,
    margin: 0,

    "& li": {
      display: "flex",
      alignItems: "center",
      borderBottom: `1px solid ${theme.palette.border.grayLighter}`,
      padding: "0 4px",
      "&:hover $editIconCnt": {
        display: "inline !important",
      },
    },
  },
  header: {
    borderBottom: "none !important",
  },
  actionColumn: {
    width: 30,
  },
  columnOne: {
    flex: 1,
  },
  columnTwo: {
    width: 125,
  },
  columnThree: {
    width: 42,
  },
  headerTitle: {
    color: theme.palette.text.grayDarker,
    fontSize: "12px !important",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
  },
  emptyStateCnt: {
    marginTop: 84,
  },
  row: {
    minHeight: 56,
  },
  unCheckedIcon: {
    fontSize: "20px !important",
    display: "none",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "100%",
  },
  checkedIcon: {
    fontSize: "20px !important",
    marginRight: 5,
  },
  roundIcon: {
    fontSize: "23px !important",
    color: "#BFBFBF",
    marginRight: 5,
  },
  milestoneTitle: {
    fontSize: "13px !important",
    fontWeight: 600,
    fontFamily: theme.typography.fontFamilyLato,
    color: "#202020",

    overflow: "hidden",
    whiteSpace: "nowrap",
    lineHeight: "normal",
    textOverflow: "ellipsis",
    maxWidth: 300,
  },
  milestoneSubTitle: {
    fontSize: "11px !important",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
    color: "#7E7E7E",
    cursor: "pointer",
  },
  datePickerCustomStyle: {
    background: "transparent",
    border: "none !important",
    borderRadius: "4px !important",
    minWidth: "105px !important",
    "&:hover": {
      background: theme.palette.background.items,
    },
  },
  checklistCheckboxCnt: {
    margin: "2px 0 0 0",
  },
  checkedIcon: {
    fontSize: "18px !important",
  },
  emptyCheckbox: {
    width: 18,
    height: 18,
    background: theme.palette.background.paper,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "100%",
  },
  unCheckedIcon: {
    fontSize: "18px !important",
    display: "none",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "100%",
  },
  editIcon: {
    fontSize: "11px !important",
    marginTop: -4,
    marginLeft: 5,
  },
  editIconCnt: {
    display: "none",
  },
  milestoneTitleCnt: {
    display: "flex",
  },
});
export default milestoneStyles;
