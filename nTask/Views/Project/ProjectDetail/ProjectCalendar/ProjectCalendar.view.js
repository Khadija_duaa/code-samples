import React, { useEffect, useState } from "react";
import "react-big-calendar/lib/css/react-big-calendar.css";
import { withStyles } from "@material-ui/core/styles";
import "react-big-calendar/lib/addons/dragAndDrop/styles.scss";
import { styles } from "../../../../components/CalendarModule/Calendar.style";
import Typography from "@material-ui/core/Typography";
import { compose } from "redux";
import { withSnackbar } from "notistack";
import CalendarForm from "../../../../components/CalendarModule/CalendarForm.view";
import CustomSelect from "../../../../components/Dropdown/CustomSelect/CustomSelect.view";
import { parseDate, DAYSOPTIONS } from "../../../../components/CalendarModule/constant";
import { format } from "date-fns";
import { useDispatch, useSelector } from 'react-redux';
import { UpdateProjectDetailDialog } from '../../../../redux/actions/projects';
import UpdateCalendarModal from "../../../../components/CalendarModule/component/UpdateCalendarModal";
import { getCalenderList, updateProjectCalender } from "../../../../redux/actions/calendar";

const ProjectCalendar = (props) => {
  const { theme, classes, value, enqueueSnackbar, projectId, selectedProject } = props;

  const dispatch = useDispatch()
  const projectDetailsDialog = useSelector(store => store.projectDetailsDialog)
  const [calendar, setCalendar] = useState(value);
  const [data, setData] = useState([]);
  const [selected, setSelected] = useState({ data: undefined, type: undefined, loading: false });

  const showSnackBar = (message, type) => {
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{message}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };

  useEffect(() => {
    getCalenderList((resp) =>
      setData(resp.entity.map((item) => ({ ...item, label: item.title, value: item.calenderId })))
    );
  }, []);

  const onSubmit = () => {
    setSelected((prev) => ({ ...prev, loading: true }));
    updateProjectCalender({ calenderId: selected.data.calenderId, projectId: projectId }, () => {
      setCalendar(selected.data);
      setSelected({ data: undefined, type: undefined, loading: false });
      showSnackBar("Project Schedule Updated", "info");
      dispatch(UpdateProjectDetailDialog({ ...projectDetailsDialog, projectSettings: { ...projectDetailsDialog.projectSettings, calender: selected.data } }))
    });
  };

  if (!calendar)
    return (
      <div className={classes.calenderLoader}>
        <div className="loader" />
      </div>
    );

  return (
    <div className={classes.CalendarMain} style={{ paddingTop: '0px' }}>
      <CustomSelect
        showCheck={false}
        bgapply={false}
        value={calendar?.calenderId}
        style={{
          width: "100%",
          margin: "10px 0px 20px 0px",
          justifyContent: "space-between",
          backgroundColor: "white",
          display: "flex",
          border: "1px solid #EAEAEA",
        }}
        onChange={(value) => setSelected({ data: value, type: "change", loading: false })}
        height="390px"
        // height={16 + 79 * data.length}
        width="100%"
        options={data ?? []}
        render={(calender, selected, index) => (
          <div
            key={index}
            style={{ display: "flex", padding: ".5rem ", borderBottom: "1px solid #f1f1f1" }}
            className="group">
            <div style={{ flex: 1 }}>
              <div
                style={{ color: selected ? "#0090FF" : "#000000" }}
                className={classes.calenderNameText}>
                {calender.title}
              </div>
              <div style={{ display: "flex", alignItems: "center" }}>

                <div style={{ display: "flex", alignItems: "center" }}>
                  {DAYSOPTIONS.filter((item) =>
                    calender.workingdays.some((day) => day.id === item.value.id)
                  ).map((item, index) => (
                    <Typography variant="body2" key={index} style={{ marginRight: ".5rem" }}>
                      {item.label}
                    </Typography>
                  ))}
                </div>
                {!!calender.workingHours.length ? (
                  <>
                    <div className={classes.horizontalLine}>&nbsp;</div>
                    <div style={{ display: "flex", alignItems: "center" }}>
                      {calender.workingHours.slice(0, 1).map((slot) => (
                        <div className={classes.chip}>
                          {format(parseDate(slot.timeFrom), "hh:mm a")}-
                          {format(parseDate(slot.timeTo), "hh:mm a")}
                        </div>
                      ))}
                      {calender.workingHours.length > 1 && <div className={classes.chip}>...</div>}
                    </div>
                  </>
                ) : (
                  !!calender.dailyCapacity && (
                    <>
                      <div className={classes.horizontalLine}>&nbsp;</div>
                      <div className={classes.chip}>{calender.dailyCapacity} hours</div>
                    </>
                  )
                )}

                {!!calender?.exceptions?.length && (
                  <>
                    <div className={classes.horizontalLine}>&nbsp;</div>
                    <div style={{ display: "flex", alignItems: "center" }} className={classes.chip}>
                      {calender?.exceptions?.length} exceptions
                    </div>
                  </>
                )}
              </div>
            </div>
          </div>
        )}
      />

      <CalendarForm
        defaultValue={calendar}
        readOnly={true}
        disabled
      />

      <UpdateCalendarModal
        open={selected.type === "change"}
        loading={selected.loading}
        onClose={() => setSelected({ data: undefined, type: undefined, loading: false })}
        onSubmit={onSubmit}
      />
    </div>
  );
};

export default compose(withSnackbar, withStyles(styles, { withTheme: true }))(ProjectCalendar);
