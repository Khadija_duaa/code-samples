import React, { Component } from "react";
import { Editor } from "react-draft-wysiwyg";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { checkDescriptionStatus } from "../../../../redux/actions/projects";
import "../../../../assets/css/richTextEditor.css";
import withStyles from "@material-ui/core/styles/withStyles";
import Style from "./style";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import { EditorState, convertToRaw, ContentState } from "draft-js";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";
import InputLabel from "@material-ui/core/InputLabel";
import ReactHtmlParser from 'react-html-parser';
import { taskDetailsHelpText } from "../../../../components/Tooltip/helptext";
import CustomTooltip from "../../../../components/Tooltip/Tooltip";
import Typography from "@material-ui/core/Typography";
import LockIcon from "@material-ui/icons/Lock";
import DescriptionEditor from "../../../../components/TextEditor/CustomTextEditor/DescriptionEditor/DescriptionEditor";
import {FormattedMessage,injectIntl} from "react-intl";
const MAX_LENGTH = 8000;
let timer;
class ProjectDescription extends Component {
  constructor(props) {
    super(props);
    this.state = {
      EditMode: false,
      CharlimitExceed: false,
      someOneTyping: false,
      count: 0,
      memberTyping: {},
      description: "",
    };
  }
  componentDidMount() {
    const { projectDescription } = this.props;
    if (projectDescription) {
      this.setState({
        description: projectDescription,
      });
    }
  }
  componentDidUpdate(prevProps, prevState) {
    const { projectDescription } = this.props;
    if (
      JSON.stringify(projectDescription) !==
      JSON.stringify(prevProps.projectDescription)
    ) {
      this.setState({
        description: projectDescription,
      });
    }
  }

  uploadImageCallBack = (file) => {
  };

  handleEditClick = () => {
    const { projectId = null, checkDescriptionStatus } = this.props;
    if (projectId) {
      checkDescriptionStatus(
        projectId,
        (succ) => {
          if (!succ.entity.isDescriptionLocked) {
            this.setState({
              EditMode: true,
              CharlimitExceed: false,
              someOneTyping: false,
            });
            timer = setInterval(() => {
              this.saveDescription();
            }, 10000);
          } else {
            this.getMemberWhoIsTyping(succ.entity);
          }
        },
        (fail) => {}
      );
    } else {
      this.setState({ EditMode: true, CharlimitExceed: false });
    }
  };

  getMemberWhoIsTyping = (data) => {
    let allMembers =
      this.props.profileState &&
      this.props.profileState.data &&
      this.props.profileState.data.member
        ? this.props.profileState.data.member.allMembers
        : [];
    let member =
      allMembers.find((a) => a.userId == data.descriptionLockedBy) || {};
    this.setState({ memberTyping: member, someOneTyping: true });
  };


  handleClickAway = () => {
    clearInterval(timer);
    this.setState({
      EditMode: false,
      CharlimitExceed: false,
    });
    this.saveDescription();
  };

  saveDescription = () => {
    this.props.setProjectDescription(this.state.description);
  };

  handleClickAwayOuterCnt = () => {
    this.setState({
      EditMode: false,
      CharlimitExceed: false,
      someOneTyping: false,
    });
  };

  handleChangeDescription = (data) => {
    this.setState({ description: data.html });
  };

  render() {
    const {
      classes,
      permission,
      marginTop = 0,
      theme,
      projectDescription,
    } = this.props;
    const {
      EditMode,
      editorState,
      CharlimitExceed,
      someOneTyping,
      description,
    } = this.state;
    // let style =  permission ? {borderTop: "0.2px solid #eaeaea"} :{pointerEvents : "none", borderTop: "0.2px solid #eaeaea" };
    return (
      <div className={classes.descriptionCnt}>
        <InputLabel classes={{ root: classes.projectDropdownsLabel }}>
          <FormattedMessage id="project.dev.details.billable.project-description.label" defaultMessage="Project Description"/>
        </InputLabel>
        <div>
          <DescriptionEditor
            onChange={this.handleChangeDescription}
            handleClickAway={this.handleClickAway}
            defaultValue={description}
            placeholder={this.props.intl.formatMessage({id:"project.dev.details.billable.project-description.placeholder",defaultMessage:"Enter project description(optional)"})}
            disableEdit={!permission}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profileState: state.profile,
  };
};

export default compose(
  withRouter,
  injectIntl,
  withStyles(Style, { withTheme: true }),
  connect(mapStateToProps, { checkDescriptionStatus })
)(ProjectDescription);
