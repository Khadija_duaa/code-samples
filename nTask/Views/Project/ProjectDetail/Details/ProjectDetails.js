import React, { useState, useEffect, useRef } from "react";
import { withSnackbar } from "notistack";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect, useDispatch } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import ToggleButton from "@material-ui/lab/ToggleButton";
import { FormattedMessage } from "react-intl";
import styles from "./style";

import AmountInput from "../../../../components/Form/AmountInput/AmountInput";
import RemoveIcon from "@material-ui/icons/Remove";
import AddIcon from "@material-ui/icons/Add";
import ProjectDescription from "./ProjectDescription";
import CustomSelectDropdown from "../../../../components/Dropdown/CustomSelectDropdown/Dropdown";
import {
  currencyData,
  billingMethodData,
  feeMethodData,
} from "../../../../helper/projectDropdownData";
import { teamCanView } from "../../../../components/PlanPermission/PlanPermission";
import CustomFieldView from "../../../CustomFieldSettings/CustomFields.view";
import SectionsView from "../../../../components/Sections/sections.view";
import {
  editCustomField,
  updateCustomFieldData,
  hideCustomField,
} from "../../../../redux/actions/customFields";
import { updateCustomFieldDialogState } from "../../../../redux/actions/allDialogs";
import {
  updateProjectCf,
  dispatchProject,
  projectDetails,
  UpdateProjectDetailListing,
} from "../../../../redux/actions/projects";
import isArray from "lodash/isArray";
import cloneDeep from "lodash/cloneDeep";
import { CanAccessFeature } from "../../../../components/AccessFeature/AccessFeature.cmp";

function Details(props) {
  const {
    theme,
    classes,
    data,
    setProjectDescription,
    projectDescription = "",
    handlePlanChangePayment,
    subscription,
    constantsState,
    billingMethod,
    setBillingMethod,
    selectedFee,
    setSelectedFee,
    amount,
    setAmount,
    selectedCurrency,
    setSelectedCurrency,
    permission,
    intl,
    budget,
    setBudget,
    editCustomField,
    updateCustomFieldData,
    hideCustomField,
    profileState,
    dispatchProject,
    updateCustomFieldDialogState,
    projectDetails,
  } = props;

  const [sFSecOpen, setSFSecOpen] = useState(true);
  const projectDetailsRef = useRef(props.data.projectDetails)
  const dispatch = useDispatch()
  const handleShowSystemField = section => {
    setSFSecOpen(!sFSecOpen);
  };
  const currencyOptionData = () => {
    return currencyData(constantsState);
  };
  const feeData = () => {
    return feeMethodData(billingMethod);
  };
  useEffect(() => {
    projectDetailsRef.current = props.data.projectDetails;
  }, [props.data]);
  const handleSelectCurrencyChange = item => {
    /** function call when user select the desired currency */
    setSelectedCurrency(item);
    props.updateProjectSettings("currency", item.value, null, () => {
      UpdateProjectDetailListing(dispatch, { currency: item.value }, data.projectDetails.projectId)
    });
  };
  const handleSelectBillingMethod = item => {
    /** function call when user select the billing method */
    setBillingMethod(item);
    props.updateProjectSettings("projectBillingMethod", item.value, item, () => {
      UpdateProjectDetailListing(dispatch, { billingMethod: item.value }, data.projectDetails.projectId)
    });
  };
  const handleFeeSelect = item => {
    /** function call when user select the fee type */
    setSelectedFee(item);
    props.updateProjectSettings("feeType", item.value, null, () => { });
  };

  const handleAmountInput = amount => {
    /** function call when user type budget/per task/per hour rate in same fee scenario */
    if (billingMethod.value == 0) {
      props.updateProjectSettings("budget", amount, null, () => {
        UpdateProjectDetailListing(dispatch, { budget: amount }, data.projectDetails.projectId)
      });
    } else {
      props.updateProjectSettings("hourlyRate", amount, null, () => { });
    }
  };
  const handleBudgetInput = amount => {
    props.updateProjectSettings("projectBudget", amount, null, () => {
      UpdateProjectDetailListing(dispatch, { budget: amount }, data.projectDetails.projectId)
    });
  };
  const onInputAmountChange = amount => {
    setAmount(amount);
  };
  const onInputBudgetChange = amount => {
    setBudget(amount);
  };

  const getMessage = (m = {}) => {
    const { theme, classes } = props;
    switch (m.value) {
      case 1:
        return (
          <Typography variant="h6" className={classes.note}>
            <FormattedMessage
              id="project.dev.details.billable.set-fee.label"
              values={{
                t1: (
                  <span className={classes.link} onClick={() => props.changeTab({}, 2)}>
                    <FormattedMessage
                      id="project.dev.details.billable.tab.task-tab"
                      defaultMessage="Tasks tab."
                    />
                  </span>
                ),
              }}
              defaultMessage={`Set fee of each task in${" "}${(
                <span className={classes.link} onClick={() => props.changeTab({}, 2)}>
                  Tasks tab.
                </span>
              )}`}
            />
          </Typography>
        );
        break;
      case 2:
        return (
          <Typography variant="h6" className={classes.note}>
            <FormattedMessage
              id="project.dev.details.billable.set-hourly.task-label"
              values={{
                t1: (
                  <span className={classes.link} onClick={() => props.changeTab({}, 2)}>
                    <FormattedMessage
                      id="project.dev.details.billable.tab.task-tab"
                      defaultMessage="Tasks tab."
                    />
                  </span>
                ),
              }}
              defaultMessage={`Set hourly rate of each task in${" "}${(
                <span className={classes.link} onClick={() => props.changeTab({}, 2)}>
                  Tasks tab.
                </span>
              )}`}
            />
          </Typography>
        );
        break;
      case 3:
        return (
          <Typography variant="h6" className={classes.note}>
            <FormattedMessage
              id="project.dev.details.billable.set-hourly.resource-label"
              values={{
                t1: (
                  <span className={classes.link} onClick={() => props.changeTab({}, 1)}>
                    <FormattedMessage
                      id="project.dev.details.billable.tab.resource-tab"
                      defaultMessage="Resources tab."
                    />
                  </span>
                ),
              }}
              defaultMessage={`Set hourly rate of each resource in${" "}${(
                <span className={classes.link} onClick={() => props.changeTab({}, 1)}>
                  Resources tab.
                </span>
              )}`}
            />
          </Typography>
        );
        break;

      default:
        return null;
        break;
    }
  };

  const editCustomFieldCallback = (obj, oldFieldObject) => {
    const { fieldId, fieldType } = obj;
    if (fieldType === "checklist") return;
    updateCustomFieldData(obj);
  };
  const handleEditField = field => {
    updateCustomFieldDialogState({ moduleViewType: "editCopyModal", data: field, mode: "edit" });
  };
  const handleCopyField = field => {
    updateCustomFieldDialogState({ moduleViewType: "editCopyModal", data: field, mode: "copy" });
  };
  const handleClickHideOption = (event, field) => {
    let object = { ...field };
    object.settings.isHideCompletedTodos = !object.settings.isHideCompletedTodos;
    delete object.team;
    delete object.workspaces;
    delete object.level;
    editCustomField(
      object,
      object.fieldId,
      res => { },
      failure => { }
    );
  };
  const handleSelectHideOption = (value, selectedFieldObj) => {
    let object = { ...selectedFieldObj };

    if (value == 1) {
      object.hideOption = "workspace";
      object.settings.isHidden = false;

      object.settings.hiddenInWorkspaces = [
        profileState.loggedInTeam,
        ...object.settings.hiddenInWorkspaces,
      ];
      object.settings.hiddenInGroup =
        object.settings.hiddenInGroup.indexOf("project") < 0
          ? ["project", ...object.settings.hiddenInGroup]
          : object.settings.hiddenInGroup;
    }
    if (value == 2) {
      /** if user select the team level */
      object.hideOption = "allWorkspaces";
      object.settings.isHidden = true;
      object.settings.hiddenInWorkspaces = [];
      object.settings.hiddenInGroup =
        object.settings.hiddenInGroup.indexOf("project") < 0
          ? ["project", ...object.settings.hiddenInGroup]
          : object.settings.hiddenInGroup;
    }
    delete object.team;
    delete object.workspaces;
    hideCustomField(
      object,
      res => { },
      failure => { }
    );
  };
  const customFieldChange = (
    option,
    obj = {},
    settings = {},
    success = () => { },
    failure = () => { }
  ) => {
    const projectCopy = cloneDeep(projectDetailsRef.current);

    if (obj.fieldType == "matrix") {
      const selectedField =
        projectCopy.customFieldData &&
        projectCopy.customFieldData.find(x => x.fieldId == obj.fieldId);
      const isExist =
        selectedField &&
        selectedField.fieldData.data.findIndex(f => f.cellName == option.cellName) > -1;
      if (isExist === true) return;
    }
    const { fieldId } = obj;
    const newObj = {
      groupType: "project",
      groupId: projectCopy.projectId,
      fieldType: obj.fieldType,
      fieldId,
      fieldData: { data: option },
    };
    if (obj.fieldType === "dropdown") {
      if (isArray(newObj.fieldData.data))
        newObj.fieldData.data = newObj.fieldData.data.map(opt => {
          return { id: opt.id };
        });
      else newObj.fieldData.data = { id: newObj.fieldData.data.id };
    }
    if (obj.fieldType === "matrix")
      newObj.fieldData.data = newObj.fieldData.data.map(opt => {
        return { cellName: opt.cellName };
      });

    props.updateProjectCf(
      newObj,
      res => {
        success();
        const resObj = res.data.entity[0];
        let customFieldsArr = [];
        // Updating Global state
        const isExist = projectCopy.customFieldData
          ? projectCopy.customFieldData.findIndex(c => c.fieldId === resObj.fieldId) > -1
          : false; /** if new risk created, its customFields are null so in null case scenario , compile the normal false flow, add custom field in risk object and save it */
        if (isExist) {
          customFieldsArr = projectCopy.customFieldData.map(c => {
            return c.fieldId === resObj.fieldId ? resObj : c;
          });
        } else {
          customFieldsArr = projectCopy.customFieldData
            ? [...projectCopy.customFieldData, resObj]
            : [
              resObj,
            ]; /** add custom field in risk object and save it, if newly created risk because its custom field is already null */
        }
        let newProjectObj = { ...projectCopy, customFieldData: customFieldsArr };
        projectDetails({
          projectDetails: newProjectObj,
        });
        dispatchProject(newProjectObj);
      },
      () => {
        failure();
      }
    );
  };

  return (
    <>
      <div
        className={classes.systemFieldsHeading}
        style={{ background: "#89A3B2" }}
        onClick={() => {
          handleShowSystemField();
        }}>
        <Typography  className={classes.systemFieldTitle}>
          System Fields
        </Typography>
        <div>
          {sFSecOpen ? (
            <RemoveIcon
              className={classes.systemFieldRemoveIcon}
              onClick={() => {
                handleShowSystemField();
              }}
            />
          ) : (
            <AddIcon
              className={classes.systemFieldAddIcon}
              onClick={() => {
                handleShowSystemField();
              }}
            />
          )}
        </div>
      </div>
      {sFSecOpen && (
        <>
          <CanAccessFeature group='project' feature='billingType'>
          <div className={classes.toggleBtnCnt}>
            <ToggleButtonGroup
              size="small"
              exclusive
              onChange={handlePlanChangePayment}
              value={subscription}
              classes={{ root: classes.toggleBtnGroup }}>
              <ToggleButton
                key={2}
                value="1"
                classes={{
                  root: classes.toggleButton,
                  selected: classes.toggleButtonSelected,
                }}
                disabled={!permission.detailsTabs.billingMode.cando}>
                <div className={classes.toggleButtonMainDiv}>
                  <span className={classes.planTypeLbl}>
                    <FormattedMessage
                      id="project.dev.details.billable.label"
                      defaultMessage="Billable"
                    />
                  </span>
                </div>
              </ToggleButton>
              <ToggleButton
                key={1}
                value="0"
                classes={{
                  root: classes.toggleButton,
                  selected: classes.toggleButtonSelected,
                }}
                disabled={!permission.detailsTabs.billingMode.cando}>
                <div className={classes.toggleButtonMainDiv}>
                  <span className={classes.planTypeLbl}>
                    <FormattedMessage
                      id="project.dev.details.non-billable.label"
                      defaultMessage="Non-Billable"
                    />
                  </span>
                </div>
              </ToggleButton>
            </ToggleButtonGroup>

            {subscription == "1" && (
              <CustomSelectDropdown
                label={
                  <FormattedMessage
                    id="project.dev.details.billable.currency.label"
                    defaultMessage="Currency:"
                  />
                }
                options={currencyOptionData}
                option={selectedCurrency}
                onSelect={handleSelectCurrencyChange}
                size="medium"
                scrollHeight={320}
                disabled={!permission.detailsTabs.currency.cando}
              />
            )}
          </div>
          {subscription == "1" && (
            <div className={classes.billingCnt}>
              <Grid container>
                <Grid item xs={6}>
                  <CustomSelectDropdown
                    label={
                      <FormattedMessage
                        id="project.dev.details.billable.billing-method.label"
                        defaultMessage="Billing Method:"
                      />
                    }
                    options={billingMethodData}
                    option={billingMethod}
                    onSelect={handleSelectBillingMethod}
                    height="140px"
                    scrollHeight={180}
                    disabled={!permission.detailsTabs.billingMethode.cando}
                  />
                </Grid>
                {billingMethod.value != 0 && (
                  <Grid item xs={6}>
                    <CustomSelectDropdown
                      label={
                        billingMethod.value == 2 || billingMethod.value == 3 ? (
                          <FormattedMessage
                            id="project.dev.details.billable.rate.label"
                            defaultMessage="Rate:"
                          />
                        ) : (
                          <FormattedMessage
                            id="project.dev.details.billable.fee.label"
                            defaultMessage="Fee:"
                          />
                        )
                      }
                      options={feeData}
                      option={selectedFee}
                      onSelect={handleFeeSelect}
                      size={
                        billingMethod.value == 2
                          ? "xmedium"
                          : billingMethod.value == 3
                            ? "large"
                            : "medium"
                      }
                      scrollHeight={180}
                      disabled={!permission.detailsTabs.billingMethode.cando}
                    />
                  </Grid>
                )}

                {selectedFee.value != 0 ? (
                  <>
                    <Grid item xs={6}>
                      <AmountInput
                        style={{ marginLeft: 7 }}
                        label={intl.formatMessage({
                          id: "project.dev.details.billable.project-budget.label",
                          defaultMessage: "Project Budget",
                        })}
                        placeholder={intl.formatMessage({
                          id: "project.dev.details.billable.project-budget.placeholder",
                          defaultMessage: "Add Budget",
                        })}
                        onInputChange={onInputBudgetChange}
                        currency={selectedCurrency.value}
                        savedAmount={budget}
                        onBlur={handleBudgetInput}
                        disabled={!permission.detailsTabs.billingMethode.cando}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      {getMessage(billingMethod)}
                    </Grid>
                  </>
                ) : (
                  <>
                    {billingMethod.value != 0 && (
                      <Grid item xs={6}>
                        <AmountInput
                          style={{ marginLeft: 7 }}
                          label={intl.formatMessage({
                            id: "project.dev.details.billable.project-budget.label",
                            defaultMessage: "Project Budget",
                          })}
                          placeholder={intl.formatMessage({
                            id: "project.dev.details.billable.project-budget.placeholder",
                            defaultMessage: "Add Budget",
                          })}
                          onInputChange={onInputBudgetChange}
                          currency={selectedCurrency.value}
                          savedAmount={budget}
                          onBlur={handleBudgetInput}
                          disabled={!permission.detailsTabs.billingMethode.cando}
                        />
                      </Grid>
                    )}
                    <Grid item xs={6}>
                      <AmountInput
                        style={{ marginLeft: 7 }}
                        label={
                          billingMethod.value == 0 ? (
                            <FormattedMessage
                              id="project.dev.details.billable.total-fee.label"
                              defaultMessage="Total Project Fee:"
                            />
                          ) : billingMethod.value == 1 ? (
                            <FormattedMessage
                              id="project.dev.details.billable.per-task.label"
                              defaultMessage="Per Task:"
                            />
                          ) : (
                            <FormattedMessage
                              id="project.dev.details.billable.per-hour.label"
                              defaultMessage="Per Hour:"
                            />
                          )
                        }
                        placeholder={
                          billingMethod.value == 0 ? (
                            <FormattedMessage
                              id="project.dev.details.billable.total-fee.placeholder"
                              defaultMessage="Add fee"
                            />
                          ) : billingMethod.value == 1 ? (
                            <FormattedMessage
                              id="project.dev.details.billable.per-task.placeholder"
                              defaultMessage="Add task fee"
                            />
                          ) : (
                            <FormattedMessage
                              id="project.dev.details.billable.per-hour.placeholder"
                              defaultMessage="Add rate"
                            />
                          )
                        }
                        onInputChange={onInputAmountChange}
                        currency={selectedCurrency.value}
                        savedAmount={amount}
                        onBlur={handleAmountInput}
                        disabled={!permission.detailsTabs.billingMethode.cando}
                      />
                    </Grid>
                  </>
                )}
              </Grid>
            </div>
          )}
           </CanAccessFeature>
          <CanAccessFeature group='project' feature='description'>
          <ProjectDescription
            marginTop={25}
            permission={permission.projectDescription.cando}
            setProjectDescription={setProjectDescription}
            projectDescription={projectDescription}
            projectId={data.projectSettings.projectId}
          />
          </CanAccessFeature>

        </>
      )}
      <div>
        <SectionsView
          type={"project"}
          disabled={data.projectDetails.isDeleted}
          customFieldData={data.projectDetails.customFieldData}
          permission={{
            ...permission.customsFields,
            isAllowUpdate: permission.isUpdateField.cando,
            isAllowHide: permission.isHideField.cando,
            isUseField: permission.isUseField.cando,
          }}
          customFieldChange={customFieldChange}
          handleEditField={handleEditField}
          handleCopyField={handleCopyField}
          groupId={data.projectDetails.projectId}
          handleClickHideOption={handleClickHideOption}
          handleSelectHideOption={handleSelectHideOption}
        />
      </div>

      {teamCanView("customFieldAccess") && (
        <div>
          <CustomFieldView
            feature="project"
            // onFieldAdd={this.addCustomFieldCallback}
            onFieldEdit={editCustomFieldCallback}
            onFieldDelete={() => { }}
            permission={{
              ...permission.customsFields,
              isAllowUpdate: permission.isUpdateField.cando,
              isAllowHide: permission.isHideField.cando,
              isUseField: permission.isUseField.cando,
            }}
            disableAssessment={true}
            disabled={data.projectDetails.isDeleted}
          />
        </div>
      )}
    </>
  );
}
const mapStateToProps = state => {
  return {
    constantsState: state.constants,
    profileState: state.profile.data || {},
    data: state.projectDetailsDialog,
  };
};

export default compose(
  withRouter,
  withSnackbar,
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, {
    editCustomField,
    updateCustomFieldData,
    hideCustomField,
    updateProjectCf,
    dispatchProject,
    updateCustomFieldDialogState,
    projectDetails,
  })
)(Details);
