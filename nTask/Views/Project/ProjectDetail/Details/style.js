const detailTabStyle = (theme) => ({
  toggleBtnGroup: {
    // textAlign: "center",
    borderRadius: 4,
    background: theme.palette.common.white,
    border: "none",
    boxShadow: "none",
    "& $toggleButtonSelected": {
      //   color: theme.palette.common.white,
      backgroundColor: theme.palette.border.blue,
      // border: `1px solid ${theme.palette.border.blue}`,
      //   '& $planPriceStyle':{
      //     color: theme.palette.text.azure,
      //   },
      "& $planTypeLbl": {
        color: theme.palette.common.white,
      },
      "&:after": {
        background: theme.palette.common.white,
      },
      "&:hover": {
        background: theme.palette.border.blue,
        // color: theme.palette.common.black
      },
    },
  },
  toggleButton: {
    border: `1px solid #F6F6F6`,
    borderRadius: 4,
    height: 32,
    fontSize: "12px !important",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    textTransform: "capitalize",
    color: theme.palette.common.white,
    fontWeight: theme.typography.fontWeightLight,
    background: "#F6F6F6",
    padding: "8px 12px",
  },
  toggleButtonSelected: {
    boxShadow: "none"
  },
  toggleButtonMainDiv: {
    position: "relative",
    alignItems: "center",
    display: "flex",
  },
  planTypeLbl: {
    display: "flex",
    fontSize: "13px !important",
    // fontWeight: 700,
    color: theme.palette.text.primary,
    whiteSpace: "nowrap",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
  },
  toggleBtnCnt: {
    display: "flex",
    justifyContent: "space-between",
    marginBottom: 10,
    marginTop: 10,
  },
  billingCnt: {
    border: `1px solid ${theme.palette.border.grayLighter}`,
    borderRadius: 4,
    padding: "5px 7px",
    // marginBottom: 28,
  },
  note: {
    fontSize: "13px !important",
    color: theme.palette.text.light,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
    marginTop: 5,
    paddingLeft: 7,
  },
  link: {
    color: theme.palette.secondary.main,
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
    cursor: "pointer",
    textDecoration: "underline",
  },
  projectDropdownsLabel: {
    transform: "translate(6px, -7px) scale(1)",
    display: "block",
    color: "#646464",
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
  },
  projectEditor: {
    maxHeight: "430px !important",
    height: "200px",
    padding: 20,
    // fontSize: "14px !important",
    // fontFamily: theme.typography.fontFamilyLato,
    // fontWeight: 400,
    overflowY: "auto",
    borderRadius: 4,
    color: "#646464",
  },
  descriptionCntt: {
    maxHeight: "430px !important",
    height: "280px",
    padding: 10,
    borderRadius: "4px",
    overflowY: "auto",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    resize: "vertical",
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
    color: "#646464",
    "& *": {
      margin: 0,
    },
  },
  lockIcon: {
    // color: "#FF4A4A",
    fontSize: "16px !important",
    paddingTop: "3px",
  },
  limitExceedCnt: {
    // background: "#ff4a4a3b",
    display: "flex",
    borderRadius: "5px",
    width: "fit-content",
    marginBottom: "5px",
    padding: "2px 5px",
    // opacity: "15%"
  },
  limitExceedTxt: {
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
  },
  descriptionCnt:{
    marginTop: 20,
    marginBottom:10
  },
  systemFieldsHeading: {
    width: "100%",
    height: "32px",
    background: "#89A3B2",
    borderRadius: "4px",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    padding: "10px",
    marginTop: "8px",
    cursor: "pointer",

    "& h3": {
      fontSize: "13px !important",
      fontFamily: theme.typography.fontFamilyLato,
      color: theme.palette.common.white,
      marginBottom: 3,
    },
    "&:hover $editIcon": {
      visibility: "visible",
    },
    "&:hover $documentIcon": {
      visibility: "visible",
    },
  },
  systemFieldTitle: {
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    padding: "9px 0px 10px 5px",
    height: "fit-content",
    marginRight: 10,
    fontWeight: theme.typography.fontWeightExtraLight,
    color: "white",
  },
  systemFieldRemoveIcon: {
    color: theme.palette.common.white,
    fontSize: "20px !important",
    cursor: "pointer",
  },
  systemFieldAddIcon: {
    color: theme.palette.common.white,
    fontSize: "20px !important",
    cursor: "pointer",
  },
});
export default detailTabStyle;
