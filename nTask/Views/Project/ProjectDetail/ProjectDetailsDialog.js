import React, { useState, useEffect } from "react";
import { withSnackbar } from "notistack";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect, useDispatch } from "react-redux";
import combinedStyles from "../../../utils/mergeStyles";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import dialogStyles from "../../../assets/jss/components/dialog";
import Header from "./Header/Header";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { statusData } from "../../../helper/projectDropdownData";
import isEmpty from "lodash/isEmpty";
import ProjectContentLeft from "./ProjectContentLeft/ProjectContent";
import { grid } from "../../../components/CustomTable2/gridInstance";
import ProjectChat from "../ProjectDetails/ProjectChat";
import { FormattedMessage } from "react-intl";
import {
  saveNewProject,
  updateProjectData,
  getProjectSettingById,
  saveBulkTasksInProject,
  getSetLoadingProjectDetails,
  markProjectCompleted,
  projectDetails,
  dispatchProject, UpdateProjectDetailListing,
} from "../../../redux/actions/projects";
import { clearDocuments } from "../../../redux/actions/documents";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";

function ProjectDetailsDialog(props) {
  const {
    theme,
    classes,
    open = false,
    handleDialogClose,
    data = {},
    allProjects,
    workspaceLevelPer,
    projectSetting,
    getSetLoadingProjectDetails,
    intl,
    clearDocuments,
  } = props;
  const dispatch = useDispatch();
  const [selectedProject, setSelectedProject] = useState({});
  const [projectStatusData, setProjectStatusData] = useState([]);
  const [permission, setPermission] = useState({}); /** permission for the project  */
  const [taskDetails, setTaskDetails] = useState(false);
  const [taskDetailsLoading, setTaskDetailsLoading] = useState(false);
  const [doneStatusDialogOpen, setDoneStatusDialog] = useState(false);
  const [doneStatusBtnQuery, setDoneStatusBtnQuery] = useState("");

  useEffect(() => {
    const { theme, classes } = props;
    if (!isEmpty(props.selectedProject)) {
      setSelectedProject(
        props.selectedProject
      ); /** set selected project coming from props into the state */

      setProjectSatus(); /** function call which sets the project status drop down data and if already created project then assiging status value to the component else assign default values */

      props.getProjectSettingById(
        /** fetching project setting from the API */
        props.selectedProject.projectId,
        succ => {
          getSetLoadingProjectDetails(false);
        },
        fail => { },
        {
          data: props.selectedProject,
          docked: data.docked ? true : false,
          fullView: data.fullView ? true : false,
        }
      );
      /** if already created project then assiging saved values to the components */
      setProjectTitle(props.selectedProject.projectName);
      setSelectedColor(props.selectedProject.colorCode);
      setProjectDescription(props.selectedProject.description);
      getPermissionAccordingToRole(); /** function for assign the relevant permission */
    }
  }, [props.selectedProject.projectId]);

  useEffect(() => {
    return () => {
      getSetLoadingProjectDetails(false);
      clearDocuments();
    };
  }, []);
  useEffect(() => {
    if (projectSetting.projectSettings) {
      const obj = {
        tasks: projectSetting.projectSettings.tasks?.length,
        milestones: projectSetting.projectSettings.mileStones?.length
      }
      UpdateProjectDetailListing(dispatch, obj, selectedProject.projectId);
    }
  }, [projectSetting.projectSettings && projectSetting.projectSettings?.tasks?.length, projectSetting.projectSettings && projectSetting.projectSettings.mileStones?.length]);

  const getPermissionAccordingToRole = () => {
    /** function that sets the project permission */
    if (props.selectedProject.projectPermission) {
      setPermission(props.selectedProject.projectPermission.permission.project.projectDetails);
    } else {
      setPermission(workspaceLevelPer.projectDetails);
    }
  };

  const setProjectSatus = () => {
    /** function call one time for setting status drop down data and selected status when use opens already created project */
    const { theme, classes, selectedProject = {} } = props;
    const projectStatusData = statusData(
      theme,
      classes,
      intl
    ); /** geeting project status drop down data and setting into state */
    setProjectStatusData(projectStatusData);
    if (!isEmpty(selectedProject)) {
      const selectedStatus = projectStatusData.find(p => p.value == selectedProject.status) || {};
      setProjectStatus(selectedStatus);
    } else {
      setProjectStatus(projectStatusData[0]);
    }
  };

  const [projectTitle, setProjectTitle] = useState("");
  const [editProjectName, setEditProjectName] = useState(false);

  const [projectObj, setProjectObj] = useState(null);
  const handleTextFieldChange = event => {
    /** function call when user enter project title */
    setProjectTitle(event.target.value);
    setError({
      errorState: false,
      errorMessage: "",
    });
  };
  const [error, setError] = useState({
    errorState: false,
    errorMessage: "",
  });
  const onKeyDownProjectName = e => {
    if (e.key == "Enter") {
      if (!isEmpty(selectedProject)) {
        const newName = selectedProject.projectName.trim() == projectTitle.trim();
        if (!newName) {

          updateProject(selectedProject, { projectName: projectTitle.trim() })
          // editProject(projectDescription, selectedColor, projectStatus, 'projectName');
        } else {
          setEditProjectName(false);
        }
      }
    }
  };
  const handleProjectNameClickAway = e => {
    e.preventDefault();
    if (e && !isEmpty(selectedProject)) {
      const newName = selectedProject.projectName.trim() == projectTitle.trim();
      if (!newName) {
        updateProject(selectedProject, { projectName: projectTitle.trim() })
        // editProject(projectDescription, selectedColor, projectStatus, 'projectName');
      } else {
        setEditProjectName(false);
      }
    }
  };

  const handleNameEdit = () => {
    setEditProjectName(true); /** only accessable if user have permission to edit name */
  };
  const updateProject = (project, obj) => {
    if (isEmpty(projectTitle)) {
      setError({
        errorState: true,
        errorMessage: intl.formatMessage({
          id: "project.creation-dialog.title-required",
          defaultMessage: "Oops! Please enter project title.",
        }),
      });
    } else {
      setEditProjectName(false);
      setSaving(true);
      data.projectSettings.updatedDate = new Date().getTime();
      updateProjectData(
        { project, obj },
        dispatch,
        //Success
        succ => {
          setSelectedProject(succ);
          const rowNode = grid.grid && grid.grid.getRowNode(succ.id);
          rowNode.setData(succ);
          setSaving(false);
          setDoneStatusBtnQuery("");
          setProjectObj(null)
          projectCompleteConfirmClose();
        },
        err => {
          setSaving(false);
          setProjectObj(null)
          setSelectedProject(props.selectedProject);
          if (err.data) showSnackBar(err.data.message, "error");
        }
      );
    }
  };
  const editProject = (description = "", color = "", status = {}, updateValue) => {
    console.log(updateValue)
    if (isEmpty(projectTitle)) {
      setError({
        errorState: true,
        errorMessage: intl.formatMessage({
          id: "project.creation-dialog.title-required",
          defaultMessage: "Oops! Please enter project title.",
        }),
      });
    } else {
      setEditProjectName(false);
      setSaving(true);
      let obj = {
        projectId: selectedProject.projectId,
        projectName: projectTitle.trim(),
        description: description,
        status: status.value,
        colorCode: color,
      };
      data.projectSettings.updatedDate = new Date().getTime();
      props.saveNewProject(
        obj,
        succ => {
          setSelectedProject(succ.data);
          setSaving(false);
        },
        err => {
          setSaving(false);
          setSelectedProject(props.selectedProject);
          if (err.data) showSnackBar(err.data.message, "error");
        },
        data.projectSettings
      );
    }
  };

  const [saving, setSaving] = useState(false);

  const [selectedColor, setSelectedColor] = useState("#F47373");
  const colorChange = color => {
    /** function call when user select/change project color  */
    if (color) {
      setSelectedColor(color == "#ffffff" ? "" : color);
      updateProject(selectedProject, { colorCode: color == "#ffffff" ? "" : color })
      // editProject(projectDescription, color == "#ffffff" ? "" : color, projectStatus, 'colorCode');
    }
  };

  const [projectStatus, setProjectStatus] = useState({});
  const projectCompleteConfirmOpen = () => {
    setDoneStatusDialog(true);
  };
  //Close project complete confirmation dialog action
  const projectCompleteConfirmClose = () => {
    setDoneStatusDialog(false);
  };
  //Function project status
  const handleupdateProjectStatus = status => {
    if (status) {
      setProjectStatus(status);
    }
  };
  const handleStatusSelect = status => {
    /** function call when user select the status of the project  */
    if (status.value === 3) {
      let projectObj = { ...selectedProject, status: status.value }
      setProjectObj(projectObj)
      projectCompleteConfirmOpen();
      return;
    }
    updateProject(selectedProject, { status: status.value })
    // editProject(projectDescription, selectedColor, status, 'status');
  };

  const projectCompleteSuccess = () => {
    const completeStatusObj = projectStatusData.find(s => s.value === 3)
    setDoneStatusBtnQuery("progress");
    handleupdateProjectStatus(completeStatusObj)
    // props.markProjectCompleted(selectedProject, (res) => {

    //   setDoneStatusBtnQuery("");
    //   projectCompleteConfirmClose();
    //   // props.dispatchProject(res.data);
    //   props.projectDetails({
    //     projectSettings: res.data,
    //     fullView: projectSetting.fullView ? true : false,
    //     docked: projectSetting.docked ? true : false,
    //     dialog: true,
    //   });
    // }); 
    updateProject(projectObj, { status: projectObj.status });
  };

  const [projectDescription, setProjectDescription] = useState("");
  const handleDescriptionInput = e => {
    /** function call when user enter project description */
    if (e != null && e != undefined && e !== projectDescription) {
      setProjectDescription(e);
      let description = e && e !== "" ? window.btoa(unescape(encodeURIComponent(e))) : e;
      updateProject(selectedProject, { description: description })
      // editProject(e, selectedColor, status, 'description');
    }
  };

  const showSnackBar = (snackBarMessage, type, options) => {
    /** showing snack bar if case of any error/ success scenario  */
    const { classes, enqueueSnackbar } = props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type || "info",
        ...options
      }
    );
  };

  const [projectDetailView, setProjectDetailView] = useState(false);
  const openProjectDetailView = () => {
    /** function for displaying/ hiding the project details view */
    setProjectDetailView(projectDetailView ? false : true);
  };
  const [openChatView, setOpenChatView] = useState(false);

  const addTasksInProject = obj => {
    props.saveBulkTasksInProject(
      obj,
      succ => { },
      fail => { },
      {
        data: selectedProject,
        docked: projectSetting.docked ? true : false,
        fullView: projectSetting.fullView ? true : false,
      }
    );
  };

  // let permissionAttachment = getCompletePermissionsWithArchieve(
  //   this.props.taskData,
  //   this.props.permission,
  //   "attachment"
  // );
  const deleteAttachment = true;
  // permissionAttachment === true ? true : permissionAttachment.delete;

  return (
    <>
      <ActionConfirmation
        open={doneStatusDialogOpen}
        closeAction={projectCompleteConfirmClose}
        cancelBtnText={<FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />}
        successBtnText={"Yes, Mark All Complete"}
        alignment="center"
        headingText={"Mark All Tasks Complete"}
        iconType="markAll"
        msgText={"Are you sure you want to mark all project tasks as completed"}
        successAction={() => projectCompleteSuccess()}
        btnQuery={doneStatusBtnQuery}
      />
      {data.fullView && !isEmpty(permission) ? (
        <Dialog
          open={open}
          disableBackdropClick={true}
          onClose={event => props.handleDialogClose(event, "projectDialog")}
          // onClick={e => {
          //   e.stopPropagation();
          // }}
          PaperProps={{
            style: {
              maxWidth: "90%",
              height: "90%",
              background: theme.palette.common.white,
              overflow: "hidden",
              // minWidth: projectDetailView ? 1100 : 650,
            },
          }}
          classes={{
            paper:
              data.chatView ||
                data.docsView ||
                data.activityLogView ||
                !isEmpty(taskDetails) ||
                taskDetailsLoading
                ? classes.dialogueWidth
                : classes.dialogPaperCntt,
            // scrollBody: classes.dialogCnt,
          }}>
          {" "}
          {!isEmpty(data.projectSettings) && (
            <>
              <DialogTitle id="form-dialog-title" classes={{ root: classes.defaultDialogTitle }}>
                <Header
                  detailView={true}
                  projectTitle={projectTitle}
                  error={error}
                  handleTextFieldChange={handleTextFieldChange}
                  onKeyDownProjectName={onKeyDownProjectName}
                  handleProjectNameClickAway={handleProjectNameClickAway}
                  projectStatusData={projectStatusData}
                  selectedStatus={projectStatus}
                  handleStatusSelect={handleStatusSelect}
                  selectedColor={selectedColor}
                  colorChange={colorChange}
                  handleDialogClose={handleDialogClose}
                  openProjectDetailView={openProjectDetailView}
                  openChatView={() => setOpenChatView(prevState => !prevState)}
                  handleNameEdit={handleNameEdit}
                  editProjectName={editProjectName}
                  permission={permission}
                  intl={intl}
                />
              </DialogTitle>
              <DialogContent
                classes={{ root: classes.defaultDialogContent }}
                style={{
                  display: "flex",
                  overflow: "hidden",
                  // paddingBottom: 24,
                }}>
                <ProjectContentLeft
                  selectedProject={selectedProject}
                  setSelectedProject={setSelectedProject}
                  saving={saving}
                  projectDescription={projectDescription}
                  setProjectDescription={handleDescriptionInput}
                  setSaving={setSaving}
                  showSnackBar={showSnackBar}
                  openChat={openChatView}
                  permission={permission}
                  taskDetails={taskDetails}
                  setTaskDetails={setTaskDetails}
                  addTasksInProject={addTasksInProject}
                  taskDetailsLoading={taskDetailsLoading}
                  setTaskDetailsLoading={setTaskDetailsLoading}
                  intl={intl}
                />
              </DialogContent>
            </>
          )}
        </Dialog>
      ) : (
        !isEmpty(data.projectSettings) &&
        !isEmpty(permission) && (
          <>
            <DialogTitle id="form-dialog-title" classes={{ root: classes.defaultDialogTitle }}>
              <Header
                detailView={true}
                projectTitle={projectTitle}
                error={error}
                handleTextFieldChange={handleTextFieldChange}
                onKeyDownProjectName={onKeyDownProjectName}
                handleProjectNameClickAway={handleProjectNameClickAway}
                projectStatusData={projectStatusData}
                selectedStatus={projectStatus}
                handleStatusSelect={handleStatusSelect}
                selectedColor={selectedColor}
                colorChange={colorChange}
                handleDialogClose={handleDialogClose}
                openProjectDetailView={openProjectDetailView}
                openChatView={() => setOpenChatView(prevState => !prevState)}
                permission={permission}
                intl={intl}
              />
            </DialogTitle>
            <DialogContent
              classes={{ root: classes.defaultDialogContent }}
              style={{
                display: "flex",
                overflow: "hidden",
                // paddingBottom: 24,
              }}>
              <ProjectContentLeft
                style={{ flex: 1 }}
                selectedProject={selectedProject}
                setSelectedProject={setSelectedProject}
                saving={saving}
                projectDescription={projectDescription}
                setProjectDescription={handleDescriptionInput}
                setSaving={setSaving}
                showSnackBar={showSnackBar}
                openChat={openChatView}
                permission={permission}
                taskDetails={taskDetails}
                setTaskDetails={setTaskDetails}
                addTasksInProject={addTasksInProject}
                taskDetailsLoading={taskDetailsLoading}
                setTaskDetailsLoading={setTaskDetailsLoading}
                intl={intl}
              />
            </DialogContent>
          </>
        )
      )}
    </>
  );
}
ProjectDetailsDialog.defaultProps = {
  classes: {},
  theme: {},
  data: {},
  allProjects: [],
  workspaceLevelPer: {},
  projectSetting: {},
  getSetLoadingProjectDetails: () => { },
  clearDocuments: () => { },
};

const mapStateToProps = state => {
  return {
    data: state.projectDetailsDialog,
    workspaceLevelPer: state.workspacePermissions.data.project,
    allProjects: state.projects.data,
    projectSetting: state.projectDetailsDialog,
  };
};

export default compose(
  withRouter,
  withSnackbar,
  withStyles(combinedStyles(styles, dialogStyles), { withTheme: true }),
  connect(mapStateToProps, {
    saveNewProject,
    getProjectSettingById,
    saveBulkTasksInProject,
    getSetLoadingProjectDetails,
    clearDocuments,
    markProjectCompleted,
    projectDetails,
    dispatchProject
  })
)(ProjectDetailsDialog);
