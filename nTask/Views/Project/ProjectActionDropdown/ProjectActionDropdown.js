import React, { useState, useRef, useEffect, memo } from "react";
import { useDispatch } from "react-redux";
import DropdownMenu from "../../../components/Dropdown/DropdownMenu";
import DeleteConfirmDialog from "../../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import DeleteBulkProjectDialogContent from "../../../components/Dialog/Popups/bulkDeleteConfirmation";
import CustomIconButton from "../../../components/Buttons/CustomIconButton";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import CustomListItem from "../../../components/ListItem/CustomListItem";
import withStyles from "@material-ui/core/styles/withStyles";
import projectActionDropdownStyles from "./projectActionDropdown.style";
import CustomMenuList from "../../../components/MenuList/CustomMenuList";
import {
  CopyTask,
  DeleteTask,
  ArchiveTask,
  updateTaskData,
  UnArchiveTask,
} from "../../../redux/actions/tasks";
import {
  CopyProject,
  updateProjectData,
  ArchiveProject,
  UnarchiveProject,
  DeleteProject,
  moveProjectToWorkspace,
} from "../../../redux/actions/projects";
import { emptyTask } from "../../../utils/constants/emptyTask";
import { v4 as uuidv4 } from "uuid";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import MoveProjectDialog from "../../../components/Dialog/MoveProjectDialog/MoveProjectDialog";
import { FormattedMessage } from "react-intl";
import { withSnackbar } from "notistack";
import { compose } from "redux";
import { publickLink, setDeleteDialogueState } from "../../../redux/actions/allDialogs";
import ColorPicker from "../../../components/Dropdown/ColorPicker/ColorPicker";
import isEqual from "lodash/isEqual";
import "react-toastify/dist/ReactToastify.css";
import IconActivity from "../../../components/Icons/TaskActionIcons/IconActivity";
import IconArchive from "../../../components/Icons/TaskActionIcons/IconArchive";
import IconColor from "../../../components/Icons/TaskActionIcons/IconColor";
import IconDelete from "../../../components/Icons/TaskActionIcons/IconDelete";
import MoveIcon from "../../../components/Icons/MoveIcon";
import EditIcon from "../../../components/Icons/EditIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import IconCopy from "../../../components/Icons/TaskActionIcons/IconCopy";
import IconLink from "../../../components/Icons/TaskActionIcons/IconLink";
import { emptyProject } from "../../../utils/constants/emptyProject";
import CopyProjectForm from "../List/CopyProjectForm";
import { FetchWorkspaceInfo } from "../../../redux/actions/workspace";

// import constants from "../constants/types";
function areEqual(prevProps, nextProps) {
  return isEqual(prevProps, nextProps);
  /*
  return true if passing nextProps to render would return
  the same result as passing prevProps to render,
  otherwise return false
  */
}
const ProjectActionDropdown = memo(props => {
  const [open, setOpen] = useState(null);
  const [archiveConfirmation, setArchiveConfirmation] = useState(false);
  const [unArchiveConfirmation, setUnArchiveConfirmation] = useState(false);
  const [renameConfirmation, setRenameConfirmation] = useState(false);

  const [moveProjectsToWorkspace, setMoveProjectToWorkspace] = useState(false);
  const [moveProjectBtnQuery, setMoveProjectBtnQuery] = useState("");

  const [archiveBtnQuery, setArchiveBtnQuery] = useState("");
  const [copyFlag, setCopyFlag] = useState(false);

  const [deleteFlag, setDeleteFlag] = useState(false);
  const [step, setStep] = useState(0);
  const [disabled, setDisabled] = useState(true);
  const [deleteBtnQuery, setDeleteBtnQuery] = useState('');

  const [unarchiveBtnQuery, setUnarchiveBtnQuery] = useState("");
  const anchorEl = useRef(null);
  const dispatch = useDispatch();
  const {
    classes,
    data,
    btnProps,
    handleActivityLog,
    handleCloseCallBack,
    projectPermission,
    isArchived = false,
    project,
    activityLogOption = false,
  } = props;
  const handleClose = event => {
    // Function closes dropdown
    setOpen(null);
  };
  const handleArchiveConfirmClose = () => {
    setArchiveConfirmation(false);
    setUnArchiveConfirmation(false);
  };
  const handleArchiveConfirmation = () => {
    setArchiveConfirmation(true);
  };
  const handleUnArchiveConfirmation = () => {
    setUnArchiveConfirmation(true);
    handleCloseCallBack();
  };
  const handleDropdownOpen = event => {
    // Function Opens the dropdown
    setOpen(state => (state ? null : event.currentTarget));
  };
  const showSnackBar = (snackBarMessage = "", type = null) => {
    const { classes, enqueueSnackbar } = props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  useEffect(() => {
    anchorEl.current &&
      anchorEl.current.addEventListener("click", e => {
        e.stopPropagation();
        handleDropdownOpen(e);
      });
  }, []);

  //Copy of Project
  const handlePopupDialogClose = () => {
    setCopyFlag(false)
  }
  const handleCopyProject = () => {
    setCopyFlag(true)
  };
  //Archive a Project
  const archiveProject = () => {
    const { onDeleteProject, GanttCmp, childProjects, isGantt } = props;
    const childIds = childProjects && childProjects.map(t => t.id);

    setArchiveBtnQuery("progress");
    handleCloseCallBack();
    const projectId = data.projectId;
    if (isGantt && onDeleteProject) {
      onDeleteProject(projectId, GanttCmp, childIds, false);
    }
    ArchiveProject(
      projectId,
      () => {
        // Success Callback
        handleArchiveConfirmClose();
        setArchiveBtnQuery("");
      },
      err => {
        // failure callback
        setArchiveBtnQuery("");
        handleArchiveConfirmClose();
        if (err.data && err.data.message) showSnackBar(err.data.message, "error");
      },
      dispatch
    );
    handleClose();
  };
  // Unarchive a Project
  const handleUnArchive = e => {
    if (e) e.stopPropagation(); 
    setUnarchiveBtnQuery("progress");
    handleCloseCallBack();
    UnarchiveProject(
      data,
      //success
      () => {
        handleArchiveConfirmClose();
        setUnarchiveBtnQuery('');
      },
      //failure
      error => {
        setUnarchiveBtnQuery('');
        showSnackBar("Project Cannot be UnArchived", "error");
      },
      dispatch
    );
  };
  // move project

  const handleRenameConfirmation = () => {
    setRenameConfirmation(true);
  };
  const handleMove = () => {
    setMoveProjectToWorkspace(true);
  };
  const handleCloseMove = () => {
    setMoveProjectToWorkspace(false);
  };
  const handleMoveProject = (e, team) => {
    if (e) e.stopPropagation();
    setMoveProjectBtnQuery("progress");
    let obj = {
      projectId: data.projectId,
      workspaceId: team.teamId,
    };
    moveProjectToWorkspace(
      obj,
      succ => {
        setMoveProjectBtnQuery("");
        handleCloseMove();
      },
      err => {
        setMoveProjectBtnQuery("");
        handleCloseMove();
        if (err.data && err.data.message) showSnackBar(err.data.message, "error");
      },
      dispatch
    );
  };
  //Confirmation Dialog close
  // const closeDeleteConfirmation = () => {
  //   dispatch(setDeleteDialogueState({ open: false, btnQuery: "" }));
  // };
  //Open delete confirmation dialog
  // const handleDeleteConfirmation = success => {
  //   const dialogObj = {
  //     open: true,
  //     cancelBtnText: "Cancel",
  //     successBtnText: "Delete",
  //     headingText: "Delete",
  //     msgText: `Are you sure you want to delete this Project?`,
  //     successAction: deleteProject,
  //     closeAction: closeDeleteConfirmation,
  //     btnQuery: "",
  //   };
  //   dispatch(setDeleteDialogueState(dialogObj));
  //   handleClose();
  // };
  //Delete single project
  // const deleteProject = () => {
  //   const { onDeleteProject, GanttCmp, childProjects, isGantt } = props;
  //   const childIds = childProjects && childProjects.map(t => t.id);
  //   const projectId = data.projectId;
  //   handleCloseCallBack();
  //   dispatch(setDeleteDialogueState({ btnQuery: "progress" }));
  //   if (isGantt && onDeleteProject) {
  //     onDeleteProject(projectId, GanttCmp, childIds, false);
  //   }
  //   DeleteProject(
  //     data,
  //     (data, res) => {
  //       closeDeleteConfirmation();
  //     },
  //     err => {
  //       closeDeleteConfirmation();
  //       if (err.data && err.data.message) showSnackBar(err.data.message, "error");
  //     },
  //     dispatch
  //   );
  // };

  const colorChange = color => {
    const obj = { colorCode: color };
    handleClose();
    updateProjectData(
      { project: data, obj },
      dispatch,
      //Success
      project => { }
    );
  };
  const handleCopy = (data, succ, fail) => {
    CopyProject(data, succ, fail, dispatch);
  }
  // delete conformation modal 
  const closeConfirmationPopup = () => {
    setDeleteFlag(false)
    setStep(0)
    // this.setState({ deleteFlag: false, step: 0 });
  };
  const handleTypeProjectInput = (event) => {
    let value = event.target.value;
    const project = data;
    project &&
      project.projectName.replace(/\s+/g, " ").toLowerCase() ===
      value.trim().toLowerCase()
      ? setDisabled(false)
      : setDisabled(true);
  };
  const handleDeleteProjectStep = (value) => {
    setStep(value)
  };
  const handleDeleteProject = (event) => {
    event.stopPropagation();
    setDeleteBtnQuery("progress")
    const { onDeleteProject, GanttCmp, childProjects, isGantt } = props;
    const childIds = childProjects && childProjects.map(t => t.id);
    const projectId = data.projectId;
    if (isGantt && onDeleteProject) {
      onDeleteProject(projectId, GanttCmp, childIds, false);
    }
    DeleteProject(
      data,
      (response) => {
        setDeleteBtnQuery('');
        closeConfirmationPopup();
        if (response.status === 200) {
          FetchWorkspaceInfo("", () => { }, () => { }, dispatch);
          setDisabled(true);
        }
        else {
          showSnackBar(response.data.message, "error")
        }
      },
      err => {
        closeConfirmationPopup();
        if (err.data && err.data.message) showSnackBar(err.data.message, "error");
      },
      dispatch
    );
  };
  // permissions allowed
  let copyPer = projectPermission ? projectPermission.copyInWorkSpace.cando : true;
  // let colorPer = projectPermission ? projectPermission.setColor.cando : true;

  let renamePer = projectPermission ? projectPermission.projectDetails.editProjectName.isAllowEdit : true;
  let movePer = projectPermission ? projectPermission.moveProject.cando : true;
  let archivePer = projectPermission ? projectPermission.archive.cando : true;
  let unarchivePer = projectPermission ? projectPermission.unarchive.cando : true;
  let deletePer = projectPermission ? projectPermission.delete.cando : true;

  const isOpen = Boolean(open);
  return (
    <>
      <CustomIconButton
        btnType="transparent"
        buttonRef={node => (anchorEl.current = node)}
        style={{ padding: 0 }}
        {...btnProps}>
        <MoreVerticalIcon className={classes.ellipsesIcon} />
      </CustomIconButton>
      <DropdownMenu
        open={isOpen}
        closeAction={handleClose}
        anchorEl={anchorEl.current}
        size={"small"}
        placement="bottom-end"
        disablePortal={false}>
        <CustomMenuList>
          {copyPer && !isArchived && (
            <CustomListItem rootProps={{ onClick: handleCopyProject }}>
              <SvgIcon viewBox="0 0 14 14" className={classes.icon}>
                <IconCopy />
              </SvgIcon>
              Copy
            </CustomListItem>
          )}
          {!isArchived && (
            <CustomListItem
              subNav={true}
              popperProps={{
                placement: "left-start",
              }}
              subNavRenderer={
                <>
                  <ColorPicker
                    triangle="hide"
                    onColorChange={colorChange}
                    selectedColor={data.colorCode || ""}
                    width={190}
                  />
                </>
              }>
              <SvgIcon viewBox="0 0 16.5 16.5" className={classes.icon}>
                <IconColor />
              </SvgIcon>
              Color
            </CustomListItem>
          )}
          {activityLogOption && !isArchived && (
            <CustomListItem rootProps={{ onClick: handleActivityLog }}>
              <SvgIcon viewBox="0 0 18 15.428" className={classes.icon}>
                <IconActivity />
              </SvgIcon>
              Activity Log
            </CustomListItem>
          )}
          {archivePer && !isArchived && (
            <CustomListItem rootProps={{ onClick: handleArchiveConfirmation }}>
              <SvgIcon viewBox="0 0 14 14.014" className={classes.icon}>
                <IconArchive />
              </SvgIcon>
              Archive
            </CustomListItem>
          )}
          {movePer && !isArchived && (
            <CustomListItem rootProps={{ onClick: handleMove }}>
              <SvgIcon viewBox="0 0 40 40" className={classes.icon}>
                <MoveIcon />
              </SvgIcon>
              Move
            </CustomListItem>
          )}
          {/* {renamePer && !isArchived && (
            <CustomListItem rootProps={{ onClick: handleArchiveConfirmation }}>
              <SvgIcon viewBox="0 0 14 13.95" className={classes.icon}>
                <EditIcon />
              </SvgIcon>
              Rename
            </CustomListItem>
          )} */}
          {unarchivePer && isArchived && (
            <CustomListItem rootProps={{ onClick: handleUnArchiveConfirmation }}>
              <SvgIcon viewBox="0 0 14 14.014" className={classes.icon}>
                <IconArchive />
              </SvgIcon>
              Un Archive
            </CustomListItem>
          )}
          {deletePer && (
            <CustomListItem rootProps={{ onClick: () => setDeleteFlag(true) }}>
              <SvgIcon viewBox="0 0 14 14.002" className={classes.icon}>
                <IconDelete />
              </SvgIcon>
              Delete
            </CustomListItem>
          )}
        </CustomMenuList>
      </DropdownMenu>
      {archiveConfirmation ? (
        <ActionConfirmation
          open={true}
          closeAction={handleArchiveConfirmClose}
          cancelBtnText={
            <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
          }
          successBtnText={
            <FormattedMessage
              id="common.action.archive.confirmation.archive-button.label"
              defaultMessage="Archive"
            />
          }
          alignment="center"
          headingText={
            <FormattedMessage
              id="common.action.archive.confirmation.archive-button.label"
              defaultMessage="Archive"
            />
          }
          iconType="archive"
          msgText={
            <FormattedMessage
              id="common.archived.tasks.messageb"
              defaultMessage="Are you sure you want to archive this {label}?"
              values={{
                label: 'Project'
              }}
            />
          }
          successAction={archiveProject}
          btnQuery={archiveBtnQuery}
        />
      ) : null}
      {unArchiveConfirmation ? (
        <ActionConfirmation
          open={true}
          closeAction={handleArchiveConfirmClose}
          cancelBtnText={
            <FormattedMessage
              id="common.action.un-archive.confirmation.cancel-button.label"
              defaultMessage="Cancel"
            />
          }
          successBtnText={
            <FormattedMessage
              id="common.action.un-archive.confirmation.un-archive-button.label"
              defaultMessage="Unarchive"
            />
          }
          alignment="center"
          iconType="unarchive"
          headingText={
            <FormattedMessage
              id="common.action.un-archive.confirmation.title"
              defaultMessage="Unarchive"
            />
          }
          msgText={
            <FormattedMessage
              id="common.action.un-archive.confirmation.task.label"
              defaultMessage="Are you sure you want to unarchive this {label}?"
              values={{
                label: 'Project'
              }}
            />
          }
          successAction={handleUnArchive}
          btnQuery={unarchiveBtnQuery}
        />
      ) : null}

      <CopyProjectForm
        open={copyFlag}
        closeAction={handlePopupDialogClose}
        project={data}
        copyProject={handleCopy}
      />

      <DeleteConfirmDialog
        open={deleteFlag}
        closeAction={closeConfirmationPopup}
        cancelBtnText={
          <FormattedMessage
            id="common.action.cancel.label"
            defaultMessage="Cancel"
          />
        }
        successBtnText={
          step == 0 ? (
            <FormattedMessage
              id="common.action.delete.anyway.label"
              defaultMessage="Delete Anyway"
            />
          ) : (
            <FormattedMessage
              id="common.action.delete.confirmation.delete-button.label"
              defaultMessage="Delete"
            />
          )
        }
        alignment="center"
        headingText={
          <FormattedMessage
            id="common.action.delete.confirmation.delete-button.label"
            defaultMessage="Delete"
          />
        }
        disabled={step === 0 ? false : disabled}
        btnQuery={deleteBtnQuery}
        successAction={
          step === 0
            ? () => {
              handleDeleteProjectStep(1);
            }
            : () => {
              handleDeleteProject(event);
            }
        }
      >
        {deleteFlag ? (
          <DeleteBulkProjectDialogContent
            step={step}
            project={data}
            handleTypeProjectInput={handleTypeProjectInput}
          />
        ) : null}
      </DeleteConfirmDialog>
      {/* move project dialog */}
      <MoveProjectDialog
        open={moveProjectsToWorkspace}
        projectName={data.projectName}
        project={data}
        successAction={handleMoveProject}
        closeAction={handleCloseMove}
        btnQuery={moveProjectBtnQuery}
        cancelBtnText={<FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />}
        exceptionText={""}
        headingText={"Move Project to Workspace"}
        isMulti={false}
      />
    </>
  );
}, areEqual);

export default compose(
  withSnackbar,
  withStyles(projectActionDropdownStyles, { withTheme: true })
)(ProjectActionDropdown);

///new
