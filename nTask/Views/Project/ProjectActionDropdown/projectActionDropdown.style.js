const projectActionDropdownStyles = theme => ({
  ellipsesIcon: {
    color: theme.palette.icon.gray600
  },
  snackBarHeadingCnt: {
    marginLeft: 10,
  },
  snackBarContent: {
    margin: 0,
    fontSize: "12px !important",
  },
  icon: {
    fontSize: "14px !important",
    marginRight: "7px !important",
    marginBottom: "-1px !important",
    color: "#969696",
  }
});

export default projectActionDropdownStyles;