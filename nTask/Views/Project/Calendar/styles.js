const calendarStyles = theme => ({
    calendarToolbar: {
        marginBottom: 20
    },
    navigationArrow: {
        fontSize: "28px !important"
    },
    toolbarCurrentDate: {
        fontSize: "18px !important",
        fontWeight: 500,
        verticalAlign: "middle",
        display: "inline-block",
        width:160,
        textAlign: 'center'
    },
    NextBackBtnCnt:{
        margin: "4px 0 0 20px",
    }
})

export default calendarStyles;