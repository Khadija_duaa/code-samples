import React from "react";
import { Calendar, momentLocalizer } from "react-big-calendar";
import { withStyles, withTheme } from "@material-ui/core/styles";
import withDragAndDrop from "react-big-calendar/lib/addons/dragAndDrop";
import moment from "moment";
// import "react-big-calendar/lib/addons/dragAndDrop/styles.less";
import 'react-big-calendar/lib/css/react-big-calendar.css';
import "../../../assets/css/react-big-calendar.css";
import LeftArrow from "@material-ui/icons/ArrowLeft";
import RightArrow from "@material-ui/icons/ArrowRight";
import CustomIconButton from "../../../components/Buttons/IconButton";
import DefaultButton from "../../../components/Buttons/DefaultButton";
import calendarStyles from "./styles";
import Grid from "@material-ui/core/Grid";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import helper from "../../../helper";
import COLORCODES from "../../../components/constants/colorCodes";
import { UpdateMeetingCalender } from "../../../redux/actions/meetings";
import sortBy from "lodash/sortBy";
import { FormattedMessage } from "react-intl";
import { statusData } from "../../../helper/projectDropdownData";
import { excludeOffdays, getCalendar } from "../../../helper/dates/dates";
// const DragAndDropCalendar = withDragAndDrop(BigCalendar);
const localizer = momentLocalizer(moment);

class TaskCalendar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      events: [],
      currentTaskId: "" //this.props.calenderTasks.calenderTasks
    };
  }
  eventStyle(event) {
    if (event.projectColor) {
      return {
        style: { background: event.projectColor }
      };
    } else {
      return {};
    }
  }
  onDoubleClickEvent = (event, e) => {
    if (event.id) {
      /** navigate to selected project from project gantt on double click */
      this.props.history.push({
        pathname: "/projects",
        search: `?projectId=${event.id}`
      });
    }
  };

  sortDates = (a, b) => {
    return new Date(a).getTime() - new Date(b).getTime();
  };


  // highligh off days

  render() {
    const {
      theme,
      classes,
      calenderTasks,
      projectsState,
      calenderDateType,
      customCalendar
    } = this.props;
    const Toolbar = toolbar => {
      const goToBack = () => {
        toolbar.date.setMonth(toolbar.date.getMonth() - 1);
        toolbar.onNavigate("prev");
      };

      const goToNext = () => {
        toolbar.date.setMonth(toolbar.date.getMonth() + 1);
        toolbar.onNavigate("next");
      };

      const goToCurrent = () => {
        const now = new Date();
        toolbar.date.setMonth(now.getMonth());
        toolbar.date.setYear(now.getFullYear());
        DefaultButton;
        toolbar.onNavigate("current");
      };

      return (
        <div className={classes.calendarToolbar}>
          <Grid container>
            <Grid item>
              <DefaultButton
                text={<FormattedMessage id="common.action.today.label" defaultMessage="Today" />}
                buttonType="Plain"
                onClick={goToCurrent}
              />
            </Grid>
            <Grid item classes={{ item: classes.NextBackBtnCnt }}>
              <CustomIconButton btnType="condensed" onClick={goToBack}>
                <LeftArrow
                  htmlColor={theme.palette.secondary.dark}
                  classes={{ root: classes.navigationArrow }}
                />
              </CustomIconButton>
              <span className={classes.toolbarCurrentDate}>
                {toolbar.label}
              </span>
              <CustomIconButton btnType="condensed" onClick={goToNext}>
                <RightArrow
                  htmlColor={theme.palette.secondary.dark}
                  classes={{ root: classes.navigationArrow }}
                />
              </CustomIconButton>
            </Grid>
          </Grid>
        </div>
      );
    };
     let components = {
      toolbar: Toolbar,
      month: {
        dateHeader: ({ date, label }) => {
          return (
            <button type="button" className={`rbc-button-link ${!excludeOffdays(date, customCalendar) ? 'highlightWeekend' : ''}`} role="cell">{label}</button>
          );
        }
      }
    };
    const customDayPropGetter = (date) => {
      if (!excludeOffdays(date, customCalendar))
        return {
          className: 'off-day',
          // style: {
          //   border: 'solid 3px ' + (date.getDate() === 7 ? '#faa' : '#afa'),
          // },
        };
      else return {};
    };
    const currentDate = moment();
    const events = () => {
      const projectStatusData = statusData(theme, classes);
      return projectsState
        ? projectsState.data
          ? projectsState.data.length > 0
            ? projectsState.data.map(project => {
              const data = this.props.tasksState.data
                ? this.props.tasksState.data
                  .filter(x => x.projectId === project.projectId)
                  .map(y => {
                    return {
                      startDate:
                        this.props.calenderDateType === "Actual Start/End"
                          ? y.actualStartDate
                          : y.startDate,
                      endDate:
                        this.props.calenderDateType === "Actual Start/End"
                          ? y.actualDueDate
                          : y.dueDate
                    };
                  })
                : [];
              if (data && data.length) {
                let taskDates = data.map(data => Object.values(data));
                let arrDates = [].concat.apply([], taskDates);
                let sorted = arrDates
                  .filter(function (el) {
                    return el != null;
                  })
                  .sort(this.sortDates);
                return {
                  id: project.projectId,
                  title: project.projectName || "",
                  start: new Date(
                    helper.RETURN_CUSTOMDATEFORMAT(
                      moment(new Date(sorted[0] ? sorted[0] : null))
                    )
                  ),
                  end: new Date(
                    helper.RETURN_CUSTOMDATEFORMAT(
                      moment(
                        new Date(
                          sorted[sorted.length - 1]
                            ? sorted[sorted.length - 1]
                            : null
                        )
                      ).add(1, 'days').format("MM/DD/YYYY")
                    )
                  ),
                  projectColor:
                    project.colorCode == "#ffffff"
                      ? "#fc7150"
                      : project.colorCode ? project.colorCode : projectStatusData.find(item => item.value == project.status).color
                };
              }
            })
            : []
          : []
        : [];
    };
    return (
      <div style={{ height: 700, width: "100%" }}>
        <Calendar
          popup
          localizer={localizer}
          components={components}
          dayPropGetter={customDayPropGetter}
          events={events()}
          eventPropGetter={this.eventStyle}
          defaultDate={currentDate._d}
          onDoubleClickEvent={this.onDoubleClickEvent}
        />
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    tasksState: state.tasks,
    projectsState: state.projects
  };
};
export default compose(
  withRouter,
  withStyles(calendarStyles, { withTheme: true }),
  connect(mapStateToProps, {
    UpdateMeetingCalender,
    excludeOffdays,
    getCalendar,
  })
)(TaskCalendar);
