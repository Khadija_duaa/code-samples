// @flow

import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import BarStyles from "./style.js";
import { compose } from "redux";
import { connect } from "react-redux";

import classNames from "classnames";
import { calculateContentHeight } from "../../../utils/common";
import SearchInput, { createFilter } from "react-search-input";
import Badge from "@material-ui/core/Badge";
import Add from "@material-ui/icons/Add";
import CustomIconButton from "../../../components/Buttons/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import ClearIcon from "@material-ui/icons/Clear";
import NotificationMessage from "../../../components/NotificationMessages/NotificationMessages";
import { Scrollbars } from "react-custom-scrollbars";
import { Droppable, Draggable } from "react-beautiful-dnd";
import { FormattedMessage } from "react-intl";
import SearchIcon from "@material-ui/icons/Search";
import CustomTooltip from "../../../components/Tooltip/Tooltip";

type UnsortedColumnProps = {
  contentHeight: Integer,
  classes: Object,
  theme: Object,
  handleUnsortedClose: Function,
};

const UnsortedColumn = (props: UnsortedColumnProps) => {
  const {
    classes,
    contentHeight,
    theme,
    data,
    handleUnsortedClose,
    barIndex,
    unsortedId,
    unsortedTaskList,
    intl,
    addUnsortedTaskInStateList,
    selectedColumn,
  } = props;
  const [allTasks, setAllTasks] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");
  const [searchQuery, setSearchQuery] = useState(["id", "value.name", "value.status"]);

  useEffect(() => {
    unsortedTasks();
  }, [unsortedTaskList]);

  const searchUpdated = term => {
    setSearchTerm(term);
  };
  const unsortedTasks = () => {
    // let tasks = data.filter((d) => {
    //   return d.projectId == null;
    // });
    // tasks = tasks.length
    //   ? tasks.map((x) => {
    //       return {
    //         id: x.taskId,
    //         value: { name: x.taskTitle, status: x.statusName, obj: x },
    //       };
    //     })
    //   : [];
    let tasks =
      unsortedTaskList.length > 0
        ? unsortedTaskList.map(x => {
            return {
              id: x.taskId,
              value: { name: x.taskTitle, status: x.statusName, obj: x },
            };
          })
        : [];
    setAllTasks(tasks);
  };

  const getStatusBadge = obj => {
    return (
      <div className={classes.badgeCnt} style={{ background: obj.statusColorCode }}>
        <span> {obj.statusName} </span>
      </div>
    );
  };

  const handleDoubleClickItem = (event, ele) => {
    setAllTasks(allTasks.filter(t => t.id !== ele.id));
  };

  const filteredData = allTasks.filter(createFilter(searchTerm, searchQuery));
  return (
    <div className={[classes.taskBar, props.positionClass].join(" ")} style={{top: props.scrollTop}}>
      <div className={classes.header}>
        <div className={classes.columnNameCnt}>
         <CustomTooltip
            helptext="Unassigned Tasks"
            placement="top"
          >
            <h3 className={classes.caption}>
              {
                <FormattedMessage
                  id="board.board-detail.unsorted-task.label2"
                  defaultMessage="Unassigned Tasks"
                />
              }
            </h3>
          </CustomTooltip>
        </div>
        <div className={classes.menu}>
          <CustomIconButton
            btnType="filledWhite"
            onClick={handleUnsortedClose}
            style={{
              borderRadius: "50%",
              border: "none",
              marginLeft: 6,
              boxShadow: "0px 3px 6px #0000001A",
              padding: 3,
              backgroundColor: "#fff",
              //   color: data.colorCode,
            }}>
            <Tooltip
              classes={{
                tooltip: classes.tooltip,
              }}
              title={"Close"}
              placement="bottom">
              <ClearIcon className={classes.closeIcon} />
            </Tooltip>
          </CustomIconButton>
        </div>
      </div>
      <div className={classes.body}>
        <NotificationMessage
          type="NewInfo"
          iconType="info"
          style={{
            width: "100%",
            textAlign: "start",
            fontSize: "11px",
            padding: "6px",
            backgroundColor: "#EAEAEA",
            borderRadius: 4,
            fontFamily: theme.typography.fontFamilyLato,
            fontWeight: 400,
          }}>
          {
            <FormattedMessage
              id="board.board-detail.unsorted-task.note"
              values={{ statusName: <b>{selectedColumn ? selectedColumn.statusTitle : ""}</b> }}
              defaultMessage={`Drag & drop task to any list of the board or click on the task to add it to ${
                selectedColumn ? selectedColumn.statusTitle : ""
              } list.`}
            />
          }
        </NotificationMessage>
        <div className={classes.searchCnt}>
          {<SearchIcon className={classes.searchIcon} />}
          <SearchInput
            className="HtmlInput"
            onChange={searchUpdated}
            maxLength="80"
            placeholder={intl.formatMessage({
              id: "board.board-detail.unsorted-task.placeholder",
              defaultMessage: "Search task by title or status.",
            })}
          />
        </div>
        <div className={classes.unsortedTasksCnt}>
          <Draggable index={barIndex} draggableId={`${unsortedId}`}>
            {(provided, snapshot) => (
              <div ref={provided.innerRef} {...provided.draggableProps}>
                <div {...provided.dragHandleProps}></div>
                <Droppable key={unsortedId} droppableId={`${unsortedId}`} type="task">
                  {(provided, snapshot) => (
                    <div ref={provided.innerRef} {...provided.droppableProps}>
                      <Scrollbars
                        autoHide
                        style={{ height: contentHeight - 16 }} // Calculate actual height : content height - margin
                      >
                        {filteredData.map((t, index) => {
                          return (
                            <>
                              <Draggable key={t.id} draggableId={t.id} index={index}>
                                {(provided, snapshot) => (
                                  <div
                                    ref={provided.innerRef}
                                    {...provided.draggableProps}
                                    {...provided.dragHandleProps}
                                    {...provided.draggableProps.style}>
                                    <div
                                      className={classes.unsortedTask}
                                      onClick={() => {
                                        addUnsortedTaskInStateList(t.value.obj, selectedColumn);
                                      }}>
                                      <span className={classes.title}> {t.value.name}</span>
                                      {getStatusBadge(t.value.obj)}
                                    </div>
                                  </div>
                                )}
                              </Draggable>
                            </>
                          );
                        })}
                        {provided.placeholder}
                      </Scrollbars>
                    </div>
                  )}
                </Droppable>
              </div>
            )}
          </Draggable>
        </div>
      </div>
    </div>
  );
};

UnsortedColumn.defaultProps = {
  data: [],
  classes: {},
  theme: {},
  handleUnsortedClose: () => {},
};

const mapStateToProps = state => {
  return {
    data: state.tasks.data || [],
  };
};

export default compose(
  withStyles(BarStyles, { withTheme: true }),
  connect(mapStateToProps, {})
)(UnsortedColumn);
