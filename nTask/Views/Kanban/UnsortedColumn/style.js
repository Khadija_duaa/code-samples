const BarStyles = (theme) => ({
  taskBar: {
    margin: "0px 5px",
    borderRadius: "6px 6px 0px 0px",
    width: 325,
  },
  header: {
    background: `#7E7E7E 0% 0% no-repeat padding-box`,
    borderRadius: "6px 6px 0px 0px",
    display: "flex",
    justifyContent: "space-between",
    padding: "5px 13px 6px 13px",
    alignItems: "center",
    color: `${theme.palette.common.white}`,
    height: 34,
  },
  newBar: {
    padding: "9px 13px 8px 13px",
    color: `${theme.palette.background.green}`,
    background: `${theme.palette.background.grayLighter} 0% 0% no-repeat padding-box`,
    width: 250,
    justifyContent: "flex-start",
  },
  inputHeader: {
    background: `${theme.palette.background.purpleLight}`,
    padding: "4px 5px",
  },
  addInput: {
    padding: "4px 8px 5px 8px",
    width: "100%",
    outline: "none",
    border: "none",
    fontSize: "14px !important",
    lineHeight: "18px",
    fontWeight: theme.typography.fontWeightMedium,
    fontFamily: "Lato, sans-serif",
    borderRadius: 4,
    cursor: "pointer",
  },
  caption: {
    fontFamily: "Lato, sans-serif",
    fontWeight: theme.typography.fontWeightMedium,
    fontSize: "14px !important",
    margin: 0,
    letterSpacing: 0,
    color: `${theme.palette.common.white}`,
    wordBreak: "break-word"
  },
  newCaption: {
    color: `${theme.palette.secondary.dark}`,
  },
  menu: {
    alignItems: "center",
    // width: "15%",
    cursor: "pointer",
    zIndex: 10,
  },
  menuIcon: {
    fontSize: "17px !important",
    color: `${theme.palette.common.white}`,
  },
  menuItem: {
    fontSize: "14px !important",
    padding: "4px 12px",
    primary: {
      color: `${theme.palette.text.primary} !important`,
      fontFamily: "Lato, sans-seriff",
    },
  },
  body: {
    padding: 8,
    background: "#f3f3f380",
    boxShadow: `0px 2px 0px ${theme.palette.border.greyBorder}`,
    borderRadius: "0 0 6px 6px",
  },
  newBody: {
    background: `${theme.palette.background.paper} 0% 0% no-repeat padding-box;`,
    border: `2px dashed ${theme.palette.background.contrast}`,
    borderTop: "none",
    padding: 8,
    cursor: "pointer",
    borderRadius: "0 0 6px 6px",
  },
  itemText: {
    fontSize: "12px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightLight,
    width: 134,
    overflow: "hidden",
    whiteSpace: "nowrap",
    lineHeight: "normal",
    textOverflow: "ellipsis",
  },
  statusMenuItemSelected: {
    background: `transparent !important`,
    "&:hover": {
      background: `transparent !important`,
    },
  },
  colorPickerCntLeft: {
    right: -212,
    top: 0,
    padding: 0,
    position: "absolute",
    background: "white",
    boxShadow: "0px 0px 5px -1px rgba(0,0,0,0.07)",
    borderRadius: 7,
  },
  addInput: {
    width: "100%",
    outline: "none",
    fontSize: "14px !important",
    lineHeight: "13px",
    fontWeight: theme.typography.fontWeightLarge,
    fontFamily: "Lato, sans-serif",
    // border: `1px solid ${theme.palette.border.blue}`,
    borderRadius: 4,
    height: 25,
  },
  listItemsCount: {
    fontFamily: "Lato, sans-serif",
    fontWeight: theme.typography.fontWeightMedium,
    fontSize: "14px !important",
    margin: 0,
    letterSpacing: 0,
    color: `${theme.palette.common.black}`,
    marginLeft: 10,
    // marginTop:1
  },
  columnNameCnt: {
    display: "flex",
    cursor: "pointer",
  },
  closeIcon: {
    fontSize: "13px !important",
  },
  tooltip: {
    fontSize: "12px !important",
    backgroundColor: theme.palette.common.black,
    "& .chat-tooltip-body": {
      padding: "5px 15px 5px 5px",
      "& ul,li": {
        margin: 0,
        padding: 0,
        listStyleType: "none",
        color: theme.palette.common.white,
      },
    },
  },
  searchCnt: {
    marginTop: 10,
  },
  searchIcon: {
    // marginTop: "5px",
    // marginRight: "-30px",
    color: "#969696",
    zIndex: 1000,
    position: "absolute",
    marginLeft: 271,
    marginTop: 5
  },
  unsortedTask: {
    backgroundColor: "#fff",
    minHeight: 37,
    marginBottom: 10,
    boxShadow: "0px 2px 0px rgba(229, 229, 229, 1)",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 10,
    borderRadius : 4,
    // cursor: "pointer"
  },
  unsortedTasksCnt: {
    marginTop: 15,
  },
  title: {
    color: "#202020",
    fontFamily: "Lato, sans-serif",
    fontWeight: theme.typography.fontWeightMedium,
    fontSize: "14px !important",
    userSelect : 'none'
    // marginLeft : 10
  },
  badgeCnt: {
    padding: 5,
    fontSize: "11px !important",
    color: "#fff",
    fontFamily: "Lato, sans-serif",
    fontWeight: theme.typography.fontWeightMedium,
    borderRadius: 4,
    height: 22,
    // minWidth: 70,
    marginLeft : 3,
    overflow: 'hidden',
    minWidth: 77,
    maxWidth: 77,
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    textAlign: 'center'
  },
});
export default BarStyles;
