import React, { useState, useEffect } from "react";
import { compose } from "redux";
import { connect, useSelector } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import DefaultDialog from "../../../components/Dialog/Dialog";
import ButtonActionsCnt from "../../../components/Dialog/ConfirmationDialogs/ButtonActionsCnt";
import Divider from "@material-ui/core/Divider";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import ToggleButton from "@material-ui/lab/ToggleButton";
import Brightness1Icon from "@material-ui/icons/Brightness1";
import SelectSearchDropdown from "../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import CustomButton from "../../../components/Buttons/CustomButton";
import RoundIcon from "@material-ui/icons/Brightness1";
import isEmpty from "lodash/isEmpty";
import NotificationMessage from "../../../components/NotificationMessages/NotificationMessages";

type TaskStatusChangeDialogProps = {
  classes: Object,
  theme: Object,
  open: Boolean,
  handleClose: Function,
  oldStatus: Object,
  newTemplateItem: Object,
  handleSaveAsTemplate: Function,
  showSnackBar: Function,
};

function TaskStatusChangeDialog(props: TaskStatusChangeDialogProps) {
  const {
    classes,
    theme,
    handleClose,
    open,
    tasks,
    oldStatus,
    newTemplateItem,
    showSnackBar,
    companyInfo
  } = props;
  const [currentView, setView] = useState("mappManually");
  const [selectedStatus, setSelectedStatus] = useState({});
  const profileState = useSelector(state => state.profile.data);
  const handleChangeTab = (event, nextView) => {
    setView(nextView);
  };

  const generateStatuses = () => {
    /* function for generating the array of status and returing array to the data prop for multi drop down */
    let statuses = newTemplateItem.statusList.map((s, i) => {
      //   return generateSelectData(s.statusTitle, s.statusId, s.statusId, s.statusId, s);
      return {
        label: s.statusTitle,
        value: s.statusId,
        icon: <RoundIcon htmlColor={s.statusColor} className={classes.statusesIcon} />,
      };
    });
    return statuses;
  };

  const handleSelectStatus = (key, opt, s) => {
    const { loggedInTeam, workspace } = profileState;
    const currentWorkspace = workspace.find(t => t.teamId === loggedInTeam);
    const selectedStatus = newTemplateItem.statusList.find(item => item.statusId === opt.value)

    if (selectedStatus.isDoneState && currentWorkspace.config.isUserTasksEffortMandatory) {
      showSnackBar("Make sure you have added effort in the task", "info", {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
      });
    }
    setSelectedStatus(opt);
  };

  useEffect(() => { }, []);

  const handleSuccess = () => {
    let obj = {
      projectId: newTemplateItem.projectId,
      StatusId: oldStatus.statusId,
      IsDefault: oldStatus.isDefault,
      isDoneState: oldStatus.isDoneState,
      isDeleteTask: false,
      toStatusId: null,
    };

    let newStatus = false;

    if (currentView == "mappManually") {
      newStatus = newTemplateItem.statusList.find(o => o.statusId == selectedStatus.value) || false;
      obj.toStatusId = newStatus.statusId;
      let columnMaxLimit =
        newStatus.statusRulesList.find(s => s.ruleTitle == "MAXTASKLIMIT_RULE") || false;
      if (columnMaxLimit) {
        if (
          !columnMaxLimit.dynamicStatusRule ||
          columnMaxLimit.dynamicStatusRule.keyValue >= newStatus.tasksList.length + oldStatus.tasksList.length
        ) {
          props.handleSaveAsTemplate(obj);
        } else {
          showSnackBar(
            "You cannot move this task because destination column has been approached to its limit!",
            "error"
          );
        }
      } else {
        props.handleSaveAsTemplate(obj);
      }
    }
    if (currentView == "allOpen") {
      obj.isDeleteTask = true;
      obj.toStatusId = null;
      props.handleSaveAsTemplate(obj);
    }
  };

  const getDefaultDone = item => {
    if (item.isDefault && item.isDoneState) {
      return (
        <>
          <span style={{ marginRight: 5 }} className={classes.defaultSpan}>
            {"Initial"}
          </span>
          and
          <span className={classes.doneSpan}>{"Final"}</span>
        </>
      );
    } else if (item.isDefault && !item.isDoneState) {
      return (
        <span
          className={
            item.isDefault ? classes.defaultSpan : item.isDoneState ? classes.doneSpan : null
          }>
          {"Initial"}
        </span>
      );
    } else if (!item.isDefault && item.isDoneState) {
      return (
        <span
          className={
            item.isDefault ? classes.defaultSpan : item.isDoneState ? classes.doneSpan : null
          }>
          {"Final"}
        </span>
      );
    }
  };

  //   const defaultStatus = newTemplateItem.statusList.find(s => s.isDefault) || {};
  let buttonDisable = currentView == "allOpen" || !isEmpty(selectedStatus) ? false : true;
  const taskLabelSingle = companyInfo?.task?.sName;
  const taskLabelMulti = companyInfo?.task?.pName;
  return (
    <>
      <DefaultDialog
        title={"Tasks Mapping"}
        sucessBtnText="Done"
        disableBackdropClick={true}
        disableEscapeKeyDown={true}
        open={open}
        onClose={handleClose}>
        <div className={classes.statusMapCnt}>
          {oldStatus.tasksList.length > 0 && (
            <>
              <div className={classes.heading}>
                <span
                  className={classes.title}
                  style={{ textAlign: "center", padding: "0px 43px", lineHeight: 1.6 }}>
                  {`Looks like you are trying to delete`}{" "}
                  <span
                    className={classes.title}
                    style={{
                      color: "#202020",
                      fontWeight: 800,
                    }}>{`"${oldStatus.statusTitle}"`}</span>{" "}
                  {`status.`}{" "}
                  {oldStatus.isDefault || oldStatus.isDoneState ? `which is marked as` : ""}
                  {oldStatus.isDefault || oldStatus.isDoneState ? getDefaultDone(oldStatus) : null}
                </span>
              </div>
              <div className={classes.heading}>
                <span className={classes.title}>How would we handle these {taskLabelMulti ? taskLabelMulti : 'tasks'}?</span>
              </div>

              <Divider style={{ margin: "20px 0" }} />

              {!oldStatus.isDefault && !oldStatus.isDoneState && (
                <div className={classes.toggleContainer}>
                  <ToggleButtonGroup
                    value={currentView}
                    exclusive
                    onChange={handleChangeTab}
                    classes={{ root: classes.toggleBtnGroup, groupedHorizontal: classes.groupedHorizontal }}>
                    <ToggleButton
                      value="mappManually"
                      classes={{
                        root: classes.toggleButton,
                        selected: classes.toggleButtonSelected,
                      }}>
                      Map Tasks To Another Status
                    </ToggleButton>
                    <ToggleButton
                      value="allOpen"
                      classes={{
                        root: classes.toggleButton,
                        selected: classes.toggleButtonSelected,
                      }}>
                      Delete All Tasks
                    </ToggleButton>
                  </ToggleButtonGroup>
                </div>
              )}
              {currentView == "mappManually" && (
                <>
                  <div className={classes.labelCnt}>
                    {" "}
                    <span className={classes.labelTitleLeft}>Old Status</span>
                    <span className={classes.labelTitleRight}>New Status</span>
                  </div>
                  {/* {statusArr.map((s, index) => {
                return ( */}
                  <div
                    className={classes.statusesCnt}
                    style={{ justifyContent: "space-between", padding: "0px 20px" }}
                    key={0}>
                    <CustomButton
                      btnType="white"
                      variant="contained"
                      style={{
                        fontSize: "14px",
                        color: "#202020",
                        border: "none",
                        background: "#F6F6F6",
                        marginLeft: 0,
                        fontFamily: theme.typography.fontFamilyLato,
                        fontWeight: theme.palette.fontWeightMedium,
                        // padding: "7px 50px 7px 9px",
                        width: 170,
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "end",
                      }}>
                      <Brightness1Icon
                        className={classes.statusesIcon}
                        style={{ color: oldStatus.statusColor }}
                      />
                      {oldStatus.statusTitle}
                    </CustomButton>

                    <SelectSearchDropdown /* Issue Status select drop down */
                      data={generateStatuses}
                      label={null}
                      selectChange={(key, opt) => {
                        handleSelectStatus(key, opt);
                      }}
                      type="status"
                      selectedValue={selectedStatus}
                      placeholder={"Select status"}
                      icon={true}
                      isMulti={false}
                      isDisabled={false}
                      styles={{ width: 170, margin: 0 }}
                    />
                  </div>
                  {/* );
              })} */}
                </>
              )}
            </>
          )}

          {oldStatus.tasksList.length == 0 && (
            <>
              <div className={classes.heading}>
                <span
                  className={classes.title}
                  style={{ textAlign: "center", padding: "0px 43px", lineHeight: 1.6 }}>
                  {`Looks like you are about to delete `}
                  <span className={classes.title} style={{ color: "#202020", fontWeight: 800 }}>
                    {oldStatus.statusTitle}
                  </span>{" "}
                  {` status which is marked as`}
                  {getDefaultDone(oldStatus)}
                </span>
              </div>
              <div className={classes.heading}>
                <span className={classes.title}>
                  Please select the new status to mark as{" "}
                  {oldStatus.isDoneState && oldStatus.isDefault
                    ? "initial and final"
                    : oldStatus.isDefault
                      ? "initial"
                      : oldStatus.isDoneState
                        ? "final"
                        : ""}
                  .
                </span>
              </div>

              <Divider style={{ margin: "20px 0" }} />
              <div className={classes.statusesCnt}>
                <SelectSearchDropdown /* Issue Status select drop down */
                  data={generateStatuses}
                  label={
                    oldStatus.isDefault && oldStatus.isDoneState
                      ? "New Initial and Final Status"
                      : oldStatus.isDoneState && !oldStatus.isDefault
                        ? "New Final Status"
                        : !oldStatus.isDoneState && oldStatus.isDefault
                          ? "New Initial Status"
                          : null
                  }
                  selectChange={(key, opt) => {
                    handleSelectStatus(key, opt);
                  }}
                  type="status"
                  selectedValue={selectedStatus}
                  placeholder={"Select status"}
                  icon={true}
                  isMulti={false}
                  isDisabled={false}
                  styles={{ width: 270, marginBottom: 0 }}
                />
              </div>
            </>
          )}

          <Divider style={{ margin: "20px 0" }} />
          {oldStatus.isDefault || oldStatus.isDoneState ? (
            <>
              <NotificationMessage
                type="info"
                iconType="info"
                style={{
                  // width: "calc(100% - 50px)",
                  // padding: "10px 15px 10px 15px",
                  backgroundColor: "rgba(243, 243, 243, 1)",
                  borderRadius: 4,
                  // margin: "10px 20px",
                }}>
                {`New selected status will also be marked as
              ${oldStatus.isDoneState && oldStatus.isDefault
                    ? "initial and final"
                    : oldStatus.isDefault
                      ? "initial"
                      : oldStatus.isDoneState
                        ? "final"
                        : ""
                  }.`}
              </NotificationMessage>
            </>
          ) : null}
        </div>
        <ButtonActionsCnt
          cancelAction={handleClose}
          successAction={handleSuccess}
          successBtnText={"Done"}
          cancelBtnText={"Cancel"}
          btnType="blue"
          btnQuery={""}
          disabled={buttonDisable}
        />
      </DefaultDialog>
    </>
  );
}

TaskStatusChangeDialog.defaultProps = {
  classes: {},
  theme: {},
  open: false,
  handleClose: () => { },
  tasks: [],
  oldStatus: {},
  newTemplateItem: {},
  handleSaveAsTemplate: () => { },
  showSnackBar: () => { },
};
const mapStateToProps = state => {
  return {
    tasks: state.tasks.data || [],
    companyInfo: state.whiteLabelInfo.data,
  };
};

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, {})
)(TaskStatusChangeDialog);
