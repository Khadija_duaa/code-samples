const taskBoardStyles = theme => ({
  container: {
    width: "100%",
    paddingRight: 30,
  },
  taskContainer: {
    width: "256px",
    paddingLeft: 32,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: "transparent",
  },
  fixedPosition: {
    position: "sticky",
    left: 15,
    zIndex: 10,
  },
  normalPosition: {},
});
export default taskBoardStyles;
