import React from "react";
import Avatar from "@material-ui/core/Avatar";
import SampleAvatar from "../../../assets/images/1.png";
import { withStyles } from "@material-ui/core/styles";
import classNames from "classnames";
import ticketStyles from "./style.js";
import CustomAvatar from "../../../components/Avatar/Avatar";

const Assigners = (props) => {
  const { classes, items, theme } = props;

  const getRandomColor = (str) => {
    /** fun that generate the random color on the basis of full name/email */
    var hash = 0;
    if (str) {
      for (var i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
      }
    }

    var h = hash % 360;
    return "hsl(" + h + ", " + 60 + "%, " + 50 + "%)";
  };

  return (
    <div className={classes.assigners}>
      {items &&
        items.map(
          (item, index) =>
            index < 3 && (
              // <Avatar
              //   key={`avatar-${index}`}
              //   alt="Assigner Avatar"
              //   src={item.imageUrl ? item.imageUrl : ""}
              //   className={classNames(classes.smallAvatar, {
              //     [classes.secondChild]: index > 0,
              //   })}
              //   style={{
              //     zIndex: 4 - index,
              //     backgroundColor: getRandomColor(
              //       item.fullName ? item.fullName : item.email
              //     ),
              //     color: "#ffffff",
              //     fontSize: "11px !important",
              //     fontWeight: 400,
              //     fontFamily: theme.typography.fontFamilyLato,
              //   }}
              // >
              //   {item.fullName.charAt().toUpperCase()}
              // </Avatar>
              <div style={{zIndex : index, marginLeft: index == 0 ? "" : "-5px"  }}>
              <CustomAvatar
                  otherMember={{
                    imageUrl: item.imageUrl,
                    fullName: item.fullName,
                    lastName: "",
                    email: item.email,
                    isOnline: false,
                    isOwner: false,
                  }}
                  size={"xsmall"}
                  closeTaskDetailsPopUp={()=>{}}
                />
                </div>
            )
        )}
      {items.length > 3 && (
        <Avatar
          alt="Assignee Avatar"
          className={classNames(classes.smallAvatar, classes.thirdChild)}
          style={{
            zIndex: 0,
            backgroundColor: "#EBF6FF",
            color: "#0090ff",
            fontSize: "11px",
          }}
        >
          +{items.length - 3}
        </Avatar>
      )}
    </div>
  );
};

export default withStyles(ticketStyles, { withTheme: true })(Assigners);
