// @flow

import React, { Fragment, useState, useRef } from "react";
import { withStyles } from "@material-ui/core/styles";
import ticketStyles from "./style.js";
import classNames from "classnames";
import Add from "@material-ui/icons/Add";
import { createNewTaskHelpText } from "../../../components/Tooltip/helptext";
import DefaultTextField from "../../../components/Form/TextField";
import ConvertToTaskIcon from "../../../components/Icons/ConvertToTaskIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import Tooltip from "@material-ui/core/Tooltip";
import { FormattedMessage } from "react-intl";
import { useSelector } from "react-redux";
type NewTicketProps = {
  onUpdate: Function,
  onAdd: Function,
  handleUnsortedOpener: Function,
  index: Integer,
  barIndex: Integer,
  item: Object,
  classes: Object,
  theme: Object,
  disabled: Boolean,
  boardPer: Object,
};

const NewTicket = (props: NewTicketProps) => {
  const { classes, onAdd, disabled, theme, handleUnsortedOpener, boardPer, intl, maxTaskObj, tasksList } = props;
  const [focusIn, setFocusIn] = useState(false);
  const [title, setTitle] = useState("");
  const [TaskError, setError] = useState(false);
  const [TaskErrorMessage, setMessage] = useState("");
  const [fieldDisabled, setFieldDisabled] = useState(false);
  const anchorEl = useRef();

  const { companyInfo } = useSelector(state => {
    return {
      companyInfo: state.whiteLabelInfo.data,
    };
  });
  const handlClick = () => {
    setFocusIn(true);
  };

  const focusOut = () => {
    setFocusIn(false);
    setError(false);
    setMessage("");
  };

  const handleChangeTitle = e => {
    const title = e.target.value;
    if (title === "") {
      setError(true);
      setMessage("Please Type Valid Title");
    } else {
      setError(false);
      setMessage("");
    }
    setTitle(e.target.value);
  };

  const addNewTicket = e => {
    if (e.key === "Enter") {
      setFieldDisabled(true);
      if (title.trim() === "") {
        setError(true);
        setMessage("Please Type Valid Title");
        setFieldDisabled(false);
      } else {
        onAdd(title, succ => {
          setTitle("");
          setFieldDisabled(false);
          anchorEl.current.focus();
          // set focus out when limit exeeds
          tasksList == maxTaskObj - 1 && setFocusIn(false);

        });
        // setFocusIn(false);
      }
    }
  };
  const taskLabelSingle = companyInfo?.task?.sName;
  const taskLabelMulti = companyInfo?.task?.pName;
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        padding: "2px 5px 2px 8px",
      }}>
      <div
        className={classNames(classes.newTicketContainer, {
          [classes.noPadding]: focusIn,
        })}>
        {!focusIn ? (
          <div
            className={classNames(classes.ticketHeader, classes.newHeader)}
            onClick={() => {
              disabled ? null : handlClick();
            }}>
            <Add className={classes.addIcon} />
            <span className={classes.itemTitle}>
              <FormattedMessage id="task.common.add-new.label" defaultMessage="Add New {label}" values={{ label: taskLabelSingle || "Task" }} />
            </span>
          </div>
        ) : (
          <DefaultTextField
            error={TaskError}
            errorState={TaskError}
            errorMessage={TaskErrorMessage}
            // helptext={createNewTaskHelpText.taskTitleHelpText}
            formControlStyles={{ marginBottom: 0 }}
            styles={fieldDisabled ? classes.addInputDisabled : classes.addInput}
            defaultProps={{
              type: "text",
              id: "TaskTitle",
              placeholder: taskLabelSingle ? `Enter ${taskLabelSingle} Title` : intl.formatMessage({
                id: "board.board-detail.addtask.placeholder",
                defaultMessage: "Enter Task Title"
              }),
              value: title,
              autoFocus: true,
              inputProps: { maxLength: 250 },
              onChange: event => handleChangeTitle(event),
              onKeyDown: addNewTicket,
              onBlur: focusOut,
              disabled: fieldDisabled,
              inputRef: node => anchorEl.current = node
            }}
          />
        )}
      </div>
      {boardPer.boardDetail.addUnsortedTask.cando && (
        <Tooltip
          classes={{
            tooltip: classes.tooltip,
          }}
          title={
            <FormattedMessage
              id="board.board-detail.unsorted-task.label2"
              defaultMessage="Unassigned Task(s)"
            />
          }
          placement="bottom">
          <SvgIcon
            viewBox="0 0 14 11.003"
            htmlColor={theme.palette.secondary.medDark}
            className={classes.actionIcon}
            onClick={handleUnsortedOpener}>
            <ConvertToTaskIcon />
          </SvgIcon>
        </Tooltip>
      )}
    </div>
  );
};

NewTicket.defaultProps = {
  index: 0,
  item: {},
  barIndex: 0,
  onUpdate: () => { },
  handleUnsortedOpener: () => { },
  boardPer: {},
};
export default withStyles(ticketStyles, { withTheme: true })(NewTicket);
