import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import { withStyles } from "@material-ui/core/styles";
import RightArrow from "@material-ui/icons/ArrowRight";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import React, { Component } from "react";
import CustomButton from "../../../components/Buttons/CustomButton";
import ColorPicker from "../../../components/Dropdown/ColorPicker/ColorPicker";
import DropdownMenu from "../../../components/Menu/DropdownMenu";
import ticketStyles from "./style";

class TicketActionMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      pickerOpen: false,
    };
  }

  componentDidMount() {
    const { open } = this.props;
    this.setState({
      open,
    });
  }

  componentWillReceiveProps() {
    const { open } = this.props;
    this.setState({
      open,
      pickerOpen: false,
    });
  }

  onColorChange = (color) => {
    const { onColorUpdate } = this.props;
    // this.setState({
    //   pickerOpen: false,
    // });
    onColorUpdate(color);
  };

  handleClick = (e) => {
    e.stopPropagation();
    this.setState({ open: true });
  };

  handleClose = (event) => {
    if (document.getElementById("ticketDropDown").contains(event.target)) {
      return;
    }

    this.setState({ open: false, pickerOpen: false, searchTerm: "" });
  };

  render() {
    const { open, pickerOpen } = this.state;
    const { theme, style, data, handleToggle, classes, selected, iconVisible } = this.props;
    return (
      <>
        <div className={`esclipeBtnCnt`}>
          <CustomButton
            onClick={this.handleClick}
            btnType="success"
            variant="text"
            style={{ minWidth: "unset" }}
            className={style}
            buttonRef={(node) => {
              this.anchorEl = node;
            }}>
            <MoreVerticalIcon htmlColor={theme.palette.secondary.light} />
          </CustomButton>
        </div>
        <DropdownMenu
          open={open}
          anchorEl={this.anchorEl}
          placement="bottom-end"
          id="ticketDropDown"
          size="small"
          closeAction={this.handleClose}>
          <List className={classes.list}>
            {data.map((item) => {
              return item.name === "Color" ? (
                <ListItem
                  key={item.name}
                  button
                  onClick={() => this.setState({ pickerOpen: true })}>
                  <ListItemText primary={item.name} classes={{ primary: classes.itemText }} />
                  <RightArrow
                    htmlColor={theme.palette.secondary.dark}
                    classes={{ root: classes.submenuArrowBtn }}
                  />
                  <div
                    id="colorPickerCnt"
                    className={classes.colorPickerCntLeft}
                    style={pickerOpen ? { display: "block" } : { display: "none" }}>
                    <ColorPicker
                      triangle="hide"
                      onColorChange={this.onColorChange}
                      selectedColor={selected}
                    />
                  </div>
                </ListItem>
              ) : (
                <ListItem
                  key={item.name}
                  button
                  onClick={(event) => {
                    this.setState({ pickerOpen: false });
                    handleToggle(event, item.name, item.id);
                  }}>
                  <ListItemText primary={item.name} classes={{ primary: classes.itemText }} />
                </ListItem>
              );
            })}
          </List>
        </DropdownMenu>
      </>
    );
  }
}

TicketActionMenu.defaultProps = {
  index: 0,
  open: false,
  style: {},
  item: {},
  selected: "",
  barIndex: 0,
  handleToggle: () => { },
  onColorUpdate: () => { },
};

export default withStyles(ticketStyles, { withTheme: true })(TicketActionMenu);
