// @flow

import { withStyles } from "@material-ui/core/styles";
import SvgIcon from "@material-ui/core/SvgIcon";
import Typography from "@material-ui/core/Typography";
import CalendarToday from "@material-ui/icons/CalendarToday";
import CheckCircle from "@material-ui/icons/CheckCircle";
import CancelIcon from "@material-ui/icons/Close";
import Comment from "@material-ui/icons/Comment";
import classNames from "classnames";
import moment from "moment";
import React, { Fragment, useEffect, useState } from "react";
import { Draggable } from "react-beautiful-dnd";
import { FormattedMessage, injectIntl } from "react-intl";
import { connect } from "react-redux";
import { compose } from "redux";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import DeleteConfirmDialog from "../../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
// import AttachFile from "@material-ui/icons/AttachFile";
import IconAttachFile from "../../../components/Icons/IconAttachFile";
import IssueIcon from "../../../components/Icons/IssueIcon";
import IconSubtask from "../../../components/Icons/ProjectAddMoreIcons/IconSubtask";
import RecurrenceIcon from "../../../components/Icons/RecurrenceIcon";
import RiskIcon from "../../../components/Icons/RiskIcon";
import NotificationMessage from "../../../components/NotificationMessages/NotificationMessages";
import CustomTooltip from "../../../components/Tooltip/Tooltip";
import { getContrastYIQ } from "../../../helper/index";
import { grid } from "../../../utils/constants/taskBoard";
import FeaturedImgsModal from "../FeaturedImgsModal";
import Assigners from "./Assigners";
import ticketStyles from "./style.js";
import TicketActionMenu from "./TicketActionMenu";

type TicketProps = {
  item: Object,
  barindex: Integer,
  index: Integer,
  onUpdate: Function,
  classes: Object,
  theme: Object,
  element: Object,
  handleClickListItem: Function,
  deleteTask: Function,
  archiveTask: Function,
  saveFeatureImage: Function,
  saveLibraryImage: Function,
  boardPer: Object,
  boardData: Object,
};

const Ticket = (props: TicketProps) => {
  const {
    classes,
    theme,
    item,
    onUpdate,
    barIndex,
    index,
    allMembers,
    handleClickListItem,
    element,
    onUpdateTaskColor,
    deleteTask,
    archiveTask,
    copyTask,
    saveFeatureImage,
    saveLibraryImage,
    boardPer,
    intl,
    boardData,
    showpill,
    setShowpill,
    companyInfo
  } = props;
  const {
    assignees,
    attachmentsCount,
    commentsCount,
    createdBy,
    featuredImage,
    issuesCount,
    meetingsCount,
    plannedDates,
    risksCount,
    toDoListCount,
    subTaskCount,
  } = boardData.cardSetting;
  const [menuVisible, setMenuVisible] = useState(false);
  const [openModal, setOpenModal] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);
  const [background, setBackground] = useState(true);
  const [deleteTaskModal, setDeleteTaskModal] = useState(false);
  const [archiveTaskModal, setArchiveTaskModal] = useState(false);
  const [ddData, setddData] = useState([]);

  const bgApply = boardData?.statusList
    ?.find((col) => col.statusId === item.statusId)
    ?.statusRulesList?.find(
      (rule) => rule.ruleTitle === "ADDTASKLABEL_RULE"
    )?.applyLabelColorToBackground;

  // Menu Array Data
  // Todo: Store array as libraries and add actions

  const settingddData = () => {
    const taskLabelSingle = companyInfo?.task?.sName;
    let dropDownData = [];
    if (boardPer.boardDetail.viewTaskdetail.cando) {
      dropDownData.push({
        id: 2,
        name: intl.formatMessage({
          id: "board.board-detail.tasklist.menu.edit-details.label",
          defaultMessage: "Edit Details",
        }),
      });
    }
    if (boardPer.boardDetail.addFeatureImage.cando) {
      dropDownData.push({
        id: 3,
        name: intl.formatMessage({
          id: "common.feature-Image.label",
          defaultMessage: "Add Featured Image",
        }),
      });
    }
    dropDownData.push({
      id: 4,
      name: intl.formatMessage({
        id: "common.action.color.label",
        defaultMessage: "Color",
      }),
    });
    if (boardPer.boardDetail.copyTask.cando) {
      dropDownData.push({
        id: 5,
        name: taskLabelSingle ? `Copy ${taskLabelSingle}` : intl.formatMessage({
          id: "common.action.copy-task.label",
          defaultMessage: "Copy Task",
        }),
      });
    }
    if (boardPer.boardDetail.archiveTask.cando) {
      dropDownData.push({
        id: 6,
        name: taskLabelSingle ? `Archive ${taskLabelSingle}` : intl.formatMessage({
          id: "common.action.archive-task.label",
          defaultMessage: "Archive Task",
        }),
      });
    }
    if (boardPer.boardDetail.deleteTask.cando) {
      dropDownData.push({
        id: 7,
        name: taskLabelSingle ? `Delete ${taskLabelSingle}` : intl.formatMessage({
          id: "common.action.delete-task.label",
          defaultMessage: "Delete Task",
        }),
      });
    }
    setddData(dropDownData);
  };

  const handleToggle = (e, name, id) => {
    e.stopPropagation();
    if (!boardData.isTemplate) {
      switch (id) {
        case 2:
          handleClickListItem(item, element);
          break;
        case 3:
          setOpenModal(true);
          break;
        case 5:
          copyTask(item, element);
          break;
        case 6:
          setArchiveTaskModal(true);
          break;
        case 7:
          // setOpenDelete(true);
          setDeleteTaskModal(true);
          break;

        default:
          break;
      }
    }
  };

  const getItemStyle = (isDragging, draggableStyle, item) => ({
    userSelect: "none",
    margin: `0 0 ${grid}px 0`,
    ...(bgApply &&
      !!item.ruleColors.length && {
        background: `${item.ruleColors[item.ruleColors.length - 1].color}63`,
      }),
    ...draggableStyle,
    borderLeft: item.colorCode ? `3px solid ${item.colorCode}` : "",
  });

  const handleDialogClose = () => {
    setOpenDelete(false);
  };

  const handleDelete = () => {};

  const setFeaturedImage = (flag, url) => {
    setBackground(flag);
  };

  const setLibraryImage = (flag, url) => {
    setBackground(flag);
  };

  const onColorChange = (color, item) => {
    // if (!item.isTemplate) {
    onUpdateTaskColor(color, item, element);
    setMenuVisible(false);
    // }
  };

  const renderAssigners = (item) => {
    /** function is responsible for rendering the assignes images and counts */
    let assigneeList = allMembers.filter((m) => {
      return item.assigneeList.includes(m.userId);
    });
    if (assigneeList && assigneeList.length > 0) {
      return <Assigners items={assigneeList} />;
      // return (
      //   <div className={classes.addAssigneeList}>
      //     <AssigneeDropdown
      //       closeTaskDetailsPopUp={}
      //       assignedTo={[]}
      //       updateAction={}
      //       isArchivedSelected={false}
      //       obj={{}}
      //       singleSelect={false}
      //       buttonVariant="small"
      //       avatarSize={"xsmall"}
      //     />
      //   </div>
      // );
    } else return null;
  };

  const handleDeleteTask = () => {
    setDeleteTaskModal(false);
    deleteTask(item, (callback) => {}, element);
  };
  const handleArchiveTask = () => {
    setArchiveTaskModal(false);
    archiveTask(item, (callback) => {}, element);
  };

  const removeImage = (task) => {
    let obj = {
      docsPath: null,
      taskId: task.taskId,
    };
    saveFeatureImage(
      obj,
      task,
      {},
      () => {},
      () => {}
    );
  };

  useEffect(() => {
    settingddData();
  }, []);

  const getAttachments = () => {
    return (
      <>
        Attachments
        <ul style={{ marginLeft: 14, padding: 0 }}>
          {item.attachmentList.map((ele) => (
            <li> {ele} </li>
          ))}
        </ul>
      </>
    );
  };
  const getToDoList = () => {
    return (
      <>
        Checklist Items
        <ul style={{ marginLeft: 14, padding: 0 }}>
          {item.toDoList.map((ele) => (
            <li> {ele} </li>
          ))}
        </ul>
      </>
    );
  };

  let countObj = {
    count1: item.totalSubTask,
    count2: item.totalToDo,
    count3: item.totalRisk,
    count4: item.totalAttachment,
    count5: item.totalComments,
    count6: item.totalMeetings,
    count7: item.totalIssues,
  };
  let values = Object.values(countObj);
  let countsBar = values.some((ele) => ele > 0);
  const taskLabelSingle = companyInfo?.task?.sName;
  const taskLabelMulti = companyInfo?.task?.pName;
  return (
    <>
      <Draggable key={item.taskId} draggableId={item.taskId} index={index}>
        {(provided, snapshot) => (
          <div
            className={classes.ticketContainer}
            ref={provided.innerRef}
            {...provided.draggableProps}
            {...provided.dragHandleProps}
            style={getItemStyle(snapshot.isDragging, provided.draggableProps.style, item)}
            onClick={(event) => {
              boardPer.boardDetail.viewTaskdetail.cando ? handleClickListItem(item, element) : null;
            }}>
            {/* {background && ( */}
            {item.featureImagePath && item.featureImagePath !== "" && featuredImage && (
              <div className={classes.taskBackgroundImage}>
                <img
                  src={item.featureImagePath}
                  style={{ width: "100%", height: "auto", borderRadius: "4px 4px 0 0" }}
                />
                <CancelIcon
                  className={classes.cancelIcon}
                  onClick={(e) => {
                    e.stopPropagation();
                    removeImage(item);
                  }}
                />
              </div>
            )}
            <div className={classes.ticketHeader}>
              <div className={classes.caption}>
                {item.parentId && <span className={classes.parentTask}>{item.parentName}</span>}
                <span>
                  {item.parentId && (
                    <SvgIcon viewBox="0 0 24 24" className={classes.subtaskIcon}>
                      <IconSubtask />
                    </SvgIcon>
                  )}

                  {!!item.ruleColors?.length && (
                    <div
                      onClick={(event) => {
                        event.stopPropagation();
                        setShowpill((prev) => !prev);
                      }}
                      style={{ display: "flex", alignItems: "center", flexWrap: "wrap" }}>
                      {item.ruleColors?.map(({ name, color }) => {
                        return (
                          <div
                            className={classes.colorBar}
                            style={{
                              height: !showpill && 8,
                              minWidth: 36,
                              backgroundColor: color,
                              color: getContrastYIQ(color),
                            }}>
                            {showpill && name?.length ? name : <>&nbsp;</>}
                          </div>
                        );
                      })}
                    </div>
                  )}
                  <span className={classes.itemTitle}>{item.taskTitle}</span>
                  {item.isRepeatTask && (
                    <SvgIcon
                      viewBox="0 0 14 12.438"
                      htmlColor={theme.palette.primary.light}
                      className={classes.recurrenceIcon}>
                      <RecurrenceIcon />
                    </SvgIcon>
                  )}
                </span>
              </div>
              {!item.isTemplate && (
                <div className={classes.menu}>
                  <TicketActionMenu
                    data={ddData}
                    handleToggle={handleToggle}
                    selected={item.colorCode ? item.colorCode : "#fff"}
                    open={menuVisible}
                    style={classes.menuIcon}
                    onColorUpdate={(color) => {
                      onColorChange(color, item);
                    }}
                  />
                </div>
              )}
            </div>
            {createdBy && (
              <span className={classes.createdBy}>{`Created by: ${item.createdBy}`}</span>
            )}
            <div
              className={classes.ticketBody}
              onClick={(event) => {
                // boardPer.boardDetail.viewTaskdetail.cando
                //   ? handleClickListItem(item, element)
                //   : null;
              }}
              style={{
                display: "flex",
                justifyContent: "space-around",
              }}>
              {/* {item.type && (
                <div
                  className={classes.ticketAttr}
                >
                  <SvgIcon
                    viewBox="0 0 24 24"
                    style={{fill:'#BFBFBF'}}
                    fontSize="small"
                  >
                    <ProjectIcon />
                  </SvgIcon>
                  <span className={classes.attrDetail}>{item.type}</span>
                </div>
              )} */}
              {(item.actualStartDate ||
                item.actualEndDate ||
                (item.assigneeList && item.assigneeList.length > 0)) && (
                <div className={classes.ticketDetails}>
                  <div className={(classes.ticketAttr, classes.duration)}>
                    {(item.actualStartDate || item.actualEndDate) && plannedDates && (
                      <Fragment>
                        <CalendarToday className={classes.attrIcon} />
                        <span className={classes.attrDetail}>
                          {item.actualStartDate
                            ? moment(item.actualStartDate).format("MMM DD")
                            : "Start"}{" "}
                          -
                          {item.actualEndDate ? moment(item.actualEndDate).format("MMM DD") : "End"}
                        </span>
                      </Fragment>
                    )}
                  </div>
                  {assignees && renderAssigners(item)}
                </div>
              )}
            </div>
            {countsBar && (
              <div
                className={classNames(classes.ticketFooter, {
                  // [classes.footerBordered]: true,
                  [classes.footerBordered]:
                    item.totalSubTask > 0 ||
                    item.totalIssues > 0 ||
                    item.totalRisk > 0 ||
                    item.totalComments > 0 ||
                    item.totalToDo > 0 ||
                    item.totalAttachment > 0,
                })}>
                {item.totalSubTask > 0 && subTaskCount && !taskLabelSingle && (
                  <CustomTooltip helptext="Subtask" placement="bottom">
                    <div className={classNames(classes.ticketAttr, classes.footerDetail)}>
                      <SvgIcon viewBox="0 0 24 24" className={classes.subtaskIcon}>
                        <IconSubtask />
                      </SvgIcon>

                      <span className={classes.attrDetail}>
                        {item.totalSubTask > 99 ? 99 : item.totalSubTask}
                        {item.totalSubTask > 99 ? <sup>+</sup> : ""}
                      </span>
                    </div>
                  </CustomTooltip>
                )}
                {item.totalToDo > 0 && toDoListCount && (
                  <CustomTooltip helptext={getToDoList()} placement="bottom">
                    <div className={classNames(classes.ticketAttr, classes.footerDetail)}>
                      <CheckCircle className={classes.attrIcon} />
                      <span className={classes.attrDetail}>
                        {item.completedToDo}/{item.totalToDo}
                      </span>
                    </div>
                  </CustomTooltip>
                )}
                {item.totalIssues > 0 && issuesCount && (
                  <CustomTooltip helptext="Issue" placement="bottom">
                    <div className={classNames(classes.ticketAttr, classes.footerDetail)}>
                      <SvgIcon viewBox="0 0 24 24" style={{ fill: "#BFBFBF" }} fontSize="small">
                        <IssueIcon />
                      </SvgIcon>
                      <span className={classes.attrDetail}>
                        {item.totalIssues > 99 ? 99 : item.totalIssues}
                        {item.totalIssues > 99 ? <sup>+</sup> : ""}
                      </span>
                    </div>
                  </CustomTooltip>
                )}

                {item.totalRisk > 0 && risksCount && (
                  <CustomTooltip helptext="Risk" placement="bottom">
                    <div className={classNames(classes.ticketAttr, classes.footerDetail)}>
                      <SvgIcon viewBox="0 0 24 24" style={{ fill: "#BFBFBF" }} fontSize="small">
                        <RiskIcon />
                      </SvgIcon>
                      <span className={classes.attrDetail}>
                        {item.totalRisk > 99 ? 99 : item.totalRisk}
                        {item.totalRisk > 99 ? <sup>+</sup> : ""}
                      </span>
                    </div>
                  </CustomTooltip>
                )}
                {item.totalComments > 0 && commentsCount && (
                  <CustomTooltip helptext="Comments" placement="bottom">
                    <div className={classNames(classes.ticketAttr, classes.footerDetail)}>
                      <Comment className={classes.attrIcon} />
                      <span className={classes.attrDetail}>
                        {item.totalComments > 99 ? 99 : item.totalComments}
                        {item.totalComments > 99 ? <sup>+</sup> : ""}
                      </span>
                    </div>
                  </CustomTooltip>
                )}

                {item.totalMeetings > 0 && meetingsCount && (
                  <CustomTooltip helptext="Meetings" placement="bottom">
                    <div className={classNames(classes.ticketAttr, classes.footerDetail)}>
                      <CalendarToday className={classes.attrIcon} />
                      <span className={classes.attrDetail}>
                        {item.totalMeetings > 99 ? 99 : item.totalMeetings}
                        {item.totalMeetings > 99 ? <sup>+</sup> : ""}
                      </span>
                    </div>
                  </CustomTooltip>
                )}
                {item.totalAttachment > 0 && attachmentsCount && (
                  <CustomTooltip helptext={getAttachments()} placement="bottom">
                    <div className={classNames(classes.ticketAttr, classes.footerDetail)}>
                      <SvgIcon viewBox="0 0 24 24" style={{ fill: "#BFBFBF" }} fontSize="small">
                        <IconAttachFile />
                      </SvgIcon>
                      <span className={classes.attrDetail} style={{ marginRight: 9 }}>
                        {item.totalAttachment > 99 ? 99 : item.totalAttachment}
                        {item.totalAttachment > 99 ? <sup>+</sup> : ""}
                      </span>
                    </div>
                  </CustomTooltip>
                )}
              </div>
            )}
          </div>
        )}
      </Draggable>
      <FeaturedImgsModal
        open={openModal}
        setOpen={setOpenModal}
        setFeaturedImage={setFeaturedImage}
        data={item}
        element={element}
        saveFeatureImage={saveFeatureImage}
        saveLibraryImage={saveLibraryImage}
        projectId={boardData.projectId}
      />
      <DeleteConfirmDialog
        open={openDelete}
        closeAction={handleDialogClose}
        cancelBtnText="Cancel"
        successBtnText="Delete List Only"
        alignment="center"
        headingText="Delete Marketing List"
        successAction={handleDelete}
        btnQuery={""}
        deleteBtnText="Delete List & All Tasks"
        actionBtnCntProps={{
          align: "left",
          deleteBtnProps: {
            style: { margin: 5 },
          },
        }}>
        <Typography variant="h5" align={"center"}>
          <p>Looks like this list has one or multiple `${taskLabelSingle ? taskLabelSingle + '(s)' : 'task(s)'}`.</p>
          <p>
            Do you want to delete only this list or also delete all `${taskLabelSingle ? taskLabelSingle + '(s)' : 'task(s)'}` associated with this
            list?
          </p>
        </Typography>
        <NotificationMessage type="info" iconType="info">
          If you delete list only, all tasks associated with this list will be moved to the linked
          status list.
        </NotificationMessage>
      </DeleteConfirmDialog>

      <ActionConfirmation
        open={archiveTaskModal}
        closeAction={() => {
          setArchiveTaskModal(false);
        }}
        cancelBtnText={
          <FormattedMessage
            id="common.action.archive.confirmation.cancel-button.label"
            defaultMessage="Cancel"
          />
        }
        successBtnText={
          <FormattedMessage
            id="common.action.archive.confirmation.archive-button.label"
            defaultMessage="Archive"
          />
        }
        alignment="center"
        iconType="archive"
        headingText={
          <FormattedMessage
            id="common.action.archive.confirmation.title"
            defaultMessage="Archive"
          />
        }
        msgText={taskLabelSingle ? `Are you sure you want to delete this ${taskLabelSingle}?` :
          <FormattedMessage
            id="common.action.archive.confirmation.task.label"
            defaultMessage="Are you sure you want to archive this task?"
          />
        }
        successAction={handleArchiveTask}
      />

      <DeleteConfirmDialog
        open={deleteTaskModal}
        closeAction={() => {
          setDeleteTaskModal(false);
        }}
        cancelBtnText={
          <FormattedMessage
            id="common.action.delete.confirmation.cancel-button.label"
            defaultMessage="Cancel"
          />
        }
        successBtnText={
          <FormattedMessage
            id="common.action.delete.confirmation.delete-button.label"
            defaultMessage="Delete"
          />
        }
        alignment="center"
        headingText={
          <FormattedMessage id="common.action.delete.confirmation.title" defaultMessage="Delete" />
        }
        successAction={handleDeleteTask}
        msgText={taskLabelSingle ? `Are you sure you want to delete this ${taskLabelSingle}?` :
          <FormattedMessage
            id="common.action.delete.confirmation.task.label"
            defaultMessage="Are you sure you want to delete this task?"
          />
        }
      />
    </>
  );
};

Ticket.defaultProps = {
  index: 0,
  item: {},
  barIndex: 0,
  onUpdate: () => {},
  allMembers: [],
  handleClickListItem: () => {},
  onUpdateTaskColor: () => {},
  deleteTask: () => {},
  archiveTask: () => {},
  saveFeatureImage: () => {},
  saveLibraryImage: () => {},
  element: {},
  boardPer: {},
  boardData: {},
};

const mapStateToProps = (state, ownProps) => {
  return {
    allMembers: state.profile.data.member.allMembers,
    companyInfo: state.whiteLabelInfo.data,
  };
};

export default compose(
  injectIntl,
  withStyles(ticketStyles, { withTheme: true }),
  connect(mapStateToProps, {})
)(Ticket);
