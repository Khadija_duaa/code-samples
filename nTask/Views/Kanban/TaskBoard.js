// @flow

import { withStyles } from "@material-ui/core/styles";
import cloneDeep from "lodash/cloneDeep";
import React, { useEffect, useState } from "react";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import { Scrollbars } from "react-custom-scrollbars";
import { connect, useSelector } from "react-redux";
import { compose } from "redux";
import { teamCanView } from "../../components/PlanPermission/PlanPermission";
import { UpdateBoardTaskOrder, updateShowpill } from "../../redux/actions/boards";
import { calculateContentHeight } from "../../utils/common";
import taskBoardStyles from "./style.js";
import TaskBar from "./TaskBar";
import NewTask from "./TaskBar/NewTask";
import TaskStatusChangeDialog from "./TaskStatusChangeDialog";
import UnsortedColumn from "./UnsortedColumn";

const reorder = (list, startIndex, endIndex, boardPer) => {
  if (boardPer && boardPer.boardDetail.taskreorderInmultiList.cando) {
    const result = JSON.parse(JSON.stringify(list));
    const [removed] = result.tasksList.splice(startIndex, 1);
    result.tasksList.splice(endIndex, 0, removed);
    return result;
  }
};
/**
 *
 * @param {*} el
 */
const getMaxTaskLimit = (el) => {
  let arr = el.statusRulesList.filter(
    (item) => item.dynamicStatusRule && item.dynamicStatusRule.keyName == "MAXTASKLIMIT_RULE"
  );
  if (arr.length > 0 && arr[0].dynamicStatusRule) {
    return { maxTaskEnable: true, maxTaskLimit: parseInt(arr[0].dynamicStatusRule.keyValue) };
  }
  return { maxTaskEnable: false, maxTaskLimit: 0 };
};

/**
 * Moves an item from one list to another list.
 */
const move = (
  source,
  destination,
  droppableSource,
  droppableDestination,
  boardPer,
  showSnackBar
) => {
  const sourceClone = JSON.parse(JSON.stringify(source));
  const destClone = JSON.parse(JSON.stringify(destination));
  const maxTaskObj = getMaxTaskLimit(destClone);
  if (boardPer && boardPer.boardDetail.taskreorderInmultiList.cando) {
    if (
      !destClone.isDefault &&
      !destClone.isDoneState &&
      maxTaskObj.maxTaskEnable &&
      maxTaskObj.maxTaskLimit > 0 &&
      (maxTaskObj.maxTaskLimit == destClone.tasksList.length ||
        destClone.tasksList.length > maxTaskObj.maxTaskLimit)
    ) {
      showSnackBar(
        "You cannot move this task because destination column has been approached to its limit or you don't have a permission to moving task from one list to another!",
        "error"
      );
      return false;
    } else {
      /** if destination column isEnableMaxTaskLimit true && destination column max task limit of task is greater than already added task length */
      const [removed] = sourceClone.tasksList.splice(droppableSource.index, 1);
      destClone.tasksList.splice(droppableDestination.index, 0, removed);
      const result = {};
      result[droppableSource.droppableId] = sourceClone;
      result[droppableDestination.droppableId] = destClone;
      return result;
    }
  } else {
    showSnackBar(
      "You cannot move this task because destination column has been approached to its limit or you don't have a permission to moving task from one list to another!",
      "error"
    );
    return false;
  }
};
const moveUnsortedColumns = (
  source,
  destination,
  droppableSource,
  droppableDestination,
  showSnackBar,
  boardPer,
  currentWorkspace
) => {
  const sourceClone = JSON.parse(JSON.stringify(source));
  const destClone = JSON.parse(JSON.stringify(destination));
  const maxTaskObj = getMaxTaskLimit(destClone);

  if (destClone.isDoneState && currentWorkspace.config.isUserTasksEffortMandatory) {
    showSnackBar("Make sure you have added effort in the task", "info", {
      anchorOrigin: {
        vertical: "bottom",
        horizontal: "right",
      },
    });
  }
  if (boardPer && boardPer.boardDetail.addUnsortedTask.cando) {
    if (
      !destClone.isDefault &&
      !destClone.isDoneState &&
      maxTaskObj.maxTaskEnable &&
      (maxTaskObj.maxTaskLimit == destClone.tasksList.length ||
        destClone.tasksList.length > maxTaskObj.maxTaskLimit)
    ) {
      showSnackBar(
        "You cannot move this task because destination column has been approached to its limit or you don't have a permission to moving task from one list to another!",
        "error"
      );
      return false;
    } else {
      /** if destination column isEnableMaxTaskLimit true && destination column max task limit of task is greater than already added task length */
      const [removed] = sourceClone.splice(droppableSource.index, 1);
      if (removed.timeLogged && removed.timeLogged !== "00:00") {
        /** if unsorted task have logged time then do not move to any status  */
        showSnackBar(
          "Oops! You cannot assign a task to project once time has been logged against the task.",
          "error"
        );
        return;
      }
      destClone.tasksList.splice(droppableDestination.index, 0, {
        ...removed,
        ruleColors: [
          ...(removed?.ruleColors ?? []),
          {
            name: null,
            color: destination?.statusRulesList?.find(
              (rule) => rule.ruleTitle === "NEWADDEDTASK_RULE"
            )?.dynamicStatusRule?.keyValue,
          },
        ],
      });
      let result = [];
      result.push(sourceClone);
      result.push(destClone);
      return result;
    }
  } else {
    showSnackBar(
      "You cannot move this task because destination column has been approached to its limit or you don't have a permission to moving task from one list to another!",
      "error"
    );
    return false;
  }
};
const TaskBoard = (props) => {
  const {
    classes,
    dataSource,
    unsortedSource,
    projectId,
    boardId,
    contentHeight,
    saveKanbanListOrder,
    allTasks,
    handleClickListItem,
    onAddNewTask,
    onColumColorChange,
    onColumTitleChange,
    UpdateKanbanColumnSetting,
    openListSettingModal,
    taskDragDrop,
    quickFilters,
    loggedInUser,
    UpdateTask,
    onUpdateTaskColor,
    deleteTask,
    archiveTask,
    copyTask,
    forTemplatesSetColumnsInParent,
    saveTaskFeatureImage,
    saveTaskLibraryImage,
    addNewColumn,
    onDeleteStatus,
    onDuplicateStatus,
    boardPer,
    intl,
    subscriptionDetails,
    elementsArr,
    updateTaskInKanbanStore,
    addUnsortedTask,
    boardData,
    UpdateKanbanBoard,
    showSnackBar,
    UpdateBoardTaskOrder,
    updateTaskPositionsInStore,
    appliedFilters,
  } = props;
  let { paymentPlanTitle } = subscriptionDetails;
  const [data, setData] = useState([]);
  const [unsortedTasks, setUnsortedTask] = useState(unsortedSource);
  const [filter, setFilter] = useState(quickFilters);
  const [memberfilter, setMemberFilter] = useState({});
  const [unsortedTaskColumn, setUnsortedTaskColumn] = useState(false);
  const [column, setColumn] = useState(null);
  const [unsortedColumnPosition, setUnsortedColumnPosition] = useState(classes.normalPosition);
  const [unsortedColumnTop, setUnsortedColumnTop] = useState(0);
  const [openStatusDialog, setOpenStatusDialog] = useState(false);
  const [oldStatusItem, setoldStatusItem] = useState({});
  const profile = useSelector((state) => state.profile.data);
  const [showpill, setShowpill] = useState(
    boardData.listRuleLabelUsers?.some((userid) => userid === profile.userId)
  );
  const [newStatusItem, setnewStatusItem] = useState({});

  const changepill = (value) => {
    updateShowpill({ projectId: boardData.projectId, labelState: showpill });
    setShowpill(value);
  };

  useEffect(() => {
    setFilter(quickFilters);
    setMemberFilter(appliedFilters);
  }, [props.quickFilters, props.appliedFilters]);

  useEffect(() => {
    settingColumnsInState(dataSource);
    return () => { };
  }, [props.dataSource]);

  useEffect(() => {
    settingColumnsInState(dataSource);
  }, [props.elementsArr]);

  const onDragEnd = (result) => {
    const { source, destination, draggableId, type } = result;
    // dropped outside the list
    if (!destination) return;


    const { loggedInTeam, workspace } = props.profileState.data;
    const currentWorkspace = workspace.find(t => t.teamId === loggedInTeam);

    // If TaskBar - column is dragged
    if (type === "column") {
      if (boardPer && boardPer.boardDetail.listreorder.cando && teamCanView("customStatusAccess")) {
        const newState = JSON.parse(JSON.stringify(data));
        const moveCol = newState.splice(result.source.index, 1);
        newState.splice(destination.index, 0, moveCol[0]);
        settingColumnsInState(newState);
        if (newState.length > 0 && !boardData.isTemplate) saveKanbanListOrder(newState);
      }
      return;
    }
    if (source.droppableId == "unsortedId") {
      const sInd = source.droppableId;
      const dInd = destination.droppableId;
      if (sInd === dInd) {
        // const items = reorder(unsortedTasks, source.index, destination.index);
        // const newState = items;
        // taskDragDrop(items); /** calling api for updating the column */
        // settingUnsortedColumnsInState(newState);
      } else {
        // Ticket is dragged through the different taskbar
        const r = moveUnsortedColumns(
          unsortedTasks,
          data[dInd],
          source,
          destination,
          showSnackBar,
          boardPer,
          currentWorkspace
        );
        if (r) {
          let destinationColum = AddDraggedTask(
            r[1],
            unsortedTasks[source.index].taskId
          ); /** update dragged task in destination colum with respect to its destination column list setting and then setting the state */

          destinationColum = updatingTaskPositions(destinationColum);
          let stateBoards = cloneDeep(data);
          stateBoards = stateBoards.map((c) => {
            if (c.id == destinationColum.id) return destinationColum;
            else return c;
          });
          setData(stateBoards);
          // taskDragDrop(destinationColum); /** calling api for updating the column */
          setUnsortedTask(r[0]);
        }
      }
      return;
    }

    const sInd = +source.droppableId;
    const dInd = +destination.droppableId;
    // Ticket is dragged through the same taskbar
    if (sInd === dInd) {
      let items = reorder(data[sInd], source.index, destination.index, boardPer);
      let orderData = { projectId: projectId, items: [] };
      items.tasksList = items.tasksList.map((task, index) => {
        let obj = {};
        obj.itemId = task.taskId;
        obj.position = index;
        task.position = index;
        orderData.items.push(obj);
        return task;
      });
      const finalItems = items;
      const newState = [...data];
      newState[sInd] = finalItems;
      updateTaskPositionsInStore(orderData.items);
      // taskDragDrop(items); /** calling api for updating the column */
      //settingColumnsInState(newState);
      // let orderData = {projectId : projectId, items: []};
      // orderData.items = items.tasksList.map((m,index) =>  {
      //   let obj = {};
      //   obj.itemId = m.taskId;
      //   obj.position = index;
      //   return obj;
      // });
      // data.projectId =
      //calling api for ordering tasks
      UpdateBoardTaskOrder(orderData, (succ) => {
        //reOrderTasksinColumnsInState(newState);
      });
      reOrderTasksinColumnsInState(newState);
    } else {
      // Ticket is dragged through the different taskbar
      const result = move(data[sInd], data[dInd], source, destination, boardPer, showSnackBar);

      const { loggedInTeam, workspace } = props.profileState.data;
      const currentWorkspace = workspace.find(t => t.teamId === loggedInTeam);

      if (data[dInd].isDoneState && currentWorkspace.config.isUserTasksEffortMandatory) {
        showSnackBar("Make sure you have added effort in the task", "info", {
          anchorOrigin: {
            vertical: "bottom",
            horizontal: "right",
          },
        });
      }

      const newState = [...data];
      if (result) {
        newState[sInd] = result[sInd];
        newState[dInd] = updatingTaskPositions(result[dInd]);
        let destinationColum = updateDraggedTask(
          newState[dInd],
          draggableId
        ); /** update dragged task in destination colum with respect to its destination column list setting and then setting the state */

        setData(newState);
        if (!boardData.isTemplate) {
          taskDragDrop(draggableId, destinationColum, {
            items: returnTaskPosition(destinationColum),
            projectId,
          });
        }
      }
    }
  };
  const updateDraggedTask = (column, id) => {
    return updateTaskKeys(column, id);
  };

  const returnTaskPosition = (column) => {
    let updatedColum = cloneDeep(column);
    let items = [];
    updatedColum.tasksList.map((c, index) => {
      items.push({
        itemId: c.taskId,
        position: index,
      });
    });
    return items;
  };

  const updatingTaskPositions = (column) => {
    let updatedColum = cloneDeep(column);
    let items = [];
    updatedColum.tasksList = updatedColum.tasksList.map((c, index) => {
      c.taskPosition = index;
      items.push({
        itemId: c.taskId,
        position: index,
      });
      return c;
    });
    updateTaskPositionsInStore(items);
    return updatedColum;
  };

  const AddDraggedTask = (column, id) => {
    let updatedColumn = cloneDeep(column);
    updatedColumn.tasksList = updatedColumn.tasksList.map((t, index) => {
      if (t.taskId == id) {
        t.statusId = updatedColumn.statusId;
        t.statusColorCode = updatedColumn.statusColor;
        t.statusName = updatedColumn.statusTitle;
        t.projectId = projectId;
        t.taskPosition = index;
        props.addUnsortedTask(t, column);
        return t;
      } else {
        t.taskPosition = index;
        return t;
      }
    });
    return updatedColumn;
  };

  const updateTaskKeys = (column, id) => {
    let updatedColumn = cloneDeep(column);
    updatedColumn.tasksList = updatedColumn.tasksList.map((t, index) => {
      if (t.taskId == id) {
        t.statusId = updatedColumn.statusId;
        updateTaskInKanbanStore(t);
        return t;
      } else return t;
    });

    return updatedColumn;
  };

  const updateGlobalTask = (column, id, actualEDate = null) => {
    if (!column.isTemplate) {
      /** updating the priority and status of global task object  */
      let updatedTask =
        allTasks.find((t) => t.taskId == id) || {}; /** updating color in store also */
      if (updatedTask.projectId == null || updatedTask.projectId == undefined)
        updatedTask.projectId = projectId;
      if (column.isEnablePriority) {
        updatedTask.priority = column.defaultPriority;
      }
      if (actualEDate) updatedTask.actualDueDateString = actualEDate;
      switch (column.listName) {
        case "Not Started": {
          updatedTask.status = 0;
          break;
        }
        case "In Progress": {
          updatedTask.status = 1;
          break;
        }
        case "In Review": {
          updatedTask.status = 2;
          break;
        }
        case "Cancelled": {
          updatedTask.status = 4;
          break;
        }
        case "Completed": {
          updatedTask.status = 3;
          break;
        }
      }
      UpdateTask(updatedTask, (callback) => { });
    }
  };

  const handleUpdateFromTicket = (item) => {
    const tasks = { ...data };
    const ind = tasks.findIndex((element) => element.id === item.id);
    tickets.splice(ind, 1, item);
    settingColumnsInState(tasks);
  };

  const onAddNewTicket = (data, title, callback = () => { }) => {
    // const newData = data.map((el) => {
    //   if (el.id === taskId) {
    //     const newTickets = JSON.parse(JSON.stringify(el.tickets)).reverse().concat({
    //       id: `item-${taskId}-${el.tickets.length + 1}`,
    //       content: title,
    //     });
    //     return {
    //       ...el,
    //       tickets: newTickets.reverse(),
    //     };
    //   } else {
    //     return el;
    //   }
    // });
    // settingColumnsInState(newData);
    onAddNewTask(data, title, callback); /** adding new task in the list */
  };
  const randomColor = () => {
    let color =
      "rgb(" +
      Math.round(Math.random() * 220) +
      "," +
      Math.round(Math.random() * 210) +
      "," +
      Math.round(Math.random() * 220) +
      ")";
    return RGBToHex(color);
  };
  const RGBToHex = (rgb) => {
    // Choose correct separator
    let sep = rgb.indexOf(",") > -1 ? "," : " ";
    // Turn "rgb(r,g,b)" into [r,g,b]
    rgb = rgb.substr(4).split(")")[0].split(sep);

    let r = (+rgb[0]).toString(16),
      g = (+rgb[1]).toString(16),
      b = (+rgb[2]).toString(16);

    if (r.length == 1) r = "0" + r;
    if (g.length == 1) g = "0" + g;
    if (b.length == 1) b = "0" + b;

    return "#" + r + g + b;
  };
  const onAddTask = (title) => {
    let obj = {
      projectId: projectId,
      StatusTitle: title,
      StatusColor: randomColor(),
    };
    props.addNewColumn(obj);
  };
  const DeleteStatus = (object, callback = () => { }) => {
    if (!object.isDefault && !object.isDoneState && object.tasksList.length == 0) {
      let obj = {
        projectId: projectId,
        StatusId: object.statusId,
        IsDefault: object.isDefault,
        isDoneState: object.isDoneState,
        isDeleteTask: true,
        toStatusId: null,
      };
      props.onDeleteStatus(obj);
      callback();
    } else {
      let template = cloneDeep(boardData);
      template.statusList = template.statusList.filter((s) => s.statusId !== object.statusId);
      setnewStatusItem(template);
      setoldStatusItem(object);
      setOpenStatusDialog(true);
    }
  };
  const handleDeleteStatus = (object) => {
    props.onDeleteStatus(object);
    handleCloseDialogStatusDialog();
  };
  const DuplicateStatus = (object, succ) => {
    let obj = {
      projectId: projectId,
      StatusTitle: object.statusTitle,
      StatusCode: object.statusCode,
      StatusColor: object.statusColor,
      StatusId: object.statusId,
      OrderId: null,
      IsDefault: object.isDefault,
      IsDoneState: object.isDoneState,
    };
    props.onDuplicateStatus(obj, succ);
  };
  const handleUpdateTaskColor = (color, item, element) => {
    let column = cloneDeep(element);
    column.tasksList = column.tasksList.map((t) => {
      if (t.taskId == item.taskId) {
        t.colorCode = color;
        return t;
      } else return t;
    });
    let stateBoards = cloneDeep(data);
    stateBoards = stateBoards.map((c) => {
      if (c.id == column.id) return column;
      else return c;
    });
    settingColumnsInState(stateBoards);
    onUpdateTaskColor(color, item);
  };

  const filterTaskFromColumn = (selectedTask, element) => {
    let column = cloneDeep(element);
    column.tasksList = column.tasksList.filter((t) => t.taskId !== selectedTask.taskId);

    let stateBoards = cloneDeep(data);
    stateBoards = stateBoards.map((c) => {
      if (c.id == column.id) return column;
      else return c;
    });
    settingColumnsInState(stateBoards);
  };

  const deleteTaskAction = (deletedTask, callBack, element) => {
    filterTaskFromColumn(deletedTask, element);
    deleteTask(deletedTask, callBack);
  };
  const archiveTaskAction = (archivedTask, callBack, element) => {
    filterTaskFromColumn(archivedTask, element);
    archiveTask(archivedTask, callBack);
  };
  const handleCopyTask = (copiedTask, column) => {
    // let notStartedColumn = data.find(c => c.listName == "Not Started");
    copyTask(copiedTask, column);
  };
  const handleUnsortedOpener = (column) => {
    setColumn(column);
    setUnsortedTaskColumn(true);
  };
  const handleUnsortedClose = () => {
    setUnsortedTaskColumn(false);
  };
  const reOrderTasksinColumnsInState = (columns) => {
    //call api to reorder positions by the user
    setData(columns);
    forTemplatesSetColumnsInParent(columns);
  };
  const settingColumnsInState = (columns) => {
    let updatedColumns = columns.map((ele) => {
      ele.tasksList = elementsArr.filter((e) => e.statusId == ele.statusId);
      ele.tasksList = sortingTask(ele.tasksList);
      return ele;
    });
    setData(updatedColumns);
    if (column) setColumn(updatedColumns.find((e) => e.id == column.id) || null);
    forTemplatesSetColumnsInParent(updatedColumns);
  };

  const sortingTask = (tasksList) => {
    return tasksList.sort(function (a, b) {
      return a.taskPosition - b.taskPosition;
    });
  };

  const settingUnsortedColumnsInState = (columns) => {
    setUnsortedTask(columns);
    forTemplatesSetColumnsInParent(columns);
  };
  const saveFeatureImage = (data, task, column, callBack) => {
    if (data.docsPath) {
      /** for creating/adding new feature image */
      const postData = {
        fileId: data.docsPath[0].fileId,
        fileName: data.docsName,
        fileSize: data.fileSize,
        fileExtension: data.docsPath[0].fileExtension,
        fileType: data.docsType,
        publicURL: data.docsPath[0].publicURL,
        taskId: task.taskId,
      };
      saveTaskFeatureImage(postData, task, column, callBack);
    }
    if (!data.docsPath) {
      /** for deleting the feature image url */
      const postData = {
        publicURL: "",
        taskId: task.taskId,
      };
      saveTaskFeatureImage(postData, task, column, callBack);
    }
  };

  const saveLibraryImage = (data, task, column, callBack) => {
    saveTaskLibraryImage(data, task, column, callBack);
  };

  const addUnsortedTaskInStateList = (task, col) => {
    const { loggedInTeam, workspace } = props.profileState.data;
    const currentWorkspace = workspace.find(t => t.teamId === loggedInTeam);

    if (col) {
      let columnMaxLimit =
        col.statusRulesList.find((s) => s.ruleTitle == "MAXTASKLIMIT_RULE") || false;
      if (task.timeLogged && task.timeLogged !== "00:00") {
        /** if unsorted task have logged time then do not move to any status  */
        showSnackBar(
          "Oops! You cannot assign a task to project once time has been logged against the task.",
          "error"
        );
        return;
      }
      if (columnMaxLimit && boardPer && boardPer.boardDetail.addUnsortedTask.cando) {
        if (
          !columnMaxLimit.dynamicStatusRule ||
          columnMaxLimit.dynamicStatusRule.keyValue > col.tasksList.length
        ) {
          const tasks = unsortedTasks.filter(function (element) {
            return element.taskId !== task.taskId;
          });
          let stateBoards = cloneDeep(data);

          task.statusId = col.statusId;
          task.projectId = projectId;
          task.ruleColors = [
            ...(task?.ruleColors ?? []),
            {
              name: null,
              color: col?.statusRulesList?.find(
                (rule) => rule.ruleTitle === "NEWADDEDTASK_RULE"
              )?.dynamicStatusRule?.keyValue,
            },
          ];
          elementsArr.push(task);
          setUnsortedTask(tasks);
          settingColumnsInState(stateBoards);
          taskDragDrop(task.taskId, col);
          if (col.isDoneState && currentWorkspace.config.isUserTasksEffortMandatory) {
            showSnackBar("Make sure you have added effort in the task", "info", {
              anchorOrigin: {
                vertical: "bottom",
                horizontal: "right",
              },
            });
          }

        } else {
          showSnackBar(
            "You cannot move this task because destination column has been approached to its limit!",
            "error"
          );
        }
      }
    }
  };
  const scrollToLeftHandler = (event) => {
    if (event.scrollLeft > 0) {
      setUnsortedColumnPosition(classes.fixedPosition);
    } else setUnsortedColumnPosition(classes.normalPosition);
    setUnsortedColumnTop(event.scrollTop + 165);
  };
  const handleCloseDialogStatusDialog = () => {
    setOpenStatusDialog(false);
  };
  return (
    <div className={classes.container} style={{ height: calculateContentHeight() }}>
      <Scrollbars autoHide onScrollFrame={scrollToLeftHandler}>
        <div className={classes.taskContainer}>
          <div style={{ display: "flex" }}>
            <DragDropContext onDragEnd={onDragEnd}>
              <Droppable droppableId="all-columns" direction="horizontal" type="column">
                {(provided) => (
                  <div
                    {...provided.droppableProps}
                    style={{ display: "flex" }}
                    ref={provided.innerRef}>
                    {unsortedTaskColumn && (
                      <UnsortedColumn
                        onAdd={onAddTask}
                        contentHeight={contentHeight}
                        handleUnsortedClose={handleUnsortedClose}
                        barIndex={data.length + 1}
                        unsortedId="unsortedId"
                        unsortedTaskList={unsortedTasks}
                        intl={intl}
                        showSnackBar={showSnackBar}
                        selectedColumn={column}
                        addUnsortedTaskInStateList={addUnsortedTaskInStateList}
                        positionClass={unsortedColumnPosition}
                        scrollTop={unsortedColumnTop}
                      />
                    )}
                    {data.length > 0 &&
                      data.map((el, index) => (
                        <TaskBar
                          showpill={showpill}
                          setShowpill={changepill}
                          key={`bar-${index}`}
                          barIndex={index}
                          el={el}
                          onUpdate={handleUpdateFromTicket}
                          onAdd={onAddNewTicket}
                          contentHeight={contentHeight}
                          allTasks={allTasks}
                          handleClickListItem={handleClickListItem}
                          onColumColorChange={onColumColorChange}
                          onColumTitleChange={onColumTitleChange}
                          UpdateKanbanColumnSetting={UpdateKanbanColumnSetting}
                          openListSettingModal={openListSettingModal}
                          filter={filter}
                          loggedInUser={loggedInUser}
                          onUpdateTaskColor={handleUpdateTaskColor}
                          deleteTask={deleteTaskAction}
                          archiveTask={archiveTaskAction}
                          copyTask={handleCopyTask}
                          handleUnsortedOpener={(e) => handleUnsortedOpener(el)}
                          saveFeatureImage={saveFeatureImage}
                          saveLibraryImage={saveLibraryImage}
                          intl={intl}
                          boardPer={boardPer}
                          onDeleteStatus={DeleteStatus}
                          onDuplicateStatus={DuplicateStatus}
                          boardData={boardData}
                          UpdateKanbanBoard={UpdateKanbanBoard}
                          showSnackBar={showSnackBar}
                          appliedFilters={memberfilter}
                          handleDisable={openStatusDialog}
                        />
                      ))}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
            </DragDropContext>
            {boardPer &&
              boardPer.boardDetail.addNewStatus.cando &&
              !boardData.isTemplate &&
              teamCanView("customStatusAccess") && (
                <NewTask
                  onAdd={onAddTask}
                  boardData={boardData}
                  contentHeight={contentHeight}
                  showSnackBar={showSnackBar}
                />
              )}
          </div>
        </div>
      </Scrollbars>
      {openStatusDialog && (
        <TaskStatusChangeDialog
          open={openStatusDialog}
          handleClose={handleCloseDialogStatusDialog}
          oldStatus={oldStatusItem}
          newTemplateItem={newStatusItem}
          handleSaveAsTemplate={handleDeleteStatus}
          showSnackBar={showSnackBar}
        />
      )}
    </div>
  );
};

TaskBoard.defaultProps = {
  dataSource: [],
  saveKanbanListOrder: () => { },
  allTasks: [],
  handleClickListItem: () => { },
  onAddNewTask: () => { },
  onColumColorChange: () => { },
  onColumTitleChange: () => { },
  UpdateKanbanColumnSetting: () => { },
  openListSettingModal: () => { },
  taskDragDrop: () => { },
  onUpdateTaskColor: () => { },
  UpdateTask: () => { },
  deleteTask: () => { },
  archiveTask: () => { },
  copyTask: () => { },
  forTemplatesSetColumnsInParent: () => { },
  saveTaskFeatureImage: () => { },
  saveTaskLibraryImage: () => { },
  addNewColumn: () => { },
  onDeleteStatus: () => { },
  onDuplicateStatus: () => { },
  updateTaskInKanbanStore: () => { },
  addUnsortedTask: () => { },
  showSnackBar: () => { },
  updateTaskPositionsInStore: () => { },
  quickFilters: [],
  loggedInUser: {},
  boardPer: null,
  subscriptionDetails: {},
  elementsArr: [],
  boardId: "",
  projectId: "",
};
const mapStateToProps = (state) => {
  return {
    profileState: state.profile,
    subscriptionDetails: state.profile.data.teams.find(
      (item) => item.companyId == state.profile.data.activeTeam
    ).subscriptionDetails,
    appliedFilters: state.appliedFilters,
  };
};
export default compose(
  withStyles(taskBoardStyles, { withTheme: true }),
  connect(mapStateToProps, { UpdateBoardTaskOrder })
)(TaskBoard);
