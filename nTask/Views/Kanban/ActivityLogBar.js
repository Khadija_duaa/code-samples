// @flow

import React, { useEffect } from "react";
import CustomDrawer from "../../components/Drawer/KanbanDrawer";
import ActivityLogItem from "./ActivityLogItem";

type ActivityLogBarProps = {
  setOpen: Function,
  handleClickActivityLogItem: Function,
  open: Boolean,
  title: String,
  data: Array<Object>,
};

const ActivityLogBar = (props: ActivityLogBarProps) => {
  const { open, setOpen, title, data, handleClickActivityLogItem, allMembers, handleClearActivityLog } = props;
  const handleClose = () => {
    setOpen(false);
    handleClearActivityLog();
  };
  useEffect(() => {
    return () => {
      handleClose();
    };
  }, []);

  return (
    <CustomDrawer open={open} closeDrawer={handleClose} headerTitle={title}>
      <div style={{height: "100%"}}>
        {!data ? (
          <div className="loaderContainer">
            <div className="loader"></div>
          </div>
        ) : (
          data.map((item, key) => {
            let selectedMember = allMembers.find(ele => ele.userId == item.userId) || null;
            return (
              <ActivityLogItem
                key={`log-${key}`}
                data={item}
                handleClickActivityLogItem={handleClickActivityLogItem}
                selectedMember={selectedMember}
              />
            );
          })
        )}
      </div>
    </CustomDrawer>
  );
};

ActivityLogBar.defaultProps = {
  open: false,
  setOpen: () => {},
  title: "",
  data: [],
  allMembers: [],
  handleClickActivityLogItem: () => {},
};
export default ActivityLogBar;
