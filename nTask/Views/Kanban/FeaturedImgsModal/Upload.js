import React, { useState, useEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./styles";
import SvgIcon from "@material-ui/core/SvgIcon";
import GoogleDrive from "../../../components/Icons/GoogleDrive";
import OneDrive from "../../../components/Icons/OneDrive";
import Dropbox from "../../../components/Icons/Dropbox";
import Dropzone from "react-dropzone";
import Typography from "@material-ui/core/Typography";
import { calculateHeight } from "../../../utils/common";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import LaptopMacIcon from "@material-ui/icons/LaptopMac";
import RemoveIcon2 from "../../../components/Icons/RemoveIcon2";
import CustomTooltip from "../../../components/Tooltip/Tooltip";
import CustomIconButton from "../../../components/Buttons/CustomIconButton";

type UploadkProps = {
  classes: Object,
  theme: Object,
  data: Object,
  projectId: String,
  setAttachment: Function,
  setProperties: Function,
  setBtnQuery: Function,
};

const imageMaxSize = 5242880; //bytes
const acceptedFileType = "image/x-png, image/png, image/jpg, image/jpeg, image/gif";
const acceptedFileTypesArray = acceptedFileType.split(",").map(item => {
  return item.trim();
});

function Upload(props: UploadProps) {
  const {
    classes,
    theme,
    setAttachment,
    attachment,
    data,
    profileState,
    setProperties,
    setBtnQuery,
    intl,
    projectId
  } = props;
  const [imgSrc, setImgSrc] = useState(null);

  const setAttachmentInState = files => {
    var newData = new FormData();
    newData.append("File", files[0]);
    newData.append("taskid", data.taskId);
    newData.append("userid", profileState.userId);
    newData.append("projectId", projectId);
    setAttachment(newData);
    let properties = {
      docsType: files[0].type,
      docsName: files[0].name,
      isTaskFile: false,
      fileSize: files[0].size,
    };
    setProperties(properties);
  };

  const onhandleDrop = (files, rejectedFiles) => {
    if (rejectedFiles && rejectedFiles.length > 0) {
      verifyFile(rejectedFiles);
    }

    if (files && files.length > 0) {
      const isVerified = verifyFile(files);

      if (isVerified) {
        const currentFile = files[0];
        const myFileItemReader = new FileReader();
        myFileItemReader.addEventListener(
          "load",
          () => {
            setImgSrc(myFileItemReader.result);
          },
          false
        );

        myFileItemReader.readAsDataURL(currentFile);

        setAttachmentInState(files);
      }
    }
  };

  const verifyFile = files => {
    if (files && files.length > 0) {
      const currentFile = files[0];
      let error = '';
      // const currentFileType = currentFile.type;
      // const currentFileSize = currentFile.size;
      // if (currentFile.hasOwnProperty("errors") && )
      // if (currentFileSize.has) {
      //   alert("This file is not allowed. " + currentFileSize + " bytes is too large");
      //   return false;
      // }
      // if (!acceptedFileTypesArray.includes(currentFileType)) {
      //   alert("This file is not allowed. Only images are allowed.");
      //   return false;
      //}
      if (currentFile.hasOwnProperty("errors")) {
        currentFile.errors.forEach((err) => {
          if (err.code === "file-too-large") {
            error = "Maximum upload image size : 5mb";
          }
          if (err.code === "file-invalid-type") {
            error = "File type not supported";
          }
        });
      }
      if (error != "") {
        alert(error);
        return false;
      }
      return true;
    }
  };

  const dndActiveStyle = {
    borderColor: theme.palette.border.darkBorder,
  };
  const dndStyles = {
    position: "relative",
    width: "100%",
    borderWidth: 2,
    borderColor: theme.palette.border.lightBorder,
    borderStyle: "dashed",
    borderRadius: 5,
    padding: 50,
    background: theme.palette.background.paper,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginBottom: 30,
  };

  return (
    <>
      <div
        className={classes.uploadImageCnt}
      // style={{ height: calculateHeight() - 450 }}
      >
        {imgSrc ? (
          <div style={{ display: "flex" }}>
            {" "}
            <img src={imgSrc} style={{ maxWidth: 300, maxHeight: 300 }} />{" "}
            <CustomTooltip
              helptext={intl.formatMessage({
                id: "board.all-boards.board-actions-tooltips.delete",
                defaultMessage: "Delete",
              })}
              iconType="help"
              placement="top"
              style={{ color: theme.palette.common.white }}>
              <CustomIconButton
                onClick={event => {
                  setImgSrc(null);
                }}
                btnType="transparent"
                variant="contained"
                style={{
                  width: 26,
                  height: 26,
                  borderRadius: 4,
                  color: "#de133e",
                  marginLeft: 10,
                }}>
                <SvgIcon viewBox="0 0 24 24" className={classes.RemoveIcon2}>
                  <RemoveIcon2 />
                </SvgIcon>
              </CustomIconButton>
            </CustomTooltip>
          </div>
        ) : (
          <Dropzone
            onDrop={onhandleDrop}
            onDragOver={() => { }}
            onDropRejected={() => { }}
            accept={acceptedFileType}
            activeStyle={dndActiveStyle}
            style={dndStyles}
            maxSize={imageMaxSize}
            // disableClick
            onClick={evt => evt.preventDefault()}>
            {({ getRootProps, getInputProps, open }) => {
              return (
                <>
                  <div style={{ textAlign: "center", width: "100%" }}>
                    <div
                      {...getRootProps()}
                      onClick={e => e.stopPropagation()}
                      style={{ border: "none", marginRight: 20, width: "100%" }}>
                      <CloudUploadIcon className={classes.uploadIcon} />
                      <Typography
                        className={classes.fontSizeHead}
                        variant="h3"
                        style={{
                          marginTop: 10,
                          fontFamily: theme.typography.fontFamilyLato,
                          fontWeight: 800,
                          fontSize: "20px",
                          color: "#202020",
                        }}>
                        {intl.formatMessage({
                          id: "common.import-dialog.message",
                          defaultMessage: "Drag and drop an image",
                        })}
                      </Typography>

                      <Typography
                        variant="h3"
                        className={classes.fontSize}
                        style={{
                          marginTop: 30,
                          marginBottom: 30,
                          fontFamily: theme.typography.fontFamilyLato,
                          fontWeight: 400,
                          fontSize: "14px",
                          color: "#7E7E7E",
                        }}>
                        {intl.formatMessage({
                          id: "common.import-dialog.uploadfrom",
                          defaultMessage: "or upload from",
                        })}
                      </Typography>
                      <input {...getInputProps()} />
                      <div className={classes.IconCnt} onClick={() => open()}>
                        <LaptopMacIcon className={classes.laptopIcon} />
                      </div>
                    </div>
                    {/* <div className={classes.IconCnt}>
                      <SvgIcon viewBox="0 0 48 48" className={classes.laptopIcon}>
                        <GoogleDrive />
                      </SvgIcon>
                    </div>
                    <div className={classes.IconCnt}>
                      <SvgIcon viewBox="0 0 48 48" className={classes.laptopIcon}>
                        <OneDrive />
                      </SvgIcon>
                    </div>
                    <div className={classes.IconCnt}>
                      <SvgIcon viewBox="0 0 48 48" className={classes.laptopIcon}>
                        <Dropbox />
                      </SvgIcon>
                    </div> */}
                  </div>
                </>
              );
            }}
          </Dropzone>
        )}
      </div>
    </>
  );
}

Upload.defaultProps = {
  classes: {},
  theme: {},
  data: {},
  profileState: {},
  setProperties: () => { },
  setBtnQuery: () => { },
  intl: {},
  projectId: null,
};
const mapStateToProps = state => {
  return {
    profileState: state.profile.data,
  };
};

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, {})
)(Upload);
