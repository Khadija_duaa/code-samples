const styles = (theme) => ({
  dialogContent: {
    padding: "5px 15px",
  },
  tileContainer: {
    padding: "5.5px 0px",
    display: "flex",
    flexWrap: "wrap",
  },
  tileContent: {
    margin: 5,
    border: "1px solid #EAEAEA",
    backgroundColor: "#F6F6F6",
    borderRadius: 4,
  },
  contentClicked: {
    border: "1px solid #35C766",
    background: "#F1FFF5 0% 0% no-repeat padding-box",
    boxShadow: "0px -1px 0px #EAEAEA",
    borderRadius: "0px 0px 4px 4px",
  },
  tileBody: {
    // width: 120,
    // height: 120,
    backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        width: "162px",
        minHeight: 105,
        borderRadius: "4px 4px 0 0",
  },
  tileImage: {
    height: "100%",
    width: "100%"
  },
  tileTitle: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: "8.5px 5.5px",
    fontSize: "12px !important",
    fontFamily: "Lato, sans-serif",
    height: 30
  },
  clicked: {
    padding: "3px 5.5px",
  },
  toggleContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
    margin: "10px 0 5px 5px",
  },
  toggleBtnGroup: {
    borderRadius: 4,
    background: theme.palette.common.white,
    border: "none",
    boxShadow: "none",
    "& $toggleButtonSelected": {
      color: "#333333",
      backgroundColor: "#ffffff",
      "&:after": {
        background: theme.palette.common.white,
      },
      "&:hover": {
        color: "#333333",
        backgroundColor: "#ffffff",
      },
    },
  },
  toggleButton: {
    height: "auto",
    padding: "5px 28px",
    fontSize: "12px !important",
    textTransform: "capitalize",
    color: "#7E7E7E",
    backgroundColor: "#ffffff",
    border: "1px solid #EAEAEA",
    "&:hover": {
      background: theme.palette.background.medium,
    },
    "&[value = 'left']": {
      borderRight: `2px solid ${theme.palette.common.white}`,
    },
  },
  toggleButtonSelected: {},
  uploadImageCnt: {
    border: "1px solid lightgray",
    borderStyle: "dashed",
    borderRadius: 8,
    marginBottom: 15,
    marginTop: 15,
    height: "330px",

    padding: "30px 30px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
  },
  csvConfirmationIconCnt: {
    border: `2px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "50%",
    width: 110,
    height: 110,
    paddingLeft: 29,
    paddingTop: 29,
    marginBottom: 20,
    background: theme.palette.background.paper,
  },
  excelIconCnt: {
    width: 110,
    border: "2px solid rgba(221, 221, 221, 1)",
    height: 110,
    background: "rgba(251, 251, 251, 1)",
    paddingTop: 29,
    margin: "0 auto",
    borderRadius: "50%",
  },
  archieveConfirmationIcon: {
    fontSize: "52px !important",
  },
  dragnDropMessageText: {
    color: theme.palette.text.secondary,
    fontWeight: theme.typography.fontWeightLight,
    fontFamily: theme.typography.fontFamilyLato,
    margin: "10px 0 0 0",
    fontSize: "15px !important",
  },
  browseText: {
    // color: theme.palette.primary.light,
    color: theme.palette.border.blue,
    cursor: "pointer",
  },
  uploadIcon: {
    fontSize: "70px !important",
    color: "#BFBFBF",
  },
  IconCnt: {
    height: 60,
    width: 60,
    border: "1px solid #DDDDDD",
    borderRadius: "50%",
    display: "inline-block",
    cursor: "pointer",
    margin: "0px 10px",
    "&:hover": {
      border: `1px solid ${theme.palette.border.blue}`,
    },
  },
  laptopIcon: {
    fontSize: "35px !important",
    color: "#0090ff",
    marginTop: 11,
  },
   RemoveIcon2: {
    fontSize: "22px !important",
    // color: theme.palette.text.danger,
  },
  creditTitle: {
    fontSize: "12px !important",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
    color: "#7E7E7E",
    marginRight: 15,
  },
  creditCnt: {
    height: 16,
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-end",
  }
});
export default styles;
