// @flow

import React, { useState, useEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import CustomDialog from "../../../components/Dialog/CustomDialog";
import styles from "./styles.js";
import { withStyles } from "@material-ui/core/styles";
import Tile from "./Tile";
import CheckCircleOutline from "@material-ui/icons/CheckCircleOutline";
import Upload from "./Upload";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import ButtonActionsCnt from "../../../components/Dialog/ConfirmationDialogs/ButtonActionsCnt";
import { attachFeatureImg } from "../../../redux/actions/projects";
import classNames from "classnames";
import { FormattedMessage, injectIntl } from "react-intl";

type FeaturedImgsModalProps = {
  open: Boolean,
  setOpen: Function,
  attachFeatureImg: Function,
  saveFeatureImage: Function,
  saveLibraryImage: Function,
  classes: Object,
  data: Object,
  element: Object,
  projectId: String,
};
const FeaturedImgsModal = (props: FeaturedImgsModalProps) => {
  const {
    open,
    setOpen,
    classes,
    theme,
    setFeaturedImage,
    data,
    attachFeatureImg,
    element,
    saveFeatureImage,
    saveLibraryImage,
    intl,
    projectId
  } = props;
  const [uploadTab, setUploadTab] = useState("upload");
  const [attachment, setAttachment] = useState(null);
  const [libAttachment, setLibAttachment] = useState(null);
  const [properties, setProperties] = useState(null);
  const [btnQuery, setBtnQuery] = useState(false);
  const [value, setValue] = useState("");
  const [searchResults, setSearchResults] = useState([]);

  const randomNames = ["bussiness", "brand", "books", "office", "coding" , "software", "Design", "Education", "Marketing", "human resource", "sales", "customer service", "Development", "health", "finance", "Budget" , "project", "team building", "target planning", "money markets"];

  const fetchImages = () => {
    let randomNumber = Math.floor(Math.random()*randomNames.length);
    let searchText = randomNames[randomNumber];
    fetch(
      // `https://api.unsplash.com/photos?client_id=dwLptGju37-qTBvddj3f-ihKzgzdhtnL65ElFj0UBIU&query=bussiness&orientation=landscape&per_page=10`
      `https://api.unsplash.com/search/photos/?client_id=dwLptGju37-qTBvddj3f-ihKzgzdhtnL65ElFj0UBIU&query=${searchText}&orientation=landscape&per_page=10`
    )
      .then(res => res.json())
      .then(data => {
        setSearchResults(data.results);
      });
  };
  // Close Modal
  const handleClose = () => {
    setOpen(false);
    setAttachment(null);
    setLibAttachment(null);
    setUploadTab("upload");
  };

  // Change Tab View
  const handleTabChange = (event, value) => {
    if (value) {
      setUploadTab(value);
      if (value == "library") {
        fetchImages();
      }
      else {
        setSearchResults([]);
      }
    }
  };
  const handleSuccess = () => {
    setBtnQuery("progress");
    if (uploadTab == "library") {
      saveLibraryImg(libAttachment);
    } else {
      attachFeatureImg(
        /** uploading file to s3 bucket */
        attachment,
        succ => {
          if (succ) {
            saveFeatureImg(succ);
          }
        },
        err => {}
      );
    }
  };

  const saveFeatureImg = docPath => {
    /** function save the uploaded file info into db */
    let obj = { ...properties, docsPath: docPath, taskId: data.taskId };
    saveFeatureImage(obj, data, element, callback => {
      setBtnQuery("");
      setOpen(false);
    });
  };
  const saveLibraryImg = docPath => {
    /** function save the uploaded file info into db */
    let obj = { docsPath: docPath, taskId: data.taskId };
    saveLibraryImage(obj, data, element, callback => {
      setBtnQuery("");
      setOpen(false);
    });
  };
  const handleClick = imageUrl => {
    setLibAttachment(imageUrl);
  };

  return (
    <CustomDialog
      title={intl.formatMessage({
        id: "common.feature-Image.label2",
        defaultMessage: "Featured Image",
      })}
      dialogProps={{
        open: open,
        onClick: e => {
          e.stopPropagation();
        },
        onClose: handleClose,
        PaperProps: {
          style: { maxWidth: 558 },
        },
        disableBackdropClick: true,
        disableEscapeKeyDown: true,
      }}>
      <div className={classes.dialogContent}>
        <div className={classes.toggleContainer}>
          <ToggleButtonGroup
            size="small"
            exclusive
            onChange={handleTabChange}
            value={uploadTab}
            classes={{ root: classes.toggleBtnGroup }}>
            <ToggleButton
              key={1}
              value="upload"
              classes={{
                root: classes.toggleButton,
                selected: classes.toggleButtonSelected,
              }}>
              {intl.formatMessage({
                id: "common.upload.label",
                defaultMessage: "Upload Image",
              })}
            </ToggleButton>

            <ToggleButton
              key={2}
              value="library"
              classes={{
                root: classes.toggleButton,
                selected: classes.toggleButtonSelected,
              }}>
              {intl.formatMessage({
                id: "common.select.label2",
                defaultMessage: "Select from Library",
              })}
            </ToggleButton>
          </ToggleButtonGroup>
        </div>
        {uploadTab == "upload" && (
          <Upload
            attachment={attachment}
            setAttachment={setAttachment}
            data={data}
            setProperties={setProperties}
            setBtnQuery={setBtnQuery}
            intl={intl}
            projectId={projectId}
          />
        )}
        {uploadTab == "library" && (
          <div className={classes.tileContainer}>
            {/* <Tile title="Wireframes.png" />
                    <Tile title="Branding.png" /> */}
            {searchResults.map((item, index) => {
              return (
                <Tile
                  key={index}
                  imageUrl={item.urls.thumb}
                  selectedUrl={libAttachment}
                  handleClick={handleClick}
                  title={item.user.name}
                  url={item.links.html}
                  theme={theme}
                />
              );
            })}
          </div>
        )}{" "}
        {uploadTab == "library" && (
          <div className={classes.creditCnt}>
            <span className={classes.creditTitle}>
              {intl.formatMessage({
                id: "board.color-photo-picker.photo-from.label",
                defaultMessage: "Photos from Unsplash",
              })}
            </span>
          </div>
        )}
      </div>
      {
        uploadTab == "upload" ? <>
          <ButtonActionsCnt
            cancelAction={handleClose}
            successAction={handleSuccess}
            successBtnText={intl.formatMessage({
              id: "common.feature-Image.label",
              defaultMessage: "Add Featured Image",
            })}
            cancelBtnText={intl.formatMessage({ id: "common.action.cancel.label", defaultMessage: "Cancel" })}
            btnType="success"
            btnQuery={btnQuery}
            disabled={!attachment}
          />
        </> : <>
            <ButtonActionsCnt
              cancelAction={handleClose}
              successAction={handleSuccess}
              successBtnText={intl.formatMessage({
                id: "common.feature-Image.label",
                defaultMessage: "Add Featured Image",
              })}
              cancelBtnText={intl.formatMessage({ id: "common.action.cancel.label", defaultMessage: "Cancel" })}
              btnType="success"
              btnQuery={btnQuery}
              disabled={!libAttachment}
            />
          </>
      }
      
    </CustomDialog>
  );
};

FeaturedImgsModal.defaultProps = {
  open: false,
  setOpen: () => {},
  data: {},
  element: {},
  projectId: null,
  attachFeatureImg: () => {},
  saveFeatureImage: () => {},
  saveLibraryImage: () => {},
};

export default compose(
  injectIntl,
  withStyles(styles, { withTheme: true }),
  connect(null, {
    attachFeatureImg,
  })
)(FeaturedImgsModal);
