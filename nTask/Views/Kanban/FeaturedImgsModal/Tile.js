// @flow

import React, { useState } from "react";
import styles from "./styles.js";
import { withStyles } from "@material-ui/core/styles";
import CheckCircleOutline from "@material-ui/icons/CheckCircleOutline";
import tempBG from "../../../assets/images/temporary.png";
import classNames from "classnames";

type TileProps = {
  imageUrl: String,
  title: String,
  classes: Object,
};

const Tile = (props: TileProps) => {
  const { classes, title, imageUrl, item, selectedUrl, url, theme } = props;
  const [clicked, setClicked] = useState(false);

  const handleClick = () => {
    setClicked(!clicked);
    // props.selectedImage(imageUrl);
  };

  return (
    <div
      className={classNames(classes.tileContent, { [classes.contentClicked]: clicked })}
      onClick={e => props.handleClick(imageUrl)}>
      <div className={classes.tileBody} 
      style={{
        backgroundImage: `url(${imageUrl === "" ? tempBG : imageUrl})`,
      }}>
        {/* <img className={classes.tileImage} src={imageUrl === "" ? tempBG : imageUrl} /> */}
      </div>
      <div className={classNames(classes.tileTitle, { [classes.clicked]: clicked })}>
        <span>
          {`By: `}{" "}
          <a
            href={url}
            target="_blank"
            onClick={e => e.stopPropagation()}
            style={{
              // position: "absolute",
              // bottom: 0,
              // right: 0,
              // fontSize: 8,
              // color: "#7E7E7E",
              // padding: "3px 4px",
              fontFamily: theme.typography.fontFamilyLato,
            }}>
            {title}
          </a>
        </span>
        {selectedUrl && selectedUrl == imageUrl && (
          <CheckCircleOutline style={{ color: "#35C766" }} />
        )}
      </div>
    </div>
  );
};

Tile.defaultProps = {
  title: "",
  imageUrl: "",
};
export default withStyles(styles)(Tile);
