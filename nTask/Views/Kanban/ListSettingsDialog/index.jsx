// @flow

import { withStyles } from "@material-ui/core/styles";
import cloneDeep from "lodash/cloneDeep";
import isArray from "lodash/isArray";
import isEmpty from "lodash/isEmpty";
import React, { useEffect, useState } from "react";
import { FormattedMessage } from "react-intl";
import ButtonActionsCnt from "../../../components/Dialog/ConfirmationDialogs/ButtonActionsCnt";
import CustomDialog from "../../../components/Dialog/CustomDialog";
import { priorityData } from "../../../helper/taskDropdownData";
import AddTaskLabel from "./AddTaskLabel/AddTaskLabel.view";
import MaxLimitRuleView from "./MaxtasklimiteRule/MaxLimitRule.view";
import NewAddedTask from "./NewAddedTask/NewAddedTask.view";
import PriorityRuleView from "./PriorityRule/PriorityRule.view";
import styles from "./styles.js";
// Options Mock Data
// TODO: Store options in global state

type ListSettingsDialogProps = {
  open: Boolean,
  setOpen: Function,
  UpdateKanbanStatusRule: Function,
  classes: Object,
  theme: Object,
  data: Object,
};

const ListSettingsDialog = (props: ListSettingsDialogProps) => {
  const { open, setOpen, classes, theme, data, UpdateKanbanStatusRule, intl, boardPer } = props;
  const taskPriorityData = priorityData(theme, classes, intl);

  const [btnQuery, setBtnQuery] = useState("");
  const [settings, setSettings] = useState([]);
  const [allData, setAllData] = useState({});

  const handleChangeTaskLimit = (el, value) => {
    if (value >= data.tasksList.length) {
      const allItems = [...settings];
      const updatedItem = allItems.find((item) => item.ruleTitle == el.ruleTitle);
      updatedItem.dynamicStatusRule.keyName = "MAXTASKLIMIT_RULE";
      updatedItem.dynamicStatusRule.keyValue = value;
      setSettings(allItems);
    }
    // setMaxLimitTask(value);
  };

  const handlePriorityChange = (el, value) => {
    const allItems = [...settings];
    const updatedItem = allItems.find((item) => item.ruleTitle == el.ruleTitle);
    updatedItem.dynamicStatusRule.keyName = "PRIORITY_RULE";
    updatedItem.dynamicStatusRule.keyValue = value;
    setSettings(allItems);
  };

  const handleAddTaskLabelChange = (rule, value) => {
    const allItems = cloneDeep(settings);
    const settingRule = allItems.find((item) => item.ruleTitle == rule.ruleTitle);
    settingRule.dynamicStatusRule.keyValue = value;
    setSettings(allItems);
  };

  const handleNewAddedTaskChange = (el, value) => {
    const allItems = [...settings];
    const updatedItem = allItems.find((item) => item.ruleTitle == el.ruleTitle);
    updatedItem.dynamicStatusRule.keyValue = value;

    setSettings(allItems);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const updateSettings = (el, value) => {
    const allItems = [...settings];

    const allItemsUpdatedArr = allItems.map((updatedItem) => {
      if (updatedItem.ruleTitle === el.ruleTitle) {
        if (updatedItem.dynamicStatusRule) {
          updatedItem.dynamicStatusRule = null;
        } else {
          updatedItem.dynamicStatusRule = {};
          if (updatedItem.ruleTitle === "TASKENDDATE_RULE") {
            updatedItem.dynamicStatusRule.keyValueMode = "after";
            updatedItem.dynamicStatusRule.keyValue = "0";
            updatedItem.dynamicStatusRule.keyValuePeriod = "day";
          } else {
            updatedItem.dynamicStatusRule.keyValue = value;
            updatedItem.dynamicStatusRule.keyName = updatedItem.ruleTitle;
          }
        }
      }
      return updatedItem;
    });
    setSettings(allItemsUpdatedArr);
  };

  const updateRuleKeyValue = (ruleTitle, value) => {
    const allRules = [...settings];

    const allNewRules = allRules.map((rule) => {
      if (rule.ruleTitle !== ruleTitle) return rule;
      return { ...rule, ...value };
    });
    setSettings(allNewRules);
  };

  const handleSubmitColumnSettings = () => {
    setBtnQuery("progress");
    let item = settings[0];
    let rulesValues = [];
    settings.map((item) => {
      if (!isEmpty(item.dynamicStatusRule)) {
        rulesValues.push({
          ruleId: item.ruleId,
          applyLabelColorToBackground: item.applyLabelColorToBackground,
          addSeperateLabel: item.addSeperateLabel,
          addroleTextheading: item.addroleTextheading,
          dynamicStatusRule: {
            ...item.dynamicStatusRule,
            keyValue: isArray(item.dynamicStatusRule?.keyValue)
              ? item.dynamicStatusRule?.keyValue?.map((rule, index) => ({
                  ...rule,
                  position: index + 1,
                  name: rule.name ?? "",
                }))
              : item.dynamicStatusRule?.keyValue,
          },
        });
      }
    });

    let obj = {
      projectId: item.projectId,
      statusId: item.statusId,
      rulesValues: [...rulesValues],
    };

    UpdateKanbanStatusRule(obj, () => {
      setBtnQuery("");
      handleClose();
    });
  };

  useEffect(() => {
    setAllData(data);
    let statuses = [...data.statusRulesList];
    let ruleList = statuses.sort((a, b) => a.position - b.position);
    setSettings(ruleList);
  }, [data]);

  return (
    <CustomDialog
      title={
        <FormattedMessage
          id="board.board-detail.tasklist.menu.list-settings.label"
          values={{ t: <span style={{ color: "#7e7e7e" }}>{data.statusTitle}</span> }}
          defaultMessage={`List Settings: ${(
            <span style={{ color: "#7e7e7e" }}>{data.statusTitle}</span>
          )}`}
        />
      }
      dialogProps={{
        open: open,
        onClick: (e) => {
          e.stopPropagation();
        },
        onClose: handleClose,
        PaperProps: {
          style: { maxWidth: 800 },
        },
      }}>
      <div className={classes.settingContainer}>
        {settings.map((rule) => (
          <>
            {rule.ruleTitle != "TASKENDDATE_RULE" && (
              <>
                {rule.ruleTitle === "PRIORITY_RULE" && (
                  <PriorityRuleView
                    rule={rule}
                    boardPer={boardPer}
                    updateSettings={updateSettings}
                    taskPriorityData={taskPriorityData}
                    handlePriorityChange={handlePriorityChange}
                  />
                )}
                {rule.ruleTitle == "MAXTASKLIMIT_RULE" && !data.isDefault && !data.isDoneState && (
                  <MaxLimitRuleView
                    rule={rule}
                    boardPer={boardPer}
                    updateSettings={updateSettings}
                    allData={allData}
                    handleChangeTaskLimit={handleChangeTaskLimit}
                  />
                )}
              </>
            )}
            {rule.ruleTitle == "NEWADDEDTASK_RULE" && (
              <NewAddedTask
                rule={rule}
                updateSettings={updateSettings}
                handleNewAddedTaskChange={handleNewAddedTaskChange}
                disabled={!boardPer.boardDetail.listsetting.setLabeltoNewTask.cando}
              />
            )}
            {rule.ruleTitle == "ADDTASKLABEL_RULE" && (
              <AddTaskLabel
                rule={rule}
                updateSettings={updateSettings}
                updateRuleKeyValue={updateRuleKeyValue}
                handleAddTaskLabelChange={handleAddTaskLabelChange}
                disabled={!boardPer.boardDetail.listsetting.addLabeltoTask.cando}
              />
            )}
          </>
        ))}
      </div>
      <ButtonActionsCnt
        cancelAction={handleClose}
        successAction={handleSubmitColumnSettings}
        successBtnText={
          <FormattedMessage
            id="board.board-detail.tasklist.menu.list-settings.confirmation.save-button.label"
            defaultMessage="Save Settings"
          />
        }
        cancelBtnText={<FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />}
        btnType="success"
        btnQuery={btnQuery}
        disabled={
          !boardPer.boardDetail.listsetting.maxTasksetting.cando ||
          !boardPer.boardDetail.listsetting.prioritysetting.cando
        }
      />
    </CustomDialog>
  );
};

ListSettingsDialog.defaultProps = {
  open: false,
  data: {},
  setOpen: () => {},
  UpdateKanbanStatusRule: () => {},
  intl: {},
};
export default withStyles(styles, {
  withTheme: true,
})(ListSettingsDialog);
