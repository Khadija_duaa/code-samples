const styles = (theme) => ({
  settingContainer: {
    padding: 14,
    maxHeight: "69vh",
    overflow: "auto",
  },
  settingItem: {
    background: `${theme.palette.background.items} 0% 0% no-repeat padding-box`,
    borderRadius: 4,
    margin: 6,
    padding: "15px 12px",
    fontFamily: "Lato, sans-serif",
    fontSize: "13px !important",
    color: "#202020",
  },
  settingItemheader: {
    display: "flex",
    alignItems: "center",
  },
  active: {
    background: `${theme.palette.common.white} 0% 0% no-repeat padding-box`,
    border: `1px solid ${theme.palette.border.grayLighter}`,
    padding: "12px 11px",
  },
  settingLabel: {
    padding: "0 8px",
  },
  priorityIcon: {
    fontSize: "15px !important",
  },
  dot: {
    marginRight: 4,
    borderRadius: "50%",
    padding: 5,
  },
  addtasklabel_whenandthen: {
    display: "flex",
    width: 72,
    flex: "unset",
    flexDirection: "column",
    justifyContent: "space-around",
    textAlign: "center",
    position: "relative",
    "& > img": {
      position: "absolute",
    },
    "& > div": {
      marginLeft: "1rem",
    },
  },
  input: {
    backgroundColor: theme.palette.background.items,
    borderRadius: 4,
    padding: 0,
    textTransform: "none",
    border: "none",
    padding: "8.5px 4px",
    maxWidth: 38,
    margin: "0 2px",
    marginLeft: 5,
  },
  caretdown: {
    padding: 2,
    color: theme.palette.text.light,
  },
  mr8: {
    marginRight: 8,
  },
  borderContainer: {
    border: `1px solid ${theme.palette.border.grayLighter}`,
    borderRadius: 5,
    flexGrow: 1,
  },
  p12: {
    padding: 12,
  },
  hr: {
    border: 0,
    borderBottom: `1px dashed ${theme.palette.border.lightBorder}`,
    margin: "4px 0.8rem",
  },
  closeContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: 50,
    flex: "unset",
    "& > button ": {
      background: `${theme.palette.text.light} !important`,
      borderRadius: 999,
      display: "flex",
      padding: 0,
    },
    "& svg ": {
      color: "white",
      borderRadius: 999,
      padding: 4,
    },
  },
});
export default styles;
