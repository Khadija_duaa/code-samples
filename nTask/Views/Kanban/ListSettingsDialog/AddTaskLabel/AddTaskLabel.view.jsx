import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import AddIcon from "@material-ui/icons/Add";
import CrossIcon from "@material-ui/icons/Close";
import DragIndicator from "@material-ui/icons/DragIndicator";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import classNames from "classnames";
import React, { useEffect, useState } from "react";
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
import ArrowCondition from "../../../../assets/images/ArrowCondition.png";
import CustomButton from "../../../../components/Buttons/CustomButton";
import CustomIconButton from "../../../../components/Buttons/CustomIconButton";
import ColorPickerDropdown from "../../../../components/Dropdown/ColorPicker/Dropdown";
import CustomSelect from "../../../../components/Dropdown/CustomSelect/CustomSelect.view";
import DefaultCheckbox from "../../../../components/Form/Checkbox";
import DefaultSwitch from "../../../../components/Form/Switch";
import CustomMenuList from "../../../../components/MenuList/CustomMenuList";
import { GetAddTaskLabelOptions } from "../../../../redux/actions/boards";
import { styles } from "./AddTaskLabel.style.js";
import { comparisonOperatorOptions, DEFAULTADDTASKLABEL_RULE } from "./constant";

const AddTaskLabel = (props) => {
  const {
    classes,
    theme,
    rule = {},
    disabled = false,
    updateSettings = () => {},
    handleAddTaskLabelChange = () => {},
    updateRuleKeyValue = () => {},
  } = props;
  const [addTaskLabelColorPicker, setAddTaskLabelColorPicker] = useState({});
  const [ruleValue, setRuleValue] = useState(
    rule.dynamicStatusRule?.keyValue
      .map((item, index) => ({
        ...item,
        id: item.id ?? item.key ?? index,
      }))
      ?.sort((a, b) => a.position - b.position)
  );

  const [options, setOptions] = useState([]);

  useEffect(() => {
    GetAddTaskLabelOptions(({ data }) =>
      setOptions(data.entity.map(({ id, title }) => ({ label: title, value: id })))
    );
  }, []);

  const updateValue = (setting, name, option) => {
    if (name === "yValue" && Number(option.value) < 0) return;

    const newValue = ruleValue.map((item) => {
      if (setting.id === item.id) {
        return { ...item, [name]: option.value };
      } else return item;
    });

    setRuleValue(newValue);
    handleAddTaskLabelChange(rule, newValue);
  };

  function onRemove(setting) {
    const newValue = ruleValue.filter((item) => item.id !== setting.id);
    setRuleValue(newValue);
    handleAddTaskLabelChange(rule, newValue);
  }

  function onAdd() {
    const newValue = [...ruleValue, { ...DEFAULTADDTASKLABEL_RULE, id: ruleValue?.length }];
    if (newValue.length > 1) handleAddTaskLabelChange(rule, newValue);
    setRuleValue(newValue);
    setTimeout(() => updateRuleKeyValue(rule.ruleTitle, { applyLabelColorToBackground: false }));
  }

  const onDragEnd = (result) => {
    const {
      source: { index: from },
      destination: { index: to },
    } = result;
    const newValue = [...ruleValue];
    const fromValue = newValue.splice(from, 1);
    newValue.splice(to, 0, ...fromValue);
    setRuleValue(newValue);
    handleAddTaskLabelChange(rule, newValue);
  };

  const onAddSeperateLabelToggle = ({ target: { checked } }) => {
    updateRuleKeyValue(rule.ruleTitle, { addSeperateLabel: checked });
  };
  const onApplyLabelColorToBackground = ({ target: { checked } }) => {
    updateRuleKeyValue(rule.ruleTitle, { applyLabelColorToBackground: checked });
  };

  return (
    <div
      key={rule.key}
      className={classNames(classes.settingItem, {
        [classes.active]: rule.dynamicStatusRule ? true : false,
      })}>
      <div className={classes.settingItemHeader}>
        <DefaultCheckbox
          checked={rule.dynamicStatusRule || false}
          onChange={(_, checked) => {
            updateSettings(rule, ruleValue ?? [{ ...DEFAULTADDTASKLABEL_RULE }]);
            if (checked && !ruleValue) setRuleValue([{ ...DEFAULTADDTASKLABEL_RULE }]);
          }}
          checkboxStyles={{ padding: 0 }}
          disabled={disabled}
        />
        <div className={classes.settingLabel}>{rule.ruleDescription || "No Descripton"}</div>
      </div>

      {rule.dynamicStatusRule && (
        <DragDropContext onDragEnd={onDragEnd}>
          <Droppable droppableId="droppable">
            {(provided) => (
              <CustomMenuList>
                <div ref={provided.innerRef}>
                  {ruleValue?.map((setting, index) => (
                    <Draggable key={`${setting.id}`} draggableId={`${setting.id}`} index={index}>
                      {(provided) => (
                        <div ref={provided.innerRef} {...provided.draggableProps} key={index}>
                          <div className={classes.setting}>
                            <div {...provided.dragHandleProps}>
                              <DragIndicator htmlColor={theme.palette.secondary.light} />
                            </div>
                            <div className={classes.index}>{index + 1}</div>

                            <div className={classes.addTaskLabelWhenAndThen}>
                              <img src={ArrowCondition} alt="ArrowCondition" />
                              <div>
                                <b>When</b>
                              </div>
                              <div>
                                <b>Then</b>
                              </div>
                            </div>
                            <div className={classes.borderContainer}>
                              <div
                                style={{
                                  padding: 12,
                                  display: "flex",
                                  alignItems: "center",
                                }}>
                                <CustomSelect
                                  defaultValue={setting.xValue}
                                  onChange={(option) => updateValue(setting, "xValue", option)}
                                  style={{ marginRight: 8 }}
                                  options={options.filter(
                                    (option) => option.value !== setting.yValueLimit
                                  )}
                                />
                                <div style={{ marginRight: 8 }}>is</div>
                                <CustomSelect
                                  style={{ marginRight: 8 }}
                                  defaultValue={setting.comparisonOperator}
                                  onChange={(option) =>
                                    updateValue(setting, "comparisonOperator", option)
                                  }
                                  options={comparisonOperatorOptions}
                                />
                                <input
                                  min={1}
                                  type="number"
                                  className={classes.input}
                                  value={setting.yValue}
                                  style={{ marginLeft: 5, marginRight: 8, maxWidth: 38 }}
                                  onChange={({ target }) =>
                                    updateValue(setting, "yValue", target.value)
                                  }
                                />
                                <div style={{ marginRight: 8 }}>day(s) from</div>
                                <CustomSelect
                                  defaultValue={setting.yValueLimit}
                                  style={{ marginRight: 8 }}
                                  onChange={(option) => updateValue(setting, "yValueLimit", option)}
                                  options={options.filter(
                                    (option) => option.value !== setting.xValue
                                  )}
                                />
                              </div>
                              <hr className={classes.hr} />
                              <div
                                style={{
                                  padding: 12,
                                  display: "flex",
                                  alignItems: "center",
                                }}>
                                <div style={{ marginRight: 8 }}>add the</div>
                                <div style={{ marginRight: 8 }}>
                                  <ColorPickerDropdown
                                    onSelect={(color) => updateValue(setting, "color", color)}
                                    obj={{}}
                                    singleSelect={true}
                                    handleCloseColor={() =>
                                      setAddTaskLabelColorPicker((prev) => ({
                                        ...prev,
                                        [index]: undefined,
                                      }))
                                    }
                                    customColorBtnRender={
                                      <CustomButton
                                        btnType="transparent"
                                        onClick={({ currentTarget }) =>
                                          setAddTaskLabelColorPicker((prev) => ({
                                            ...prev,
                                            [index]: currentTarget,
                                          }))
                                        }
                                        style={{
                                          display: "inline-flex",
                                          alignItems: "center",
                                          padding: "4px 8px",
                                          fontSize: "11px",
                                          background: "rgba(241, 241, 241, 1)",
                                          textTransform: "none",
                                        }}>
                                        <span
                                          className={classes.dot}
                                          style={{ background: setting.color }}
                                        />
                                        {setting.color}{" "}
                                        <ExpandMoreIcon className={classes.caretDown} />
                                      </CustomButton>
                                    }
                                    anchorEl={addTaskLabelColorPicker[index]}
                                  />
                                </div>
                                <div style={{ padding: 4 }}>label to the task,</div>
                                <div style={{ padding: 6 }}>
                                  <i>Optional</i>:
                                </div>
                                <input
                                  type="name"
                                  className={classes.input}
                                  placeholder="Value name"
                                  maxLength={20}
                                  value={setting.name}
                                  onChange={({ target }) =>
                                    updateValue(setting, "name", target.value)
                                  }
                                />
                              </div>
                            </div>
                            <div className={classes.closeContainer}>
                              {ruleValue?.length > 1 && (
                                <CustomIconButton
                                  btnType="transparent"
                                  onClick={() => onRemove(setting)}>
                                  <CrossIcon />
                                </CustomIconButton>
                              )}
                            </div>
                          </div>
                        </div>
                      )}
                    </Draggable>
                  ))}
                </div>
              </CustomMenuList>
            )}
          </Droppable>
        </DragDropContext>
      )}

      {rule.dynamicStatusRule && ruleValue?.length < 5 && (
        <div style={{ display: "flex", flexWrap: "wrap" }}>
          <div className={classes.addTaskLabelWhenAndThen} />
          <div>
            <CustomButton
              btnType="transparent"
              onClick={() => onAdd()}
              style={{
                marginLeft: "-10px",
                textTransform: "none",
                marginTop: 6,
                background: "transparent !important",
              }}>
              <Typography
                color="primary"
                style={{
                  display: "flex",
                  alignItems: "center",
                  textDecoration: "underline",
                }}>
                <AddIcon style={{ fontSize: "1rem" }} /> Add New Rule
              </Typography>
            </CustomButton>
          </div>
        </div>
      )}
      {rule.dynamicStatusRule && (
        <>
          <div className={classes.AddRollValue}>
            <div>
              <DefaultSwitch
                checked={rule.addSeperateLabel}
                onChange={onAddSeperateLabelToggle}
                size="small"
              />
            </div>
            <div style={{ marginLeft: 16 }}>
              <div className={classes.addroleTextheading}>Add separate label for each rule</div>
              <Typography className={classes.addroleTypography}>
                A new/separate label will be added to the card when a new rule will be triggered
                rather then replacing the same label color
              </Typography>
            </div>
          </div>
          {ruleValue?.length <= 1 && (
            <div className={classes.AddRollValue}>
              <div>
                <DefaultSwitch
                  checked={rule.applyLabelColorToBackground}
                  onChange={onApplyLabelColorToBackground}
                  size="small"
                />
              </div>
              <div style={{ marginLeft: 16 }}>
                <div className={classes.addroleTextheading}>Apply label color to the card</div>
                <Typography className={classes.addroleTypography}>
                  shade of label color will be applied to the background of the card
                </Typography>
              </div>
            </div>
          )}
        </>
      )}
    </div>
  );
};

export default withStyles(styles, { withTheme: true })(AddTaskLabel);
