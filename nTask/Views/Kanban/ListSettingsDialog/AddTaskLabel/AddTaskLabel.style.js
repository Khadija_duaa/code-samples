export const styles = (theme) => ({
  setting: {
    display: "flex",
    flexWrap: "wrap",
    alignItems: "center",
    padding: "8px 0",
    backgroundColor: theme.palette.common.white,
  },
  index: {
    background: theme.palette.background.green,
    padding: 4,
    borderRadius: 4,
    minWidth: 22,
    textAlign: "center",
    color: theme.palette.common.white,
    marginRight: 8,
  },
  settingItem: {
    background: theme.palette.background.items,
    borderRadius: 4,
    margin: 6,
    padding: "15px 12px",
    fontFamily: theme.typography.fontFamilyLato,
    fontSize: "13px !important",
    color: theme.palette.text.primary,
  },
  settingItemHeader: {
    display: "flex",
    alignItems: "center",
  },
  active: {
    background: theme.palette.common.white,
    border: `1px solid ${theme.palette.border.grayLighter}`,
    padding: "12px 11px",
  },
  settingLabel: {
    margin: "0 8px",
  },

  dot: {
    marginRight: 4,
    borderRadius: "50%",
    padding: 5,
  },
  addTaskLabelWhenAndThen: {
    display: "flex",
    width: 72,
    flexDirection: "column",
    justifyContent: "space-around",
    position: "relative",
    alignSelf: "normal",
    "& > img": {
      position: "absolute",
    },
    "& > div": {
      marginLeft: "1.5rem",
    },
  },
  input: {
    backgroundColor: theme.palette.background.items,
    borderRadius: 4,
    textTransform: "none",
    border: "none",
    padding: "8.5px 4px",
    margin: "0 2px",
    padding: 8,
  },
  caretDown: {
    padding: 2,
    color: theme.palette.text.light,
  },
  borderContainer: {
    border: `1px solid ${theme.palette.border.grayLighter}`,
    borderRadius: 5,
    flexGrow: 1,
  },
  hr: {
    border: 0,
    borderBottom: `1px dashed ${theme.palette.border.lightBorder}`,
    margin: "4px 0.8rem",
  },
  closeContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: 50,
    "& > button ": {
      background: `${theme.palette.text.light} !important`,
      borderRadius: "50%",
      display: "flex",
      padding: 0,
    },
    "& svg ": {
      color: theme.palette.common.white,
      borderRadius: "50%",
      padding: 4,
    },
  },
  AddRollValue: {
    display: "flex",
    backgroundColor: "#E6F7FD",
    marginLeft: 20,
    marginTop: 10,
    padding: 20,
    borderRadius: 4,
  },
  addroleTextheading: {
    fontSize: "16px !important",
    color: "#161717",
    fontWeight: 600,
  },
  addroleTypography: {
    marginTop: 5,
    fontSize: "13px !important",
    color: "#424445",
  },
});
