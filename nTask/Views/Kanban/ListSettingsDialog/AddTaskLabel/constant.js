export const comparisonOperatorOptions = [
  { label: "greater than", value: "greater than" },
  { label: "less than", value: "less than" },
];

export const DEFAULTADDTASKLABEL_RULE = {
  id: 0,
  xValue: "today",
  comparisonOperator: "less than",
  yValue: 1,
  yValueLimit: "creation",
  color: "#FFBF00",
};
