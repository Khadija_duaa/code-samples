export const styles = (theme) => ({
  settingContainer: {
    padding: 14,
  },
  settingItem: {
    background: `${theme.palette.background.items} 0% 0% no-repeat padding-box`,
    borderRadius: 4,
    margin: 6,
    padding: "15px 12px",
    fontFamily: "Lato, sans-serif",
    fontSize: "13px !important",
    color: "#202020",
  },
  active: {
    background: `${theme.palette.common.white} 0% 0% no-repeat padding-box`,
    border: `1px solid ${theme.palette.border.grayLighter}`,
    padding: "12px 11px",
  },

  settingItemheader: {
    display: "flex",
    alignItems: "center",
  },

  settingLabel: {
    padding: "0 8px",
  },
  input: {
    backgroundColor: theme.palette.background.items,
    borderRadius: 4,
    padding: 0,
    textTransform: "none",
    border: "none",
    padding: "8.5px 4px",
    maxWidth: 38,
    margin: "0 2px",
    marginLeft: 5,
  },
});
