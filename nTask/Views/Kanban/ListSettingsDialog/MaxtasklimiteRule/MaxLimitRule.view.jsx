import withStyles from "@material-ui/core/styles/withStyles";
import classNames from "classnames";
import React, { useMemo, useState } from "react";
import DefaultCheckbox from "../../../../components/Form/Checkbox";
import { styles } from "./MaxTaskLimitRule.style";

export const MaxLimitRule = (props) => {
  const {
    classes,
    rule = {},
    updateSettings = () => {},
    boardPer,
    allData,
    handleChangeTaskLimit,
  } = props;

  const DEFAULTVALUE = useMemo(
    () => (allData.tasksList?.length > 0 ? allData.tasksList?.length : 1),
    [allData.tasksList]
  );

  const [ruleValue, setRuleValue] = useState(rule.dynamicStatusRule?.keyValue);

  const handleKeyPress = (evt) => {
    let val = String.fromCharCode(evt.which);
    let fVal = evt.target.value + val;
    if (evt.which < 48 || evt.which > 57) evt.preventDefault();
    if (parseInt(fVal) > 1000) evt.preventDefault();
  };

  const onChange = (value) => {
    if (Number(value) < 0) return;

    setRuleValue(value);
    handleChangeTaskLimit(rule, value);
  };

  return (
    <div>
      <div
        key={rule.key}
        className={classNames(classes.settingItem, classes.settingItemheader, {
          [classes.active]: rule.dynamicStatusRule ? true : false,
        })}>
        <DefaultCheckbox
          checked={rule.dynamicStatusRule || false}
          onChange={(_, checked) => {
            updateSettings(rule, ruleValue ?? DEFAULTVALUE);
            if (checked && !ruleValue) setRuleValue(DEFAULTVALUE);
          }}
          checkboxStyles={{ padding: 0 }}
          disabled={!boardPer.boardDetail.listsetting.maxTasksetting.cando}
        />
        <span className={classes.settingLabel}>{rule.ruleDescription || "No Descripton"}</span>
        {rule.dynamicStatusRule && (
          <input
            type="number"
            max={1000}
            onKeyPress={(evt) => handleKeyPress(evt)}
            value={ruleValue}
            min={allData.tasksList.length > 0 ? allData.tasksList.length : 1}
            onChange={({ target }) => onChange(target.value)}
            className={classes.input}
            disabled={!boardPer.boardDetail.listsetting.maxTasksetting.cando}
          />
        )}
      </div>
    </div>
  );
};
export default withStyles(styles, { withTheme: true })(MaxLimitRule);
