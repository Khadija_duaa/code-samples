import withStyles from "@material-ui/core/styles/withStyles";
import classNames from "classnames";
import React, { useState } from "react";
import StatusDropdown from "../../../../components/Dropdown/StatusDropdown/Dropdown";
import DefaultCheckbox from "../../../../components/Form/Checkbox";
import { styles } from "./Priorityrule.style";

const DEFAULTVALUE = 3;

const PriorityRule = (props) => {
  const {
    classes,
    rule = {},
    updateSettings = () => {},
    boardPer = {},
    taskPriorityData,
    handlePriorityChange,
  } = props;

  const [ruleValue, setRuleValue] = useState(rule.dynamicStatusRule?.keyValue);

  const getPriorityOption = (val) => {
    let priority = taskPriorityData ? taskPriorityData.find((el) => el.value == val) || {} : {};
    return priority;
  };

  const onChange = (option) => {
    setRuleValue(option.value);
    handlePriorityChange(rule, option.value);
  };

  return (
    <div
      key={rule.key}
      className={classNames(classes.settingItem, classes.settingItemheader, {
        [classes.active]: rule.dynamicStatusRule ? true : false,
      })}>
      <div className={classes.parentcheckBox}>
        <DefaultCheckbox
          checked={rule.dynamicStatusRule || false}
          onChange={(_, checked) => {
            updateSettings(rule, ruleValue ?? DEFAULTVALUE);
            if (checked && !ruleValue) setRuleValue(DEFAULTVALUE);
          }}
          checkboxStyles={{ padding: 0 }}
          disabled={!boardPer.boardDetail.listsetting.prioritysetting.cando}
        />
        <span className={classes.settingLabel}>{rule.ruleDescription || "No Descripton"}</span>
        {rule.dynamicStatusRule && (
          <StatusDropdown
            onSelect={(option) => onChange(option)}
            iconButton={false}
            style={{
              fontSize: "13px",
              height: 28,
              borderRadius: 4,
              color: "#202020",
              background: "#F1F1F1",
              display: "flex",
              flexDirection: "row",
              justifyContent: "start",
            }}
            option={getPriorityOption(ruleValue)}
            options={taskPriorityData}
            toolTipTxt="Task Priority"
            disabled={!boardPer.boardDetail.listsetting.prioritysetting.cando}
            dropdownProps={{ disablePortal: false }}
          />
        )}
      </div>
    </div>
  );
};

export default withStyles(styles, { withTheme: true })(PriorityRule);
