export const styles = (theme) => ({
  settingItem: {
    background: `${theme.palette.background.items} 0% 0% no-repeat padding-box`,
    borderRadius: 4,
    margin: 6,
    padding: "15px 12px",
    fontFamily: "Lato, sans-serif",
    fontSize: "13px !important",
    color: "#202020",
  },
  settingItemheader: {
    display: "flex",
    alignItems: "center",
  },
  parentcheckBox: {
    display: "flex",
    alignItems: "center",
  },
  settingLabel: {
    padding: "0 8px",
  },
  active: {
    background: `${theme.palette.common.white} 0% 0% no-repeat padding-box`,
    border: `1px solid ${theme.palette.border.grayLighter}`,
    padding: "12px 11px",
  },
});
export default styles;
