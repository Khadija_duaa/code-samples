import withStyles from "@material-ui/core/styles/withStyles";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import classNames from "classnames";
import React, { useState } from "react";
import CustomButton from "../../../../components/Buttons/CustomButton";
import ColorPickerDropdown from "../../../../components/Dropdown/ColorPicker/Dropdown";
import DefaultCheckbox from "../../../../components/Form/Checkbox";
import { styles } from "./NewAddedTask.style";

const DEFAULTVALUE = "#FFBF00";

const NewAddedTask = (props) => {
  const {
    classes,
    rule = {},
    updateSettings = () => {},
    handleNewAddedTaskChange = () => {},
    disabled = false,
  } = props;

  const [newAddedTaskColorPicker, setNewAddedTaskColorPicker] = useState();
  const [ruleValue, setRuleValue] = useState(rule.dynamicStatusRule?.keyValue);

  return (
    <div
      key={rule.key}
      className={classNames(classes.settingItem, classes.settingItemHeader, {
        [classes.active]: rule.dynamicStatusRule ? true : false,
      })}>
      <DefaultCheckbox
        checked={rule.dynamicStatusRule || false}
        onChange={(_, checked) => {
          updateSettings(rule, ruleValue ?? DEFAULTVALUE);
          if (checked && !ruleValue) setRuleValue(DEFAULTVALUE);
        }}
        checkboxStyles={{ padding: 0 }}
        disabled={disabled}
      />
      <div className={classes.settingLabel}>{rule.ruleDescription || "No Descripton"}</div>
      {rule.dynamicStatusRule && (
        <ColorPickerDropdown
          onSelect={(value) => {
            setRuleValue(value);
            handleNewAddedTaskChange(rule, value);
          }}
          obj={{}}
          singleSelect={true}
          handleCloseColor={(e) => setNewAddedTaskColorPicker(undefined)}
          customColorBtnRender={
            <CustomButton
              btnType="transparent"
              onClick={({ currentTarget }) => setNewAddedTaskColorPicker(currentTarget)}
              style={{
                display: "inline-flex",
                alignItems: "center",
                padding: "4px 8px",
                fontSize: "11px",
                background: "rgba(241, 241, 241, 1)",
                textTransform: "none",
              }}>
              <span className={classes.dot} style={{ background: ruleValue }} />
              {ruleValue} <ExpandMoreIcon className={classes.caretDown} />
            </CustomButton>
          }
          anchorEl={newAddedTaskColorPicker}
        />
      )}
    </div>
  );
};

export default withStyles(styles, { withTheme: true })(NewAddedTask);
