export const styles = (theme) => ({
  settingContainer: {
    padding: 14,
  },
  settingItem: {
    background: `${theme.palette.background.items} 0% 0% no-repeat padding-box`,
    borderRadius: 4,
    margin: 6,
    padding: "15px 12px",
    fontFamily: theme.typography.fontFamilyLato,
    fontSize: "13px !important",
    color: theme.palette.text.primary,
  },
  settingItemHeader: {
    display: "flex",
    alignItems: "center",
  },
  active: {
    background: `${theme.palette.common.white} 0% 0% no-repeat padding-box`,
    border: `1px solid ${theme.palette.border.grayLighter}`,
    padding: "12px 11px",
  },
  settingLabel: {
    margin: "0 8px",
  },
  priorityIcon: {
    fontSize: "15px !important",
  },
  dot: {
    marginRight: 4,
    borderRadius: "50%",
    padding: 5,
  },

  caretDown: {
    padding: 2,
    color: theme.palette.text.light,
  },
});
