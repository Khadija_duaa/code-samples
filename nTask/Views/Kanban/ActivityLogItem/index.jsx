// @flow

import React from "react";
import logItemStyle from "./style.js";
import { withStyles } from "@material-ui/core/styles";
import CustomAvatar from "../../../components/Avatar/Avatar";
import Avatar from "../../../assets/images/1.png";
import ReactHtmlParser from 'react-html-parser';

/**
 * Activity Log Item Component
 * @props {*} data
 */

type ActivityLogItemProps = {
  classes: Object,
  theme: Object,
  data: Object, // Log data shown in list
  selectedMember: Object, // Log data shown in list
  handleClickActivityLogItem: Function, // Log data shown in list
};

const ActivityLogItem = (props: ActivityLogItemProps) => {
  const { classes, data, handleClickActivityLogItem, selectedMember } = props;
  let taskActivityLog = false;
  let beforeMsg = "";
  let afterMsg = "";
  if (data.taskId !== "" && data.taskName && data.taskName !== "") {
    taskActivityLog = true;
    let msg = data.action;
    let taskNameIndex = msg.search(data.taskName);
    beforeMsg = msg.substr(0, taskNameIndex);
    afterMsg = msg.substr(taskNameIndex + data.taskName.length, msg.length);
  }
  return (
    <div className={classes.container}>
      <div>
        <CustomAvatar
          otherMember={{
            imageUrl: selectedMember ? selectedMember.imageUrl : null,
            fullName: selectedMember ? selectedMember.fullName : null,
            lastName: "",
            email: selectedMember ? selectedMember.email : null,
          }}
          size="xsmall"
          disableCard
        />
      </div>
      <div className={classes.detail}>
        <div className={classes.description}>
          <span className={classes.user}>{`${data.userName} `}</span>
          {taskActivityLog ? (
            <>
              <span className={classes.actionDescription}>
                {` ${beforeMsg}`}{" "}
              </span>
              <span
                className={classes.ticket}
                onClick={() => {
                  handleClickActivityLogItem(data);
                }}
              >
                {data.taskName}{" "}
              </span>
              <span className={classes.actionDescription}>{afterMsg} </span>
            </>
          ) : (
            // <span className={classes.actionDescription}>{ReactHtmlParser(data.action)} </span>
            <span className={classes.actionDescription}>{data.action.replace(data.userName, "")} </span>
          )}
        </div>
        <div className={classes.date}>{`${data.date} at ${data.time}`}</div>
      </div>
    </div>
  );
};

ActivityLogItem.defaultProps = {
  data: [],
  selectedMember: {},
  handleClickActivityLogItem: () => {},
};
export default withStyles(logItemStyle, { withTheme: true })(ActivityLogItem);
