// @flow

import React, { useState } from "react";
import TaskBoard from "./TaskBoard";
import ActivityLogBar from "./ActivityLogBar";
import { calculateContentHeight } from "../../utils/common";
import ReactResizeDetector from "react-resize-detector";
import isEmpty from "lodash/isEmpty";
import ListSettingsDialog from "./ListSettingsDialog";
import cloneDeep from "lodash/cloneDeep";

const Board = (props) => {
  // const [open, setOpen] = useState(false);
  const [contentHeight, setContentHeight] = useState(calculateContentHeight());
  const [openSettings, setOpenSettings] = useState(false);
  const [columnSettings, setColumnSettings] = useState({});

  const onResize = () => {
    setContentHeight(calculateContentHeight());
  };
  const openListSettingModal = (column) => {
    setColumnSettings(cloneDeep(column));
    setOpenSettings(true);
  };
  const handleCloseColumSettings = () => {
    // setColumnSettings({});
    setOpenSettings(false);
  };
  // const sortTasks = (tasksList) => {
  //   return tasksList.sort(function(a, b) {return a.taskPosition - b.taskPosition});
  // };
  const {
    dataList,
    saveKanbanListOrder,
    activityLogs,
    allTasks,
    handleClickListItem,
    onAddNewTask,
    onColumColorChange,
    onColumTitleChange,
    UpdateKanbanColumnSetting,
    UpdateKanbanStatusRule,
    activityLogDrawer,
    handleCloseActivityLog,
    handleClickActivityLogItem,
    taskDragDrop,
    quickFilters,
    loggedInUser,
    UpdateTask,
    onUpdateTaskColor,
    deleteTask,
    archiveTask,
    copyTask,
    forTemplatesSetColumnsInParent,
    saveTaskFeatureImage,
    saveTaskLibraryImage,
    addNewColumn,
    onDeleteStatus,
    onDuplicateStatus,
    intl,
    allMembers,
    unsortedTask,
    updateTaskInKanbanStore,
    addUnsortedTask,
    UpdateKanbanBoard,
    showSnackBar,
    handleClearActivityLog,
    updateTaskPositionsInStore
  } = props;

  return (
    <>
      <ReactResizeDetector handleWidth handleHeight onResize={onResize} />
      <TaskBoard
        dataSource={dataList.statusList}
        projectId= {dataList.projectId}
        boardId={dataList.boardId}
        // elementsArr={sortTasks(dataList.tasksList)}
        elementsArr={dataList.tasksList}
        unsortedSource={unsortedTask}
        contentHeight={contentHeight - 40}
        saveKanbanListOrder={saveKanbanListOrder}
        allTasks={allTasks}
        handleClickListItem={handleClickListItem}
        onAddNewTask={onAddNewTask}
        onColumColorChange={onColumColorChange}
        onColumTitleChange={onColumTitleChange}
        UpdateKanbanColumnSetting={UpdateKanbanColumnSetting}
        openListSettingModal={openListSettingModal}
        taskDragDrop={taskDragDrop}
        quickFilters={quickFilters}
        loggedInUser={loggedInUser}
        UpdateTask={UpdateTask}
        onUpdateTaskColor={onUpdateTaskColor}
        deleteTask={deleteTask}
        archiveTask={archiveTask}
        copyTask={copyTask}
        forTemplatesSetColumnsInParent={forTemplatesSetColumnsInParent}
        saveTaskFeatureImage={saveTaskFeatureImage}
        saveTaskLibraryImage={saveTaskLibraryImage}
        addNewColumn={addNewColumn}
        onDeleteStatus={onDeleteStatus}
        onDuplicateStatus={onDuplicateStatus}
        boardPer={dataList.userPermission.permission.board}
        intl={intl}
        updateTaskInKanbanStore={updateTaskInKanbanStore}
        addUnsortedTask={addUnsortedTask}
        boardData={dataList}
        UpdateKanbanBoard={UpdateKanbanBoard}
        showSnackBar={showSnackBar}
        updateTaskPositionsInStore={updateTaskPositionsInStore}
      />
      {activityLogDrawer && (
        <ActivityLogBar
          open={activityLogDrawer}
          setOpen={handleCloseActivityLog}
          title={intl.formatMessage({id:"activity-log.label", defaultMessage: "Activity Logd"})}
          data={activityLogs}
          handleClearActivityLog={handleClearActivityLog}
          handleClickActivityLogItem={handleClickActivityLogItem}
          allMembers={allMembers}
        />
      )}
      {!isEmpty(columnSettings) && (
        <ListSettingsDialog
          open={openSettings}
          setOpen={handleCloseColumSettings}
          data={columnSettings}
          UpdateKanbanStatusRule={UpdateKanbanStatusRule}
          intl={intl}
          boardPer={dataList.userPermission.permission.board}
        />
      )}
    </>
  );
};

Board.defaultProps = {
  data: [],
  dataList: {},
  theme: {},
  classes: {},
  saveKanbanListOrder: () => {},
  activityLogs: [],
  allTasks: [],
  handleClickListItem: () => {},
  onAddNewTask: () => {},
  onColumColorChange: () => {},
  onColumTitleChange: () => {},
  UpdateKanbanColumnSetting: () => {},
  UpdateKanbanStatusRule: () => {},
  handleCloseActivityLog: () => {},
  handleClickActivityLogItem: () => {},
  taskDragDrop: () => {},
  UpdateTask: () => {},
  onUpdateTaskColor: () => {},
  deleteTask: () => {},
  archiveTask: () => {},
  copyTask: () => {},
  addNewColumn: () => {},
  onDeleteStatus: () => {},
  onDuplicateStatus: () => {},
  forTemplatesSetColumnsInParent: () => {},
  updateTaskInKanbanStore: () => {},
  addUnsortedTask: () => {},
  showSnackBar: () => {},
  updateTaskPositionsInStore: () => {},
  activityLogDrawer: false,
  quickFilters: [],
  loggedInUser: {},
  allMembers: [],
  unsortedTask:[]
};
export default Board;
