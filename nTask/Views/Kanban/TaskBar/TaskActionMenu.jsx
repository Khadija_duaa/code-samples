// @flow

import React, { Component } from "react";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import CustomButton from "../../../components/Buttons/CustomButton";
import DropdownMenu from "../../../components/Menu/DropdownMenu";
import ColorPicker from "../../../components/Dropdown/ColorPicker/ColorPicker";
import taskBarStyles from "./style.js";
import { withStyles } from "@material-ui/core/styles";
import RightArrow from "@material-ui/icons/ArrowRight";
import { FormattedMessage } from "react-intl";
import SvgIcon from "@material-ui/core/SvgIcon";

class TaskActionMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      pickerOpen: false,
    };
  }

  componentDidMount() {
    const { open } = this.props;
    this.setState({
      open,
    });
  }

  componentWillReceiveProps() {
    const { open } = this.props;
    this.setState({
      open,
      pickerOpen: false
    });
  }

  onColorChange = (color) => {
    const { onColorUpdate } = this.props;
    // this.setState({
    //   pickerOpen: false,
    // });
    onColorUpdate(color);
  };

  handleClick = (event) => {
    //  event.stopPropagation();
    this.setState({ open: true });
  };

  handleClose = (event) => {
    if (document.getElementById("taskBarDropDown").contains(event.target)) {
      return;
    }
    this.setState({ open: false, pickerOpen: false, searchTerm: "" });
  };
  translate = (text) => {
    let id = "";
    switch (text) {
      case "List Settings":
        id = "board.board-detail.tasklist.menu.list-settings.label";
        break;
    }
    return id == "" ? text : <FormattedMessage id={id} defaultMessage={text} />
  };
  render() {
    const { open, pickerOpen } = this.state;
    const {
      theme,
      style,
      data,
      handleToggle,
      classes,
      selected,
      element,
      boardPer
    } = this.props;
    return (
      // <ClickAwayListener
      //   mouseEvent="onMouseDown"
      //   touchEvent="onTouchStart"
      //   onClickAway={this.handleClose}
      // >
        <div>
          <CustomButton
            onClick={this.handleClick}
            btnType="success"
            variant="text"
            className={style}
            buttonRef={(node) => {
              this.anchorEl = node;
            }}
          >
            <MoreVerticalIcon htmlColor={theme.palette.common.black} />
          </CustomButton>
          <DropdownMenu
            // isClickAway={true}
            open={open}
            anchorEl={this.anchorEl}
            placement="bottom-end"
            id="taskBarDropDown"
            size="small"
            disablePortal={false}
            closeAction={this.handleClose}
          >
            <List>
              {data.map((item, index) => {
                return item.name === "Color" ? (
                  <ListItem
                    key={item.name}
                    button
                    onClick={() =>
                      this.setState({ pickerOpen: true })
                    }
                  >
                    <ListItemText
                      primary={<FormattedMessage id="common.action.color.label" defaultMessage={item.name} />}
                      classes={{ primary: classes.itemText }}
                    />
                    <RightArrow
                      htmlColor={theme.palette.secondary.dark}
                      classes={{ root: classes.submenuArrowBtn }}
                    />
                    <div
                      id="colorPickerCnt"
                      className={classes.colorPickerCntLeft}
                      style={
                        pickerOpen ? { display: "block" } : { display: "none" }
                      }
                    >
                      <ColorPicker
                        triangle="hide"
                        onColorChange={this.onColorChange}
                        selectedColor={selected}
                      />
                    </div>
                  </ListItem>
                ) : (
                  <ListItem
                    key={item.name}
                    button
                    onClick={(event) => {
                      this.setState({ pickerOpen: false });
                      handleToggle(event, item.name, item.id, element);
                    }}
                  >
                    <ListItemText
                      primary={this.translate(item.name)}
                      classes={{ primary: index === 3 ? classes.itemTextDelete : classes.itemText }}
                    />
                  </ListItem>
                );
              })}
            </List>
          </DropdownMenu>
        </div>
      // </ClickAwayListener>
    );
  }
}

TaskActionMenu.defaultProps = {
  index: 0,
  open: false,
  style: {},
  item: {},
  selected: "",
  barIndex: 0,
  handleToggle: () => { },
  onColorUpdate: () => { },
  element: {},
  boardPer: {},
};
export default withStyles(taskBarStyles, { withTheme: true })(TaskActionMenu);
