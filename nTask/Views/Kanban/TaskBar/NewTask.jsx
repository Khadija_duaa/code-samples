// @flow

import React, { useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import taskBarStyles from "./style.js";
import classNames from "classnames";
import { calculateContentHeight } from "../../../utils/common";
import DefaultTextField from "../../../components/Form/TextField";
import Add from "@material-ui/icons/Add";

type NewTaskProps = {
  contentHeight: Integer,
  onAdd: Function,
  classes: Object,
  theme: Object,
};

const NewTask = (props: NewTaskProps) => {
  const { classes, onAdd, contentHeight,boardData } = props;
  const [focusIn, setFocusIn] = useState(false);
  const [title, setTitle] = useState("");
  const [error, setError] = useState(false);
  const [message, setMessage] = useState("");

  const handleClick = e => {
    e.stopPropagation();
    setTitle("");
    setFocusIn(true);
  };

  const focusOut = e => {
    e.stopPropagation();
    setFocusIn(false);
  };

  const handleChangeTitle = e => {
    const title = e.target.value;
    if (title === "") {
      setError(true);
      setMessage("Please Type Valid Title");
    } else {
      setError(false);
      setMessage("");
    }
    setTitle(title);
  };

  const addNewTask = e => {
    if (e.key === "Enter") {
      if (title === "") {
        setError(true);
        setMessage("Please Type Valid Title");
      } else {
      let boardDataDetail = boardData;
      const statusExist = boardDataDetail.statusList.some(el => el.statusTitle.toLowerCase() === title.toLowerCase().trim());
      if(!statusExist) {
      onAdd(title.trim());
      setFocusIn(false);
    }
    else {
      setError(true);
      setMessage("Status exist already.");
    }
  }
  }
  };

  return (
    <div className={classes.taskBar}>
      {focusIn ? (
        <div
          className={classNames(classes.header, classes.newBar, {
            [classes.inputHeader]: focusIn,
          })}
          onClick={handleClick}>
          <DefaultTextField
            error={error}
            errorState={error}
            errorMessage={message}
            formControlStyles={{ marginBottom: 0 }}
            styles={classes.addInput}
            defaultProps={{
              type: "text",
              id: "columnTitle",
              placeholder: "Enter List Title",
              value: title,
              autoFocus: true,
              inputProps: { maxLength: 80 },
              onChange: event => handleChangeTitle(event),
              onKeyDown: addNewTask,
              onBlur: focusOut,
            }}
          />
        </div>
      ) : (
        <div className={classNames(classes.header, classes.newBar)} onClick={handleClick}>
          <Add size="small" style={{ fontSize: "18px" }} />
          <h3 className={`${classes.caption} ${classes.newCaption}`}> Add New </h3>
        </div>
      )}
      <div
        className={focusIn ? classes.body : classes.newBody}
        onClick={handleClick}
        style={{ height: contentHeight + 50 }}></div>
    </div>
  );
};

NewTask.defaultProps = {
  onAdd: () => {},
};
export default withStyles(taskBarStyles, { withTheme: true })(NewTask);
