// @flow

import { withStyles } from "@material-ui/core/styles";
import SvgIcon from "@material-ui/core/SvgIcon";
import intersectionWith from "lodash/intersectionWith";
import isEmpty from "lodash/isEmpty";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { Draggable, Droppable } from "react-beautiful-dnd";
import { Scrollbars } from "react-custom-scrollbars";
import CustomButton from "../../../components/Buttons/CustomButton";
import DefaultTextField from "../../../components/Form/TextField";
import ListSettingIcon from "../../../components/Icons/ListSettingIcon";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";
import CustomTooltip from "../../../components/Tooltip/Tooltip";
import helper from "../../../helper";
import Ticket from "../Ticket";
import NewTicket from "../Ticket/NewTicket";
import taskBarStyles from "./style.js";
import TaskActionMenu from "./TaskActionMenu";

/**
 * TaskBar Component
 * @param {} props
 */

type TaskBarProps = {
  barindex: Boolean,
  el: Object,
  onUpdate: Function,
  onAdd: Function,
  contentHeight: Integer,
  classes: Object,
  theme: Object,
  handleClickListItem: Function,
  onColumColorChange: Function,
  onColumTitleChange: Function,
  openListSettingModal: Function,
  deleteTask: Function,
  copyTask: Function,
  handleUnsortedOpener: Function,
  saveFeatureImage: Function,
  saveLibraryImage: Function,
  onDeleteStatus: Function,
  onDuplicateStatus: Function,
  filter: Array,
  boardPer: Object,
  intl: Object,
};

const TaskBar = (props: TaskBarProps) => {
  const {
    classes,
    barIndex,
    el,
    onUpdate,
    onAdd,
    contentHeight,
    theme,
    allTasks,
    handleClickListItem,
    onColumColorChange,
    onColumTitleChange,
    UpdateKanbanColumnSetting,
    openListSettingModal,
    filter,
    loggedInUser,
    onUpdateTaskColor,
    deleteTask,
    archiveTask,
    copyTask,
    handleUnsortedOpener,
    saveFeatureImage,
    saveLibraryImage,
    boardPer,
    intl,
    onDeleteStatus,
    onDuplicateStatus,
    boardData,
    appliedFilters,
    handleDisable,
    showpill,
    setShowpill,
  } = props;
  const [menuVisible, setMenuVisible] = useState(false);
  // const [openSettings, setOpenSettings] = useState(false);

  const [editColumn, setEditColumn] = useState({});
  const [title, setTitle] = useState(el.listName);
  const [error, setError] = useState(false);
  const [message, setMessage] = useState("");

  const [data, setData] = useState({});
  const [ddData, setddData] = useState([]);
  const [pointerEvents, setPointerEvents] = useState(false);

  const settingddData = () => {
    let dataDropdown = [];
    dataDropdown.push({
      id: 0,
      name: intl.formatMessage({
        id: "common.edit-details.label",
        defaultMessage: "Settings",
      }),
    });
    if (boardPer.boardDetail.duplicateStatus.cando && teamCanView("customStatusAccess")) {
      dataDropdown.push({
        id: 1,
        name: intl.formatMessage({
          id: "common.duplicate-list.label",
          defaultMessage: "Copy",
        }),
      });
    }
    if (boardPer.boardDetail.changeStatusColor.cando && teamCanView("customStatusAccess")) {
      dataDropdown.push({
        id: 2,
        name: intl.formatMessage({
          id: "common.action.color.label",
          defaultMessage: "Color",
        }),
      });
    }
    if (boardPer.boardDetail.deleteStatus.cando && teamCanView("customStatusAccess")) {
      dataDropdown.push({
        id: 3,
        name: intl.formatMessage({
          id: "common.delete-list.label",
          defaultMessage: "Delete",
        }),
      });
    }
    setddData(dataDropdown);
  };

  useEffect(() => {
    if (el) {
      setData(el);
    }
  }, [el]);

  useEffect(() => {
    settingddData();
  }, []);

  // set pointer event false if modal close
  useEffect(() => {
    if (!handleDisable) {
      setPointerEvents(false);
    }
  }, [handleDisable]);
  const onAddNewTicket = (title, callback) => {
    onAdd(data, title, callback);
  };

  const handleToggle = (e, name, id, el) => {
    if (!boardData.isTemplate) {
      if (id === 0) {
        // setOpenSettings(true);
        openListSettingModal(el);
      }
      if (id === 1) {
        setPointerEvents(true);
        /** Duplicate Status from kanban board */
        onDuplicateStatus(el, (succ) => {
          setPointerEvents(false);
        });
      }
      if (id === 3) {
        setPointerEvents(true);
        /** Delete Status from kanban board */
        onDeleteStatus(el, (succ) => {
          setPointerEvents(false);
        });
      }
    }
  };
  const onColorChange = (color, editColumn) => {
    // setData({...data, color});
    // if (!column.isTemplate) {
    setPointerEvents(true);
    let boardDataDetail = boardData;
    boardDataDetail.statusList = boardDataDetail.statusList.map(function (status) {
      if (status.statusId == editColumn.statusId) {
        status.statusColor = color;
        return status;
      } else return status;
    });
    onColumColorChange(boardDataDetail, editColumn, (succ) => {
      setPointerEvents(false);
    });
    // }
  };

  const onUpdateTicket = (item) => {
    const { tickets } = el;
    const ind = tickets.findIndex((element) => element.id === item.id);
    tickets.splice(ind, 1, item);
    onUpdate({ ...el, tickets });
  };
  const quickFilter = (elements, filters) => {
    let tasks = elements;
    const isOtherOptionSelected =
      filters.indexOf("Starred") > -1 ||
      filters.indexOf("Due Today") > -1 ||
      filters.indexOf("Assigned To Me") > -1 ||
      filters.indexOf("Over Due") > -1 ||
      filters.indexOf("Subtask") > -1;
    if (isOtherOptionSelected) {
      // Filtering Other values [Due Today, Starred, Over Due, Assigned To Me]
      tasks = tasks.filter((x) => {
        if (filters.indexOf("Subtask") > -1) {
          if (filters.indexOf("Starred") > -1 && x.isStarted) {
            return x;
          } else if (filters.indexOf("Due Today") > -1) {
            if (x.actualEndDate) {
              let days = helper.RETURN_REMAINING_DAYS_STATUS(
                moment(x.actualEndDate).format("MM/DD/YYYY hh:mm A"),
                0
              );
              if (days === "Due today") {
                return x;
              }
            }
          } else if (filters.indexOf("Over Due") > -1) {
            if (x.actualEndDate) {
              let days = helper.RETURN_REMAINING_DAYS_STATUS(
                moment(x.actualEndDate).format("MM/DD/YYYY hh:mm A"),
                0
              );
              if (days.includes("overdue")) {
                return x;
              }
            }
          } else if (filters.indexOf("Assigned To Me") > -1) {
            if (x.assigneeList.indexOf(loggedInUser.userId) > -1) return x;
          } else {
            return x;
          }
        }
        if (filters.indexOf("Starred") > -1 && x.isStarted) {
          return x;
        }
        if (filters.indexOf("Due Today") > -1) {
          if (x.actualEndDate) {
            let days = helper.RETURN_REMAINING_DAYS_STATUS(
              moment(x.actualEndDate).format("MM/DD/YYYY hh:mm A"),
              0
            );
            if (days === "Due today") {
              return x;
            }
          }
        }
        if (filters.indexOf("Over Due") > -1) {
          if (x.actualEndDate) {
            let days = helper.RETURN_REMAINING_DAYS_STATUS(
              moment(x.actualEndDate).format("MM/DD/YYYY hh:mm A"),
              0
            );
            if (days.includes("overdue")) {
              return x;
            }
          }
        }
        if (filters.indexOf("Assigned To Me") > -1) {
          if (x.assigneeList.indexOf(loggedInUser.userId) > -1) return x;
        }
        // if (filters.indexOf("Subtask") > -1) {
        //   return x;
        // }
      });
      return tasks;
    } else {
      return tasks.filter((x) => x.parentId == null || x.parentId == "");
    }
  };

  const applySidebarFilter = (taskArr) => {
    let tasks = taskArr;
    let { Board } = appliedFilters;
    if (Board && Board.assignees.length > 0) {
      tasks = tasks.filter((x) => {
        if (intersectionWith(Board.assignees, x.assigneeList, (x, y) => x === y).length > 0) {
          return x;
        }
      });
    }
    return tasks;
  };

  const ticketMaping = (element, barIndex) => {
    // let filteredArr = allTasks.filter((t) => {
    //   return element.listItems.includes(t.taskId);
    // });
    // let filteredArr = element.tasksList;
    let filteredArr = quickFilter(element.tasksList, filter);
    filteredArr = applySidebarFilter(filteredArr);
    if (!isEmpty(filteredArr)) {
      return (
        <>
          {filteredArr.map((item, index) => (
            <Ticket
              key={`ticket-${index}`}
              showpill={showpill}
              setShowpill={setShowpill}
              index={index}
              item={item}
              barIndex={barIndex}
              onUpdate={onUpdateTicket}
              handleClickListItem={handleClickListItem}
              element={element}
              onUpdateTaskColor={onUpdateTaskColor}
              deleteTask={deleteTask}
              archiveTask={archiveTask}
              copyTask={copyTask}
              saveFeatureImage={saveFeatureImage}
              saveLibraryImage={saveLibraryImage}
              boardPer={boardPer}
              intl={intl}
              boardData={boardData}
            />
          ))}
        </>
      );
    } else return null;
  };

  const handleChangeTitle = (e) => {
    const title = e.target.value;
    if (title === "") {
      setError(true);
      setMessage("Please Type Valid Title");
    } else {
      setError(false);
      setMessage("");
    }
    setTitle(title);
  };

  const onKeyDownColumTitle = (e) => {
    let obj = {};
    if (e.key === "Enter") {
      if (title === "") {
        setError(true);
        setMessage("Please Type Valid Title");
      } else {
        let boardDataDetail = boardData;
        const statusExist = boardDataDetail.statusList.some(
          (el) => el.statusTitle.toLowerCase() === title.toLowerCase().trim()
        );
        if (!statusExist) {
          boardDataDetail.statusList = boardDataDetail.statusList.map(function (status) {
            if (status.statusId == editColumn.statusId) {
              status.statusTitle = title;
              status.isPointerEvents = true;
              obj = status;
              return status;
            } else return status;
          });
          setEditColumn((prevState) => obj);

          setPointerEvents(true);
          setError(false);
          setMessage("");
          props.UpdateKanbanBoard(
            boardDataDetail,
            (succ) => {
              setEditColumn({});
              setPointerEvents(false);
            },
            (fail) => { }
          );
        } else {
          setError(true);
          setMessage("Status exist already.");
        }
      }
    }
  };

  const handlClick = (element) => {
    setTitle(element.statusTitle);
    setEditColumn(element);
  };

  const focusOut = () => {
    setEditColumn({});
  };
  const getMaxTaskLimit = (el) => {
    let arr = el.statusRulesList.filter(
      (item) => item.dynamicStatusRule && item.dynamicStatusRule.keyName == "MAXTASKLIMIT_RULE"
    );
    if (arr.length > 0 && arr[0].dynamicStatusRule) {
      return { maxTaskEnable: true, maxTaskLimit: parseInt(arr[0].dynamicStatusRule.keyValue) };
    }
    return { maxTaskEnable: false, maxTaskLimit: 0 };
  };

  let maxTaskObj = getMaxTaskLimit(el);
  let listSettingIconCheck = el.statusRulesList.some((r) => r.dynamicStatusRule);

  return (
    <>
      {!isEmpty(editColumn) &&
        boardPer.boardDetail.renameStatus.cando &&
        teamCanView("customStatusAccess") ? (
        <div
          style={{
            pointerEvents: pointerEvents ? "none" : "all",
            opacity: pointerEvents ? 0.5 : 1,
          }}>
          <Draggable index={barIndex} draggableId={`${editColumn.id}`} key={editColumn.id}>
            {(provided, snapshot) => (
              <div
                className={classes.taskBar}
                ref={provided.innerRef}
                {...provided.draggableProps}
                onMouseLeave={() => {
                  setMenuVisible(false); // Invisible TaskActionMenu Dropdown Menu when mouse leave
                }}>
                <>
                  <div
                    {...provided.dragHandleProps}
                    className={classes.header}
                    style={{ borderTop: `4px solid ${editColumn.statusColor}` }}>
                    {!pointerEvents ? (
                      <DefaultTextField
                        error={error}
                        errorState={error}
                        errorMessage={message}
                        // helptext={createNewTaskHelpText.taskTitleHelpText}
                        formControlStyles={{ marginBottom: 0 }}
                        styles={classes.addInput}
                        defaultProps={{
                          type: "text",
                          id: "columnTitle",
                          placeholder: "Enter Column Title",
                          value: title,
                          autoFocus: true,
                          inputProps: { maxLength: 80 },
                          onChange: (event) => handleChangeTitle(event),
                          onKeyDown: onKeyDownColumTitle,
                          onBlur: focusOut,
                        }}
                      />
                    ) : (
                      <div className={classes.columnNameCnt}>
                        <CustomTooltip helptext={el.statusTitle} placement="top">
                          <h3
                            title={el.statusTitle}
                            className={classes.caption}
                            onClick={() => {
                              handlClick(el);
                            }}>
                            {el.statusTitle}
                          </h3>
                        </CustomTooltip>
                        <span className={classes.listItemsCount}>
                          {`${el.tasksList.length}`}
                          {maxTaskObj.maxTaskLimit > 0 ? `/${maxTaskObj.maxTaskLimit}` : null}
                        </span>
                      </div>
                    )}
                    <div className={classes.menu}>
                      {listSettingIconCheck && (
                        <CustomButton
                          onClick={(event) => {
                            handleToggle(event, "Settings", 0, editColumn);
                          }}
                          btnType="success"
                          variant="text"
                          className={classes.menuIcon}>
                          <SvgIcon viewBox="0 0 14 14" classes={{ root : classes.iconRoot}}>
                            <ListSettingIcon />
                          </SvgIcon>
                        </CustomButton>
                      )}
                      {ddData.length > 0
                        ? ddData.length !== 1 && (
                          <TaskActionMenu
                            data={ddData}
                            element={editColumn}
                            handleToggle={handleToggle}
                            selected={editColumn.statusColor ? editColumn.statusColor : "#bcbcbc"}
                            open={menuVisible}
                            style={classes.menuIcon}
                            onColorUpdate={(color) => {
                              onColorChange(color, editColumn);
                            }}
                            boardPer={boardPer}
                          />
                        )
                        : null}
                    </div>
                  </div>
                  {!boardData.isTemplate && (
                    <NewTicket
                      onAdd={onAddNewTicket}
                      handleUnsortedOpener={handleUnsortedOpener}
                      // disabled={!boardPer.boardDetail.addNewTask.cando}
                      disabled={
                        (!editColumn.isDefault &&
                          !editColumn.isDoneState &&
                          maxTaskObj.maxTaskLimit > 0 &&
                          (maxTaskObj.maxTaskLimit == editColumn.tasksList.length ||
                            editColumn.tasksList.length > maxTaskObj.maxTaskLimit)) ||
                        !boardPer.boardDetail.addNewTask.cando
                      }
                      maxTaskObj={maxTaskObj.maxTaskLimit}
                      tasksList={editColumn.tasksList.length}
                      intl={intl}
                      boardPer={boardPer}
                    />
                  )}
                </>
                <Droppable key={barIndex} droppableId={`${barIndex}`} type="task">
                  {(provided, snapshot) => (
                    <div
                      className={classes.body}
                      ref={provided.innerRef}
                      {...provided.droppableProps}>
                      <Scrollbars
                        autoHide
                        style={{ height: contentHeight - 16 }} // Calculate actual height : content height - margin
                      >
                        {/* if filter contains over due check and the status is in completed state then do not show tasks  */}
                        {filter.indexOf("Over Due") > -1 && editColumn.isDoneState
                          ? null
                          : ticketMaping(editColumn, barIndex)}
                        {provided.placeholder}
                      </Scrollbars>
                    </div>
                  )}
                </Droppable>
              </div>
            )}
          </Draggable>
        </div>
      ) : (
        <div
          style={{
            pointerEvents: pointerEvents ? "none" : "all",
            opacity: pointerEvents ? 0.5 : 1,
          }}>
          <Draggable index={barIndex} draggableId={`${el.id}`} key={el.id}>
            {(provided, snapshot) => (
              <div
                className={classes.taskBar}
                ref={provided.innerRef}
                {...provided.draggableProps}
                onMouseLeave={() => {
                  setMenuVisible(false); // Invisible TaskActionMenu Dropdown Menu when mouse leave
                }}>
                <>
                  <div
                    {...provided.dragHandleProps}
                    className={classes.header}
                    style={{ borderTop: `4px solid ${el.statusColor}` }}>
                    <div className={classes.columnNameCnt}>
                      <CustomTooltip helptext={el.statusTitle} placement="top">
                        <h3
                          title={el.statusTitle}
                          className={classes.caption}
                          onClick={() => {
                            handlClick(el);
                          }}>
                          {el.statusTitle}
                        </h3>
                      </CustomTooltip>
                      <span className={classes.listItemsCount}>
                        {`${el.tasksList.length}`}
                        {maxTaskObj.maxTaskLimit > 0 ? `/${maxTaskObj.maxTaskLimit}` : null}
                      </span>
                    </div>

                    <div className={classes.menu}>
                      {listSettingIconCheck && (
                        <CustomButton
                          onClick={(event) => {
                            handleToggle(event, "Settings", 0, el);
                          }}
                          btnType="success"
                          variant="text"
                          className={classes.menuIcon}>
                          <SvgIcon viewBox="0 0 14 14" classes={{ root : classes.iconRoot}}>
                            <ListSettingIcon />
                          </SvgIcon>
                        </CustomButton>
                      )}
                      {ddData.length > 0
                        ? ddData.length !== 1 && (
                          <TaskActionMenu
                            data={ddData}
                            element={el}
                            handleToggle={handleToggle}
                            selected={el.statusColor ? el.statusColor : "#bcbcbc"}
                            open={menuVisible}
                            style={classes.menuIcon}
                            onColorUpdate={(color) => {
                              onColorChange(color, el);
                            }}
                            boardPer={boardPer}
                          />
                        )
                        : null}
                    </div>
                  </div>
                  {!boardData.isTemplate && (
                    <NewTicket
                      onAdd={onAddNewTicket}
                      handleUnsortedOpener={handleUnsortedOpener}
                      disabled={!boardPer.boardDetail.addNewTask.cando}
                      disabled={
                        (!el.isDefault &&
                          !el.isDoneState &&
                          maxTaskObj.maxTaskLimit > 0 &&
                          (maxTaskObj.maxTaskLimit == el.tasksList.length ||
                            el.tasksList.length > maxTaskObj.maxTaskLimit)) ||
                        !boardPer.boardDetail.addNewTask.cando
                      }
                      maxTaskObj={maxTaskObj.maxTaskLimit}
                      tasksList={el.tasksList.length}
                      intl={intl}
                      boardPer={boardPer}
                    />
                  )}
                </>
                <Droppable key={barIndex} droppableId={`${barIndex}`} type="task">
                  {(provided, snapshot) => (
                    <div
                      className={classes.body}
                      ref={provided.innerRef}
                      {...provided.droppableProps}>
                      <Scrollbars
                        autoHide
                        style={{ height: contentHeight - 16 }} // Calculate actual height : content height - margin
                      >
                        {/* if filter contains over due check and the status is in completed state then do not show tasks  */}
                        {filter.indexOf("Over Due") > -1 && el.isDoneState
                          ? null
                          : ticketMaping(el, barIndex)}
                        {provided.placeholder}
                      </Scrollbars>
                    </div>
                  )}
                </Droppable>
              </div>
            )}
          </Draggable>
        </div>
      )}
    </>
  );
};

TaskBar.defaultProps = {
  barIndex: 0,
  el: {},
  onUpdate: () => { },
  onAdd: () => { },
  theme: {},
  classes: {},
  allTasks: [],
  handleClickListItem: () => { },
  onColumColorChange: () => { },
  onColumTitleChange: () => { },
  UpdateKanbanColumnSetting: () => { },
  openListSettingModal: () => { },
  onUpdateTaskColor: () => { },
  deleteTask: () => { },
  archiveTask: () => { },
  copyTask: () => { },
  handleUnsortedOpener: () => { },
  saveFeatureImage: () => { },
  saveLibraryImage: () => { },
  onDeleteStatus: () => { },
  onDuplicateStatus: () => { },
  filter: [],
  loggedInUser: {},
  boardPer: {},
  intl: {},
};
export default withStyles(taskBarStyles, { withTheme: true })(TaskBar);
