const verifyEmailStyles = theme => ({
  varticalAlignParent: {
    overflowY: "auto",
    height: "100%"
  },
  varticalAlignChild: {
    display: "table",
    width: "100%",
    height: "100%"
  },
  loginMainCnt: {
    height: "100%",
    display: "table-cell",
    verticalAlign: "middle",
    textAlign: "center",
    padding: "60px 0"
  },
  verifyEmailCnt: {
    width: "50%",
    margin: "0 auto",
    maxWidth: 540,
    padding: "30px 60px",
    background: theme.palette.background.default,
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  "@media (max-width: 1024px)": {
    verifyEmailCnt: {
      width: "60%",
      maxWidth: "unset"
    }
  },
  "@media (max-width: 768px)": {
    verifyEmailCnt: {
      width: "80%",
      maxWidth: "unset",
      padding: "30px 20px"
    }
  },
  mailIcon: {
    fontSize: "12px !important",
    marginBottom: 20
  },

  mainHeadingText: {
    marginBottom: 16
  },
  btnCnt: {
    textAlign: "center"
  }
});

export default verifyEmailStyles;
