import React, { useState, useEffect, useMemo, useContext, useRef } from "react";
import CustomDatePicker from "../../components/DatePicker/DatePicker/DatePicker";
import withStyles from "@material-ui/core/styles/withStyles";
import { compose } from "redux";
import { connect, useDispatch } from "react-redux";
import moment from "moment";
import CustomButton from "../../components/Buttons/CustomButton";
import StatusReportsStyle from "./StatusReports.style";
import CustomMultiSelectDropdown from "../../components/Dropdown/CustomMultiSelectWithSearchBarDropdown/Dropdown";
import { generateWorkspaceData } from "../../helper/generateTasksOverviewDropdownData";
import CustomListItem from "../../../src/components/ListItem/CustomListItem";
import ArrowDropDown from "@material-ui/icons/ArrowDropDown";
import DropdownMenu from "../../components/Dropdown/DropdownMenu";
import ClearIcon from "@material-ui/icons/Clear";
import { FetchWorkspaceInfo } from "../../redux/actions/workspace";


const createdDateFilterTypes = [
  {
    label: "today",
    value: "Today",
  },
  {
    label: "yesterday",
    value: "Yesterday",
  },
  {
    label: "currentWeek",
    value: "This Week",
  },
  {
    label: "currentMonth",
    value: "This Month",
  },
  {
    label: "custom",
    value: "Custom",
  },
];

function StatusReports(props) {
  const {
    classes,
    theme,
    profileState: { workspace, activeTeam },
  } = props;
  const dispatchFn = useDispatch();
  const userToken =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZWFtSWQiOiJiMjg2YjQxODIyZDg0NWFlOTY0ODlhNjBiMDYyODBhZSJ9.PpMJjl6nwaSG_23nAlE3hG2PXM2-Cp_JcyyqBNFz-2E"; //localStorage.getItem("token");

  let startDate = moment().format("YYYY-MM-DD");
  let endDate = moment().format("YYYY-MM-DD");

  const [testStartDate, setTestStartDate] = useState(startDate);
  const [testEndDate, setTestEndDate] = useState(endDate);
  const [firstStartDate, setFirstStartDate] = useState("");
  const [firstEndDate, setFirstEndDate] = useState("");
  const [wSpaceData, setWSpaceData] = useState([]);
  const [filterOption, setFilterOption] = useState(createdDateFilterTypes[0].value);
  const [openDropDown, setOpenDropDown] = useState(false);

  const anchorEl = useRef(null);

  const onSelectDateItems = (e, option) => {
    e.stopPropagation();
    setFilterOption(option);
    setOpenDropDown(true);
    creatingDates(option);
  };

  const handleStartDate = date => {
    if (date) {
      const fDate = date.format("YYYY-MM-DD");
      setTestStartDate(fDate);
    }
  };

  const handleEndDate = date => {
    if (date) {
      const eDate = date.format("YYYY-MM-DD");
      setTestEndDate(eDate);
    }
  };

  const handleUpdateFilter = () => {
    loadBoldBI();
  };

  const handleSelectedData = option => {
    setWSpaceData(option);
  };

  const handleClear = () => {
    // reset all filter values and send null to boldBI
    setFirstStartDate("");
    setFirstEndDate("");
    setWSpaceData([]);
    setTestStartDate("");
    setTestEndDate("");
    setFilterOption(createdDateFilterTypes[0].label);
    const workspaceSelectedItem = workspacesDropdownData[0];
    setWSpaceData([workspaceSelectedItem]);
    loadBoldBI();
  };
  const creatingDates = item => {
    let startDate = "";
    let endDate = "";

    switch (item) {
      case "Today":
        startDate = moment().format("YYYY-MM-DD");
        endDate = moment().format("YYYY-MM-DD");
        setTestStartDate(startDate);
        setTestEndDate(endDate);
        break;
      case "Yesterday":
        var date = new Date();
        date.setDate(date.getDate() - 1);
        startDate = moment(date).format("YYYY-MM-DD");
        endDate = moment(date).format("YYYY-MM-DD");
        setTestStartDate(startDate);
        setTestEndDate(endDate);
        break;
      case "This Week":
        const weekstartDate = new Date(moment().startOf("week"));
        const weekEndDate = new Date(moment().endOf("week"));
        startDate = moment(weekstartDate).format("YYYY-MM-DD");
        endDate = moment(weekEndDate).format("YYYY-MM-DD");
        setTestStartDate(startDate);
        setTestEndDate(endDate);
        break;
      case "This Month":
        const monthStartDate = new Date(moment().startOf("month"));
        const monthEndDate = new Date(moment().endOf("month"));
        startDate = moment(monthStartDate).format("YYYY-MM-DD");
        endDate = moment(monthEndDate).format("YYYY-MM-DD");
        setTestStartDate(startDate);
        setTestEndDate(endDate);
        break;
      default:
        console.log("No  Information Found");
        break;
    }
  };
  // Generate workspace dropdown data
  const workspacesDropdownData = useMemo(() => {
    let activeWorkspaces = workspace.filter(w => w.isActive); /** only show active worksapces  */
    let wd = generateWorkspaceData(activeWorkspaces);
    return wd;
  }, workspace);

  const loadBoldBI = () => {
    let workspaceIds;
    if (workspace.length === wSpaceData.length) {
      workspaceIds = "";
    } else {
      workspaceIds = wSpaceData
        .map(x => {
          return x.obj.teamId;
        })
        .toString();
    }
    // Task startdate and endDate and token overwrite lines
    const reportTokenTask =
      userToken != null && userToken != "" ? "&&jwtTokenTask=" + userToken : "";
    const reportStartDateTask =
      testStartDate != null && testStartDate != "" ? "&&startDateTask=" + testStartDate : "";
    const reportEndDateTask =
      testEndDate != null && testEndDate != "" ? "&&endDateTask=" + testEndDate : "";
    const reportworkSpaceIdTask =
      workspaceIds != null && workspaceIds != "" ? "&&workspaceIdTask=" + workspaceIds : "";

    // project startdate and endDate and token overwrite lines
    const reportTokenProject =
      userToken != null && userToken != "" ? "&&jwtTokenProject=" + userToken : "";
    const reportStartDateProject =
      testStartDate != null && testStartDate != "" ? "&&startDateProject=" + testStartDate : "";
    const reportEndDateProject =
      testEndDate != null && testEndDate != "" ? "&&endDateProject=" + testEndDate : "";
    const reportworkSpaceIdProject =
      workspaceIds != null && workspaceIds != "" ? "&&workspaceIdProject=" + workspaceIds : "";

    // Issues startdate and endDate and token overwrite lines
    const reportTokenIssue =
      userToken != null && userToken != "" ? "&&jwtTokenIssue=" + userToken : "";
    const reportStartDateIssue =
      testStartDate != null && testStartDate != "" ? "&&startDateIssue=" + testStartDate : "";
    const reportEndDateIssue =
      testEndDate != null && testEndDate != "" ? "&&endDateIssue=" + testEndDate : "";
    const reportworkSpaceIdIssue =
      workspaceIds != null && workspaceIds != "" ? "&&workspaceIdIssue=" + workspaceIds : "";

    // Timesheetdaily startdate and endDate and token overwrite lines

    const reportTokenTimesheetDaily =
      userToken != null && userToken != "" ? "&&jwtTokenTimesheetDaily=" + userToken : "";
    const reportStartDateTimesheetDaily =
      testStartDate != null && testStartDate != "" ? "&&startDateDaily=" + testStartDate : "";
    const reportEndDateTimesheetDaily =
      testEndDate != null && testEndDate != "" ? "&&endDateDaily=" + testEndDate : "";
    const reportworkSpaceIdTimesheetDaily =
      workspaceIds != null && workspaceIds != "" ? "&&workspaceIdDaily=" + workspaceIds : "";

    // Risk startdate and endDate and token overwrite lines

    const reportStartDateRisk =
      testStartDate != null && testStartDate != "" ? "&&startDateRisk=" + testStartDate : "";

    const reportEndDateRisk =
      testEndDate != null && testEndDate != "" ? "&&endDateRisk=" + testEndDate : "";

    const reportworkSpaceIdRisk =
      workspaceIds != null && workspaceIds != "" ? "&&workspaceIdRisk=" + workspaceIds : "";

    const reportTokenRisk =
      userToken != null && userToken != "" ? "&&jwtTokenRisk=" + userToken : "";

    // actual startDate and endDate and token
    const reportStartDate =
      testStartDate != null && testStartDate != "" ? "&&startDate=" + testStartDate : "";
    const reportEndDate =
      testEndDate != null && testEndDate != "" ? "&&endDate=" + testEndDate : "";
    const reportworkSpaceId =
      workspaceIds != null && workspaceIds != "" ? "&&WorkspaceId=" + workspaceIds : "";
    const reportjwtToken = userToken != null && userToken != "" ? "&&jwtToken=" + userToken : "";

    let boldbiEmbedInstance = BoldBI.create({
      serverUrl: "https://bip.ntaskmanager.com/bi/site/1",
      dashboardId: "e0a72589-963d-45b6-aa41-696baaa77ce5",
      embedContainerId: "dashboard-container", // This should be the container id where you want to embed the dashboard
      filterParameters: `${reportTokenTask}${reportStartDateTask}${reportEndDateTask}${reportworkSpaceIdTask} ${reportTokenProject}${reportStartDateProject}${reportEndDateProject}${reportworkSpaceIdProject}${reportTokenIssue} ${reportStartDateIssue}${reportEndDateIssue}${reportworkSpaceIdIssue}${reportTokenTimesheetDaily}${reportStartDateTimesheetDaily}${reportEndDateTimesheetDaily}${reportworkSpaceIdTimesheetDaily}${reportStartDateRisk}${reportEndDateRisk}${reportworkSpaceIdRisk}${reportTokenRisk}${reportjwtToken}${reportStartDate}${reportEndDate}${reportworkSpaceId}`,
      dashboardSettings: {
        showDashboardParameter: false,
        enableTheme: false,
        enableFilterOverview: false,
        showHeader: false,
      },
      embedType: BoldBI.EmbedType.Component,
      environment: BoldBI.Environment.Enterprise,
      mode: BoldBI.Mode.View,
      height: "800px",
      width: "1735px",
      authorizationServer: {
        url: "https://rapi.ntaskmanager.com/embeddetail/get/",
      },
      expirationTime: "100000",
    });
    boldbiEmbedInstance.loadDashboard();
    console.log(boldbiEmbedInstance);
  };

  useEffect(() => {
    const workspaceSelectedItem = workspacesDropdownData[0];
    setWSpaceData([workspaceSelectedItem]);
    loadBoldBI();
    return () => {
      if (!window.location.pathname.includes("/reports")) {
        FetchWorkspaceInfo(
          "",
          () => { },
          () => { },
          dispatchFn
        );
      }
    }
  }, []);

  const isDescendant = (parent, child) => {
    let node = child.parentNode;
    while (node != null) {
      if (node == parent) {
        return true;
      }
      node = node.parentNode;
    }
    return false;
  };

  return (
    <>
      {openDropDown && (
        <DropdownMenu
          open={openDropDown}
          closeAction={e => {
            let parent = document.querySelector("#dateFilterSelectionMenu");
            let children = e.target;
            const checkIsDescendant = isDescendant(parent, children);
            if (checkIsDescendant) {
              return;
            } else {
              setOpenDropDown(false);
            }
          }}
          anchorEl={anchorEl.current}
          placement="bottom-start"
          disablePortal={false}
          id={"dateFilterSelectionMenu"}
          size={"medium"}>
          {createdDateFilterTypes.map(t => {
            const isSelected = filterOption == t.value;
            return (
              <CustomListItem
                isSelected={isSelected}
                rootProps={{ onClick: e => onSelectDateItems(e, t.value) }}>
                <span>{t.value}</span>
              </CustomListItem>
            );
          })}

          {filterOption && filterOption === "Custom" ? (
            <>
              <div
                style={{
                  margin: "0px 6px",
                  border: "1px solid #DDDDDD",
                  borderRadius: "4px",
                  padding: 6,
                }}>
                <CustomDatePicker
                  // date={testStartDate}
                  label={"From:"}
                  PopperProps={{ disablePortal: true, size: null }}
                  icon={false}
                  timeInput={false}
                  onSelect={handleStartDate}
                  btnProps={{ className: {} }}
                  disabled={false}
                  deleteIcon={false}
                  placeholder={"Select Date"}
                  containerProps={{ style: { alignItems: "center" } }}
                  btnProps={{
                    style: {
                      background: "transparent",
                      border: "none",
                      padding: 0,
                      textAlign: "left",
                    },
                  }}
                  labelProps={{
                    style: {
                      width: "auto",
                      marginTop: 0,
                      marginRight: 5,
                      fontSize: "14px",
                    },
                  }}
                  closeOnDateSelect={true}
                />
              </div>
              <div
                style={{
                  margin: "4px 6px",
                  border: "1px solid #DDDDDD",
                  borderRadius: "4px",
                  padding: 6,
                }}>
                <CustomDatePicker
                  // date={testEndDate}
                  label={"To:"}
                  PopperProps={{ disablePortal: true, size: null }}
                  icon={false}
                  timeInput={false}
                  onSelect={handleEndDate}
                  btnProps={{ className: {} }}
                  disabled={false}
                  deleteIcon={false}
                  placeholder={"Select Date"}
                  containerProps={{ style: { alignItems: "center" } }}
                  btnProps={{
                    style: {
                      background: "transparent",
                      border: "none",
                      padding: 0,
                      textAlign: "left",
                    },
                  }}
                  labelProps={{
                    style: {
                      width: "auto",
                      marginTop: 0,
                      marginRight: 5,
                      fontSize: "14px",
                    },
                  }}
                  closeOnDateSelect={true}
                />
              </div>
            </>
          ) : (
            <></>
          )}
        </DropdownMenu>
      )}
      <div className={classes.mainContainer}>
        <div className={classes.innerSection}>
          <div className={classes.analyticsDatePicker}>
            <div className={classes.dateDropdown}>
              <label>Date Range</label>
              <CustomButton
                onClick={e => {
                  setOpenDropDown(prev => !prev);
                }}
                buttonRef={node => {
                  anchorEl.current = node;
                }}
                variant="contained"
                style={{
                  height: 31,
                  fontWeight: 400,
                  fontFamily: theme.typography.fontFamilyLato,
                  fontSize: "13px",
                  border: "1px solid rgba(234,234,234,1)",
                  padding: "5px 10px",
                  color: "black",
                  background: "white",
                  boxShadow: " none",
                  borderRadius: "4px",
                  textTransform: "capitalize",
                  marginRight: "15px",
                }}>
                {filterOption} <ArrowDropDown style={{ color: "#BFBFBF" }} />
              </CustomButton>
            </div>
            <CustomMultiSelectDropdown
              label="Workspace"
              options={() => workspacesDropdownData}
              option={wSpaceData}
              onSelect={handleSelectedData}
              scrollHeight={400}
              size={"large"}
              buttonProps={{
                variant: "contained",
                btnType: "white",
                labelAlign: "left",
                style: {
                  minWidth: "100%",
                  padding: "3px 4px 3px 8px",
                  height: 31,
                  marginTop: 5,
                  border: "1px solid rgba(234, 234, 234, 1)",
                },
              }}
            />
            <CustomButton
              variant="contained"
              btnType="blue"
              style={{
                marginLeft: "15px",
                padding: "5px 10px",
              }}
              onClick={handleUpdateFilter}>
              Apply Filter
            </CustomButton>
            <CustomButton
              variant="contained"
              btnType="blue"
              style={{
                marginLeft: "15px",
                padding: "5px 10px",
              }}
              onClick={handleClear}>
              Clear
            </CustomButton>
          </div>
        </div>
        <div id="dashboard-container"></div>

      </div>
    </>
  );
}

const mapStateToProps = state => {
  return {
    profileState: state.profile.data,
  };
};

export default compose(
  withStyles(StatusReportsStyle, { withTheme: true }),
  connect(mapStateToProps)
)(StatusReports);
