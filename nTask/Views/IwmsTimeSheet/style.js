const Style = theme => ({
  mainContainer: {
    display: "flex",
    alignItems: "center",
  },
  mainNavBar: {
    width: "100%",
    height: 40,
    border: "1px solid rgba(221, 221, 221, 1)",
  },
  innerContainer: {
    display: "flex",
    paddingTop: 5,
  },
  dateBox: {
    display: "flex",
    alignItems: "center",
    width: "15%",
    justifyContent: "space-evenly",
  },
  filterBtn: {
    border: "1px solid rgba(234, 234, 234, 1)",
    borderRadius: 50,
    fontFamily: "lato",
    fontWeight: 400,
    textTransform: "none",
    background: "rgba(241, 241, 241, 1)",
    padding: " 3px 12px",
  },
  TabStyle: {
    fontSize: "12px !important",
    fontWeight: 500,
    color: theme.palette.text.lightGray,
    textTransform: "none",
    minWidth:"100px"
  },
});
export default Style;
