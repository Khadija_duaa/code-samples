import React, { useEffect, useState } from "react";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import withStyles from "@material-ui/core/styles/withStyles";
import Style from "./style";
import DailyTimesheet from "./Reports/DailyTimesheet";
import WeeklyTimesheet from "./Reports/WeeklyTimesheet";
import ProjectReports from "./Reports/ProjectReports";
import TaskReports from "./Reports/TaskReports";
import IssuesReports from "./Reports/IssuesReports";
import RiskReports from "./Reports/RiskReports";
const TimeSheetTabs = ({ classes }) => {
  const boldBiIds = [
    {
      label: "Daily TimeSheet",
      id: "e70f653b-959b-4e5c-8ed8-9fbdbb3c0b9a",
      value: 0,
    },
    {
      label: "Weekly TimeSheet",
      id: "3eddbd6c-5b60-405b-9891-ef367ba8f638",
      value: 1,
    },
    {
      label: "Project",
      id: "50f9947c-88da-4414-8152-c05ddb642aa2",
      value: 2,
    },
    {
      label: "Tasks",
      id: "9b6e0727-00d5-454b-9b69-44d3cb3f430a",
      value: 3,
    },
    {
      label: " Issues",
      id: "00be059b-51e3-4e07-9148-ca523bc364e2",
      value: 4,
    },
    {
      label: "Risk",
      id: "801a9222-3e4c-4c50-ab6b-85d7e1b458c1",
      value: 5,
    },
  ];

  const [tabValue, setTabValue] = useState(boldBiIds[0]);

  const handleChangeTab = (e, newValue) => {
    e && e.stopPropagation();
    let selectedTab = boldBiIds.find(item => item.value === newValue) || {};
    setTabValue(selectedTab);
  };

  return (
    <>
      <div>
        <Tabs
          style={{ flex: "1px" }}
          value={tabValue.value}
          onChange={handleChangeTab}
          indicatorColor="primary"
          textColor="primary"
          flexStart>
          {boldBiIds.map(item => {
            return <Tab label={item.label} className={classes.TabStyle} />;
          })}
        </Tabs>

        {tabValue.value == 0 && <DailyTimesheet boldUiId={tabValue.id} />}

        {tabValue.value == 1 && <WeeklyTimesheet boldUiId={tabValue.id} />}

        {tabValue.value == 2 && <ProjectReports boldUiId={tabValue.id} />}

        {tabValue.value == 3 && <TaskReports boldUiId={tabValue.id} />}

        {tabValue.value == 4 && <IssuesReports boldUiId={tabValue.id} />}

        {tabValue.value == 5 && <RiskReports boldUiId={tabValue.id} />}
      </div>
    </>
  );
};
TimeSheetTabs.defaultProps = {};
export default withStyles(Style, { withTheme: true })(TimeSheetTabs);
