import React, { useState, useEffect } from "react";
import CustomDatePicker from "../../../components/DatePicker/DatePicker/DatePicker";
import Style from "../style";
import withStyles from "@material-ui/core/styles/withStyles";
import moment from "moment";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import CustomButton from "../../../components/Buttons/CustomButton";

function ProjectReports({ classes, boldUiId }) {
  let startDate = moment()
    .startOf("isoWeek")
    .toISOString();
  let endDate = moment()
    .endOf("isoWeek")
    .toISOString();

  const [firstStartDate, setFirstStartDate] = useState("");
  const [firstEndDate, setFirstEndDate] = useState("");

  const loadBoldBI = (id, startDate, endDate) => {
    let boldbiEmbedInstance = BoldBI.create({
      serverUrl: "https://bi.ntaskmanager.com/bi/site/1",
      dashboardId: id,
      embedContainerId: "dashboard-container", // This should be the container id where you want to embed the dashboard
      filterParameters: `startDate=${startDate}&&endDate=${endDate}&&jwtToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZWFtSWQiOiJiMjg2YjQxODIyZDg0NWFlOTY0ODlhNjBiMDYyODBhZSJ9.PpMJjl6nwaSG_23nAlE3hG2PXM2-Cp_JcyyqBNFz-2E`,
      dashboardSettings: {
        showDashboardParameter: false,
        enableTheme: false,
        enableFilterOverview: false,
      },
      embedType: BoldBI.EmbedType.Component,
      environment: BoldBI.Environment.Enterprise,
      mode: BoldBI.Mode.View,
      height: "900px",
      width: "1735px",
      authorizationServer: {
        url: "https://rapi.ntaskmanager.com/embeddetail/get/",
      },
      expirationTime: "100000",
    });
    boldbiEmbedInstance.loadDashboard();
  };

  const handleStartDate = date => {
    setFirstStartDate(date);
  };

  const handleEndDate = date => {
    setFirstEndDate(date);
  };
  const handleUpdateFilter = () => {
    loadBoldBI(boldUiId,firstStartDate, firstEndDate);
    console.log(firstStartDate,firstEndDate)
  };

  useEffect(() => {
    loadBoldBI(boldUiId, startDate, endDate);
  }, [boldUiId]);

  return (
    <>
        <div className={classes.mainNavBar}>
          <div className={classes.innerContainer}>
            <div className={classes.dateBox}>
              <CustomDatePicker
                btnProps={{
                  style: {
                    border: "1px solid rgba(234, 234, 234, 1)",
                  },
                }}
                btnType="plain"
                placeholder={"start date"}
                startDate={firstStartDate}
                onSelect={handleStartDate}
              />
              <CustomDatePicker
                btnProps={{
                  style: {
                    border: "1px solid rgba(234, 234, 234, 1)",
                  },
                }}
                btnType="plain"
                placeholder={"end date"}
                endDate={firstEndDate}
                onSelect={handleEndDate}
              />
            </div>
            <div>
              <CustomButton onClick={handleUpdateFilter} className={classes.filterBtn}>
                Apply Filter
              </CustomButton>
            </div>
          </div>
        </div>
      <div id="dashboard-container"></div>
    </>
  );
}
ProjectReports.defaultProps = {
  classes: {},
  boldUiId: "",
};
export default withStyles(Style, { withTheme: true })(ProjectReports);
