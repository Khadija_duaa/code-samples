import React, { useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import styles from "./Settings.style.js";
import CustomButton from "../../../components/Buttons/CustomButton";
import CustomIconButton from "../../../components/Buttons/IconButton";
import CustomDrawer from "../../../components/Drawer/CustomDrawer";
import { IconBoardSettings } from "../../../components/Icons/IconBoardSettings";
import { IconSlidePanel } from "../../../components/Icons/IconSlidePanel";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Background from "./Background/Background.view";
import CardSettings from "./CardSettings/CardSettings.view";
import EmailToTask from "./EmailToTask/EmailToTask.view";
import { useSelector } from "react-redux";

function Settings(props) {
  const { boardData, classes } = props;
  const {companyInfo} = useSelector(state => {
    return {
      companyInfo: state.whiteLabelInfo.data,
    };
  });
  const {
    background,
    cardSetting,
    emailTask,
    backGroundApplyforAll,
    cardSettingApplyforAll,
  } = boardData.userPermission?.permission?.board?.boardDetail.boardSetting;

  const [open, setOpen] = useState(false);
  const tabsArr = [];
  if (background.cando && backGroundApplyforAll.cando) {
    tabsArr.push("background");
  }
  if (cardSetting.cando && cardSettingApplyforAll.cando) {
    tabsArr.push("cardSettings");
  }
  if (emailTask.cando) {
    tabsArr.push("emailToTask");
  }
  const [tab, setTab] = useState(tabsArr.length ? tabsArr[0] : "");
  const taskLabelSingle = companyInfo?.task?.sName;
  const taskLabelMulti = companyInfo?.task?.pName;
  return (
    <div className={classes.root}>
      <CustomIconButton
        iconBtn
        style={{
          padding: "5px 15px",
          fontSize: "12px",
          fontWeight: 500,
          height: 32,
          marginLeft: 10,
          backgroundColor: !boardData.background?.backGroundImage ? "#fff" : "#f3f3f380",
          color: !boardData.background?.backGroundImage ? "#333333" : "black",
        }}
        variant="outlined"
        onClick={() => setOpen(true)}
        btnType="filledWhite">
        <IconBoardSettings
          style={{ marginRight: 2, padding: 5 }}
          color={!boardData.background?.backGroundImage ? "#85898b" : "black"}
        />
        Settings
      </CustomIconButton>

      {open ? (
        <CustomDrawer
          hideCloseIcon
          drawerProps={{ className: classes.drawer }}
          open={open}
          closeDrawer={() => setOpen(false)}
          hideHeader={true}
          drawerWidth={tab === "emailToTask" ? 600 : 416}>
          <div className={classes.drawerContent}>
            <Grid container justify="space-between" alignItems="center">
              <Grid item>
                <Typography variant="h3" className={classes.title}>Board Setting</Typography>
              </Grid>
              <Grid item>
                <CustomButton
                  iconBtn
                  style={{ minWidth: "unset", marginLeft: 5, padding: ".5rem" }}
                  onClick={() => setOpen(false)}>
                  <IconSlidePanel />
                </CustomButton>
              </Grid>
            </Grid>

            <Tabs
              value={tab}
              indicatorColor="primary"
              textColor="primary"
              onChange={(e, value) => setTab(value)}
              className={classes.tabs}>
              {(background.cando && !boardData.background?.applyForAll) ||
              (background.cando &&
                (boardData.userPermission.roleId.includes("001") ||
                  boardData.userPermission.roleId.includes("002") ||
                  boardData.userPermission.roleId.includes("101"))) ? (
                <Tab classes={{root: classes.tab, selected: classes.tabSelected}}  value="background" label="Background" />
              ) : null}
              {(cardSetting.cando && !boardData.cardSetting?.applyForAll) ||
              (cardSetting.cando &&
                (boardData.userPermission.roleId.includes("001") ||
                  boardData.userPermission.roleId.includes("002") ||
                  boardData.userPermission.roleId.includes("101"))) ? (
                <Tab classes={{root: classes.tab, selected: classes.tabSelected}} value="cardSettings" label="Card Settings" />
              ) : null}
              {emailTask.cando && (
                <Tab classes={{root: classes.tab, selected: classes.tabSelected}} value="emailToTask" label={`Email To ${taskLabelSingle ? taskLabelSingle : "Task"}`} />
              )}
            </Tabs>
            <div className={classes.backgroundTabCnt} hidden={tab !== "background"}>
              <Background boardData={boardData} />
            </div>
            <div className={classes.cardSettingsTabCnt} hidden={tab !== "cardSettings"}>
              <CardSettings boardData={boardData} />
            </div>
            <div className={classes.emailCnt} hidden={tab !== "emailToTask"}>
              <EmailToTask boardData={boardData} />
            </div>
          </div>
        </CustomDrawer>
      ) : null}
    </div>
  );
}
export default withStyles(styles, { withTheme: true })(Settings);
