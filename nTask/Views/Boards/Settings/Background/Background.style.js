const backgroundStyles = theme => ({
  tab: {
    minWidth: "unset",
    flexGrow: 1,
    whiteSpace: "nowrap",
    textTransform: "none",
  },
  tabSelected: {
    background: "unset !important",
    boxShadow: "unset !important",
  },
  applySettingOutCnt: {
    position: "absolute",
    bottom: 77,
    left: 0,
    right: 0,

    background: theme.palette.common.white,
  },
  applySettingHeading: {
    fontFamily: "Lato, sans-serif !important"
  },
  fontFamily: {
    fontFamily: "Lato, sans-serif !important" 
  },
  applySettingCnt: {
    background: theme.palette.common.white,
    display: "flex",
    alignItems: "flex-start",
    padding: "12px 25px 10px 25px",
    boxShadow: "0px -1px 0px #D8DEE1",
    position: "fixed",
  },
});

export default backgroundStyles;
