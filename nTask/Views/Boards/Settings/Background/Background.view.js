import React, { useEffect, useState } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import CustomTabs from "../../../../components/CustomTab/CustomTabs.view";
import CustomTab from "../../../../components/CustomTab/CustomTab.view";
import styles from "./Background.style";
import ColorImagesModal from "../../../../components/ColorImagesModal";
import DefaultCheckbox from "../../../../components/Form/Checkbox";
import Typography from "@material-ui/core/Typography";
import { useDispatch } from "react-redux";
import {
  deleteBoardImage,
  getBoardImages,
  setBoardBackground,
  uploadCustomBoardImage,
} from "../../../../redux/actions/boards";
import { uploadFileToS3 } from "../../../../redux/actions/projects";
import UploadFile from "../UploadFile";
import BackgroundImagesSelector from "./BackgroundImagesSelector/BackgroundImagesSelector.view";
import { withSnackbar } from "notistack";
import { compose } from "redux";

function Background({ boardData, classes, enqueueSnackbar }) {
  const dispatch = useDispatch();
  const {
    backGroundApplyforAll,uploadImage,deleteImage
  } = boardData?.userPermission?.permission?.board?.boardDetail?.boardSetting;
  const [tab, setTab] = useState("Gallery");
  const [background, setBackground] = useState("");
  const [applyForAll, setApplyForAll] = useState(false);
  const [bgColor, setBgColor] = useState("");
  const [statusColorChecked, setStatusColorChecked] = useState("");
  const [featuredImage, setFeaturedImage] = useState("");
  const [images, setImages] = useState([]);

  useEffect(() => {
    getBoardImages(
      response => {
        setImages(response.data);
      },
      () => {}
    );
    setApplyForAll(boardData.background.applyForAll);
    setStatusColorChecked(boardData.background.useProjectStatusColor);
  }, []);

  useEffect(() => {
    setBgColor(boardData?.background?.backGroundColor);
  }, [boardData?.background?.backGroundColor]);

  const handleDeleteBoardImage = image => {
    setImages(prev => prev.filter(img => img.id !== image.id));

    deleteBoardImage(
      image.fileId,
      data => {
        showSnackBar("Image Deleted Sucessfully.", "success");
      },
      () => {
        setImages(prev => [image, ...prev]);
      }
    );
  };

  //Handle Select Image
  const handleImageSelect = selectedImage => {
    const imageUrl = selectedImage && selectedImage.urls ? selectedImage.urls.full : selectedImage;
    setBackground(imageUrl);
    setBoardBackground(
      {
        backGroundColor: "",
        ApplyForAll: applyForAll,
        backGroundImage: imageUrl,
        boardId: boardData.projectId,
        useProjectStatusColor: false,
      },
      dispatch,
      () => {},
      () => {}
    );
  };
  //Handle Select Color
  const handleColorSelect = selectedColor => {
    setBgColor(selectedColor);
    setBoardBackground(
      {
        backGroundColor: selectedColor,
        ApplyForAll: applyForAll,
        backGroundImage: "",
        boardId: boardData.projectId,
        useProjectStatusColor: false,
      },
      dispatch,
      () => {},
      () => {}
    );
  };
  const handleApplyForAll = () => {
    setApplyForAll(!applyForAll);
    setBoardBackground(
      { ApplyForAll: !applyForAll, boardId: boardData.projectId },
      dispatch,
      () => {},
      () => setApplyForAll(!applyForAll)
    );
  };
  //Handle use project status Color
  const handleSetProjectStatusColor = () => {
    setStatusColorChecked(!statusColorChecked);
    setBoardBackground(
      {
        backGroundColor: "",
        ApplyForAll: applyForAll,
        backGroundImage: "",
        boardId: boardData.projectId,
        useProjectStatusColor: !statusColorChecked,
      },
      dispatch,
      () => {},
      () => {}
    );
  };

  const handleSaveAttachment = attachment => {
    setFeaturedImage(attachment);
  };
  const handleUploadFile = (featuredImage, callback, file, failureCallback) => {
    if (file.size > 5242880) {
      showSnackBar("Oops ! The file you are trying to upload is greater than 5mb", "error");
      failureCallback();
      return;
    }
    if (images.length == 10) {
      showSnackBar(
        "Oops ! You have already reached maximum background image limit, please delete any old image to upload new image.",
        "error"
      );
      return;
    }

    uploadFileToS3(
      featuredImage,
      dispatch,
      res => {
        callback(res);
        const filePayload = { projectId: boardData.projectId, ApplyForAll: applyForAll, ...res[0] };
        uploadCustomBoardImage(
          filePayload,
          //success
          res => {
            setImages(res.data);
          },
          //failure
          res => {}
        );
        // handleImageSelect(res[0].publicURL);
      },
      () => {}
    );
  };
  //Handle Image click
  const handleImageClick = image => {
    handleImageSelect(image.publicURL);
  };
  const showSnackBar = (snackBarMessage, type) => {
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type || "info",
      }
    );
  };
  return (
    <>
      <CustomTabs value={tab} fullWidth={true} onChange={(e, value) => setTab(value)}>
        <CustomTab  classes={{root: classes.tab, selected: classes.tabSelected}} disableRipple={true} value="Gallery" label="Gallery" />
        <CustomTab  classes={{root: classes.tab, selected: classes.tabSelected}} disableRipple={true} value="Custom Background" label="Custom Background" />
      </CustomTabs>
      {tab == "Gallery" ? (
        <>
          <ColorImagesModal
            modal={false}
            handleImageSelect={handleImageSelect}
            handleColorSelect={handleColorSelect}
            viewMode={"edit"}
            size="large"
            useProjectStatusColor={statusColorChecked}
            handleClickStatusSelect={handleSetProjectStatusColor}
            selectedColor={bgColor}
          />
        </>
      ) : (
        <>
          {/*<UploadBoardImage setAttachment={handleSaveAttachment}/>*/}
          {uploadImage.cando && <UploadFile uploadCallback={handleUploadFile} disableUpload={images.length == 10} />}
          <BackgroundImagesSelector
            handleDeleteBoardImage={handleDeleteBoardImage}
            images={images}
            handleImageClick={handleImageClick}
            deletePer={deleteImage.cando}
          />
        </>
      )}
      {backGroundApplyforAll.cando && (
        <div className={classes.applySettingOutCnt}>
          <div className={classes.applySettingCnt}>
            <DefaultCheckbox
              checkboxStyles={{ padding: "0px 5px 0 0 " }}
              checked={applyForAll}
              onChange={handleApplyForAll}
              fontSize={20}
              color={"#0090ff"}
            />

            <div>
              <Typography variant={"h5"} className={classes.fontFamily}>
                Apply Background for All
              </Typography>
              <Typography variant={"body2"} className={classes.fontFamily}>
                Selected color/image will be set as default for this board for all users. Only project managers can update this setting.
              </Typography>
            </div>
          </div>
        </div>
      )}
    </>
  );
}
export default compose(withSnackbar, withStyles(styles, { withTheme: true }))(Background);
