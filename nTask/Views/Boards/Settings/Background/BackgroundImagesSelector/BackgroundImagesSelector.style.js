const backgroundImgStyles = theme => ({
  root: {},
    imagecontainer: {
      position: "relative",
      cursor: "pointer",
      borderRadius: 4,
      "&:hover .desc": {
        opacity: 1,
      },
    },
    desc: {
      padding: "4px 7px",
      background: "#000000b3",
      color: theme.palette.common.white,
      opacity: 0,
      position: "absolute",
      bottom: 0,
      left: 0,
      right: 0,
      transition: "opacity .2s linear",
      textDecoration: "underline",
      fontSize: "13px !important",
      cursor: "pointer",
    },
    image: {
      objectFit: "cover",
      maxWidth: "100%",
      height: "auto",
      width: "100%",
      height: 94,
    },
});

export default backgroundImgStyles;