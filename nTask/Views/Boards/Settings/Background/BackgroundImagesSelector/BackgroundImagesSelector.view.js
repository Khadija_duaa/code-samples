import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import React, { useEffect, useState } from "react";
import { getBoardImages, deleteBoardImage } from "../../../../../redux/actions/boards";
import styles from "./BackgroundImagesSelector.style";

function BackgroundImagesSelector({ handleDeleteBoardImage, images, handleImageClick, classes, deletePer }) {
  return (
    <Grid container spacing={0} className={classes.root} alignItems="center">
      {images?.map(image => (
        <Grid key={image.id} xs={6} item style={{ padding: 8 }}>
          <div className={classes.imagecontainer}>
            <img
              src={image.publicURL}
              alt={image.fileName}
              className={classes.image}
              onClick={() => handleImageClick(image)}
            />
            {deletePer && <Typography
              className={`desc ${classes.desc}`}
              color="inherit"
              align="right"
              onClick={() => handleDeleteBoardImage(image)}>
              Delete Image
            </Typography>}
          </div>
        </Grid>
      ))}
    </Grid>
  );
}

export default withStyles(styles, { withTheme: true })(BackgroundImagesSelector);
