const uploadStyle = theme => ({
  uploadCnt: {
    margin: '12px 0'
  },
  uploadIcon: {
    fontSize: "36px !important",
    marginBottom: 12
  },
  csvConfirmationIconCnt: {
    border: `2px solid ${theme.palette.border.lightBorder}`,
  },
  browseText: {
    // color: theme.palette.primary.light,
    color: theme.palette.border.blue,
    cursor: "pointer",
  },
  dragDropHeading: {
    fontSize: "14px !important",
    marginBottom: 12
  },
  subText: {
    fontSize: "13px !important",
    color: theme.palette.text.medGray,
    marginBottom: 5
  },
  exportCnt: {
    border: `2px dashed ${theme.palette.border.lightBorder}`,
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    padding: 15,
    cursor: 'pointer',
    '&:hover':{
      background: theme.palette.border.greenBorder + '0f',
      border: `2px dashed ${theme.palette.border.greenBorder}`
    }
  },
  dragnDropMessageText: {
    color: theme.palette.text.secondary,
    fontWeight: theme.typography.fontWeightLight,
    fontFamily: theme.typography.fontFamilyLato,
    margin: "10px 0 0 0",
    fontSize: "15px !important",
  },
  clProgressValue: {
    position: "absolute",
    right: -10,
    left: 0,
    textAlign: "center",
    top: 35,
    fontSize: "28px !important",
    fontWeight: theme.typography.fontWeightMedium,
  },
  centerAlignDialogContent: {
    textAlign: 'center'
  },
  importBulkProgressCnt: {
    borderWidth: 2,
    borderColor: theme.palette.border.lightBorder,
    borderStyle: "solid",
    borderRadius: 5,
    padding: 50,
    background: theme.palette.background.paper,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginBottom: 30,
  },
});

export default uploadStyle;