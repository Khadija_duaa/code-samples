const isOldSidebarView = localStorage.getItem("oldSidebarView") == 'true'

const boardSettingsStyle = theme => ({
  backgroundTabCnt: {
    marginTop: "1rem",
    height: isOldSidebarView ? "calc(100vh - 334px)" : "calc(100vh - 300px)" ,
    overflowY: "auto",
    marginRight: -8,
    paddingRight: 6,
  },
  title: {
    fontFamily: "Lato, sans-serif !important"
  },
  cardSettingsTabCnt: {
    height: isOldSidebarView ? "calc(100vh - 318px)" : "calc(100vh - 284px)" ,
    overflowY: "auto",
    marginRight: -18,
    padding: "7px 8px 35px 0px",
  },
  emailCnt: {
    marginTop: "1rem",
  },
  root: {
    position: "relative",
    "& *": {
      fontFamily: "Lato, sans-serif",
    },
  },
  tab: {
    minWidth: "unset",
    flexGrow: 1,
    fontSize: "14px !important",
    whiteSpace: "nowrap",
    textTransform: "none",
  },
  tabSelected: {
    background: "unset !important",
    boxShadow: "unset !important",
  },
  drawer: {
    position: "fixed",
    zIndex: 1000,
  },
  drawerContent: {
    height: "100%",
    padding: "1rem",
    position: "relative",
  },
  tabs: {
    "& > div": { borderBottom: "1px solid #DFE5E8" },
  },
});

export default boardSettingsStyle;
