const emailCmpStyles = theme => ({
  codeArea: {
    padding: "1rem",
    fontSize: "13px !important",
    background: "#F1F1F1",
    borderRadius: 6,
    opacity: 1,
    display: "flex",
  },
  copyBtn: {
    marginLeft: 10,
  },
  emailContent: {
    wordBreak: "break-word",
  },
  image: {
    maxWidth: "100%",
    height: "auto",
    width: "100%",
  },
  learnmore: {
    cursor: "pointer",
    fontSize: "14px !important",
    color: theme.palette.text.linkBlue,
    textDecoration: "underline",
    '& a': {
      textDecoration: 'none',
      color: theme.palette.text.linkBlue,
    }
  },
  generate: {
    cursor: "pointer",
    marginBottom: "1rem",
    marginTop: ".5rem",
    fontSize: "14px !important",
    textDecoration: "underline",
  },
});

export default emailCmpStyles;
