import React, { useEffect, useRef, useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import CustomButton from "../../../../components/Buttons/CustomButton";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import sendemail from "../../../../assets/images/sendemail.svg";
import styles from "./EmailToTask.style";
import { useSelector } from "react-redux";
import {
  getBoardEmailtotaskMail,
  updateBoardEmailtotaskMail,
} from "../../../../redux/actions/boards";

function EmailToTask(props) {
  const { boardData, classes } = props;
  const { boardPer, workspaceid, companyInfo } = useSelector(state => {
    return {
      boardPer: state.workspacePermissions.data.board,
      workspaceid : state.profile.data.loggedInTeam,
      companyInfo: state.whiteLabelInfo.data,
    };
  });
  const { generateNewEmail } = boardPer?.boardDetail?.boardSetting;
  const codeElem = useRef();
  const [email, setEmail] = useState();

  if (!email) {
    getBoardEmailtotaskMail(
      workspaceid,
      boardData.projectId,
      response => {
        setEmail(response.data.project);
      },
      () => {}
    );

    return <div>Loading</div>;
  }

  const handleUpdateEmail = () => {
    updateBoardEmailtotaskMail(
      workspaceid,
      boardData.projectId,
      response => {
        setEmail(response.data.project);
      },
      () => {}
    );
  };
  const taskLabelSingle = companyInfo?.task?.sName;
  const taskLabelMulti = companyInfo?.task?.pName;
  return (
    <div>
      <Typography variant="h5" style={{ marginBottom: "1rem" }}>
        Send Email To
      </Typography>
      <div className={classes.codeArea}>
        <div ref={element => (codeElem.current = element)} className={classes.emailContent}>
          {email}
        </div>
        <CustomButton
          variant="contained"
          btnType="green"
          className={classes.copyBtn}
          onClick={({ target }) => {
            if (codeElem.current) {
              navigator.clipboard.writeText(codeElem.current.innerText);
              target.innerText = "Copied";
              setTimeout(() => (target.innerText = "Copy"), 2000);
            }
          }}>
          Copy
        </CustomButton>
      </div>

      {generateNewEmail.cando && (
        <Typography
          color="primary"
          onClick={handleUpdateEmail}
          className={classes.generate}
          align="right">
          Generate New Email Address
        </Typography>
      )}

      {taskLabelSingle ? null :
        <><img src={sendemail} alt="sendemail" className={classes.image} />

      <Typography className={classes.learnmore}>
        <a href={'#'}>Learn more about Create Task from Email in Boards</a>
      </Typography></>}
    </div>
  );
}
export default withStyles(styles, { withTheme: true })(EmailToTask);
