import React, { Component, Fragment, useState } from "react";
import SvgIcon from "@material-ui/core/SvgIcon";
import CircularProgress from "@material-ui/core/CircularProgress";
import ExcelIcon from "../../../components/Icons/ExcelIcon";
import dialogStyles from "../../../assets/jss/components/dialog";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import DefaultButton from "../../../components/Buttons/DefaultButton";
import Dropzone from "react-dropzone";
import Typography from "@material-ui/core/Typography";
import { Circle } from "rc-progress";
import helper from "../../../helper";
import { ImportFile } from "../../../redux/actions/ImportExport";
import apiInstance, { CancelToken } from "../../../redux/instance";
import NotificationMessage from "../../../components/NotificationMessages/NotificationMessages";
import getErrorMessages from "../../../utils/constants/errorMessages";
import UploadFileCloudIcon from "../../../components/Icons/uploadFileCloud";
import styles from "./uploadFile.style";
import { defaultTheme as theme} from "../../../assets/jss/theme";

function UploadFile(props) {
  // const classes = useStyles();
  const { open, showMessage, closeAction, handleRefresh, uploadCallback, classes, disableUpload } = props;
  const [state, setState] = useState({
    dnd: true,
    progress: 0,
    cancelRequest: null,
    successfullyImortedCount: 0,
    totalAdded: 0,
    errorMesseage: "",
  });
  const { dnd, progress,
    cancelRequest,
    successfullyImortedCount,
    totalAdded,
    errorMesseage,
  } = state;
  // const [dnd, setDnd] = useState(true);

  const onHandleDrop = files => {

    if (files.length) {
      manageImports(0, files);

    }
  };
  const manageImports = (index, files) => {
    const file = files[index];
    if(!disableUpload) {
      setState({ ...state, dnd: false });
    }
    const successApiCallback = response => {
        const successfullyImortedCount = response;
        // closeAction();
        // showMessage(`${successfullyImortedCount} ${type}s have been added to System.`);
        // handleRefresh();
        if (index === files.length - 1) {
          setState({
            ...state,
            progress: 100,
            cancelRequest: null,
            successfullyImortedCount,
            totalAdded: state.totalAdded + successfullyImortedCount,
          });
        }
        else {
          // self.manageImports(index + 1, files);
          setState({
            ...state,
            totalAdded: state.totalAdded + successfullyImortedCount,
          });
        }
        setState({ ...state, errorMesseage: "" });

    };
    const failureCallback = (error) => {
      setState({ ...state, dnd: true });
    }
    // const failureApiCallback = (error) => {
    //   if (error.status && error.status === 500) {
    //     if (index === files.length - 1) setState(initialState);
    //     setState({ errorMesseage: error.data.message });
    //   } else if (
    //     (error.status && error.status === 422) ||
    //     error.status === 406
    //   ) {
    //     if (index === files.length - 1) setState(initialState);
    //     setState({ errorMesseage: error.data.message });
    //   } else {
    //     if (index === files.length - 1) setState(initialState);
    //     setState({ errorMesseage: error.data.message });
    //   }
    // };

    let formdata = new FormData();

    formdata.append("file", file, file.name.replace(/\s+/g, '-').toLowerCase());
    uploadCallback(formdata, successApiCallback, file, failureCallback)
    // const config = {
    //   headers: { "Content-Type": "application/x-www-form-urlencoded" },
    //   cancelToken: new CancelToken(function executor(c) {
    //     setState({ ...state, cancelRequest: c });
    //   }),
    // };
  };
  const handleDragOver = event => {
    event.stopPropagation();
  };

  const handleCancelRequest = () => {
    const { cancelRequest } = state;
    if (cancelRequest) cancelRequest("User cancelled upload.");
  };

  const handleInvalidFileDrop = () => {
    // showMessage(getErrorMessages().INVALID_IMPORT_EXTENSION, "error");
  };


  const dndStyles = {
    position: "relative",
    width: "100%",
    borderWidth: 2,
    borderColor: theme.palette.border.lightBorder,
    borderStyle: "dashed",
    borderRadius: 5,
    padding: 50,
    background: theme.palette.background.paper,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginBottom: 30,
  };
  const dndActiveStyle = {
    borderColor: theme.palette.border.darkBorder,
  };

  return (
    <div className={classes.uploadCnt}>
      {errorMesseage ? (
        <NotificationMessage
          type="failure"
          iconType="failure"
          style={{ marginBottom: 20, width: "100%" }}>
          {errorMesseage}
        </NotificationMessage>
      ) : null}
      {dnd ? (
        <Dropzone
          onDrop={onHandleDrop}
          onDragOver={handleDragOver}
          onDropRejected={handleInvalidFileDrop}
          accept="image/jpeg,image/png,image/gif"
          activeStyle={dndActiveStyle}
          style={dndStyles}
          // disableClick
          onClick={evt => evt.preventDefault()}>
          {({ getRootProps, getInputProps, open }) => {
            return (

                <div className={classes.exportCnt} {...getRootProps()}>
                 <UploadFileCloudIcon className={classes.uploadIcon}/>

                <Typography variant="h5" className={classes.dragDropHeading} > Drag and drop an image here or browse</Typography>
                  <Typography variant='body1' className={classes.subText}>Supported formats: JPG, PNG, GIF</Typography>
                  <Typography variant='body1' className={classes.subText}>Maximum file size: 5MB</Typography>
                <input {...getInputProps()} />
                <p className={classes.dragnDropMessageText}>

                </p>

          </div>
            );
          }}
        </Dropzone>
      ) : (
        <div className={classes.importBulkProgressCnt}>
          <div
            style={{
              width: 100,
              position: "relative",
              marginBottom: 30,
              display: "flex",
              flexDirection: "row",
              justifyContent: "center",
            }}>
            {progress < 100 ? (
              <CircularProgress className={classes.progress} />
            ) : (
              <>
                <Circle
                  percent={progress}
                  strokeWidth="3"
                  trailWidth=""
                  trailColor="#dedede"
                  strokeColor="#0090ff"
                />
                <span className={classes.clProgressValue}>{progress}%</span>
              </>
            )}
          </div>

          <Typography variant="h3">
            {progress < 100 ? "Uploading..." : "Congratulations!"}
          </Typography>
          <p className={classes.dragnDropMessageText}>
            {progress < 100
              ? "Please wait! It will take a moment."
              : `${totalAdded}/${totalAdded} have been imported to nTask successfully.`}
          </p>
        </div>
      )}
    </div>
  );
}

export default withStyles(styles, { withTheme: true })(UploadFile);
