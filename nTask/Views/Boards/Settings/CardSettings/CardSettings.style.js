const cardStyles = theme => ({
  root: {
    border: `1px solid ${theme.palette.background.grayLighter}`,
    borderRadius: 6,
    marginBottom: "0.5rem",
    height: "38px"
  },
  label: {
    flexGrow: 1,
  },
  checkboxLabel: {
    fontSize: "14px !important",
    color: "#161717",
  },
  fontFamily: {
    fontFamily: "Lato, sans-serif !important" 
  },
  applySettingOutCnt: {
    position: "absolute",
    bottom: 77,
    left: 0,
    right: 0,

    background: theme.palette.common.white,
  },
  applySettingCnt: {
    background: theme.palette.common.white,
    display: "flex",
    alignItems: "flex-start",
    padding: "12px 25px 10px 25px",
    boxShadow: "0px -1px 0px #D8DEE1",
    position: "fixed",
  },
  icon: {
    display: "flex",
    flexGrow: 0,
  },
});

export default cardStyles;
