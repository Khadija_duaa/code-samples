import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import styles from "./CardSettings.style";
import DefaultSwitch from "../../../../components/Form/Switch";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { useDispatch, useSelector } from "react-redux";
import DefaultCheckbox from "../../../../components/Form/Checkbox";

import { IconUser } from "../../../../components/Icons/iconUser";
import { IconAttachment } from "../../../../components/Icons/icon_cardsettings_Attachment";
import { IconAssignee } from "../../../../components/Icons/icon_cardsettings_Assignee";
import { IconDates } from "../../../../components/Icons/icon_cardsettings_Dates";
import { IconImage } from "../../../../components/Icons/icon_cardsettings_Image";
import { IconSubtask } from "../../../../components/Icons/icon_cardsettings_Subtask";
import { IconToDoList } from "../../../../components/Icons/icon_cardsettings_ToDoList";

import IconMeetings from "../../../../components/Icons/IconMeetings";
import IconIssues from "../../../../components/Icons/IconIssues";
import IconRisks from "../../../../components/Icons/IconRisks";
import IconChat from "../../../../components/Icons/IconChat";

import { updateBoardCardSetting } from "../../../../redux/actions/boards";
import { getMenuIcons } from "../../../Sidebar/constants.js";
import SvgIcon from "@material-ui/core/SvgIcon";

const SETTINGS = [
  {
    key: "createdBy",
    label: "Created By",
    icon: <IconUser style={{ padding: "2px" }} />,
  },
  {
    key: "plannedDates",
    label: "Planned Dates",
    icon: <IconDates style={{ padding: "2px" }} />,
  },
  {
    key: "assignees",
    label: "Assignees",
    icon: <IconAssignee style={{ padding: "2px" }} />,
  },
  {
    key: "featuredImage",
    label: "Featured Image",
    icon: <IconImage style={{ padding: "2px" }} />,
  },
  {
    key: "toDoListCount",
    label: "To-do List Count",
    icon: <IconToDoList style={{ padding: "2px" }} />,
  },
  {
    key: "subTaskCount",
    label: "Subtask Count",
    icon: <IconSubtask style={{ padding: "2px" }} />,
  },
  {
    key: "issuesCount",
    label: "Issue Count",
    icon: (
      <SvgIcon style={{ padding: "2px" }} viewBox="0 0 14 15.256">
        <IconIssues />
      </SvgIcon>
    ),
  },
  {
    key: "risksCount",
    label: "Risk Count",
    icon: (
      <SvgIcon style={{ padding: "2px" }} viewBox="0 0 15 13.127">
        <IconRisks />
      </SvgIcon>
    ),
  },
  {
    key: "attachmentsCount",
    label: "Attachment Count",
    icon: <IconAttachment style={{ padding: "2px" }} />,
  },
  {
    key: "commentsCount",
    label: "Comments Count",
    icon: (
      <SvgIcon style={{ padding: "2px" }} viewBox="0 0 16 16.297">
        <IconChat />
      </SvgIcon>
    ),
  },
  {
    key: "meetingsCount",
    label: "Meeting Count",
    icon: (
      <SvgIcon style={{ padding: "2px" }} viewBox="0 0 15 15.004">
        <IconMeetings />
      </SvgIcon>
    ),
  },
];

function CardSettings(props) {
  const dispatch = useDispatch();
  const { boardData, classes } = props;
  const { companyInfo } = useSelector(state => {
    return {
      companyInfo: state.whiteLabelInfo.data,
    };
  });
  const {
    cardSettingApplyforAll,
  } = boardData?.userPermission?.permission?.board?.boardDetail?.boardSetting;
  const [settings, setSettings] = useState(boardData.cardSetting);
  const [applyForAll, setApplyForAll] = useState(boardData.cardSetting.applyForAll);

  const handleSettingChange = (key, checked) => {
    setSettings(prev => {
      const newval = { ...prev };
      newval[key] = checked;
      return newval;
    });
    updateBoardCardSetting(
      dispatch,
      boardData.projectId,
      { [key]: checked },
      () => { },
      () => {
        setSettings(prev => {
          const newval = { ...prev };
          newval[key] = !checked;
          return newval;
        });
      }
    );
  };

  const handleApplyForAll = () => {
    setApplyForAll(prev => !prev);
    updateBoardCardSetting(
      dispatch,
      boardData.projectId,
      { applyForAll: !applyForAll },
      () => { },
      () => {
        setApplyForAll(prev => !prev);
      }
    );
  };
  const isIssueHidden = companyInfo?.issue?.isHidden;
  const isMeetingHidden = companyInfo?.meeting?.isHidden;
  const isRiskHidden = companyInfo?.risk?.isHidden;
  const isTaskHidden = companyInfo?.task?.isHidden;
  const viewsToHide = { meetingsCount: isMeetingHidden, issuesCount: isIssueHidden, risksCount: isRiskHidden, subTaskCount: isTaskHidden };
  const cardSettingAfterFilter = SETTINGS.filter(s => !viewsToHide[s.key])
  return (
    <>
      {cardSettingAfterFilter.map(({ key, label, icon }) => (
        <Grid key={label} container className={classes.root} alignItems="center">
          <Grid className={classes.icon} item style={{ padding: '0 14px' }}>
            {icon}
          </Grid>
          <Grid className={classes.label} item style={{ fontSize: '16px' }}>
            {label}
          </Grid>
          <Grid className={classes.switch} item xs={2}  >
            <DefaultSwitch
              size={"small"}
              checked={settings[key]}
              onChange={(_, checked) => handleSettingChange(key, checked)}
              rootProps={{
                button: false,
                disableRipple: true,
                style: { cursor: "default" },
              }}
            />
          </Grid>
        </Grid>
      ))}
      {cardSettingApplyforAll.cando && (
        <div className={classes.applySettingOutCnt}>
          <div className={classes.applySettingCnt}>
            <DefaultCheckbox
              checkboxStyles={{ padding: "0px 5px 0 0 " }}
              checked={applyForAll}
              onChange={handleApplyForAll}
              fontSize={20}
              color={"#0x090ff"}
            />

            <div>
              <Typography variant={"h5"} className={classes.applySettingHeading}>
                Apply Settings for All
              </Typography>
              <Typography variant={"body2"} className={classes.applySettingSubTitle}>
                Selected settings will be set as default for this board for all the users. Only project managers can update this setting.



              </Typography>
            </div>
          </div>
        </div>
      )}
    </>
  );
}

export default withStyles(styles, { withTheme: true })(CardSettings);
