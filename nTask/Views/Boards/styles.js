const drawerWidth = 350;
import bgPattern from "../../assets/images/unplanned/dots_pattern.png";

const boardsStyle = theme => ({
  rootCustomButton: {
  //  fontSize: "7px !important"
  },
  root: {
    display: "flex",
  },
  menuRoot: {
    minHeight: "28px",
    lineHeight: "1.3"
  },
  content: {
    flexGrow: 1,
    // marginRight: -drawerWidth,
    // maxWidth: "90vw",
    width: "100%"
  },
  drawerHeader: {
    display: "flex",
    alignItems: "center",
    ...theme.mixins.toolbar,
    justifyContent: "flex-start",
  },
  taskDashboardCnt: {
    padding: "13px 0 0 0",
  },
  taskDashboardHeader: {
    // margin: "0 0 15px 0",
    padding: "0 70px 0 31px",
    flexWrap: "nowrap",
  },
  boardGridViewCnt: {
    padding: "0 20px 0 31px",
  },
  toggleBtnGroup: {
    display: "flex",
    flexWrap: "nowrap",
    borderRadius: 4,
    background: theme.palette.common.white,
    // border: `1px solid ${theme.palette.border.lightBorder}`,
    boxShadow: "none",
    "& $toggleButtonSelected": {
      color: theme.palette.text.secondary,
      backgroundColor: "white",
      "&:after": {
        // background: theme.palette.common.white,
      },
      "&:hover": {
        // background: theme.palette.common.white,
      },
    },
  },
  groupedHorizontal:{
    borderLeft: `1px solid ${theme.palette.border.lightBorder} !important`
  }, 
  toggleButton: {
    height: "auto",
    padding: "4px 20px 5px",
    fontSize: "12px !important",
    fontWeight: theme.palette.fontWeightMedium,
    textTransform: "capitalize",
    "&:hover": {
      background: theme.palette.common.white,
    },
    "&[value = 'boards']": {
      //  borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
      borderRight: `1px solid ${theme.palette.border.lightBorder}`,
    },
  },
  toggleButtonSelected: {},
  boardsCnt: {
    // marginTop: 10,
    padding: "0 51px 10px 51px",
    overflowY: "auto",
  },
  boardsDashboardStyles: {},
  boardListItemInnerFirst: {
    padding: "20px 20px 13px 20px",
    background: theme.palette.background.grayLighter,
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    borderRadius: 4,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    border: `2px ${theme.palette.border.grayLighter}`,
    position: "relative",
    cursor: "pointer",
    minHeight: 140,
    "&:hover": {
      boxShadow: "0px 2px 6px 0px rgba(0,0,0,0.1)",
      background: theme.palette.common.white,
      border: `1px solid ${theme.palette.border.lightBorder}`,
    },
  },
  boardIcon: {
    fontSize: "24px !important",
    position: "absolute",
    right: 13,
    opacity: 0,
    bottom: 10,
  },
  boardListItemInner: {
    padding: "20px 20px 13px 20px",
    background: theme.palette.common.white,
    borderRadius: 4,
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    border: `1px solid ${theme.palette.border.grayLighter}`,
    position: "relative",
    cursor: "pointer",
    "&:hover": {
      boxShadow: "0px 2px 6px 0px rgba(0,0,0,0.1)",
      border: `1px solid ${theme.palette.border.lightBorder}`,
    },
  },
  boardItemHeading: {
    marginTop: 12,
    height: 36,
    textTransform: "capitalize",
    fontSize: "13px !important",
    textAlign: "center",
    fontWeight: 700,
    fontFamily: theme.typography.fontFamilyLato,
  },
  addBtn: {
    marginTop: 20,
  },
  projectNameCnt: {
    display: "flex",
  },
  boardProjectHeading: {
    textTransform: "unset",
    fontSize: "16px !important",
    color: "white",
    fontWeight: 500,
    fontFamily: theme.typography.fontFamilyLato,
    lineHeight: "1.2",
    marginBottom: 8,
  },
  boardProjectItemInner: {
    padding: "12px",
    background: theme.palette.common.white,
    borderRadius: 4,
    display: "flex",
    flexDirection: "column",
    border: `1px solid ${theme.palette.border.grayLighter}`,
    position: "relative",
    cursor: "pointer",
    minHeight: "140px",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover !important",
    width: "100%",
    // minWidth: 276,
    "&:hover": {
      boxShadow: "0px 2px 6px 0px rgba(0,0,0,0.1)",
      border: `1px solid ${theme.palette.border.lightBorder}`,
      // background: "linear-gradient(rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)) !important",
    },
    "&:hover $boardIcon": {
      opacity: 1,
    },
    "&:hover $overlay": {
      opacity: 0.1,
      color: "#fff",
    },
  },
  overlay: {
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    height: "100%",
    width: "100%",
    opacity: 0,
    transition: ".5s ease",
    backgroundColor: "#000",
  },
  boardProjectDescription: {
    fontSize: "13px !important",
    color: "white",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
    lineHeight: "1.2",
  },
  listItem: {
    padding: "7px 14px",
    fontSize: "13px !important",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
    "&:hover": {
      backgroundColor: "transparent !important",
    },
  },
  deleteTxt: {
    color: theme.palette.text.danger,
  },
  listItemColor: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    padding: "0px 6px 0px 14px",
    fontSize: "13px !important",
    fontWeight: 600,
    fontFamily: theme.typography.fontFamilyLato,
    "&:hover": {
      backgroundColor: "transparent !important",
    },
  },
  label: {
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
    color: "#646464",
  },
  colorPicker: {
    right: 195,
    top: 34,
    position: "fixed",
  },
  importTxt: {
    fontSize: "13px !important",
    fontWeight: 600,
    fontFamily: theme.typography.fontFamilyLato,
  },
  menuHeadingItem: {
    padding: "0 14px !important",
    border: "none !important",
    "&:hover": {
      backgroundColor: "transparent",
      cursor: "unset",
    },
  },
  menuHeadingListItemText: {
    fontSize: "12px !important",
    marginTop: 5,
    marginBottom: 2,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: theme.typography.fontWeightRegular,
    textTransform: "capitalize",
    color: theme.palette.secondary.light,
  },
  editIcon: {
    fontSize: "11px !important",
    marginTop: -4,
    marginLeft: 5,
  },
  editIconCnt: {
    display: "none",
  },
  plainMenuItemIcon: {
    fontSize: "24px !important",
    color: "white",
    // marginBottom: 3
  },
  tooltip: {
    fontSize: "12px !important",
    backgroundColor: theme.palette.common.black,
    "& .chat-tooltip-body": {
      padding: "5px 15px 5px 5px",
      "& ul,li": {
        margin: 0,
        padding: 0,
        listStyleType: "none",
        color: theme.palette.common.white,
      },
    },
  },
  actionIcon: {
    fontSize: "16px !important",
  },
  dropdownLabel: {
    fontSize: "13px !important",
    color: theme.palette.text.light,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
  },
  dropdownValue: {
    fontSize: "18px !important",
    color: "#333333",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 900,
    marginLeft: 5,
  },
  templatePreviewTitle: {
    fontSize: "18px !important",
    color: "#7E7E7E",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 900,
    marginLeft: 5,
  },
  ddIcon: {
    marginBottom: -7,
    color: "#7E7E7E",
    marginLeft: 0,
  },
  ddIconTransparent: {
    marginBottom: -7,
    color: "black",
    marginLeft: 0,
  },
  formControl: {
    width: "100%",
    marginRight: "10px",
    marginLeft: "10px",
  },
  quickFilterIconSize:{
    fontSize: "1.5rem !important"
  },
  qckFilterIconSvg: {
    color: theme.palette.secondary.medDark,
  },

  qckFilterLblSelected: {
    paddingRight: 4,
  },

  qckFilterLbl: {
    paddingRight: 4,
    fontSize: "12px !important",
    color: "#333",
  },
  checkname: {
    paddingLeft: 0,
  },
  statusMenuItemCnt: {
    padding: "0px 0px 0px 16px",
    marginBottom: 2,
  },
  selectedValue: {
    //Adds left border to selected list item
    "&:before": {
      content: "''",
      position: "absolute",
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      width: 5,
      background: theme.palette.secondary.main,
      borderTopRightRadius: 4,
      borderBottomRightRadius: 4,
    },
  },
  statusMenuItemSelected: {
    background: `${theme.palette.common.white} !important`,
    "&:hover": {
      background: `${theme.palette.background.items}`,
    },
    "&:focus": {
      background: `${theme.palette.background.items} !important`,
    },
  },
  plainItemText: {
    fontSize: "12px !important",
    color: theme.palette.text.secondary,
    fontWeight: theme.typography.fontWeightLight,
  },
  statusIcon: {
    fontSize: "11px !important",
  },
  statusItemText: {
    padding: "0 16px",
    fontSize: "12px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightRegular,
  },
  statusItemTextSelected: {
    color: "#0090ff !important",
  },
  highlightItem: {
    // background: theme.palette.background.items,
    padding: 10,
    fontSize: "12px !important",
    color: theme.palette.text.secondary,
    fontWeight: theme.typography.fontWeightRegular,
    borderTop: `1px solid ${theme.palette.border.lightBorder}`,
    "&:hover": {
      background: `${theme.palette.background.items} !important`,
    },
    "&:focus": {
      background: `${theme.palette.background.items} !important`,
    },
  },
  selectHighlightItemIcon: {
    marginRight: 10,
  },

  "@media (max-width: 1024px)": {
    qckFilterLblSelected: {
      display: "none",
    },
    qckFilterLbl: {
      display: "none",
      paddingRight: 0,
    },
    checkname: {
      paddingLeft: 0,
    },
  },
  templateBtnTitle: {
    fontSize: "16px !important",
    fontWeight: 500,
    fontFamily: theme.typography.fontFamilyLato,
    color: "#202020",
  },
  searchInput: {
    width: 280,
    "& $input": {
      paddingLeft: "35px !important",
    },
  },
  searchIcon: {
    marginTop: "5px",
    marginRight: "-30px",
    color: "#969696",
    zIndex: 1000,
  },
  searchInputCnt: {
    display: "flex",
    flexDirection: "row",
  },
  sortBtnsCnt: {
    display: "flex",
    marginRight: 8,
  },
  closeIcon: {
    fontSize: "12px !important",
  },
  asdecIcon: {
    marginLeft: 5,
  },
  sortIconSvg: {
    color: theme.palette.secondary.medDark,
  },
  sortIconSelected: {
    color: theme.palette.text.azure,
  },
  sortLbl: {
    paddingRight: 4,
  },
  sortLblSelected: {
    paddingRight: 0,
  },
  checkname: {
    paddingLeft: 4,
  },
  "@media (max-width: 1024px)": {
    sortLbl: {
      display: "none",
    },
    sortLblSelected: {
      display: "none",
    },
    checkname: {
      paddingLeft: 0,
    },
  },
  boardOptTitle: {
    marginLeft: 2,
    color: "#292929",
    fontSize: "16px !important",
    fontWeight: 600,
    fontFamily: theme.typography.fontFamilyLato,
  },
  unplannedMain: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    height: "calc(100vh - 130px)",
    overflowY: "auto",
    padding: "15px 0 23px 0",
    background: `url(${bgPattern})`,
  },
  unplannedCnt: {
    width: 550,
  },
  dropdownBtn: {
    padding: 7,
    borderRadius: 4,
    backgroundColor: "#f3f3f380 !important",
    "&:hover": {
      backgroundColor: "#f3f3f380 !important",
    },
  },
  dropdownBtnTransparent: {
    padding: 7,
    borderRadius: 4,
    "&:hover": {
      backgroundColor: "#f1f1f1 !important",
    },
  },
});

export default boardsStyle;
