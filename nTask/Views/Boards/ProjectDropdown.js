import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles/";
import SearchDropdown from "../../components/Menu/SearchMenu";
import Button from "@material-ui/core/Button";
import listStyles from "./styles";
import combineStyles from "../../utils/mergeStyles";
import CustomButton from "../../components/Buttons/CustomButton";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import { injectIntl } from "react-intl";
import clsx from "clsx";

class ChangeProjectDropDown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      selectedProject: "Select Project",
      ddData: [],
      selectedOption: "",
    };
  }

  componentDidMount() {
    const { projectsState, data } = this.props;
    let selectedProject = projectsState.data.length
      ? projectsState.data.find(x => x.projectId == data.projectId) || ""
      : "";
    this.setState({
      ddData: this.filterProjects(projectsState, data),
      selectedOption: data.boardName,
    });
  }

  filterProjects = (allProjects, obj) => {
    let project = allProjects.data.filter(p => p.projectId !== obj.projectId);
    project = project.length
      ? project.map(x => {
          return { id: x.projectId, value: { name: x.projectName } };
        })
      : [];
    return project;
  };

  handleClose = event => {
    this.setState({ open: false });
  };
  handleClick = (event, placement) => {
    const { currentTarget } = event;
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  };
  handleSelect = (event, value, id, avatar, email) => {
    this.props.handleSelectProject(event, { projectId: id });
    this.setState({
      selectedOption: value,
      open: false,
      ddData: this.filterProjects(this.props.projectsState, { projectId: id }),
    });
  };
  render() {
    const { classes, theme, iconVisible, label, data, handleClickBackBtn, intl } = this.props;
    const { open, placement, ddData, selectedOption } = this.state;

    return (
      <Fragment>
        {data.isTemplate ? (
          <>
            <span className={classes.templatePreviewTitle}>Template Preview:</span>
            <span className={classes.dropdownValue}>{data.boardName}</span>
          </>
        ) : (
          <>
            {" "}
            <CustomButton
              buttonRef={node => {
                this.anchorEl = node;
              }}
              onClick={event => {
                event.stopPropagation();
                this.handleClick(event, "bottom-start");
              }}
              btnType="plainbackground"
              variant="text"
              className={clsx({
                [classes.dropdownBtn]: data.background?.backGroundImage,
                [classes.dropdownBtnTransparent]: !data.background?.backGroundImage,
              })}
              disabled={false}>
              <span className={classes.dropdownLabel}>{label}</span>
              <span className={classes.dropdownValue}>
                {selectedOption}
                {iconVisible ? (
                  <ArrowDropDownIcon
                    className={clsx({
                      [classes.ddIcon]: !data.background?.backGroundImage,
                      [classes.ddIconTransparent]: data.background?.backGroundImage,
                    })}
                  />
                ) : (
                  ""
                )}
              </span>
            </CustomButton>
          </>
        )}

        <SearchDropdown
          placement={placement}
          open={open}
          title={intl.formatMessage({ id: "board.all-boards.label", defaultMessage: "All Boards" })}
          closeAction={this.handleClose}
          selectAction={this.handleSelect}
          searchQuery={["id", "value.name"]}
          data={ddData}
          isProjectSelect={true}
          anchorRef={this.anchorEl}
          searchInputPlaceholder={intl.formatMessage({
            id: "board.search.searchbyboard",
            defaultMessage: "Search boards by title",
          })}
          backOption={true}
          backOptionTxt={intl.formatMessage({
            id: "board.back-to-board.label",
            defaultMessage: "Back to Board View",
          })}
          handleClickBackBtn={handleClickBackBtn}
        />
      </Fragment>
    );
  }
}

ChangeProjectDropDown.defaultProps = {
  iconVisible: true,
  label: "",
  data: {},
  handleClickBackBtn: () => {},
  handleSelectProject: () => {},
};

const mapStateToProps = state => {
  return {
    projectsState: state.projects,
  };
};

export default compose(
  withRouter,
  injectIntl,
  withStyles(listStyles, { withTheme: true }),
  connect(mapStateToProps, {})
)(ChangeProjectDropDown);
