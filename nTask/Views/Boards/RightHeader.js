import React, { useState, useEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./styles";
import CustomIconButton from "../../components/Buttons/IconButton";
import { ActivityIcon } from "../../components/Icons/ActivityIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import QuickFilter from "./QuickFilter";
import Tooltip from "@material-ui/core/Tooltip";
import DefaultSwitch from "../../components/Form/Switch";
import CustomButton from "../../components/Buttons/CustomButton";
import ImportExportDD from "../../components/Dropdown/ImportExportDD";
import { withSnackbar } from "notistack";
import { FetchWorkspaceInfo } from "../../redux/actions/workspace";
import { GetKanbanListingByProjectId } from "../../redux/actions/boards";
import { FormattedMessage } from "react-intl";
import Settings from "./Settings/Settings.view.js";
import ConstantsPlan from "../../components/constants/planConstant";

type RightHeaderProps = {
  classes: Object,
  theme: Object,
  handleClickActivityLog: Function,
  handleCheckSubtaskFilter: Function,
  updateSubtaskFilter: Function,
  handleClickEndPreview: Function,
  handleUseTemplate: Function,
  data: Object,
  boardPer: Object,
  enqueueSnackbar: Function,
  callBack: Function,
};

const btn = {
  padding: "10px 17px",
  height: 36,
};

function RightHeader(props: RightHeaderProps) {
  const {
    classes,
    theme,
    handleClickActivityLog,
    handleSelectQuickFilter,
    handleCheckSubtaskFilter,
    updateSubtaskFilter,
    data,
    handleClickEndPreview,
    handleUseTemplate,
    callBack,
    boardPer,
    companyInfo,
    subscriptionDetails = {},
  } = props;
  const { boardSetting } = data.userPermission?.permission?.board?.boardDetail;
  // let tasks = [];
  // data.kanbanboard.map(item => {
  //   tasks.push(...item.tasksList);
  // });
  // if (data.unsortedTask) tasks.push(...data.unsortedTask.tasksList);
  // const tasksList = tasks;
  const [subTaskCheck, setSubTaskCheck] = useState(data.subtaskFilter);
  const handleChangeSubtaskFilter = value => {
    setSubTaskCheck(value);
    handleCheckSubtaskFilter("Subtask", value ? "Add" : "Remove");
    updateSubtaskFilter(
      value,
      succ => { },
      fail => { }
    );
  };
  const handleExportType = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  const handleRefresh = () => {
    props.FetchWorkspaceInfo("", () => { });
    // props.GetKanbanListingByProjectId(
    //   data.projectId,
    //   (succ) => {
    //     // this.createActivityLog();
    //   },
    //   (failure) => {}
    // );
  };

  useEffect(() => {
    setSubTaskCheck(data.subtaskFilter);
  }, [data.subtaskFilter]);
  const taskAccess = companyInfo?.task?.isHidden ? false : true;
  return (
    <>
      {data.isTemplate ? (
        <>
          <CustomIconButton
            onClick={e => {
              handleClickEndPreview(e);
            }}
            btnType="filledWhite"
            style={btn}>
            <span className={classes.templateBtnTitle}>
              <FormattedMessage
                id="board.template.end-preview.label"
                defaultMessage="End Preview"
              />
            </span>
          </CustomIconButton>
          <CustomButton
            btnType="success"
            variant="contained"
            style={{ height: 36, marginLeft: 10 }}
            onClick={event => {
              handleUseTemplate({ isTemplate: true, template: data });
            }}
            disabled={!boardPer.creatBoard.cando}>
            <FormattedMessage
              id="board.template.use-this-template.label"
              defaultMessage="Use this Template"
            />
          </CustomButton>
        </>
      ) : (
        <>
          {taskAccess &&
            <CustomIconButton
              classes={{ root: classes.rootCustomButton }}
              btnType="filledWhite"
              style={{
                fontSize: "12px",
                color: !data.background?.backGroundImage ? "#333333" : "black",
                fontWeight: 500,
                backgroundColor: !data.background?.backGroundImage ? "#fff" : "#f3f3f380",
                height: 32,
              }}
              disabled={false}>
              <span style={{ marginRight: 6 }}>
                <FormattedMessage id="board.board-detail.subtask.label" defaultMessage="Subtasks" />
              </span>
              <div>
                <DefaultSwitch
                  checked={subTaskCheck}
                  onChange={event => {
                    handleChangeSubtaskFilter(!subTaskCheck, event);
                  }}
                  // value={true}
                  size="small"
                />
              </div>
            </CustomIconButton>}
          <QuickFilter handleSelectQuickFilter={handleSelectQuickFilter} data={data} />
          <CustomIconButton
            onClick={handleClickActivityLog}
            btnType="filledWhite"
            style={{
              padding: "5px 7px",
              height: 32,
              backgroundColor: !data.background?.backGroundImage ? "#fff" : "#f3f3f380",
              color: !data.background?.backGroundImage ? "#333333" : "black",
            }}
            disabled={false}>
            <Tooltip
              classes={{
                tooltip: classes.tooltip,
              }}
              title={<FormattedMessage id="activity-log.label" defaultMessage="Activity Log" />}
              placement="bottom">
              <SvgIcon viewBox="0 0 512 512" classes={{ root: classes.plainMenuItemIcon }}>
                <path
                  d={ActivityIcon}
                  fill={!data.background?.backGroundImage ? theme.palette.secondary.light : "black"}
                />
              </SvgIcon>
            </Tooltip>
          </CustomIconButton>
          {data.userPermission.permission.board.boardDetail.importExport.cando && (
            <ImportExportDD
              handleExportType={handleExportType}
              // filterList={quickFilters}
              // multipleFilters={multipleFilters}
              handleRefresh={handleRefresh}
              id="boardImportExport"
              ImportExportType={"board"}
              data={data.tasksList}
              // data={tasksList}
              projectId={data.projectId}
              callBack={callBack}
              btnProps={{
                style: {
                  padding: "5px 7px",
                  height: 32,
                  backgroundColor: !data.background?.backGroundImage ? "#fff" : "#f3f3f380",
                  color: !data.background?.backGroundImage ? "#7e7e7e" : "black",
                },
              }}
              labelProps={{
                style: {
                  color: !data.background?.backGroundImage ? "#7e7e7e" : "black",
                },
              }}
            />
          )}
          {(ConstantsPlan.isBusinessPlan(subscriptionDetails.paymentPlanTitle) ||
            ConstantsPlan.isBusinessTrial(subscriptionDetails.paymentPlanTitle)) &&
            boardSetting.cando ? (
            <Settings boardData={data} />
          ) : null}
        </>
      )}
    </>
  );
}

RightHeader.defaultProps = {
  classes: {},
  theme: {},
  data: {},
  boardPer: {},
  handleClickActivityLog: () => { },
  handleSelectQuickFilter: () => { },
  handleCheckSubtaskFilter: () => { },
  updateSubtaskFilter: () => { },
  handleClickEndPreview: () => { },
  handleUseTemplate: () => { },
  enqueueSnackbar: () => { },
  callBack: () => { },
};
const mapStateToProps = state => {
  return {
    boardPer: state.workspacePermissions.data.board,
    subscriptionDetails: state.profile.data.teams.find(
      item => item.companyId == state.profile.data.activeTeam
    )?.subscriptionDetails,
    companyInfo: state.whiteLabelInfo.data,
  };
};

export default compose(
  withSnackbar,
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, { FetchWorkspaceInfo, GetKanbanListingByProjectId })
)(RightHeader);
