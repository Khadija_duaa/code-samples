import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withSnackbar } from "notistack";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import boardsStyle from "./styles";
import TaskDetails from "../Task/TaskDetails/TaskDetails";
import isEmpty from "lodash/isEmpty";

import Footer from "../../components/Footer/Footer";
import BoardsKanban from "../Kanban";
import { taskDetailDialogState } from "../../redux/actions/allDialogs";

import {
  UpdateKanban,
  UpdateKanbanTask,
  UpdateTaskDragDrop,
  UpdateKanbanTitleColor,
  updateColumSettings,
  updateStatusRule,
  getActivityLog,
  clearKanbanStoreData,
  UpdateSubtaskFilterStatus,
  SaveFeatureImage,
  SaveLibraryImage,
  CreateNewCustomStatus,
  DeleteStatus,
  DuplicateStatus,
  SaveTask,
  updateTaskKanbanStore,
  AddUnsortedTask,
  UpdateKanbanBoard,
  CopyTask,
  updateTaskPositionsInStore,
} from "../../redux/actions/boards";

import { UpdateTask, DeleteTask, ArchiveTask, updateTaskData } from "../../redux/actions/tasks";
import cloneDeep from "lodash/cloneDeep";
import RightHeader from "./RightHeader";
import ChangeBoardDropDown from "./ProjectDropdown";
import { injectIntl } from "react-intl";
import { clearAllAppliedFilters } from "../../redux/actions/appliedFilters";
import { statusData } from "../../helper/projectDropdownData";

class Boards extends Component {
  constructor(props) {
    super(props);
    this.state = {
      taskDetails: false,
      currentTask: {},
      activityLogDrawer: false,
      quickFilters: ["Show All"],
      selectedTaskColumn: {},
      pointerEvents: false,
    };
  }

  componentDidMount() {
    const { data } = this.props;
    if (data && data.subtaskFilter)
      this.setState({
        quickFilters: [...this.state.quickFilters, "Subtask"],
      });
  }

  componentWillUnmount() {
    this.props.clearAllAppliedFilters(null);
  }

  componentDidUpdate(prevProps, prevState) {
    const { allTasks, allProjects, data } = this.props;
    const { currentTask } = this.state;
    const doesProjectExist = allProjects.some(p => p.projectId == data.projectId)
    if (!doesProjectExist && data && data.projectId) {
      this.props.clearKanbanStoreData();
      this.props.openBoardView();
    }
    if (JSON.stringify(allTasks) !== JSON.stringify(prevProps.allTasks)) {
      if (!isEmpty(currentTask)) {
        let task = allTasks.find(t => t.taskId == currentTask.taskId) || false;
        if (task) {
          if (JSON.stringify(task) !== JSON.stringify(currentTask)) {
            this.setState({ currentTask: task });
          }
        }
      }
    }
  }

  saveKanbanListOrder = kanbanList => {
    /** function updates the position key in each kanban list object for saving the APi */
    const { UpdateKanban, data } = this.props;
    let object = cloneDeep(data);
    object.statusList = kanbanList;
    let statusIds = [];
    statusIds = kanbanList.map(el => el.statusId);
    let obj = {
      projectId: data.projectId,
      statusIds,
    };
    UpdateKanban(
      obj,
      succ => { },
      fail => { },
      object
    );
  };

  handleClickListItem = (item, column) => {
    /** function calls when user clicks on any task to open task details  */

    // this.setState({ taskDetails: true, currentTask: item });
    this.handleClickActivityLogItem(item, column);
  };
  handleClickActivityLogItem = (item, column = {}) => {
    /** function calls when user clicks on any task's activity to open task details  */
    console.log(item, "  here");
    if (!item.isTemplate) {
      /** if click task is not a dummy task reference to template view */
      let task = this.props.allTasks.find(t => t.taskId == item.taskId) || null;
      if (task) {
        task = this.getTaskPermission(task);
        this.setState({
          currentTask: task,
          taskDetails: true,
          selectedTaskColumn: column,
        }, () => {
          this.setState({
            currentTask: {},
            taskDetails: false,
          })
        });
      }
    }
  };
  getTaskPermission = task => {
    /** pushing the task permission object in the task , if task has linked with any project i-e projectId !== null then find the project and fetch the task permission in project object */
    const { projects, taskPer, workspaceStatus } = this.props;
    if (task.projectId) {
      let attachedProject = projects.find(p => p.projectId == task.projectId);
      if (attachedProject) {
        task.taskPermission = attachedProject.projectPermission.permission.task;
        // return task;
      } else {
        task.taskPermission = taskPer;
        // return task;
      }
      if (attachedProject && attachedProject.projectTemplate) {
        task.taskStatus = attachedProject.projectTemplate;
      } else {
        task.taskStatus = workspaceStatus;
      }
      return task;
    } else {
      task.taskPermission = taskPer;
      task.taskStatus = workspaceStatus;
      return task;
    }
  };
  closeTaskDetailsPopUp = () => {
    /** function for clng the task details popup */
    // this.onColumColorChange(this.state.selectedTaskColumn.colorCode, this.state.selectedTaskColumn);
    this.props.handleSelectProject(null, this.props.data);
    this.setState({ taskDetails: false, currentTask: {} });
  };
  onAddNewTask = (column, title, callback = () => { }) => {
    //Object made to be posted to the backend understandable form
    let obj = {
      taskTitle: title,
      projectId: this.props.data.projectId,
      status: column.statusId,
    };
    //Action Called to save task object to backend
    this.props.SaveTask(
      obj,
      success => {
        //Success Function
        // if (success.task) this.updateKanbanTask(success.task, column);
        callback();
      },
      failure => {
        //Failure Function
      },
      null
    );
  };

  updateKanbanTask = (task, column) => {
    let obj = {
      /** adding new created task in the desired kanban colum */
      id: column.id,
      listItems: [task.taskId, ...column.listItems],
    };
    this.props.UpdateKanbanTask(
      /** APi call for adding task in the desired column */
      obj,
      succ => { },
      fail => { }
    );
  };
  onColumColorChange = (data, obj, success = () => { }) => {
    // let column = cloneDeep(obj);
    // column.colorCode = color;
    this.props.UpdateKanbanBoard(
      data,
      succ => {
        success();
      },
      fail => { }
    );
  };
  onColumTitleChange = (title, obj, callBack) => {
    let column = cloneDeep(obj);
    column.listName = title;

    this.props.UpdateKanbanTitleColor(
      column,
      succ => {
        callBack();
      },
      fail => { }
    );
  };
  UpdateKanbanColumnSetting = (obj, callBack) => {
    this.props.updateColumSettings(
      obj,
      succ => {
        callBack();
      },
      fail => { }
    );
  };
  UpdateKanbanStatusRule = (obj, callBack) => {
    this.props.updateStatusRule(
      obj,
      succ => {
        callBack();
      },
      fail => { }
    );
  };
  handleClickActivityLog = () => {
    this.setState(
      {
        activityLogDrawer: true,
      },
      () => {
        this.props.getActivityLog(
          this.props.data.projectId,
          succ => {
            //  this.props.createActivityLog(succ.data.activitylog, () => {});
          },
          fail => { }
        );
      }
    );
  };
  handleCloseActivityLog = () => {
    this.setState({
      activityLogDrawer: false,
    });
  };

  taskDragDrop = (id, column, itemOrder = null) => {
    // let obj = {
    //   /** getting the updated tasklist aftyer droping the task from one colum to another */
    //   id: column.id,
    //   listItems: column.tasksList.map(c => {
    //     return c.taskId;
    //   }),
    // };
    let task = this.props.allTasks.find(ele => ele.taskId === id) || null;
    task.status = column.statusId;
    task.statusColorCode = column.statusColor;
    task.statusName = column.statusTitle;
    task.projectId = this.props.data.projectId;
    if (task) {
      this.props.UpdateTaskDragDrop(
        /** APi call for saving column */
        { objTask: task, itemOrder },
        succ => { },
        fail => { }
      );
    }
  };
  updateTaskInKanbanStore = task => {
    this.props.updateTaskKanbanStore(task);
  };
  addUnsortedTask = (task, column) => {
    this.props.AddUnsortedTask(task);
    this.taskDragDrop(task.taskId, column, {
      items: this.returnTaskPosition(column),
      projectId: this.props.data.projectId,
    });
  };

  returnTaskPosition = column => {
    let updatedColum = cloneDeep(column);
    let items = [];
    updatedColum.tasksList.map((c, index) => {
      items.push({
        itemId: c.taskId,
        position: index,
      });
    });
    return items;
  };

  handleClickBackBtn = () => {
    this.props.clearKanbanStoreData();
    this.props.openBoardView();
  };
  handleSelectQuickFilter = item => {
    this.setState({
      quickFilters: item,
    });
  };
  handleCheckSubtaskFilter = (value, check) => {
    if (check == "Add") {
      this.setState({
        quickFilters: [...this.state.quickFilters, value],
      });
    } else if (check == "Remove") {
      this.setState({
        quickFilters: this.state.quickFilters.filter(q => q !== "Subtask"),
      });
    }
  };
  UpdateSubtaskFilter = value => {
    const { data, UpdateSubtaskFilterStatus } = this.props;
    let obj = {
      projectId: data.projectId,
      subtaskFilter: value,
    };
    UpdateSubtaskFilterStatus(
      obj,
      succ => { },
      fail => { }
    );
  };
  onUpdateTaskColor = (color, item) => {
    let updatedTask = { ...item };
    updatedTask.colorCode = color;
    this.props.updateTaskKanbanStore(updatedTask);
    this.updateGlobalTask(color, item.taskId);
  };

  updateGlobalTask = (color, id) => {
    const { allTasks, updateTaskData } = this.props;
    let updatedTask = allTasks.find(t => t.taskId == id) || {}; /** updating color in store also */
    const obj = { colorCode: color };
    updateTaskData({ task: updatedTask, obj });
  };
  deleteTask = (deletedTask, callback) => {
    this.props.DeleteTask(
      deletedTask,
      succ => { },
      fail => {
        if (fail.data) {
          const errMessage = fail.data.message || 'Oops! Server throws error!'
          this.showSnackBar(errMessage, "error");
        }
      }
    );
  };
  archiveTask = (archivedTask, callback) => {
    this.props.ArchiveTask(
      archivedTask.taskId,
      succ => { },
      fail => { }
    );
  };
  getMaxTaskLimit = el => {
    let arr = el.statusRulesList.filter(
      item => item.dynamicStatusRule && item.dynamicStatusRule.keyName == "MAXTASKLIMIT_RULE"
    );
    if (arr.length > 0 && arr[0].dynamicStatusRule) {
      return { maxTaskEnable: true, maxTaskLimit: parseInt(arr[0].dynamicStatusRule.keyValue) };
    }
    return { maxTaskEnable: false, maxTaskLimit: 0 };
  };
  copyTask = (copiedTask, column, success = () => { }) => {
    // let isAllowed = true;
    // const maxTaskObj = this.getMaxTaskLimit(column);
    // if (
    //   !column.isDefault &&
    //   !column.isDoneState &&
    //   maxTaskObj.maxTaskLimit > 0 &&
    //   (maxTaskObj.maxTaskLimit == column.tasksList.length ||
    //     column.tasksList.length > maxTaskObj.maxTaskLimit)
    // ) {
    //   this.showSnackBar(
    //     "You cannot move this task because destination column has been approached to its limit or you don't have a permission to moving task from one list to another!",
    //     "error"
    //   );
    // } else {
    this.props.CopyTask(
      copiedTask,
      succ => {
        success()
        //Success Function
        // if (succ.data) this.updateKanbanTask(succ.data, column);
      },
      fail => { }
    );
    // }
  };

  saveTaskFeatureImage = (data, task, column, callBack) => {
    this.props.SaveFeatureImage(
      data,
      task,
      column,
      succ => {
        callBack();
      },
      fail => { }
    );
  };

  saveTaskLibraryImage = (data, task, column, callBack) => {
    this.props.SaveLibraryImage(
      data,
      task,
      column,
      (succ, Task) => {
        callBack();
      },
      fail => { }
    );
  };

  addNewColumn = obj => {
    this.props.CreateNewCustomStatus(
      obj,
      succ => { },
      fail => { }
    );
  };
  onDeleteStatus = (obj, success = () => { }) => {
    this.props.DeleteStatus(
      obj,
      succ => {
        this.showSnackBar("Status Deleted Successfully.", "success");
        success();
      },
      fail => {
        this.showSnackBar("Oops! Something went wrong.", "error");
      }
    );
  };
  onDuplicateStatus = (obj, success = () => { }) => {
    this.props.DuplicateStatus(
      obj,
      succ => {
        this.showSnackBar("Status Copied Successfully.", "success");
        success();
      },
      fail => { }
    );
  };
  callBackAfterImport = dataProject => {
    this.props.handleSelectProject(null, dataProject);
  };
  UpdateKanbanBoard = (board, success = () => { }, failure = () => { }) => {
    this.setState({ pointerEvents: true }, () => {
      this.props.UpdateKanbanBoard(
        board,
        succ => {
          this.setState({ pointerEvents: false });
          success();
        },
        fail => {
          this.setState({ pointerEvents: false });
          failure();
        }
      );
    });
  };

  showSnackBar = (snackBarMessage = "", type = null, options) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
        ...options
      }
    );
  };
  render() {
    const {
      classes,
      theme,
      quote,
      data,
      activityLogs,
      allTasks,
      allProjects,
      handleSelectProject,
      loggedInUser,
      UpdateTask,
      updateSubtaskFilter,
      handleClickEndPreview,
      handleUseTemplate,
      forTemplatesSetColumnsInParent,
      intl,
      allMembers,
      unsortedTask,
      handleClearActivityLog,
      updateTaskPositionsInStore,
      companyInfo,
      updateTaskData
    } = this.props;

    const { currentTask, taskDetails, activityLogDrawer, quickFilters, pointerEvents } = this.state;
    const boardProject = allProjects.find(x => x.projectId == data.projectId);
    const projectStatusColor = boardProject && statusData(theme, classes).find(s => boardProject.status == s.value).color;
    const isProjectStatusColor = data.background?.useProjectStatusColor
    const taskAccess = companyInfo?.task?.isHidden ? false : true;

    return (
      <>
        <div
          className={classes.root}
          style={{
            pointerEvents: pointerEvents ? "none" : "all",
            backgroundImage: data.background?.backGroundImage ? `url('${data.background.backGroundImage}')` : null,
            backgroundColor: isProjectStatusColor ? projectStatusColor : data.background?.backGroundColor,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat'
          }}>
          <main className={classes.content}>
            <div className={classes.drawerHeader}>
              <Grid container classes={{ container: classes.taskDashboardCnt }}>
                <Grid
                  container
                  direction="row"
                  justify="space-between"
                  alignItems="center"
                  classes={{ container: classes.taskDashboardHeader }}>
                  <div className="flex_center_start_row">
                    <ChangeBoardDropDown
                      handleSelectProject={handleSelectProject}
                      data={data}
                      handleClickBackBtn={this.handleClickBackBtn}
                    />
                  </div>
                  <div className="flex_center_start_row">
                    <RightHeader
                      handleClickActivityLog={this.handleClickActivityLog}
                      handleSelectQuickFilter={this.handleSelectQuickFilter}
                      handleCheckSubtaskFilter={this.handleCheckSubtaskFilter}
                      updateSubtaskFilter={this.UpdateSubtaskFilter}
                      data={data}
                      handleClickEndPreview={handleClickEndPreview}
                      handleUseTemplate={handleUseTemplate}
                      callBack={() => {
                        this.callBackAfterImport(data);
                      }}
                    />
                  </div>
                </Grid>
                <BoardsKanban
                  dataList={data}
                  unsortedTask={unsortedTask}
                  quickFilters={quickFilters}
                  theme={theme}
                  classes={classes}
                  saveKanbanListOrder={this.saveKanbanListOrder}
                  activityLogDrawer={activityLogDrawer}
                  handleCloseActivityLog={this.handleCloseActivityLog}
                  activityLogs={activityLogs}
                  handleClearActivityLog={handleClearActivityLog}
                  allTasks={allTasks}
                  handleClickListItem={this.handleClickListItem}
                  onAddNewTask={this.onAddNewTask}
                  onColumColorChange={this.onColumColorChange}
                  onColumTitleChange={this.onColumTitleChange}
                  UpdateKanbanColumnSetting={this.UpdateKanbanColumnSetting}
                  UpdateKanbanStatusRule={this.UpdateKanbanStatusRule}
                  handleClickActivityLogItem={this.handleClickActivityLogItem}
                  taskDragDrop={this.taskDragDrop}
                  loggedInUser={loggedInUser}
                  UpdateTask={UpdateTask}
                  onUpdateTaskColor={this.onUpdateTaskColor}
                  deleteTask={this.deleteTask}
                  archiveTask={this.archiveTask}
                  copyTask={this.copyTask}
                  forTemplatesSetColumnsInParent={forTemplatesSetColumnsInParent}
                  saveTaskFeatureImage={this.saveTaskFeatureImage}
                  saveTaskLibraryImage={this.saveTaskLibraryImage}
                  addNewColumn={this.addNewColumn}
                  onDeleteStatus={this.onDeleteStatus}
                  onDuplicateStatus={this.onDuplicateStatus}
                  intl={intl}
                  allMembers={allMembers}
                  updateTaskInKanbanStore={this.updateTaskInKanbanStore}
                  addUnsortedTask={this.addUnsortedTask}
                  UpdateKanbanBoard={this.UpdateKanbanBoard}
                  showSnackBar={this.showSnackBar}
                  updateTaskPositionsInStore={updateTaskPositionsInStore}
                />
              </Grid>
            </div>
          </main>
          <Footer quote={quote} total={data.statusList.length} display={true} open={this.props.open} type="Status" />
          {taskDetails && !isEmpty(currentTask) && (
            // <TaskDetails
            //   closeTaskDetailsPopUp={this.closeTaskDetailsPopUp}
            //   currentTask={currentTask}
            // />
            this.props.taskDetailDialogState(null, {
              id: currentTask.taskId,
              afterCloseCallBack: () => {
                this.closeTaskDetailsPopUp()
              },
            })
          )}
        </div>
      </>
    );
  }
}
Boards.defaultProps = {
  // data: {},
  UpdateKanban: () => { },
  createActivityLog: () => { },
  UpdateTaskDragDrop: () => { },
  handleSelectProject: () => { },
  handleCheckSubtaskFilter: () => { },
  UpdateTask: () => { },
  updateSubtaskFilter: () => { },
  DeleteTask: () => { },
  ArchiveTask: () => { },
  CopyTask: () => { },
  handleClickEndPreview: () => { },
  handleUseTemplate: () => { },
  forTemplatesSetColumnsInParent: () => { },
  SaveFeatureImage: () => { },
  SaveLibraryImage: () => { },
  openBoardView: () => { },
  AddUnsortedTask: () => { },
  updateTaskPositionsInStore: () => { },
  activityLogs: [],
  allTasks: [],
  allProjects: [],
  quote: "",
  loggedInUser: {},
  allMembers: [],
  unsortedTask: [],
};

const mapStateToProps = (state, ownProps) => {
  return {
    allTasks: state.tasks.data,
    allProjects: state.projects.data,
    loggedInUser: state.profile.data.profile,
    taskPer: state.workspacePermissions.data.task,
    projects: state.projects.data || [],
    workspaceStatus: state.workspaceTemplates.data.defaultWSTemplate,
    allMembers: state.profile.data.member.allMembers,
    companyInfo: state.whiteLabelInfo.data,
  };
};

export default compose(
  withRouter,
  withSnackbar,
  injectIntl,
  withStyles(boardsStyle, {
    withTheme: true,
  }),
  connect(mapStateToProps, {
    UpdateKanban,
    SaveTask,
    UpdateKanbanTask,
    UpdateKanbanTitleColor,
    updateColumSettings,
    updateStatusRule,
    getActivityLog,
    UpdateTaskDragDrop,
    clearKanbanStoreData,
    UpdateTask,
    UpdateSubtaskFilterStatus,
    DeleteTask,
    ArchiveTask,
    CopyTask,
    SaveFeatureImage,
    SaveLibraryImage,
    CreateNewCustomStatus,
    DeleteStatus,
    DuplicateStatus,
    updateTaskKanbanStore,
    AddUnsortedTask,
    UpdateKanbanBoard,
    updateTaskPositionsInStore,
    clearAllAppliedFilters,
    taskDetailDialogState,
    updateTaskData
  })
)(Boards);
