//flow

import React, { PureComponent } from "react";
import { withSnackbar } from "notistack";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./styles";
import isEmpty from "lodash/isEmpty";
import { teamCanView } from "../../components/PlanPermission/PlanPermission";
import Board from "../../assets/images/unplanned/board.png";
import UnPlanned from "../billing/UnPlanned/UnPlanned";

import BoardGridView from "./boardGridView";
import KanbanView from "./Dashboard";
import {
  GetKanbanListingByProjectId,
  clearKanbanStoreData,
  // GetTemplates,
  GetKanbanTemplate,
  UseTheTemplate,
  clearActivityLog
} from "../../redux/actions/boards";
import cloneDeep from "lodash/cloneDeep";
import moment from "moment";
import DefaultDialog from "../../components/Dialog/Dialog";
import AddNewBoard from "../AddNewForms/AddNewBoard";
import { FormattedMessage, injectIntl } from "react-intl";

class Index extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      activityLogs: false,
      boardsTemplates: [],
      currentView: "boards",
      isUseTemplateDialog: false,
      kanbanData: [],
      unsortedData: [],
      template: {},
      updateBoard: false,
      isSavedAsTemplate: false,
      kanbanView: false,
    };
  }

  setView = nextView => {
    this.setState({
      currentView: nextView,
    });
  };

  onSelectGrid = (e, project) => {
    /** on select grid Api calls for that specific project and return the kanban boards and other details as well */
    const { GetKanbanListingByProjectId, projects } = this.props;
    e && e.stopPropagation();
    GetKanbanListingByProjectId(
      project.projectId,
      succ => {
        // this.createActivityLog();
        this.setState({
          kanbanView: true,
        });
      },
      failure => {},
    );
  };
  componentDidMount() {}
  componentDidUpdate(prevProps, prevState) {
    const {activitylog, kanban} = this.props.board.data;
    if(isEmpty(kanban)){
      this.setState({ kanbanView : false})
    }
    if(JSON.stringify(activitylog) !== JSON.stringify(prevProps.board.data.activitylog)){
      this.createActivityLog(activitylog, ()=>{});
    }
  }

  createActivityLog = (activitylog, callback) => {
    /** function that creates the activity logs */
    if (!isEmpty(activitylog)) {
      let activiLogs = [];
      let desiredLogs = [];
      let logs = cloneDeep(activitylog.results);
      logs.map(l => {
        /** concatinate the arrays of activites of different days */
        l.activities.map(a => {
          activiLogs.push(a);
        });
      });

      activiLogs = activiLogs.sort((a, b) => b.activity.updatedDate - a.activity.updatedDate)

      activiLogs.map(el => {
        desiredLogs.push({
          /** creating the desired array structure for activity logs */
          id: el.activity.id,
          userName: el.userName,
          action: el.activity.message,
          ticketId: el.activity.entityId,
          date: moment(el.activity.updatedDate).format("MMM Do"),
          time: moment(el.activity.updatedDate).format("LT"),
          taskName: el.activity.taskTitle,
          taskId: el.activity.taskId,
          userId: el.activity.createdBy,
        });
      });
      this.setState({ activityLogs: desiredLogs }, () => {
        callback();
      }); /** set in state */
    }
    else{
      this.setState({ activityLogs: false });
    }
  };
  handleTemplatePreview = (e, template) => {
    /** on select template Api calls for that specific template and return the kanban boards and other details as well */
    const { GetKanbanTemplate } = this.props;
    e.stopPropagation();
    GetKanbanTemplate(
      template,
      succ => {
        this.setState({
          kanbanView: true,
        });
      },
      failure => {}
    );
  };
  handleClickEndPreview = e => {
    e.stopPropagation();
    this.setState(
      {
        kanbanView: false,
      },
      () => {
        this.props.clearKanbanStoreData();
      }
    );
  };
  //isSavedAsTemplate when user open board modal from board actiona s saved as template
  handleUseTemplate = (template = {}, updateBoard = false, isSavedAsTemplate = false) => {
    this.setState({
      isUseTemplateDialog: true,
      template: template,
      updateBoard: updateBoard,
      isSavedAsTemplate: isSavedAsTemplate,
    });
  };

  handleDialogClose = () => {
    this.setState({
      isUseTemplateDialog: false,
    });
  };
  createdProject = (obj, callback) => {
    let apiObj = {
      projectId: obj.project.entity.projectId,
      keepCards: obj.keepCards,
      kanbanTemplates: this.state.kanbanData,
    };
    this.props.UseTheTemplate(
      apiObj,
      succ => {
        callback();
        this.props.clearKanbanStoreData();
      },
      fail => {}
    );
  };
  forTemplatesSetColumnsInParent = columns => {
    this.setState({
      kanbanData: columns,
    });
  };
  openBoardView=()=>{
    this.setState({ kanbanView : false})
  }
  handleClearActivityLog=()=>{
    this.setState({activityLogs: false}, ()=>{
      this.props.clearActivityLog();
    })
  }

  render() {
    const { theme, classes, board, quote, quickFilters, intl } = this.props;
    const {
      activityLogs,
      boardsTemplates,
      currentView,
      isUseTemplateDialog,
      template,
      updateBoard,
      isSavedAsTemplate,
      kanbanView
    } = this.state;
    const newBoardData = template;
    return (
      <>
        {/* {!teamCanView("boardAccess") ? (
          <>
            {" "}
            <div className={classes.unplannedMain}>
              <div className={classes.unplannedCnt}>
                <UnPlanned
                  feature="premium"
                  titleTxt={
                    <FormattedMessage
                      id="common.discovered-dialog.premium-title"
                      defaultMessage="Wow! You've discovered a premium feature!"
                    />
                  }
                  boldText={this.props.intl.formatMessage({
                    id: "board.label",
                    defaultMessage: "Boards",
                  })}
                  descriptionTxt={
                    <FormattedMessage
                      id="common.discovered-dialog.list.project.label"
                      defaultMessage={"is available on our premium plan. Upgrade now or start a {TRIALPERIOD}-day free trial to find out all about nTask premium features."}
                      values={{TRIALPERIOD: TRIALPERIOD}}
                    />
                  }
                  showBodyImg={true}
                  showDescription={true}
                  imgUrl={Board}
                />
              </div>
            </div>{" "}
          </>
        ) : ( */}
          <>
            {kanbanView && !isEmpty(board.data.kanban) ? (
              <KanbanView
                open={this.props.open}
                data={board.data.kanban}
                unsortedTask={board.data.unsortedTask}
                quickFilters={quickFilters}
                activityLogs={activityLogs}
                handleClearActivityLog={this.handleClearActivityLog}
                createActivityLog={this.createActivityLog}
                quote={quote}
                handleSelectProject={this.onSelectGrid}
                handleClickEndPreview={this.handleClickEndPreview}
                handleUseTemplate={this.handleUseTemplate}
                forTemplatesSetColumnsInParent={this.forTemplatesSetColumnsInParent}
                openBoardView={this.openBoardView}
              />
            ) : (
              <BoardGridView
                onSelectGrid={this.onSelectGrid}
                quote={quote}
                open={this.props.open}
                boardsTemplates={boardsTemplates}
                handleTemplatePreview={this.handleTemplatePreview}
                setView={this.setView}
                currentView={currentView}
                handleUseTemplate={this.handleUseTemplate}
                handleEditBoard={this.handleUseTemplate}
                intl={intl}
              />
            )}
            {isUseTemplateDialog && (
              <DefaultDialog
                title={
                  isSavedAsTemplate ? (
                    <FormattedMessage
                      id="board.all-boards.board-creation-dialogue.saveasa-template-title"
                      defaultMessage="Save Board as a Template"
                    />
                  ) : updateBoard ? (
                    <FormattedMessage
                      id="board.all-boards.board-creation-dialogue.editdetails"
                      defaultMessage="Edit Details"
                    />
                  ) : (
                    <FormattedMessage
                      id="board.all-boards.board-creation-dialogue.create-from-template-label"
                      defaultMessage="Create Board from Template"
                    />
                  )
                }
                open={isUseTemplateDialog}
                disableBackdropClick={true}
                disableEscapeKeyDown={true}
                disableEnforceFocus
                onClose={this.handleDialogClose}>
                <AddNewBoard
                  closeAction={this.handleDialogClose}
                  isTemplateView={updateBoard ? false : true}
                  data={newBoardData}
                  createdProject={this.createdProject}
                  updateBoard={updateBoard}
                  isSavedAsTemplate={isSavedAsTemplate}
                  // getTemplates={this.getTemplates}
                  boardPer={newBoardData.userPermission ? newBoardData.userPermission.permission.board : false}
                />
              </DefaultDialog>
            )}
          </>
        {/* // )} */}
      </>
    );
  }
}

Index.defaultProps = {
  /** default props  */
  classes: {},
  theme: {},
  GetKanbanListingByProjectId: () => {},
  UseTheTemplate: () => {},
  clearActivityLog: () => {},
  quickFilters: [],
  projects: [],
};

const mapStateToProps = state => {
  return {
    board: state.boards,
    projects: state.projects.data || [],
  };
};

export default compose(
  withRouter,
  withSnackbar,
  injectIntl,
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, {
    GetKanbanListingByProjectId,
    clearKanbanStoreData,
    // GetTemplates,
    GetKanbanTemplate,
    UseTheTemplate,
    clearActivityLog
  })
)(Index);
