import React, { useState, useEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Tooltip from "@material-ui/core/Tooltip";
import SvgIcon from "@material-ui/core/SvgIcon";
import FiberManualRecordIcon from "@material-ui/icons/FiberManualRecord";
import { BlockPicker } from "react-color";
import { FormattedMessage } from "react-intl";
import styles from "./styles";
import CustomIconButton from "../../components/Buttons/IconButton";
import ConvertToTaskIcon from "../../components/Icons/ConvertToTaskIcon";
import DeleteActionIcon from "../../components/Icons/DeleteActionIcon";
import EditReplyActionIcon from "../../components/Icons/EditReplyActionIcon";
import IconSaveAsTemplate from "../../components/Icons/IconSaveAsTemplate";
import DeleteBulkProjectDialogContent from "../../components/Dialog/Popups/deleteBoardConfirmation";
import DeleteConfirmDialog from "../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import helper from "../../helper";
import { DeleteProject, SaveProject } from "../../redux/actions/projects";
import { UpdateKanbanBoard, DeleteBoard } from "../../redux/actions/boards";
import { FetchWorkspaceInfo } from "../../redux/actions/workspace";
import ColorImagesModal from "../../components/ColorImagesModal";
import autoCompleteStyles from "../../assets/jss/components/autoComplete";
import cloneDeep from "lodash/cloneDeep";
import { teamCanView } from "../../components/PlanPermission/PlanPermission";

type BoardsActionsProps = {
  classes: Object,
  theme: Object,
  data: {},
  handleEditBoard: Function,
  FetchWorkspaceInfo: Function,
  handleSelectStatusColor: Function,
  boardPer: Object,
};

function BoardsActions(props: BoardsActionsProps) {
  const {
    classes,
    theme,
    data,
    handleEditBoard,
    handleUseTemplate,
    intl,
    boardPer,
    FetchWorkspaceInfo,
    handleSelectStatusColor,
    showSnackBar
  } = props;
  const [isDeletion, setValue] = useState(false);
  const [disabled, setDisableValue] = useState(true);
  const [step, setStep] = useState(0);
  const [deleteBtnQuery, setQuery] = useState("");
  const [pickerOpen, setPicker] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const [color, setColor] = useState("");
  const [prevColor, setPrev] = useState("");
  const handleImageSelect = selectedImage => {
    setAnchorEl(null);
    setPicker(false);
    updatePicker(false, selectedImage);
  };
  const handleColorSelect = selectedColor => {
    setAnchorEl(null);
    setPicker(false);
    updatePicker(true, selectedColor);
  };
  const openDeleteModal = event => {
    setValue(true);
    event.stopPropagation();
  };
  const openColorPicker = event => {
    event.stopPropagation();
    setPicker(true);
    setAnchorEl(event.target);
  };
  const closeConfirmationPopup = () => {
    setValue(false);
  };
  const handleTypeProjectInput = event => {
    const { value } = event.target;
    value && value.trim().toLowerCase() === data.boardName.toLowerCase()
      ? setDisableValue(false)
      : setDisableValue(true);
  };
  const handleClickAway = () => {
    setAnchorEl(null);
    //setPicker(false);
  };
  const handleDelete = (e, pId) => {
    // const type = helper.PROJECT_BULK_TYPES("Delete");
    setQuery("progress");
    let deleteData = { projectId: pId };
    props.DeleteBoard(deleteData, response => {
      setQuery("");
      setValue(false);
      FetchWorkspaceInfo("", () => {});
    }, err => {
      if(err && err.data) showSnackBar(err.data.message, "error");
      setQuery("");
      setValue(false);
    });
  };
  const handleDeleteProjectStep = () => {
    setStep(1);
  };
  const setStepBack = () => {
    setStep(0);
  };
  const updatePicker = (isColor, item) => {
    let objectClone = { ...data };
    if (isColor) {
      setColor(item);
      if (item !== prevColor) {
        objectClone.boardColorCode = item;
        objectClone.boardImagePath = null;
        objectClone.useProjectStatusColor = false;
        setPrev(item);
        props.UpdateKanbanBoard(objectClone, () => {});
      }
    } else {
      objectClone.boardImagePath = item.urls.small;
      objectClone.boardColorCode = "";
      objectClone.useProjectStatusColor = false;
      props.UpdateKanbanBoard(objectClone, () => {});
    }
  };

  return (
    <>
      <div style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
        {boardPer.backgroundBoardColor.cando ? (
          data.boardImagePath ? (
            <Tooltip
              classes={{
                tooltip: classes.tooltip,
              }}
              title={
                <FormattedMessage
                  id="board.all-boards.board-actions-tooltips.color"
                  defaultMessage="Color"
                />
              }
              placement="bottom">
              <button
                onClick={openColorPicker}
                style={{
                  borderRadius: "50%",
                  height: 36,
                  width: 36,
                  overflow: "hidden",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  border: "none",
                }}>
                <img
                  src={data.boardImagePath}
                  style={{
                    border: "1px solid rgb(255, 255, 255)",
                    borderRadius: "50%",
                    width: 28,
                    height: 28,
                    padding: "0",
                    alignSelf: "auto",
                  }}
                />
              </button>
            </Tooltip>
          ) : (
            <CustomIconButton
              btnType="filledWhite"
              onClick={openColorPicker}
              style={{
                borderRadius: "50%",
                border: "none",
                marginLeft: 6,
                boxShadow: "0px 3px 6px #0000001A",
                padding: 5,
                // backgroundImage: `url(${data.backgroundImagePath})`
                backgroundColor: "#fff",
                color: data.boardColorCode,
              }}>
              <Tooltip
                classes={{
                  tooltip: classes.tooltip,
                }}
                title={
                  <FormattedMessage
                    id="board.all-boards.board-actions-tooltips.color"
                    defaultMessage="Color"
                  />
                }
                placement="bottom">
                <FiberManualRecordIcon />
              </Tooltip>
            </CustomIconButton>
          )
        ) : null}
        {anchorEl && (
          <ColorImagesModal
            anchorTarget={anchorEl}
            handleImageSelect={handleImageSelect}
            handleColorSelect={handleColorSelect}
            handleClickAway={handleClickAway}
            intl={intl}
            useProjectStatusColor={data.useProjectStatusColor}
            viewMode="edit"
            handleClickStatusSelect={() => {
              setAnchorEl(null);
              handleSelectStatusColor();
            }}
          />
        )}
        {boardPer.saveBoardTemplate.cando && teamCanView("customStatusAccess") && (
          <CustomIconButton
            btnType="filledWhite"
            //   onClick={onConvertToTaskHandler.bind(this, comment)}
            onClick={e => {
              e.stopPropagation();
              handleUseTemplate(data, false, true);
            }}
            style={{
              borderRadius: "50%",
              border: "none",
              marginLeft: 6,
              boxShadow: "0px 3px 6px #0000001A",
              padding: 8,
              backgroundColor: "#fff",
            }}>
            <Tooltip
              classes={{
                tooltip: classes.tooltip,
              }}
              title={
                <FormattedMessage
                  id="board.all-boards.board-actions-tooltips.saveastemplate"
                  defaultMessage="Save as template"
                />
              }
              placement="bottom">
              <SvgIcon
                viewBox="0 0 12 15.325"
                htmlColor={theme.palette.secondary.medDark}
                style={{ fontSize: "16px" }}
                className={classes.actionIcon}>
                <IconSaveAsTemplate />
              </SvgIcon>
            </Tooltip>
          </CustomIconButton>
        )}
        {boardPer.boardDetail.cando && (
          <CustomIconButton
            btnType="filledWhite"
            onClick={e => {
              e.stopPropagation();
              handleEditBoard(data, true);
            }}
            style={{
              borderRadius: "50%",
              border: "none",
              marginLeft: 6,
              boxShadow: "0px 3px 6px #0000001A",
              padding: 8,
              backgroundColor: "#fff",
            }}>
            <Tooltip
              classes={{
                tooltip: classes.tooltip,
              }}
              title={
                <FormattedMessage
                  id="board.all-boards.board-actions-tooltips.edit"
                  defaultMessage="Edit"
                />
              }
              placement="bottom">
              <SvgIcon
                viewBox="0 0 16 16.07"
                htmlColor={theme.palette.secondary.medDark}
                style={{ fontSize: "16px" }}
                className={classes.actionIcon}>
                <EditReplyActionIcon />
              </SvgIcon>
            </Tooltip>
          </CustomIconButton>
        )}
        {boardPer.deleteBoard.cando && (
          <CustomIconButton
            btnType="filledWhite"
            //   onClick={onConvertToTaskHandler.bind(this, comment)}
            style={{
              borderRadius: "50%",
              border: "none",
              marginLeft: 6,
              boxShadow: "0px 3px 6px #0000001A",
              padding: 8,
              backgroundColor: "#fff",
            }}
            onClick={openDeleteModal}>
            <Tooltip
              classes={{
                tooltip: classes.tooltip,
              }}
              title={
                <FormattedMessage
                  id="board.all-boards.board-actions-tooltips.delete"
                  defaultMessage="Delete"
                />
              }
              placement="bottom">
              <SvgIcon
                viewBox="0 0 16 16"
                htmlColor={theme.palette.secondary.medDark}
                style={{ fontSize: "16px" }}
                className={classes.actionIcon}>
                <DeleteActionIcon />
              </SvgIcon>
            </Tooltip>
          </CustomIconButton>
        )}

        <DeleteConfirmDialog
          open={isDeletion}
          closeAction={closeConfirmationPopup}
          cancelBtnText={
            <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
          }
          successBtnText={
            <FormattedMessage
              id="common.action.delete.anyway.label"
              defaultMessage="Delete Anyway"
            />
          }
          alignment="center"
          headingText={
            <FormattedMessage
              id="common.action.delete.confirmation.delete-button.label"
              defaultMessage="Delete"
            />
          }
          disabled={step === 0 ? false : disabled}
          successAction={
            step === 0
              ? () => {
                  handleDeleteProjectStep();
                }
              : () => handleDelete(event, data.projectId)
          }
          btnQuery={deleteBtnQuery}>
          {isDeletion ? (
            <DeleteBulkProjectDialogContent
              step={step}
              project={data}
              handleTypeProjectInput={handleTypeProjectInput}
              setStepBack={setStepBack}
            />
          ) : null}
        </DeleteConfirmDialog>
      </div>
    </>
  );
}

BoardsActions.defaultProps = {
  classes: {},
  theme: {},
  data: {},
  boardPer: {},
  handleEditBoard: () => {},
  handleSelectStatusColor: () => {},
  // showSnackBar: () => {},
};
const mapStateToProps = state => {
  return {};
};

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, {
    FetchWorkspaceInfo,
    DeleteProject,
    UpdateKanbanBoard,
    DeleteBoard,
  })
)(BoardsActions);
