import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./styles";
import FormControl from "@material-ui/core/FormControl";
import CustomButton from "../../components/Buttons/CustomButton";
import QuickFilterIcon from "../../components/Icons/QuickFilterIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import PlainMenu from "../../components/Menu/menu";
import MenuList from "@material-ui/core/MenuList";
import ListItemText from "@material-ui/core/ListItemText";
import MenuItem from "@material-ui/core/MenuItem";
import RoundIcon from "@material-ui/icons/Brightness1";
import { FormattedMessage, injectIntl } from "react-intl";
class QuickFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quickFilters: ["Show All"],
      open: false,
      placement: "bottom-start",
      anchorEl: "anchorEl",
    };
    this.allFilterList = [
      { key: "Show All", value: "Show All" },
      { key: "Due Today", value: "Due Today" },
      { key: "Over Due", value: "Over Due" },
      { key: "Starred", value: "Starred" },
      { key: "Assigned To Me", value: "Assigned To Me" },
    ];
    this.child = React.createRef();
  }

  handleClick(event, placement) {
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }

  handleClose = () => {
    if (this.anchorEl.contains(event.target)) {
      return;
    }
    this.setState({ open: false });
  };
  handleSelectClick = value => {
    const { data } = this.props;
    let items = [];
    let { quickFilters } = this.state;

    if (value == "Show All") {
      if (data && data.subtaskFilter) {
        items = [...items, "Show All", "Subtask"];
      } else {
        items = ["Show All"];
      }
    } else {
      if (quickFilters.indexOf("Show All") > -1) {
        quickFilters = quickFilters.filter(item => item != "Show All");
      }
      quickFilters.indexOf(value) == -1
        ? (items = [...quickFilters, value])
        : (items = quickFilters.filter(item => item !== value));
    }
    this.setState({ quickFilters: items }, () => {
      this.props.handleSelectQuickFilter(items);
    });
  };
  translate = filterText => {
    let id = "";
    switch (filterText) {
      case "Due Today":
        id = "common.action.other.duetoday";
        break;
      case "Over Due":
        id = "common.action.other.overdue";
        break;
      case "Starred":
        id = "common.action.other.started";
        break;
      case "Assigned To Me":
        id = "common.search-filter.actionb";
        break;
    }
    return id == ""
      ? filterText
      : this.props.intl.formatMessage({ id: id, defaultMessage: filterText });
  };
  render() {
    const { classes, theme, data } = this.props;
    const { open, quickFilters } = this.state;

    const other = [
      { name: "Due Today", color: theme.palette.filter.dueToday },
      { name: "Over Due", color: theme.palette.filter.overDue },
      { name: "Starred", color: theme.palette.filter.starred },
      { name: "Assigned To Me", color: theme.palette.filter.starred },
    ];

    const selectedFilter = [];
    this.allFilterList.map(item => {
      if (quickFilters.indexOf(item.key) != -1) selectedFilter.push(item.value);
    });
    const customFilterSelect =
      selectedFilter.length == 0 ||
      (selectedFilter.length == 1 && selectedFilter.indexOf("Show All") > -1)
        ? false
        : true;
    const selectedFiltersLbl = customFilterSelect
      ? selectedFilter.length > 1
        ? this.translate(selectedFilter[0]) + " & +" + (selectedFilter.length - 1)
        : this.translate(selectedFilter[0])
      : this.props.intl.formatMessage({ id: "common.all.label", defaultMessage: "All" });

    return (
      <>
        <FormControl className={classes.formControl}>
          <CustomButton
            onClick={event => {
              this.handleClick(event, "bottom-end");
            }}
            id="taskQuickFilterButton"
            buttonRef={node => {
              this.anchorEl = node;
            }}
            style={{
              padding: customFilterSelect ? "4px 8px 4px 4px" : "3px 8px 3px 4px",
              borderRadius: "4px",
              display: "flex",
              justifyContent: "space-between",
              minWidth: "auto",
              whiteSpace: "nowrap",
              height: 32,
              backgroundColor: !data.background?.backGroundImage ? "#fff" : "#f3f3f380",
              color: !data.background?.backGroundImage ? "#333333" : "black",
            }}
            btnType={customFilterSelect ? "lightBlue" : "white"}
            variant="contained">
            <SvgIcon
              classes={{root: classes.quickFilterIconSize}}
              viewBox="0 0 24 24"
              className={
                customFilterSelect ? classes.qckFfilterIconSelected : classes.qckFilterIconSvg
              }
              style={{
                color: !data.background?.backGroundImage ? "#7e7e7e" : "black",
              }}>
              <QuickFilterIcon />
            </SvgIcon>
            <span
              className={customFilterSelect ? classes.qckFilterLblSelected : classes.qckFilterLbl}
              style={{
                color: !data.background?.backGroundImage ? "#7e7e7e" : "black",
              }}>
              <FormattedMessage id="common.show.label" defaultMessage="Show" /> :{" "}
            </span>
            <span className={classes.checkname}>{selectedFiltersLbl}</span>
          </CustomButton>
          <PlainMenu
            open={this.state.open}
            closeAction={this.handleClose}
            placement="bottom-start"
            anchorRef={this.anchorEl}
            style={{ width: 280 }}
            offset="0 15px">
            <MenuList disablePadding>
              <MenuItem
                className={`${classes.statusMenuItemCnt} ${
                  !customFilterSelect ? classes.selectedValue : ""
                }`}
                classes={{
                  root: classes.menuRoot,
                  selected: classes.statusMenuItemSelected,
                }}
                style={{ borderBottom: `1px solid ${theme.palette.border.lightBorder}` }}
                value="Show All"
                onClick={() => {
                  this.handleSelectClick("Show All");
                }}>
                <ListItemText
                  primary={<FormattedMessage id="common.show.all" defaultMessage="Show All" />}
                  classes={{ primary: classes.plainItemText }}
                />
              </MenuItem>
              {other.map(item => {
                if (item.name == "Assigned To Me") {
                  return (
                    <>
                      <MenuItem
                        value={item.name}
                        key={item.name}
                        classes={{ root: classes.highlightItem }}
                        onClick={() => {
                          this.handleSelectClick(item.name);
                        }}
                        className={`${
                          quickFilters.indexOf(item.name) > -1 ? classes.selectedValue : ""
                        }`}>
                        {
                          <FormattedMessage
                            id="common.search-filter.actionb"
                            defaultMessage="Assigned to me"
                          />
                        }
                      </MenuItem>
                    </>
                  );
                } else
                  return (
                    <>
                      {" "}
                      <MenuItem
                        key={item.name}
                        value={item.name}
                        className={`${classes.statusMenuItemCnt} ${
                          quickFilters.indexOf(item.name) > -1 ? classes.selectedValue : ""
                        }`}
                        classes={{
                          root: classes.menuRoot,
                          selected: classes.statusMenuItemSelected,
                        }}
                        onClick={() => {
                          this.handleSelectClick(item.name);
                        }}>
                        <RoundIcon
                          htmlColor={item.color}
                          classes={{ root: classes.statusIcon }}
                        />
                        <ListItemText
                          primary={this.translate(item.name)}
                          classes={{ primary: classes.statusItemText }}
                        />
                      </MenuItem>
                    </>
                  );
              })}
            </MenuList>
          </PlainMenu>
        </FormControl>
      </>
    );
  }
}

QuickFilter.defaultProps = {
  classes: {},
  theme: {},
};
const mapStateToProps = state => {
  return {};
};

export default compose(
  injectIntl,
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, {})
)(QuickFilter);
