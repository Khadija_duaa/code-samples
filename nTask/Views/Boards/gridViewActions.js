import React, { useState, useEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import combineStyles from "../../utils/mergeStyles";
import styles from "./styles";
import menuStyles from "../../assets/jss/components/menu";
import SortBy from "../../components/Icons/SortBy";
import SvgIcon from "@material-ui/core/SvgIcon";
import CustomButton from "../../components/Buttons/CustomButton";
import SelectionMenu from "../../components/Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import { FormattedMessage } from "react-intl";

type GridViewActionsProps = {
  classes: Object,
  theme: Object,
  handleSelectOption: Function,
  data: Array,
};

function GridViewActions(props: GridViewActionsProps) {
  const { classes, theme, handleSelectOption, data } = props;

  const [checkSelected, setCheckSelected] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const [checked, setChecked] = useState({ key: 0, name: "None" });

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleClickBtn = event => {
    // Function Opens the dropdown
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };

  const handleToggle = (e, value) => {
    e.stopPropagation();
    if (value.key == 0) {
      setCheckSelected(false);
    } else {
      setCheckSelected(true);
    }
    setChecked(value);
    handleClose();
    handleSelectOption(value);
  };

  const open = Boolean(anchorEl);
  return (
    <ClickAwayListener onClickAway={handleClose}>
      <div>
        <div className={classes.sortBtnsCnt}>
          <CustomButton /** Sort By Button  */
            onClick={event => {
              handleClickBtn(event);
            }}
            ref={anchorEl}
            style={{
              // minWidth: 95,
              padding: checkSelected ? "4px 8px 4px 4px" : "3px 4px",
              display: "flex",
              justifyContent: "space-between",
              minWidth: "auto",
              whiteSpace: "nowrap",
              marginRight: 5,
              height: 34,
            }}
            btnType={checkSelected ? "lightBlue" : "white"}
            variant="contained">
            <SvgIcon
              viewBox="0 0 24 24"
              className={checkSelected ? classes.sortIconSelected : classes.sortIconSvg}>
              <SortBy />
            </SvgIcon>
            <span className={checkSelected ? classes.sortLblSelected : classes.sortLbl}>
              {<FormattedMessage id="common.sort-by.label" defaultMessage="Sort By" />}
            </span>
            {checkSelected ? <span className={classes.checkname}>{`: ${checked.label}`}</span> : null}
          </CustomButton>
        </div>
        <SelectionMenu
          open={open}
          closeAction={handleClose} /** function call when close drop down */
          placement={"bottom-start"}
          checkedType="multi"
          anchorRef={anchorEl}
          // style={{ width: 400 }}
          list={
            <List>
              {data.map((value /** displaying the drop down list array coming from props  */) => (
                <ListItem
                  key={value.key}
                  button
                  disableRipple={true}
                  className={`${classes.MenuItem} ${checked && checked.key == value.key ? classes.selectedValue : ""
                    }`}
                  classes={{ selected: classes.statusMenuItemSelected }}
                  onClick={e => {
                    handleToggle(e, value);
                  }} /** on select value from drop down , function call passing the whole 'value' object */
                >
                  <ListItemText
                    primary={value.label} /** displaying name property from array coming ffrom props */
                    classes={{
                      primary: classes.statusItemText + (checked && checked.key == value.key ? (" " + classes.statusItemTextSelected) : ""),
                    }}
                  />
                </ListItem>
              ))}
            </List>
          }
        />
      </div>
    </ClickAwayListener>
  );
}

GridViewActions.defaultProps = {
  classes: {},
  theme: {},
  handleSelectOption: () => { },
  data: [],
};
const mapStateToProps = state => {
  return {};
};

export default compose(
  withStyles(combineStyles(styles, menuStyles), { withTheme: true }),
  connect(mapStateToProps, {})
)(GridViewActions);
