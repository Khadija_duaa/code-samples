//flow

import React, { useState, useEffect } from "react";
import { withSnackbar } from "notistack";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect, useDispatch } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./styles";
import Grid from "@material-ui/core/Grid";
import Truncate from "react-truncate";
import CustomTooltip from "../../components/Tooltip/Tooltip";
import { getAllBoards, GetAllTemplatesByGroup, clearBoardsData, UpdateKanbanBoard } from "../../redux/actions/boards";

import Typography from "@material-ui/core/Typography";
import LeftArrow from "@material-ui/icons/ChevronLeft";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import ToggleButton from "@material-ui/lab/ToggleButton";
import TrainRounded from "@material-ui/icons/TrainRounded";
import { calculateContentHeight } from "../../utils/common";
import CustomIconButton from "../../components/Buttons/CustomIconButton";
import AddIcon from "@material-ui/icons/Add";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import MoreActionDropDownView from "./MoreActionDropDownView";
import DefaultDialog from "../../components/Dialog/Dialog";
import AddNewBoard from "../AddNewForms/AddNewBoard";
import BoardsActions from "./BoardsActions";
import Footer from "../../components/Footer/Footer";
import Templates from "../../components/BoardsTemplates";
import IconButton from "../../components/Buttons/IconButton";
import UpArrow from "@material-ui/icons/ArrowDropUp";
import DropdownArrow from "@material-ui/icons/ArrowDropDown";
import { FormattedMessage, injectIntl } from "react-intl";
import BoardsGridViewActions from "./gridViewActions";
import SearchInput, { createFilter } from "react-search-input";
import SearchIcon from "@material-ui/icons/Search";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import cloneDeep from "lodash/cloneDeep";
import isEmpty from "lodash/isEmpty";
import { teamCanView } from "../../components/PlanPermission/PlanPermission";
import Board from "../../assets/images/unplanned/board.png";
// import boardAnimation from "../../assets/images/unplanned/Board Animation.gif";
import boardAnimation from "../../assets/images/unplanned/Board-Animation-Improved.gif";
import template from "../../assets/images/unplanned/templatesImage.png";
import UnPlanned from "../billing/UnPlanned/UnPlanned";
import queryString from "query-string";
import { taskDetailDialogState } from "../../redux/actions/allDialogs";
const htmlToText = require("html-to-text");
import {TRIALPERIOD} from '../../components/constants/planConstant';
import Loader from "../../components/Loader/Loader";

type BoardGridViewProps = {
  /** type checking */
  classes: Object,
  theme: Object,
  allProjects: Array,
  // boardsTemplates: Array,
  onSelectGrid: Function,
  handleTemplatePreview: Function,
  handleEditBoard: Function,
  getAllBoards: Function,
  GetAllTemplatesByGroup: Function,
  clearBoardsData: Function,
  setView: Function,
  quote: String,
  currentView: String,
  boardPer: Object,
  boards: Object,
  workspaceTemplates: Array,
};

function BoardGridView(props: BoardGridViewProps) {
  const {
    theme,
    classes,
    allProjects,
    onSelectGrid,
    quote,
    // boardsTemplates,
    handleTemplatePreview,
    currentView,
    setView,
    handleUseTemplate,
    handleEditBoard,
    intl,
    boardPer,
    getAllBoards,
    boards,
    GetAllTemplatesByGroup,
    clearBoardsData,
    UpdateKanbanBoard,
    enqueueSnackbar,
    tasks
  } = props;

  // const [currentView, setView] = useState("boards");
  const [isDialogView, setDialogView] = useState(false);
  const [boardView, setBoardView] = useState(true);
  const [recentlyBoardView, setRecentlyBoardView] = useState(true);
  const handleChange = (event, nextView) => {
    if (nextView) {
      setView(nextView);
      if (nextView == "templates") getAllTemplatesData();
    }
  };
  const dispatch = useDispatch()
  const [allboards, setAllBoards] = useState([]);
  const [sortby, setSortBy] = useState(1);
  const [searchTerm, setSearchTerm] = useState("");
  const [searchQuery, setSearchQuery] = useState(["boardName"]);
  const [sortBydata, setSortBydata] = useState([
    // {
    //   label: intl.formatMessage({ id: "common.sort-by.none", defaultMessage: "None" }),
    //   key: 0,
    //   name: "None",
    // },
    {
      label: intl.formatMessage({
        id: "common.sort-by.newesttooldest",
        defaultMessage: "Newest to Oldest",
      }),
      key: 1,
      name: "Newest to Oldest",
    },
    {
      label: intl.formatMessage({
        id: "common.sort-by.oldesttonewest",
        defaultMessage: "Oldest to Newest",
      }),
      key: 2,
      name: "Oldest to Newest",
    },
    {
      label: intl.formatMessage({
        id: "common.sort-by.ascending",
        defaultMessage: "Ascending (A-Z)",
      }),
      key: 3,
      name: "Ascending (A-Z)",
    },
    {
      label: intl.formatMessage({
        id: "common.sort-by.decending",
        defaultMessage: "Descending (Z-A)",
      }),
      key: 4,
      name: "Descending (Z-A)",
    },
  ]);

  const [templates, setTemplates] = useState(null);
  const [boardsTemplates, setBoardsTemplates] = useState([]);

  const handleDialogClose = () => setDialogView(false);
  const handleDialogOpen = () => setDialogView(true);
  const handleBoardView = () => setBoardView(view => !boardView);
  const handleRecentBoardView = () => setRecentlyBoardView(!recentlyBoardView);
  const handleSortBy = option => {
    // const projectss = [...allboards];
    switch (option.key) {
      case 0:
        setAllBoards(props.boards.data.boards);
        break;
      case 1: {
        //   projectss.sort((a, b) => {
        //   let dateA = new Date(a.createdDate);
        //   let dateB = new Date(b.createdDate);
        //   return dateB - dateA;
        // });
        // setAllBoards(projectss);
        setSortBy(1);
        break;
      }
      case 2: {
        //  projectss.sort((a, b) => {
        //   let dateA = new Date(a.createdDate);
        //   let dateB = new Date(b.createdDate);
        //   return dateA - dateB;
        // });
        // setAllBoards(projectss);
        setSortBy(2);
        break;
      }
      case 3: {
        // projectss.sort((a, b) => {
        //   let nameA = a.boardName.toLowerCase();
        //   let nameB = b.boardName.toLowerCase();
        //   if (nameA < nameB) return -1;
        // });
        // setAllBoards(cloneDeep(projectss));
        setSortBy(3);
        break;
      }
      case 4: {
        //  projectss.sort((a, b) => {
        //   let nameA = a.boardName.toLowerCase();
        //   let nameB = b.boardName.toLowerCase();
        //   if (nameA > nameB) return -1;
        // });
        // setAllBoards(cloneDeep(projectss));
        setSortBy(4);
        break;
      }
      default:
        break;
    }
  };

  const getRecentProject = (allProjects = []) => {
    let recentProject = allProjects.filter(ele => ele.lastViewedDate);
    recentProject = recentProject.sort(
      (a, b) => new Date(b.lastViewedDate) - new Date(a.lastViewedDate)
    );
    if (recentProject.length > 4) {
      return recentProject.slice(0, 4);
    }
    return recentProject;
  };
  const sortBy = (a,b) => {
    if (sortby == 1) {
      return new Date(b.createdDate) - new Date(a.createdDate);
    }
    else if (sortby == 2) {
      return new Date(a.createdDate) - new Date(b.createdDate);
    }
    else if (sortby == 3) {
      let nameA = a.boardName.toLowerCase();
      let nameB = b.boardName.toLowerCase();
      if (nameA < nameB) return -1;
    }
    else if (sortby == 4) {
      let nameA = a.boardName.toLowerCase();
      let nameB = b.boardName.toLowerCase();
      if (nameA > nameB) return -1;
    }
  }
  const renderAllProjects = projects => {
    /** function that returns UI of all projects */
    return (
      <>
        {projects &&
          projects.sort((a, b) => {
            return sortBy(a,b); //sort value according to user selected type
          }).map(board => {
            let description = htmlToText.fromString(board.boardDescription);
            return (
              <>
                <Grid
                  item
                  classes={{ item: classes.boardsDashboardStyles }}
                  xs={6}
                  sm={6}
                  md={4}
                  lg={3}
                  xl={2}>
                  <div
                    className={classes.boardProjectItemInner}
                    style={{
                      backgroundColor: board.boardColorCode,
                      backgroundImage: board.boardImagePath
                        ? `${"linear-gradient(rgb(0 0 0 / 50%), rgb(0 0 0 / 0%)),"}url(${
                            board.boardImagePath
                          })`
                        : ``,
                      backgroundPosition:"50% center"

                    }}
                    onClick={event => {
                      boardPer.boardDetail.cando ? onSelectGrid(event, board) : null;
                    }}>
                    <div style={{ zIndex: 1000 }}>
                      <div className={classes.boardIcon}>
                        {/* <MoreActionDropDownView
                      color={ board.colorCode ? theme.palette.common.white : theme.palette.secondary.medDark}
                      font={{fontSize: "24px !important"}} 
                    /> */}
                        <BoardsActions
                          data={board}
                          handleEditBoard={handleEditBoard}
                          handleUseTemplate={handleUseTemplate}
                          intl={intl}
                          boardPer={board.userPermission.permission.board}
                          handleSelectStatusColor={()=>{handleSelectStatusColor(board)}}
                          showSnackBar={showSnackBar}
                        />
                      </div>
                      <div className={classes.projectNameCnt}>
                      <CustomTooltip
                         helptext={board.boardName}
                         placement="bottom"
                      >
                        <Truncate
                          lines={2}
                          trimWhitespace={true}
                          width={235}
                          ellipsis={<span>...</span>}
                          className={classes.boardProjectHeading}
                          style={{
                            color:
                              (!board.boardColorCode ||
                                board.boardColorCode === "#FFFFFF" ||
                                board.boardColorCode === "#e8e8e8" ||
                                board.boardColorCode === "") &&
                              !board.boardImagePath &&
                              "#202020",
                          }}>
                          {board.boardName}
                         
                        </Truncate>
                       </CustomTooltip>
                      </div>
                      <div title={description} className={classes.projectNameCnt}>
                     
                        <Truncate
                          lines={2}
                          trimWhitespace={true}
                          width={225}
                          ellipsis={<span>...</span>}
                          className={classes.boardProjectDescription}
                          style={{
                            color:
                              (!board.boardColorCode ||
                                board.boardColorCode === "#FFFFFF" ||
                                board.boardColorCode === "#e8e8e8" ||
                                board.boardColorCode === "") &&
                              !board.boardImagePath &&
                              "#202020",
                          }}>
                          {description}
                        </Truncate>
                      
                      </div>
                    </div>
                    {/* <div className={classes.overlay}></div> */}
                  </div>
                </Grid>
              </>
            );
          })}
      </>
    );
  };
  const renderRecentProjects = () => {
    /** function that returns UI of all projects */

    let projects = getRecentProject(allboards);
    return (
      <>
        {projects.map(board => {
          let description = htmlToText.fromString(board.boardDescription);
          return (
            <>
              <Grid
                item
                classes={{ item: classes.boardsDashboardStyles }}
                xs={6}
                sm={6}
                md={4}
                lg={3}
                xl={2}>
                <div
                  className={classes.boardProjectItemInner}
                  style={{
                    background: board.boardColorCode,
                    backgroundImage: board.boardImagePath
                      ? `${"linear-gradient(rgb(0 0 0 / 50%), rgb(0 0 0 / 0%)),"}url(${
                          board.boardImagePath
                        })`
                      : ``,
                      backgroundPosition:"50% center"
                  }}
                  onClick={event => {
                    boardPer.boardDetail.cando ? onSelectGrid(event, board) : null;
                  }}>
                  <div style={{ zIndex: 1000 }}>
                    <div className={classes.boardIcon}>
                      {/* <MoreActionDropDownView
                      color={ board.colorCode ? theme.palette.common.white : theme.palette.secondary.medDark}
                      font={{fontSize: "24px !important"}} 
                    /> */}
                      <BoardsActions
                        data={board}
                        handleEditBoard={handleEditBoard}
                        handleUseTemplate={handleUseTemplate}
                        intl={intl}
                        boardPer={board.userPermission.permission.board}
                        handleSelectStatusColor={()=>{handleSelectStatusColor(board)}}
                      />
                    </div>
                    <div title={board.boardName} className={classes.projectNameCnt}>
                      <CustomTooltip
                         helptext={board.boardName}
                         placement="bottom"
                      >
                      <Truncate
                        lines={2}
                        trimWhitespace={true}
                        width={235}
                        ellipsis={<span>...</span>}
                        className={classes.boardProjectHeading}
                        style={{
                          zIndex: 999,
                          color:
                            (!board.boardColorCode ||
                              board.boardColorCode === "#FFFFFF" ||
                              board.boardColorCode === "#e8e8e8" ||
                              board.boardColorCode === "") &&
                            !board.boardImagePath &&
                            "#202020",
                        }}>
                        {board.boardName}
                      </Truncate>
                      </CustomTooltip>
                    </div>
                    <div title={description} className={classes.projectNameCnt}>
                      <Truncate
                        lines={2}
                        trimWhitespace={true}
                        width={225}
                        ellipsis={<span>...</span>}
                        className={classes.boardProjectDescription}
                        style={{
                          color:
                            (!board.boardColorCode ||
                              board.boardColorCode === "#FFFFFF" ||
                              board.boardColorCode === "#e8e8e8" ||
                              board.boardColorCode === "") &&
                            !board.boardImagePath &&
                            "#202020",
                        }}>
                        {description}
                      </Truncate>
                    </div>
                  </div>
                  {/* <div className={classes.overlay}></div> */}
                </div>
              </Grid>
            </>
          );
        })}
      </>
    );
  };

  const handleSelectStatusColor=(data)=>{
    let obj = {
      boardColorCode: "",
      projectId: data.projectId,
      boardId: data.boardId,
      boardName: data.boardName,
      boardImagePath: "",
      useProjectStatusColor: !data.useProjectStatusColor
    };
    UpdateKanbanBoard(
      obj,
      succ => {},
      err => {}
    );
  }

  const searchUpdated = term => {
    setSearchTerm(term);
    const allProjectsBoards = props.boards.data.boards.filter(createFilter(term, searchQuery));
    if (term == "") {
      setAllBoards(props.boards.data.boards);
    } else {
      setAllBoards(allProjectsBoards);
    }
  };

  const searchTemplateUpdated = term => {
    const searchTemplates = templates.filter(createFilter(term, ["name"]));
    if (term == "") {
      setTemplates(boardsTemplates);
    } else {
      setTemplates(searchTemplates);
    }
  };

  const getAllBoardsData = () => {
    getAllBoards(
      succ => {},
      fail => {}
    );
  };

  const settingTemplates = templ => {
    let updatedArr = [];
    templ.map(ele => {
      updatedArr = updatedArr.concat(ele.templates);
    });
    setBoardsTemplates(updatedArr);
    setTemplates(updatedArr);
  };

  const getAllTemplatesData = () => {
    props.GetAllTemplatesByGroup(
      result => {
        settingTemplates(result);
      },
      fail => {}
    );
  };
  //Close task detail
  const closeTaskDetailsPopUp = () => {
    taskDetailDialogState(dispatch, {
      id: "",
      afterCloseCallBack: null,
      type: "",
    });
  };
  useEffect(() => {
    const taskDetailIdToOpen = queryString.parseUrl(window.location.href).query.taskId;
    const taskObj = tasks.find(t => t.taskId == taskDetailIdToOpen)
    if(taskObj && allboards.length) {
      const taskProjectBoard = allboards.find(b => b.projectId == taskObj.projectId);
      onSelectGrid(event, taskProjectBoard)
      taskDetailDialogState(dispatch, {
        id: taskDetailIdToOpen,
        afterCloseCallBack: () => {
          closeTaskDetailsPopUp()
        },
      })
    }
  }, [allboards]);
  useEffect(() => {
    /** RUNS FIRST TIME AND CALLS 2 API ONE FOR GETTING BOARDS AND OTHER IS GETTING FOR TEMPLATES */
    getAllBoardsData();
    // getAllTemplatesData();
    return () => {
      // clearBoardsData(false);
    };
  }, []);

  useEffect(() => {
    /** IF BOARDS COMING FROM STORE UPDATES THEN STATE OF THE BOARD ALSO UPDATES */
    setAllBoards(props.boards.data.boards);
  }, [props.boards.data.boards]);

  useEffect(() => {
    /** IF TEMPLATES COMING FROM STORE UPDATES THEN STATE OF THE TEMPLATES ALSO UPDATES */
    settingTemplates(props.workspaceTemplates);
  }, [props.workspaceTemplates]);

  //Snackbar function
  const showSnackBar = (snackBarMessage = "", type = null) => {
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "top",
          horizontal: "centerdp",
        },
        variant: type ? type : "info",
      }
    );
  };

  return !allboards ? (
    
    <Loader />
  ) : (
    <>
      {teamCanView("boardAccess") || teamCanView("customStatusAccess") ?
        <div className={classes.boardGridViewCnt}>
        <div className={classes.drawerHeader}>
          <Grid container classes={{ container: classes.taskDashboardCnt }}>
            {/* heading container starts here */}
            <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="center"
              classes={{ container: classes.taskDashboardHeader }}>
              <div className="flex_center_start_row">
                <div className={classes.toggleContainer}>
                  <ToggleButtonGroup
                    value={currentView}
                    exclusive
                    onChange={handleChange}
                    classes={{ root: classes.toggleBtnGroup, groupedHorizontal: classes.groupedHorizontal  }}>
                    <ToggleButton
                      value="boards"
                      classes={{
                        root: classes.toggleButton,
                        selected: classes.toggleButtonSelected,
                      }}>
                      <FormattedMessage id="board.label" defaultMessage="Boards" />
                    </ToggleButton>
                    <ToggleButton
                      value="templates"
                      classes={{
                        root: classes.toggleButton,
                        selected: classes.toggleButtonSelected,
                      }}>
                      <FormattedMessage id="board.template.label" defaultMessage="Templates" />
                    </ToggleButton>
                  </ToggleButtonGroup>
                </div>
              </div>

              <div className="flex_center_start_row">
                {currentView == "boards" && (
                  teamCanView("boardAccess") ? (
                  <>
                    {" "}
                    <BoardsGridViewActions handleSelectOption={handleSortBy} data={sortBydata} />
                    <div className={classes.searchInputCnt}>
                      {<SearchIcon className={classes.searchIcon} />}
                      <SearchInput
                        className={`${classes.searchInput} HtmlInput`}
                        onChange={searchUpdated}
                        maxLength="80"
                        placeholder={intl.formatMessage({
                          id: "board.search.searchbyboard",
                          defaultMessage: "Search boards by title",
                        })}
                      />
                    </div>{" "}
                  </>
                  ) : ("")
                )}
                {currentView == "templates" && (
                   teamCanView("templateAccess") ? (
                    <>
                  <div className={classes.searchInputCnt}>
                    {<SearchIcon className={classes.searchIcon} />}
                    <SearchInput
                      className={`${classes.searchInput} HtmlInput`}
                      onChange={searchTemplateUpdated}
                      maxLength="80"
                      placeholder={intl.formatMessage({
                        id: "board.search.searchbytemplate",
                        defaultMessage: "Search templates by title",
                      })}
                    />
                  </div>
                  </>
                   ) : ("")
                )}
              </div>
            </Grid>
          </Grid>
        </div>
      </div>
      : null}
      {currentView === "boards" && (
 
        !teamCanView("boardAccess") ? (
          <>
            {" "}
            <div className={classes.unplannedMain}>
              <div className={classes.unplannedCnt}>
                <UnPlanned
                  feature="premium"
                  titleTxt={
                    <FormattedMessage
                      id="common.discovered-dialog.premium-title"
                      defaultMessage="Wow! You've discovered a premium feature!"
                    />
                  }
                  boldText={intl.formatMessage({
                    id: "board.label",
                    defaultMessage: "Boards",
                  })}
                  descriptionTxt={
                    <FormattedMessage
                      id="common.discovered-dialog.list.project.label"
                      defaultMessage={"is available on our premium plan. Upgrade now or start a {TRIALPERIOD}-day free trial to find out all about nTask premium features."}
                      values={{TRIALPERIOD: TRIALPERIOD}}
                    />
                  }
                  showBodyImg={true}
                  showDescription={true}
                  imgUrl={boardAnimation}
                />
              </div>
            </div>{" "}
          </>
        ) : (
          <>
                 <div className={classes.boardsCnt} style={{ height: calculateContentHeight() }}>
          {isDialogView && (
            <DefaultDialog
              title={intl.formatMessage({
                id: "board.all-boards.board-creation-dialogue.title",
                defaultMessage: "Create New Board",
              })}
              disableEnforceFocus
              open={isDialogView}
              disableBackdropClick = {true}
              disableEscapeKeyDown = {true}
              onClose={handleDialogClose}>
              <AddNewBoard
                closeAction={handleDialogClose}
                isTemplateView={false}
                boardPer={false}
              />
            </DefaultDialog>
          )}
          <div className={classes.panel} style={{ marginBottom: 20 }}>
            <div className={classes.panelHeader}>
              <div className={classes.panelHeaderCnt}>
                <IconButton
                  style={{ marginBottom: 5 }}
                  btnType="transparent"
                  onClick={e => handleRecentBoardView()}>
                  {!recentlyBoardView ? (
                    <ArrowRightIcon htmlColor={theme.palette.secondary.medDark} />
                  ) : (
                    <DropdownArrow htmlColor={theme.palette.secondary.medDark} />
                  )}
                </IconButton>
                <span className={classes.boardOptTitle}>
                  <FormattedMessage
                    id="board.recently-viewed.label"
                    defaultMessage="Recently Viewed"
                  />
                </span>
              </div>
            </div>
            {recentlyBoardView ? (
              <div className={classes.panelBody}>
                <Grid container spacing={2}>
                  {renderRecentProjects()}
                </Grid>
              </div>
            ) : null}
          </div>
          <div className={classes.panel}>
            <div className={classes.panelHeader}>
              <div className={classes.panelHeaderCnt}>
                <IconButton
                  style={{ marginBottom: 5 }}
                  btnType="transparent"
                  onClick={e => handleBoardView()}>
                  {!boardView ? (
                    <ArrowRightIcon htmlColor={theme.palette.secondary.medDark} />
                  ) : (
                    <DropdownArrow htmlColor={theme.palette.secondary.medDark} />
                  )}
                </IconButton>
                <span className={classes.boardOptTitle}>
                  <FormattedMessage id="board.all-boards.label" defaultMessage="All Boards" />
                </span>
              </div>
            </div>
            {boardView ? (
              <div className={classes.panelBody}>
                <Grid container spacing={2}>
                  {boardPer.creatBoard.cando && (
                    <Grid
                      item
                      classes={{ item: classes.boardsDashboardStyles }}
                      xs={6}
                      sm={6}
                      md={4}
                      lg={3}
                      xl={2}>
                      <div className={classes.boardListItemInnerFirst} onClick={handleDialogOpen}>
                        <CustomIconButton
                          onClick={handleDialogOpen}
                          btnType="DarkGray"
                          className={classes.addBtn}>
                          <AddIcon
                            style={{ fontSize: "18px" }}
                            htmlColor={theme.palette.common.white}
                          />
                        </CustomIconButton>
                        <Typography variant="h5" className={classes.boardItemHeading}>
                          <FormattedMessage
                            id="board.all-boards.create-board-button.label"
                            defaultMessage="Create New Board"
                          />
                        </Typography>
                      </div>
                    </Grid>
                  )}
                  {renderAllProjects(allboards)}
                </Grid>
              </div>
            ) : null}
          </div>
        </div>
          </>
        )
      )}

      {currentView === "templates" && (

        !teamCanView("customStatusAccess") ? (
          <>
            {" "}
            <div className={classes.unplannedMain}>
              <div className={classes.unplannedCnt}>
                <UnPlanned
                  feature="business"
                  titleTxt={
                    <FormattedMessage
                      id="common.discovered-dialog.list.template.title"
                      defaultMessage="Board Templates are available on our Business Plan."
                    />
                  }
                  boldText={""}
                  descriptionTxt={
                    <FormattedMessage
                      id="common.discovered-dialog.list.template.label"
                      defaultMessage={"Upgrade now or start a {TRIALPERIOD}-day free trial to find out all nTask Business features."}
                      values={{TRIALPERIOD: TRIALPERIOD}}
                    />
                  }
                  showBodyImg={true}
                  showDescription={true}
                  imgUrl={template}
                />
              </div>
            </div>{" "}
          </>
        ) : ( 
          <>
         <div className={classes.boardsCnt} style={{ height: calculateContentHeight() }}>
           <Templates
             templates={templates}
             setTemplates={setTemplates}
             boardsTemplates={boardsTemplates}
             setBoardsTemplates={setBoardsTemplates}
             handleTemplatePreview={handleTemplatePreview}
             handleUseTemplate={handleUseTemplate}
           />
         </div>
          </>
        )
      )}
      <Footer quote={quote} open={props.open} total={allProjects.length} display={true} type="Boards" />
    </>
  );
}

BoardGridView.defaultProps = {
  /** default props  */
  classes: {},
  theme: {},
  allProjects: [],
  onSelectGrid: () => {},
  handleTemplatePreview: () => {},
  handleUseTemplate: () => {},
  setView: () => {},
  handleEditBoard: () => {},
  getAllBoards: () => {},
  clearBoardsData: () => {},
  GetAllTemplatesByGroup: () => {},
  // GetTemplates: () => {},
  quote: "",
  // boardsTemplates: [],
  currentView: "boards",
  boardPer: {},
  workspaceTemplates: [],
};

const mapStateToProps = state => {
  return {
    allProjects: state.projects.data,
    tasks: state.tasks.data,
    boardPer: state.workspacePermissions.data.board,
    boards: state.boards,
    workspaceTemplates: state.workspaceTemplates.data.allWSTemplates,
  };
};

export default compose(
  withRouter,
  withSnackbar,
  injectIntl,
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, {
    getAllBoards,
    GetAllTemplatesByGroup,
    clearBoardsData,
    UpdateKanbanBoard
  })
)(BoardGridView);
