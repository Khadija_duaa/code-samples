// @flow

import React, { useState, useEffect } from "react";
import dropdownStyle from "./styles";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import DropdownMenu from "../../components/Dropdown/DropdownMenu";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import RightArrow from "@material-ui/icons/ArrowRight";
import ListItemText from "@material-ui/core/ListItemText";

import CustomIconButton from "../../components/Buttons/IconButton";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { Scrollbars } from "react-custom-scrollbars";
import ColorPicker from "../../components/Dropdown/ColorPicker/ColorPicker";
import { useSelector } from "react-redux";

type DropdownProps = {
  classes: Object,
  theme: Object,
  size: String,
  handleOptionSelect: Function,
  selectedColor: String,
  handleColorSelect: Function,
  permission: Object,
};

//MoreActionDropDown Main Component
function MoreActionDropDown(props: DropdownProps) {
  const {
    classes,
    theme,
    size,
    handleOptionSelect,
    selectedColor,
    handleColorSelect,
    permission,
    color,
    font
  } = props;
  const [anchorEl, setAnchorEl] = useState(null);
  const [colorPicker, setColorPicker] = useState(false);
  const {companyInfo} = useSelector(state => {
    return {
      companyInfo: state.whiteLabelInfo.data,
    };
  });
  const handleClick = (event) => {
    // Function Opens the dropdown
    event.stopPropagation();
    setAnchorEl(state => state ? null : event.currentTarget);
  };
  const handleClose = (event) => {
    // Function closes dropdown
    setAnchorEl(null);
    setColorPicker(false);
  };
  const handleItemClick = (event, option) => {
    //Fuction responsible for selecting item from the list
    handleOptionSelect(option); // Callback on item select used to lift up option to parent if needed

    if (option == "color") {
      /** if option is color then drop down will be open and color control will open */
      setColorPicker(true);
    } else {
      setAnchorEl(null);
      setColorPicker(false);
    }
  };
  const onColorChange = (color) => {
    /** function calls when user change color */
    handleColorSelect(color);
    setColorPicker(false);
  };
  const open = Boolean(anchorEl);
  const taskLabelSingle = companyInfo?.task?.sName;
  const taskLabelMulti = companyInfo?.task?.pName;
  return (
    <>
      <CustomIconButton
        btnType="condensed"
        style={{ padding: 0 }}
        onClick={handleClick}
        buttonRef={anchorEl}
      >
        <MoreVerticalIcon
          htmlColor={color}
          style={ font }
        />
      </CustomIconButton>
      <DropdownMenu
        open={open}
        closeAction={handleClose}
        anchorEl={anchorEl}
        size={size}
        placement="bottom-end"
      >
        {/* <Scrollbars autoHide autoHeight autoHeightMin={0} autoHeightMax={130}> */}
        <List>
          {false && (
            <ListItem
              button
              className={classes.listItem}
              onClick={(event) => {
                // handleItemClick(event, "publickLink");
              }}
            >
              <span>Public Link</span>
            </ListItem>
          )}
          {true && (
            <ListItem
              button
              className={classes.listItemColor}
              onClick={(event) => {
                // handleItemClick(event, "color");
              }}
            >
              <span>Color</span>
              <RightArrow htmlColor={theme.palette.secondary.light} />
              {/* <div
                className={classes.colorPicker}
                style={{ display: colorPicker ? "block" : "none" }}
              >
                <ColorPicker
                  triangle="hide"
                  onColorChange={onColorChange}
                  selectedColor={selectedColor}
                />
              </div> */}
            </ListItem>
          )}
          {true && (
            <ListItem
              button
              className={classes.listItem}
              onClick={(event) => {
                // handleItemClick(event, "copyTask");
              }}
            >
              <span>Copy {taskLabelSingle ? taskLabelSingle : 'Task'}</span>
            </ListItem>
          )}
          {true && (
            <ListItem
              button
              className={classes.listItem}
              onClick={(event) => {
                // handleItemClick(event, "archiveTask");
              }}
            >
              <span>Archive {taskLabelSingle ? taskLabelSingle : 'Task'}</span>
            </ListItem>
          )}
          {true && (
            <ListItem
              button
              className={classes.listItem}
              onClick={(event) => {
                // handleItemClick(event, "unlinkTask");
              }}
            >
              <span>Unlink {taskLabelSingle ? taskLabelSingle : 'Task'}</span>
            </ListItem>
          )}
          {true && (
            <ListItem
              button
              className={classes.listItem}
              onClick={(event) => {
                // handleItemClick(event, "deleteTask");
              }}
            >
              <span className={classes.deleteTxt}>Delete {taskLabelSingle ? taskLabelSingle : 'Task'}</span>
            </ListItem>
          )}
        </List>
        {/* </Scrollbars> */}
      </DropdownMenu>
    </>
  );
}
MoreActionDropDown.defaultProps = {
  classes: {},
  theme: {},
  size: "small",
  handleOptionSelect: () => {},
  selectedColor: "",
  handleColorSelect: () => {},
  permission: {},
};
export default compose(
  withRouter,
  withStyles(dropdownStyle, { withTheme: true })
)(MoreActionDropDown);
