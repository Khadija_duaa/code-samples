const getRisksPermissions = (riskData, props) => {
  const permission = riskData
    ? !riskData.isArchive
      ? riskData.isOwner
        ? true
        : props.workspacePermissionsState.data &&
          props.workspacePermissionsState.data.risk
        ? props.workspacePermissionsState.data.risk
        : false
      : false
    : false;
  return permission;
};

const getRisksPermissionsWithoutArchieve = (riskData, props) => {
  const permission = riskData
    ? riskData.isOwner
      ? true
      : props.workspacePermissionsState.data && props.workspacePermissionsState.data.risk
      ? props.workspacePermissionsState.data.risk
      : false
    : false;
  return permission;
};

const getRisksEditPermissionsWithArchieve = (riskData, permission, key) => {
  if (permission && Object.keys(permission).length && riskData && key) {
    return permission[key] && (permission[key].edit || permission[key].add) ? true : false;
  }
  return permission;
};

const getCompleteRisksPermissionsWithArchieve = (riskData, permission, key) => {
  if (permission && Object.keys(permission).length && riskData && key) {
    return permission[key];
  }
  return permission;
};

export function canDelete(riskData, permissions, key){
  return riskData.isOwner ? true : permissions[key].delete
  }
export {
  getRisksEditPermissionsWithArchieve,
  getRisksPermissions,
  getRisksPermissionsWithoutArchieve,
  getCompleteRisksPermissionsWithArchieve
};
