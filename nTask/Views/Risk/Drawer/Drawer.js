import React, { Component } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Drawer from "@material-ui/core/Drawer";
import { withStyles } from "@material-ui/core/styles";
import { Scrollbars } from "react-custom-scrollbars";
import ReactResizeDetector from "react-resize-detector";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import { createNextOccurrence, deleteTaskSchedule } from "../../../redux/actions/tasks";
import moment from "moment";
import Typography from "@material-ui/core/Typography";
import CustomIconButton from "../../../components/Buttons/CustomIconButton";
import SvgIcon from "@material-ui/core/SvgIcon";
import RecurrenceIcon from "../../../components/Icons/RecurrenceIcon";
import CloseIcon from "@material-ui/icons/Close";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import headerHeight from "../../../helper/getHeaderHeight";
import getInfoMessage from "../RepeatTask/getInfoMessage";
import {
  calculateFilterContentHeight,
  calculateHeaderHeight
} from "../../../utils/common";
import { withSnackbar } from "notistack";
import { canAdd } from "../permissions";
import MoreActionDropdown from "./MoreActionDropdown";
import DeleteConfirmDialog from "../../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import DefaultDialog from "../../../components/Dialog/Dialog";
import RepeatTask from "../RepeatTask/RepeatTask";
import {FormattedMessage} from "react-intl";
import ProjectPermissionSelector from "../../../redux/selectors/projectPermissionSelector";


const drawerWidth = 350;

const styles = theme => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0
  },
  drawerPaper: {
    width: drawerWidth,
    padding: 0,
    background: theme.palette.background.paper,
    position: "fixed",
    top: headerHeight,
    display: "flex",
    height: "100%",
    zIndex: 100,
    flexDirection: "row",
    justifyContent: "stretch",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    overflowX: "hidden",
    borderTop: 0,
    borderBottom: 0
  },
  drawerHeader: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between"
  },
  taskScheduleTilesCnt: {
    padding: 15
  },
  filterFormCnt: {
    padding: "10px 20px 40px 20px"
  },
  switchCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    paddingLeft: 12
  },
  switchLabel: {
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    margin: 0,
    marginRight: 30,
    fontWeight: theme.typography.fontWeightRegular,
    display: "flex"
  },
  filterBottomBtns: {
    padding: "10px 20px",
    borderTop: `1px solid ${theme.palette.border.lightBorder}`,
    marginTop: 40,
    position: "fixed",
    bottom: 178,
    background: theme.palette.common.white,
    zIndex: 1
  },
  filterItem: {
    // marginTop: 20
  },
  filterCntHeader: {
    background: "#404040",
    padding: "12px 15px",
    position: "fixed",
    width: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between"
  },
  filterHeaderClearBtn: {
    textDecoration: "underline",
    color: theme.palette.common.white,
    fontSize: "12px !important",
    cursor: "pointer"
  },
  filterHeaderTextCnt: {
    display: "flex",
    alignItems: "center"
  },
  filtersHeaderText: {
    color: theme.palette.text.medLight,
    fontWeight: theme.typography.fontWeightLight,
    fontSize: "16px !important"
  },
  filtersHeaderCloseIcon: {
    fontSize: "14px !important",
    color: "#404040"
  },
  filtersHeaderHelpIcon: {
    fontSize: "14px !important",
    marginLeft: 5
  },
  recurrenceIcon: {
    fontSize: "18px !important"
  },
  taskScheduleTile: {
    border: `1px solid ${theme.palette.border.extraLightBorder}`,
    background: theme.palette.common.white,
    padding: "10px 15px",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 10,
    borderRadius: 4
  },
  dateCnt: {
    background: theme.palette.background.items,
    padding: 10,
    marginRight: 10,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    borderRadius: 4
  },
  dateTimeCnt: {
    display: "flex",
    alignItems: "center",
    maxWidth: 'calc(100% - 34px)',
  },
  occurenceDetails: {
    width: '80%',
  },
  occurenceTitle: {
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
  },
  drawerHeadingCnt: {
    display: "flex",
    marginLeft: 10,
    flexDirection: 'column'
  },
  drawerHeadingCaption: {
    fontSize: "10px !important",
    width: 270,
    color: theme.palette.text.medLight,
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    lineHeight: "normal",
  }
});
class SideDrawer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterCntHeight: `${calculateFilterContentHeight()}px`,
      openRecurrenceConfirm: false,
      deleteScheduleModal: false,
      editSchedule: false
    };
  }

  onResize = () => {
    this.setState({ filterCntHeight: `${calculateFilterContentHeight()}px` });
  };
  //Close action of dialog
  handleDialogClose = name => {
    this.setState({ openRecurrenceConfirm: false, createReccurrenceDate: "" });
  };
  handleCreateReccureTask = () => {
    const { createReccurrenceDate } = this.state;
    const { repeatTaskId, closeAction } = this.props;
    const data = { TaskId: repeatTaskId, date: moment(createReccurrenceDate).format("MM/DD/YYYY hh:mm:ss A") };
    this.setState({ btnQuery: "progress" });
    //Create Next task occurence
    this.props.createNextOccurrence(
      data,
      //Success
      (task) => {
        this.setState({ btnQuery: "" });
        this.handleDialogClose();
        if (task.repeatDates == null) { closeAction(); }
      },
      //Failure
      error => {
        this.setState({ btnQuery: "" });
        this.showSnackBar(error.data.message, "error");
      }
    );
  };
  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right"
        },
        variant: type ? type : "info"
      }
    );
  };
  handleDialogOpen = (event, name, date) => {
    this.setState({ [name]: true, createReccurrenceDate: date });
  };
  openDeleteSceheduleModal = () => {
    this.setState({ deleteScheduleModal: true })
  }
  closeDeleteSceheduleModal = () => {
    this.setState({ deleteScheduleModal: false })
  }
  handleDeleteSchedule = () => {
    /** function calls when user delete schedule  */
    const { tasks, repeatTaskId } = this.props;
    const activeTask = tasks.find(t => t.taskId == repeatTaskId);
    this.props.deleteTaskSchedule(
      activeTask.taskId,
      success => {
        this.closeDeleteSceheduleModal();
        this.props.closeAction();
      },
      err => { }
    );
  }
  openEditSceheduleModal = () => {
    this.setState({ editSchedule: true })
  }
  closeEditSceheduleModal = () => {
    this.setState({ editSchedule: false })
  }
  render() {
    const {
      classes,
      theme,
      open,
      closeAction,
      tasks,
      repeatTaskId,
      taskPer,
      allTasks,
      permissionObject
    } = this.props;
    const { deleteScheduleModal, editSchedule } = this.state;
    const activeTask = allTasks.find(t => t.taskId == repeatTaskId);
    const taskPermission =
    activeTask &&
    activeTask.projectId /** if task link with project then assign project permissions */
      ? permissionObject[activeTask.projectId].task
      : taskPer; /** workspace level permissions */
    const { filterCntHeight, openRecurrenceConfirm, btnQuery } = this.state;
    const repeatTaskType = activeTask.repeatedTaskType.toLowerCase();
    const selectedPeriod = repeatTaskType == "daily"
      ? "left"
      : repeatTaskType == "weekly"
        ? "center"
        : "right";
    const repeatTaskInfoMsg = getInfoMessage({ repeatTypeTab: selectedPeriod, [repeatTaskType]: { repeatDetails: activeTask.repeatTask } })
    const repeatDatesArr = activeTask.repeatDates ? activeTask.repeatDates : [];
    return (
      <>
        {editSchedule && (
          <DefaultDialog
            title={<FormattedMessage id="task.drawer.task-schedule.edit-schedule.title" defaultMessage="Edit Task Schedule"/>}
            sucessBtnText=""
            open={editSchedule}
            onClose={this.closeEditSceheduleModal}
          >
            <RepeatTask
              closeAction={this.closeEditSceheduleModal}
              task={activeTask}
              label={false}
              taskPer={taskPermission}
            />
          </DefaultDialog>
        )}
        {deleteScheduleModal && (<DeleteConfirmDialog
          open={deleteScheduleModal}
          closeAction={this.closeDeleteSceheduleModal}
          cancelBtnText={<FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel"/>}
          successBtnText={<FormattedMessage id="common.action.delete.label" defaultMessage="Delete"/>}
          alignment="center"
          headingText={<FormattedMessage id="task.drawer.task-schedule.delete-schedule.confirmation.title" defaultMessage="Delete"/>}
          successAction={this.handleDeleteSchedule}
          msgText={<FormattedMessage id="task.drawer.task-schedule.delete-schedule.confirmation.label" defaultMessage="Do you really want to delete all upcoming task schedule?"/>}
          btnQuery={btnQuery}
        />)}
        {openRecurrenceConfirm && (
          <ActionConfirmation
            open={openRecurrenceConfirm}
            closeAction={this.handleDialogClose}
            cancelBtnText={<FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel"/>}
            successBtnText={<FormattedMessage id="task.drawer.task-schedule.copy-of-copy.confirmation.yes-button.label" defaultMessage="Yes, Create Next Occurrence"/>}
            alignment="center"
            headingText={<FormattedMessage id="task.drawer.task-schedule.copy-of-copy.confirmation.title" defaultMessage="Create Next Occurrence"/>}
            iconType="recurrence"
            msgText={
              <>
              <FormattedMessage id="task.drawer.task-schedule.copy-of-copy.confirmation.label" values={{title: activeTask.taskTitle, t1: <br />, t2: <br />}} defaultMessage={`This task is part of "${activeTask.taskTitle}" schedule and does
                not exist yet.
                ${<br />}
                ${<br />}
                Would you like to create next occurrence?`}/>

              </>
            }
            successAction={this.handleCreateReccureTask}
            btnQuery={btnQuery}
          />
        )}
        {/* <ClickAwayListener onClickAway={closeAction}> */}

        <Drawer
          className={classes.drawer}
          variant="persistent"
          anchor="right"
          open={open}
          PaperProps={{ style: { top: calculateHeaderHeight() } }}
          classes={{
            paper: classes.drawerPaper
          }}
        >
          <ReactResizeDetector
            handleWidth
            handleHeight
            onResize={this.onResize}
          />
          <div className={classes.drawerHeader}>
            <div className={classes.filterCntHeader}>
              <div className={classes.filterHeaderTextCnt}>
                <CustomIconButton onClick={closeAction} btnType="whiteround">
                  <CloseIcon
                    classes={{ root: classes.filtersHeaderCloseIcon }}
                    htmlColor={theme.palette.secondary.medDark}
                  />
                </CustomIconButton>
                <div className={classes.drawerHeadingCnt}>
                  <Typography
                    variant="h3"
                    classes={{ h3: classes.filtersHeaderText }}
                  >
                   <FormattedMessage id="task.drawer.task-schedule.title" defaultMessage="Task Schedule"/> 
                </Typography>
                  <span title={repeatTaskInfoMsg} className={classes.drawerHeadingCaption}>{repeatTaskInfoMsg}</span>
                </div>
                {taskPermission.taskDetail.repeatTask.isAllowEdit || taskPermission.taskDetail.repeatTask.isAllowDelete ?
                  (<MoreActionDropdown
                    editSchedule={this.openEditSceheduleModal}
                    deleteSchedule={this.openDeleteSceheduleModal}
                    taskPer={taskPermission}
                  />):null}
              </div>
            </div>
            <Scrollbars
              autoHide
              style={{ width: 349, marginTop: 60, height: filterCntHeight }}
            >
              <div className={classes.taskScheduleTilesCnt}>
                {repeatDatesArr.map(d => {
                  const day = moment(d)
                    .format("DD")
                    .toUpperCase();
                  const month = moment(d)
                    .format("MMM")
                    .toUpperCase();
                  const dayName = moment(d).format("dddd");
                  const time = moment(d).format("hh:mm a");

                  return (
                    <div className={classes.taskScheduleTile}>
                      <div className={classes.dateTimeCnt}>
                        <div className={classes.dateCnt}>
                          <Typography variant="h3">{day}</Typography>
                          <Typography variant="caption">{month}</Typography>
                        </div>
                        <div className={classes.occurenceDetails}>
                          <Typography className={classes.occurenceTitle} variant="h5">
                            {activeTask.taskTitle}
                          </Typography>
                          <Typography variant="caption">
                            {dayName} at {time}
                          </Typography>
                        </div>
                      </div>
                      {taskPermission.taskDetail.repeatTaskOccurances.cando && (<CustomIconButton
                        onClick={event => {
                          this.handleDialogOpen(
                            event,
                            "openRecurrenceConfirm",
                            d
                          );
                        }}
                        btnType="whiteround"
                        style={{ width: 34, height: 34 }}
                      >
                        <SvgIcon
                          viewBox="0 0 14 12.438"
                          htmlColor={theme.palette.primary.light}
                          className={classes.recurrenceIcon}
                        >
                          <RecurrenceIcon />
                        </SvgIcon>
                      </CustomIconButton>)}
                    </div>
                  );
                })}
              </div>
            </Scrollbars>
          </div>
        </Drawer>
        {/* </ClickAwayListener> */}
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    profileState: state.profile,
    members: state.profile.data ? state.profile.data.member.allMembers : [],
    appliedFiltersState: state.appliedFilters,
    itemOrderState: state.itemOrder,
    tasks: state.tasks.data,
    projects: state.projects.data,
    permissions: state.workspacePermissions.data.task,
    taskPer: state.workspacePermissions.data.task,
    permissionObject: ProjectPermissionSelector(state),
  };
};

export default compose(
  withRouter,
  withSnackbar,
  withStyles(styles, {
    withTheme: true
  }),
  connect(mapStateToProps, {
    createNextOccurrence,
    deleteTaskSchedule
  })
)(SideDrawer);
