import React, { Component } from "react";
import CustomDialog from "../../../components/Dialog/CustomDialog";
import repeatTaskStyles from "./styles";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import ButtonActionsCnt from "../../../components/Dialog/ConfirmationDialogs/ButtonActionsCnt";
import Typography from "@material-ui/core/Typography";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import Daily from "./Daily";
import Weekly from "./Weekly";
import Monthly from "./Monthly";
import DefaultCheckbox from "../../../components/Form/Checkbox";
import { repeatTaskInitObj } from "./repeatTaskObj";
import cloneDeep from "lodash/cloneDeep";
import { repeatTask, deleteTaskSchedule } from "../../../redux/actions/tasks";
import NotificationMessage from "../../../components/NotificationMessages/NotificationMessages";
import moment from "moment";
import getInfoMessage from "./getInfoMessage";
import { withSnackbar } from "notistack";
import { debug } from "util";
import { FormattedMessage } from "react-intl";

class RepeatTask extends Component {
  constructor(props) {
    super(props);
    this.state = {
      repeatTypeTab: "left",
      weekendTaskTab: "left",
      weekEndTaskCheck: false,
      daily: repeatTaskInitObj.daily,
      weekly: repeatTaskInitObj.weekly,
      monthly: repeatTaskInitObj.monthly,
      btnQueryDelete: "",
    };
  }
  componentDidMount() {
    const { task } = this.props;
    const { daily, weekly, monthly } = this.state;

    if (task.repeatTask) {
      //Pre filling form values, if repeat Task already exist, especially selected tabs
      const repeatTaskType = task.repeatTask.repeatType;
      const repeatTypeTab =
        repeatTaskType == "Daily" ? "left" : repeatTaskType == "Weekly" ? "center" : "right";
      const weekendOptionSelected =
        repeatTaskType == "Daily"
          ? task.repeatTask.repeatDaily.weekendTask
          : repeatTaskType == "Monthly"
          ? task.repeatTask.repeatMonthly.weekendTask
          : "";
      const weekendTaskTab =
        weekendOptionSelected == "3" ? "left" : weekendOptionSelected == "2" ? "center" : "right";
      const weekEndTaskCheck = weekendOptionSelected ? true : false;
      this.setState({ weekendTaskTab, repeatTypeTab, weekEndTaskCheck });
      // Conditions applied to set repeat details object according to selected tab
      if (repeatTaskType == "Daily") {
        //Daily
        this.setState({ daily: { ...daily, repeatDetails: task.repeatTask } });
      } else if (repeatTaskType == "Weekly") {
        // Weekly
        this.setState({
          weekly: { ...weekly, repeatDetails: task.repeatTask },
        });
      } else {
        // Monthly
        this.setState({
          monthly: { ...monthly, repeatDetails: task.repeatTask },
        });
      }
    }
  }

  handleWeekendTaskChange = event => {
    const { weekEndTaskCheck, repeatTypeTab } = this.state;
    // function to toggle checked and unchecked state of checkbox
    this.setState({ weekEndTaskCheck: !weekEndTaskCheck });
    if (!weekEndTaskCheck) {
      // Incase the weekend checkbox is unchecked the weekendTask Value in state will be empty
      if (repeatTypeTab == "right") {
        this.updateDailyRepeatDetails("repeatMonthly", "weekendTask", "3");
      } else {
        this.updateDailyRepeatDetails("repeatDaily", "weekendTask", "3");
      }
    } else {
      if (repeatTypeTab == "right") {
        this.updateDailyRepeatDetails("repeatMonthly", "weekendTask", "");
      } else {
        this.updateDailyRepeatDetails("repeatDaily", "weekendTask", "");
      }
    }
  };
  handleTabSelect = (event, tab) => {
    if (tab) {
      this.setState({ repeatTypeTab: tab });
    }
  };
  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  handleWeekendTaskTabSelect = (event, tab) => {
    const { repeatTypeTab } = this.state;
    if (tab) {
      //Setting the ENUMS based on the tab selected
      let selectedWeekendValue = tab == "left" ? "3" : tab == "center" ? "2" : "1";
      this.setState({ weekendTaskTab: tab });
      if (repeatTypeTab == "right") {
        this.updateDailyRepeatDetails("repeatMonthly", "weekendTask", selectedWeekendValue);
      } else {
        this.updateDailyRepeatDetails("repeatDaily", "weekendTask", selectedWeekendValue);
      }
    }
  };

  //Updating Repeat Type in repeat task object
  updateRepeatType = () => {};
  //Updating RepeatDetails
  updateDailyRepeatDetails = (key, type, value) => {
    const { repeatTypeTab } = this.state;
    const selectedPeriod = // Saving reference to object depending on the selected Tab
      repeatTypeTab == "left" ? "daily" : repeatTypeTab == "center" ? "weekly" : "monthly";

    let clonedObj = cloneDeep(this.state[selectedPeriod]); // Cloning Object

    if (type == "root") {
      clonedObj["repeatDetails"][key] = value; // Updating Value on clone object
      this.setState({ [selectedPeriod]: clonedObj }); // Updating state with the new object of daily repeat meeting
    } else {
      clonedObj["repeatDetails"][key][type] = value; // Updating Value on clone object
      this.setState({ [selectedPeriod]: clonedObj }); // Updating state with the new object of daily repeat meeting
    }
  };
  handleSubmitRepeatTask = () => {
    const { repeatTypeTab } = this.state;
    const { task, returnUpdatedTask } = this.props;
    const updateRepeatTask = task.repeatTask ? true : false;
    this.setState({ btnQuery: "progress" });
    const selectedPeriod = // Saving reference to object depending on the selected Tab
      repeatTypeTab == "left" ? "daily" : repeatTypeTab == "center" ? "weekly" : "monthly";
    const postObj = { ...this.state[selectedPeriod], taskId: task.taskId };
    this.props.repeatTask(
      postObj,
      //success
      res => {
        this.setState({ btnQuery: "" });
        this.props.closeAction();
        if (returnUpdatedTask) returnUpdatedTask(res);
      },
      //failure
      error => {
        this.showSnackBar(error.data.message, "error");
        this.setState({ btnQuery: "" });
      },
      updateRepeatTask
    );
  };

  deleteTaskSchedule = () => {
    /** function calls when user delete schedule  */
    const { task, returnUpdatedTask } = this.props;
    this.setState({ btnQueryDelete: "progress" }, () => {
      this.props.deleteTaskSchedule(
        task.taskId,
        success => {
          this.setState({btnQueryDelete:""})
          this.props.closeAction();
          if (returnUpdatedTask) returnUpdatedTask(success);
        },
        err => {
          this.setState({btnQueryDelete:""})
        }
      );
    });
  };
  render() {
    const { classes, theme, open, closeAction, task, label, taskPer } = this.props;
    const {
      btnQuery,
      alignment,
      weekEndTaskCheck,
      repeatTypeTab,
      weekendTaskTab,
      daily,
      weekly,
      monthly,
      btnQueryDelete
    } = this.state;

    return (
      <div
        onClick={event => {
          event.stopPropagation();
        }}>
        {/* // <DetailsDialog
      //   title="Repeat Task"
      //   dialogProps={{
      //     open: open,
      //     onClick: e => {
      //       e.stopPropagation();
      //     },
      //     onClose: closeAction,
      //     PaperProps: {
      //       style: { maxWidth: 580 }
      //     }
      //   }}
      // > */}
        <div className={classes.topGrayBar}>
          <Typography variant="body1" className={classes.repeatTaskHeading}>
            {label ? (
              <FormattedMessage
                id="task.detail-dialog.repeat-task.title"
                defaultMessage="Repeat Task"
              />
            ) : (
              ""
            )}
          </Typography>
          <div className={classes.toggleContainer}>
            <ToggleButtonGroup
              value={repeatTypeTab}
              exclusive
              onChange={this.handleTabSelect}
              classes={{ root: classes.toggleBtnGroup, groupedHorizontal: classes.groupedHorizontal }}>
              <ToggleButton
                value="left"
                classes={{
                  root: classes.toggleButton,
                  selected: classes.toggleButtonSelected,
                }}>
                <FormattedMessage
                  id="task.detail-dialog.repeat-task.daily.label"
                  defaultMessage="Daily"
                />
              </ToggleButton>
              <ToggleButton
                value="center"
                classes={{
                  root: classes.toggleButton,
                  selected: classes.toggleButtonSelected,
                }}>
                <FormattedMessage
                  id="task.detail-dialog.repeat-task.weekly.label"
                  defaultMessage="Weekly"
                />
              </ToggleButton>

              <ToggleButton
                value="right"
                classes={{
                  root: classes.toggleButton,
                  selected: classes.toggleButtonSelected,
                }}>
                <FormattedMessage
                  id="task.detail-dialog.repeat-task.monthly.label"
                  defaultMessage="Monthly"
                />
              </ToggleButton>
            </ToggleButtonGroup>
          </div>
        </div>
        <div className={classes.contentCnt}>
          {repeatTypeTab == "left" ? (
            <Daily data={daily} updateRepeatData={this.updateDailyRepeatDetails} task={task} />
          ) : repeatTypeTab == "center" ? (
            <Weekly data={weekly} updateRepeatData={this.updateDailyRepeatDetails} task={task} />
          ) : (
            <Monthly data={monthly} updateRepeatData={this.updateDailyRepeatDetails} task={task} />
          )}
          {/* Weekend Check Container */}

          <div className={classes.weekendCheckCnt}>
            {(repeatTypeTab == "left" ||
              (repeatTypeTab == "right" &&
                monthly.repeatDetails.repeatMonthly.timePeriod == "Day")) && (
              <>
                <DefaultCheckbox
                  checked={weekEndTaskCheck}
                  onChange={this.handleWeekendTaskChange}
                  label={
                    <FormattedMessage
                      id="task.detail-dialog.repeat-task.common.create-week-task.label"
                      defaultMessage="Create weekend task"></FormattedMessage>
                  }
                  styles={{ display: "flex", alignItems: "center" }}
                  checkboxStyles={{ paddingLeft: 0 }}
                />
                {/* Weekend Task Toggle Buttons */}
                {weekEndTaskCheck && (
                  <div className={classes.toggleContainer}>
                    <ToggleButtonGroup
                      value={weekendTaskTab}
                      exclusive
                      onChange={this.handleWeekendTaskTabSelect}
                      classes={{ root: classes.toggleBtnGroup, groupedHorizontal: classes.groupedHorizontal }}>
                      <ToggleButton
                        value="left"
                        //   onClick={this.renderListView}
                        classes={{
                          root: classes.toggleButton,
                          selected: classes.toggleButtonSelected,
                        }}>
                        <FormattedMessage
                          id="task.detail-dialog.repeat-task.common.before-week.label"
                          defaultMessage="Before weekend"></FormattedMessage>
                      </ToggleButton>
                      <ToggleButton
                        value="center"
                        //   onClick={this.renderGridView}
                        classes={{
                          root: classes.toggleButton,
                          selected: classes.toggleButtonSelected,
                        }}>
                        <FormattedMessage
                          id="task.detail-dialog.repeat-task.common.on-weekend.label"
                          defaultMessage="On weekend"></FormattedMessage>
                      </ToggleButton>

                      <ToggleButton
                        value="right"
                        //   onClick={this.renderCalendarView}
                        classes={{
                          root: classes.toggleButton,
                          selected: classes.toggleButtonSelected,
                        }}>
                        <FormattedMessage
                          id="task.detail-dialog.repeat-task.common.after-weekend.label"
                          defaultMessage="After weekend"></FormattedMessage>
                      </ToggleButton>
                    </ToggleButtonGroup>
                  </div>
                )}
              </>
            )}
          </div>

          <NotificationMessage type="info" iconType="info" style={{ marginTop: 10 }}>
            {getInfoMessage(this.state)}
          </NotificationMessage>
        </div>
        <ButtonActionsCnt
          // cancelAction={closeAction}
          successAction={this.handleSubmitRepeatTask}
          successBtnText={
            task.repeatTask ? (
              <FormattedMessage
                id="task.detail-dialog.repeat-task.save-button.update.label"
                defaultMessage="Update"
              />
            ) : (
              <FormattedMessage
                id="task.detail-dialog.repeat-task.save-button.save.label"
                defaultMessage="Repeat Task"
              />
            )
          }
          deleteBtnText={
            task.repeatTask ? (
              <FormattedMessage
                id="task.detail-dialog.repeat-task.delete-button.label"
                defaultMessage="Delete Schedule"
              />
            ) : (
              ""
            )
          }
          deleteAction={this.deleteTaskSchedule}
          // cancelBtnText="Discard Changes"
          btnType="success"
          btnQuery={btnQuery}
          disabled={
            task.repeatTask == null
              ? false
              : taskPer.taskDetail.repeatTask.isAllowEdit
              ? false
              : true
          }
          deletePer={!taskPer.taskDetail.repeatTask.isAllowDelete}
          btnQueryDelete={btnQueryDelete}
        />
      </div>
    );
  }
}

export default compose(
  withSnackbar,
  withStyles(repeatTaskStyles, { withTheme: true }),
  connect(state => state, {
    repeatTask,
    deleteTaskSchedule,
  })
)(RepeatTask);
