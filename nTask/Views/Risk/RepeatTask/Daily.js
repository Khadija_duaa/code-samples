import React, { Component } from "react";
import { compose } from "redux";
import TimeSelect from "../../../components/TimePicker/TimeSelect";
import SelectSearchDropdown from "../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { generateRepeatStopByData } from "../../../helper/generateSelectData";
import InputLabel from "@material-ui/core/InputLabel";
import repeatTaskStyles from "./styles";
import { withStyles } from "@material-ui/core/styles";
import StaticDatePicker from "../../../components/DatePicker/StaticDatePicker";
import DefaultTextField from "../../../components//Form/TextField";
import moment from "moment";
import { FormattedMessage, injectIntl } from "react-intl";

class Daily extends Component {
  constructor(props) {
    super(props);
    this.state = {
      stopBy: this.generateRepeatStopByData()[0],
      afterTaskCount: 5,
      repeatInterval: 1,
      date: moment(),
      hours: "12",
      minutes: "00",
      timeFormat: "AM",
    };
  }
  generateRepeatStopByData() {
    let ddData = [
      {
        value: "Date",
        label: this.props.intl.formatMessage({
          id: "common.date.label",
          defaultMessage: "Date",
        }),
      },
      {
        value: "After",
        label: this.props.intl.formatMessage({
          id: "common.after.label",
          defaultMessage: "After",
        }),
      },
    ];
    return ddData;
  }
  updateTime = (hours, minutes, timeFormat) => {
    const timeObj = { hours: hours, minutes, timeFormat };
    this.setState({ hours, minutes, timeFormat });
    this.props.updateRepeatData("repeatAt", "root", timeObj);
  };
  clearSelect = (key, value) => {
    this.setState({ [key]: value });
  };
  //Local state is update for dropdown so object can be made to send update to backend with API
  //Updating local state when option is selected
  handleOptionsSelect = (type, option) => {
    this.setState({ [type]: option }, () => {
      this.props.updateRepeatData("stopBy", "type", option.value);
    });
  };

  //Saving date to local so later on can be used to send to backend
  updateDate = date => {
    this.setState({
      date,
    });
    this.props.updateRepeatData("stopBy", "date", date.format("MM/DD/YYYY"));
  };
  //Function handles on change of number field of stopBy Task Count
  handleInputChange = (event, name) => {
    let value = event.target.value;
    //Update value only if value is greater than 0 and less than 1000
    switch (name) {
      case "afterTaskCount":
        if (value > 0 && value < 1000 && value.indexOf(".") == -1) {
          this.setState({ [name]: value });
          this.props.updateRepeatData("stopBy", "value", value);
        }
        break;
      case "repeatInterval":
        if (value > 0 && value < 1000 && value.indexOf(".") == -1) {
          this.setState({ [name]: value });
          this.props.updateRepeatData("repeatDaily", "interval", value);
        }

      default:
        break;
    }
  };
  componentDidMount() {
    const { task } = this.props;
    // Checking the repeat Type of the task if it already exist on daily than prefill the values of input fields accordingly
    if (task.repeatedTaskType == "Daily") {
      const stopByValue = this.generateRepeatStopByData().find(
        s => s.value == task.repeatTask.stopBy.type
      );
      const afterTaskCount = task.repeatTask.stopBy.value;
      const date = moment(task.repeatTask.stopBy.date);
      const interval = task.repeatTask.repeatDaily.interval;
      const hours = task.repeatTask.repeatAt.hours;
      const minutes = task.repeatTask.repeatAt.minutes;
      const timeFormat = task.repeatTask.repeatAt.timeFormat;
      this.setState({
        stopBy: stopByValue,
        afterTaskCount: afterTaskCount ? afterTaskCount : "5",
        repeatInterval: interval,
        date,
        hours,
        minutes,
        timeFormat,
      });
    }
  }

  // componentDidUpdate = (prevProps) => {
  //   const { task } = this.props;
  //   if (JSON.stringify(prevProps.task) !== JSON.stringify(task)) { /** checking if the prev task data is not equal to this.props task data */
  //     if (task.repeatTask == null) { /** if repeatTask is null / user delete schedule then setting the default values  */
  //       this.setState({
  //         stopBy: this.generateRepeatStopByData()[0],
  //         afterTaskCount: 5,
  //         repeatInterval: 1,
  //         date: moment()
  //       })
  //     }
  //   }
  // }

  render() {
    const { stopBy, afterTaskCount, repeatInterval, date, hours, minutes, timeFormat } = this.state;
    const { classes, theme } = this.props;
    return (
      <>
        <div className={classes.intervalCnt}>
          <InputLabel
            classes={{
              root: classes.selectLabel,
            }}
            shrink={false}>
            <FormattedMessage
              id="task.detail-dialog.repeat-task.daily.every.label"
              defaultMessage="Every:"></FormattedMessage>
          </InputLabel>
          <DefaultTextField
            label={false}
            fullWidth={true}
            error={false}
            errorState={false}
            formControlStyles={{ marginBottom: 0, width: 80 }}
            defaultProps={{
              id: "repeatInterval",
              type: "number",
              onChange: e => this.handleInputChange(e, "repeatInterval"),

              value: repeatInterval,
              inputProps: {
                style: { paddingTop: 11, paddingBottom: 11 },
              },
            }}
          />
          <InputLabel
            classes={{
              root: classes.inputCenterLabel,
            }}
            shrink={false}>
            <FormattedMessage
              id="task.detail-dialog.repeat-task.daily.day.label"
              defaultMessage="day(s)"></FormattedMessage>
          </InputLabel>
        </div>
        <TimeSelect
          clearSelect={this.clearSelect}
          updateTime={this.updateTime}
          label={
            <FormattedMessage
              id="task.detail-dialog.repeat-task.common.repeat-at.label"
              defaultMessage="Repeat at:"></FormattedMessage>
          }
          hours={hours}
          mins={minutes}
          timeFormat={timeFormat}
        />
        {/* StopBy Container */}
        <div className={classes.stopByCnt}>
          <InputLabel
            classes={{
              root: classes.selectLabel,
            }}
            shrink={false}>
            <FormattedMessage
              id="task.detail-dialog.repeat-task.common.stop-by.label"
              defaultMessage="Stop by:"></FormattedMessage>
          </InputLabel>
          {/* Stop By Dropdown  */}
          <SelectSearchDropdown
            data={() => {
              return this.generateRepeatStopByData();
            }}
            isClearable={false}
            label=""
            selectChange={this.handleOptionsSelect}
            selectClear={this.handleClearSelect}
            styles={{ flex: 1, marginRight: 10, marginTop: 0, marginBottom: 0 }}
            type="stopBy"
            selectedValue={stopBy}
            placeholder={"Stop By"}
            isMulti={false}
          />
          {/* Date picker */}
          {stopBy.value == "Date" ? (
            <StaticDatePicker
              label=""
              placeholder={this.props.intl.formatMessage({
                id: "common.date.placeholder",
                defaultMessage: "Select Date",
              })}
              isInput={true}
              selectedSaveDate={this.updateDate}
              date={date}
              isCreation={true}
              style={{ width: 170 }}
              formControlStyles={{ margin: 0 }}
              margin={0}
              minDate={new Date()}
            />
          ) : (
            <DefaultTextField
              label={false}
              fullWidth={true}
              error={false}
              errorState={false}
              formControlStyles={{ marginBottom: 0, width: 80 }}
              defaultProps={{
                id: "afterTaskCount",
                type: "number",
                onChange: e => this.handleInputChange(e, "afterTaskCount"),
                value: afterTaskCount,
              }}
            />
          )}
        </div>
      </>
    );
  }
}

export default compose(withStyles(repeatTaskStyles, { withTheme: true }), injectIntl)(Daily);
