import React, { Component } from "react";
import { Editor } from "react-draft-wysiwyg";
import "../../../assets/css/richTextEditor.css";
import { relative } from "path";
import withStyles from "@material-ui/core/styles/withStyles";
import taskDetailStyles from "./styles";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import {
  EditorState,
  convertToRaw,
  ContentState,
  convertFromRaw,
  convertFromHTML
} from "draft-js";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";
import draftToMarkdown from "draftjs-to-markdown";
import InputLabel from "@material-ui/core/InputLabel";
import ReactHtmlParser from 'react-html-parser';

class CommentsTextBox extends Component {
  constructor(props) {
    super(props);

    const contentBlock = htmlToDraft("");
    if (contentBlock) {
      const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
      const editorState = EditorState.createWithContent(contentState);
      this.state = {
        editorCommentState: editorState,
        toolbarHidden: true,
        contentState
      };
    }

    // this.handleEditClick = this.handleEditClick.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.onEditorStateChange = this.onEditorStateChange.bind(this);
    this.handleEditorFocus = this.handleEditorFocus.bind(this);
    this.handleEditorBlur = this.handleEditorBlur.bind(this);
  }
  componentDidMount() {
    //  
  }

  //   handleEditClick() {
  //     this.setState({ EditMode: true });
  //   }
  onEditorStateChange(editorCommentState) {
    this.setState({
      editorCommentState
    });
  }
  handleEditorFocus() {
    this.setState({ toolbarHidden: false })
  }
  handleEditorBlur() {
    this.setState({ toolbarHidden: true })
  }

  handleKeyPress(event) {
    if (event == 'split-block') {
      let saveComment = {
        "commentText": draftToHtml(convertToRaw(this.state.editorCommentState.getCurrentContent())),
        "taskId": this.props.currentTask.taskId,
        "commentType": 1
      };
      // this.setState({ editorCommentState: editorState }, () => {
      this.props.handleNewComments(saveComment);
      //  });
      // const contentBlock = htmlToDraft("");
      // if (contentBlock) {
      //   const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
      //   const editorState = EditorState.createWithContent(contentState);


      // }

      // const editorCommentState = EditorState.push(this.state.editorCommentState, ContentState.createFromText(''), 'remove-range');
      // this.setState({
      //   editorCommentState
      // }, () => {
      //   
      // });


    }
  }
  render() {
    const { classes } = this.props;
    const { editorCommentState, toolbarHidden, clear } = this.state;

    return (

      <Editor
        wrapperClassName="commentBoxWrapper"
        editorClassName="commentBoxEditor"
        toolbarClassName="commentBoxToolBar"
        wrapperStyle={toolbarHidden ? { minHeight: 50 } : { minHeight: 150 }}
        toolbarHidden={toolbarHidden}
        //  editorState={editorCommentState}
        onEditorStateChange={this.onEditorStateChange}
        onFocus={this.handleEditorFocus}
        onBlur={this.handleEditorBlur}
        handleKeyCommand={this.handleKeyPress}
        placeholder="Type your comment here..."
        toolbar={{
          options: ['inline', 'list', 'emoji'],
          inline: {
            options: ['bold', 'italic', 'underline', 'strikethrough']
          },
          list: {
            options: ['unordered']
          },
          emoji: {
            className: "emojiOption",
            popupClassName: undefined,
            emojis: [
              '😀', '😁', '😂', '😃', '😉', '😋', '😎', '😍', '😗', '🤗', '🤔', '😣', '😫', '😴', '😌', '🤓',
              '😛', '😜', '😠', '😇', '😷', '😈', '👻', '😺', '😸', '😹', '😻', '😼', '😽', '🙀', '🙈',
              '🙉', '🙊', '👼', '👮', '🕵', '💂', '👳', '🎅', '👸', '👰', '👲', '🙍', '🙇', '🚶', '🏃', '💃',
              '⛷', '🏂', '🏌', '🏄', '🚣', '🏊', '⛹', '🏋', '🚴', '👫', '💪', '👈', '👉', '👉', '👆', '🖕',
              '👇', '🖖', '🤘', '🖐', '👌', '👍', '👎', '✊', '👊', '👏', '🙌', '🙏', '🐵', '🐶', '🐇', '🐥',
              '🐸', '🐌', '🐛', '🐜', '🐝', '🍉', '🍄', '🍔', '🍤', '🍨', '🍪', '🎂', '🍰', '🍾', '🍷', '🍸',
              '🍺', '🌍', '🚑', '⏰', '🌙', '🌝', '🌞', '⭐', '🌟', '🌠', '🌨', '🌩', '⛄', '🔥', '🎄', '🎈',
              '🎉', '🎊', '🎁', '🎗', '🏀', '🏈', '🎲', '🔇', '🔈', '📣', '🔔', '🎵', '🎷', '💰', '🖊', '📅',
              '✅', '❎', '💯',
            ],
          },
        }}
        mention={{
          separator: " ",
          trigger: "@",
          suggestions: this.props.members
          // [
          //   { text: "APPLE", value: "apple", url: "apple" },
          //   { text: "BANANA", value: "banana", url: "banana" },
          //   { text: "CHERRY", value: "cherry", url: "cherry" },
          //   { text: "DURIAN", value: "durian", url: "durian" },
          //   { text: "EGGFRUIT", value: "eggfruit", url: "eggfruit" },
          //   { text: "FIG", value: "fig", url: "fig" },
          //   {
          //     text: "GRAPEFRUIT",
          //     value: "grapefruit",
          //     url: "grapefruit"
          //   },
          //   { text: "HONEYDEW", value: "honeydew", url: "honeydew" }
          // ]
        }}

      />

    );
  }
}

export default withStyles(taskDetailStyles)(CommentsTextBox);
