import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import combineStyles from "../../../utils/mergeStyles";
import InputLabel from "@material-ui/core/InputLabel";
import Avatar from "@material-ui/core/Avatar";
import DoneIcon from "@material-ui/icons/Done";
import Select from 'react-select';
import { components } from "react-select";
import autoCompleteStyles from "../../../assets/jss/components/autoComplete";
import Grid from "@material-ui/core/Grid";
import DropdownArrow from "@material-ui/icons/ArrowDropDown"
const options = [
  { value: "vanilla", label: "Vanilla", rating: "safe" },
  { value: "chocolate", label: "Chocolate", rating: "good" },
  { value: "strawberry", label: "Strawberry", rating: "wild" },
  { value: "salted-caramel", label: "Salted Caramel", rating: "crazy" }
];
class SelectTask extends Component {
  constructor(props) {
    super(props);
    this.state = {
      AssignedToLabel: false,
      AddToProjectLabel: false,
      value: [],
      options: options,
      AssignEmailError: false,
      taskTitle: undefined
    };
    this.handleSelectChange = this.handleSelectChange.bind(this);
  }
  handleSelectChange(newValue, actionMeta) {
      this.setState({ value: newValue });
  }


  render() {
    
    const { classes, theme } = this.props;
    const DropdownIndicator = (props) => {
      return components.DropdownIndicator && (
        <components.DropdownIndicator {...props}>
         <DropdownArrow htmlColor={theme.palette.secondary.light} fontSize="default" />
        </components.DropdownIndicator>
      );
    };
    // Styles of react-select autocomplete
    const customStyles = {
      control: (base, state) => ({
        ...base,
        background: "transparent",
        padding: "5.5px 10px 2px 10px",
        borderRadius: 4,
        borderColor: theme.palette.border.lightBorder,
        boxShadow: 'none',
        ":focus": {
            border: `1px solid ${theme.palette.border.darkBorder}`
          },
          ":hover": {
            border: `1px solid ${theme.palette.border.darkBorder}`
          }
      }),
      dropdownIndicator: (base, state) => ({
        padding: 0
      }),
      valueContainer: (base, state) => ({
        ...base,
        padding: "5px 0"
      })
    };
    const Option = props => {
     // 
      return (
        <components.Option {...props}>
          <Grid
            container
            direction="row"
            justify="flex-start"
            alignItems="center"
          >
           
              <Avatar classes={{ root: classes.dropDownAvatar }}>H</Avatar>
       
            <span>{props.children}</span>
          </Grid>
        </components.Option>
      );
    };

    return (
      <FormControl classes={{ root: classes.formControl }} fullWidth={true}>
        <InputLabel
          required
          htmlFor="AddToProject"
          classes={{
            root: classes.autoCompleteLabel,
            shrink: classes.shrinkLabel
          }}
          shrink={false}
        >
          Select Task
        </InputLabel>
        <Select
          styles={customStyles}
          
          onChange={this.handleSelectChange}
          inputId="AddToProject"
          components={{ DropdownIndicator, IndicatorSeparator: false }}
          options={options}
          value={this.state.value}
          placeholder=""
        />
      </FormControl>
    );
  }
}
export default withStyles(autoCompleteStyles, {
  withTheme: true
})(SelectTask);