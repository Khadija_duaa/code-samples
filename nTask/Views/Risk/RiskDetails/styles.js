const taskDetailStyles = theme => ({
  taskDetailsDialog: {
    paddingRight: "0 !important",
  },
  dialogCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  dialogPaperCnt: {
    overflowY: "visible",
    background: theme.palette.common.white,
    maxWidth: "95%",
    minHeight: "90%",
  },
  defaultDialogTitle: {
    padding: "0",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
  },
  defaultDialogContent: {
    padding: "0",
    overflowY: "visible",
    display: "flex",
    justifyContent: "stretch",
    alignItems: "stretch",
  },
  defaultDialogAction: {
    padding: " 25px 25px",
  },
  assigneeListCnt: {
    padding: "0 20px",
    margin: "0 10px",
    borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
    borderRight: `1px solid ${theme.palette.border.lightBorder}`,
  },
  mainTaskDetailsCnt: {
    marginBottom: 20,
  },

  taskDetailsLeftCnt: {
    borderRight: `1px solid ${theme.palette.border.lightBorder}`,
    padding: "30px",
    height: "100%",
  },
  dropdownsLabel: {
    transform: "translate(6px, -7px) scale(1)",
    display: "block",
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular,
  },
  btnDropdownsLabel: {
    transform: "translate(0, -7px) scale(1)",
    display: "block",
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular,
  },
  taskDetailsFieldCnt: {},
  descriptionCnt: {
    height: 120,
    padding: 10,
    resize: "vertical",
    borderRadius: "4px",
    overflowY: "auto",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    fontSize: "12px !important",
    "& *": {
      margin: 0,
    },
  },
  tabLabelCnt: {
    padding: 0,
    fontSize: "12px !important",
    textTransform: "capitalize",
    fontWeight: theme.typography.fontWeightRegular,
  },
  TabsRoot: {
    background: theme.palette.background.paper,
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
  },
  tab: {
    minWidth: "unset",
    flexGrow: 1,
    flexBasis: "unset"
  },
  tabSelected: {
    background: "unset !important",
    boxShadow: "unset !important",
  },
  TabContentCnt: {
    background: theme.palette.background.light,
    position: "relative",
    height: "calc(100% - 49px)",
  },
  TabContentCntActivity: {
    background: "white",
    height: "calc(100% - 49px)",
    position: "relative",
  },
  tabIndicator: {
    background: theme.palette.primary.light,
  },
  outlinedInputCnt: {
    marginRight: 15,
  },
  outlinedTimeInputCnt: {
    margin: 0,
    background: theme.palette.common.white,
    borderRadius: 4,
  },
  outlinedMinInputCnt: {
    "&:hover $notchedOutlineMinCnt": {
      border: `1px solid ${theme.palette.border.lightBorder} !important`,
    },
  },
  outlinedHoursInputCnt: {
    "&:hover $notchedOutlineHoursCnt": {
      border: `1px solid ${theme.palette.border.lightBorder} !important`,
      borderRight: "none !important",
    },
  },
  outlineInputFocus: {
    "& $notchedOutlineCnt": {
      border: `1px solid ${theme.palette.border.lightBorder}`,
    },
    "& $notchedOutlineMinCnt": {
      border: `1px solid ${theme.palette.border.lightBorder}`,
    },
    "& $notchedOutlineHoursCnt": {
      border: `1px solid ${theme.palette.border.lightBorder}`,
      borderRight: 0,
    },
    "& $notchedOutlineAMCnt": {
      border: `1px solid ${theme.palette.border.lightBorder}`,
    },
  },
  outlinedInput: {
    padding: "13.5px 14px",
    "&:hover": {
      cursor: "auto",
    },
  },
  outlinedDateInput: {
    padding: "13.5px 14px",
    background: theme.palette.background.default,
    borderRadius: 4,
  },
  outlinedTaskInput: {
    padding: "10px 14px",
    background: theme.palette.background.default,
    borderRadius: 4,
    fontSize: "22px !important",
    fontWeight: theme.typography.fontWeightRegular,
  },
  outlinedInputDisabled: {
    background: "transparent",
  },
  outlinedTimeInput: {
    padding: 14,
    textAlign: "center",
  },
  outlinedHoursInput: {
    background: theme.palette.background.default,
    textAlign: "center",
    borderRadius: "30px 0 0 30px",
    padding: 14,
  },
  outlinedMinInput: {
    padding: 14,
    background: theme.palette.background.default,
    textAlign: "center",
  },
  outlinedAmInput: {
    padding: 14,
    borderRadius: "0 30px 30px 0",
    borderLeft: 0,
    textAlign: "center",
    "&:hover": {
      cursor: "pointer",
    },
  },
  notchedOutlineCnt: {
    borderRadius: 4,
    border: `1px solid ${theme.palette.border.lightBorder}`,
  },
  notchedOutlineHoursCnt: {
    borderRadius: "30px 0 0 30px",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRight: 0,
  },
  notchedOutlineMinCnt: {
    borderRadius: 0,
    border: `1px solid ${theme.palette.border.lightBorder}`,
  },
  notchedOutlineAMCnt: {
    borderRadius: "0 30px 30px 0",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderLeft: 0,
  },
  addTimeBtnCnt: {
    display: "flex",
    background: theme.palette.background.medium,
    padding: "5px 5px",
    cursor: "pointer",
    borderRadius: 20,
  },
  addTimeActionBtnCnt: {
    textAlign: "right",
    marginTop: 10,
  },
  addTimeBtnText: {
    color: theme.palette.text.primary,
    fontSize: "16px !important",
    lineHeight: "20px",
    marginLeft: 5,
    textDecoration: "underline",
  },

  clInputProgressCnt: {
    display: "flex",
    alignItems: "center",
    marginBottom: 10,
  },
  clProgressValue: {
    position: "absolute",
    right: 13,
    top: 24,
    fontSize: "15px !important",
  },
  CloseIconBtn: {
    visibility: "hidden",
  },
  checkboxCheckedIcon: {
    color: theme.palette.primary.light,
  },
  checkboxIcon: {},
  datePickerPopper: {
    zIndex: 2,
  },

  menuTab: {
    background: theme.palette.background.light,
    borderRadius: "10px 10px 0 0",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderBottom: "none",
    opacity: 1,
    minWidth: 152,
  },
  menuTabSelected: {
    background: theme.palette.common.white,
    borderRadius: "10px 10px 0 0",
    border: `1px solid ${theme.palette.border.lightBorder}`,
  },
  menuTabIndicator: {
    background: "transparent",
    height: 1,
  },
  menuTimeCnt: {
    background: theme.palette.background.light,
    marginTop: 48,
    marginBottom: 4,
    border: "1px solid rgba(221,221,221,1)",
    borderRadius: "0 12px 12px 0",
    borderLeft: 0,
    width: 250,
  },
  dateTimeHeading: {
    background: theme.palette.background.default,
    padding: "10px 20px",
    margin: 0,
    textTransform: "uppercase",
    fontSize: "0.944rem !important",
    fontWeight: theme.typography.fontWeightRegular,
    borderRadius: "0 12px 0 0",
    color: theme.palette.text.secondary,
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
  },
  menuDateTimeInnerCnt: {
    padding: "15px 20px",
  },
  timeFieldsCnt: {
    padding: "0 20px",
  },
  otherCommentAvatar: {
    marginRight: 10,
  },
  selfCommentAvatar: {
    marginLeft: 10,
  },
  commentContentCnt: {
    borderRadius: 4,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    padding: "10px 15px",
    fontSize: 13,
    background: theme.palette.background.default,
    wordBreak: "break-all",
    "& p": {
      margin: 0,
      padding: 0,
      lineHeight: 1.5,
      wordBreak: "break-word",
    },
  },
  commentAttachContentCnt: {
    borderRadius: 10,
    fontSize: 13,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    padding: "10px 15px",
    background: theme.palette.background.default,
    "& p": {
      margin: 0,
      padding: 0,
    },
  },
  commentBoxMention: {
    color: theme.palette.primary.light,
  },
  otherTimeStamp: {
    fontSize: "11px !important",
    marginLeft: 12,
    color: theme.palette.text.secondary,
    textAlign: "left",
    margin: 0,
    marginTop: 3,
  },
  selfTimeStamp: {
    fontSize: "11px !important",
    marginRight: 12,
    color: theme.palette.text.secondary,
    textAlign: "right",
    margin: 0,
    marginTop: 3,
  },
  attachmentCnt: {
    background: theme.palette.background.light,
    borderRadius: 5,
    marginTop: 10,
    padding: "10px 15px 10px 15px",
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
  },
  commentsAttachmentIcon: {
    marginRight: 10,
  },
  commentsFileName: {
    fontSize: "12px !important",
    paddingRight: "20px !important",
    color: theme.palette.text.primary,
    marginBottom: "3px !important",
  },
  commentsFileSize: {
    fontSize: "10px !important",
    color: theme.palette.text.secondary,
  },
  attachmentDownloadIcon: {
    background: theme.palette.secondary.medDark,
    borderRadius: "100%",
  },
  // Notification Menu Styles
  NotifAvatar: {
    borderRadius: "100%",
    width: 56,
    height: 56,
  },
  notifMenuItem: {
    height: "auto",
    padding: "15px 15px",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
  },
  notifMenuHeadingItem: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    cursor: "unset",
    "&:hover": {
      background: "transparent",
    },
  },
  menuHeaderReadAllText: {
    color: theme.palette.text.primary,
    cursor: "pointer",
  },
  menuHeadingText: {
    textTransform: "uppercase",
  },
  notifMenuItemText: {
    padding: 0,
    "& p": {
      fontSize: "12px !important",
      fontWeight: theme.typography.fontWeightLight,
      color: theme.palette.text.secondary,
      whiteSpace: "normal",
      margin: 0,
      "& b": {
        fontWeight: theme.typography.fontWeightMedium,
        color: theme.palette.text.primary,
      },
    },
    "& $notifMenuDate": {
      fontSize: "10px !important",
    },
  },
  notifMenuDate: {},
  activityCardCnt: {
    padding: "8px 10px",
    background: theme.palette.common.white,
    borderRadius: 4,
    border: `1px solid ${theme.palette.border.lightBorder}`,
  },
  activityMsg: {
    color: theme.palette.text.primary,
    "& b": {
      color: theme.palette.text.primary,
      fontWeight: theme.typography.fontWeightMedium,
    },
  },
  activityDate: {
    textTransform: "uppercase",
    marginBottom: 10,
    display: "flex",
    alignItems: "center",
  },
  calendarIcon: {
    marginRight: 5,
  },
  activityTime: {
    fontSize: "12px !important",
    margin: "0 0 5px 0",
    color: theme.palette.text.secondary,
  },
  singleActivityCnt: {
    marginBottom: 20,
  },
  descriptionEditor: {
    marginBottom: 30,
  },
  commentsEditor: {
    position: "absolute",
    bottom: 12,
    left: 17,
    right: 17,
  },
  taskTitle: {
    lineHeight: "47px",
    padding: "0 15px",
    transition: "ease background 1s",
    borderRadius: 4,
    "&:hover": {
      background: theme.palette.background.items,
      transition: "ease background 1s",
      cursor: "text",
    },
  },
  commentsTabCnt: {
    display: "flex",
    flexDirection: "column",
  },
  commentsCnt: {
    flex: 1,
  },
  editor: {
    height: 120,
    padding: 20,
    fontSize: "12px !important",
    overflowY: "auto",
    borderRadius: 4,
  },
  impactIcon: {
    fontSize: "14px !important",
    marginRight: 5,
  },
  unArchiveTaskCnt: {
    background: theme.palette.background.black,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    padding: 10,
  },
  unArchiveTaskTxt: {
    color: theme.palette.common.white,
  },
  updatesIcon: {
    fontSize: "22px !important",
  },
  container: {
    padding: "12px 10px 0px  10px",
    display: "flex",
    alignItems: "flex-start",
  },
  detail: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    padding: "0px 0px 6px 0px",
    margin: "0px 8px 0px 6px",
    borderBottom: `1px solid ${theme.palette.background.grayLighter}`,
    width: "100%",
  },
  description: {
    textAlign: "left",
    fontFamily: "Lato, sans-serif",
    fontWeight: theme.typography.fontWeightLight,
    fontSize: "13px !important",
    lineHeight: "16px",
    letterSpacing: 0,
    color: `${theme.palette.text.primay}`,
  },
  user: {
    color: `${theme.palette.common.black}`,
    fontSize: 13,
    fontWeight: theme.typography.fontWeightLarge,
    fontFamily: theme.typography.fontFamilyLato,
  },
  actionDescription: {
    color: `#333333`,
    fontSize: "13px !important",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
  },
  date: {
    padding: "5px 0",
    textAlign: "left",
    fontFamily: "Lato, sans-serif",
    fontWeight: theme.typography.fontWeightLight,
    fontSize: "12px !important",
    lineHeight: "15px",
    letterSpacing: 0,
    color: `${theme.palette.text.medGray}`,
  },
  addCustomFieldCnt: {
    marginBottom: 20,
  },
  customFieldsCnt: {
    marginBottom: 20,
  },
  addCustomFieldCnt: {
    padding: "20px 0 20px 0",
    marginTop: 20,
    // borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    borderTop: `1px solid ${theme.palette.border.lightBorder}`,
  },
  assessmentGraphHeadingCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  systemFieldsHeading: {
    width: "100%",
    height: "32px",
    background: "#7F8F9A",
    borderRadius: "4px",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    padding: "10px",
    marginTop: "8px",
    cursor: "pointer",

    "& h3": {
      fontSize: "13px !important",
      fontFamily: theme.typography.fontFamilyLato,
      color: theme.palette.common.white,
      marginBottom: 3,
    },
    "&:hover $editIcon": {
      opacity: "1",
      cursor: "pointer",
    },
    "&:hover $documentIcon": {
      opacity: "1",
      cursor: "pointer",
    },
  },
  editIcon: {
    fontSize: "14px !important",
    top: "-2px",
    color: "#fff",
    position: "relative",
    opacity: "0",
    marginRight: "14px",
    fontSize: "14px !important",
  },
  documentIcon: {
    color: "#fff",
    opacity: "0",
    fontSize: "20px !important",
    marginRight: "7px",
  },
  systemFieldRemoveIcon: {
    color: theme.palette.common.white,
    fontSize: "20px !important",
    cursor: "pointer",
  },
  systemFieldAddIcon: {
    color: theme.palette.common.white,
    fontSize: "20px !important",
    cursor: "pointer",
  },
  editorContainer:{
    display: "flex",
    "& .fr-box": {
      border: "none !important"
    },
    "& .fr-second-toolbar": {
      display: "none !important"
    },
    "& .fr-wrapper": {
      "&:hover":{
        background: "#F6F6F6 !important",
        borderRadius: 4
      },
    },
    "& .fr-element": {
      padding: "5px 5px !important",
      // "&:hover":{
      //   background: "#F6F6F6"
      // },
      "& p":{
        background: "transparent !important"
      },
      "& span":{
        background: "transparent"
      }
    },
  },

  /* Extra small devices (phones, 600px and down and tablet) */
  "@media only screen and (max-width: 1024px)": {
    editorContainer: {
      display: "block",
    },
  },
});

export default taskDetailStyles;
