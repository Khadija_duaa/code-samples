import React, { Component } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withSnackbar } from "notistack";
import { ResponsiveLine } from "@nivo/line";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import CloseIcon from "@material-ui/icons/Close";
import IconButton from "../../../components/Buttons/IconButton";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import taskDetailStyles from "./styles";
import { withStyles } from "@material-ui/core/styles";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import { Scrollbars } from "react-custom-scrollbars";
import SideTabs from "./SideTabs";
import SelectIconMenu from "../../../components/Menu/TaskMenus/SelectIconMenu";
import RiskDescriptionEditor from "./RiskDetailsDescription";
// import MemberListMenu from "../../Menu/TaskMenus/MemberListMenu";
import RiskActionDropdown from "../List/RiskActionDropDown";
import helper from "../../../helper";
import NotificationMenu from "../../../components/Header/NotificationsMenu";
import AssigneeDropdown from "../../../components/Dropdown/AssigneeDropdown";
import SelectSearchDropdown from "../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { generateSelectData } from "../../../helper/generateSelectData";
import { customFieldDropdownData, getCustomFields } from "../../../helper/customFieldsData";
import { getActiveFields } from "../../../helper/getActiveCustomFields";
import { CopyTask, DeleteTask, ArchiveTask, UnArchiveTask } from "../../../redux/actions/tasks";
import { UpdateCalenderTask } from "../../../redux/actions/calenderTasks";
import {
  UpdateRisk,
  DeleteRisk,
  UnarchiveRisk,
  ArchiveRisk,
  MarkRiskNotificationsAsRead,
  updateRisk,
  dispatchRisk,
  bulkUpdateRisk,
  bulkUpdateSelectedCustomField,
} from "../../../redux/actions/risks";
import {
  getRisksPermissions,
  getRisksEditPermissionsWithArchieve,
  getRisksPermissionsWithoutArchieve,
} from "../permissions";
import { impactData, likelihoodData } from "../../../helper/riskDropdownData";
import CustomButton from "../../../components/Buttons/CustomButton";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import { GetPermission } from "../../../components/permissions";
import { FormattedMessage, injectIntl } from "react-intl";
import CustomFieldView from "../../CustomFieldSettings/CustomFields.view";
import Checklist from "../../../components/Checklist/Checklist";
import cloneDeep from "lodash/cloneDeep";
import UpdateMatrixDialog from "../../../components/Matrix/updateMatrixDialog.cmp";
import { SaveItemOrder } from "../../../redux/actions/itemOrder";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";
import moment from "moment";
import { editCustomField, updateCustomFieldData } from "../../../redux/actions/customFields";
import RepeatScheduleCmp from "../../../components/RepeatSchedule/repeatSchedule.cmp";
import DropdownMenu from "../../../components/Dropdown/DropdownMenu";
import RepeatTask from "../../Task/RepeatTask/RepeatTask";
import CustomFieldsDropdownsView from "../../../components/CustomFieldsViews/CustomFieldsDropdowns.view";
import CustomFieldsChecklistView from "../../../components/CustomFieldsViews/CustomFieldsChecklist.view";
import CustomFieldsText from "../../../components/CustomFieldsViews/CustomFieldsText.view";
import { calculateHeight } from "../../../utils/common";
import CustomFieldsEmailView from "../../../components/CustomFieldsViews/CustomFieldsEmail.view";
// import CustomFieldsNumberView from "../../../components/CustomFieldsViews/CustomFieldsNumber.view ";
import CustomFieldsAddressView from "../../../components/CustomFieldsViews/CustomFieldsAddress.view";
import CustomFieldsWebSiteView from "../../../components/CustomFieldsViews/CustomFieldsWebSite.view";
import CustomFieldsContactView from "../../../components/CustomFieldsViews/CustomFieldsContact.view";
import isArray from "lodash/isArray";

import RemoveIcon from "@material-ui/icons/Remove";
import AddIcon from "@material-ui/icons/Add";
import IconEditSmall from "../../../components/Icons/IconEditSmall";
import IconColor from "../../../components/Icons/IconColor";
import SvgIcon from "@material-ui/core/SvgIcon";

const matrixData = [
  {
    id: "assessment",
    color: "hsl(302, 70%, 50%)",
    data: [
      {
        x: "A",
        y: "3",
      },
      {
        x: "B",
        y: "8",
      },
      {
        x: "C",
        y: "2",
      },
    ],
  },
];
const MyResponsiveLine = ({ data, axisLeft, axisBottom, chartProps, xScale, yScale }) => {
  return (
    <ResponsiveLine
      data={data}
      margin={{ top: 20, right: 20, bottom: 50, left: 40 }}
      xScale={{ type: "linear", stacked: false, ...xScale }}
      yScale={{ type: "linear", stacked: false, ...yScale }}
      tooltip={({ point }) => {
        return (
          <div
            style={{
              padding: 5,
              background: "white",
              border: "1px solid rgba(221, 221, 221, 1)",
              borderRadius: 4,
              fontSize: "13px",
              fontFamily: "'lato', sans-serif",
            }}>
            {point.data.obj.cellName} : {moment(point.data.obj.createdDate).format("DD MMM, YYYY")}
          </div>
        );
      }}
      // yFormat=" >-.2f"
      axisTop={null}
      axisRight={null}
      enableGridX={false}
      axisBottom={{
        orient: "bottom",
        tickSize: 5,
        tickPadding: 5,
        tickRotation: 0,
        tickValues: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        format: function(value) {
          const alphabets = "ABCDEFGHIJ";
          return alphabets[value - 1];
        },
        // tickValues: ['A', 'B', 'C', 'D', 'E', 'F'],
        ...axisBottom,
        // legend: 'transportation',
        // legendOffset: 36,
        // legendPosition: 'middle'
      }}
      axisLeft={{
        orient: "left",
        tickSize: 5,
        tickPadding: 5,
        tickRotation: 0,
        tickValues: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        ...axisLeft,
        // legend: 'count',
        // legendOffset: -40,
        // legendPosition: 'middle'
      }}
      lineWidth={0}
      pointColor={{ theme: "background" }}
      pointBorderWidth={2}
      pointBorderColor="#7f3493"
      pointLabelYOffset={-12}
      useMesh={true}
      pointSize={10}
      // legends={[
      //     {
      //         anchor: 'bottom-right',
      //         direction: 'column',
      //         justify: false,
      //         translateX: 100,
      //         translateY: 0,
      //         itemsSpacing: 0,
      //         itemDirection: 'left-to-right',
      //         itemWidth: 80,
      //         itemHeight: 20,
      //         itemOpacity: 0.75,
      //         symbolSize: 12,
      //         symbolShape: 'circle',
      //         symbolBorderColor: 'rgba(0, 0, 0, .5)',
      //         effects: [
      //             {
      //                 on: 'hover',
      //                 style: {
      //                     itemBackground: 'rgba(0, 0, 0, .03)',
      //                     itemOpacity: 1
      //                 }
      //             }
      //         ]
      //     }
      // ]}
    />
  );
};
class RiskDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: true,
      name: [],
      editRiskName: false,
      riskNameError: false,
      riskName: "",
      currentRisk: {},
      riskNameOriginal: "",
      readAll: true,
      task: [],
      unArchiveFlag: false,
      unarchiveBtnQuery: "",
      editRisk: false,
      editMittigation: false,
      assessmentDialogOpen: false,
      repeatDialogOpen: false,
      showsystemfield: false,
    };
    this.handleRiskNameEdit = this.handleRiskNameEdit.bind(this);
    this.handleRiskNameSave = this.handleRiskNameSave.bind(this);
    this.handleRiskNameInput = this.handleRiskNameInput.bind(this);
    this.handleAllRead = this.handleAllRead.bind(this);
  }

  componentDidMount() {
    this.setState({
      currentRisk: this.props.currentRisk,
      riskName: this.props.currentRisk.title,
      riskNameOriginal: this.props.currentRisk.title,
      task: this.generateSelectedValue(),
    });
    document.querySelector("#root").style.filter = "blur(8px)";
  }
  componentWillUnmount() {
    document.querySelector("#root").style.filter = "";
    if (this.props.listView) {
      this.props.history.push("/risks");
    }
  }
  componentDidUpdate(prevProps, prevState) {
    const { currentRisk } = this.props;

    // if currentRisk is deleted from other users
    if (!currentRisk && JSON.stringify(prevProps.currentRisk) !== JSON.stringify(currentRisk)) {
      this.closeActionMenu();
    } else if (JSON.stringify(prevProps.currentRisk) !== JSON.stringify(currentRisk)) {
      this.setState({
        currentRisk: currentRisk,
        riskName: currentRisk.title,
        riskNameOriginal: currentRisk.title,
      });
    }
  }

  handleAllRead() {
    const currentUser = this.props.userId;
    if (this.state.readAll) {
      this.setState({ readAll: false }, () => {
        this.props.MarkRiskNotificationsAsRead(
          this.state.currentRisk.id,
          currentUser,
          (err, data) => {
            this.setState({ readAll: true });
          }
        );
      });
    }
  }

  handleRiskNameEdit() {
    this.setState({ editRiskName: true });
  }
  riskNameValidator = () => {
    let response1 = helper.HELPER_EMPTY("Risk title", this.state.riskName);
    let response2 = helper.HELPER_CHARLIMIT80("Risk title", this.state.riskName);
    let response3 = helper.RETURN_CECKSPACES(this.state.riskName, "Risk title");
    let error = null;
    if (response1) {
      error = response1;
    } else {
      if (response2) {
        error = response2;
      } else if (response3) {
        error = response3;
      }
    }
    if (error) {
      this.setState({
        riskNameError: true,
      });
      return false;
    } else {
      return true;
    }
  };
  handleRiskNameSave(event, isClick) {
    if (
      (event.key === "Enter" || isClick) &&
      this.riskNameValidator() &&
      this.state.riskName &&
      this.state.riskNameOriginal !== this.state.riskName
    ) {
      let currentRisk = this.state.currentRisk,
        self = this;
      this.setState({ riskName: self.state.riskName }, function() {
        currentRisk.title = self.state.riskName;
        this.props.isUpdated(currentRisk, () => {
          this.setState({
            riskNameError: false,
            editRiskName: false,
            riskNameOriginal: this.state.riskName,
          });
        });
      });
    }
  }
  handleRiskNameInput(event) {
    this.setState({ riskName: event.target.value });
  }
  handleRiskClickAway = event => {
    if (!helper.RETURN_CECKSPACES(this.state.riskName))
      this.setState({ editRiskName: false }, () => {
        this.handleRiskNameSave(event, true);
      });
  };

  closeActionMenu = () => {
    if (this.props.closeActionMenu) this.props.closeActionMenu("CloseDetailsDialogue");
  };
  //Function that updates assignee of respective document (e.g. task/risk/issue/meeting)
  updateRiskOwner = (assignedTo, risk) => {
    let newRiskObj = { ...risk, riskOwner: assignedTo[0] && assignedTo[0].userId };

    this.props.UpdateRisk(newRiskObj, () => {});
  };

  generateTaskData = () => {
    /* function for generating the array of task and returing array to the data prop for multi drop down */
    const { tasks } = this.props; /** task array coming from mapStateToProps */
    let taskList = tasks.map((task, i) => {
      return generateSelectData(task.taskTitle, task.taskTitle, task.taskId, task.uniqueId, task);
    });
    return taskList;
  };
  handleClearTask = (key, value) => {
    const { currentRisk, task } = this.state;

    let taskSelectedIds = value.map(obj => {
      return obj.id;
    });

    this.setState({
      [key]: value,
      currentRisk: { ...currentRisk, linkedTasks: taskSelectedIds },
    });
    this.props.UpdateRisk(
      { ...currentRisk, linkedTasks: taskSelectedIds },
      response => {}
    ); /** api calling to update the risk details */
  };
  handleTaskChange = (key, value) => {
    /* function for fetching the task id's from selected task and update the risk details  */
    const { currentRisk, task } = this.state;

    let permission = this.checkRiskListAccordingToPermission(task, value);

    if (permission) {
      let taskSelectedIds = value.map(obj => {
        return obj.id;
      });

      this.setState({
        [key]: value,
        currentRisk: { ...currentRisk, linkedTasks: taskSelectedIds },
      });
      this.props.UpdateRisk(
        { ...currentRisk, linkedTasks: taskSelectedIds },
        response => {}
      ); /** api calling to update the risk details */
    }
  };

  checkRiskListAccordingToPermission = (stateTask, selectedTask) => {
    if (stateTask.length <= 0) {
      /** if state task is empty then accept the selected task */
      return true;
    } else {
      this.showSnackBar(
        "Oops! You cannot link this task to this risk, because it is already linked to another task. Unlink the risk with the other task first and then try again. ",
        "error"
      );
      return false;
    }
  };

  showSnackBar(snackBarMessage, type) {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  }

  generateSelectedValue = () => {
    /** function for generating the selected task list objects from task id and return taskList for showing pre populated selected task if any => function call in componentDidMount */
    const { tasks, currentRisk } = this.props;
    const selectedTaskList = tasks.filter(task => {
      return currentRisk.linkedTasks.indexOf(task.taskId) > -1;
    });
    let taskList = selectedTaskList.map((task, i) => {
      return generateSelectData(task.taskTitle, task.taskTitle, task.taskId, task.uniqueId, task);
    });
    return taskList;
  };
  handleOptionsSelect = (key, value) => {
    /** Function that updates risk on select option */
    let { currentRisk } = this.props;
    this.setState({ currentRisk: { ...currentRisk, [key]: value.value } });
    let newRiskObj = { ...currentRisk, [key]: value.value };
    this.props.UpdateRisk(newRiskObj, () => {});
  };
  // It is called to open unarchived dialog
  openPopUP = event => {
    this.setState({ unArchiveFlag: true });
  };
  // It is called to unarchived task
  handleUnArchived = e => {
    if (e) e.stopPropagation();
    const { currentRisk } = this.state;
    const { id } = currentRisk || "";
    const obj = { id, unarchive: true };
    this.setState({ unarchiveBtnQuery: "progress" }, () => {
      this.props.isUpdated(obj, () => {
        this.setState({ unarchiveBtnQuery: "", unArchiveFlag: false }, () => {
          if (this.closeActionMenu) this.closeActionMenu();
          this.props.filterRisk(id);
        });
      });
    });
  };
  // It is called to close unarchived dialog
  handleDialogClose = event => {
    this.setState({ unArchiveFlag: false });
  };

  getPermission = (key, val) => {
    const { riskPermission = {} } = this.props;
    const { currentRisk } = this.state;
    switch (key) {
      case "edit":
        return GetPermission.canEdit(currentRisk.isOwner, riskPermission, val);
        break;
      case "add":
        return GetPermission.canAdd(currentRisk.isOwner, riskPermission, val);
        break;
      case "delete":
        return GetPermission.canDelete(currentRisk.isOwner, riskPermission, val);
        break;

      default:
        break;
    }
  };

  riskNotifications = () => {
    const { riskNotifications, userId, allMembers } = this.props;
    let notifications = riskNotifications.filter(x => {
      return x.users.filter(y => y.userId === userId && y.isViewed === false).length;
    });
    notifications = notifications
      .map(x => {
        x.createdName = allMembers.find(m => m.userId === x.createdBy);
        x.users = x.users.map(u => ({
          userId: u.userId,
          isViewed: u.isViewed,
        }));
        return x;
      })
      .map(x => ({
        id: x.notificationId,
        // title: x.createdName
        //   ? (x.createdName.fullName ||
        //       x.createdName.userName ||
        //       x.createdName.email) +
        //     " " +
        //     x.description
        //   : " ",
        title: x.description ? x.description : " ",
        date: helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(x.updatedDate || x.createdDate),
        time: helper.RETURN_CUSTOMDATEFORMATFORTIME(x.updatedDate || x.createdDate),
        profilePic: x.createdName ? x.createdName.imageUrl : "",
        createdName: x.createdName,
        notificationUrl: x.notificationUrl,
        teamName: x.teamName || "N/A",
        teamId: x.teamId,
        workspaceName: x.workSpaceName || "N/A",
        workspaceId: x.workSpaceId,
      }));
    return notifications;
  };
  //Handle Custom Field Select change
  customFieldChange = (option, obj = {}, settings = {}, success = () => {}, failure = () => {}) => {
    const riskCopy = this.state.currentRisk;

    if (obj.fieldType == "matrix") {
      const selectedField =
        riskCopy.customFieldData && riskCopy.customFieldData.find(x => x.fieldId == obj.fieldId);
      const isExist =
        selectedField &&
        selectedField.fieldData.data.findIndex(f => f.cellName == option.cellName) > -1;
      if (isExist === true) return;
    }
    const { fieldId } = obj;
    const newObj = {
      groupType: "risk",
      groupId: riskCopy.id,
      fieldType: obj.fieldType,
      fieldId,
      fieldData: { data: option },
    };
    if (obj.fieldType === "dropdown") {
      if (isArray(newObj.fieldData.data))
        newObj.fieldData.data = newObj.fieldData.data.map(opt => {
          return { id: opt.id };
        });
      else newObj.fieldData.data = { id: newObj.fieldData.data.id };
    }
    if (obj.fieldType === "matrix")
      newObj.fieldData.data = newObj.fieldData.data.map(opt => {
        return { cellName: opt.cellName };
      });

    //Updating Localstate

    const customFields =
      riskCopy.customFieldData &&
      riskCopy.customFieldData.map(c => {
        return c.fieldId === newObj.fieldId ? newObj : c;
      });

    let newRiskObj = { ...riskCopy, customFieldData: customFields ? customFields : [newObj] };
    this.setState({ currentRisk: newRiskObj });

    updateRisk(
      newObj,
      res => {
        success();
        const resObj = res.data.entity[0];
        let customFieldsArr = [];
        // Updating Global state
        const isExist = riskCopy.customFieldData
          ? riskCopy.customFieldData.findIndex(c => c.fieldId === resObj.fieldId) > -1
          : false; /** if new risk created, its customFields are null so in null case scenario , compile the normal false flow, add custom field in risk object and save it */
        if (isExist) {
          customFieldsArr = riskCopy.customFieldData.map(c => {
            return c.fieldId === resObj.fieldId ? resObj : c;
          });
        } else {
          customFieldsArr = riskCopy.customFieldData
            ? [...riskCopy.customFieldData, resObj]
            : [
                resObj,
              ]; /** add custom field in risk object and save it, if newly created risk because its custom field is already null */
        }
        let newRiskObj = { ...riskCopy, customFieldData: customFieldsArr };
        this.props.dispatchRisk(newRiskObj);
      },
      () => {
        failure();
      }
    );
    this.handleAddAssessmentDialog(false);
  };
  onChatMount = () => {};
  onChatUnMount = () => {};

  // addCustomFieldCallback = obj => {
  //   const { fieldId } = obj;
  //   const customFieldDataObj = {
  //     groupType: "risk",
  //     groupId: this.state.currentRisk.id,
  //     fieldId: fieldId,
  //     fieldData: { data: obj.settings.multiSelect ? [] : {} },
  //   };
  //   const riskCopy = cloneDeep(this.state.currentRisk);
  //   riskCopy.customFieldData = [...riskCopy.customFieldData, customFieldDataObj];
  //   this.setState({ currentRisk: riskCopy });
  //   this.props.dispatchRisk(riskCopy);
  // };

  editCustomFieldCallback = (obj, oldFieldObject) => {
    const { fieldId, fieldType } = obj;
    if (fieldType === "checklist") return;
    this.props.updateCustomFieldData(obj);
  };

  editCustomField = (riskParam, obj) => {
    const { fieldId, fieldType, settings } = obj;
    const riskCopy = cloneDeep(riskParam);
    riskCopy.customFieldData = riskCopy.customFieldData.map(r => {
      if (r.fieldId === fieldId) {
        switch (fieldType) {
          case "dropdown":
            if (!settings.multiSelect) {
              /** if type is object/single select then simply replace the values after finding the selected field value by ID */
              let item = obj.values.data.find(v => v.id == r.fieldData.data.id) || false;
              if (item) {
                item.label = item.value;
                r.fieldData.data = {
                  ...item,
                  obj: item,
                }; /** setting edited field value in risk object */
              }
            }
            if (settings.multiSelect) {
              /** if type is array means multi select then find the updated selected values and update in risk object */
              let selectedArr = obj.values.data.filter(x =>
                r.fieldData.data.some(y => x.id === y.id)
              );
              if (selectedArr.length > 0) {
                selectedArr = customFieldDropdownData(selectedArr);
                r.fieldData.data = selectedArr; /** setting edited field value in risk object */
              }
            }
            break;

          case "matrix":
            let allcolumn = obj.settings.matrix.reduce((elem1, elem2) =>
              elem1.concat(elem2)
            ); /** merging all columns rows into one array */
            let selectedColum = allcolumn.find(
              c => c.cellName == r.fieldData.data[0].cellName
            ); /** finding the selected risk column option itno all columns */
            if (selectedColum) {
              r.fieldData.data[0] = {
                ...r.fieldData.data[0],
                color: selectedColum.color,
              }; /** updating risk field data  */
            }
            break;

          default:
            break;
        }
        return r;
      } else return r;
    });
    return riskCopy;
  };

  handleAddAssessmentDialog = value => {
    this.setState({ assessmentDialogOpen: value });
  };
  generateAssessmentChartData = (data = []) => {
    const alphabets = "ABCDEFGHIJ";
    return data.map(a => {
      return {
        id: "assessment",
        color: "hsl(302, 70%, 50%)",
        data: [
          {
            y: a.columnValue,
            x: alphabets.indexOf(a.rowValue) + 1,
            obj: a,
          },
        ],
      };
    });
  };
  handleClickHideOption = (event, field) => {
    let object = { ...field };
    object.settings.isHideCompletedTodos = !object.settings.isHideCompletedTodos;
    delete object.team;
    delete object.workspaces;
    delete object.level;
    this.props.editCustomField(
      object,
      object.fieldId,
      res => {},
      failure => {}
    );
  };
  // Open Repeat Dialog Function
  repeatDropdownOpen = () => {
    this.setState({ repeatDialogOpen: true });
  };

  // Close Repeat Dialog Function
  closeRepeatDropdown = () => {
    this.setState({ repeatDialogOpen: false });
  };
  handleShowSystemField = () => {
    this.setState({ showsystemfield: !this.state.showsystemfield });
  };
  render() {
    const {
      classes,
      theme,
      allMembers,
      userId,
      riskNotifications,
      isArchivedSelected,
      riskPer,
      intl,
      customFields,
      profile,
    } = this.props;

    const { getPermission } = this;
    const {
      open,
      editRiskName,
      riskName,
      currentRisk,
      task,
      unArchiveFlag,
      unarchiveBtnQuery,
      repeatDialogOpen,
      showsystemfield,
    } = this.state;
    let t0 = performance.now();
    const permission = getRisksPermissions(currentRisk, this.props);
    let notifications = this.riskNotifications();
    const riskTitle = currentRisk.riskPermission
      ? currentRisk.riskPermission.riskDetails.editRiskName.isAllowEdit
      : true;
    const canEditMitigationPlan = currentRisk.riskPermission
      ? currentRisk.riskPermission.riskDetails.riskMitigationPlan.isAllowEdit
      : true;
    const canEditRiskTask = currentRisk.riskPermission
      ? currentRisk.riskPermission.riskDetails.riskTask.isAllowEdit
      : true;
    const canEditRiskDescription = currentRisk.riskPermission
      ? currentRisk.riskPermission.riskDetails.riskDescription.isAllowEdit
      : true;
    const canEditLikelihood = currentRisk.riskPermission
      ? currentRisk.riskPermission.riskDetails.editlikelihood.isAllowEdit
      : true;
    const canEditStatus = currentRisk.riskPermission
      ? currentRisk.riskPermission.riskDetails.editRiskStatus.isAllowEdit
      : true;
    const canEditImpact = currentRisk.riskPermission
      ? currentRisk.riskPermission.riskDetails.editRiskImpact.isAllowEdit
      : true;
    const canAddRiskOwner = currentRisk.riskPermission
      ? currentRisk.riskPermission.riskDetails.riskOwner.cando
      : true;

    const actionDropDownPermission = getRisksPermissionsWithoutArchieve(currentRisk, this.props);
    const riskAssigneeList = allMembers.filter(member => {
      return currentRisk.riskOwner == member.userId;
    });
    const chatPermission = {
      addAttachment: currentRisk.riskPermission
        ? currentRisk.riskPermission.riskDetails.riskComments.riskAttachment.isAllowAdd &&
          !isArchivedSelected
        : true,
      deleteAttachment: currentRisk.riskPermission
        ? currentRisk.riskPermission.riskDetails.riskComments.riskAttachment.isAllowDelete &&
          !isArchivedSelected
        : true,
      downloadAttachment: currentRisk.riskPermission
        ? currentRisk.riskPermission.riskDetails.riskComments.downloadAttachment.cando &&
          !isArchivedSelected
        : true,
      addComments: currentRisk.riskPermission
        ? currentRisk.riskPermission.riskDetails.riskComments.cando && !isArchivedSelected
        : true,
      editComment: currentRisk.riskPermission
        ? currentRisk.riskPermission.riskDetails.riskComments.comments.isAllowEdit &&
          !isArchivedSelected
        : true,
      deleteComment: currentRisk.riskPermission
        ? currentRisk.riskPermission.riskDetails.riskComments.comments.isAllowDelete &&
          !isArchivedSelected
        : true,
      convertToTask: currentRisk.riskPermission
        ? currentRisk.riskPermission.riskDetails.riskComments.convertToTask.cando &&
          !isArchivedSelected
        : true,
      reply: currentRisk.riskPermission
        ? currentRisk.riskPermission.riskDetails.riskComments.reply.cando && !isArchivedSelected
        : true,
      replyLater: currentRisk.riskPermission
        ? currentRisk.riskPermission.riskDetails.riskComments.replyLater.cando &&
          !isArchivedSelected
        : true,
      showReceipt: currentRisk.riskPermission
        ? currentRisk.riskPermission.riskDetails.riskComments.showReceipt.cando &&
          !isArchivedSelected
        : true,
      clearAll: currentRisk.riskPermission
        ? currentRisk.riskPermission.riskDetails.riskComments.clearAllReplies.cando &&
          !isArchivedSelected
        : true,
      clearReply: currentRisk.riskPermission
        ? currentRisk.riskPermission.riskDetails.riskComments.clearReply.cando &&
          !isArchivedSelected
        : true,
    };
    const docPermission = {
      uploadDocument: currentRisk.riskPermission
        ? currentRisk.riskPermission.riskDetails.riskDocuments.uploadDocument.cando &&
          !isArchivedSelected
        : true,
      addFolder: currentRisk.riskPermission
        ? currentRisk.riskPermission.riskDetails.riskDocuments.addFolder.cando &&
          !isArchivedSelected
        : true,
      downloadDocument: currentRisk.riskPermission
        ? currentRisk.riskPermission.riskDetails.riskDocuments.downloadDocument.cando &&
          !isArchivedSelected
        : true,
      viewDocument: currentRisk.riskPermission
        ? currentRisk.riskPermission.riskDetails.riskDocuments.cando && !isArchivedSelected
        : true,
      deleteDocument: currentRisk.riskPermission
        ? currentRisk.riskPermission.riskDetails.riskDocuments.deleteDocument.cando &&
          !isArchivedSelected
        : true,
      openFolder: currentRisk.riskPermission
        ? currentRisk.riskPermission.riskDetails.riskDocuments.openFolder.cando &&
          !isArchivedSelected
        : true,
      renameFolder: currentRisk.riskPermission
        ? currentRisk.riskPermission.riskDetails.riskDocuments.renameFolder.cando &&
          !isArchivedSelected
        : true,
    };
    const chatConfig = {
      contextUrl: "communication",
      contextView: "Risk",
      contextId: currentRisk.id,
      contextKeyId: "riskId",
      contextChat: "single",
      assigneeLists: riskAssigneeList,
    };
    // let editorDescriptionStyle = canEditRiskDescription ? {borderTop: `0.2px solid ${theme.palette.border.grayLighter}`} :{pointerEvents : "none", borderTop: `0.2px solid ${theme.palette.border.grayLighter}` };
    // let editorMittigationStyle = canEditMitigationPlan ? {borderTop: `0.2px solid ${theme.palette.border.grayLighter}`} :{pointerEvents : "none", borderTop: `0.2px solid ${theme.palette.border.grayLighter}` };
    // const workspaceFields = customFields.data.filter(f => {
    //   return f.workspaces
    // })
    let customFieldArr = getCustomFields(this.props.customFields, profile, "risk").filter(
      f => f.fieldType == "dropdown"
    );
    let customSections = getCustomFields(this.props.customFields, profile, "risk").filter(
      f => f.fieldType == "section"
    );

    const customFieldPermission =
      (currentRisk.riskPermission && currentRisk.riskPermission.riskDetails.customsFields) || {};
    const selectedMatrixObj =
      customFields.data.find(
        m =>
          m.settings.assessment &&
          m.settings.assessment.includes(profile.loggedInTeam) &&
          !m.settings.isHidden &&
          m.settings.hiddenInWorkspaces.indexOf(profile.loggedInTeam) == -1 &&
          m.settings.hiddenInGroup.indexOf("risk") == -1
      ) || false;
    const assessmentMatrixValues =
      (currentRisk.customFieldData &&
        currentRisk.customFieldData.find(c => c.fieldId === selectedMatrixObj.fieldId)) ||
      false;

    const filterDataByDay = () => {
      return assessmentMatrixValues && assessmentMatrixValues.fieldData.data
        ? assessmentMatrixValues.fieldData.data.reduce((r, cv) => {
            const sameDayAssessmentIndex = r.findIndex(v => {
              return moment(v.createdDate).diff(cv.createdDate, "day") == 0;
            });
            if (
              sameDayAssessmentIndex > -1 &&
              moment(cv.createdDate).diff(r[sameDayAssessmentIndex].createdDate, "seconds") > -1
            ) {
              r[sameDayAssessmentIndex] = cv;
            } else {
              r.push(cv);
            }
            return r;
          }, [])
        : [];
    };
    const assessmentChartData = this.generateAssessmentChartData(filterDataByDay());
    const impactDropdown = customFieldArr.find(d => d.fieldName === "Impact");
    const likeDropdown = customFieldArr.find(d => d.fieldName === "Likelihood");
    const taskDropdown = customFieldArr.find(d => d.fieldName === "Task");

    {
      /*Replace [1,2] with the array of team id's for which you want to show custom field*/
    }

    var t1 = performance.now();
    return (
      <Dialog
        classes={{
          root: classes.taskDetailsDialog,
          paper: classes.dialogPaperCnt,
          scrollBody: classes.dialogCnt,
        }}
        aria-labelledby="form-dialog-title"
        fullWidth={true}
        open={open}
        onEscapeKeyDown={this.closeActionMenu}>
        <DropdownMenu
          open={repeatDialogOpen}
          anchorEl={this.anchorEl}
          placement="bottom-start"
          style={{ width: 600 }}
          // disablePortal={false}
          id="riskReviewFrequency"
          closeAction={this.closeRepeatDropdown}>
          <RepeatScheduleCmp
            customFieldData={currentRisk.customFieldData}
            label="Review Frequency"
            customFieldChange={this.customFieldChange}
            closeAction={this.closeRepeatDropdown}
            groupType={"risk"}
          />
        </DropdownMenu>

        <DialogTitle classes={{ root: classes.defaultDialogTitle }}>
          {isArchivedSelected ? (
            <div className={classes.unArchiveTaskCnt}>
              <Typography variant="h6" className={classes.unArchiveTaskTxt}>
                This risk is archived. You can't edit this risk.
              </Typography>
              <CustomButton
                btnType="success"
                variant="contained"
                style={{ marginLeft: 10 }}
                onClick={event => {
                  this.openPopUP(event);
                }}>
                Unarchive
              </CustomButton>
            </div>
          ) : null}
          <Grid
            container
            direction="row"
            justify="space-between"
            alignItems="center"
            style={{ padding: "10px 15px" }}>
            <Grid item>
              {editRiskName && riskTitle ? (
                <ClickAwayListener mouseEvent="onMouseDown" touchEvent="onTouchStart" onClickAway={this.handleRiskClickAway}>
                  <OutlinedInput
                    labelWidth={150}
                    notched={false}
                    value={riskName}
                    onKeyDown={this.handleRiskNameSave}
                    autoFocus
                    error={this.state.riskNameError}
                    onChange={this.handleRiskNameInput}
                    placeholder={"Add Risk Title"}
                    style={{ width: 500 }}
                    inputProps={{ maxLength: 80 }}
                    classes={{
                      root: classes.outlinedInputCnt,
                      input: classes.outlinedTaskInput,
                      notchedOutline: classes.notchedOutlineCnt,
                      focused: classes.outlineInputFocus,
                    }}
                  />
                </ClickAwayListener>
              ) : (
                <Typography
                  classes={{ h2: classes.taskTitle }}
                  variant="h2"
                  onClick={this.handleRiskNameEdit}>
                  {riskName}
                </Typography>
              )}
            </Grid>
            <Grid item>
              <Grid container direction="row" justify="flex-end" alignItems="center">
                <div>
                  <SelectIconMenu
                    iconType="status"
                    menuType="riskStatus"
                    keyType="status"
                    MenuData={currentRisk}
                    isSingle={true}
                    isUpdated={this.props.isUpdated}
                    permission={canEditStatus && !isArchivedSelected}
                    permissionKey="status"
                    view="risk"
                    style={{ marginBottom: 0 }}
                    intl={intl}
                  />
                </div>
                <CustomButton
                  onClick={this.repeatDropdownOpen}
                  btnType="gray"
                  style={{ marginLeft: 10, height: 40, border: "1px solid #c4c4c4" }}
                  variant={"outlined"}
                  buttonRef={node => {
                    this.anchorEl = node;
                  }}
                  disabled={isArchivedSelected}>
                  Review Frequency
                </CustomButton>

                <div className={classes.assigneeListCnt}>
                  <AssigneeDropdown
                    assignedTo={riskAssigneeList}
                    updateAction={this.updateRiskOwner}
                    isArchivedSelected={isArchivedSelected}
                    obj={currentRisk}
                    singleSelect={true}
                    assigneeHeading={
                      <FormattedMessage
                        id="risk.common.risk-owner.label"
                        defaultMessage="Risk Owner"
                      />
                    }
                    addPermission={canAddRiskOwner}
                    deletePermission={canAddRiskOwner}
                  />
                </div>
                <Grid item>
                  <RiskActionDropdown
                    risk={currentRisk}
                    UpdateRisk={this.props.UpdateRisk}
                    selectedColor={currentRisk.colorCode ? currentRisk.colorCode : ""}
                    isUpdated={this.props.isUpdated}
                    DeleteRisk={this.props.DeleteRisk}
                    closeActionMenu={this.closeActionMenu}
                    permission={actionDropDownPermission}
                    isArchivedSelected={isArchivedSelected}
                    filterRisk={this.props.filterRisk}
                    riskPer={currentRisk.riskPermission}
                  />
                  <NotificationMenu
                    notificationsData={notifications}
                    MarkAllNotificationsAsRead={this.handleAllRead}
                    history={this.props.history}
                  />

                  <IconButton btnType="transparent" onClick={this.props.closeRiskDetailsPopUp}>
                    <CloseIcon htmlColor={theme.palette.secondary.medDark} />
                  </IconButton>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </DialogTitle>

        <DialogContent classes={{ root: classes.defaultDialogContent }}>
          <Grid container direction="row" justify="flex-start" alignItems="stretch">
            <Grid item xs={7} style={{ zIndex: 1 }}>
              <Scrollbars autoHeight autoHeightMin={0} autoHeightMax={calculateHeight() - 165}>
                <div className={classes.taskDetailsLeftCnt}>
                  <Grid container classes={{ container: classes.mainTaskDetailsCnt }} spacing={2}>
                    {customFieldArr.map(f => {
                      const ddData = () => customFieldDropdownData(f.values.data);
                      const currentField =
                        currentRisk.customFieldData &&
                        currentRisk.customFieldData.find(cf => cf.fieldId == f.fieldId);
                      return f.isSystem &&
                        f.fieldName !== "Task" &&
                        f.fieldName !== "Impact" &&
                        f.fieldName !== "Likelihood" ? (
                        <Grid item xs={4} classes={{ item: classes.taskDetailsFieldCnt }}>
                          <SelectSearchDropdown /* Task multi select drop down */
                            data={ddData} /* function calling for generating task array */
                            label={f.fieldName}
                            isMulti={f.settings.multiSelect}
                            selectChange={(type, option) =>
                              this.customFieldChange(option, f, f.settings)
                            } /** function calling on select task in drop down */
                            selectRemoveValue={(type, option) =>
                              this.customFieldChange(option, f, f.settings)
                            }
                            optionBackground={true}
                            selectedValue={
                              currentField && f.settings.multiSelect
                                ? currentField.fieldData.data
                                : currentField
                                ? currentField.fieldData.data
                                : []
                            }
                            placeholder={f.placeholder}
                            isDisabled={currentRisk.isArchive || !customFieldPermission.isAllowEdit}
                            styles={{ marginBottom: 0 }}
                          />
                        </Grid>
                      ) : (
                        <> </>
                      );
                    })}
                    {impactDropdown ? (
                      <Grid item xs={4} classes={{ item: classes.taskDetailsFieldCnt }}>
                        <SelectSearchDropdown /* Impact select drop down */
                          data={() => {
                            return impactData(theme, classes, intl);
                          }}
                          label={
                            <FormattedMessage
                              id="risk.common.impact.label"
                              defaultMessage="Impact"
                            />
                          }
                          selectChange={this.handleOptionsSelect}
                          type="impact"
                          selectedValue={impactData(theme, classes, intl).find(s => {
                            return s.value == currentRisk.impact;
                          })}
                          placeholder={
                            <FormattedMessage
                              id="risk.common.impact.label"
                              defaultMessage="Impact"
                            />
                          }
                          icon={true}
                          isMulti={false}
                          isDisabled={
                            currentRisk.isDeleted || currentRisk.isArchive || !canEditImpact
                          }
                          styles={{ marginBottom: 0 }}
                        />
                      </Grid>
                    ) : null}
                    {likeDropdown ? (
                      <Grid item xs={4} classes={{ item: classes.taskDetailsFieldCnt }}>
                        <SelectSearchDropdown /* Likelihood select drop down */
                          data={() => {
                            return likelihoodData(intl);
                          }}
                          label={
                            <FormattedMessage
                              id="risk.common.likelihood.label"
                              defaultMessage="Likelihood"
                            />
                          }
                          selectChange={this.handleOptionsSelect}
                          type="likelihood"
                          selectedValue={likelihoodData(intl).find(s => {
                            return s.value == currentRisk.likelihood;
                          })}
                          placeholder={
                            <FormattedMessage
                              id="risk.common.likelihood.placeholder"
                              defaultMessage="Select Likelihood"
                            />
                          }
                          icon={true}
                          isMulti={false}
                          isDisabled={
                            currentRisk.isDeleted || currentRisk.isArchive || !canEditLikelihood
                          }
                          styles={{ marginBottom: 0 }}
                        />
                      </Grid>
                    ) : null}
                    {taskDropdown ? (
                      <Grid item xs={4} classes={{ item: classes.taskDetailsFieldCnt }}>
                        <SelectSearchDropdown /* Task multi select drop down */
                          data={
                            this.generateTaskData
                          } /* function calling for generating task array */
                          label={
                            <FormattedMessage id="risk.common.task.label" defaultMessage="Task" />
                          }
                          selectChange={
                            this.handleTaskChange
                          } /** function calling on select task in drop down */
                          selectRemoveValue={this.handleClearTask}
                          type="task"
                          selectedValue={task}
                          placeholder={
                            <FormattedMessage
                              id="risk.common.task.placeholder"
                              defaultMessage="Select Task"
                            />
                          }
                          isDisabled={currentRisk.isArchive || !canEditRiskTask}
                          styles={{ marginBottom: 0 }}
                        />
                      </Grid>
                    ) : null}
                  </Grid>
                  <div
                    className={classes.descriptionEditor}
                    style={{ pointerEvents: isArchivedSelected ? "none" : "all" }}>
                    <RiskDescriptionEditor
                      type={
                        <FormattedMessage
                          id="common.description.detail-label"
                          defaultMessage="Details"
                        />
                      }
                      keyType="detail"
                      currentRisk={currentRisk}
                      isUpdated={this.props.isUpdated}
                      descriptionPermission={canEditRiskDescription}
                      view="risk"
                      isArchivedSelected={isArchivedSelected}
                    />
                  </div>
                  <div
                    className={classes.descriptionEditor}
                    style={{ pointerEvents: isArchivedSelected ? "none" : "all" }}>
                    <RiskDescriptionEditor
                      type={
                        <FormattedMessage
                          id="risk.detail-dialog.mitigation.label"
                          defaultMessage="Mitigation Plan"
                        />
                      }
                      keyType="mittigation"
                      currentRisk={currentRisk}
                      isUpdated={this.props.isUpdated}
                      mittigationPermission={canEditMitigationPlan}
                      view="risk"
                    />{" "}
                  </div>

                  <UpdateMatrixDialog
                    saveOption={this.customFieldChange}
                    open={this.state.assessmentDialogOpen}
                    feature={"risk"}
                    closeAction={() => this.handleAddAssessmentDialog(false)}
                    selectedValues={
                      assessmentMatrixValues ? assessmentMatrixValues.fieldData.data : []
                    }
                    assessMatrix={true}
                  />
                  {selectedMatrixObj && (
                    <>
                      <div className={classes.assessmentGraphHeadingCnt}>
                        <Typography
                          variant="heading3"
                          style={{ fontSize: theme.typography.fontFamilyLato }}>
                          Assessment
                        </Typography>
                        <CustomButton
                          variant="contained"
                          btnType="success"
                          onClick={() => this.handleAddAssessmentDialog(true)}
                          disabled={!customFieldPermission.isAllowEdit || isArchivedSelected}>
                          Add Assessment
                        </CustomButton>
                      </div>
                      <div style={{ height: 250 }}>
                        <MyResponsiveLine
                          data={assessmentChartData}
                          chartProps={{ pointSize: selectedMatrixObj.settings.gridSize }}
                          yScale={{ max: selectedMatrixObj.settings.gridSize, min: 1 }}
                          xScale={{ max: selectedMatrixObj.settings.gridSize, min: 1 }}
                          axisLeft={{
                            legend: selectedMatrixObj && selectedMatrixObj.settings.columns.title,
                            legendOffset: -30,
                            legendPosition: "middle",
                          }}
                          axisBottom={{
                            legend: selectedMatrixObj && selectedMatrixObj.settings.rows.title,
                            legendOffset: 35,
                            legendPosition: "middle",
                          }}
                        />
                      </div>
                    </>
                  )}

                  {/* <CustomFieldsChecklistView
                    groupType={"risk"}
                    groupId={currentRisk.id}
                    handleClickHideOption={this.handleClickHideOption}
                    permission={customFieldPermission}
                    isSystem={true}
                    isArchivedSelected={isArchivedSelected}
                  />

                  <CustomFieldsChecklistView
                    groupType={"risk"}
                    groupId={currentRisk.id}
                    handleClickHideOption={this.handleClickHideOption}
                    permission={customFieldPermission}
                    isSystem={false}
                    isArchivedSelected={isArchivedSelected}
                  />

                  <CustomFieldsDropdownsView
                    groupType={"risk"}
                    disabled={currentRisk.isArchive}
                    permission={customFieldPermission.isAllowEdit}
                    customFieldChange={this.customFieldChange}
                    customFieldData={currentRisk.customFieldData}
                  />

                  <CustomFieldsText
                    groupType={"risk"}
                    disabled={currentRisk.isArchive}
                    permission={customFieldPermission.isAllowEdit}
                    customFieldChange={this.customFieldChange}
                    customFieldData={currentRisk.customFieldData}
                  />
                  <CustomFieldsEmailView
                    groupType={"risk"}
                    disabled={currentRisk.isArchive}
                    permission={customFieldPermission.isAllowEdit}
                    customFieldChange={this.customFieldChange}
                    customFieldData={currentRisk.customFieldData}
                  />
                  <CustomFieldsNumberView
                    groupType={"risk"}
                    disabled={currentRisk.isArchive}
                    permission={customFieldPermission.isAllowEdit}
                    customFieldChange={this.customFieldChange}
                    customFieldData={currentRisk.customFieldData}
                  />
                  <CustomFieldsAddressView
                    groupType={"risk"}
                    disabled={currentRisk.isArchive}
                    permission={customFieldPermission.isAllowEdit}
                    customFieldChange={this.customFieldChange}
                    customFieldData={currentRisk.customFieldData}
                  />
                  <CustomFieldsWebSiteView
                    groupType={"risk"}
                    disabled={currentRisk.isArchive}
                    permission={customFieldPermission.isAllowEdit}
                    customFieldChange={this.customFieldChange}
                    customFieldData={currentRisk.customFieldData}
                  />
                  <CustomFieldsContactView
                    groupType={"risk"}
                    disabled={currentRisk.isArchive}
                    permission={customFieldPermission.isAllowEdit}
                    customFieldChange={this.customFieldChange}
                    customFieldData={currentRisk.customFieldData}
                  /> */}

                  {/* {customSections.map((section, secIndex) => {
                    return (
                      <div key={secIndex}>
                        <div className={classes.systemFieldsHeading} style={{ background : section.settings.color}}>
                          <Typography variant="h3">{section.fieldName}</Typography>
                          <div>
                            <>
                              <SvgIcon viewBox="0 0 12 11.957" className={classes.editIcon}>
                                <IconEditSmall />
                              </SvgIcon>
                              <SvgIcon viewBox="0 5 25 12" className={classes.documentIcon}>
                                <IconColor />
                              </SvgIcon>
                              {showsystemfield ? (
                                <RemoveIcon
                                  className={classes.systemFieldRemoveIcon}
                                  // onClick={this.handleShowSystemField}
                                />
                              ) : (
                                <AddIcon
                                  className={classes.systemFieldAddIcon}
                                  // onClick={this.handleShowSystemField}
                                />
                              )}
                            </>
                          </div>
                        </div>
                      </div>
                    );
                  })} */}

                  {/*if bussiness plan then show add custom field component*/}
                  {teamCanView("customFieldAccess") && (
                    <div className={classes.addCustomFieldCnt}>
                      <CustomFieldView
                        feature="risk"
                        // onFieldAdd={this.addCustomFieldCallback}
                        onFieldEdit={this.editCustomFieldCallback}
                        onFieldDelete={() => {}}
                        permission={customFieldPermission}
                        disableAssessment={false}
                        disabled={currentRisk.isArchive}
                      />
                    </div>
                  )}
                </div>
              </Scrollbars>
            </Grid>
            <Grid item xs={5}>
              <SideTabs
                MenuData={currentRisk}
                members={allMembers}
                permission={permission}
                riskActivitiesData={this.props.riskActivities}
                riskPer={currentRisk.riskPermission}
                onChatMount={this.onChatMount}
                onChatUnMount={this.onChatUnMount}
                chatConfig={chatConfig}
                chatPermission={chatPermission}
                docConfig={chatConfig}
                docPermission={docPermission}
                selectedTab={0}
                intl={intl}
              />
              {/* // } */}
            </Grid>
          </Grid>
          {unArchiveFlag ? (
            <ActionConfirmation
              open={unArchiveFlag}
              closeAction={this.handleDialogClose}
              cancelBtnText="Cancel"
              successBtnText="Unarchive"
              alignment="center"
              iconType="unarchive"
              headingText="Unarchive"
              msgText={`Are you sure you want to unarchive this risk?`}
              successAction={this.handleUnArchived}
              btnQuery={unarchiveBtnQuery}
            />
          ) : null}
        </DialogContent>
      </Dialog>
    );
  }
}

RiskDetails.defaultProps = {
  updatingSelectedColumn: () => {},
  filterRisk: () => {},
};

const mapStateToProps = (state, ownProps) => {
  return {
    userId: state.profile.data.userId || "",
    profile: state.profile.data,
    allMembers: state.profile.data.member.allMembers || [],
    riskNotifications: state.riskNotifications.data || [],
    workspacePermissionsState: state.workspacePermissions,
    riskActivities: state.riskActivities.data || [],
    tasks: state.tasks.data,
    riskPermission: state.workspacePermissions.data.risk,
    riskPer: state.workspacePermissions.data.risk,
    customFields: state.customFields,
    risksState: state.risks,
    itemOrderState: state.itemOrder,
  };
};

export default compose(
  withRouter,
  injectIntl,
  withSnackbar,
  withStyles(taskDetailStyles, { withTheme: true }),
  connect(mapStateToProps, {
    CopyTask,
    DeleteTask,
    ArchiveTask,
    UnArchiveTask,
    UpdateCalenderTask,
    UpdateRisk,
    DeleteRisk,
    UnarchiveRisk,
    ArchiveRisk,
    MarkRiskNotificationsAsRead,
    dispatchRisk,
    bulkUpdateRisk,
    SaveItemOrder,
    editCustomField,
    updateCustomFieldData,
  })
)(RiskDetails);
