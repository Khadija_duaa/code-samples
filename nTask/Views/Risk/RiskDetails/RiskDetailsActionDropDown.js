import React, { Component, Fragment } from "react";
import { withStyles, withTheme } from "@material-ui/core/styles";
import itemStyles from "./styles";
import menuStyles from "../../../assets/jss/components/menu";
import CustomIconButton from "../../../components/Buttons/IconButton";
import SelectionMenu from "../../../components/Menu/SelectionMenu";
import { BlockPicker } from "react-color";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import combineStyles from "../../../utils/mergeStyles";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import RightArrow from "@material-ui/icons/ArrowRight";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import DeleteConfirmDialog from "../../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import { FormattedMessage } from "react-intl";

class TaskDetailsActionDropDown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      pickerOpen: false,
      pickerPlacement: "",
      archiveFlag: false,
      unArchiveFlag: false,
      deleteFlag: false,
      btnQuery: "",
      archiveBtnQuery: "",
      unarchiveBtnQuery: "",
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleColorClick = this.handleColorClick.bind(this);
    this.handlePickerClose = this.handlePickerClose.bind(this);
    this.handleCustomColor = this.handleCustomColor.bind(this);
    this.handleDialogClose = this.handleDialogClose.bind(this);
  }
  componentDidMount = () => {
    this.setState({
      selectedColor: this.props.selectedColor,
    });
  };

  handleCustomColor(event) {
    let obj = Object.assign({}, this.props.task);
    obj.colorCode = event.hex;
    delete obj.CalenderDetails;

    this.props.UpdateTaskColor(obj, data => {
      this.props.UpdateCalender(data);
    });
  }
  handleOperations(value) {
    switch (value) {
      case "Copy":
        this.props.CopyTask(this.props.task);
        break;
      case "Public Link":
        break;
      case "Archive":
        this.setState({ archiveFlag: true, popupFlag: true });
        break;
      case "Unarchive":
        this.setState({ unArchiveFlag: true, popupFlag: true });
        break;
      case "Delete":
        this.setState({ deleteFlag: true, popupFlag: true });
        break;
    }

    this.setState({ open: false, pickerOpen: false });
  }
  handleClose(event) {
    this.setState({ open: false, pickerOpen: false });
  }
  handlePickerClose(event) {
    this.setState({ pickerOpen: false });
  }
  handleDialogClose() {
    this.setState(
      {
        popupFlag: false,
        archiveFlag: false,
        unArchiveFlag: false,
        deleteFlag: false,
      },
      () => {
        if (this.props.closeTaskDetailsPopUp) this.props.closeTaskDetailsPopUp();
      }
    );
  }

  deleteRisk = () => {
    this.setState({ btnQuery: "progress" }, () => {
      this.props.DeleteTask(
        this.props.task,
        response => {
          this.setState({ btnQuery: "", open: false });
        },
        error => {
          this.setState({ btnQuery: "", open: false });
          if (error) {
            // self.showSnackBar('Server throws error','error');
          }
        }
      );
    });
  };

  handleClick(event, placement) {
    const { currentTarget } = event;
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }
  handleColorClick(event) {
    if (event.target.closest("#colorPickerCnt") == null) {
      this.setState(prevState => ({ pickerOpen: !prevState.pickerOpen }));
    }
  }

  colorChange = (color, task) => {
    let self = this;
    this.setState({ selectedColor: color.hex }, function () {
      let obj = Object.assign({}, task);
      this.setState({
        selectedColor: color.hex,
      });
      obj.colorCode = color.hex;
      delete obj.CalenderDetails;
      self.props.UpdateTaskColorFromGridItem(obj, data => {
        self.props.UpdateCalenderTask(data, () => { });
      });
    });
  };

  handleArchive = e => {
    if (e) e.stopPropagation();
    const { id } = this.props.risk || "";
    const obj = { id, archive: true };
    this.setState({ archiveBtnQuery: "progress" }, () => {
      this.props.isUpdated(obj, () => {
        this.setState({ archiveBtnQuery: "", archiveFlag: false }, () => {
          if (this.props.closeActionMenu) this.props.closeActionMenu();
        });
      });
    });
  };

  handleUnArchive = e => {
    if (e) e.stopPropagation();
    const { id } = this.props.risk || "";
    const obj = { id, unarchive: true };
    this.setState({ unarchiveBtnQuery: "progress" }, () => {
      this.props.isUpdated(obj, () => {
        this.setState({ unarchiveBtnQuery: "", unArchiveFlag: false }, () => {
          if (this.props.closeActionMenu) this.props.closeActionMenu();
        });
      });
    });
  };
  getTranslatedId(value) {
    switch (value) {
      case "Copy":
        value = "common.action.copy.label";
        break;
      case "Public Link":
        break;
      case "Archive":
        value = "common.action.archive.label";
        break;
      case "Unarchive":
        value = "common.action.un-archive.label";
        break;
      case "Delete":
        value = "common.action.delete.label";
        break;
    }
    return value;
  }
  render() {
    const { classes, theme } = this.props;
    const {
      open,
      placement,
      pickerOpen,
      popupFlag,
      selectedColor,
      btnQuery,
      archiveBtnQuery,
      unarchiveBtnQuery,
    } = this.state;
    const ddData = ["Copy", "Color", "Public Link", "Archive", "Delete"];

    return (
      <Fragment>
        <ClickAwayListener onClickAway={this.handleClose}>
          <div>
            <CustomIconButton
              btnType="condensed"
              onClick={event => {
                this.handleClick(event, "bottom-end");
              }}
              buttonRef={node => {
                this.anchorEl = node;
              }}>
              <MoreVerticalIcon
                htmlColor={theme.palette.secondary.medDark}
                style={{ fontSize: "24px" }}
              />
            </CustomIconButton>

            <SelectionMenu
              open={open}
              closeAction={this.handleClose}
              placement={placement}
              anchorRef={this.anchorEl}
              list={
                <List>
                  <ListItem disableRipple={true} classes={{ root: classes.menuHeadingItem }}>
                    <ListItemText
                      primary={
                        <FormattedMessage id="common.action.label" defaultMessage="Select Action" />
                      }
                      classes={{ primary: classes.menuHeadingListItemText }}
                    />
                  </ListItem>
                  {ddData.map(value =>
                    value == "Color" ? (
                      <ListItem
                        key={value}
                        button
                        disableRipple={true}
                        classes={{
                          root: classes.selectColorMenuItem,
                          selected: classes.statusMenuItemSelected,
                        }}
                        onClick={event => {
                          this.handleColorClick(event);
                        }}>
                        <ListItemText
                          primary={
                            <FormattedMessage id="common.action.color.label" defaultMessage={value} />
                          }
                          classes={{
                            primary: classes.statusItemText,
                          }}
                        />
                        <RightArrow
                          htmlColor={theme.palette.secondary.dark}
                          classes={{ root: classes.submenuArrowBtn }}
                        />
                        <div
                          id="colorPickerCnt"
                          className={classes.colorPickerCntLeft}
                          style={pickerOpen ? { display: "block" } : { display: "none" }}>
                          <BlockPicker
                            triangle="hide"
                            color={selectedColor}
                            onChangeComplete={event => {
                              this.colorChange(event, this.props.task);
                            }}
                            colors={[
                              "#D9E3F0",
                              "#F47373",
                              "#697689",
                              "#37D67A",
                              "#2CCCE4",
                              "#555555",
                              "#dce775",
                              "#ff8a65",
                              "#ba68c8",
                              "#ffffff",
                            ]}
                          />
                        </div>
                      </ListItem>
                    ) : (
                      <ListItem
                        key={
                          value === "Archive"
                            ? this.props.task.isDeleted === true
                              ? "Unarchive"
                              : "Archive"
                            : value
                        }
                        button
                        disableRipple={true}
                        classes={{ selected: classes.statusMenuItemSelected }}
                        onClick={this.handleOperations.bind(
                          this,
                          value === "Archive"
                            ? this.props.task.isDeleted === true
                              ? "Unarchive"
                              : "Archive"
                            : value
                        )}>
                        <ListItemText
                          primary={
                            value === "Archive" ? (
                              this.props.task.isDeleted === true ? (
                                <FormattedMessage
                                  id={this.getTranslatedId("Unarchive")}
                                  defaultMessage="Unarchive"
                                />
                              ) : (
                                <FormattedMessage
                                  id={this.getTranslatedId("Archive")}
                                  defaultMessage="Archive"
                                />
                              )
                            ) : (
                              <FormattedMessage
                                id={this.getTranslatedId(value)}
                                defaultMessage={value}
                              />
                            )
                          }
                          classes={{
                            primary: classes.statusItemText,
                          }}
                        />
                      </ListItem>
                    )
                  )}
                </List>
              }
            />
          </div>
        </ClickAwayListener>
        <React.Fragment>
          {this.state.archiveFlag ? (
            <ActionConfirmation
              open={popupFlag}
              closeAction={this.handleDialogClose}
              cancelBtnText={
                <FormattedMessage
                  id="common.action.archive.confirmation.cancel-button.label"
                  defaultMessage="Cancel"
                />
              }
              successBtnText={
                <FormattedMessage
                  id="common.action.archive.confirmation.archive-button.label"
                  defaultMessage="Archive"
                />
              }
              alignment="center"
              iconType="archive"
              headingText="Archive"
              msgText={`Are you sure you want to archive this task?`}
              successAction={this.handleArchive}
              btnQuery={archiveBtnQuery}
            />
          ) : null}
        </React.Fragment>
        <React.Fragment>
          {this.state.unArchiveFlag ? (
            <ActionConfirmation
              open={popupFlag}
              closeAction={this.handleDialogClose}
              cancelBtnText={
                <FormattedMessage
                  id="common.action.un-archive.confirmation.cancel-button.label"
                  defaultMessage="Cancel"
                />
              }
              successBtnText={
                <FormattedMessage
                  id="common.action.un-archive.confirmation.un-archive-button.label"
                  defaultMessage="Unarchive"
                />
              }
              alignment="center"
              iconType="unarchive"
              headingText="Unarchive"
              msgText={`Are you sure you want to unarchive this task?`}
              successAction={this.handleUnArchive}
              btnQuery={unarchiveBtnQuery}
            />
          ) : null}
        </React.Fragment>
        <React.Fragment>
          {this.state.deleteFlag ? (
            <DeleteConfirmDialog
              open={popupFlag}
              closeAction={this.handleDialogClose}
              cancelBtnText={
                <FormattedMessage
                  id="common.action.delete.confirmation.cancel-button.label"
                  defaultMessage="Cancel"
                />
              }
              successBtnText={
                <FormattedMessage
                  id="common.action.delete.confirmation.delete-button.label"
                  defaultMessage="Delete"
                />
              }
              alignment="center"
              headingText={
                <FormattedMessage
                  id="common.action.delete.confirmation.title"
                  defaultMessage="Delete"
                />
              }
              successAction={this.deleteRisk}
              msgText={`Are you sure you want to delete this task?`}
              btnQuery={btnQuery}
            />
          ) : null}
        </React.Fragment>
      </Fragment>
    );
  }
}

export default withStyles(combineStyles(itemStyles, menuStyles), {
  withTheme: true,
})(TaskDetailsActionDropDown);
