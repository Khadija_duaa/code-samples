import React, { Component } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import taskDetailStyles from "./styles";
import "../../../assets/css/richTextEditor.css";

import withStyles from "@material-ui/core/styles/withStyles";
import { injectIntl } from "react-intl";
import DescriptionEditor from "../../../components/TextEditor/CustomTextEditor/DescriptionEditor/DescriptionEditor";
import MittigationEditor from "../../../components/TextEditor/CustomTextEditor/MittigationEditor/DescriptionEditor";
import CustomFieldLabelCmp from "../../../components/CustomFieldsViews/CustomFieldLabel.cmp";

class RiskDescriptionEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editorDetail: props.currentRisk.detail || "",
      editorMittigation: props.currentRisk.mittigation || "",
    };
  }
  componentDidMount() {
    let members =
      this.props.profileState && this.props.profileState.data && this.props.profileState.data.member
        ? this.props.profileState.data.member.allMembers
        : [];
    let memberList = members.map(x => {
      return {
        text: x.userName,
        value: x.userName,
        //  url: x.userName,
        userId: x.userId,
      };
    });
    this.setState({ members: memberList });
  }
  componentDidUpdate(prevProps) {
    if (JSON.stringify(prevProps.currentRisk) !== JSON.stringify(this.props.currentRisk)) {
      this.setState({
        editorDetail: this.props.currentRisk.detail || "",
        editorMittigation: this.props.currentRisk.mittigation || "",
      })
    }
  }
  handleRiskDescription = editorDetail => {
    this.setState({
      editorDetail: editorDetail.html,
    });
  };
  handleClickAwayRiskDescription = () => {
    const { currentRisk, keyType } = this.props;
    const { editorDetail } = this.state;
    if (editorDetail != currentRisk.detail) {
      currentRisk.detail = editorDetail;
      this.props.handleUpdateDetails(currentRisk, keyType, editorDetail);
      // this.props.isUpdated(currentRisk, response => { });
    }
  };
  handleMittigationDescription = editorMittigation => {
    this.setState({
      editorMittigation: editorMittigation.html,
    });
  };
  handleClickAwayMittigationDescription = () => {
    const { currentRisk, keyType } = this.props;
    const { editorMittigation } = this.state;
    if (editorMittigation != currentRisk.mittigation) {
      currentRisk.mittigation = editorMittigation;
      this.props.handleUpdateDetails(currentRisk, keyType, editorMittigation);
      // this.props.isUpdated(currentRisk, response => { });
    }
  };

  render() {
    const { classes, theme, id, intl, isArchivedSelected = false, type, customfieldlabelProps = {} } = this.props;
    const { editorDetail, editorMittigation } = this.state;

    return (
      <div className={classes.editorContainer}>
        <CustomFieldLabelCmp
          label={type}
          iconType={"textarea"}
          handleDelete={() => { }}
          handleEdit={() => { }}
          handleSelectHideOption={() => { }}
          {...customfieldlabelProps}
        />
        {this.props.keyType == "detail" ? (
          <div style={{ flex: 1, padding: "0px 10px 0px 4px" }}>
            <DescriptionEditor
              id="RiskDescriptionEditor"
              defaultValue={editorDetail}
              onChange={this.handleRiskDescription}
              handleClickAway={this.handleClickAwayRiskDescription}
              placeholder={"Enter here"}
              disableEdit={isArchivedSelected}
            />
          </div>
        ) : (
          <div style={{ flex: 1, padding: "0px 10px 0px 4px" }}>
            <MittigationEditor
              id="RiskMitigationEditor"
              defaultValue={editorMittigation}
              onChange={this.handleMittigationDescription}
              handleClickAway={this.handleClickAwayMittigationDescription}
              placeholder={"Enter here"}
              disableEdit={isArchivedSelected}
            />
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    profileState: state.profile,
  };
};

export default compose(
  withRouter,
  injectIntl,
  withStyles(taskDetailStyles, { withTheme: true }),
  connect(mapStateToProps, {})
)(RiskDescriptionEditor);
