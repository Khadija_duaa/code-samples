import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import taskDetailsStyles from "./styles";
import ActivityList from "./RiskActivity";
import CommentsTab from "./CommentsTab";
import { Scrollbars } from "react-custom-scrollbars";
import { FormattedMessage, injectIntl } from "react-intl";
import ChatUpdates from "../../../components/Chat/ChatUpdates";
import Documents from "../../../components/Documents/Documents";
import {
  clearDocuments
} from "../../../redux/actions/documents";

class SideTabs extends React.Component {
  state = {
    value: 0,
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };
  componentWillUnmount() {
    this.props.clearDocuments();//clear Documents
  }
  render() {
    const { classes, riskNotificationsData, riskPer } = this.props;
    const intl = this.props.intl;
    const { value } = this.state;

    return (
      <Fragment>
        <Tabs
          value={value}
          onChange={this.handleChange}
          variant="fullWidth"
          classes={{ root: classes.TabsRoot, indicator: classes.tabIndicator }}>
          <Tab
            disableRipple={true}
            classes={{ root: classes.tab, wrapper: classes.tabLabelCnt, selected: classes.tabSelected }}
            label={<FormattedMessage id="common.comment.label" defaultMessage="Comments" />}
          />
          <Tab
            disableRipple={true}
            classes={{
              root: classes.tab,
              wrapper: classes.tabLabelCnt,
              selected: classes.tabSelected
            }}
            label={<FormattedMessage id="project.dev.documents.label" defaultMessage="Documents" />}
          />
          <Tab
            disableRipple={true}
            classes={{ root: classes.tab, wrapper :  classes.tabLabelCnt, selected: classes.tabSelected }}
            label={<FormattedMessage id="activity-log.detail-label" defaultMessage="Activity" />}
          />
        </Tabs>
        {value === 0 && (
          <div className={`${classes.TabContentCnt} ${classes.commentsTabCnt}`}>           
            <ChatUpdates
              onMount={this.props.onChatMount}
              onUnmount={this.props.onChatUnMount}
              chatConfig={this.props.chatConfig}
              chatPermission={this.props.chatPermission}
              selectedTab={0}
              intl={intl}
            />
          </div>
        )}
        {value === 1 && (
          <div className={`${classes.TabContentCnt} ${classes.commentsTabCnt}`}>
            <Documents
              onMount={this.props.onChatMount}
              onUnmount={this.props.onChatUnMount}
              docConfig={this.props.docConfig}
              docPermission={this.props.docPermission}
              intl={intl}
            />
          </div>
        )}
        {value === 2 && (
          <div className={classes.TabContentCntActivity}>
                <ActivityList
                  MenuData={this.props.MenuData}
                  members={this.props.members}
                  // riskActivitiesData={this.props.riskActivitiesData}
                  riskActivitiesData={this.props.MenuData.activityLog}
                  intl={intl}
                />
          </div>
        )}
      </Fragment>
    );
  }
}
const mapStateToProps = state => {
  return {
  };
};

export default compose(withStyles(taskDetailsStyles),  connect(mapStateToProps, {
  
  clearDocuments,
}), injectIntl)(SideTabs);
