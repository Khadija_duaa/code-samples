import intersectionWith from "lodash/intersectionWith";
import findIndex from "lodash/findIndex";

export function getFilteredRisks(
  RiskArray,
  multipleFilters,
  riskState,
  tasks,
  riskPer,
  projects
) {
  let risks = JSON.parse(JSON.stringify(RiskArray)) || [];
  const filter = [
    "Critical",
    "Major",
    "Minor",
    "Moderate",
    "Critical",
    "High",
    "Medium",
    "Low",
  ];

  const status = ["Identified", "In Review", "Agreed", "Rejected"];
  let isStatusSelected = false;
  let islikelihoodSelected = false;
  const likelihood = ["0-25", "26-50", "51-75", "76-100"];

  let severity = [],
    priority = [],
    flag = 0;
  if (riskState && riskState.length) {
    riskState.map((x) => {
      if (status.indexOf(x) >= 0) isStatusSelected = true;
      if (likelihood.indexOf(x) >= 0) islikelihoodSelected = true;
      if (x - 1 < 4) severity.push(filter[x - 1]);
      else priority.push(filter[x - 1]);
    });
  }
  let newRisks = [],
    isArchived =
      riskState && riskState.length
        ? riskState.indexOf("Archived") > -1
        : false,
    start = "",
    end = "",
    compare = false;

  // Filtering Archived
  if (!isArchived) {
    const filtered = risks.filter((x) => {
      if (x.isArchive === false) {
        return x;
      }
    });
    risks = filtered;
  }

  if (multipleFilters) {
    let value = risks || [];
    if (multipleFilters.owner && multipleFilters.owner.length > 0) {
      flag = 1;
      value = risks.filter((x) => {
        if (multipleFilters.owner.indexOf(x.riskOwner) > -1) return x;
      });
    }
    if (multipleFilters.createdBy && multipleFilters.createdBy.length > 0) {
      value = value.length >= 0 && flag === 1 ? value : risks;
      flag = 1;
      value = value.filter((x) => {
        if (multipleFilters.createdBy.indexOf(x.createdBy) > -1) return x;
      });
    }

    if (multipleFilters.status && multipleFilters.status.length > 0) {
      value = value.length >= 0 && flag === 1 ? value : risks;
      flag = 1;
      value = value.filter((x) => {
        let riskStatusCheck = multipleFilters.status.findIndex((item) => {
          return item == x.status;
        });
        if (riskStatusCheck >= 0) {
          return x;
        }
      });
    }
    if (multipleFilters.impact && multipleFilters.impact.length > 0) {
      value = value.length >= 0 && flag === 1 ? value : risks;
      flag = 1;
      value = value.filter((x) => {
        let riskImpactCheck = multipleFilters.impact.findIndex((item) => {
          return item == x.impact;
        });
        if (riskImpactCheck >= 0) {
          return x;
        }
      });
    }
    if (multipleFilters.likeHood && multipleFilters.likeHood.length > 0) {
      value = value.length >= 0 && flag === 1 ? value : risks;
      flag = 1;
      value = value.filter((x) => {
        let riskLikelihoodCheck = multipleFilters.likeHood.findIndex((item) => {
          return item == `${x.likelihood}`;
        });
        if (riskLikelihoodCheck >= 0) {
          return x;
        }
      });
    }
    if (multipleFilters.task && multipleFilters.task.length > 0) {
      value = value.length >= 0 && flag === 1 ? value : risks;
      flag = 1;
      value = value.filter((x) => {
        if (
          intersectionWith(
            multipleFilters.task,
            x.linkedTasks,
            (x, y) => x === y
          ).length > 0
        ) {
          return x;
        }
      });
    }

    flag = 0;
    risks = value;
  } else if (riskState && riskState.length) {
    newRisks = risks.concat();
    isArchived = riskState.indexOf("Archived") >= 0;

    // newRisks = risks.filter(x => {
    //     if (riskState.indexOf(x.likelihood) >= 0) {
    //         return x;
    //     }
    //     if (riskState.indexOf(x.status) >= 0) {
    //         return x;
    //     }
    //     if(isArchived && x.isArchive){
    //         return x;
    //     }
    // });

    if (isStatusSelected)
      newRisks = newRisks.filter((x) => {
        if (riskState.indexOf(x.status) >= 0) {
          return x;
        }
      });

    if (islikelihoodSelected)
      newRisks = newRisks.filter((x) => {
        if (riskState.indexOf(x.likelihood) >= 0) {
          return x;
        }
      });

    if (isArchived)
      newRisks = newRisks.filter((x) => {
        if (isArchived && x.isArchive) {
          return x;
        }
      });

    risks = newRisks;
  }

  return risks;
}
