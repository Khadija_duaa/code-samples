import React, { Component } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { SaveItemOrder } from "../../../redux/actions/itemOrder";
import Drawer from "@material-ui/core/Drawer";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import { withStyles } from "@material-ui/core/styles";
import FilterDropdown from "./FilterDropdown";
import Grid from "@material-ui/core/Grid";
import DefaultSwitch from "../../../components/Form/Switch";
import DefaultButton from "../../../components/Buttons/DefaultButton";
import CustomButton from "../../../components/Buttons/CustomButton";
import { Scrollbars } from "react-custom-scrollbars";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import HelpIcon from "@material-ui/icons/HelpOutline";
import BackArrow from "@material-ui/icons/KeyboardArrowLeft";
import IconButton from "../../../components/Buttons/IconButton";
import ReactResizeDetector from "react-resize-detector";
import DefaultTextField from "../../../components/Form/TextField";
import SvgIcon from "@material-ui/core/SvgIcon";
import PushPin from "../../../components/Icons/PushPin";
import SelectIconMenu from "../../../components/Menu/TaskMenus/SelectIconMenu";
import isEqual from "lodash/isEqual";
import cloneDeep from "lodash/cloneDeep";
import { setAppliedFilters } from "../../../redux/actions/appliedFilters";
import { validateGenericField } from "../../../utils/formValidations";
import SelectSearchDropdown from "../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { generateSelectData } from "../../../helper/generateSelectData";
import { generateAssigneeData } from "../../../helper/generateSelectData";
import { getCustomFields } from "../../../helper/customFieldsData";

import {
  calculateFilterContentHeight,
  calculateHeaderHeight,
} from "../../../utils/common";
import RingIcon from "@material-ui/icons/RadioButtonUnchecked";
import RoundIcon from "@material-ui/icons/Brightness1";
import {
  statusData,
  impactData,
  likelihoodData,
} from "../../../helper/riskDropdownData";
import { FormattedMessage, injectIntl } from "react-intl";

const drawerWidth = 350;

const styles = (theme) => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
    padding: "43px 0 0 0",
    background: theme.palette.common.white,
    position: "fixed",
    top: 108,
    display: "flex",
    height: "100%",
    zIndex: 100,
    flexDirection: "row",
    justifyContent: "stretch",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    overflowX: "hidden",
    borderTop: 0,
    borderBottom: 0,
  },
  drawerHeader: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
  },
  filterFormCnt: {
    padding: "10px 20px 40px 20px",
  },
  switchCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    paddingLeft: 12,
  },
  switchLabel: {
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    margin: 0,
    marginRight: 30,
    fontWeight: theme.typography.fontWeightRegular,
    display: "flex",
  },
  filterBottomBtns: {
    padding: "10px 20px",
    borderTop: `1px solid ${theme.palette.border.lightBorder}`,
    marginTop: 40,
    position: "fixed",
    bottom: 130,
    background: theme.palette.common.white,
    zIndex: 1,
  },
  filterItem: {
    // marginTop: 20
  },
  filterCntHeader: {
    background: "#404040",
    padding: "12px 15px",
    position: "fixed",
    top: 0,
    left: 0,
    right: 0,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  filterHeaderClearBtn: {
    textDecoration: "underline",
    color: theme.palette.common.white,
    fontSize: "12px !important",
    cursor: "pointer",
  },
  filterHeaderTextCnt: {
    display: "flex",
    alignItems: "center",
  },
  filtersHeaderText: {
    marginLeft: 10,
    color: theme.palette.common.white,
    fontWeight: theme.typography.fontWeightLight,
    fontSize: "16px !important",
  },
  filtersHeaderCloseIcon: {
    fontSize: "11px !important",
    color: "#404040",
  },
  pushPinIcon: {
    fontSize: "14px !important",
    marginRight: 5,
  },
  filtersHeaderHelpIcon: {
    fontSize: "14px !important",
    marginLeft: 5,
  },
  impactIcon: {
    fontSize: "14px !important",
    marginRight: 5,
  },
  statusIcon: {
    fontSize: "11px !important",
    marginRight: 5,
  },
});
class RiskFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterCntHeight: `${calculateFilterContentHeight()}px`,
      saveFilter: false,
      isClear: false,
      isDisable: true,
      isSaveDisable: true,
      loggedInTeam: this.props.profileState.data.loggedInTeam,
      filterId: ``,
      category: [],
      owner: [],
      priority: null,
      recurrence: false,
      status: [],
      impact: [],
      createdBy: [],
      risk: [],
      likeHood: [],
      filterName: "",
      defaultFilter: false,
    };
    this.initialState = this.state;

    this.handleSwitch = this.handleSwitch.bind(this);
    this.onResize = this.onResize.bind(this);
    this.handleInput = this.handleInput.bind(this);
    this.handleSaveFilter = this.handleSaveFilter.bind(this);
    this.handleBackFilter = this.handleBackFilter.bind(this);
    this.handleFilterChange = this.handleFilterChange.bind(this);
    this.handleClearState = this.handleClearState.bind(this);
    this.handleSearchFilter = this.handleSearchFilter.bind(this);
  }
  componentDidMount() {
    // For handling default risk filter
    const { Risk } = this.props.appliedFiltersState;
    if (Risk) {
      this.setState({ ...this.initialState, ...this.transformObject() });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { Risk } = this.props.appliedFiltersState;
    if (!isEqual(prevProps.appliedFiltersState.Risk, Risk)) {
      if (prevProps.appliedFiltersState.Risk && Risk === null)
        this.setState({ ...this.initialState, ...Risk });
      else {
        this.setState({
          ...this.initialState,
          isSaveDisable: false,
          ...this.transformObject(),
        });
      }
    }
  }

  transformObject = () => {
    const { Risk } = this.props.appliedFiltersState;
    const { theme, classes, members, intl } = this.props;
    let riskObj = cloneDeep(Risk);

    if (riskObj.owner) {
      const ownerData = generateAssigneeData(members);
      riskObj.owner = ownerData.filter((a) => {
        return riskObj.owner.indexOf(a.id) > -1;
      });
    }

    if (riskObj.createdBy && riskObj.createdBy.length) {
      const createdByData = generateAssigneeData(members);
      riskObj.createdBy = createdByData.filter((c) => {
        return riskObj.createdBy.indexOf(c.id) > -1;
      });
    }
    if (riskObj.risk && riskObj.risk.length) {
      const riskData = this.generateRiskData();
      riskObj.risk = riskData.filter((t) => {
        return riskObj.risk.indexOf(t.id) > -1;
      });
    }

    if (riskObj.status && riskObj.status.length) {
      riskObj.status = statusData(theme, classes, intl).filter((s) => {
        return riskObj.status.indexOf(s.value) > -1;
      });
    }

    if (riskObj.impact && riskObj.impact.length) {
      riskObj.impact = impactData(theme, classes, intl).filter((i) => {
        return riskObj.impact.indexOf(i.value) > -1;
      });
    }
    if (riskObj.likeHood && riskObj.likeHood.length) {
      riskObj.likeHood = likelihoodData(intl).filter((i) => {
        return riskObj.likeHood.indexOf(i.value) > -1;
      });
    }

    return riskObj;
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.loggedInTeam !== nextProps.profileState.data.loggedInTeam) {
      return { loggedInTeam: nextProps.profileState.data.loggedInTeam };
    } else {
      return { loggedInTeam: prevState.loggedInTeam };
    }
  }

  handleSearchFilter() {
    const {
      filterCntHeight,
      saveFilter,
      isClear,
      isDisable,
      isSaveDisable,
      loggedInTeam,
      category,
      ...rest
    } = this.state;

    let riskObj = cloneDeep(rest); 
    if (riskObj.owner.length) {
      riskObj.owner = riskObj.owner.map((p) => {
        return p.id;
      });
    }

    if (riskObj.createdBy.length) {
      riskObj.createdBy = riskObj.createdBy.map((c) => {
        return c.id;
      });
    }

    if (riskObj.risk.length) {
      riskObj.risk = riskObj.risk.map((t) => {
        return t.id;
      });
    }
    if (riskObj.status && riskObj.status.length) {
      riskObj.status = riskObj.status.map((s) => {
        return s.value;
      });
    }

    if (riskObj.impact && riskObj.impact.length) {
      riskObj.impact = riskObj.impact.map((i) => {
        return i.value;
      });
    }

    if (riskObj.likeHood && riskObj.likeHood.length) {
      riskObj.likeHood = riskObj.likeHood.map((l) => {
        return l.value;
      });
    }

    this.setState(
      {
        isDisable: true,
      },
      () => {
        this.props.setAppliedFilters(riskObj, "Risk");
      }
    );
  }
  handleClearState(isCleared) {
    if (isCleared) {
    } else {
      this.setState(this.initialState, () => {
        this.props.setAppliedFilters(null, "Risk");
      });
    }
  }
  handleFilterChange(key, value) {
    // setting empty owner array to null, as its mapped from backend with null,
    // it is required for save button disable state
    if (key === "owner" && value.length === 0) value = null;

    this.setState(
      {
        [key]: value,
        isDisable: false,
        isSaveDisable: false,
      },
      () => {
        const {
          filterCntHeight,
          saveFilter,
          isClear,
          isDisable,
          isSaveDisable,
          loggedInTeam,
          ...rest
        } = this.state;
        const changedStateData = { ...this.initialState, ...rest };
        if (isEqual(changedStateData, this.initialState)) {
          this.handleClearState(false);
          this.setState({ isDisable: true });
        }
      }
    );
  }
  handleSaveFilter() {
    if (this.state.saveFilter) {
      let filterName = this.state.filterName;
      let validationObj = validateGenericField(
        "filter name",
        filterName,
        true,
        40
      );
      if (validationObj["isError"]) {
        this.setState({
          isSaveDisable: true,
          filterNameError: validationObj["isError"],
          filterNameErrorMessage: validationObj["message"],
        });
        return;
      }

      const { Risk } = this.props.appliedFiltersState;
      const {
        filterCntHeight,
        saveFilter,
        isClear,
        isDisable,
        isSaveDisable,
        ...data
      } = this.state;
      let dataCopy = { ...data };
      dataCopy.owner = this.state.owner.map((o) => o.id);
      dataCopy.createdBy = this.state.createdBy.map((c) => c.id);
      dataCopy.risk = this.state.risk.map((t) => t.id);
      dataCopy.status = this.state.status.map((s) => s.value);
      dataCopy.impact = this.state.impact.map((i) => i.value);
      dataCopy.likeHood = this.state.likeHood.map((l) => l.value);

      let itemOrder = cloneDeep(this.props.itemOrderState.data);
      itemOrder.riskFilter = {
        ...dataCopy,
        filterId: Risk && Risk["filterId"] ? Risk["filterId"] : ``,
      };
      this.setState({ saveBtnQuery: "progress" });
      this.props.SaveItemOrder(
        itemOrder,
        (resp) => {
          this.setState({ saveBtnQuery: "" });
          this.props.closeAction();
          if (!this.props.appliedFiltersState.Risk) {
            this.setState(this.initialState);
          } else {
            if (data.filterId)
              this.setState({ saveFilter: false, isSaveDisable: true });
            else {
              const filter =
                resp.data.userRiskFilter[0].riskFilter[
                  resp.data.userRiskFilter[0].riskFilter.length - 1
                ];
              this.props.setAppliedFilters(filter, "Risk");
            }
          }
        },
        (error) => {
          if (error.status === 409) {
            this.setState({ saveBtnQuery: "" });
            this.setState({ filterNameError: true });
            this.setState({
              filterNameErrorMessage: "Same filter name already exists",
            });
          } else {
            this.setState({ saveFilter: false });
          }
        }
      );
    } else this.setState({ saveFilter: true });
  }

  handleBackFilter() {
    this.setState({
      saveFilter: false,
      filterNameError: false,
      filterNameErrorMessage: "",
      filterName: "",
    });
  }
  onResize() {
    this.setState({ filterCntHeight: `${calculateFilterContentHeight()}px` });
  }
  handleInput(event, name) {
    this.setState({
      filterName: event.target.value,
      filterNameError: false,
      filterNameErrorMessage: ``,
      isSaveDisable: false,
    });
  }
  handleSwitch(name) {
    this.setState((prevState) => ({
      [name]: !prevState[name],
      isDisable: false,
    }));
  }
  handleClickAway = () => {
    const isDatePickerDialogueOpen = document.querySelectorAll(
      'div[class^="RangeDatePicker"]'
    ).length;
    const isDropdownMenuOpen = document.getElementById("menu-") !== null;

    if (!isDropdownMenuOpen && !isDatePickerDialogueOpen)
      this.props.closeAction();
  };

  generateRiskData = () => {
    const { risks } = this.props;
    let ddData = risks.map((risk, i) => {
      return generateSelectData(
        risk.riskTitle,
        risk.riskTitle,
        risk.id,
        risk.uniqueId,
        risk
      );
    });
    return ddData;
  };
  render() {
    const {
      classes,
      theme,
      open,
      closeAction,
      multipleFilters,
      risks,
      members,
      promoBar,
      notificationBar,
      intl,
    } = this.props;
    const {
      filterCntHeight,
      saveFilter,
      defaultFilter,
      filterName,
      filterNameError,
      filterNameErrorMessage,
      isDisable,
      isSaveDisable,
      status,
      impact,
      risk,
      owner,
      createdBy,
      likeHood,
      saveBtnQuery,
    } = this.state;
    const impactColor = theme.palette.riskImpact;
    const statusColor = theme.palette.riskStatus;

    const impactData = (theme, classes, t) => {
      return [
        {
          label: getText(t, "risk.common.impact.dropdown.minor", "Minor", {}),
          value: "Minor",
          icon: (
            <RingIcon
              htmlColor={impactColor.Minor}
              classes={{ root: classes.impactIcon }}
            />
          ),
        },
        {
          label: getText(
            t,
            "risk.common.impact.dropdown.moderate",
            "Moderate",
            {}
          ),
          value: "Moderate",
          icon: (
            <RingIcon
              htmlColor={impactColor.Moderate}
              classes={{ root: classes.impactIcon }}
            />
          ),
        },
        {
          label: getText(t, "risk.common.impact.dropdown.major", "Major", {}),
          value: "Major",
          icon: (
            <RingIcon
              htmlColor={impactColor.Major}
              classes={{ root: classes.impactIcon }}
            />
          ),
        },
        {
          label: getText(
            t,
            "risk.common.impact.dropdown.critical",
            "Critical",
            {}
          ),
          value: "Critical",
          icon: (
            <RingIcon
              htmlColor={impactColor.Critical}
              classes={{ root: classes.impactIcon }}
            />
          ),
        },
      ];
    };
    function getText(t, id, dMessage, values) {
      return t.formatMessage({ id: id, defaultMessage: dMessage }, values);
    }
    const statusData = (theme, classes, t) => {
      return [
        {
          label: getText(
            t,
            "risk.common.status.dropdown.identified",
            "Identified",
            {}
          ),
          value: "Identified",
          icon: (
            <RoundIcon
              htmlColor={statusColor.Identified}
              className={classes.statusIcon}
            />
          ),
        },
        {
          label: getText(
            t,
            "risk.common.status.dropdown.in-review",
            "In Review",
            {}
          ),
          value: "In Review",
          icon: (
            <RoundIcon
              htmlColor={statusColor.InReview}
              className={classes.statusIcon}
            />
          ),
        },
        {
          label: getText(t, "risk.common.status.dropdown.agreed", "Agreed", {}),
          value: "Agreed",
          icon: (
            <RoundIcon
              htmlColor={statusColor.Agreed}
              className={classes.statusIcon}
            />
          ),
        },
        {
          label: getText(
            t,
            "risk.common.status.dropdown.rejected",
            "Rejected",
            {}
          ),
          value: "Rejected",
          icon: (
            <RoundIcon
              htmlColor={statusColor.Rejected}
              className={classes.statusIcon}
            />
          ),
        },
      ];
    };

    let customFieldsArr = getCustomFields(this.props.customFields,this.props.profileState.data,"risk");
    const impactDropdown = customFieldsArr.find(d => d.fieldName === "Impact");
    const likeDropdown = customFieldsArr.find(d => d.fieldName === "Likelihood");
    const riskDropdown = customFieldsArr.find(d => d.fieldName === "Risk");

    return (
      <ClickAwayListener onClickAway={this.handleClickAway}>
        <Drawer
          className={classes.drawer}
          variant="persistent"
          anchor="right"
          open={open}
          PaperProps={{ style: { top: calculateHeaderHeight() } }}
          classes={{
            paper: classes.drawerPaper,
          }}
        >
          <ReactResizeDetector
            handleWidth
            handleHeight
            onResize={this.onResize}
          />
          <Scrollbars autoHide style={{ width: 415, height: filterCntHeight }}>
            <div className={classes.drawerHeader}>
              <div className={classes.filterCntHeader}>
                <div className={classes.filterHeaderTextCnt}>
                  <IconButton
                    btnType="grayBg"
                    onClick={saveFilter ? this.handleBackFilter : closeAction}
                    style={{ padding: 4 }}
                  >
                    {saveFilter ? (
                      <BackArrow
                        classes={{ root: classes.filtersHeaderCloseIcon }}
                        htmlColor={theme.palette.secondary.medDark}
                      />
                    ) : (
                      <CloseIcon
                        classes={{ root: classes.filtersHeaderCloseIcon }}
                        htmlColor={theme.palette.secondary.medDark}
                      />
                    )}
                  </IconButton>

                  <Typography
                    variant="h3"
                    classes={{ h3: classes.filtersHeaderText }}
                  >
                    {saveFilter ? (
                      <FormattedMessage
                        id="filters.save.label"
                        defaultMessage="Save Filter"
                      />
                    ) : (
                      <FormattedMessage
                        id="filters.label"
                        defaultMessage="Filters"
                      />
                    )}
                  </Typography>
                </div>
                {!saveFilter ? (
                  <span
                    className={classes.filterHeaderClearBtn}
                    onClick={this.handleClearState.bind(this, false)}
                  >
                    <FormattedMessage
                      id="common.action.Clear.label"
                      defaultMessage="Clear"
                    />
                  </span>
                ) : null}
              </div>
              {saveFilter ? (
                <Grid container classes={{ container: classes.filterFormCnt }}>
                  <Grid
                    item
                    classes={{ item: classes.filterItem }}
                    style={{ marginBottom: 0, width: "100%" }}
                  >
                    <DefaultTextField
                      label={
                        <FormattedMessage
                          id="filters.form.name"
                          defaultMessage="Filter Name"
                        />
                      }
                      formControlStyles={{ marginBottom: 0 }}
                      fullWidth={true}
                      errorState={filterNameError}
                      errorMessage={filterNameErrorMessage}
                      defaultProps={{
                        id: "filterName",
                        placeholder: this.props.intl.formatMessage({
                          id: "filters.form.placeholder",
                          defaultMessage: "Enter Filter Name",
                        }),
                        onChange: (event) =>
                          this.handleInput(event, "filterName"),
                        value: filterName,
                        inputProps: { maxLength: 40 },
                      }}
                    />
                  </Grid>
                  <Grid
                    item
                    classes={{ item: classes.filterItem }}
                    style={{ marginTop: 0 }}
                  >
                    <div
                      className={classes.switchCnt}
                      style={{ marginLeft: 0 }}
                    >
                      <p className={classes.switchLabel}>
                        <SvgIcon
                          viewBox="0 0 8 11.562"
                          className={classes.pushPinIcon}
                          htmlColor={theme.palette.secondary.light}
                        >
                          <PushPin />
                        </SvgIcon>
                        <FormattedMessage
                          id="filters.form.message"
                          defaultMessage="Make this filter my default view"
                        />
                        <HelpIcon
                          classes={{ root: classes.filtersHeaderHelpIcon }}
                          htmlColor={theme.palette.secondary.medDark}
                          fontSize="small"
                        />
                      </p>
                      <DefaultSwitch
                        checked={defaultFilter}
                        onChange={(event) => {
                          this.handleSwitch("defaultFilter", event);
                        }}
                      />
                    </div>
                  </Grid>
                </Grid>
              ) : (
                <Grid container classes={{ container: classes.filterFormCnt }}>
                  <Grid item xs={12} classes={{ item: classes.filterItem }}>
                    <SelectSearchDropdown
                      data={() => generateAssigneeData(members)}
                      label={
                        <FormattedMessage
                          id="risk.risk-owner.label"
                          defaultMessage="Risk Owner"
                        />
                      }
                      selectChange={this.handleFilterChange}
                      type="owner"
                      selectedValue={owner}
                      placeholder={this.props.intl.formatMessage({
                        id: "risk.risk-owner.action",
                        defaultMessage: "Select Owner",
                      })}
                      avatar={true}
                    />
                  </Grid>
                  {/* to do createdBy null backend */}
                  {/* <Grid item xs={12} classes={{ item: classes.filterItem }}>
                      <SelectSearchDropdown
                        data={this.generateAssigneeData}
                        label="Created By"
                        selectChange={this.handleFilterChange}
                        type="createdBy"
                        selectedValue={createdBy}
                        placeholder={'Select Member'}
                        avatar={true}
                      />
                    </Grid> */}
                  <Grid item xs={12} classes={{ item: classes.filterItem }}>
                    <SelectSearchDropdown
                      data={() => statusData(theme, classes, intl)}
                      label={this.props.intl.formatMessage({
                        id: "issue.common.status.label",
                        defaultMessage: "Select Status",
                      })}
                      selectChange={this.handleFilterChange}
                      type="status"
                      selectedValue={status}
                      placeholder={this.props.intl.formatMessage({
                        id: "issue.common.status.label",
                        defaultMessage: "Select Status",
                      })}
                      icon={true}
                    />
                  </Grid>
                  {impactDropdown && <Grid item xs={12} classes={{ item: classes.filterItem }}>
                    <SelectSearchDropdown
                      data={() => impactData(theme, classes, intl)}
                      label={
                        <FormattedMessage
                          id="risk.creation-dialog.form.impact.label"
                          defaultMessage="Impact"
                        />
                      }
                      selectChange={this.handleFilterChange}
                      type="impact"
                      selectedValue={impact}
                      placeholder={this.props.intl.formatMessage({
                        id: "risk.common.impact.label",
                        defaultMessage: "Select Impact",
                      })}
                      icon={true}
                    />
                  </Grid>}

                  {likeDropdown && <Grid item xs={12} classes={{ item: classes.filterItem }}>
                    <SelectSearchDropdown
                      data={() => likelihoodData(intl)}
                      label={
                        <FormattedMessage
                          id="risk.common.likelihood.label"
                          defaultMessage="Likelihood"
                        />
                      }
                      selectChange={this.handleFilterChange}
                      type="likeHood"
                      selectedValue={likeHood}
                      placeholder={this.props.intl.formatMessage({
                        id: "risk.common.likelihood.placeholder",
                        defaultMessage: "Select Likelihood",
                      })}
                    />
                  </Grid>}
                  {riskDropdown && <Grid item xs={12} classes={{ item: classes.filterItem }}>
                    <SelectSearchDropdown
                      data={this.generateRiskData}
                      label={
                        <FormattedMessage
                          id="risk.common.risk.label"
                          defaultMessage="Risk"
                        />
                      }
                      selectChange={this.handleFilterChange}
                      type="risk"
                      selectedValue={risk}
                      placeholder={this.props.intl.formatMessage({
                        id: "meeting.creation-dialog.form.risk.placeholder",
                        defaultMessage: "Select Risk",
                      })}
                    />
                  </Grid>}
                </Grid>
              )}

              <Grid
                container
                direction="row"
                justify="flex-end"
                alignItems="center"
                classes={{ container: classes.filterBottomBtns }}
                style={{ bottom: promoBar || notificationBar ? 185 : 140 }}
              >
                {saveFilter ? (
                  <CustomButton
                    style={{ flex: 1, marginRight: 20 }}
                    btnType="white"
                    variant="contained"
                    onClick={this.handleBackFilter}
                  >
                    <FormattedMessage
                        id="common.action.cancel.label"
                        defaultMessage="Cancel"
                      />
                  </CustomButton>
                ) : null}
                <CustomButton
                  style={{ flex: 1 }}
                  btnType="success"
                  variant="contained"
                  onClick={this.handleSaveFilter}
                  disabled={
                    saveFilter ? saveBtnQuery == "progress" : isSaveDisable
                  }
                  query={saveFilter ? saveBtnQuery : null}
                >
                  {saveFilter ? (
                    <FormattedMessage
                      id="filters.save.label"
                      defaultMessage="Save Filter"
                    />
                  ) : (
                    <FormattedMessage
                    id="filters.custom-filter.label"
                    defaultMessage="Custom Filter"
                  />
                  )}
                </CustomButton>
                {!saveFilter ? (
                  <DefaultButton
                    text={
                      <FormattedMessage
                        id="common.search.label"
                        defaultMessage="Search"
                      />
                    }
                    buttonType="Plain"
                    style={{ flex: 1, marginLeft: 20 }}
                    onClick={this.handleSearchFilter}
                    disabled={isDisable}
                  />
                ) : null}
              </Grid>
            </div>
          </Scrollbars>
        </Drawer>
      </ClickAwayListener>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    appliedFiltersState: state.appliedFilters,
    profileState: state.profile,
    itemOrderState: state.itemOrder,
    members: state.profile.data ? state.profile.data.member.allMembers : [],
    risks: state.risks.data,
    promoBar: state.promoBar.promoBar,
    notificationBar: state.notificationBar.notificationBar,
    customFields: state.customFields,
  };
};

export default compose(
  withRouter,
  injectIntl,
  withStyles(styles, {
    withTheme: true,
  }),
  connect(mapStateToProps, {
    SaveItemOrder,
    setAppliedFilters,
  })
)(RiskFilter);
