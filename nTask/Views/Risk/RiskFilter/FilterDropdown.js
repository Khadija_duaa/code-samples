import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import ListItemText from "@material-ui/core/ListItemText";
import DoneIcon from "@material-ui/icons/Done";
import selectStyles from "../../../assets/jss/components/select";
import readyDropdownStyles from "../../../components/Menu/TaskMenus/style";
import dropdownStyles from "../../../components/Menu/TaskMenus/style";
import Checkbox from "@material-ui/core/Checkbox";
import SelectMenu from "../../../components/Menu/SelectMenu";
import combineStyles from "../../../utils/mergeStyles";
let object = {};
class FilterDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: [],
      clear: false
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.isClear) {
      object = {
        name: [],
        clear: true
      };
    } else {
      object = {
        clear: false
      };
    }
    return object;
  }

  handleChange = event => {
    this.setState({ name: event.target.value}, () => {
      if (this.props.type)
        this.props.handleFilterChange(this.props.type, this.state.name);
    });
  };
  componentDidUpdate(prevProps, prevState) {
    if (this.state.clear) {
      this.props.handleClearState(true);
    }
    let name = this.props.name || [];
    if (name && JSON.stringify(name) !== JSON.stringify(prevProps.name || []))
      this.setState({ name });
  }
  componentDidMount() {
    let name = this.props.name;
    if (name)
      this.setState({ name });
  }
  render() {
    const { classes, theme, label, error, style, tasks } = this.props;
    const { name, clear } = this.state;
    const statusColor = theme.palette.status;
    const newTasks =
      this.props.tasks && this.props.tasks.length
        ? this.props.tasks.map(x => {
          return { name: x.taskTitle, id: x.taskId };
        })
        : [];
    const names =
      label === "Likelihood"
        ? [
          { name: "0-25 %", value: "0-25", color: statusColor.dueToday },
          { name: "26-50 %", value: "26-50", color: statusColor.starred },
          { name: "51-75 %", value: "51-75", color: statusColor.review },
          { name: "76-100 %", value: "76-100", color: statusColor.completed }
        ]
        : label === "Tasks"
          ? newTasks
          : label === "Category"
            ? [
              { name: "Task Board", color: statusColor.completed },
              { name: "Website Development", color: statusColor.review },
              { name: "Meeting Board", color: statusColor.inProgress }
            ]
            : [];
    return (
      <SelectMenu
        label={label}
        multiple={true}
        value={name}
        onChange={this.handleChange}
        error={error}
        style={style}
      >
        {names.map(status => (
          <MenuItem
            key={label === "Tasks" ? status.id : status.name}
            value={label === "Tasks" ? status.id : (label === "Likelihood") ? status.value : status.name}
            className={`${classes.statusMenuItemCnt} ${
              this.state.name.indexOf(
                label === "Tasks" ? status.id : (label === "Likelihood") ? status.value : status.name
              ) > -1
                ? classes.selectedValue
                : null
              }`}
            classes={{
              selected: classes.statusMenuItemSelected
            }}
          >
            <ListItemText
              primary={status.name}
              classes={{ primary: classes.statusItemText }}
            />

            <Checkbox
              checkedIcon={
                <DoneIcon htmlColor={theme.palette.primary.light} />
              }
              icon={false}
            />
          </MenuItem>
        ))}
      </SelectMenu>
    );
  }
}

export default withStyles(
  combineStyles(dropdownStyles, readyDropdownStyles, selectStyles),
  {
    withTheme: true
  }
)(FilterDropdown);
