import React, { useState, useRef, useEffect, memo } from "react";
import { useDispatch, useSelector } from "react-redux";
import DropdownMenu from "../../../components/Dropdown/DropdownMenu";
import CustomIconButton from "../../../components/Buttons/CustomIconButton";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import CustomListItem from "../../../components/ListItem/CustomListItem";
import withStyles from "@material-ui/core/styles/withStyles";
import riskActionDropdownStyles from "./riskActionDropdown.style";
import CustomMenuList from "../../../components/MenuList/CustomMenuList";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import { FormattedMessage } from "react-intl";
import { withSnackbar } from "notistack";
import { compose } from "redux";
import { setDeleteDialogueState } from "../../../redux/actions/allDialogs";
import ColorPicker from "../../../components/Dropdown/ColorPicker/ColorPicker";
import isEqual from "lodash/isEqual";
import "react-toastify/dist/ReactToastify.css";
import IconArchive from "../../../components/Icons/TaskActionIcons/IconArchive";
import IconColor from "../../../components/Icons/TaskActionIcons/IconColor";
import IconDelete from "../../../components/Icons/TaskActionIcons/IconDelete";
import SvgIcon from "@material-ui/core/SvgIcon";
import {
  CopyRisk,
  DeleteRisk,
  ArchiveRisk,
  updateRiskData,
  UnarchiveRisk,
  MoveRisk,
} from "../../../redux/actions/risks";

// import constants from "../constants/types";
function areEqual(prevProps, nextProps) {
  return isEqual(prevProps, nextProps);
  /*
  return true if passing nextProps to render would return
  the same result as passing prevProps to render,
  otherwise return false
  */
}
const RiskActionDropdown = memo(props => {
  const [open, setOpen] = useState(null);
  const [archiveConfirmation, setArchiveConfirmation] = useState(false);
  const [unArchiveConfirmation, setUnArchiveConfirmation] = useState(false);
  const [archiveBtnQuery, setArchiveBtnQuery] = useState("");
  const [unarchiveBtnQuery, setUnarchiveBtnQuery] = useState("");
  const { companyInfo } = useSelector(state => {
    return {
      companyInfo: state.whiteLabelInfo.data,
    };
  });
  const anchorEl = useRef(null);
  const dispatch = useDispatch();
  const {
    classes,
    data,
    btnProps,
    handleCloseCallBack,
    riskPermission,
    isArchived = false,
  } = props;
  const handleClose = event => {
    // Function closes dropdown
    setOpen(null);
  };
  const handleArchiveConfirmClose = () => {
    setArchiveConfirmation(false);
    setUnArchiveConfirmation(false);
  };
  const handleArchiveConfirmation = () => {
    setArchiveConfirmation(true);
  };
  const handleUnArchiveConfirmation = () => {
    setUnArchiveConfirmation(true);
  };
  const handleDropdownOpen = event => {
    // Function Opens the dropdown
    setOpen(state => (state ? null : event.currentTarget));
  };
  const showSnackBar = (snackBarMessage = "", type = null) => {
    const { classes, enqueueSnackbar } = props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  useEffect(() => {
    anchorEl.current &&
      anchorEl.current.addEventListener("click", e => {
        e.stopPropagation();
        handleDropdownOpen(e);
      });
  }, []);

  //archive a risk
  const archiveRisk = () => {
    setArchiveBtnQuery("progress");
    handleCloseCallBack();
    const riskId = data.id;
    ArchiveRisk(
      riskId,
      () => {
        // Success Callback
        handleArchiveConfirmClose();
        setArchiveBtnQuery("");
      },
      err => {
        // failure callback
        setArchiveBtnQuery("");
        handleArchiveConfirmClose();
        if (err.data && err.data.message) showSnackBar(err.data.message, "error");
      },
      dispatch
    );
    handleClose();
  };

  const handleUnArchive = e => {
    if (e) e.stopPropagation();
    setUnarchiveBtnQuery('progress');
    handleCloseCallBack();
    UnarchiveRisk(
      data,
      //success
      () => {
        handleArchiveConfirmClose();
        setUnarchiveBtnQuery('');
      },
      //failure
      error => {
        setUnarchiveBtnQuery('');
        showSnackBar("Risk Cannot be UnArchived", "error");
      },
      dispatch
    );
  };
  //Delete single risk

  const deleteRisk = () => {
    const riskId = data.id;
    handleCloseCallBack();
    dispatch(setDeleteDialogueState({ btnQuery: "progress" }));
    DeleteRisk(
      riskId,
      (data, res) => {
        closeDeleteConfirmation();
      },
      err => {
        closeDeleteConfirmation();
        if (err.data && err.data.message) showSnackBar(err.data.message, "error");
      },
      dispatch
    );
  };

  //Confirmation Dialog close
  const closeDeleteConfirmation = () => {
    dispatch(setDeleteDialogueState({ open: false, btnQuery: "" }));
  };
  //Open delete confirmation dialog
  const handleDeleteConfirmation = success => {
    const riskLabelSingle = companyInfo?.risk?.sName;
    const dialogObj = {
      open: true,
      cancelBtnText: "Cancel",
      successBtnText: "Delete",
      headingText: "Delete",
      msgText: `Are you sure you want to delete this risk?`,
      successAction: deleteRisk,
      closeAction: closeDeleteConfirmation,
      btnQuery: "",
    };
    dispatch(setDeleteDialogueState(dialogObj));
    handleClose();
  };
  const colorChange = color => {
    const obj = { colorCode: color };
    handleClose();
    updateRiskData(
      { risk: data, obj },
      dispatch,
      //Success
      task => { }
    );
  };
  // permissions allowed 

  let archivePer = riskPermission ? riskPermission.archive.cando : false;
  let unarchivePer = riskPermission ? riskPermission.unarchives.cando : false;
  let deletePer = riskPermission ? riskPermission.delete.cando : false;

  const isOpen = Boolean(open);
  const taskLabelSingle = companyInfo?.task?.sName;
  return (
    <>
      <CustomIconButton
        btnType="transparent"
        buttonRef={node => (anchorEl.current = node)}
        style={{ padding: 0 }}
        {...btnProps}>
        <MoreVerticalIcon className={classes.ellipsesIcon} />
      </CustomIconButton>
      <DropdownMenu
        open={isOpen}
        closeAction={handleClose}
        anchorEl={anchorEl.current}
        size={"small"}
        placement="bottom-end"
        disablePortal={false}>
        <CustomMenuList>
          {!isArchived && (
            <CustomListItem
              subNav={true}
              popperProps={{
                placement: "left-start",
              }}
              subNavRenderer={
                <>
                  <ColorPicker
                    triangle="hide"
                    onColorChange={colorChange}
                    selectedColor={data.colorCode || ""}
                    width={190}
                  />
                </>
              }>
              <SvgIcon viewBox="0 0 16.5 16.5" className={classes.icon}>
                <IconColor />
              </SvgIcon>
              Color
            </CustomListItem>
          )}
          {archivePer && !isArchived && (
            <CustomListItem rootProps={{ onClick: handleArchiveConfirmation }}>
              <SvgIcon viewBox="0 0 14 14.014" className={classes.icon}>
                <IconArchive />
              </SvgIcon>
              Archive
            </CustomListItem>
          )}
          {unarchivePer && isArchived && (
            <CustomListItem rootProps={{ onClick: handleUnArchiveConfirmation }}>
              <SvgIcon viewBox="0 0 14 14.014" className={classes.icon}>
                <IconArchive />
              </SvgIcon>
              Un Archive
            </CustomListItem>
          )}
          {deletePer && (
            <CustomListItem rootProps={{ onClick: handleDeleteConfirmation }}>
              <SvgIcon viewBox="0 0 14 14.002" className={classes.icon}>
                <IconDelete />
              </SvgIcon>
              Delete
            </CustomListItem>
          )}
        </CustomMenuList>
      </DropdownMenu>
      {archiveConfirmation ? (
        <ActionConfirmation
          open={true}
          closeAction={handleArchiveConfirmClose}
          cancelBtnText={
            <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
          }
          successBtnText={
            <FormattedMessage
              id="common.action.archive.confirmation.archive-button.label"
              defaultMessage="Archive"
            />
          }
          alignment="center"
          headingText={
            <FormattedMessage
              id="common.action.archive.confirmation.archive-button.label"
              defaultMessage="Archive"
            />
          }
          iconType="archive"
          msgText={
            <FormattedMessage
              id="common.action.archive.confirmation.risk.label"
              defaultMessage="Are you sure you want to archive this risk?"
            />
          }
          successAction={archiveRisk}
          btnQuery={archiveBtnQuery}
        />
      ) : null}
      {unArchiveConfirmation ? (
        <ActionConfirmation
          open={unArchiveConfirmation}
          closeAction={handleArchiveConfirmClose}
          cancelBtnText={
            <FormattedMessage
              id="common.action.un-archive.confirmation.cancel-button.label"
              defaultMessage="Cancel"
            />
          }
          successBtnText={
            <FormattedMessage
              id="common.action.un-archive.confirmation.un-archive-button.label"
              defaultMessage="Unarchive"
            />
          }
          alignment="center"
          iconType="unarchive"
          headingText={
            <FormattedMessage
              id="common.action.un-archive.confirmation.title"
              defaultMessage="Unarchive"
            />
          }
          msgText={
            <FormattedMessage
              id="common.action.un-archive.confirmation.risk.label"
              defaultMessage="Are you sure you want to unarchive this risk?"
            />
          }
          successAction={handleUnArchive}
          btnQuery={unarchiveBtnQuery}
        />
      ) : null}
    </>
  );
}, areEqual);

export default compose(
  withSnackbar,
  withStyles(riskActionDropdownStyles, { withTheme: true })
)(RiskActionDropdown);
