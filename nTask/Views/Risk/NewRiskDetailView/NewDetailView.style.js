const riskDetailStyles = theme => ({
  iconAdd: {
    fontSize: "18px !important",
    position: "relative",
    top: "6px",
    marginRight: "7px",
  },
  selectBox: {
    font: "normal normal normal 13px / 16px Lato",
    letterSpacing: "0px",
    color: "#7E7E7E",
  },
  textContainer: {
    font: " normal normal normal 13px/16px Lato",
    letterSpacing: "0px",
    color: "#505050",
  },

  mainContainer: {
    padding: "15px",
  },

  createdBy: {
    marginBottom: "10px",
    "& span": {
      fontSize: "12px !important",
      fontFamily: theme.typography.fontFamilyLato,
      color: "#7E7E7E",
      paddingLeft: 5,
    },
  },

  systemFieldIcon: {
    display: "flex",
  },

  customFieldHeading: {
    width: "100%",
    height: "32px",
    background: "#7F8F9A",
    borderRadius: "4px",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    padding: "10px",
    marginTop: "8px",

    "& h3": {
      fontSize: "13px !important",
      fontFamily: theme.typography.fontFamilyLato,
      color: theme.palette.common.white,
    },
  },

  customFieldRemoveIcon: {
    color: theme.palette.common.white,
  },
  customFieldAddIcon: {
    color: theme.palette.common.white,
  },
  customFieldDropDown: {
    "& button": {
      display: "flex",
      padding: "10px",
      border: "none",
      alignItems: "center",
      background: "#F6F6F6",
      fontSize: "13px !important",
      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: "300",
      color: " #505050",
    },
  },
  dropDown: {
    marginRight: "7px",
    marginTop: "3px",
  },
  systemFieldsDropDown: {
    minHeight: 630,
  },
  customDropDownListItem: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    width: "154px",
    boxShadow: "0px 2px 6px #00000026",
    borderRadius: "6px",
    opacity: 1,
  },
  customDropDownIcon1: {
    display: "flex",
    alignItems: "center",
    marginTop: "12px",
  },
  customDropDownText: {
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    color: theme.palette.common.black,
    marginLeft: "8px",
    fontWeight: "600",
  },
  addCustomFieldCnt: {
    // padding: "20px 0 20px 0",
    // marginTop: 20,
    // marginBottom: 20,
    // borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    // borderTop: `1px solid ${theme.palette.border.lightBorder}`,
  },
  systemFieldsHeading: {
    width: "100%",
    height: "32px",
    background: "#7F8F9A",
    borderRadius: "4px",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    padding: "10px",
    marginTop: "8px",
    cursor: "pointer",

    "& h3": {
      fontSize: "13px !important",
      fontFamily: theme.typography.fontFamilyLato,
      color: theme.palette.common.white,
      marginBottom: 3,
    },
    "&:hover $editIcon": {
      visibility: "visible",
    },
    "&:hover $documentIcon": {
      visibility: "visible",
    },
  },
  systemFieldTitle: {
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    padding: "9px 0px 10px 5px",
    height: "fit-content",
    marginRight: 10,
    fontWeight: theme.typography.fontWeightExtraLight,
    color: "white !important",
    cursor: "pointer",
  },
  systemFieldRemoveIcon: {
    color: theme.palette.common.white,
    fontSize: "20px !important",
    cursor: "pointer",
  },
  systemFieldAddIcon: {
    color: theme.palette.common.white,
    fontSize: "20px !important",
    cursor: "pointer",
  },
  sectionCnt: {
    margin: "10px 4px",
    width: 'auto'
  },
  impactIcon: {
    fontSize: "14px !important",
    marginRight: 5,
  },
  btnStyleClass: {
    marginRight: 10,
    fontSize: "13px !important",
    fontWeight: theme.typography.fontWeightExtraLight,
    fontFamily: theme.typography.fontFamilyLato,
    padding: "2px 3px 2px 10px",
    minHeight: 20,
    color: theme.palette.common.white,
    textTransform: "capitalize",
    borderRadius: 4,
    minWidth: 95,
    // letterSpacing: "1.6",
    boxShadow: "none",
    "&:focus": {
      boxShadow: "none",
    },
    "& $circularProgress": {
      color: `${theme.palette.common.white} !important`,
    },
  },
  dropdownIndicatorIcon: {
    marginLeft: 5,
    color: "white",
  },
  vl: {
    border: `1px solid #E7E9EB`,
    marginRight: 15,
    marginLeft: 15,
    height: 20,
  },
  iconBtnStyles: {
    padding: 4,
    background: theme.palette.common.white,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    marginRight: 5,
    borderRadius: "100%",
  },
  assessmentGraphHeadingCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    padding: "0px 10px 0px 12px",
    width: "100%",
  },
  iconStyle: {
    marginRight: 5,
    marginTop: 8,
    marginBottom: 3,
  },
  textFieldCnt: {
    display: "flex",
    flexDirection: "row",
  },
  verticalText:{
    position: 'absolute', 
    fontSize: "12px !important", 
    fontFamily: theme.typography.fontFamilyLato,
    width: 250,
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    top: 118,
    transform: "rotate(-90deg)",
    left: -112,
    textAlign: "center"
  },

  /* Extra small devices (phones, 600px and down and tablet) */
  "@media only screen and (max-width: 1024px)": {
    textFieldCnt: {
      display: "block",
    },
  },
});

export default riskDetailStyles;
