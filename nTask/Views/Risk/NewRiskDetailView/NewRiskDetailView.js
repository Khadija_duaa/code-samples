import React, { useState, useMemo, useRef, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import isArray from "lodash/isArray";
import Typography from "@material-ui/core/Typography";
import moment from "moment";
import RemoveIcon from "@material-ui/icons/Remove";
import AddIcon from "@material-ui/icons/Add";
import Grid from "@material-ui/core/Grid";
import { withSnackbar } from "notistack";
import CustomButton from "../../../components/Buttons/CustomButton";
import UpdateMatrixDialog from "../../../components/Matrix/updateMatrixDialog.cmp";

import newDetailStyles from "./NewDetailView.style";
import DetailsDialog from "../../../components/Dialog/DetailsDialog/DetailsDialog";
import EditableTextField from "../../../components/Form/EditableTextField";
import helper from "../../../helper";
import RiskActionDropdown from "./RiskActionDropDown";
import { getCustomFields } from "../../../helper/customFieldsData";
import SideTabs from "../RiskDetails/SideTabs";
import SectionsView from "../../../components/Sections/sections.view";
import { riskDetailDialog } from "../../../redux/actions/allDialogs";
import CircularIcon from "@material-ui/icons/Brightness1";
import {
  UpdateRisk,
  DeleteRisk,
  updateRisk,
  dispatchRisk,
  MarkRiskNotificationsAsRead,
} from "../../../redux/actions/risks";
import {
  editCustomField,
  updateCustomFieldData,
  hideCustomField,
} from "../../../redux/actions/customFields";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";
import CustomFieldView from "../../CustomFieldSettings/CustomFields.view";
import CustomFieldsDropDown from "../../../components/CustomFieldsViews/CustomFieldsDropDown.view";
import { impactData, likelihoodData, statusData } from "../../../helper/riskDropdownData";
import { generateTaskData, generateProjectData } from "../../../helper/generateSelectData";
import RiskDescriptionEditor from "../RiskDetails/RiskDetailsDescription";
import CustomFieldsChecklistView from "../../../components/CustomFieldsViews/CustomFieldsChecklist.view";
import { updateCustomFieldDialogState } from "../../../redux/actions/allDialogs";
import NotificationMenu from "../../../components/Header/NotificationsMenu";
import IconMenu from "../../../components/Menu/TaskMenus/IconMenu";
import ChevronDown from "@material-ui/icons/ArrowDropDown";
import AssigneeDropdown from "../../../components/Dropdown/AssigneeDropdown";
import { ResponsiveLine } from '@nivo/line'
import CustomFieldIcons from "../../../Views/CustomFieldSettings/CustomFieldIcons/CustomFieldIcons";
import CustomFieldLabelCmp from "../../../components/CustomFieldsViews/CustomFieldLabel.cmp";
import SelectSearchDropdown from "../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import ProjectPermissionSelector from "../../../redux/selectors/projectPermissionSelector";
import useUpdateRisk from "../../../helper/customHooks/Risks/updateRisk";
import { grid } from "../../../components/CustomTable2/gridInstance";
import StatusDropdown from "../../../components/Dropdown/StatusDropdown/Dropdown";
import { CanAccess, CanAccessFeature } from "../../../components/AccessFeature/AccessFeature.cmp.js";
import { updateFeatureAccessPermissions } from "../../../redux/actions/team";

const MyResponsiveLine = ({ data, axisLeft, axisBottom, chartProps, xScale, yScale }) => {
  return (
    <ResponsiveLine
      data={data}
      margin={{ top: 20, right: 20, bottom: 50, left: 40 }}
      xScale={{ type: "linear", stacked: false, ...xScale }}
      yScale={{ type: "linear", stacked: false, ...yScale }}
      yFormat=" >-.2f"
      axisTop={null}
      axisRight={null}
      enableGridX={false}
      axisBottom={{
        orient: "bottom",
        tickSize: 5,
        tickPadding: 5,
        tickRotation: 0,
        tickValues: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        format: function (value) {
          const alphabets = "ABCDEFGHIJ";
          return alphabets[value - 1];
        },
        ...axisBottom,
      }}
      axisLeft={{
        orient: "left",
        tickSize: 5,
        tickPadding: 5,
        tickRotation: 0,
        tickValues: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        ...axisLeft,
      }}
      tooltip={({ point }) => {
        return (
          <div
            style={{
              padding: 5,
              background: "white",
              border: "1px solid rgba(221, 221, 221, 1)",
              borderRadius: 4,
              fontSize: 13,
              fontFamily: "'lato', sans-serif",
            }}>
            {point.data.obj.cellName} : {moment(point.data.obj.createdDate).format("DD MMM, YYYY")}
          </div>
        );
      }}
      pointSize={10}
      pointColor={{ theme: 'background' }}
      pointBorderWidth={2}
      pointBorderColor={{ from: 'serieColor' }}
      pointLabelYOffset={-12}
      useMesh={true}
    />
  );
};

function NewDetailView(props) {
  const {
    theme,
    classes,
    members,
    dialogsState,
    riskDetailDialog,
    risksState,
    allTasks,
    workspaceRiskPer,
    projects,
    appliedFiltersState,
    UpdateRisk,
    isUpdated,
    DeleteRisk,
    closeActionMenu,
    filterRisk,
    customFields,
    profileState,
    riskActivities,
    intl,
    editCustomField,
    updateCustomFieldData,
    updateRisk,
    dispatchRisk,
    updateCustomFieldDialogState,
    MarkRiskNotificationsAsRead,
    hideCustomField,
    archivedRisks,
    enqueueSnackbar,
    accessFeaturesRisk,
    updateFeatureAccessPermissions
  } = props;
  const { editRisk } = useUpdateRisk();
  const [selectedTasks, setSelectedTasks] = useState([]);
  const [sFSecOpen, setSFSecOpen] = useState(true);
  const [readAll, setReadAll] = useState(true);
  const handleShowSystemField = section => {
    setSFSecOpen(!sFSecOpen);
  };
  const [assessmentDialogOpen, setAssessmentDialogOpen] = useState(false);
  const handleAddAssessmentDialog = value => {
    setAssessmentDialogOpen(value);
  };
  const customFieldChange = (
    option,
    obj = {},
    settings = {},
    success = () => { },
    failure = () => { }
  ) => {
    // const riskCopy = { ...selectedRisk };
    const riskCopy = { ...riskDetails.current };

    if (obj.fieldType == "matrix") {
      const selectedField =
        riskCopy.customFieldData && riskCopy.customFieldData.find(x => x.fieldId == obj.fieldId);
      const isExist =
        selectedField &&
        selectedField.fieldData.data.findIndex(f => f.cellName == option.cellName) > -1;
      if (isExist === true) return;
    }
    const { fieldId } = obj;
    const newObj = {
      groupType: "risk",
      groupId: riskCopy.id,
      fieldType: obj.fieldType,
      fieldId,
      fieldData: { data: option },
    };
    if (obj.fieldType === "dropdown") {
      if (isArray(newObj.fieldData.data))
        newObj.fieldData.data = newObj.fieldData.data.map(opt => {
          return { id: opt.id };
        });
      else newObj.fieldData.data = { id: newObj.fieldData.data.id };
    }
    if (obj.fieldType === "matrix")
      newObj.fieldData.data = newObj.fieldData.data.map(opt => {
        return { cellName: opt.cellName, createdDate: opt.createdDate };
      });

    updateRisk(
      newObj,
      res => {
        success();
        const resObj = res.data.entity[0];
        let customFieldsArr = [];
        // Updating Global state
        const isExist = riskCopy.customFieldData
          ? riskCopy.customFieldData.findIndex(c => c.fieldId === resObj.fieldId) > -1
          : false; /** if new risk created, its customFields are null so in null case scenario , compile the normal false flow, add custom field in risk object and save it */
        if (isExist) {
          customFieldsArr = riskCopy.customFieldData.map(c => {
            return c.fieldId === resObj.fieldId ? resObj : c;
          });
        } else {
          customFieldsArr = riskCopy.customFieldData
            ? [...riskCopy.customFieldData, resObj]
            : [
              resObj,
            ]; /** add custom field in risk object and save it, if newly created risk because its custom field is already null */
        }
        let newRiskObj = { ...riskCopy, customFieldData: customFieldsArr };
        props.dispatchRisk(newRiskObj);
      },
      err => {
        if (err && err.data) showSnackbar(err.data.message, "error");
        failure();
      }
    );
    handleAddAssessmentDialog(false);
  };
  const editCustomFieldCallback = (obj, oldFieldObject) => {
    // const { fieldId, fieldType } = obj;
    // if (fieldType === "checklist") return;
    // updateCustomFieldData(obj);
  };

  const handleUpdateTitle = title => {
    /** Update risk title when user changes */
    // let newRiskObj = { ...selectedRisk, title: title };
    // UpdateRisk(newRiskObj, () => { });
    UpdateRiskData(selectedRisk, 'title', title);
  };

  const generateSelectedValue = currentRisk => {
    /** function for generating the selected task list objects from task id and return taskList for showing pre populated selected task if any => function call in componentDidMount */

    const selectedTaskList = allTasks.filter(task => {
      return currentRisk.linkedTasks.indexOf(task.taskId) > -1;
    });
    let taskList = generateTaskData(selectedTaskList);
    setSelectedTasks(taskList);
  };

  const handleClose = () => {
    let searchQuery = props.history.location.pathname; //getting url
    if (
      !searchQuery.includes("gantt") &&
      !searchQuery.includes("boards") &&
      !searchQuery.includes("/reports/overview")
    ) {
      props.history.push("/risks");
    }
    riskDetailDialog({ id: "", isArchived: false });
    dialogsState.riskDetailDialog.afterCloseCallBack();
  };

  const selectedRisk = useMemo(() => {
    let risks = risksState.data;
    const currentRisk = risks.find(risk => risk.id === dialogsState.riskDetailDialog.id);
    if (currentRisk) {
      generateSelectedValue(currentRisk);
      return currentRisk;
    }
    handleClose();
  }, [dialogsState.riskDetailDialog.id, risksState.data]);

  const riskDetails = useRef(selectedRisk);

  useEffect(() => {
    riskDetails.current = selectedRisk;
  }, [dialogsState.riskDetailDialog.id, risksState.data]);
  const UpdateRiskData = (risk, type, value) => {
    editRisk(
      risk,
      type,
      value,
      (risk) => {
      },
      (errMessage) => {
        showSnackBar(errMessage, "error");
      }
    );
  };
  // handle uodate color 
  const updateColor = (value) => {
    UpdateRiskData(selectedRisk, 'colorCode', value);
  }
  const handleOptionsSelect = (key, value) => {
    /** Function that updates risk on select option */
    let newRiskObj = { ...selectedRisk, [key]: value.value };
    // UpdateRisk(newRiskObj, () => {});
    UpdateRiskData(selectedRisk, key, value.value);
  };
  const generateTaskDData = useMemo(() => {
    /* function for generating the array of task and returing array to the data prop for multi drop down */
    let tasksData = selectedRisk.projectId
      ? allTasks.filter(t => t.projectId === selectedRisk.projectId)
      : allTasks;
    let taskList = generateTaskData(tasksData);
    return taskList;
  }, [selectedRisk.projectId, selectedRisk.linkedTasks]);

  const generateProjectDData = useMemo(() => {
    let selectedProject = selectedTasks.length
      ? projects.filter(t => t.projectId === selectedTasks[0].obj.projectId)
      : projects;
    selectedProject = selectedProject.length ? selectedProject : projects;

    return generateProjectData(selectedProject.filter(p => p.projectId !== selectedRisk.projectId));
  }, [selectedRisk.projectId, selectedRisk.linkedTasks]);
  const checkRiskListAccordingToPermission = (stateTask, selectedTask) => {
    if (stateTask.length <= 0) {
      /** if state task is empty then accept the selected task */
      return true;
    } else {
      showSnackbar(
        "Oops! You cannot link this task to this risk, because it is already linked to another task. Unlink the risk with the other task first and then try again. ",
        "error"
      );
      return false;
    }
  };
  const handleTaskChange = (key, value) => {
    /* function for fetching the task id's from selected task and update the risk details  */
    let permission = checkRiskListAccordingToPermission(selectedTasks, value);
    if (permission) {
      let taskSelectedIds = value.map(obj => {
        return obj.id;
      });
      setSelectedTasks(value);
      // UpdateRisk(
      //   { ...selectedRisk, linkedTasks: taskSelectedIds, projectId: value[0].obj.projectId },
      //   response => { }
      // ); /** api calling to update the risk details */
      UpdateRiskData(selectedRisk, 'linkedTasks', taskSelectedIds);
    }
  };
  // depricated function after patch api
  const handleClearTask = (key, value) => {
    /* function for fetching the task id's from selected task and update the risk details  */
    let taskSelectedIds = value.map(obj => {
      return obj.id;
    });
    setSelectedTasks(value);
    UpdateRiskData(selectedRisk, 'linkedTasks', taskSelectedIds);
    // UpdateRisk(
    //   {
    //     ...selectedRisk,
    //     linkedTasks: taskSelectedIds,
    //     projectId:
    //       selectedRisk.projectId == "" && value.length
    //         ? value[0].obj.projectId
    //         : selectedRisk.projectId,
    //   },
    //   response => { }
    // ); /** api calling to update the risk details */
  };

  const handleClickHideOption = (event, field) => {
    let object = { ...field };
    object.settings.isHideCompletedTodos = !object.settings.isHideCompletedTodos;
    delete object.team;
    delete object.workspaces;
    delete object.level;
    editCustomField(
      object,
      object.fieldId,
      res => { },
      failure => { }
    );
  };

  const handleSelectHideOption = (value, selectedFieldObj) => {
    let object = { ...selectedFieldObj };

    if (value == 1) {
      object.hideOption = "workspace";
      object.settings.isHidden = false;

      object.settings.hiddenInWorkspaces = [
        profileState.loggedInTeam,
        ...object.settings.hiddenInWorkspaces,
      ];
      object.settings.hiddenInGroup =
        object.settings.hiddenInGroup.indexOf("risk") < 0
          ? ["risk", ...object.settings.hiddenInGroup]
          : object.settings.hiddenInGroup;
    }
    if (value == 2) {
      /** if user select the team level */
      object.hideOption = "allWorkspaces";
      object.settings.isHidden = true;
      object.settings.hiddenInWorkspaces = [];
      object.settings.hiddenInGroup =
        object.settings.hiddenInGroup.indexOf("risk") < 0
          ? ["risk", ...object.settings.hiddenInGroup]
          : object.settings.hiddenInGroup;
    }
    delete object.team;
    delete object.workspaces;
    hideCustomField(
      object,
      res => { },
      failure => { }
    );
  };

  const handleSystemOptionSelect = (value, selectedFieldObj) => {
    let object = {
      fieldId: selectedFieldObj.fieldId
    };
    if (value == 0) {
      object.isHidden = false;
      object.IsHiddenInWorkspace = false;
      object.IsHiddenInTeam = false;
    }
    if (value == 1) {
      object.IsHiddenInWorkspace = true;
    }
    if (value == 2) {
      object.IsHiddenInTeam = true;
    }
    updateFeatureAccessPermissions(
      'risk',
      object,
      succ => {
      },
      fail => {
      }
    );
  };

  const generateTaskSelectedVal = () => {
    /** function for generating the selected task list objects from task id and return taskList for showing pre populated selected task if any => function call in componentDidMount */
    const selectedTaskList = allTasks.filter(task => {
      return selectedRisk.linkedTasks.indexOf(task.taskId) > -1;
    });
    let taskList = generateTaskData(selectedTaskList);
    return taskList;
  };
  // generate likeli hood data 
  const generateLikelyhoodSelectedVal = () => {
    /** function for generating the selected task list objects from task id and return taskList for showing pre populated selected task if any => function call in componentDidMount */
    let likelihoodList = likelihoodData(intl);
    const selectedTaskList = likelihoodList.filter(task => {
      return selectedRisk.likelihood.indexOf(task.value) > -1;
    })
    // let taskList = generateTaskData(selectedTaskList);
    return selectedTaskList;
  };

  const handleEditField = field => {
    updateCustomFieldDialogState({ moduleViewType: "editCopyModal", data: field, mode: "edit" });
  };
  const handleCopyField = field => {
    updateCustomFieldDialogState({ moduleViewType: "editCopyModal", data: field, mode: "copy" });
  };
  const riskNotifications = () => {
    const { riskNotifications, userId, members } = props;
    let notifications = riskNotifications.filter(x => {
      return x.users.filter(y => y.userId === userId && y.isViewed === false).length;
    });
    notifications = notifications
      .map(x => {
        x.createdName = members.find(m => m.userId === x.createdBy);
        x.users = x.users.map(u => ({
          userId: u.userId,
          isViewed: u.isViewed,
        }));
        return x;
      })
      .map(x => ({
        id: x.notificationId,
        title: x.description ? x.description : " ",
        date: helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(x.updatedDate || x.createdDate),
        time: helper.RETURN_CUSTOMDATEFORMATFORTIME(x.updatedDate || x.createdDate),
        profilePic: x.createdName ? x.createdName.imageUrl : "",
        createdName: x.createdName,
        notificationUrl: x.notificationUrl,
        teamName: x.teamName || "N/A",
        teamId: x.teamId,
        workspaceName: x.workSpaceName || "N/A",
        workspaceId: x.workSpaceId,
      }));
    return notifications;
  };
  const handleAllRead = () => {
    if (readAll) {
      setReadAll(false);
      MarkRiskNotificationsAsRead(selectedRisk.id, props.userId, (err, data) => {
        setReadAll(true);
        riskDetailDialog("");
      });
    }
  };
  const updateRiskOwner = (assignedTo, risk) => {
    // let newRiskObj = { ...risk, riskOwner: assignedTo[0] && assignedTo[0].userId };
    // UpdateRisk(newRiskObj, () => { });
    const ownerId = assignedTo.length ? assignedTo[0].userId : "none";
    UpdateRiskData(selectedRisk, "riskOwner", ownerId);
  };

  //  update status
  const handleStatusChange = (status, obj) => {
    UpdateRiskData(selectedRisk, "status", status.value);
  };

  const riskAssigneeList = selectedRisk
    ? members.filter(member => {
      return selectedRisk.riskOwner == member.userId;
    })
    : [];
  const chatConfig = {
    contextUrl: "communication",
    contextView: "Risk",
    contextId: selectedRisk ? selectedRisk.id : "",
    contextKeyId: "riskId",
    contextChat: "single",
    assigneeLists: riskAssigneeList,
  };

  const riskPermission =
    selectedRisk && selectedRisk.projectId
      ? props.permissionObject[selectedRisk.projectId]
        ? props.permissionObject[selectedRisk.projectId].risk
        : workspaceRiskPer
      : workspaceRiskPer;

  const chatPermission = {
    addAttachment:
      riskPermission && !selectedRisk.isArchive
        ? riskPermission.riskDetails.riskComments.riskAttachment.isAllowAdd
        : false,
    deleteAttachment:
      riskPermission && !selectedRisk.isArchive
        ? riskPermission.riskDetails.riskComments.riskAttachment.isAllowDelete
        : false,
    downloadAttachment:
      riskPermission && !selectedRisk.isArchive
        ? riskPermission.riskDetails.riskComments.downloadAttachment.cando
        : false,
    addComments:
      riskPermission && !selectedRisk.isArchive
        ? riskPermission.riskDetails.riskComments.cando
        : false,
    editComment:
      riskPermission && !selectedRisk.isArchive
        ? riskPermission.riskDetails.riskComments.comments.isAllowEdit
        : false,
    deleteComment:
      riskPermission && !selectedRisk.isArchive
        ? riskPermission.riskDetails.riskComments.comments.isAllowDelete
        : false,
    convertToTask:
      riskPermission && !selectedRisk.isArchive
        ? riskPermission.riskDetails.riskComments.convertToTask.cando
        : false,
    reply:
      riskPermission && !selectedRisk.isArchive
        ? riskPermission.riskDetails.riskComments.reply.cando
        : false,
    replyLater:
      riskPermission && !selectedRisk.isArchive
        ? riskPermission.riskDetails.riskComments.replyLater.cando
        : false,
    showReceipt:
      riskPermission && !selectedRisk.isArchive
        ? riskPermission.riskDetails.riskComments.showReceipt.cando
        : false,
    clearAll:
      riskPermission && !selectedRisk.isArchive
        ? riskPermission.riskDetails.riskComments.clearAllReplies.cando
        : false,
    clearReply:
      riskPermission && !selectedRisk.isArchive
        ? riskPermission.riskDetails.riskComments.clearReply.cando
        : false,
  };
  const docPermission = {
    uploadDocument:
      riskPermission && !selectedRisk.isArchive
        ? riskPermission.riskDetails.riskDocuments.uploadDocument.cando
        : false,
    addFolder:
      riskPermission && !selectedRisk.isArchive
        ? riskPermission.riskDetails.riskDocuments.addFolder.cando
        : false,
    downloadDocument:
      riskPermission && !selectedRisk.isArchive
        ? riskPermission.riskDetails.riskDocuments.downloadDocument.cando
        : false,
    viewDocument:
      riskPermission && !selectedRisk.isArchive
        ? riskPermission.riskDetails.riskDocuments.cando
        : false,
    deleteDocument:
      riskPermission && !selectedRisk.isArchive
        ? riskPermission.riskDetails.riskDocuments.deleteDocument.cando
        : false,
    openFolder:
      riskPermission && !selectedRisk.isArchive
        ? riskPermission.riskDetails.riskDocuments.openFolder.cando
        : false,
    renameFolder:
      riskPermission && !selectedRisk.isArchive
        ? riskPermission.riskDetails.riskDocuments.renameFolder.cando
        : false,
  };
  const riskTitle =
    riskPermission && !selectedRisk.isArchive
      ? riskPermission.riskDetails.editRiskName.isAllowEdit
      : false;
  const canEditRiskDescription =
    riskPermission && !selectedRisk.isArchive
      ? riskPermission.riskDetails.riskDescription.isAllowEdit
      : false;
  const canAddRiskOwner =
    riskPermission && !selectedRisk.isArchive ? riskPermission.riskDetails.riskOwner.cando : false;
  const canEditRiskTask =
    riskPermission && !selectedRisk.isArchive
      ? riskPermission.riskDetails.riskTask.isAllowEdit
      : true;
  const canEditRiskProject =
    riskPermission && !selectedRisk.isArchive
      ? riskPermission.riskDetails.riskProject.isAllowEdit
      : true;
  const canEditLikelihood =
    riskPermission && !selectedRisk.isArchive
      ? riskPermission.riskDetails.editlikelihood.isAllowEdit
      : true;
  const canEditImpact =
    riskPermission && !selectedRisk.isArchive
      ? riskPermission.riskDetails.editRiskImpact.isAllowEdit
      : true;
  const canEditMitigationPlan =
    riskPermission && !selectedRisk.isArchive
      ? riskPermission.riskDetails.riskMitigationPlan.isAllowEdit
      : true;
  const canHideField =
    riskPermission && !selectedRisk.isArchive
      ? riskPermission.riskDetails.isHideField.cando
      : false;
  const canUpdateField =
    riskPermission && !selectedRisk.isArchive
      ? riskPermission.riskDetails.isUpdateField.cando
      : false;
  const canUseField =
    riskPermission && !selectedRisk.isArchive ? riskPermission.riskDetails.isUseField.cando : false;

  let systemFieldsDropdowns = getCustomFields(customFields, profileState, "risk").filter(
    f => f.fieldType == "dropdown"
  );
  let systemFieldsTextarea = getCustomFields(customFields, profileState, "risk").filter(
    f => f.fieldType == "textarea"
  );
  const impactDropdown = systemFieldsDropdowns.find(d => d.fieldName === "Impact");
  const likelihoodDd = systemFieldsDropdowns.find(d => d.fieldName === "Likelihood");
  const taskDropdown = systemFieldsDropdowns.find(d => d.fieldName === "Task");
  const projectDropdown = systemFieldsDropdowns.find(d => d.fieldName === "Project");
  const detailsField = systemFieldsTextarea.find(d => d.fieldName === "Details");
  const mitigationPlanField = systemFieldsTextarea.find(d => d.fieldName === "Mitigation Plan");
  let notifications = riskNotifications();
  const customFieldPermission =
    riskPermission && !selectedRisk.isArchive
      ? riskPermission.riskDetails.customsFields
      : {
        isAllowEdit: false,
        isAllowDelete: false,
        isAllowAdd: false,
      };
  const showSnackbar = (snackBarMessage, type) => {
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  const generateAssessmentChartData = (data = []) => {
    const alphabets = "ABCDEFGHIJ";
    return data.map((a, i) => {
      return {
        id: "assessment" + i,
        color: "hsl(302, 70%, 50%)",
        data: [
          {
            y: a.columnValue,
            x: alphabets.indexOf(a.rowValue) + 1,
            obj: a,
          },
        ],
      };
    });
  };
  const selectedMatrixObj =
    customFields.data.find(
      m =>
        m.settings.assessment &&
        m.settings.assessment.includes(profileState.loggedInTeam) &&
        !m.settings.isHidden &&
        m.settings.hiddenInWorkspaces.indexOf(profileState.loggedInTeam) == -1 &&
        m.settings.hiddenInGroup.indexOf("risk") == -1
    ) || false;
  const assessmentMatrixValues =
    (selectedRisk.customFieldData &&
      selectedRisk.customFieldData.find(c => c.fieldId === selectedMatrixObj.fieldId)) ||
    false;

  const filterDataByDay = () => {
    return assessmentMatrixValues && assessmentMatrixValues.fieldData.data
      ? assessmentMatrixValues.fieldData.data.reduce((r, cv) => {
        const sameDayAssessmentIndex = r.findIndex(v => {
          return moment(v.createdDate).diff(cv.createdDate, "day") == 0;
        });
        if (
          sameDayAssessmentIndex > -1 &&
          moment(cv.createdDate).diff(r[sameDayAssessmentIndex].createdDate, "seconds") > -1
        ) {
          r[sameDayAssessmentIndex] = cv;
        } else {
          r.push(cv);
        }
        return r;
      }, [])
      : [];
  };
  const assessmentChartData = generateAssessmentChartData(filterDataByDay());

  const getSelectedProject = () => {
    if (selectedRisk.projectId) {
      const selectedProject = projects.find(project => {
        return project.projectId == selectedRisk.projectId;
      });

      return selectedProject
        ? [
          {
            label: selectedProject.projectName,
            id: selectedProject.projectId,
            obj: {},
          },
        ]
        : [];
    } else return [];
  };
  const handleSelectChange = (newValue, risk) => {
    const projectId = newValue.length == 0 ? "" : newValue[newValue.length - 1].id;

    let linkTask = selectedTasks.length
      ? selectedTasks[0].obj.projectId == "" || !selectedTasks[0].obj.projectId
        ? []
        : selectedRisk.linkedTasks
      : selectedRisk.linkedTasks;

    const newRiskObj = {
      ...selectedRisk,
      projectId: projectId,
      linkedTasks: newValue.length == 0 ? [] : linkTask,
    };

    // UpdateRisk(newRiskObj, () => { });
    UpdateRiskData(selectedRisk, 'projectId', projectId);
  };

  const statusArr = statusData(theme, classes, intl);
  const generateStatusData = (statusArr) => {
    return statusArr.map(item => {
      return {
        label: item.value,
        statusColor: item.color,
        value: item.value,
        icon: <CircularIcon htmlColor={item.color} style={{ fontSize: 16 }} />,
        obj: item,
        statusTitle: item.label,
      };
    });
  };
  let riskStatusData = generateStatusData(statusArr);

  let selectedStatus = selectedRisk
    ? riskStatusData.find((item) => item.value == selectedRisk.status) || riskStatusData[0]
    : riskStatusData[0];



  return selectedRisk ? (
    <DetailsDialog
      dialogProps={{
        open: dialogsState.riskDetailDialog.id !== "",
        onClose: () => {
          handleClose();
        },
        disableBackdropClick: true,
        disableEscapeKeyDown: true,
      }}
      headerRenderer={
        <>
          <CanAccessFeature group='risk' feature='status'>
            <div>
              <StatusDropdown
                onSelect={status => {
                  handleStatusChange(status, selectedRisk);
                }}
                buttonType={"listButton"}
                iconButton={false}
                option={selectedStatus}
                options={riskStatusData}
                toolTipTxt={selectedStatus.label}
                writeFirst={true}
                disabled={!riskPermission.riskDetails.editRiskStatus.isAllowEdit}
                dropdownProps={{
                  disablePortal: true,
                }}
                style={{ marginRight: 8 }}
                legacyButton={true}
                btnStyle={{
                  padding: "8px 0px",
                  marginRight: 15,
                  height: 32,
                }}
              />
            </div>
          </CanAccessFeature>
          <CanAccessFeature group='risk' feature='riskOwner'>
            <AssigneeDropdown
              assignedTo={riskAssigneeList}
              updateAction={updateRiskOwner}
              isArchivedSelected={selectedRisk.isArchive}
              obj={selectedRisk}
              singleSelect={true}
              assigneeHeading={"Risk Owner"}
              addPermission={canAddRiskOwner}
              deletePermission={canAddRiskOwner}
              customIconButtonProps={{
                classes: {
                  root: classes.iconBtnStyles,
                },
              }}
              avatarSize={"xsmall"}
            />
          </CanAccessFeature>
          <div className={classes.vl}></div>
          <NotificationMenu
            notificationsData={notifications}
            MarkAllNotificationsAsRead={handleAllRead}
            history={props.history}
            customButtonProps={{
              style: {
                marginRight: 10,
                minWidth: 0,
              },
            }}
          />

          <RiskActionDropdown
            risk={selectedRisk}
            selectedColor={selectedRisk.colorCode ? selectedRisk.colorCode : ""}
            isUpdated={UpdateRisk}
            DeleteRisk={DeleteRisk}
            updateColor={updateColor}
            closeActionMenu={() => {
              handleClose();
              closeActionMenu();
            }}
            isArchivedSelected={selectedRisk.isArchive}
            filterRisk={filterRisk}
            riskPer={riskPermission}
            btnStyles={{
              marginRight: 10,
            }}
            iconStyles={{
              transform: "rotate(90deg)",
            }}
          />
        </>
      }
      leftSideContent={
        <>
          <div className={classes.createdBy}>
            <span>
              <b>{`#${selectedRisk.uniqueId}`}</b>
              {` by ${selectedRisk.createdBy} on ${moment(selectedRisk.createdDate).format(
                "ll"
              )} at ${moment(selectedRisk.createdDate).format("LT")}`}
            </span>
            <EditableTextField
              title={selectedRisk.title}
              module={"Risk"}
              handleEditTitle={handleUpdateTitle}
              permission={riskTitle}
              defaultProps={{
                inputProps: { maxLength: 250 },
              }}
            />
          </div>
          <div className={classes.systemFields}>
            <div>
              <div
                className={classes.systemFieldsHeading}
                style={{ background: "#89A3B2" }}
                onClick={() => {
                  handleShowSystemField();
                }}>
                <Typography variant="h2" className={classes.systemFieldTitle}>
                  System Fields
                </Typography>
                <div>
                  {sFSecOpen ? (
                    <RemoveIcon
                      className={classes.systemFieldRemoveIcon}
                      onClick={() => {
                        handleShowSystemField();
                      }}
                    />
                  ) : (
                    <AddIcon
                      className={classes.systemFieldAddIcon}
                      onClick={() => {
                        handleShowSystemField();
                      }}
                    />
                  )}
                </div>
              </div>
              {sFSecOpen && (
                <Grid container spacing={1} classes={{ container: classes.sectionCnt }}>
                  {systemFieldsDropdowns.map(field => {
                    return field.isSystem &&
                      field.fieldName !== "Task" &&
                      field.fieldName !== "Impact" &&
                      field.fieldName !== "Project" &&
                      field.fieldName !== "Likelihood" ? (
                      <Grid item sm={12} md={6} key={field.fieldId}>
                        <CustomFieldsDropDown
                          groupType={"risk"}
                          disabled={selectedRisk.isArchive}
                          permission={canUpdateField}
                          customFieldChange={customFieldChange}
                          customFieldData={selectedRisk.customFieldData}
                          field={field}
                          handleDeleteField={() => { }}
                          handleEdit={handleEditField}
                          handleCopyField={handleCopyField}
                          handleSelectHideOption={handleSelectHideOption}
                          customfieldlabelProps={{
                            deleteOperation: false,
                            editOperation: customFieldPermission.isAllowEdit,
                            copyOperation: customFieldPermission.isAllowAdd,
                            hideOperation: canHideField,
                            customIconProps: {
                              color: "#999",
                            },
                          }}
                          customDropDownProps={{
                            isDisabled:
                              selectedRisk.isArchive || !customFieldPermission.isAllowEdit,
                          }}
                        />
                      </Grid>
                    ) : (
                      <> </>
                    );
                  })}
                  {CanAccess({ group: 'risk', feature: 'impact' }) && (
                    <Grid item sm={12} md={6}>
                      <div className={classes.textFieldCnt}>
                        <CustomFieldLabelCmp
                          label={"Impact"}
                          iconType={"dropdown"}
                          handleDelete={() => { }}
                          handleEdit={() => { }}
                          handleCopy={() => { }}
                          handleSelectHideOption={option =>
                            handleSystemOptionSelect(option, accessFeaturesRisk['impact'])
                          }
                          deleteOperation={false}
                          editOperation={false}
                          copyOperation={false}
                          hideOperation={canHideField}
                          customIconProps={{
                            color: "#999",
                          }}
                        />
                        <SelectSearchDropdown
                          data={() => impactData(theme, classes, intl)}
                          selectChange={handleOptionsSelect}
                          type="impact"
                          selectedValue={impactData(theme, classes, intl).find(s => {
                            return s.value == selectedRisk.impact;
                          })}
                          placeholder={"Select"}
                          icon
                          isMulti={false}
                          isDisabled={!canEditImpact || selectedRisk.isArchive}
                          selectRemoveValue={(type, option) => {
                            handleSelectChange(option, type);
                          }}
                          styles={{ marginBottom: 0, marginTop: 0, flex: 1 }}
                          customStyles={{
                            control: {
                              border: "none",
                              // padding: "0px 5px",
                              fontFamily: theme.typography.fontFamilyLato,
                              fontWeight: theme.typography.fontWeightExtraLight,
                              "&:hover": {
                                background: `${theme.palette.background.items} !important`,
                              },
                              minHeight: 32,
                            },
                          }}
                          writeFirst={true}
                        />
                      </div>
                    </Grid>
                  )}
                  {CanAccess({ group: 'risk', feature: 'likelihood' }) && (
                    <Grid item sm={12} md={6}>
                      <div className={classes.textFieldCnt}>
                        <CustomFieldLabelCmp
                          label={"Likelihood"}
                          iconType={"dropdown"}
                          handleDelete={() => { }}
                          handleEdit={() => { }}
                          handleCopy={() => { }}
                          handleSelectHideOption={option =>
                            handleSystemOptionSelect(option, accessFeaturesRisk['likelihood'])
                          }
                          deleteOperation={false}
                          editOperation={false}
                          copyOperation={false}
                          hideOperation={canHideField}
                          customIconProps={{
                            color: "#999",
                          }}
                        />
                        <SelectSearchDropdown
                          data={() => likelihoodData(intl)}
                          selectChange={handleOptionsSelect}
                          type={"likelihood"}
                          selectedValue={likelihoodData(intl).find(s => {
                            return s.value == selectedRisk.likelihood;
                          })}
                          placeholder={"Select"}
                          isMulti={false}
                          isDisabled={!canEditLikelihood || selectedRisk.isArchive}
                          optionBackground={true}
                          styles={{ marginBottom: 0, marginTop: 0, flex: 1 }}
                          customStyles={{
                            control: {
                              border: "none",
                              // padding: "0px 5px",
                              fontFamily: theme.typography.fontFamilyLato,
                              fontWeight: theme.typography.fontWeightExtraLight,
                              "&:hover": {
                                background: `${theme.palette.background.items} !important`,
                              },
                              minHeight: 32,
                            },
                            placeholder: {
                              color: "#7E7E7E",
                            },
                          }}
                          writeFirst={true}
                        />
                      </div>
                    </Grid>
                  )}

                  {CanAccess({ group: 'risk', feature: 'tasks' }) && (
                    <Grid item sm={12} md={6}>
                      <div className={classes.textFieldCnt}>
                        <CustomFieldLabelCmp
                          label={"Task"}
                          iconType={"dropdown"}
                          handleDelete={() => { }}
                          handleEdit={() => { }}
                          handleCopy={() => { }}
                          handleSelectHideOption={option =>
                            handleSystemOptionSelect(option, accessFeaturesRisk['tasks'])
                          }
                          deleteOperation={false}
                          editOperation={false}
                          copyOperation={false}
                          hideOperation={canHideField}
                          customIconProps={{
                            color: "#999",
                          }}
                        />
                        <SelectSearchDropdown
                          data={() => {
                            return generateTaskDData;
                          }}
                          placeholder={"Select"}
                          optionBackground={true}
                          isMulti={true}
                          selectChange={handleTaskChange}
                          selectedValue={generateTaskSelectedVal()}
                          selectRemoveValue={handleClearTask}
                          styles={{ marginBottom: 0, marginTop: 0, flex: 1 }}
                          customStyles={{
                            control: {
                              border: "none",
                              // padding: "0px 5px",
                              fontFamily: theme.typography.fontFamilyLato,
                              fontWeight: theme.typography.fontWeightExtraLight,
                              "&:hover": {
                                background: `${theme.palette.background.items} !important`,
                              },
                              minHeight: 32,
                            },
                            placeholder: {
                              color: "#7E7E7E",
                            },
                          }}
                          isDisabled={!canEditRiskTask || selectedRisk.isArchive}
                          writeFirst={false}
                        />
                      </div>
                    </Grid>
                  )}

                  {CanAccess({ group: 'risk', feature: 'project' }) && (
                    <Grid item sm={12} md={6}>
                      <div className={classes.textFieldCnt}>
                        <CustomFieldLabelCmp
                          label={"Project"}
                          iconType={"dropdown"}
                          handleDelete={() => { }}
                          handleEdit={() => { }}
                          handleCopy={() => { }}
                          handleSelectHideOption={option =>
                            handleSystemOptionSelect(option, accessFeaturesRisk['project'])
                          }
                          deleteOperation={false}
                          editOperation={false}
                          copyOperation={false}
                          hideOperation={canHideField}
                          customIconProps={{
                            color: "#999",
                          }}
                        />
                        <SelectSearchDropdown
                          data={() => {
                            return generateProjectDData;
                          }}
                          placeholder={"Select"}
                          optionBackground={true}
                          isMulti={true}
                          selectChange={(type, option) => {
                            handleSelectChange(option, type);
                          }}
                          selectedValue={getSelectedProject()}
                          selectRemoveValue={(type, option) => {
                            handleSelectChange(option, type);
                          }}
                          styles={{ marginBottom: 0, marginTop: 0, flex: 1 }}
                          customStyles={{
                            control: {
                              border: "none",
                              // padding: "0px 5px",
                              fontFamily: theme.typography.fontFamilyLato,
                              fontWeight: theme.typography.fontWeightExtraLight,
                              "&:hover": {
                                background: `${theme.palette.background.items} !important`,
                              },
                              minHeight: 32,
                            },
                            placeholder: {
                              color: "#7E7E7E",
                            },
                          }}
                          isDisabled={!canEditRiskProject || selectedRisk.isArchive}
                          writeFirst={false}
                        />
                      </div>
                    </Grid>
                  )}

                  {CanAccess({ group: 'risk', feature: 'details' }) && (
                    <Grid item sm={12} md={12}>
                      <RiskDescriptionEditor
                        type={"Details"}
                        keyType="detail"
                        currentRisk={selectedRisk}
                        isUpdated={UpdateRisk}
                        view="risk"
                        handleUpdateDetails={UpdateRiskData}
                        isArchivedSelected={selectedRisk.isArchive || !canEditRiskDescription}
                        customfieldlabelProps={{
                          editOperation: false,
                          copyOperation: false,
                          deleteOperation: false,
                          hideOperation: canHideField,
                          customIconProps: {
                            color: "#999",
                          },
                          handleSelectHideOption: option =>
                            handleSystemOptionSelect(option, accessFeaturesRisk['details'])
                        }}
                      />
                    </Grid>
                  )}
                  {CanAccess({ group: 'risk', feature: 'mitigationPlan' }) && (
                    <Grid item sm={12} md={12}>
                      <RiskDescriptionEditor
                        type={"Mitigation Plan"}
                        keyType="mittigation"
                        currentRisk={selectedRisk}
                        isUpdated={UpdateRisk}
                        handleUpdateDetails={UpdateRiskData}
                        view="risk"
                        isArchivedSelected={selectedRisk.isArchive || !canEditMitigationPlan}
                        customfieldlabelProps={{
                          editOperation: false,
                          copyOperation: false,
                          deleteOperation: false,
                          hideOperation: canHideField,
                          customIconProps: {
                            color: "#999",
                          },
                          handleSelectHideOption: option =>
                            handleSystemOptionSelect(option, accessFeaturesRisk['mitigationPlan'])
                        }}
                      />
                    </Grid>
                  )}
                  <Grid item sm={12} md={12}>
                    <CustomFieldsChecklistView
                      groupType={"risk"}
                      groupId={selectedRisk.id}
                      handleClickHideOption={handleClickHideOption}
                      permission={{
                        ...customFieldPermission,
                        isAllowUpdate: canUpdateField,
                        isAllowHide: canHideField,
                        isUseField: canUseField,
                      }}
                      isSystem={true}
                      isArchivedSelected={selectedRisk.isArchive}
                      customfieldlabelProps={{
                        editOperation: false,
                        copyOperation: customFieldPermission.isAllowAdd,
                        deleteOperation: false,
                        hideOperation: canHideField,
                        customIconProps: {
                          color: "#999",
                        },
                      }}
                      renderFieldsWithoutSec={false}
                      handleEdit={handleEditField}
                      handleCopyField={handleCopyField}
                      handleDeleteField={() => { }}
                      handleSelectHideOption={handleSelectHideOption}
                    />
                  </Grid>
                </Grid>
              )}

              {assessmentDialogOpen && (
                <UpdateMatrixDialog
                  saveOption={customFieldChange}
                  open={assessmentDialogOpen}
                  feature={"risk"}
                  closeAction={() => handleAddAssessmentDialog(false)}
                  selectedValues={
                    assessmentMatrixValues ? assessmentMatrixValues.fieldData.data : []
                  }
                  assessMatrix={true}
                />
              )}

              {selectedMatrixObj && sFSecOpen && (
                <>
                  <div className={classes.assessmentGraphHeadingCnt}>
                    <Typography
                      variant="heading3"
                      style={{
                        fontSize: 13,
                        fontFamily: theme.typography.fontFamilyLato,
                        fontWeight: theme.typography.fontWeightExtraLight,
                        color: "#505050",
                        display: "flex",
                        alignItems: "center",
                      }}>
                      <CustomFieldIcons
                        value={"matrix"}
                        color={"#999999"}
                        customProps={{ className: classes.iconStyle }}
                      />
                      Assessment
                    </Typography>
                    <CustomButton
                      variant="contained"
                      btnType="blue"
                      onClick={() => handleAddAssessmentDialog(true)}
                      disabled={!customFieldPermission.isAllowEdit || selectedRisk.isArchive}>
                      Add Assessment
                    </CustomButton>
                  </div>
                  <div style={{ height: 250, position: 'relative' }}>
                    <MyResponsiveLine
                      data={assessmentChartData}
                      chartProps={{ pointSize: selectedMatrixObj.settings.gridSize }}
                      yScale={{ max: selectedMatrixObj.settings.gridSize, min: 1 }}
                      xScale={{ max: selectedMatrixObj.settings.gridSize, min: 1 }}
                      axisLeft={{
                        // legend: selectedMatrixObj && selectedMatrixObj.settings.columns.title,
                        legend: "",
                        legendOffset: -30,
                        legendPosition: "middle",
                      }}
                      axisBottom={{
                        legend: selectedMatrixObj && selectedMatrixObj.settings.rows.title,
                        legendOffset: 35,
                        legendPosition: "middle",
                      }}
                    />
                    {selectedMatrixObj && <span title={selectedMatrixObj.settings.columns.title} className={classes.verticalText}>{selectedMatrixObj.settings.columns.title}</span>}
                  </div>
                </>
              )}
            </div>

            <SectionsView
              type={"risk"}
              disabled={selectedRisk.isArchive}
              customFieldData={selectedRisk.customFieldData}
              permission={{
                ...customFieldPermission,
                isAllowUpdate: canUpdateField,
                isAllowHide: canHideField,
                isUseField: canUseField,
              }}
              customFieldChange={customFieldChange}
              handleEditField={handleEditField}
              handleCopyField={handleCopyField}
              groupId={selectedRisk.id}
              handleClickHideOption={handleClickHideOption}
              handleSelectHideOption={handleSelectHideOption}
            />
          </div>
          {teamCanView("customFieldAccess") && (
            <div className={classes.addCustomFieldCnt}>
              <CustomFieldView
                feature="risk"
                // onFieldAdd={this.addCustomFieldCallback}
                onFieldEdit={editCustomFieldCallback}
                onFieldDelete={() => { }}
                permission={{
                  ...customFieldPermission,
                  isAllowUpdate: canUpdateField,
                  isAllowHide: canHideField,
                  isUseField: canUseField,
                }}
                disableAssessment={false}
                disabled={selectedRisk.isArchive}
              />
            </div>
          )}
        </>
      }
      rightSideContent={
        <>
          <SideTabs
            MenuData={selectedRisk}
            members={members}
            riskActivitiesData={riskActivities}
            riskPer={riskPermission}
            onChatMount={() => { }}
            onChatUnMount={() => { }}
            chatConfig={chatConfig}
            chatPermission={chatPermission}
            docConfig={chatConfig}
            docPermission={docPermission}
            selectedTab={0}
            intl={intl}
          />
        </>
      }
    />
  ) : (
    <> </>
  );
}
NewDetailView.defaultProps = {
  closeActionMenu: () => { },
  profileState: {},
  members: [],
  handleEditTitle: () => { },
  UpdateRisk: () => { },
  DeleteRisk: () => { },
  isUpdated: () => { },
  editCustomField: () => { },
  updateCustomFieldData: () => { },
  MarkRiskNotificationsAsRead: () => { },
  updateCustomFieldDialogState: () => { },
  updateRisk: () => { },
  dispatchRisk: () => { },
  dialogsState: {},
  riskActivities: [],
  intl: {},
  theme: {},
};

const mapStateToProps = state => {
  return {
    profileState: state.profile.data || {},
    members: state.profile.data.member.allMembers || [],
    dialogsState: state.dialogStates || {},
    risksState: state.risks || [],
    appliedFiltersState: state.appliedFilters || {},
    allTasks: state.tasks.data || [],
    workspaceRiskPer: state.workspacePermissions.data.risk || {},
    projects: state.projects.data || [],
    customFields: state.customFields,
    riskActivities: state.riskActivities.data || [],
    userId: state.profile.data.userId || "",
    riskNotifications: state.riskNotifications.data || [],
    archivedRisks: state.archivedItems.data,
    permissionObject: ProjectPermissionSelector(state),
    accessFeaturesRisk: state.featureAccessPermissions.risk,
  };
};

export default compose(
  injectIntl,
  withRouter,
  withSnackbar,
  withStyles(newDetailStyles, { withTheme: true }),
  connect(mapStateToProps, {
    riskDetailDialog,
    UpdateRisk,
    DeleteRisk,
    editCustomField,
    updateCustomFieldData,
    updateRisk,
    dispatchRisk,
    updateCustomFieldDialogState,
    MarkRiskNotificationsAsRead,
    hideCustomField,
    updateFeatureAccessPermissions
  })
)(NewDetailView);
