export const columns = {
  riskNone: {
    key: "",
    name: "None"
  },
  id: {
    key: "riskId",
    name: "ID"
  },
  riskTitle: {
    key: "title",
    name: "Title"
  },
  riskStatus: {
    key: "status",
    name: "Status"
  },
  riskTask: {
    key: "task",
    name: "Task"
  },
  riskImpact: {
    key: "impact",
    name: "Impact"
  },
  riskLikelihood: {
    key: "likelihood",
    name: "Likelihood"
  },
  riskOwner: {
    key: "riskOwner",
    name: "Risk Owner"
  },
  updatedDate: {
    key: "updatedDate",
    name: "Last Updated"
  },
  createdBy: {
    key: "createdBy",
    name: "Created By"
  },
  createdDate: {
    key: "createdDate",
    name: "Creation Date"
  },
  projectName:{
    key:"project",
    name:"Project"
  }
}