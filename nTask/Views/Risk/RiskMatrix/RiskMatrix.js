import React, { useState, useEffect } from "react";
import "./styles.css";
import { GetRiskMatrix } from "../../../redux/actions/risks";
import { FormattedMessage } from "react-intl";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import MatrixGrid from "../../CustomFieldSettings/Matrix/matrixGrid.cmp";
import data from "../../WorkspaceSetting/data";
import riskMatrixStyles from "./styles";
import { withStyles } from "@material-ui/core/styles";
import useMatrixData from "./useMatrixData.hook";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Scrollbars from "react-custom-scrollbars";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { getActiveFields } from "../../../helper/getActiveCustomFields";
import { CanAccess } from "../../../components/AccessFeature/AccessFeature.cmp";

const CellDetailsRenderer = ({ data }, classes) => {
  return (
    <Scrollbars autoHide={false} autoHeight autoHeightMin={0} autoHeightMax={180}>
      <span className={classes.dropdownHeading}>Risks</span>
      <List>
        {data &&
          data.map((r, i) => {
            return (
              <ListItem className={classes.listItem} key={r.risk.id}>
                <span title={r.risk.title}>
                  <b>·</b> {r.risk.title}
                </span>
              </ListItem>
            );
          })}
      </List>
    </Scrollbars>
  );
};

function RiskMatrix(props) {
  const { applyFilter, customFields, classes, risks, profileState } = props;
  const [riskMatrix, setRiskMatrix] = useState([]);
  const [riskMatrixValues, setRiskMatrixValues] = useState([]);

  // const [matrix, setMatrix] = useState({});
  useEffect(() => {
    GetRiskMatrix(
      data => {
        setRiskMatrix(data);
      },
      error => {
        if (error.status === 500) {
          // self.showSnackBar('Server throws error','error');
        }
      }
    );
  }, []);

  const { matrix } = useMatrixData(customFields, profileState, 'risk');

  const plotRiskMatrix = () => {
    const matrixFields = matrix.map(m => {
      if (m.fieldType === "matrix") return m.fieldId;
    });
    let values = risks.data.reduce((r, cv) => {
      const filterMatrixData =
        cv.customFieldData && cv.customFieldData.filter(f => matrixFields.includes(f.fieldId));
      filterMatrixData &&
        filterMatrixData.forEach(c => {
          const latestValue = c.fieldData.data[c.fieldData.data.length - 1];
          const isFieldExist = matrixFields.includes(c.fieldId);
          const cellExist = r[c.fieldId] && r[c.fieldId][latestValue.cellName];
          if (isFieldExist && r[c.fieldId] && cellExist) {
            r[c.fieldId][latestValue.cellName].push({ latestValue, risk: cv });
          } else if (isFieldExist && r[c.fieldId] && !cellExist) {
            r[c.fieldId][latestValue.cellName] = [{ ...latestValue, risk: cv }];
          } else if (isFieldExist) {
            r[c.fieldId] = {};
            r[c.fieldId][latestValue.cellName] = [{ ...latestValue, risk: cv }];
          }
        });
      return r;
    }, {});
    return values;
  };
  const riskCounts = plotRiskMatrix();
  let matrixFieldArr = getActiveFields(matrix, profileState);
  const isDataFetched = riskMatrix.length ? true : false;
  let legacyMatrix = CanAccess({ group: 'risk', feature: 'impact' }) && CanAccess({ group: 'risk', feature: 'likelihood' });
  return (
    <div className={classes.riskMatrixCnt}>
      <Grid container spacing={3}>
        {matrixFieldArr.length
          ? matrixFieldArr.map(m => (
              <Grid xs={12} item>
                <div className={classes.matrixGridCnt}>
                  <Typography variant="h1" classes={{h1: classes.riskHeading}} align="left" style={{ marginBottom: 25 }}>
                    {m.fieldName}
                  </Typography>
                  <MatrixGrid
                    legend={true}
                    gridCounts={riskCounts[m.fieldId]}
                    colorSelect={false}
                    data={m}
                    hideDisabled={true}
                    CellDetailsRenderer={data => CellDetailsRenderer(data, classes)}
                  />
                </div>
              </Grid>
            ))
          : null}
       {legacyMatrix && <Grid item xs={12}>
          <div className="risk-matrix-box">
            <p className="likelihoodText">
              <FormattedMessage id="risk.common.likelihood.label" defaultMessage="Likelihood" />
            </p>
            <div className="responsive-table">
              <p className="impactText">
                <FormattedMessage
                  id="risk.creation-dialog.form.impact.label"
                  defaultMessage="Impact"
                />
              </p>
              <table className="table table-bordered table-responsive text-center">
                <tbody>
                  <tr>
                    <td />
                    <td className="td-top-header">
                      <FormattedMessage
                        id="risk.common.impact.dropdown.minor"
                        defaultMessage="Minor"
                      />
                    </td>
                    <td className="td-top-header">
                      <FormattedMessage
                        id="risk.common.impact.dropdown.moderate"
                        defaultMessage="Moderate"
                      />
                    </td>
                    <td className="td-top-header">
                      <FormattedMessage
                        id="risk.common.impact.dropdown.major"
                        defaultMessage="Major"
                      />
                    </td>
                    <td className="td-top-header">
                      <FormattedMessage
                        id="risk.common.impact.dropdown.critical"
                        defaultMessage="Critical"
                      />
                    </td>
                  </tr>

                  <tr>
                    <td className="td-left-header">76-100%</td>
                    <td
                      className="td-yellow"
                      onClick={() =>
                        applyFilter("76-100", "Minor", isDataFetched ? riskMatrix[0].minor : 0)
                      }>
                      {isDataFetched ? riskMatrix[0].minor : ""}
                    </td>
                    <td
                      className="td-red"
                      onClick={() =>
                        applyFilter(
                          "76-100",
                          "Moderate",
                          isDataFetched ? riskMatrix[0].moderate : 0
                        )
                      }>
                      {isDataFetched ? riskMatrix[0].moderate : ""}
                    </td>
                    <td
                      className="td-red"
                      onClick={() =>
                        applyFilter("76-100", "Major", isDataFetched ? riskMatrix[0].major : 0)
                      }>
                      {isDataFetched ? riskMatrix[0].major : ""}
                    </td>
                    <td
                      className="td-red"
                      onClick={() =>
                        applyFilter(
                          "76-100",
                          "Critical",
                          isDataFetched ? riskMatrix[0].critical : 0
                        )
                      }>
                      {isDataFetched ? riskMatrix[0].critical : ""}
                    </td>
                  </tr>

                  <tr>
                    <td className="td-left-header">51-75%</td>
                    <td
                      className="td-yellow"
                      onClick={() =>
                        applyFilter("51-75", "Minor", isDataFetched ? riskMatrix[1].minor : 0)
                      }>
                      {isDataFetched ? riskMatrix[1].minor : ""}
                    </td>
                    <td
                      className="td-yellow"
                      onClick={() =>
                        applyFilter("51-75", "Moderate", isDataFetched ? riskMatrix[1].moderate : 0)
                      }>
                      {isDataFetched ? riskMatrix[1].moderate : ""}
                    </td>
                    <td
                      className="td-yellow"
                      onClick={() =>
                        applyFilter("51-75", "Major", isDataFetched ? riskMatrix[1].major : 0)
                      }>
                      {isDataFetched ? riskMatrix[1].major : ""}
                    </td>
                    <td
                      className="td-red"
                      onClick={() =>
                        applyFilter("51-75", "Critical", isDataFetched ? riskMatrix[1].critical : 0)
                      }>
                      {isDataFetched ? riskMatrix[1].critical : ""}
                    </td>
                  </tr>

                  <tr>
                    <td className="td-left-header">26-50%</td>
                    <td
                      className="td-green"
                      onClick={() =>
                        applyFilter("26-50", "Minor", isDataFetched ? riskMatrix[2].minor : 0)
                      }>
                      {isDataFetched ? riskMatrix[2].minor : ""}
                    </td>
                    <td
                      className="td-green"
                      onClick={() =>
                        applyFilter("26-50", "Moderate", isDataFetched ? riskMatrix[2].moderate : 0)
                      }>
                      {isDataFetched ? riskMatrix[2].moderate : ""}
                    </td>
                    <td
                      className="td-yellow"
                      onClick={() =>
                        applyFilter("26-50", "Major", isDataFetched ? riskMatrix[2].major : 0)
                      }>
                      {isDataFetched ? riskMatrix[2].major : ""}
                    </td>
                    <td
                      className="td-yellow"
                      onClick={() =>
                        applyFilter("26-50", "Critical", isDataFetched ? riskMatrix[2].critical : 0)
                      }>
                      {isDataFetched ? riskMatrix[2].critical : ""}
                    </td>
                  </tr>

                  <tr>
                    <td className="td-left-header" riskMatrix-bind="text: likelyhoodRange">
                      0-25%
                    </td>
                    <td
                      className="td-green"
                      onClick={() =>
                        applyFilter("0-25", "Minor", isDataFetched ? riskMatrix[3].minor : 0)
                      }>
                      {isDataFetched ? riskMatrix[3].minor : ""}
                    </td>
                    <td
                      className="td-green"
                      onClick={() =>
                        applyFilter("0-25", "Moderate", isDataFetched ? riskMatrix[3].moderate : 0)
                      }>
                      {isDataFetched ? riskMatrix[3].moderate : ""}
                    </td>
                    <td
                      className="td-green"
                      onClick={() =>
                        applyFilter("0-25", "Major", isDataFetched ? riskMatrix[3].major : 0)
                      }>
                      {isDataFetched ? riskMatrix[3].major : ""}
                    </td>
                    <td
                      className="td-green"
                      onClick={() =>
                        applyFilter("0-25", "Critical", isDataFetched ? riskMatrix[3].critical : 0)
                      }>
                      {isDataFetched ? riskMatrix[3].critical : ""}
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </Grid>}
      </Grid>
    </div>
  );
}

const mapStateToProps = (state, ownProps) => {
  return {
    customFields: state.customFields,
    risks: state.risks,
    profileState: state.profile.data,
  };
};

export default compose(
  connect(mapStateToProps),
  withStyles(riskMatrixStyles, { withTheme: true })
)(RiskMatrix);
