import { useEffect, useState } from "react";

function useMatrixData(customFields, profileState, feature) {
  const [matrix, setMatrix] = useState([]);
  const generateData = obj => {
    const newMatrix = obj.settings.matrix.reduce((r, cv, index) => {
      const newArr = cv.filter((x, i) => {
        return !x.isDisabled;
      });
      if (newArr.length) r.push(newArr);
      return r;
    }, []);
    return newMatrix;
  };
  useEffect(() => {
    let matrixField = customFields.data.reduce((result, cv) => { 
      const isMatrix = cv.fieldType === "matrix";
      const isExist =
        cv.workspaces ? cv.workspaces.find(wf => wf.workspaceId == profileState.loggedInTeam && wf.groupType.includes(feature)) || cv.level == "team" : false;
        if(isMatrix && isExist) result.push(cv);
        return result
    }, []);
    // const matrixField = customFields.data.filter(m => m.fieldType === "matrix");

    let matrixArr;
    if (matrixField.length) {
      matrixArr = matrixField.map(m => {
        return { ...m, settings: { ...m.settings, matrix: generateData(m) } };
      });

      setMatrix(matrixArr);
    }
  }, [customFields.data]);

  return { matrix };
}

export default useMatrixData;
