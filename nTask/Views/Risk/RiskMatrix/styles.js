const riskMatrixStyles = theme => ({
  riskMatrixCnt: {
    padding: "32px 32px 32px 64px",
  },
  matrixGridCnt: {
    background: theme.palette.common.white,
    padding: "16px 36px 56px 70px",
    boxShadow: "0px 1px 2px #0000001F",
    minHeight: 420,
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  riskHeading: {
   fontSize: "24px !important",
   fontWeight: 600,
   fontFamily: "lato !important"
  },
  listItem: {
    fontFamily: theme.typography.fontFamilyLato,
    fontSize: "13px !important",
    padding: "3px 10px",
    display: "inline-block",
    width: "100%",
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
  },
  dropdownHeading: {
    padding: "10px 10px 0 10px",
    fontSize: "0.75em !important",
    fontWeight: theme.typography.fontWeightRegular,
    textTransform: "capitalize",
    color: theme.palette.secondary.light,
    display: "block",
  },
});

export default riskMatrixStyles;
