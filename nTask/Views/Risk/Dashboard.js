import React, { Component } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { withSnackbar } from "notistack";
import queryString from "query-string";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import dashboardStyles from "./styles";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import selectStyles from "../../assets/jss/components/select";
import combineStyles from "../../utils/mergeStyles";
import ListItemText from "@material-ui/core/ListItemText";
import RoundIcon from "@material-ui/icons/Brightness1";
import ArchivedIcon from "@material-ui/icons/Archive";
import SvgIcon from "@material-ui/core/SvgIcon";
import QuickFilterIcon from "../../components/Icons/QuickFilterIcon";
import CustomButton from "../../components/Buttons/CustomButton";
import ImportExportDD from "../../components/Dropdown/ImportExportDD";
import classNames from "classnames";
import Divider from "@material-ui/core/Divider";
import { FetchWorkspaceInfo } from "../../redux/actions/workspace";
import { setAppliedFilters } from "../../redux/actions/appliedFilters";
import RiskMatrix from "./RiskMatrix/RiskMatrix";
import { teamCanView } from "../../components/PlanPermission/PlanPermission";
import { saveSortingProjectList } from "../../redux/actions/projects";
import {
  getRisks,
  updateQuickFilter,
  showAllRisks,
  removeRiskQuickFilter,
  getArchivedData,
  updateRiskFilter,
} from "../../redux/actions/risks";
import { getTasks } from "../../redux/actions/tasks";
import { getProjects } from "../../redux/actions/projects";
import { updateArchivedItems } from "../../redux/actions/archivedItems";
import UnPlanned from "../billing/UnPlanned/UnPlanned";
import Risks from "../../assets/images/unplanned/Risks.png";
import PlainMenu from "../../components/Menu/menu";
import MenuList from "@material-ui/core/MenuList";
import { FormattedMessage, injectIntl } from "react-intl";
import { calculateContentHeight } from "../../utils/common";
import RiskList from "./List/riskList.view";
import ListLoader from "../../components/ContentLoader/List";
import ColumnSelectionDropdown from "../../components/Dropdown/SelectedItemsDropDown";
import { grid } from "../../components/CustomTable2/gridInstance";
import IconRefresh from "../../components/Icons/IconRefresh";
import DefaultTextField from "../../components/Form/TextField";
import isEmpty from "lodash/isEmpty";
import { doesFilterPass } from "./List/RiskFilter/riskFilter.utils";
import { CanAccessFeature } from "../../components/AccessFeature/AccessFeature.cmp";
import { TRIALPERIOD } from '../../components/constants/planConstant';
import { riskDetailDialog } from "../../redux/actions/allDialogs";

class RiskDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      alignment: "left",
      open: false,
      placement: "bottom-start",
      anchorEl: "",
      listView: true,
      openFilterSidebar: false,
      multipleFilters: "",
      renderRiskMatrix: false,
      isLoading: true,
      refreshBtnQuery: "",
    };
    this.allFilterList = [
      { key: "Show All", value: "Show All" },
      { key: "Identified", value: "Identified" },
      { key: "In Review", value: "In Review" },
      { key: "Agreed", value: "Agreed" },
      { key: "Rejected", value: "Rejected" },
      { key: "0-25", value: "0-25" },
      { key: "26-50", value: "26-50" },
      { key: "51-75", value: "51-75" },
      { key: "76-100", value: "76-100" },
      { key: "Archived", value: "Archived" },
    ];

    this.child = React.createRef();
    this.renderListView = this.renderListView.bind(this);
    this.renderRiskMatrix = this.renderRiskMatrix.bind(this);
  }

  getAllRisks = () => {
    this.props.getRisks(
      null,
      //success
      () => {
        setTimeout(() => {
          this.setState({ isLoading: false });
        }, 2000);
      },
      //failure
      () => { }
    );
  };
  componentDidMount() {
    this.getAllRisks();
  }
  componentDidUpdate(prevProps, prevState) {
    const {
      dialogsState: { riskDetailDialog },
    } = this.props;
    let searchQuery = this.props.history.location.search; //getting task id in the url
    if (searchQuery) {
      let riskId = queryString.parseUrl(searchQuery).query.riskId; //Parsing the url and extracting the task id using query string
      let riskFound = this.props.risksState.find(item => item.id == riskId);
      if (riskId && riskFound) {
        !riskDetailDialog.id &&
          this.props.riskDetailDialog({
            id: riskId,
            afterCloseCallBack: () => { },
            type: "comment",
          }, null);
      } else {
        this.showSnackbar("Oops! Risk not found.", "error");
        this.props.history.push("/risks");
      }
    }
  }
  showSnackbar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  renderListView = () => {
    this.setState({
      listView: true,
      renderRiskMatrix: false,
      alignment: "left",
    });
  };
  renderRiskMatrix = () => {
    this.setState({
      renderRiskMatrix: true,
      listView: false,
      alignment: "right",
    });
  };
  handleClick(event, placement) {
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }
  handleClose = () => {
    if (this.anchorEl.contains(event.target)) {
      return;
    }
    this.setState({ open: false });
  };
  handleAlignment = (event, alignment) => this.setState({ alignment });

  handleSelectClick = (value, type) => {
    const { riskFilters, updateQuickFilter } = this.props;
    const filterObj = { [value]: { type: value, selectedValue: [] } };
    if (riskFilters.Archived) {
      this.props.removeRiskQuickFilter(type);
      setTimeout(() => {
        !isEmpty(grid.grid) && grid.grid.redrawRows();
      }, 0);
      return;
    }
    if (value == "Archived") {
      const filterObj = { [value]: { type: value, selectedValues: [] } };
      this.props.getArchivedData(5, () => {
        updateQuickFilter(filterObj);
        setTimeout(() => {
          !isEmpty(grid.grid) && grid.grid.redrawRows();
        }, 0);
      });
      return;
    } else {
      const filterObj = !riskFilters[type]
        ? {
          ...riskFilters,
          [type]: {
            type: "",
            selectedValues: [value],
          },
        }
        : {
          ...riskFilters,
          [type]: {
            type: "",
            selectedValues: riskFilters[type]["selectedValues"].includes(value)
              ? riskFilters[type]["selectedValues"].filter(item => item !== value)
              : [...riskFilters[type]["selectedValues"], ...[value]],
          },
        };
      const objKeys = Object.keys(filterObj);
      const isFilterEmpty = objKeys.every(item => filterObj[item]["selectedValues"].length == 0);
      if (isFilterEmpty) {
        this.props.showAllRisks({});
      } else {
        this.props.updateQuickFilter(filterObj);
      }
    }
  };

  applyRiskMatrixFilter = (Likelihood, impact, value) => {
    if (value) {
      this.setState({ listView: true, renderRiskMatrix: false, alignment: "left" }, () => {
        // Apply Advance filter and replace previous values        
        let obj = {
          impact: {
            type: "",
            selectedValues: [impact]
          },
          likelihood: {
            type: "",
            selectedValues: [Likelihood]
          },

        }
        this.props.updateRiskFilter(obj, null);
      });
    }
  };

  translate = value => {
    let id = value;
    switch (value) {
      case "Identified":
        id = "risk.common.status.dropdown.identified";
        break;
      case "In Review":
        id = "risk.common.status.dropdown.in-review";
        break;
      case "Agreed":
        id = "risk.common.status.dropdown.agreed";
        break;
      case "Rejected":
        id = "risk.common.status.dropdown.rejected";
        break;
      case "Show":
        id = "common.show.label";
        break;
      case "Archived":
        id = "common.archived.archived";
        break;
      default:
        break;
    }
    return this.props.intl.formatMessage({ id: id, defaultMessage: value });
  };
  onColumnHide = obj => {
    if (grid.grid) {
      grid.grid.columnModel.applyColumnState({
        state: [
          {
            colId: obj.id,
            hide: obj.hide,
            rowGroup: obj.rowGroup,
          },
        ],
      });
    }
  };
  handleRefresh = () => {
    this.setState({ refreshBtnQuery: "progress", isLoading: true }, () => {
      this.props.getProjects(null, null, succ => {
        this.props.getTasks(null, null, succ => {
          this.props.getRisks(
            null,
            () => {
              this.setState({ refreshBtnQuery: "", isLoading: false });
              this.showSnackbar("Grid Refreshed Successfully!", "success");
            },
            () => {
              this.setState({ refreshBtnQuery: "", isLoading: false });
              this.showSnackbar("Oops! Server throws Error.", "error");
            }
          );
        })
      })
    });
  };
  handleRefreshList = () => {
    this.setState({ refreshBtnQuery: "progress", isLoading: true }, () => {
      this.props.getRisks(
        null,
        //success
        () => {
          this.setState({ refreshBtnQuery: "", isLoading: false });
          this.showSnackbar("Grid Refreshed Successfully!", "success");
        },
        //failure
        () => {
          this.setState({ refreshBtnQuery: "", isLoading: false });
          this.showSnackbar("Oops! Server throws Error.", "error");
        }
      );
    });
  };
  throttleHandleSearch = data => {
    grid.grid && grid.grid.setQuickFilter(data);
  };
  //handle task search
  handleSearch = e => {
    this.throttleHandleSearch(e.target.value);
  };
  handleShowAllRisks = () => {
    this.props.showAllRisks({});
    setTimeout(() => {
      !isEmpty(grid.grid) && grid.grid.redrawRows();
    }, 0);
  };
  componentWillUnmount() {
    this.props.showAllRisks({});
  }

  render() {
    const { classes, theme, riskFilters } = this.props;
    const {
      alignment,
      openFilterSidebar,
      multipleFilters,
      listView,
      renderRiskMatrix,
    } = this.state;

    const statusColor = theme.palette;
    const isArchived = riskFilters["Archived"];
    const status = [
      { name: "Identified", color: statusColor.riskStatus.Identified },
      { name: "In Review", color: statusColor.riskStatus.InReview },
      { name: "Agreed", color: statusColor.riskStatus.Agreed },
      { name: "Rejected", color: statusColor.riskStatus.Rejected },
    ];
    const other = [{ name: "0-25" }, { name: "26-50" }, { name: "51-75" }, { name: "76-100" }];
    const activeQuickFilterKeys = Object.keys(riskFilters).length
      ? Object.keys(riskFilters)
        .toString()
        .split(",")
        .join(", ")
      : "All";

    return !teamCanView("riskAccess") ? (
      <div className={classes.unplannedMain}>
        <div className={classes.unplannedCnt}>
          <UnPlanned
            feature="business"
            titleTxt={
              <FormattedMessage
                id="common.discovered-dialog.business-title"
                defaultMessage="Wow! You've discovered a business feature!"
              />
            }
            boldText={this.props.intl.formatMessage({ id: "risk.label", defaultMessage: "Risks" })}
            descriptionTxt={
              <FormattedMessage
                id="common.discovered-dialog.list.risk.label"
                defaultMessage={"is available on Business Plan. Upgrade now or start a {TRIALPERIOD}-day free trial to find out all about nTask business features."}
                values={{ TRIALPERIOD: TRIALPERIOD }}
              />
            }
            showBodyImg={true}
            showDescription={true}
            imgUrl={Risks}
          />
        </div>
      </div>
    ) : (
      <div className={classes.root}>
        <main
          className={
            teamCanView("advanceFilterAccess")
              ? classNames(classes.content, {
                [classes.contentShift]: openFilterSidebar,
              })
              : classes.noFiltercontent
          }>
          <div className={classes.drawerHeader}>
            <div className={classes.taskDashboardCnt}>
              <Grid
                container
                direction="row"
                justify="space-between"
                alignItems="center"
                classes={{ container: classes.taskDashboardHeader }}>
                <Grid item className="flex_center_start_row">
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      marginRight: "16px",
                    }}>
                    <Typography variant="h1" className={classes.listViewHeading}>
                      Risks
                    </Typography>
                    <span className={classes.count}>
                      {this.props.risksState.filter(t => doesFilterPass({ data: t })).length}
                    </span>
                  </div>
                  {!isArchived ? (
                    <div className={classes.toggleContainer}>
                      <ToggleButtonGroup
                        value={alignment}
                        exclusive
                        onChange={this.handleAlignment}
                        classes={{ root: classes.toggleBtnGroup, groupedHorizontal: classes.groupedHorizontal }}>
                        <ToggleButton
                          value="left"
                          onClick={this.renderListView}
                          classes={{
                            root: classes.toggleButton,
                            selected: classes.toggleButtonSelected,
                          }}>
                          <FormattedMessage id="common.list.label" defaultMessage="List" />
                        </ToggleButton>

                        <ToggleButton
                          value="right"
                          onClick={this.renderRiskMatrix}
                          style={{
                            borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
                          }}
                          classes={{
                            root: classes.toggleButton,
                            selected: classes.toggleButtonSelected,
                          }}>
                          <FormattedMessage
                            id="common.risk-matrix.label"
                            defaultMessage="Risk Matrix"
                          />
                        </ToggleButton>
                      </ToggleButtonGroup>
                    </div>
                  ) : null}
                </Grid>

                {renderRiskMatrix ? null : (
                  <Grid item className="flex_center_start_row">
                    <DefaultTextField
                      fullWidth={false}
                      error={false}
                      formControlStyles={{ width: 250, marginBottom: 0, marginRight: 10 }}
                      defaultProps={{
                        id: "riskListSearch",
                        onChange: this.handleSearch,
                        placeholder: "Search Risks",
                        autoFocus: true,
                        inputProps: { maxLength: 150, style: { padding: "9px 14px" } },
                      }}
                    />
                    <CustomButton
                      onClick={this.handleRefreshList}
                      // onClick={event => {
                      //   this.setState({ refreshBtnQuery: "progress", isLoading: true },
                      //     () => {
                      //       this.props.getRisks(
                      //         null,
                      //         //success
                      //         () => {
                      //           this.setState({ refreshBtnQuery: "", isLoading: false });
                      //           this.showSnackbar("Grid Refreshed Successfully!", "success");
                      //         },
                      //         //failure
                      //         () => {
                      //           this.setState({ refreshBtnQuery: "", isLoading: false });
                      //           this.showSnackbar("Oops! Server throws Error.", "error");
                      //         }
                      //       );
                      //     }
                      //   );
                      // }}
                      query={this.state.refreshBtnQuery}
                      style={{
                        padding: "4px 8px 4px 8px",
                        borderRadius: "4px",
                        display: "flex",
                        justifyContent: "space-between",
                        minWidth: "auto",
                        whiteSpace: "nowrap",
                        marginRight: 10,
                        height: 32,
                      }}
                      btnType={"white"}
                      variant="contained">
                      <SvgIcon viewBox="0 0 12 12.015" className={classes.qckFfilterIconRefresh}>
                        <IconRefresh />
                      </SvgIcon>
                    </CustomButton>
                    <FormControl className={classes.formControl}>
                      <CustomButton
                        onClick={event => {
                          this.handleClick(event, "bottom-end");
                        }}
                        buttonRef={node => {
                          this.anchorEl = node;
                        }}
                        style={{
                          padding: "3px 8px 3px 4px",
                          borderRadius: "4px",
                          display: "flex",
                          justifyContent: "space-between",
                          minWidth: "auto",
                          whiteSpace: "nowrap",
                        }}
                        btnType={!isEmpty(riskFilters) ? "lightBlue" : "white"}
                        variant="contained">
                        <SvgIcon
                          classes={{ root: classes.quickFilterIconSize }}
                          viewBox="0 0 24 24"
                          className={
                            true ? classes.qckFfilterIconSelected : classes.qckFilterIconSvg
                          }>
                          <QuickFilterIcon />
                        </SvgIcon>
                        <span
                          className={
                            !isEmpty(riskFilters)
                              ? classes.qckFilterLblSelected
                              : classes.qckFilterLbl
                          }>
                          <FormattedMessage id="common.show.label" defaultMessage="Show" /> :{" "}
                        </span>
                        <span className={classes.checkname}>{activeQuickFilterKeys}</span>
                      </CustomButton>
                      <PlainMenu
                        open={this.state.open}
                        closeAction={this.handleClose}
                        placement="bottom-start"
                        anchorRef={this.anchorEl}
                        style={{ width: 280 }}
                        offset="0 15px">
                        <MenuList disablePadding>
                          <MenuItem
                            className={`${classes.statusMenuItemCnt} ${isEmpty(riskFilters) ? classes.selectedValue : ""
                              }`}
                            classes={{
                              selected: classes.statusMenuItemSelected,
                              root: classes.customRootMenuItem
                            }}
                            value="Show All"
                            onClick={this.handleShowAllRisks}>
                            <ListItemText
                              primary={
                                <FormattedMessage id="common.show.all" defaultMessage="Show All" />
                              }
                              classes={{ primary: classes.plainItemText }}
                            />
                          </MenuItem>
                          <Divider />
                          {!riskFilters["Archived"] && (
                            <>
                              <CanAccessFeature group='risk' feature='status'>
                                <MenuItem
                                  disableRipple={true}
                                  classes={{
                                    root: classes.menuHeadingItem,
                                  }}>
                                  <FormattedMessage
                                    id="task.creation-dialog.form.status.label"
                                    defaultMessage="Status"
                                  />
                                </MenuItem>
                                {status.map(status => (
                                  <MenuItem
                                    key={status.name}
                                    value={status.name}
                                    className={`${classes.statusMenuItemCnt}  
                                    
                                    
                                    ${!isEmpty(riskFilters) &&
                                        riskFilters?.status?.selectedValues.includes(status.name)
                                        ? classes.selectedValue
                                        : ""
                                      }`}
                                    classes={{
                                      root: classes.customRootMenuItem,
                                      selected: classes.statusMenuItemSelected,
                                    }}
                                    onClick={() => this.handleSelectClick(status.name, "status")}>
                                    <RoundIcon
                                      htmlColor={status.color}
                                      classes={{ root: classes.statusIcon }}
                                    />
                                    <ListItemText
                                      primary={this.translate(status.name)}
                                      classes={{ primary: classes.statusItemText }}
                                    />
                                  </MenuItem>
                                ))}
                                <Divider />
                              </CanAccessFeature>

                              <CanAccessFeature group='risk' feature='likelihood'>
                                <MenuItem classes={{ root: classes.menuHeadingItem }}>
                                  <FormattedMessage
                                    id="risk.common.likelihood.label"
                                    defaultMessage="Likelihood"
                                  />
                                </MenuItem>
                                {other.map(item => (
                                  <MenuItem
                                    key={item.name}
                                    value={item.name}
                                    className={`${classes.statusMenuItemCnt} 
                                  ${!isEmpty(riskFilters) &&
                                        riskFilters?.likelihood?.selectedValues.includes(item.name)
                                        ? classes.selectedValue
                                        : ""
                                      }`}
                                    classes={{
                                      root: classes.customRootMenuItem,
                                      selected: classes.statusMenuItemSelected,
                                    }}
                                    onClick={() => this.handleSelectClick(item.name, "likelihood")}>
                                    <ListItemText
                                      primary={item.name + " %"}
                                      classes={{ primary: classes.statusItemText }}
                                    />
                                  </MenuItem>
                                ))}
                              </CanAccessFeature>
                            </>
                          )}
                          <MenuItem
                            value={"Archived"}
                            classes={{ root: classes.highlightItem }}
                            onClick={() => this.handleSelectClick("Archived")}>
                            <ArchivedIcon
                              htmlColor={theme.palette.secondary.light}
                              classes={{
                                root: classes.selectHighlightItemIcon,
                              }}
                            />
                            <FormattedMessage
                              id="common.archived.archived"
                              defaultMessage="Archived"
                            />
                          </MenuItem>
                        </MenuList>
                      </PlainMenu>
                    </FormControl>
                    {!riskFilters["Archived"] && (
                      <ImportExportDD
                        handleExportType={this.showSnackbar}
                        filterList={riskFilters}
                        multipleFilters={multipleFilters}
                        handleRefresh={this.handleRefresh}
                        ImportExportType={"risk"}
                        data={this.props.risksState}
                      />
                    )}
                    <ColumnSelectionDropdown
                      feature={"risk"}
                      onColumnHide={this.onColumnHide}
                      hideColumns={[""]}
                      btnProps={{
                        style: {
                          border: "1px solid #dddddd",
                          padding: "5px 10px",
                          borderRadius: 4,
                          marginLeft: 10,
                        },
                      }}
                    />
                  </Grid>
                )}
              </Grid>
              <Grid container classes={{ container: classes.dashboardContentCnt }}>
                {listView ? (
                  <Grid item classes={{ item: classes.taskListCnt }}>
                    {this.state.isLoading ? <ListLoader style={{ paddingLeft: 32 }} /> : <RiskList />}

                  </Grid>
                ) : null}
              </Grid>
            </div>
          </div>
          {renderRiskMatrix ? (
            <div style={{ height: calculateContentHeight(), overflow: "auto" }}>
              <RiskMatrix applyFilter={this.applyRiskMatrixFilter} />
            </div>
          ) : null}
        </main>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    risksState: state.risks.data,
    riskFilters: state.risks.quickFilters || {},
    dialogsState: state.dialogStates,
  };
};

export default compose(
  withRouter,
  withSnackbar,
  injectIntl,
  withStyles(combineStyles(dashboardStyles, selectStyles), {
    withTheme: true,
  }),
  connect(mapStateToProps, {
    FetchWorkspaceInfo,
    setAppliedFilters,
    saveSortingProjectList,
    updateArchivedItems,
    getRisks,
    getProjects,
    getTasks,
    updateQuickFilter,
    showAllRisks,
    removeRiskQuickFilter,
    getArchivedData,
    riskDetailDialog,
    updateRiskFilter
  })
)(RiskDashboard);
