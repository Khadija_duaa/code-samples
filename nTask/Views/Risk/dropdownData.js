import { columns } from "./constants";

export const sortingDropdownData = [
  { key: "", name: "None" },
  { key: "riskId", name: "ID", },
  { key: "title", name: "Title" },
  { key: "status", name: "Status" },
  { key: "impact", name: "Impact" },
  { key: "likelihood", name: "Likelihood" },
  { key: "updatedDate", name: "Last Updated" },
  { key: "createdDate", name: "Creation Date" },
]
export const groupingDropdownData = [columns.riskNone, columns.riskStatus, columns.riskImpact, columns.riskLikelihood]