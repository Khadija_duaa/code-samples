import React, { useEffect, useRef } from "react";
import CustomIconButton from "../../../components/Buttons/CustomIconButton";
import SvgIcon from "@material-ui/core/SvgIcon";
import RecurrenceIcon from "../../../components/Icons/RecurrenceIcon";



function RecurrenceBtn({onClick, theme}) {
  const recBtnRef = useRef(null);
  useEffect(() => {
    recBtnRef.current && recBtnRef.current.addEventListener('click', onClick, false);
    return () => {
      recBtnRef.current && recBtnRef.current.removeEventListener('click', onClick, true)
    }
  }, [])
 return( <CustomIconButton
    btnType="transparent"
    buttonRef={node => {
      recBtnRef.current = node
    }}
    disableRipple={true}
    style={{ padding: 5, marginRight: 2 }}>
    <SvgIcon
      viewBox="0 0 14 12.438"
      htmlColor={theme.palette.primary.light}
      style={{fontSize: "14px"}}>
      <RecurrenceIcon />
    </SvgIcon>
  </CustomIconButton>)
}

export default RecurrenceBtn;