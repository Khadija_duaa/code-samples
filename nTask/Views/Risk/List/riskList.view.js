import { AgGridColumn } from "@ag-grid-community/react";
import withStyles from "@material-ui/core/styles/withStyles";
import fileDownload from "js-file-download";
import isArray from "lodash/isArray";
import isEmpty from "lodash/isEmpty";
import isEqual from "lodash/isEqual";
import isUndefined from "lodash/isUndefined";
import moment from "moment";
import { withSnackbar } from "notistack";
import React, { useCallback, useEffect, useRef, useState } from "react";
import { FormattedMessage, injectIntl } from "react-intl";
import { useDispatch, useSelector } from "react-redux";
import { compose } from "redux";
import Riskcmp from "../../../components/BulkActions/Risk.cmp.js";
import CustomFieldRenderer from "../../../components/CustomTable2/CustomFieldsColumn/CustomFieldRenderer";
import { grid } from "../../../components/CustomTable2/gridInstance";
import GroupByComponents from "../../../components/CustomTable2/GroupByComponents/GroupByComponents";
import loadable from '@loadable/component'
const CustomTable = loadable(() => import("../../../components/CustomTable2/listViewTable.cmp"));
import AssigneeDropdown from "../../../components/Dropdown/AssigneeDropdown";
import SearchDropdown from "../../../components/Dropdown/SearchDropdown";
import StatusDropdown from "../../../components/Dropdown/StatusDropdown/Dropdown";
import EmptyState from "../../../components/EmptyStates/EmptyState";
import MenuList from "../../../components/Menu/TaskMenus/MemberListMenu";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";
import Stared from "../../../components/Starred/Starred";
import useUpdateRisk from "../../../helper/customHooks/Risks/updateRisk";
import { generateTaskData } from "../../../helper/generateSelectData";
import { severity as severityData } from "../../../helper/issueDropdownData";
import { statusData } from "../../../helper/riskDropdownData";
import { riskDetailDialog } from "../../../redux/actions/allDialogs";
import {
  createRisk,
  dispatchRisk,
  exportBulkRisk,
  getSavedFilters,
  updateRisk,
} from "../../../redux/actions/risks";
import ColumnSelector from "../../../redux/selectors/columnSelector";
import ProjectPermissionSelector from "../../../redux/selectors/projectPermissionSelector";
import { emptyRisk } from "../../../utils/constants/emptyRisk";
import RiskActionDropdown from "../RiskActionDropdown/RiskActionDropdown";
import { headerProps } from "./constants";
import { riskColumnDefs, sortAlphabetically } from "./riskColumns";
import { doesFilterPass } from "./RiskFilter/riskFilter.utils";
import RiskFilter from "./RiskFilter/riskFilter.view";
import riskListStyles from "./riskList.style";
import { addPendingHandler } from "../../../redux/actions/backProcesses.js";

// const defaultColDef = { minWidth: 200, headerClass: "customHeader" };
const RiskList = React.memo(({ classes, theme, intl, enqueueSnackbar, style }) => {
  const { editRisk } = useUpdateRisk();
  const state = useSelector((state) => {
    return {
      riskColumns: ColumnSelector(state).risk.columns,
      sections: ColumnSelector(state).risk.sections,
      nonSectionFields: ColumnSelector(state).risk.nonSectionFields,
      risks: state.risks.data,
      tasks: state.tasks.data,
      projects: state.projects.data,
      members: state.profile.data.member.allMembers,
      permissionObject: ProjectPermissionSelector(state),
      workspaceRiskPer: state.workspacePermissions.data.risk,
      quickFilters: state.risks.quickFilters,
      riskFilters: state.risks.riskFilter,
    };
  });
  const sectionGroup = localStorage.getItem("sectiongrouping");
  const [sectionGrouping, setSectionGrouping] = useState(sectionGroup === "true" ? true : false);
  const dispatch = useDispatch();
  const [selectedRisks, setSelectedRisks] = useState([]);
  const {
    riskColumns = [],
    risks = [],
    projects,
    members,
    sections = [],
    nonSectionFields = [],
    workspaceRiskPer,
    permissionObject,
    riskFilters,
    tasks,
    quickFilters,
  } = state;
  const taskListRef = useRef([]);
  const projectListRef = useRef([]);

  useEffect(() => {
    taskListRef.current = tasks;
  }, [tasks]);

  useEffect(() => {
    projectListRef.current = projects;
  }, [projects]);

  useEffect(() => {
    getSavedFilters("risk", dispatch);
    return () => {
      grid.grid = null;
    };
  }, []);

  useEffect(() => {
    if (quickFilters && quickFilters.Archived) {
      grid.grid && grid.grid.deselectAll();
    }
  }, [quickFilters]);

  useEffect(() => {
    grid.grid && grid.grid.onFilterChanged();
  }, [riskFilters, quickFilters]);

  const updateDataRisk = (risk, type, value) => {
    editRisk(
      risk,
      type,
      value,
      (risk) => {
      },
      (errMessage) => {
        showSnackBar(errMessage, "error");
      }
    );
  };
  const handleRiskStared = (stared, obj) => {
    updateDataRisk(obj, "isStared", stared);
  };
  const handleStatusChange = (status, obj) => {
    updateDataRisk(obj, "status", status.value);
  };
  //Snackbar function
  const showSnackBar = (snackBarMessage = "", type = null) => {
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "top",
          horizontal: "centerdp",
        },
        variant: type ? type : "info",
      }
    );
  };

  // Handle Project change
  const handleProjectChange = (newValue, obj) => {
    const projectId = newValue.length ? newValue[0].id : "";
    updateDataRisk(obj, "projectId", projectId);
  };
  // export risk api function
  const handleExportRisk = (props) => {
    const selectedNodes = grid.grid.getSelectedNodes();
    const selectedNodesLength = selectedNodes?.length || 1;
    const postObj = selectedNodes.length
      ? { riskIds: selectedNodes.map((i) => i.id) }
      : { riskIds: [props.node?.data?.id || ""] };

    const obj = {
      type: 'risks',
      apiType: 'post',
      data: postObj,
      fileName: 'risks.xlsx',
      apiEndpoint: 'api/risks/exportbulk',
    }
    addPendingHandler(obj, dispatch);

    // exportBulkRisk(postObj, dispatch, (res) => {
    //   fileDownload(res.data, "risks.xlsx");
    //   showSnackBar(`${selectedNodesLength} risks exported successfully`, "success");
    // },
    // (err) => {
    //   if (err.response.status == 405) {
    //     showSnackBar("You don't have sufficient rights to export risk", "error");
    //   } else {
    //     showSnackBar("oops! There seems to be an issue, please contact support@ntaskmanager.com", "error");
    //   }
    // });
  };
  // export all risks api function
  const handleExportAllRisks = (props) => {
    let allNodes = [];
    grid.grid.forEachNodeAfterFilter((node) => allNodes.push(node.data));
    const postObj = allNodes.length && { riskIds: allNodes.map((i) => i.id) };

    const obj = {
      type: 'risks',
      apiType: 'post',
      data: postObj,
      fileName: 'risks.xlsx',
      apiEndpoint: 'api/risks/exportbulk',
    }
    addPendingHandler(obj, dispatch);

    // exportBulkRisk(postObj, dispatch, (res) => {
    //   fileDownload(res.data, "risks.xlsx");
    //   showSnackBar(`${allNodes.length} risks exported successfully`, "success");
    // },
    // (err) => {
    //   if (err.response.status == 405) {
    //     showSnackBar("You don't have sufficient rights to export risks", "error");
    //   } else {
    //     showSnackBar("oops! There seems to be an issue, please contact support@ntaskmanager.com", "error");
    //   }
    // });
  };
  const getContextMenuItems = useCallback((params) => {
    const selectedNodes = grid.grid.getSelectedNodes();
    let result = [
      {
        // custom item
        name: selectedNodes.length ? "Export Selected Risk(s)" : "Export Risk",
        action: () => handleExportRisk(params),
      },
      {
        // custom item
        name: "Export all Risks",
        action: () => handleExportAllRisks(params),
      },
    ];
    return result;
  }, []);
  //Handle Add risk
  const handleAddRisk = (data, callback) => {
    //Post Obj is object to be posted to backend for task creation
    const postObj = { title: data.value, clientId: data.clientId };

    //Dispatch Obj is object that is dispatched before the api call for quick entry
    const dispatchObj = {
      ...emptyRisk,
      title: data.value,
      clientId: data.clientId,
      id: data.clientId,
      riskId: data.clientId,
      isNew: true,
    };
    //Calling grid add method to add risk
    createRisk(postObj, dispatch, dispatchObj, (res) => { });
  };

  // Generate list of all projects for dropdown understandable form
  const generateProjectDropdownData = (row) => {
    let filteredProjects = projectListRef.current.filter((p) => p.projectId !== row.projectId);
    const projectsArr = filteredProjects.map((project) => {
      return { label: project.projectName, id: project.projectId, obj: row };
    });
    return projectsArr;
  };
  const StatusDropdownCmp = (row = { data: {} }) => {
    const rowData = row.data ? row.data : { data: {} };
    let riskPermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceRiskPer
          : permissionObject[rowData.projectId].risk
        : workspaceRiskPer;
    const riskStatusData = statusData(theme, classes, intl);
    let selectedStatus = rowData
      ? riskStatusData.find((item) => item.value == rowData.status) || riskStatusData[0]
      : riskStatusData[0];
    return (
      <StatusDropdown
        onSelect={(status) => {
          handleStatusChange(status, rowData);
        }}
        buttonType={"listButton"}
        option={selectedStatus}
        options={riskStatusData}
        toolTipTxt={selectedStatus.label}
        writeFirst={true}
        disabled={rowData.isArchive || !riskPermission.riskDetails.editRiskStatus.isAllowEdit}
        preSelection={false}
        dropdownProps={{
          disablePortal: false,
        }}
      />
    );
  };
  const isUpdatedImpact = (data, riskData) => {
    updateDataRisk(riskData, "impact", data.value);
  };
  const isUpdatedLikelihood = (data, callback) => {
    updateDataRisk(data, "likelihood", data.likelihood);
  };
  const ImpactDropdownCmp = (row = { data: {} }) => {
    const rowData = row.data ? row.data : {};
    let riskPermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceRiskPer
          : permissionObject[rowData.projectId].risk
        : workspaceRiskPer;
    const riskSeverity = severityData(theme, classes, intl);
    let selectedImpact = rowData
      ? riskSeverity.find((item) => item.value == rowData.impact) || riskSeverity[0]
      : riskSeverity[0];
    return (
      <StatusDropdown
        onSelect={(impact) => { isUpdatedImpact(impact, rowData); }}
        buttonType={"listButton"}
        option={selectedImpact}
        options={riskSeverity}
        toolTipTxt={
          <FormattedMessage id="risk.common.impact.label" defaultMessage="Select Impact" />
        }
        writeFirst={true}
        disabled={rowData.isArchive || !riskPermission.riskDetails.editRiskImpact.isAllowEdit}
        dropdownProps={{
          disablePortal: false,
        }}
        btnProps={{
          customClasses: {
            label: classes.priorityLabel,
          },
        }}

      />
    );
  };
  const LikelihoodDropdownCmp = (row = { data: {} }) => {
    const rowData = row.data ? row.data : {};
    let riskPermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceRiskPer
          : permissionObject[rowData.projectId].risk
        : workspaceRiskPer;

    return (
      <span data-rowClick={riskPermission.riskDetails.editlikelihood.isAllowEdit ? "cell" : ''}>
        <MenuList
          id={rowData.id || ""}
          likelihood={rowData.likelihood}
          MenuData={rowData}
          placementType="bottom-end"
          isUpdated={isUpdatedLikelihood}
          viewType="risk"
          isLikelihood={true}
          showRiskDetails={false}
          permission={riskPermission.riskDetails.editlikelihood.isAllowEdit}
          permissionKey="likelihood"
          riskPermission={riskPermission}
        />
      </span>
    );
  };
  const titleCellRenderer = (row = { data: {} }) => {
    const rowData = row.data ? row.data : { data: {} };
    return (
      <div className={classes.taskTitleCnt}>
        <div className={`${classes.taskTitleTextCnt} wrapText`} title={rowData.title}>
          <span className={classes.taskTitle}>{rowData.title} </span>
        </div>
        {rowData.isNew && <div style={{ minWidth: 54 }}>
          <span className={classes.newTag}>New</span>
        </div>}
        <Stared
          isStared={rowData.isStared}
          handleCallback={(isStared) => handleRiskStared(isStared, rowData)}
          diabled={rowData.isDeleted}
        />
      </div>
    );
  };
  const ProjectDropdownRenderer = (row) => {
    const rowData = row.data || {};
    const selectedProject = rowData.projectId
      ? [
        {
          label: rowData.project?.projectName || "",
          id: rowData.projectId,
          obj: rowData,
        },
      ]
      : [];
    let riskPermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceRiskPer
          : permissionObject[rowData.projectId].risk
        : workspaceRiskPer;
    const isArcheive = rowData.isArchive;

    return (
      <SearchDropdown
        initSelectedOption={selectedProject}
        obj={rowData}
        disabled={isArcheive || !riskPermission.riskDetails.riskProject.isAllowEdit}
        tooltip={false}
        key={rowData.projectId}
        popperProps={{ disablePortal: false }}
        tooltipText={""}
        optionsList={generateProjectDropdownData(rowData)}
        singleSelect
        updateAction={handleProjectChange}
        selectedOptionHead={"Risk Project"}
        allOptionsHead={intl.formatMessage({
          id: "project.label",
          defaultMessage: "Projects",
        })}
        buttonPlaceholder={
          <FormattedMessage
            id="task.creation-dialog.form.project.placeholder"
            defaultMessage="Select Project"
          />
        }
        buttonProps={{
          style: {
            width: "100%",
          },
        }}
        btnTextProps={{
          style: {
            width: "90%",
            overflow: "hidden",
            whiteSpace: "nowrap",
            textOverflow: "ellipsis",
          },
        }}
      />
    );
  };
  //Generate list of selected options for project dropdown understandable form
  const getSelectedTask = (risk) => {
    let selectedTask = taskListRef.current.filter((task) => {
      return risk.linkedTasks.indexOf(task.taskId) > -1;
    });
    const selectedTaskArr = selectedTask.map((selected) => {
      return {
        label: selected.taskTitle,
        id: selected.taskId,
        obj: risk,
      };
    });
    return selectedTaskArr
  };
  // Generate list of all projects for dropdown understandable form
  const generateTaskDropdownData = (risk) => {
    let tasksData = risk.projectId
      ? taskListRef.current.filter((t) => t.projectId === risk.projectId)
      : taskListRef.current;

    let tasksArr = generateTaskData(tasksData);
    return tasksArr;
  };
  const handleRiskTaskChange = (newValue, obj) => {
    if (newValue.length) updateDataRisk(obj, "linkedTasks", [newValue[0].id]);
    /** for selecting task */ else updateDataRisk(obj, "linkedTasks", []); /** for removing task */
  };
  const TaskDropdownRenderer = (row) => {
    const rowData = row.data || {};
    let riskPermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceRiskPer
          : permissionObject[rowData.projectId].risk
        : workspaceRiskPer;
    const isArchive = rowData.isArchive;

    return (
      <SearchDropdown
        initSelectedOption={getSelectedTask(rowData)}
        obj={rowData}
        disabled={isArchive || !riskPermission.riskDetails.riskTask.isAllowEdit}
        tooltip={false}
        popperProps={{ disablePortal: false }}
        tooltipText={""}
        optionsList={generateTaskDropdownData(rowData)}
        singleSelect
        updateAction={(option, value) => handleRiskTaskChange(option, rowData)}
        selectedOptionHead={"Risk Task"}
        allOptionsHead={intl.formatMessage({
          id: "task.label",
          defaultMessage: "Tasks",
        })}
        buttonPlaceholder={
          <FormattedMessage id="common.bulk-action.selectTask" defaultMessage="Select Task" />
        }
        multipleOptionsPlaceHolder="Multiple Tasks"
        buttonProps={{
          style: {
            width: "100%",
          },
        }}
        btnTextProps={{
          style: {
            width: "90%",
            overflow: "hidden",
            whiteSpace: "nowrap",
            textOverflow: "ellipsis",
          },
        }}
      />
    );
  };
  const updateRiskOwner = (owner, obj) => {
    const ownerId = owner.length ? owner[0].userId : "none";
    updateDataRisk(obj, "riskOwner", ownerId);
  };
  const RiskOwnerRenderer = (row) => {
    const rowData = row.data || {};
    const membersObjArr = members && members.filter((m) => rowData.riskOwner?.includes(m.userId));
    let riskPermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceRiskPer
          : permissionObject[rowData.projectId].risk
        : workspaceRiskPer;
    return (
      <span data-rowClick="cell">
        <AssigneeDropdown
          popperProps={{ disablePortal: false }}
          assignedTo={membersObjArr || []}
          updateAction={updateRiskOwner}
          isArchivedSelected={rowData.isArchive}
          obj={rowData}
          avatarSize={"xsmall"}
          buttonVariant={"small"}
          customIconButtonProps={{
            classes: {
              root: classes.iconBtnStyles,
            },
          }}
          totalAssigneeBtnProps={{
            style: {
              height: 29,
              width: 29,
              fontSize: "14px",
              fontWeight: theme.typography.fontWeightLarge,
            },
          }}
          customIconRenderer={null}
          style={{ height: "100%" }}
          singleSelect={true}
          disabled={false}
          addPermission={riskPermission.riskDetails.riskOwner.cando}
          deletePermission={riskPermission.riskDetails.riskOwner.cando}
        />
      </span>
    );
  };
  const DateRenderer = (row) => {
    const rowData = row.data || {};
    const formatedDate = moment(rowData.createdDate).format("MMM DD, YY");
    return formatedDate;
  };
  const UpdatedDateRenderer = row => {
    const rowData = row.data || {};
    const formatedDate = moment(rowData.updatedDate).format("MMM DD, YY");
    return formatedDate;
  };
  const CreatedByRenderer = (row = { data: {} }) => {
    const rowData = row.data ? row.data : { data: {} };
    return (
      <div className={`${classes.taskTitleTextCnt} ${classes.textCenter} wrapText`} title={rowData.createdBy}>
        <span className={classes.taskTitle}>
          {!isEmpty(rowData.createdBy) ? rowData.createdBy : "-"}{" "}
        </span>
      </div>
    );
  };
  const UpdatedByRenderer = (row = { data: {} }) => {
    const rowData = row.data ? row.data : { data: {} };
    return (
      <div className={`${classes.taskTitleTextCnt} ${classes.textCenter} wrapText`} title={rowData.updatedBy}>
        <span className={classes.taskTitle}>
          {!isEmpty(rowData.updatedBy) ? rowData.updatedBy : "-"}{" "}
        </span>
      </div>
    );
  };
  const ColorRenderer = (row) => {
    const rowData = row.data || {};

    return (
      <span
        style={{ background: rowData.colorCode || "transparent" }}
        className={classes.taskColor}></span>
    );
  };
  const isExternalFilterPresent = () => {
    return true;
  };
  useEffect(() => {
    if (quickFilters && quickFilters.Archived) {
      grid.grid && grid.grid.deselectAll();
    }
  }, [quickFilters]);

  const handleRiskSelection = (risks) => {
    setSelectedRisks(risks);
  };

  const handleClearSelection = () => {
    setSelectedRisks([]);
    grid.grid && grid.grid.deselectAll();
  };
  //Close task detail
  const closeRiskDetailsPopUp = () => {
    riskDetailDialog(
      {
        id: "",
        isArchived: false,
        afterCloseCallBack: () => { },
      },
      dispatch
    );
  };
  //Open task details on risks row click
  const handleRiskRowClick = (row) => {
    let rowData = row.data;
    const isRowClicked = row.event.target.closest("*[data-rowClick='cell']");
    let riskPermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceRiskPer
          : permissionObject[rowData.projectId].risk
        : workspaceRiskPer;
    if (isRowClicked) return;
    if (riskPermission.riskDetails.cando && row.data && row.data.id && row.data.uniqueId !== "-") {
      riskDetailDialog(
        {
          id: row.data.id,
          isArchived: row.data.isArchive,
          afterCloseCallBack: () => {
            closeRiskDetailsPopUp();
          },
        },
        dispatch
      );
    }
  };
  const handleUpdateCustomField = (
    rowData,
    option,
    obj,
    settings,
    succ = () => { },
    fail = () => { }
  ) => {
    const riskCopy = { ...rowData };

    if (obj.fieldType == "matrix") {
      const selectedField =
        riskCopy.customFieldData && riskCopy.customFieldData.find((x) => x.fieldId == obj.fieldId);
      const isExist =
        selectedField &&
        selectedField.fieldData.data.findIndex((f) => f.cellName == option.cellName) > -1;
      if (isExist === true) return;
    }
    const { fieldId } = obj;
    const newObj = {
      groupType: "risk",
      groupId: riskCopy.id,
      fieldType: obj.fieldType,
      fieldId,
      fieldData: { data: option },
    };
    if (obj.fieldType === "dropdown") {
      if (isArray(newObj.fieldData.data))
        newObj.fieldData.data = newObj.fieldData.data.map((opt) => {
          return { id: opt.id };
        });
      else newObj.fieldData.data = { id: newObj.fieldData.data.id };
    }
    if (obj.fieldType === "matrix")
      newObj.fieldData.data = newObj.fieldData.data.map((opt) => {
        return { cellName: opt.cellName, createdDate: opt.createdDate };
      });
    updateRisk(
      newObj,
      (res) => {
        succ();
        const resObj = res.data.entity[0];
        let customFieldsArr = [];
        // Updating Global state
        const isExist = riskCopy.customFieldData
          ? riskCopy.customFieldData.findIndex((c) => c.fieldId === resObj.fieldId) > -1
          : false; /** if new risk created, its customFields are null so in null case scenario , compile the normal false flow, add custom field in risk object and save it */
        if (isExist) {
          customFieldsArr = riskCopy.customFieldData.map((c) => {
            return c.fieldId === resObj.fieldId ? resObj : c;
          });
        } else {
          customFieldsArr = riskCopy.customFieldData
            ? [...riskCopy.customFieldData, resObj]
            : [
              resObj,
            ]; /** add custom field in risk object and save it, if newly created risk because its custom field is already null */
        }
        let newRiskObj = { ...riskCopy, customFieldData: customFieldsArr };
        if (grid.grid) {
          const rowNode = grid.grid && grid.grid.getRowNode(newRiskObj.id);
          // grid.grid.redrawRows(rowNode);
          rowNode?.setData(newRiskObj);
        }
        dispatchRisk(newRiskObj, dispatch);
      },
      (err) => {
        if (err && err.data) showSnackBar(err.data.message, "error");
        fail();
      },
      dispatch
    );
  };
  const CustomfieldRendere = (row) => {
    /** custom fields columns rendere */
    const obj = {
      fieldId: row.colDef.fieldId,
      fieldType: row.colDef.fieldType,
      fieldData: { data: null },
    };
    const rowData = row.data || {};
    const data = rowData.customFieldData
      ? rowData.customFieldData.find((cf) => cf.fieldId === row.colDef.fieldId) || obj
      : obj;
    let riskPermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceRiskPer
          : permissionObject[rowData.projectId].risk
        : workspaceRiskPer;
    const canUpdateField =
      riskPermission && !rowData.isArchive ? riskPermission.riskDetails.isUpdateField.cando : false;
    return (
      <CustomFieldRenderer
        field={data}
        fieldType={row.colDef.fieldType}
        rowData={rowData}
        handleUpdateCustomField={handleUpdateCustomField}
        permission={canUpdateField}
        groupType={"risk"}
        placeholder={false}
      />
    );
  };

  const RiskActionDropdownRenderer = (row) => {
    const rowData = row.data || {};
    let riskPermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceRiskPer
          : permissionObject[rowData.projectId].risk
        : workspaceRiskPer;
    return (
      <RiskActionDropdown
        btnProps={{ style: { width: 37 } }}
        data={rowData}
        handleCloseCallBack={() => { }}
        riskPermission={riskPermission}
        isArchived={rowData.isArchive}
      />
    );
  };
  const getFieldsNames = (fieldsToFilter = []) => {
    const nonSectionsFieldskeys = nonSectionFields
      .filter(
        (item) =>
          fieldsToFilter.includes(item.fieldType) ||
          (item.fieldType == "dropdown" && item.settings.multiSelect) ||
          (item.fieldType == "people" && item.settings.multiplePeople)
      )
      .map((nsf) => nsf.columnKey);
    const sectionsFieldskeys = sections.reduce((res, cv) => {
      let keys = cv.fields
        .filter(
          (item) =>
            fieldsToFilter.includes(item.fieldType) ||
            (item.fieldType == "dropdown" && item.settings.multiSelect) ||
            (item.fieldType == "people" && item.settings.multiplePeople)
        )
        .map((cv) => cv.columnKey);
      res = [...res, ...keys];
      return res;
    }, []);
    return [...nonSectionsFieldskeys, ...sectionsFieldskeys];
  };

  const riskTitleColumn = (riskColumns && riskColumns.find((c) => c.columnKey == "title")) || {};

  let headProps = headerProps;

  const columnsNamesDisableGrouping = getFieldsNames([
    "location",
    "filesAndMedia",
    "formula",
    "textarea",
  ]);
  headProps.columnGroupingDisabled = [
    ...headProps.columnGroupingDisabled,
    ...columnsNamesDisableGrouping,
  ];

  const handleChangeGrouping = (value) => {
    let g = grid.grid;
    let sectionGroup = localStorage.getItem("sectiongrouping");
    if (sectionGroup == "true") localStorage.setItem("sectiongrouping", value);
    else {
      localStorage.setItem("sectiongrouping", value);
    }
    setSectionGrouping(value);
  };
  const RenderColumnsWithoutSectionGrouping = () => {
    const superSystemColumns = riskColumns
      .map((c, i) => {
        const defaultColDef = riskColumnDefs(classes)[c.columnKey];
        const {
          hide,
          pinned,
          rowGroup,
          rowGroupIndex,
          sort,
          sortIndex,
          width,
          wrapText,
          autoHeight,
          position,
        } = c;
        return (
          c.columnKey !== "title" &&
          headerProps.ntaskFields.includes(c.columnKey) && (
            <AgGridColumn
              key={c.id}
              headerName={c.columnName}
              field={c.columnKey}
              hide={hide}
              suppressKeyboardEvent={true}
              pinned={pinned}
              align="left"
              rowGroup={rowGroup}
              sort={sort}
              sortIndex={sortIndex == -1 ? null : sortIndex}
              wrapText={wrapText}
              rowGroupIndex={rowGroup && parseFloat(rowGroupIndex)}
              autoHeight={autoHeight}
              width={width || (defaultColDef && defaultColDef.minWidth)}
              cellStyle={{ justifyContent: "center" }}
              position={position}
              {...defaultColDef}
            />
          )
        );
      })
      .filter((c) => c);
    const systemColumns = riskColumns
      .map((c, i) => {
        const defaultColDef = riskColumnDefs(classes)[c.fieldType];
        const { hide, pinned, rowGroup, sort, sortIndex, width, wrapText, autoHeight, position, rowGroupIndex } =
          c;
        return (
          c.columnKey !== "title" &&
          !headerProps.ntaskFields.includes(c.columnKey) && (
            <AgGridColumn
              key={c.id}
              headerName={c.columnName}
              field={c.columnKey}
              fieldType={c.fieldType}
              hide={hide}
              pinned={pinned}
              align="left"
              rowGroup={rowGroup}
              rowGroupIndex={rowGroup && parseFloat(rowGroupIndex)}
              suppressKeyboardEvent={true}
              sort={sort}
              sortIndex={sortIndex == -1 ? null : sortIndex}
              wrapText={wrapText}
              autoHeight={autoHeight}
              fieldId={c.fieldId}
              position={position}
              width={width || (defaultColDef && defaultColDef.minWidth)}
              cellStyle={{
                justifyContent:
                  c.fieldType === "dropdown" || c.fieldType === "filesAndMedia"
                    ? "flex-start"
                    : "center",
              }}
              {...defaultColDef}
            />
          )
        );
      })
      .filter((c) => c);
    const nonSectionColumns = nonSectionFields.map((c, i) => {
      const defaultColDef = riskColumnDefs(classes)[c.fieldType];
      const { hide, pinned, rowGroup, sort, sortIndex, width, wrapText, autoHeight, position, rowGroupIndex } = c;
      return (
        <AgGridColumn
          key={c.id}
          headerName={c.columnName}
          field={c.columnKey}
          fieldType={c.fieldType}
          hide={hide}
          pinned={pinned}
          align="left"
          rowGroup={rowGroup}
          rowGroupIndex={rowGroup && parseFloat(rowGroupIndex)}
          suppressKeyboardEvent={true}
          sort={sort}
          sortIndex={sortIndex == -1 ? null : sortIndex}
          wrapText={wrapText}
          autoHeight={autoHeight}
          fieldId={c.fieldId}
          position={position}
          width={width || (defaultColDef && defaultColDef.minWidth)}
          cellStyle={{
            justifyContent:
              c.fieldType === "dropdown" || c.fieldType === "filesAndMedia"
                ? "flex-start"
                : "center",
          }}
          {...defaultColDef}
        />
      );
    });
    const sectionColumns = sections
      .map((section) => {
        return (
          section.fields.length > 0 &&
          section.fields.map((c, i) => {
            const defaultColDef = riskColumnDefs(classes)[c.fieldType];
            const {
              hide,
              pinned,
              rowGroup,
              sort,
              sortIndex,
              width,
              wrapText,
              autoHeight,
              position,
              rowGroupIndex
            } = c;
            return (
              <AgGridColumn
                key={c.id}
                headerName={c.columnName}
                field={c.columnKey}
                hide={hide}
                pinned={pinned}
                align="left"
                suppressKeyboardEvent={true}
                position={position}
                rowGroup={rowGroup}
                fieldType={c.fieldType}
                sort={sort}
                rowGroupIndex={rowGroup && parseFloat(rowGroupIndex)}
                sortIndex={sortIndex == -1 ? null : sortIndex}
                wrapText={wrapText}
                autoHeight={autoHeight}
                width={width || (defaultColDef && defaultColDef.minWidth)}
                fieldId={c.fieldId}
                cellStyle={{
                  justifyContent:
                    c.fieldType === "dropdown" || c.fieldType === "filesAndMedia"
                      ? "flex-start"
                      : "center",
                }}
                {...defaultColDef}
              />
            );
          })
        );
      })
      .filter((c) => c);
    const spreadSectionCols = sectionColumns.reduce((r, cv) => {
      r = [...r, ...cv];
      return r;
    }, []);
    const allColumns = [
      ...superSystemColumns,
      ...systemColumns,
      ...nonSectionColumns,
      ...spreadSectionCols,
    ];
    const sortedColumns = allColumns.sort((pv, cv) => {
      return pv.props.position - cv.props.position;
    });
    return sortedColumns;
  };

  return (
    <>
      {selectedRisks.length >= 1 ? (
        <Riskcmp selectedRisks={selectedRisks} clearSelection={handleClearSelection} />
      ) : null}

      {risks.length == 0 ? (
        <div className={classes.emptyContainer} style={style}>
          <EmptyState
            screenType="risk"
            heading={
              <FormattedMessage
                id="common.create-first.risk.label"
                defaultMessage="Create your first risk"
              />
            }
            message={
              <FormattedMessage
                id="common.create-first.risk.messagea"
                defaultMessage='You do not have any risks yet. Press "Alt + R" or click on button below.'
              />
            }
            button={workspaceRiskPer.createRisk.cando}
          />
        </div>
      ) : (
        <div className={classes.taskListViewCnt} style={style}>
          <CustomTable
            columns={state.riskColumns}
            defaultColDef={{ lockPinned: true }}
            isExternalFilterPresent={isExternalFilterPresent}
            doesExternalFilterPass={doesFilterPass}
            onSelectionChanged={handleRiskSelection}
            idKey={"id"}
            frameworkComponents={{
              riskActionCellRenderer: RiskActionDropdownRenderer,
              statusDropdown: StatusDropdownCmp,
              impactDropdown: ImpactDropdownCmp,
              likelihoodDropdown: LikelihoodDropdownCmp,
              title: titleCellRenderer,
              riskProject: ProjectDropdownRenderer,
              riskTask: TaskDropdownRenderer,
              groupRowInnerRenderer: GroupRowInnerRenderer,
              createdDateRenderer: DateRenderer,
              updatedDateRenderer: UpdatedDateRenderer,
              createdByRenderer: CreatedByRenderer,
              updatedByRenderer: UpdatedByRenderer,
              riskOwner: RiskOwnerRenderer,
              color: ColorRenderer,
              textfield: CustomfieldRendere,
              location: CustomfieldRendere,
              country: CustomfieldRendere,
              number: CustomfieldRendere,
              money: CustomfieldRendere,
              email: CustomfieldRendere,
              websiteurl: CustomfieldRendere,
              date: CustomfieldRendere,
              phone: CustomfieldRendere,
              rating: CustomfieldRendere,
              formula: CustomfieldRendere,
              people: CustomfieldRendere,
              dropdown: CustomfieldRendere,
              filesAndMedia: CustomfieldRendere,
              matrix: CustomfieldRendere,
            }}
            data={risks}
            selectedRisks={selectedRisks}
            type="risk"
            onRowGroupChange={(obj, key) => {
              updateDataRisk(obj, key, obj[key]);
            }}
            createableProps={{
              placeholder: `Enter title for new risk`,
              id: "quickAddRisk",
              btnText: `Add new risk`,
              addAction: handleAddRisk,
              addHelpTask: `Press enter to add new risk`,
              emptyErrorMessage: 'Please Enter Risk title',
              disabled: !workspaceRiskPer.createRisk.cando,
            }}
            headerProps={headProps}
            gridProps={{
              getContextMenuItems: getContextMenuItems,
              groupDisplayType: "groupRows",
              onRowClicked: handleRiskRowClick,
              reactUi: true,
              maintainColumnOrder: true,

            }}>
            <AgGridColumn
              headerName=""
              field="colorCode"
              cellRenderer="color"
              filter={false}
              cellClass={classes.taskColorCell}
              maxWidth={6}
              resizeable={false}
              pinned="left"
              lockPosition={true}
              suppressMovable={true}
            />

            <AgGridColumn
              headerName={riskTitleColumn.columnName}
              field={riskTitleColumn.columnKey}
              resizable={true}
              suppressMovable={true}
              checkboxSelection={teamCanView("bulkActionAccess")}
              sortable={true}
              filter={true}
              lockPosition={true}
              wrapText={riskTitleColumn.wrapText}
              cellRenderer={"title"}
              cellClass={`${classes.taskTitleCell} ag-textAlignLeft`}
              pinnedRowCellRenderer="customPinnedRowRenderer"
              pinned={"left"}
              comparator={sortAlphabetically}
              autoHeight={riskTitleColumn.autoHeight}
              width={riskTitleColumn.width || 300}
              minWidth={200}
              align="left"
              sort={riskTitleColumn.sort}
              sortIndex={riskTitleColumn.sortIndex == -1 ? null : riskTitleColumn.sortIndex}
              cellStyle={{ padding: 0 }}
              rowDrag={true}
            />
            {!sectionGroup || sectionGroup == "false"
              ? RenderColumnsWithoutSectionGrouping()
              : null}

            {sectionGroup == "true" && (
              <AgGridColumn
                visible={false}
                headerName="System Fields"
                headerClass={classes.systemFieldGroup}>
                {riskColumns.map((c, i) => {
                  const superSystemField = headerProps.ntaskFields.includes(c.columnKey);
                  const defaultColDef = superSystemField
                    ? riskColumnDefs(classes)[c.columnKey]
                    : riskColumnDefs(classes)[c.fieldType];
                  const {
                    hide,
                    pinned,
                    rowGroup,
                    rowGroupIndex,
                    sort,
                    sortIndex,
                    width,
                    wrapText,
                    autoHeight,
                  } = c;
                  return (
                    c.columnKey !== "title" && (
                      <AgGridColumn
                        key={c.id}
                        headerName={c.columnName}
                        field={c.columnKey}
                        suppressKeyboardEvent={!superSystemField}
                        hide={hide}
                        fieldType={c.fieldType}
                        pinned={pinned}
                        align="left"
                        rowGroup={rowGroup}
                        sort={sort}
                        sortIndex={sortIndex == -1 ? null : sortIndex}
                        wrapText={wrapText}
                        rowGroupIndex={rowGroup && parseFloat(rowGroupIndex)}
                        autoHeight={autoHeight}
                        fieldId={c.fieldId}
                        width={width || (defaultColDef && defaultColDef.minWidth)}
                        cellStyle={{
                          justifyContent:
                            (c.fieldType === "dropdown" || c.fieldType === "filesAndMedia") &&
                              !superSystemField
                              ? "flex-start"
                              : "center",
                        }}
                        {...defaultColDef}
                      />
                    )
                  );
                })}
              </AgGridColumn>
            )}
            {sectionGroup == "true" &&
              sections.map((section) => {
                return (
                  section.fields.length > 0 && (
                    <AgGridColumn
                      headerName={section.columnName}
                      resizeable={false}
                      headerGroupComponent="customHeaderGroupComponent"
                      color={section.color}
                      headerClass={classes.sectionFieldsGroup}>
                      {section.fields.map((c, i) => {
                        const defaultColDef = riskColumnDefs(classes)[c.fieldType];
                        const {
                          hide,
                          pinned,
                          rowGroup,
                          sort,
                          sortIndex,
                          width,
                          wrapText,
                          autoHeight,
                          rowGroupIndex
                        } = c;
                        return (
                          <AgGridColumn
                            key={c.id}
                            headerName={c.columnName}
                            field={c.columnKey}
                            suppressKeyboardEvent={true}
                            hide={hide}
                            fieldType={c.fieldType}
                            pinned={pinned}
                            align="left"
                            rowGroup={rowGroup}
                            rowGroupIndex={rowGroup && parseFloat(rowGroupIndex)}
                            sort={sort}
                            sortIndex={sortIndex == -1 ? null : sortIndex}
                            wrapText={wrapText}
                            autoHeight={autoHeight}
                            width={width || (defaultColDef && defaultColDef.minWidth)}
                            fieldId={c.fieldId}
                            cellStyle={{
                              justifyContent:
                                c.fieldType === "dropdown" || c.fieldType === "filesAndMedia"
                                  ? "flex-start"
                                  : "center",
                            }}
                            {...defaultColDef}
                          />
                        );
                      })}
                    </AgGridColumn>
                  )
                );
              })}
            {sectionGroup == "true" && nonSectionFields.length ? (
              <AgGridColumn resizeable={false} headerClass={classes.taskTitleField}>
                {nonSectionFields.map((c, i) => {
                  const defaultColDef = riskColumnDefs(classes)[c.fieldType];
                  const { hide, pinned, rowGroup, sort, sortIndex, width, wrapText, autoHeight, rowGroupIndex } =
                    c;
                  return (
                    <AgGridColumn
                      key={c.id}
                      headerName={c.columnName}
                      field={c.columnKey}
                      fieldType={c.fieldType}
                      hide={hide}
                      suppressKeyboardEvent={true}
                      pinned={pinned}
                      align="left"
                      rowGroup={rowGroup}
                      sort={sort}
                      sortIndex={sortIndex == -1 ? null : sortIndex}
                      rowGroupIndex={rowGroup && parseFloat(rowGroupIndex)}
                      wrapText={wrapText}
                      autoHeight={autoHeight}
                      fieldId={c.fieldId}
                      width={width || (defaultColDef && defaultColDef.minWidth)}
                      cellStyle={{
                        justifyContent:
                          c.fieldType === "dropdown" || c.fieldType === "filesAndMedia"
                            ? "flex-start"
                            : "center",
                      }}
                      {...defaultColDef}
                    />
                  );
                })}
              </AgGridColumn>
            ) : null}

            <AgGridColumn
              headerName={""}
              field={"columnDropdown"}
              resizable={false}
              sortable={false}
              lockPinned={true}
              filter={false}
              width={40}
              headerClass={"columnSelectHeader"}
              pinned={"right"}
              suppressMovable={true}
              cellRenderer={"riskActionCellRenderer"}
              cellStyle={{ padding: 0, textAlign: "center" }}
            />
          </CustomTable>
          <RiskFilter
            sectionGrouping={sectionGrouping}
            handleChangeGrouping={handleChangeGrouping}
          />
        </div>
      )}
    </>
  );
}, areEqual);

function areEqual(prevProps, nextProps) {
  return isEqual(prevProps, nextProps);
  /*
  return true if passing nextProps to render would return
  the same result as passing prevProps to render,
  otherwise return false
  */
}

const GroupRowInnerRenderer = (props) => {
  return <GroupByComponents data={props} feature="risk" />;
};

export default compose(
  injectIntl,
  withSnackbar,
  withStyles(riskListStyles, { withTheme: true })
)(RiskList);
