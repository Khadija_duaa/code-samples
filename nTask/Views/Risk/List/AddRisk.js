import React from "react";
import ReactDOM from "react-dom";


class AddIssue extends React.Component {
  constructor(props) {
    super(props);


    this.container = document.createElement("div");
  }

  componentDidMount() {
    this.rootSelector = document.querySelector(".react-grid-Header");
    this.rootSelector.appendChild(this.container);
  }

  componentWillUnmount() {
    this.rootSelector.removeChild(this.container);
  }

  render() {
    return ReactDOM.createPortal(<div {...this.props}>{this.props.children}</div>, this.container);
  }
}

export default AddIssue;