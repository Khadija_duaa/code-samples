import React, { Component, Fragment, useState } from "react";
import ReactDataGrid from "react-data-grid";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import BulkActions from "./BulkActions";
import AddRisk from "./AddRisk";
import Hotkeys from "react-hot-keys";
import Grid from "@material-ui/core/Grid";
import listStyles from "./styles";
import AddIcon from "@material-ui/icons/Add";
import CancelIcon from "@material-ui/icons/Cancel";
import IconButton from "../../../components/Buttons/IconButton";
// import TableActionDropDown from "./ActionDropDown";
import ColumnSelectionDropdown from "../../../components/Dropdown/SelectedItemsDropDown/index";
import { getCustomFields } from "../../../helper/customFieldsData";
import helper from "../../../helper";
import Typography from "@material-ui/core/Typography";
import RiskDetails from "../RiskDetails/RiskDetails";
import { calculateContentHeight } from "../../../utils/common";
import DefaultDialog from "../../../components/Dialog/Dialog";
import AddRiskForm from "../../AddNewForms/AddNewRiskForm";
import StarredCheckBox from "../../../components/Form/Starred";
import MenuList from "../../../components/Menu/TaskMenus/MemberListMenu";
import IconMenu from "../../../components/Menu/TaskMenus/IconMenu";
import cloneDeep from "lodash/cloneDeep";
import find from "lodash/find";
import queryString from "query-string";
import AssigneeDropdown from "../../../components/Dropdown/AssigneeDropdown";
import SearchDropdown from "../../../components/Dropdown/SearchDropdown";
import { columns } from "../constants";
import { Data } from "react-data-grid-addons";
import {
  UpdateTask,
  FetchTasksInfo,
  CopyTask,
  DeleteTask,
  ArchiveTask,
  UnArchiveTask,
} from "../../../redux/actions/tasks";
import { sortListData, getSortOrder } from "../../../helper/sortListData";
import { SaveItemOrder } from "../../../redux/actions/itemOrder";
import { UpdateCalenderTask } from "../../../redux/actions/calenderTasks";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";
import { FormattedMessage, injectIntl } from "react-intl";
import { getActiveFields } from "../../../helper/getActiveCustomFields";
import ProjectPermissionSelector from "../../../redux/selectors/projectPermissionSelector";

import {
  BulkUpdateRisk,
  SaveRisk,
  UpdateRisk,
  DeleteRisk,
  ArchiveRisk,
  UnarchiveRisk,
  StoreCopyRiskInfo,
  MarkRiskAsStarred,
  updateRisk,
  dispatchRisk,
} from "../../../redux/actions/risks";
import { generateUsername } from "../../../utils/common";
import RiskActionDropdown from "./RiskActionDropDown";
import EmptyState from "../../../components/EmptyStates/EmptyState";
import { getRisksPermissions, getRisksPermissionsWithoutArchieve } from "../permissions";
import { GetPermission } from "../../../components/permissions";
import { GroupingRowRenderer } from "./GroupingRowRenderer";
import Starred from "./Starred";
import { headerTranslations } from "../../../utils/headerTranslations";
import getTableColumns from "../../../helper/getTableColumns";
import CustomTooltip from "../../../components/Tooltip/Tooltip";
import UpdateMatrixDialog from "../../../components/Matrix/updateMatrixDialog.cmp";
import CustomFieldsColumns from "../../CustomFieldsColumns/CustomFieldsColumns";
import isEqual from "lodash/isEqual";
import DetailsDialog from "../../../components/Dialog/DetailsDialog/DetailsDialog";
import NewDetailView from "../NewRiskDetailView/NewRiskDetailView";
import { riskDetailDialog } from "../../../redux/actions/allDialogs";
import { generateTaskData, generateProjectData } from "../../../helper/generateSelectData";

const {
  Draggable: { Container: DraggableContainer, RowActionsCell, DropTargetRowContainer },
  Data: { Selectors },
  Toolbar,
} = require("react-data-grid-addons");

const {
  id,
  riskTitle,
  riskStatus,
  riskTask,
  riskImpact,
  riskLikelihood,
  riskOwner,
  projectName,
  updatedDate,
  createdBy,
  createdDate,
} = columns;
const RowRenderer = DropTargetRowContainer(ReactDataGrid.Row);

let increment = 20;
const Default_Column_Count = 8;
let total = Default_Column_Count;
const screenSize = 1380;
class RiskList extends React.Component {
  static defaultProps = { rowKey: "id" };
  constructor(props, context) {
    super(props, context);
    this.state = {
      _columns: [],
      rows: [],
      selectedIds: [],
      addRisk: false,
      addNewForm: false,
      completeDetails: [],
      value: "",
      progress: 0,
      riskId: "",
      isProgress: false,
      TaskError: false,
      TaskErrorMessage: "",
      isReorder: false,
      startDateOpen: false,
      dueDateOpen: false,
      newCol: [],
      showRiskDetails: true,
      detailsRiskId: "",
      loggedInTeam: "",
      isWorkspaceChanged: false,
      showRecords: increment,
      totalRecords: 0,
      members: [],
      isArchived: false,
      sortColumn: null,
      sortDirection: "NONE",
      selectedRiskObj: [],
      columnddData: getTableColumns("risk"),
      checkedItems: ["ID", "Title", "Status", "Risk Owner"],
      nTaskFields: ["Task", "Impact", "Likelihood"],
      assessmentDialogOpen: false,
      selectedRisk: {},
      assessmentMatrixValues: null,
    };
    this.addRisk = this.addRisk.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.addRiskInput = React.createRef();
    this.onKeyDown = this.onKeyDown.bind(this);
    this.cancelAddRisk = this.cancelAddRisk.bind(this);
    this.showRiskDetailsPopUp = this.showRiskDetailsPopUp.bind(this);
    this.closeRiskDetailsPopUp = this.closeRiskDetailsPopUp.bind(this);
    this.handleScrollDown = this.handleScrollDown.bind(this);
    this.scrollListener = this.scrollListener.bind(this);
    this.BulkUpdate = this.BulkUpdate.bind(this);
    this.closeActionMenu = this.closeActionMenu.bind(this);
  }
  customFieldChange = (option, obj, currentRisk) => {
    const riskCopy = currentRisk;
    const { fieldId } = obj;
    const newObj = {
      groupType: "risk",
      groupId: riskCopy.id,
      fieldId,
      fieldData: { data: option },
    };
    if (obj.fieldType == "matrix") {
      const selectedField =
        riskCopy.customFieldData && riskCopy.customFieldData.find(x => x.fieldId == obj.fieldId);
      const isExist =
        selectedField &&
        selectedField.fieldData.data.findIndex(f => f.cellName == option.cellName) > -1;
      if (isExist === true) return;
    }

    if (obj.fieldType == "matrix") {
      newObj.fieldData.data = newObj.fieldData.data.map(opt => {
        return { cellName: opt.cellName, createdDate: opt.createdDate };
      });
    } else {
      if (obj.settings.multiSelect)
        newObj.fieldData.data = newObj.fieldData.data.map(opt => {
          return { id: opt.id };
        });
      else {
        newObj.fieldData.data = { id: `${option.length ? option[0].id : ""}` };
      }
    }

    this.props.updateRisk(newObj, res => {
      const resObj = res.data.entity[0];
      let customFieldsArr = [];
      // Updating Global state
      const isExist =
        riskCopy.customFieldData &&
        riskCopy.customFieldData.findIndex(c => c.fieldId === resObj.fieldId) > -1;
      if (isExist) {
        customFieldsArr = riskCopy.customFieldData.map(c => {
          return c.fieldId === resObj.fieldId ? resObj : c;
        });
      } else {
        customFieldsArr = riskCopy.customFieldData
          ? [...riskCopy.customFieldData, resObj]
          : [resObj];
      }
      let newRiskObj = { ...riskCopy, customFieldData: customFieldsArr };
      this.props.dispatchRisk(newRiskObj);
      this.handleAddAssessmentDialog(false, false, {});
    });
  };
  columnChangeCallback = () => {
    this.selectedColumnArr();
  };

  onColumnResize = (columnIndex, size) => {
    const { _columns } = this.state;
    if (size < 0) return;
    let columnWidth = JSON.parse(localStorage.getItem("columnWidthRisk"));
    if (columnWidth) {
      columnWidth[_columns[columnIndex - 1].key] = {
        width: size,
      };
      localStorage.setItem("columnWidthRisk", JSON.stringify(columnWidth));
    } else {
      let obj = {};
      obj[_columns[columnIndex - 1].key] = {
        width: size,
      };
      localStorage.setItem("columnWidthRisk", JSON.stringify(obj));
    }
  };

  getRiskColumns = () => {
    let columnWidth = JSON.parse(localStorage.getItem("columnWidthRisk"));
    const customColumnsList = getCustomFields(
      this.props.customFields,
      this.props.profileState.data,
      "risk"
    );
    const customColumnsListNames = customColumnsList
      .map(c => c.fieldName)
      .filter(c => c.toLowerCase() !== "checklist" && c.toLowerCase() !== "reminder");
    let customColumns = customColumnsList
      .filter(
        c => c.fieldType.toLowerCase() !== "checklist" && c.fieldType.toLowerCase() !== "reminder"
      )
      .filter(
        c => c.fieldName !== "Task" && c.fieldName !== "Impact" && c.fieldName !== "Likelihood"
      ); /** finding all custom fields realated to risk modules  */

    customColumns = customColumns.map((e, index) => {
      return {
        key: e.fieldName,
        name: e.fieldName,
        width: columnWidth && columnWidth[e.fieldName] ? columnWidth[e.fieldName].width : 250,
        resizable: true,
        formatter: value => {
          const riskPermission =
            value && value.row && value.row.projectId
              ? this.props.permissionObject[value.row.projectId]
                ? this.props.permissionObject[value.row.projectId].risk
                : this.props.workspaceRiskPer
              : this.props.workspaceRiskPer;
          return (
            <div key={e.fieldId}>
              <CustomFieldsColumns
                column={e}
                rowValue={value}
                permission={riskPermission.riskDetails.customsFields}
                customFieldData={value.row.customFieldData}
                customFieldChange={this.customFieldChange}
                handleAddAssessmentDialog={this.handleAddAssessmentDialog}
                disabled={this.props.isArchivedSelected}
              />
            </div>
          );
        },
        columnOrder: 10 + index,
        sortable: teamCanView("advanceSortAccess") ? false : true,
        cellClass: "dropDownCell",
      };
    });
    let _columns = [
      {
        key: id.key,
        name: id.name,
        cellClass: "firstColoumn",
        width: columnWidth && columnWidth[id.key] ? columnWidth[id.key].width : 130,
        resizable: true,
        sortDescendingFirst: true,
        formatter: value => {
          return (
            <div
              className={this.props.classes.taskListTitleTextCnt}
              style={{
                borderLeft: `6px solid ${value.row ? value.row.colorCode : ""}`,
              }}>
              <span className={this.props.classes.taskListTitleText}>
                {value.row ? value.row.riskId : ""}
              </span>
            </div>
          );
        },
        sortable: teamCanView("advanceSortAccess") ? false : true,
        columnOrder: 0,
      },
      {
        key: riskTitle.key,
        name: riskTitle.name,
        cellClass: "secondColoumn",
        visible: true,
        sortDescendingFirst: true,
        resizable: true,
        width: columnWidth && columnWidth[riskTitle.key] ? columnWidth[riskTitle.key].width : 400,
        formatter: value => {
          return (
            <>
              <div style={{ display: "flex", alignItems: "center" }}>
                <Starred
                  userId={this.props.profileState.data.userId}
                  currentRisk={value.row}
                  handleExportType={this.props.handleExportType}
                  disabled={this.props.isArchivedSelected}
                />
                <span
                  title={value.row ? value.row.title : ""}
                  className={this.props.classes.taskListTitleText}>
                  {value.row ? value.row.title : ""}
                </span>
              </div>
            </>
          );
        },
        columnOrder: 1,
        sortable: teamCanView("advanceSortAccess") ? false : true,
      },
      {
        key: riskStatus.key,
        name: riskStatus.name,
        width: columnWidth && columnWidth[riskStatus.key] ? columnWidth[riskStatus.key].width : 155,
        resizable: true,
        formatter: value => {
          const riskPermission =
            value && value.row && value.row.projectId
              ? this.props.permissionObject[value.row.projectId]
                ? this.props.permissionObject[value.row.projectId].risk
                : this.props.workspaceRiskPer
              : this.props.workspaceRiskPer;
          return (
            <IconMenu
              status={value.row && value.row.status ? value.row.status : ""}
              MenuData={value.row}
              isUpdated={this.isUpdated}
              menuType="riskStatus"
              keyType="status"
              heading="Select Status"
              viewType="risk"
              permission={riskPermission.riskDetails.editRiskStatus.isAllowEdit}
              permissionKey="status"
              riskPermission={riskPermission}
            />
          );
        },
        cellStyle: { background: "red" },
        cellClass: "dropDownCell",
        columnOrder: 2,
        sortable: teamCanView("advanceSortAccess") ? false : true,
      },
      {
        key: riskTask.key,
        name: riskTask.name,
        width: columnWidth && columnWidth[riskTask.key] ? columnWidth[riskTask.key].width : 180,
        resizable: true,
        formatter: value => {
          const riskPermission =
            value && value.row && value.row.projectId
              ? this.props.permissionObject[value.row.projectId]
                ? this.props.permissionObject[value.row.projectId].risk
                : this.props.workspaceRiskPer
              : this.props.workspaceRiskPer;
          const risk = value.row ? value.row : [];
          return (
            <SearchDropdown
              initSelectedOption={this.getSelectedTask(risk)}
              buttonProps={{
                disabled: this.props.isArchivedSelected,
              }}
              obj={risk}
              optionsList={this.generateTaskDropdownData(risk)}
              singleSelect={true}
              updateAction={(selectedTasks, task) => this.updateTask(selectedTasks, task, risk)}
              selectedOptionHead="Risk Task"
              allOptionsHead={<FormattedMessage id="task.label" defaultMessage="Tasks" />}
              buttonPlaceholder={
                <FormattedMessage id="common.bulk-action.selectTask" defaultMessage="Select Task" />
              }
              isMulti={false}
              multipleOptionsPlaceHolder="Multiple Tasks"
              disabled={!riskPermission.riskDetails.riskTask.isAllowEdit}
            />
          );
        },
        cellClass: "dropDownCell",
        columnOrder: 3,
      },
      {
        key: projectName.key,
        name: projectName.name,
        resizable: true,
        width: 240,
        visible: teamCanView("projectAccess"),
        formatter: value => {
          const riskPermission =
            value && value.row && value.row.projectId
              ? this.props.permissionObject[value.row.projectId]
                ? this.props.permissionObject[value.row.projectId].risk
                : this.props.workspaceRiskPer
              : this.props.workspaceRiskPer;
          const risk = value.row ? value.row : [];
          const canEditRiskProject = !value.row.isArchive
            ? riskPermission.riskDetails.riskProject.isAllowEdit
            : true;
          return (
            <SearchDropdown
              initSelectedOption={risk.projectId ? this.getSelectedProject(risk) : []}
              obj={risk}
              disableDropdown={!canEditRiskProject}
              tooltip={false}
              key={risk.projectId}
              tooltipText={
                <FormattedMessage
                  id="task.detail-dialog.project.hint1"
                  defaultMessage="You cannot change project once user effort is added to task or you don't have permission to change project"
                />
              }
              optionsList={this.generateProjectDropdownData(risk)}
              singleSelect
              updateAction={(newValue, project) => this.handleSelectChange(newValue, project, risk)}
              selectedOptionHead={this.props.intl.formatMessage({
                id: "common.task-project.label",
                defaultMessage: "Task Project",
              })}
              allOptionsHead={this.props.intl.formatMessage({
                id: "project.label",
                defaultMessage: "Projects",
              })}
              buttonPlaceholder={
                <FormattedMessage
                  id="task.creation-dialog.form.project.placeholder"
                  defaultMessage="Select Project"
                />
              }
              buttonProps={{
                disabled: !canEditRiskProject,
              }}
              IssueView={true}
            />
          );
        },
        cellClass: "dropDownCell",
        columnOrder: 6,

        sortable: !teamCanView("advanceSortAccess"),
      },
      {
        key: riskImpact.key,
        name: riskImpact.name,
        width: columnWidth && columnWidth[riskImpact.key] ? columnWidth[riskImpact.key].width : 130,
        resizable: true,
        formatter: value => {
          const riskPermission =
            value && value.row && value.row.projectId
              ? this.props.permissionObject[value.row.projectId]
                ? this.props.permissionObject[value.row.projectId].risk
                : this.props.workspaceRiskPer
              : this.props.workspaceRiskPer;
          return (
            <IconMenu
              status={value.row && value.row.impact ? value.row.impact : ""}
              MenuData={value.row}
              isUpdated={this.isUpdated}
              menuType="impact"
              keyType="impact"
              heading={this.props.intl.formatMessage({
                id: "risk.common.impact.label",
                defaultMessage: "Select Impact",
              })}
              viewType="risk"
              permission={riskPermission.riskDetails.editRiskImpact.isAllowEdit}
              permissionKey="impact"
              riskPermission={riskPermission}
            />
          );
        },
        cellClass: "dropDownCell",
        sortable: teamCanView("advanceSortAccess") ? false : true,
        columnOrder: 4,
      },
      {
        key: riskLikelihood.key,
        name: riskLikelihood.name,
        width:
          columnWidth && columnWidth[riskLikelihood.key]
            ? columnWidth[riskLikelihood.key].width
            : 155,
        resizable: true,
        formatter: value => {
          const riskPermission =
            value && value.row && value.row.projectId
              ? this.props.permissionObject[value.row.projectId]
                ? this.props.permissionObject[value.row.projectId].risk
                : this.props.workspaceRiskPer
              : this.props.workspaceRiskPer;
          return (
            <MenuList
              id={value.row ? value.row.id : ""}
              likelihood={value.row ? value.row.likelihood : []}
              MenuData={value.row}
              placementType="bottom-end"
              isUpdated={this.isUpdated}
              viewType="risk"
              isLikelihood={true}
              showRiskDetails={this.state.showRiskDetails}
              permission={riskPermission.riskDetails.editlikelihood.isAllowEdit}
              permissionKey="likelihood"
              riskPermission={riskPermission}
            />
          );
        },
        cellClass: "dropDownCell",
        columnOrder: 5,
        sortable: teamCanView("advanceSortAccess") ? false : true,
      },
      {
        key: riskOwner.key,
        name: riskOwner.name,
        width: columnWidth && columnWidth[riskOwner.key] ? columnWidth[riskOwner.key].width : 180,
        resizable: true,
        formatter: value => {
          const riskPermission =
            value && value.row && value.row.projectId
              ? this.props.permissionObject[value.row.projectId]
                ? this.props.permissionObject[value.row.projectId].risk
                : this.props.workspaceRiskPer
              : this.props.workspaceRiskPer;
          return (
            <AssigneeDropdown
              assignedTo={value.row ? value.row.assigneeList : []}
              updateAction={this.updateRiskOwner}
              isArchivedSelected={this.props.isArchivedSelected}
              obj={value.row}
              singleSelect={true}
              assigneeHeading="Risk Owner"
              addPermission={riskPermission.riskDetails.riskOwner.cando}
              deletePermission={riskPermission.riskDetails.riskOwner.cando}
            />
          );
        },
        cellClass: "dropDownCell",
        columnOrder: 6,
        sortable: false,
      },
      {
        key: updatedDate.key,
        name: updatedDate.name,
        width:
          columnWidth && columnWidth[updatedDate.key] ? columnWidth[updatedDate.key].width : 185,
        resizable: true,
        formatter: value => {
          return (
            <Typography variant="h6">
              {value.row
                ? helper.RETURN_CUSTOMDATEFORMATFORLASTUPDATE(value.row.updatedDate)
                : helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(new Date())}
            </Typography>
          );
        },
        columnOrder: 7,
        sortable: teamCanView("advanceSortAccess") ? false : true,
        cellClass: "dropDownCell",
      },
      {
        key: createdBy.key,
        name: createdBy.name,
        width: columnWidth && columnWidth[createdBy.key] ? columnWidth[createdBy.key].width : 185,
        resizable: true,
        formatter: value => {
          let rowData = {};
          if (value.row && value.row.createdBy)
            rowData = this.state.members.find(x => x.userId === value.row.createdBy);

          const name = rowData
            ? generateUsername(rowData.email, rowData.userName, rowData.fullName)
            : null;
          return <Typography variant="h6">{name}</Typography>;
        },
        columnOrder: 8,
        sortable: teamCanView("advanceSortAccess") ? false : true,
        cellClass: "dropDownCell",
      },
      {
        key: createdDate.key,
        name: createdDate.name,
        width:
          columnWidth && columnWidth[createdDate.key] ? columnWidth[createdDate.key].width : 150,
        resizable: true,
        formatter: value => {
          return (
            <Typography variant="h6">
              {value.row
                ? helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(value.row.createdDate)
                : helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(new Date())}
            </Typography>
          );
        },
        columnOrder: 9,
        sortable: teamCanView("advanceSortAccess") ? false : true,
        cellClass: "dropDownCell",
      },
    ].filter(c => {
      const isSystemField = c.name == "Task" || c.name == "Likelihood" || c.name == "Impact";
      return (isSystemField && customColumnsListNames.includes(c.name)) || !isSystemField;
    });
    let arr = [..._columns, ...customColumns];
    let lastColumn = [
      {
        key: "optionList",
        name: "",
        formatter: value => {
          const riskPermission =
            value && value.row && value.row.projectId
              ? this.props.permissionObject[value.row.projectId]
                ? this.props.permissionObject[value.row.projectId].risk
                : this.props.workspaceRiskPer
              : this.props.workspaceRiskPer;
          return (
            <RiskActionDropdown
              risk={value.row}
              CopyTask={this.props.CopyTask}
              DeleteTask={this.props.DeleteTask}
              ArchiveTask={this.props.ArchiveTask}
              CopyCalenderTask={this.props.CopyCalenderTask}
              UnArchiveTask={this.props.UnArchiveTask}
              UpdateRisk={this.props.UpdateRisk}
              selectedColor={value.row && value.row.colorCode ? value.row.colorCode : { hex: "" }}
              isUpdated={this.isUpdated}
              DeleteRisk={this.props.DeleteRisk}
              closeActionMenu={this.closeActionMenu}
              isArchivedSelected={this.props.isArchivedSelected}
              filterRiskDeleteTask={this.props.filterRiskDeleteTask}
              filterRisk={this.props.filterRisk}
              riskPer={riskPermission}
              handleExportType={this.props.handleExportType}
            />
          );
        },
        cellClass: "dropDownCell lastColoumn",
        columnOrder: arr.length,
        width: columnWidth && columnWidth["optionList"] ? columnWidth["optionList"].width : 50,
      },
    ];
    return [...arr, ...lastColumn];
  };
  handleAddAssessmentDialog = (value, e = false, risk, fieldId) => {
    const { customFields, profileState } = this.props;
    e && e.stopPropagation();
    e && e.preventDefault();
    const selectedMatrixObj = customFields.data.find(m => m.fieldId === fieldId) || false;
    const assessmentMatrixValues =
      (risk.customFieldData &&
        risk.customFieldData.find(c => c.fieldId === selectedMatrixObj.fieldId)) ||
      false;
    this.setState({
      assessmentDialogOpen: value,
      selectedRisk: risk,
      selectedMatrix: fieldId,
      assessmentMatrixValues: assessmentMatrixValues,
    });
  };

  //Function that updates assignee of respective document (e.g. task/risk/issue/meeting)
  updateRiskOwner = (assignedTo, risk) => {
    let newRiskObj = { ...risk, riskOwner: assignedTo[0] && assignedTo[0].userId };

    this.props.UpdateRisk(
      newRiskObj,
      succ => {},
      err => {
        this.props.handleExportType(err.data.message, "error");
      }
    );
  };
  componentWillUnmount() {
    if (this.canvas) {
      this.canvas.removeEventListener("scroll", this.scrollListener);
    }
  }
  filterOnBasicsOfScreeenSize = param => {
    if (window.innerWidth <= screenSize) {
      total = 5;
      return param.slice(0, 5);
    } else {
      total = Default_Column_Count;
      return param;
    }
  };

  selectedColumnArr = () => {
    let newCols = this.getRiskColumns();
    let newColsId = newCols.map(c => c.name);
    const { riskColumns = [] } = this.props;
    const riskColumnOrder = riskColumns
      .filter(c => !c.hide && newColsId.includes(c.columnName))
      .map(c => c.columnName);

    let columns = newCols.filter(
      x =>
        riskColumnOrder.indexOf(x.name) >= 0 ||
        x.columnOrder === newCols.length - 1 ||
        x.key == "title"
    );
    // columnDropDownData = this.filterNTaskField(
    //   columnDropDownData
    // ); /** function that filters the ntask field if user have hide them */
    // columnDropDownData = [...new Set(columnDropDownData)]; /** containing unique values */
    this.setState(
      {
        _columns: columns,
      },
      () => {}
    );
  };

  filterNTaskField = itemArray => {
    /** function that checks if nTask fields exists and if it is not hide by the user/ other wise filter it from rows and also the column seletion drop down */
    const { customFields, profileState } = this.props;

    let items = itemArray.reduce((result, currentValue) => {
      let isExist = customFields.data.find(cf => cf.fieldName == currentValue);
      if (
        isExist &&
        !isExist.settings.isHidden &&
        isExist.settings.hiddenInWorkspaces.indexOf(profileState.data.loggedInTeam) == -1 &&
        isExist.settings.hiddenInGroup.indexOf("risk") == -1
      )
        result.push(currentValue);
      if (!isExist) result.push(currentValue);
      return result;
    }, []);
    return items;
  };

  componentDidMount() {
    const { workspaces } = this.props;
    let loggedInTeam = this.props.profileState.data.loggedInTeam;

    if (loggedInTeam) {
      let { riskColumn, riskDirection } = getSortOrder(workspaces, loggedInTeam, 5);

      if (riskColumn && riskDirection) {
        let rows = this.getDetailedRisks();
        this.sortRows(rows.completeDetails, riskColumn, riskDirection);
      } else {
        this.setState({
          rows: this.getDetailedRisks().completeDetails,
          riskColumn: null,
          riskDirection: "",
        });
      }
    }

    this.setState({
      // rows: details.completeDetails,
      // newCol: this.state._columns,
      showRiskDetails: false,
      loggedInTeam: loggedInTeam,
      // members: details.members
    });
  }
  checkRiskListAccordingToPermission = (stateTask, selectedTask) => {
    if (stateTask.length <= 0 || selectedTask.length == 0) {
      /** if state task is empty then accept the selected task */
      return true;
    } else {
      this.props.showSnackBar(
        "Oops! You cannot link this task to this risk, because it is already linked to another task. Unlink the risk with the other task first and then try again. ",
        "error"
      );
      return false;
    }
  };
  //Function that updates task project
  updateTask = (selectedTasks, task, risk) => {
    let permission = this.checkRiskListAccordingToPermission(
      this.generateSelectedValue(risk),
      selectedTasks
    );
    if (permission) {
      let linkedTaskId = selectedTasks.map(task => {
        return task.id;
      });
      let newRiskObj = {
        ...risk,
        linkedTasks: linkedTaskId,
        projectId: selectedTasks.length > 0 ? selectedTasks[0].obj.projectId : risk.projectId,
      };
      this.props.UpdateRisk(
        newRiskObj,
        () => {},
        err => {
          this.props.handleExportType(err.data.message, "error");
        }
      );
    }
  };
  // Generate list of all projects for dropdown understandable form
  generateTaskDropdownData = risk => {
    const { tasks } = this.props;
    let tasksData = risk.projectId ? tasks.filter(t => t.projectId === risk.projectId) : tasks;

    let tasksArr = generateTaskData(tasksData);
    return tasksArr;
  };

  generateSelectedValue = currentRisk => {
    /** function for generating the selected task list objects from task id and return taskList for showing pre populated selected task if any => function call in componentDidMount */

    const selectedTaskList = this.props.tasks.filter(task => {
      return currentRisk.linkedTasks.indexOf(task.taskId) > -1;
    });
    let taskList = generateTaskData(selectedTaskList);
    return taskList;
  };

  // Generate list of all projects for dropdown understandable form
  generateProjectDropdownData = risk => {
    const { projects } = this.props;
    let selectedTasks = this.generateSelectedValue(risk);

    let selectedProject = selectedTasks.length
      ? projects.filter(t => t.projectId === selectedTasks[0].obj.projectId)
      : projects;
    selectedProject = selectedProject.length ? selectedProject : projects;

    return generateProjectData(selectedProject.filter(p => p.projectId !== risk.projectId));
  };

  handleSelectChange = (newValue, project, risk) => {
    const projectId = newValue.length ? newValue[0].id : "";
    let selectedTasks = this.generateSelectedValue(risk);

    let linkTask = selectedTasks.length
      ? selectedTasks[0].obj.projectId == "" || !selectedTasks[0].obj.projectId
        ? []
        : risk.linkedTasks
      : risk.linkedTasks;

    const newRiskObj = {
      ...risk,
      projectId: projectId,
      linkedTasks: newValue.length == 0 ? [] : linkTask,
    };
    this.props.UpdateRisk(newRiskObj, () => {});
  };
  // Generate list of all options for dropdown understandable form
  generateFieldDropdownData = param => {
    let Arr = param.values.data.map(p => {
      return { label: p.value, id: p.value, obj: p };
    });
    return Arr;
  };
  //Generate list of selected options for project dropdown understandable form
  getSelectedTask = risk => {
    const { tasks } = this.props;
    let selectedTask = tasks.filter(task => {
      return risk.linkedTasks.indexOf(task.taskId) > -1;
    });
    return selectedTask.map(selected => {
      return {
        label: selected.taskTitle,
        id: selected.taskId,
        obj: risk,
      };
    });
  };
  getSelectedProject = risk => {
    const { projects } = this.props;
    const selectedProject = projects.find(project => {
      return project.projectId == risk.projectId;
    });

    return selectedProject
      ? [
          {
            label: selectedProject.projectName,
            id: selectedProject.projectId,
            obj: risk,
          },
        ]
      : [];
  };
  //Generate list of selected options for fields dropdown understandable form
  // getSelectedField = param => {
  //   if (param) {
  //     let selectedField = param.fieldData.data.map(selected => {
  //       return {
  //         label: selected.label,
  //         value: selected.label,
  //         id: param.fieldId,
  //         obj: param,
  //       };
  //     });
  //     return selectedField;
  //   }
  //   return [];
  // };
  getSelectedSingleField = param => {
    if (param) {
      return [
        {
          label: param.fieldData.data.label,
          value: param.fieldData.data.label,
          id: param.fieldData.data.label,
          obj: param,
        },
      ];
    } else return [];
  };
  queryRiskUrl = () => {
    const {
      dialogsState: { riskDetailDialog },
    } = this.props;
    let searchQuery = this.props.history.location.search; //getting risk id in the url
    const { filteredRisks } = this.props;
    const { showRiskDetails } = this.state;
    if (searchQuery && !showRiskDetails && !riskDetailDialog.id) {
      // checking if there is risk Id in the url or not
      let riskId = queryString.parseUrl(searchQuery).query.riskId; //Parsing the url and extracting the risk id using query string
      let validateRisk = filteredRisks.findIndex(risk => {
        // Checking if the risk id is valid or not
        return riskId == risk.id;
      });

      if (validateRisk > -1) {
        //If the risk id is valid
        this.setState({ showRiskDetails: true, detailsRiskId: riskId });
      } else {
        this.props.showSnackBar("Oops! Item not found", "info");
        //In case the risk id in the url is wrong user is redirected to risks Page
        this.props.history.push("/risks");
      }
    }
  };
  componentDidUpdate(prevProps, prevState) {
    const { addRisk, isReorder, sortColumn, sortDirection, detailsRiskId } = this.state;
    const { sortObj, filteredRisks, customFields } = this.props;
    const customFieldsChanged =
      JSON.stringify(prevProps.customFields) !== JSON.stringify(customFields);
    const riskChanged = JSON.stringify(prevProps.filteredRisks) !== JSON.stringify(filteredRisks);

    const riskDetailsRisk = filteredRisks.find(r => {
      // Checking if the issue details risk exist in the list or not
      return r.id == detailsRiskId;
    });
    if (detailsRiskId && !riskDetailsRisk) {
      // if risk does not exist in the filtered list remove the risk details from state
      //This scenario can be produced if you apply filter on a risk with assignee from sidebar and open risk details and change assignee and than toggle filter from sidebar again
      this.setState({ showRiskDetails: false, detailsRiskId: "" });
    }

    if (addRisk == true) {
      this.addRiskInput.current.focus();
    }

    if (riskChanged) {
      let details = this.getDetailedRisks();
      if (sortColumn) this.sortRows(details.completeDetails, sortColumn, sortDirection);
      else
        this.setState({
          rows: details.completeDetails,
          members: details.members,
        });
    } else if (isReorder && isReorder !== prevState.isReorder) {
      this.setItemsOrder();
    }
    // Clearing bulk selection on archive mode selection
    else if (JSON.stringify(prevProps.riskState) !== JSON.stringify(this.props.riskState)) {
      this.setState({ selectedIds: [] });
    }

    if (
      sortObj &&
      sortObj.column &&
      (sortObj.column !== prevProps.sortObj.column ||
        sortObj.direction !== prevProps.sortObj.direction)
    ) {
      /** when user click on selected sort button for changing the sort order, this condtion will execute */
      let details = this.getDetailedRisks();
      this.sortRows(
        details.completeDetails,
        sortObj.column,
        sortObj.direction
      ); /** function call for reseting the state risk list into selected sort manner */
    } else if (
      sortObj &&
      sortObj.cancelSort == "NONE" &&
      sortObj.cancelSort !== prevProps.sortObj.cancelSort
    ) {
      /** when user click on cancel sort this condtion will execute */
      let details = this.getDetailedRisks();
      this.setState({
        rows: details.completeDetails,
      }); /** updating state risk list  */
    }

    if (
      customFieldsChanged ||
      riskChanged ||
      !isEqual(prevProps.riskColumns, this.props.riskColumns)
    ) {
      /** when adding new column or field then render again the columns so the added column will remaain added */
      this.selectedColumnArr();
    }

    this.queryRiskUrl();
    // this.renderHeaders();
  }

  renderHeaders = () => {
    var elements = document.getElementsByClassName("widget-HeaderCell__value");
    var sortableElements = document.getElementsByClassName("react-grid-HeaderCell-sortable");
    for (var i = 0; i < elements.length; i++) {
      elements[i].innerText = headerTranslations(elements[i].innerText, this.props.intl);
    }
    for (var i = 0; i < sortableElements.length; i++) {
      sortableElements[i].innerText = headerTranslations(
        sortableElements[i].innerText,
        this.props.intl
      );
    }
  };
  getDetailedRisks = () => {
    let risks = cloneDeep(this.props.filteredRisks) || [];
    let members = this.props.profileState.data.member.allMembers || [];

    let completeDetails = risks.map(x => {
      let member = members.filter(m => m.userId === x.riskOwner);
      x.assigneeList = member && member.length ? member : [];
      return x;
    });
    return {
      completeDetails,
      members,
    };
  };

  setItemsOrder = () => {
    let props = this.props;
    let rows = cloneDeep(this.state.rows)
      .map(x => {
        let position = helper.RETURN_ITEMORDER(props.itemOrderState.data.itemOrder, x.id);
        x.taskOrder = position || 0;
        return x;
      })
      .sort((a, b) => a.taskOrder - b.taskOrder);
    this.setState({ rows, isReorder: false });
  };

  toggleStarChecked = (event, risk) => {
    let isStared = event.target.checked;
    if (risk) {
      let obj = {
        Id: risk.id,
        MarkStar: isStared,
      };
      this.props.MarkRiskAsStarred(obj, (err, data) => {});
    }
  };

  closeActionMenu(option) {
    if (this.props.riskState.indexOf("Archived") >= 0)
      this.setState({
        isArchived: true,
        detailsRiskId: option === "CloseDetailsDialogue" ? "" : this.state.detailsRiskId,
        showRiskDetails: option === "CloseDetailsDialogue" ? false : this.state.showRiskDetails,
      });
    else
      this.setState({
        isArchived: false,
        detailsRiskId: option === "CloseDetailsDialogue" ? "" : this.state.detailsRiskId,
        showRiskDetails: option === "CloseDetailsDialogue" ? false : this.state.showRiskDetails,
      });
  }

  scrollListener() {
    if (this.canvas.scrollHeight - (this.canvas.scrollTop + this.canvas.clientHeight) < 1) {
      this.handleScrollDown();
    }
  }

  handleScrollDown = () => {
    this.setState({ showRecords: this.state.showRecords + increment }, () => {
      if (this.state.showRecords >= this.state.rows.length) {
        this.setState({ showRecords: this.state.rows.length });
      }
    });
  };
  showRiskDetailsPopUp(e, data) {
    const riskPermission =
      data && data.projectId
        ? this.props.permissionObject[data.projectId]
          ? this.props.permissionObject[data.projectId].risk
          : this.props.workspaceRiskPer
        : this.props.workspaceRiskPer;

    if (data && riskPermission.riskDetails.cando) {
      const riskDetailsRisk = this.props.filteredRisks.find(r => {
        // Checking if the issue details risk exist in the list or not
        return r.id == data.id;
      });
      this.setState(
        {
          showRiskDetails: true,
          detailsRiskId: data.id,
          selectedRisk: riskDetailsRisk,
        },
        () => {
          this.setState({
            showRiskDetails: false,
            detailsRiskId: null,
          });
        }
      );
    }
  }
  closeRiskDetailsPopUp() {
    this.setState({ showRiskDetails: false, selectedRisk: {}, detailsRiskId: "" });
  }

  isUpdated = (data, callback) => {
    if (data.unarchive) {
      const risk = this.props.filteredRisks.filter(risk => risk.id === data.id)[0];
      this.props.UnarchiveRisk(
        data,
        resp => {
          if (this.props.riskState && this.props.riskState.indexOf("Archived") >= 0)
            this.setState({ isArchived: true });
          if (callback) callback();
          this.props.filterRisk([data.id]);
          // this.props.StoreCopyRiskInfo(risk);
        },
        error => {
          if (error && error.status === 500) {
            this.props.handleExportType("Server throws error", "error");
          }
        }
      );
    } else {
      this.props.UpdateRisk(
        data,
        data => {
          if (this.props.riskState && this.props.riskState.indexOf("Archived") >= 0)
            this.setState({ isArchived: true });

          if (callback) callback();
          //
        },
        err => {
          this.props.handleExportType(err.data.message, "error");
        }
      );
    }
  };

  BulkUpdate(data, callback) {
    this.props.BulkUpdateRisk(data, (err, resp) => {
      if (callback) {
        this.setState({ selectedIds: [] });
        callback();
      }
      //
      if (resp) {
        // Unarchive scenerio
        if (this.props.riskState.indexOf("Archived") >= 0) {
          this.setState({ isArchived: true, selectedIds: [] });
          this.props.filterRisk(data.riskIds);
        } else this.setState({ isArchived: false });
      }
    });
  }

  addRisk(keyName) {
    if (keyName && keyName === "enter") {
      this.setState({ addNewForm: true });
    } else if (keyName && keyName === "inline") {
      this.setState({ addRisk: true });
    } else {
      this.setState({ addNewForm: true });
    }
  }

  cancelAddRisk() {
    this.setState({
      addRisk: false,
      addNewForm: false,
      TaskError: false,
      TaskErrorMessage: "",
    });
  }
  handleChange(e) {
    if (e.target.value.length > 250) {
      this.setState({
        TaskError: true,
        TaskErrorMessage: this.props.intl.formatMessage({
          id: "risk.creation-dialog.form.validation.risk-title.length",
          defaultMessage: "Please enter less than 250 characters",
        }),
        value: e.target.value,
      });
    } else {
      this.setState({
        TaskError: false,
        TaskErrorMessage: "",
        value: e.target.value,
      });
    }
  }
  onKeyDown(event) {
    if (event.keyCode === 13) {
      let checkEmpty = helper.RETURN_CECKSPACES(this.state.value);
      if (!this.state.value || checkEmpty) {
        this.setState({
          TaskError: true,
          TaskErrorMessage:
            checkEmpty ||
            this.props.intl.formatMessage({
              id: "risk.creation-dialog.form.validation.risk-title.empty",
              defaultMessage: "Please enter risk title",
            }),
        });
        return;
      }

      if (this.state.value && this.state.value.length > 250) {
        this.setState({
          TaskError: true,
          TaskErrorMessage: this.props.intl.formatMessage({
            id: "risk.creation-dialog.form.validation.risk-title.length",
            defaultMessage: "Please enter less than 250 characters",
          }),
        });
        return;
      }

      this.props.SaveRisk(
        {
          title: this.state.value,
        },
        (err, x) => {
          if (x) {
            this.grid.selectAllCheckbox.checked = false;
          } else {
            this.setState({
              TaskError: false,
              TaskErrorMessage: err ? err.data : err,
            });
          }
        }
      );
      this.setState({ value: "" });
    } else if (event.keyCode == 27) {
      this.setState({ addRisk: false });
    } else {
      this.setState({ addRisk: true });
    }
  }

  getRandomDate = (start, end) => {
    return new Date(
      start.getTime() + Math.random() * (end.getTime() - start.getTime())
    ).toLocaleDateString();
  };

  rowGetter = i => {
    if (i >= 0) return this.state.rows[i];
  };

  isDraggedRowSelected = (selectedRows, rowDragSource) => {
    if (selectedRows && selectedRows.length > 0) {
      let key = this.props.rowKey;
      return selectedRows.filter(r => r[key] === rowDragSource.data[key]).length > 0;
    }
    return false;
  };

  reorderRows = e => {
    let selectedRows = Selectors.getSelectedRowsByKey({
      rowKey: this.props.rowKey,
      selectedKeys: this.state.selectedIds,
      rows: this.state.rows,
    });
    let draggedRows = this.isDraggedRowSelected(selectedRows, e.rowSource)
      ? selectedRows
      : [e.rowSource.data];
    let undraggedRows = this.state.rows.filter(function(r) {
      return draggedRows.indexOf(r) === -1;
    });
    let args = [e.rowTarget.idx, 0].concat(draggedRows);
    Array.prototype.splice.apply(undraggedRows, args);

    undraggedRows = undraggedRows.map((x, i) => {
      x.taskOrder = i;
      return x;
    });
    let orderToBeSaved = undraggedRows.map(x => {
      return x.id;
    });
    let itemOrder = this.props.itemOrderState.data;
    itemOrder.itemOrder = itemOrder.itemOrder
      .map((x, i) => {
        if (orderToBeSaved.indexOf(x.itemId) >= 0) {
          return {
            itemId: x.itemId,
            position: orderToBeSaved.indexOf(x.itemId),
          };
        } else return x;
      })
      .sort((a, b) => a.position - b.position);
    this.props.SaveItemOrder(
      itemOrder,
      data => {
        this.setState({
          rows: undraggedRows,
          isReorder: true,
          showRiskDetails: false,
        });
      },
      () => {},
      undraggedRows,
      "risk"
    );
  };

  onRowsSelected = rows => {
    const filterRows = rows.filter(r => !r.row.__metaData); // Removing Grouping Rows from the selected list
    let selectedRiskObjArr = [...this.state.selectedRiskObj, ...rows];

    this.setState(
      {
        selectedIds: this.state.selectedIds.concat(filterRows.map(r => r.row[this.props.rowKey])),
        showRiskDetails: false,
        selectedRiskObj: selectedRiskObjArr,
      },
      () => {
        if (this.state.rows.length === this.state.selectedIds.length) {
          this.grid.selectAllCheckbox.checked = true;
        }
      }
    );
  };

  onRowsDeselected = rows => {
    let rowIds = rows.map(r => r.row[this.props.rowKey]);
    this.setState({
      selectedIds: this.state.selectedIds.filter(i => rowIds.indexOf(i) === -1),
      showRiskDetails: false,
      selectedRiskObj: this.state.selectedRiskObj.filter(
        r => rowIds.indexOf(r.row[this.props.rowKey]) === -1
      ),
    });
  };

  sortRows = (initialRows, sortColumn, sortDirection) => {
    if (sortDirection === "NONE") {
      this.setState({
        rows: this.getDetailedRisks().completeDetails,
        sortColumn: null,
        sortDirection,
      });
    } else {
      let rows = sortListData(initialRows, sortColumn, sortDirection);
      this.setState({ rows, sortColumn, sortDirection });
    }
  };

  handleUpdateRiskName = title => {
    let newRiskObj = { ...this.state.selectedRisk, title: title };
    this.props.UpdateRisk(
      newRiskObj,
      succ => {},
      err => {
        this.props.handleExportType(err.data.message, "error");
      }
    );
  };

  render() {
    const {
      addRisk,
      addNewForm,
      selectedIds,
      detailsRiskId,
      isWorkspaceChanged,
      rows,
      selectedRiskObj,
      checkedItems,
      columnddData,
      selectedRisk,
    } = this.state;
    const {
      classes,
      theme,
      riskState,
      filteredRisks,
      selectedGroup,
      appliedFiltersState,
      isArchivedSelected,
      workspaceRiskPer,
      dialogsState: { riskDetailDialog },
    } = this.props;
    if (this.state.rows.length > 0 && this.state.rows.length === this.state.selectedIds.length) {
      if (this.grid) this.grid.selectAllCheckbox.checked = true;
    } else {
      if (this.grid) this.grid.selectAllCheckbox.checked = false;
    }
    if (isWorkspaceChanged) {
      if (this.grid) this.grid.selectAllCheckbox.checked = false;
    }
    const EmptyRowsView = () => {
      return isArchivedSelected ? (
        <EmptyState
          screenType="Archived"
          heading={
            <FormattedMessage id="common.archived.label" defaultMessage="No archived items found" />
          }
          message={
            <FormattedMessage
              id="common.archived.message"
              defaultMessage="You haven't archived any items yet."
            />
          }
          button={false}
        />
      ) : (riskState.length > 0 || appliedFiltersState.Risk) &&
        (filteredRisks || filteredRisks.length <= 0) ? (
        <EmptyState
          screenType="search"
          heading={
            <FormattedMessage id="common.search-list.label" defaultMessage="No Results Found" />
          }
          message={
            <FormattedMessage
              id="common.search-list.message"
              defaultMessage="No matching results found against your filter criteria."
            />
          }
          button={false}
        />
      ) : workspaceRiskPer.createRisk.cando ? (
        <EmptyState
          screenType="risk"
          heading={
            <FormattedMessage
              id="common.create-first.risk.label"
              defaultMessage="Create your first risk"
            />
          }
          message={
            <FormattedMessage
              id="common.create-first.risk.messagea"
              defaultMessage='You do not have any risks yet. Press "Alt + R" or click on button below.'
            />
          }
          button={true}
        />
      ) : (
        <EmptyState
          screenType="risk"
          heading={
            <FormattedMessage
              id="common.create-first.risk.messageb"
              defaultMessage="You do not have any risks yet."
            />
          }
          message={
            <FormattedMessage
              id="common.create-first.risk.messagec"
              defaultMessage="Request your team member(s) to assign you a risk."
            />
          }
          button={false}
        />
      );
    };

    const groupBy = selectedGroup.key && [selectedGroup.key];
    const groupedRows = groupBy ? Data.Selectors.getRows({ rows, groupBy }) : rows;
    return (
      <Fragment>
        <DefaultDialog title="Add Risk" open={addNewForm} onClose={this.cancelAddRisk}>
          <AddRiskForm view={"Header-Menu"} closeAction={this.cancelAddRisk} />
        </DefaultDialog>
        {this.state.showRiskDetails &&
        detailsRiskId &&
        filteredRisks.length > 0 &&
        !riskDetailDialog.id
          ? // <RiskDetails
            //   isUpdated={this.isUpdated}
            //   closeRiskDetailsPopUp={this.closeRiskDetailsPopUp}
            //   currentRisk={find(rows, { id: detailsRiskId })}
            //   closeActionMenu={this.closeActionMenu}
            //   isArchivedSelected={isArchivedSelected}
            //   listView={true}
            //   filterRisk={this.props.filterRisk}
            //   riskPer={riskPer}
            // />
            this.props.riskDetailDialog({
              id: detailsRiskId,
              isArchived: isArchivedSelected,
              afterCloseCallBack: () => {
                this.setState({
                  showRiskDetails: false,
                  detailsRiskId: "",
                });
              },
            })
          : null}

        <Hotkeys keyName="alt+r" onKeyDown={this.onKeyDown} />
        <div style={{ width: "100%" }} className={selectedIds.length > 1 ? "multiChecked" : null}>
          {selectedIds.length > 1 ? (
            <BulkActions
              selectedIdsList={this.state.selectedIds}
              BulkUpdate={this.BulkUpdate}
              isArchivedFilter={this.props.riskState.indexOf("Archived") >= 0}
              workspaceRiskPer={workspaceRiskPer}
              selectedRiskObj={selectedRiskObj}
              permissionObject={this.props.permissionObject}
            />
          ) : null}
          <div className={classes.TableActionBtnDD}>
            <ColumnSelectionDropdown
              feature={"risk"}
              columnChangeCallback={this.columnChangeCallback}
            />
          </div>

          <DraggableContainer>
            <ReactDataGrid
              onRowClick={this.showRiskDetailsPopUp.bind(this)}
              enableCellSelection={true}
              ref={node => (this.grid = node)}
              rowActionsCell={RowActionsCell}
              columns={this.state._columns}
              emptyRowsView={EmptyRowsView}
              onGridSort={(sortColumn, sortDirection) =>
                this.sortRows(this.props.filteredRisks, sortColumn, sortDirection)
              }
              rowGetter={i => {
                return groupedRows[i];
              }}
              rowsCount={groupedRows.length}
              rowHeight={60}
              rowGroupRenderer={param => {
                return GroupingRowRenderer(param, rows, classes);
              }}
              headerRowHeight={isArchivedSelected || !workspaceRiskPer.createRisk.cando ? 50 : 105}
              minHeight={calculateContentHeight()}
              selectAllCheckbox={true}
              rowRenderer={<RowRenderer onRowDrop={this.reorderRows} />}
              rowSelection={{
                showCheckbox: true,
                enableShiftSelect: true,
                onRowsSelected: this.onRowsSelected,
                onRowsDeselected: this.onRowsDeselected,
                selectBy: {
                  keys: {
                    rowKey: this.props.rowKey,
                    values: this.state.selectedIds,
                  },
                },
              }}
              onColumnResize={this.onColumnResize}
            />
          </DraggableContainer>
          {/* </BottomScrollListener> */}
          {isArchivedSelected ? null : workspaceRiskPer.createRisk.cando ? (
            <AddRisk
              className={addRisk ? "addTaskInputCnt" : "addTaskCnt"}
              onClick={!addRisk ? () => this.addRisk("inline") : undefined}>
              {addRisk ? (
                <Fragment>
                  {this.state.TaskError ? (
                    <div for="TaskError" className={classes.selectError}>
                      {this.state.TaskErrorMessage ? this.state.TaskErrorMessage : ""}
                    </div>
                  ) : null}
                  <input
                    ref={this.addRiskInput}
                    onKeyDown={event => this.onKeyDown(event)}
                    id="addTaskInput"
                    style={{
                      border: this.state.TaskError
                        ? `1px solid ${theme.palette.border.redBorder}`
                        : null,
                    }}
                    autoFocus={this.state.focus}
                    onBlur={this.cancelAddRisk}
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.value}
                  />
                  <Grid item classes={{ item: classes.addTaskInputClearCnt }}>
                    <span className={classes.addTaskInputClearText}>
                      <FormattedMessage
                        id="common.press.message"
                        defaultMessage="Press Enter to Add"
                      />
                    </span>
                    <IconButton btnType="condensed" onClick={this.cancelAddRisk}>
                      <CancelIcon fontSize="default" htmlColor={theme.palette.secondary.light} />
                    </IconButton>
                  </Grid>
                </Fragment>
              ) : (
                <Grid container direction="row" justify="space-between" alignItems="center">
                  <Grid item>
                    <Grid container direction="row" justify="flex-start" alignItems="center">
                      <AddIcon htmlColor={theme.palette.primary.light} />
                      <span className={classes.addNewTaskText}>
                        <FormattedMessage id="risk.add-new.label" defaultMessage="Add New Risk" />
                      </span>
                    </Grid>
                  </Grid>
                  <Grid item>
                    <p className={classes.shortcutText}>
                      {" "}
                      {`${this.props.intl.formatMessage({
                        id: "common.press.label",
                        defaultMessage: "Press",
                      })} Alt + R`}
                    </p>
                  </Grid>
                </Grid>
              )}
            </AddRisk>
          ) : null}
        </div>

        <UpdateMatrixDialog
          saveOption={(param1, param2) =>
            this.customFieldChange(param1, param2, this.state.selectedRisk)
          }
          feature={"risk"}
          selectedMatrix={this.state.selectedMatrix}
          open={this.state.assessmentDialogOpen}
          closeAction={e => this.handleAddAssessmentDialog(false, e, {}, null)}
          selectedValues={
            this.state.assessmentMatrixValues
              ? this.state.assessmentMatrixValues.fieldData.data
              : []
          }
          assessMatrix={false}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    workspacePermissionsState: state.workspacePermissions,
    appliedFiltersState: state.appliedFilters,
    profileState: state.profile,
    itemOrderState: state.itemOrder,
    tasks: state.tasks.data,
    workspaces: state.profile.data.workspace,
    workspaceRiskPer: state.workspacePermissions.data.risk,
    customFields: state.customFields,
    riskColumns: state.columns.risk,
    projects: state.projects.data,
    permissionObject: ProjectPermissionSelector(state),
    dialogsState: state.dialogStates,
  };
};

export default compose(
  withRouter,
  injectIntl,
  withStyles(listStyles, { withTheme: true }),
  connect(mapStateToProps, {
    UpdateTask,
    UpdateCalenderTask,
    SaveItemOrder,
    FetchTasksInfo,
    CopyTask,
    DeleteTask,
    ArchiveTask,
    UnArchiveTask,
    BulkUpdateRisk,
    SaveRisk,
    UpdateRisk,
    DeleteRisk,
    ArchiveRisk,
    UnarchiveRisk,
    StoreCopyRiskInfo,
    MarkRiskAsStarred,
    updateRisk,
    dispatchRisk,
    riskDetailDialog,
  })
)(RiskList);
