import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";

import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button/";
import listStyles from "./styles";
import IconMenu from "../../../components/Menu/TaskMenus/IconMenu";
import MenuList from "../../../components/Menu/TaskMenus/MemberListMenu";
import helper from "../../../helper";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import DeleteConfirmDialog from "../../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import ColorPicker from "../../../components/Dropdown/ColorPicker/ColorPicker";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Popper from "@material-ui/core/Popper";
import Grow from "@material-ui/core/Grow";
import { FormattedMessage, injectIntl } from "react-intl";
import Typography from "@material-ui/core/Typography";
import { getCustomFields } from "../../../helper/customFieldsData";

class BulkActions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDateOpen: false,
      dueDateOpen: false,
      inputType: "",
      showConfirmation: false,
      deleteBtnQuery: "",
      unarchiveBtnQuery: "",
      statusBtnQuery: "",
      anchorEl: null,
      open: false,
      selectedColor: "#D9E3F0",
    };

    this.handleStartDateClose = this.handleStartDateClose.bind(this);
    this.handleStartDateToggle = this.handleStartDateToggle.bind(this);
    this.handleDueDateClose = this.handleDueDateClose.bind(this);
    this.handleDueDateToggle = this.handleDueDateToggle.bind(this);
    this.handleBulkClick = this.handleBulkClick.bind(this);
    this.showConfirmationPopup = this.showConfirmationPopup.bind(this);
    this.closeConfirmationPopup = this.closeConfirmationPopup.bind(this);
  }
  showConfirmationPopup(inputType) {
    this.setState({
      showConfirmation: true,
      inputType: inputType || "",
    });
  }

  closeConfirmationPopup() {
    this.setState({ showConfirmation: false });
  }

  handleBulkClick(inputType) {
    const { selectedRiskObj, workspaceRiskPer, permissionObject } = this.props;

    let filterRiskHaveDeletePer = selectedRiskObj.reduce((res, cv) => { /** filter those risks which user have the permission to delete */
      if (cv.projectId) {
        if (permissionObject[cv.projectId].risk.delete.cando) res.push(cv);
      } else if (workspaceRiskPer.delete.cando) {
        res.push(cv);
      }
      return res;
    }, []);

    //Generating array of riskIds
    let riskIdsArr = filterRiskHaveDeletePer.map((r) => {
      return r.row.id;
    });

    this.setState(
      {
        inputType: "",
        deleteBtnQuery:
          inputType === "Delete" ? "progress" : this.state.deleteBtnQuery,
      },
      () => {
        const type = helper.RETURN_BULKACTIONTYPES(inputType);
        const data = {
          type,
          riskIds: riskIdsArr,
        };
        this.props.BulkUpdate(data, () => {
          if (inputType === "Delete") this.setState({ deleteBtnQuery: "" });
        });
      }
    );
  }

  handleStartDateClose(
    completeDateFormat,
    completePlannedDate,
    selectedIdsList
  ) {
    const type = helper.RETURN_BULKACTIONTYPES("StartDate");
    let data = {
      type,
      riskIds: selectedIdsList,
      planDate: completePlannedDate,
      actualDate: completeDateFormat,
    };

    this.setState({ startDateOpen: false });
  }
  handleStartDateToggle() {
    this.setState((state) => ({ startDateOpen: !state.startDateOpen }));
  }
  handleDueDateClose(completeDateFormat, completePlannedDate, selectedIdsList) {
    const type = helper.RETURN_BULKACTIONTYPES("DueDate");
    let data = {
      type,
      riskIds: selectedIdsList,
      planDate: completePlannedDate,
      actualDate: completeDateFormat,
    };
    this.props.BulkUpdate(data);
    this.setState({ dueDateOpen: false });
  }
  handleDueDateToggle() {
    this.setState((state) => ({ dueDateOpen: !state.dueDateOpen }));
  }
  handleArchive = (e) => {
    if (e) e.stopPropagation();
    let inputType = this.state.inputType;
    const type = helper.RETURN_BULKACTIONTYPES(inputType);
    const data = {
      type,
      riskIds: this.props.selectedIdsList,
    };
    this.setState({ archiveBtnQuery: "progress" }, () => {
      this.props.BulkUpdate(data, () => {
        this.setState({
          archiveBtnQuery: "",
          inputType: "",
          showConfirmation: false,
        });
      });
    });
  };
  handleUnArchive = (e) => {
    if (e) e.stopPropagation();
    let inputType = this.state.inputType;
    const type = helper.RETURN_BULKACTIONTYPES(inputType);
    const data = {
      type,
      riskIds: this.props.selectedIdsList,
    };
    this.setState({ unarchiveBtnQuery: "progress" }, () => {
      this.props.BulkUpdate(data, () => {
        this.setState({
          unarchiveBtnQuery: "",
          inputType: "",
          showConfirmation: false,
        });
      });
    });
  };

  handleColorPickerBtnClick = (event) => {
    const { currentTarget } = event;
    this.setState((state) => ({
      anchorEl: currentTarget,
      open: !state.open,
    }));
  };

  handleClickAway = () => {
    this.setState({ open: false });
  };

  handleColorChange = (color) => {
    const { selectedIdsList } = this.props;
    this.setState({ selectedColor: color }, () => {
      const type = helper.RETURN_BULKACTIONTYPES("Color");
      let data = {
        type,
      };
      data["colorCode"] = this.state.selectedColor;
      data.riskIds = selectedIdsList;
      this.props.BulkUpdate(data, () => {});
    });
  };
  getTranslatedId(value) {
    switch (value) {
      case "Color":
        value = "common.action.color.label";
        break;
      case "Select Risk Owner":
        value = "common.action.risk.label";
        break;
      case "Select Task":
        value = "common.action.risk.label1";
        break;
      case "Set Status":
        value = "common.action.risk.label2";
        break;
      case "Set Impact":
        value = "common.action.risk.label3";
        break;
      case "Select Likelihood":
        value = "risk.common.likelihood.placeholder";
        break;
      case "Delete":
        value = "common.action.delete.confirmation.delete-button.label";
        break;
      case "Cancel":
        value = "common.action.cancel.label";
        break;
      case "Archive":
        value = "common.action.archive.confirmation.archive-button.label";
        break;
    }
    return value;
  }

  render() {
    const {
      classes,
      theme,
      selectedIdsList,
      isArchivedFilter,
      workspaceRiskPer,
      selectedRiskObj,
      intl,
      profileState,
      customFields,
      permissionObject,
    } = this.props;
    const {
      dueDateOpen,
      startDateOpen,
      inputType,
      showConfirmation,
      deleteBtnQuery,
      archiveBtnQuery,
      unarchiveBtnQuery,
      anchorEl,
      open,
      selectedColor,
    } = this.state;
    const id = open ? "simple-popper" : null;
    const styles = (action, i, arr) => {
      return i == 0
        ? { borderRadius: "4px 0 0 4px" }
        : i == arr.length - 1
        ? { borderRadius: "0 4px 4px 0", borderLeft: "none" }
        : { borderLeft: "none", borderRadius: 0 };
    };

    let BulkActionsType = [];
    if (isArchivedFilter) {
      BulkActionsType = [
        selectedIdsList.length + " "+intl.formatMessage({id:"common.item-selected.label",defaultMessage:"items Selected"}),
        "Unarchive",
        "Delete",
        //  "More Actions"
      ];
    } else {
      BulkActionsType = [
        selectedIdsList.length + " "+intl.formatMessage({id:"common.item-selected.label",defaultMessage:"items Selected"}),
        "Color",
        "Select Risk Owner",
        "Select Task",
        "Set Status",
        "Set Impact",
        "Set Likelihood",
        "Archive",
        "Delete",
      ];
    }
    let filterRiskHaveNotDeletePer = selectedRiskObj.reduce((res, cv) => { /** filter those risks which user have the permission to delete */
      if (cv.projectId) {
        if (!permissionObject[cv.projectId].risk.delete.cando) res.push(cv);
      } else if (!workspaceRiskPer.delete.cando) {
        res.push(cv);
      }
      return res;
    }, []);

    let customFieldsArr = getCustomFields(
      customFields,
      profileState.data,
      "risk",
    ).filter(f => f.fieldType == 'dropdown');
    const impactDropdown = customFieldsArr.find(d => d.fieldName === "Impact");
    const likeDropdown = customFieldsArr.find(d => d.fieldName === "Likelihood");
    const taskDropdown = customFieldsArr.find(d => d.fieldName === "Task");

    return (
      <div className={classes.BulkActionsCnt}>
        {inputType === "Archive" ? (
          <ActionConfirmation
            open={showConfirmation && workspaceRiskPer.archive.cando}
            closeAction={this.closeConfirmationPopup}
            cancelBtnText={
              <FormattedMessage
                id="common.action.cancel.label"
                defaultMessage="Cancel"
              />
            }
            successBtnText={
              <FormattedMessage
                id="common.archived.risk.label2"
                defaultMessage="Archive risks"
              />
            }
            alignment="center"
            headingText={
              <FormattedMessage
                id="common.action.archive.confirmation.archive-button.label"
                defaultMessage="Archive"
              />
            }
            iconType="archive"
            msgText={
              <FormattedMessage
                id="common.archived.risk.messagea"
                defaultMessage={`Are you sure you want to archive these ${selectedIdsList.length} risks?`}
                values={{ r: selectedIdsList.length }}
              />
            }
            successAction={this.handleArchive}
            btnQuery={archiveBtnQuery}
          />
        ) : inputType === "Unarchive" ? (
          <ActionConfirmation
            open={showConfirmation && workspaceRiskPer.unarchives.cando}
            closeAction={this.closeConfirmationPopup}
            cancelBtnText={
              <FormattedMessage
                id="common.action.cancel.label"
                defaultMessage="Cancel"
              />
            }
            successBtnText={
              <FormattedMessage
                id="common.un-archived.risk.label"
                defaultMessage="Unarchive risks"
              />
            }
            alignment="center"
            iconType="unarchive"
            headingText={
              <FormattedMessage
                id="common.action.un-archive.confirmation.title"
                defaultMessage="Unarchive"
              />
            }
            msgText={
              <FormattedMessage
                id="common.un-archived.risk.messagea"
                defaultMessage={`Are you sure you want to unarchive these ${selectedIdsList.length} risks?`}
                values={{ r: selectedIdsList.length }}
              />
            }
            successAction={this.handleUnArchive}
            btnQuery={unarchiveBtnQuery}
          />
        ) : inputType === "Delete" ? (
          <DeleteConfirmDialog
            open={showConfirmation && workspaceRiskPer.delete.cando}
            closeAction={this.closeConfirmationPopup}
            cancelBtnText={
              <FormattedMessage
                id="common.action.cancel.label"
                defaultMessage="Cancel"
              />
            }
            successBtnText={
              <FormattedMessage
                id="common.action.delete.confirmation.delete-button.label"
                defaultMessage="Delete"
              />
            }
            alignment="center"
            headingText={
              <FormattedMessage
                id="common.action.delete.confirmation.delete-button.label"
                defaultMessage="Delete"
              />
            }
            successAction={(e) => this.handleBulkClick(inputType)}
            msgText={
              <FormattedMessage
                id="common.action.delete.risk.message"
                defaultMessage={`Are you sure you want to delete these ${selectedIdsList.length} risks?`}
                values={{ r: selectedIdsList.length }}
              />
            }
            btnQuery={deleteBtnQuery}
            disabled={
              selectedIdsList.length - filterRiskHaveNotDeletePer.length == 0
            }
          >
            <>
              {filterRiskHaveNotDeletePer.length > 0 && (
                <Typography
                  variant="h5"
                  style={{
                    userSelect: "none",
                    marginBottom: 20,
                    // textAlign: "center",
                  }}
                >
                  {`Note :  You cannot perform this action because some of the risks that you are trying to delete are associated with those tasks which are linked to a project on which you do not have deletion rights. If you continue, only those risks will be deleted which are not associated with any task.`}
                </Typography>
              )}
              <Typography
                variant="h5"
                style={{ userSelect: "none", width: "100%" }}
              >
                <FormattedMessage id="common.action.delete.risk.message" values={{r: selectedIdsList.length -
                  filterRiskHaveNotDeletePer.length}} defaultMessage={`Are you sure you want to delete these ${selectedIdsList.length -
                  filterRiskHaveNotDeletePer.length} risks?`} />
              </Typography>
            </>
          </DeleteConfirmDialog>
        ) : null}

        {BulkActionsType.map(
          (action, i, arr) => {
            return i == 1 && !isArchivedFilter ? (
              <>
                <Button
                  disabled={i == 0 || !workspaceRiskPer.color.cando ? true : false}
                  style={styles(action, i, arr)}
                  variant="outlined"
                  onClick={this.handleColorPickerBtnClick}
                  classes={{ outlined: classes.BulkActionBtn }}
                  selectedIdsList={selectedIdsList}
                >
                  <FormattedMessage
                    id={this.getTranslatedId(action)}
                    defaultMessage={action}
                  />
                </Button>
                <Popper
                  id={id}
                  open={open}
                  anchorEl={anchorEl}
                  disablePortal
                  transition
                >
                  {({ TransitionProps, placement }) => (
                    <Grow
                      {...TransitionProps}
                      id="menu-list-grow"
                      style={{
                        transformOrigin:
                          placement === "bottom" ? "center top" : "left bottom",
                      }}
                    >
                      <ClickAwayListener onClickAway={this.handleClickAway}>
                        <div id="colorPickerCnt">
                          <ColorPicker
                            triangle="hide"
                            onColorChange={(color) => {
                              this.handleColorChange(color);
                            }}
                            selectedColor={selectedColor}
                          />
                        </div>
                      </ClickAwayListener>
                    </Grow>
                  )}
                </Popper>
              </>
            ) : i == 2 &&
              !isArchivedFilter &&
              workspaceRiskPer.riskDetails.riskOwner.cando ? (
              // <Button
              //   disabled={i == 0 ? true : false}
              //   style={styles(action, i, arr)}
              //   variant="outlined"
              //   classes={{ outlined: classes.BulkActionBtn }}
              //   selectedIdsList={this.state.selectedIds}
              // >
              <MenuList
                assigneeList={[]}
                placementType="bottom-end"
                selectedIdsList={this.props.selectedIdsList}
                BulkUpdate={this.props.BulkUpdate}
                viewType="risk"
                buttonStyles={styles(action, i, arr)}
                isRiskOwner={true}
                listType="assignee"
                buttonText={action}
                isBulk={true}
                keyType="AddOwner"
              />
            ) : //  </Button>
            i == 3 &&
              !isArchivedFilter &&
              workspaceRiskPer.riskDetails.riskTask.isAllowEdit && taskDropdown ? (
              <MenuList
                linkedTasks={[]}
                placementType="bottom-end"
                buttonStyles={styles(action, i, arr)}
                viewType="risk"
                isTask={true}
                buttonText={action}
                isBulk={true}
                checkedType="multi"
                keyType="AddTask"
                selectedIdsList={this.props.selectedIdsList}
                BulkUpdate={this.props.BulkUpdate}
              />
            ) : i == 4 &&
              !isArchivedFilter &&
              workspaceRiskPer.riskDetails.editRiskStatus.isAllowEdit ? (
              <IconMenu
                menuType="riskStatus"
                heading={this.props.intl.formatMessage({
                  id: this.getTranslatedId(action),
                  defaultMessage: action,
                })}
                keyType="Status"
                isBulk={true}
                buttonText={this.props.intl.formatMessage({
                  id: this.getTranslatedId(action),
                  defaultMessage: action,
                })}
                buttonStyles={styles(action, i, arr)}
                selectedIdsList={this.props.selectedIdsList}
                BulkUpdate={this.props.BulkUpdate}
                viewType="risk"
                selectedIdsList={this.props.selectedIdsList}
              />
            ) : i == 5 &&
              !isArchivedFilter &&
              workspaceRiskPer.riskDetails.editRiskImpact.isAllowEdit && impactDropdown ? (
              <IconMenu
                menuType="impact"
                heading={this.props.intl.formatMessage({
                  id: this.getTranslatedId(action),
                  defaultMessage: action,
                })}
                isBulk={true}
                keyType="Impact"
                buttonText={this.props.intl.formatMessage({
                  id: this.getTranslatedId(action),
                  defaultMessage: action,
                })}
                buttonStyles={styles(action, i, arr)}
                selectedIdsList={this.props.selectedIdsList}
                BulkUpdate={this.props.BulkUpdate}
                viewType="risk"
                selectedIdsList={this.props.selectedIdsList}
              />
            ) : i == 6 &&
              !isArchivedFilter &&
              workspaceRiskPer.riskDetails.editlikelihood.isAllowEdit && likeDropdown ? (
              <MenuList
                menuType="likelihood"
                heading={this.props.intl.formatMessage({
                  id: this.getTranslatedId(action),
                  defaultMessage: action,
                })}
                placementType="bottom-end"
                BulkUpdate={this.props.BulkUpdate}
                viewType="risk"
                isLikelihood={true}
                buttonText={this.props.intl.formatMessage({
                  id: this.getTranslatedId(action),
                  defaultMessage: action,
                })}
                buttonStyles={styles(action, i, arr)}
                isBulk={true}
                keyType="Likelihood"
                selectedIdsList={this.props.selectedIdsList}
              />
            ) : (
              <Button
                disabled={i == 0 ? true : false}
                style={styles(action, i, arr)}
                variant="outlined"
                classes={{ outlined: classes.BulkActionBtn }}
                onClick={this.showConfirmationPopup.bind(this, action)}
              >
                <FormattedMessage
                  id={this.getTranslatedId(action)}
                  defaultMessage={action}
                />
              </Button>
            );
          }
          // ) : i == 1 ? (

          // ) : i == 3 ? (
          //   <Fragment>
          //     <Button
          //       disabled={i == 0 ? true : false}
          //       style={styles(action, i, arr)}
          //       variant="outlined"
          //       classes={{ outlined: classes.BulkActionBtn }}
          //       onClick={this.handleStartDateToggle}
          //       buttonRef={node => {
          //         this.startDateButton = node;
          //       }}
          //     >
          //       {action}
          //     </Button>
          //   </Fragment>
          // ) : i == 4 ? (

          // ) : (
          //   <Button
          //     disabled={i == 0 ? true : false}
          //     style={styles(action, i, arr)}
          //     variant="outlined"
          //     classes={{ outlined: classes.BulkActionBtn }}
          //   >
          //     {action}
          //   </Button>
          // );
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profileState: state.profile,
    customFields: state.customFields,
  };
};

export default compose(
  injectIntl,
  connect(mapStateToProps, {
  }),
  withStyles(listStyles, { withTheme: true })
)(BulkActions);
