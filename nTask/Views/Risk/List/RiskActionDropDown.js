import React, { Component, Fragment } from "react";
import { withStyles, withTheme } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import itemStyles from "./styles";
import menuStyles from "../../../assets/jss/components/menu";
import CustomIconButton from "../../../components/Buttons/IconButton";
import SelectionMenu from "../../../components/Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import combineStyles from "../../../utils/mergeStyles";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import RightArrow from "@material-ui/icons/ArrowRight";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import DeleteConfirmDialog from "../../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import { getCompleteRisksPermissionsWithArchieve } from "../permissions";
import { GetPermission } from "../../../components/permissions";
import ColorPicker from "../../../components/Dropdown/ColorPicker/ColorPicker";
import { FormattedMessage } from "react-intl";
import { ArchiveRisk, UnarchiveRisk } from "../../../redux/actions/risks";

class RiskActionDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      pickerOpen: false,
      pickerPlacement: "",
      archiveFlag: false,
      unArchiveFlag: false,
      deleteFlag: false,
      deleteBtnQuery: "",
      archiveBtnQuery: "",
      unarchiveBtnQuery: "",
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleColorClick = this.handleColorClick.bind(this);
    this.handlePickerClose = this.handlePickerClose.bind(this);
    this.handleDialogClose = this.handleDialogClose.bind(this);
  }

  componentDidMount = () => {
    this.setState({
      selectedColor: this.props.selectedColor,
    });
  };

  handleOperations = (event, value) => {
    event.stopPropagation();
    //
    switch (value) {
      case "Copy":
        this.props.CopyTask(this.props.risk);
        break;
      case "Public Link":
        break;
      case "Archive":
        this.setState({ archiveFlag: true });
        break;
      case "Unarchive":
        this.setState({ unArchiveFlag: true });
        break;
      case "Delete":
        this.setState({ deleteFlag: true });
        break;
    }

    this.setState({ open: false, pickerOpen: false });
  };
  getTranslatedId(value) {
    switch (value) {
      case "Copy":
        value = "common.action.copy.label";
        break;
      case "Public Link":
        break;
      case "Archive":
        value = "common.action.archive.label";
        break;
      case "Unarchive":
        value = "common.action.un-archive.label";
        break;
      case "Delete":
        value = "common.action.delete.label";
        break;
    }
    return value;
  }
  handleClose(event) {
    this.setState({ open: false, pickerOpen: false });
  }
  handlePickerClose(event) {
    event.stopPropagation();
    this.setState({ pickerOpen: false });
  }
  handleDialogClose(isChanged) {
    this.setState(
      {
        archiveFlag: false,
        unArchiveFlag: false,
        deleteFlag: false,
      },
      () => {
        // if (isChanged) this.props.closeActionMenu();
      }
    );
  }
  deleteRisk = () => {
    const { risk } = this.props;
    this.setState({ deleteBtnQuery: "progress" });
    this.props.DeleteRisk(
      risk.id,
      success => {
        this.setState({ deleteBtnQuery: "", deleteFlag: false });
        this.props.filterRiskDeleteTask(risk.id);
      },
      error => {
        this.setState({ deleteBtnQuery: "", deleteFlag: false });
      }
    );
  };

  handleClick(event, placement) {
    event.stopPropagation();
    const { currentTarget } = event;
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }
  handleColorClick(event) {
    event.stopPropagation();
    if (event.target.closest("#colorPickerCnt") == null) {
      this.setState(prevState => ({ pickerOpen: !prevState.pickerOpen }));
    }
  }

  colorChange = (color, task) => {
    if (color) {
      this.setState({ selectedColor: color }, function () {
        let obj = Object.assign({}, task);
        this.setState({
          selectedColor: color,
        });
        obj.colorCode = color;
        this.props.isUpdated(obj, () => { });
      });
    }
  };

  handleArchive = e => {
    if (e) e.stopPropagation();
    const { id } = this.props.risk || "";
    const obj = { id, archive: true };
    this.setState({ archiveBtnQuery: "progress" }, () => {
      if (obj.archive) {
        this.props.ArchiveRisk(
          obj,
          succ => {
            this.setState({ archiveBtnQuery: "", archiveFlag: false }, () => {
              if (this.props.closeActionMenu) this.props.closeActionMenu();
              this.props.filterRisk(id);
            });
          },
          error => {
            if (error && error.status === 500) {
              this.props.handleExportType("Server throws error", "error");
            }
          }
        );
      }
    });
  };

  handleUnArchive = e => {
    if (e) e.stopPropagation();
    const { id } = this.props.risk || "";
    const obj = { id, unarchive: true };
    this.setState({ unarchiveBtnQuery: "progress" }, () => {
      this.props.isUpdated(obj, () => {
        this.setState({ unarchiveBtnQuery: "", unArchiveFlag: false }, () => {
          if (this.props.closeActionMenu) this.props.closeActionMenu();
          this.props.filterRisk(id);
        });
      });
    });
  };

  render() {
    const {
      classes,
      theme,
      risk,
      isArchivedSelected,
      riskPer,
      btnStyles = {},
      iconStyles = {},
    } = this.props;
    const {
      open,
      placement,
      pickerOpen,
      archiveFlag,
      unArchiveFlag,
      deleteFlag,
      selectedColor,
      deleteBtnQuery,
      archiveBtnQuery,
      unarchiveBtnQuery,
    } = this.state;
    const ddData = [];
    if (!isArchivedSelected && riskPer.color.cando) {
      ddData.push("Color");
    }

    if (riskPer.archive.cando && !isArchivedSelected) {
      ddData.push("Archive");
    }
    if (riskPer.unarchives.cando && isArchivedSelected) {
      ddData.push("Unarchive");
    }
    if (riskPer.delete.cando) {
      ddData.push("Delete");
    }

    return (
      <Fragment>
        <ClickAwayListener onClickAway={this.handleClose}>
          <div>
            <CustomIconButton
              btnType="condensed"
              onClick={event => {
                this.handleClick(event, "bottom-end");
              }}
              buttonRef={node => {
                this.anchorEl = node;
              }}
              style={btnStyles}>
              <MoreVerticalIcon
                htmlColor={theme.palette.secondary.medDark}
                style={{ fontSize: "24px", ...iconStyles }}
              />
            </CustomIconButton>

            <SelectionMenu
              open={open}
              closeAction={this.handleClose}
              placement={placement}
              anchorRef={this.anchorEl}
              disablePortal={true}
              list={
                <List>
                  <ListItem disableRipple={true} classes={{ root: classes.menuHeadingItem }}>
                    <ListItemText
                      primary={
                        <FormattedMessage id="common.action.label" defaultMessage="Select Action" />
                      }
                      classes={{ primary: classes.menuHeadingListItemText }}
                    />
                  </ListItem>
                  {ddData.map(value =>
                    value == "Color" ? (
                      <ListItem
                        key={value}
                        button
                        disableRipple={true}
                        classes={{
                          root: classes.selectColorMenuItem,
                          selected: classes.statusMenuItemSelected,
                        }}
                        onClick={event => {
                          this.handleColorClick(event);
                        }}>
                        <ListItemText
                          primary={
                            <FormattedMessage id="common.action.color.label" defaultMessage={value} />
                          }
                          classes={{
                            primary: classes.statusItemText,
                          }}
                        />
                        <RightArrow
                          htmlColor={theme.palette.secondary.dark}
                          classes={{ root: classes.submenuArrowBtn }}
                        />
                        <div
                          id="colorPickerCnt"
                          className={classes.colorPickerCntLeft}
                          style={pickerOpen ? { display: "block" } : { display: "none" }}>
                          <ColorPicker
                            triangle="hide"
                            onColorChange={color => {
                              this.colorChange(color, this.props.risk);
                            }}
                            selectedColor={selectedColor}
                          />
                        </div>
                      </ListItem>
                    ) : (
                      <ListItem
                        key={value}
                        button
                        disableRipple={true}
                        classes={{ selected: classes.statusMenuItemSelected }}
                        onClick={event => this.handleOperations(event, value)}>
                        <ListItemText
                          primary={
                            <FormattedMessage id={this.getTranslatedId(value)} defaultMessage={value} />
                          }
                          classes={{
                            primary: classes.statusItemText,
                          }}
                        />
                      </ListItem>
                    )
                  )}
                </List>
              }
            />
          </div>
        </ClickAwayListener>
        <ActionConfirmation
          open={archiveFlag}
          closeAction={this.handleDialogClose}
          cancelBtnText={
            <FormattedMessage
              id="common.action.archive.confirmation.cancel-button.label"
              defaultMessage="Cancel"
            />
          }
          successBtnText={
            <FormattedMessage
              id="common.action.archive.confirmation.archive-button.label"
              defaultMessage="Archive"
            />
          }
          alignment="center"
          headingText={
            <FormattedMessage
              id="common.action.archive.confirmation.title"
              defaultMessage="Archive"
            />
          }
          iconType="archive"
          msgText={
            <FormattedMessage
              id="common.action.archive.confirmation.risk.label"
              defaultMessage="Are you sure you want to archive this risk?"
            />
          }
          successAction={this.handleArchive}
          btnQuery={archiveBtnQuery}
        />

        <ActionConfirmation
          open={unArchiveFlag}
          closeAction={this.handleDialogClose}
          cancelBtnText={
            <FormattedMessage
              id="common.action.un-archive.confirmation.cancel-button.label"
              defaultMessage="Cancel"
            />
          }
          successBtnText={
            <FormattedMessage
              id="common.action.un-archive.confirmation.un-archive-button.label"
              defaultMessage="Unarchive"
            />
          }
          alignment="center"
          iconType="unarchive"
          headingText={
            <FormattedMessage
              id="common.action.un-archive.confirmation.title"
              defaultMessage="Unarchive"
            />
          }
          msgText={
            <FormattedMessage
              id="common.action.un-archive.confirmation.risk.label"
              defaultMessage="Are you sure you want to unarchive this risk?"
            />
          }
          successAction={this.handleUnArchive}
          btnQuery={unarchiveBtnQuery}
        />

        <DeleteConfirmDialog
          open={deleteFlag}
          closeAction={() => this.handleDialogClose(true)}
          cancelBtnText={
            <FormattedMessage
              id="common.action.delete.confirmation.cancel-button.label"
              defaultMessage="Cancel"
            />
          }
          successBtnText={
            <FormattedMessage
              id="common.action.delete.confirmation.delete-button.label"
              defaultMessage="Delete"
            />
          }
          alignment="center"
          headingText={
            <FormattedMessage
              id="common.action.delete.confirmation.title"
              defaultMessage="Delete"
            />
          }
          successAction={this.deleteRisk}
          msgText={
            <FormattedMessage
              id="common.action.delete.confirmation.risk.label"
              defaultMessage="Are you sure you want to delete this risk?"
            />
          }
          btnQuery={deleteBtnQuery}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

export default compose(
  connect(mapStateToProps, {
    ArchiveRisk,
    UnarchiveRisk,
  }),
  withStyles(combineStyles(itemStyles, menuStyles), {
    withTheme: true,
  })
)(RiskActionDropdown);
