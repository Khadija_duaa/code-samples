import React from "react"
import getRiskByStatusType from "../../../helper/getRiskByStatusType";
import getRiskByImpactType from "../../../helper/getRiskByImpactType";
import getRiskByLikelihoodType from "../../../helper/getRiskByLikelihoodType";
import { getTaskProjectName } from "../../../helper/getTaskProjectName";
import Typography from "@material-ui/core/Typography";

export function GroupingRowRenderer(param, risks, classes) { // Grouping row renderer

  const { name, columnGroupName } = param;
  let riskCount;
  let value;
  switch (columnGroupName) { // Switch statement used to calculate countes of the risks according to the grouping selected
    case ('impact'):
      riskCount = getRiskByImpactType(risks, name) //Function to return risks based on priority
      value = name
      break;
    case ('status'): {
      riskCount = getRiskByStatusType(risks, name) //Function to return risks based on task status
      value = name
    }
      break;
    case ('likelihood'): {
      riskCount = getRiskByLikelihoodType(risks, name) //Function to return risks based on task project
      value = name ? `${name} %` : ""
    }
    default:
      break;
  }
  return <div className={classes.groupingRow} {...param.renderer}>
    <div className={classes.groupingRowTextCnt}>
      <Typography variant="body1" className={classes.groupingRowText}>{value && value !== "null" ? value : "Others"}</Typography>
      <span className={classes.groupingCount}>{riskCount && riskCount > 999 ? "999+" : riskCount ? riskCount.length : null}</span>
    </div>
  </div>
}
