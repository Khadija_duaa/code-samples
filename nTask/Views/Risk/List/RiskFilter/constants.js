export const dateFilterOptions = [
  {
    value: "today",
    label: "Today",
  },
  {
    value: "yesterday",
    label: "Yesterday",
  },
  {
    value: "currentWeek",
    label: "This Week",
  },
  {
    value: "nextWeek",
    label: "Next Week",
  },
  {
    value: "currentMonth",
    label: "This Month",
  },
  {
    value: "nextMonth",
    label: "Next Month",
  },
  {
    value: "custom",
    label: "Custom",
  },
];
export const textFilterOptions = [
  {
    value: "contains",
    label: "Contains",
  },
  {
    value: "notContains",
    label: "Not contains",
  },
  {
    value: "equals",
    label: "Equals",
  },
  {
    value: "notEquals",
    label: "Not equals",
  },
  {
    value: "startsWith",
    label: "Starts with",
  },
  {
    value: "endsWith",
    label: "Ends with",
  },
];
export const numberFilterOptions = [
  {
    value: "equals",
    label: "Equals",
  },
  {
    value: "notEquals",
    label: "Not equals",
  },
  {
    value: "lessThan",
    label: "Less than",
  },
  {
    value: "lessThanOrEqual",
    label: "Less than or equals",
  },
  {
    value: "greaterThan",
    label: "Greater than",
  },
  {
    value: "greaterThanOrEqual",
    label: "Greater than or equals",
  },
  {
    value: "inRange",
    label: "In range",
  },
];
const createdDateFilterTypes = [
  {
    value: "today",
    label: "Today",
  },
  {
    value: "yesterday",
    label: "Yesterday",
  },
  {
    value: "currentWeek",
    label: "This Week",
  },
  {
    value: "currentMonth",
    label: "This Month",
  },
  {
    value: "custom",
    label: "Custom",
  },
];
export const taskDateTypes = [
  { key: "createdDate", value: "Created Date", data: createdDateFilterTypes },
  { key: "updatedDate", value: "Updated Date", data: createdDateFilterTypes },
];

export const initFilters = {
  project: { type: "", selectedValues: [] },
  riskOwner: { type: "", selectedValues: [] },
  status: { type: "", selectedValues: [] },
  impact: { type: "", selectedValues: [] },
  tasks: { type: "", selectedValues: [] },
  likelihood: { type: "", selectedValues: [] },
  createdDate: { type: "", selectedValues: [] },
  createdBy: { type: "", selectedValues: [] },
  updatedDate: { type: "", selectedValues: ['', ''] }, 
  updatedBy: { type: "", selectedValues: [] },
};
export const ntaskFields = [
  "Impact",
  "Likelihood",
  "Task",
  "Project",
  "Risk Owner",
  "Created By",
  "Status",
  "Created Date",
  "uniqueId",
]
