import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import CustomIconButton from "../../../components/Buttons/IconButton";
import StarIcon from "@material-ui/icons/StarRate";
import itemStyles from "./styles";
import { MarkRiskAsStarred } from "../../../redux/actions/risks";

class Starred extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isStarred: false,
    };
  }
  componentDidMount() {
    this.setState({
      isStarred: this.props.currentRisk ? this.props.currentRisk.isStared : false,
    });
  }

  static getDerivedStateFromProps(nextProps) {
    return {
      isStarred: nextProps.currentRisk ? nextProps.currentRisk.isStared : false,
    };
  }

  handleStarClick = (e, data) => {
    e.stopPropagation();
    if (!data.isDeleted) {
      let obj = {
        Id: data.id,
        MarkStar: !this.state.isStarred,
      };
      this.setState({ isStarred: !this.state.isStarred });
      this.props.MarkRiskAsStarred(
        obj,
        (err, data) => {},
        err => {
          if(err) this.props.handleExportType(err.data.message, "error");
        }
      );
    }
  };
  render() {
    const { classes, theme, currentRisk, userId, style, disabled=false } = this.props;
    const { isStarred } = this.state;
    return (
      <CustomIconButton
        btnType="condensed"
        id="starBtn"
        onClick={e => this.handleStarClick(e, currentRisk)}
        disabled={disabled}
        style={{
          opacity: isStarred ? 1 : "",
          color: isStarred ? "#FFC700" : "",
          ...style,
        }}>
        <StarIcon
          htmlColor={
            this.state.isStarred ? theme.palette.background.star : theme.palette.secondary.light
          }
        />
      </CustomIconButton>
    );
  }
}

export default compose(
  withRouter,
  withStyles(itemStyles, { withTheme: true }),
  connect(null, { MarkRiskAsStarred })
)(Starred);
