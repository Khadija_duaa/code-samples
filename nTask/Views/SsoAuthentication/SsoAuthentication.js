import React, { useEffect } from "react";
import { withRouter } from "react-router-dom";
import queryString from "query-string";
import { signinWithSso } from "../../redux/actions/authentication";
import { compose } from "redux";
import { connect } from "react-redux";

function SsoAuthentication(props) {
  const q = queryString;
  const { location, signinWithSso } = props;
  const { search } = location; 

  useEffect(() => {
    if (!search || search == "") {
      props.history.push('/account/sso-signin');
    } else {
      signinWithSso(
        search.substring(1),
        //success
        res => {
          localStorage.setItem("token", `Bearer ${res.data.token.access_token}`);
          props.history.push("/teams/");
        },
        //failure
        (res) => {
            props.history.push('/account/sso-signin?error=' + res.response.data.message + '&email=' + res.response.data.email)
        }
      );
    }
  }, []);
  return <div></div>;
}

export default compose(
  connect(() => {}, { signinWithSso }),
  withRouter
)(SsoAuthentication);
