import React, { Component } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { SaveItemOrder } from "../../../redux/actions/itemOrder";
import Drawer from "@material-ui/core/Drawer";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import { withStyles } from "@material-ui/core/styles";
import TypeDropdown from "./TypeDropdown";
import RangeDatePicker from "../../../components/DatePicker/RangeDatePicker";
import Grid from "@material-ui/core/Grid";
import DefaultSwitch from "../../../components/Form/Switch";
import DefaultButton from "../../../components/Buttons/DefaultButton";
import CustomButton from "../../../components/Buttons/CustomButton";
import { Scrollbars } from "react-custom-scrollbars";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import HelpIcon from "@material-ui/icons/HelpOutline";
import BackArrow from "@material-ui/icons/KeyboardArrowLeft";
import IconButton from "../../../components/Buttons/IconButton";
import ReactResizeDetector from "react-resize-detector";
import DefaultTextField from "../../../components/Form/TextField";
import SvgIcon from "@material-ui/core/SvgIcon";
import PushPin from "../../../components/Icons/PushPin";
import isEqual from "lodash/isEqual";
import cloneDeep from "lodash/cloneDeep";
import { setAppliedFilters } from "../../../redux/actions/appliedFilters";
import { validateGenericField } from "../../../utils/formValidations";
import SelectSearchDropdown from "../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { generateSelectData } from "../../../helper/generateSelectData";
import { generateAssigneeData } from "../../../helper/generateSelectData";

import RoundIcon from "@material-ui/icons/Brightness1";
import FlagIcon from "../../../components/Icons/FlagIcon";
import RingIcon from "@material-ui/icons/RadioButtonUnchecked";
import BugIcon from "../../../components/Icons/BugIcon";
import StarIcon from "../../../components/Icons/StarIcon";
import ImprovementIcon from "../../../components/Icons/ImprovementIcon";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";
import {
  calculateFilterContentHeight,
  calculateHeaderHeight,
} from "../../../utils/common";

const drawerWidth = 350;
import {
  statusData,
  priorityData,
  typeData,
} from "../../../helper/issueDropdownData";
import UnPlanned from "../../billing/UnPlanned/UnPlanned";
import { FormattedMessage, injectIntl } from "react-intl";
import {TRIALPERIOD} from '../../components/constants/planConstant';

const styles = (theme) => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: !teamCanView("advanceFilterAccess") ? 390 : drawerWidth,
    padding: "43px 0 0 0",
    background: theme.palette.common.white,
    position: "fixed",
    top: 108,
    display: "flex",
    height: "100%",
    zIndex: 100,
    flexDirection: "row",
    justifyContent: "stretch",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    overflowX: "hidden",
    borderTop: 0,
    borderBottom: 0,
  },
  drawerHeader: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    height: "100%",
  },
  filterFormCnt: {
    padding: "10px 20px 40px 20px",
  },
  switchCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    paddingLeft: 12,
  },
  switchLabel: {
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    margin: 0,
    marginRight: 30,
    fontWeight: theme.typography.fontWeightRegular,
    display: "flex",
  },
  filterBottomBtns: {
    padding: "10px 20px",
    borderTop: `1px solid ${theme.palette.border.lightBorder}`,
    marginTop: 40,
    position: "fixed",
    bottom: 130,
    background: theme.palette.common.white,
    zIndex: 1,
  },
  statusIcon: {
    fontSize: "11px !important",
    marginRight: 5,
  },

  filterItem: {},
  filterCntHeader: {
    background: "#404040",
    padding: "12px 15px",
    position: "fixed",
    top: 0,
    left: 0,
    right: 0,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  filterHeaderClearBtn: {
    textDecoration: "underline",
    color: theme.palette.common.white,
    fontSize: "12px !important",
    cursor: "pointer",
  },
  filterHeaderTextCnt: {
    display: "flex",
    alignItems: "center",
  },
  filtersHeaderText: {
    marginLeft: 10,
    color: theme.palette.common.white,
    fontWeight: theme.typography.fontWeightLight,
    fontSize: "16px !important",
  },
  filtersHeaderCloseIcon: {
    fontSize: "11px !important",
    color: "#404040",
  },
  pushPinIcon: {
    fontSize: "14px !important",
    marginRight: 5,
  },
  filtersHeaderHelpIcon: {
    fontSize: "14px !important",
    marginLeft: 5,
  },
  priorityIcon: {
    fontSize: "16px !important",
    marginRight: 5,
  },
  severityIcon: {
    fontSize: "14px !important",
    marginRight: 5,
  },
  bugIcon: {
    fontSize: "16px !important",
    marginRight: 5,
  },
  ratingStarIcon: {
    fontSize: "16px !important",
    marginRight: 5,
  },
  ImprovmentIcon: {
    fontSize: "16px !important",
    marginRight: 5,
  },
  unplannedMain: {
    padding: 15,
    display: "flex",
    alignItems: "center",
    height: "100%",
  },
});
class IssueFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterCntHeight: `${calculateFilterContentHeight()}px`,
      saveFilter: false,
      isClear: false,
      isDisable: true,
      isSaveDisable: true,
      loggedInTeam: this.props.profileState.data.loggedInTeam,
      filterId: ``,
      recurrence: false,
      label: [],
      category: [],
      assignees: [],
      createdBy: [],
      status: [],
      priority: [],
      severity: [],
      type: [],
      task: [],
      actualStart: { endDate: "", startDate: "" },
      plannedStart: { endDate: "", startDate: "" },
      plannedEnd: { endDate: "", startDate: "" },
      actualEnd: { endDate: "", startDate: "" },
      filterName: "",
      defaultFilter: false,
      showSaveFilterPlan: false,
    };
    this.initialState = this.state;

    this.handleSwitch = this.handleSwitch.bind(this);
    this.onResize = this.onResize.bind(this);
    this.handleInput = this.handleInput.bind(this);
    this.handleSaveFilter = this.handleSaveFilter.bind(this);
    this.handleBackFilter = this.handleBackFilter.bind(this);
    this.handleFilterChange = this.handleFilterChange.bind(this);
    this.handleClearState = this.handleClearState.bind(this);
    this.handleSearchFilter = this.handleSearchFilter.bind(this);
  }
  componentDidMount() {
    // For handling default issue filter
    const { Issue } = this.props.appliedFiltersState;
    if (Issue) {
      this.setState({ ...this.initialState, ...this.transformObject() });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { Issue } = this.props.appliedFiltersState;
    if (!isEqual(prevProps.appliedFiltersState.Issue, Issue)) {
      if (prevProps.appliedFiltersState.Issue && Issue === null)
        this.setState({ ...this.initialState, ...Issue });
      else {
        this.setState({
          ...this.initialState,
          isSaveDisable: false,
          ...this.transformObject(),
        });
      }
    }
  }

  transformObject = () => {
    const { theme, classes, members } = this.props;
    const { Issue, intl } = this.props.appliedFiltersState;
    let issueObj = cloneDeep(Issue);

    if (issueObj.assignees.length) {
      const assigneesData = generateAssigneeData(members);
      issueObj.assignees = assigneesData.filter((a) => {
        return issueObj.assignees.indexOf(a.id) > -1;
      });
    }

    if (issueObj.createdBy && issueObj.createdBy.length) {
      const createdByData = generateAssigneeData(members);
      issueObj.createdBy = createdByData.filter((c) => {
        return issueObj.createdBy.indexOf(c.id) > -1;
      });
    }

    if (issueObj.task.length) {
      const taskData = this.generateTaskData();
      issueObj.task = taskData.filter((t) => {
        return issueObj.task.indexOf(t.id) > -1;
      });
    }
    if (issueObj.status) {
      issueObj.status = statusData(theme, classes, intl).filter((s) => {
        return issueObj.status.indexOf(s.value) > -1;
      });
    }
    if (issueObj.priority) {
      issueObj.priority = priorityData(theme, classes, intl).filter((p) => {
        return issueObj.priority.indexOf(p.value) > -1;
      });
    }
    if (issueObj.type) {
      issueObj.type = typeData(theme, classes, intl).filter((t) => {
        return issueObj.type.indexOf(t.value) > -1;
      });
    }

    return issueObj;
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.loggedInTeam !== nextProps.profileState.data.loggedInTeam) {
      return {
        loggedInTeam: nextProps.profileState.data.loggedInTeam,
      };
    } else {
      return { loggedInTeam: prevState.loggedInTeam };
    }
  }

  handleSearchFilter() {
    const {
      filterCntHeight,
      saveFilter,
      isClear,
      isDisable,
      isSaveDisable,
      loggedInTeam,
      ...rest
    } = this.state;
    const { theme, classes } = this.props;
    let issueObj = cloneDeep(rest);

    if (issueObj.assignees.length) {
      issueObj.assignees = issueObj.assignees.map((p) => {
        return p.id;
      });
    }

    if (issueObj.createdBy.length) {
      issueObj.createdBy = issueObj.createdBy.map((c) => {
        return c.id;
      });
    }

    if (issueObj.task.length) {
      issueObj.task = issueObj.task.map((t) => {
        return t.id;
      });
    }
    if (issueObj.status && issueObj.status.length) {
      issueObj.status = issueObj.status.map((i) => {
        return i.value;
      });
    }
    if (issueObj.priority && issueObj.priority.length) {
      issueObj.priority = issueObj.priority.map((p) => {
        return p.value;
      });
    }
    if (issueObj.type && issueObj.type.length) {
      issueObj.type = issueObj.type.map((t) => {
        return t.value;
      });
    }

    this.setState(
      {
        isDisable: true,
      },
      () => {
        this.props.setAppliedFilters(issueObj, "Issue");
      }
    );
  }

  handleClearState(isCleared) {
    if (isCleared) {
    } else {
      this.setState(this.initialState, () => {
        this.props.setAppliedFilters(null, "Issue");
      });
    }
  }
  handleFilterChange(key, value) {
    this.setState(
      {
        [key]: value,
        isDisable: false,
        isSaveDisable: false,
      },
      () => {
        const {
          filterCntHeight,
          saveFilter,
          isClear,
          isDisable,
          isSaveDisable,
          loggedInTeam,
          label,
          category,
          ...rest
        } = this.state;
        const changedStateData = { ...this.initialState, ...rest };
        if (isEqual(changedStateData, this.initialState)) {
          this.handleClearState(false);
          this.setState({ isDisable: true });
        }
      }
    );
  }

  handleSaveFilter() {
    if (this.state.saveFilter) {
      let filterName = this.state.filterName;
      let validationObj = validateGenericField(
        "filter name",
        filterName,
        true,
        40
      );
      if (validationObj["isError"]) {
        this.setState({
          isSaveDisable: true,
          filterNameError: validationObj["isError"],
          filterNameErrorMessage: validationObj["message"],
        });
        return;
      }

      const {
        filterCntHeight,
        saveFilter,
        isClear,
        isDisable,
        isSaveDisable,
        ...data
      } = this.state;
      const { Issue } = this.props.appliedFiltersState;
      let itemOrder = cloneDeep(this.props.itemOrderState.data);

      let dataCopy = cloneDeep(data);
      dataCopy.assignees = this.state.assignees.map((a) => a.id);
      dataCopy.createdBy = this.state.createdBy.map((c) => c.value);
      dataCopy.status = this.state.status.map((c) => c.value);
      dataCopy.task = this.state.task.map((c) => c.id);
      dataCopy.type = this.state.type.map((t) => t.value);
      dataCopy.priority = this.state.priority.map((p) => p.value);
      itemOrder.issueFilter = {
        ...dataCopy,
        filterId: Issue && Issue["filterId"] ? Issue["filterId"] : ``,
      };
      this.setState({ saveBtnQuery: "progress" });
      this.props.SaveItemOrder(
        itemOrder,
        (resp) => {
          this.setState({ saveBtnQuery: "" });
          this.props.closeAction();
          if (!this.props.appliedFiltersState.Issue) {
            this.setState(this.initialState);
          } else {
            if (data.filterId)
              this.setState({ saveFilter: false, isSaveDisable: true });
            else {
              const filter =
                resp.data.userIssueFilter[0].issueFilter[
                  resp.data.userIssueFilter[0].issueFilter.length - 1
                ];
              this.props.setAppliedFilters(filter, "Issue");
            }
          }
        },
        (error) => {
          if (error.status === 409) {
            this.setState({ saveBtnQuery: "" });
            this.setState({ filterNameError: true });
            this.setState({
              filterNameErrorMessage: "Same filter name already exists",
            });
          } else {
            this.setState({ saveFilter: false });
          }
        }
      );
    } else if (
      teamCanView("advanceFilterAccess") &&
      !teamCanView("saveCustomFilter")
    ) {
      this.setState({ showSaveFilterPlan: true });
    } else this.setState({ saveFilter: true });
  }
  handleBackFilter() {
    this.setState({
      saveFilter: false,
      filterNameError: false,
      filterNameErrorMessage: "",
      filterName: "",
      showSaveFilterPlan: false,
    });
  }
  onResize() {
    this.setState({ filterCntHeight: `${calculateFilterContentHeight()}px` });
  }
  handleInput(event, name) {
    this.setState({
      filterName: event.target.value,
      filterNameError: false,
      filterNameErrorMessage: ``,
      isSaveDisable: false,
    });
  }
  handleSwitch(name) {
    this.setState((prevState) => ({
      [name]: !prevState[name],
      isDisable: false,
    }));
  }
  handleClickAway = () => {
    const isDatePickerDialogueOpen = document.querySelectorAll(
      'div[class^="RangeDatePicker"]'
    ).length;
    const isDropdownMenuOpen = document.getElementById("menu-") !== null;

    if (!isDropdownMenuOpen && !isDatePickerDialogueOpen)
      this.props.closeAction();
  };
  generateAssigneeData = () => {
    const members = generateAssigneeData();
    let ddData = members.map((member, i) => {
      return generateSelectData(
        member.fullName,
        member.fullName,
        member.userId,
        null,
        member
      );
    });
    return ddData;
  };

  generateTaskData = () => {
    const { tasks } = this.props;
    let ddData = tasks.map((task, i) => {
      return generateSelectData(
        task.taskTitle,
        task.taskTitle,
        task.taskId,
        task.uniqueId,
        task
      );
    });
    return ddData;
  };

  render() {
    const {
      classes,
      theme,
      open,
      closeAction,
      promoBar,
      notificationBar,
      members,intl
    } = this.props;
    const {
      filterCntHeight,
      saveFilter,
      defaultFilter,
      filterName,
      filterNameError,
      filterNameErrorMessage,
      isDisable,
      isSaveDisable,
      assignees,
      createdBy,
      actualStart,
      plannedStart,
      plannedEnd,
      actualEnd,
      task,
      status,
      priority,
      severity,
      type,
      saveBtnQuery,
      showSaveFilterPlan,
    } = this.state;
    const statusColor = theme.palette.issueStatus;
    const priorityColor = theme.palette.issuePriority;
    const severityColor = theme.palette.severity;
    const statusData = () => {
      return [
        {
          label: intl.formatMessage({id:"issue.common.status.drop-down.open", defaultMessage:"Open"}),
          value: "Open",
          icon: (
            <RoundIcon
              htmlColor={statusColor.Open}
              className={classes.statusIcon}
            />
          ),
        },
        {
          label: intl.formatMessage({id:"issue.common.status.drop-down.close", defaultMessage:"Closed"}),
          value: "Closed",
          icon: (
            <RoundIcon
              htmlColor={statusColor.Closed}
              className={classes.statusIcon}
            />
          ),
        },
      ];
    };
    const priorityData = () => {
      return [
        {
          label: intl.formatMessage({id:"issue.common.priority.dropdown.critical", defaultMessage:"Critical"}),
          value: "Critical",
          icon: (
            <SvgIcon
              viewBox="0 0 24 32.75"
              htmlColor={priorityColor.Critical}
              className={classes.priorityIcon}
            >
              <FlagIcon />
            </SvgIcon>
          ),
        },
        {
          label: intl.formatMessage({id:"issue.common.priority.dropdown.high", defaultMessage:"High"}),
          value: "High",
          icon: (
            <SvgIcon
              viewBox="0 0 24 32.75"
              htmlColor={priorityColor.High}
              className={classes.priorityIcon}
            >
              <FlagIcon />
            </SvgIcon>
          ),
        },
        {
          label: intl.formatMessage({id:"issue.common.priority.dropdown.medium", defaultMessage:"Medium"}),
          value: "Medium",
          icon: (
            <SvgIcon
              viewBox="0 0 24 32.75"
              htmlColor={priorityColor.Medium}
              className={classes.priorityIcon}
            >
              <FlagIcon />
            </SvgIcon>
          ),
        },
        {
          label: intl.formatMessage({id:"issue.common.priority.dropdown.low", defaultMessage:"Low"}),
          value: "Low",
          icon: (
            <SvgIcon
              viewBox="0 0 24 32.75"
              htmlColor={priorityColor.Low}
              className={classes.priorityIcon}
            >
              <FlagIcon />
            </SvgIcon>
          ),
        },
      ];
    };
    const severityData = () => {
      return [
        {
          label: intl.formatMessage({id:"issue.common.severity.dropdown.minor", defaultMessage:"Minor"}),
          value: "Minor",
          icon: (
            <RingIcon
              htmlColor={severityColor.minor}
              classes={{ root: classes.severityIcon }}
            />
          ),
        },
        {
          label: intl.formatMessage({id:"issue.common.severity.dropdown.moderate", defaultMessage:"Moderate"}),
          value: "Moderate",
          icon: (
            <RingIcon
              htmlColor={severityColor.moderate}
              classes={{ root: classes.severityIcon }}
            />
          ),
        },
        {
          label: intl.formatMessage({id:"issue.common.severity.dropdown.major", defaultMessage:"Major"}),
          value: "Major",
          icon: (
            <RingIcon
              htmlColor={severityColor.major}
              classes={{ root: classes.severityIcon }}
            />
          ),
        },
        {
          label: intl.formatMessage({id:"issue.common.severity.dropdown.critical", defaultMessage:"Critical"}),
          value: "Critical",
          icon: (
            <RingIcon
              htmlColor={severityColor.critical}
              classes={{ root: classes.severityIcon }}
            />
          ),
        },
      ];
    };
    const typeData = () => {
      return [
        {
          label: intl.formatMessage({id:"issue.common.issue-type.drop-down.bug", defaultMessage:"Bug"}),
          value: "Bug",
          icon: (
            <SvgIcon
              viewBox="0 0 456.828 456.828"
              htmlColor={theme.palette.error.light}
              className={classes.bugIcon}
            >
              <BugIcon />
            </SvgIcon>
          ),
        },
        {
          label: intl.formatMessage({id:"issue.common.issue-type.drop-down.feature", defaultMessage:"Feature"}),
          value: "Feature",
          icon: (
            <SvgIcon
              viewBox="0 0 11 10.188"
              classes={{ root: classes.ImprovmentIcon }}
              htmlColor={theme.palette.primary.light}
            >
              <ImprovementIcon />
            </SvgIcon>
          ),
        },
        {
          label: intl.formatMessage({id:"issue.common.issue-type.drop-down.improvement", defaultMessage:"Improvement"}),
          value: "Improvement",
          icon: (
            <SvgIcon
              viewBox="0 0 19.481 19.481"
              classes={{ root: classes.ratingStarIcon }}
              htmlColor={theme.palette.background.star}
            >
              <StarIcon />
            </SvgIcon>
          ),
        },
      ];
    };
    return (
      <ClickAwayListener onClickAway={this.handleClickAway}>
        <Drawer
          className={classes.drawer}
          variant="persistent"
          anchor="right"
          open={open}
          PaperProps={{ style: { top: calculateHeaderHeight() } }}
          classes={{
            paper: classes.drawerPaper,
          }}
        >
          <ReactResizeDetector
            handleWidth
            handleHeight
            onResize={this.onResize}
          />
          <Scrollbars autoHide style={{ width: 415, height: filterCntHeight }}>
            <div className={classes.drawerHeader}>
              <div className={classes.filterCntHeader}>
                <div className={classes.filterHeaderTextCnt}>
                  <IconButton
                    btnType="grayBg"
                    onClick={
                      saveFilter || showSaveFilterPlan
                        ? this.handleBackFilter
                        : closeAction
                    }
                    style={{ padding: 4 }}
                  >
                    {saveFilter || showSaveFilterPlan ? (
                      <BackArrow
                        classes={{ root: classes.filtersHeaderCloseIcon }}
                        htmlColor={theme.palette.secondary.medDark}
                      />
                    ) : (
                      <CloseIcon
                        classes={{ root: classes.filtersHeaderCloseIcon }}
                        htmlColor={theme.palette.secondary.medDark}
                      />
                    )}
                  </IconButton>

                  <Typography
                    variant="h3"
                    classes={{ h3: classes.filtersHeaderText }}
                  >
                    {saveFilter ? (
                      <FormattedMessage
                        id="filters.save.label"
                        defaultMessage="Save Filter"
                      />
                    ) : (
                      <FormattedMessage
                        id="filters.label"
                        defaultMessage="Filters"
                      />
                    )}
                  </Typography>
                </div>
                {!saveFilter &&
                teamCanView("advanceFilterAccess") &&
                !showSaveFilterPlan ? (
                  <span
                    className={classes.filterHeaderClearBtn}
                    onClick={this.handleClearState.bind(this, false)}
                  >
                    <FormattedMessage
                      id="common.action.Clear.label"
                      defaultMessage="Clear"
                    />
                  </span>
                ) : null}
              </div>
              {saveFilter ? (
                <Grid container classes={{ container: classes.filterFormCnt }}>
                  <Grid
                    item
                    classes={{ item: classes.filterItem }}
                    style={{ marginBottom: 0 }}
                  >
                    <DefaultTextField
                      label={
                        <FormattedMessage
                          id="filters.form.name"
                          defaultMessage="Filter Name"
                        />
                      }
                      formControlStyles={{ marginBottom: 0 }}
                      fullWidth={true}
                      errorState={filterNameError}
                      errorMessage={filterNameErrorMessage}
                      defaultProps={{
                        id: "filterName",
                        placeholder: this.props.intl.formatMessage({
                          id: "filters.form.placeholder",
                          defaultMessage: "Enter Filter Name",
                        }),
                        onChange: (event) =>
                          this.handleInput(event, "filterName"),
                        value: filterName,
                        inputProps: { maxLength: 40 },
                      }}
                    />
                  </Grid>
                  <Grid
                    item
                    classes={{ item: classes.filterItem }}
                    style={{ marginTop: 0 }}
                  >
                    <div
                      className={classes.switchCnt}
                      style={{ marginLeft: 0 }}
                    >
                      <p className={classes.switchLabel}>
                        <SvgIcon
                          viewBox="0 0 8 11.562"
                          className={classes.pushPinIcon}
                          htmlColor={theme.palette.secondary.light}
                        >
                          <PushPin />
                        </SvgIcon>
                        <FormattedMessage
                          id="filters.form.message"
                          defaultMessage="Make this filter my default view"
                        />
                        <HelpIcon
                          classes={{ root: classes.filtersHeaderHelpIcon }}
                          htmlColor={theme.palette.secondary.medDark}
                          fontSize="small"
                        />
                      </p>
                      <DefaultSwitch
                        checked={defaultFilter}
                        onChange={(event) => {
                          this.handleSwitch("defaultFilter", event);
                        }}
                      />
                    </div>
                  </Grid>
                </Grid>
              ) : !teamCanView("advanceFilterAccess") ? (
                <div className={classes.unplannedMain}>
                  <UnPlanned
                    feature="premium"
                    titleTxt={<FormattedMessage id="common.discovered-dialog.premium-title" defaultMessage="Wow! You've discovered a Premium feature!"/>}
                    boldText={this.props.intl.formatMessage({ id:"common.discovered-dialog.list.custom-filter.title" ,defaultMessage:"Custom Filters"})}
                    descriptionTxt={<FormattedMessage id="common.discovered-dialog.list.custom-filter.label" defaultMessage={"is available on our Premium Plan. Upgrade now or start a {TRIALPERIOD}-day free trial to find out all nTask Premium features."}
                    values={{TRIALPERIOD: TRIALPERIOD}}/>}
                    showBodyImg={false}
                    showDescription={true}
                  />
                </div>
              ) : showSaveFilterPlan ? (
                <div className={classes.unplannedMain}>
                  <UnPlanned
                    feature="business"
                    titleTxt={<FormattedMessage id="common.discovered-dialog.business-title" defaultMessage="Wow! You've discovered a Business feature!"/>}
                    boldText={this.props.intl.formatMessage({id:"common.discovered-dialog.list.custom-filter.save",defaultMessage:"Save Custom Filters"})}
                    descriptionTxt={<FormattedMessage id="common.discovered-dialog.list.risk.label" defaultMessage={"is available on our Business Plan. Upgrade now or start a {TRIALPERIOD}-day free trial to find out all nTask Business features."}values={{TRIALPERIOD: TRIALPERIOD}}/>}
                    showBodyImg={false}
                    showDescription={true}
                  />
                </div>
              ) : (
                <Grid container classes={{ container: classes.filterFormCnt }}>
                  <Grid item xs={12} classes={{ item: classes.filterItem }}>
                    <SelectSearchDropdown
                      data={() => generateAssigneeData(members)}
                      label={
                        <FormattedMessage
                          id="common.assigned.assignee"
                          defaultMessage="Assignee"
                        />
                      }
                      selectChange={this.handleFilterChange}
                      type="assignees"
                      selectedValue={assignees}
                      placeholder={this.props.intl.formatMessage({
                        id: "issue.creation-dialog.form.assign-to.placeholder",
                        defaultMessage: "Select Assignee",
                      })}
                      avatar={true}
                    />
                  </Grid>
                  <Grid item xs={12} classes={{ item: classes.filterItem }}>
                    <SelectSearchDropdown
                      data={() => generateAssigneeData(members)}
                      label={
                        <FormattedMessage
                          id="common.created-by.action"
                          defaultMessage="Created By"
                        />
                      }
                      selectChange={this.handleFilterChange}
                      type="createdBy"
                      selectedValue={createdBy}
                      placeholder={this.props.intl.formatMessage({
                        id: "common.action.select.label",
                        defaultMessage: "Select",
                      })}
                      avatar={true}
                    />
                  </Grid>

                  <Grid item xs={12} classes={{ item: classes.filterItem }}>
                    <SelectSearchDropdown
                      data={statusData}
                      label={this.props.intl.formatMessage({
                        id: "issue.common.status.label",
                        defaultMessage: "Select Status",
                      })}
                      selectChange={this.handleFilterChange}
                      type="status"
                      selectedValue={status}
                      placeholder={this.props.intl.formatMessage({
                        id: "issue.common.status.label",
                        defaultMessage: "Select Status",
                      })}
                      icon={true}
                    />
                  </Grid>
                  <Grid item xs={12} classes={{ item: classes.filterItem }}>
                    <SelectSearchDropdown
                      data={priorityData}
                      label={
                        <FormattedMessage
                          id="task.detail-dialog.priority.label"
                          defaultMessage="Priority"
                        />
                      }
                      selectChange={this.handleFilterChange}
                      type="priority"
                      selectedValue={priority}
                      placeholder={
                        <FormattedMessage
                          id="issue.common.priority.placeholder"
                          defaultMessage="Select Priority"
                        />
                      }
                      icon={true}
                    />
                  </Grid>
                  <Grid item xs={12} classes={{ item: classes.filterItem }}>
                    <SelectSearchDropdown
                      data={severityData}
                      label={this.props.intl.formatMessage({
                        id: "issue.detail-dialog.severity.placeholder",
                        defaultMessage: "Select Severity",
                      })}
                      selectChange={this.handleFilterChange}
                      type="severity"
                      selectedValue={severity}
                      placeholder={this.props.intl.formatMessage({
                        id: "issue.detail-dialog.severity.placeholder",
                        defaultMessage: "Select Severity",
                      })}
                      icon={true}
                    />
                  </Grid>

                  <Grid item xs={12} classes={{ item: classes.filterItem }}>
                    <SelectSearchDropdown
                      data={typeData}
                      label={this.props.intl.formatMessage({
                        id: "issue.common.issue-type.label",
                        defaultMessage: "Issue Type",
                      })}
                      selectChange={this.handleFilterChange}
                      type="type"
                      selectedValue={type}
                      placeholder={this.props.intl.formatMessage({
                        id: "common.action.select.label",
                        defaultMessage: "Select",
                      })}
                      icon={true}
                    />
                  </Grid>

                  <Grid item xs={12} classes={{ item: classes.filterItem }}>
                    <SelectSearchDropdown
                      data={this.generateTaskData}
                      label={
                        <FormattedMessage
                          id="risk.common.task.label"
                          defaultMessage="Task"
                        />
                      }
                      selectChange={this.handleFilterChange}
                      type="task"
                      selectedValue={task}
                      placeholder={this.props.intl.formatMessage({
                        id: "meeting.creation-dialog.form.task.placeholder",
                        defaultMessage: "Select Task",
                      })}
                    />
                  </Grid>

                  <Grid item xs={12} classes={{ item: classes.filterItem }}>
                    <RangeDatePicker
                      label={
                        <FormattedMessage
                          id="common.date.startActual"
                          defaultMessage="Start Date (Actual)"
                        />
                      }
                      placeholder={this.props.intl.formatMessage({
                        id: "common.action.select.label",
                        defaultMessage: "Select",
                      })}
                      handleFilterChange={this.handleFilterChange}
                      handleClearState={this.handleClearState}
                      type="actualStart"
                      error={false}
                      selectedDate={actualStart}
                    />
                  </Grid>
                  <Grid item xs={12} classes={{ item: classes.filterItem }}>
                    <RangeDatePicker
                      label={
                        <FormattedMessage
                          id="common.date.startPlanned"
                          defaultMessage="Start Date (Planned)"
                        />
                      }
                      placeholder={this.props.intl.formatMessage({
                        id: "common.action.select.label",
                        defaultMessage: "Select",
                      })}
                      handleFilterChange={this.handleFilterChange}
                      handleClearState={this.handleClearState}
                      error={false}
                      type="plannedStart"
                      selectedDate={plannedEnd}
                    />
                  </Grid>
                  <Grid item xs={12} classes={{ item: classes.filterItem }}>
                    <RangeDatePicker
                      label={
                        <FormattedMessage
                          id="common.date.dueActual"
                          defaultMessage="Due Date (Actual)"
                        />
                      }
                      placeholder={this.props.intl.formatMessage({
                        id: "common.action.select.label",
                        defaultMessage: "Select",
                      })}
                      handleFilterChange={this.handleFilterChange}
                      handleClearState={this.handleClearState}
                      type="actualEnd"
                      error={false}
                      selectedDate={actualEnd}
                    />
                  </Grid>
                  <Grid item xs={12} classes={{ item: classes.filterItem }}>
                    <RangeDatePicker
                      label={
                        <FormattedMessage
                          id="common.date.duePlanned"
                          defaultMessage="Due Date (Planned)"
                        />
                      }
                      placeholder={this.props.intl.formatMessage({
                        id: "common.action.select.label",
                        defaultMessage: "Select",
                      })}
                      handleFilterChange={this.handleFilterChange}
                      handleClearState={this.handleClearState}
                      type="plannedEnd"
                      error={false}
                      selectedDate={plannedEnd}
                    />
                  </Grid>
                </Grid>
              )}

              {teamCanView("advanceFilterAccess") && !showSaveFilterPlan && (
                <Grid
                  container
                  direction="row"
                  justify="flex-end"
                  alignItems="center"
                  classes={{ container: classes.filterBottomBtns }}
                  style={{ bottom: promoBar || notificationBar ? 185 : 140 }}
                >
                  {saveFilter ? (
                    <CustomButton
                      style={{ flex: 1, marginRight: 20 }}
                      btnType="white"
                      variant="contained"
                      onClick={this.handleBackFilter}
                    >
                      <FormattedMessage
                        id="common.action.cancel.label"
                        defaultMessage="Cancel"
                      />
                    </CustomButton>
                  ) : null}
                  <CustomButton
                    style={{ flex: 1 }}
                    btnType="success"
                    variant="contained"
                    onClick={this.handleSaveFilter}
                    disabled={
                      saveFilter ? saveBtnQuery == "progress" : isSaveDisable
                    }
                    query={saveFilter ? saveBtnQuery : null}
                  >
                    {saveFilter ? (
                      <FormattedMessage
                      id="filters.save.label"
                      defaultMessage="Save Filter"
                      />
                    ) : (
                      <FormattedMessage
                        id="filters.custom-filter.label"
                        defaultMessage="Custom Filter"
                      />
                    )}
                  </CustomButton>
                  {!saveFilter ? (
                    <DefaultButton
                      text={
                        <FormattedMessage
                          id="common.search.label"
                          defaultMessage="Search"
                        />
                      }
                      buttonType="Plain"
                      style={{
                        flex: 1,
                        marginLeft: 20,
                      }}
                      onClick={this.handleSearchFilter}
                      disabled={isDisable}
                    />
                  ) : null}
                </Grid>
              )}
            </div>
          </Scrollbars>
        </Drawer>
      </ClickAwayListener>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profileState: state.profile,
    members: state.profile.data ? state.profile.data.member.allMembers : [],
    tasks: state.tasks.data || [],
    appliedFiltersState: state.appliedFilters,
    itemOrderState: state.itemOrder,
    promoBar: state.promoBar.promoBar,
    notificationBar: state.notificationBar.notificationBar,
  };
};

export default compose(
  withRouter,
  injectIntl,
  withStyles(styles, {
    withTheme: true,
  }),
  connect(mapStateToProps, {
    SaveItemOrder,
    setAppliedFilters,
  })
)(IssueFilter);
