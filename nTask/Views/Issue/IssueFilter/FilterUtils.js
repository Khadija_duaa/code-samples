import intersectionWith from "lodash/intersectionWith";
import findIndex from "lodash/findIndex";
import helper from "../../../helper";
import { generateUsername } from "../../../utils/common";

export function getFilteredIssues(
  IssueArray,
  multipleFilters,
  issueState,
  members,
  projects,
  issuePer,
  tasks
) {
  let issues = JSON.parse(JSON.stringify(IssueArray)) || [];

  let newIssues = [],
    isArchived =
      issueState && issueState.length
        ? issueState.indexOf("Archived") > -1
        : false,
    showAll = true,
    start = "",
    end = "",
    compare = false,
    flag = 0;

  // Filtering Archived
  if (!isArchived) {
    const filtered = issues.filter((x) => {
      if (x.isArchive === false) {
        return x;
      }
    });
    issues = filtered;
  }

  if (multipleFilters) {
    let value = issues || [];
    if (multipleFilters.assignees && multipleFilters.assignees.length > 0) {
      flag = 1;
      value = issues.filter((x) => {
        // if (intersectionWith(multipleFilters.assignees, x.issueAssingnees, (x, y) => x.id === y).length > 0) {
        if (
          intersectionWith(
            multipleFilters.assignees,
            x.issueAssingnees,
            (x, y) => x === y
          ).length > 0
        ) {
          return x;
        }
      });
    }
    if (multipleFilters.createdBy && multipleFilters.createdBy.length > 0) {
      value = value.length >= 0 && flag === 1 ? value : issues;
      flag = 1;
      value = value.filter((x) => {
        // if (findIndex(multipleFilters.createdBy, (o) => o.id === x.createdBy) > -1)
        if (multipleFilters.createdBy.indexOf(x.createdBy) > -1) return x;
      });
    }

    if (multipleFilters.status && multipleFilters.status.length > 0) {
      value = value.length >= 0 && flag === 1 ? value : issues;
      flag = 1;
      value = value.filter((x) => {
        let issueStatusCheck = multipleFilters.status.findIndex((item) => {
          return item == x.status;
        });
        if (issueStatusCheck >= 0) {
          return x;
        }
      });
    }
    if (multipleFilters.priority && multipleFilters.priority.length > 0) {
      value = value.length >= 0 && flag === 1 ? value : issues;
      flag = 1;
      value = value.filter((x) => {
        let issuePriorityCheck = multipleFilters.priority.findIndex((item) => {
          return item == x.priority;
        });
        if (issuePriorityCheck >= 0) {
          return x;
        }
      });
    }

    if (multipleFilters.severity && multipleFilters.severity.length > 0) {
      value = value.length >= 0 && flag === 1 ? value : issues;
      flag = 1;
      value = value.filter((x) => {
        let issueSeverityCheck = multipleFilters.severity.findIndex((item) => {
          return item.label == x.severity;
        });
        if (issueSeverityCheck >= 0) {
          return x;
        }
      });
    }
    if (multipleFilters.type && multipleFilters.type.length > 0) {
      value = value.length >= 0 && flag === 1 ? value : issues;
      flag = 1;
      value = value.filter((x) => {
        let issueTypeCheck = multipleFilters.type.findIndex((item) => {
          return item == x.type;
        });
        if (issueTypeCheck >= 0) {
          return x;
        }
      });
    }

    if (multipleFilters.task && multipleFilters.task.length > 0) {
      value = value.length >= 0 && flag === 1 ? value : issues;
      flag = 1;
      value = value.filter((x) => {
        // if (intersectionWith(multipleFilters.task, x.linkedTasks, (x, y) => x.id === y).length > 0) {
        if (
          intersectionWith(
            multipleFilters.task,
            x.linkedTasks,
            (x, y) => x === y
          ).length > 0
        ) {
          return x;
        }
      });
    }
    if (multipleFilters.label && multipleFilters.label.length > 0) {
      value = value.length >= 0 && flag === 1 ? value : issues;
      flag = 1;
      value = value.filter((x) => {
        if (multipleFilters.label.indexOf(x.label) >= 0) {
          return x;
        }
      });
    }
    if (multipleFilters.category && multipleFilters.category.length > 0) {
      value = value.length >= 0 && flag === 1 ? value : issues;
      flag = 1;
      value = value.filter((x) => {
        if (multipleFilters.category.indexOf(x.category) >= 0) {
          return x;
        }
      });
    }

    if (
      multipleFilters.actualStart.startDate ||
      multipleFilters.actualStart.endDate
    ) {
      value = value.length >= 0 && flag === 1 ? value : issues;
      flag = 1;
      value = value.filter((x) => {
        start = new Date(
          helper.RETURN_CUSTOMDATEFORMAT(
            multipleFilters.actualStart.startDate,
            "12:00:00 AM"
          )
        );
        end = multipleFilters.actualStart.endDate
          ? new Date(
              helper.RETURN_CUSTOMDATEFORMAT(
                multipleFilters.actualStart.endDate,
                "12:00:00 AM"
              )
            )
          : "";
        compare = helper.CHECKDATEIFPRESENTINRANGE(
          start,
          end,
          new Date(x.actualStartDateString)
        );
        if (compare) return x;
      });
    }
    if (
      multipleFilters.plannedStart.startDate ||
      multipleFilters.plannedStart.endDate
    ) {
      value = value.length >= 0 && flag === 1 ? value : issues;
      flag = 1;
      value = value.filter((x) => {
        start = new Date(
          helper.RETURN_CUSTOMDATEFORMAT(
            multipleFilters.plannedStart.startDate,
            "12:00:00 AM"
          )
        );
        end = multipleFilters.plannedStart.endDate
          ? new Date(
              helper.RETURN_CUSTOMDATEFORMAT(
                multipleFilters.plannedStart.endDate,
                "12:00:00 AM"
              )
            )
          : "";
        compare = helper.CHECKDATEIFPRESENTINRANGE(
          start,
          end,
          new Date(x.startDateString)
        );
        if (compare) return x;
      });
    }
    if (
      multipleFilters.actualEnd.startDate ||
      multipleFilters.actualEnd.endDate
    ) {
      value = value.length >= 0 && flag === 1 ? value : issues;
      flag = 1;
      value = value.filter((x) => {
        start = new Date(
          helper.RETURN_CUSTOMDATEFORMAT(
            multipleFilters.actualEnd.startDate,
            "12:00:00 AM"
          )
        );
        end = multipleFilters.actualEnd.endDate
          ? new Date(
              helper.RETURN_CUSTOMDATEFORMAT(
                multipleFilters.actualEnd.endDate,
                "12:00:00 AM"
              )
            )
          : "";
        compare = helper.CHECKDATEIFPRESENTINRANGE(
          start,
          end,
          new Date(x.actualDueDateString)
        );
        if (compare) return x;
      });
    }
    if (
      multipleFilters.plannedEnd.startDate ||
      multipleFilters.plannedEnd.endDate
    ) {
      value = value.length >= 0 && flag === 1 ? value : issues;
      flag = 1;
      value = value.filter((x) => {
        start = new Date(
          helper.RETURN_CUSTOMDATEFORMAT(
            multipleFilters.plannedEnd.startDate,
            "12:00:00 AM"
          )
        );
        end = multipleFilters.plannedEnd.endDate
          ? new Date(
              helper.RETURN_CUSTOMDATEFORMAT(
                multipleFilters.plannedEnd.endDate,
                "12:00:00 AM"
              )
            )
          : "";
        compare = helper.CHECKDATEIFPRESENTINRANGE(
          start,
          end,
          new Date(x.dueDateString)
        );
        if (compare) return x;
      });
    }
    flag = 0;
    issues = value;
  } else if (issueState && issueState.length) {
    const Severity = ["Critical", "Major", "Minor", "Moderate"];
    const Priority = ["Critical", "High", "Medium", "Low"];
    let severity = [],
      priority = [];

    issueState.forEach((x) => {
      if (x < 5) severity.push(Severity[x - 1]);
      else if (x < 9) priority.push(Priority[x - 5]);
    });

    newIssues = issues.slice(0);

    isArchived = issueState.indexOf("Archived") >= 0;
    showAll = issueState.indexOf("Show All") >= 0;

    if (severity.length) {
      newIssues = newIssues.filter((x) => {
        if (severity.indexOf(x.severity) >= 0) {
          return x;
        }
      });
    }

    if (priority.length) {
      newIssues = newIssues.filter((x) => {
        if (priority.indexOf(x.priority) >= 0) {
          return x;
        }
      });
    }

    if (isArchived) {
      newIssues = newIssues.filter((x) => {
        if (x.isArchive) {
          return x;
        }
      });
    }

    flag = 0;
    issues = newIssues;
  }

  // inserting new fields {...issue, assigneeLists, createdByName}
  issues = issues.map((issue) => {
    issue.assigneeLists = members.filter((member) => {
      if (member.userId === issue.createdBy)
        issue.createdByName = generateUsername(
          member.email,
          member.userName,
          member.fullName
        );
      if (issue.issueAssingnees && issue.issueAssingnees.length >= 0)
        return issue.issueAssingnees.indexOf(member.userId) >= 0;
    });
    return issue;
  });
  return issues;
}
