import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import ListItemText from "@material-ui/core/ListItemText";
import DoneIcon from "@material-ui/icons/Done";
import selectStyles from "../../../assets/jss/components/select";
import Checkbox from "@material-ui/core/Checkbox";
import SelectMenu from "../../../components/Menu/SelectMenu";
import FlagIcon from "@material-ui/icons/Flag";
import BugIcon from "../../../components/Icons/BugIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import StarIcon from "../../../components/Icons/StarIcon";
import ImprovementIcon from "../../../components/Icons/ImprovementIcon";

let object = {};
class TypeDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: [],
      clear: false
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.isClear) {
      object = {
        name: [],
        clear: true
      };
    } else {
      object = {
        clear: false
      };
    }
    return object;
  }

  handleChange = event => {
    this.setState({ name: event.target.value }, () => {
      this.props.handleFilterChange("type", this.state.name);
    });
  };

  componentDidMount() {
    let name = this.props.name;
    if (name) this.setState({ name });
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.clear) {
      this.props.handleClearState(true);
    }

    let name = this.props.name;
    if (name && JSON.stringify(name) !== JSON.stringify(prevProps.name))
      this.setState({ name });
  }
  render() {
    const { classes, theme } = this.props;
    const { name, clear } = this.state;
    const statusColor = theme.palette.status;
    const names = [
      { name: "Bug", color: statusColor.completed },
      { name: "Feature", color: statusColor.review },
      { name: "Improvement", color: statusColor.inProgress }
    ];

    return (
      <SelectMenu
        label="Issue Type"
        multiple={true}
        error={false}
        value={name}
        onChange={this.handleChange}
      >
        {names.map((status, i) => (
          <MenuItem
            key={status.name}
            value={status.name}
            className={`${classes.statusMenuItemCnt} ${
                this.state.name.indexOf(status.name) > -1 ? classes.selectedValue : ""
              }`}
            classes={{
              root: classes.statusMenuItemCnt,
              selected: classes.statusMenuItemSelected
            }}
          >
            {i == 0 ? (
              <SvgIcon
                viewBox="0 0 456.828 456.828"
                htmlColor={theme.palette.error.light}
                className={classes.bugIcon}
              >
                <BugIcon />
              </SvgIcon>
            ) : i == 2 ? (
              <SvgIcon
                viewBox="0 0 19.481 19.481"
                classes={{ root: classes.ratingStarIcon }}
                htmlColor={theme.palette.background.star}
              >
                <StarIcon />
              </SvgIcon>
            ) : (
              <SvgIcon
                viewBox="0 0 11 10.188"
                classes={{ root: classes.ImprovmentIcon }}
                htmlColor={theme.palette.primary.light}
              >
                <ImprovementIcon />
              </SvgIcon>
            )}
            <ListItemText
              primary={status.name}
              classes={{ primary: classes.statusItemText }}
            />

           
          </MenuItem>
        ))}
      </SelectMenu>
    );
  }
}

export default withStyles(selectStyles, {
  withTheme: true
})(TypeDropdown);
