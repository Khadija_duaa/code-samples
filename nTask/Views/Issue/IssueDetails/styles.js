const taskDetailStyles = theme => ({
  taskDetailsDialog: {
    paddingRight: "0 !important",
  },
  dialogCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  dialogPaperCnt: {
    overflowY: "visible",
    background: theme.palette.common.white,
    maxWidth: "95%",
    minHeight: "90%",
  },
  defaultDialogTitle: {
    padding: "0",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
  },
  defaultDialogContent: {
    padding: "0",
    overflowY: "visible",
    display: "flex",
    justifyContent: "stretch",
    alignItems: "stretch",
  },
  defaultDialogAction: {
    padding: " 25px 25px",
  },
  assigneeListCnt: {
    padding: "0 20px",
    margin: "0 10px",
    borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
    borderRight: `1px solid ${theme.palette.border.lightBorder}`,
  },
  mainTaskDetailsCnt: {},

  taskDetailsLeftCnt: {
    borderRight: `1px solid ${theme.palette.border.lightBorder}`,
    padding: 30,
    height: "100%",
  },
  dropdownsLabel: {
    transform: "translate(6px, -7px) scale(1)",
    display: "block",
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular,
  },
  btnDropdownsLabel: {
    transform: "translate(0, -7px) scale(1)",
    display: "block",
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular,
  },
  taskDetailsFieldCnt: {},
  descriptionCnt: {
    height: 120,
    resize: "vertical",
    padding: 10,
    borderRadius: "4px",
    overflowY: "auto",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    fontSize: "12px !important",
    "& *": {
      margin: 0,
    },
  },
  tab: {
    minWidth: "unset",
    flexGrow: 1,
    flexBasis: "unset"
  },
  tabSelected: {
    background: "unset !important",
    boxShadow: "unset !important",
  },
  tabLabelCnt: {
    padding: 0,
    fontSize: "12px !important",
    textTransform: "capitalize",
    fontWeight: theme.typography.fontWeightRegular,
  },
  TabsRoot: {
    background: theme.palette.background.paper,
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
  },
  TabContentCnt: {
    background: theme.palette.background.light,
    position: "relative",
    height: "calc(100% - 49px)",
  },
  tabIndicator: {
    background: theme.palette.primary.light,
  },
  outlinedInputCnt: {
    marginRight: 15,
  },
  outlinedTimeInputCnt: {
    margin: 0,
    background: theme.palette.common.white,
    borderRadius: 4,
  },
  outlinedMinInputCnt: {
    "&:hover $notchedOutlineMinCnt": {
      border: `1px solid ${theme.palette.border.lightBorder} !important`,
    },
  },
  outlinedHoursInputCnt: {
    "&:hover $notchedOutlineHoursCnt": {
      border: `1px solid ${theme.palette.border.lightBorder} !important`,
      borderRight: "none !important",
    },
  },
  outlineInputFocus: {
    "& $notchedOutlineCnt": {
      border: `1px solid ${theme.palette.border.lightBorder}`,
    },
    "& $notchedOutlineMinCnt": {
      border: `1px solid ${theme.palette.border.lightBorder}`,
    },
    "& $notchedOutlineHoursCnt": {
      border: `1px solid ${theme.palette.border.lightBorder}`,
      borderRight: 0,
    },
    "& $notchedOutlineAMCnt": {
      border: `1px solid ${theme.palette.border.lightBorder}`,
    },
  },
  outlinedInput: {
    padding: "13.5px 14px",
    "&:hover": {
      cursor: "auto",
    },
  },
  outlinedDateInput: {
    padding: "13.5px 14px",
    background: theme.palette.background.default,
    borderRadius: 4,
  },
  outlinedTaskInput: {
    padding: "10px 14px",
    background: theme.palette.background.default,
    borderRadius: 4,
    fontSize: "22px !important",
    fontWeight: theme.typography.fontWeightRegular,
  },
  outlinedInputDisabled: {
    background: "transparent",
  },
  outlinedTimeInput: {
    padding: 14,
    textAlign: "center",
  },
  outlinedHoursInput: {
    background: theme.palette.background.default,
    textAlign: "center",
    borderRadius: "30px 0 0 30px",
    padding: 14,
  },
  outlinedMinInput: {
    padding: 14,
    background: theme.palette.background.default,
    textAlign: "center",
  },
  outlinedAmInput: {
    padding: 14,
    borderRadius: "0 30px 30px 0",
    borderLeft: 0,
    textAlign: "center",
    "&:hover": {
      cursor: "pointer",
    },
  },
  notchedOutlineCnt: {
    borderRadius: 4,
    border: `1px solid ${theme.palette.border.lightBorder}`,
  },
  notchedOutlineHoursCnt: {
    borderRadius: "30px 0 0 30px",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRight: 0,
  },
  notchedOutlineMinCnt: {
    borderRadius: 0,
    border: `1px solid ${theme.palette.border.lightBorder}`,
  },
  notchedOutlineAMCnt: {
    borderRadius: "0 30px 30px 0",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderLeft: 0,
  },
  addTimeBtnCnt: {
    display: "flex",
    background: theme.palette.background.medium,
    padding: "5px 5px",
    cursor: "pointer",
    borderRadius: 20,
  },
  addTimeActionBtnCnt: {
    textAlign: "right",
    marginTop: 10,
  },
  addTimeBtnText: {
    color: theme.palette.text.primary,
    fontSize: "16px !important",
    lineHeight: "20px",
    marginLeft: 5,
    textDecoration: "underline",
  },

  menuTab: {
    background: theme.palette.background.light,
    borderRadius: "10px 10px 0 0",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderBottom: "none",
    opacity: 1,
    minWidth: 152,
  },
  menuTabSelected: {
    background: theme.palette.common.white,
    borderRadius: "10px 10px 0 0",
    border: `1px solid ${theme.palette.border.lightBorder}`,
  },
  menuTabIndicator: {
    background: "transparent",
    height: 1,
  },
  menuTimeCnt: {
    background: theme.palette.background.light,
    marginTop: 48,
    marginBottom: 4,
    border: "1px solid rgba(221,221,221,1)",
    borderRadius: "0 12px 12px 0",
    borderLeft: 0,
    width: 250,
  },
  dateTimeHeading: {
    background: theme.palette.background.default,
    padding: "10px 20px",
    margin: 0,
    textTransform: "uppercase",
    fontSize: "0.944rem !important",
    fontWeight: theme.typography.fontWeightRegular,
    borderRadius: "0 12px 0 0",
    color: theme.palette.text.secondary,
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
  },
  menuDateTimeInnerCnt: {
    padding: "15px 20px",
  },
  timeFieldsCnt: {
    padding: "0 20px",
  },
  otherCommentAvatar: {
    marginRight: 10,
  },
  selfCommentAvatar: {
    marginLeft: 10,
  },
  commentContentCnt: {
    borderRadius: 4,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    padding: "10px 15px",
    fontSize: "13px !important",
    background: theme.palette.background.default,
    wordBreak: "break-all",
    "& p": {
      margin: 0,
      padding: 0,
      lineHeight: 1.5,
      wordBreak: "break-word",
    },
  },
  commentAttachContentCnt: {
    borderRadius: 10,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    fontSize: "13px !important",
    padding: "10px 15px",
    background: theme.palette.background.default,
    "& p": {
      margin: 0,
      padding: 0,
    },
  },
  commentBoxMention: {
    color: theme.palette.primary.light,
  },
  otherTimeStamp: {
    fontSize: "11px !important",
    marginLeft: 12,
    color: theme.palette.text.secondary,
    textAlign: "left",
    margin: 0,
    marginTop: 3,
  },
  selfTimeStamp: {
    fontSize: "11px !important",
    marginRight: 12,
    color: theme.palette.text.secondary,
    textAlign: "right",
    margin: 0,
    marginTop: 3,
  },
  attachmentCnt: {
    background: theme.palette.background.light,
    borderRadius: 5,
    marginTop: 10,
    padding: "10px 15px 10px 15px",
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
  },
  commentsAttachmentIcon: {
    marginRight: 10,
  },
  commentsFileName: {
    fontSize: "12px !important",
    paddingRight: "20px !important",
    color: theme.palette.text.primary,
    marginBottom: "3px !important",
  },
  commentsFileSize: {
    fontSize: "10px !important",
    color: theme.palette.text.secondary,
  },
  attachmentDownloadIcon: {
    background: theme.palette.secondary.medDark,
    borderRadius: "100%",
    fontSize: "20px !important",
  },
  // Notification Menu Styles
  NotifAvatar: {
    borderRadius: "100%",
    width: 56,
    height: 56,
  },
  notifMenuItem: {
    height: "auto",
    padding: "15px 15px",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
  },
  notifMenuHeadingItem: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    cursor: "unset",
    "&:hover": {
      background: "transparent",
    },
  },
  menuHeaderReadAllText: {
    color: theme.palette.text.primary,
    cursor: "pointer",
  },
  menuHeadingText: {
    textTransform: "uppercase",
  },
  notifMenuItemText: {
    padding: 0,
    "& p": {
      fontSize: "12px !important",
      fontWeight: theme.typography.fontWeightLight,
      color: theme.palette.text.secondary,
      whiteSpace: "normal",
      margin: 0,
      "& b": {
        fontWeight: theme.typography.fontWeightMedium,
        color: theme.palette.text.primary,
      },
    },
    "& $notifMenuDate": {
      fontSize: "10px !important",
    },
  },
  notifMenuDate: {},
  activityCardCnt: {
    padding: "8px 10px",
    background: theme.palette.common.white,
    borderRadius: 4,
    border: `1px solid ${theme.palette.border.lightBorder}`,
  },
  activityMsg: {
    color: theme.palette.text.primary,
    "& b": {
      color: theme.palette.text.primary,
      fontWeight: theme.typography.fontWeightMedium,
    },
  },
  activityDate: {
    textTransform: "uppercase",
    marginBottom: 10,
    display: "flex",
    alignItems: "center",
  },
  calendarIcon: {
    marginRight: 5,
  },
  activityTime: {
    fontSize: "12px !important",
    margin: "0 0 5px 0",
    color: theme.palette.text.secondary,
  },
  singleActivityCnt: {
    marginBottom: 20,
  },
  taskTitle: {
    lineHeight: "47px",
    paddingLeft: 15,
    transition: "ease background 1s",
    borderRadius: 4,
    "&:hover": {
      background: theme.palette.background.items,
      transition: "ease background 1s",
      cursor: "text",
    },
  },
  taskCreationText: {
    fontSize: "10px !important",
    color: theme.palette.text.light,
    position: "absolute",
    padding: "10px 0",
    bottom: 0,
    margin: 0,
    left: 10,
    right: 1,
    background: theme.palette.common.white,
  },
  commentsTabCnt: {
    display: "flex",
    flexDirection: "column",
  },
  commentsCnt: {
    flex: 1,
  },
  editor: {
    height: 120,
    padding: 20,
    fontSize: "12px !important",
    overflowY: "auto",
    borderRadius: 4,
  },
  priorityIcon: {
    fontSize: "16px !important",
    marginRight: 5,
  },
  statusIcon: {
    fontSize: "11px !important",
    marginRight: 5,
  },
  severityIcon: {
    fontSize: "14px !important",
    marginRight: 5,
  },
  bugIcon: {
    fontSize: "16px !important",
    marginRight: 5,
  },
  ratingStarIcon: {
    fontSize: "16px !important",
    marginRight: 5,
  },
  ImprovmentIcon: {
    fontSize: "16px !important",
    marginRight: 5,
  },
  unArchiveTaskCnt: {
    background: theme.palette.background.black,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    padding: 10,
  },
  unArchiveTaskTxt: {
    color: theme.palette.common.white,
  },
  updatesIcon: {
    fontSize: "22px !important",
  },
  TabContentCntActivity: {
    background: "white",
    height: "calc(100% - 49px)",
    position: "relative",
  },
  container: {
    padding: "12px 10px 0px  10px",
    display: "flex",
    alignItems: "flex-start",
  },
  detail: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    padding: "0px 0px 6px 0px",
    margin: "0px 8px 0px 6px",
    borderBottom: `1px solid ${theme.palette.background.grayLighter}`,
    width: "100%",
  },
  description: {
    textAlign: "left",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: theme.typography.fontWeightLight,
    fontSize: "13px !important",
    lineHeight: "16px",
    letterSpacing: 0,
    color: `${theme.palette.text.primay}`,
  },
  user: {
    color: `${theme.palette.common.black}`,
    fontSize: "13px !important",
    fontWeight: theme.typography.fontWeightLarge,
    fontFamily: theme.typography.fontFamilyLato,
  },
  actionDescription: {
    color: `#333333`,
    fontSize: "13px !important",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
  },
  date: {
    padding: "5px 0",
    textAlign: "left",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: theme.typography.fontWeightLight,
    fontSize: "12px !important",
    lineHeight: "15px",
    letterSpacing: 0,
    color: `${theme.palette.text.medGray}`,
  },
  addCustomFieldCnt: {
    marginBottom: 20,
  },
  assessmentGraphHeadingCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
});

export default taskDetailStyles;
