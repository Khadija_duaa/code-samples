import React, { Component } from "react";
import { Editor } from "react-draft-wysiwyg";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import "../../../assets/css/richTextEditor.css";
import withStyles from "@material-ui/core/styles/withStyles";
import taskDetailStyles from "./styles";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import {
  EditorState,
  convertToRaw,
  ContentState,
} from "draft-js";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";
import InputLabel from "@material-ui/core/InputLabel";
import ReactHtmlParser from 'react-html-parser';
import { uploadDescriptionImage, saveDescriptionImage } from "../../../redux/actions/tasks";
import {FormattedMessage, injectIntl } from 'react-intl';
import helper from "../../../helper";
import DescriptionEditor from "../../../components/TextEditor/CustomTextEditor/DescriptionEditor/DescriptionEditor";


const MAX_LENGTH = 10000;

class IssueDescriptionEditor extends Component {
  constructor(props) {
    super(props);
    // const html = props.issueData.detail || "";
    // const contentBlock = htmlToDraft(html);
    // if (contentBlock) {
    //   const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
    //   const editorState = EditorState.createWithContent(contentState);
    //   this.state = {
    //     editorState,
    //     EditMode: false,
    //     contentState
    //   };
    // }
    // this.handleEditClick = this.handleEditClick.bind(this);
    // this.uploadImageCallBack = this.uploadImageCallBack.bind(this);
    // this.handleClickAway = this.handleClickAway.bind(this);
    // this.onEditorStateChange = this.onEditorStateChange.bind(this);
    this.state = {
      editorState: props.issueData.detail || "",
    }
  }
  componentDidMount() {
    let members = (this.props.profileState && this.props.profileState.data && this.props.profileState.data.member) ? this.props.profileState.data.member.allMembers : [];
    let memberList = members.map(x => {
      return {
        text: x.userName,
        value: x.userName,
        //  url: x.userName,
        userId: x.userId
      }
    })
    this.setState({ members: memberList })
  }

  // uploadImageCallBack(file) {
  //   var data = new FormData();
  //   data.append("File", file);
  //   data.append("issueId", this.props.issueData.issueId);
  //   data.append("userid", this.props.profileState.data.userId);
  //   return  (new Promise(
  //     (resolve, reject) => {
  //       uploadDescriptionImage(data)
  //         .then(response => {
  //           let fileObj = response.data[0];
  //           resolve({ data: { link: `${fileObj.publicURL}` } });
  //         })
  //         .catch(response => {
  //           reject(response);
  //         })
  //     }));
  // }
  // handleEditClick() {
  //   this.setState({ EditMode: true });
  // }
  // onEditorStateChange(editorState) {
  //   this.setState({
  //     editorState
  //   });
  // }
  // handleClickAway() {
  //   this.setState({
  //     EditMode: false
  //   });
  //   if (draftToHtml(convertToRaw(this.state.editorState.getCurrentContent())) !== this.props.issueData.detail) {
  //     let data = this.props.issueData;
  //     data.detail = window.btoa(draftToHtml(convertToRaw(this.state.editorState.getCurrentContent())));
  //     this.props.isUpdated(data);
  //   }

  // }
  // getLengthOfSelectedText = () => {
  //   const currentSelection = this.state.editorState.getSelection();
  //   const isCollapsed = currentSelection.isCollapsed();

  //   let length = 0;

  //   if (!isCollapsed) {
  //     const currentContent = this.state.editorState.getCurrentContent();
  //     const startKey = currentSelection.getStartKey();
  //     const endKey = currentSelection.getEndKey();
  //     const startBlock = currentContent.getBlockForKey(startKey);
  //     const isStartAndEndBlockAreTheSame = startKey === endKey;
  //     const startBlockTextLength = startBlock.getLength();
  //     const startSelectedTextLength = startBlockTextLength - currentSelection.getStartOffset();
  //     const endSelectedTextLength = currentSelection.getEndOffset();
  //     const keyAfterEnd = currentContent.getKeyAfter(endKey);
  //     if (isStartAndEndBlockAreTheSame) {
  //       length += currentSelection.getEndOffset() - currentSelection.getStartOffset();
  //     } else {
  //       let currentKey = startKey;

  //       while (currentKey && currentKey !== keyAfterEnd) {
  //         if (currentKey === startKey) {
  //           length += startSelectedTextLength + 1;
  //         } else if (currentKey === endKey) {
  //           length += endSelectedTextLength;
  //         } else {
  //           length += currentContent.getBlockForKey(currentKey).getLength() + 1;
  //         }

  //         currentKey = currentContent.getKeyAfter(currentKey);
  //       };
  //     }
  //   }

  //   return length;
  // }
  // handleBeforeInput = () => {
  //   const currentContent = this.state.editorState.getCurrentContent();
  //   const currentContentLength = currentContent.getPlainText('').length;
  //   const selectedTextLength = this.getLengthOfSelectedText();

  //   if (currentContentLength - selectedTextLength > MAX_LENGTH - 1) {

  //     return 'handled';
  //   }
  // }
  // handlePastedText = (pastedText) => {
  //   const currentContent = this.state.editorState.getCurrentContent();
  //   const currentContentLength = currentContent.getPlainText('').length;
  //   const selectedTextLength = this.getLengthOfSelectedText();

  //   if (currentContentLength + pastedText.length - selectedTextLength > MAX_LENGTH) {

  //     return 'handled';
  //   }
  // }
  handleIssueDescription = editorState => {
    this.setState({
      editorState: editorState.html,
    });
  };
  handleClickAwayIssueDescription = () => {
    const {issueData} =  this.props;
    const {editorState} =  this.state;
    // let taskData = {...currentTask};
    if (editorState != issueData.detail) {
      issueData.detail = editorState;
      this.props.isUpdated(issueData, (response) => {
        this.setState({
          // currentTask: currentTask,
        });
      })
    }
  }

  render() {
    const { classes, theme, permission, disabled, intl } = this.props;
    const { EditMode, editorState } = this.state;
    // const html = draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()));

    return (
      <div>
        <InputLabel classes={{ root: classes.dropdownsLabel }}>
        {<FormattedMessage id="common.description.label" defaultMessage="Enter your description here"></FormattedMessage>}
        </InputLabel>
        <div>
          <DescriptionEditor
              id="issueEditor"
            defaultValue={editorState}
            onChange={this.handleIssueDescription}
            handleClickAway={this.handleClickAwayIssueDescription}
            placeholder={intl.formatMessage({
              id: "common.type.label",
              defaultMessage: "Type something",
            })}
          />
        </div>
        {/* {disabled && EditMode && permission ? (
          <ClickAwayListener onClickAway={this.handleClickAway}>
            <Editor
              wrapperClassName="demo-wrapper"
              editorClassName={classes.editor}
              placeholder={<FormattedMessage id="common.description.placeholder" defaultMessage="Enter your description here"></FormattedMessage>}
              handleBeforeInput={this.handleBeforeInput}
              handlePastedText={this.handlePastedText}
              editorState={this.state.editorState}
              onEditorStateChange={this.onEditorStateChange}
              toolbar={{
                image: {
                  uploadCallback: this.uploadImageCallBack,
                  previewImage: true,
                  alt: { present: true, mandatory: false }
                },
                inline: { inDropdown: true },
                list: { inDropdown: true },
                textAlign: { inDropdown: true },
                link: { inDropdown: true },
                history: { inDropdown: true },
              }}
              mention={{
                separator: " ",
                trigger: "@",
                suggestions: this.state.members
              }}
              hashtag={{}}
            />
          </ClickAwayListener>
        ) : (
            <div
              className={classes.descriptionCnt}
              onClick={this.handleEditClick}
            >
              {editorState.getCurrentContent().hasText() ? ReactHtmlParser(html) : <FormattedMessage id="common.description.inner-label" defaultMessage="Add description"/>}
            </div>
          )
        } */}
      </div>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    profileState: state.profile
  }
}

export default compose(
  withRouter,
  injectIntl,
  withStyles(taskDetailStyles, { withTheme: true }),
  connect(
    mapStateToProps,
    {}
  )
)(IssueDescriptionEditor);
