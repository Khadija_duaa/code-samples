import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import EmptyState from "../../../components/EmptyStates/EmptyState";
import { withSnackbar } from "notistack";
import {
  GetRiskIssueComments,
  SaveRiskIssueComments,
  saveRiskIssueAttachment,
  SaveUpdates,
  DeleteAttachment,
} from "../../../redux/actions/issues";
import TextEditor from "../../../components/TextEditor/TextEditor";
import { withStyles } from "@material-ui/core/styles";

import taskDetailStyles from "./styles";
import ReactHtmlParser from 'react-html-parser';
import Truncate from "react-truncate";
import ArrowDownward from "@material-ui/icons/ArrowDownward";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import { Scrollbars } from "react-custom-scrollbars";
import helper from "../../../helper";
import moment from "moment";
import Icon from "../../../components/Icons/Icons";
import {
  UpdateIssueCommentData,
  DeleteIssueCommentData,
  AddNewIssueCommentData,
} from "../../../redux/actions/issueComments";
import { uploadFileTextEditor } from "../../../redux/actions/constants";
import issueRiskUpload from "../../../components/IssueRiskComment/IssueRiskCommentUpload";
import { getCompletePermissionsWithArchieve } from "../permissions";
import DeleteConfirmDialog from "../../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import CustomAvatar from "../../../components/Avatar/Avatar";
import { GetPermission } from "../../../components/permissions";
import { FormattedMessage } from "react-intl";
class CommentsTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      comments: [],
      members: [],
      attachment: "",
      fileAttachment: null,
      fileAttachmentData: null,
      selectedDeleteComment: null,
      btnQuery: "",
      deleteDialogueStatus: false,
    };
    this.handleNewComments = this.handleNewComments.bind(this);
    this.scrollComponent = React.createRef();
  }
  componentDidMount() {
    let members =
      this.props.profileState &&
      this.props.profileState.data &&
      this.props.profileState.data.member
        ? this.props.profileState.data.member.allMembers
        : [];
    let memberList = members.map((x) => {
      return {
        text: x.userName,
        value: x.userName,
        //  url: x.userName,
        userId: x.userId,
      };
    });
    this.setState({ members: memberList }, () => {
      this.props.GetRiskIssueComments(
        { id: this.props.MenuData.id, type: "Issue" },
        (response) => {
          this.setState(
            { comments: this.props.issueCommentsDataState.data },
            () => {
              if (this.scrollComponent.container) {
                this.scrollComponent.scrollToBottom();
              }
            }
          );
        },
        (error) => {
          this.setState({ comments: [] });
        }
      );
    });
  }
  componentDidUpdate(prevProps, prevState) {
    if (
      JSON.stringify(this.props.issueCommentsDataState.data) !==
      JSON.stringify(prevProps.issueCommentsDataState.data)
    ) {
      const filterUnrelatedComments = this.props.issueCommentsDataState.data.filter(
        (c) => {
          return c.update.riskIssueId == prevProps.MenuData.id;
        }
      );
      this.setState({ comments: filterUnrelatedComments }, () => {
        if (this.scrollComponent && this.scrollComponent.container) {
          this.scrollComponent.scrollToBottom();
        }
      });
    }
  }
  handleNewComments(data) {
    issueRiskUpload.SaveRiskIssueTextComment(data, "Issue", this);
  }

  UploadFileIssue = (text, EditorState, self) => {
    issueRiskUpload.UploadIssueRiskFile(text, "Issue", this, EditorState, self);
  };

  saveAttachmentObj = (obj) => {
    this.setState({
      fileAttachmentData: obj.fileAttachmentData,
      fileAttachment: obj.fileAttachment,
    });
  };
  _handleImageChange = (e) => {
    e.preventDefault();
    issueRiskUpload.handleImageUpload(
      e,
      this.props.MenuData.id,
      "Issue",
      this.props.profileState.data.userId,
      this.saveAttachmentObj
    );
  };
  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  handleDeleteIssueAttachment = () => {
    const comment = this.state.selectedDeleteComment;
    this.setState({ btnQuery: "progress" }, () => {
      this.props.DeleteAttachment(
        comment.commentId,
        (response) => {
          this.setState({ btnQuery: "", deleteDialogueStatus: false });
          if (response.data === true)
            this.props.DeleteIssueCommentData(comment.commentId);
          else
            this.props.UpdateIssueCommentData({
              response: response.data,
              id: comment.commentId,
            });
        },
        (error) => {
          this.setState({ btnQuery: "", deleteDialogueStatus: false });
        }
      );
    });
  };

  openDeleteCommentAttachmentDialogue = (comment) => {
    this.setState({
      deleteDialogueStatus: true,
      selectedDeleteComment: comment,
    });
  };

  handleDeleteCommentDialogClose = () => {
    this.setState({ deleteDialogueStatus: false, selectedDeleteComment: null });
  };
  handleDeleteAttachment = () => {
    this.setState({
      attachment: "",
      fileAttachment: null,
      fileAttachmentData: null,
    });
  };

  render() {
    const {
      classes,
      theme,
      members,
      permission,
      MenuData,
      issuePermission,
      issuePer,
    } = this.props;
    const { deleteDialogueStatus, btnQuery } = this.state;
    let newComments = this.state.comments || [];
    const issueUpdates = issuePer.issueDetail.issuecomments.cando;
    const issueAttachment =
      issuePer.issueDetail.issuecomments.issueattachment.isAllowAdd;
    const issueAttachmentDel =
      issuePer.issueDetail.issuecomments.issueattachment.isAllowDelete;
    newComments = newComments.map((x) => {
      if (x && x.userId === this.props.profileState.data.userId) {
        return {
          id: x.userId,
          user: "self",
          avatar: x.pictureUrl,
          timeStamp: moment(x.update.createdDate).fromNow(),
          fileSize: x.update.attachment
            ? x.update.attachment.fileSize + "kb"
            : "0kb",
          attachment: x.update.attachment ? x.update.attachment.docsName : null,
          commentId: x.update.id,
          text:
            (members
              ? members.find((m) => m.userId === x.update.updatedBy)
                ? members.find((m) => m.userId === x.update.updatedBy)
                    .fullName ||
                  members.find((m) => m.userId === x.update.updatedBy).userName
                : ""
              : " ") + x.update.message,
          filteType: x.update.attachment ? x.update.attachment.docsType : null,
          filtePath: x.update.attachment ? x.update.attachment.docsPath : null,
        };
      } else {
        let selectedMember = this.props.profileState.data.member.allMembers.filter(
          (member) => {
            return member.userId == x.userId;
          }
        );

        return {
          id: x.userId,
          user: "other",
          commentId: x.update.id,
          fullName: x.fullName,
          userName: x.userName,
          isOwner: selectedMember[0].isOwner,
          avatar: x.pictureUrl,
          timeStamp: moment(x.update.createdDate).fromNow(),
          fileSize: x.update.attachment
            ? x.update.attachment.fileSize + "kb"
            : "0kb",
          isOnline: selectedMember[0].isOnline,
          attachment: x.update.attachment ? x.update.attachment.docsName : null,
          text:
            (members
              ? members.find((m) => m.userId === x.update.updatedBy)
                ? members.find((m) => m.userId === x.update.updatedBy)
                    .fullName ||
                  members.find((m) => m.userId === x.update.updatedBy).userName
                : ""
              : " ") + x.update.message,
          filteType: x.update.attachment ? x.update.attachment.docsType : null,
          filtePath: x.update.attachment ? x.update.attachment.docsPath : null,
        };
      }
    });

    const comments = newComments;
    return (
      <Fragment>
        <div className={classes.commentsCnt}>
          {comments.length <= 0 ? (
            <div style={{ display: "flex", height: "100%" }}>
              <EmptyState
                screenType="comments"
                heading={
                  <FormattedMessage
                    id="common.comment.no-conversation.label"
                    defaultMessage="No Conversations Yet"
                  />
                }
                message={
                  <FormattedMessage
                    id="common.comment.no-conversation.placeholder"
                    defaultMessage="Start conversation with your team members by typing your comment below."
                  />
                }
                button={false}
                style={{ width: "90%" }}
              />
            </div>
          ) : (
            <Scrollbars
              style={{ height: "100%" }}
              ref={(c) => {
                this.scrollComponent = c;
              }}
            >
              <div style={{ padding: "20px 20px 0 20px" }}>
                {comments.map((comment, i) => (
                  <div
                    key={i}
                    style={{
                      display: "flex",
                      alignItems: "baseline",
                      marginBottom: 20,

                      flexDirection:
                        comment.user == "self" ? "row-reverse" : "row",
                    }}
                  >
                    {comment.user === "self" ? (
                      <CustomAvatar
                        styles={{ marginLeft: 10 }}
                        personal
                        size="small"
                      />
                    ) : (
                      <CustomAvatar
                        otherMember={{
                          imageUrl: comment.avatar,
                          fullName: comment.fullName,
                          lastName: "",
                          email: comment.userName,
                          isOnline: comment.isOnline,
                          isOwner: comment.isOwner,
                        }}
                        styles={{ marginRight: 10 }}
                        size="small"
                      />
                    )}

                    {/* <Avatar
                      classes={{
                        root:
                          comment.user == "self"
                            ? classes.selfCommentAvatar
                            : classes.otherCommentAvatar
                      }}
                      src={comment.avatar}
                    >
                      {comment.avatar}
                    </Avatar> */}
                    <div className={classes.commentContentOuterCnt}>
                      <div
                        className={
                          comment.attachment
                            ? classes.commentAttachContentCnt
                            : classes.commentContentCnt
                        }
                      >
                        {ReactHtmlParser(comment.text)}
                        {comment.attachment ? (
                          <div className={classes.attachmentCnt}>
                            <img
                              className={classes.commentsAttachmentIcon}
                              src={
                                comment.filteType == "pdf"
                                  ? Icon.PdfIcon
                                  : comment.filteType == "xlsx"
                                  ? Icon.XlsxIcon
                                  : ""
                              }
                            />
                            <div>
                              <p className={classes.commentsFileName}>
                                <Truncate
                                  trimWhitespace={true}
                                  width={250}
                                  ellipsis={<span>...</span>}
                                >
                                  {comment.attachment.split("/").pop()}
                                </Truncate>
                              </p>
                              <p className={classes.commentsFileSize}>
                                {comment.fileSize}
                              </p>
                            </div>
                            <a
                              onClick={() =>
                                helper.DOWNLOAD_TEMPLATE(
                                  comment.filtePath,
                                  comment.attachment,
                                  null,
                                  "attachment"
                                )
                              }
                            >
                              <ArrowDownward
                                classes={{
                                  root: classes.attachmentDownloadIcon,
                                }}
                                htmlColor={theme.palette.common.white}
                                fontSize="small"
                              />
                            </a>
                            {issueAttachmentDel && (
                              <a
                                onClick={() =>
                                  this.openDeleteCommentAttachmentDialogue(
                                    comment
                                  )
                                }
                              >
                                <DeleteOutline
                                  classes={{
                                    root: classes.attachmentDownloadIcon,
                                  }}
                                  htmlColor={theme.palette.common.white}
                                  fontSize="small"
                                />
                              </a>
                            )}
                          </div>
                        ) : null}
                      </div>
                      <p
                        className={
                          comment.user == "self"
                            ? classes.selfTimeStamp
                            : classes.otherTimeStamp
                        }
                      >
                        {comment.timeStamp}
                      </p>
                    </div>
                  </div>
                ))}
              </div>
            </Scrollbars>
          )}
        </div>
        <div className={classes.textEditorCnt}>
          {issueUpdates ? (
            <TextEditor
              type="Issue"
              MenuData={this.props.MenuData}
              handleNewComments={this.handleNewComments}
              fileUploadIssues={this._handleImageChange}
              fileAttachment={this.state.fileAttachment}
              UploadFileComment={this.UploadFileIssue}
              view={"Issue"}
              permissionAttachment={issueAttachment}
              deleteAttachment={this.handleDeleteAttachment}
            />
          ) : null}
          {/* <CommentsTextBox currentTask={this.props.currentTask} handleNewComments={this.handleNewComments} members={this.state.members} /> */}
        </div>
        <DeleteConfirmDialog
          open={deleteDialogueStatus}
          closeAction={this.handleDeleteCommentDialogClose}
          cancelBtnText={
            <FormattedMessage
              id="common.action.cancel.label"
              defaultMessage="Cancel"
            />
          }
          successBtnText={
            <FormattedMessage
              id="common.action.delete.label"
              defaultMessage="Delete"
            />
          }
          alignment="center"
          headingText={
            <FormattedMessage
              id="common.action.delete.label"
              defaultMessage="Delete"
            />
          }
          successAction={this.handleDeleteIssueAttachment}
          msgText={
            <FormattedMessage
              id="common.attach-file.action"
              defaultMessage="Are you sure you want to delete this attachment?"
            />
          }
          btnQuery={btnQuery}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profileState: state.profile,
    issueCommentsDataState: state.issueCommentsData,
    issuePermission: state.workspacePermissions.data.issue,
  };
};

export default compose(
  withRouter,
  withSnackbar,
  withStyles(taskDetailStyles, { withTheme: true }),
  connect(mapStateToProps, {
    GetRiskIssueComments,
    SaveRiskIssueComments,
    saveRiskIssueAttachment,
    uploadFileTextEditor,
    SaveUpdates,
    UpdateIssueCommentData,
    DeleteIssueCommentData,
    DeleteAttachment,
    AddNewIssueCommentData,
  })
)(CommentsTab);
