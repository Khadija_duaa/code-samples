import React, { Component } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { withSnackbar } from "notistack";
import { connect } from "react-redux";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import CloseIcon from "@material-ui/icons/Close";
import IconButton from "../../../components/Buttons/IconButton";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import taskDetailStyles from "./styles";
import { withStyles } from "@material-ui/core/styles";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import { Scrollbars } from "react-custom-scrollbars";
import SideTabs from "./SideTabs";
import IssueDescriptionEditor from "./IssueDetailsDescription";
import NotificationMenu from "../../../components/Header/NotificationsMenu";
import helper from "../../../helper";
import IssueActionDropdown from "../List/IssueActionDropDown";
import AssigneeDropdown from "../../../components/Dropdown/AssigneeDropdown";
import SelectSearchDropdown from "../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { generateSelectData } from "../../../helper/generateSelectData";
import { priorityData, statusData, typeData, severity } from "../../../helper/issueDropdownData";
import CustomFieldView from "../../CustomFieldSettings/CustomFields.view";
import { customFieldDropdownData } from "../../../helper/customFieldsData";
import cloneDeep from "lodash/cloneDeep";
import {
  UpdateIssue,
  DeleteIssue,
  ArchiveIssue,
  UnarchiveIssue,
  MarkIssueNotificationsAsRead,
  dispatchIssue,
  bulkUpdateIssue,
  UpdateIssueCustomField,
} from "../../../redux/actions/issues";
import {
  getIssuesPermissions,
  getEditPermissionsWithArchieve,
  getIssuesPermissionsWithoutArchieve,
  canEdit,
} from "../permissions";
import CustomRangeDatePicker from "../../../components/DatePicker/CustomRangeDatePicker";
import CustomButton from "../../../components/Buttons/CustomButton";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import { GetPermission } from "../../../components/permissions";
import { FormattedMessage, injectIntl } from "react-intl";
import { editCustomField, updateCustomFieldData, hideCustomField } from "../../../redux/actions/customFields";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";
import { ResponsiveLine } from "@nivo/line";
import { calculateHeight } from "../../../utils/common";
import { updateCustomFieldDialogState } from "../../../redux/actions/allDialogs";
import SectionsView from "../../../components/Sections/sections.view";

const MyResponsiveLine = ({ data, axisLeft, axisBottom, chartProps, xScale, yScale }) => {
  return (
    <ResponsiveLine
      data={data}
      margin={{ top: 20, right: 20, bottom: 50, left: 40 }}
      xScale={{ type: "linear", stacked: false, ...xScale }}
      yScale={{ type: "linear", stacked: false, ...yScale }}
      tooltip={({ point }) => {
        return (
          <div
            style={{
              padding: 5,
              background: "white",
              border: "1px solid rgba(221, 221, 221, 1)",
              borderRadius: 4,
              fontSize: "13px",
              fontFamily: "'lato', sans-serif",
            }}>
            {point.data.obj.cellName} : {moment(point.data.obj.createdDate).format("DD MMM, YYYY")}
          </div>
        );
      }}
      axisTop={null}
      axisRight={null}
      enableGridX={false}
      axisBottom={{
        orient: "bottom",
        tickSize: 5,
        tickPadding: 5,
        tickRotation: 0,
        tickValues: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        format: function(value) {
          const alphabets = "ABCDEFGHIJ";
          return alphabets[value - 1];
        },
        ...axisBottom,
      }}
      axisLeft={{
        orient: "left",
        tickSize: 5,
        tickPadding: 5,
        tickRotation: 0,
        tickValues: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        ...axisLeft,
      }}
      lineWidth={0}
      pointColor={{ theme: "background" }}
      pointBorderWidth={2}
      pointBorderColor="#7f3493"
      pointLabelYOffset={-12}
      useMesh={true}
      pointSize={10}
    />
  );
};
class IssueDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: true,
      editIssueName: false,
      issueNameError: false,
      issueName: "",
      currentIssue: this.props.currentIssue,
      issueNameOriginal: "",
      readAll: true,
      task: [],
      unArchiveFlag: false,
      unarchiveBtnQuery: "",
      assessmentDialogOpen: false,
      // EditMode:false
    };
    this.handleIssueNameEdit = this.handleIssueNameEdit.bind(this);
    this.handleIssueNameInput = this.handleIssueNameInput.bind(this);
    this.handleIssueNameSave = this.handleIssueNameSave.bind(this);
    this.handleAllRead = this.handleAllRead.bind(this);
  }

  componentDidMount() {
    this.setState({
      currentIssue: this.props.currentIssue,
      issueName: this.props.currentIssue.title,
      task: this.generateSelectedValue(),
    });
    document.querySelector("#root").style.filter = "blur(8px)";
  }
  componentWillUnmount() {
    document.querySelector("#root").style.filter = "";
    if (this.props.listView) {
      this.props.history.push("/issues");
    }
  }
  componentDidUpdate(prevProps, prevState) {
    const { currentIssue } = this.props;

    // if currentIssue is deleted from other users
    if (!currentIssue && JSON.stringify(prevProps.currentIssue) !== JSON.stringify(currentIssue)) {
      this.closeActionMenu();
    } else if (JSON.stringify(prevProps.currentIssue) !== JSON.stringify(currentIssue)) {
      this.setState({
        currentIssue: currentIssue,
        issueName: currentIssue.title,
        task: this.generateSelectedValue(),
      });
    }
  }

  handleAllRead() {
    const currentUser = this.props.userId;
    if (this.state.readAll) {
      this.setState({ readAll: false }, () => {
        this.props.MarkIssueNotificationsAsRead(
          this.state.currentIssue.id,
          currentUser,
          (err, data) => {
            this.setState({ readAll: true });
          }
        );
      });
    }
  }

  handleIssueNameEdit() {
    this.setState({ editIssueName: true });
  }
  issueNameValidator = () => {
    //if (event === "enter") {

    let response1 = helper.HELPER_EMPTY("Issue title", this.state.issueName);
    let response2 = helper.HELPER_CHARLIMIT80("Issue title", this.state.issueName);
    let response3 = helper.RETURN_CECKSPACES(this.state.issueName, "Issue title");
    let error = null;
    if (response1) {
      error = response1;
    } else {
      if (response2) {
        error = response2;
      } else if (response3) {
        error = response3;
      }
    }
    if (error) {
      this.setState({
        issueNameError: true,
      });
      return false;
    } else {
      return true;
    }
  };
  handleIssueNameSave(event, isClick) {
    if (
      (event.key === "Enter" || isClick) &&
      this.issueNameValidator() &&
      this.state.issueName &&
      this.state.issueNameOriginal !== this.state.issueName
    ) {
      let currentIssue = this.state.currentIssue,
        self = this;
      this.setState({ issueName: self.state.issueName }, function() {
        currentIssue.title = self.state.issueName;
        this.props.isUpdated(currentIssue, () => {
          this.setState({
            editIssueName: false,
            issueNameError: false,
            issueNameOriginal: this.state.issueName,
          });
        });
      });
    }
  }
  handleIssueNameInput(event) {
    this.setState({ issueName: event.target.value });
  }
  handleIssueClickAway = event => {
    if (!helper.RETURN_CECKSPACES(this.state.issueName))
      this.setState({ editIssueName: false }, () => {
        this.handleIssueNameSave(event, true);
      });
  };
  handleDateSave = (startDate, endDate, dateType, startTime, endTime) => {
    let currentIssue = this.state.currentIssue;
    const obj =
      dateType == "planned"
        ? {
            startDateString: helper.RETURN_CUSTOMDATEFORMAT(startDate),
            dueDateString: helper.RETURN_CUSTOMDATEFORMAT(endDate),
            startTime: startTime,
            dueTime: endTime,
          }
        : {
            actualStartDateString: helper.RETURN_CUSTOMDATEFORMAT(startDate),
            actualDueDateString: helper.RETURN_CUSTOMDATEFORMAT(endDate),
            actualStartTime: startTime,
            actualDueTime: endTime,
          };
    this.props.UpdateIssue(
      { ...currentIssue, ...obj },
      response => {},
      err => {}
    );
  };
  // isUpdated = (data, callback) => {
  //   if (data.archive) {
  //     this.props.ArchiveIssue(
  //       data,
  //       data => {
  //         if (callback) callback();
  //         this.props.closeActionMenu();
  //       },
  //       error => {
  //         if (error && error.status === 500) {
  //           this.props.handleExportType("Server throws error", "error");
  //         }
  //       }
  //     );
  //   } else if (data.unarchive) {
  //     this.props.UnarchiveIssue(
  //       data,
  //       data => {
  //         if (callback) callback();
  //         this.props.closeActionMenu();
  //       },
  //       error => {
  //         if (error && error.status === 500) {
  //           this.props.handleExportType("Server throws error", "error");
  //         }
  //       }
  //     );
  //   } else {
  //     this.props.UpdateIssue(
  //       data,
  //       data => {
  //         if (callback) callback();
  //       },
  //       error => {
  //         if (error && error.status === 500) {
  //           this.props.handleExportType("Server throws error", "error");
  //         }
  //       }
  //     );
  //   }
  // };

  closeActionMenu = () => {
    if (this.props.closeActionMenu) this.props.closeActionMenu("CloseDetailsDialogue");
  };

  //Function that updates issue on select option select
  handleOptionsSelect = (key, value) => {
    let { currentIssue } = this.props;
    this.setState({ currentIssue: { ...currentIssue, [key]: value.value } });
    let newIssueObj = { ...currentIssue, [key]: value.value };
    this.props.UpdateIssue(
      newIssueObj,
      () => {},
      err => {}
    );
  };
  //Function that updates assignee of respective document (e.g. task/risk/issue/meeting)
  updateAssignee = (assignedTo, issue) => {
    let assignedToArr = assignedTo.map(ass => {
      return ass.userId;
    });
    let newIssueObj = { ...issue, issueAssingnees: assignedToArr };

    this.props.UpdateIssue(
      newIssueObj,
      () => {},
      err => {}
    );
  };

  generateTaskData = () => {
    /* function for generating the array of task and returing array to the data prop for multi drop down */
    const { tasks } = this.props; /** task array coming from mapStateToProps */
    let taskList = tasks.map((task, i) => {
      return generateSelectData(task.taskTitle, task.taskTitle, task.taskId, task.uniqueId, task);
    });
    return taskList;
  };

  generateSelectedValue = () => {
    /** function for generating the selected task list objects from task id and return taskList for showing pre populated selected task if any => function call in componentDidMount */
    const { tasks, currentIssue } = this.props;
    const selectedTaskList = tasks.filter(task => {
      return currentIssue.linkedTasks && currentIssue.linkedTasks.indexOf(task.taskId) > -1;
    });
    let taskList = selectedTaskList.map((task, i) => {
      return generateSelectData(task.taskTitle, task.taskTitle, task.taskId, task.uniqueId, task);
    });
    return taskList;
  };

  handleClearTask = (key, value) => {
    const { currentIssue, task } = this.state;
    let taskSelectedIds = value.map(obj => {
      /* taskSelectedIds is a list of task id's fetching from selected task object in drop down */
      return obj.id;
    });
    this.setState({
      [key]: value,
      currentIssue: { ...currentIssue, linkedTasks: taskSelectedIds },
    });
    this.props.UpdateIssue(
      { ...currentIssue, linkedTasks: taskSelectedIds },
      response => {},
      err => {}
    ); /** api calling to update the issue details */
  };

  handleTaskChange = (key, value) => {
    const { currentIssue, task } = this.state;
    let permission = this.checkIssueListAccordingToPermission(task, value);
    /* function for fetching the task id's and from selected task and update the issue details  */
    if (permission) {
      let taskSelectedIds = value.map(obj => {
        /* taskSelectedIds is a list of task id's fetching from selected task object in drop down */
        return obj.id;
      });
      this.setState({
        [key]: value,
        currentIssue: { ...currentIssue, linkedTasks: taskSelectedIds },
      });
      this.props.UpdateIssue(
        { ...currentIssue, linkedTasks: taskSelectedIds },
        response => {},
        err => {}
      ); /** api calling to update the issue details */
    }
  };
  // checkIssueListAccordingToPermission = (stateTask, selectedTask) => {
  //   if (stateTask.length <= 0) {
  //     /** if state task is empty then accept the selected task */
  //     return true;
  //   } else {
  //     /** if false means state task array is not empty, then check other permissions */
  //     let taskLinkedToProject = stateTask.every((t) => t.obj.projectId == null);
  //     if (taskLinkedToProject) {
  //       return true;
  //     } else {
  //       let alreadySelectedTasksProjectId = stateTask.map(
  //         (t) => t.obj.projectId
  //       );
  //       let checkIfProjectIdExists = alreadySelectedTasksProjectId.some(
  //         (p) => p == selectedTask[selectedTask.length - 1].obj.projectId
  //       );
  //       if (checkIfProjectIdExists) return true;
  //       else {
  //         if (selectedTask[selectedTask.length - 1].obj.projectId == null)
  //           return true;
  //         else {
  //           this.showSnackBar(
  //             "Oops! You cannot link this task to this issue, because it is already linked to another task. Unlink the issue with the other task first and then try again.",
  //             "error"
  //           );
  //           return false;
  //         }
  //       }
  //     }
  //   }
  // };
  checkIssueListAccordingToPermission = (stateTask, selectedTask) => {
    if (stateTask.length <= 0) {
      /** if state task is empty then accept the selected task */
      return true;
    } else {
      this.showSnackBar(
        "Oops! You cannot link this task to this issue, because it is already linked to another task. Unlink the issue with the other task first and then try again. ",
        "error"
      );
      return false;
    }
  };
  showSnackBar(snackBarMessage, type) {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  }
  // It is called to open unarchived dialog
  openPopUP = event => {
    this.setState({ unArchiveFlag: true });
  };
  // It is called to unarchived task
  handleUnArchived = e => {
    if (e) e.stopPropagation();
    const { currentIssue } = this.state;
    let { id } = currentIssue || "";
    const obj = { id, unarchive: true };
    this.setState({ unarchiveBtnQuery: "progress" }, () => {
      this.props.isUpdated(
        obj,
        success => {
          this.setState({ unarchiveBtnQuery: "", unArchiveFlag: false }, () => {
            if (this.closeActionMenu) this.closeActionMenu();
          });
        },
        error => {}
      );
    });
  };
  // It is called to close unarchived dialog
  handleDialogClose = event => {
    this.setState({ unArchiveFlag: false });
  };

  getPermission = (key, val) => {
    const { issuePer = {} } = this.props;
    const { currentIssue } = this.state;
    switch (key) {
      case "edit":
        return GetPermission.canEdit(currentIssue.isOwner, currentIssue.issuePermission, val);
        break;
      case "add":
        return GetPermission.canAdd(currentIssue.isOwner, currentIssue.issuePermission, val);
        break;
      case "delete":
        return GetPermission.canDelete(currentIssue.isOwner, currentIssue.issuePermission, val);
        break;

      default:
        break;
    }
  };

  issueNotifications = () => {
    const { issueNotifications, userId, allMembers } = this.props;
    const issueNotificationsData = issueNotifications;
    let notifications = issueNotificationsData.filter(x => {
      return x.users.filter(y => y.userId === userId && y.isViewed === false).length;
    });
    notifications = notifications
      .map(x => {
        x.createdName = allMembers.find(m => m.userId === x.createdBy);
        x.users = x.users.map(u => ({
          userId: u.userId,
          isViewed: u.isViewed,
        }));
        return x;
      })
      .map(x => ({
        id: x.notificationId,
        // title: x.createdName
        //   ? (x.createdName.fullName ||
        //       x.createdName.userName ||
        //       x.createdName.email) +
        //     " " +
        //     x.description
        //   : " ",
        title: x.description ? x.description : " ",
        date: helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(x.updatedDate || x.createdDate),
        time: helper.RETURN_CUSTOMDATEFORMATFORTIME(x.updatedDate || x.createdDate),
        profilePic: x.createdName ? x.createdName.imageUrl : "",
        createdName: x.createdName,
        notificationUrl: x.notificationUrl,
        teamName: x.teamName || "N/A",
        teamId: x.teamId,
        workspaceName: x.workSpaceName || "N/A",
        workspaceId: x.workSpaceId,
      }));
    return notifications;
  };
  onChatMount = () => {};
  onChatUnMount = () => {};
  // handleIssueDescription = data => {
  //   const {currentIssue} =  this.state;
  //   currentIssue.detail = data.html; /** function set the detail i the state */
  //   this.setState({currentIssue, EditMode: true});
  // };
  // handleClickAwayIssueDescription = () => {
  //   const {currentIssue, EditMode} =  this.state;
  //   let issueData = {...currentIssue};
  //   // issueData.detail = window.btoa(issueData.detail);
  //   if (EditMode) {
  //     this.props.isUpdated(issueData, (response) => {
  //       this.setState({
  //         currentIssue: currentIssue,
  //         EditMode: false
  //       });
  //     })
  //   }
  // }

  editCustomFieldCallback = (obj, oldFieldObject) => {
    const { fieldId, fieldType } = obj;
    if (fieldType === "checklist") return;
    this.props.updateCustomFieldData(obj);
  };

  editCustomField = (issueParam, obj) => {
    const { fieldId, fieldType, settings } = obj;
    const issueCopy = cloneDeep(issueParam);
    issueCopy.customFieldData = issueCopy.customFieldData.map(r => {
      if (r.fieldId === fieldId) {
        switch (fieldType) {
          case "dropdown":
            if (!settings.multiSelect) {
              /** if type is object/single select then simply replace the values after finding the selected field value by ID */
              let item = obj.values.data.find(v => v.id == r.fieldData.data.id) || false;
              if (item) {
                item.label = item.value;
                r.fieldData.data = {
                  ...item,
                  obj: item,
                }; /** setting edited field value in issue object */
              }
            }
            if (settings.multiSelect) {
              /** if type is array means multi select then find the updated selected values and update in issue object */
              let selectedArr = obj.values.data.filter(x =>
                r.fieldData.data.some(y => x.id === y.id)
              );
              if (selectedArr.length > 0) {
                selectedArr = customFieldDropdownData(selectedArr);
                r.fieldData.data = selectedArr; /** setting edited field value in issue object */
              }
            }
            break;

          case "matrix":
            let allcolumn = obj.settings.matrix.reduce((elem1, elem2) =>
              elem1.concat(elem2)
            ); /** merging all columns rows into one array */
            let selectedColum = allcolumn.find(
              c => c.cellName == r.fieldData.data[0].cellName
            ); /** finding the selected issue column option itno all columns */
            if (selectedColum) {
              r.fieldData.data[0] = {
                ...r.fieldData.data[0],
                color: selectedColum.color,
              }; /** updating issue field data  */
            }
            break;

          default:
            break;
        }
        return r;
      } else return r;
    });
    return issueCopy;
  };

  handleClickHideOption = (event, field) => {
    let object = { ...field };
    object.settings.isHideCompletedTodos = !object.settings.isHideCompletedTodos;
    delete object.team;
    delete object.workspaces;
    delete object.level;
    this.props.editCustomField(
      object,
      object.fieldId,
      res => {},
      failure => {}
    );
  };

  customFieldChange = (option, obj, settings) => {
    const issueCopy = this.state.currentIssue;

    if (obj.fieldType == "matrix") {
      const selectedField =
        issueCopy.customFieldData && issueCopy.customFieldData.find(x => x.fieldId == obj.fieldId);
      const isExist =
        selectedField &&
        selectedField.fieldData.data.findIndex(f => f.cellName == option.cellName) > -1;
      if (isExist === true) return;
    }
    const { fieldId } = obj;
    const newObj = {
      groupType: "issue",
      groupId: issueCopy.id,
      fieldId,
      fieldData: { data: option },
    };

    //Updating Localstate

    const customFields =
      issueCopy.customFieldData &&
      issueCopy.customFieldData.map(c => {
        return c.fieldId === newObj.fieldId ? newObj : c;
      });

    let newIssueObject = { ...issueCopy, customFieldData: customFields ? customFields : [newObj] };
    this.setState({ currentIssue: newIssueObject });

    UpdateIssueCustomField(newObj, res => {
      const issueObj = res.data.entity[0];
      let customFieldsArr = [];
      // Updating Global state
      const isExist = issueCopy.customFieldData
        ? issueCopy.customFieldData.findIndex(c => c.fieldId === issueObj.fieldId) > -1
        : false; /** if new issue created, its customFields are null so in null case scenario , compile the normal false flow, add custom field in issue object and save it */
      if (isExist) {
        customFieldsArr = issueCopy.customFieldData.map(c => {
          return c.fieldId === issueObj.fieldId ? issueObj : c;
        });
      } else {
        customFieldsArr = issueCopy.customFieldData
          ? [...issueCopy.customFieldData, issueObj]
          : [
              issueObj,
            ]; /** add custom field in issue object and save it, if newly created issue because its custom field is already null */
      }
      let newIssueObj = { ...issueCopy, customFieldData: customFieldsArr };
      this.setState({ currentIssue: newIssueObj });
      this.props.dispatchIssue(newIssueObj);
    });
    this.handleAddAssessmentDialog(false);
  };
  handleAddAssessmentDialog = value => {
    this.setState({ assessmentDialogOpen: value });
  };
  addCustomFieldCallback = obj => {
    const { fieldId } = obj;
    const customFieldDataObj = {
      groupType: "issue",
      groupId: this.state.currentIssue.id,
      fieldId: fieldId,
      fieldData: { data: obj.settings.multiSelect ? [] : {} },
    };
    const issueCopy = cloneDeep(this.state.currentIssue);
    issueCopy.customFieldData = [...issueCopy.customFieldData, customFieldDataObj];
    this.setState({ currentIssue: issueCopy });
    this.props.dispatchIssue(issueCopy);
  };

  handleEditField = field => {
    this.props.updateCustomFieldDialogState({ moduleViewType: "editCopyModal", data: field, mode: "edit" });
  };
  handleCopyField = field => {
    this.props.updateCustomFieldDialogState({ moduleViewType: "editCopyModal", data: field, mode: "copy" });
  };
  handleSelectHideOption = (value, selectedFieldObj) => {
    let object = { ...selectedFieldObj };

    if (value == 1) {
      object.hideOption = "workspace";
      object.settings.isHidden = false;

      object.settings.hiddenInWorkspaces = [
        profileState.loggedInTeam,
        ...object.settings.hiddenInWorkspaces,
      ];
      object.settings.hiddenInGroup =
        object.settings.hiddenInGroup.indexOf("issue") < 0
          ? ["issue", ...object.settings.hiddenInGroup]
          : object.settings.hiddenInGroup;
    }
    if (value == 2) {
      /** if user select the team level */
      object.hideOption = "allWorkspaces";
      object.settings.isHidden = true;
      object.settings.hiddenInWorkspaces = [];
      object.settings.hiddenInGroup =
        object.settings.hiddenInGroup.indexOf("issue") < 0
          ? ["issue", ...object.settings.hiddenInGroup]
          : object.settings.hiddenInGroup;
    }
    delete object.team;
    delete object.workspaces;
    this.props.hideCustomField(
      object,
      res => {},
      failure => {}
    );
  };

  render() {
    const {
      classes,
      theme,
      isArchived,
      userId,
      allMembers,
      isArchivedSelected,
      workspacePermissionsState,
      issuePer,
      intl,
      customFields,
      profile,
    } = this.props;
    const {
      open,
      currentIssue,
      editIssueName,
      issueName,
      task,
      unArchiveFlag,
      unarchiveBtnQuery,
      assessmentDialogOpen,
    } = this.state;

    const { t, getPermission } = this;

    let notifications = this.issueNotifications();

    const permission = getIssuesPermissions(currentIssue, this.props);
    const actionDropDownPermission = getIssuesPermissionsWithoutArchieve(currentIssue, this.props);
    const issueTitle = currentIssue.issuePermission.issueDetail.editIssueName.isAllowEdit;
    const canEditStartDate =
      currentIssue.issuePermission.issueDetail.issuePlanndStartEndDate.isAllowEdit;
    const canEditActualStartDate =
      currentIssue.issuePermission.issueDetail.issueActualstartEndDate.isAllowEdit;
    const canEditStatus = currentIssue.issuePermission.issueDetail.issueStatus.isAllowEdit;
    const canAddIssueTask = currentIssue.issuePermission.issueDetail.issueTask.isAllowEdit;
    const canEditIssueDescription =
      currentIssue.issuePermission.issueDetail.issueDescription.isAllowEdit;
    const canEditSeverity = currentIssue.issuePermission.issueDetail.issueSeverity.isAllowEdit;
    const canEditPriority = currentIssue.issuePermission.issueDetail.issuePriority.isAllowEdit;
    const canEdittype = currentIssue.issuePermission.issueDetail.issueType.isAllowEdit;
    const canAddAsignee = currentIssue.issuePermission.issueDetail.issueAssignee.isAllowAdd;
    const canDeleteAsignee = currentIssue.issuePermission.issueDetail.issueAssignee.isAllowDelete;
    const customFieldPermission =
      (currentIssue.issuePermission && currentIssue.issuePermission.issueDetail.customsFields) ||
      {};
    const issueAssigneeList = currentIssue.issueAssingnees
      ? allMembers.filter(member => {
          return currentIssue.issueAssingnees.indexOf(member.userId) > -1;
        })
      : null;
    const chatPermission = {
      addAttachment:
        currentIssue.issuePermission.issueDetail.issuecomments.issueattachment.isAllowAdd,
      deleteAttachment:
        currentIssue.issuePermission.issueDetail.issuecomments.issueattachment.isAllowDelete,
      downloadAttachment:
        currentIssue.issuePermission.issueDetail.issuecomments.downloadAttachment.cando,
      addComments: currentIssue.issuePermission.issueDetail.issuecomments.cando,
      editComment: currentIssue.issuePermission.issueDetail.issuecomments.comments.isAllowEdit,
      deleteComment: currentIssue.issuePermission.issueDetail.issuecomments.comments.isAllowDelete,
      convertToTask: currentIssue.issuePermission.issueDetail.issuecomments.convertToTask.cando,
      reply: currentIssue.issuePermission.issueDetail.issuecomments.reply.cando,
      replyLater: currentIssue.issuePermission.issueDetail.issuecomments.replyLater.cando,
      showReceipt: currentIssue.issuePermission.issueDetail.issuecomments.showReceipt.cando,
      clearAll: currentIssue.issuePermission.issueDetail.issuecomments.clearAllReplies.cando,
      clearReply: currentIssue.issuePermission.issueDetail.issuecomments.clearReply.cando,
    };
    const docPermission = {
      uploadDocument: currentIssue.issuePermission.issueDetail.issueDocuments.uploadDocument.cando,
      addFolder: currentIssue.issuePermission.issueDetail.issueDocuments.addFolder.cando,
      downloadDocument:
        currentIssue.issuePermission.issueDetail.issueDocuments.downloadDocument.cando,
      viewDocument: currentIssue.issuePermission.issueDetail.issueDocuments.cando,
      deleteDocument: currentIssue.issuePermission.issueDetail.issueDocuments.deleteDocument.cando,
      openFolder: currentIssue.issuePermission.issueDetail.issueDocuments.openFolder.cando,
      renameFolder: currentIssue.issuePermission.issueDetail.issueDocuments.renameFolder.cando,
    };
    const chatConfig = {
      contextUrl: "communication",
      contextView: "Issue",
      contextId: currentIssue.id,
      contextKeyId: "issueId",
      contextChat: "single",
      assigneeLists: issueAssigneeList,
    };
    return (
      <Dialog
        classes={{
          root: classes.taskDetailsDialog,
          paper: classes.dialogPaperCnt,
          scrollBody: classes.dialogCnt,
        }}
        aria-labelledby="form-dialog-title"
        fullWidth={true}
        open={open}
        onEscapeKeyDown={this.closeActionMenu}>
        <DialogTitle classes={{ root: classes.defaultDialogTitle }}>
          {isArchivedSelected ? (
            <div className={classes.unArchiveTaskCnt}>
              <Typography variant="h6" className={classes.unArchiveTaskTxt}>
                This issue is archived. You can't edit this issue.
              </Typography>
              <CustomButton
                btnType="success"
                variant="contained"
                style={{ marginLeft: 10 }}
                onClick={event => {
                  this.openPopUP(event);
                }}>
                Unarchive
              </CustomButton>
            </div>
          ) : null}
          <Grid
            container
            direction="row"
            justify="space-between"
            alignItems="center"
            style={{ padding: "10px 15px" }}>
            <Grid item>
              {!isArchivedSelected && editIssueName && issueTitle ? (
                <ClickAwayListener onClickAway={this.handleIssueClickAway}>
                  <OutlinedInput
                    notched={false}
                    value={issueName}
                    onKeyDown={this.handleIssueNameSave}
                    autoFocus
                    error={this.state.issueNameError}
                    onChange={this.handleIssueNameInput}
                    placeholder={"Add Issue Title"}
                    style={{ width: 500 }}
                    inputProps={{ maxLength: 80 }}
                    classes={{
                      root: classes.outlinedInputCnt,
                      input: classes.outlinedTaskInput,
                      notchedOutline: classes.notchedOutlineCnt,
                      focused: classes.outlineInputFocus,
                    }}
                  />
                </ClickAwayListener>
              ) : (
                <Typography
                  classes={{ h2: classes.taskTitle }}
                  variant="h2"
                  onClick={this.handleIssueNameEdit}>
                  {issueName}
                </Typography>
              )}
            </Grid>
            <Grid item>
              <Grid container direction="row" justify="flex-end" alignItems="center">
                <div className={classes.assigneeListCnt}>
                  {issueAssigneeList ? (
                    <AssigneeDropdown
                      assignedTo={issueAssigneeList}
                      updateAction={this.updateAssignee}
                      isArchivedSelected={isArchivedSelected}
                      obj={currentIssue}
                      closeIssueDetailsPopUp={this.props.closeIssueDetailsPopUp}
                      addPermission={canAddAsignee}
                      deletePermission={canDeleteAsignee}
                    />
                  ) : null}
                </div>
                <Grid item>
                  <IssueActionDropdown
                    issue={currentIssue}
                    selectedColor={currentIssue.colorCode ? currentIssue.colorCode : ""}
                    isUpdated={this.props.isUpdated}
                    UpdateIssue={this.props.UpdateIssue}
                    DeleteIssue={this.props.DeleteIssue}
                    closeActionMenu={this.closeActionMenu}
                    permission={actionDropDownPermission}
                    isArchivedSelected={isArchivedSelected}
                    issuePer={currentIssue.issuePermission}
                    handleExportType={this.props.handleExportType}
                    showSnackBar={this.props.handleExportType}
                  />
                  <NotificationMenu
                    notificationsData={notifications}
                    MarkAllNotificationsAsRead={this.handleAllRead}
                    history={this.props.history}
                  />
                  <IconButton btnType="transparent" onClick={this.props.closeIssueDetailsPopUp}>
                    <CloseIcon htmlColor={theme.palette.secondary.medDark} />
                  </IconButton>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </DialogTitle>

        <DialogContent classes={{ root: classes.defaultDialogContent }}>
          <Grid container direction="row" justify="flex-start" alignItems="stretch">
            <Grid item xs={7} style={{ zIndex: 1, position: "relative" }}>
              <Scrollbars autoHeight autoHeightMin={0} autoHeightMax={calculateHeight() - 160}>
                <div className={classes.taskDetailsLeftCnt}>
                  <Grid
                    container
                    direction="row"
                    justify="space-between"
                    classes={{ container: classes.mainTaskDetailsCnt }}
                    spacing={2}>
                    <Grid item xs={3}>
                      <SelectSearchDropdown /* Task multi select drop down */
                        data={
                          this.generateTaskData
                        } /* function calling for generating task array */
                        label={
                          <FormattedMessage
                            id="issue.detail-dialog.task.label"
                            defaultMessage="Task"
                          />
                        }
                        selectChange={
                          this.handleTaskChange
                        } /** function calling on select task in drop down */
                        selectRemoveValue={this.handleClearTask}
                        type="task"
                        selectedValue={task}
                        placeholder={
                          <FormattedMessage
                            id="issue.detail-dialog.task.placeholder"
                            defaultMessage="Select Task"
                          />
                        }
                        isDisabled={isArchivedSelected || !canAddIssueTask}
                      />
                    </Grid>

                    <Grid item xs={3}>
                      <CustomRangeDatePicker
                        label={
                          <FormattedMessage
                            id="issue.detail-dialog.plan-start-end.label"
                            defaultMessage="Planned Start/End"
                          />
                        }
                        placeholder={intl.formatMessage({
                          id: "issue.detail-dialog.plan-start-end.placeholder",
                          defaultMessage: "Select Date",
                        })}
                        pickerType="input"
                        startDate={currentIssue.startDateString}
                        endDate={currentIssue.dueDateString}
                        startTime={currentIssue.startTime}
                        endTime={currentIssue.dueTime}
                        permission={isArchivedSelected || !canEditStartDate}
                        dateSaveAction={(startDate, endDate, startTime, endTime) =>
                          this.handleDateSave(startDate, endDate, "planned", startTime, endTime)
                        }
                        error={false}
                      />
                    </Grid>
                    <Grid item xs={3}>
                      <CustomRangeDatePicker
                        label={
                          <FormattedMessage
                            id="issue.detail-dialog.actual-start-end.label"
                            defaultMessage="Actual Start/End"
                          />
                        }
                        placeholder={intl.formatMessage({
                          id: "issue.detail-dialog.actual-start-end.placeholder",
                          defaultMessage: "Select Date",
                        })}
                        pickerType="input"
                        error={false}
                        startDate={currentIssue.actualStartDateString}
                        endDate={currentIssue.actualDueDateString}
                        startTime={currentIssue.actualStartTime}
                        endTime={currentIssue.actualDueTime}
                        permission={isArchivedSelected || !canEditActualStartDate}
                        dateSaveAction={(startDate, endDate, startTime, endTime) =>
                          this.handleDateSave(startDate, endDate, "actual", startTime, endTime)
                        }
                      />
                    </Grid>
                    <Grid item xs={3}>
                      <SelectSearchDropdown /* Issue Status select drop down */
                        data={() => {
                          return statusData(theme, classes, intl);
                        }}
                        label={
                          <FormattedMessage
                            id="issue.common.status.label"
                            defaultMessage="Issue Status"
                          />
                        }
                        selectChange={this.handleOptionsSelect}
                        type="status"
                        selectedValue={statusData(theme, classes, intl).find(s => {
                          return s.value == currentIssue.status;
                        })}
                        placeholder={
                          <FormattedMessage
                            id="issue.common.status.placeholder"
                            defaultMessage="Select Status"
                          />
                        }
                        icon={true}
                        isMulti={false}
                        isDisabled={currentIssue.isDeleted || !canEditStatus || isArchivedSelected}
                      />
                    </Grid>
                  </Grid>

                  <Grid
                    container
                    direction="row"
                    justify="flex-start"
                    alignItems="flex-start"
                    classes={{ container: classes.mainTaskDetailsCnt }}
                    spacing={2}>
                    <Grid item xs={3} classes={{ item: classes.taskDetailsFieldCnt }}>
                      <SelectSearchDropdown /* Severity select drop down */
                        data={() => {
                          return severity(theme, classes, intl);
                        }}
                        label={
                          <FormattedMessage
                            id="issue.detail-dialog.severity.label"
                            defaultMessage="Severity"
                          />
                        }
                        selectChange={this.handleOptionsSelect}
                        type="severity"
                        selectedValue={severity(theme, classes, intl).find(s => {
                          return s.value == currentIssue.severity;
                        })}
                        placeholder={
                          <FormattedMessage
                            id="issue.detail-dialog.severity.placeholder"
                            defaultMessage="Select Severity"
                          />
                        }
                        icon={true}
                        isMulti={false}
                        isDisabled={
                          currentIssue.isDeleted || !canEditSeverity || isArchivedSelected
                        }
                      />
                    </Grid>
                    <Grid item xs={3} classes={{ item: classes.taskDetailsFieldCnt }}>
                      <SelectSearchDropdown /* Priority select drop down */
                        data={() => {
                          return priorityData(theme, classes, intl);
                        }}
                        label={
                          <FormattedMessage
                            id="issue.common.priority.label"
                            defaultMessage="Priority"
                          />
                        }
                        selectChange={this.handleOptionsSelect}
                        type="priority"
                        selectedValue={priorityData(theme, classes, intl).find(s => {
                          return s.value == currentIssue.priority;
                        })}
                        placeholder={
                          <FormattedMessage
                            id="issue.common.priority.placeholder"
                            defaultMessage="Select Priority"
                          />
                        }
                        icon={true}
                        isMulti={false}
                        isDisabled={
                          currentIssue.isDeleted || isArchivedSelected || !canEditPriority
                        }
                      />
                    </Grid>
                    <Grid item xs={3} classes={{ item: classes.taskDetailsFieldCnt }}>
                      <SelectSearchDropdown /* Issue Type select drop down */
                        data={() => {
                          return typeData(theme, classes, intl);
                        }}
                        label={
                          <FormattedMessage
                            id="issue.common.issue-type.label"
                            defaultMessage="Issue Type"
                          />
                        }
                        selectChange={this.handleOptionsSelect}
                        type="type"
                        selectedValue={typeData(theme, classes, intl).find(s => {
                          return s.value == currentIssue.type;
                        })}
                        placeholder={intl.formatMessage({
                          id: "issue.common.issue-type.placeholder",
                          defaultMessage: "Select Type",
                        })}
                        icon={true}
                        isMulti={false}
                        isDisabled={currentIssue.isDeleted || isArchivedSelected || !canEdittype}
                      />
                    </Grid>
                  </Grid>
                  {/* <div> */}
                  {/* <ClickAwayListener onClickAway={this.handleClickAwayIssueDescription}> */}
                  {/* <DescriptionEditor
                          id="issueEditor"
                        defaultValue={currentIssue.detail||""}
                        onChange={this.handleIssueDescription}
                        handleClickAway={this.handleClickAwayIssueDescription}
                        placeholder={intl.formatMessage({
                          id: "common.type.label",
                          defaultMessage: "Type something",
                        })}
                      /> */}
                  {/* </ClickAwayListener> */}
                  {/* </div> */}
                  <div style={{ marginBottom: 10 }}>
                    <IssueDescriptionEditor
                      issueData={currentIssue}
                      isUpdated={this.props.isUpdated}
                      permission={canEditIssueDescription}
                      disabled={!isArchivedSelected}
                      view="issue"
                    />
                  </div>
                  <SectionsView
                    type={"issue"}
                    disabled={currentIssue.isDeleted}
                    customFieldData={currentIssue.customFieldData}
                    permission={
                      currentIssue &&
                      currentIssue.issuePermission &&
                      currentIssue.issuePermission.issueDetail.customsFields
                    }
                    customFieldChange={this.customFieldChange}
                    handleEditField={this.handleEditField}
                    handleCopyField={this.handleCopyField}
                    groupId={currentIssue.id}
                    handleClickHideOption={this.handleClickHideOption}
                    permission={customFieldPermission}
                    isSystem={true}
                  />
                  {/* <CustomFieldsDropdownsView
                    groupType={"issue"}
                    disabled={currentIssue.isArchive}
                    permission={customFieldPermission.isAllowEdit}
                    customFieldChange={this.customFieldChange}
                    customFieldData={currentIssue.customFieldData}
                  /> */}
                  {/* <CustomFieldsText
                    groupType={"issue"}
                    disabled={currentIssue.isArchive}
                    permission={customFieldPermission.isAllowEdit}
                    customFieldChange={this.customFieldChange}
                    customFieldData={currentIssue.customFieldData}
                  /> */}
                  {/*if bussiness plan then show add custom field component*/}
                  {teamCanView("customFieldAccess") && (
                    <div className={classes.addCustomFieldCnt}>
                      <CustomFieldView
                        feature="issue"
                        onFieldAdd={this.addCustomFieldCallback}
                        onFieldEdit={this.editCustomFieldCallback}
                        onFieldDelete={() => {}}
                        permission={customFieldPermission}
                        disableAssessment={true}
                      />
                    </div>
                  )}
                </div>
              </Scrollbars>
              {currentIssue.createdByName ? (
                <p className={classes.taskCreationText}>
                  {intl.formatMessage(
                    {
                      id: "common.created-by.label",
                      defaultMessage: `Created by: ${
                        currentIssue.createdByName
                      } - ${helper.RETURN_CUSTOMDATEFORMATFORCHECKLIST(
                        currentIssue.createdDate || new Date()
                      )}`,
                    },
                    {
                      name: currentIssue.createdByName,
                      date: intl.formatDate(currentIssue.createdDate || new Date(), {
                        month: "short",
                        day: "numeric",
                        year: "numeric",
                        hour: "numeric",
                        minute: "numeric",
                        hourCycle: "h12",
                      }),
                    }
                  )}
                </p>
              ) : null}
            </Grid>

            <Grid item xs={5}>
              <SideTabs
                MenuData={currentIssue}
                members={allMembers}
                permission={isArchivedSelected ? !isArchivedSelected : permission}
                issueActivitiesData={this.props.issueActivities}
                issuePer={currentIssue.issuePermission}
                onChatMount={this.onChatMount}
                onChatUnMount={this.onChatUnMount}
                chatConfig={chatConfig}
                chatPermission={chatPermission}
                docConfig={chatConfig}
                docPermission={docPermission}
                selectedTab={0}
                intl={intl}
              />
              {/* // } */}
            </Grid>
          </Grid>
          {unArchiveFlag ? (
            <ActionConfirmation
              open={unArchiveFlag}
              closeAction={this.handleDialogClose}
              cancelBtnText={
                <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
              }
              successBtnText={
                <FormattedMessage
                  id="common.action.un-archive.confirmation.un-archive-button.label"
                  defaultMessage="Unarchive"
                />
              }
              alignment="center"
              iconType="unarchive"
              headingText={
                <FormattedMessage
                  id="common.action.un-archive.confirmation.title"
                  defaultMessage="Unarchive"
                />
              }
              msgText={
                <FormattedMessage
                  id="common.action.un-archive.confirmation.issue.label"
                  defaultMessage="Are you sure you want to unarchive this issue?"
                />
              }
              successAction={this.handleUnArchived}
              btnQuery={unarchiveBtnQuery}
            />
          ) : null}
        </DialogContent>
      </Dialog>
    );
  }
}

IssueDetails.defaultProps = {};

const mapStateToProps = state => {
  return {
    userId: state.profile.data.userId || "",
    allMembers: state.profile.data.member.allMembers || [],
    issues: state.issues.data || [],
    issueNotifications: state.issueNotifications.data || [],
    issueActivities: state.issueActivities.data || [],
    workspacePermissionsState: state.workspacePermissions.data,
    tasks: state.tasks.data,
    issuePer: state.workspacePermissions.data.issue,
    customFields: state.customFields,
    profile: state.profile.data,
  };
};

export default compose(
  withRouter,
  injectIntl,
  withSnackbar,
  withStyles(taskDetailStyles, { withTheme: true }),
  connect(mapStateToProps, {
    UpdateIssue,
    DeleteIssue,
    ArchiveIssue,
    UnarchiveIssue,
    MarkIssueNotificationsAsRead,
    editCustomField,
    dispatchIssue,
    updateCustomFieldData,
    bulkUpdateIssue,
    updateCustomFieldDialogState,
    hideCustomField
  })
)(IssueDetails);
