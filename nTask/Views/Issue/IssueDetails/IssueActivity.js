import React, { useEffect, useState } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import taskDetailStyles from "./styles";
import ReactHtmlParser from 'react-html-parser';
import moment from "moment";
import CustomAvatar from "../../../components/Avatar/Avatar";
import EmptyState from "../../../components/EmptyStates/EmptyState";
import { getActivites } from "../../../redux/actions/issues";
import { Scrollbars } from "react-custom-scrollbars";
import Loader from "../../../components/Loader/Loader";

function ActivityList(props) {
  const { classes, theme, MenuData, members, intl, getActivites } = props;
  const [activities, setActivities] = useState(false);

  useEffect(() => {
    getAllActivites();
    return () => {};
  }, []);

  const getAllActivites = () => {
    getActivites(
      MenuData.id,
      succ => {
        if (succ.data) {
          let activitiesData = succ.data.activitylog.results.reduce((result, cv) => {
            return result.concat(cv.activities);
          }, []);
          desiredActivityLogs(activitiesData);
        }
      },
      fail => {
        desiredActivityLogs([]);
      }
    );
  };

  const desiredActivityLogs = activitiesData => {
    let activiLogs = activitiesData ? activitiesData : [];
    let logs = [];
    activiLogs.map(el => {
      logs.push({
        /** creating the desired array structure for activity logs */
        id: el.activity.id,
        action: el.activity.message,
        ticketId: el.activity.entityId,
        datetime: moment(el.activity.updatedDate).format("LLL"),
        taskName: el.activity.taskTitle,
        taskId: el.activity.taskId,
        userId: el.activity.createdBy,
      });
    });
    setActivities(logs);
  };
  return (
    <>
      {activities.length == 0 ? (
        <div
          style={{
            height: "100%",
            width: "100%",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            marginTop: 50,
          }}>
          <EmptyState
            screenType="search"
            heading={intl.formatMessage({
              id: "common.activities.nologfound",
              defaultMessage: "No Log Found",
            })}
            message={intl.formatMessage({
              id: "common.search-list.message",
              defaultMessage: "No matching results found against your filter criteria.",
            })}
            button={false}
          />
        </div>
      ) : !activities ? (
        
        <Loader />
      ) : (
        <>
          <Scrollbars autoHide={true} autoHeight autoHeightMin={0} autoHeightMax={700}>
            {activities.map((data, i, arr) => {
              let selectedMember = members.find(ele => ele.userId == data.userId) || null;
              return (
                <div className={classes.container}>
                  <div>
                    <CustomAvatar
                      otherMember={{
                        // imageUrl: Avatar,
                        imageUrl: selectedMember ? selectedMember.imageUrl : null,
                        fullName: selectedMember ? selectedMember.fullName : null,
                        lastName: "",
                        email: selectedMember ? selectedMember.email : null,
                        // isOnline: selectedMember ? selectedMember.isOnline : null,
                        // isOwner: selectedMember ? selectedMember.isOwner : null,
                      }}
                      size="xsmall"
                      disableCard
                    />
                  </div>
                  <div className={classes.detail}>
                    <div className={classes.description}>
                      <span className={classes.user}>{`${
                        selectedMember ? selectedMember.fullName : ""
                      } `}</span>
                      {/* <span className={classes.actionDescription}>{data.action} </span> */}
                      <span className={classes.actionDescription}>{ReactHtmlParser(data.action)} </span>
                    </div>
                    <div className={classes.date}>{data.datetime}</div>
                  </div>
                </div>
              );
            })}
          </Scrollbars>
        </>
      )}
    </>
  );
}

export default compose(
  connect(null, {
    getActivites,
  }),
  withStyles(taskDetailStyles, { withTheme: true })
)(ActivityList);
