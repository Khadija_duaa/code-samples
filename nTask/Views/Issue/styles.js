
const dashboardStyles = theme => ({
  dashboardCnt: {
    padding: '14px 0 0 0',
    height: 'calc(100vh - 156px)'
  },
  taskDashboardHeader: {
    margin: "0 0 10px 0",
    padding: "14px 20px 0 28px"
  },
  calendarCnt: {
    padding: "0 20px 0 28px",
    height: '100%'
  },
  className: {},
  toggleContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
    marginLeft: 20
  },
  headerCnt: {
      display: 'flex',
    alignItem: 'center',
    justifyContent: 'space-between'
  },
  listViewHeading: {
    marginRight: 5,
    fontSize: "18px !important",
    color : "#333333",
    fontWeight: 700,
    fontFamily: theme.typography.fontFamilyLato
  },
  count:{
    fontSize:12,
    padding: "1px 6px 1px 5px",
    fontWeight: 400,
    borderRadius: 36,
    // minWidth: 26,
    minHeight: 16,
    border: "1px solid #BFBFBF"
  },
  backArrowIcon: {
    fontSize: "28px !important",
    marginRight: 10,
    cursor: "pointer"
  },
  toggleBtnGroup: {
    borderRadius: 4,
    background: theme.palette.common.white,
    // border: `1px solid ${theme.palette.border.lightBorder}`,
    boxShadow: "none",
    "& $toggleButtonSelected": {
      color: theme.palette.text.secondary,
      backgroundColor: theme.palette.common.white,
      "&:after": {
        background: theme.palette.common.white
      },
      "&:hover":{
        background: theme.palette.common.white
      },
    },
  },
  groupedHorizontal:{
    borderLeft: `1px solid ${theme.palette.border.lightBorder} !important`
  }, 
  toggleButton: {
   height: "auto",
    padding: "4px 20px 5px",
    fontSize: "12px !important",
    fontWeight: theme.palette.fontWeightMedium,
    textTransform: "capitalize",
    "&:hover":{
      background: theme.palette.common.white
    },
    "&[value = 'calendar']": {
      borderLeft: `1px solid ${theme.palette.border.lightBorder}`
    }
  },
  toggleButtonSelected:{},
  advFilterButtonCnt: {
    marginLeft: 8
  },
  advFilterIconSvg:{
    color: theme.palette.secondary.medDark
  },
  advFilterIconSelected: {
      color: theme.palette.text.azure
  },
  advFilterLbl: {
      paddingRight: 4,
      fontSize: "12px !important",
      color: '#333',
  },
  advFilterLblSelected: {
      paddingRight: 4
  },
  quickFilterIconSize:{
    fontSize: "1.5rem !important"
  },
  qckFilterIconSvg:{
    color: theme.palette.secondary.medDark,
  },
  qckFilterIconSelected: {
      color: theme.palette.text.azure
  },
  qckFilterLbl: {
      paddingRight: 4,
      fontSize: "12px !important",
      color: '#333',
  },
  qckFilterLblSelected: {
      paddingRight: 4
  },
  checkname: {
    paddingLeft: 0,
  },
  "@media (max-width: 1024px)": {
    advFilterLbl: {
      display: 'none',
      paddingRight: 0,
    },
    advFilterLblSelected: {
        display: 'none',
    },
    qckFilterLbl: {
      display: 'none',
      paddingRight: 0,
    },
    qckFilterLblSelected: {
        display: 'none',
    },
    checkname: {
      paddingLeft: 0,
    },
  },
  importExportButtonCnt: {
    margin: "0 0 0 8px"
  },
  importExportIcon: {
    // fontSize: 29
  },
  filterDDText: {
    fontSize: "12px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightRegular
  },
  taskGridCnt: {
    padding: "0 30px 0 60px",
    flex: 1
  },
  taskCalendarCnt: {
    padding: "0 30px 0 60px",
    width: "100%"
  },
  taskListCnt:{
    
    width: "100%"
  },
  content: {
    flexGrow: 1,
    paddingBottom: 120,
    background: theme.palette.common.white
    // transition: theme.transitions.create('margin', {
    //   easing: theme.transitions.easing.sharp,
    //   duration: theme.transitions.duration.leavingScreen,
    // }),
    // marginRight: -drawerWidth,
  },
  noFiltercontent:{
    flexGrow: 1,
    paddingBottom: 50
  },
  contentShift: {
    // transition: theme.transitions.create('margin', {
    //   easing: theme.transitions.easing.easeOut,
    //   duration: theme.transitions.duration.enteringScreen,
    // }),
    // marginRight: 0,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    ...theme.mixins.toolbar,
    justifyContent: 'flex-start',
  },
  hide: {
    display: 'none',
  },
  root: {
    display: 'flex',
  },
  qckFfilterIconRefresh:{
    fontSize: "12px !important",
    margin : "0px 5px"
  },
});

export default dashboardStyles;
