import React, { Component, Fragment } from "react";
import { withSnackbar } from "notistack";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import dashboardStyles from "./styles";
import MenuItem from "@material-ui/core/MenuItem";
import selectStyles from "../../assets/jss/components/select";
import combineStyles from "../../utils/mergeStyles";
import ListItemText from "@material-ui/core/ListItemText";
import ArchivedIcon from "@material-ui/icons/Archive";
import IssueList from "./List/issueList";
import SvgIcon from "@material-ui/core/SvgIcon";
import QuickFilterIcon from "../../components/Icons/QuickFilterIcon";
import CustomButton from "../../components/Buttons/CustomButton";
import ImportExportDD from "../../components/Dropdown/ImportExportDD";
import Divider from "@material-ui/core/Divider";
import { saveSortingProjectList } from "../../redux/actions/projects";
import { FetchWorkspaceInfo } from "../../redux/actions/workspace";
import {
  getArchivedData,
  removeIssueQuickFilter,
  updateQuickFilter,
  showAllIssues,
  updateIssueData,
} from "../../redux/actions/issues";
import DefaultDialog from "../../components/Dialog/Dialog";
import AddIssueForm from "../../Views/AddNewForms/AddNewIssueForm";


import { setAppliedFilters } from "../../redux/actions/appliedFilters";
import RingIcon from "@material-ui/icons/RadioButtonUnchecked";
import FlagIcon from "../../components/Icons/FlagIcon";
import { sortingDropdownData, groupingDropdownData } from "./dropdownData";
import PlainMenu from "../../components/Menu/menu";
import MenuList from "@material-ui/core/MenuList";
import { injectIntl, FormattedMessage } from "react-intl";
import { updateArchivedItems } from "../../redux/actions/archivedItems";
import { getIssues } from "../../redux/actions/issues";
import ColumnSelectionDropdown from "../../components/Dropdown/SelectedItemsDropDown";
import isEmpty from "lodash/isEmpty";
import { grid } from "../../components/CustomTable2/gridInstance";
import DefaultTextField from "../../components/Form/TextField";
import debounce from "lodash/debounce";
import IconRefresh from "../../components/Icons/IconRefresh";
import ListLoader from "../../components/ContentLoader/List";
import { doesFilterPass } from "./List/IssueFilter/issueFilter.utils";
import {
  generateTaskDropdownData,
  priorityData,
  severity as severityData,
  statusData,
  typeData,
} from "../../helper/issueDropdownData";
import { issueDetailDialogState } from "../../redux/actions/allDialogs";
import queryString from "query-string";
import CustomCalendar from "../../components/Calendar/Calendar.cmp";
import RecurrenceIcon from "../../components/Icons/RecurrenceIcon";
import uniqBy from "lodash/uniqBy";
import moment from "moment";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import ToggleButton from "@material-ui/lab/ToggleButton";
import PlannedVsActualDropDown from "./PlannedVsActualDropDown";
import helper from "../../helper";
import { CanAccessFeature } from "../../components/AccessFeature/AccessFeature.cmp";
import { getTasks } from "../../redux/actions/tasks";
import { getProjects } from "../../redux/actions/projects";
class IssueDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filteredIssues: [],
      quickFilters: [],
      open: false,
      placement: "bottom-start",
      anchorEl: "",
      listView: true,
      isArchived: false,
      openFilterSidebar: false,
      multipleFilters: "",
      clearlastState: false,
      total: 0,
      loadMore: 0,
      isChanged: false,
      sortObj: {},
      groupingDropdownData,
      selectedGroup: {},
      sortingDropDownData: sortingDropdownData,
      refreshBtnQuery: "",
      isLoading: true,
      activeView: "list",
      calenderDateType: "Creation Date",
      selectableIssue: false,
      startDate: new Date(),
      endDate: new Date(),
    };

    this.child = React.createRef();
    this.renderListView = this.renderListView.bind(this);
    this.handleExportType = this.handleExportType.bind(this);
    this.closeSnakBar = this.closeSnakBar.bind(this);
    this.searchFilter = this.searchFilter.bind(this);
    this.searchFilterApplied = this.searchFilterApplied.bind(this);
    this.clearFilter = this.clearFilter.bind(this);
    this.returnCount = this.returnCount.bind(this);
    this.handleChangeState = this.handleChangeState.bind(this);
    this.throttleHandleSearch = debounce(this.throttleHandleSearch, 1000);
  }
  openIssueDetail = () => {
    const {
      dialogsState: { issueDetailDialog },
    } = this.props;
    let searchQuery = this.props.history.location.search; //getting issue id in the url
    if (searchQuery) {
      let issueId = queryString.parseUrl(searchQuery).query.issueId; //Parsing the url and extracting the issue id using query string
      let issueFound = this.props.issues.find(item => item.id == issueId);
      if (issueFound && issueId) {
        !issueDetailDialog.id &&
          this.props.issueDetailDialogState(null, {
            id: issueId,
            afterCloseCallBack: () => { },
          });
      } else {
        this.handleExportType("Oops! Issue not found.", "error");
        this.props.history.push("/issues");
      }
    }
  };
  getAllIssues = () => {
    this.props.getIssues(
      null,
      null,
      //success
      () => {
        setTimeout(() => {
          this.setState({ isLoading: false });
          this.openIssueDetail();
        }, 2000);
      }
    );
  };
  componentDidUpdate() {
    this.openIssueDetail();
  }

  componentDidMount() {
    this.getAllIssues();
  }
  returnCount(total, loadMore) {
    this.setState({ total, loadMore });
  }
  handleChangeState = () => {
    this.setState({ isChanged: false });
  };
  clearFilter() {
    this.setState({ multipleFilters: "", clearlastState: false });
  }
  searchFilterApplied() {
    this.setState({ multipleFilters: "" });
  }
  searchFilter(data) {
    this.setState({ multipleFilters: data, isChanged: true });
  }
  closeSnakBar() {
    this.setState({ showSnakBar: false });
  }

  handleDrawerOpen = () => {
    this.setState({ openFilterSidebar: true });
  };

  handleDrawerClose = () => {
    this.setState({ openFilterSidebar: false });
  };

  handleExportType(snackBarMessage, type) {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  }
  handleImportedIssues = () => {
    this.getAllIssues();
  };

  renderListView() {
    this.setState({ listView: true });
  }
  handleClick(event, placement) {
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }
  handleClose = () => {
    if (this.anchorEl.contains(event.target)) {
      return;
    }
    this.setState({ open: false });
  };
  handleRefresh = () => {
    // this.props.FetchWorkspaceInfo("", () => {});
    // this.getAllIssues();
    this.setState({ refreshBtnQuery: "progress", isLoading: true }, () => {
      this.props.getProjects(null, null, succ => {
        this.props.getTasks(null, null, succ => {
          this.props.getIssues(
            null,
            null,
            //success
            () => {
              this.setState({ refreshBtnQuery: "", isLoading: false });
              this.openIssueDetail();
            },
            () => {
              this.setState({ refreshBtnQuery: "", isLoading: false });
              this.handleExportType("Oops! Server throws Error.", "error");
            }
          );
        })
      })
    });
  };
  handleRefreshList = () => {
    this.setState({ refreshBtnQuery: "progress", isLoading: true }, () => {
      this.props.getIssues(
        null,
        null,
        () => {
          this.setState({ refreshBtnQuery: "", isLoading: false });
          this.handleExportType("Grid Refreshed Successfully!", "success");
        },
        () => {
          this.setState({ refreshBtnQuery: "", isLoading: false });
          this.handleExportType("Oops! Server throws Error.", "error");
        }
      );
    });
  };
  handleViewSwitch = (event, view) => {
    if (view) {
      this.setState({ activeView: view });
    }
  };
  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };

  // show all task function

  handleShowAllIssues = () => {
    this.props.showAllIssues({});
    setTimeout(() => {
      !isEmpty(grid.grid) && grid.grid.redrawRows();
    }, 0);
  };
  componentWillUnmount() {
    this.handleShowAllIssues({});
  }

  handleSelectClick = (value, type) => {
    const { quickFilters } = this.props;
    if (quickFilters.Archived) {
      this.props.removeIssueQuickFilter(type);
      setTimeout(() => {
        !isEmpty(grid.grid) && grid.grid.redrawRows();
      }, 0);
      return;
    }
    if (type == "unassignedIssues") {
      const filterObj = { unassignedIssues: { type: type, selectedValues: [] } };
      this.props.updateQuickFilter(filterObj);
      return
    }
    if (type == "Archived") {
      const filterObj = { [value]: { type: value, selectedValues: [] } };
      this.props.getArchivedData(4, () => {
        this.props.updateQuickFilter(filterObj);
        setTimeout(() => {
          !isEmpty(grid.grid) && grid.grid.redrawRows();
        }, 0);
      });
    } else {
      const filterObj = !quickFilters[type]
        ? {
          ...quickFilters,
          [type]: {
            type: "",
            selectedValues: [value],
          },
        }
        : {
          ...quickFilters,
          [type]: {
            type: "",
            selectedValues: quickFilters[type]["selectedValues"].includes(value)
              ? quickFilters[type]["selectedValues"].filter(item => item !== value)
              : [...quickFilters[type]["selectedValues"], ...[value]],
          },
        };
      const objKeys = Object.keys(filterObj);
      const isFilterEmpty = objKeys.every(item => filterObj[item]["selectedValues"].length == 0);
      if (isFilterEmpty) {
        this.props.showAllIssues({});
      } else {
        this.props.updateQuickFilter(filterObj);
      }
    }
  };
  handleUpdateIssueDates = data => {
    const { actualStartTime, actualDueTime, startTime, dueTime } = data.obj;

    const startDate = helper.RETURN_CUSTOMDATEFORMAT(data.start);
    const endDate = helper.RETURN_CUSTOMDATEFORMAT(data.end);
    let obj;
    let key;
    switch (this.state.calenderDateType) {
      case "Planned Start/End":
        obj = {
          startDate: startDate,
          startTime: startTime || "12:00 AM",
          dueDate: endDate,
          dueTime: dueTime || "12:00 AM",
        };
        break;
      case "Actual Start/End":
        obj = {
          actualStartDate: startDate,
          actualStartTime: actualStartTime || "12:00 AM",
          actualDueDate: endDate,
          actualDueTime: actualDueTime || "12:00 AM",
        };
        break;
      default:
    }
    this.props.updateIssueData({ issue: data.obj, obj }, null, () => { });
  };
  getTranslatedLabel = name => {
    var trans = { id: name, defaultMessage: name };
    switch (name) {
      case "Open":
        trans = { id: "issue.common.status.drop-down.open", defaultMessage: name };
        break;
      case "Closed":
        trans = { id: "issue.common.status.drop-down.close", defaultMessage: name };
        break;
      case "Critical":
        trans = { id: "issue.common.severity.dropdown.critical", defaultMessage: name };
        break;
      case "Major":
        trans = { id: "issue.common.severity.dropdown.major", defaultMessage: name };
        break;
      case "Minor":
        trans = { id: "issue.common.severity.dropdown.minor", defaultMessage: name };
        break;
      case "Moderate":
        trans = { id: "issue.common.severity.dropdown.moderate", defaultMessage: name };
        break;
      case "High":
        trans = { id: "issue.common.priority.dropdown.high", defaultMessage: name };
        break;
      case "Low":
        trans = { id: "issue.common.priority.dropdown.low", defaultMessage: name };
        break;
      case "Medium":
        trans = { id: "issue.common.priority.dropdown.medium", defaultMessage: name };
        break;

      case "Agreed":
        trans = { id: "risk.common.status.dropdown.agreed", defaultMessage: name };
        break;
      case "Identified":
        trans = { id: "risk.common.status.dropdown.identified", defaultMessage: name };
        break;
      case "In Review":
        trans = { id: "risk.common.status.dropdown.in-review", defaultMessage: name };
        break;
      case "Rejected":
        trans = { id: "risk.common.status.dropdown.rejected", defaultMessage: name };
        break;
    }
    return this.props.intl.formatMessage(trans);
  };
  onColumnHide = obj => {
    if (grid.grid) {
      grid.grid.columnModel.applyColumnState({
        state: [
          {
            colId: obj.id,
            hide: obj.hide,
            rowGroup: obj.rowGroup,
          },
        ],
      });
    }
  };
  handleCalendarDatesType = (event, type) => {
    this.setState({ calenderDateType: type });
  };
  throttleHandleSearch = data => {
    grid.grid && grid.grid.setQuickFilter(data);
  };
  //handle task search
  handleSearch = e => {
    this.throttleHandleSearch(e.target.value);
    // this.setState({searchQuery: e.target.value});
  };
  //Close issue detail
  closeIssueDetailsPopUp = () => {
    this.props.issueDetailDialogState(null, {
      id: "",
    });
  };
  onDoubleClickEvent = event => {
    this.props.issueDetailDialogState(null, {
      id: event.id,
      afterCloseCallBack: () => {
        this.closeIssueDetailsPopUp();
      },
    });
  };
  addNewIssue = ({ start, end }) => {
    const { issuePer } = this.props;
    if (start && end && issuePer?.creatIssue?.cando) {
      this.setState({
        startDate: start,
        endDate: end,
        selectableIssue: true,
      });
    }
  };
  handleCloseMeeting = () => {
    this.setState({
      startDate: "",
      endDate: "",
      selectableIssue: false,
    });
  };
  getDate = issue => {
    const { calenderDateType } = this.state;
    const datesObj = {
      "Actual Start/End": {
        start: issue.actualStartDate || issue.actualDueDate,
        // end: issue.actualDueDate || issue.actualStartDate,
        end: moment(issue.actualDueDate).format("MM/DD/YYYY") || moment(issue.actualStartDate).format("MM/DD/YYYY"),
      },
      "Planned Start/End": {
        start: issue.startDate || issue.dueDate,
        // end: issue.dueDate || issue.startDate,
        end: moment(issue.dueDate).format("MM/DD/YYYY") || moment(issue.startDate).format("MM/DD/YYYY"),
      },
      "Creation Date": { start: issue.createdDate, end: issue.createdDate },
    };
    return datesObj[calenderDateType];
  };
  render() {
    const {
      classes,
      theme,
      quote,
      workspaces,
      loggedInTeam,
      intl,
      quickFilters,
      issuePer,
    } = this.props;
    const {
      alignment,
      openFilterSidebar,
      multipleFilters,
      listView,
      groupingDropdownData,
      filteredIssues,
      refreshBtnQuery,
      selectedGroup = {},
      isLoading,
      activeView,
      calenderDateType,
      endDate,
      startDate,
    } = this.state;

    const Severity = severityData(theme, classes, intl);
    const Priority = priorityData(theme, classes, intl);
    const isArchived = false;
    let selectedItems = [];
    let activeQuickFilterKeys = Object.keys(quickFilters).length
      ? Object.keys(quickFilters)
      : "All";
    if (Array.isArray(activeQuickFilterKeys)) {
      if (activeQuickFilterKeys[0] == "Archived") {
        activeQuickFilterKeys = "Archived";
      } else {
        activeQuickFilterKeys.map(item => {
          selectedItems = [...selectedItems, ...quickFilters[item]["selectedValues"]];
        });
        activeQuickFilterKeys = selectedItems.length
          ? `${selectedItems[0]} ${selectedItems.length > 1 ? `& +${selectedItems.length - 1}` : ""
          }`
          : "All";
      }
    }

    const eventStyle = event => {
      if (event.taskColor) {
        if (event.borderColor) {
          return {
            style: {
              background: event.taskColor,
              border: event.borderColor,
              color: event.color,
              borderRadius: event.borderRadius,
            },
            className: event.className,
          };
        } else {
          return {
            style: {
              background: event.taskColor,
              borderRadius: event.borderRadius,
            },
          };
        }
      } else {
        return {};
      }
    };
    const issueStatusColors = statusData(theme, classes).reduce((r, cv) => {
      r[cv.value] = cv.color;
      return r;
    }, {});
    const issuesData = this.props.issues.map(x => {
      return {
        id: x.id,
        title: x.title,
        allDay: true,
        start: this.getDate(x).start,
        end: this.getDate(x).end,
        taskColor:
          x.colorCode && x.colorCode !== "#ffffff" ? x.colorCode : issueStatusColors[x.status],
        obj: x,
      };
    });
    const EventRenderer = ({ event }) => {
      return <span>{event.title}</span>;
    };
    const plannedDatePer = issuePer?.issueDetail?.issuePlanndStartEndDate?.isAllowEdit || true;
    const actualDatePer = issuePer?.issueDetail?.issueActualstartEndDate?.isAllowEdit || true;
    return (
      <>
        <div style={{ height: "100%" }}>
          <div className={classes.taskDashboardHeader}>
            {!isArchived ? (
              <div className={classes.headerCnt}>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    marginRight: "16px",
                    flex: 1,
                  }}>
                  <Typography variant="h1" className={classes.listViewHeading}>
                    Issues
                  </Typography>
                  <span className={classes.count}>
                    {this.props.issues.filter(t => doesFilterPass({ data: t })).length}
                  </span>
                  <div className={classes.toggleContainer}>
                    <ToggleButtonGroup
                      value={activeView}
                      exclusive
                      onChange={this.handleViewSwitch}
                      classes={{ root: classes.toggleBtnGroup }}>
                      <ToggleButton
                        value="list"
                        // onClick={this.renderListView}
                        classes={{
                          root: classes.toggleButton,
                          selected: classes.toggleButtonSelected,
                        }}>
                        <FormattedMessage id="common.list.label" defaultMessage="List" />
                      </ToggleButton>

                      <ToggleButton
                        value="calendar"
                        // onClick={this.renderCalendarView}

                        classes={{
                          root: classes.toggleButton,
                          selected: classes.toggleButtonSelected,
                        }}>
                        <FormattedMessage id="common.calendar.label" defaultMessage="Calendar" />
                      </ToggleButton>
                    </ToggleButtonGroup>
                  </div>
                </div>
                {activeView == "calendar" && (
                  <PlannedVsActualDropDown
                    handleTaskbarsType={this.handleCalendarDatesType}
                    calenderDateType={calenderDateType}
                  />
                )}
                {activeView == "list" && (
                  <>
                    <DefaultTextField
                      fullWidth={false}
                      // errorState={forgotEmailError}
                      error={false}
                      // errorMessage={forgotEmailMessage}
                      formControlStyles={{ width: 250, marginBottom: 0, marginRight: 10 }}
                      defaultProps={{
                        id: "issueListSearch",
                        onChange: this.handleSearch,
                        placeholder: "Search Issues",
                        autoFocus: true,
                        inputProps: { maxLength: 150, style: { padding: "9px 14px" } },
                      }}
                    />
                  </>
                )}
                {activeView !== "calendar" && (
                  <>
                    <CustomButton
                      onClick={this.handleRefreshList}
                      query={refreshBtnQuery}
                      style={{
                        // padding: customFilterSelect ? "4px 8px 4px 4px" : "3px 8px 3px 4px",
                        padding: "4px 8px 4px 8px",
                        borderRadius: "4px",
                        display: "flex",
                        justifyContent: "space-between",
                        minWidth: "auto",
                        whiteSpace: "nowrap",
                        marginRight: 10,
                      }}
                      btnType={"white"}
                      variant="contained">
                      <SvgIcon viewBox="0 0 12 12.015" className={classes.qckFfilterIconRefresh}>
                        <IconRefresh />
                      </SvgIcon>
                    </CustomButton>

                    <CustomButton
                      onClick={event => {
                        this.handleClick(event, "bottom-end");
                      }}
                      id="taskQuickFilterButton"
                      buttonRef={node => {
                        this.anchorEl = node;
                      }}
                      style={{
                        // padding: customFilterSelect ? "4px 8px 4px 4px" : "3px 8px 3px 4px",
                        padding: "3px 8px 3px 4px",
                        borderRadius: "4px",
                        display: "flex",
                        justifyContent: "space-between",
                        minWidth: "auto",
                        whiteSpace: "nowrap",
                      }}
                      // btnType={customFilterSelect ? "lightBlue" : "white"}
                      btnType={!isEmpty(quickFilters) ? "lightBlue" : "white"}
                      variant="contained">
                      <SvgIcon
                        classes={{root: classes.quickFilterIconSize}}
                        viewBox="0 0 24 24"
                        className={
                          true ? classes.qckFfilterIconSelected : classes.qckFilterIconSvg
                        }>
                        <QuickFilterIcon />
                      </SvgIcon>
                      <span
                        className={
                          !isEmpty(quickFilters)
                            ? classes.qckFilterLblSelected
                            : classes.qckFilterLbl
                        }>
                        <FormattedMessage id="common.show.label" defaultMessage="Show" /> :{" "}
                      </span>
                      <span className={classes.checkname}>{activeQuickFilterKeys}</span>
                    </CustomButton>
                    <PlainMenu
                      open={this.state.open}
                      closeAction={this.handleClose}
                      placement="bottom-start"
                      anchorRef={this.anchorEl}
                      style={{ width: 280 }}
                      offset="0 15px">
                      <MenuList disablePadding>
                        <MenuItem
                          className={`${classes.statusMenuItemCnt} ${isEmpty(quickFilters) ? classes.selectedValue : ""
                            }`}
                          classes={{
                            root: classes.customRootMenuItem,
                            selected: classes.statusMenuItemSelected,
                          }}
                          value="Show All"
                          onClick={this.handleShowAllIssues}>
                          <ListItemText
                            primary={
                              <FormattedMessage id="common.show.all" defaultMessage="Show All" />
                            }
                            classes={{ primary: classes.plainItemText }}
                          />
                        </MenuItem>

                        {!quickFilters["Archived"] ?
                          <>
                            <CanAccessFeature group='issue' feature='severity'>
                              <Divider />
                              <MenuItem
                                disableRipple={true}
                                classes={{
                                  root: classes.menuHeadingItem
                                }}>
                                <FormattedMessage
                                  id="issue.detail-dialog.severity.label"
                                  defaultMessage="Severity"
                                />
                              </MenuItem>
                              {Severity.map(item => (
                                <MenuItem
                                  key={item.value}
                                  value={item.value}
                                  disableRipple={true}
                                  className={`${classes.statusMenuItemCnt} 
                          ${!isEmpty(quickFilters) &&
                                      quickFilters?.severity?.selectedValues.includes(item.value)
                                      ? classes.selectedValue
                                      : ""
                                    }`}
                                  classes={{
                                    selected: classes.statusMenuItemSelected,
                                    root: classes.customRootMenuItem
                                  }}
                                  onClick={() => this.handleSelectClick(item.value, "severity")}>
                                  <RingIcon
                                    htmlColor={item.color}
                                    classes={{ root: classes.statusIcon }}
                                  />
                                  <ListItemText
                                    primary={item.label}
                                    classes={{ primary: classes.statusItemText }}
                                  />
                                </MenuItem>
                              ))}
                            </CanAccessFeature>
                            <CanAccessFeature group='issue' feature='priority'>
                              <Divider />
                              <MenuItem classes={{ root: classes.menuHeadingItem}}>
                                {this.props.intl.formatMessage({
                                  id: "project.gant-view.columns.priority",
                                  defaultMessage: "Priority",
                                })}
                              </MenuItem>
                              {Priority.map(item => (
                                <MenuItem
                                  key={item.name}
                                  value={item.id}
                                  disableRipple={true}
                                  className={`${classes.statusMenuItemCnt} 
                          ${!isEmpty(quickFilters) &&
                                      quickFilters?.priority?.selectedValues.includes(item.value)
                                      ? classes.selectedValue
                                      : ""
                                    }
                          `}
                                  classes={{
                                    root: classes.customRootMenuItem,
                                    selected: classes.statusMenuItemSelected,
                                  }}
                                  onClick={() => this.handleSelectClick(item.value, "priority")}>
                                  <SvgIcon
                                    viewBox="0 0 24 32.75"
                                    htmlColor={item.color}
                                    classes={{ root: classes.priorityIcon }}>
                                    <FlagIcon />
                                  </SvgIcon>
                                  <ListItemText
                                    primary={item.label}
                                    classes={{ primary: classes.statusItemText }}
                                  />
                                </MenuItem>
                              ))}
                            </CanAccessFeature>
                          </> : null}
                        <Divider />
                        <MenuItem
                          className={`${classes.statusMenuItemCnt} ${quickFilters?.unassignedIssues ? classes.selectedValue : ""
                            }`}
                          classes={{
                            selected: classes.statusMenuItemSelected,
                            root: classes.rootUnassigned
                          }}
                          value="Unassigned Issues"
                          onClick={() => this.handleSelectClick('', "unassignedIssues")}>
                          <ListItemText
                            primary={'Unassigned issues'}
                            classes={{ primary: classes.plainItemText }}
                          />
                        </MenuItem>
                        <MenuItem
                          value={"Archived"}
                          disableRipple={true}
                          classes={{ root: classes.customRootArchived }}
                          onClick={() => this.handleSelectClick("Archived", "Archived")}>
                          <ArchivedIcon
                            htmlColor={theme.palette.secondary.light}
                            classes={{
                              root: classes.selectHighlightItemIconArchive,
                            }}
                          />
                          <FormattedMessage
                            id="common.archived.archived"
                            defaultMessage="Archived"
                          />
                        </MenuItem>
                      </MenuList>
                    </PlainMenu>
                  </>
                )}
                {!quickFilters["Archived"] && (
                  <ImportExportDD
                    handleExportType={this.handleExportType}
                    filterList={quickFilters}
                    multipleFilters={multipleFilters}
                    handleRefresh={this.handleRefresh}
                    ImportExportType={"issue"}
                    data={filteredIssues}
                  // callBack={this.handleImportedIssues}
                  />
                )}

                {activeView == "list" && (
                  <ColumnSelectionDropdown
                    feature={"issue"}
                    onColumnHide={this.onColumnHide}
                    hideColumns={["matrix"]}
                    btnProps={{
                      style: {
                        border: "1px solid #dddddd",
                        padding: "5px 10px",
                        borderRadius: 4,
                        marginLeft: 10,
                      },
                    }}
                  />
                )}
              </div>
            ) : null}
          </div>
          {activeView == "list" ?
            <> 
              {isLoading ? <ListLoader style={{ paddingLeft: 32 }} /> : <IssueList />}
            </>
            : activeView == "calendar" ?
              <div className={classes.calendarCnt}>
                <CustomCalendar
                  onDoubleClickEvent={this.onDoubleClickEvent}
                  updateDates={this.handleUpdateIssueDates}
                  data={issuesData}
                  // draggable={false}
                  EventRenderer={EventRenderer}
                  eventPropGetter={eventStyle}
                  onSelectSlot={this.addNewIssue}
                  draggableAccessor={event => {
                    /** if calendar view is planned date then check planned date permission otherwise check actual date permission */
                    return calenderDateType.includes("Planned") ? plannedDatePer : actualDatePer;
                  }}
                />
              </div> : ''
          }
        </div>
        <DefaultDialog
          title="Add Issue"
          open={this.state.selectableIssue}
          onClose={this.handleCloseMeeting}>
          <AddIssueForm
            view="Header-Menu"
            closeAction={this.handleCloseMeeting}
            preFilledData={{
              startDate,
              endDate,
              moreDetails: true,
              calenderDateType,
            }}
          />
        </DefaultDialog>
      </>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    loggedInTeam: state.profile.data.loggedInTeam,
    appliedFilters: state.appliedFilters,
    itemOrderState: state.itemOrder.data,
    issues: state.issues.data || [],
    members:
      state.profile && state.profile.data && state.profile.data.member
        ? state.profile.data.member.allMembers
        : [],
    workspaces: state.profile.data.workspace,
    issuePer: state.workspacePermissions.data.issue,
    projects: state.projects.data || [],
    quickFilters: state.issues.quickFilters || {},
    dialogsState: state.dialogStates,
  };
};

export default compose(
  withRouter,
  withSnackbar,
  injectIntl,
  withStyles(combineStyles(dashboardStyles, selectStyles), {
    withTheme: true,
  }),
  connect(mapStateToProps, {
    FetchWorkspaceInfo,
    setAppliedFilters,
    saveSortingProjectList,
    updateArchivedItems,
    getIssues,
    getProjects,
    getTasks,
    getArchivedData,
    updateQuickFilter,
    removeIssueQuickFilter,
    showAllIssues,
    issueDetailDialogState,
    updateIssueData,
  })
)(IssueDashboard);
