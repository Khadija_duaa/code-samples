export const columns = {
  none: {
    key: "",
    name: "None"
  },
  id_C: {
    key: "issueId",
    name: "ID"
  },
  issueTitle_C: {
    key: "title",
    name: "Title"
  },
  issueStatus_C: {
    key: "status",
    name: "Status"
  },
  actualStartDate_C: {
    key: "actualStartDate",
    name: "Actual Start/End"
  },
  plannedStartDate_C: {
    key: "startDate",
    name: "Planned Start/End"
  },
  task_C: {
    key: "task",
    name: "Task"
  },
  severity_C: {
    key: "severity",
    name: "Severity"
  },
  priority_C: {
    key: "priority",
    name: "Priority"
  },
  assignees_C: {
    key: "assignees",
    name: "Assignee"
  },
  type_C: {
    key: "type",
    name: "Type"
  },
  updatedDate_C: {
    key: "updatedDate",
    name: "Last Updated"
  },
  createdBy_C:{
    key: "createdBy",
    name: "Created By"
  },
  createdDate_C: {
    key: "createdDate",
    name: "Creation Date"
  },
  label_C: {
    key: "label",
    name: "Label"
  },
  category_C: {
    key: "category",
    name: "Category"
  },
  projectName:{
    key:"project",
    name:"Project"
  }
}