const getIssuesPermissions = (issueData, props) => {
  const permission = issueData
      ? issueData.isOwner
        ? true
        : props.workspacePermissionsState &&
          props.workspacePermissionsState.issue
        ? props.workspacePermissionsState.issue
        : false
      : false
  return permission;
};

const getIssuesPermissionsWithoutArchieve = (issueData, props) => {
  const permission = issueData
    ? issueData.isOwner
      ? true
      : props.workspacePermissionsState && props.workspacePermissionsState.issue
      ? props.workspacePermissionsState.issue
      : false
    : false;
  return permission;
};

const getEditPermissionsWithArchieve = (issueData, permission, key) => {
  if (permission && Object.keys(permission).length && issueData && key) {
    return permission[key] && (permission[key].edit || permission[key].add)  ? true : false;
  }
  return permission;
};

const getCompletePermissionsWithArchieve = (issueData, permission, key) => {
  if (permission && Object.keys(permission).length && issueData && key) {
    return permission[key];
  }
  return permission;
};
export function canView(issue, permissions, key){
  // return issue.isOwner ? true : permissions[key]
  return  true 
}
export function canDo(issue, permissions, key){
  return  true 
// return issue.isOwner ? true : permissions[key]
}
export function canAdd(issue, permissions, key){
  return  true 
// return issue.isOwner ? true : permissions[key].add

}
export function canEdit(issue, permissions, key){
  return  true 
// return issue.isOwner ? true : permissions[key].edit

}
export function canDelete(issue, permissions, key){
  return  true 
// return issue.isOwner ? true : permissions[key].delete
}
export {
  getEditPermissionsWithArchieve,
  getIssuesPermissions,
  getIssuesPermissionsWithoutArchieve,
  getCompletePermissionsWithArchieve
};
