import React, { Component } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import "../../../assets/css/richTextEditor.css";
import withStyles from "@material-ui/core/styles/withStyles";
import taskDetailStyles from "./style";
import { injectIntl } from "react-intl";
import DescriptionEditor from "../../../components/TextEditor/CustomTextEditor/DescriptionEditor/DescriptionEditor";
import CustomFieldLabelCmp from "../../../components/CustomFieldsViews/CustomFieldLabel.cmp";

class IssueDescriptionEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editorState: props.issueData.description || "",
    };
  }
  componentDidMount() {
    let members =
      this.props.profileState && this.props.profileState.data && this.props.profileState.data.member
        ? this.props.profileState.data.member.allMembers
        : [];
    let memberList = members.map(x => {
      return {
        text: x.userName,
        value: x.userName,
        userId: x.userId,
      };
    });
    this.setState({ members: memberList });
  }

  componentDidUpdate(prevProps) {
    if (JSON.stringify(prevProps.issueData) !== JSON.stringify(this.props.issueData)) {
      this.setState({
        editorState: this.props.issueData.description || "",
      });
    }
  }

  handleIssueDescription = editorState => {
    this.setState({
      editorState: editorState.html,
    });
  };
  handleClickAwayIssueDescription = () => {
    const { issueData } = this.props;
    const { editorState } = this.state;
    if (editorState != issueData.description) {
      issueData.description = editorState;
      this.props.isUpdated(issueData, response => {});
    }
  };

  render() {
    const {
      classes,
      theme,
      permission,
      disabled,
      intl,
      handleSelectHideOption = () => {},
      hideOperation = true
    } = this.props;
    const { EditMode, editorState } = this.state;
    // const html = draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()));

    return (
      <div className={classes.editorContainer}>
        <CustomFieldLabelCmp
          label={"Description"}
          iconType={"textarea"}
          handleDelete={() => {}}
          handleEdit={() => {}}
          handleCopy={() => {}}
          handleSelectHideOption={handleSelectHideOption}
          deleteOperation={false}
          editOperation={false}
          copyOperation={false}
          hideOperation={hideOperation}
          customIconProps={{
            color: "#999",
          }}
        />
        <div style={{ flex: 1, padding: "0px 8px 0px 4px" }}>
          <DescriptionEditor
            id="issueEditor"
            defaultValue={editorState}
            onChange={this.handleIssueDescription}
            handleClickAway={this.handleClickAwayIssueDescription}
            placeholder={intl.formatMessage({
              id: "common.type.label",
              defaultMessage: "Type something",
            })}
            disableEdit={permission}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    profileState: state.profile,
  };
};

export default compose(
  withRouter,
  injectIntl,
  withStyles(taskDetailStyles, { withTheme: true }),
  connect(mapStateToProps, {})
)(IssueDescriptionEditor);
