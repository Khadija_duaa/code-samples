import React, { useState, useMemo, useRef, useEffect } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { withSnackbar } from "notistack";
import { connect, useDispatch } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style.js";
import { FormattedMessage, injectIntl } from "react-intl";
import {
  UpdateIssue,
  DeleteIssue,
  ArchiveIssue,
  UnarchiveIssue,
  MarkIssueNotificationsAsRead,
  dispatchIssue,
  bulkUpdateIssue,
  UpdateIssueCustomField,
  updateIssueData,
} from "../../../redux/actions/issues";
import {
  editCustomField,
  updateCustomFieldData,
  hideCustomField,
} from "../../../redux/actions/customFields";
import { updateCustomFieldDialogState } from "../../../redux/actions/allDialogs";
import helper from "../../../helper";
import DetailsDialog from "../../../components/Dialog/DetailsDialog/DetailsDialog";
import { issueDetailDialogState } from "../../../redux/actions/allDialogs";
import { getFilteredIssues } from "../IssueFilter/FilterUtils";
import SideTabs from "../IssueDetails/SideTabs";
import IssueActionDropdown from "./IssueActionDropDown";
import NotificationMenu from "../../../components/Header/NotificationsMenu";
import AssigneeDropdown from "../../../components/Dropdown/AssigneeDropdown";
import moment from "moment";
import EditableTextField from "../../../components/Form/EditableTextField";
import SectionsView from "../../../components/Sections/sections.view";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";
import CustomFieldView from "../../CustomFieldSettings/CustomFields.view";
import cloneDeep from "lodash/cloneDeep";
import RemoveIcon from "@material-ui/icons/Remove";
import AddIcon from "@material-ui/icons/Add";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import IssueDateDropDown from "./IssueDateDropdown";
import CustomFieldLabelCmp from "../../../components/CustomFieldsViews/CustomFieldLabel.cmp";
import SelectSearchDropdown from "../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { priorityData, statusData, typeData, severity } from "../../../helper/issueDropdownData";
import { generateTaskData, generateProjectData } from "../../../helper/generateSelectData";
import IssueDescriptionEditor from "./IssueDetailsDescription";
import ProjectPermissionSelector from "../../../redux/selectors/projectPermissionSelector";
import isUndefined from "lodash/isUndefined";
import useUpdateIssueHook from "../../../helper/customHooks/issues/updateIssue";
import { grid } from "../../../components/CustomTable2/gridInstance";
import { getCustomFields } from "../../../helper/customFieldsData";
import { CanAccess, TeamCanAccessFeature } from "../../../components/AccessFeature/AccessFeature.cmp.js";
import { updateFeatureAccessPermissions } from "../../../redux/actions/team";
import { excludeOffdays, getCalendar } from "../../../helper/dates/dates.js";

function NewIssueDetails({
  classes,
  theme,
  intl,
  UpdateIssue,
  DeleteIssue,
  MarkIssueNotificationsAsRead,
  editCustomField,
  dispatchIssue,
  updateCustomFieldData,
  updateCustomFieldDialogState,
  hideCustomField,
  dialogsState,
  issues,
  members,
  workspaceIssuePer,
  projects,
  allTasks,
  enqueueSnackbar,
  issueActivities,
  issueNotifications,
  profile,
  history,
  archivedIssues,
  permissionObject,
  customFields,
  loggedInTeam,
  accessFeaturesIssue,
  updateFeatureAccessPermissions
}) {
  const { editIssue, handleDateSave } = useUpdateIssueHook();
  const dispatch = useDispatch();
  let searchQuery = history.location.pathname; //getting url
  const { issueDetailDialog } = dialogsState;
  const [readAll, setReadAll] = useState(true);
  const [sFSecOpen, setSFSecOpen] = useState(true);
  const handleShowSystemField = section => {
    setSFSecOpen(!sFSecOpen);
  };
  const handleClose = () => {
    issueDetailDialogState(dispatch, { id: "", issueWorkspacePer: null, isArchived: false });
    issueDetailDialog.afterCloseCallBack();
    if (
      !searchQuery.includes("issueOverview") &&
      !searchQuery.includes("gantt") &&
      !searchQuery.includes("boards") &&
      !searchQuery.includes("/reports/taskOverview")
    ) {
      history.push("/issues");
    } /** if modal is opend from issue overview view */
  };
  const currentIssue = useMemo(() => {
    let issueList = issueDetailDialog.isArchived
      ? archivedIssues
      : issues; /** if user screen is archived issues than get archived issues from store otherwise get all store issue */
    const selectedIssue = issueList.find(item => item.id === issueDetailDialog.id);
    return selectedIssue;
  }, [issueDetailDialog.id, issues]);

  const issueDetails = useRef(currentIssue);

  const updateIssueObj = (type, value) => {
    editIssue(currentIssue, type, value);
  };
  useEffect(() => {
    issueDetails.current = currentIssue;
  }, [issueDetailDialog.id, issues]);

  const showSnackBar = (snackBarMessage, type) => {
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  const issueAssigneeList =
    currentIssue && currentIssue.assignee
      ? members.filter(member => {
        return currentIssue.assignee.indexOf(member.userId) > -1;
      })
      : null;

  const permissionObj = issueDetailDialog.issueWorkspacePer
    ? issueDetailDialog.issueWorkspacePer
    : workspaceIssuePer;

  const issuePermission =
    currentIssue &&
      currentIssue.projectId /** if issue link with project then assign project permission */
      ? isUndefined(permissionObject[currentIssue.projectId])
        ? permissionObj
        : permissionObject[currentIssue.projectId].issue
      : permissionObj; /** workspace level permission */

  const chatPermission = {
    addAttachment:
      !currentIssue?.isArchive &&
      issuePermission.issueDetail.issuecomments.issueattachment.isAllowAdd,
    deleteAttachment:
      !currentIssue?.isArchive &&
      issuePermission.issueDetail.issuecomments.issueattachment.isAllowDelete,
    downloadAttachment:
      !currentIssue?.isArchive &&
      issuePermission.issueDetail.issuecomments.downloadAttachment.cando,
    addComments: !currentIssue?.isArchive && issuePermission.issueDetail.issuecomments.cando,
    editComment:
      !currentIssue?.isArchive && issuePermission.issueDetail.issuecomments.comments.isAllowEdit,
    deleteComment:
      !currentIssue?.isArchive && issuePermission.issueDetail.issuecomments.comments.isAllowDelete,
    convertToTask:
      !currentIssue?.isArchive && issuePermission.issueDetail.issuecomments.convertToTask.cando,
    reply: !currentIssue?.isArchive && issuePermission.issueDetail.issuecomments.reply.cando,
    replyLater:
      !currentIssue?.isArchive && issuePermission.issueDetail.issuecomments.replyLater.cando,
    showReceipt:
      !currentIssue?.isArchive && issuePermission.issueDetail.issuecomments.showReceipt.cando,
    clearAll:
      !currentIssue?.isArchive && issuePermission.issueDetail.issuecomments.clearAllReplies.cando,
    clearReply:
      !currentIssue?.isArchive && issuePermission.issueDetail.issuecomments.clearReply.cando,
  };
  const docPermission = {
    uploadDocument:
      !currentIssue?.isArchive && issuePermission.issueDetail.issueDocuments.uploadDocument.cando,
    addFolder:
      !currentIssue?.isArchive && issuePermission.issueDetail.issueDocuments.addFolder.cando,
    downloadDocument:
      !currentIssue?.isArchive && issuePermission.issueDetail.issueDocuments.downloadDocument.cando,
    viewDocument: !currentIssue?.isArchive && issuePermission.issueDetail.issueDocuments.cando,
    deleteDocument:
      !currentIssue?.isArchive && issuePermission.issueDetail.issueDocuments.deleteDocument.cando,
    openFolder:
      !currentIssue?.isArchive && issuePermission.issueDetail.issueDocuments.openFolder.cando,
    renameFolder:
      !currentIssue?.isArchive && issuePermission.issueDetail.issueDocuments.renameFolder.cando,
  };
  const chatConfig = {
    contextUrl: "communication",
    contextView: "Issue",
    contextId: currentIssue?.id,
    contextKeyId: "issueId",
    contextChat: "single",
    assigneeLists: issueAssigneeList,
  };

  const handleAllRead = () => {
    if (readAll) {
      setReadAll(false);
      MarkIssueNotificationsAsRead(currentIssue.id, profile.userId, (err, data) => {
        setReadAll(true);
      });
    }
  };
  const issueNotificationss = () => {
    const issueNotificationsData = issueNotifications;
    let notifications = issueNotificationsData.filter(x => {
      return x.users.filter(y => y.userId === profile.userId && y.isViewed === false).length;
    });
    notifications = notifications
      .map(x => {
        x.createdName = members.find(m => m.userId === x.createdBy);
        x.users = x.users.map(u => ({
          userId: u.userId,
          isViewed: u.isViewed,
        }));
        return x;
      })
      .map(x => ({
        id: x.notificationId,
        title: x.description ? x.description : " ",
        date: helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(x.updatedDate || x.createdDate),
        time: helper.RETURN_CUSTOMDATEFORMATFORTIME(x.updatedDate || x.createdDate),
        profilePic: x.createdName ? x.createdName.imageUrl : "",
        createdName: x.createdName,
        notificationUrl: x.notificationUrl,
        teamName: x.teamName || "N/A",
        teamId: x.teamId,
        workspaceName: x.workSpaceName || "N/A",
        workspaceId: x.workSpaceId,
      }));
    return notifications;
  };
  const updateAssignee = (assignedTo, issue) => {
    let assignedToArr = assignedTo.map(ass => {
      return ass.userId;
    });
    updateIssueObj("assignee", assignedToArr);
  };
  const canAddAsignee =
    !currentIssue?.isArchive && issuePermission.issueDetail.issueAssignee.isAllowAdd;
  const canDeleteAsignee =
    !currentIssue?.isArchive && issuePermission.issueDetail.issueAssignee.isAllowDelete;

  const canHideField = !currentIssue?.isArchive
    ? issuePermission.issueDetail.isHideField.cando
    : false;
  const canUpdateField = !currentIssue?.isArchive
    ? issuePermission.issueDetail.isUpdateField.cando
    : false;
  const canUseField = !currentIssue?.isArchive
    ? issuePermission.issueDetail.isUseField.cando
    : false;

  let notifications = issueNotificationss();

  const getCreatedByName = () => {
    let createdByUser = currentIssue?.createdBy;
    return createdByUser ? createdByUser : "User not found!";
  };
  const issueTitle =
    !currentIssue?.isArchive && issuePermission.issueDetail.editIssueName.isAllowEdit;

  const handleUpdateTitle = title => {
    /** Update issue title when user changes */
    updateIssueObj("title", title);
  };

  const customFieldChange = (option, obj, settings, success = () => { }, failure = () => { }) => {
    // const issueCopy = { ...currentIssue };
    const issueCopy = { ...issueDetails.current };

    if (obj.fieldType == "matrix") {
      const selectedField =
        issueCopy.customFieldData && issueCopy.customFieldData.find(x => x.fieldId == obj.fieldId);
      const isExist =
        selectedField &&
        selectedField.fieldData.data.findIndex(f => f.cellName == option.cellName) > -1;
      if (isExist === true) return;
    }
    const { fieldId } = obj;
    const newObj = {
      groupType: "issue",
      groupId: issueCopy.id,
      fieldId,
      fieldData: { data: option },
    };

    UpdateIssueCustomField(
      newObj,
      res => {
        success();
        const issueObj = res.data.entity[0];
        let customFieldsArr = [];
        // Updating Global state
        const isExist = issueCopy.customFieldData
          ? issueCopy.customFieldData.findIndex(c => c.fieldId === issueObj.fieldId) > -1
          : false; /** if new issue created, its customFields are null so in null case scenario , compile the normal false flow, add custom field in issue object and save it */
        if (isExist) {
          customFieldsArr = issueCopy.customFieldData.map(c => {
            return c.fieldId === issueObj.fieldId ? issueObj : c;
          });
        } else {
          customFieldsArr = issueCopy.customFieldData
            ? [...issueCopy.customFieldData, issueObj]
            : [
              issueObj,
            ]; /** add custom field in issue object and save it, if newly created issue because its custom field is already null */
        }
        let newIssueObj = { ...issueCopy, customFieldData: customFieldsArr };
        dispatchIssue(newIssueObj);
      },
      () => {
        failure();
      }
    );
  };

  const handleEditField = field => {
    updateCustomFieldDialogState({ moduleViewType: "editCopyModal", data: field, mode: "edit" });
  };
  const handleCopyField = field => {
    updateCustomFieldDialogState({ moduleViewType: "editCopyModal", data: field, mode: "copy" });
  };
  const handleSelectHideOption = (value, selectedFieldObj) => {
    let object = { ...selectedFieldObj };

    if (value == 1) {
      object.hideOption = "workspace";
      object.settings.isHidden = false;

      object.settings.hiddenInWorkspaces = [
        profile.loggedInTeam,
        ...object.settings.hiddenInWorkspaces,
      ];
      object.settings.hiddenInGroup =
        object.settings.hiddenInGroup.indexOf("issue") < 0
          ? ["issue", ...object.settings.hiddenInGroup]
          : object.settings.hiddenInGroup;
    }
    if (value == 2) {
      /** if user select the team level */
      object.hideOption = "allWorkspaces";
      object.settings.isHidden = true;
      object.settings.hiddenInWorkspaces = [];
      object.settings.hiddenInGroup =
        object.settings.hiddenInGroup.indexOf("issue") < 0
          ? ["issue", ...object.settings.hiddenInGroup]
          : object.settings.hiddenInGroup;
    }
    delete object.team;
    delete object.workspaces;
    hideCustomField(
      object,
      res => { },
      failure => { }
    );
  };

  const handleSystemOptionSelect = (value, selectedFieldObj) => {
    let object = {
      fieldId: selectedFieldObj.fieldId
    };
    if (value == 0) {
      object.isHidden = false;
      object.IsHiddenInWorkspace = false;
      object.IsHiddenInTeam = false;
    }
    if (value == 1) {
      object.IsHiddenInWorkspace = true;
    }
    if (value == 2) {
      object.IsHiddenInTeam = true;
    }
    updateFeatureAccessPermissions(
      'issue',
      object,
      succ => {
      },
      fail => {
      }
    );
  };

  const handleClickHideOption = (event, field) => {
    let object = { ...field };
    object.settings.isHideCompletedTodos = !object.settings.isHideCompletedTodos;
    delete object.team;
    delete object.workspaces;
    delete object.level;
    editCustomField(
      object,
      object.fieldId,
      res => { },
      failure => { }
    );
  };
  const editCustomFieldCallback = (obj, oldFieldObject) => {
    const { fieldId, fieldType } = obj;
    if (fieldType === "checklist") return;
    updateCustomFieldData(obj);
  };
  const addCustomFieldCallback = obj => {
    const { fieldId } = obj;
    const customFieldDataObj = {
      groupType: "issue",
      groupId: currentIssue.id,
      fieldId: fieldId,
      fieldData: { data: obj.settings.multiSelect ? [] : {} },
    };
    const issueCopy = cloneDeep(currentIssue);
    issueCopy.customFieldData = [...issueCopy.customFieldData, customFieldDataObj];
    dispatchIssue(issueCopy);
  };
  const handleDateUpdate = (key, date, time) => {
    handleDateSave(
      key,
      date,
      currentIssue,
      time,
      succ => { },
      err => {
        showSnackBar(err, "error");
      }
    );
  };
  const handleOptionsSelect = (key, value) => {
    updateIssueObj(key, value.value);
  };

  const generateSelectedValue = () => {
    /** function for generating the selected task list objects from task id and return taskList for showing pre populated selected task if any => function call in componentDidMount */
    const selectedTaskList = allTasks.filter(task => {
      return currentIssue?.linkedTasks && currentIssue?.linkedTasks.indexOf(task.taskId) > -1;
    });
    let taskList = generateTaskData(selectedTaskList);
    return taskList;
  };
  const selectedTask = generateSelectedValue();

  const generateTaskDData = useMemo(() => {
    /* function for generating the array of task and returing array to the data prop for multi drop down */
    let tasksData = currentIssue?.projectId
      ? allTasks.filter(t => t.projectId === currentIssue?.projectId)
      : allTasks;
    tasksData = tasksData.filter(p => p.teamId == currentIssue?.workSpaceId)
    let taskList = generateTaskData(tasksData);
    return taskList;
  }, [currentIssue?.projectId, currentIssue?.linkedTasks]);
  const checkIssueListAccordingToPermission = (stateTask, selectedTask) => {
    if (stateTask.length <= 0) {
      /** if state task is empty then accept the selected task */
      return true;
    } else {
      showSnackBar(
        "Oops! You cannot link this task to this issue, because it is already linked to another task. Unlink the issue with the other task first and then try again. ",
        "error"
      );
      return false;
    }
  };
  const handleTaskChange = (key, value) => {
    let permission = checkIssueListAccordingToPermission(selectedTask, value);
    /* function for fetching the task id's and from selected task and update the issue details  */
    if (permission) {
      let taskSelectedIds = value.map(obj => {
        /* taskSelectedIds is a list of task id's fetching from selected task object in drop down */
        return obj.id;
      });
      const obj = {
        linkedTasks: taskSelectedIds,
        projectId: value[0].obj.projectId,
      };
      updateIssueData(
        { issue: currentIssue, obj },
        dispatch,
        res => {
          if (grid.grid) grid.grid.redrawRows([res]);
        },
        error => {
          const err = error.data.message || "";
          const message = err || "Server throws error";
          showSnackBar(message, "error");
        }
      );
    }
  };
  const handleClearTask = (key, value) => {
    let taskSelectedIds = value.map(obj => {
      /* taskSelectedIds is a list of task id's fetching from selected task object in drop down */
      return obj.id;
    });
    const obj = {
      linkedTasks: taskSelectedIds,
      projectId:
        currentIssue.projectId == "" && value.length
          ? value[0].obj.projectId
          : currentIssue.projectId,
    };
    updateIssueData(
      { issue: currentIssue, obj },
      dispatch,
      res => {
        if (grid.grid) grid.grid.redrawRows([res]);
      },
      error => {
        const err = error.data.message || "";
        const message = err || "Server throws error";
        showSnackBar(message, "error");
      }
    );
  };
  const canEditIssueDescription =
    currentIssue &&
    !currentIssue.isArchive &&
    issuePermission.issueDetail.issueDescription.isAllowEdit;

  const customFieldPermission =
    currentIssue && !currentIssue.isArchive
      ? issuePermission.issueDetail.customsFields
      : {
        isAllowEdit: false,
        isAllowDelete: false,
        isAllowAdd: false,
      };

  const generateProjectDData = useMemo(() => {
    let selectedProject = selectedTask.length
      ? projects.filter(t => t.projectId === selectedTask[0].obj.projectId)
      : projects;
    selectedProject = selectedProject.length ? selectedProject : projects;
    selectedProject = selectedProject.filter(p => p.projectId !== currentIssue?.projectId);
    selectedProject = selectedProject.filter(p => p.teamId == currentIssue?.workSpaceId);
    return generateProjectData(selectedProject);
  }, [currentIssue?.projectId, currentIssue?.linkedTasks]);

  const handleSelectChange = (newValue, issue) => {
    const projectId = newValue.length == 0 ? "" : newValue[newValue.length - 1].id;

    let linkTask = selectedTask.length
      ? selectedTask[0].obj.projectId == "" || !selectedTask[0].obj.projectId
        ? []
        : currentIssue?.linkedTasks
      : currentIssue?.linkedTasks;

    const obj = {
      projectId: projectId,
      linkedTasks: newValue.length == 0 ? [] : linkTask,
    };
    updateIssueData(
      { issue: currentIssue, obj },
      dispatch,
      res => {
        if (grid.grid) grid.grid.redrawRows([res]);
      },
      error => {
        const err = error.data.message || "";
        const message = err || "Server throws error";
        showSnackBar(message, "error");
      }
    );
  };
  const getSelectedProject = () => {
    if (currentIssue?.projectId) {
      const selectedProject = projects.find(project => {
        return project.projectId == currentIssue.projectId;
      });

      return selectedProject
        ? [
          {
            label: selectedProject.projectName,
            id: selectedProject.projectId,
            obj: {},
          },
        ]
        : [];
    } else return [];
  };
  const canEditIssueProject =
    currentIssue && !currentIssue.isArchive
      ? issuePermission.issueDetail.issueProject.isAllowEdit
      : true;

  const canAccessField = ({ feature, group }) => {
    if (searchQuery.includes("/reports")) {
      return TeamCanAccessFeature({ group, feature, workspaceId: currentIssue.teamId || "" });
    }
    else {
      return CanAccess({ group, feature })
    }
  }

  const obj = currentIssue?.projectId ? { projectId: currentIssue.projectId } : { workspaceId: currentIssue.teamId };
  const calendarWorkingdays = getCalendar(obj) || null;
  return currentIssue ? (
    <DetailsDialog
      dialogProps={{
        open: issueDetailDialog.id !== "",
        onClose: () => {
          handleClose();
        },
        disableBackdropClick: true,
        disableEscapeKeyDown: true,
      }}
      headerRenderer={
        <>
          {canAccessField({ group: 'issue', feature: 'assignee' }) &&
            <AssigneeDropdown
              assignedTo={issueAssigneeList}
              updateAction={updateAssignee}
              isArchivedSelected={currentIssue.isArchive}
              obj={currentIssue}
              closeIssueDetailsPopUp={() => {
                handleClose();
              }}
              addPermission={canAddAsignee}
              deletePermission={canDeleteAsignee}
              customIconButtonProps={{
                classes: {
                  root: classes.iconBtnStyles,
                },
              }}
              totalAssigneeBtnProps={{
                style: {
                  height: 29,
                  width: 29,
                  fontSize: 14,
                },
              }}
              avatarSize={"xsmall"}
            />}
          <div className={classes.vl}></div>
          <NotificationMenu
            notificationsData={notifications}
            MarkAllNotificationsAsRead={handleAllRead}
            history={history}
            customButtonProps={{
              style: {
                marginRight: 10,
                minWidth: 0,
              },
            }}
          />
          <IssueActionDropdown
            issue={currentIssue}
            selectedColor={currentIssue.colorCode ? currentIssue.colorCode : ""}
            isUpdated={issueDetailDialog.isUpdated}
            UpdateIssue={UpdateIssue}
            DeleteIssue={DeleteIssue}
            closeActionMenu={() => {
              handleClose();
              issueDetailDialog.closeActionMenu("CloseDetailsDialogue");
            }}
            permission={true}
            isArchivedSelected={false}
            issuePer={issuePermission}
            handleExportType={showSnackBar}
            showSnackBar={showSnackBar}
            btnStyles={{
              marginRight: 10,
            }}
            iconStyles={{
              transform: "rotate(90deg)",
            }}
          />
        </>
      }
      leftSideContent={
        <>
          <div className={classes.createdBy}>
            <span>
              <b>{`#${currentIssue.uniqueId}`}</b>
              {` by ${getCreatedByName()} on ${moment(currentIssue.createdDate).format(
                "ll"
              )} at ${moment(currentIssue.createdDate).format("LT")}`}
            </span>
            <EditableTextField
              title={currentIssue.title}
              module={"Issue"}
              handleEditTitle={handleUpdateTitle}
              permission={issueTitle}
              defaultProps={{
                inputProps: { maxLength: 250 },
              }}
            />
          </div>
          <div className={classes.systemFields}>
            <div>
              <div
                className={classes.systemFieldsHeading}
                style={{ background: "#89A3B2" }}
                onClick={() => {
                  handleShowSystemField();
                }}>
                <Typography
                  className={classes.systemFieldTitle}
                  onClick={() => {
                    handleShowSystemField();
                  }}>
                  System Fields
                </Typography>
                <div>
                  {sFSecOpen ? (
                    <RemoveIcon
                      className={classes.systemFieldRemoveIcon}
                      onClick={() => {
                        handleShowSystemField();
                      }}
                    />
                  ) : (
                    <AddIcon
                      className={classes.systemFieldAddIcon}
                      onClick={() => {
                        handleShowSystemField();
                      }}
                    />
                  )}
                </div>
              </div>
              {sFSecOpen && (
                <Grid container spacing={1} classes={{ container: classes.sectionCnt }}>
                  {canAccessField({ group: 'issue', feature: 'tasks' }) && (
                    <Grid item sm={12} md={6}>
                      <div className={classes.textFieldCnt}>
                        <CustomFieldLabelCmp
                          label={"Task"}
                          iconType={"dropdown"}
                          handleDelete={() => { }}
                          handleEdit={() => { }}
                          handleCopy={() => { }}
                          handleSelectHideOption={option =>
                            handleSystemOptionSelect(option, accessFeaturesIssue['tasks'])
                          }
                          deleteOperation={false}
                          editOperation={false}
                          copyOperation={false}
                          hideOperation={canHideField}
                          customIconProps={{
                            color: "#999",
                          }}
                        />
                        <SelectSearchDropdown /* task select drop down */
                          data={() => {
                            return generateTaskDData;
                          }}
                          selectChange={handleTaskChange}
                          selectRemoveValue={handleClearTask}
                          type="task"
                          selectedValue={selectedTask}
                          optionBackground={true}
                          placeholder="Select"
                          isDisabled={
                            !currentIssue.isArchive
                              ? !issuePermission.issueDetail.issueTask.isAllowEdit
                              : true
                          }
                          styles={{ marginBottom: 0, marginTop: 0, flex: 1 }}
                          customStyles={{
                            control: {
                              border: "none",
                              // padding: "0px 5px",
                              fontFamily: theme.typography.fontFamilyLato,
                              fontWeight: theme.typography.fontWeightExtraLight,
                              "&:hover": {
                                background: `${theme.palette.background.items} !important`,
                              },
                              minHeight: 32,
                            },
                            placeholder: {
                              color: "#7E7E7E",
                            },
                          }}
                          writeFirst={false}
                        />
                      </div>
                    </Grid>
                  )}
                  {teamCanView("projectAccess") && canAccessField({ group: 'issue', feature: 'project' }) && (
                    <Grid item sm={12} md={6}>
                      <div className={classes.textFieldCnt}>
                        <CustomFieldLabelCmp
                          label={"Project"}
                          iconType={"dropdown"}
                          handleDelete={() => { }}
                          handleEdit={() => { }}
                          handleCopy={() => { }}
                          handleSelectHideOption={option =>
                            handleSystemOptionSelect(option, accessFeaturesIssue['project'])
                          }
                          deleteOperation={false}
                          editOperation={false}
                          copyOperation={false}
                          hideOperation={canHideField}
                          customIconProps={{
                            color: "#999",
                          }}
                        />
                        <SelectSearchDropdown
                          data={() => {
                            return generateProjectDData;
                          }}
                          placeholder={"Select"}
                          optionBackground={true}
                          isMulti={true}
                          selectChange={(type, option) => {
                            handleSelectChange(option, type);
                          }}
                          selectedValue={getSelectedProject()}
                          selectRemoveValue={(type, option) => {
                            handleSelectChange(option, type);
                          }}
                          styles={{ marginBottom: 0, marginTop: 0, flex: 1 }}
                          customStyles={{
                            control: {
                              border: "none",
                              // padding: "0px 5px",
                              fontFamily: theme.typography.fontFamilyLato,
                              fontWeight: theme.typography.fontWeightExtraLight,
                              "&:hover": {
                                background: `${theme.palette.background.items} !important`,
                              },
                              minHeight: 32,
                            },
                            placeholder: {
                              color: "#7E7E7E",
                            },
                          }}
                          isDisabled={!canEditIssueProject || currentIssue.isArchive}
                          writeFirst={false}
                        />
                      </div>
                    </Grid>
                  )}
                  {canAccessField({ group: 'issue', feature: 'type' }) && (
                    <Grid item sm={12} md={6}>
                      <div className={classes.textFieldCnt}>
                        <CustomFieldLabelCmp
                          label={"Issue Type"}
                          iconType={"dropdown"}
                          handleDelete={() => { }}
                          handleEdit={() => { }}
                          handleCopy={() => { }}
                          handleSelectHideOption={option =>
                            handleSystemOptionSelect(option, accessFeaturesIssue['type'])
                          }
                          deleteOperation={false}
                          editOperation={false}
                          copyOperation={false}
                          hideOperation={canHideField}
                          customIconProps={{
                            color: "#999",
                          }}
                        />
                        <SelectSearchDropdown /* Status select drop down */
                          data={() => {
                            return typeData(theme, classes, intl);
                          }}
                          selectChange={handleOptionsSelect}
                          type="type"
                          selectedValue={typeData(theme, classes, intl).find(s => {
                            return s.value == currentIssue.type;
                          })}
                          placeholder="Select"
                          icon
                          isMulti={false}
                          isDisabled={
                            !currentIssue.isArchive
                              ? !issuePermission.issueDetail.issueType.isAllowEdit
                              : true
                          }
                          styles={{ marginBottom: 0, marginTop: 0, flex: 1 }}
                          customStyles={{
                            control: {
                              border: "none",
                              // padding: "0px 5px",
                              fontFamily: theme.typography.fontFamilyLato,
                              fontWeight: theme.typography.fontWeightExtraLight,
                              "&:hover": {
                                background: `${theme.palette.background.items} !important`,
                              },
                              minHeight: 32,
                            },
                            placeholder: {
                              color: "#7E7E7E",
                            },
                          }}
                          writeFirst={true}
                        />
                      </div>
                    </Grid>
                  )}
                  {canAccessField({ group: 'issue', feature: 'status' }) && (
                    <Grid item sm={12} md={6}>
                      <div className={classes.textFieldCnt}>
                        <CustomFieldLabelCmp
                          label={"Status"}
                          iconType={"dropdown"}
                          handleDelete={() => { }}
                          handleEdit={() => { }}
                          handleCopy={() => { }}
                          handleSelectHideOption={option =>
                            handleSystemOptionSelect(option, accessFeaturesIssue['status'])
                          }
                          deleteOperation={false}
                          editOperation={false}
                          copyOperation={false}
                          hideOperation={canHideField}
                          customIconProps={{
                            color: "#999",
                          }}
                        />
                        <SelectSearchDropdown /* Status select drop down */
                          data={() => {
                            return statusData(theme, classes, intl);
                          }}
                          selectChange={handleOptionsSelect}
                          type="status"
                          selectedValue={statusData(theme, classes, intl).find(s => {
                            return s.value == currentIssue.status;
                          })}
                          placeholder="Select"
                          icon
                          isMulti={false}
                          isDisabled={
                            !currentIssue.isArchive
                              ? !issuePermission.issueDetail.issueStatus.isAllowEdit
                              : true
                          }
                          styles={{ marginBottom: 0, marginTop: 0, flex: 1 }}
                          customStyles={{
                            control: {
                              border: "none",
                              // padding: "0px 5px",
                              fontFamily: theme.typography.fontFamilyLato,
                              fontWeight: theme.typography.fontWeightExtraLight,
                              "&:hover": {
                                background: `${theme.palette.background.items} !important`,
                              },
                              minHeight: 32,
                            },
                          }}
                          writeFirst={true}
                        />
                      </div>
                    </Grid>
                  )}
                  {canAccessField({ group: 'issue', feature: 'severity' }) && (
                    <Grid item sm={12} md={6}>
                      <div className={classes.textFieldCnt}>
                        <CustomFieldLabelCmp
                          label={"Severity"}
                          iconType={"dropdown"}
                          handleDelete={() => { }}
                          handleEdit={() => { }}
                          handleCopy={() => { }}
                          handleSelectHideOption={option =>
                            handleSystemOptionSelect(option, accessFeaturesIssue['severity'])
                          }
                          deleteOperation={false}
                          editOperation={false}
                          copyOperation={false}
                          hideOperation={canHideField}
                          customIconProps={{
                            color: "#999",
                          }}
                        />
                        <SelectSearchDropdown /* Severity select drop down */
                          data={() => {
                            return severity(theme, classes, intl);
                          }}
                          selectChange={handleOptionsSelect}
                          type="severity"
                          selectedValue={severity(theme, classes, intl).find(s => {
                            return s.value == currentIssue.severity;
                          })}
                          placeholder="Select"
                          icon
                          isMulti={false}
                          isDisabled={
                            !currentIssue.isArchive
                              ? !issuePermission.issueDetail.issueSeverity.isAllowEdit
                              : true
                          }
                          styles={{ marginBottom: 0, marginTop: 0, flex: 1 }}
                          customStyles={{
                            control: {
                              border: "none",
                              // padding: "0px 5px",
                              fontFamily: theme.typography.fontFamilyLato,
                              fontWeight: theme.typography.fontWeightExtraLight,
                              "&:hover": {
                                background: `${theme.palette.background.items} !important`,
                              },
                              minHeight: 32,
                            },
                          }}
                          writeFirst={true}
                        />
                      </div>
                    </Grid>
                  )}
                  {canAccessField({ group: 'issue', feature: 'priority' }) && (
                    <Grid item sm={12} md={6}>
                      <div className={classes.textFieldCnt}>
                        <CustomFieldLabelCmp
                          label={"Priority"}
                          iconType={"dropdown"}
                          handleDelete={() => { }}
                          handleEdit={() => { }}
                          handleCopy={() => { }}
                          handleSelectHideOption={option =>
                            handleSystemOptionSelect(option, accessFeaturesIssue['priority'])
                          }
                          deleteOperation={false}
                          editOperation={false}
                          copyOperation={false}
                          hideOperation={canHideField}
                          customIconProps={{
                            color: "#999",
                          }}
                        />
                        <SelectSearchDropdown /* Priority select drop down */
                          data={() => {
                            return priorityData(theme, classes, intl);
                          }}
                          selectChange={handleOptionsSelect}
                          type="priority"
                          selectedValue={priorityData(theme, classes, intl).find(s => {
                            return s.value == currentIssue.priority;
                          })}
                          placeholder="Select"
                          icon
                          isMulti={false}
                          isDisabled={
                            !currentIssue.isArchive
                              ? !issuePermission.issueDetail.issuePriority.isAllowEdit
                              : true
                          }
                          styles={{ marginBottom: 0, marginTop: 0, flex: 1 }}
                          customStyles={{
                            control: {
                              border: "none",
                              // padding: "0px 5px",
                              fontFamily: theme.typography.fontFamilyLato,
                              fontWeight: theme.typography.fontWeightExtraLight,
                              "&:hover": {
                                background: `${theme.palette.background.items} !important`,
                              },
                              minHeight: 32,
                            },
                          }}
                          writeFirst={true}
                        />
                      </div>
                    </Grid>
                  )}
                  {canAccessField({ group: 'issue', feature: 'startDate' }) && (
                    <Grid item sm={12} md={6}>
                      <IssueDateDropDown
                        label={"Planned Start"}
                        timeInputLabel={"Start Time"}
                        date={currentIssue.startDate}
                        selectedTime={currentIssue.startTime}
                        id={"startDate"}
                        handleSelectDate={handleDateUpdate}
                        permission={
                          !currentIssue.isArchive
                            ? issuePermission.issueDetail.issuePlanndStartEndDate.isAllowEdit
                            : false
                        }
                        customProps={{
                          datePickerProps: {
                            filterDate: date => {
                              return excludeOffdays(moment(date), calendarWorkingdays) ?
                                currentIssue.dueDate
                                  ? moment(date).isBefore(currentIssue.dueDate, "day") ||
                                  moment(date).isSame(currentIssue.dueDate, "day")
                                  : true
                                : false;
                              // return currentIssue.dueDate
                              //   ? date.isBefore(currentIssue.dueDate, "day") ||
                              //   date.isSame(currentIssue.dueDate, "day")
                              //   : true;
                            },
                          },
                        }}
                        handleSelectHideOption={option =>
                          handleSystemOptionSelect(option, accessFeaturesIssue['startDate'])
                        }
                        hideOperation={canHideField}
                      />
                    </Grid>
                  )}
                  {canAccessField({ group: 'issue', feature: 'dueDate' }) && (
                    <Grid item sm={12} md={6}>
                      <IssueDateDropDown
                        label={"Planned End"}
                        timeInputLabel={"End Time"}
                        date={currentIssue.dueDate}
                        selectedTime={currentIssue.dueTime}
                        id={"dueDate"}
                        handleSelectDate={handleDateUpdate}
                        permission={
                          !currentIssue.isArchive
                            ? issuePermission.issueDetail.issuePlanndStartEndDate.isAllowEdit
                            : false
                        }
                        customProps={{
                          datePickerProps: {
                            filterDate: date => {
                              return excludeOffdays(moment(date), calendarWorkingdays) ?
                                currentIssue.startDate
                                  ? moment(date).isAfter(currentIssue.startDate, "day") ||
                                  moment(date).isSame(currentIssue.startDate, "day")
                                  : true
                                : false;
                              // return currentIssue.startDate
                              //   ? date.isAfter(currentIssue.startDate, "day") ||
                              //   date.isSame(currentIssue.startDate, "day")
                              //   : true;
                            },
                          },
                        }}
                        handleSelectHideOption={option =>
                          handleSystemOptionSelect(option, accessFeaturesIssue['dueDate'])
                        }
                        hideOperation={canHideField}
                      />
                    </Grid>
                  )}
                  {canAccessField({ group: 'issue', feature: 'actualStartDate' }) && (
                    <Grid item sm={12} md={6}>
                      <IssueDateDropDown
                        label={"Actual Start"}
                        timeInputLabel={"Start Time"}
                        date={currentIssue.actualStartDate}
                        selectedTime={currentIssue.actualStartTime}
                        id={"actualStartDate"}
                        handleSelectDate={handleDateUpdate}
                        permission={
                          !currentIssue.isArchive
                            ? issuePermission.issueDetail.issueActualstartEndDate.isAllowEdit
                            : false
                        }
                        customProps={{
                          datePickerProps: {
                            filterDate: date => {
                              return currentIssue.actualDueDate
                                ? moment(date).isBefore(currentIssue.actualDueDate, "day") ||
                                moment(date).isSame(currentIssue.actualDueDate, "day")
                                : true;
                            },
                          },
                        }}
                        handleSelectHideOption={option =>
                          handleSystemOptionSelect(option, accessFeaturesIssue['actualStartDate'])
                        }
                        hideOperation={canHideField}
                      />
                    </Grid>
                  )}
                  {canAccessField({ group: 'issue', feature: 'actualDueDate' }) && (
                    <Grid item sm={12} md={6}>
                      <IssueDateDropDown
                        label={"Actual End"}
                        timeInputLabel={"End Time"}
                        date={currentIssue.actualDueDate}
                        selectedTime={currentIssue.actualDueTime}
                        id={"actualDueDate"}
                        handleSelectDate={handleDateUpdate}
                        permission={
                          !currentIssue.isArchive
                            ? issuePermission.issueDetail.issueActualstartEndDate.isAllowEdit
                            : false
                        }
                        customProps={{
                          datePickerProps: {
                            filterDate: date => {
                              return currentIssue.actualStartDate
                                ? moment(date).isAfter(currentIssue.actualStartDate, "day") ||
                                moment(date).isSame(currentIssue.actualStartDate, "day")
                                : true;
                            },
                          },
                        }}
                        handleSelectHideOption={option =>
                          handleSystemOptionSelect(option, accessFeaturesIssue['actualDueDate'])
                        }
                        hideOperation={canHideField}
                      />
                    </Grid>
                  )}
                  {canAccessField({ group: 'issue', feature: 'description' }) && (
                    <Grid item sm={12} md={12}>
                      <IssueDescriptionEditor
                        issueData={currentIssue}
                        isUpdated={data => {
                          updateIssueObj("description", data.description);
                        }}
                        permission={!canEditIssueDescription}
                        view="issue"
                        handleSelectHideOption={option =>
                          handleSystemOptionSelect(option, accessFeaturesIssue['description'])
                        }
                        hideOperation={canHideField}
                      />
                    </Grid>
                  )}
                </Grid>
              )}
            </div>
            <SectionsView
              type={"issue"}
              disabled={currentIssue.isArchive}
              customFieldData={currentIssue.customFieldData}
              permission={{
                ...customFieldPermission,
                isAllowUpdate: canUpdateField,
                isAllowHide: canHideField,
                isUseField: canUseField,
              }}
              workspaceId={currentIssue.workSpaceId}
              customFieldChange={customFieldChange}
              handleEditField={handleEditField}
              handleCopyField={handleCopyField}
              groupId={currentIssue.id}
              handleClickHideOption={handleClickHideOption}
              handleSelectHideOption={handleSelectHideOption}
            />
          </div>
          {teamCanView("customFieldAccess") && !searchQuery.includes("/reports/issueOverview") && (
            <div className={classes.addCustomFieldCnt}>
              <CustomFieldView
                feature="issue"
                // onFieldAdd={addCustomFieldCallback}
                onFieldEdit={editCustomFieldCallback}
                onFieldDelete={() => { }}
                permission={{
                  ...customFieldPermission,
                  isAllowUpdate: canUpdateField,
                  isAllowHide: canHideField,
                  isUseField: canUseField,
                }}
                disableAssessment={true}
                disabled={currentIssue.isArchive}
              />
            </div>
          )}
        </>
      }
      rightSideContent={
        <>
          <SideTabs
            MenuData={currentIssue}
            members={members}
            issueActivitiesData={issueActivities}
            issuePer={issuePermission}
            onChatMount={() => { }}
            onChatUnMount={() => { }}
            chatConfig={chatConfig}
            chatPermission={chatPermission}
            docConfig={chatConfig}
            docPermission={docPermission}
            selectedTab={0}
            intl={intl}
          />
        </>
      }
    />
  ) : (
    <> </>
  );
}

NewIssueDetails.defaultProps = {
  classes: {},
  theme: {},
};
const mapStateToProps = state => {
  return {
    dialogsState: state.dialogStates,
    loggedInTeam: state.profile.data.loggedInTeam,
    appliedFilters: state.appliedFilters,
    itemOrderState: state.itemOrder.data,
    issues: state.issues.data || [],
    members:
      state.profile && state.profile.data && state.profile.data.member
        ? state.profile.data.member.allMembers
        : [],
    workspaces: state.profile.data.workspace,
    workspaceIssuePer: state.workspacePermissions.data.issue,
    projects: state.projects.data || [],
    allTasks: state.tasks.data || [],
    issueActivities: state.issueActivities.data || [],
    issueNotifications: state.issueNotifications.data || [],
    profile: state.profile.data,
    archivedIssues: state.archivedItems.data,
    permissionObject: ProjectPermissionSelector(state),
    customFields: state.customFields,
    accessFeaturesIssue: state.featureAccessPermissions.issue,
  };
};
export default compose(
  withRouter,
  injectIntl,
  withSnackbar,
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, {
    UpdateIssue,
    DeleteIssue,
    ArchiveIssue,
    UnarchiveIssue,
    MarkIssueNotificationsAsRead,
    editCustomField,
    dispatchIssue,
    updateCustomFieldData,
    bulkUpdateIssue,
    updateCustomFieldDialogState,
    hideCustomField,
    updateFeatureAccessPermissions
  })
)(NewIssueDetails);
