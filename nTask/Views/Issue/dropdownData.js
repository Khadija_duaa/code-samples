import { columns } from "./constants";

export const sortingDropdownData = [
        { key: "", name: "None" },
        { key: "issueId", name: "ID", },
        { key: "title", name: "Title" },
        { key: "status", name: "Status" },
        { key: "severity", name: "Severity" },
        { key: "priority", name: "Priority" },
        { key: "type", name: "Type" },
        { key: "updatedDate", name: "Last Updated" },
        { key: "createdDate", name: "Creation Date" },
        { key: "startDate", name: "Planned Start Date" },
        { key: "dueDate", name: "Planned End Date" },
        { key: "actualStartDate", name: "Actual Start Date" },
        { key: "actualDueDate", name: "Actual End Date" }
      ]
export const groupingDropdownData = [columns.none, columns.issueStatus_C, columns.severity_C, columns.priority_C, columns.type_C]