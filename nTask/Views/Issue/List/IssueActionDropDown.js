import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles, withTheme } from "@material-ui/core/styles";
import itemStyles from "./styles";
import menuStyles from "../../../assets/jss/components/menu";
import CustomIconButton from "../../../components/Buttons/IconButton";
import SelectionMenu from "../../../components/Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import combineStyles from "../../../utils/mergeStyles";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import RightArrow from "@material-ui/icons/ArrowRight";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import DeleteConfirmDialog from "../../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import { getCompletePermissionsWithArchieve } from "../permissions";
import Typography from "@material-ui/core/Typography";
import { GetPermission } from "../../../components/permissions";
import ColorPicker from "../../../components/Dropdown/ColorPicker/ColorPicker";
import { FormattedMessage } from "react-intl";
import { ArchiveIssue, moveIssueToWorkspace } from "../../../redux/actions/issues";
import MoveIssueDialog from "../../../components/Dialog/MoveIssueDialog/MoveIssueDialog";
import { FetchWorkspaceInfo } from "../../../redux/actions/workspace";

const elementClosest = require("element-closest");

class IssueActionDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      pickerOpen: false,
      pickerPlacement: "",
      archiveFlag: false,
      unArchiveFlag: false,
      deleteFlag: false,
      archiveBtnQuery: "",
      unarchiveBtnQuery: "",
      moveIssueToWorkspace: false,
      moveIssueBtnQuery: "",
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleColorClick = this.handleColorClick.bind(this);
    this.handlePickerClose = this.handlePickerClose.bind(this);
    this.handleDialogClose = this.handleDialogClose.bind(this);
  }

  componentDidMount = () => {
    this.setState({
      selectedColor: this.props.selectedColor,
    });
  };

  handleOperations(event, value) {
    event.stopPropagation();
    switch (value) {
      case "Copy":
        //this.props.CopyTask(this.props.issue);
        break;
      case "Public Link":
        break;
      case "Archive":
        this.setState({ archiveFlag: true });
        break;
      case "Unarchive":
        this.setState({ unArchiveFlag: true, popupFlag: true });
        break;
      case "Delete":
        this.setState({ deleteFlag: true, popupFlag: true });
        break;
      case "Move":
        if (this.props.issue.linkedTasks.length > 0) {
          this.props.showSnackBar("You cannot move issues those are linked to task", "info");
          return;
        }
        this.setState({ moveIssueToWorkspace: true });
        break;
    }

    this.setState({ open: false, pickerOpen: false });
  }
  getTranslatedId(value) {
    switch (value) {
      case "Copy":
        value = "common.action.copy.label";
        break;
      case "Public Link":
        value = "common.action.public-link.label";
        break;
      case "Archive":
        value = "common.action.archive.label";
        break;
      case "Unarchive":
        value = "common.action.un-archive.label";
        break;
      case "Delete":
        value = "common.action.delete.label";
        break;
    }
    return value;
  }
  handleClose(event) {
    this.setState({ open: false, pickerOpen: false });
  }
  handlePickerClose(event) {
    this.setState({ pickerOpen: false });
  }
  handleDialogClose(isChanged) {
    this.setState(
      {
        popupFlag: false,
        archiveFlag: false,
        unArchiveFlag: false,
        deleteFlag: false,
        unarchiveBtnQuery: "",
        archiveBtnQuery: "",
      },
      () => {
        // if (isChanged) this.props.closeActionMenu();
      }
    );
  }
  handleDeleteIssue = () => {
    const { issue } = this.props;
    this.setState({ btnQuery: "progress" });
    this.props.DeleteIssue(issue.id, () => {
      this.setState({ btnQuery: "" }, () => {
        this.handleDialogClose(true);
        if (this.props.closeActionMenu) this.props.closeActionMenu();
        if (this.props.filterArchivedIssueDelete) this.props.filterArchivedIssueDelete(issue.id);
      });
    });
  };
  handleClick(event, placement) {
    event.stopPropagation();
    const { currentTarget } = event;
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }
  handleColorClick(event) {
    event.stopPropagation();
    if (event.target.closest("#colorPickerCnt") == null) {
      this.setState(prevState => ({ pickerOpen: !prevState.pickerOpen }));
    }
  }

  colorChange = (color, task) => {
    let self = this;
    this.setState({ selectedColor: color }, function () {
      let obj = Object.assign({}, task);
      this.setState({
        selectedColor: color,
      });
      obj.colorCode = color;
      this.props.isUpdated(obj, () => { });
    });
  };

  handleArchive = e => {
    if (e) e.stopPropagation();
    let { id } = this.props.issue;
    const obj = { id, archive: true };
    this.setState({ archiveBtnQuery: "progress" }, () => {
      if (obj.archive) {
        this.props.ArchiveIssue(
          obj,
          data => {
            this.handleDialogClose();
            if (this.props.closeActionMenu) this.props.closeActionMenu();
          },
          error => {
            this.handleDialogClose();
            if (error && error.status === 500) {
              this.props.handleExportType("Server throws error", "error");
            }
          }
        );
      }
    });
  };

  handleUnArchive = e => {
    if (e) e.stopPropagation();
    let { id } = this.props.issue || "";
    const obj = { id, unarchive: true };
    this.setState({ unarchiveBtnQuery: "progress" }, () => {
      this.props.isUpdated(obj, () => {
        this.setState({ unarchiveBtnQuery: "", archiveFlag: false, popupFlag: false }, () => {
          if (this.props.closeActionMenu) this.props.closeActionMenu();
        });
      });
    });
  };
  handleMoveDialogClose = () => {
    this.setState({ moveIssueToWorkspace: false });
  };
  handleMoveIssue = (e, team) => {
    if (e) e.stopPropagation();
    this.setState({ moveIssueBtnQuery: "progress" }, () => {
      let data = {
        issuesId: [this.props.issue.id],
        workspaceId: team.teamId,
      };
      this.props.moveIssueToWorkspace(
        data,
        succ => {
          this.props.FetchWorkspaceInfo("", () => { });
          this.setState({ moveIssueBtnQuery: "" });
          this.handleMoveDialogClose();
        },
        failure => {
          if (this.props.showSnackBar)
            this.props.showSnackBar("Oops! Server throws error.", "error");
          this.setState({ moveIssueBtnQuery: "" });
          this.handleMoveDialogClose();
        }
      );
      if (this.props.closeActionMenu) this.props.closeActionMenu();

    });
  };

  render() {
    const { classes, theme, issue, isArchivedSelected, issuePer, btnStyles = {}, iconStyles = {} } = this.props;
    const {
      open,
      placement,
      pickerOpen,
      popupFlag,
      selectedColor,
      btnQuery,
      archiveFlag,
      archiveBtnQuery,
      unarchiveBtnQuery,
      moveIssueBtnQuery,
    } = this.state;
    const ddData = [];

    if (!isArchivedSelected) {
      ddData.push("Color");
    }

    if (issuePer.archive.cando && !issue.isArchive) {
      ddData.push("Archive");
    }
    if (issuePer.unarchives.cando && issue.isArchive) {
      ddData.push("Unarchive");
    }
    if (!issue.isArchive) {
      ddData.push("Move");
    }
    if (issuePer.delete.cando) {
      ddData.push("Delete");
    }

    return (
      <Fragment>
        <ClickAwayListener onClickAway={this.handleClose}>
          <div>
            <CustomIconButton
              btnType="condensed"
              onClick={event => {
                this.handleClick(event, "bottom-end");
              }}
              buttonRef={node => {
                this.anchorEl = node;
              }}
              style={btnStyles}>
              <MoreVerticalIcon
                htmlColor={theme.palette.secondary.medDark}
                style={{ fontSize: "24px", ...iconStyles }}
              />
            </CustomIconButton>

            <SelectionMenu
              open={open}
              closeAction={this.handleClose}
              placement={placement}
              anchorRef={this.anchorEl}
              disablePortal={true}
              list={
                <List>
                  <ListItem disableRipple={true} classes={{ root: classes.menuHeadingItem }}>
                    <ListItemText
                      primary={
                        <FormattedMessage id="common.action.label" defaultMessage="Select Action" />
                      }
                      classes={{ primary: classes.menuHeadingListItemText }}
                    />
                  </ListItem>
                  {ddData.map(value =>
                    value == "Color" ? (
                      <ListItem
                        key={value}
                        button
                        disableRipple={true}
                        classes={{
                          root: classes.selectColorMenuItem,
                          selected: classes.statusMenuItemSelected,
                        }}
                        onClick={event => {
                          this.handleColorClick(event);
                        }}>
                        <ListItemText
                          primary={
                            <FormattedMessage id="common.action.color.label" defaultMessage={value} />
                          }
                          classes={{
                            primary: classes.statusItemText,
                          }}
                        />
                        <RightArrow
                          htmlColor={theme.palette.secondary.dark}
                          classes={{ root: classes.submenuArrowBtn }}
                        />
                        <div
                          id="colorPickerCnt"
                          className={classes.colorPickerCntLeft}
                          style={pickerOpen ? { display: "block" } : { display: "none" }}>
                          <ColorPicker
                            triangle="hide"
                            onColorChange={color => {
                              this.colorChange(color, this.props.issue);
                            }}
                            selectedColor={selectedColor}
                          />
                        </div>
                      </ListItem>
                    ) : (
                      <ListItem
                        key={value}
                        button
                        disableRipple={true}
                        classes={{ selected: classes.statusMenuItemSelected }}
                        onClick={event => this.handleOperations(event, value)}>
                        <ListItemText
                          primary={
                            <FormattedMessage id={this.getTranslatedId(value)} defaultMessage={value} />
                          }
                          classes={{
                            primary: classes.statusItemText,
                          }}
                        />
                      </ListItem>
                    )
                  )}
                </List>
              }
            />
          </div>
        </ClickAwayListener>
        <React.Fragment>
          {this.state.archiveFlag && (
            <ActionConfirmation
              open={archiveFlag}
              closeAction={this.handleDialogClose}
              cancelBtnText={
                <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
              }
              successBtnText={
                <FormattedMessage
                  id="common.action.archive.confirmation.archive-button.label"
                  defaultMessage="Archive"
                />
              }
              alignment="center"
              headingText={
                <FormattedMessage
                  id="common.action.archive.confirmation.title"
                  defaultMessage="Archive"
                />
              }
              iconType="archive"
              msgText={
                <FormattedMessage
                  id="common.action.archive.confirmation.issue.label"
                  defaultMessage="Are you sure you want to archive this issue?"
                />
              }
              successAction={this.handleArchive}
              btnQuery={archiveBtnQuery}
            />
          )}
        </React.Fragment>
        <React.Fragment>
          {this.state.unArchiveFlag ? (
            <ActionConfirmation
              open={popupFlag}
              closeAction={this.handleDialogClose}
              cancelBtnText={
                <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
              }
              successBtnText={
                <FormattedMessage
                  id="common.action.un-archive.confirmation.un-archive-button.label"
                  defaultMessage="Unarchive"
                />
              }
              alignment="center"
              iconType="unarchive"
              headingText={
                <FormattedMessage
                  id="common.action.un-archive.confirmation.title"
                  defaultMessage="Unarchive"
                />
              }
              msgText={
                <FormattedMessage
                  id="common.action.un-archive.confirmation.issue.label"
                  defaultMessage="Are you sure you want to unarchive this issue?"
                />
              }
              successAction={this.handleUnArchive}
              btnQuery={unarchiveBtnQuery}
            />
          ) : null}
        </React.Fragment>

        {this.state.deleteFlag ? (
          <DeleteConfirmDialog
            open={popupFlag}
            closeAction={this.handleDialogClose}
            cancelBtnText={
              <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
            }
            successBtnText={
              <FormattedMessage
                id="common.action.delete.confirmation.delete-button.label"
                defaultMessage="Delete"
              />
            }
            alignment="center"
            headingText={
              <FormattedMessage
                id="common.action.delete.confirmation.title"
                defaultMessage="Delete"
              />
            }
            successAction={this.handleDeleteIssue}
            msgText={
              <FormattedMessage
                id="common.action.delete.confirmation.issue.label"
                defaultMessage="Are you sure you want to delete this issue?"
              />
            }
            btnQuery={btnQuery}
          />
        ) : null}
        <MoveIssueDialog
          open={this.state.moveIssueToWorkspace}
          issueName={this.props.issue.title}
          successAction={this.handleMoveIssue}
          closeAction={this.handleMoveDialogClose}
          btnQuery={moveIssueBtnQuery}
          cancelBtnText={
            <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
          }
          exceptionText={""}
          headingText={"Move Issue to Workspace"}
          isMulti={false}
        />
      </Fragment>
    );
  }
}
IssueActionDropdown.defaultProps = {
  handleExportType: () => { },
};
export default compose(
  connect(null, {
    ArchiveIssue,
    moveIssueToWorkspace,
    FetchWorkspaceInfo,
  }),
  withStyles(combineStyles(itemStyles, menuStyles), {
    withTheme: true,
  })
)(IssueActionDropdown);
