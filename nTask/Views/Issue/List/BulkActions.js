import React, { Component, Fragment } from "react";
import { compose } from "redux";

import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button/";
import listStyles from "./styles";
import IconMenu from "../../../components/Menu/TaskMenus/IconMenu";
import MenuList from "../../../components/Menu/TaskMenus/MemberListMenu";
import helper from "../../../helper";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import DeleteConfirmDialog from "../../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import ColorPicker from "../../../components/Dropdown/ColorPicker/ColorPicker";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Popper from "@material-ui/core/Popper";
import Grow from "@material-ui/core/Grow";
import { FormattedMessage, injectIntl } from "react-intl";
import Typography from "@material-ui/core/Typography";
import MoveIssueDialog from "../../../components/Dialog/MoveIssueDialog/MoveIssueDialog";

class BulkActions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDateOpen: false,
      dueDateOpen: false,
      inputType: "",
      showConfirmation: false,
      bulkDeleteBtnQuery: "",
      archiveBtnQuery: "",
      unarchiveBtnQuery: "",
      anchorEl: null,
      open: false,
      selectedColor: "#D9E3F0",
      moveIssueBtnQuery: "",
    };

    this.handleStartDateClose = this.handleStartDateClose.bind(this);
    this.handleStartDateToggle = this.handleStartDateToggle.bind(this);
    this.handleDueDateClose = this.handleDueDateClose.bind(this);
    this.handleDueDateToggle = this.handleDueDateToggle.bind(this);
    this.handleBulkClick = this.handleBulkClick.bind(this);
    this.showConfirmationPopup = this.showConfirmationPopup.bind(this);
    this.closeConfirmationPopup = this.closeConfirmationPopup.bind(this);
  }

  showConfirmationPopup(inputType) {
    this.setState({
      showConfirmation: true,
      inputType: inputType || "",
    });
  }

  closeConfirmationPopup() {
    this.setState({ showConfirmation: false, inputType: "" });
  }

  handleBulkClick() {
    const { selectedIssueObj, permissionObject, workspaceIssuePer } = this.props;

    let filterIssueHaveDeletePer = selectedIssueObj.reduce((res, cv) => { /** filter those issues which user have the permission to delete */
      if (cv.projectId) {
        if (permissionObject[cv.projectId].issue.delete.cando) res.push(cv);
      } else if (workspaceIssuePer.delete.cando) {
        res.push(cv);
      }
      return res;
    }, []);

    //Generating array of issueIds
    let issueIdsArr = filterIssueHaveDeletePer.map(i => {
      return i.row.id;
    });

    let inputType = this.state.inputType;
    this.setState({ inputType: "" }, () => {
      const type = helper.RETURN_BULKACTIONTYPES(inputType);
      const data = {
        type,
        issueIds: issueIdsArr,
      };
      this.setState({ bulkDeleteBtnQuery: "progress" });
      this.props.BulkUpdate(data, () => {
        this.setState({ bulkDeleteBtnQuery: "" });
      });
    });
  }

  handleStartDateClose(completeDateFormat, completePlannedDate, selectedIdsList) {
    const type = helper.RETURN_BULKACTIONTYPES("StartDate");
    let data = {
      type,
      issueIds: selectedIdsList,
      planDate: completePlannedDate,
      actualDate: completeDateFormat,
    };
    this.props.BulkUpdateTask(data);
    this.setState({ startDateOpen: false });
  }
  handleStartDateToggle() {
    this.setState(state => ({ startDateOpen: !state.startDateOpen }));
  }
  handleDueDateClose(completeDateFormat, completePlannedDate, selectedIdsList) {
    const type = helper.RETURN_BULKACTIONTYPES("DueDate");
    let data = {
      type,
      issueIds: selectedIdsList,
      planDate: completePlannedDate,
      actualDate: completeDateFormat,
    };
    this.props.BulkUpdate(data);
    this.setState({ dueDateOpen: false });
  }
  handleDueDateToggle() {
    this.setState(state => ({ dueDateOpen: !state.dueDateOpen }));
  }
  handleArchive = e => {
    if (e) e.stopPropagation();
    let inputType = this.state.inputType;
    const type = helper.RETURN_BULKACTIONTYPES(inputType);
    const data = {
      type,
      issueIds: this.props.selectedIdsList,
    };
    this.setState({ archiveBtnQuery: "progress" }, () => {
      this.props.BulkUpdate(data, () => {
        this.setState({
          archiveBtnQuery: "",
          inputType: "",
          showConfirmation: false,
        });
      });
    });
  };
  handleUnArchive = e => {
    if (e) e.stopPropagation();
    let inputType = this.state.inputType;
    const type = helper.RETURN_BULKACTIONTYPES(inputType);
    const data = {
      type,
      issueIds: this.props.selectedIdsList,
    };
    this.setState({ unarchiveBtnQuery: "progress" }, () => {
      this.props.BulkUpdate(data, () => {
        this.setState({
          unarchiveBtnQuery: "",
          inputType: "",
          showConfirmation: false,
        });
      });
    });
  };

  handleColorPickerBtnClick = event => {
    const { currentTarget } = event;
    this.setState(state => ({
      anchorEl: currentTarget,
      open: !state.open,
    }));
  };

  handleClickAway = () => {
    this.setState({ open: false });
  };

  handleColorChange = color => {
    const { selectedIdsList } = this.props;
    this.setState({ selectedColor: color }, () => {
      const type = helper.RETURN_BULKACTIONTYPES("Color");
      let data = {
        type,
      };
      data["colorCode"] = this.state.selectedColor;
      data.issueIds = selectedIdsList;
      this.props.BulkUpdate(data, () => {});
    });
  };
  getTranslatedId(value) {
    switch (value) {
      case "Color":
        value = "common.action.color.label";
        break;
      case "Archive":
        value = "common.action.archive.confirmation.archive-button.label";
        break;
      case "Delete":
        value = "common.action.delete.confirmation.delete-button.label";
        break;
      case "Unarchive":
        value = "common.action.un-archive.confirmation.title";
        break;
      case "Add Assignee":
        value = "common.bulk-action.addAssignee";
        break;
      case "Cancel":
        value = "common.action.cancel.label";
        break;
      case "Set Priority":
        value = "common.bulk-action.priority";
        break;
      case "Set Status":
        value = "common.bulk-action.status";
        break;
      case "Set Severity":
        value = "common.bulk-action.severity";
        break;
      case "Set Task":
        value = "common.bulk-action.selectTask";
        break;
    }
    return value;
  }
  handleMoveIssue = (e, team) => {
    const { selectedIssueObj } = this.props;

    if (e) e.stopPropagation();
    this.setState({ moveIssueBtnQuery: "progress" }, () => {
      let filterIssue = selectedIssueObj.filter(
        i => i.row.linkedTasks.length == 0
      ); /** filter those issues which user have the permission to delete */

      //Generating array of issueIds
      let issueIdsArr = filterIssue.map(i => {
        return i.row.id;
      });
      let data = {
        issuesId: issueIdsArr,
        workspaceId: team.teamId,
      };
      this.props.moveIssueToWorkspace(
        data,
        succ => {
          if (filterIssue.length !== selectedIssueObj.length)
            this.props.showSnackBar(
              "Some of your issues cannot move to another workspace because these are linked to tasks",
              "info"
            );
          this.setState({ moveIssueBtnQuery: "" });
          this.closeConfirmationPopup();
        },
        fail => {
          this.setState({ moveIssueBtnQuery: "" });
          this.closeConfirmationPopup();
        }
      );
    });
  };
  render() {
    const {
      classes,
      theme,
      selectedIdsList,
      isArchivedFilter,
      workspaceIssuePer,
      selectedIssueObj,
      intl,
      permissionObject
    } = this.props;
    const {
      dueDateOpen,
      startDateOpen,
      inputType,
      showConfirmation,
      bulkDeleteBtnQuery,
      archiveBtnQuery,
      unarchiveBtnQuery,
      open,
      anchorEl,
      selectedColor,
      moveIssueBtnQuery,
    } = this.state;
    const id = open ? "simple-popper" : null;
    const styles = (action, i, arr) => {
      return i == 0
        ? { borderRadius: "4px 0 0 4px" }
        : i == arr.length - 1
        ? { borderRadius: "0 4px 4px 0", borderLeft: "none" }
        : { borderLeft: "none", borderRadius: 0 };
    };

    let BulkActionsType = [];
    if (isArchivedFilter) {
      BulkActionsType = [
        selectedIdsList.length +
          " " +
          intl.formatMessage({
            id: "common.item-selected.label",
            defaultMessage: "items Selected",
          }),
        "Unarchive",
        "Delete",
      ];
    } else {
      BulkActionsType = [
        selectedIdsList.length +
          " " +
          intl.formatMessage({
            id: "common.item-selected.label",
            defaultMessage: "items Selected",
          }),
        "Color",
        "Add Assignee",
        "Select Task",
        "Due Date",
        "Set Status",
        "Set Severity",
        "Set Priority",
        "Archive",
        "Delete",
        "Move",
      ];
    }
    let filterIssueHaveNotDeletePer = selectedIssueObj.reduce((res, cv) => { /** filter those issues which user have the permission to delete */
      if (cv.projectId) {
        if (!permissionObject[cv.projectId].issue.delete.cando) res.push(cv);
      } else if (!workspaceIssuePer.delete.cando) {
        res.push(cv);
      }
      return res;
    }, []);

    return (
      <div className={classes.BulkActionsCnt}>
        {inputType === "Archive" ? (
          <ActionConfirmation
            open={showConfirmation && workspaceIssuePer.archive.cando}
            closeAction={this.closeConfirmationPopup}
            cancelBtnText={
              <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
            }
            successBtnText={
              <FormattedMessage
                id="common.archived.issue.messagea"
                defaultMessage="Archive issues"
              />
            }
            alignment="center"
            headingText={
              <FormattedMessage
                id="common.action.archive.confirmation.archive-button.label"
                defaultMessage="Archive"
              />
            }
            iconType="archive"
            msgText={
              <FormattedMessage
                id="common.archived.issue.messageb"
                defaultMessage={`Are you sure you want to archive these ${selectedIdsList.length} issues?`}
                values={{ i: selectedIdsList.length }}
              />
            }
            successAction={this.handleArchive}
            btnQuery={archiveBtnQuery}
          />
        ) : inputType === "Unarchive" ? (
          <ActionConfirmation
            open={showConfirmation && workspaceIssuePer.unarchives.cando}
            closeAction={this.closeConfirmationPopup}
            cancelBtnText={
              <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
            }
            successBtnText={
              <FormattedMessage
                id="common.un-archived.issue.label"
                defaultMessage="Unarchive issues"
              />
            }
            alignment="center"
            iconType="unarchive"
            headingText={
              <FormattedMessage
                id="common.action.un-archive.confirmation.title"
                defaultMessage="Unarchive"
              />
            }
            msgText={
              <FormattedMessage
                id="common.un-archived.issue.messagea"
                defaultMessage={`Are you sure you want to unarchive these ${selectedIdsList.length} issues?`}
                values={{ i: selectedIdsList.length }}
              />
            }
            successAction={this.handleUnArchive}
            btnQuery={unarchiveBtnQuery}
          />
        ) : inputType === "Delete" ? (
          <DeleteConfirmDialog
            open={showConfirmation && workspaceIssuePer.delete.cando}
            closeAction={this.closeConfirmationPopup}
            cancelBtnText={
              <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
            }
            successBtnText={
              <FormattedMessage
                id="common.action.delete.confirmation.delete-button.label"
                defaultMessage="Delete"
              />
            }
            alignment="center"
            headingText={
              <FormattedMessage
                id="common.action.delete.confirmation.delete-button.label"
                defaultMessage="Delete"
              />
            }
            successAction={this.handleBulkClick}
            msgText={
              <FormattedMessage
                id="common.action.delete.issue.label"
                defaultMessage={`Are you sure you want to delete these ${selectedIdsList.length} issues?`}
                values={{ i: selectedIdsList.length }}
              />
            }
            btnQuery={bulkDeleteBtnQuery}
            disabled={selectedIdsList.length - filterIssueHaveNotDeletePer.length == 0}>
            <>
              {filterIssueHaveNotDeletePer.length > 0 && (
                <Typography
                  variant="h5"
                  style={{
                    userSelect: "none",
                    marginBottom: 20,
                    // textAlign: "center",
                  }}>
                  {`Note : You cannot perform this action because some of the issues that you are trying to delete are associated with those tasks which are linked to a project on which you do not have deletion rights. If you continue, only those issues will be deleted which are not associated with any task.`}
                </Typography>
              )}
              <Typography variant="h5" style={{ userSelect: "none", width: "100%" }}>
                <FormattedMessage
                  id="common.action.delete.issue.label"
                  values={{ i: selectedIdsList.length - filterIssueHaveNotDeletePer.length }}
                  defaultMessage={`Are you sure you want to delete these ${selectedIdsList.length -
                    filterIssueHaveNotDeletePer.length} issues?`}
                />
              </Typography>
            </>
          </DeleteConfirmDialog>
        ) : inputType === "Move" ? (
          <MoveIssueDialog
            open={showConfirmation}
            issueName={""}
            successAction={this.handleMoveIssue}
            closeAction={this.closeConfirmationPopup}
            btnQuery={moveIssueBtnQuery}
            cancelBtnText={
              <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
            }
            exceptionText={""}
            headingText={"Move Issues to Workspace"}
            isMulti={true}
          />
        ) : null}

        {BulkActionsType.map(
          (action, i, arr) => {
            return i == 1 && !isArchivedFilter ? (
              <>
                <Button
                  disabled={false}
                  style={styles(action, i, arr)}
                  variant="outlined"
                  onClick={this.handleColorPickerBtnClick}
                  classes={{ outlined: classes.BulkActionBtn }}
                  selectedIdsList={selectedIdsList}>
                  <FormattedMessage id={this.getTranslatedId(action)} defaultMessage={action} />
                </Button>
                <Popper id={id} open={open} anchorEl={anchorEl} disablePortal transition>
                  {({ TransitionProps, placement }) => (
                    <Grow
                      {...TransitionProps}
                      id="menu-list-grow"
                      style={{
                        transformOrigin: placement === "bottom" ? "center top" : "left bottom",
                      }}>
                      <ClickAwayListener onClickAway={this.handleClickAway}>
                        <div id="colorPickerCnt">
                          <ColorPicker
                            triangle="hide"
                            onColorChange={color => {
                              this.handleColorChange(color);
                            }}
                            selectedColor={selectedColor}
                          />
                        </div>
                      </ClickAwayListener>
                    </Grow>
                  )}
                </Popper>
              </>
            ) : i == 2 && !isArchivedFilter && workspaceIssuePer.issueDetail.issueAssignee.isAllowAdd ? (
              // <Button
              //   disabled={i == 0 ? true : false}
              //   style={styles(action, i, arr)}
              //   variant="outlined"
              //   classes={{ outlined: classes.BulkActionBtn }}
              //   selectedIdsList={this.state.selectedIds}
              // >
              <MenuList
                assigneeList={[]}
                placementType="bottom-end"
                selectedIdsList={this.props.selectedIdsList}
                BulkUpdate={this.props.BulkUpdate}
                viewType="issue"
                buttonStyles={styles(action, i, arr)}
                checkAssignee={true}
                checkedType="multi"
                listType="assignee"
                buttonText={"Add Assignee"}
                isBulk={true}
                keyType="AddAssignee"
              />
            ) : //  </Button>
            i == 3 && !isArchivedFilter && workspaceIssuePer.issueDetail.issueTask.isAllowEdit ? (
              <MenuList
                linkedTasks={[]}
                placementType="bottom-end"
                buttonStyles={styles(action, i, arr)}
                viewType="issue"
                isTask={true}
                buttonText={"Select Task"}
                isBulk={true}
                checkedType={"multi"}
                keyType="AddTask"
                selectedIdsList={this.props.selectedIdsList}
                BulkUpdate={this.props.BulkUpdate}
              />
            ) : i == 4 && !isArchivedFilter ? null : i == 4 &&
              !isArchivedFilter &&
              workspaceIssuePer.issueDetail.issueStatus.isAllowEdit ? (
              <IconMenu
                menuType="issueStatus"
                heading={
                  <FormattedMessage id={this.getTranslatedId(action)} defaultMessage={action} />
                }
                keyType="Status"
                isBulk={true}
                buttonText={
                  <FormattedMessage id={this.getTranslatedId(action)} defaultMessage={action} />
                }
                buttonStyles={styles(action, i, arr)}
                selectedIdsList={this.props.selectedIdsList}
                BulkUpdate={this.props.BulkUpdate}
                viewType={"issue"}
              />
            ) : i == 6 && !isArchivedFilter && workspaceIssuePer.issueDetail.issueSeverity.isAllowEdit ? (
              <IconMenu
                menuType="severity"
                // heading={"Set Severity"}
                heading={this.props.intl.formatMessage({
                  id: "common.bulk-action.severity",
                  defaultMessage: "Set Severity",
                })}
                isBulk={true}
                keyType="Severity"
                buttonText={
                  <FormattedMessage id={this.getTranslatedId(action)} defaultMessage={action} />
                }
                buttonStyles={styles(action, i, arr)}
                selectedIdsList={this.props.selectedIdsList}
                BulkUpdate={this.props.BulkUpdate}
                viewType={"issue"}
              />
            ) : i == 7 && !isArchivedFilter && workspaceIssuePer.issueDetail.issuePriority.isAllowEdit ? (
              <IconMenu
                menuType="priority"
                heading={this.props.intl.formatMessage({
                  id: "common.bulk-action.priority",
                  defaultMessage: "Set Priority",
                })}
                isBulk={true}
                buttonText={
                  <FormattedMessage id={this.getTranslatedId(action)} defaultMessage={action} />
                }
                buttonStyles={styles(action, i, arr)}
                selectedIdsList={this.props.selectedIdsList}
                BulkUpdate={this.props.BulkUpdate}
                keyType="Priority"
                viewType={"issue"}
              />
            ) : (
              <Button
                disabled={i == 0 ? true : false}
                style={styles(action, i, arr)}
                variant="outlined"
                classes={{ outlined: classes.BulkActionBtn }}
                onClick={this.showConfirmationPopup.bind(this, action)}>
                <FormattedMessage id={this.getTranslatedId(action)} defaultMessage={action} />
              </Button>
            );
          }
          // ) : i == 1 ? (

          // ) : i == 3 ? (
          //   <Fragment>
          //     <Button
          //       disabled={i == 0 ? true : false}
          //       style={styles(action, i, arr)}
          //       variant="outlined"
          //       classes={{ outlined: classes.BulkActionBtn }}
          //       onClick={this.handleStartDateToggle}
          //       buttonRef={node => {
          //         this.startDateButton = node;
          //       }}
          //     >
          //       {action}
          //     </Button>
          //   </Fragment>
          // ) : i == 4 ? (

          // ) : (
          //   <Button
          //     disabled={i == 0 ? true : false}
          //     style={styles(action, i, arr)}
          //     variant="outlined"
          //     classes={{ outlined: classes.BulkActionBtn }}
          //   >
          //     {action}
          //   </Button>
          // );
        )}
      </div>
    );
  }
}

export default compose(injectIntl, withStyles(listStyles, { withTheme: true }))(BulkActions);
