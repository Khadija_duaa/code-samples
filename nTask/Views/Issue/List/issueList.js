import { AgGridColumn } from "@ag-grid-community/react";
import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import AddIcon from "@material-ui/icons/Add";
import fileDownload from "js-file-download";
import isEmpty from "lodash/isEmpty";
import isEqual from "lodash/isEqual";
import isUndefined from "lodash/isUndefined";
import moment from "moment";
import { withSnackbar } from "notistack";
import { Circle } from "rc-progress";
import React, { useCallback, useEffect, useRef, useState } from "react";
import { FormattedMessage, injectIntl } from "react-intl";
import { useDispatch, useSelector } from "react-redux";
import { compose } from "redux";
import Issuecmp from "../../../components/BulkActionsIssue/Issue.cmp";
import CustomFieldRenderer from "../../../components/CustomTable2/CustomFieldsColumn/CustomFieldRenderer";
import { grid } from "../../../components/CustomTable2/gridInstance";
import GroupByComponents from "../../../components/CustomTable2/GroupByComponents/GroupByComponents";
import loadable from '@loadable/component'
const CustomTable = loadable(() => import("../../../components/CustomTable2/listViewTable.cmp"));
import CustomDatePicker from "../../../components/DatePicker/DatePicker/ListViewDatePicker";
import AssigneeDropdown from "../../../components/Dropdown/AssigneeDropdown";
import SearchDropdown from "../../../components/Dropdown/SearchDropdown";
import StatusDropdown from "../../../components/Dropdown/StatusDropdown/Dropdown";
import EmptyState from "../../../components/EmptyStates/EmptyState";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";
import Stared from "../../../components/Starred/Starred";
import helper from "../../../helper";
import { generateTaskData } from "../../../helper/generateSelectData";
import {
  generateTaskDropdownData,
  priorityData,
  severity as severityData,
  statusData,
  typeData,
} from "../../../helper/issueDropdownData";
import { issueDetailDialogState } from "../../../redux/actions/allDialogs";
import {
  createIssue,
  exportBulkIssue,
  getSavedFilters,
  UpdateIssueCustomField,
  updateIssueData,
  updateIssueObject,
} from "../../../redux/actions/issues";
import ColumnSelector from "../../../redux/selectors/columnSelector";
import ProjectPermissionSelector from "../../../redux/selectors/projectPermissionSelector";
import { emptyIssue } from "../../../utils/constants/emptyIssue";
import IssueActionDropdown from "../IssueActionDropdown/IssueActionDropdown";
import { headerProps } from "./constants";
import { issueColumnDefs, sortAlphabetically } from "./issueColumns";
import { doesFilterPass } from "./IssueFilter/issueFilter.utils";
import IssueFilter from "./IssueFilter/issueFilter.view";
import issueListStyles from "./issueList.style";
import { addPendingHandler } from "../../../redux/actions/backProcesses";
import { excludeOffdays, getCalendar } from "../../../helper/dates/dates";
import mixpanel from 'mixpanel-browser';
import { MixPanelEvents } from '../../../mixpanel';

// const defaultColDef = { minWidth: 200, headerClass: "customHeader" };
const IssueList = React.memo(({ classes, theme, intl, enqueueSnackbar, style }) => {
  const state = useSelector((state) => {
    return {
      issueColumns: ColumnSelector(state).issue.columns,
      sections: ColumnSelector(state).issue.sections,
      nonSectionFields: ColumnSelector(state).issue.nonSectionFields,
      issues: state.issues.data,
      projects: state.projects.data,
      allTasks: state.tasks.data,
      members: state.profile.data.member.allMembers,
      workspaceTemplates: state.workspaceTemplates.data,
      globalTimerTaskState: state.globalTimerTask,
      permissionObject: ProjectPermissionSelector(state),
      workspaceIssuePer: state.workspacePermissions.data.issue,
      workspaceStatus: state.workspaceTemplates.data.defaultWSTemplate,
      profileState: state.profile.data,
      quickFilters: state.issues.quickFilters,
      issueFilters: state.issues.issueFilters,
    };
  });
  const sectionGroup = localStorage.getItem("issueSectionGrouping");
  const dispatch = useDispatch();
  const [selectedIssues, setSelectedIssues] = useState([]);
  const [mappingDialogState, setMappingDialogState] = useState({
    tempProject: null,
    openStatusDialog: false,
    newTemplateItem: {},
    newTemplateItem: {},
    oldStatusItem: {},
    tempTask: {},
  });
  const timerState = useRef(null);
  const statusTemplates = useRef(null);
  const [sectionGrouping, setSectionGrouping] = useState(sectionGroup === "true" ? true : false);
  const {
    issueColumns = [],
    issues = [],
    projects,
    members,
    sections = [],
    nonSectionFields = [],
    globalTimerTaskState,
    workspaceIssuePer,
    permissionObject,
    workspaceStatus,
    profileState,
    quickFilters,
    issueFilters,
    allTasks = [],
  } = state;

  useEffect(() => {
    getSavedFilters("issue", dispatch);

    return () => {
      grid.grid = null;
    };
  }, []);

  const updateIssue = (issue, obj) => {
    updateIssueData(
      { issue, obj },
      dispatch,
      //Success
      (issue) => {
        if (grid.grid) {
          const rowNode = grid.grid && grid.grid.getRowNode(issue.id);
          rowNode.setData(issue);
        }
      },
      (err) => {
        if (err && err.data) showSnackBar(err.data.message, "error");
      }
    );
  };
  //Handle planned/actual date save
  const handleDateSave = (type, date, issue, time) => {
    const formatDate = helper.RETURN_CUSTOMDATEFORMAT(date);

    let obj;
    let key;
    switch (type) {
      case "actualStartDate":
        obj = { actualStartDate: formatDate, actualStartTime: time };
        key = "actualdate";
        break;
      case "actualDueDate":
        obj = { actualDueDate: formatDate, actualDueTime: time };
        key = "actualdate";
        break;
      case "startDate":
        obj = { startDate: formatDate, startTime: time };
        key = "planneddate";
        break;
      case "dueDate":
        obj = { dueDate: formatDate, dueTime: time };
        key = "planneddate";
        break;
      default:
    }

    updateIssue(issue, obj);
  };
  const handleTaskStared = (stared, obj) => {
    updateIssue(obj, { isStared: stared });
  };
  const handlePriorityChange = (priority, obj) => {
    // Handle Priority change
    updateIssue(obj, { priority: priority.value });
  };
  const handleSeverityChange = (severity, obj) => {
    // Handle severity change
    updateIssue(obj, { severity: severity.value });
  };
  const handleTypeChange = (type, obj) => {
    // Handle type change
    updateIssue(obj, { type: type.value });
  };
  const handleTaskChange = (task, obj) => {
    // Handle task change
    updateIssue(obj, {
      linkedTasks: task.length ? [task[0].id] : [],
      projectId: !obj.projectId && task.length ? task[0].obj.projectId : obj.projectId,
    });
  };
  const handleUpdateAssignee = (assignees, obj) => {
    // Handle Assignee change
    const assigneeList = assignees.map((a) => a.userId);
    updateIssue(obj, { assignee: assigneeList });
  };

  const handleStatusChange = (status, obj) => {
    updateIssue(obj, { status: status.value });
  };

  const handleProjectChange = (newValue, issue) => {
    const projectId = newValue.length ? newValue[0].id : "";
    const selectedIssues = allTasks.filter((task) => {
      return issue.linkedTasks.indexOf(task.taskId) > -1;
    });

    let linkTask = selectedIssues.length
      ? selectedIssues[0].projectId == "" || !selectedIssues[0].projectId
        ? []
        : issue.linkedTasks
      : issue.linkedTasks;

    const obj = {
      projectId: projectId,
      linkedTasks: newValue.length == 0 ? [] : linkTask,
    };
    updateIssue(issue, obj);
  };
  //Handle Add Task
  const handleAddIssue = (data, callback) => {
    //Post Obj is object to be posted to backend for task creation
    const postObj = { title: data.value, clientId: data.clientId };

    //Dispatch Obj is object that is dispatched before the api call for quick entry
    const dispatchObj = {
      ...emptyIssue,
      title: data.value,
      clientId: data.clientId,
      id: data.clientId,
      issueId: data.clientId,
      type: "Bug",
      isNew: true,
    };
    //Calling grid add method to add task
    // callback(dispatchObj, "add");
    createIssue(postObj, dispatch, dispatchObj, (res) => {
      mixpanel.track(MixPanelEvents.IssueCreationEvent, res);
      //Calling callback to edit the added record with the actual data
      // callback(res, "edit");
    });
  };

  // Generate list of all projects for dropdown understandable form
  const generateProjectDropdownData = (issue) => {
    let filteredProjects = projects.filter((p) => p.projectId !== issue.projectId);
    const projectsArr = filteredProjects.map((project) => {
      return { label: project.projectName, id: project.projectId, obj: issue };
    });
    return projectsArr;
  };
  //Update Row if  Task Timer value is changed
  useEffect(() => {
    if (!isEmpty(grid.grid)) {
      const task = timerState.current
        ? timerState.current.task
        : globalTimerTaskState
          ? globalTimerTaskState.task
          : null;
      const rowNode = task && grid.grid.getRowNode(task.id);
      rowNode && rowNode.setData(task);
      timerState.current = globalTimerTaskState;
    }
  }, [globalTimerTaskState, grid.grid]);

  // export issue api function
  const handleExportIssue = (props) => {
    const selectedNodes = grid.grid.getSelectedNodes();
    const selectedNodesLength = selectedNodes?.length || 1;
    const postObj = selectedNodes.length
      ? { issueIds: selectedNodes.map((i) => i.id) }
      : { issueIds: [props.node.data.id] };

    const obj = {
      type: 'issues',
      apiType: 'post',
      data: postObj,
      fileName: 'issues.xlsx',
      apiEndpoint: 'api/issues/exportbulk',
    }
    addPendingHandler(obj, dispatch);

    // exportBulkIssue(postObj, dispatch, 
    //   (res) => {
    //   fileDownload(res.data, "issues.xlsx");
    //   showSnackBar(`${selectedNodesLength} issues exported successfully`, "success");
    // },
    // (err) => {
    //   if (err.response.status == 405) {
    //     showSnackBar("You don't have sufficient rights to export issues", "error");
    //   }  else {
    //     showSnackBar("oops! There seems to be an issue, please contact support@ntaskmanager.com", "error");
    //   }
    // });
  };
  // export all issue api function
  const handleExportAllIssues = (props) => {
    let allNodes = [];
    grid.grid.forEachNodeAfterFilter((node) => allNodes.push(node.data));
    const postObj = allNodes.length && { issueIds: allNodes.map((i) => i.id) };

    const obj = {
      type: 'issues',
      apiType: 'post',
      data: postObj,
      fileName: 'issues.xlsx',
      apiEndpoint: 'api/issues/exportbulk',
    }
    addPendingHandler(obj, dispatch);

    // exportBulkIssue(postObj, dispatch, 
    //   (res) => {
    //   fileDownload(res.data, "issues.xlsx");
    //   showSnackBar(`${allNodes.length} issues exported successfully`, "success");
    // },
    // (err) => {
    //   if (err.response.status == 405) {
    //     showSnackBar("You don't have sufficient rights to export issues", "error");
    //   }  else {
    //     showSnackBar("oops! There seems to be an issue, please contact support@ntaskmanager.com", "error");
    //   }
    // });
  };
  const StatusDropdownCmp = (row = { data: {} }) => {
    const rowData = row.data ? row.data : { data: {} };
    const issueStatusData = statusData(theme, classes, intl);
    let selectedStatus = rowData
      ? issueStatusData.find((item) => item.value == rowData.status) || issueStatusData[0]
      : issueStatusData[0];
    let issuePermission =
      rowData && rowData.projectId
        ? !permissionObject[rowData.projectId]
          ? workspaceIssuePer
          : permissionObject[rowData.projectId].issue
        : workspaceIssuePer;
    const isArchive = rowData.isArchive;
    return (
      <StatusDropdown
        onSelect={(status) => {
          handleStatusChange(status, rowData);
        }}
        buttonType={"listButton"}
        option={selectedStatus}
        options={issueStatusData}
        toolTipTxt={selectedStatus.label}
        writeFirst={true}
        disabled={isArchive || !issuePermission.issueDetail.issueStatus.isAllowEdit}
        preSelection={false}
        dropdownProps={{
          disablePortal: false,
        }}
      />
    );
  };
  const PriorityDropdown = (row) => {
    const rowData = row.data ? row.data : { data: {} };
    const issuePriority = priorityData(theme, classes, intl);
    const selectedPriority = issuePriority.find((p) => p.value == rowData.priority);
    let issuePermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceIssuePer
          : permissionObject[rowData.projectId].issue
        : workspaceIssuePer;
    const isArchive = rowData.isArchive;
    return (
      <StatusDropdown
        onSelect={(priority) => {
          handlePriorityChange(priority, row.data);
        }}
        option={selectedPriority}
        options={issuePriority}
        buttonType={"listIconButton"}
        toolTipTxt={
          <FormattedMessage id="common.action.priority.label" defaultMessage="Issue Priority" />
        }
        btnProps={{
          customClasses: {
            label: classes.priorityLabel,
          },
        }}
        dropdownProps={{
          disablePortal: false,
        }}
        disabled={isArchive || !issuePermission.issueDetail.issuePriority.isAllowEdit}
      />
    );
  };
  const SeverityDropdown = (row) => {
    const rowData = row.data ? row.data : { data: {} };
    const issueSeverity = severityData(theme, classes, intl);
    const selectedSeverity = issueSeverity.find((p) => p.value == rowData.severity);
    let issuePermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceIssuePer
          : permissionObject[rowData.projectId].issue
        : workspaceIssuePer;
    const isArchive = rowData.isArchive;
    return (
      <StatusDropdown
        onSelect={(severity) => {
          handleSeverityChange(severity, row.data);
        }}
        option={selectedSeverity}
        options={issueSeverity}
        buttonType={"listButton"}
        toolTipTxt={
          <FormattedMessage id="common.action.priority.label" defaultMessage="Issue Severity" />
        }
        btnProps={{
          customClasses: {
            label: classes.priorityLabel,
          },
        }}
        dropdownProps={{
          disablePortal: false,
        }}
        disabled={isArchive || !issuePermission.issueDetail.issueSeverity.isAllowEdit}
      />
    );
  };
  const TypeDropdown = (row) => {
    const rowData = row.data ? row.data : { data: {} };
    const issueTypeData = typeData(theme, classes, intl);
    const selectedType = issueTypeData.find((p) => p.value == rowData.type);
    let issuePermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceIssuePer
          : permissionObject[rowData.projectId].issue
        : workspaceIssuePer;
    const isArchive = rowData.isArchive;
    return (
      <StatusDropdown
        onSelect={(type) => {
          handleTypeChange(type, row.data);
        }}
        option={selectedType}
        options={issueTypeData}
        buttonType={"listIconButton"}
        toolTipTxt={
          <FormattedMessage id="common.action.priority.label" defaultMessage="Issue Severity" />
        }
        btnProps={{
          customClasses: {
            label: classes.typeLabel,
          },
        }}
        dropdownProps={{
          disablePortal: false,
        }}
        disabled={isArchive || !issuePermission.issueDetail.issueType.isAllowEdit}
      />
    );
  };
  const IssueTitleCellRenderer = (row = { data: {} }) => {
    const rowData = row.data ? row.data : { data: {} };
    return (
      <div className={classes.taskTitleCnt}>
        <div className={`${classes.taskTitleTextCnt} wrapText`} title={rowData.title}>
          <span className={classes.taskTitle}>{rowData.title} </span>
        </div>
        {rowData.isNew && <div style={{ minWidth: 55 }}>
          <span className={classes.newTag}>New</span>
        </div>}
        <Stared
          isStared={rowData.isStared}
          handleCallback={(isStared) => handleTaskStared(isStared, rowData)}
          diabled={rowData.isArchive}
        />
      </div>
    );
  };
  const ActualStartDateRenderer = (row) => {
    const rowData = row.data ? row.data : { data: {} };
    const { actualStartDate, actualStartTime, actualDueDate } = rowData;
    let issuePermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceIssuePer
          : permissionObject[rowData.projectId].issue
        : workspaceIssuePer;
    const isArchive = rowData.isArchive;
    return (
      <CustomDatePicker
        date={actualStartDate && moment(actualStartDate)}
        dateFormat="MMM DD, YYYY"
        timeInput={true}
        onSelect={(date, time) => {
          handleDateSave("actualStartDate", date, rowData, time);
        }}
        selectedTime={actualStartTime}
        timeInputLabel={intl.formatMessage({
          id: "meeting.creation-dialog.form.start-time.label",
          defaultMessage: "Start Time",
        })}
        datePickerProps={{
          filterDate: (date) => {
            return actualDueDate
              ? moment(date).isBefore(actualDueDate, "day") || moment(date).isSame(actualDueDate, "day")
              : true;
          },
        }}
        // btnProps={{ className: classes.datePickerCustomStyle }}
        disabled={isArchive || !issuePermission.issueDetail.issueActualstartEndDate.isAllowEdit}
        filterDate={moment()}
        style={{ display: "flex", alignItems: "center", height: "100%" }}
        PopperProps={{ disablePortal: false }}
        btnType="transparent"
        placeholder={"Select"}
        icon={false}
      />
    );
  };
  const ActualDueDateRenderer = (row) => {
    const rowData = row.data ? row.data : { data: {} };
    const { actualDueDate, actualDueTime, actualStartDate } = rowData;

    let issuePermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceIssuePer
          : permissionObject[rowData.projectId].issue
        : workspaceIssuePer;
    const isArchive = rowData.isArchive;
    return (
      <CustomDatePicker
        date={actualDueDate && moment(actualDueDate)}
        dateFormat="MMM DD, YYYY"
        timeInput={true}
        // datePickerProps={{
        //   maxDate: plannedMaxDate,
        // }}
        onSelect={(date, time) => {
          handleDateSave("actualDueDate", date, rowData, time);
        }}
        selectedTime={actualDueTime}
        timeInputLabel={intl.formatMessage({
          id: "meeting.creation-dialog.form.start-time.label",
          defaultMessage: "Start Time",
        })}
        datePickerProps={{
          filterDate: (date) => {
            return actualStartDate
              ? moment(date).isAfter(actualStartDate, "day") || moment(date).isSame(actualStartDate, "day")
              : true;
          },
        }}
        // btnProps={{ className: classes.datePickerCustomStyle }}
        disabled={isArchive || !issuePermission.issueDetail.issueActualstartEndDate.isAllowEdit}
        style={{ display: "flex", alignItems: "center", height: "100%" }}
        PopperProps={{ disablePortal: false }}
        btnType="transparent"
        placeholder={"Select"}
        icon={false}
      />
    );
  };
  const PlannedStartDateRenderer = (row) => {
    const rowData = row.data ? row.data : { data: {} };
    const { startDate, startTime, dueDate } = rowData;
    let issuePermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceIssuePer
          : permissionObject[rowData.projectId].issue
        : workspaceIssuePer;
    const obj = rowData.projectId ? { projectId: rowData.projectId } : { workspaceId: profileState.loggedInTeam };
    const calendarWorkingdays = getCalendar(obj) || null;
    const isArchive = rowData.isArchive;
    return (
      <CustomDatePicker
        date={startDate && moment(startDate)}
        dateFormat="MMM DD, YYYY"
        timeInput={true}
        // datePickerProps={{
        //   maxDate: plannedMaxDate,
        // }}
        onSelect={(date, time) => {
          handleDateSave("startDate", date, rowData, time);
        }}
        selectedTime={startTime}
        timeInputLabel={intl.formatMessage({
          id: "meeting.creation-dialog.form.start-time.label",
          defaultMessage: "Start Time",
        })}
        datePickerProps={{
          filterDate: (date) => {
            // return dueDate ? date.isBefore(dueDate, "day") || date.isSame(dueDate, "day") : true;
            return excludeOffdays(moment(date), calendarWorkingdays) ?
              dueDate ? moment(date).isBefore(dueDate, "day") || moment(date).isSame(dueDate, "day") : true
              : false;
          },
        }}
        // btnProps={{ className: classes.datePickerCustomStyle }}
        disabled={isArchive || !issuePermission.issueDetail.issuePlanndStartEndDate.isAllowEdit}
        style={{ display: "flex", alignItems: "center", height: "100%" }}
        PopperProps={{ disablePortal: false }}
        btnType="transparent"
        placeholder={"Select"}
        icon={false}
      />
    );
  };
  const PlannedDueDateRenderer = (row) => {
    const rowData = row.data ? row.data : { data: {} };
    const { dueDate, dueTime, startDate } = rowData;
    let issuePermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceIssuePer
          : permissionObject[rowData.projectId].issue
        : workspaceIssuePer;
    const obj = rowData.projectId ? { projectId: rowData.projectId } : { workspaceId: profileState.loggedInTeam };
    const calendarWorkingdays = getCalendar(obj) || null;
    const isArchive = rowData.isArchive;
    return (
      <CustomDatePicker
        date={dueDate && moment(dueDate)}
        dateFormat="MMM DD, YYYY"
        timeInput={true}
        onSelect={(date, time) => {
          handleDateSave("dueDate", date, rowData, time);
        }}
        selectedTime={dueTime}
        timeInputLabel={intl.formatMessage({
          id: "meeting.creation-dialog.form.start-time.label",
          defaultMessage: "Start Time",
        })}
        datePickerProps={{
          filterDate: (date) => {
            // return startDate ? date.isAfter(startDate, "day") || date.isSame(startDate, "day") : true;
            return excludeOffdays(moment(date), calendarWorkingdays) ?
              startDate
                ? moment(date).isAfter(startDate, "day") || moment(date).isSame(startDate, "day")
                : true
              : false;
          },
        }}
        // btnProps={{ className: classes.datePickerCustomStyle }}

        disabled={isArchive || !issuePermission.issueDetail.issuePlanndStartEndDate.isAllowEdit}
        style={{ display: "flex", alignItems: "center", height: "100%" }}
        PopperProps={{ disablePortal: false }}
        btnType="transparent"
        placeholder={"Select"}
        icon={false}
      />
    );
  };
  const ProjectDropdownRenderer = (row) => {
    const rowData = row.data || {};
    const selectedProject = rowData.projectId
      ? [
        {
          label: rowData.project ? rowData.project.projectName : "",
          id: rowData.projectId,
          obj: rowData,
        },
      ]
      : [];
    let issuePermission =
      rowData && rowData.projectId
        ? !permissionObject[rowData.projectId]
          ? workspaceIssuePer
          : permissionObject[rowData.projectId].issue
        : workspaceIssuePer;
    const isArchive = rowData.isArchive;
    return (
      <SearchDropdown
        initSelectedOption={selectedProject}
        obj={rowData}
        disabled={isArchive || !issuePermission.issueDetail.issueProject.isAllowEdit}
        // tooltip={isDisabled}
        key={rowData.projectId}
        popperProps={{ disablePortal: false }}
        tooltipText={
          <FormattedMessage
            id="task.detail-dialog.project.hint1"
            defaultMessage="You cannot change project once user effort is added to task or you don't have permission to change project"
          />
        }
        optionsList={generateProjectDropdownData(rowData)}
        singleSelect
        updateAction={handleProjectChange}
        selectedOptionHead={"Issue Project"}
        allOptionsHead={intl.formatMessage({
          id: "project.label",
          defaultMessage: "Projects",
        })}
        buttonPlaceholder={
          <FormattedMessage
            id="task.creation-dialog.form.project.placeholder"
            defaultMessage="Select Project"
          />
        }
        buttonProps={{
          style: {
            width: "100%",
          },
        }}
        btnTextProps={{
          style: {
            width: "90%",
            overflow: "hidden",
            whiteSpace: "nowrap",
            textOverflow: "ellipsis",
          },
        }}
      />
    );
  };

  const TaskDropdownRenderer = (row) => {
    const rowData = row.data || {};
    let issuePermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceIssuePer
          : permissionObject[rowData.projectId].issue
        : workspaceIssuePer;
    const isArchive = rowData.isArchive;

    const tasksData = generateTaskDropdownData(rowData);
    let selectedTask = allTasks.filter((t) => {
      return rowData.linkedTasks && rowData.linkedTasks.indexOf(t.taskId) > -1;
    });
    selectedTask = generateTaskData(selectedTask);

    return (
      <SearchDropdown
        initSelectedOption={selectedTask}
        obj={rowData}
        disabled={isArchive || !issuePermission.issueDetail.issueTask.isAllowEdit}
        popperProps={{ disablePortal: false }}
        tooltipText={
          <FormattedMessage
            id="task.detail-dialog.project.hint1"
            defaultMessage="You cannot change project once user effort is added to task or you don't have permission to change project"
          />
        }
        optionsList={tasksData}
        singleSelect
        updateAction={(option) => handleTaskChange(option, rowData)}
        selectedOptionHead="Tasks"
        allOptionsHead={"Tasks"}
        buttonPlaceholder="Select Task"
        buttonProps={{
          style: {
            width: "100%",
          },
        }}
        btnTextProps={{
          style: {
            width: "90%",
            overflow: "hidden",
            whiteSpace: "nowrap",
            textOverflow: "ellipsis",
          },
        }}
      />
    );
  };
  const AssigneeDropdownRenderer = (row) => {
    const rowData = row.data || {};
    const membersObjArr = members && members.filter((m) => rowData.assignee.includes(m.userId));
    let issuePermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceIssuePer
          : permissionObject[rowData.projectId].issue
        : workspaceIssuePer;
    const isArchive = rowData.isArchive;
    return (
      <span data-rowClick="cell">
        <AssigneeDropdown
          popperProps={{ disablePortal: false }}
          assignedTo={membersObjArr || []}
          updateAction={handleUpdateAssignee}
          isArchivedSelected={isArchive}
          obj={rowData}
          avatarSize={"xsmall"}
          buttonVariant={"small"}
          customIconButtonProps={{
            classes: {
              root: classes.iconBtnStyles,
            },
          }}
          totalAssigneeBtnProps={{
            style: {
              height: 29,
              width: 29,
              fontSize: "14px",
              fontWeight: theme.typography.fontWeightLarge,
            },
          }}
          customIconRenderer={<AddIcon htmlColor={theme.palette.text.dark} />}
          style={{ height: "100%" }}
          disabled={
            isArchive ||
            (!issuePermission.issueDetail.issueAssignee.isAllowDelete &&
              !issuePermission.issueDetail.issueAssignee.isAllowAdd)
          }
          addPermission={issuePermission.issueDetail.issueAssignee.isAllowAdd}
          deletePermission={issuePermission.issueDetail.issueAssignee.isAllowDelete}
        />
      </span>
    );
  };
  const DateRenderer = (row) => {
    const rowData = row.data || {};
    const formatedDate = moment(rowData.createdDate).format("MMM DD, YY");
    return formatedDate;
  };
  const UpdatedDateRenderer = (row) => {
    const rowData = row.data || {};
    const formatedDate = moment(rowData.updatedDate).format("MMM DD, YY");
    return formatedDate;
  };
  const CreatedByRenderer = (row = { data: {} }) => {
    const rowData = row.data ? row.data : { data: {} };
    return (
      <div className={`${classes.taskTitleTextCnt} ${classes.textCenter} wrapText`} title={rowData.createdBy}>
        <span className={classes.taskTitle}>
          {!isEmpty(rowData.createdBy) ? rowData.createdBy : "-"}{" "}
          {/* Test Thissssssss lorem ispum Test Thissssssss lorem ispum */}
        </span>
      </div>
    );
  };
  const UpdatedByRenderer = (row = { data: {} }) => {
    const rowData = row.data ? row.data : { data: {} };
    return (
      <div className={`${classes.taskTitleTextCnt} ${classes.textCenter} wrapText`} title={rowData.createdBy}>
        <span className={classes.taskTitle}>
          {!isEmpty(rowData.createdBy) ? rowData.updatedBy : "-"}{" "}
        </span>
      </div>
    );
  };
  const ProgressRenderer = (row) => {
    const rowData = row.data || {};

    const overdue = helper.RETURN_OVER_DUE_DAYS_WITH_PROGRESS(
      rowData.actualDueDateString || "",
      rowData.progress || 0
    );

    return (
      <div style={{ display: "flex", alignItems: "center" }}>
        <div style={{ width: 30, height: 30, position: "relative" }}>
          <Circle
            percent={rowData.progress}
            strokeWidth="10"
            trailWidth=""
            trailColor="#dedede"
            strokeColor={overdue ? "#de133e" : "#30d56e"}
          />
          <Typography
            variant="h6"
            align="center"
            style={{
              position: "absolute",
              top: "50%",
              left: "50%",
              fontSize: "10px",
              transform: "translate(-50%, -50%)",
            }}>
            {rowData.progress}
          </Typography>
        </div>
      </div>
    );
  };

  const ColorRenderer = (row) => {
    const rowData = row.data || {};

    return (
      <span
        style={{ background: rowData.colorCode || "transparent" }}
        className={classes.taskColor}></span>
    );
  };
  const isExternalFilterPresent = () => {
    return true;
  };

  //Clear selection if archived view is selected
  useEffect(() => {
    if (quickFilters && quickFilters.Archived) {
      grid.grid && grid.grid.deselectAll();
    }
  }, [quickFilters]);

  useEffect(() => {
    grid.grid && grid.grid.onFilterChanged();
  }, [issueFilters, quickFilters]);

  const handleTaskSelection = (tasks) => {
    setSelectedIssues(tasks);
  };

  const handleClearSelection = () => {
    setSelectedIssues([]);
    grid.grid && grid.grid.deselectAll();
  };
  //Close task detail
  const closeIssueDetailsPopUp = () => {
    issueDetailDialogState(dispatch, {
      id: "",
    });
  };
  //Open issue details on issues row click
  const handleIssueRowClick = (row) => {
    let rowData = row.data;
    const isRowClicked = row.event.target.closest("*[data-rowClick='cell']");
    let issuePermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceIssuePer
          : permissionObject[rowData.projectId].issue
        : workspaceIssuePer;
    if (isRowClicked) {
      return false;
    }
    if (issuePermission.issueDetail.cando && row.data && row.data.id && row.data.uniqueId !== "-") {
      issueDetailDialogState(dispatch, {
        id: row.data.id,
        afterCloseCallBack: () => {
          closeIssueDetailsPopUp();
        },
      });
    }
  };
  const handleUpdateCustomField = (
    rowData,
    option,
    obj,
    settings,
    succ = () => { },
    fail = () => { }
  ) => {
    const issueCopy = { ...rowData };
    const { fieldId } = obj;
    const newObj = {
      groupType: "issue",
      groupId: issueCopy.id,
      fieldId,
      fieldData: { data: option },
    };
    if (obj.fieldType == "matrix") {
      newObj.fieldData.data = newObj.fieldData.data.map((opt) => {
        return { cellName: opt.cellName };
      });
    } else if (obj.fieldType == "dropdown") {
      if (obj.settings.multiSelect)
        newObj.fieldData.data = newObj.fieldData.data.map((opt) => {
          return { id: opt.id };
        });
      else {
        newObj.fieldData.data = { id: `${option.id ? option.id : ""}` };
      }
    }
    UpdateIssueCustomField(
      newObj,
      (res) => {
        const issueObj = res.data.entity[0];
        let customFieldsArr = [];
        // Updating Global state
        const isExist = issueCopy.customFieldData
          ? issueCopy.customFieldData.findIndex((c) => c.fieldId === issueObj.fieldId) > -1
          : false; /** if new issue created, its customFields are null so in null case scenario , compile the normal false flow, add custom field in issue object and save it */
        if (isExist) {
          customFieldsArr = issueCopy.customFieldData.map((c) => {
            return c.fieldId === issueObj.fieldId ? issueObj : c;
          });
        } else {
          customFieldsArr = issueCopy.customFieldData
            ? [...issueCopy.customFieldData, issueObj]
            : [
              issueObj,
            ]; /** add custom field in issue object and save it, if newly created issue because its custom field is already null */
        }
        succ(customFieldsArr);
        let newIssueObj = { ...issueCopy, customFieldData: customFieldsArr };
        if (grid.grid) {
          const rowNode = grid.grid && grid.grid.getRowNode(newIssueObj.id);
          rowNode.setData(newIssueObj);
        }
        updateIssueObject(newIssueObj, dispatch);
      },
      () => {
        fail();
      },
      dispatch
    );
  };
  const CustomfieldRendere = (row) => {
    /** custom fields columns rendere */
    const obj = {
      fieldId: row.colDef.fieldId,
      fieldType: row.colDef.fieldType,
      fieldData: { data: null },
    };
    const rowData = row.data || {};
    const data = rowData.customFieldData
      ? rowData.customFieldData.find((cf) => cf.fieldId === row.colDef.fieldId) || obj
      : obj;
    let issuePermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceIssuePer
          : permissionObject[rowData.projectId].issue
        : workspaceIssuePer;
    const canUpdateField =
      issuePermission && !rowData.isArchive
        ? issuePermission.issueDetail.isUpdateField.cando
        : false;
    return (
      <CustomFieldRenderer
        field={data}
        fieldType={row.colDef.fieldType}
        rowData={rowData}
        handleUpdateCustomField={handleUpdateCustomField}
        permission={canUpdateField}
        groupType={"issue"}
        placeholder={false}
      />
    );
  };
  const IssueActionDropdownRenderer = (row) => {
    const rowData = row.data || {};
    let issuePermission =
      rowData && rowData.projectId
        ? isUndefined(permissionObject[rowData.projectId])
          ? workspaceIssuePer
          : permissionObject[rowData.projectId].issue
        : workspaceIssuePer;
    return (
      <IssueActionDropdown
        btnProps={{ style: { width: 37 } }}
        data={rowData}
        handleActivityLog={() => { }}
        handleCloseCallBack={() => { }}
        issuePermission={issuePermission}
        isArchived={rowData.isArchive}
      />
    );
  };
  const getContextMenuItems = useCallback((params) => {
    const selectedNodes = grid.grid.getSelectedNodes();
    let result = [
      {
        // custom item
        name: selectedNodes.length ? "Export Selected Issue(s)" : "Export Issue",
        action: () => handleExportIssue(params),
        // cssClasses: ['redFont', 'bold'],
      },
      {
        // custom item
        name: "Export all Issues",
        action: () => handleExportAllIssues(params),
        // cssClasses: ['redFont', 'bold'],
      },
    ];
    return result;
  }, []);
  const handleCloseDialog = () => {
    grid.grid.redrawRows([mappingDialogState.tempTask]);
    setMappingDialogState({
      tempProject: null,
      openStatusDialog: false,
      newTemplateItem: {},
      oldStatusItem: {},
      tempTask: {},
    });
  };
  //Snackbar function
  const showSnackBar = (snackBarMessage = "", type = null) => {
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "top",
          horizontal: "centerdp",
        },
        variant: type ? type : "info",
      }
    );
  };
  const issueTitleColumn = (issueColumns && issueColumns.find((c) => c.columnKey == "title")) || {};
  let headProps = headerProps;
  const nonSectionsFieldskeys = nonSectionFields
    .filter(
      (item) =>
        item.fieldType == "textarea" ||
        item.fieldType == "formula" ||
        item.fieldType == "filesAndMedia" ||
        item.fieldType == "location" ||
        (item.fieldType == "dropdown" && item.settings.multiSelect) ||
        (item.fieldType == "people" && item.settings.multiplePeople)
    )
    .map((nsf) => nsf.columnKey);
  const sectionsFieldskeys = sections.reduce((res, cv) => {
    let keys = cv.fields
      .filter(
        (item) =>
          item.fieldType == "textarea" ||
          item.fieldType == "formula" ||
          item.fieldType == "filesAndMedia" ||
          item.fieldType == "location" ||
          (item.fieldType == "dropdown" && item.settings.multiSelect) ||
          (item.fieldType == "people" && item.settings.multiplePeople)
      )
      .map((cv) => cv.columnKey);
    res = [...res, ...keys];
    return res;
  }, []);
  headProps.columnGroupingDisabled = [
    ...headProps.columnGroupingDisabled,
    ...nonSectionsFieldskeys,
    ...sectionsFieldskeys,
  ];

  const handleChangeGrouping = (value) => {
    let sectionGroup = localStorage.getItem("issueSectionGrouping");
    if (sectionGroup == "true") localStorage.setItem("issueSectionGrouping", value);
    else {
      localStorage.setItem("issueSectionGrouping", value);
    }
    setSectionGrouping(value);
  };
  const RenderColumnsWithoutSectionGrouping = () => {
    const systemColumns = issueColumns
      .map((c, i) => {
        const defaultColDef = issueColumnDefs(classes, members)[c.columnKey];
        const {
          hide,
          pinned,
          rowGroup,
          rowGroupIndex,
          sort,
          sortIndex,
          width,
          wrapText,
          autoHeight,
          position,
        } = c;
        return (
          c.columnKey !== "title" &&
          c.isSystem && (
            <AgGridColumn
              key={c.id}
              headerName={c.columnName}
              field={c.columnKey}
              hide={hide}
              suppressKeyboardEvent={true}
              pinned={pinned}
              align="left"
              rowGroup={rowGroup}
              sort={sort}
              sortIndex={sortIndex == -1 ? null : sortIndex}
              wrapText={wrapText}
              rowGroupIndex={rowGroup && parseFloat(rowGroupIndex)}
              autoHeight={autoHeight}
              width={width || (defaultColDef && defaultColDef.minWidth)}
              cellStyle={{ justifyContent: "center" }}
              position={position}
              {...defaultColDef}
            />
          )
        );
      })
      .filter((c) => c !== false);
    const nonSectionColumns = nonSectionFields.map((c, i) => {
      const defaultColDef = issueColumnDefs(classes, members)[c.fieldType];
      const { hide, pinned, rowGroup, sort, sortIndex, width, wrapText, autoHeight, position } = c;
      return (
        <AgGridColumn
          key={c.id}
          headerName={c.columnName}
          field={c.columnKey}
          fieldType={c.fieldType}
          hide={hide}
          pinned={pinned}
          align="left"
          rowGroup={rowGroup}
          suppressKeyboardEvent={true}
          sort={sort}
          sortIndex={sortIndex == -1 ? null : sortIndex}
          wrapText={wrapText}
          autoHeight={autoHeight}
          fieldId={c.fieldId}
          position={position}
          width={width || (defaultColDef && defaultColDef.minWidth)}
          cellStyle={{
            justifyContent:
              c.fieldType === "dropdown" || c.fieldType === "filesAndMedia"
                ? "flex-start"
                : "center",
          }}
          {...defaultColDef}
        />
      );
    });
    const sectionColumns = sections
      .map((section) => {
        return (
          section.fields.length > 0 &&
          section.fields.map((c, i) => {
            const defaultColDef = issueColumnDefs(classes, members)[c.fieldType];
            const {
              hide,
              pinned,
              rowGroup,
              sort,
              sortIndex,
              width,
              wrapText,
              autoHeight,
              position,
            } = c;
            return (
              <AgGridColumn
                key={c.id}
                headerName={c.columnName}
                field={c.columnKey}
                hide={hide}
                fieldType={c.fieldType}
                pinned={pinned}
                align="left"
                suppressKeyboardEvent={true}
                position={position}
                rowGroup={rowGroup}
                sort={sort}
                sortIndex={sortIndex == -1 ? null : sortIndex}
                wrapText={wrapText}
                autoHeight={autoHeight}
                width={width || (defaultColDef && defaultColDef.minWidth)}
                fieldId={c.fieldId}
                cellStyle={{
                  justifyContent:
                    c.fieldType === "dropdown" || c.fieldType === "filesAndMedia"
                      ? "flex-start"
                      : "center",
                }}
                {...defaultColDef}
              />
            );
          })
        );
      })
      .filter((c) => c !== false);
    const spreadSectionCols = sectionColumns.reduce((r, cv) => {
      r = [...r, ...cv];
      return r;
    }, []);
    const allColumns = [...systemColumns, ...nonSectionColumns, ...spreadSectionCols];
    const sortedColumns = allColumns.sort((pv, cv) => {
      return pv.props.position - cv.props.position;
    });
    return sortedColumns;
  };
  return (
    <>
      {selectedIssues.length >= 1 ? (
        <Issuecmp selectedIssues={selectedIssues} clearSelection={handleClearSelection} />
      ) : null}

      {issues.length == 0 ? (
        <div className={classes.emptyContainer} style={style}>
          <EmptyState
            screenType="issue"
            heading={
              <FormattedMessage
                id="common.create-first.issue.label"
                defaultMessage="Create your first issue"
              />
            }
            message={
              <FormattedMessage
                id="common.create-first.issue.messagea"
                defaultMessage='You do not have any issues yet. Press "Alt + I" or click on button below.'
              />
            }
            button={workspaceIssuePer.creatIssue.cando}
          />
        </div>
      ) : (
        <div className={classes.taskListViewCnt} style={style}>
          <CustomTable
            columns={state.issueColumns}
            exportCallback={handleExportIssue}
            defaultColDef={{ lockPinned: true }}
            isExternalFilterPresent={isExternalFilterPresent}
            doesExternalFilterPass={doesFilterPass}
            onSelectionChanged={handleTaskSelection}
            idKey={"id"}
            frameworkComponents={{
              taskActionCellRenderer: IssueActionDropdownRenderer,
              statusDropdown: StatusDropdownCmp,
              issueTitleCmp: IssueTitleCellRenderer,
              priorityCmp: PriorityDropdown,
              severityCmp: SeverityDropdown,
              actualStartDate: ActualStartDateRenderer,
              plannedStartDate: PlannedStartDateRenderer,
              actualDueDate: ActualDueDateRenderer,
              plannedDueDate: PlannedDueDateRenderer,
              createdByRenderer: CreatedByRenderer,
              updatedByRenderer: UpdatedByRenderer,
              issueProject: ProjectDropdownRenderer,
              assignee: AssigneeDropdownRenderer,
              issueType: TypeDropdown,
              groupRowInnerRenderer: GroupRowInnerRenderer,
              createdDateRenderer: DateRenderer,
              updatedDateRenderer: UpdatedDateRenderer,
              progressRenderer: ProgressRenderer,
              taskRenderer: TaskDropdownRenderer,
              color: ColorRenderer,
              textfield: CustomfieldRendere,
              textarea: CustomfieldRendere,
              location: CustomfieldRendere,
              country: CustomfieldRendere,
              number: CustomfieldRendere,
              money: CustomfieldRendere,
              email: CustomfieldRendere,
              websiteurl: CustomfieldRendere,
              date: CustomfieldRendere,
              phone: CustomfieldRendere,
              rating: CustomfieldRendere,
              formula: CustomfieldRendere,
              people: CustomfieldRendere,
              dropdown: CustomfieldRendere,
              filesAndMedia: CustomfieldRendere,
            }}
            data={issues}
            selectedTasks={selectedIssues}
            type="issue"
            onRowGroupChange={(obj, key) => updateIssue(obj, { [key]: obj[key] })}
            createableProps={{
              placeholder: "Enter title for new issue",
              id: "quickAddIssue",
              btnText: "Add new issue",
              addAction: handleAddIssue,
              addHelpTask: "Press enter to add new issue",
              emptyErrorMessage: 'Please Enter Issue title',
              disabled: !workspaceIssuePer.creatIssue.cando,
            }}
            headerProps={headProps}
            gridProps={{
              getContextMenuItems: getContextMenuItems,
              groupDisplayType: "groupRows",
              onRowClicked: handleIssueRowClick,
              reactUi: true,
              // groupIncludeFooter: true,
              // groupIncludeTotalFooter: true,
              maintainColumnOrder: true,
              // autoGroupColumnDef: { minWidth: 300 },
            }}>
            {/*<AgGridColumn headerClass={classes.taskTitleField} >*/}
            <AgGridColumn
              headerName=""
              field="colorCode"
              cellRenderer="color"
              filter={false}
              cellClass={classes.taskColorCell}
              maxWidth={6}
              resizeable={false}
              pinned="left"
              lockPosition={true}
              suppressMovable={true}
            />
            <AgGridColumn
              headerName={issueTitleColumn.columnName}
              field={issueTitleColumn.columnKey}
              resizable={true}
              suppressMovable={true}
              checkboxSelection={teamCanView("bulkActionAccess")}
              sortable={true}
              filter={true}
              lockPosition={true}
              wrapText={issueTitleColumn.wrapText}
              cellRenderer={"issueTitleCmp"}
              cellClass={`${classes.taskTitleCell} ag-textAlignLeft`}
              pinnedRowCellRenderer="customPinnedRowRenderer"
              pinned={"left"}
              comparator={sortAlphabetically}
              autoHeight={issueTitleColumn.autoHeight}
              width={issueTitleColumn.width || 300}
              minWidth={200}
              align="left"
              sort={issueTitleColumn.sort}
              sortIndex={issueTitleColumn.sortIndex == -1 ? null : issueTitleColumn.sortIndex}
              cellStyle={{ padding: 0 }}
              rowDrag={true}
            />

            {!sectionGroup || sectionGroup == "false"
              ? RenderColumnsWithoutSectionGrouping()
              : null}

            {sectionGroup == "true" && (
              <AgGridColumn
                visible={false}
                headerName="System Fields"
                headerClass={classes.systemFieldGroup}>
                {issueColumns.map((c, i) => {
                  const defaultColDef = issueColumnDefs(classes, members)[c.columnKey];
                  const {
                    hide,
                    pinned,
                    rowGroup,
                    rowGroupIndex,
                    sort,
                    sortIndex,
                    width,
                    wrapText,
                    autoHeight,
                  } = c;
                  return (
                    c.columnKey !== "title" &&
                    c.isSystem && (
                      <AgGridColumn
                        key={c.id}
                        headerName={c.columnName}
                        field={c.columnKey}
                        hide={hide}
                        pinned={pinned}
                        align="left"
                        rowGroup={rowGroup}
                        sort={sort}
                        sortIndex={sortIndex == -1 ? null : sortIndex}
                        wrapText={wrapText}
                        rowGroupIndex={rowGroup && parseFloat(rowGroupIndex)}
                        autoHeight={autoHeight}
                        width={width || (defaultColDef && defaultColDef.minWidth)}
                        cellStyle={{ justifyContent: "center" }}
                        {...defaultColDef}
                      />
                    )
                  );
                })}
              </AgGridColumn>
            )}
            {sectionGroup == "true" &&
              sections.map((section) => {
                return (
                  section.fields.length > 0 && (
                    <AgGridColumn
                      headerName={section.columnName}
                      resizeable={false}
                      headerGroupComponent="customHeaderGroupComponent"
                      color={section.color}
                      headerClass={classes.sectionFieldsGroup}>
                      {section.fields.map((c, i) => {
                        const defaultColDef = issueColumnDefs(classes, members)[c.fieldType];
                        const {
                          hide,
                          pinned,
                          rowGroup,
                          sort,
                          sortIndex,
                          width,
                          wrapText,
                          autoHeight,
                        } = c;
                        return (
                          <AgGridColumn
                            key={c.id}
                            headerName={c.columnName}
                            field={c.columnKey}
                            suppressKeyboardEvent={true}
                            hide={hide}
                            fieldType={c.fieldType}
                            pinned={pinned}
                            align="left"
                            rowGroup={rowGroup}
                            sort={sort}
                            sortIndex={sortIndex == -1 ? null : sortIndex}
                            wrapText={wrapText}
                            autoHeight={autoHeight}
                            width={width || (defaultColDef && defaultColDef.minWidth)}
                            fieldId={c.fieldId}
                            cellStyle={{
                              justifyContent:
                                c.fieldType === "dropdown" || c.fieldType === "filesAndMedia"
                                  ? "flex-start"
                                  : "center",
                            }}
                            {...defaultColDef}
                          />
                        );
                      })}
                    </AgGridColumn>
                  )
                );
              })}
            {sectionGroup == "true" && nonSectionFields.length ? (
              <AgGridColumn resizeable={false} headerClass={classes.taskTitleField}>
                {nonSectionFields.map((c, i) => {
                  const defaultColDef = issueColumnDefs(classes, members)[c.fieldType];
                  const { hide, pinned, rowGroup, sort, sortIndex, width, wrapText, autoHeight } =
                    c;
                  return (
                    <AgGridColumn
                      key={c.id}
                      headerName={c.columnName}
                      field={c.columnKey}
                      fieldType={c.fieldType}
                      hide={hide}
                      suppressKeyboardEvent={true}
                      pinned={pinned}
                      align="left"
                      lockPinned={true}
                      rowGroup={rowGroup}
                      sort={sort}
                      sortIndex={sortIndex == -1 ? null : sortIndex}
                      wrapText={wrapText}
                      autoHeight={autoHeight}
                      fieldId={c.fieldId}
                      width={width || (defaultColDef && defaultColDef.minWidth)}
                      cellStyle={{
                        justifyContent:
                          c.fieldType === "dropdown" || c.fieldType === "filesAndMedia"
                            ? "flex-start"
                            : "center",
                      }}
                      {...defaultColDef}
                    />
                  );
                })}
              </AgGridColumn>
            ) : null}

            <AgGridColumn
              headerName={""}
              field={"columnDropdown"}
              resizable={false}
              sortable={false}
              lockPinned={true}
              filter={false}
              width={40}
              headerClass={"columnSelectHeader"}
              pinned={"right"}
              suppressMovable={true}
              cellRenderer={"taskActionCellRenderer"}
              cellStyle={{ padding: 0, textAlign: "center" }}
            />
          </CustomTable>
          <IssueFilter
            sectionGrouping={sectionGrouping}
            handleChangeGrouping={handleChangeGrouping}
          />
        </div>
      )}

      {/*<Footer*/}
      {/*  total={tasks && tasks.length}*/}
      {/*  display={true}*/}
      {/*  type="Task"*/}
      {/*  // quote={this.props.quote}*/}
      {/*/>*/}
    </>
  );
}, areEqual);

function areEqual(prevProps, nextProps) {
  return isEqual(prevProps, nextProps);
  /*
  return true if passing nextProps to render would return
  the same result as passing prevProps to render,
  otherwise return false
  */
}

const GroupRowInnerRenderer = (props) => {
  return <GroupByComponents data={props} feature="issue" />;
};

export default compose(
  injectIntl,
  withSnackbar,
  withStyles(issueListStyles, { withTheme: true })
)(IssueList);
