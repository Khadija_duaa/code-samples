import React, { Component, Fragment, useState } from "react";
import ReactDataGrid from "react-data-grid";
import { withSnackbar } from "notistack";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import IssueMenuList from "../../../components/Menu/TaskMenus/MemberListMenu";
import BulkActions from "./BulkActions";
import AddIssue from "./AddIssue";
import Hotkeys from "react-hot-keys";
import Grid from "@material-ui/core/Grid";
import listStyles from "./styles";
import AddIcon from "@material-ui/icons/Add";
import CancelIcon from "@material-ui/icons/Cancel";
import IconButton from "../../../components/Buttons/IconButton";
// import TableActionDropDown from "./ActionDropDown";
import ColumnSelectionDropdown from "../../../components/Dropdown/SelectedItemsDropDown/index";
import { generateTaskData, generateProjectData } from "../../../helper/generateSelectData";

// import TableActionDropDown from "./ActionDropDown";
import StarredCheckBox from "../../../components/Form/Starred";
import helper from "../../../helper";
import IssueDetails from "../IssueDetails/IssueDetails";
import IconMenu from "../../../components/Menu/TaskMenus/IconMenu";
import Typography from "@material-ui/core/Typography";
import EmptyState from "../../../components/EmptyStates/EmptyState";
import { calculateContentHeight } from "../../../utils/common";
import DefaultDialog from "../../../components/Dialog/Dialog";
import AddIssueForm from "../../AddNewForms/AddNewIssueForm";
import CustomRangeDatePicker from "../../../components/DatePicker/CustomRangeDatePicker";
import { SaveItemOrder } from "../../../redux/actions/itemOrder";
import cloneDeep from "lodash/cloneDeep";
import find from "lodash/find";
import queryString from "query-string";
import AssigneeDropdown from "../../../components/Dropdown/AssigneeDropdown";
import SearchDropdown from "../../../components/Dropdown/SearchDropdown";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";
import { canEdit } from "../permissions";
import {
  UpdateIssue,
  SaveIssue,
  BulkUpdateIssue,
  DeleteIssue,
  ArchiveIssue,
  UnarchiveIssue,
  MarkIssueAsStarred,
  dispatchIssue,
  UpdateIssueCustomField,
  moveIssueToWorkspace,
} from "../../../redux/actions/issues";
import IssueActionDropdown from "./IssueActionDropDown";
import { sortListData, getSortOrder } from "../../../helper/sortListData";
import {
  getIssuesPermissions,
  getEditPermissionsWithArchieve,
  getIssuesPermissionsWithoutArchieve,
} from "../permissions";
import UnPlanned from "../../billing/UnPlanned/UnPlanned";
import Modal from "@material-ui/core/Modal";
import { GetPermission } from "../../../components/permissions";
import { columns } from "../constants";
import { GroupingRowRenderer } from "./GroupingRowRenderer";
import { Data } from "react-data-grid-addons";
import Starred from "./Starred";
import { FormattedMessage, injectIntl } from "react-intl";
import { headerTranslations } from "../../../utils/headerTranslations";
import getTableColumns from "../../../helper/getTableColumns";
import { getActiveFields } from "../../../helper/getActiveCustomFields";
import { customFieldColumnDropdownData, getCustomFields } from "../../../helper/customFieldsData";
import UpdateMatrixDialog from "../../../components/Matrix/updateMatrixDialog.cmp";
import CustomFieldsColumns from "../../CustomFieldsColumns/CustomFieldsColumns";
import { updateRisk } from "../../../redux/actions/risks";
import isEqual from "lodash/isEqual";
import StatusDropdown from "../../../components/Dropdown/StatusDropdown/Dropdown";
import { typeData } from "../../../helper/issueDropdownData";
import { FetchWorkspaceInfo } from "../../../redux/actions/workspace";
import { issueDetailDialogState } from "../../../redux/actions/allDialogs";
import ProjectPermissionSelector from "../../../redux/selectors/projectPermissionSelector";
import isUndefined from "lodash/isUndefined";
import {TRIALPERIOD} from '../../constants/planConstant';

const {
  id_C,
  issueTitle_C,
  issueStatus_C,
  actualStartDate_C,
  plannedStartDate_C,
  task_C,
  projectName,
  severity_C,
  priority_C,
  assignees_C,
  type_C,
  updatedDate_C,
  createdBy_C,
  createdDate_C,
  label_C,
  category_C,
} = columns;

const {
  Draggable: { Container: DraggableContainer, RowActionsCell, DropTargetRowContainer },
  Data: { Selectors },
  Toolbar,
} = require("react-data-grid-addons");

const RowRenderer = DropTargetRowContainer(ReactDataGrid.Row);

let increment = 20;
const Default_Column_Count = 8;
let total = Default_Column_Count;
const screenSize = 1380;

class IssueList_old extends React.Component {
  static defaultProps = { rowKey: "id" };

  constructor(props, context) {
    super(props, context);
    this.state = {
      _columns: [],
      rows: [],
      output: "",
      selectedIds: [],
      addIssue: false,
      addNewForm: false,
      completeDetails: [],
      userId: "",
      value: "",
      progress: 0,
      issueId: "",
      isProgress: false,
      TaskError: false,
      TaskErrorMessage: "",
      isReorder: false,
      startDateOpen: false,
      dueDateOpen: false,
      newCol: [],
      showIssueDetails: false,
      currentIssue: {},
      detailsIssueId: "",
      loggedInTeam: "",
      isWorkspaceChanged: false,
      showRecords: increment,
      totalRecords: 0,
      isArchived: false,
      instantData: [],
      isInstanceChange: false,
      sortColumn: null,
      sortDirection: "NONE",
      showUnplanned: false,
      selectedIssueObj: [],
      checkedItems: [
        "ID",
        "Title",
        "Status",
        "Planned Start/End",
        "Severity",
        "Priority",
        "Assignee",
        "Task",
      ],
      columnddData: getTableColumns("issue"),
      assessmentDialogOpen: false,
      selectedRisk: {},
      assessmentMatrixValues: null,
      issueTypeData: typeData(this.props.theme, this.props.classes, this.props.intl),
    };
    this.addIssue = this.addIssue.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.addIssueInput = React.createRef();
    this.onKeyDown = this.onKeyDown.bind(this);
    this.cancelAddIssue = this.cancelAddIssue.bind(this);
    this.isUpdated = this.isUpdated.bind(this);
    this.showIssueDetailsPopUp = this.showIssueDetailsPopUp.bind(this);
    this.closeIssueDetailsPopUp = this.closeIssueDetailsPopUp.bind(this);
    this.handleScrollDown = this.handleScrollDown.bind(this);
    this.scrollListener = this.scrollListener.bind(this);
    this.BulkUpdate = this.BulkUpdate.bind(this);
    this.closeActionMenu = this.closeActionMenu.bind(this);
  }
  // Generate list of selected options for project dropdown understandable form
  getSelectedProject = issue => {
    const { projects } = this.props;
    const selectedProject = projects.find(project => {
      return project.projectId == issue.projectId;
    });

    return selectedProject
      ? [
          {
            label: selectedProject.projectName,
            id: selectedProject.projectId,
            obj: issue,
          },
        ]
      : [];
  };
  generateSelectedValue = currentIssue => {
    /** function for generating the selected task list objects from task id and return taskList for showing pre populated selected task if any => function call in componentDidMount */

    const selectedTaskList = this.props.tasks.filter(task => {
      return currentIssue.linkedTasks.indexOf(task.taskId) > -1;
    });
    let taskList = generateTaskData(selectedTaskList);
    return taskList;
  };

  // Generate list of all projects for dropdown understandable form
  generateProjectDropdownData = issue => {
    const { projects } = this.props;
    let selectedTasks = this.generateSelectedValue(issue);

    let selectedProject = selectedTasks.length
      ? projects.filter(t => t.projectId === selectedTasks[0].obj.projectId)
      : projects;
    selectedProject = selectedProject.length ? selectedProject : projects;

    return generateProjectData(selectedProject.filter(p => p.projectId !== issue.projectId));
  };

  handleSelectChange = (newValue, project, issue) => {
    const projectId = newValue.length ? newValue[0].id : "";
    let selectedTasks = this.generateSelectedValue(issue);

    let linkTask = selectedTasks.length
      ? selectedTasks[0].obj.projectId == "" || !selectedTasks[0].obj.projectId
        ? []
        : issue.linkedTasks
      : issue.linkedTasks;

    const newIssueObj = {
      ...issue,
      projectId: projectId,
      linkedTasks: newValue.length == 0 ? [] : linkTask,
    };
    this.props.UpdateIssue(newIssueObj, () => {});
  };

  onColumnResize = (columnIndex, size) => {
    const { _columns } = this.state;
    if (size < 0) return;
    let columnWidth = JSON.parse(localStorage.getItem("columnWidthIssue"));
    if (columnWidth) {
      columnWidth[_columns[columnIndex - 1].key] = {
        width: size,
      };
      localStorage.setItem("columnWidthIssue", JSON.stringify(columnWidth));
    } else {
      let obj = {};
      obj[_columns[columnIndex - 1].key] = {
        width: size,
      };
      localStorage.setItem("columnWidthIssue", JSON.stringify(obj));
    }
  };

  getColumns = () => {
    let columnWidth = JSON.parse(localStorage.getItem("columnWidthIssue"));

    let _columns = [
      {
        key: id_C.key,
        width: columnWidth && columnWidth[id_C.key] ? columnWidth[id_C.key].width : 130,
        name: id_C.name,
        cellClass: "firstColoumn",
        resizable: true,
        sortDescendingFirst: true,
        formatter: value => {
          return (
            <div
              className={this.props.classes.taskListTitleTextCnt}
              style={{
                borderLeft: `6px solid ${value.row ? value.row.colorCode : ""}`,
              }}>
              <span className={this.props.classes.taskListTitleText}>
                {value.row ? value.row.issueId : ""}
              </span>
            </div>
          );
        },
        sortable: teamCanView("advanceSortAccess") ? false : true,
        columnOrder: 0,
      },

      {
        key: issueTitle_C.key,
        name: issueTitle_C.name,
        cellClass: "secondColoumn",
        visible: true,
        resizable: true,
        sortDescendingFirst: true,
        width:
          columnWidth && columnWidth[issueTitle_C.key] ? columnWidth[issueTitle_C.key].width : 400,
        formatter: value => {
          return (
            <>
              <div style={{ display: "flex", alignItems: "center" }}>
                <Starred
                  userId={this.props.profileState.data.userId}
                  currentIssue={value.row}
                  disabled={this.props.isArchivedSelected}
                />
                <span
                  title={value.row ? value.row.title : ""}
                  className={this.props.classes.taskListTitleText}>
                  {value.row ? value.row.title : ""}
                </span>
              </div>
            </>
          );
        },

        columnOrder: 1,
        sortable: true,
      }, //Here-----------------------------------------------------------------------------
      {
        key: issueStatus_C.key,
        name: issueStatus_C.name,
        resizable: true,
        width:
          columnWidth && columnWidth[issueStatus_C.key]
            ? columnWidth[issueStatus_C.key].width
            : 155,
        formatter: value => {
          const row = value.row ? value.row : "";
          const issuePermission =
            value && value.row && value.row.projectId
              ? isUndefined(this.props.permissionObject[value.row.projectId]) ? this.props.workspaceIssuePer : this.props.permissionObject[value.row.projectId].issue 
              : this.props.workspaceIssuePer;
          return (
            <IconMenu
              status={row && row.status ? row.status : ""}
              MenuData={row}
              searchFilterApplied={this.props.searchFilterApplied}
              isUpdated={this.isUpdated}
              menuType="issueStatus"
              keyType="status"
              heading={this.props.intl.formatMessage({
                id: "issue.common.status.placeholder",
                defaultMessage: "Select Status",
              })}
              isArchivedSelected={this.props.isArchivedSelected}
              permission={issuePermission.issueDetail.issueStatus.isAllowEdit}
              viewType="issue"
              permissionKey="status"
              issuePermission={issuePermission}
            />
          );
        },
        cellStyle: { background: "red" },
        cellClass: "dropDownCell",
        columnOrder: 2,
        sortable: teamCanView("advanceSortAccess") ? false : true,
      },
      {
        key: projectName.key,
        name: projectName.name,
        resizable: true,
        width: 240,
        visible: teamCanView("projectAccess"),
        formatter: value => {
          const issue = value.row ? value.row : [];
          // some times task effort comes in string and some times empty array so to check that below line of code is added
          const issueEffort = issue.userTaskEffort
            ? issue.userTaskEffort.length
            : issue.userTaskEffort;
          const issuePermission =
            value && value.row && value.row.projectId
              ? isUndefined(this.props.permissionObject[value.row.projectId]) ? this.props.workspaceIssuePer : this.props.permissionObject[value.row.projectId].issue 
              : this.props.workspaceIssuePer;
          return (
            <SearchDropdown
              initSelectedOption={issue.projectId ? this.getSelectedProject(issue) : []}
              obj={issue}
              disableDropdown={
                !issuePermission.issueDetail.issueProject.isAllowEdit || value.row.isArchive
              }
              tooltip={!!issueEffort}
              key={issue.projectId}
              tooltipText={
                <FormattedMessage
                  id="task.detail-dialog.project.hint1"
                  defaultMessage="You cannot change project once user effort is added to task or you don't have permission to change project"
                />
              }
              optionsList={this.generateProjectDropdownData(issue)}
              singleSelect
              updateAction={(newValue, project) =>
                this.handleSelectChange(newValue, project, issue)
              }
              selectedOptionHead={this.props.intl.formatMessage({
                id: "common.task-project.label",
                defaultMessage: "Task Project",
              })}
              allOptionsHead={this.props.intl.formatMessage({
                id: "project.label",
                defaultMessage: "Projects",
              })}
              buttonPlaceholder={
                <FormattedMessage
                  id="task.creation-dialog.form.project.placeholder"
                  defaultMessage="Select Project"
                />
              }
              buttonProps={{
                disabled:
                  !issuePermission.issueDetail.issueProject.isAllowEdit || value.row.isArchive,
              }}
              IssueView={true}
            />
          );
        },
        cellClass: "dropDownCell",
        columnOrder: 6,

        sortable: !teamCanView("advanceSortAccess"),
      },
      {
        key: actualStartDate_C.key,
        name: actualStartDate_C.name,
        resizable: true,
        width:
          columnWidth && columnWidth[actualStartDate_C.key]
            ? columnWidth[actualStartDate_C.key].width
            : 150,
        formatter: value => {
          const row = value.row ? value.row : "";
          const issuePermission =
            value && value.row && value.row.projectId
              ? isUndefined(this.props.permissionObject[value.row.projectId]) ? this.props.workspaceIssuePer : this.props.permissionObject[value.row.projectId].issue 
              : this.props.workspaceIssuePer;
          return (
            <Fragment>
              <CustomRangeDatePicker
                label="Actual Start/End"
                placeholder={
                  <FormattedMessage id="common.date.placeholder" defaultMessage="Select Date" />
                }
                startDate={row.actualStartDateString}
                endDate={row.actualDueDateString}
                startTime={row.actualStartTime}
                endTime={row.actualDueTime}
                permission={
                  this.props.isArchivedSelected ||
                  !issuePermission.issueDetail.issueActualstartEndDate.isAllowEdit
                }
                dateSaveAction={(startDate, endDate, startTime, endTime) =>
                  this.handleDateSave(startDate, endDate, row, "actual", startTime, endTime)
                }
                error={false}
              />
            </Fragment>
          );
        },
        columnOrder: 3,
        sortable: teamCanView("advanceSortAccess") ? false : true,
        cellClass: "dropDownCell",
      },
      {
        key: plannedStartDate_C.key,
        name: plannedStartDate_C.name,
        resizable: true,
        width:
          columnWidth && columnWidth[plannedStartDate_C.key]
            ? columnWidth[plannedStartDate_C.key].width
            : 150,
        formatter: value => {
          const row = value.row ? value.row : "";
          const issuePermission =
            value && value.row && value.row.projectId
              ? isUndefined(this.props.permissionObject[value.row.projectId]) ? this.props.workspaceIssuePer : this.props.permissionObject[value.row.projectId].issue 
              : this.props.workspaceIssuePer;
          return (
            <Fragment>
              <CustomRangeDatePicker
                label="Planned Start/End"
                placeholder={
                  <FormattedMessage id="common.date.placeholder" defaultMessage="Select Date" />
                }
                startDate={row.startDateString}
                endDate={row.dueDateString}
                startTime={row.startTime}
                endTime={row.dueTime}
                permission={
                  this.props.isArchivedSelected ||
                  !issuePermission.issueDetail.issuePlanndStartEndDate.isAllowEdit
                }
                dateSaveAction={(startDate, endDate, startTime, endTime) =>
                  this.handleDateSave(startDate, endDate, row, "planned", startTime, endTime)
                }
                error={false}
              />
            </Fragment>
          );
        },
        cellClass: "dropDownCell",
        sortable: teamCanView("advanceSortAccess") ? false : true,
        columnOrder: 4,
      },
      {
        key: task_C.key,
        name: task_C.name,
        resizable: true,
        width: columnWidth && columnWidth[task_C.key] ? columnWidth[task_C.key].width : 240,
        formatter: value => {
          const issue = value.row ? value.row : [];
          const issuePermission =
            value && value.row && value.row.projectId
              ? isUndefined(this.props.permissionObject[value.row.projectId]) ? this.props.workspaceIssuePer : this.props.permissionObject[value.row.projectId].issue 
              : this.props.workspaceIssuePer;
          const canEditIssueTask = issuePermission.issueDetail.issueTask.isAllowEdit;
          return (
            <SearchDropdown
              initSelectedOption={this.getSelectedTask(issue)}
              obj={issue}
              optionsList={this.generateTaskDropdownData(issue)}
              maxSelection={1}
              singleSelect={true}
              updateAction={(selectedTasks, task) => this.updateTask(selectedTasks, task, issue)}
              selectedOptionHead={
                <FormattedMessage id="common.issue-task.label" defaultMessage="Issue Task" />
              }
              allOptionsHead={<FormattedMessage id="task.label" defaultMessage="Tasks" />}
              buttonPlaceholder={
                <FormattedMessage id="common.bulk-action.selectTask" defaultMessage="Select Task" />
              }
              isMulti={false}
              multipleOptionsPlaceHolder="Multiple Tasks"
              buttonProps={{
                disabled: this.props.isArchivedSelected || !canEditIssueTask,
              }}
              disabled={this.props.isArchivedSelected || !canEditIssueTask}
            />
          );
        },
        cellClass: "dropDownCell",
        columnOrder: 5,
      },

      {
        key: severity_C.key,
        name: severity_C.name,
        resizable: true,
        width: columnWidth && columnWidth[severity_C.key] ? columnWidth[severity_C.key].width : 130,
        formatter: value => {
          const issuePermission =
            value && value.row && value.row.projectId
              ? isUndefined(this.props.permissionObject[value.row.projectId]) ? this.props.workspaceIssuePer : this.props.permissionObject[value.row.projectId].issue 
              : this.props.workspaceIssuePer;
          return (
            <IconMenu
              severity={value.row && value.row.severity ? value.row.severity : "Moderate"}
              MenuData={value.row}
              searchFilterApplied={this.props.searchFilterApplied}
              isUpdated={this.isUpdated}
              menuType="severity"
              keyType="severity"
              heading={this.props.intl.formatMessage({
                id: "issue.detail-dialog.severity.placeholder",
                defaultMessage: "Select Severity",
              })}
              viewType="issue"
              permission={issuePermission.issueDetail.issueSeverity.isAllowEdit}
              permissionKey="severity"
              isArchivedSelected={this.props.isArchivedSelected}
              issuePermission={issuePermission}
            />
          );
        },
        cellClass: "dropDownCell",
        columnOrder: 6,
        sortable: teamCanView("advanceSortAccess") ? false : true,
      },
      {
        key: priority_C.key,
        name: priority_C.name,
        resizable: true,
        width: columnWidth && columnWidth[priority_C.key] ? columnWidth[priority_C.key].width : 95,
        formatter: value => {
          const issuePermission =
            value && value.row && value.row.projectId
              ? isUndefined(this.props.permissionObject[value.row.projectId]) ? this.props.workspaceIssuePer : this.props.permissionObject[value.row.projectId].issue 
              : this.props.workspaceIssuePer;
          return (
            <IconMenu
              priority={value.row && value.row.priority ? value.row.priority : ""}
              MenuData={value.row}
              searchFilterApplied={this.props.searchFilterApplied}
              isUpdated={this.isUpdated}
              menuType="priority"
              keyType="priority"
              heading={this.props.intl.formatMessage({
                id: "issue.common.priority.placeholder",
                defaultMessage: "Select Priority",
              })}
              viewType="issue"
              permission={issuePermission.issueDetail.issuePriority.isAllowEdit}
              permissionKey="priority"
              isArchivedSelected={this.props.isArchivedSelected}
              issuePermission={issuePermission}
            />
          );
        },
        cellClass: "dropDownCell",
        columnOrder: 7,
        sortable: teamCanView("advanceSortAccess") ? false : true,
      },
      {
        key: assignees_C.key,
        name: assignees_C.name,
        resizable: true,
        width:
          columnWidth && columnWidth[assignees_C.key] ? columnWidth[assignees_C.key].width : 240,
        formatter: value => {
          const issuePermission =
            value && value.row && value.row.projectId
              ? isUndefined(this.props.permissionObject[value.row.projectId]) ? this.props.workspaceIssuePer : this.props.permissionObject[value.row.projectId].issue 
              : this.props.workspaceIssuePer;
          const assigneeList = this.props.profileState.data.member.allMembers.filter(m => {
            return value.row.issueAssingnees.indexOf(m.userId) > -1;
          });

          return (
            <AssigneeDropdown
              assignedTo={value.row ? assigneeList : []}
              updateAction={this.updateAssignee}
              isArchivedSelected={this.props.isArchivedSelected}
              obj={value.row}
              addPermission={issuePermission.issueDetail.issueAssignee.isAllowAdd}
              deletePermission={issuePermission.issueDetail.issueAssignee.isAllowDelete}
            />
          );
        },
        cellClass: "dropDownCell",
        columnOrder: 8,
      },
      {
        key: type_C.key,
        name: type_C.name,
        resizable: true,
        width: columnWidth && columnWidth[type_C.key] ? columnWidth[type_C.key].width : 135,
        formatter: value => {
          const issuePermission =
            value && value.row && value.row.projectId
              ? isUndefined(this.props.permissionObject[value.row.projectId]) ? this.props.workspaceIssuePer : this.props.permissionObject[value.row.projectId].issue 
              : this.props.workspaceIssuePer;

          return (
            <StatusDropdown
              onSelect={type => {
                this.handleTypeChange(type, value.row);
              }}
              option={this.state.issueTypeData.find(s => {
                return s.value == value.row.type;
              })}
              style={{ marginRight: 8 }}
              options={this.state.issueTypeData}
              toolTipTxt={value.row.type}
              disabled={
                !issuePermission.issueDetail.issueType.isAllowEdit || this.props.isArchivedSelected
              }
              writeFirst={false}
              iconWithText={true}
            />
          );
        },
        cellClass: "dropDownCell",
        columnOrder: 9,
        sortable: teamCanView("advanceSortAccess") ? false : true,
      },
      {
        key: updatedDate_C.key,
        name: updatedDate_C.name,
        resizable: true,
        width:
          columnWidth && columnWidth[updatedDate_C.key]
            ? columnWidth[updatedDate_C.key].width
            : 185,
        formatter: value => {
          return (
            <Typography variant="h6">
              {value.row
                ? helper.RETURN_CUSTOMDATEFORMATFORLASTUPDATE(value.row.updatedDate)
                : helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(new Date())}
            </Typography>
          );
        },
        columnOrder: 10,
        cellClass: "dropDownCell",
        sortable: teamCanView("advanceSortAccess") ? false : true,
      },
      {
        key: createdBy_C.key,
        name: createdBy_C.name,
        resizable: true,
        width:
          columnWidth && columnWidth[createdBy_C.key] ? columnWidth[createdBy_C.key].width : 240,
        formatter: value => {
          return <Typography variant="h6">{value.row ? value.row.createdByName : ""}</Typography>;
        },
        columnOrder: 11,
        sortable: teamCanView("advanceSortAccess") ? false : true,
        cellClass: "dropDownCell",
      },

      {
        key: createdDate_C.key,
        name: createdDate_C.name,
        resizable: true,
        width:
          columnWidth && columnWidth[createdDate_C.key]
            ? columnWidth[createdDate_C.key].width
            : 150,
        formatter: value => {
          return (
            <Typography variant="h6">
              {value.row
                ? helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(value.row.createdDate)
                : helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(new Date())}
            </Typography>
          );
        },
        columnOrder: 12,
        sortable: teamCanView("advanceSortAccess") ? false : true,
        cellClass: "dropDownCell",
      },

      // {
      //   key: label_C.key,
      //   name: label_C.name,
      //   formatter: value => {
      //     return (
      //       // <LabelMenu
      //       //   label={value.row && value.row.label ? value.row.label : "Black Box Testing"}
      //       //   MenuData={value.row}
      //       //   searchFilterApplied={this.props.searchFilterApplied}
      //       //   isUpdated={this.isUpdated}
      //       // />
      //       <IssueMenuList
      //         id={value.row ? value.row.id : ""}
      //         issueLabel={value.row ? value.row.label : ""}
      //         MenuData={value.row}
      //         placementType="bottom-end"
      //         isUpdated={this.isUpdated}
      //         viewType="issue"
      //         isLabel={true}
      //         isArchivedSelected={this.props.isArchivedSelected}
      //       />
      //     );
      //   },
      //   cellClass: "dropDownCell",
      //   columnOrder: 13,
      //   sortable: teamCanView("advanceSortAccess") ? false : true,
      // },
      // {
      //   key: category_C.key,
      //   name: category_C.name,
      //   formatter: value => {
      //     return (
      //       // <CategoryMenu
      //       //   category={value.row && value.row.category ? value.row.category : "Task Board"}
      //       //   MenuData={value.row}
      //       //   searchFilterApplied={this.props.searchFilterApplied}
      //       //   isUpdated={this.isUpdated}
      //       // />
      //       <IssueMenuList
      //         id={value.row ? value.row.id : ""}
      //         issueCategory={value.row ? value.row.category : ""}
      //         MenuData={value.row}
      //         placementType="bottom-end"
      //         isUpdated={this.isUpdated}
      //         viewType="issue"
      //         isCategory={true}
      //         isArchivedSelected={this.props.isArchivedSelected}
      //       />
      //     );
      //   },
      //   cellClass: "dropDownCell",
      //   columnOrder: 14,
      //   width: 200,
      //   sortable: teamCanView("advanceSortAccess") ? false : true,
      // },
    ];
    let customColumns = getCustomFields(
      this.props.customFields,
      this.props.profileState.data,
      "issue"
    ).filter(
      c => c.fieldType.toLowerCase() !== "checklist" && c.fieldType.toLowerCase() !== "matrix"
    );
    /** finding all custom fields realated to risk modules  */
    customColumns = customColumns.map((e, index) => {
      return {
        key: e.fieldName,
        name: e.fieldName,
        resizable: true,
        width: columnWidth && columnWidth[e.fieldName] ? columnWidth[e.fieldName].width : 230,
        formatter: value => {
          const issuePermission =
            value && value.row && value.row.projectId
              ? isUndefined(this.props.permissionObject[value.row.projectId]) ? this.props.workspaceIssuePer : this.props.permissionObject[value.row.projectId].issue 
              : this.props.workspaceIssuePer;
          return (
            <CustomFieldsColumns
              column={e}
              rowValue={value}
              permission={issuePermission.issueDetail.customsFields}
              customFieldData={value.row.customFieldData}
              customFieldChange={this.customFieldChange}
              handleAddAssessmentDialog={this.handleAddAssessmentDialog}
              disabled={this.props.isArchivedSelected}
            />
          );
        },
        columnOrder: 10 + index,
        sortable: teamCanView("advanceSortAccess") ? false : true,
        cellClass: "dropDownCell",
      };
    });
    let arr = [..._columns, ...customColumns];

    let lastColumn = [
      {
        key: "optionList",
        name: "",
        formatter: value => {
          const issuePermission =
            value && value.row && value.row.projectId
              ? isUndefined(this.props.permissionObject[value.row.projectId]) ? this.props.workspaceIssuePer : this.props.permissionObject[value.row.projectId].issue 
              : this.props.workspaceIssuePer;
          return (
            <IssueActionDropdown
              issue={value.row}
              selectedColor={value.row && value.row.colorCode ? value.row.colorCode : { hex: "" }}
              isUpdated={this.isUpdated}
              UpdateIssue={this.props.UpdateIssue}
              DeleteIssue={this.props.DeleteIssue}
              closeActionMenu={this.closeActionMenu}
              isArchivedSelected={this.props.isArchivedSelected}
              filterArchivedIssueDelete={this.props.filterArchivedIssueDelete}
              issuePer={issuePermission}
              showSnackBar={this.props.showSnackBar}
            />
          );
        },
        cellClass: "dropDownCell lastColoumn",
        columnOrder: arr.length,
        width: columnWidth && columnWidth["optionList"] ? columnWidth["optionList"].width : 50,
      },
    ];
    return [...arr, ...lastColumn];
  };

  handleTypeChange = (type, issue) => {
    let issueUpdated = cloneDeep(issue);
    issueUpdated.type = type.value;
    this.props.UpdateIssue(
      issueUpdated,
      data => {
        this.setState({ isInstanceChange: false });
        if (this.props.issueState.indexOf("Archived") >= 0) if (callback) callback();
      },
      error => {
        if (error && error.status === 500) {
          this.setState({ isInstanceChange: false });
          this.props.handleExportType("Server throws error", "error");
        }
      }
    );
  };
  customFieldChange = (option, obj, currentIssue) => {
    const issueCopy = currentIssue;
    const { fieldId } = obj;
    const newObj = {
      groupType: "issue",
      groupId: issueCopy.id,
      fieldId,
      fieldData: { data: option },
    };
    if (obj.fieldType == "matrix") {
      newObj.fieldData.data = newObj.fieldData.data.map(opt => {
        return { cellName: opt.cellName };
      });
    } else {
      if (obj.settings.multiSelect)
        newObj.fieldData.data = newObj.fieldData.data.map(opt => {
          return { id: opt.id };
        });
      else {
        newObj.fieldData.data = { id: `${option.length ? option[0].id : ""}` };
      }
    }

    UpdateIssueCustomField(newObj, res => {
      const resObj = res.data.entity[0];
      let customFieldsArr = [];
      // Updating Global state
      const isExist =
        issueCopy.customFieldData &&
        issueCopy.customFieldData.findIndex(c => c.fieldId === resObj.fieldId) > -1;
      if (isExist) {
        customFieldsArr = issueCopy.customFieldData.map(c => {
          return c.fieldId === resObj.fieldId ? resObj : c;
        });
      } else {
        customFieldsArr = issueCopy.customFieldData
          ? [...issueCopy.customFieldData, resObj]
          : [resObj];
      }
      let newIssueObj = { ...issueCopy, customFieldData: customFieldsArr };
      this.props.dispatchIssue(newIssueObj);
      this.handleAddAssessmentDialog(false, false, {});
    });
  };

  handleAddAssessmentDialog = (value, e = false, issue, fieldId) => {
    const { customFields, profileState } = this.props;
    e && e.stopPropagation();
    e && e.preventDefault();
    const selectedMatrixObj =
      customFields.data.find(
        m =>
          m.settings.assessment &&
          m.settings.assessment.includes(profileState.loggedInTeam) &&
          !m.settings.isHidden &&
          m.settings.hiddenInWorkspaces.indexOf(profileState.loggedInTeam) == -1 &&
          m.settings.hiddenInGroup.indexOf("issue") == -1
      ) || false;
    const assessmentMatrixValues =
      (issue.customFieldData &&
        issue.customFieldData.find(c => c.fieldId === selectedMatrixObj.fieldId)) ||
      false;
    this.setState({
      assessmentDialogOpen: value,
      selectedIssue: issue,
      selectedMatrix: fieldId,
      assessmentMatrixValues: assessmentMatrixValues,
    });
  };

  getContrastYIQ(hexcolor) {
    if (!hexcolor) return "#202020";
    var r = parseInt(hexcolor.substr(1, 2), 16);
    var g = parseInt(hexcolor.substr(3, 2), 16);
    var b = parseInt(hexcolor.substr(5, 2), 16);
    var yiq = (r * 299 + g * 587 + b * 114) / 1000;
    return yiq >= 128 ? "#202020" : "white";
  }

  getCombinedValues = (param, value) => {
    if (param === "array") return value.map(v => v.label).join(",");
    else if (param === "object")
      return value.label ? value.label : value.cellName ? value.cellName : "Select";
    else return "";
  };

  //Function that updates assignee of respective document (e.g. task/risk/issue/meeting)
  updateAssignee = (assignedTo, issue) => {
    let assignedToArr = assignedTo.map(ass => {
      return ass.userId;
    });
    let newIssueObj = { ...issue, issueAssingnees: assignedToArr };

    this.props.UpdateIssue(
      newIssueObj,
      succ => {},
      err => {}
    );
  };

  componentDidMount() {
    const { workspaces } = this.props;
    let loggedInTeam = this.props.profileState.data.loggedInTeam;
    let userId = this.props.profileState.data.userId;

    if (loggedInTeam) {
      let { issueColumn, issueDirection } = getSortOrder(workspaces, loggedInTeam, 4);

      if (issueColumn && issueDirection) {
        let rows = this.getDetailedIssues();
        this.sortRows(rows.completeDetails, issueColumn, issueDirection);
      }
    }

    this.setState({
      // rows: details.completeDetails,
      userId,
      showIssueDetails: false,
      loggedInTeam: this.props.profileState.data.loggedInTeam,
      // members: details.members
    });
  }
  columnChangeCallback = () => {
    this.selectedColumnArr();
  };
  getIssueColumns = () => {
    let columnsDropDownData = getCustomFields(
      this.props.customFields,
      this.props.profileState.data,
      "issue"
    ).map(e => e.fieldName);
    return [...this.state.columnddData, ...columnsDropDownData];
  };

  //Function that updates task project
  updateTask = (selectedTasks, task, issue) => {
    let permission = this.checkIssueListAccordingToPermission(
      this.generateSelectedValue(issue),
      selectedTasks
    );

    if (permission) {
      let taskSelectedIds = selectedTasks.map(obj => {
        /* taskSelectedIds is a list of task id's fetching from selected task object in drop down */
        return obj.id;
      });
      let newIssueObj = {
        ...issue,
        linkedTasks: taskSelectedIds,
        projectId: selectedTasks.length > 0 ? selectedTasks[0].obj.projectId : issue.projectId,
      };
      this.props.UpdateIssue(
        newIssueObj,
        response => {
          this.props.showSnackBar("Issue updated successfully.", "success");
        },
        err => {
          this.props.showSnackBar(err.data.message, "error");
        }
      ); /** api calling to update the issue details */
    }
  };
  checkIssueListAccordingToPermission = (stateTask, selectedTask) => {
    if (stateTask.length <= 1) {
      /** if state task is empty then accept the selected task */
      return true;
    } else {
      this.props.showSnackBar(
        "Oops! You cannot link this task to this issue, because it is already linked to another task. Unlink the issue with the other task first and then try again. ",
        "error"
      );
      return false;
    }
  };
  showSnackBar(snackBarMessage, type) {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  }
  // Generate list of all projects for dropdown understandable form
  generateTaskDropdownData = issue => {
    const { tasks } = this.props;
    let tasksData =
      issue.projectId && issue.projectId !== ""
        ? tasks.filter(t => t.projectId === issue.projectId)
        : tasks;

    let tasksArr = generateTaskData(tasksData);
    return tasksArr;
  };
  //Generate list of selected options for project dropdown understandable form
  getSelectedTask = issue => {
    const { tasks } = this.props;
    let selectedTask = tasks.filter(task => {
      return issue.linkedTasks && issue.linkedTasks.indexOf(task.taskId) > -1;
    });
    return selectedTask.map(selected => {
      return {
        label: selected.taskTitle,
        id: selected.taskId,
        obj: issue,
      };
    });
  };
  queryIssueUrl = () => {
    let searchQuery = this.props.history.location.search; //getting issue id in the url
    const { filteredIssues } = this.props;
    const { showIssueDetails } = this.state;
    if (searchQuery && !showIssueDetails) {
      // checking if there is issue Id in the url or not
      let issueId = queryString.parseUrl(searchQuery).query.issueId; //Parsing the url and extracting the issue id using query string
      let validateissue = filteredIssues.findIndex(issue => {
        // Checking if the issue id is valid or not
        return issueId == issue.id;
      });
      if (validateissue > -1) {
        //If the issue id is valid
        this.setState({ showIssueDetails: true, detailsIssueId: issueId });
      } else {
        this.props.showSnackBar("Oops! Item not found", "info");
        //In case the issue id in the url is wrong user is redirected to issues Page
        this.props.history.push("/issues");
      }
    }
  };

  componentDidUpdate(prevProps, prevState) {
    const { addIssue, isReorder, sortColumn, sortDirection, detailsIssueId } = this.state;
    const { sortObj, filteredIssues } = this.props;
    const issueDetailsIssue = filteredIssues.find(i => {
      // Checking if the issue details issue exist in the list or not
      return i.id == detailsIssueId;
    });
    if (detailsIssueId && !issueDetailsIssue) {
      // if issue does not exist in the filtered list remove the issue details from state
      //This scenario can be produced if you apply filter on a issue with assignee from sidebar and open issue details and change assignee and than toggle filter from sidebar again
      this.setState({ showIssueDetails: false, detailsIssueId: "" });
    }

    if (addIssue == true) {
      this.addIssueInput.current.focus();
    }

    if (JSON.stringify(prevProps.filteredIssues) !== JSON.stringify(this.props.filteredIssues)) {
      if (sortColumn)
        this.sortRows(this.getDetailedIssues().completeDetails, sortColumn, sortDirection);
      else this.setState({ rows: this.getDetailedIssues().completeDetails });
    }
    // Clearing bulk selection on archive mode selection
    else if (JSON.stringify(prevProps.issueState) !== JSON.stringify(this.props.issueState)) {
      this.setState({ selectedIds: [] });
    } else if (isReorder) {
      this.setIssuesOrder();
    }

    if (
      sortObj &&
      sortObj.column &&
      (sortObj.column !== prevProps.sortObj.column ||
        sortObj.direction !== prevProps.sortObj.direction)
    ) {
      /** when user click on selected sort button for changing the sort order, this condtion will execute */
      let details = this.getDetailedIssues();
      this.sortRows(
        details.completeDetails,
        sortObj.column,
        sortObj.direction
      ); /** function call for reseting the state issue list into selected sort manner */
    } else if (
      sortObj &&
      sortObj.cancelSort == "NONE" &&
      sortObj.cancelSort !== prevProps.sortObj.cancelSort
    ) {
      /** when user click on cancel sort this condtion will execute */
      let details = this.getDetailedIssues();
      this.setState({
        rows: details.completeDetails,
      }); /** updating state issue list  */
    }
    if (
      !isEqual(prevProps.customFields, this.props.customFields) ||
      !isEqual(prevProps.issueColumns, this.props.issueColumns)
    ) {
      this.selectedColumnArr();
    }
    this.queryIssueUrl();
    this.renderHeaders();
  }
  handleUpdateColumns = () => {
    /** when adding new column or field then render again the columns so the added column will remaain added */
    this.setState(
      {
        _columns: this.getColumns(),
      },
      () => {
        this.selectedColumnArr();
      }
    );
  };
  renderHeaders = () => {
    var elements = document.getElementsByClassName("widget-HeaderCell__value");
    var sortableElements = document.getElementsByClassName("react-grid-HeaderCell-sortable");
    for (var i = 0; i < elements.length; i++) {
      elements[i].innerText = headerTranslations(elements[i].innerText, this.props.intl);
    }
    for (var i = 0; i < sortableElements.length; i++) {
      sortableElements[i].innerText = headerTranslations(
        sortableElements[i].innerText,
        this.props.intl
      );
    }
  };
  setIssuesOrder = () => {
    let completeDetails = cloneDeep(this.state.rows)
      .map(x => {
        let position = helper.RETURN_ITEMORDER(this.props.itemOrderState.data.itemOrder, x.id);
        x.taskOrder = position || 0;
        return x;
      })
      .sort((a, b) => a.taskOrder - b.taskOrder);
    this.setState({ rows: completeDetails, isReorder: false });
  };

  componentWillUnmount() {
    if (this.canvas) {
      this.canvas.removeEventListener("scroll", this.scrollListener);
    }
  }

  toggleStarChecked = (event, issue) => {
    let isStared = event.target.checked;
    if (issue) {
      let obj = {
        Id: issue.id,
        MarkStar: isStared,
      };
      this.props.MarkIssueAsStarred(obj, (err, data) => {});
    }
  };

  getDetailedIssues = () => {
    let issues = cloneDeep(this.props.filteredIssues) || [];
    let members =
      this.props.profileState && this.props.profileState.data && this.props.profileState.data.member
        ? this.props.profileState.data.member.allMembers
        : [];
    let completeDetails = issues;
    return {
      completeDetails,
      members,
    };
  };

  closeActionMenu(option) {
    if (this.props.issueState.indexOf("Archived") >= 0)
      this.setState({
        isArchived: true,
        detailsIssueId: option === "CloseDetailsDialogue" ? "" : this.state.detailsIssueId,
        showIssueDetails: option === "CloseDetailsDialogue" ? false : this.state.showIssueDetails,
      });
    else
      this.setState({
        isArchived: false,
        detailsIssueId: option === "CloseDetailsDialogue" ? "" : this.state.detailsIssueId,
        showIssueDetails: option === "CloseDetailsDialogue" ? false : this.state.showIssueDetails,
      });
  }

  scrollListener() {
    if (this.canvas.scrollHeight - (this.canvas.scrollTop + this.canvas.clientHeight) < 1) {
      this.handleScrollDown();
    }
  }

  handleScrollDown = () => {
    this.setState({ showRecords: this.state.showRecords + increment }, () => {
      if (this.state.showRecords >= this.state.rows.length) {
        this.setState({ showRecords: this.state.rows.length });
      }
    });
  };

  showIssueDetailsPopUp(e, data) {
    const {permissionObject, workspaceIssuePer} = this.props;
    const issuePermission =
      data && data.projectId
        ? isUndefined(permissionObject[data.projectId]) ? workspaceIssuePer : permissionObject[data.projectId].issue
        : workspaceIssuePer;
    if (data && issuePermission.issueDetail.cando)
      this.setState({ showIssueDetails: true, detailsIssueId: data.id });
  }

  closeIssueDetailsPopUp() {
    this.setState({ showIssueDetails: false });
  }

  isUpdated(data, callback) {
    let issues = this.props.filteredIssues || [];
    let members =
      this.props.profileState && this.props.profileState.data && this.props.profileState.data.member
        ? this.props.profileState.data.member.allMembers
        : [];
    let completeDetails = issues.map(x => {
      if (data.id === x.id) {
        data.assigneeLists = members.filter(m => {
          if (data.issueAssingnees && data.issueAssingnees.length >= 0)
            return data.issueAssingnees.indexOf(m.userId) >= 0;
        });
        return data;
      }
      return x;
    });
    this.setState({ instantData: completeDetails, isInstanceChange: true }, () => {
      if (data.unarchive) {
        this.props.UnarchiveIssue(
          data,
          resp => {
            this.setState({ isInstanceChange: false });
            if (callback) callback();
            this.props.filterIssue([data.id]);
          },
          error => {
            if (error && error.status === 500) {
              this.setState({ isInstanceChange: false });
              this.props.handleExportType("Server throws error", "error");
            }
            if (callback) callback();
          }
        );
      } else {
        this.props.UpdateIssue(
          data,
          data => {
            this.setState({ isInstanceChange: false });
            if (this.props.issueState.indexOf("Archived") >= 0) if (callback) callback();
          },
          error => {
            if (error && error.status === 500) {
              this.setState({ isInstanceChange: false });
              this.props.handleExportType("Server throws error", "error");
            }
          }
        );
      }
    });
  }

  handleDateSave = (startDate, endDate, issue, dateType, startTime, endTime) => {
    const obj =
      dateType == "planned"
        ? {
            startDateString: helper.RETURN_CUSTOMDATEFORMAT(startDate),
            dueDateString: helper.RETURN_CUSTOMDATEFORMAT(endDate),
            startTime: startTime,
            dueTime: endTime,
          }
        : {
            actualStartDateString: helper.RETURN_CUSTOMDATEFORMAT(startDate),
            actualDueDateString: helper.RETURN_CUSTOMDATEFORMAT(endDate),
            actualStartTime: startTime,
            actualDueTime: endTime,
          };
    this.props.UpdateIssue(
      { ...issue, ...obj },
      response => {},
      err => {}
    );
  };

  BulkUpdate(data, callback) {
    this.props.BulkUpdateIssue(
      data,
      resp => {
        if (callback) {
          this.setState({ selectedIds: [] });
          callback();
        }
        if (resp) {
          // Unarchive scenerio
          if (this.props.issueState.indexOf("Archived") >= 0) {
            this.setState({ isArchived: true, selectedIds: [] });
            this.props.filterIssue(data.issueIds);
          } else this.setState({ isArchived: false });
        }
      },
      error => {
        if (callback) callback();
        if (error && error.status === 500) {
          this.props.handleExportType("Server throws error", "error");
        }
      }
    );
  }

  addIssue(keyName) {
    if (keyName && keyName === "enter") {
      this.setState({ addNewForm: true });
    } else if (keyName && keyName === "inline") {
      this.setState({ addIssue: true });
    } else {
      this.setState({ addNewForm: true });
    }
  }

  cancelAddIssue() {
    this.setState({
      addIssue: false,
      addNewForm: false,
      TaskError: false,
      TaskErrorMessage: "",
    });
  }

  handleChange(e) {
    if (e.target.value.length > 250) {
      this.setState({
        TaskError: true,
        TaskErrorMessage: this.props.intl.formatMessage({
          id: "issue.creation-dialog.form.validation.title.length",
          defaultMessage: "Please enter less than 250 characters",
        }),
        value: e.target.value,
      });
    } else {
      this.setState({
        TaskError: false,
        TaskErrorMessage: "",
        value: e.target.value,
      });
    }
  }

  onKeyDown(event) {
    if (event.keyCode === 13) {
      let checkEmpty = helper.RETURN_CECKSPACES(this.state.value);
      if (!this.state.value || checkEmpty) {
        this.setState({
          TaskError: true,
          TaskErrorMessage:
            checkEmpty ||
            this.props.intl.formatMessage({
              id: "issue.creation-dialog.form.validation.title.empty",
              defaultMessage: "Please enter issue title",
            }),
        });

        return;
      }

      if (this.state.value && this.state.value.length > 250) {
        this.setState({
          TaskError: true,
          TaskErrorMessage: this.props.intl.formatMessage({
            id: "issue.creation-dialog.form.validation.title.length2",
            defaultMessage: "Oops! You can add only 250 characters in issue title field.",
          }),
        });
        return;
      }
      this.props.SaveIssue(
        {
          title: this.state.value,
        },
        x => {
          if (x) {
            {
              if (teamCanView("bulkActionAccess")) {
                this.grid.selectAllCheckbox.checked = false;
              }
            }
          }
        },
        error => {
          if (error && error.status === 500) {
            this.props.handleExportType("Server throws error", "error");
          } else {
            this.setState({
              TaskError: false,
              TaskErrorMessage: error && error.data ? error.data : "",
            });
            this.props.handleExportType(error.data, "info");
          }
        }
      );
      this.setState({ value: "" });
    } else if (event.keyCode == 27) {
      this.setState({ addIssue: false });
    } else {
      this.setState({ addIssue: true });
    }
  }

  getRandomDate = (start, end) => {
    return new Date(
      start.getTime() + Math.random() * (end.getTime() - start.getTime())
    ).toLocaleDateString();
  };

  rowGetter = i => {
    if (i >= 0) return this.state.rows[i];
  };

  isDraggedRowSelected = (selectedRows, rowDragSource) => {
    if (selectedRows && selectedRows.length > 0) {
      let key = this.props.rowKey;
      return selectedRows.filter(r => r[key] === rowDragSource.data[key]).length > 0;
    }
    return false;
  };

  reorderRows = e => {
    let selectedRows = Selectors.getSelectedRowsByKey({
      rowKey: this.props.rowKey,
      selectedKeys: this.state.selectedIds,
      rows: this.state.rows,
    });
    let draggedRows = this.isDraggedRowSelected(selectedRows, e.rowSource)
      ? selectedRows
      : [e.rowSource.data];
    let undraggedRows = this.state.rows.filter(function(r) {
      return draggedRows.indexOf(r) === -1;
    });
    let args = [e.rowTarget.idx, 0].concat(draggedRows);
    Array.prototype.splice.apply(undraggedRows, args);

    undraggedRows = undraggedRows.map((x, i) => {
      x.taskOrder = i;
      return x;
    });
    let orderToBeSaved = undraggedRows.map(x => {
      return x.id;
    });
    let itemOrder = this.props.itemOrderState.data;
    itemOrder.itemOrder = itemOrder.itemOrder
      .map((x, i) => {
        if (orderToBeSaved.indexOf(x.itemId) >= 0) {
          return {
            itemId: x.itemId,
            position: orderToBeSaved.indexOf(x.itemId),
          };
        } else return x;
      })
      .sort((a, b) => a.position - b.position);
    this.props.SaveItemOrder(
      itemOrder,
      data => {
        this.setState({
          rows: undraggedRows,
          isReorder: true,
          showIssueDetails: false,
        });
      },
      () => {},
      undraggedRows,
      "issue"
    );
  };
  onRowsSelected = rows => {
    const filterRows = rows.filter(r => !r.row.__metaData); // Removing Grouping Rows from the selected list

    let selectedIds = this.state.selectedIds.concat(filterRows.map(r => r.row[this.props.rowKey]));
    if (selectedIds.length > 1 && !teamCanView("bulkActionAccess")) {
      this.setState({ showUnplanned: true });
      return;
    }
    let selectedIssueObjArr = [...this.state.selectedIssueObj, ...rows];

    this.setState(
      {
        selectedIds: selectedIds,
        showIssueDetails: false,
        selectedIssueObj: selectedIssueObjArr,
      },
      () => {
        if (this.state.rows.length === this.state.selectedIds.length) {
          this.grid.selectAllCheckbox.checked = true;
        }
      }
    );
  };
  filterNTaskField = itemArray => {
    /** function that checks if nTask fields exists and if it is not hide by the user/ other wise filter it from rows and also the column seletion drop down */
    const { customFields, profileState } = this.props;

    let items = itemArray.reduce((result, currentValue) => {
      let isExist = customFields.data.find(cf => cf.fieldName == currentValue);
      if (
        isExist &&
        !isExist.settings.isHidden &&
        isExist.settings.hiddenInWorkspaces.indexOf(profileState.data.loggedInTeam) == -1 &&
        isExist.settings.hiddenInGroup.indexOf("issue") == -1
      )
        result.push(currentValue);
      if (!isExist) result.push(currentValue);
      return result;
    }, []);
    return items;
  };
  selectedColumnArr = () => {
    const { issueColumns = [] } = this.props;
    const issueColumnOrder = issueColumns.filter(c => !c.hide).map(c => c.columnName);

    let newCols = this.getColumns();

    let columns = newCols.filter(
      x =>
        issueColumnOrder.indexOf(x.name) >= 0 ||
        x.columnOrder === newCols.length - 1 ||
        x.key == "title"
    );

    this.setState(
      {
        _columns: columns,
      },
      () => {}
    );
  };

  onRowsDeselected = rows => {
    let rowIds = rows.map(r => r.row[this.props.rowKey]);
    this.setState({
      selectedIds: this.state.selectedIds.filter(i => rowIds.indexOf(i) === -1),
      showIssueDetails: false,
      selectedIssueObj: this.state.selectedIssueObj.filter(
        i => rowIds.indexOf(i.row[this.props.rowKey]) === -1
      ),
    });
  };

  sortRows = (initialRows, sortColumn, sortDirection) => {
    if (sortDirection === "NONE")
      this.setState({
        rows: this.getDetailedIssues().completeDetails,
        sortColumn: null,
        sortDirection,
      });
    else {
      let rows = sortListData(initialRows, sortColumn, sortDirection);
      this.setState({ rows, sortColumn, sortDirection });
    }
  };
  handleClose = event => {
    /** function call when close drop down */
    this.setState({ showUnplanned: false });
  };
  moveIssuesToWorkspace = (data, succ, fail) => {
    this.props.moveIssueToWorkspace(
      data,
      success => {
        this.setState(
          {
            selectedIds: [],
            showIssueDetails: false,
            selectedIssueObj: [],
          },
          () => {
            this.props.FetchWorkspaceInfo("", () => {
              succ();
            });
          }
        );
      },
      failure => {
        this.setState(
          {
            selectedIds: [],
            showIssueDetails: false,
            selectedIssueObj: [],
          },
          () => {
            this.props.showSnackBar("Oops! Server throws error.", "error");
            fail();
          }
        );
      }
    );
  };

  render() {
    const {
      addIssue,
      addNewForm,
      selectedIds,
      detailsIssueId,
      isWorkspaceChanged,
      isInstanceChange,
      rows,
      showUnplanned,
      selectedIssueObj,
      checkedItems,
      columnddData,
    } = this.state;
    const {
      classes,
      theme,
      issueState,
      filteredIssues,
      appliedFiltersState,
      isArchivedSelected,
      selectedGroup,
      workspaceIssuePer,
      intl,
    } = this.props;
    if (this.state.rows.length > 0 && this.state.rows.length === this.state.selectedIds.length) {
      if (this.grid && this.grid.selectAllCheckbox) this.grid.selectAllCheckbox.checked = true;
    } else {
      if (this.grid && this.grid.selectAllCheckbox) this.grid.selectAllCheckbox.checked = false;
    }
    if (isWorkspaceChanged) {
      if (this.grid && this.grid.selectAllCheckbox) this.grid.selectAllCheckbox.checked = false;
    }
    const EmptyRowsView = () => {
      return isArchivedSelected ? (
        <EmptyState
          screenType="Archived"
          heading={
            <FormattedMessage id="common.archived.label" defaultMessage="No archived items found" />
          }
          message={
            <FormattedMessage
              id="common.archived.message"
              defaultMessage="You haven't archived any items yet."
            />
          }
          button={false}
        />
      ) : (issueState.length > 0 || appliedFiltersState.Issue) &&
        (filteredIssues || filteredIssues.length <= 0) ? (
        <EmptyState
          screenType="search"
          heading={
            <FormattedMessage id="common.search-list.label" defaultMessage="No Results Found" />
          }
          message={
            <FormattedMessage
              id="common.search-list.message"
              defaultMessage="No matching results found against your filter criteria."
            />
          }
          button={false}
        />
      ) : workspaceIssuePer.creatIssue.cando ? (
        <EmptyState
          screenType="issue"
          heading={
            <FormattedMessage
              id="common.create-first.issue.label"
              defaultMessage="Create your first issue"
            />
          }
          message={
            <FormattedMessage
              id="common.create-first.issue.messagea"
              defaultMessage='You do not have any issues yet. Press "Alt + I" or click on button below.'
            />
          }
          button={true}
        />
      ) : (
        <EmptyState
          screenType="issue"
          heading={
            <FormattedMessage
              id="common.create-first.issue.messageb"
              defaultMessage="You do not have any issues yet."
            />
          }
          message={
            <FormattedMessage
              id="common.create-first.issue.messagec"
              defaultMessage="Request your team member(s) to assign you a issue."
            />
          }
          button={false}
        />
      );
    };
    const groupBy = selectedGroup.key && [selectedGroup.key];
    const groupedRows = groupBy ? Data.Selectors.getRows({ rows, groupBy }) : rows;
    return (
      <Fragment>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={showUnplanned}
          onClose={this.handleClose}
          onBackdropClick={this.handleClose}
          BackdropProps={{
            classes: {
              root: classes.unplannedRoot,
            },
          }}>
          <div className={classes.unplannedMain} style={{ top: 400, left: 200 }}>
            <UnPlanned
              feature="premium"
              titleTxt={
                <FormattedMessage
                  id="common.discovered-dialog.business-title"
                  defaultMessage="Wow! You've discovered a Premium feature!"
                />
              }
              boldText={this.props.intl.formatMessage({
                id: "common.discovered-dialog.list.bulk-action.title",
                defaultMessage: "Bulk Actions",
              })}
              descriptionTxt={
                <FormattedMessage
                  id="common.discovered-dialog.list.bulk-action.label"
                  defaultMessage={"is available on our Premium Plan. Upgrade now or start a {TRIALPERIOD}-day free trial to find out all nTask Premium features."}
                  values={{TRIALPERIOD: TRIALPERIOD}}
                />
              }
              showBodyImg={false}
              showDescription={true}
              selectUpgrade={this.handleClose}
              titleStyle={{ fontSize: "18px" }}
            />
          </div>
        </Modal>
        <DefaultDialog title="Add Issue" open={addNewForm} onClose={this.cancelAddIssue}>
          <AddIssueForm view={"Header-Menu"} closeAction={this.cancelAddIssue} />
        </DefaultDialog>
        {this.state.showIssueDetails && detailsIssueId && filteredIssues.length > 0
          ? this.props.issueDetailDialogState(null,{
              id: detailsIssueId,
              afterCloseCallBack: () => {
                this.closeIssueDetailsPopUp();
              },
              isUpdated: this.isUpdated,
              closeActionMenu: this.closeActionMenu,
              isArchived: isArchivedSelected,
            })
          : null}
        <Hotkeys keyName="alt+i" onKeyDown={this.onKeyDown} />
        <div style={{ position: "relative" }}>
          <div
            style={{
              width: "100%",
              position: "absolute",
              paddingLeft: 0,
            }}
            className={selectedIds.length > 1 ? "multiChecked" : null}>
            {selectedIds.length > 1 ? (
              <BulkActions
                selectedIdsList={this.state.selectedIds}
                BulkUpdate={this.BulkUpdate}
                isArchivedFilter={this.props.issueState.indexOf("Archived") >= 0}
                workspaceIssuePer={workspaceIssuePer}
                selectedIssueObj={selectedIssueObj}
                moveIssueToWorkspace={this.moveIssuesToWorkspace}
                showSnackBar={this.props.showSnackBar}
                permissionObject={this.props.permissionObject}
              />
            ) : null}
            <div className={classes.TableActionBtnDD}>
              <ColumnSelectionDropdown
                feature={"issue"}
                columnChangeCallback={this.columnChangeCallback}
                hideColumns={["matrix"]}
              />
            </div>

            <DraggableContainer>
              <ReactDataGrid
                onRowClick={this.showIssueDetailsPopUp.bind(this)}
                enableCellSelection={teamCanView("bulkActionAccess")}
                ref={node => (this.grid = node)}
                rowActionsCell={RowActionsCell}
                columns={this.state._columns}
                emptyRowsView={EmptyRowsView}
                onGridSort={(sortColumn, sortDirection) =>
                  this.sortRows(rows, sortColumn, sortDirection)
                }
                rowGetter={i => {
                  return groupedRows[i];
                }}
                rowsCount={groupedRows.length}
                rowHeight={60}
                headerRowHeight={
                  isArchivedSelected || !workspaceIssuePer.creatIssue.cando ? 50 : 105
                }
                minHeight={calculateContentHeight()}
                selectAllCheckbox={true}
                rowRenderer={<RowRenderer onRowDrop={this.reorderRows} />}
                rowGroupRenderer={param => {
                  return GroupingRowRenderer(param, rows, classes, intl);
                }}
                rowSelection={{
                  showCheckbox: true,
                  enableShiftSelect: true,
                  onRowsSelected: this.onRowsSelected,
                  onRowsDeselected: this.onRowsDeselected,
                  selectBy: {
                    keys: {
                      rowKey: this.props.rowKey,
                      values: this.state.selectedIds,
                    },
                  },
                }}
                onColumnResize={this.onColumnResize}
              />
            </DraggableContainer>
            {/* </BottomScrollListener> */}
            {isArchivedSelected ? null : workspaceIssuePer.creatIssue.cando ? (
              <AddIssue
                className={addIssue ? "addTaskInputCnt" : "addTaskCnt"}
                style={{ left: 36 }}
                onClick={!addIssue ? () => this.addIssue("inline") : undefined}>
                {addIssue ? (
                  <Fragment>
                    {this.state.TaskError ? (
                      <div for="TaskError" className={classes.selectError}>
                        {this.state.TaskErrorMessage ? this.state.TaskErrorMessage : ""}
                      </div>
                    ) : null}
                    <input
                      ref={this.addIssueInput}
                      onKeyDown={event => this.onKeyDown(event)}
                      id="addTaskInput"
                      style={{
                        border: this.state.TaskError
                          ? `1px solid ${theme.palette.border.redBorder}`
                          : null,
                      }}
                      autoFocus={this.state.focus}
                      onBlur={this.cancelAddIssue}
                      type="text"
                      onChange={this.handleChange}
                      value={this.state.value}
                    />
                    <Grid item classes={{ item: classes.addTaskInputClearCnt }}>
                      <span className={classes.addTaskInputClearText}>
                        <FormattedMessage
                          id="common.press.message"
                          defaultMessage="Press Enter to Add"
                        />
                      </span>
                      <IconButton btnType="condensed" onClick={this.cancelAddIssue}>
                        <CancelIcon
                          fontSize="default"
                          htmlColor={theme.palette.secondary.light}
                        />
                      </IconButton>
                    </Grid>
                  </Fragment>
                ) : (
                  <Grid container direction="row" justify="space-between" alignItems="center">
                    <Grid item>
                      <Grid container direction="row" justify="flex-start" alignItems="center">
                        <AddIcon htmlColor={theme.palette.primary.light} />
                        <span className={classes.addNewTaskText}>
                          <FormattedMessage
                            id="issue.add-new.label"
                            defaultMessage="Add New Issue"
                          />
                        </span>
                      </Grid>
                    </Grid>
                    <Grid item>
                      <p className={classes.shortcutText}>
                        {" "}
                        {`${this.props.intl.formatMessage({
                          id: "common.press.label",
                          defaultMessage: "Press",
                        })} Alt + I`}
                      </p>
                    </Grid>
                  </Grid>
                )}
              </AddIssue>
            ) : null}
          </div>
        </div>

        <UpdateMatrixDialog
          saveOption={(param1, param2) =>
            this.customFieldChange(param1, param2, this.state.selectedIssue)
          }
          feature={"issue"}
          selectedMatrix={this.state.selectedMatrix}
          open={this.state.assessmentDialogOpen}
          closeAction={e => this.handleAddAssessmentDialog(false, e, {}, null)}
          selectedValues={
            this.state.assessmentMatrixValues
              ? this.state.assessmentMatrixValues.fieldData.data
              : []
          }
        />
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    profileState: state.profile,
    itemOrderState: state.itemOrder,
    appliedFiltersState: state.appliedFilters,
    workspacePermissionsState: state.workspacePermissions,
    tasks: state.tasks.data,
    projects: state.projects.data,
    workspaces: state.profile.data.workspace,
    workspaceIssuePer: state.workspacePermissions.data.issue,
    customFields: state.customFields,
    issueColumns: state.columns.issue,
    permissionObject : ProjectPermissionSelector(state)
  };
};
export default compose(
  withRouter,
  injectIntl,
  withSnackbar,
  withStyles(listStyles, {
    withTheme: true,
  }),
  connect(mapStateToProps, {
    SaveItemOrder,

    ////////////////////////
    issueDetailDialogState,
    UpdateIssue,
    SaveIssue,
    BulkUpdateIssue,
    DeleteIssue,
    ArchiveIssue,
    UnarchiveIssue,
    MarkIssueAsStarred,
    dispatchIssue,
    moveIssueToWorkspace,
    FetchWorkspaceInfo,
  })
)(IssueList_old);
