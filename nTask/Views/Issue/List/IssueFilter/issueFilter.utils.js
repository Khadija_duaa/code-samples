import searchQuery from "../../../../components/CustomTable2/ColumnSettingDropdown/searchQuery";
import { getTemplate } from "../../../../utils/getTemplate";
import { inDateRange, isDateEqual } from "../../../../helper/dates/dates";
import moment from "moment";
import isEmpty from "lodash/isEmpty";

const dateFilter = (selectedFilter, date) => {
  const dateRange = selectedFilter.selectedValues;
  const type = selectedFilter.type;
  if (type == "custom" && (dateRange[0] || dateRange[1])) {
    return inDateRange(date, dateRange[0], dateRange[1]);
  }
  return date && type ? isDateEqual(date, type) : type && !date ? false : true;
};
export const doesFilterPass = node => {
  const { filterId, filterName, recurrence, defaultFilter, ...rest } = searchQuery.issueFilter;
  const appliedFilters = Object.keys(rest);
  const filter = searchQuery.issueFilter;
  const quickFiltersApplied = Object.keys(searchQuery.quickFilters);
  let date;

  const isQuickFilterMatched = quickFiltersApplied.length
    ? quickFiltersApplied.every(q => {
      let selectedFilter = searchQuery.quickFilters[q];
      date = node.data.dueDate;
      switch (q) {
        case "Due Today":
          {
            let template = getTemplate(node.data);
            let taskStatus = template.statusList.find(el => el.statusId == node.data.status);
            return selectedFilter && selectedFilter.type
              ? isDateEqual(date, "today") && !taskStatus.isDoneState
              : true;
          }

        case "Over Due":
          {
            let template = getTemplate(node.data);
            let taskStatus = template.statusList.find(el => el.statusId == node.data.status);
            return selectedFilter && selectedFilter.type
              ? moment()
                .subtract(1, "days")
                .isAfter(date) && !taskStatus.isDoneState
              : true;
          }
          break;
        case "Starred":
          return selectedFilter && selectedFilter.type ? node.data.isStared : true;
          break;
        case "priority":
          return selectedFilter.selectedValues && selectedFilter.selectedValues.length
            ? selectedFilter.selectedValues.includes(node.data.priority)
            : true;
          break;
        case "severity":
          return selectedFilter.selectedValues && selectedFilter.selectedValues.length
            ? selectedFilter.selectedValues.includes(node.data.severity)
            : true;
          break;
        case "unassignedIssues":
          return selectedFilter && selectedFilter.type ? isEmpty(node.data.assignee) : true;
          break;
        case "Archived":
          return selectedFilter && selectedFilter.type ? node.data.isArchive : true;
          break;
      }
    })
    : true;

  const isMatched = appliedFilters.length
    ? appliedFilters.every(f => {
      let selectedFilter = filter[f];
      if (!selectedFilter) {
        return true;
      }
      if (selectedFilter.customField) {
        const customField = node.data.customFieldData && node.data.customFieldData.find(x => x.fieldId == f);
        if (
          customField &&
          (customField.fieldType === "textfield" ||
            customField.fieldType === "email" ||
            customField.fieldType === "websiteurl" ||
            customField.fieldType === "location" ||
            customField.fieldType === "filesAndMedia")
        ) {
          const fieldData =
            customField.fieldType === "location"
              ? `${customField.fieldData.data.lineOne || ""} ${customField.fieldData.data
                .lineTwo || ""} ${customField.fieldData.data.city || ""} ${customField.fieldData
                  .data.province || ""} ${customField.fieldData.data.zipCode || ""} ${customField
                    .fieldData.data.countryCode || ""}`
              : customField.fieldType === "filesAndMedia"
                ? customField.fieldData.data.map(file => file.fileName).join(",")
                : customField.fieldData.data;
          switch (selectedFilter.type) {
            case "contains":
              return selectedFilter.selectedValues
                ? fieldData.trim().toLowerCase().includes(selectedFilter.selectedValues.trim().toLowerCase())
                : true;
              break;
            case "notContains":
              return selectedFilter.selectedValues
                ? !fieldData.trim().toLowerCase().includes(selectedFilter.selectedValues.trim().toLowerCase())
                : true;
              break;
            case "equals":
              return selectedFilter.selectedValues
                ? fieldData.trim().toLowerCase() === selectedFilter.selectedValues.trim().toLowerCase()
                : true;
              break;
            case "notEquals":
              return selectedFilter.selectedValues
                ? fieldData.trim().toLowerCase() !== selectedFilter.selectedValues.trim().toLowerCase()
                : true;
              break;
            case "startsWith":
              return selectedFilter.selectedValues
                ? fieldData.trim().toLowerCase().startsWith(selectedFilter.selectedValues.trim().toLowerCase())
                : true;
              break;
            case "endsWith":
              return selectedFilter.selectedValues
                ? fieldData.trim().toLowerCase().endsWith(selectedFilter.selectedValues.trim().toLowerCase())
                : true;
              break;
            default:
              return true;
              break;
          }
        }
        if (
          customField &&
          (customField.fieldType === "number" ||
            customField.fieldType === "money" ||
            customField.fieldType === "formula" ||
            customField.fieldType === "phone")
        ) {
          const fieldDataNumeric =
            customField.fieldType === "formula"
              ? customField.fieldData.data.result
              : customField.fieldType === "phone"
                ? customField.fieldData.data.substring(1)
                : customField.fieldData.data;
          switch (selectedFilter.type) {
            case "equals":
              return selectedFilter.selectedValues
                ? parseFloat(fieldDataNumeric) == parseFloat(selectedFilter.selectedValues)
                : true;
              break;
            case "notEquals":
              return selectedFilter.selectedValues
                ? parseFloat(fieldDataNumeric) !== parseFloat(selectedFilter.selectedValues)
                : true;
              break;
            case "lessThan":
              return selectedFilter.selectedValues
                ? parseFloat(fieldDataNumeric) < parseFloat(selectedFilter.selectedValues)
                : true;
              break;
            case "lessThanOrEqual":
              return selectedFilter.selectedValues
                ? parseFloat(fieldDataNumeric) <= parseFloat(selectedFilter.selectedValues)
                : true;
              break;
            case "greaterThan":
              return selectedFilter.selectedValues
                ? parseFloat(fieldDataNumeric) > parseFloat(selectedFilter.selectedValues)
                : true;
              break;
            case "greaterThanOrEqual":
              return selectedFilter.selectedValues
                ? parseFloat(fieldDataNumeric) >= parseFloat(selectedFilter.selectedValues)
                : true;
              break;
            case "inRange":
              return selectedFilter.selectedValues
                ? parseFloat(fieldDataNumeric) >= parseFloat(selectedFilter.selectedValues[0]) &&
                parseFloat(fieldDataNumeric) <= parseFloat(selectedFilter.selectedValues[1])
                : true;
              break;

            default:
              return true;
              break;
          }
        }
        if (customField) {
          switch (customField.fieldType) {
            case "date": {
              return dateFilter(selectedFilter, customField.fieldData.data.date);
              break;
            }
            case "people": {
              return selectedFilter.selectedValues && selectedFilter.selectedValues.length
                ? selectedFilter.selectedValues.some(x => customField.fieldData.data.includes(x))
                : true;
              break;
            }
            case "country": {
              return selectedFilter.selectedValues && selectedFilter.selectedValues.length
                ? selectedFilter.selectedValues.some(x => x == customField.fieldData.data)
                : true;
              break;
            }
            case "rating": {
              return selectedFilter.selectedValues && selectedFilter.selectedValues.length
                ? selectedFilter.selectedValues.some(x => x == customField.fieldData.data)
                : true;
              break;
            }
            case "dropdown": {
              if (selectedFilter.selectedValues && selectedFilter.selectedValues.length) {
                if (selectedFilter.type == "multi")
                  return customField.fieldData.data.some(s =>
                    selectedFilter.selectedValues.includes(s.id)
                  );
                else return selectedFilter.selectedValues.includes(customField.fieldData.data.id);
              } else return true;
              break;
            }
            default:
              return true;
          }
        }
        return false;
      }
      switch (f) {
        case "priority":
          return selectedFilter.selectedValues && selectedFilter.selectedValues.length
            ? selectedFilter.selectedValues.includes(node.data.priority)
            : true;
          break;
        case "project":
          return selectedFilter.selectedValues && selectedFilter.selectedValues.length
            ? selectedFilter.selectedValues.includes(node.data.projectId)
            : true;
          break;
        case "tasks":
          return selectedFilter.selectedValues && selectedFilter.selectedValues.length
            ? selectedFilter.selectedValues.some(t => node.data.tasks && node.data.tasks.length && node.data.tasks[0].taskId == t)
            : true;
          break;
        case "createdBy":
          return selectedFilter.selectedValues && selectedFilter.selectedValues.length
            ? selectedFilter.selectedValues.some(x => node.data.createdById == x)
            : true;
          break;
        case "updatedBy":
          return selectedFilter.selectedValues && selectedFilter.selectedValues.length
            ? selectedFilter.selectedValues.some(x => node.data.updatedById == x)
            : true;
          break;
        case "assignee":
          return selectedFilter.selectedValues && selectedFilter.selectedValues.length
            ? selectedFilter.selectedValues.some(x => node.data[f].includes(x))
            : true;
          break;
        case "actualStartDate":
          return dateFilter(selectedFilter, node.data[f]);
          break;
        case "actualDueDate":
          return dateFilter(selectedFilter, node.data[f]);
          break;
        case "startDate":
          return dateFilter(selectedFilter, node.data[f]);
        case "dueDate":
          return dateFilter(selectedFilter, node.data[f]);
          break;
        case "createdDate":
          return dateFilter(selectedFilter, node.data[f]);
          break;
        case "updatedDate":
          return dateFilter(selectedFilter, node.data[f]);
          break;
        case "status":
          return selectedFilter.selectedValues && selectedFilter.selectedValues.length
            ? selectedFilter.selectedValues.includes(node.data.status)
            : true;
          break;
        case "severity":
          return selectedFilter.selectedValues && selectedFilter.selectedValues.length
            ? selectedFilter.selectedValues.includes(node.data.severity)
            : true;
          break;
        case "type":
          return selectedFilter.selectedValues && selectedFilter.selectedValues.length
            ? selectedFilter.selectedValues.includes(node.data.type)
            : true;
          break;
        default:
          return false;
      }
    })
    : true;

  return isMatched && isQuickFilterMatched;
};
