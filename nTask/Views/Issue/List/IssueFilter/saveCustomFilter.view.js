import React, { useEffect, useState } from "react";
import { compose } from "redux";

import { useDispatch, useSelector } from "react-redux";
import SelectSearchDropdown from "../../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { FormattedMessage, injectIntl } from "react-intl";
import { generateAssigneeData } from "../../../../helper/generateSelectData";
import CreateableSelectDropdown from "../../../../components/Dropdown/SelectCreateableDropdown/SelectCreateableDropdown";
import CircularIcon from "@material-ui/icons/Brightness1";
import { dateFilterOptions, taskDateTypes } from "./constants";
import CustomDatePicker from "../../../../components/DatePicker/DatePicker/DatePicker";
import taskFilterStyles from "./issueFilter.style";
import { withStyles } from "@material-ui/core/styles";
import { priorityData } from "../../../../helper/taskDropdownData";
import Typography from "@material-ui/core/Typography";
import CustomButton from "../../../../components/Buttons/CustomButton";
import { updateTaskFilter } from "../../../../redux/actions/tasks";
import moment from "moment";
import DefaultTextField from "../../../../components/Form/TextField";
import SvgIcon from "@material-ui/core/SvgIcon";
import PushPin from "../../../../components/Icons/PushPin";
import HelpIcon from "@material-ui/icons/HelpOutline";
import DefaultSwitch from "../../../../components/Form/Switch";
import UnPlanned from "../../../billing/UnPlanned/UnPlanned";
import { teamCanView } from "../../../../components/PlanPermission/PlanPermission";
import { addNewFilter } from "../../../../redux/actions/issues";
import {TRIALPERIOD} from '../../../../components/constants/planConstant';

function SaveCustomFilter(props) {
  const state = useSelector(state => {
    return {
      taskFilter: state.tasks.taskFilter,
    };
  });
  const dispatch = useDispatch();
  const {
    classes,
    theme,
    feature,
    createNewFilterCallback,
    currentFilterValues,
    isFilterApplied,
    intl,
    backButtonProps
  } = props;

  const [filterName, setFilterName] = useState("");
  const [error, setError] = useState("");
  const [isDefault, setIsDefault] = useState(false);
  const [saveFilterBtnQuery, setSaveFilterBtnQuery] = useState("");
  const handleFilterNameChange = e => {
    setError("");
    setFilterName(e.target.value);
  };
  //Handle Default Switch
  const handleDefaultSwitch = () => {
    setIsDefault(!isDefault);
  };

  //Save Custom filter to backend
  const handleSaveFilter = () => {
    setSaveFilterBtnQuery("progress");

    let filtes = Object.keys(currentFilterValues);
    let updatedObj = filtes.reduce(
      (result, cv) => {
        if (currentFilterValues[cv] && currentFilterValues[cv].customField) {
          result.customFieldsFilters[cv] = {
            ...result.customFieldsFilters,
            ...currentFilterValues[cv],
          };
        } else {
          result[cv] = currentFilterValues[cv];
        }
        return result;
      },
      {
        customFieldsFilters: {},
      }
    );
    const filterObj = {
      IssueFilter: {
        ...updatedObj,
        filterName: filterName.replace(/^\s+|\s+$/gm, ""),
        defaultFilter: isDefault,
      },
    };
    addNewFilter(filterObj, feature, dispatch, () => {
      setFilterName("");
      setSaveFilterBtnQuery("");
      createNewFilterCallback();
    });
  };
  return (
    <>
      {!teamCanView("saveCustomFilter") ? (
        <div className={classes.unplannedMain}>
          <UnPlanned
            feature="business"
            titleTxt={
              <FormattedMessage
                id="common.discovered-dialog.business-title"
                defaultMessage="Wow! You've discovered a Business feature!"
              />
            }
            boldText={intl.formatMessage({
              id: "common.discovered-dialog.list.custom-filter.title",
              defaultMessage: "Custom Filters",
            })}
            descriptionTxt={
              <FormattedMessage
                id="common.discovered-dialog.list.custom-status.label"
                values={{ n: <span style={{ color: "#0090ff" }}>nTask Business</span> }}
                defaultMessage={"is available on our Business Plan. Upgrade now or start a {TRIALPERIOD}-day free trial to find out all nTask Business features."}
                values={{TRIALPERIOD: TRIALPERIOD}}
              />
            }
            showBodyImg={false}
            showDescription={true}
          />
        </div>
      ) : (
        <div className={classes.filterContentCnt}>
          <DefaultTextField
            label="Filter Name"
            error={!error}
            errorMessage={error}
            defaultProps={{
              type: "text",
              placeholder: "Enter Filter Name",
              value: filterName,
              autoFocus: true,
              inputProps: { maxLength: 80 },
              onChange: handleFilterNameChange,
            }}
          />
          <div className={classes.switchCnt} style={{ marginLeft: 0 }}>
            <p className={classes.switchLabel}>
              <SvgIcon
                viewBox="0 0 8 11.562"
                className={classes.pushPinIcon}
                htmlColor={theme.palette.secondary.light}>
                <PushPin />
              </SvgIcon>
              <FormattedMessage
                id="filters.form.message"
                defaultMessage="Make this filter my default view"
              />

              <HelpIcon
                classes={{ root: classes.filtersHeaderHelpIcon }}
                htmlColor={theme.palette.secondary.medDark}
                fontSize="small"
              />
            </p>
            <DefaultSwitch checked={isDefault} onChange={handleDefaultSwitch} />
          </div>
        </div>
      )}

      <div className={classes.searchBtnCnt}>
        <CustomButton
          style={{ flex: 1, marginRight: 20 }}
          btnType="plain"
          variant="text"
          // disabled={saveFilterBtnQuery == "progress"}
          // query={saveFilterBtnQuery}
          {...backButtonProps}
        >
          Back
        </CustomButton>
        {teamCanView("saveCustomFilter") && (
          <CustomButton
            style={{ flex: 1 }}
            btnType="success"
            variant="contained"
            onClick={handleSaveFilter}
            disabled={!filterName || saveFilterBtnQuery == "progress"}
            query={saveFilterBtnQuery}>
            Save Filter
          </CustomButton>
        )}
      </div>
    </>
  );
}

export default compose(
  injectIntl,
  withStyles(taskFilterStyles, { withTheme: true })
)(SaveCustomFilter);
