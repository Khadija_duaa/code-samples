import React from "react"
import getIssueByStatusType from "../../../helper/getIssueByStatusType";
import getIssueByType from "../../../helper/getIssueByType";
import getIssueBySeverityType from "../../../helper/getIssueBySeverityType";
import getIssueByPriorityType from "../../../helper/getIssueByPriorityType";
import { getTaskProjectName } from "../../../helper/getTaskProjectName";
import Typography from "@material-ui/core/Typography";
function getTranslatedLabel(name, intl) {
  let trans = null;
  switch(name) {
     case "Open":
      trans = {id:"issue.common.status.drop-down.open", defaultMessage: name};
      break;
     case "Closed":
      trans = {id:"issue.common.status.drop-down.close", defaultMessage: name};
        break;
     case "Critical":
      trans = {id:"issue.common.severity.dropdown.critical", defaultMessage: name};
      break;
     case "Major":
      trans = {id:"issue.common.severity.dropdown.major", defaultMessage: name};
      break;   
     case "Minor":
      trans = {id:"issue.common.severity.dropdown.minor", defaultMessage: name};
      break;
     case "Moderate":
      trans = {id:"issue.common.severity.dropdown.moderate", defaultMessage: name};
      break;
      case "High":
      trans = {id:"issue.common.priority.dropdown.high", defaultMessage: name};
      break;
      case "Low":
      trans = {id:"issue.common.priority.dropdown.low", defaultMessage: name};
      break;
      case "Medium":
      trans = {id:"issue.common.priority.dropdown.medium", defaultMessage: name};
      break;
      case "Agreed":
      trans = {id:"risk.common.status.dropdown.agreed", defaultMessage: name};
      break;
      case "Identified":
      trans = {id:"risk.common.status.dropdown.identified", defaultMessage: name};
      break;
      case "In Review":
      trans = {id:"risk.common.status.dropdown.in-review", defaultMessage: name};
      break;
      case "Rejected":
      trans = {id:"risk.common.status.dropdown.rejected", defaultMessage: name};
      break;
      case "Bug":
        trans = {id:"issue.common.issue-type.drop-down.bug", defaultMessage: name};
        break;
        case "Feature":
          trans = {id:"issue.common.issue-type.drop-down.feature", defaultMessage: name};
          break;
          case "Improvement":
            trans = {id:"issue.common.issue-type.drop-down.improvement", defaultMessage: name};
            break;
      case "null":
        trans = {id: "common.others.label", defaultMessage: name};
        break;
  }
  return trans == null ? name : intl.formatMessage(trans);
}
export function GroupingRowRenderer(param, issues, classes, intl=null) { // Grouping row renderer
  const { name, columnGroupName } = param;
  let issueCount;
  let value;
  switch (columnGroupName) { // Switch statement used to calculate countes of the issues according to the grouping selected
    case ('type'):
      issueCount = getIssueByType(issues, name) //Function to return issues based on priority
      value = intl != null ?  getTranslatedLabel(name, intl) : name;
      break;
    case ('priority'):
      issueCount = getIssueByPriorityType(issues, name) //Function to return issues based on priority
      value = intl != null ?  getTranslatedLabel(name, intl) : name;
      break;
    case ('status'): {
      issueCount = getIssueByStatusType(issues, name) //Function to return issues based on issue status
      value = intl != null ?  getTranslatedLabel(name, intl) : name;
    }
      break;
    case ('severity'): {
      issueCount = getIssueBySeverityType(issues, name); //Function to return issues based on issue project
      value = intl != null ?  getTranslatedLabel(name, intl) : name;
    }
    default:
      break;
  }
  return <div className={classes.groupingRow} {...param.renderer}>
    <div className={classes.groupingRowTextCnt}>
      <Typography variant="body1" className={classes.groupingRowText}>{value && value !== "null" ? value : "Others"}</Typography>
      <span className={classes.groupingCount}>{issueCount && issueCount > 999 ? "999+" : issueCount ? issueCount.length : null}</span>
    </div>
  </div>
}