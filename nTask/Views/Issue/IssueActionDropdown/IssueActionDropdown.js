import React, { useState, useRef, useEffect, memo } from "react";
import { useDispatch } from "react-redux";
import DropdownMenu from "../../../components/Dropdown/DropdownMenu";
import CustomIconButton from "../../../components/Buttons/CustomIconButton";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import CustomListItem from "../../../components/ListItem/CustomListItem";
import withStyles from "@material-ui/core/styles/withStyles";
import taskActionDropdownStyles from "./issueActionDropdown.style";
import CustomMenuList from "../../../components/MenuList/CustomMenuList";

import {
  DeleteIssue,
  ArchiveIssue,
  updateIssueData,
  UnarchiveIssue,
  moveIssueToWorkspace,
} from "../../../redux/actions/issues";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import { FormattedMessage } from "react-intl";
import { withSnackbar } from "notistack";
import { compose } from "redux";
import { setDeleteDialogueState } from "../../../redux/actions/allDialogs";
import ColorPicker from "../../../components/Dropdown/ColorPicker/ColorPicker";
import isEqual from "lodash/isEqual";
import "react-toastify/dist/ReactToastify.css";
import IconActivity from "../../../components/Icons/TaskActionIcons/IconActivity";
import IconArchive from "../../../components/Icons/TaskActionIcons/IconArchive";
import IconColor from "../../../components/Icons/TaskActionIcons/IconColor";
import IconDelete from "../../../components/Icons/TaskActionIcons/IconDelete";
import SvgIcon from "@material-ui/core/SvgIcon";
import MoveIssueDialog from "../../../components/Dialog/MoveIssueDialog/MoveIssueDialog";
import MoveIcon from "../../../components/Icons/MoveIcon";

// import constants from "../constants/types";
function areEqual(prevProps, nextProps) {
  return isEqual(prevProps, nextProps);
  /*
  return true if passing nextProps to render would return
  the same result as passing prevProps to render,
  otherwise return false
  */
}
const IssueActionDropdown = memo(props => {
  const [open, setOpen] = useState(null);
  const [archiveConfirmation, setArchiveConfirmation] = useState(false);
  const [unArchiveConfirmation, setUnArchiveConfirmation] = useState(false);
  const [archiveBtnQuery, setArchiveBtnQuery] = useState("");
  const [unarchiveBtnQuery, setUnarchiveBtnQuery] = useState("");
  const [moveIssuesToWorkspace, setMoveIssueToWorkspace] = useState(false);
  const [moveIssueBtnQuery, setMoveIssueBtnQuery] = useState("");
  const anchorEl = useRef(null);
  const dispatch = useDispatch();
  const {
    classes,
    data,
    btnProps,
    handleActivityLog,
    handleCloseCallBack,
    issuePermission,
    isArchived = false,
    activityLogOption = false,
  } = props;
  const handleClose = event => {
    // Function closes dropdown
    setOpen(null);
  };
  const handleArchiveConfirmClose = () => {
    setArchiveConfirmation(false);
    setUnArchiveConfirmation(false);
  };
  const handleArchiveConfirmation = () => {
    setArchiveConfirmation(true);
  };
  const handleUnArchiveConfirmation = () => {
    setUnArchiveConfirmation(true);
  };
  const handleDropdownOpen = event => {
    // Function Opens the dropdown
    setOpen(state => (state ? null : event.currentTarget));
  };
  const showSnackBar = (snackBarMessage = "", type = null) => {
    const { classes, enqueueSnackbar } = props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  useEffect(() => {
    anchorEl.current &&
      anchorEl.current.addEventListener("click", e => {
        e.stopPropagation();
        handleDropdownOpen(e);
      });
  }, []);
  //Delete single issue
  const deleteIssue = () => {
    handleCloseCallBack();
    dispatch(setDeleteDialogueState({ btnQuery: "progress" }));
    DeleteIssue(
      data.id,
      (data, res) => {
        closeDeleteConfirmation();
      },
      err => {
        if (err.data && err.data.message) showSnackBar(err.data.message, "error");
        closeDeleteConfirmation();
      },
      dispatch
    );
  };

  //archive a issue
  const archiveIssue = () => {
    ArchiveIssue(
      data,
      () => {
        // Success Callback
        handleArchiveConfirmClose();
        setArchiveBtnQuery("");
      },
      err => {
        // failure callback
        setArchiveBtnQuery("");
        handleArchiveConfirmClose();
        if (err.data && err.data.message) showSnackBar(err.data.message, "error");
      },
      dispatch
    );
    handleClose();
  };

  const handleUnArchive = e => {
    if (e) e.stopPropagation();
    setUnarchiveBtnQuery(true);
    handleCloseCallBack();
    UnarchiveIssue(
      data,
      //success
      () => {
        handleArchiveConfirmClose();
        setUnarchiveBtnQuery(false);
      },
      //failure
      error => {
        setUnarchiveBtnQuery(false);
        showSnackBar("Issue Cannot be UnArchived", "error");
      },
      dispatch
    );
  };

  //Confirmation Dialog close
  const closeDeleteConfirmation = () => {
    dispatch(setDeleteDialogueState({ open: false, btnQuery: "" }));
  };
  //Open delete confirmation dialog
  const handleDeleteConfirmation = success => {
    const dialogObj = {
      open: true,
      cancelBtnText: "Cancel",
      successBtnText: "Delete",
      headingText: "Delete",
      msgText: `Are you sure you want to delete this issue?`,
      successAction: deleteIssue,
      closeAction: closeDeleteConfirmation,
      btnQuery: "",
    };
    dispatch(setDeleteDialogueState(dialogObj));
    handleClose();
  };
  const colorChange = color => {
    const obj = { colorCode: color };
    handleClose();
    updateIssueData(
      { issue: data, obj },
      dispatch,
      //Success
      issue => { }
    );
  };
  const handleMove = () => {
    setMoveIssueToWorkspace(true);
  };
  const handleCloseMove = () => {
    setMoveIssueToWorkspace(false);
  };
  const handleMoveIssue = (e, team) => {
    if (e) e.stopPropagation();
    setMoveIssueBtnQuery("progress");
    let obj = {
      issuesId: [data.id],
      workspaceId: team.teamId,
    };
    moveIssueToWorkspace(
      obj,
      succ => {
        setMoveIssueBtnQuery("");
        handleCloseMove();
      },
      err => {
        setMoveIssueBtnQuery("");
        handleCloseMove();
        if (err.data && err.data.message) showSnackBar(err.data.message, "error");
      },
      dispatch
    );
  };
  // permissions allowed
  let archivePer = issuePermission ? issuePermission.archive.cando : true;
  let unarchivePer = issuePermission ? issuePermission.unarchives.cando : true;
  let deletePer = issuePermission ? issuePermission.delete.cando : true;
  let moveIssue = issuePermission ? issuePermission.issueDetail.moveIssue.cando : true;

  const isOpen = Boolean(open);
  return (
    <>
      <CustomIconButton
        btnType="transparent"
        buttonRef={node => (anchorEl.current = node)}
        style={{ padding: 0 }}
        {...btnProps}>
        <MoreVerticalIcon className={classes.ellipsesIcon} />
      </CustomIconButton>
      <DropdownMenu
        open={isOpen}
        closeAction={handleClose}
        anchorEl={anchorEl.current}
        size={"small"}
        placement="bottom-end"
        disablePortal={false}>
        <CustomMenuList>
          {!isArchived && (
            <CustomListItem
              subNav={true}
              popperProps={{
                placement: "left-start",
              }}
              subNavRenderer={
                <>
                  <ColorPicker
                    triangle="hide"
                    onColorChange={colorChange}
                    selectedColor={data.colorCode || ""}
                    width={190}
                  />
                </>
              }>
              <SvgIcon viewBox="0 0 16.5 16.5" className={classes.icon}>
                <IconColor />
              </SvgIcon>
              Color
            </CustomListItem>
          )}

          {activityLogOption && !isArchived && (
            <CustomListItem rootProps={{ onClick: handleActivityLog }}>
              <SvgIcon viewBox="0 0 18 15.428" className={classes.icon}>
                <IconActivity />
              </SvgIcon>
              Activity Log
            </CustomListItem>
          )}
          {archivePer && !isArchived && (
            <CustomListItem rootProps={{ onClick: handleArchiveConfirmation }}>
              <SvgIcon viewBox="0 0 14 14.014" className={classes.icon}>
                <IconArchive />
              </SvgIcon>
              Archive
            </CustomListItem>
          )}
          {!isArchived && moveIssue && (
            <CustomListItem rootProps={{ onClick: handleMove }}>
              <SvgIcon viewBox="0 0 40 40" className={classes.icon}>
                <MoveIcon />
              </SvgIcon>
              Move
            </CustomListItem>
          )}
          {unarchivePer && isArchived && (
            <CustomListItem rootProps={{ onClick: handleUnArchiveConfirmation }}>
              <SvgIcon viewBox="0 0 14 14.014" className={classes.icon}>
                <IconArchive />
              </SvgIcon>
              Un Archive
            </CustomListItem>
          )}
          {deletePer && (
            <CustomListItem rootProps={{ onClick: handleDeleteConfirmation }}>
              <SvgIcon viewBox="0 0 14 14.002" className={classes.icon}>
                <IconDelete />
              </SvgIcon>
              Delete
            </CustomListItem>
          )}
        </CustomMenuList>
      </DropdownMenu>
      {archiveConfirmation ? (
        <ActionConfirmation
          open={true}
          closeAction={handleArchiveConfirmClose}
          cancelBtnText={
            <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
          }
          successBtnText={
            <FormattedMessage
              id="common.action.archive.confirmation.archive-button.label"
              defaultMessage="Archive"
            />
          }
          alignment="center"
          headingText={
            <FormattedMessage
              id="common.action.archive.confirmation.archive-button.label"
              defaultMessage="Archive"
            />
          }
          iconType="archive"
          msgText={
            <FormattedMessage
              id="common.archived.tasks.messageb"
              defaultMessage="Are you sure you want to archive this issue?"
              values={{
                label: 'issue'
              }}
            />
          }
          successAction={archiveIssue}
          btnQuery={archiveBtnQuery}
        />
      ) : null}
      {unArchiveConfirmation ? (
        <ActionConfirmation
          open={unArchiveConfirmation}
          closeAction={handleArchiveConfirmClose}
          cancelBtnText={
            <FormattedMessage
              id="common.action.un-archive.confirmation.cancel-button.label"
              defaultMessage="Cancel"
            />
          }
          successBtnText={
            <FormattedMessage
              id="common.action.un-archive.confirmation.un-archive-button.label"
              defaultMessage="Unarchive"
            />
          }
          alignment="center"
          iconType="unarchive"
          headingText={
            <FormattedMessage
              id="common.action.un-archive.confirmation.title"
              defaultMessage="Unarchive"
            />
          }
          msgText={
            <FormattedMessage
              id="common.action.un-archive.confirmation.task.label"
              defaultMessage="Are you sure you want to unarchive this issue?"
              values={{
                label: 'issue'
              }}
            />
          }
          successAction={handleUnArchive}
          btnQuery={unarchiveBtnQuery}
        />
      ) : null}
      <MoveIssueDialog
        open={moveIssuesToWorkspace}
        issueName={data.title}
        issue={data}
        successAction={handleMoveIssue}
        closeAction={handleCloseMove}
        btnQuery={moveIssueBtnQuery}
        cancelBtnText={<FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />}
        exceptionText={""}
        headingText={"Move Issue to Workspace"}
        isMulti={false}
      />
    </>
  );
}, areEqual);

export default compose(
  withSnackbar,
  withStyles(taskActionDropdownStyles, { withTheme: true })
)(IssueActionDropdown);
