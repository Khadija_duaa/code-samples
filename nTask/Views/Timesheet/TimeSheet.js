import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
import { connect } from "react-redux";
import moment from "moment";
import findIndex from "lodash/findIndex";
import sortBy from "lodash/sortBy";
import cloneDeep from "lodash/cloneDeep";
import CloseIcon from "@material-ui/icons/Close";
import IconButton from "../../components/Buttons/IconButton";
import LeftArrow from "@material-ui/icons/ArrowLeft";
import RightArrow from "@material-ui/icons/ArrowRight";
import { withStyles } from "@material-ui/core/styles";
import timesheetStyles from "./styles";
import DefaultTextField from "../../components/Form/TextField";
import CustomIconButton from "../../components/Buttons/CustomIconButton";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import CustomButton from "../../components/Buttons/CustomButton";
import LockIcon from "@material-ui/icons/LockOpen";
import ActionConfirmation from "../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import TaskListMenu from "../../components/Menu/TaskMenus/TaskListMenu";
import EmptyState from "../../components/EmptyStates/EmptyState";
import CustomTooltip from "../../components/Tooltip/Tooltip";
import AddIcon from "@material-ui/icons/Add";
import AddDurationDialog from "./AddDurationDialog";
import { calculateContentHeight, setCaretPosition, maskInputValue } from "../../utils/common";
import { addUserEffort } from "../../redux/actions/tasks";
import CustomDatePicker from "../../components/DatePicker/DatePicker/ListViewDatePicker";

import {
  getWeekDates,
  calRowsTotalTime,
  generateWeekDates,
  getDate,
  calColsTotalTime,
  calTotalTime,
} from "./timingFunctions";
import TaskIcon from "../../components/Icons/TaskIcon";
import helper from "../../helper";
import ProjectsIcon from "../../components/Icons/ProjectIcon";
import MessageIcon from "@material-ui/icons/Message";
import SvgIcon from "@material-ui/core/SvgIcon";
import {
  getTimesheetWeekDurations,
  addNewTask,
  addTaskDuration,
  submitTimesheet,
  removeTask,
  copyTimeSheet,
} from "../../redux/actions/timesheet";
import { updateTasksTimesheetStatus } from "../../redux/actions/tasks";
import { timesheetsHelpText } from "../../components/Tooltip/helptext";
import DeleteConfirmDialog from "../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import TimerIconWhite from "../../assets/images/timer_white.svg";
import { FormattedMessage } from "react-intl";
import ImportExport from "@material-ui/icons/ImportExport";
import CustomMultiSelectDropdown from "../../components/Dropdown/CustomMultiSelectWithSearchBarDropdown/Dropdown";
import { grid } from "../../components/CustomTable2/gridInstance";
import TimesheetComments from "../../components/TimesheetComments/TimesheetComments.view";
import BillableIcon from "../../components/Icons/Billable";
import NonBillableIcon from "../../components/Icons/NonBillable";
import Loader from "../../components/Loader/Loader";
import { GlobalTimeCustom } from "../../helper/config.helper";
class Timesheet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      weekDates: [],
      expanded: null,
      tooltipRights: false,
      approvalDialog: false,
      timesheetData: {},
      taskListTrigger: null,
      selectedProjectId: null,
      submitApprovalBtnQuery: "",
      deleteModal: false,
      openModal: false,
      deleteBtnQuery: "",
      copyBtnQuery: "",
      selectedProjecIdDelete: null,
      selectedTaskIdDelete: null,
      selectedWeekDateDelete: null,
      addDurationDialogState: {
        open: false,
        hours: "",
        mins: "",
        effortObj: "",
        timeStamp: "",
        project: {},
      },
      totalTimeLogged: "",
      loading: true,
    };
    this.handleInput = this.handleInput.bind(this);
    this.handleThisWeek = this.handleThisWeek.bind(this);
    this.approvalDialogClose = this.approvalDialogClose.bind(this);
    this.approvalDialogOpen = this.approvalDialogOpen.bind(this);
  }
  componentDidMount() {
    const { date } = this.props;
    if (date.toString() === moment().startOf("isoWeek").toString()) {
      this.loadWeekData("");
    } else {
      this.loadWeekData(`&currentDate=${date.format("YYYY-MM-DDTHH:mm:ss")}`)
    }
    // this.loadWeekData("");
  }
  componentDidUpdate(prevProps) {
    const { date, timesheetState } = this.props;
    if (prevProps.profileState.data.loggedInTeam !== this.props.profileState.data.loggedInTeam) {
      this.loadWeekData("");
    } else if (prevProps.date.toString() !== date.toString()) {
      if (date.toString() === moment().startOf("isoWeek").toString()) {
        this.loadWeekData("");
      }
      else {
        this.loadWeekData(`&currentDate=${date.format("YYYY-MM-DDTHH:mm:ss")}`);
      }
    } else if (
      JSON.stringify(timesheetState.timeEntry) !==
      JSON.stringify(prevProps.timesheetState.timeEntry)
    ) {
      this.setState({ timesheetData: cloneDeep(timesheetState.timeEntry) });
    }
  }
  setTaskListTrigger = elem => {
    this.setState({ taskListTrigger: elem });
  };

  handleInput(event, oldValue, projectId, taskId, day) {
    const inputRef = event.target;
    let cursorPosition = event.target.selectionStart;
    let newValue = event.target.value;
    if (oldValue === "24:00" && (cursorPosition == 4 || cursorPosition == 5)) {
      oldValue = "24:00";
      newValue = "24:0";
    }

    let inputData = maskInputValue(newValue, oldValue, cursorPosition);
    let value = inputData.value;
    cursorPosition = inputData.cursorPosition;

    let prevData = { ...this.state.timesheetData };
    const projectIndex = findIndex(prevData["projectTimesheetVM"], {
      projectId: projectId,
    });
    const taskIndex = findIndex(prevData["projectTimesheetVM"][projectIndex]["taskEffortVM"], {
      taskId: taskId,
    });
    const fieldIndex = findIndex(
      prevData["projectTimesheetVM"][projectIndex]["taskEffortVM"][taskIndex]["timestamp"],
      { date: day }
    );
    prevData["projectTimesheetVM"][projectIndex]["taskEffortVM"][taskIndex]["timestamp"][
      fieldIndex
    ].duration = value;

    this.setState({ timesheetData: prevData }, () => {
      setCaretPosition(inputRef, cursorPosition);
    });
  }
  onKeyPress = (event, projectId, taskId, day) => {
    if (event.key === "Enter") {
      const value = event.target.value;
      this.updateFieldValue(value, projectId, taskId, day);
    }
  };
  handleBlur = (projectId, taskId, day) => {
    let data = this.state.timesheetData;
    const projectIndex = findIndex(data["projectTimesheetVM"], {
      projectId: projectId,
    });
    const taskIndex = findIndex(data["projectTimesheetVM"][projectIndex]["taskEffortVM"], {
      taskId: taskId,
    });
    const fieldIndex = findIndex(
      data["projectTimesheetVM"][projectIndex]["taskEffortVM"][taskIndex]["timestamp"],
      { date: day }
    );
    const value =
      data["projectTimesheetVM"][projectIndex]["taskEffortVM"][taskIndex]["timestamp"][fieldIndex]
        .duration;
    this.updateFieldValue(value, projectId, taskId, day);
  };
  updateFieldValue = (value, projectId, taskId, day) => {
    const { timeEntry } = this.props.timesheetState;
    let prevData = { ...timeEntry };

    const projectIndex = findIndex(prevData["projectTimesheetVM"], {
      projectId: projectId,
    });
    const taskIndex = findIndex(prevData["projectTimesheetVM"][projectIndex]["taskEffortVM"], {
      taskId: taskId,
    });
    const fieldIndex = findIndex(
      prevData["projectTimesheetVM"][projectIndex]["taskEffortVM"][taskIndex]["timestamp"],
      { date: day }
    );
    const prevValue =
      prevData["projectTimesheetVM"][projectIndex]["taskEffortVM"][taskIndex]["timestamp"][
        fieldIndex
      ].duration;

    if (prevValue !== value) {
      const values = value.split(":");
      const hours = values[0] * 1;
      const mins = values[1] * 1;
      const timeStamp = moment(day).format("YYYY-MM-DDTHH:mm:ss");

      const data = {
        effortHours: hours,
        effortMinutes: mins,
        taskId,
        projectId,
        weekDate: timeStamp,
      };

      this.props.addTaskDuration(
        data,
        response => {
          // this.setState({ timesheetData: cloneDeep(this.props.timesheetState.timeEntry) });
          this.props.addUserEffort(response.data);
        },
        error => {
          if (error.status === 500) {
            // self.showSnackBar('Server throws error','error');
          }
        }
      );
    }
  };

  renderCurrentDate = (stamp, i) => {
    const date = this.props.date.clone().add(i, "days");
    const formatedDate = date.format("MM/DD/YYYY");
    if (moment(formatedDate).isSame(stamp.date)) {
      return stamp.duration ? stamp.duration : "00:00";
    }
  };
  loadWeekData = parms => {
    this.props.getTimesheetWeekDurations(parms, (error, response) => {
      if (error) {
        this.setState({
          loading: false,
        });
      } else {
        if (response) {
          this.setState({
            loading: false,
            timesheetData: cloneDeep(this.props.timesheetState.timeEntry),
          });
        }
      }
    });
  };
  handleThisWeek() {
    const { date } = this.props;
    const currentDate = moment().startOf("isoWeek");
    if (date.toString() !== currentDate.toString()) {
      this.setState({ loading: true });
      this.props.setTimesheetSelectedWeek(currentDate);
    }
  }
  handleNextWeek = () => {
    const { date } = this.props;
    const nextWeek = date.clone().add(7, "days");
    this.setState({ loading: true });
    this.props.setTimesheetSelectedWeek(nextWeek);
  };
  handlePrevWeek = () => {
    const { date } = this.props;
    const prevWeek = date.clone().subtract(7, "days");
    this.setState({ loading: true });
    this.props.setTimesheetSelectedWeek(prevWeek);
  };
  handleSelectDate = (date) => {
    if (date) {
      const startDate = moment(date).startOf('isoWeek');
      this.setState({ loading: true });
      this.props.setTimesheetSelectedWeek(startDate);
    }
  }
  approvalDialogOpen(event, selectedProjectId, totalTime) {
    event.stopPropagation();
    this.setState({ approvalDialog: true, selectedProjectId, totalTimeLogged: totalTime });
  }
  approvalDialogClose() {
    this.setState({ approvalDialog: false });
  }

  handleSubmitForApproval = () => {
    const { selectedProjectId, timesheetData, totalTimeLogged } = this.state;
    const { date } = this.props;
    let data = {
      projectId: selectedProjectId,
      other: selectedProjectId ? false : true,
      startDate: date.format("YYYY-MM-DDTHH:mm:ss"),
      endDate: date
        .clone()
        .add(6, "days")
        .format("YYYY-MM-DDTHH:mm:ss"),
      weeklyTime: totalTimeLogged,
    };
    if (!selectedProjectId) {
      let otherTasks = [];
      const projects = timesheetData["projectTimesheetVM"] || [];
      const projectIndex = findIndex(projects, {
        projectId: selectedProjectId,
      });
      const tasks = projects[projectIndex]["taskEffortVM"] || [];
      tasks.map(obj => {
        otherTasks.push(obj.taskId);
      });
      data.OtherTaskIds = otherTasks;
    }
    this.setState({ submitApprovalBtnQuery: "progress" });
    this.props.submitTimesheet(
      data,
      response => {
        this.setState({
          submitApprovalBtnQuery: "",
          timesheetData: cloneDeep(this.props.timesheetState.timeEntry),
          approvalDialog: false,
        });
      },
      error => {
        this.setState({
          submitApprovalBtnQuery: "",
          approvalDialog: false,
        });
        const err = error.data && error.data.message;
        const message =
          err || "Oops! There seems to be an issue, please contact support@ntaskmanager.com";
        this.props.showSnackBar(message, "error");
      }
    );
    this.props.getTimesheetWeekDurations(
      `&currentDate=${this.props.date.format("YYYY-MM-DDTHH:mm:ss")}`,
      (error, response) => {
        if (error) {
        } else {
          if (response) {
            //this.setState({ timesheetData: cloneDeep(this.props.timesheetState.timeEntry) })
          }
        }
      }
    );
  };

  handleAddTask = (taskId, projectId) => {
    const { date } = this.props;

    const data = {
      effortHours: 0,
      effortMinutes: 0,
      taskId: taskId,
      projectId: projectId,
      date: date.format("YYYY-MM-DDTHH:mm:ss"),
      weekStartDate: date.format("YYYY-MM-DDTHH:mm:ss"),
    };
    const suppressInReviewProjectTask = this.props.timesheetState.timeEntry.projectTimesheetVM.some(
      t => t.projectId == projectId && t.status == "In Review"
    );
    const suppressInReviewUnSortedTask = this.props.timesheetState.timeEntry.projectTimesheetVM.some(
      t => !t.projectId && t.status == "In Review"
    );
    const suppressApprovedProjectTask = this.props.timesheetState.timeEntry.projectTimesheetVM.some(
      t => t.projectId == projectId && t.status == "Approved"
    );
    const suppressApprovedUnSortedTask = this.props.timesheetState.timeEntry.projectTimesheetVM.some(
      t => !t.projectId && t.status == "In Review"
    );

    if (suppressInReviewProjectTask) {
      this.props.showSnackBar(
        "Oops, cannot add task! Timesheet is already in review for current week for this project.",
        "error"
      );
      return;
    }
    if (suppressInReviewUnSortedTask && !projectId) {
      this.props.showSnackBar(
        "Oops, cannot add task! Timesheet is already in review for current week for tasks not assigned to projects.",
        "error"
      );
      return;
    }
    if (suppressApprovedProjectTask) {
      this.props.showSnackBar(
        "Oops, cannot add task! Timesheet is already approved for current week for this project.",
        "error"
      );
      return;
    }
    if (suppressApprovedUnSortedTask && !projectId) {
      this.props.showSnackBar(
        "Oops, cannot add task! Timesheet is already approved for current week for tasks not assigned to projects.",
        "error"
      );
      return;
    }

    this.props.addNewTask(data, (error, response) => {
      if (error) {
      } else {
        if (response) {
          const resData = response.data;
          // Approved -> In Progress
          if (resData.status) {
            const statusProjectId = resData.status.projectId;
            const timesheetData = this.props.timesheetState.timeEntry;
            let taskIds = [];
            const projects = timesheetData["projectTimesheetVM"] || [];
            const projectIndex = findIndex(projects, {
              projectId: statusProjectId,
            });
            const tasks = projects[projectIndex]["taskEffortVM"] || [];
            tasks.map(obj => {
              taskIds.push(obj.taskId);
            });
            // this.props.updateTasksTimesheetStatus(taskIds, "In Progress");
          }
          this.setState({
            timesheetData: cloneDeep(this.props.timesheetState.timeEntry),
            approvalDialog: false,
          });
        }
      }
    },
      err => {
        this.props.showSnackBar(
          err.data.message,
          "error"
        );
      }
    );
  };
  getAddedTasks = () => {
    const { timesheetData } = this.state;
    const projects = timesheetData["projectTimesheetVM"] || [];
    let timesheetTasks = [];
    projects.map(project => {
      const tasks = project["taskEffortVM"] || [];
      tasks.map(task => {
        timesheetTasks.push({
          projectId: project["projectId"],
          taskId: task["taskId"],
        });
      });
    });
    return timesheetTasks;
  };
  openTaskListMenu = () => {
    this.state.taskListTrigger.click();
  };
  //Function that open task duration popup
  handleAddTaskDuration = (project, effortObj, timeStamp) => {
    this.setState({
      addDurationDialogState: {
        open: true,
        effortObj,
        timeStamp,
        project,
        workspaceConfig : GlobalTimeCustom(project.workspaceId),
      },
    });
  };
  //Function that open task duration popup
  closeAddTaskDuration = () => {
    this.setState({
      addDurationDialogState: {
        open: false,
        effortObj: {},
        timeStamp: {},
        project: {},
        workspaceConfig: {},
      },
    });
  };
  removeTaskFromSheet = (projectId, taskId, weekDate) => {
    /** function calls when user clicks on close Icon */
    this.setState({
      deleteModal: true,
      openModal: true,
      selectedProjecIdDelete: projectId,
      selectedTaskIdDelete: taskId,
      selectedWeekDateDelete: weekDate,
    });
  };

  handleDialogClose = e => {
    /** function that closes the modal */
    if (e) e.stopPropagation();
    this.setState({
      openModal: false,
      deleteModal: false,
    });
  };

  handleDelete = e => {
    /** function when click on delete button in modal */
    e.stopPropagation();
    const { selectedProjecIdDelete, selectedTaskIdDelete, selectedWeekDateDelete } = this.state;
    this.setState({ openModal: false, deleteModal: false });
    this.props.removeTask(
      selectedProjecIdDelete,
      selectedTaskIdDelete,
      selectedWeekDateDelete.startOf("week").format("MM/DD/YYYY HH:mm:ss a"),
      () => {
        this.loadWeekData(`&currentDate=${this.props.date.format("YYYY-MM-DDTHH:mm:ss")}`);
      },
      err => {
        this.props.showSnackBar(
          err.data.message,
          "error"
        );
      }
    );
  };
  showTranslatedStatus = status => {
    let intl = { id: "", message: "" };
    switch (status) {
      case "In Progress":
        intl.id = "task.common.status.dropdown.in-progress";
        intl.message = "In Progress";
        break;
      case "In Review":
        intl.id = "task.common.status.dropdown.in-review";
        intl.message = "In Review";
        break;
      case "Rejected":
        intl.id = "timesheet.status.reject";
        intl.message = "Rejected";
        break;
      case "Approved":
        intl.id = "timesheet.status.approve";
        intl.message = "Approved";
        break;
      case "In Progress - Withdrawn Approval":
        intl.id = "timesheet.status.withDraw";
        intl.message = "In Progress - Withdrawn Approval";
        break;
      default:
        intl.id = "";
        intl.message = status;
        break;
    }
    return <FormattedMessage id={intl.id} defaultMessage={intl.message} />;
  };
  generateWeekAggregation = () => {
    //Generating array of all time entries of all tasks in the timesheet
    const allWeeksData = this.props.timesheetState.timeEntry["projectTimesheetVM"].reduce(
      (r, cv) => {
        cv.taskEffortVM.forEach(x => {
          r = [...r, ...x.timestamp];
        });
        return r;
      },
      []
    );
    //Grouping all time entries by id
    const allWeekDurations = allWeeksData.reduce((r, cv) => {
      const selectedTimestamp = allWeeksData.filter(t => t.id == cv.id);
      r[cv.id] = selectedTimestamp.map(t => (t.duration ? t.duration : "00:00"));
      return r;
    }, {});
    //Generating keys from above grouped data
    const weekDayKeys = Object.keys(allWeekDurations);

    //Summing up all the data
    const summedUpDurations = weekDayKeys.reduce((r, cv, i) => {
      r[i] = calTotalTime(allWeekDurations[cv]);
      return r;
    }, []);
    return summedUpDurations;
  };
  CopyTimeSheet = (project, index) => {
    this.setState({ copyBtnQuery: index });
    copyTimeSheet({
      copyFrom: this.props.date.format("YYYY-MM-DDTHH:mm:ss"),
      copyTo: moment().startOf("isoWeek").format("YYYY-MM-DDTHH:mm:ss"),
      taskIds: project["taskEffortVM"].map(task => task.taskId),
      ...(project.projectId && { projectId: project.projectId, })
    },
      (response) => {
        this.setState({ copyBtnQuery: '' });
        this.props.showSnackBar(response.message ?? "Operation completed successfully", "success");
      },
      (error) => {
        this.setState({ copyBtnQuery: '' });
        const message = error.data?.message ?? "Oops! There seems to be an issue, please contact support@ntaskmanager.com";
        this.props.showSnackBar(message, "error")
      }
    );
  }

  render() {
    const { classes, theme, date, timesheetPer } = this.props;
    const {
      expanded,
      loading,
      approvalDialog,
      timesheetData,
      submitApprovalBtnQuery,
      openModal,
      deleteBtnQuery,
      copyBtnQuery,
      deleteModal,
      addDurationDialogState,
    } = this.state;
    const addedTasks = this.getAddedTasks();
    let projects = [];
    if (addedTasks.length) projects = timesheetData["projectTimesheetVM"] || [];

    // Sort data
    projects = sortBy(projects, [
      function (o) {
        if (!o.projectName) return "";
        else return o.projectName;
      },
    ]);
    const weekDays = getWeekDates(date);
    const allProjectsData = this.generateWeekAggregation();
    const aggregatedSum = calTotalTime(allProjectsData);
    return (
      <>
        {loading ?
          <Loader />
          :
          <div>
            {/* Next Prev Buttons Starts */}
            <div className="flex_center_space_between_row mb20">
              <div className={classes.NextBackBtnCnt}>
                <CustomIconButton
                  btnType="condensed"
                  style={{ padding: 0 }}
                  onClick={this.handlePrevWeek}>
                  <LeftArrow
                    htmlColor={theme.palette.secondary.dark}
                    classes={{ root: classes.navigationArrow }}
                  />
                </CustomIconButton>
                <div className={classes.selectDate}>
                  <span className={classes.toolbarCurrentDate}>{getDate(date)}</span>
                  <CustomDatePicker
                    dateFormat="MMM DD, YYYY"
                    timeInput={false}
                    datePickerProps={{
                    }}
                    onSelect={(date) => {
                      this.handleSelectDate(date)
                    }}
                    filterDate={moment()}
                    btnProps={{ className: classes.datePickerCustomStyle }}
                    style={{
                      display: "flex",
                      alignItems: "center",
                      opacity: 0,
                      position: 'absolute',
                      top: 0,
                      left: 0,
                      width: '100%',
                      height: '100%',
                      zIndex: 2,
                    }}
                    PopperProps={{ disablePortal: false }}
                    showDates={false}
                    placeholder={"Select Date"}
                    icon={true}
                  />
                </div>
                <CustomIconButton
                  btnType="condensed"
                  style={{ padding: 0, marginRight: 20 }}
                  onClick={this.handleNextWeek}>
                  <RightArrow
                    htmlColor={theme.palette.secondary.dark}
                    classes={{ root: classes.navigationArrow }}
                  />
                </CustomIconButton>

                <TaskListMenu
                  filteredList={addedTasks}
                  setTaskListTrigger={this.setTaskListTrigger}
                  handleItemSelect={this.handleAddTask}
                  label="Task"
                  keyType="task"
                  disabled={!timesheetPer.addTask.cando}
                />
              </div>
              {!date.isSame(moment().startOf("isoWeek")) ? (
                <CustomButton onClick={this.handleThisWeek} btnType="graydashed" variant="contained">
                  <FormattedMessage id="timesheet.this-week.label" defaultMessage="This Week" />
                </CustomButton>
              ) : null}
            </div>
            {/* Next Prev Buttton Ends */}
            {addedTasks.length ? null : (
              <div className={classes.emptyStateCnt}>
                <EmptyState
                  screenType="timesheet"
                  heading={
                    <FormattedMessage
                      id="timesheet.no-time.label"
                      defaultMessage="No Time Logged Yet"
                    />
                  }
                  message={
                    <FormattedMessage
                      id="timesheet.no-time.message"
                      defaultMessage="This is your weekly timesheet view. All your time entries made  in the tasks will be shown here."
                    />
                  }
                  button={false}
                />
              </div>
            )}
            {projects && projects.length ? (
              <div className={classes.timeAggregationCnt}>
                {/* timehseet */}
                <p className={classes.footerTotalLabel}>Timesheets Total</p>
                <div className={classes.weekDatesCnt}>
                  {allProjectsData.map((d, i) => {
                    const weekDaysCmp = generateWeekDates(weekDays, classes, false);
                    return (
                      <div className={this.props.classes.footerTotalCell}>
                        {weekDaysCmp[i]}
                        {helper.standardTimeFormat(d)}
                      </div>
                    );
                  })}
                  <div className={classes.footerTotalCell} style={{ paddingRight: 0 }}>
                    {helper.standardTimeFormat(aggregatedSum)}
                  </div>
                </div>
              </div>
            ) : null}
            <div className={classes.timeSheetCnt} style={{ height: calculateContentHeight() - 90 }}>
              {addDurationDialogState.open && (
                <AddDurationDialog
                  loadTimeSheet={this.loadWeekData}
                  timeEntryProps={addDurationDialogState}
                  closeAction={this.closeAddTaskDuration}
                  date={date}
                  timesheetPer={timesheetPer}
                  globalTimeIncrement={addDurationDialogState.workspaceConfig}
                />
              )}
              <ActionConfirmation
                open={approvalDialog}
                closeAction={this.approvalDialogClose}
                cancelBtnText={
                  <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
                }
                successBtnText={
                  <FormattedMessage
                    id="timesheet.submit-for-approval-confirmation.confirm-button.label"
                    defaultMessage="Confirm"
                  />
                }
                alignment="center"
                iconType="timerIcon"
                headingText={
                  <FormattedMessage
                    id="timesheet.submit-for-approval-confirmation.title"
                    defaultMessage="Submit For Approval"
                  />
                }
                msgText={
                  <FormattedMessage
                    id="timesheet.submit-for-approval-confirmation.label"
                    defaultMessage="Are you sure you want to submit this timesheet for approval?"
                  />
                }
                note={
                  <FormattedMessage
                    id="timesheet.submit-for-approval-confirmation.note"
                    defaultMessage="Timesheet will be sent to workspace owners, admins and project managers for approval."
                  />
                }
                successAction={this.handleSubmitForApproval}
                btnQuery={submitApprovalBtnQuery}
              />

              {projects.map((project, index) => {
                const data = project["taskEffortVM"] || [];
                const rowsTotalTime = calRowsTotalTime(weekDays, data);
                const colsTotalTime = calColsTotalTime(weekDays, data);
                const totalTime = calTotalTime(rowsTotalTime);
                const statusClass =
                  project["status"] == "In Review"
                    ? classes.inReview
                    : project["status"] == "Rejected"
                      ? classes.rejected
                      : project["status"] == "Approved"
                        ? classes.approved
                        : project["status"] == "In Progress - Withdrawn Approval"
                          ? classes.withdraw
                          : classes.inProgress;
                return (
                  <ExpansionPanel
                    defaultExpanded={true}
                    // expanded={expanded === `${index}`}
                    // onChange={this.handleChange(`${index}`)}
                    classes={{
                      root: classes.accordPanelRoot,
                      expanded: classes.accordPanelRootExpand,
                    }}
                    key={index}>
                    <ExpansionPanelSummary
                      classes={{
                        root: classes.accordSummaryPanel,
                        content: classes.accordSummaryPanelContent,
                        expanded: classes.accordSummaryPanelExpand,
                      }}
                      expandIcon={<ExpandMoreIcon />}>
                      <div className={classes.accordSummaryInnerCnt}>
                        <div className="flex_center_start_row">
                          {!project["projectName"] ? (
                            <SvgIcon viewBox="0 0 18 15.188" className={classes.smallBtnIcon}>
                              <TaskIcon />
                            </SvgIcon>
                          ) : (
                            <SvgIcon viewBox="0 0 18 15.188" className={classes.smallBtnIcon}>
                              <ProjectsIcon />
                            </SvgIcon>
                          )}
                          <Typography variant="h5" className={classes.accorheading}>
                            <b>Project Name: </b>
                            {!project["projectName"] ? (
                              <FormattedMessage
                                id="timesheet.not-assigned.label"
                                defaultMessage="Not assigned to projects"
                              />
                            ) : (
                              project["projectName"]
                            )}
                          </Typography>
                          {!project["projectName"] ? (
                            <CustomTooltip
                              helptext={
                                <FormattedMessage
                                  id="timesheet.not-assigned.hint"
                                  defaultMessage={timesheetsHelpText.otherTasksHelpText}
                                />
                              }
                              iconType="help"
                              position="static"
                            />
                          ) : null}
                          <span className={`${classes.statusTag} ${statusClass}`}>
                            {this.showTranslatedStatus(project["status"])}
                          </span>
                          <TimesheetComments comments={project["rejectReason"]} />
                        </div>
                        <div className="flex_center_start_row">
                          {!moment(this.props.date).isSame(new Date(), 'week') && (
                            <CustomButton
                              btnType="gray"
                              style={{
                                marginRight: 10
                              }}
                              onClick={(event) => {
                                event.stopPropagation();
                                this.CopyTimeSheet(project, index.toString())
                              }}
                              query={copyBtnQuery == index.toString() ? 'progress' : ''}
                              disabled={copyBtnQuery == index.toString() ? true : false}
                              variant="contained"
                            >
                              Copy Timesheet
                            </CustomButton>
                          )}
                          {project["status"] == "In Progress" ? (
                            <CustomButton
                              btnType="gray"
                              variant="contained"
                              onClick={e =>
                                this.approvalDialogOpen(
                                  e,
                                  project["projectId"],
                                  helper.standardTimeFormat(totalTime)
                                )
                              }
                              disabled={!timesheetPer.submitTimesheet.cando}>
                              <FormattedMessage
                                id="timesheet.submit-approval-button.label"
                                defaultMessage="Submit for Approval"
                              />
                            </CustomButton>
                          ) : null}
                          {project["status"] == "Rejected" ||
                            project["status"] === "In Progress - Withdrawn Approval" ? (
                            <CustomButton
                              btnType="gray"
                              variant="contained"
                              onClick={e =>
                                this.approvalDialogOpen(
                                  e,
                                  project["projectId"],
                                  helper.standardTimeFormat(totalTime)
                                )
                              }
                              disabled={!timesheetPer.resubmitTimeSheet.cando}>
                              <FormattedMessage
                                id="timesheet.resubmit-for-approval.label"
                                defaultMessage="Resubmit for Approval"
                              />
                            </CustomButton>
                          ) : null}

                          <div className={`flex_center_center_col ${classes.timeLoggedCnt}`}>
                            <Typography variant="caption">
                              {
                                <FormattedMessage
                                  id="timesheet.time-logged.label"
                                  defaultMessage="Time Logged"
                                />
                              }
                            </Typography>
                            <p>
                              {helper.standardTimeFormat(totalTime)}
                              <span>
                                {` `}
                                <FormattedMessage id="timesheet.hours.label" defaultMessage="hours" />
                              </span>
                            </p>
                          </div>
                        </div>
                      </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails classes={{ root: classes.expansionDetailsCnt }}>
                      {/* Time Sheet Header Starts */}
                      <div className={classes.timesheetHeaderCnt}>
                        <div className={classes.addTaskBtnCnt}>Task Title</div>
                        <div className={classes.weekDatesCnt}>
                          {generateWeekDates(weekDays, classes)}
                          <div className={classes.headerTotalCell} style={{ paddingRight: 0 }}>
                            <FormattedMessage id="common.total.label" defaultMessage="Total" />
                          </div>
                        </div>
                      </div>
                      {/* Time Sheet Header Ends */}
                      {data.map((ele, i) => {
                        return (
                          <div
                            key={ele.taskId}
                            className={classes.weekDatesRow}
                            style={{ borderBottom: i == 0 ? "none" : "" }}>
                            <p className={classes.taskTitle}>
                              {ele.isBillable ? <CustomTooltip
                                helptext={
                                  'Billable Task'
                                }
                                placement="top"
                                style={{ color: theme.palette.common.white }}>
                                <SvgIcon
                                  viewBox="0 0 17.99 17.99"
                                  className={classes.billableIcon}>
                                  <BillableIcon />
                                </SvgIcon>
                              </CustomTooltip> :
                                <CustomTooltip
                                  helptext={
                                    'Non-Billable Task'
                                  }
                                  placement="top"
                                  style={{ color: theme.palette.common.white }}>
                                  <SvgIcon
                                    viewBox="0 0 18.39 18.39"
                                    className={classes.nonBillableIcon}>
                                    <NonBillableIcon />
                                  </SvgIcon>
                                </CustomTooltip>}
                                <div>{ele.taskTitle}</div>
                            </p>
                            <div className={classes.weekDatesInputCnt}>
                              {ele.timestamp.map((stamp, index) => {
                                let val = this.renderCurrentDate(stamp, index);
                                const renderUI = (
                                  <div key={stamp.id} className={classes.weekDatesCell}>
                                    {stamp.notes && <div className={classes.triangleTopRight}></div>}
                                    <span className={classes.addBtnCnt}>
                                      <CustomIconButton
                                        btnType="success"
                                        onClick={event =>
                                          this.handleAddTaskDuration(project, ele, stamp)
                                        }
                                        style={{
                                          borderRadius: "4px 0 0 4px",
                                        }}>
                                        <img src={TimerIconWhite} className={classes.whiteTimerIcon} />
                                      </CustomIconButton>
                                    </span>
                                    <DefaultTextField
                                      label={false}
                                      error={false}
                                      formControlStyles={{
                                        marginTop: 0,
                                        marginBottom: 0,
                                      }}
                                      fullWidth={false}
                                      defaultProps={{
                                        id: `${stamp.id}`,
                                        onClick: event =>
                                          this.handleAddTaskDuration(project, ele, stamp),
                                        // disabled:
                                        //   project["status"] == "Approved"
                                        // ? true
                                        //     : false,
                                        readOnly: true,
                                        inputProps: {
                                          style: {
                                            padding: "8px 14px",
                                            textAlign: "center",
                                            fontSize: 12,
                                            transition: "0.15s padding ease",
                                            cursor: "pointer",
                                          },
                                        },
                                        // onKeyDown: event =>
                                        //   this.onKeyPress(
                                        //     event,
                                        //     project["projectId"],
                                        //     ele.taskId,
                                        //     stamp["date"]
                                        //   ),
                                        // onBlur: () =>
                                        //   this.handleBlur(
                                        //     project["projectId"],
                                        //     ele.taskId,
                                        //     stamp["date"]
                                        //   ),
                                        // onChange: event =>
                                        //   this.handleInput(
                                        //     event,
                                        //     val,
                                        //     project["projectId"],
                                        //     ele.taskId,
                                        //     stamp["date"]
                                        //   ),
                                        value: val == "00:00" ? "-" : val,
                                      }}
                                    />
                                  </div>
                                );
                                return stamp.notes ? (
                                  <CustomTooltip
                                    helptext={
                                      <div>
                                        <span className={classes.notesLabel}>Notes:</span>
                                        <div className={classes.notes}>{stamp.notes}</div>
                                      </div>
                                    }
                                    iconType="help"
                                    position="static"
                                    placement="right"
                                    hideArrow={true}
                                    classes={classes}>
                                    {renderUI}
                                  </CustomTooltip>
                                ) : (
                                  renderUI
                                );
                              })}
                              <div className={classes.totalTaskTime}>
                                {helper.standardTimeFormat(rowsTotalTime[i])}
                              </div>
                              {project["status"] !== "Approved" &&
                                project["status"] !== "In Review" &&
                                timesheetPer.deleteTimesheet.cando ? (
                                <IconButton
                                  btnType="smallFilledGray"
                                  style={{
                                    padding: 2,
                                    position: "absolute",
                                    right: 0,
                                    background: "#de133e",
                                  }}
                                  onClick={() =>
                                    this.removeTaskFromSheet(project["projectId"], ele.taskId, date)
                                  }>
                                  <CloseIcon
                                    className={classes.closeIcon}
                                    htmlColor={theme.palette.common.white}
                                  />
                                </IconButton>
                              ) : null}
                            </div>
                          </div>
                        );
                      })}
                      <div className={classes.timesheetFooterCnt}>
                        {/* timehseet */}
                        <p className={classes.footerTotalLabel}>
                          <FormattedMessage id="common.total.label" defaultMessage="Total" />
                        </p>
                        <div className={classes.weekDatesCnt}>
                          {colsTotalTime.map((time, i) => {
                            return (
                              <div key={i} className={this.props.classes.footerTotalCell}>
                                {helper.standardTimeFormat(time)}
                              </div>
                            );
                          })}
                          <div className={classes.footerTotalCell} style={{ paddingRight: 0 }}>
                            {helper.standardTimeFormat(totalTime)}
                          </div>
                        </div>
                      </div>
                    </ExpansionPanelDetails>
                  </ExpansionPanel>
                );
              })}
              {/*{projects && projects.length ? <div className={classes.timeAggregationCnt}>*/}
              {/*  /!* timehseet *!/*/}
              {/*  <p className={classes.footerTotalLabel}>Timesheets Total</p>*/}
              {/*  <div className={classes.weekDatesCnt}>*/}
              {/*    {allProjectsData.map(d => (*/}
              {/*    <div*/}
              {/*      className={this.props.classes.footerTotalCell}*/}
              {/*    >*/}
              {/*      {helper.standardTimeFormat(d)}*/}
              {/*    </div>*/}
              {/*    ))}*/}
              {/*    <div*/}
              {/*      className={classes.footerTotalCell}*/}
              {/*      style={{ paddingRight: 0 }}*/}
              {/*    >*/}
              {/*      {helper.standardTimeFormat(aggregatedSum)}*/}
              {/*    </div>*/}
              {/*</div>*/}
              {/*</div> : null}*/}

              <React.Fragment>
                {deleteModal /** delete modal */ ? (
                  <DeleteConfirmDialog
                    open={openModal}
                    closeAction={this.handleDialogClose}
                    cancelBtnText={
                      <FormattedMessage
                        id="common.action.delete.confirmation.cancel-button.label"
                        defaultMessage="Cancel"
                      />
                    }
                    successBtnText={
                      <FormattedMessage
                        id="common.action.delete.confirmation.delete-button.label"
                        defaultMessage="Delete"
                      />
                    }
                    alignment="center"
                    headingText={
                      <FormattedMessage
                        id="timesheet.delete-confirmation.title"
                        defaultMessage="Delete"
                      />
                    }
                    successAction={this.handleDelete}
                    msgText={
                      <FormattedMessage
                        id="timesheet.delete-confirmation.label"
                        defaultMessage="Are you sure you want to delete this timesheet?"
                      />
                    }
                    btnQuery={deleteBtnQuery}
                  />
                ) : null}
              </React.Fragment>
            </div>
          </div>
        }
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    timesheetState: state.timesheet,
    profileState: state.profile,
  };
};

export default compose(
  withRouter,
  withStyles(timesheetStyles, { withTheme: true }),
  connect(mapStateToProps, {
    getTimesheetWeekDurations,
    addUserEffort,
    addNewTask,
    addTaskDuration,
    submitTimesheet,
    updateTasksTimesheetStatus,
    removeTask,
  })
)(Timesheet);
