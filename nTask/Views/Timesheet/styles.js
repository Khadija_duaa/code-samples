const timesheetStyles = theme => ({
  expandIcon: {
   padding: "12px 12px 12px 0px"
  },
  NextBackBtnCnt: {
    display: "flex",
    alignItems: "center"
  },
  timeSheetCnt: {
    overflowY: "auto",
    padding: '0 30px 70px 0',
    display: "flex",
    flexDirection: "column",
    '& *': {
      fontFamily: theme.typography.fontFamilyLato
    }
  },
  emptyStateCnt: {
    flex: 1,
    display: "flex"
  },
  closeIcon: {
    fontSize: "12px !important"
  },
  accordPanelRoot: {
    boxShadow: "none",
    margin: "5px 0 !important",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    "&:before": {
      background: "transparent"
    }
  },
  expansionDetailsCnt: {
    display: "block",
    padding: 0
  },
  timesheetHeaderCnt: {
    display: "flex",
    background: theme.palette.background.light,
    padding: "0 20px",
    borderTop: `1px solid ${theme.palette.border.lightBorder}`,
    borderBottom: "none"
  },
  timesheetFooterCnt: {
    display: "flex",
    background: theme.palette.background.light,
    borderTop: `1px solid ${theme.palette.border.lightBorder}`,

    padding: "0 20px"
    // border: `1px solid ${theme.palette.border.lightBorder}`,
    // borderTop: "none"
  },
  timeAggregationCnt: {
    display: "flex",
    alignItems: 'center',
    background: theme.palette.background.darkBlack,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    padding: "1px 20px",
    // position: 'fixed',
    margin: '8px 48px 3px 0',
    borderRadius: 6,
    bottom: 17,
    // boxShadow: 'rgb(0 0 0 / 20%) 0px 2px 4px -1px, rgb(0 0 0 / 14%) 0px 4px 5px 0px, rgb(0 0 0 / 12%) 0px 1px 10px 0px',
    // width: 'calc(100vw - 290px)',
    '& *': {
      color: `${theme.palette.common.white} !important`,
      fontFamily: theme.typography.fontFamilyLato,
      fontSize: "13px !important"
    },
    '& $headerDate': {
      marginBottom: 3
    },
    '& $weekDatesCnt $footerTotalCell:last-child': {
      alignItems: 'flex-start'
    }
  },
  footerTotalCell: {
    fontSize: "11px !important",
    marginRight: 0,
    justifyContent: "center",
    padding: "12px 0",
    color: theme.palette.text.primary
  },
  weekDatesCnt: {
    display: "flex",
    flex: 1.7,
    justifyContent: "center",
    "& > div": {
      flex: 1,
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      padding: "10px 10px"
    }
  },
  addTaskBtnCnt: {
    fontSize: "16px",
    flex: 1,
    padding: "10px 0"
  },
  footerTotalLabel: {
    flex: 1,
    padding: 0,
    fontSize: "14px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightRegular
  },
  taskTitle: {
    flex: 1,
    margin: 0,
    fontSize: "12px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightRegular,
    display: 'flex',
    alignItems: 'center',
    maxWidth: '37%',
    '& div': {
      width: '100%',
      overflow: "hidden",
      textOverflow: "ellipsis"
    }
  },
  billableIcon: {
    fontSize: "16px !important",
    marginRight: 8,
    color: theme.palette.icon.brightBlue
  },
  nonBillableIcon: {
    fontSize: "16px !important",
    marginRight: 8,
    color: theme.palette.icon.gray600
  },
  weekDatesRow: {
    display: "flex",
    alignItems: "center",
    padding: "10px 20px",
    borderTop: `1px solid ${theme.palette.border.lightBorder}`,
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    background: theme.palette.common.white
  },
  backArrowIcon: {
    fontSize: "28px !important",
    marginRight: 10,
    cursor: "pointer"
  },
  weekDatesInputCnt: {
    display: "flex",
    flex: 1.7,
    justifyContent: "center",
    fontSize: "11px !important",
    alignItems: "center",
    position: "relative"
  },
  weekDatesCellPending: {
    position: "relative",
    flex: 1,
    margin: "0 10px",
    textAlign: "center",
    minHeight: 32,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    border: "1px solid #dddddd",
    borderRadius: 4,
    cursor: "pointer",
    "&:hover $addBtnCnt": {
      "& button": {
        width: 28,
        padding: 4,
        transition: "0.15s all ease"
      }
    },
    "&:hover input": {
      paddingLeft: "40px !important"
    }
  },
  weekDatesCell: {
    margin: "0 10px",
    position: "relative",
    flex: 1,
    textAlign: "center",

    "&:hover $addBtnCnt": {
      "& button": {
        width: 28,
        padding: 4,
        transition: "0.15s all ease"
      }
    },
    "&:hover input": {
      paddingLeft: "40px !important"
    }
  },
  headerDay: {
    fontSize: "12px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightRegular
  },
  headerDate: {
    fontSize: "11px !important",
    color: theme.palette.text.secondary
  },
  totalTaskTime: {
    flex: 1,
    padding: "10px 0 10px 10px",
    fontSize: "11px !important",
    color: theme.palette.text.primary,
    textAlign: "center"
  },
  headerTotalCell: {
    fontSize: "12px !important",
    marginRight: 0,
    justifyContent: "center",
    padding: "12px 0",
    color: theme.palette.text.primary
  },
  taskIcon: {
    marginRight: 10
  },
  accordSummaryPanel: {
    border: "none",
    display: "flex",
    background: theme.palette.common.white
  },
  accordPanelRootExpand: {
      margin: "0 0 10px 0",
  },
  accordSummaryPanelPending: {
    border: "none",
    display: "flex",
    background: theme.palette.common.white,
    padding: "0 24px 0 14px",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    marginBottom: 5
  },
  accordSummaryPanelContent: {
    margin: 0
  },
  accordSummaryPanelExpand: {
    minHeight: '48px !important',
    margin: '0px !important'
  },
  accordSummaryInnerCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    flex: 1
  },
  statusTag: {
    padding: "4px 7px !important",
    border: "1px solid",
    fontSize: "11px !important",
    textAlign: "center",
    marginLeft: 15
  },
  inProgress: {
    color: theme.palette.status.inProgress,
    border: `1px solid ${theme.palette.status.inProgress}`
  },
  withdraw: {
    color: "#cc7722",
    border: `1px solid #cc7722`
  },
  inReview: {
    color: theme.palette.status.inReview,
    border: `1px solid ${theme.palette.status.inReview}`
  },
  approved: {
    color: theme.palette.status.completed,
    border: `1px solid ${theme.palette.status.completed}`
  },
  rejected: {
    color: theme.palette.status.rejected,
    border: `1px solid ${theme.palette.status.rejected}`
  },
  helpIcon: {
    fontSize: "16px !important",
    marginLeft: 5
  },
  smallBtnIcon: {
    fontSize: "20px !important",
    color: theme.palette.secondary.light,
    marginRight: 10
  },
  timeLoggedCnt: {
    padding: "0 20px",
    borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
    borderRight: `1px solid ${theme.palette.border.lightBorder}`,
    marginLeft: 20,
    "& p": {
      margin: 0,
      fontSize: "11px !important",
      "& span": {
        fontSize: "12px !important"
      }
    }
  },
  lockIcon: {
    fontSize: "18px !important",
    marginRight: 5
  },
  singleComment: {
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    paddingBottom: 20,
    marginBottom: 20
  },
  commentsHeader: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 5
  },
  messageCnt: {
    background: theme.palette.background.items,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    padding: 10
  },
  commentsIcon: {
    fontSize: "16px !important",
    marginRight: 5
  },
  approvalTimerIconCnt: {
    border: `2px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "50%",
    width: 110,
    height: 110,
    paddingLeft: 22,
    paddingTop: 22,
    marginBottom: 30,
    background: theme.palette.background.paper
  },
  approvalTimerIcon: {
    fontSize: "64px !important"
  },
  timeSheetDashboardHeader: {
    margin: "0 0 30px 0",
    paddingRight: 30
  },

  timeSheetHeadingDimmed: {
    marginRight: 20,
    color: "#cbcbcb"
  },
  listViewHeading: {
    marginRight: 5,
    fontSize: "18px !important",
    color : "#333333",
    fontWeight: 700,
    fontFamily: theme.typography.fontFamilyLato
},
  toggleContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
    // marginLeft: 20
  },
  toggleBtnGroup: {
    borderRadius: 4,
    background: theme.palette.common.white,
    // border: `1px solid ${theme.palette.border.lightBorder}`,
    boxShadow: "none",
    "& $toggleButtonSelected": {
      boxShadow: "none",
      color: theme.palette.text.secondary,
      backgroundColor: theme.palette.common.white,
      "&:after": {
        background: theme.palette.common.white
      },
      "&:hover": {
        background: theme.palette.common.white
      }
    }
  },
  groupedHorizontal:{
    borderLeft: `1px solid ${theme.palette.border.lightBorder} !important`
  },
  toggleButton: {
    height: "auto",
    padding: "4px 20px 5px",
    fontSize: "12px !important",
    fontWeight: theme.palette.fontWeightMedium,
    textTransform: "capitalize",
    "&:hover": {
      background: theme.palette.common.white
    },
    "&[value = 'center']": {
      borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
      borderRight: `1px solid ${theme.palette.border.lightBorder}`
    }
  },
  toggleButtonSelected: {},
  timeSheetDashboard: {
    height: '100%',
    position: 'relative',
    padding: "20px 0 0 60px"
  },
  avatar: {
    width: 32,
    height: 32,
    marginRight: 5
  },
  accorheading: {
    fontWeight: theme.typography.fontWeightLight,
    marginLeft: 10
  },
  dialogContentCnt: {
    padding: "20px 20px 20px 20px"
  },
  addBtnCnt: {
    position: "absolute",
    left: 0,
    zIndex: 1,
    "& button": {
      width: 0,
      padding: "5px 0",
      overflow: 'hidden',
      transition: "0.15s all ease"
    }
  },
  timerBtnIcon: {
    fontSize: "24px !important"
  },
  dropdownsLabel: {
    display: 'block',
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular,
    marginBottom: 3
  },
  showNotesBtn: {
    fontSize: "12px !important",
    color: theme.palette.text.azure,
    textDecoration: "underline",
    margin: 0,
    cursor: "pointer",
    marginRight: 10
  },
  addMoreDetailsBtn: {
    fontSize: "13px !important",
    color: theme.palette.text.azure,
    textDecoration: "underline",
    margin: 0,
    cursor: "pointer"
  },
  dialogHeaderDate:{
    color: theme.palette.text.light,
    fontWeight: theme.typography.fontWeightLight
  },
  totalTimeCnt:{
    background: theme.palette.background.items,
    padding: "5px 10px",
    marginTop: 14,
    borderRadius: 4,
    display: "flex",
    alignItems: "center",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    '& span':{
      fontSize: "14px !important"
    }
  },
  timerIconGray:{
    marginRight: 5
  },
  triangleTopRight: {
    width: 0,
    height: 0,
    borderTop: "15px solid #F4B60B",
    borderLeft: "15px solid transparent",
    position: "absolute",
    zIndex: 1,
    right: 0,
    top: 0,
    cursor: "pointer",
  },
  notesLabel: {
    fontWeight: theme.typography.fontWeightLarge,
    color: "#101010",
  },
  notes: {
    color: "#333333",
    fontWeight: 100
  },
  bootstrapTooltip: {
    fontSize: "13px !important",
    backgroundColor: "rgb(255 255 255)",
    color: "black",
    boxShadow: "0px 2px 8px #00000029",
  },
  bootstrapPopper: {
    maxWidth: 280,
    opacity: 1,
    left: "-15px !important",
  },
  selectDate: {
    position: 'relative',
    '&:hover $toolbarCurrentDate': {
      background: theme.palette.border.grayLighter
    }
  },
  toolbarCurrentDate: {
    transition: '0.4s ease all',
    borderRadius: 5,
    padding: '4px 15px',
    background: 'transparent',
    fontSize: '16px !important'
  },
  datePickerCustomStyle: {
    height: '100%',
    width: '100%',
  },
});

export default timesheetStyles;
