import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import moment from "moment";
import TimeSheet from "./TimeSheet";
import ToggleButton from "@material-ui/lab/ToggleButton";
import LeftArrow from "@material-ui/icons/ChevronLeft";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import timesheetStyles from "./styles";
import Typography from "@material-ui/core/Typography";
import PendingTimesheet from "./PendingTimesheet";
import PrintIcon from "@material-ui/icons/Print";
import EmailIcon from "@material-ui/icons/Email";
import CustomIconButton from "../../components/Buttons/CustomIconButton";
import TimesheetEmailDialog from "../../components/Dialog/TimesheetEmailDialog";
import { canDo } from "../WorkspacePermission/Permissions";
import GetUserProjects from "../../helper/getUserProjects";
import { FormattedMessage } from "react-intl";

class TimeSheetDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      alignment: "left",
      activeView: 0,
      activeViewPendingList: 0,
      openTimesheetEmail: false,
      timesheetSelectedWeekDate: moment().startOf("isoWeek"),
    };
    this.renderView = this.renderView.bind(this);
    this.TimesheetEmailDialogClose = this.TimesheetEmailDialogClose.bind(this);
    this.TimesheetEmailDialogOpen = this.TimesheetEmailDialogOpen.bind(this);
  }

  renderView(view, selectedTab) {
    this.setState({
      activeView: view,
      alignment: selectedTab,
      activeViewPendingList: 0,
      // timesheetSelectedWeekDate: moment().startOf("isoWeek"),
    });
  }
  handlePendingListView = type => {
    this.setState({ activeViewPendingList: type });
  };
  TimesheetEmailDialogClose() {
    this.setState({ openTimesheetEmail: false });
  }
  TimesheetEmailDialogOpen() {
    this.setState({ openTimesheetEmail: true });
  }
  setTimesheetSelectedWeek = timesheetSelectedWeekDate => {
    this.setState({ timesheetSelectedWeekDate });
  };
  render() {
    const { classes, theme, timesheetState, timesheetPer } = this.props;

    const {
      alignment,
      activeView,
      activeViewPendingList,
      openTimesheetEmail,
      timesheetSelectedWeekDate,
    } = this.state;
    return (
      <div className={classes.timeSheetDashboard}>
        <TimesheetEmailDialog
          date={timesheetSelectedWeekDate}
          open={openTimesheetEmail}
          closeAction={this.TimesheetEmailDialogClose}
        />

        <div className={`${classes.timeSheetDashboardHeader} flex_center_space_between_row`}>
          <div className="flex_center_start_row">
            {activeViewPendingList !== 0 ? (
              <>
                <LeftArrow
                  onClick={() => this.handlePendingListView(0)}
                  className={classes.backArrowIcon}
                />
                {/* <Typography variant="h1" classes={{ h1: classes.timeSheetHeadingDimmed }}>
                    {
                      `Timesheet: `
                    }
                  </Typography> */}
                <Typography variant="h1" classes={{ h1: classes.timeSheetHeading }}>
                  {activeViewPendingList}
                </Typography>
              </>
            ) : (
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  marginRight: "16px",
                }}>
                <Typography variant="h1" className={classes.listViewHeading}>
                  Timesheets
                </Typography>
              </div>
            )}
            {/* #cbcbcb */}
            {activeViewPendingList === 0 ? (
              <div className={classes.toggleContainer}>
                <ToggleButtonGroup
                  value={alignment}
                  exclusive
                  onChange={this.handleAlignment}
                  classes={{
                    root: classes.toggleBtnGroup,
                    selected: classes.toggleButtonSelected,
                    groupedHorizontal: classes.groupedHorizontal
                  }}>
                  <ToggleButton
                    value="left"
                    onClick={event => this.renderView(0, "left")}
                    classes={{
                      root: classes.toggleButton,
                      selected: classes.toggleButtonSelected,
                    }}>
                    <FormattedMessage id="timesheet.time-entry.label" defaultMessage="Time Entry" />
                  </ToggleButton>
                  {timesheetPer.acceptTimesheet.cando ||
                    timesheetPer.rejectTimesheet.cando ||
                    GetUserProjects(this.props.projectState.data, this.props.profileState.data.userId)
                      .length > 0 ? (
                    <ToggleButton
                      value="right"
                      onClick={event => this.renderView(1, "right")}
                      style={{
                        borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
                      }}
                      classes={{
                        root: classes.toggleButton,
                        selected: classes.toggleButtonSelected,
                      }}>
                      {/* <FormattedMessage id="timesheet.pending-approved.label" defaultMessage="Pending/Approved"/> */}
                      {/* <FormattedMessage id="task.common.status.dropdown.in-review" defaultMessage="In Review"/>   */}
                      In Review/Approved
                    </ToggleButton>
                  ) : null}
                </ToggleButtonGroup>
              </div>
            ) : null}
          </div>
          {activeView == 0 &&
            timesheetState.timeEntry.projectTimesheetVM &&
            timesheetState.timeEntry.projectTimesheetVM.length &&
            timesheetPer.sendTimesheet.cando ? (
            <div className="flex_center_start_row">
              <CustomIconButton onClick={this.TimesheetEmailDialogOpen} btnType="white">
                <EmailIcon htmlColor={theme.palette.secondary.medDark} />
              </CustomIconButton>
              {/* <CustomIconButton btnType="white">
                <PrintIcon htmlColor={theme.palette.secondary.medDark} />
              </CustomIconButton> */}
            </div>
          ) : null}
        </div>
        {activeView == 0 ? (
          <TimeSheet
            date={timesheetSelectedWeekDate}
            showSnackBar={this.props.showSnackBar}
            setTimesheetSelectedWeek={this.setTimesheetSelectedWeek}
            timesheetPer={timesheetPer}
          />
        ) : activeView == 1 ? (
          <PendingTimesheet
            date={timesheetSelectedWeekDate}
            activeView={activeViewPendingList}
            handleChangeView={this.handlePendingListView}
            setTimesheetSelectedWeek={this.setTimesheetSelectedWeek}
            timesheetPer={timesheetPer}
          />
        ) : null}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    timesheetState: state.timesheet,
    projectState: state.projects,
    profileState: state.profile,
    timesheetPer: state.workspacePermissions.data.timesheet,
  };
};

export default compose(
  withStyles(timesheetStyles, { withTheme: true }),
  connect(mapStateToProps)
)(TimeSheetDashboard);
