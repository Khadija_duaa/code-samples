import React, { Component } from "react";
import sortBy from "lodash/sortBy";
import find from "lodash/find";
import findIndex from "lodash/findIndex";
import filter from "lodash/filter";
import cloneDeep from "lodash/cloneDeep";
import CustomIconButton from "../../components/Buttons/CustomIconButton";
import RightArrow from "@material-ui/icons/ArrowRight";
import LeftArrow from "@material-ui/icons/ArrowLeft";
import CustomButton from "../../components/Buttons/CustomButton";
import timesheetStyles from "./styles";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
import { connect } from "react-redux";
import moment from "moment";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import TaskIcon from "@material-ui/icons/ViewList";
import Typography from "@material-ui/core/Typography";
import RejectDialog from "./RejectDialog";
import MessageIcon from "@material-ui/icons/Message";
import EmptyState from "../../components/EmptyStates/EmptyState";
import SvgIcon from "@material-ui/core/SvgIcon";
import CustomTooltip from "../../components/Tooltip/Tooltip";
import { timesheetsHelpText } from "../../components/Tooltip/helptext";
import helper from "../../helper";
import ProjectsIcon from "../../components/Icons/ProjectIcon";
import CustomAvatar from "../../components/Avatar/Avatar";
import {
  getWeekDates,
  calRowsTotalTime,
  generateWeekDates,
  getDate,
  calColsTotalTime,
  calTotalTime,
} from "./timingFunctions";
import { generateUsername } from "../../utils/common";
import {
  getPendingTimesheetWeekDurations,
  withdrawTimesheet,
  approvePendingTimesheet,
  rejectPendingTimesheet,
} from "../../redux/actions/timesheet";
import { updateTasksTimesheetStatus } from "../../redux/actions/tasks";
import { calculateContentHeight } from "../../utils/common";
import { FormattedMessage } from "react-intl";
import ProjectPermissionSelector from "../../redux/selectors/projectPermissionSelector";
import LockIcon from "@material-ui/icons/Lock";
import WithdrawDialog from "./WithdrawDialog";
import TimesheetComments from "../../components/TimesheetComments/TimesheetComments.view";
import BillableIcon from "../../components/Icons/Billable";
import NonBillableIcon from "../../components/Icons/NonBillable";
import CustomDatePicker from "../../components/DatePicker/DatePicker/ListViewDatePicker";

class PendingTimesheet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: moment().startOf("isoWeek"),
      expanded: null,
      rejectDialog: false,
      weekDates: [],
      timesheetData: {},
      selectedUserId: null,
      selectedProjectId: null,
      rejectBtnQuery: "",
      approveBtnQuery: "",
      totalTimeLogged: "",
    };
    this.handleThisWeek = this.handleThisWeek.bind(this);
  }
  componentDidMount() {
    this.loadWeekData("");
  }
  handleThisWeek() {
    const { date } = this.state;
    const currentDate = moment().startOf("isoWeek");
    if (date.toString() !== currentDate.toString())
      this.setState({ date: currentDate }, () => {
        this.loadWeekData("");
      });
  }
  handleNextWeek = () => {
    const { date } = this.state;
    const nextWeek = date.clone().add(7, "days");
    this.setState({ date: nextWeek });
    this.loadWeekData(`&currentDate=${nextWeek.format("YYYY-MM-DDTHH:mm:ss")}`);
  };
  handlePrevWeek = () => {
    const { date } = this.state;
    const prevWeek = date.clone().subtract(7, "days");
    this.setState({ date: prevWeek });
    this.loadWeekData(`&currentDate=${prevWeek.format("YYYY-MM-DDTHH:mm:ss")}`);
  };
  handleSelectDate = (date) => {
    if (date) {
      const startDate = moment(date).startOf('isoWeek');
      this.setState({ date: startDate });
      this.loadWeekData(`&currentDate=${startDate.format("YYYY-MM-DDTHH:mm:ss")}`);
    }
  }
  handleChange = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false,
    });
  };
  loadWeekData = parms => {
    this.props.getPendingTimesheetWeekDurations(parms, (error, response) => {
      if (error) {
      } else {
        if (response) {
          this.setState({
            timesheetData: cloneDeep(this.props.timesheetState.pendingApproved),
          });
        }
      }
    });
  };
  rejectDialogOpen = (event, selectedProjectId, totalTime) => {
    event.stopPropagation();
    this.setState({ rejectDialog: true, selectedProjectId, totalTimeLogged: totalTime });
  };
  rejectDialogClose = () => {
    this.setState({ rejectDialog: false });
  };
  withdrawDialogOpen = (event, selectedProjectId, totalTime) => {
    event.stopPropagation();
    this.setState({ withdrawDialog: true, selectedProjectId, totalTimeLogged: totalTime });
  };
  withdrawDialogClose = () => {
    this.setState({ withdrawDialog: false });
  };
  handleWithdraw = comment => {
    const { date, selectedProjectId, timesheetData, selectedUserId, totalTimeLogged } = this.state;
    const data = {
      projectId: selectedProjectId,
      submitBy: selectedUserId,
      other: selectedProjectId ? false : true,
      startDate: date.format("YYYY-MM-DDTHH:mm:ss"),
      weeklyTime: totalTimeLogged,
      endDate: date
        .clone()
        .add(6, "days")
        .format("YYYY-MM-DDTHH:mm:ss"),
      comment,
    };
    if (!selectedProjectId) {
      let otherTasks = [];
      const userIndex = findIndex(timesheetData["userSubmittedTimesheet"] || [], {
        submitUser: selectedUserId,
      });
      const projects =
        timesheetData["userSubmittedTimesheet"][userIndex]["projectTimesheetVM"] || [];
      const projectIndex = findIndex(projects, {
        projectId: selectedProjectId,
      });
      const tasks =
        timesheetData["userSubmittedTimesheet"][userIndex]["projectTimesheetVM"][projectIndex][
        "taskEffortVM"
        ] || [];
      tasks.map(obj => {
        otherTasks.push(obj.taskId);
      });
      data.OtherTaskIds = otherTasks;
    }
    this.setState({ rejectBtnQuery: "progress" });
    this.props.withdrawTimesheet(data, (error, response) => {
      this.setState({ rejectBtnQuery: "" });
      if (error) {
        this.setState({
          rejectDialog: false,
          withdrawDialog: false,
        });
      } else {
        if (response) {
          this.setState({
            timesheetData: cloneDeep(this.props.timesheetState.pendingApproved),
            rejectDialog: false,
            withdrawDialog: false,
          });
        }
      }
    });
  };
  handleReject = comment => {
    const { date, selectedProjectId, timesheetData, selectedUserId, totalTimeLogged } = this.state;
    const data = {
      projectId: selectedProjectId,
      submitBy: selectedUserId,
      other: selectedProjectId ? false : true,
      startDate: date.format("YYYY-MM-DDTHH:mm:ss"),
      weeklyTime: totalTimeLogged,
      endDate: date
        .clone()
        .add(6, "days")
        .format("YYYY-MM-DDTHH:mm:ss"),
      comment,
    };
    if (!selectedProjectId) {
      let otherTasks = [];
      const userIndex = findIndex(timesheetData["userSubmittedTimesheet"] || [], {
        submitUser: selectedUserId,
      });
      const projects =
        timesheetData["userSubmittedTimesheet"][userIndex]["projectTimesheetVM"] || [];
      const projectIndex = findIndex(projects, {
        projectId: selectedProjectId,
      });
      const tasks =
        timesheetData["userSubmittedTimesheet"][userIndex]["projectTimesheetVM"][projectIndex][
        "taskEffortVM"
        ] || [];
      tasks.map(obj => {
        otherTasks.push(obj.taskId);
      });
      data.OtherTaskIds = otherTasks;
    }
    this.setState({ rejectBtnQuery: "progress" });
    this.props.rejectPendingTimesheet(data, (error, response) => {
      this.setState({ rejectBtnQuery: "" });
      if (error) {
      } else {
        if (response) {
          this.setState({
            timesheetData: cloneDeep(this.props.timesheetState.pendingApproved),
            rejectDialog: false,
          });
        }
      }
    });
  };

  handleApprove = (event, projectId, time) => {
    event.stopPropagation();
    const { date, timesheetData, selectedUserId } = this.state;
    const data = {
      projectId: projectId,
      submitBy: selectedUserId,
      other: projectId ? false : true,
      startDate: date.format("YYYY-MM-DDTHH:mm:ss"),
      endDate: date
        .clone()
        .add(6, "days")
        .format("YYYY-MM-DDTHH:mm:ss"),
      weeklyTime: time,
    };
    if (!projectId) {
      let otherTasks = [];
      const userIndex = findIndex(timesheetData["userSubmittedTimesheet"] || [], {
        submitUser: selectedUserId,
      });
      const projects =
        timesheetData["userSubmittedTimesheet"][userIndex]["projectTimesheetVM"] || [];
      const projectIndex = findIndex(projects, { projectId: projectId });
      const tasks =
        timesheetData["userSubmittedTimesheet"][userIndex]["projectTimesheetVM"][projectIndex][
        "taskEffortVM"
        ] || [];
      tasks.map(obj => {
        otherTasks.push(obj.taskId);
      });
      data.OtherTaskIds = otherTasks;
    }
    this.setState({ approveBtnQuery: "progress" });
    this.props.approvePendingTimesheet(data, (error, response) => {
      this.setState({ approveBtnQuery: "" });
      if (error) {
      } else {
        if (response) {
          const resData = response.data;
          // In Progress -> Approved
          if (resData.status) {
            const statusProjectId = projectId;
            const timesheetData = this.props.timesheetState.pendingApproved;
            let taskIds = [];
            const userIndex = findIndex(timesheetData["userSubmittedTimesheet"] || [], {
              submitUser: selectedUserId,
            });
            const projects =
              timesheetData["userSubmittedTimesheet"][userIndex]["projectTimesheetVM"] || [];
            const projectIndex = findIndex(projects, {
              projectId: statusProjectId,
            });
            const tasks = projects[projectIndex]["taskEffortVM"] || [];
            tasks.map(obj => {
              taskIds.push(obj.taskId);
            });
            // this.props.updateTasksTimesheetStatus(taskIds, "Approved");
          }
          this.setState({
            timesheetData: cloneDeep(this.props.timesheetState.pendingApproved),
          });
        }
      }
    });
  };
  handleTimesheetClick = (userName, selectedUserId) => {
    this.setState({ selectedUserId }, () => {
      this.props.handleChangeView(userName);
    });
  };
  renderCurrentDate = (stamp, i) => {
    const date = this.state.date.clone().add(i, "days");
    const formatedDate = date.format("MM/DD/YYYY");
    if (moment(formatedDate).isSame(stamp.date)) {
      return stamp.duration || helper.standardTimeFormat("0:0");
    }
  };
  filterSelectedUserTimesheetData = () => {
    const { selectedUserId, timesheetData } = this.state;
    let filtered = cloneDeep(timesheetData);
    filtered =
      filter(timesheetData["userSubmittedTimesheet"], {
        submitUser: selectedUserId,
      }) || [];
    if (filtered.length) return filtered[0]["projectTimesheetVM"].filter(p => p.status != "Rejected");
    return filtered;
  };
  calculateUserBasedTimeLoggedTime = (user, weekDays) => {
    const timesheets = user["projectTimesheetVM"];
    let rowsTotal = [];
    timesheets.map(timesheet => {
      rowsTotal = rowsTotal.concat(calRowsTotalTime(weekDays, timesheet["taskEffortVM"]));
    });
    return calTotalTime(rowsTotal);
  };
  showTranslatedStatus = status => {
    let intl = { id: "", message: "" };
    switch (status) {
      case "In Progress":
        intl.id = "task.common.status.dropdown.in-progress";
        intl.message = "In Progress";
        break;
      case "In Review":
        intl.id = "task.common.status.dropdown.in-review";
        intl.message = "In Review";
        break;
      case "Rejected":
        intl.id = "timesheet.status.reject";
        intl.message = "Rejected";
        break;

      case "Approved":
        intl.id = "timesheet.status.approve";
        intl.message = "Approved";
        break;
      case "In Progress - Withdrawn Approval":
        intl.id = "timesheet.status.withDraw";
        intl.message = "In Progress - Withdrawn Approval";
        break;
      default:
        intl.id = "";
        intl.message = status;
        break;
    }
    return <FormattedMessage id={intl.id} defaultMessage={intl.message} />;
  };
  getMemberName = id => {
    return this.props.profileState.data.teamMember.find(m => m.userId == id).fullName.toLowerCase();
  };
  render() {
    const { classes, theme, activeView, timesheetPer, permissionObject } = this.props;
    const { allMembers } = this.props.profileState.data.member;
    const {
      date,
      expanded,
      rejectDialog,
      timesheetData,
      rejectBtnQuery,
      approveBtnQuery,
      withdrawDialog,
    } = this.state;
    let users = [],
      projects = [];
    if (activeView === 0) {
      users = timesheetData["userSubmittedTimesheet"] || [];
      if (users.length) {
        users = users.reduce((r, cv) => {
          const filterdTimesheet = cv.projectTimesheetVM.filter(p => p.status != "Rejected");
          let tempObj;
          if (filterdTimesheet.length) {
            tempObj = { ...cv, projectTimesheetVM: filterdTimesheet }
            r.push(tempObj);
          }
          return r;
        }, []);
      }
      users = users.sort((a, b) => {
        if (this.getMemberName(a.submitUser) < this.getMemberName(b.submitUser)) {
          return -1;
        }
        if (this.getMemberName(a.submitUser) > this.getMemberName(b.submitUser)) {
          return 1;
        }
        return 0;
      });
      users = users.filter(o => {
        return o.submitUser;
      });
    } else {
      projects = this.filterSelectedUserTimesheetData();
      if (projects.length) {
        // Sort data
        projects = sortBy(projects, [
          function (o) {
            if (o.projectName === "Other Tasks") return "";
            else return o.projectName;
          },
        ]);
      } else {
        this.props.handleChangeView(0);
      }
    }
    const weekDays = getWeekDates(date);

    return (
      <div className={classes.timeSheetCnt} style={{ height: calculateContentHeight() }}>
        {activeView === 0 ? (
          <>
            {/* Next Prev Buttons Starts */}
            <div className="flex_center_space_between_row mb20">
              <div className={classes.NextBackBtnCnt}>
                <CustomIconButton
                  btnType="condensed"
                  style={{ padding: 0 }}
                  onClick={this.handlePrevWeek}>
                  <LeftArrow
                    htmlColor={theme.palette.secondary.dark}
                    style={{fontSize: "24px"}}
                    // classes={{ root: classes.navigationArrow }}
                  />
                </CustomIconButton>
                <div className={classes.selectDate}>
                  <span className={classes.toolbarCurrentDate}>{getDate(date)}</span>
                  <CustomDatePicker
                    dateFormat="MMM DD, YYYY"
                    timeInput={false}
                    datePickerProps={{
                    }}
                    onSelect={(date) => {
                      this.handleSelectDate(date)
                    }}
                    filterDate={moment()}
                    btnProps={{ className: classes.datePickerCustomStyle }}
                    style={{
                      display: "flex",
                      alignItems: "center",
                      opacity: 0,
                      position: 'absolute',
                      top: 0,
                      left: 0,
                      width: '100%',
                      height: '100%',
                      zIndex: 2,
                    }}
                    PopperProps={{ disablePortal: false }}
                    showDates={false}
                    placeholder={"Select Date"}
                    icon={true}
                  />
                </div>


                <CustomIconButton
                  btnType="condensed"
                  style={{ padding: 0 }}
                  onClick={this.handleNextWeek}>
                  <RightArrow
                    htmlColor={theme.palette.secondary.dark}
                    style={{fontSize: "24px"}}
                    // classes={{ root: classes.navigationArrow }}
                  />
                </CustomIconButton>
              </div>
              {!date.isSame(moment().startOf("isoWeek")) ? (
                <CustomButton
                  onClick={this.handleThisWeek}
                  btnType="graydashed"
                  variant="contained">
                  <FormattedMessage id="timesheet.this-week.label" defaultMessage="This Week" />
                </CustomButton>
              ) : null}
            </div>
            {/* Next Prev Buttton Ends */}
            {users.length ? null : (
              <div className={classes.emptyStateCnt}>
                <EmptyState
                  screenType="timesheet"
                  heading={
                    <FormattedMessage
                      id="timesheet.no-timesheet-submitted.label"
                      defaultMessage="No Timesheets Submitted"
                    />
                  }
                  message={
                    <FormattedMessage
                      id="timesheet.no-timesheet-submitted.message"
                      defaultMessage="There are no timesheets pending to be approved."
                    />
                  }
                  button={false}
                />
              </div>
            )}
            {users.map((user, index) => {
              const totalTime = this.calculateUserBasedTimeLoggedTime(user, weekDays);
              let localUser = find(allMembers, { userId: user.submitUser });
              let name = "";
              if (localUser)
                name = generateUsername(localUser.email, localUser.userName, localUser.fullName);
              else {
                localUser = { imageUrl: `` };
              }
              return (
                <ExpansionPanelSummary
                  classes={{
                    root: classes.accordSummaryPanelPending,
                    content: classes.accordSummaryPanelContent,
                  }}
                  expandIcon={<RightArrow style={{ right: 0 }} />}
                  key={index}>
                  <div
                    className={classes.accordSummaryInnerCnt}
                    onClick={() => this.handleTimesheetClick(name, user["submitUser"])}>
                    <div className="flex_center_start_row">
                      <CustomAvatar
                        otherMember={{
                          imageUrl: localUser.imageUrl,
                          fullName: localUser.fullName,
                          lastName: "",
                          email: localUser.email,
                          isOnline: localUser.isOnline,
                          isOwner: localUser.isOwner,
                        }}
                        size="small"
                      />
                      <Typography variant="h5" className={classes.accorheading}>
                        {name}
                      </Typography>
                    </div>
                    <div className="flex_center_start_row">
                      <div className={`flex_center_center_col ${classes.timeLoggedCnt}`}>
                        <Typography variant="caption">
                          <FormattedMessage
                            id="timesheet.time-logged.label"
                            defaultMessage="Time Logged"
                          />
                        </Typography>
                        <p>
                          {helper.standardTimeFormat(totalTime)}
                          <span>
                            <FormattedMessage id="timesheet.hours.label" defaultMessage=" hours" />
                          </span>
                        </p>
                      </div>
                    </div>
                  </div>
                </ExpansionPanelSummary>
              );
            })}
          </>
        ) : (
          <>
            <RejectDialog
              open={rejectDialog}
              closeAction={this.rejectDialogClose}
              action={this.handleReject}
              btnQuery={rejectBtnQuery}
            />
            <WithdrawDialog
              open={withdrawDialog}
              closeAction={this.withdrawDialogClose}
              action={this.handleWithdraw}
              btnQuery={rejectBtnQuery}
            />
            {projects.map((project, index) => {
              const data = project["taskEffortVM"] || [];
              const rowsTotalTime = calRowsTotalTime(weekDays, data);
              const colsTotalTime = calColsTotalTime(weekDays, data);
              const totalTime = calTotalTime(rowsTotalTime);
              const statusClass =
                project["status"] == "In Review"
                  ? classes.inReview
                  : project["status"] == "Rejected"
                    ? classes.rejected
                    : project["status"] == "Approved"
                      ? classes.approved
                      : project["status"] == "In Progress - Withdrawn Approval"
                        ? classes.withdraw
                        : classes.inProgress;
              const timeSheetProjectPermission = permissionObject[project.projectId]
                ? permissionObject[project.projectId].timesheet
                : timesheetPer;
              return (
                <ExpansionPanel
                  expanded={expanded === `${index}`}
                  onChange={this.handleChange(`${index}`)}
                  classes={{ root: classes.accordPanelRoot }}
                  key={index}>
                  <ExpansionPanelSummary
                    classes={{
                      root: classes.accordSummaryPanel,
                      content: classes.accordSummaryPanelContent,
                    }}
                    expandIcon={<ExpandMoreIcon />}>
                    <div className={classes.accordSummaryInnerCnt}>
                      <div className="flex_center_start_row">
                        {project["projectName"] == "Other Tasks" ? (
                          <SvgIcon viewBox="0 0 18 15.188" className={classes.smallBtnIcon}>
                            <TaskIcon />
                          </SvgIcon>
                        ) : (
                          <SvgIcon viewBox="0 0 18 15.188" className={classes.smallBtnIcon}>
                            <ProjectsIcon />
                          </SvgIcon>
                        )}
                        <Typography variant="h5" className={classes.accorheading}>
                          {project["projectName"] == "Other Tasks" ||
                            project["projectName"] == "" ? (
                            "Other Tasks"
                          ) : (
                            <>
                              <b>Project Name: </b>
                              {project["projectName"]}
                            </>
                          )}
                        </Typography>
                        {project["projectName"] == "Other Tasks" ? (
                          <CustomTooltip
                            helptext={timesheetsHelpText.otherTasksHelpText}
                            iconType="help"
                            position="static"
                          />
                        ) : null}
                        <span className={`${classes.statusTag} ${statusClass}`}>
                          {this.showTranslatedStatus(project["status"])}
                        </span>
                        <TimesheetComments comments={project["rejectReason"]} />
                      </div>
                      <div className="flex_center_start_row">
                        {project["status"] == "Approved" ? (
                          <CustomButton
                            btnType="gray"
                            variant="contained"
                            onClick={e =>
                              this.withdrawDialogOpen(
                                e,
                                project["projectId"],
                                helper.standardTimeFormat(totalTime)
                              )
                            }>
                            <LockIcon
                              htmlColor={theme.palette.secondary.light}
                              className={classes.lockIcon}
                            />
                            <FormattedMessage
                              id="timesheet.withdraw-approval-button.label"
                              defaultMessage="Withdraw Approval"
                            />
                          </CustomButton>
                        ) : null}
                        {project["status"] == "In Review" ? (
                          <>
                            <CustomButton
                              btnType="dangerText"
                              style={{ marginRight: 10 }}
                              variant="contained"
                              onClick={e =>
                                this.rejectDialogOpen(
                                  e,
                                  project["projectId"],
                                  helper.standardTimeFormat(totalTime)
                                )
                              }
                              disabled={!timeSheetProjectPermission.rejectTimesheet.cando}>
                              <FormattedMessage
                                id="timesheet.reject.label"
                                defaultMessage="Reject"
                              />
                            </CustomButton>
                            <CustomButton
                              btnType="success"
                              variant="outlined"
                              onClick={event =>
                                this.handleApprove(
                                  event,
                                  project["projectId"],
                                  helper.standardTimeFormat(totalTime)
                                )
                              }
                              query={approveBtnQuery}
                              disabled={
                                approveBtnQuery == "progress" ||
                                !timeSheetProjectPermission.acceptTimesheet.cando
                              }>
                              <FormattedMessage
                                id="timesheet.approve-button.label"
                                defaultMessage="Approve Timesheet"
                              />
                            </CustomButton>
                          </>
                        ) : null}
                        <div className={`flex_center_center_col ${classes.timeLoggedCnt}`}>
                          <Typography variant="caption">
                            <FormattedMessage
                              id="timesheet.time-logged.label"
                              defaultMessage="Time Logged"
                            />
                          </Typography>
                          <p>
                            {helper.standardTimeFormat(totalTime)}
                            <span>
                              <FormattedMessage id="timesheet.hours.label" defaultMessage="hours" />
                            </span>
                          </p>
                        </div>
                      </div>
                    </div>
                  </ExpansionPanelSummary>
                  <ExpansionPanelDetails classes={{ root: classes.expansionDetailsCnt }}>
                    {/* Time Sheet Header Starts */}
                    <div className={classes.timesheetHeaderCnt}>
                      <div className={classes.addTaskBtnCnt}>Task Title</div>
                      <div className={classes.weekDatesCnt}>
                        {generateWeekDates(weekDays, classes)}
                        <div className={classes.headerTotalCell} style={{ paddingRight: 0 }}>
                          <FormattedMessage id="common.total.label" defaultMessage="Total" />
                        </div>
                      </div>
                    </div>
                    {/* Time Sheet Header Ends */}
                    {data.map((ele, i) => {
                      return (
                        <div
                          key={ele.taskId}
                          className={classes.weekDatesRow}
                          style={{ borderBottom: i == 0 ? "none" : "" }}>
                          <p className={classes.taskTitle}>
                            {ele.isBillable ? <CustomTooltip
                              helptext={
                                'Billable Task'
                              }
                              placement="top"
                              style={{ color: theme.palette.common.white }}>
                              <SvgIcon
                                viewBox="0 0 17.99 17.99"
                                className={classes.billableIcon}>
                                <BillableIcon />
                              </SvgIcon>
                            </CustomTooltip> :
                              <CustomTooltip
                                helptext={
                                  'Non-Billable Task'
                                }
                                placement="top"
                                style={{ color: theme.palette.common.white }}>
                                <SvgIcon
                                  viewBox="0 0 18.39 18.39"
                                  className={classes.nonBillableIcon}>
                                  <NonBillableIcon />
                                </SvgIcon>
                              </CustomTooltip>}
                            {ele.taskTitle}</p>
                          <div className={classes.weekDatesInputCnt}>
                            {ele.timestamp.map((stamp, index) => {
                              const renderUI = (
                                <div key={stamp.id} className={classes.weekDatesCellPending}>
                                  {stamp.notes && <div className={classes.triangleTopRight}></div>}
                                  <span>{this.renderCurrentDate(stamp, index)}</span>
                                </div>
                              );
                              return stamp.notes ? (
                                <CustomTooltip
                                  helptext={
                                    <div>
                                      <span className={classes.notesLabel}>Notes:</span>
                                      <div className={classes.notes}>{stamp.notes}</div>
                                    </div>
                                  }
                                  iconType="help"
                                  position="static"
                                  placement="right"
                                  hideArrow={true}
                                  classes={classes}>
                                  {renderUI}
                                </CustomTooltip>
                              ) : (
                                renderUI
                              );
                            })}
                            <div className={classes.totalTaskTime} style={{ paddingRight: 0 }}>
                              {helper.standardTimeFormat(rowsTotalTime[i])}
                            </div>
                          </div>
                        </div>
                      );
                    })}
                    <div className={classes.timesheetFooterCnt}>
                      <p className={classes.footerTotalLabel}>
                        <FormattedMessage id="common.total.label" defaultMessage="Total" />
                      </p>
                      <div className={classes.weekDatesCnt}>
                        {colsTotalTime.map((time, i) => {
                          return (
                            <div key={i} className={this.props.classes.footerTotalCell}>
                              {helper.standardTimeFormat(time)}
                            </div>
                          );
                        })}
                        <div className={classes.footerTotalCell} style={{ paddingRight: 0 }}>
                          {helper.standardTimeFormat(totalTime)}
                        </div>
                      </div>
                    </div>
                  </ExpansionPanelDetails>
                </ExpansionPanel>
              );
            })}
          </>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    timesheetState: state.timesheet,
    profileState: state.profile,
    permissionObject: ProjectPermissionSelector(state),
  };
};

export default compose(
  withRouter,
  withStyles(timesheetStyles, { withTheme: true }),
  connect(mapStateToProps, {
    getPendingTimesheetWeekDurations,
    rejectPendingTimesheet,
    withdrawTimesheet,
    approvePendingTimesheet,
    updateTasksTimesheetStatus,
  })
)(PendingTimesheet);
