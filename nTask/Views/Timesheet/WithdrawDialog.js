import React, { Component } from "react";
import CustomDialog from "../../components/Dialog/CustomDialog";
import dialogStyles from "../../assets/jss/components/dialog";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import Grid from "@material-ui/core/Grid";
import DefaultButton from "../../components/Buttons/DefaultButton";
import CustomButton from "../../components/Buttons/CustomButton";
import TimerIcon from "@material-ui/icons/Timer";
import Typography from "@material-ui/core/Typography";
import MessageIcon from '@material-ui/icons/Message';
import combineStyles from "../../utils/mergeStyles";
import timesheetStyles from "./styles";
import DefaultTextField from "../../components/Form/TextField";
import {FormattedMessage,injectIntl} from "react-intl";
class WithdrawDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      comments: ''
    }
    this.handleInput = this.handleInput.bind(this);
  }
  handleInput(event) {
    this.setState({ comments: event.target.value })
  }
  handleReject = () => {
    const { action } = this.props;
    if (action)
      action(this.state.comments.trim());
  }
  render() {
    const { classes, theme, open, closeAction, btnQuery} = this.props;
    const { comments } = this.state;
    return (
      <CustomDialog
        title='Withdraw Timesheet'
        dialogProps={{ open: open, onClose: closeAction }}
      >
        <div className={classes.dialogContentCnt}>


          <Typography variant="h5">
            Are you sure you want to withdraw this timesheet?
            </Typography>
          <DefaultTextField
            label={false}
            fullWidth={true}
            error={false}
            defaultProps={{
              id: "comments",
              onChange: this.handleInput,
              value: comments,
              multiline: true,
              placeholder: "Add your comment (Optional)",
              rows: 7,
              autoFocus: true,
              inputProps: { maxLength: 1500 }
            }}
          />
        </div>
        <Grid
          container
          direction="row"
          justify="flex-end"
          alignItems="center"
          classes={{
            container: classes.dialogActionsCnt
          }}
        >

          <DefaultButton
            onClick={closeAction}
            text={<FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel"/>}
            buttonType="Transparent"
            style={{ marginRight: 20 }}
          />
          <CustomButton
            onClick={this.handleReject}
            btnType="dangerText"
            variant="contained"
            query={btnQuery}
            disabled={btnQuery == "progress"}
          >
            Withdraw
          </CustomButton>
        </Grid>
      </CustomDialog>
    );
  }
}

export default compose( injectIntl,withStyles(combineStyles(dialogStyles, timesheetStyles), { withTheme: true }))(WithdrawDialog);
