import React from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import customFieldsColumns from "./customFieldsColumns.style";
import { customFieldColumnDropdownData } from "../../helper/customFieldsData";
import SearchDropdown from "../../components/Dropdown/SearchDropdown";
import CustomTooltip from "../../components/Tooltip/Tooltip";
import { getContrastYIQ } from "../../helper/index";

function CustomFieldsColumns(props) {
  const {
    theme,
    classes,
    permission,
    customFieldData,
    column,
    rowValue,
    customFieldChange,
    handleAddAssessmentDialog,
    disabled
  } = props;

  const getCombinedValues = (param, value) => {
    if (param === "array") return value.map(v => v.label).join(",");
    else if (param === "object")
      return value.label ? value.label : value.cellName ? value.cellName : "Select";
    else return "";
  };

  const renderDropDown = () => {
    let val = "Select";
    const selectedField =
      customFieldData && customFieldData.find(cf => cf.fieldId === column.fieldId);

    let data = null;
    if (selectedField && selectedField.fieldData.data) {
      data = !column.settings.multiSelect
        ? column.values.data.find(ele => ele.id === selectedField.fieldData.data.id)
        : selectedField.fieldData.data.reduce((res, cv) => {
            const isExists = column.values.data.find(ele => ele.id === cv.id);
            if (isExists) res.push(isExists);
            return res;
          }, []);
    }

    let selectedOptions = [];
    if (data && column.fieldType == "dropdown") {
      selectedOptions = column.settings.multiSelect
        ? customFieldColumnDropdownData(data)
        : customFieldColumnDropdownData([data]);
    }
    if (selectedField && selectedField.fieldType === "matrix") {
      val = Array.isArray(selectedField.fieldData.data)
        ? selectedField.fieldData.data[selectedField.fieldData.data.length - 1].cellName
        : "Select";
    }
    if (selectedField && selectedField.fieldType === "dropdown") {
      val = getCombinedValues(
        "array",
        selectedOptions
      ); /** on basics of element type merge its values into single string */
    }
    if (!selectedField) {
      val = "Select";
    }
    return (
      <SearchDropdown
        initSelectedOption={selectedOptions}
        buttonProps={{
          disabled: !permission.isAllowEdit,
          style: !column.settings.multiSelect
            ? {
                /** if single select the set the background color and text color */
                background: data ? data.color : "",
                color: data ? getContrastYIQ(data.color && data.color) : "",
                padding: 4,
              }
            : {},
        }}
        obj={column}
        optionBackground={true}
        optionsList={customFieldColumnDropdownData(column.values.data)}
        updateAction={(option, obj) => customFieldChange(option, column, rowValue.row)}
        selectedOptionHead="Custom Field"
        allOptionsHead={"Options"}
        buttonPlaceholder={"Select"}
        isMulti={column.settings.multiSelect}
        multipleOptionsPlaceHolder={val}
        disabled={!permission.isAllowEdit || disabled}
        tooltip={true}
        tooltipText={val}
        labelAlign={"center"}
      />
    );
  };
  const renderInput = () => {
    let val = "Select";
    const selectedField =
      customFieldData && customFieldData.find(cf => cf.fieldId === column.fieldId);
    let data = selectedField && selectedField.fieldData.data;

    if (!selectedField) {
      val = "-";
    } else {
      val = data;
    }
    return (
      <h3
        className={classes.textFieldText}
        key={rowValue.row.id}
        title={val}
        style={{
          color: `${theme.palette.common.black}`,
        }}>
        {val}
      </h3>
    );
  };
  const renderMatrix = () => {
    let val = "Select";
    const selectedField =
      customFieldData && customFieldData.find(cf => cf.fieldId === column.fieldId);
    let data = column;
    let columnObject = {};
    if (selectedField && selectedField.fieldType === "matrix") {
      let indexing = "ABCDEFGH";
      let rowValueIndex = indexing.indexOf(selectedField.fieldData.data[selectedField.fieldData.data.length - 1].cellName.slice(0,1));
      let columnValueIndex = selectedField.fieldData.data[selectedField.fieldData.data.length - 1].cellName.slice(1,2);
      columnObject = data.settings.matrix[rowValueIndex][columnValueIndex - 1];
      val = Array.isArray(selectedField.fieldData.data)
        ? selectedField.fieldData.data[selectedField.fieldData.data.length - 1].cellName
        : "Select";
    }
    if (!selectedField) {
      val = "Select";
    }
    return (
      <CustomTooltip helptext={val} placement="top">
        {val === "Select" /** no value select respect to each risk */ ? (
          <h3
            key={rowValue.row.id}
            className={classes.captionCustom}
            style={{
              paddingLeft: 0,
              color: `${theme.palette.common.black}`,
              textDecoration: "underline",
            }}
            onClick={event =>
              permission.isAllowEdit && !disabled &&
              handleAddAssessmentDialog(true, event, rowValue.row, column.fieldId)
            }>
            {val}
          </h3>
        ) : (
          <h3
            className={classes.captionCustom}
            key={rowValue.row.id}
            style={
              selectedField.fieldData && selectedField.fieldData.data.length > 0
                ? {
                    backgroundColor:
                      columnObject.color,
                    color: getContrastYIQ(
                      columnObject.color
                    ),
                    border: `1px solid ${
                      !columnObject.color
                        ? "black"
                        : columnObject
                            .color
                    }`,
                  }
                : {
                    color: `${theme.palette.common.black}`,
                    border: `1px solid ${theme.palette.common.black}`,
                  }
            }
            onClick={event =>
              permission.isAllowEdit && !disabled &&
              handleAddAssessmentDialog(true, event, rowValue.row, column.fieldId)
            }>
            {val}
          </h3>
        )}
      </CustomTooltip>
    );
  };

  return column.fieldType == "dropdown"
    ? renderDropDown()
    : column.fieldType == "textfield"
    ? renderInput()
    : column.fieldType == "matrix"
    ? renderMatrix()
    : null;
}

CustomFieldsColumns.defaultProps = {
  permission: {
    isAllowEdit: true,
  },
  customFieldData: [],
  column: {},
  customFieldChange: () => {},
  handleAddAssessmentDialog: () => {},
  classes: {},
  theme: {},
  rowValue: {},
};
export default compose(withStyles(customFieldsColumns, { withTheme: true }))(CustomFieldsColumns);
