const customFieldsColumns = theme => ({
    captionCustom: {
        // fontFamily: "Lato, sans-serif",
        fontWeight: theme.typography.fontWeightRegular,
        fontSize: "12px !important",
        // margin: 0,
        // letterSpacing: 0,
        color: `${theme.palette.common.white}`,
        // height: 18,
        overflow: "hidden",
        whiteSpace: "nowrap",
        textOverflow: "ellipsis",
        maxWidth: "122px !important",
        padding: "5px 10px",
        borderRadius: 4,
        cursor : "pointer"
      },
      textFieldText: {
        textAlign: 'center',
        fontWeight: theme.typography.fontWeightRegular,
        fontSize: "12px !important",
        color: `${theme.palette.common.white}`,
        overflow: "hidden",
        whiteSpace: "nowrap",
        textOverflow: "ellipsis",
        maxWidth: "122px !important",
        padding: "5px 10px",
        borderRadius: 4,
        cursor : "pointer"
      },
});

export default customFieldsColumns;
