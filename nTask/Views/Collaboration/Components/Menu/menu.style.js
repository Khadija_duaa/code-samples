const menuStyles = theme => ({  
    icon: {
      color: theme.palette.text.azure,
      fontSize: "22px",
    },
    menuItemSelected: {
      background: `transparent !important`,
      "&:before": {
        content: "''",
        position: "absolute",
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        width: 5,
        background: theme.palette.secondary.main,
      },
      "&:hover": {
        background: `transparent !important`,
      },
    },
    menuText: {
      fontSize: 13,
      color: theme.palette.text.primary,
      fontWeight: theme.typography.fontWeightRegular,
    },
  });
  
  export default menuStyles;
  