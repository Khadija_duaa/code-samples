import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import SvgIcon from "@material-ui/core/SvgIcon";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import SelectionMenu from "../../../../components/Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ZoomIcon from "../../../../components/Icons/ZoomIcon";
import NTaskLogo from "../../../../components/Icons/nTaskLogo";
import menuStyles from "./menu.style";

const startZoomMeeting=()=>{
    console.log('ZoomMeeting')
}
const startWebexMeeting=()=>{
    console.log('WebexMeeting')
}
const startnTaskMeeting=()=>{
    console.log('nTaskMeeting')
}
const menuData=[
    {description:'Start a Zoom Meeting', type:'zoom', func: startZoomMeeting},
    {description:'Start a Webex Meeting', type:'zoom', func: startWebexMeeting},
    {description:'Start a nTask Meeting', type:'ntask', func: startnTaskMeeting},
]

const getIcons = (classes, type, theme)=>{
    switch (type) {
        case "zoom":
          return (
            <SvgIcon viewBox="0 0 22 22" className={classes.icon}>
                <ZoomIcon />
            </SvgIcon>
          );
          break
        case "ntask":
          return (
            <SvgIcon viewBox="0 0 18 15.282" className={classes.icon}>
                <NTaskLogo />
            </SvgIcon>
          );
          break
        default:
          return <> </>;
          break;
      }
}

const Menu = ({classes, theme, open, closeAction, anchorRef}) => {
  return (
    <SelectionMenu
        open={open}
        closeAction={closeAction}
        placement={'bottom-end'}
        anchorRef={anchorRef}
        style={{width: 250}}
        list={
        <List>  
            {menuData.map((item,index)=>{
                return(
                    <ListItem 
                        key={index}
                        button
                        disableRipple={true}
                        classes={{selected: classes.menuItemSelected}}
                        onClick={item.func} 
                        >
                        {getIcons(classes,item.type,theme)}
                        <ListItemText
                            primary={item.description}
                            classes={{
                                primary: classes.menuText,
                            }}
                        />
                    </ListItem>
                )
            })} 
        </List>
        }
    />
  )
}

export default compose(withStyles(menuStyles, { withTheme: true }))(Menu);
  