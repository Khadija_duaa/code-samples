import { Badge } from "@material-ui/core";
import ListItem from "@material-ui/core/ListItem";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import MuteIcon from "@material-ui/icons/VolumeOff";
import clsx from "clsx";
import moment from "moment";
import React, { memo, useContext, useState } from "react";
import CustomAvatar from "../../../../components/Avatar/Avatar";
import collabContext from "../../Context/collaboration.context";
import { dispatchSelectedMsg } from "../../Context/actions";
import CollaborationContext from "../../Context/collaboration.context";
import cardStyles from "./card.style";
import { getMenuIcons } from "../../../Sidebar/constants";
import MoreActionDropDown from "../../GroupList/GroupListView/moreActionDropdown/MoreActionDropDown";
import List from "@material-ui/core/List";
import Loader from "../../../../components/Loader/Loader";
import ReactHtmlParser from 'react-html-parser';
import { useSelector, useDispatch } from "react-redux";
import { getChatTime } from '../../../../utils/convertTime';
import { createChat, getGroupMessages } from '../../../../redux/actions/collaboration';
import "./card.css";
import CombinedAvatar from "../../../../components/CombinedAvatar/CombinedAvatarView";

const Card = ({ itemProps, list, classes, theme, isNewConCardEnabled, loader,
  loggedInUser, showSnackBar, updatedList }) => {

  const [isCursorOnGroupCard, setIsCursorOnGroupCard] = useState(false);
  const [isGroupCardClicked, setIsGroupCardClicked] = useState(false);
  let stateContext = useContext(collabContext);
  const dispatch = useDispatch();

  // let userState = useSelector(state => {
  //   return {
  //     profileState: state.profile.data,
  //   };
  // }, shallowEqual);

  // let loggedInUser = profileState && profileState.userId;

  const state = useSelector(state => {
    return {
      group: state && state.collab && state.collab.group && state.collab.group,
    };
  });
  const { key, index, style } = itemProps;

  const onMouseHover = () => {
    setIsCursorOnGroupCard(true)
  }

  const onMouseLeave = () => {
    setIsCursorOnGroupCard(false)
  }

  const onClickCard = () => {
    let _list = [...list];
    if (item.groupName !== "New Conversation") {
      if (_list && _list.length > 0) {
        for (let ind = 0; ind < _list.length; ind++) {
          if (item.groupId === list[ind].groupId) {
            _list[ind].isChatCardClicked = true;

          }
          else {
            _list[ind].isChatCardClicked = false;
          }

        }
      }
      updatedList(_list);
      stateContext.updateIsNewChatEnabled(false);
      getGroupMessages(item, dispatch, () => {
        stateContext.updateIsNewChatEnabled(false);
        if (list[0].groupName === "New Conversation") {
          list.shift();
          updatedList(list)
        }
      }, error => {
        if (error) {
          showSnackBar("Server throws error", "error");
        }
      })
    }
  }

  const onClickDeleteIcon = () => {
    stateContext.updateIsNewChatEnabled(false);
    setIsGroupCardClicked(false);
    list.shift();
    let selListItem;
    if (list && list.length > 0) {
      for (let ind = 0; ind < list.length; ind++) {
        if (list[ind].isChatCardClicked === true) {
          selListItem = list[ind]
        }
      }
    }
    getGroupMessages(selListItem, dispatch, () => {

    }, error => {
      if (error) {
        showSnackBar("Server throws error", "error");
      }
    })
    updatedList(list)

  }

  // handle Message select
  const handleMessageSelect = (event, message) => {
  };

  const getFirstName = () => {
    let _fullName = item && item.userMessages &&
      item.userMessages[0].personalProfiles && item.userMessages[0].personalProfiles[0]
      && item.userMessages[0].personalProfiles[0].fullName;
    return `${_fullName.substring(0, _fullName.indexOf(' '))}: `;
  }


  const item = list[index];

  let profileData = [];
  let _profileArr = [];
  let _sortedData = [];
  if (item && item.personalProfiles && item.personalProfiles.length > 0) {
    for (let ind = 0; ind < item.personalProfiles.length; ind++) {
      profileData.push(
        {
          profilePicUrl: item.personalProfiles[ind].pictureUrl === null ||
            item.personalProfiles[ind].pictureUrl === "" ? '' :
            'https://d1yxkj8tr89l78.cloudfront.net/' + item.personalProfiles[ind].pictureUrl,
          initial: item.personalProfiles[ind].fullName,
          userId: item.personalProfiles[ind].userId
        })
    }
    let _filteredData = profileData.filter((data) => data.userId !== loggedInUser)
    _sortedData = _filteredData.sort((a, b) => a.initial.localeCompare(b.initial))
    if (item && item.groupType && item.groupType === 'customgroup') {
      _profileArr = _sortedData.slice(0, 2)
    }
  }


  let _text = "New Conversation";
  let _length = item && item.groupMembers && item.groupMembers.length;

  let selName = "";
  if (item && item.personalProfiles && item.personalProfiles.length > 0) {
    for (let i = 0; i < item.personalProfiles.length; i++) {
      if (item.personalProfiles[i].userId === loggedInUser) {

      }
      else {
        selName = item.personalProfiles[i].fullName
      }
    }
  }


  let _groupName = _length === 2 ? selName : _length < 2 || item.groupName === "New Conversation" || item.isRoomNameChanged ? item && item.groupName :
    item && item.groupName && item.groupName.concat(`, +${_length - 2}`)

  let lastMessageFromName = item && item.userMessages &&
    item.userMessages[0] && item.userMessages[0] ? item && item.userMessages &&
      item.userMessages[0] && item.userMessages[0].createdBy !== loggedInUser ?
    getFirstName(item) : "You: " : " ";


  let lastMessageFrom = item && item.userMessages && item.userMessages[0] &&
    item.userMessages[0].message &&
    item.userMessages[0].message;

  let createdDateChat = item && item.userMessages && item.userMessages[0] &&
    item.userMessages[0].createdDate &&
    item.userMessages[0].createdDate;

  const lastMessageTime = createdDateChat ? getChatTime(createdDateChat) : ".";

  let _lastMessageFrom = ReactHtmlParser(lastMessageFrom, {
    transform: (node, index) => {
      if (node.type === 'text') {
        const subMessage = node.data.substring(0, 35);
        node.data = subMessage;
        // const wordsArray = node.data.split(' ');
        // const firstSixWords = wordsArray.slice(0, 6);
        // node.data = firstSixWords.join(' ');
      }
    }
  });

  return (
    <>
      {item && index >= 0 && (
        <div
          style={style}
          className={item.isChatCardClicked ? classes.messageItemCntClicked : classes.messageItemCnt}
          onMouseEnter={onMouseHover}
          onMouseLeave={onMouseLeave}
          onClick={onClickCard}
        >
          <ListItem
            key={key}
            onClick={(event) => handleMessageSelect(event, item)}
            className={classes.messageItemWrapper}
          >
            <div
              className={clsx({
                [classes.messageItem]: true,

              })}
            >
              {
                item && item.groupType && item.groupType === 'customgroup' ?
                  <CombinedAvatar profileData={_profileArr} />
                  :
                  item && item.groupType && item.groupType === 'privateChat' ?
                    <CustomAvatar otherMember={{
                      imageUrl:_sortedData &&  _sortedData[0].profilePicUrl,
                    }} hideBadge={true} size="avatarxmedium" styles={{ fontSize: '14px' }} /> :
                    <CustomAvatar personal hideBadge={true} size="avatarxmedium" styles={{ fontSize: '14px' }} />


              }
              <div className={classes.messageItemContentCnt}>
                <div className={classes.lastMessageFromCnt}>
                  <Typography
                    variant="body1"
                    className={clsx({
                      [classes.messageFromName]: true,
                    })}>
                    {_groupName}
                  </Typography>
                  <span className={classes.messageActionDropdownCnt}>
                    {item && item.groupName === _text ?
                      <div onClick={onClickDeleteIcon}>{getMenuIcons("delete", classes)}</div> :
                      <MoreActionDropDown iconColor={isCursorOnGroupCard ? theme.palette.secondary.medDark : "transparent"} />}
                  </span>
                </div>
                {item && item.groupName !== _text && <div className={classes.messageCont}>
                  <div
                    className="lastMessageFrom"
                    style={{ display: "flex" }}
                  >
                    <p>{lastMessageFrom && lastMessageFromName}</p>
                    {_lastMessageFrom}
                  </div>
                  <Typography variant="body2" className={lastMessageTime === "." ?
                    classes.lastMessageNoTime : classes.lastMessageTime}>

                    {lastMessageTime}
                  </Typography>
                </div>}
              </div>
            </div>
          </ListItem>
        </div>
      )}
    </>
  );
}

export default withStyles(cardStyles, { withTheme: true })(memo(Card));
