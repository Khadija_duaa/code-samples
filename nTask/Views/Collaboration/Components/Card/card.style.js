const cardStyles = (theme) => ({
  messageItemCnt: {
    width: "94% !important",
    margin: "0 0 0 8px",
    padding: '0px 10px 0px 10px',
    display: "flex",
    alignItems: "center",
    borderRadius: 6,
    "&:hover": {
      background: "#e6f7fd",
    },
  },

  messageItemCntClicked: {
    width: "94% !important",
    margin: "0 0 0 8px",
    padding: '0px 10px 0px 10px',
    display: "flex",
    alignItems: "center",
    borderRadius: 6,
    background:  "#e6f7fd" 
  },


  messageItemCntMain: {
    padding: '0px 10px 0px 10px',
  },
  messageCont: {
    display: "flex",
    justifyContent: "space-between",
  },
  messageItemWrapper: {
    // borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    padding: '0 0',
    display: 'block',
  },
  messageItem: {
    position: 'relative',
    // background: theme.palette.common.white,
    padding: "0px",
    display: "flex",
    borderRadius: 6,
    alignItems: 'center',
    cursor: "pointer",
    "&:hover $messageActionDropdownCnt": {
      display: "inline-block",
    },
    "&:hover $lastMessageTime": {
      // display: "none",
    },
  },
  messageItemContentCnt: {
    marginLeft: "8px",
    flex: 1,
    "& $lastMessageTime": {
      display: "flex",
      alignItems: "center",
      fontFamily: "Lato",
      color: "#646464",
      marginTop: '2px',
      fontSize: "12px",
      lineHeight: "15px",
      letterSpacing: "0px",
      opacity: 1
    },
    "& $lastMessageNoTime": {
      display: "flex",
      alignItems: "center",
      fontFamily: "Lato",
      color: "white",
      marginTop: '2px',
      fontSize: "1px",
      lineHeight: "15px",
      letterSpacing: "0px",
      opacity: 1
    },
  },
  lastMessageFromCnt: {
    display: "flex",
    justifyContent: "space-between",
    "& $messageFromName": {
      fontWeight: 800,
      textAlign: "left",
      fontSize: "14px",
      // lineHeight: "22px",
      fontFamily: "Lato",
      letterSpacing: "0px",
      color: "#161717",
      opacity: 1,
      // paddingLeft: "4px",
    },
  },
  messageFromName: {},
  lastMessageTime: {},
  lastMessageNoTime: {},
  messageActionDropdownCnt: {
    display: "'inline-block',",
  },
  muteIcon: {
    color: theme.palette.icon.red,
    fontSize: "18px !important",
  },
  notificationBadge: {
    // background: theme.palette.common.white,
    background: "#FD4F44",
    color: theme.palette.common.white,
    height: 16,
    minWidth: 16,
    borderRadius: "50%",
    top: 10,
    right: 9,
  },
  unreadMsgStyle: {
    fontWeight: 700,
    color: theme.palette.text.primary,
  },
  moreActionDropDownIcon: {
    color: 'red',
  },
  messageItemSelected: {
    "& $lastMessageTime, & $lastMessageFrom, & $messageFromName": {
      // color: theme.palette.common.white,
    },
  },
  multiUserAvatar: {
    width: 36,
    height: 36,
    display: "flex",
    alignItems: "center",
    borderRadius: "50%",
    overflow: "hidden",
  },
  leftAvatar: {
    width: 18,
    height: 36,
    background: "#F8E7E7",
    color: "#D91F1F",
    fontFamily: theme.typography.fontFamilyLato,
    fontSize: "13px !important",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginRight: 1,
  },
  rightAvatar: {
    width: 18,
    height: 36,
    background: "#F8F4E7",
    color: "#D86C0E",
    fontFamily: theme.typography.fontFamilyLato,
    fontSize: "13px !important",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  newMessageIcon: {
    fontSize: "20px !important",
    color: theme.palette.common.white,
  },
});

export default cardStyles;
