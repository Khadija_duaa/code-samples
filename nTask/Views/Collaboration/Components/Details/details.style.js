const detailsStyles = theme => ({
    addIcon: {
      color:'#00B781',
      fontSize:'18px',
    },
    addBtn:{
      display:'flex',
      fontSize:'14px',
      color:'#00B781',
      textDecoration: 'underline',
      alignItems:'center',
      cursor:'pointer',
    },
    drawer: {
      position: "absolute",
      top: 0,
      right: 0,
      height: '100%',
      width: 0,
      minWidth: '0 !important',
    },
    mainDetailsFeaturesCnt:{
      padding:'0 15px'
    },
    featuresCnt:{
      display:'flex',
      flexDirection: 'row',
      justifyContent:'space-between',
      padding:'20px 5px',
      borderBottom: `1px solid ${theme.palette.border.grayLighter}`,
    },
    featuresContent:{
      display:'flex',
      flexDirection:'column',
      alignItems: 'center',
      width:'30%',
      textAlign: 'center',
    },
    circle:{
      position:'relative',
      width:'44px',
      height:'44px',
      border: `1px solid ${theme.palette.border.grayLighter}`,
      borderRadius:'50%',
      marginBottom:'6px',
    },
    icon: {
      position:'absolute',
      top:13,
      left:13,
      marginRight: 5,
      fontSize: 18,
      color:'#6F7374',
    },
    participantsCnt:{
      display:'flex',
      flexDirection:'column',
    },
    participantscontent:{
      textAlign: 'center',
      fontSize:'16px',
    },
    participantsHeading:{
      display:'flex',
      flexDirection: 'row',
      justifyContent:'space-between',
      padding:'20px 0px 15px 0px',
    },
    itemText: {
      fontSize: 13,
      color: theme.palette.text.primary,
      fontWeight: theme.typography.fontWeightLight,
      // width: 134,
      overflow: "hidden",
      whiteSpace: "nowrap",
      lineHeight: "normal",
      textOverflow: "ellipsis"
    },
    itemTextRoot:{
      padding:'0 10px',
    },
    removeAssigneeIcon: {
      fontSize: 18,
      cursor: "pointer",
      display: "none",
    },
    menuItemavatar: {
      padding: "2px 0px 2px 0px",
      marginBottom: 2,
      "&:hover $removeAssigneeIcon": {
        display: "inline-block",
      },
    },
    addParticipantCnt: {
      padding: "14px 15px",
      "& *": {
        fontFamily: theme.typography.fontFamilyLato,
      },
      "& button": {
        maxHeight: 32,
      },
    },
    addPartActionBtnCnt: {
      display: "flex",
      justifyContent: "flex-end",
    },

  });
  
  export default detailsStyles;
  