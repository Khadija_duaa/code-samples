import detailsStyles from "./details.style";
import React, { useState, useRef } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import Typography from "@material-ui/core/Typography";
import SelectSearchDropdown from "../../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { generateAssigneeData } from "../../../../helper/generateSelectData";
import CustomButton from "../../../../components/Buttons/CustomButton";
import InfoIcon from "@material-ui/icons/Info";
import CustomDrawer from "../../../../components/Drawer/CustomDrawer";
import CustomAvatar from "../../../../components/Avatar/Avatar";
import AddIcon from "@material-ui/icons/Add";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import RemoveIcon from "@material-ui/icons/Close";
import DropdownMenu from "../../../../components/Dropdown/DropdownMenu";
import DefaultCheckbox from "../../../../components/Form/Checkbox";

const Details = ({classes, theme, open, assigneeData }) => {
  const [anchorEl, setAnchorEl] = useState(null);
  
  return (
    <>
      <CustomDrawer
        open={open}
        drawerProps={{
          className: classes.drawer,
          PaperProps: {
            height: "calc(100% - 45px)",
            // boxShadow: "-4px 6px 12px #00000029",
            border: `none`,
            borderTop: `1px solid ${theme.palette.border.lightBorder}`,
            borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
            right:'0',
            left:'auto',
            top:'45px',
            position: 'absolute',
          },
        }}
        // closeDrawer={() => setOpen(false)}
        hideHeader={true}
        drawerWidth={350}
      >
        <div className={classes.mainDetailsFeaturesCnt}>
          <div className={classes.featuresCnt}>
              <Typography variant="h6" className={classes.featuresContent}>
                <span className={classes.circle}>
                  <InfoIcon className={classes.icon}/>
                </span>
                  Pin to Top
              </Typography>
              <Typography variant="h6" className={classes.featuresContent}>
                <span className={classes.circle}>
                  <InfoIcon className={classes.icon}/>
                </span>
                  Mute Conversation
              </Typography>
              <Typography variant="h6" className={classes.featuresContent}>
                <span className={classes.circle}>
                  <InfoIcon className={classes.icon}/>
                </span>
                  Leave Conversation
              </Typography>
          </div>
          <div className={classes.participantsCnt}>
            <div className={classes.participantsHeading}>
              <Typography variant="h4" className={classes.participantscontent}>
                  Participants
              </Typography>

              <span className={classes.addBtn} 
                onClick={({ currentTarget }) => setAnchorEl(currentTarget)} 
                buttonRef={anchorEl}
              >
                <AddIcon className={classes.addIcon}/>
                Add
              </span>
            </div>

            <ListItem 
            // key={i}
              className={classes.menuItemavatar}
            >
              <CustomAvatar
                // otherMember={{
                //   imageUrl: member.imageUrl,
                //   fullName: member.fullName,
                //   lastName: "",
                //   email: member.email,
                //   isOnline: member.isOnline,
                //   isOwner: member.isOwner,
                // }}
                personal
                size="small"
              />
              <ListItemText
                primary={
                  <>
                    {/* {member.fullName} */}
                    John Wick
                    {/* <br />
                    {member.isActiveMember === null ? (
                      <Typography variant="caption">
                        {" "}
                        <FormattedMessage
                          id="common.action.invite.label"
                          defaultMessage="Invite sent"
                        />
                      </Typography>
                    ) : null} */}
                  </>
                }
                classes={{ 
                    primary: classes.itemText,
                    root: classes.itemTextRoot
                  }}
              />
              {/* {minOneSelection && assignedTo.length < 2 ? null : deletePermission ? ( */}
                <RemoveIcon
                  className={classes.removeAssigneeIcon}
                  nativeColor={theme.palette.secondary.dark}
                  // onClick={event => this.handleRemoveAssignee(member)}
                />
              {/* ) : null} */}
            </ListItem>
          </div>
        </div>
      </CustomDrawer>
      
      <DropdownMenu
        open={!!anchorEl}
        closeAction={() => setAnchorEl(null)}
        anchorEl={anchorEl}
        placement="bottom-end"
        style={{
          width: 389,
        }}
        id="addparticipant"
        >
          <div className={classes.addParticipantCnt}>
            <SelectSearchDropdown
              data={() => generateAssigneeData(assigneeData)}
              label={
                'Add Participant(s)'
              }
              // label={
              //   <FormattedMessage id="common.assigned.label" defaultMessage="Participant(s)" />
              // }
              // selectChange={this.handleFilterChange}
              // selectedValue={assignees}
              styles={{ marginBottom: 0}}
              placeholder={'Search'}
              avatar
            />
            <DefaultCheckbox
              checkboxStyles={{ padding: "5px 5px 6px 0", color:"#00CC90" }}
              styles={{ alignItems: "center" }}
              // checked={}
              // onChange={}
              fontSize={20}
              color="#00CC90"
              labelStyle={{color:'black', fontSize:13}}
              label="Share all chat history"
            />
            <div className={classes.addPartActionBtnCnt}>
              <CustomButton variant="contained" btnType="gray" style={{ marginRight: 10 }}>
                Cancel
              </CustomButton>
              <CustomButton variant="contained" btnType="green" >
                Add
              </CustomButton>
            </div>
          </div>
      </DropdownMenu>
    </>
  )
}

export default compose(withStyles(detailsStyles, { withTheme: true }))(Details);