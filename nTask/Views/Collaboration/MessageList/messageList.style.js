const messageListStyles = (theme) => ({
  chatCon: {
    position: "absolute",
    left: 0,
    bottom: "105px", //17vh before //UI fix - chatbox positioning
    width: "100%",
    padding: "0 2vw"
  },
  dividerBottom: {
    marginBottom: "6px"
  },
  messageListCnt: {
    minHeight: "100vh",
    height: "100vh",
    maxHeight: "100vh",
    position: "relative",
    background: "#f9f9f9",
  },
  headerCont: {
    background: theme.palette.common.white,
    height: "40%"
  },
  messageListDetailsContentCnt: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    // padding: "0 30px 10px 62px",
    // padding: "0 40px 10px 40px",
    // background: theme.palette.background.airy,
  },
  messageListDetailsCnt: {
    flex: 1,
    // padding: "0 40px 10px 40px",
  },
  messageTextEditorCnt: {
    marginBottom: '10px',
    // boxShadow: '0px 2px 8px #0000000F',
    // border: '1px solid #EAEAEA',
    padding: "0 40px 10px 40px",
    borderRadius: '4px',
    zIndex: 0,
    // background: theme.palette.background.airy,
  },
});

export default messageListStyles;
