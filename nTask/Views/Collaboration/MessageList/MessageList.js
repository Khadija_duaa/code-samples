import React, { useState, useContext, useEffect, useRef } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import messageListStyles from "./messageList.style";
import Grid from "@material-ui/core/Grid";
import Divider from "@material-ui/core/Divider";
import MessageListHeader from "./MessageListHeader/MessageListHeader";
import collabContext from "../Context/collaboration.context";
import MessageListDetails from "./MessageListDetails/MessageListDetails";
import ChatTextEditor from "../../../components/TextEditor/CustomTextEditor/ChatTextEditor/ChatTextEditor";
import Scrollbars from "react-custom-scrollbars";
import { useSelector, shallowEqual } from "react-redux";
import "./messageListStyle.css"


function MessageList({ classes, showSnackBar, updatedList, groupChatList }) {

  let listRef = useRef();
  let stateContext = useContext(collabContext);
  const state = useSelector(state => {
    return {
      profileState: state.profile.data,
      groupName: state && state.collab && state.collab.group && state.collab.group.groupName,
      userMessages: state && state.collab && state.collab.group && state.collab.group &&
        state.collab.group._userMessage && state.collab.group._userMessage,
    };
  }, shallowEqual);

  const { profileState } = state;

  const [isGroupNameCardEnabled, setGroupNameCardEnabled] = useState(false);

  useEffect(() => {
    if (state.groupName !== undefined) {
      setGroupNameCardEnabled(true)
    }
  }, [state.groupName]);

  useEffect(() => {
    if (state.userMessages !== undefined) {
      stateContext.handleSetMessageList(state.userMessages)
    }
  }, [state.userMessages]);

  useEffect(() => {
    if (listRef.current) {
      listRef.current.scrollToBottom();
    }

    return () => { };
  }, [stateContext.messageList]);


  const setMessage = (data) => {
    stateContext.handleSetMessageList(data);
  };

  return (
    <Grid container className={classes.messageListCnt} direction="row" style={{ height: '100%' }}>
      {stateContext.isNewChatEnabled || isGroupNameCardEnabled ?
        <Grid item xs={12}>
          <MessageListHeader showSnackBar={showSnackBar}
            updatedList={updatedList}
            groupChatList={groupChatList}
          />

        </Grid>
        : null}
      <Grid item xs={12}>
        {(!stateContext.isNewChatEnabled || stateContext.messageList) &&
          <Scrollbars
            ref={node => {
              listRef.current = node;
            }}
            autoHide={true}
            autoHeight={false}
            // autoHeightMin={320}
            // autoHeightMax={320}
            // onScroll={handleScroll}
            className="scrollConMess"
            style={{

            }}

          >
            <MessageListDetails messageList={stateContext && stateContext.messageList} profileState={profileState} />
          </Scrollbars>}
      </Grid>
      <Grid item xs={12} className={classes.chatCon}>
        <Divider className={classes.dividerBottom} />
        <ChatTextEditor
          updatedList={updatedList}
          groupChatList={groupChatList}
          setMessage={setMessage}
          messageList={stateContext && stateContext.messageList}
          showSnackBar={showSnackBar}
          stateContext={stateContext}
          profileState={profileState}
          id='collaborationEditorToolbar'
        />
      </Grid>
    </Grid>
  );
}

export default compose(withStyles(messageListStyles, { withTheme: true }))(MessageList);
