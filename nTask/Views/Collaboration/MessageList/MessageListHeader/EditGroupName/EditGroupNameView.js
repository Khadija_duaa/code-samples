import React, { useState } from "react";
import Dialog from "@material-ui/core/Dialog";
import { compose } from "redux";
import { useSelector, useDispatch, shallowEqual } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { DialogContent, Grid, OutlinedInput, Typography } from "@material-ui/core";
import EditGroupNameStyles from "./EditGroupName.style";
import PropTypes from "prop-types";
import { updateChatRoom, createChat } from '../../../../../redux/actions/collaboration';
import CustomButton from "../../../../../components/Buttons/CustomButton";



const EditGroupNameDialog = (props) => {

    const { classes, theme, open, onClose, showSnackBar, updatedList, groupChatList } = props;
    const dispatch = useDispatch();

    const state = useSelector(state => {
        return {
            group: state && state.collab && state.collab.group && state.collab.group,
            groupId: state && state.collab && state.collab.group && state.collab.group.groupId
        };
    }, shallowEqual);

    const [groupName, setGroupName] = useState("");

    const onClickSaveChanges = () => {
        dispatch(updateChatRoom(groupName, state.groupId, (res) => {
            let _tempGroup = state.group;
            _tempGroup.groupName = groupName
            createChat(_tempGroup);
            let _groupChatList = groupChatList;
            if (_groupChatList && _groupChatList.length > 0) {
                for (let ind = 0; ind < _groupChatList.length; ind++) {
                    if (_groupChatList[ind].groupId === _tempGroup.groupId) {
                        _groupChatList[ind].isRoomNameChanged = true
                    }
                }
            }

            updatedList(_groupChatList)
            showSnackBar(res.data.message, "success");
        }, (error) => {
            if (error) {
                showSnackBar("Server throws error", "error");
            }
        }))
    }

    const onChange = (event) => {
        const value = event.currentTarget.value;
        setGroupName(value)
    }

    return (
        <Dialog open={open} onClose={onClose}
            scroll="body"
            // disableBackdropClick={true}
            // disableEscapeKeyDown={true}
            classes={{
                paper: classes.dialogPaperCnt,
                scrollBody: classes.dialogCnt,
                root: classes.root
            }}>
            <DialogContent>
                <Grid container alignItems="center" justifyContent="center" >
                    <Grid item sm={12}>
                        <Typography variant="body2" className={classes.heading}>Group Name</Typography>
                    </Grid>
                    <Grid item sm={12} className={classes.inputGrid}>
                        <OutlinedInput
                            classes={{
                                root: classes.rootInput,
                                input: classes.outlinedInput,
                                notchedOutline: classes.notchedOutline,
                                focused: classes.focused,
                            }}
                            notched={true}
                            autoFocus={true}
                            id="outlined-full-width"
                            onChange={onChange}
                            value={groupName}
                            placeholder="Enter group name"
                            fullWidth
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            variant="outlined"
                        />
                    </Grid>
                    <Grid container item sm={12} justifyContent='flex-end'>
                        <CustomButton variant="contained" className={classes.cancelBtn} btnType="gray" onClick={onClose}>
                            Cancel
                        </CustomButton>
                        <CustomButton variant="contained" disabled={!groupName} className={classes.saveBtn} btnType={"green"} onClick={onClickSaveChanges}>
                            Save
                        </CustomButton>
                    </Grid>
                </Grid>
            </DialogContent>
        </Dialog >
    );
}

EditGroupNameDialog.propTypes = {
    onClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
};

export default compose(withStyles(EditGroupNameStyles, { withTheme: true }))(EditGroupNameDialog);