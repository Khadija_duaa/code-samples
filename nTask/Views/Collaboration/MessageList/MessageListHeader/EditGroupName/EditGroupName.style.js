const EditGroupNameStyles = (theme) => ({
    dialogPaperCnt: {
        overflowY: "visible",
        background: theme.palette.common.white,
        width: "480px !important",
        borderRadius: 6,
        paddingBottom: '18px',
        position: "fixed",
        top: '8%',
        m: 0
    },
    dialogCnt: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
    },
    rootInput: {
        "& $notchedOutline": {
            borderColor: '#D0D0D0'
        },
        "&:hover $notchedOutline": {
            borderColor: theme.palette.primary.main
        },
        "&$focused $notchedOutline": {
            borderWidth: '1px',
            borderColor: theme.palette.primary.main
        }
    },
    outlinedInput: {
        padding: '12px',
        color: 'inherit',
        fontFamily: theme.typography.fontFamilyLato,
        fontWeight: 500,
        fontSize: '13px !important',
    },
    notchedOutline: {},
    focused: {},
    inputGrid: {
        marginTop: '8px',
        marginBottom: '16px'
    },
    heading: {
        color: '#595C5D',
        fontFamily: theme.typography.fontFamilyLato,
        fontWeight: 600,
        fontSize: '13px !important',
    },
    saveBtn: {
        width: '78px'
    },
    cancelBtn: {
        width: '78px',
        marginRight: '10px'
    }
});

export default EditGroupNameStyles;
