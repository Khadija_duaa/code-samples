import React, { useContext, useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import messageListHeaderStyle from "./messageListHeader.style";
import SelectSearchDropdown from "../../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { generateAssigneeData } from "../../../../helper/generateSelectData";
import { compose } from "redux";
import { useSelector, useDispatch, shallowEqual } from "react-redux";
import Typography from "@material-ui/core/Typography";
import { updateChatRoom } from '../../../../redux/actions/collaboration';
import collabContext from "../../Context/collaboration.context";
import { SvgIcon } from "@material-ui/core";
import EditNameIcon from "../../../../components/Icons/CollaborationIcons/EditNameIcon";
import EditGroupNameView from "./EditGroupName/EditGroupNameView";

const MessageListHeader = ({ classes, showSnackBar, updatedList, groupChatList  }) => {

  const dispatch = useDispatch();
  let stateContext = useContext(collabContext);

  const [groupName, setGroupName] = useState("");
  const [openEditDialog, setEditOpenDialog] = useState(false);
  let _text = "New Conversation";

  const state = useSelector(state => {
    return {
      profileState: state && state.profile && state.profile.data,
      groupName: state && state.collab && state.collab.group && state.collab.group.groupName,
      groupType: state && state.collab && state.collab.group && state.collab.group.groupType,
      groupId: state && state.collab && state.collab.group && state.collab.group.groupId
    };
  }, shallowEqual);


  useEffect(() => {
    if (stateContext.isNewChatEnabled) {
      setGroupName(_text)
    }
    else {
      setGroupName(state.groupName)
    }
  }, [state.groupName, stateContext.isNewChatEnabled]);



  const handleFilterChange = (value, data, profileState) => {
    let _participantList = [];
    data && data.map((val, ind) => {
      _participantList.push(val)
    })

    stateContext.getSelectedParticipantList(_participantList);
  }

  const handleCancelEditDialog = () => {
    setEditOpenDialog(false);
  };

  const onClickEditIcon = () => {
    setEditOpenDialog(true);
  }

  const { profileState } = state;
  const assigneeData = profileState && profileState.teamMember;
  return (
    <Grid container className={classes.headerCont}>
      <Grid item xs={12} className={classes.headerContItem} alignItems="center">
        <Grid container alignItems="center" spacing={1}>
          <Grid item>
            <Typography variant="h3" classes={{ h3: classes.topHeading }}>
              {groupName}
            </Typography>
          </Grid>
          <Grid item>

            {(groupName !== _text && state.groupType === "customgroup") && <SvgIcon viewBox="0 0 16 15.54" className={classes.editNameIcon} onClick={onClickEditIcon}>
              <EditNameIcon />
            </SvgIcon>}
          </Grid>
        </Grid>
      </Grid>


      {groupName === _text && <Grid item xs={12} className={classes.headerContItemSearch}>
        <SelectSearchDropdown
          customStyles={{
            control: {
              border: "none",
              fontSize: '14px',
            }
          }}
          placeholder={"Enter name or email address"}
          data={() => generateAssigneeData(assigneeData)}
          selectChange={(value, data) => handleFilterChange(value, data, profileState)}
          // selectedValue={assignees}
          styles={{ marginBottom: 3, marginTop: 3 }}
          isCollabEnabled={true}
          avatar
        />
      </Grid>}

      {openEditDialog && <EditGroupNameView
        updatedList={updatedList}
        groupChatList={groupChatList}
        open={openEditDialog} onClose={handleCancelEditDialog} showSnackBar={showSnackBar} />}
    </Grid>
  );
}

export default compose(withStyles(messageListHeaderStyle, { withTheme: true }))(
  MessageListHeader
);

