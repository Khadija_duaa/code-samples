const messageListHeaderStyle = theme => ({
  messageListHeaderCnt: {
    padding: "6px 8px",
    height: 45,
    background: theme.palette.common.white,
    // border: `1px solid ${theme.palette.border.grayLighter}`,
    borderLeft: "none",
    borderBottom: "none",
    borderRadius: "0 20px 0 0",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    zIndex: 1,
    boxShadow: "0px 2px 0px rgba(234, 234, 234, 1)",
    "& input": {
      fontFamily: theme.typography.fontFamilyLato,
    },
  },
  headerCont: {
    background: theme.palette.common.white,
  },
  headerContItem: {
    marginTop: '4px',
    padding: "12px 10px",
    borderBottom: `1px solid ${theme.palette.background.medium}`,
    alignItems: 'center',
  },
  headerContItemSearch: {
    borderBottom: `1px solid ${theme.palette.background.medium}`,
  },
  topHeading: {
    fontSize: "17px",
    fontWeight: 700,
  },
  messageListHeaderLeft: {
    // width: '25%',
    display: "flex",
    alignItems: "center",
    "& h2": {
      marginLeft: 6,
      fontFamily: theme.typography.fontFamilyLato,
    },
    "& svg": {
      fontSize: 14,
    },
  },
  messageListHeaderRight: {
    display: "flex",
    alignItems: "center",
  },

  buttonsCnt: {
    display: "flex",
    paddingRight: 8,
    // borderRight: `1px solid ${theme.palette.border.lightBorder}`,
  },
  startMeetingIcon: {
    fontSize: 16,
    // color: theme.palette.common.white,
    color: '#6F7374',
    marginRight: '8px',
  },
  startMeetingButton: {
    borderRadius: '6px',
    color: theme.palette.common.black,
    marginRight: 10,
    padding: '5px 14px',
    fontSize: '13px',
    alignItems: 'center',
  },
  detailsButton: {
    borderRadius: '6px',
    color: theme.palette.common.black,
    padding: '5px 14px',
    fontSize: '13px',
    alignItems: 'center',
  },
  infoIcon: {
    marginRight: 5,
    fontSize: 18,
    color: '#6F7374',
  },

  TabsRoot: {
    // background: theme.palette.background.airy,
    // borderBottom: `1px solid #F6F6F6`,
    // boxShadow: `0px 1px 0px ${theme.palette.background.grayLighter}`,
    position: 'relative',
    top: '3px',
    padding: "0px 25px 0px 0px",
    minHeight: 40,
    width: '160px',
  },
  tabIndicator: {
    // background: theme.palette.border.blue,
    background: '#00CC90',
    borderRadius: "10px 10px 0px 0px",
    height: 3,
  },
  tab: {
    minWidth: "unset",
    minHeight: 40,
  },
  tabLabel: {
    fontSize: 13,
    marginBottom: 6,
  },
  tabLabelCnt: {
    padding: 0,
    fontSize: 12,
    textTransform: "capitalize",
    "& $h6": {
      // fontWeight: theme.typography.fontWeightMedium,
      fontWeight: theme.typography.fontWeightRegular,
    },
  },
  tabSelected: {
    color: theme.palette.common.black,
    "& $h6": {
      color: theme.palette.border.black,
      fontWeight: theme.typography.fontWeightLarge,
    },
  },
  editNameIcon: {
    height: '16px',
    width: '16px',
    cursor: "pointer"
  }

});

export default messageListHeaderStyle;
