const messageListDetailsStyles = theme => ({
    messageListDetails: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
        width: '100%' //check    
    },
    messageListTop: {
        display: 'block',
        alignItems: 'center',
        margin: '20px 31px 10px 31px'
    },
    dividerLeft: {
        marginLeft: 13,
        // flex: 4    
    },
    dividerRight: {
        marginRight: 13,
        // flex: 1    
    },
    messageSenderCon: {
        marginTop: '10px',
    },
    messageSenderBackground: {
        display: 'flex',
        justifyContent: 'flex-end',
        marginRight: '24px'
    },
    messageReceiverBackground: {
        display: 'flex',
        justifyContent: 'flex-start',
        marginLeft: '24px'
    },
    messageSenderMessCon: {
        overflowWrap: 'anywhere',
        padding: '0px',
        // margin: '4px 0px 11px 0px',
    },
    messageSenderInnerDivCon: {
        background: '#ffff',
        width: '100%',
        padding: '11px',
        borderRadius: '6px',
        maxWidth: '97%',
        minWidth: '97%',
        height: 'auto',
        minHeight: 'auto',
        border: '1px solid #e8e8e8'
    },
    messageReceiverIconCon: {
        padding: '5px 5px 0px 7px'
    },
    messageReceiverTimeNameCon: {
        padding: '0px 0px 3px 0px',
        fontSize: '12px'
    },
    messageReceiverNameCon: {
        marginRight: '14px',
        fontWeight: '800',
        margin: '11px 0px 0px 0px',
    },
    messageReceiverTimeCon: {
        color: '#595C5D'
    },
    messageReceiverInnerDivCon: {
        display: 'flex',
        flexDirection: 'column',
        background: '#ffffff',
        width: '100%',
        // width: 'fit-content',        
        padding: '10px',
        borderRadius: '6px',
        marginTop: '5px',
        border: '1px solid #e8e8e8',
        maxWidth: '70%',
    },
    messageSenderTimeCon: {
        color: '#595C5D',
        fontSize: '12px',
        padding: '0px 0px 3px 0px'
    }
});
export default messageListDetailsStyles;