import React, { useState, useContext, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import messageListDetailsStyles from "./messageListDetails.style";
import MessageSender from "./MessageSender";
import ReactHtmlParser from 'react-html-parser';
import Grid from "@material-ui/core/Grid";
import moment from "moment";
import MessageReceiver from "./MessageReceiver";
import Divider from "@material-ui/core/Divider";
import { Typography } from "@material-ui/core";
const MessageListDetails = ({ classes, theme, messageList, profileState }) => {

  const [newMessageList, setNewMessageList] = useState([]);

  useEffect(() => {
    setNewMessageList(messageList);
  }, [messageList]);
  let chatDetailData = [
    // {    //  name: 'John Wick',    //  time: '02:00 pm' ,    //  message: 'message'    // },  
  ]
  return (
    <Grid container >
      <Grid item xs={12} className={classes.messageListTop}>
        {newMessageList && newMessageList.map((data, ind) => {
          let _id = data.createdBy;
          if (_id === profileState.userId) {
            return (
              <MessageSender
                name={data.name}
                time={data.time}
                pictureUrl={data.imageUrl.includes('https') ? data.imageUrl : 'https://d1yxkj8tr89l78.cloudfront.net/' + data.imageUrl}
                message={ReactHtmlParser(data.message)} />
            )
          }
          else {
            return (
              <MessageReceiver
                name={data.name}
                time={data.time}
                pictureUrl={'https://d1yxkj8tr89l78.cloudfront.net/' + data.imageUrl}
                message={ReactHtmlParser(data.message)} />
            )
          }

        })}
      </Grid>
    </Grid>);
}
export default compose(withStyles(messageListDetailsStyles, { withTheme: true }))(MessageListDetails);