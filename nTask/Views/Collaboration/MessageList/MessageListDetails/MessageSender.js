import React from 'react';
import { Grid, Typography } from "@material-ui/core";
import { compose } from 'redux';
import { withStyles } from "@material-ui/core/styles";
import moment from "moment";
import CustomAvatar from "../../../../components/Avatar/Avatar";
import messageListDetailsStyles from "./messageListDetails.style";
import { DriveEtaSharp, DriveEtaTwoTone } from '@material-ui/icons';

const MessageSender = ({ classes, theme, name, time, message, pictureUrl }) => {
    return (
        <>
            <Grid container spacing={3} >

                <Grid item xs={12} style={{ display: "flex" }}>

                    <div className={classes.messageSenderInnerDivCon}>
                        <span className={classes.messageReceiverNameCon}>
                            {name}
                        </span>
                        <span className={classes.messageSenderTimeCon}>
                            {moment(time).format("LLL")}
                        </span>
                        <span classes={classes.messageSenderMessCon}>
                            {message}
                        </span>
                    </div>
                    <div className={classes.messageReceiverIconCon}>
                        <CustomAvatar
                            otherMember={{
                                imageUrl: pictureUrl,
                            }}
                            hideBadge={true}
                            size="small"
                        />
                    </div>
                </Grid>
            </Grid>
        </>
    );
}
export default compose(withStyles(messageListDetailsStyles, { withTheme: true }))(MessageSender);