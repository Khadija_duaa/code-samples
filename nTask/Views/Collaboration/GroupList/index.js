import { withStyles } from "@material-ui/core/styles";
import React, { useCallback, useContext, useEffect, useRef, useState } from "react";
import { compose } from "redux";
import Grid from "@material-ui/core/Grid";
import indexStyles from "./index.style";
import CollaborationContext from "../Context/collaboration.context";
import { HubConnectionBuilder } from '@microsoft/signalr';
import GroupListHeader from "./GroupListHeader/GroupListHeader";
import GroupListView from "./GroupListView/GroupListView";
import helper from "../../../helper/index";
import collabContext from "../Context/collaboration.context";
import { createChat } from '../../../redux/actions/collaboration';

const GroupListData = (props) => {

  const AUTH_TOKEN = helper.getToken() || null;
  let stateContext = useContext(collabContext);
  let _messages = stateContext && stateContext.messageList

  const { classes, theme, groupChatList, loggedInUser, getPayloadDataForChatGroupListing,
    loadingScroll, noData, updatedList, showSnackBar } = props;

  const [connection, setConnection] = useState(null);

  useEffect(() => {
    const newConnection = new HubConnectionBuilder()
      .withUrl('https://chat.ntaskmanager.com/chatHub', { accessTokenFactory: () => AUTH_TOKEN.substring(7) })
      .withAutomaticReconnect()
      .build();
    setConnection(newConnection);
  }, []);

  useEffect(() => {
    if (connection) {
      connection.start()
        .then(result => {
          console.log('signalR connected!');
        })
        .catch(e => {
          console.log('signalR connection failed: ', e);
        })

      connection.on('Created', (res) => {
        console.log("ress", res)
        let parsedData = JSON.parse(res);
        let _action = parsedData && parsedData.NotificationInfo && parsedData.NotificationInfo.Action;
        let _info = parsedData && parsedData.Entity && parsedData.Entity;

        if (_action === "AddedNewGroup") {
          let _groupChatList = props && props.groupChatList && props.groupChatList;
          _groupChatList.shift();
          if (_groupChatList && _groupChatList.length > 0) {
            for (let ind = 0; ind < _groupChatList.length; ind++) {
              _groupChatList[ind].isChatCardClicked = false
            }
          }
          _info.isChatCardClicked = true;
          createChat(_info)
          _groupChatList.splice(0, 0, _info);

          props && props.updatedList(_groupChatList);
        }
        if (_action === "NotifyNewMessage") {
          let _messageList = [..._messages];
          _messageList.push({
            message: _info.Message, time: _info.CreatedDate, name: "", imageUrl: "",
            createdBy: _info.CreatedBy
          });
          stateContext.handleSetMessageList(_messageList);
        }

      });
      connection.on('Updated', (res) => {
        console.log("ress", res)
        let parsedData = JSON.parse(res);
        let _action = parsedData && parsedData.NotificationInfo && parsedData.NotificationInfo.Action;
        let _groupId = parsedData && parsedData.NotificationInfo && parsedData.NotificationInfo.GroupId;
        let _name = parsedData && parsedData.Entity && parsedData.Entity;
        if (_action === "NotifyRoomNameUpdate") {
          let _groupChatList = props && props.groupChatList && props.groupChatList;
          if (_groupChatList && _groupChatList.length > 0) {
            for (let ind = 0; ind < _groupChatList.length; ind++) {
              if(_groupChatList[ind].groupId === _groupId)
              {
                _groupChatList[ind].isRoomNameChanged = true
                _groupChatList[ind].groupName =_name
              }
            }
          }  
          props && props.updatedList(_groupChatList);
        }

      });
    }
  }, [connection]);


  return (
    <div className={classes.groupListDataCnt}>
      <Grid container>
        <Grid item xs={12}>
          <Grid container>
            <Grid item xs={12}>
              <GroupListHeader updatedList={updatedList} groupChatList={groupChatList} />
            </Grid>
          </Grid>
          <Grid container>
            <Grid item xs={12}>
              <GroupListView groupChatList={groupChatList} loggedInUser={loggedInUser}
                loadingScroll={loadingScroll} updatedList={updatedList}
                noData={noData} showSnackBar={showSnackBar}
                getPayloadDataForChatGroupListing={getPayloadDataForChatGroupListing} />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      {/* } */}
    </div>
  );
}

export default compose(withStyles(indexStyles, { withTheme: true }))(GroupListData);
