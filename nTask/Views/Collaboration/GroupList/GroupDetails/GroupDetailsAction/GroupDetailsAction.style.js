const groupDetailsActionStyles = (theme) => ({

    title: {
        fontWeight: 500,
        overflow: "hidden",
        whiteSpace: "nowrap",
        textOverflow: "ellipsis",
        maxWidth: "160px",
        color: '#E3352A',
    },
})

export default groupDetailsActionStyles;