import React from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import { Divider, Grid, ListItem, SvgIcon } from "@material-ui/core";
import List from "@material-ui/core/List";
import groupDetailsActionStyles from "./GroupDetailsAction.style";


const GroupDetailsAction = (props) => {

    const { theme, classes, title, icon } = props;
    return (
        <List>
            <ListItem disableGutters={true}>
                <Grid container alignItems="center" justifyContent="flex-start" spacing={3} >
                    <Grid item>
                        <SvgIcon viewBox="0 0 18.483 18" fontSize="small">
                            {icon}
                        </SvgIcon>
                    </Grid>
                    <Grid item>
                        <Typography
                            variant="h5"
                            className={classes.title}>
                            {title}
                        </Typography>
                    </Grid>
                </Grid>
            </ListItem>
        </List >
    );
}

GroupDetailsAction.propTypes = {
    title: PropTypes.string.isRequired,
};

export default compose(withStyles(groupDetailsActionStyles, { withTheme: true }))(GroupDetailsAction);