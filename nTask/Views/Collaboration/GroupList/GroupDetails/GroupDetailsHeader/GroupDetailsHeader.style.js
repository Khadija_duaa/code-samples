const collabGroupDetailsHeaderStyles = (theme) => ({
    avatarContainer: {
        padding: '5px',
        margin: '10px auto',
    },
    avatar: {
        borderRadius: '50%',
        padding: '5px',
        margin: '10px auto',
        width: '325px'
    },
    title: {
        fontFamily: theme.typography.fontFamilyLato,
        fontSize: "22px !important",
        color: '#ffffff',
        fontWeight: 600,
        lineHeight: "normal",
    },
})

export default collabGroupDetailsHeaderStyles;