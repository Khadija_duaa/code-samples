import React from 'react';
import { compose } from "redux";
import { Grid, Typography } from '@material-ui/core'
import { withStyles } from "@material-ui/core/styles";
import collabGroupDetailsHeaderStyles from "./GroupDetailsHeader.style";
// import GroupDetailsDummyIcon from '../../../../../../src/assets/images/collaboration/GroupDetailsDummyIcon.svg'
import Avatar from '../../../../../components/Avatar/Avatar';


function GroupHeaderView({ classes }) {

    return (
        <>
            <Grid container justifyContent='center' className={classes.avatarContainer}>
                <Avatar
                    // otherMember={{
                    //     imageUrl: `${GroupDetailsDummyIcon}`,
                    // }}
                    size="xxlarge"
                    disableCard={true}
                    className={classes.avatar}
                />
            </Grid>
            <Typography variant="h3" className={classes.title}>
                Sales & Marketing
            </Typography>
        </>
    )
}


export default compose(withStyles(collabGroupDetailsHeaderStyles, { withTheme: true }))(GroupHeaderView);