import React from "react";
import { compose } from "redux";
import Typography from "@material-ui/core/Typography";
import Avatar from "../../../../../components/Avatar/Avatar";
import participantsListStyles from "./ParticipantsList.style";
import { withStyles } from "@material-ui/styles";
import { Divider, Grid, List, ListItem } from "@material-ui/core";

const ParticipantsListView = (props) => {
  const { classes, theme, item } = props;

  return (
    <Grid container alignItems="center" justifyContent='flex-start'>
      <List>
        <ListItem disableRipple>
          <Grid container alignItems="center" spacing={3} justifyContent='space-around'>
            <Grid item>
              <Avatar
                otherMember={{
                  imageUrl: item && item.imageUrl,
                }}
                size="medium"
                disableCard={true}
                className={classes.avatar}
              />
            </Grid>
            <Grid item>
              <Typography
                variant="h5"
                className={classes.fullName}
                title={item.fullName}>
                {item && item.fullName}
              </Typography>
              <Typography
                variant="caption"
                className={classes.email}
                title={item.email}>
                {item && item.email}
              </Typography>
            </Grid>
          </Grid>
        </ListItem>
      </List>
    </Grid >
  );
};

export default compose(withStyles(participantsListStyles, { withTheme: true }))(
  ParticipantsListView
);
