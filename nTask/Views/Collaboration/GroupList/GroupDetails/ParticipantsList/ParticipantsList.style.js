const participantsListStyles = (theme) => ({
  avatar: {},
  fullName: {
    fontWeight: theme.typography.fontWeightMedium,
    overflow: "hidden",
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
    maxWidth: "160px",
  },
  email: {
    fontSize: "11px",
    overflow: "hidden",
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
    maxWidth: "160px ",
  },

});

export default participantsListStyles;
