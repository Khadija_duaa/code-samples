const indexStyles = (theme) => ({
    groupListDataCnt: {
      height: "100%",
      borderRight: `1px solid ${theme.palette.border.grayLighter}`,
    }
  });
  
  export default indexStyles;
  