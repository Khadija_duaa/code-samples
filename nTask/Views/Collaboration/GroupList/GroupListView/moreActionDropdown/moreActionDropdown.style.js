const moreActionDropdownStyles = theme => ({

  listItem: {
    padding: "6px 14px",
    fontSize: "13px !important",
    color: theme.palette.text.primary,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 600,
    lineHeight: "normal",
  },

  moreOptionsCustomIcon: {
    padding: 0,
    height: 18,
    overflow: "hidden",
  }

});

export default moreActionDropdownStyles
