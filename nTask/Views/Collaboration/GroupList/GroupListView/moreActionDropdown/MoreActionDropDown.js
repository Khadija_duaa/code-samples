// @flow
import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import MoreHorizontalIcon from "@material-ui/icons/MoreHoriz";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import CustomIconButton from "../../../../../components/Buttons/IconButton";
import DropdownMenu from "../../../../../components/Dropdown/DropdownMenu";
import moreActionDropdownStyles from "./moreActionDropdown.style";
import ViewDetailsDialogView from "../ViewDetails/ViewDetailsDialogView";

// MoreActionDropDown Main Component
function MoreActionDropDown(props) {
  const { classes, theme, handleOptionSelect, style, iconColor } = props;

  const [anchorEl, setAnchorEl] = useState(null);

  //hook to manage the open/close state of view details dialog
  const [openDialog, setOpenDialog] = useState(false);

  const handleCloseDialog = () => {
    setOpenDialog(false);
    console.log("handle close dialog triggered");
  };

  const handleClick = (event) => {
    // Function Opens the dropdown
    event.stopPropagation();
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };
  const handleClose = (event) => {
    // Function closes dropdown
    setAnchorEl(null);
  };

  const handleItemClick = (event, option) => {
    // Fuction responsible for selecting item from the list
    handleOptionSelect(option); // Callback on item select used to lift up option to parent if needed

    if (option === "color") {
      /** if option is color then drop down will be open and color control will open */
    } else {
      setAnchorEl(null);
    }

    // if (event && option === "viewDetail") {
    //   console.log("view details clicked");
    //   setOpenDialog(true);
    // }
  };

  const open = Boolean(anchorEl);

  return (
    <>
      <CustomIconButton
        btnType="condensed"
        className={classes.moreOptionsCustomIcon}
        style={{ ...style }} // change inline styling
        onClick={handleClick}
        buttonRef={anchorEl}>
        <MoreHorizontalIcon htmlColor={iconColor} />
      </CustomIconButton>
      <DropdownMenu
        open={open}
        closeAction={handleClose}
        anchorEl={anchorEl}
        size="small"
        placement="bottom-end">
        {/* <Scrollbars autoHide autoHeight autoHeightMin={0} autoHeightMax={130}> */}
        <List>
          <ListItem
            button
            className={classes.listItem}
            onClick={(event) => {
              handleItemClick(event, "pinToTop");
            }}>
            Pin to Top
          </ListItem>
          {/* <ListItem
            button
            className={classes.listItem}
            onClick={(event) => {
              handleItemClick(event, "viewDetail");
            }}>
            View Detail
          </ListItem> */}
          <ListItem
            button
            className={classes.listItem}
            onClick={(event) => {
              handleItemClick(event, "muteConversation");
            }}>
            Mute Conversation
          </ListItem>
          <ListItem
            button
            className={classes.listItem}
            onClick={(event) => {
              handleItemClick(event, "leaveConversation");
            }}>
            Leave Conversation
          </ListItem>
        </List>
        {/* </Scrollbars> */}
      </DropdownMenu>
      {openDialog && <ViewDetailsDialogView open={openDialog} onClose={handleCloseDialog} />}
    </>
  );
}
MoreActionDropDown.defaultProps = {
  classes: {},
  theme: {},
  handleOptionSelect: () => { },
  style: {},
};
export default compose(
  withRouter,
  withStyles(moreActionDropdownStyles, { withTheme: true })
)(MoreActionDropDown);
