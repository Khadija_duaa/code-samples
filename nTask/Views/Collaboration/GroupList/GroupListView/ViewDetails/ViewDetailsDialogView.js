import React from "react";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import Dialog from "@material-ui/core/Dialog";
import Typography from "@material-ui/core/Typography";
import viewDetailsDialogStyles from "./ViewDetailsDialog.style";
import { DialogContent, Grid, SvgIcon } from "@material-ui/core";
import ParticipantsListView from "../../GroupDetails/ParticipantsList/ParticipantsListView";
import GroupHeaderView from "../../GroupDetails/GroupDetailsHeader/GroupDetailsHeaderView";
import { Scrollbars } from "react-custom-scrollbars";
import AddIcon from "@material-ui/icons/Add";
import CustomButton from "../../../../../components/Buttons/CustomButton";
import GroupDetailsActionView from "../../GroupDetails/GroupDetailsAction/GroupDetailsActionView";
import LeaveGroupIcon from "../../../../../components/Icons/CollaborationIcons/LeaveGroupIcon";
import "./ViewDetailsDialogStyle.css"
import CloseIcon from '@material-ui/icons/Close';
import IconButton from "@material-ui/core/IconButton";


const ViewDetailsDialog = (props) => {

  const { classes, theme, open, onClose } = props;

  const itemData = [
    {
      imageUrl: "https://images.unsplash.com/photo-1532074205216-d0e1f4b87368?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=741&q=80",
      fullName: "Ashley Blake",
      lastName: "",
      email: "ashes.blaked@gmail.com",
      isOnline: true,
      isOwner: false,
    },
    {
      imageUrl: "https://images.unsplash.com/photo-1532074205216-d0e1f4b87368?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=741&q=80",
      fullName: "Ashley Blake",
      lastName: "",
      email: "ashes.blaked@gmail.com",
      isOnline: true,
      isOwner: false,
    },
    {
      imageUrl: "https://images.unsplash.com/photo-1532074205216-d0e1f4b87368?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=741&q=80",
      fullName: "Ashley Blake",
      lastName: "",
      email: "ashes.blaked@gmail.com",
      isOnline: true,
      isOwner: false,
    }, {
      imageUrl: "https://images.unsplash.com/photo-1532074205216-d0e1f4b87368?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=741&q=80",
      fullName: "Ashley Blake",
      lastName: "",
      email: "ashes.blaked@gmail.com",
      isOnline: true,
      isOwner: false,
    },
    {
      imageUrl: "https://images.unsplash.com/photo-1532074205216-d0e1f4b87368?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=741&q=80",
      fullName: "Ashley Blake",
      lastName: "",
      email: "ashes.blaked@gmail.com",
      isOnline: true,
      isOwner: false,
    },
    {
      imageUrl: "https://images.unsplash.com/photo-1532074205216-d0e1f4b87368?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=741&q=80",
      fullName: "Ashley Blake",
      lastName: "",
      email: "ashes.blaked@gmail.com",
      isOnline: true,
      isOwner: false,
    },
    {
      imageUrl: "https://images.unsplash.com/photo-1532074205216-d0e1f4b87368?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=741&q=80",
      fullName: "Ashley Blake",
      lastName: "",
      email: "ashes.blaked@gmail.com",
      isOnline: true,
      isOwner: false,
    }
  ]

  const getParticipantListHeader = () => {
    return (
      <Grid container alignItems="center" justifyContent="space-between">
        <Typography variant="h4" className={classes.participantsListText}>
          Participants List
        </Typography>

        <CustomButton
          btnType="green"
          variant="text"
          className={classes.addParticipantsButton}
          onClick={() => { }}
        >
          <SvgIcon className={classes.addIcon}>
            <AddIcon fontSize='small' />
          </SvgIcon>
          Add Participants
        </CustomButton>
      </Grid>
    )
  }

  return (
    <Dialog open={open} onClose={onClose}
      scroll="body"
      disableBackdropClick={true}
      disableEscapeKeyDown={true}
      classes={{
        paper: classes.dialogPaperCnt,
        scrollBody: classes.dialogCnt,
        root: classes.root
      }}>
      <Grid container justifyContent="flex-end" style={{ marginBottom: '-10px' }}>
        <IconButton btnType="transparent" onClick={onClose} edgeEnd>
          <CloseIcon
            htmlColor={theme.palette.secondary.medDark}
            style={{ fontSize: "18px", justifyContent: 'flex-end' }}
          />
        </IconButton>
      </Grid>
      <DialogContent>
        <Grid container alignItems="center" justifyContent="center">
          <Grid item sm={12} className={classes.header}>
            <GroupHeaderView />
          </Grid>

          <Grid item sm={12} className={classes.groupDetailsActionView}>
            <GroupDetailsActionView title="Leave Group" icon={<LeaveGroupIcon />} />
          </Grid>

          <Grid item sm={12} style={{
            paddingBottom: '12px'
          }}>
            {getParticipantListHeader()}
            <Scrollbars autoHide autoHeightMax={250} autoHeight={true} className="scrollCon">
              {Array.isArray(itemData) && itemData.map((data, index) => {
                return (
                  <ParticipantsListView item={data} key={index} />
                )
              })}
            </Scrollbars>
          </Grid>
        </Grid>
      </DialogContent>
    </Dialog>
  );
};

ViewDetailsDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
};

export default compose(withStyles(viewDetailsDialogStyles, { withTheme: true }))(ViewDetailsDialog);
