// import ViewDetailsHeader from "../../../../../assets/images/collaboration/ViewDetailsHeader.png";

const viewDetailsDialogStyles = (theme) => ({

  dialogPaperCnt: {
    overflowY: "visible",
    background: theme.palette.common.white,
    width: "600px!important",
    borderRadius: 6,
  },
  dialogCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  header: {
    // backgroundImage: `url('${ViewDetailsHeader}')`,
    padding: '25px',
    backgroundRepeat: "no-repeat",
    height: "100%",
    textAlign: "center",
  },
  avatar: {
    backgroundColor: '#e63946',
    padding: '25px',
    margin: '25px'
  },
  participantsListText: {
    textTransform: 'capitalize',
    fontSize: "16px !important",
    color: theme.palette.text.primary,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 600,
    lineHeight: "normal",
  },
  addIcon: {
    marginRight: '5px',
    fontSize: "18px !important",
  },
  addParticipantsButton: {
    letterSpacing: 0.75,
    textTransform: 'capitalize',
    fontSize: "14px !important",
    color: theme.palette.primary.main,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: theme.typography.fontWeightExtraLight,
  },
  groupDetailsActionView: {
    paddingBottom: '0px',
    paddingTop: '12px'
  },
  scrollbars: {
    overflow: "hidden",
    padding: "25px 0px 0px 0px",
    overflowY: "hidden"
  }
});

export default viewDetailsDialogStyles;
