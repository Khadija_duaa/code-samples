import { withStyles } from "@material-ui/core/styles";
import React, { useCallback, useContext, useEffect, useRef, useState } from "react";
import { compose } from "redux";
import { connect, useSelector, useDispatch } from "react-redux";
import { withRouter } from "react-router";
// import { InfiniteLoader, List, AutoSizer } from "react-virtualized";
import "react-virtualized/styles.css"; // only needs to be imported once
import Grid from "@material-ui/core/Grid";
import Card from "../../Components/Card/Card";
import groupListViewStyles from "./groupListView.style";
import mockData from "../mockData";
import { Scrollbars } from "react-custom-scrollbars";
import { FixedSizeList as List } from "react-window";
import { getBoardImages } from "../../../../redux/actions/boards";
import { getChatGroups } from '../../../../redux/actions/collaboration';
import './groupListStyle.css'
import collabContext from "../../Context/collaboration.context";
import Loader from "../../../../components/Loader/Loader";
import { isScrollBottom } from "../../../../utils/common";


const GroupListView = (props) => {
    let listRef = useRef();

    let { groupChatList, loggedInUser, getPayloadDataForChatGroupListing, loadingScroll,
        noData, showSnackBar, updatedList} = props;
    const [groupChat, setGroupChat] = useState(groupChatList);
    const [loading, setLoading] = useState(loadingScroll);
    const [noDataBol, setNoDataBol] = useState(noData);
    const [forceUpdate, setForceUpdate] = useState(false);

    useEffect(() => {
        setGroupChat(groupChatList)
    }, [groupChatList]);

    useEffect(() => {
        setLoading(loadingScroll)
    }, [loadingScroll]);

    useEffect(() => {
        setNoDataBol(noData)
    }, [noData]);

    // automatic reset state to get real time chat time in card component
    setInterval(() => {
        setForceUpdate(!forceUpdate);
    }, 50000)

    const handleScroll = ({ target }) => {
        const { scrollTop, scrollHeight, clientHeight } = target;

        if (scrollTop + clientHeight === scrollHeight) {
            getPayloadDataForChatGroupListing()
        }

        
        listRef.current.scrollTo(scrollTop);
    };

    const onScroll = () => {

    }

    const getCard = () => {
        return (
            <>
                <List
                    onScroll={onScroll}
                    ref={listRef}
                    className="List"
                    height={1000}
                    itemCount={!groupChat ? console.log('!groupChat', groupChat) : groupChat && groupChat.length}
                    itemSize={70}
                    width={"100%"}
                    style={{
                        overflow: true
                    }}
                >
                    {Row}
                </List>
            </>
        )
    }

    const Row = (props) => (
        <Card itemProps={props} list={groupChat} loggedInUser={loggedInUser} 
        updatedList={updatedList}
        showSnackBar={showSnackBar} />

    );

    return (
        <Grid container>
            <Grid item xs={12}>
                <Scrollbars
                    className="scrollCon"
                    autoHide={true}
                    autoHeight={true}
                    autoHeightMax={"100vh"}
                    refList={listRef}
                    onScroll={handleScroll}
                    style={{ overflow: scroll }}
                >
                    {getCard()}
                </Scrollbars>
                {loading ? <div className="text-center">loading data ...</div> : ""}
                {noDataBol ? <div className="text-center">no data anymore ...</div> : ""}
            </Grid>
        </Grid>
    );
}

export default compose(withStyles(groupListViewStyles, { withTheme: true }))(GroupListView);
