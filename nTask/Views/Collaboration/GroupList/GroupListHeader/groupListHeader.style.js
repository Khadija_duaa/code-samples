const groupListHeaderStyles = (theme) => ({
  groupListHeaderCnt: {
    height: "100%",
    padding: theme.spacing(1.5),
    // justifyContent: 'right'
  },
  customBtnAddNeww: {
    borderRadius: "7px",
    padding: "8px 9px",
    minWidth: '100%'
  },
  addMessageIcon: {
    color: theme.palette.icon.white,
    fontSize: "18px !important",
  },
  groupListHeaderGrid: {
    width: '100%',
    margin: '-4px 0px'
  },
  searchBarGrid: {
    width: '86%'
  },
  addNewBtnGrid: {
    width: '14%'
  },
});

export default groupListHeaderStyles;
