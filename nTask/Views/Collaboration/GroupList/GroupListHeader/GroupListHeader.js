import React, { useState, useContext, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { connect, useSelector, useDispatch, shallowEqual } from "react-redux";
import { compose } from "redux";
import FilterListIcon from "@material-ui/icons/FilterList";
import Grid from "@material-ui/core/Grid";
import groupListHeaderStyles from "./groupListHeader.style";
import collabContext from "../../Context/collaboration.context";
import DefaultTextField from "../../../../components/Form/TextField";
import CustomChipSelectDropdown from "../../../../components/Dropdown/CustomChipSelectDropdown/CustomChipSelectDropdown";
import filterData from "../../../../utils/constants/collaborationFilterData";
import CustomButton from "../../../../components/Buttons/CustomButton";
import NewConversationIcon from '../../../../components/Icons/CollaborationIcons/NewConversationIcon';
import mockData from "../mockData";
import { createChat, resetChatStore, resetRedux } from '../../../../redux/actions/collaboration';
import SvgIcon from "@material-ui/core/SvgIcon";

const GroupListHeader = ({ classes, updatedList, groupChatList }) => {

  const dispatch = useDispatch();
  const state = useSelector(state => {
    return {
      group: state && state.collab && state.collab.group && state.collab.group,
    };
  }, shallowEqual);
  let stateContext = useContext(collabContext);
  const [search, setSearch] = useState("");

  let mockFistIndexedData = {
    id: 0,
    groupName: "New Conversation",
    lastMessage: "",
    lastMessageFrom: "",
    isMuted: true,
    isPinned: true,
    isRead: true,
    // groupName: "",
    updatedDate: "",
    groupMembers: "",
  };

  // Handle Search Input On Change
  const handleSearchInput = event => {
    setSearch(event.target.value);
  };

  const onClickNewCon = () => {
    stateContext.updateIsNewChatEnabled(true);
    if (groupChatList && groupChatList[0].groupName !== mockFistIndexedData.groupName) {
      groupChatList.unshift(mockFistIndexedData);
      updatedList(groupChatList);
    }

    stateContext.handleSetMessageList([]);

    // let _tempGroup = state.group;
    // if (_tempGroup && _tempGroup) {
    //   Object.keys(_tempGroup).forEach(key => {
    //     delete _tempGroup[key];
    //   })
    resetChatStore(dispatch);
  }

  const onMouseHover = () => {
    stateContext.onHover();
  }

  const onMouseLeave = () => {
    stateContext.onHoverLeave();
  }

  const getSearchBar = () => {
    return (
      <DefaultTextField
        label={false}
        fullWidth
        error={false}
        errorState={false}
        formControlStyles={{ marginBottom: 0 }}
        endAdornment={
          <CustomChipSelectDropdown
            label="Search"
            options={filterData}
            option={null}
            icon={
              <FilterListIcon
                onMouseEnter={onMouseHover}
                onMouseLeave={onMouseLeave} color={stateContext.iconColor} />
            }
            height="110px"
            scrollHeight={180}
          />
        }
        defaultProps={{
          type: "text",
          onChange: e => handleSearchInput(e),
          value: search,
          placeholder: "Search",
          inputProps: { style: { padding: "10px 14px" } },
        }}
      />
    )
  }

  const getAddMoreButton = () => {
    return (
      <CustomButton
        btnType="green"
        variant="contained"
        className={classes.customBtnAddNeww}
        onClick={onClickNewCon}
      >
        <SvgIcon viewBox="0 0 16 16.275" className={classes.addMessageIcon} >
          <NewConversationIcon />
        </SvgIcon>
      </CustomButton>
    )
  }

  return (
    <div className={classes.groupListHeaderCnt}>
      <Grid container spacing={1} className={classes.groupListHeaderGrid}>
        <Grid item className={classes.searchBarGrid}>
          {getSearchBar()}
        </Grid>
        <Grid item className={classes.addNewBtnGrid}>
          {getAddMoreButton()}
        </Grid>
      </Grid>
    </div>
  );
}

export default compose(withStyles(groupListHeaderStyles, { withTheme: true }))(GroupListHeader);
