import constants from './constants';

function reducer(state, action) {
    switch (action.type) {
      case constants.SET_SELECTED_MESSAGE:
        return {...state,  selectedMessage: action.payload};
      case constants.START_NEW_CONVERSATION:
        return {...state, newConversation: action.payload}
      default:
        throw new Error();
    }
  }

  export default reducer