import React, { useState, useEffect } from "react";
import collabContext from "./collaboration.context";

const CollaborationContext = (props) => {

    const onHover = () => {
        setIconColor("secondary")
    }

    const onHoverLeave = () => {
        setIconColor(open ? "secondary" : "inherit")
    }

    const updateOpen = (value) => {
        setOpen(value)
    }

    const updateIsNewChatEnabled = (value) => {
        setNewChatEnabled(value)
    }

    const getSelectedParticipantList = (array) => {
        setSelectedParticipantList(array)
    }

    const handleSetMessageList = (value) => {
        setMessageList(value)
    }

    const handleIsLoading = (value) => {
        setIsLoading(value)
    }

    useEffect(() => {
    }, [isLoading]);

    const [isLoading, setIsLoading] = useState(true);
    const [messageList, setMessageList] = useState([]);
    const [iconColor, setIconColor] = useState("inherit");
    const [open, setOpen] = useState(false);
    const [isNewChatEnabled, setNewChatEnabled] = useState(false);
    const [selectedParticipantList, setSelectedParticipantList] = useState([])

    return (
        <collabContext.Provider value={{ iconColor, onHover, onHoverLeave, open, updateOpen, isNewChatEnabled, updateIsNewChatEnabled, selectedParticipantList, getSelectedParticipantList, isLoading, handleIsLoading, messageList, handleSetMessageList }}>
            {props.children}
        </collabContext.Provider>
    )
}

export default CollaborationContext;