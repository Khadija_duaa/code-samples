import constants from "./constants";

export const dispatchSelectedMsg = (dispatch, obj) => {
  dispatch({ type: constants.SET_SELECTED_MESSAGE, payload: obj });
};
export const dispatchNewConversation = (dispatch, obj) => {
  dispatch({ type: constants.START_NEW_CONVERSATION, payload: obj });
};
export const dispatchChatGroups = (dispatch, obj) => {
  dispatch({ type: constants.SET_CHAT_GROUPS, payload: obj });
};
