import React, { useState, useEffect, useContext } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import collaborationStyles from "./collaboration.style";
import Grid from "@material-ui/core/Grid";
import CollaborationContext from "./Context/state";
// import collabContext from "../../Context/collaboration.context";
import collabContext from "../Collaboration/Context/collaboration.context";
import GroupList from "./GroupList/index";
import MessageList from "./MessageList/MessageList";
import { withSnackbar } from "notistack";
import { store } from "../../index";
import { populateGroupList, getGroupMessages, getChatGroups } from '../../redux/actions/collaboration';

import Loader from "../../components/Loader/Loader";
import { useSelector, useDispatch, shallowEqual } from "react-redux";


const CollaborationView = ({ classes, enqueueSnackbar }) => {

  let state = useSelector(state => {
    return {
      profileState: state.profile.data,
    };
  }, shallowEqual);
  const { profileState } = state;
  let loggedInUser = profileState && profileState.userId;

  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(true);
  const [loadingScroll, setLoadingScroll] = useState(false);
  const [page, setPage] = useState(1);
  const [noData, setNoData] = useState(false);
  const [groupChatList, setGroupChatList] = useState([]);



  const showSnackBar = (snackBarMessage = "", type = null) => {

    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };

  useEffect(() => {

    getChatGroups(page, dispatch, (res) => {
      let { entities } = res && res.data;
      if (entities && entities.length > 0) {
        for (let ind = 0; ind < entities.length; ind++) {
          entities[ind].isChatCardClicked = false
        }
      }
      getGroupMessages(entities[0], dispatch, () => {
        // stateContext.updateIsNewChatEnabled(false);
        entities[0].isChatCardClicked = true
        let _uniqueList = entities.filter((obj, index, self) =>
          index === self.findIndex((t) => (
            t.groupId === obj.groupId
          ))
        );
        setGroupChatList(_uniqueList)
        setIsLoading(false);

      }, error => {
        if (error) {
          showSnackBar("Server throws error", "error");
        }
      })

    },
      (error) => {
        setIsLoading(false);
        if (error) {
          // props.showSnackBar("Server throws error", "error");
        }
      });
  }, []);

  const updatedList = (list) => {
    let _uniqueList = list.filter((obj, index, self) =>
      index === self.findIndex((t) => (
        t.groupId === obj.groupId
      ))
    );
    setGroupChatList(_uniqueList);
    populateGroupList(_uniqueList);
  }


  const getPayloadDataForChatGroupListing = () => {
    if (!noData) {
      setLoadingScroll(true);
      getChatGroups(page + 1, dispatch, (res) => {
        let { totalRecords } = res && res.data;
        let { entities } = res && res.data;
        if (entities && entities.length > 0) {
          for (let ind = 0; ind < entities.length; ind++) {
            entities[ind].isChatCardClicked = false
          }
        }
        const newPage = page + 1;
        setPage(newPage)
        const newList = groupChatList.concat(entities);
        let _uniqueList = newList.filter((obj, index, self) =>
          index === self.findIndex((t) => (
            t.groupId === obj.groupId
          ))
        );

        setGroupChatList(_uniqueList);
        populateGroupList(_uniqueList);
        if (_uniqueList.length === totalRecords) {
          setNoData(true);
        }
        setLoadingScroll(false);
      },
        (error) => {
          if (error) {
            // props.showSnackBar("Server throws error", "error");
          }
          setLoadingScroll(false);
        });
    }

  }


  return (
    <CollaborationContext>
      {isLoading ?
        <Loader /> :
        <Grid container >
          <Grid item xs={3} className={classes.border} >
            <GroupList groupChatList={groupChatList} loggedInUser={loggedInUser}
              updatedList={updatedList}
              loadingScroll={loadingScroll} noData={noData} showSnackBar={showSnackBar}
              getPayloadDataForChatGroupListing={getPayloadDataForChatGroupListing} />
          </Grid>
          <Grid item xs={9} className={classes.border}>
            <MessageList showSnackBar={showSnackBar}
              updatedList={updatedList}
              groupChatList={groupChatList} />
          </Grid>
        </Grid>
      }
    </CollaborationContext>
  );
}


export default compose(withSnackbar, withStyles(collaborationStyles, { withTheme: true }))(CollaborationView);
