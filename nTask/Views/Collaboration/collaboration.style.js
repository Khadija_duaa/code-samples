const collaborationStyles = (theme) => ({
  contentCon: {
   height: "100%"
  },
  content: {
    flexGrow: 1,
    borderTop:  `1px solid ${theme.palette.border.grayLighter}`,   
    // transition: theme.transitions.create("margin", {
    //   easing: theme.transitions.easing.sharp,
    //   duration: theme.transitions.duration.leavingScreen
    // }),
    "& *": {
      fontFamily: theme.typography.fontFamilyLatoWithoutImportant,
    },
  },
  collaborationViewCnt: {
    padding: '20px 20px 20px 46px'
  },
  //Header Styles
  collaborationHeaderCnt:{
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      marginBottom: 12,
      flexWrap: 'wrap'
  },
  toggleContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  toggleBtnGroup: {
    display: "flex",
    flexWrap: "nowrap",
    borderRadius: 6,
    background: theme.palette.common.white,
    backgroundColor: "#F6F6F6",
    padding: 3,
    boxShadow: "none",
    "& $toggleButtonSelected": {
      boxShadow: "0px 1px 4px -2px rgb(0 0 0 / 70%)",
      borderRadius: 4,
      color: theme.palette.text.secondary,
      backgroundColor: "white",
      "&:after": {
        background: theme.palette.common.white,
      },
      "&:hover": {
        background: theme.palette.common.white,
      },
    },
  },
  toggleButton: {
    height: 'auto',
    padding: '4px 20px 5px',
    fontSize: "12px !important",
    fontWeight: theme.palette.fontWeightMedium,
    textTransform: "capitalize",
    "&:hover": {
      backgroundColor: "#F6F6F6",
    },
  },
  toggleButtonSelected: {},
  addMessageIcon: {
    color: theme.palette.icon.white,
    fontSize: "20px !important",
    marginRight: 6
  },
  newConversationButton: {
    borderRadius:'6px',
    fontSize:'14px',
    '&:hover':{
      background:'#00B781',
    } 
  },
  collaborationsHeading: {
    marginRight: 15,
    fontSize: 18,
    color : "#333333",
    fontWeight: 700,
    fontFamily: theme.typography.fontFamilyLato
  },
  notificationDot: {
    background: '#FD4F44',
    boxShadow: "0px 0px 4px #0090ff4D",
    width: 8,
    height: 8,
    borderRadius: "50%",
    marginLeft: 8,
  },
})

export default collaborationStyles