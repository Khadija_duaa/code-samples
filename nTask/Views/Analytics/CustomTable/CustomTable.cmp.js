import React, { useContext, useEffect } from "react";
import styled from "styled-components";
import {
  useTable,
  useBlockLayout,
  useGroupBy,
  useExpanded,
  useAsyncDebounce,
  useGlobalFilter,
  useSortBy,
  useResizeColumns,
  usePagination,
} from "react-table";
import { FixedSizeList } from "react-window";
import { Circle } from "rc-progress";
import Typography from "@material-ui/core/Typography";
import { connect } from "react-redux";
import AddIcon from "@material-ui/icons/Add";
import withStyles from "@material-ui/core/styles/withStyles";
import { compose } from "redux";
import Avatar from "@material-ui/core/Avatar";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import clsx from "clsx";
import { statusDataNew, priorityData } from "../../../helper/taskDropdownData";
import { defaultTheme } from "../../../assets/jss/theme";
import TaskOverviewContext from "../ProjectOverview/Context/ProjectOverview.context";
import {
  dispatchWidgetSetting,
  dispatchTaskGroupByAssignee,
} from "../ProjectOverview/Context/actions";
import { updateWidgetSetting } from "../../../redux/actions/overviews";
import CustomMultiSelectDropdown from "../../../components/Dropdown/CustomMultiSelectDropdown/Dropdown";

import customTableStyles from "./customTableStyles";
import CustomAvatar from "../../../components/Avatar/Avatar";
import ColumnActionDropDown from "./ColumnActionDropdown/ColumnActionDropDown";
import CancelIcon from "@material-ui/icons/Cancel";
import EmptyState from "../../../components/EmptyStates/EmptyState";
import DefaultTextField from "../../../components/Form/TextField";

const Styles = styled.div`
  padding: 0 15px 15px 15px;
  ${
    "" /* These styles are suggested for the table fill all available space in its containing element */
  }
  display: block;
  ${"" /* These styles are required for a horizontaly scrollable table overflow */}
  // overflow: auto;
  .table {
    display: inline-block;
    border-spacing: 0;
    min-width: 100%;

    .thead {
        overflow-y: auto;
        overflow-x: hidden;
      }

    .tbody {
        ${"" /* These styles are required for a scrollable table body */}
        // overflow-y: scroll;
        // overflow-x: hidden;
        // height: 250px;
      }

    .tr {
      padding-left: 15px;
      padding-right: 15px;

      :last-child {
        .td {
          border-bottom: 0;
        }
      }
      .progress, .completedEffortInHours, .overDueTasks, .lateTasks, .onTrackTask, .futureTasks, .completedTasks, .startDate, .dueDate, .totalTasks {
        justify-content: center !important;
    align-items:  center !important;
  }
    }
    .gtr {
      .td{
        background: #eaeaea;
      }
      :last-child {
        .td {
          border-bottom: 0;
        }
      }
    }
    .childRow{
      // box-shadow: inset 10px 0px 0px 0px #eaeaea, inset -10px 1px 0px 0px #eaeaea;
    }
    .glr {
      box-shadow: inset 10px 0px 0px 0px #eaeaea;
      .td {
        box-shadow: 0px 10px 0px #eaeaea;
      }
    }

    .th,
    .td {
      position: relative;
      margin: 0;
      padding: 4px 0;
      border-bottom: none;
      border-right: none;
      display: flex !important;
      align-items: center;
      justify-content: center;
      :last-child {
        border-right: none;
      }
    }
    .th.taskTitle {
      justify-content: flex-start;
    }
    .th {
      font-size: 12px;
      padding: 0;
      margin-top: 10px;
      margin-Bottom: 10px;
      padding-left: 2px;
      justify-content: flex-start !important;
      align-items: flex-start !important;
      :first-child {
        display: flex;
        justify-content: flex-start;
      }
      .columnHeader {
        display: inline-block;
        transition: ease all 0.2s;
        padding: 3px 6px 3px 0;
        cursor: default;
        height: 22px;
      
      }
      :hover{
        .columnHeader {
        transition: ease all 0.2s;
        background: #EAEAEA;
        border-radius: 4px 0 0 4px;
        padding-left: 7px;
        border-right: 2px solid #BFBFBF;
        
      }
      button{
        display: inline-block;
      }
      }
    }
    .td {
      font-size: 12px;
      box-shadow: 0px 1px 0px #eaeaea;
      display: flex !important;
      align-items: center;
      justify-content: center;
      padding-Right: 10px;
      :first-child {
        font-size: 13px;
        justify-content: flex-start;
      }
    }
    .td.taskTitle {
      font-size: 13px;
      justify-content: flex-start;
    }
    .priorityIcon {
      width: 28px;
      height: 28px;
      border: 1px solid #dddddd;
      border-radius: 50%;
      display: flex;
      align-items: center;
      justify-content: center;
      background: #fff;
    }
    .addIcon {
      color: white;
    }
    .arrowIcon{
      color: #7E7E7E
    },
   
  }
  .taskGroupCountTag{
    padding: 4px;
    color: white;
    background: #0090ff;
    font-size: 11px;
    margin-left: 8px;
    display: inline-block;
    border-radius: 4px;
  }
  .groupByColumnsCnt{
    border-bottom: 1px solid #dddddd;
    padding: 8px 0 8px 0;
    display: flex;
    align-items: center;
  }
  .groupByColumnChipLabel{
    font-size: 13px;
    color: #000;
    display: inline-block;
    margin-right: 8px;
  }
  .groupByColumnChip{
    font-size: 13px;
    color: #000;
    padding: 3px 8px;
    display: flex;
    background: #EAEAEA;
    border-radius: 4px;
    display: flex;
    align-items: center;
    margin-right: 10px;
    svg{
      font-size: 18px;
      color: #969696;
      cursor: pointer;
      margin-left: 8px
    }
  }
  .empty {
    padding: 0 30px 0 60px;
      flex: 1;
      display: flex;
  },
  .pagination {
    // padding: 0.5rem;
  }
  .footer{
    display: flex;
    border: 1px solid #eaeaea;
    height: 27px;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    padding-left: 5px;
    padding-right: 5px;
    background-color: #eaeaea;
  }
  .page_arrow_Btn{
    border: none;
    background: no-repeat;
    font-size: 16px;
    font-weight: 700;
    cursor: pointer;
    font-family: 'lato', sans-serif;
  }
  .page_arrow_Btn:hover{
    color: #6A96FC;
  }
  .page_No_Field{
    font-weight: 400;
    font-size: 13px;
    height: 29px;
    border: 1px solid #dddddd;
  }
  .progress, .completedEffortInHours, .overDueTasks, .lateTasks, .onTrackTask, .futureTasks, .completedTasks, .startDate, .dueDate, .totalTasks {
        justify-content: center !important;
    align-items:  center !important;
  }
  .page_Label:{
    font-family: 'lato', sans-serif;
    font-weight: 400;
    font-size: 13px;
  }
  .resizer {
        display: inline-block;
        background: rgba(221, 221, 221, 1);
        width: 1px;
        height: 100%;
        position: absolute;
        right: 0;
        top: 0;
        transform: translateX(50%);
        z-index: 1;
        ${"" /* prevents from scrolling while dragging on touch devices */}
        touch-action:none;

        &.isResizing {
          background: red;
        }
      }
`;
// Define a default UI for filtering
function GlobalFilter({ preGlobalFilteredRows, globalFilter, setGlobalFilter }) {
  const count = preGlobalFilteredRows.length;

  const [value, setValue] = React.useState(globalFilter);
  const onChange = useAsyncDebounce(v => {
    setGlobalFilter(v || undefined);
  }, 200);

  return (
    <span
      style={{
        fontFamily: "'lato', sans-serif",
        fontSize: "13px",
        position: "absolute",
        right: 19,
        top: -38,
      }}>
      Search:{" "}
      <input
        value={value || ""}
        onChange={e => {
          setValue(e.target.value);
          onChange(e.target.value);
        }}
        placeholder={`${count} records...`}
        style={{
          border: "1px solid rgba(221, 221, 221, 1)",
          borderRadius: 4,
          fontFamily: "'lato', sans-serif",
          fontSize: "13px",
          padding: 8,
          width: 250,
        }}
      />
    </span>
  );
}
function Table({ columns, data }) {
  // Use the state and functions returned from useTable to build your UI
  const {
    state: { widgetSettings },
    dispatch,
  } = useContext(TaskOverviewContext);
  const defaultColumn = React.useMemo(
    () => ({
      minWidth: 30,
      // width: 150,
      // maxWidth: 400,
    }),
    []
  );

  // Style for Table Cells
  const getCellStyles = (props, col) => {
    return [
      props,
      {
        style: {
          // margin: 0,
          // width: col.width,
          //  flex: 1,
          // minWidth: col.minWidth,
          border: "none",
        },
      },
    ];
  };
  // Style for Table Header
  const getHeaderStyles = (props, col) => {
    return [
      props,
      {
        style: {
          // width: col.width,
          // height: 30,
          // flex: 1,
          // border: 'none',
          // minWidth: col.minWidth,
        },
      },
    ];
  };
  const getTableStyles = props => [
    props,
    {
      style: {
        // width: "100%",
        borderSpacing: 0,
      },
    },
  ];
  // const headerProps = (props, rowInfo) => getHeaderStyles(props, rowInfo.column);

  // const cellProps = (props, rowInfo) => getCellStyles(props, rowInfo.cell.column);
  const tableProps = props => getTableStyles(props);
  const getStyles = (props, align = "left") => [
    props,
    {
      style: {
        justifyContent: align === "right" ? "flex-end" : "flex-start",
        alignItems: "flex-start",
        display: "flex",
      },
    },
  ];

  const headerProps = (props, { column }) => getStyles(props, column.align);

  const cellProps = (props, { cell }) => getStyles(props, cell.column.align);

  const scrollbarWidth = () => {
    // thanks too https://davidwalsh.name/detect-scrollbar-width
    const scrollDiv = document.createElement("div");
    scrollDiv.setAttribute(
      "style",
      "width: 100px; height: 100px; overflow: scroll; position:absolute; top:-9999px;"
    );
    document.body.appendChild(scrollDiv);
    const scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
    document.body.removeChild(scrollDiv);
    return scrollbarWidth;
  };

  const scrollBarSize = React.useMemo(() => scrollbarWidth(), []);
  // Define a default UI for filtering

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    footerGroups,
    rows,
    totalColumnsWidth,
    preGlobalFilteredRows,
    state,
    prepareRow,
    setGlobalFilter,
    toggleGroupBy,
    pageOptions,
    page,
    gotoPage,
    previousPage,
    nextPage,
    setPageSize,
    canPreviousPage,
    canNextPage,
    state: { pageIndex, pageSize },
  } = useTable(
    {
      columns,
      data,
      defaultColumn,
      initialState: { groupBy: widgetSettings.groupBy, pageIndex: 0 },
    },
    useBlockLayout,
    useGroupBy,
    useGlobalFilter,
    useSortBy,
    useExpanded,
    usePagination,
    useResizeColumns
  );

  const RenderRow = () => {
    return (
      <>
        {page.map((item, i) => {
          const row = rows[i];
          const canExpanded = rows[i].canExpand;
          const { isGrouped } = row;
          const isExp = row.isExpanded;
          prepareRow(row);
          return (
            <div
              {...row.getRowProps()}
              className={clsx({
                gtr: isGrouped && isExp,
                childRow: state.groupBy.length && !canExpanded,
                tr: !canExpanded,
              })}>
              {row.cells.map(cell => {
                return (
                  <div {...cell.getCellProps(cellProps)} className={`td ${cell.column.id}`}>
                    {cell.isGrouped ? (
                      // If it's a grouped cell, add an expander and row count
                      <>
                        <span {...row.getToggleRowExpandedProps()}>
                          {row.isExpanded ? (
                            <ArrowDropDownIcon className="arrowIcon" />
                          ) : (
                            <ArrowRightIcon className="arrowIcon" />
                          )}
                        </span>
                        {cell.render("Cell")}
                        <span className="taskGroupCountTag">{row.leafRows.length} projects</span>
                      </>
                    ) : cell.isAggregated ? null : cell.isPlaceholder ? null : ( // cell.render("Aggregated") // renderer for cell // If the cell is aggregated, use the Aggregated // For cells with repeated values, render null
                      // Otherwise, just render the regular cell
                      cell.render("Cell")
                    )}
                  </div>
                );
              })}
            </div>
          );
        })}
      </>
    );


    // prepareRow(row);
    // return (
    //   <div
    //     {...row.getRowProps({
    //       style,
    //     })}
    //     className={clsx({
    //       gtr: isGrouped && isExp,
    //       childRow: state.groupBy.length && !canExpanded,
    //       tr: !canExpanded,
    //     })}>
    //     {row.cells.map(cell => {
    //       return (
    //         <div {...cell.getCellProps(cellProps)} className={`td ${cell.column.id}`}>
    //           {/* {cell.render("Cell")} */}
    //           {cell.isGrouped ? (
    //             // If it's a grouped cell, add an expander and row count
    //             <>
    //               <span {...row.getToggleRowExpandedProps()}>
    //                 {row.isExpanded ? (
    //                   <ArrowDropDownIcon className="arrowIcon" />
    //                 ) : (
    //                   <ArrowRightIcon className="arrowIcon" />
    //                 )}
    //               </span>
    //               {cell.render("Cell")}
    //               <span className="taskGroupCountTag">{row.leafRows.length} tasks</span>
    //             </>
    //           ) : cell.isAggregated ? null : cell.isPlaceholder ? null : ( // cell.render("Aggregated") // renderer for cell // If the cell is aggregated, use the Aggregated // For cells with repeated values, render null
    //             // Otherwise, just render the regular cell
    //             cell.render("Cell")
    //           )}
    //         </div>
    //       );
    //     })}
    //   </div>
    // );
  };

  // Render the UI for your table
  return (
    <>
      <GlobalFilter
        preGlobalFilteredRows={preGlobalFilteredRows}
        globalFilter={state.globalFilter}
        setGlobalFilter={setGlobalFilter}
        manualGlobalFilter
      />
      {/* {state.groupBy.length ? (
        <div className="groupByColumnsCnt">
          <label className="groupByColumnChipLabel">Group by:</label>
          {state.groupBy.map(g => {
            const groupByColumn = columns.find(c => c.accessor == g);
            return groupByColumn ? (
              <span className="groupByColumnChip">
                <span>{groupByColumn.Header}</span>
                <CancelIcon
                  onClick={() => {
                    const updatedGroupBy = widgetSettings.groupBy.filter(
                      g => g !== groupByColumn.accessor
                    );
                    if (groupByColumn.accessor == "assignee") {
                      // dispatchTaskGroupByAssignee(dispatch, []);
                    }
                    // dispatchWidgetSetting(dispatch, { groupBy: updatedGroupBy });
                    // updateWidgetSetting(
                    //   "task",
                    //   { ...widgetSettings, groupBy: updatedGroupBy },
                    //   // Success
                    //   res => {}
                    // );
                    toggleGroupBy(groupByColumn.accessor);
                  }}
                />
              </span>
            ) : null;
          })}
        </div>
      ) : null} */}
      <div {...getTableProps(tableProps)} className="table">
        <div>
          {headerGroups.map(headerGroup => (
            <div {...headerGroup.getHeaderGroupProps()} className="tr">
              {headerGroup.headers.map(column => (
                <div {...column.getHeaderProps(headerProps)} className={`th ${column.id}`}>
                  {/* {column.canGroupBy ? (
                    // If the column can be grouped, let's add a toggle
                    <span {...column.getGroupByToggleProps()}>
                      {column.isGrouped ? '🛑 ' : '👊 '}
                    </span>
                  ) : null} */}
                  {/* {column.id == "projectManager" ? (
                    <span className="columnHeader">
                      {column.render("Header")}
                      {!state.groupBy.includes(column.id) && (
                        <ColumnActionDropDown column={column} data={data} />
                      )}
                    </span>
                  ) : (
                    column.render("Header")
                  )} */}
                  {column.render("Header")}
                  <div
                    {...column.getResizerProps()}
                    className={`resizer ${column.isResizing ? "isResizing" : ""}`}
                  />
                </div>
              ))}
            </div>
          ))}
        </div>

        <div
          {...getTableBodyProps()}
          className="tbody"
          style={{
            minHeight:
              pageSize == 10
                ? 250
                : pageSize == 20
                ? 350
                : pageSize == 30
                ? 400
                : pageSize == 40
                ? 450
                : pageSize == 50
                ? 500
                : 250,
          }}>
          {data.length && data.length > 0 ? (
            <RenderRow />
          ) : (
            <div
              style={{
                height: window.innerHeight - 325,
              }}>
              <div className="empty" style={{ height: "100%" }}>
                <EmptyState
                  screenType="newWorkspace"
                  heading="Please select workspace."
                  button={false}
                />
              </div>
            </div>
          )}
        </div>
        {/* <div className="footer">
          <tfoot>
            {footerGroups.map(footerGroups => (
              <tr {...footerGroups.getFooterGroupProps()}>
                {footerGroups.headers.map(column => (
                  <td {...column.getFooterProps}>{column.render("Footer")}</td>
                ))}
              </tr>
            ))}
          </tfoot>
        </div> */}
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "center",
            height: "40px",
          }}>
          <div className="pagination">
            <button
              onClick={() => gotoPage(0)}
              disabled={!canPreviousPage}
              className="page_arrow_Btn">
              {"<<"}
            </button>{" "}
            <button
              onClick={() => previousPage()}
              disabled={!canPreviousPage}
              className="page_arrow_Btn">
              {"<"}
            </button>{" "}
            <button onClick={() => nextPage()} disabled={!canNextPage} className="page_arrow_Btn">
              {">"}
            </button>{" "}
            <button
              onClick={() => gotoPage(pageCount - 1)}
              disabled={!canNextPage}
              className="page_arrow_Btn">
              {">>"}
            </button>{" "}
            <span style={{ fontWeight: 400, fontSize: "13px !important" }}>
              Page{" "}
              <strong>
                {pageIndex + 1} of {pageOptions.length}
              </strong>{" "}
            </span>
            <span>
              | <span style={{ fontWeight: 400, fontSize: "13px !important" }}> Go to page: </span>{" "}
              <DefaultTextField
                label={""}
                errorState={false}
                errorMessage={""}
                formControlStyles={{ marginTop: 0, marginBottom: 0, width: 80 }}
                defaultProps={{
                  type: "number",
                  id: "pageNo",
                  placeholder: "",
                  value: pageIndex + 1,
                  autoFocus: false,
                  // inputProps: { maxLength: 80 },
                  style: { width: 80, height: 30 },
                  onChange: e => {
                    const page = e.target.value ? Number(e.target.value) - 1 : 0;
                    gotoPage(page);
                  },
                }}
              />
            </span>{" "}
            <select
              value={pageSize}
              style={{
                fontWeight: 400,
                fontSize: "13px !important",
                height: 29,
                border: "1px solid #dddddd",
              }}
              onChange={e => {
                setPageSize(Number(e.target.value));
              }}>
              {[10, 20, 30, 40, 50].map(pageSize => (
                <option key={pageSize} value={pageSize}>
                  Show {pageSize}
                </option>
              ))}
            </select>
          </div>
        </div>
      </div>
    </>
  );
}

function CustomTable({ data, profileState, classes, isLoading, headerColumns }) {
  const {
    state: { widgetSettings },
    dispatch,
  } = useContext(TaskOverviewContext);

  return isLoading ? (
    <div
      style={{
        height: window.innerHeight - 360,
        width: "100%",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}>
      <div className="loader" />
    </div>
  ) : (
    <div style={{ position: "relative", overflow: "auto" }} id="custom-scroll">
      <Styles>
        <Table columns={headerColumns} data={data} />
      </Styles>
    </div>
  );
}

const mapStateToProps = state => {
  return {
    profileState: state.profile.data,
  };
};

export default compose(
  withStyles(customTableStyles, { withTheme: true }),
  connect(mapStateToProps)
)(CustomTable);
