const generateTaskColumnsData = [
    {
      label: "Project",
      value: "workspaceName",
    },
    {
      label: "Link",
      value: "projectName",
    },
    {
      label: "Project Manager",
      value: "uniqueId",
    },
    {
      label: "Start",
      value: "statusTitle",
    },
    {
      label: "Finish",
      value: "priority",
    },
    {
      label: "Progress",
      value: "startDate",
    },
    {
      label: "Effort(Hours)",
      value: "dueDate",
    },
    {
      label: "Task Count",
      value: "plannedEffort",
    },
    {
      label: "Overdue Tasks",
      value: "effortToDate",
    },
    {
      label: "Late Tasks",
      value: "actualStartDate",
    },
    {
      label: "On Track Tasks",
      value: "actualDueDate",
    },
    {
      label: "Future Tasks",
      value: "progress",
    },
    {
      label: "Completed Tasks",
      value: "assignee",
    },
 
   
  
  
  
  ];

  export default generateTaskColumnsData