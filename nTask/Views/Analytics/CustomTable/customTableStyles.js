const customTableStyles = theme => ({
  TotalAssignee: {
    background: theme.palette.secondary.lightBlue,
    color: theme.palette.secondary.main,
    width: 30,
    height: 30,
  },
  Footer: {
    backgroundColor: "red",
  },
  page_No_Field: {
    fontWeight: 400,
    fontSize: "13px !important",
    height: 29,
    border: "1px solid #dddddd",
  },
});

export default customTableStyles;
