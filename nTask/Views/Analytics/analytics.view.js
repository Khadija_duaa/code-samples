import React, { useReducer, useState, useEffect } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
//import TasksOverview from "../TasksOverview/TasksOverview.view";
import analyticsStyles from "./analytics.style";
import AnalyticsContent from "./analyticsContent.js";
import ResourceDashboard from "./resourceDashboard";
import { calculateContentHeight } from "../../utils/common";
import { dispatchWidgetSetting, dispatchWidgetData } from "./ProjectOverview/Context/actions";
import ProjectOverviewContext from "./ProjectOverview/Context/ProjectOverview.context";
import initialState from "./ProjectOverview/Context/initialState";
import reducer from "./ProjectOverview/Context/reducer";
import { getWidgetAnalyticsData, getProjectAnalyticsData } from "../../redux/actions/analytics";
import { FetchWorkspaceInfo } from "../../redux/actions/workspace";
import { useDispatch } from "react-redux";
import Loader from "../../components/Loader/Loader";

function Analytics({ classes }) {
  const [state, dispatch] = useReducer(reducer, initialState);
  const dispatchFn = useDispatch();
  const [isLoading, setIsLoading] = useState(true);

  //Getting project data
  useEffect(() => {
    getProjectAnalyticsData(res => {
      dispatchWidgetSetting(dispatch, res);
      getWidgetAnalyticsData("", res => {
        dispatchWidgetData(dispatch, res.data.entity); 
        setIsLoading(false);
      });
    });
    return () => { 
      if (!window.location.pathname.includes("/reports")) {
        FetchWorkspaceInfo(
          "",
          () => { },
          () => { },
          dispatchFn
        );
      }
    }
  }, []);
 const oldSidebarActive =  localStorage.getItem('oldSidebarView')
  return (
    <ProjectOverviewContext.Provider value={{ dispatch, state }}>
      <div className={classes.overviewCnt} >
        {isLoading && (
          <Loader />
        )}
        {!isLoading && <AnalyticsContent />}
      </div>
    </ProjectOverviewContext.Provider>
  );
}

export default withStyles(analyticsStyles, { withTheme: true })(Analytics);
