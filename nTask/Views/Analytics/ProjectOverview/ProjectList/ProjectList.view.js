import React, { useContext, useEffect, useState } from "react";
import CustomTable from "../../CustomTable/CustomTable.cmp";
import ProjectOverviewContext from "../Context/ProjectOverview.context";
import {
  dispatchTaskGroupByAssignee,
  dispatchSelectedColumns,
  dispatchWidgetSetting,
} from "../Context/actions";
import { updateProjectAnalyticsData } from "../../../../redux/actions/analytics";

import generateTaskColumnsData from "./constants";
import { Circle } from "rc-progress";
import Typography from "@material-ui/core/Typography";
import CustomMultiSelectDropdown from "../../../../components/Dropdown/CustomMultiSelectDropdown/Dropdown";
import AddIcon from "@material-ui/icons/Add";
import { defaultTheme } from "../../../../assets/jss/theme";
import isNaN from "lodash/isNaN";
import moment from "moment";

function ProjectList(props) {
  const {
    state: { widgetSettings, projectWidgetData, filteredTasks, groupedTasks },
    dispatch,
  } = useContext(ProjectOverviewContext);
  const { projects } = projectWidgetData;
  const { selectedColumns } = widgetSettings.widgetSettings;

  const [projectData, setProjectData] = useState([]);

  const selectedColumnsOptions = generateTaskColumnsData.filter(c => {
    return selectedColumns.includes(c.value);
  });
  const headerColumns = () => {
    // const columnWidth = `${100 / selectedColumns.length}%`;
    return [
      {
        Header: "Project",
        accessor: "projectName",
        Footer: (
          <div style={{}}>
            <strong>Total</strong>
          </div>
        ),
        width: 300,
        Cell: ({ row }) => {
          return (
            <span style={{ wordBreak: "break-all" }}>
              {row.values.projectName ? row.values.projectName : ""}
            </span>
          );
        },
      },
      {
        Header: "Project Manager",
        accessor: "projectManager",
        // width: columnWidth,
        Cell: ({ row }) => {
          return <span>{row.values.projectManager ? row.values.projectManager : ""}</span>;
        },
      },
      {
        Header: "Project Start",
        accessor: "startDate",
        // width: columnWidth,
        Cell: ({ row }) => {
          return (
            <span>{row.values.startDate ? moment(row.values.startDate).format("ll") : ""}</span>
          );
        },
      },
      {
        Header: "Project Finish",
        accessor: "dueDate",
        // width: columnWidth,
        Cell: ({ row }) => {
          return <span>{row.values.dueDate ? moment(row.values.dueDate).format("ll") : ""}</span>;
        },
      },

      {
        Header: "Progress",
        accessor: "progress",
        // width: columnWidth,
        Cell: ({ row }) => {
          return (
            <>
              <div
                style={{
                  color: "#000",
                  backgroundColor: "#ffffff",
                  width: "100%",
                  height: "20px",
                }}>
                <div
                  style={{
                    width: row.values.progress ? `${row.values.progress}%` : "100%",
                    backgroundColor: row.values.progress ? "#4DD778" : "#ffffff",
                    textAlign: "center",
                    display: "flex",
                    height: "20px",
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                  }}></div>
              </div>
              <span style={{ position: "absolute" }}>
                {row.values.progress ? `${row.values.progress}%` : "0%"}
              </span>
            </>
          );
        },
      },
      {
        Header: "Effort (Hours)",
        accessor: "completedEffortInHours",
        // width: columnWidth,
        Footer: row => {
          let totaleffort = 0;

          row.rows.map(ele => {
            totaleffort = totaleffort + ele.original.completedEffortInHours;
          });
          return (
            <div style={{ paddingLeft: "730px" }}>
              <strong>{totaleffort}</strong>
            </div>
          );
        },
        Cell: ({ row }) => {
          let maxCompletedEffortVal = Math.max(...projectData.map(p => p.completedEffortInHours));
          let effortWidth = 0;
          if (row.original) {
            effortWidth = (row.original.completedEffortInHours / maxCompletedEffortVal) * 100;
            effortWidth = isNaN(effortWidth) ? 0 : effortWidth;
          }
          return (
            <>
              <div
                style={{
                  color: "#000",
                  backgroundColor: "#ffffff",
                  width: "100%",
                  height: "20px",
                }}>
                <div
                  style={{
                    width: effortWidth ? `${effortWidth}%` : "100%",
                    backgroundColor: effortWidth !== 0 ? "#C8C8C8" : "#ffffff",
                    textAlign: "center",
                    display: "flex",
                    height: "20px",
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                  }}></div>
              </div>
              <span style={{ position: "absolute" }}>
                {row.values.completedEffortInHours ? row.values.completedEffortInHours : ""}
              </span>
            </>
          );
        },
      },
      {
        Header: "Task Count",
        accessor: "totalTasks",
        // width: columnWidth,
        Footer: row => {
          let totalTasks = 0;
          row.rows.map(ele => {
            totalTasks = totalTasks + ele.original.totalTasks;
          });
          return (
            <div style={{ paddingLeft: "143px" }}>
              <strong>{totalTasks}</strong>
            </div>
          );
        },
        Cell: ({ row }) => {
          return <span> {row.values.totalTasks ? row.values.totalTasks : ""}</span>;
        },
      },
      {
        Header: "Overdue Tasks",
        accessor: "overDueTasks",
        // width: columnWidth,
        Footer: row => {
          let totaloverduetasks = 0;

          row.rows.map(ele => {
            totaloverduetasks = totaloverduetasks + ele.original.overDueTasks;
          });
          return (
            <div style={{ paddingLeft: "100px" }}>
              <strong>{totaloverduetasks}</strong>
            </div>
          );
        },
        Cell: ({ row }) => {
          let maxoverDueTasksVal = Math.max(...projectData.map(p => p.overDueTasks));

          let overdueWidth = 0;
          if (row.original) {
            overdueWidth = (row.original.overDueTasks / maxoverDueTasksVal) * 100;
            overdueWidth = isNaN(overdueWidth) ? 0 : overdueWidth;
          }
          return (
            <>
              <div
                style={{
                  color: "#000",
                  backgroundColor: "#ffffff",
                  width: "100%",
                  height: "20px",
                }}>
                <div
                  style={{
                    width: overdueWidth ? `${overdueWidth}%` : "100%",
                    backgroundColor: overdueWidth !== 0 ? "#E64E4E" : "#ffffff",
                    textAlign: "center",
                    display: "flex",
                    height: "20px",
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                  }}>
                  {" "}
                </div>
              </div>
              <span style={{ position: "absolute" }}>
                {row.values.overDueTasks ? row.values.overDueTasks : ""}
              </span>
            </>
          );
        },
      },
      {
        Header: "Late Tasks",
        accessor: "lateTasks",
        // width: columnWidth,
        Footer: row => {
          let totallatetask = 0;

          row.rows.map(ele => {
            totallatetask = totallatetask + ele.original.lateTasks;
          });
          return (
            <div style={{ paddingLeft: "100px" }}>
              <strong>{totallatetask}</strong>
            </div>
          );
        },
        Cell: ({ row }) => {
          let maxLateTasksVal = Math.max(...projectData.map(p => p.lateTasks));
          let lateTaskWidth = 0;
          if (row.original) {
            lateTaskWidth = (row.original.lateTasks / maxLateTasksVal) * 100;
            lateTaskWidth = isNaN(lateTaskWidth) ? 0 : lateTaskWidth;
          }

          return (
            <>
              <div
                style={{
                  color: "#000",
                  backgroundColor: "#ffffff",
                  width: "100%",
                  height: "20px",
                }}>
                <div
                  style={{
                    width: lateTaskWidth ? `${lateTaskWidth}%` : "100%",
                    backgroundColor: lateTaskWidth !== 0 ? "#F2C80F" : "#ffffff",
                    textAlign: "center",
                    display: "flex",
                    height: "20px",
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                  }}>
                  {" "}
                </div>
              </div>
              <span style={{ position: "absolute" }}>
                {row.values.lateTasks ? row.values.lateTasks : ""}
              </span>
            </>
          );
        },
      },
      {
        Header: "On Track Tasks",
        accessor: "onTrackTask",
        // width: columnWidth,
        Footer: row => {
          let totalontarcktask = 0;
          row.rows.map(ele => {
            totalontarcktask = totalontarcktask + ele.original.onTrackTask;
          });
          return (
            <div style={{ paddingLeft: "150px" }}>
              <strong>{totalontarcktask}</strong>
            </div>
          );
        },
        Cell: ({ row }) => {
          let maxOnTrackVal = Math.max(...projectData.map(p => p.onTrackTask));
          let onTrackTaskWidth = 0;
          if (row.original) {
            onTrackTaskWidth = (row.original.onTrackTask / maxOnTrackVal) * 100;
            onTrackTaskWidth = isNaN(onTrackTaskWidth) ? 0 : onTrackTaskWidth;
          }
          return (
            <>
              <div
                style={{
                  color: "#000",
                  backgroundColor: "#ffffff",
                  width: "100%",
                  height: "20px",
                }}>
                <div
                  style={{
                    width: onTrackTaskWidth ? `${onTrackTaskWidth}%` : "100%",
                    backgroundColor: onTrackTaskWidth !== 0 ? "#6BC6FF" : "#ffffff",
                    textAlign: "center",
                    display: "flex",
                    height: "20px",
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                  }}>
                  {" "}
                </div>
              </div>
              <span style={{ position: "absolute" }}>
                {row.values.onTrackTask ? row.values.onTrackTask : ""}
              </span>
            </>
          );
        },
      },
      {
        Header: "Future Tasks",
        accessor: "futureTasks",
        // width: columnWidth,
        Footer: row => {
          let totalfuturetask = 0;

          row.rows.map(ele => {
            totalfuturetask = totalfuturetask + ele.original.futureTasks;
          });
          return (
            <div style={{ paddingLeft: "100px" }}>
              <strong>{totalfuturetask}</strong>
            </div>
          );
        },
        Cell: ({ row }) => {
          let maxFutureTasksVal = Math.max(...projectData.map(p => p.futureTasks));
          let futureWidth = 0;
          if (row.original) {
            futureWidth = (row.original.futureTasks / maxFutureTasksVal) * 100;
            futureWidth = isNaN(futureWidth) ? 0 : futureWidth;
          }

          return (
            <>
              <div
                style={{
                  color: "#000",
                  backgroundColor: "#ffffff",
                  width: "100%",
                  height: "20px",
                }}>
                <div
                  style={{
                    width: futureWidth ? `${futureWidth}%` : "100%",
                    backgroundColor: futureWidth !== 0 ? "#FC844F" : "#ffffff",
                    textAlign: "center",
                    display: "flex",
                    height: "20px",
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                  }}>
                  {" "}
                </div>
              </div>
              <span style={{ position: "absolute" }}>
                {row.values.futureTasks ? row.values.futureTasks : ""}
              </span>
            </>
          );
        },
      },
      {
        Header: "Completed Tasks",
        accessor: "completedTasks",
        // width: columnWidth,
        Footer: row => {
          let totalcompletedtask = 0;

          row.rows.map(ele => {
            totalcompletedtask = totalcompletedtask + ele.original.completedTasks;
          });
          return (
            <div style={{ paddingLeft: "130px" }}>
              <strong>{totalcompletedtask}</strong>
            </div>
          );
        },
        Cell: ({ row }) => {
          let maxCompletedTasksVal = Math.max(...projectData.map(p => p.completedTasks));

          let completedWidth = 0;
          if (row.original) {
            completedWidth = (row.original.completedTasks / maxCompletedTasksVal) * 100;
            completedWidth = isNaN(completedWidth) ? 0 : completedWidth;
          }

          return (
            <>
              <div
                style={{
                  color: "#000",
                  backgroundColor: "#ffffff",
                  width: "100%",
                  height: "20px",
                }}>
                <div
                  style={{
                    width: completedWidth ? `${completedWidth}%` : "100%",
                    backgroundColor: completedWidth !== 0 ? "#4DD778" : "#ffffff",
                    textAlign: "center",
                    display: "flex",
                    height: "20px",
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                  }}>
                  {" "}
                </div>
              </div>
              <span style={{ position: "absolute" }}>
                {row.values.completedTasks ? row.values.completedTasks : ""}
              </span>
            </>
          );
        },
      },
      {
        Header: (
          <CustomMultiSelectDropdown
            label=""
            options={() => generateTaskColumnsData}
            option={selectedColumnsOptions}
            open={false}
            iconButton
            icon={<AddIcon style={{ color: defaultTheme.palette.common.white, fontSize: "14px" }} />}
            onSelect={onSelectedColumns}
            maxSelections={11}
            heading={`Customize Column (${selectedColumnsOptions.length}/11)`}
            height="140px"
            width="140px"
            scrollHeight={180}
            buttonProps={{
              btnType: "DarkGray",
              style: { padding: 2 },
            }}
          />
        ),
        accessor: "columnOptions",
        width: 50,
        Cell: ({ row }) => {
          return <></>;
        },
      },
    ];
  };
  const onSelectedColumns = options => {
    const colsArr = options.map(opt => {
      return opt.value;
    }); // set all columns
    let object = {
      ...widgetSettings,
      widgetSettings: {
        ...widgetSettings.widgetSettings,
        selectedColumns: colsArr,
      },
    };
    dispatchWidgetSetting(dispatch, object);
    updateProjectAnalyticsData(
      object,
      // Success
      res => {}
    );
    // dispatchSelectedColumns(dispatch, colsArr);
  };
  // const filteredColumns = headerColumns();
  const filteredColumns = headerColumns().filter(c => {
    return (
      selectedColumns.includes(c.accessor) ||
      c.accessor === "projectName" ||
      c.accessor === "columnOptions"
    );
  });

  useEffect(() => {
    setProjectData(projects);
  }, [projects]);

  return (
    <CustomTable
      data={projectData}
      cols={selectedColumns}
      dispatch={dispatch}
      widgetSettings={widgetSettings}
      isLoading={props.isLoading}
      headerColumns={filteredColumns}
    />
  );
}

export default ProjectList;
