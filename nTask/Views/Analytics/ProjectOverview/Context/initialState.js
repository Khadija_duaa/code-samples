const initialState = {
  widgetSettings: {
    groupBy: [],
    widgetSettings: {
      selectedWorkspaces: [],
      projectManagers: [],
      selectedStatus: [],
      selectedManagers: [],
      selectedColumns: [],
    },
  },
  projectWidgetData: {
    projectCount: 0,
    projectProgress: [],
    projectStatus: [],
    projectSummary: {},
    projects: [],
    projectsbyProgress: [],
    effortByProject: [],
    // effortByProject: [
    //   {
    //     id: "Gantt Access1",
    //     completedEfforts: 2,
    //     remainingEfforts: 4,
    //   },
    //   {
    //     id: "Manual Roller1",
    //     completedEfforts: 6,
    //     remainingEfforts: 8,
    //   },
    //   {
    //     id: "Gantt Access2",
    //     completedEfforts: 9,
    //     remainingEfforts: 12,
    //   },
    //   {
    //     id: "Giveaways1",
    //     completedEfforts: 14,
    //     remainingEfforts: 18,
    //   },
    //   {
    //     id: "Test-APEX01",
    //     completedEfforts: 20,
    //     remainingEfforts: 22,
    //   },
    //   {
    //     id: "Flora 2.00",
    //     completedEfforts: 22,
    //     remainingEfforts: 30,
    //   },
    //   {
    //     id: "Gantt Access3",
    //     completedEfforts: 35,
    //     remainingEfforts: 45,
    //   },
    //   {
    //     id: "Manual Roller2",
    //     completedEfforts: 50,
    //     remainingEfforts: 75,
    //   },
    //   {
    //     id: "Gantt Access4",
    //     completedEfforts: 80,
    //     remainingEfforts: 55,
    //   },
    //   {
    //     id: "Giveaways2",
    //     completedEfforts: 90,
    //     remainingEfforts: 65,
    //   },
    //   {
    //     id: "Test-APEX02",
    //     completedEfforts: 30,
    //     remainingEfforts: 50,
    //   },
    //   {
    //     id: "Flora 2.01",
    //     completedEfforts: 40,
    //     remainingEfforts: 18,
    //   },
    //   {
    //     id: "Gantt Access5",
    //     completedEfforts: 8,
    //     remainingEfforts: 30,
    //   },
    //   {
    //     id: "Manual Roller3",
    //     completedEfforts: 55,
    //     remainingEfforts: 66,
    //   },
    //   {
    //     id: "Gantt Access6",
    //     completedEfforts: 77,
    //     remainingEfforts: 88,
    //   },
    //   {
    //     id: "Giveaways3",
    //     completedEfforts: 10,
    //     remainingEfforts: 25,
    //   },
    //   {
    //     id: "Test-APEX03",
    //     completedEfforts: 30,
    //     remainingEfforts: 39,
    //   },
    //   {
    //     id: "Flora 2.02",
    //     completedEfforts: 10,
    //     remainingEfforts: 52,
    //   },
    //   {
    //     id: "Gantt Access7",
    //     completedEfforts: 26,
    //     remainingEfforts: 53,
    //   },
    //   {
    //     id: "Manual Roller4",
    //     completedEfforts: 22,
    //     remainingEfforts: 33,
    //   },
    //   {
    //     id: "Gantt Access8",
    //     completedEfforts: 80,
    //     remainingEfforts: 93,
    //   },
    //   {
    //     id: "Giveaways4",
    //     completedEfforts: 25,
    //     remainingEfforts: 36,
    //   },
    //   {
    //     id: "Test-APEX04",
    //     completedEfforts: 44,
    //     remainingEfforts: 35,
    //   },
    //   {
    //     id: "Flora 2.03",
    //     completedEfforts: 65,
    //     remainingEfforts: 83,
    //   },
    //   {
    //     id: "Gantt Access9",
    //     completedEfforts: 75,
    //     remainingEfforts: 83,
    //   },
    //   {
    //     id: "Manual Roller5",
    //     completedEfforts: 92,
    //     remainingEfforts: 100,
    //   },
    //   {
    //     id: "Gantt Access10",
    //     completedEfforts: 46,
    //     remainingEfforts: 76,
    //   },
    //   {
    //     id: "Giveaways5",
    //     completedEfforts: 68,
    //     remainingEfforts: 85,
    //   },
    //   {
    //     id: "Test-APEX05",
    //     completedEfforts: 85,
    //     remainingEfforts: 98,
    //   },
    //   {
    //     id: "Flora 2.04",
    //     completedEfforts: 9,
    //     remainingEfforts: 12,
    //   },
    //   {
    //     id: "Gantt Access11",
    //     completedEfforts: 25,
    //     remainingEfforts: 35,
    //   },
    //   {
    //     id: "Manual Roller6",
    //     completedEfforts: 36,
    //     remainingEfforts: 56,
    //   },
    //   {
    //     id: "Gantt Access12",
    //     completedEfforts: 45,
    //     remainingEfforts: 65,
    //   },
    //   {
    //     id: "Giveaways6",
    //     completedEfforts: 13,
    //     remainingEfforts: 32,
    //   },
    //   {
    //     id: "Test-APEX06",
    //     completedEfforts: 46,
    //     remainingEfforts: 65,
    //   },
    //   {
    //     id: "Flora 2.05",
    //     completedEfforts: 79,
    //     remainingEfforts: 98,
    //   },
    //   {
    //     id: "Gantt Access13",
    //     completedEfforts: 14,
    //     remainingEfforts: 74,
    //   },
    //   {
    //     id: "Manual Roller7",
    //     completedEfforts: 25,
    //     remainingEfforts: 85,
    //   },
    //   {
    //     id: "Gantt Access14",
    //     completedEfforts: 65,
    //     remainingEfforts: 69,
    //   },
    //   {
    //     id: "Giveaways7",
    //     completedEfforts: 56,
    //     remainingEfforts: 68,
    //   },
    //   {
    //     id: "Test-APEX07",
    //     completedEfforts: 46,
    //     remainingEfforts: 59,
    //   },
    //   {
    //     id: "Flora 2.06",
    //     completedEfforts: 68,
    //     remainingEfforts: 89,
    //   },
    //   {
    //     id: "Gantt Access15",
    //     completedEfforts: 45,
    //     remainingEfforts: 65,
    //   },
    //   {
    //     id: "Manual Roller8",
    //     completedEfforts: 75,
    //     remainingEfforts: 95,
    //   },
    //   {
    //     id: "Gantt Access16",
    //     completedEfforts: 65,
    //     remainingEfforts: 95,
    //   },
    //   {
    //     id: "Giveaways8",
    //     completedEfforts: 66,
    //     remainingEfforts: 88,
    //   },
    //   {
    //     id: "Test-APEX08",
    //     completedEfforts: 55,
    //     remainingEfforts: 68,
    //   },
    //   {
    //     id: "Flora 2.07",
    //     completedEfforts: 59,
    //     remainingEfforts: 69,
    //   },
    //   {
    //     id: "Gantt Access17",
    //     completedEfforts: 75,
    //     remainingEfforts: 83,
    //   },
    //   {
    //     id: "Manual Roller9",
    //     completedEfforts: 9,
    //     remainingEfforts: 10,
    //   },
    //   {
    //     id: "Manual Roller 102",
    //     completedEfforts: 66,
    //     remainingEfforts: 88,
    //   },
    //   {
    //     id: "Gantt Access 103",
    //     completedEfforts: 9,
    //     remainingEfforts: 12,
    //   },
    //   {
    //     id: "Giveaways 104",
    //     completedEfforts: 14,
    //     remainingEfforts: 18,
    //   },
    //   {
    //     id: "Test-APEX01 105",
    //     completedEfforts: 20,
    //     remainingEfforts: 22,
    //   },
    //   {
    //     id: "Flora 2.0 106",
    //     completedEfforts: 22,
    //     remainingEfforts: 30,
    //   },
    //   {
    //     id: "Gantt Access 107",
    //     completedEfforts: 35,
    //     remainingEfforts: 45,
    //   },
    //   {
    //     id: "Manual Roller 108",
    //     completedEfforts: 70,
    //     remainingEfforts: 75,
    //   },
    //   {
    //     id: "Gantt Access 109",
    //     completedEfforts: 81,
    //     remainingEfforts: 93,
    //   },
    //   {
    //     id: "Giveaways 110",
    //     completedEfforts: 10,
    //     remainingEfforts: 65,
    //   },
    //   {
    //     id: "Test-APEX01 111",
    //     completedEfforts: 30,
    //     remainingEfforts: 42,
    //   },
    //   {
    //     id: "Flora 2.0 112",
    //     completedEfforts: 46,
    //     remainingEfforts: 18,
    //   },
    //   {
    //     id: "Gantt Access 113",
    //     completedEfforts: 81,
    //     remainingEfforts: 39,
    //   },
    //   {
    //     id: "Manual Roller 114",
    //     completedEfforts: 95,
    //     remainingEfforts: 46,
    //   },
    //   {
    //     id: "Gantt Access 115",
    //     completedEfforts: 66,
    //     remainingEfforts: 88,
    //   },
    //   {
    //     id: "Giveaways 116",
    //     completedEfforts: 40,
    //     remainingEfforts: 65,
    //   },
    //   {
    //     id: "Test-APEX01 117",
    //     completedEfforts: 60,
    //     remainingEfforts: 29,
    //   },
    //   {
    //     id: "Flora 2.0 118",
    //     completedEfforts: 60,
    //     remainingEfforts: 62,
    //   },
    //   {
    //     id: "Gantt Access 119",
    //     completedEfforts: 26,
    //     remainingEfforts: 53,
    //   },
    //   {
    //     id: "Manual Roller 120",
    //     completedEfforts: 25,
    //     remainingEfforts: 33,
    //   },
    //   {
    //     id: "Gantt Access 121",
    //     completedEfforts: 86,
    //     remainingEfforts: 93,
    //   },
    //   {
    //     id: "Giveaways 122",
    //     completedEfforts: 22,
    //     remainingEfforts: 36,
    //   },
    //   {
    //     id: "Test-APEX01 123",
    //     completedEfforts: 94,
    //     remainingEfforts: 36,
    //   },
    //   {
    //     id: "Flora 2.0 124",
    //     completedEfforts: 22,
    //     remainingEfforts: 83,
    //   },
    //   {
    //     id: "Gantt Access 125",
    //     completedEfforts: 75,
    //     remainingEfforts: 94,
    //   },
    //   {
    //     id: "Manual Roller 126",
    //     completedEfforts: 92,
    //     remainingEfforts: 100,
    //   },
    //   {
    //     id: "Gantt Access 127",
    //     completedEfforts: 33,
    //     remainingEfforts: 66,
    //   },
    //   {
    //     id: "Giveaways 128",
    //     completedEfforts: 1,
    //     remainingEfforts: 10,
    //   },
    //   {
    //     id: "Test-APEX01 129",
    //     completedEfforts: 55,
    //     remainingEfforts: 75,
    //   },
    //   {
    //     id: "Flora 2.0 130",
    //     completedEfforts: 99,
    //     remainingEfforts: 100,
    //   },
    //   {
    //     id: "Gantt Access 131",
    //     completedEfforts: 11,
    //     remainingEfforts: 36,
    //   },
    //   {
    //     id: "Manual Roller 132",
    //     completedEfforts: 16,
    //     remainingEfforts: 96,
    //   },
    //   {
    //     id: "Gantt Access 133",
    //     completedEfforts: 45,
    //     remainingEfforts: 35,
    //   },
    //   {
    //     id: "Giveaways 134",
    //     completedEfforts: 30,
    //     remainingEfforts: 92,
    //   },
    //   {
    //     id: "Test-APEX01 135",
    //     completedEfforts: 1,
    //     remainingEfforts: 1,
    //   },
    //   {
    //     id: "Flora 2.0 136",
    //     completedEfforts: 23,
    //     remainingEfforts: 50,
    //   },
    //   {
    //     id: "Gantt Access 138",
    //     completedEfforts: 14,
    //     remainingEfforts: 54,
    //   },
    //   {
    //     id: "Manual Roller 139",
    //     completedEfforts: 25,
    //     remainingEfforts: 15,
    //   },
    //   {
    //     id: "Gantt Access 140",
    //     completedEfforts: 15,
    //     remainingEfforts: 69,
    //   },
    //   {
    //     id: "Giveaways 141",
    //     completedEfforts: 16,
    //     remainingEfforts: 68,
    //   },
    //   {
    //     id: "Test-APEX01 142",
    //     completedEfforts: 16,
    //     remainingEfforts: 59,
    //   },
    //   {
    //     id: "Flora 2.0 143",
    //     completedEfforts: 68,
    //     remainingEfforts: 99,
    //   },
    //   {
    //     id: "Gantt Access 144",
    //     completedEfforts: 45,
    //     remainingEfforts: 56,
    //   },
    //   {
    //     id: "Manual Roller 145",
    //     completedEfforts: 75,
    //     remainingEfforts: 99,
    //   },
    //   {
    //     id: "Gantt Access 146",
    //     completedEfforts: 65,
    //     remainingEfforts: 66,
    //   },
    //   {
    //     id: "Giveaways 147",
    //     completedEfforts: 66,
    //     remainingEfforts: 98,
    //   },
    //   {
    //     id: "Test-APEX01 148",
    //     completedEfforts: 55,
    //     remainingEfforts: 18,
    //   },
    //   {
    //     id: "Flora 2.0 149",
    //     completedEfforts: 59,
    //     remainingEfforts: 6,
    //   },
    //   {
    //     id: "Gantt Access 150",
    //     completedEfforts: 75,
    //     remainingEfforts: 99,
    //   },
    //   {
    //     id: "Manual Roller 151",
    //     completedEfforts: 92,
    //     remainingEfforts: 89,
    //   },
    //   {
    //     id: "Gantt Access 49",
    //     completedEfforts: 2,
    //     remainingEfforts: 14,
    //   },
    //   {
    //     id: "Manual Roller 48",
    //     completedEfforts: 6,
    //     remainingEfforts: 86,
    //   },
    //   {
    //     id: "Gantt Access 47",
    //     completedEfforts: 9,
    //     remainingEfforts: 1,
    //   },
    //   {
    //     id: "Giveaways 46",
    //     completedEfforts: 14,
    //     remainingEfforts: 56,
    //   },
    //   {
    //     id: "Test-APEX01 45",
    //     completedEfforts: 20,
    //     remainingEfforts: 29,
    //   },
    //   {
    //     id: "Flora 2.0 44",
    //     completedEfforts: 22,
    //     remainingEfforts: 86,
    //   },
    //   {
    //     id: "Gantt Access 43",
    //     completedEfforts: 35,
    //     remainingEfforts: 45,
    //   },
    //   {
    //     id: "Manual Roller 42",
    //     completedEfforts: 50,
    //     remainingEfforts: 70,
    //   },
    //   {
    //     id: "Gantt Access 41",
    //     completedEfforts: 80,
    //     remainingEfforts: 95,
    //   },
    //   {
    //     id: "Giveaways 40",
    //     completedEfforts: 90,
    //     remainingEfforts: 35,
    //   },
    //   {
    //     id: "Test-APEX01 39",
    //     completedEfforts: 10,
    //     remainingEfforts: 20,
    //   },
    //   {
    //     id: "Flora 2.0 38",
    //     completedEfforts: 9,
    //     remainingEfforts: 18,
    //   },
    //   {
    //     id: "Gantt Access 37",
    //     completedEfforts: 55,
    //     remainingEfforts: 69,
    //   },
    //   {
    //     id: "Manual Roller 36",
    //     completedEfforts: 50,
    //     remainingEfforts: 90,
    //   },
    //   {
    //     id: "Gantt Access 35",
    //     completedEfforts: 77,
    //     remainingEfforts: 80,
    //   },
    //   {
    //     id: "Giveaways 34",
    //     completedEfforts: 10,
    //     remainingEfforts: 95,
    //   },
    //   {
    //     id: "Test-APEX01 33",
    //     completedEfforts: 30,
    //     remainingEfforts: 49,
    //   },
    //   {
    //     id: "Flora 2.0 32",
    //     completedEfforts: 16,
    //     remainingEfforts: 52,
    //   },
    //   {
    //     id: "Gantt Access 31",
    //     completedEfforts: 261,
    //     remainingEfforts: 53,
    //   },
    //   {
    //     id: "Manual Roller 30",
    //     completedEfforts: 8,
    //     remainingEfforts: 55,
    //   },
    //   {
    //     id: "Gantt Access 29",
    //     completedEfforts: 80,
    //     remainingEfforts: 93,
    //   },
    //   {
    //     id: "Giveaways 28",
    //     completedEfforts: 25,
    //     remainingEfforts: 100,
    //   },
    //   {
    //     id: "Test-APEX01 27",
    //     completedEfforts: 1,
    //     remainingEfforts: 2,
    //   },
    //   {
    //     id: "Flora 2.0 26",
    //     completedEfforts: 60,
    //     remainingEfforts: 83,
    //   },
    //   {
    //     id: "Gantt Access 25",
    //     completedEfforts: 75,
    //     remainingEfforts: 83,
    //   },
    //   {
    //     id: "Manual Roller 24",
    //     completedEfforts: 92,
    //     remainingEfforts: 100,
    //   },
    //   {
    //     id: "Gantt Access 23",
    //     completedEfforts: 46,
    //     remainingEfforts: 76,
    //   },
    //   {
    //     id: "Giveaways 22",
    //     completedEfforts: 33,
    //     remainingEfforts: 85,
    //   },
    //   {
    //     id: "Test-APEX01 21",
    //     completedEfforts: 85,
    //     remainingEfforts: 80,
    //   },
    //   {
    //     id: "Flora 2.0 20",
    //     completedEfforts: 52,
    //     remainingEfforts: 122,
    //   },
    //   {
    //     id: "Gantt Access 19",
    //     completedEfforts: 1,
    //     remainingEfforts: 99,
    //   },
    //   {
    //     id: "Manual Roller 18",
    //     completedEfforts: 31,
    //     remainingEfforts: 26,
    //   },
    //   {
    //     id: "Gantt Access 17",
    //     completedEfforts: 55,
    //     remainingEfforts: 85,
    //   },
    //   {
    //     id: "Giveaways 16",
    //     completedEfforts: 130,
    //     remainingEfforts: 56,
    //   },
    //   {
    //     id: "Test-APEX01 15",
    //     completedEfforts: 6,
    //     remainingEfforts: 1,
    //   },
    //   {
    //     id: "Flora 2.0 14",
    //     completedEfforts: 2,
    //     remainingEfforts: 1,
    //   },
    //   {
    //     id: "Gantt Access 13",
    //     completedEfforts: 144,
    //     remainingEfforts: 75,
    //   },
    //   {
    //     id: "Manual Roller 12",
    //     completedEfforts: 25,
    //     remainingEfforts: 100,
    //   },
    //   {
    //     id: "Gantt Access 11",
    //     completedEfforts: 65,
    //     remainingEfforts: 111,
    //   },
    //   {
    //     id: "Giveaways 10",
    //     completedEfforts: 32,
    //     remainingEfforts: 7,
    //   },
    //   {
    //     id: "Test-APEX01 9",
    //     completedEfforts: 18,
    //     remainingEfforts: 9,
    //   },
    //   {
    //     id: "Flora 2.0 8",
    //     completedEfforts: 120,
    //     remainingEfforts: 12,
    //   },
    //   {
    //     id: "Gantt Access 7",
    //     completedEfforts: 15,
    //     remainingEfforts: 69,
    //   },
    //   {
    //     id: "Manual Roller 6",
    //     completedEfforts: 50,
    //     remainingEfforts: 50,
    //   },
    //   {
    //     id: "Gantt Access 5",
    //     completedEfforts: 99,
    //     remainingEfforts: 1,
    //   },
    //   {
    //     id: "Giveaways 4",
    //     completedEfforts: 55,
    //     remainingEfforts: 45,
    //   },
    //   {
    //     id: "Test-APEX01 3",
    //     completedEfforts: 55,
    //     remainingEfforts: 5,
    //   },
    //   {
    //     id: "Flora 2.0 2",
    //     completedEfforts: 19,
    //     remainingEfforts: 99,
    //   },
    //   {
    //     id: "Gantt Access 1",
    //     completedEfforts: 75,
    //     remainingEfforts: 0,
    //   },
    //   {
    //     id: "Manual Roller 0",
    //     completedEfforts: 90,
    //     remainingEfforts: 100,
    //   },
    // ],
    projectByProjectManagers: [],
  },
  filteredTasks: {
    assignedToMe: [],
    dueToday: [],
    dueInFiveDays: [],
    overDueTasks: [],
    unscheduledTasks: [],
  },
  groupedTasks: [],
};

export default initialState;
