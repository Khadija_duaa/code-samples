import { createContext } from "react";

const ProjectOverviewContext = createContext({});

export default ProjectOverviewContext;
