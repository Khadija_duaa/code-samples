import { SET_WIDGET_SETTINGS, SET_GROUPBY_ASSIGNEE, SET_WIDGET_TASK_DATA, SET_WIDGET_DATA, SET_SELECTED_COLUMN } from "./constants";

export const dispatchWidgetSetting = (dispatch, obj) => {
  dispatch({ type: SET_WIDGET_SETTINGS, payload: obj });
};
export const dispatchTaskWidgetData = (dispatch, obj) => {
  dispatch({ type: SET_WIDGET_TASK_DATA, payload: obj });
};
export const dispatchTaskGroupByAssignee = (dispatch, data) => {
  dispatch({ type: SET_GROUPBY_ASSIGNEE, payload: data });
};
export const dispatchWidgetData = (dispatch, data) => {
  dispatch({ type: SET_WIDGET_DATA, payload: data });
};
export const dispatchSelectedColumns = (dispatch, data) => {
  dispatch({ type: SET_SELECTED_COLUMN, payload: data });
};
