import React, { useReducer, useEffect, useState } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import ProjectOverviewHeader from "./ProjectOverviewHeader/ProjectOverviewHeader.view";
import projectOverviewStyles from "./ProjectOverview.style";
import ProjectOverviewContext from "./Context/ProjectOverview.context";
import reducer from "./Context/reducer";
import initialState from "./Context/initialState";
import { getWidgetSetting, getWidget } from "../../../redux/actions/overviews";  
import { dispatchWidgetSetting, dispatchTaskWidgetData } from "./Context/actions";
import ProjectList from "./ProjectList/ProjectList.view";

function ProjectOverview({ classes }) {
  // const [state, dispatch] = useReducer(reducer, initialState);
  const [isLoading, setIsLoading] = useState(false);
  // useEffect(() => {
  //   getWidget(
  //     "Task",
  //     //Success
  //     res => {
  //       setIsLoading(false);
  //       dispatchTaskWidgetData(dispatch, res.data.entity);
  //     },
  //     err => {
  //       setIsLoading(false);
  //       dispatchTaskWidgetData(dispatch, {userTasks: [], issues: [], risks: []});
  //     }
  //   );
  // }, []);
  return (
    
      <div className={classes.projectOverview}>
        
     
            <ProjectOverviewHeader /> <ProjectList isLoading={isLoading}/>
          
      </div>
  );
}

export default withStyles(projectOverviewStyles, { withTheme: true })(ProjectOverview);
