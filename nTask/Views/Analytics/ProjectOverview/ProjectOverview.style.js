const projectOverviewStyles = theme => ({
  projectOverview: {
    background: theme.palette.common.white,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: 5,
    margin: 7,
    marginBottom: 35,
    width: "99.3%",
    "& *": {
      fontFamily: theme.typography.fontFamilyLato,
    },
  },
});

export default projectOverviewStyles;
