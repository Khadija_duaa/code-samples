import React from 'react';
import PropTypes from 'prop-types';
import styles from "./analytics.style.js"
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import CustomMultiSelectDropdown from "../../components/Dropdown/CustomMultiSelectDropdown/Dropdown";
import ntaskLogo from "../../assets/images/svgs/nTaskLogo.svg";
import { ResponsivePie } from '@nivo/pie';
import { ResponsiveBar } from '@nivo/bar';
import ImportExportDD from "../../components/Dropdown/ImportExportDD";
import CustomButton from "../../components/Buttons/CustomButton";
import ImportExport from "@material-ui/icons/ImportExport";
class ResourceDashboard extends React.Component {
  state = {
    alignment: 'left',
    formats: ['bold'],
    age: '',
    name: 'hai',
    labelWidth: 0,
  };
  /*bar chart*/

  bardata = [
    {
      "country": "AD",
      "hot dog": 3,
      "hot dogColor": "hsl(339, 70%, 50%)",
      "burger": 1,
      "burgerColor": "hsl(65, 70%, 50%)",
      "sandwich": 69,
      "sandwichColor": "hsl(18, 70%, 50%)",
      "kebab": 32,
      "kebabColor": "hsl(180, 70%, 50%)",
      "fries": 146,
      "friesColor": "hsl(216, 70%, 50%)",
      "donut": 43,
      "donutColor": "hsl(318, 70%, 50%)"
    },
    {
      "country": "AE",
      "hot dog": 13,
      "hot dogColor": "hsl(248, 70%, 50%)",
      "burger": 175,
      "burgerColor": "hsl(317, 70%, 50%)",
      "sandwich": 171,
      "sandwichColor": "hsl(235, 70%, 50%)",
      "kebab": 165,
      "kebabColor": "hsl(274, 70%, 50%)",
      "fries": 36,
      "friesColor": "hsl(174, 70%, 50%)",
      "donut": 116,
      "donutColor": "hsl(347, 70%, 50%)"
    },
    {
      "country": "AF",
      "hot dog": 178,
      "hot dogColor": "hsl(145, 70%, 50%)",
      "burger": 198,
      "burgerColor": "hsl(93, 70%, 50%)",
      "sandwich": 119,
      "sandwichColor": "hsl(193, 70%, 50%)",
      "kebab": 108,
      "kebabColor": "hsl(0, 70%, 50%)",
      "fries": 129,
      "friesColor": "hsl(8, 70%, 50%)",
      "donut": 25,
      "donutColor": "hsl(248, 70%, 50%)"
    },
    {
      "country": "AG",
      "hot dog": 118,
      "hot dogColor": "hsl(182, 70%, 50%)",
      "burger": 103,
      "burgerColor": "hsl(236, 70%, 50%)",
      "sandwich": 123,
      "sandwichColor": "hsl(57, 70%, 50%)",
      "kebab": 59,
      "kebabColor": "hsl(117, 70%, 50%)",
      "fries": 58,
      "friesColor": "hsl(10, 70%, 50%)",
      "donut": 102,
      "donutColor": "hsl(175, 70%, 50%)"
    },
    {
      "country": "AI",
      "hot dog": 11,
      "hot dogColor": "hsl(203, 70%, 50%)",
      "burger": 197,
      "burgerColor": "hsl(337, 70%, 50%)",
      "sandwich": 104,
      "sandwichColor": "hsl(91, 70%, 50%)",
      "kebab": 193,
      "kebabColor": "hsl(171, 70%, 50%)",
      "fries": 71,
      "friesColor": "hsl(92, 70%, 50%)",
      "donut": 74,
      "donutColor": "hsl(197, 70%, 50%)"
    },
    {
      "country": "AL",
      "hot dog": 32,
      "hot dogColor": "hsl(346, 70%, 50%)",
      "burger": 24,
      "burgerColor": "hsl(192, 70%, 50%)",
      "sandwich": 87,
      "sandwichColor": "hsl(345, 70%, 50%)",
      "kebab": 161,
      "kebabColor": "hsl(346, 70%, 50%)",
      "fries": 65,
      "friesColor": "hsl(198, 70%, 50%)",
      "donut": 57,
      "donutColor": "hsl(98, 70%, 50%)"
    },
    {
      "country": "AM",
      "hot dog": 163,
      "hot dogColor": "hsl(287, 70%, 50%)",
      "burger": 122,
      "burgerColor": "hsl(258, 70%, 50%)",
      "sandwich": 9,
      "sandwichColor": "hsl(120, 70%, 50%)",
      "kebab": 37,
      "kebabColor": "hsl(337, 70%, 50%)",
      "fries": 110,
      "friesColor": "hsl(11, 70%, 50%)",
      "donut": 93,
      "donutColor": "hsl(333, 70%, 50%)"
    }
  ]
  /*
   graph data
*/
  data = [
    {
      "id": "php",
      "label": "php",
      "value": 57,
      "color": "hsl(179, 70%, 50%)"
    },
    {
      "id": "css",
      "label": "css",
      "value": 469,
      "color": "hsl(74, 70%, 50%)"
    },
    {
      "id": "python",
      "label": "python",
      "value": 60,
      "color": "hsl(156, 70%, 50%)"
    },
    {
      "id": "erlang",
      "label": "erlang",
      "value": 22,
      "color": "hsl(176, 70%, 50%)"
    },
    {
      "id": "lisp",
      "label": "lisp",
      "value": 151,
      "color": "hsl(9, 70%, 50%)"
    }
  ]

  handleChange = (event) => {

  };
  setValue = () => { }

  handleFormat = (event, formats) => this.setState({ formats });

  handleAlignment = (event, alignment) => this.setState({ alignment });

  render() {
    const { classes } = this.props;
    const { alignment, formats } = this.state;

    return (
      /*heading*/ 
      <Grid container spacing={2}>
        <Grid container direction="row"
          justify="space-between"
          alignItems="center">
          <Grid item sm={12} md={3} className={classes.gridStyle} >
            <div className={classes.dashboardTitle}>

              <img src={ntaskLogo} className={classes.headerLogo} />
              <label>Resource Dashboard</label>

            </div>
          </Grid>
          {/* first select dropdown */}
          <Grid item sm={12} md={2} className={classes.gridStyle} >
         
            <CustomMultiSelectDropdown
              label="Workspace:"
              options={() => []}
              option={() => []}
              onSelect={() => []}
              height="140px"
              scrollHeight={180}
              buttonProps={{
                variant: "contained",
                btnType: "white",
                labelAlign: "left",
                style: { minWidth: "100%", padding: "3px 4px 3px 8px" },
              }}

            />
          
          </Grid>
          {/* second select dropdown */}
          <Grid item sm={12} md={2} className={classes.gridStyle} >
            <CustomMultiSelectDropdown
              label="Project:"
              options={() => []}
              option={() => []}
              onSelect={() => []}
              height="140px"
              scrollHeight={180}
              buttonProps={{
                variant: "contained",
                btnType: "white",
                labelAlign: "left",
                style: { minWidth: "100%", padding: "3px 4px 3px 8px" },
              }}

            />
           
          </Grid>
          {/* third select dropdown */}
          <Grid item sm={12} md={2} className={classes.gridStyle} >
            
            <CustomMultiSelectDropdown
              label="Task Status:"
              options={() => []}
              option={() => []}
              onSelect={() => []}
              height="140px"
              scrollHeight={180}
              buttonProps={{
                variant: "contained",
                btnType: "white",
                labelAlign: "left",
                style: { minWidth: "100%", padding: "3px 4px 3px 8px" },
              }}
            />
            
            <div className={classes.horizontalDivider}></div>
           
          </Grid>
          {/* fourth select dropdown */}
          <Grid item sm={12} md={2} className={classes.gridStyle} >
            
            <CustomMultiSelectDropdown
              label="Resource:"
              options={() => []}
              option={() => []}
              onSelect={() => []}
              height="140px"
              scrollHeight={180}
              buttonProps={{
                variant: "contained",
                btnType: "white",
                labelAlign: "left",
                style: { minWidth: "100%", padding: "3px 4px 3px 8px" },
              }}
            />
            
            <div className={classes.horizontalDivider}></div>
           
          </Grid>
          {/* export button */}

          <Grid  item sm={12} md={1} className={classes.gridStyle} >
          <CustomButton /** Sort By Button  */
              onClick={event => {
                // handleClickBtn(event);
              }}
              ref={null}
              style={{
                // minWidth: 95,
                padding: "3px 4px",
                display: "flex",
                justifyContent: "space-between",
                minWidth: "auto",
                whiteSpace: "nowrap",
                marginRight: 5,
                height: 34,
              }}
              btnType={"white"}
              variant="contained">
              <ImportExport classes={{ root: classes.importExportIcon }} />
                
              <span className={classes.sortLbl}>
                Export
              </span>
            </CustomButton>
          </Grid>
        </Grid>

        {/*colored boxes*/}
        <Grid container direction="row"
          justify="space-between"
          alignItems="center">
          <Grid item sm={6} md={2} className={classes.gridStyle} spacing={2}>
            <div style={{ background: "#2EBAE6" }} className={classes.content}>
              <label className={classes.contentHeading}>Resources</label>
              <label>20</label>
            </div>
          </Grid>
          <Grid item sm={6} md={2} className={classes.gridStyle} spacing={2}>

            <div style={{ background: "#F6C111" }} className={classes.content}>
              <label className={classes.contentHeading}>Not Started Tasks</label>
              <label >18k</label>
              <label className={classes.time}>Hours</label>
            </div>
          </Grid>
          <Grid item sm={6} md={2} className={classes.gridStyle} spacing={2}>
            <div style={{ background: "#29C8A2" }} className={classes.content}>
              <label className={classes.contentHeading}>In Progress Tasks</label>
              <label>6,596</label>
              <label className={classes.time}>Hours</label>
            </div>
          </Grid>
          <Grid item sm={6} md={2} className={classes.gridStyle} spacing={2}>
            <div style={{ background: "#465457" }} className={classes.content}>
              <label className={classes.contentHeading}>Completed Tasks</label>
              <label>11k</label>
              <label className={classes.time}>Hours</label>
            </div>
          </Grid>
          <Grid item sm={6} md={2} className={classes.gridStyle} spacing={2}>
            <div style={{ background: "#E64E4E" }} className={classes.content}>
              <label className={classes.contentHeading}>Effort Completed</label>
              <label>11k</label>
            </div>
          </Grid>
          <Grid item sm={6} md={2} className={classes.gridStyle} spacing={2}>
            <div style={{ background: "#E64E4E" }} className={classes.content}>
              <label className={classes.contentHeading}>Effort Remaining</label>
              <label>11k</label>
            </div>
          </Grid>
          <Grid item sm={6} md={2} className={classes.gridStyle} spacing={2}>
            <div style={{ background: "#E64E4E" }} className={classes.content}>
              <label className={classes.contentHeading}>Issues Open</label>
              <label>11k</label>
            </div>
          </Grid>
          <Grid item sm={6} md={2} className={classes.gridStyle} spacing={2}>
            <div style={{ background: "#FC844F" }} className={classes.content}>
              <label className={classes.contentHeading}>Risks</label>
              <label>11k</label>
              <label className={classes.time}>Hours</label>
            </div>
          </Grid>
        </Grid>
        {/*Donut Graphs*/}
        <Grid container direction="row"
          justify="space-between"
          alignItems="center">
          <Grid item sm={12} md={4} spacing={2} className={classes.gridStyle}  >
            <div className={classes.graphContent}>
              <label className={classes.graphHeading}>Resource Tasks By Progress</label>
              <ResponsivePie
                data={this.data}
                margin={{ top: 40, right: 80, bottom: 80, left: 80 }}
                innerRadius={0.5}
                padAngle={0.7}
                cornerRadius={3}
                colors={{ scheme: 'nivo' }}
                borderWidth={1}
                borderColor={{ from: 'color', modifiers: [['darker', 0.2]] }}
                radialLabelsSkipAngle={10}
                radialLabelsTextColor="#333333"
                radialLabelsLinkColor={{ from: 'color' }}
                sliceLabelsSkipAngle={10}
                sliceLabelsTextColor="#333333"
                defs={[
                  {
                    id: 'dots',
                    type: 'patternDots',
                    background: 'inherit',
                    color: 'rgba(255, 255, 255, 0.3)',
                    size: 4,
                    padding: 1,
                    stagger: true
                  },
                  {
                    id: 'lines',
                    type: 'patternLines',
                    background: 'inherit',
                    color: 'rgba(255, 255, 255, 0.3)',
                    rotation: -45,
                    lineWidth: 6,
                    spacing: 10
                  }
                ]}
                fill={[
                  {
                    match: {
                      id: 'ruby'
                    },
                    id: 'dots'
                  },
                  {
                    match: {
                      id: 'c'
                    },
                    id: 'dots'
                  },
                  {
                    match: {
                      id: 'go'
                    },
                    id: 'dots'
                  },
                  {
                    match: {
                      id: 'python'
                    },
                    id: 'dots'
                  },
                  {
                    match: {
                      id: 'scala'
                    },
                    id: 'lines'
                  },
                  {
                    match: {
                      id: 'lisp'
                    },
                    id: 'lines'
                  },
                  {
                    match: {
                      id: 'elixir'
                    },
                    id: 'lines'
                  },
                  {
                    match: {
                      id: 'javascript'
                    },
                    id: 'lines'
                  }
                ]}
              // legends={[
              //     {
              //         anchor: 'bottom',
              //         direction: 'row',
              //         justify: false,
              //         translateX: 0,
              //         translateY: 56,
              //         itemsSpacing: 0,
              //         itemWidth: 100,
              //         itemHeight: 18,
              //         itemTextColor: '#999',
              //         itemDirection: 'left-to-right',
              //         itemOpacity: 1,
              //         symbolSize: 18,
              //         symbolShape: 'circle',
              //         effects: [
              //             {
              //                 on: 'hover',
              //                 style: {
              //                     itemTextColor: '#000'
              //                 }
              //             }
              //         ]
              //     }
              // ]}
              />
            </div>

          </Grid>
          {/*bar chart*/}
          <Grid item sm={12} md={4} spacing={2} className={classes.gridStyle}  >
            <div className={classes.bargraphContent}>
              <label className={classes.graphHeading}>Effort Completed and Remaining by Resource</label>

              <ResponsiveBar
                data={this.bardata}
                keys={['hot dog', 'burger', 'sandwich', 'kebab', 'fries', 'donut']}
                indexBy="country"
                margin={{ top: 10, right: 30, bottom: 90, left: 60 }}
                padding={0}
                valueScale={{ type: 'linear' }}
                indexScale={{ type: 'band', round: true }}
                colors={{ scheme: 'nivo' }}
                defs={[
                  {
                    id: 'dots',
                    type: 'patternDots',
                    background: 'inherit',
                    color: '#38bcb2',
                    size: 4,
                    padding: 1,
                    stagger: true
                  },
                  {
                    id: 'lines',
                    type: 'patternLines',
                    background: 'inherit',
                    color: '#eed312',
                    rotation: -45,
                    lineWidth: 6,
                    spacing: 10
                  }
                ]}
                fill={[
                  {
                    match: {
                      id: 'fries'
                    },
                    id: 'dots'
                  },
                  {
                    match: {
                      id: 'sandwich'
                    },
                    id: 'lines'
                  }
                ]}
                borderColor={{ from: 'color', modifiers: [['darker', 1.6]] }}
                axisTop={null}
                axisRight={null}
                axisBottom={{
                  tickSize: 5,
                  tickPadding: 5,
                  tickRotation: 0,
                  legend: 'country',
                  legendPosition: 'middle',
                  legendOffset: 32
                }}
                axisLeft={{
                  tickSize: 5,
                  tickPadding: 5,
                  tickRotation: 0,
                  legend: 'food',
                  legendPosition: 'middle',
                  legendOffset: -40
                }}
                labelSkipWidth={12}
                labelSkipHeight={12}
                labelTextColor={{ from: 'color', modifiers: [['darker', 1.6]] }}
                /*legends={[
                  {
                    dataFrom: 'keys',
                    anchor: 'bottom-right',
                    direction: 'column',
                    justify: false,
                    translateX: 120,
                    translateY: 0,
                    itemsSpacing: 2,
                    itemWidth: 100,
                    itemHeight: 20,
                    itemDirection: 'left-to-right',
                    itemOpacity: 0.85,
                    symbolSize: 20,
                    effects: [
                      {
                        on: 'hover',
                        style: {
                          itemOpacity: 1
                        }
                      }
                    ]
                  }
                ]}*/
                animate={true}
                motionStiffness={90}
                motionDamping={15}
              />
            </div>
          </Grid>
        </Grid>
        <Grid container direction="row"
          justify="space-between"
          alignItems="center">
         {/*Pie Graph*/}
         <Grid item sm={12} md={4} spacing={2} className={classes.gridStyle}  >
            <div className={classes.graphContent}>
              <label className={classes.graphHeading}>Tasks by Status</label>
              <ResponsivePie
                data={this.data}
                margin={{ top: 40, right: 80, bottom: 80, left: 80 }}
                innerRadius={0}
                padAngle={0.7}
                cornerRadius={3}
                colors={{ scheme: 'nivo' }}
                borderWidth={1}
                borderColor={{ from: 'color', modifiers: [['darker', 0.2]] }}
                radialLabelsSkipAngle={10}
                radialLabelsTextColor="#333333"
                radialLabelsLinkColor={{ from: 'color' }}
                sliceLabelsSkipAngle={10}
                sliceLabelsTextColor="#333333"
                defs={[
                  {
                    id: 'dots',
                    type: 'patternDots',
                    background: 'inherit',
                    color: 'rgba(255, 255, 255, 0.3)',
                    size: 4,
                    padding: 1,
                    stagger: true
                  },
                  {
                    id: 'lines',
                    type: 'patternLines',
                    background: 'inherit',
                    color: 'rgba(255, 255, 255, 0.3)',
                    rotation: -45,
                    lineWidth: 6,
                    spacing: 10
                  }
                ]}
                fill={[
                  {
                    match: {
                      id: 'ruby'
                    },
                    id: 'dots'
                  },
                  {
                    match: {
                      id: 'c'
                    },
                    id: 'dots'
                  },
                  {
                    match: {
                      id: 'go'
                    },
                    id: 'dots'
                  },
                  {
                    match: {
                      id: 'python'
                    },
                    id: 'dots'
                  },
                  {
                    match: {
                      id: 'scala'
                    },
                    id: 'lines'
                  },
                  {
                    match: {
                      id: 'lisp'
                    },
                    id: 'lines'
                  },
                  {
                    match: {
                      id: 'elixir'
                    },
                    id: 'lines'
                  },
                  {
                    match: {
                      id: 'javascript'
                    },
                    id: 'lines'
                  }
                ]}
              // legends={[
              //     {
              //         anchor: 'bottom',
              //         direction: 'row',
              //         justify: false,
              //         translateX: 0,
              //         translateY: 56,
              //         itemsSpacing: 0,
              //         itemWidth: 100,
              //         itemHeight: 18,
              //         itemTextColor: '#999',
              //         itemDirection: 'left-to-right',
              //         itemOpacity: 1,
              //         symbolSize: 18,
              //         symbolShape: 'circle',
              //         effects: [
              //             {
              //                 on: 'hover',
              //                 style: {
              //                     itemTextColor: '#000'
              //                 }
              //             }
              //         ]
              //     }
              // ]}
              />
            </div>

          </Grid>

          </Grid>
        {/* 
      <SelectSearchDropdown
        data={()=>[]}
                      label={"Project Manager"}
                      selectChange={()=>{}}
                      type="project"
                      selectedValue={null}
                      placeholder={
                         "Select Project"
                      }
                    /> */}
      </Grid>
    );
  }
}

ResourceDashboard.propTypes = {
  classes: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ResourceDashboard);