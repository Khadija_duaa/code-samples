import React, { useMemo, useEffect, useState, useContext, useRef } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import PropTypes from "prop-types";
import styles from "./analytics.style.js";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import CustomMultiSelectDropdown from "../../components/Dropdown/CustomMultiSelectWithSearchBarDropdown/Dropdown";
import ntaskLogo from "../../assets/images/svgs/nTaskLogo.svg";
import { ResponsivePie } from "@nivo/pie";
import { ResponsiveBar } from "@nivo/bar";
import ImportExportDD from "../../components/Dropdown/ImportExportDD2";
import CustomButton from "../../components/Buttons/CustomButton";
import ImportExport from "@material-ui/icons/ImportExport";
import ProjectOverview from "./ProjectOverview/ProjectOverview.view";
import { getWidgetAnalyticsData, updateProjectAnalyticsData } from "../../redux/actions/analytics";
import ProjectOverviewContext from "./ProjectOverview/Context/ProjectOverview.context";
import { generateWorkspaceData } from "../../helper/generateTasksOverviewDropdownData";
import {
  dispatchTaskWidgetData,
  dispatchWidgetSetting,
  dispatchWidgetData,
} from "./ProjectOverview/Context/actions";
import isUndefined from "lodash/isUndefined";
import SvgIcon from "@material-ui/core/SvgIcon";

import IconDonut from "../../components/Icons/IconDonut";
import IconBar from "../../components/Icons/IconBar";
import IconPie from "../../components/Icons/IconPie";
import Typography from "@material-ui/core/Typography";
import { calculateContentHeight, calculateHeaderHeight } from "../../utils/common";

function AnalyticsContent(props) {
  const {
    classes,
    profileState: { workspace, activeTeam },
  } = props;
  const {
    state: { widgetSettings, projectWidgetData, filteredTasks },
    dispatch,
  } = useContext(ProjectOverviewContext);
  const {
    projectSummary,
    projectsbyProgress,
    effortByProject,
    projectByProjectManagers,
  } = projectWidgetData;
  const {
    selectedWorkspaces,
    projectManagers,
    selectedStatus,
    selectedManagers,
  } = widgetSettings.widgetSettings;
  const [donutData, setDonutData] = useState([]);
  const [bardata, setBardata] = useState([]);
  const [pieData, setPieData] = useState([]);
  const [defaultVal, setDefaultVal] = useState([{ label: "Select All", value: "1" }]);
  const [defaultValStatus, setDefaultValStatus] = useState([{ label: "Select All", value: "5" }]);
  const [btnDisabled, setBtnDisabled] = useState(true);
  const [btnQuery, setBtnQuery] = useState("");

  const prevSelectedWorkspaceRef = useRef();
  const prevSelectedStatusRef = useRef();
  const prevSelectedPmRef = useRef();

  //Generate projectStatus Data
  const projectStatuses = () => {
    return [
      // { label: "Select All", value: 5 },
      { label: "Not Started", value: 0 },
      { label: "In Progress", value: 1 },
      { label: "On Hold", value: 2 },
      { label: "Completed", value: 3 },
      { label: "Cancelled", value: 4 },
    ];
  };

  // Generate workspace dropdown data
  const workspacesDropdownData = useMemo(() => {
    let activeWorkspaces = workspace.filter(w => w.isActive); /** only show active worksapces  */
    let wd = generateWorkspaceData(activeWorkspaces);
    // return [{ label: "Select All", value: "1", obj: { teamId: "1" } }, ...wd];
    return wd;
  }, workspace);

  //Generate projectManagers Data
  const projectManagersDdata = () => {
    // let pMs = [{ label: "Select All", value: "1" }];
    let pMs = [];
    projectManagers.map(p => {
      pMs.push({
        label: p.fullName,
        value: p.fullName,
        id: p.userId,
      });
    });
    return pMs;
  };

  // fetching selected workspace
  const selectedWorkspaceOptions = workspacesDropdownData.filter(w => {
    return selectedWorkspaces.includes(w.obj.teamId);
  });

  // fetching selected statuses
  const selectedStatusOptions = projectStatuses().filter(s => {
    return selectedStatus.includes(s.value);
  });

  // fetching selected Pms
  const selectedPmOptions = projectManagersDdata().filter(p => {
    return selectedManagers.includes(p.id);
  });

  // Handle workspace selection in dropdown
  const onWorkspaceSelect = options => {
    let selectAll = options.find(o => o.value == "1") || false;
    let workspaceIds = options.map(o => o.obj.teamId);
    if (selectAll) {
      if (selectAll && options.length > 0) {
        if (selectedWorkspaces.length > 0 && selectAll) {
          workspaceIds = workspaceIds;
        } else {
          workspaceIds = workspaceIds.filter(w => w !== "1");
        }
      } else {
        workspaceIds = [];
      }
    }

    let object = {
      ...widgetSettings,
      widgetSettings: {
        ...widgetSettings.widgetSettings,
        selectedWorkspaces: workspaceIds,
      },
    };
    dispatchWidgetSetting(dispatch, object);
    // updateProjectAnalyticsData(
    //   object,
    //   // Success
    //   res => {
    //     dispatchWidgetSetting(dispatch, res);
    //     getWidgetAnalyticsData("", res => {
    //       dispatchWidgetData(dispatch, res.data.entity);
    //     });
    //   }
    // );
  };

  // Handle status selection in dropdown
  const onStatusSelect = options => {
    let selectAll = options.find(o => o.value == "5") || false;
    let statusIds = options.map(o => o.value);
    if (selectAll) {
      if (selectAll && options.length > 1) {
        if (selectedStatusOptions.length > 0 && selectAll) {
          statusIds = [];
        } else {
          statusIds = statusIds.filter(w => w !== "5");
        }
      } else {
        statusIds = [];
      }
    }

    let object = {
      ...widgetSettings,
      widgetSettings: {
        ...widgetSettings.widgetSettings,
        selectedStatus: statusIds,
      },
    };
    dispatchWidgetSetting(dispatch, object);
    // updateProjectAnalyticsData(
    //   object,
    //   // Success
    //   res => {
    //     dispatchWidgetSetting(dispatch, res);
    //     getWidgetAnalyticsData("", res => {
    //       dispatchWidgetData(dispatch, res.data.entity);
    //     });
    //   }
    // );
  };

  // Handle pms selection in dropdown
  const onPmSelect = options => {
    const managerIds = options.map(p => p.id);

    let object = {
      ...widgetSettings,
      widgetSettings: {
        ...widgetSettings.widgetSettings,
        selectedManagers: managerIds,
      },
    };
    dispatchWidgetSetting(dispatch, object);
    // updateProjectAnalyticsData(
    //   object,
    //   // Success
    //   res => {
    //     dispatchWidgetSetting(dispatch, res);
    //     getWidgetAnalyticsData("", res => {
    //       dispatchWidgetData(dispatch, res.data.entity);
    //     });
    //   }
    // );
  };

  const compilingProjectByProgress = param => {
    let data = [];
    param.map(element => {
      switch (element.id) {
        case "NotStarted":
          data.push({
            id: "Not Started",
            label: "Not Started",
            value: element.value,
            color: "hsl(12, 97%, 65%)",
          });
          break;
        case "InProgress":
          data.push({
            id: "In Progress",
            label: "In Progress",
            value: element.value,
            color: "hsl(222, 97%, 65%)",
          });
          break;
        case "OnHold":
          data.push({
            id: "On Hold",
            label: "On Hold",
            value: element.value,
            color: "hsl(33, 97%, 65%)",
          });
          break;
        case "Completed":
          data.push({
            id: "Completed",
            label: "Completed",
            value: element.value,
            color: "hsl(139, 63%, 57%)",
          });
          break;
        case "Cancelled":
          data.push({
            id: "Cancelled",
            label: "Cancelled",
            value: element.value,
            color: "hsl(0, 100%, 68%)",
          });
          break;
        default:
          break;
      }
    });
    setDonutData(data);
  };

  const compilingEffortByProject = param => {
    let data = [];
    param.map(element => {
      data.push({
        id: element.id,
        "Effort Completed": element.completedEfforts,
        "Effort Remaining": element.remainingEfforts,
        completedEfforts: element.completedEfforts,
        remainingEfforts: element.remainingEfforts,
      });
    });
    setBardata(data);
  };

  const compilingProjectByProjectManagers = param => {
    let data = [];
    param.map(element => {
      data.push({
        id: element.id,
        label: element.label,
        value: element.value,
      });
    });
    setPieData(data);
  };

  const handleClickApply = () => {
    setBtnQuery("progress");
    let object = {
      ...widgetSettings,
    };
    updateProjectAnalyticsData(
      object,
      // Success
      res => {
        // dispatchWidgetSetting(dispatch, res);
        getWidgetAnalyticsData("", res => {
          dispatchWidgetData(dispatch, res.data.entity);
          setBtnQuery("");
          setBtnDisabled(true);
        });
      }
    );
  };

  useEffect(() => {
    if (
      !isUndefined(prevSelectedWorkspaceRef.current) &&
      !isUndefined(prevSelectedStatusRef.current) &&
      !isUndefined(prevSelectedPmRef.current)
    ) {
      if (
        JSON.stringify(prevSelectedWorkspaceRef.current) !==
          JSON.stringify(selectedWorkspaceOptions) ||
        JSON.stringify(prevSelectedStatusRef.current) !== JSON.stringify(selectedStatusOptions) ||
        JSON.stringify(prevSelectedPmRef.current) !== JSON.stringify(selectedPmOptions)
      ) {
        setBtnDisabled(false);
      }
    }
    prevSelectedWorkspaceRef.current = selectedWorkspaceOptions;
    prevSelectedStatusRef.current = selectedStatusOptions;
    prevSelectedPmRef.current = selectedPmOptions;
  });

  useEffect(() => {
    let a = prevSelectedWorkspaceRef.current;
    let b = prevSelectedStatusRef.current;
    let c = prevSelectedPmRef.current;

    compilingProjectByProgress(projectsbyProgress);
    compilingEffortByProject(effortByProject);
    compilingProjectByProjectManagers(projectByProjectManagers);
  }, [projectsbyProgress, effortByProject, projectByProjectManagers]);

  const noRecordProgress = donutData.every(e => e.value == 0);
  const noRecordPms = pieData.every(e => e.value == 0);
  const noRecordProjectEffort = bardata.every(
    e => e.completedEfforts == 0 && e.remainingEfforts == 0
  );
  const BarDataLength = bardata.length;
  const isOldSidebar = localStorage.getItem('oldSidebarView') == 'true'
  return (
    /*heading*/

    <Grid container spacing={2}>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
        className={classes.topContainer}
        // style={{marginTop: calculateHeaderHeight() + 27}}
      >
        <Grid item sm={12} md={2} lg={5} className={classes.gridStyle}>
          <div>
            {/* <img src={ntaskLogo} className={classes.headerLogo} /> */}
            {/* <label className={classes.dashboardTitle}>Project Portfolio</label> */}
            <Typography variant="h1" className={classes.mainHeading}>
              Project Portfolio
            </Typography>
          </div>
        </Grid>
        {/* first select dropdown */}
        <Grid item sm={12} md={3} lg={2} className={classes.gridStyle}>
          <CustomMultiSelectDropdown
            label="Workspace"
            options={() => workspacesDropdownData}
            option={selectedWorkspaceOptions}
            onSelect={onWorkspaceSelect}
            scrollHeight={400}
            size={"large"}
            buttonProps={{
              variant: "contained",
              btnType: "white",
              labelAlign: "left",
              style: { minWidth: "100%", padding: "3px 4px 3px 8px", height: 35 },
            }}
          />
        </Grid>
        {/* second select dropdown */}
        <Grid item sm={12} md={3} lg={2} className={classes.gridStyle}>
          <CustomMultiSelectDropdown
            label="Project Status"
            options={projectStatuses}
            option={selectedStatusOptions}
            onSelect={onStatusSelect}
            scrollHeight={400}
            size={"large"}
            buttonProps={{
              variant: "contained",
              btnType: "white",
              labelAlign: "left",
              style: { minWidth: "100%", padding: "3px 4px 3px 8px", height: 35 },
            }}
          />
        </Grid>
        {/* third select dropdown */}
        <Grid item sm={12} md={3} lg={2} className={classes.gridStyle}>
          <CustomMultiSelectDropdown
            label="Project Manager"
            options={projectManagersDdata}
            option={selectedPmOptions}
            onSelect={onPmSelect}
            scrollHeight={400}
            size={"large"}
            buttonProps={{
              variant: "contained",
              btnType: "white",
              labelAlign: "left",
              style: { minWidth: "100%", padding: "3px 4px 3px 8px", height: 35 },
            }}
          />

          <div className={classes.horizontalDivider}></div>
        </Grid>
        {/* export button */}

        <Grid item sm={12} md={1} lg={1} className={classes.gridStyle}>
          <CustomButton
            onClick={event => {
              handleClickApply();
            }}
            ref={null}
            style={{
              marginTop: 15,
              height: 33,
            }}
            btnType={"blue"}
            variant="contained"
            disabled={btnDisabled || btnQuery === "progress"}
            query={btnQuery}>
            {/* <ImportExport classes={{ root: classes.importExportIcon }} /> */}
            <span className={classes.sortLbl}>Apply</span>
          </CustomButton>
        </Grid>
      </Grid>

      {/*colored boxes*/}
      <div className={classes.widgetContentCnt} style={{ height: calculateContentHeight() + 44,
        // marginTop: isOldSidebar ? calculateHeaderHeight() + 27 : calculateHeaderHeight() + 27
      }}>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
        className={classes.widgetCnt}>
        <Grid
          item
          sm={6}
          md={4}
          lg={activeTeam !== "013f092268fb464489129c5ea19b89d3" ? 2 : 4}
          className={classes.gridStyle}
          spacing={2}>
          <div style={{ background: "#2EBAE6", padding: 10 }} className={classes.content}>
            <label className={classes.contentHeading}>Project</label>
            <label>{`${projectSummary.inprogressProjects}/${projectSummary.totalProjects}`}</label>
            <span className={classes.time}>In Progress / Total</span>
            <span className={classes.time}>(Count)</span>
          </div>
        </Grid>
        <Grid
          item
          sm={6}
          md={4}
          lg={activeTeam !== "013f092268fb464489129c5ea19b89d3" ? 2 : 4}
          className={classes.gridStyle}
          spacing={2}>
          <div style={{ background: "#29C8A2", padding: 10 }} className={classes.content}>
            <label className={classes.contentHeading}>Tasks</label>
            <label>{`${projectSummary.completedTasks}/${projectSummary.totalTasks}`}</label>
            <span className={classes.time}>Completed / Total</span>
            <span className={classes.time}>(Count)</span>
          </div>
        </Grid>
        <Grid
          item
          sm={6}
          md={4}
          lg={activeTeam !== "013f092268fb464489129c5ea19b89d3" ? 2 : 4}
          className={classes.gridStyle}
          spacing={2}>
          <div style={{ background: "#29C8A2", padding: 10 }} className={classes.content}>
            <label className={classes.contentHeading}>Tasks</label>
            <label>{`${projectSummary.overDueTasks}/${projectSummary.lateTasks}/${projectSummary.onTrackTasks}`}</label>
            <span className={classes.time}>Overdue / Late / On Track</span>
            <span className={classes.time}>(Count)</span>
          </div>
        </Grid>
        {activeTeam !== "013f092268fb464489129c5ea19b89d3" && (
          <>
            <Grid item sm={6} md={4} lg={2} className={classes.gridStyle} spacing={2}>
              <div style={{ background: "#465457", padding: 10 }} className={classes.content}>
                <label className={classes.contentHeading}>Effort</label>
                <label>{`${projectSummary.completedEfforts}/${projectSummary.remainingEfforts}/${projectSummary.totalEfforts}`}</label>
                <span className={classes.time}>Completed / Remaining / Total</span>
                <span className={classes.time}>(Hours)</span>
              </div>
            </Grid>
            <Grid item sm={6} md={4} lg={2} className={classes.gridStyle} spacing={2}>
              <div style={{ background: "#E64E4E", padding: 10 }} className={classes.content}>
                <label className={classes.contentHeading}>Issues</label>
                <label>{`${projectSummary.issuesOpen}/${projectSummary.issuesClosed}`}</label>
                <span className={classes.time}>Open / Closed</span>
                <span className={classes.time}>(Count)</span>
              </div>
            </Grid>
            <Grid item sm={6} md={4} lg={2} className={classes.gridStyle} spacing={2}>
              <div style={{ background: "#FC844F", padding: 10 }} className={classes.content}>
                <label className={classes.contentHeading}>Risks</label>
                <label>{projectSummary.risks}</label>
                <span className={classes.time}>Total</span>
                <span className={classes.time}>(Count)</span>
              </div>
            </Grid>
          </>
        )}
      </Grid>
      {/*Donut Graphs*/}
      <Grid container direction="row" justify="space-between" alignItems="center">
        <Grid item sm={12} md={6} lg={6} spacing={2} className={classes.gridStyle}>
          <div className={classes.graphContent}>
            <label className={classes.graphHeading}>Projects By Progress</label>
            {noRecordProgress ? (
              <div className={classes.emptyParentCnt}>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                  }}>
                  <SvgIcon viewBox="0 0 140 140" className={classes.svgIcon}>
                    <IconDonut />
                  </SvgIcon>
                  <span className={classes.noFoundTxt}> No Record Available</span>
                </div>
              </div>
            ) : (
              <ResponsivePie
                data={donutData}
                margin={{ top: 40, right: 80, bottom: 80, left: 80 }}
                innerRadius={0.5}
                padAngle={0}
                cornerRadius={0}
                colors={[
                  "#fc7150", // Not Started
                  "#5185fc", // In Progress
                  "#faa948", // On Hold
                  "#4dd778", // Completed
                  "#ff5b5b", // Cancelled
                ]}
                borderWidth={0}
                borderColor={{ from: "color", modifiers: [["darker", 0]] }}
                radialLabelsSkipAngle={10}
                radialLabelsTextColor="#333333"
                radialLabelsLinkColor={{ from: "color" }}
                sliceLabelsSkipAngle={10}
                sliceLabelsTextColor="#333333"
                defs={[
                  {
                    id: "dots",
                    type: "patternDots",
                    background: "inherit",
                    color: "rgba(255, 255, 255, 0.3)",
                    size: 4,
                    padding: 1,
                    stagger: true,
                  },
                  {
                    id: "lines",
                    type: "patternLines",
                    background: "inherit",
                    color: "rgba(255, 255, 255, 0.3)",
                    rotation: -45,
                    lineWidth: 6,
                    spacing: 10,
                  },
                ]}
                fill={[
                  {
                    match: {
                      id: "ruby",
                    },
                    id: "dots",
                  },
                  {
                    match: {
                      id: "c",
                    },
                    id: "dots",
                  },
                  {
                    match: {
                      id: "go",
                    },
                    id: "dots",
                  },
                  {
                    match: {
                      id: "python",
                    },
                    id: "dots",
                  },
                  {
                    match: {
                      id: "scala",
                    },
                    id: "lines",
                  },
                  {
                    match: {
                      id: "lisp",
                    },
                    id: "lines",
                  },
                  {
                    match: {
                      id: "elixir",
                    },
                    id: "lines",
                  },
                  {
                    match: {
                      id: "javascript",
                    },
                    id: "lines",
                  },
                ]}
                radialLabelsLinkOffset={-5}
                radialLabelsLinkHorizontalLength={15}
                radialLabelsTextXOffset={2}
              />
            )}
          </div>
        </Grid>
        {/*Pie Graph*/}
        <Grid item sm={12} md={6} lg={6} spacing={2} className={classes.gridStyle}>
          <div className={classes.graphContent}>
            <label className={classes.graphHeading}>Projects By Project Manager</label>
            {noRecordPms ? (
              <div className={classes.emptyParentCnt}>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                  }}>
                  <SvgIcon viewBox="0 0 140 140.009" className={classes.svgIcon}>
                    <IconPie />
                  </SvgIcon>
                  <span className={classes.noFoundTxt}> No Record Available</span>
                </div>
              </div>
            ) : (
              <ResponsivePie
                data={pieData}
                margin={{ top: 40, right: 80, bottom: 80, left: 80 }}
                innerRadius={0}
                padAngle={0}
                cornerRadius={0}
                colors={{ scheme: "paired" }}
                borderWidth={0}
                borderColor={{ from: "color", modifiers: [["darker", 0]] }}
                radialLabelsSkipAngle={10}
                radialLabelsTextColor="#333333"
                radialLabelsLinkColor={{ from: "color" }}
                sliceLabelsSkipAngle={10}
                sliceLabelsTextColor="#333333"
                defs={[
                  {
                    id: "dots",
                    type: "patternDots",
                    background: "inherit",
                    color: "rgba(255, 255, 255, 0.3)",
                    size: 4,
                    padding: 1,
                    stagger: true,
                  },
                  {
                    id: "lines",
                    type: "patternLines",
                    background: "inherit",
                    color: "rgba(255, 255, 255, 0.3)",
                    rotation: -45,
                    lineWidth: 6,
                    spacing: 10,
                  },
                ]}
                fill={[
                  {
                    match: {
                      id: "ruby",
                    },
                    id: "dots",
                  },
                  {
                    match: {
                      id: "c",
                    },
                    id: "dots",
                  },
                  {
                    match: {
                      id: "go",
                    },
                    id: "dots",
                  },
                  {
                    match: {
                      id: "python",
                    },
                    id: "dots",
                  },
                  {
                    match: {
                      id: "scala",
                    },
                    id: "lines",
                  },
                  {
                    match: {
                      id: "lisp",
                    },
                    id: "lines",
                  },
                  {
                    match: {
                      id: "elixir",
                    },
                    id: "lines",
                  },
                  {
                    match: {
                      id: "javascript",
                    },
                    id: "lines",
                  },
                ]}
                radialLabelsLinkOffset={-5}
                radialLabelsLinkHorizontalLength={15}
                radialLabelsTextXOffset={2}
              />
            )}
          </div>
        </Grid>
        {/*bar chart*/}
        {activeTeam !== "013f092268fb464489129c5ea19b89d3" && (
          <Grid item sm={12} md={12} lg={12} spacing={2} className={classes.gridStyle}>
            <div className={classes.bargraphContent} id="custom-scroll">
              <label className={classes.graphHeading}>Effort By Project</label>
              {noRecordProjectEffort ? (
                <div className={classes.emptyParentCnt}>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center",
                    }}>
                    <SvgIcon viewBox="0 0 140 140.117" className={classes.svgIcon}>
                      <IconBar />
                    </SvgIcon>
                    <span className={classes.noFoundTxt}> No Record Available</span>
                  </div>
                </div>
              ) : (
                <div
                  className={
                    BarDataLength > 500
                      ? classes.barData2
                      : BarDataLength > 400
                      ? classes.barData3
                      : BarDataLength > 300
                      ? classes.barData4
                      : BarDataLength > 200
                      ? classes.barData5
                      : BarDataLength > 100
                      ? classes.barData6
                      : classes.barData1
                  }>
                  <ResponsiveBar
                    data={bardata}
                    keys={["Effort Completed", "Effort Remaining"]}
                    indexBy="id"
                    margin={{ top: 40, right: 30, bottom: 100, left: 60 }}
                    padding={0}
                    valueScale={{ type: "linear" }}
                    indexScale={{ type: "band", round: true }}
                    colors={["#29c8a2", "#465457"]}
                    defs={[
                      {
                        id: "dots",
                        type: "patternDots",
                        background: "inherit",
                        color: "#38bcb2",
                        size: 4,
                        padding: 1,
                        stagger: true,
                      },
                      {
                        id: "lines",
                        type: "patternLines",
                        background: "inherit",
                        color: "#eed312",
                        rotation: -45,
                        lineWidth: 6,
                        spacing: 10,
                      },
                    ]}
                    fill={[
                      {
                        match: {
                          id: "fries",
                        },
                        id: "dots",
                      },
                      {
                        match: {
                          id: "sandwich",
                        },
                        id: "lines",
                      },
                    ]}
                    borderColor={{ from: "color", modifiers: [["darker", 1.6]] }}
                    axisTop={null}
                    axisRight={null}
                    axisBottom={{
                      tickSize: 6,
                      tickPadding: 5,
                      tickRotation: 90,
                      legend: "",
                      legendPosition: "middle",
                      legendOffset: 60,
                    }}
                    axisLeft={{
                      tickSize: 5,
                      tickPadding: 5,
                      tickRotation: 0,
                      legend: "Effort",
                      legendPosition: "middle",
                      legendOffset: -40,
                    }}
                    labelSkipWidth={12}
                    labelSkipHeight={12}
                    labelTextColor={{ from: "color", modifiers: [["darker", 1.6]] }}
                    legends={[
                      {
                        dataFrom: ["Effort Completed", "Effort Remaining"],
                        anchor: "top",
                        direction: "row",
                        justify: false,
                        translateX: 46,
                        translateY: -46,
                        itemsSpacing: 40,
                        itemWidth: 75,
                        itemHeight: 44,
                        itemDirection: "left-to-right",
                        itemOpacity: 0.85,
                        symbolSize: 10,
                        effects: [
                          {
                            on: "hover",
                            style: {
                              itemOpacity: 1,
                            },
                          },
                        ],
                      },
                    ]}
                    animate={true}
                    motionStiffness={90}
                    motionDamping={15}
                  />
                </div>
              )}
            </div>
          </Grid>
        )}
      </Grid>
      {/*Project OverView*/}
      <ProjectOverview />
      </div>
    </Grid>
  );
}

AnalyticsContent.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => {
  return {
    profileState: state.profile.data,
    sidebarPannel: state.sidebarPannel,
  };
};

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps)
)(AnalyticsContent);
