const analyticsStyles = theme => ({
  widgetContentCnt: {
  overflow: 'hidden',
    overflowY: 'auto',
    padding: '20px 0 0 0'
  },
  overviewCnt: {
    // padding: "12px 12px 12px 34px",
    padding: "0px 0 0 32px",
    width: "100%",
    margin: -8,
    height: '100%'
  },
  analyticDropdown: {
    float: "right",
  },
  content: {
    background: "#ffffff",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    minHeight: "118px",
    borderRadius: "5px",
    color: "#FFFFFF !important",
    "& label": {
      fontSize: "25px !important",
      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: "400",
      marginBottom: "10px",
      color: "#ffffff",
    },
  },
  contentHeading: {
    textAlign: "center",
    fontFamily: theme.typography.fontFamilyLato,
    // letterSpacing: "0px",
    color: "#FFFFFF",
    opacity: 1,
    fontSize: "20px !important",
    marginBottom: 8,
    // lineHeight: "45px",
  },
  graphHeading: {
    // textAlign: "center",
    fontFamily: theme.typography.fontFamilyLato,
    // letterSpacing: "0px",
    color: "#00000",
    // opacity: 1,
    fontSize: "20px !important",
    fontWeight: 600,
    // lineHeight: "45px",
  },
  graphContent: {
    height: "500px",
    background: " #FFFFFF",
    border: "1px solid rgba(221, 221, 221, 1)",
    borderRadius: "5px",
    // opacity: "1",
    // textAlign: "center",
    padding: "10px",
  },
  bargraphContent: {
    height: "500px",
    background: "#FFFFFF",
    border: "1px solid rgba(221, 221, 221, 1)",
    borderRadius: "7px",
    padding: "10px",
    overflow: "auto",
    // opacity: "1",
    // textAlign: "center",
  },
  dashboardTitle: {
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: "600",
    // letterSpacing: "0px",
    color: "black",
    fontSize: "26px !important",
  },

  mainHeading: {
    marginRight: 16,
    fontSize: "18px !important",
    fontFamily: theme.typography.fontFamilyLato,
},

  /*nTask Logo*/
  headerLogo: {
    width: "30px",
    marginRight: 10,
    marginBottom: -3,
  },
  toggleContainer: {
    height: 56,
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`,
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
    margin: `${theme.spacing.unit}px 0`,
    background: theme.palette.background.default,
  },
  root: {
    display: "flex",
    flexWrap: "wrap",
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
  gridStyle: {
    padding: "5px",
  },
  horizontalDivider: {
    borderLeft: "1px solid #EAEAEA",
    marginLeft: 10,
  },
  sortLbl: {
    paddingRight: 4,
  },
  importExportIcon: {
    fontSize: "18px !important",
  },
  selectDropdown: {
    marginRight: "500px",
  },
  time: {
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: "400",
    color: "#ffffff !important",
    marginBottom: 10,
  },
  topContainer: {
    minHeight: 60,
    borderBottom: "1px solid rgba(221, 221, 221, 1)",
    background: "#ffffff",
    zIndex: 1,
    // padding: "11px 10px 11px 16px",
    padding: "11px 10px 11px 0px",
  },
  emptyParentCnt: {
    width: "100%",
    height: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
  },
  emptyCntt: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  svgIcon: {
    fontSize: "70px !important",
  },
  noFoundTxt: {
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: "600",
    color: "#374649",
    padding: 15,
    fontSize: "18px !important",
  },
  barData1: {
    width: "100%",
    height: "100%",
  },
  barData2: {
    width: "1000%",
    height: "100%",
  },
  barData3: {
    width: "600%",
    height: "100%",
  },
  barData4: {
    width: "450%",
    height: "100%",
  },
  barData5: {
    width: "300%",
    height: "100%",
  },
  barData6: {
    width: "150%",
    height: "100%",
  },

  /* Extra small devices (phones, 600px and down) */
  "@media only screen and (max-width: 768px)": {
    content: {
      marginBottom: "15px",
    },
  },
  /* Medium devices (landscape tablets, 768px and up) */
  "@media only screen and (min-width: 768px)": {
    content: {
      marginBottom: "15px",
    },
  },
  "@media (max-width: 1024px)": {
    barData: {
      width: "270%",
    },

  },
});

export default analyticsStyles;
