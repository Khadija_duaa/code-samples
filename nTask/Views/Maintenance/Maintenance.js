import React, { Component } from "react";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import UpdateInProgress from "../../assets/images/update-in-progress.png";
import maintenanceStyles from "./style";

class Maintenance extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
      const {classes} = this.props
    return (
      <div className={classes.maintenanceContentCnt}>
        <div>
          <img src={UpdateInProgress} width={500} />
          <Typography variant="h1" className={classes.maintenanceHeading}>
            A system update is in progress, please hold tight,
            while we load Ntask with new and exciting features.
          </Typography>
        </div>
      </div>
    )
  }
}

export default withStyles(maintenanceStyles, {withTheme: true})(Maintenance);
