// @flow
import React, { useEffect } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import queryString from "query-string";
import { authenticateUser } from "../../redux/actions/authentication";
function SocialLogin(props) {
  const { location, classes } = props;
  const parsedUrl = queryString.parse(location.search);

  useEffect(() => {
    const socialLoginToken = parsedUrl.token;
    //Setting Token in localstorage
    localStorage.setItem("token", "Bearer " + socialLoginToken);
    props.authenticateUser(true); //setting Authentication to true in redux store
    props.history.push("/teams/"); //redirecting to teams dashboard
  }, []);
  return <></>;
}

export default compose(
  connect(null, { authenticateUser }),
  withRouter
)(SocialLogin);
