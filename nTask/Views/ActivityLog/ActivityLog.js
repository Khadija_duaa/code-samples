import React, { Component, Fragment } from "react";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import activityLogStyles from "./style";
import CalendarIcon from "@material-ui/icons/DateRange";
import ReactHtmlParser from 'react-html-parser';
function ActivityList(props) {
  const { classes, theme,activityList } = props;
  return activityList.map((value, i, arr) => {
    return (
      <div className={classes.singleActivityCnt} key={value.id}>
        <Typography variant="body2" classes={{ body2: classes.activityDate }}>
          <CalendarIcon
            htmlColor={theme.palette.secondary.light}
            classes={{ root: classes.calendarIcon }}
          />
          {value.date}
        </Typography>
        {value.activity.map((activity, i, arr) => {
          return (
            <div className={classes.activityCardCnt} style={{marginTop: i == 0 ? 0 : 15}} key={activity.id}>
              <p className={classes.activityTime}>{activity.time}</p>
              <Typography
                variant="body2"
                classes={{ body2: classes.activityMsg }}
              >
                {ReactHtmlParser(activity.description)}
              </Typography>
            </div>
          );
        })}
      </div>
    );
  });
}
export default withStyles(activityLogStyles, { withTheme: true })(ActivityList);
