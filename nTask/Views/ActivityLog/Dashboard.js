import React, { Component, Fragment } from "react";
import { withRouter } from "react-router";
import { GetWorkspaceActivityLog } from "../../redux/actions/activityLogs";
import { connect } from "react-redux";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import activityLogStyles from "./style";
import Typography from "@material-ui/core/Typography";
import DropdownArrow from "@material-ui/icons/ArrowDropDown";
import ActivityList from "./ActivityLog";
import helper from "../../helper";
import groupBy from "lodash/groupBy";
import map from "lodash/map";
import LeftArrow from "@material-ui/icons/ChevronLeft";
import { calculateContentHeight } from "../../utils/common";
import { Scrollbars } from "react-custom-scrollbars";
import { FormattedMessage } from "react-intl";
import "core-js/features/array/flat";
import CustomButton from "../../components/Buttons/CustomButton";
import fileDownload from "js-file-download";
import { exportActivityLog } from "../../redux/actions/team";
import { withSnackbar } from "notistack";
class ActivityDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      alignment: "left",
      activeView: 0,
      activeViewPendingList: 0,
      openTimesheetEmail: false,
      pageNumber: 1,
      showRecords: true,
      totalRecords: 20,
    };
    this.viewMorePages = this.viewMorePages.bind(this);
    this.handleBackBtn = this.handleBackBtn.bind(this);
  }
  componentDidMount() {
    this.props.GetWorkspaceActivityLog(this.state.pageNumber, (err, data) => {
      this.setState({
        showRecords: data.showMore,
        totalRecords: data.totalRecords,
      });
    });
  }
  handleBackBtn() {
    const { history } = this.props;
    if (history.length <= 2) {
      history.push("/tasks");
    } else {
      this.props.history.goBack();
    }
  }
  viewMorePages() {
    this.setState({ pageNumber: this.state.pageNumber + 1 }, () => {
      this.props.GetWorkspaceActivityLog(this.state.pageNumber, (err, data) => {
        this.setState({ showRecords: data.showMore });
      });
    });
  }

  handleExportActivity = () => {
    exportActivityLog(res => {

      fileDownload(res.data, "activity-log.xlsx");
      this.props.showSnackBar('Activity Log exported successfully', "success");
    });
  }

  render() {
    const { classes, theme, activityLogsState } = this.props;
    let activityList = [];
    if (activityLogsState && activityLogsState.data && activityLogsState.data.length) {
      activityList = activityLogsState.data
        .map((x, i) => ({
          id: i,
          date: x.date,
          activity: x.activities,
        }))
        .map(x => {
          x.activity = x.activity.map((x, i) => {
            return {
              id: x.activity.id,
              description: `<b>${x.userName}</b>  ${x.activity.message}`,
              time: helper.RETURN_CUSTOMDATEFORMATFORTIME(
                x.activity.updatedDate || x.activity.createdDate
              ),
            };
          });

          return x;
        });
    }
    activityList = map(
      groupBy(activityList, x => x.date),
      (value, key) => ({
        date: key,
        activity: value.map(x => x.activity).flat(1),
      })
    );
    const { showRecords, totalRecords } = this.state;
    const newSidebarView = localStorage.getItem("oldSidebarView") == "true" ? false : true;
    return (
      <div className={classes.activityLogDashboard}>
        <div className={`${classes.activityLogDashboardHeader} flex_center_space_between_row`}>

          {!newSidebarView && (
            <LeftArrow onClick={this.handleBackBtn} className={classes.backArrowIcon} />
          )}
          <Typography variant="h1" className={classes.fontWeight}>
            {" "}
            <FormattedMessage id="activity-log.label" defaultMessage="Activity Log" />
          </Typography>
          <CustomButton style={{ color: theme.palette.text.lightGray, fontFamily: theme.typography.fontFamilyLato }} variant={'outlined'} btnType={'plain'} onClick={this.handleExportActivity}>
            Export
          </CustomButton>

        </div>

        <div className={classes.activityListingCnt}>
          <Scrollbars autoHide style={{ height: calculateContentHeight() }}>
            <div className={classes.activityList}>
              <ActivityList activityList={activityList} />

              {showRecords ? (
                <div
                  className={`${classes.viewMoreCnt} flex_center_center_row`}
                  style={{ marginTop: 15 }}
                  onClick={this.viewMorePages}>
                  <Typography variant="h5">View More</Typography>
                  <DropdownArrow htmlColor={theme.palette.secondary.medDark} />
                </div>
              ) : null}
            </div>
          </Scrollbars>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    activityLogsState: state.activityLogs,
  };
};

export default compose(
  withRouter,
  withSnackbar,
  withStyles(activityLogStyles, { withTheme: true }),
  connect(mapStateToProps, { GetWorkspaceActivityLog })
)(ActivityDashboard);
