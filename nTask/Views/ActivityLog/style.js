const activityLogStyles = theme =>({
    activityLogDashboard: {
        padding: "20px 40px",
        marginBottom: 70
      },
    activityListingCnt:{
        background: theme.palette.common.white,
        border: `1px solid ${theme.palette.border.lightBorder}`,
        width: 900,
        margin: "0 auto" 
    },
    activityList:{
      padding: 20
    },
    fontWeight:{
      fontWeight: "600 !important"
    },
    backArrowIcon: {
        fontSize: "28px !important",
        marginRight: 10,
        cursor: "pointer"
    
      },
      activityLogDashboardHeader: {
        margin: "0 0 20px 0",
        width: '100%'
      },

      activityCardCnt: {
        padding: "8px 10px",
        background: theme.palette.common.white,
        borderRadius: 4,
        border: `1px solid ${theme.palette.border.lightBorder}`
    },
    activityMsg:{
      color: theme.palette.text.primary,
      '& b': {
        color: theme.palette.text.primary,
        fontWeight: theme.typography.fontWeightMedium
      }
    },
    activityDate:{
      textTransform: "uppercase",
      marginBottom: 10,
      display: 'flex',
      alignItems: "center"
    },
    calendarIcon: {
      marginRight: 5
    },
    activityTime:{
      fontSize: "12px !important",
      margin: '0 0 5px 0',
      color: theme.palette.text.secondary
    },
    
    viewMoreCnt: {
        background: theme.palette.common.white,
        borderRadius: 4,
        textAlign: "center",
        padding: 5,
        border: `1px solid ${theme.palette.border.lightBorder}`,
        cursor: "pointer"
      },
})

export default activityLogStyles;