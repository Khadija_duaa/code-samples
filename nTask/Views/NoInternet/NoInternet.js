// @flow
import React, { useEffect, useState } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import noInternetStyles from "./style";
import Typography from "@material-ui/core/Typography";
import CustomButton from "../../components/Buttons/CustomButton";
import { noInternetAction } from "../../redux/actions/noInternet";
import NoInternetImg from "../../assets/images/no-internet.gif";

function NoInternet(props) {
  const { classes } = props;
  const [btnQuery, updateBtnQuery] = useState("");
  const [retryTime, setRetryTime] = useState(14);

  const checkInternet = () => {
    // Function calls an external API to check if internet connection is up or not
    updateBtnQuery("progress");

    props.noInternetAction(
      () => {
        updateBtnQuery("");
      },
      (response) => {
        updateBtnQuery("");
      }
    );
  };
  const handleRetryTime = () => {
    if (retryTime > 0) {
      // If retry time is greater than 0 than add 1 into existing value
      setRetryTime(retryTime - 1);
    } else {
      updateBtnQuery("progress");
      props.noInternetAction(
        // Auto retrying to check if internet connection exists or not through an external API call
        //Success
        () => {},
        //Failure
        (response) => {
          updateBtnQuery("");
          setRetryTime(14); // If it fails to connect the reset retry time to 14 seconds
        }
      );
    }
  };
  useEffect(() => {
    setTimeout(() => {
      handleRetryTime();
    }, 1000);
  }, [retryTime]);

  return (
    <div className={classes.noInternetOutCnt}>
      <div className={classes.noInternetCnt}>
        <img
          src="https://www.ntaskmanager.com/wp-content/themes/ntask-website/images/no-internet.gif"
          className={classes.noWifiImg}
        />
        <Typography variant="h2" className={classes.mainHeading}>
          No Internet Connection
        </Typography>

        <Typography variant="body2">
          Please check your internet connection settings.
        </Typography>
        <div className={classes.btnCnt}>
          <Typography variant="body2" className={classes.retryText}>
            {" "}
            Retrying in {retryTime} seconds.
          </Typography>
          <CustomButton
            onClick={checkInternet}
            btnType="blue"
            variant="contained"
            query={btnQuery}
          >
            Retry
          </CustomButton>
        </div>
      </div>
    </div>
  );
}

export default compose(
  connect(null, { noInternetAction }),
  withStyles(noInternetStyles, { withTheme: true })
)(NoInternet);
