const noInternetStyles = (theme) => ({
  noInternetOutCnt: {
    position: "fixed",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 1500,
    background: theme.palette.common.white,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  noInternetCnt: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  noWifiImg: {
    width: 300,
  },
  mainHeading: {
    marginBottom: 5,
  },
  retryText: {
    color: theme.palette.text.primary,
    minWidth: 140,
    marginRight: 10,
  },
  btnCnt: {
    display: "flex",
    alignItems: "center",
    marginTop: 10,
  },
});

export default noInternetStyles;
