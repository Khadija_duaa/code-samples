import React, { useEffect, useState } from "react";
import { withStyles } from "@material-ui/core/styles";

// import jsPDF from "jspdf";
import * as XLSX from "xlsx";
// import "jspdf-autotable";

import * as Survey from "survey-core";

import * as SurveyAnalyticsTabulator from "survey-analytics/survey.analytics.tabulator";
import "tabulator-tables/dist/css/tabulator.min.css";
import "survey-analytics/survey.analytics.tabulator.css";

import { getFormResponseData } from "../../redux/actions/forms";
import analyticsStyles from "./styles";

// window.jsPDF = jsPDF;
window.XLSX = XLSX;

function AnalyticsFormTabular(params) {
  const { classes, theme, json } = params;

  const vizPanelOptions = {
    allowHideQuestions: false,
    // labelTruncateLength: 27,
  };

  const [survey, setSurvey] = useState(null);
  const [vizPanel, setVizPanel] = useState(null);

  if (!survey) {
    const survey = new Survey.Model(json);
    setSurvey(survey);
  }

  const generateAnalytics = (surveyResults) => {
    const vizPanel = new SurveyAnalyticsTabulator.Tabulator(
      survey,
      surveyResults,
      // vizPanelOptions
    );
    vizPanel.options.downloadButtons = ['xlsx', 'csv'];
    setVizPanel(vizPanel);
  };

  useEffect(() => {
    if (!vizPanel && !!survey) {
      getFormResponseData(
        json.formId,
        (success) => {
          generateAnalytics(success.data);
        },
        (fail) => {
          generateAnalytics([]);
        }
      );
    }
  }, []);

  useEffect(() => {
    if (vizPanel) vizPanel.render(document.getElementById("surveyVizPanelTabular"));
    return () => {
    //   document.getElementById("surveyVizPanelTabular").innerHTML = "";
    };
  }, [vizPanel]);

  return <div id="surveyVizPanelTabular" />;
}

AnalyticsFormTabular.defaultProps = {
  classes: {},
  theme: {},
};

export default withStyles(analyticsStyles, { withTheme: true })(AnalyticsFormTabular);
