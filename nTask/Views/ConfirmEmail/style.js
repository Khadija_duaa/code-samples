const confirmEmailStyles = () => ({
  snackBarHeadingCnt: {
    marginLeft: 10,
  },
  snackBarContent: {
    margin: 0,
    fontSize: "12px !important",
  },
});

export default confirmEmailStyles;
