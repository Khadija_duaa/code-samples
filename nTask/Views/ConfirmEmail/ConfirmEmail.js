// @flow
import React, { useEffect } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { confirmEmail } from "../../redux/actions/profile";
import queryString from "query-string";
import { withSnackbar } from "notistack";
import confirmEmailStyles from "./style";

function ConfirmEmail(props) {
  const { location, classes } = props;
  const parsedUrl = queryString.parse(location.search);

  //Function to show snackbar

  const showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };

  useEffect(() => {
    props.confirmEmail(
      parsedUrl.token,
      () => {
        props.history.push("/teams/");
      },
      (response) => {
        props.history.push("/teams/");
        showSnackBar(response.data.message, "error");
      }
    );
  }, []);
  return <></>;
}

export default compose(
  withRouter,
  withStyles(confirmEmailStyles, { withTheme: true }),
  withSnackbar,
  connect(null, { confirmEmail })
)(ConfirmEmail);
