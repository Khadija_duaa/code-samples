import React, { useReducer, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import customFieldStyles from "./customFields.style";
import CustomFieldsContext from "./Context/customFields.context";
import reducer from "./Context/reducer";
import initialState from "./Context/initialState";
import {
  dispatchSetViewType,
  dispatchEditViewType,
  dispatchFieldTypeAndView,
  dispatchCustomFieldsType,
} from "./Context/actions/dropdown.actions";
import NewField from "./NewField/newFields.view";
import CustomFieldOption from "./CustomFieldsOptions/CustomFieldsOptions.view";
import AddIcon from "@material-ui/icons/Add";
import CustomButton from "../../components/Buttons/CustomButton";
import isEmpty from "lodash/isEmpty";
import { getCustomFieldTypes } from "../../redux/actions/customFields";
import { fieldTypeData } from "../../helper/customFieldsData";

function CustomFieldView({
  classes,
  theme,
  feature,
  styles,
  onFieldAdd,
  onFieldEdit,
  onFieldDelete,
  permission,
  disableAssessment,
  disabled,
  dialogsState,
  customFields,
  customBtnRenderer
}) {
  const [state, dispatch] = useReducer(reducer, initialState);
  const { customFieldDialog } = dialogsState;

  const { viewType, selectedField } = state;

  //Dialog Open
  const openDialog = () => {
    dispatchSetViewType(dispatch, "optionsSelect");
  };

  const handleItemClick = (e, value, fieldTypes) => {
    e && e.stopPropagation();
    let viewTypeOption = fieldTypes.find(vt => vt.value === value) || {};
    dispatchFieldTypeAndView(dispatch, {
      selectedField: viewTypeOption,
      viewType: "newField",
    });
  };

  useEffect(() => {
    if (customFieldDialog.moduleViewType === "optionsSelect") {
      openDialog();
    } else if (customFieldDialog.moduleViewType === "editCopyModal") {
      getCustomFieldTypes(res => {
        dispatchEditViewType(dispatch, customFieldDialog.data, customFieldDialog.mode);
        handleItemClick(null, customFieldDialog.data.fieldType, fieldTypeData(res));
      });
    }else if(customFieldDialog.moduleViewType === "optionsSelect"){
      openDialog();
    }
  }, [customFieldDialog]);

  return (
    <CustomFieldsContext.Provider
      value={{ dispatch, state, onFieldAdd, onFieldEdit, onFieldDelete, permission }}>
      <>
        {customBtnRenderer ? customBtnRenderer : <CustomButton
          onClick={openDialog}
          ref={null}
          btnType={"plain"}
          labelAlign="left"
          variant="text"
          disabled={disabled}
          customClasses={{
            text: classes.btnStyles,
          }}>
          <AddIcon className={classes.addIcon} />
          <span className={classes.btnTxt}> Add/Edit Field</span>
        </CustomButton>}
        {viewType !== "" && (
          <>
            <CustomFieldOption feature={feature} />
            {!isEmpty(selectedField) && (
              <NewField disableAssessment={disableAssessment} feature={feature} />
            )}
          </>
        )}
      </>
    </CustomFieldsContext.Provider>
  );
}
CustomFieldView.defaultProps = {
  disabled: false,
  customBtnRenderer: null,
  onFieldAdd:()=>{},
  onFieldEdit:()=>{},
  onFieldDelete:()=>{}
};

const mapStateToProps = state => {
  return {
    dialogsState: state.dialogStates,
    customFields: state.customFields,
  };
};

export default compose(
  connect(mapStateToProps, {
    getCustomFieldTypes,
  }),
  withStyles(customFieldStyles, { withTheme: true })
)(CustomFieldView);
