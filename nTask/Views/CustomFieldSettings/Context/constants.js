export const UPDATE_OPTION = "UPDATE_OPTION";
export const SELECT_FIELD_TYPE = "SELECT_FIELD_TYPE";
export const ADD_OPTION = "ADD_OPTION";
export const ADD_DROPDOWN_CONFIG = "ADD_DROPDOWN_CONFIG";
export const REMOVE_OPTION = "REMOVE_OPTION";
export const GET_CUSTOM_FIELD_TYPES = "GET_CUSTOM_FIELD_TYPES";
export const SET_VIEW_TYPE = "SET_VIEW_TYPE";
export const SET_FIELD_NAME_TYPE = "SET_FIELD_NAME_TYPE";
export const CLEAR_FIELDS = "CLEAR_FIELDS";
export const SET_LEVEL = "SET_LEVEL";
export const UPDATE_CONFIG = "UPDATE_CONFIG";
export const SET_FIELD_AND_VIEW_TYPE = "SET_FIELD_AND_VIEW_TYPE";
export const UPDATE_MATRIX_CONFIG = "UPDATE_MATRIX_CONFIG";
export const EDIT_COPY_VIEW_TYPE = "EDIT_COPY_VIEW_TYPE";
export const UPDATE_COLOR_OPTION = "UPDATE_COLOR_OPTION";
export const EDIT_COPY_MATRIX = "EDIT_COPY_MATRIX";
export const UPDATE_ERRORS = "UPDATE_ERRORS";
export const SET_BACK_VIEW = "SET_BACK_VIEW";
export const SET_ALL_WORKSPACE_LEVEL = "SET_ALL_WORKSPACE_LEVEL";
export const UPDATE_DECIMAL_PLACES = "UPDATE_DECIMAL_PLACES";
export const UPDATE_UNIT = "UPDATE_UNIT";
export const UPDATE_DIRECTION = "UPDATE_DIRECTION";
export const DISPATCH_SETTINGS = "DISPATCH_SETTINGS";
export const SET_SECTION = "SET_SECTION";
export const CustomFieldColor = {
  dropdown: {
    background: "rgb(255 161 72 / 10%)",
  },
  checklist: {
    background: "rgb(91 181 236 / 10%)",
  },
  reminder: {
    background: " rgb(243 102 154 / 10%)",
  },
  textfield: {
    background: "rgb(0 204 144 / 10%)",
  },
  email: {
    background: "rgb(0 204 144 / 10%)",
  },
  phone: {
    background: "rgb(243 102 154 / 10%)",
  },
  website: {
    background: "rgb(134 117 255 / 10%)",
  },
  number: {
    background: "rgb(0 204 144 / 10%)",
  },
  location: {
    background: "rgb(134 117 255 / 10%)",
  },
  money: {
    background: "rgb(243 102 154 / 10%)",
  },
  textarea: {
    background: "rgb(243 102 154 / 10%)",
  },
  rating: {
    background: "rgb(95 183 236 / 10%)",
  },
  country: {
    background: "rgb(255 161 72 / 10%)",
  },
};
