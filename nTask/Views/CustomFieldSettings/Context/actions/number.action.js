import {
    UPDATE_DECIMAL_PLACES,
    UPDATE_UNIT,
    UPDATE_DIRECTION
  } from "../constants";
  
  export const updateNumberConfigue = (dispatch, obj) => {
    dispatch({ type: UPDATE_DECIMAL_PLACES, payload: obj });
  };
  export const updateUnitConfigue = (dispatch, obj) => {
    dispatch({ type: UPDATE_UNIT, payload: obj });
  };
  export const updateDirectionConfigue = (dispatch, obj) => {
    dispatch({ type: UPDATE_DIRECTION, payload: obj });
  };
