import {
    UPDATE_MATRIX_CONFIG,
    EDIT_COPY_MATRIX
  } from "../constants";
  
  export const updateMatrixConfig = (dispatch, obj) => {
    dispatch({ type: UPDATE_MATRIX_CONFIG, payload: obj });
  };
  export const editMatrix = (dispatch, obj) => {
    dispatch({ type: EDIT_COPY_MATRIX, payload: obj });
  };
