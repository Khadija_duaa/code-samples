import {
  UPDATE_OPTION,
  SELECT_FIELD_TYPE,
  ADD_OPTION,
  ADD_DROPDOWN_CONFIG,
  REMOVE_OPTION,
  GET_CUSTOM_FIELD_TYPES,
  SET_VIEW_TYPE,
  SET_FIELD_NAME_TYPE,
  CLEAR_FIELDS,
  SET_LEVEL,
  UPDATE_CONFIG,
  SET_FIELD_AND_VIEW_TYPE,
  EDIT_COPY_VIEW_TYPE,
  UPDATE_COLOR_OPTION,
  SET_BACK_VIEW,
  SET_ALL_WORKSPACE_LEVEL,
  SET_SECTION
} from "../constants";

export const dispatchDropdownOption = (dispatch, obj) => {
  dispatch({ type: UPDATE_OPTION, payload: obj });
};
export const dispatchDropdownColorOption = (dispatch, obj) => {
  dispatch({ type: UPDATE_COLOR_OPTION, payload: obj });
};
export const dispatchAddDropdownOption = (dispatch, obj) => {
  dispatch({ type: ADD_OPTION });
};
export const dispatchSelectFieldType = (dispatch, option) => {
  dispatch({ type: SELECT_FIELD_TYPE, payload: option });
};
export const dispatchDropdownConfig = (dispatch, option) => {
  dispatch({ type: ADD_DROPDOWN_CONFIG, payload: option });
};
export const dispatchDeleteDropdownOption = (dispatch, option) => {
  dispatch({ type: REMOVE_OPTION, payload: option });
};
export const dispatchCustomFieldsType = (dispatch, option) => {
  dispatch({ type: GET_CUSTOM_FIELD_TYPES, payload: option });
};
export const dispatchSetViewType = (dispatch, option) => {
  dispatch({ type: SET_VIEW_TYPE, payload: option });
};
export const dispatchBackView = (dispatch) => {
  dispatch({ type: SET_BACK_VIEW, payload: null });
};
export const dispatchFieldTypeAndView = (dispatch, option) => {
  dispatch({ type: SET_FIELD_AND_VIEW_TYPE, payload: option });
};
export const dispatchFieldName = (dispatch, option, fieldType) => {
  dispatch({ type: SET_FIELD_NAME_TYPE, payload: {option, fieldType} });
};
export const dispatchLevel = (dispatch, option, fieldType) => {
  dispatch({ type: SET_LEVEL, payload: {option, fieldType} });
};
export const dispatchAllWorkspaceLevel = (dispatch, option, fieldType) => {
  dispatch({ type: SET_ALL_WORKSPACE_LEVEL, payload: {option, fieldType} });
};
export const dispatchConfigChange = (dispatch, option) => {
  dispatch({ type: UPDATE_CONFIG, payload: option });
};
export const dispatchEditViewType = (dispatch, option, mode) => {
  dispatch({ type: EDIT_COPY_VIEW_TYPE, payload: {option,mode} });
};
export const clearFields = dispatch => {
  dispatch({ type: CLEAR_FIELDS });
};
export const dispatchSection = (dispatch, option, fieldType) => {
  dispatch({ type: SET_SECTION, payload: {option, fieldType } });
};
