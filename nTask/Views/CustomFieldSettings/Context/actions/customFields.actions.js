import {
    UPDATE_ERRORS,
    DISPATCH_SETTINGS
  } from "../constants";
  
  export const updateErrors = (dispatch, obj) => {
    dispatch({ type: UPDATE_ERRORS, payload: obj });
  };
  export const dispatchSettings = (dispatch, obj, fieldType) => {
    dispatch({ type: DISPATCH_SETTINGS, payload: {obj , fieldType}  });
  };
