import cloneDeep from "lodash/cloneDeep";
import { fieldTypeData } from "../../../helper/customFieldsData";
import { randomColor } from "../../../helper/getRandomColor";
import {
  UPDATE_OPTION,
  ADD_OPTION,
  SELECT_FIELD_TYPE,
  ADD_DROPDOWN_CONFIG,
  REMOVE_OPTION,
  GET_CUSTOM_FIELD_TYPES,
  SET_VIEW_TYPE,
  SET_FIELD_NAME_TYPE,
  CLEAR_FIELDS,
  SET_LEVEL,
  UPDATE_CONFIG,
  SET_FIELD_AND_VIEW_TYPE,
  EDIT_COPY_VIEW_TYPE,
  UPDATE_MATRIX_CONFIG,
  UPDATE_COLOR_OPTION,
  EDIT_COPY_MATRIX,
  UPDATE_ERRORS,
  SET_BACK_VIEW,
  SET_ALL_WORKSPACE_LEVEL,
  UPDATE_DECIMAL_PLACES,
  UPDATE_UNIT,
  UPDATE_DIRECTION,
  DISPATCH_SETTINGS,
  SET_SECTION,
} from "./constants";
import { randomId } from "../../../helper/getRandomId";
import initialState from "./initialState";

function reducer(state, action) {
  switch (action.type) {
    case UPDATE_ERRORS: {
      return {
        ...state,
        errors: action.payload,
      };
    }
    case UPDATE_MATRIX_CONFIG: {
      const stateCopy = cloneDeep(state.matrix);
      stateCopy.settings = { ...stateCopy.settings, ...action.payload };
      return {
        ...state,
        matrix: stateCopy,
      };
    }
    case GET_CUSTOM_FIELD_TYPES: {
      return {
        ...state,
        fieldTypes: action.payload,
        selectedField: fieldTypeData([action.payload[0]])[0],
      };
    }
    case UPDATE_DECIMAL_PLACES: {
      const stateCopy = cloneDeep(state.number);
      stateCopy.settings = { ...stateCopy.settings, ...action.payload };
      return {
        ...state,
        number: stateCopy,
      };
    }
    case UPDATE_UNIT: {
      const stateCopy = cloneDeep(state.number);
      stateCopy.settings = { ...stateCopy.settings, ...action.payload };
      return {
        ...state,
        number: stateCopy,
      };
    }
    case UPDATE_DIRECTION: {
      const stateCopy = cloneDeep(state.number);
      stateCopy.settings = { ...stateCopy.settings, ...action.payload };
      return {
        ...state,
        number: stateCopy,
      };
    }
    case UPDATE_OPTION: {
      const stateCopy = cloneDeep(state);
      stateCopy.dropdown.options[action.payload.index].value = action.payload.value;
      return stateCopy;
    }
    case UPDATE_COLOR_OPTION: {
      const stateCopy = cloneDeep(state);
      stateCopy.dropdown.options[action.payload.index].color = action.payload.color;
      return stateCopy;
    }
    case ADD_OPTION: {
      const stateCopy = cloneDeep(state);
      stateCopy.dropdown.options = [
        ...stateCopy.dropdown.options,
        { value: "", color: "", id: randomId() },
      ];
      return stateCopy;
    }
    case REMOVE_OPTION: {
      const index = action.payload;
      const stateCopy = cloneDeep(state);
      stateCopy.dropdown.options.splice(index, 1);
      return stateCopy;
    }
    case SELECT_FIELD_TYPE: {
      return { ...state, selectedField: action.payload };
    }
    case ADD_DROPDOWN_CONFIG: {
      const stateCopy = cloneDeep(state);
      stateCopy.dropdown.config = { ...stateCopy.dropdown.config, ...action.payload };
      return stateCopy;
    }
    case SET_VIEW_TYPE: {
      const stateCopy = cloneDeep(state);
      stateCopy.viewType = action.payload;
      return stateCopy;
    }
    case SET_BACK_VIEW: {
      return {
        ...initialState,
        selectedField: state.selectedField,
        viewType: "optionsSelect",
        fieldTypes: state.fieldTypes,
      };
    }
    case SET_FIELD_NAME_TYPE: {
      const stateCopy = cloneDeep(state[action.payload.fieldType]);
      stateCopy.fieldName = action.payload.option;
      return {
        ...state,
        [action.payload.fieldType]: stateCopy,
      };
    }
    case SET_SECTION: {
      const stateCopy = cloneDeep(state[action.payload.fieldType]);
      if (stateCopy.sectionId == null) {
        stateCopy.sectionId = [action.payload.option.newSectionId];
      } else {
        let oldSectionIdIndex = stateCopy.sectionId.indexOf(action.payload.option.oldSectionId);
        if (oldSectionIdIndex < 0)
          stateCopy.sectionId = [...stateCopy.sectionId, action.payload.option.newSectionId];
        else stateCopy.sectionId[oldSectionIdIndex] = action.payload.option.newSectionId;
      }
      return {
        ...state,
        [action.payload.fieldType]: stateCopy,
      };
    }
    case EDIT_COPY_VIEW_TYPE: {
      const stateCopy = cloneDeep(state);
      stateCopy[action.payload.option.fieldType].fieldName =
        action.payload.mode == "edit"
          ? action.payload.option.fieldName
          : `Copy of ${action.payload.option.fieldName}`;
      stateCopy[action.payload.option.fieldType].fieldId = action.payload.option.fieldId;
      stateCopy[action.payload.option.fieldType].sectionId = action.payload.option.sectionId;
      stateCopy[action.payload.option.fieldType].level =
        action.payload.option.level == "team" ? "All Workspaces" : "This Workspace";
      stateCopy[action.payload.option.fieldType].options = action.payload.option.values.data;
      stateCopy.dropdown.config.multiSelect = action.payload.option.settings.multiSelect;
      stateCopy[action.payload.option.fieldType].settings = action.payload.option.settings;
      if(action.payload.option.fieldType ===  "dropdown") {
        stateCopy[action.payload.option.fieldType].config = action.payload.option.settings;
      }
      stateCopy.viewMode = action.payload.mode;
      return stateCopy;
    }
    case EDIT_COPY_MATRIX: {
      return { ...state, matrix: action.payload, viewMode: "edit" };
    }
    case SET_LEVEL: {
      const stateCopy = cloneDeep(state);
      stateCopy[action.payload.fieldType].level = action.payload.option;
      return stateCopy;
    }
    case SET_ALL_WORKSPACE_LEVEL: {
      const stateCopy = cloneDeep(state);
      stateCopy[action.payload.fieldType].level = action.payload.option;
      stateCopy.matrix.settings.assessment = [];
      return stateCopy;
    }
    case CLEAR_FIELDS: {
      return initialState;
    }
    case UPDATE_CONFIG: {
      let stateCopy = cloneDeep(state);
      return stateCopy;
    }
    case SET_FIELD_AND_VIEW_TYPE: {
      const stateCopy = cloneDeep(state);
      stateCopy.viewType = action.payload.viewType;
      stateCopy.selectedField = action.payload.selectedField;
      return stateCopy;
    }
    case DISPATCH_SETTINGS: {
      const stateCopy = cloneDeep(state[action.payload.fieldType]);
      if (action.payload.fieldType === "dropdown") {
        stateCopy.config = { ...stateCopy.config, ...action.payload.obj };
        stateCopy.settings = { ...stateCopy.settings, ...action.payload.obj };
      } else stateCopy.settings = { ...stateCopy.settings, ...action.payload.obj };
      return {
        ...state,
        [action.payload.fieldType]: stateCopy,
      };
    }
    default:
      throw new Error();
  }
}

export default reducer;
