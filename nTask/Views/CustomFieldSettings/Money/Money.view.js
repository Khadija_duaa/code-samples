import React, { useContext } from "react";
import moneyFieldStyles from "./money.style";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import SelectSearchDropdown from "../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { compose } from "redux";
import { connect } from "react-redux";
import {
  UpdateUserPreferences,
} from "../../../redux/actions/profileSettings";
import { dispatchSettings } from "../Context/actions/customFields.actions";
import CustomFieldsContext from "../Context/customFields.context";

function MoneyField(props) {
  const { classes, userPreferenceState } = props;
  const {
    state: { money },
    dispatch,
  } = useContext(CustomFieldsContext);
  const { settings } = money;

  //Generating money select data
  const handleCurrency = () => {
    {
      return userPreferenceState.data.constant.currencies.map(c => {
        return {
          label: [`${c.isoCurrencySymbol} - ${c.currencyEnglishName} (${c.currencySymbol}) `],
          value: c.isoCurrencySymbol,
          obj: c
        }
      })
    }
  }
  const handleSelect = (type, option) => {
    dispatchSettings(dispatch, { currency: option.value }, "money");
  };
  let selectedCurency = handleCurrency().find(s=> s.value === settings.currency) || {};

  return (
    <>

      <Grid item xs={12} classes={{ item: classes.filterItem }}>
        <SelectSearchDropdown
          data={() => handleCurrency()}
          placeholder={"Select"}
          icon={false}
          label={"Currency"}
          isMulti={false}
          selectChange={(type, option) => {
            handleSelect(type, option);
          }}
          selectedValue={selectedCurency}
          styles={{ marginBottom: 10 }}
        />
      </Grid>
    </>
  )

}
const mapStateToProps = (state) => {
  return {
    userPreferenceState: state.userPreference,
  };
};
export default compose(
  connect(mapStateToProps, {
    UpdateUserPreferences
  }),
  withStyles(moneyFieldStyles, { withTheme: true })
)(MoneyField);
