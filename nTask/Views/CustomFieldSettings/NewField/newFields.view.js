import React, { useContext, useEffect, useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import { withSnackbar } from "notistack";

import newFieldStyles from "./newFields.view.style";
import CustomDialog from "../../../components/Dialog/CustomDialog";
import CustomFieldsContext from "../Context/customFields.context";
import DefaultTextField from "../../../components/Form/TextField";
import Grid from "@material-ui/core/Grid";
import { fieldTypeData, generateSectionData } from "../../../helper/customFieldsData";
import SelectSearchDropdown from "../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import ButtonActionsCnt from "../../../components/Dialog/ConfirmationDialogs/ButtonActionsCnt";
import DropdownField from "../Dropdown/DropdownField.view";

import {
  dispatchSelectFieldType,
  dispatchSetViewType,
  dispatchFieldName,
  clearFields,
  dispatchCustomFieldsType,
  dispatchLevel,
  dispatchDropdownConfig,
  dispatchBackView,
  dispatchAllWorkspaceLevel,
  dispatchSection,
} from "../Context/actions/dropdown.actions";
import { updateErrors, dispatchSettings } from "../Context/actions/customFields.actions";
import {
  addCustomField,
  getCustomFieldTypes,
  editCustomField,
  dispatchDeleteField,
  updateCustomFieldData,
} from "../../../redux/actions/customFields";
import CustomRadio from "../../../components/Form/Radio/Radio";
import Matrix from "../Matrix/matrix.view";
import DefaultCheckbox from "../../../components/Form/Checkbox";
import cloneDeep from "lodash/cloneDeep";
import { dropdownFieldSchema, matrixFieldSchema } from "../schema";
import NumberField from "../Number/Number.view";
import PhoneField from "../Phone/Phone.view";
import PeopleField from "../People/Person/People.view";
import MoneyField from "../Money/Money.view";
import RatingField from "../Rating/Rating.view";
import SectionField from "../Section/Section.view";
import {
  updateCustomFieldDialogState,
  updateDeleteDialogState,
} from "../../../redux/actions/allDialogs";
import { getCustomFields } from "../../../helper/customFieldsData";
import { grid } from "../../../components/CustomTable2/gridInstance";

const fieldLayoutData = [
  {
    label: "Full Width",
    value: 12,
    color: "",
    id: 12,
    obj: {},
  },
  {
    label: "1/2 Column",
    value: 6,
    color: "",
    id: 6,
    obj: {},
  },
  // {
  //   label: "2/2 Column",
  //   value: 2,
  //   color: "",
  //   id: 2,
  //   obj: {},
  // },
];
function NewFieldView(props) {
  const {
    state,
    state: {
      selectedField,
      dropdown: { config },
      dropdown,
      fieldTypes,
      viewType,
      matrix,
      viewMode /** if the modal is opening in edit mode or create mode */,
    },
    dispatch,
    onFieldAdd,
    onFieldEdit,
    onFieldDelete,
    permission,
  } = useContext(CustomFieldsContext);
  const {
    classes,
    theme,
    profile,
    feature,
    addCustomField,
    editCustomField,
    customFields,
    dispatchDeleteField,
    updateCustomFieldData,
    disableAssessment,
    updateCustomFieldDialogState,
    dialogsState,
    getCustomFieldTypes,
    updateDeleteDialogState,
    enqueueSnackbar,
    allFieldsColumns,
  } = props;
  const { customFieldDialog } = dialogsState;

  const [btnQuery, setBtnQuery] = useState("");
  const [btnQueryDelete, setBtnQueryDelete] = useState("");
  const [errorState, setErrorState] = useState(false);
  const [errorStateMessage, setErrorStateMessage] = useState("");

  //Dialog Close
  const closeDialog = () => {
    dispatchSetViewType(dispatch, "");
    clearFields(dispatch);
    updateCustomFieldDialogState({ moduleViewType: "", data: {}, mode: "" });
  };

  const handleFieldTypeSelect = (type, option) => {
    dispatchSelectFieldType(dispatch, option);
  };

  const createRiskMatrix = () => {
    matrixFieldSchema
      .validate(
        { rowTitle: matrix.settings.rows.title, columnTitle: matrix.settings.columns.title },
        { abortEarly: false }
      )
      .then(succ => {
        const defaultMatrix =
          customFields.data.find(
            m => m.settings.assessment && m.settings.assessment.indexOf(profile.loggedInTeam) >= 0
          ) || false;
        /** function that generates the data and dispatch object to the Api */
        setBtnQuery("progress");
        const { fieldName, level, fieldId } = state[selectedField.value];
        const { fieldType } = selectedField.obj;
        const fieldLevel = level === "All Workspaces" ? "team" : "workspace";
        const workspace = [{ workspaceId: profile.loggedInTeam, groupType: [feature] }];
        const team = { teamId: profile.activeTeam, groupType: [feature] };
        let object = {
          fieldName: fieldName,
          fieldType,
          level: fieldLevel,
          groupType: feature,
          settings: matrix.settings,
          sectionId: matrix.sectionId,
          values: {
            data: [],
          },
          team: fieldLevel == "team" ? team : {},
          workspaces: fieldLevel == "team" ? [] : workspace,
          placeHolder: "NTask Selection Entry",
        };

        let oldFieldObject =
          customFields.data.find(cf => cf.fieldId === fieldId) ||
          false; /** fetching old field so if user already selected edit filed as a column, then on edit its name will be updated and add in risk column order */

        /** In case of new field or copy field */
        let validFieldName = validateCustomFieldName(
          object.fieldName
        ); /** validate custom field name */
        if (!validFieldName && viewMode == "copy") {
          setErrorState(true);
          setErrorStateMessage("Field name already exists!");
          setBtnQuery("");

          return;
        }

        if (
          matrix.settings.assessment.includes(profile.loggedInTeam) &&
          defaultMatrix &&
          defaultMatrix.settings.assessment.includes(profile.loggedInTeam) &&
          defaultMatrix.fieldName !== fieldName
        ) {
          /** if there is any risk matrix available who is using as a default matrix / use as a assesment then edit this assement to false and set a new default assesment  */
          let temp = cloneDeep(defaultMatrix);
          temp.settings.assessment = temp.settings.assessment.filter(
            t => t !== profile.loggedInTeam
          );
          editCustomFieldApi(temp, temp.fieldId, oldFieldObject);
        }
        if (viewMode == "edit") {
          editCustomFieldApi(object, fieldId, oldFieldObject);
        } else {
          /** In case of new field or copy field */
          let validFieldName = validateCustomFieldName(
            object.fieldName
          ); /** validate custom field name */
          if (!validFieldName) {
            setErrorState(true);
            setErrorStateMessage("Field name already exists!");
            setBtnQuery("");

            return;
          }
          addCustomField(
            object,
            res => {
              setBtnQuery("");
              closeDialog();
              // onFieldAdd(res);
            },
            err => {
              setBtnQuery("");
              showSnackbar(err.data.message, "error");
            }
          );
        }
      })
      .catch(err => {
        const errors = err.inner.reduce((r, cv) => {
          return { ...r, [cv.path]: cv.message };
        }, {});
        const errObj = { fieldType: "matrix", errors };
        updateErrors(dispatch, errObj);
      });
  };

  const editCustomFieldApi = (object, id, oldFieldObject) => {
    /** if editing fields then call edit API otherwise add new field */
    delete object.team;
    delete object.workspaces;
    editCustomField(
      object,
      id,
      res => {
        setBtnQuery("");
        closeDialog();
        onFieldEdit(res, oldFieldObject);
        setTimeout(() => {
          grid.grid && grid.grid.redrawRows();
        }, 0);
      },
      failure => {
        setBtnQuery("");
      }
    );
  };
  const getFieldSettings = () => {
    return state[selectedField.value].settings;
  };
  //Function for create checklist and dropdown field
  const createCustomField = () => {
    const { options, sectionId, fieldName, level, fieldId } = state[selectedField.value];
    const { fieldType } = selectedField.obj;
    const fieldLevel = level === "All Workspaces" ? "team" : "workspace";
    const workspace = [{ workspaceId: profile.loggedInTeam, groupType: [feature] }];
    const team = { teamId: profile.activeTeam, groupType: [feature] };

    let object = {
      fieldName: fieldName,
      fieldType,
      level: fieldLevel,
      groupType: feature,
      settings:
        selectedField.value == "dropdown"
          ? {
              multiSelect: config.multiSelect,
              isHidden: config.isHidden,
              hiddenInWorkspaces: config.hiddenInWorkspaces,
              hiddenInGroup: config.hiddenInGroup,
              layoutWidth: config.layoutWidth,
            }
          : getFieldSettings(),
      values: {
        data: options,
      },
      team: fieldLevel == "team" ? team : null,
      workspaces: fieldLevel == "team" ? [] : workspace,
      placeHolder: "NTask Selection Entry",
      sectionId: sectionId,
    };

    let oldFieldObject =
      customFields.data.find(cf => cf.fieldId === fieldId) ||
      false; /** fetching old field so if user already selected edit filed as a column, then on edit its name will be updated and add in risk column order */

    /** In case of new field or copy field */
    let validFieldName = validateCustomFieldName(
      object.fieldName
    ); /** validate custom field name */
    if (!validFieldName && viewMode == "copy") {
      setErrorState(true);
      setErrorStateMessage("Field name already exists!");
      setBtnQuery("");

      return;
    }
    /** function that generates the data and dispatch object to the Api */
    setBtnQuery("progress");
    if (fieldType === "checklist") object.settings.isHideCompletedTodos = false;
    if (viewMode == "edit") {
      /** In case of new field or copy field */
      if (
        oldFieldObject.fieldName !== object.fieldName &&
        !validateCustomFieldName(object.fieldName)
      ) {
        setErrorState(true);
        setErrorStateMessage("Field name already exists!");
        setBtnQuery("");

        return;
      } /** validate custom field name */

      /** if editing fields then call edit API otherwise add new field */
      delete object.team;
      delete object.workspaces;

      editCustomField(
        object,
        fieldId,
        res => {
          grid.grid && grid.grid.redrawRows();
          setTimeout(() => {
            setBtnQuery("");
            closeDialog();
            onFieldEdit(res, oldFieldObject);
            updateCustomFieldData(res);
          }, 2000);
        },
        failure => {
          setBtnQuery("");
        }
      );
    } else {
      /** In case of new field or copy field */
      let validFieldName = validateCustomFieldName(
        object.fieldName
      ); /** validate custom field name */
      if (!validFieldName) {
        setErrorState(true);
        setErrorStateMessage("Field name already exists!");
        setBtnQuery("");

        return;
      }
      addCustomField(
        object,
        res => {
          setBtnQuery("");
          setBtnQueryDelete("");
          closeDialog();
          onFieldAdd(res);
        },
        err => {
          setBtnQuery("");
          showSnackbar(err.data.message, "error");
        }
      );
    }
  };
  const compilingData = () => {
    const { fieldType } = selectedField.obj;
    const { options, fieldName } = dropdown;

    switch (fieldType) {
      case "dropdown":
        dropdownFieldSchema
          .validate({ options, fieldName }, { abortEarly: false })
          .then(succ => {
            createCustomField();
          })
          .catch(err => {
            const errObj = { fieldType: "dropdown", errors: err.errors };
            updateErrors(dispatch, errObj);
          });
        break;
      default:
        createCustomField();
        break;
    }
  };

  const validateCustomFieldName = param => {
    /** validate custom field name */
    return (
      param !== "Details" &&
      param !== "Mitigation Plan" &&
      param !== "Description" &&
      param !== "To-do Items List" &&
      allFieldsColumns.filter(ele => ele.columnName == param).length < 1
    );
  };

  const handleSuccessAddField = () => {
    /** function trrigers when user clicks on add field button */
    // if (selectedField.value && selectedField.value === "dropdown")
    if (selectedField.obj && selectedField.obj.fieldType === "matrix") {
      createRiskMatrix();
    } else {
      compilingData();
    }
  };

  const handleFieldNameChange = e => {
    const value = e.target.value;
    dispatchFieldName(dispatch, value, selectedField.value);
    setErrorState(false);
    setErrorStateMessage("");
    setBtnQuery("");
  };
  const handleLevelChange = e => {
    const value = e.target.value;
    switch (value) {
      case "All Workspaces":
        dispatchAllWorkspaceLevel(dispatch, value, selectedField.value);
        break;
      case "This Workspace":
        dispatchLevel(dispatch, value, selectedField.value);
        break;

      default:
        break;
    }
  };
  const handleChangeFieldWidth = (type, option) => {
    dispatchSettings(dispatch, { layoutWidth: option.value }, selectedField.value);
  };

  const handleBackClick = () => {
    // clearFields(dispatch);
    setErrorState(false);
    setErrorStateMessage("");
    dispatchBackView(dispatch);
    updateCustomFieldDialogState({ moduleViewType: "", data: {}, mode: "" });
    getCustomFieldTypes(res => {
      dispatchCustomFieldsType(dispatch, res);
    });
  };
  const handleDelete = () => {
    updateDeleteDialogState({
      open: true,
      cancelBtnText: "Cancel",
      successBtnText: "Delete",
      headingText: "Delete",
      msgText: "Are you sure you want to delete this field?",
      successAction: handleSuccessDelete,
      closeAction: () => updateDeleteDialogState({ open: false }),
      btnQuery: "",
    });
  };
  const handleSuccessDelete = param => {
    /** handle delete function responsible for deleting the custom field */
    setBtnQueryDelete("progress");
    updateDeleteDialogState({ btnQuery: "progress" });
    let id = state[selectedField.value].fieldId;
    let obj = [id];
    dispatchDeleteField(
      obj,
      succ => {
        updateDeleteDialogState({ btnQuery: "", open: false });

        handleBackClick();
        setBtnQueryDelete("");
        onFieldDelete({ param });
      },
      fail => {
        updateDeleteDialogState({ btnQuery: "", open: false });

        setBtnQueryDelete("");
      }
    );
  };
  //Handles Multi select checkbox
  const handleMultiSelect = () => {
    dispatchDropdownConfig(dispatch, { multiSelect: !config.multiSelect });
  };

  const showSnackbar = (snackBarMessage, type) => {
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };

  const cf = customFields.data.find(c => c.fieldId == state[selectedField.value].fieldId) || false;
  const filterSections = getCustomFields(customFields, profile, feature).filter(
    f => f.fieldType == "section"
  );
  const fieldTypeDdata =
    feature == "risk"
      ? fieldTypeData(fieldTypes)
      : fieldTypeData(fieldTypes).filter(f => f.value !== "matrix");
  let width = state[selectedField.value].settings.layoutWidth;
  let selectedFieldWidth = fieldLayoutData.filter(f => f.value === width);
  let sections = generateSectionData(filterSections);

  let selectedSection = state[selectedField.value].sectionId
    ? sections.reduce((result, cv) => {
        let isExist = state[selectedField.value].sectionId.find(id => id === cv.obj.fieldId);
        if (isExist) result.push(cv);
        return result;
      }, [])
    : [];

  // let selectedSection = sections.filter(
  //   s => s.obj.fieldId === state[selectedField.value].sectionId
  // );
  let selectedfieldName = state[selectedField.value].fieldName;
  let selectedLevel = state[selectedField.value].level;
  let layoutData = () => {
    if (
      selectedField.obj &&
      (selectedField.obj.fieldType === "checklist" ||
        selectedField.obj.fieldType === "textarea" ||
        selectedField.obj.fieldType === "matrix" ||
        selectedField.obj.fieldType === "location")
    )
      return fieldLayoutData.filter(layout => layout.value === 12);
    return fieldLayoutData;
  };
  const handleSelectSection = (type, option) => {
    dispatchSection(
      dispatch,
      {
        oldSectionId: selectedSection.length ? selectedSection[0].obj.fieldId : "",
        newSectionId: option ? option.obj.fieldId : "",
      },
      selectedField.value
    );
  };
  return (
    <>
      <CustomDialog
        title={
          viewMode == "edit"
            ? "Edit Field"
            : viewMode == "new"
            ? "Create New Field"
            : viewMode == "copy"
            ? "Copy Field"
            : ""
        }
        backBtn={customFieldDialog.moduleViewType == ""}
        handleBackClick={handleBackClick}
        dialogProps={{
          open: viewType === "newField",
          fullWidth: false,
          onClose: closeDialog,
          PaperProps: {
            className: classes.dialogPaperCmp,
            style: {
              maxWidth: selectedField.obj && selectedField.obj.fieldType === "matrix" && 1024,
            },
          },
          disableBackdropClick: true,
          disableEscapeKeyDown: true,
        }}
        headerStyles={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
        }}>
        <div className={classes.dialogContentCnt}>
          <Grid container spacing={2} className={classes.fieldDetailsCnt}>
            <Grid item xs={6}>
              <DefaultTextField
                label="Field Name"
                errorState={errorState}
                errorMessage={errorStateMessage}
                defaultProps={{
                  id: "Field Name",
                  placeholder: "Enter field name",
                  onChange: e => handleFieldNameChange(e),
                  value: selectedfieldName,
                  disabled: cf && cf.isSystem && viewMode !== "copy",
                  inputProps: { maxLength: 80 },
                  autoComplete: "off",
                }}
                labelStyle={{
                  fontFamilyLato: theme.typography.fontFamilyLato,
                  color: theme.palette.text.primary,
                }}
              />
            </Grid>

            <Grid item xs={6}>
              <SelectSearchDropdown
                data={() => {
                  return fieldTypeDdata;
                }}
                label="Field Type"
                selectChange={handleFieldTypeSelect}
                type="fieldType"
                selectedValue={selectedField}
                placeholder="Select Field Type"
                isMulti={false}
                isDisabled={viewMode !== "new"}
                icon={true}
                labelStyle={{
                  fontFamilyLato: theme.typography.fontFamilyLato,
                  color: theme.palette.text.primary,
                }}
              />
            </Grid>
          </Grid>
          {selectedField.obj && selectedField.obj.fieldType === "number" && <NumberField />}
          {selectedField.obj && selectedField.obj.fieldType === "phone" && <PhoneField profile={profile} />}
          {selectedField.obj && selectedField.obj.fieldType === "people" && <PeopleField />}
          {selectedField.obj && selectedField.obj.fieldType === "money" && <MoneyField />}
          {selectedField.obj && selectedField.obj.fieldType === "section" && <SectionField />}
          {selectedField.obj && selectedField.obj.fieldType === "rating" && <RatingField />}
          {selectedField.obj && selectedField.obj.fieldType === "dropdown" && (
            <>
              <DropdownField />
              {viewMode !== "edit" && (
                <DefaultCheckbox
                  checked={config.multiSelect}
                  label="Multiple Selection Dropdown"
                  onChange={handleMultiSelect}
                  styles={{
                    display: "flex",
                    alignItems: "center",
                    marginTop: 15,
                    marginBottom: 10,
                  }}
                  checkboxStyles={{ padding: "0 5px 0 0" }}
                  disabled={cf && cf.isSystem && viewMode !== "copy"}
                  labelStyle={{
                    fontFamily: theme.typography.fontFamilyLato,
                    color: theme.palette.text.primary,
                  }}
                />
              )}{" "}
            </>
          )}
          {selectedField.obj && selectedField.obj.fieldType === "matrix" && (
            <Matrix disableAssessment={disableAssessment} data={matrix} />
          )}
          <div className={classes.hl}></div>
          {selectedField.obj &&
            selectedField.obj.fieldType !== "section" &&
            selectedField.obj.fieldType !== "matrix" && 
            !cf.isSystem && (
              <Grid container spacing={2} className={classes.fieldDetailsCnt}>
                <Grid item xs={8}>
                  <SelectSearchDropdown
                    data={() => sections} /* function calling for generating task array */
                    label={"Add to Section"}
                    isMulti={false}
                    optionBackground={true}
                    selectChange={(type, option) => {
                      handleSelectSection(type, option);
                    }} /** function calling on select task in drop down */
                    selectRemoveValue={(type, option) => {}}
                    selectedValue={selectedSection}
                    placeholder={"Select"}
                    isDisabled={false}
                    isClearable={true}
                    labelStyle={{
                      marginBottom: 15,
                      fontFamilyLato: theme.typography.fontFamilyLato,
                      color: theme.palette.text.primary,
                    }}
                    selectClear={(type, option) => {
                      handleSelectSection(type, option);
                    }}
                  />
                </Grid>
                <Grid item xs={4}>
                  <SelectSearchDropdown
                    data={layoutData} /* function calling for generating task array */
                    label={"Field Layout"}
                    isMulti={false}
                    selectChange={(type, option) => {
                      handleChangeFieldWidth(type, option);
                    }} /** function calling on select task in drop down */
                    selectRemoveValue={(type, option) => {}}
                    selectedValue={selectedFieldWidth}
                    placeholder={"Select"}
                    optionBackground={false}
                    isDisabled={false}
                    labelStyle={{
                      marginBottom: 15,
                      fontFamilyLato: theme.typography.fontFamilyLato,
                      color: theme.palette.text.primary,
                    }}
                  />
                </Grid>
              </Grid>
            )}
          {/* {!cf || !cf.isSystem || viewMode !== "edit"  ? ( */}
          {viewMode !== "edit" ? (
            <>
              <label className={classes.fieldLabel}>Add this field to</label>
              <div className={classes.radioBtnCnt}>
                <CustomRadio
                  options={["All Workspaces", "This Workspace"]}
                  value={selectedLevel}
                  onRadioChange={handleLevelChange}
                  iconProps={{ style: { fontSize: "18px" } }}
                  radioGroupProps={{
                    style: { display: "flex", alignItems: "center", flexDirection: "row" },
                  }}
                />
              </div>
            </>
          ) : null}
        </div>
        <ButtonActionsCnt
          cancelAction={() => {
            handleBackClick();
          }}
          successAction={() => {
            handleSuccessAddField();
          }}
          successBtnText={
            viewMode == "edit"
              ? "Save Changes"
              : viewMode == "new"
              ? "Create Field"
              : viewMode == "copy"
              ? "Copy Field"
              : ""
          }
          deleteBtnText={viewMode == "edit" && (!cf || !cf.isSystem) ? "Delete Field" : undefined}
          // deleteBtnText={undefined}
          deleteAction={() => {
            handleDelete();
          }}
          btnTypeDelete={"danger"}
          btnTypeVariant="contained"
          cancelBtnText="Cancel"
          btnType="blue"
          btnQuery={btnQuery}
          btnQueryDelete={btnQueryDelete}
          disabled={selectedfieldName == ""}
        />
      </CustomDialog>
    </>
  );
}
const mapStateToProps = state => {
  return {
    profile: state.profile.data,
    customFields: state.customFields,
    dialogsState: state.dialogStates,
    allFieldsColumns: state.columns.allColumns || [],
  };
};
export default compose(
  withSnackbar,
  connect(mapStateToProps, {
    addCustomField,
    editCustomField,
    dispatchDeleteField,
    updateCustomFieldData,
    dispatchSettings,
    updateCustomFieldDialogState,
    getCustomFieldTypes,
    updateDeleteDialogState,
  }),
  withStyles(newFieldStyles, { withTheme: true })
)(NewFieldView);
