const newFieldStyles = theme => ({
  dialogContentCnt: {
    padding: "20px 30px 0 30px",
  },
  searchInput: {
    // padding: "4px 8px 5px 8px",
    width: "100%",
    outline: "none",
    border: "none",
    fontSize: "14px !important",
    // lineHeight: "18px",
    fontWeight: theme.typography.fontWeightMedium,
    fontFamily: "Lato, sans-serif",
    borderRadius: 0,
    cursor: "pointer",
    height: 35,
  },
  searchIcon: {
    fontSize: "14px !important",
  },
  btnsCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },

  radioBtnCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    // borderTop: `1px solid ${theme.palette.border.lightBorder}`,
    // padding: "7px 0 7px 0",
    // marginTop: 10,
    marginBottom: 20,
    marginTop: 4,
  },
  hl: {
    borderTop: `1px solid ${theme.palette.border.lightBorder}`,
    // margin: "10px 0 20px 0px"
    marginBottom: 10,
    marginTop: 10
  },
  fieldLabel:{
    fontWeight: theme.typography.fontWeightMedium,
    fontFamily: "Lato, sans-serif",
    color: "#646464",
    fontSize: "13px !important"
  },
  fieldDetailsCnt: {
    width: 556,
    height:90
  }
});

export default newFieldStyles;
