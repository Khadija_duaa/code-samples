const ratingFieldStyle = theme => ({
  btnsCnt: {
    // display: 'flex',
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 10,
  },
  addAnotherOptBtn: {
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
    color: "#0090ff",
    textDecoration: "underline",
    cursor: "pointer",
  },
  ToggleButtonSelected: {
    border: "1px solid rgb(0, 144, 255)",
    borderRadius: "6px 0px 0px 6px",
    backgroundColor: "#fff",
  },
  addIcon: {
    fontSize: "18px !important",
    marginBottom: -4,
  },
  optionInputCnt: {
    display: "flex",
    alignItems: "center",
  },
  deleteIcon: {
    fontSize: "19px !important",
    marginLeft: 15,
    cursor: "pointer",
  },
  MuiTypography: {
    color: "red",
  },
  unitLabel: {
    font: "normal normal normal 13px/16px Lato",
    color: "#171717",
    fontWeight: 500,
    position: "relative",
    top: "-3px",
  },
  listItem: {
    textAlign: "left",
    font: "normal normal normal 13px/16px Lato",
    letterSpacing: 0,
    border: "1px solid #dddddd",
    verticalAlign: "middle",
    width: "66px",
    height: "100%",
    fontWeight: 500,
    padding: "9px",
    textAlign: "center",
    borderRight: "none",
    cursor: "pointer",
  },
  mainContainer: {
    listStyleType: "none",
    display: "flex",
    padding: 0,
    position: "relative",
    "& li:last-child": {
      borderRight: "1px solid #dddddd",
      borderRadius: "0px 6px 6px 0px",
    },
    "& li:first-child": {
      borderRadius: "6px 0px 0px 6px",
    },
  },
  textField: {
    height: "33px",
  },
  buttonContainer: {
    width: "47%",
    border: "none",
    paddingTop: "2px",
  },
  buttonDropdown: {
    width: "45%",
    background: "#FFFFFF 0% 0% no-repeat padding-box",
    border: "1px solid #DDDDDD",
    borderRadius: 6,
    display: "block",
    height: "36px",
    textAlign: "initial",
    fontSize: "12px !important",
  },
  dropdown: {
    position: "relative",
    top: 5,
    right: 0,
    left: 0,
    zIndex: 1,
    border: "1px solid",
    padding: 10,
    backgroundColor: "white",
  },
  cstButton: {
    display: "block",
    border: "1px solid #dddd",
    width: "100%",
    height: "36px",
    borderRadius: "6px",
    textAlign: "start",
    fontSize: "20px !important",
  },
  iconVal: {
    position: "relative",
    display: "block",
    top: "-2px",
    left: "4px",
  },
  customDropDownListItem: {
    display: "flex",
    position: "absolute",
    alignItems: "center",
    padding: "16px",
    top: "-.8rem",
  },
  cstDropdown: {
    width: "50%",
    zIndex: 5
  },
  active: {
    border: "0.2px solid rgb(0, 144, 255)",
    color: "rgb(0, 144, 255)",
  },

  toggleContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
    marginBottom: 10
  },

  toggleBtnGroup: {
    display: "flex",
    flexWrap: "nowrap",
    borderRadius: 6,
    height: 36,
    background: theme.palette.common.white,
    boxShadow: "none",
    "& $toggleButtonSelected": {
      color: "rgb(0, 144, 255)",
      backgroundColor: "rgb(0 144 255 / 10%)",
      border: "1px solid rgb(0, 144, 255)",
      borderRight: "1px solid rgb(0, 144, 255) !important",
      "&:after": {
        backgroundColor: "rgb(0 144 255 / 10%)",
      },
      "&:hover": {
        backgroundColor: "rgb(0 144 255 / 10%)",
      },
      "& $toggleButton":{
        borderRight: "none",
        borderLeft: "none"
      }
    },
  },
  toggleButton: {
    height: "auto",
    padding: "4px 25px 5px",
    fontSize: "12px !important",
    fontWeight: theme.palette.fontWeightMedium,
    textTransform: "capitalize",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    "&:hover": {
      background: theme.palette.common.white,
    },
    "&[value = '1']": {
      borderRight: "none",
      borderTopLeftRadius: 6,
      borderBottomLeftRadius: 6,
    },
    "&[value = '2']": {
      borderRight: "none",
    },
    "&[value = '3']": {
      borderRight: "none",
    },
    "&[value = '4']": {
      borderRight: "none",
    },
    "&[value = '5']": {
      borderTopRightRadius: 6,
      borderBottomRightRadius: 6,
    },
  },
  toggleButtonSelected: {},
});

export default ratingFieldStyle;
