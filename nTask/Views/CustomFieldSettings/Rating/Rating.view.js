import React, { useContext, useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import "emoji-mart/css/emoji-mart.css";
import { Picker } from "emoji-mart";
import ListItem from "@material-ui/core/ListItem";
import Grid from "@material-ui/core/Grid";
import CustomFieldsContext from "../Context/customFields.context";
import ratingFieldStyle from "./Rating.style";
import DropdownMenu from "../../../components/Dropdown/DropdownMenu";
import CustomButton from "../../../components/Buttons/CustomButton";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import { dispatchSettings } from "../Context/actions/customFields.actions";

function RatingField({ classes }) {
  const {
    state: { rating },
    dispatch,
  } = useContext(CustomFieldsContext);
  const { settings } = rating;

  const [anchorEl, setAnchorEl] = useState(null);

  const addEmoji = emoji => {
    dispatchSettings(dispatch, { emoji: emoji.native }, "rating");
  };

  const handleClick = event => {
    // Function Opens the dropdown
    event.stopPropagation();
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };
  const handleClose = event => {
    // Function closes dropdown
    setAnchorEl(null);
  };
  const handleChange = (event, scale) => {
    if (scale) dispatchSettings(dispatch, { scale }, "rating");
  };
  const open = Boolean(anchorEl);

  return (
    <Grid container spacing={2}>
      <Grid item xl={6} xs={12} lg={6} md={6}>
        <div className={classes.iconContainer}>
          <label className={classes.unitLabel}>Scale</label>
          <div className={classes.toggleContainer}>
            <ToggleButtonGroup
              value={settings.scale}
              exclusive
              onChange={handleChange}
              classes={{ root: classes.toggleBtnGroup }}>
              <ToggleButton
                value={1}
                classes={{
                  root: classes.toggleButton,
                  selected: classes.toggleButtonSelected,
                }}>
                1{" "}
              </ToggleButton>
              <ToggleButton
                value={2}
                classes={{
                  root: classes.toggleButton,
                  selected: classes.toggleButtonSelected,
                }}>
                2{" "}
              </ToggleButton>
              <ToggleButton
                value={3}
                classes={{
                  root: classes.toggleButton,
                  selected: classes.toggleButtonSelected,
                }}>
                3{" "}
              </ToggleButton>
              <ToggleButton
                value={4}
                classes={{
                  root: classes.toggleButton,
                  selected: classes.toggleButtonSelected,
                }}>
                4
              </ToggleButton>
              <ToggleButton
                value={5}
                classes={{
                  root: classes.toggleButton,
                  selected: classes.toggleButtonSelected,
                }}>
                5
              </ToggleButton>
            </ToggleButtonGroup>
          </div>
        </div>
      </Grid>
      <Grid item xl={6} xs={12} lg={6} md={6}>
        <label className={classes.unitLabel}>Emojis</label>
        <CustomButton
          variant="text"
          btnType="plain"
          onClick={handleClick}
          buttonRef={anchorEl}
          className={classes.cstButton}>
          <span className={classes.iconVal}>{settings.emoji}</span>
        </CustomButton>

        <DropdownMenu
          open={open}
          closeAction={handleClose}
          anchorEl={anchorEl}
          size="small"
          placement="bottom-start"
          disablePortal={true}
          className={classes.cstDropdown}>
          <ListItem className={classes.customDropDownListItem}>
            {
              <Picker
                onSelect={addEmoji}
                exclude={["flags"]}
                emojiTooltip="false"
                set="google"
                i18n={{
                  search: "Search",
                  categories: { search: "Search Results", recent: "Recents" },
                }}
                enableFrequentEmojiSort={true}
                perLine={9}
                title={'Pick..'}
              />
            }
          </ListItem>
        </DropdownMenu>
      </Grid>
    </Grid>
  );
}
export default withStyles(ratingFieldStyle, { withTheme: true })(RatingField);
