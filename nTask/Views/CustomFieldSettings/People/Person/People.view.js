import React, { useContext } from "react";
import Grid from "@material-ui/core/Grid";
import CustomFieldsContext from "../../Context/customFields.context";
import { withStyles } from "@material-ui/core/styles";
import DefaultCheckbox from "../../../../components/Form/Checkbox";
import peopleFieldStyle from "./People.style";
import { dispatchSettings } from "../../Context/actions/customFields.actions";

function PeopleField({ classes }) {
  const {
    state: { people },
    dispatch,
  } = useContext(CustomFieldsContext);
  const { settings } = people;

  const handleSelect = (value) => {
    dispatchSettings(dispatch, { multiplePeople: value }, "people");
  };
  return (
    <Grid container spacing={2} style={{ marginLeft: "0rem" }}>
      <DefaultCheckbox
        onChange={()=>handleSelect(!settings.multiplePeople)}
        label={"Select Multiple People / Persons"}
        checkboxStyles={{ padding: "0 8px 0 0" }}
        styles={{
          marginTop: 5,
          marginBottom: 10,
          display: "flex",
          alignItems: "center",
          font: "normal normal normal 13px/16px Lato",
        }}
        checked={settings.multiplePeople}
      />
    </Grid>
  );
}
export default withStyles(peopleFieldStyle, { withTheme: true })(PeopleField);
