import React, { useContext } from "react";
import CustomButton from "../../../components/Buttons/CustomButton";
import DefaultTextField from "../../../components/Form/TextField";
import {
  dispatchDropdownOption,
  dispatchAddDropdownOption,
  dispatchDropdownConfig,
  dispatchDeleteDropdownOption,
  dispatchDropdownColorOption,
} from "../Context/actions/dropdown.actions";
import CustomFieldsContext from "../Context/customFields.context";
import dropdownFieldStyles from "./DropdownField.style";
import { withStyles } from "@material-ui/core/styles";
import Tooltip from "../../../components/Tooltip/Tooltip";
import IconRemove from "../../../components/Icons/IconRemove";
import SvgIcon from "@material-ui/core/SvgIcon";
import AddIcon from "@material-ui/icons/Add";
import ColorPickerDropdown from "../../../components/Dropdown/ColorPicker/Dropdown";

function DropdownField({ options, classes, theme }) {
  const {
    state: { dropdown, errors },
    dispatch,
  } = useContext(CustomFieldsContext);

  //Options input handle Change function
  const handleOptionChange = (e, index) => {
    const value = e.target.value;
    const obj = { value, index };
    dispatchDropdownOption(dispatch, obj);
  };
  //Add new dropdown option
  const handleAddNewOption = () => {
    dispatchAddDropdownOption(dispatch);
  };

  //
  const handleDeleteOption = (e, i) => {
    dispatchDeleteDropdownOption(dispatch, i);
  };
  const handleSelectColor = (color, index) => {
    const obj = { color, index };
    dispatchDropdownColorOption(dispatch, obj);
  };
  const optionsErrors = errors.fieldType === 'dropdown' && errors.errors.reduce((r, cv) => {
    r[cv.split(" ")[0]] = cv.replace(cv.split(" ")[0] + " ", "")
    return r
  }, {});
  return (
    <div>
      {dropdown.options.map((o, i) => {
        return (
          <div className={classes.optionInputCnt}>
            <DefaultTextField
              label={`Option ${i + 1} (Required)`}
              errorState={optionsErrors && !o.value && optionsErrors[`options[${i}].value`] ? true : false}
              errorMessage={optionsErrors && !o.value && optionsErrors[`options[${i}].value`]}
              formControlStyles={{ marginRight: 15 }}
              defaultProps={{
                value: o.value,
                autoFocus: true,
                onChange: e => handleOptionChange(e, i),
                onKeyDown: e => {
                  if(e.keyCode === 13) handleAddNewOption(); /** if user press enter after filling field */
                },
                id: `optioninput${o.value}`,
                placeholder: `Enter Option ${i + 1}`,
                inputProps: { maxLength: 80 },
              }}
            />
            <ColorPickerDropdown
              theme={theme}
              selectedColor={o.color || ""}
              onSelect={color => {
                handleSelectColor(color, i);
              }}
            />
            {dropdown.options.length > 1 ? (
              <Tooltip helptext="Remove option" iconType="help" placement="top">
                <SvgIcon
                  onClick={e => handleDeleteOption(e, i)}
                  className={classes.deleteIcon}
                  viewBox="0 0 18 18">
                  <IconRemove />
                </SvgIcon>
              </Tooltip>
            ) : null}
          </div>
        );
      })}

      <div className={classes.btnsCnt}>
        <span className={classes.addAnotherOptBtn} onClick={handleAddNewOption}>
          <AddIcon className={classes.addIcon} /> Add Another Option
        </span>
      </div>
    </div>
  );
}

export default withStyles(dropdownFieldStyles, { withTheme: true })(DropdownField);
