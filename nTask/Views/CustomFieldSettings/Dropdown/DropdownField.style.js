const dropdownFieldStyles = (theme) => ({
    btnsCnt: {
        // display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    addAnotherOptBtn:{
        fontSize: "13px !important",
        fontFamily: theme.typography.fontFamilyLato,
        fontWeight: 400,
        color: "#0090ff",
        textDecoration: "underline",
        cursor: "pointer"
    },
    addIcon:{
        fontSize: "18px !important",
        marginBottom: -4
    },
    optionInputCnt: {
        display: 'flex',
        alignItems: 'center'
    },
    deleteIcon:{
        fontSize: "19px !important",
        marginLeft: 15,
        cursor: "pointer"
    }
})

export default dropdownFieldStyles;