import { optionsSchema } from "../../utils/validator/customFields/customFields";
import * as yup from "yup";

export const dropdownFieldSchema = yup.object().shape({
  options: yup.array().of(
    yup.object().shape({
      value: yup
        .string()
        .required('${path} Field required')
    }).required()
  ),
});
export const numberFieldSchema = yup.object().shape({
  options: yup.array().of(
    yup.object().shape({
      value: yup
        .string()
        .required('${path} Field required')
    }).required()
  ),
});

export const matrixFieldSchema = yup.object().shape({
  rowTitle: yup.string().required('Rows (Y-Axis) is required'),
  columnTitle: yup.string().required('Columns (X-Axis) is required'),
});
