const numberFieldStyles = theme => ({
  btnsCnt: {
    // display: 'flex',
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 10,
  },
  cstBorder: {
    borderRadius: "6px 0px 0px 6px",
  },
  cstBorderInp: {
    padding: "0 !important",
    borderRadius: "0px 6px 6px 0px",
    height: "32px !important",
  },
  iconHandle:{
    position:"relative",
    top:"-2px"
  },
  active: {
    border: "1px solid rgb(0, 144, 255)",
    color: "rgb(0, 144, 255)",
    textAlign: "left",
    font: "normal normal normal 13px/16px Lato",
    letterSpacing: 0,
    border: " 1px solid #0090ff",
    verticalAlign: "middle",
    height: "32px",
    padding: "9px",
    fontWeight: 500,
    backgroundColor: "rgb(0 144 255 / 10%)",
    cursor: "pointer",
  },
  addAnotherOptBtn: {
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 400,
    color: "#0090ff",
    textDecoration: "underline",
    cursor: "pointer",
  },
  ToggleButtonSelected: {
    border: "1px solid rgb(0, 144, 255)",
    borderRadius: "6px 0px 0px 6px",
    backgroundColor: "#fff",
  },
  addIcon: {
    fontSize: "18px !important",
    marginBottom: -4,
  },
  optionInputCnt: {
    display: "flex",
    alignItems: "center",
  },
  deleteIcon: {
    fontSize: "19px !important",
    marginLeft: 15,
    cursor: "pointer",
  },
  mainContainer: {
    listStyleType: "none",
    display: "flex",
    padding: 0,
    position: "relative",
  },
  MuiTypography: {
    color: "red",
  },
  unitLabel: {
    font: "normal normal normal 13px/16px Lato",
    color: "#171717",
    fontWeight: 500,
    position: "relative",
    top: "-3px",
  },
  listItem: {
    textAlign: "left",
    font: "normal normal normal 13px/16px Lato",
    letterSpacing: 0,
    border: " 1px solid #dddddd",
    verticalAlign: "middle",
    height: "100%",
    borderRight: "none",
    padding: "9px",
    fontWeight: 500,
    cursor: "pointer",
    position: "relative",
    top: "-0.3px",
    height: 32
  },
  textField: {
    height: "36px",
    padding: "0 !important",
    borderRadius: "0px 6px 6px 0px",
  },
  buttonContainer: {
    width: "47%",
    border: "none",
    paddingTop: "2px",
  },
  buttonDropdown: {
    width: "45%",
    background: "#FFFFFF 0% 0% no-repeat padding-box",
    border: "1px solid #DDDDDD",
    borderRadius: 6,
    display: "block",
    height: "36px",
    textAlign: "initial",
    fontSize: "12px !important",
  },
  dropdown: {
    position: "relative",
    top: 5,
    right: 0,
    left: 0,
    zIndex: 1,
    border: "1px solid",
    padding: 10,
    backgroundColor: "white",
  },

  toggleContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
  },

  toggleBtnGroup: {
    display: "flex",
    flexWrap: "nowrap",
    height: 32,
    background: theme.palette.common.white,
    // border: `1px solid ${theme.palette.border.lightBorder}`,
    boxShadow: "none",
    "& $toggleButtonSelected": {
      color: "rgb(0, 144, 255)",
      backgroundColor: "rgb(0 144 255 / 10%)",
      border: "1px solid rgb(0, 144, 255)",
      borderRight: "1px solid rgb(0, 144, 255) !important",
      borderLeft: "1px solid rgb(0, 144, 255) !important",
      "&:after": {
        backgroundColor: "rgb(0 144 255 / 10%)",
      },
      "&:hover": {
        backgroundColor: "rgb(0 144 255 / 10%)",
      },
    },
  },
  toggleButton: {
    height: "auto",
    padding: "4px 11px 5px",
    fontSize: "12px !important",
    fontWeight: theme.palette.fontWeightMedium,
    textTransform: "capitalize",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    "&:hover": {
      // background: theme.palette.common.white,
    },
    "&[value = 'left']": {
      borderTopLeftRadius: 6,
      borderBottomLeftRadius: 6,
      borderRight : "none"
    },
    "&[value = 'right']": {
      borderTopRightRadius: 6,
      borderBottomRightRadius: 6,
      borderLeft : "none"
    },
  },
  toggleButtonSelected: {},
  notchedOutlineCnt:{
    borderRadius: "0px 6px 6px 0px"
  }
});

export default numberFieldStyles;
