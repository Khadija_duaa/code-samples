import React, { useContext, useState } from "react";
import DefaultTextField from "../../../components/Form/TextField";
import Grid from "@material-ui/core/Grid";
import CustomFieldsContext from "../Context/customFields.context";
import numberFieldStyles from "./Number.style";
import { withStyles } from "@material-ui/core/styles";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import DefaultCheckbox from "../../../components/Form/Checkbox";
import SelectSearchDropdown from "../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { dispatchSettings } from "../Context/actions/customFields.actions";
import { generateIntergers } from "../../../helper/generateSelectData";

function NumberField({ classes, theme }) {
  const {
    state: { number },
    dispatch,
  } = useContext(CustomFieldsContext);
  const { settings } = number;

  const handleOptionChange = e => {
    dispatchSettings(dispatch, { unit: e.target.value }, "number");
  };

  const handleInputFocus = () => {
    dispatchSettings(dispatch, { unit: "" }, "number");
  };

  const selectUnit = unit => {
    if (unit === "%") {
      dispatchSettings(dispatch, { unit, direction: "right" }, "number");
    } else dispatchSettings(dispatch, { unit }, "number");
  };

  const handleChange = (event, direction) => {
    if (direction) dispatchSettings(dispatch, { direction }, "number");
  };

  const handleSelectComma = () => {
    dispatchSettings(dispatch, { useComma: !settings.useComma }, "number");
  };

  const handleSelectNegativeValue = () => {
    dispatchSettings(dispatch, { negativeValue: !settings.negativeValue }, "number");
  };

  const handleSelect = (type, option) => {
    dispatchSettings(dispatch, { decimalPlace: option.value }, "number");
  };

  let inputValue =
    settings.unit !== "none" &&
    settings.unit !== "$" &&
    settings.unit !== "€" &&
    settings.unit !== "£" &&
    settings.unit !== "%"
      ? true
      : false;
  let selectedDecimalValue = generateIntergers().filter(int => int.value == settings.decimalPlace);
  return (
    <div className={classes.numberHandling}>
      <Grid container spacing={2}>
        <Grid item xl={6} xs={6} lg={6} md={6}>
          <div className={classes.iconContainer}>
            <label className={classes.unitLabel}>Unit</label>
            <ul className={classes.mainContainer}>
              <li
                className={
                  settings.unit == "none"
                    ? `${classes.active} ${classes.cstBorder}`
                    : `${classes.listItem} ${classes.cstBorder}`
                }
                onClick={e => {
                  selectUnit("none");
                }}>
                <span className={classes.iconHandle}>None</span>
              </li>
              <li
                className={settings.unit == "$" ? classes.active : classes.listItem}
                onClick={e => {
                  selectUnit("$");
                }}>
                <span className={classes.iconHandle}>$</span>
              </li>
              <li
                className={settings.unit == "€" ? classes.active : classes.listItem}
                onClick={e => {
                  selectUnit("€");
                }}>
                <span className={classes.iconHandle}>€</span>
              </li>
              <li
                className={settings.unit == "£" ? classes.active : classes.listItem}
                onClick={e => {
                  selectUnit("£");
                }}>
                <span className={classes.iconHandle}>£</span>
              </li>
              <li
                className={settings.unit == "%" ? classes.active : classes.listItem}
                onClick={e => {
                  selectUnit("%");
                }}>
                <span className={classes.iconHandle}>%</span>
              </li>
              <li>
                <DefaultTextField
                  defaultProps={{
                    onChange: e => handleOptionChange(e),
                    onFocus: () => handleInputFocus(),
                    id: "numberSymbol",
                    placeholder: "Type your own",
                    inputProps: { maxLength: 15, style: { padding: "10px 6px", height: "12px" } },
                    value: inputValue ? settings.unit : "",
                    autoComplete: "off",
                  }}
                  classes={classes}
                />
              </li>
            </ul>
          </div>
        </Grid>
        <Grid item xl={6} xs={6} lg={6} md={6}>
          <lable className={classes.unitLabel}>Direction</lable>
          <div className={classes.toggleContainer}>
            <ToggleButtonGroup
              value={settings.direction}
              exclusive
              onChange={handleChange}
              classes={{ root: classes.toggleBtnGroup }}>
              <ToggleButton
                value={"left"}
                disabled={settings.unit === "%"}
                classes={{
                  root: classes.toggleButton,
                  selected: classes.toggleButtonSelected,
                }}>
                LEFT
              </ToggleButton>
              <ToggleButton
                value={"right"}
                classes={{
                  root: classes.toggleButton,
                  selected: classes.toggleButtonSelected,
                }}>
                RIGHT
              </ToggleButton>
            </ToggleButtonGroup>
          </div>
        </Grid>
      </Grid>
      <Grid container spacing={2}>
        <Grid item xl={6} xs={6} lg={6} md={6}>
          <DefaultCheckbox
            onChange={handleSelectComma}
            label={"Use Comma as Thousand Separator"}
            checkboxStyles={{ padding: "0 6px 0 0" }}
            styles={{
              alignItems: "center",
              marginBottom: 7
            }}
            checked={settings.useComma}
            labelStyle={{
              fontFamily: theme.typography.fontFamilyLato,
              color:"#171717"
            }}
          />
          {/* <DefaultCheckbox
            onChange={handleSelectNegativeValue}
            label={"Allow Negative Values"}
            checkboxStyles={{ padding: "0 6px 0 0" }}
            styles={{
              alignItems: "center",
            }}
            checked={settings.negativeValue}
            labelStyle={{
              fontFamily: theme.typography.fontFamilyLato,
              color:"#171717"
            }}
          /> */}
        </Grid>

        <Grid item xl={6} xs={6} lg={6} md={6}>
          <SelectSearchDropdown
            data={generateIntergers}
            label={"Decimal Places"}
            placeholder={"Select"}
            isMulti={false}
            selectChange={(type, option) => {
              handleSelect(type, option);
            }}
            selectedValue={selectedDecimalValue}
            // isDisabled={!settings.useComma}
            styles={{ marginBottom: 10, width: 110, }}
          />
        </Grid>
      </Grid>
    </div>
  );
}
export default withStyles(numberFieldStyles, { withTheme: true })(NumberField);
