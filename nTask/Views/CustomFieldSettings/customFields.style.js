const customFieldStyles = theme => ({
  dialogContentCnt: {
    padding: "20px 30px",
  },
  searchInput: {
    // padding: "4px 8px 5px 8px",
    width: "100%",
    outline: "none",
    border: "none",
    fontSize: "14px !important",
    // lineHeight: "18px",
    fontWeight: theme.typography.fontWeightMedium,
    fontFamily: "Lato, sans-serif",
    borderRadius: 0,
    cursor: "pointer",
    height: 35,
  },
  searchIcon: {
    fontSize: "14px !important",
  },
  addOptionCnt: {
    display: "flex",
    alignItems: "center",
  },
  addFieldButton: {
    // background: theme.palette.background.paper,
    padding: "0px 5px",
    // border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: 4,
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
    // marginTop: 25,
    color: "white",
    background: "#0090ff",
    height: 35,
    width: 120,
    "&:hover": {
      // background: theme.palette.background.light
      // background: theme.palette.background.paper,
      // color: theme.palette.icon.brightBlue
    },
  },
  btnTxt: {
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: theme.typography.fontWeightExtraLight,
  },
  addIcon: {
    fontSize: "16px !important",
    marginRight: 6,
    borderRadius: "50%",
    color: 'white',
    backgroundColor: theme.palette.background.blue,
  },
  btnStyles: {
    fontFamily: theme.typography.fontFamilyLato,
    fontSize: "13px !important",
    fontWeight: theme.typography.fontWeightExtraLight,
    cursor: "pointer",
    color:"#505050",
    textTransform: "none",
    "&:hover": {
      backgroundColor: "#F6F6F6",
    },
  },
});

export default customFieldStyles;
