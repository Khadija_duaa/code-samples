const sectionFieldStyle = (theme) => ({
    sectionContainer: {
        display: "flex",
        height: "36px",
        width: "36px",
        margin: "5px",
        borderRadius: "4px",
        justifyContent: "center",
        alignItems: "center"
    },
    unitLabel: {
        font: "normal normal normal 13px/16px Lato",
        display: "block"
    },
    boxHandling: {
        display: "flex",
        width: "100%",
        marginLeft: "5px"
    },
    checkIcon: {
        fill: "#fff",
    },
    fieldDetailsCnt: {
        // width: 556,
        marginLeft: -4, 
        marginBottom: 10
      }
})

export default sectionFieldStyle;