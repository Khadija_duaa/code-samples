import React, { useContext, useState } from "react";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import sectionFieldStyle from "./Section.style";
import CheckIcon from "@material-ui/icons/Check";
import CustomFieldsContext from "../Context/customFields.context";
import { dispatchSettings } from "../Context/actions/customFields.actions";

const colors = [
  "#89A3B2", //set default selected at least one color
  "#6A7AA1",
  "#6886C5",
  "#5EAAA8",
  "#9E7777",
  "#E59881",
  "#DB996C",
  "#AC8DAF",
  "#949CDF",
  "#F38BA0",
  "#986D8E",
  "#E36387",
];

function SectionField({ classes }) {
  const {
    state: { section },
    dispatch,
  } = useContext(CustomFieldsContext);
  const { settings } = section;

  const handleSelect = color => {
    dispatchSettings(dispatch, { color }, "section");
  };

  return (
      <Grid container spacing={2} className={classes.fieldDetailsCnt}>
        <div className={classes.boxHandling}>
          <label className={classes.unitLabel}>Colors</label>
        </div>
        {colors.map(clr => {
          return (
            <span
              style={{ backgroundColor: clr }}
              className={classes.sectionContainer}
              onClick={e => handleSelect(clr)}>
              {clr == settings.color && <CheckIcon className={classes.checkIcon} />}
            </span>
          );
        })}
      </Grid>
  );
}
export default withStyles(sectionFieldStyle, { withTheme: true })(SectionField);
