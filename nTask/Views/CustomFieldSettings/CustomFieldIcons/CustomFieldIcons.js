import React from "react";
import customIconsStyles from "./customFieldIcons.style";
import { withStyles } from "@material-ui/core/styles";
import SvgIcon from "@material-ui/core/SvgIcon";
import TextFieldIcon from "../../../components/Icons/IconTextfield";
import IconTextarea from "../../../components/Icons/IconTextarea";
import IconDate from "../../../components/Icons/IconDate";
import IconPhone from "../../../components/Icons/IconPhone";
import IconEmail from "../../../components/Icons/IconEmail";
import IconWebUrl from "../../../components/Icons/IconWebUrl";
import IconNumber from "../../../components/Icons/IconNumber";
import IconMoney from "../../../components/Icons/IconMoney";
import IconReminder from "../../../components/Icons/IconReminder";
import IconLocation from "../../../components/Icons/IconLocation";
import IconDropdown from "../../../components/Icons/IconDropdown";
import IconChecklist from "../../../components/Icons/IconChecklist";
import IconMatrix from "../../../components/Icons/IconMatrix";
import IconSection from "../../../components/Icons/IconSection";
import IconPeople from "../../../components/Icons/IconPeople";
import IconRating from "../../../components/Icons/IconRating";
import IconAttachment from "../../../components/Icons/IconAttachment";
import IconFormula from "../../../components/Icons/IconFormula";
import IconCountry from "../../../components/Icons/IconCountry";
import IconProgress from "../../../components/Icons/IconProgress";
import IconDuration from "../../../components/Icons/IconDuration";
function CustomFieldIcon({ value, classes, color, customProps }) {
  /** get icons on the basis of field type */
  switch (value) {
    case "dropdown":
      return (
        <div className={classes.iconCntdd} {...customProps}>
          <SvgIcon viewBox="0 0 18 18" fontSize="inherit" className={classes.cstStyling} >
            <IconDropdown color={color} />
          </SvgIcon>
        </div>
      );
      break;
    case "textfield":
      return (
        <div className={classes.iconCntdd} {...customProps}>
          <SvgIcon viewBox="0 0 20 12.969" fontSize="inherit" className={classes.cstStyling}>
            <TextFieldIcon color={color} />
          </SvgIcon>
        </div>
      );
      break;
    case "textarea":
      return (
        <div className={classes.iconCntdd} {...customProps}>
          <SvgIcon viewBox="0 0 18 18" fontSize="inherit" className={classes.cstStyling}>
            <IconTextarea color={color} />
          </SvgIcon>
        </div>
      );
    case "checklist":
      return (
        <div className={classes.iconCntdd} {...customProps}>
          <SvgIcon viewBox="0 0 14 12.703" fontSize="inherit" className={classes.cstStyling}>
            <IconChecklist color={color} />
          </SvgIcon>
        </div>
      );
      break;
    case "matrix":
      return (
        <div className={classes.iconCntdd} {...customProps}>
          <SvgIcon viewBox="0 0 16 16.511" fontSize="inherit" className={classes.cstStyling}>
            <IconMatrix color={color} />
          </SvgIcon>
        </div>
      );
      break;
    case "progress":
      return (
        <div className={classes.iconCntdd} {...customProps}>
          <SvgIcon viewBox="0 0 22 10.522" fontSize="inherit" className={classes.cstStyling}>
            <IconProgress color={color} />
          </SvgIcon>
        </div>
      );
      break;
    case "duration":
      return (
        <div className={classes.iconCntdd} {...customProps}>
          <SvgIcon viewBox="0 0 18 18" fontSize="inherit" className={classes.cstStyling}>
            <IconDuration color={color} />
          </SvgIcon>
        </div>
      );
      break;
    case "datefield":
      return (
        <div className={classes.iconCntdd} {...customProps}>
          <SvgIcon viewBox="0 0 16 17.778" fontSize="inherit" className={classes.cstStyling}>
            <IconDate color={color} />
          </SvgIcon>
        </div>
      );
      break;
    case "email":
      return (
        <div className={classes.iconCntdd} {...customProps}>
          <SvgIcon viewBox="0 0 18 14.4" fontSize="inherit" className={classes.cstStyling}>
            <IconEmail color={color} />
          </SvgIcon>
        </div>
      );
      break;
    case "phone":
      return (
        <div className={classes.iconCntdd} {...customProps}>
          <SvgIcon viewBox="0 0 16 17.778" fontSize="inherit" className={`${classes.cstStyling} ${classes.phone}`}>
            <IconPhone color={color} />
          </SvgIcon>
        </div>
      );
      break;
    case "websiteurl":
      return (
        <div className={classes.iconCntdd} {...customProps}>
          <SvgIcon viewBox="0 0 20.209 11" fontSize="inherit" className={classes.cstStyling}>
            <IconWebUrl color={color} />
          </SvgIcon>
        </div>
      );
      break;
    case "number":
      return (
        <div className={classes.iconCntdd} {...customProps}>
          <SvgIcon viewBox="0 0 16 17.778" fontSize="inherit" className={classes.cstStyling}>
            <IconNumber color={color} />
          </SvgIcon>
        </div>
      );
      break;
    case "money":
      return (
        <div className={classes.iconCntdd} {...customProps}>
          <SvgIcon viewBox="0 0 10 17.485" fontSize="inherit" className={classes.cstStyling}>
            <IconMoney color={color} />
          </SvgIcon>
        </div>
      );
      break;
    case "reminder":
      return (
        <div className={classes.iconCntdd} {...customProps}>
          <SvgIcon viewBox="0 0 19.802 19.962" fontSize="inherit" className={classes.cstStyling}>
            <IconReminder color={color} />
          </SvgIcon>
        </div>
      );
      break;
    case "location":
      return (
        <div className={classes.iconCntdd} {...customProps}>
          <SvgIcon viewBox="0 0 15 17.551" fontSize="inherit" className={classes.cstStyling}>
            <IconLocation color={color} />
          </SvgIcon>
        </div>
      );
      break;
    case "people":
      return (
        <div className={classes.iconCntdd} {...customProps}>
          <SvgIcon viewBox="0 0 16 14.4" fontSize="inherit" className={classes.cstStyling}>
            <IconPeople color={color} />
          </SvgIcon>
        </div>
      );
      break;
    case "rating":
      return (
        <div className={classes.iconCntdd} {...customProps}>
          <SvgIcon viewBox="0 0 17.219 16.517" fontSize="inherit" className={classes.cstStyling}>
            <IconRating color={color} />
          </SvgIcon>
        </div>
      );
      break;
    case "section":
      return (
        <div className={classes.iconCntdd} {...customProps}>
          <SvgIcon viewBox="0 0 18 17.5" fontSize="inherit" className={classes.cstStyling}>
            <IconSection color={color} />
          </SvgIcon>
        </div>
      );
      break;
    case "date":
      return (
        <div className={classes.iconCntdd} {...customProps}>
          <SvgIcon viewBox="0 0 16 17.778" fontSize="inherit" className={classes.cstStyling}>
            <IconDate color={color} />
          </SvgIcon>
        </div>
      );
      break;
    case "filesAndMedia":
      return (
        <div className={classes.iconCntdd} {...customProps}>
          <SvgIcon viewBox="0 0 18 17.5" fontSize="inherit" className={classes.cstStyling}>
            <IconAttachment color={color} />
          </SvgIcon>
        </div>
      );
      break;
    case "formula":
      return (
        <div className={classes.iconCntdd} {...customProps}>
          <SvgIcon viewBox="0 0 18 16.671" fontSize="inherit" className={classes.cstStyling}>
            <IconFormula color={color} />
          </SvgIcon>
        </div>
      );
      break;
    case "country":
      return (
        <div className={classes.iconCntdd} {...customProps}>
          <SvgIcon viewBox="0 0 18 18" fontSize="inherit" className={classes.cstStyling}>
            <IconCountry color={color} />
          </SvgIcon>
        </div>
      );
      break;
    default:
      return <> </>;
      break;
  }
}

export default withStyles(customIconsStyles, { withTheme: true })(CustomFieldIcon);
