const customIconsStyles = theme => ({
  iconCntdd: {
    display: "flex",
    marginRight: "9px",
  },
  iconCntChecklist: {
    marginRight: "5px",
  },
  iconCntTextField: {
    marginRight: "7px",
  },
  cstFieldStyling:{
    height:"16px",
    width:"16px"
  },
  cstStyling:{
    height:"16px",
    width:"16px"
  },
  phone:{
    color: "#999"
  }
});

export default customIconsStyles;
