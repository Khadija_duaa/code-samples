const existingFieldStyles = theme => ({
  dialogContentCnt: {
    padding: "20px 30px",
  },
  searchInput: {
    // padding: "4px 8px 5px 8px",
    width: "100%",
    outline: "none",
    border: "none",
    fontSize: "14px !important",
    // lineHeight: "18px",
    fontWeight: theme.typography.fontWeightMedium,
    fontFamily: "Lato, sans-serif",
    borderRadius: 0,
    cursor: "pointer",
    height: 35,
  },
  searchIcon: {
    fontSize: "14px !important",
  },
  btnsCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  existingFieldCnt: {
    // margin: "10px 0px",
    cursor: "pointer",
    padding: "10px 0",
    color: "#333",
    "&:hover": {
      backgroundColor: `#fbfbfb`,
      color: theme.palette.border.blue,
    },
  },
  existingFieldChildCnt: {
    // margin: "10px 0px",
    cursor: "pointer",
    padding: "10px 0",
    color: "#333",
    paddingLeft: 21,
    "&:hover": {
      backgroundColor: `#fbfbfb`,
      color: theme.palette.border.blue,
    },
    "&:hover $defaultChipsTag": {
      visibility: "visible",
    },
  },
  existingFieldTitle: {
    fontWeight: theme.typography.fontWeightMedium,
    // fontFamily: "Lato, sans-serif",
    fontSize: "14px !important",
    textTransform: "capitalize",
  },
  existingFieldChildTitle: {
    // fontWeight: theme.typography.fontWeightMedium,
    // fontFamily: "Lato, sans-serif",
    fontSize: "13px !important",
    textTransform: "capitalize",

    width: "250px",
    // whiteSpace: "nowrap",
    // overflow: "hidden",
    // textOverflow: "ellipsis",
    wordBreak: "break-word",
  },
  existingFieldBaseCnt: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "start",
    alignItems: "center",
  },
  iconFields: {
    fontSize: "16px !important",
    marginRight: 5,
  },
  headCnt: {
    marginTop: 10,
  },
  defaultChipsTag: {
    background: theme.palette.background.btnBlue,
    padding: "3px 6px",
    color: theme.palette.common.white,
    fontSize: "12px !important",
    lineHeight: "normal",
    borderRadius: 4,
    cursor: "pointer",
    visibility: "hidden",
    marginLeft: 10,
    textTransform: "initial",
  },
  emptyFieldCnt: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    padding: "16px",
    fontSize: "14px !important",
    border: "1px dotted #bfbfbf"
  },
});

export default existingFieldStyles;
