import React, { useState, useContext, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import existingFieldStyles from "./existingFields.style";
import CustomDialog from "../../../components/Dialog/CustomDialog";
import CustomFieldsContext from "../Context/customFields.context";
import DefaultTextField from "../../../components/Form/TextField";
import Grid from "@material-ui/core/Grid";
import ButtonActionsCnt from "../../../components/Dialog/ConfirmationDialogs/ButtonActionsCnt";
import { dispatchSetViewType } from "../Context/actions/dropdown.actions";
import SearchTextIcon from "../../../components/Icons/SearchTextIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import InputAdornment from "@material-ui/core/InputAdornment";
import DropDownIcon from "@material-ui/icons/ArrowDropDownCircleOutlined";
import CheckListIcon from "@material-ui/icons/CheckCircleOutlineOutlined";
import { useExistingCustomField } from "../../../redux/actions/customFields";

function ExistingFieldView({
  classes,
  theme,
  customFields,
  profile,
  feature,
  useExistingCustomField,
}) {
  const [searchTerm, setSearchTerm] = useState("");
  const {
    state: { viewType },
    dispatch,
  } = useContext(CustomFieldsContext);

  const [fields, setFields] = useState([]);

  //Dialog Close and returing back to first state
  const closeDialog = () => {
    dispatchSetViewType(dispatch, "");
  };

  const handleActionBackClick = () => {
    dispatchSetViewType(dispatch, "newField");
  };
  const searchUpdated = e => {
    setSearchTerm(e.target.value);
  };
  const renderIcon = e => {
    switch (e.fieldType) {
      case "dropdown":
        return <DropDownIcon className={classes.iconFields} />;
        break;
      case "checklist":
        return <CheckListIcon className={classes.iconFields} />;
        break;
      default:
        return <DropDownIcon className={classes.iconFields} />;
        break;
    }
  };

  const renderCustomFields = param => {
    let arr = param.map(e => {
      e.collapse = false;
      return e;
    });
    setFields(arr);
  };

  const handleClickExistingField = ele => {
    let arr = fields.map(e => {
      if (e.fieldType === ele.fieldType) e.collapse = !e.collapse;
      return e;
    });
    setFields(arr);
  };
  const filterFields = a => {
    const dropdowns = a.reduce((result, cv) => {
      const existingFields = cv.workspaces.length && cv.workspaces.every(
        wf =>
          wf.workspaceId !== profile.loggedInTeam ||
          !wf.groupType.includes(feature)
      );
      if (existingFields || (cv.team && !cv.team.groupType.includes(feature)))

        result.push(cv);
      return result;
    }, []);
    return dropdowns;
  };

  const handleClickUseField = param => {
    let obj = {
      fieldId: param.fieldId,
      workspaces: [{ workspaceId: profile.loggedInTeam, groupType: [feature] }],
    };
    useExistingCustomField(
      obj,
      succ => {
        closeDialog();
      },
      fail => {}
    );
  };

  useEffect(() => {
    renderCustomFields(customFields.groupCustomFields);
  }, [customFields.groupCustomFields]);

  return (
    <>
      <CustomDialog
        title="Existing Fields"
        dialogProps={{
          open: viewType === "existingField",
          onClose: closeDialog,
          PaperProps: {
            className: classes.dialogPaperCmp,
          },
          disableBackdropClick: true,
          disableEscapeKeyDown: true,
        }}>
        <div className={classes.dialogContentCnt}>
          <Grid container>
            <Grid item xs={12}>
              <DefaultTextField
                error={false}
                errorState={false}
                errorMessage={""}
                formControlStyles={{ marginBottom: 0 }}
                styles={classes.searchInput}
                defaultProps={{
                  type: "text",
                  placeholder: `Search exisiting fields`,
                  value: searchTerm,
                  autoFocus: true,
                  inputProps: { maxLength: 80 },
                  onChange: e => {
                    searchUpdated(e);
                  },
                  onKeyDown: () => {},
                  onBlur: () => {},
                  startAdornment: (
                    <InputAdornment position="start">
                      <SvgIcon
                        viewBox="0 0 14 14"
                        classes={{ root: classes.searchIcon }}
                        htmlColor={theme.palette.text.grayDarker}>
                        <SearchTextIcon />
                      </SvgIcon>
                    </InputAdornment>
                  ),
                }}
              />
            </Grid>
            <Grid item xs={12} classes={{ item: classes.headCnt }}>
              {fields.length > 0 &&
                fields.map(e => {
                  let filteredFields = filterFields(e.fields);
                  return filteredFields.length > 0 ? (
                    <>
                      <div
                        className={classes.existingFieldCnt}
                        onClick={() => handleClickExistingField(e)}>
                        <div className={classes.existingFieldBaseCnt}>
                          {renderIcon(e)}
                          <span className={classes.existingFieldTitle}>
                            {`${e.fieldType} (${filteredFields.length})`}
                          </span>
                        </div>
                      </div>
                      {e.collapse &&
                        filteredFields.map(f => {
                          return (
                            <div className={classes.existingFieldChildCnt}>
                              <span className={classes.existingFieldChildTitle} title={f.fieldName}>
                                {f.fieldName}
                                <span
                                  className={classes.defaultChipsTag}
                                  onClick={() => {
                                    handleClickUseField(f);
                                  }}>
                                  Use this field
                                </span>
                              </span>
                            </div>
                          );
                        })}
                    </>
                  ) : (
                    <div className={classes.emptyFieldCnt}>
                      <span> No Exisiting Fields</span>
                    </div>
                  );
                })}
            </Grid>
          </Grid>
        </div>
        <ButtonActionsCnt
          // cancelAction={() => {
          //   closeDialog();
          // }}
          // successAction={() => {
          //   const obj = { options: options };
          //   let validate = schema.validate(obj, { abortEarly: false }).catch(err => {
          //   });
          // }}
          deleteAction={handleActionBackClick}
          deleteBtnText="Back"
          btnTypeVariant="text"
          // successBtnText="Add Field"
          // cancelBtnText="Cancel"
          //   classes={classes}
          // btnType="success"
          btnTypeDelete="plain"
          //   btnQuery={btnQuery}
        />
      </CustomDialog>
    </>
  );
}

ExistingFieldView.defaultProps = {
  /** default props for the component  */
  classes: {},
  theme: {},
  feature: "",
};

const mapStateToProps = state => {
  return {
    customFields: state.customFields,
    profile: state.profile.data,
  };
};

export default compose(
  withStyles(existingFieldStyles, { withTheme: true }),
  connect(mapStateToProps, { useExistingCustomField })
)(ExistingFieldView);
