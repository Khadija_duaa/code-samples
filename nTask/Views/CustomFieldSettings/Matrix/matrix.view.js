import React, { useContext } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import CustomButton from "../../../components/Buttons/CustomButton";
import DefaultTextField from "../../../components/Form/TextField";
import { updateMatrixConfig } from "../Context/actions/matrix.actions";
import CustomFieldsContext from "../Context/customFields.context";
import CustomRadio from "../../../components/Form/Radio/Radio";
import netRiskStyles from "./matrix.style";
import { withStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import InputAdornment from "@material-ui/core/InputAdornment";
import RemoveIcon2 from "../../../components/Icons/RemoveIcon2";
import SvgIcon from "@material-ui/core/SvgIcon";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import ColorPickerDropdown from "../../../components/Dropdown/ColorPicker/Dropdown";
import cloneDeep from "lodash/cloneDeep";
import MatrixGrid from "./matrixGrid.cmp";
import DefaultCheckbox from "../../../components/Form/Checkbox";
import { Scrollbars } from "react-custom-scrollbars";
import NotificationMessage from "../../../components/NotificationMessages/NotificationMessages";

function Matrix({ options, classes, theme, data, customFields, disableAssessment, profileState }) {
  const {
    dispatch,
    state: { 
      viewMode, 
      errors,
      matrix: { level },
    },
  } = useContext(CustomFieldsContext);

  const handleAxisUpdate = (e, key, axis) => {
    let obj;
    const value = e.target.value;
    switch (key) {
      case "title":
        obj = { [axis]: { ...data.settings[axis], title: value } };
        break;

      default:
        break;
    }
    updateMatrixConfig(dispatch, obj);
  };
  //Generating columns and rows values
  const generateValues = (arr, gridSize, type) => {
    const alb = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    const emptyArr = [...Array(gridSize)];
    if (type === "columns") {
      return emptyArr.map((v, i) => {
        if (arr[i]) return arr[i];
        return { defaultValue: i + 1, customValue: "" };
      });
    }
    return emptyArr.map((v, i) => {
      if (arr[i]) return arr[i];
      return { defaultValue: alb[i], customValue: "" };
    });
  };
  //Update Values of columns
  const handleValuesUpdate = (e, obj, key) => {
    const value = e.target.value;
    const columns = data.settings.columns;
    const rows = data.settings.rows;
    let newArr;
    if (key === "columns") {
      newArr = columns.values.map(c => {
        return c.defaultValue === obj.defaultValue ? { ...obj, customValue: value } : c;
      });
      updateMatrixConfig(dispatch, { columns: { ...columns, values: newArr } });
    } else {
      newArr = rows.values.map(c => {
        return c.defaultValue === obj.defaultValue ? { ...obj, customValue: value } : c;
      });
      updateMatrixConfig(dispatch, { rows: { ...rows, values: newArr } });
    }
  };
  //Function to set grid size
  const handleGridSizeSelect = (e, option) => {
    const columns = data.settings.columns;
    const rows = data.settings.rows;
    const newColumns = generateValues(columns.values, option, "columns");
    const newRows = generateValues(rows.values, option, "rows");
    const matrix = data.settings.matrix.reduce((r, cv, index) => {
      const newArr = cv.map((x, i) => {
        return i > option - 1 || index > option - 1
          ? { ...x, isDisabled: true }
          : { ...x, isDisabled: false };
      });
      r.push(newArr);
      return r;
    }, []);

    // const matrix = newRows.map((x, i) => {
    //   let newArr = [];
    //   for (let r = 0; r < newRows.length; r++) {
    //     newArr.push({
    //       cellName: x.defaultValue + newColumns[r].defaultValue,
    //       rowValue: x.defaultValue,
    //       columnValue: newColumns[r].defaultValue,
    //     });
    //   }
    //   return newArr;
    // });
    const obj = {
      gridSize: option,
      columns: { ...columns, values: newColumns },
      rows: { ...rows, values: newRows },
      matrix,
    };
    
    updateMatrixConfig(dispatch, obj); 
  };
  const handleConfigUpdate = (checkedValue, workspaceId) => {
    const obj={};
    if(checkedValue) obj.assessment = data.settings.assessment.filter(d=> d !== workspaceId);
    else obj.assessment = [...data.settings.assessment, workspaceId];
    updateMatrixConfig(dispatch, obj);
  };
  const defaultMatrix = customFields.data.find(m => m.settings.assessment && m.settings.assessment.indexOf(profileState.loggedInTeam) >= 0 ) || false;
  const matrixErrors = errors.fieldType === "matrix" ? errors.errors : {};
  let checked = data.settings.assessment.includes(profileState.loggedInTeam);
  return (
    <div className={classes.matrixMainCnt}>
      <div className={classes.matrixLeftCnt}>
        <div className={classes.gridSizeCnt}>
          {viewMode !== "edit" ? (
            <>
              <Typography variant="h4" className={classes.valuesHeading}>
                Grid Size
              </Typography>
              <CustomRadio
                options={[2, 3, 4, 5, 6, 7, 8, 9, 10]}
                value={data.settings.gridSize}
                onRadioChange={handleGridSizeSelect}
                iconProps={{ style: { fontSize: "18px" } }}
                radioGroupProps={{
                  style: { display: "flex", alignItems: "center", flexDirection: "row" },
                }}
              />{" "}
            </>
          ) : null}
        </div>
        <Scrollbars autoHide autoHeight autoHeightMin={0} autoHeightMax={400}>
          <div style={{ padding: 16 }}>
            <Grid container spacing={2} style={{ paddingRight: 25 }}>
              <Grid item xs={6}>
                <div className={classes.axisHeadersCnt}>
                  <DefaultTextField
                    label={data.settings.rows.label}
                    errorState={matrixErrors.rowTitle && !data.settings.rows.title ? true : false}
                    errorMessage={!data.settings.rows.title && matrixErrors.rowTitle}
                    defaultProps={{
                      value: data.settings.rows.title,
                      onChange: e => handleAxisUpdate(e, "title", "rows"),
                      placeholder: "Custom value (Optional)",
                      id: data.settings.rows.label,
                      inputProps: { maxLength: 80 },
                    }}
                  />
                  <Typography variant="h4" className={classes.valuesHeading}>
                    Values
                  </Typography>
                  {data.settings.rows.values.map((v, i) => {
                    return (
                      <div className={classes.axisValuesCnt} key={v.defaultValue}>
                        <Typography variant="h4">{v.defaultValue}:</Typography>
                        <DefaultTextField
                          label=""
                        
                          defaultProps={{
                            value: v.customValue,
                            onChange: e => handleValuesUpdate(e, v, "rows"),
                            id: v.defaultValue,
                            inputProps: { maxLength: 80 },
                            autoComplete: "off"
                          }}
                        />
                      </div>
                    );
                  })}
                </div>
              </Grid>
              <Grid item xs={6}>
                <div className={classes.axisHeadersCnt}>
                  <DefaultTextField
                    label={data.settings.columns.label}
                    errorState={
                            matrixErrors.columnTitle && !data.settings.columns.title ? true : false
                          }
                          errorMessage={!data.settings.rows.title && matrixErrors.columnTitle}
                    defaultProps={{
                      value: data.settings.columns.title,
                      onChange: e => handleAxisUpdate(e, "title", "columns"),
                      id: data.settings.columns.label,
                      placeholder: "Custom value (Optional)",
                      inputProps: { maxLength: 80 },
                      autoComplete: "off"
                    }}
                  />
                  <Typography variant="h4" className={classes.valuesHeading}>
                    Values
                  </Typography>
                  {data.settings.columns.values.map((v, i) => {
                    return (
                      <div className={classes.axisValuesCnt} key={v.defaultValue}>
                        <Typography variant="h4">{i + 1}:</Typography>
                        <DefaultTextField
                          label=""
                          defaultProps={{
                            value: v.customValue,
                            onChange: e => handleValuesUpdate(e, v, "columns"),
                            id: v.defaultValue,
                            inputProps: { maxLength: 80 },
                            autoComplete: "off"
                          }}
                        />
                      </div>
                    );
                  })}
                </div>
              </Grid>
              <Grid xs={12}>
               {level !== "All Workspaces" && !disableAssessment && <DefaultCheckbox
                  checked={checked}
                  label="Use matrix as default for assessment"
                  onChange={() => handleConfigUpdate(checked, profileState.loggedInTeam)}
                  styles={{ display: "flex", alignItems: "center", marginTop: 0, marginLeft: 6 }}
                  checkboxStyles={{ padding: "0 5px 0 0" }}
                />}

                {defaultMatrix && (
                  <NotificationMessage
                    type="info"
                    iconType="info"
                    style={{ marginBottom: 20, marginTop: 20 }}>
                    {defaultMatrix.fieldName} is currently default matrix for assessment.
                  </NotificationMessage>
                )}
              </Grid>
            </Grid>
          </div>
        </Scrollbars>
      </div>
      <div className={classes.matrixRightCnt}>
        <MatrixGrid data={data} legend={false} />
      </div>
    </div>
  );
}

const mapStateToProps = state => {
  return {
    customFields: state.customFields,
    profileState : state.profile.data
  };
};
export default compose(
  connect(mapStateToProps, {}),
  withStyles(netRiskStyles, { withTheme: true })
)(Matrix);
