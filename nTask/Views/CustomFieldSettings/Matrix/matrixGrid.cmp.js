import React, { useContext, useState } from "react";
import { updateMatrixConfig } from "../Context/actions/matrix.actions";
import CustomFieldsContext from "../Context/customFields.context";
import netRiskStyles from "./matrix.style";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import ColorPickerDropdown from "../../../components/Dropdown/ColorPicker/Dropdown";
import cloneDeep from "lodash/cloneDeep";
import CustomTooltip from "../../../components/Tooltip/Tooltip";
import isEmpty from "lodash/isEmpty";
import DropdownMenu from "../../../components/Dropdown/DropdownMenu";

function MatrixGrid({
  options,
  classes,
  theme,
  data,
  cellClick,
  colorSelect,
  gridCounts,
  selectionDialog,
  CellDetailsRenderer,
  legend,
}) {
  const { dispatch } = useContext(CustomFieldsContext);
  const [anchorEl, setAnchorEl] = useState(null);
  const [activeCell, setActiveCell] = useState({});
  //Update matrix Obj
  const handleUpdateMatrix = (color, obj, index, parentIndex) => {
    const matrixObjClone = cloneDeep(data.settings.matrix);
    let updatedItems = cloneDeep(data.settings.updatedMatrixItems) || [];
    matrixObjClone[parentIndex][index] = { ...obj, color };
    updatedItems = handleUpdateRow(updatedItems, { ...obj, color });
    updateMatrixConfig(dispatch, { matrix: matrixObjClone, updatedMatrixItems: updatedItems });
  };
  const handleUpdateRow = (columns, item) => {
    if (columns.length) {
      let itemArr = [];
      let isExist = columns.find(c => c.cellName === item.cellName);
      if (isExist) {
        itemArr = columns.map(co => {
          if (co.cellName === item.cellName) return item;
          return co;
        });
      } else itemArr = [...columns, item];
      return itemArr;
    } else {
      return [...columns, item];
    }
  };
  //Get custom value if it's set to show in tooltip
  const getCustomValue = (key, index) => {
    const obj = data.settings[key].values[index];

    return obj && obj.customValue ? obj.customValue : "";
  };

  //handle click to show cell details
  const handleShowCellDetails = (e, obj, data) => {
    setActiveCell(obj);
    setAnchorEl(e.currentTarget);
  };
  //handle click to hide cell details
  const handleHideCellDetails = () => {
    setAnchorEl(null);
  };
  const columns = data.settings.columns.values;
  const rows = data.settings.rows.values;
  return (
    !isEmpty(data) && (
      <div style={{ display: "flex", flex: 1, height: "100%" }}>
        <div className={classes.matrixOuterCnt}>
          {CellDetailsRenderer && gridCounts ? (
            <DropdownMenu
              open={Boolean(anchorEl)}
              closeAction={handleHideCellDetails}
              style={{ minWidth: 180, maxWidth: 450, width: "auto" }}
              anchorEl={anchorEl}
              size={"small"}
              placement="bottom-start">
              <CellDetailsRenderer data={gridCounts[activeCell.cellName]} gridCounts={gridCounts} />
            </DropdownMenu>
          ) : null}

          <Typography variant="h4" classes={{h4: classes.smallHeading}} title={data.settings.columns.title} className={classes.lengendTextTop}>
            {data.settings.columns.title}
          </Typography>
          <Typography variant="h4" classes={{h4: classes.smallHeading}} title={data.settings.rows.title} className={classes.lengendTextLeft}>
         {data.settings.rows.title}
          </Typography>

          <ul
            style={{
              height: 30,
              display: "flex",
              listStyleType: "none",
              padding: 0,
              margin: 0,
              marginLeft: 35,
              textAlign: "center",
            }}>
            {data.settings.matrix[0].map((c, i) => (
              <li style={{ height: 30, flex: 1 }} key={c.columnValue}>
                <CustomTooltip
                  helptext={getCustomValue("columns", c.columnValue - 1) || c.defaultValue}>
                  <Typography variant="h4" classes={{h4: classes.smallHeading}} className={classes.valuesHeading}>
                    {" "}
                    {c.columnValue}
                  </Typography>
                </CustomTooltip>
              </li>
            ))}
          </ul>
          {data.settings.matrix.map((m, pi) => {
            return (
              <div className={classes.matrixListInnerCnt} key={pi}>
                <Typography
                  variant="h4"
                  className={classes.valuesHeading}
                  style={{ display: "flex", alignItems: "center" }}>
                  <CustomTooltip helptext={getCustomValue("rows", pi) || m[0].defaultValue}>
                    <span>{m[0].rowValue}</span>
                  </CustomTooltip>
                </Typography>

                <ul className={classes.matrixCnt}>
                  {m.map((mi, i) => (
                    <li
                      key={mi.cellName}
                      className={`${classes.matrixItem} ${
                        mi.isDisabled ? classes.matrixItemDisabled : classes.matrixItemEnabled
                      }`}
                      onClick={e =>
                        cellClick ? cellClick(mi, data) : handleShowCellDetails(e, mi, data)
                      }
                      style={{ flex: 1, background: mi.color }}>
                      {!isEmpty(gridCounts) && gridCounts[mi.cellName] ? (
                        <span className={classes.countValue}>{gridCounts[mi.cellName].length}</span>
                      ) : null}

                      {selectionDialog && <span className={classes.columnName}>{mi.cellName}</span>}

                      {!mi.isDisabled && colorSelect !== false && (
                        <ColorPickerDropdown
                          theme={theme}
                          // classes={classes}
                          selectedColor={mi.color}
                          onSelect={color => handleUpdateMatrix(color, mi, i, pi)}
                          style={{ width: "100%", height: "100%", padding: 0 }}
                          iconRenderer={
                            <div
                              style={{ height: "100%", width: "100%", background: mi.color }}></div>
                          }
                        />
                      )}
                    </li>
                  ))}
                </ul>
              </div>
            );
          })}
        </div>
        {legend ? (
          <>
            <div className={classes.columnsLegendCnt}>
              <div className={classes.axisHeadingCnt}>
                <Typography variant="h4" classes={{h4: classes.smallHeading}} title={data.settings.rows.title}>{data.settings.rows.title}</Typography>
              </div>
              <ul className={classes.legendTopList}>
                {rows.map(c => {
                  return (
                    <li key={c.defaultValue}>
                      {c.customValue ? (
                        <>
                          <span className={classes.legendValueBullet}>{c.defaultValue.toUpperCase()} :</span>
                          <span className={classes.legendValue}>{c.customValue}</span>
                        </>
                      ) : (
                        <>
                          <span>{c.defaultValue.toUpperCase()} :</span>
                          <span>-</span>{" "}
                        </>
                      )}
                    </li>
                  );
                })}
              </ul>
            </div>
            <div className={classes.rowsLegendCnt}>
              <div className={classes.axisHeadingCnt}>
                <Typography variant="h4" classes={{h4: classes.smallHeading}} title={data.settings.columns.title}> {data.settings.columns.title}</Typography>
              </div>
              <ul className={classes.legendRightList}>
                {columns.map(c => {
                  return (
                    <li key={c.defaultValue}>
                      {c.customValue ? (
                        <>
                          <span className={classes.legendValueBullet}>{c.defaultValue} :</span>
                          <span className={classes.legendValue}>{c.customValue}</span>
                        </>
                      ) : (
                        <>
                          <span>{c.defaultValue} :</span>
                          <span>-</span>{" "}
                        </>
                      )}
                    </li>
                  );
                })}
              </ul>
            </div>
          </>
        ) : null}
      </div>
    )
  );
}

MatrixGrid.defaultProps = {
  selectionDialog: false,
};

export default withStyles(netRiskStyles, { withTheme: true })(MatrixGrid);
