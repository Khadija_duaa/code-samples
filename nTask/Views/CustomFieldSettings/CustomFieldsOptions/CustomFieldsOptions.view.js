import React, { useState, useContext, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import CustomFieldsOptionsStyle from "./CustomFieldsOptions.style";
import CustomDialog from "../../../components/Dialog/CustomDialog";
import CustomFieldsContext from "../Context/customFields.context";
import Grid from "@material-ui/core/Grid";
import {
  dispatchFieldTypeAndView,
  dispatchSetViewType,
  dispatchEditViewType,
  clearFields,
  dispatchCustomFieldsType,
} from "../Context/actions/dropdown.actions";
import SvgIcon from "@material-ui/core/SvgIcon";
import {
  useExistingCustomField,
  dispatchDeleteField,
  editCustomField,
  getCustomFieldTypes,
} from "../../../redux/actions/customFields";
import { editMatrix } from "../Context/actions/matrix.actions";
import { fieldTypeData } from "../../../helper/customFieldsData";
import IconChecklist from "../../../components/Icons/IconChecklist";
import IconDropdown from "../../../components/Icons/IconDropdown";
import IconMatrix from "../../../components/Icons/IconMatrix";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ArrowDown from "@material-ui/icons/ArrowDropDown";
import ArrowUp from "@material-ui/icons/ArrowDropUp";
import EditIcon from "../../../components/Icons/EditIcon";
import DeleteIcon from "../../../components/Icons/Delete";
import CopyIcon from "../../../components/Icons/Copy";
import cloneDeep from "lodash/cloneDeep";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import DeleteConfirmDialog from "../../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import { injectIntl, FormattedMessage } from "react-intl";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import DropdownMenu from "../../../components/Dropdown/DropdownMenu";
import NewFieldView from "./Tabs/NewField.view";
import ManageFieldView from "./Tabs/ManageField.view";
import useCustomFieldHook from "./Tabs/useCustomField.hook";
import ExistingFieldView from "./Tabs/ExistingField.view";
import { updateCustomFieldDialogState } from "../../../redux/actions/allDialogs";

function CustomFieldsOptions({
  classes,
  theme,
  customFields,
  profile,
  feature,
  useExistingCustomField,
  editCustomField,
  dialogsState,
  updateCustomFieldDialogState
}) {
  const {
    state: { viewType },
    dispatch,
  } = useContext(CustomFieldsContext);

  const [currentView, setView] = useState("new");
  const { customFieldDialog } = dialogsState;

  const handleChange = (event, nextView) => {
    if (nextView) {
      setView(nextView);
    }
  };

  useEffect(() => {
    if (customFieldDialog.moduleViewType === "" || customFieldDialog.moduleViewType == "optionsSelect") {
      getCustomFieldTypes(res => {
        dispatchCustomFieldsType(dispatch, res);
      });
    }
  }, []);

  //Dialog Close and returing back to first state
  const closeDialog = () => {
    dispatchSetViewType(dispatch, "");
    clearFields(dispatch);
    updateCustomFieldDialogState({ moduleViewType: "", data: {}, mode: "" });
  };
  return (
    <>
      <CustomDialog
        title="Custom Fields"
        subTitle="Custom fields allow you to create data according to your business needs."
        dialogProps={{
          open: viewType === "optionsSelect",
          onClose: closeDialog,
          PaperProps: {
            className: classes.dialogPaperCmp,
          },
          disableBackdropClick: true,
          disableEscapeKeyDown: true,
        }}>
        <div className={classes.dialogContentCnt}>
          <Grid
            container
            direction="row"
            justify="space-between"
            alignItems="center"
            classes={{ container: classes.CustomFieldDashboardHeader }}>
            <div className="flex_center_start_row">
              <div className={classes.toggleContainer}>
                <ToggleButtonGroup
                  value={currentView}
                  exclusive
                  onChange={handleChange}
                  classes={{ root: classes.toggleBtnGroup }}>
                  <ToggleButton
                    value="new"
                    classes={{
                      root: classes.toggleButton,
                      selected: classes.toggleButtonSelected,
                    }}>
                    Create New Field
                  </ToggleButton>
                  <ToggleButton
                    value="manage"
                    classes={{
                      root: classes.toggleButton,
                      selected: classes.toggleButtonSelected,
                    }}>
                    Manage Fields
                  </ToggleButton>
                  <ToggleButton
                    value="existing"
                    classes={{
                      root: classes.toggleButton,
                      selected: classes.toggleButtonSelected,
                    }}>
                    Existing Fields
                  </ToggleButton>
                </ToggleButtonGroup>
              </div>
            </div>
          </Grid>
          {currentView === "new" && <NewFieldView feature={feature} />}
          {currentView === "manage" && <ManageFieldView feature={feature} />}
          {currentView === "existing" && <ExistingFieldView feature={feature} />}
        </div>
      </CustomDialog>
    </>
  );
}

CustomFieldsOptions.defaultProps = {
  /** default props for the component  */
  classes: {},
  theme: {},
  feature: "",
};

const mapStateToProps = state => {
  return {
    customFields: state.customFields,
    profile: state.profile.data,
    dialogsState: state.dialogStates,
  };
};

export default compose(
  injectIntl,
  withStyles(CustomFieldsOptionsStyle, { withTheme: true }),
  connect(mapStateToProps, {
    useExistingCustomField,
    dispatchDeleteField,
    editCustomField,
    getCustomFieldTypes,
    updateCustomFieldDialogState
  })
)(CustomFieldsOptions);
