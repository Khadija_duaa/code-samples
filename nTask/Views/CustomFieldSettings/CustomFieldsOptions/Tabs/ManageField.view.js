import React, { useContext, useEffect, useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";

import CustomFieldsOptionsStyle from "../CustomFieldsOptions.style";
import CustomFieldsContext from "../../Context/customFields.context";
import Grid from "@material-ui/core/Grid";
import { Scrollbars } from "react-custom-scrollbars";
import DropdownMenu from "../../../../components/Dropdown/DropdownMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { injectIntl, FormattedMessage } from "react-intl";
import SvgIcon from "@material-ui/core/SvgIcon";
import EditIcon from "../../../../components/Icons/EditIcon";
import DeleteIcon from "../../../../components/Icons/Delete";
import CopyIcon from "../../../../components/Icons/Copy";
import cloneDeep from "lodash/cloneDeep";
import { editCustomField, hideCustomField } from "../../../../redux/actions/customFields";
import useCustomFieldHook from "./useCustomField.hook";
import { dispatchDeleteField } from "../../../../redux/actions/customFields";
import DeleteConfirmDialog from "../../../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import CustomSelectDropdown from "../../../../components/Dropdown/CustomSelectDropdown/Dropdown";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import { updateFeatureAccessPermissions } from "../../../../redux/actions/team";
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';


function ManageField({
  classes,
  theme,
  customFields,
  profile,
  accessFeatures,
  editCustomField,
  hideCustomField,
  feature,
  dispatchDeleteField,
  updateFeatureAccessPermissions,
}) {
  const {
    state: { },
    permission,
    dispatch,
    onFieldDelete,
  } = useContext(CustomFieldsContext);

  const { isAllowAdd, isAllowDelete, isAllowEdit, isAllowHide } = permission;

  const [anchorEl, setAnchorEl] = useState(null);
  const [selectedFieldObj, setSelectedFieldObj] = useState({});
  const [deleteDialog, setDeleteDialog] = useState(false);
  const [deleteBtnQuery, setDeleteBtnQuery] = useState("");
  const [data, setData] = useState([
    {
      label: "Hide from this workspace",
      value: 1,
    },
    {
      label: "Hide from all workspaces",
      value: 2,
    },
  ]);
  const [unHideData, setUnHideData] = useState([
    {
      label: "Unhide from this workspace",
      value: 1,
    },
    {
      label: "Unhide from all workspaces",
      value: 2,
    },
  ]);
  const { fields, handleEditCopyField, renderExistingFieldIcon, superSystemFieldsArr } = useCustomFieldHook(
    classes,
    customFields,
    theme
  );
  //Checking if used fields list is empty or not
  const isEmptyUsedFields = (arr = []) => {
    let isEmpty = [];
    fields.forEach(e => {
      isEmpty = [...isEmpty, ...filterUsedFields(e.fields)];
    });
    return isEmpty;
  };

  const filterUsedFields = fields => {
    return fields.reduce((result, cv) => {
      const isExist =
        cv.workspaces &&
        cv.workspaces.find(
          wf => wf.workspaceId == profile.loggedInTeam && wf.groupType.includes(feature)
        );
      if (isExist || (!isExist && cv.team && cv.team.groupType.includes(feature))) result.push(cv);
      return result;
    }, []);
  };

  const handleClickVisibilty = (event, field) => {
    setSelectedFieldObj(field);
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };


  const checkVisibilityScenario = field => {
    if (field.settings.isHidden && field.settings.hiddenInGroup.includes(feature)) {
      return "Field may be hidden in all workspaces.";
    } else if (
      field.settings.hiddenInWorkspaces.length > 0 &&
      field.settings.hiddenInWorkspaces.indexOf(profile.loggedInTeam) >= 0 &&
      field.settings.hiddenInGroup.includes(feature)
    ) {
      return "Field is hidden from this workspace.";
    } else return null;
  };

  const handleClose = event => {
    // Function closes dropdown
    setAnchorEl(null);
    setAnchorSystemEl(null);
    setSelectedFieldObj({});
  };

  const unHidehandleOptionSelect = (value) => {
    let object = cloneDeep(selectedFieldObj);
   if (value == 1) {      
      object.hideOption = "unhideWorkspace";
    }
   if (value == 2) {
      object.settings.isHidden =
        object.settings.isHidden && object.settings.hiddenInGroup.length > 1
          ? object.settings.isHidden
          : false;
      object.settings.hiddenInWorkspaces =
        object.settings.hiddenInGroup.length == 1 ? [] : object.settings.hiddenInWorkspaces;
      object.hideOption = "unhideAllWorkspaces";
      object.settings.hiddenInGroup = object.settings.hiddenInGroup.filter(g => g !== feature);
    }
    delete object.team;
    delete object.workspaces;
    hideCustomField(
      object,
      res => {
        setSelectedFieldObj({});
        setAnchorEl(null);
        if (value !== 0) onFieldDelete(object);
      },
      failure => {
        setSelectedFieldObj({});
        setAnchorEl(null);
      }
    );
  }

  const handleOptionSelect = value => {
    let object = cloneDeep(selectedFieldObj);
    if (value == 1) {
      object.hideOption = "workspace";
      object.settings.isHidden = false;

      object.settings.hiddenInWorkspaces = [
        profile.loggedInTeam,
        ...object.settings.hiddenInWorkspaces,
      ];
      object.settings.hiddenInGroup =
        object.settings.hiddenInGroup.indexOf(feature) < 0
          ? [feature, ...object.settings.hiddenInGroup]
          : object.settings.hiddenInGroup;
    }
    if (value == 2) {
      /** if user select the team level */
      object.hideOption = "allWorkspaces";
      object.settings.isHidden = true;
      object.settings.hiddenInWorkspaces = [];
      object.settings.hiddenInGroup =
        object.settings.hiddenInGroup.indexOf(feature) < 0
          ? [feature, ...object.settings.hiddenInGroup]
          : object.settings.hiddenInGroup;
    }
    delete object.team;
    delete object.workspaces;
    hideCustomField(
      object,
      res => {
        setSelectedFieldObj({});
        setAnchorEl(null);
        if (value !== 0) onFieldDelete(object);
      },
      failure => {
        setSelectedFieldObj({});
        setAnchorEl(null);
      }
    );
  };
  const checkFieldHideOpt = (value) => {
    if (value == "workspace" || value == "allWorkspaces"){
      return unHideData;
    }
    else return data;
  }

  const hideShowdropDown = () => {
    let dropdownData = checkFieldHideOpt(selectedFieldObj.hideOption);
    let optionUnhide = dropdownData.some(item => item.label.includes('Unhide'));
    return (
      <>
        <DropdownMenu
          open={open}
          closeAction={handleClose}
          anchorEl={anchorEl}
          size={"small"}
          placement="bottom-start">
          <Scrollbars autoHide={false} autoHeight autoHeightMin={0} autoHeightMax={180}>
            <List>
              {dropdownData.map((o, i) => {
                return (
                  <ListItem
                    button
                    className={classes.listItem}
                    onClick={event => {
                      optionUnhide ? unHidehandleOptionSelect(o.value) : handleOptionSelect(o.value)
                    }}
                    key={i}>
                    <div className={classes.itemTextCnt}>
                      <span
                        className={classes.itemText}>
                        {o.label}
                      </span>
                    </div>
                  </ListItem>
                );
              })}
            </List>
          </Scrollbars>
        </DropdownMenu>
      </>
    );
  };

  const handleDelete = () => {
    /** handle delete function responsible for deleting the custom field */
    setDeleteBtnQuery("progress");
    let obj = [selectedFieldObj.fieldId];
    dispatchDeleteField(
      obj,
      succ => {
        handleDeleteDialogClose();
        onFieldDelete(selectedFieldObj);
      },
      fail => {
        setDeleteBtnQuery("");
      },
      selectedFieldObj
    );
  };

  const handleDeleteField = param => {
    setDeleteDialog(true);
    setSelectedFieldObj(param);
  };
  const handleDeleteDialogClose = () => {
    setDeleteDialog(false);
    setSelectedFieldObj({});
    setDeleteBtnQuery("");
  }; 

  const countCalculate = key => {
    let filteredFields = [];
    fields.map(f => {
      filteredFields = [...filteredFields, ...filterUsedFields(f.fields)];
    });
    if (key == "all") return filteredFields.length + featureArray.length;
    else if (key == "visible") {
      const systemField = featureArray.filter(e =>
        !featureObj[e].isHidden
      ).length
      const customField = filteredFields.filter(
        e =>
          !e.settings.isHidden &&
          !e.settings.hiddenInWorkspaces.includes(profile.loggedInTeam) &&
          !e.settings.hiddenInGroup.includes(module)
      ).length;
      return customField + systemField
    }
    else if (key == "hidden") {
      const systemField = featureArray.filter(e =>
        featureObj[e].isHidden
      ).length
      const customField = filteredFields.filter(
        e =>
          e.settings.isHidden ||
          e.settings.hiddenInWorkspaces.includes(profile.loggedInTeam) ||
          e.settings.hiddenInGroup.includes(module)
      ).length;
      return customField + systemField
    }
  };
  const options = () => [
    {
      label: `All ${feature} Fields (${countCalculate("all")})`,
      value: "all",
    },
    {
      label: `Visible Fields (${countCalculate("visible")})`,
      value: "visible",
    },
    {
      label: `Hidden Fields (${countCalculate("hidden")})`,
      value: "hidden",
    },
  ];
  const [selectedOpt, setSelectedOpt] = useState(null);

  const handleSelectOption = item => {
    setSelectedOpt(item);
  };
  const updateFieldFilter = fields => {
    if (fields.length > 0 && selectedOpt) {
      switch (selectedOpt.value) {
        case "all":
          return fields;
          break;
        case "visible":
          return fields.filter(
            e =>
              !e.settings.isHidden &&
              !e.settings.hiddenInWorkspaces.includes(profile.loggedInTeam) &&
              !e.settings.hiddenInGroup.includes(module)
          );
          break;
        case "hidden":
          return fields.filter(
            e =>
              e.settings.isHidden ||
              e.settings.hiddenInWorkspaces.includes(profile.loggedInTeam) ||
              e.settings.hiddenInGroup.includes(module)
          );
          break;

        default:
          break;
      }
    } else return fields;
  };
  const [featureObj, setFeatureObj] = useState({});
  const [featureArray, setFeatureArray] = useState([]);
  const [anchorSystemEl, setAnchorSystemEl] = useState(null);
  const [systemFieldView, setSystemFieldView] = useState(true);
  const [customFieldView, setCustomFieldView] = useState(true);

  useEffect(() => {
    setFeatureObj(accessFeatures[feature]);
    setFeatureArray(Object.keys(accessFeatures[feature]))
  }, [accessFeatures])
  // show system field dropdown
  const handleClickVisibiltySystemField = (event, field) => {
    setSelectedFieldObj(field);
    setAnchorSystemEl(anchorSystemEl ? null : event.currentTarget);
  };

  const handleSystemOptionSelect = value => {
    let object = {
      fieldId: selectedFieldObj.fieldId
    };
    if (value == 1) {
      object.IsHiddenInWorkspace = true;
    }
    if (value == 2) {
      object.IsHiddenInTeam = true;
    }
    updateFeatureAccessPermissions(
      feature,
      object,
      succ => {
        handleClose();
      },
      fail => {
        handleClose();
      }
    );
  };
  const unHideHandleSystemOptionSelect = (value) => {
    let object = {
      fieldId: selectedFieldObj.fieldId
    };
    if (value == 1) {
      object.IsHiddenInWorkspace = false;
    }
    if (value == 2) {
      object.IsHiddenInTeam = false;
    }
    updateFeatureAccessPermissions(
      feature,
      object,
      succ => {
        handleClose();
      },
      fail => {
        handleClose();
      }
    );
  }

  const checkVisibilityScenarioSysemtFields = field => {
    if (!field.isHidden) {
      return null;
    } else if (field.isHidden && field.settings.hiddenInWorkspaces.length) {
      return "Field is hidden from this workspace.";
    } else if (field.isHidden && !field.settings.hiddenInWorkspaces.length) {
      return "Field may be hidden in all workspaces.";
    }
  };
  const updateSytemFieldFilter = fields => {
    if (featureArray.length > 0 && selectedOpt) {
      switch (selectedOpt.value) {
        case "all":
          return true;
          break;
        case "visible":
          return !fields.isHidden;
          break;
        case "hidden":
          return fields.isHidden
          break;
        default:
          break;
      }
    } else return true;
  };
  const checkFieldHideOptSystem = (field) => {
    return !field.isHidden ? data : unHideData;
  }
  const hideShowSystemdropDown = () => {
    let dropdownData = checkFieldHideOptSystem(selectedFieldObj);
    let optionUnhide = dropdownData.some(item => item.label.includes('Unhide'));
    return (
      <>
        <DropdownMenu
          open={openSystemField}
          closeAction={handleClose}
          anchorEl={anchorSystemEl}
          size={"small"}
          placement="bottom-start">
          <Scrollbars autoHide={false} autoHeight autoHeightMin={0} autoHeightMax={180}>
            <List>
              {dropdownData.map((o, i) => {
                return (
                  <ListItem
                    button
                    className={classes.listItem}
                    onClick={event => {
                      optionUnhide ? unHideHandleSystemOptionSelect(o.value) : handleSystemOptionSelect(o.value)
                    }}
                    key={i}>
                    <div className={classes.itemTextCnt}>
                      <span
                        className={classes.itemText}>
                        {o.label}
                      </span>
                    </div>
                  </ListItem>
                );
              })}
            </List>
          </Scrollbars>
        </DropdownMenu>
      </>
    );
  };

  const open = Boolean(anchorEl);
  const openSystemField = Boolean(anchorSystemEl);
  const handleOpenView = (key) => {
    switch (key) {
      case "systemFieldView":
        setSystemFieldView(!systemFieldView);
        return;
        break;
      case "customFieldView":
        setCustomFieldView(!customFieldView);
        return;
        break;
      default:
        break;
    }
  };
  return (
    <>
      <Grid container>
        <Grid item xs={12} classes={{ item: classes.headManageCnt }}>
          <CustomSelectDropdown
            label={null}
            options={options}
            option={selectedOpt || options()[0]}
            onSelect={handleSelectOption}
            size="medium"
            scrollHeight={320}
            disabled={false}
            buttonProps={{
              style: {
                padding: "5px 0px",
                borderRadius: 4,
                fontSize: "15px",
                textTransform: "capitalize"
              },
            }}
          />
          <Scrollbars autoHide={true} autoHeight autoHeightMin={0} autoHeightMax={470}>
            {/* show hide system fields */}
            {/* {featureArray.length? : null} */}
            <div>
              <div
                className={classes.manageFieldAccordion}
                onClick={(e) => handleOpenView("systemFieldView")}>
                <span className={classes.boardOptTitle}>System Fields</span>
                {!systemFieldView ? (
                  <KeyboardArrowDownIcon htmlColor={theme.palette.secondary.medDark} />
                ) : (
                  <KeyboardArrowUpIcon htmlColor={theme.palette.secondary.medDark} />
                )}
              </div>
              {/* featureObj */}
              {systemFieldView && featureArray.length ? (
                featureArray.map(item => {
                  let filteredFields = updateSytemFieldFilter(featureObj[item]);
                  return (<>
                    {filteredFields ?
                      <div className={classes.manageFieldChildCnt}>
                        <div className={classes.manageFieldBaseCnt}>
                          {renderExistingFieldIcon(featureObj[item], "grey")}
                          <span className={classes.manageFieldChildTitle} title={featureObj[item].fieldName}>
                            {featureObj[item].fieldName} <br />
                            <span style={{ fontSize: "12px", color: "#505050", fontWeight: 400 }}>
                              {checkVisibilityScenarioSysemtFields(featureObj[item])}
                            </span>
                          </span>
                        </div>
                        <div
                          className={
                            selectedFieldObj.fieldId === featureObj[item].fieldId
                              ? classes.btnContainerVisible
                              : classes.btnContainer
                          }>
                          {isAllowHide && <span
                            onClick={e => {
                              handleClickVisibiltySystemField(e, featureObj[item]);
                            }}
                            ref={anchorSystemEl}>
                            <MoreVerticalIcon
                              htmlColor={theme.palette.secondary.medDark}
                              style={{ fontSize: "24px", marginBottom: -6 }}
                            />
                          </span>}
                        </div>
                      </div> : null}
                  </>
                  )
                })
              ) : systemFieldView ? (
                <div className={classes.emptyFieldCnt}>
                  <span> No fields to manage</span>
                </div>
              ) : null}
            </div>
            {/* show hide custom fields */}
            <div
              className={`${classes.manageFieldAccordion} ${classes.manageCustomFieldAccordion}`}
              onClick={(e) => handleOpenView("customFieldView")}>
              <span className={classes.boardOptTitle}>Custom Fields</span>{!customFieldView ? (
                <KeyboardArrowDownIcon htmlColor={theme.palette.secondary.medDark} />
              ) : (
                <KeyboardArrowUpIcon htmlColor={theme.palette.secondary.medDark} />
              )}
            </div>
            {customFieldView && isEmptyUsedFields().length > 0 ? (
              fields.map(e => {
                let filteredFields = filterUsedFields(e.fields);
                filteredFields = updateFieldFilter(filteredFields);
                return (
                  filteredFields.length > 0 && (
                    <div
                    // style={{ borderBottom: `1px solid ${theme.palette.border.lightBorder}` }}
                    >
                      {filteredFields.map(f => {
                        let superSystemFields = !superSystemFieldsArr.includes(f.fieldName);
                        return (
                          <div className={classes.manageFieldChildCnt}>
                            <div className={classes.manageFieldBaseCnt}>
                              {renderExistingFieldIcon(f, f.isSystem ? "grey" : "blue")}
                              <span className={classes.manageFieldChildTitle} title={f.fieldName}>
                                {f.fieldName} <br />
                                <span style={{ fontSize: "12px", color: "#505050", fontWeight: 400 }}>
                                  {checkVisibilityScenario(f)}
                                </span>
                              </span>
                            </div>
                            <div
                              className={
                                selectedFieldObj.fieldId === f.fieldId
                                  ? classes.btnContainerVisible
                                  : classes.btnContainer
                              }>
                              {isAllowAdd && superSystemFields && (
                                <span
                                  className={classes.editbtn}
                                  onClick={() => {
                                    handleEditCopyField(f, "copy");
                                  }}>
                                  <SvgIcon
                                    className={classes.editFieldIcon}
                                    color={theme.palette.icon.gray600}
                                    viewBox="0 0 15 15">
                                    <CopyIcon />
                                  </SvgIcon>
                                  Copy
                                </span>
                              )}
                              {f.fieldType !== "checklist" && isAllowAdd && superSystemFields && (
                                <span
                                  className={classes.editbtn}
                                  onClick={() => {
                                    handleEditCopyField(f, "edit");
                                  }}>
                                  <SvgIcon
                                    className={classes.editFieldIcon}
                                    color={theme.palette.icon.gray600}
                                    viewBox="0 0 14 13.95">
                                    <EditIcon />
                                  </SvgIcon>
                                  Edit
                                </span>
                              )}
                              {!f.isSystem && isAllowDelete && superSystemFields && (
                                <span
                                  className={classes.deletebtn}
                                  onClick={() => {
                                    handleDeleteField(f);
                                  }}>
                                  <SvgIcon
                                    className={classes.deleteFieldIcon}
                                    viewBox="0 0 14 14.005">
                                    <DeleteIcon />
                                  </SvgIcon>
                                  Delete
                                </span>
                              )}
                              {isAllowHide && f.fieldType !== "section" && (
                                <span
                                  // className={classes.editbtn}
                                  onClick={e => {
                                    handleClickVisibilty(e, f);
                                  }}
                                  ref={anchorEl}>
                                  <MoreVerticalIcon
                                    htmlColor={theme.palette.secondary.medDark}
                                    style={{ fontSize: "24px", marginBottom: -6 }}
                                  />
                                  {/* <SvgIcon
                                    className={classes.hideFieldIcon}
                                    color={theme.palette.icon.gray600}
                                    viewBox="0 0 14 13.95">
                                    <Visibility />
                                  </SvgIcon> */}
                                </span>
                              )}
                            </div>
                          </div>
                        );
                      })}
                    </div>
                  )
                );
              })
            ) : customFieldView ? (
              <div className={classes.emptyFieldCnt}>
                <span> No fields to manage</span>
              </div>
            ) : null}


          </Scrollbars>
        </Grid>
        {hideShowdropDown() /** render drop down component */}
        {hideShowSystemdropDown() /** render drop down component */}
      </Grid>

      {deleteDialog ? (
        <DeleteConfirmDialog
          open={deleteDialog}
          closeAction={handleDeleteDialogClose}
          cancelBtnText={
            <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
          }
          successBtnText={
            <FormattedMessage
              id="common.action.delete.confirmation.delete-button.label"
              defaultMessage="Delete"
            />
          }
          alignment="center"
          headingText={
            <FormattedMessage
              id="common.action.delete.confirmation.delete-button.label"
              defaultMessage="Delete"
            />
          }
          successAction={handleDelete}
          msgText={"Are you sure you want to delete this field?"}
          btnQuery={deleteBtnQuery}
        />
      ) : null}
    </>
  );
}

ManageField.defaultProps = {
  /** default props for the component  */
  classes: {},
  theme: {},
};

const mapStateToProps = state => {
  return {
    customFields: state.customFields,
    profile: state.profile.data,
    accessFeatures: state.featureAccessPermissions,
  };
};

export default compose(
  injectIntl,
  withStyles(CustomFieldsOptionsStyle, { withTheme: true }),
  connect(mapStateToProps, {
    editCustomField,
    dispatchDeleteField,
    hideCustomField,
    updateFeatureAccessPermissions
  })
)(ManageField);
