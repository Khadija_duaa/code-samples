import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";
import TextFieldIcon from "../../../../components/Icons/IconTextfield";
import IconTextarea from "../../../../components/Icons/IconTextarea";
import IconDate from "../../../../components/Icons/IconDate";
import IconPhone from "../../../../components/Icons/IconPhone";
import IconEmail from "../../../../components/Icons/IconEmail";
import IconWebUrl from "../../../../components/Icons/IconWebUrl";
import IconNumber from "../../../../components/Icons/IconNumber";
import IconMoney from "../../../../components/Icons/IconMoney";
import IconReminder from "../../../../components/Icons/IconReminder";
import IconLocation from "../../../../components/Icons/IconLocation";
import IconPeople from "../../../../components/Icons/IconPeople";
import IconRating from "../../../../components/Icons/IconRating";
import IconSection from "../../../../components/Icons/IconSection";
import IconAttachment from "../../../../components/Icons/IconAttachment";
import IconFormula from "../../../../components/Icons/IconFormula";
import IconCountry from "../../../../components/Icons/IconCountry";
import IconChecklist from "../../../../components/Icons/IconChecklist";
import IconDropdown from "../../../../components/Icons/IconDropdown";
import IconMatrix from "../../../../components/Icons/IconMatrix";

export function getIcon(classes, value, theme) {
  /** get icons on the basis of field type */
  switch (value) {
    case "dropdown":
      return (
        <div className={classes.iconCntdd}>
          <SvgIcon viewBox="0 0 18 18" fontSize="inherit">
            <IconDropdown />
          </SvgIcon>
        </div>
      );
      break;
    case "textarea":
      return (
        <div className={classes.textArea}>
          <SvgIcon viewBox="0 0 18 18" fontSize="inherit">
            <IconTextarea />
          </SvgIcon>
        </div>
      );
    case "checklist":
      return (
        <div className={classes.iconCntChecklist}>
          <SvgIcon viewBox="0 0 14 12.703" fontSize="inherit">
            <IconChecklist color={theme.palette.customFieldIcons.blue} />
          </SvgIcon>
        </div>
      );
      break;
    case "matrix":
      return (
        <div className={classes.iconCntMatrix}>
          <SvgIcon viewBox="0 0 16 16.511" fontSize="inherit">
            <IconMatrix />
          </SvgIcon>
        </div>
      );
      break;
    case "textfield":
      return (
        <div className={classes.iconCntTextFieldSingle}>
          <SvgIcon viewBox="0 0 20 12.969" fontSize="inherit">
            <TextFieldIcon />
          </SvgIcon>
        </div>
      );
      break;
    case "datefield":
      return (
        <div className={classes.iconCntTextField}>
          <SvgIcon viewBox="0 0 16 17.778" fontSize="inherit">
            <IconDate />
          </SvgIcon>
        </div>
      );
      break;

    case "email":
      return (
        <div className={classes.iconEmail}>
          <SvgIcon viewBox="0 0 18 14.4" fontSize="inherit">
            <IconEmail />
          </SvgIcon>
        </div>
      );
      break;
    case "phone":
      return (
        <div className={classes.iconPhone}>
          <SvgIcon viewBox="0 0 16 17.778" fontSize="inherit">
            <IconPhone />
          </SvgIcon>
        </div>
      );
      break;
    case "websiteurl":
      return (
        <div className={classes.websiteUrl}>
          <SvgIcon viewBox="0 0 20.209 11" fontSize="inherit">
            <IconWebUrl />
          </SvgIcon>
        </div>
      );
      break;
    case "number":
      return (
        <div className={classes.iconNumber}>
          <SvgIcon viewBox="0 0 16 17.778" fontSize="inherit">
            <IconNumber />
          </SvgIcon>
        </div>
      );
      break;
    case "money":
      return (
        <div className={classes.iconMoney}>
          <SvgIcon viewBox="0 0 10 17.485" fontSize="inherit">
            <IconMoney />
          </SvgIcon>
        </div>
      );
      break;
    case "reminder":
      return (
        <div className={classes.iconReminder}>
          <SvgIcon viewBox="0 0 19.802 19.962" fontSize="inherit">
            <IconReminder />
          </SvgIcon>
        </div>
      );
      break;
    case "location":
      return (
        <div className={classes.iconLocation}>
          <SvgIcon viewBox="0 0 15 17.551" fontSize="inherit">
            <IconLocation />
          </SvgIcon>
        </div>
      );
      break;
    case "people":
      return (
        <div className={classes.iconCntdd}>
          <SvgIcon viewBox="0 0 16 14.4" fontSize="inherit">
            <IconPeople />
          </SvgIcon>
        </div>
      );
      break;
    case "rating":
      return (
        <div className={classes.iconRating}>
          <SvgIcon viewBox="0 0 17.219 16.517" fontSize="inherit">
            <IconRating />
          </SvgIcon>
        </div>
      );
      break;
    case "section":
      return (
        <div className={classes.iconNumber}>
          <SvgIcon viewBox="0 0 18 17.5" fontSize="inherit">
            <IconSection />
          </SvgIcon>
        </div>
      );
      break;
    case "date":
      return (
        <div className={classes.iconCntdd}>
          <SvgIcon viewBox="0 0 16 17.778" fontSize="inherit">
            <IconDate />
          </SvgIcon>
        </div>
      );
      break;
    case "filesAndMedia":
      return (
        <div className={classes.iconRating}>
          <SvgIcon viewBox="0 0 18 17.5" fontSize="inherit">
            <IconAttachment />
          </SvgIcon>
        </div>
      );
      break;
    case "formula":
      return (
        <div className={classes.iconLocation}>
          <SvgIcon viewBox="0 0 18 16.671" fontSize="inherit">
            <IconFormula />
          </SvgIcon>
        </div>
      );
      break;
    case "country":
      return (
        <div className={classes.iconCountry}>
          <SvgIcon viewBox="0 0 18 18" fontSize="inherit">
            <IconCountry />
          </SvgIcon>
        </div>
      );
      break;
    default:
      return <> </>;
      break;
  }
}
