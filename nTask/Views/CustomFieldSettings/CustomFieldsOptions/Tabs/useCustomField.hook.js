import React, { useEffect, useState, useContext } from "react";
import {
  dispatchEditViewType,
  dispatchFieldTypeAndView,
} from "../../Context/actions/dropdown.actions";
import { editMatrix } from "../../Context/actions/matrix.actions";
import SvgIcon from "@material-ui/core/SvgIcon";
import TextFieldIcon from "../../../../components/Icons/IconTextfield";
import IconTextarea from "../../../../components/Icons/IconTextarea";
import IconDate from "../../../../components/Icons/IconDate";
import IconPhone from "../../../../components/Icons/IconPhone";
import IconEmail from "../../../../components/Icons/IconEmail";
import IconWebUrl from "../../../../components/Icons/IconWebUrl";
import IconNumber from "../../../../components/Icons/IconNumber";
import IconMoney from "../../../../components/Icons/IconMoney";
import IconReminder from "../../../../components/Icons/IconReminder";
import IconLocation from "../../../../components/Icons/IconLocation";
import IconDropdown from "../../../../components/Icons/IconDropdown";
import IconChecklist from "../../../../components/Icons/IconChecklist";
import IconMatrix from "../../../../components/Icons/IconMatrix";
import IconSection from "../../../../components/Icons/IconSection";
import IconPeople from "../../../../components/Icons/IconPeople";
import IconRating from "../../../../components/Icons/IconRating";
import IconAttachment from "../../../../components/Icons/IconAttachment";
import IconFormula from "../../../../components/Icons/IconFormula";
import CustomFieldsContext from "../../Context/customFields.context";

import { fieldTypeData } from "../../../../helper/customFieldsData";
import IconCountry from "../../../../components/Icons/IconCountry";
import IconBoolean from "../../../../components/Icons/IconBoolean";

function useCustomFieldHook(classes, customFields, theme) {
  const {
    state: { fieldTypes },
    dispatch,
  } = useContext(CustomFieldsContext);

  const viewTypeOpt = fieldTypeData(fieldTypes);
  const superSystemFieldsArr = [
    "Task",
    "Impact",
    "Likelihood",
    "Project",
    "Priority",
    "Planned Start",
    "Planned End",
    "Actual Start",
    "Actual End",
    "Description",
    "To-do Items List",
    "Status",
    "Severity",
    "Issue Type",
    "Task",
    "Details",
    "Mitigation Plan",
  ];
  const [fields, setFields] = useState([]);

  const renderExistingFieldIcon = (e, type) => {
    switch (e.fieldType) {
      case "dropdown":
        return (
          <SvgIcon viewBox="0 0 18 18" className={classes.iconFields}>
            <IconDropdown color={theme.palette.customFieldIcons[type]} />
          </SvgIcon>
        );
        break;
      case "checklist":
        return (
          <SvgIcon viewBox="0 0 14 12.703" className={classes.iconFields}>
            <IconChecklist color={theme.palette.customFieldIcons[type]} />
          </SvgIcon>
        );
        break;
      case "matrix":
        return (
          <SvgIcon viewBox="0 0 16 16.511" className={classes.iconFields}>
            <IconMatrix color={theme.palette.customFieldIcons[type]} />
          </SvgIcon>
        );
        break;
      case "textfield":
        return (
          <SvgIcon viewBox="0 0 20 12.969" className={classes.iconFields}>
            <TextFieldIcon color={theme.palette.customFieldIcons[type]} />
          </SvgIcon>
        );
        break;
      case "section":
        return (
          <SvgIcon viewBox="0 0 18 17.5" className={classes.iconFields}>
            <IconSection color={theme.palette.customFieldIcons[type]} />
          </SvgIcon>
        );
        break;
      case "email":
        return (
          <SvgIcon viewBox="0 0 18 14.4" className={classes.iconFields}>
            <IconEmail color={theme.palette.customFieldIcons[type]} />
          </SvgIcon>
        );
        break;
      case "phone":
        return (
          <SvgIcon
            viewBox="0 0 16 17.778"
            className={classes.iconFields}
            style={{ color: theme.palette.customFieldIcons[type] }}>
            <IconPhone color={theme.palette.customFieldIcons[type]} />
          </SvgIcon>
        );
        break;
      case "websiteurl":
        return (
          <SvgIcon viewBox="0 0 20.209 11" className={classes.iconFields}>
            <IconWebUrl color={theme.palette.customFieldIcons[type]} />
          </SvgIcon>
        );
        break;
      case "money":
        return (
          <SvgIcon viewBox="0 0 10 17.485" className={classes.iconFields}>
            <IconMoney color={theme.palette.customFieldIcons[type]} />
          </SvgIcon>
        );
        break;
      case "people":
        return (
          <SvgIcon viewBox="0 0 16 14.4" className={classes.iconFields}>
            <IconPeople color={theme.palette.customFieldIcons[type]} />
          </SvgIcon>
        );
        break;
      case "location":
        return (
          <SvgIcon viewBox="0 0 15 17.551" className={classes.iconFields}>
            <IconLocation color={theme.palette.customFieldIcons[type]} />
          </SvgIcon>
        );
        break;
      case "textarea":
        return (
          <SvgIcon viewBox="0 0 18 18" className={classes.iconFields}>
            <IconTextarea color={theme.palette.customFieldIcons[type]} />
          </SvgIcon>
        );
        break;
      case "rating":
        return (
          <SvgIcon viewBox="0 0 17.219 16.517" className={classes.iconFields}>
            <IconRating color={theme.palette.customFieldIcons[type]} />
          </SvgIcon>
        );
        break;
      case "reminder":
        return (
          <SvgIcon viewBox="0 0 19.802 19.962" className={classes.iconFields}>
            <IconReminder color={theme.palette.customFieldIcons[type]} />
          </SvgIcon>
        );
        break;
      case "Reminder":
        return (
          <SvgIcon viewBox="0 0 19.802 19.962" className={classes.iconFields}>
            <IconReminder color={theme.palette.customFieldIcons[type]} />
          </SvgIcon>
        );
        break;
      case "date":
        return (
          <SvgIcon viewBox="0 0 16 17.778" fontSize="inherit" className={classes.iconFields}>
            <IconDate color={theme.palette.customFieldIcons[type]} />
          </SvgIcon>
        );
        break;
      case "filesAndMedia":
        return (
          <SvgIcon viewBox="0 0 18 17.5" fontSize="inherit" className={classes.iconFields}>
            <IconAttachment color={theme.palette.customFieldIcons[type]} />
          </SvgIcon>
        );
        break;
      case "formula":
        return (
          <SvgIcon viewBox="0 0 18 16.671" fontSize="inherit" className={classes.iconFields}>
            <IconFormula color={theme.palette.customFieldIcons[type]} />
          </SvgIcon>
        );
        break;
      case "number":
        return (
          <SvgIcon viewBox="0 0 13 15.57" fontSize="inherit" className={classes.iconFields}>
            <IconNumber color={theme.palette.customFieldIcons[type]} />
          </SvgIcon>
        );
        break;
      case "country":
        return (
          <SvgIcon viewBox="0 0 18 18" fontSize="inherit" className={classes.iconFields}>
            <IconCountry color={theme.palette.customFieldIcons[type]} />
          </SvgIcon>
        );
        break;
      case "boolean":
        return (
          <SvgIcon viewBox="0 0 20.2 16.479" fontSize="inherit" className={classes.iconFields}>
            <IconBoolean color={theme.palette.customFieldIcons[type]} />
          </SvgIcon>
        );
        break;
      default:
        return <> </>;
        break;
    }
  };

  const handleItemClick = (e, value) => {
    e && e.stopPropagation();
    let viewTypeOption = viewTypeOpt.find(vt => vt.value === value) || {};
    dispatchFieldTypeAndView(dispatch, {
      selectedField: viewTypeOption,
      viewType: "newField",
    });
  };

  const handleEditCopyField = (obj, mode) => {
    /** function call when user edit/copy custom field */
    if (obj.fieldType === "matrix") editMatrix(dispatch, obj);
    dispatchEditViewType(dispatch, obj, mode);
    handleItemClick(null, obj.fieldType);
  };

  const renderCustomFields = param => {
    let arr = param.map(e => {
      e.collapse = false;
      return e;
    });
    setFields(arr);
  };

  const handleClickExistingField = ele => {
    let arr = fields.map(e => {
      if (e.fieldType === ele.fieldType) e.collapse = !e.collapse;
      return e;
    });
    setFields(arr);
  };

  useEffect(() => {
    renderCustomFields(customFields.groupCustomFields);
  }, [customFields.groupCustomFields]);

  return {
    fields,
    superSystemFieldsArr,
    handleEditCopyField,
    renderExistingFieldIcon,
    handleItemClick,
    handleClickExistingField,
  };
}

export default useCustomFieldHook;
