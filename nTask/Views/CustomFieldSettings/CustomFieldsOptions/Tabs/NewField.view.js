import React, { useContext } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import { fieldTypeData } from "../../../../helper/customFieldsData";
import { Scrollbars } from "react-custom-scrollbars";
import CustomFieldsOptionsStyle from "../CustomFieldsOptions.style";
import CustomFieldsContext from "../../Context/customFields.context";
import { injectIntl } from "react-intl";
import { dispatchFieldTypeAndView } from "../../Context/actions/dropdown.actions";
import { getIcon } from "./Icons";

function NewField({ classes, theme, feature }) {
  const {
    state: { fieldTypes },
    permission,
    dispatch,
  } = useContext(CustomFieldsContext);

  const viewTypeOpt =
    feature == "risk"
      ? fieldTypeData(fieldTypes)
      : fieldTypeData(fieldTypes).filter(f => f.value !== "matrix");
  const { isAllowAdd } = permission;

  const handleItemClick = (e, value) => {
    e && e.stopPropagation();
    let viewTypeOption = viewTypeOpt.find(vt => vt.value === value) || {};
    dispatchFieldTypeAndView(dispatch, {
      selectedField: viewTypeOption,
      viewType: "newField",
    });
  };
  
  return (
    <Scrollbars
      autoHeight
      autoHeightMin={0}
      autoHeightMax={485}
      renderTrackHorizontal={props => <div {...props} style={{ display: "none" }} />}>
      <Grid container spacing={2} style={{margin : "0 5px 0px 0px", width: "auto"}}>
        {viewTypeOpt.length > 0 &&
          viewTypeOpt.map(opt => {
            return (
              <>
                <Grid item xs={6}>
                  <div
                    className={!isAllowAdd ? classes.disableItemContainer : classes.itemContainer}
                    onClick={e => handleItemClick(e, opt.value)}>
                    <div>{getIcon(classes, opt.value, theme)}</div>
                    <div className={classes.txtCnt}>
                      <span className={classes.optionTxt}>{opt.label}</span>
                      <span className={classes.optionSubTxt}>{opt.obj.fieldDescription}</span>
                    </div>
                  </div>
                </Grid>
              </>
            );
          })}
      </Grid>
    </Scrollbars>
  );
}

NewField.defaultProps = {
  /** default props for the component  */
  classes: {},
  theme: {},
};

const mapStateToProps = state => {
  return {
    customFields: state.customFields,
    profile: state.profile.data,
  };
};

export default compose(
  injectIntl,
  withStyles(CustomFieldsOptionsStyle, { withTheme: true }),
  connect(mapStateToProps, {})
)(NewField);
