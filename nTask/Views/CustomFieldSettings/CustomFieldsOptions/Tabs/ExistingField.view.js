import React, { useContext, useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";

import CustomFieldsOptionsStyle from "../CustomFieldsOptions.style";
import CustomFieldsContext from "../../Context/customFields.context";
import useCustomFieldHook from "./useCustomField.hook";

import { injectIntl, FormattedMessage } from "react-intl";
import Grid from "@material-ui/core/Grid";
import { Scrollbars } from "react-custom-scrollbars";
import SvgIcon from "@material-ui/core/SvgIcon";
import DeleteIcon from "../../../../components/Icons/Delete";
import CopyIcon from "../../../../components/Icons/Copy";
import ArrowDown from "@material-ui/icons/ArrowDropDown";
import ArrowUp from "@material-ui/icons/ArrowDropUp";

import { editCustomField, dispatchDeleteField } from "../../../../redux/actions/customFields";
import DeleteConfirmDialog from "../../../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import { dispatchSetViewType, clearFields } from "../../Context/actions/dropdown.actions";

function ExistingField({
  classes,
  theme,
  customFields,
  profile,
  editCustomField,
  feature,
  dispatchDeleteField,
}) {
  const {
    state: {},
    permission,
    dispatch,
    onFieldDelete,
  } = useContext(CustomFieldsContext);

  const {
    fields,
    handleEditCopyField,
    renderExistingFieldIcon,
    handleClickExistingField,
  } = useCustomFieldHook(classes, customFields, theme);

  const { isAllowAdd, isAllowDelete, isAllowEdit, isUseField } = permission;

  const [deleteDialog, setDeleteDialog] = useState(false);
  const [btnQuery, setBtnQuery] = useState("");
  const [selectedFieldObj, setSelectedFieldObj] = useState({});

  const filterFields = a => {
    let dropdowns = a.reduce((res, cv) => {
      const existingFields =
        cv.workspaces.length &&
        cv.workspaces.every(
          wf => wf.workspaceId !== profile.loggedInTeam || !wf.groupType.includes(feature)
        );
      if ((existingFields || (cv.team && !cv.team.groupType.includes(feature))) && !cv.isSystem)
        res.push(cv);
      return res;
    }, []);
    return dropdowns;
  };

  const handleDelete = () => {
    /** handle delete function responsible for deleting the custom field */
    setBtnQuery("progress");
    let obj = [selectedFieldObj.fieldId];
    dispatchDeleteField(
      obj,
      succ => {
        setBtnQuery("");

        handleDeleteDialogClose();
        onFieldDelete(selectedFieldObj);
      },
      fail => {
        setBtnQuery("");
      }
    );
  };

  const handleDeleteField = param => {
    setDeleteDialog(true);
    setSelectedFieldObj(param);
  };
  const handleDeleteDialogClose = () => {
    setDeleteDialog(false);
    setSelectedFieldObj({});
  };
  const handleClickUseField = field => {
    let updatedField;
    const teamGroupType = field.team && field.team.groupType ? field.team.groupType : [];
    if (field.team) {
      updatedField = {
        ...field,
        team: { ...field.team, groupType: [...field.team.groupType, feature] },
      };
    } else {
      const isExist = field.workspaces.findIndex(w => w.workspaceId == profile.loggedInTeam) > -1;
      let updatedWorkspaces;
      if (isExist) {
        updatedWorkspaces = field.workspaces.map(w => {
          if (w.workspaceId == profile.loggedInTeam) {
            return { ...w, groupType: [...w.groupType, feature] };
          }
          return w;
        });
      } else {
        updatedWorkspaces = [
          ...field.workspaces,
          { workspaceId: profile.loggedInTeam, groupType: [feature] },
        ];
      }
      updatedField = { ...field, workspaces: updatedWorkspaces };
      // delete field.team;
    }

    editCustomField(
      updatedField,
      updatedField.fieldId,
      res => {
        // closeDialog();
      },
      failure => {}
    );
  };

  //Checking if the existing fields list is empty or not
  const isEmptyExistinFields = () => {
    let isEmpty = [];
    fields.forEach(e => {
      isEmpty = [...isEmpty, ...filterFields(e.fields)];
    });
    return isEmpty;
  };
  //Dialog Close and returing back to first state
  const closeDialog = () => {
    dispatchSetViewType(dispatch, "");
    clearFields(dispatch);
  };
  const isMatrixHidden = cf => {
    if (cf.fieldType == "matrix" && (feature == "issue" || feature == "task" || feature == "project")) {
      return true;
    }
    return false;
  };
  return (
    <>
      <Grid container>
        <Grid item xs={12} classes={{ item: classes.headCnt }}>
          <Scrollbars autoHide={true} autoHeight autoHeightMin={0} autoHeightMax={470}>
            {isEmptyExistinFields().length > 0 ? (
              fields.map(e => {
                if (e.fieldType === "section" || isMatrixHidden(e)) return;
                let filteredFields = filterFields(e.fields);
                return (
                  filteredFields.length > 0 && (
                    <div className={classes.mainContainer}>
                      <div
                        className={classes.existingFieldCnt}
                        onClick={() => handleClickExistingField(e)}>
                        <div className={classes.existingFieldBaseCnt}>
                          {renderExistingFieldIcon(e, e.fieldType)}
                          <span className={classes.existingFieldTitle}>
                            {`${e.fieldType} (${filteredFields.length})`}
                          </span>
                        </div>
                        <div className={classes.collapseIcons}>
                          {!e.collapse ? <ArrowDown /> : <ArrowUp />}
                        </div>
                      </div>
                      <div>
                        {e.collapse &&
                          filteredFields.map(f => {
                            let superSystemFields =
                              f.fieldName !== "Task" &&
                              f.fieldName !== "Impact" &&
                              f.fieldName !== "Likelihood";
                            return (
                              <div className={classes.existingFieldChildCnt}>
                                <span
                                  className={classes.existingFieldChildTitle}
                                  title={f.fieldName}>
                                  {f.fieldName}
                                </span>
                                <div
                                  className={
                                    selectedFieldObj.fieldId === f.fieldId
                                      ? classes.btnContainerVisible
                                      : classes.btnContainer
                                  }>
                                  {isAllowAdd && superSystemFields && !isMatrixHidden(f) && (
                                    <span
                                      className={classes.editbtn}
                                      onClick={() => {
                                        handleEditCopyField(f, "copy");
                                      }}>
                                      <SvgIcon
                                        className={classes.editFieldIcon}
                                        color={theme.palette.icon.gray600}
                                        viewBox="0 0 15 15">
                                        <CopyIcon />
                                      </SvgIcon>
                                      Copy
                                    </span>
                                  )}
                                  {!f.isSystem &&
                                    isAllowDelete &&
                                    superSystemFields &&
                                    !isMatrixHidden(f) && (
                                      <span
                                        className={classes.deletebtn}
                                        onClick={() => {
                                          handleDeleteField(f);
                                        }}>
                                        <SvgIcon
                                          className={classes.deleteFieldIcon}
                                          viewBox="0 0 14 14.005">
                                          <DeleteIcon />
                                        </SvgIcon>
                                        Delete
                                      </span>
                                    )}
                                  {isUseField && superSystemFields && !isMatrixHidden(f) && (
                                    <span
                                      className={classes.defaultChipsTag}
                                      onClick={() => {
                                        handleClickUseField(f);
                                      }}>
                                      Use this field
                                    </span>
                                  )}
                                </div>
                              </div>
                            );
                          })}
                      </div>
                    </div>
                  )
                );
              })
            ) : (
              <div className={classes.emptyFieldCnt}>
                <span> No Exisiting Fields</span>
              </div>
            )}
          </Scrollbars>
        </Grid>
      </Grid>

      {deleteDialog ? (
        <DeleteConfirmDialog
          open={deleteDialog}
          closeAction={handleDeleteDialogClose}
          cancelBtnText={
            <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
          }
          successBtnText={
            <FormattedMessage
              id="common.action.delete.confirmation.delete-button.label"
              defaultMessage="Delete"
            />
          }
          alignment="center"
          headingText={
            <FormattedMessage
              id="common.action.delete.confirmation.delete-button.label"
              defaultMessage="Delete"
            />
          }
          successAction={handleDelete}
          msgText={"Are you sure you want to delete this field?"}
          btnQuery={btnQuery}
        />
      ) : null}
    </>
  );
}

ExistingField.defaultProps = {
  /** default props for the component  */
  classes: {},
  theme: {},
};

const mapStateToProps = state => {
  return {
    customFields: state.customFields,
    profile: state.profile.data,
  };
};

export default compose(
  injectIntl,
  withStyles(CustomFieldsOptionsStyle, { withTheme: true }),
  connect(mapStateToProps, { editCustomField, dispatchDeleteField })
)(ExistingField);
