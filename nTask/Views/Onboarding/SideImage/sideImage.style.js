
const sideImageStyle = theme => ({
  artImgCnt : {
    position: 'absolute',
    right: 0,
    top: 0,
    bottom: 0,
    height: '100%',
    width: 309
  },
  artImage: {
    height: "100%",
    position: "fixed",
    zIndex: 11,
    right:0,
    [theme.breakpoints.down('sm')]: {
      display: "none",
    },
  },

});

export default sideImageStyle;
