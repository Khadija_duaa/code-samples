import withStyles from "@material-ui/core/styles/withStyles";
import React, { useEffect, useState } from "react";
import sideImageStyle from "./sideImage.style";
import ArtImage from "../../../assets/images/artwork_mask.svg";
import ArtWork from '../../../assets/images/signUpTopPatternRepeat.svg'
import constants from "../../../redux/constants/types";
import isEmpty from "lodash/isEmpty";
import axios from "axios";
import { useDispatch } from "react-redux";
import { PopulateWhiteLabelInfo } from "../../../redux/actions/teamMembers";
const SideImageView = ({ classes }) => {
  const [showLogo, setShowLogo] = useState(false)
  const [companyData, setCompanyData] = useState({})
  const dispatchFn = useDispatch();
  useEffect(() => {
    // if (window.location.host == 'app.totalenvironment.com' || window.location.host == 'uat.naxxa.io' || window.location.host == 'sqa.naxxa.io' || window.location.host == 'dev.naxxa.io' || window.location.host == 'localhost:3001') {
    //   getCompanyData('totalenvironment');
    // } else {
    //   setShowLogo(true);
    // }
    getCompanyDataWithUrl()
  }, []);

  const getCompanyData = (companyName, isAfterSuccess = false) => {
    setShowLogo(false);
    axios.get(`${constants.WHITELABELURL}?brandUrl=${companyName}`)
      .then(response => {
        if (response.data.success) {
          setCompanyData(response.data.data);
          dispatchFn(PopulateWhiteLabelInfo(response.data.data));
        } else {
          setCompanyData({})
        }
        setShowLogo(true);
        localStorage.removeItem("companyName");
      })
      .catch(err => {
        setCompanyData({})
        setShowLogo(true);
        localStorage.removeItem("companyName");
      });
  }; 
  const getCompanyDataWithUrl = () => {
    setShowLogo(false);
    const data = { "domain": window.location.origin }
    axios.post(constants.WHITELABELURL, data)
      .then(response => {
        if (response.data.success) {
          setCompanyData(response.data.data);
          dispatchFn(PopulateWhiteLabelInfo(response.data.data));
        } else {
          setCompanyData({})
        }
        setShowLogo(true);
        localStorage.removeItem("companyName");
      }).catch(err => {
        setCompanyData({})
        setShowLogo(true);
        localStorage.removeItem("companyName");
      });
  }
  return (
    <>
      <div className={classes.artImgCnt}>
        {showLogo && <img
          src={!isEmpty(companyData) ? companyData.onboardingArtworkUrl : ArtImage} alt="" className={classes.artImage} />}
        {/* <img src={ArtImage} alt="" className={classes.artImage} /> */}
      </div>

    </>
  );
};
export default withStyles(sideImageStyle, { withTheme: true })(SideImageView);
