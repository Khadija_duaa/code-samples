const onBoardStyles = theme => ({
  dialogPaperCnt: {
    overflowY: "visible",
    background: theme.palette.common.white,
    maxWidth: 780,
  },
  mobileDialogPaperCnt: {
    overflowY: "visible",
    background: theme.palette.common.white,
    // maxWidth: 375,
    maxWidth: "100%",
    height: "100%",
    margin: "0!important",
    padding: "0!important",
  },
  defaultDialogContent: {
    padding: "0",
    overflowY: "visible",

    "&:first-child": {
      padding: 0,
    },
  },
  onBoardLeftCnt: {
    background: "linear-gradient(135deg, #4daef9 0%,#026fc0 100%)",
    padding: "34px 25px",
    borderRadius: "4px 0 0 4px",
  },
  onBoardLeftContentCnt: {
    display: "flex",
    flexDirection: "column",
    height: "100%",
  },
  nTaskLogo: {
    width: 80,
    height: "auto",
    marginBottom: 18,
  },
  onBoardLeftSliderCnt: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-end",
  },
  heading: {
    
    fontWeight: 700,
  },
  headingTagline: {
    
    fontWeight: 400,
    fontSize: "13px !important",
  },
  headingTagline2: {
    
    fontWeight: 400,
    fontSize: "12px !important",
  },
  leftHeading: {
    color: theme.palette.common.white,
    marginBottom: 15,
    
    fontWeight: 500,
    fontSize: "23px !important",
    lineHeight: "30px",
  },
  leftHeadingTagline: {
    color: theme.palette.common.white,
    
    fontWeight: 400,
    fontSize: "14px !important",
  },
  rightHeading: {
    color: theme.palette.text.primary,
    marginBottom: 20,
    
  },
  rightHeadingTagline: {
    color: theme.palette.text.light,
    
    fontWeight: 500,
  },
  createProfileCnt: {
    padding: "40px 65px",
    textAlign: "center",
  },
  mobileInviteToTeamCnt: {
    padding: 0,
    textAlign: "center",
    minHeight: 500,
    display: "flex",
    flexDirection: "column",
  },
  InviteToTeamCnt: {
    padding: "40px 65px",
    textAlign: "center",
    minHeight: 500,
    display: "flex",
    flexDirection: "column",
  },
  inviteToTeamContentCnt: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    flex: 1,
  },
  createProfileHeadingCnt: {
    marginBottom: 20,
  },
  slider: {
    background: "transparent",
    padding: "8px 0",
  },
  sliderHeading: {
    color: theme.palette.common.white,
    
    fontWeight: 400,
    marginBottom: 8,
  },
  sliderTagLine: {
    color: theme.palette.common.white,
    
    fontWeight: 400,
    fontSize: "14px !important",
  },
  sliderDot: {
    background: theme.palette.common.white,
    opacity: 0.5,
  },
  activeDot: {
    background: theme.palette.common.white,
    opacity: 1,
  },
  termsLink: {
    color: theme.palette.text.azure,
    textDecoration: "underline",
  },
  actionBtnCnt: {
    display: "flex",
    justifyContent: "flex-end",
    marginTop: 25,
  },
  passwordVisibilityBtn: {
    padding: 0,
    "&:hover": {
      background: "transparent",
    },
  },
  stepsIconCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 5,
  },
  activeStepIcon: {
    fontSize: "14px !important",
  },
  inactiveStepIcon: {
    width: 8,
    height: 8,
    background: theme.palette.secondary.light,
    borderRadius: "50%",
  },
  inactiveDoneStepIcon: {
    width: 8,
    height: 8,
    background: theme.palette.secondary.main,
    borderRadius: "50%",
  },
  actionBtnCntStep2: {
    display: "flex",
    justifyContent: "space-between",
  },
  container: {
    // maxWidth: 480px;
    margin: "0 auto",
    padding: 10,
  },
  imgCnt: {},
  imageStyle: {
    textAlign: "center",
    objectFit: "contain",
    objectFit: "contain",
    height: "auto",
  },
  text_gray: {
    margintop: 10,
    marginBottom: 0,
    color: "#7E7E7E",
    fontSize: "15px !important",
    
    maxWidth: 310,
  },
  controls: {
    display: "flex",
    flexFlow: "column",
    justifyContent: "center",
    alignItems: "center",
    flexWrap: "wrap",
    textAlign: "center",
  },
  h4Style: {
    marginTop: 40,
    marginBottom: 0,
    fontSize: "20px !important",
    color: "#333333",
    
  },
  app_button: {
    marginTop: 40,
  },
});

export default onBoardStyles;
