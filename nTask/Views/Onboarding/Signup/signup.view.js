import React, { useEffect, useState } from "react";
import OnBoardingHeaderView from '../OnboardingHeader/onboardingHeader.view'
import CustomButton from "../../../components/Buttons/CustomButton";
import { withStyles } from "@material-ui/core/styles";
import signUpStyle from './signup.style';
import SideImageView from '../SideImage/SideImage.view';
import GoogleIcon from "../../../components/Icons/GoogleIcon"
import NewFacebookIcon from "../../../components/Icons/NewFacebookIcon"
import DefaultTextField from "../../../components/Form/TextField";
import VerifyEmailView from '../VerifyEmail/verifyEmail.view';
import TeamFeaturesView from '../OnboardingHeader/TeamFeature/teamFeatures.view';
import WorkCmpView from '../WorkCmp/WorkCmp.View';
import { SignUpRequest } from "../../../../src/redux/actions/signUp";
import { Link, withRouter } from "react-router-dom";
import isEmpty from "lodash/isEmpty";
import { validateSignUpForm } from "./formSchema";
import './signup.css'
import { isMobileDevice } from "../../../utils/common";
import constants from "../../../redux/constants/types";
import SocialLoginButtons from "../../../components/SocialLoginButtons/SocialLoginButtons";
import { compose } from "redux";
import Hotkeys from "react-hot-keys";
import queryString from "query-string";
import mixpanel from 'mixpanel-browser';
import { MixPanelEvents } from '../../../mixpanel';

function SignUp({ classes, history, location, enqueueSnackbar }) {
    // add onchange function and state 
    const [email, setEmail] = useState("");
    const [emailError, setEmailError] = useState('')
    const [verifyEmail, setVerifyEmail] = useState(false);
    const [signUpBtnQuery, setSignUpBtnQuery] = useState('');
    useEffect(() => {
      let parseUrl = queryString.parseUrl(location.search);
      if (parseUrl.query?.email){
        setEmail(parseUrl.query.email)
    }
        mixpanel.time_event(MixPanelEvents.SignupEvent);
    }, [])
    const handleInput = (e, value) => {
        setEmailError('')
        setEmail(e.target.value)
    }
    const handleClickSignup = (e) => {
        validateSignUpForm().validate({email}, {abortEarly: false}).then(() => {
          let parseUrl = queryString.parseUrl(location.search);
          let selectedPlan = parseUrl?.query?.plan || 'free'
        setSignUpBtnQuery('progress')
        SignUpRequest(email, selectedPlan,
            //Success
            (res) => {
                mixpanel.track(MixPanelEvents.SignupEvent,{email, selectedPlan});
                setSignUpBtnQuery('');
                setVerifyEmail(true);
            }, (err) => {
            setEmailError(err.message);
              setSignUpBtnQuery('')
            })
        }).catch(err => {
            const errors = err.inner.reduce((r, cv) => {
                r[cv.path] = cv.message;
                return r;
            }, {});
            setEmailError(errors.email);
        });

    }
    const navigateToDuplicateExtAcc = () => {
        history.push("/account/duplicateaccount");
    };
    const onKeyDown = (keyName, e, handle) => {
        if (keyName === "enter") {
            handleClickSignup()
        }
    };
    const handleInputKeyDown = (e) => {
        if (e.keyCode == 13) {
            handleClickSignup()
        }
    }
    const showSnackBar = (snackBarMessage, type) => {
        enqueueSnackbar(
            <div className={classes.snackBarHeadingCnt}>
                <p className={classes.snackBarContent}>{snackBarMessage}</p>
            </div>,
            {
                anchorOrigin: {
                    vertical: "bottom",
                    horizontal: "right",
                },
                variant: type ? type : "info",
            }
        );
    };

    // // click resend email and come back to the signup form
    // const resendSignUpEmail  = () =>{
    //     setVerifyEmail(false);
    // }

    const isValidEmail = validateSignUpForm().isValidSync({ email }, { abortEarly: false });
    return (
        <Hotkeys keyName="enter" onKeyDown={onKeyDown}>
            <OnBoardingHeaderView hideStepper={true} hideBackButton={true} />
            {verifyEmail ?
                <VerifyEmailView email={email} /> :
                <>

                    <div className={classes.signUpForm}>
                        <div className={classes.signUpHeading}>
                            <h1>Sign Up for Free</h1>
                        </div>
                        <div className={classes.signUpEmail}>
                            <DefaultTextField
                                label="Your Email Address"
                                fullWidth={true}
                                inputType='underline'
                                labelProps={{
                                    classes: {
                                        root: classes.profileInputFieldLabel,
                                    },
                                }}
                                errorState={!isEmpty(emailError)}
                                errorMessage={emailError}
                                defaultProps={{
                                    id: "emailInput",
                                    placeholder: "name@company.com",
                                    onChange: (event) => handleInput(event, "email"),
                                    onKeyDown: handleInputKeyDown,
                                    value: email,
                                    inputProps: { maxLength: 250, tabIndex: 1 },
                                    // disabled: parsedUrl.email ? true : false,
                                }}
                            />
                        </div>
                        <div className={classes.signUpTC}>
                            <label>By signing up, I agree to the nTask <a href="https://www.ntaskmanager.com/privacy-policy" target="_blank" className={classes.inputFieldLinks}>Privacy Policy</a> and <a href="https://www.ntaskmanager.com/terms-conditions" target="_blank" className={classes.inputFieldLinks}>Terms of Service.</a></label>
                            <CustomButton
                                className={classes.signUpTCBtn}
                                btnType='blue'
                                variant='contained'
                                query={signUpBtnQuery}
                                disabled={signUpBtnQuery == 'progress' || !isValidEmail}
                                onClick={handleClickSignup}>
                                Sign Up for Free
                            </CustomButton>
                        </div>
                        <div className={classes.signUpwith}>
                            <div className={classes.signUpStraightLine1}></div>
                            <label>or sign up with</label>
                            <div className={classes.signUpStraightLine2}></div>
                        </div>
                        <SocialLoginButtons
                            duplicateAccount={navigateToDuplicateExtAcc}
                            showSnackBar={showSnackBar}
                        />

                    </div>

                    <div className={classes.signUpFooter}>
                        <label>Already have an account?   <Link to="/account/login">Sign In</Link> instead.</label>
                    </div>
                    <SideImageView />
                </>}
        </Hotkeys>
    )
}

export default compose(withRouter, withStyles((signUpStyle), { withTheme: true }))(SignUp);