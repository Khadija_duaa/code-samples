const signUpStyle = theme => ({

    signUpForm: {
        width: "430px",
        marginTop: 125,
        marginLeft: 119,
        '& *': {
            fontFamily: theme.typography.fontFamilyLato,
        },
        [theme.breakpoints.between('xs', 'sm')]: {
            width: "90%",
            margin: "0 auto",
            marginTop: 150,
        },
        [theme.breakpoints.between('sm', 'md')]: {
            width: 600,
            marginTop: 125
        },


    },
    snackBarHeadingCnt: {
        marginLeft: 10
    },
    snackBarContent: {
        margin: 0,
        fontSize: "12px !important",
    },
    signUpHeading: {
        "& h1": {
            fontSize: "28px !important",
            color: theme.palette.common.black,
            fontWeight: 600,
            fontFamily: 'poppins, sans-serif !important'
        },
    },
    signUpEmail: {
        display: "flex",
        flexDirection: " column",
        marginTop: "32px",
        [theme.breakpoints.down('sm')]: {
            marginTop: 26,
        },
    },
    signUpTC: {
        marginBottom: "19px",
        marginTop: "16px",
   
        "& label": {
            fontSize: "13px !important",
            fontFamily: theme.typography.fontFamilyLato,
            fontWeight: theme.typography.fontWeightRegular,
            color: theme.palette.text.light,


            "& a ": {
                fontSize: "13px !important",
                fontFamily: theme.typography.fontFamilyLato,
                fontWeight: theme.typography.fontWeightRegular,
                color: theme.palette.common.black,
            }
        },
    },
    signUpTCBtn: {
        width: "100%",
        padding: "12px",
        borderRadius: " 6px",
        marginTop: "23px",
        fontSize: "14px !important",
        fontFamily: theme.typography.fontFamilyLato,
        textTransform: "capitalize",
        fontWeight: theme.typography.fontWeightExtraLight,
        [theme.breakpoints.down('sm')]: {
            marginBottom: 14,
            marginTop: 34
        },

    },
    signUpwith: {
        display: "flex",
        alignItems: "center",
        width: "100%",
        justifyContent: "center",
        marginBottom: 20,
        [theme.breakpoints.down('sm')]: {
            marginTop: 26,
        },
        "& label": {
            fontSize: "14px !important",
            fontFamily: theme.typography.fontFamilyLato,
            fontWeight: theme.typography.fontWeightMedium,
            color: theme.palette.text.medGray,
            marginLeft: " 10px",

            width: "100px",

            [theme.breakpoints.down('sm')]: {
                width: 100,
                marginRight: 0,
            },
        },
    },
    signUpStraightLine1: {
        flex: 1,
        background: theme.palette.background.contrast,
        height: "1px",

    },
    signUpStraightLine2: {
        flex: 1,
        background: theme.palette.background.contrast,
        height: "1px",

    },

    signUpButton: {
        marginTop: '16px',
        display: "flex",
        width: " 100%",
        [theme.breakpoints.down('sm')]: {
            width: "100%",
            flexDirection: "column",
        }
    },
    signUpInnerBtn1: {
        width: "100%",
        padding: "12px 0",
        background: theme.palette.background.default,
        border: "1px solid #DDDDDD",
        borderRadius: "4px",
        fontSize: "14px !important",
        fontFamily: theme.typography.fontFamilyLato,
        fontWeight: theme.typography.fontWeightMedium,
        color: theme.palette.common.black,
        marginRight: "10px",
        textTransform: "capitalize",

        "&:hover": {
            background: theme.palette.background.default,
            boxShadow: '0px 2px 4px #00000014'
        },


    },
    signUpInnerBtn2: {
        width: "100%",
        padding: "12px 0",
        background: theme.palette.common.white,
        border: "1px solid #DDDDDD",
        borderRadius: "4px",
        fontSize: "14px !important",
        fontFamily: theme.typography.fontFamilyLato,
        fontWeight: theme.typography.fontWeightMedium,
        color: theme.palette.common.black,
        textTransform: "capitalize",

        "&:hover": {
            background: theme.palette.common.white,
        },
    },
    signUpFooter: {
        marginTop: 'calc(100vh - 530px)',
        marginLeft: "119px",
        [theme.breakpoints.between('xs', 'sm')]: {
            width: "44%",
            margin: "50px auto 0 auto",
            textAlign: "center"
        },

        "& label": {

            fontSize: "14px !important",
            fontFamily: theme.typography.fontFamilyLato,
            color: theme.palette.common.black,
            fontWeight: theme.typography.fontWeightMedium,
            "& a": {
                fontSize: "14px !important",
                fontFamily: theme.typography.fontFamilyLato,
                fontWeight: theme.typography.fontWeightMedium,
                color: theme.palette.text.linkBlue,
            },
        },
    },
    signUpIcon: {
        fontSize: "20px !important",

    },

    profileInputFieldLabel: {
        display: "flex",
        transform: "translate(2px, -17px) scale(1)",
        fontSize: "16px !important",
        alignItems: "center",
        fontWeight: 400,
        fontFamily: 'poppins, sans-serif !important',
        lineHeight: "5px",
        color: theme.palette.common.black
    },
    btnGrid: {
        marginTop: 16
    },
})

export default signUpStyle;