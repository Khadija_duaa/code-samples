import * as yup from "yup";
import { validateEmail } from "../../../utils/validator/common/email";


export function validateSignUpForm(intl) {
  return yup.object({
    email: validateEmail()
  })
}