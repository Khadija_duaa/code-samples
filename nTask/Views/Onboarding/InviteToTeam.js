// @flow
import React, { useState, Fragment } from "react";
import Grid from "@material-ui/core/Grid";
import onBoardStyles from "./style";
import { withStyles } from "@material-ui/core/styles";
import DefaultTextField from "../../components/Form/TextField";
import DefaultCheckbox from "../../components/Form/Checkbox";
import InviteMember from "../../components/InviteMember/InviteMember";
import { validateEmailAddress } from "../../redux/actions/teamMembers";
import CustomButton from "../../components/Buttons/CustomButton";
import Typography from "@material-ui/core/Typography";
import InputAdornment from "@material-ui/core/InputAdornment";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import IconButton from "@material-ui/core/IconButton";
import { validName } from "../../utils/validator/common/fullName";
import { validPassword } from "../../utils/validator/common/password";
import { validTeamName } from "../../utils/validator/team/teamName";
import { isEmpty, trim, isLength } from "validator";
import RadioChecked from "@material-ui/icons/RadioButtonChecked";
import RadioUnChecked from "@material-ui/icons/RadioButtonUnchecked";
import { compose } from "redux";
import { connect } from "react-redux";
import {
  createProfile,
  createSocialProfile,
} from "../../redux/actions/onboarding";
import queryString from "query-string";
import { withRouter } from "react-router-dom";
import { FetchUserInfo } from "../../redux/actions/profile";
import { withSnackbar } from "notistack";
import MobileDevice from "./MobileDevice";
import { isMobileDevice } from "../../utils/common";
let moment = require("moment-timezone");

type InviteToTeamProps = {
  classes: Object,
  theme: Object,
  validateEmailAddress: Function,
  setDialogOpen: Function,
  profileData: Function,
  location: Object,
  setOnBoardStep: Function,
  FetchUserInfo: Function,
  createProfile: Function,
  history: Object,
  enqueueSnackbar: Function,
};

//Invite to team Component
function InviteToTeam(props: InviteToTeamProps) {
  const [emailList, setEmailList] = useState([]);
  const [mobileDevice, setMobileDevice] = useState(false);
  const [btnQuery, setBtnQuery] = useState("");
  (btnQuery: "" | "progress");
  (emailList: Array<string>);

  const updateEmailList = (emails: Array<string>, currentEmail: string) => {
    // Update email list in state
    setEmailList(emails);
  };

  const showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  // When user click on get Started button
  const onGetStarted = () => {
    setBtnQuery("progress");
    const {
      profileData,
      setDialogOpen,
      location,
      FetchUserInfo,
      createProfile,
      createSocialProfile,
      handleGoogleAnalytics,
    } = props;
    const parsedUrl = queryString.parse(location.search);
    const token = localStorage.getItem("tempToken");
    const email = parsedUrl.email;

    const data = {
      Invitee: emailList,
      email: email ? email : profileData.email,
      FullName: profileData.fullName,
      teamName: profileData.teamName,
      password: profileData.password,
      code: !parsedUrl.isExternal ? parsedUrl.code : "",
      invitedby: parsedUrl.invitedby,
      InvitedteamId: parsedUrl.invitedteamId,
      plan: parsedUrl.plan,
      TimeZone: moment.tz.guess(),
    };
    if (parsedUrl.isExternal) {
      createSocialProfile(
        data,
        token,
        () => {
          setBtnQuery("");
          // handleGoogleAnalytics();
          // props.history.push(`/teams/`);
          // setDialogOpen(false); // Closing Onboarding Dialog
          let is_mobile = isMobileDevice()
          if(is_mobile){
            setMobileDevice(true);
          } else {
            handleGoogleAnalytics();
            props.history.push(`/teams/`);
            setDialogOpen(false); // Closing Onboarding Dialog
          }
        },
        (response) => {
          setBtnQuery("");

          showSnackBar(response.data.message, "error");
        }
      );
    } else {
      createProfile(
        data,
        () => {
          setBtnQuery("");
          // handleGoogleAnalytics();
          // props.history.push(`/teams/`);
          // setDialogOpen(false); // Closing Onboarding Dialog
          let is_mobile = isMobileDevice()
          if(is_mobile){
            setMobileDevice(true);
          } else {
            handleGoogleAnalytics();
            props.history.push(`/teams/`);
            setDialogOpen(false); // Closing Onboarding Dialog
          }
        },
        (response) => {
          setBtnQuery("");
          showSnackBar(response.data.message, "error");
        }
      );
    }
  };
  const webAppHandler = () => {
    setMobileDevice(false);
    handleGoogleAnalytics();
    props.history.push(`/teams/`);
    setDialogOpen(false); // Closing Onboarding Dialog
  }
  const onBack = () => {
    // When user click on back button
    const { setOnBoardStep } = props;
    setOnBoardStep(0); // Going back to first step
  };
  const { classes, theme, profileData } = props;

  return (
    <div className={mobileDevice ? classes.mobileInviteToTeamCnt: classes.InviteToTeamCnt}>
      {mobileDevice ? 
        <MobileDevice
          GoToWebApp = {webAppHandler}
        ></MobileDevice>:
        <Fragment>

          <div className={classes.createProfileHeadingCnt}>
            <Typography variant="h1" className={classes.heading}>
              You're almost done
            </Typography>
            <Typography variant="body2" className={classes.headingTagline}>
              Invite a few team members to your team.
              <br />{" "}
              <span className={classes.headingTagline2}>
                (You can always do it later if you want)
              </span>
            </Typography>
            <div className={classes.stepsIconCnt}>
              <span className={classes.inactiveDoneStepIcon}></span>
              <RadioChecked
                className={classes.activeStepIcon}
                htmlColor={theme.palette.secondary.main}
              />
            </div>
          </div>
          <div className={classes.inviteToTeamContentCnt}>
            <InviteMember
              emailList={emailList}
              updateEmailList={updateEmailList}
              placeholder="Enter email address"
              invitationLimit={4}
              label="Invite Team Members (optional)"
              ignoreEmails={[profileData.email]}
              // CheckMemberAlreadyExist={false}
            />
            <div className={classes.actionBtnCntStep2}>
              <CustomButton onClick={onBack} btnType="plain" variant="text">
                <u>Go Back</u>
              </CustomButton>
              <CustomButton
                onClick={onGetStarted}
                btnType="blue"
                variant="contained"
                query={btnQuery}
                disabled={btnQuery == "progress"}
              >
                Let's Get Started
              </CustomButton>
            </div>
          </div>
        </Fragment>
      }
    </div>
  );
}

export default compose(
  withRouter,
  withSnackbar,
  connect(null, {
    validateEmailAddress,
    FetchUserInfo,
    createProfile,
    createSocialProfile,
  }),
  withStyles(onBoardStyles, { withTheme: true })
)(InviteToTeam);
