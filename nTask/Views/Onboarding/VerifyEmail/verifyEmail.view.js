import withStyles from "@material-ui/core/styles/withStyles";
import React, { useEffect, useState } from 'react'
import VerifyEmailStyle from './verifyEmail.style'
import emailLogo from "../../../assets/images/illustration_Email.svg"
import exclimationMark from "../../../assets/images/Iconmaterial-info.svg"
import { SignUpRequest } from "../../../../src/redux/actions/signUp";
function VerifyEmailView({ classes, email}) {

    const [emailcounter, setEmailCounter] = useState(59);

    useEffect(() => {
        const updateEmailCounter = setInterval(() => {
            setEmailCounter(preCount => preCount == 0 ? 0 : preCount - 1);
        }, 1000);
        return () => {
            clearInterval(updateEmailCounter)
        }
    }, [])
    const handleResendEmail = (e) => {
        e.stopPropagation();
        SignUpRequest(email, "free",
            //Success
            () => {
              setInterval(() => {
                    setEmailCounter(preCount => preCount == 0 ? 59 : preCount);
                }, 1000);
            },
             (err) => {

            })
    }
    return (
        <>
            {/* <header>
                <div className={classes.verifyEmailnTaskLogo}>
                    <img src={nTaskLogo} alt="" />
                </div>
            </header> */}

            <div className={classes.verifyEmailSection}>
                <div className={classes.verifyEmailLogo}>
                    <img src={emailLogo} alt="" />
                </div>
                <div className={classes.verifyEmailInnerSection}>
                    <div className={classes.verifyEmailContent}>
                        <h1>Verify your email</h1>
                        <p>Kindly check your inbox in order to verify your account and continue to nTask.</p>
                        <div className={classes.verifyEmailText}>
                            <span><img src={exclimationMark} alt="" /> Check your spam/junk folder if you don't see the email in your inbox.</span>
                            <label>Didn't receive verification email?
                                {emailcounter !== 0 && <b> Resend Email in 00:{emailcounter < 10 ? "0" + `${emailcounter}` : `${emailcounter}`}</b>}
                                {emailcounter == 0 && <b className={classes.resendEmail} onClick={handleResendEmail}> Resend Email </b> }
                               
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default withStyles((VerifyEmailStyle), { withTheme: true })(VerifyEmailView)



