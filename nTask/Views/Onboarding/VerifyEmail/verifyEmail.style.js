const VerifyEmailStyle = theme => ({
    verifyEmailSection: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        margin: "0 auto",
        width: "484px",
        marginTop: 125,
        [theme.breakpoints.down('sm')]: {
        width:"98%",
        margin:"0 auto",
        marginTop:200,
        },
        '& *': {
            fontFamily: theme.typography.fontFamilyLato
        }
    },
    verifyEmailContent: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        "& h1": {
            // [theme.breakpoints.down('sm')]: {
            //     width:"100%",
            //     },
            marginTop: "24px",
            marginBottom: "12px",
            fontSize: "28px",
            
            fontWeight: theme.typography.fontWeightLarge,
            color: theme.palette.common.black,
        },

        "& p": {
            fontSize: "16px",
            
            fontWeight: theme.typography.fontWeightRegular,
            color: theme.palette.secondary.dark,
            textAlign: "center",
            lineHeight: "22px",
        },


    },

    verifyEmailText: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        marginTop: "20px",
        position: "relative",


        "& span": {
            marginBottom: "24px",
            background: " rgba(51, 51, 51, 0.1)",
            fontSize: "13px !important",
            
            fontWeight: theme.typography.fontWeightRegular,
            color: theme.palette.secondary.dark,
            padding: "8px 10px",
            borderRadius: "6px",
            display: "flex",
            alignItems: "center",
            [theme.breakpoints.down('sm')]: {
                width:"100%",
                padding:13,
                marginBottom:119,
                },
            "& img": {
                marginRight: "8px",
            },
        },

        "& label": {
            fontSize: "16px",
            
            fontWeight: theme.typography.fontWeightRegular,
            color: theme.palette.secondary.dark,
            [theme.breakpoints.down('sm')]: {
                width:"76%",
                textAlign:"center",
                lineHeight:"26px",
                },
        },
        "& b": {
            fontSize: "16px",
            
            fontWeight: theme.typography.fontWeightLarge,
            color: theme.palette.secondary.dark,
        }
    },

    verifyEmailnTaskLogo: {
        padding: '30px',
        marginBottom: "40px",
        "& img": {
            width: " 120px",
        },
    },
    resendEmail: {
        fontSize: "16px",
        fontFamily: theme.typography.fontFamilyLato,
        fontWeight: theme.typography.fontWeightLarge,
        color: "#0086E6 !important",
        textDecoration: "underline",
        // textDecorationThickness: "2px",
        cursor: "pointer"
    }
})
export default VerifyEmailStyle;