import {INCREMENT, DECREMENT} from './constant'
import {toast} from 'react-toastify';

export const incrementAction = (obj)=>{
    return ({type:INCREMENT, payload:obj})
}
export const decreaseAction = (obj)=>{
    return ({type:DECREMENT, payload:obj})
}