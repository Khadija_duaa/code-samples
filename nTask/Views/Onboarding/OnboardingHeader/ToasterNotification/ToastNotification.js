import React from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
function ToastNotification() {
  const toastId = React.useRef(null);
  const notify = () => {
    const id = toast.loading("Please wait...");
    setTimeout(() => {
      toast.update(id, {
        render: "All is good",
        type: "success",
        isLoading: false,
        autoClose: 2000,
        hideProgressBar: false,
        pauseOnHover: true,
        closeOnClick: true,
        draggable: true,
      });
    }, 2000);
  };
  const dismiss = () => toast.dismiss(toastId.current);
  const dismissAll = () => toast.dismiss();
  return (
    <div style={{ marginTop: 200 }}>
      <button onClick={notify}>Notify</button>
      <button onClick={dismiss}>Dismiss</button>
      <button onClick={dismissAll}>Dismiss All</button>
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={true}
        newestOnTop
        closeOnClick
        rtl={false}
        pauseOnFocusLoss={false}
        draggable
        pauseOnHover
      />
    </div>
  );
}

export default ToastNotification;
