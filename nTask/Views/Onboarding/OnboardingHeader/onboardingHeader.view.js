import React, { useContext, useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import onboardingHeaderStyle from './onboardingHeader.style';
import nTaskLogo from '../../../assets/images/nTask-Logo-Black.svg'
import StepperCmp from '../Stepper/stepper.cmp';
import CustomIconButton from "../../../components/Buttons/CustomIconButton";
import IconBlackButton from '../../../components/Icons/IconBlackButton';
import OnBoadingContext from "../Context/OnBoarding.context"
import { useSelector } from "react-redux";
const OnBoardingHeaderView = (props) => {
    const {
        classes,
        hideCloseButton,
        hideStepper,
        hideBackButton,
        onBackButtonClick
    } = props;
    const { state = {} } = useContext(OnBoadingContext);
    const { auth } = useSelector((state) => ({
        auth: state.authenticationResponse.authState,
    }));
    const {whiteLabelingData, step} = state
    // const {state: { step, },dispatch, } = useContext(OnBoadingContext);
    const brandLogo = whiteLabelingData && whiteLabelingData.signinLogoUrl
    return (
        <div className={classes.headerCnt}>
            <header className={classes.headerFixed}>
                <div className={classes.signUpHeaderCnt}>

                    <div className={classes.signUpLogo}>
                        <div className={classes.logoInnerCnt}>
                        {hideBackButton || (step == "profile" || (auth && step == "payment") || (auth && step == "work")) ? null :<div className={classes.boardingHeaderBlackBtn}>
                            <CustomIconButton onClick={onBackButtonClick}>
                                <IconBlackButton />
                            </CustomIconButton>
                        </div>}
                        <img src={brandLogo ? brandLogo : nTaskLogo} alt="" />
                        </div>
                        {hideStepper ? null : <div className={classes.settperCmpList}>
                            <StepperCmp />
                        </div>}
                    </div>
                </div>
                <div className={classes.artWorkTopImage} ></div>
            </header>

        </div>
    )
}
OnBoardingHeaderView.defaultProps = {
    hideCloseButton: false,
    hideStepper: false,
    hideBackButton: false
}

export default withStyles((onboardingHeaderStyle), { withTheme: true })(OnBoardingHeaderView);