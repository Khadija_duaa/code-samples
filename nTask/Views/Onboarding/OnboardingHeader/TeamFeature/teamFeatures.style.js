const teamFeaturesStyle = theme => ({
  teamFeatureCnt: {
    width: "100%",
    [theme.breakpoints.between('xs', 'sm')]: {
      width: "100%",
      marginTop: 210,
      height:"60vh",
      overflowY:"scroll",
      overflowX:"hidden",
    },
    [theme.breakpoints.between('sm', 'md')]: {
      width: "100%",
      marginTop: 210,
      overflowY:"scroll",
      overflowX:"hidden",
    },
    [theme.breakpoints.between('md', "lg")]: {
      width: "100%",
      margin: 0,
      // overflow:"hidden"
    },

  },
  teamFeatureInnerSection: {
    width: "540px",
    marginLeft: 119,
    [theme.breakpoints.between('xs', 'sm')]: {
      width: "94%",
      margin:"0 auto",
    },
    [theme.breakpoints.between('sm', 'md')]: {
     width:"571px",
     margin:"0 auto",
    },
    [theme.breakpoints.between('md', 'lg')]: {
      marginLeft:119
     },
  },
  teamFeatureHeading: {
    "& h1": {
      fontSize: "28px !important",
      fontWeight: 600,
      color: theme.palette.common.black,
    width: "95%",
    [theme.breakpoints.between('xs', 'sm')]: {
   width:"100%"
    },
    [theme.breakpoints.between('sm', 'md')]: {
      fontSize:"22px",
      lineHeight:"30px",
      // width:"66%",
    },
    },

    "& label": {
      fontSize: "16px !important",
      fontWeight: 400,
      color: theme.palette.secondary.dark,
      [theme.breakpoints.between('xs', 'sm')]: {
        fontSize:14,
        display:"none",
           },
    },
    "& span":{
      fontSize: "16px !important",
      fontWeight: 400,
      color: theme.palette.secondary.dark,
      display:"none",
      [theme.breakpoints.between('xs', 'sm')]: {
        display:"block",
        lineHeight:"25px",
           },
     }, 
    "&:hover": {
      cursor: "context-menu",
    }
  },

  teamFeatureInnerCnt: {
    display: "flex",
    marginTop: "32px",
    [theme.breakpoints.between('md', 'lg')]: {
      height:"41vh"
     },
  },
  teamFeatureListbox: {
    padding: 0,
    [theme.breakpoints.between('xs', 'sm')]: {
      marginLeft: 6,
      width:"100%",
    },

    "& li": {
      width: "266px",
      padding: "14px 18px",
      border: "1px solid rgba(221, 221, 221, 1)",
      borderRadius: "6px",
      fontSize: "14px !important",
      fontWeight: theme.typography.fontWeightMedium,
      color: theme.palette.common.black,
      display: "flex",
      alignItems: "center",
      [theme.breakpoints.between('xs', 'sm')]: {
      width:"205%",
      },
      [theme.breakpoints.between('sm', 'md')]: {
        width:470,
        },
        [theme.breakpoints.between('md', 'lg')]: {
          width:"100%"
          },
      "& span": {
        marginRight: "15px",
        fontWeight: 400
      },

      "&:hover": {
        cursor: "pointer",
      },
    },
  },
  teamFeatureInnerColumn: {

    [theme.breakpoints.between('xs', 'sm')]: {
 display:"inline-block"
    },
  },

  teamFeatureGridStyle: {
    padding: "8px !important",
  },
  teamFeatureBtn: {
    marginTop: "78px",
    marginLeft:119,
    [theme.breakpoints.between('xs', 'sm')]: {
      width:"93%",
      margin:"0 auto",
      position:"fixed",
      top:670,
      left:12,
    },
    [theme.breakpoints.between('sm', 'md')]: {
    width:570,
    margin:"0 auto",
    marginTop:20,
    position:"unset",
    },
    [theme.breakpoints.between('md', 'lg')]: {
marginLeft:115,
marginTop:78,
      },
  },
  teamFeatureInnerbtn: {
    padding: "9px 16px",
    background: theme.palette.background.btnBlue,
    fontSize: "13px !important",
    fontWeight: 400,
    color: theme.palette.common.white,
    borderRadius: "6px",
    textTransform: "capitalize",
    letterSpacing: "1px",
    [theme.breakpoints.between('xs', 'sm')]: {
      width:"100%",
      height:48,
      marginBottom:12,
      fontSize:14,
    },
    [theme.breakpoints.between('sm', 'md')]: {
      width:"83%",
      fontSize:14,
    },
    [theme.breakpoints.between('md', 'lg')]: {
      width:91,
      fontSize:13
            },
    "&:hover": {
      background: theme.palette.background.darkblue,
    },
  },
  teamFeatureInnerbtn1: {
    marginLeft: "16px",
    padding: "8px 16px",
    background: theme.palette.background.default,
    fontSize: "13px !important",
    fontWeight: 400,
    color: theme.palette.common.black,
    border: "1px solid rgba(221, 221, 221, 1)",
    borderRadius: "6px",
    textTransform: "none",
    [theme.breakpoints.between('xs', 'sm')]: {
      width:"100%",
      marginLeft:0,
      height:48,
      fontSize:14,
    },
    [theme.breakpoints.between('sm', 'md')]: {
      width:"83%",
      marginLeft:0,
      height:48,
      fontSize:14,
    },
    [theme.breakpoints.between('md', 'lg')]: {
  width:145,
  height:40,
  marginLeft:16,
  padding:"9px 16px",
            },
    "&:hover": {
      background: theme.palette.background.default,
      boxShadow: "0px 2px 4px #00000014"
    },
  },
  IssuesIcon: {
    fontSize: "18px !important",
    color: "#EF7070",
  },
  RiskIcon: {
    fontSize: "20px !important",
    color: "#F7B41C",
  },
  meetingIcon: {
    fontSize: "20px !important",
    color: "#1DB5DE",
  },
  timeSheetIcon: {
    fontSize: "20px !important",
    color: "royalblue",
  },
  icons: {
    fontSize: "20px !important",
  },

});

export default teamFeaturesStyle;
