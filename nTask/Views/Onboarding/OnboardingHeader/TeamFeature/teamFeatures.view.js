import React, { useEffect, useContext, useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import teamFeaturesStyle from "./teamFeatures.style";
import NewProjectIcon from "../../../../components/Icons/NewProjectIcon";
import NewGanttIocn from "../../../../components/Icons/NewGanttIcon";
import IssuesIcon from "../../../../components/Icons/IssueIcon";
import NewIconCustomField from "../../../../components/Icons/NewIconCustomField";
import MeetingIcon from "../../../../components/Icons/MeetingIcon";
import NewTaskIcon from "../../../../components/Icons/NewTaskIcon";
import NewKanbanIcon from "../../../../components/Icons/NewKanbanIcon";
import RiskIcon from "../../../../components/Icons/RiskIcon";
import NewCustomStatusIcon from "../../../../components/Icons/NewCustomStatusIcon";
import TimesheetIcon from "../../../../components/Icons/TimesheetIcon";
import Grid from "@material-ui/core/Grid";
import SvgIcon from "@material-ui/core/SvgIcon";
import CustomButton from "../../../../components/Buttons/CustomButton";
import ProjectImage from "../../../../../src/assets/images/TeamFeaturesImage/illustrationProjects.svg";
import TaskManagmentImage from "../../../../../src/assets/images/TeamFeaturesImage/illustration_TaskManagement.svg";
import GanttImage from "../../../../../src/assets/images/TeamFeaturesImage/illustration_Gantt.svg";
import NtaskBoardsImage from "../../../../../src/assets/images/TeamFeaturesImage/illustration_Board.svg";
import IssuesImage from "../../../../../src/assets/images/TeamFeaturesImage/illustration_Issues.svg";
import RiskManagementImage from "../../../../../src/assets/images/TeamFeaturesImage/illustration_RiskManagement.svg";
import CustomFieldImage from "../../../../../src/assets/images/TeamFeaturesImage/illustration_CustomFields.svg";
import CustomStatusImage from "../../../../../src/assets/images/TeamFeaturesImage/illustration_CustomStatus.svg";
import MeetingManagementImage from "../../../../../src/assets/images/TeamFeaturesImage/illustration_Meetings.svg";
import TimeTrackingImage from "../../../../../src/assets/images/TeamFeaturesImage/illustration_Timesheet.svg";
import OnBoadingContext from "../../Context/OnBoarding.context";
import { sendInvite } from "../../Context/actions/work.action";
import { setTeamFeatures } from "../../Context/actions/featuresModule.action";
import { nextStep } from "../../Context/actions/steps.action";
import { slideView } from "../../Context/constants";
function teamFeaturesView({ classes }) {
  const {
    state: { features, selectedFeature },
    dispatch,
  } = useContext(OnBoadingContext);
  const goToStepInvite = step => {
    dispatch(nextStep());
  };
  const handleTeamFeatures = item => {
    if(selectedFeature.length > 2 && !selectedFeature.includes(item.id)) {
        return false;
    }
      dispatch(setTeamFeatures(item.id));
    }
  const teamListRender = {
    projectmanagement: {
      icon: <NewProjectIcon className={classes.icons} />,
      img: <img src={ProjectImage} alt="" className={classes.teamFeatureImages} />,
      color: "#EFDFF9",
    },
    taskmanagement: {
      icon: <NewTaskIcon className={classes.icons} />,
      img: <img src={TaskManagmentImage} alt="" className={classes.teamFeatureImages} />,
      color: "#DDF8EE",
    },
    ganttchart: {
      icon: <NewGanttIocn className={classes.icons} />,
      img: <img src={GanttImage} alt="" className={classes.teamFeatureImages} />,
      color: "#FEEDDE",
    },
    ntaskboards: {
      icon: <NewKanbanIcon className={classes.icons} />,
      img: <img src={NtaskBoardsImage} alt="" className={classes.teamFeatureImages} />,
      color: "#DDF4FA",
    },
    issueincidentreporting: {
      icon: (
        <SvgIcon className={classes.IssuesIcon} viewBox="0 0 16 17.375">
          <IssuesIcon />
        </SvgIcon>
      ),
      img: <img src={IssuesImage} alt="" className={classes.teamFeatureImages} />,
      color: "#FDEAEA",
    },
    riskmanagement: {
      icon: (
        <SvgIcon className={classes.RiskIcon} viewBox="0 0 18 15.75">
          <RiskIcon />
        </SvgIcon>
      ),
      img: <img src={RiskManagementImage} alt="" className={classes.teamFeatureImages} />,
      color: "#FEF4DF",
    },
    customfields: {
      icon: <NewIconCustomField className={classes.icons} />,
      img: <img src={CustomFieldImage} alt="" className={classes.teamFeatureImages} />,
      color: "#DBF4F4",
    },
    customstatus: {
      icon: <NewCustomStatusIcon className={classes.icons} />,
      img: <img src={CustomStatusImage} alt="" className={classes.teamFeatureImages} />,
      color: "#FAE3EF",
    },
    meetingmanagement: {
      icon: (
        <SvgIcon className={classes.meetingIcon} viewBox="0 0 17.031 17">
          <MeetingIcon />
        </SvgIcon>
      ),
      img: <img src={MeetingManagementImage} alt="" className={classes.teamFeatureImages} />,
      color: "#DDF4FA",
    },
    timesheet: {
      icon: (
        <SvgIcon className={classes.timeSheetIcon} viewBox="0 0 17 19.625">
          <TimesheetIcon />
        </SvgIcon>
      ),
      img: <img src={TimeTrackingImage} alt="" className={classes.teamFeatureImages} />,
      color: "#E5E8FC",
    },
  };
  const lastItem = selectedFeature[selectedFeature.length - 1];

  const getFeatureAttributes = () => {
    const selectedFeature = features.find(x => lastItem == x.id);
    return selectedFeature ? teamListRender[selectedFeature.feature] : {};
  };

  return (
    <>

      <div className={classes.teamFeatureCnt}>
        <div className={classes.teamFeatureInnerSection}>
        <div className={classes.teamFeatureHeading}>
          <h1>
            Select the most relevant features
            for your team?
          </h1>
          <label>Your choices here won’t limit what you can do in nTask</label>
          <span>Your choices here won’t limit what you can do in nTask. Select upto 3 choices.</span>
        </div>
        <div className={classes.teamFeatureInnerCnt}>
          <ul className={classes.teamFeatureListbox}>
            <Grid container spacing={3} className={classes.teamFeatureInnerColumn}>
              {features.map(item => {
                return (
                  <Grid item xs={6} className={classes.teamFeatureGridStyle}>
                    <li
                   
                      onClick={() => handleTeamFeatures(item)}
                      style={
                        selectedFeature.includes(item.id)
                          ? { background: teamListRender[item.feature].color }
                          : []
                      }>
                      <span>{teamListRender[item.feature].icon}</span>
                      <span>{item.featureDescription}</span>
                    </li>
                  </Grid>
                );
              })}
            </Grid>
          </ul>
        </div>
      
        </div>
      </div>
      <div className={classes.teamFeatureBtn}>
          <CustomButton
            className={classes.teamFeatureInnerbtn}
            onClick={goToStepInvite}>
            Continue
          </CustomButton>
          <CustomButton className={classes.teamFeatureInnerbtn1} onClick={goToStepInvite}>I'm not sure yet</CustomButton>
        </div>
    </>
  );
}

export default withStyles(teamFeaturesStyle, { withTheme: true })(teamFeaturesView);
