import ArtWork from '../../../assets/images/signUpTopPatternRepeat.svg'
const onboardingHeaderStyle = theme => ({
    headerCnt: {
        position: 'absolute',
        top: 0,
        left: 0
    },
    headerFixed: {
        position: 'fixed',
        zIndex: 1,
        [theme.breakpoints.between('xs', 'sm')]: {
            marginTop: 43,
            height: 100,
            background: 'transparent',
            width: '100%'
        },
    },
    signUpHeaderCnt: {
        display: " flex",
        alignItems: "center",
        justifyContent: "space-between",
        background: theme.palette.common.white,
        padding: '30px',
        [theme.breakpoints.between('xs', 'sm')]: {
            width: "100%",
            padding: 22,
            background: theme.palette.common.white
        },
        [theme.breakpoints.between('sm', 'md')]: {
            width: 600,
            margin: "0 auto",
            background: theme.palette.common.white
        },
        [theme.breakpoints.between('md', 'lg')]: {
            width: "100%"
        },
    },
    signUpLogo: {
        display: " flex",
        alignItems: "center",
        [theme.breakpoints.between('xs', 'sm')]: {
            width: "100%",
            flexDirection: "column",
            alignItems: "flex-start",
            zIndex: 111,
            marginLeft: "-12px",
        },
        [theme.breakpoints.between('sm', 'md')]: {
            width: "100%",
            flexDirection: "column",
            alignItems: "flex-start",
        },

        [theme.breakpoints.between('md', 'lg')]: {
            width: "100%",
            flexDirection: "row",
            alignItems: "center",
        },
        "& img": {
            width: " 120px",
        },
    },
    settperCmpList: {
        [theme.breakpoints.between('xs', 'sm')]: {
            width: "100%",
            marginTop: 24,
        },
    },
    signUpCloseIconCnt: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        zIndex: 111,

        "& span": {
            width: "40px",
            height: "40px",
            background: "rgba(241, 241, 241, 0.6)",
            borderRadius: "100%",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            color: theme.palette.background.default,

        },

        "& label": {
            marginTop: "4px",
            fontSize: "13px !important",

            fontWeight: theme.typography.fontWeightMedium,
            color: theme.palette.common.white,
        }
    },
    artWorkTopImage: {
        backgroundImage: `url(${ArtWork})`,
        backgroundColor: theme.palette.common.white,
        [theme.breakpoints.between('xs', 'sm')]: {
            display: "block",
            position: "fixed",
            width: "100%",
            height: "125px",
            top: 0
        },

    },
    boardingHeaderBlackBtn: {
        zIndex: "11111",
        display: "none",
        [theme.breakpoints.between('xs', 'sm')]: {
            display: "block",
        },
    },
    logoInnerCnt: {
        display: 'flex'
    }
})

export default onboardingHeaderStyle