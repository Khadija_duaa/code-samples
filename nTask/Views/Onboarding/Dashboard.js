// @flow
import React, { useState } from "react";
import onBoardStyles from "./style";
import { withStyles } from "@material-ui/core/styles";
import Onboarding from "./Onboardingold";
import MasterLoader from "../../components/ContentLoader/Master";

function onBoardingDashboard(props) {
  return (
    <>
      <MasterLoader />
      <Onboarding />
    </>
  );
}

export default withStyles(onBoardStyles, { withTheme: true })(
  onBoardingDashboard
);
