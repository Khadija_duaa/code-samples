// @flow

import React, { useState } from "react";
import Typography from "@material-ui/core/Typography";
import onBoardStyles from "./style";
import { withStyles } from "@material-ui/core/styles";
import MobileStepper from "@material-ui/core/MobileStepper";
import { autoPlay } from "react-swipeable-views-utils";
import SwipeableViews from "react-swipeable-views";
import Logo from "../../assets/images/nTask-White-logo.svg";

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

//Left slider content
const stepsData = [
  {
    heading: "Manage Projects and Resource​",
    value: "Create projects, tasks, meetings, timesheet and more.",
  },
  {
    heading: "Visual Planners​",
    value: "Manage your projects, tasks, meetings effectively.​",
  },
  {
    heading: "No boundaries - Real time collaboration​",
    value: "Stay informed, make better faster decisions.​",
  },
];

//Onboarding Main Component
function Slider(props) {
  const [activeStep, setActiveStep] = useState(0);

  const handleStepChange = (step: number) => {
    // Function that changes the slider step automatically
    setActiveStep(step);
  };
  const { classes, theme } = props;
  return (
    <>
      <MobileStepper
        variant="dots"
        steps={3}
        position="static"
        activeStep={activeStep}
        classes={{
          root: classes.slider,
          dotActive: classes.activeDot,
          dot: classes.sliderDot,
        }}
      />
      <AutoPlaySwipeableViews
        axis={"x"}
        index={activeStep}
        onChangeIndex={handleStepChange}
        enableMouseEvents
      >
        {stepsData.map((step, index) => (
          <div>
            <Typography variant="h2" className={classes.sliderHeading}>
              {step.heading}
            </Typography>
            <Typography variant="body2" className={classes.sliderTagLine}>
              {step.value}
            </Typography>
          </div>
        ))}
      </AutoPlaySwipeableViews>
    </>
  );
}

export default withStyles(onBoardStyles, { withTheme: true })(Slider);
