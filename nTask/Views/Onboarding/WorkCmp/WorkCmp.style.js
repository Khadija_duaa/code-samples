const WorkCmpStyle = theme => ({
  workInfo: {
    fontSize: "28px !important",
    color: theme.palette.text.primary,
    fontWeight: 600,
    marginBottom: 30,
    [theme.breakpoints.between('xs', 'sm')]: {
fontSize:"22px !important",
    },
  },
  workTextCnt: {
    width: "100%",
    [theme.breakpoints.between('xs', 'sm')]: {
      width: "92%",
      margin: "0 auto",
      marginTop: 210,
    },
    [theme.breakpoints.between('sm', 'md')]: {
      width: 571,
      margin: "0 auto",
      marginTop: 210,
    },
    [theme.breakpoints.between('md', "lg")]: {
      width: "100%",
      margin: 0
    },
  },
  workTextInnerSection: {
    width: 430,
    marginLeft: 119,
    [theme.breakpoints.between('xs', 'sm')]: {
      width: "100%",
      marginLeft: 0,
    },
  },

  workCnt: {
    width: "690px",
    [theme.breakpoints.between('xs', 'sm')]: {
      width: "100%",
    },
    "& h2": {
      color: theme.palette.text.lightGray,
      fontSize: "16px !important",
      fontFamily: "poppins, sans-serif",
      fontWeight: 400,
      lineHeight: "1.17",
    },
    "& ul": {
      flexWrap: 'wrap',
      display: "flex",
      listStyle: "none",
      marginTop: "16px",
      padding: 0,
      [theme.breakpoints.between('xs', 'sm')]: {
        display: "inline-block",
      },
      "& li": {
        marginBottom: 10,
        [theme.breakpoints.between('xs', 'sm')]: {
          display: "inline-block",
          marginBottom: 8,
        },
        [theme.breakpoints.between('md', 'lg')]: {
          marginRight: 5,
        },
        padding: "10px 20px",
        border: "1px solid #DDDDDD",
        borderRadius: "70px",
        fontSize: "14px !important",
        fontWeight: 400,
        color: theme.palette.common.black,
        marginRight: 20,

        "&:hover": {
          background: theme.palette.background.blue,
          color: theme.palette.common.white,
          cursor: "context-menu",
        },
      },
    },
  },

  workCmpIocn: {
    color: "white",

    "&:hover": { cursor: "pointer " },
  },
  workBtn: {
    display: "flex",
    alignItems: "baseline",
    [theme.breakpoints.between('xs', 'sm')]: {
      width: "100%",
      display: "inline-block",
    },
  },
  continueBtn: {
    padding:"9px 16px",
    marginTop: 60,
    marginRight: 16,
    fontWeight: 400,
    fontSize:"13px",
    textTransform:"capitalize",
    borderRadius:"6px",
    letterSpacing:"1px",
    [theme.breakpoints.between('xs', 'sm')]: {
      width: "100%",
      height: 48,
      marginTop: 12,
      fontSize: "14px !important"
    },
    [theme.breakpoints.between('sm', 'md')]: {
      width: "100%",
      padding: "9px 14px",
      fontSize: "14px !important"
    },
  },
  continueBtn1: {
    height:"auto",
    padding:"8px 16px",
    border: "1px solid #DDDDDD",
    borderRadius: "6px",
    background: theme.palette.common.white,
    textTransform: "lowercase",
    fontFamily: theme.typography.fontFamilyLato,
    fontSize: "13px !important",
    fontWeight: 400,

    [theme.breakpoints.between('xs', 'sm')]: {
      width: "100%",
      height: 48,
      marginTop: 12,
      fontSize: "14px !important"
    },
    [theme.breakpoints.between('sm', 'md')]: {
      width: "100%",
      height: 48,
      fontSize: "14px !important"
    },
    [theme.breakpoints.between('md', 'lg')]: {
      width: "100%",
      height: 48,
      fontSize: "13px !important"

    },
    [theme.breakpoints.between('md', 'lg')]: {
      padding: "9px 16px",
      fontSize: "13px !important",
      height: "auto"

    },
    "&:hover": {
      background: theme.palette.common.white,
      boxShadow: '0px 2px 4px #00000014'
    },
  },
  workUnorderList: {
    [theme.breakpoints.between('md', 'lg')]: {
      display: "inline-block"

    },
  },
  selectedAnswer: {
    background: theme.palette.background.darkblue,
    border: 'none',
    color: `${theme.palette.common.white} !important`
  },
  singleMcqCnt: {
    marginBottom: 32
  }
});

export default WorkCmpStyle;
