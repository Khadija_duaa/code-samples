import React, { useContext, useState,useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import WorkCmpStyle from "./WorkCmp.style";
import Typography from "@material-ui/core/Typography";
import CustomButton from "../../../components/Buttons/CustomButton";
import SelectSearchDropdown from "../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { FormattedMessage } from "react-intl";
import OnBoadingContext from "../Context/OnBoarding.context";
import instance from "../../../redux/instance";
import axios from 'axios'
import { nextStep } from "../Context/actions/steps.action";
import { setQuestionAnswer } from "../Context/actions/work.action";
import classNames from "classnames";
import { slideView } from "../Context/constants";
function WorkCmpView({ classes }) {
  const [ questions, setQuestions] = useState([])
  useEffect(()=>{
    instance()
    axios.get(`${BASE_URL}api/marketingquestion`).then(response=>{
      setQuestions(response.data.entities)
    })
  },[]);

  const {
    state: { step, workDetail, stepsData },
    dispatch,
  } = useContext(OnBoadingContext);

  const fireGoogleTag = (tagName) => {
    if (window.dataLayer) {
      window.dataLayer.push({
        event: tagName,              
      });
    }
  }
  const goToStepFeature = () => {
    dispatch(nextStep())
    fireGoogleTag("work-screen");
  };
  const goToInviteStep = () => {
    dispatch(nextStep())
    fireGoogleTag("do-later-option");
    // dispatch(nextStep())
  };
  const handleMcqOptionSelect = (question, data) => {
      const answerObj = {questionId: question.questionId, answer: [data]}
    dispatch(setQuestionAnswer(answerObj))
  }
  const handleDropdownQuestion = (question, data) => {
    const answerObj = {questionId: question.questionId, answer: [data.value]}
    dispatch(setQuestionAnswer(answerObj))
  }
  const isFormFilled = questions.every(q => {
    return workDetail.some(a => q.questionId == a.questionId);
  })

  return (
    <>
      <div className={classes.workTextCnt}>
        <div className={classes.workTextInnerSection}>
          <div>
            <Typography variant="h1" className={classes.workInfo}>
              Tell us about work, {stepsData.fullName}?
            </Typography>
          </div>
          <div>
            {questions.map((q,i) => {
              const selectedAnswer = workDetail.find(a => a.questionId == q.questionId);
                const selectedAnswerValue = selectedAnswer && q.values.data.find(a => a.label == selectedAnswer.answer);
              return q.type == 'dropdown' &&
                <>
                <div>
                  <SelectSearchDropdown
                    data={() => q.values.data}
                    label={q.label}
                    isMulti={false}
                    inputType='underline'
                    selectChange={(type, option) => handleDropdownQuestion(q, option)}
                    type='questionDropdown'
                    selectedValue={selectedAnswerValue}
                    placeholder={q.placeholder}
                  />
              </div>
              
            </>


              
            })}
          </div>
          <div className={classes.workCnt}>
            {questions.map(q => {
              const selectedAnswer = workDetail.find(a => a.questionId == q.questionId );
                return q.type == 'mcq' ?
                  <div className={classes.singleMcqCnt}>
                    <div>
                      {" "}
                      <h2>{q.label}</h2>
                    </div>
                    <div>
                      <ul>
                        {q.values.data.map(data => {
                          return <li className={classNames({ [classes.selectedAnswer]: selectedAnswer && selectedAnswer.answer.includes(data) })}
                                     onClick={() => handleMcqOptionSelect(q, data)}>{data}</li>;
                        })}
                      </ul>
                    </div>
                  </div> : null
              }
            )}
            </div>

          
          <div className={classes.workBtn}>
            <CustomButton
              btnType="blue"
              variant="contained"
              className={classes.continueBtn}
              onClick={goToStepFeature}>
              Continue
            </CustomButton>
            <CustomButton btnType="white" variant="outlined" onClick={goToInviteStep} className={classes.continueBtn1}>
              I'll do it later
            </CustomButton>
          </div>
        </div>
      </div>
    </>
  );
}

export default withStyles(WorkCmpStyle, { withTheme: true })(WorkCmpView);
