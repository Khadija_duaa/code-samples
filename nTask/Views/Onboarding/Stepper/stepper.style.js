const stepperStyle = theme => ({
    stepperUnorderdList: {
        display: "flex",
        listStyle: "none",
        alignItems: "center",
        margin: 0,
        padding: 0,
        paddingLeft: "24px",
        borderLeft: "1px solid #BFBFBF",
        marginLeft: "24px",
        height: "36px",
        width: 520,
        [theme.breakpoints.between('xs', 'sm')]: {
            width: "105%",
            padding: 0,
            margin: "0 auto",
            border: "none",
        },
        // [theme.breakpoints.between("sm", "md")]: {
        //     width: "100%"
        // },

        "& li": {
            textAlign: "center",
            [theme.breakpoints.between('xs', 'sm')]: {
                flex: 1,
            },
            [theme.breakpoints.between("sm", "md")]: {
                flex: 1,
            },
            "&:hover": {
                cursor: "context-menu",
            }
        },
    },
    stepperListInnerSpan: {
        paddingLeft: "10px",
        fontSize: "12px !important",
        fontWeight: 400,
        color: theme.palette.secondary.medDark,
        textTransform: "capitalize",
    },
    stepperInnerCnt: {
        display: "flex",
        alignItems: "center",
        position: "relative",
        [theme.breakpoints.between('xs', 'sm')]: {
            width: "100%",
        },
    },
    stepperCircle: {
        width: "14px",
        height: "14px",
        background: theme.palette.background.grayLighter,
        border: "2px solid #FFFFFF",
        borderRadius: "50%",
    },
    stepperCircle1: {
        width: "14px",
        height: "14px",
        background: theme.palette.background.darkGreen,
        border: "2px solid #FFFFFF",
        borderRadius: "50%",
        position: "absolute",
        top: 0,
        left: 0,

    },
    stepperstrightLine: {
        display: "flex",
        width: "82px",
        height: "4px",
        background: theme.palette.background.grayLighter,
        borderRadius: "30px",
        [theme.breakpoints.between('xs', 'sm')]: {
            width: 0,
            flex: 1,
        },
    },
    stepperGreenstrightLine: {
        width: "100%",
        height: "4px",
        background: theme.palette.background.darkGreen,
        borderRadius: "30px",
    },
    endStepperCircle: {
        width: "14px",
        height: "14px",
        background: theme.palette.background.grayLighter,
        border: "2px solid #FFFFFF",
        borderRadius: "50%",
        marginTop: "13px",
        marginTop:17,
    },
    halfGreenStraightLine: {
        width: '50%'
    },
    halfWidth: {
        width: '50%'
    }
})

export default stepperStyle