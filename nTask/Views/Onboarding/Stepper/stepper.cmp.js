import React, { useContext } from "react";
import { withStyles } from "@material-ui/core/styles";
import stepperStyle from "./stepper.style";
import OnBoadingContext from "../Context/OnBoarding.context";

function stepperCmp({ classes }) {

  const { state: { step, steps } } = useContext(OnBoadingContext);
  const currentStepIndex = steps.findIndex(x => x.stepTitle == step);

  return (
    <>
      <ul className={classes.stepperUnorderdList}>
        {
          steps.map((item, i) => {
            const childIndex = steps.findIndex(x => x.stepTitle == item.child);
            return (!item.parent ?
                <li className={classes.stepperList}>
                  <span className={classes.stepperListInnerSpan}>{item.stepTitle}</span>
                  <div className={classes.stepperInnerCnt}>
                    <span className={classes.stepperCircle}></span>
                    {i <= currentStepIndex ?
                      <span className={classes.stepperCircle1}></span> : null}
                    <div className={classes.stepperstrightLine}>
                      {i < currentStepIndex && item.child ? <span
                        className={`${classes.stepperGreenstrightLine} ${classes.halfWidth}`}></span> : null}
                      {i < currentStepIndex && !item.child ?
                        <span className={classes.stepperGreenstrightLine}></span> : null}
                      {i < currentStepIndex && item.child && (childIndex < currentStepIndex) ?
                        <span className={classes.stepperGreenstrightLine}></span> : null}

                    </div>
                  </div>

                </li> : null
            );
          })

        }
        <span className={classes.endStepperCircle}></span>
      </ul>
    </>
  );
}

export default withStyles((stepperStyle), { withTheme: true })(stepperCmp);
