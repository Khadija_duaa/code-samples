import React, { useReducer, useContext, useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import Payment from "./Steps/Payment/payment.view";
import Profile from "./Steps/Profile/profile.view";
import Team from "./Steps/Team/team.view";
import OnBoardingHeaderView from "./OnboardingHeader/onboardingHeader.view";
import initialState from "./Context/initialStates";
import OnBoadingContext from "./Context/OnBoarding.context";
import reducer from "./Context/reducers";
import SideImageView from "./SideImage/SideImage.view";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import onBoardingStyle from "./OnBoarding.style";
import CustomIconButton from "../../components/Buttons/CustomIconButton";
import TeamFeatures from "../Onboarding/OnboardingHeader/TeamFeature/teamFeatures.view";
import WorkCmpView from "./WorkCmp/WorkCmp.View";
import {
  getFeaturesData,
  getUserEmail,
} from "../../Views/Onboarding/Context/actions/featuresModule.action";
import "./onboarding.css";
import MemberInviteCmp from "./Steps/members/MemberInviteCmp";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import queryString from "query-string";
import { setTeamPlan, setPromoCodeAction, userPlan } from "./Context/actions/payment.action";
import ToastNotification from "./OnboardingHeader/ToasterNotification/ToastNotification";
import { Elements } from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";
import { validateEmailSchema } from "../../utils/validator/common/email";
import { prevStep } from "./Context/actions/steps.action";
import { useDispatch, useSelector } from "react-redux";
import { getCompanyData, validateInvitationToken } from "../../redux/actions/onboarding";
import { setFullName, setWhiteLabelingData } from "./Context/actions/profile.action";
import Scrollbars from "react-custom-scrollbars";
import { withSnackbar } from "notistack";
import NewProjectIcon from "../../components/Icons/NewProjectIcon";
import ProjectImage from "../../assets/images/TeamFeaturesImage/illustrationProjects.svg";
import NewTaskIcon from "../../components/Icons/NewTaskIcon";
import TaskManagmentImage
  from "../../assets/images/TeamFeaturesImage/illustration_TaskManagement.svg";
import NewGanttIocn from "../../components/Icons/NewGanttIcon";
import GanttImage from "../../assets/images/TeamFeaturesImage/illustration_Gantt.svg";
import NewKanbanIcon from "../../components/Icons/NewKanbanIcon";
import NtaskBoardsImage from "../../assets/images/TeamFeaturesImage/illustration_Board.svg";
import SvgIcon from "@material-ui/core/SvgIcon";
import IssuesIcon from "../../components/Icons/IssueIcon";
import IssuesImage from "../../assets/images/TeamFeaturesImage/illustration_Issues.svg";
import RiskIcon from "../../components/Icons/RiskIcon";
import RiskManagementImage
  from "../../assets/images/TeamFeaturesImage/illustration_RiskManagement.svg";
import NewIconCustomField from "../../components/Icons/NewIconCustomField";
import CustomFieldImage from "../../assets/images/TeamFeaturesImage/illustration_CustomFields.svg";
import NewCustomStatusIcon from "../../components/Icons/NewCustomStatusIcon";
import CustomStatusImage from "../../assets/images/TeamFeaturesImage/illustration_CustomStatus.svg";
import MeetingIcon from "../../components/Icons/MeetingIcon";
import MeetingManagementImage
  from "../../assets/images/TeamFeaturesImage/illustration_Meetings.svg";
import TimesheetIcon from "../../components/Icons/TimesheetIcon";
import TimeTrackingImage from "../../assets/images/TeamFeaturesImage/illustration_Timesheet.svg";
import axios from "axios";
import constants from "../../redux/constants/types";
import { PopulateWhiteLabelInfo } from "../../redux/actions/teamMembers";
import { FetchPromotionCode } from "../../redux/actions/userBillPlan";
import mixpanel from "mixpanel-browser";
import { MixPanelEvents } from "../../mixpanel";

function OnBoardingSteps(props) {
  const { classes, location, history } = props;
  const {
    state: { step, paymentDetail, steps, features, selectedFeature, whiteLabelingData },
    dispatch,
  } = useContext(OnBoadingContext);
  const dispatchFn = useDispatch();
  const { auth } = useSelector((state) => ({
    auth: state.authenticationResponse.authState,
  }));
  const parsedUrl = queryString.parse(location.search);
  const [companyData, setCompanyData] = useState(false)
  //Getting data for features step
  useEffect(() => {
    getFeaturesData(dispatch, response => {
    });
  }, []);

  useEffect(() => {
    let userEmail = parsedUrl.email;
    let userFullName = parsedUrl.firstname + ` ${parsedUrl.lastname}`;
    mixpanel.track(MixPanelEvents.OnboardingEvent, { step, paymentDetail, userEmail, userFullName});
    mixpanel.time_event(MixPanelEvents.OnboardingEvent, { step });
  }, [step]);
  
  const showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "top",
          horizontal: "center",
        },
        variant: type ? type : "info",
      }
    )
  };
  useEffect(() => {
    const { location } = props;
    const parsedUrl = queryString.parse(location.search);
    if (parsedUrl.teamId && !companyData) {
      getCompanyData(parsedUrl.teamId,
        //success
        (data) => {
          dispatch(setWhiteLabelingData(data));
          setCompanyData(true)
        },
        //failure
        () => {
          setCompanyData(true)
        })
    }
    if (parsedUrl.token && companyData) {
      // Validating if the invitation token is valid or not
      validateInvitationToken(
        parsedUrl.token,
        (response) => {
          if (parsedUrl.iswhitelabelenabled && parsedUrl.iswhitelabelenabled == "True") {
            // setIsWhiteLabelEnabled(true);
            // getCompanyData(parsedUrl.teamId);
          } else {
            // setIsWhiteLabelEnabled(false);
            // setShow(true);
          }
        },
        () => {
          if (whiteLabelingData) {
            history.push(`/wl/${whiteLabelingData.brandName}`);
          } else {
            history.push("/account/login");
          }
        },
      );
    } else {
      // setShow(true);
      // setIsWhiteLabelEnabled(false);
    }
    if (parsedUrl.tempToken && !companyData) {
      // This case will work when user is redirected from website for social sign up
      localStorage.setItem("tempToken", parsedUrl.tempToken);
    }
  }, [companyData]);
  useEffect(() => {
    const parsedUrl = queryString.parse(location.search);
    //If user is authenticated redirect to teams dashboard
    if (auth) {
      history.push("/teams/");
    }
    //Getting user email from url query params
    //In case of invalid email, redirect user to login
    let userEmail = parsedUrl.email;
    let userPlan = parsedUrl.plan;
    let userFullName = parsedUrl.firstname + ` ${parsedUrl.lastname}`;
    let promoCode = parsedUrl.promoCode
    // if(companyData){
    validateEmailSchema().validate({ email: userEmail }, { abortEarly: false }).then(() => {
      dispatch(getUserEmail(userEmail));
      //Full name exist in case of social user sign up
      dispatch(setFullName({ fullName: parsedUrl.firstname ? userFullName : '' }));
    }).catch(() => {
      if (whiteLabelingData) {
        history.push(`/wl/${whiteLabelingData.brandName}`);
      } else {
        history.push("/account/login");
      }
      // history.push("/account/login");
    });

    if (promoCode) {
      let data = {
        userPlan: 'annually',
        promoCode: promoCode,
        planType: userPlan,
        quantity: 1,
        email: userEmail,
      }
      applyPromoCode(data);
    }
  }, []);
  const applyPromoCode = (data) => { 
    FetchPromotionCode(
      data,
      response => {
        if (response.data.valid) {
          dispatch(setPromoCodeAction({...response.data}));
          dispatch(setTeamPlan(data.planType));
          dispatch(setTeamPlan({ userPlan: data.userPlan }));
        }
      },
      fail => {
        showSnackBar(fail.data);
      }
    )();
  };
  //Getting user plan from url query params
  useEffect(() => {
    const parsedUrl = queryString.parse(location.search);
    let plan = parsedUrl.token && parsedUrl.teamId ? 'invited' : parsedUrl.plan;
    dispatch(userPlan(plan ? plan : "free"));

  }, []);
  const goToPrevStep = () => {
    dispatch(prevStep());
  };
  const teamListRender = {
    projectmanagement: {
      icon: <NewProjectIcon className={classes.icons} />,
      img: <img src={ProjectImage} alt="" className={classes.teamFeatureImages} />,
      color: "#EFDFF9",
    },
    taskmanagement: {
      icon: <NewTaskIcon className={classes.icons} />,
      img: <img src={TaskManagmentImage} alt="" className={classes.teamFeatureImages} />,
      color: "#DDF8EE",
    },
    ganttchart: {
      icon: <NewGanttIocn className={classes.icons} />,
      img: <img src={GanttImage} alt="" className={classes.teamFeatureImages} />,
      color: "#FEEDDE",
    },
    ntaskboards: {
      icon: <NewKanbanIcon className={classes.icons} />,
      img: <img src={NtaskBoardsImage} alt="" className={classes.teamFeatureImages} />,
      color: "#DDF4FA",
    },
    issueincidentreporting: {
      icon: (
        <SvgIcon className={classes.IssuesIcon} viewBox="0 0 16 17.375">
          <IssuesIcon />
        </SvgIcon>
      ),
      img: <img src={IssuesImage} alt="" className={classes.teamFeatureImages} />,
      color: "#FDEAEA",
    },
    riskmanagement: {
      icon: (
        <SvgIcon className={classes.RiskIcon} viewBox="0 0 18 15.75">
          <RiskIcon />
        </SvgIcon>
      ),
      img: <img src={RiskManagementImage} alt="" className={classes.teamFeatureImages} />,
      color: "#FEF4DF",
    },
    customfields: {
      icon: <NewIconCustomField className={classes.icons} />,
      img: <img src={CustomFieldImage} alt="" className={classes.teamFeatureImages} />,
      color: "#DBF4F4",
    },
    customstatus: {
      icon: <NewCustomStatusIcon className={classes.icons} />,
      img: <img src={CustomStatusImage} alt="" className={classes.teamFeatureImages} />,
      color: "#FAE3EF",
    },
    meetingmanagement: {
      icon: (
        <SvgIcon className={classes.meetingIcon} viewBox="0 0 17.031 17">
          <MeetingIcon />
        </SvgIcon>
      ),
      img: <img src={MeetingManagementImage} alt="" className={classes.teamFeatureImages} />,
      color: "#DDF4FA",
    },
    timesheet: {
      icon: (
        <SvgIcon className={classes.timeSheetIcon} viewBox="0 0 17 19.625">
          <TimesheetIcon />
        </SvgIcon>
      ),
      img: <img src={TimeTrackingImage} alt="" className={classes.teamFeatureImages} />,
      color: "#E5E8FC",
    },
  };
  const lastItem = selectedFeature[selectedFeature.length - 1];
  const getFeatureAttributes = () => {
    const selectedFeature = features.find(x => lastItem == x.id);
    return selectedFeature ? teamListRender[selectedFeature.feature] : {};
  };

  return (
    <div className={classes.onBoardingCnt}>
      <OnBoardingHeaderView onBackButtonClick={goToPrevStep} />
      <SideImageView />
      {step == "profile" || (auth && step == "payment") || (auth && step == "work") ? null :
        <CustomIconButton className={classes.boardingArrowUp} onClick={goToPrevStep}>
          <ExpandLessIcon />
        </CustomIconButton>}
      {/*<Scrollbars autoHide style={{ height: 'calc(100vh - 125px)', marginTop: "125px" }}>*/}
      <div className={classes.stepsCnt}>
        <div id="profile" style={{ opacity: 1, top: 125, display: 'block' }}><Profile /></div>
        <div id="team"><Team showSnackBar={showSnackBar} /></div>
        <div id="payment"><Payment showSnackBar={showSnackBar} /></div>
        <div id="work"><WorkCmpView showSnackBar={showSnackBar} /></div>
        {/*<div id="features"><TeamFeatures showSnackBar={showSnackBar}/></div>*/}
        <div id="invite"><MemberInviteCmp showSnackBar={showSnackBar} /></div>
        {/*<div id="profile" style={{ opacity: 1, transform: "translateY(0)" }}>{step === "profile" &&<Profile/>}</div>*/}
        {/*<div id="team">{step === "team" && <Team showSnackBar={showSnackBar}/>}</div>*/}
        {/*<div id="payment">{step === "payment" && <Payment showSnackBar={showSnackBar}/>}</div>*/}
        {/*<div id="work">{step === "work" && <WorkCmpView showSnackBar={showSnackBar}/>}</div>*/}
        {/*<div id="features">{step === "features" && <TeamFeatures showSnackBar={showSnackBar}/>}</div>*/}
        {/*<div id="invite">{step === "invite" && <MemberInviteCmp showSnackBar={showSnackBar}/>}</div>*/}

      </div>
      {/*</Scrollbars>*/}
      {step === "features" && getFeatureAttributes().img}
    </div>
  );
}

const stripePromise = loadStripe(STRIPE_PUBLISH_KEY);

function OnBoarding({ classes, location, history, enqueueSnackbar }) {
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <Elements stripe={stripePromise}>
      <OnBoadingContext.Provider value={{ dispatch, state }}>
        <OnBoardingSteps classes={classes} location={location} enqueueSnackbar={enqueueSnackbar} history={history} />
      </OnBoadingContext.Provider>
    </Elements>
  );
}

export default compose(withSnackbar, withRouter, withStyles(onBoardingStyle, { withTheme: true }))(OnBoarding);
