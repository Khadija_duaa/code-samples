import React, { useContext, useState } from "react";
import InviteMember from "../../../../components/InviteMember/InviteMember";
import { withStyles } from "@material-ui/core/styles";
import MemberStyle from "./Member.style";
import CustomButton from "../../../../components/Buttons/CustomButton";
import GanttImage from "../../../../assets/images/TeamFeaturesImage/illustration_Gantt.svg";
import { saveInvitee, sendStepData, sendWorkInviteData } from "../../Context/actions/invite.action";
import OnBoadingContext from "../../Context/OnBoarding.context";
import { useDispatch, useSelector } from "react-redux";
import withRouter from "react-router-dom/withRouter";
import { compose } from "redux";
import queryString from "query-string";
import { createSocialProfile } from "../../../../redux/actions/onboarding";
import mixpanel from "mixpanel-browser";
import { MixPanelEvents } from "../../../../mixpanel";
function InviteCmp({ classes, history, location, showSnackBar }) {
  const {auth} = useSelector((state) => ({
    auth: state.authenticationResponse.authState
  }))
  const dispatchFn = useDispatch();
  const { state, dispatch, state: {plan, paymentDetail} } = useContext(OnBoadingContext);
  const [btnQuery, setBtnQuery] = useState('')
  const savaEmailAddress = (list, email) => {
    dispatch(saveInvitee(list));
  };
  const getStoreData = () => {
    const parsedUrl = queryString.parse(location.search);
    const code = parsedUrl.isExternal ? { code: parsedUrl.code} : {};
    setBtnQuery('progress')
    const { invitee, selectedFeature, workDetail, paymentDetail,timezone, plan } = state;
    const { confirmPassword, ...rest } = state.stepsData;
    const withPaymentData = { invitee, selectedFeature, workDetail,timezone, ...code, plan, ...rest };
    const withoutOutPaymentData = { invitee, selectedFeature,timezone, workDetail, ...code };
    // mixpanel.track(MixPanelEvents.OnboardingEvent, withPaymentData);
    if(auth){ // In case payment is already made and user is created on payment step
      sendWorkInviteData(withoutOutPaymentData,
        //Success
        () => {
          setBtnQuery('')
          history.push(`/tasks/`);
      },
        //Failure
        () => {
          setBtnQuery('')
        })
      return;
    }
    if(parsedUrl.isExternal){
      const token = localStorage.setItem("tempToken", parsedUrl.tempToken);
      createSocialProfile(withPaymentData, token,
        //success
        () => {
          setBtnQuery('')
          history.push(`/tasks/`);
      },
        //failure
        () => {
          setBtnQuery('')

      }, dispatchFn);
      return;
    }
    //In case of no payment, just a free sign up
    sendStepData(withPaymentData, dispatchFn,
      //success
      (res) => {
      localStorage.setItem(
        "token",
        `Bearer ${res.data.data.access_token}`
      );
        setBtnQuery('')
        history.push(`/tasks/`);
    },
      //failure
      (err) => {
        setBtnQuery('')
        showSnackBar(err.response.data.message, 'error')
      });
  };
  const userInviteLimit = plan == 'premium' || plan == 'business' ? paymentDetail.users : 5
  return (
    <>
      <div className={classes.memberMainCnt}>
        <div className={classes.memberInnerCnt}>
          <div className={classes.memberCnt}>
            <h1>Congratulations! you are almost done.</h1>
            <label>Invite your team members.</label>
          </div>
          <div className={classes.memberInput}>
            <InviteMember
              emailList={[]}
              // updateEmailList={() => {}}
              placeholder="Enter email address"
              // invitationLimit={4}
              label="Email Address"
              ignoreEmails={[]}
              invitationLimit={userInviteLimit - 1}
              // CheckMemberAlreadyExist={false}
              updateEmailList={savaEmailAddress}
              errorMessage={'Your team has reached to its maximum member(s) limit.'}

            />
          </div>
          <div className={classes.memberBtnCmp}>
            <CustomButton btnType='blue' variant='contained' disabled={btnQuery == 'progress'} query={btnQuery} onClick={getStoreData}>
              <span style={{textTransform: 'none'}}>Take me to nTask</span>
            </CustomButton>
          </div>
        </div>
      </div>
    </>
  );
}

export default compose(withRouter, withStyles(MemberStyle, { withTheme: true }))(InviteCmp);
