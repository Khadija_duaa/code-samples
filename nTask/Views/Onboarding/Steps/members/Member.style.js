import zIndex from "@material-ui/core/styles/zIndex";

const MemberStyle = theme => ({
  memberMainCnt: {
    width: "100%",
    [theme.breakpoints.between("xs", "sm")]: {
      width: "92%",
      margin: "0 auto",
      marginTop: 210,
    },
    [theme.breakpoints.between("sm", "md")]: {
      width: 569,
      margin: "0 auto",
      marginTop: 210,
    },
    [theme.breakpoints.between("md", "lg")]: {
      width: "100%",
      margin: 0,
    },
  },
  memberInnerCnt: {
    width: "430px",
    marginLeft: "119px",
    [theme.breakpoints.between("xs", "sm")]: {
      width: "100%",
      marginLeft: 0,
    },
  },
  memberCnt: {
    position: "relative",
    width:560,
    "& h1": {
      fontSize: "28px !important",
      fontWeight: 600,
      color: theme.palette.common.black,
      marginBottom: "15px",
      [theme.breakpoints.between("xs", "sm")]: {
        fontSize: "22px !important",
        fontWeight: 600,
      },
    },
    "& label": {
      fontSize: "16px !important",
      fontWeight: 400,
      color: theme.palette.common.black,
      lineHeight: "5px",
    },
  },
  memberInput: {
    marginTop: "30px",
  },
  memberBtnCmp: {
    marginTop: "40px",
    [theme.breakpoints.between("xs", "sm")]: {
      marginTop: 318,
    },
  },
  memberInnerbtn: {
    height: "36px",
    padding: "9px 18px",
    background: theme.palette.background.btnBlue,
    fontSize: "13px !important",
    fontWeight: 400,
    color: theme.palette.common.white,
    borderRadius: "6px",
    textTransform: "none",
    letterSpacing: "1px",
    [theme.breakpoints.between("xs", "sm")]: {
      width: "100%",
      height: 48,
    },
    "&:hover": {
      background: theme.palette.background.darkblue,
    },
  },
});
export default MemberStyle;