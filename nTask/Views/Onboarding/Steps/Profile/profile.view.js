import React, { useState, useContext, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import DefaultTextField from "../../../../components/Form/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import profileStyle from "./profile.style";
import CustomButton from "../../../../components/Buttons/CustomButton";
import Link from "@material-ui/core/Link";
import { createTeam } from "../../Context/actions/team.action";
import { setFullName, setPassword, setConfirmPassword } from "../../Context/actions/profile.action";
import OnBoadingContext from "../../Context/OnBoarding.context";
import PasswordInput from "../../../../components/Form/PasswordInput/password.cmp";
import { updateErrors } from "../../../CustomFieldSettings/Context/actions/customFields.actions";
import isEmpty from "lodash/isEmpty";
import { validateProfileForm } from "./formSchema";
import Hotkeys from 'react-hot-keys';
import { nextStep } from "../../Context/actions/steps.action";
import { slideView } from "../../Context/constants";
import { sendStepData, sendWorkInviteData } from "../../Context/actions/invite.action";
import { useDispatch } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import queryString from "query-string";
import { passwordSchema } from "../../../../components/Form/PasswordInput/passwordSchema";
import NotificationMessage from "../../../../components/NotificationMessages/NotificationMessages";

function Profile({ classes, history, location, showSnackBar }) {
  const [showPassword, setShowPassword] = useState(false);
  const [isPasswordMatched, setIsPasswordMatched] = useState(true);
  const [focus, setFocus] = useState(false);
  const [errors, setErrors] = useState({});
  const [btnQuery, setBtnQuery] = useState('')
  const {
    state: { step, stepsData, plan, whiteLabelingData, timezone },
    dispatch,
  } = useContext(OnBoadingContext);
  const dispatchFn = useDispatch()
  const { fullName, password, email, confirmPassword } = stepsData;

  const fireGoogleTag = () => {
    if (window.dataLayer) {
      window.dataLayer.push({
        event: "profile-screen",
      });
    }
  }

  const handleStepValue = () => {
    const parsedUrl = queryString.parse(location.search);
    validateProfileForm().validate({ fullName: fullName.trim().replace(/\s\s+/g, ' '), password, confirmPassword }, { abortEarly: false })
      .then(succ => {
        if (plan == 'invited') {
          setBtnQuery('progress')
          const signUpData = { timezone, password, fullName, email, teamId: parsedUrl.teamId, token: parsedUrl.token }
          //In case of invited user
          sendStepData(signUpData, dispatchFn,
            //success
            (res) => {
              setBtnQuery('');
              localStorage.setItem(
                "token",
                `Bearer ${res.data.data.access_token}`
              );
              history.push(`/tasks/`);
            },
            //failure
            (err) => {
              setBtnQuery('')
              showSnackBar(err.response.data.message, 'error')
            });
        }
        dispatch(nextStep());
        fireGoogleTag();
      }).catch(err => {
        const errors = err.inner.reduce((r, cv) => {
          r[cv.path] = cv.message;
          return r;
        }, {});
        setErrors(errors);
      });
  };
  //Handle Inputs Change
  const handleChange = (e, type) => {
    const obj = { [type]: e.target.value.replace(/\s\s+/g, ' ') };
    switch (type) {
      case "fullName":
        dispatch(setFullName(obj));
        setErrors({ ...errors, fullName: '' })
        break;
      case "password":
        dispatch(setPassword(obj));
        setErrors({ ...errors, password: '' })
        break;
      case "confirmPassword":
        dispatch(setConfirmPassword(obj));
        setErrors({ ...errors, confirmPassword: '' })
        break;

      default:
        break;
    }
  };
  const handleFocus = () => {
    setFocus(true);
  };
  const isFormValid = validateProfileForm().isValidSync({ fullName: fullName.trim().replace(/\s\s+/g, ' '), password, confirmPassword }, { abortEarly: false });
  const isPasswordValid = passwordSchema.isValidSync(
    {
      password: password,
    },
    { abortEarly: false }
  );

  return (
    <>

      <div className={classes.profileViewContainer}>
        <div className={classes.profileViewWrapper}>
          <div>
            <Typography variant="h1" className={classes.welcomeTonTask}>
              Welcome to {whiteLabelingData && whiteLabelingData.brandName || 'nTask!'}
            </Typography>
            <Typography variant="body1" className={classes.gmailInfo}>
              You're signing up as <b>{stepsData.email}</b>
            </Typography>
          </div>
          <Hotkeys keyName="return" onKeyUp={() => handleStepValue("team")}>
            <div className={classes.profileViewCnt}>

              <DefaultTextField
                style={{fontFamily: "poppins, sans-serif !important"}}
                className={classes.profileViewCnt}
                labelProps={{
                  classes: {
                    root: classes.profileInputFieldLabel,
                  },
                }}
                formControlStyles={{ marginBottom: 11 }}
                inputType='underline'
                label="Full Name"
                errorState={!isEmpty(errors.fullName)}
                errorMessage={errors.fullName}
                defaultProps={{
                  onChange: e => handleChange(e, "fullName"),
                  id: "firstNameInput",
                  value: fullName,
                  placeholder: "Type your full name",
                  inputProps: { maxLength: 40, tabIndex: 1 },
                }}

              />

              <PasswordInput
                showConfirmPassword={true}
                onChangeCallback={handleChange}
                confirmPasswordProps={{ errorState: !isEmpty(errors.confirmPassword), errorMessage: errors.confirmPassword }}
                passwordProps={{ errorState: !isEmpty(errors.password), errorMessage: errors.password }}
                passwordValue={password}
                confirmPasswordValue={confirmPassword}
                inputType='underline'
              />


            </div>
            {isFormValid && isPasswordValid ?
              <NotificationMessage type="password" iconType="sucess">
                Your password is secure and you're all set!
              </NotificationMessage> : ''}
            {/* {isFormValid || isPasswordValid ?
              <NotificationMessage type="sucess" iconType="info">
                Your password is secure and you're all set!
              </NotificationMessage> : ''} */}


          </Hotkeys>
          <div className={classes.signInSection}>
            <CustomButton
              btnType="blue"
              variant="contained"
              className={classes.continueBtn}
              onClick={() => handleStepValue()}
              disabled={btnQuery == 'progress' || !isFormValid || !isPasswordValid}
              query={btnQuery}
            >
              {" "}
              {plan == 'invited' ? 'Take me to ' + `${whiteLabelingData && whiteLabelingData.brandName ? whiteLabelingData.brandName : 'nTask'}` : 'Continue'}
            </CustomButton>
            <Typography variant="body1" className={classes.alreadyAcc}>
              Already have an account?{" "}
              <b>
                <Link href={whiteLabelingData && whiteLabelingData.brandName ? `/wl/${whiteLabelingData.brandName}` : '/'} className={classes.link}>
                  Sign In
                </Link>{" "}
              </b>{" "}
              instead.
            </Typography>
          </div>
        </div>
      </div>

    </>
  );
}

export default compose(withRouter, withStyles(profileStyle, { withTheme: true }))(Profile);
