import * as yup from "yup";
import {
  validateConfirmPassword,
  validateFullName,
  validatePassword,
} from "../../../../utils/validator/common/password";

export function validateProfileForm(intl) {
  return yup.object({
    fullName: validateFullName,
    password: validatePassword,
    confirmPassword: validateConfirmPassword
  })
}