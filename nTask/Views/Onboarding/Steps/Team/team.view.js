import React, { useContext, useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import teamStyle from "./team.style";
import CustomButton from "../../../../components/Buttons/CustomButton";
import TeamNameInput from "../../../../components/Form/TeamNameInput/teamNameInput.cmp";
import OnBoadingContext from "../../Context/OnBoarding.context";
import { paymentInformation, userPlan } from "../../Context/actions/payment.action";
import {
  createTeam,
  createTeamName,
  createWorkspaceName, setDataLocation,
} from "../../Context/actions/team.action";
import { validateProfileForm } from "../Profile/formSchema";
import isEmpty from "lodash/isEmpty";
import { validateTeamForm } from "./formSchema";
import WorkspaceNameInput
  from "../../../../components/Form/WorkspaceNameInput/workspaceNameInput.cmp";
import { nextStep } from "../../Context/actions/steps.action";
import { slideView } from "../../Context/constants";
import { dataLocationData } from "../../../../helper/dataCenterDropdownData";
import SelectSearchDropdown
  from "../../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";

function Team({ classes, theme }) {
  const {
    state: { step, stepsData, plan },
    dispatch,
  } = useContext(OnBoadingContext);
  const { teamName, workspaceName, dataLocation } = stepsData;
  const [getUserPlan, setGetUserPlan] = useState("");
  const [errors, setErrors] = useState({});
  const [dataLocationValue, setDataLocationValue] = useState("");
  useEffect(() => {
    setGetUserPlan(plan);
  }, [getUserPlan]);

  const fireGoogleTag = () => {
    if (window.dataLayer) {
      window.dataLayer.push({
        event: "team-screen",              
      });
    }
  }

  const goToPaymentInformation = step => {
    validateTeamForm().validate({
      teamName: teamName.trim(),
      workspaceName: workspaceName.trim(),
    }, { abortEarly: false })
      .then(succ => {
        dispatch(nextStep());
        fireGoogleTag();
      })
      .catch(err => {
        const errorObj = err.inner.reduce((r, cv) => {
          r[cv.path] = cv.message;
          return r;
        }, {});
        setErrors(errorObj);
      });

  };

  const onInputChange = (value, type) => {
    const obj = { [type]: value };
    setErrors({ ...errors, [type]: "" });
    switch (type) {
      case "teamName":
        dispatch(createTeamName(obj));
        break;
      case "workspaceName":
        dispatch(createWorkspaceName(obj));
        break;
      default:
        break;
    }
  };


  const onDatalocationValue = (data, type) => {
    dispatch(setDataLocation({ dataLocation: data.value }));
  };

  const isTeamFormValid = validateTeamForm().isValidSync({
    teamName: teamName.trim(),
    workspaceName: workspaceName.trim(),
  }, { abortEarly: false });
  const selectedLocation = dataLocationData.filter(d => d.value == dataLocation);
  return (
    <>
      <div className={classes.teamViewWrapper}>
        <div className={classes.teamWorkspaceContainer}>
          <div>
            <Typography variant="h1" className={classes.teamViewContainer}>
              First things first, <br/>
              Create your team and workspace
            </Typography>
          </div>
          <div style={{ marginTop: "33px" }} className={classes.teamInputFieldCnt}>
            <TeamNameInput
              style={{fontFamily: "poppins, sans-serif !important"}}
              errorState={!isEmpty(errors.teamName)}
              errorMessage={errors.teamName}
              onChangeCallback={onInputChange}
              value={teamName}
              inputType="underline"
              labelProps={{
                classes: {
                  root: classes.teamInputFieldLabel,
                },
              }}
            />
            <WorkspaceNameInput
              style={{fontFamily: "poppins, sans-serif !important"}}
              errorState={!isEmpty(errors.workspaceName)}
              errorMessage={errors.workspaceName}
              onChangeCallback={onInputChange}
              value={workspaceName}
              inputType="underline"
              labelProps={{
                classes: {
                  root: classes.teamInputFieldLabel,
                },
              }}
            />
            <SelectSearchDropdown
              data={() => dataLocationData}
              label="Select your Regional Data Center"
              isMulti={false}
              selectChange={(type, option) => onDatalocationValue(option, "dataLocation")}
              type="dataLocationDropdown"
              selectedValue={selectedLocation}
              placeholder="Data Location"
              inputType='underline'
            />
          </div>
          <div className={classes.signInSection}>
            <CustomButton
              btnType="blue"
              variant="contained"
              className={classes.teamBtn}
              disabled={!isTeamFormValid}
              onClick={() => goToPaymentInformation(getUserPlan ? "payment" : "work")}>
              {" "}
              Continue
            </CustomButton>
          </div>
        </div>
      </div>
    </>
  );
}

export default withStyles(teamStyle, { withTheme: true })(Team);
