const teamStyle = theme => ({
  teamViewWrapper: {
    width: "100%",
    [theme.breakpoints.between('xs', 'sm')]: {
      width:"92%",
      margin:"0 auto",
      marginTop: 210,
  },
  [theme.breakpoints.between('sm', 'md')]: {
    width: 571,
    margin: "0 auto",
    marginTop: 210,
  },
  [theme.breakpoints.between('md', "lg")]: {
    width: "100%",
    margin: 0
  },
  },
  teamWorkspaceContainer:{
width:430,
marginLeft:119,
[theme.breakpoints.between('xs', 'sm')]: {
width:"100%",
marginLeft:0,
},
  },
  // teamInputFieldCnt:{
  //   marginTop: "33px" ,
  //   [theme.breakpoints.between('md', "lg")]: {
  //     width: "100%",

  //   },
  // },
  teamViewContainer: {
    color:theme.palette.text.primary,
    fontSize:'28px',
    fontWeight:600,
  },
  teamBtn:{
    fontSize: "13px !important",
    padding:"9px 16px",
    borderRadius:"6px",
    fontWeight:400,
    letterSpacing:"1px",
    textTransform:"capitalize",
    [theme.breakpoints.between('xs', 'sm')]: {
      width: "100%",
      padding: 13,
      fontSize: "14px !important",
      height:48,
    },
    [theme.breakpoints.between('sm', 'md')]: {
      width: "100%",
      fontSize: "14px !important",
      height:48,
    },
  },
  signInSection: {
    marginTop: 60,
    [theme.breakpoints.between('xs', 'sm')]: {
      width: "100%",
      textAlign: "center",
      marginTop: 118,
    },
  },
  labelTeamName:{
    fontSize:16
  },
  teamInputFieldLabel:{
    display: "flex",
    transform: "translate(2px, -17px) scale(1)",
    fontSize: "16px !important",
    alignItems: "center",
    fontWeight: 400,
    fontFamily:theme.typography.fontFamilyLato,
    color: theme.palette.common.black,
    lineHeight: "5px",
  }
});
export default teamStyle;
