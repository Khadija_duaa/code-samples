import * as yup from "yup";
import { validateTeamName, validateWorkspaceName } from "../../../../utils/validator/team/teamName";


export function validateTeamForm(intl) {
  return yup.object({
    teamName: validateTeamName,
    workspaceName: validateWorkspaceName
  })
}