
import IconButton from "@material-ui/core/IconButton";
import InputAdornment from "@material-ui/core/InputAdornment";
import Typography from "@material-ui/core/Typography";
import React, { useContext, useState, useEffect } from "react";
import paymentStyle from "./payment.style";
import { withStyles } from "@material-ui/core/styles";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import DefaultTextField from "../../../../components/Form/TextField";
import CustomButton from "../../../../components/Buttons/CustomButton";
import Link from "@material-ui/core/Link";
import iconLock from "../../../../../src/assets/images/iconLock.svg";
import satisfactionGuaranteed from "../../../../../src/assets/images/satisfactionGuaranteed.svg";
import secureSSL from "../../../../../src/assets/images/secureSSL.svg";
import privacyProtection from "../../../../../src/assets/images/privacyProtection.svg";
import iconStripe from "../../../../../src/assets/images/iconStripe.svg";
import DefaultCheckbox from "../../../../components/Form/Checkbox";
import OnBoadingContext from "../../Context/OnBoarding.context";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import {
  removePromoCodeAction,
  setPromoCode,
  setPromoCodeAction,
  setTeamPlan,
} from "../../Context/actions/payment.action";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import queryString from "query-string";
import { nextStep } from "../../Context/actions/steps.action";
import { FetchPromotionCode, GetPackagePlans } from "../../../../redux/actions/userBillPlan";
import isEmpty from "lodash/isEmpty";
import { loadStripe } from "@stripe/stripe-js";
import { CardElement, Elements, useStripe, useElements } from "@stripe/react-stripe-js";
import { sendStepData } from "../../Context/actions/invite.action";
import { useDispatch } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { slideView } from "../../Context/constants";
import DefaultButton from "../../../../components/Buttons/DefaultButton";
import clsx from "clsx";
import PromoPlan from "../../../billing/PromoPlan/PromoPlan";

function Payment({ classes, props, history, location, showSnackBar }) {
  const stripe = useStripe();
  const elements = useElements();
  const dispatchFn = useDispatch();
  const {
    state,
    state: {
      stepsData: { teamName, email },
      promoCodeDetail,
      timezone,
      paymentDetail: { planType, userPlan, byContinuing, users, promoCode },
    },
    dispatch,
  } = useContext(OnBoadingContext);
  const [planDetails, setPlanDetails] = useState({});
  const [isCardDetailFilled, setIsCardDetailFilled] = useState(false);
  const [payBtnLoading, setPayBtnLoading] = useState("");
  const [promoCodeValue, setPromoCodeValue] = useState("");
  const [showPromoInput, setShowPromoInput] = useState(false);
  const [promoCodeBtnQuery, setPromoCodeBtnQuery] = useState("");
  useEffect(() => {
    GetPackagePlans(res => {
      setPlanDetails(res.data);
    });
    if (promoCode != "") {
      setShowPromoInput(true)
      setPromoCodeValue(promoCode);
    }
  }, [promoCode]);
  //  {state.paymentDetail.teamPlan} = plan;
  // const {state:paymentDetailteamPlan} = plan
  // props.paymentDetail.teamPlan = plan

  const goToStepWorkReview = async () => {
    setPayBtnLoading("progress");
    if (elements == null) {
      return;
    }
    const card = elements.getElement(CardElement);
    const result = await stripe.createToken(card);
    if (result.error) {
      // Show error to your customer.
      setPayBtnLoading("");
      console.log(result.error.message);
    } else {
      dispatch(setTeamPlan({ stripeToken: result.token.id, cardName: result.token.card.brand }));
      const { invitee, selectedFeature, workDetail, paymentDetail, plan } = state;
      const paymentDetails = {
        ...paymentDetail,
        stripeToken: result.token.id,
        cardName: result.token.card.brand,
      };
      const { confirmPassword, ...rest } = state.stepsData;
      const data = { invitee, selectedFeature, workDetail, paymentDetails, plan, timezone, ...rest };
      sendStepData(
        data,
        dispatchFn,
        //success
        res => {
          localStorage.setItem("token", `Bearer ${res.data.data.access_token}`);
          setPayBtnLoading("");
          // history.push(`/teams/`);
          dispatch(nextStep());
        },
        //failure

        err => {
          showSnackBar(err.response.data.message, "error");
          setPayBtnLoading("");
        }
      );
    }
  };
  const setTeamPlaning = step => {
    dispatch(setTeamPlan({ planType: step }));
  };
  const setTotalTeamMembers = e => {
    if (e == 0) {
      return;
    }
    dispatch(setTeamPlan({ users: e }));
  };
  // handle increment and decerement
  const handleIncrement = () => {
    dispatch(setTeamPlan({ users: Number(users) + +1 }));
  };
  const handleDecrement = () => {
    if (Number(users) - 1 == 0) {
      return;
    }
    dispatch(setTeamPlan({ users: Number(users) - 1 }));
  };
  const setBillingCycleSD = e => {
    const parsedUrl = queryString.parse(location.search);
    if (!isEmpty(promoCode) && e == 'monthly' && parsedUrl.promoCode) {
      removePromoCode();
      showSnackBar('This Promotion Code is not valid for monthly payments');
    }
    dispatch(setTeamPlan({ userPlan: e }));
  };
  const setBillingCycleOS = e => {
    dispatch(setTeamPlan({ userPlan: e }));
  };
  const handleAgreeTerm = e => {
    dispatch(setTeamPlan({ byContinuing: e.target.checked }));
  };
  const handleCardChange = e => {
    setIsCardDetailFilled(e.complete);
  };
  //handle promo code input
  const handlePromocodeInput = e => {
    setPromoCodeValue(e.target.value);
  };
  const removePromoCode = () => {
    dispatch(removePromoCodeAction());
    setPromoCodeValue("");
  };
  //Apply promo code
  const applyPromoCode = () => {
    setPromoCodeBtnQuery("progress");
    let data = {};
    data.userPlan = userPlan;
    data.promoCode = promoCodeValue;
    data.planType = planType
    data.quantity = users;
    data.email = email;
    FetchPromotionCode(
      data,
      response => {
        if (response.data.valid) {
          dispatch(setPromoCodeAction(response.data));
          setPromoCodeBtnQuery("");
          showSnackBar('Promotion Code added successfully');
        }
      },
      fail => {
        showSnackBar(fail.data);
        setPromoCodeBtnQuery("");
      }
    )();
  };

  //show promo code input
  const handleShowPromoCode = () => {
    setShowPromoInput(true);
  };
  const selectedPlanDetails = planDetails[planType.toLowerCase()];
  const priceMonthly = selectedPlanDetails && selectedPlanDetails.unitPriceMonthly;
  const priceAnnually = selectedPlanDetails && selectedPlanDetails.unitPriceAnually;
  const premiumPlan = planDetails ? planDetails.premium : {};
  const businessPlan = planDetails ? planDetails.business : {};
  const totalAnnualPrice = priceAnnually * 12 * users;
  const totalMonthlyPrice = priceMonthly * users;
  const promoDiscount = promoCodeDetail ? totalAnnualPrice * (promoCodeDetail.percentOff / 100) : null;
  const promoDiscountMonthly = promoCodeDetail ? totalMonthlyPrice * (promoCodeDetail.percentOff / 100) : null;
  const annualPriceAfterDiscount = promoDiscount
    ? (totalAnnualPrice - promoDiscount).toFixed(2)
    : totalAnnualPrice;
  const monthlyPriceAfterDiscount = promoDiscountMonthly
    ? (totalMonthlyPrice - promoDiscountMonthly).toFixed(2)
    : totalMonthlyPrice;
  return (
    !isEmpty(planDetails) && (
      <>
        <div className={classes.paymentViewContainer}>
          <div className={classes.paymentViewInnerSection}>
            <div className={classes.paymentViewWrapper}>
              <Typography variant="h1" className={classes.paymentInfo}>
                Payment Information
              </Typography>
            </div>
            <div className={classes.teamPlanView}>
              <div>
                <Typography variant="h6" className={classes.teamPlan}>
                  Select Team Plan
                </Typography>
              </div>
              <div className={classes.preBusViewContainer}>
                <div
                  className={
                    planType === "Premium" ? `${classes.applyStyle}` : `${classes.premiumView}`
                  }
                  style={{ marginRight: 20 }}
                  onClick={() => setTeamPlaning("Premium")}>
                  <span className={classes.defaultCheckBox}>
                    {planType === "Premium" ? <CheckCircleIcon checked={true} /> : ""}
                  </span>
                  <div className={classes.paymentHeadingSection}>
                    <Typography variant="h4" className={classes.premiumVal}>
                      Premium
                    </Typography>
                    {userPlan == "monthly" ? (
                      <Typography variant="h1" className={classes.dollorVal}>
                        ${premiumPlan.unitPriceMonthly}
                      </Typography>
                    ) : (
                      <Typography variant="h1" className={classes.dollorVal}>
                        ${premiumPlan.unitPriceAnually}
                      </Typography>
                    )}
                    <Typography variant="h6" className={classes.totalMembers}>
                      members per month
                    </Typography>
                  </div>
                </div>
                <div
                  className={
                    planType === "Business" ? `${classes.applyStyle}` : `${classes.premiumView}`
                  }
                  onClick={() => setTeamPlaning("Business")}>
                  <span className={classes.defaultCheckBox}>
                    {planType === "Business" ? <CheckCircleIcon checked={true} /> : ""}
                  </span>
                  <div className={classes.paymentHeadingSection}>
                    <Typography variant="h4" className={classes.premiumVal}>
                      Business
                    </Typography>
                    {userPlan == "monthly" ? (
                      <Typography variant="h1" className={classes.dollorVal}>
                        ${businessPlan.unitPriceMonthly}
                      </Typography>
                    ) : (
                      <Typography variant="h1" className={classes.dollorVal}>
                        ${businessPlan.unitPriceAnually}
                      </Typography>
                    )}
                    <Typography variant="h6" className={classes.totalMembers}>
                      members per month
                    </Typography>
                  </div>
                </div>
              </div>
            </div>
            <div className={classes.subscriptionDContainer}>
              <div className={classes.subDetContainer}>
                <Typography varian="h3" className={classes.subscriptionDetCont}>
                  Subscription Details
                </Typography>
              </div>
              <div className={classes.teamCorporationContainer} style={{ marginTop: 15 }}>
                <div className={classes.teamSec}>
                  <Typography varian="h3" className={classes.subsDetailsContent}>
                    Team
                  </Typography>
                </div>
                <div className={classes.globexCor}>
                  <Typography varian="h3" className={classes.teamNameText}>
                    {teamName}
                  </Typography>
                </div>
              </div>
              <div className={classes.teamCorporationContainer} style={{ marginTop: 15 }}>
                <div className={classes.teamSec}>
                  <Typography varian="h3" className={classes.subsDetailsContent}>
                    No. of Team Members
                  </Typography>
                </div>
                <div className={classes.integerInput}>
                  <DefaultTextField
                    customInputClass={{
                      adornedEnd: classes.adornedEnd,
                    }}
                    defaultProps={{
                      value: users,
                      id: "onBoardPasswordInput",
                      placeholder: "1",
                      inputProps: { maxLength: 40, tabIndex: 2, className: classes.noArrowNumberField },
                      type: "number",
                      onChange: e => setTotalTeamMembers(e.target.value),
                      endAdornment: (
                        <InputAdornment className={classes.inputAdorement} position="end">
                          <IconButton
                            disableRipple={true}
                            classes={{
                              root: classes.inputButton,
                            }}
                            style={{ transform: "translateY(3px)" }}
                            onClick={handleIncrement}>
                            <KeyboardArrowUpIcon className={classes.inputIcon} />
                          </IconButton>
                          <IconButton
                            disableRipple={true}
                            classes={{
                              root: classes.inputButton,
                            }}
                            style={{ transform: "translateY(-3px)" }}
                            onClick={handleDecrement}>
                            <KeyboardArrowDownIcon className={classes.inputIcon} />
                          </IconButton>
                        </InputAdornment>
                      ),
                    }}
                  />
                </div>
              </div>
              <div className={classes.teamCorporationContainer} style={{ marginTop: 15 }}>
                <div className={classes.teamSec}>
                  <Typography varian="h3" className={classes.subsDetailsContent}>
                    Billing Cycle
                  </Typography>
                </div>
                <div className={classes.monthlyAnnualBtn}>
                  <div className={classes.toggleContainer}>
                    <ToggleButtonGroup
                      // value={settings.direction}
                      exclusive
                      classes={{ root: classes.toggleBtnGroup }}>
                      <ToggleButton
                        value={"monthly"}
                        selected={userPlan === "monthly" ? "selected" : ""}
                        // disabled={settings.unit === "%"}
                        classes={{
                          root: classes.toggleButton,
                          selected: classes.toggleButtonSelected,
                        }}
                        onClick={() => setBillingCycleSD("monthly")}>
                        Monthly
                      </ToggleButton>
                      <ToggleButton
                        value={"annually"}
                        selected={userPlan === "annually" ? "selected" : ""}
                        classes={{
                          root: classes.toggleButton,
                          selected: classes.toggleButtonSelected,
                        }}
                        onClick={() => setBillingCycleSD("annually")}>
                        Annually
                      </ToggleButton>
                    </ToggleButtonGroup>
                  </div>
                </div>
              </div>
              {userPlan == "monthly" ? (
                <div className={classes.savePlan}>
                  <div>
                    <Typography className={classes.switchPlan}>
                      Switch to Annual plan to save <b style={{ color: "#00916D" }}>33%</b>
                    </Typography>
                  </div>
                </div>
              ) : (
                ""
              )}

              <div className={classes.teamCorporationContainer} style={{ marginTop: 15 }}>
                <div className={classes.teamSec}>
                  <Typography varian="h3" className={classes.subsDetailsContent}>
                    Subscription Charges
                    {userPlan == "monthly" && planType == "Business" ? (
                      <span className={classes.subText}>
                        <sub className={classes.totalChargesMA}>
                          ${priceMonthly} x {users} user
                        </sub>
                      </span>
                    ) : userPlan == "annually" && planType == "Business" ? (
                      <span className={classes.subText}>
                        <sub className={classes.totalChargesMA}>
                          ${priceAnnually} x {users} user x 12 months
                        </sub>
                      </span>
                    ) : (
                      ""
                    )}
                    {userPlan == "monthly" && planType == "Premium" ? (
                      <span className={classes.subText}>
                        <sub className={classes.totalChargesMA}>
                          ${priceMonthly} x {users} user
                        </sub>
                      </span>
                    ) : userPlan == "annually" && planType == "Premium" ? (
                      <span className={classes.subText}>
                        <sub className={classes.totalChargesMA}>
                          ${priceAnnually} x {users} user(s) x 12 months
                        </sub>
                      </span>
                    ) : (
                      ""
                    )}
                  </Typography>
                </div>
                <div className={classes.globexCor}>
                  {userPlan == "monthly" && planType === "Premium" ? (
                    <Typography varian="h3" className={classes.pricingRate}>
                      <b className={classes.subscriptioncharges}>${priceMonthly * users}</b>
                      <span className={classes.subText}>
                        <sub className={classes.totalChargesMA}>Billed monthly</sub>
                      </span>
                    </Typography>
                  ) : userPlan == "annually" && planType === "Premium" ? (
                    <Typography varian="h3" className={classes.pricingRate}>
                      <span className={classes.disableCharges}>${priceMonthly * users * 12}</span>
                      <b className={classes.subscriptioncharges}>${priceAnnually * users * 12}</b>
                      <span className={classes.subText}>
                        <sub className={classes.totalChargesMA}>Billed annually</sub>
                      </span>
                    </Typography>
                  ) : userPlan == "monthly" && planType === "Business" ? (
                    <Typography varian="h3" className={classes.pricingRate}>
                      <b className={classes.subscriptioncharges}>${priceMonthly * users}</b>
                      <span className={classes.subText}>
                        <sub className={classes.totalChargesMA}>Billed monthly</sub>
                      </span>
                    </Typography>
                  ) : userPlan == "annually" && planType === "Business" ? (
                    <Typography varian="h3" className={classes.pricingRate}>
                      <span className={classes.disableCharges}>${priceMonthly * 12 * users}</span>
                      <b className={classes.subscriptioncharges}>${priceAnnually * users * 12}</b>
                      <span className={classes.subText}>
                        <sub className={classes.totalChargesMA}>Billed annually</sub>
                      </span>
                    </Typography>
                  ) : (
                    ""
                  )}
                </div>
              </div>
              <div className={classes.paymentDetailsContainer}>
                <div className={classes.paymentDetContainer}>
                  <Typography varian="h3" className={classes.subscriptionDetCont}>
                    Payment Details
                  </Typography>
                </div>
                <div className={classes.cardNumDet}>
                  <CardElement onChange={handleCardChange} options={{ hidePostalCode: true }} />
                </div>
                {!showPromoInput ? (
                  <span
                    className={classes.addPromoBtn}
                    onClick={handleShowPromoCode}>
                    Add Promo Code
                  </span>
                ) : (
                  <>
                    <div className={classes.promocodeValueCnt}>
                      <DefaultTextField
                        label={"Promo Code"}
                        labelProps={{
                          classes: {
                            root: classes.defaultInputLabel,
                          },
                        }}
                        fullWidth={true}
                        error={false}
                        errorState={false}
                        formControlStyles={{ marginBottom: 0 }}
                        inputType="underline"
                        defaultProps={{
                          placeholder: "Enter promo code",
                          id: "promocode",
                          type: "text",
                          onChange: e => handlePromocodeInput(e),
                          value: promoCodeValue,
                          inputProps: {
                            maxLength: 80,
                            disabled: !isEmpty(promoCode),
                          },
                          endAdornment: (
                            <InputAdornment
                              className={clsx({
                                [classes.inputAdorementPromoCode]: !promoCode,
                                [classes.inputAdorementPromoCodeDanger]: promoCode,
                              })}
                              position="end">
                              {promoCodeValue !== "" && (
                                <span
                                  onClick={() =>
                                    promoCode ? removePromoCode() : applyPromoCode()
                                  }>
                                  {promoCode ? "Remove" : "Apply"}
                                </span>
                              )}
                            </InputAdornment>
                          ),
                        }}
                      />
                      {/* <CustomButton
                        btnType={promoCode ? "danger" : "plain"}
                        variant="outlined"
                        style={{
                          marginBottom: 0,
                          width: 110,
                          height: 40,
                          marginLeft: 20,
                        }}
                        onClick={() => (promoCode ? removePromoCode() : applyPromoCode())}
                        query={promoCodeBtnQuery}
                        disabled={promoCodeBtnQuery == "progress"}>
                        {!promoCode ? "Apply" : "Remove"}
                      </CustomButton> */}
                    </div>
                  </>
                )}
              </div>
            </div>
            {/* ////////////////// Order Summary  ///////////////////////// */}
            <div className={classes.subscriptionDContainer}>
              <div className={classes.subDetContainer}>
                <Typography
                  varian="h3"
                  className={`${classes.subscriptionDetCont} ${classes.customFontFamily}`}
                  style={{ paddingBottom: 9 }}>
                  Order Summary
                </Typography>
              </div>
              <div className={classes.orderSummaryContainer}>
                <div className={classes.teamCorporationContainer}>
                  <div className={classes.teamSec}>
                    <Typography varian="h3" className={classes.subsDetailsContent}>
                      Team
                    </Typography>
                  </div>
                  <div className={classes.globexCor}>
                    <Typography varian="h3" className={classes.teamNameText}>
                      {teamName}
                    </Typography>
                  </div>
                </div>
                <div className={`${classes.teamCorporationContainer} ${classes.premiumPlan}`}>
                  <div className={classes.teamSec}>
                    {planType === "Business" ? (
                      <Typography varian="h3" className={classes.subsDetailsContent}>
                        Business Plan for {users} users
                      </Typography>
                    ) : (
                      <Typography varian="h3" className={classes.subsDetailsContent}>
                        Premium Plan for {users} users
                      </Typography>
                    )}
                  </div>
                  <div className={classes.integerInput}>
                    <Typography variant="body1" className={classes.premiumPrice}>
                      {/* ${userPlan == "monthly" ? priceMonthly * users : priceAnnually * 12 * users} */}
                      ${userPlan == "monthly" && planType === "Premium" ? priceMonthly * users : ""}
                      {userPlan == "annually" && planType === "Premium" ? priceMonthly * users * 12 : ""}
                      {userPlan == "monthly" && planType === "Business" ? priceMonthly * users : ""}
                      {userPlan == "annually" && planType === "Business" ? priceMonthly * 12 * users : ""}
                    </Typography>
                  </div>
                </div>
                <div
                  className={
                    userPlan == "monthly"
                      ? `${classes.teamCorporationContainer} ${classes.billingCyc} ${classes.cstMargin}`
                      : `${classes.teamCorporationContainer} ${classes.billingCyc}`
                  }>
                  <div className={classes.teamSec}>
                    <Typography varian="h3" className={classes.subsDetailsContent}>
                      Billing Cycle
                    </Typography>
                  </div>
                  <div className={classes.monthlyAnnualBtn}>
                    <div className={classes.toggleContainer}>
                      <Typography variant="body1" className={classes.planCycle}>
                        {userPlan}
                      </Typography>
                    </div>
                  </div>
                </div>
                {userPlan == "annually" ? (
                  <div
                    className={classes.teamCorporationContainer}
                    style={{ alignItems: "baseline", marginTop: 15 }}>
                    <div className={classes.teamSec}>
                      <Typography varian="h3" className={classes.subsDetailsContent}>
                        Annual Discount
                      </Typography>
                    </div>
                    <div className={classes.globexCor}>
                      <div className={classes.integerInput}>
                        <Typography variant="body1" className={classes.premiumPrice}>
                          - ${priceMonthly * users * 12 - totalAnnualPrice}
                        </Typography>
                      </div>
                    </div>
                  </div>
                ) : (
                  ""
                )}
                {promoCode ? (
                  <div
                    className={classes.teamCorporationContainer}
                    style={{ alignItems: "baseline" }}>
                    <div className={classes.teamSec}>
                      <Typography varian="h3" className={classes.subsDetailsContent}>
                        Promo Code
                      </Typography>
                    </div>
                    <div className={classes.globexCor}>
                      <div className={classes.integerInput}>
                        <Typography variant="body1" className={classes.premiumPrice}>
                          -({promoCodeDetail.percentOff}%)
                        </Typography>
                      </div>
                    </div>
                  </div>
                ) : null}
                <div className={`${classes.teamCorporationContainer} ${classes.orderSummary}`}>
                  <div className={classes.teamSec}>
                    <Typography
                      varian="h1"
                      className= {classes.fontSize}
                      className={classes.subsDetailsContent}
                      style={{ fontSize: "18px" }}>
                      <b>Total</b>
                    </Typography>
                  </div>
                  {userPlan == "annually" ? (
                    <div className={classes.globexCor}>
                      <Typography varian="h3" className={classes.pricingRate}>
                        <b className={classes.subscriptioncharges} style={{ fontSize: "20px" }}>
                          ${annualPriceAfterDiscount}
                        </b>
                      </Typography>
                    </div>
                  ) : (
                    <div className={classes.globexCor}>
                      <Typography varian="h3" className={classes.pricingRate}>
                        <b className={classes.subscriptioncharges} style={{ fontSize: "20px" }}>
                          ${monthlyPriceAfterDiscount}
                        </b>
                      </Typography>
                    </div>
                  )}
                </div>
              </div>
              <div className={classes.termsNconditions}>
                <div className={classes.continuingContent}>
                  <DefaultCheckbox
                    deafultChecked={false}
                    checked={byContinuing}
                    checkboxStyles={{ padding: "0 8px 0 0" }}
                    onChange={e => handleAgreeTerm(e)}
                  />
                  <Typography variant="body1" className={classes.alreadyAcc}>
                    By continuing, you agree to our
                    <Link
                      href="https://www.ntaskmanager.com/terms-conditions/"
                      className={classes.link} target="_blank">
                      terms and conditions
                    </Link>
                  </Typography>
                </div>
              </div>
              <div className={classes.paySection}>
                <div className={classes.payAmountBtn}>
                  <CustomButton
                    btnType="blue"
                    variant="contained"
                    disabled={!byContinuing || !isCardDetailFilled || !isEmpty(payBtnLoading)}
                    className={classes.continueSummaryBtn}
                    onClick={goToStepWorkReview}
                    query={payBtnLoading}>
                    pay &nbsp;$
                    {userPlan == "annually" ? annualPriceAfterDiscount : monthlyPriceAfterDiscount}
                  </CustomButton>
                </div>
                <div className={classes.stripePayment}>
                  <img src={iconLock} />
                  <Typography className={classes.poweredByContent} variant="body1">
                    100% secure, powered by
                  </Typography>
                  <img src={iconStripe} className={classes.iconStripeImg} />
                </div>
              </div>
              <div className={classes.privacyContentContainer}>
                <div className={`${classes.securityIcons} ${classes.width27}`}>
                  <img src={satisfactionGuaranteed} />
                </div>
                <div className={classes.securityIcons}>
                  <img src={secureSSL} />
                </div>
                <div className={`${classes.securityIcons} ${classes.width25}`}>
                  <img src={privacyProtection} />
                </div>
              </div>
              <div className={classes.privacyContentContainer} style={{ marginTop: 0 }}>
                <div className={`${classes.securityIcons} ${classes.width27}`}>
                  <div>
                    <Typography variant="body1" className={classes.privacyCont}>
                      100% Satisfaction Guaranteed
                    </Typography>
                  </div>
                </div>
                <div className={classes.securityIcons}>
                  <div>
                    <Typography variant="body1" className={classes.privacyCont}>
                      Your Information is Secure
                    </Typography>
                  </div>
                </div>
                <div className={`${classes.securityIcons} ${classes.width25} `}>
                  <div>
                    <Typography variant="body1" className={classes.privacyCont}>
                      We Protect Your Privacy
                    </Typography>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    )
  );
}
export default compose(withRouter, withStyles(paymentStyle, { withTheme: true }))(Payment);
