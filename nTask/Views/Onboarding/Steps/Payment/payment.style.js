const paymentStyle = theme => ({
  fontSize: {
    fontSize: "18px !important"
  },
  paymentViewContainer: {
    width: "100%",
    [theme.breakpoints.between('xs', 'sm')]: {
      width: "92%",
      margin: "0 auto",
      marginTop: 210,
    },
    [theme.breakpoints.between('sm', 'md')]: {
      width: 569,
      margin: "0 auto",
      marginTop: 210,
    },
    [theme.breakpoints.between('md', "lg")]: {
      width: "100%",
      margin: 0
    },
  },
  paymentViewInnerSection: {
    width: 430,
    marginLeft: 119,
    [theme.breakpoints.between('xs', 'sm')]: {
      width: "100%",
      marginLeft: 0,
    },
  },
  typFont: {
    color: theme.palette.text.primary,
    fontSize: "28px !important",
  },
  teamPlanView: {
    marginTop: 28,
  },
  teamPlan: {
    fontSize: "14px !important",
    color: theme.palette.text.primary,
    marginBottom: 4,
    fontWeight: 400,
  },
  paymentHeadingSection: {
    padding: "14px 10px"
  },
  premiumVal: {
    fontSize: "16px !important",
    fontWeight: 700,
  },
  dollorVal: {
    fontSize: "24px !important",
    marginTop: 4,
    fontWeight: 800,
  },
  totalMembers: {
    fontSize: "13px !important",
    color: theme.palette.text.light,
    fontWeight: 400,
  },
  premiumView: {
    textAlign: "center",
    border: `1px solid  #bfbfbf`,
    display: "inline-block",
    flex: 1,
    borderRadius: 4,
    " &:hover": {
      cursor: 'pointer'
    },
  },
  applyStyle: {
    position: "relative",
    textAlign: "center",
    border: `1px solid  ${theme.palette.border.blue}`,
    display: "inline-block",
    flex: 1,
    borderRadius: 4,
    color: theme.palette.text.linkBlue,
    background: `#EFF8FF 0% 0% no-repeat padding-box`,
    "& *": {
      color: theme.palette.text.linkBlue,
    },
  },
  preBusViewContainer: {
    display: "flex",
  },
  subsDetailsContent: {
    fontSize: "14px !important",
    color: theme.palette.text.primary,
    fontFamily: "poppins, sans-serif !important",
  },
  teamNameText: {
    fontSize: "14px !important",
    color: theme.palette.text.primary,
    fontWeight: 400,
    width: 200,
    textAlign: 'right'
  },
  pricingRate: {
    fontSize: "16px !important",
    color: theme.palette.text.grayDarker,
    textAlign: "end",

  },
  totalChargesMA: {
    fontSize: "13px !important",
    color: theme.palette.text.light,
    fontStyle: "italic",
  },
  subscriptionDetCont: {
    fontSize: "16px !important",
    color: theme.palette.text.primary,
    paddingBottom: 5,
    borderBottom: `2px solid  ${theme.palette.border.lightBorder}`,
    marginTop: 39,
    fontFamily: "poppins, sans-serif !important",

  },
  customFontFamily: {

  },
  paymentDetContainer: {
    fontSize: "16px !important",
    color: theme.palette.text.primary,
    paddingBottom: 5,
    marginTop: 37,
  },
  paymentInfo: {
    fontSize: "28px !important",
    color: theme.palette.text.primary,
    fontWeight: 600,
    //   [theme.breakpoints.between('xs', 'sm')]: {
    //     width:324
    // },
  },
  teamCorporationContainer: {
    marginTop: 0,
    display: "inline-flex",
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    alignItems: "flex-start",
  },
  subText: {
    display: "block",
    marginTop: -5,
  },
  cardNumDet: {
    marginTop: 15,
    padding: 15,
    border: `1px solid ${theme.palette.border.lightBorder}`
  },
  integerInput: {
    width: 100,
    height: 36,
    textAlign: "end",
    position: 'relative',
  },
  ToggleButtonSelected: {
    border: "1px solid rgb(0, 144, 255)",
    borderRadius: "6px 0px 0px 6px",
    backgroundColor: "#fff",
  },
  toggleBtnGroup: {
    display: "flex",
    flexWrap: "nowrap",
    background: theme.palette.common.white,
    boxShadow: "none",

    "& $toggleButtonSelected": {
      color: "rgb(0, 144, 255)",
      backgroundColor: "rgb(0 144 255 / 10%)",
      border: "1px solid rgb(0, 144, 255)",
      borderRight: "1px solid rgb(0, 144, 255) !important",
      borderLeft: "1px solid rgb(0, 144, 255) !important",
      borderTopLeftRadius: 4,
      borderBottomLeftRadius: 4,
      fontWeight: 400,

      "&:after": {
        backgroundColor: "rgb(0 144 255 / 10%)",
      },
      "&:hover": {
        backgroundColor: "rgb(0 144 255 / 10%)",
      },
    },
  },
  toggleButtonSelected: {},
  toggleButton: {
    height: 36,
    padding: "18px 21px",
    fontSize: "14px !important",
    fontWeight: 400,
    textTransform: "capitalize",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    [theme.breakpoints.between('xs', 'sm')]: {
      padding: "8px 8px",
    },
    [theme.breakpoints.between('sm', 'md')]: {
      padding: "18px 21px",
    },
    "&:hover": {
      background: theme.palette.common.white,
    },
    "&[value = 'left']": {
      borderTopLeftRadius: 6,
      borderBottomLeftRadius: 6,
      borderRight: "none",
    },
    "&[value = 'right']": {
      borderTopRightRadius: 6,
      borderBottomRightRadius: 6,
      borderLeft: "none",
    },
  },
  disableCharges: {
    textDecoration: "line-through",
    fontWeight: 400,
    marginRight: 5
  },
  subscriptioncharges: {
    color: theme.palette.text.primary,
    fontWeight: 700,
    fontSize: "20px !important"
  },
  crossIconSym: {
    fontSize: "12px !important",
  },
  savePlan: {
    marginTop: 15,
    border: "1px solid #00CC99",
    background: "#D9F8F0",
    borderRadius: 4,
    padding: "7px 180px 6px 12px",
  },
  switchPlan: {
    color: "#00916D",
    fontSize: "13px !important",
    fontWeight: 400,
    [theme.breakpoints.between('xs', 'sm')]: {
      width: "223px",
    },
  },
  orderSummary: {
    borderTop: `2px solid  ${theme.palette.border.lightBorder}`,
    paddingTop: 16,
    marginTop: 0,
    marginBottom: 0,
  },
  link: {
    color: theme.palette.background.btnBlue,
    textDecoration: "underline",
    marginLeft: 5
  },
  orderSummaryContainer: {
    padding: "15px",
    margin: "16px 0 29px 0",
    background: "#EFF8FF",
    borderRadius: 4,
  },
  continuingContent: {
    display: "flex",
    alignItems: "center",
    marginTop: 30,
  },
  continueSummaryBtn: {
    width: "100%",
    padding: "10px",
    fontWeight: 400,
    fontSize: "16px !important",
  },
  stripePayment: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    borderBottom: `1px solid  ${theme.palette.border.lightBorder}`,
    paddingBottom: "22px",
  },
  poweredByContent: {
    padding: "0 5px",
    color: theme.palette.text.primary,
    fontWeight: 400,
    display: "flex",
    alignItems: "baseline",
  },
  privacyContentContainer: {
    display: "flex",
    alignItems: "center",
    marginTop: 20,
    paddingBottom: 20,
    width: "100%",
    justifyContent: "space-between",
  },
  securityIcons: {
    display: "block",
    textAlign: "center",
    width: "33.33%",
  },
  premiumPrice: {
    color: theme.palette.text.primary,

  }, planCycle: {
    color: theme.palette.text.primary,
    textTransform: 'capitalize'
  },
  subscriptionDContainer: {
    marginTop: "39px",
  },
  alreadyAcc: {

    color: theme.palette.text.primary,
    fontSize: "13px !important",
    fontWeight: 400,
  },
  privacyCont: {

    color: theme.palette.text.light,
    fontSize: "13px !important",
  },
  payAmountBtn: {
    margin: "28px 0 22px 0",
  },
  premiumPlan: {
    marginTop: 16,
    alignItems: "baseline",
  },
  billingCyc: {
    marginTop: 0,
    alignItems: "baseline",
  },
  width25: {
    width: "25%",
  },
  width27: {
    width: "27%",
  },
  defaultCheckBox: {
    position: 'absolute',
    display: 'block',
    top: 10,
    left: 10,
    [theme.breakpoints.between('xs', 'ms')]: {
      left: 6
    },
  },
  cstMargin: {
    marginBottom: 14
  },
  promocodeValueCnt: {
    marginTop: 10,
    display: "flex",
    alignItems: "center",
    flexDirection: "row",
    // width: '60%'
  },
  addPromoBtn: {
    textDecoration: 'underline',
    marginTop: 15,
    cursor: 'pointer',
    display: 'block',
    fontSize: "14px !important"
  },
  inputAdorement: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    padding: 0,
    margin: 0,
    marginTop: '-8px',
    marginRight: '-5px',
  },
  inputButton: {
    fontSize: "8px !important",
    padding: 0,
    "&:hover": {
      background: "transparent",
    },
  },
  inputIcon: {
    fontSize: "18px !important",
  },
  adornedEnd:{
    paddingRight:10
  },
  defaultInputLabel: {
    transform: "translate(2px, -17px) scale(1)",
    color: theme.palette.text.primary,
    fontSize: "14px !important",
    display: "flex",
    alignItems: "center",
    fontWeight: theme.typography.fontWeightExtraLight,
  },
  inputAdorementPromoCode:{
    color: theme.palette.background.btnBlue,
    marginRight: 8,
    cursor: "pointer"
  },
  inputAdorementPromoCodeDanger:{
    color: theme.palette.background.danger,
    marginRight: 8,
    cursor: "pointer"
  },
  noArrowNumberField: {
    '-moz-appearance': 'textfield',
    '&::-webkit-outer-spin-button': {
      '-webkit-appearance': 'none',
        margin: 0
    },
    '&::-webkit-inner-spin-button': {
      '-webkit-appearance': 'none',
      margin: 0
    }
  }
});

export default paymentStyle;
