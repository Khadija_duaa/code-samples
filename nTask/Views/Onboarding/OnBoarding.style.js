const onBoardingStyle = theme => ({
    stepsCnt: {
        position:'relative',
        height: '100%',
        '& > div': {
            transition: 'ease all 1s',
            opacity: 0,
            position: 'fixed',
            top: '100%',
            [theme.breakpoints.between('xs', 'sm')]: {
                position: `static !important`,
                display: 'none'
            },
        }
    },
    snackBarHeadingCnt: {
        marginLeft: 10
    },
    snackBarContent: {
        margin: 0,
        fontSize: "12px !important",
    },
    boardingArrowUp: {
        width: "32px",
        height: "32px",
        background: theme.palette.background.btnBlue,
        borderRadius: "50%",
        textAlign: "center",
        position: "fixed",
        bottom: "20px",
        left: "19px",
        zIndex: 1111,
        padding: " 12px 10px",
        display: "flex",
        alignItems: " center",
        justifyContent: "center",
        color:theme.palette.common.white,
        [theme.breakpoints.between('xs', 'sm')]: {
           display:"none",
          },

        "&:hover":{
            background: theme.palette.background.btnBlue,
        }
    },
    onBoardingCnt: {
        position: 'relative',
        overflowY: 'auto',
        height: '100%',
        '& *': {
            fontFamily: 'poppins, sans-serif'
        },
        '& input::placeholder': {
            fontFamily: 'poppins, sans-serif',
            fontSize: "14px !important"
        }
    },

    teamFeatureImages: {
        position: "absolute",
        transform: 'translateY(-15%)',
        width: '50%',
        top: "30%",
        right: 50,
        zIndex: "111111",
        [theme.breakpoints.between('xs', 'sm')]: {
            display:"none",
        },
        [theme.breakpoints.between('sm', 'md')]: {
            display:"none",
        },
    },


})

export default onBoardingStyle;