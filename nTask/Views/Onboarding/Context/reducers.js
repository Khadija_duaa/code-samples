import initialState from "./initialStates";
import {
  SET_USER_PROFILE,
  SET_CREATE_TEAM,
  SET_PAYMENT_INFORMATION,
  SET_WORK_PLAN,
  SET_INVITATION,
  UPDATE_VALUE,
  SET_WORK_REVIEW,
  SET_TEAM_PLAN,
  SET_SEND_INVITE,
  GET_FEATURES_MODULE,
  SET_TEAM_FEATURES,
  SET_INVITEE,
  SEND_STEP_DATA,
  PREV_STEP,
  GET_USER_EMAIL,
  GET_USER_PLAN,
  withoutPaymentSteps,
  inviteUserSteps,
  withPaymentSteps,
  NEXT_STEP,
  SET_QUESTION_ANSWER,
  slideView,
  slideToPrevView,
  SET_PROMO_CODE, REMOVE_PROMO_CODE,
  SET_WHITE_LABELING_DATA
} from "./constants";

function reducer(state = initialState, action) {
  switch (action.type) {
    case SET_WHITE_LABELING_DATA: {
      return { ...state, whiteLabelingData: action.payload }
    }
      break;
    case NEXT_STEP: {
      const nextStep = state.steps.findIndex(s => s.stepTitle == state.step) + 1;
      const nextStepTitle = state.steps[nextStep] ? state.steps[nextStep].stepTitle : ''
      nextStepTitle && slideView(state.step, nextStepTitle);
      return {
        ...state,
        step: nextStepTitle
      }
    }
      break;
    case PREV_STEP: {
      const prevStep = state.steps.findIndex(s => s.stepTitle == state.step) - 1;
      slideToPrevView(state.step, state.steps[prevStep].stepTitle);
      return {
        ...state,
        step: state.steps[prevStep].stepTitle
      }
    }
    case SET_USER_PROFILE: {
      return {
        ...state,
        step: action.payload,
      };
    }
    case SET_PROMO_CODE: {
      return {
        ...state,
        promoCodeDetail: action.payload,
        paymentDetail: { ...state.paymentDetail, promoCode: action.payload.id }
      };
    }
    case REMOVE_PROMO_CODE: {
      return {
        ...state,
        promoCodeDetail: null,
        paymentDetail: { ...state.paymentDetail, promoCode: '' }
      };
    }
    case SET_CREATE_TEAM: {
      return {
        ...state,
        step: action.payload,
      };
    }
    case SET_SEND_INVITE: {
      return {
        ...state,
        step: action.payload,
      };
    }
    case SET_WORK_REVIEW: {
      return {
        ...state,
        step: action.payload,
      };
    }
    case UPDATE_VALUE: {
      return {
        ...state,
        stepsData: { ...state.stepsData, ...action.payload },
      };
    }
    case SET_TEAM_PLAN: {
      return {
        ...state,
        paymentDetail: { ...state.paymentDetail, ...action.payload },
      };
    }
    case GET_FEATURES_MODULE: {
      return {
        ...state,
        features: action.payload,
      };
    }
    case SET_INVITEE: {
      return {
        ...state,
        invitee: action.payload
      }
    }
    case SEND_STEP_DATA: {
      return {
        ...state,
        data: action.payload
      }
    }
    case GET_USER_EMAIL: {
      return {
        ...state,
        stepsData: { ...state.stepsData, email: action.payload }
      }
    }
    case GET_USER_PLAN: {
      const steps = action.payload == 'business' || action.payload == 'premium' ?
        withPaymentSteps :
        action.payload == 'invited' ? inviteUserSteps :
          withoutPaymentSteps;
      // if(action.payload == 'promoCode') {

      // }
      // const finalSteps = steps.filter(step => step.stepTitle != "work");
      // console.log(finalSteps);
      return {
        ...state,
        plan: action.payload,
        steps,
        paymentDetail: { ...state.paymentDetail, planType: action.payload == 'premium' ? "Premium" : action.payload == 'business' ? "Business" : action.payload }
      }
    }
      break;
    case SET_QUESTION_ANSWER: {
      const isAnswerExist = state.workDetail.find(w => w.questionId == action.payload.questionId);
      const updatedAnswers = state.workDetail.map(a => {
        if (a.questionId == action.payload.questionId) {
          return action.payload
        } else {
          return a
        }
      }
      )
      const updatedAnswersState = isAnswerExist ? updatedAnswers : [...state.workDetail, action.payload]
      return {
        ...state,
        workDetail: updatedAnswersState
      }
    }
      break;
    case SET_TEAM_FEATURES: {
      let updatedFeature;
      if (state.selectedFeature.includes(action.payload)) {
        updatedFeature = state.selectedFeature.filter(x => x != action.payload)
      } else {
        updatedFeature = [...state.selectedFeature, action.payload]
      }
      return {
        ...state,
        selectedFeature: updatedFeature,
      };
    }
    default:
      break;
  }
}
export default reducer;
