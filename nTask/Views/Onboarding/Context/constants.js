export const SET_USER_PROFILE = "SET_USER_PROFILE";
export const SET_CREATE_TEAM = "SET_CREATE_TEAM";
export const SET_PAYMENT_INFORMATION = "SET_PAYMENT_INFORMATION";
export const SET_WORK_PLAN = "SET_WORK_PLAN";
export const SET_WORK_REVIEW = "SET_WORK_REVIEW";
export const SET_QUESTION_ANSWER = "SET_QUESTION_ANSWER";
export const SET_INVITATION = "SET_INVITATION";
export const UPDATE_VALUE = "UPDATE_VALUE";
export const SET_TEAM_PLAN = "SET_TEAM_PLAN";
export const GET_FEATURES_MODULE= 'GET_FEATURES_MODULE'
export const SET_SEND_INVITE = 'SET_SEND_INVITE'
export const SET_TEAM_FEATURES = 'SET_TEAM_FEATURES'
export const SET_INVITEE = 'SET_INVITEE'
export const SEND_STEP_DATA = 'SEND_STEP_DATA'
export const GET_USER_EMAIL = 'GET_USER_EMAIL'
export const GET_USER_PLAN = 'GET_USER_PLAN'
export const NEXT_STEP = 'NEXT_STEP'
export const PREV_STEP = 'PREV_STEP'
export const SET_PROMO_CODE = 'SET_PROMO_CODE'
export const REMOVE_PROMO_CODE = 'REMOVE_PROMO_CODE'
export const SET_WHITE_LABELING_DATA = 'SET_WHITE_LABELING_DATA'

export const withPaymentSteps = [
  { stepTitle: "profile" },
  { stepTitle: "team" },
  { stepTitle: "payment" },
  // { stepTitle: "work", child: "features" },
  // { stepTitle: "features", parent: "work" },
  // { stepTitle: "work" },
  { stepTitle: "invite" },
];
export const withoutPaymentSteps = [
  { stepTitle: "profile" },
  { stepTitle: "team" },
  // { stepTitle: "work" },
  // { stepTitle: "work", child: "features" },
  // { stepTitle: "features", parent: "work" },
  { stepTitle: "invite" },
];

export const inviteUserSteps = [
  { stepTitle: "profile" }
];

export const slideView = (currentView, nextView) => {
  if(window.screen.width > 960) {
    document.getElementById(currentView).style.cssText = `
        opacity: 0;
        top: -100vh; 
        position: fixed;
        display: block;
`;
    document.getElementById(nextView).style.cssText = `opacity: 1;display: block; top: 125px; position: absolute`
  } else {
    document.getElementById(currentView).style.cssText = `
        opacity: 0; display: none;
`;
    document.getElementById(nextView).style.cssText = `opacity: 1;display: block;`
  }
}
export const slideToPrevView = (currentView, nextView) => {
  if(window.screen.width > 960) {
    document.getElementById(currentView).style.cssText = `
        opacity: 0;
        top: 100vh
        display: block;
        `;

    document.getElementById(nextView).style.cssText = `opacity: 1; top:125px; position: absolute;display: block;`;
  } else {
    document.getElementById(currentView).style.cssText = `opacity: 0; display: none`
    document.getElementById(nextView).style.cssText = `opacity: 1; display: block;`;
  }
}