import { SET_CREATE_TEAM ,UPDATE_VALUE} from "../constants";

export const createTeam = obj => {
  return { type: SET_CREATE_TEAM, payload: obj };
};
export const createTeamName = obj => {
  return { type: UPDATE_VALUE, payload: obj };
};
export const createWorkspaceName = obj => {
  return { type: UPDATE_VALUE, payload: obj };
};
export const setDataLocation = obj => {
  return { type: UPDATE_VALUE, payload: obj };
};
