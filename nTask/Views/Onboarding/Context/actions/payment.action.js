import { SET_PAYMENT_INFORMATION, SET_TEAM_PLAN, GET_USER_PLAN, REMOVE_PROMO_CODE, SET_PROMO_CODE } from "../constants";
export const setTeamPlan = obj => {
  return { type: SET_TEAM_PLAN, payload: obj };
};

export const userPlan = plan =>{
  return { type:GET_USER_PLAN, payload:plan}
}
export const setPromoCodeAction = (obj) =>{
  return { type: SET_PROMO_CODE, payload: obj };
};
export const removePromoCodeAction = () =>{
  return { type: REMOVE_PROMO_CODE };
};