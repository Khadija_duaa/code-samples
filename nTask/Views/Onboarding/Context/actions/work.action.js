import { SET_WORK_PLAN, SET_DESIRED_FEATURES, SET_WORK_REVIEW,SET_SEND_INVITE, SET_QUESTION_ANSWER} from "../constants";

export const workPlan = (obj) =>{
    return{type:SET_WORK_PLAN, payload:obj}
}
export const workReview = (obj) =>{
    return{type:SET_WORK_REVIEW, payload:obj}
}
export const setQuestionAnswer = (obj) =>{
  return{type:SET_QUESTION_ANSWER, payload:obj}
}
export const sendInvite = ( obj) =>{
   return {type:SET_SEND_INVITE, payload:obj}
} 