import { GET_FEATURES_MODULE,SET_TEAM_FEATURES,GET_USER_EMAIL } from "../constants";
import axios from "axios";
export const getFeaturesData = (dispatch, callBack) => {
  axios.get("https://dev.naxxa.io/api/featuremodule").then(response => {
    callBack(response);
    return dispatch({ type: GET_FEATURES_MODULE, payload: response.data.entities });
  });
};
export const setTeamFeatures  = (obj)=>{
  return({type:SET_TEAM_FEATURES, payload:obj})
}
export const getUserEmail  = (obj)=>{
  return({type:GET_USER_EMAIL, payload:obj})
}