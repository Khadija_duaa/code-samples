import axios from "axios";
import { SET_INVITATION, SET_INVITEE ,SEND_STEP_DATA} from "../constants";
import constants from "../../../../redux/constants/types";
import instance from "../../../../redux/instance";
export const sendInvite = (dispatch, obj) => {
  dispatch({ type: SET_INVITATION, payload: obj });
};

export const saveInvitee = obj => {
  return { type: SET_INVITEE, payload: obj };
};
export const sendStepData = (data, dispatch, success, failure) => {
    axios.post(BASE_URL + "api/Account/onboarding", data)
    .then((response) => {
      switch(data.plan) {
        case 'businesstrial':
          if(window.dataLayer){
            window.dataLayer.push({'event':'BusinessTrial-TeamOwner'});
          }
          break;
        case 'premiumtrial':
          if(window.dataLayer){
            window.dataLayer.push({'event':'PremiumTrial-TeamOwner'});
          }
          break;
        case 'free':
          if(window.dataLayer){
            window.dataLayer.push({'event':'Free'});
          }
          break;
        default:
        // code block
      }
        success(response)
      dispatch({
        type: constants.CHECKAUTH,
        payload: true,
      });
    })
    .catch(error=>{
        failure(error)
    })
};
export const sendWorkInviteData = (data, success, failure) => {
    instance().post("api/Account/onboardingwork", data)
    .then((response) => {
        success(response)
    })
    .catch(error=>{
      failure && failure(error)
    })
};
