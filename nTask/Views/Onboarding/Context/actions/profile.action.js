import { SET_USER_PROFILE,UPDATE_VALUE, SET_WHITE_LABELING_DATA } from "../constants";

export const setUserProfile = (obj) => {
  return { type: SET_USER_PROFILE, payload: obj };
};
export const setFullName = (obj) =>{
  return { type: UPDATE_VALUE, payload: obj };
};
export const setPassword = (obj)=> {
  return { type: UPDATE_VALUE, payload: obj };
};
export const setConfirmPassword = (obj) => {
  return { type: UPDATE_VALUE, payload: obj };
};
export const setWhiteLabelingData = (obj) => {
  return { type: SET_WHITE_LABELING_DATA, payload: obj };
};
