import { SET_CREATE_TEAM ,UPDATE_VALUE, NEXT_STEP, PREV_STEP} from "../constants";

export const createTeam = obj => {
  return { type: SET_CREATE_TEAM, payload: obj };
};
export const createTeamName = obj => {
  return { type: UPDATE_VALUE, payload: obj };
};
export const nextStep = () => {
  return { type: NEXT_STEP };
};
export const prevStep = () => {
  return { type: PREV_STEP };
};
