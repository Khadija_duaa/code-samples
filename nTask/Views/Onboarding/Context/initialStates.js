let moment = require("moment-timezone");

const initialState = {
    step:'profile',
    steps: [],
    stepsData: {
        email:'',
        fullName: '',
        password: '',
        confirmPassword: '',
        teamName: '',
        dataLocation: 'US',
        workspaceName: ''
    },
    plan: 'free',
    timezone: moment.tz.guess(),
    paymentDetail: {
        userPlan:'monthly',
        users:1,
        planType:'Premium',
        byContinuing:'',
        cardName: '',
        stripeToken: '',
        promoCode: ''
    },
    features:[],
    workDetail: [],
    selectedFeature:[],
    invitee:[],
    promoCodeDetail: null,
    whiteLabelingData: null
}
export default initialState;
