// @flow

import React, { useState, useEffect } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import onBoardStyles from "./style";
import { withStyles } from "@material-ui/core/styles";
import CreateProfile from "./CreateProfile";
import InviteToTeam from "./InviteToTeam";
import MobileStepper from "@material-ui/core/MobileStepper";
import { autoPlay } from "react-swipeable-views-utils";
import SwipeableViews from "react-swipeable-views";
import Logo from "../../assets/images/ntask-all-white-logo.svg";
import Slider from "./Slider";
import { validateInvitationToken } from "../../redux/actions/onboarding";
import {
  PopulateWhiteLabelInfo
} from "../../redux/actions/teamMembers";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import queryString from "query-string";
import constants from "../../redux/constants/types";
import axios from "axios";
import { isMobileDevice } from "../../utils/common";

//Onboarding Main Component
function Onboarding(props) {
  const [dialogOpen, setDialogOpen] = useState(true);
  const [onBoardStep, setOnBoardStep] = useState(0);
  const [profileData, setProfileData] = useState({
    teamName: "",
    password: "",
    fullName: "",
    email: "",
    termsChecked: false,
  });
  const [companyInfo, setCompanyInfo] = useState(null);
  const [show, setShow] = useState(false);
  const [isWhiteLabelEnabled , setIsWhiteLabelEnabled] = useState(false);
  (onBoardStep: number);
  (dialogOpen: boolean);
  useEffect(() => {
    const { location } = props;
    const parsedUrl = queryString.parse(location.search);
    if (parsedUrl.token) {
      // Validating if the invitation token is valid or not
      validateInvitationToken(
        parsedUrl.token,
        (response) => {
          if(parsedUrl.iswhitelabelenabled && parsedUrl.iswhitelabelenabled == "True") {
            setIsWhiteLabelEnabled(true);
            getCompanyData(parsedUrl.teamId);
          }
          else {
            setIsWhiteLabelEnabled(false);
            setShow(true);
          }   
        },
        () => {
          props.history.push("/account/login");
        }
      );
    }
    else {
      setShow(true);
      setIsWhiteLabelEnabled(false);
    }
    if (parsedUrl.tempToken) {
      // This case will work when user is redirected from website for social sign up
      localStorage.setItem("tempToken", parsedUrl.tempToken);
    }
  }, []);
  //Function that handles where user has signed up from
  const handleGoogleAnalytics = () => {
    if (window.ga && ENV == "production") {
      // Check is manadatory, incase user is unable to connect to google analytics, ga won't be available and it will through error
      let parseProvider = queryString.parse(props.location.search).provider;
      let provider = parseProvider ? parseProvider : "Web";
      const facebookObj = {
        eventCategory: "Registration",
        eventAction: "Sign Up",
        eventLabel: "Facebook Sign Up",
      };
      const googleObj = {
        eventCategory: "Registration",
        eventAction: "Sign Up",
        eventLabel: "Google Sign Up",
      };
      const twitterObj = {
        eventCategory: "Registration",
        eventAction: "Sign Up",
        eventLabel: "Twitter Sign Up",
      };
      const webObj = {
        eventCategory: "Registration",
        eventAction: "Sign Up",
        eventLabel: "Web Sign Up",
      };
      setTimeout(() => {
        if (window.userpilot && !isWhiteLabelEnabled) {
          userpilot.trigger("1587073772tSad0917");
        }
      }, 2000);
      ga("create", "UA-96014584-2", "auto");
      switch (provider) {
        case "Facebook":
          ga("send", "event", facebookObj);
          if (window.userpilot) {
            /** tracking successfully signup with facebook in app */
            window.userpilot.track("UserPilot New Signups", {
              ...facebookObj,
              email: profileData.email,
            });
          }
          break;
        case "Google":
          ga("send", "event", googleObj);
          if (window.userpilot) {
            /** tracking successfully signup with google in app */
            window.userpilot.track("UserPilot New Signups", {
              ...googleObj,
              email: profileData.email,
            });
          }
          break;
        case "Twitter":
          ga("send", "event", twitterObj);
          if (window.userpilot) {
            /** tracking successfully signup with twitter in app */
            window.userpilot.track("UserPilot New Signups", {
              ...twitterObj,
              email: profileData.email,
            });
          }
          break;
        case "Web":
          ga("send", "event", webObj);
          if (window.userpilot) {
            /** tracking successfully signup with web in app */
            window.userpilot.track("UserPilot New Signups", {
              ...webObj,
              email: profileData.email,
            });
          }
          break;

        default:
          break;
      }
    }
  };
  const getCompanyData = (teamId) => {
    //this.setState({loading: true});
    setShow(false);
    axios.get(`${constants.WHITELABELURL}?teamId=${teamId}`)
    .then(response => {
      if(response.data.success) {
      // localStorage.setItem("companyName",response.data.data.companyName);
      // store.dispatch(PopulateWhiteLabelInfo(response.data.data));
      setCompanyInfo(response.data.data);
      }
      else {
        // store.dispatch(PopulateWhiteLabelInfo(null)); 
        // localStorage.removeItem("companyName");
        
        setCompanyInfo(null);
      }
      // let favIcon = document.getElementById("favicon");
      // favIcon.removeAttribute('href');
      // favIcon.setAttribute('href', `${response.data.data.faviconImageUrl}?v=${new Date().getTime()}`);
      //this.setState({loading: false});
      setShow(true);
    }).catch((response) => { 
      // store.dispatch(PopulateWhiteLabelInfo(null)); 
      // localStorage.removeItem("companyName");
      //this.setState({loading: false});
      setCompanyInfo(null);
      setShow(true);
    });
  };
  const { classes, theme } = props;
  let is_mobile = isMobileDevice();
  return (
    <Dialog
      open={dialogOpen}
      classes={{
        paper: is_mobile ? classes.mobileDialogPaperCnt : classes.dialogPaperCnt,
        scrollBody: classes.dialogCnt,
      }}
      aria-labelledby="form-dialog-title"
      fullWidth={true}
      scroll="body"
    >
      <DialogContent classes={{ root: classes.defaultDialogContent }}>
        <Grid container>
          {!is_mobile && <Grid xs={5} className={classes.onBoardLeftCnt} item>
            <div className={classes.onBoardLeftContentCnt}>
              <div>
                <div style={{width:85, height:45}}> 
                {show ?
                <img
                  src={companyInfo ? companyInfo.headerImageUrl :Logo}
                  alt="Ntask_Logo"
                  // className={classes.nTaskLogo}
                  style={{maxHeight: '100%',maxWidth:'100%'}}
                /> : null}
                </div>
               
                <Typography variant="h1" className={classes.leftHeading}>
                  The Platform Smart <br /> Teams Use To Do More
                </Typography>
                <Typography
                  variant="body2"
                  className={classes.leftHeadingTagline}
                >
                  Take control of your projects and tasks.​
                </Typography>
              </div>
              <div className={classes.onBoardLeftSliderCnt}>
                <div>
                  <Slider />
                </div>
              </div>
            </div>
          </Grid>}
          <Grid xs={is_mobile ? 12 : 7} item>
            {onBoardStep == 0 ? (
              <CreateProfile
                setOnBoardStep={setOnBoardStep}
                profileData={profileData}
                setProfileData={setProfileData}
                setDialogOpen={setDialogOpen}
                handleGoogleAnalytics={handleGoogleAnalytics}
                companyInfo={companyInfo}
                show={show}
              />
            ) : (
              <InviteToTeam
                setOnBoardStep={setOnBoardStep}
                setDialogOpen={setDialogOpen}
                profileData={profileData}
                handleGoogleAnalytics={handleGoogleAnalytics}
              />
            )}
          </Grid>
        </Grid>
      </DialogContent>
    </Dialog>
  );
}

export default compose(
  withRouter,
  withStyles(onBoardStyles, { withTheme: true })
)(Onboarding);
