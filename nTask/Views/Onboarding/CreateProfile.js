// @flow

import React, { useState, useEffect, Fragment } from "react";
import Grid from "@material-ui/core/Grid";
import onBoardStyles from "./style";
import { withStyles } from "@material-ui/core/styles";
import DefaultTextField from "../../components/Form/TextField";
import DefaultCheckbox from "../../components/Form/Checkbox";
import CustomButton from "../../components/Buttons/CustomButton";
import Typography from "@material-ui/core/Typography";
import InputAdornment from "@material-ui/core/InputAdornment";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import IconButton from "@material-ui/core/IconButton";
import { validName } from "../../utils/validator/common/fullName";
import { validPassword } from "../../utils/validator/common/password";
import { validTeamName } from "../../utils/validator/team/teamName";
import { validEmail } from "../../utils/validator/common/email";
import { isEmpty, trim, isLength } from "validator";
import RadioChecked from "@material-ui/icons/RadioButtonChecked";
import RadioUnChecked from "@material-ui/icons/RadioButtonUnchecked";
import queryString from "query-string";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
import { connect } from "react-redux";
import Hotkeys from "react-hot-keys";
import { isTeamUrlValid } from "../../redux/actions/team";
import { createProfile } from "../../redux/actions/onboarding";
import { withSnackbar } from "notistack";
import MobileDevice from "./MobileDevice";
import { isMobileDevice } from "../../utils/common";
let moment = require("moment-timezone");

type CreateProfileProps = {
  classes: any,
  theme: any,
  setOnBoardStep: Function,
  setProfileData: Function,
  profileData: Object,
  location: Object,
  history: Object,
  setDialogOpen: Function,
  createProfile: Function,
  enqueueSnackbar: Function,
};

function CreateProfile(props: CreateProfileProps) {
  const { profileData } = props;
  const [profile, setProfile] = useState({});
  const [mobileDevice, setMobileDevice] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [termsCheckedError, setTermsCheckedError] = useState(false);
  const [userFullName, setFullName] = useState({
    fullName: profileData.fullName,
    fullNameError: false,
    fullNameErrorMsg: "",
  });
  const [userPassword, setPassword] = useState({
    password: profileData.password,
    passwordError: false,
    passwordErrorMsg: "",
  });
  const [userTeamName, setTeamName] = useState({
    teamName: profileData.teamName,
    teamNameError: false,
    teamNameErrorMsg: "",
  });
  const [userEmail, setEmail] = useState({
    email: profileData.email,
    emailError: false,
    emailErrorMsg: "",
  });
  const [btnQuery, setBtnQuery] = useState("");
  const [termsChecked, setTermsChecked] = useState(profileData.termsChecked);
  (btnQuery: string);
  (termsChecked: boolean);
  (showPassword: boolean);
  (termsCheckedError: boolean);

  const showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  const handleShowPassword = () => {
    // toggle visibility of Password
    setShowPassword(!showPassword);
  };
  const handleTermsCheckbox = () => {
    // Toggle terms and condition checkbox
    setTermsChecked(!termsChecked);
    setTermsCheckedError(false);
  };
  //Handle Input
  const handleInput = (
    event: SyntheticKeyboardEvent<HTMLInputElement>,
    type: string
  ) => {
    const value = event.currentTarget.value;
    switch (type) {
      case "fullName": //case will run incase of updating full name
        setFullName({
          ...userFullName,
          fullName: value,
          fullNameError: false, // setting error to false incase error is triggered and user start typing again
          fullNameErrorMsg: "",
        });
        break;
      case "password": //case will run incase of updating password
        setPassword({
          ...userPassword,
          password: value,
          passwordError: false, // setting error to false incase error is triggered and user start typing again
          passwordErrorMsg: "",
        });
        break;
      case "teamName": //case will run incase of updating password
        setTeamName({
          ...userTeamName,
          teamName: value,
          teamNameError: false,
          teamNameErrorMsg: "",
        });
        break;
      case "email": //case will run incase of updating email
        setEmail({
          ...userEmail,
          email: value,
          emailError: false,
          emailErrorMsg: "",
        });
        break;

      default:
        break;
    }
  };
  const validateForm = () => {
    // validate if the form is filled or not
    const parsedUrl = queryString.parse(location.search);
    let validatedFullName = validName(userFullName.fullName);
    let validatePassword = parsedUrl.isExternal
      ? { validated: true }
      : validPassword(userPassword.password);
    let validateTeamName = validTeamName(userTeamName.teamName);
    let validateEmail = validEmail(userEmail.email);
    if (parsedUrl.teamId) {
      // This case will run incase user is new sign up is invited into the team
      return validatedFullName.validated &&
        validatePassword.validated &&
        termsChecked
        ? true
        : false;
    } else {
      //This case will run incase user is a new sign up without any invitation
      return validatedFullName.validated &&
        validatePassword.validated &&
        validateTeamName.validated &&
        validateEmail.validated &&
        termsChecked
        ? true
        : false;
    }
  };
  const submitProfileForm = () => {
    const { setOnBoardStep } = props;
    let validatedFullName = validName(userFullName.fullName);
    let validatePassword = validPassword(userPassword.password);
    let validateTeamName = validTeamName(userTeamName.teamName);
    let validateEmail = validEmail(userEmail.email);

    if (!validatedFullName.validated) {
      // This will run incase name is invalid
      setFullName({
        ...userFullName,
        fullNameError: true,
        fullNameErrorMsg: validatedFullName.errorMessage,
      });
    }
    if (!validateEmail.validated) {
      // This will run incase name is invalid
      setEmail({
        ...userEmail,
        emailError: true,
        emailErrorMsg: validateEmail.errorMessage,
      });
    }
    if (!validatePassword.validated) {
      // This will run incase password is invalid
      setPassword({
        ...userPassword,
        passwordError: true,
        passwordErrorMsg: validatePassword.errorMessage,
      });
    }
    if (!validateTeamName.validated) {
      // This will run incase team name is invalid
      setTeamName({
        ...userTeamName,
        teamNameError: true,
        teamNameErrorMsg: validateTeamName.errorMessage,
      });
    }
    if (!termsChecked) {
      // This will run in case terms and conditions are not checked
      setTermsCheckedError(true);
    }
    if (validateForm()) {
      // If all fields are validated that go to next step

      isTeamUrlValid(userTeamName.teamName, (response) => {
        if (!response.data.isExist) {
          const { setProfileData } = props;
          setProfileData({
            email: email,
            password: userPassword.password,
            teamName: userTeamName.teamName,
            fullName: userFullName.fullName,
            termsChecked: termsChecked,
          });
          setOnBoardStep(1);
        } else {
          setTeamName({
            ...userTeamName,
            teamNameError: true,
            teamNameErrorMsg: response.data.message,
          });
        }
      });
    }
  };

  const handleGetStarted = () => {
    setBtnQuery('progress');
    const { setDialogOpen, handleGoogleAnalytics } = props;
    let validatedFullName = validName(userFullName.fullName);
    let validatePassword = validPassword(userPassword.password);
    const parsedUrl = queryString.parse(location.search);

    if (!validatePassword.validated) {
      // This will run incase password is invalid
      setPassword({
        ...userPassword,
        passwordError: true,
        passwordErrorMsg: validatePassword.errorMessage,
      });
    }
    if (!validatedFullName.validated) {
      // This will run incase name is invalid
      setFullName({
        ...userFullName,
        fullNameError: true,
        fullNameErrorMsg: validatedFullName.errorMessage,
      });
    }
    if (!termsChecked) {
      // This will run in case terms and conditions are not checked
      setTermsCheckedError(true);
    }
    if (validateForm()) {
      // If all fields are validated than submit the form
      let postObject = {
        email: userEmail.email,
        FullName: userFullName.fullName,
        password: userPassword.password,
        token: parsedUrl.token,
        teamId: parsedUrl.teamId,
        TimeZone: moment.tz.guess(),
      };
      props.createProfile(
        postObject,
        () => {
          setBtnQuery("");
          // handleGoogleAnalytics();
          // props.history.push(`/teams/`);
          // setDialogOpen(false); // Closing Onboarding Dialog
          let is_mobile = isMobileDevice();
          if (is_mobile) {
            setMobileDevice(true);
          } else {
            handleGoogleAnalytics();
            props.history.push(`/teams/`);
            setDialogOpen(false); // Closing Onboarding Dialog
          }
        },
        (response) => {
          setBtnQuery("");
          showSnackBar(response.data.message, "error");
        }
      );
    }
  };
  const webAppHandler = () => {
    setMobileDevice(false);
    handleGoogleAnalytics();
    props.history.push(`/teams/`);
    setDialogOpen(false); // Closing Onboarding Dialog
  }
  useEffect(() => {
    const { location } = props;
    const parsedUrl = queryString.parse(location.search);
    const email = parsedUrl.email;
    const fullName = parsedUrl.firstname
      ? `${parsedUrl.firstname} ${parsedUrl.lastname}`
      : "";
    if (email) {
      setEmail({ ...userEmail, email: email });
    }
    if (fullName) {
      setFullName({
        ...userFullName,
        fullName,
      });
    }
  }, []);
  const { fullName, fullNameError, fullNameErrorMsg } = userFullName;
  const { password, passwordError, passwordErrorMsg } = userPassword;
  const { teamName, teamNameError, teamNameErrorMsg } = userTeamName;
  const { email, emailError, emailErrorMsg } = userEmail;
  const { theme, location, companyInfo, show } = props;
  const { classes } = props;
  const parsedUrl = queryString.parse(location.search);
  const companytitle = companyInfo ? companyInfo.companyName : "nTask";
  return (
    <div className={classes.createProfileCnt}>
      {mobileDevice ?
        <MobileDevice
          GoToWebApp={webAppHandler}
        ></MobileDevice> :
        <Fragment>

          <div className={classes.createProfileHeadingCnt}>
            <Typography variant="h1" className={classes.heading}>
              Welcome to {show ? companytitle : ""}
            </Typography>
            <Typography variant="body2" className={classes.headingTagline}>
              Complete your account details
            </Typography>
            <div className={classes.stepsIconCnt}>
              <RadioChecked
                className={classes.activeStepIcon}
                htmlColor={theme.palette.secondary.main}
              />
              {!parsedUrl.teamId && (
                <span className={classes.inactiveStepIcon}></span>
              )}
            </div>
          </div>
          <Hotkeys keyName="enter" onKeyDown={submitProfileForm}>
            <DefaultTextField
              inputType='underline'
              label="Full Name"
              fullWidth={true}
              errorState={fullNameError}
              errorMessage={fullNameErrorMsg}
              defaultProps={{
                id: "firstNameInput",
                placeholder: "Enter your full name (e.g. John Doe)",
                onChange: (event) => handleInput(event, "fullName"),
                value: fullName,
                inputProps: { maxLength: 40, tabIndex: 1 },
              }}
            />
            <DefaultTextField
              label="Email"
              inputType='underline'
              fullWidth={true}
              errorState={emailError}
              errorMessage={emailErrorMsg}
              defaultProps={{
                id: "emailInput",
                placeholder: "Enter your email address",
                onChange: (event) => handleInput(event, "email"),
                value: email,
                inputProps: { maxLength: 40, tabIndex: 1 },
                disabled: parsedUrl.email ? true : false,
              }}
            />
            {!parsedUrl.isExternal ? (
              <DefaultTextField
                label="Create Password"
                inputType='underline'
                fullWidth={true}
                errorState={passwordError}
                errorMessage={passwordErrorMsg}
                defaultProps={{
                  id: "onBoardPasswordInput",
                  placeholder: "Enter your account password",
                  onChange: (event) => handleInput(event, "password"),
                  value: password,
                  inputProps: { maxLength: 40, tabIndex: 2 },
                  type: showPassword ? "text" : "password",
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        disableRipple={true}
                        classes={{
                          root: classes.passwordVisibilityBtn,
                        }}
                        onClick={handleShowPassword}
                      >
                        {showPassword ? <Visibility /> : <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
              />
            ) : null}
            {!parsedUrl.teamId && (
              <DefaultTextField
                label="Team Name"
                fullWidth={true}
                inputType='underline'
                errorState={teamNameError}
                errorMessage={teamNameErrorMsg}
                defaultProps={{
                  id: "onBoardTeamNameInput",
                  placeholder: "Enter your team name (e.g ...)",
                  onChange: (event) => handleInput(event, "teamName"),
                  value: teamName,
                  inputProps: { maxLength: 80, tabIndex: 3 },
                  type: "text",
                }}
              />
            )}
            <DefaultCheckbox
              checked={termsChecked}
              onChange={handleTermsCheckbox}
              label={
                <span
                  style={{
                    color: termsCheckedError ? theme.palette.text.danger : "",
                  }}
                >
                  I agree to the{" "}
                  <a
                    className={classes.termsLink}
                    target="_blank"
                    href="https://www.ntaskmanager.com/terms-conditions/"
                    style={{
                      color: termsCheckedError ? theme.palette.text.danger : "",
                    }}
                  >
                    Terms of Use
                  </a>
                  &nbsp;and{" "}
                  <a
                    className={classes.termsLink}
                    target="_blank"
                    href="https://www.ntaskmanager.com/privacy-policy/"
                    style={{
                      color: termsCheckedError ? theme.palette.text.danger : "",
                    }}
                  >
                    Privacy Policy
                  </a>
                </span>
              }
              checkboxStyles={{ padding: "0 5px 0 0" }}
              styles={{ display: "flex", alignItems: "center" }}
            />{" "}
          </Hotkeys>
          <div className={classes.actionBtnCnt}>
            <CustomButton
              //teamId is url is a check that a new user is invited in a team and signup should be served accordingly
              onClick={parsedUrl.teamId ? handleGetStarted : submitProfileForm}
              btnType="blue"
              variant="contained"
              query={btnQuery}
              disabled={btnQuery == "progress"}
            >
              {parsedUrl.teamId ? "Let's Get Started" : "Continue"}
              {/* teamId is url is a check that a new user is invited in a team and signup should be served accordingly */}
            </CustomButton>
          </div>
        </Fragment>
      }
    </div>
  );
}

export default compose(
  withSnackbar,
  withRouter,
  connect(null, { createProfile }),
  withStyles(onBoardStyles, { withTheme: true })
)(CreateProfile);
