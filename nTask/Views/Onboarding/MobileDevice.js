// @flow
import React, { useState } from "react";
import Grid from "@material-ui/core/Grid";
import onBoardStyles from "./style";
import { withStyles } from "@material-ui/core/styles";
import CustomButton from "../../components/Buttons/CustomButton";
import Typography from "@material-ui/core/Typography";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { withSnackbar } from "notistack";
import AppStoreIcon from "../../assets/images/AppStoreIcon.svg";
import GooglePlayIcon from "../../assets/images/GooglePlayIcon.svg";
import IllustrationIcon from "../../assets/images/IllustrationIcon.svg";

type MobileDeviceProps = {
  classes: Object,
  theme: Object,
  enqueueSnackbar: Function,
};

//Invite to team Component
function MobileDevice(props: MobileDeviceProps) {

  const [btnQuery, setBtnQuery] = useState("")

  const showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  const isAppleDevice = () => {
    return !!navigator.userAgent.toLowerCase().match(/iphone|ipod|ipad/) || false
  }

  const { classes, theme, profileData } = props;

  return (
      <div className={classes.container}>
        <div className={classes.imgCnt}>
            <img src={IllustrationIcon} className={classes.imageStyle} />
        </div>
        <div className={classes.controls}>
          <CustomButton
            //teamId is url is a check that a new user is invited in a team and signup should be served accordingly
            onClick={props.GoToWebApp}
            btnType="blue"
            variant="contained"
            style={{display: 'none'}}
            disabled={btnQuery == "progress"}
          >
            Open in nTask App
          </CustomButton>
          <Typography variant="h4" className={classes.h4Style}>
            Don't have the app?
          </Typography>
          <p className={classes.text_gray}>
              {`Remain connected with work & team members wherever you are, with nTask for ${isAppleDevice() ? 'Apple' : 'Android'}. Download today.`}
          </p>

          <div className={classes.app_button}>
            {isAppleDevice() ? <a href="https://apps.apple.com/us/app/ntask/id1377651443">
              <img src={AppStoreIcon} className={classes.imageStyle} />
            </a>:
            <a href="https://play.google.com/store/apps/details?id=com.ntask.android">
              <img src={GooglePlayIcon} className={classes.imageStyle} />
            </a>}
          </div>
        </div>
      </div>
  );
}

export default compose(
  withRouter,
  withSnackbar,
  connect(null, {
    
  }),
  withStyles(onBoardStyles, { withTheme: true })
)(MobileDevice);
