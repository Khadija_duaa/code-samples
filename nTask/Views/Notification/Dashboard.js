import React, { Component, Fragment } from "react";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import { compose } from "redux";
import { withStyles } from "@material-ui/core/styles";
import notificationStyles from "./style";
import Typography from "@material-ui/core/Typography";
import helper from "../../helper";
import DropdownArrow from "@material-ui/icons/ArrowDropDown";
import NotificationList from "./Notification";
import LeftArrow from "@material-ui/icons/ChevronLeft";
import { calculateContentHeight } from "../../utils/common";
import { Scrollbars } from "react-custom-scrollbars";
import { getNotifications } from "../../redux/actions/notifications";
import {FormattedMessage } from "react-intl";

class NotificationDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notifications: [],
      hasMoreRecords: false,
      pageNumber: 1,
    };
    this.viewMoreRecords = this.viewMoreRecords.bind(this);
    this.handleBackBtn = this.handleBackBtn.bind(this);
  }

  componentDidMount() {
    this.fetchNotifications(this.state.pageNumber);
  }

  fetchNotifications = (pageNumber) => {
    getNotifications(
      pageNumber,
      (data) => {
        const { members } = this.props;

        let notifications = data.notifications.map((notification) => {
          // notification.member = members.find(
          //   (m) => m.userId === notification.createdBy
          // );
          notification.member = notification.member;
          (notification.date = helper.RETURN_CUSTOMDATEFORMATFORCREATEDATE(
            notification.updatedDate || notification.createdDate
          )),
            (notification.time = helper.RETURN_CUSTOMDATEFORMATFORTIME(
              notification.updatedDate || notification.createdDate
            ));
          notification.description = this.messageParts(
            notification.description
          );
          notification.teamName = notification.teamName || 'N/A';
          notification.workspaceName = notification.workSpaceName || 'N/A';
          notification.teamId = notification.teamId;
          notification.workspaceId = notification.workSpaceId;
          return notification;
        });

        const hasMoreRecords =
          this.state.notifications.length + notifications.length <
          data.totalRecords;
        this.setState({
          notifications: this.state.notifications.concat(notifications),
          pageNumber: pageNumber + 1,
          hasMoreRecords,
        });
      },
      (error) => {}
    );
  };

  messageParts = (description) => {
    let msg = description;
    msg = msg.replace(/(<\w+>|<\/\w+>)/g, '"');

    let parts = msg.split(/(")/);
    let convertedString = "",
      isSelected = false;
    for (let i = 0; i < parts.length; i++) {
      if (parts[i] === '"') {
        if (isSelected) {
          isSelected = false;
          convertedString = `${convertedString}</span>`;
        } else {
          isSelected = true;
          convertedString = `${convertedString}<span style="font-weight: bolder;">`;
        }
      } else if (isSelected) convertedString = `${convertedString}${parts[i]} `;
      else convertedString = `${convertedString}${parts[i]}`;
    }
    return convertedString;
  };

  viewMoreRecords() {
    this.fetchNotifications(this.state.pageNumber);
  }

  handleBackBtn() {
    const { history } = this.props;
    if (history.length <= 2) {
      history.push("/tasks");
    } else {
      this.props.history.goBack();
    }
  }

  render() {
    const { classes, theme } = this.props;
    const { notifications, hasMoreRecords } = this.state;

    return (
      <div className={classes.notificationDashboard}>
        <div
          className={`${classes.notificationDashboardHeader} flex_center_space_between_row`}
        >
          <div className="flex_center_start_row">
            <LeftArrow
              onClick={this.handleBackBtn}
              className={classes.backArrowIcon}
            />
            <Typography variant="h1">
              {" "}
              <FormattedMessage
                id="common.notifications.all-notification.label"
                defaultMessage="All Notifications"
              />
            </Typography>
          </div>
        </div>
        <div className={classes.notificationListingCnt}>
          <Scrollbars autoHide style={{ height: calculateContentHeight() }}>
            <div className={classes.notificationList}>
              <NotificationList notifications={notifications} />
              {hasMoreRecords ? (
                <div
                  className={`${classes.viewMoreCnt} flex_center_center_row`}
                  onClick={this.viewMoreRecords}
                >
                  <Typography variant="h5">View More</Typography>

                  <DropdownArrow
                    htmlColor={theme.palette.secondary.medDark}
                  />
                </div>
              ) : null}
            </div>
          </Scrollbars>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    members:
      state.profile && state.profile.data && state.profile.data.member
        ? state.profile.data.member.allMembers
        : [],
  };
};

export default compose(
  withRouter,
  withStyles(notificationStyles, { withTheme: true }),
  connect(mapStateToProps)
)(NotificationDashboard);
