import React, { Component, Fragment } from "react";
import MenuList from "@material-ui/core/MenuList";
import MenuItem from "@material-ui/core/MenuItem";
import { withStyles } from "@material-ui/core/styles";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import notificationStyles from "./style";
import CustomAvatar from "../../components/Avatar/Avatar";
import { connect } from "react-redux";
import { compose } from "redux";
import ReactHtmlParser from 'react-html-parser';
import { generateUsername } from "../../utils/common";
import Typography from "@material-ui/core/Typography";
import { withRouter } from "react-router-dom";
import isEmpty from "lodash/isEmpty";
import AuomationRuleicon from '../../components/Icons/AuomationRuleicon';

class NotificationList extends Component {

  isActiveWS = (item) => {
    if (item.workspaceId && item.workspaceId == this.props.workspaceId && this.props.workspaceLoaded) return true;
    return false;
  }
  isValidURL = (item) => {
    if (item.notificationUrl && item.notificationUrl != "#") return true;
    return false;
  }
  goToNotification = (item) => {
    if (this.isValidURL(item)) {
      let nUrl = new URL(item.notificationUrl)
      if (this.isActiveWS(item)) {
        let n = item.notificationUrl.replace(nUrl.origin, "")
        this.props.history.push(n);
      } else {
        window.location.href = item.notificationUrl;
      }
    }
  };

  render() {
    const { classes, notifications } = this.props;
    return (
      <MenuList disableRipple={true} disablePadding>
        {notifications.map((item, key) => {

          const member = item.member;
          const name = generateUsername(member.email, member.userName, member.fullName, null);

          const title = item.description.indexOf("Notification Alert!") === 0 ? item.description.replace("Notification Alert!", "<b>Notification Alert!</b><br/>")
            .replace("Message:", "<br/><b>Message: </b>")
            .replace("Task:", "<br/><b>Task: </b>")
            .replace(`${item.description.substring(20, item.description.indexOf('Message'))}`, `<b>${item.description.substring(20, item.description.indexOf('Message'))}</b>`)
            : item.description

          return (
            <MenuItem
              key={key}
              classes={{ root: `${classes.notifMenuItem} ${!item.isViewed ? classes.viewedItem : ''}` }}
              onClick={() =>
                this.goToNotification(item)
              }
            >
              <ListItemIcon>
                {item.description.indexOf("Notification Alert") === 0 ?
                  <AuomationRuleicon /> :
                  <CustomAvatar
                    otherMember={{
                      imageUrl: item.pictureUrl,
                      fullName: member.fullName,
                      lastName: '',
                      email: member.email,
                      isOnline: member.isOnline,
                      isOwner: member.isOwner
                    }}

                    size="small"
                  />
                }
              </ListItemIcon>

              <ListItemText classes={{ root: classes.notifMenuItemText }}>
                <p>
                  {/* {ReactHtmlParser(name + " " + item.description)} */}
                  {ReactHtmlParser(title)}
                </p>
                <p className={classes.notifMenuData}>
                  <Typography variant="h6" classes={{ h6: classes.nameCnt }}>{item.teamName}</Typography>
                  <span className={classes.dot}>•</span>
                  <Typography variant="h6" classes={{ h6: classes.nameCnt }}>{item.workspaceName}</Typography>
                </p>
                <p className={classes.notifMenuDate}>
                  {item.date} - {item.time}
                </p>
              </ListItemText>
            </MenuItem>
          )
        })}
      </MenuList>
    );
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    profile: state.profile.data,
    workspaceId: state.profile.data.loggedInTeam,
    workspaceLoaded: !isEmpty(state.workspacePermissions.data.workSpace)
  }
}
export default compose(withRouter, connect(mapStateToProps), withStyles(notificationStyles, { withTheme: true }))(
  NotificationList
);
