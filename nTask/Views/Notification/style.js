const notificationStyles = theme => ({
  notificationDashboard: {
    padding: "20px 40px",
    marginBottom: 70
  },
  notificationListingCnt: {
    background: theme.palette.common.white,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    width: 900,
    margin: "0 auto"
  },
  notificationList: {
    padding: 20
  },
  backArrowIcon: {
    fontSize: "28px !important",
    marginRight: 10,
    cursor: "pointer"

  },
  notificationDashboardHeader: {
    margin: "0 0 20px 0"
  },
  timeSheetHeading: {
    marginRight: 20
  },


  viewMoreCnt: {
    background: theme.palette.common.white,
    borderRadius: 4,
    textAlign: "center",
    padding: 5,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    cursor: "pointer"
  },

  // Notification Menu Styles
  NotifAvatar: {
    borderRadius: "100%",
    width: 36,
    height: 36
  },
  notifMenuItem: {
    height: "auto",
    padding: "15px 0",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    "&:hover": {
      background: "none"
    }
  },

  notifMenuItemText: {
    padding: 0,
    whiteSpace: 'initial',
    "& p": {
      fontSize: "12px !important",
      fontWeight: theme.typography.fontWeightLight,
      color: theme.palette.text.secondary,
      margin: 0,
      "& b": {
        fontWeight: theme.typography.fontWeightMedium,
        color: theme.palette.text.primary
      }
    },
    "& $notifMenuDate": {
      fontSize: "10px !important"
    },
    "& $notifMenuData": {
      fontSize: "10px !important",
      display: 'flex'
    },
  },
  notifMenuDate: {},
  notifMenuData: {},
  dot:{
    color: theme.palette.secondary.medDark,
    margin: '0px 5px',
    fontSize: "24px !important",
    lineHeight: '0.7'
  },
  nameCnt: {
    maxWidth: 200,
    overflow: 'hidden',
    fontSize: "12px !important",
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
  },
  viewedItem: {
    background: theme.palette.background.light
  }
})

export default notificationStyles;