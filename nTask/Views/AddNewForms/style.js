const addNewFormStyles = (theme) => ({
  formActionsCnt: {
    padding: 20,
  },
  formCnt: {
    padding: 20,
  },
  priorityIcon: {
    fontSize: "16px !important",
    marginRight: 5,
  },
  statusIcon: {
    fontSize: "11px !important",
    marginRight: 5,
  },

  addMoreDetailsBtn: {
    fontSize: "13px !important",
    color: theme.palette.text.azure,
    textDecoration: "underline",
    margin: 0,
    cursor: "pointer",
  },
  multipleDropdownCnt: {
    display: "flex",
  },
  descriptionCnt: {
    height: 120,
    resize: "vertical",
    padding: 10,
    borderRadius: "4px",
    overflowY: "auto",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    fontSize: "12px !important",
    "& *": {
      margin: 0,
    },
  },
  editor: {
    height: 120,
    padding: 20,
    fontSize: "12px !important",
    overflowY: "auto",
    borderRadius: 4,
  },
  dropdownsLabel: {
    transform: "translate(6px, -7px) scale(1)",
    display: "block",
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular,
  },
  selectedDropdownsLabel: {
    transform: "translate(6px, -7px) scale(1)",
    display: "block",
    color: theme.palette.text.green,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular,
  },
  dialogFormActionsCnt: {
    padding: 20,
    background: theme.palette.background.paper,
    borderTop: `1px solid ${theme.palette.border.lightBorder}`,
  },
  bugIcon: {
    fontSize: "16px !important",
    marginRight: 5,
  },
  ratingStarIcon: {
    fontSize: "16px !important",
    marginRight: 5,
  },
  ImprovmentIcon: {
    fontSize: "16px !important",
    marginRight: 5,
  },
  impactIcon: {
    fontSize: "14px !important",
    marginRight: 5,
  },
  timeInputCnt: {},
  durationInputCnt: {
    flex: 1,
  },
  listIcon: {
    fontSize: "8px !important",
    color: theme.palette.secondary.light,
    marginRight: 10,
  },
  addMeetingDetailList: {
    listStyleType: "none",
    marginBottom: 20,
    padding: 0,
    "& > li": {
      padding: "8px 10px",
      borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
      fontSize: "12px !important",
      color: theme.palette.text.primary,
      cursor: "text",
      "& p": {
        margin: 0,
        padding: 0,
      },
      "& $CloseIconBtn": {
        marginLeft: 10,
      },
      "&:hover $CloseIconBtn": {
        visibility: "visible",
      },
    },
    "& > $inputListItem": {
      padding: 0,
    },
  },
  CloseIconBtn: {
    visibility: "hidden",
  },
  startTimeContainer: {
    display: "flex",
    alignItems: "center",
    margin: "20px 0",
    width: 350,
  },
  title: {
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular,
  },
  backgroundContainer: {
    display: "flex",
    flexDirection: "row",
    marginTop: 10,
  },
  colorCnt: {
    width: 48,
    height: 48,
    borderRadius: 4,
    marginRight: 10,
  },
  selectedColor: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    color: "white",
  },
  contentClicked: {
    border: "1px solid #35C766",
    background: "#F1FFF5 0% 0% no-repeat padding-box",
    boxShadow: "0px -1px 0px #EAEAEA",
    borderRadius: "0px 0px 4px 4px",
  },
  heading: {
    fontSize: "13px !important",
    fontWeight: 700,
    fontFamily: theme.typography.fontFamilyLato,
    // marginBottom: 5,
    flex: 1,
    marginTop: 5
  },
  keepCardsCnt: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 12
  },
  snackBarBtn:{
    color: "white",
    background: "#0090ff",
    padding: 8,
    borderRadius: 4,
    cursor: "pointer",
    fontSize: "13px !important"
  }
});

export default addNewFormStyles;
