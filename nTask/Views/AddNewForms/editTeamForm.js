import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import onBoardStyles from "../../components/Onboarding/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import DefaultTextField from "../../components/Form/TextField";
import TeamIcon from "@material-ui/icons/SupervisorAccount";
import GlobeIcon from "@material-ui/icons/Language";
import ButtonActionsCnt from "../../components/Dialog/ConfirmationDialogs/ButtonActionsCnt";
import { updateTeam } from "../../redux/actions/profile";
import {injectIntl,FormattedMessage} from "react-intl";

class EditTeamForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      teamName: this.props.activeTeam.companyName,
      teamUrl: this.props.activeTeam.companyUrl,
    };
  }

  handleInput = (fieldName) => (event) => {
    let inputValue = event.target.value;
    //* Replacing all spaces with Dash from team url
    let teamUrl = inputValue.replace(/[\s\+]+/g, "-").toLowerCase();
    switch (fieldName) {
      case "teamName": {
        this.setState({
          [fieldName]: inputValue,
          teamError: false,
          teamErrorMessage: "",
        });
        break;
      }
      case "teamUrl": {
        //Disallowing user to enter space in url
        let newTeamUrl = inputValue.replace(/ /g, "");
        this.setState({
          [fieldName]: teamUrl,
          teamUrlError: false,
          teamUrlErrorMessage: "",
        });
        if (this.state.teamNameBind) {
          this.setState({ teamNameBind: false });
        }
      }
    }
  };

  validInput = (key, value = "") => {
    /**Function for checking if the field is empty or not */
    let validationObj = value == "" ? false : true;
    if (!validationObj) {
      switch (key) {
        case "teamName":
          this.setState({
            teamError: true,
            teamErrorMessage: "Oops! Please enter team name.",
            btnQuery: "",
          });
          break;
        case "teamUrl":
          this.setState({
            teamUrlError: true,
            teamUrlErrorMessage: "Oops! Please enter team url.",
            btnQuery: "",
          });
          break;

        default:
          break;
      }
    }
    return validationObj;
  };

  handleCreateTeam = () => {
    const { teamName, teamUrl } = this.state;
    const { activeTeam } = this.props;

    this.setState({ btnQuery: "progress" });
    let teamNameValue = teamName.trim();
    let teamUrlValue = teamUrl.trim();
    let teamNameValidate = this.validInput("teamName", teamNameValue);
    let teamUrlValidate = this.validInput("teamUrl", teamUrlValue);

    if (teamNameValidate && teamUrlValidate) {
      let newActiveTeamObj = {
        ...activeTeam,
        companyName: teamNameValue,
        companyUrl: teamUrlValue,
      };
      this.props.updateTeam(
        newActiveTeamObj,
        (succ) => {
          this.props.handleDialogClose();
        },
        (err) => {
          // if (err.data.message == "Team URL already exists.") {
          //   this.setState({
          //     teamUrlError: true,
          //     teamUrlErrorMessage: err.data.message,
          //     btnQuery: "",
          //   });
          // } else {
            this.setState({
              teamUrlError: true,
              teamUrlErrorMessage: err.data.message,
              btnQuery: "",
            });
          // }
        }
      );
    }
  };

  render() {
    const { handleDialogClose, theme, classes } = this.props;
    const {
      btnQuery = "",
      teamError = false,
      teamErrorMessage = "",
      teamName,
      teamUrl,
      teamUrlError,
      teamUrlErrorMessage,
    } = this.state;
    const disableButtonAction = teamName == "" && teamUrl == "" ? true : false;
    return (
      <Fragment>
        <div className={classes.innerPaddedCnt}>
          <DefaultTextField
            label={<FormattedMessage id="team-settings.team-creation-dialog.form.name.label" defaultMessage="Team Name" />}
            // helptext={onboardingHelpText.workspaceNameHelpText}
            fullWidth={true}
            errorState={teamError}
            errorMessage={teamErrorMessage}
            defaultProps={{
              id: "teamName",
              onChange: this.handleInput("teamName"),
              placeholder: this.props.intl.formatMessage({id:"team-settings.team-creation-dialog.form.name.label" , defaultMessage: "Team Name"}),
              value: teamName,
              autoFocus: true,
              inputProps: {
                maxLength: 50,
                tabIndex: 1,
                style: { paddingLeft: 0 },
              },
              startAdornment: (
                <InputAdornment position="start">
                  <TeamIcon
                    htmlColor={theme.palette.secondary.dark}
                    className={classes.teamIcon}
                  />
                </InputAdornment>
              ),
            }}
          />
          <DefaultTextField
            label={<FormattedMessage id="team-settings.team-creation-dialog.form.team-url.label" defaultMessage="Team URL"/>}
            // helptext={onboardingHelpText.workspaceUrlHelpText}
            fullWidth={true}
            errorState={teamUrlError}
            errorMessage={teamUrlErrorMessage}
            defaultProps={{
              id: "teamUrl",
              onChange: this.handleInput("teamUrl"),
              placeholder: this.props.intl.formatMessage({id:"team-settings.team-creation-dialog.form.team-url.label",defaultMessage:"team URL"}),
              value: teamUrl,
              inputProps: {
                style: { paddingLeft: 0 },
                maxLength: 80,
                tabIndex: 4,
              },
              startAdornment: (
                <InputAdornment
                  position="start"
                  classes={{ root: classes.urlAdornment }}
                >
                  <GlobeIcon
                    htmlColor={theme.palette.secondary.dark}
                    className={classes.globeIcon}
                  />
                  <p className={classes.workspaceUrlAdornment}>
                  {window.location.origin+"/"}
                  </p>
                </InputAdornment>
              ),
            }}
          />
        </div>
        <ButtonActionsCnt
          cancelAction={handleDialogClose}
          successAction={this.handleCreateTeam}
          successBtnText={
            <FormattedMessage
              id="profile-settings-dialog.signin-security.change-password.form.save-button.label"
              defaultMessage="Save Changes"
            />
          }
          cancelBtnText={
            <FormattedMessage
              id="common.action.delete.confirmation.cancel-button.label"
              defaultMessage="Cancel"
            />
          }
          btnType="success"
          btnQuery={btnQuery}
          disabled={disableButtonAction}
        />
      </Fragment>
    );
  }
}

export default compose(
  injectIntl,
  connect(null, {
    updateTeam,
  }),
  withRouter,
  withStyles(onBoardStyles, { withTheme: true })
)(EditTeamForm);
