import React, { Component, Fragment } from "react";
import { withSnackbar } from "notistack";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import DefaultTextField from "../../components/Form/TextField";
import helper from "../../helper";
import { canDo } from "../../components/workspacePermissions/permissions";
import SelectSearchDropdown from "../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import addNewFormStyles from "./style";
import CreateableSelectDropdown from "../../components/Dropdown/SelectCreateableDropdown/SelectCreateableDropdown";
import ButtonActionsCnt from "../../components/Dialog/ConfirmationDialogs/ButtonActionsCnt";
import { SaveRisk, createRisk } from "../../redux/actions/risks";
import { updateTaskRiskData, updateTaskObject } from "../../redux/actions/tasks";
import { createNewRiskHelpText } from "../../components/Tooltip/helptext";
import { validateTitleField } from "../../utils/formValidations";
import { generateAssigneeData, generateTaskData } from "../../helper/generateSelectData";
import { likelihoodData, impactData } from "../../helper/riskDropdownData";
import isEmail from "validator/lib/isEmail";
import TextEditor from "./TextEditor";
import { validRiskTitle } from "../../utils/validator/risk/riskTitle";
import { FormattedMessage, injectIntl } from "react-intl";
import { getCustomFields } from "../../helper/customFieldsData";
import { v4 as uuidv4 } from "uuid";
import { emptyRisk } from "../../utils/constants/emptyRisk";
import { CanAccessFeature, CanAccess } from "../../components/AccessFeature/AccessFeature.cmp";
import { MixPanelEvents } from '../../mixpanel';
import mixpanel from 'mixpanel-browser';

class AddRiskForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "ganttView",
      AssignEmailError: false,
      riskTitle: "",
      riskTitleError: false,
      riskTitleErrorMessage: null,
      riskOwnerError: false,
      riskOwnerErrorMessage: null,
      disableButton: false,
      btnQuery: "",
      assignee: [],
      likeHood: [],
      impact: impactData(props.theme, props.classes, props.intl).find(i => {
        return i.value == "Moderate";
      }),
      task: [],
      moreDetails: false,
    };
  }
  handleTitle = event => {
    this.setState({
      riskTitle: event.target.value,
      riskTitleError: false,
      riskTitleErrorMessage: null,
    });
  };
  componentDidMount() {
    mixpanel.time_event(MixPanelEvents.RiskCreationEvent)
  }
  handleSubmit = () => {
    let { riskTitle, moreDetails, assignee, impact, description, task, likeHood } = this.state;
    let { currentTask, disableCreateRisk, intl } = this.props;
    //Validating risk Title
    let validateRiskTitle = validRiskTitle(riskTitle, intl);
    if (validateRiskTitle) {
      this.setState({
        riskTitleError: true,
        riskTitleErrorMessage: validateRiskTitle,
      });
      return;
    }
    //Setting state to show loader on submit button
    this.setState({ btnQuery: "progress" });
    //Generate new tasks array from selected data with taskId to save on the backend understanable form
    let taskObj = task.map(a => {
      return a.id;
    });
    //Object made to be posted to the backend understandable form
    let obj = {
      riskOwner: assignee.id,
      linkedTasks: currentTask ? [currentTask.taskId] : taskObj,
      likelihood:
        moreDetails && !likeHood.length ? likeHood.value && likeHood.value.replace(" %", "") : "",
      impact: moreDetails ? impact.value : "",
      detail: moreDetails
        ? description
          ? window.btoa(unescape(encodeURIComponent(description)))
          : null
        : null,
    };
    if (currentTask) {
      this.props.updateTaskObject({ ...currentTask, risks: currentTask.risks + 1 });
    }
    const idClient = uuidv4();
    const postObj = { title: riskTitle, clientId: idClient, ...obj };

    //Dispatch Obj is object that is dispatched before the api call for quick entry
    const dispatchObj = {
      ...emptyRisk,
      title: riskTitle,
      clientId: idClient,
      id: idClient,
      riskId: idClient,
      isNew: true,
      ...obj
    };
    //Calling grid add method to add risk
    this.props.createRisk(
      postObj,
      null,
      dispatchObj,
      succ => {
        mixpanel.track(MixPanelEvents.RiskCreationEvent, succ);
        if (currentTask) {
          disableCreateRisk();
        } else {
          this.props.closeAction();
        }
        this.setState({ btnQuery: "" });
      },
      fail => { }
    );

    //Action Called to save risk object to backend
    // this.props.SaveRisk(
    //   obj,
    //   () => {
    //     if (currentTask) {
    //       disableCreateRisk();
    //     } else {
    //       this.props.closeAction();
    //     }
    //     this.setState({ btnQuery: "" });
    //   },
    //   () => {}
    // );
  };

  //Validate Email function for inviting new members to workspace via createable select
  validateEmail = value => {
    return isEmail(value);
  };
  //Local state is update for dropdown so object can be made to send update to backend with API
  //Updating local state when option is selected in assignee dropdown
  handleOptionsSelect = (type, value) => {
    this.setState({ [type]: value });
  };
  //Local state is update for dropdown so object can be made to send update to backend with API
  //Updating local state when option is selected in assignee dropdown
  handleCreateOptionsSelect = (type, option) => {
    this.setState({ assignee: option });
  };
  //Updating local state when option is created in assignee dropdown
  handleCreateOption = (type, option) => {
    const { newAssignee } = this.state;
    this.setState({ newAssignee: [...newAssignee, option] });
  };
  //Updating local state when select is cleared in project dropdown
  handleClearOption = () => {
    this.setState({ project: [] });
  };
  //Updating local state when option is removed from task dropdown
  handleRemoveOption = (type, option = {}) => {
    const { assignee, newAssignee } = this.state;
    if (option.__isNew__) {
      this.setState({
        newAssignee: newAssignee.filter(a => {
          return a.label !== option.label;
        }),
      });
    } else {
      this.setState({
        assignee: assignee.filter(a => {
          return a.id !== option.id;
        }),
      });
    }
  };
  //Function handles date select dropdown
  handleDateSelect = (startDate, endDate, dateType, startTime, endTime) => {
    this.setState({ startDate, endDate, startTime, endTime });
  };
  //This function toggle show more options
  handleMoreDetailsClick = () => {
    this.setState(prevState => ({ moreDetails: !prevState.moreDetails }));
  };
  //Update content of text editor in local state to send it to backend
  handleDescriptionChange = value => {
    this.setState({ description: value });
  };

  render() {
    const {
      classes,
      theme,
      style,
      workspacePermissionsState,
      members,
      tasks,
      currentTask,
      closeAction,
      disableCreateRisk,
      intl,
      customFields,
      profile,
    } = this.props;
    const {
      riskTitleError,
      riskTitleErrorMessage,
      riskTitle,
      btnQuery,
      assignee,
      likeHood,
      impact,
      task,
      moreDetails,
      description,
    } = this.state;
    let customFieldArr = getCustomFields(customFields, profile, "risk").filter(
      f => f.fieldType == "dropdown"
    );
    const impactDropdown = customFieldArr.find(d => d.fieldName === "Impact") || false;
    const likeDropdown = customFieldArr.find(d => d.fieldName === "Likelihood") || false;
    const taskDropdown = customFieldArr.find(d => d.fieldName === "Task") || false;

    return (
      <Fragment>
        <div style={style} className={classes.formCnt} noValidate autoComplete="off">
          <DefaultTextField
            label={
              <FormattedMessage
                id="risk.creation-dialog.form.risk-title.label"
                defaultMessage="Risk Title"
              />
            }
            helptext={
              <FormattedMessage
                id="risk.creation-dialog.form.risk-title.hint"
                defaultMessage={createNewRiskHelpText.riskTitleHelpText}
              />
            }
            errorState={riskTitleError}
            errorMessage={riskTitleErrorMessage}
            defaultProps={{
              type: "text",
              id: "riskTitle",
              placeholder: intl.formatMessage({
                id: "risk.creation-dialog.form.risk-title.placeholder",
                defaultMessage: "Add Risk Title",
              }),
              value: riskTitle,
              autoFocus: true,
              inputProps: { maxLength: 250 },
              onChange: event => {
                this.handleTitle(event, "riskTitle");
              },
            }}
          />

          <div className={classes.multipleDropdownCnt}>
            <CanAccessFeature group='risk' feature='riskOwner'>
              {canDo(workspacePermissionsState.data.workSpace, "inviteMembers") ? (
                <CreateableSelectDropdown
                  data={() => {
                    return generateAssigneeData(members);
                  }}
                  label="Risk Owner (Optional)"
                  placeholder="Select / Add Risk Owner"
                  name="assignee"
                  styles={{ marginRight: 20, flex: 1 }}
                  selectOptionAction={this.handleCreateOptionsSelect}
                  createOptionAction={this.handleCreateOption}
                  removeOptionAction={this.handleRemoveOption}
                  type="assignee"
                  createText="Invite Member"
                  selectedValue={assignee}
                  avatar={true}
                  createLabelValidation={this.validateEmail}
                  isMulti={false}
                />
              ) : (
                <SelectSearchDropdown
                  data={() => {
                    return generateAssigneeData(members);
                  }}
                  styles={{ marginRight: 20, flex: 1 }}
                  label={
                    <FormattedMessage
                      id="risk.creation-dialog.form.assign-to.label"
                      defaultMessage="Risk Owner (Optional)"
                    />
                  }
                  selectChange={this.handleOptionsSelect}
                  type="assignee"
                  selectedValue={assignee}
                  placeholder={
                    <FormattedMessage id="common.action.select.label" defaultMessage="Select" />
                  }
                  avatar={true}
                  isMulti={false}
                />
              )}
            </CanAccessFeature>
            <CanAccessFeature group='risk' feature='tasks'>
              {currentTask ? null : (
                <SelectSearchDropdown
                  data={() => {
                    return generateTaskData(tasks);
                  }}
                  label={<FormattedMessage id="risk.common.task.label" defaultMessage="Task" />}
                  styles={{ flex: 1 }}
                  selectChange={this.handleOptionsSelect}
                  type="task"
                  selectedValue={task}
                  placeholder={
                    <FormattedMessage
                      id="risk.common.task.placeholder"
                      defaultMessage="Select Task"
                    />
                  }
                />
              )}
            </CanAccessFeature>
          </div>
          {moreDetails ? (
            <>
              <div className={classes.multipleDropdownCnt}>
                {CanAccess({ group: 'risk', feature: 'likelihood' }) && (
                  <SelectSearchDropdown
                    data={() => {
                      return likelihoodData(intl);
                    }}
                    label={
                      <FormattedMessage
                        id="risk.common.likelihood.label"
                        defaultMessage="Likelihood"
                      />
                    }
                    styles={{ marginRight: 20, flex: 1 }}
                    selectChange={this.handleOptionsSelect}
                    type="likeHood"
                    selectedValue={likeHood}
                    placeholder={
                      <FormattedMessage
                        id="risk.common.likelihood.placeholder"
                        defaultMessage="Select Likelihood"
                      />
                    }
                    isMulti={false}
                  />
                )}
                {CanAccess({ group: 'risk', feature: 'impact' }) && (
                  <SelectSearchDropdown
                    data={() => {
                      return impactData(theme, classes, intl);
                    }}
                    label={
                      <FormattedMessage
                        id="risk.creation-dialog.form.impact.label"
                        defaultMessage="Impact"
                      />
                    }
                    selectChange={this.handleOptionsSelect}
                    type="impact"
                    styles={{ flex: 1 }}
                    selectedValue={impact}
                    placeholder={this.props.intl.formatMessage({
                      id: "risk.creation-dialog.form.impact.placeholder",
                      defaultMessage: "Select Impact",
                    })}
                    isMulti={false}
                    icon={true}
                  />
                )}
              </div>

              <CanAccessFeature group='risk' feature='details'>
                <TextEditor changeAction={this.handleDescriptionChange} />
              </CanAccessFeature>
            </>
          ) : null}
          <p className={classes.addMoreDetailsBtn} onClick={this.handleMoreDetailsClick}>
            {moreDetails ? (
              <FormattedMessage
                id="risk.creation-dialog.form.show.show-less.label"
                defaultMessage="Show Less"
              />
            ) : (
              <FormattedMessage
                id="risk.creation-dialog.form.show.show-more.label"
                defaultMessage="Show More"
              />
            )}
          </p>
        </div>

        <ButtonActionsCnt
          cancelAction={closeAction || disableCreateRisk}
          successAction={this.handleSubmit}
          successBtnText={
            <FormattedMessage
              id="risk.creation-dialog.form.create-risk-button.label"
              defaultMessage="Create Risk"
            />
          }
          cancelBtnText={
            <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
          }
          btnType="success"
          btnQuery={btnQuery}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    workspacePermissionsState: state.workspacePermissions,
    members: state.profile.data.member.allMembers,
    tasks: state.tasks.data,
    customFields: state.customFields,
    profile: state.profile.data,
  };
};

export default compose(
  withRouter,
  injectIntl,
  withSnackbar,
  withStyles(addNewFormStyles, {
    withTheme: true,
  }),

  connect(mapStateToProps, {
    SaveRisk,
    updateTaskRiskData,
    updateTaskObject,
    createRisk,
  })
)(AddRiskForm);
