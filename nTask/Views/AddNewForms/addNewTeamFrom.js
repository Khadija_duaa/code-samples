import React, { Component, Fragment } from "react";
import Button from "@material-ui/core/Button";
import CustomButton from "../../components/Buttons/CustomButton";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { withSnackbar } from "notistack";
import { connect } from "react-redux";
import Paper from "@material-ui/core/Paper";
import onBoardStyles from "../../components/Onboarding/styles";
import Typography from "@material-ui/core/Typography";
import InputAdornment from "@material-ui/core/InputAdornment";
import DefaultTextField from "../../components/Form/TextField";
import { WORKSPACE_ICON } from "../../utils/constants/icons";
import TeamIcon from "@material-ui/icons/SupervisorAccount";
import GlobeIcon from "@material-ui/icons/Language";
import { createTeam } from "../../redux/actions/onboarding";
import ButtonActionsCnt from "../../components/Dialog/ConfirmationDialogs/ButtonActionsCnt";
import { FetchUserInfo } from "../../redux/actions/profile";
import { switchTeam, whiteLabelInfo, DefaultWhiteLabelInfo } from "../../redux/actions/teamMembers";
import { FetchWorkspaceInfo } from "../../redux/actions/workspace";
import { isEmpty, isUndefined } from "validator";
import { injectIntl, FormattedMessage } from "react-intl";
class CreateTeamForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      teamNameBind: true,
      teamName: "",
      teamUrl: "",
    };
  }
  handleBack = () => {
    this.props.handleBack(1);
    this.props.history.push("/welcome/company");
  };
  handleInput = (fieldName) => (event) => {
    let inputValue = event.target.value;
    const { teamNameBind } = this.state;
    //* Replacing all spaces with Dash from team url
    let teamUrl = inputValue.replace(/[\s\+]+/g, "-").toLowerCase();
    switch (fieldName) {
      case "teamName": {
        if (teamNameBind) {
          this.setState({
            [fieldName]: inputValue,
            teamError: false,
            teamErrorMessage: "",
            teamNameBind: true,
            teamUrl: teamUrl,
            teamUrlError: false,
            teamUrlErrorMessage: "",
          });
        } else {
          /** */
          this.setState({
            [fieldName]: inputValue,
            teamError: false,
            teamErrorMessage: "",
          });
        }
        break;
      }
      case "teamUrl": {
        //Disallowing user to enter space in url
        let newTeamUrl = inputValue.replace(/ /g, "");
        this.setState({
          [fieldName]: newTeamUrl,
          teamUrlError: false,
          teamUrlErrorMessage: "",
        });
        if (this.state.teamNameBind) {
          this.setState({ teamNameBind: false });
        }
      }
    }
  };
  validInput = (key, value = "") => {
    /**Function for checking if the field is empty or not */
    let validationObj = value == "" ? false : true;
    if (!validationObj) {
      switch (key) {
        case "teamName":
          this.setState({
            teamError: true,
            teamErrorMessage: this.props.intl.formatMessage({ id: "team-settings.team-creation-dialog.form.validation.name.empty", defaultMessage: "Oops! Please enter team name." }),
            btnQuery: "",
          });
          break;
        case "teamUrl":
          this.setState({
            teamUrlError: true,
            teamUrlErrorMessage: this.props.intl.formatMessage({ id: "team-settings.team-creation-dialog.form.validation.team-url.empty", defaultMessage: "Oops! Please enter team url." }),
            btnQuery: "",
          });
          break;

        default:
          break;
      }
    }
    return validationObj;
  };

  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right"
        },
        variant: type ? type : "info"
      }
    );
  };
  handleCreateTeam = () => {
    const { teamName, teamUrl } = this.state;
    const { noTeamFound } = this.props;
    this.setState({ btnQuery: "progress" });
    let teamNameValue = teamName.trim();
    let teamUrlValue = teamUrl.trim();
    let teamNameValidate = this.validInput("teamName", teamNameValue);
    let teamUrlValidate = this.validInput("teamUrl", teamUrlValue);

    if (teamNameValidate && teamUrlValidate) {
      const teamObj = {
        CompanyName: teamNameValue.replace(/\s\s+/g, " "),
        CompanyUrl: teamUrlValue,
      };
      if (noTeamFound) {
        createTeam(
          teamObj,
          (response) => {
            this.props.FetchUserInfo((userApiResponse) => {
              this.props.history.push(
                `/teams/${response.data.team.companyUrl}`
              );
              this.setState({ btnQuery: "" });
            });
          },
          (err) => {
            // if (err.data.message == "Team URL already exists.") {
            //   this.setState({
            //     btnQuery: "",
            //     teamUrlError: true,
            //     teamUrlErrorMessage: this.props.intl.formatMessage({id:"team-settings.team-creation-dialog.form.validation.team-url.exists",defaultMessage:err.data.message}),
            //   });
            // } else {
            this.setState({
              btnQuery: "",
              teamUrlError: true,
              teamUrlErrorMessage: err.data?.message,
            });
            this.showSnackBar(err.data?.message, "error")
            // }
          }
        );
      } else {
        createTeam(
          teamObj,
          //Function to be called in case of success
          (response) => {
            $.connection.hub.stop();

            switchTeam(response.data.team.companyId, (response) => {
              if (response.data.team.isEnableWhiteLabeling && response.data.team.isEnableWhiteLabeling == true)
                this.props.whiteLabelInfo(response.data.team.companyId);
              else
                this.props.DefaultWhiteLabelInfo();
              this.props.FetchUserInfo((userApiResponse) => {
                this.props.history.push(
                  `/teams/${response.data.team.companyUrl}`
                );
                this.setState({ btnQuery: "" });
                this.props.handleDialogClose();
              });
              if (this.props.createTeamHandler) this.props.createTeamHandler();
            });
          },
          //Function to be executed in case of failure
          (err) => {
            // if (err.data.message == "Team URL already exists.") {
            //   this.setState({
            //     btnQuery: "",
            //     teamUrlError: true,
            //     teamUrlErrorMessage: this.props.intl.formatMessage({id:"team-settings.team-creation-dialog.form.validation.team-url.exists",defaultMessage:err.data.message}),
            //   });
            // } else {
            this.setState({
              btnQuery: "",
              teamUrlError: true,
              teamUrlErrorMessage: err.data?.message,
            });
            this.showSnackBar(err.data?.message, "error")
            // }
          }
        );
      }
    }
  };
  render() {
    const {
      handleDialogClose,
      activeStep,
      theme,
      classes,
      onBackClick,
      noTeamFound,
      disableActionBtnBg,
    } = this.props;
    const {
      btnQuery = "",
      teamError = false,
      teamErrorMessage = "",
      teamName = "",
      teamUrl = "",
      teamUrlError,
      teamUrlErrorMessage,
    } = this.state;
    const disableButtonAction = teamName == "" && teamUrl == "" ? true : false;
    return (
      <Fragment>
        <div className={classes.innerPaddedCnt}>
          <DefaultTextField
            label={<FormattedMessage id="team-settings.team-creation-dialog.form.name.label" defaultMessage="Team Name" />}
            // helptext={onboardingHelpText.workspaceNameHelpText}
            fullWidth={true}
            errorState={teamError}
            errorMessage={teamErrorMessage}
            defaultProps={{
              id: "teamName",
              onChange: this.handleInput("teamName"),
              placeholder: this.props.intl.formatMessage({ id: "team-settings.team-creation-dialog.form.name.label", defaultMessage: "Team Name" }),
              value: teamName,
              autoFocus: true,
              inputProps: {
                maxLength: 50,
                tabIndex: 1,
                style: { paddingLeft: 0 },
              },
              startAdornment: (
                <InputAdornment position="start">
                  <TeamIcon
                    htmlColor={theme.palette.secondary.dark}
                    className={classes.teamIcon}
                  />
                </InputAdornment>
              ),
            }}
          />
          {/*<DefaultTextField*/}
          {/*  label={<FormattedMessage id="team-settings.team-creation-dialog.form.team-url.label" defaultMessage="Team URL"/>}*/}
          {/*  // helptext={onboardingHelpText.workspaceUrlHelpText}*/}
          {/*  fullWidth={true}*/}
          {/*  errorState={teamUrlError}*/}
          {/*  errorMessage={teamUrlErrorMessage}*/}
          {/*  defaultProps={{*/}
          {/*    id: "teamUrl",*/}
          {/*    onChange: this.handleInput("teamUrl"),*/}
          {/*    placeholder: this.props.intl.formatMessage({id:"team-settings.team-creation-dialog.form.team-url.label",defaultMessage:"team URL"}),*/}
          {/*    value: teamUrl,*/}
          {/*    inputProps: {*/}
          {/*      style: { paddingLeft: 0 },*/}
          {/*      maxLength: 80,*/}
          {/*      tabIndex: 4,*/}
          {/*    },*/}
          {/*    startAdornment: (*/}
          {/*      <InputAdornment*/}
          {/*        position="start"*/}
          {/*        classes={{ root: classes.urlAdornment }}*/}
          {/*      >*/}
          {/*        <GlobeIcon*/}
          {/*          htmlColor={theme.palette.secondary.dark}*/}
          {/*          className={classes.globeIcon}*/}
          {/*        />*/}
          {/*        <p className={classes.workspaceUrlAdornment}>*/}
          {/*          app.ntaskmanager.com/*/}
          {/*        </p>*/}
          {/*      </InputAdornment>*/}
          {/*    ),*/}
          {/*  }}*/}
          {/*/>*/}
        </div>
        {disableActionBtnBg ? (
          <div className={classes.btnsCnt}>
            <CustomButton
              style={{ marginRight: "20px" }}
              onClick={onBackClick}
              btnType="transparent"
              variant="text"
            >
              <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
            </CustomButton>
            <CustomButton
              onClick={this.handleCreateTeam}
              btnType="success"
              variant="contained"
              btnQuery={btnQuery}
            >
              <FormattedMessage id="team-settings.team-creation-dialog.form.save-button.label" defaultMessage="Create New Team" />
            </CustomButton>
          </div>
        ) : (
          <ButtonActionsCnt
            cancelAction={handleDialogClose || onBackClick}
            successAction={this.handleCreateTeam}
            successBtnText={<FormattedMessage id="team-settings.team-creation-dialog.form.save-button.label" defaultMessage="Create New Team" />}
            cancelBtnText={noTeamFound ? <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" /> : <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />}
            btnType="success"
            btnQuery={btnQuery}
            disabled={disableButtonAction}
          />
        )}
      </Fragment>
    );
  }
}
const mapStateToProps = (state, ownProps) => {
  return {};
};
export default compose(
  withSnackbar,
  connect(mapStateToProps, { FetchWorkspaceInfo, FetchUserInfo, whiteLabelInfo, DefaultWhiteLabelInfo }),
  injectIntl,
  withRouter,
  withStyles(onBoardStyles, { withTheme: true })
)(CreateTeamForm);
