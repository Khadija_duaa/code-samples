import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withSnackbar } from "notistack";
import { SaveMeeting, UpdateMeeting, updateMeetingData } from "../../redux/actions/meetings";
import { updateTaskMeetingData, updateTaskObject } from "../../redux/actions/tasks";
import Grid from "@material-ui/core/Grid";
import { grid } from "../../components/CustomTable2/gridInstance";
import { withStyles } from "@material-ui/core/styles";
import autoCompleteStyles from "../../assets/jss/components/autoComplete";
import combineStyles from "../../utils/mergeStyles";
import DefaultButton from "../../components/Buttons/DefaultButton";
import CustomButton from "../../components/Buttons/CustomButton";
import dialogFormStyles from "../../assets/jss/components/dialogForm";
import DefaultTextField from "../../components/Form/TextField";
import StaticDatePicker from "../../components/DatePicker/StaticDatePicker";
import TimeInput from "../../components/DatePicker/TimeInput";
import DurationInput from "../../components/DatePicker/DurationInput";
import RoundIcon from "@material-ui/icons/Brightness1";
import IconButton from "../../components/Buttons/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import helper from "../../helper";
import moment from "moment";
import NotificationMessage from "../../components/NotificationMessages/NotificationMessages";
// import { Scrollbars } from "react-custom-scrollbars";
import { calculateHeight } from "../../utils/common";
import { createNewMeetingHelpText } from "../../components/Tooltip/helptext";
import SelectSearchDropdown from "../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { generateAssigneeData, generateTaskData } from "../../helper/generateSelectData";
import { generateTaskDropdownData } from "../../helper/meetingDropdownData";
import addNewFormStyles from "./style";
import ButtonActionsCnt from "../../components/Dialog/ConfirmationDialogs/ButtonActionsCnt";
import TimeSelect from "../../components/TimePicker/TimeSelect";
import CreateableSelectDropdown from "../../components/Dropdown/SelectCreateableDropdown/SelectCreateableDropdown";
import { injectIntl } from "react-intl";
import cloneDeep from "lodash/cloneDeep";
import mixpanel from 'mixpanel-browser';
import { MixPanelEvents } from '../../mixpanel';

const locationArr = [{ id: "@zoom", label: "@Zoom" }];
class AddMeetingForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "ganttView",
      AssignEmailError: false,
      time: "12:00 AM",
      duration: "01:00",
      date: helper.RETURN_CUSTOMDATEFORMAT(moment()),
      location: "",
      agenda: "",
      agendaArray: [],
      meetingTitle: "",
      taskId: props.taskId,
      attendeeList: [],
      meetingTitleError: "",
      meetingTitleErrorMessage: "",
      requiredInput: false,
      requiredInputTime: false,
      requiredInputDuration: false,
      disableButton: false,
      btnQuery: "",
      hours: "1",
      mins: "0",
      isChangedDate: true,
      meetingAgendaObjects: [],
      removedMeetingAgendas: [],
      participant:
        props.currentMeeting && props.currentMeeting.attendeeList ? this.getParticipants() : [],
      task: "",
      timeHours: "12",
      timeMinutes: "00",
      timeFormat: "AM",
      selectedLocation: [],
      newLocation: [],
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleTimeChange = this.handleTimeChange.bind(this);
    this.handleDurationChange = this.handleDurationChange.bind(this);
    this.selectedSaveDate = this.selectedSaveDate.bind(this);
    this.generateCheckList = this.generateCheckList.bind(this);
    this.removeAgenda = this.removeAgenda.bind(this);
    this.selectedTask = this.selectedTask.bind(this);
    this.handleSelectedChange = this.handleSelectedChange.bind(this);
    this.getSelectedLocation = this.getSelectedLocation.bind(this);
  }
  componentDidMount() {
    const { currentMeeting, preFilledData, tasks } = this.props;
    if (currentMeeting) {
      const taskData = generateTaskData(tasks);
      const selectedtask = taskData.find(t => {
        return t.id == currentMeeting.taskId;
      });
      this.setState({
        meetingTitle: currentMeeting.meetingDisplayName,
        date: preFilledData
          ? helper.RETURN_CUSTOMDATEFORMAT(moment(preFilledData.meetingDate))
          : helper.RETURN_CUSTOMDATEFORMAT(moment(currentMeeting.endDate)),
        location: currentMeeting.location,
        meetingAgendaObjects: currentMeeting.meetingAgendas,
        task: selectedtask,
        taskId: selectedtask ? selectedtask.id : "",
      });
    }
    if (preFilledData) {
      const {
        calenderView: { view },
        meetingDuration: { hours, minutes },
      } = preFilledData;

      if (view === "week" || view === "day" || view === "month") {
        this.setState({
          hours: view != 'month' ? hours : 1,
          mins: minutes,
          timeHours: preFilledData.startTime.hours,
          timeMinutes: preFilledData.startTime.minutes,
          timeFormat: preFilledData.startTime.meridian,
          time: `${preFilledData.startTime.hours}:${preFilledData.startTime.minutes} ${preFilledData.startTime.meridian}`,
          date: preFilledData
            ? helper.RETURN_CUSTOMDATEFORMAT(moment(preFilledData.meetingDate))
            : helper.RETURN_CUSTOMDATEFORMAT(moment(currentMeeting.endDate)),
        });
      }
    }
  }
  getParticipants = () => {
    let data = this.props.members.filter(item => {
      return this.props.currentMeeting.attendeeList.indexOf(item.userId) != -1;
    });
    return generateAssigneeData(data);
  };
  getLocations = () => {
    const { isZoomLinked, isTeamLinked, isGoogleLinked } = this.props;
    let locationArr = [
      { id: "@zoom", label: "@Zoom", value: "@Zoom" },
      { id: "@microsoftTeams", label: "@Microsoft Teams", value: "@MicrosoftTeams" },
      { id: "@googlemeet", label: "@Google Meet", value: "@googlemeet" },
    ];
    if (!isZoomLinked) locationArr = locationArr.filter(item => item.id !== "@zoom");
    if (!isTeamLinked) locationArr = locationArr.filter(item => item.id !== "@microsoftTeams");
    if (!isGoogleLinked) locationArr = locationArr.filter(item => item.id !== "@googlemeet");
    return locationArr;
  };
  getSelectedLocation = selectedLoc => {
    let selectedLocation = [
      { id: "@zoom", label: "@Zoom", value: "@Zoom" },
      { id: "@microsoftTeams", label: "@Microsoft Teams", value: "@MicrosoftTeams" },
      { id: "@googlemeet", label: "@Microsoft Teams", value: "@googlemeet" },
    ].find(location => {
      return location.id == selectedLoc;
    });

    return selectedLocation
      ? [
        {
          label: selectedLocation.label,
          id: selectedLocation.id,
          obj: selectedLocation,
        },
      ]
      : [];
  };
  handleSelectedChange(key, attendeeList) {
    this.setState({ attendeeList });
  }
  removeAgenda(index) {
    if (index > -1) {
      this.setState({
        agendaArray: this.state.agendaArray.filter((x, i) => i !== index),
      });
    }
  }
  removeMeetingAgenda = agenda => {
    this.setState({
      removedMeetingAgendas: this.state.removedMeetingAgendas.concat(agenda.agendaId),
      meetingAgendaObjects: this.state.meetingAgendaObjects.filter(
        mAgenda => mAgenda.agendaId !== agenda.agendaId
      ),
    });
  };
  selectedTask(taskId) {
    this.setState({ taskId });
  }
  generateCheckList(event) {
    if (event.key === "Enter" && !helper.RETURN_CECKSPACES(this.state.agenda)) {
      this.setState({
        agendaArray: [...this.state.agendaArray, this.state.agenda],
        agenda: "",
      });
    }
  }

  selectedSaveDate(date) {
    if (this.props.preFilledData) {
      this.setState({ isChangedDate: false });
    }

    this.setState({
      date,
      requiredInput: false,
      requiredInputTime: false,
      requiredInputDuration: false,
    });
  }

  clearEffort = (key, value) => {
    this.setState({ [key]: value });
  };
  handleDurationChange = (hrs, mins) => {
    this.setState({
      hours: hrs,
      mins: mins,
      requiredInput: false,
      requiredInputTime: false,
      requiredInputDuration: false,
    });
  };

  handleTimeChange(time) {
    this.setState({
      time,
      requiredInput: false,
      requiredInputTime: false,
      requiredInputDuration: false,
    });
  }

  handleSubmit() {
    const {
      meetingTitle,
      date,
      time,
      duration,
      removedMeetingAgendas,
      isChangedDate,
      task,
      taskId,
    } = this.state;
    const intl = this.props.intl;
    const {
      isAddMeetingDetails,
      userId,
      currentMeeting,
      preFilledData,
      handleCloseMeeting,
      updateTaskMeetingData,
      SaveMeeting,
      updateMeetingData,
      UpdateMeeting,
    } = this.props;

    let flag = 0;
    let checkEmpty = helper.RETURN_CECKSPACES(meetingTitle);

    if (checkEmpty) {
      this.setState({
        meetingTitleError: true,
        meetingTitleErrorMessage: checkEmpty || "Please enter meeting title",
      });
      return;
    }

    if (meetingTitle.length > this.props.maxLength) {
      this.setState({
        meetingTitleError: true,
        meetingTitleErrorMessage: intl.formatMessage({
          id: "meeting.creation-dialog.form.validation.meeting-title.length",
          defaultMessage: "Please enter less than 250 characters",
        }),
      });
      return;
    }

    if (!meetingTitle && !isAddMeetingDetails) {
      flag = 1;
      this.setState({
        meetingTitleError: "Error",
        meetingTitleErrorMessage: intl.formatMessage({
          id: "meeting.creation-dialog.form.validation.meeting-title.label",
          defaultMessage: "Oops! Please enter meeting title",
        }),
      });
    } else {
      this.setState({
        meetingTitleError: "",
        meetingTitleErrorMessage: "",
      });
    }
    if (!date) {
      this.setState({ requiredInput: true });
      flag = 1;
    } else {
      this.setState({ requiredInput: false });
    }
    if (
      !time ||
      time === "0:0 " ||
      time === "0:0 PM" ||
      time === "0:0 AM" ||
      time === "00:00 AM" ||
      time === "00:00 PM"
    ) {
      flag = 1;
      this.setState({ requiredInputTime: true });
      this.showSnackBar(`Oops! Just thought of reminding you to set your meeting time.`, "error");
    } else {
      this.setState({ requiredInputTime: false });
    }
    if (
      !duration ||
      duration === "0:0" ||
      duration === "00:0" ||
      duration === "00:00" ||
      duration === "0:00"
    ) {
      flag = 1;
      this.setState({ requiredInputDuration: true });
    } else {
      this.setState({
        requiredInputDuration: false,
      });
    }
    if (flag) {
      return;
    }
    const object = helper.CALCULATEDURATIONDATETIME(this.state);
    if (currentMeeting) {
      this.setState({ btnQuery: "progress" });

      let curr = currentMeeting;
      // delete object.taskId;
      delete object.meetingDisplayName;
      let updateMeeting = {
        ...curr,
        ...object,
        removedAgendasList: removedMeetingAgendas,
      };

      if (updateMeeting.attendeeList.indexOf(userId) == -1)
        updateMeeting.attendeeList.unshift(userId); /**Add meeting creator User If not exist */
      if (this.state.newLocation && this.state.newLocation.length > 0)
        object.location = this.state.newLocation[0].value;
      else updateMeeting.location = "";
      const { meetingDisplayName, endDateString, endTime,
        ...obj } = object;

      const meeting = currentMeeting;
      updateMeetingData(
        { meeting, obj },
        null,
        //Success
        succ => {
          this.setState({ btnQuery: "", disableButton: false });
          handleCloseMeeting(null, "meetingDialog");
          if (grid.grid) {
            const rowNode = grid.grid && grid.grid.getRowNode(succ.id);
            rowNode.setData(succ);
          }
        },
        err => {
          this.setState({ btnQuery: "", disableButton: false });
          if (err && err.data.url) {
            this.showSnackBar(err.data.message, "error", err.data.url);
          } else this.showSnackBar(err.data.message, "error");
        }
      );
    } else {
      this.setState({ btnQuery: "progress", disableButton: true });

      if (preFilledData && isChangedDate) {
        let meetingDate = helper.RETURN_CUSTOMDATEFORMAT(moment(preFilledData.meetingDate));
        object.startDateString = meetingDate;
      }
      if (this.state.newLocation && this.state.newLocation.length > 0)
        object.location = this.state.newLocation[0].value;
      else object.location = "";
      if (task && taskId) {
        this.props.updateTaskObject({
          ...task,
          meetings: task.meetings + 1,
        }); /** updating count in task */
      }
      SaveMeeting(object, (err, succ) => {
        if (succ) {
          mixpanel.track(MixPanelEvents.MeetingCreationEvent, succ);
          this.setState({ btnQuery: "", disableButton: false });
          handleCloseMeeting(null, "meetingDialog");
          updateTaskMeetingData(data || []);
        } else if (err) {
          this.setState({ btnQuery: "", disableButton: false });
          if (err && err.data.url) {
            this.showSnackBar(err.data.message, "error", err.data.url);
          } else this.showSnackBar(err.data.message, "error");
        }
      });
    }
  }

  showSnackBar = (snackBarMessage, type, url = null) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
        {url && (
          <span
            className={classes.snackBarBtn}
            onClick={() => {
              window.location.href = url;
            }}>
            Reauthorize Microsoft Teams
          </span>
        )}
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  handleChange = (event, key) => {
    this.setState({
      [key]: event.target.value,
      meetingTitleError: "",
      meetingTitleErrorMessage: "",
      requiredInput: false,
      requiredInputTime: false,
      requiredInputDuration: false,
    });
  };
  handleTextFieldChange = event => {
    this.setState({ MeetingTitle: event.target.value });
  };
  //Local state is update for dropdown so object can be made to send update to backend with API
  //Updating local state when option is selected in participant dropdown
  handleOptionsSelect = (type, value) => {
    type == "task"
      ? this.setState({ [type]: value, taskId: value.id })
      : this.setState({ [type]: value });
  };
  handleLocationSelect = (type, value) => {
    this.setState({ [type]: value.id });
  };
  selectClear = (type, value) => {
    type == "task"
      ? this.setState({ [type]: value, taskId: "" })
      : this.setState({ [type]: value });
  };
  clearSelect = (key, value) => {
    this.setState({ [key]: value });
  };
  updateTime = (hours, minutes, timeFormat) => {
    const time = `${hours}:${minutes} ${timeFormat}`;
    this.setState({ time });
  };

  //Local state is update for dropdown so object can be made to send update to backend with API
  //Updating local state when option is selected in location dropdown
  handleLocatinOptionsSelect = (type, option) => {
    this.setState({ newLocation: [option] });
  };
  //Updating local state when option is created in location dropdown
  handleLocatinCreateOption = (type, option) => {
    const { newLocation } = this.state;
    this.setState({ newLocation: [...[], option] });
  };

  //Updating local state when option is removed from location dropdown
  handleLocatinRemoveOption = (type, option = {}) => {
    const { selectedLocation, newLocation } = this.state;
    if (type.__isNew__) {
      this.setState({
        newLocation: newLocation.filter(location => {
          return location.label !== type.label;
        }),
      });
    } else {
      this.setState({
        selectedLocation: selectedLocation.filter(location => {
          return location.id !== type.id;
        }),
      });
    }
  };

  render() {
    const {
      classes,
      theme,
      style,
      isAddMeetingDetails,
      currentMeeting,
      members,
      tasks,
      preFilledData,
      handleCloseMeeting,
      linkMeetingToTask,
      isZoomLinked,
      isTeamLinked,
      isGoogleLinked,
    } = this.props;
    const intl = this.props.intl;
    const {
      meetingTitleError,
      meetingTitleErrorMessage,
      meetingTitle,
      locationError,
      locationErrorMessage,
      location,
      agendaError,
      agendaErrorMessage,
      agenda,
      agendaArray,
      requiredInputTime,
      requiredInputDuration,
      btnQuery,
      meetingAgendaObjects,
      hours,
      mins,
      task,
      participant,
      timeHours,
      timeMinutes,
      timeFormat,
    } = this.state;
    const height = calculateHeight();

    return (
      <Fragment>
        {/* <Scrollbars autoHide style={{ minHeight: height < 750 ? 360 : 627}}> */}
        <div style={style} className={classes.formCnt} noValidate autoComplete="off">
          {isAddMeetingDetails ? (
            <div className="mb20">
              <NotificationMessage type="info" iconType="info">
                {intl.formatMessage({
                  id: "meeting.creation-dialog.form.note.label",
                  defaultMessage: "Before we send out meeting invite, please fill in some details.",
                })}
              </NotificationMessage>
            </div>
          ) : null}
          {isAddMeetingDetails ? null : (
            <DefaultTextField
              label={intl.formatMessage({
                id: "meeting.creation-dialog.form.meeting-title.label",
                defaultMessage: "Meeting Title",
              })}
              helptext={intl.formatMessage({
                id: "meeting.creation-dialog.form.meeting-title.hint",
                defaultMessage: createNewMeetingHelpText.meetingTitleHelpText,
              })}
              errorState={meetingTitleError}
              errorMessage={meetingTitleErrorMessage}
              defaultProps={{
                type: "text",
                id: "meetingTitle",
                placeholder: intl.formatMessage({
                  id: "meeting.creation-dialog.form.meeting-title.placeholder",
                  defaultMessage: "Add Meeting Title",
                }),
                value: meetingTitle,
                autoFocus: true,
                inputProps: { maxLength: 250 },
                onChange: event => {
                  this.handleChange(event, "meetingTitle");
                },
              }}
            />
          )}
          <div className={classes.multipleDropdownCnt}>
            {/* <div className={classes.startTimeContainer}>
            </div> */}

            <StaticDatePicker
              label={intl.formatMessage({
                id: "meeting.creation-dialog.form.date.label",
                defaultMessage: "Date",
              })}
              helptext={intl.formatMessage({
                id: "meeting.creation-dialog.form.date.hint",
                defaultMessage: createNewMeetingHelpText.meetingDateHelpText,
              })}
              placeholder={intl.formatMessage({
                id: "meeting.creation-dialog.form.date.placeholder",
                defaultMessage: "Select Date",
              })}
              isInput={true}
              selectedSaveDate={this.selectedSaveDate}
              isCreation={true}
              preFilledData={preFilledData}
              data={currentMeeting}
              style={{ flex: 1, marginRight: 20 }}
            />
            <div className={classes.timeInputCnt} style={{ marginTop: 5, flex: 1 }}>
              <TimeSelect
                clearSelect={this.clearSelect}
                updateTime={this.updateTime}
                label={intl.formatMessage({
                  id: "meeting.creation-dialog.form.start-time.label",
                  defaultMessage: "Start Time",
                })}
                hours={timeHours}
                mins={timeMinutes}
                timeFormat={timeFormat}
                labelAlign="top"
              />
              {/* <TimeInput
                label="Start Time"
                error={true}
                handleTimeChange={this.handleTimeChange}
                requiredInput={requiredInputTime}
                data={currentMeeting}
                preFilledData={preFilledData}
                view={!isAddMeetingDetails ? "create-meeting" : ""}
              /> */}
            </div>
          </div>
          <div className={classes.multipleDropdownCnt}>
            {linkMeetingToTask ? null /** if add meeting form is called by task details => side tabs => meeting then task dropdown not shows  */ : (
              <SelectSearchDropdown
                data={() => {
                  // console.log(generateTaskData(tasks));
                  return generateTaskDropdownData(currentMeeting);
                }}
                label={intl.formatMessage({
                  id: "meeting.creation-dialog.form.task.label",
                  defaultMessage: "Task (Optional)",
                })}
                styles={{ flex: 1, marginRight: 20 }}
                selectChange={this.handleOptionsSelect}
                selectClear={this.selectClear}
                type="task"
                selectedValue={task}
                isClearable={true}
                placeholder={intl.formatMessage({
                  id: "meeting.creation-dialog.form.task.placeholder",
                  defaultMessage: "Select Task",
                })}
                isMulti={false}
              />
            )}

            <div className={classes.durationInputCnt}>
              <DurationInput
                clearSelect={this.clearEffort}
                label={intl.formatMessage({
                  id: "meeting.creation-dialog.form.duration.label",
                  defaultMessage: "Duration (Hrs / Mins)",
                })}
                error={false}
                hours={hours}
                mins={mins}
                updateTime={this.handleDurationChange}
                requiredInput={requiredInputDuration}
                data={currentMeeting}
                view={!isAddMeetingDetails ? "create-meeting" : "meetingList"}
                hourClock={24}
              />
            </div>
          </div>

          <CreateableSelectDropdown
            data={this.getLocations}
            label={intl.formatMessage({
              id: "meeting.creation-dialog.form.location.label",
              defaultMessage: "Location",
            })}
            placeholder={intl.formatMessage({
              id: "meeting.creation-dialog.form.location.label",
              defaultMessage: "Select / Add Location",
            })}
            id="selectTaskTitleDropdown"
            name="task"
            selectOptionAction={this.handleLocatinOptionsSelect}
            createOptionAction={this.handleLocatinCreateOption}
            removeOptionAction={this.handleLocatinRemoveOption}
            type="zoom"
            isMulti={false}
            createText="Add Location"
            selectedValue={this.getSelectedLocation(location)}
            createLabelValidation={() => {
              return true;
            }}
            noOptionMessage={intl.formatMessage({
              id: "meeting.creation-dialog.form.location.nooption.label",
              defaultMessage: "Add location",
            })}
          />

          {this.props.isNew ? (
            <SelectSearchDropdown
              data={() => {
                return generateAssigneeData(members);
              }}
              styles={{ marginRight: 20, marginBottom: 20, flex: 1 }}
              isClearable={false}
              selectChange={this.handleOptionsSelect}
              label={intl.formatMessage({
                id: "meeting.creation-dialog.form.participant.label",
                defaultMessage: "Participant (Optional)",
              })}
              type="participant"
              selectedValue={participant}
              placeholder={""}
              isMulti={true}
              avatar={true}
            />
          ) : null}

          <DefaultTextField
            label={intl.formatMessage({
              id: "meeting.creation-dialog.form.agenda.label",
              defaultMessage: "Agenda (Optional)",
            })}
            helptext={intl.formatMessage({
              id: "meeting.creation-dialog.form.agenda.hint",
              defaultMessage: createNewMeetingHelpText.meetingAgendaHelpText,
            })}
            errorState={agendaError}
            errorMessage={agendaErrorMessage}
            formControlStyles={{ marginBottom: 0 }}
            error={false}
            defaultProps={{
              type: "text",
              id: "agenda",
              placeholder: intl.formatMessage({
                id: "meeting.creation-dialog.form.agenda.placeholder",
                defaultMessage: "Type and press enter to add agenda",
              }),
              value: agenda,
              inputProps: { maxLength: 1500 },
              //autoFocus: true,
              onKeyUp: this.generateCheckList,
              onChange: event => {
                this.handleChange(event, "agenda");
              },
            }}
          />
          <ul className={classes.addMeetingDetailList}>
            {agendaArray.map((x, i) => {
              return (
                <li className="flex_center_space_between_row" key={i}>
                  <div className="flex_center_start_row">
                    <RoundIcon className={classes.listIcon} />
                    {x}
                  </div>
                  <div className={classes.CloseIconBtn}>
                    <IconButton
                      btnType="smallFilledGray"
                      style={{ padding: 0 }}
                      onClick={this.removeAgenda.bind(this, i)}>
                      <CloseIcon fontSize="small" htmlColor={theme.palette.common.white} />
                    </IconButton>
                  </div>
                </li>
              );
            })}
            {meetingAgendaObjects.map(agenda => {
              return (
                <li className="flex_center_space_between_row" key={agenda.agendaId}>
                  <div>
                    <RoundIcon className={classes.listIcon} />
                    {agenda.description}
                  </div>
                  <div className={classes.CloseIconBtn}>
                    <IconButton
                      btnType="smallFilledGray"
                      style={{ padding: 0 }}
                      onClick={() => this.removeMeetingAgenda(agenda)}>
                      <CloseIcon fontSize="small" htmlColor={theme.palette.common.white} />
                    </IconButton>
                  </div>
                </li>
              );
            })}
          </ul>
        </div>

        <ButtonActionsCnt
          cancelAction={handleCloseMeeting}
          successAction={this.handleSubmit}
          successBtnText={
            isAddMeetingDetails
              ? intl.formatMessage({
                id: "meeting.creation-dialog.form.invite-button.label",
                defaultMessage: "Send Invite",
              })
              : currentMeeting
                ? intl.formatMessage({
                  id: "meeting.creation-dialog.form.update-button.label",
                  defaultMessage: "Update Meeting",
                })
                : intl.formatMessage({
                  id: "meeting.creation-dialog.form.create-button.label",
                  defaultMessage: "Create Meeting",
                })
          }
          cancelBtnText={intl.formatMessage({
            id: "common.action.cancel.label",
            defaultMessage: "Cancel",
          })}
          btnType="success"
          btnQuery={btnQuery}
        />
      </Fragment>
    );
  }
}
AddMeetingForm.defaultProps = {
  maxLength: 250,
};

function mapStateToProps(state) {
  return {
    userId: state.profile.data.userId,
    members: state.profile.data.member.allMembers.filter(
      item => item.userId != state.profile.data.userId
    ),
    tasks: state.tasks.data,
    isZoomLinked: state.profile.data.isZoomLinked,
    isTeamLinked: state.profile.data.isTeamLinked,
    isGoogleLinked: state.profile.data.isGoogleLinked,
  };
}

export default compose(
  injectIntl,
  withRouter,
  withSnackbar,
  withStyles(addNewFormStyles, {
    withTheme: true,
  }),
  connect(mapStateToProps, {
    SaveMeeting,
    UpdateMeeting,
    updateMeetingData,
    updateTaskMeetingData,
    updateTaskObject,
  })
)(AddMeetingForm);
