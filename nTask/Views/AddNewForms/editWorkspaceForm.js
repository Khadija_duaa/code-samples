import React, { Component, Fragment } from "react";
import { withSnackbar } from "notistack";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import autoCompleteStyles from "../../assets/jss/components/autoComplete";
import combineStyles from "../../utils/mergeStyles";
import DefaultButton from "../../components/Buttons/DefaultButton";
import CustomButton from "../../components/Buttons/CustomButton";
import dialogFormStyles from "../../assets/jss/components/dialogForm";
import DefaultTextField from "../../components/Form/TextField";
import { UpdateWorkSpaceData } from "../../redux/actions/workspace";
import { createNewWorkspaceHelpText } from "../../components/Tooltip/helptext";
import GlobeIcon from "@material-ui/icons/Language";
import { FormattedMessage, injectIntl } from "react-intl";

class EditWorkSpaceForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      wsName: this.props.activeWorkspace.teamName,
      wsUrl: this.props.activeWorkspace.teamUrl,
      value: [],
      options: [],
      invalidEmail: false,
      invalidEmailMessage: false,
      AddWorkspaceMember: false,
      invalidWorkspaceName: false,
      invalidWorkspaceUrl: false,
      wsUrlErrorMessage: "",
      wsNameErrorMessage: "",
      wsNameBind: false,
      isFailed: false,
      message: "",
      isButtonDisabled: false,
      disableButton: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  validInput = (key, value = "") => {
    /**Function for checking if the field is empty or not */
    let validationObj = value == "" ? false : true;
    if (!validationObj) {
      switch (key) {
        case "workspaceName":
          this.setState({
            invalidWorkspaceName: true,
            wsNameErrorMessage: this.props.intl.formatMessage({
              id:
                "workspace-settings.general-information.edit-modal.form.validation.worksapce-name.empty",
              defaultMessage: "Oops! Please enter workspace name.",
            }),
            btnQuery: "",
          });
          break;
        case "workspaceUrl":
          this.setState({
            invalidWorkspaceUrl: true,
            wsUrlErrorMessage: this.props.intl.formatMessage({
              id:
                "workspace-settings.general-information.edit-modal.form.validation.workspace-url.empty",
              defaultMessage: "Oops! Please enter valid workspace url.",
            }),
            btnQuery: "",
          });
          break;

        default:
          break;
      }
    }
    return validationObj;
  };
  handleSubmit() {
    const { wsName, wsUrl } = this.state;
    const { activeWorkspace, closeDialog } = this.props;

    this.setState({ btnQuery: "progress" });
    let workspaceNameValue = wsName.trim();
    let workspaceUrlValue = wsUrl.trim();
    let workspaceNameValidate = this.validInput(
      "workspaceName",
      workspaceNameValue
    );
    let workspaceUrlValidate = this.validInput(
      "workspaceUrl",
      workspaceUrlValue
    );

    if (workspaceNameValidate && workspaceUrlValidate) {
      let newActiveWorkspaceObj = {
        ...activeWorkspace,
        teamName: workspaceNameValue,
        teamUrl: workspaceUrlValue,
      };
      this.props.UpdateWorkSpaceData(
        newActiveWorkspaceObj,
        (response) => {
          this.setState({ btnQuery: "" });
          closeDialog();
        },
        (error) => {
          if (error.data.message == "Workspace name already exists.") {
            this.setState({
              invalidWorkspaceName: true,
              wsNameErrorMessage: error.data.message,
              btnQuery: "",
            });
          } else {
            this.setState({
              invalidWorkspaceUrl: true,
              wsUrlErrorMessage: error.data.message,
              btnQuery: "",
            });
          }
        }
      );
    }
  }
  handleChange(event, name) {
    let inputValue = event.currentTarget.value;
    switch (name) {
      case "wsUrl": {
        let wsUrl = inputValue.replace(/ /g, "-");
        this.setState({
          invalidWorkspaceUrl: false,
          wsUrlErrorMessage: "",
          [name]: wsUrl, // Mapping value of textfield to local state,
          disableButton: true,
        });
        break;
      }
      default: {
        this.setState({
          invalidWorkspaceName: false,
          wsNameErrorMessage: "",
          [name]: inputValue, // Mapping value of textfield to local state,
          disableButton: true,
        });
      }
    }
  }

  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  render() {
    const {
      classes,
      theme,
      editNamePer = true,
      editUrlPer = true,
    } = this.props;
    const {
      btnQuery,
      invalidWorkspaceName,
      invalidWorkspaceUrl,
      wsNameErrorMessage,
      wsUrlErrorMessage,
      wsName,
      wsUrl,
      disableButton,
    } = this.state;
    return (
      <Fragment>
        <div className={classes.dialogFormCnt} noValidate autoComplete="off">
          <DefaultTextField
            label={
              <FormattedMessage
                id="workspace-settings.general-information.edit-modal.form.workspace-name.label"
                defaultMessage="Workspace Name"
              />
            }
            helptext={
              <FormattedMessage
                id="workspace-settings.general-information.edit-modal.form.workspace-name.hint"
                defaultMessage={
                  createNewWorkspaceHelpText.workspaceNameHelpText
                }
              />
            }
            errorState={invalidWorkspaceName}
            errorMessage={wsNameErrorMessage}
            defaultProps={{
              type: "text",
              id: "WorkspaceName",
              placeholder: "",
              value: wsName,
              autoFocus: true,
              inputProps: { maxLength: 80 },
              onChange: (event) => {
                this.handleChange(event, "wsName");
              },
              disabled: !editNamePer,
            }}
          />

          <DefaultTextField
            label={
              <FormattedMessage
                id="workspace-settings.general-information.edit-modal.form.workspace-url.label"
                defaultMessage="Workspace URL"
              />
            }
            helptext={
              <FormattedMessage
                id="workspace-settings.general-information.edit-modal.form.workspace-url.hint"
                defaultMessage={createNewWorkspaceHelpText.workspaceUrlHelpText}
              />
            }
            errorState={invalidWorkspaceUrl}
            errorMessage={wsUrlErrorMessage}
            defaultProps={{
              id: "WorkspaceUrl",
              onChange: (event) => {
                this.handleChange(event, "wsUrl");
              },
              placeholder: "",
              type: "text",
              value: wsUrl,
              inputProps: {
                style: { paddingLeft: 0 },
                maxLength: 80,
              },
              startAdornment: (
                <InputAdornment
                  position="start"
                  classes={{ root: classes.urlAdornment }}
                >
                  <GlobeIcon
                    htmlColor={theme.palette.secondary.dark}
                    className={classes.globeIcon}
                  />
                  <p className={classes.workspaceUrlAdornment}>
                    {window.location.origin+"/"}
                  </p>
                </InputAdornment>
              ),
              disabled: !editUrlPer,
            }}
          />
        </div>
        <Grid
          container
          direction="row"
          justify="flex-end"
          alignItems="center"
          classes={{ container: classes.dialogFormActionsCnt }}
        >
          <DefaultButton
            onClick={this.props.closeDialog}
            text={
              <FormattedMessage
                id="common.action.delete.confirmation.cancel-button.label"
                defaultMessage="Cancel"
              />
            }
            buttonType="Transparent"
            style={{ marginRight: 20 }}
          />
          <CustomButton
            onClick={this.handleSubmit}
            btnType="success"
            variant="contained"
            query={btnQuery}
            disabled={btnQuery == "progress" || !disableButton}
          >
            <FormattedMessage
              id="workspace-settings.general-information.edit-modal.form.success-button.label"
              defaultMessage="Save Changes"
            />
          </CustomButton>
        </Grid>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profileState: state.profile,
  };
};

export default compose(
  withRouter,
  withSnackbar,
  injectIntl,
  withStyles(combineStyles(autoCompleteStyles, dialogFormStyles), {
    withTheme: true,
  }),
  connect(mapStateToProps, {
    UpdateWorkSpaceData,
  })
)(EditWorkSpaceForm);
