import React, { Component, Fragment } from "react";
import { withSnackbar } from "notistack";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { isUserPresent } from "../../redux/actions/authentication";
import { SaveProject, FetchProjectsInfo } from "../../redux/actions/projects";
import { SaveTask } from "../../redux/actions/tasks";
import addNewFormStyles from "./style";
import { withStyles } from "@material-ui/core/styles";
import DefaultTextField from "../../components/Form/TextField";
import { createNewTaskHelpText } from "../../components/Tooltip/helptext";
import { canDo } from "../../components/workspacePermissions/permissions";
import { validTaskTitle } from "../../utils/validator/task/taskTitle";
import CreateableSelectDropdown from "../../components/Dropdown/SelectCreateableDropdown/SelectCreateableDropdown";
import { generateAssigneeData, generateProjectData } from "../../helper/generateSelectData";
import SelectSearchDropdown from "../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import isEmail from "validator/lib/isEmail";
import { priorityData, statusData } from "../../utils/constants/taskDropdownData";
import CustomRangeDatePicker from "../../components/DatePicker/CustomRangeDatePicker";
import TextEditor from "./TextEditor";
import ButtonActionsCnt from "../../components/Dialog/ConfirmationDialogs/ButtonActionsCnt";
import queryString from "query-string";
import { teamCanView } from "../../components/PlanPermission/PlanPermission";
import { injectIntl } from "react-intl";
import CircularIcon from "@material-ui/icons/Brightness1";
import { ProjectMandatory } from "../../helper/config.helper";
import isArray from "lodash/isArray";
import { grid } from "../../components/CustomTable2/gridInstance";
import { CanAccess, CanAccessFeature } from "../../components/AccessFeature/AccessFeature.cmp";
import { MixPanelEvents } from '../../mixpanel';
import mixpanel from 'mixpanel-browser';

let taskStatus = ["Not Started", "In Progress", "In Review", "Completed", "Cancelled"];
let taskPriorities = ["Critical", "High", "Medium", "Low"];
// import Uploader from "../Uploader/Uploader"

class AddNewTaskForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      taskTitle: undefined,
      TaskError: false,
      TaskErrorMessage: "",
      assignee: [],
      newAssignee: [],
      project: [],
      startDate: "",
      endDate: "",
      startTime: "",
      endTime: "",
      moreDetails: false,
      calenderDateType: false,
      dateType: "actual",
      priority: priorityData(props.theme, props.classes, props.intl).find(p => {
        return p.value == "Medium";
      }),
      status: this.generateStatusData(this.props.workspaceStatus.statusList)[0],
      statusDropDownData: this.generateStatusData(this.props.workspaceStatus.statusList),
    };
  }
  componentDidMount() {
    mixpanel.time_event(MixPanelEvents.TaskCreationEvent);
    const { preFilledData } = this.props;
    if (preFilledData) {
      const { startDate, endDate, moreDetails, calenderDateType } = preFilledData;
      this.setState({
        startDate,
        endDate,
        moreDetails,
        dateType: calenderDateType == "Planned Start/End" ? "planned" : "actual",
      });
    }
  }
  handleChange = (event, name) => {
    this.setState({
      TaskError: false,
      TaskErrorMessage: "",
      [name]: event.target.value,
    });
  };
  handleSubmit = () => {
    let param = queryString.parse(this.props.location.search);
    let {
      taskTitle,
      moreDetails,
      assignee,
      priority,
      status,
      project,
      endDate,
      startDate,
      description,
      startTime,
      endTime,
      dateType,
    } = this.state;
    //Validating Task Title
    let validateTaskTitle = validTaskTitle(taskTitle, this.props.intl);
    if (validateTaskTitle) {
      this.setState({ TaskError: true, TaskErrorMessage: validateTaskTitle });
      return;
    }
    if (ProjectMandatory() && isArray(project) && project.length == 0) {
      this.showSnackBar('Task cannot be created without the project being selected!',"error");
      return;
    }
    //Setting state to show loader on submit buttongff
    this.setState({ btnQuery: "progress" });
    //Generate new assignee array from selected data with Member ID's to save on the backend understanable form
    let assigneeObj = assignee.map(a => {
      return a.id;
    });
    //Object made to be posted to the backend understandable form
    let obj = {
      taskTitle,
      assigneeList: assigneeObj,
      priority: moreDetails ? taskPriorities.indexOf(priority.value) + 1 : null,
      status: moreDetails
        ? taskStatus.indexOf(status.value) == -1
          ? status.value
          : taskStatus.indexOf(status.value)
        : null,
      projectId: project.id,
      actualStartDate: startDate && dateType == "actual" ? (typeof startDate === "object" ? `${startDate.getMonth() + 1}-${startDate.getDate()}-${startDate.getFullYear()}` : startDate) : null,
      actualDueDate: endDate && dateType == "actual" ? (typeof endDate === "object" ? `${endDate.getMonth() + 1}-${endDate.getDate()}-${endDate.getFullYear()}` : endDate) : null,
      startDate: startDate && dateType == "planned" ? (typeof startDate === "object" ? `${startDate.getMonth() + 1}-${startDate.getDate()}-${startDate.getFullYear()}` : startDate) : null,
      dueDate: endDate && dateType == "planned" ? (typeof endDate === "object" ? `${endDate.getMonth() + 1}-${endDate.getDate()}-${endDate.getFullYear()}` : endDate) : null,
      description: moreDetails
        ? description
          ? window.btoa(unescape(encodeURIComponent(description)))
          : null
        : null,
    };
    //Action Called to save task object to backend
    this.props.SaveTask(
      obj,
      response => {
        mixpanel.track(MixPanelEvents.TaskCreationEvent, {
          workspaceName: response.task.workspaceName,
          taskTitle: response.task.taskTitle,
        });
        // mixpanel.alias('new_id', 'existing_id');
        //Success Function
        //Checking if the user is on gantt view and in response of task creation api the gantt object is there or not
        if (param.projectId && response.taskDTo) {
          // Adding task to gantt
          this.addTaskToGantt([response.taskDTo]);
        }
        this.props.closeAction();
        this.setState({ btnQuery: "" });
        this.setState({ btnQuery: "" },()=>{
          this.showSnackBar('Task created successfully',"success");
          });
        setTimeout(() => {
          grid.grid && grid.grid.redrawRows();
        }, 0);
      },
      err => {
        this.setState({ btnQuery: "" }, () => {
          this.showSnackBar(err.data.message, "error");
        });
      },
      param
    );
  };
  //Function that adds task to Gantt
  addTaskToGantt = taskArr => {
    const plannedVsActual = this.props.ganttComponent.plannedVsActual;
    let param = queryString.parse(this.props.location.search);
    if (param.projectId && taskArr[0].projectId == param.projectId) {
      taskArr.forEach(task => {
        let startDate;
        let duration;
        let actualStartDate = task.start_date ? new Date(task.start_date) : null;
        let actualEndDate = task.start_date
          ? new Date(
            moment(task.start_date)
              .clone()
              .add(task.duration, "days")
          )
          : null;
        let plannedStartDate = task.plannedstartdate ? new Date(task.plannedstartdate) : null;
        let plannedEndDate = task.plannedstartdate
          ? new Date(
            moment(task.plannedstartdate)
              .clone()
              .add(task.plannedDuration, "days")
          )
          : null;

        if (plannedVsActual == "Planned" || plannedVsActual == "Planned vs Actual") {
          startDate = task.plannedstartdate ? new Date(task.plannedstartdate) : null;
          duration = task.plannedDuration;
        } else {
          startDate = task.start_date ? new Date(task.start_date) : null;
          duration = task.duration;
        }
        this.props.ganttComponent.gantt.addTask(
          {
            id: task.taskId,
            sortorder: task.sortorder,
            text: task.text,
            start_date: startDate,
            // open: true,
            duration: duration,
            actual_start: actualStartDate,
            unscheduled:
              plannedVsActual == "Planned" && task.plannedstartdate
                ? false
                : plannedVsActual == "Actual" && task.start_date
                  ? false
                  : plannedVsActual == "Planned vs Actual" && task.plannedstartdate
                    ? false
                    : true,
            actual_end: actualEndDate,
            progress: task.progress / 100,
            type: task.type,
            status: task.status,
            issues: task.issues,
            risks: task.risks,
            planned_start: plannedStartDate,
            planned_end: plannedEndDate,
            estiTime: task.estimatedEffort,
            actTime: task.taskEffort,
            assignee: [],
            priority: task.priority,
            attachments: task.taskAttachments,
            comments: task.comments,
            cost: task.cost,
            creationDate: new Date(task.createdDate),
            meetings: task.meetings,
          },
          task.parent == "0" || !task.parent ? 0 : task.parent,
          0
        );
      });
    }
  };
  //Validate Email function for inviting new members to workspace via createable select
  validateEmail = value => {
    return isEmail(value);
  };
  //Local state is update for dropdown so object can be made to send update to backend with API
  //Updating local state when option is selected in assignee dropdown
  handleOptionsSelect = (type, value) => {
    if (type == "project") {
      let statuses = value.obj.projectTemplate
        ? value.obj.projectTemplate.statusList
        : this.props.workspaceStatus.statusList;
      let newDData = this.generateStatusData(statuses);
      this.setState({ [type]: value, status: newDData[0], statusDropDownData: newDData });
    } else {
      this.setState({ [type]: value });
    }
  };
  //Local state is update for dropdown so object can be made to send update to backend with API
  //Updating local state when option is selected in assignee dropdown
  handleCreateOptionsSelect = (type, option) => {
    this.setState({ assignee: option });
  };
  //Updating local state when option is created in assignee dropdown
  handleCreateOption = (type, option) => {
    const { newAssignee } = this.state;
    this.setState({ newAssignee: [...newAssignee, option] });
  };
  //Updating local state when select is cleared in project dropdown
  handleClearOption = () => {
    this.setState({
      project: [],
      status: this.generateStatusData(this.props.workspaceStatus.statusList)[0],
    });
  };
  //Updating local state when option is removed from task dropdown
  handleRemoveOption = (type, option = {}) => {
    const { assignee, newAssignee } = this.state;
    if (option.__isNew__) {
      this.setState({
        newAssignee: newAssignee.filter(a => {
          return a.label !== option.label;
        }),
      });
    } else {
      this.setState({
        assignee: assignee.filter(a => {
          return a.id !== option.id;
        }),
      });
    }
  };
  //Function handles date select dropdown
  handleDateSelect = (startDate, endDate, dateType, startTime, endTime) => {
    this.setState({ startDate, endDate, startTime, endTime, dateType });
  };
  //This function toggle show more options
  handleMoreDetailsClick = () => {
    this.setState(prevState => ({ moreDetails: !prevState.moreDetails }));
  };
  //Update content of text editor in local state to send it to backend
  handleDescriptionChange = value => {
    this.setState({ description: value });
  };
  generateStatusData = (statusArr = false) => {
    if (statusArr) {
      return statusArr.map(item => {
        return {
          label: item.statusTitle,
          value: item.statusId,
          icon: (
            <CircularIcon htmlColor={item.statusColor} style={{ fontSize: "12px", marginRight: 5 }} />
          ),
          statusColor: item.statusColor,
        };
      });
    } else return [];
  };
  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type || "info",
      }
    );
  };
  render() {
    const { classes, theme, closeAction, projects, members, companyInfo } = this.props;
    const intl = this.props.intl;

    const {
      btnQuery,
      TaskError,
      TaskErrorMessage,
      assignee,
      newAssignee,
      project,
      priority,
      status,
      startDate,
      endDate,
      startTime,
      endTime,
      description,
      moreDetails,
      statusDropDownData,
      dateType,
    } = this.state;
    const taskLabelSingle = companyInfo?.task?.sName;
    const taskLabelMulti = companyInfo?.task?.pName;
    return (
      <Fragment>
        <div className={classes.formCnt} noValidate autoComplete="off">
          <DefaultTextField
            label={intl.formatMessage({
              id: "task.creation-dialog.form.title.label",
              defaultMessage: "Title",
            })}
            errorState={TaskError}
            errorMessage={TaskErrorMessage}
            helptext={intl.formatMessage({
              id: "task.creation-dialog.form.title.hint",
              defaultMessage: createNewTaskHelpText.taskTitleHelpText,
            })}
            defaultProps={{
              type: "text",
              id: "TaskTitle",
              placeholder: intl.formatMessage({
                id: "task.creation-dialog.form.title.placeholder",
                defaultMessage: "Title",
              }),
              value: this.state.taskTitle,
              autoFocus: true,
              inputProps: { maxLength: 250 },
              onChange: event => {
                this.handleChange(event, "taskTitle");
              },
            }}
          />

          <CanAccessFeature group='task' feature='assigneeList'>
            {canDo(this.props.workspacePermissionsState.data.workSpace, "inviteMembers") ? (
              <CreateableSelectDropdown
                data={() => {
                  return generateAssigneeData(members);
                }}
                label="Assigned To (Optional)"
                placeholder="Select / Add Assignee"
                name="assignee"
                selectOptionAction={this.handleCreateOptionsSelect}
                createOptionAction={this.handleCreateOption}
                removeOptionAction={this.handleRemoveOption}
                type="assignee"
                createText="Invite Member"
                selectedValue={assignee}
                avatar={true}
                createLabelValidation={this.validateEmail}
              />
            ) : (
              <SelectSearchDropdown
                data={() => {
                  return generateAssigneeData(members);
                }}
                label={intl.formatMessage({
                  id: "task.creation-dialog.form.assign-to.label",
                  defaultMessage: "Assigned To (Optional)",
                })}
                selectChange={this.handleOptionsSelect}
                type="assignee"
                selectedValue={assignee}
                placeholder={intl.formatMessage({
                  id: "task.creation-dialog.form.assign-to.label",
                  defaultMessage: "Select Assignee",
                })}
                avatar={true}
              />
            )}
          </CanAccessFeature>
          <div className={classes.multipleDropdownCnt}>
            {teamCanView("projectAccess") && (
              <CanAccessFeature group='task' feature='project'>
                <SelectSearchDropdown
                  data={() => {
                    return generateProjectData(projects);
                  }}
                  styles={{ marginRight: 20, flex: 1 }}
                  isClearable={true}
                  label={ProjectMandatory() ? "Project (Required)" : intl.formatMessage({
                    id: "task.creation-dialog.form.project.label",
                    defaultMessage: "Project (Optional)",
                  })}
                  selectChange={this.handleOptionsSelect}
                  selectClear={this.handleClearOption}
                  type="project"
                  selectedValue={project}
                  placeholder={intl.formatMessage({
                    id: "task.creation-dialog.form.project.placeholder",
                    defaultMessage: "Select Project",
                  })}
                  isMulti={false}
                />
              </CanAccessFeature>
            )}
            {moreDetails ? (
              <CanAccessFeature group='task' feature='priority'>
                <SelectSearchDropdown
                  data={() => {
                    return priorityData(theme, classes, intl);
                  }}
                  label={intl.formatMessage({
                    id: "task.creation-dialog.form.priority.label",
                    defaultMessage: "Priority",
                  })}
                  styles={{ flex: 1 }}
                  selectChange={this.handleOptionsSelect}
                  type="priority"
                  selectedValue={priority}
                  placeholder={intl.formatMessage({
                    id: "task.creation-dialog.form.priority.placeholder",
                    defaultMessage: "Select Priority",
                  })}
                  icon={true}
                  isMulti={false}
                />
              </CanAccessFeature>
            ) : (
              <div style={{ flex: 1 }}></div>
            )}
          </div>
          {moreDetails ? (
            <>
              <div className={classes.multipleDropdownCnt}>

                <CanAccessFeature group='task' feature='statusTitle'>
                  <CreateableSelectDropdown
                    data={statusDropDownData}
                    // data={() => {
                    //   return statusData(theme, classes, intl);
                    // }}
                    label={intl.formatMessage({
                      id: "task.creation-dialog.form.status.label",
                      defaultMessage: "Status",
                    })}
                    styles={{ marginRight: 20, flex: 1 }}
                    selectOptionAction={this.handleOptionsSelect}
                    type="status"
                    selectedValue={status}
                    placeholder={intl.formatMessage({
                      id: "task.creation-dialog.form.status.placeholder",
                      defaultMessage: "Select Status",
                    })}
                    icon={true}
                    isMulti={false}
                    acceptDataInFormOfFun={false}
                  />
                </CanAccessFeature>
                {CanAccess({ group: 'task', feature: 'actualStartDate' }) && CanAccess({ group: 'task', feature: 'actualDueDate' }) ?
                  <>
                    {dateType == "planned" ? (
                      <CustomRangeDatePicker
                        label={intl.formatMessage({
                          id: "task.detail-dialog.plan-start-end.label",
                          defaultMessage: "Planned Start/End",
                        })}
                        styles={{ marginRight: 20, flex: 1 }}
                        placeholder={intl.formatMessage({
                          id: "task.creation-dialog.form.actual-start-end.placeholder",
                          defaultMessage: "Select Date",
                        })}
                        pickerType="input"
                        error={false}
                        startDate={startDate}
                        endDate={endDate}
                        startTime={startTime}
                        endTime={endTime}
                        dateSaveAction={(startDate, endDate, startTime, endTime) =>
                          this.handleDateSelect(startDate, endDate, "planned", startTime, endTime)
                        }
                      />
                    ) : (
                      <CustomRangeDatePicker
                        label={intl.formatMessage({
                          id: "task.creation-dialog.form.actual-start-end.label",
                          defaultMessage: "Actual Start/End",
                        })}
                        styles={{ flex: 1 }}
                        placeholder={intl.formatMessage({
                          id: "task.creation-dialog.form.actual-start-end.placeholder",
                          defaultMessage: "Select Date",
                        })}
                        pickerType="input"
                        error={false}
                        startDate={startDate}
                        endDate={endDate}
                        startTime={startTime}
                        endTime={endTime}
                        dateSaveAction={(startDate, endDate, startTime, endTime) =>
                          this.handleDateSelect(startDate, endDate, "actual", startTime, endTime)
                        }
                      />
                    )}
                  </> : null}
              </div>
              <CanAccessFeature group='task' feature='description'>
                <TextEditor changeAction={this.handleDescriptionChange} />
              </CanAccessFeature>
            </>
          ) : null}
          {/* <Uploader /> */}

          <p className={classes.addMoreDetailsBtn} onClick={this.handleMoreDetailsClick}>
            {moreDetails
              ? intl.formatMessage({
                id: "task.creation-dialog.form.show.show-less.label",
                defaultMessage: "Show Less",
              })
              : intl.formatMessage({
                id: "task.creation-dialog.form.show.show-more.label",
                defaultMessage: "Show More",
              })}
          </p>
        </div>
        <ButtonActionsCnt
          cancelAction={closeAction}
          successAction={this.handleSubmit}
          successBtnText={`Create ${taskLabelSingle ? taskLabelSingle : "Task"}`}
          cancelBtnText={intl.formatMessage({
            id: "task.creation-dialog.form.cancel-button.label",
            defaultMessage: "Cancel",
          })}
          btnType="success"
          btnQuery={btnQuery}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    members: state.profile.data.member.allMembers,
    projectsState: state.projects,
    workspacePermissionsState: state.workspacePermissions,
    projects: state.projects.data,
    ganttComponent: state.ganttComponent,
    workspaceStatus: state.workspaceTemplates.data.defaultWSTemplate,
    companyInfo: state.whiteLabelInfo.data,
  };
};
// export default injectIntl injectIntl(AddNewTaskForm,
//   {withRef: true});
export default compose(
  withRouter,
  injectIntl,
  withSnackbar,
  withStyles(addNewFormStyles, {
    withTheme: true,
  }),
  connect(mapStateToProps, {
    isUserPresent,
    SaveProject,
    FetchProjectsInfo,
    SaveTask,
  })
)(AddNewTaskForm);
