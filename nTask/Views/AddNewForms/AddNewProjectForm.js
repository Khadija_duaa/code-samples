import React, { Component, Fragment } from "react";
import Hotkeys from "react-hot-keys";
import { withSnackbar } from "notistack";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import autoCompleteStyles from "../../assets/jss/components/autoComplete";
import combineStyles from "../../utils/mergeStyles";
import dialogFormStyles from "../../assets/jss/components/dialogForm";
import DefaultTextField from "../../components/Form/TextField";
import { SaveProject, FetchProjectsInfo } from "../../redux/actions/projects";
import { createNewProjectHelpText } from "../../components/Tooltip/helptext";
// import TextEditor from "./TextEditor";
import { validProjectTitle } from "../../utils/validator/project/projectTitle";
import ButtonActionsCnt from "../../components/Dialog/ConfirmationDialogs/ButtonActionsCnt";
import {injectIntl} from 'react-intl';
import { grid } from "../../components/CustomTable2/gridInstance";
import mixpanel from 'mixpanel-browser';
import { MixPanelEvents } from '../../mixpanel';

class AddNewProject extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "ganttView",
      projectTitle: "",
      projectTitleError: false,
      projectTitleErrorMessage: "",
      btnQuery: ""
    };
  }
  componentDidMount() {
    mixpanel.time_event(MixPanelEvents.ProjectCreationEvent);
  }
  handleTextFieldChange = event => {
    this.setState({
      projectTitle: event.target.value,
      projectTitleError: false,
      projectTitleErrorMessage: ""
    });
  };

  handleSubmit = () => {
    let { projectTitle } = this.state;
    //Validating project Title
    let validateProjectTitle = validProjectTitle(projectTitle,this.props.intl);
    if (validateProjectTitle) {
      this.setState({
        projectTitleError: true,
        projectTitleErrorMessage: validateProjectTitle
      });
      return;
    }
    //Setting state to show loader on submit button
    this.setState({ btnQuery: "progress" });
    //Object made to be posted to the backend understandable form
    let obj = {
      projectName: projectTitle.replace(/\s\s+/g, ' ')
    };
    //Action Called to save project object to backend
    this.props.SaveProject(
      obj,
      res => {
        mixpanel.track(MixPanelEvents.ProjectCreationEvent, res);
        if (res.status === 409) {
          this.setState({ btnQuery: "" });
          this.setState({
            projectTitleErrorMessage: "The project name is already exists",
            projectTitleError: true
          });
        } else {
          this.setState({ btnQuery: "" });
          this.props.closeAction();
          setTimeout(() => {
            grid.grid && grid.grid.redrawRows();
          }, 0);
        }
      },
      () => { }
    );
  };
  onKeyDown = (keyName, e, handle) => {
    if (keyName === "enter") {
      if (this.state.btnQuery === "") this.handleSubmit();
    }
  };
  //Update content of text editor in local state to send it to backend
  handleDescriptionChange = value => {
    this.setState({ description: value });
  };
  render() {
    const { classes, theme, closeAction,intl } = this.props;
    const {
      projectTitleError,
      projectTitleErrorMessage,
      projectTitle,
      btnQuery
    } = this.state;

    return (
      <Fragment>
        <div className={classes.dialogFormCnt} noValidate autoComplete="off">
          <Hotkeys keyName="enter" onKeyDown={this.onKeyDown}>
            <DefaultTextField
              label={intl.formatMessage({ id:"project.creation-dialog.project-title.label", defaultMessage: "Project Title"})}
              helptext={intl.formatMessage({ id:"project.creation-dialog.project-title.hint", defaultMessage: createNewProjectHelpText.projectTitleHelpText})}
              errorState={projectTitleError}
              errorMessage={projectTitleErrorMessage}
              defaultProps={{
                type: "text",
                id: "projectTitle",
                placeholder: intl.formatMessage({id:"project.creation-dialog.project-title.palceholder" , defaultMessage:"Add Project Title"}),
                value: projectTitle,
                autoFocus: true,
                inputProps: { maxLength: 80 },
                onChange: event => {
                  this.handleTextFieldChange(event, "projectTitle");
                }
              }}
            />
          </Hotkeys>
          {/* <TextEditor changeAction={this.handleDescriptionChange} /> */}
        </div>

        <ButtonActionsCnt
          cancelAction={closeAction}
          successAction={this.handleSubmit}
          successBtnText={intl.formatMessage({ id:"project.creation-dialog.create-button.label", defaultMessage: "Create Project"})}
          cancelBtnText={intl.formatMessage({ id:"common.action.cancel.label", defaultMessage: "Cancel"})}
          btnType="success"
          btnQuery={btnQuery}
        />
      </Fragment>
    );
  }
}

export default compose(
  withRouter,
  withSnackbar,
  injectIntl,
  withStyles(combineStyles(autoCompleteStyles, dialogFormStyles), {
    withTheme: true
  }),
  connect(null, {
    SaveProject,
    FetchProjectsInfo
  })
)(AddNewProject);
