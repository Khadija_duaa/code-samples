import React, { Component, Fragment } from "react";
import Hotkeys from "react-hot-keys";
import { withSnackbar } from "notistack";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./style";
import autoCompleteStyles from "../../assets/jss/components/autoComplete";
import combineStyles from "../../utils/mergeStyles";
import dialogFormStyles from "../../assets/jss/components/dialogForm";
import DefaultTextField from "../../components/Form/TextField";
import {
  SaveProject,
  FetchProjectsInfo,
  updateProject,SaveProjectAsTemplate
} from "../../redux/actions/projects";
import {CreateBoard, UpdateKanbanBoard, SaveAsTemplate, UseTemplate} from "../../redux/actions/boards";
import { createNewProjectHelpText } from "../../components/Tooltip/helptext";
// import TextEditor from "./TextEditor";
import { validProjectTitle } from "../../utils/validator/project/projectTitle";
import ButtonActionsCnt from "../../components/Dialog/ConfirmationDialogs/ButtonActionsCnt";
import InputLabel from "@material-ui/core/InputLabel";
import classNames from "classnames";
import CheckIcon from "@material-ui/icons/Check";
import DefaultCheckbox from "../../components/Form/Checkbox";
import Typography from "@material-ui/core/Typography";
import isEmpty from "lodash/isEmpty";
import ColorImagesModal from "../../components/ColorImagesModal";
import { FormattedMessage, injectIntl } from "react-intl";
import {Prompt} from "react-router-dom";
import { CanAccessFeature } from "../../components/AccessFeature/AccessFeature.cmp";
import mixpanel from 'mixpanel-browser';
import { MixPanelEvents } from '../../mixpanel';

class AddNewBoard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "ganttView",
      projectTitle: "",
      projectId: "",
      boardId: "",
      projectTitleError: false,
      projectTitleErrorMessage: "",
      btnQuery: "",
      description: "",
      selectedColor: "",
      selectedImage: "",
      anchorEl: null,
      keepCards: true,
      colors: [
        { color: "#359bcf", isClicked: false }, //set default selected at least one color
        { color: "#e57451", isClicked: false },
        { color: "#f89B19", isClicked: false },
        { color: "#13be7f", isClicked: false },
        { color: "#5174e5", isClicked: false },
        { color: "#E551c7", isClicked: false },
        { color: "#9651e5", isClicked: false },
        { color: "#e55151", isClicked: false },
        { color: "#3cccbe", isClicked: false },
        { color: "#eebf03", isClicked: false },
        { color: "#195b8a", isClicked: false },
        { color: "#105900", isClicked: false },
        { color: "#b00b1e", isClicked: false },
        { color: "#bc8a9d", isClicked: false },
        { color: "#4c4c4c", isClicked: false },
        { color: "#a15773", isClicked: false },
      ],
      isDirty: false,
      message:
        "Looks like there are some unsaved changes on this page.if you leave this page, change you amde will not be saved. What would you like to do?",
      useProjectStatusColor: true,
    };
  }

  componentDidMount() {
    const { data } = this.props;
    mixpanel.time_event(MixPanelEvents.BoardCreationEvent);
    if (!isEmpty(data)) {
      let title = data.isTemplate
        ? data.template.name
          ? data.template.name
          : data.template.boardName
        : data.boardName;
      let desc = data.isTemplate
        ? data.template.description
          ? data.template.description
          : data.template.boardDescription
        : data.boardDescription;
      let color = data.isTemplate
        ? data.template.colorCode
          ? data.template.colorCode
          : data.template.boardColorCode
        : data.boardColorCode;
      let selectedImg = data.boardImagePath ? data.boardImagePath : "";
      this.setState({
        projectId: data.projectId,
        boardId: data.boardId,
        projectTitle: title,
        description: desc,
        selectedImage: selectedImg,
        selectedColor: color ? color : "",
        colors: this.state.colors.map(p =>
          p.color === color && selectedImg == ""
            ? { ...p, isClicked: true }
            : { ...p, isClicked: false }
        ),
        useProjectStatusColor : data.useProjectStatusColor
      });
    }
  }
  handleKeepCards = () => {
    this.setState({
      keepCards: !this.state.keepCards,
    });
  };
  handleSelectStatusColor = () => {
    this.setState({
      useProjectStatusColor: !this.state.useProjectStatusColor,
      selectedColor: this.state.useProjectStatusColor ? this.state.colors[0].color : "",
      selectedImage: "",
      colors: this.state.useProjectStatusColor
        ? this.state.colors.map((p, index) =>
            index == 0 ? { ...p, isClicked: true } : { ...p, isClicked: false }
          )
        : this.state.colors.map(c => {
            c.isClicked = false;
            return c;
          }),
    });
  };

  handleClick = color => {
    this.setState({
      // clicked: !this.state.clicked,
      selectedColor: color,
      colors: this.state.colors.map(p =>
        p.color === color ? { ...p, isClicked: true } : { ...p, isClicked: false }
      ),
      useProjectStatusColor: false
    });
  };

  handleTextFieldChange = event => {
    this.setState({
      projectTitle: event.target.value,
      projectTitleError: false,
      projectTitleErrorMessage: "",
      isDirty: this.props.isSavedAsTemplate == true,
    });
  };

  handleSubmit = () => {
    let { projectTitle, projectId, keepCards } = this.state;
    const { data } = this.props;
    //Validating project Title
    let validateProjectTitle = validProjectTitle(projectTitle);
    if (validateProjectTitle) {
      this.setState({
        projectTitleError: true,
        projectTitleErrorMessage: validateProjectTitle,
      });
      return;
    }
    //Setting state to show loader on submit button
    this.setState({ btnQuery: "progress" });
    //Object made to be posted to the backend understandable form
    let obj = {
      boardType: "Kanban", //creationg board so project type would be kanban
      boardName: projectTitle.trim().replace(/\s\s+/g, " "),
      boardDescription: this.state.description,
      boardColorCode: this.state.selectedColor,
      boardImagePath: this.state.selectedImage,
      projectId: projectId,
      isKeepCards: this.state.keepCards,
      useProjectStatusColor: this.state.useProjectStatusColor,
    };
    //Action Called to save project object to backend
    if (this.props.isSavedAsTemplate) {
      this.props.SaveAsTemplate(
        obj,
        r => {
          mixpanel.track(MixPanelEvents.BoardCreationEvent, r);
          this.setState({ btnQuery: "" });
          this.props.closeAction();
          // this.props.getTemplates();
        },
        err => {
          this.setState({
            btnQuery: "",
            projectTitleError: true,
            projectTitleErrorMessage: err.message,
          });
        }
      );
    } else {
      if (data.isTemplate) {
        obj.templateId = data.template.templateId;
        obj.isKeepCards = keepCards;
        this.props.UseTemplate(
          obj,
          succ => {
            this.setState({ btnQuery: "" });
            this.props.closeAction();
          },
          err => {
            this.setState({
              btnQuery: "",
              projectTitleError: true,
              projectTitleErrorMessage: err.message,
            });
          }
        );
      } else {
        this.props.CreateBoard(
          obj,
          res => {
            this.setState({ btnQuery: "" });
            this.props.closeAction();
            // if (res.status === 409) {
            //   this.setState({ btnQuery: "" });
            //   this.setState({
            //     projectTitleErrorMessage: "The project name is already exists",
            //     projectTitleError: true,
            //   });
            // } else {
            //   if (this.props.isTemplateView) {
            //     this.props.createdProject(
            //       {
            //         project: res.data,
            //         keepCards: this.state.keepCards,
            //       },
            //       (callback) => {
            //         this.setState({ btnQuery: "" });
            //         this.props.closeAction();
            //       }
            //     );
            //   } else {
            //     this.props.closeAction();
            //     this.setState({ btnQuery: "" });
            //   }
            // }
          },
          err => {
            this.setState({
              btnQuery: "",
              projectTitleError: true,
              projectTitleErrorMessage: err.message,
            });
          }
        );
      }
    }
  };
  onKeyDown = (keyName, e, handle) => {
    // if (keyName === "enter") {
    //   if (this.state.btnQuery === "") this.handleSubmit();
    // }
  };
  //Update content of text editor in local state to send it to backend
  handleDescriptionChange = e => {
    this.setState({ description: e.target.value, isDirty: this.props.isSavedAsTemplate == true });
  };

  handleUpdateBoard = () => {
    const { description, selectedColor, projectTitle, selectedImage } = this.state;
    const { data, UpdateKanbanBoard } = this.props;
    this.setState({ btnQuery: "progress" });
    let obj = {
      boardColorCode: selectedColor,
      boardDescription: description,
      projectId: data.projectId,
      boardId: data.boardId,
      boardName: projectTitle.trim(),
      boardImagePath: selectedImage,
      useProjectStatusColor: this.state.useProjectStatusColor
    };
    UpdateKanbanBoard(
      obj,
      succ => {
        this.setState({ btnQuery: "" }, () => {
          this.props.closeAction();
        });
      },
      err => {
        this.setState({
          btnQuery: "",
          projectTitleError: true,
          projectTitleErrorMessage: err.message,
        });
      }
    );
  };
  openModal = event => {
    this.setState({ anchorEl: event.target });
  };
  handleImageSelect = selectedImage => {
    let updatedColors = this.state.colors.map(c => {
      c.isClicked = false;
      return c;
    });
    this.setState({
      anchorEl: null,
      selectedImage: selectedImage.urls.small,
      selectedColor: "",
      colors: updatedColors,
      useProjectStatusColor: false
    });
  };
  handleColorSelect = selectedColor => {
    this.handleClick(selectedColor);
    // this.setState({ anchorEl: null, selectedImage: null });
  };
  handleClickAway = e => {
    e.preventDefault();
    e.stopPropagation();
    this.setState({ anchorEl: null, selectedImage: null, selectedColor: "", useProjectStatusColor: false });
  };
  handleLeavePage(e) {
    const confirmationMessage =
      "Looks like there are some unsaved changes on this page.if you leave this page, change you amde will not be saved. What would you like to do?";
    e.returnValue = confirmationMessage; // Gecko, Trident, Chrome 34+
    return confirmationMessage; // Gecko, WebKit, Chrome <34
  }
  render() {
    const {
      classes,
      theme,
      closeAction,
      isTemplateView,
      data,
      updateBoard,
      isSavedAsTemplate,
      intl,
      boardPer,
    } = this.props;
    const {
      projectTitleError,
      projectTitleErrorMessage,
      projectTitle,
      btnQuery,
      description,
      colors,
      keepCards,
      anchorEl,
      isDirty,
      message,
      useProjectStatusColor,
    } = this.state;

    let backgroundChange = boardPer ? boardPer.boardDescription.cando : true;
    if (isDirty) window.addEventListener("beforeunload", this.handleLeavePage);
    else window.removeEventListener("beforeunload", this.handleLeavePage);
    return (
      <Fragment>
        <div className={classes.dialogFormCnt} noValidate autoComplete="off">
          <Hotkeys keyName="enter" onKeyDown={this.onKeyDown}>
            <DefaultTextField
              label={
                <FormattedMessage
                  id="board.all-boards.board-creation-dialogue.form.title.label"
                  defaultMessage="Board Title"
                />
              }
              // helptext={createNewProjectHelpText.projectTitleHelpText}
              errorState={projectTitleError}
              errorMessage={projectTitleErrorMessage}
              defaultProps={{
                type: "text",
                id: "boardTitle",
                placeholder: intl.formatMessage({
                  id: "board.all-boards.board-creation-dialogue.form.title.placeholder",
                  defaultMessage: "Enter Board Title",
                }),
                value: projectTitle,
                autoFocus: true,
                inputProps: { maxLength: 80 },
                onChange: event => {
                  this.handleTextFieldChange(event, "projectTitle");
                },
                disabled: boardPer ? !boardPer.boardName.cando : false,
              }}
            />
            <DefaultTextField
              label={
                <FormattedMessage
                  id="board.all-boards.board-creation-dialogue.form.description.label"
                  defaultMessage="Description (Optional)"
                />
              }
              fullWidth={true}
              errorState={null}
              defaultProps={{
                id: "description",
                placeholder: intl.formatMessage({
                  id: "board.all-boards.board-creation-dialogue.form.description.placeholder",
                  defaultMessage:
                    "Let people know what this board is used for and what they can expect to see.",
                }),
                onChange: this.handleDescriptionChange,
                value: description,
                multiline: true,
                rows: 7,
                autoFocus: false,
                inputProps: { maxLength: 1500 },
                disabled: boardPer ? !boardPer.boardDescription.cando : false,
              }}
            />
            {backgroundChange && (
              <>
                <span className={classes.title}>
                  <FormattedMessage
                    id="board.all-boards.board-creation-dialogue.form.background.label"
                    defaultMessage="Background"
                  />
                </span>
                <div className={classes.backgroundContainer}>
                  {colors.slice(0, 9).map((color, index) => {
                    return (
                      <div
                        key={`color-${index}`}
                        className={`${classes.colorCnt}${
                          color.isClicked ? ` ` + classes.selectedColor : ``
                        }`}
                        onClick={this.handleClick.bind(this, color.color)}
                        style={{ backgroundColor: color.color }}>
                        {color.isClicked && <CheckIcon />}
                      </div>
                    );
                  })}
                  <div
                    onClick={this.openModal.bind(this)}
                    className={`${classes.colorCnt}${" "}${classes.selectedColor}`}
                    style={{ background: "#EAEAEA", color: "#7E7E7E" }}>
                    ...
                  </div>
                  <ColorImagesModal
                    anchorTarget={anchorEl}
                    handleImageSelect={this.handleImageSelect}
                    handleColorSelect={this.handleColorSelect}
                    intl={intl}
                    handleClickAway={this.handleClickAway}
                    useProjectStatusColor={useProjectStatusColor}
                    handleClickStatusSelect={()=>{}}
                    viewMode="new"
                  />
                </div>
              </>
            )}
            {/* <div className={classes.backgroundContainer}>
              {colors.slice(9, colors.length).map((color, index) => {
                return (
                  <div
                    key={`color-${index}`}
                    className={`${classes.colorCnt}${
                      color.isClicked ? ` ` + classes.selectedColor : ``
                    }`}
                    onClick={this.handleClick.bind(this, color.color)}
                    style={{ backgroundColor: color.color }}
                  >
                    {color.isClicked && <CheckIcon />}
                  </div>
                );
              })}
            </div> */}
             <CanAccessFeature group='project' feature='status'>
            {!isTemplateView &&<div className={classes.keepCardsCnt}>
              <DefaultCheckbox
                checkboxStyles={{ padding: "5px 5px 0 0 " }}
                checked={useProjectStatusColor}
                onChange={this.handleSelectStatusColor}
                fontSize={20}
                color={"#0090ff"}
              />
              <Typography variant="h6" className={classes.heading}>
                Use Project Status Color
              </Typography>
            </div>}
             </CanAccessFeature>
            {isTemplateView && (
              <div className={classes.keepCardsCnt}>
                <DefaultCheckbox
                  checkboxStyles={{ padding: "5px 5px 0 0 " }}
                  checked={keepCards}
                  onChange={this.handleKeepCards}
                  fontSize={20}
                  color={"#0090ff"}
                />
                <Typography variant="h6" className={classes.heading}>
                  <FormattedMessage
                    id="board.all-boards.board-creation-dialogue.form.keep-template-cards.label"
                    defaultMessage="Keep Template Cards"
                  />{" "}
                </Typography>
              </div>
            )}
          </Hotkeys>
          {/* <TextEditor changeAction={this.handleDescriptionChange} /> */}
        </div>

        <ButtonActionsCnt
          cancelAction={closeAction}
          successAction={updateBoard ? this.handleUpdateBoard : this.handleSubmit}
          successBtnText={
            isSavedAsTemplate ? (
              <FormattedMessage
                id="board.all-boards.board-creation-dialogue.saveas-template-title"
                defaultMessage="Save as Template"
              />
            ) : updateBoard ? (
              <FormattedMessage
                id="profile-settings-dialog.signin-security.change-password.form.save-button.label"
                defaultMessage="Save Changes"
              />
            ) : (
              <FormattedMessage
                id="board.all-boards.board-creation-dialogue.title"
                defaultMessage="Create Board"
              />
            )
          }
          cancelBtnText={
            <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
          }
          btnType="success"
          btnQuery={btnQuery}
          disabled={projectTitle == ""}
        />
        <Prompt when={isDirty} message={message} />
      </Fragment>
    );
  }
}

AddNewBoard.defaultProps = {
  isTemplateView: false,
  updateBoard: false,
  data: {},
  createdProject: () => {},
  updateProject: () => {},
  boardPer: false
};

export default compose(
  withRouter,
  withSnackbar,
  injectIntl,
  withStyles(combineStyles(autoCompleteStyles, dialogFormStyles, styles), {
    withTheme: true,
  }),
  connect(null, {
    SaveProject,
    FetchProjectsInfo,
    updateProject,
    SaveProjectAsTemplate,
    CreateBoard,
    UseTemplate,
    UpdateKanbanBoard,
    SaveAsTemplate
  })
)(AddNewBoard);
