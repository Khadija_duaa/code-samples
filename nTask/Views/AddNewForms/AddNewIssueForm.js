import React, { Component, Fragment } from "react";
import { withSnackbar } from "notistack";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import DefaultTextField from "../../components/Form/TextField";
import { generateAssigneeData } from "../../helper/generateSelectData";
import { createIssue } from "../../redux/actions/issues";
import { updateTaskIssuesData, updateTaskObject } from "../../redux/actions/tasks";
import { createNewIssueHelpText } from "../../components/Tooltip/helptext";
import SelectSearchDropdown from "../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import addNewFormStyles from "./style";
import { canDo } from "../../components/workspacePermissions/permissions";
import CreateableSelectDropdown from "../../components/Dropdown/SelectCreateableDropdown/SelectCreateableDropdown";
import ButtonActionsCnt from "../../components/Dialog/ConfirmationDialogs/ButtonActionsCnt";
import { priorityData, statusData, typeData } from "../../helper/issueDropdownData";
import CustomRangeDatePicker from "../../components/DatePicker/CustomRangeDatePicker";
import TextEditor from "./TextEditor";
import isEmail from "validator/lib/isEmail";
import { validIssueTitle } from "../../utils/validator/issue/issueTitle";
import { injectIntl } from "react-intl";
import { v4 as uuidv4 } from "uuid";
import { emptyIssue } from "../../utils/constants/emptyIssue";
import { CanAccess, CanAccessFeature } from "../../components/AccessFeature/AccessFeature.cmp";
import mixpanel from 'mixpanel-browser';
import { MixPanelEvents } from '../../mixpanel';

class AddIssueForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      issueTitle: "",
      issueTitleError: false,
      issueTitleMessage: null,
      assignee: [],
      btnQuery: "",
      startDate: "",
      endDate: "",
      startTime: "",
      endTime: "",
      moreDetails: false,
      calenderDateType: false,
      dateType: "actual",
      priority: priorityData(props.theme, props.classes, props.intl).find(p => {
        return p.value == "Medium";
      }),
      type: typeData(props.theme, props.classes, props.intl).find(t => {
        return t.value == "Bug";
      }),
      status: statusData(props.theme, props.classes, props.intl).find(s => {
        return s.value == "Open";
      }),
    };
  }
  handleTitle = event => {
    this.setState({
      issueTitle: event.target.value,
      issueTitleError: false,
      issueTitleErrorMessage: "",
    });
  };
  componentDidMount() {
    mixpanel.time_event(MixPanelEvents.IssueCreationEvent);
    const { preFilledData } = this.props;
    if (preFilledData) {
      const { startDate, endDate, moreDetails, calenderDateType } = preFilledData;
      this.setState({
        startDate,
        endDate,
        moreDetails,
        dateType: calenderDateType == "Planned Start/End" ? "planned" : "actual",
      });
    }
  }
  //Update content of text editor in local state to send it to backend
  handleDescriptionChange = value => {
    this.setState({ description: value });
  };
  handleSubmit = () => {
    let {
      issueTitle,
      moreDetails,
      assignee,
      priority,
      status,
      type,
      endDate,
      startDate,
      description,
      dateType,
    } = this.state;
    let { currentTask, disableCreateIssue } = this.props;
    //Validating issue Title
    let validateIssueTitle = validIssueTitle(issueTitle, this.props.intl);
    if (validateIssueTitle) {
      this.setState({
        issueTitleError: true,
        issueTitleErrorMessage: validateIssueTitle,
      });
      return;
    }
    //Setting state to show loader on submit buttongff
    this.setState({ btnQuery: "progress" });
    //Generate new assignee array from selected data with Member ID's to save on the backend understanable form
    let assigneeObj = assignee.map(a => {
      return a.id;
    });
    //Object made to be posted to the backend understandable form
    let obj = {
      clientId: uuidv4(),
      title: issueTitle,
      issueAssingnees: assigneeObj,
      type: type.value,
      linkedTasks: currentTask ? [currentTask.taskId] : "",
      priority: moreDetails ? priority.value : "Medium",
      status: moreDetails ? status.value : null,
      actualStartDate: moreDetails && dateType == "actual" ? startDate : null,
      actualDueDate: moreDetails && dateType == "actual" ? endDate : null,
      startDate: moreDetails && dateType == "planned" ? startDate : null,
      dueDate: moreDetails && dateType == "planned" ? endDate : null,
      detail: moreDetails
        ? description
          ? window.btoa(unescape(encodeURIComponent(description)))
          : null
        : null,
      projectId: currentTask && currentTask.projectId ? currentTask.projectId : null,
    };
    if (currentTask) {
      this.props.updateTaskObject({ ...currentTask, issues: currentTask.issues + 1 });
    }
    //Action Called to save issue object to backend
    this.props.createIssue(
      obj,
      null,
      { ...emptyIssue, ...obj },
      () => {
        mixpanel.track(MixPanelEvents.IssueCreationEvent, obj);
        if (currentTask) {
          // The check applies based upon where this whole component is rendered,
          //in case of task details there is no dialog which needs to be closed on saving issue so it will just hide the form and show the entry
          disableCreateIssue();
        } else {
          //Called in case of dialog box so it can be closed on new issue save
          this.props.closeAction();
        }
        this.setState({ btnQuery: "" });
      },
      () => { }
    );
  };
  //Validate Email function for inviting new members to workspace via createable select
  validateEmail = value => {
    return isEmail(value);
  };
  //Local state is update for dropdown so object can be made to send update to backend with API
  //Updating local state when option is selected in assignee dropdown
  handleOptionsSelect = (type, value) => {
    this.setState({ [type]: value });
  };
  //Local state is update for dropdown so object can be made to send update to backend with API
  //Updating local state when option is selected in assignee dropdown
  handleCreateOptionsSelect = (type, option) => {
    this.setState({ assignee: option });
  };
  //Updating local state when option is created in assignee dropdown
  handleCreateOption = (type, option) => {
    const { newAssignee } = this.state;
    this.setState({ newAssignee: [...newAssignee, option] });
  };
  //Updating local state when select is cleared in project dropdown
  handleClearOption = () => {
    this.setState({ project: [] });
  };
  //Updating local state when option is removed from issue dropdown
  handleRemoveOption = (type, option = {}) => {
    const { assignee, newAssignee } = this.state;
    if (option.__isNew__) {
      this.setState({
        newAssignee: newAssignee.filter(a => {
          return a.label !== option.label;
        }),
      });
    } else {
      this.setState({
        assignee: assignee.filter(a => {
          return a.id !== option.id;
        }),
      });
    }
  };
  //Function handles date select dropdown
  handleDateSelect = (startDate, endDate, dateType, startTime, endTime) => {
    this.setState({ startDate, endDate, startTime, endTime, dateType });
  };
  //This function toggle show more options
  handleMoreDetailsClick = () => {
    this.setState(prevState => ({ moreDetails: !prevState.moreDetails }));
  };
  handleClickAwayIssueDescription = () => { };
  render() {
    const {
      classes,
      theme,
      style,
      workspacePermissionsState,
      members,
      closeAction,
      disableCreateIssue,
    } = this.props;
    const intl = this.props.intl;
    const {
      issueTitleError,
      issueTitleErrorMessage,
      issueTitle,
      btnQuery,
      assignee,
      priority,
      type,
      startDate,
      endDate,
      startTime,
      endTime,
      status,
      moreDetails,
      description,
      dateType
    } = this.state;
    return (
      <Fragment>
        <div style={style} className={classes.formCnt} noValidate autoComplete="off">
          <DefaultTextField
            label={intl.formatMessage({
              id: "issue.creation-dialog.form.title.label",
              defaultMessage: "Issue Title",
            })}
            helptext={intl.formatMessage({
              id: "issue.creation-dialog.form.title.hint",
              defaultMessage: createNewIssueHelpText.issueTitleHelpText,
            })}
            errorState={issueTitleError}
            errorMessage={issueTitleErrorMessage}
            defaultProps={{
              type: "text",
              id: "issueTitle",
              placeholder: intl.formatMessage({
                id: "issue.creation-dialog.form.title.placeholder",
                defaultMessage: "Add Issue Title",
              }),
              value: issueTitle,
              autoFocus: true,
              inputProps: { maxLength: 250 },
              onChange: event => {
                this.handleTitle(event, "issueTitle");
              },
            }}
          />

          <CanAccessFeature group='issue' feature='assignee'>
            {canDo(workspacePermissionsState.data.workSpace, "inviteMembers") ? (
              <CreateableSelectDropdown
                data={() => {
                  return generateAssigneeData(members);
                }}
                label="Assigned To (Optional)"
                placeholder="Select / Add Assignee"
                name="assignee"
                selectOptionAction={this.handleCreateOptionsSelect}
                createOptionAction={this.handleCreateOption}
                removeOptionAction={this.handleRemoveOption}
                type="assignee"
                createText="Invite Member"
                selectedValue={assignee}
                avatar={true}
                createLabelValidation={this.validateEmail}
              />
            ) : (
              <SelectSearchDropdown
                data={() => {
                  return generateAssigneeData(members);
                }}
                label={intl.formatMessage({
                  id: "issue.creation-dialog.form.assign-to.label",
                  defaultMessage: "Assigned To (Optional)",
                })}
                selectChange={this.handleOptionsSelect}
                type="assignee"
                selectedValue={assignee}
                placeholder={intl.formatMessage({
                  id: "issue.creation-dialog.form.assign-to.placeholder",
                  defaultMessage: "Select Assignee",
                })}
                avatar={true}
              />
            )}
          </CanAccessFeature>
          <div className={classes.multipleDropdownCnt}>
            <CanAccessFeature group='issue' feature='type'>
              <SelectSearchDropdown
                data={() => {
                  return typeData(theme, classes, intl);
                }}
                label={intl.formatMessage({
                  id: "issue.common.issue-type.label",
                  defaultMessage: "Issue Type",
                })}
                styles={{ marginRight: 20, flex: 1 }}
                selectChange={this.handleOptionsSelect}
                type="type"
                selectedValue={type}
                placeholder={intl.formatMessage({
                  id: "issue.common.issue-type.placeholder",
                  defaultMessage: "Select Type",
                })}
                icon={true}
                isMulti={false}
              />
            </CanAccessFeature>
            {moreDetails ? (

              <CanAccessFeature group='issue' feature='priority'>
                <SelectSearchDropdown
                  data={() => {
                    return priorityData(theme, classes, intl);
                  }}
                  label={intl.formatMessage({
                    id: "issue.common.priority.label",
                    defaultMessage: "Priority",
                  })}
                  styles={{ flex: 1 }}
                  selectChange={this.handleOptionsSelect}
                  type="priority"
                  selectedValue={priority}
                  placeholder={intl.formatMessage({
                    id: "issue.common.priority.label",
                    defaultMessage: "Select Priority",
                  })}
                  icon={true}
                  isMulti={false}
                /></CanAccessFeature>
            ) : (
              <div style={{ flex: 1 }}></div>
            )}
          </div>
          {moreDetails ? (
            <>
              <div className={classes.multipleDropdownCnt}>
                {CanAccess({ group: 'issue', feature: 'actualStartDate' }) && CanAccess({ group: 'issue', feature: 'actualDueDate' }) ?
                  <>
                    {dateType == "planned" ? (
                      <CustomRangeDatePicker
                        label={intl.formatMessage({
                          id: "issue.detail-dialog.plan-start-end.label",
                          defaultMessage: "Planned Start/End",
                        })}
                        styles={{ marginRight: 20, flex: 1 }}
                        placeholder={intl.formatMessage({
                          id: "issue.creation-dialog.form.actual-start-end.placeholder",
                          defaultMessage: "Select Date",
                        })}
                        pickerType="input"
                        error={false}
                        startDate={startDate}
                        endDate={endDate}
                        startTime={startTime}
                        endTime={endTime}
                        dateSaveAction={(startDate, endDate, startTime, endTime) =>
                          this.handleDateSelect(startDate, endDate, "planned", startTime, endTime)
                        }
                      />
                    ) : (
                      <CustomRangeDatePicker
                        label={intl.formatMessage({
                          id: "issue.creation-dialog.form.actual-start-end.label",
                          defaultMessage: "Actual Start/End",
                        })}
                        styles={{ marginRight: 20, flex: 1 }}
                        placeholder={intl.formatMessage({
                          id: "issue.creation-dialog.form.actual-start-end.placeholder",
                          defaultMessage: "Select Date",
                        })}
                        pickerType="input"
                        error={false}
                        startDate={startDate}
                        endDate={endDate}
                        startTime={startTime}
                        endTime={endTime}
                        dateSaveAction={(startDate, endDate, startTime, endTime) =>
                          this.handleDateSelect(startDate, endDate, "actual", startTime, endTime)
                        }
                      />
                    )}
                  </> : null}
                <CanAccessFeature group='issue' feature='status'>
                  <SelectSearchDropdown
                    data={() => {
                      return statusData(theme, classes, intl);
                    }}
                    label={intl.formatMessage({
                      id: "issue.common.status.label",
                      defaultMessage: "Issue Status",
                    })}
                    styles={{ flex: 1 }}
                    selectChange={this.handleOptionsSelect}
                    type="status"
                    selectedValue={status}
                    placeholder={intl.formatMessage({
                      id: "issue.common.status.label",
                      defaultMessage: "Select Status",
                    })}
                    icon={true}
                    isMulti={false}
                  />
                </CanAccessFeature>
              </div>

              <CanAccessFeature group='issue' feature='description'>
                <TextEditor changeAction={this.handleDescriptionChange} />
              </CanAccessFeature>
            </>
          ) : null}
          <p className={classes.addMoreDetailsBtn} onClick={this.handleMoreDetailsClick}>
            {moreDetails
              ? intl.formatMessage({
                id: "issue.creation-dialog.form.show.show-less.label",
                defaultMessage: "Show Less",
              })
              : intl.formatMessage({
                id: "issue.creation-dialog.form.show.show-more.label",
                defaultMessage: "Show More",
              })}
          </p>
        </div>

        <ButtonActionsCnt
          cancelAction={disableCreateIssue || closeAction}
          successAction={this.handleSubmit}
          successBtnText={intl.formatMessage({
            id: "issue.creation-dialog.form.create-issue-button.label",
            defaultMessage: "Create Issue",
          })}
          cancelBtnText={intl.formatMessage({
            id: "common.action.cancel.label",
            defaultMessage: "Cancel",
          })}
          btnType="success"
          btnQuery={btnQuery}
        />
      </Fragment>
    );
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    workspacePermissionsState: state.workspacePermissions,
    members: state.profile.data.member.allMembers,
  };
};
export default compose(
  withRouter,
  injectIntl,
  withSnackbar,
  withStyles(addNewFormStyles, {
    withTheme: true,
  }),
  connect(mapStateToProps, {
    createIssue,
    updateTaskIssuesData,
    updateTaskObject,
  })
)(AddIssueForm);
