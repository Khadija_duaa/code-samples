import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withSnackbar } from "notistack";
import LeftArrow from "@material-ui/icons/ChevronLeft";
import { withRouter } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ArrowRight from "@material-ui/icons/KeyboardArrowRight";
import isEqual from "lodash/isEqual";
import { injectIntl, FormattedMessage } from "react-intl";
import isEmpty from "lodash/isEmpty";
import workspaceSetting from "./styles";
import GeneralSetting from "./GeneralSetting";
import ManageMembers from "./ManageMembers";
import { calculateContentHeight, getTemplateType } from "../../utils/common";
import RolesAndPermission from "./Permissions/RolesAndPermission";
import StatusList from "../../components/Templates/StatusList";
import { getWorkspaceRoles, getWorkspaceTemplates } from "../../redux/actions/workspace";
import { teamCanView } from "../../components/PlanPermission/PlanPermission";
import { GetPermission } from "../../components/permissions";
import DefaultTextField from "../../components/Form/TextField";
import TaskStatus from "../../components/Templates/WorkspaceTemplatesList/TaskStatusTemplates";
import CustomIconButton from "../../components/Buttons/CustomIconButton";
import ConstantsPlan from "../../components/constants/planConstant";
import {
  // SaveWSTemplateStatusItem,
  CreateWSTemplate,
  UpdateWSSavedTemplate,
  // DeleteWSTemplateStatusItem,
  MapTaskWithSetDefaultTemplate,
  // DeleteAndMapStatus,
  // restoreStatus,
  AddWSTemplate,
  // CreateNewStatus,
  DeleteSavedTemplate,
} from "../../redux/actions/workspaceTemplates";
import PropTypes from "prop-types";
import templatesConstants from "../../utils/constants/templatesConstants";
import EmailToNtask from "./EmailToNTask/EmailToNtask";
import Configurations from "./Configurations";
import Automation from "./Automation/Automation";
import WorkspaceCalendar from "./WorkspaceCalendar/WorkspaceCalendar.view";
class WorkspaceSetting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0,
      selectedTemplate: null,
    };
  }

  componentDidMount() {
    if (this.props.location.state == "inviteMember") {
      this.setState({ selectedIndex: 1 });
    }
    this.props.getWorkspaceRoles(
      res => { },
      error => { }
    );

    this.props.getWorkspaceTemplates(
      res => {
        const selectedTemplate = this.getDefaultStatusTemp(res.data.entity);
        this.setState({ selectedTemplate });
      },
      error => { }
    );
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.profileState.data.loggedInTeam === "000000") {
      this.props.history.push("/tasks");
    }
    if (prevProps.profileState.data.loggedInTeam !== this.props.profileState.data.loggedInTeam) {
      this.props.getWorkspaceRoles(
        success => { },
        error => { }
      );
      // this.props.getWorkspaceTemplates(
      //   success => {},
      //   error => {}
      // );
    }
    // if (prevState.selectedStatus) {
    //   const status = [];
    //   let checkDeleteStatus =
    //     this.props.workSpaceAllTemplates.find(w => w.id == prevState.selectedStatus.id) || false;

    //   this.props.workSpaceAllTemplates.map(w => {
    //     const list =
    //       w.templates.find(t => t.statusCode == prevState.selectedStatus.statusCode) || {};
    //     if (!isEmpty(list)) status.push(list);
    //   });
    //   if (JSON.stringify(status[0]) !== JSON.stringify(prevState.selectedStatus)) {
    //     if (checkDeleteStatus) {
    //       this.setState({
    //         selectedStatus: status[0],
    //       });
    //     } else {
    //       this.setState({
    //         selectedStatus: this.props.workSpaceAllTemplates[0].templates[0],
    //       });
    //     }
    //   }
    // }
  }

  getDefaultStatusTemp = data => {
    let defaultStatus;
    data.forEach(g => {
      g.templates.forEach(t => {
        if (t.isWorkspaceDefault) {
          defaultStatus = t;
        }
      });
    });
    return defaultStatus;
  };

  tabClick = (event, index) => {
    this.setState({ selectedIndex: index });
  };

  goBack = () => {
    this.props.history.goBack();
  };

  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type || "info",
      }
    );
  };

  selectTemplateItem = statusItem => {
    this.setState({ selectedTemplate: statusItem });
  };

  addTemplate = (item, success, fail) => {
    this.props.AddWSTemplate(
      item,
      response => {
        success();
      },
      error => {
        fail();
      }
    );
  };

  enterKeyHandler = () => { };

  handleItemNameInput = () => { };
  /**
   * it is called when user change workspace default template and its mapping with old template
   * @param {object} data
   * @param {function} success
   * @param {function} fail
   */
  mapTaskWithSetDefaultStatusHandler = (data, success, fail) => {
    this.props.MapTaskWithSetDefaultTemplate(
      data,
      response => {
        this.selectTemplateItem(response.data.entity.template);
        success(response);
      },
      error => {
        fail(error);
      }
    );
  };
  /**
   * it is called when user delete templte from saved template group
   * @param {object} data
   * @param {function} success
   * @param {function} fail
   */
  deleteSavedTemplateHandler = (data, success, fail) => {
    this.props.DeleteSavedTemplate(
      data,
      response => {
        this.selectTemplateItem(this.getDefaultStatusTemp(this.props.workSpaceAllTemplates));
        success(response);
      },
      error => {
        fail(error);
      }
    );
  };
  /**
   * it is called when user change and update saved template
   * @param {object} data
   * @param {function} success
   * @param {function} fail
   */
  UpdateWSSavedTemplateHandler = (data, success, fail) => {
    this.props.UpdateWSSavedTemplate(
      data,
      response => {
        this.selectTemplateItem(response);
        success(response);
      },
      error => {
        fail(error);
      }
    );
  };
  CreateWSTemplateHandler = (data, success, fail) => {
    this.props.CreateWSTemplate(
      data,
      result => {
        success(result);
        this.showSnackBar(
          this.props.intl.formatMessage({
            id: "board.template.message.success",
            defaultMessage: "New template is saved",
          }),
          "success"
        );
      },
      error => {
        fail(error);
      }
    );
  };

  render() {
    const {
      classes,
      theme,
      workspacePermissionsState,
      workSpacePer,
      workSpaceAllTemplates,
      intl,
      subscriptionDetails,
      profileState
    } = this.props;
    const { selectedIndex, selectedTemplate } = this.state;
    const permissions = workspacePermissionsState.data
      ? workspacePermissionsState.data.workSpace
      : "";
    const templatePermission = {
      isDeleted: false,
    };
    const templateConfig = {
      contextView: templatesConstants.WORKSPACE_VIEW,
      contextId: "",
      contextKeyId: "workspaceId",
      templateType: selectedTemplate && getTemplateType(selectedTemplate),
      projectTemplate: null,
      workspaceDefaultTemplate: this.props.workspaceDefault,
    };
    const currentUser = profileState.data.teamMember.find(m => {
      /** finding current user from team members array */
      return m.userId == profileState.data.userId;
    });
    return true ? (
      <>
        <div
          className={classes.workspaceSettingCnt}
          style={{ height: calculateContentHeight() + 77, overflowY: "auto" }}>
          <div className={classes.workspaceSettingHeader}>
            <div className="flex_center_start_row" style={{ marginLeft: 20 }}>
              {/* <LeftArrow
              onClick={this.goBack}
              className={classes.backArrowIcon}
            /> */}

              <Typography
                variant="h4"
                classes={{ h4: classes.workspaceHeading }}
                style={{ color: "#7E7E7E" }}>
                <FormattedMessage
                  id="workspace-settings.label"
                  defaultMessage="Workspace Settings"
                />
              </Typography>
            </div>
            <div className="flex_center_start_row" style={{ marginLeft: 5 }}>
              <ArrowRight style={{ fontSize: "19px" }} />
            </div>
            <div className="flex_center_start_row" style={{ marginLeft: 5 }}>
              <Typography variant="h4" classes={{ h4: classes.workspaceHeading }}>
                {selectedIndex == 0 ? (
                  <FormattedMessage
                    id="workspace-settings.general-information.label"
                    defaultMessage="General Information"
                  />
                ) : selectedIndex == 1 ? (
                  <FormattedMessage
                    id="workspace-settings.user-management.label"
                    defaultMessage="User Management"
                  />
                ) : selectedIndex == 2 ? (
                  <FormattedMessage
                    id="workspace-settings.roles-and-permission.label"
                    defaultMessage="Roles & Permissions"
                  />
                ) : selectedIndex == 3 ? (
                  // <CanAccessFeature group='task' feature='statusTitle'>
                  <FormattedMessage
                    id="workspace-settings.task-status.label"
                    defaultMessage="Task Status"
                  />
                  // </CanAccessFeature>
                ) : selectedIndex == 5 ? (
                  "Workspace Configuration"
                ) :
                  selectedIndex == 4 ? (
                    <FormattedMessage
                      id="workspace-settings.email-nTask.label"
                      defaultMessage="Email to nTask"
                    />
                  ) :
                    selectedIndex == 6 ? (
                      "Work Schedules"
                    ) :
                      (
                        ""
                      )}
              </Typography>
            </div>
          </div>
          <div className={classes.workspaceSettingContentCnt}>
            <Grid container direction="row" justify="flex-start" alignItems="stretch">
              <Grid item classes={{ item: classes.workspaceSettingListCnt }}>
                <List component="nav" classes={{ root: classes.workspaceSettingSideList }}>
                  {workSpacePer.workspaceSetting.generalInformation.cando && (
                    <ListItem
                      button
                      selected={selectedIndex === 0}
                      onClick={event => this.tabClick(event, 0)}
                      classes={{
                        root: classes.listItem,
                        selected: classes.listItemSelected,
                        focusVisible: classes.focusVisible,
                      }}>
                      <ListItemText
                        primary={
                          <FormattedMessage
                            id="workspace-settings.general-information.label"
                            defaultMessage="General Information"
                          />
                        }
                        classes={{ primary: classes.workspaceSideListText }}
                      />
                      {/* {selectedIndex === 0 ? <ArrowRight style={{ fontSize: "19px !important" }} /> : null} */}
                    </ListItem>
                  )}
                  {workSpacePer.workspaceSetting.usersManagement.cando && (
                    <ListItem
                      button
                      selected={selectedIndex === 1}
                      onClick={event => this.tabClick(event, 1)}
                      style={{ borderTop: "none" }}
                      classes={{
                        root: classes.listItem,
                        selected: classes.listItemSelected,
                      }}>
                      <ListItemText
                        primary={
                          <FormattedMessage
                            id="workspace-settings.user-management.label"
                            defaultMessage="User Management"
                          />
                        }
                        classes={{ primary: classes.workspaceSideListText }}
                      />
                      {/* {selectedIndex === 1 ? <ArrowRight style={{ fontSize: "19px !important" }} /> : null} */}
                    </ListItem>
                  )}
                  {workSpacePer.workspaceSetting.rolesPermissions.cando && (
                    <ListItem
                      button
                      selected={selectedIndex === 2}
                      onClick={event => this.tabClick(event, 2)}
                      style={{ borderTop: "none" }}
                      classes={{
                        root: classes.listItem,
                        selected: classes.listItemSelected,
                      }}>
                      <ListItemText
                        primary={
                          <FormattedMessage
                            id="workspace-settings.roles-and-permission.label"
                            defaultMessage="Roles & Permissions"
                          />
                        }
                        classes={{ primary: classes.workspaceSideListText }}
                      />
                      {selectedIndex === 2 ? <ArrowRight style={{ fontSize: "19px" }} /> : null}
                    </ListItem>
                  )}
                  {(this.props.profileState.data.userPlan !== "Free" && this.props.profileState.data.userPlan !== "free2018") && (currentUser.isOwner || currentUser.role == 'Admin') ? <ListItem
                    button
                    selected={selectedIndex === 5}
                    onClick={event => this.tabClick(event, 5)}
                    style={{ borderTop: "none" }}
                    classes={{
                      root: classes.listItem,
                      selected: classes.listItemSelected,
                    }}>
                    <ListItemText
                      primary={"Configuration"}
                      classes={{ primary: classes.workspaceSideListText }}
                    />
                  </ListItem> : null}
                  {false && (
                    <ListItem
                      button
                      selected={selectedIndex === 4}
                      onClick={event => this.tabClick(event, 4)}
                      style={{ borderTop: "none" }}
                      classes={{
                        root: classes.listItem,
                        selected: classes.listItemSelected,
                      }}>
                      <ListItemText
                        primary={
                          <FormattedMessage
                            id="workspace-settings.email-to-nTask.label"
                            defaultMessage="Email to nTask"
                          />}

                        classes={{ primary: classes.workspaceSideListText }}
                      />
                      {selectedIndex === 4 ? <ArrowRight style={{ fontSize: "19px" }} /> : null}
                    </ListItem>
                  )}
                  {(ConstantsPlan.isAnyBusinessPlan(subscriptionDetails.paymentPlanTitle) || ConstantsPlan.isEnterprisePlan(subscriptionDetails.paymentPlanTitle)) && (
                    <ListItem
                      button
                      selected={selectedIndex === 6}
                      onClick={event => this.tabClick(event, 6)}
                      style={{ borderTop: "none" }}
                      classes={{
                        root: classes.listItem,
                        selected: classes.listItemSelected,
                      }}>
                      <ListItemText
                        primary="Work Schedules"
                        classes={{ primary: classes.workspaceSideListText }}
                      />
                      {/* {selectedIndex === 1 ? <ArrowRight style={{ fontSize: "19px !important" }} /> : null} */}
                    </ListItem>
                  )}
                  {(ConstantsPlan.isAnyBusinessPlan(subscriptionDetails.paymentPlanTitle) || ConstantsPlan.isEnterprisePlan(subscriptionDetails.paymentPlanTitle)) && (
                    <ListItem
                      button
                      selected={selectedIndex === 7}
                      onClick={event => this.tabClick(event, 7)}
                      classes={{
                        root: classes.listItem,
                        selected: classes.listItemSelected,
                        focusVisible: classes.focusVisible,
                      }}>
                      <ListItemText
                        primary="Automation"
                        classes={{ primary: classes.workspaceSideListText }}
                      />
                      {/* {selectedIndex === 0 ? <ArrowRight style={{ fontSize: "19px !important" }} /> : null} */}
                    </ListItem>
                  )}
                  {/* <CanAccessFeature group='task' feature='statusTitle'> */}
                  {workSpacePer.workspaceSetting.tasksStatus.cando && (
                    <TaskStatus
                      data={workSpaceAllTemplates}
                      templatePermission={templatePermission}
                      templateConfig={templateConfig}
                      onClick={event => this.tabClick(event, 3)}
                      selectedTemplate={{ customstatus: selectedTemplate, mappingStatus: [] }}
                      onStatusSelect={this.selectTemplateItem}
                      addTemplate={this.addTemplate}
                      selectedIndex={selectedIndex}
                      intl={intl}
                      workspaceDefaultTemplate={this.props.workspaceDefault}
                      permission={workSpacePer.workspaceSetting.tasksStatus}
                    />
                  )}
                  {/* </CanAccessFeature> */}
                </List>
              </Grid>
              <Grid item classes={{ item: classes.profileSettingContentCnt }}>
                {selectedIndex === 0 && workSpacePer.workspaceSetting.generalInformation.cando ? (
                  <GeneralSetting
                    classes={classes}
                    theme={theme}
                    permissions={permissions}
                    showSnackBar={this.showSnackBar}
                  />
                ) : selectedIndex === 1 ? (
                  <ManageMembers
                    classes={classes}
                    theme={theme}
                    permissions={permissions}
                    showSnackBar={this.showSnackBar}
                  />
                ) : selectedIndex == 2 ? (
                  <RolesAndPermission />
                ) : selectedIndex == 3 && selectedTemplate ? (
                  <StatusList
                    templateItem={{ customstatus: selectedTemplate, mappingStatus: [] }}
                    draggable
                    style={{ paddingRight: 220 }}
                    // permission={workSpacePer}
                    intl={intl}
                    templatePermission={templatePermission}
                    templateConfig={templateConfig}
                    workSpaceStatus={workSpaceAllTemplates}
                    // SaveTemplateStatusItem={this.props.SaveWSTemplateStatusItem}
                    CreateTemplate={this.CreateWSTemplateHandler}
                    UpdateSaveTemplate={this.UpdateWSSavedTemplateHandler}
                    // DeleteWSTemplateStatusItem={this.props.DeleteWSTemplateStatusItem}
                    MapTaskWithSetDefaultTemplate={this.mapTaskWithSetDefaultStatusHandler}
                    // DeleteAndMapStatus={this.props.DeleteAndMapStatus}
                    // restoreStatus={this.props.restoreStatus}
                    // CreateNewStatus={this.props.CreateNewStatus}
                    DeleteSavedTemplate={this.deleteSavedTemplateHandler}
                    permission={workSpacePer.workspaceSetting.tasksStatus}
                  />
                ) : selectedIndex == 4 &&
                  (ConstantsPlan.isBusinessPlan(subscriptionDetails.paymentPlanTitle) ||
                    ConstantsPlan.isBusinessTrial(subscriptionDetails.paymentPlanTitle)) &&
                  workSpacePer?.workspaceSetting.emailManagement.cando ?
                  (
                    <EmailToNtask permission={workSpacePer?.workspaceSetting.emailManagement} />
                  ) : selectedIndex == 5 ? (
                    <Configurations showSnackBar={this.showSnackBar} />
                  ) : selectedIndex == 6 ? (
                    <WorkspaceCalendar />
                  ) : selectedIndex == 7 ? (
                    <Automation />
                  ) : null}
              </Grid>
              {selectedIndex == 3 && (
                <Grid item classes={{ item: null }}>
                  <div className={classes.statusInfoCnt}>
                    <span className={classes.defaultSpan}>{"Initial"}</span>
                    <span className={classes.infoMessage}>
                      {
                        "Any status can be marked as your Initial. This means any newly created tasks will automatically be added to the initial status."
                      }
                    </span>
                  </div>
                  <div className={classes.statusInfoCnt} style={{ background: "#1dc57130" }}>
                    <span className={classes.doneSpan}>{"Final"}</span>
                    <span className={classes.infoMessage}>
                      {
                        "Any status can be marked as your Final. This means all tasks in this status will be picked up by the system as completed and won't be marked as overdue."
                      }
                    </span>
                  </div>
                </Grid>
              )}
            </Grid>
          </div>
        </div>
      </>
    ) : null;
  }
}

const mapStateToProps = state => {
  return {
    workspacePermissionsState: state.workspacePermissions,
    profileState: state.profile,
    workSpacePer: state.workspacePermissions.data.workSpace,
    workSpaceAllTemplates: state.workspaceTemplates.data.allWSTemplates,
    workspaceDefault: state.workspaceTemplates.data.defaultWSTemplate,
    subscriptionDetails: state.profile.data.teams.find(
      item => item.companyId == state.profile.data.activeTeam
    )?.subscriptionDetails,
  };
};
WorkspaceSetting.defaultProps = {
  // SaveWSTemplateStatusItem(e) {
  //   return e;
  // },
  CreateWSTemplate(e) {
    return e;
  },
  UpdateWSSavedTemplate(e) {
    return e;
  },
  // DeleteWSTemplateStatusItem(e) {
  //   return e;
  // },
  MapTaskWithSetDefaultTemplate(e) {
    return e;
  },
  // DeleteAndMapStatus(e) {
  //   return e;
  // },
  // restoreStatus(e) {
  //   return e;
  // },
};
WorkspaceSetting.propTypes = {
  // SaveWSTemplateStatusItem: PropTypes.func.isRequired,
  CreateWSTemplate: PropTypes.func.isRequired,
  UpdateWSSavedTemplate: PropTypes.func.isRequired,
  // DeleteWSTemplateStatusItem: PropTypes.func.isRequired,
  MapTaskWithSetDefaultTemplate: PropTypes.func.isRequired,
  // DeleteAndMapStatus: PropTypes.func.isRequired,
  // restoreStatus: PropTypes.func.isRequired,
};

export default compose(
  withRouter,
  injectIntl,
  connect(mapStateToProps, {
    getWorkspaceRoles,
    getWorkspaceTemplates,
    // SaveWSTemplateStatusItem,
    CreateWSTemplate,
    UpdateWSSavedTemplate,
    // DeleteWSTemplateStatusItem,
    MapTaskWithSetDefaultTemplate,
    // DeleteAndMapStatus,
    // restoreStatus,
    AddWSTemplate,
    // CreateNewStatus,
    DeleteSavedTemplate,
  }),
  withSnackbar,
  withStyles(workspaceSetting, { withTheme: true })
)(WorkspaceSetting);
