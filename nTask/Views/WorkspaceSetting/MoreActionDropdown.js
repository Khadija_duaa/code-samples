import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import workspaceSetting from "./styles";
import menuStyles from "../../assets/jss/components/menu";
import CustomIconButton from "../../components/Buttons/IconButton";
import SelectionMenu from "../../components/Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import combineStyles from "../../utils/mergeStyles";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import { FormattedMessage } from "react-intl";

class MoreActionDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      pickerOpen: false,
      pickerPlacement: "",
      archiveFlag: false,
      unArchiveFlag: false,
      deleteFlag: false,
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handlePickerClose = this.handlePickerClose.bind(this);
  }

  handleClose(event) {
    this.setState({ open: false, pickerOpen: false });
  }
  handlePickerClose(event) {
    this.setState({ pickerOpen: false });
  }

  handleClick(event, placement) {
    const { currentTarget } = event;
    this.setState((state) => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }
  render() {
    const {
      classes,
      theme,
      permission,
      removeMembers,
      deleteMember,
      isActive,
      enableMember,
      removeWsMember,
      workSpacePer,
    } = this.props;
    const {
      open,
      placement,
      pickerOpen,
      popupFlag,
      selectedColor,
    } = this.state;
    if (
      workSpacePer.workspaceSetting.usersManagement.disableMembers.cando ||
      workSpacePer.workspaceSetting.usersManagement.removeMembers.cando
    ) {
      /** if user have not access to disable member then drop down not shows, this is temporary check because there is only one option in drop down for now */
      return (
        <ClickAwayListener onClickAway={this.handleClose}>
          <div>
            <CustomIconButton
              btnType="condensed"
              onClick={(event) => {
                this.handleClick(event, "bottom-end");
              }}
              buttonRef={(node) => {
                this.anchorEl = node;
              }}
            >
              <MoreVerticalIcon
                htmlColor={theme.palette.secondary.medDark}
                style={{ fontSize: "28px" }}
              />
            </CustomIconButton>

            <SelectionMenu
              open={open}
              closeAction={this.handleClose}
              placement={placement}
              anchorRef={this.anchorEl}
              list={
                <List>
                  <ListItem disableRipple className={classes.headingItem}>
                    <ListItemText
                      primary={
                        <FormattedMessage
                          id="common.action.label"
                          defaultMessage="Select Action"
                        />
                      }
                      classes={{ primary: classes.headingText }}
                    />
                  </ListItem>
                  {isActive ===
                    false /** getting permission if the current user have access of enable members regarding its role */ &&
                    // <ListItem
                    //   button
                    //   disableRipple
                    //   classes={{ selected: classes.statusMenuItemSelected }}
                    //   onClick={enableMember}
                    // >
                    //   <ListItemText
                    //     primary="Enable this user"
                    //     classes={{
                    //       primary: classes.statusItemText
                    //     }}
                    //   />
                    // </ListItem>
                    null}
                  {isActive == true &&
                    workSpacePer.workspaceSetting.usersManagement.disableMembers
                      .cando /** getting permission if the current user have access of disable members regarding its role */ && (
                      <ListItem
                        button
                        disableRipple
                        classes={{ selected: classes.statusItemText }}
                        onClick={() => {
                          removeMembers();
                          this.handleClose();
                        }}
                      >
                        <ListItemText
                          primary={
                            <FormattedMessage
                              id="workspace-settings.user-management.add-members.user-disable.label"
                              defaultMessage="Disabled User"
                            />
                          }
                          classes={{
                            primary: classes.statusItemTextDanger,
                          }}
                        />
                      </ListItem>
                    )}
                  {workSpacePer.workspaceSetting.usersManagement.removeMembers
                    .cando && (
                      <ListItem
                        button
                        disableRipple
                        classes={{ selected: classes.statusItemText }}
                        onClick={() => {
                          deleteMember();
                          this.handleClose();
                        }}
                      >
                        <ListItemText
                          primary={
                            <FormattedMessage
                              id="workspace-settings.user-management.add-members.remove-user.label"
                              defaultMessage="Remove User"
                            />
                          }
                          classes={{
                            primary: classes.statusItemTextDanger,
                          }}
                        />
                      </ListItem>
                    )}
                </List>
              }
            />
          </div>
        </ClickAwayListener>
      );
    } else return null;
  }
}

export default withStyles(combineStyles(workspaceSetting, menuStyles), {
  withTheme: true,
})(MoreActionDropdown);
