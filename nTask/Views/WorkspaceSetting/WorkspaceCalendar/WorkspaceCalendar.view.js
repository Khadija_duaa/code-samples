import React, { useEffect, useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { withSnackbar } from "notistack";
import CustomButton from "../../../components/Buttons/CustomButton";
import { styles } from "../../../components/CalendarModule/Calendar.style";
import { Add } from "@material-ui/icons";
import { FormattedMessage } from "react-intl";
import { withRouter } from "react-router-dom";
import { Typography } from "@material-ui/core";
import { format } from "date-fns";
import { useDispatch, useSelector } from "react-redux";
import { updateCurrentWorkspaceCalender } from "../../../redux/actions/workspace";
import CustomSelect from "../../../components/Dropdown/CustomSelect/CustomSelect.view";
import CalendarForm from "../../../components/CalendarModule/CalendarForm.view";
import { getCalenderList, setDefaultWorkspaceCalender } from "../../../redux/actions/calendar";
import { parseDate, DAYSOPTIONS } from "../../../components/CalendarModule/constant";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import UpdateCalendarModal from "../../../components/CalendarModule/component/UpdateCalendarModal";


const WorkspaceCalendar = (props) => {
  const { theme, classes, enqueueSnackbar, history } = props;

  const user = useSelector((store) => store.profile.data);
  const dispatch = useDispatch();
  const [data, setData] = useState(undefined);
  const [refresh, setRefresh] = useState(true);
  const [selected, setSelected] = useState({ data: undefined, type: undefined, loading: false });

  const activeWorkspace = user.workspace.find(
    (workspace) => workspace.teamId === user.loggedInTeam
  );
  const role = user.teamMember.find((member) => member.userId == user.userId)?.role;

  useEffect(() => {
    if (refresh)
      getCalenderList((resp) => {
        setData(
          resp.entity.map((item) => ({ ...item, label: item.title, value: item.calenderId }))
        );
        setRefresh(false);
      });
  }, [refresh]);

  const showSnackBar = (message, type) => {
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{message}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  const onSubmit = () => {
    setSelected((prev) => ({ ...prev, loading: true }));
    setDefaultWorkspaceCalender(
      selected.data.calenderId,
      () => {
        dispatch(updateCurrentWorkspaceCalender(selected.data));
        setSelected({ data: undefined, type: undefined, loading: false });
        showSnackBar("Workspace Schedule Updated", "info");
      },
      (error) => {
        setSelected((prev) => ({ ...prev, loading: false }));
        showSnackBar(error.data.message, "error");
      }
    );
  };

  if (!data)
    return (
      <div
        style={{
          height: window.innerHeight - 325,
          width: "100%",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}>
        <div className="loader" />
      </div>
    );

  return (
    <div style={{ maxWidth: 685 }}>
      <div className={classes.CalendarMain} style={props.style}>
        <div className={classes.SelectWorkSchedule}>
          <div className={classes.SelectWorkScheduletext}>Select Work Schedule</div>
          {(role == "Owner" || role == "Admin") && (
            <CustomButton
              onClick={() => history.push("/team-settings/work-calendar")}
              btnType="success"
              variant="text"
              style={{ marginBottom: 0, padding: "8px 20px" }}>
              <Add className={classes.addIcon} />
              Create New
            </CustomButton>
          )}
        </div>

        <div className={classes.CalendarMain}>
          <CustomSelect
            showCheck={false}
            bgapply={false}
            value={activeWorkspace.calendar?.calenderId}
            style={{
              width: "100%",
              margin: "0px 0px 20px 0px",
              justifyContent: "space-between",
              backgroundColor: "white",
              display: "flex",
              border: "1px solid #EAEAEA",
            }}
            onChange={(value) => setSelected({ data: value, type: "default", loading: false })}
            height="390px"
            width="100%"
            options={data ?? []}
            render={(calender, selected, index) => (
              <div
                key={index}
                style={{ display: "flex", padding: ".5rem ", borderBottom: "1px solid #f1f1f1" }}
                className="group">
                <div style={{ flex: 1 }}>
                  <div
                    style={{ color: selected ? "#0090FF" : "#000000" }}
                    className={classes.calenderNameText}>
                    {calender.title}
                  </div>
                  <div style={{ display: "flex", alignItems: "center" }}>
                    <div style={{ display: "flex", alignItems: "center" }}>
                      {DAYSOPTIONS.filter((item) =>
                        calender.workingdays.some((day) => day.id === item.value.id)
                      ).map((item, index) => (
                        <Typography variant="body2" key={index} style={{ marginRight: ".5rem" }}>
                          {item.label}
                        </Typography>
                      ))}
                    </div>
                    {!!calender.workingHours.length ? (
                      <>
                        <div className={classes.horizontalLine}>&nbsp;</div>
                        <div style={{ display: "flex", alignItems: "center" }}>
                          {calender.workingHours.slice(0, 1).map((slot) => (
                            <div className={classes.chip}>
                              {format(parseDate(slot.timeFrom), "hh:mm a")}-
                              {format(parseDate(slot.timeTo), "hh:mm a")}
                            </div>
                          ))}
                          {calender.workingHours.length > 1 && <div className={classes.chip}>...</div>}
                        </div>
                      </>
                    ) : (
                      !!calender.dailyCapacity && (
                        <>
                          <div className={classes.horizontalLine}>&nbsp;</div>
                          <div className={classes.chip}>{calender.dailyCapacity} hours</div>
                        </>
                      )
                    )}

                    {!!calender?.exceptions?.length && (
                      <>
                        <div className={classes.horizontalLine}>&nbsp;</div>
                        <div
                          style={{ display: "flex", alignItems: "center" }}
                          className={classes.chip}>
                          {calender?.exceptions?.length} exceptions
                        </div>
                      </>
                    )}
                  </div>
                </div>
              </div>
            )}
          />
          <CalendarForm
            defaultValue={activeWorkspace.calendar}
            readOnly={true}
            disabled
          />
        </div>
      </div>

      {/* <ActionConfirmation
        open={selected.type === "default"}
        closeAction={() => setSelected({ data: undefined, type: undefined, loading: false })}
        cancelBtnText={<FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />}
        successBtnText={'Update Schedule'}
        alignment="left"
        headingText={'Update Work Schedule'}
        // iconType="markAll"
        msgText={'This might affect the dates and timeline of the project and resources. Are you sure you want to update the work schedule of this Workspace?'}
        successAction={onSubmit}
        btnQuery={selected.loading && 'progress'}
      /> */}
      <UpdateCalendarModal
        open={selected.type === "default"}
        loading={selected.loading}
        onClose={() => setSelected({ data: undefined, type: undefined, loading: false })}
        onSubmit={onSubmit}
      />
    </div>
  );
};

export default compose(
  withRouter,
  withSnackbar,
  withStyles(styles, { withTheme: true })
)(WorkspaceCalendar);
