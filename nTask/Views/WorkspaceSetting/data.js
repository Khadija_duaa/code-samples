const data =    {
  "roleName": "Owner 02",
  "permission": [
      {
          "title": "Workspace Settings",
          "permission": [
              {
                  "type": 0,
                  "title": "View Workspace Settings",
                  "key": "ViewWorkSpaceSetting",
                  "value": true
              },
              {
                  "type": 0,
                  "title": "Edit Name & Thumbnail",
                  "key": "EditNameAndThumbnail",
                  "value": true
              },
              {
                  "type": 0,
                  "title": "Delete Workspace",
                  "key": "DeleteWorkSpace",
                  "value": true
              }
          ]
      },
      {
          "title": "Users & Groups",
          "permission": [
              {
                  "type": 0,
                  "title": "View Workspace Settings",
                  "key": "ViewWorkSpaceSetting",
                  "value": true
              },
              {
                  "type": 0,
                  "title": "Edit Name & Thumbnail",
                  "key": "EditNameAndThumbnail",
                  "value": true
              },
              {
                  "type": 0,
                  "title": "Delete Workspace",
                  "key": "DeleteWorkSpace",
                  "value": true
              }
          ]
      },
      {
          "title": "Roles & Permissions",
          "permission": [
              {
                  "type": 2,
                  "title": "View Roles & Permissions",
                  "key": "ViewRolesAndPermission",
                  "value": true
              },
              {
                  "type": 2,
                  "title": "Manage roles",
                  "key": "ManageRoles",
                  "value": true
              },
              {
                  "type": 2,
                  "title": "View/Edit project settings",
                  "key": "EditProjectSetting",
                  "value": true
              }
          ]
      },
      {
          "title": "Projects",
          "permission": [
              {
                  "type": 3,
                  "title": "Can view Project Settings",
                  "key": "ViewProjectSetting",
                  "value": true
              },
              {
                  "type": 3,
                  "title": "Can view Project Gantt",
                  "key": "accessGantt",
                  "value": true
              },
              {
                  "type": 3,
                  "title": "View unassigned projects",
                  "key": "ViewUnassignedProject",
                  "value": true
              },
              {
                  "type": 3,
                  "title": "Import projects from CSV",
                  "key": "CanImport",
                  "value": true
              },
              {
                  "type": 3,
                  "title": "Export projects in CSV",
                  "key": "CanExport",
                  "value": true
              },
              {
                  "type": 3,
                  "title": "Archive/Unarchive projects",
                  "key": "CanArchiveUnarchive",
                  "value": true
              },
              {
                  "type": 3,
                  "title": "Create projects",
                  "key": "CreateProjects",
                  "value": true
              },
              {
                  "type": 3,
                  "title": "Delete projects",
                  "key": "DeleteProjects",
                  "value": true
              },
              {
                  "type": 3,
                  "title": "Copy within workspace",
                  "key": "CopyInWorkspace",
                  "value": true
              },
              {
                  "type": 3,
                  "title": "Copy to another workspace",
                  "key": "CopyOutWorkspace",
                  "value": true
              },
              {
                  "type": 3,
                  "title": "Public link",
                  "key": "PublicLink",
                  "value": true
              }
          ]
      },
      {
          "title": "Tasks",
          "permission": [
              {
                  "type": 4,
                  "title": "View unassigned tasks",
                  "key": "ViewUnassignedTasks",
                  "value": true
              },
              {
                  "type": 4,
                  "title": "Import tasks from CSV",
                  "key": "ImportTasks",
                  "value": true
              },
              {
                  "type": 4,
                  "title": "Export tasks to CSV",
                  "key": "ExportTasks",
                  "value": true
              },
              {
                  "type": 4,
                  "title": "Task title and description",
                  "key": "TitleAndDescription",
                  "value": true
              },
              {
                  "type": 4,
                  "title": "Change task's project",
                  "key": "ChangeProject",
                  "value": true
              },
              {
                  "type": 4,
                  "title": "Start and End date",
                  "key": "ChangeStartEndDate",
                  "value": true
              },
              {
                  "type": 4,
                  "title": "Add/Edit checklist items",
                  "key": "AddEditCheckList",
                  "value": true
              },
              {
                  "type": 4,
                  "title": "Change task status & priority",
                  "key": "ChangeStatusPriority",
                  "value": true
              },
              {
                  "type": 4,
                  "title": "Add/remove assignee",
                  "key": "AddRemoveAssignee",
                  "value": true
              },
              {
                  "type": 4,
                  "title": "Time Entry & Estimation",
                  "key": "TimeAndEstimation",
                  "value": true
              },
              {
                  "type": 4,
                  "title": "Repeat task",
                  "key": "RepeatTasks",
                  "value": true
              },
              {
                  "type": 4,
                  "title": "Comments & attachments",
                  "key": "AddComments",
                  "value": true
              },
              {
                  "type": 4,
                  "title": "Archive/Unarchive",
                  "key": "ArchiveUnarchive",
                  "value": true
              },
              {
                  "type": 4,
                  "title": "Create tasks",
                  "key": "CreateTasks",
                  "value": true
              },
              {
                  "type": 4,
                  "title": "Delete tasks",
                  "key": "DeleteTasks",
                  "value": true
              },
              {
                  "type": 4,
                  "title": "Copy within workspace",
                  "key": "CopyInWorkspace",
                  "value": true
              },
              {
                  "type": 4,
                  "title": "Copy to another workspace",
                  "key": "CopyOutWorkspace",
                  "value": true
              },
              {
                  "type": 4,
                  "title": "Public link",
                  "key": "PublicLink",
                  "value": true
              },
              {
                  "type": 4,
                  "title": "Add category",
                  "key": "AddCategory",
                  "value": true
              }
          ]
      },
      {
          "title": "Issues",
          "permission": [
              {
                  "type": 5,
                  "title": "View unassigned issues",
                  "key": "ViewUnassignedIssues",
                  "value": true
              },
              {
                  "type": 5,
                  "title": "Import issues from CSV",
                  "key": "ImportIssues",
                  "value": true
              },
              {
                  "type": 5,
                  "title": "Export issues to CSV",
                  "key": "ExportIssues",
                  "value": true
              },
              {
                  "type": 5,
                  "title": "Issue title and description",
                  "key": "TitleAndDescription",
                  "value": true
              },
              {
                  "type": 5,
                  "title": "Link tasks",
                  "key": "LinkTasks",
                  "value": true
              },
              {
                  "type": 5,
                  "title": "Start and End date",
                  "key": "ChangeStartEndDate",
                  "value": true
              },
              {
                  "type": 5,
                  "title": "Add category",
                  "key": "AddCategory",
                  "value": true
              },
              {
                  "type": 5,
                  "title": "Change issue status, priority & severity",
                  "key": "ChangeStatusPriority",
                  "value": true
              },
              {
                  "type": 5,
                  "title": "Issue type",
                  "key": "ChangeIssueType",
                  "value": true
              },
              {
                  "type": 5,
                  "title": "Add/remove assignee",
                  "key": "AddRemoveAssignee",
                  "value": true
              },
              {
                  "type": 5,
                  "title": "Time Entry & estimation",
                  "key": "TimeAndEstimation",
                  "value": true
              },
              {
                  "type": 5,
                  "title": "Comments & attachments",
                  "key": "AddComments",
                  "value": true
              },
              {
                  "type": 5,
                  "title": "Archive/Unarchive",
                  "key": "ArchieveUnarchive",
                  "value": true
              },
              {
                  "type": 5,
                  "title": "Create issues",
                  "key": "CreateIssues",
                  "value": true
              },
              {
                  "type": 5,
                  "title": "Delete issues",
                  "key": "DeleteIssues",
                  "value": true
              },
              {
                  "type": 5,
                  "title": "Copy within workspace",
                  "key": "CopyInWorkspace",
                  "value": true
              },
              {
                  "type": 5,
                  "title": "Copy to another workspace",
                  "key": "CopyOutWorkspace",
                  "value": true
              },
              {
                  "type": 5,
                  "title": "Public link",
                  "key": "PublicLink",
                  "value": true
              }
          ]
      },
      {
          "title": "Risks",
          "permission": [
              {
                  "type": 6,
                  "title": "View unassigned risks",
                  "key": "ViewUnassignedRisks",
                  "value": true
              },
              {
                  "type": 6,
                  "title": "Import risks from CSV",
                  "key": "ImportRisks",
                  "value": true
              },
              {
                  "type": 6,
                  "title": "Export issues to CSV",
                  "key": "ExportRisks",
                  "value": true
              },
              {
                  "type": 6,
                  "title": "Risk title and details",
                  "key": "TitleAndDetails",
                  "value": true
              },
              {
                  "type": 6,
                  "title": "Mitigation plan",
                  "key": "MigitationPlan",
                  "value": true
              },
              {
                  "type": 6,
                  "title": "Link tasks",
                  "key": "LinkTasks",
                  "value": true
              },
              {
                  "type": 6,
                  "title": "Add category",
                  "key": "AddCategory",
                  "value": true
              },
              {
                  "type": 6,
                  "title": "Change risk status, impact & likelihood",
                  "key": "ChangeStatusImpace",
                  "value": true
              },
              {
                  "type": 6,
                  "title": "Add/remove risk owner",
                  "key": "AddRemoveRiskOwner",
                  "value": true
              },
              {
                  "type": 6,
                  "title": "Comments & attachments",
                  "key": "AddComments",
                  "value": true
              },
              {
                  "type": 6,
                  "title": "Archive/Unarchive",
                  "key": "ArchiveUnarchive",
                  "value": true
              },
              {
                  "type": 6,
                  "title": "Create risk",
                  "key": "CreateRisk",
                  "value": true
              },
              {
                  "type": 6,
                  "title": "Delete risk",
                  "key": "DeleteRisk",
                  "value": true
              },
              {
                  "type": 6,
                  "title": "Copy within workspace",
                  "key": "CopyInWorkspace",
                  "value": true
              },
              {
                  "type": 6,
                  "title": "Copy to another workspace",
                  "key": "CopyOutWorkspace",
                  "value": true
              },
              {
                  "type": 6,
                  "title": "Public link",
                  "key": "PublicLink",
                  "value": true
              }
          ]
      },
      {
          "title": "Meetings",
          "permission": [
              {
                  "type": 7,
                  "title": "View all meetings",
                  "key": "ViewAllMeetings",
                  "value": true
              },
              {
                  "type": 7,
                  "title": "Edit meeting details (title, location, date, time)",
                  "key": "EditDetails",
                  "value": true
              },
              {
                  "type": 7,
                  "title": "Add/remove participants",
                  "key": "AddRemoveParticipant",
                  "value": true
              },
              {
                  "type": 7,
                  "title": "Add/edit discussion points",
                  "key": "AddEditDiscussion",
                  "value": true
              },
              {
                  "type": 7,
                  "title": "Add/edit agenda",
                  "key": "AddEditAgenda",
                  "value": true
              },
              {
                  "type": 7,
                  "title": "Add/edit follow-up actions",
                  "key": "AddEditFollowup",
                  "value": true
              },
              {
                  "type": 7,
                  "title": "Add/edit decisions",
                  "key": "AddEditDecision",
                  "value": true
              },
              {
                  "type": 7,
                  "title": "Cancel meeting",
                  "key": "CancelMeeting",
                  "value": true
              },
              {
                  "type": 7,
                  "title": "Archive/Unarchive meetings",
                  "key": "ArchiveUnarchive",
                  "value": true
              },
              {
                  "type": 7,
                  "title": "Delete meeting",
                  "key": "DeleteMeeting",
                  "value": true
              },
              {
                  "type": 7,
                  "title": "Submit for review",
                  "key": "SubmitForReview",
                  "value": true
              },
              {
                  "type": 7,
                  "title": "Publish meeting",
                  "key": "PublishMeeting",
                  "value": true
              },
              {
                  "type": 7,
                  "title": "Unpublish meeting",
                  "key": "UnpublishMeeting",
                  "value": true
              },
              {
                  "type": 7,
                  "title": "Create/edit meeting schedule",
                  "key": "EditSchedule",
                  "value": true
              }
          ]
      },
      {
          "title": "Timesheets",
          "permission": [
              {
                  "type": 8,
                  "title": "Approve/reject timesheet",
                  "key": "ApproveReject",
                  "value": true
              },
              {
                  "type": 8,
                  "title": "Withdraw timesheet",
                  "key": "WithdrawTimesheet",
                  "value": true
              },
              {
                  "type": 8,
                  "title": "View unsubmitted timesheet",
                  "key": "ViewUnsubmitted",
                  "value": true
              }
          ]
      }
  ]
}
export default data;
