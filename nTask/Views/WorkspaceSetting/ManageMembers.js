import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import workspaceSetting from "./styles";
import Typography from "@material-ui/core/Typography";
import DefaultTextField from "../../components/Form/TextField";
import NotificationMessage from "../../components/NotificationMessages/NotificationMessages";
import CustomAvatar from "../../components/Avatar/Avatar";
import CustomButton from "../../components/Buttons/CustomButton";
import MoreActionDropdown from "./MoreActionDropdown";
import FormHelperText from "@material-ui/core/FormHelperText";
import {
  InviteTeamMembers,
  RemoveMembers,
  EnableDisableMembers,
  RemoveWorkspaceMembers,
  ResendInviteTeamMembers,
  ChangeUserRole,
  AddMembersToWorkSpace,
} from "../../redux/actions/workspace";
import { filterMutlipleEmailsInput } from "../../utils/formValidations";
import DeleteConfirmDialog from "../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import ActionConfirmation from "../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import { generateAssigneeData } from "../../helper/generateSelectData";
import SelectSearchDropdown from "../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import differenceWith from "lodash/differenceWith";
import {
  generateRoleData,
  generateWorkspaceAdminRoleData,
  generateWorkspaceRoleData,
} from "../../helper/generateSelectData";
import { GetPermission } from "../../components/permissions";
import { injectIntl, FormattedMessage } from "react-intl";
import { Scrollbars } from "react-custom-scrollbars";

class ManageMembers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      EmailAddressError: "",
      EmailAddressErrorText: "",
      emailList: "",
      disableButton: false,
      confirmDelete: false,
      confirmDisable: false,
      confirmEnable: false,
      confirminvite: false,
      confirmSelectedRole: false,
      loggedInTeam: "",
      userId: "",
      email: "",
      changedRoleValue: "Member",
      previousRoleValue: "Member",
      role: {},
      workspaceName: "",
      btnQuery: "",
      disable: false,
      resendInviteEmailBtnQuery: "",
      deleteMemberBtnQuery: "",
      changeRoleBtnQuery: "",
      enableMemberBtnQuery: "",
      disableMemberBtnQuery: "",
      reset: false,
      roleId: "003",
      emailListValues: [],
      newAssignee: [],
      errorInvitation: false,
      errorInvitationMessage: "",
      userFilter: {
        label: this.props.intl.formatMessage({
          id:
            "workspace-settings.user-management.add-members.filter-dropdown.all-user.label",
          defaultMessage: "All Users",
        }),
        value: "allUsers",
      },
    };
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.handleInviteSend = this.handleInviteSend.bind(this);
    this.deleteMemberConfrimation = this.deleteMemberConfrimation.bind(this);
    this.deleteMember = this.deleteMember.bind(this);
    this.closeDeleteMember = this.closeDeleteMember.bind(this);
    this.enableMemberConfrimation = this.enableMemberConfrimation.bind(this);
    this.enableMember = this.enableMember.bind(this);
    this.closeEnableMember = this.closeEnableMember.bind(this);
    this.disableMemberConfrimation = this.disableMemberConfrimation.bind(this);
    this.disableMember = this.disableMember.bind(this);
    this.closeDisableMember = this.closeDisableMember.bind(this);
    this.inviteMemberConfrimation = this.inviteMemberConfrimation.bind(this);
    this.inviteMember = this.inviteMember.bind(this);
    this.closeinviteMember = this.closeinviteMember.bind(this);
    this.handleSelectedRole = this.handleSelectedRole.bind(this);
    this.closeSelectedRolePopup = this.closeSelectedRolePopup.bind(this);
    this.updateSelectedRole = this.updateSelectedRole.bind(this);
  }
  componentDidMount() {
    const { profileState, permissions } = this.props;
    const loggedInTeam =
      profileState.data && profileState.data.workspace
        ? profileState.data.workspace.find(
            (x) => x.teamId === profileState.data.loggedInTeam
          )
        : {};
    this.setState({
      loggedInTeam,
      workspaceName: loggedInTeam.teamName,
      disableButton: permissions ? !permissions.inviteMembers : false,
    });
  }
  updateSelectedRole(type) {
    const object = {
      MemberId: type,
      Role: this.state[type].value,
      RoleId: this.state[type].id,
    };
    this.setState({ changeRoleBtnQuery: "progress" }, () => {
      this.props.ChangeUserRole(
        object,
        (response) => {
          this.setState({
            changeRoleBtnQuery: "",
            confirmSelectedRole: false,
            role: "",
            changedRoleValue: "",
          });
        },
        (error) => {
          this.setState({
            changeRoleBtnQuery: "",
            confirmSelectedRole: false,
            role: "",
            changedRoleValue: "",
          });
          this.props.showSnackBar(error.data.message, "error");
        }
      );
    });
  }
  handleSelectedRole(role, value) {
    this.setState({
      role,
      confirmSelectedRole: true,
      changedRoleValue: value.Role,
      roleId: value.RoleId,
    });
  }
  closeSelectedRolePopup(reset) {
    this.setState({
      confirmSelectedRole: false,
      role: this.state.role,
      changedRoleValue: "",
      reset: reset === true ? false : true,
    });
  }

  deleteMemberConfrimation() {
    this.setState({ deleteMemberBtnQuery: "progress" }, () => {
      this.props.RemoveMembers(
        { userId: this.state.userId },
        (response) => {
          this.setState({
            userId: "",
            confirmDelete: false,
            deleteMemberBtnQuery: "",
          });
        },
        (error) => {
          this.setState({
            userId: "",
            confirmDelete: false,
            deleteMemberBtnQuery: "",
          });
          this.props.showSnackBar(error.data.message, "error");
        }
      );
    });
  }
  deleteMember(userId) {
    this.setState({ confirmDelete: true, userId });
  }
  closeDeleteMember() {
    this.setState({ confirmDelete: false, userId: "" });
  }

  enableMemberConfrimation() {
    let obj = {
      enableDisableUser: 0,
      MemberId: this.state.userId,
    };
    this.setState({ enableMemberBtnQuery: "progress" }, () => {
      this.props.EnableDisableMembers(
        obj,
        (response) => {
          this.setState({
            enableMemberBtnQuery: "",
            confirmEnable: false,
            userId: "",
          });
        },
        (error) => {
          this.setState({
            enableMemberBtnQuery: "",
            confirmEnable: false,
            userId: "",
          });
          this.props.showSnackBar(error.data.message, "error");
        }
      );
    });
  }
  enableMember(userId) {
    this.setState({ confirmEnable: true, userId });
  }
  closeEnableMember() {
    this.setState({ confirmEnable: false, userId: "" });
  }

  disableMemberConfrimation() {
    let obj = {
      enableDisableUser: 1,
      MemberId: this.state.userId,
    };
    this.setState({ disableMemberBtnQuery: "progress" }, () => {
      this.props.EnableDisableMembers(
        obj,
        (response) => {
          this.setState({
            disableMemberBtnQuery: "",
            confirmDisable: false,
            userId: "",
          });
        },
        (error) => {
          this.setState({
            disableMemberBtnQuery: "",
            confirmDisable: false,
            userId: "",
          });
          this.props.showSnackBar(error.data.message, "error");
        }
      );
    });
  }
  disableMember(userId) {
    this.setState({ confirmDisable: true, userId });
  }
  closeDisableMember() {
    this.setState({ confirmDisable: false, userId: "" });
  }

  inviteMemberConfrimation() {
    let obj = {
      Members: [{ email: this.state.email, userId: this.state.userId }],
    };
    this.setState({ resendInviteEmailBtnQuery: "progress" }, () => {
      this.props.ResendInviteTeamMembers(
        obj,
        (response) => {
          this.setState({
            confirminvite: false,
            userId: "",
            email: "",
            resendInviteEmailBtnQuery: "",
          });
        },
        (error) => {
          this.setState({
            confirminvite: false,
            resendInviteEmailBtnQuery: "",
          });
          this.props.showSnackBar(error.data.message, "error");
        }
      );
    });
  }
  inviteMember(x) {
    this.setState({ confirminvite: true, userId: x.userId, email: x.email });
  }
  closeinviteMember() {
    this.setState({ confirminvite: false, userId: "", email: "" });
  }

  handleEmailInput = (name) => (event) => {
    let inputValue = event.target.value;
    //  localStorage.setItem("MomEmails", inputValue);
    inputValue = filterMutlipleEmailsInput(inputValue);
    this.setState({
      emailList: inputValue,
      EmailAddressError: false,
      EmailAddressErrorText: "",
      disable: inputValue ? false : true,
    });
  };
  handleSelectChange(value) {
    this.setState({ feedbackType: value });
  }

  checkForSelfInvitation = (userEmails) => {
    return userEmails.find((email) => {
      return email === this.props.profileState.data.email;
    });
  };

  checkForAlreadyInvitedMember = (userEmail, selectedWorkspaceMembers) => {
    return selectedWorkspaceMembers.find((obj) => {
      return obj.email === userEmail;
    });
  };

  handleInviteSend() {
    /** function calling onClick Add To WorkSpace Button */
    const { emailListValues } = this.state;
    const { profileState } = this.props;
    let newObj = emailListValues.map(function(key) {
      return key.id;
    });
    this.setState({ btnQuery: "progress" }, () => {
      this.props.AddMembersToWorkSpace(
        /** action for sending array of objects to add memebers in workspace   */
        newObj,
        (res) => {
          this.setState({
            btnQuery: "",
            emailListValues: [],
            disable: true,
            errorInvitation: false,
            errorInvitationMessage: "",
          });
        },
        (err) => {
          this.setState({
            btnQuery: "",
            errorInvitation: true,
            errorInvitationMessage: err.data,
          });
        },
        profileState.data.loggedInTeam
      );
    });
  }
  handleInput = (name) => (event) => {
    let inputValue = event.target.value;
    this.setState({
      [name]: inputValue,
      EmailAddressError: false,
      EmailAddressErrorText: "",
      disable: inputValue && name !== "workspaceMember" ? false : true,
    });
  };
  generateEmailsData = () => {
    const { profileState } = this.props;
    let teamMembers = profileState.data.teamMember;
    let allMembers = profileState.data.member.allMembers;
    let deletedMembers = profileState.data.member.allMembers.filter((m) => {
      return m.isDeleted;
    });
    let updatedList = differenceWith(
      teamMembers,
      allMembers,
      (first, second) => {
        return first.userId == second.userId && !second.isDeleted;
      }
    );

    return generateAssigneeData(updatedList);
  };

  handleCreateOptionsSelect = (type, option) => {
    this.setState({
      emailListValues: option,
      disable: option ? false : true,
      errorInvitation: false,
      errorInvitationMessage: "",
    });
  };
  handleSelectedRole = (type, option) => {
    this.setState({ [type]: option }, () => {
      this.updateSelectedRole(type);
    });
  };
  //Local state is update for dropdown so object can be made to send update to backend with API
  //Updating local state when option is selected in workspaces dropdown
  handleOptionsSelect = (type, value) => {
    this.setState({ [type]: value });
  };
  sortingMembersByRole = (selectedTeam, userFilter) => {
    /** function for sorting the  members by its role */
    let users;
    switch (userFilter) {
      case "allUsers":
        let teamOwner = selectedTeam.filter((a) => {
          /** members which are team owner => 000 = Team Owner */
          return a.roleId == "000";
        });
        let teamAdmin = selectedTeam.filter((a) => {
          /** members which are team admin => 006 = Team admin */
          return a.roleId == "006";
        });
        let workspaceAdmin = selectedTeam.filter((a) => {
          /** members which are workspace admin => 002 = workspace admin */
          return a.roleId == "002";
        });
        let workspaceMember = selectedTeam.filter((a) => {
          /** members which are workspace member => 003 = workspace member */
          return a.roleId == "003";
        });
        let workspaceLimitedMember = selectedTeam.filter((a) => {
          /** members which are workspace limited member => 001 =  workspace limited member */
          return a.roleId == "004";
        });
        let activeMember = selectedTeam.filter((a) => {
          /** members which are workspace member and enabled */
          return a.roleId == "003" && a.isActive == true;
        });
        let disableMember = selectedTeam.filter((a) => {
          /** members which are workspace member and disabled */
          return a.roleId == "003" && a.isActive == false;
        });
        let invitedMember = selectedTeam.filter((a) => {
          /** members which are workspace member and disabled */
          return a.roleId == "003" && a.isActive == null;
        });
        let customRoles = selectedTeam.filter((c) => {
          /** members which have custom role assigned */
          return c.isCustomRole == true;
        });
        users = [
          ...teamOwner,
          ...teamAdmin,
          ...workspaceAdmin,
          ...workspaceLimitedMember,
          ...activeMember,
          ...customRoles,
          ...disableMember,
          ...invitedMember,
        ];
        break;
      case "teamOwner":
        users = selectedTeam.filter((a) => {
          /** members which are team owner => 000 = Team Owner */
          return a.roleId == "000";
        });
        break;
      case "teamAdmin":
        users = selectedTeam.filter((a) => {
          /** members which are team admin => 006 = Team admin */
          return a.roleId == "006";
        });
        break;
      case "workspaceAdmin":
        users = selectedTeam.filter((a) => {
          /** members which are workspace admin => 002 = workspace admin */
          return a.roleId == "002";
        });
        break;
      case "workspaceMember":
        users = selectedTeam.filter((a) => {
          /** members which are workspace member => 003 = workspace member */
          return a.roleId == "003" && a.isActive != null;
        });
        break;
      case "workspaceLimitedMember":
        users = selectedTeam.filter((a) => {
          /** members which are workspace limited member => 001 =  workspace limited member */
          return a.roleId == "004";
        });
        break;
      case "disabledUser":
        users = selectedTeam.filter((a) => {
          /** members which are workspace member and disabled */
          return a.isActive == false;
        });
        break;
      case "enabledUser":
        users = selectedTeam.filter((a) => {
          /** members which are workspace member and enabled */
          return a.isActive == true;
        });
        break;
    }
    return users;
  };

  generateDropDownData = (permission) => {
    const workspaceRoles =
      this.props.permissionSettingsState.data.defaultRoles || [];
    if (permission) {
      return generateWorkspaceAdminRoleData();
    } else {
      return generateWorkspaceRoleData(workspaceRoles);
    }
  };
  generateFilterDropDown = () => {
    let dd = [
      {
        label: this.props.intl.formatMessage({
          id:
            "workspace-settings.user-management.add-members.filter-dropdown.all-user.label",
          defaultMessage: "All Users",
        }),
        value: "allUsers",
      },
      // { label: "Team Owner", value: "teamOwner" },
      // { label: "Team Admin", value: "teamAdmin" },
      {
        label: this.props.intl.formatMessage({
          id:
            "workspace-settings.user-management.add-members.filter-dropdown.admin.label",
          defaultMessage: "Admin",
        }),
        value: "workspaceAdmin",
      },
      {
        label: this.props.intl.formatMessage({
          id:
            "workspace-settings.user-management.add-members.filter-dropdown.member.label",
          defaultMessage: "Member",
        }),
        value: "workspaceMember",
      },
      {
        label: this.props.intl.formatMessage({
          id:
            "workspace-settings.user-management.add-members.filter-dropdown.limited-member.label",
          defaultMessage: "Limited Member",
        }),
        value: "workspaceLimitedMember",
      },
      {
        label: this.props.intl.formatMessage({
          id:
            "workspace-settings.user-management.add-members.filter-dropdown.disabled-user.label",
          defaultMessage: "Disabled User",
        }),
        value: "disabledUser",
      },
      {
        label: this.props.intl.formatMessage({
          id:
            "workspace-settings.user-management.add-members.filter-dropdown.enable-user.label",
          defaultMessage: "Enabled User",
        }),
        value: "enabledUser",
      },
    ];
    let data = dd.map((item, index) => {
      return {
        label: item.label,
        value: item.value,
      };
    });
    return data;
  };
  removeWorkspaceMember = () => {
    const { RemoveWorkspaceMembers } = this.props;
    const { userId } = this.state;
    this.setState({ deleteMemberBtnQuery: "progress" });
    RemoveWorkspaceMembers(
      userId,
      () => {
        this.setState({ deleteMemberBtnQuery: "", confirmDelete: false });
      },
      () => {}
    );
  };
  getPermission = (key) => {
    /** function calling for getting permission */
    const { workspacePermissionsState, profileState } = this.props;
    let currentUser = profileState.data.teamMember.find((m) => {
      /** finding current user from team members array */
      return m.userId == profileState.data.userId;
    });
    return GetPermission.canDo(
      currentUser.isOwner,
      workspacePermissionsState.data.workSpace,
      key
    ); /** passing current user owner , workspace permission and a key to check */
  };
  selectedValue = (permission, x) => {
    const workspaceRoles =
      this.props.permissionSettingsState.data.defaultRoles || [];
    if (permission) {
      return this.state[x.userId]
        ? this.state[x.userId]
        : generateWorkspaceAdminRoleData().find((r) => {
            return r.value.replace(" ", "") == x.roleName.replace(" ", "");
          });
    } else {
      return this.state[x.userId]
        ? this.state[x.userId]
        : generateWorkspaceRoleData(workspaceRoles).find((r) => {
            return r.value.replace(" ", "") == x.roleName.replace(" ", "");
          });
    }
  };
  render() {
    const {
      EmailAddressErrorText,
      EmailAddressError,
      emailList,
      workspaceMember,
      workspaceErrorText,
      workspaceError,
      confirmDelete,
      loggedInTeam,
      disableButton,
      confirmEnable,
      confirmDisable,
      confirminvite,
      confirmSelectedRole,
      changedRoleValue,
      role,
      workspaceName,
      btnQuery,
      disable,
      resendInviteEmailBtnQuery,
      deleteMemberBtnQuery,
      changeRoleBtnQuery,
      enableMemberBtnQuery,
      disableMemberBtnQuery,
      emailListValues,
      errorInvitationMessage,
      errorInvitation,
      userFilter,
    } = this.state;
    const {
      classes,
      theme,
      profileState,
      workspacePermissionsState,
      permissionSettingsState,
      workSpacePer,
    } = this.props;
    const workspaceRoles = this.props.permissionSettingsState.data.defaultRoles;

    const { getPermission } = this;

    let sortedWorkspaceMembers = this.sortingMembersByRole(
      profileState.data.member.allMembers,
      userFilter.value
    );
    if (workspaceMember && workspaceMember != "") {
      sortedWorkspaceMembers = sortedWorkspaceMembers.filter(
        (item) =>
          item.fullName.toLowerCase().includes(workspaceMember.toLowerCase()) ||
          item.email.toLowerCase().includes(workspaceMember.toLowerCase())
      );
    }

    const permissions = workspacePermissionsState.data
      ? workspacePermissionsState.data.workSpace
      : "";
    let roleName = "";
    if (role.roleId) {
      roleName =
        permissionSettingsState.data &&
        permissionSettingsState.data.defaultRoles &&
        permissionSettingsState.data.defaultRoles.length
          ? permissionSettingsState.data.defaultRoles
              .filter((x) => x.roleId !== "003")
              .find((x) => {
                return role.roleId === x.roleId;
              })
          : {};
    }
    let currentUser = profileState.data.teamMember.find((m) => {
      return m.userId == profileState.data.userId;
    });
    let currentWorkspaceUser = profileState.data.member.allMembers.find((m) => {
      return m.userId == profileState.data.userId;
    });
    return (
      <div className={classes.container}>
        {workspaceRoles && (
          <div className={classes.manageMembersCnt}>
            {workSpacePer.workspaceSetting.usersManagement.addMembers
              .cando /** getting permission if the current user have access of inviting members regarding its role */ && (
              <div className={classes.inviteTeamCnt}>
                <Typography variant="h4" className={classes.inviteHeadingText}>
                  <FormattedMessage
                    id="workspace-settings.user-management.add-members.title"
                    defaultMessage={`Add Team Members to ${workspaceName}`}
                    // values={{ workspaceName: workspaceName }}
                  />
                  {` ${workspaceName}`}
                </Typography>
                <div className={classes.inviteDropdownsCnt}>
                  <SelectSearchDropdown
                    data={this.generateEmailsData}
                    label=""
                    styles={{ margin: 0 }}
                    selectChange={this.handleCreateOptionsSelect}
                    type="assignees"
                    selectedValue={emailListValues}
                    placeholder={
                      <FormattedMessage
                        id="workspace-settings.user-management.add-members.placeholder"
                        defaultMessage="Enter name or email to select member"
                      />
                    }
                    avatar={true}
                  />
                  {errorInvitation == false ? null : (
                    <FormHelperText classes={{ root: classes.errorText }}>
                      {errorInvitationMessage}
                    </FormHelperText>
                  )}
                </div>
                <div className={classes.sendInviteBtnCnt}>
                  <NotificationMessage
                    type="NewInfo"
                    iconType="info"
                    style={{
                      width: "calc(100% - 176px)",
                      padding: "10px 15px 10px 15px",
                      backgroundColor: theme.palette.background.light,
                      borderRadius: 4,
                    }}
                  >
                    <FormattedMessage
                      id="workspace-settings.user-management.add-members.message"
                      defaultMessage="Adding a workspace member will give access to all projects within this workspace."
                    />
                  </NotificationMessage>
                  <div style={{ marginLeft: 20 }}>
                    <CustomButton
                      onClick={this.handleInviteSend}
                      btnType="success"
                      variant="contained"
                      query={btnQuery}
                      disabled={btnQuery === "progress" || disable || emailListValues.length == 0}
                    >
                      <FormattedMessage
                        id="workspace-settings.user-management.add-members.button.title"
                        defaultMessage="Add To WorkSpace"
                      />
                    </CustomButton>
                  </div>
                </div>
              </div>
            )}
            <div className={classes.workspaceMembersCnt}>
              <div className={classes.userListCnt}>
                <div style={{ display: "flex" }}>
                  <DefaultTextField
                    fullWidth={true}
                    labelProps={{
                      shrink: true,
                    }}
                    errorState={workspaceError}
                    errorMessage={workspaceErrorText}
                    defaultProps={{
                      id: "workspaceMember",
                      onChange: this.handleInput("workspaceMember"),
                      value: workspaceMember,
                      placeholder: this.props.intl.formatMessage({
                        id: "common.search.label",
                        defaultMessage: "Search",
                      }),
                    }}
                    formControlStyles={{ width: "70%", padding: "0px" }}
                  />
                  <div style={{ width: 170, marginLeft: 20 }}>
                    <SelectSearchDropdown
                      data={() => {
                        return this.generateFilterDropDown();
                      }}
                      label=""
                      selectChange={this.handleOptionsSelect}
                      type="userFilter"
                      styles={{ margin: 0 }}
                      selectedValue={this.generateFilterDropDown().find((s) => {
                        return s.value == userFilter.value;
                      })}
                      placeholder=""
                      icon={false}
                      isMulti={false}
                    />
                  </div>
                </div>
                {/* <Scrollbars autoHide={false} autoHeight={true}> */}
                <ul className={classes.userList}>
                  {sortedWorkspaceMembers
                    .filter((m) => !m.isDeleted)
                    .map((x) => {
                      let m = profileState.data.teamMember.find((obj) => {
                        return obj.userId == x.userId;
                      });
                      return (
                        <li>
                          <div className="flex_center_start_row">
                            <CustomAvatar
                              styles={{ marginRight: 10 }}
                              otherMember={{
                                imageUrl: x.imageUrl,
                                fullName: x.fullName,
                                lastName: "",
                                email: x.email,
                                isOnline: x.isOnline,
                                isOwner:
                                  x.roleId == "000"
                                    ? true
                                    : false /** 000 role id is for owner */,
                              }}
                              size="small"
                            />
                            <div>
                              <Typography variant="h6">
                                <span
                                  className= {classes.fontSize}
                                  style={{
                                    fontSize: "13px",
                                    fontWeight:
                                      theme.typography.fontWeightMedium,
                                  }}
                                >
                                  {x.fullName}{" "}
                                </span>
                                {x.isActive === false ? (
                                  <span className={classes.disabledUserText}>
                                    · User Disabled
                                  </span>
                                ) : x.isActive == null ? (
                                  <span className={classes.InvitedUserText}>
                                    · Invited
                                  </span>
                                ) : null}
                              </Typography>
                              <Typography variant="caption">
                                {x.email}
                              </Typography>
                            </div>
                          </div>
                          <div className="flex_center_start_row">
                            {x.isActive || x.isActive === null ? (
                              <div className="flex_center_start_row">
                                {/* isActive null means user is invited */}
                                {(x.isCustomRole &&
                                  x.userId == currentUser.userId &&
                                  currentUser.role !== "Owner" &&
                                  currentUser.role !== "Admin") ||
                                x.roleId == "000" ||
                                x.roleId == "006" ? null : x.roleId == "002" &&
                                  currentUser.role !== "Owner" &&
                                  currentUser.role !== "Admin" ? null : (
                                  <SelectSearchDropdown
                                    data={() =>
                                      this.generateDropDownData(false)
                                    }
                                    styles={{
                                      margin: 0,
                                      marginRight: 0,
                                      flex: 1,
                                      width: 150,
                                    }}
                                    isClearable={false}
                                    label=""
                                    selectChange={this.handleSelectedRole}
                                    type={x.userId}
                                    selectedValue={this.selectedValue(false, x)}
                                    placeholder={"Role"}
                                    isMulti={false}
                                    isDisabled={
                                      !workSpacePer.workspaceSetting
                                        .usersManagement.updateRole.cando
                                    } /** ! is for reversing the permission value because permission true means user can do this action, but isDisabled true means disable the component */
                                  />
                                )}
                              </div>
                            ) : null}
                            {x.isActive === false &&
                              workSpacePer.workspaceSetting.usersManagement
                                .enableMembers.cando && (
                                <span
                                  className={classes.EnabledUserText}
                                  onClick={() => this.enableMember(x.userId)}
                                >
                                  <FormattedMessage
                                    id="workspace-settings.user-management.add-members.user-enable.label"
                                    defaultMessage="Enable User"
                                  />
                                </span>
                              )}
                            {x.roleId == "000" /** team owner */ && (
                              <div className="flex_center_start_row">
                                <Typography variant="h6">
                                  <FormattedMessage
                                    id="workspace-settings.user-management.add-members.filter-dropdown.team-owner.label"
                                    defaultMessage="Team Owner"
                                  />
                                </Typography>
                              </div>
                            )}
                            {x.roleId == "006" /** team admin */ && (
                              <div className="flex_center_start_row">
                                <Typography variant="h6">
                                  <FormattedMessage
                                    id="workspace-settings.user-management.add-members.filter-dropdown.team-admin.label"
                                    defaultMessage="Team Admin"
                                  />
                                </Typography>
                              </div>
                            )}
                            {x.roleId == "002" &&
                            currentUser.role !== "Owner" &&
                            currentUser.role !==
                              "Admin" /**workspace admin*/ ? (
                              <div className="flex_center_start_row">
                                <Typography variant="h6">
                                  <FormattedMessage
                                    id="workspace-settings.user-management.add-members.filter-dropdown.admin.label"
                                    defaultMessage="Admin"
                                  />
                                </Typography>
                              </div>
                            ) : null}
                            {x.isCustomRole &&
                            x.userId == currentUser.userId &&
                            currentUser.role !== "Owner" &&
                            currentUser.role !== "Admin" ? (
                              <div className="flex_center_start_row">
                                <Typography variant="h6">
                                  {x.roleName.charAt(0).toUpperCase() +
                                    x.roleName.slice(1)}
                                </Typography>
                              </div>
                            ) : null}

                            {(x.isCustomRole &&
                              x.userId == currentUser.userId &&
                              currentUser.role !== "Owner" &&
                              currentUser.role !== "Admin") ||
                            x.roleId == "000" ||
                            x.roleId == "006" ||
                            (x.roleId == "002" &&
                              currentUser.role !== "Owner" &&
                              currentUser.role !== "Admin") ? null : (
                              <MoreActionDropdown
                                isActive={x.isActive}
                                enableMember={this.enableMember.bind(
                                  this,
                                  x.userId
                                )}
                                deleteMember={this.deleteMember.bind(
                                  this,
                                  x.userId
                                )}
                                removeMembers={this.disableMember.bind(
                                  this,
                                  x.userId
                                )}
                                workSpacePer={workSpacePer}
                              />
                            )}
                          </div>
                        </li>
                      );
                    })}
                </ul>
                    {/* </Scrollbars> */}
              </div>
            </div>
          </div>
        )}
        <DeleteConfirmDialog
          open={confirmDelete}
          closeAction={this.closeDeleteMember}
          cancelBtnText={
            <FormattedMessage
              id="common.action.cancel.label"
              defaultMessage="Cancel"
            />
          }
          successBtnText={<FormattedMessage id="workspace-settings.confiramtion.remove-member.title" defaultMessage="Remove Member"/>}
          alignment="center"
          headingText={<FormattedMessage id="workspace-settings.confiramtion.remove-member.title" defaultMessage="Remove Member"/>}
          successAction={this.removeWorkspaceMember}
          msgText={<FormattedMessage id="workspace-settings.confiramtion.remove-member.label" defaultMessage="Are you sure you want to remove the selected user?"/>}
          btnQuery={deleteMemberBtnQuery}
        />
        <ActionConfirmation
          open={confirmEnable}
          closeAction={this.closeEnableMember}
          cancelBtnText={
            <FormattedMessage
              id="common.action.cancel.label"
              defaultMessage="Cancel"
            />
          }
          successBtnText={
            <FormattedMessage
              id="team-settings.user-management.confirmation.enable-user.enable-user-button.label"
              defaultMessage="Enable User"
            />
          }
          alignment="center"
          headingText={
            <FormattedMessage
              id="team-settings.user-management.confirmation.enable-user.title"
              defaultMessage="Enable"
            />
          }
          iconType="archive"
          msgText={
            <FormattedMessage
              id="team-settings.user-management.confirmation.enable-user.label"
              defaultMessage="Are you sure you want to enable the selected user?"
            />
          }
          successAction={this.enableMemberConfrimation}
          btnQuery={enableMemberBtnQuery}
        />
        <ActionConfirmation
          open={confirmDisable}
          closeAction={this.closeDisableMember}
          cancelBtnText={
            <FormattedMessage
              id="common.action.cancel.label"
              defaultMessage="Cancel"
            />
          }
          successBtnText={
            <FormattedMessage
              id="team-settings.user-management.disable-user-button.label"
              defaultMessage="Disable User"
            />
          }
          alignment="center"
          headingText={
            <FormattedMessage
              id="common.action.disable.label"
              defaultMessage={`Disable`}
            />
          }
          iconType="archive"
          msgText={
            <FormattedMessage
              id="team-settings.user-management.disable-user-button.action"
              defaultMessage={`Are you sure you want to disable the selected user?`}
            />
          }
          successAction={this.disableMemberConfrimation}
          btnQuery={disableMemberBtnQuery}
        />
        <ActionConfirmation
          open={confirminvite}
          closeAction={this.closeinviteMember}
          cancelBtnText={
            <FormattedMessage
              id="common.action.cancel.label"
              defaultMessage="Cancel"
            />
          }
          successBtnText={
            <FormattedMessage
              id="team-settings.user-management.confirmation.resend-invite.resend-invite-button.label"
              defaultMessage="Resend Invite"
            />
          }
          alignment="center"
          headingText={
            <FormattedMessage
              id="team-settings.user-management.confirmation.resend-invite.title"
              defaultMessage="Resend Invite"
            />
          }
          iconType="archive"
          msgText={
            <FormattedMessage
              id="team-settings.user-management.confirmation.resend-invite.label"
              defaultMessage="Are you sure you want to resend invite to the selected user?"
            />
          }
          successAction={this.inviteMemberConfrimation}
          btnQuery={resendInviteEmailBtnQuery}
        />
        <ActionConfirmation
          open={confirmSelectedRole}
          closeAction={this.closeSelectedRolePopup}
          cancelBtnText={
            <FormattedMessage
              id="common.action.cancel.label"
              defaultMessage="Cancel"
            />
          }
          successBtnText={<FormattedMessage id="common.action.yes.label" defaultMessage="Yes"/>}
          alignment="center"
          headingText={<FormattedMessage id="workspace-settings.confiramtion.update-role.title" defaultMessage="Update Role"/>}
          iconType="archive"
          msgText={<FormattedMessage id="workspace-settings.confiramtion.update-role.label" values={{t1: roleName ? roleName.roleName : "", t2: changedRoleValue.toLowerCase()}}
          defaultMessage={`Are you sure you want to change selected member’s role from ${
            roleName ? roleName.roleName : ""
          } to ${changedRoleValue.toLowerCase()}?`} />}
          successAction={this.updateSelectedRole}
          btnQuery={changeRoleBtnQuery}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profileState: state.profile,
    permissionSettingsState: state.permissionSettings,
    workspacePermissionsState: state.workspacePermissions,
    members: state.profile.data ? state.profile.data.member.allMembers : [],
    workSpacePer: state.workspacePermissions.data.workSpace,
  };
};

export default compose(
  withRouter,
  injectIntl,
  connect(mapStateToProps, {
    InviteTeamMembers,
    RemoveMembers,
    EnableDisableMembers,
    RemoveWorkspaceMembers,
    ResendInviteTeamMembers,
    ChangeUserRole,
    AddMembersToWorkSpace,
  }),
  withStyles(workspaceSetting, { withTheme: true })
)(ManageMembers);
