import React, { Component, Fragment } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { compose } from "redux";
import DeleteConfirmation from "../../components/Dialog/Popups/DeleteConfirmation";
import DefaultTextField from "../../components/Form/TextField";
import DefaultButton from "../../components/Buttons/DefaultButton";
import Grid from "@material-ui/core/Grid";
import DeleteIcon from "@material-ui/icons/Cancel";
import DefaultSwitch from "../../components/Form/Switch";
import Typography from "@material-ui/core/Typography";
import CustomButton from "../../components/Buttons/CustomButton";
import Avatar from "@material-ui/core/Avatar";
import CustomAvatar from "../../components/Avatar/Avatar";
import { validateTitleField } from "../../utils/formValidations";
import getErrorMessages from "../../utils/constants/errorMessages";
import {
  UpdateWorkSpaceImage,
  UpdateWorkSpaceData,
  RemoveWorkspaceLogo,
  DeleteWorkSpaceData,
  FetchWorkspaceInfo,
} from "../../redux/actions/workspace";
import DefaultDialog from "../../components/Dialog/Dialog";
import { FetchUserInfo } from "../../redux/actions/profile";
import Hotkeys from "react-hot-keys";
import { PopulateTempData } from "../../redux/actions/tempdata";
import EditIcon from "@material-ui/icons/Edit";
import AddWorkspaceForm from "../AddNewForms/editWorkspaceForm";
import DeleteWorkspaceDialogContent from "./DeleteWorkspaceDialogContent";
import DeleteConfirmDialog from "../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import { GetPermission } from "../../components/permissions";
import { injectIntl, FormattedMessage } from "react-intl";

const hideBtn = {
  display: "none",
};

class GeneralSetting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inviteChecked: false,
      workspaceName: "",
      imageUploadFlag: false,
      pictureUrlFull: "",
      imageUrl: "",
      imageBasePath: "",
      fileName: "",
      workspaceNameError: "",
      workspaceNameErrorMessage: "",
      loggedInTeam: "",
      teamId: "",
      pictureThumbnail_40X40: "",
      pictureThumbnail_70X70: "",
      isDelete: false,
      confirmDeleteWorkspace: false,
      isEdit: false,
      btnQuery: "",
      nameEdit: false,
      step: 0,
      openDialog: false,
      deleteWorkspaceDialogOpen: false,
      workspaceNameVerifyErrorMessage: "",
      workspaceNameVerifyError: false,
      workspaceNameVerify: "",
      workspaceNameErrorState: false,
    };
    this.handleInput = this.handleInput.bind(this);
    this.handleInviteSwitch = this.handleInviteSwitch.bind(this);
    this.clickFile = this.clickFile.bind(this);
    this._handleImageChange = this._handleImageChange.bind(this);
    this.removeProfilePicture = this.removeProfilePicture.bind(this);
    this.handleSaveChanges = this.handleSaveChanges.bind(this);
    this.resetChanges = this.resetChanges.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
  }
  handleEdit() {
    this.setState({ isEdit: true });
  }
  //delete workspace confirmation dialog close
  handleDeleteWorkspaceDialogClose = () => {
    this.setState({ deleteWorkspaceDialogOpen: false });
  };
  //delete workspace confirmation dialog open
  deleteWorkspaceDialogOpen = () => {
    this.setState({ deleteWorkspaceDialogOpen: true });
  };
  //Handle Verify team name input for delete team dialog
  handleWorkspaceNameInput = (event) => {
    this.setState({
      workspaceNameVerify: event.target.value,
      workspaceNameVerifyError: false,
      workspaceNameVerifyErrorMessage: "",
    });
  };

  validWorkspaceDeleteInput = (key, value = "") => {
    const { loggedInTeam, workspaceNameVerify } = this.state;
    const { workspace } = this.props;

    let activeWorkspace = workspace.find((w) => {
      /** finding active workspace by matching logged in team team ID with workspace id's */
      return w.teamId == loggedInTeam.teamId;
    });

    let validationObj = value == "" ? false : true;
    if (!validationObj) {
      switch (key) {
        case "workspaceName":
          this.setState({
            workspaceNameVerifyError: true,
            workspaceNameVerifyErrorMessage: this.props.intl.formatMessage({
              id:
                "workspace-settings.general-information.edit-modal.form.validation.worksapce-name.empty",
              defaultMessage: "Oops! Please enter workspace name.",
            }),
          });
          break;

        default:
          break;
      }
    } else if (
      key == "workspaceName" &&
      activeWorkspace.teamName.replace(/\s\s+/g, " ") !== workspaceNameVerify
    ) {
      this.setState({
        workspaceNameVerifyError: true,
        workspaceNameVerifyErrorMessage: this.props.intl.formatMessage({
          id:
            "workspace-settings.general-information.edit-modal.form.validation.worksapce-name.doesntMatch",
          defaultMessage: "Oops! Workspace name does not match.",
        }),
      });
    } else return validationObj;
  };
  validInput = (value = "") => {
    let validationObj = value == "" ? false : true;
    if (!validationObj)
      this.setState({
        workspaceNameError: true,
        workspaceNameErrorMessage: this.props.intl.formatMessage({
          id:
            "workspace-settings.general-information.edit-modal.form.validation.worksapce-name.empty",
          defaultMessage: "Oops! Please enter workspace name.",
        }),
      });
    return validationObj;
  };

  handleSaveChanges() {
    const { workspaceName, loggedInTeam } = this.state;
    let workspaceNameValue = workspaceName.trim();
    let workspaceNameValidate = this.validInput(workspaceNameValue);

    if (workspaceNameValidate) {
      let newTeamObj = { ...loggedInTeam, teamName: workspaceName };
      this.props.UpdateWorkSpaceData(
        newTeamObj,
        (response) => {
          this.setState({
            isEdit: false,
            imageUploadFlag: false,
            btnQuery: "",
          });
        },
        (error) => {
          this.setState({
            workspaceNameError: true,
            workspaceNameErrorMessage: error.data.message,
            imageUploadFlag: false,
            btnQuery: "",
          });
          //  self.showSnackBar('Server throws error','error');
        }
      );
    }
  }

  resetChanges() {
    this.setState({
      inviteChecked: false,
      workspaceName: this.state.loggedInTeam.teamName,
      imageUploadFlag: false,
      pictureUrlFull: "",
      imageUrl: "",
      workspaceNameError: "",
      workspaceNameErrorMessage: "",
      isEdit: false,
    });
  }
  componentDidUpdate(prevProps, prevState) {
    const { profileState } = this.props;
    const loggedInTeam =
      profileState.data && profileState.data.workspace
        ? profileState.data.workspace.find(
            (x) => x.teamId === profileState.data.loggedInTeam
          )
        : {};
    if (this.state.teamId !== this.props.profileState.data.loggedInTeam) {
      if (loggedInTeam) {
        this.setState({
          teamId: this.props.profileState.data.loggedInTeam,
          workspaceName: loggedInTeam.teamName,
          loggedInTeam,
          pictureThumbnail_40X40: loggedInTeam.pictureThumbnail_40X40,
          pictureThumbnail_70X70: loggedInTeam.pictureThumbnail_70X70,
          isEdit: false,
        });
      }
    }
  }

  componentDidMount() {
    const { profileState } = this.props;
    const loggedInTeam =
      profileState.data && profileState.data.workspace
        ? profileState.data.workspace.find(
            (x) => x.teamId === profileState.data.loggedInTeam
          )
        : {};
    if (loggedInTeam) {
      this.setState({
        teamId: this.props.profileState.data.loggedInTeam,
        workspaceName: loggedInTeam.teamName,
        loggedInTeam,
        pictureThumbnail_40X40: loggedInTeam.pictureThumbnail_40X40,
        pictureThumbnail_70X70: loggedInTeam.pictureThumbnail_70X70,
        isEdit: false,
      });
    }
  }
  clickFile() {
    let fileinputbtn = document.getElementById("workspaceImageUpload");
    fileinputbtn.click();
  }
  removeProfilePicture() {
    this.props.RemoveWorkspaceLogo(
      this.props.profileState.data.loggedInTeam,
      (response) => {
        this.setState({ isDelete: true });
      },
      (error) => {
        if (error.status === 500) {
          this.props.showSnackBar("Server throws error", "error");
        } else {
          this.props.showSnackBar(error.data.message, "error");
        }
      }
    );
  }
  _handleImageChange(e) {
    e.preventDefault();
    this.setState({
      imageUploadFlag: true,
      workspaceNameError: "",
      workspaceNameErrorMessage: "",
    });
    let reader = new FileReader();
    let file = e.target;
    let self = this;
    if (file.files.length > 0) {
      let type = file.files[0].name.toLowerCase().match(/[0-9a-z]{1,5}$/gm)[0];
      if (
        (type != null && type.toLowerCase() == "jpg") ||
        type.toLowerCase() == "jpeg" ||
        type.toLowerCase() == "png" ||
        type.toLowerCase() == "gif" ||
        type.toLowerCase() == "bmp"
      ) {
        reader.onloadend = () => {
          this.setState({
            file: file.files[0],
            imagePreviewUrl: reader.result,
          });
        };
        reader.readAsDataURL(file.files[0]);
        let data = {
          teamId: this.props.profileState.data.loggedInTeam,
          ["PrevImageName"]: this.state.imageUrl,
          ["UploadedImage"]: file.files[0],
        };
        self.props.UpdateWorkSpaceImage(
          data,
          (response) => {
            this.setState({
              imageUploadFlag: true,
              isDelete: false,
              pictureUrlFull: data,
              imageBasePath: response.data.imageBasePath,
              fileName: response.data.fileName,
              pictureThumbnail_70X70: response.data.pictureThumbnail_70X70,
              pictureThumbnail_40X40: response.data.pictureThumbnail_40X40,
            });
            this.props.showSnackBar(
              response.data.message
                ? response.data.message
                : "Workspace image is successfully uploaded",
              "success"
            );
          },
          (error) => {
            if (error.status === 500) {
              this.props.showSnackBar("Server throws error", "error");
            } else {
              this.props.showSnackBar(error.data.message, "error");
            }
          }
        );
      } else {
        this.setState({
          imageUploadFlag: false,
        });
        this.props.showSnackBar(
          getErrorMessages().INVALID_IMAGE_EXTENSION,
          "error"
        );
      }
    }
    this.setState({
      imageUploadFlag: false,
    });
  }

  handleInput = (name) => (event) => {
    this.setState({
      [name]: event.target.value,
      workspaceNameError: "",
      workspaceNameErrorMessage: "",
    });
  };
  handleInviteSwitch() {
    this.setState((prevState) => ({
      inviteChecked: !prevState.inviteChecked,
    }));
  }
  disableEdit = () => {
    this.setState({
      nameEdit: false,
      workspaceNameError: false,
      workspaceNameErrorMessage: "",
    });
  };

  handleEdit = () => {
    this.setState({ openDialog: true });
  };
  handleDialogClose = () => {
    this.setState({ openDialog: false });
  };
  handleDeleteWorkspaceStep = (value) => {
    // Function that change the step
    this.setState({ step: value });
  };

  handleDeleteWorkspaceDialog = () => {
    const { profileState } = this.props;
    const { workspaceNameVerify } = this.state;
    this.setState({ btnQuery: "progress" });
    let workspaceNameValidate = this.validWorkspaceDeleteInput(
      "workspaceName",
      workspaceNameVerify
    );
    if (workspaceNameValidate) {
      this.props.DeleteWorkSpaceData(
        this.state.loggedInTeam.teamId,
        //Success Callback
        () => {
          //User Info
          this.setState({ btnQuery: "" });
          this.handleDeleteWorkspaceDialogClose();
          this.props.FetchUserInfo((response) => {
            let activeTeam = response.data.teams.find((t) => {
              return t.companyId == response.data.activeTeam;
            });
            this.props.history.push(`/teams/${activeTeam.companyUrl}`);
          });
        },
        //Failure Callback
        (error) => {
          this.setState({ btnQuery: "" });
        }
      );
    } else {
      this.setState({ btnQuery: "" });
    }
  };
  getPermission = (key) => {
    /** function calling for getting permission */
    const { workspacePermissionsState, profileState } = this.props;
    let currentUser = profileState.data.teamMember.find((m) => {
      /** finding current user from team members array */
      return m.userId == profileState.data.userId;
    });
    return GetPermission.canEdit(
      currentUser.isOwner,
      workspacePermissionsState.data.workSpace,
      key
    ); /** passing current user owner , workspace permission and a key to check */
  };
  render() {
    const {
      workspaceName,
      workspaceNameError,
      workspaceNameErrorMessage,
      imageUploadFlag,
      inviteChecked,
      loggedInTeam,
      isDelete,
      imageBasePath,
      pictureThumbnail_40X40,
      pictureThumbnail_70X70,
      confirmDeleteWorkspace,
      isEdit,
      btnQuery,
      step,
      nameEdit,
      openDialog,
      workspaceNameVerifyErrorMessage,
      workspaceNameVerifyError,
      workspaceNameVerify,
      deleteWorkspaceDialogOpen,
      workspaceNameErrorState,
    } = this.state;
    const {
      classes,
      theme,
      permissions,
      profileState,
      workSpacePer,
    } = this.props;

    let activeWorkspace = profileState.data.workspace.find((w) => {
      return w.teamId == loggedInTeam.teamId;
    });
    let url = activeWorkspace ? activeWorkspace.teamUrl : null;
    let currentUser = profileState.data.teamMember.find((m) => {
      return m.userId == profileState.data.userId;
    });
    return (
      <div className={classes.personalInfoCnt}>
        <Grid container direction="row" justify="flex-start">
          <div className={classes.profilePicCnt}>
            {(loggedInTeam.pictureThumbnail_40X40 || imageUploadFlag) &&
            !isDelete ? (
              <div className={classes.profilePic}>
                <img
                  src={
                    imageUploadFlag
                      ? imageBasePath + pictureThumbnail_70X70
                      : loggedInTeam.imageBasePath +
                        loggedInTeam.pictureThumbnail_70X70
                  }
                />
                {workSpacePer.workspaceSetting.generalInformation.workspaceImage
                  .isAllowDelete && (
                  <DeleteIcon
                    onClick={this.removeProfilePicture}
                    htmlColor={theme.palette.error.main}
                    classes={{ root: classes.deleteProfileImageIcon }}
                  />
                )}
              </div>
            ) : loggedInTeam.teamName ? (
              <CustomAvatar
                styles={{ marginRight: 10 }}
                loggedInTeam
                size="large"
              />
            ) : loggedInTeam.teamName ? (
              <CustomAvatar
                styles={{ marginRight: 10 }}
                loggedInTeam
                size="large"
              />
            ) : (
              "U"
            )}
            {workSpacePer.workspaceSetting.generalInformation.workspaceImage
              .isAllowAdd && (
              <p className={classes.uploadPhotoBtn} onClick={this.clickFile}>
                <FormattedMessage
                id="profile-settings-dialog.upload-photo.label"
                defaultMessage="Upload Photo"
              ></FormattedMessage>
                {/* {translate("Upload Photo")} */}
              </p>
            )}
            <input
              style={hideBtn}
              className="fileInput"
              type="file"
              id="workspaceImageUpload"
              onChange={this._handleImageChange}
              accept={"png, jpeg, jpg, gif, PNG, JPG, JPEG, GIF, bmp, BMP"}
            />
          </div>
          {!nameEdit ? (
            <div>
              <Typography variant="h2">
                {activeWorkspace ? activeWorkspace.teamName : null}
                {workSpacePer.workspaceSetting.generalInformation
                  .isWorkSpaceName.isAllowEdit ||
                workSpacePer.workspaceSetting.generalInformation.isWorkspaceURL
                  .isAllowEdit ? (
                  <EditIcon
                    htmlColor={theme.palette.secondary.dark}
                    className={classes.editIcon}
                    onClick={
                      nameEdit ? this.handleSaveChanges : this.handleEdit
                    }
                  />
                ) : null}
              </Typography>
              <Typography variant="body2">
                {window.location.origin + "/" + url}
              </Typography>
              {workSpacePer.workspaceSetting.generalInformation
                .isDeleteWorkSpace.cando && (
                <CustomButton
                  btnType="gray"
                  variant="contained"
                  style={{ marginTop: 10 }}
                  onClick={this.deleteWorkspaceDialogOpen}
                >
                  <FormattedMessage
                    id="workspace-settings.general-information.delete-workspace.label"
                    defaultMessage="Delete Workspace"
                  />
                </CustomButton>
              )}
            </div>
          ) : (
            <>
              <DefaultTextField
                label=""
                fullWidth={true}
                errorState={workspaceNameError}
                errorMessage={workspaceNameErrorMessage}
                formControlStyles={{ width: 350 }}
                defaultProps={{
                  id: "workspaceNameInput",
                  onChange: this.handleInput("workspaceName"),
                  value: workspaceName,
                  inputProps: { maxLength: 80 },
                  autoFocus: true,
                  onBlur: this.disableEdit,
                }}
              />
              <Hotkeys keyName="enter,esc" onKeyDown={this.handleSaveChanges} />
            </>
          )}
        </Grid>
        <DeleteConfirmation
          open={confirmDeleteWorkspace}
          theme={theme}
          view="Workspace"
          message={
            <FormattedMessage
              id="workspace-settings.general-information.delete-workspace.delete-modal.form.delete-workspace-sentence.sentence"
              defaultMessage="Are you sure you want to permanently delete this workspace?"
            />
          }
          closeAction={this.deleteWorkspaceCloseConfirmation}
          deleteWorkspace={this.deleteWorkspace}
          buttonName={
            <FormattedMessage
              id="workspace-settings.general-information.delete-workspace.label"
              defaultMessage="Delete Workspace"
            />
          }
          loggedInTeam={loggedInTeam}
        />
        <DeleteConfirmDialog
          open={deleteWorkspaceDialogOpen}
          closeAction={this.handleDeleteWorkspaceDialogClose}
          cancelBtnText={
            step == 0 ? (
              <FormattedMessage
                id="common.action.delete.confirmation.cancel-button.label"
                defaultMessage="Cancel"
              />
            ) : (
              <FormattedMessage
                id="workspace-settings.general-information.delete-workspace.delete-modal.form.changed-mind.label"
                defaultMessage="No, I've changed my mind"
              />
            )
          }
          successBtnText={
            step == 0 ? (
              <FormattedMessage
                id="common.action.delete.confirmation.delete-button.label"
                defaultMessage="Delete"
              />
            ) : (
              <FormattedMessage
                id="workspace-settings.general-information.delete-workspace.delete-modal.form.delete-workspace.label"
                defaultMessage="Yes, Delete Workspace"
              />
            )
          }
          alignment="center"
          headingText={
            <FormattedMessage
              id="common.action.delete.confirmation.delete-button.label"
              defaultMessage="Delete"
            />
          }
          successAction={
            step == 0
              ? () => {
                  this.handleDeleteWorkspaceStep(1);
                }
              : this.handleDeleteWorkspaceDialog
          }
          btnQuery={btnQuery}
        >
          <DeleteWorkspaceDialogContent
            step={step}
            workspaceNameErrorMessage={workspaceNameVerifyErrorMessage}
            workspaceNameError={workspaceNameVerifyError}
            handleWorkspaceNameInput={this.handleWorkspaceNameInput}
            workspaceName={workspaceNameVerify}
            activeWorkspace={activeWorkspace}
          />
        </DeleteConfirmDialog>

        <DefaultDialog
          title={
            <FormattedMessage
              id="workspace-settings.general-information.edit-modal.form.label"
              defaultMessage="Edit Workspace"
            />
          }
          sucessBtnText="Save Changes"
          open={openDialog}
          onClose={this.handleDialogClose}
        >
          <AddWorkspaceForm
            closeDialog={this.handleDialogClose}
            activeWorkspace={activeWorkspace}
            editNamePer={
              workSpacePer.workspaceSetting.generalInformation.isWorkSpaceName
                .isAllowEdit
            }
            editUrlPer={
              workSpacePer.workspaceSetting.generalInformation.isWorkspaceURL
                .isAllowEdit
            }
          />
        </DefaultDialog>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profileState: state.profile,
    workspacePermissionsState: state.workspacePermissions,
    workspace: state.profile.data.workspace,
    workSpacePer: state.workspacePermissions.data.workSpace,
  };
};

export default compose(
  withRouter,
  injectIntl,
  connect(mapStateToProps, {
    UpdateWorkSpaceImage,
    UpdateWorkSpaceData,
    RemoveWorkspaceLogo,
    DeleteWorkSpaceData,
    PopulateTempData,
    FetchUserInfo,
    FetchWorkspaceInfo,
  })
)(GeneralSetting);
