const Style = (theme) => ({
  container: {
    padding: "1rem",
  },
  autoHeader: {
    justifyContent: "space-between",
    display: "flex",
    alignItems: "center",
  },
  tablehead: {
    fontSize: 13,
    color: "#646464",
    fontFamily: theme.typography.fontFamilyLato,
  },
  createdby: {
    color: "#969696",
    marginTop: 5,
  },
  Custommodel: {
    marginTop: "5px",
    backgroundColor: "white",
    zIndex: 1111111111,
    position: "absolute",
    boxShadow: "0 0 1px 1px rgba(0,0,0,0.1)",
    borderRadius: "5px",
    padding: "20px",
    width: "320px",
  },

  modelFooter: {
    borderTop: "1px solid #e0dcdc",
    width: "100%",
    display: "flex",
    justifyContent: "end",
    backgroundColor: "#F6F6F6",
    padding: "20px",
  },

  outerSuccess: {
    backgroundColor: "#EAF9FF",
    borderRadius: "5px",
  },
  modelIcon: {
    padding: "10px",
    marginRight: "10px",
    alignItems: "center",
    backgroundColor: "#E6F7FD",
    height: "50px",
    width: "50px",
    borderRadius: "50%",
    justifyContent: "center",
  },

  history: {
    marginTop: "10px",
    marginBottom: "10px",
  },


  failedCounter: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
  },
  createnewrule: {
    display: "flex",
    alignItems: "center",
  },
  iconruletex: {
    display: "flex",
    alignItems: "center"
  },
  rultexheading: {
    fontSize: 14,
    color: "black",
    marginLeft: 10
  },

  ruletypeicon: {
    marginRight: 15,
    backgroundColor: "#E6F7FD",
    borderRadius: "5px",
    width: 40,
    height: 40,
    display: "flex",
    justifyContent: "center",
    flex: '0 0 40px',
    alignItems: "center",
  },
  namecolumn: {
    display: "flex",
    alignItems: "center",
    cursor: "pointer"

  },
  avatarcolumn: {
    display: "flex",
    alignItems: "center"
  },
  triggertypebeforeicon: {
    display: "flex",
    flexWrap: "wrap",
    alignItems: "center",
    fontFamily: theme.typography.fontFamilyLato,
    fontSize: 14,
    "& svg": {
      transform: 'rotate(90deg)',
      margin: '0px 10px',
    }
  },
  confirmdellicon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    padding: 22,
  },
  confirmdellicontext: {
    color: "#161717",
    display: "flex",
    justifyContent: "center",
    marginBottom: 27,
    fontSize: 15,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: "bold"
  },
  custombtnmodel: {
    display: "flex",
    alignItems: "center",
    justifyContent: "end",
    backgroundColor: "#F6F6F6",
    padding: 20,
  },
  actionhistoryheader: {
    display: "flex",
    alignItems: "center",
    backgroundColor: "#E6F7FD",
    padding: "10px",
    borderRadius: "5px",
  },
  actionhistoryheading: {
    display: "flex",
    fontSize: "11px",
    alignItems: "center",
    fontWeight: 600,
    color: "black"
  },
  actionhistorybody: {
    display: "flex",
    width: "100%",
    marginTop: "15px",
    marginBottom: "15px",
  },
  actionhistoryfiger: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    borderRight: "1px solid black",
    width: "100%",
  },
  actiontext: {
    fontSize: 13
  },
  seccessbtn: {
    backgroundColor: "#D9F1E8",
    borderRadius: "5px"
  },
  Filebtn: {
    backgroundColor: "#F6E2E2",
    borderRadius: "5px"
  },
  fieldbtntext: {
    padding: "6px",
    color: "#C33D3D",
    fontSize: 12
  },
  historyField: {
    marginTop: "8px",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    position: "relative"
  },
  tableCell: {
    padding: '4px 56px 4px 24px',
  },
  seccessbtntex: {
    padding: "6px",
    color: "#19AC75",
    fontSize: 12,
  },
  actioncounterfiggertext: {
    fontWeight: "bold",
    fontSize: "14px"
  },
  progress: {
    margin: theme.spacing.unit * 2,
  },
  prgressmodel: {

    justifyContent: "space-between",
    display: "flex",
    alignItems: "center",
    marginBottom: "10px",
  },
  actionusedtext: {
    color: "black",
    fontSize: 15,
    fontWeight: "bold"
  },
  monthActiontext: {
    fontSize: 13,
    color: "black"
  },
  restOnText: {
    color: "black",
    fontSize: 14
  },
  restOnDate: {
    fontWeight: "bold",
    fontSize: 14,
    color: "black"
  },
  discriptionprogressmodel: {
    fontSize: 13,
    color: "#7E7E7E"
  },
  AHbgLine: {

    backgroundColor: "#BFBFBF",
    width: 2,
    position: "absolute",
    top: 45,
    bottom: 18,
    marginLeft: 18,
  },
  openModel: {
    color: "#00ABED",
    cursor: "pointer"
  },
  emptyBody: {
    display: "flex",
    justifyContent: "center",
    textAlign: "center",
    maxWidth: 390,
    margin: "auto",
    height: "100%",
  },
  AutomationEmptyText: {
    margin: "15px 0 10px 0",
    fontFamily: theme.typography.fontFamilyLato,
    fontSize: 18,
    color: "#161717",
    fontWeight: "bold"
  },
  LinearProgressbar: {
    marginTop: 10, borderRadius: 5
  },
  actionIcons: {
    flex: 1,
    display: "none",
    justifyContent: "space-around"
  },

  tableRowHover: {
    "&:hover": {
      borderColor: "#BFBFBF",

      "& .actionIcons": {
        display: "flex"
      }
    }
  },
  automationRuleText: {
    fontSize: 18,
    fontFamily: theme.typography.fontFamilyLato,
    color: "#161717",
    fontWeight: "bold"
  },
  userName: {
    fontSize: 14,
    fontFamily: theme.typography.fontFamilyLato,
    color: "#161717",
  },
  AutomationDesc: {
    color: "#161717",
    fontFamily: theme.typography.fontFamilyLato,
    fontSize: 14
  }

});
export default Style;
