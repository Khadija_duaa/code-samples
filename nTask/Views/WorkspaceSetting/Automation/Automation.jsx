import { withStyles } from "@material-ui/core/styles";
import React, { useEffect, useState } from "react";
import Style from "./Automation.style";

import {
  Avatar,
  Divider,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Tooltip,
  Typography,
} from "@material-ui/core";
import { Add } from "@material-ui/icons";
import ArrowRightAltIcon from "@material-ui/icons/ArrowRightAlt";
import { omit } from "lodash";
import { useSelector } from "react-redux";
import CustomButton from "../../../components/Buttons/CustomButton";
import CustomDialog from "../../../components/Dialog/CustomDialog";
import DefaultSwitch from "../../../components/Form/Switch";
import IconBot from "../../../components/Icons/IconBot";
import IconCopy from "../../../components/Icons/IconCopyAutomation";
import IconDelete from "../../../components/Icons/IconDeleteAutomation";
import IconIssue from "../../../components/Icons/IconIssue";
import IconProject from "../../../components/Icons/IconProject";
import IconRisk from "../../../components/Icons/IconRisk";
import IconRule from "../../../components/Icons/IconRule";
import IconTask from "../../../components/Icons/IconTask";
import Loader from "../../../components/Loader/Loader";
import { generateActiveMembers } from "../../../helper/getActiveMembers";
import {
  createAutomationRule,
  getAutomationRules,
  updateAutomationRule,
  updateAutomationRuleStatus,
} from "../../../redux/actions/team";
import { compose } from "redux";
import AutomationRuleForm from "./AutomationRuleForm/AutomationRuleForm.view";
import { Condition } from "./AutomationRuleForm/TriggerForm/DateCondition/DateCondition";
import Empty from "./Empty";
import DeleteRuleModal from "./DeleteRuleModal";
import IconArrowForward from "../../../components/Icons/IconArrowForward";
import { withSnackbar } from "notistack";

export const show = [
  {
    label: "Select All",
    value: "select All",
  },
  {
    label: "Task Rules",
    value: "task Rules",
  },
  {
    label: "Project Rules",
    value: "project Rules",
  },
  {
    label: "Issue Rules",
    value: "issue Rules",
  },
];

export const action = [
  {
    label: "Select All",
    value: "select All",
  },
];

export const getAutomationTriggerName = (trigger, fields) => {
  if (trigger?.type === "date")
    // for dynamic trigger name
    // return `${fields.find((field) => trigger.fieldType === field.fieldKey)?.fieldName} ${trigger.condition
    //   } ${trigger.durationValue === 0 ? "Due Today" : trigger.durationValue} ${trigger.durationType
    //   } ${trigger.condition === Condition.approaching ? "before" : "after"}`;
    return 'Date meets the condition'

  if (trigger?.type === "status")
    return `status changes to ${trigger.checked?.join(", ").length < 60
      ? trigger.checked?.join(", ")
      : `${trigger.checked?.slice(0, 3)?.join(", ")} ${trigger.checked?.length - 3} more`
      }`;

  if (trigger?.type === "field")
    return `${trigger.field} Changes to ${Array.isArray(trigger.value)
      ? trigger.value?.join(", ").length < 60
        ? trigger.value?.join(", ")
        : trigger.value?.slice(0, 3)?.join(", ") + ` ${trigger.value?.length - 3} more`
      : trigger.value
      }`;

  return "";
};

export const getAutomationActionName = (action) => {
  if (action?.type === "reminder")
    return `Send ${action?.isEmail && action?.isInApp
      ? "email/in-app alert"
      : action?.isEmail
        ? "email alert"
        : action?.isInApp
          ? "in-app alert"
          : ""
      }`;
  return "";
};

const Automation = (props) => {
  const { classes, enqueueSnackbar } = props;

  const profile = useSelector((state) => state.profile.data);
  const featurefield = useSelector((store) => store.featureAccessPermissions);
  const members = generateActiveMembers();

  const [refresh, setRefresh] = useState(true);
  const [detail, setDetail] = useState(undefined);
  const [showForm, setShowForm] = useState(false);
  const [editting, setEditting] = useState(undefined);
  const [data, setData] = useState(undefined);
  const [deleteRule, setDeleteRule] = useState(undefined);

  useEffect(() => {
    if (refresh) {
      getAutomationRules(
        profile.loggedInTeam,
        (resp) => {
          setRefresh(false);
          setData(resp.data);
        },
        () => setRefresh(false)
      );
    }
  }, [refresh]);

  if (showForm || editting)
    return (
      <AutomationRuleForm
        data={editting}
        OnCreate={(data, success = () => { }, failure = () => { }) => {
          if (data._id) {
            updateAutomationRule(
              data,
              (rule) => {
                setData(prev => prev.map(item => item._id === rule._id ? rule : item))
                setRefresh(true);
                setShowForm(false);
                setEditting(undefined);
                success('Rule updated Successfully')
              },
              (error) => {
                failure(error.message)
              }
            );
          } else {
            createAutomationRule(
              data,
              (rule) => {
                setData(prev => [...prev, rule.data,])
                setRefresh(true);
                setShowForm(false);
                setEditting(undefined);
                success('Rule created Successfully')
              },
              (error) => {
                failure(error.message)
              }
            );
          }
        }}
        OnCancel={() => {
          setEditting(undefined);
          setShowForm(false);
        }}
      />
    );

  if (!data) return <Loader />;
  if (!data.length) return <Empty OnCreate={() => setShowForm(true)} />;

  const fields = Object.values(featurefield?.task).filter((field) => field.fieldType === "date");

  const activeChange = (rule, value) => {
    setData(prev => prev.map(item => {
      if (item._id !== rule._id)
        return item;
      return { ...item, isActive: value };
    }));

    updateAutomationRuleStatus({ id: rule._id, data: value }, () => {
      rule.isActive ?
        showSnackBar("The rule will not be triggered for future scheduled tasks", "info")
        : null;
    }, () => setRefresh(true));
  }


  const onSuccess = () => {
    setRefresh(true);
    setShowForm(false);
    setEditting(undefined);
    showSnackBar("Rule duplicated succcessfully", "success");
  };
  const onSuccessDelete = (rule) => {
    setData(prev => prev.filter(item => item._id !== rule._id))
    setDeleteRule(undefined);
    setRefresh(true);
    setShowForm(false);
    showSnackBar("Rule deleted succcessfully", "success");
  };
  const showSnackBar = (snackBarMessage = "", type) => {
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  return (
    <div className={classes.container}>
      <div className={classes.autoHeader}>
        <Typography className={classes.automationRuleText}>Rules</Typography>
        <div className={classes.createnewrule}>
          <CustomButton
            onClick={() => setShowForm(true)}
            btnType="success"
            variant="contained"
            style={{ marginBottom: 0 }}>
            <Add
              style={{
                fontSize: "1rem",
                marginRight: 7,
                transform: "scale(1.5)",
              }}
            />
            Create New Rule
          </CustomButton>
        </div>
      </div>

      <div>
        <Table className={classes.Table} style={{ marginTop: "10px" }}>
          <TableHead>
            <TableRow>
              <TableCell component="th" className={classes.tablehead}>
                Name
              </TableCell>
              <TableCell component="th" width="30%" className={classes.tablehead}>
                Created by
              </TableCell>
              <TableCell component="th" className={classes.tablehead}>
                Status
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map((rule, index) => (
              <TableRow key={index} className={classes.tableRowHover}>
                <TableCell classes={{ root: classes.tableCell }} component="th" scope="row">
                  <div className={classes.namecolumn} onClick={() => setEditting(rule)}>
                    <div className={classes.ruletypeicon}>
                      {rule?.type == "project" && <IconProject />}
                      {rule?.type == "task" && <IconTask />}
                      {rule?.type == "issue" && <IconIssue />}
                      {rule?.type == "risk" && <IconRisk />}
                    </div>
                    <div>
                      <Typography color="inherit" className={classes.triggertypebeforeicon}>
                        {!!rule?.name ? (
                          rule?.name
                        ) : (
                          <>
                            When{" "}
                            {rule.triggers?.length
                              ?
                              // for dynamic name
                              // rule.triggers
                              // .map((trigger) => getAutomationTriggerName(trigger, fields))
                              // .join(" and ")
                              // for static name
                              getAutomationTriggerName(rule.triggers[0], fields)
                              : "this trigger happens"}
                            <IconArrowForward />
                            {rule.actions.length
                              ? rule.actions
                                .map((action) => getAutomationActionName(action))
                                .join(" and ")
                              : "do this action"}
                          </>
                        )}
                      </Typography>
                    </div>
                  </div>
                </TableCell>
                <TableCell classes={{ root: classes.tableCell }}>
                  <div className={classes.avatarcolumn}>
                    <Avatar
                      style={{ marginRight: 10, height: 30, width: 30 }}
                      alt="Profie Image"
                      src={members.find((user) => user.userId === rule.createdBy)?.imageUrl}
                      className={classes.avatar}>
                      {members.find((user) => user.userId === rule.createdBy)?.fullName[0]}
                    </Avatar>
                    <Typography className={classes.userName}>
                      {members.find((user) => user.userId === rule.createdBy)?.fullName}
                    </Typography>
                  </div>
                </TableCell>
                <TableCell classes={{ root: classes.tableCell }} style={{ width: "200px" }}>
                  <div style={{ display: "flex" }}>
                    <DefaultSwitch
                      checked={rule.isActive}
                      onChange={(_, value) => activeChange(rule, value)}
                      value={rule.isActive} />

                    <div className={`actionIcons ${classes.actionIcons}`}>
                      <Tooltip title="Duplicate">
                        <IconButton
                          style={{ padding: 0, backgroundColor: "transparent" }}
                          onClick={() => createAutomationRule(
                            omit(rule, "_id"),
                            onSuccess,
                            () => { }
                          )}>
                          <IconCopy />
                        </IconButton>
                      </Tooltip>
                      <Tooltip title="Delete">
                        <IconButton
                          style={{ padding: 0, backgroundColor: "transparent" }}
                          onClick={() => setDeleteRule(rule)}>
                          <IconDelete />
                        </IconButton>
                      </Tooltip>
                    </div>
                  </div>
                </TableCell>
              </TableRow>
            )).reverse()}
          </TableBody>
        </Table>
      </div>
      <DeleteRuleModal
        rule={deleteRule}
        onDelete={onSuccessDelete}
        onCancel={() => setDeleteRule(undefined)}
      />
      <CustomDialog
        title="Action History"
        dialogProps={{
          open: !!detail,
          onClick: (e) => e.stopPropagation(),
          onClose: () => setDetail(undefined),
          PaperProps: { style: { maxWidth: 500 } },
        }}>
        <div style={{ padding: 30 }}>
          <div className={classes.actionhistoryheader}>
            <IconBot />
            <div style={{ marginLeft: 10 }}>
              <Typography className={classes.actionhistoryheading}>
                When status changes
                <ArrowRightAltIcon style={{ padding: "0 2 0 2" }} />
                then send email/in-app in app alert
              </Typography>
              <Typography variant="caption" style={{ fontSize: 11 }}>
                Created by: Frank Lewis{" "}
              </Typography>
            </div>
          </div>
          <div className={classes.actionhistorybody}>
            <div
              className={classes.actionhistoryfiger}
              style={{
                marginLeft: "-10px",
              }}>
              <div>
                <Typography className={classes.actiontext}>Action</Typography>
                <Typography variant="h5" className={classes.actioncounterfiggertext}>
                  05
                </Typography>
              </div>
            </div>
            <div className={classes.actionhistoryfiger}>
              <div>
                <Typography className={classes.actiontext}>Success</Typography>
                <Typography variant="h5" className={classes.actioncounterfiggertext}>
                  04
                </Typography>
              </div>
            </div>
            <div className={classes.actionhistoryfiger}>
              <div>
                <Typography className={classes.actiontext}>Panding</Typography>
                <Typography variant="h5" className={classes.actioncounterfiggertext}>
                  0
                </Typography>
              </div>
            </div>
            <div className={classes.failedCounter}>
              <div>
                <Typography className={classes.actiontext}>Failed</Typography>
                <Typography variant="h5" className={classes.actioncounterfiggertext}>
                  0
                </Typography>
              </div>
            </div>
          </div>
          <Divider light />

          <div style={{ position: "relative" }}>
            <div className={classes.AHbgLine} />
            <div>
              <Typography className={classes.history}>History</Typography>
              <div className={classes.historyField}>
                <div className={classes.iconruletex}>
                  <IconRule />
                  <Typography className={classes.rultexheading}>
                    Friday,Jully 22,2022-05:40 pm
                  </Typography>
                </div>
                <div className={classes.seccessbtn}>
                  <Typography className={classes.seccessbtntex}>Success</Typography>
                </div>
              </div>
              <div className={classes.historyField}>
                <div className={classes.iconruletex}>
                  <IconRule />
                  <Typography className={classes.rultexheading}>
                    Friday,Jully 22,2022-05:40 pm
                  </Typography>
                </div>
                <div className={classes.seccessbtn}>
                  <Typography className={classes.seccessbtntex}>Success</Typography>
                </div>
              </div>
              <div className={classes.historyField}>
                <div className={classes.iconruletex}>
                  <IconRule />
                  <Typography className={classes.rultexheading}>
                    Friday,Jully 22,2022-05:40 pm
                  </Typography>
                </div>
                <div className={classes.Filebtn}>
                  <Typography className={classes.fieldbtntext}>Failed</Typography>
                </div>
              </div>
              <div className={classes.historyField}>
                <div className={classes.iconruletex}>
                  <IconRule />
                  <Typography className={classes.rultexheading}>
                    Friday,Jully 22,2022-05:40 pm
                  </Typography>
                </div>
                <div className={classes.seccessbtn}>
                  <Typography className={classes.seccessbtntex}>Success</Typography>
                </div>
              </div>
              <div className={classes.historyField}>
                <div className={classes.iconruletex}>
                  <IconRule />
                  <Typography className={classes.rultexheading}>
                    Friday,Jully 22,2022-05:40 pm
                  </Typography>
                </div>
                <div className={classes.seccessbtn}>
                  <Typography className={classes.seccessbtntex}>Success</Typography>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className={classes.modelFooter}>
          <CustomButton btnType="success" variant="contained" style={{ display: "flex" }}>
            Done
          </CustomButton>
        </div>
      </CustomDialog>
    </div>
  );
};

export default compose(
  withSnackbar,
  withStyles(Style, { withTheme: true })
)(Automation);


// withStyles(Style, { withTheme: true })(Automation);
