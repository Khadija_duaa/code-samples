import { Typography } from "@material-ui/core";
import { Add } from "@material-ui/icons";
import React from "react";
import CustomButton from "../../../components/Buttons/CustomButton";
import image from "../../../assets/images/automation/Empty.svg";
import { withStyles } from "@material-ui/core/styles";
import Style from "./Automation.style";

const Empty = (props) => {
  const { OnCreate, classes } = props;

  return (
    <section className={classes.emptyBody}>
      <div style={{ marginTop: "50px" }}>
        <img src={image} />
        <Typography className={classes.AutomationEmptyText}>
          Make your life easier with Automation
        </Typography>
        <Typography className={classes.AutomationDesc}>
          Automation rules help reduce busy work and let you spend more time working on what really
          matters
        </Typography>
        <CustomButton
          onClick={OnCreate}
          btnType="success"
          variant="contained"
          style={{ margin: "15px 0 10px 0", padding: "6px 16px 6px 11px " }}>
          <Add />
          Create Rule
        </CustomButton>
      </div>
    </section>
  );
};

export default withStyles(Style, { withTheme: true })(Empty);
