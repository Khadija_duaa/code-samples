
const Style = (theme) => ({
    commentText:{
        display: "flex", alignItems: "center", marginBottom:10
    },
    iconButton:{
        padding: 0, marginRight: 8
    },
    TextFieldText:{
        marginTop: "1rem", marginBottom: "5px"
    }
});
export default Style;