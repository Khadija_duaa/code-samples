import React, { useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { Typography, IconButton, TextField } from "@material-ui/core";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import DefaultTextField from "../../../../../../components/Form/TextField";
import DefaultCheckbox from "../../../../../../components/Form/Checkbox";
import { useState } from "react";
import style from "./MentionCondition.style"


const people = ["Any Value", "Frank Lewis", "Diana Smiley", "Oscar Thomsen", "Matthew Walsh","All the assignees of the item"];

const MentionCondition = (props) => {
  const { onChange, data, onBack, classes } = props;

  const [rule, setRule] = useState(
    data?.type === "mention"
      ? data
      : {
          ...(data?.id !== undefined && { id: data.id }),
          type: "mention",
          message: "",
          sendTo: [],
        }
  );

  useEffect(() => {
    setRule(
      data?.type === "mention"
        ? data
        : {
            ...(data?.id !== undefined && { id: data.id }),
            type: "mention",
            message: "",
            sendTo: [],
          }
    );
  }, [data]);

  const updateRule = (value) => {
    setRule(value);
    onChange(value);
  };

  return (
    <div>
      <Typography className={classes.commentText} variant="h4" >
        <IconButton className={classes.iconButton} onClick={onBack}>
        <ArrowBackIosIcon fontSize="small" />
        </IconButton>
        Add comment or mention
      </Typography>

        <Typography>Comment</Typography>
        <TextField
        style={{width:250}}
          onChange={({ target: { value } }) => updateRule({ ...rule, message: value })}
          variant="outlined"
          multiline
          rows={5}
        />

      <div>
        <Typography className={classes.TextFieldText}>Mention</Typography>
   <div style={{width:250}}>
   <DefaultTextField style={{width:250}} fullWidth defaultProps={{ id: "search", placeholder: "Search"}} />
   </div>
        {people.map((value) => (
          <label className={classes.commentText} >
            <DefaultCheckbox
              checked={rule.sendTo?.includes(value)}
              onChange={({ target: { name } }, value) =>
                updateRule({
                  ...rule,
                  sendTo: value
                    ? [...(rule.sendTo ?? []), name]
                    : rule.sendTo.filter((item) => item !== name),
                })
              }
              checkboxStyles={{ padding: 0, marginRight: 8 }}
              name={value}
            />
            <Typography>{value}</Typography>
          </label>
        ))}
      </div>
    </div>
  );
};

export default withStyles(style, { withTheme: true })(MentionCondition);
