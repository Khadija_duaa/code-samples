
const Style = (theme) => ({
    actionformBody: {
        position: "sticky",
        top: "70px",
    },
    actiontext: {
        fontSize: "16px !important",
        fontWeight: "bold",
        color: "black",
        fontFamily: theme.typography.fontFamilyLato,
    },
    form: {
        display: "flex",
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        cursor: "pointer",
    },
    formicon: {
        marginRight: 15,
        padding: "5px",
        borderRadius: "5px",
        width: 40,
        height: 40,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#E6FAF4",
    },
    remindertext: {
        color: "black",
        fontSize: "14px !important",
        fontFamily: theme.typography.fontFamilyLato,
    },
    descriptionModel: {
        borderTop: "1px solid #e0dcdc",
        width: "100%",
        display: "flex",
        justifyContent: "end",
        backgroundColor: "#F6F6F6",
        padding: "20px",
    },
    selectAction_text: {
        marginRight: 15,
        padding: "10px",
        borderRadius: "5px",
        width: "40px",
        height: "40px",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#E6FAF4",
        border: "2px #00CC90 dashed ",
    },
    notificationIcon: {
        marginRight: 15,
        padding: "10px",
        borderRadius: "5px",
        width: "40px",
        height: "40px",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#E6FAF4",
        border: "2px #00CC90 solid ",
    },
    allerts: {
        fontWeight: "bold",
        color: "black",
        fontSize: "14px !important",
        fontFamily: theme.typography.fontFamilyLato,

    },
    user_massege: {
        fontSize: "14px !important",
        color: "black",
        fontFamily: theme.typography.fontFamilyLato,
    },
    dell_icon: {
        alignSelf: "stretch",
        padding: "0 10px",
        display: "none",
        alignItems: "center",
        justifyContent: "center",
    },
    letUsanchor: {
        color: "#76C3FF",
        cursor: "pointer"
    },
    actionidea: {
        marginTop: "1rem",
        color: "black"
    },
    container: {
        backgroundColor: "white",
        marginBottom: 10,
        borderRadius: 5,
        border: "1px solid #ddd",
        padding: 10,
        display: "flex",
        alignItems: "center",
        justifyContent: "space-between",
        cursor: "pointer",

        "&:hover": {
            borderColor: "#BFBFBF",

            "& .icondelete": {
                display: "flex"
            }
        }
    },
    actionTrigerText: {
        color: "#7E7E7E",
        fontFamily: theme.typography.fontFamilyLato,
        fontSize: "14px !important"
    }
});
export default Style;