import React, { useState } from "react";
import { Typography } from "@material-ui/core";
import iconDelete from "../../../../../assets/images/automation/iconDelete.svg";
import IconNotification from "../../../../../components/Icons/IconNotification";
import { generateActiveMembers } from "../../../../../helper/getActiveMembers";
import { withStyles } from "@material-ui/core/styles";
import Style from "./Action.style";
import { getAutomationActionName } from '../../Automation';

const ActionCard = (props) => {
  const { onClick, action, active, onDelete, classes } = props;

  const members = generateActiveMembers();

  const usersstring = action.sendTo?.map(
    (userId) => members.find((user) => user.userId === userId)?.fullName
  );

  return (
    <div
      className={classes.container}
      style={{
        ...(active && { borderColor: "#00ABED" }),
      }}
      onClick={onClick}>
      <div className={classes.notificationIcon}>
        <IconNotification />
      </div>
      <div style={{ flex: 1 }}>
        {action.type === "reminder" && (
          <>
            <Typography className={classes.allerts}>
              {getAutomationActionName(action)} reminder to
            </Typography>
            <Typography className={classes.user_massege}>
              {usersstring.join(", ").length < 60
                ? usersstring?.join(", ")
                : usersstring?.slice(0, 3)?.join(", ") + `, ${usersstring?.length - 3} more`}{" "}
            </Typography>
          </>
        )}
        {action.type === "mention" && (
          <>
            <Typography className={classes.allerts}>Add comment or mention</Typography>
            <Typography className={classes.allerts}>
              {usersstring.join(", ").length < 60
                ? usersstring?.join(", ")
                : usersstring?.slice(0, 3)?.join(", ") + `, ${usersstring?.length - 3} more`}{" "}
            </Typography>
            <Typography className={classes.user_massege}>{action.message}</Typography>
          </>
        )}

        {action.type === "field" && (
          <>
            <Typography className={classes.allerts}>Change {action.field} value to</Typography>
            <Typography className={classes.user_massege}>
              {action.value?.join(", ").length < 60
                ? action.value?.join(", ")
                : action.value?.slice(0, 3)?.join(", ") + `, ${action.value?.length - 3} more`}
            </Typography>
          </>
        )}
      </div>
      <div onClick={onDelete} className={`icondelete ${classes.dell_icon}`}>
        <img src={iconDelete} />
      </div>
    </div>
  );
};

export default withStyles(Style, { withTheme: true })(ActionCard);
