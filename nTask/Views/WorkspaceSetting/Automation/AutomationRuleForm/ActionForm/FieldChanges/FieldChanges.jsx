import React, { useEffect, useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import { IconButton, Typography } from "@material-ui/core";
import CustomSelect from "../../../../../../components/Dropdown/CustomSelect/CustomSelect.view";
import DefaultCheckbox from "../../../../../../components/Form/Checkbox";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import style from "./FieldChanges.style";

const fieldchanges = [
  {
    label: "Required Design Formats",
    value: "Required Design Formats",
  },
  {
    label: "Design Dimension",
    value: "Design Dimension",
  },
  {
    label: "Inspection Site",
    value: "Inspection Site",
  },
  {
    label: "Responsible Person",
    value: "Responsible Person",
  },
];

const people = ["Any Value", "Frank Lewis", "Diana Smiley", "Oscar Thomsen", "Matthew Walsh"];

const FieldChanges = (props) => {
  const { onChange, data, onBack, classes } = props;

  const [rule, setRule] = useState(
    data?.type === "field"
      ? data
      : {
          ...(data?.id !== undefined && { id: data.id }),
          type: "field",
          field: undefined,
          value: [],
        }
  );

  useEffect(() => {
    setRule(
      data?.type === "field"
        ? data
        : {
            ...(data?.id !== undefined && { id: data.id }),
            type: "field",
            field: undefined,
            value: [],
          }
    );
  }, [data]);

  const updateRule = (value) => {
    setRule(value);
    onChange(value);
  };

  return (
    <div>
      <Typography className={classes.fieldChangesText}>
        <IconButton style={{ padding: 0 }} onClick={onBack}>
          <ChevronLeftIcon />
        </IconButton>
        Field changes
      </Typography>
      <Typography>Select Field</Typography>
      <CustomSelect
        showCheck={false}
        bgapply={false}
        style={{
          display: "flex",
          justifyContent: "space-between",
          backgroundColor: "white",
          border: "1px solid #EAEAEA",
          width: 250,
          justifyContent: "space-between",
        }}
        value={rule.field}
        onChange={(option) =>
          updateRule({
            ...rule,
            field: option.value,
            ...(rule.field === "Responsible Person" && { value: [] }),
          })
        }
        options={fieldchanges}
      />
      <div className={classes.responsible}>
        {rule.field === "Responsible Person" &&
          people.map((value) => (
            <label className={classes.checkBoxLabel}>
              <DefaultCheckbox
                checked={rule.value?.includes(value)}
                onChange={({ target: { name } }, value) =>
                  updateRule({
                    ...rule,
                    value: value
                      ? [...(rule.value ?? []), name]
                      : rule.value.filter((item) => item !== name),
                  })
                }
                checkboxStyles={{ padding: 0, marginRight: 8 }}
                name={value}
              />
              <Typography>{value}</Typography>
            </label>
          ))}
      </div>
    </div>
  );
};

export default withStyles(style, { withTheme: true })(FieldChanges);
