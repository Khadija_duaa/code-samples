
const Style = (theme) => ({
    fieldChangesText:{
        display: "flex", alignItems: "center", fontSize:16, fontWeight:"bold" , color:"black"
    },
    selectFieldText:{
        marginTop: "1rem", marginBottom: "5px", fontSize:13
    },
    responsible:{
        marginTop: "1rem", marginBottom: "5px"
    },
    checkBoxLabel:{
        display: "flex", alignItems: "center"
    }
});
export default Style;