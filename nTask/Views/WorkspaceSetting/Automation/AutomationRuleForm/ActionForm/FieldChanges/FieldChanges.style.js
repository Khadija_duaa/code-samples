
const Style = (theme) => ({
    fieldChangesText: {
        display: "flex", alignItems: "center", fontSize: "16px !important", fontWeight: "bold", color: "black",marginBottom:10
    },
    selectFieldText: {
        marginTop: "1rem", marginBottom: "5px", fontSize: "13px !important"
    },
    responsible: {
        marginTop: "1rem", marginBottom: "5px"
    },
    checkBoxLabel: {
        display: "flex",
        alignItems: "center"
    }
});
export default Style;