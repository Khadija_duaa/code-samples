import React, { useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import style from "./Action.style";
import { Divider, TextField, Typography } from "@material-ui/core";
import CustomDialog from "../../../../../components/Dialog/CustomDialog";
import CustomButton from "../../../../../components/Buttons/CustomButton";
import Reminder from "./Reminder/Reminder";
import FieldChanges from "./FieldChanges/FieldChanges";
import Mention from "./MentionCondition/MentionCondition";
import IconNotification from "../../../../../components/Icons/IconNotification";
import IconComment from "../../../../../components/Icons/IconComment";
import IconField from "../../../../../components/Icons/IconField";

const ActionForm = (props) => {
  const { onChange, data, classes } = props;

  const [open, setOpen] = useState(false);
  const [type, setType] = useState(data?.type ?? "");

  if (type === "reminder")
    return <Reminder data={data} onChange={onChange} onBack={() => setType("")} />;

  if (type === "mention")
    return <Mention data={data} onChange={onChange} onBack={() => setType("")} />;

  if (type === "field")
    return <FieldChanges data={data} onChange={onChange} onBack={() => setType("")} />;

  return (
    <div className={classes.actionformBody}>
      <Typography className={classes.actiontext}>Actions</Typography>
      <div>
        <div className={classes.form} onClick={() => setType("reminder")}>
          <div className={classes.formicon}>
            <IconNotification />
          </div>
          <Typography className={classes.remindertext}>Send Reminder</Typography>
        </div>
        {/* <div onClick={() => setType("mention")} className={classes.form}>
          <div className={classes.formicon}>
            <IconComment />
          </div>
          <Typography className={classes.remindertext}>A connect or mention</Typography>
        </div>
        <div onClick={() => setType("field")} className={classes.form}>
          <div className={classes.formicon}>
            <IconField />
          </div>
          <Typography className={classes.remindertext}>Change Field value</Typography>
        </div> */}
        {/* <Divider light style={{ marginTop: "1rem" }} />
        <Typography className={classes.actionidea}>
          Do you have any ideas for new actions?
        </Typography>
        <Typography className={classes.letUsanchor} onClick={() => setOpen(true)}>
          Let Us Know
        </Typography> */}
      </div>

      <CustomDialog
        title="Description your action idea"
        dialogProps={{
          open: open,
          onClick: (e) => e.stopPropagation(),
          onClose: () => setOpen(false),
          PaperProps: { style: { maxWidth: 500 } },
        }}>
        <div style={{ padding: "20px" }}>
          <TextField variant="outlined" fullWidth multiline rows={5} />
        </div>

        <div className={classes.descriptionModel}>
          <CustomButton onClick={() => setOpen(false)} style={{ marginRight: "10px" }}>
            Cancel
          </CustomButton>
          <CustomButton btnType="success" variant="contained">
            Send
          </CustomButton>
        </div>
      </CustomDialog>
    </div>
  );
};

export default withStyles(style, { withTheme: true })(ActionForm);
