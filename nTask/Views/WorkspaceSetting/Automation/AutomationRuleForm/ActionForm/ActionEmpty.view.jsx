import React from "react";
import { Typography } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import style from "./Action.style";
import iconDelete from "../../../../../assets/images/automation/iconDelete.svg";

const ActionEmpty = (props) => {
  const { onClick, active, classes, onDelete } = props;

  return (
    <div
      className={classes.container}
      style={{ border: `1px solid ${active ? "#00ABED" : "#ddd"}` }}
      onClick={onClick}>
      <div style={{ display: "flex", alignItems: "center" }}>
        <div className={classes.selectAction_text}>&nbsp;</div>
        <Typography className={classes.actionTrigerText}>Select an Action</Typography>
      </div>

      <div onClick={onDelete} className={`icondelete ${classes.dell_icon}`}>
        <img src={iconDelete} />
      </div>
    </div>
  );
};

export default withStyles(style, { withTheme: true })(ActionEmpty);
