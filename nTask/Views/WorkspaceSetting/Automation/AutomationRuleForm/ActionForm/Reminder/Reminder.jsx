import React, { useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { Typography, IconButton, TextField } from "@material-ui/core";
import DefaultCheckbox from "../../../../../../components/Form/Checkbox";
import { useState } from "react";
import AssigneeDropdown from "../../../../../../components/Dropdown/AssigneeDropdown2";
import { generateActiveMembers } from "../../../../../../helper/getActiveMembers";
import style from "./Reminder.style";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";

const ReminderCondition = (props) => {
  const { onChange, data, onBack, classes } = props;
  const members = generateActiveMembers();

  const updateRule = (value) => {
    setRule(value);
    onChange(value);
  };

  const [rule, setRule] = useState(
    data?.type === "reminder"
      ? data
      : {
          ...(data?.id !== undefined && { id: data.id }),
          type: "reminder",
          isInApp: false,
          isEmail: false,
          sendTo: [],
          message: "",
        }
  );

  useEffect(() => {
    setRule(
      data?.type === "reminder"
        ? data
        : {
            ...(data?.id !== undefined && { id: data.id }),
            type: "reminder",
            isInApp: false,
            isEmail: false,
            sendTo: [],
            message: "",
          }
    );
  }, [data]);

  return (
    <div className={classes.container}>
      <div className={classes.Remider}>
        <IconButton style={{ marginRight: "10px", padding: 0 }} onClick={onBack}>
          <ChevronLeftIcon style={{ color: "black" }} />
        </IconButton>
        <Typography className={classes.remider_text}>Send reminder</Typography>
      </div>

      <div className={classes.spacing}>
        <Typography className={classes.reminderTypeText}>Reminder Type</Typography>
        <label className={classes.checkBoxLabel}>
          <DefaultCheckbox
            checked={rule.isEmail}
            onChange={(_, value) => updateRule({ ...rule, isEmail: value })}
            checkboxStyles={{ padding: 0, marginRight: 8 }}
          />
          <Typography className={classes.checkBoxText}>Email</Typography>
        </label>

        <label className={classes.checkBoxLabel}>
          <DefaultCheckbox
            checked={rule.isInApp}
            onChange={(_, value) => updateRule({ ...rule, isInApp: value })}
            checkboxStyles={{ padding: 0, marginRight: 8 }}
          />
          <Typography className={classes.checkBoxText}>In-app & Desktop</Typography>
        </label>
      </div>

      <div className={classes.spacing}>
        <Typography className={classes.reminderTypeText}>Message</Typography>

        <TextField
          placeholder="Type your message (optional)"
          onChange={({ target: { value } }) =>
            updateRule({ ...rule, message: value.slice(0, 250) })
          }
          variant="outlined"
          fullWidth
          multiline
          value={rule.message}
          rows={5}
        />
        <Typography className={classes.reminderTypeText}>Send To</Typography>
        <AssigneeDropdown
          assignedTo={rule.sendTo?.map((userId) => members.find((user) => user.userId === userId))}
          updateAction={(users) =>
            updateRule({ ...rule, sendTo: users.map((user) => user.userId) })
          }
          isArchivedSelected={false}
          avatarSize={"xsmall"}
        />
      </div>
    </div>
  );
};

export default withStyles(style, { withTheme: true })(ReminderCondition);
