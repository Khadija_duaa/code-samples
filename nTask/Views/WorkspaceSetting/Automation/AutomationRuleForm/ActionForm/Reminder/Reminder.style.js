
const Style = (theme) => ({
    remider_text: {
        color: "black",
        fontSize: "16px !important",
        fontWeight: "bold",
        fontFamily: theme.typography.fontFamilyLato,
    },

    checkBoxLabel: {
        display: "flex",
        alignItems: "center",
        marginTop: "5px"
    },
    spacing: {
        marginTop: "10px",
    },
    reminderTypeText: {
        color: "#7E7E7E",
        marginTop: "18px",
        marginBottom: 5,
        fontFamily: theme.typography.fontFamilyLato,
    },
    Remider: {
        display: "flex",
        alignItems: "center"
    },
    container: {
        maxWidth: 600
    },
    checkBoxText: {
        fontSize: "14px !important",
        color: "#000000",
        fontFamily: theme.typography.fontFamilyLato,
    }
});
export default Style;