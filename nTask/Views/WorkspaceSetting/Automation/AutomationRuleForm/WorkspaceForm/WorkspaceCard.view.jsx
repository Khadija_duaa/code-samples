import { Typography } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { lowerCase, startCase } from "lodash";
import React from "react";
import { useSelector } from "react-redux";
import IconIssue from "../../../../../components/Icons/IconIssue";
import IconProject from "../../../../../components/Icons/IconProject";
import IconRisk from "../../../../../components/Icons/IconRisk";
import IconTask from "../../../../../components/Icons/IconTask";
import Style from "./WorkspaceCard.style";

const WorkspaceCard = (props) => {
  const { onClick, workspace, active, classes } = props;
  const profile = useSelector((state) => state.profile.data);

  return (
    <div
      className={classes.container}
      style={{
        ...(active && { borderColor: "#00ABED" }),
      }}
      onClick={onClick}>
      <div
        style={{
          marginRight: 15,
          backgroundColor:
            workspace.type == "issue"
              ? "#FFECEB"
              : workspace.type == "risk"
              ? "#FFF3E6"
              : "#E6F7FD",
          borderRadius: "5px",
          width: 40,
          height: 40,
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          border: "2px solid #00CC90",
        }}>
        {workspace.type == "project" && <IconProject />}
        {workspace.type == "task" && <IconTask />}
        {workspace.type == "issue" && <IconIssue />}
        {workspace.type == "risk" && <IconRisk />}
      </div>
      <div>
        <Typography className={classes.apply}>
          Apply to {workspace.type && `${startCase(lowerCase(workspace.type))}s`}
        </Typography>
        <Typography className={classes.applyDescription}>
          in{" "}
          {
            profile.workspace.find(
              (profileworkspace) => profileworkspace.teamId === workspace.workspaceId
            )?.teamName
          }
        </Typography>
      </div>
    </div>
  );
};
export default withStyles(Style, { withTheme: true })(WorkspaceCard);
