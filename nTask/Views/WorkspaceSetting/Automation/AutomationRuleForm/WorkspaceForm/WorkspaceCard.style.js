const Style = (theme) => ({
    applyToText: {
        fontWeight: "bold",
        color: "black",
        fontSize: "16px !important",
        fontFamily: theme.typography.fontFamilyLato
    },
    taskslabel: {
        display: "flex",
        alignItems: "center"
    },
    itemnametext: {
        display: "flex",
        alignItems: "center",
        marginTop: 10,
    },

    cardapplytext: {
        fontWeight: "bold",
        color: "black",
        fontSize: "14px !important"
    },
    apply: {
        fontWeight: "bold",
        color: "black",
        fontSize: "14px !important",
        fontFamily: theme.typography.fontFamilyLato
    },
    applyDescription: {
        fontSize: "14px !important",
        color: "black",
        fontFamily: theme.typography.fontFamilyLato
    },
    container: {
        backgroundColor: "white",
        marginBottom: 10,
        borderRadius: 5,
        border: "1px solid #ddd",
        padding: 10,
        display: "flex",
        alignItems: "center",
        cursor: "pointer",
        position: "relative",
        "&:hover": {
            borderColor: "#BFBFBF"
        }
    }
});

export default Style;
