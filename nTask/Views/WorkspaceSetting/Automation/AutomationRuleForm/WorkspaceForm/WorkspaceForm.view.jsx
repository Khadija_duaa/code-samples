import React from "react";
import { Typography } from "@material-ui/core";
import CustomSelect from "../../../../../components/Dropdown/CustomSelect/CustomSelect.view";
import { useState } from "react";
import { useSelector } from "react-redux";
// import DefaultCheckbox from "../../../../../components/Form/Checkbox";
// import { useRef } from "react";
// import DefaultTextField from "../../../../../components/Form/TextField";
// import { groupBy } from "lodash";
import { withStyles } from "@material-ui/core/styles";
import Style from "./WorkspaceCard.style";

export const options = [
  // {
  //   label: "Project",
  //   value: "project",
  // },
  {
    label: "Tasks",
    value: "task",
  },
  // {
  //   label: "Issues",
  //   value: "issue",
  // },
  // {
  //   label: "Risks",
  //   value: "risk",
  // },
];

export const tasks = [
  {
    active: false,
    name: "Architectural Design",
  },
  {
    active: true,
    name: "Business Development",
  },
  {
    active: false,
    name: "Facility Management",
  },
  {
    active: true,
    name: "Website Development",
  },
  {
    active: false,
    name: "Android Development",
  },
  {
    active: true,
    name: "Game Development",
  },
  {
    active: false,
    name: "Automation Testing",
  },
];

const WorkspaceForm = (props) => {
  const { data, onChange, classes } = props;

  const profile = useSelector((state) => state.profile.data);

  // const buttonref = useRef();
  // const [open, setOpen] = useState(false);
  const [value, setValue] = useState(
    data ?? {
      type: options[0].value,
      workspaceId: profile.workspace.find((item) => item.teamId === profile.loggedInTeam).teamId,
    }
  );

  const UpdateValue = (value) => {
    setValue(value);
    onChange?.(value);
  };

  // const tastlist = groupBy(tasks, (task) => (task.active ? "selected" : "unselected"));

  return (
    <div>
      <Typography className={classes.applyToText}>Apply to</Typography>
      <CustomSelect
        showCheck={false}
        bgapply={false}
        style={{
          marginTop: "10px",
          justifyContent: "space-between",
          backgroundColor: "white",
          display: "flex",
          width:"100%",
          border: "1px solid #EAEAEA",
        }}
        value={value.type}
        onChange={(option) => UpdateValue({ ...value, type: option.value })}
        options={options}
      />
      {/* <label className={classes.taskslabel}>
        <DefaultCheckbox checkboxStyles={{ padding: 0, marginRight: 8 }} />
        <Typography>Select All Tasks</Typography>
      </label>
      <DefaultTextField
        formControlStyles={{ marginBottom: 0 }}
        defaultProps={{
          id: "searchApply",
          placeholder: "Search",
          autoFocus: true,
          style: { paddingLeft: 6, width: 250, marginTop: 10 },
          inputProps: { style: { padding: "9px 6px" } },
        }}
      /> */}
      {/* {tastlist.selected?.map((item) => (
        <label className={classes.itemnametext}>
          <DefaultCheckbox checkboxStyles={{ padding: 0, marginRight: 8 }} />
          <Typography>{item.name}</Typography>
        </label>
      ))}
      {tastlist.unselected?.map((item) => (
        <label className={classes.itemnametext}>
          <DefaultCheckbox checkboxStyles={{ padding: 0, marginRight: 8 }} />
          <Typography>{item.name}</Typography>
        </label>
      ))} */}

      {/* <Typography variant="h6">in following workspace(s)</Typography>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          marginTop: "10px",
          alignItems: "center",
          flexWrap: "wrap",
        }}>
        {value.workspaceId?.map((teamId) => {
          const workspace = profile.workspace.find((item) => item.teamId === teamId);

          return (
            <div
              key={teamId}
              style={{
                display: "flex",
                padding: 8,
                margin: "0 8px 8px 0",
                backgroundColor: "#F1F1F1",
                borderRadius: "5px",
                alignItems: "center",
              }}>
              <Typography variant="h6">{workspace.teamName}</Typography>
              <IconButton
                style={{ padding: 0, marginLeft: 8 }}
                onClick={() =>
                  UpdateValue({
                    ...value,
                    workspaceId: value.workspaceId.filter((item) => item.teamId !== teamId),
                  })
                }>
                <Close fontSize="small" />
              </IconButton>
            </div>
          );
        })}
        <div ref={buttonref}>
          <IconButton
            style={{ marginBottom: 11, padding: 0, marginLeft: 8 }}
            onClick={() => setOpen((value) => !value)}>
            <IconAddWorkspace />
          </IconButton>
        </div>
        <DropdownMenu
          open={open}
          clickAway={false}
          closeAction={() => setOpen(false)}
          anchorEl={buttonref.current}
          placement="top-start">
          <div style={{ padding: 16 }}>
            <small style={{ display: "inline-block", marginBottom: 8 }}>Workspaces</small>
            <Scrollbars autoHide style={{ height: 300 }}>
              {profile.workspace.map((workspace) => (
                <label style={{ display: "flex", alignItems: "center" }}>
                  <DefaultCheckbox
                    checked={value.workspaceId.some((id) => id === workspace.teamId)}
                    onChange={(event, checked) => {
                      event.preventDefault();
                      event.stopPropagation();

                      return UpdateValue({
                        ...value,
                        workspaceId: checked
                          ? [...value.workspaceId, workspace.teamId]
                          : value.workspaceId.filter((id) => id !== workspace.teamId),
                      });
                    }}
                    checkboxStyles={{ padding: 0, marginRight: 8 }}
                  />
                  <Typography>{workspace.teamName}</Typography>
                </label>
              ))}
            </Scrollbars>
          </div>
        </DropdownMenu>
      </div> */}
    </div>
  );
};
export default withStyles(Style, { withTheme: true })(WorkspaceForm);
