const Style = (theme) => ({
  header: {
    justifyContent: "space-between",
    display: "flex",
    alignItems: "center",
    paddingBottom: "18px",
    borderBottom: "1px solid #ddd",
    fontFamily: theme.typography.fontFamilyLato,
  },
  headerBtn: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end"
  },
  bodyFlex: {
    display: "flex",
  },
  Card: {
    marginTop: "20px",
    marginBottom: "20px",
    border: "1px solid #ebebeb",
    borderRadius: "5px",
    padding: "10px",
    display: "flex",
    alignItems: "center",
  },
  iconCard: {
    padding: "10px",
    borderRadius: "5px",
    width: "40px",
    height: "40px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  sideBar: {
    width: "40%",
    padding: "30px",
  },
  main: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
    padding: "1rem 1rem 1rem 4%"
  },
  TriggerText: {
    lineHeight: "1.5",
    display: "inline",
    alignItems: "center",
    display: "flex",
    alignItems: "center",
    fontSize: "15px !important",
    maxWidth: "unset !important",
    fontFamily: theme.typography.fontFamilyLato,

  },
  form: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-between",
    flex: 1,
    alignItems: "flex-start"
  },
  wpbackline: {
    backgroundColor: "#ddd",
    top: "32px",
    bottom: "32px",
    width: 2,
    position: "absolute",
    marginLeft: 25,
    alignSelf: "start"
  },
  wpbody: {
    marginTop: "20px",
    marginBottom: "10px",
    position: "relative",
  },
  wpcard: {
    transform: "translate(80%, -210%)",
    background: "white",
    position: "absolute",
    height: 22,
    marginLeft: "-105px",
  },
  wpcardicon: {
    display: "flex",
    alignItems: "center"
  },
  actioncard: {

    marginTop: "20px",
    marginBottom: "10px",
    cursor: "pointer",
    position: "relative",
  },
  wptotalbody: {
    flex: 1,
    padding: "20px",
    paddingLeft: 0,
    position: "relative",
  },
  addAnother: {
    position: "relative", marginLeft: 16, marginTop: 26
  },
  addAnotherIcon: {
    display: "flex", alignItems: "center"
  },
  anotherTrigText: {
    fontSize: "14px !important",
    marginLeft: 10,
    cursor: "pointer",
    fontFamily: theme.typography.fontFamilyLato,
  },
  triggerInput: {
    fontFamily: theme.typography.fontFamilyLato,
  }

});
export default Style;
