import { ClickAwayListener, Typography } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import React, { useState } from "react";
import CustomTooltip from "../../../../components/Tooltip/Tooltip";
import { useSelector } from "react-redux";
import CustomButton from "../../../../components/Buttons/CustomButton";
import CustomSelect from "../../../../components/Dropdown/CustomSelect/CustomSelect.view";
import IconAddWorkspace from "../../../../components/Icons/IconAddWorkspace";
import ActionCard from "./ActionForm/ActionCard.view";
import ActionEmpty from "./ActionForm/ActionEmpty.view";
import ActionForm from "./ActionForm/ActionForm.view";
import TriggerEmpty from "./TriggerForm/TriggerEmpty.view";
import TriggerForm from "./TriggerForm/TriggerForm.view";
import WorkspaceCard from "./WorkspaceForm/WorkspaceCard.view";
import WorkspaceFormView, { options } from "./WorkspaceForm/WorkspaceForm.view";
// import IconAddAction from "../../../../components/Icons/IconAddWorkspace";

import { useRef } from "react";
import TextField from "../../../../components/Form/TextField";
import { getAutomationActionName, getAutomationTriggerName } from "../Automation";
import style from "./AutomationRuleForm.style";
import TriggerCardView from "./TriggerForm/TriggerCard.view";
import IconArrowForward from "../../../../components/Icons/IconArrowForward";
import { compose } from "redux";
import { withSnackbar } from "notistack";

const operatorOptions = [
  {
    value: "or",
    label: "Or",
    description: (
      <Typography style={{ fontSize: "10px" }}>When any of these triggers happen</Typography>
    ),
  },
  {
    label: "And",
    value: "and",
    description: (
      <Typography style={{ fontSize: "10px" }}>When all of these triggers happen</Typography>
    ),
  },
];

const AutomationRuleForm = (props) => {
  const { classes, theme, OnCancel, data: val, OnCreate, enqueueSnackbar } = props;

  const profile = useSelector((state) => state.profile.data);
  const featurefield = useSelector((store) => store.featureAccessPermissions);
  const input = useRef(null);
  const [submitted, setSubmitted] = useState(false);
  const [editting, setEditting] = useState(false);

  const fields = Object.values(featurefield.task).filter((field) => field.fieldType === "date");

  const [data, setData] = useState(
    val ?? {
      name: undefined,
      type: options[0].value,
      workspaceId: profile.workspace.find((item) => item.teamId === profile.loggedInTeam).teamId,
      operator: operatorOptions[0].value,
      triggers: [],
      actions: [],
    }
  );

  const [view, setView] = useState({
    name: "WorkspaceForm",
    data: { type: data.type, workspaceId: data.workspaceId },
  });

  //Snackbar function
  const showSnackBar = (snackBarMessage = "", type = null) => {
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };

  return (
    <div>
      <div className={classes.main}>
        <div className={classes.header}>
          <ClickAwayListener onClickAway={() => setEditting(false)}>
            <div
              style={{ flexGrow: 0.6, height: 40, display: "flex", alignItems: "center" }}
              onClick={() => {
                setEditting(true);
                setTimeout(() => input.current?.focus());
              }}>
              {!editting && (
                <div>
                  <CustomTooltip
                    helptext="click to edit the rule title"
                    iconType="help"
                    placement="top"
                    style={{ color: theme.palette.common.white }}>
                    {!!data.name ? (
                      <div className={classes.TriggerText}>{data.name}</div>
                    ) : (
                      <div className={classes.TriggerText}>
                        <div style={{ color: data.triggers.length ? "#161717" : "#7E7E7E" }}>
                          When{" "}
                          {data.triggers.length
                            ?
                            // for dynamic name
                            // rule.triggers
                            // .map((trigger) => getAutomationTriggerName(trigger, fields))
                            // .join(" and ")
                            // for static name
                            getAutomationTriggerName(data.triggers[0], fields)
                            : "this trigger happens"}
                        </div>
                        <IconArrowForward
                          style={{ transform: " rotate(90deg)", margin: "-3px 7" }}
                        />
                        <div style={{ color: data.actions.length ? "#161717" : "#7E7E7E" }}>
                          then{" "}
                          {data.actions.length
                            ? data.actions
                              .map((action) => getAutomationActionName(action))
                              .join(" and ")
                            : "do this action"}
                        </div>
                      </div>
                    )}
                  </CustomTooltip>
                </div>
              )}
              {editting && (
                <>
                  <TextField
                    inputProps={{ maxLength: 250 }}
                    classes={classes.triggerInput}
                    value={data.name}
                    onChange={({ target: { value } }) => setData((prev) => ({ ...prev, name: value?.slice(0, 250) ?? undefined }))}
                    innerRef={input}
                    formControlStyles={{ margin: 0 }}
                    fullWidth
                    error={false}
                  />
                </>
              )}
            </div>
          </ClickAwayListener>
          <div>
            <div className={classes.headerBtn}>
              <CustomButton
                style={{ marginRight: "10px", textTransform: "none", fontSize: "12px" }}
                onClick={OnCancel}>
                Cancel
              </CustomButton>
              <CustomButton
                style={{ marginBottom: 0, whiteSpace: "nowrap" }}
                btnType="success"
                variant="contained"
                query={submitted ? "progress" : ""}
                disabled={submitted}
                onClick={() => {
                  if (data.triggers.filter(Boolean).length && data.actions.filter(Boolean).length) {
                    setSubmitted(true);
                    OnCreate(
                      {
                        ...data,
                        triggers: data.triggers.filter(Boolean),
                        actions: data.actions.filter(Boolean)
                      },
                      (success) => {
                        showSnackBar(success, "success");
                      },
                      (failure) => {
                        showSnackBar(failure, "error");
                      }
                    );
                  } else {
                    showSnackBar("Please Select atleast one trigger and action", "error");
                    setSubmitted(false);
                  }
                }}>
                {data._id ? "Update" : "Create"} Rule
              </CustomButton>
            </div>
          </div>
        </div>
        <div className={classes.form}>
          <div className={classes.wptotalbody}>
            <div className={classes.wpbackline} />
            <WorkspaceCard
              onClick={() =>
                setView({
                  name: "WorkspaceForm",
                  data: { type: data.type, workspaceId: data.workspaceId },
                })
              }
              active={view.name === "WorkspaceForm"}
              workspace={{ type: data.type, workspaceId: data.workspaceId }}
            />
            <div className={classes.wpbody}>
              {data.triggers.length ? (
                data.triggers.map((trigger, index) => {
                  return trigger ? (
                    <>
                      <TriggerCardView
                        key={trigger.id ?? index}
                        active={view.name === "TriggerForm" && view?.data?.id === trigger.id}
                        trigger={trigger}
                        onDelete={(e) => {
                          e.stopPropagation();
                          setView({ name: undefined, data: undefined });
                          setData({
                            ...data,
                            triggers: data.triggers.filter((item) => item?.id !== trigger.id) ?? [],
                          });
                        }}
                        onClick={() => setView({ name: "TriggerForm", data: trigger })}
                      />
                      {data.triggers.length > 1 && index < data.triggers.length - 1 && (
                        <CustomSelect
                          showCheck={false}
                          bgapply={false}
                          style={{
                            margin: "10px 0",
                            justifyContent: "space-between",
                            backgroundColor: "white",
                            display: "flex",
                            border: "1px solid #EAEAEA",
                          }}
                          value={data.operator}
                          onChange={(option) =>
                            setData((prev) => ({ ...prev, operator: option.value }))
                          }
                          height="100px"
                          width={250}
                          options={operatorOptions}
                          button={
                            <CustomButton
                              btnType="transparent"
                              style={{
                                fontSize: "11px",
                                textTransform: "none",
                                border: "1px solid #EAEAEA",
                                backgroundColor: "white",
                                width: "100%",
                                display: "flex",
                                justifyContent: "space-between",
                                marginTop: "5px",
                              }}
                            />
                          }
                        />
                      )}
                    </>
                  ) : (
                    <TriggerEmpty
                      key={index}
                      active={view.name === "TriggerForm" && view?.data === undefined}
                      onClick={() => setView({ name: "TriggerForm", data: undefined })}
                      onDelete={(e) => {
                        e.stopPropagation();
                        setView({ name: undefined, data: undefined });
                        setData({
                          ...data,
                          triggers: data.triggers.filter(Boolean),
                        });
                      }}
                    />
                  );
                })
              ) : (
                <TriggerEmpty
                  active={view.name === "TriggerForm" && view?.data === undefined}
                  onClick={() => setView({ name: "TriggerForm", data: undefined })}
                  onDelete={(e) => {
                    e.stopPropagation();
                    setView({ name: undefined, data: undefined });
                    setData({
                      ...data,
                      triggers: data.triggers.filter(Boolean),
                    });
                  }}
                />
              )}
            </div>
            {!!data.triggers.length &&
              !data.triggers.some((val) => val === undefined) &&
              data.triggers.length < 3 && (
                <div
                  style={{ position: "relative", marginTop: data.triggers.length ? "70px" : "" }}>
                  <div
                    className={classes.wpcard}
                    onClick={() =>
                      setData({ ...data, triggers: [...(data.triggers ?? []), undefined] }) ||
                      setView({ name: "TriggerForm", data: undefined })
                    }>
                    <div className={classes.wpcardicon}>
                      <div>
                        <IconAddWorkspace />
                      </div>
                      <Typography className={classes.anotherTrigText}>
                        Add another trigger
                      </Typography>
                    </div>
                  </div>
                </div>
              )}
            <div className={classes.actioncard}>
              {!!data.actions.length ? (
                data.actions.map((action, index) =>
                  action ? (
                    <ActionCard
                      key={action.id ?? index}
                      active={view.name === "ActionForm" && view?.data?.id === action.id}
                      action={action}
                      onDelete={(e) => {
                        e.stopPropagation();
                        setView({ name: undefined, data: undefined });
                        setData({
                          ...data,
                          actions: data.actions.filter((item) => item?.id !== action.id) ?? [],
                        });
                      }}
                      onClick={() => setView({ name: "ActionForm", data: action })}
                    />
                  ) : (
                    <ActionEmpty
                      key={index}
                      active={view.name === "ActionForm" && view?.data === undefined}
                      onClick={() => setView({ name: "ActionForm", data: undefined })}
                      onDelete={(e) => {
                        e.stopPropagation();
                        setView({ name: undefined, data: undefined });
                        setData({
                          ...data,
                          actions: data.actions.filter(Boolean),
                        });
                      }}
                    />
                  )
                )
              ) : (
                <ActionEmpty
                  active={view.name === "ActionForm" && view?.data === undefined}
                  onClick={() => setView({ name: "ActionForm", data: undefined })}
                  onDelete={(e) => {
                    e.stopPropagation();
                    setView({ name: undefined, data: undefined });
                    setData({
                      ...data,
                      actions: data.actions.filter(Boolean),
                    });
                  }}
                />
              )}
            </div>

            {/* <div className={classes.addAnother}>
              {!!data.actions.length && !data.actions.some((val) => val === undefined) && (
                <div onClick={() => setData({ ...data, actions: [...data.actions, undefined] })}>
                  <div className={classes.addAnotherIcon}>
                    <div>
                      <IconAddAction />
                    </div>
                    <Typography style={{ marginLeft: 10 }}>Add another action</Typography>
                  </div>
                </div>
              )}
            </div> */}
          </div>

          <div
            style={{
              padding: 20,
              width: "40%",
              ...(view.name && { borderLeft: "1px solid #ddd ", height: "100%" }),
            }}>
            {view.name === "WorkspaceForm" && (
              <WorkspaceFormView
                data={view.data}
                onChange={(workspace) => setData({ ...data, ...workspace })}
              />
            )}
            {view.name === "TriggerForm" && (
              <TriggerForm
                data={view.data}
                onChange={(val) => {
                  const newval = { id: val.id ?? data.triggers.length, ...val };

                  setData({
                    ...data,
                    triggers:
                      val.id !== undefined
                        ? data.triggers.map((item) => (item?.id !== val.id ? item : newval))
                        : [...data.triggers.filter(Boolean), newval],
                  });

                  setView({ ...view, data: newval });
                }}
              />
            )}
            {view.name === "ActionForm" && (
              <ActionForm
                data={view.data}
                onChange={(val) => {
                  const newval = { id: val.id ?? data.actions.length, ...val };

                  setData({
                    ...data,
                    actions:
                      val.id !== undefined
                        ? data.actions.map((item) => (item?.id !== val.id ? item : newval))
                        : [...data.actions.filter(Boolean), newval],
                  });

                  setView({ ...view, data: newval });
                }}
              />
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default compose(withSnackbar, withStyles(style, { withTheme: true }))(AutomationRuleForm);
