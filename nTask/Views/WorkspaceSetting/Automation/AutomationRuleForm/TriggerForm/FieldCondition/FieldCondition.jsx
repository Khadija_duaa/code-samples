import React, { useEffect, useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import { IconButton, Typography } from "@material-ui/core";
import CustomSelect from "../../../../../../components/Dropdown/CustomSelect/CustomSelect.view";
import DefaultCheckbox from "../../../../../../components/Form/Checkbox";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import style from "./FieldCondition.style";
import IconDropdownfieldchanges from "../../../../../../components/Icons/IconDropdownfieldchanges";
import IconPeoplefieldchanges from "../../../../../../components/Icons/IconPeoplefieldchanges";

const fieldchanges = [
  {
    label: "Required Design Formats",
    value: "Required Design Formats",
    description: (
      <Typography style={{ fontSize: "14px", color: "#7E7E7E" }}>usiness Development</Typography>
    ),
    icon: <IconDropdownfieldchanges />,
  },
  {
    label: "Design Dimension",
    value: "Design Dimension",
    description: (
      <Typography style={{ fontSize: "14px", color: "#7E7E7E" }}>usiness Development</Typography>
    ),
    icon: <IconDropdownfieldchanges />,
  },
  {
    label: "Inspection Site",
    value: "Inspection Site",
    description: (
      <Typography style={{ fontSize: "14px", color: "#7E7E7E" }}>usiness Development</Typography>
    ),
    icon: <IconDropdownfieldchanges />,
  },
  {
    label: "Responsible Person",
    value: "Responsible Person",
    description: (
      <Typography style={{ fontSize: "14px", color: "#7E7E7E" }}>usiness Development</Typography>
    ),
    icon: <IconPeoplefieldchanges />,
  },
];

const people = ["Any Value", "Frank Lewis", "Diana Smiley", "Oscar Thomsen", "Matthew Walsh"];

const FieldCondition = (props) => {
  const { onChange, data, onBack, classes } = props;

  const [rule, setRule] = useState(
    data?.type === "field"
      ? data
      : {
          ...(data?.id !== undefined && { id: data.id }),
          type: "field",
          field: undefined,
          value: undefined,
        }
  );

  useEffect(() => {
    setRule(
      data?.type === "field"
        ? data
        : {
            ...(data?.id !== undefined && { id: data.id }),
            type: "field",
            field: undefined,
            value: undefined,
          }
    );
  }, [data]);

  const updateRule = (value) => {
    setRule(value);
    onChange(value);
  };

  return (
    <div>
      <Typography className={classes.field_change_text}>
        <IconButton style={{ padding: 0 }} onClick={onBack}>
          <ChevronLeftIcon style={{ color: "black" }} />
        </IconButton>
        Field changes
      </Typography>
      <Typography>Select Field</Typography>
      <CustomSelect
        showCheck={false}
        bgapply={false}
        style={{
          width: 250,
          justifyContent: "space-between",
          display: "flex",
          backgroundColor: "white",
          border: "1px solid #EAEAEA",
          justifyContent: "space-between",
          marginTop: "5px",
        }}
        value={rule.field}
        onChange={(option) =>
          updateRule({
            ...rule,
            field: option.value,
            ...(rule.field === "Responsible Person" && { value: [] }),
          })
        }
        width={250}
        options={fieldchanges}
      />
      <div className={classes.rule_field}>
        {rule.field === "Responsible Person" &&
          people.map((value) => (
            <label className={classes.value_label}>
              <DefaultCheckbox
                checked={rule.value?.includes(value)}
                onChange={({ target: { name } }, value) =>
                  updateRule({
                    ...rule,
                    value: value
                      ? [...(rule.value ?? []), name]
                      : rule.value.filter((item) => item !== name),
                  })
                }
                checkboxStyles={{ padding: 0, marginRight: 8 }}
                name={value}
              />
              <Typography>{value}</Typography>
            </label>
          ))}
      </div>
    </div>
  );
};

export default withStyles(style, { withTheme: true })(FieldCondition);
