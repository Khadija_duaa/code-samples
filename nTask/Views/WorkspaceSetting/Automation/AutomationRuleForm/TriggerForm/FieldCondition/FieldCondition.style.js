const Style = (theme) => ({
    field_change_text: {
        display: "flex",
        alignItems: "center",
        fontSize: "16px !important",
        fontWeight: "bold",
        color: "black",
        marginBottom:10,
    },

    rule_field: {
        marginTop: "1rem",
        marginBottom: "5px"
    },
    value_label: {
        display: "flex",
        alignItems: "center",
        marginTop: "5px"
    }
});
export default Style;