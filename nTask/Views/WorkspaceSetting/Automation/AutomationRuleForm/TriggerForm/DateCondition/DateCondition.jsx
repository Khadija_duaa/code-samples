import React, { useEffect, useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import CustomSelect from "../../../../../../components/Dropdown/CustomSelect/CustomSelect.view";
import { Typography, IconButton } from "@material-ui/core";
import CustomButton from "../../../../../../components/Buttons/CustomButton";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import { useSelector } from "react-redux";
import style from "./DateCondition.style";

const TimeOptions = [
  {
    label: "Minutes",
    value: "minutes",
  },
  {
    label: "Hours",
    value: "hours",
  },
  {
    label: "Days",
    value: "days",
  },
  {
    label: "Weeks",
    value: "weeks",
  },
];

const approachingOptions = [
  {
    label: "Due Today",
    value: { durationType: "days", durationValue: 0, isCustom: false },
  },
  {
    label: "1 day before",
    value: { durationType: "days", durationValue: 1, isCustom: false },
  },
  {
    label: "3 Days before",
    value: { durationType: "days", durationValue: 3, isCustom: false },
  },
  {
    label: "5 Days before",
    value: { durationType: "days", durationValue: 5, isCustom: false },
  },
  {
    label: "Custom",
    value: { durationType: "days", durationValue: 1, isCustom: true },
  },
];

const overdueOptions = [
  {
    label: "1 day after",
    value: { durationType: "days", durationValue: 1, isCustom: false },
  },
  {
    label: "3 Days after",
    value: { durationType: "days", durationValue: 3, isCustom: false },
  },
  {
    label: "5 Days after",
    value: { durationType: "days", durationValue: 5, isCustom: false },
  },
  {
    label: "Custom",
    value: { durationType: "days", durationValue: 1, isCustom: true },
  },
];

export const Condition = {
  approaching: "approaches",
  overdue: "overdue",
};

export const DateCondition = (props) => {
  const { onChange, data, onBack, classes } = props;

  const featurefield = useSelector((store) => store.featureAccessPermissions);
  const fields = Object.values(featurefield.task).filter((field) => field.fieldType === "date" && !field.isHidden);

  const [rule, setRule] = useState(
    data?.type === "date"
      ? data
      : {
          ...(data?.id !== undefined && { id: data.id }),
          type: "date",
          fieldType: fields[0].fieldKey,
          condition: Condition.approaching,
          ...approachingOptions[0].value,
          isCustom: false,
        }
  );

  useEffect(() => {
    setRule(
      data?.type === "date"
        ? data
        : {
            ...(data?.id !== undefined && { id: data.id }),
            type: "date",
            fieldType: fields[0].fieldKey,
            condition: Condition.approaching,
            ...approachingOptions[0].value,
            isCustom: false,
          }
    );
  }, [data]);

  const updateRule = (value) => {
    setRule(value);
    onChange(value);
  };

  return (
    <div>
      <Typography className={classes.dateTextAndIcon}>
        <IconButton style={{ padding: 0 }} onClick={onBack}>
          <ChevronLeftIcon className={classes.dateIcon} />
        </IconButton>
        Date
      </Typography>
      <Typography className={classes.fieldText}>Field</Typography>
      <CustomSelect
        showCheck={false}
        bgapply={false}
        style={{
          margin: "10px 0",
          width: 250,
          justifyContent: "space-between",
          backgroundColor: "white",
          display: "flex",
          border: "1px solid #EAEAEA",
        }}
        options={fields.map((field) => ({ label: field.fieldName, value: field.fieldKey }))}
        onChange={(option) => updateRule({ ...rule, fieldType: option.value })}
        value={rule.fieldType}
        button={
          <CustomButton
            btnType="transparent"
            style={{
              fontSize: "11px",
              textTransform: "none",
              border: "1px solid #EAEAEA",
              backgroundColor: "white",
              width: "100%",
              display: "flex",
              justifyContent: "space-between",
              marginTop: "5px",
            }}
          />
        }
      />

      <Typography className={classes.ConditionText}>Condition</Typography>
      <div className={classes.connditionPerrent}>
        <CustomButton
          style={{
            width: 125,
            textTransform: "lowercase",
            borderRadius: "5px 0px 0px 5px",
            backgroundColor: rule.condition === Condition.approaching ? "#00ABED" : "white",
            border: "1px solid #F1F1F1",
          }}
          onClick={() =>
            updateRule({
              ...rule,
              condition: Condition.approaching,
              ...approachingOptions[0].value,
            })
          }>
          <Typography
            style={{ color: rule.condition === Condition.approaching ? "white" : "black", fontSize: "12px" }}>
            is approaching
          </Typography>
        </CustomButton>
        <CustomButton
          style={{
            width: 125,
            padding: "0 10px",
            textTransform: "lowercase",
            borderRadius: "0px 5px 5px 0px",
            backgroundColor: rule.condition === Condition.overdue ? "#00ABED" : "white",
            border: "1px solid #F1F1F1",
            marginLeft: "-1px",
          }}
          onClick={() =>
            updateRule({
              ...rule,
              condition: Condition.overdue,
              ...overdueOptions[0].value,
            })
          }>
          <Typography style={{ color: rule.condition === Condition.overdue ? "white" : "black", fontSize: "12px" }}>
            is overdue
          </Typography>
        </CustomButton>
      </div>

      {rule.condition === Condition.approaching ? (
        <CustomSelect
          showCheck={false}
          bgapply={false}
          style={{
            backgroundColor: "white",
            width: 250,
            display: "flex",
            justifyContent: "space-between",
            marginTop: 10,
            border: "1px solid #EAEAEA",
          }}
          value={
            rule.isCustom
              ? approachingOptions.at(-1).value
              : approachingOptions.find(
                  (option) =>
                    option.value.durationValue === rule.durationValue &&
                    option.value.durationType === rule.durationType
                ).value ?? approachingOptions.at(-1).value
          }
          options={approachingOptions}
          onChange={(option) => updateRule({ ...rule, ...option.value })}
        />
      ) : (
        <CustomSelect
          showCheck={false}
          bgapply={false}
          style={{
            backgroundColor: "white",
            width: 250,
            display: "flex",
            justifyContent: "space-between",
            marginTop: 10,
            border: "1px solid #EAEAEA",
          }}
          value={
            rule.isCustom
              ? overdueOptions.at(-1).value
              : overdueOptions.find(
                  (option) =>
                    option.value.durationValue === rule.durationValue &&
                    option.value.durationType === rule.durationType
                ).value ?? overdueOptions.at(-1).value
          }
          options={overdueOptions}
          onChange={(option) => updateRule({ ...rule, ...option.value })}
        />
      )}

      {rule.isCustom && (
        <div className={classes.customInputs}>
          <input
            className={classes.numberInput}
            value={rule.durationValue}
            onChange={({ target: { value } }) =>
              updateRule({ ...rule, durationValue: value.slice(0, 4) })
            }
            min={1}
            type="number"
          />
          <CustomSelect
            showCheck={false}
            bgapply={false}
            style={{
              marginLeft: 10,
              width: 177,
              display: "flex",
              justifyContent: "space-between",
              backgroundColor: "white",
              border: "1px solid #EAEAEA",
            }}
            value={rule.durationType}
            options={TimeOptions}
            onChange={(option) => updateRule({ ...rule, durationType: option.value })}
          />
        </div>
      )}
    </div>
  );
};

export default withStyles(style, { withTheme: true })(DateCondition);
