
const Style = (theme) => ({
    dateTextAndIcon: {
        display: "flex",
        alignItems: "center",
        fontSize: "16px !important",
        color: "black",
        fontWeight: "bold",
    },
    dateIcon: {
        color: "black"
    },
    fieldText: {
        marginTop: "1rem",
        marginBottom: "5px"
    },
    ConditionText: {
        marginTop: "1rem",
        marginBottom: "5px"
    },
    connditionPerrent: {
        width: "100%",
        display: "flex"
    },
    customInputs: {
        display: "flex",
        marginTop: 10
    },
    numberInput: {
        fontSize: "13px !important",
        width: 80,
        marginRight: 10,
        maxWidth: 60,
        borderRadius: 4,
        textTransform: "none",
        padding: "8.5px 4px",
        margin: "0 2px",
        border: "1px solid #EAEAEA",
        textAlign: "center",
    }

});
export default Style;