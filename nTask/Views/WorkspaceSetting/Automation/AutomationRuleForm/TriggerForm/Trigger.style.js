const Style = (theme) => ({
    bodycard: {
        marginRight: 15,
        padding: "10px",
        borderRadius: "5px",
        width: "40px",
        height: "40px",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#E6F7FD",
        border: "2px #00ABED solid ",
    },
    dell_icon: {
        alignSelf: "stretch",
        padding: "0 10px",
        display: "none",
        alignItems: "center",
        justifyContent: "center",
    },
    empty_trigger_card: {
        marginRight: 15,
        padding: "10px",
        borderRadius: "5px",
        width: "40px",
        height: "40px",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#E6F7FD",
        border: "2px #00ABED dashed ",
    },
    trigger_text: {
        fontSize: "16px !important",
        fontWeight: "bold",
        color: "black",
        fontFamily: theme.typography.fontFamilyLato,
    },
    trigger_form: {

        display: "flex",
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        cursor: "pointer",
    },
    triger_form_icon: {
        marginRight: 15,
        padding: "5px",
        borderRadius: "5px",
        width: 40,
        height: 40,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#E6F7FD",
    },
    date_meet_text: {
        fontSize: "14px !important",
        color: "black",
        fontFamily: theme.typography.fontFamilyLato,

    },
    letUsanchor: {
        color: "#76C3FF",
        cursor: "pointer"
    },
    statusChangeText: {
        fontSize: "14px !important",
        color: "black"
    },
    statusChangeTextIcon: {
        marginRight: 15,
        padding: "10px",
        borderRadius: "5px",
        width: 32,
        height: 32,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#E6F7FD",
    },
    CardText: {
        display: "flex", alignItems: "center", marginTop: "10px"
    },
    descriptionFooter: {
        borderTop: "1px solid #e0dcdc",
        width: "100%",
        display: "flex",
        justifyContent: "end",
        backgroundColor: "#F6F6F6",
        padding: "20px",
    },
    triggersFooterText: {
        marginTop: "1rem", color: "black"
    },
    fieldchangesText: {
        fontSize: "14px !important", color: "black"
    },

    Card: {
        marginTop: "10px"
    },
    container: {
        backgroundColor: "white",
        marginBottom: 10,
        borderRadius: 5,
        border: "1px solid #ddd",
        padding: 10,
        display: "flex",
        alignItems: "center",
        justifyContent: "space-between",
        cursor: "pointer",
        "&:hover": {
            borderColor: "#BFBFBF",

            "& .icondelete": {
                display: "flex"
            }
        }
    },
    tiggerText: {
        color: "#7E7E7E",
        fontFamily: theme.typography.fontFamilyLato,
        fontSize: "14px !important"

    },
    conditionName: {
        fontWeight: "bold",
        color: "#161717",
        fontFamily: theme.typography.fontFamilyLato,
        fontSize: "14px !important"
    },
    datePosition: {
        fontSize: "14px !important",
        fontFamily: theme.typography.fontFamilyLato,
    }
});
export default Style;
