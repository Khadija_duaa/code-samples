import React, { useState } from "react";
import { Typography } from "@material-ui/core";
import { Condition } from "./DateCondition/DateCondition";
import iconDelete from "../../../../../assets/images/automation/iconDelete.svg";
import IconDateWorkSpace from "../../../../../components/Icons/IconDateWorkSpace";
import { useSelector } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import style from "./Trigger.style";

const TriggerCard = (props) => {
  const { onClick, trigger, active, onDelete, classes } = props;

  const featurefield = useSelector((store) => store.featureAccessPermissions);
  const fields = Object.values(featurefield.task).filter((field) => field.fieldType === "date");

  return (
    <div
      className={classes.container}
      style={{
        ...(active && { borderColor: "#00ABED" }),
      }}
      onClick={onClick}>
      <div className={classes.bodycard}>
        <IconDateWorkSpace />
      </div>
      <div style={{ flex: 1 }}>
        {trigger.type === "date" && (
          <>
            <Typography color="inherit" className={classes.conditionName}>
              When {fields.find((field) => trigger.fieldType === field.fieldKey)?.fieldName}{" "}
              {trigger.condition}
            </Typography>
            <Typography className={classes.datePosition}>
              {trigger.durationValue === 0
                ? "Due Today"
                : `${trigger.durationValue} ${trigger.durationType} ${
                    trigger.condition === Condition.approaching ? "before" : "after"
                  }
              `}
            </Typography>
          </>
        )}
        {trigger.type === "status" && (
          <>
            <Typography color="inherit" className={classes.conditionName}>
              When status changes to
            </Typography>
            <Typography>
              {trigger.checked?.join(", ").length < 60
                ? trigger.checked?.join(", ")
                : trigger.checked?.slice(0, 3)?.join(", ") +
                  `, ${trigger.checked?.length - 3} more`}
            </Typography>
          </>
        )}
        {trigger.type === "field" && (
          <>
            <Typography color="inherit" style={{ fontWeight: "bold" }}>
              When {trigger.field} Changes to
            </Typography>
            <Typography>
              {Array.isArray(trigger.value)
                ? trigger.value?.join(", ").length < 60
                  ? trigger.value?.join(", ")
                  : trigger.value?.slice(0, 3)?.join(", ") + `, ${trigger.value?.length - 3} more`
                : trigger.value}
            </Typography>
          </>
        )}
      </div>
      <div onClick={onDelete} className={`icondelete ${classes.dell_icon}`}>
        <img src={iconDelete} />
      </div>
    </div>
  );
};
export default withStyles(style, { withTheme: true })(TriggerCard);
