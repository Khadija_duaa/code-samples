import React, { useState } from "react";
import { Typography, TextField, Divider } from "@material-ui/core";
import CustomButton from "../../../../../components/Buttons/CustomButton";
import CustomDialog from "../../../../../components/Dialog/CustomDialog";
import DateCondition from "./DateCondition/DateCondition";
import StatusCondition from "./StatusCondition/StatusCondition";
import FieldCondition from "./FieldCondition/FieldCondition";
import IconDateWorkSpace from "../../../../../components/Icons/IconDateWorkSpace";
import IconStatusWorkspace from "../../../../../components/Icons/IconStatusWorkspace";
import IconFieldstatus from "../../../../../components/Icons/IconFieldstatus";
import { withStyles } from "@material-ui/core/styles";
import style from "./Trigger.style";

const TriggerForm = (props) => {
  const { onChange, data, classes } = props;

  const [open, setOpen] = useState(false);
  const [type, setType] = useState(data?.type ?? "");

  if (type === "date")
    return <DateCondition data={data} onChange={onChange} onBack={() => setType("")} />;

  if (type === "status")
    return <StatusCondition data={data} onChange={onChange} onBack={() => setType("")} />;

  if (type === "field")
    return <FieldCondition data={data} onChange={onChange} onBack={() => setType("")} />;

  return (
    <div>
      <Typography className={classes.trigger_text}>Trigger</Typography>
      <div>
        <div className={classes.trigger_form} onClick={() => setType("date")}>
          <div className={classes.triger_form_icon}>
            <IconDateWorkSpace />
          </div>
          <Typography className={classes.date_meet_text}>Date meets a condition</Typography>
        </div>
        {/* <div onClick={() => setType("status")} className={classes.CardText}>
          <div className={classes.statusChangeTextIcon}>
            <IconStatusWorkspace />
          </div>
          <Typography className={classes.statusChangeText}>Status changes</Typography>
        </div>
        <div onClick={() => setType("field")} className={classes.CardText}>
          <div className={classes.statusChangeTextIcon}>
            <IconFieldstatus />
          </div>
          <Typography className={classes.fieldchangesText}>Field changes</Typography>
        </div> */}
        {/* <Divider light style={{ marginTop: "1rem" }} />
        <Typography className={classes.triggersFooterText}>
          Do you have any ideas for new triggers ?
        </Typography>
        <Typography className={classes.letUsanchor} onClick={() => setOpen(true)}>
          Let Us Know
        </Typography> */}
      </div>

      <CustomDialog
        title="Description your action idea"
        dialogProps={{
          open: open,
          onClick: (e) => e.stopPropagation(),
          onClose: () => setOpen(false),
          PaperProps: { style: { maxWidth: 500 } },
        }}>
        <div style={{ padding: "20px" }}>
          <TextField variant="outlined" fullWidth multiline rows={5} />
        </div>

        <div className={classes.descriptionFooter}>
          <CustomButton onClick={() => setOpen(false)} style={{ marginRight: "10px" }}>
            Cancel
          </CustomButton>
          <CustomButton btnType="success" variant="contained">
            Send
          </CustomButton>
        </div>
      </CustomDialog>
    </div>
  );
};
export default withStyles(style, { withTheme: true })(TriggerForm);
