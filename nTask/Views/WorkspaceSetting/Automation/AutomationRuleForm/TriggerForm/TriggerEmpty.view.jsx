import React from "react";
import { Typography } from "@material-ui/core";
import style from "./Trigger.style";
import { withStyles } from "@material-ui/core/styles";
import iconDelete from "../../../../../assets/images/automation/iconDelete.svg";

const TriggerEmpty = (props) => {
  const { onClick, onDelete, active, classes } = props;

  return (
    <div
      className={classes.container}
      style={{ border: `1px solid ${active ? "#00ABED" : "#ddd"}` }}
      onClick={onClick}>
      <div style={{ display: "flex", alignItems: "center" }}>
        <div className={classes.empty_trigger_card}>&nbsp;</div>
        <Typography className={classes.tiggerText}>Select a trigger</Typography>
      </div>
      <div onClick={onDelete} className={`icondelete ${classes.dell_icon}`}>
        <img src={iconDelete} />
      </div>
    </div>
  );
};
export default withStyles(style, { withTheme: true })(TriggerEmpty);
