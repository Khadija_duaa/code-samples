import React, { useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { Typography, IconButton } from "@material-ui/core";
import DefaultCheckbox from "../../../../../../components/Form/Checkbox";
import { useState } from "react";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import style from "./StatusCondition.style";

const StatusCondition = (props) => {
  const { onChange, data, onBack, classes } = props;

  const [rule, setRule] = useState(
    data?.type === "status"
      ? data
      : {
          ...(data?.id !== undefined && { id: data.id }),
          type: "status",
          checked: [],
        }
  );

  useEffect(() => {
    setRule(
      data?.type === "status"
        ? data
        : {
            ...(data?.id !== undefined && { id: data.id }),
            type: "status",
            checked: [],
          }
    );
  }, [data]);

  const updateRule = (value) => {
    setRule(value);
    onChange(value);
  };

  return (
    <div>
      <Typography className={classes.status_change_text}>
        <IconButton style={{ padding: 0 }} onClick={onBack}>
          <ChevronLeftIcon style={{ color: "black" }} />
        </IconButton>
        Status changes to
      </Typography>

        <Typography style={{marginBottom:"5px"}}>ntask Standard</Typography>
        {["Not Standard", "In Progress", "In Reviews", "Completed", "Cancelled"].map((value) => (
          <label className={classes.text_box_label}>
            <DefaultCheckbox
              checked={rule.checked.includes(value)}
              onChange={({ target: { name } }, value) =>
                updateRule({
                  ...rule,
                  checked: value
                    ? [...rule.checked, name]
                    : rule.checked.filter((item) => item !== name),
                })
              }
              checkboxStyles={{ padding: 0, marginRight: 8 }}
              name={value}
            />
            <Typography className={classes.value_text}>{value}</Typography>
          </label>
        ))}
        <Typography className={classes.Facilities_text}>Facilities Management</Typography>
        {[
          "Pending Alloction",
          "On-Site Inspection",
          "In Progress",
          "To be Schedule",
          "Work Completed",
        ].map((value) => (
          <label className={classes.text_box_label}>
            <DefaultCheckbox
              checked={rule.checked.includes(value)}
              onChange={({ target: { name } }, value) =>
                updateRule({
                  ...rule,
                  checked: value
                    ? [...rule.checked, name]
                    : rule.checked.filter((item) => item !== name),
                })
              }
              checkboxStyles={{ padding: 0, marginRight: 8 }}
              name={value}
            />
            <Typography className={classes.value_text}>{value}</Typography>
          </label>
        ))}
        <Typography className={classes.Facilities_text}>Sprint Retrospectives</Typography>
        {["Went Well", "Requiment Changes", "Action Items"].map((value) => (
          <label className={classes.text_box_label}>
            <DefaultCheckbox
              checked={rule.checked.includes(value)}
              onChange={({ target: { name } }, value) =>
                updateRule({
                  ...rule,
                  checked: value
                    ? [...rule.checked, name]
                    : rule.checked.filter((item) => item !== name),
                })
              }
              checkboxStyles={{ padding: 0, marginRight: 8 }}
              name={value}
            />
            <Typography className={classes.value_text}>{value}</Typography>
          </label>
        ))}
      </div>
  );
};

export default withStyles(style, { withTheme: true })(StatusCondition);
