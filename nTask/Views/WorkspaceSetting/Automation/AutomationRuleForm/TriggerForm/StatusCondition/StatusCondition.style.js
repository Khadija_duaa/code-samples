const Style = (theme) => ({
    status_change_text: {
        display: "flex",
        alignItems: "center",
        fontWeight: "bold",
        color: "black",
        fontSize: "16px !important",
        marginBottom:10,
    },
    Ntask_text: {
        marginTop: "1rem",
        marginBottom: "5px",
        fontSize: "13px !important"
    },
    value_text: {
        fontSize: "14px !important"
    },
    Facilities_text: {
        marginTop: 10,
        marginBottom: "5px",
        fontSize: "13px !important"
    },
    text_box_label: {
        display: "flex",
        alignItems: "center",
        marginTop:"5px"
    }
});
export default Style;