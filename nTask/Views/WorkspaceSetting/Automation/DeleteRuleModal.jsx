import { withStyles } from "@material-ui/core/styles";
import React, { useState } from "react";
import Style from "./Automation.style";

import { Typography } from "@material-ui/core";
import CustomButton from "../../../components/Buttons/CustomButton";
import CustomDialog from "../../../components/Dialog/CustomDialog";
import IllustrationDeleteItem from "../../../components/Icons/IllustrationDeleteItem";
import { deleteAutomationRule } from "../../../redux/actions/team";

const DeleteRuleModal = (props) => {
  const { classes, onDelete, onCancel, rule } = props;

  const [loading, setLoading] = useState(false);

  return (
    <CustomDialog
      title="Delete Rule"
      dialogProps={{
        open: !!rule,
        onClose: onCancel,
        PaperProps: { style: { maxWidth: 500 } },
      }}>
      <div>
        <div className={classes.confirmdellicon}>
          <IllustrationDeleteItem />
        </div>
        <Typography className={classes.confirmdellicontext}>
          Are you sure you want to delete this rule?
        </Typography>
      </div>
      <div className={classes.custombtnmodel}>
        <CustomButton
          style={{ marginRight: 20, }}
          btnType="plain"
          variant="text"
          onClick={onCancel}>
          Cancel
        </CustomButton>
        <CustomButton
          query={loading ? "progress" : undefined}
          btnType="danger"
          variant="contained"
          onClick={() => {
            setLoading(true);
            deleteAutomationRule(rule?._id, () => {
              setLoading(false);
              onDelete?.(rule);
            });
          }}>
          Delete Rule
        </CustomButton>
      </div>
    </CustomDialog>
  );
};

export default withStyles(Style, { withTheme: true })(DeleteRuleModal);
