import React, { Component, Fragment } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { compose } from "redux";
import Typography from "@material-ui/core/Typography";
import DashIcon from "@material-ui/icons/Remove";
import workspaceSetting from "./styles";
import { withStyles } from "@material-ui/core/styles";
import DefaultTextField from "../../components/Form/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import IconButton from "@material-ui/core/IconButton";
import { injectIntl, FormattedMessage } from "react-intl";

class DeleteWorkspaceDialogContent extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const {
      classes,
      theme,
      step,
      workspaceNameErrorMessage,
      workspaceNameError,
      handleWorkspaceNameInput,
      workspaceName,
      activeWorkspace,
    } = this.props;
    return (
      <div className={classes.deleteWorkspaceDialogContent}>
        {step == 0 ? (
          <div>
            <Typography variant="body2">
              <FormattedMessage
                id="workspace-settings.general-information.delete-workspace.delete-modal.form.delete-workspace-sentence.sentence-a"
                defaultMessage="Are you sure you want to permanently delete this workspace?"
              />
              <br />
            </Typography>
            <Typography variant="body2">
              <FormattedMessage
                id="workspace-settings.general-information.delete-workspace.delete-modal.form.delete-workspace-sentence.sentence-b"
                defaultMessage=" Permanantly deleting your workspace will automatically erase:"
              />
              <br />
            </Typography>
            <ul>
              <li>
                <DashIcon
                  htmlColor={theme.palette.error.light}
                  className={classes.dashIcon}
                />

                <Typography variant="body2">
                  <FormattedMessage
                    id="workspace-settings.general-information.delete-workspace.delete-modal.form.delete-workspace-sentence.sentence-c"
                    defaultMessage="All created and assigned tasks"
                  />
                </Typography>
              </li>
              <li>
                <DashIcon
                  htmlColor={theme.palette.error.light}
                  className={classes.dashIcon}
                />

                <Typography variant="body2">
                  <FormattedMessage
                    id="workspace-settings.general-information.delete-workspace.delete-modal.form.delete-workspace-sentence.sentence-d"
                    defaultMessage="All created and assigned projects"
                  />
                </Typography>
              </li>
              <li>
                <DashIcon
                  htmlColor={theme.palette.error.light}
                  className={classes.dashIcon}
                />

                <Typography variant="body2">
                  <FormattedMessage
                    id="workspace-settings.general-information.delete-workspace.delete-modal.form.delete-workspace-sentence.sentence-e"
                    defaultMessage="All timesheets (pending & approved)"
                  />
                </Typography>
              </li>
              <li>
                <DashIcon
                  htmlColor={theme.palette.error.light}
                  className={classes.dashIcon}
                />

                <Typography variant="body2">
                  {" "}
                  <FormattedMessage
                    id="workspace-settings.general-information.delete-workspace.delete-modal.form.delete-workspace-sentence.sentence-f"
                    defaultMessage="All comments history"
                  />
                </Typography>
              </li>
              <li>
                <DashIcon
                  htmlColor={theme.palette.error.light}
                  className={classes.dashIcon}
                />

                <Typography variant="body2">
                  {" "}
                  <FormattedMessage
                    id="workspace-settings.general-information.delete-workspace.delete-modal.form.delete-workspace-sentence.sentence-g"
                    defaultMessage="All file attachments"
                  />
                </Typography>
              </li>
              <li>
                <DashIcon
                  htmlColor={theme.palette.error.light}
                  className={classes.dashIcon}
                />

                <Typography variant="body2">
                  <FormattedMessage
                    id="workspace-settings.general-information.delete-workspace.delete-modal.form.delete-workspace-sentence.sentence-h"
                    defaultMessage="All meetings"
                  />
                </Typography>
              </li>
              <li>
                <DashIcon
                  htmlColor={theme.palette.error.light}
                  className={classes.dashIcon}
                />

                <Typography variant="body2">
                  <FormattedMessage
                    id="workspace-settings.general-information.delete-workspace.delete-modal.form.delete-workspace-sentence.sentence-i"
                    defaultMessage="All created and assigned issues and risks"
                  />
                </Typography>
              </li>
            </ul>
          </div>
        ) : step == 1 ? (
          <div>
            <Typography variant="body2">
              <b>
                {" "}
                <FormattedMessage
                  id="common.action.delete.confirmation.warning.label"
                  defaultMessage="WARNING:"
                />
              </b>
              {" "}
              <FormattedMessage
                id="workspace-settings.general-information.delete-workspace.delete-modal.form.delete-workspace-sentence.sentence-j"
                defaultMessage="By deleting a workspace, you will permanently lose all your workspace's tasks, projects, meetings, timesheets and files shared. You"
              />
              {" "}
              <b>
                <FormattedMessage
                  id="workspace-settings.general-information.delete-workspace.delete-modal.form.delete-workspace-sentence.sentence-k"
                  defaultMessage="CANNOT"
                />
              </b>{" "}
              <FormattedMessage
                id="workspace-settings.general-information.delete-workspace.delete-modal.form.delete-workspace-sentence.sentence-l"
                defaultMessage="undo this or regain access to your workspace once you proceed."
              />{" "}
              <br />
              <br />
            </Typography>
            <Typography variant="body2">
              <FormattedMessage
                id="workspace-settings.general-information.delete-workspace.delete-modal.form.delete-workspace-sentence.sentence-m"
                defaultMessage="Type"
              />{" "}
              <b>"{activeWorkspace.teamName}"</b>{" "}
              <FormattedMessage
                id="workspace-settings.general-information.delete-workspace.delete-modal.form.delete-workspace-sentence.sentence-n"
                defaultMessage="in the field below to permanently delete your workspace and everything in it."
              />{" "}
            </Typography>

            <DefaultTextField
              label={
                <FormattedMessage
                  id="workspace-settings.general-information.edit-modal.form.workspace-name.label"
                  defaultMessage="Workspace Name"
                />
              }
              fullWidth={true}
              errorState={workspaceNameError}
              errorMessage={workspaceNameErrorMessage}
              defaultProps={{
                id: "workspaceNameInput",
                type: "text",
                onChange: handleWorkspaceNameInput,
                value: workspaceName,
                autoFocus: true,
                placeholder: this.props.intl.formatMessage({
                  id:
                    "workspace-settings.general-information.edit-modal.form.workspace-name.placeholder",
                  defaultMessage: "Enter your workspace name",
                }),
              }}
            />
          </div>
        ) : null}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profileState: state.profile,
  };
};

export default compose(
  withRouter,
  injectIntl,
  connect(mapStateToProps),
  withStyles(workspaceSetting, { withTheme: true })
)(DeleteWorkspaceDialogContent);
