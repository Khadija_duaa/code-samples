import React, { Component } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import CustomDialog from "../../../components/Dialog/CustomDialog";
import dialogStyles from "../../../assets/jss/components/dialog";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import DefaultTextField from "../../../components/Form/TextField";
import CustomButton from "../../../components/Buttons/CustomButton";
import ButtonActionsCnt from "../../../components/Dialog/ConfirmationDialogs/ButtonActionsCnt";
import { RenameRole } from "../../../redux/actions/workspace";
class RenameRoleDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      roleNameError: false,
      roleNameErrorText: "",
      roleName: "Owner",
      create: true
    };
  }

  componentDidUpdate(prevProps) {
    if (this.props.roleData) {
      if (prevProps.roleData.roleName !== this.props.roleData.roleName) {
        this.setState({ roleName: this.props.roleData.roleName });
      }
    }
  }

  handleInput = event => {
    this.setState({ roleName: event.target.value });
  };
  renameRoleData = () => {
    const { roleData } = this.props;
    if (this.state.create) {
      this.setState({ create: false }, () => {
        this.props.RenameRole(
          {
            newRoleName: this.state.roleName,
            roleName: roleData.roleName,
            roleId: roleData.roleId
          },
          success => {
            this.setState({ create: true }, () => {
              if (this.props.onSelect) {
                this.props.onSelect(
                  success.data ? success.data.roleName : "Unknown"
                );
              }
              this.props.closeAction();
            });
          },
          error => {
            this.setState({
              roleNameError: true,
              roleNameErrorText: error ? error.message : "Error",
              create: true
            });
          }
        );
      });
    }
  };
  render() {
    const { classes, theme, open, closeAction, addNewRole } = this.props;
    const { roleNameError, roleNameErrorText, roleName } = this.state;
    return (
      <CustomDialog
        title="Rename"
        dialogProps={{
          open: open,
          onClose: closeAction
        }}
      >
        <div className={classes.dialogContentCnt} style={{ marginBottom: 20 }}>
          <DefaultTextField
            label="Role Name"
            errorState={roleNameError}
            errorMessage={roleNameErrorText}
            defaultProps={{
              id: "roleName",
              value: roleName,
              onChange: this.handleInput
            }}
          />
        </div>

        <ButtonActionsCnt
          cancelAction={closeAction}
          successAction={this.renameRoleData}
          successBtnText="Update"
          cancelBtnText="Cancel"
          btnType="success"
          // btnQuery={btnQuery}
        />
      </CustomDialog>
    );
  }
}

export default compose(
  withRouter,
  withStyles(dialogStyles, { withTheme: true }),
  connect(null, { RenameRole })
)(RenameRoleDialog);
