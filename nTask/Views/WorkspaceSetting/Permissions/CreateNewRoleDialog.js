import React, { Component } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import CustomDialog from "../../../components/Dialog/CustomDialog";
import dialogStyles from "../../../assets/jss/components/dialog";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import DefaultTextField from "../../../components/Form/TextField";
import CustomButton from "../../../components/Buttons/CustomButton";
import ButtonActionsCnt from "../../../components/Dialog/ConfirmationDialogs/ButtonActionsCnt";
import {
  getWorkspaceRoles,
  CreateCustomRole,
} from "../../../redux/actions/workspace";
import { FormattedMessage } from "react-intl";

class CreateNewRoleDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      roleNameError: false,
      roleNameErrorText: "",
      roleName: "",
      create: true,
    };
    this.hadleSubmit = this.hadleSubmit.bind(this);
  }
  hadleSubmit(key) {
    if (key == "newRole") {
      if (this.state.create) {
        this.setState({ create: false }, () => {
          this.props.CreateCustomRole(
            {
              newRoleName: this.state.roleName,
              roleName: "",
              roleId: "",
            },
            (success) => {
              this.setState({ create: true }, () => {
                if (this.props.onSelect && this.props.changeSelectedValue) {
                  // this.props.changeSelectedValue(
                  //   success.data ? success.data.roleName : "Unknown"
                  // );
                  this.props.onSelect(
                    success.data ? success.data.roleName : "Unknown"
                  );
                }
                this.props.closeAction();
              });
            },
            (error) => {
              this.setState({
                roleNameError: true,
                roleNameErrorText: error ? error.data : "Error",
                create: true,
              });
            }
          );
        });
      }
    } else if (key == "duplicateRole") {
      this.props.duplicateRole(this.state.roleName);
    }
  }
  componentDidMount() {
    const { selectedRoleValue, keyValue } = this.props;
    if (selectedRoleValue && keyValue == "duplicateRole") {
      this.setState({ roleName: `Copy of ${selectedRoleValue}` });
    }
  }
  componentDidUpdate(prevProps, prevState) {
    const { selectedRoleValue } = this.props;
    if (
      JSON.stringify(prevProps.selectedRoleValue) !==
      JSON.stringify(selectedRoleValue)
    ) {
      this.setState({ roleName: `Copy of ${selectedRoleValue}` });
    }
  }

  handleInput = (event) => {
    this.setState({ roleName: event.target.value });
  };
  render() {
    const {
      classes,
      theme,
      open,
      closeAction,
      addNewRole,
      successBtnText,
      title,
      keyValue,
      roleNameErrText,
      roleNameErr,
    } = this.props;
    const { roleNameError, roleNameErrorText, roleName } = this.state;
    const errorMessage = roleNameErrText ? roleNameErrText : roleNameErrorText;
    const errState = roleNameErr ? roleNameErr : roleNameError;
    return (
      <CustomDialog
        title={title}
        dialogProps={{
          open: open,
          onClose: closeAction,
        }}
      >
        <div className={classes.dialogContentCnt} style={{ marginBottom: 20 }}>
          <DefaultTextField
            label={
              <FormattedMessage
                id="workspace-settings.roles-and-permission.form.name.label"
                defaultMessage="Role Name"
              />
            }
            errorState={errState}
            errorMessage={errorMessage}
            defaultProps={{
              id: "roleName",
              value: roleName,
              onChange: this.handleInput,
            }}
          />
        </div>

        <ButtonActionsCnt
          cancelAction={closeAction}
          successAction={() => {
            this.hadleSubmit(keyValue);
          }}
          successBtnText={successBtnText}
          cancelBtnText={
            <FormattedMessage
              id="common.action.delete.confirmation.cancel-button.label"
              defaultMessage="Cancel"
            />
          }
          btnType="success"
          disabled={roleName == ""}
          // btnQuery={btnQuery}
        />
      </CustomDialog>
    );
  }
}

export default compose(
  withRouter,
  withStyles(dialogStyles, { withTheme: true }),
  connect(null, { getWorkspaceRoles, CreateCustomRole })
)(CreateNewRoleDialog);
