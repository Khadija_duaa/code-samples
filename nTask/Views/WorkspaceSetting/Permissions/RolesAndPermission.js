// @flow
import React, { Component } from "react";
import Typography from "@material-ui/core/Typography";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import permissionStyles from "./styles";
import data from "../data";
import CustomButton from "../../../components/Buttons/CustomButton";
import AddIcon from "@material-ui/icons/Add";
import DoneIcon from "@material-ui/icons/Done";
import CrossIcon from "@material-ui/icons/Close";
import { duplicateRole, DeleteWorkspaceRole, saveRole } from "../../../redux/actions/workspace";
import {
  getOverAllPermissions,
  getHandledPermissions,
  updatePermissions,
} from "../../../utils/getOverAllPermissions";
import PermissionOptionDropdown from "./PermissionOptionDropdown";
import SelectRoleDropdown from "./SelectRoleDropdown";
import CreateNewRoleDialog from "./CreateNewRoleDialog";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import { ExportProjectsToCSV } from "../../../redux/actions/projects";
import ArrowRight from "@material-ui/icons/KeyboardArrowRight";
import DefaultCheckbox from "../../../components/Form/Checkbox";
import TickIcon from "../../../components/Icons/TickIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import cloneDeep from "lodash/cloneDeep";
import isEqual from "lodash/isEqual";
import forIn from "lodash/forIn";
import DeleteConfirmDialog from "../../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";
import CreateDuplicateRoleDialog from "./CreateNewRoleDialog";
import TaskIcon from "../../../components/Icons/rolesPermissionsIcons/TaskIcon";
import MeetingsIcon from "../../../components/Icons/rolesPermissionsIcons/MeetingIcon";
import ProjectsIcon from "../../../components/Icons/rolesPermissionsIcons/ProjectIcon";
import IssuesIcon from "../../../components/Icons/rolesPermissionsIcons/IssueIcon";
import RiskIcon from "../../../components/Icons/rolesPermissionsIcons/RiskIcon";
import WorkspaceIcon from "../../../components/Icons/rolesPermissionsIcons/WorkspaceIcon";
import TimesheetIcon from "../../../components/Icons/rolesPermissionsIcons/TimesheetIcon";
import IconBoard from "../../../components/Icons/IconBoard";
import FilledIcon from "../../../components/Icons/FilledCircle";
import Badge from "@material-ui/core/Badge";
import permissionData from "./permissionsData";
import { translatePermission } from "../../../utils/permissionTranslation";
import isUndefined from "lodash/isUndefined";
import isNull from "lodash/isNull";
import isString from "lodash/isString";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ContrastIcon from "../../../components/Icons/ContrastIcon";
import ConstantsPlan from "../../../components/constants/planConstant";
import { injectIntl, FormattedMessage } from "react-intl";
import { store } from "../../../index";

let rootNode = "";
type Props = any;
type State = {
  expanded: string,
  roleData: string,
  defaultRoles: Array<mixed>,
  value: string,
  create: boolean,
  permission: string,
  changed: boolean,
  changes: boolean,
  deleteModal: boolean,
  openModal: boolean,
  deleteBtnQuery: string,
  btnQuery: string,
  duplicateRoleDialogOpen: boolean,
  roleNameErrorText: string,
  permissionData: Array<mixed>,
  expandedParent: string,
};

class RolesAndPermission extends Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      expanded: " ",
      roleData: "",
      defaultRoles: [],
      value: "Admin",
      create: true,
      permission: "",
      changed: false,
      changes: false,
      deleteModal: false,
      openModal: false,
      deleteBtnQuery: "",
      btnQuery: "",
      duplicateRoleDialogOpen: false,
      roleNameErrorText: "",
      permissionData: [],
      expandedParent: "",
      discardChanges: false,
    };
    this.initState = "";
  }

  componentDidMount() {
    const { permissionSettingsState } = this.props;
    if (permissionSettingsState.data && permissionSettingsState.data.defaultRoles) {
      const selectedPermission =
        permissionSettingsState.data.defaultRoles.find(x => x.roleName == this.state.value) || {};
      this.setState(
        {
          defaultRoles: permissionSettingsState.data.defaultRoles,
          roleData:
            permissionSettingsState.data.defaultRoles.find(x => x.roleName === this.state.value) ||
            "",
          permissionData: this.transformPermissionArray(selectedPermission),
        },
        () => {
          this.initState = this.state.defaultRoles.find(x => x.roleName === this.state.value) || "";
        }
      );
    } else this.setState({ roleData: "" });
  }

  transformPermissionArray = (permissionObj: Object) => {
    /** function return the array of all parent, first child and second child permissions*/
    const children: Array<mixed> = [];
    const permObj = cloneDeep(permissionObj.permission);
    const traverse = (obj, parent) => {
      forIn(obj, (value, key) => {
        rootNode = isUndefined(parent) || isNull(parent) ? key : rootNode;
        if (typeof value == "object" && value != null) {
          traverse(value, key);
          children.push({
            ...value,
            id: key,
            parent: parent ? parent : null,
            rootnode: rootNode,
          });
        }
      });
    };
    traverse(permObj);
    return children;
  };

  handleChange = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false,
    });
  };

  onSelect = value => {
    const permissionSettingsState = store.getState().permissionSettings;
      if (permissionSettingsState.data?.defaultRoles) {
        const selectedPermission =
          permissionSettingsState.data.defaultRoles.find(x => x.roleName == value) || {};
        this.setState(
          {
            defaultRoles: permissionSettingsState.data.defaultRoles,
            roleData:
              permissionSettingsState.data.defaultRoles.find(x => x.roleName === value) || "",
            permissionData: this.transformPermissionArray(selectedPermission),
            value: value
          },
          () => {
            this.initState = this.state.defaultRoles.find(x => x.roleName === value) || "";
          }
        );
      } else {
        this.setState({ roleData: "" });
      }
  };

  handleAction = type => {
    if (type === "Duplicate") {
      this.setState({ duplicateRoleDialogOpen: true });
    } else {
      this.setState({ openModal: true, deleteModal: true }, () => {});
    }
  };

  handleDelete = () => {
    /** function for deleting the copies of workspace */
    this.setState({ openModal: false, deleteModal: false });
    this.props.DeleteWorkspaceRole(
      this.state.roleData.id,
      success => {
        this.onSelect("Admin");
      },
      error => {}
    );
  };

  handleDialogClose = e => {
    this.setState({
      openModal: false,
      deleteModal: false,
      duplicateRoleDialogOpen: false,
      roleNameError: false,
      roleNameErrorText: null,
    });
  };
  discardChangesDialogClose = () => {
    this.setState({
      discardChanges: false,
    });
  };

  handleDiscardChanges = event => {
    const { value } = this.state;
    const { permissionSettingsState } = this.props;
    const selectedPermission =
      permissionSettingsState.data.defaultRoles.find(x => x.roleName == value) || {};
    this.setState({
      permissionData: this.transformPermissionArray(selectedPermission),
      changes: false,
      discardChanges: false,
    });
  };

  handleSavePermissionSetting = e => {
    // function that save project setting on backend
    const { roleData, permissionData } = this.state;
    if (this.state.create) {
      this.setState({ btnQuery: "progress" }, () => {
        this.props.saveRole(
          {
            roleId: roleData.roleId,
            roleName: roleData.roleName,
            permission: permissionData,
          },
          success => {
            this.setState({ changes: false, btnQuery: "" }, () => {
              if (this.props.onSelect) {
                this.onSelect(success.data ? success.data.roleName : "Admin");
              }
            });
          },
          error => {
            this.setState({
              create: true,
            });
          }
        );
      });
    }
  };
  onChangePermission = permission => {
    this.setState({ permission });
  };

  duplicateRole = name => {
    const { create, roleData } = this.state;
    const roleName = name;
    if (create) {
      this.setState({ create: false, changes: false }, () => {
        this.props.duplicateRole(
          {
            newRoleName: roleName,
            roleName: roleData.roleName,
            roleId: roleData.roleId,
          },
          success => {
            this.setState({ create: true, changes: false, duplicateRoleDialogOpen: false }, () => {
              this.onSelect(success.data ? success.data.roleName : "Unknown");
            });
          },
          error => {
            this.setState({
              roleNameError: true,
              roleNameErrorText: error ? error.data : "Error",
              create: true,
              changes: false,
            });
          }
        );
      });
    }
  };

  handleChildrenClick = (obj: Object, per: string) => {
    const { permissionData, roleData } = this.state;
    const updatedPermission = permissionData.map(p => {
      if (obj.id == p.id && per == "add") {
        p.isAllowAdd = !p.isAllowAdd;
        return p;
      } else if (obj.id == p.id && per == "edit") {
        p.isAllowEdit = !p.isAllowEdit;
        return p;
      } else if (obj.id == p.id && per == "delete") {
        p.isAllowDelete = !p.isAllowDelete;
        return p;
      } else return p;
    });
    this.setState({ permissionData: updatedPermission, changes: true });
  };

  handleClick = (obj: Object = {}, per: string = "") => {
    const { permissionData, roleData } = this.state;
    const updatedPermission = permissionData.map(p => {
      if (obj.id == p.id && per == "allow") {
        p.cando = true;
        return p;
      } else if (obj.id == p.id && per == "dontAllow") {
        p.cando = false;
        return p;
      } else return p;
    });
    this.setState({ permissionData: updatedPermission, changes: true });
  };

  getIcon = (
    key: "Workspaces" | "Projects" | "Tasks" | "Meetings" | "Issues" | "Risks" | "Timesheet"
  ) => {
    /** function returing the icons */
    const { classes } = this.props;
    switch (key) {
      case "Workspaces":
        return (
          <SvgIcon viewBox="0 0 16 15.188" className={classes.plainMenuItemIcon}>
            <WorkspaceIcon />
          </SvgIcon>
        );
      case "Projects":
        return (
          <SvgIcon viewBox="0 0 18 15.188" className={classes.plainMenuItemIcon}>
            <ProjectsIcon />
          </SvgIcon>
        );
      case "Tasks":
        return (
          <SvgIcon viewBox="0 0 17 13" className={classes.plainMenuItemIcon}>
            <TaskIcon />
          </SvgIcon>
        );
      case "Meetings":
        return (
          <SvgIcon viewBox="0 0.3 17 17" className={classes.plainMenuItemIcon}>
            <MeetingsIcon />
          </SvgIcon>
        );
      case "Issues":
        return (
          <SvgIcon viewBox="-0.5 0 16 15" className={classes.plainMenuItemIcon}>
            <IssuesIcon />
          </SvgIcon>
        );
      case "Risks":
        return (
          <SvgIcon viewBox="-0.5 1 18 15.188" className={classes.plainMenuItemIcon}>
            <RiskIcon />
          </SvgIcon>
        );
      case "Timesheet":
        return (
          <SvgIcon viewBox="-0.5 1 18 15.188" className={classes.plainMenuItemIcon}>
            <TimesheetIcon />
          </SvgIcon>
        );
      case "Boards":
        return (
          <SvgIcon viewBox="0 0 14 13.188" className={classes.plainMenuItemIcon}>
            <IconBoard />
          </SvgIcon>
        );
    }
  };
  handleChangeParent = panel => (event, expanded) => {
    this.setState({
      expandedParent: expanded ? panel : false,
    });
  };

  getCollapseMenuParent = (p, firstChildLev, assignedRoleActive) => {
    const { classes, theme, workSpacePer } = this.props;
    const { expandedParent, permissionData, roleData } = this.state;

    return p.label ? <>
      <li
        style={{
          padding: 0,
          marginRight: 0,
        }}>
        <ExpansionPanel
          square
          expanded={p.cando && firstChildLev.length > 0}
          // onChange={this.handleChangeParent(p.label)}
          classes={{ root: classes.epRootParent, expanded: classes.epExpanded }}>
          <ExpansionPanelSummary
            classes={{
              root: classes.epsRootParent,
              expanded: classes.epsExpandedParent,
              content: classes.epsContent,
            }}>
            <SvgIcon
              viewBox="0 0 512 512"
              className={classes.filledIcon}
              style={{ color: p.cando ? "#0090ff" : "#F24C4C" }}>
              <FilledIcon />
            </SvgIcon>
            <div className={classes.titleContainer2}>
              <Typography variant="h6" className={classes.permissionListItemTitle}>
                {translatePermission(p.label, this.props.intl) || ""}
              </Typography>
            </div>
            {!roleData.isDefault &&
            workSpacePer.workspaceSetting.rolesPermissions.customRoles.isAllowEdit &&
            !assignedRoleActive /** if role is custom or user created then show clickable buttons/text */ ? (
              <div
                style={{
                  right: 0,
                  position: "absolute",
                  display: "flex",
                  padding: 0,
                }}>
                <Typography
                  variant="h6"
                  className={classes.allowTitle}
                  style={
                    p.cando
                      ? {
                          color: "#0090ff",
                          textDecoration: "underline",
                        }
                      : { color: "#bfbfbf" }
                  }
                  onClick={event => this.handleClick(p, "allow")}>
                  <FormattedMessage
                    id="workspace-settings.roles-and-permission.allow-button.label"
                    defaultMessage="Allow"
                  />
                </Typography>
                <span style={{ color: "#bfbfbf" }}>|</span>
                <Typography
                  variant="h6"
                  className={classes.dontAllowTitle}
                  style={
                    p.cando
                      ? { color: "#bfbfbf" }
                      : {
                          color: "#F24C4C",
                          textDecoration: "underline",
                        }
                  }
                  onClick={event => this.handleClick(p, "dontAllow")}>
                  <FormattedMessage
                    id="workspace-settings.roles-and-permission.dont-allow.label"
                    defaultMessage="Don't Allow"
                  />
                </Typography>
              </div>
            ) : (
              /** other wise show only label */
              <Typography
                variant="h6"
                className={classes.defaultRole}
                style={p.cando ? { color: "#0090ff" } : { color: "#F24C4C" }}>
                {p.cando ? (
                  <FormattedMessage
                    id="workspace-settings.roles-and-permission.allow-button.label"
                    defaultMessage="Allow"
                  />
                ) : (
                  <FormattedMessage
                    id="workspace-settings.roles-and-permission.dont-allow.label"
                    defaultMessage="Don't Allow"
                  />
                )}
              </Typography>
            )}
          </ExpansionPanelSummary>
          <ExpansionPanelDetails classes={{ root: classes.panelContainer3 }}>
            <ul className={classes.permissionsList}>
              {this.getCollapseMenuChildOne(firstChildLev, assignedRoleActive)}
            </ul>
          </ExpansionPanelDetails>
        </ExpansionPanel>
      </li>
    </>
    : null;
  };

  getLeftBadge = obj => {
    const { classes } = this.props;
    let canDoExists = Object.keys(obj).includes("cando");
    if (canDoExists) {
      /** if canDo permission exists, then show red/blue circle according to permission */
      return (
        <SvgIcon
          viewBox="0 0 512 512"
          className={classes.filledIcon}
          style={{ color: obj.cando ? "#0090ff" : "#F24C4C" }}>
          <FilledIcon />
        </SvgIcon>
      );
    } else {
      /** it means obj having isAllowAdd, isAllowEdit and isAllowDelete permission keys */
      let otherPermissionKeys = Object.values(obj).filter(p => {
        /** it filters those keys which are not have value null and string  */
        return p !== null && !isString(p);
      });
      let reduceRepetation = [
        ...new Set(otherPermissionKeys),
      ]; /** Set function reduce the duplicate value to 1 e.g true true == single true */
      if (reduceRepetation.length > 1) {
        /** if e.g true and false then show multicolor icon */
        return (
          <>
            <SvgIcon
              viewBox="0 0 510 510"
              className={classes.filledIcon}
              // style={{ color: "#F24C4C" }}
            >
              <ContrastIcon />
            </SvgIcon>
          </>
        );
      } else {
        return (
          <>
            {" "}
            <SvgIcon
              viewBox="0 0 512 512"
              className={classes.filledIcon}
              style={{ color: reduceRepetation[0] ? "#0090ff" : "#F24C4C" }}>
              <FilledIcon />
            </SvgIcon>{" "}
          </>
        );
      }
    }
  };

  getCollapseMenuChildOne = (f, assignedRoleActive) => {
    const { expandedParent, permissionData, roleData } = this.state;
    const { classes, theme } = this.props;

    if (f.length > 0) {
      return f.map(f1 => {
        const secondChildLev = permissionData.filter(d => {
          /** filtering the second child level permission of every parent level permission */
          if (d.parent == f1.id) return d;
        });
        return f1.label ? <>
          <li style={{ padding: 0, margin: "0 0 0 20px" }}>
            <ExpansionPanel
              square
              expanded={f1.cando && secondChildLev.length > 0}
              // onChange={this.handleChangeParent(p.label)}
              classes={{
                root: classes.epRootParent,
                expanded: classes.epExpanded,
              }}>
              <ExpansionPanelSummary
                classes={{
                  root: classes.epsRootParent,
                  expanded: classes.epsExpandedParent,
                  content: classes.epsContent,
                }}>
                {this.getLeftBadge(f1)}

                <div className={classes.titleContainer2}>
                  <Typography variant="h6" className={classes.permissionListItemTitle}>
                    {translatePermission(f1.label, this.props.intl) || ""}
                  </Typography>
                </div>
                {this.getChildPermission(f1, assignedRoleActive)}
              </ExpansionPanelSummary>
              <ExpansionPanelDetails classes={{ root: classes.panelContainer3 }}>
                <ul className={classes.permissionsList}>
                  {this.getCollapseMenuChildTwo(secondChildLev, assignedRoleActive)}
                </ul>
              </ExpansionPanelDetails>
            </ExpansionPanel>
          </li>
        </> : null;
      });
    } else return null;
  };
  getCollapseMenuChildTwo = (s, assignedRoleActive) => {
    const { expandedParent, permissionData, roleData } = this.state;
    const { classes, theme } = this.props;
    if (s.length > 0) {
      return s.map(s => {
        return (
          <li style={{ padding: 0, margin: "0 0 0 20px" }}>
            <ExpansionPanel
              square
              // expanded={f1.cando && secondChildLev.length > 0}
              // onChange={this.handleChangeParent(p.label)}
              classes={{
                root: classes.epRootParent,
                expanded: classes.epExpanded,
              }}>
              <ExpansionPanelSummary
                classes={{
                  root: classes.epsRootParent,
                  expanded: classes.epsExpandedParent,
                  content: classes.epsContent,
                }}>
                {this.getLeftBadge(s)}
                <div className={classes.titleContainer2}>
                  <Typography variant="h6" className={classes.permissionListItemTitle}>
                    {translatePermission(s.label, this.props.intl) || ""}
                  </Typography>
                </div>
                {this.getChildPermission(s, assignedRoleActive)}
              </ExpansionPanelSummary>
            </ExpansionPanel>
          </li>
        );
      });
    } else return null;
  };
  renderRoles = (data, assignedRoleActive) => {
    const { expanded, permissionData, roleData } = this.state;
    const { classes, theme } = this.props;
    const { paymentPlanTitle } = this.props.subscriptionDetails;

    /** filtering the permission regarding workspaces, projects, meetings, tasks, risk, issues */
    const permissions = data.filter(d => {
      /** filtering the permission regarding free package plan */
      if ((d.id == "project" || d.id == "risk") && ConstantsPlan.isPlanFree(paymentPlanTitle))
        return false;
      /** filtering the permission regarding premium package plan */ else if (
        d.id == "risk" &&
        ConstantsPlan.isAnyPremiumPlan(paymentPlanTitle)
      )
        return false;
      else if (
        d.id == "workSpace" ||
        d.id == "project" ||
        d.id == "task" ||
        d.id == "meeting" ||
        d.id == "issue" ||
        d.id == "risk" ||
        d.id == "timesheet" ||
        d.id == "board"
      )
        return d;
    });
    return permissions.map(p => {
      const parentLev = data.filter(d => {
        /** filtering the parent level permissions of respective module workspace/projects/issue/risks/meeting/tasks */
        if (d.parent == p.id) return d;
      });
      return (
        <ExpansionPanel
          square
          expanded={expanded === p.id}
          onChange={this.handleChange(p.id)}
          classes={{ root: classes.epRoot, expanded: classes.epExpanded }}>
          <ExpansionPanelSummary
            classes={{
              root: classes.epsRoot,
              expanded: classes.epsExpanded,
              content: classes.epsContent,
            }}>
            <div className={classes.iconContainer}>
              {this.getIcon(p.label) /** passing the label and getting the respective icon  */}
            </div>
            <ArrowRight
              className={classes.arrowRight}
              htmlColor={theme.palette.secondary.medDark}
            />
            <div className={classes.titleContainer}>
              <Typography variant="h5">
                {translatePermission(p.label, this.props.intl) || ""}
              </Typography>
              {/* <Typography variant="body2">{p.description || ""}</Typography> */}
            </div>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails classes={{ root: classes.panelContainer }}>
            <ul className={classes.permissionsList}>
              {parentLev.map(p => {
                /** maping the parent level permission in their respective collapse menu */
                const firstChildLev = data.filter(f => {
                  /** filtering the first child level permission of every parent level permsision */
                  if (f.parent == p.id) return f;
                });
                return <>{this.getCollapseMenuParent(p, firstChildLev, assignedRoleActive)}</>;
              })}
            </ul>
          </ExpansionPanelDetails>
        </ExpansionPanel>
      );
    });
  };

  getChildPermission = (
    obj: {
      cando: boolean,
      isAllowAdd: boolean,
      isAllowEdit: boolean,
      isAllowDelete: boolean,
    } = {},
    assignedRoleActive
  ) => {
    const { classes, theme, workSpacePer } = this.props;
    const { roleData } = this.state;
    return (
      <>
        {!isUndefined(obj.cando) &&
        !roleData.isDefault &&
        !assignedRoleActive &&
        workSpacePer.workspaceSetting.rolesPermissions.customRoles.isAllowEdit ? (
          <div style={{ right: 0, position: "absolute", display: "flex" }}>
            <Typography
              variant="h6"
              className={classes.allowTitle}
              style={
                obj.cando
                  ? {
                      color: "#0090ff",
                      textDecoration: "underline",
                    }
                  : { color: "#bfbfbf" }
              }
              onClick={event => this.handleClick(obj, "allow")}>
              <FormattedMessage
                id="workspace-settings.roles-and-permission.allow-button.label"
                defaultMessage="Allow"
              />
            </Typography>
            <span style={{ color: "#bfbfbf" }}>|</span>
            <Typography
              variant="h6"
              className={classes.dontAllowTitle}
              style={
                obj.cando
                  ? {
                      color: "#bfbfbf",
                    }
                  : {
                      color: "#F24C4C",
                      textDecoration: "underline",
                    }
              }
              onClick={event => this.handleClick(obj, "dontAllow")}>
              <FormattedMessage
                id="workspace-settings.roles-and-permission.dont-allow.label"
                defaultMessage="Don't Allow"
              />
            </Typography>
          </div>
        ) : (
          !isUndefined(obj.cando) && (
            <Typography
              variant="h6"
              className={classes.defaultRole}
              style={obj.cando ? { color: "#0090ff" } : { color: "#F24C4C" }}>
              {obj.cando ? (
                <FormattedMessage
                  id="workspace-settings.roles-and-permission.allow-button.label"
                  defaultMessage="Allow"
                />
              ) : (
                <FormattedMessage
                  id="workspace-settings.roles-and-permission.dont-allow.label"
                  defaultMessage="Don't Allow"
                />
              )}
            </Typography>
          )
        )}
        <span
          style={{
            right: 0,
            position: "absolute",
            display: "flex",
            padding: 0,
          }}>
          {!isUndefined(
            obj.isAllowAdd
          ) /** if isAllowAdd exists in the object then show Add button*/ &&
            !isNull(obj.isAllowAdd) &&
            (!roleData.isDefault &&
            !assignedRoleActive &&
            workSpacePer.workspaceSetting.rolesPermissions.customRoles.isAllowEdit ? (
              <>
                <Typography
                  variant="h6"
                  className={classes.allowTitle}
                  style={
                    obj.isAllowAdd
                      ? {
                          color: "#0090ff",
                          textDecoration: "underline",
                        }
                      : { color: "#bfbfbf" }
                  }
                  onClick={event => this.handleChildrenClick(obj, "add")}>
                  <FormattedMessage
                    id="workspace-settings.roles-and-permission.add-button.label"
                    defaultMessage="Add"
                  />
                </Typography>
              </>
            ) : (
              <Typography
                variant="h6"
                className={classes.dontAllowTitle}
                style={obj.isAllowAdd ? { color: "#0090ff" } : { color: "#F24C4C" }}>
                <FormattedMessage
                  id="workspace-settings.roles-and-permission.add-button.label"
                  defaultMessage="Add"
                />
              </Typography>
            ))}
          {!isUndefined(
            obj.isAllowEdit
          ) /** if isAllowEdit exists in the object then show Edit button*/ &&
            !isNull(obj.isAllowEdit) &&
            (!roleData.isDefault &&
            !assignedRoleActive &&
            workSpacePer.workspaceSetting.rolesPermissions.customRoles.isAllowEdit ? (
              <Typography
                variant="h6"
                className={classes.allowTitle}
                style={
                  obj.isAllowEdit
                    ? {
                        color: "#0090ff",
                        textDecoration: "underline",
                      }
                    : { color: "#bfbfbf" }
                }
                onClick={event => this.handleChildrenClick(obj, "edit")}>
                <FormattedMessage
                  id="workspace-settings.roles-and-permission.edit-button.label"
                  defaultMessage="Edit"
                />
              </Typography>
            ) : (
              <Typography
                variant="h6"
                className={classes.dontAllowTitle}
                style={obj.isAllowEdit ? { color: "#0090ff" } : { color: "#F24C4C" }}>
                <FormattedMessage
                  id="workspace-settings.roles-and-permission.edit-button.label"
                  defaultMessage="Edit"
                />
              </Typography>
            ))}
          {!isUndefined(
            obj.isAllowDelete
          ) /** if isAllowDelete exists in the object then show delete button*/ &&
            !isNull(obj.isAllowDelete) &&
            (!roleData.isDefault &&
            !assignedRoleActive &&
            workSpacePer.workspaceSetting.rolesPermissions.customRoles.isAllowEdit ? (
              <Typography
                variant="h6"
                className={classes.allowTitle}
                style={
                  obj.isAllowDelete
                    ? {
                        color: "#0090ff",
                        textDecoration: "underline",
                      }
                    : { color: "#bfbfbf" }
                }
                onClick={event => this.handleChildrenClick(obj, "delete")}>
                <FormattedMessage id="common.action.delete.label" defaultMessage="Delete" />
              </Typography>
            ) : (
              <Typography
                variant="h6"
                className={classes.dontAllowTitle}
                style={obj.isAllowDelete ? { color: "#0090ff" } : { color: "#F24C4C" }}>
                <FormattedMessage id="common.action.delete.label" defaultMessage="Delete" />
              </Typography>
            ))}
        </span>
      </>
    );
  };
  openModalDiscardChanges = () => {
    this.setState({ discardChanges: true });
  };

  render() {
    const { classes, theme, workSpacePer, allMembers, userId } = this.props;
    const {
      expanded,
      roleData,
      value,
      changes,
      deleteBtnQuery,
      deleteModal,
      openModal,
      duplicateRoleDialogOpen,
      btnQuery,
      roleNameErrorText,
      roleNameError,
      permissionData,
      defaultRoles,
      discardChanges,
    } = this.state;
    const permission = roleData ? roleData.permission : "";
    const currentUser =
      allMembers.find(item => {
        return item.userId == userId;
      }) || {};
    const assignedRoleActive = currentUser.roleName == value;
    return (
      <div className={classes.container}>
        <div className={classes.permissionOptionsHeader}>
          {duplicateRoleDialogOpen && (
            <CreateDuplicateRoleDialog
              open={duplicateRoleDialogOpen}
              closeAction={this.handleDialogClose}
              duplicateRole={this.duplicateRole}
              onSelect={this.onSelect}
              roleNameErrText={roleNameErrorText}
              roleNameErr={roleNameError}
              selectedRoleValue={value}
              successBtnText={
                <FormattedMessage
                  id="workspace-settings.roles-and-permission.duplicate-role.title"
                  defaultMessage="Duplicate Role"
                />
              }
              title={`${this.props.intl.formatMessage({
                id: "common.duplicate.label",
                defaultMessage: "Duplicate",
              })} ${value} ${this.props.intl.formatMessage({
                id: "common.role.label",
                defaultMessage: "Role",
              })}`}
              keyValue="duplicateRole"
            />
          )}
          <SelectRoleDropdown
            onSelect={this.onSelect}
            defaultRoles={defaultRoles}
            value={value}
            workSpacePer={workSpacePer}
          />
          <PermissionOptionDropdown
            roleData={roleData}
            onSelect={this.onSelect}
            handleAction={this.handleAction}
            workSpacePer={workSpacePer}
          />
          {!roleData.isDefault &&
          workSpacePer.workspaceSetting.rolesPermissions.customRoles.isAllowEdit &&
          !assignedRoleActive ? (
            <>
              <CustomButton
                variant="text"
                btnType="plain"
                disabled={!changes}
                onClick={this.openModalDiscardChanges}>
                <FormattedMessage id="common.action.Discard.label" defaultMessage="Discard" />
              </CustomButton>
              <CustomButton
                style={{ marginLeft: 20 }}
                variant="contained"
                btnType="success"
                disabled={!changes}
                query={btnQuery}
                onClick={e => this.handleSavePermissionSetting(e)}>
                <FormattedMessage id="common.action.update.label" defaultMessage="Update" />
              </CustomButton>
            </>
          ) : null}
        </div>
        <div className={classes.permissionsCnt}>
          {this.renderRoles(
            permissionData,
            assignedRoleActive
          ) /** function returning the UI elements of permissions */}
        </div>
        <>
          {deleteModal ? (
            <DeleteConfirmDialog
              open={openModal}
              closeAction={this.handleDialogClose}
              cancelBtnText={
                <FormattedMessage
                  id="common.action.archive.confirmation.cancel-button.label"
                  defaultMessage="Cancel"
                />
              }
              successBtnText={
                <FormattedMessage
                  id="common.action.delete.confirmation.delete-button.label"
                  defaultMessage="Delete"
                />
              }
              alignment="center"
              headingText={
                <FormattedMessage
                  id="common.action.delete.confirmation.delete-button.label"
                  defaultMessage="Delete"
                />
              }
              successAction={this.handleDelete}
              msgText={
                <FormattedMessage
                  id="common.action.role.delete.label"
                  defaultMessage="Are you sure you want to delete this role?"
                />
              }
              btnQuery={deleteBtnQuery}
            />
          ) : null}
          {discardChanges ? (
            <DeleteConfirmDialog
              open={discardChanges}
              closeAction={this.discardChangesDialogClose}
              cancelBtnText={
                <FormattedMessage
                  id="common.action.archive.confirmation.cancel-button.label"
                  defaultMessage="Cancel"
                />
              }
              successBtnText={
                <FormattedMessage
                  id="common.action.discard-changes.label"
                  defaultMessage="Discard Changes"
                />
              }
              alignment="center"
              headingText={
                <FormattedMessage
                  id="workspace-settings.user-management.discard-role.label"
                  defaultMessage="Discard Role Changes"
                />
              }
              successAction={this.handleDiscardChanges}
              msgText={
                <FormattedMessage
                  id="common.action.role.delete.message"
                  defaultMessage="Are you sure you want to discard all changes you have made to this role?"
                />
              }
              // btnQuery={deleteBtnQuery}
            />
          ) : null}
        </>
      </div>
    );
  }
}

const mapStateToProps = state => {
  let company = state.profile.data.teams.find(
    item => item.companyId == state.profile.data.activeTeam
  );
  return {
    permissionSettingsState: state.permissionSettings,
    workSpacePer: state.workspacePermissions.data.workSpace,
    allMembers: state.profile.data.member.allMembers,
    userId: state.profile.data.userId,
    subscriptionDetails: company ? company.subscriptionDetails : "",
  };
};

export default compose(
  injectIntl,
  connect(mapStateToProps, { duplicateRole, DeleteWorkspaceRole, saveRole }),
  withStyles(permissionStyles, {
    withTheme: true,
  })
)(RolesAndPermission);
