import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import workspaceSetting from "../styles";
import menuStyles from "../../../assets/jss/components/menu";
import CustomButton from "../../../components/Buttons/CustomButton";
import SelectionMenu from "../../../components/Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import combineStyles from "../../../utils/mergeStyles";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import DownArrow from "@material-ui/icons/ArrowDropDown";
import AddIcon from "@material-ui/icons/Add";
import CreateNewRoleDialog from "./CreateNewRoleDialog";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";
import { Scrollbars } from "react-custom-scrollbars";
import { FormattedMessage } from "react-intl";

class SelectRoleDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      pickerOpen: false,
      archiveFlag: false,
      unArchiveFlag: false,
      deleteFlag: false,
      newRoleDialogOpen: false,
      selectedValue: "Admin",
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handlePickerClose = this.handlePickerClose.bind(this);
    this.handleOperations = this.handleOperations.bind(this);
    this.changeSelectedValue = this.changeSelectedValue.bind(this);
  }
  handleOperations(event, value) {
    event.stopPropagation();
    if (value) {
      this.setState({ selectedValue: value }, () => {
        this.props.onSelect(value);
      });
    }
    // switch (value) {
    //   case "Owner":
    //     //this.props.CopyTask(this.props.issue);
    //     break;
    //   case "Admin":
    //     break;
    //   case "Member":
    //     this.setState({ archiveFlag: true, popupFlag: true });
    //     break;
    //   case "Limited Member":
    //     this.setState({ unArchiveFlag: true, popupFlag: true });
    //     break;
    //   case "Delete":
    //     this.setState({ deleteFlag: true, popupFlag: true });
    //     break;
    // }

    this.setState({ open: false, pickerOpen: false, selectedValue: value });
  }
  changeSelectedValue(value) {
    this.setState({ selectedValue: value });
  }
  handleClose(event) {
    this.setState({ open: false, pickerOpen: false });
  }
  handlePickerClose(event) {
    this.setState({ pickerOpen: false });
  }

  handleClick(event, placement) {
    this.setState((state) => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }
  handleDialogOpen = () => {
    this.setState({ newRoleDialogOpen: true, open: false, pickerOpen: false });
  };
  handleDialogClose = () => {
    this.setState({ newRoleDialogOpen: false });
  };
  componentDidUpdate(props) {
    if (props.value !== this.props.value) {
      this.setState({ selectedValue: this.props.value });
    }
  }
  translateRole = (role) => {
    let id = "";
    switch (role) {
      case "Admin":
        id = "workspace-settings.roles-and-permission.drop-down.admin";
        break;
      case "Member":
        id = "workspace-settings.roles-and-permission.drop-down.member";
        break;
      case "Limited Member":
        id = "workspace-settings.roles-and-permission.drop-down.limited-member";
        break;
    }
    return id == "" ? role : <FormattedMessage id={id} defaultMessage={role} />
  }
  render() {
    const { classes, theme, defaultRoles, workSpacePer } = this.props;
    const { open, newRoleDialogOpen, selectedValue } = this.state;
    return (
      <Fragment>
        {newRoleDialogOpen && (
          <CreateNewRoleDialog
            open={newRoleDialogOpen}
            closeAction={this.handleDialogClose}
            onSelect={this.props.onSelect}
            changeSelectedValue={this.changeSelectedValue}
            selectedRoleValue={selectedValue}
            successBtnText={
              <FormattedMessage
                id="workspace-settings.roles-and-permission.new-role.label"
                defaultMessage="Create New Role"
              />
            }
            title={
              <FormattedMessage
                id="workspace-settings.roles-and-permission.new-role.label"
                defaultMessage="Create New Role"
              />
            }
            keyValue="newRole"
          />
        )}
        <ClickAwayListener onClickAway={this.handleClose}>
          <div>
            <CustomButton
              onClick={(event) => {
                this.handleClick(event, "bottom-end");
              }}
              buttonRef={(node) => {
                this.anchorEl = node;
              }}
              style={{
                minWidth: 150,
                padding: "7px 8px 7px 14px",
                display: "flex",
                justifyContent: "space-between",
              }}
              btnType="white"
              variant="contained"
            >
              {this.translateRole(selectedValue)}
              <DownArrow
                htmlColor={theme.palette.secondary.medDark}
                className={classes.dropdownArrow}
              />
            </CustomButton>

            <SelectionMenu
              open={open}
              closeAction={this.handleClose}
              placement="bottom-start"
              style={{ width: 200 }}
              anchorRef={this.anchorEl}
              list={
                <>
                  <Scrollbars
                    autoHide={false}
                    autoHeight
                    autoHeightMin={0}
                    autoHeightMax={150}
                  >
                    <List disablePadding={true}>
                      <ListItem disableRipple className={classes.headingItem}>
                        <ListItemText
                          primary={
                            <FormattedMessage
                              id="common.action.role.label"
                              defaultMessage="Select Role"
                            />
                          }
                          classes={{ primary: classes.headingText }}
                        />
                      </ListItem>

                      {defaultRoles.map((x) => {
                        return (
                          <ListItem
                            button
                            key={x.roleName}
                            disableRipple
                            classes={{ selected: classes.statusMenuItemSelected }}
                            onClick={(event) =>
                              this.handleOperations(event, x.roleName)
                            }
                          >
                            <ListItemText
                              primary={this.translateRole(x.roleName)}
                              classes={{
                                primary: classes.statusItemText,
                              }}
                            />
                          </ListItem>
                        );
                      })}
                    </List>
                  </Scrollbars>
                  <>
                    {workSpacePer.workspaceSetting.rolesPermissions.customRoles
                      .isAllowAdd &&
                      teamCanView("customRoleAndPermissionAccess") && (
                        <ListItem
                          button
                          disableRipple
                          classes={{ root: classes.addNewRoleMenuItem }}
                          onClick={this.handleDialogOpen}
                        >
                          <AddIcon
                            htmlColor={theme.palette.primary.light}
                            style={{ fontSize: "18px" }}
                          />
                          <ListItemText
                            primary={
                              <FormattedMessage
                                id="workspace-settings.roles-and-permission.new-role.label"
                                defaultMessage="Create New Role"
                              />
                            }
                            className={classes.addNewRoleMenuItemText}
                            classes={{
                              primary: classes.statusItemText,
                            }}
                          />
                        </ListItem>
                      )}
                  </>
                </>
              }
            />
          </div>
        </ClickAwayListener>
      </Fragment>
    );
  }
}

export default withStyles(combineStyles(workspaceSetting, menuStyles), {
  withTheme: true,
})(SelectRoleDropdown);
