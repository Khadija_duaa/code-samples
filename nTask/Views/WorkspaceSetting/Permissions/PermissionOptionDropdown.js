import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import workspaceSetting from "../styles";
import menuStyles from "../../../assets/jss/components/menu";
import CustomIconButton from "../../../components/Buttons/IconButton";
import SelectionMenu from "../../../components/Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import combineStyles from "../../../utils/mergeStyles";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import RenameRoleDialog from "./RenameRoleDialog";
import { teamCanView } from "../../../components/PlanPermission/PlanPermission";
import { FormattedMessage } from "react-intl";
import MoveIssueDialog from "../../../components/Dialog/MoveIssueDialog/MoveIssueDialog";
import CopyToWorkspaceDialog
  from "../../../components/Dialog/MoveToWorkspaceDialog/CopyToWorkspaceDialog";
import { copyRoleToWorkspace } from "../../../redux/actions/workspace";

class PermissionOptionDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      pickerOpen: false,
      pickerPlacement: "",
      archiveFlag: false,
      unArchiveFlag: false,
      deleteFlag: false,
      renameRoleDialogOpen: false,
      copyWorkspaceDialogOpen: false,
      copyRoleBtnQuery: ''
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handlePickerClose = this.handlePickerClose.bind(this);
  }
  handleDialogOpen = () => {
    this.setState({ renameRoleDialogOpen: true, open: false });
  };
  handleDialogClose = () => {
    this.setState({ renameRoleDialogOpen: false });
  };
  handleClose(event) {
    this.setState({ open: false, pickerOpen: false });
  }
  handlePickerClose(event) {
    this.setState({ pickerOpen: false });
  }

  handleClick(event, placement) {
    const { currentTarget } = event;
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement
    }));
  }
  handleCopyToWorkspace = () => {
    this.copyToWsDialogOpen()
  }
  copyToWsDialogClose = () => {
    this.setState({ copyWorkspaceDialogOpen: false })
  }

  copyToWsDialogOpen = () => {
    this.setState({ copyWorkspaceDialogOpen: true, open: false, pickerOpen: false })

  }
  handleRoleCopy = (e, team) => {
    const { roleData } = this.props;
    const postObj = { roleId: roleData.roleId, workspaceIds: team.map(t => t.teamId) }
    this.setState({ copyRoleBtnQuery: 'progress' })
    copyRoleToWorkspace(postObj, () => {
      this.setState({ copyRoleBtnQuery: '', copyWorkspaceDialogOpen: false })
    });
  }
  render() {
    const { classes, theme, roleData, handleAction, workSpacePer } = this.props;
    const { open, placement, renameRoleDialogOpen, copyWorkspaceDialogOpen, copyRoleBtnQuery } = this.state;

    return (
      <Fragment>
        <CopyToWorkspaceDialog
          open={copyWorkspaceDialogOpen}
          message={'Are you sure you want to copy role to selected workspace(s).'}
          successAction={this.handleRoleCopy}
          closeAction={this.copyToWsDialogClose}
          btnQuery={copyRoleBtnQuery}
          cancelBtnText={
            <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
          }
          exceptionText={""}
          headingText={"Copy Role to Workspace(s)"}
          isMulti={true}
        />
        <RenameRoleDialog
          open={renameRoleDialogOpen}
          closeAction={this.handleDialogClose}
          roleData={roleData}
          onSelect={this.props.onSelect}
        />
        <ClickAwayListener onClickAway={this.handleClose}>
          <div>
            {teamCanView('customRoleAndPermissionAccess') && <CustomIconButton
              btnType="condensed"
              onClick={event => {
                this.handleClick(event, "bottom-start");
              }}
              buttonRef={node => {
                this.anchorEl = node;
              }}
            >
              <MoreVerticalIcon
                htmlColor={theme.palette.secondary.medDark}
                style={{ fontSize: "24px" }}
              />
            </CustomIconButton>}

            <SelectionMenu
              open={open}
              closeAction={this.handleClose}
              placement={placement}
              style={{ width: 200 }}
              anchorRef={this.anchorEl}
              list={
                <List disablePadding={true}>
                  <ListItem disableRipple className={classes.headingItem}>
                    <ListItemText
                      primary={<FormattedMessage id="common.action.label" defaultMessage="Select Action" />}
                      classes={{ primary: classes.headingText }}
                    />
                  </ListItem>
                  {/* {!roleData.isDefault &&
              workSpacePer.workspaceSetting.rolesPermissions.renameCustomRole
                .cando ? (
                <ListItem
                  button
                  disableRipple
                  classes={{ selected: classes.statusMenuItemSelected }}
                  onClick={this.handleDialogOpen}
                >
                  <ListItemText
                    primary="Rename"
                    classes={{
                      primary: classes.statusItemText
                    }}
                  />
                </ListItem>
              ) : null} */}

                  {workSpacePer.workspaceSetting.rolesPermissions.customRoles
                    .isAllowAdd && teamCanView('customRoleAndPermissionAccess') && (
                      <ListItem
                        button
                        disableRipple
                        classes={{ selected: classes.statusMenuItemSelected }}
                        onClick={e => {
                          this.handleClose();
                          handleAction("Duplicate");
                        }}
                      >
                        <ListItemText
                          primary={<FormattedMessage id="common.duplicate.label" defaultMessage="Duplicate" />}
                          classes={{
                            primary: classes.statusItemText
                          }}
                        />
                      </ListItem>
                    )}
                  {!roleData.isDefault &&
                    workSpacePer.workspaceSetting.rolesPermissions.customRoles
                      .isAllowDelete ? (
                    <ListItem
                      button
                      disableRipple
                      classes={{ selected: classes.statusMenuItemSelected }}
                      onClick={e => {
                        this.handleClose();
                        handleAction("Delete");
                      }}
                    >
                      <ListItemText
                        primary={<FormattedMessage id="common.action.delete.confirmation.delete-button.label" defaultMessage="Delete" />}
                        classes={{
                          primary: classes.statusItemText
                        }}
                      />
                    </ListItem>
                  ) : null}
                  {!roleData.isDefault &&
                    <ListItem
                      button
                      disableRipple
                      classes={{ selected: classes.statusMenuItemSelected }}
                      onClick={this.handleCopyToWorkspace}
                    // onClick={e => {
                    //   this.handleClose();
                    //   handleAction("Delete");
                    // }}
                    >
                      <ListItemText
                        primary='Copy to workspace(s)'
                        classes={{
                          primary: classes.statusItemText
                        }}
                      />
                    </ListItem>}
                </List>
              }
            />
          </div>
        </ClickAwayListener>
      </Fragment>
    );
  }
}

export default withStyles(combineStyles(workspaceSetting, menuStyles), {
  withTheme: true
})(PermissionOptionDropdown);
