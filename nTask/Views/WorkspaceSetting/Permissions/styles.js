const permissionStyles = theme => ({
  container: {
    maxWidth: 750,
  },

  permissionOptionsHeader: {
    // marginBottom: 10,
    margin: "15px 0px 15px 18px",
    display: "flex"
  },
  epRoot: {
    // border: "1px solid rgba(0,0,0,.125)",
    background: theme.palette.common.white,
    boxShadow: "none",
    borderRadius: 4,
    "&:not(:last-child)": {
      borderBottom: 0
    },
    "&:before": {
      display: "none"
    }
  },
  epRootParent: {
    // border: "1px solid rgba(0,0,0,.125)",
    background: theme.palette.common.white,
    boxShadow: "none",
    borderRadius: 4,
    "&:not(:last-child)": {
      borderBottom: 0
    },
    "&:before": {
      display: "none"
    },
    width: "100%"
  },
  epExpanded: {
    margin: "auto"
  },
  epsRoot: {
    backgroundColor: theme.palette.common.white,
    minHeight: 60,
    padding: 0,
    margin: "0 20px",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    "&$expanded": {
      minHeight: 60
    }
  },
  epsRootParent: {
    backgroundColor: theme.palette.common.white,
    padding: 0,
    "&$expanded": {
      minHeight: 60
    }
  },
  epsContent: {
    margin: "12px 0 !important",
    "&$expanded": {
      margin: "12px 0"
    }
  },
  epsExpanded: {},
  epsExpandedParent: { minHeight: "0 !important" },
  epdroot: {
    padding: "20px 0",
    flexDirection: "column"
  },
  arrowRight: {
    position: "absolute",
    right: 0,
    marginTop: 7
  },
  permissionsCnt: {
    // border: `1px solid ${theme.palette.border.lightBorder}`,
    background: theme.palette.common.white,
    borderRadius: 4,
    // marginBottom: 10
  },
  permissionsListCnt: {
    flex: 1
  },
  permissionsListHeader: {
    background: theme.palette.background.paper,
    padding: "8px 20px 8px 0",
    display: "flex",
    justifyContent: "flex-end"
  },
  permissionListHeaderText: {
    width: 130,
    textAlign: "center"
  },
  permissionsList: {
    listStyleType: "none",
    padding: 0,
    width: "100%",
    margin: 0,
    "& li": {
      padding: "8px 0 8px 0",
      margin: "0 20px",
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
      borderBottom: `1px solid ${theme.palette.border.lightBorder}`
    },
    "& li:last-child": {
      border: "none"
    }
  },
  permissionListItemTitle: {
    flex: 1,
    marginLeft: 10
  },
  checkboxCnt: {
    width: 130,
    textAlign: "center"
  },
  tickIcon: {
    fontSize: "12px !important"
  },
  crossIcon: {
    fontSize: "15px !important"
  },
  plainMenuItemIcon: {
    fontSize: "19px !important",
    color: "white",
    // marginBottom: 3
  },
  iconContainer: {
    width: 40,
    height: 40,
    backgroundColor: "#0090ff",
    borderRadius: "50%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  titleContainer: {
    margin: "0 8px",
    paddingTop : 10,
    "& p": {
      color: "#B2B2B2",
      fontSize: "12px !important"
    }
  },
  titleContainer2: {
    margin: "0",
    "& p": {
      color: "#B2B2B2",
      fontSize: "12px !important"
    }
  },
  allowStatusDot: {
    backgroundColor: "#0090ff"
  },
  dontAllowStatusDot: {
    backgroundColor: "#F24C4C"
  },
  allowTitle: {
    marginRight: 5,
    cursor: "pointer"
    // textDecoration: 'underline'
  },
  dontAllowTitle: {
    marginLeft: 5,
    cursor: "pointer"
    // textDecoration: 'underline'
  },
  dot: {
    backgroundColor: "#bfbfbf"
  },
  dotRoot: {
    margin: "0 8px"
  },
  panelContainer: {
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    padding: "0 20px"
  },
  defaultRole: {
    right: 0,
    position: "absolute",
    paddingRight: '0 !important'
  },
  panelContainer2: {
    padding: "0 24px"
  },
  panelContainer3: {
    padding: 0
  },
  filledIcon:{
    marginTop: 7,
    fontSize : 8
  }
});

export default permissionStyles;
