import React, { useState, useEffect } from "react";
import Link from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";
import { CopyToClipboard } from "react-copy-to-clipboard";

const SendEmailHeader = params => {
  const {
    classes,
    handleGenerateLink,
    emailHeaderConfig: { type, imgUrl, url  },
  } = params;

  const [btnTitle, setBtnTitle] = useState("Copy");
  const handleCopy = () => {
    setBtnTitle("Copied");
  };

  useEffect(() => {
    setBtnTitle("Copy");
  }, [url]);

  return (
    <>
      <div className={classes.emailTitle}>Send Email To</div>
      <CopyToClipboard text={url}>
        <div className={classes.urlWrapper}>
            <Typography className={classes.url}>{url}</Typography>
          <button className={classes.copyBtn} onClick={handleCopy}>
            {btnTitle}
          </button>
        </div>
      </CopyToClipboard>
      <Link
        className={classes.generateLink}
        component="a"
        variant="inherit"
        onClick={()=>handleGenerateLink(type)}>
        Generate New Email Address
      </Link>
      <div className={classes.imgSS}>
        <img src={imgUrl} alt="project" />
      </div>
      <Link
        className={classes.generateLinkProject}
        component="a"
        variant="inherit"
        onClick={() => {
          alert("I'm a link.");
        }}>
        Learn more about Create {type} from Email
      </Link>
    </>
  );
};
export default SendEmailHeader;
