const workspaceEmailSetting = theme => ({
  // Email to Ntask
  root: {
    width: "730px",
    marginLeft: "54px",
    marginTop: "20px",
    outLine: "none",
    "& *": {
      fontFamily: theme.typography.fontFamilyLato,
    },
  },
  summary: {
    marginBottom: theme.typography.h4.fontSize,
    padding: 0,
    backgroundColor: theme.palette.common.white,
    border: `1px solid ${theme.palette.border.grayLighter}`,
    borderRadius: "6px",
    boxShadow: "none",
    "&::before": {
      backgroundColor: "transparent",
    },
  },
  heading: {
    fontSize: theme.typography.h4.fontSize,
    flexBasis: "33.33%",
    color: "#161717",
    letterSpacing: 0,
    fontWeight: theme.typography.fontWeightxLarge,
  },

  icon: {
    color: theme.palette.text.linkBlue,
    width: "21px",
    height: "24px",
    marginTop: "6px",
  },
  secondaryHeading: {
    fontSize: theme.typography.h5.fontSize,
    flexBasis: "33.33%",
    color: theme.palette.text.darkGray,
    letterSpacing: 0,
    fontWeight: theme.typography.fontWeightRegular,
  },
  summaryWrapper: {
    display: "flex",
    justifyContent: "center",
  },
  divider: {
    backgroundColor: "#eaeaea",
    width: "667px",
    margin: "auto",
    height: "0.5px",
  },
  wrapperHeading: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    paddingLeft: 15,
  },
  emailTitle: {
    fontSize: theme.typography.h5.fontSize,
    letterSpacing: 0,
    color: theme.palette.text.darkGray,
  },
  headerWrappper: {
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
  },
  urlWrapper: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: theme.palette.border.extraLightBorder,
    padding: "10px",
    borderRadius: "6px",
    margin: "8px 0px",
  },
  copyBtn: {
    backgroundColor: theme.palette.background.green,
    border: "none",
    padding: "6px",
    color: theme.palette.common.white,
    borderRadius: "4px",
    cursor: "pointer",
    fontSize: theme.typography.h5.fontSize,
    "&:hover": {
      backgroundColor: "#00B781",
    },
  },
  generateLink: {
    color: theme.palette.primary.main,
    fontSize: theme.typography.h5.fontSize,
    cursor: "pointer",
    textAlign: "end",
    marginRight: 4,
    textDecoration: "underline",
  },
  imgSS: {
    marginLeft: -10,
    marginTop: 30,
  },
  generateLinkProject: {
    color: theme.palette.text.linkBlue,
    fontSize: theme.typography.h5.fontSize,
    cursor: "pointer",
    textDecoration: "underline",
    fontWeight: theme.typography.fontWeightLarge,
    letterSpacing: 0,
  },
  url: {
    color: theme.palette.text.darkGray,
    fontWeight: theme.typography.fontWeightRegular,
    fontSize: "13px !important",
  },
});

export default workspaceEmailSetting;
