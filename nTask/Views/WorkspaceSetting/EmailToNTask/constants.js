import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";
import IconIssues from "../../../components/Icons/IconIssues";
import IconProjects from "../../../components/Icons/IconProjects";
import IconRisks from "../../../components/Icons/IconRisks";
import IconTasks from "../../../components/Icons/IconTasks";
import IconMeetings from "../../../components/Icons/IconMeetings";

import projectImage from "../../../assets/images/svgs/project.svg";
import taskImage from "../../../assets/images/svgs/task.svg";
import issueImage from "../../../assets/images/svgs/issue.svg";
import riskImage from "../../../assets/images/svgs/risk.svg";

export function data(classes) {
  return [
    {
      id: "project",
      icon: (
        <SvgIcon className={classes.sidebarIcon} viewBox="0 0 16 13.516">
          <IconProjects />
        </SvgIcon>
      ),
      heading: "Create Project from Email",
      secondaryHeading: "Send or forward email to nTask to create project.",
      permissionKey:"emailToProject"
    },
    
    {
      id: "task",
      icon: (
        <SvgIcon className={classes.sidebarIcon} viewBox="0 0 14 15.556">
          <IconTasks />
        </SvgIcon>
      ),
      heading: "Create Task from Email",
      secondaryHeading: "Send or forward email to nTask to create task.",
      permissionKey:"emailToTask"
    },
    {
      id: "issue",
      icon: (
        <SvgIcon className={classes.sidebarIcon} viewBox="0 0 14 15.256">
          <IconIssues />
        </SvgIcon>
      ),
      heading: "Create Issue from Email",
      secondaryHeading: "Send or forward email to nTask to create issue.",
      permissionKey:"emailToIssue"

    },
    {
      id: "risk",
      icon: (
        <SvgIcon className={classes.sidebarIcon} viewBox="0 0 15 13.127">
          <IconRisks />
        </SvgIcon>
      ),
      heading: "Create Risk from Email",
      secondaryHeading: "Send or forward email to nTask to create risk.",
      permissionKey:"emailToRisk"

    },
    // {
    //   id: "meeting",
    //   icon: (
    //     <SvgIcon className={classes.sidebarIcon} viewBox="0 0 15 13.127">
    //       <IconMeetings />
    //     </SvgIcon>
    //   ),
    //   heading: "Create Meeting from Email",
    //   secondaryHeading: "Send or forward email to nTask to create meeting.",
    //   permissionKey:"emailToMeeting"

    // },
  ];
}

export function getImage(type) {
  const images = {
    project: projectImage,
    task: taskImage,
    issue: issueImage,
    risk: riskImage,
    // meeting:projectImage
  };
  return images[type];
}
