import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Divider from "@material-ui/core/Divider";
import { data as pannelData, getImage } from "./constants";
import { useSelector } from "react-redux";
import { workspaceEmailNtask } from "../../../redux/actions/workspace";
import workspaceEmailSetting from "./styles";
import SendEmailHeader from "./SendEmailHeader";
import CustomTimer from "../../../components/CustomTimer/CustomTimer";

const EmailToNtask = props => {
  const { classes, permission } = props;
  console.log("per", permission);
  const activeTeamId = useSelector(state => state.profile.data.loggedInTeam);
  const [expanded, setExpanded] = React.useState(false);
  const [nTask, setNtask] = React.useState({});

  const handleChange = panel => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  const handleGenerateLink = type => {
    workspaceEmailNtask(activeTeamId, type, response => {
      setNtask(response);
      setNtask(prevState => ({
        ...prevState,
        type: response,
      }));
    });
  };
  useEffect(() => {
    workspaceEmailNtask(activeTeamId, "get", response => {
      setNtask(response);
    });
  }, []);
  useEffect(() => {}, [nTask]);

  const data = pannelData(classes);
  return (
    <div className={classes.root}>
      <CustomTimer/>
      {data?.map(accordion => {
        const { id, icon, heading, secondaryHeading, permissionKey } = accordion;
        return (
          permission[permissionKey][`cando`] && (
            <ExpansionPanel
              expanded={expanded === id}
              key={id}
              onChange={handleChange(id)}
              className={classes.summary}>
              <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
                className={classes.summaryWrapper}>
                <Typography className={classes.icon}>{icon}</Typography>
                <div className={classes.wrapperHeading}>
                  <Typography className={classes.heading}>{heading}</Typography>
                  <Typography className={classes.secondaryHeading}>{secondaryHeading}</Typography>
                </div>
              </ExpansionPanelSummary>
              <Divider className={classes.divider} />
              <ExpansionPanelDetails className={classes.headerWrappper}>
                <SendEmailHeader
                  classes={classes}
                  emailHeaderConfig={{
                    type: expanded,
                    imgUrl: getImage(expanded),
                    url: nTask[expanded],
                  }}
                  handleGenerateLink={handleGenerateLink}
                />
              </ExpansionPanelDetails>
            </ExpansionPanel>
          )
        );
      })}
    </div>
  );
};

EmailToNtask.propTypes = {
  classes: PropTypes.object.isRequired,
};
// const mapStateToProps = state => {
//   return {
//     // EmailPer: state.workspacePermissions.data.board,
//     // subscriptionDetails: state.profile.data.teams.find(
//     //   item => item.companyId == state.profile.data.activeTeam
//     // )?.subscriptionDetails,
//   };
// };
export default withStyles(workspaceEmailSetting, { withTheme: true })(EmailToNtask);
// export default compose(
//   withStyles(workspaceEmailSetting, { withTheme: true }),
//   connect(mapStateToProps)
// )(EmailToNtask);
