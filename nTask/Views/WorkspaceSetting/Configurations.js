import React, { useState, useEffect } from "react";
import styles from "./styles";
import { withStyles } from "@material-ui/core/styles";
import DefaultSwitch from "../../components/Form/Switch";
import CustomTooltip from "../../components/Tooltip/Tooltip";
import { useDispatch, useSelector } from "react-redux";
import { updateWorkspaceConfig } from "../../redux/actions/workspace";
import { updateProgressConfig } from "../../redux/actions/workspace";
import { updateTaskEffortWorkspaceConfig, updateTimeSettingWorkspaceConfig } from "../../redux/actions/workspace";
import SelectSearchDropdown from "../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import DropdownMenu from "../../components/Dropdown/DropdownMenu";
import { CanAccess, CanAccessFeature } from "../../components/AccessFeature/AccessFeature.cmp";

const defaultTeamObj = {
  config: {
    isProjectFieldMandatory: false,
    isUserTasksEffortMandatory: false
  },
};
const dropdownData = [
  { label: 'Automatic', value: 0 },
  { label: 'Manual', value: 1 },
  // { label: 'Hybrid', value: 2 }
]
function Configuration(props) {
  const dispatch = useDispatch();
  const { profileState } = useSelector(state => {
    return {
      profileState: state.profile.data,
    };
  });
  const { classes, showSnackBar = () => { } } = props;
  const { loggedInTeam, workspace, activeTeam, teams } = profileState;

  const currentWorkspace = workspace.find(t => t.teamId === loggedInTeam) || defaultTeamObj;
  const currentTeam = teams.find(t => t.companyId === activeTeam) || defaultTeamObj;

  const [checked, setChecked] = useState(currentWorkspace.config.isProjectFieldMandatory);
  const [taskEffortsChecked, setTaskEffortsChecked] = useState(currentWorkspace.config.isUserTasksEffortMandatory);
  const [timeSettingchecked, setTimeSettingchecked] = useState(currentWorkspace.config.isCustomTime);
  const [progressCheck, setProgressCheck] = useState(currentWorkspace.taskProgressCalculation);
  const handleChange = () => {
    if (!currentTeam.config.isProjectFieldMandatory) {
      showSnackBar("Oops! This action cannot be performed because the required project mandatory settings are disabled at the team level", 'error')
      return;
    }
    setChecked(!checked);
    const obj = {
      teamId: currentWorkspace.teamId,
      isProjectFieldMandatory: !checked,
    };
    updateWorkspaceConfig(
      dispatch,
      obj,
      succ => {
        showSnackBar(
          `Configuration is now ${checked ? 'disabled' : 'enabled'} in workspace ${currentWorkspace.teamName}.`,
          'info')
        // console.log("-------------success");
      },
      fail => {
        // console.log("-------------fail");
      }
    );
  };
  const handleChangeTaskEfforts = () => {
    if (!currentTeam.config.isUserTasksEffortMandatory) {
      showSnackBar("Oops! This action cannot be performed because the required task effort reminder settings are disabled at the team level", 'error')
      return;
    }
    setTaskEffortsChecked(!taskEffortsChecked);
    const obj = {
      teamId: currentWorkspace.teamId,
      isUserTasksEffortMandatory: !taskEffortsChecked,
    };
    updateTaskEffortWorkspaceConfig(
      dispatch,
      obj,
      succ => {
        showSnackBar(
          `Configuration is now ${taskEffortsChecked ? 'disabled' : 'enabled'} in workspace ${currentWorkspace.teamName}.`,
          'info')
      },
      fail => { }
    );
  };
  const handleChangeTimeSetting = () => {
    if (!currentTeam.config.isCustomTime) {
      showSnackBar("Oops! This action cannot be performed because the 30 Minutes Time Interval settings is disabled at the team level", 'error')
      return;
    }
    setTimeSettingchecked(!timeSettingchecked);
    const obj = {
      teamId: currentWorkspace.teamId,
      isCustomTime: !timeSettingchecked,
      isCustomTimeApplyOnTimer: true,
      timespan: 30
    };
    updateTimeSettingWorkspaceConfig(
      dispatch,
      obj,
      succ => {
        showSnackBar(
          `Configuration is now ${timeSettingchecked ? 'disabled' : 'enabled'} in workspace ${currentWorkspace.teamName}.`,
          'info')
      },
      fail => { }
    );
  };

  const handleProgressChange = (type, option) => {

    setProgressCheck(option.value);
    const obj = {
      teamId: currentWorkspace.teamId,
      taskProgressCalculation: option.value,
    };
    updateProgressConfig(
      dispatch,
      obj,
      succ => {
        showSnackBar(
          `Configuration is updated in workspace ${currentWorkspace.teamName}.`,
          'info')
      },
      fail => { }
    );
  };
  useEffect(() => {
    setChecked(currentWorkspace.config.isProjectFieldMandatory);
  }, [loggedInTeam]);


  const selectedValue = (dropdownData.find(o => o.value == progressCheck)) || "";

  return (
    <>
      <div className={classes.configCnt}>
        <div className={classes.configText}>
          <p>
            Project selection is mandatory/required when creating a new task
            <CustomTooltip
              size="Large"
              helptext={<>When this setting is on or off, it applies to current workspaces.</>}
              iconType="help"
              position="static"
            />
          </p>
          <DefaultSwitch size="medium" checked={checked} onChange={handleChange} />
        </div>
      </div>
      <div className={classes.configCnt}>
        <div className={classes.configText}>
          <p>
            Remind User to add task efforts before marking it in final status
            <CustomTooltip
              size="Large"
              helptext={<>When enabled, this setting will reminder user to add effort for task when it's status is changed to Final state.</>}
              iconType="help"
              position="static"
            />
          </p>
          <DefaultSwitch size="medium" checked={taskEffortsChecked} onChange={handleChangeTaskEfforts} />
        </div>
      </div>

      <div className={classes.configCnt}>
        <div className={classes.configText}>
          <p>
            Enable 'Time logging' minutes interval to 30
            <CustomTooltip
              size="Large"
              helptext={<>When enabled, this setting will allow users to log time only between 0 and 30 minutes.</>}
              iconType="help"
              position="static"
            />
          </p>
          <DefaultSwitch size="medium" checked={timeSettingchecked} onChange={handleChangeTimeSetting} />
        </div>
      </div>

      {/* total team check */}
      {[
        "07d0428cf16746b8b43dbdada7d11b1b",
        "edfb9312bc4d4332bff52fc3e75a6802",
        "ad318cec84f54ff0b48cb38624a57664",
        "09a2ddd9fbcb4a03bbfe5bcc1a480e48",
        "9892d3cf5f5e4a9786172841dc4646f3"].includes(activeTeam) &&
        <div className={classes.configCnt}>
          <div className={classes.configText}>
            <p>
              Allow progress to be editable
              <CustomTooltip
                size="Large"
                helptext={<>Enabling this setting allows you to enter progress from either task details or project gantt.</>}
                iconType="help"
                position="static"
              />
            </p>
            {/* <DefaultSwitch checked={progressCheck} onChange={handleProgressChange} /> */}
            <SelectSearchDropdown
              data={() => dropdownData}
              label={dropdownData.value}
              styles={{ margin: 0, width: 150 }}
              isMulti={false}
              placeholder={`Select Value`}
              selectChange={handleProgressChange}
              type={'progress'}
              selectedValue={selectedValue}
              isClearable={false}
            />
          </div>
        </div>
      }
    </>
  );
}

export default withStyles(styles, { withTheme: true })(Configuration);
