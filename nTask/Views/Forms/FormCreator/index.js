import React, { useContext, useEffect, useReducer, useState } from "react";
import "survey-core/defaultV2.css";
import "survey-core/survey.i18n";
import SurveyContext from "../Context/form.context";
import reducer from "../Context/reducer";
import initialState from "../Context/initialState";
import FormEditor from "./FormCreator";
import queryString from "query-string";
import { updateState } from "../Context/actions";
import { getAllForms, updateForm, addNewForm, addNewVersion } from "../../../redux/actions/forms";
import { Grid } from "@material-ui/core";
import CustomButton from "../../../components/Buttons/CustomButton";
import DefaultButton from "../../../components/Buttons/DefaultButton";
import CustomDialog from "../../../components/Dialog/CustomDialog";
import DefaultTextField from "../../../components/Form/TextField";
import styles from "./styles";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core";
import { compose } from "redux";

// export default function FormCreator(location) {
function FormCreator(params) {
  const { classes, theme, location } = params;
  const [state, dispatch] = useReducer(reducer, initialState);
  const [open, setOpen] = useState(false);
  const [flag, setFlag] = useState(true);
  const [formVersionDetails, setFormVersionDetails] = useState(null);
  const [versionTitle, setVersionTitle] = useState("");
  const searchQuery = location.search;
  const formId = queryString.parse(searchQuery).id;
  const { view, selectedForm, allForms, assignedForms, allTemplates } = state;
  useEffect(() => {
    handleGetForms();
  }, [])
  useEffect(() => {
    setFlag(true)
    if (formId && allForms.length) {
      const item = allForms.find(x => x.formId == formId)
      updateState(dispatch, { selectedForm: item, view: "new-survey" });
      setFlag(false)
    } else {
      setFlag(false)
    }
  }, [allForms]);

  // get all forms
  const handleGetForms = () => {
    getAllForms(
      (succ) => {
        updateState(dispatch, {
          allForms: succ.data,
        });
        setFlag(false)
      },
      (fail) => {
        console.log("================", fail);
      }
    );
  };
  const closeAction = () => {
    let obj = {
      ...formVersionDetails,
      versionName: "",
    };
    handleSaveFormVersion(obj);
    setOpen(false);
    setFormVersionDetails(null);
    setVersionTitle("");
  };
  const handleChangeTitle = (e) => {
    setVersionTitle(e.target.value);
  };
  const handleSaveVersionName = () => {
    let obj = {
      ...formVersionDetails,
      versionName: versionTitle,
    };
    handleSaveFormVersion(obj);
    setOpen(false);
    setFormVersionDetails(null);
    setVersionTitle("");
  };

  const handleSaveFormVersion = (formData) => {
    if (formData) {
      let obj = formData;
      addNewVersion(
        obj,
        (succ) => {
          updateState(dispatch, {
            selectedForm: obj,
          });
        },
        (fail) => {
          console.log("================", fail);
        }
      );
    }
  };
  // update form
  const updatedFormData = (creator, saveNo, form) => {
    const {
      formSettings: { formVersion },
    } = form;
    if (formVersion) {
      /** if user have enabled its form versioning option */
      setOpen(true);
      setFormVersionDetails({
        ...form,
        mode: '',
        navigateToUrl: '',
        ...creator.JSON,
      });
      // handleSaveFormVersion(creator, saveNo, form);
      return;
    }
    // const updatedformList = allForms.map((item) => {
    //   if (item._id === form._id) return { ...form, ...creator.JSON };
    //   return item;
    // });
    const updatedForm = {
      ...form,
      mode: '',
      maxTimeToFinish: '',
      maxTimeToFinishPage: '',
      navigateToUrl: '',
      ...creator.JSON
    };
    updateForm(
      updatedForm,
      (succ) => {
        updateState(dispatch, {
          // allForms: updatedformList,
          selectedForm: updatedForm,
        });
      },
      (fail) => {
        console.log("================", fail);
      }
    );
  };
  // app new form here
  const addNewFormData = (creator, saveNo, form) => {
    if (form) {
      let obj = {
        ...form,
        ...creator.JSON,
        versionName: "",
      };
      if (obj.isTemplate) delete obj.isTemplate;
      addNewForm(
        obj,
        (succ) => {
          updateState(dispatch, {
            allForms: [...allForms, succ.data],
            selectedForm: succ.data,
          });
        },
        (fail) => {
          console.log("================", fail);
        }
      );
    }
  };
  // on complete form
  const handleComplete = (creator, saveNo, form, thisState) => {
    if (form._id) {
      if (form.isTemplate == undefined) {
        updatedFormData(creator, saveNo, form);
        return;
      }
      addNewFormData(creator, saveNo, form);
      return;
    }
    addNewFormData(creator, saveNo, form);
  };

  return (
    <SurveyContext.Provider
      value={{
        dispatch,
        state,
      }}>
      <div style={{ height: "100%" }}>
        {flag ?
          <div className="loaderContainer">
            <div className="loader"></div>
          </div>
          : <FormEditor json={selectedForm} onComplete={handleComplete} />}
      </div>

      {open && (
        <CustomDialog
          title={"Form Version Details"}
          dialogProps={{
            open: open,
            onClose: closeAction,
          }}>
          <div className={classes.dialogFormCnt}>
            <DefaultTextField
              label={"Version Name"}
              defaultProps={{
                type: "text",
                id: "versionTitle",
                placeholder: "Type version name (optional)",
                // value: versionTitle,
                autoFocus: true,
                inputProps: { maxLength: 80 },
                onChange: (e) => handleChangeTitle(e),
              }}
            />
          </div>
          <Grid
            container
            direction="row"
            justify="flex-end"
            alignItems="center"
            classes={{ container: classes.dialogFormActionsCnt }}>
            <DefaultButton
              onClick={closeAction}
              text={"Close"}
              buttonType="Transparent"
              style={{ marginRight: 20 }}
            />
            <CustomButton
              onClick={(e) => handleSaveVersionName()}
              btnType="green"
              variant="contained"
              query={""}
              disabled={versionTitle == ""}>
              Save
            </CustomButton>
          </Grid>
        </CustomDialog>
      )}
    </SurveyContext.Provider>
  );
}

export default compose(withRouter, withStyles(styles, { withTheme: true }))(FormCreator);
