import React, { useContext, useEffect, useRef, useState } from "react";
import "survey-core/survey.i18n.js";
import "survey-creator-core/survey-creator-core.i18n.js";
// import * as SurveyCreatorCore from "survey-creator-core";
import * as SurveyCreatorReact from "survey-creator-react";
import * as SurveyReact from "survey-react-ui";
import * as Survey from "survey-core";
import "survey-core/defaultV2.css";
import "survey-creator-core/survey-creator-core.css";
import SurveyContext from "../Context/form.context";
import SettingsView from "./settings/settings.view";
import Loader from "../../../components/Loader/Loader";

export default function FormEditor(props) {
  const { state, dispatch } = useContext(SurveyContext);
  let [creator, setCreator] = useState();
  let [flag, setFlag] = useState(true);
  let surveyObj = useRef(null);

  const createSurveyCreator = () => {
    if (creator === undefined) {
      let creatorOptions = {
        showEmbededSurveyTab: false,
        showLogicTab: true,
        showTranslationTab: false,
        isAutoSave: false,
        showJSONEditorTab: false,
      };
      creator = new SurveyCreatorReact.SurveyCreator(creatorOptions);
      // creator.toolboxLocation = "left";
      creator.saveSurveyFunc = (saveNo, callback) => {
        props.onComplete(creator, saveNo, surveyObj.current);
        callback(saveNo, true);
      };
      const templatesPlugin = {
        activate: () => { },
        deactivate: () => {
          return true;
        },
      };

      if (state.selectedForm?._id) {

        creator.addPluginTab("settings", templatesPlugin, "Settings", "svc-tab-settings", 3);

        SurveyReact.ReactElementFactory.Instance.registerElement("svc-tab-settings", (props) => {
          return React.createElement(SettingsView, props);
        });
      }
      // creator.addPluginTab("goBack", templatesPlugin, "Go Back", "svc-tab-settings", 0);
      const blackList = [
        "clearInvisibleValues",
        "textUpdateMode",
        "sendResultOnPageNext",
        "storeOthersAsComment",
      ];

      creator.onShowingProperty.add(function (_, options) {
        const value = options.obj.getType();
        if (value == "survey") {
          // Hide properties found in `blackList`
          // console.log(options.property.name);
          options.canShow = blackList.indexOf(options.property.name) < 0;
        }
      });

      // creator.onDefineElementMenuItems.add((sender, options) => {
      //   const questionTypeObj = options.items.find(item => item.innerItem.id == "convertTo");
      //   var barItem = {
      //     id: "linkCustomFields", // Unique identifier
      //     css: new Survey.ComputedUpdater(() => (
      //       true
      //         ? "sv-action-bar-item--secondary"
      //         : ""
      //     )),
      //     title: "Link Custom Fields",
      //     // Action text
      //     // The icon depends on the property value
      //     iconName: new Survey.ComputedUpdater(() => {
      //       return "linkCfSvg";
      //     }),
      //     iconSize: 18,
      //     // Change the property value on a click
      //     action: (actionObj) => {
      //       console.log("=======handle Click button", { name: options.obj.propertyHash.name, type: questionTypeObj.innerItem.title })
      //     }
      //   };
      //   if (questionTypeObj)
      //     options.items.push(barItem);
      // });

      setCreator(creator);
    }
    if (creator && !creator.plugins.settings && state.selectedForm?._id) {
      const templatesPlugin = {
        activate: () => { },
        deactivate: () => {
          return true;
        },
      };
      creator.addPluginTab("settings", templatesPlugin, "Settings", "svc-tab-settings", 3);

      SurveyReact.ReactElementFactory.Instance.registerElement("svc-tab-settings", (props) => {
        return React.createElement(SettingsView, props);
      });

      setCreator(creator);
    }
    setFlag(false);
  }

  useEffect(() => {
    surveyObj.current = { ...state.selectedForm };
  }, [state.selectedForm]);

  useEffect(() => {
    createSurveyCreator();
  }, [state.selectedForm])

  if (creator) {
    creator.JSON = { ...state.selectedForm };

    return (
      // calc(100vh - 350px)
      // calc(100vh - 145px) height adjustment with view height
      <div style={{ position: 'relative', height: "calc(100% - 74px)" }}>
        <SurveyCreatorReact.SurveyCreatorComponent
          creator={creator}></SurveyCreatorReact.SurveyCreatorComponent>

        <svg style={{ 'display': 'none' }} xmlns="http://www.w3.org/2000/svg" >
          <symbol viewBox="0 0 24 24" id="linkCfSvg">
            <g stroke="#ff9814" fill="none" stroke-width="3" stroke-linecap="round" stroke-linejoin="round">
              <path d="M15 7h3a5 5 0 0 1 5 5 5 5 0 0 1-5 5h-3m-6 0H6a5 5 0 0 1-5-5 5 5 0 0 1 5-5h3" /><line x1="8" y1="12" x2="16" y2="12" />
            </g>
          </symbol>
        </svg>
        {flag ?
        <Loader
          styles={{
            height: '100%',
            position: 'absolute',
            top: 0,
            left: 0,
            background: '#e5dddd61',
          }}
        /> : null}
      </div>
    );
  }
  return <></>
}
