import { SvgIcon } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import CloseIcon from "@material-ui/icons/Cancel";
import OpenIcon from "@material-ui/icons/Launch";
import LockIcon from "@material-ui/icons/Lock";
import PublicIcon from "@material-ui/icons/Public";
import clsx from "clsx";
import { City, Country, State } from "country-state-city";
import React, { useContext, useMemo } from "react";
import { CopyToClipboard } from "react-copy-to-clipboard";
import { FormattedMessage } from "react-intl";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
import { v4 as uuidv4 } from "uuid";
import isEmail from "validator/lib/isEmail";
import CustomButton from "../../../../components/Buttons/CustomButton";
import CreateableSelectDropdown from "../../../../components/Dropdown/SelectCreateableDropdown/SelectCreateableDropdown";
import SelectSearchDropdown from "../../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import DefaultSwitch from "../../../../components/Form/Switch";
import DefaultTextField from "../../../../components/Form/TextField";
import CopyIcon from "../../../../components/Icons/IconCopyContent";
import NotificationMessages from "../../../../components/NotificationMessages/NotificationMessages";
import { generateAssigneeData } from "../../../../helper/generateSelectData";
import { generateActiveMembers } from "../../../../helper/getActiveMembers";
import { updateForm } from "../../../../redux/actions/forms";
import { updateState } from "../../Context/actions";
import SurveyContext from "../../Context/form.context";
import styles from "../styles";

function PrivacySettings(params) {
  const { classes, theme, history } = params;
  const {
    state: { selectedForm },
    dispatch,
  } = useContext(SurveyContext);

  const countries = Country.getAllCountries();
  const { formSettings, formId, apiKey = "", secretKey = "" } = selectedForm;

  const formUrl = `https://d20dboq8f45o06.cloudfront.net?i=${formId}&a=${apiKey}&t=${secretKey}`;

  const members = generateActiveMembers() || [];

  const { locationSettings, privacySettings, limitedUser, locationInfo } = formSettings;

  const handleChangePrivacySettings = (key, value) => {
    const updatedSurvey = {
      ...selectedForm,
      formSettings: { ...formSettings, [key]: value, ["limitedUser"]: [] },
    };
    updateState(dispatch, { selectedForm: updatedSurvey });
    handleUpdateForm(updatedSurvey);
  };
  const handleChangeSettings = (key, value, extraKeys) => {
    const updatedSurvey = {
      ...selectedForm,
      formSettings: { ...formSettings, [key]: value, ...extraKeys },
    };
    updateState(dispatch, { selectedForm: updatedSurvey });
    handleUpdateForm(updatedSurvey);
  };
  const handleOptionsSelect = (type, option) => {
    const usersEmails = option.map((item) => item.obj.email);
    handleChangeSettings("limitedUser", usersEmails);
  };
  const handleCreateOption = (type, option) => {
    handleChangeSettings("limitedUser", [...limitedUser, option.value]);
  };
  const handleRemoveOption = (option) => {
    let val = "";
    if (option.obj) val = option.obj.email;
    else val = option.value;
    const updatedEmails = limitedUser.filter((item) => item !== val);
    handleChangeSettings("limitedUser", updatedEmails);
  };
  const handleMembers = () => {
    return generateAssigneeData(members);
  };
  const selectedLimitedUser = limitedUser.reduce((result, cv) => {
    const isExist = members.find((item) => item.email == cv);
    if (isExist) {
      result.push(isExist);
    } else {
      result.push({
        fullName: cv,
        email: cv,
        userId: cv,
        isActive: true,
        isDeleted: false,
      });
    }
    return result;
  }, []);

  const handleCountryCode = useMemo(() => {
    return countries.map((f) => {
      return {
        label: f.name,
        value: f.phonecode,
        code: f.isoCode,
        obj: f,
        icon: (
          <img
            style={{ width: "20px", height: "15px", marginRight: "5px" }}
            src={`https://flagpedia.net/data/flags/normal/${f.isoCode.toLowerCase()}.png`}
          />
        ),
      };
    });
  }, [countries]);

  const generateDropDownData = (data) => {
    return data.map((f) => {
      return {
        label: f.name,
        value: f.isoCode || f.name,
        code: f.countryCode,
        obj: f,
      };
    });
  };

  const handleUpdateForm = (data) => {
    updateForm(
      data,
      (succ) => {},
      (fail) => {
        console.log("================", fail);
      }
    );
  };
  const handleGetCity = (countryCode, stateCode) => {
    const citiesOfStateArr = City.getCitiesOfState(countryCode, stateCode);
    let generateData = generateDropDownData(citiesOfStateArr);
    return generateData;
  };
  const handleGetStates = (code) => {
    const statesOfCountryArr = State.getStatesOfCountry(code);
    let generateData = generateDropDownData(statesOfCountryArr);
    return generateData;
  };
  const handleSelect = (type, option, id) => {
    console.log(type, option);
    let updatedData = locationInfo.map((item) => {
      if (item.id === id) return { ...item, [type]: option.label };
      return item;
    });
    handleChangeSettings("locationInfo", updatedData);
  };
  const handleOpenForm = () => {
    window.open(`/form/fill?id=${formId}`, "_blank");
    // window.open(formUrl, "_blank");
  };
  const getEmptyLocaInfo = () => {
    return {
      id: uuidv4(),
      country: "",
      state: "",
      city: "",
    };
  };
  const handleAddLocation = () => {
    let data = [...locationInfo];
    data.push(getEmptyLocaInfo());
    handleChangeSettings("locationInfo", data);
  };
  const handleDeleteLocation = (id) => {
    let updatedData = locationInfo.filter((item) => item.id !== id);
    handleChangeSettings("locationInfo", updatedData);
  };

  return (
    <>
      <h3 className={classes.heading}>Privacy Settings</h3>
      <div className={classes.settingContainer}>
        <div className={classes.privacySettings}>
          <div className={classes.stateWrapper}>
            <div className={classes.stateWrapper}>
              <div
                className={clsx({
                  [classes.stateItem]: true,
                  [classes.stateItemActive]: privacySettings == "public",
                })}>
                <div
                  className={classes.wrapper}
                  onClick={(event) => {
                    event.stopPropagation();
                    handleChangePrivacySettings("privacySettings", "public");
                  }}>
                  <PublicIcon className={classes.privacyIcons} />
                  <div className={classes.shareItemTitle}>
                    <span className={classes.title}>Public</span>
                    <span className={classes.text}>
                      People can see this form, those have this form url shared with".
                    </span>
                  </div>
                </div>
              </div>
              {privacySettings == "public" && (
                <div className={classes.stateWrapper}>
                  <div className={classes.userWrapper}>
                    <div className={classes.publicLinkInputCnt}>
                      <DefaultTextField
                        label={<span className={classes.label}>Form URL</span>}
                        error={false}
                        formControlStyles={{ marginBottom: 0, marginTop: 0 }}
                        noRightBorder={true}
                        defaultProps={{
                          id: "formUrl",
                          value: formUrl,
                          readOnly: true,
                          disabled: false,
                        }}
                      />
                      <CopyToClipboard text={formUrl}>
                        <CustomButton
                          variant="contained"
                          btnType="gray"
                          disableRipple
                          disabled={false}
                          style={{
                            borderRadius: "0",
                            borderLeft: 0,
                            minWidth: 105,
                            fontFamily: theme.typography.fontFamilyLato,
                          }}>
                          <SvgIcon className={classes.copyIcon} viewBox="0 0 14 16.211">
                            <CopyIcon />
                          </SvgIcon>
                          <FormattedMessage
                            id="common.action.copy-link.label"
                            defaultMessage="Copy Link"
                          />
                        </CustomButton>
                      </CopyToClipboard>
                      <CustomButton
                        variant="contained"
                        btnType="gray"
                        disableRipple
                        onClick={(e) => {
                          e.stopPropagation();
                          handleOpenForm();
                        }}
                        disabled={false}
                        style={{
                          borderRadius: "0 4px 4px 0",
                          borderLeft: 0,
                          minWidth: 105,
                          fontFamily: theme.typography.fontFamilyLato,
                        }}>
                        <OpenIcon className={classes.copyIcon} /> Open
                      </CustomButton>
                    </div>
                  </div>
                </div>
              )}
            </div>
            <div
              className={classes.stateWrapper}
              onClick={(event) => {
                event.stopPropagation();
                handleChangePrivacySettings("privacySettings", "private");
              }}>
              <div
                className={clsx({
                  [classes.stateItem]: true,
                  [classes.stateItemActive]: privacySettings == "private",
                })}>
                <div className={classes.wrapper}>
                  <LockIcon className={classes.privacyIcons} />
                  <div className={classes.shareItemTitle}>
                    <span className={classes.title}>Private</span>
                    <span className={classes.text}>Only You. No one else can view this form.</span>
                  </div>
                </div>
              </div>
            </div>
            <div
              className={classes.stateWrapper}
              onClick={(event) => {
                event.stopPropagation();
                handleChangePrivacySettings("privacySettings", "limited");
              }}>
              <div
                className={clsx({
                  [classes.stateItem]: true,
                  [classes.stateItemActive]: privacySettings == "limited",
                })}>
                <div className={classes.wrapper}>
                  <LockIcon className={classes.privacyIcons} />
                  <div className={classes.shareItemTitle}>
                    <span className={classes.title}>Limited</span>
                    <span className={classes.text}>Only users listed here can view this form.</span>
                  </div>
                </div>
              </div>
            </div>
            {privacySettings == "limited" && (
              <div className={classes.stateWrapper}>
                <div className={classes.userWrapper}>
                  <CreateableSelectDropdown
                    data={handleMembers}
                    label={
                      <span className={classes.label}>User listed here will see this form</span>
                    }
                    placeholder={"Search or add new email address"}
                    id=""
                    name=""
                    selectOptionAction={handleOptionsSelect}
                    createOptionAction={handleCreateOption}
                    removeOptionAction={handleRemoveOption}
                    type="task"
                    createText="Add email"
                    selectedValue={generateAssigneeData(selectedLimitedUser)}
                    createLabelValidation={(value) => {
                      return isEmail(value);
                    }}
                    noOptionMessage={
                      "Either you dont have any resource in workspace or add new email"
                    }
                    avatar={true}
                  />
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
      <h3 className={classes.heading}>Location Settings</h3>
      <div>
        <DefaultSwitch
          checked={locationSettings}
          onChange={(event) => {
            if (locationSettings) {
              handleChangeSettings("locationSettings", !locationSettings, {
                locationInfo: [],
              });
              return;
            }
            handleChangeSettings("locationSettings", !locationSettings, {
              locationInfo: [getEmptyLocaInfo()],
            });
          }}
          value={locationSettings}
        />
        <span className={classes.settingLabel}> Enabled</span>
      </div>
      <div className={classes.locationInfoCnt}>
        {locationSettings && locationInfo?.length ? (
          locationInfo.map((item) => {
            const selectedCountry = handleCountryCode.filter((c) => c.label === item.country);
            const statesArr = selectedCountry.length
              ? handleGetStates(selectedCountry[0].code)
              : [];
            const selectedState = statesArr.length
              ? statesArr.filter((s) => s.label === item.state)
              : [];
            const citiesArr = selectedState.length
              ? handleGetCity(selectedState[0].code, selectedState[0].value)
              : [];
            const selectedCity = citiesArr.length
              ? citiesArr.filter((cit) => cit.label === item.city)
              : [];
            return (
              <>
                <div className={classes.locationActions} key={item.id}>
                  <SelectSearchDropdown
                    data={() => handleCountryCode}
                    placeholder={"Select Country"}
                    icon={true}
                    label={""}
                    isMulti={false}
                    selectChange={(type, option) => {
                      handleSelect(type, option, item.id);
                    }}
                    type={"country"}
                    selectedValue={selectedCountry}
                    styles={{
                      marginTop: 10,
                      marginBottom: 0,
                    }}
                    customStyles={{}}
                    isDisabled={false}
                    writeFirst={true}
                  />
                  {statesArr.length ? (
                    <CreateableSelectDropdown
                      data={statesArr}
                      label={""}
                      styles={{ marginTop: 10, marginBottom: 0, marginLeft: 10 }}
                      selectOptionAction={(type, option) => {
                        handleSelect(type, option, item.id);
                      }}
                      type={"state"}
                      selectedValue={selectedState}
                      placeholder={"Select State"}
                      icon={false}
                      isMulti={false}
                      acceptDataInFormOfFun={false}
                      isDisabled={statesArr.length == 0}
                    />
                  ) : (
                    <></>
                  )}
                  {citiesArr.length ? (
                    <CreateableSelectDropdown
                      data={citiesArr}
                      label={""}
                      styles={{ marginTop: 10, marginBottom: 0, marginLeft: 10 }}
                      selectOptionAction={(type, option) => {
                        handleSelect(type, option, item.id);
                      }}
                      type={"city"}
                      selectedValue={selectedCity}
                      placeholder={"Select City"}
                      icon={false}
                      isMulti={false}
                      acceptDataInFormOfFun={false}
                      isDisabled={citiesArr.length == 0}
                    />
                  ) : (
                    <></>
                  )}
                  {locationInfo.length > 1 ? (
                    <CloseIcon
                      className={classes.cancelIcon2}
                      onClick={() => handleDeleteLocation(item.id)}
                    />
                  ) : (
                    <></>
                  )}
                </div>
              </>
            );
          })
        ) : (
          <></>
        )}
      </div>
      {locationSettings && (
        <span
          onClick={() => {
            handleAddLocation();
          }}
          className={classes.addLocTxt}>
          Add Location
        </span>
      )}
      <NotificationMessages
        type="NewInfo"
        iconType="info"
        style={{
          width: "100%",
          textAlign: "start",
          fontSize: "12px",
          padding: "6px",
          backgroundColor: "#EAEAEA",
          borderRadius: 4,
          fontFamily: theme.typography.fontFamilyLato,
          fontWeight: 400,
          marginTop: 10,
          maxWidth: 300,
          marginLeft: 12,
        }}>
        Note: Location may not be 100% accurate.
      </NotificationMessages>
    </>
  );
}
PrivacySettings.defaultProps = {
  classes: {},
  theme: {},
};
export default compose(withRouter, withStyles(styles, { withTheme: true }))(PrivacySettings);
