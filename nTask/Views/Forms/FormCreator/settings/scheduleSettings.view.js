import { InputAdornment } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import CloseIcon from "@material-ui/icons/Cancel";
import clsx from "clsx";
import moment from "moment";
import React, { useContext, useState } from "react";
import { injectIntl } from "react-intl";
import { compose } from "redux";
import ColorImagesModal from "../../../../components/ColorImagesModal";
import CustomDatePicker from "../../../../components/DatePicker/DatePicker/DatePicker";
import ColorPickerDropdown from "../../../../components/Dropdown/ColorPicker/Dropdown";
import DefaultSwitch from "../../../../components/Form/Switch";
import DefaultTextField from "../../../../components/Form/TextField";
import { updateForm } from "../../../../redux/actions/forms";
import { updateState } from "../../Context/actions";
import SurveyContext from "../../Context/form.context";
import styles from "../styles";
import ImageIcon from "@material-ui/icons/ImageSearch";

function GeneralSettings(params) {
  const { classes, intl, theme } = params;
  const {
    state: { selectedForm },
    dispatch,
  } = useContext(SurveyContext);
  const { formSettings } = selectedForm;

  const {
    publishSettings,
    publishCustomBranding,
    publishDate,
    publishTime,
    publishRedirectUrl,
    publishRedirectBtnText,
    publishDescription,
    publishBgColor,
    publishFontColor,
    publishRedirectBtnColor,
    publishLogoUrl,
    publishLogoTagline,
    publishBgImg,

    unPublishSettings,
    unPublishCustomBranding,
    unPublishRedirectUrl,
    unPublishDate,
    unPublishTime,
    unPublishLogoUrl,
    unPublishLogoTagline,
    unPublishRedirectBtnText,
    unPublishRedirectBtnColor,
    unPublishBgImg,
    unPublishBgColor,
    unPublishFontColor,
    unPublishDescription,
    unPublishIllustration,
  } = formSettings;
  const [colorEl, setColorEl] = useState(null);
  const [fontColorEl, setFontColorEl] = useState(null);
  const [publishPageImg, setPublishPageImg] = useState(null);
  const [unPublishColorEl, setUnPublishColorEl] = useState(null);
  const [unPublishFontColorEl, setUnPublishFontColorEl] = useState(null);
  const [unPublishPageImg, setUnPublishPageImg] = useState(null);

  const handleChangeSettings = (key, value, otherParams) => {
    const updatedSurvey = {
      ...selectedForm,
      formSettings: { ...formSettings, [key]: value, ...otherParams },
    };
    updateState(dispatch, { selectedForm: updatedSurvey });
    handleUpdateForm(updatedSurvey);
  };
  const handleSelectDate = (key, date, time) => {
    const updatedSurvey = {
      ...selectedForm,
      formSettings: { ...formSettings, ["publishDate"]: date, ["publishTime"]: time },
    };
    updateState(dispatch, { selectedForm: updatedSurvey });
    handleUpdateForm(updatedSurvey);
  };
  const handleUnPublishDate = (key, date, time) => {
    const updatedSurvey = {
      ...selectedForm,
      formSettings: { ...formSettings, ["unPublishDate"]: date, ["unPublishTime"]: time },
    };
    updateState(dispatch, { selectedForm: updatedSurvey });
    handleUpdateForm(updatedSurvey);
  };
  const handleUpdateForm = (data) => {
    updateForm(
      data,
      (succ) => { },
      (fail) => {
        console.log("================", fail);
      }
    );
  };
  const handleCloseColor = () => {
    setColorEl(null);
  };
  const handleCloseUnPublishFontEl = () => {
    setUnPublishFontColorEl(null);
  };
  const handleCloseUnPublishColor = () => {
    setUnPublishColorEl(null);
  };
  const handleCloseFontColor = () => {
    setFontColorEl(null);
  };
  const handleSelectColor = (e) => {
    e.stopPropagation();
    setColorEl(e.currentTarget);
  };
  const handleSelectUnpublishFontEl = (e) => {
    e.stopPropagation();
    setUnPublishFontColorEl(e.currentTarget);
  };
  const handleSelectUnpublishColor = (e) => {
    e.stopPropagation();
    setUnPublishColorEl(e.currentTarget);
  };
  const handleSelectPageFontColor = (e) => {
    e.stopPropagation();
    setFontColorEl(e.currentTarget);
  };
  const handleUnPublishPageImg = (e) => {
    e.stopPropagation();
    setUnPublishPageImg(e.currentTarget);
  };
  const handlePublishPageImg = (e) => {
    e.stopPropagation();
    setPublishPageImg(e.currentTarget);
  };
  const handleUnPublishImageSelect = (selectedImage) => {
    setUnPublishPageImg(null);
    handleChangeSettings("unPublishBgImg", selectedImage.urls.full, {
      unPublishBgColor: null,
    });
  };
  const handleImageSelect = (selectedImage) => {
    setPublishPageImg(null);
    handleChangeSettings("publishBgImg", selectedImage.urls.full, {
      publishBgColor: null,
    });
  };
  const handleUnPublishColorSelect = (selectedColor) => {
    setUnPublishPageImg(null);
    handleChangeSettings("unPublishBgColor", selectedColor, {
      unPublishBgImg: null,
    });
  };
  const handleColorSelect = (selectedColor) => {
    setPublishPageImg(null);
    handleChangeSettings("publishBgColor", selectedColor, {
      publishBgImg: null,
    });
  };
  const handleUnPublishClickAway = () => {
    setUnPublishPageImg(null);
  };
  const handleClickAway = () => {
    setPublishPageImg(null);
  };
  return (
    <>
      <h3 className={classes.heading}>Publish Date Settings</h3>
      <div>
        <DefaultSwitch
          checked={publishSettings}
          onChange={(event) => handleChangeSettings("publishSettings", !publishSettings)}
          value={publishSettings}
        />
        <span className={classes.settingLabel}> Enabled</span>
      </div>

      <Grid
        container
        spacing={2}
        classes={{
          container: clsx({
            [classes.sectionCnt]: true,
            [classes.disableContainer]: !publishSettings,
          }),
        }}>
        <Grid item sm={12} md={12}>
          <CustomDatePicker
            date={publishDate && moment(publishDate)}
            dateFormat="MMM DD, YYYY"
            timeInput={true}
            onSelect={(date, time) => handleSelectDate("publishDate", date, time)}
            selectedTime={publishTime}
            style={{
              marginBottom: 5,
            }}
            label={"Publish Date & Time"}
            icon={false}
            PopperProps={{ size: null }}
            filterDate={moment()}
            timeInputLabel={"Select Time"}
            btnProps={{
              className: classes.datePickerCustomStyle,
            }}
            deleteIcon={false}
            placeholder={"Select Date"}
            containerProps={{
              className: classes.datePickerContainer,
            }}
            disabled={false}
            deleteIconProps={{
              style: {
                marginLeft: 10,
              },
            }}
            labelProps={{
              className: classes.label,
            }}
            datePickerProps={{
              filterDate: (date) => {
                return unPublishDate && unPublishSettings
                  ? moment(date).isBefore(unPublishDate, "day") || moment(date).isSame(unPublishDate, "day")
                  : true;
              },
            }}
          />
        </Grid>
      </Grid>
      <Grid
        container
        spacing={2}
        classes={{
          container: clsx({
            [classes.subSectionCnt]: true,
            [classes.disableContainer]: !publishSettings,
          }),
        }}>
        <Grid item sm={12} md={12} style={{ paddingLeft: 0 }}>
          <DefaultSwitch
            checked={publishCustomBranding}
            onChange={(event) => handleChangeSettings("publishCustomBranding", !publishCustomBranding)}
            value={publishCustomBranding}
          />
          <span className={classes.settingLabel}> Custom Branding</span>
        </Grid>
        <Grid item sm={12} md={6}
          classes={{
            item: clsx({
              [classes.disableContainer]: !publishCustomBranding,
            }),
          }}>
          <DefaultTextField
            label={"Logo URL"}
            error={false}
            defaultProps={{
              type: "text",
              id: "publishLogoUrl",
              placeholder: "Add your logo url (optional)",
              value: publishLogoUrl,
              autoFocus: false,
              inputProps: { maxLength: 250 },
              onChange: (event) => {
                handleChangeSettings("publishLogoUrl", event.target.value);
              },
            }}
            formControlStyles={{
              marginBottom: 0,
            }}
          />
        </Grid>
        <Grid item sm={12} md={6} classes={{
          item: clsx({
            [classes.disableContainer]: !publishCustomBranding,
          }),
        }}>
          <DefaultTextField
            label={"Logo Tagline"}
            error={false}
            defaultProps={{
              type: "text",
              id: "publishLogoTagline",
              placeholder: "Add your logo tagline/slogan (optional)",
              value: publishLogoTagline,
              autoFocus: false,
              inputProps: { maxLength: 250 },
              onChange: (event) => {
                handleChangeSettings("publishLogoTagline", event.target.value);
              },
            }}
            formControlStyles={{
              marginBottom: 0,
            }}
          />
        </Grid>
        <Grid item sm={12} md={6} classes={{
          item: clsx({
            [classes.disableContainer]: !publishCustomBranding,
          }),
        }}>
          <DefaultTextField
            label={"Redirect URL"}
            error={false}
            defaultProps={{
              type: "text",
              id: "publishRedirectUrl",
              placeholder: "Add any redirect url (optional)",
              value: publishRedirectUrl,
              autoFocus: false,
              inputProps: { maxLength: 250 },
              onChange: (event) => {
                handleChangeSettings("publishRedirectUrl", event.target.value);
              },
            }}
            formControlStyles={{
              marginBottom: 0,
            }}
          />
        </Grid>
        <Grid item sm={12} md={6} classes={{
          item: clsx({
            [classes.disableContainer]: !publishCustomBranding,
          }),
        }}>
          <DefaultTextField
            label={<span className={classes.label}>Redirect Button Text</span>}
            error={false}
            defaultProps={{
              type: "text",
              id: "publishRedirectBtnText",
              placeholder: "The text will display on redirect button (optional)",
              value: publishRedirectBtnText,
              autoFocus: false,
              inputProps: { maxLength: 250 },
              onChange: (event) => {
                handleChangeSettings("publishRedirectBtnText", event.target.value);
              },
            }}
            formControlStyles={{
              marginBottom: 0,
            }}
          />
        </Grid>
        <Grid item sm={12} md={6} classes={{
          item: clsx({
            [classes.disableContainer]: !publishCustomBranding,
          }),
        }}>
          <ColorPickerDropdown
            onSelect={(color) => {
              handleChangeSettings("publishRedirectBtnColor", color);
            }}
            obj={{}}
            singleSelect={true}
            handleCloseColor={handleCloseColor}
            customColorBtnRender={
              <div onClick={handleSelectColor} className={classes.textFieldCnt}>
                <DefaultTextField
                  label={<span className={classes.label}>Redirect Button Color</span>}
                  error={false}
                  defaultProps={{
                    id: "publishRedirectBtnColor",
                    value: publishRedirectBtnColor,
                    readOnly: true,
                    disabled: false,
                    placeholder: "Choose redirect button background color (optional)",
                  }}
                  formControlStyles={{
                    marginBottom: 0,
                  }}
                />
                {publishRedirectBtnColor && (
                  <div
                    className={classes.cancelCnt}
                    onClick={() => {
                      handleChangeSettings("publishRedirectBtnColor", "");
                    }}>
                    <CloseIcon className={classes.cancelIcon} />
                  </div>
                )}
              </div>
            }
            anchorEl={colorEl}
          />
        </Grid>
        <Grid item sm={12} md={6} classes={{
          item: clsx({
            [classes.disableContainer]: !publishCustomBranding,
          }),
        }}>
          <ColorPickerDropdown
            onSelect={(color) => {
              handleChangeSettings("publishFontColor", color);
            }}
            obj={{}}
            singleSelect={true}
            handleCloseColor={handleCloseFontColor}
            customColorBtnRender={
              <div onClick={handleSelectPageFontColor} className={classes.textFieldCnt}>
                <DefaultTextField
                  label={<span className={classes.label}>Page Font Color</span>}
                  error={false}
                  defaultProps={{
                    type: "text",
                    id: "publishFontColor",
                    placeholder: "Choose page font color (optional)",
                    value: publishFontColor,
                    readOnly: true,
                    disabled: false,
                  }}
                  formControlStyles={{
                    marginBottom: 0,
                  }}
                />
                {publishFontColor && (
                  <div
                    className={classes.cancelCnt}
                    onClick={() => {
                      handleChangeSettings("publishFontColor", "");
                    }}>
                    <CloseIcon className={classes.cancelIcon} />
                  </div>
                )}
              </div>
            }
            anchorEl={fontColorEl}
          />
        </Grid>
        <Grid item sm={12} md={12} classes={{
          item: clsx({
            [classes.disableContainer]: !publishCustomBranding,
          }),
        }}>
          <div className={classes.textFieldCnt}>
            <DefaultTextField
              label={<span className={classes.label}>Page Background Image/Color</span>}
              error={false}
              defaultProps={{
                type: "text",
                id: "publishBgImg",
                placeholder: "Choose page background image/color from library or paste your custom image url here (optional)",
                value: publishBgImg || publishBgColor,
                readOnly: false,
                disabled: false,
                onChange: e =>  handleChangeSettings("publishBgImg", e.target.value, {
                  publishBgColor: null,
                }),
                startAdornment: (
                  <div onClick={handlePublishPageImg}>
                  <InputAdornment position="start">
                  <ImageIcon
                    htmlColor={theme.palette.secondary.light}
                    style={{ cursor: "pointer" }}
                  />
                </InputAdornment>
                  </div>
                ),
              }}
              formControlStyles={{
                marginBottom: 0,
              }}
            />
            {(publishBgImg || publishBgColor) && (
              <div
                className={classes.cancelCnt}
                onClick={(e) => {
                  handleChangeSettings("publishBgImg", "", {
                    publishBgColor: "",
                  });
                }}>
                <CloseIcon className={classes.cancelIcon} />
              </div>
            )}
          </div>
          {publishPageImg && (
            <ColorImagesModal
              anchorTarget={publishPageImg}
              handleImageSelect={handleImageSelect}
              handleColorSelect={handleColorSelect}
              handleClickAway={handleClickAway}
              intl={intl}
              useProjectStatusColor={false}
              viewMode="new"
              handleClickStatusSelect={() => { }}
            />
          )}
        </Grid>
        <Grid item sm={12} md={12} classes={{
          item: clsx({
            [classes.disableContainer]: !publishCustomBranding,
          }),
        }}>
          <DefaultTextField
            label={<span className={classes.label}>Description</span>}
            fullWidth={true}
            error={false}
            formControlStyles={{ marginTop: 15, marginBottom: 0 }}
            defaultProps={{
              onChange: (event) => {
                handleChangeSettings("publishDescription", event.target.value);
              },
              onBlur: (event) => {
                handleChangeSettings("publishDescription", event.target.value);
              },
              value: publishDescription,
              multiline: true,
              rows: 8,
              autoFocus: false,
              placeholder: "Add any description (optional)",
              inputProps: { maxLength: 2000 },
            }}
          />
        </Grid>
      </Grid>


      <h3 className={classes.heading}>Unpublish Date Settings</h3>
      <div>
        <DefaultSwitch
          checked={unPublishSettings}
          onChange={(event) => handleChangeSettings("unPublishSettings", !unPublishSettings)}
          value={unPublishSettings}
        />
        <span className={classes.settingLabel}> Enabled</span>
      </div>
      <Grid
        container
        spacing={2}
        classes={{
          container: clsx({
            [classes.sectionCnt]: true,
            [classes.disableContainer]: !unPublishSettings,
          }),
        }}>
        <Grid item sm={12} md={12}>
          <CustomDatePicker
            date={unPublishDate && moment(unPublishDate)}
            dateFormat="MMM DD, YYYY"
            timeInput={true}
            onSelect={(date, time) => handleUnPublishDate("unPublishDate", date, time)}
            selectedTime={unPublishTime}
            style={{
              marginBottom: 5,
            }}
            label={"Unpublish Date & Time"}
            icon={false}
            PopperProps={{ size: null }}
            filterDate={moment()}
            timeInputLabel={"Select Time"}
            btnProps={{
              className: classes.datePickerCustomStyle,
            }}
            deleteIcon={false}
            placeholder={"Select Date"}
            containerProps={{
              className: classes.datePickerContainer,
            }}
            disabled={false}
            deleteIconProps={{
              style: {
                marginLeft: 10,
              },
            }}
            labelProps={{
              className: classes.label,
            }}
            datePickerProps={{
              filterDate: (date) => {
                return publishDate && publishSettings
                  ? moment(date).isAfter(publishDate, "day") || moment(date).isSame(publishDate, "day")
                  : true;
              },
            }}
          />
        </Grid>
      </Grid>
      <Grid
        container
        spacing={2}
        classes={{
          container: clsx({
            [classes.subSectionCnt]: true,
            [classes.disableContainer]: !unPublishSettings,
          }),
        }}>
        <Grid item sm={12} md={12} style={{ paddingLeft: 0 }}>
          <DefaultSwitch
            checked={unPublishCustomBranding}
            onChange={(event) => handleChangeSettings("unPublishCustomBranding", !unPublishCustomBranding)}
            value={unPublishCustomBranding}
          />
          <span className={classes.settingLabel}> Custom Branding</span>
        </Grid>
        <Grid item sm={12} md={6} classes={{
          item: clsx({
            [classes.sectionCnt]: true,
            [classes.disableContainer]: !unPublishCustomBranding,
          }),
        }}>
          <DefaultTextField
            label={"Logo URL"}
            error={false}
            defaultProps={{
              type: "text",
              id: "unPublishLogoUrl",
              placeholder: "Add your logo url (optional)",
              value: unPublishLogoUrl,
              autoFocus: false,
              inputProps: { maxLength: 250 },
              onChange: (event) => {
                handleChangeSettings("unPublishLogoUrl", event.target.value);
              },
            }}
            formControlStyles={{
              marginBottom: 0,
            }}
          />
        </Grid>
        <Grid item sm={12} md={6} classes={{
          item: clsx({
            [classes.sectionCnt]: true,
            [classes.disableContainer]: !unPublishCustomBranding,
          }),
        }}>
          <DefaultTextField
            label={"Logo Tagline"}
            error={false}
            defaultProps={{
              type: "text",
              id: "unPublishLogoTagline",
              placeholder: "Add your logo tagline/slogan (optional)",
              value: unPublishLogoTagline,
              autoFocus: false,
              inputProps: { maxLength: 250 },
              onChange: (event) => {
                handleChangeSettings("unPublishLogoTagline", event.target.value);
              },
            }}
            formControlStyles={{
              marginBottom: 0,
            }}
          />
        </Grid>
        <Grid item sm={12} md={12} classes={{
          item: clsx({
            [classes.sectionCnt]: true,
            [classes.disableContainer]: !unPublishCustomBranding,
          }),
        }}>
          <DefaultTextField
            label={"Custom Illustration URL"}
            error={false}
            defaultProps={{
              type: "text",
              id: "unPublishIllustration",
              placeholder: "Add your custom illustration (optional)",
              value: unPublishIllustration,
              autoFocus: false,
              inputProps: { maxLength: 250 },
              onChange: (event) => {
                handleChangeSettings("unPublishIllustration", event.target.value);
              },
            }}
            formControlStyles={{
              marginBottom: 0,
            }}
          />
        </Grid>
        <Grid item sm={12} md={6} classes={{
          item: clsx({
            [classes.sectionCnt]: true,
            [classes.disableContainer]: !unPublishCustomBranding,
          }),
        }}>
          <DefaultTextField
            label={"Redirect URL"}
            error={false}
            defaultProps={{
              type: "text",
              id: "unPublishRedirectUrl",
              placeholder: "Add any redirect url",
              value: unPublishRedirectUrl,
              autoFocus: false,
              inputProps: { maxLength: 250 },
              onChange: (event) => {
                handleChangeSettings("unPublishRedirectUrl", event.target.value);
              },
            }}
            formControlStyles={{
              marginBottom: 0,
            }}
          />
        </Grid>
        <Grid item sm={12} md={6} classes={{
          item: clsx({
            [classes.sectionCnt]: true,
            [classes.disableContainer]: !unPublishCustomBranding,
          }),
        }}>
          <DefaultTextField
            label={<span className={classes.label}>Redirect Button Text</span>}
            error={false}
            defaultProps={{
              type: "text",
              id: "unPublishRedirectBtnText",
              placeholder: "The text will display on redirect button",
              value: unPublishRedirectBtnText,
              autoFocus: false,
              inputProps: { maxLength: 250 },
              onChange: (event) => {
                handleChangeSettings("unPublishRedirectBtnText", event.target.value);
              },
            }}
            formControlStyles={{
              marginBottom: 0,
            }}
          />
        </Grid>
        <Grid item sm={12} md={6} classes={{
          item: clsx({
            [classes.sectionCnt]: true,
            [classes.disableContainer]: !unPublishCustomBranding,
          }),
        }}>
          <ColorPickerDropdown
            onSelect={(color) => {
              handleChangeSettings("unPublishRedirectBtnColor", color);
            }}
            obj={{}}
            singleSelect={true}
            handleCloseColor={handleCloseUnPublishColor}
            customColorBtnRender={
              <div onClick={handleSelectUnpublishColor} className={classes.textFieldCnt}>
                <DefaultTextField
                  label={<span className={classes.label}>Redirect Button Color</span>}
                  error={false}
                  defaultProps={{
                    id: "unPublishRedirectBtnColor",
                    placeholder: "Choose redirect button background color (optional)",
                    value: unPublishRedirectBtnColor,
                    readOnly: true,
                    disabled: false,
                  }}
                  formControlStyles={{
                    marginBottom: 0,
                  }}
                />
                {unPublishRedirectBtnColor && (
                  <div
                    className={classes.cancelCnt}
                    onClick={() => handleChangeSettings("unPublishRedirectBtnColor", "")}>
                    <CloseIcon className={classes.cancelIcon} />
                  </div>
                )}
              </div>
            }
            anchorEl={unPublishColorEl}
          />
        </Grid>
        <Grid item sm={12} md={6} classes={{
          item: clsx({
            [classes.sectionCnt]: true,
            [classes.disableContainer]: !unPublishCustomBranding,
          }),
        }}>
          <ColorPickerDropdown
            onSelect={(color) => {
              handleChangeSettings("unPublishFontColor", color);
            }}
            obj={{}}
            singleSelect={true}
            handleCloseColor={handleCloseUnPublishFontEl}
            customColorBtnRender={
              <div onClick={handleSelectUnpublishFontEl} className={classes.textFieldCnt}>
                <DefaultTextField
                  label={<span className={classes.label}>Page Font Color</span>}
                  error={false}
                  defaultProps={{
                    id: "unPublishFontColor",
                    placeholder: "Choose page font color (optional)",
                    value: unPublishFontColor,
                    readOnly: true,
                    disabled: false,
                  }}
                  formControlStyles={{
                    marginBottom: 0,
                  }}
                />
                {unPublishFontColor && (
                  <div
                    className={classes.cancelCnt}
                    onClick={() => handleChangeSettings("unPublishFontColor", "")}>
                    <CloseIcon className={classes.cancelIcon} />
                  </div>
                )}
              </div>
            }
            anchorEl={unPublishFontColorEl}
          />
        </Grid>
        <Grid item sm={12} md={12} classes={{
          item: clsx({
            [classes.sectionCnt]: true,
            [classes.disableContainer]: !unPublishCustomBranding,
          }),
        }}>
          <div className={classes.textFieldCnt}>
            <DefaultTextField
              label={<span className={classes.label}>Page Background Image/Color</span>}
              error={false}
              defaultProps={{
                id: "unPublishBgImg",
                placeholder: "Choose page background image/color from library or paste your custom image url here (optional)",
                value: unPublishBgImg || unPublishBgColor,
                readOnly: false,
                onChange: e => handleChangeSettings("unPublishBgImg", e.target.value, {
                  unPublishBgColor: null,
                }),
                startAdornment: (
                  <div onClick={handleUnPublishPageImg}>
                  <InputAdornment position="start">
                  <ImageIcon
                    htmlColor={theme.palette.secondary.light}
                    style={{ cursor: "pointer" }}
                  />
                </InputAdornment>
                  </div>
                ),
              }}
              formControlStyles={{
                marginBottom: 0,
              }}
            />
            {(unPublishBgImg || unPublishBgColor) && (
              <div
                className={classes.cancelCnt}
                onClick={() =>
                  handleChangeSettings("unPublishBgImg", "", {
                    unPublishBgColor: "",
                  })
                }>
                <CloseIcon className={classes.cancelIcon} />
              </div>
            )}
          </div>
          {unPublishPageImg && (
            <ColorImagesModal
              anchorTarget={unPublishPageImg}
              handleImageSelect={handleUnPublishImageSelect}
              handleColorSelect={handleUnPublishColorSelect}
              handleClickAway={handleUnPublishClickAway}
              intl={intl}
              useProjectStatusColor={false}
              viewMode="new"
              handleClickStatusSelect={() => { }}
            />
          )}
        </Grid>
        <Grid item sm={12} md={12} classes={{
          item: clsx({
            [classes.sectionCnt]: true,
            [classes.disableContainer]: !unPublishCustomBranding,
          }),
        }}>
          <DefaultTextField
            label={<span className={classes.label}>Description</span>}
            fullWidth={true}
            error={false}
            formControlStyles={{ marginTop: 15, marginBottom: 0 }}
            defaultProps={{
              onChange: (event) => {
                handleChangeSettings("unPublishDescription", event.target.value);
              },
              onBlur: (event) => {
                handleChangeSettings("unPublishDescription", event.target.value);
              },
              value: unPublishDescription,
              multiline: true,
              rows: 8,
              autoFocus: false,
              placeholder: "Add any description",
              inputProps: { maxLength: 2000 },
            }}
          />
        </Grid>
      </Grid>
    </>
  );
}
GeneralSettings.defaultProps = {
  classes: {},
  theme: {},
};
export default compose(injectIntl, withStyles(styles, { withTheme: true }))(GeneralSettings);
