import { withStyles } from "@material-ui/core/styles";
import React, { useContext } from "react";
import DefaultSwitch from "../../../../components/Form/Switch";
import NotificationMessages from "../../../../components/NotificationMessages/NotificationMessages";
import { updateForm } from "../../../../redux/actions/forms";
import { updateState } from "../../Context/actions";
import SurveyContext from "../../Context/form.context";
import styles from "../styles";

function GeneralSettings(params) {
  const { classes, theme } = params;
  const {
    state: { selectedForm },
    dispatch,
  } = useContext(SurveyContext);
  const { formSettings } = selectedForm;

  const { requiredLogin, multipleSubmission, showCaptcha, formVersion } = formSettings;

  const handleChangeSettings = (key, value) => {
    const updatedSurvey = {
      ...selectedForm,
      formSettings: { ...formSettings, [key]: value },
    };
    updateState(dispatch, { selectedForm: updatedSurvey });
    updateForm(
      updatedSurvey,
      (succ) => {},
      (fail) => {
        console.log("================", fail);
      }
    );
  };

  return (
    <>
      <h3 className={classes.heading}>General Settings</h3>
      {/* <div className={classes.switchWrapper}>
        <DefaultSwitch
          checked={requiredLogin}
          onChange={(event) => handleChangeSettings("requiredLogin", !requiredLogin)}
          value={requiredLogin}
        />
        <span className={classes.settingLabel}> Required login for submission</span>
      </div> */}
      <div className={classes.switchWrapper}>
        <DefaultSwitch
          checked={multipleSubmission}
          onChange={(event) => handleChangeSettings("multipleSubmission", !multipleSubmission)}
          value={multipleSubmission}
        />
        <span className={classes.settingLabel}> Allow multiple submissions</span>
      </div>
      <div className={classes.switchWrapper}>
        <DefaultSwitch
          checked={showCaptcha}
          onChange={(event) => handleChangeSettings("showCaptcha", !showCaptcha)}
          value={showCaptcha}
        />
        <span className={classes.settingLabel}> Show captcha</span>
      </div>
      <div className={classes.switchWrapper}>
        <DefaultSwitch
          checked={formVersion}
          onChange={(event) => handleChangeSettings("formVersion", !formVersion)}
          value={formVersion}
        />
        <span className={classes.settingLabel}> Form versioning</span>
        <NotificationMessages
          type="NewInfo"
          iconType="info"
          style={{
            width: "100%",
            textAlign: "start",
            fontSize: "12px",
            padding: "6px",
            backgroundColor: "#EAEAEA",
            borderRadius: 4,
            fontFamily: theme.typography.fontFamilyLato,
            fontWeight: 400,
            marginTop: 20,
            marginLeft: 12,
            maxWidth: 604,
          }}>
          Note: If you turn on this toggle, a new form version will be created on every design customization.
        </NotificationMessages>
      </div>
    </>
  );
}
GeneralSettings.defaultProps = {
  classes: {},
  theme: {},
};
export default withStyles(styles, { withTheme: true })(GeneralSettings);
