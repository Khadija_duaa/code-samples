import { Grid } from "@material-ui/core";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { withStyles } from "@material-ui/core/styles";
import ScheduleIcon from "@material-ui/icons/Schedule";
import PrivacyIcon from "@material-ui/icons/Security";
import SettingsIcon from "@material-ui/icons/Settings";
import React, { useContext, useState } from "react";
import SurveyContext from "../../Context/form.context";
import styles from "../styles";
import GeneralSettingsView from "./generalSettings.view";
import PrivacySettingsView from "./privacySettings.view";
import ScheduleSettings from "./scheduleSettings.view";

function Settings(params) {
  const { classes, theme } = params;
  const {
    state: { selectedForm },
    dispatch,
  } = useContext(SurveyContext);

  const [selectedIndex, setSelectedIndex] = useState(1);
  const handleListItemClick = (event, index) => {
    setSelectedIndex(index);
  };
  return (
    <>
      <Grid container direction="row" justify="flex-start" alignItems="stretch">
        <Grid item xs={2} classes={{ item: classes.settingListCnt }}>
          <List component="nav" classes={{ root: classes.settingSideList }}>
            <ListItem
              button
              selected={selectedIndex === 0}
              onClick={(event) => handleListItemClick(event, 0)}
              classes={{
                root: classes.listItem,
                selected: classes.listItemSelected,
              }}>
              <SettingsIcon className={classes.icon} />{" "}
              <span className={classes.settingLabel}>General Settings</span>
            </ListItem>
            <ListItem
              button
              selected={selectedIndex === 1}
              onClick={(event) => handleListItemClick(event, 1)}
              classes={{
                root: classes.listItem,
                selected: classes.listItemSelected,
              }}>
              <ScheduleIcon className={classes.icon} />{" "}
              <span className={classes.settingLabel}> Schedule Settings</span>
            </ListItem>
            <ListItem
              button
              selected={selectedIndex === 2}
              onClick={(event) => handleListItemClick(event, 2)}
              classes={{
                root: classes.listItem,
                selected: classes.listItemSelected,
              }}>
              <PrivacyIcon className={classes.icon} />{" "}
              <span className={classes.settingLabel}> Privacy Settings</span>
            </ListItem>
          </List>
        </Grid>
        <Grid item xs={10} classes={{ item: classes.settingContentCnt }}>
          {selectedIndex == 0 && <GeneralSettingsView />}
          {selectedIndex == 1 && <ScheduleSettings />}
          {selectedIndex == 2 && <PrivacySettingsView />}
        </Grid>
      </Grid>
    </>
  );
}
Settings.defaultProps = {
  classes: {},
  theme: {},
};
export default withStyles(styles, { withTheme: true })(Settings);
