const creatorStyles = (theme) => ({
  settingListCnt: {
    background: "#fff",
    borderRight: `1px solid ${theme.palette.border.lightBorder}`,
  },
  settingSideList: {
    padding: 0,
    "& $listItemSelected": {
      background: "#e8fbfe !important",
      // border: `1px solid ${theme.palette.border.lightBorder}`,
      borderLeft: "none",
      borderRight: "none",
      "& $profileSideListText": {
        color: theme.palette.text.primary,
      },
      "& $icon": {
        color: "#2f2f2f",
      },
    },
  },
  listItem: {
    padding: "1.3em 2em 1.3em 1.6em",
    "&:hover": {
      backgroundColor: "#f8f9fa",
    },
  },
  listItemSelected: {},
  settingContentCnt: {
    background: "#fff",
    paddingLeft: 25,
    paddingRight: 15,
    height: "100%",
    overflow: "auto",
    paddingBottom: 25,
  },
  settingLabel: {
    color: "#2f2f2f",
    fontSize: "1em !important",
    fontWeight: theme.typography.fontWeightExtraLight,
    fontFamily: theme.typography.fontFamilyLato,
    marginLeft: 5,
  },
  icon: {
    fontSize: "18px !important",
    color: "#999999",
  },
  heading: {
    fontWeight: theme.typography.fontWeightMedium,
    fontFamily: theme.typography.fontFamilyLato,
    borderBottom: "1px solid #cecece",
    paddingBottom: "1.2em",
    paddingTop: "0.2em",
    marginTop: "0.8em",
    marginBottom: "1em",
  },
  switchWrapper: {
    borderBottom: "1px solid #dedede",
    paddingBottom: "1.2em",
    paddingTop: "1.2em",
  },
  switchScheduleWrapper: {
    // borderBottom: "1px solid #dedede",
    // paddingBottom: "1.2em",
    paddingTop: "1.2em",
  },
  settingContainer: {
    maxWidth: 850,
  },
  privacySettings: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    paddingTop: "2em",
    paddingBottom: "2em",
    border: "1px solid #dedede",
    borderRadius: "8px",
    marginBottom: "3em",
  },
  publicLinkInputCnt: {
    display: "flex",
    marginBottom: 20,
    marginTop: 20,
    maxWidth: 700,
    width: "100%",
  },
  label: {
    fontWeight: theme.typography.fontWeightRegular,
    fontFamily: theme.typography.fontFamilyLato,
    fontSize: "13px !important",
  },
  copyIcon: {
    fontSize: "16px !important",
    color: "#999",
    cursor: "pointer",
    marginRight: 5,
  },
  stateWrapper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    maxWidth: 700,
    width: "100%",
  },
  stateItem: {
    border: "1px solid #d2d2d2",
    marginTop: "0.8em",
    borderRadius: "8px",
    minHeight: "3.5em",
    cursor: "pointer",
    display: "inline-flex",
    width: "100%",
    flexDirection: "column",
    justifyContent: "space-evenly",
    alignItems: "center",
    fontSize: "1.4em !important",
    padding: "0.7em 1em",
    background: "#fff",
  },
  stateItemActive: {
    border: "2px solid #40d07a !important",
    color: "#40d07a",
    backgroundColor: "rgba(64,208,122,.05)",
    "& $privacyIcons": {
      color: "#40d07a",
    },
  },
  shareItemTitle: {
    marginLeft: "10px",
    display: "flex",
    flexDirection: "column",
    width: "100%",
    lineHeight: "normal",
  },
  wrapper: {
    width: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    fontSize: "13px !important",
    fontWeight: theme.typography.fontWeightExtraLight,
    fontFamily: theme.typography.fontFamilyLato,
  },
  title: {
    fontSize: "15px !important",
    fontWeight: theme.typography.fontWeightMedium,
    fontFamily: theme.typography.fontFamilyLato,
  },
  privacyIcons: {
    color: "#999999",
  },
  userWrapper: {
    backgroundColor: "white",
    padding: "1em",
    fontSize: "1rem !important",
    border: "1px solid #40d07a",
    position: "relative",
    marginTop: "1em",
    borderRadius: "8px",
    transition: "max-height .3s linear",
    width: "100%",
    "&:after": {
      content: "''",
      position: "absolute",
      left: "10%",
      height: "1em",
      width: "1em",
      top: "0",
      transform: "rotate(-45deg) translateX(-50%) translateY(-1.3em)",
      backgroundColor: "inherit",
      border: "1px solid #40d07a",
      borderBottom: "none",
      borderLeft: "none",
      borderTopRightRadius: "6px",
    },
  },
  locationActions: {
    maxWidth: 600,
    paddingLeft: 12,
    display: "flex",
    alignItems: "end",
  },
  datePickerCustomStyle: {
    width: "100% !important",
    border: "1px solid #dddddd !important",
    padding: "9px 0px 10px 8px !important",
    borderRadius: "4px !important",
    marginTop: "8px",
    "&:hover": {
      background: `${theme.palette.background.items} !important`,
    },
  },
  datePickerContainer: {
    // flex: 1,
    // position: "absolute",
    // top: 0,
    // width: "100%",
    // display: "block",
    // left: 0,
    "& svg": {
      display: "none",
      "& path": {
        fill: "#999999 !important",
      },
      fontSize: "22px !important",
      marginBottom: -8,
    },
  },
  sectionCnt: {
    padding: "30px 0px 0px 15px",
    maxWidth: 850,
  },
  subSectionCnt: {
    padding: "0px 0px 0px 15px",
    maxWidth: 850,
  },
  disableContainer: {
    pointerEvents: "none",
    opacity: "0.5",
  },
  cancelIcon: {
    fontSize: "20px !important",
    color: "#999999",
    cursor: "pointer",
  },
  cancelIcon2: {
    fontSize: "20px !important",
    color: "#999999",
    cursor: "pointer",
    marginBottom: 10,
    marginLeft: 5,
  },
  cancelCnt: {
    position: "absolute",
    bottom: "6px",
    right: "10px",
    visibility: "hidden",
    background: "white",
  },
  textFieldCnt: {
    position: "relative",
    "&:hover": {
      "& $cancelCnt": {
        visibility: "visible",
      },
    },
  },
  locationInfoCnt: {
    margin: "15px 0 15px 0",
  },
  addLocTxt: {
    paddingLeft: "15px",
    color: "#00cc90",
    fontSize: "13px !important",
    textDecoration: "underline",
    cursor: "pointer",
    fontWeight: theme.typography.fontWeightMedium,
    fontFamily: theme.typography.fontFamilyLato,
  },
  versionTxt: {
    textTransform: "lowercase",
    color: theme.palette.background.blue,
  },
  dialogFormCnt: {
    padding: 20,
    paddingBottom: 0
  },
  dialogFormActionsCnt: {
    padding: 20
  },
  editIconCnt: {
    display: "none",
  },
  editIcon: {
    fontSize: 11,
    marginTop: -4,
    marginLeft: 5,
  },
});

export default creatorStyles;
