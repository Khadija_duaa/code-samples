import { withStyles } from "@material-ui/core/styles";
import SvgIcon from "@material-ui/core/SvgIcon";
import Tooltip from "@material-ui/core/Tooltip";
import AnalyticsIcon from "@material-ui/icons/BarChart";
import PreviewIcon from "@material-ui/icons/Visibility";
import { withRouter } from "react-router-dom";
import React, { useContext, useState } from "react";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import { compose } from "redux";
import * as Survey from "survey-core";
import * as SurveyPDF from "survey-pdf";
import CustomIconButton from "../../components/Buttons/IconButton";
import DeleteConfirmDialog from "../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import DeleteActionIcon from "../../components/Icons/DeleteActionIcon";
import FileDownloadIcon from "../../components/Icons/DownloadIcon";
import EditReplyActionIcon from "../../components/Icons/EditReplyActionIcon";
import { deleteForm } from "../../redux/actions/forms";
import { updateState } from "./Context/actions";
import SurveyContext from "./Context/form.context";
import styles from "./forms.style";
function FormActions(props) {
  const {
    classes,
    theme,
    data,
    history,
    permission: {
      editPermission,
      previewPermission,
      deletePermission,
      analyticsPermission,
      exportPermission,
    },
  } = props;
  const { state, dispatch } = useContext(SurveyContext);
  const [deleteDialog, setDeleteDialog] = useState(false);

  const handleEditForm = () => {
    updateState(dispatch, { selectedForm: data, view: "new-survey" });
  };
  // const handleEditForm = () => {
  //   // if (view !== "assignedToMe") {
  //   updateState(dispatch, { selectedForm: data });
  //   history.push(`/form?id=${data.formId}`);
  //   // if open in new tab than use below action
  //   // window.open(`/form?id=${item.formId}`, "_blank");
  //   // }
  // };
  const handlePreviewAnalytics = () => {
    updateState(dispatch, { selectedForm: data, view: "survey-analytics" });
  };
  const handlePreviewForm = () => {
    window.open(`/form/fill?id=${data.formId}`, "_blank");
  };

  const handleDeleteForm = () => {
    setDeleteDialog(true);
  };
  const handleDeleteSucc = () => {
    setDeleteDialog(false);
    deleteForm(
      data.formId,
      (succ) => {
        updateState(dispatch, {
          allForms: state.allForms.filter((item) => item.formId !== data.formId),
        });
      },
      (fail) => {
        console.log("================", fail);
      }
    );
  };

  const savePDF = () => {
    let options = {
      fontSize: "14px",
      margins: {
        left: 10,
        right: 10,
        top: 18,
        bot: 10,
      },
    };

    let model = new Survey.Model(data);
    let surveyPDF = new SurveyPDF.SurveyPDF(data, options);
    surveyPDF.data = model.data;
    surveyPDF.save();
  };

  return (
    <>
      <div style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
        {analyticsPermission && (
          <CustomIconButton
            btnType="filledWhite"
            onClick={(e) => {
              e.stopPropagation();
              handlePreviewAnalytics();
            }}
            style={{
              borderRadius: "50%",
              border: "none",
              marginLeft: 6,
              boxShadow: "0px 3px 6px #0000001A",
              padding: 6,
              backgroundColor: "#fff",
            }}>
            <Tooltip
              classes={{
                tooltip: classes.tooltip,
              }}
              title={"Analytics"}
              placement="bottom">
              <AnalyticsIcon
                className={classes.actionIcon}
                style={{
                  fontSize: "20px",
                  color: theme.palette.secondary.medDark,
                }}
              />
            </Tooltip>
          </CustomIconButton>
        )}
        {previewPermission && (
          <CustomIconButton
            btnType="filledWhite"
            onClick={(e) => {
              e.stopPropagation();
              handlePreviewForm();
            }}
            style={{
              borderRadius: "50%",
              border: "none",
              marginLeft: 6,
              boxShadow: "0px 3px 6px #0000001A",
              padding: 6,
              backgroundColor: "#fff",
            }}>
            <Tooltip
              classes={{
                tooltip: classes.tooltip,
              }}
              title={"Preview"}
              placement="bottom">
              <PreviewIcon
                className={classes.actionIcon}
                style={{
                  fontSize: "20px",
                  color: theme.palette.secondary.medDark,
                }}
              />
            </Tooltip>
          </CustomIconButton>
        )}
        {editPermission && (
          <CustomIconButton
            btnType="filledWhite"
            onClick={(e) => {
              e.stopPropagation();
              handleEditForm();
            }}
            style={{
              borderRadius: "50%",
              border: "none",
              marginLeft: 6,
              boxShadow: "0px 3px 6px #0000001A",
              padding: 8,
              backgroundColor: "#fff",
            }}>
            <Tooltip
              classes={{
                tooltip: classes.tooltip,
              }}
              title={
                <FormattedMessage
                  id="board.all-boards.board-actions-tooltips.edit"
                  defaultMessage="Edit"
                />
              }
              placement="bottom">
              <SvgIcon
                viewBox="0 0 16 16.07"
                htmlColor={theme.palette.secondary.medDark}
                style={{ fontSize: "16px" }}
                className={classes.actionIcon}>
                <EditReplyActionIcon />
              </SvgIcon>
            </Tooltip>
          </CustomIconButton>
        )}
        {exportPermission && (
          <CustomIconButton
            btnType="filledWhite"
            onClick={(e) => {
              e.stopPropagation();
              savePDF();
            }}
            style={{
              borderRadius: "50%",
              border: "none",
              marginLeft: 6,
              boxShadow: "0px 3px 6px #0000001A",
              padding: 8,
              backgroundColor: "#fff",
            }}>
            <Tooltip
              classes={{
                tooltip: classes.tooltip,
              }}
              title={"Download pdf"}
              placement="bottom">
              <SvgIcon
                viewBox="0 0 16 16.07"
                htmlColor={theme.palette.secondary.medDark}
                style={{ fontSize: "16px" }}
                className={classes.actionIcon}>
                <FileDownloadIcon />
              </SvgIcon>
            </Tooltip>
          </CustomIconButton>
        )}
        {deletePermission && (
          <CustomIconButton
            btnType="filledWhite"
            style={{
              borderRadius: "50%",
              border: "none",
              marginLeft: 6,
              boxShadow: "0px 3px 6px #0000001A",
              padding: 8,
              backgroundColor: "#fff",
            }}
            onClick={(e) => {
              e.stopPropagation();
              handleDeleteForm();
            }}>
            <Tooltip
              classes={{
                tooltip: classes.tooltip,
              }}
              title={
                <FormattedMessage
                  id="board.all-boards.board-actions-tooltips.delete"
                  defaultMessage="Delete"
                />
              }
              placement="bottom">
              <SvgIcon
                viewBox="0 0 16 16"
                htmlColor={theme.palette.secondary.medDark}
                style={{ fontSize: "16px" }}
                className={classes.actionIcon}>
                <DeleteActionIcon />
              </SvgIcon>
            </Tooltip>
          </CustomIconButton>
        )}
      </div>
      {deleteDialog && (
        <DeleteConfirmDialog
          open={deleteDialog}
          closeAction={() => setDeleteDialog(false)}
          cancelBtnText={
            <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
          }
          successBtnText={
            <FormattedMessage
              id="common.action.delete.confirmation.delete-button.label"
              defaultMessage="Delete"
            />
          }
          alignment="center"
          headingText={
            <FormattedMessage
              id="common.action.delete.confirmation.delete-button.label"
              defaultMessage="Delete"
            />
          }
          successAction={handleDeleteSucc}
          msgText={`Deleting a form will delete all its previous form versions.`}
          btnQuery={""}
        />
      )}
    </>
  );
}

FormActions.defaultProps = {
  classes: {},
  theme: {},
  permission: {
    editPermission: false,
    previewPermission: false,
    deletePermission: false,
    analyticsPermission: false,
    exportPermission: false,
  },
};
const mapStateToProps = (state) => {
  return {};
};

export default compose(
  withRouter,
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, {})
)(FormActions);
