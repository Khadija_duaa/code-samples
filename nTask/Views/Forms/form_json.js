import { v4 as uuidv4 } from "uuid";

export let defaultSurveyJSON = {
  completedHtml:
    "<h3>Thank you for your feedback.</h3> <h5>Your thoughts and ideas will help us to create a great product!</h5>",
  completedHtmlOnCondition: [
    {
      expression: "{nps_score} > 8",
      html: "<h3>Thank you for your feedback.</h3> <h5>We glad that you love our product. Your ideas and suggestions will help us to make our product even better!</h5>",
    },
    {
      expression: "{nps_score} < 7",
      html: "<h3>Thank you for your feedback.</h3> <h5> We are glad that you share with us your ideas.We highly value all suggestions from our customers. We do our best to improve the product and reach your expectation.</h5><br/>",
    },
  ],
  pages: [
    {
      name: "page1",
      elements: [
        {
          type: "rating",
          name: "nps_score",
          title:
            "On a scale of zero to ten, how likely are you to recommend our product to a friend or colleague?",
          isRequired: true,
          rateMin: 0,
          rateMax: 10,
          minRateDescription: "(Most unlikely)",
          maxRateDescription: "(Most likely)",
        },
        {
          type: "checkbox",
          name: "promoter_features",
          visibleIf: "{nps_score} >= 9",
          title: "What features do you value the most?",
          isRequired: true,
          validators: [
            {
              type: "answercount",
              text: "Please select two features maximum.",
              maxCount: 2,
            },
          ],
          hasOther: true,
          choices: ["Performance", "Stability", "User Interface", "Complete Functionality"],
          otherText: "Other feature:",
          colCount: 2,
        },
        {
          type: "comment",
          name: "passive_experience",
          visibleIf: "{nps_score} > 6  and {nps_score} < 9",
          title: "What is the primary reason for your score?",
        },
        {
          type: "comment",
          name: "disappointed_experience",
          visibleIf: "{nps_score} notempty",
          title: "What do you miss and what was disappointing in your experience with us?",
        },
      ],
    },
  ],
  showQuestionNumbers: "off",
};

export let surveyList = [
  {
    id: uuidv4(),
    title: "NPS Survey",
    description: "NPS survey with follow-up questions",
    logoPosition: "right",
    completedHtml:
      "<h3>Thank you for your feedback.</h3><h5>Your thoughts and ideas will help us to create a great product!</h5>",
    completedHtmlOnCondition: [
      {
        expression: "{nps_score} > 8",
        html: "<h3>Thank you for your feedback.</h3><h5>We glad that you love our product. Your ideas and suggestions will help us to make our product even better!</h5>",
      },
      {
        expression: "{nps_score} < 7",
        html: "<h3>Thank you for your feedback.</h3><h5> We are glad that you share with us your ideas.We highly value all suggestions from our customers. We do our best to improve the product and reach your expectation.</h5><br />",
      },
    ],
    pages: [
      {
        name: "page1",
        elements: [
          {
            type: "rating",
            name: "nps_score",
            title:
              "On a scale of zero to ten, how likely are you to recommend our product to a friend or colleague?",
            isRequired: true,
            rateMin: 0,
            rateMax: 10,
            minRateDescription: "(Most unlikely)",
            maxRateDescription: "(Most likely)",
          },
          {
            type: "checkbox",
            name: "promoter_features",
            visible: false,
            visibleIf: "{nps_score} >= 9",
            title: "Which features do you value the most?",
            isRequired: true,
            validators: [
              {
                type: "answercount",
                text: "Please select two features maximum.",
                maxCount: 2,
              },
            ],
            choices: ["Performance", "Stability", "User Interface", "Complete Functionality"],
            hasOther: true,
            otherText: "Other feature:",
            colCount: 2,
          },
          {
            type: "comment",
            name: "passive_experience",
            visible: false,
            visibleIf: "{nps_score} > 6  and {nps_score} < 9",
            title: "What do you like about our product?",
          },
          {
            type: "comment",
            name: "disappointed_experience",
            visible: false,
            visibleIf: "{nps_score} notempty",
            title: "What do you miss or find disappointing in your experience with our products?",
          },
        ],
      },
    ],
    showQuestionNumbers: "off",
    formSettings: {
      requiredLogin: true,
      multipleSubmission: true,
      showCaptcha: false,
      privacySettings: "public",
      locationSettings: true,
      limitedUser: [],
      country: null,
      city: null,
      publishSettings: true,
      unPublishSettings: false,
      publishDate: null,
      publishTime: null,
      publishRedirectUrl: "",
      publishRedirectBtnText: "Lorem Ipsum",
      publishDescription: "",
      unPublishDate: null,
      unPublishTime: null,
      unPublishRedirectUrl: "",
      unPublishRedirectBtnText: "",
      unPublishDescription: "",
    },
  },
  {
    id: uuidv4(),
    title: "COVID-19",
    description: "Minimum data reporting form – for suspected and probable cases of COVID-19",
    logoPosition: "right",
    pages: [
      {
        name: "page1",
        elements: [
          {
            type: "image",
            name: "first_page_image",
            width: "400px",
            minWidth: "400px",
            maxWidth: "400px",
            imageLink: "https://surveyjs.io/Content/Images/examples/covid/01.png",
            imageFit: "none",
            imageHeight: 726,
            imageWidth: 400,
          },
          {
            type: "panel",
            name: "first_page_container_panel",
            elements: [
              {
                type: "text",
                name: "unique_case_id_textbox",
                startWithNewLine: false,
                title: "Unique Case ID / Cluster Number (if applicable):",
                hideNumber: true,
                inputType: "number",
              },
              {
                type: "panel",
                name: "current_status_panel",
                elements: [
                  {
                    type: "boolean",
                    name: "current_status",
                    titleLocation: "hidden",
                    hideNumber: true,
                    labelTrue: "Alive",
                    labelFalse: "Dead",
                  },
                ],
                title: "Current Status",
                showNumber: true,
              },
              {
                type: "panel",
                name: "data_collector_information",
                elements: [
                  {
                    type: "text",
                    name: "name_of_data_collector",
                    title: "Name of data collector",
                  },
                  {
                    type: "text",
                    name: "data_collector_institution",
                    title: "Data collector Institution",
                  },
                  {
                    type: "text",
                    name: "data_collector_telephone_number",
                    title: "Data collector telephone number",
                    inputType: "tel",
                  },
                  {
                    type: "text",
                    name: "email",
                    title: "Email",
                    inputType: "email",
                  },
                  {
                    type: "text",
                    name: "form_completion_date",
                    title: "Form completion date",
                    inputType: "date",
                  },
                ],
                title: "Data Collector Information",
                showNumber: true,
                showQuestionNumbers: "off",
              },
            ],
            startWithNewLine: false,
          },
        ],
        navigationTitle: "Collector",
        navigationDescription: "Collector's info",
      },
      {
        name: "page2",
        elements: [
          {
            type: "boolean",
            name: "is_the_person_providing_the_information_is_the_patient",
            title: "Is the person providing the information is the patient?",
            hideNumber: true,
            isRequired: true,
            labelTrue: "Yes",
            labelFalse: "No",
          },
          {
            type: "panel",
            name: "case_identifier_information",
            elements: [
              {
                type: "text",
                name: "given_names",
                title: "Given name(s)",
              },
              {
                type: "text",
                name: "family_name",
                startWithNewLine: false,
                title: "Family name",
              },
              {
                type: "radiogroup",
                name: "case_identifier_information_sex",
                title: "Sex",
                choices: [
                  {
                    value: "item1",
                    text: "Male",
                  },
                  {
                    value: "item2",
                    text: "Female",
                  },
                  {
                    value: "item3",
                    text: "Not known",
                  },
                ],
                colCount: 3,
              },
              {
                type: "panel",
                name: "case_identifier_information_patient_date_of_birth_panel",
                elements: [
                  {
                    type: "text",
                    name: "case_identifier_information_patient_date_of_birth_date",
                    visibleIf: "{case_identifier_information_patient_date_of_birth_checkbox} empty",
                    titleLocation: "hidden",
                    inputType: "date",
                  },
                  {
                    type: "checkbox",
                    name: "case_identifier_information_patient_date_of_birth_checkbox",
                    startWithNewLine: false,
                    titleLocation: "hidden",
                    choices: ["Unknown"],
                  },
                ],
                title: "Date of Birth",
              },
              {
                type: "text",
                name: "case_identifier_information_patient_telephone_mobile_number",
                title: "Telephone (mobile) number",
                inputType: "tel",
              },
              {
                type: "panel",
                name: "case_identifier_information_patient_age",
                elements: [
                  {
                    type: "text",
                    name: "case_identifier_information_patient_age_years",
                    visibleIf: "{case_identifier_information_patient_age_checkbox} empty",
                    title: "Years:",
                    inputType: "number",
                  },
                  {
                    type: "text",
                    name: "case_identifier_information_patient_age_months",
                    visibleIf: "{case_identifier_information_patient_age_checkbox} empty",
                    startWithNewLine: false,
                    title: "Months:",
                    inputType: "number",
                  },
                  {
                    type: "checkbox",
                    name: "case_identifier_information_patient_age_checkbox",
                    startWithNewLine: false,
                    titleLocation: "hidden",
                    choices: ["Unknown"],
                  },
                ],
                title: "Age (years, months) ",
              },
              {
                type: "text",
                name: "case_identifier_information_patient_email",
                title: "Email",
                inputType: "email",
              },
              {
                type: "text",
                name: "case_identifier_information_patient_address",
                startWithNewLine: false,
                title: "Address",
              },
              {
                type: "text",
                name: "case_identifier_information_patient_national_social_number",
                title: "National social number/ identifier (if applicable)",
              },
              {
                type: "dropdown",
                name: "case_identifier_information_patient_country_of_residence",
                title: "Country of residence",
                choices: ["item1", "item2", "item3"],
                choicesByUrl: {
                  url: "https://surveyjs.io/api/CountriesExample",
                  valueName: "name",
                },
              },
              {
                type: "radiogroup",
                name: "case_identifier_information_patient_case_status",
                startWithNewLine: false,
                title: "Case status",
                choices: [
                  {
                    value: "item1",
                    text: "Suspected",
                  },
                  {
                    value: "item2",
                    text: "Probable",
                  },
                  {
                    value: "item3",
                    text: "Confirmed",
                  },
                ],
                colCount: 3,
              },
            ],
            visible: false,
            visibleIf: "{is_the_person_providing_the_information_is_the_patient} = true",
            title: "Case Identifier Information",
            showNumber: true,
            showQuestionNumbers: "off",
          },
          {
            type: "panel",
            name: "interview_respondent_information_if_the_persons_providing_the_information_is_not_the_patient",
            elements: [
              {
                type: "text",
                name: "first_name",
                title: "First name ",
              },
              {
                type: "text",
                name: "surname",
                startWithNewLine: false,
                title: "Surname",
              },
              {
                type: "radiogroup",
                name: "interview_respondent_information_sex",
                title: "Sex",
                choices: [
                  {
                    value: "item1",
                    text: "Male",
                  },
                  {
                    value: "item2",
                    text: "Female",
                  },
                  {
                    value: "item3",
                    text: "Not known",
                  },
                ],
                colCount: 3,
              },
              {
                type: "panel",
                name: "interview_respondent_information_patient_date_of_birth_panel",
                elements: [
                  {
                    type: "text",
                    name: "interview_respondent_information_patient_date_of_birth_date",
                    visibleIf:
                      "{interview_respondent_information_patient_date_of_birth_checkbox} empty",
                    titleLocation: "hidden",
                    inputType: "date",
                  },
                  {
                    type: "checkbox",
                    name: "interview_respondent_information_patient_date_of_birth_checkbox",
                    startWithNewLine: false,
                    titleLocation: "hidden",
                    choices: ["Unknown"],
                  },
                ],
                title: "Date of Birth",
              },
              {
                type: "text",
                name: "relationship_to_patient",
                title: "Relationship to patient",
              },
              {
                type: "text",
                name: "respondent_address",
                startWithNewLine: false,
                title: "Respondent address",
              },
              {
                type: "text",
                name: "interview_respondent_information_patient_telephone_mobile_number",
                startWithNewLine: false,
                title: "Telephone (mobile) number",
                inputType: "tel",
              },
            ],
            visible: false,
            visibleIf: "{is_the_person_providing_the_information_is_the_patient} = false",
            title: "Interview respondent information",
            showNumber: true,
            showQuestionNumbers: "off",
          },
        ],
        navigationTitle: "Person",
        navigationDescription: "Person's info",
      },
      {
        name: "page3",
        elements: [
          {
            type: "image",
            name: "third_page_image",
            width: "400px",
            minWidth: "400px",
            maxWidth: "400px",
            imageLink: "https://surveyjs.io/Content/Images/examples/covid/03.png",
            imageHeight: 690,
            imageWidth: 400,
          },
          {
            type: "panel",
            name: "patient_symptoms_from_disease_onset",
            elements: [
              {
                type: "panel",
                name: "date_of_first_symptom_onset",
                elements: [
                  {
                    type: "text",
                    name: "date_of_first_symptom_onset_date",
                    width: "314px",
                    minWidth: "314px",
                    maxWidth: "314px",
                    titleLocation: "hidden",
                    enableIf: "{date_of_first_symptom_onset_checkbox} empty",
                    inputType: "date",
                  },
                  {
                    type: "checkbox",
                    name: "date_of_first_symptom_onset_checkbox",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "No symptoms",
                        enableIf: "{date_of_first_symptom_onset_checkbox} <> ['item2']",
                      },
                      {
                        value: "item2",
                        text: "Unknown",
                        enableIf: "{date_of_first_symptom_onset_checkbox} <> ['item1']",
                      },
                    ],
                    colCount: 2,
                  },
                  {
                    type: "radiogroup",
                    name: "question1",
                    title: "Fever (≥38 °C) or history of fever ",
                    titleLocation: "top",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Unknown",
                      },
                    ],
                    colCount: 3,
                  },
                ],
                title: "Date of first symptom onset",
              },
              {
                type: "radiogroup",
                name: "sore_throat",
                title: "Sore throat",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
              {
                type: "radiogroup",
                name: "runny_nose",
                title: "Runny nose",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
              {
                type: "radiogroup",
                name: "cough",
                title: "Cough",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
              {
                type: "radiogroup",
                name: "shortness_of_Breath",
                title: "Shortness of Breath",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
              {
                type: "radiogroup",
                name: "vomiting",
                title: "Vomiting",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
              {
                type: "radiogroup",
                name: "nausea",
                title: "Nausea",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
              {
                type: "radiogroup",
                name: "diarrhea",
                title: "Diarrhea",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
            ],
            title: "Patient symptoms (from disease onset)",
            startWithNewLine: false,
            showNumber: true,
            showQuestionNumbers: "off",
          },
        ],
        navigationTitle: "Symptoms",
        navigationDescription: "Patient symptoms",
      },
      {
        name: "page4",
        elements: [
          {
            type: "image",
            name: "fouth_page_image",
            width: "400px",
            minWidth: "400px",
            maxWidth: "400px",
            imageLink: "https://surveyjs.io/Content/Images/examples/covid/04.png",
            imageHeight: 567,
            imageWidth: 400,
          },
          {
            type: "panel",
            name: "initial_sample_collection",
            elements: [
              {
                type: "text",
                name: "date_respiratory_sample_collected",
                title: "Date respiratory sample collected",
                inputType: "date",
              },
              {
                type: "radiogroup",
                name: "what_type_of_respiratory_sample_was_collected",
                title: "What type of respiratory sample was collected?",
                choices: [
                  {
                    value: "item1",
                    text: "Nasal swab",
                  },
                  {
                    value: "item2",
                    text: "Throat swab ",
                  },
                  {
                    value: "item3",
                    text: "Nasopharyngeal swab",
                  },
                ],
                hasOther: true,
                otherText: "Other, specify",
                colCount: 2,
              },
              {
                type: "panel",
                name: "has_baseline_serum_been_taken_panel",
                elements: [
                  {
                    type: "radiogroup",
                    name: "has_baseline_serum_been_taken_radio",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Unknown",
                      },
                    ],
                    colCount: 3,
                  },
                  {
                    type: "text",
                    name: "has_baseline_serum_been_taken_date",
                    visible: false,
                    visibleIf: "{has_baseline_serum_been_taken_radio} = 'item1'",
                    startWithNewLine: false,
                    title: "Date baseline serum taken",
                    inputType: "date",
                  },
                ],
                title: "Has baseline serum been taken?",
              },
              {
                type: "panel",
                name: "were_other_samples_collected_panel",
                elements: [
                  {
                    type: "radiogroup",
                    name: "were_other_samples_collected_radio",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Unknown",
                      },
                    ],
                    colCount: 3,
                  },
                  {
                    type: "text",
                    name: "were_other_samples_collected_textbox",
                    visible: false,
                    visibleIf: "{were_other_samples_collected_radio} = 'item1'",
                    startWithNewLine: false,
                    title: "Which samples:",
                  },
                  {
                    type: "text",
                    name: "were_other_samples_collected_date",
                    visible: false,
                    visibleIf: "{were_other_samples_collected_radio} = 'item1'",
                    startWithNewLine: false,
                    title: "Date taken",
                    inputType: "date",
                  },
                ],
                title: "Were other samples collected? ",
              },
            ],
            title: "Initial sample collection",
            startWithNewLine: false,
            showNumber: true,
            showQuestionNumbers: "off",
          },
        ],
        navigationTitle: "Sample",
        navigationDescription: "Initial sample",
      },
      {
        name: "page5",
        elements: [
          {
            type: "image",
            name: "fifth_image",
            width: "400px",
            minWidth: "400px",
            maxWidth: "400px",
            imageLink: "https://surveyjs.io/Content/Images/examples/covid/05.png",
            imageHeight: 713,
            imageWidth: 400,
          },
          {
            type: "panel",
            name: "clinical_course_complications_panel",
            elements: [
              {
                type: "panel",
                name: "hospitalization_required_panel",
                elements: [
                  {
                    type: "radiogroup",
                    name: "hospitalization_required_radio",
                    width: "49%",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Unknown",
                      },
                    ],
                    colCount: 3,
                  },
                  {
                    type: "text",
                    name: "hospitalization_required_hospital",
                    width: "313px",
                    minWidth: "313px",
                    maxWidth: "313px",
                    title: "Name of hospital",
                    enableIf: "{hospitalization_required_radio} = 'item1'",
                    readOnly: true,
                  },
                ],
                title: "Hospitalization required?",
              },
              {
                type: "radiogroup",
                name: "icu_Intensive_Care_Unit_admission_required",
                title: "ICU (Intensive Care Unit) admission required",
                titleLocation: "top",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
              {
                type: "radiogroup",
                name: "acute_respiratory_distress_syndrome_ards",
                title: "Acute Respiratory Distress Syndrome (ARDS)",
                titleLocation: "top",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
              {
                type: "panel",
                name: "pneumonia_by_chest_xray_panel",
                elements: [
                  {
                    type: "radiogroup",
                    name: "pneumonia_by_chest_xray_radio",
                    width: "49%",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Not applicable (no X-ray performed)",
                      },
                    ],
                    colCount: 3,
                  },
                  {
                    type: "text",
                    name: "pneumonia_by_chest_xray_date",
                    width: "313px",
                    minWidth: "313px",
                    maxWidth: "313px",
                    title: "Date",
                    enableIf: "{pneumonia_by_chest_xray_radio} = 'item1'",
                    readOnly: true,
                    inputType: "date",
                  },
                ],
                title: "Pneumonia by chest X-ray ",
              },
              {
                type: "panel",
                name: "other_severe_or_life_threatening_illness_suggestive_of_an_infection_panel",
                elements: [
                  {
                    type: "radiogroup",
                    name: "other_severe_or_life_threatening_illness_suggestive_of_an_radio",
                    width: "49%",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Unknown",
                      },
                    ],
                    colCount: 3,
                  },
                  {
                    type: "text",
                    name: "other_severe_or_life_threatening_illness_suggestive_of_an_specify",
                    width: "313px",
                    minWidth: "313px",
                    maxWidth: "313px",
                    title: "Specify:",
                    enableIf:
                      "{other_severe_or_life_threatening_illness_suggestive_of_an_radio} = 'item1'",
                    readOnly: true,
                  },
                ],
                title: "Other severe or life-threatening illness suggestive of an infection",
              },
              {
                type: "radiogroup",
                name: "mechanical_ventilation_required",
                title: "Mechanical ventilation required",
                titleLocation: "top",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
              {
                type: "radiogroup",
                name: "extracorporeal_membrane_oxygenation_emo",
                title: "Extracorporeal membrane oxygenation (EMO)",
                titleLocation: "top",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
            ],
            title: "Clinical Course: Complications",
            startWithNewLine: false,
            showNumber: true,
            showQuestionNumbers: "off",
          },
        ],
        navigationTitle: "Complications",
        navigationDescription: "Clinical Course",
      },
      {
        name: "page6",
        elements: [
          {
            type: "panel",
            name: "human_exposures_in_the_days_before_illness_onset",
            elements: [
              {
                type: "panel",
                name: "have_you_travelled_within_the_last_days_domestically_panel",
                elements: [
                  {
                    type: "radiogroup",
                    name: "have_you_travelled_within_the_last_days_domestically_radio",
                    width: "50%",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Unknown",
                      },
                    ],
                    colCount: 3,
                  },
                  {
                    type: "panel",
                    name: "have_you_travelled_within_the_last_days_domestically_date_panel",
                    elements: [
                      {
                        type: "text",
                        name: "have_you_travelled_within_the_last_days_domestically_date_from",
                        title: "from",
                        inputType: "date",
                      },
                      {
                        type: "text",
                        name: "have_you_travelled_within_the_last_days_domestically_date_to",
                        startWithNewLine: false,
                        title: "to",
                        inputType: "date",
                      },
                    ],
                    visible: false,
                    visibleIf:
                      "{have_you_travelled_within_the_last_days_domestically_radio} = 'item1'",
                    title: "Dates of travel",
                  },
                  {
                    type: "text",
                    name: "have_you_travelled_within_the_last_days_domestically_regions",
                    visible: false,
                    visibleIf:
                      "{have_you_travelled_within_the_last_days_domestically_radio} = 'item1'",
                    title: "Regions:",
                  },
                  {
                    type: "text",
                    name: "have_you_travelled_within_the_last_days_domestically_cities_visited",
                    visible: false,
                    visibleIf:
                      "{have_you_travelled_within_the_last_days_domestically_radio} = 'item1'",
                    startWithNewLine: false,
                    title: "Cities visited:",
                  },
                ],
                title: "Have you travelled within the last 14 days domestically?",
                showQuestionNumbers: "off",
              },
              {
                type: "panel",
                name: "have_you_travelled_within_the_last_days_internationall_panel",
                elements: [
                  {
                    type: "radiogroup",
                    name: "have_you_travelled_within_the_last_days_internationall_radio",
                    width: "50%",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Unknown",
                      },
                    ],
                    colCount: 3,
                  },
                  {
                    type: "panel",
                    name: "have_you_travelled_within_the_last_days_internationall_date_panel",
                    elements: [
                      {
                        type: "text",
                        name: "have_you_travelled_within_the_last_days_internationall_date_from",
                        title: "from",
                        inputType: "date",
                      },
                      {
                        type: "text",
                        name: "have_you_travelled_within_the_last_days_internationall_date_to",
                        startWithNewLine: false,
                        title: "to",
                        inputType: "date",
                      },
                    ],
                    visible: false,
                    visibleIf:
                      "{have_you_travelled_within_the_last_days_internationall_radio} = 'item1'",
                    title: "Dates of travel",
                  },
                  {
                    type: "text",
                    name: "have_you_travelled_within_the_last_days_internationall_countries",
                    visible: false,
                    visibleIf:
                      "{have_you_travelled_within_the_last_days_internationall_radio} = 'item1'",
                    title: "Countries visited:",
                  },
                  {
                    type: "text",
                    name: "have_you_travelled_within_the_last_days_internationall_cities",
                    visible: false,
                    visibleIf:
                      "{have_you_travelled_within_the_last_days_internationall_radio} = 'item1'",
                    startWithNewLine: false,
                    title: "Cities visited:",
                  },
                ],
                title: "Have you travelled within the last 14 days internationally?",
                showQuestionNumbers: "off",
              },
              {
                type: "panel",
                name: "in_the_past_days_have_you_had_contact_with_a_anyone_with_suspected_or_confirmed_ncov_infection_panel",
                elements: [
                  {
                    type: "radiogroup",
                    name: "in_the_past_days_have_you_had_contact_with_a_anyone_with_suspected_or_confirmed_ncov_infection_radio",
                    width: "50%",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Unknown",
                      },
                    ],
                    colCount: 3,
                  },
                  {
                    type: "text",
                    name: "in_the_past_days_have_you_had_contact_with_a_anyone_with_suspected_or_confirmed_ncov_infection_date",
                    startWithNewLine: false,
                    title: "Date of last contact",
                    enableIf:
                      "{in_the_past_days_have_you_had_contact_with_a_anyone_with_suspected_or_confirmed_ncov_infection_radio} = 'item1'",
                    readOnly: true,
                    inputType: "date",
                  },
                ],
                title:
                  "In the past 14 days, have you had contact with a anyone with suspected or confirmed 2019-nCoV infection?",
                showQuestionNumbers: "off",
              },
              {
                type: "panel",
                name: "patient_attended_festival_or_mass_gathering_panel",
                elements: [
                  {
                    type: "radiogroup",
                    name: "patient_attended_festival_or_mass_gathering_radio",
                    width: "50%",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Unknown",
                      },
                    ],
                    colCount: 3,
                  },
                  {
                    type: "text",
                    name: "patient_attended_festival_or_mass_gathering_specify",
                    startWithNewLine: false,
                    title: "Specify:",
                    enableIf: "{patient_attended_festival_or_mass_gathering_radio} = 'item1'",
                    readOnly: true,
                  },
                ],
                title: "Patient attended festival or mass gathering",
                showQuestionNumbers: "off",
              },
              {
                type: "radiogroup",
                name: "patient_exposed_to_person_with_similar_illness",
                width: "50%",
                title: "Patient exposed to person with similar illness",
                hideNumber: true,
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
              {
                type: "radiogroup",
                name: "location_of_exposure",
                width: "50%",
                title: "Location of exposure",
                hideNumber: true,
                choices: [
                  {
                    value: "item1",
                    text: "Home",
                  },
                  {
                    value: "item2",
                    text: "Hospital",
                  },
                  {
                    value: "item3",
                    text: "Workplace",
                  },
                  {
                    value: "item4",
                    text: "Tour group",
                  },
                  {
                    value: "item5",
                    text: "Unknown",
                  },
                ],
                hasOther: true,
                otherText: "Other, specify:",
                colCount: 3,
              },
              {
                type: "panel",
                name: "patient_visited_or_was_admitted_to_inpatient_health_facility_panel",
                elements: [
                  {
                    type: "radiogroup",
                    name: "patient_visited_or_was_admitted_to_inpatient_health_facility_radio",
                    width: "50%",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Unknown",
                      },
                    ],
                    colCount: 3,
                  },
                  {
                    type: "text",
                    name: "patient_visited_or_was_admitted_to_inpatient_health_facility_specify",
                    startWithNewLine: false,
                    title: "Specify:",
                    enableIf:
                      "{patient_visited_or_was_admitted_to_inpatient_health_facility_radio} = 'item1'",
                    readOnly: true,
                  },
                  {
                    type: "panel",
                    name: "patient_visited_outpatient_treatment_facility_panel",
                    elements: [
                      {
                        type: "radiogroup",
                        name: "patient_visited_outpatient_treatment_facility_radio",
                        width: "50%",
                        titleLocation: "hidden",
                        choices: [
                          {
                            value: "item1",
                            text: "Yes",
                          },
                          {
                            value: "item2",
                            text: "No",
                          },
                          {
                            value: "item3",
                            text: "Unknown",
                          },
                        ],
                        colCount: 3,
                      },
                      {
                        type: "text",
                        name: "patient_visited_outpatient_treatment_facility_specify",
                        startWithNewLine: false,
                        title: "Specify:",
                        enableIf: "{patient_visited_outpatient_treatment_facility_radio} = 'item1'",
                        readOnly: true,
                      },
                    ],
                    title: "Patient visited outpatient treatment facility",
                  },
                  {
                    type: "panel",
                    name: "patient_visited_traditional_healer_panel",
                    elements: [
                      {
                        type: "radiogroup",
                        name: "patient_visited_traditional_healer_radio",
                        width: "50%",
                        titleLocation: "hidden",
                        choices: [
                          {
                            value: "item1",
                            text: "Yes",
                          },
                          {
                            value: "item2",
                            text: "No",
                          },
                          {
                            value: "item3",
                            text: "Unknown",
                          },
                        ],
                        colCount: 3,
                      },
                      {
                        type: "text",
                        name: "patient_visited_traditional_healer_specify",
                        startWithNewLine: false,
                        title: "Specify:",
                        enableIf: "{patient_visited_traditional_healer_radio} = 'item1'",
                        readOnly: true,
                      },
                      {
                        type: "panel",
                        name: "patient_occupation_specify_location_facility_panel",
                        elements: [
                          {
                            type: "checkbox",
                            name: "patient_occupation_specify_location_facility_checkbox",
                            titleLocation: "hidden",
                            choices: [
                              {
                                value: "item1",
                                text: "Health care worker",
                              },
                              {
                                value: "item2",
                                text: "Working with animals ",
                              },
                              {
                                value: "item3",
                                text: "Health laboratory worker",
                              },
                              {
                                value: "item4",
                                text: "Student",
                              },
                            ],
                            hasOther: true,
                            otherText: "Other, specify:",
                            colCount: 3,
                          },
                          {
                            type: "text",
                            name: "patient_occupation_specify_location_facility_specify",
                            visible: false,
                            visibleIf:
                              "{patient_occupation_specify_location_facility_checkbox} notempty",
                            title: "For each occupation, please specify location or facility:",
                          },
                        ],
                        title: "Patient occupation (specify location/facility)",
                        showQuestionNumbers: "off",
                      },
                    ],
                    title: "Patient visited traditional healer",
                  },
                ],
                title: "Patient visited or was admitted to inpatient health facility",
                showQuestionNumbers: "off",
              },
            ],
            title: "Human exposures in the 14 days before illness onset",
            showNumber: true,
          },
        ],
        navigationTitle: "Exposures",
        navigationDescription: "Before illness",
      },
      {
        name: "page7",
        elements: [
          {
            type: "image",
            name: "seventh_image",
            width: "400px",
            minWidth: "400px",
            maxWidth: "400px",
            imageLink: "https://surveyjs.io/Content/Images/examples/covid/07.png",
            imageHeight: 441,
            imageWidth: 400,
          },
          {
            type: "panel",
            name: "status_of_form_completion_panel",
            elements: [
              {
                type: "boolean",
                name: "status_of_form_completion_boolean",
                title: "Form completed",
                isRequired: true,
                labelTrue: "Yes",
                labelFalse: "No or partially",
              },
              {
                type: "radiogroup",
                name: "status_of_form_completion_radio",
                visible: false,
                visibleIf: "{status_of_form_completion_boolean} = false",
                title: "Reason:",
                choices: [
                  {
                    value: "item1",
                    text: "Missed",
                  },
                  {
                    value: "item2",
                    text: "Not attempted",
                  },
                  {
                    value: "item3",
                    text: "Not performed",
                  },
                  {
                    value: "item4",
                    text: "Refusal",
                  },
                ],
                hasOther: true,
                otherText: "Other, specific:",
              },
            ],
            title: "Status of form completion",
            startWithNewLine: false,
            showNumber: true,
            showQuestionNumbers: "off",
          },
        ],
        navigationTitle: "Completion",
        navigationDescription: "Status of form",
      },
    ],
    showProgressBar: "top",
    progressBarType: "buttons",
    formSettings: {
      requiredLogin: false,
      multipleSubmission: true,
      showCaptcha: false,
      privacySettings: "public",
      locationSettings: false,
      limitedUser: [],
      country: null,
      city: null,
      publishSettings: true,
      unPublishSettings: false,
      publishDate: null,
      publishTime: null,
      publishRedirectUrl: "",
      publishRedirectBtnText: "",
      publishDescription: "",
      unPublishDate: null,
      unPublishTime: null,
      unPublishRedirectUrl: "",
      unPublishRedirectBtnText: "",
      unPublishDescription: "",
    },
  },
  {
    id: uuidv4(),
    title: "Product/Market Fit Survey",
    description: "Product/Market Fit Survey Template",
    logoPosition: "right",
    pages: [
      {
        name: "page1",
        elements: [
          {
            type: "radiogroup",
            name: "customer_role",
            title: "What best describes your role?",
            choices: [
              "Engineering Lead",
              "Project Manager",
              "Software Developer",
              "Designer",
              "Product Manager",
              "CEO / Founder",
              "Customer Support",
            ],
            hasOther: true,
            otherText: "Other",
            colCount: 3,
          },
          {
            type: "radiogroup",
            name: "start_using",
            title: "How did you start using the product?",
            choices: [
              {
                value: "created",
                text: "I created my account",
              },
              {
                value: "invited",
                text: "I was invited to an account",
              },
            ],
          },
          {
            type: "radiogroup",
            name: "product_discovering",
            title: "How did you first discover the product? ",
            choices: ["Friend or colleague", "Search engine", "Facebook", "Twitter", "Blog"],
            hasOther: true,
            otherText: "Other",
            colCount: 3,
          },
          {
            type: "radiogroup",
            name: "paid_customer",
            title: "Do you currently pay for the product? ",
            isRequired: true,
            choices: ["Yes", "No"],
          },
        ],
      },
      {
        name: "page2",
        elements: [
          {
            type: "radiogroup",
            name: "product_fit",
            title: "How would you feel if you could no longer use the product?",
            isRequired: true,
            choices: [
              {
                value: "3",
                text: "Very disappointed",
              },
              {
                value: "2",
                text: "Somewhat disappointed",
              },
              {
                value: "1",
                text: "Not disappointed",
              },
            ],
          },
          {
            type: "comment",
            name: "product_fit_comment",
            visibleIf: "{product_fit} notempty",
            title: "Please help us understand why you selected the answer above",
          },
        ],
      },
      {
        name: "page3",
        elements: [
          {
            type: "radiogroup",
            name: "product_alternative",
            title:
              "What would you use as an alternative if [the product] were no\nlonger available?",
            choices: [
              "Alternative 1",
              "Alternative 2",
              "Alternative 3",
              "Alternative 4",
              "Alternative 5",
              "Alternative 6",
            ],
            hasOther: true,
            otherText: "Other (please name)",
            colCount: 3,
          },
          {
            type: "radiogroup",
            name: "product_benefit",
            title: "What is the primary benefit that you have received from the\nproduct?",
            choices: ["Benefit 1", "Benefit 2", "Benefit 3", "Benefit 4", "Benefit 5", "Benefit 6"],
            hasOther: true,
            colCount: 3,
          },
          {
            type: "radiogroup",
            name: "product_recommend",
            title: "Have you recommended the product to anyone?",
            choices: ["Yes", "No"],
          },
        ],
      },
      {
        name: "page4",
        elements: [
          {
            type: "rating",
            name: "nps_score",
            title: "How likely are you to recommend the product to a friend or\ncolleague? ",
            isRequired: true,
            rateMin: 0,
            rateMax: 10,
            minRateDescription: "Most unlikely",
            maxRateDescription: "Most likely",
          },
          {
            type: "radiogroup",
            name: "favorite_functionality",
            title: "What's your favorite functionality / add-on for the product?",
            choices: ["Feature 1", "Feature 2", "Feature 3", "Feature 4", "Feature 5", "Feature 6"],
            hasOther: true,
            colCount: 3,
          },
          {
            type: "comment",
            name: "product_improvement",
            title: "How could the product be improved to better meet your\nneeds?",
          },
        ],
      },
      {
        name: "page5",
        elements: [
          {
            type: "multipletext",
            name: "contact_customer",
            title: "Want us to follow-up? Leave your name and email here:",
            items: [
              {
                name: "Name",
              },
              {
                name: "E-mail",
                inputType: "email",
                validators: [
                  {
                    type: "email",
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
    formSettings: {
      requiredLogin: false,
      multipleSubmission: true,
      showCaptcha: false,
      privacySettings: "public",
      locationSettings: false,
      limitedUser: [],
      country: null,
      city: null,
      publishSettings: true,
      unPublishSettings: false,
      publishDate: null,
      publishTime: null,
      publishRedirectUrl: "",
      publishRedirectBtnText: "",
      publishDescription: "",
      unPublishDate: null,
      unPublishTime: null,
      unPublishRedirectUrl: "",
      unPublishRedirectBtnText: "",
      unPublishDescription: "",
    },
  },
  {
    id: uuidv4(),
    title: "Cancellation Survey",
    description:
      "Thank you for using our service. We would highly appreciate if you would take the time to fill our cancellation survey. This would help us improve the service.",
    logoPosition: "right",
    pages: [
      {
        name: "page1",
        elements: [
          {
            type: "radiogroup",
            name: "using_duration",
            title: "How long have you been using the service?",
            choices: [
              "Less than a month",
              "1-6 months",
              "7-12 months",
              "1-3 years",
              "Over 3 years",
            ],
          },
          {
            type: "radiogroup",
            name: "using_frequency",
            title: "How often did you use the service?",
            choices: [
              "Once a week",
              "2 or 3 times a month",
              "Once a month",
              "Less than once a month",
            ],
          },
          {
            type: "radiogroup",
            name: "cancel_reason",
            title: "What was the main reason for cancelling the service?",
            choices: [
              "No longer need it",
              "It didn't meet my needs",
              "Found a better alternative",
              "Found a cheaper alternative",
              "Quality was less than expected",
              "Ease of use was less than expected",
              "Access to the service was less than expected",
              "Customer service was less than expected",
            ],
            hasOther: true,
          },
          {
            type: "radiogroup",
            name: "satisfaction",
            title: "Overall, how satisfied were you with the service?",
            choices: ["Very Satisfied", "Satisfied", "Neutral", "Unsatisfied", "Very Unsatisfied"],
          },
          {
            type: "matrix",
            name: "future_using",
            titleLocation: "hidden",
            columns: ["Definitely", "Probably", "Not Sure", "Probably Not", "Definitely Not"],
            rows: [
              {
                value: "use_in_future",
                text: "Will you use our service in the future?",
              },
              {
                value: "recommend",
                text: "Will you recommend our service to others?",
              },
            ],
          },
          {
            type: "comment",
            name: "service_improvements",
            title: "How can we improve our service?",
          },
        ],
      },
    ],
    showQuestionNumbers: "off",
    formSettings: {
      requiredLogin: false,
      multipleSubmission: true,
      showCaptcha: false,
      privacySettings: "public",
      locationSettings: false,
      limitedUser: [],
      country: null,
      city: null,
      publishSettings: true,
      unPublishSettings: false,
      publishDate: null,
      publishTime: null,
      publishRedirectUrl: "",
      publishRedirectBtnText: "",
      publishDescription: "",
      unPublishDate: null,
      unPublishTime: null,
      unPublishRedirectUrl: "",
      unPublishRedirectBtnText: "",
      unPublishDescription: "",
    },
  },
  {
    id: uuidv4(),
    title: "Product Feedback",
    description: "Customer Satisfaction Survey",
    logoPosition: "right",
    pages: [
      {
        name: "page1",
        elements: [
          {
            type: "matrix",
            name: "Quality",
            title: "Please indicate if you agree or disagree with the following statements",
            columns: [
              {
                value: 1,
                text: "Strongly Disagree",
              },
              {
                value: 2,
                text: "Disagree",
              },
              {
                value: 3,
                text: "Neutral",
              },
              {
                value: 4,
                text: "Agree",
              },
              {
                value: 5,
                text: "Strongly Agree",
              },
            ],
            rows: [
              {
                value: "affordable",
                text: "Product is affordable",
              },
              {
                value: "does what it claims",
                text: "Product does what it claims",
              },
              {
                value: "better then others",
                text: "Product is better than other products on the market",
              },
              {
                value: "easy to use",
                text: "Product is easy to use",
              },
            ],
          },
          {
            type: "rating",
            name: "satisfaction",
            title: "How satisfied are you with the Product?",
            isRequired: true,
            minRateDescription: "Not Satisfied",
            maxRateDescription: "Completely satisfied",
          },
          {
            type: "rating",
            name: "recommend friends",
            visibleIf: "{satisfaction} > 3",
            title: "How likely are you to recommend the Product to a friend or co-worker?",
            minRateDescription: "Will not recommend",
            maxRateDescription: "I will recommend",
          },
          {
            type: "comment",
            name: "suggestions",
            title: "What would make you more satisfied with the Product?",
          },
        ],
      },
      {
        name: "page2",
        elements: [
          {
            type: "radiogroup",
            name: "price to competitors",
            title: "Compared to our competitors, do you feel the Product is",
            choices: ["Less expensive", "Priced about the same", "More expensive", "Not sure"],
          },
          {
            type: "radiogroup",
            name: "price",
            title: "Do you feel our current price is merited by our product?",
            choices: [
              {
                value: "correct",
                text: "Yes, the price is about right",
              },
              {
                value: "low",
                text: "No, the price is too low for your product",
              },
              {
                value: "high",
                text: "No, the price is too high for your product",
              },
            ],
          },
          {
            type: "multipletext",
            name: "pricelimit",
            title: "What is the... ",
            items: [
              {
                name: "mostamount",
                title: "Most amount you would every pay for a product like ours",
              },
              {
                name: "leastamount",
                title: "The least amount you would feel comfortable paying",
              },
            ],
          },
        ],
      },
      {
        name: "page3",
        elements: [
          {
            type: "text",
            name: "email",
            title:
              "Thank you for taking our survey. Your survey is almost complete, please enter your email address in the box below if you wish to participate in our drawing, then press the 'Submit' button.",
          },
        ],
      },
    ],
    formSettings: {
      requiredLogin: false,
      multipleSubmission: true,
      showCaptcha: false,
      privacySettings: "public",
      locationSettings: false,
      limitedUser: [],
      country: null,
      city: null,
      publishSettings: true,
      unPublishSettings: false,
      publishDate: null,
      publishTime: null,
      publishRedirectUrl: "",
      publishRedirectBtnText: "",
      publishDescription: "",
      unPublishDate: null,
      unPublishTime: null,
      unPublishRedirectUrl: "",
      unPublishRedirectBtnText: "",
      unPublishDescription: "",
    },
  },
  {
    id: uuidv4(),
    title: "Patient Medical History",
    description: "Patient Past Medical, Social & Family History",
    logoPosition: "right",
    pages: [
      {
        name: "introduction",
        elements: [
          {
            type: "panel",
            name: "patienName",
            elements: [
              {
                type: "text",
                name: "patientLastName",
                title: "(Last)",
                isRequired: true,
              },
              {
                type: "text",
                name: "patienFirstName",
                startWithNewLine: false,
                title: "(First)",
                isRequired: true,
              },
              {
                type: "text",
                name: "patientMiddleName",
                title: "(M.I)",
              },
            ],
            questionTitleLocation: "bottom",
            title: "Patient Name",
          },
          {
            type: "panel",
            name: "panel2",
            elements: [
              {
                type: "text",
                name: "socialsecurity",
                title: "Social Security #:",
                isRequired: true,
              },
              {
                type: "text",
                name: "birthDate",
                startWithNewLine: false,
                title: "Date of birth:",
                isRequired: true,
                inputType: "date",
              },
              {
                type: "radiogroup",
                name: "sex",
                title: "Sex:",
                isRequired: true,
                choices: [
                  {
                    value: "male",
                    text: "Male",
                  },
                  {
                    value: "female",
                    text: "Female",
                  },
                ],
                colCount: 0,
              },
            ],
            questionTitleLocation: "left",
            title: "Social Security & Birth Date",
          },
          {
            type: "panel",
            name: "panel1",
            elements: [
              {
                type: "radiogroup",
                name: "completedBy",
                title: "Who completed this form:",
                isRequired: true,
                choices: [
                  {
                    value: "patient",
                    text: "Patient",
                  },
                  {
                    value: "spouse",
                    text: "Spouse",
                  },
                ],
                hasOther: true,
                otherText: "Other (specify)",
                colCount: 0,
              },
              {
                type: "text",
                name: "completedByOtherName",
                visibleIf: '{completedBy} != "patient"',
                startWithNewLine: false,
                title: "Name (if other than patient):",
                isRequired: true,
              },
            ],
            title: "Completed By",
          },
        ],
        title: "Introduction",
      },
      {
        name: "medicalHistory",
        elements: [
          {
            type: "radiogroup",
            name: "everhospitalized",
            title: "Have you ever been hospitalized?",
            isRequired: true,
            choices: [
              {
                value: "no",
                text: "No",
              },
              {
                value: "yes",
                text: "Yes",
              },
            ],
            colCount: 0,
          },
          {
            type: "radiogroup",
            name: "injuriesbrokenbones",
            title: "Have you had any serious injuries and/or broken bones?",
            isRequired: true,
            choices: [
              {
                value: "no",
                text: "No",
              },
              {
                value: "yes",
                text: "Yes",
              },
            ],
            colCount: 0,
          },
          {
            type: "comment",
            name: "injuriesbrokenbones_describe",
            visible: false,
            visibleIf: '{injuriesbrokenbones} = "yes"',
            startWithNewLine: false,
            title: "Describe",
            isRequired: true,
          },
          {
            type: "radiogroup",
            name: "bloodtransfusion",
            title: "Have you ever received a blood transfusion?",
            isRequired: true,
            choices: [
              {
                value: "unknown",
                text: "Unknown",
              },
              {
                value: "no",
                text: "No",
              },
              {
                value: "yes",
                text: "Yes",
              },
            ],
            colCount: 0,
          },
          {
            type: "text",
            name: "bloodtransfusion_years",
            visible: false,
            visibleIf: '{bloodtransfusion}="yes"',
            startWithNewLine: false,
            title: "Approximate year(s)",
          },
          {
            type: "radiogroup",
            name: "ousideUSACanada",
            title: "Have you ever traveled or lived outside the United States or Canada?",
            isRequired: true,
            choices: [
              {
                value: "no",
                text: "No",
              },
              {
                value: "yes",
                text: "Yes",
              },
            ],
            colCount: 0,
          },
          {
            type: "comment",
            name: "ousideUSACanada_describe",
            visible: false,
            visibleIf: '{ousideUSACanada} = "yes"',
            startWithNewLine: false,
            title: "Describe",
            isRequired: true,
          },
          {
            type: "matrixdropdown",
            name: "immunizations",
            title:
              "Have you received the following IMMUNIZATIONS?  If yes, indicate the approximate year it was last given:",
            titleLocation: "top",
            columns: [
              {
                name: "answer",
                title: "Please select",
                cellType: "radiogroup",
                isRequired: true,
                choices: [
                  {
                    value: "unknown",
                    text: "Unknown",
                  },
                  {
                    value: "no",
                    text: "No",
                  },
                  {
                    value: "yes",
                    text: "Yes",
                  },
                ],
              },
              {
                name: "year",
                title: "Year",
                cellType: "text",
                isRequired: true,
                visibleIf: '{row.answer} = "yes"',
              },
            ],
            choices: [1, 2, 3, 4, 5],
            rows: [
              "Pneumococcal (for pneumonia)",
              "Hepatitis A",
              "Hepatitis B",
              "Tetanus/Diphtheria within last 10 years",
              "Influenza (flu)",
              "Measles",
              "Mumps",
              "Rubella",
              "Polio",
            ],
          },
          {
            type: "matrixdynamic",
            name: "problems1",
            title: "Have you ever had any of the following?",
            titleLocation: "top",
            columns: [
              {
                name: "problem",
                title: "Problem",
                choices: [
                  "Abnormal chest x-ray",
                  "Anesthesia complications",
                  "Anxiety, depression or mental illness",
                  "Blood problems (abnormal bleeding, anemia, high or low white count)",
                  "Diabetes",
                  "Growth removed from the colon or rectum (polyp or tumor)",
                  "High blood pressure",
                  "High cholesterol or triglycerides",
                  "Sexually transmitted disease",
                  "Stroke or TIA",
                  "Treatment for alcohol and/or drug abuse",
                  "Tuberculosis or positive tuberculin skin test",
                  "Cosmetic or plastic surgery",
                ],
              },
              {
                name: "answer",
                title: "Please answer",
                cellType: "radiogroup",
                isRequired: true,
                choices: [
                  {
                    value: "no",
                    text: "No",
                  },
                  {
                    value: "yes",
                    text: "Yes",
                  },
                ],
              },
              {
                name: "description",
                title: "Describe the problem",
                cellType: "comment",
                isRequired: true,
                visibleIf: '{row.answer} = "yes"',
              },
            ],
            choices: [1, 2, 3, 4, 5],
            rowCount: 1,
          },
          {
            type: "matrixdynamic",
            name: "problems2",
            title:
              "Indicate whether you have ever had a medical problem and/or surgery related to each of the following",
            titleLocation: "top",
            columns: [
              {
                name: "problem",
                title: "Problem",
                cellType: "dropdown",
                isRequired: true,
                maxWidth: "300px",
                choices: [
                  "Eyes (cataracts, glaucoma)",
                  "Ears, nose, sinuses, or tonsils",
                  "Thyroid or parathyroid glands",
                  "Heart valves or abnormal heart rhythm",
                  "Coronary (heart) arteries (angina)",
                  "Arteries (aorta, arteries to head, arms, legs) ",
                  "Veins or blood clots in the veins",
                  "Lungs",
                  "Esophagus or stomach (ulcer)",
                  "Bowel (small & large intestine)",
                  "Appendix",
                  "Liver or gallbladder (including hepatitis)",
                  "Hernia",
                  "Kidneys or bladder",
                  "Bones, joints or muscles",
                  "Back, neck or spine",
                  "Brain",
                  "Skin",
                  "Breasts",
                  "Females: uterus, tubes, ovaries",
                  "Males: prostate, penis, testes, vasectomy",
                  "Other: Describe",
                ],
                choicesOrder: "asc",
              },
              {
                name: "type",
                title: "Type",
                cellType: "checkbox",
                isRequired: true,
                choices: [
                  {
                    value: "medical",
                    text: "Medical Problem",
                  },
                  {
                    value: "surgery",
                    text: "Surgery",
                  },
                ],
              },
              {
                name: "year",
                title: "Year(s) of Surgery",
                cellType: "text",
                isRequired: true,
                visibleIf: '{row.type} contains "surgery"',
              },
              {
                name: "describe",
                title: "Describe",
                cellType: "comment",
                isRequired: true,
              },
            ],
            choices: [1, 2, 3, 4, 5],
            rowCount: 1,
            addRowText: "Add Problem",
          },
        ],
        title: "Past Medical History",
      },
      {
        name: "socialHistory",
        elements: [
          {
            type: "panel",
            name: "education",
            elements: [
              {
                type: "dropdown",
                name: "schoolYearsCompleted",
                title: "How many yeas of school have you completed?",
                isRequired: true,
                choicesMax: 12,
              },
            ],
            title: "Education",
          },
          {
            type: "panel",
            name: "occupations",
            elements: [
              {
                type: "radiogroup",
                name: "employmentStatus",
                title: "Your current employment status:",
                isRequired: true,
                choices: ["Retired", "Unemployed", "Homemaker", "Employed"],
                colCount: 0,
              },
              {
                type: "text",
                name: "currentOcupation",
                visible: false,
                visibleIf: "{employmentStatus} = 'Employed'",
                title: "Current Ocupation(s):",
                isRequired: true,
              },
              {
                type: "comment",
                name: "previousOccupations",
                title: "Previous Occupations/Jobs:",
              },
            ],
            title: "Occupations",
          },
          {
            type: "panel",
            name: "disability",
            elements: [
              {
                type: "radiogroup",
                name: "disabled",
                title: "Are you disabled?",
                isRequired: true,
                choices: ["No", "Yes"],
                colCount: 0,
              },
              {
                type: "comment",
                name: "disableInfo",
                visible: false,
                visibleIf: "{disabled} = 'Yes'",
                title: "Info:",
              },
            ],
            title: "Disability",
          },
          {
            type: "panel",
            name: "abuse",
            elements: [
              {
                type: "radiogroup",
                name: "abused",
                title: "Have you even been physically, sexually, or emotionally abused?",
                isRequired: true,
                choices: ["No", "Yes"],
                colCount: 0,
              },
              {
                type: "comment",
                name: "abusedInfo",
                visible: false,
                visibleIf: "{abused} = 'Yes'",
                title: "Info:",
              },
              {
                type: "matrixdropdown",
                name: "subsctancesUsing",
                title: "Have you used any of the following substances?",
                titleLocation: "top",
                columns: [
                  {
                    name: "current",
                    title: "Currently Use?",
                    cellType: "radiogroup",
                    isRequired: true,
                    choices: ["No", "Yes"],
                  },
                  {
                    name: "previous",
                    title: "Previously Used?",
                    cellType: "radiogroup",
                    isRequired: true,
                    choices: ["No", "Yes"],
                  },
                  {
                    name: "amount",
                    title: "Type/Amount/Frequancy",
                    cellType: "text",
                    isRequired: true,
                    visibleIf: "{row.current} = 'Yes' or {row.previous} = 'Yes'",
                  },
                  {
                    name: "long",
                    title: "How Long? (Years)",
                    cellType: "text",
                    isRequired: true,
                    visibleIf: "{row.current} = 'Yes' or {row.previous} = 'Yes'",
                  },
                  {
                    name: "stopped",
                    title: "When stopped? (Year)",
                    cellType: "text",
                    isRequired: true,
                    visibleIf: "{row.previous} = 'Yes'",
                  },
                ],
                choices: [1, 2, 3, 4, 5],
                rows: [
                  "Caffeine: coffee, tea, soda",
                  "Tobacco",
                  "Alcohol - bear, wine, liquor",
                  "Recreational/Street drugs",
                ],
              },
            ],
            title: "Abuse",
          },
          {
            type: "panel",
            name: "maritalStatus",
            elements: [
              {
                type: "radiogroup",
                name: "currentlyMarried",
                title: "Are you currently married?",
                isRequired: true,
                choices: ["No", "Yes"],
                colCount: 0,
              },
              {
                type: "text",
                name: "currentMarriageYear",
                visible: false,
                visibleIf: "{currentlyMarried} = 'Yes'",
                startWithNewLine: false,
                title: "In what year did this marriage occure?",
                isRequired: true,
              },
              {
                type: "paneldynamic",
                name: "previousMarriages",
                title: "List all previous marriages",
                templateElements: [
                  {
                    type: "text",
                    name: "previousMarriageYear",
                    title: "Year married:",
                    isRequired: true,
                  },
                  {
                    type: "text",
                    name: "previousMarriageDuration",
                    startWithNewLine: false,
                    title: "Duration (in Years):",
                    isRequired: true,
                  },
                ],
                panelCount: 1,
                panelAddText: "Add marriage",
              },
              {
                type: "panel",
                name: "currentSpouse",
                elements: [
                  {
                    type: "radiogroup",
                    name: "currentSpouseStatus",
                    title: " ",
                    isRequired: true,
                    choices: ["Not applicable", "Alive", "Deceased"],
                    colCount: 0,
                  },
                  {
                    type: "text",
                    name: "currentSpouseAge",
                    visible: false,
                    visibleIf: "{currentSpouseStatus} = 'Alive'",
                    startWithNewLine: false,
                    title: "Age",
                    isRequired: true,
                  },
                  {
                    type: "comment",
                    name: "currentSpouseHelthProblems",
                    visibleIf: "{currentSpouseStatus} != 'Not applicable' ",
                    title: "Health problems or cause of death:",
                    isRequired: true,
                    rows: 2,
                  },
                  {
                    type: "radiogroup",
                    name: "currentSpouseEmploymentStatus",
                    visible: false,
                    visibleIf: "{currentSpouseStatus} = 'Alive'",
                    title: "Current employment status:",
                    isRequired: true,
                    choices: ["Retired", "Unemployed", "Homemaker", "Employed"],
                    colCount: 0,
                  },
                ],
                title: "Current Spouse Information",
              },
              {
                type: "text",
                name: "currentSpouseOccupation",
                visible: false,
                visibleIf: "{currentSpouseEmploymentStatus} = 'Employed'",
                title: "Current occupation(s):",
                isRequired: true,
              },
            ],
            title: "Marital Status",
          },
        ],
        questionTitleLocation: "left",
        title: "Social History",
      },
      {
        name: "familyHistory",
        elements: [
          {
            type: "radiogroup",
            name: "adopted",
            title: "Are you adopted?",
            isRequired: true,
            choices: ["Yes", "No"],
            colCount: 0,
          },
          {
            type: "html",
            name: "adoptedInfo",
            visible: false,
            visibleIf: "{adopted} = 'Yes'",
            html: "If known, complete the following information about your <b>blood</b> relatives (include children).  Exclude adoptive parents, siblings and   adopted children.",
          },
          {
            type: "html",
            name: "nonAdoptedInfo",
            visible: false,
            visibleIf: "{adopted} = 'No'",
            html: "Complete the following information about your <b>blood</b> relatives.  Exclude adoptive siblings and adopted children. ",
          },
          {
            type: "paneldynamic",
            name: "bloodRelativesInfo",
            title: "Blood relatives information",
            templateElements: [
              {
                type: "dropdown",
                name: "relativeType",
                title: "Blood Relative:",
                isRequired: true,
                choices: ["Father", "Mother", "Brother", "Sister", "Son", "Daughter"],
              },
              {
                type: "radiogroup",
                name: "relativeStatus",
                startWithNewLine: false,
                title: "Is he/she:",
                isRequired: true,
                choices: ["Alive", "Deceased", "Unknown"],
                colCount: 0,
              },
              {
                type: "text",
                name: "relativeAge",
                visible: false,
                visibleIf: "{panel.relativeStatus} = 'Alive'",
                startWithNewLine: false,
                title: "Current age:",
                isRequired: true,
              },
              {
                type: "text",
                name: "relativeAgeOfDeath",
                visible: false,
                visibleIf: "{panel.relativeStatus} = 'Deceased'",
                startWithNewLine: false,
                title: "Age of death:",
                isRequired: true,
              },
              {
                type: "panel",
                name: "relativeDeathInfo",
                elements: [
                  {
                    type: "radiogroup",
                    name: "relativeCauseOfDeath",
                    title: "Cause of death:",
                    isRequired: true,
                    choices: ["Known", "Unknown"],
                    colCount: 0,
                  },
                  {
                    type: "comment",
                    name: "relativeCauseOfDeathDescription",
                    visible: false,
                    visibleIf: "{panel.relativeCauseOfDeath} = 'Known'",
                    title: "Description:",
                    isRequired: true,
                  },
                ],
                visible: false,
                visibleIf: "{panel.relativeStatus} = 'Deceased'",
              },
              {
                type: "matrixdynamic",
                name: "relativeCondition",
                title: "Describe the illness or conditios",
                titleLocation: "top",
                columns: [
                  {
                    name: "name",
                    title: "Illness/Condition",
                    cellType: "dropdown",
                    isRequired: true,
                    choices: [
                      "Cancer",
                      "Heart Disease",
                      "Diabetes",
                      "Stroke/TIA",
                      "High Blood Pressure",
                      "High Cholesterol or Triglycerides",
                      "Liver Disease",
                      "Alcohol or Drug Abuse",
                      "Anxiety, Depression or Psychiatric Illness",
                      "Tuberculosis",
                      "Anesthesia Complications",
                      "Genetic Disorder",
                      "Other",
                    ],
                  },
                  {
                    name: "description",
                    title: "Describe",
                    cellType: "comment",
                    isRequired: true,
                  },
                ],
                choices: [1, 2, 3, 4, 5],
                rowCount: 1,
                addRowText: "Add illness",
              },
            ],
            panelCount: 1,
            panelAddText: "Add Family Member",
          },
          {
            type: "comment",
            name: "relativesAdditionalInfo",
            title: "Other information about your family which you want us to know: ",
          },
        ],
        questionTitleLocation: "left",
        title: "Family History",
      },
      {
        name: "healthcareProvider",
        elements: [
          {
            type: "radiogroup",
            name: "primaryCareProvider",
            title: "Do you have a Primary Care Provider?",
            isRequired: true,
            choices: ["No", "Yes"],
            colCount: 0,
          },
          {
            type: "panel",
            name: "primaryCareProviderInfo",
            elements: [
              {
                type: "text",
                name: "primaryCareProviderName",
                title: "Name:",
                isRequired: true,
              },
              {
                type: "text",
                name: "primaryCareProviderPhone",
                startWithNewLine: false,
                title: "Phone:",
                isRequired: true,
              },
              {
                type: "comment",
                name: "primaryCareProviderAddress",
                title: "Address:",
              },
              {
                type: "radiogroup",
                name: "primaryCareProviderSent",
                title: "Do you want a summary of your visit sent to this person?",
                isRequired: true,
                choices: ["No", "Yes"],
                colCount: 0,
              },
            ],
            visible: false,
            visibleIf: "{primaryCareProvider} = 'Yes'",
            title: "Primary Care Provider",
          },
          {
            type: "radiogroup",
            name: "primaryCareProviderRecommend",
            title:
              "Did a non-Vanderbilt physician or healthcare provider recommend or arrange this visit for you?",
            isRequired: true,
            choices: ["No", "Yes"],
            colCount: 0,
          },
          {
            type: "panel",
            name: "primaryCareProviderRecommendInfo",
            elements: [
              {
                type: "radiogroup",
                name: "primaryCareProviderWhoSent",
                title: "Who sent you?",
                isRequired: true,
                choices: [
                  {
                    value: "primary",
                    text: "Your Primary Care Provider (as listed above) ",
                  },
                  {
                    value: "other",
                    text: "Other physician or healthcare provider",
                  },
                ],
                colCount: 0,
              },
              {
                type: "panel",
                name: "primaryCareProviderOtherInfo",
                elements: [
                  {
                    type: "text",
                    name: "primaryCareProviderOtherName",
                    title: "Name:",
                    isRequired: true,
                  },
                  {
                    type: "text",
                    name: "primaryCareProviderOtherPhone",
                    startWithNewLine: false,
                    title: "Phone:",
                    isRequired: true,
                  },
                  {
                    type: "comment",
                    name: "primaryCareProviderOtherAddress",
                    title: "Address:",
                  },
                  {
                    type: "radiogroup",
                    name: "primaryCareProviderOtherSent",
                    title: "Do you want a summary of your visit sent to this person?",
                    isRequired: true,
                    choices: ["No", "Yes"],
                    colCount: 0,
                  },
                ],
                visible: false,
                visibleIf: "{primaryCareProviderWhoSent} = 'other'",
                title: "Other physician or healthcare provider",
              },
            ],
            visible: false,
            visibleIf: "{primaryCareProviderRecommend} = 'Yes'",
          },
        ],
        questionTitleLocation: "left",
        title: "Healthcare Provider Information",
      },
      {
        name: "medications",
        elements: [
          {
            type: "panel",
            name: "currentUsingMedication",
            elements: [
              {
                type: "radiogroup",
                name: "currentMedication",
                title:
                  "Are you currently taking any prescription and/or non-prescription medications including vitamins, nutritional supplements, oral contraceptives, pain relievers, diuretics, laxatives, herbal remedies, and cold medications? ",
                isRequired: true,
                choices: ["No", "Yes"],
                colCount: 0,
              },
              {
                type: "matrixdynamic",
                name: "currentMedicationList",
                visible: false,
                visibleIf: "{currentMedication} = 'Yes'",
                title: "Medication list",
                description: "Please add all medication you are currently taking",
                columns: [
                  {
                    name: "name",
                    title: "Name of Medication",
                    cellType: "text",
                    isRequired: true,
                  },
                  {
                    name: "dose",
                    title: "Dose",
                    cellType: "text",
                    isRequired: true,
                  },
                  {
                    name: "often",
                    title: "How Often Taken",
                    cellType: "text",
                    isRequired: true,
                  },
                ],
                choices: [1, 2, 3, 4, 5],
                rowCount: 1,
                addRowText: "Add medication",
              },
            ],
            title: "Medications currently using",
          },
          {
            type: "panel",
            name: "recentlyUsedMedication",
            elements: [
              {
                type: "radiogroup",
                name: "recentlyUsedMedication",
                title: "Are there other medications you have recently used?",
                isRequired: true,
                choices: ["No", "Yes"],
                colCount: 0,
              },
              {
                type: "matrixdynamic",
                name: "recentlyUsedMedicationList",
                visible: false,
                visibleIf: "{recentlyUsedMedication} = 'Yes'",
                title: "Medication list",
                description: "Please add all medication you have recently used",
                columns: [
                  {
                    name: "name",
                    title: "Name of Medication",
                    cellType: "text",
                    isRequired: true,
                  },
                  {
                    name: "dose",
                    title: "Dose",
                    cellType: "text",
                    isRequired: true,
                  },
                  {
                    name: "often",
                    title: "How Often Taken",
                    cellType: "text",
                    isRequired: true,
                  },
                ],
                choices: [1, 2, 3, 4, 5],
                rowCount: 1,
                addRowText: "Add medication",
              },
            ],
            title: "Medication recently used",
          },
          {
            type: "radiogroup",
            name: "aspirinContainingProducts",
            title: "Have you taken aspirin-containing products in the last two weeks?",
            isRequired: true,
            choices: ["No", "Yes"],
            colCount: 0,
          },
          {
            type: "radiogroup",
            name: "steroidDrugs",
            title: "Have you taken steroid or cortisone-type drugs within the last year?",
            isRequired: true,
            choices: ["No", "Yes"],
            colCount: 0,
          },
        ],
        title: "Medications",
      },
      {
        name: "allergies",
        elements: [
          {
            type: "panel",
            name: "allergiesMedication",
            elements: [
              {
                type: "radiogroup",
                name: "hasAllergiesMedication",
                title:
                  "Have you had hives, skin rash, breathing problems, or other allergic reactions to medications?",
                isRequired: true,
                choices: ["No", "Yes"],
                colCount: 0,
              },
              {
                type: "matrixdynamic",
                name: "allergiesMedicationList",
                visible: false,
                visibleIf: "{hasAllergiesMedication} = 'Yes'",
                title: "Medication list",
                columns: [
                  {
                    name: "name",
                    title: "Name of Medication",
                    cellType: "text",
                    isRequired: true,
                  },
                  {
                    name: "description",
                    title: "Describe Allergic Reaction",
                    cellType: "comment",
                    isRequired: true,
                  },
                ],
                choices: [1, 2, 3, 4, 5],
                rowCount: 1,
                addRowText: "Add medication",
              },
            ],
            title: "Allergies to Medication",
          },
          {
            type: "panel",
            name: "unpleasentToMedication",
            elements: [
              {
                type: "radiogroup",
                name: "hasUnpleasantMedication",
                title:
                  "Are there medications, other than those you are allergic to, that you would prefer not to take due to prior unpleasant side effects?",
                isRequired: true,
                choices: ["No", "Yes"],
                colCount: 0,
              },
              {
                type: "matrixdynamic",
                name: "unpleasantMedicationList",
                visible: false,
                visibleIf: "{hasUnpleasantMedication} = 'Yes'",
                title: "Medication list",
                columns: [
                  {
                    name: "name",
                    title: "Name of Medication",
                    cellType: "text",
                    isRequired: true,
                  },
                  {
                    name: "description",
                    title: "Describe the Reaction",
                    cellType: "text",
                    isRequired: true,
                  },
                ],
                choices: [1, 2, 3, 4, 5],
                rowCount: 1,
                addRowText: "Add medication",
              },
              {
                type: "panel",
                name: "allergiesReaction",
                elements: [
                  {
                    type: "radiogroup",
                    name: "allergicReactionXRay",
                    title: "Iodine or X-ray contrast dye",
                    isRequired: true,
                    choices: ["No", "Yes"],
                    colCount: 0,
                  },
                  {
                    type: "radiogroup",
                    name: "allergicReactionBee",
                    startWithNewLine: false,
                    title: "Bee or wasp stings",
                    isRequired: true,
                    choices: ["No", "Yes"],
                    colCount: 0,
                  },
                  {
                    type: "radiogroup",
                    name: "allergicReactionLatext",
                    title: "Latex or Rubber",
                    isRequired: true,
                    choices: ["No", "Yes"],
                    colCount: 0,
                  },
                  {
                    type: "radiogroup",
                    name: "allergicReactionTape",
                    startWithNewLine: false,
                    title: "Adhesive tape",
                    isRequired: true,
                    choices: ["No", "Yes"],
                    colCount: 0,
                  },
                ],
                questionTitleLocation: "left",
                title: "Have you had an allergic reaction to:",
              },
            ],
            title: "Unpleasant Side Effects to Medications",
          },
          {
            type: "panel",
            name: "foodAllergiesInfo",
            elements: [
              {
                type: "radiogroup",
                name: "hasFoodAllergies",
                title: "Do you have any food allergies?",
                isRequired: true,
                choices: ["No", "Yes"],
                colCount: 0,
              },
              {
                type: "comment",
                name: "foodAllergiesDescription",
                visible: false,
                visibleIf: "{hasFoodAllergies} = 'Yes'",
                title: "Describe",
              },
            ],
            questionTitleLocation: "left",
            title: "Food Allergies",
          },
        ],
        title: "Allergies",
      },
      {
        name: "systemsReview",
        elements: [
          {
            type: "matrix",
            name: "sympthoms",
            title:
              "Indicate whether you have experienced the following symptoms during recent months, unless otherwise specified",
            isRequired: true,
            columns: ["No", "Yes"],
            rows: [
              "Skin rash, sore, excessive bruising or change of a mole?",
              "Excessive thirst or urination?",
              "Change in sexual drive or performance?",
              "Significant headaches, seizures, slurred speech or difficulty moving an arm or leg?",
              "Eye problems such as double or blurred vision, cataracts or glaucoma?",
              "Diminished hearing, dizziness, hoarseness or sinus problems?",
              {
                value: "dentures",
                text: "Do you wear dentures?",
              },
              "Bothered with cough, shortness of breath, wheezing or asthma?",
              "Coughing up sputum or blood?",
              "Exposed to anyone with tuberculosis?",
              "“Blacked out” or lost consciousness?",
              "Chest pain or pressure, rapid or irregular heart beats, or known difficulty with a heart valve?",
              "Awakening at night with shortness of breath?",
              "Abnormal swelling in the legs or feet?",
              "Pain in the calves of your legs when you walk?",
              "Difficulty with swallowing, heartburn, nausea, vomiting or stomach trouble?",
              "Significant problems with constipation, diarrhea, blood/changes in bowel movements?",
              {
                value: "colon_rectum_x-ray",
                text: "Have you had a colon or rectum x-ray or instrument examination (proctoscopy, sigmoidoscopy, colonoscopy)?",
              },
              {
                value: "endoscopy",
                text: "Have you had an upper endoscopy to evaluate the stomach for varices?",
              },
              "Have you had any treatment for varices? (sclerotherapy, banding)",
              "Difficulty starting your urinary stream, completely emptying your bladder or leaking urine from your bladder?",
              "Burning or pain when urinating?",
              "Pain, stiffness or swelling in your back, joints or muscles?",
              "Fever within the last month?",
              "Enlarged glands (lymph nodes)?",
              "Feel you are at risk for HIV or AIDS?",
              "Immunized for influenza, tetanus/diphtheria and/or pneumonia within the last year?",
              "Experiencing an unusually stressful situation?",
              "Weight gain or loss of more than 10 pounds during the last 6 months?",
              "Problems falling asleep, staying asleep, sleep apnea or disruptive snoring?",
              "Have you ever felt a need to cut down on your alcohol consumption?",
              "Do relatives/friends worry or complain about your alcohol consumption?",
              "Have you been physically, sexually, or emotionally abused?",
            ],
            isAllRowRequired: true,
          },
          {
            type: "radiogroup",
            name: "howWearDentures",
            visible: false,
            visibleIf: "{sympthoms.dentures}  = 'Yes'",
            title: "How do you wear dentures?",
            isRequired: true,
            choices: [" Ful", "Upper", "Lower", "Partial"],
            colCount: 0,
          },
          {
            type: "multipletext",
            name: "colonRectumDate",
            visible: false,
            visibleIf: "{sympthoms.colon_rectum_x-ray} = 'Yes'",
            title:
              "Whe did you do a colon or rectum x-ray or instrument examination (proctoscopy, sigmoidoscopy, colonoscopy)?  Approximate date:",
            isRequired: true,
            items: [
              {
                name: "mo",
                title: "Month",
              },
              {
                name: "year",
                title: "Year",
              },
            ],
            colCount: 2,
          },
          {
            type: "multipletext",
            name: "endoscopyDate",
            visible: false,
            visibleIf: "{sympthoms.endoscopy} = 'Yes'",
            title:
              "Whe did you have an upper endoscopy to evaluate the stomach for varices?  Approximate date:",
            isRequired: true,
            items: [
              {
                name: "mo",
                title: "Month",
              },
              {
                name: "year",
                title: "Year",
              },
            ],
            colCount: 2,
          },
          {
            type: "panel",
            name: "questionsToFemale",
            elements: [
              {
                type: "radiogroup",
                name: "abnormalPapSmear",
                title: "Have you ever had an abnormal Pap smear?",
                isRequired: true,
                choices: ["Unknown", "No", "Yes"],
                colCount: 0,
              },
              {
                type: "radiogroup",
                name: "menopause",
                title: "Have you experienced menopause or had a hysterectomy?",
                isRequired: true,
                choices: ["No", "Yes"],
                colCount: 0,
              },
              {
                type: "panel",
                name: "panelNoMenopause",
                elements: [
                  {
                    type: "radiogroup",
                    name: "concernedAboutMenstrualPeriods",
                    title: "Are you concerned about your menstrual periods?",
                    isRequired: true,
                    choices: ["No", "Yes"],
                    colCount: 0,
                  },
                  {
                    type: "radiogroup",
                    name: "pregnant",
                    title: "Might you be pregnant at this time?",
                    isRequired: true,
                    choices: ["No", "Yes"],
                    colCount: 0,
                  },
                  {
                    type: "text",
                    name: "lastMenstrualPeriod",
                    title: "Date of onset of your last menstrual period:",
                    inputType: "date",
                  },
                  {
                    type: "panel",
                    name: "panelPapSmearExam",
                    elements: [
                      {
                        type: "radiogroup",
                        name: "papSmearExam",
                        title: "Have you ever done Pap smear and pelvic exam:",
                        isRequired: true,
                        choices: ["No", "Yes"],
                        colCount: 0,
                      },
                      {
                        type: "multipletext",
                        name: "papSmearExamDate",
                        visible: false,
                        visibleIf: "{papSmearExam} = 'Yes'",
                        title: "Approximate date of your last Pap smear and pelvic exam:",
                        isRequired: true,
                        items: [
                          {
                            name: "mo",
                            title: "Month",
                          },
                          {
                            name: "year",
                            title: "Year",
                          },
                        ],
                        colCount: 2,
                      },
                      {
                        type: "radiogroup",
                        name: "mammogram",
                        title: "Have you ever done mammogram:",
                        isRequired: true,
                        choices: ["No", "Yes"],
                        colCount: 0,
                      },
                      {
                        type: "multipletext",
                        name: "mammogramDate",
                        visible: false,
                        visibleIf: "{mammogram} = 'Yes'",
                        title: "Approximate date of your last mammogram:",
                        isRequired: true,
                        items: [
                          {
                            name: "mo",
                            title: "Month",
                          },
                          {
                            name: "year",
                            title: "Year",
                          },
                        ],
                        colCount: 2,
                      },
                    ],
                  },
                  {
                    type: "panel",
                    name: "panelPregnancies",
                    elements: [
                      {
                        type: "text",
                        name: "pregnancies",
                        title: "Pregnancies:",
                        isRequired: true,
                        inputType: "number",
                      },
                      {
                        type: "text",
                        name: "liveBirths",
                        startWithNewLine: false,
                        title: "Live Births:",
                        isRequired: true,
                        inputType: "number",
                      },
                      {
                        type: "text",
                        name: "abortions",
                        startWithNewLine: false,
                        title: "Miscarriages/abortions:",
                        isRequired: true,
                        inputType: "number",
                      },
                    ],
                    title: "Number of:",
                  },
                ],
                visible: false,
                visibleIf: "{menopause} = 'No'",
              },
            ],
            visible: false,
            visibleIf: "{sex} = 'female'",
            title: "For Female Patients",
          },
        ],
        questionTitleLocation: "top",
        title: "System Review",
      },
      {
        name: "selfCare",
        elements: [
          {
            type: "matrix",
            name: "performingActivities",
            title: "Do you have difficulty performing these activities by YOURSELF?",
            columns: ["No", "Yes"],
            rows: ["Eating", "Bathing", "Dressing", "Walking", "Using Toilet", "Housekeeping"],
            isAllRowRequired: true,
          },
          {
            type: "radiogroup",
            name: "specialDietary",
            title: "Do you have any special dietary needs?",
            isRequired: true,
            choices: ["No", "Yes"],
            colCount: 0,
          },
          {
            type: "comment",
            name: "specialDietaryDescription",
            visibleIf: "{specialDietary} = 'Yes'",
            title: "Please describe your special dietery:",
            isRequired: true,
          },
          {
            type: "radiogroup",
            name: "livingArrangement",
            title: "What is your current living arrangement?",
            isRequired: true,
            choices: ["House", "Apartment", "Nursing Home"],
            hasOther: true,
            colCount: 0,
          },
          {
            type: "radiogroup",
            name: "liveWithWhom",
            title: "Do you live?",
            isRequired: true,
            choices: ["Alone", "With Spouse/Family", "With others"],
            colCount: 0,
          },
          {
            type: "comment",
            name: "liveWithWhomDescription",
            visibleIf: "{liveWithWhom} = 'With others'",
            title: "Please describe with whom do you live:",
            isRequired: true,
          },
          {
            type: "comment",
            name: "peopleProvidedAssistence",
            title:
              "List family or friends able to provide assistance with your homecare needs if you would ever require such assistance:",
            titleLocation: "top",
          },
        ],
        questionTitleLocation: "top",
        title: "Self-Care/Home Environment Assessment",
      },
      {
        name: "educationalNeeds",
        elements: [
          {
            type: "checkbox",
            name: "interestedInTopics",
            title: "Are you interested in more information about a specific topic(s)?",
            choices: [
              "How to stop smoking",
              "Exercise",
              "Stress",
              "Safe sexual practices",
              "Safety (seat belts, smoke detectors, firearms)",
              "Nutrition",
              "Weight control",
              "Violent & abusive behavior",
              "Living wills",
              "Diabetes",
              "Cancer screening   ",
            ],
            hasOther: true,
            colCount: 3,
          },
        ],
        title: "Educational Needs",
      },
    ],
    showQuestionNumbers: "off",
    showProgressBar: "top",
    formSettings: {
      requiredLogin: false,
      multipleSubmission: true,
      showCaptcha: false,
      privacySettings: "public",
      locationSettings: false,
      limitedUser: [],
      country: null,
      city: null,
      publishSettings: true,
      unPublishSettings: false,
      publishDate: null,
      publishTime: null,
      publishRedirectUrl: "",
      publishRedirectBtnText: "",
      publishDescription: "",
      unPublishDate: null,
      unPublishTime: null,
      unPublishRedirectUrl: "",
      unPublishRedirectBtnText: "",
      unPublishDescription: "",
    },
  },
  {
    id: uuidv4(),
    title: "Income Survey",
    description: "Customer and his/her partner income survey",
    logoPosition: "right",
    pages: [
      {
        name: "page0",
        elements: [
          {
            type: "panel",
            name: "panel1",
            elements: [
              {
                type: "html",
                name: "income_intro",
                html: "<article class='intro'>    <h1 class='intro__heading intro__heading--income title'>                     Income              </h1>    <div class='intro__body wysiwyg'>       <p>In this section, you will be asked about your current employment and any other way you and your partner currently receive income.</p>       <p>It will be handy to have the following in front of you:</p>       <ul>        \t<li>        \t\tPayslip (for employment details)        \t</li>        \t<li>        \t\t<p>A current Centrelink Schedule for any account based pension from super, annuities, or other income stream products that you own</p>        \t\t<p>        \t\t\tIf you don't have a current one you can get these schedules by contacting your income stream provider        \t\t</p>        \t</li>        \t<li>        \t\tLatest statement from any payments (from Centrelink or other authority)        \t</li>       </ul>         </div> </article>",
              },
            ],
          },
        ],
      },
      {
        name: "page1",
        elements: [
          {
            type: "panel",
            name: "panel13",
            elements: [
              {
                type: "radiogroup",
                name: "maritalstatus_c",
                title: " ",
                choices: [
                  "Married",
                  "In a registered relationship",
                  "Living with my partner",
                  "Widowed",
                  "Single",
                ],
              },
            ],
            title: "What is your marital status?",
          },
        ],
      },
      {
        name: "page2",
        elements: [
          {
            type: "panel",
            name: "panel5",
            elements: [
              {
                type: "panel",
                name: "panel2",
                elements: [
                  {
                    type: "radiogroup",
                    name: "member_receives_income_from_employment",
                    title: " ",
                    isRequired: true,
                    choices: [
                      {
                        value: "1",
                        text: "Yes",
                      },
                      {
                        value: "0",
                        text: "No",
                      },
                    ],
                    colCount: 2,
                  },
                  {
                    type: "checkbox",
                    name: "member_type_of_employment",
                    visible: false,
                    visibleIf: "{member_receives_income_from_employment} =1",
                    title: "  ",
                    isRequired: true,
                    choices: ["Self employment", "All other types of employment"],
                  },
                ],
                title: "You",
              },
              {
                type: "panel",
                name: "panel1",
                elements: [
                  {
                    type: "radiogroup",
                    name: "partner_receives_income_from_employment",
                    title: " ",
                    isRequired: true,
                    choices: [
                      {
                        value: "1",
                        text: "Yes",
                      },
                      {
                        value: "0",
                        text: "No",
                      },
                    ],
                    colCount: 2,
                  },
                  {
                    type: "checkbox",
                    name: "partner_type_of_employment",
                    visible: false,
                    visibleIf: "{partner_receives_income_from_employment} =1",
                    title: " ",
                    isRequired: true,
                    choices: ["Self employment", "All other types of employment"],
                  },
                ],
                visible: false,
                visibleIf:
                  "{maritalstatus_c} = 'Married' or {maritalstatus_c} = 'In a registered relationship' or {maritalstatus_c} = 'Living with my partner'",
                title: "Your Partner",
                startWithNewLine: false,
              },
            ],
            title: "Do you and/or your partner currently receive income from employment?",
          },
        ],
      },
      {
        name: "page3.1",
        elements: [
          {
            type: "panel",
            name: "panel6",
            elements: [
              {
                type: "panel",
                name: "panel2",
                elements: [
                  {
                    type: "paneldynamic",
                    name: "member_arrray_employer_names",
                    title: "Please enter all your employers",
                    valueName: "member_arrray_employer",
                    templateElements: [
                      {
                        type: "text",
                        name: "member_employer_name",
                        title: "Name of employer",
                        valueName: "name",
                      },
                    ],
                    panelCount: 1,
                    minPanelCount: 1,
                    panelAddText: "Add another employer",
                  },
                ],
                visible: false,
                visibleIf: "{member_type_of_employment} contains 'All other types of employment'",
                title: "You",
              },
              {
                type: "panel",
                name: "panel8",
                elements: [
                  {
                    type: "paneldynamic",
                    name: "partner_arrray_employer_names",
                    title: "Please enter all your partner employers",
                    valueName: "partner_arrray_employer",
                    templateElements: [
                      {
                        type: "text",
                        name: "partner_employer_name",
                        title: "Name of employer",
                        valueName: "name",
                      },
                    ],
                    panelCount: 1,
                    minPanelCount: 1,
                    panelAddText: "Add another employer",
                  },
                ],
                visible: false,
                visibleIf: "{partner_type_of_employment} contains 'All other types of employment'",
                title: "Your Partner",
                startWithNewLine: false,
              },
            ],
            title: "Who are you employed by?",
          },
        ],
        visible: false,
        visibleIf:
          "{member_type_of_employment} contains 'All other types of employment' or {partner_type_of_employment} contains 'All other types of employment'",
      },
      {
        name: "page3.2",
        elements: [
          {
            type: "panel",
            name: "panel16",
            elements: [
              {
                type: "panel",
                name: "panel17",
                elements: [
                  {
                    type: "paneldynamic",
                    name: "member_arrray_employer_info",
                    title: "Your employers",
                    valueName: "member_arrray_employer",
                    templateElements: [
                      {
                        type: "panel",
                        name: "panel_member_employer_address",
                        elements: [
                          {
                            type: "text",
                            name: "member_employer_address",
                            title: "Address",
                            valueName: "address",
                          },
                          {
                            type: "text",
                            name: "member_employer_phone",
                            title: "Phone number:",
                            valueName: "phone",
                          },
                          {
                            type: "text",
                            name: "member_employer_abn",
                            title: "ABN",
                            valueName: "abn",
                          },
                        ],
                        title: "Address",
                      },
                      {
                        type: "panel",
                        name: "panel_member_employer_role",
                        elements: [
                          {
                            type: "radiogroup",
                            name: "member_employer_role",
                            title: "Your role",
                            valueName: "role",
                            choices: ["Full time", "Part time", "Casual", "Seasonal"],
                          },
                        ],
                        title: "What is your role?",
                      },
                      {
                        type: "panel",
                        name: "panel_member_employer_hours_work",
                        elements: [
                          {
                            type: "text",
                            name: "member_employer_hours_worked",
                            title: "Hours:",
                            valueName: "hours_worked",
                            inputType: "number",
                          },
                          {
                            type: "dropdown",
                            name: "member_employer_hours_worked_frequency",
                            startWithNewLine: false,
                            title: "Worked Frequency:",
                            valueName: "hours_worked_frequency",
                            defaultValue: "Year",
                            choices: ["Day", "Week", "Fortnight", "Month", "Year"],
                          },
                        ],
                        title: "What hours do you work?",
                      },
                      {
                        type: "panel",
                        name: "panel_member_employer_income",
                        elements: [
                          {
                            type: "text",
                            name: "member_employer_income",
                            title: "Income:",
                            valueName: "income",
                            inputType: "number",
                          },
                          {
                            type: "dropdown",
                            name: "member_employer_income_frequency",
                            startWithNewLine: false,
                            title: "Income Frequency",
                            valueName: "income_frequency",
                            defaultValue: "Year",
                            choices: ["Day", "Week", "Fortnight", "Month", "Year"],
                          },
                        ],
                        title: "What income do you receive?",
                      },
                    ],
                    templateTitle: "Employer name: {panel.name}",
                    allowAddPanel: false,
                    allowRemovePanel: false,
                    panelCount: 1,
                    renderMode: "progressTop",
                  },
                ],
                visibleIf: "{member_type_of_employment} contains 'All other types of employment'",
                title: "You",
              },
              {
                type: "panel",
                name: "panel18",
                elements: [
                  {
                    type: "paneldynamic",
                    name: "partner_arrray_employer_info",
                    title: "Your partner employers",
                    valueName: "partner_arrray_employer",
                    templateElements: [
                      {
                        type: "panel",
                        name: "panel_partner_employer_address",
                        elements: [
                          {
                            type: "text",
                            name: "partner_employer_address",
                            title: "Address:",
                            valueName: "address",
                          },
                          {
                            type: "text",
                            name: "partner_employer_phone",
                            title: "Phone number",
                            valueName: "phone",
                          },
                          {
                            type: "text",
                            name: "partner_employer_abn",
                            title: "ABN",
                            valueName: "abn",
                          },
                        ],
                        title: "Address",
                      },
                      {
                        type: "panel",
                        name: "panel_partner_employer_role",
                        elements: [
                          {
                            type: "radiogroup",
                            name: "partner_employer_role",
                            title: "Your role",
                            valueName: "role",
                            choices: ["Full time", "Part time", "Casual", "Seasonal"],
                          },
                        ],
                        title: "What is your role?",
                      },
                      {
                        type: "panel",
                        name: "panel_partner_employer_hours_work",
                        elements: [
                          {
                            type: "text",
                            name: "partner_employer_hours_worked",
                            title: "Hours",
                            valueName: "hours_worked",
                            inputType: "number",
                          },
                          {
                            type: "dropdown",
                            name: "partner_employer_hours_worked_frequency",
                            startWithNewLine: false,
                            title: "Worked Frequency:",
                            valueName: "hours_worked_frequency",
                            defaultValue: "Year",
                            choices: ["Day", "Week", "Fortnight", "Month", "Year"],
                          },
                        ],
                        title: "What hours do you work?",
                      },
                      {
                        type: "panel",
                        name: "panel_partner_employer_income",
                        elements: [
                          {
                            type: "text",
                            name: "partner_employer_income",
                            title: "Income:",
                            valueName: "income",
                            inputType: "number",
                          },
                          {
                            type: "dropdown",
                            name: "partner_employer_income_frequency",
                            startWithNewLine: false,
                            title: "Income frequency:",
                            valueName: "income_frequency",
                            defaultValue: "Year",
                            choices: ["Day", "Week", "Fortnight", "Month", "Year"],
                          },
                        ],
                        title: "What income do you receive?",
                      },
                    ],
                    templateTitle: "Employer name: {panel.name}",
                    allowAddPanel: false,
                    allowRemovePanel: false,
                    panelCount: 1,
                    renderMode: "progressTop",
                  },
                ],
                visibleIf: "{partner_type_of_employment} contains 'All other types of employment'",
                title: "You partner",
                startWithNewLine: false,
              },
            ],
            title: "Tells us about your employer(s)",
          },
        ],
        visibleIf:
          "{member_type_of_employment} contains 'All other types of employment' or {partner_type_of_employment} contains 'All other types of employment'",
      },
      {
        name: "page4",
        elements: [
          {
            type: "panel",
            name: "panel9",
            elements: [
              {
                type: "panel",
                name: "panel2",
                elements: [
                  {
                    type: "radiogroup",
                    name: "member_receive_fringe_benefits",
                    title: " ",
                    isRequired: true,
                    choices: [
                      {
                        value: "1",
                        text: "Yes",
                      },
                      {
                        value: "0",
                        text: "No",
                      },
                    ],
                    colCount: 2,
                  },
                  {
                    type: "panel",
                    name: "panel11",
                    elements: [
                      {
                        type: "text",
                        name: "member_fringe_benefits_type",
                      },
                      {
                        type: "text",
                        name: "member_fringe_benefits_value",
                      },
                      {
                        type: "radiogroup",
                        name: "member_fringe_benefits_grossing",
                        choices: ["Grossed up", "Not grossed up"],
                      },
                    ],
                    visible: false,
                    visibleIf: "{member_receive_fringe_benefits} = 1",
                  },
                ],
                visible: false,
                visibleIf: "{member_type_of_employment} contains 'All other types of employment'",
                title: "You",
              },
              {
                type: "panel",
                name: "panel1",
                elements: [
                  {
                    type: "radiogroup",
                    name: "partner_receive_fringe_benefits",
                    title: " ",
                    isRequired: true,
                    choices: [
                      {
                        value: "1",
                        text: "Yes",
                      },
                      {
                        value: "0",
                        text: "No",
                      },
                    ],
                    colCount: 2,
                  },
                  {
                    type: "panel",
                    name: "panel12",
                    elements: [
                      {
                        type: "text",
                        name: "partner_fringe_benefits_type",
                      },
                      {
                        type: "text",
                        name: "partner_fringe_benefits_value",
                      },
                      {
                        type: "radiogroup",
                        name: "partner_fringe_benefits_grossing",
                        choices: ["Grossed up", "Not grossed up"],
                      },
                    ],
                    visible: false,
                    visibleIf: "{partner_receive_fringe_benefits} = 1",
                  },
                ],
                visible: false,
                visibleIf: "{partner_type_of_employment} contains 'All other types of employment'",
                title: "Your Partner",
                startWithNewLine: false,
              },
            ],
            title: "Do any of your employers provide you with fringe benefits?",
          },
        ],
        visible: false,
        visibleIf:
          "{member_type_of_employment} contains 'All other types of employment' or {partner_type_of_employment} contains 'All other types of employment'",
      },
      {
        name: "page5",
        elements: [
          {
            type: "panel",
            name: "panel10",
            elements: [
              {
                type: "panel",
                name: "panel2",
                elements: [
                  {
                    type: "radiogroup",
                    name: "member_seasonal_intermittent_or_contract_work",
                    title: " ",
                    isRequired: true,
                    choices: [
                      {
                        value: "1",
                        text: "Yes",
                      },
                      {
                        value: "0",
                        text: "No",
                      },
                    ],
                    colCount: 2,
                  },
                ],
                visible: false,
                visibleIf: "{member_receives_income_from_employment} = 1",
                title: "You",
              },
              {
                type: "panel",
                name: "panel1",
                elements: [
                  {
                    type: "radiogroup",
                    name: "partner_seasonal_intermittent_or_contract_work",
                    title: " ",
                    isRequired: true,
                    choices: [
                      {
                        value: "1",
                        text: "Yes",
                      },
                      {
                        value: "0",
                        text: "No",
                      },
                    ],
                    colCount: 2,
                  },
                ],
                visible: false,
                visibleIf: "{partner_receives_income_from_employment} =1 ",
                title: "Your Partner",
                startWithNewLine: false,
              },
            ],
            title:
              "In the last 6 months, have you done any seasonal, intermittent or contract work?",
          },
        ],
        visible: false,
        visibleIf:
          "{member_receives_income_from_employment} = 1 or {partner_receives_income_from_employment} =1 ",
      },
    ],
    showQuestionNumbers: "off",
    storeOthersAsComment: false,
    pagePrevText: "Previous",
    pageNextText: "Continue",
    completeText: "Finish",
    requiredText: "",
    formSettings: {
      requiredLogin: false,
      multipleSubmission: true,
      showCaptcha: false,
      privacySettings: "public",
      locationSettings: false,
      limitedUser: [],
      country: null,
      city: null,
      publishSettings: true,
      unPublishSettings: false,
      publishDate: null,
      publishTime: null,
      publishRedirectUrl: "",
      publishRedirectBtnText: "",
      publishDescription: "",
      unPublishDate: null,
      unPublishTime: null,
      unPublishRedirectUrl: "",
      unPublishRedirectBtnText: "",
      unPublishDescription: "",
    },
  },
];
export let surveyList2 = [
  {
    id: uuidv4(),
    title: "NPS Survey",
    description: "NPS survey with follow-up questions",
    logoPosition: "right",
    completedHtml:
      "<h3>Thank you for your feedback.</h3><h5>Your thoughts and ideas will help us to create a great product!</h5>",
    completedHtmlOnCondition: [
      {
        expression: "{nps_score} > 8",
        html: "<h3>Thank you for your feedback.</h3><h5>We glad that you love our product. Your ideas and suggestions will help us to make our product even better!</h5>",
      },
      {
        expression: "{nps_score} < 7",
        html: "<h3>Thank you for your feedback.</h3><h5> We are glad that you share with us your ideas.We highly value all suggestions from our customers. We do our best to improve the product and reach your expectation.</h5><br />",
      },
    ],
    pages: [
      {
        name: "page1",
        elements: [
          {
            type: "rating",
            name: "nps_score",
            title:
              "On a scale of zero to ten, how likely are you to recommend our product to a friend or colleague?",
            isRequired: true,
            rateMin: 0,
            rateMax: 10,
            minRateDescription: "(Most unlikely)",
            maxRateDescription: "(Most likely)",
          },
          {
            type: "checkbox",
            name: "promoter_features",
            visible: false,
            visibleIf: "{nps_score} >= 9",
            title: "Which features do you value the most?",
            isRequired: true,
            validators: [
              {
                type: "answercount",
                text: "Please select two features maximum.",
                maxCount: 2,
              },
            ],
            choices: ["Performance", "Stability", "User Interface", "Complete Functionality"],
            hasOther: true,
            otherText: "Other feature:",
            colCount: 2,
          },
          {
            type: "comment",
            name: "passive_experience",
            visible: false,
            visibleIf: "{nps_score} > 6  and {nps_score} < 9",
            title: "What do you like about our product?",
          },
          {
            type: "comment",
            name: "disappointed_experience",
            visible: false,
            visibleIf: "{nps_score} notempty",
            title: "What do you miss or find disappointing in your experience with our products?",
          },
        ],
      },
    ],
    showQuestionNumbers: "off",
    formSettings: {
      requiredLogin: true,
      multipleSubmission: true,
      showCaptcha: false,
      privacySettings: "public",
      locationSettings: true,
      limitedUser: [],
      country: null,
      city: null,
      publishSettings: true,
      unPublishSettings: false,
      publishDate: null,
      publishTime: null,
      publishRedirectUrl: "",
      publishRedirectBtnText: "Lorem Ipsum",
      publishDescription: "",
      unPublishDate: null,
      unPublishTime: null,
      unPublishRedirectUrl: "",
      unPublishRedirectBtnText: "",
      unPublishDescription: "",
    },
  },
  {
    id: uuidv4(),
    title: "COVID-19",
    description: "Minimum data reporting form – for suspected and probable cases of COVID-19",
    logoPosition: "right",
    pages: [
      {
        name: "page1",
        elements: [
          {
            type: "image",
            name: "first_page_image",
            width: "400px",
            minWidth: "400px",
            maxWidth: "400px",
            imageLink: "https://surveyjs.io/Content/Images/examples/covid/01.png",
            imageFit: "none",
            imageHeight: 726,
            imageWidth: 400,
          },
          {
            type: "panel",
            name: "first_page_container_panel",
            elements: [
              {
                type: "text",
                name: "unique_case_id_textbox",
                startWithNewLine: false,
                title: "Unique Case ID / Cluster Number (if applicable):",
                hideNumber: true,
                inputType: "number",
              },
              {
                type: "panel",
                name: "current_status_panel",
                elements: [
                  {
                    type: "boolean",
                    name: "current_status",
                    titleLocation: "hidden",
                    hideNumber: true,
                    labelTrue: "Alive",
                    labelFalse: "Dead",
                  },
                ],
                title: "Current Status",
                showNumber: true,
              },
              {
                type: "panel",
                name: "data_collector_information",
                elements: [
                  {
                    type: "text",
                    name: "name_of_data_collector",
                    title: "Name of data collector",
                  },
                  {
                    type: "text",
                    name: "data_collector_institution",
                    title: "Data collector Institution",
                  },
                  {
                    type: "text",
                    name: "data_collector_telephone_number",
                    title: "Data collector telephone number",
                    inputType: "tel",
                  },
                  {
                    type: "text",
                    name: "email",
                    title: "Email",
                    inputType: "email",
                  },
                  {
                    type: "text",
                    name: "form_completion_date",
                    title: "Form completion date",
                    inputType: "date",
                  },
                ],
                title: "Data Collector Information",
                showNumber: true,
                showQuestionNumbers: "off",
              },
            ],
            startWithNewLine: false,
          },
        ],
        navigationTitle: "Collector",
        navigationDescription: "Collector's info",
      },
      {
        name: "page2",
        elements: [
          {
            type: "boolean",
            name: "is_the_person_providing_the_information_is_the_patient",
            title: "Is the person providing the information is the patient?",
            hideNumber: true,
            isRequired: true,
            labelTrue: "Yes",
            labelFalse: "No",
          },
          {
            type: "panel",
            name: "case_identifier_information",
            elements: [
              {
                type: "text",
                name: "given_names",
                title: "Given name(s)",
              },
              {
                type: "text",
                name: "family_name",
                startWithNewLine: false,
                title: "Family name",
              },
              {
                type: "radiogroup",
                name: "case_identifier_information_sex",
                title: "Sex",
                choices: [
                  {
                    value: "item1",
                    text: "Male",
                  },
                  {
                    value: "item2",
                    text: "Female",
                  },
                  {
                    value: "item3",
                    text: "Not known",
                  },
                ],
                colCount: 3,
              },
              {
                type: "panel",
                name: "case_identifier_information_patient_date_of_birth_panel",
                elements: [
                  {
                    type: "text",
                    name: "case_identifier_information_patient_date_of_birth_date",
                    visibleIf: "{case_identifier_information_patient_date_of_birth_checkbox} empty",
                    titleLocation: "hidden",
                    inputType: "date",
                  },
                  {
                    type: "checkbox",
                    name: "case_identifier_information_patient_date_of_birth_checkbox",
                    startWithNewLine: false,
                    titleLocation: "hidden",
                    choices: ["Unknown"],
                  },
                ],
                title: "Date of Birth",
              },
              {
                type: "text",
                name: "case_identifier_information_patient_telephone_mobile_number",
                title: "Telephone (mobile) number",
                inputType: "tel",
              },
              {
                type: "panel",
                name: "case_identifier_information_patient_age",
                elements: [
                  {
                    type: "text",
                    name: "case_identifier_information_patient_age_years",
                    visibleIf: "{case_identifier_information_patient_age_checkbox} empty",
                    title: "Years:",
                    inputType: "number",
                  },
                  {
                    type: "text",
                    name: "case_identifier_information_patient_age_months",
                    visibleIf: "{case_identifier_information_patient_age_checkbox} empty",
                    startWithNewLine: false,
                    title: "Months:",
                    inputType: "number",
                  },
                  {
                    type: "checkbox",
                    name: "case_identifier_information_patient_age_checkbox",
                    startWithNewLine: false,
                    titleLocation: "hidden",
                    choices: ["Unknown"],
                  },
                ],
                title: "Age (years, months) ",
              },
              {
                type: "text",
                name: "case_identifier_information_patient_email",
                title: "Email",
                inputType: "email",
              },
              {
                type: "text",
                name: "case_identifier_information_patient_address",
                startWithNewLine: false,
                title: "Address",
              },
              {
                type: "text",
                name: "case_identifier_information_patient_national_social_number",
                title: "National social number/ identifier (if applicable)",
              },
              {
                type: "dropdown",
                name: "case_identifier_information_patient_country_of_residence",
                title: "Country of residence",
                choices: ["item1", "item2", "item3"],
                choicesByUrl: {
                  url: "https://surveyjs.io/api/CountriesExample",
                  valueName: "name",
                },
              },
              {
                type: "radiogroup",
                name: "case_identifier_information_patient_case_status",
                startWithNewLine: false,
                title: "Case status",
                choices: [
                  {
                    value: "item1",
                    text: "Suspected",
                  },
                  {
                    value: "item2",
                    text: "Probable",
                  },
                  {
                    value: "item3",
                    text: "Confirmed",
                  },
                ],
                colCount: 3,
              },
            ],
            visible: false,
            visibleIf: "{is_the_person_providing_the_information_is_the_patient} = true",
            title: "Case Identifier Information",
            showNumber: true,
            showQuestionNumbers: "off",
          },
          {
            type: "panel",
            name: "interview_respondent_information_if_the_persons_providing_the_information_is_not_the_patient",
            elements: [
              {
                type: "text",
                name: "first_name",
                title: "First name ",
              },
              {
                type: "text",
                name: "surname",
                startWithNewLine: false,
                title: "Surname",
              },
              {
                type: "radiogroup",
                name: "interview_respondent_information_sex",
                title: "Sex",
                choices: [
                  {
                    value: "item1",
                    text: "Male",
                  },
                  {
                    value: "item2",
                    text: "Female",
                  },
                  {
                    value: "item3",
                    text: "Not known",
                  },
                ],
                colCount: 3,
              },
              {
                type: "panel",
                name: "interview_respondent_information_patient_date_of_birth_panel",
                elements: [
                  {
                    type: "text",
                    name: "interview_respondent_information_patient_date_of_birth_date",
                    visibleIf:
                      "{interview_respondent_information_patient_date_of_birth_checkbox} empty",
                    titleLocation: "hidden",
                    inputType: "date",
                  },
                  {
                    type: "checkbox",
                    name: "interview_respondent_information_patient_date_of_birth_checkbox",
                    startWithNewLine: false,
                    titleLocation: "hidden",
                    choices: ["Unknown"],
                  },
                ],
                title: "Date of Birth",
              },
              {
                type: "text",
                name: "relationship_to_patient",
                title: "Relationship to patient",
              },
              {
                type: "text",
                name: "respondent_address",
                startWithNewLine: false,
                title: "Respondent address",
              },
              {
                type: "text",
                name: "interview_respondent_information_patient_telephone_mobile_number",
                startWithNewLine: false,
                title: "Telephone (mobile) number",
                inputType: "tel",
              },
            ],
            visible: false,
            visibleIf: "{is_the_person_providing_the_information_is_the_patient} = false",
            title: "Interview respondent information",
            showNumber: true,
            showQuestionNumbers: "off",
          },
        ],
        navigationTitle: "Person",
        navigationDescription: "Person's info",
      },
      {
        name: "page3",
        elements: [
          {
            type: "image",
            name: "third_page_image",
            width: "400px",
            minWidth: "400px",
            maxWidth: "400px",
            imageLink: "https://surveyjs.io/Content/Images/examples/covid/03.png",
            imageHeight: 690,
            imageWidth: 400,
          },
          {
            type: "panel",
            name: "patient_symptoms_from_disease_onset",
            elements: [
              {
                type: "panel",
                name: "date_of_first_symptom_onset",
                elements: [
                  {
                    type: "text",
                    name: "date_of_first_symptom_onset_date",
                    width: "314px",
                    minWidth: "314px",
                    maxWidth: "314px",
                    titleLocation: "hidden",
                    enableIf: "{date_of_first_symptom_onset_checkbox} empty",
                    inputType: "date",
                  },
                  {
                    type: "checkbox",
                    name: "date_of_first_symptom_onset_checkbox",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "No symptoms",
                        enableIf: "{date_of_first_symptom_onset_checkbox} <> ['item2']",
                      },
                      {
                        value: "item2",
                        text: "Unknown",
                        enableIf: "{date_of_first_symptom_onset_checkbox} <> ['item1']",
                      },
                    ],
                    colCount: 2,
                  },
                  {
                    type: "radiogroup",
                    name: "question1",
                    title: "Fever (≥38 °C) or history of fever ",
                    titleLocation: "top",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Unknown",
                      },
                    ],
                    colCount: 3,
                  },
                ],
                title: "Date of first symptom onset",
              },
              {
                type: "radiogroup",
                name: "sore_throat",
                title: "Sore throat",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
              {
                type: "radiogroup",
                name: "runny_nose",
                title: "Runny nose",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
              {
                type: "radiogroup",
                name: "cough",
                title: "Cough",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
              {
                type: "radiogroup",
                name: "shortness_of_Breath",
                title: "Shortness of Breath",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
              {
                type: "radiogroup",
                name: "vomiting",
                title: "Vomiting",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
              {
                type: "radiogroup",
                name: "nausea",
                title: "Nausea",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
              {
                type: "radiogroup",
                name: "diarrhea",
                title: "Diarrhea",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
            ],
            title: "Patient symptoms (from disease onset)",
            startWithNewLine: false,
            showNumber: true,
            showQuestionNumbers: "off",
          },
        ],
        navigationTitle: "Symptoms",
        navigationDescription: "Patient symptoms",
      },
      {
        name: "page4",
        elements: [
          {
            type: "image",
            name: "fouth_page_image",
            width: "400px",
            minWidth: "400px",
            maxWidth: "400px",
            imageLink: "https://surveyjs.io/Content/Images/examples/covid/04.png",
            imageHeight: 567,
            imageWidth: 400,
          },
          {
            type: "panel",
            name: "initial_sample_collection",
            elements: [
              {
                type: "text",
                name: "date_respiratory_sample_collected",
                title: "Date respiratory sample collected",
                inputType: "date",
              },
              {
                type: "radiogroup",
                name: "what_type_of_respiratory_sample_was_collected",
                title: "What type of respiratory sample was collected?",
                choices: [
                  {
                    value: "item1",
                    text: "Nasal swab",
                  },
                  {
                    value: "item2",
                    text: "Throat swab ",
                  },
                  {
                    value: "item3",
                    text: "Nasopharyngeal swab",
                  },
                ],
                hasOther: true,
                otherText: "Other, specify",
                colCount: 2,
              },
              {
                type: "panel",
                name: "has_baseline_serum_been_taken_panel",
                elements: [
                  {
                    type: "radiogroup",
                    name: "has_baseline_serum_been_taken_radio",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Unknown",
                      },
                    ],
                    colCount: 3,
                  },
                  {
                    type: "text",
                    name: "has_baseline_serum_been_taken_date",
                    visible: false,
                    visibleIf: "{has_baseline_serum_been_taken_radio} = 'item1'",
                    startWithNewLine: false,
                    title: "Date baseline serum taken",
                    inputType: "date",
                  },
                ],
                title: "Has baseline serum been taken?",
              },
              {
                type: "panel",
                name: "were_other_samples_collected_panel",
                elements: [
                  {
                    type: "radiogroup",
                    name: "were_other_samples_collected_radio",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Unknown",
                      },
                    ],
                    colCount: 3,
                  },
                  {
                    type: "text",
                    name: "were_other_samples_collected_textbox",
                    visible: false,
                    visibleIf: "{were_other_samples_collected_radio} = 'item1'",
                    startWithNewLine: false,
                    title: "Which samples:",
                  },
                  {
                    type: "text",
                    name: "were_other_samples_collected_date",
                    visible: false,
                    visibleIf: "{were_other_samples_collected_radio} = 'item1'",
                    startWithNewLine: false,
                    title: "Date taken",
                    inputType: "date",
                  },
                ],
                title: "Were other samples collected? ",
              },
            ],
            title: "Initial sample collection",
            startWithNewLine: false,
            showNumber: true,
            showQuestionNumbers: "off",
          },
        ],
        navigationTitle: "Sample",
        navigationDescription: "Initial sample",
      },
      {
        name: "page5",
        elements: [
          {
            type: "image",
            name: "fifth_image",
            width: "400px",
            minWidth: "400px",
            maxWidth: "400px",
            imageLink: "https://surveyjs.io/Content/Images/examples/covid/05.png",
            imageHeight: 713,
            imageWidth: 400,
          },
          {
            type: "panel",
            name: "clinical_course_complications_panel",
            elements: [
              {
                type: "panel",
                name: "hospitalization_required_panel",
                elements: [
                  {
                    type: "radiogroup",
                    name: "hospitalization_required_radio",
                    width: "49%",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Unknown",
                      },
                    ],
                    colCount: 3,
                  },
                  {
                    type: "text",
                    name: "hospitalization_required_hospital",
                    width: "313px",
                    minWidth: "313px",
                    maxWidth: "313px",
                    title: "Name of hospital",
                    enableIf: "{hospitalization_required_radio} = 'item1'",
                    readOnly: true,
                  },
                ],
                title: "Hospitalization required?",
              },
              {
                type: "radiogroup",
                name: "icu_Intensive_Care_Unit_admission_required",
                title: "ICU (Intensive Care Unit) admission required",
                titleLocation: "top",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
              {
                type: "radiogroup",
                name: "acute_respiratory_distress_syndrome_ards",
                title: "Acute Respiratory Distress Syndrome (ARDS)",
                titleLocation: "top",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
              {
                type: "panel",
                name: "pneumonia_by_chest_xray_panel",
                elements: [
                  {
                    type: "radiogroup",
                    name: "pneumonia_by_chest_xray_radio",
                    width: "49%",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Not applicable (no X-ray performed)",
                      },
                    ],
                    colCount: 3,
                  },
                  {
                    type: "text",
                    name: "pneumonia_by_chest_xray_date",
                    width: "313px",
                    minWidth: "313px",
                    maxWidth: "313px",
                    title: "Date",
                    enableIf: "{pneumonia_by_chest_xray_radio} = 'item1'",
                    readOnly: true,
                    inputType: "date",
                  },
                ],
                title: "Pneumonia by chest X-ray ",
              },
              {
                type: "panel",
                name: "other_severe_or_life_threatening_illness_suggestive_of_an_infection_panel",
                elements: [
                  {
                    type: "radiogroup",
                    name: "other_severe_or_life_threatening_illness_suggestive_of_an_radio",
                    width: "49%",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Unknown",
                      },
                    ],
                    colCount: 3,
                  },
                  {
                    type: "text",
                    name: "other_severe_or_life_threatening_illness_suggestive_of_an_specify",
                    width: "313px",
                    minWidth: "313px",
                    maxWidth: "313px",
                    title: "Specify:",
                    enableIf:
                      "{other_severe_or_life_threatening_illness_suggestive_of_an_radio} = 'item1'",
                    readOnly: true,
                  },
                ],
                title: "Other severe or life-threatening illness suggestive of an infection",
              },
              {
                type: "radiogroup",
                name: "mechanical_ventilation_required",
                title: "Mechanical ventilation required",
                titleLocation: "top",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
              {
                type: "radiogroup",
                name: "extracorporeal_membrane_oxygenation_emo",
                title: "Extracorporeal membrane oxygenation (EMO)",
                titleLocation: "top",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
            ],
            title: "Clinical Course: Complications",
            startWithNewLine: false,
            showNumber: true,
            showQuestionNumbers: "off",
          },
        ],
        navigationTitle: "Complications",
        navigationDescription: "Clinical Course",
      },
      {
        name: "page6",
        elements: [
          {
            type: "panel",
            name: "human_exposures_in_the_days_before_illness_onset",
            elements: [
              {
                type: "panel",
                name: "have_you_travelled_within_the_last_days_domestically_panel",
                elements: [
                  {
                    type: "radiogroup",
                    name: "have_you_travelled_within_the_last_days_domestically_radio",
                    width: "50%",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Unknown",
                      },
                    ],
                    colCount: 3,
                  },
                  {
                    type: "panel",
                    name: "have_you_travelled_within_the_last_days_domestically_date_panel",
                    elements: [
                      {
                        type: "text",
                        name: "have_you_travelled_within_the_last_days_domestically_date_from",
                        title: "from",
                        inputType: "date",
                      },
                      {
                        type: "text",
                        name: "have_you_travelled_within_the_last_days_domestically_date_to",
                        startWithNewLine: false,
                        title: "to",
                        inputType: "date",
                      },
                    ],
                    visible: false,
                    visibleIf:
                      "{have_you_travelled_within_the_last_days_domestically_radio} = 'item1'",
                    title: "Dates of travel",
                  },
                  {
                    type: "text",
                    name: "have_you_travelled_within_the_last_days_domestically_regions",
                    visible: false,
                    visibleIf:
                      "{have_you_travelled_within_the_last_days_domestically_radio} = 'item1'",
                    title: "Regions:",
                  },
                  {
                    type: "text",
                    name: "have_you_travelled_within_the_last_days_domestically_cities_visited",
                    visible: false,
                    visibleIf:
                      "{have_you_travelled_within_the_last_days_domestically_radio} = 'item1'",
                    startWithNewLine: false,
                    title: "Cities visited:",
                  },
                ],
                title: "Have you travelled within the last 14 days domestically?",
                showQuestionNumbers: "off",
              },
              {
                type: "panel",
                name: "have_you_travelled_within_the_last_days_internationall_panel",
                elements: [
                  {
                    type: "radiogroup",
                    name: "have_you_travelled_within_the_last_days_internationall_radio",
                    width: "50%",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Unknown",
                      },
                    ],
                    colCount: 3,
                  },
                  {
                    type: "panel",
                    name: "have_you_travelled_within_the_last_days_internationall_date_panel",
                    elements: [
                      {
                        type: "text",
                        name: "have_you_travelled_within_the_last_days_internationall_date_from",
                        title: "from",
                        inputType: "date",
                      },
                      {
                        type: "text",
                        name: "have_you_travelled_within_the_last_days_internationall_date_to",
                        startWithNewLine: false,
                        title: "to",
                        inputType: "date",
                      },
                    ],
                    visible: false,
                    visibleIf:
                      "{have_you_travelled_within_the_last_days_internationall_radio} = 'item1'",
                    title: "Dates of travel",
                  },
                  {
                    type: "text",
                    name: "have_you_travelled_within_the_last_days_internationall_countries",
                    visible: false,
                    visibleIf:
                      "{have_you_travelled_within_the_last_days_internationall_radio} = 'item1'",
                    title: "Countries visited:",
                  },
                  {
                    type: "text",
                    name: "have_you_travelled_within_the_last_days_internationall_cities",
                    visible: false,
                    visibleIf:
                      "{have_you_travelled_within_the_last_days_internationall_radio} = 'item1'",
                    startWithNewLine: false,
                    title: "Cities visited:",
                  },
                ],
                title: "Have you travelled within the last 14 days internationally?",
                showQuestionNumbers: "off",
              },
              {
                type: "panel",
                name: "in_the_past_days_have_you_had_contact_with_a_anyone_with_suspected_or_confirmed_ncov_infection_panel",
                elements: [
                  {
                    type: "radiogroup",
                    name: "in_the_past_days_have_you_had_contact_with_a_anyone_with_suspected_or_confirmed_ncov_infection_radio",
                    width: "50%",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Unknown",
                      },
                    ],
                    colCount: 3,
                  },
                  {
                    type: "text",
                    name: "in_the_past_days_have_you_had_contact_with_a_anyone_with_suspected_or_confirmed_ncov_infection_date",
                    startWithNewLine: false,
                    title: "Date of last contact",
                    enableIf:
                      "{in_the_past_days_have_you_had_contact_with_a_anyone_with_suspected_or_confirmed_ncov_infection_radio} = 'item1'",
                    readOnly: true,
                    inputType: "date",
                  },
                ],
                title:
                  "In the past 14 days, have you had contact with a anyone with suspected or confirmed 2019-nCoV infection?",
                showQuestionNumbers: "off",
              },
              {
                type: "panel",
                name: "patient_attended_festival_or_mass_gathering_panel",
                elements: [
                  {
                    type: "radiogroup",
                    name: "patient_attended_festival_or_mass_gathering_radio",
                    width: "50%",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Unknown",
                      },
                    ],
                    colCount: 3,
                  },
                  {
                    type: "text",
                    name: "patient_attended_festival_or_mass_gathering_specify",
                    startWithNewLine: false,
                    title: "Specify:",
                    enableIf: "{patient_attended_festival_or_mass_gathering_radio} = 'item1'",
                    readOnly: true,
                  },
                ],
                title: "Patient attended festival or mass gathering",
                showQuestionNumbers: "off",
              },
              {
                type: "radiogroup",
                name: "patient_exposed_to_person_with_similar_illness",
                width: "50%",
                title: "Patient exposed to person with similar illness",
                hideNumber: true,
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
              {
                type: "radiogroup",
                name: "location_of_exposure",
                width: "50%",
                title: "Location of exposure",
                hideNumber: true,
                choices: [
                  {
                    value: "item1",
                    text: "Home",
                  },
                  {
                    value: "item2",
                    text: "Hospital",
                  },
                  {
                    value: "item3",
                    text: "Workplace",
                  },
                  {
                    value: "item4",
                    text: "Tour group",
                  },
                  {
                    value: "item5",
                    text: "Unknown",
                  },
                ],
                hasOther: true,
                otherText: "Other, specify:",
                colCount: 3,
              },
              {
                type: "panel",
                name: "patient_visited_or_was_admitted_to_inpatient_health_facility_panel",
                elements: [
                  {
                    type: "radiogroup",
                    name: "patient_visited_or_was_admitted_to_inpatient_health_facility_radio",
                    width: "50%",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Unknown",
                      },
                    ],
                    colCount: 3,
                  },
                  {
                    type: "text",
                    name: "patient_visited_or_was_admitted_to_inpatient_health_facility_specify",
                    startWithNewLine: false,
                    title: "Specify:",
                    enableIf:
                      "{patient_visited_or_was_admitted_to_inpatient_health_facility_radio} = 'item1'",
                    readOnly: true,
                  },
                  {
                    type: "panel",
                    name: "patient_visited_outpatient_treatment_facility_panel",
                    elements: [
                      {
                        type: "radiogroup",
                        name: "patient_visited_outpatient_treatment_facility_radio",
                        width: "50%",
                        titleLocation: "hidden",
                        choices: [
                          {
                            value: "item1",
                            text: "Yes",
                          },
                          {
                            value: "item2",
                            text: "No",
                          },
                          {
                            value: "item3",
                            text: "Unknown",
                          },
                        ],
                        colCount: 3,
                      },
                      {
                        type: "text",
                        name: "patient_visited_outpatient_treatment_facility_specify",
                        startWithNewLine: false,
                        title: "Specify:",
                        enableIf: "{patient_visited_outpatient_treatment_facility_radio} = 'item1'",
                        readOnly: true,
                      },
                    ],
                    title: "Patient visited outpatient treatment facility",
                  },
                  {
                    type: "panel",
                    name: "patient_visited_traditional_healer_panel",
                    elements: [
                      {
                        type: "radiogroup",
                        name: "patient_visited_traditional_healer_radio",
                        width: "50%",
                        titleLocation: "hidden",
                        choices: [
                          {
                            value: "item1",
                            text: "Yes",
                          },
                          {
                            value: "item2",
                            text: "No",
                          },
                          {
                            value: "item3",
                            text: "Unknown",
                          },
                        ],
                        colCount: 3,
                      },
                      {
                        type: "text",
                        name: "patient_visited_traditional_healer_specify",
                        startWithNewLine: false,
                        title: "Specify:",
                        enableIf: "{patient_visited_traditional_healer_radio} = 'item1'",
                        readOnly: true,
                      },
                      {
                        type: "panel",
                        name: "patient_occupation_specify_location_facility_panel",
                        elements: [
                          {
                            type: "checkbox",
                            name: "patient_occupation_specify_location_facility_checkbox",
                            titleLocation: "hidden",
                            choices: [
                              {
                                value: "item1",
                                text: "Health care worker",
                              },
                              {
                                value: "item2",
                                text: "Working with animals ",
                              },
                              {
                                value: "item3",
                                text: "Health laboratory worker",
                              },
                              {
                                value: "item4",
                                text: "Student",
                              },
                            ],
                            hasOther: true,
                            otherText: "Other, specify:",
                            colCount: 3,
                          },
                          {
                            type: "text",
                            name: "patient_occupation_specify_location_facility_specify",
                            visible: false,
                            visibleIf:
                              "{patient_occupation_specify_location_facility_checkbox} notempty",
                            title: "For each occupation, please specify location or facility:",
                          },
                        ],
                        title: "Patient occupation (specify location/facility)",
                        showQuestionNumbers: "off",
                      },
                    ],
                    title: "Patient visited traditional healer",
                  },
                ],
                title: "Patient visited or was admitted to inpatient health facility",
                showQuestionNumbers: "off",
              },
            ],
            title: "Human exposures in the 14 days before illness onset",
            showNumber: true,
          },
        ],
        navigationTitle: "Exposures",
        navigationDescription: "Before illness",
      },
      {
        name: "page7",
        elements: [
          {
            type: "image",
            name: "seventh_image",
            width: "400px",
            minWidth: "400px",
            maxWidth: "400px",
            imageLink: "https://surveyjs.io/Content/Images/examples/covid/07.png",
            imageHeight: 441,
            imageWidth: 400,
          },
          {
            type: "panel",
            name: "status_of_form_completion_panel",
            elements: [
              {
                type: "boolean",
                name: "status_of_form_completion_boolean",
                title: "Form completed",
                isRequired: true,
                labelTrue: "Yes",
                labelFalse: "No or partially",
              },
              {
                type: "radiogroup",
                name: "status_of_form_completion_radio",
                visible: false,
                visibleIf: "{status_of_form_completion_boolean} = false",
                title: "Reason:",
                choices: [
                  {
                    value: "item1",
                    text: "Missed",
                  },
                  {
                    value: "item2",
                    text: "Not attempted",
                  },
                  {
                    value: "item3",
                    text: "Not performed",
                  },
                  {
                    value: "item4",
                    text: "Refusal",
                  },
                ],
                hasOther: true,
                otherText: "Other, specific:",
              },
            ],
            title: "Status of form completion",
            startWithNewLine: false,
            showNumber: true,
            showQuestionNumbers: "off",
          },
        ],
        navigationTitle: "Completion",
        navigationDescription: "Status of form",
      },
    ],
    showProgressBar: "top",
    progressBarType: "buttons",
    formSettings: {
      requiredLogin: false,
      multipleSubmission: true,
      showCaptcha: false,
      privacySettings: "public",
      locationSettings: false,
      limitedUser: [],
      country: null,
      city: null,
      publishSettings: true,
      unPublishSettings: false,
      publishDate: null,
      publishTime: null,
      publishRedirectUrl: "",
      publishRedirectBtnText: "",
      publishDescription: "",
      unPublishDate: null,
      unPublishTime: null,
      unPublishRedirectUrl: "",
      unPublishRedirectBtnText: "",
      unPublishDescription: "",
    },
  },
];
export let surveyList3 = [
  {
    id: uuidv4(),
    title: "COVID-19",
    description: "Minimum data reporting form – for suspected and probable cases of COVID-19",
    logoPosition: "right",
    pages: [
      {
        name: "page1",
        elements: [
          {
            type: "image",
            name: "first_page_image",
            width: "400px",
            minWidth: "400px",
            maxWidth: "400px",
            imageLink: "https://surveyjs.io/Content/Images/examples/covid/01.png",
            imageFit: "none",
            imageHeight: 726,
            imageWidth: 400,
          },
          {
            type: "panel",
            name: "first_page_container_panel",
            elements: [
              {
                type: "text",
                name: "unique_case_id_textbox",
                startWithNewLine: false,
                title: "Unique Case ID / Cluster Number (if applicable):",
                hideNumber: true,
                inputType: "number",
              },
              {
                type: "panel",
                name: "current_status_panel",
                elements: [
                  {
                    type: "boolean",
                    name: "current_status",
                    titleLocation: "hidden",
                    hideNumber: true,
                    labelTrue: "Alive",
                    labelFalse: "Dead",
                  },
                ],
                title: "Current Status",
                showNumber: true,
              },
              {
                type: "panel",
                name: "data_collector_information",
                elements: [
                  {
                    type: "text",
                    name: "name_of_data_collector",
                    title: "Name of data collector",
                  },
                  {
                    type: "text",
                    name: "data_collector_institution",
                    title: "Data collector Institution",
                  },
                  {
                    type: "text",
                    name: "data_collector_telephone_number",
                    title: "Data collector telephone number",
                    inputType: "tel",
                  },
                  {
                    type: "text",
                    name: "email",
                    title: "Email",
                    inputType: "email",
                  },
                  {
                    type: "text",
                    name: "form_completion_date",
                    title: "Form completion date",
                    inputType: "date",
                  },
                ],
                title: "Data Collector Information",
                showNumber: true,
                showQuestionNumbers: "off",
              },
            ],
            startWithNewLine: false,
          },
        ],
        navigationTitle: "Collector",
        navigationDescription: "Collector's info",
      },
      {
        name: "page2",
        elements: [
          {
            type: "boolean",
            name: "is_the_person_providing_the_information_is_the_patient",
            title: "Is the person providing the information is the patient?",
            hideNumber: true,
            isRequired: true,
            labelTrue: "Yes",
            labelFalse: "No",
          },
          {
            type: "panel",
            name: "case_identifier_information",
            elements: [
              {
                type: "text",
                name: "given_names",
                title: "Given name(s)",
              },
              {
                type: "text",
                name: "family_name",
                startWithNewLine: false,
                title: "Family name",
              },
              {
                type: "radiogroup",
                name: "case_identifier_information_sex",
                title: "Sex",
                choices: [
                  {
                    value: "item1",
                    text: "Male",
                  },
                  {
                    value: "item2",
                    text: "Female",
                  },
                  {
                    value: "item3",
                    text: "Not known",
                  },
                ],
                colCount: 3,
              },
              {
                type: "panel",
                name: "case_identifier_information_patient_date_of_birth_panel",
                elements: [
                  {
                    type: "text",
                    name: "case_identifier_information_patient_date_of_birth_date",
                    visibleIf: "{case_identifier_information_patient_date_of_birth_checkbox} empty",
                    titleLocation: "hidden",
                    inputType: "date",
                  },
                  {
                    type: "checkbox",
                    name: "case_identifier_information_patient_date_of_birth_checkbox",
                    startWithNewLine: false,
                    titleLocation: "hidden",
                    choices: ["Unknown"],
                  },
                ],
                title: "Date of Birth",
              },
              {
                type: "text",
                name: "case_identifier_information_patient_telephone_mobile_number",
                title: "Telephone (mobile) number",
                inputType: "tel",
              },
              {
                type: "panel",
                name: "case_identifier_information_patient_age",
                elements: [
                  {
                    type: "text",
                    name: "case_identifier_information_patient_age_years",
                    visibleIf: "{case_identifier_information_patient_age_checkbox} empty",
                    title: "Years:",
                    inputType: "number",
                  },
                  {
                    type: "text",
                    name: "case_identifier_information_patient_age_months",
                    visibleIf: "{case_identifier_information_patient_age_checkbox} empty",
                    startWithNewLine: false,
                    title: "Months:",
                    inputType: "number",
                  },
                  {
                    type: "checkbox",
                    name: "case_identifier_information_patient_age_checkbox",
                    startWithNewLine: false,
                    titleLocation: "hidden",
                    choices: ["Unknown"],
                  },
                ],
                title: "Age (years, months) ",
              },
              {
                type: "text",
                name: "case_identifier_information_patient_email",
                title: "Email",
                inputType: "email",
              },
              {
                type: "text",
                name: "case_identifier_information_patient_address",
                startWithNewLine: false,
                title: "Address",
              },
              {
                type: "text",
                name: "case_identifier_information_patient_national_social_number",
                title: "National social number/ identifier (if applicable)",
              },
              {
                type: "dropdown",
                name: "case_identifier_information_patient_country_of_residence",
                title: "Country of residence",
                choices: ["item1", "item2", "item3"],
                choicesByUrl: {
                  url: "https://surveyjs.io/api/CountriesExample",
                  valueName: "name",
                },
              },
              {
                type: "radiogroup",
                name: "case_identifier_information_patient_case_status",
                startWithNewLine: false,
                title: "Case status",
                choices: [
                  {
                    value: "item1",
                    text: "Suspected",
                  },
                  {
                    value: "item2",
                    text: "Probable",
                  },
                  {
                    value: "item3",
                    text: "Confirmed",
                  },
                ],
                colCount: 3,
              },
            ],
            visible: false,
            visibleIf: "{is_the_person_providing_the_information_is_the_patient} = true",
            title: "Case Identifier Information",
            showNumber: true,
            showQuestionNumbers: "off",
          },
          {
            type: "panel",
            name: "interview_respondent_information_if_the_persons_providing_the_information_is_not_the_patient",
            elements: [
              {
                type: "text",
                name: "first_name",
                title: "First name ",
              },
              {
                type: "text",
                name: "surname",
                startWithNewLine: false,
                title: "Surname",
              },
              {
                type: "radiogroup",
                name: "interview_respondent_information_sex",
                title: "Sex",
                choices: [
                  {
                    value: "item1",
                    text: "Male",
                  },
                  {
                    value: "item2",
                    text: "Female",
                  },
                  {
                    value: "item3",
                    text: "Not known",
                  },
                ],
                colCount: 3,
              },
              {
                type: "panel",
                name: "interview_respondent_information_patient_date_of_birth_panel",
                elements: [
                  {
                    type: "text",
                    name: "interview_respondent_information_patient_date_of_birth_date",
                    visibleIf:
                      "{interview_respondent_information_patient_date_of_birth_checkbox} empty",
                    titleLocation: "hidden",
                    inputType: "date",
                  },
                  {
                    type: "checkbox",
                    name: "interview_respondent_information_patient_date_of_birth_checkbox",
                    startWithNewLine: false,
                    titleLocation: "hidden",
                    choices: ["Unknown"],
                  },
                ],
                title: "Date of Birth",
              },
              {
                type: "text",
                name: "relationship_to_patient",
                title: "Relationship to patient",
              },
              {
                type: "text",
                name: "respondent_address",
                startWithNewLine: false,
                title: "Respondent address",
              },
              {
                type: "text",
                name: "interview_respondent_information_patient_telephone_mobile_number",
                startWithNewLine: false,
                title: "Telephone (mobile) number",
                inputType: "tel",
              },
            ],
            visible: false,
            visibleIf: "{is_the_person_providing_the_information_is_the_patient} = false",
            title: "Interview respondent information",
            showNumber: true,
            showQuestionNumbers: "off",
          },
        ],
        navigationTitle: "Person",
        navigationDescription: "Person's info",
      },
      {
        name: "page3",
        elements: [
          {
            type: "image",
            name: "third_page_image",
            width: "400px",
            minWidth: "400px",
            maxWidth: "400px",
            imageLink: "https://surveyjs.io/Content/Images/examples/covid/03.png",
            imageHeight: 690,
            imageWidth: 400,
          },
          {
            type: "panel",
            name: "patient_symptoms_from_disease_onset",
            elements: [
              {
                type: "panel",
                name: "date_of_first_symptom_onset",
                elements: [
                  {
                    type: "text",
                    name: "date_of_first_symptom_onset_date",
                    width: "314px",
                    minWidth: "314px",
                    maxWidth: "314px",
                    titleLocation: "hidden",
                    enableIf: "{date_of_first_symptom_onset_checkbox} empty",
                    inputType: "date",
                  },
                  {
                    type: "checkbox",
                    name: "date_of_first_symptom_onset_checkbox",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "No symptoms",
                        enableIf: "{date_of_first_symptom_onset_checkbox} <> ['item2']",
                      },
                      {
                        value: "item2",
                        text: "Unknown",
                        enableIf: "{date_of_first_symptom_onset_checkbox} <> ['item1']",
                      },
                    ],
                    colCount: 2,
                  },
                  {
                    type: "radiogroup",
                    name: "question1",
                    title: "Fever (≥38 °C) or history of fever ",
                    titleLocation: "top",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Unknown",
                      },
                    ],
                    colCount: 3,
                  },
                ],
                title: "Date of first symptom onset",
              },
              {
                type: "radiogroup",
                name: "sore_throat",
                title: "Sore throat",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
              {
                type: "radiogroup",
                name: "runny_nose",
                title: "Runny nose",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
              {
                type: "radiogroup",
                name: "cough",
                title: "Cough",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
              {
                type: "radiogroup",
                name: "shortness_of_Breath",
                title: "Shortness of Breath",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
              {
                type: "radiogroup",
                name: "vomiting",
                title: "Vomiting",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
              {
                type: "radiogroup",
                name: "nausea",
                title: "Nausea",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
              {
                type: "radiogroup",
                name: "diarrhea",
                title: "Diarrhea",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
            ],
            title: "Patient symptoms (from disease onset)",
            startWithNewLine: false,
            showNumber: true,
            showQuestionNumbers: "off",
          },
        ],
        navigationTitle: "Symptoms",
        navigationDescription: "Patient symptoms",
      },
      {
        name: "page4",
        elements: [
          {
            type: "image",
            name: "fouth_page_image",
            width: "400px",
            minWidth: "400px",
            maxWidth: "400px",
            imageLink: "https://surveyjs.io/Content/Images/examples/covid/04.png",
            imageHeight: 567,
            imageWidth: 400,
          },
          {
            type: "panel",
            name: "initial_sample_collection",
            elements: [
              {
                type: "text",
                name: "date_respiratory_sample_collected",
                title: "Date respiratory sample collected",
                inputType: "date",
              },
              {
                type: "radiogroup",
                name: "what_type_of_respiratory_sample_was_collected",
                title: "What type of respiratory sample was collected?",
                choices: [
                  {
                    value: "item1",
                    text: "Nasal swab",
                  },
                  {
                    value: "item2",
                    text: "Throat swab ",
                  },
                  {
                    value: "item3",
                    text: "Nasopharyngeal swab",
                  },
                ],
                hasOther: true,
                otherText: "Other, specify",
                colCount: 2,
              },
              {
                type: "panel",
                name: "has_baseline_serum_been_taken_panel",
                elements: [
                  {
                    type: "radiogroup",
                    name: "has_baseline_serum_been_taken_radio",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Unknown",
                      },
                    ],
                    colCount: 3,
                  },
                  {
                    type: "text",
                    name: "has_baseline_serum_been_taken_date",
                    visible: false,
                    visibleIf: "{has_baseline_serum_been_taken_radio} = 'item1'",
                    startWithNewLine: false,
                    title: "Date baseline serum taken",
                    inputType: "date",
                  },
                ],
                title: "Has baseline serum been taken?",
              },
              {
                type: "panel",
                name: "were_other_samples_collected_panel",
                elements: [
                  {
                    type: "radiogroup",
                    name: "were_other_samples_collected_radio",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Unknown",
                      },
                    ],
                    colCount: 3,
                  },
                  {
                    type: "text",
                    name: "were_other_samples_collected_textbox",
                    visible: false,
                    visibleIf: "{were_other_samples_collected_radio} = 'item1'",
                    startWithNewLine: false,
                    title: "Which samples:",
                  },
                  {
                    type: "text",
                    name: "were_other_samples_collected_date",
                    visible: false,
                    visibleIf: "{were_other_samples_collected_radio} = 'item1'",
                    startWithNewLine: false,
                    title: "Date taken",
                    inputType: "date",
                  },
                ],
                title: "Were other samples collected? ",
              },
            ],
            title: "Initial sample collection",
            startWithNewLine: false,
            showNumber: true,
            showQuestionNumbers: "off",
          },
        ],
        navigationTitle: "Sample",
        navigationDescription: "Initial sample",
      },
      {
        name: "page5",
        elements: [
          {
            type: "image",
            name: "fifth_image",
            width: "400px",
            minWidth: "400px",
            maxWidth: "400px",
            imageLink: "https://surveyjs.io/Content/Images/examples/covid/05.png",
            imageHeight: 713,
            imageWidth: 400,
          },
          {
            type: "panel",
            name: "clinical_course_complications_panel",
            elements: [
              {
                type: "panel",
                name: "hospitalization_required_panel",
                elements: [
                  {
                    type: "radiogroup",
                    name: "hospitalization_required_radio",
                    width: "49%",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Unknown",
                      },
                    ],
                    colCount: 3,
                  },
                  {
                    type: "text",
                    name: "hospitalization_required_hospital",
                    width: "313px",
                    minWidth: "313px",
                    maxWidth: "313px",
                    title: "Name of hospital",
                    enableIf: "{hospitalization_required_radio} = 'item1'",
                    readOnly: true,
                  },
                ],
                title: "Hospitalization required?",
              },
              {
                type: "radiogroup",
                name: "icu_Intensive_Care_Unit_admission_required",
                title: "ICU (Intensive Care Unit) admission required",
                titleLocation: "top",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
              {
                type: "radiogroup",
                name: "acute_respiratory_distress_syndrome_ards",
                title: "Acute Respiratory Distress Syndrome (ARDS)",
                titleLocation: "top",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
              {
                type: "panel",
                name: "pneumonia_by_chest_xray_panel",
                elements: [
                  {
                    type: "radiogroup",
                    name: "pneumonia_by_chest_xray_radio",
                    width: "49%",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Not applicable (no X-ray performed)",
                      },
                    ],
                    colCount: 3,
                  },
                  {
                    type: "text",
                    name: "pneumonia_by_chest_xray_date",
                    width: "313px",
                    minWidth: "313px",
                    maxWidth: "313px",
                    title: "Date",
                    enableIf: "{pneumonia_by_chest_xray_radio} = 'item1'",
                    readOnly: true,
                    inputType: "date",
                  },
                ],
                title: "Pneumonia by chest X-ray ",
              },
              {
                type: "panel",
                name: "other_severe_or_life_threatening_illness_suggestive_of_an_infection_panel",
                elements: [
                  {
                    type: "radiogroup",
                    name: "other_severe_or_life_threatening_illness_suggestive_of_an_radio",
                    width: "49%",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Unknown",
                      },
                    ],
                    colCount: 3,
                  },
                  {
                    type: "text",
                    name: "other_severe_or_life_threatening_illness_suggestive_of_an_specify",
                    width: "313px",
                    minWidth: "313px",
                    maxWidth: "313px",
                    title: "Specify:",
                    enableIf:
                      "{other_severe_or_life_threatening_illness_suggestive_of_an_radio} = 'item1'",
                    readOnly: true,
                  },
                ],
                title: "Other severe or life-threatening illness suggestive of an infection",
              },
              {
                type: "radiogroup",
                name: "mechanical_ventilation_required",
                title: "Mechanical ventilation required",
                titleLocation: "top",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
              {
                type: "radiogroup",
                name: "extracorporeal_membrane_oxygenation_emo",
                title: "Extracorporeal membrane oxygenation (EMO)",
                titleLocation: "top",
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
            ],
            title: "Clinical Course: Complications",
            startWithNewLine: false,
            showNumber: true,
            showQuestionNumbers: "off",
          },
        ],
        navigationTitle: "Complications",
        navigationDescription: "Clinical Course",
      },
      {
        name: "page6",
        elements: [
          {
            type: "panel",
            name: "human_exposures_in_the_days_before_illness_onset",
            elements: [
              {
                type: "panel",
                name: "have_you_travelled_within_the_last_days_domestically_panel",
                elements: [
                  {
                    type: "radiogroup",
                    name: "have_you_travelled_within_the_last_days_domestically_radio",
                    width: "50%",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Unknown",
                      },
                    ],
                    colCount: 3,
                  },
                  {
                    type: "panel",
                    name: "have_you_travelled_within_the_last_days_domestically_date_panel",
                    elements: [
                      {
                        type: "text",
                        name: "have_you_travelled_within_the_last_days_domestically_date_from",
                        title: "from",
                        inputType: "date",
                      },
                      {
                        type: "text",
                        name: "have_you_travelled_within_the_last_days_domestically_date_to",
                        startWithNewLine: false,
                        title: "to",
                        inputType: "date",
                      },
                    ],
                    visible: false,
                    visibleIf:
                      "{have_you_travelled_within_the_last_days_domestically_radio} = 'item1'",
                    title: "Dates of travel",
                  },
                  {
                    type: "text",
                    name: "have_you_travelled_within_the_last_days_domestically_regions",
                    visible: false,
                    visibleIf:
                      "{have_you_travelled_within_the_last_days_domestically_radio} = 'item1'",
                    title: "Regions:",
                  },
                  {
                    type: "text",
                    name: "have_you_travelled_within_the_last_days_domestically_cities_visited",
                    visible: false,
                    visibleIf:
                      "{have_you_travelled_within_the_last_days_domestically_radio} = 'item1'",
                    startWithNewLine: false,
                    title: "Cities visited:",
                  },
                ],
                title: "Have you travelled within the last 14 days domestically?",
                showQuestionNumbers: "off",
              },
              {
                type: "panel",
                name: "have_you_travelled_within_the_last_days_internationall_panel",
                elements: [
                  {
                    type: "radiogroup",
                    name: "have_you_travelled_within_the_last_days_internationall_radio",
                    width: "50%",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Unknown",
                      },
                    ],
                    colCount: 3,
                  },
                  {
                    type: "panel",
                    name: "have_you_travelled_within_the_last_days_internationall_date_panel",
                    elements: [
                      {
                        type: "text",
                        name: "have_you_travelled_within_the_last_days_internationall_date_from",
                        title: "from",
                        inputType: "date",
                      },
                      {
                        type: "text",
                        name: "have_you_travelled_within_the_last_days_internationall_date_to",
                        startWithNewLine: false,
                        title: "to",
                        inputType: "date",
                      },
                    ],
                    visible: false,
                    visibleIf:
                      "{have_you_travelled_within_the_last_days_internationall_radio} = 'item1'",
                    title: "Dates of travel",
                  },
                  {
                    type: "text",
                    name: "have_you_travelled_within_the_last_days_internationall_countries",
                    visible: false,
                    visibleIf:
                      "{have_you_travelled_within_the_last_days_internationall_radio} = 'item1'",
                    title: "Countries visited:",
                  },
                  {
                    type: "text",
                    name: "have_you_travelled_within_the_last_days_internationall_cities",
                    visible: false,
                    visibleIf:
                      "{have_you_travelled_within_the_last_days_internationall_radio} = 'item1'",
                    startWithNewLine: false,
                    title: "Cities visited:",
                  },
                ],
                title: "Have you travelled within the last 14 days internationally?",
                showQuestionNumbers: "off",
              },
              {
                type: "panel",
                name: "in_the_past_days_have_you_had_contact_with_a_anyone_with_suspected_or_confirmed_ncov_infection_panel",
                elements: [
                  {
                    type: "radiogroup",
                    name: "in_the_past_days_have_you_had_contact_with_a_anyone_with_suspected_or_confirmed_ncov_infection_radio",
                    width: "50%",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Unknown",
                      },
                    ],
                    colCount: 3,
                  },
                  {
                    type: "text",
                    name: "in_the_past_days_have_you_had_contact_with_a_anyone_with_suspected_or_confirmed_ncov_infection_date",
                    startWithNewLine: false,
                    title: "Date of last contact",
                    enableIf:
                      "{in_the_past_days_have_you_had_contact_with_a_anyone_with_suspected_or_confirmed_ncov_infection_radio} = 'item1'",
                    readOnly: true,
                    inputType: "date",
                  },
                ],
                title:
                  "In the past 14 days, have you had contact with a anyone with suspected or confirmed 2019-nCoV infection?",
                showQuestionNumbers: "off",
              },
              {
                type: "panel",
                name: "patient_attended_festival_or_mass_gathering_panel",
                elements: [
                  {
                    type: "radiogroup",
                    name: "patient_attended_festival_or_mass_gathering_radio",
                    width: "50%",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Unknown",
                      },
                    ],
                    colCount: 3,
                  },
                  {
                    type: "text",
                    name: "patient_attended_festival_or_mass_gathering_specify",
                    startWithNewLine: false,
                    title: "Specify:",
                    enableIf: "{patient_attended_festival_or_mass_gathering_radio} = 'item1'",
                    readOnly: true,
                  },
                ],
                title: "Patient attended festival or mass gathering",
                showQuestionNumbers: "off",
              },
              {
                type: "radiogroup",
                name: "patient_exposed_to_person_with_similar_illness",
                width: "50%",
                title: "Patient exposed to person with similar illness",
                hideNumber: true,
                choices: [
                  {
                    value: "item1",
                    text: "Yes",
                  },
                  {
                    value: "item2",
                    text: "No",
                  },
                  {
                    value: "item3",
                    text: "Unknown",
                  },
                ],
                colCount: 3,
              },
              {
                type: "radiogroup",
                name: "location_of_exposure",
                width: "50%",
                title: "Location of exposure",
                hideNumber: true,
                choices: [
                  {
                    value: "item1",
                    text: "Home",
                  },
                  {
                    value: "item2",
                    text: "Hospital",
                  },
                  {
                    value: "item3",
                    text: "Workplace",
                  },
                  {
                    value: "item4",
                    text: "Tour group",
                  },
                  {
                    value: "item5",
                    text: "Unknown",
                  },
                ],
                hasOther: true,
                otherText: "Other, specify:",
                colCount: 3,
              },
              {
                type: "panel",
                name: "patient_visited_or_was_admitted_to_inpatient_health_facility_panel",
                elements: [
                  {
                    type: "radiogroup",
                    name: "patient_visited_or_was_admitted_to_inpatient_health_facility_radio",
                    width: "50%",
                    titleLocation: "hidden",
                    choices: [
                      {
                        value: "item1",
                        text: "Yes",
                      },
                      {
                        value: "item2",
                        text: "No",
                      },
                      {
                        value: "item3",
                        text: "Unknown",
                      },
                    ],
                    colCount: 3,
                  },
                  {
                    type: "text",
                    name: "patient_visited_or_was_admitted_to_inpatient_health_facility_specify",
                    startWithNewLine: false,
                    title: "Specify:",
                    enableIf:
                      "{patient_visited_or_was_admitted_to_inpatient_health_facility_radio} = 'item1'",
                    readOnly: true,
                  },
                  {
                    type: "panel",
                    name: "patient_visited_outpatient_treatment_facility_panel",
                    elements: [
                      {
                        type: "radiogroup",
                        name: "patient_visited_outpatient_treatment_facility_radio",
                        width: "50%",
                        titleLocation: "hidden",
                        choices: [
                          {
                            value: "item1",
                            text: "Yes",
                          },
                          {
                            value: "item2",
                            text: "No",
                          },
                          {
                            value: "item3",
                            text: "Unknown",
                          },
                        ],
                        colCount: 3,
                      },
                      {
                        type: "text",
                        name: "patient_visited_outpatient_treatment_facility_specify",
                        startWithNewLine: false,
                        title: "Specify:",
                        enableIf: "{patient_visited_outpatient_treatment_facility_radio} = 'item1'",
                        readOnly: true,
                      },
                    ],
                    title: "Patient visited outpatient treatment facility",
                  },
                  {
                    type: "panel",
                    name: "patient_visited_traditional_healer_panel",
                    elements: [
                      {
                        type: "radiogroup",
                        name: "patient_visited_traditional_healer_radio",
                        width: "50%",
                        titleLocation: "hidden",
                        choices: [
                          {
                            value: "item1",
                            text: "Yes",
                          },
                          {
                            value: "item2",
                            text: "No",
                          },
                          {
                            value: "item3",
                            text: "Unknown",
                          },
                        ],
                        colCount: 3,
                      },
                      {
                        type: "text",
                        name: "patient_visited_traditional_healer_specify",
                        startWithNewLine: false,
                        title: "Specify:",
                        enableIf: "{patient_visited_traditional_healer_radio} = 'item1'",
                        readOnly: true,
                      },
                      {
                        type: "panel",
                        name: "patient_occupation_specify_location_facility_panel",
                        elements: [
                          {
                            type: "checkbox",
                            name: "patient_occupation_specify_location_facility_checkbox",
                            titleLocation: "hidden",
                            choices: [
                              {
                                value: "item1",
                                text: "Health care worker",
                              },
                              {
                                value: "item2",
                                text: "Working with animals ",
                              },
                              {
                                value: "item3",
                                text: "Health laboratory worker",
                              },
                              {
                                value: "item4",
                                text: "Student",
                              },
                            ],
                            hasOther: true,
                            otherText: "Other, specify:",
                            colCount: 3,
                          },
                          {
                            type: "text",
                            name: "patient_occupation_specify_location_facility_specify",
                            visible: false,
                            visibleIf:
                              "{patient_occupation_specify_location_facility_checkbox} notempty",
                            title: "For each occupation, please specify location or facility:",
                          },
                        ],
                        title: "Patient occupation (specify location/facility)",
                        showQuestionNumbers: "off",
                      },
                    ],
                    title: "Patient visited traditional healer",
                  },
                ],
                title: "Patient visited or was admitted to inpatient health facility",
                showQuestionNumbers: "off",
              },
            ],
            title: "Human exposures in the 14 days before illness onset",
            showNumber: true,
          },
        ],
        navigationTitle: "Exposures",
        navigationDescription: "Before illness",
      },
      {
        name: "page7",
        elements: [
          {
            type: "image",
            name: "seventh_image",
            width: "400px",
            minWidth: "400px",
            maxWidth: "400px",
            imageLink: "https://surveyjs.io/Content/Images/examples/covid/07.png",
            imageHeight: 441,
            imageWidth: 400,
          },
          {
            type: "panel",
            name: "status_of_form_completion_panel",
            elements: [
              {
                type: "boolean",
                name: "status_of_form_completion_boolean",
                title: "Form completed",
                isRequired: true,
                labelTrue: "Yes",
                labelFalse: "No or partially",
              },
              {
                type: "radiogroup",
                name: "status_of_form_completion_radio",
                visible: false,
                visibleIf: "{status_of_form_completion_boolean} = false",
                title: "Reason:",
                choices: [
                  {
                    value: "item1",
                    text: "Missed",
                  },
                  {
                    value: "item2",
                    text: "Not attempted",
                  },
                  {
                    value: "item3",
                    text: "Not performed",
                  },
                  {
                    value: "item4",
                    text: "Refusal",
                  },
                ],
                hasOther: true,
                otherText: "Other, specific:",
              },
            ],
            title: "Status of form completion",
            startWithNewLine: false,
            showNumber: true,
            showQuestionNumbers: "off",
          },
        ],
        navigationTitle: "Completion",
        navigationDescription: "Status of form",
      },
    ],
    showProgressBar: "top",
    progressBarType: "buttons",
    formSettings: {
      requiredLogin: false,
      multipleSubmission: true,
      showCaptcha: false,
      privacySettings: "public",
      locationSettings: false,
      limitedUser: [],
      country: null,
      city: null,
      publishSettings: true,
      unPublishSettings: false,
      publishDate: null,
      publishTime: null,
      publishRedirectUrl: "",
      publishRedirectBtnText: "",
      publishDescription: "",
      unPublishDate: null,
      unPublishTime: null,
      unPublishRedirectUrl: "",
      unPublishRedirectBtnText: "",
      unPublishDescription: "",
    },
  },
];

export let initialEmptySurvey = {
  formSettings: {
    requiredLogin: false,
    multipleSubmission: true,
    showCaptcha: false,
    privacySettings: "public",
    locationSettings: false,
    limitedUser: [],
    locationInfo:[],
    formVersion: false,

    publishSettings: true,
    publishCustomBranding: false,
    publishDate: null,
    publishTime: null,
    publishLogoUrl: "",
    publishLogoTagline: "",
    publishRedirectUrl: "",
    publishRedirectBtnText: "",
    publishRedirectBtnColor: "",
    publishBgImg: "",
    publishBgColor: "",
    publishFontColor: "",
    publishDescription: "",

    unPublishSettings: false,
    unPublishCustomBranding: false,
    unPublishDate: null,
    unPublishTime: null,
    unPublishLogoUrl: "",
    unPublishLogoTagline: "",
    unPublishIllustration:"",
    unPublishRedirectUrl: "",
    unPublishRedirectBtnText: "",
    unPublishRedirectBtnColor: "",
    unPublishBgImg: "",
    unPublishBgColor: "",
    unPublishFontColor: "",
    unPublishDescription: "",
  },
};
