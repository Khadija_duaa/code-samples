import { Grid } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import React, { useContext, useState } from "react";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
import Truncate from "react-truncate";
import CustomTooltip from "../../components/Tooltip/Tooltip";
import { deleteFormVersion, getAllFormVersions } from "../../redux/actions/forms";
import { updateState } from "./Context/actions";
import SurveyContext from "./Context/form.context";
import FormActions from "./FormActions";
import styles from "./forms.style";
import VersionsDialog from "./versionsDialog";

function AllSurveys(params) {
  const { classes, history, list, permission, view } = params;
  const { state, dispatch } = useContext(SurveyContext);
  const [open, setOpen] = useState(false);
  const [formId, setFormId] = useState(false);


  const handleFormClick = (item) => {
    if (view !== "assignedToMe") {
      // open in new tab
      // updateState(dispatch, { selectedForm: item });
      // history.push(`/form?id=${item.formId}`);
      // if open in new tab than use below action
      // window.open(`/form?id=${item.formId}`, "_blank");

      // onpen in same tab
      updateState(dispatch, { selectedForm: item, view: "new-survey" });
      
    }
  };

  const handleVersionClick = (e, form) => {
    e.stopPropagation();
    if (permission.versionPermission) {
      setOpen(true);
      setFormId(form.formId)
    }
  };

  const closeAction = () => {
    setOpen(false);
  };


  return (
    <div className={classes.boardContainer}>
      <Grid container spacing={2}>
        {list.map((form, index) => {
          return (
            <Grid
              item
              classes={{ item: classes.boardsDashboardStyles }}
              xs={6}
              sm={6}
              md={4}
              lg={3}
              xl={2}
              key={form._id}>
              <div
                className={classes.boardProjectItemInner}
                style={{
                  backgroundColor: "rgb(74, 170, 238)",
                  //   backgroundImage: ``,
                  //   backgroundPosition: "50% center",
                }}
                onClick={(event) => {
                  handleFormClick(form);
                }}>
                <div style={{ zIndex: 1000 }}>
                  <div className={classes.boardIcon}>
                    <FormActions data={form} permission={permission} />
                  </div>
                  <div className={classes.versionCnt} onClick={(e) => handleVersionClick(e, form)}>
                    <span>{`v ${form.version}`}</span>
                  </div>
                  <div className={classes.projectNameCnt}>
                    <CustomTooltip helptext={form.title} placement="bottom">
                      <Truncate
                        lines={2}
                        trimWhitespace={true}
                        width={235}
                        ellipsis={<span>...</span>}
                        className={classes.boardProjectHeading}
                        style={{
                          color: "#FFFFFF",
                        }}>
                        {form.title}
                      </Truncate>
                    </CustomTooltip>
                  </div>
                  <div title={form.description} className={classes.projectNameCnt}>
                    <Truncate
                      lines={2}
                      trimWhitespace={true}
                      width={225}
                      ellipsis={<span>...</span>}
                      className={classes.boardProjectDescription}
                      style={{
                        color: "#FFFFFF",
                      }}>
                      {form.description}
                    </Truncate>
                  </div>
                </div>
              </div>
            </Grid>
          );
        })}
      </Grid>
      {open && (
        <VersionsDialog
          open={open}
          formId={formId}
          closeAction={closeAction}
        />
      )}
    </div>
  );
}

AllSurveys.defaultProps = {
  classes: {},
  theme: {},
  permission: undefined,
  view: ""
};

export default compose(withRouter, withStyles(styles, { withTheme: true }))(AllSurveys);
// export default withStyles(styles, { withTheme: true })(AllSurveys);
