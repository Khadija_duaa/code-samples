import { SvgIcon } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import Tooltip from "@material-ui/core/Tooltip";
import DefaultIcon from "@material-ui/icons/FlipToBack";
import SearchIcon from "@material-ui/icons/Search";
import PreviewIcon from "@material-ui/icons/Visibility";
import moment from "moment";
import React, { useEffect, useState, useContext } from "react";
import Scrollbars from "react-custom-scrollbars";
import Hotkeys from "react-hot-keys";
import CustomIconButton from "../../components/Buttons/CustomIconButton";
import CustomDialog from "../../components/Dialog/CustomDialog";
import DefaultTextField from "../../components/Form/TextField";
import DeleteIcon from "../../components/Icons/Delete";
import IconEditSmall from "../../components/Icons/IconEditSmall";
import CustomTooltip from "../../components/Tooltip/Tooltip";
import {
    deleteFormVersion,
    getAllFormVersions,
    versionNameUpdate,
    markCurrentVersion,
    getAllForms
} from "../../redux/actions/forms";
import styles from "./forms.style";
import SurveyContext from "./Context/form.context";
import { updateState } from "./Context/actions";


function VersionsDialog(params) {
    const { theme, classes, open, closeAction, formId } = params;
    const [editForm, setEditForm] = useState(null);
    const [searchInput, setSearchInput] = useState("");
    const [allVersions, setAllVersions] = useState([]);
    const { state, dispatch } = useContext(SurveyContext);


    const handlePreviewForm = (data) => {
        window.open(`/form/fill?id=${data.formId}`, "_blank");
    };

    const handleUpdateSearch = (value) => {
        setSearchInput(value);
    };
    const getEditIcon = (form) => {
        return (
            <div className={classes.editIconCnt}>
                <CustomTooltip
                    helptext="Edit form Name"
                    iconType="help"
                    placement="top"
                    style={{ color: theme.palette.common.white }}>
                    <CustomIconButton
                        onClick={(event) => {
                            setEditForm(form);
                        }}
                        btnType="transparent"
                        variant="contained"
                        style={{
                            padding: "3px 3px 0px 0px",
                            backgroundColor: "transparent",
                        }}
                        disabled={false}>
                        <SvgIcon viewBox="0 0 12 11.957" className={classes.editIcon}>
                            <IconEditSmall />
                        </SvgIcon>
                    </CustomIconButton>
                </CustomTooltip>
            </div>
        );
    };
    const handleChangeFormName = (e) => {
        setEditForm({ ...editForm, versionName: e.target.value });
    };
    const handleFormNameBlur = () => {
        let obj = {
            _id: editForm._id,
            versionName: editForm.versionName,
        };
        let updatedData = allVersions.map(item => {
            if (item._id === editForm._id) return editForm
            return item;
        })
        setAllVersions(updatedData);
        setEditForm(null);
        versionNameUpdate(
            obj,
            (succ) => {
                console.log(succ);
            },
            (fail) => {
                console.log(succ);
            }
        );
    };
    const getVersions = () => {
        getAllFormVersions(
            formId,
            (succ) => {
                setAllVersions(succ.data);
            },
            (fail) => {
                console.log("Something went wrong!");
            }
        );
    };
    useEffect(() => {
        getVersions();
    }, []);

    const handleDeleteForm = (data) => {
        deleteFormVersion(
            data._id,
            (succ) => {
                const updatedData = allVersions.filter((item) => item._id !== data._id);
                setAllVersions(updatedData);
            },
            (fail) => {
                console.log("================", fail);
            }
        );
    };
    const filteredItems = allVersions.filter(
        (obj) => obj[`title`].toLowerCase().indexOf(searchInput.toLowerCase()) > -1
    );
    const handleGetForms = () => {
        getAllForms(
            (succ) => {
                updateState(dispatch, {
                    allForms: succ.data,
                });
            },
            (fail) => {
                console.log("================", fail);
            }
        );
    };
    const handleCurrentVersion = (data) => {
        let obj = {
            _id: data._id,
            formId: data.formId,
            version: data.version
        };
        markCurrentVersion(
            obj,
            (succ) => {
                handleGetForms();
            },
            (fail) => {
                console.log(succ);
            }
        );
    }

    return (
        <CustomDialog
            title="Previous Form Versions"
            dialogProps={{
                open: open,
                onClose: closeAction,
                PaperProps: {
                    style: { maxWidth: 480 },
                },
                disableBackdropClick: true,
                disableEscapeKeyDown: true,
            }}>
            <div className={classes.contentCnt}>
                <div className={classes.searchBarCnt}>
                    <DefaultTextField
                        formControlStyles={{ marginBottom: 0 }}
                        defaultProps={{
                            id: "formVersionId",
                            onChange: (e) => handleUpdateSearch(e.target.value),
                            value: searchInput,
                            placeholder: "Search Versions",
                            autoFocus: true,
                            style: { paddingLeft: 6 },
                            inputProps: { style: { padding: "9px 6px" } },
                            startAdornment: <SearchIcon className={classes.searchIcon} />,
                        }}
                    />
                </div>
                <Scrollbars
                    autoHeight
                    hideTracksWhenNotNeeded={true}
                    autoHeightMin={100}
                    autoHeightMax={500}>
                    {filteredItems.map((item, index) => {
                        return (
                            <div className={classes.existingFieldChildCnt} key={index}>
                                {editForm?._id === item._id ? (
                                    <Hotkeys keyName="enter" onKeyDown={handleFormNameBlur}>
                                        <DefaultTextField
                                            error={false}
                                            formControlStyles={{
                                                marginBottom: 0,
                                                minwidth: 100,
                                                paddingRight: 25,
                                            }}
                                            defaultProps={{
                                                type: "text",
                                                id: "formVersionTitle",
                                                placeholder: "Enter version name",
                                                value: editForm.versionName,
                                                autoFocus: true,
                                                onBlur: handleFormNameBlur,
                                                inputProps: { maxLength: 250 },
                                                onKeyDown: (e) => {
                                                    if (e.keyCode == 13) handleFormNameBlur();
                                                    if (e.keyCode == 27) setEditForm(null);
                                                },
                                                onChange: (event) => {
                                                    handleChangeFormName(event);
                                                },
                                            }}
                                        />
                                    </Hotkeys>
                                ) : (
                                    <>
                                        <span className={classes.existingFieldChildTitle} title={item.title}>
                                            {item.versionName || item.title} {getEditIcon(item)} <br />{" "}
                                            <span className={classes.versionTxt}>{`v ${item.version}`}</span>
                                            <br />
                                            <span style={{ fontSize: "11px" }}>
                                                {moment(item.createdDate).format("LLLL")}
                                            </span>
                                        </span>
                                        <div className={classes.btnContainer}>
                                            <Tooltip
                                                classes={{
                                                    tooltip: classes.tooltip,
                                                }}
                                                title={"Preview"}
                                                placement="bottom">
                                                <span
                                                    className={classes.previewIcon}
                                                    onClick={(e) => {
                                                        e.stopPropagation();
                                                        handlePreviewForm(item);
                                                    }}>
                                                    <PreviewIcon
                                                        className={classes.actionIcon}
                                                        style={{
                                                            fontSize: "22px",
                                                            color: theme.palette.background.blue,
                                                            marginRight: 6,
                                                        }}
                                                    />
                                                </span>
                                            </Tooltip>
                                            <Tooltip
                                                classes={{
                                                    tooltip: classes.tooltip,
                                                }}
                                                title={"Make this current version"}
                                                placement="bottom">
                                                <span
                                                    className={classes.previewIcon}
                                                    onClick={(e) => {
                                                        e.stopPropagation();
                                                        handleCurrentVersion(item);
                                                    }}>
                                                    <DefaultIcon
                                                        className={classes.actionIcon}
                                                        style={{
                                                            fontSize: "22px",
                                                            color: theme.palette.background.blue,
                                                            marginRight: 6,
                                                        }}
                                                    />
                                                </span>
                                            </Tooltip>
                                            <Tooltip
                                                classes={{
                                                    tooltip: classes.tooltip,
                                                }}
                                                title={"Delete"}
                                                placement="bottom">
                                                <span
                                                    className={classes.deletebtn}
                                                    onClick={(e) => {
                                                        e.stopPropagation();
                                                        handleDeleteForm(item);
                                                    }}>
                                                    <SvgIcon className={classes.deleteFieldIcon} viewBox="0 0 14 14.005">
                                                        <DeleteIcon />
                                                    </SvgIcon>
                                                </span>
                                            </Tooltip>
                                        </div>
                                    </>
                                )}
                            </div>
                        );
                    })}
                </Scrollbars>
            </div>
        </CustomDialog>
    );
}

VersionsDialog.defaultProps = {
    classes: {},
    theme: {},
    open: false,
    formId: "",
};

export default withStyles(styles, { withTheme: true })(VersionsDialog);
