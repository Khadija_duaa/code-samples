import { Grid } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import DropdownArrow from "@material-ui/icons/ArrowDropDown";
import LeftArrow from "@material-ui/icons/ArrowLeft";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import React, { useEffect, useReducer, useState } from "react";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
import CustomButton from "../../components/Buttons/CustomButton";
import DefaultButton from "../../components/Buttons/DefaultButton";
import IconButton from "../../components/Buttons/IconButton";
import CustomDialog from "../../components/Dialog/CustomDialog";
import DefaultTextField from "../../components/Form/TextField";
import {
  addNewForm,
  addNewVersion,
  getAllAssignedForms,
  getAllForms,
  getAllTemplatesForms,
  updateForm,
} from "../../redux/actions/forms";
import FormAnalytics from "../FormAnalytics";
import FormAnalyticsTable from "../FormAnalyticsTabular";
import AllFormsView from "./allForms.view";
import { updateState } from "./Context/actions";
import SurveyContext from "./Context/form.context";
import initialState from "./Context/initialState";
import reducer from "./Context/reducer";
import FormCreator from "./FormCreator/FormCreator";
import styles from "./forms.style";
import { initialEmptySurvey } from "./form_json.js";

function Surveys(params) {
  const { classes, theme, history } = params;
  const [state, dispatch] = useReducer(reducer, initialState);
  const [assignedToMe, setAssignedToMe] = useState(true);
  const [myForms, setMyForms] = useState(true);
  const [templateView, setTemplateView] = useState(true);
  const [currentView, setCurrentView] = useState("grid");
  const [open, setOpen] = useState(false);
  const [formVersionDetails, setFormVersionDetails] = useState(null);
  const [versionTitle, setVersionTitle] = useState("");

  const closeAction = () => {
    let obj = {
      ...formVersionDetails,
      versionName: "",
    };
    handleSaveFormVersion(obj);
    setOpen(false);
    setFormVersionDetails(null);
    setVersionTitle("");
  };
  const handleSaveVersionName = () => {
    let obj = {
      ...formVersionDetails,
      versionName: versionTitle,
    };
    handleSaveFormVersion(obj);
    setOpen(false);
    setFormVersionDetails(null);
    setVersionTitle("");
  };

  const handleChange = (event, nextView) => {
    if (nextView) {
      setCurrentView(nextView);
    }
  };

  const { view, selectedForm, allForms, assignedForms, allTemplates } = state;

  const handleViewChange = (value) => {
    updateState(dispatch, { view: value, selectedForm: initialEmptySurvey });
    if (value == "all-surveys") handleGetForms();
    // if (value == "new-survey") history.push("/form");
  };

  const handleSaveFormVersion = (formData) => {
    if (formData) {
      let obj = formData;
      addNewVersion(
        obj,
        (succ) => {
          updateState(dispatch, {
            selectedForm: obj,
          });
        },
        (fail) => {
          console.log("================", fail);
        }
      );
    }
  };

  const handleOpenView = (key) => {
    switch (key) {
      case "assignedToMe":
        setAssignedToMe(!assignedToMe);
        return;
        break;
      case "myForms":
        setMyForms(!myForms);
        return;
        break;
      case "templateView":
        setTemplateView(!templateView);
        return;
        break;

      default:
        break;
    }
  };

  const handleGetForms = () => {
    getAllForms(
      (succ) => {
        updateState(dispatch, {
          allForms: succ.data,
        });
      },
      (fail) => {
        console.log("================", fail);
      }
    );
  };
  const handleGetAssignedForms = () => {
    getAllAssignedForms(
      (succ) => {
        updateState(dispatch, {
          assignedForms: succ.data,
        });
      },
      (fail) => {
        console.log("================", fail);
      }
    );
  };
  const handleGetTemplateForms = () => {
    getAllTemplatesForms(
      (succ) => {
        updateState(dispatch, {
          allTemplates: succ.data,
        });
      },
      (fail) => {
        console.log("================", fail);
      }
    );
  };
  const handleChangeTitle = (e) => {
    setVersionTitle(e.target.value);
  };
  // update form
  const updatedFormData = (creator, saveNo, form) => {
    const {
      formSettings: { formVersion },
    } = form;
    if (formVersion) {
      /** if user have enabled its form versioning option */
      setOpen(true);
      setFormVersionDetails({
        ...form,
        // mode: '',
        // maxTimeToFinish: '',
        // maxTimeToFinishPage: '',
        // navigateToUrl: '', 
        ...creator.JSON,
      });
      // handleSaveFormVersion(creator, saveNo, form);
      return;
    }
    const updatedformList = allForms.map((item) => {
      if (item._id === form._id) return { ...form, ...creator.JSON };
      return item;
    });
    const dymmyForm = {
      _id: form._id,
      formSettings: form.formSettings,
      versionName: form.versionName,
      teamId: form.teamId,
      location: form.location,
      formId: form.formId,
      version: form.version,
      createdDate: form.createdDate,
      createdBy: form.createdBy,
      updatedBy: form.updatedBy,
      apiKey: form.apiKey,
      secretKey: form.secretKey,
      updatedDate: form.updatedDate,
      __v: form.__v
    }
    const updatedForm = {
      ...dymmyForm,
      // ...form,
      // mode: '',
      // maxTimeToFinish: '',
      // maxTimeToFinishPage: '',
      // navigateToUrl: '',
      ...creator.JSON
    };
    updateForm(
      updatedForm,
      (succ) => {
        updateState(dispatch, {
          allForms: updatedformList,
          selectedForm: updatedForm,
        });
      },
      (fail) => {
        console.log("================", fail);
      }
    );
  };
  // app new form here
  const addNewFormData = (creator, saveNo, form) => {
    if (form) {
      let obj = {
        ...form,
        ...creator.JSON,
        versionName: "",
      };
      if (obj.isTemplate) delete obj.isTemplate;
      addNewForm(
        obj,
        (succ) => {
          updateState(dispatch, {
            allForms: [...allForms, succ.data],
            selectedForm: succ.data,
          });
        },
        (fail) => {
          console.log("================", fail);
        }
      );
    }
  };
  // on complete form
  const handleComplete = (creator, saveNo, form, thisState) => {
    if (form._id) {
      if (form.isTemplate == undefined) {
        updatedFormData(creator, saveNo, form);
        return;
      }
      addNewFormData(creator, saveNo, form);
      return;
    }
    addNewFormData(creator, saveNo, form);
  };
  useEffect(() => {
    handleGetForms();
    handleGetAssignedForms();
    handleGetTemplateForms();
  }, []);

  return (
    <SurveyContext.Provider
      value={{
        dispatch,
        state,
      }}>
      <div className={classes.mainContainer}>
        {view == "all-surveys" && (
          <>
            <div className={classes.titleContainer}>
              <span className={classes.title}>Forms</span>
              <CustomButton
                onClick={() => handleViewChange("new-survey")}
                btnType="green"
                style={{
                  width: "auto",
                  height: 34,
                  whiteSpace: "nowrap",
                }}
                variant="contained"
                query={""}
                disabled={false}>
                New Form
              </CustomButton>
            </div>
            <div className={classes.panel} style={{ marginBottom: 20, padding: "0 12px 0px 12px" }}>
              <IconButton
                style={{ marginBottom: 5 }}
                btnType="transparent"
                onClick={(e) => handleOpenView("assignedToMe")}>
                {!assignedToMe ? (
                  <ArrowRightIcon htmlColor={theme.palette.secondary.medDark} />
                ) : (
                  <DropdownArrow htmlColor={theme.palette.secondary.medDark} />
                )}
              </IconButton>
              <span className={classes.boardOptTitle}>Assigned to me</span>
              {assignedToMe && (
                <AllFormsView
                  list={assignedForms}
                  permission={{
                    editPermission: false,
                    previewPermission: true,
                    deletePermission: false,
                    analyticsPermission: false,
                    exportPermission: false,
                    versionPermission: false
                  }}
                  view={"assignedToMe"}
                />
              )}
            </div>
            <div className={classes.panel} style={{ marginBottom: 20, padding: "0 12px 0px 12px" }}>
              <IconButton
                style={{ marginBottom: 5 }}
                btnType="transparent"
                onClick={(e) => handleOpenView("myForms")}>
                {!myForms ? (
                  <ArrowRightIcon htmlColor={theme.palette.secondary.medDark} />
                ) : (
                  <DropdownArrow htmlColor={theme.palette.secondary.medDark} />
                )}
              </IconButton>
              <span className={classes.boardOptTitle}>My Forms</span>
              {myForms && (
                <AllFormsView
                  list={allForms}
                  permission={{
                    editPermission: true,
                    previewPermission: true,
                    deletePermission: true,
                    analyticsPermission: true,
                    exportPermission: true,
                    versionPermission: true
                  }}
                  view={"myForms"}
                />
              )}
            </div>
            <div className={classes.panel} style={{ marginBottom: 20, padding: "0 12px 0px 12px" }}>
              <IconButton
                style={{ marginBottom: 5 }}
                btnType="transparent"
                onClick={(e) => handleOpenView("templateView")}>
                {!templateView ? (
                  <ArrowRightIcon htmlColor={theme.palette.secondary.medDark} />
                ) : (
                  <DropdownArrow htmlColor={theme.palette.secondary.medDark} />
                )}
              </IconButton>
              <span className={classes.boardOptTitle}>Templates</span>
              {templateView && (
                <AllFormsView
                  list={allTemplates}
                  permission={{
                    editPermission: false,
                    previewPermission: true,
                    deletePermission: false,
                    analyticsPermission: false,
                    exportPermission: false,
                    versionPermission: false
                  }}
                  view={"templateView"}
                />
              )}
            </div>
          </>
        )}
        {view == "new-survey" && (
          <>
            <div className={classes.titleContainer} onClick={() => handleViewChange("all-surveys")}>
              <LeftArrow
                htmlColor={theme.palette.secondary.dark}
                classes={{ root: classes.navigationArrow }}
              />
              <span className={classes.title}>Go Back</span>
            </div>
            <div className={classes.headingContainer}>
              <span className={classes.heading}>
                {Object.keys(selectedForm).length > 1 ? "Edit Form" : "Create new form"}
              </span>
            </div>
            <FormCreator json={selectedForm} onComplete={handleComplete} />
          </>
        )}
        {view == "survey-analytics" && (
          <>
            <div className={classes.titleContainer} onClick={() => handleViewChange("all-surveys")}>
              <LeftArrow
                htmlColor={theme.palette.secondary.dark}
                classes={{ root: classes.navigationArrow }}
              />
              <span className={classes.title}>Go Back</span>
            </div>
            <div className={classes.headingContainer}>
              <span className={classes.heading}>{selectedForm.title}</span>
              <span className={classes.subTitle}>{selectedForm.description}</span>
            </div>
            <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="center"
              classes={{ container: classes.dashboardHeader }}>
              <div className="flex_center_start_row">
                <div className={classes.toggleContainer}>
                  <ToggleButtonGroup
                    value={currentView}
                    exclusive
                    onChange={handleChange}
                    classes={{ root: classes.toggleBtnGroup }}>
                    <ToggleButton
                      value="grid"
                      classes={{
                        root: classes.toggleButton,
                        selected: classes.toggleButtonSelected,
                      }}>
                      <span>Grid View</span>
                    </ToggleButton>
                    <ToggleButton
                      value="table"
                      classes={{
                        root: classes.toggleButton,
                        selected: classes.toggleButtonSelected,
                      }}>
                      <span>Tabular View</span>
                    </ToggleButton>
                  </ToggleButtonGroup>
                </div>
              </div>
            </Grid>
            <div className={classes.boardContainer}>
              {currentView == "grid" && <FormAnalytics json={selectedForm} />}
              {currentView == "table" && <FormAnalyticsTable json={selectedForm} />}
            </div>
          </>
        )}
        {open && (
          <CustomDialog
            title={"Form Version Details"}
            dialogProps={{
              open: open,
              onClose: closeAction,
            }}>
            <div className={classes.dialogFormCnt} noValidate autoComplete="off">
              <DefaultTextField
                label={"Version Name"}
                defaultProps={{
                  type: "text",
                  id: "versionTitle",
                  placeholder: "Type version name (optional)",
                  value: versionTitle,
                  autoFocus: true,
                  inputProps: { maxLength: 80 },
                  onChange: handleChangeTitle,
                }}
              />
            </div>
            <Grid
              container
              direction="row"
              justify="flex-end"
              alignItems="center"
              classes={{ container: classes.dialogFormActionsCnt }}>
              <DefaultButton
                onClick={closeAction}
                text={"Close"}
                buttonType="Transparent"
                style={{ marginRight: 20 }}
              />
              <CustomButton
                onClick={(e) => handleSaveVersionName()}
                btnType="green"
                variant="contained"
                query={""}
                disabled={versionTitle == ""}>
                Save
              </CustomButton>
            </Grid>
          </CustomDialog>
        )}
      </div>
    </SurveyContext.Provider>
  );
}

Surveys.defaultProps = {
  classes: {},
  theme: {},
};

export default compose(withRouter, withStyles(styles, { withTheme: true }))(Surveys);
