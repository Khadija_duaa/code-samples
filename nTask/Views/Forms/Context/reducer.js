import { UPDATE_STATE } from "./constants";

function reducer(state, action) {
  switch (action.type) {
    case UPDATE_STATE:
      {
        return { ...state, ...action.payload };
      }
      break;
    default:
      return { ...state };
      break;
  }
}

export default reducer;
