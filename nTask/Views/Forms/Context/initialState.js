import { initialEmptySurvey } from "../form_json";

const initialState = {
  view: "all-surveys",
  allForms: [],
  selectedForm: initialEmptySurvey,
  assignedForms:[],
  allTemplates:[]
};

export default initialState;
