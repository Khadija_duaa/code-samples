import { UPDATE_STATE } from "./constants";

export const updateState = (dispatch, obj) => {
  dispatch({ type: UPDATE_STATE, payload: obj });
};
