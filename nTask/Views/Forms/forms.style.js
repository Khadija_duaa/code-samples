const documentsStyles = (theme) => ({
  mainContainer: {
    padding: "12px 0px",
    height: "100%",
  },
  title: {
    fontSize: "20px !important",
    fontWeight: theme.typography.fontWeightLarge,
    fontFamily: theme.typography.fontFamilyLato,
    color: theme.palette.common.black,
    width: "auto",
    display: "inline-block",
    margin: "10px 0px 15px 0px",
    marginRight: 15,
  },
  heading: {
    fontSize: "20px !important",
    fontWeight: theme.typography.fontWeightLarge,
    fontFamily: theme.typography.fontFamilyLato,
    margin: "5px 0px 10px 0px",
  },
  subTitle: {
    fontSize: "13px !important",
    fontWeight: theme.typography.fontWeightExtraLight,
    fontFamily: theme.typography.fontFamilyLato,
    color: theme.palette.common.black,
    width: "auto",
    marginBottom: 5,
  },
  titleContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    padding: "0 12px 0px 23px",
    maxWidth: 200,
    cursor: "pointer"
  },
  navigationArrow: {
    fontSize: "30px !important",
    marginBottom: 5,
  },
  headingContainer: {
    display: "flex",
    flexDirection: "column",
    padding: "0 37px",
  },
  boardsDashboardStyles: {},
  boardProjectItemInner: {
    padding: "12px",
    background: theme.palette.common.white,
    borderRadius: 4,
    display: "flex",
    flexDirection: "column",
    border: `1px solid ${theme.palette.border.grayLighter}`,
    position: "relative",
    cursor: "pointer",
    minHeight: "140px",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover !important",
    width: "100%",
    // minWidth: 276,
    "&:hover": {
      boxShadow: "0px 2px 6px 0px rgba(0,0,0,0.1)",
      border: `1px solid ${theme.palette.border.lightBorder}`,
      // background: "linear-gradient(rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)) !important",
    },
    "&:hover $boardIcon": {
      opacity: 1,
    },
    "&:hover $overlay": {
      opacity: 0.1,
      color: "#fff",
    },
  },
  projectNameCnt: {
    display: "flex",
  },
  boardProjectHeading: {
    textTransform: "unset",
    fontSize: "16px !important",
    color: "white",
    fontWeight: 500,
    fontFamily: theme.typography.fontFamilyLato,
    lineHeight: "1.2",
    marginBottom: 8,
  },
  boardProjectDescription: {
    fontSize: "13px !important",
    color: "white",
    fontWeight: 400,
    fontFamily: theme.typography.fontFamilyLato,
    lineHeight: "1.2",
  },
  boardContainer: {
    padding: 10,
  },
  boardOptTitle: {
    marginLeft: 2,
    color: "#292929",
    fontSize: "16px !important",
    fontWeight: 600,
    fontFamily: theme.typography.fontFamilyLato,
  },
  tooltip: {
    fontSize: "12px !important",
    backgroundColor: theme.palette.common.black,
    "& .chat-tooltip-body": {
      padding: "5px 15px 5px 5px",
      "& ul,li": {
        margin: 0,
        padding: 0,
        listStyleType: "none",
        color: theme.palette.common.white,
      },
    },
  },
  actionIcon: {
    fontSize: "16px !important",
  },
  boardIcon: {
    fontSize: "24px !important",
    position: "absolute",
    right: 13,
    opacity: 0,
    bottom: 10,
  },
  dashboardHeader: {
    // margin: "0 0 15px 0",
    padding: "10px 0px 0 37px",
    flexWrap: "nowrap",
  },
  toggleBtnGroup: {
    display: "flex",
    flexWrap: "nowrap",
    borderRadius: 4,
    background: theme.palette.common.white,
    // border: `1px solid ${theme.palette.border.lightBorder}`,
    boxShadow: "none",
    "& $toggleButtonSelected": {
      color: theme.palette.text.secondary,
      backgroundColor: "white",
      "&:after": {
        background: theme.palette.common.white,
      },
      "&:hover": {
        background: theme.palette.common.white,
      },
    },
  },
  groupedHorizontal:{
    borderLeft: `1px solid ${theme.palette.border.lightBorder} !important`
  }, 
  toggleButton: {
    height: "auto",
    padding: "4px 20px 5px",
    fontSize: "12px !important",
    fontWeight: theme.palette.fontWeightMedium,
    fontFamily: theme.typography.fontFamilyLato,
    textTransform: "capitalize",
    "&:hover": {
      background: theme.palette.common.white,
    },
    "&[value = 'grid']": {
      //  borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
      borderRight: `1px solid ${theme.palette.border.lightBorder}`,
    },
  },
  toggleButtonSelected: {},
  versionCnt: {
    color: "white",
    position: "absolute",
    left: 10,
    bottom: 10,
    fontWeight: theme.palette.fontWeightMedium,
    fontFamily: theme.typography.fontFamilyLato,
    // textDecoration: "underline"
  },
  contentCnt: {
    padding: 20,
    minHeight: 500,
  },
  searchBarCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    // borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    // marginBottom: 15,
    "& svg": {
      fontSize: "18px !important",
    },
  },
  searchIcon: {
    color: theme.palette.icon.gray300,
  },
  existingFieldChildCnt: {
    cursor: "pointer",
    // padding: "10px 0px",
    height: 66,
    alignItems: "center",
    color: "#333",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    "&:hover $btnContainer": {
      visibility: "visible",
    },
    "&:hover $editIconCnt": {
      display: "inline !important",
    },
  },
  existingFieldChildTitle: {
    fontWeight: theme.typography.fontWeightRegular,
    fontFamily: "Lato, sans-serif",
    fontSize: "14px !important",
    textTransform: "capitalize",
    color: "#171717",

    width: "250px",
    wordBreak: "break-word",
    lineHeight: "1.3"
  },
  btnContainer: {
    visibility: "hidden",
    display: "flex",
    alignItems: "center",
    paddingRight: 10
  },

  previewIcon: {
    cursor: "pointer",
    fontWeight: theme.typography.fontWeightRegular,
    fontFamily: "Lato, sans-serif",
    fontSize: "14px !important",
    textTransform: "capitalize",
    marginRight: 7,
    display: "flex",
    alignItems: "center",
  },
  editFieldIcon: {
    fontSize: "13px !important",
    marginRight: 5,
    color: theme.palette.background.btnBlue,
  },
  deletebtn: {
    cursor: "pointer",

    fontWeight: theme.typography.fontWeightRegular,
    fontFamily: "Lato, sans-serif",
    fontSize: "14px !important",
    textTransform: "capitalize",
    color: "#171717",
  },
  deleteFieldIcon: {
    fontSize: "16px !important",
    marginRight: 5,
    color: theme.palette.background.btnBlue,
  },
  versionTxt: {
    textTransform: "lowercase",
    color: theme.palette.background.blue,
  },
  dialogFormCnt: {
    padding: 20,
    paddingBottom: 0
  },
  dialogFormActionsCnt: {
    padding: 20
  },
  editIconCnt: {
    display: "none",
  },
  editIcon: {
    fontSize: "11px !important",
    marginTop: -4,
    marginLeft: 5,
  },
});

export default documentsStyles;
