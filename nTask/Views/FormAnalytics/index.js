import { withStyles } from "@material-ui/core/styles";
import React, { useEffect, useState } from "react";
import { VisualizationPanel } from "survey-analytics";
import "survey-analytics/survey.analytics.min.css";
import { Model } from "survey-core";
import { getFormResponseData } from "../../redux/actions/forms";
import analyticsStyles from "./styles";

function AnalyticsForm(params) {
  const { classes, theme, json } = params;

  const vizPanelOptions = {
    allowHideQuestions: false,
    // labelTruncateLength: 27,
  };

  const [survey, setSurvey] = useState(null);
  const [vizPanel, setVizPanel] = useState(null);

  if (!survey) {
    const survey = new Model(json);
    setSurvey(survey);
  }

  const generateAnalytics = (surveyResults) => {
    // console.log("=======survey.getAllQuestions()===", survey.getAllQuestions())
    console.log("=======surveyResults===", surveyResults)
    const vizPanel = new VisualizationPanel(
      survey.getAllQuestions(),
      surveyResults,
      vizPanelOptions
    );
    vizPanel.showHeader = false;
    setVizPanel(vizPanel);
  };

  useEffect(() => {
    if (!vizPanel && !!survey) {
      getFormResponseData(
        json.formId,
        (success) => {
          generateAnalytics(success.data);
        },
        (fail) => {
          generateAnalytics([]);
        }
      );
    }
  }, []);

  useEffect(() => {
    if (vizPanel) vizPanel.render(document.getElementById("surveyVizPanel"));
    return () => {
      //   document.getElementById("surveyVizPanel").innerHTML = "";
    };
  }, [vizPanel]);

  return <div id="surveyVizPanel" />;
}

AnalyticsForm.defaultProps = {
  classes: {},
  theme: {},
};

export default withStyles(analyticsStyles, { withTheme: true })(AnalyticsForm);
