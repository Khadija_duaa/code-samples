import React, { Component, Fragment } from "react";
import DefaultTextField from "../../components/Form/TextField";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { compose } from "redux";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import CustomAvatar from "../../components/Avatar/Avatar";
import teamDashboardStyles from "./style";
import { withStyles } from "@material-ui/core/styles";
import { FetchWorkspaceInfo } from "../../redux/actions/workspace";
import { FetchUserInfo } from "../../redux/actions/profile";
import { clearBoardsData } from "../../redux/actions/boards";
import Avatar from "@material-ui/core/Avatar";
import EmptyState from "../../components/EmptyStates/EmptyState";
import { calculateContentHeight } from "../../utils/common";
import CustomIconButton from "../../components/Buttons/CustomIconButton";
import AddIcon from "@material-ui/icons/Add";
import DefaultDialog from "../../components/Dialog/Dialog";
import AddWorkspaceForm from "../../components/Header/addWorkSpaceForm";
import Hotkeys from "react-hot-keys";
import { nTaskSignalConnectionOpen } from "../../utils/ntaskSignalR";
import { teamCanView } from "../../components/PlanPermission/PlanPermission";
import { clearAllAppliedFilters } from "../../redux/actions/appliedFilters";
import Truncate from "react-truncate";
import { FormattedMessage } from "react-intl";
import { getCustomField } from "../../redux/actions/customFields";
import { getTasks } from "../../redux/actions/tasks";
import { Scrollbars } from "react-custom-scrollbars";

class TeamDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = { newWorkspace: false, workspaceSearchValue: "" };
  }

  componentDidMount() {
    // $.connection.hub.stop();
  }

  handleDialogClose = () => this.setState({ newWorkspace: false });

  handleDialogOpen = () => this.setState({ newWorkspace: true });

  handleWorkspaceGridClick = workspaceId => event => {
    const {companyInfo} = this.props;
    this.props.showLoadingState();
    this.props.clearBoardsData(false);
    $.connection.hub.stop();
    this.props.FetchWorkspaceInfo(
      workspaceId,
      () => {
        /** fetching all user created custom fields */
        this.props.getTasks(null, null, ()=>{},()=>{});
        this.props.getCustomField(
          succ => {},
          fail => {}
        );
        this.props.FetchUserInfo(() => {
          nTaskSignalConnectionOpen(this.props.profileState.userId);
          if(companyInfo?.workspaceLandingPageUrl) {
            this.props.history.push(companyInfo.workspaceLandingPageUrl);
          } else {
            this.props.history.push("/tasks");
          }
          this.props.clearAllAppliedFilters(null);
          this.props.hideLoadingState();
        });
      },
      error => {
        this.props.FetchUserInfo(response => {
          let activeTeam = response.data.teams.find(t => {
            return t.companyId == response.data.activeTeam;
          });
          this.props.history.push(`/teams/${activeTeam.companyUrl}`);
          this.props.hideLoadingState();
        });
      }
    );
  };

  memberList = workspace => {
    const { profileState } = this.props;
    let membersObj = profileState.member.allMembers.filter(
      m =>
        workspace.members && workspace.members.indexOf(m.userId) >= 0 && !m.isDeleted && m.isActive
    );
    return membersObj;
  };

  GenerateList = workspace => {
    const { profileState } = this.props;
    let membersObj = this.memberList(workspace);
    return membersObj.slice(0, 3).map((Assignee, i) => {
      return (
        <li key={i} style={{ marginLeft: !i == 0 ? -5 : null }}>
          <CustomAvatar
            otherMember={{
              imageUrl: Assignee.imageUrl,
              fullName: Assignee.fullName,
              lastName: "",
              email: Assignee.email,
              isOnline: Assignee.isOnline,
              isOwner: Assignee.isOwner,
            }}
            size="xsmall"
            disableCard
          />
        </li>
      );
    });
  };
  onKeyDown = event => {
    this.setState({ newWorkspace: true });
  };
  handleWorkspaceChange = e => {
    this.setState({ workspaceSearchValue: e.target.value });
  };

  render() {
    const { classes, theme, profileState, workSpacePer, companyInfo } = this.props;
    const { newWorkspace } = this.state;
    const filterWorkspaces = profileState.workspace;
    const filterData = filterWorkspaces.filter(data =>
      data.teamName.toLowerCase().includes(this.state.workspaceSearchValue.toLowerCase())
    );
    const projectAccess = companyInfo?.project?.isHidden ? false : true;
    const issueAccess = companyInfo?.issue?.isHidden ? false : true;
    const riskAccess = companyInfo?.risk?.isHidden ? false : true;
    const taskAccess = companyInfo?.risk?.isHidden ? false : true;
    const taskLabelSingle = companyInfo?.task?.sName;
    const taskLabelMulti = companyInfo?.task?.pName;
    return (
      <>
        <Grid
          container
          direction="row"
          justify="space-between"
          alignItems="center"
          classes={{ container: classes.workspacesDashboardHeader }}>
          <Grid item xs={12}>
            <div className="flex_center_space_between_row">
              <div className="flex_start_start_row">
                <Typography variant="h4">
                  {" "}
                  <FormattedMessage id="workspace-settings.label4" defaultMessage="Workspaces" />
                </Typography>
                <Typography
                  variant="body2"
                  style={{ marginLeft: 5 }}>{`(${profileState.workspace.length})`}</Typography>
              </div>

              <DefaultTextField
                fullWidth={false}
                // errorState={forgotEmailError}
                error={false}
                // errorMessage={forgotEmailMessage}
                formControlStyles={{ width: 250, marginBottom: 0 }}
                defaultProps={{
                  id: "workspaceSearch",
                  onChange: this.handleWorkspaceChange,
                  value: this.state.workspaceSearchValue,
                  placeholder: "Search Workspaces",
                  autoFocus: true,
                  inputProps: { maxLength: 50 },
                }}
              />
            </div>
          </Grid>
        </Grid>
        <Scrollbars autoHide={true} autoHeight autoHeightMin={0} autoHeightMax={calculateContentHeight()}>

        <div className={classes.workspaceListCnt}>
          {newWorkspace && (
            <DefaultDialog
              title={
                <FormattedMessage
                  id="workspace-settings.create-workspace.label"
                  defaultMessage="Create Workspace"
                />
              }
              sucessBtnText={
                <FormattedMessage
                  id="workspace-settings.create-workspace.label"
                  defaultMessage="Create Workspace"
                />
              }
              open={newWorkspace}
              onClose={event => this.handleDialogClose(event, "newWorkspace")}>
              <AddWorkspaceForm closeDialog={this.handleDialogClose} />
            </DefaultDialog>
          )}
          {profileState.workspace && profileState.workspace.length > 0 ? (
            <Grid container spacing={2}>
              {true /**workSpacePer.createWorkspaceName && workSpacePer.createWorkspaceName.cando*/ && (
                <Grid item classes={{ item: classes.workspaceListItem }} xs={6} lg={4} xl={3}>
                  <div
                    className={classes.workspaceListItemInnerFirst}
                    style={{ height: "100%" }}
                    onClick={event => this.handleDialogOpen(event)}>
                    <CustomIconButton
                      onClick={event => this.handleDialogOpen(event)}
                      btnType="success"
                      style={{ marginTop: 67 }}>
                      <AddIcon htmlColor={theme.palette.common.white} />
                    </CustomIconButton>
                    <Typography variant="h5" className={classes.workspaceItemHeading}>
                      <FormattedMessage
                        id="workspace-settings.create-new-workspace.label"
                        defaultMessage="Create New Workspace"
                      />
                    </Typography>
                  </div>
                </Grid>
              )}

              {profileState.workspace &&
                filterData.map(w => {
                  return (
                    <Grid
                      item
                      classes={{ item: classes.workspaceListItem }}
                      xs={6}
                      lg={4}
                      xl={3}
                      onClick={w.isActive ? this.handleWorkspaceGridClick(w.teamId) : () => {}}>
                      <div className={classes.workspaceListItemInner}>
                        {/* {!w.isActive && (
                        <div className={classes.blockWorkspace}>
                          <span>Blocked</span>
                        </div>
                      )} */}
                        <CustomAvatar
                          otherWorkspace={{
                            teamName: w.teamName,
                            pictureUrl: w.pictureUrl,
                            baseUrl: w.imageBasePath,
                          }}
                          size="medium"
                        />
                        <div title={w.teamName} style={{ display: "flex" }}>
                          <Truncate
                            lines={2}
                            trimWhitespace={true}
                            width={225}
                            ellipsis={<span>...</span>}
                            className={`${classes.workspaceItemHeading} ${
                              !w.isActive ? classes.disableWorkspace : ""
                            }`}
                            style={{ textTransform: "unset" }}>
                            {w.teamName}
                          </Truncate>
                        </div>
                        {/* <Typography
                        variant="h5"
                        className={`${classes.workspaceItemHeading} ${
                          !w.isActive ? classes.disableWorkspace : ""
                        }`}
                      >
                        {w.teamName}
                      </Typography> */}
                        {/* <WorkspaceStatusDropdown /> */}
                        {!w.isActive ? (
                          <div className={classes.blockCnt}>
                            <span>
                              <FormattedMessage
                                id="common.action.block.label"
                                defaultMessage="Blocked"
                              />
                            </span>
                          </div>
                        ) : (
                          <div
                            className={`${classes.WorkspaceInfo} ${
                              !w.isActive ? classes.disableWorkspace : ""
                            }`}>
                            {teamCanView(
                              "projectAccess"
                            ) /** passing projectAccess param to function and getting permission */ && (
                              projectAccess && <div className={classes.WorkspaceInfoItem}>
                                <span className={classes.WorkspaceInfoItemLbl}>
                                  <FormattedMessage id="project.label" defaultMessage="Projects" />
                                </span>
                                <span className={classes.WorkspaceInfoItemCount}>
                                  {w.projects > 99 ? "99+" : String(w.projects).padStart(2, "0")}
                                </span>
                              </div>
                            )}

                            <div className={classes.WorkspaceInfoItem}>
                              <span className={classes.WorkspaceInfoItemLbl}>
                                {taskLabelMulti ? taskLabelMulti :
                                  <FormattedMessage id="task.label" defaultMessage="Tasks"/>}
                              </span>
                              <span className={classes.WorkspaceInfoItemCount}>
                                {w.tasks > 99 ? "99+" : String(w.tasks).padStart(2, "0")}
                              </span>
                            </div>
                            {issueAccess && <div className={classes.WorkspaceInfoItem}>
                              <span className={classes.WorkspaceInfoItemLbl}>
                                <FormattedMessage id="issue.label" defaultMessage="Issues" />
                              </span>
                              <span className={classes.WorkspaceInfoItemCount}>
                                {w.issues > 99 ? "99+" : String(w.issues).padStart(2, "0")}
                              </span>
                            </div>}

                            {teamCanView(
                              "riskAccess"
                            ) /** passing riskAccess param to function and getting permission */ && (
                              riskAccess && <div className={classes.WorkspaceInfoItem}>
                                <span className={classes.WorkspaceInfoItemLbl}>
                                  <FormattedMessage id="risk.label" defaultMessage="Risks" />
                                </span>
                                <span className={classes.WorkspaceInfoItemCount}>
                                  {w.risks > 99 ? "99+" : String(w.risks).padStart(2, "0")}
                                </span>
                              </div>
                            )}
                          </div>
                        )}
                        <div className={classes.memberListCnt}>
                          <Typography
                            variant="body2"
                            className={classes.fontSize}
                            style={{
                              fontSize: "10px",
                              marginRight: 5,
                              display: "flex",
                              justifyContent: "flex-end",
                            }}>
                            <FormattedMessage id="common.members.label" defaultMessage="Members:" />
                          </Typography>
                          <ul
                            className={`AssigneeAvatarList "${
                              !w.isActive ? classes.disableWorkspace : ""
                            }`}>
                            {this.GenerateList(w)}
                            {this.memberList(w).length > 3 ? (
                              <li>
                                <Avatar classes={{ root: classes.TotalAssignee }}>
                                  +{w.members.length - 3}
                                </Avatar>
                              </li>
                            ) : (
                              ""
                            )}
                          </ul>
                        </div>
                      </div>
                    </Grid>
                  );
                })}
            </Grid>
          ) : (
            <div className={classes.emptyTeamStateCnt} style={{ height: "100%" }}>
              <EmptyState
                screenType="newWorkspace"
                heading={
                  <FormattedMessage
                    id="common.create-first.workspace.label"
                    defaultMessage="Create your first workspace"
                  />
                }
                message={
                  <FormattedMessage
                    id="common.create-first.workspace.message"
                    defaultMessage='You do not have any workspaces yet. Press "Alt + W" or click on button below.'
                  />
                }
                button={true}
              />
            </div>
          )}
          {profileState.workspace && profileState.workspace.length ? null : (
            <Hotkeys keyName="alt+w" onKeyDown={this.onKeyDown} />
          )}
        </div>
        </Scrollbars>
      </>
    );
  }
}
const mapStateToProps = state => {
  return {
    profileState: state.profile.data,
    workSpacePer: state.workspacePermissions.data.workSpace,
    companyInfo: state.whiteLabelInfo.data,
  };
};

export default compose(
  withRouter,
  withStyles(teamDashboardStyles, {
    withTheme: true,
  }),
  connect(mapStateToProps, {
    FetchWorkspaceInfo,
    FetchUserInfo,
    clearAllAppliedFilters,
    clearBoardsData,
    getCustomField,
    getTasks
  })
)(TeamDashboard);
