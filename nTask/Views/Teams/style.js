const teamDashboardStyles = theme => ({
  workspaceListCnt: {
    marginTop: 10,
    padding: "0 51px 10px 51px",
    // overflowY: "auto",
  },
  fontSize: {
    fontSize: "10px !important"
  },
  workspaceListItemInnerFirst: {
    padding: "20px 20px 13px 20px",
    background: theme.palette.background.airy,
    borderRadius: 4,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    border: `2px dashed ${theme.palette.border.grayLighter}`,
    position: "relative",
    cursor: "pointer",
    "&:hover": {
      boxShadow: "0px 2px 6px 0px rgba(0,0,0,0.1)",
      background: theme.palette.common.white,
      border: `1px solid ${theme.palette.border.lightBorder}`,
    },
  },
  workspaceListItemInner: {
    padding: "20px 20px 13px 20px",
    background: theme.palette.common.white,
    borderRadius: 4,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    border: `1px solid ${theme.palette.border.grayLighter}`,
    position: "relative",
    cursor: "pointer",
    "&:hover": {
      boxShadow: "0px 2px 6px 0px rgba(0,0,0,0.1)",
      border: `1px solid ${theme.palette.border.lightBorder}`,
    },
  },
  workspaceItemHeading: {
    marginTop: 12,
    // marginBottom: 15,
    height: 36,
    textTransform: "capitalize",
    fontSize: "15px !important",
    textAlign: "center",
  },
  emptyTeamStateCnt: {
    padding: "0 30px 0 60px",
    flex: 1,
    display: "flex",
  },
  allWorkspacesList: {
    marginBottom: 10,
  },
  workspaceCount: {
    color: theme.palette.text.medGray,
    fontWeight: theme.typography.fontWeightLight,
  },
  memberListCnt: {
    borderTop: `1px solid ${theme.palette.border.extraLightBorder}`,
    display: "flex",
    justifyContent: "center",
    paddingTop: 10,
    width: "100%",
    alignItems: "center",
    marginTop: 15,
  },
  TotalAssignee: {
    background: theme.palette.secondary.lightBlue,
    color: theme.palette.secondary.main,
    width: 30,
    height: 30,
    fontSize: "11px !important",
  },
  blockCnt: {
    padding: "7px 9px 6px 9px",
    borderRadius: 4,
    fontSize: "12px !important",
    backgroundColor: theme.palette.background.danger,
    color: theme.palette.common.white,
    display: "flex",
    marginTop: 24,
    justifyContent: "center",
  },
  WorkspaceInfo: {
    display: "flex",
    width: "100%",
    justifyContent: "center",
    // paddingBottom: 18,
    marginTop: 15,
  },
  WorkspaceInfoItem: {
    display: "flex",
    flexDirection: "column",
    alignItems: 'center',
    "&:not(:first-child)": {
      marginLeft: 25,
    },
  },
  WorkspaceInfoItemLbl: {
    fontSize: "12px !important",
    color: theme.palette.text.light,
  },
  WorkspaceInfoItemCount: {
    fontSize: "18px !important",
    fontWeight: theme.typography.fontWeightMedium,
    marginTop: 2,
  },
  disableWorkspace: {
    pointerEvents: "none",
    opacity: 0.5,
  },
  workspacesDashboardHeader: {
    // margin: "0 0 15px 0",
    marginTop: 20,
    padding: "0 51px 0 51px",
    // padding: "12px 20px 10px 62px"
  },
});

export default teamDashboardStyles;
