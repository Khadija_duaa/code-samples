import React, { useContext, useEffect, useState, useLayoutEffect } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import CustomTable from "../../../components/CustomTable/CustomTable.cmp";
import TaskOverviewContext from "../Context/taskOverview.context";
import {
  dispatchTaskGroupByAssignee,
  dispatchWidgetSetting,
  dispatchTaskWidgetData,
  dispatchTask,
  dispatchLoading,
} from "../Context/actions";
import { taskDetailDialogState } from "../../../redux/actions/allDialogs";
import { getWidgetSetting, getWidget } from "../../../redux/actions/overviews";
import { dispatchProjects } from "../../../redux/actions/projects";
import { dispatchTasks } from "../../../redux/actions/tasks";
import { dispatchIssues } from "../../../redux/actions/issues";
import { dispatchRisks } from "../../../redux/actions/risks";
import TaskListReport from '../TasksReport.view';

function TaskList(props) {
  const {
    state: { widgetSettings, taskWidgetData, filteredTasks, groupedTasks, selectedTask },
    dispatch,
  } = useContext(TaskOverviewContext);
  //Getting selected columns
  const { selectedColumns } = widgetSettings;
  const [taskData, setTaskData] = useState([]);
  const [scrollPosition, setScrollPosition] = useState(null);
  const {
    taskDetailDialogState,
    dispatchProjects,
    dispatchTasks,
    dispatchIssues,
    dispatchRisks,
  } = props;
  const handleSetScrollPosition = val => {
    setScrollPosition(val);
  };
  //Getting task data based upon filter
  const getTasksData = () => {
    const { selectedFilter } = widgetSettings;
    if (groupedTasks.length) {
      return groupedTasks;
    }
    if (selectedFilter === "viewAllTasks") {
      return taskWidgetData.userTasks;
    }
    return filteredTasks[selectedFilter];
  };
  useEffect(() => {
    setTaskData(getTasksData());
  }, [groupedTasks]);

  const closeTaskDetailsPopUp = () => {
    dispatchTask(dispatch, null);
    dispatchLoading(dispatch, true);
    taskDetailDialogState(null, { workspaceStatus: null, permissionObject: null, isTaskOverview: false });
    setTimeout(() => {
      getWidgetSetting(
        "Task",
        //success
        res => {
          dispatchWidgetSetting(dispatch, res);
        }
      );
      getWidget(
        "Task",
        //Success
        res => {
          dispatchTaskWidgetData(dispatch, res.data.entity);
          dispatchProjects(res.data.entity.projects);
          dispatchTasks(res.data.entity.taskDetails);
          dispatchIssues(res.data.entity.issues);
          dispatchRisks(res.data.entity.risks);
          setTimeout(() => {
            let target = document.querySelector("#taskOverViewTable");
            let child = target.querySelector("#taskOverViewTable > div");
            child.scrollTop = scrollPosition;
            dispatchLoading(dispatch, false);
          }, 100);
        },
        err => {
          setTimeout(() => {
            let target = document.querySelector("#taskOverViewTable");
            let child = target.querySelector("#taskOverViewTable > div");
            child.scrollTop = scrollPosition;
            dispatchLoading(dispatch, false);
          }, 100);
          dispatchTaskWidgetData(dispatch, {
            userTasks: [],
            issues: [],
            risks: [],
            taskDetails: [],
            permissions: [],
            projects: [],
            templates: [],
            customFields:[]
          });
        }
      );
    }, 1000);
  };

  useEffect(() => {
    const { selectedFilter } = widgetSettings;
    if (widgetSettings.groupBy.includes("assignee")) {
      const tasks = selectedFilter == "viewAllTasks" ? taskWidgetData.userTasks : filteredTasks[selectedFilter]
      dispatchTaskGroupByAssignee(dispatch, tasks);
    } else {
      setTaskData(getTasksData());
    }
  }, [taskWidgetData.userTasks, widgetSettings.selectedFilter, widgetSettings.filteredTasks]);

  return (
    <TaskListReport />
  )
  // return (
  //   <>
  //   <CustomTable
  //     data={taskData}
  //     cols={selectedColumns}
  //     dispatch={dispatch}
  //     widgetSettings={widgetSettings}
  //     isLoading={props.isLoading}
  //     handleSetScrollPosition={handleSetScrollPosition}
  //   />
  //   {selectedTask &&
  //     taskDetailDialogState(null,{
  //       id: selectedTask.taskId,
  //       afterCloseCallBack: () => {
  //         closeTaskDetailsPopUp();
  //       },
  //       type: "comment",
  //       workspaceStatus: selectedTask.template,
  //       permissionObject: selectedTask.permissionObject,
  //       isTaskOverview: true
  //     })}
  // </>
  // );
}

export default compose(
  connect(null, {
    taskDetailDialogState,
    dispatchProjects,
    dispatchTasks,
    dispatchIssues,
    dispatchRisks,
  })
)(TaskList);
