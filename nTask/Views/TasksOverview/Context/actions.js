import {
  SET_WIDGET_SETTINGS,
  SET_GROUPBY_ASSIGNEE,
  SET_WIDGET_TASK_DATA,
  SET_SELECTED_TASK,
  SET_OPENED_GROUP,
  SET_LOADING,
  SET_ALL_COLUMNS,
  UPDATE_TASK_DATA
} from "./constants";

export const dispatchWidgetSetting = (dispatch, obj) => {
  dispatch({ type: SET_WIDGET_SETTINGS, payload: obj });
};
export const dispatchTaskWidgetData = (dispatch, obj) => {
  dispatch({ type: SET_WIDGET_TASK_DATA, payload: obj });
};
export const dispatchTaskGroupByAssignee = (dispatch, data) => {
  dispatch({ type: SET_GROUPBY_ASSIGNEE, payload: data });
};
export const dispatchTask = (dispatch, data) => {
  dispatch({ type: SET_SELECTED_TASK, payload: data });
};
export const dispatchOpenedGroup = (dispatch, data) => {
  dispatch({ type: SET_OPENED_GROUP, payload: data });
};
export const dispatchLoading = (dispatch, data) => {
  dispatch({ type: SET_LOADING, payload: data });
};

export const dispatchColumns = (dispatch, data) => {
  dispatch({ type: SET_ALL_COLUMNS, payload: data });
};

export const dispatchUpdateTask = (dispatch, data) => {
  dispatch({ type: UPDATE_TASK_DATA, payload: data });
};