import moment from "moment";
import {
  SET_WIDGET_SETTINGS,
  SET_OPENED_GROUP,
  SET_WIDGET_TASK_DATA,
  SET_GROUPBY_ASSIGNEE,
  SET_SELECTED_TASK,
  SET_LOADING,
  SET_ALL_COLUMNS,
  UPDATE_TASK_DATA
} from "./constants";
import { store } from "../../../index";
import cloneDeep from "lodash/cloneDeep";

const formatDate = date => {
  return moment(date).format("MMM DD, YY");
};
function reducer(state, action) {
  switch (action.type) {
    case SET_WIDGET_SETTINGS: {
      const updatedState = { ...state.widgetSettings, ...action.payload };

      return { ...state, widgetSettings: updatedState };
    }
    case SET_GROUPBY_ASSIGNEE: {
      const storeState = store.getState();
      const newTaskList = [];
      action.payload.forEach(t => {
        if (t.assigneeList && t.assigneeList.length) {
          t.assigneeList.forEach(a => {
            const assignee = storeState.profile.data.teamMember.find(m => m.userId === a);
            const task = { ...t, assignee: assignee.fullName };
            newTaskList.push(task);
          });
        } else {
          const task = { ...t, assignee: "Others" };
          newTaskList.push(task);
        }
      });
      return { ...state, groupedTasks: newTaskList };
    }
    case SET_WIDGET_TASK_DATA: {
      const storeState = store.getState();
      const filteredTasks = {
        assignedToMe: [],
        dueToday: [],
        dueInFiveDays: [],
        overDueTasks: [],
        unscheduledTasks: [],
      };
      const issues = {};
      const risks = {};
      const today = new Date();
      // Filter linked issues
      action.payload.issues.forEach(i => {
        if (i.linkedTasks && i.linkedTasks.length > 0) {
          i.linkedTasks.forEach(l => {
            if (issues[l]) {
              issues[l] = [...issues[l], i];
            } else {
              issues[l] = [i];
            }
          });
        }
      });
      // Filter linked risks
      action.payload.risks.forEach(r => {
        if (r.linkedTasks && r.linkedTasks.length > 0) {
          r.linkedTasks.forEach(l => {
            if (risks[l]) {
              risks[l] = [...risks[l], r];
            } else {
              risks[l] = [r];
            }
          });
        }
      });
      // action.payload.userTasks.forEach(t => {
      //   // Formating values to be used in list view later on
      //   // Formating in reducer to avoid formating on each render
      //   t.startDate = t.startDate && formatDate(t.startDate);
      //   t.dueDate = t.dueDate && formatDate(t.dueDate);
      //   t.actualStartDate = t.actualStartDate && formatDate(t.actualStartDate);
      //   t.actualDueDate = t.actualDueDate && formatDate(t.actualDueDate);
      //   t.createdDate = t.createdDate && formatDate(t.createdDate);
      //   t.issues = issues[t.taskId] ? issues[t.taskId].length : 0;
      //   t.risks = risks[t.taskId] ? risks[t.taskId].length : 0;

      //   if (t.assigneeList.includes(storeState.profile.data.userId)) {
      //     filteredTasks.assignedToMe.push(t);
      //   }
      //   if (t.status !== 5 && t.status !== 4) {
      //     if (t.dueDate && moment(t.dueDate).isSame(today, "day")) {
      //       filteredTasks.dueToday.push(t);
      //     }
      //     if (
      //       t.dueDate &&
      //       moment(t.dueDate).diff(today, "day") <= 4 &&
      //       moment(t.dueDate).diff(today, "day") >= 0
      //     ) {
      //       filteredTasks.dueInFiveDays.push(t);
      //     }
      //     if (
      //       t.dueDate &&
      //       moment(t.dueDate).isBefore(today, "day") &&
      //       t.status !== 0 && t.status !== 3
      //     ) {
      //       filteredTasks.overDueTasks.push(t);
      //     }
      //     if (!t.dueDate && !t.startDate) {
      //       filteredTasks.unscheduledTasks.push(t);
      //     }
      //   }
      // });
      return { ...state, taskWidgetData: { ...action.payload, userTasks: action.payload.userTasks }, filteredTasks };
    }
    case SET_SELECTED_TASK: {
      return { ...state, selectedTask: action.payload };
    }
    case SET_OPENED_GROUP: {
      return { ...state, openedGroup: action.payload };
    }
    case SET_LOADING: {
      return { ...state, loadingState: action.payload };
    }
    case SET_ALL_COLUMNS: {
      const customColumns = action.payload.filter(
        column =>
          !column.isSystem &&
          column.fieldType !== "textarea" &&
          column.fieldType !== "section"
      );
      const allColumnUpdated = action.payload.filter(
        column => column.fieldType !== "textarea" && column.fieldType !== "section"
      );
      window.taskoverviewColumns = allColumnUpdated;
      return { ...state, allColumns: allColumnUpdated, customColumns };
    }

    case UPDATE_TASK_DATA: {
      let stateData = cloneDeep(state);
      stateData.taskWidgetData.userTasks = stateData.taskWidgetData.userTasks.map(item => {
        if (item.taskId === action.payload.taskId) {
          const { actualDueDate, actualStartDate, assigneeList, createdDate, customFieldData, dueDate, parentId, priority, progress, startDate,
            status, statusId, statusColor, statusTitle, taskTitle, timeLogged, totalAttachment, updatedDate, project } = action.payload;
          return {
            ...item, actualDueDate, actualStartDate, assigneeList, createdDate, customFieldData, dueDate, parentId, priority, progress, startDate,
            status, statusId, statusColor, statusTitle, taskTitle, timeLogged, totalAttachment, updatedDate, project, projectName: project ? project.projectName : ''
          };
        }
        else return item;
      });
      stateData.taskWidgetData.taskDetails = stateData.taskWidgetData.taskDetails.map(item => {
        if (item.taskId === action.payload.taskId) {
          return { ...item, ...action.payload };
        }
        else return item;
      });
      return stateData;
    }
      break;

    default:
      throw new Error();
  }
}

export default reducer;
