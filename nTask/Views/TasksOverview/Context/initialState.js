const initialState = {
  widgetSettings: {
    selectedFilter: "viewAllTasks",
    groupBy: [],
    selectedWorkspaces: [],
    subTasks: false,
    selectedColumns: ["taskTitle", "status", "plannedEndDate", "priority", "progress", "assignee"],
  },
  taskWidgetData: {
    userTasks: [],
    issues: [],
    risks: [],
    taskDetails: [],
    permissions: [],
    projects: [],
    templates: [],
    templateUsedStatuses:[],
    customFields:[]
  },
  filteredTasks: {
    assignedToMe: [],
    dueToday: [],
    dueInFiveDays: [],
    overDueTasks: [],
    unscheduledTasks: [],
  },
  groupedTasks: [],
  selectedTask: null,
  openedGroup: [],
  loadingState: null,
  allColumns: [],
  customColumns: [],
};

export default initialState;