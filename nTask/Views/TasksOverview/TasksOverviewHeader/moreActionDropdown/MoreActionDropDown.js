// @flow

import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import MoreHorizontalIcon from "@material-ui/icons/MoreHoriz";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import CustomIconButton from "../../../../components/Buttons/IconButton";
import DropdownMenu from "../../../../components/Dropdown/DropdownMenu";
import dropdownStyles from "./moreActionDropdown.style";

// MoreActionDropDown Main Component
function MoreActionDropDown(props) {
  const { classes, theme, handleOptionSelect, style } = props;
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = event => {
    // Function Opens the dropdown
    event.stopPropagation();
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };
  const handleClose = event => {
    // Function closes dropdown
    setAnchorEl(null);
  };
  const handleItemClick = (event, option) => {
    // Fuction responsible for selecting item from the list
    handleOptionSelect(option); // Callback on item select used to lift up option to parent if needed

    if (option == "color") {
      /** if option is color then drop down will be open and color control will open */
    } else {
      setAnchorEl(null);
    }
  };

  const open = Boolean(anchorEl);

  return (
    <>
      <CustomIconButton
        btnType="filledWhite"
        style={{ padding: '0 7px', height: 30, overflow: "hidden", ...style }}
        onClick={handleClick}
        buttonRef={anchorEl}>
        <MoreHorizontalIcon htmlColor={theme.palette.secondary.medDark} />
      </CustomIconButton>
      <DropdownMenu
        open={open}
        closeAction={handleClose}
        anchorEl={anchorEl}
        size="small"
        placement="bottom-start">
        {/* <Scrollbars autoHide autoHeight autoHeightMin={0} autoHeightMax={130}> */}
        <List>
          <ListItem
            button
            className={classes.listItem}
            onClick={event => {
              handleItemClick(event, "pinToTop");
            }}>
            Export
          </ListItem>
          <ListItem
            button
            className={classes.listItem}
            onClick={event => {
              handleItemClick(event, "muteConversation");
            }}>
            Share via Email
          </ListItem>
          <ListItem
            button
            className={classes.listItem}
            onClick={event => {
              handleItemClick(event, "leaveConversation");
            }}>
            Share Public Link
          </ListItem>
        </List>
        {/* </Scrollbars> */}
      </DropdownMenu>
    </>
  );
}
MoreActionDropDown.defaultProps = {
  classes: {},
  theme: {},
  handleOptionSelect: () => {},
  style: {},
};
export default compose(
  withRouter,
  withStyles(dropdownStyles, { withTheme: true })
)(MoreActionDropDown);
