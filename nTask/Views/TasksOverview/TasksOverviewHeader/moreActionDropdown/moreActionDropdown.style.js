const dropdownStyles = theme => ({
  listItem: {
    padding: "6px 14px",
    fontSize: "13px !important",
    color: theme.palette.text.primary,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 600,
    lineHeight: "normal",
  },

});

export default dropdownStyles;
