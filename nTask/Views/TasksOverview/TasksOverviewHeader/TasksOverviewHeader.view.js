import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import React, { useMemo, useEffect, useState, useContext } from "react";
import { connect, useDispatch } from "react-redux";
import { compose } from "redux";
import CustomSelectDropdown from "../../../components/Dropdown/CustomSelectDropdown/Dropdown";
import CustomMultiSelectDropdown from "../../../components/Dropdown/CustomMultiSelectWithSearchBarDropdown/Dropdown";
import {
  filtersData,
  generateWorkspaceData,
} from "../../../helper/generateTasksOverviewDropdownData";
import taskOverviewHeaderStyles from "./tasksOverviewHeader.style";
import DefaultSwitch from "../../../components/Form/Switch";
import MoreActionDropDown from "./moreActionDropdown/MoreActionDropDown";
import TaskOverviewContext from "../Context/taskOverview.context";
import { getWidget, updateWidgetSetting } from "../../../redux/actions/overviews";
import { dispatchTaskWidgetData, dispatchWidgetSetting, dispatchColumns } from "../Context/actions";
import { dispatchProjects } from "../../../redux/actions/projects";
import { dispatchTasks } from "../../../redux/actions/tasks";
import { dispatchIssues } from "../../../redux/actions/issues";
import { dispatchRisks } from "../../../redux/actions/risks";
import TeamColumnSelectionDropdown from "../../../components/Dropdown/SelectedItemsDropDown/teamColumnsDropdown";
import DefaultTextField from "../../../components/Form/TextField";
import { grid } from "../../../components/CustomTable2/gridInstance";
import debounce from "lodash/debounce";
import { doesFilterPass } from "../../../components/ReportingList/reportFilter.utils";
import CustomButton from "../../../components/Buttons/CustomButton";
import ImportExport from "@material-ui/icons/ImportExport";
import CustomIconButton from "../../../components/Buttons/IconButton";
import fileDownload from "js-file-download";
import { exportOverviewTasks } from "../../../redux/actions/reports";
import IconRefresh from "../../../components/Icons/IconRefresh";
import SvgIcon from "@material-ui/core/SvgIcon";
import CustomTooltip from "../../../components/Tooltip/Tooltip";
import { addPendingHandler } from "../../../redux/actions/backProcesses";

function TasksOverviewHeader(props) {
  const {
    classes,
    theme,
    profileState: { workspace },
    dispatchProjects,
    dispatchTasks,
    dispatchIssues,
    dispatchRisks,
    feature,
    handleIsLoading,
    groupType,
    tasks,
    isQuickFilterApplied,
    quickFilteredData = [],
  } = props;
  const {
    state: { widgetSettings, taskWidgetData, filteredTasks, allColumns },
    dispatch,
  } = useContext(TaskOverviewContext);
  const [btnQuery, setBtnQuery] = useState("");
  const [exportBtnQuery, setExportBtnQuery] = useState("");
  const [refreshBtnQuery, setRefreshBtnQuery] = useState("");
  const [disableRefreshBtn, setDisableRefreshBtn] = useState(false);
  const [disableExportBtn, setDisableExportBtn] = useState(false);
  const [refreshInterval, setRefreshInterval] = useState(30);
  const dispatchFn = useDispatch();

  // Handle workspace selection in dropdown
  const onWorkspaceSelect = options => {
    // setSelectedWorkspace(option);
    const workspaceIds = options.map(o => o.obj.teamId);
    dispatchWidgetSetting(dispatch, { selectedWorkspaces: workspaceIds });
  };
  const handleFetchData = () => {
    handleIsLoading(true);
    setBtnQuery("progress");
    updateWidgetSetting(
      "task",
      { ...widgetSettings },
      // Success
      res => {
        getWidget(
          "Task",
          //Success
          resp => {
            handleIsLoading(false);
            setBtnQuery("");
            dispatchTaskWidgetData(dispatch, resp.data.entity);
            dispatchProjects(resp.data.entity.projects);
            dispatchTasks(resp.data.entity.taskDetails);
            dispatchIssues(resp.data.entity.issues);
            dispatchRisks(resp.data.entity.risks);
          },
          err => {
            handleIsLoading(false);
            setBtnQuery("");
            dispatchTaskWidgetData(dispatch, {
              userTasks: [],
              issues: [],
              risks: [],
              taskDetails: [],
              permissions: [],
              projects: [],
              templates: [],
              customFields:[]
            });
          }
        );
      }
    );
  };
  // Handle Filter Select
  const handleFilterSelect = option => {
    dispatchWidgetSetting(dispatch, { selectedFilter: option.id });
    updateWidgetSetting(
      "task",
      { ...widgetSettings, selectedFilter: option.id },
      // Success
      res => { }
    );
  };
  // Handle subtasks switch check/uncheck
  const handleSubtaskSwitch = () => {
    setSubTaskChecked(prevValue => setSubTaskChecked(!prevValue));
  };
  // Generate workspace dropdown data
  const workspacesDropdownData = useMemo(() => {
    let activeWorkspaces = workspace.filter(w => w.isActive); /** only show active worksapces  */
    let wd = generateWorkspaceData(activeWorkspaces);
    // return [{ label: "Select All", value: "1", obj: { teamId: "1" } }, ...wd];
    return wd;
  }, workspace);

  const selectedFilterOption = filtersData.find(f => f.id === widgetSettings.selectedFilter);

  const selectedWorkspaceOptions = workspacesDropdownData.filter(w => {
    return widgetSettings.selectedWorkspaces.includes(w.obj.teamId);
  });
  const getTotalTasks = () => {
    const filteredTasks = isQuickFilterApplied
      ? quickFilteredData.filter(t => doesFilterPass({ data: t }, "taskoverview"))
      : taskWidgetData.userTasks.filter(t => doesFilterPass({ data: t }, "taskoverview"));
    return filteredTasks.length;
  };

  const updateColumnCallback = updatedColumns => {
    dispatchColumns(dispatch, updatedColumns);
  };

  const throttleHandleSearch = debounce(data => {
    grid.grid && grid.grid.setQuickFilter(data);
  }, 1000);

  //handle task search
  const handleSearch = e => {
    throttleHandleSearch(e.target.value);
  };
  const handleExportTasks = () => {

    // handleIsLoading(true);
    // setRefreshBtnQuery("progress");
    setDisableExportBtn(true);

    const obj = {
      type: 'tasks',
      apiType: 'post',
      data: [],
      fileName: 'tasks.xlsx',
      apiEndpoint: 'api/export/taskoverview',
    }
    addPendingHandler(obj, dispatchFn);
    let tempInterval = 30;
    const updateTimeInterval = setInterval(() => {
      tempInterval = tempInterval - 1; 
      setRefreshInterval(tempInterval);
      if (!tempInterval) {
        setDisableExportBtn(false);
        // console.log('destroy interval here ', tempInterval);
        clearInterval(updateTimeInterval);
      }
    }, 1000);
    // setExportBtnQuery('progress');
    // exportOverviewTasks(res => {
    //   fileDownload(res.data, "tasks.xlsx");
    //   setExportBtnQuery('');
    // }, fail => {
    //   setExportBtnQuery('');
    // });
  }
  // refresh grid function here
  const handleRefreshGrid = () => {
    handleIsLoading(true);
    setRefreshBtnQuery("progress");
    updateWidgetSetting(
      "task",
      { ...widgetSettings },
      // Success
      res => {
        getWidget(
          "Task",
          //Success
          resp => {
            setDisableRefreshBtn(true);
            handleIsLoading(false);
            setRefreshBtnQuery("");
            dispatchTaskWidgetData(dispatch, resp.data.entity);
            dispatchProjects(resp.data.entity.projects);
            dispatchTasks(resp.data.entity.taskDetails);
            dispatchIssues(resp.data.entity.issues);
            dispatchRisks(resp.data.entity.risks);
            let tempInterval = 30;
            const updateTimeInterval = setInterval(() => {
              tempInterval = tempInterval - 1; 
              setRefreshInterval(tempInterval);
              if (!tempInterval) {
                setDisableRefreshBtn(false);
                // console.log('destroy interval here ', tempInterval);
                clearInterval(updateTimeInterval);
              }
            }, 1000);
          },
          err => {
            handleIsLoading(false);
            setRefreshBtnQuery("");
            dispatchTaskWidgetData(dispatch, {
              userTasks: [],
              issues: [],
              risks: [],
              taskDetails: [],
              permissions: [],
              projects: [],
              templates: [],
              customFields:[]
            });
          }
        );
      }
    );
  }
  return (
    <div className={classes.headerCnt}>
      <div className={classes.headerLeftCnt}>
        <Typography variant="h1" className={classes.mainHeadingTasksReporting}>
          Tasks{" "}
          <span
            style={{
              color: "rgba(126, 126, 126, 1)",
              fontSize: "12px",
              fontWeight: 500,
              lineHeight: 1.5,
            }}>
            ({getTotalTasks()})
          </span>
        </Typography>

        <DefaultTextField
          fullWidth={false}
          // errorState={forgotEmailError}
          error={false}
          // errorMessage={forgotEmailMessage}
          formControlStyles={{ width: 250, marginBottom: 0, marginRight: 10 }}
          defaultProps={{
            id: "issueListSearch",
            onChange: handleSearch,
            placeholder: `Search ${groupType}`,
            autoFocus: true,
            inputProps: { maxLength: 150, style: { padding: "7px 14px" } },
          }}
        />
        <CustomSelectDropdown
          options={() => filtersData}
          option={selectedFilterOption}
          onSelect={handleFilterSelect}
          isDashboard={true}
          height="140px"
          scrollHeight={180}
          buttonProps={{
            variant: "contained",
            btnType: "white",
            labelAlign: "left",
            style: { minWidth: 140, padding: "3px 4px 3px 8px", marginRight: 8 },
          }}
        // disabled={!permission.detailsTabs.billingMethode.cando}
        />
        <CustomMultiSelectDropdown
          placeholder="Workspace"
          options={() => workspacesDropdownData}
          option={selectedWorkspaceOptions}
          onSelect={onWorkspaceSelect}
          scrollHeight={400}
          size={"large"}
          buttonProps={{
            variant: "contained",
            btnType: "white",
            labelAlign: "left",
            style: { minWidth: "170px", padding: "3px 4px 3px 8px", height: 31 },
          }}
        />
        <CustomButton
          btnType="blue"
          variant="contained"
          className={classes.customButtonBlue}
          onClick={handleFetchData}
          query={btnQuery}
          disabled={btnQuery == "progress"}>
          Apply
        </CustomButton>
        <div className={classes.importExportButtonCnt}>
          <CustomButton
            onClick={handleExportTasks}
            variant={'outlined'}
            btnType="gray"
            query={exportBtnQuery}
            disabled={disableExportBtn}
            style={{ padding: "5px 7px", height: 32,
            background: disableExportBtn ? theme.palette.border.lightBorder : 'transparent'
          }}
          >
            <ImportExport />
            <span className={classes.labelImportExport}>Export Tasks</span>
          </CustomButton>
        </div>
        <CustomTooltip
          helptext={
            disableRefreshBtn ? `Refresh will enables in ${refreshInterval} sec` : 'Refresh Grid'
          }
          placement="top"
          style={{ color: theme.palette.common.white }}>
          <CustomButton
            onClick={event => disableRefreshBtn ? () => { } : handleRefreshGrid()}
            query={refreshBtnQuery}
            style={{
              padding: "8px 8px",
              borderRadius: "4px",
              display: "flex",
              justifyContent: "space-between",
              minWidth: "auto",
              whiteSpace: "nowrap",
              marginLeft: 10,
              height: 30,
              background: disableRefreshBtn ? theme.palette.border.lightBorder : 'transparent'
            }}
            btnType={"white"}
            variant="contained">
            <SvgIcon
              viewBox="0 0 12 12.015"
              className={classes.qckFfilterIconRefresh}>
              <IconRefresh />
            </SvgIcon>
          </CustomButton>
        </CustomTooltip>
        <TeamColumnSelectionDropdown
          feature={feature}
          groupType={groupType}
          hideColumns={["matrix"]}
          allColumns={allColumns}
          updateColumn={updateColumnCallback}
          btnProps={{
            style: {
              border: "1px solid #dddddd",
              padding: "5px 10px",
              borderRadius: 4,
              marginLeft: 10,
            },
          }}
        />
        {/* <div className={classes.subTaskSwitchCnt}>
          <Typography variant="h6" className={classes.subTaskSwitchLabel}>
            Subtasks
          </Typography>

          <DefaultSwitch checked={subTaskChecked} onChange={handleSubtaskSwitch} size="small" />
        </div> */}
      </div>
      {/* <MoreActionDropDown /> */}
    </div>
  );
}
const mapStateToProps = state => {
  return {
    profileState: state.profile.data,
    tasks: state.tasks.data,
    reportingFilters: state.reportingFilters?.taskoverview,
  };
};
TasksOverviewHeader.defaultProps = {
  tasks: [],
};
export default compose(
  withStyles(taskOverviewHeaderStyles, { withTheme: true }),
  connect(mapStateToProps, {
    dispatchProjects,
    dispatchTasks,
    dispatchIssues,
    dispatchRisks
  })
)(TasksOverviewHeader);
