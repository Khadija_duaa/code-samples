const tasksOverviewStyles = theme => ({
  tasksOverview: {
    background: theme.palette.common.white,
    // border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: 8,
    width: "100%",
    height: "100%",
    "& *": {
      fontFamily: "'lato', sans-serif"   },
  },
});

export default tasksOverviewStyles;
