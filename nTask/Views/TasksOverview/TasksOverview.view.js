import React, { useReducer, useEffect, useState, useMemo } from "react";
import { compose } from "redux";
import { connect, useSelector, useDispatch } from "react-redux";
import withStyles from "@material-ui/core/styles/withStyles";
import TasksOverviewHeader from "./TasksOverviewHeader/TasksOverviewHeader.view";
import tasksOverviewStyles from "./tasksOverview.style";
import TaskOverviewContext from "./Context/taskOverview.context";
import reducer from "./Context/reducer";
import initialState from "./Context/initialState";
import { getWidgetSetting, getWidget } from "../../redux/actions/overviews";
import { dispatchWidgetSetting, dispatchTaskWidgetData, dispatchColumns, dispatchUpdateTask } from "./Context/actions";
import { dispatchProjects } from "../../redux/actions/projects";
import { dispatchTasks } from "../../redux/actions/tasks";
import { dispatchIssues } from "../../redux/actions/issues";
import { getTeamColumn } from "../../redux/actions/columns";
import { dispatchRisks } from "../../redux/actions/risks";
import RepostingList from "../../components/ReportingList/ReportingList.cmp";
import { taskDetailDialogState } from "../../redux/actions/allDialogs";
import CircularIcon from "@material-ui/icons/Brightness1";
import moment from "moment";
import { PopulateDefaultWSTemplate } from "../../redux/actions/workspace";
import { FetchWorkspaceInfo } from "../../redux/actions/workspace";
import { grid } from "../../components/CustomTable2/gridInstance";
import uniqBy from "lodash/uniqBy";
import isEmpty from "lodash/isEmpty";
import { isDateEqual } from "../../helper/dates/dates";
import Loader from "../../components/Loader/Loader";

function TasksOverview({
  classes,
  dispatchProjects,
  dispatchTasks,
  dispatchIssues,
  dispatchRisks,
}) {
  const [state, dispatch] = useReducer(reducer, initialState);
  const dispatchFn = useDispatch();
  const { profileState } = useSelector(state => {
    return {
      profileState: state.profile.data,
    };
  });
  const [isLoading, setIsLoading] = useState(true);
  const [TaskData, setTaskData] = useState([]);
  const handleIsLoading = value => {
    setIsLoading(value);
  };

  useEffect(() => {
    getWidgetSetting(
      "Task",
      //success
      res => {
        dispatchWidgetSetting(dispatch, res);
      }
    );
    getWidget(
      "Task",
      //Success
      res => {
        setIsLoading(false);
        dispatchTaskWidgetData(dispatch, res.data.entity);
        dispatchProjects(res.data.entity.projects);
        dispatchTasks(res.data.entity.taskDetails);
        dispatchIssues(res.data.entity.issues);
        dispatchRisks(res.data.entity.risks);
      },
      err => {
        setIsLoading(false);
        dispatchTaskWidgetData(dispatch, {
          userTasks: [],
          issues: [],
          risks: [],
          taskDetails: [],
          permissions: [],
          projects: [],
          templates: [],
          customFields: []
        });
      },
      dispatchFn
    );
    // all columns api from actions
    getTeamColumn("taskoverview", "task", res => {
      dispatchColumns(dispatch, res.data.entity);
    });
    return () => {
      dispatchProjects([]);
      dispatchTasks([]);
      dispatchIssues([]);
      dispatchRisks([]);
      if (!window.location.pathname.includes("/reports")) {
        FetchWorkspaceInfo(
          "",
          () => { },
          () => { },
          dispatchFn
        );
      }
      grid.grid = null;
    };
  }, []);
  const {
    widgetSettings,
    taskWidgetData,
    filteredTasks,
    groupedTasks,
    selectedTask,
    allColumns,
    customColumns,
  } = state;

  //Close task detail
  const closeTaskDetailsPopUp = (taskDetails) => {
    if (grid.grid) {
      let rowNode = grid.grid && grid.grid.getRowNode(taskDetails.id);
      const { actualDueDate, actualStartDate, assigneeList, createdDate, customFieldData, dueDate, parentId, priority, progress, startDate,
        status, statusColor, statusTitle, taskTitle, timeLogged, totalAttachment, updatedDate, project } = taskDetails;
      const updatedData = {
        ...rowNode.data, actualDueDate, actualStartDate, assigneeList, createdDate, customFieldData, dueDate, parentId, priority, progress, startDate,
        statusId: status, status, statusColor, statusTitle, taskTitle, timeLogged, totalAttachment, updatedDate, project,
        projectName: project ? project.projectName : ""
      };
      rowNode && rowNode.setData(updatedData);
      dispatchUpdateTask(dispatch, updatedData);
    }
  };
  const allTemplates = () => {
    let projectTemplates = taskWidgetData.projects.map(item => item.projectTemplate);
    projectTemplates = uniqBy(projectTemplates, "templateId");
    let finalArray = [...taskWidgetData.templates, ...projectTemplates];
    // finalArray = uniqBy(finalArray, "templateId");
    return finalArray;
  };
  //Open task details on tasks row click
  const handleRowClick = row => {
    const isRowClicked = row.event.target.closest("*[data-rowClick='cell']");
    if (isRowClicked) return;
    if (row.data && row.data.taskId && row.data.uniqueId !== "-") {
      let task = taskWidgetData.taskDetails.find(task => task.taskId == row.data.taskId) || {};
      if (!task.projectId) {
        const allTemplatesArray = allTemplates();
        let wstemplate = allTemplatesArray.find(item => item.workspace == task.teamId) || null;
        // wstemplate = wstemplate ? wstemplate : allTemplatesArray.find(item => !item.workspace || item.templateId == "109a903d40914c10b52935810bd56f31"); /** 109a903d40914c10b52935810bd56f31 is nTask Standard template id */
        PopulateDefaultWSTemplate(wstemplate, dispatchFn);
      }
      let permissionObject = taskWidgetData.permissions.find(
        p => p.workSpaceId === task.teamId
      ); /** workspace task permission */
      taskDetailDialogState(dispatchFn, {
        id: row.data.taskId,
        afterCloseCallBack: (taskDetails) => {
          closeTaskDetailsPopUp(taskDetails.current);
        },
        type: "comment",
        permissionObject: permissionObject,
      });
    }
  };
  const generateStatus = (statusArr, parentId = null, rootId = null) => {
    if (statusArr) {
      let allStatusArr = statusArr.map(item => {
        return {
          value: item.statusId,
          color: item.statusColor,
          icon: (
            <CircularIcon htmlColor={item.statusColor} style={{ fontSize: "12px !important", marginRight: 5 }} />
          ),
          label: item.statusTitle,
          isDoneState: item.isDoneState,
          parentId,
          rootId
        };
      });
      allStatusArr = allStatusArr.sort((a, b) => {
        let nameA = a.label.toLowerCase();
        let nameB = b.label.toLowerCase();
        if (nameA < nameB)
          //sort string ascending
          return -1;
        if (nameA > nameB) return 1;
        return 0; //default return value (no sorting)
      });
      // allStatusArr = allStatusArr.reduce((res, cv) => {
      //   let isExist = TaskData.some(item => item.templateId == parentId && item.statusTitle == cv.label)
      //   if (isExist) res.push(cv)
      //   return res;
      // }, [])
      return allStatusArr;
    } else return [];
  };
  // generate task status list from api data comes form context
  const generateStatusData = useMemo(() => {
    let templates = taskWidgetData.templates,
      projects = taskWidgetData.projects;
    if (templates.length) {
      let wsTemplate = templates.reduce((r, cv) => {
        let statusArr = cv.statusList.map(s => {
          return { ...s, workspaceId: cv.workspace };
        });
        r = [...r, ...statusArr];
        return r;
      }, []);
      let allProjectsStatusList = projects.reduce((res, cur) => {
        const exists = templates.some(el => el.templateId === cur.projectTemplateId);
        if (!exists) res = [...res, ...cur.projectTemplate.statusList];
        else res = [...res];
        return res;
      }, []);
      let finalArray = [...wsTemplate, ...allProjectsStatusList];
      finalArray = uniqBy(finalArray, "statusId");
      return generateStatus(finalArray);
    } else return [];
  }, [taskWidgetData, taskWidgetData.userTasks, taskWidgetData.taskDetails]);

  const taskStatusGroup = (() => {
    const { templateUsedStatuses = [] } = taskWidgetData;
    return templateUsedStatuses;
  })();

  const getTaskData = () => {
    const { selectedFilter } = widgetSettings;
    const today = new Date();

    switch (selectedFilter) {
      case "viewAllTasks":
        return taskWidgetData.userTasks;
        break;
      case "assignedToMe":
        return taskWidgetData.userTasks.filter(task =>
          task.assigneeList.includes(profileState.userId)
        );
        break;
      case "dueToday":
        {
          let updatedTasks = taskWidgetData.userTasks.reduce((res, cv) => {
            const taskStatus = generateStatusData.find(item => item.value == cv.statusId) || {};
            if (cv.dueDate
              &&
              isDateEqual(cv.dueDate, "today") && !taskStatus?.isDoneState &&
              (!cv.actualDueDate || moment(cv.actualDueDate).diff(cv.dueDate, "day") >= 1)
            ) {
              res.push(cv);
            }
            return res;
          }, []);
          return updatedTasks;
        }
        break;
      case "dueInFiveDays":
        {
          let updatedTasks = taskWidgetData.userTasks.reduce((res, cv) => {
            const taskStatus = generateStatusData.find(item => item.value == cv.statusId) || {};
            if (
              cv.dueDate &&
              !moment(cv.dueDate).isSame(today, "day") &&
              moment(cv.dueDate).diff(today, "day") <= 4 &&
              moment(cv.dueDate).diff(today, "day") >= 0 &&
              !taskStatus?.isDoneState && (!cv.actualDueDate || moment(cv.actualDueDate).diff(cv.dueDate, "day") >= 1)
            ) {
              res.push(cv);
            }
            return res;
          }, []);
          return updatedTasks;
        }
        break;
      case "unassignedTasks":
        let updatedTasks = taskWidgetData.userTasks.filter(t => isEmpty(t.assigneeList));
        return updatedTasks
        break;
      case "overDueTasks":
        {
          let updatedTasks = taskWidgetData.userTasks.reduce((res, cv) => {
            const taskStatus = generateStatusData.find(item => item.value == cv.statusId) || {};
            if (cv.dueDate && moment(cv.dueDate).isBefore(today, "day") && !taskStatus?.isDoneState &&
              (!cv.actualDueDate || moment(cv.actualDueDate).isAfter(today, "day"))) {
              res.push(cv);
            }
            return res;
          }, []);
          return updatedTasks;
        }
        break;
      case "unscheduledTasks":
        return taskWidgetData.userTasks.filter(task => !task.dueDate && !task.startDate);
        break;
      default:
        break;
    }
    setIsLoading(false);
    return filteredTasks[selectedFilter];
  };

  useEffect(() => {
    setTaskData(getTaskData());
    // generate filter status data for task overview
    // generateStatusData(taskWidgetData.templates, taskWidgetData.projects);
  }, [taskWidgetData.userTasks, taskWidgetData.taskDetails, widgetSettings.selectedFilter, widgetSettings.filteredTasks]);

  const columnChangeCallback = updatedColumns => {
    dispatchColumns(dispatch, updatedColumns);
  };
  const isQuickFilterApplied = widgetSettings.selectedFilter !== "viewAllTasks";

  return (
    <TaskOverviewContext.Provider value={{ dispatch, state }}>
      <div className={classes.tasksOverview}>
        {allColumns && allColumns.length ? (
          <TasksOverviewHeader
            feature={"taskoverview"}
            groupType={"task"}
            handleIsLoading={handleIsLoading}
            columnChangeCallback={columnChangeCallback}
            isQuickFilterApplied={isQuickFilterApplied}
            quickFilteredData={TaskData}
          />
        ) : null}
        {allColumns && allColumns.length && !isLoading ? (
          <>
            <RepostingList
              feature={"taskoverview"}
              exportConfig={{
                accessorKey: "taskIds",
                accessorId: "taskId",
                apiKey: "export/bulk/taskoverview",
                documentName: "tasks.xlsx",
              }}
              groupType={"task"}
              columns={allColumns}
              data={TaskData}
              columnChangeCallback={columnChangeCallback}
              customColumns={customColumns}
              handleRowClick={handleRowClick}
              emptyHeading={"No Task found"}
              emptyText={
                widgetSettings.selectedWorkspaces.length
                  ? "You do not have any task yet."
                  : "Please select a workspace."
              }
              headerProps={{ customStatusList: generateStatusData, taskStatusGroup }}
            />
          </>
        ) : (
          <Loader />
        )}
      </div>
    </TaskOverviewContext.Provider>
  );
}

export default compose(
  withStyles(tasksOverviewStyles, { withTheme: true }),
  connect(null, {
    dispatchProjects,
    dispatchTasks,
    dispatchIssues,
    dispatchRisks,
    getTeamColumn,
  })
)(TasksOverview);
