import React, { Fragment } from "react";
import ReactDataGrid from "react-data-grid";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { Circle } from "rc-progress";
import listStyles from "./styles";
import DefaultDialog from "../../../components/Dialog/Dialog";
import AddNewProject from "../../AddNewForms/AddNewProjectForm";
import Typography from "@material-ui/core/Typography";
import ReportsColumnDropdown from "./ReportsColumnDropdown";
import DefaultButton from "../../../components/Buttons/DefaultButton";
import MenuList from "../../../components/Menu/TaskMenus/MemberListMenu";
import SvgIcon from "@material-ui/core/SvgIcon";
import TaskIcon from "../../../components/Icons/TaskIcon";
import MeetingsIcon from "../../../components/Icons/MeetingIcon";
import IssuesIcon from "../../../components/Icons/IssueIcon";
import RiskIcon from "../../../components/Icons/RiskIcon";
import { calculateContentHeight } from "../../../utils/common";
import cloneDeep from "lodash/cloneDeep";

const Progress = (progress, object) => {
  return (
    <div style={{ display: "flex", alignItems: "center" }}>
      <div style={{ width: 30, marginRight: 5, marginTop: 4 }}>
        <Circle
          percent={progress}
          strokeWidth="10"
          trailWidth=""
          trailColor="#dedede"
          strokeColor="#30d56e"
        />
      </div>
      <Typography variant="h6" align="center">
        {progress}%
      </Typography>
    </div>
  );
};

let increment = 20;

class WorkspaceList extends React.Component {
  static defaultProps = { rowKey: "id" };
  constructor(props, context) {
    super(props, context);
    this.state = {
      _columns: [
        {
          key: "workSpace",
          name: "Workspace Name",
          cellClass: "firstColoumn",
          formatter: value => {
            return (
              <>
                <span className={this.props.classes.taskListTitleText}>
                  {value.row ? value.row.workSpace : ""}
                </span>
              </>
            );
          },
          columnOrder: 1
        },
        {
          key: "projects",
          name: "Projects",
          width: 100,
          formatter: value => {
            return (
              <DefaultButton buttonType="smallIconBtn">
                <SvgIcon
                  viewBox="0 0 18 15.188"
                  className={props.classes.smallBtnIcon}
                >
                  <TaskIcon />
                </SvgIcon>
                <Typography variant="body2" align="center">
                  {value.row && value.row.projects ? value.row.projects : 0}
                </Typography>
              </DefaultButton>
            );
          },
          cellClass: "dropDownCell",
          columnOrder: 2
        },
        {
          key: "tasks",
          name: "Tasks",
          width: 100,
          formatter: value => {
            return (
              <DefaultButton buttonType="smallIconBtn">
                <SvgIcon
                  viewBox="0 0 18 15.188"
                  className={props.classes.smallBtnIcon}
                >
                  <TaskIcon />
                </SvgIcon>
                <Typography variant="body2" align="center">
                  {value.row && value.row.tasks ? value.row.tasks : 0}
                </Typography>
              </DefaultButton>
            );
          },
          cellClass: "dropDownCell",
          columnOrder: 2
        },
        {
          key: "issues",
          name: "Issues",
          width: 100,
          formatter: value => {
            return (
              <DefaultButton buttonType="smallIconBtn">
                <SvgIcon
                  viewBox="0 0 16 17.375"
                  className={props.classes.smallBtnIcon}
                >
                  <IssuesIcon />
                </SvgIcon>
                <Typography variant="body2" align="center">
                  {value.row && value.row.issues ? value.row.issues : 0}
                </Typography>
              </DefaultButton>
            );
          },
          cellClass: "dropDownCell",
          columnOrder: 3
        },
        {
          key: "risks",
          name: "Risks",
          width: 100,
          formatter: value => {
            return (
              <DefaultButton buttonType="smallIconBtn">
                <RiskIcon classes={{ root: props.classes.smallBtnIcon }} />
                <SvgIcon
                  viewBox="0 0 18 15.75"
                  className={props.classes.smallBtnIcon}
                >
                  <RiskIcon />
                </SvgIcon>
                <Typography variant="body2" align="center">
                  {value.row && value.row.risks ? value.row.risks : 0}
                </Typography>
              </DefaultButton>
            );
          },
          cellClass: "dropDownCell",
          columnOrder: 4
        },
        {
          key: "meetings",
          name: "Meetings",
          width: 100,
          formatter: value => {
            return (
              <DefaultButton buttonType="smallIconBtn">
                <SvgIcon
                  viewBox="0 0 17.031 17"
                  className={props.classes.smallBtnIcon}
                >
                  <MeetingsIcon />
                </SvgIcon>
                <Typography variant="body2" align="center">
                  {value.row && value.row.meeting ? value.row.meeting : 0}
                </Typography>
              </DefaultButton>
            );
          },
          cellClass: "dropDownCell",
          columnOrder: 5
        },
        {
          key: "progress",
          name: "Progress",
          width: 155,
          formatter: value => {
            return Progress(
              Math.round(value.row ? value.row.progress : 0),
              this.state
            );
          },
          cellClass: "dropDownCell",
          columnOrder: 6
        },
        {
          key: "assignee",
          name: "Resources",
          width: 205,
          formatter: value => {
            let members = this.props.allMembers;

            let assigneeList = members.filter(m => {
              if (value.row.resources && value.row.resources.length >= 0)
                return value.row.resources.indexOf(m.userId) >= 0;
            });
            return (
              <MenuList
                id={value.row ? value.row.projectId : ""}
                assigneeList={assigneeList}
                MenuData={value.row}
                placementType="bottom-end"
                isUpdated={this.isUpdated}
                viewType="project"
                checkAssignee={true}
                listType="assignee"
              />
            );
          },
          cellClass: "dropDownCell lastColoumn",
          columnOrder: 7,
          sortable: false
        }
      ]
        .map(c => ({ sortable: true, ...c })),

      rows: [],
      output: "",
      addNewForm: false,
      userId: props.userId,
      value: "",
      progress: 0,
      issueId: "",
      isProgress: false,
      isReorder: false,
      startDateOpen: false,
      dueDateOpen: false,
      newCol: [],
      showProjects: false,
      currentTask: {},
      isWorkspaceChanged: false,
      isSorted: "NONE",
      showRecords: increment,
      totalRecords: 0,
      members: [],
      sortColumn: null,
      sortDirection: "NONE"
    };
    this.isUpdated = this.isUpdated.bind(this);
    this.selectedAction = this.selectedAction.bind(this);
    this.showProjectsPopUp = this.showProjectsPopUp.bind(this);
    this.handleScrollDown = this.handleScrollDown.bind(this);
    this.scrollListener = this.scrollListener.bind(this);
  }

  componentDidMount() {
    this.setFormattedProjectRows();
  }

  componentDidUpdate(prevProps, prevState) {
    const { sortColumn, sortDirection } = this.state;
    if (
      JSON.stringify(this.props.workspaceStats) !==
      JSON.stringify(prevProps.workspaceStats)
    ) {
      if (sortColumn)
        this.sortRows(this.setFormattedProjectRows(), sortColumn, sortDirection);
      else
        this.setFormattedProjectRows();
    }
  }

  componentWillUnmount() {
    if (this.canvas) {
      this.canvas.removeEventListener("scroll", this.scrollListener);
    }
  }

  setFormattedProjectRows = () => {
    let members = this.props.allMembers;
    const workspaceStats = this.props.workspaceStats || [];

    this.setState({
      members,
      rows: workspaceStats,
      totalRecords: workspaceStats.length,
      newCol: this.state._columns
    });

    return workspaceStats;
  };

  handleColorChange = (data, color) => { };


  scrollListener() {
    if (
      this.canvas.scrollHeight -
      (this.canvas.scrollTop + this.canvas.clientHeight) <
      1
    ) {
      this.handleScrollDown();
    }
  }

  selectedAction(selectedColumns, isFirstLoad) { }

  handleScrollDown = () => {
    this.setState({ showRecords: this.state.showRecords + increment }, () => {
      if (this.state.showRecords >= this.props.workspaceStats.length) {
        this.setState({ showRecords: this.props.workspaceStats.length });
      }
    });
  };

  showProjectsPopUp(e, data) {
    if (!this.props.isArchivedSelected && data && data.projectId) {
      this.props.projectClick(data);
    }
  }

  isUpdated(data) { }

  rowGetter = i => {
    if (i >= 0) return this.state.rows[i] ? this.state.rows[i] : {};
  };

  sortRows = (initialRows, sortColumn, sortDirection) => {
    const comparer = (a, b) => {
      if (typeof a[sortColumn] === "string") {
        let str1 = a[sortColumn].toLowerCase();
        let str2 = b[sortColumn].toLowerCase();
        if (sortDirection === "ASC") {
          return str1 > str2 ? 1 : -1;
        } else if (sortDirection === "DESC") {
          return str1 < str2 ? 1 : -1;
        }
      }

      if (sortDirection === "ASC") {
        return a[sortColumn] > b[sortColumn] ? 1 : -1;
      } else if (sortDirection === "DESC") {
        return a[sortColumn] < b[sortColumn] ? 1 : -1;
      }
    };

    if (sortDirection === "NONE")
      this.setState({ rows: initialRows, sortColumn: null, sortDirection });
    else
      this.setState({ rows: [...initialRows].sort(comparer), sortColumn, sortDirection });
  };

  render() {
    const { addNewForm } = this.state;
    const { reports } = this.props;
    let initialRows = this.props.workspaceStats || [];

    return (
      <Fragment>
        <DefaultDialog
          title="Select Project"
          open={addNewForm}
          onClose={this.cancelAddProject}
        >
          <AddNewProject closeAction={this.cancelAddProject} />
        </DefaultDialog>
        <ReportsColumnDropdown
        // columnSelectAction={this.handleReportsListColumn}
        // selectedColumns={reports.columnOrder.reportColumnOrder}
        />
        <div style={{ width: "100%" }}>
          <ReactDataGrid
            onRowClick={this.showProjectsPopUp}
            enableCellSelection={true}
            columns={this.state._columns.filter((col, i) => {
              return (
                this.props.reports.columnOrder.reportColumnOrder.indexOf(col.name) >
                -1 || col.name == "Workspace Name"
              );
            })}
            onGridSort={(sortColumn, sortDirection) =>
              this.sortRows(initialRows, sortColumn, sortDirection)
            }
            rowGetter={this.rowGetter}
            rowsCount={initialRows.length}
            rowHeight={60}
            headerRowHeight={50}
            minHeight={calculateContentHeight()}
          />
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    userId: state.profile.data.userId || "",
    allMembers: state.profile.data.member.allMembers || [],
    reports: state.reports
  };
};

export default compose(
  withRouter,
  withStyles(listStyles, { withTheme: true }),
  connect(mapStateToProps)
)(WorkspaceList);
