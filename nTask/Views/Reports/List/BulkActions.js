import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Button from "@material-ui/core/Button/";
import listStyles from "./styles";

import { BlockPicker } from "react-color";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Popper from "@material-ui/core/Popper";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import { BulkUpdateProject } from "../../../redux/actions/projects";
import { FetchWorkspaceInfo } from "../../../redux/actions/workspace";
import helper from "../../../helper";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";

// Bulk action types
//   Unknown = 0,
//   AddAssignee = 1,
//   AddProject = 2,
//   RemoveProject = 3,
//   StartDate = 4,
//   DueDate = 5,
//   Priority = 6,
//   Status = 7,
//   Color = 8,
//   Copy = 9,
//   CopyToWorkspace = 10,
//   Export = 11,
//   Archive = 12,
//   UnArchive = 13,
//   Delete = 14

// Bulk action types
//    public List < string > ProjectIds { get; set; }
//    public List < string > AssigneIds { get; set; }
//    public string WorkspaceId { get; set; }
//    public string ProjectId { get; set; }
//    public string PlanDate { get; set; }
//    public string ActualDate { get; set; }
//    public int Priority { get; set; }
//    public int Status { get; set; }
//    public string ColorCode { get; set; }
//    public UpdateProjectType Type { get; set; }

class BulkActions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDateOpen: false,
      dueDateOpen: false,
      selectedColor: "#D9E3F0",
      anchorEl: null,
      open: false,
      archiveBtnQuery: '',
      unarchiveBtnQuery: ''
    };

    this.handleStartDateClose = this.handleStartDateClose.bind(this);
    this.handleStartDateToggle = this.handleStartDateToggle.bind(this);
    this.handleDueDateClose = this.handleDueDateClose.bind(this);
    this.handleDueDateToggle = this.handleDueDateToggle.bind(this);
    this.handleColorPickerBtnClick = this.handleColorPickerBtnClick.bind(this);
    this.handleClickAway = this.handleClickAway.bind(this);
    this.showConfirmationPopup = this.showConfirmationPopup.bind(this);
    this.closeConfirmationPopup = this.closeConfirmationPopup.bind(this);
  }
  showConfirmationPopup(inputType) {
    this.setState({
      showConfirmation: true,
      inputType: inputType || ""
    });
  }

  closeConfirmationPopup() {
    this.setState({ showConfirmation: false });

  }

  handleStartDateClose(event) {
    this.setState({ startDateOpen: false });
  }
  handleStartDateToggle() {
    this.setState(state => ({ startDateOpen: !state.startDateOpen }));
  }
  handleDueDateClose(event) {
    this.setState({ dueDateOpen: false });
  }
  handleDueDateToggle() {
    this.setState(state => ({ dueDateOpen: !state.dueDateOpen }));
  }
  handleColorPickerBtnClick(event) {
    const { currentTarget } = event;
    this.setState(state => ({
      anchorEl: currentTarget,
      open: !state.open
    }));
  }
  handleClickAway() {
    this.setState({ open: false }, () => {
      if (this.state.selectedColor !== this.state.prevColor) {
        const type = helper.PROJECT_BULK_TYPES("Color");
        let data = {
          type,
          projectIds: this.props.selectedIdsList
        };
        data["colorCode"] = this.state.selectedColor;

        this.setState({ prevColor: this.state.selectedColor }, () => {
          this.props.BulkUpdateProject(data, () => {
            this.FetchWorkSpace();
          })
        })
      }
    });
    this.setState({ open: false });
  }
  FetchWorkSpace = () => {
    this.props.FetchWorkspaceInfo("", () => { });
  };
  handleBulkClick = type => {
    this.props.BulkUpdateProject(
      {
        type,
        projectIds: this.props.selectedIdsList
      },
      () => {
        this.FetchWorkSpace();
      }
    );
  };
  handleArchive = (e) => {
    if (e)
      e.stopPropagation()
    let type = this.state.inputType;
    this.setState({ archiveBtnQuery: 'progress' }, () => {
      this.props.BulkUpdateProject({ type, projectIds: this.props.selectedIdsList },
        () => {
          this.props.FetchWorkspaceInfo("", () => {
            this.setState({ archiveBtnQuery: '', inputType: "", showConfirmation: false });
          });
        }
      );
    });
  }
  handleUnArchive = (e) => {
    if (e)
      e.stopPropagation()
    let type = 13;
    this.setState({ unarchiveBtnQuery: 'progress' }, () => {
      this.props.BulkUpdateProject({ type, projectIds: this.props.selectedIdsList },
        () => {
          this.props.handleUnarchive();
          this.props.FetchWorkspaceInfo("", () => {
            this.setState({ unarchiveBtnQuery: '', inputType: "", showConfirmation: false });
          });
        }
      );
    });
  }
  render() {
    const { classes, theme, selectedIdsList, isArchivedSelected } = this.props;
    const {
      dueDateOpen,
      startDateOpen,
      selectedColor,
      anchorEl,
      open,
      inputType,
      showConfirmation,
      archiveBtnQuery,
      unarchiveBtnQuery
    } = this.state;
    const id = open ? "simple-popper" : null;
    const styles = (action, i, arr) => {
      return i == 0
        ? { borderRadius: "4px 0 0 4px" }
        : i == arr.length - 1
          ? { borderRadius: "0 4px 4px 0", borderLeft: "none" }
          : { borderLeft: "none", borderRadius: 0 };
    };

    const BulkActionsType = isArchivedSelected ?
      [selectedIdsList.length + " items Selected", "Unarchive"] :
      [selectedIdsList.length + " items Selected", "Color", "Archive"];

    return (
      <div className={classes.BulkActionsCnt}>
        {inputType === "Archive" ? (
          <ActionConfirmation
            open={showConfirmation}
            closeAction={this.closeConfirmationPopup}
            cancelBtnText="Cancel"
            successBtnText="Archive projects"
            alignment="center"
            headingText="Archive"
            iconType="archive"
            msgText={`Are you sure you want to archive these ${selectedIdsList.length} projects?`}
            successAction={this.handleArchive}
            btnQuery={archiveBtnQuery}
          />
        ) : inputType === "Unarchive" ? (
          <ActionConfirmation
            open={showConfirmation}
            closeAction={this.closeConfirmationPopup}
            cancelBtnText="Cancel"
            successBtnText="Unarchive projects"
            alignment="center"
            iconType="unarchive"
            headingText="Unarchive"
            msgText={`Are you sure you want to unarchive these ${selectedIdsList.length} projects?`}
            successAction={this.handleUnArchive}
            btnQuery={unarchiveBtnQuery}
          />
        ) : null}
        
          closeAction={this.handleStartDateClose}
          popperProps={{
            anchorEl: this.startDateButton,
            placement: "bottom-start",
            open: startDateOpen
          }}
          type="SelectionOnly"
        />
        
          closeAction={this.handleDueDateClose}
          popperProps={{
            anchorEl: this.dueDateButton,
            placement: "bottom-start",
            open: dueDateOpen
          }}
          type="SelectionOnly"
        />
        {BulkActionsType.map((action, i, arr) => {
          return i == 1 ? (
            <>
              <Button
                disabled={i == 0 ? true : false}
                style={styles(action, i, arr)}
                variant="outlined"
                onClick={this.handleUnArchive}
                classes={{ outlined: classes.BulkActionBtn }}
                selectedIdsList={this.state.selectedIds}
              >
                {action}
              </Button>
              <Popper
                id={id}
                open={open}
                anchorEl={anchorEl}
                disablePortal
                transition
              >
                {({ TransitionProps, placement }) => (
                  <Grow
                    {...TransitionProps}
                    id="menu-list-grow"
                    style={{
                      transformOrigin:
                        placement === "bottom" ? "center top" : "left bottom"
                    }}
                  >
                    <ClickAwayListener onClickAway={this.handleClickAway}>
                      <Paper id="colorPickerCnt">
                        <BlockPicker
                          triangle="hide"
                          color={selectedColor}
                          onChangeComplete={event => {
                            if (event.hex) {
                              this.setState({ selectedColor: event.hex });
                            }
                          }}
                          colors={[
                            "#D9E3F0",
                            "#F47373",
                            "#697689",
                            "#37D67A",
                            "#2CCCE4",
                            "#555555",
                            "#dce775",
                            "#ff8a65",
                            "#ba68c8",
                            "#ffffff"
                          ]}
                        />
                      </Paper>
                    </ClickAwayListener>
                  </Grow>
                )}
              </Popper>
            </>
          ) : (
              <Button
                disabled={i == 0 ? true : false}
                style={styles(action, i, arr)}
                variant="outlined"
                classes={{ outlined: classes.BulkActionBtn }}
                onClick={this.showConfirmationPopup.bind(this, action)}
              >
                {action}
              </Button>
            );
        })}
      </div>
    );
  }
}

// export default withStyles(listStyles, { withTheme: true })(BulkActions);

export default compose(
  withRouter,
  withStyles(listStyles, { withTheme: true }),
  connect(
   null,
    { BulkUpdateProject, FetchWorkspaceInfo }
  )
)(BulkActions);
