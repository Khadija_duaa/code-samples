const listStyles = theme => ({
  BulkActionsCnt: {
    marginLeft: 62,
    marginBottom: 15,
    position: "absolute",
    top: 0,
    zIndex: 1
  },
  BulkActionBtn: {
    border: `1px solid ${theme.palette.border.lightBorder}`,
    textTransform: "capitalize",
    fontWeight: theme.typography.fontWeightLight,
    color: theme.palette.text.primary,
    background: theme.palette.background.default
  },
  shortcutText: {
    color: theme.palette.text.secondary,
    fontSize: "12px !important"
  },
  addNewTaskText: {
    color: theme.palette.text.primary,
    fontSize: "12px !important"
  },
  addTaskInputClearCnt: {
    position: "absolute",
    right: 10,
    zIndex: 1,
    top: 12
  },
  addTaskInputClearText: {
    marginRight: 10,
    lineHeight: "26px",
    color: theme.palette.text.light,
    fontSize: "12px !important"
  },
  TableActionBtnDD: {
    position: "absolute",
    right: 26,
    zIndex: 5
  },
  smallBtnIcon: {
    fontSize: "18px !important",
    color: theme.palette.secondary.light,
    marginRight: 5
  },
  smallIconBtnCount: {
    fontSize: "16px !important",
    marginLeft: 3,
    lineHeight: "normal"
  },
  statusItemText: {
    fontSize: "12px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightRegular
  },
  statusMenuItemCnt: {
    padding: "2px 0 2px 16px",
    fontSize: "12px !important",
    height: 20
  },
  statusMenuItemSelected: {
    background: `transparent !important`,
    "&:hover": {
      background: `transparent !important`
    }
  },
  taskListTitleText: {
    width: "90%",
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    lineHeight: "normal",
    fontSize: "12px !important",
    paddingLeft: 31
  },
  taskListTitleTextCnt: {
    display: "flex",
    justifyContent: "space-between",
    height: 54,
    paddingLeft: 31,
    borderRadius: "4px 0 0 4px",
    alignItems: "center"
  },
  selectError:{
    fontSize: "12px !important",
    position: "absolute",
    top: -18,
    left: 10,
    color: theme.palette.text.danger
  }
});

export default listStyles;
