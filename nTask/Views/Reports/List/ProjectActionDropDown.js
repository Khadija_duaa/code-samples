import React, { Component, Fragment } from "react";
import { withStyles, withTheme } from "@material-ui/core/styles";
import { compose } from "redux";
import { connect } from "react-redux";
import itemStyles from "./styles";
import menuStyles from "../../../assets/jss/components/menu";
import CustomIconButton from "../../../components/Buttons/IconButton";
import SelectionMenu from "../../../components/Menu/SelectionMenu";
import { BlockPicker } from "react-color";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import combineStyles from "../../../utils/mergeStyles";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import RightArrow from "@material-ui/icons/ArrowRight";
import ActionConfirmation from "../../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import DeleteConfirmation from "../../../components/Dialog/Popups/DeleteConfirmation";
import CopyProjectForm from "./CopyProjectForm";
import EditProjectForm from "./EditProjectForm";
import { canEdit, canDo } from "../permissions";

import MoveProjectDialog from "../../../components/Dialog/MoveProjectDialog/MoveProjectDialog";

class ProjectActionDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      pickerOpen: false,
      pickerPlacement: "",
      archiveFlag: false,

      unArchiveFlag: false,
      deleteFlag: false,
      copyFlag: false,
      renameFlag: false,
      archiveBtnQuery: "",
      unarchiveBtnQuery: "",
      moveProjectBtnQuery: "",
      moveProjectDialogue: false
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleColorClick = this.handleColorClick.bind(this);
    this.handlePickerClose = this.handlePickerClose.bind(this);
    this.handleCustomColor = this.handleCustomColor.bind(this);
    this.handleDialogClose = this.handleDialogClose.bind(this);
  }

  componentDidMount = () => {
    this.setState({
      selectedColor: this.props.selectedColor
    });
  };

  handlePopupDialogClose = (event, name) => {
    this.setState({ renameFlag: false, copyFlag: false });
  };

  handleCustomColor(event) {
    event.stopPropagation();
    let obj = Object.assign({}, this.props.project);
    obj.colorCode = event.hex;
    //
    //this.props.isUpdated(obj, this.props.meeting);
    this.props.SaveProject(obj, () => { });
  }
  handleOperations = (event, value) => {
    event.stopPropagation();
    switch (value) {
      case "Rename":
        //this.props.CopyTask(this.props.meeting);
        this.setState({ renameFlag: true });
        break;
      case "Copy":
        this.setState({ copyFlag: true });
        break;
      case "Public Link":
        break;
      case "Move":
        this.setState({ moveProjectDialogue: true });
        break;
      case "Archive":
        this.setState({ archiveFlag: true, popupFlag: true });
        break;
      case "Unarchive":
        this.setState({ unArchiveFlag: true, popupFlag: true });
        break;
      case "Delete":
        this.setState({ deleteFlag: true, popupFlag: true });
        break;
    }

    this.setState({ open: false, pickerOpen: false });
  };
  handleMoveProjectDialogueClose = () => {
    this.setState({ moveProjectDialogue: false });
  }
  handleClose(event) {
    this.setState({ open: false, pickerOpen: false });
  }
  handlePickerClose(event) {
    this.setState({ pickerOpen: false });
  }
  handleDialogClose(event) {
    if (event) event.stopPropagation();
    this.setState({
      popupFlag: false,
      archiveFlag: false,
      unArchiveFlag: false,
      deleteFlag: false
    });
  }

  handleClick(event, placement) {
    event.stopPropagation();
    const { currentTarget } = event;
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement
    }));
  }
  handleColorClick(event) {
    event.stopPropagation();
    if (event.target.closest("#colorPickerCnt") == null) {
      this.setState(prevState => ({ pickerOpen: !prevState.pickerOpen }));
    }
  }

  colorChange = (color, project) => {
    //event.stopPropagation();
    let self = this;
    this.setState({ selectedColor: color.hex }, function () {
      let obj = Object.assign({}, project);
      // this.setState({
      //   selectedColor: color.hex
      // });
      obj.colorCode = color.hex;
      this.props.SaveProject(obj, () => { });
      //this.props.isUpdated(obj, this.props.meeting);
    });
  };

  handleArchive = e => {
    if (e) e.stopPropagation();
    this.setState({ archiveBtnQuery: "progress" }, () => {
      this.props.ArchiveProject(this.props.project.projectId, response => {
        this.setState({ archiveBtnQuery: "", archiveFlag: false });
        if (response.status === 200)
          this.props.FetchWorkSpaceData("", () => { });
      });
    });
  };

  handleUnArchive = e => {
    if (e) e.stopPropagation();
    this.setState({ unarchiveBtnQuery: "progress" }, () => {
      this.props.UnarchiveProject(this.props.project.projectId, response => {
        this.setState({ unarchiveBtnQuery: "", unArchiveFlag: false });
        if (response.status === 200) {
          this.props.handleUnarchive([this.props.project.id]);
          this.props.FetchWorkSpaceData("", () => { });
        }
      });
    });
  };

  handleMoveProject = (e, team) => {
    if (e) e.stopPropagation();
    this.setState({ moveProjectBtnQuery: "progress" }, () => {
      let data = { projectId: this.props.project.projectId, workspaceId: team.teamId };

      this.props.moveProjectToWorkspace(data,
        response => {
          this.setState({ moveProjectBtnQuery: "", moveProjectDialogue: false });
          this.props.FetchWorkSpaceData("", () => { });
        },
        error => {
          this.setState({ moveProjectBtnQuery: "", exceptionText: error.data });
        });
    });
  }

  render() {
    const { classes, theme, project, openProjectSetting, permissions, isArchivedSelected } = this.props;
    const {
      open,
      placement,
      pickerOpen,
      popupFlag,
      deleteFlag,
      archiveFlag,
      unArchiveFlag,
      selectedColor,
      renameFlag,
      copyFlag,
      archiveBtnQuery,
      unarchiveBtnQuery,
      moveProjectDialogue,
      moveProjectBtnQuery
    } = this.state;
    const ddData = isArchivedSelected ?
      [
        "Unarchive",
        "Delete"
      ]
      : [
        "Rename",
        "Copy",
        this.props.teams.length ? "Move" : null,
        "Delete",
        "Archive",
        "Color",
        //"Public Link",
        "Settings",


      ].filter(Boolean);

    return (
      <Fragment>
        <ClickAwayListener onClickAway={this.handleClose}>
          <div>
            <CustomIconButton
              btnType="condensed"
              onClick={event => {
                this.handleClick(event, "bottom-end");
              }}
              buttonRef={node => {
                this.anchorEl = node;
              }}
            >
              <MoreVerticalIcon
                htmlColor={theme.palette.secondary.medDark}
                style={{ fontSize: "24px" }}
              />
            </CustomIconButton>

            <SelectionMenu
              open={open}
              closeAction={this.handleClose}
              placement={placement}
              anchorRef={this.anchorEl}
              list={
                <List
                  onClick={event => {
                    event.stopPropagation();
                  }}
                >
                  <ListItem
                    disableRipple={true}
                    classes={{ root: classes.menuHeadingItem }}
                  >
                    <ListItemText
                      primary="Select Action"
                      classes={{ primary: classes.menuHeadingListItemText }}
                    />
                  </ListItem>
                  {ddData.map(value =>
                    value === "Color" ? (
                      <ListItem
                        key={value}
                        button
                        disableRipple={true}
                        classes={{
                          root: classes.selectColorMenuItem,
                          selected: classes.statusMenuItemSelected
                        }}
                        onClick={event => {
                          this.handleColorClick(event);
                        }}
                      >
                        <ListItemText
                          primary={value}
                          classes={{
                            primary: classes.statusItemText
                          }}
                        />
                        <RightArrow
                          htmlColor={theme.palette.secondary.dark}
                          classes={{ root: classes.submenuArrowBtn }}
                        />
                        <div
                          id="colorPickerCnt"
                          className={classes.colorPickerCntLeft}
                          style={
                            pickerOpen ? { display: "block" } : { display: "none" }
                          }
                        >
                          <BlockPicker
                            triangle="hide"
                            color={selectedColor}
                            onChangeComplete={event => {
                              this.colorChange(event, this.props.project);
                            }}
                            colors={[
                              "#D9E3F0",
                              "#F47373",
                              "#697689",
                              "#37D67A",
                              "#2CCCE4",
                              "#555555",
                              "#dce775",
                              "#ff8a65",
                              "#ba68c8",
                              "#ffffff"
                            ]}
                          />
                        </div>
                      </ListItem>
                    ) : value === "Settings" ? (
                      canDo(project, permissions, "accessSettings") ?
                        <ListItem
                          button
                          disableRipple={true}
                          classes={{
                            selected: classes.statusMenuItemSelected
                          }}
                          onClick={event =>
                            openProjectSetting(
                              event,
                              project.projectId,
                              project.projectName
                            )
                          }
                        >
                          <ListItemText
                            primary="Settings"
                            classes={{
                              primary: classes.statusItemText
                            }}
                          />
                        </ListItem> : null
                    ) : value == "Rename" ? (
                      canEdit(project, permissions, "projectName") ?
                        <ListItem
                          key={value}
                          button
                          disableRipple={true}
                          classes={{ selected: classes.statusMenuItemSelected }}
                          onClick={event => this.handleOperations(event, value)}
                        >
                          <ListItemText
                            primary={value}
                            classes={{
                              primary: classes.statusItemText
                            }}
                          />
                        </ListItem> : null
                    ) : value == "Copy" ? (
                      canDo(project, permissions, "copyInWorkSpace") ?
                        <ListItem
                          key={value}
                          button
                          disableRipple={true}
                          classes={{ selected: classes.statusMenuItemSelected }}
                          onClick={event => this.handleOperations(event, value)}
                        >
                          <ListItemText
                            primary={value}
                            classes={{
                              primary: classes.statusItemText
                            }}
                          />
                        </ListItem> : null
                    ) : value == "Move" ? (
                      <ListItem
                        key={value}
                        button
                        disableRipple={true}
                        classes={{ selected: classes.statusMenuItemSelected }}
                        onClick={event => this.handleOperations(event, value)}
                      >
                        <ListItemText
                          primary={value}
                          classes={{
                            primary: classes.statusItemText
                          }}
                        />
                      </ListItem>
                    ) : value == "Archive" ? (
                      canDo(project, permissions, "archive") ?
                        <ListItem
                          key={
                            value === "Archive"
                              ? this.props.project.isDeleted === true
                                ? "Unarchive"
                                : "Archive"
                              : value
                          }
                          button
                          disableRipple={true}
                          classes={{ selected: classes.statusMenuItemSelected }}
                          onClick={event =>
                            this.handleOperations(
                              event,
                              value === "Archive"
                                ? this.props.project.isDeleted === true
                                  ? "Unarchive"
                                  : "Archive"
                                : value
                            )
                          }
                        >
                          <ListItemText
                            primary={value}
                            classes={{
                              primary: classes.statusItemText
                            }}
                          />
                        </ListItem> : null
                    ) : value == "Delete" ? (
                      canDo(project, permissions, "delete") ?
                        <ListItem
                          key={value}
                          button
                          disableRipple={true}
                          classes={{ selected: classes.statusMenuItemSelected }}
                          onClick={event => this.handleOperations(event, value)}
                        >
                          <ListItemText
                            primary={value}
                            classes={{
                              primary: classes.statusItemText
                            }}
                          />
                        </ListItem> : null
                    ) : (
                      <ListItem
                        key={value}
                        button
                        disableRipple={true}
                        classes={{ selected: classes.statusMenuItemSelected }}
                        onClick={event =>
                          this.handleOperations(
                            event,
                            value
                          )
                        }
                      >
                        <ListItemText
                          primary={value}
                          classes={{
                            primary: classes.statusItemText
                          }}
                        />
                      </ListItem>
                    )
                  )}
                </List>
              }
            />
          </div>
        </ClickAwayListener>

        <MoveProjectDialog
          open={moveProjectDialogue}
          projectName={this.props.project.projectName}
          successAction={this.handleMoveProject}
          closeAction={this.handleMoveProjectDialogueClose}
          btnQuery={moveProjectBtnQuery}
          cancelBtnText="Cancel"
          exceptionText={this.state.exceptionText}
          headingText="Move Project to Workspace"
        />

        <ActionConfirmation
          open={archiveFlag}
          closeAction={this.handleDialogClose}
          cancelBtnText="Cancel"
          successBtnText="Archive"
          alignment="center"
          headingText="Archive"
          iconType="archive"
          msgText={`Are you sure you want to archive this project?`}
          successAction={this.handleArchive}
          btnQuery={archiveBtnQuery}
        />

        <ActionConfirmation
          open={unArchiveFlag}
          closeAction={this.handleDialogClose}
          cancelBtnText="Cancel"
          successBtnText="Unarchive"
          alignment="center"
          iconType="unarchive"
          headingText="Unarchive"
          msgText={`Are you sure you want to unarchive this project?`}
          successAction={this.handleUnArchive}
          btnQuery={unarchiveBtnQuery}
        />

        <DeleteConfirmation
          action={2}
          open={deleteFlag}
          theme={theme}
          view={"Project"}
          message={""} //"Are you sure you want to delete this project?"
          project={project}
          closeAction={this.handleDialogClose}
          DeleteProject={this.props.DeleteProject}
          FetchWorkSpaceData={this.props.FetchWorkSpaceData}
          showSnackbar={this.props.showSnackbar}
          buttonName="Delete"
        />

        <CopyProjectForm
          open={copyFlag}
          closeAction={this.handlePopupDialogClose}
          project={project}
          CopyProject={this.props.CopyProject}
          FetchWorkSpaceData={this.props.FetchWorkSpaceData}
        />

        <EditProjectForm
          open={renameFlag}
          closeAction={this.handlePopupDialogClose}
          project={project}
          EditProject={this.props.EditProject}
          showSnackbar={this.props.showSnackbar}
        />
      </Fragment>
    );
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    permissions: state.workspacePermissions.data.project,
    teams: state.profile.data.workspace || []
  };
};
export default compose(
  withStyles(combineStyles(itemStyles, menuStyles), { withTheme: true }),
  connect(
    mapStateToProps,
    {}
  )
)(ProjectActionDropdown);
