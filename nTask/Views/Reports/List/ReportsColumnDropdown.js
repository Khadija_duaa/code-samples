import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles, withTheme } from "@material-ui/core/styles";
import menuStyles from "../../../assets/jss/components/menu";
import CustomIconButton from "../../../components/Buttons/IconButton";
import SelectionMenu from "../../../components/Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import combineStyles from "../../../utils/mergeStyles";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import { Scrollbars } from "react-custom-scrollbars";
import { saveWorkspaceReportColumnOrder } from "../../../redux/actions/reports";


let total = 6;

class ReportsColumnDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "top-start",
      pickerOpen: false,
      pickerPlacement: "",
      checked: [],
      isLoaded: false,
      loggedInTeam: "",
      isFirstLoad: false
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
  }
  handleClose(event) {
    this.setState({ open: false });
  }
  handleClick(event, placement) {
    event.stopPropagation();
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement
    }));
  }

  componentDidMount() {
    this.setState({
      checked: this.props.reports.columnOrder.reportColumnOrder,
    });
  }

  handleToggle = (e, value) => {
    e.stopPropagation();
    const { checked } = this.state;
    const { columnSelectAction } = this.props;
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex > -1) {
      newChecked.splice(currentIndex, 1)
      this.props.saveWorkspaceReportColumnOrder({ reportColumnOrder: newChecked })
    } else if (checked.length <= total - 1) {
      newChecked.push(value)
      this.props.saveWorkspaceReportColumnOrder({ reportColumnOrder: newChecked })
    }
    this.setState({ checked: newChecked })
  };
  render() {
    const { classes, theme, selectedColor, colorChange, style } = this.props;
    const { open, placement, checked } = this.state;
    const ddData = [
      "Projects",
      "Tasks",
      "Issues",
      "Risks",
      "Meetings",
      "Progress",
      "Resources"
    ]
    return (
      <ClickAwayListener onClickAway={this.handleClose}>
        <div>
          <CustomIconButton
            btnType="condensed"
            style={{ position: "absolute", right: 20, zIndex: 1 }}
            onClick={event => {
              this.handleClick(event, "bottom-end");
            }}
            buttonRef={node => {
              this.anchorEl = node;
            }}
          >
            <MoreVerticalIcon
              htmlColor={theme.palette.secondary.medDark}
              style={{ fontSize: "24px" }}
            />
          </CustomIconButton>
          <SelectionMenu
            open={open}
            closeAction={this.handleClose}
            placement={placement}
            checkedType="multi"
            anchorRef={this.anchorEl}
            list={
              <Scrollbars autoHide style={{ height: 250 }}>
                <List>
                  <ListItem
                    disableRipple={true}
                    classes={{ root: classes.menuHeadingItem }}
                  >
                    <ListItemText
                      primary={`${checked.length}/${total} selected`}
                      classes={{ primary: classes.menuHeadingListItemText }}
                    />
                  </ListItem>
                  {ddData.map(value => (
                    <ListItem
                      key={value}
                      button
                      disableRipple={true}
                      className={`${classes.MenuItem} ${this.state.checked.indexOf(value) !== -1
                          ? classes.selectedValue
                          : ""
                        }`}
                      classes={{ selected: classes.statusMenuItemSelected }}
                      onClick={e => this.handleToggle(e, value)}
                    >
                      <ListItemText
                        primary={value}
                        classes={{
                          primary: classes.statusItemText
                        }}
                      />
                    </ListItem>
                  ))}
                </List>
              </Scrollbars>
            }
          />
      </ClickAwayListener>
            </div >
    );
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    reports: state.reports
  };
};

export default compose(
  withRouter,
  withStyles(menuStyles, {
    withTheme: true
  }),

  connect(
    mapStateToProps,
    { saveWorkspaceReportColumnOrder }
  )
)(ReportsColumnDropdown);
