import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles/";
import DefaultButton from "../../../components/Buttons/DefaultButton";
import SearchDropdown from "../../../components/Menu/SearchMenu";
import itemStyles from "./styles";


class TaskDropDown extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            placement: "",
            selectedProject: "Select Project"
        };
        this.handleClick = this.handleClick.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
    }

    componentDidMount() {
        if (this.props.projectId) {
            let project = this.props.projectsState.data;
            project = project.length ? project.filter(x => x.projectId === this.props.projectId) : []
            if (project.length) {
                this.setState({ selectedProject: project[0].projectName })
            }
        } else {
            this.setState({ selectedProject: "Select Project" })
        }

    }
    static getDerivedStateFromProps(nextProps, prevState) {

        let project = nextProps.projectsState.data;
        if (nextProps.projectId) {
            project = project.length ? project.filter(x => x.projectId === nextProps.projectId) : []
            if (project.length) {
                return ({ selectedProject: project[0].projectName })
            }
        }
        return ({ selectedProject: "Select Project" })
    }

    handleClose(event) {
        this.setState({ open: false });
    }
    handleSelect(value) {
        this.setState({ selectedProject: value })
    }
    handleClick(event, placement) {
        const { currentTarget } = event;
        this.setState(state => ({
            open: state.placement !== placement || !state.open,
            placement
        }));
    }
    render() {

        const { classes, theme } = this.props;
        const { open, placement, selectedProject } = this.state;
        const ddData = (this.props.projectsState && this.props.projectsState.data && this.props.projectsState.data.length) ? this.props.projectsState.data.map(x => {
            return {
                id: x.projectId,
                value: { name: x.projectName }
            }
        }) : [];
        return (
            <Fragment>
                <DefaultButton
                    text={selectedProject}
                    buttonType="DropdownButton"
                    onClick={event => {
                        this.handleClick(event, "bottom-start");
                    }}
                />
                <SearchDropdown
                    placement={placement}
                    open={open}
                    title="Projects"
                    selectAction={this.handleSelect}
                    closeAction={this.handleClose}
                    searchQuery={["id", "value.name"]}
                    data={ddData}
                    isProject={true}
                    projectTaskData={this.props.taskData}
                />
            </Fragment>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        projectsState: state.projects
    }
}


export default compose(
    withRouter,
    withStyles(itemStyles, { withTheme: true }),
    connect(
       mapStateToProps,
        {}
    )
)(TaskDropDown);
