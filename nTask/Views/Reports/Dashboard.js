import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import dashboardStyles from "./styles";
import differenceWith from "lodash/differenceWith";
import selectStyles from "../../assets/jss/components/select";
import combineStyles from "../../utils/mergeStyles";
import ProjectList from "./List/ProjectList";
import { withSnackbar } from "notistack";
import classNames from "classnames";
import Footer from "../../components/Footer/Footer";
import { showLoading, hideLoading } from "react-redux-loading-bar";
import {
  FetchWorkspaceInfo,
  getArchivedData
} from "../../redux/actions/workspace";
import Grid from "@material-ui/core/Grid";
import {getWorkspaceStats, getWorkspaceListCols} from "../../redux/actions/reports";


let increment = 20;
class ReportsDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      alignment: "left",
      name: [],
      open: false,
      anchorEl: "",
      listView: true,
      calendarView: false,
      isArchived: false,
      openFilterSidebar: false,
      multipleFilters: "",
      clearlastState: false,
      filteredProjects: [],
      quickFilters: [],
      total: 0, 
      loadMore: 0,
      isChanged: false,
      selectedProject: "",
      showRecords: increment,
      projectId: "",
      currency: "",
      selectedView: "Default",
      taskBarsType: "Planned"
    };
    this.child = React.createRef();
    this.handleArchiveChange = this.handleArchiveChange.bind(this);
    this.handleExportType = this.handleExportType.bind(this);
    this.searchFilter = this.searchFilter.bind(this);
    this.searchFilterApplied = this.searchFilterApplied.bind(this);
    this.clearFilter = this.clearFilter.bind(this);
    this.returnCount = this.returnCount.bind(this);
    this.handleChangeState = this.handleChangeState.bind(this);
    this.handleProjectListClick = this.handleProjectListClick.bind(this);
    this.handleBackBtn = this.handleBackBtn.bind(this);
    this.resetCount = this.resetCount.bind(this);
  }

  resetCount() {
    this.setState({ showRecords: increment, loadMore: increment });
  }
  returnCount(total, loadMore) {
    this.setState({ total, loadMore });
  }
  handleChangeState = () => {
    this.setState({ isChanged: false });
  };
  clearFilter() {
    this.setState({ multipleFilters: "", clearlastState: false });
  }
  searchFilterApplied() {
    this.setState({ multipleFilters: "" });
  }
  searchFilter(data) {
    this.setState({ multipleFilters: data, isChanged: true }, () => {
      this.setState({ name: [] });
    });
  }
  componentDidMount() {
    this.props.getWorkspaceListCols(() => {
    this.props.getWorkspaceStats((response) => {
      this.setState({workspaceStats: response})
    })
  })
    // this.setState({
    //   loggedInTeam: this.props.loggedInTeam,
    //   filteredProjects: this.props.projects
    // });
  }
  componentDidUpdate(prevProps, prevState) {
  
    // const { workspaceStats } = this.props;
    // if (
    //   JSON.stringify(prevProps.workspaceStats) !== JSON.stringify(workspaceStats)
    // ) {
    //   this.setState({ workspaceStats: workspaceStats });
    // }
  }

  handleProjectListClick(project) {
    
  }
  
  // static getDerivedStateFromProps(nextProps, prevState) {
  //   if (prevState.loggedInTeam !== nextProps.loggedInTeam)
  //     return { loggedInTeam: nextProps.loggedInTeam, name: [] };
  //   else return { loggedInTeam: prevState.loggedInTeam, name: prevState.name };
  // }
  handleDrawerOpen = () => {
    this.setState({ openFilterSidebar: true });
  };

  handleDrawerClose = () => {
    this.setState({ openFilterSidebar: false });
  };

  handleExportType(snackBarMessage, type) {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right"
        },
        variant: type ? type : "info"
      }
    );
  }
  handleChange = event => {
    this.setState({ name: event.target.value, isChanged: true }, () => {
      this.setState({ multipleFilters: "", clearlastState: true });
    });
  };
  handleBackBtn() {
    this.props.history.goBack();
  }
  handleArchiveChange() {
    this.setState({ isArchived: !this.state.isArchived });
  }
  
  handleClose = () => {
    this.setState({ open: false });
  };
  handleRefresh = () => {
    this.props.FetchWorkspaceInfo("", () => {});
  };
  handleAlignment = (event, alignment) => this.setState({ alignment });
  openProjectSetting = (event, projectId, projectName) => {
    this.setState({ projectId, projectName });
  };

  handleClearArchived = () => {
    this.setState({ quickFilters: [], filteredProjects: this.props.projects });
  };
  handleSelectClick = (event, date) => {
    const { value } = event.target;
    // Preventing from default open dialog click
    if (typeof value === "object") {
      getArchivedData(
        1,
        resp => {
          this.setState({
            quickFilters: value,
            filteredProjects: resp.data
          });
        },
        error => {}
      );
    } else this.setState({ quickFilters: value || [] });
  };
  handleSwitch = name => {
    this.setState({ [name]: !this.state[name] });
  };
  filterProject = ids => {
    this.setState(({ filteredProjects }) => {
      return {
        filteredProjects: differenceWith(
          filteredProjects,
          ids,
          (project, id) => {
            return id === project.id;
          }
        )
      };
    });
  };
  render() {
    const { classes, theme,quote } = this.props;
    const {
      openFilterSidebar,
      filteredProjects,
      quickFilters,
      workspaceStats
    } = this.state;

    const isArchived =
      quickFilters.length &&
      quickFilters[quickFilters.length - 1] === "Archived";

 
    return (
      <div className={classes.root}>
        <main
          className={classes.content}
        >
          <div className={classes.drawerHeader}>
            <Grid container classes={{ container: classes.taskDashboardCnt }}>
              <Grid
                container
                direction="row"
                justify="space-between"
                alignItems="center"
                classes={{ container: classes.taskDashboardHeader }}
              >
                <div className="flex_center_start_row">
                  <Typography variant="h1">
                  Workspaces Summary Report
                  </Typography>
                  
                </div>
              </Grid>
              <Grid
                container
                xs={12}
                classes={{ container: classes.dashboardContentCnt }}
              >
                  <Grid item classes={{ item: classes.taskListCnt }}>
                    {workspaceStats ?
                    <ProjectList
                      projectState={quickFilters}
                      filteredProjects={filteredProjects}
                      filterProject={this.filterProject}
                      isArchivedSelected={isArchived}
                      projectClick={this.handleProjectListClick}
                      openProjectSetting={this.openProjectSetting}
                      handleExportType={this.handleExportType}
                      multipleFilters={this.state.multipleFilters}
                      searchFilterApplied={this.searchFilterApplied}
                      returnCount={this.returnCount}
                      handleChangeState={this.handleChangeState}
                      isChanged={this.state.isChanged}
                      workspaceStats={workspaceStats}
                    /> : null}
                  </Grid>
              </Grid>
            </Grid>
          </div>
        </main>
        <Footer
          quote={quote}
          total={this.state.filteredProjects.length}
          display={true}
          type="Project"
        />
      </div>
    );
  }
}
const mapStateToProps = (state, ownProps) => {
  
  return {
    loggedInTeam: state.profile.data.loggedInTeam,
    projects: state.projects.data || [],
    tasks: state.tasks.data || [],
    permissions: state.workspacePermissions.data.project,
    // workspaceStats: state.reports.data || []
  };
};

export default compose(
  withRouter,
  withSnackbar,
  withStyles(combineStyles(dashboardStyles, selectStyles), {
    withTheme: true
  }),
  connect(
    mapStateToProps,
    { showLoading, hideLoading, FetchWorkspaceInfo, getWorkspaceStats, getWorkspaceListCols }
  )
)(ReportsDashboard);
