const drawerWidth = 350;
const dashboardStyles = theme => ({
  taskDashboardCnt: {
    padding: 20px,
    paddingLeft: 0,
    paddingRight: 0
  },
  taskDashboardHeader: {
    margin: "0 0 10px 0",
    padding: "0 20px 0 62px"
  },
  className: {},
  toggleContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
    marginLeft: 20
  },
  backArrowIcon: {
    fontSize: "28px !important",
    marginRight: 10,
    cursor: "pointer"
  },
  toggleBtnGroup: {
    borderRadius: 4,
    background: theme.palette.common.white,
    // border: `1px solid ${theme.palette.border.lightBorder}`,
    boxShadow: "none",
    "& $toggleButtonSelected": {
      color: theme.palette.text.secondary,
      backgroundColor: theme.palette.common.white,
      "&:after": {
        background: theme.palette.common.white
      },
      "&:hover":{
        background: theme.palette.common.white
      },
    },
  },
  groupedHorizontal:{
    borderLeft: `1px solid ${theme.palette.border.lightBorder} !important`
  },
  toggleButton: {
   height: "auto",
    padding: "5px 20px 6px",
    fontSize: "12px !important",
    fontWeight: theme.palette.fontWeightMedium,
    textTransform: "capitalize",
    "&:hover":{
      background: theme.palette.common.white
    },
    "&[value = 'center']": {
      borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
      borderRight: `1px solid ${theme.palette.border.lightBorder}`
    }
  },
  toggleButtonSelected:{},
  filterButtonCnt: {},
  filterIcon: {
    fontSize: "18px !important"
  },
  importExportButtonCnt: {
    margin: "0 0 0 8px"
  },
  importExportIcon: {
    // fontSize: 29
  },
  fullScreenIcon: {
    fontSize: "19px !important"
  },
  filterDDText: {
    fontSize: "12px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightRegular
  },
  taskGridCnt: {
    padding: "0 30px 0 60px",
    flex: 1,
    overflowY: "auto",
    display: "flex"
  },
  taskCalendarCnt: {
    padding: "0 30px 0 60px",
    width: "100%",
    overflowY: "auto"
  },
  taskListCnt: {
    width: "100%",
    marginLeft: 60
  },
  content: {
    flexGrow: 1,
    paddingBottom: 120,
    // transition: theme.transitions.create("margin", {
    //   easing: theme.transitions.easing.sharp,
    //   duration: theme.transitions.duration.leavingScreen
    // }),
  },
  contentShift: {
    // transition: theme.transitions.create("margin", {
    //   easing: theme.transitions.easing.easeOut,
    //   duration: theme.transitions.duration.enteringScreen
    // }),
    // marginRight: 0
  },
  drawerHeader: {
    display: "flex",
    alignItems: "center",
    ...theme.mixins.toolbar,
    justifyContent: "flex-start"
  },
  hide: {
    display: "none"
  },
  root: {
    display: "flex"
  },
  taskGanttCnt: {
    padding: "0 2px 0 40px",
    width: "100%"
  },
  backArrowIcon: {
    fontSize: "28px !important",
    marginRight: 10,
    cursor: "pointer"
  },
  plannedVsActualSwtichCnt:{
    borderRadius: 4,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    background: theme.palette.common.white,
    padding: "4.5px 0px 4.5px 12px",
    marginRight: 20
  },
  projectHeading:{
    '@media (max-width: 1670px)':{
      maxWidth: 500,
      minWidth: 250,
      overflow: "hidden",
      fontSize: "18px !important",
      color: theme.palette.text.primary,
      whiteSpace: "nowrap",
      textOverflow: "ellipsis"
    },
    '@media (max-width: 1200px)':{
      maxWidth: 300,
      overflow: "hidden",
      color: theme.palette.text.primary,
      fontSize: "18px !important",
      whiteSpace: "nowrap",
      textOverflow: "ellipsis"
    }
  }

});

export default dashboardStyles;
