import React from "react";
import withStyles from '@material-ui/core/styles/withStyles';
import IssuesOverview from "../IssuesOverview/IssuesOverview.view";
import overviewStyles from './overview.style';

function Overviews({classes}) {

  return (
    <div className={classes.overviewCnt}>
      <IssuesOverview />
    </div>
  );
}

export default withStyles(overviewStyles, {withTheme: true})(Overviews);
