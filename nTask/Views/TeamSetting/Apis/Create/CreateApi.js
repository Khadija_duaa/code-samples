import React, { useEffect, useState } from "react";
import CreateApiStyle from "./CreateApi.Style";
import { withStyles } from "@material-ui/core/styles";
import CustomButton from "../../../../components/Buttons/CustomButton";
import DefaultTextField from "../../../../components/Form/TextField";
import Checkbox from "@material-ui/core/Checkbox";
import CheckBoxIcon from "../../../../components/Icons/CheckBoxIcon";
import SvgIcon from "@material-ui/core/SvgIcon";
import { multipleTasks } from "../constant";
import clsx from "clsx";
import { saveApi, updateChangesApis } from "../../../../redux/actions/apiKeys";
import { useSelector } from "react-redux";
import { generateWorkspaceData } from "../../../../helper/generateSelectData";
import CustomMultiSelectDropdown from "../../../../components/Dropdown/CustomMultiSelectWithSearchBarDropdown/Dropdown";

function CreateApi({
  classes,
  theme,
  handleShowDrawar,
  handleCloseDrawer,
  apisData,
  setNewApi,
  saveData,
  handleUpdateData,
  handleAddNewApi,
}) {
  const [options, setOptions] = useState(multipleTasks);
  const [inputValue, setInputValue] = useState("");
  const [workspaces, setWorkspaces] = useState([]);
  const [btnQuery, setBtnQuery] = useState("");
  const { profile } = useSelector(state => {
    return { profile: state.profile.data };
  });

  const handleCheck = (id, value) => {
    const updatedOptions = options.map(item => {
      if (item.id === id) return { ...item, checked: value };
      else return item;
    });
    setOptions(updatedOptions);
  };
  const onSelectionChangeInWorkspaces = selectedOption => {
    setWorkspaces(selectedOption);
  };
  const handleInputChange = (e, value) => {
    setInputValue(e.target.value);
  };
  const handleCancel = () => {
    handleCloseDrawer();
  };
  const handleUpdate = () => {
    setBtnQuery("progress");
    const UpdateApiData = {
      isProject: options[0].checked,
      isTask: options[1].checked,
      isIssue: options[2].checked,
      isRisk: options[3].checked,
      isTimesheet: options[4].checked,
      workspaceIds: workspaces.map(w => w.id),
      teamId: profile && profile.activeTeam,
      name: inputValue,
      _id: saveData._id,
    };
    updateChangesApis(UpdateApiData, response => {
      setBtnQuery("");
      let updatedData = apisData.find(item => item._id === UpdateApiData._id);
      updatedData = { ...updatedData, ...UpdateApiData };
      setNewApi(updatedData);
      handleUpdateData(updatedData);
      handleShowDrawar();
    });
  };
  const handleCreate = () => {
    setBtnQuery("progress");
    const createApiData = {
      isProject: options[0].checked,
      isTask: options[1].checked,
      isIssue: options[2].checked,
      isRisk: options[3].checked,
      isTimesheet: options[4].checked,
      workspaceIds: workspaces.map(w => w.id),
      teamId: profile && profile.activeTeam,
      name: inputValue,
    };
    saveApi(
      createApiData,
      response => {
        setBtnQuery("");
        if (response.data.statusCode == 200) {
          handleAddNewApi({ ...response.data.data, name: createApiData.name });
          handleShowDrawar();
        }
      },
      () => {
        setBtnQuery("");
      }
    );
  };

  useEffect(() => {
    if (saveData) {
      setInputValue(saveData.name);
      const selectedWorkspace = generateWorkspaceData().filter(item =>
        saveData.workspaceIds.includes(item.id)
      );
      setWorkspaces(selectedWorkspace);
      const multipleTasksValue = multipleTasks.map(item => {
        switch (item.label) {
          case "Projects":
            return { ...item, checked: saveData.isProject };
            break;
          case "Tasks":
            return { ...item, checked: saveData.isTask };
            break;
          case "Issues":
            return { ...item, checked: saveData.isIssue };
            break;
          case "Risks":
            return { ...item, checked: saveData.isRisk };
            break;
          case "TimeSheet":
            return { ...item, checked: saveData.isTimesheet };
            break;
          default:
            return { ...item };
            break;
        }
      });
      setOptions(multipleTasksValue);
    }
    return () => {
      setWorkspaces([]);
      setInputValue("");
      setOptions(multipleTasks);
    };
  }, [saveData]);

  const disableBtn = options.every(p => p.checked == false);
  return (
    <>
      <div className={classes.createApiCnt}>
        <div className={classes.createContent}>
          <h1>{saveData ? "edit Api" : "create Api"}</h1>
        </div>
        <div className={classes.innerbox}>
          <DefaultTextField
            label="Name"
            error={false}
            errorState={false}
            errorMessage={""}
            formControlStyles={{ marginTop: 24, marginBottom: 12 }}
            defaultProps={{
              id: "customTextField",
              value: inputValue,
              placeholder: "Enter api key name",
              autoFocus: true,
              inputProps: { maxLength: 250 },
              style: {},
              disabled: false,
              autoComplete: "off",
              inputRef: () => {},
              onChange: e => handleInputChange(e),
            }}
            customInputClass={{
              input: classes.innerInputStyle,
            }}
          />
          <labe className={classes.dropLabel}>Workspace</labe>
          <CustomMultiSelectDropdown
            onSelect={option => onSelectionChangeInWorkspaces(option)}
            options={generateWorkspaceData}
            option={workspaces}
            isClearable={false}
            size={"large"}
            label=""
            placeholder="Workspace"
            dropdownProps={{ style: { width: 376 } }}
            isMulti={true}
            optionBackground={true}
            className={classes.removeBorder}
            buttonProps={{
              variant: "contained",
              btnType: "white",
              labelAlign: "left",
              style: {
                minWidth: "100%",
                padding: "3px 4px 3px 8px",
                height: 35,
                border: "1px solid #DDDDDD",
                marginTop: "5px",
                marginBottom: "16px",
              },
            }}
          />
        </div>
        <div className={classes.innerContent}>
          <h1>Permissions</h1>
          <p>Select one or multiple features you want to allow in this API.</p>
          <div className={classes.Cnt}>
            <ul className={classes.listBox}>
              {options.map(item => {
                return (
                  <>
                    <li
                      className={clsx({
                        [classes.active]: item.checked,
                      })}
                      onClick={() => handleCheck(item.id, !item.checked)}>
                      <div>
                        <Checkbox
                          checked={item.checked}
                          disabled={false}
                          style={{ padding: "0px 10px 0 0" }}
                          disableRipple
                          value={item.label}
                          checkedIcon={
                            <SvgIcon
                              viewBox="0 0 426.667 426.667"
                              htmlColor={theme.palette.status.completed}
                              classes={{
                                root: classes.checkedIcon,
                              }}>
                              <CheckBoxIcon />
                            </SvgIcon>
                          }
                          icon={
                            <>
                              <SvgIcon
                                viewBox="0 0 426.667 426.667"
                                htmlColor={theme.palette.background.items}
                                classes={{
                                  root: classes.unCheckedIcon,
                                }}>
                                <CheckBoxIcon />
                              </SvgIcon>
                              <span className={classes.emptyCheckbox} />
                            </>
                          }
                          color="primary"
                        />
                      </div>
                      <div className={classes.innerLiItems}>
                        <label>{item.label}</label>
                        <span>{item.descripation}</span>
                      </div>
                    </li>
                  </>
                );
              })}
            </ul>
          </div>
        </div>
        <div className={classes.footerButton}>
          <CustomButton
            variant="contained"
            btnType="white"
            className={classes.cancelBtn}
            onClick={handleCancel}>
            Cancel
          </CustomButton>
          <CustomButton
            query={btnQuery}
            disabled={inputValue == "" || workspaces.length == 0 || disableBtn}
            variant="contained"
            onClick={saveData ? handleUpdate : handleCreate}
            className={classes.createBtn}>
            {saveData ? "Save Changes" : "Create API"}
          </CustomButton>
        </div>
      </div>
    </>
  );
}
0;
CreateApi.defaultProps = {
  handleDone: false,
};
export default withStyles(CreateApiStyle, { withTheme: true })(CreateApi);
