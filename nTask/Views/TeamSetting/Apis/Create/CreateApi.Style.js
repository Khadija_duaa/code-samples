const CreateApiStyle = theme => ({
  createApiCnt: {
    width: 400,
    padding: "11px 12px",
    height: "100%",
    position: "relative",
  },
  createContent: {
    padding: "11px 0px",
    background: theme.palette.common.white,
    marginBottom: 12,

    "& h1": {
      fontSize: "16px !important",
      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: theme.typography.fontWeightLarge,
      color: "#111111",
      textTransform: "capitalize",
      margin: 0,
    },
  },

  innerContent: {
    borderTop: `1px solid ${theme.palette.border.grayLighter}`,

    "& h1": {
      fontSize: "14px !important",
      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: theme.typography.fontWeightLarge,
      color: "#171717",
    },
    "& p": {
      fontSize: "13px !important",
      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: theme.typography.fontWeightRegular,
      color: "#555555",
      marginTop: "-4px",
    },
  },

  listBox: {
    margin: 0,
    padding: 0,
    "& li": {
      display: "flex",
      border: `1px solid ${theme.palette.border.lightBorder}`,
      padding: "10px 10px",
      borderRadius: "6px",
      marginBottom: 8,
      alignItems: " center",
      cursor: "pointer",
    },
  },
  innerLiItems: {
    display: "flex",
    flexDirection: "column",
    "& label": {
      fontSize: "13px !important",
      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: theme.typography.fontWeightMedium,
      color: "#292929",
    },
    "& span": {
      fontSize: "13px !important",
      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: theme.typography.fontWeightRegular,
      color: "#555555",
    },
  },
  footerButton: {
    borderTop: `1px solid ${theme.palette.border.grayLighter}`,
    position: "absolute",
    bottom: 52,
    left: 0,
    right: 0,
    paddingRight: 12,
    paddingTop: 10,
    display: "flex",
    alignItems: "center",
    justifyContent: "right",
  },
  cancelBtn: {
    border: "1px solid #BFBFBF",
    color: "#111111",
    marginRight: 10,
    padding: "10px 19px",
    height: 36,
    "&:hover": {
      border: "1px solid #BFBFBF",
    },
  },
  createBtn: {
    background: theme.palette.background.blue,
    color: theme.palette.common.white,
    textTransform: "capitalize",
    padding: "10px 22px",
    height: 36,
    fontSize: "13px !important",
    boxShadow: "none",
    "&:hover": {
      background: theme.palette.background.blue,
    },
  },
  checkedIcon: {
    fontSize: "18px !important",
    color: "#0090ff",
  },
  unCheckedIcon: {
    fontSize: "18px !important",
    display: "none",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "100%",
  },
  emptyCheckbox: {
    width: 18,
    height: 18,
    background: theme.palette.background.paper,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "100%",
  },
  active: {
    border: "1px solid #80C8FF !important",
    backgroundColor: "#EBF6FF !important",
  },
  innerInputStyle: {
    padding: "10px 12px",
  },
  disableBtn: {
    color: "#555555 !important",
    background: "#DDDDDD !important",
  },
  removeBorder: {
    "&:hover": {
      border: "1px solid #DDDDDD",
      boxShadow: "none",
    },
  },

  dropLabel: {
    color: "#202020",
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: theme.typography.fontWeightRegular,
  },
});
export default CreateApiStyle;
