import React, { useState } from 'react'
import { withStyles } from '@material-ui/core/styles'
import HeaderStyle from './Header.Style'
import DefaultTextField from '../../../../components/Form/TextField'
import SelectSearchDropdown from '../../../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown'
import CustomButton from '../../../../components/Buttons/CustomButton'
import Typography from "@material-ui/core/Typography";
import { headerApiDD } from "../constant";

function Header({ classes, handleOpenDrawer, theme, searchTerm, handleInputChange, handleSelectFilter }) {
    const handleCreateNewApi = () => {
        handleOpenDrawer();
    }

    return (
        <>
            <div className={classes.header}>
                <div className={classes.LeftContent}>
                    <Typography variant="h1">
                        API Keys
                    </Typography>
                </div>
                <div className={classes.headerBtn}>
                    <DefaultTextField
                        // fullWidth
                        error={false}
                        errorState={false}
                        errorMessage={""}
                        formControlStyles={{ width: "198px", marginBottom: 0 }}
                        defaultProps={{
                            value: searchTerm,
                            placeholder: "Search by API name",
                            autoFocus: true,
                            inputProps: { maxLength: 250 },
                            style: {},
                            disabled: false,
                            autoComplete: "off",
                            inputRef: () => { },
                            onChange: e => handleInputChange(e),
                        }}
                        customInputClass={{
                            root: classes.headerInput,
                            input: classes.inputStyle
                        }}
                    />
                    <SelectSearchDropdown
                        data={() => headerApiDD}
                        selectChange={(type, option) => {
                            handleSelectFilter(type, option)
                        }}
                        isClearable={false}
                        label=""
                        type="workspace"
                        // placeholder="All APIs"
                        isMulti={false}
                        customStyles={{
                            control: {
                                minHeight: "35px",
                            }
                        }}
                        styles={{
                            width: "150px",
                            margin: "0px",
                            marginLeft: "10px",
                            marginRight: "10px",
                        }}
                    />
                    <CustomButton
                        variant='contained'
                        btnType='success'
                        onClick={handleCreateNewApi}
                        className={classes.createBtn}
                    >
                        Create New API</CustomButton>
                </div>
            </div>

        </>
    )
}
Header.defaultProps = {

}
export default withStyles((HeaderStyle), { withTheme: true })(Header)
