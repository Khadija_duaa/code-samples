const HeaderStyle = (theme) => ({
    header: {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
      
    },
    headerBtn: {
        display: "flex",
        alignItems: "center",
    },
    headerInput: {
        fontSize: "13px !important",
        fontFamily: theme.typography.fontFamilyLato,
        fontWeight: theme.typography.fontWeightRegular,
        color: theme.palette.text.medGray,

    },
    inputStyle: {
        padding: "10px 12px",
    },
    createBtn: {
        padding: "10px 14px",
        height: 35,
        [theme.breakpoints.between('sm', 'md')]: {
            width: "33%",
        },
    },
    LeftContent: {

        "& h1": {
            fontSize: "20px",
            fontWeight: "700",
            [theme.breakpoints.between('sm', 'md')]: {
                width: "100px",
            },
        }
    }
})
export default HeaderStyle