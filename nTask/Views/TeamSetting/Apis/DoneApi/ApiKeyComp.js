import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import ApiKeyCompStyle from "./ApiKeyComp.style";
import MoreActionDropdown from "../MoreActionDropdown/MoreActionDropdown";
import ApiIcon from "../../../../components/Icons/ApiIcon";
import CustomButton from "../../../../components/Buttons/CustomButton";
import { updateChangesApis } from "../../../../redux/actions/apiKeys";
import SearchInput, { createFilter } from "react-search-input";
import CheckIcon from "@material-ui/icons/Check";
import CloseIcon from "@material-ui/icons/Close";
import CustomIconButton from "../../../../components/Buttons/CustomIconButton";
import Scrollbars from "react-custom-scrollbars";

function ApiKeyComp({
  classes,
  handleOpenDrawer,
  handleSaveChanges,
  handleEdit,
  apisData,
  setApisData,
  handleUpdateData,
  searchTerm,
  filter,
}) {
  // Enable function
  const handleEnable = item => {
    const UpdateApiData = {
      isDisabled: false,
      _id: item._id,
    };
    updateChangesApis(UpdateApiData, response => {
      let updatedData = apisData.find(item => item._id === UpdateApiData._id);
      updatedData = { ...updatedData, ...UpdateApiData };
      handleUpdateData(updatedData);
    });
  };
  //  Disable function
  const handleDisable = item => {
    const UpdateApiData = {
      isDisabled: true,
      _id: item._id,
    };
    updateChangesApis(UpdateApiData, response => {
      let updatedData = apisData.find(item => item._id === UpdateApiData._id);
      updatedData = { ...updatedData, ...UpdateApiData };
      handleUpdateData(updatedData);
    });
  };
  // Delete Function
  const handleApiDelete = id => {
    const updatedData = apisData.filter(item => item._id !== id);
    setApisData(updatedData);
  };
  let filteredData = apisData.filter(createFilter(searchTerm, ["name"]));
  filteredData =
    filter == 1
      ? filteredData
      : filter == 2
      ? filteredData.filter(d => !d.isDisabled)
      : filter == 3
      ? filteredData.filter(d => d.isDisabled)
      : filteredData;

  return (
    <>
    <div className={classes.mainContainer}>
            <div className={classes.headerStyle}>
        <ul>
          <li>
            <div className={classes.headerName}>
              <span>Name</span>
            </div>
          </li>
          <li className={classes.mainApiFlex}>
            <div className={classes.headerApi}>
              <span>API ID</span>
            </div>
          </li>
          <li>
            <div className={classes.headerProject}>
              <span>Projects</span>
            </div>
          </li>
          <li>
            <div className={classes.headerTask}>
              <span>Tasks</span>
            </div>
          </li>
          <li>
            <div className={classes.headerIssue}>
              <span> Issues</span>
            </div>
          </li>
          <li>
            <div className={classes.headerRisk}>
              <span>Risks</span>
            </div>
          </li>
          <li>
            <div className={classes.headerTimeSheet}>
              <span>TimeSheet</span>
            </div>
          </li>
        </ul>
      </div>

      <div className={classes.apiSection}>
        <Scrollbars>
          <ul className={classes.innerCnt}>
            {filteredData && filteredData.length ? (
              filteredData.map(item => {
                return (
                  <>
                    <li className={classes.renderItems}>
                      <div className={classes.innerContentStyle}>
                        <label>{item.name}</label>
                        {item.isDisabled ? (
                          <CustomButton variant="contained" className={classes.disabledBtn}>
                            disabled
                          </CustomButton>
                        ) : null}
                      </div>
                      <div className={classes.secretKeys}>
                        <p>{item.apiKey}</p>
                      </div>
                      <div className={classes.tickOne}>
                        {item.isProject ? (
                          <CustomIconButton className={classes.styleIconButton}>
                            <CheckIcon className={classes.checkIconStyle} />
                          </CustomIconButton>
                        ) : (
                          <CustomIconButton className={classes.styleIconButton}>
                            <CloseIcon className={classes.closeIconStyle} />
                          </CustomIconButton>
                        )}
                      </div>
                      <div className={classes.tickTwo}>
                        {item.isTask ? (
                          <CustomIconButton className={classes.styleIconButton}>
                            <CheckIcon className={classes.checkIconStyle} />
                          </CustomIconButton>
                        ) : (
                          <CustomIconButton className={classes.styleIconButton}>
                            <CloseIcon className={classes.closeIconStyle} />
                          </CustomIconButton>
                        )}
                      </div>

                      <div className={classes.tickThree}>
                        {item.isIssue ? (
                          <CustomIconButton className={classes.styleIconButton}>
                            <CheckIcon className={classes.checkIconStyle} />
                          </CustomIconButton>
                        ) : (
                          <CustomIconButton className={classes.styleIconButton}>
                            <CloseIcon className={classes.closeIconStyle} />
                          </CustomIconButton>
                        )}
                      </div>
                      <div className={classes.tickFour}>
                        {item.isRisk ? (
                          <CustomIconButton className={classes.styleIconButton}>
                            <CheckIcon className={classes.checkIconStyle} />
                          </CustomIconButton>
                        ) : (
                          <CustomIconButton className={classes.styleIconButton}>
                            <CloseIcon className={classes.closeIconStyle} />
                          </CustomIconButton>
                        )}
                      </div>
                      <div className={classes.tickFive}>
                        {item.isTimesheet ? (
                          <CustomIconButton className={classes.styleIconButton}>
                            <CheckIcon className={classes.checkIconStyle} />
                          </CustomIconButton>
                        ) : (
                          <CustomIconButton className={classes.styleIconButton}>
                            <CloseIcon className={classes.closeIconStyle} />
                          </CustomIconButton>
                        )}
                      </div>
                      <div className={classes.apiDropDown}>
                        <MoreActionDropdown
                          handleEnable={handleEnable}
                          handleEdit={handleEdit}
                          handleDisable={handleDisable}
                          apiKeyData={item}
                          handleApiDelete={handleApiDelete}
                          handleOpenDrawer={handleOpenDrawer}
                          handleSaveChanges={handleSaveChanges}
                        />
                      </div>
                    </li>
                  </>
                );
              })
            ) : (
              <div className={classes.Section}>
                <ApiIcon />
                <h1>Create and Manage your APIs here.</h1>
                <h2>
                  Click on<b> Create New API</b> button to start creating API.
                </h2>
              </div>
            )}
          </ul>
        </Scrollbars>
      </div>
      </div>

    </>
  );
}

export default withStyles(ApiKeyCompStyle, { withTheme: true })(ApiKeyComp);
