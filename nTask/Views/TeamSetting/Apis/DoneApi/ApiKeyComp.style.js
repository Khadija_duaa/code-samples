const AliKeyCompStyle = theme => ({
  mainContainer:{
    width: "100%",
    height: "100%",
  
    [theme.breakpoints.down("sm")]: {
      width: "100%",
      height: "100%",
      overflowX: "auto",
    },
  },
  apiSection: {
    height: "93%",
    width: "100%",
    [theme.breakpoints.down("sm")]: {
      width: "1000px",
    
    },
  },
  mainSection: {
    display: "flex",
    justifyContent: "space-between",
  },
  innerCnt: {
    width: "100%",
    listStyle: "none",
    margin: 0,
    padding: 0,
  },
  renderItems: {
    marginLeft: "10px",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    "& p": {
      fontSize: "13px !important",
      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: theme.typography.fontWeightRegular,
      color: "#555555",
    },
  },
  secretKeys: {
    flex: "1",
    textAlign: "left",

    "& p": {
      fontSize: "13px !important",
      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: theme.typography.fontWeightMedium,
      color: "#111111",
      [theme.breakpoints.down("lg")]: {
        fontSize: "11px !important",
      },
    },
  },
  Section: {
    textAlign: "center",
    position: "absolute",
    top: "45%",
    left: "34%",

    "& h1": {
      fontSize: "18px !important",
      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: theme.typography.fontWeightLarge,
      color: theme.palette.text.lightGray,
    },
    "& h2": {
      fontSize: "14px !important",
      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: theme.typography.fontWeightRegular,
      color: "rgba(85, 85, 85, 1)",

      "& b": {
        color: "rgba(17, 17, 17, 1)",
        fontWeight: theme.typography.fontWeightLarge,
      },
    },
  },
  iconKeyStyle: {
    display: "flex",
    alignItems: "center",

    "& label": {
      fontSize: "13px !important",
      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: theme.typography.fontWeightRegular,
      color: "#555555",
      marginLeft: 6,
    },
  },
  innerContentStyle: {
    width: "250px",
    textAlign: "left",
    [theme.breakpoints.down("lg")]: {
      width: "200px",
    },
    "& label": {
      fontSize: "15px !important",
      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: theme.typography.fontWeightLarge,
      color: "#111111",
      overflow: "hidden",
      textOverflow: "ellipsis",
      whiteSpace: "nowrap",
      display: "inline-block",
      width: "115px",
    },
  },
  disabledBtn: {
    height: " 19px",
    borderRadius: "4px",
    background: "#FDE4E4",
    fontSize: "12px",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: theme.typography.fontWeightRegular,
    color: "#D12626",
    boxShadow: " none",
    padding: "2px 6px",
    textTransform: "capitalize",
    marginLeft: 10,

    "&:hover": {
      background: "#FDE4E4",
    },
  },
  headerStyle: {
    width: "100%",
    height: "29px",
    padding: "7px 8px",
    background: "#F1F1F1",
    marginTop: "10px",
    [theme.breakpoints.down("sm")]: {
      width: "1000px",
    },

    "& ul": {
      display: "flex",
      margin: 0,
      padding: 0,
      listStyleType: "none",

      //   "& li": {
      //     fontSize: "13px !important",
      //     fontFamily: theme.typography.fontFamilyLato,
      //     fontWeight: theme.typography.fontWeightLarge,
      //     color: "#555555",
      //     width: "145px",

      //     [theme.breakpoints.between("sm","lg")]: {
      //       width: "89px",
      //     },
      //   },
    },
  },
  //   headerlabel: {
  //     flex: "0.4",
  //     fontSize: "13px !important",
  //     fontFamily: theme.typography.fontFamilyLato,
  //     fontWeight: theme.typography.fontWeightLarge,
  //     color: "#555555",
  //   },
  headerItemSetting: {
    "& ul": {
      display: "flex",
      margin: 0,
      padding: 0,
      listStyleType: "none",

      "& li": {
        width: "150px",
        textAlign: "center",
      },
    },
  },

  headerApiId: {
    flex: "0.9",
    textAlign: "left",
  },
  checkIconStyle: {
    fontSize: " 20px",
    color: "#7CB147",
  },
  closeIconStyle: {
    fontSize: " 20px",
    color: "#7E7E7E",
  },
  styleIconButton: {
    "&:hover": {
      background: "white",
    },
  },
  tickOne: {
    width: "141px",
    textAlign: "center",

    [theme.breakpoints.between("sm", "md")]: {
      width: "34px",
    },
    [theme.breakpoints.down("lg")]: {
      width: "51px",
    },
  },
  tickTwo: {
    width: "47px",
    textAlign: "center",

    [theme.breakpoints.between("sm", "md")]: {
      width: "93px",
    },
    [theme.breakpoints.down("lg")]: {
      width: "68px",
    },
  },
  tickThree: {
    width: "157px",
    textAlign: "center",

    [theme.breakpoints.between("sm", "md")]: {
      width: "34px",
    },
    [theme.breakpoints.down("lg")]: {
      width: "35px",
    },
  },
  tickFour: {
    width: "42px",
    textAlign: "center",

    [theme.breakpoints.between("sm", "md")]: {
      width: "98px",
    },
    [theme.breakpoints.down("lg")]: {
      width: "85px",
    },
  },

  tickFive: {
    width: "154px",
    textAlign: "center",

    [theme.breakpoints.between("sm", "md")]: {
      width: "64px",
    },
    [theme.breakpoints.down("lg")]: {
      width: "61px",
    },
  },

  headerName: {
    width: "250px",
    [theme.breakpoints.down("lg")]: {
      width: "200px",
    },
    "& span": {
      fontSize: "13px !important",
      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: theme.typography.fontWeightLarge,
      color: "#555555",
    },
  },
  headerApi: {
    // width: " 413px",
    "& span": {
      fontSize: "13px !important",
      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: theme.typography.fontWeightLarge,
      color: "#555555",
    },
  },
  headerProject: {
    width: "100px",
    [theme.breakpoints.between("sm", "md")]: {
      width: "65px",
    },
    [theme.breakpoints.down("lg")]: {
      width: "67px",
    },
    "& span": {
      fontSize: "13px !important",
      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: theme.typography.fontWeightLarge,
      color: "#555555",
    },
  },
  headerTask: {
    width: "100px",
    [theme.breakpoints.between("sm", "md")]: {
      width: "65px",
    },
    [theme.breakpoints.down("lg")]: {
      width: "55px",
    },
    "& span": {
      fontSize: "13px !important",
      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: theme.typography.fontWeightLarge,
      color: "#555555",
    },
  },

  headerIssue: {
    width: "100px",
    [theme.breakpoints.between("sm", "md")]: {
      width: "65px",
    },
    [theme.breakpoints.down("lg")]: {
      width: "59px",
    },
    "& span": {
      fontSize: "13px !important",
      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: theme.typography.fontWeightLarge,
      color: "#555555",
    },
  },

  headerRisk: {
    width: "80px",
    [theme.breakpoints.between("sm", "md")]: {
      width: "65px",
    },
    [theme.breakpoints.down("lg")]: {
      width: "55px",
    },
    "& span": {
      fontSize: "13px !important",
      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: theme.typography.fontWeightLarge,
      color: "#555555",
    },
  },

  headerTimeSheet: {
    width: "127px",
    [theme.breakpoints.between("sm", "md")]: {
      width: " 80px",
    },
    [theme.breakpoints.down("lg")]: {
      width: "80px",
    },
    "& span": {
      fontSize: "13px !important",
      fontFamily: theme.typography.fontFamilyLato,
      fontWeight: theme.typography.fontWeightLarge,
      color: "#555555",
    },
  },

  mainApiFlex: {
    flex: "1",
  },
});

export default AliKeyCompStyle;
