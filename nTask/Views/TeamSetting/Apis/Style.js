const Style = theme => ({
  container: {
    width: "100%",
    // overflowY: "scroll",
    height: "calc(100vh - 255px)",
    background: theme.palette.common.white,
    border: "1px solid #F1F1F1",
    borderRadius: " 6px",
    paddingTop: "24px",
    paddingBottom: "30px",
    paddingLeft: " 20px",
    paddingRight: "20px",
    position: "relative",
    [theme.breakpoints.between("sm", "md")]: {
      width: "1000px",
    },
    [theme.breakpoints.down("sm")]: {
      width: "612px",
      overflow:"hidden"
    },
  },

  // createapi style
});

export default Style;
