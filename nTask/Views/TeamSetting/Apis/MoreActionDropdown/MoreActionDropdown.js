import React, { useState, useRef, memo, useEffect } from "react";
import DropdownMenu from "../../../../components/Dropdown/DropdownMenu";
import CustomIconButton from "../../../../components/Buttons/CustomIconButton";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import withStyles from "@material-ui/core/styles/withStyles";
import MoreActionDropdownStyles from "./MoreActionDropdown.style";
import CustomMenuList from "../../../../components/MenuList/CustomMenuList";
import CustomListItem from "../../../../components/ListItem/CustomListItem";
import { withSnackbar } from "notistack";
import { compose } from "redux";
import isEqual from "lodash/isEqual";
import "react-toastify/dist/ReactToastify.css";
import EditIcon from "../../../../components/Icons/EditIcon";
import IconDisable from "../../../../components/Icons/IconDisable";
import DeleteIcons from "../../../../components/Icons/DeleteIcons";
import SvgIcon from "@material-ui/core/SvgIcon";
import { useSelector } from "react-redux";
import CircleTickIcon from "../../../../components/Icons/CircleTickIcon";
import { deleteApis, disableApis } from "../../../../redux/actions/apiKeys";
// import constants from "../constants/types";
function areEqual(prevProps, nextProps) {
  return isEqual(prevProps, nextProps);
  /*
  return true if passing nextProps to render would return
  the same result as passing prevProps to render,
  otherwise return false
  */
}

const MoreActionDropdown = memo(props => {
  const [open, setOpen] = useState(false);
  const [disableKey, setDisableKey] = useState(true);
  const anchorEl = useRef(null);
  const {
    classes,
    btnProps,
    handleEnable,
    apiKeyData,
    handleApiDelete,
    handleDisable,
    handleOpenDrawer,
    handleSaveChanges,
  } = props;
  const handleClose = event => {
    // Function closes dropdown
    setOpen(null);
  };
  const handleDropdownOpen = event => {
    // Function Opens the dropdown
    setOpen(true);
  };
  // delete api integration and function
  const handleDelete = () => {
    setOpen(false);
    deleteApis(
      apiKeyData._id,
      response => {
        handleApiDelete(apiKeyData._id);
      },
      () => {}
    );
  };
  // Disable function and Disable api integration
  const handleDisableKey = () => {
    setOpen(false);
    handleDisable(apiKeyData);
  };
  // Edit api function
  const handleEditApis = () => {
    handleOpenDrawer(apiKeyData);
    setOpen(false);
    handleSaveChanges(apiKeyData);
  };
  const isOpen = Boolean(open);
  return (
    <>
      <CustomIconButton
        btnType="transparent"
        buttonRef={node => (anchorEl.current = node)}
        onClick={handleDropdownOpen}
        style={{ padding: 0 }}
        {...btnProps}>
        <MoreVerticalIcon className={classes.ellipsesIcon} />
      </CustomIconButton>
      <DropdownMenu
        open={isOpen}
        closeAction={handleClose}
        anchorEl={anchorEl.current}
        size={"xsmall"}
        placement="bottom-end"
        disablePortal={false}>
        <CustomMenuList>
          <CustomListItem
            rootProps={{ onClick: e => handleEditApis() }}
            icon={
              <SvgIcon viewBox="0 0 14 13.95" className={classes.editIcon}>
                <EditIcon />
              </SvgIcon>
            }>
            <label className={classes.editLabel}> Edit Api</label>
          </CustomListItem>
          {apiKeyData.isDisabled ? (
            <CustomListItem
              rootProps={{ onClick: e => handleEnable(apiKeyData) }}
              icon={
                <CircleTickIcon
                  style={{ color: "rgba(150, 150, 150, 1)", fontSize: "14px" }}
                />
              }>
              <label className={classes.disableLabel}> Enable Key</label>
            </CustomListItem>
          ) : (
            <CustomListItem
              rootProps={{ onClick: e => handleDisableKey(apiKeyData._id) }}
              icon={
                <IconDisable
                  style={{ color: "rgba(150, 150, 150, 1)", fontSize: "14px" }}
                />
              }>
              <label className={classes.disableLabel}> Disable Key</label>
            </CustomListItem>
          )}
          <CustomListItem rootProps={{ onClick: e => handleDelete() }} icon={<DeleteIcons />}>
            <label className={classes.deleteLabel}> Delete Key</label>
          </CustomListItem>
        </CustomMenuList>
      </DropdownMenu>
    </>
  );
});

export default compose(
  withSnackbar,
  withStyles(MoreActionDropdownStyles, { withTheme: true })
)(MoreActionDropdown);
