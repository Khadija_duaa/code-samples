const MoreActionDropdownStyles = theme => ({
  ellipsesIcon: {
    color: theme.palette.icon.gray600,
  },
  snackBarHeadingCnt: {
    marginLeft: 10,
  },
  snackBarContent: {
    margin: 0,
    fontSize: "12px !important",
  },
  // icon: {
  //   fontSize: "14px !important",
  //   marginRight: "7px !important",
  //   marginBottom: "-1px !important"
  // },
  styleDropDownItem: {
    display: "flex",
    alignItems: "center",
  },
  editLabel: {
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: theme.typography.fontWeightRegular,
    color: theme.palette.text.primary,
    "&:hover": {
      cursor: "pointer",
    },
  },
  disableLabel: {
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: theme.typography.fontWeightRegular,
    color: theme.palette.text.primary,
    "&:hover": {
      cursor: "pointer",
    },
  },
  deleteLabel: {
    fontSize: "13px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: theme.typography.fontWeightRegular,
    color: "#EB5151",
    "&:hover": {
      cursor: "pointer",
    },
  },
  editIcon: {
    color: theme.palette.icon.gray400,
    fontSize: "14px !important",
  },
  blockIcon: {
    display: "flex",
    alignItems: "center",
  },
  editHover: {
    "&:hover": {
      cursor: "pointer",
    },
  },
});

export default MoreActionDropdownStyles;
