export const headerApiDD = [
    {
        label: 'All APIs',
        value: 'allapis',
        id: 1,
        obj: {}
    },
    {
        label: 'Active/Enabled APIs',
        value: 'active/enabled',
        id: 2,
        obj: {}
    },
    {
        label: 'Inactive/Disabled APIs',
        value: 'inactive/disabled',
        id: 3,
        obj: {}
    },
]

export const multipleTasks = [
    {
        label: "Projects",
        descripation: "Allow projects with all the linked tasks.",
        checked: false,
        id: 1
    },
    {
        label: "Tasks",
        descripation: "Allow tasks and all the details including custom fields.",
        checked: false,
        id: 2

    },
    {
        label: "Issues",
        descripation: "Allow issues and all the details.",
        checked: false,
        id: 3
    },
    {
        label: "Risks",
        descripation: "Allow risks and all the details including risk matrix.",
        checked: false,
        id: 4
    },
    {
        label: "TimeSheet",
        descripation: "Allow TimeSheet and all the details including TimeSheet.",
        checked: false,
        id: 5
    }
]

