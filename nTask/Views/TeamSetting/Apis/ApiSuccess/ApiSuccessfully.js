import React, { useEffect, useState } from "react";
import TickApiIcon from "../../../../components/Icons/TickApiIcon";
import DefaultTextField from "../../../../components/Form/TextField";
import { withStyles } from "@material-ui/core/styles";
import ApiSuccessStyle from "./ApiSuccess.Style";
import { CopyToClipboard } from "react-copy-to-clipboard";
import CustomButton from "../../../../components/Buttons/CustomButton";
import IconCopy from "../../../../components/Icons/TaskActionIcons/IconCopy";
import SvgIcon from "@material-ui/core/SvgIcon";
import VisibilityIcon from "@material-ui/icons/Visibility";
import DefaultCheckbox from "../../../../components/Form/Checkbox";

function ApiSuccessfully({ classes, handleCloseDrawer, data, handleGetApis, saveData }) {
  const [showPassword, setShowPassword] = useState(true);
  const [keyCopied, setKeyCopied] = useState(false);
  const handleShowPassword = () => {
    setShowPassword(prevState => !prevState);
  };
  const handleDone = () => {
    handleGetApis();
    handleCloseDrawer();
  };
  const handleChange = () => {
    setKeyCopied(!keyCopied);
  };
  useEffect(() => {
    if (saveData) setKeyCopied(true);
  }, []);
  return (
    <>
      <div className={classes.mainCnt}>
        <div className={classes.content}>
          <TickApiIcon />
          <h1>{saveData ? "API Updated Successfully" : "API Created Successfully"}</h1>
          <p>
            {saveData
              ? "API has been updated with following credentials."
              : " New API has been created with following credentials."}
          </p>
        </div>
        <div className={classes.apiInput}>
          <DefaultTextField
            fullWidth
            label="API Name"
            error={false}
            errorState={false}
            errorMessage={""}
            formControlStyles={{ marginTop: "24px", marginBottom: "16px", fontSize: "13px" }}
            defaultProps={{
              type: "text",
              value: data.name || "",
              placeholder: "name",
              autoFocus: false,
              inputProps: { maxLength: 250 },
              style: {},
              disabled: false,
              autoComplete: "off",
              inputRef: () => {},
            }}
            customInputClass={{
              input: classes.innerInputStyle,
            }}
          />
          <DefaultTextField
            fullWidth
            label="API ID"
            error={false}
            errorState={false}
            errorMessage={""}
            formControlStyles={{ marginBottom: 0, fontSize: "13px" }}
            defaultProps={{
              type: "text",
              value: data.apiKey || "",
              autoFocus: false,
              inputProps: { maxLength: 250 },
              style: {},
              disabled: false,
              autoComplete: "off",
              inputRef: () => {},
              onChange: e => handleInputChange(e),
              endAdornment: (
                <div className={classes.innerBtn}>
                  <CopyToClipboard text={data.apiKey}>
                    <SvgIcon className={classes.iconCopyStyle}>
                      <IconCopy>Copy</IconCopy>
                    </SvgIcon>
                  </CopyToClipboard>
                </div>
              ),
            }}
            customInputClass={{
              input: classes.innerInputStyle,
            }}
          />
        </div>
        <div className={classes.passwordField}>
          {saveData ? null : (
            <DefaultTextField
              fullWidth
              label="Secret Key"
              error={false}
              errorState={false}
              errorMessage={""}
              formControlStyles={{ fontSize: "13px", marginBottom: 0 }}
              defaultProps={{
                type: `${showPassword ? "text" : "password"}`,
                value: data.secretKey || "",
                autoFocus: false,
                inputProps: { maxLength: 250 },
                style: {},
                disabled: false,
                autoComplete: "off",
                inputRef: () => {},
                endAdornment: (
                  <div className={classes.innerBtn}>
                    <span className={classes.mainIconSec}>
                      {" "}
                      <VisibilityIcon
                        onClick={handleShowPassword}
                        className={classes.visibilityIconStyle}>
                        Show
                      </VisibilityIcon>
                    </span>
                    <CopyToClipboard text={data.secretKey}>
                      <SvgIcon className={classes.iconCopyStyle}>
                        <IconCopy>Copy</IconCopy>
                      </SvgIcon>
                    </CopyToClipboard>
                  </div>
                ),
              }}
              customInputClass={{
                input: classes.innerInputStyle,
              }}
            />
          )}

          {!saveData && (
            <>
              <div className={classes.checkboxCnt}>
                <DefaultCheckbox
                  checkboxStyles={{ padding: "1px" }}
                  checked={keyCopied}
                  onChange={handleChange}
                  fontSize={18}
                  color={"#0090ff"}
                  disabled={false}
                />
                <span className={classes.checkboxLabel}>Yes, I have copied my secret Key</span>
              </div>

              <div className={classes.secretKeySec}>
                <p>
                  Secret key is only shown once. We recommend to download the secret key and keep it
                  at safe place.
                </p>
              </div>
            </>
          )}
        </div>
        <div className={classes.footerBtn}>
          <CustomButton
            variant="contained"
            disabled={!keyCopied}
            btnType="plain"
            className={classes.doneStyles}
            style={{
              width: "100%",
              padding: "10px 14px",
              height: "36px",
              color: keyCopied ? "#ffffff" : "#555555",
              background: keyCopied ? "#0090ff" : "#DDDDDD",
            }}
            onClick={handleDone}>
            Done
          </CustomButton>
        </div>
      </div>
    </>
  );
}

export default withStyles(ApiSuccessStyle, { withTheme: true })(ApiSuccessfully);
