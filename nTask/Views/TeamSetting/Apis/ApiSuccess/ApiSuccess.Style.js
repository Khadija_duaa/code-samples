const ApiSuccessStyle = (theme) => ({
    mainCnt: {
        width: 400,
        boxShadow: "-4px 0px 8px #00000014",
        textAlign: "center",
        paddingTop: 100,
        paddingLeft: 15,
        paddingRight: 15,
    },
    apiInput: {
        marginTop: 20,
    },
    content: {
        marginTop: 20,
        "& h1": {
            fontSize: "18px !important",
            fontFamily: theme.typography.fontFamilyLato,
            fontWeight: theme.typography.fontWeightLarge,
            color: "#111111",
            marginTop: 20,
        },
        "& p": {
            fontSize: "14px !important",
            fontFamily: theme.typography.fontFamilyLato,
            fontWeight: theme.typography.fontWeightRegular,
            color: theme.palette.secondary.dark,
        },
    },
    passwordField: {
        display: "flex",
        marginTop: 16,
        alignItems: "baseline",
        flexDirection: "column",
    },
    innerBtn: {
        display: "flex",
        alignItems: "center"
    },
    footerBtn: {
        marginTop: 50,
        marginBottom: 20,
    },
    showBtn: {
        marginLeft: 8,
        marginRight: 8,
        color: "#111111",
        "&:hover": {
            border: `1px solid ${theme.palette.border.lightBorder}`,
        }
    },
    innerInputStyle: {
        padding: "10px 12px",
        fontSize: "13px !important",
        fontFamily: theme.typography.fontFamilyLato,
        fontWeight: theme.typography.fontWeightRegular,
    },
    checkboxLabel: {
        fontFamily: theme.typography.fontFamilyLato,
        fontWeight: theme.typography.fontWeightRegular,
        fontSize: "13px !important",
        marginLeft:5,
    },
    visibilityIconStyle: {
        marginRight: "12px",
        marginTop: "1px",
        color: "#7E7E7E",
        width: "18px",

        "&:hover": {
            cursor: "pointer",
        }
    },
    iconCopyStyle: {
        marginTop: "10px",
        marginLeft: "12px",
        color: "#7E7E7E",
        "&:hover": {
            cursor: "pointer",
        }
    },
    mainIconSec: {
        borderRight: "1px solid #CCCCCC",
        height: "25px",
    },
    secretKeySec: {
        marginTop: "10px",
        width: "100%",
        /* height: 82px; */
        background: "#FFF5DB",
        textAlign: "left",
        padding: "6px 12px",
        lineHeight: "20px",
        borderRadius: "4px",

        "& p": {
            fontSize: "13px !important",
            fontFamily: theme.typography.fontFamilyLato,
            fontWeight: theme.typography.fontWeightRegular,
            color: "#5E4500",
            margin: 0
        },
    },
    checkbox1: {
        margin: 0,
        padding: 0,
    },
    checkboxCnt:{
        display:"flex",
        alignItems:"center",
        marginTop: 10
    },
    doneStyles:{
"&:hover":{
    boxShadow:"none"
}
    }
})

export default ApiSuccessStyle