import React, { useState, useEffect } from 'react'
import { withStyles } from "@material-ui/core/styles";
import Style from '../Apis/Style';
import Header from '../Apis/Header/Header';
import CreateApi from '../Apis/Create/CreateApi'
import ApiSuccessfully from './ApiSuccess/ApiSuccessfully';
import CustomDrawer from '../../../components/Drawer/CustomDrawer';
import ApiKeyComp from './DoneApi/ApiKeyComp';
import { getApis } from '../../../redux/actions/apiKeys';
import { useSelector } from 'react-redux';

function Apis({ classes }) {
    const [drawer, setDrawer] = useState(false);
    const [apisData, setApisData] = useState([])
    const [showSuccessDrawer, setSuccessDrawar] = useState(false)
    const [newApi, setNewApi] = useState({})
    const [apiSaveData, setSaveApiData] = useState(null);
    const [searchTerm, setSearchTerm] = useState("");
    const [filter, setFilter] = useState(1);

    const handleInputChange = (e, value) => {
        setSearchTerm(e.target.value)
    }
    const handleSelectFilter = (type, option) => {
        setFilter(option.id)
    }

    const { profile } = useSelector((state) => {
        return { profile: state.profile.data }
    })
    const handleOpenDrawer = () => {
        setDrawer(true)
    }
    const handleCloseDrawer = () => {
        setDrawer(false)
        setSaveApiData(null)
        setSuccessDrawar(false)
    }
    const handleShowDrawar = () => {
        setSuccessDrawar(true)
    }
    const handleSaveChanges = (data) => {
        setSaveApiData(data)
    }
    const handleUpdateData = (data) => {
        let updatedData = apisData.map(item => {
            if (item._id === data._id) return data;
            else return item;
        })
        setApisData(updatedData);
    }
    const handleAddNewApi = (data) => {
        setNewApi(data);
    }
    const handleGetApis = () => {
        getApis(profile.activeTeam, (response) => {
            setApisData(response.data.data)
        }, () => { })
    }
    useEffect(() => {
        handleGetApis()
    }, [])

    return (
        <>
            <div className={classes.container}>
                <Header handleOpenDrawer={handleOpenDrawer} searchTerm={searchTerm} handleInputChange={handleInputChange} handleSelectFilter={handleSelectFilter} />
                <CustomDrawer
                    open={drawer}
                    closeDrawer={handleCloseDrawer}
                    hideHeader={true}
                    drawerWidth={400}
                    hideCloseIcon={true}
                >
                    {!showSuccessDrawer && <CreateApi handleAddNewApi={handleAddNewApi} handleUpdateData={handleUpdateData} apisData={apisData} handleShowDrawar={handleShowDrawar} handleCloseDrawer={handleCloseDrawer} isEdit={true} setNewApi={setNewApi} saveData={apiSaveData} />}
                    {showSuccessDrawer && <ApiSuccessfully handleGetApis={handleGetApis} handleCloseDrawer={handleCloseDrawer} data={newApi} saveData={apiSaveData}/>}
                </CustomDrawer>
                <ApiKeyComp filter={filter} searchTerm={searchTerm} handleOpenDrawer={handleOpenDrawer} handleUpdateData={handleUpdateData} handleSaveChanges={handleSaveChanges} apisData={apisData} setApisData={setApisData}  />

            </div>
        </>
    )
}
Apis.defaultProps = {
    title: "",
    open: false,
    hideHeader: false,
}
export default withStyles((Style), { withTheme: true })(Apis)
