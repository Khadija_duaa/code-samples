import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import menuStyles from "../../assets/jss/components/menu";
import CustomButton from "../../components/Buttons/CustomButton";
import SelectionMenu from "../../components/Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import combineStyles from "../../utils/mergeStyles";
import DownArrow from "@material-ui/icons/ArrowDropDown";
import { blockWorkspace, activateWorkspace } from "../../redux/actions/team";
import { FetchWorkspaceInfo } from "../../redux/actions/workspace";
import { FetchUserInfo } from "../../redux/actions/profile";
import { FormattedMessage } from "react-intl";
class WorkspaceStatusDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      pickerOpen: false,
      archiveFlag: false,
      unArchiveFlag: false,
      deleteFlag: false,
      newRoleDialogOpen: false,
      selectedValue: "Active"
    };
  }
  handleOperations = (event, value) => {
    event.stopPropagation();
    const { workspace, profileState } = this.props;
    this.setState({ open: false, selectedValue: value });

    if (value == "Active") {
      activateWorkspace(
        workspace.teamId,
        //Callback function to be executed in case of success
        () => { this.props.FetchUserInfo(() => { }) }
      );
    } else {
      blockWorkspace(
        workspace.teamId,
        //Callback function to be executed in case of success
        () => {
          this.props.FetchUserInfo(() => { });
          //This condition is to check if the disabled workspace is your active workspace or not
          // if (workspace.teamId == profileState.loggedInTeam) { /**The below function is executed if disabled workspace is your active workspace so you switch to another workspace */
          //   //
          //   this.props.FetchUserInfo((response) => {

          //     // this.props.FetchWorkspaceInfo(response.data.teamId, () => {
          //     //   // this.props.history.push("/tasks");

          //     // }, () => {
          //     //   // this.props.history.push(
          //     //   //   `/teams/${response.data.team.companyUrl}`
          //     //   // );
          //     // });
          //   });
          // }
        }
      );
    }
  };
  changeSelectedValue = value => { };
  handleClose = event => {
    this.setState({ open: false, pickerOpen: false });
  };
  handlePickerClose = event => {
    this.setState({ pickerOpen: false });
  };

  handleClick = (event, placement) => {
    this.setState(state => ({
      open: state.placement !== placement || !state.open,
      placement
    }));
  };
  handleDialogOpen = () => {
    this.setState({ newRoleDialogOpen: true, open: false, pickerOpen: false });
  };
  handleDialogClose = () => {
    this.setState({ newRoleDialogOpen: false });
  };
  componentDidUpdate(props) {
    if (props.value !== this.props.value) {
      this.setState({ selectedValue: this.props.value });
    }
  }
  componentDidMount() {
    const { workspace } = this.props;
    this.setState({ selectedValue: workspace.isActive ? "Active" : "Blocked" })
  }
  render() {
    const { classes, theme, defaultRoles } = this.props;
    const { open, newRoleDialogOpen, selectedValue } = this.state;
    return (
      <ClickAwayListener onClickAway={this.handleClose}>
        <div>
          <CustomButton
            onClick={event => {
              this.handleClick(event, "bottom-end");
            }}
            buttonRef={node => {
              this.anchorEl = node;
            }}
            style={{
              minWidth: 85,
              padding: "4px 5px 4px 10px",
              display: "flex",
              cursor: "default",
              justifyContent: "space-between",
              textAlign: 'initial',
              background:
                selectedValue == "Active"
                  ? theme.palette.background.emrald
                  : theme.palette.background.danger,
              color: theme.palette.common.white
            }}
            btnType="white"
            variant="contained"
          >

            <span style={{ minWidth: 50 }}>{selectedValue == "Active" ? <FormattedMessage id="common.action.active.label" defaultMessage="Active" /> : <FormattedMessage id="common.action.block.label" defaultMessage="Blocked" />}</span>
            <DownArrow
              htmlColor={theme.palette.common.white}
              className={classes.dropdownArrow}
              style={{ marginLeft: 10 }}
            />
          </CustomButton>

          <SelectionMenu
            open={open}
            closeAction={this.handleClose}
            placement="bottom-start"
            style={{ width: 100 }}
            anchorRef={this.anchorEl}
            list={
              <List disablePadding={true}>
                <ListItem
                  button
                  disableRipple
                  classes={{ selected: classes.statusMenuItemSelected }}
                  onClick={event => this.handleOperations(event, "Active")}
                >
                  <ListItemText
                    primary={<FormattedMessage id="common.action.active.label" defaultMessage="Active" />}
                    classes={{
                      primary: classes.statusItemText
                    }}
                  />
                </ListItem>
                <ListItem
                  button
                  disableRipple
                  classes={{ selected: classes.statusMenuItemSelected }}
                  onClick={event => this.handleOperations(event, "Blocked")}
                >
                  <ListItemText
                    primary={<FormattedMessage id="common.action.block.label" defaultMessage="Blocked" />}
                    classes={{
                      primary: classes.statusItemText
                    }}
                  />
                </ListItem>
              </List>
            }
          />
        </div>
      </ClickAwayListener>
    );
  }
}


const mapStateToProps = (state, ownProps) => {
  return {
    profileState: state.profile.data
  }
}
export default compose(
  withRouter,
  withStyles(menuStyles, {
    withTheme: true
  }),
  connect(
    mapStateToProps,
    {
      FetchUserInfo,
      FetchWorkspaceInfo
    }
  )
)(WorkspaceStatusDropdown);