const Style = theme => ({
    container: {
        width: "100%",
        // overflowY: "scroll",
        height: "calc(100vh - 255px)",
        background: theme.palette.common.white,
        border: "1px solid #F1F1F1",
        borderRadius: " 6px",
        paddingTop: "24px",
        paddingBottom: "30px",
        paddingLeft: " 20px",
        paddingRight: "20px",
        position: "relative",
        [theme.breakpoints.between("sm", "md")]: {
            width: "1000px",
        },
        [theme.breakpoints.down("sm")]: {
            width: "612px",
            overflow: "hidden"
        },
        '& *': {
            fontFamily:theme.typography.fontFamilyLato
        }
    },
    lineSpacing:{
        marginBottom:6
    },
    subContainer: {
        width: "100%",
        marginTop: 15,
        background: "#E6FAF4",
        display: "flex",
        padding: 15,
        borderRadius: 8,
        alignItems: "center" 
    },
    infoContainer:{
        paddingLeft: 20,
        display: "flex",
        /* align-items: center; */
        flexDirection: "column",
    },
    dataLocationIcon: {
        width: 54,
        height: 54,
    },
    emailLinkColor:{
        color:theme.palette.text.green
    },
    spanWeigth:{
        fontWeight:theme.typography.fontWeightLarge
    }
    // createapi style
});

export default Style;