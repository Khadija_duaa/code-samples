import React, { useState, useEffect } from 'react'
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Style from './Style'
import { useSelector } from 'react-redux';
import dataLocationIcon from "../../../assets/images/iconDataCenter.svg";
import { dataLocationData } from '../../../helper/dataCenterDropdownData'
const DataCenter = ({ classes }) => {

    const { profile } = useSelector((state) => {
        return { profile: state.profile.data }
    })
    const dataLocation = profile.teams.find(team => team.companyId === profile.activeTeam)?.dataLocation;
    const dataLocationName = dataLocation ? dataLocationData.find(d => d.value == dataLocation).label : 'United States';
    return (
        <>
            <div className={classes.container}>
                <Typography variant="h1"  >Data Location</Typography>
                <div className={classes.subContainer}>
                    <img src={dataLocationIcon} className={classes.dataLocationIcon} />
                    <div className={classes.infoContainer}>
                        <Typography variant="h3" className={classes.lineSpacing}  >Your current regional data center is: <span className={classes.spanWeigth}>{dataLocationName}</span></Typography>
                        <Typography variant="h5"  >If you want to change your regional data center, please contact us at <a href="mailto:support@ntaskmanager.com" className={classes.emailLinkColor}>support@ntaskmanager.com</a></Typography>
                    </div>
                </div>
            </div>
        </>
    )
}

export default withStyles((Style), { withTheme: true })(DataCenter)