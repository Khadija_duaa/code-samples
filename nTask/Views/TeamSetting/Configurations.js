import React, { useEffect, useRef, useState } from "react";
import styles from "./styles";
import { withStyles } from "@material-ui/core/styles";
import { useDispatch, useSelector } from "react-redux";
import DefaultSwitch from "../../components/Form/Switch";
import CustomTooltip from "../../components/Tooltip/Tooltip";
import { updateTeamConfig, updateWeekendsConfig, updateTaskEffortTeamConfig, updateTimeSettingTeamConfig } from "../../redux/actions/team";
import { FormControl, ListItemText, MenuItem, MenuList, SvgIcon } from "@material-ui/core";
import CustomButton from "../../components/Buttons/CustomButton";
import QuickFilterIcon from "../../components/Icons/QuickFilterIcon";
import { FormattedMessage } from "react-intl";
import PlainMenu from "../../components/Menu/menu";
import isEmpty from "lodash/isEmpty";
import cloneDeep from "lodash/cloneDeep";

const defaultTeamObj = {
    config: {
        isProjectFieldMandatory: false,
        isUserTasksEffortMandatory: false,
    },
};
const weekDaysData = [
    { day: 'Sunday', id: 0 },
    { day: 'Monday', id: 1 },
    { day: 'Tuesday', id: 2 },
    { day: 'Wednesday', id: 3 },
    { day: 'Thursday', id: 4 },
    { day: 'Friday', id: 5 },
    { day: 'Saturday', id: 6 },
]
function Configuration(props) {
    const dispatch = useDispatch();
    const { profileState } = useSelector(state => {
        return {
            profileState: state.profile.data,
        };
    });
    const { classes, showSnackBar = () => { } } = props;
    const { activeTeam, teams } = profileState;
    const currentTeam = teams.find(t => t.companyId === activeTeam) || defaultTeamObj;

    const [checked, setChecked] = useState(currentTeam.config.isProjectFieldMandatory);
    const [taskEffortsChecked, setTaskEffortsChecked] = useState(currentTeam.config.isUserTasksEffortMandatory);
    const [timeSettingchecked, setTimeSettingchecked] = useState(currentTeam.config.isCustomTime);


    const [weekendCheck, setWeekendCheck] = useState(currentTeam.IsApplyWeekendToTaskDuration);

    const [weekDays, setWeekDays] = useState(currentTeam.weekEndDays ? currentTeam.weekEndDays : []);
    const [open, setOpen] = useState(false);
    const anchorEl = useRef();

    const handleChange = () => {
        setChecked(!checked);
        const obj = {
            companyId: currentTeam.companyId,
            isProjectFieldMandatory: !checked,
        };
        updateTeamConfig(
            dispatch,
            obj,
            succ => {
                showSnackBar(
                    `Configuration is now set to ${checked ? 'OFF' : 'ON'} in all workspaces.`,
                    'info')
                // console.log("-------------success");
            },
            fail => {
                // console.log("-------------fail");
            }
        );
    };

    const handleChangeTaskEfforts = () => {
        setTaskEffortsChecked(!taskEffortsChecked);
        const obj = {
            companyId: currentTeam.companyId,
            isUserTasksEffortMandatory: !taskEffortsChecked,
        };
        updateTaskEffortTeamConfig(
            dispatch,
            obj,
            succ => {
                showSnackBar(
                    `Configuration is now set to ${taskEffortsChecked ? 'OFF' : 'ON'} in all workspaces.`,
                    'info')
                // console.log("-------------success");
            },
            fail => {
                // console.log("-------------fail");
            }
        );
    };
    const handleChangeTimeSetting = () => {
        setTimeSettingchecked(!timeSettingchecked);
        const obj = {
            companyId: currentTeam.companyId,
            isCustomTime: !timeSettingchecked,
        };
        updateTimeSettingTeamConfig(
            dispatch,
            obj,
            succ => {
                showSnackBar(
                    `Configuration is now set to ${timeSettingchecked ? 'OFF' : 'ON'} in all workspaces.`,
                    'info')
                // console.log("-------------success");
            },
            fail => {
                // console.log("-------------fail");
            }
        );
    };

    useEffect(() => {
        setChecked(currentTeam.config.isProjectFieldMandatory);
        setWeekDays(currentTeam.weekEndDays ? currentTeam.weekEndDays : []);
        setWeekendCheck(currentTeam.IsApplyWeekendToTaskDuration);
    }, [activeTeam]);
    // toggle weekend configuration on and off
    const handleChangeWeekendConfiguration = () => {
        setWeekendCheck(!weekendCheck);
        updateWeekendsConfig(
            dispatch,
            {
                companyId: currentTeam.companyId,
                IsApplyWeekendToTaskDuration: !weekendCheck,
            },
            succ => { },
            fail => { }
        );
    };
    // select weekend days dropdown
    const handleSelectWeekend = value => {
        let tempArray = cloneDeep(weekDays);
        if (weekDays.find(t => t.id == value.id)) {
            tempArray = tempArray.filter(t => t.id != value.id);
        } else {
            tempArray.push(value);
        }
        setWeekDays(tempArray);
        updateWeekendsConfig(
            dispatch,
            { weekenddays: tempArray, companyId: currentTeam.companyId, },
            succ => { },
            fail => { }
        );
    }
    const handleOpen = value => {
        setOpen(!open)
    }
    const handleClose = () => {
        setOpen(false);
    }
    return (
        <>
            {/* project mandatory check */}
            <div className={classes.configCnt}>
                <div className={classes.configText}>
                    <p>
                        Project selection is mandatory/required when creating a new task
                        <CustomTooltip
                            size="Large"
                            helptext={
                                <>When this setting is on or off, it applies to all workspaces in the team.</>
                            }
                            iconType="help"
                            position="static"
                        />
                    </p>
                    <DefaultSwitch size="medium" checked={checked} onChange={handleChange} />
                </div>
            </div>
            {/* task reminder */}
            <div className={classes.configCnt}>
                <div className={classes.configText}>
                    <p>
                        Remind User to add task efforts before marking it in final status
                        <CustomTooltip
                            size="Large"
                            helptext={
                                <>When enabled, this setting will reminder user to add effort for task when it's status is changed to Final state.</>
                            }
                            iconType="help"
                            position="static"
                        />
                    </p>
                    <DefaultSwitch size="medium" checked={taskEffortsChecked} onChange={handleChangeTaskEfforts} />
                </div>
            </div>
            {/* gloabal timing check */}
            <div className={classes.configCnt}>
                <div className={classes.configText}>
                    <p>
                        Enable 'Time logging' minutes interval to 30.
                        <CustomTooltip
                            size="Large"
                            helptext={
                                <>When enabled, this setting will allow users to log time only between 0 and 30 minutes.</>
                            }
                            iconType="help"
                            position="static"
                        />
                    </p>
                    <DefaultSwitch size="medium" checked={timeSettingchecked} onChange={handleChangeTimeSetting} />
                </div>
            </div>


            {/* hide for these configuration */}
            {false &&
                <>
                    <div className={classes.configCnt}>
                        <div className={classes.configText}>
                            <p>
                                Weekends
                                <CustomTooltip
                                    size="Large"
                                    helptext={<>......</>}
                                    iconType="help"
                                    position="static"
                                />
                            </p>
                            <FormControl className={classes.formControl}>
                                <CustomButton
                                    onClick={event => handleOpen(event, "bottom-end")}
                                    id="taskQuickFilterButton"
                                    buttonRef={node => {
                                        anchorEl.current = node;
                                    }}
                                    style={{
                                        // padding: customFilterSelect ? "4px 8px 4px 4px" : "3px 8px 3px 4px",
                                        padding: "4px 8px 4px 4px",
                                        borderRadius: "4px",
                                        display: "flex",
                                        justifyContent: "space-between",
                                        minWidth: "auto",
                                        whiteSpace: "nowrap",
                                    }}
                                    btnType={!isEmpty(weekDays) ? "lightBlue" : "white"}
                                    variant="contained">
                                    <SvgIcon
                                        classes={{ root: classes.quickFilterIconSize }}
                                        viewBox="0 0 24 24"
                                        className={
                                            !isEmpty(weekDays)
                                                ? classes.qckFfilterIconSelected
                                                : classes.qckFilterIconSvg
                                        }>
                                        <QuickFilterIcon />
                                    </SvgIcon>
                                    <span
                                        className={
                                            !isEmpty(weekDays)
                                                ? classes.qckFilterLblSelected
                                                : classes.qckFilterLbl
                                        }>
                                        {weekDays.length ? 'Weekends : ' : 'None'} {" "}
                                    </span>
                                    <span className={classes.checkname}>
                                        {weekDays.map(item => (
                                            item.day + ', '
                                        ))}
                                    </span>
                                </CustomButton>
                                <PlainMenu
                                    open={open}
                                    closeAction={handleClose}
                                    placement="bottom-start"
                                    anchorRef={anchorEl.current}
                                    style={{ width: 280 }}
                                    offset="0 15px">
                                    <MenuList disablePadding>
                                        <MenuItem classes={{ root: classes.menuHeadingItem }}>
                                            Week days
                                        </MenuItem>
                                        {weekDaysData.map(item => (
                                            <MenuItem
                                                key={item.day}
                                                value={item.day}
                                                className={`${classes.statusMenuItemCnt} ${weekDays.find(t => t.id == item.id) ? classes.selectedValue : ""}`}
                                                classes={{ selected: classes.statusMenuItemSelected }}
                                                onClick={() => handleSelectWeekend(item)}>
                                                <ListItemText
                                                    primary={item.day}
                                                    classes={{ primary: classes.weekDaysItemText }}
                                                />
                                            </MenuItem>
                                        ))}
                                    </MenuList>
                                </PlainMenu>
                            </FormControl>
                        </div>
                    </div>
                    <div className={classes.configCnt}>
                        <div className={classes.configText}>
                            <p>
                                Exclude weekend in date calculation
                                {/* <CustomTooltip
                            size="Large"
                            helptext={
                                <>When this setting is on or off, it applies to all workspaces in the team.</>
                            }
                            iconType="help"
                            position="static"
                        /> */}
                            </p>
                            <DefaultSwitch checked={weekendCheck} onChange={handleChangeWeekendConfiguration} />
                        </div>
                    </div>
                </>
            }
        </>
    );
}

export default withStyles(styles, { withTheme: true })(Configuration);
