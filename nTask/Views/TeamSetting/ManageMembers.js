import React, { Component, Fragment } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Typography from "@material-ui/core/Typography";
import DefaultTextField from "../../components/Form/TextField";
import NotificationMessage from "../../components/NotificationMessages/NotificationMessages";
import CustomAvatar from "../../components/Avatar/Avatar";
import CustomIconButton from "../../components/Buttons/CustomIconButton";
import EmailIcon from "@material-ui/icons/Email";
import InfoIcon from "@material-ui/icons/Info";
import CustomButton from "../../components/Buttons/CustomButton";
import MoreActionDropdown from "./MoreActionDropdown";
import SelectSearchDropdown from "../../components/Dropdown/SelectSearchDropdown/SelectSearchDropdown";
import { validateEmailAddress } from "../../redux/actions/teamMembers";
import CloseIcon from "@material-ui/icons/Close";
import IconButton from "../../components/Buttons/IconButton";
import Tooltip from "@material-ui/core/Tooltip";

import {
  InviteTeamMembers,
  ResendInviteTeamMembers,
  ChangeTeamUserRole,
  EnableTeamMember,
} from "../../redux/actions/team";
import { filterMutlipleEmailsInput, validateMultifieldEmails } from "../../utils/formValidations";
import DeleteConfirmDialog from "../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import ActionConfirmation from "../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import {
  generateWorkspaceData,
  generateRoleData,
  generateTeamRoleData,
  generateTeamAdminRoleData,
} from "../../helper/generateSelectData";
import InviteMember from "../../components/InviteMember/InviteMember";
import UpdateSubscription from "../billing/UpdateSubscription/UpdateSubscription";
import Constants from "../../components/constants/planConstant";
import DefaultSwitch from "../../components/Form/Switch";
import ErrorIcon from "@material-ui/icons/Error";
import CheckCirclrIcon from "@material-ui/icons/CheckCircle";
import WatchLaterIcon from "@material-ui/icons/WatchLater";
import moment from "moment";
import upgradePro from "../../assets/images/upgrade_pro.png";
import { showUpgradePlan } from "../../redux/actions/userBillPlan";
import { injectIntl, FormattedMessage } from "react-intl";

class ManageMembers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      EmailAddressError: "",
      EmailAddressErrorText: "",
      emailList: [],
      confirmDelete: false,
      confirmDisable: false,
      confirmEnable: false,
      confirminvite: false,
      confirmSelectedRole: false,
      loggedInTeam: "",
      userId: "",
      email: "",
      changedRoleValue: "Member",
      previousRoleValue: "Member",
      role: {},
      teamName: "",
      btnQuery: "",
      disable: true,
      resendInviteEmailBtnQuery: "",

      changeRoleBtnQuery: "",
      enableMemberBtnQuery: "",
      disableMemberBtnQuery: "",
      reset: false,
      roleId: "003",
      showUpdateSubscription: false,
      minimumUser: 1,
      inviteRole: generateRoleData().find(r => {
        return r.id == "003";
      }),
      inviteHistoryArr: {},
      userFilter: {
        label: this.props.intl.formatMessage({
          id: "workspace-settings.user-management.add-members.filter-dropdown.all-user.label",
          defaultMessage: "All Users",
        }),
        value: "allUsers",
      },
    };
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.closeEnableMember = this.closeEnableMember.bind(this);
    this.inviteMemberConfrimation = this.inviteMemberConfrimation.bind(this);
    this.closeinviteMember = this.closeinviteMember.bind(this);
    this.handleSelectedRole = this.handleSelectedRole.bind(this);
    this.closeSelectedRolePopup = this.closeSelectedRolePopup.bind(this);
    this.updateSelectedRole = this.updateSelectedRole.bind(this);
  }

  componentDidMount() {
    const { profileState } = this.props;
    const loggedInTeam =
      profileState.data && profileState.data.teams
        ? profileState.data.teams.find(x => x.companyId === profileState.data.activeTeam)
        : {};
    //const emails = localStorage.getItem("MomEmails");
    this.setState({
      loggedInTeam,
      teamName: loggedInTeam.companyName,
      // emailList: emails ? emails : "",
      // disable: emails ? false : true,
    });
  }
  updateSelectedRole() {
    const { changedRoleValue, memberId } = this.state;
    const object = {
      memberId: memberId,
      role: changedRoleValue,
    };
    // this.setState({memberId: role == 'Admin' ? "Member" : "Admin"})
    this.setState({ changeRoleBtnQuery: "progress" }, () => {
      this.props.ChangeTeamUserRole(
        object,
        response => {
          this.setState({
            changeRoleBtnQuery: "",
            changedRoleValue: "",
            memberId: "",
            confirmSelectedRole: false,
          });
        },
        error => {
          this.setState({
            changeRoleBtnQuery: "",
          });
          this.props.showSnackBar(
            error.data, "error"
          );
        }
      );
    });
  }
  handleSelectedRole(role, memberId) {
    this.setState({
      [role]: memberId,
      confirmSelectedRole: true,
      changedRoleValue: role,
      memberId: memberId,
    });
  }
  closeSelectedRolePopup(reset) {
    this.setState({
      confirmSelectedRole: false,
      role: this.state.role,
      reset: reset === true ? false : true,
    });
  }
  openEnableConfirm = userId => {
    let paidMembers = this.props.profileState.data.teamMember.filter(
      item => item.isDeleted == false && (item.isActive == null || item.isActive == true)
    ).length;
    let companyInfo = this.props.profileState.data.teams.find(
      item => item.companyId == this.props.profileState.data.activeTeam
    );
    const { allowedMembers, subscriptionDetails } = companyInfo;
    const { paymentPlanTitle } = subscriptionDetails;
    if (Constants.isPlanPaid(paymentPlanTitle) && allowedMembers < paidMembers + 1) {
      let minimumUser = paidMembers + 1 - allowedMembers;
      this.setState({
        showUpdateSubscription: true,
        minimumUser: minimumUser,
        userId,
        mode: "enabled",
      });
    } else {
      if (paidMembers >= allowedMembers) {
        this.props.showSnackBar(
          `Your team has reached maximum limit (${allowedMembers} users), upgrade to add more users.`
        );
      } else this.setState({ confirmEnable: true, userId });
    }
  };
  enableMember = () => {
    const { userId } = this.state;
    this.setState({ enableMemberBtnQuery: "progress" });
    let workspaceMemberObj = this.props.profileState.data.member.allMembers.find(m => {
      return m.userId == userId;
    });
    this.props.EnableTeamMember(
      userId,
      workspaceMemberObj,
      response => {
        this.setState({
          enableMemberBtnQuery: "",
          confirmEnable: false,
        });
      },
      error => {
        this.setState({
          enableMemberBtnQuery: "",
        });
        if (error && error.status === 500) {
          // self.showSnackBar('Server throws error','error');
        }
      }
    );
  };
  // enableMember(userId) {
  //   this.setState({ confirmEnable: true, userId });
  // }
  closeEnableMember() {
    this.setState({ confirmEnable: false, userId: "" });
  }
  updateEmailList = (emails, currentEmail) => {
    this.setState({ emailList: emails });
    // this.props.validateEmailAddress(
    //   currentEmail,
    //   response => {
    //     this.setState({ emailList: emails });
    //   },
    //   error => {
    //     this.setState({
    //       EmailAddressError: true,
    //       EmailAddressErrorText: error.data,
    //     });
    //   }
    // );
  };

  checkInvite = () => {
    let emailListObj = {
      userEmails: this.state.emailList,
      workspaceId: this.state.workspace
        ? this.state.workspace.map(w => {
          return w.id;
        })
        : null,
    };
    let paidMembers = this.props.profileState.data.teamMember.filter(
      item => item.isDeleted == false && (item.isActive == null || item.isActive == true)
    ).length;
    let companyInfo = this.props.profileState.data.teams.find(
      // Getting Active Team object
      item => item.companyId == this.props.profileState.data.activeTeam
    );
    const { allowedMembers, subscriptionDetails } = companyInfo;
    const { paymentPlanTitle } = subscriptionDetails;
    if (
      Constants.isPlanPaid(paymentPlanTitle) &&
      allowedMembers < paidMembers + emailListObj.userEmails.length
    ) {
      let minimumUser = paidMembers + emailListObj.userEmails.length - allowedMembers;
      this.setState({
        showUpdateSubscription: true,
        minimumUser: minimumUser,
        mode: "invite",
      });
    } else {
      this.sendMemberInvite();
    }
  };
  sendMemberInvite = () => {
    let emailListObj = {
      userEmails: this.state.emailList,
      workspaceId: this.state.workspace
        ? this.state.workspace.map(w => {
          return w.id;
        })
        : null,
    };
    this.setState({ btnQuery: "progress" }, () => {
      this.props.InviteTeamMembers(
        emailListObj,
        response => {
          this.setState({
            emailList: [],
            workspace: [],
            disable: true,
            btnQuery: "",
          });
        },
        error => {
          this.setState({ btnQuery: "" });
          if (error && error.data === 500) {
            this.props.showSnackBar('Server throws error', 'error');
          } else {
            this.setState({
              EmailAddressError: true,
              EmailAddressErrorText: error.data.message,
            });
            const err = error.data && error.data.message;
            const message = err || "Server throws error";
            this.props.showSnackBar(message, 'error');
          }
        }
      );
    });
  };

  inviteMemberConfrimation() {
    this.setState({ resendInviteEmailBtnQuery: "progress" }, () => {
      this.props.ResendInviteTeamMembers(
        this.state.email,
        response => {
          this.setState({
            confirminvite: false,
            userId: "",
            email: "",
            resendInviteEmailBtnQuery: "",
          });
        },
        error => {
          this.setState({
            confirminvite: false,
            resendInviteEmailBtnQuery: "",
          });
          if (error && error.status === 500) {
            // self.showSnackBar('Server throws error','error');
          }
        }
      );
    });
  }
  inviteMember = x => {
    this.setState({ confirminvite: true, userId: x.userId, email: x.email });
  };
  showHistoryMember = x => {
    const { inviteHistoryArr } = this.state;
    inviteHistoryArr[x.userId] = true;
    this.setState({ inviteHistoryArr });
  };
  hideHistoryMember = x => {
    const { inviteHistoryArr } = this.state;
    inviteHistoryArr[x.userId] = false;
    this.setState({ inviteHistoryArr });
  };
  closeinviteMember() {
    this.setState({ confirminvite: false, userId: "", email: "" });
  }

  handleEmailInput = name => event => {
    let inputValue = event.target.value;
    //  localStorage.setItem("MomEmails", inputValue);
    inputValue = filterMutlipleEmailsInput(inputValue);
    this.setState({
      emailList: inputValue,
      EmailAddressError: false,
      EmailAddressErrorText: "",
      disable: inputValue ? false : true,
    });
  };
  handleSelectChange(value) {
    this.setState({ feedbackType: value });
  }

  checkForSelfInvitation = userEmails => {
    return userEmails.find(email => {
      return email === this.props.profileState.data.email;
    });
  };

  checkForAlreadyInvitedMember = (userEmail, selectedTeamMembers) => {
    return selectedTeamMembers.find(obj => {
      return obj.email === userEmail;
    });
  };

  handleInput = name => event => {
    let inputValue = event.target.value;
    this.setState({
      [name]: inputValue,
      EmailAddressError: false,
      EmailAddressErrorText: "",
      disable: inputValue && name !== "teamMember" ? false : true,
    });
  };

  //Local state is update for dropdown so object can be made to send update to backend with API
  //Updating local state when option is selected in workspaces dropdown
  handleOptionsSelect = (type, value) => {
    this.setState({ [type]: value });
  };
  //Updating local state when select is cleared in workspaces dropdown
  handleClearOption = () => {
    this.setState({ workspace: [] });
  };
  exitUpdateSubscription = () => {
    this.setState({ showUpdateSubscription: false, minimumUser: 1, mode: "" });
  };
  subscriptionSuccessfull = () => {
    this.setState({ showUpdateSubscription: false, minimumUser: 1 }, () => {
      const { mode } = this.state;
      if (mode == "invite") {
        this.sendMemberInvite();
      } else if (mode == "enabled") {
        this.enableMember();
      }
    });
  };

  sortingMembersByRole = (selectedTeam, userFilter) => {
    /** function for sorting the invited members by its role */
    let users;
    switch (userFilter) {
      case "allUsers":
        let owner = selectedTeam.filter(a => {
          /** members having role owner */
          return a.role == "Owner";
        });
        let admin = selectedTeam.filter(a => {
          /** members having role admin */
          return a.role == "Admin";
        });
        let teamAdmin = selectedTeam.filter(a => {
          /** members having role team admin */
          return a.role == "Team Admin";
        });
        let member = selectedTeam.filter(a => {
          /** members having role memeber */
          return a.role == "Member";
        });
        let activeMember = selectedTeam.filter(a => {
          /** members which are invited and enabled */
          return a.role == "Member" && a.isActive == true;
        });
        let disableMember = selectedTeam.filter(a => {
          /** members which are invited but disabled */
          return a.role == "Member" && a.isActive == false;
        });
        let invitedMember = selectedTeam.filter(a => {
          /** members which are invited and they did not accept their invitation */
          return a.role == "Member" && a.isActive == null;
        });
        users = [
          ...owner,
          ...admin,
          ...teamAdmin,
          ...activeMember,
          ...invitedMember,
          ...disableMember,
        ];
        break;
      case "teamAdmin":
        users = selectedTeam.filter(a => {
          /** members having role admin */
          return a.role == "Admin";
        });
        break;
      case "teamMembers":
        users = selectedTeam.filter(a => {
          /** members having role memeber */
          return a.role == "Member" && a.isActive != null;
        });
        break;
      case "disabledUsers":
        users = selectedTeam.filter(a => {
          /** members which are invited but disabled */
          return a.isActive == false;
        });
        break;
      case "enabledUsers":
        users = selectedTeam.filter(a => {
          /** members which are invited and enabled */
          return a.isActive == true;
        });
        break;
      case "invitedUsers":
        users = selectedTeam.filter(a => {
          /** members which are invited and they did not accept their invitation */
          return a.isActive == null;
        });
        break;
    }
    return users;
  };
  generateDropDownData = per => {
    if (per) {
      return generateTeamAdminRoleData();
    } else {
      return generateTeamRoleData();
    }
  };
  generateFilterDropDown = () => {
    let dd = [
      {
        label: this.props.intl.formatMessage({
          id: "workspace-settings.user-management.add-members.filter-dropdown.all-user.label",
          defaultMessage: "All Users",
        }),
        value: "allUsers",
      },
      {
        label: this.props.intl.formatMessage({
          id: "workspace-settings.user-management.add-members.filter-dropdown.team-admin.label",
          defaultMessage: "Team Admin",
        }),
        value: "teamAdmin",
      },
      {
        label: this.props.intl.formatMessage({
          id: "project.dev.resources.headers.teammebers",
          defaultMessage: "Team Members",
        }),
        value: "teamMembers",
      },
      {
        label: this.props.intl.formatMessage({
          id: "workspace-settings.user-management.add-members.filter-dropdown.disabled-user.label",
          defaultMessage: "Disabled User",
        }),
        value: "disabledUsers",
      },
      {
        label: this.props.intl.formatMessage({
          id: "workspace-settings.user-management.add-members.filter-dropdown.enable-user.label",
          defaultMessage: "Enabled User",
        }),
        value: "enabledUsers",
      },
      {
        label: this.props.intl.formatMessage({
          id: "workspace-settings.user-management.add-members.filter-dropdown.invited-user.label",
          defaultMessage: "Invited User",
        }),
        value: "invitedUsers",
      },
    ];
    let data = dd.map((item, index) => {
      return {
        label: item.label,
        value: item.value,
      };
    });
    return data;
  };
  showUpgradeDialog = () => {
    let data = {
      show: true,
      mode: Constants.UPGRADEPLANMODE,
    };
    this.props.showUpgradePlan(data);
  };
  render() {
    const {
      EmailAddressErrorText,
      EmailAddressError,
      emailList,
      teamMember,
      teamErrorText,
      teamError,
      confirmDelete,
      confirminvite,
      confirmSelectedRole,
      teamName,
      btnQuery,
      inviteRole,
      disable,
      resendInviteEmailBtnQuery,
      changeRoleBtnQuery,
      workspace,
      showUpdateSubscription,
      minimumUser,
      inviteHistoryArr,
      userFilter,
      confirmEnable,
      enableMemberBtnQuery,
      changedRoleValue,
      memberId,
    } = this.state;

    const { classes, theme, profileState, companyInfo } = this.props;
    const intl = this.props.intl;
    let sortedInvitedMembers = this.sortingMembersByRole(
      profileState.data.teamMember,
      userFilter.value
    ).filter(m => !m.isDeleted);
    if (teamMember && teamMember != "") {
      sortedInvitedMembers = sortedInvitedMembers.filter(
        item =>
          item.fullName.toLowerCase().includes(teamMember.toLowerCase()) ||
          item.email.toLowerCase().includes(teamMember.toLowerCase())
      );
    }

    let currentTeamUser = profileState.data.teamMember.find(m => {
      return m.userId == profileState.data.userId;
    });
    let successBtnText = "";
    let headingText = "";
    let msgText = "";
    let note = "";
    let roleChangeUser;
    if (memberId != "") {
      roleChangeUser = profileState.data.teamMember.find(m => {
        return m.userId == memberId;
      });
    }
    if (roleChangeUser && roleChangeUser != "") {
      successBtnText =
        changedRoleValue == "Admin"
          ? this.props.intl.formatMessage({
            id: "team-settings.user-management.promote-admin.title",
            defaultMessage: "Yes, Promote to Admin",
          })
          : this.props.intl.formatMessage({
            id: "team-settings.user-management.demote-admin.title",
            defaultMessage: "Yes, Demote to Member",
          });
      headingText =
        changedRoleValue == "Admin"
          ? this.props.intl.formatMessage({
            id: "team-settings.user-management.promote-admin.action",
            defaultMessage: "Promote to Admin",
          })
          : this.props.intl.formatMessage({
            id: "team-settings.user-management.demote-admin.action",
            defaultMessage: "Demote to Member",
          });
      msgText =
        changedRoleValue == "Admin"
          ? this.props.intl.formatMessage(
            {
              id: "team-settings.user-management.promote-admin.message",
              defaultMessage: `Looks like you're assiging Admin Role to "${roleChangeUser.fullName}". Doing so will grant this user full Admin Access across all your Workspaces under "${companyInfo.companyName}".`,
            },
            {
              name: roleChangeUser.fullName,
              companyName: companyInfo.companyName,
            }
          )
          : this.props.intl.formatMessage(
            {
              id: "team-settings.user-management.demote-admin.message",
              defaultMessage: `Looks like you are demoting "${roleChangeUser.fullName}" to Member Role. Doing so will assign Member level access to this user across all your Workspaces under "${companyInfo.companyName}".`,
            },
            {
              name: roleChangeUser.fullName,
              companyName: companyInfo.companyName,
            }
          );
      note =
        changedRoleValue == "Admin"
          ? this.props.intl.formatMessage({
            id: "team-settings.user-management.promote-admin.messagea",
            defaultMessage: `Are you sure you want to promote this user to Admin?`,
          })
          : this.props.intl.formatMessage({
            id: "team-settings.user-management.demote-admin.messagea",
            defaultMessage: `Are you sure you want to demote this user to Member?`,
          });
    }
    const { paymentPlanTitle } = companyInfo.subscriptionDetails;
    let showInvite = true;
    let invitedMembers = profileState.data.teamMember.filter(
      item => item.isDeleted == false && (item.isActive == null || item.isActive == true)
    ).length;
    let disableInvited = false;
    let addBtnToolTip = "";
    let planName = Constants.DisplayPlanName(paymentPlanTitle);
    const { allowedMembers } = companyInfo;

    if (Constants.isPlanNonPaid(paymentPlanTitle)) {
      showInvite = invitedMembers < allowedMembers;
      if (showInvite && emailList.length + invitedMembers >= allowedMembers) {
        disableInvited = true;
        addBtnToolTip = `Your team has reached maximum limit (${allowedMembers} users), upgrade to add more users.`;
      }
    }

    return (
      <>
        {showUpdateSubscription && (
          <UpdateSubscription
            minimumUser={minimumUser}
            exitUpdateSubscription={this.exitUpdateSubscription}
            subscriptionSuccessfull={this.subscriptionSuccessfull}
            showSnackBar={this.props.showSnackBar}
            currentTeamUserRole={currentTeamUser.role}
          />
        )}
        <div className={classes.manageMembersCnt}>
          <div style={{ minWidth: 594 }}>
            {showInvite ? (
              <div className={classes.inviteTeamCnt}>
                <Typography variant="h4" className={classes.inviteHeadingText}>
                  <FormattedMessage
                    id="team-settings.user-management.invite-members.form.invite.label"
                    values={{ name: teamName }}
                    defaultMessage={`Invite Team Members to ` + teamName}
                  />
                </Typography>
                <InviteMember
                  emailList={emailList}
                  updateEmailList={this.updateEmailList}
                  disableInvited={disableInvited}
                  addBtnToolTip={addBtnToolTip}
                />
                <div className={classes.selectWithBtnCnt}>
                  <div className={classes.inviteDropdownsCnt}>
                    <SelectSearchDropdown
                      data={() => {
                        return generateWorkspaceData();
                      }}
                      styles={{ margin: 0, marginRight: 20, flex: 1 }}
                      isClearable={true}
                      label=""
                      selectChange={this.handleOptionsSelect}
                      selectClear={this.handleClearOption}
                      type="workspace"
                      selectedValue={workspace}
                      placeholder={
                        <FormattedMessage
                          id="team-settings.user-management.invite-members.form.select-workspace.placeholder"
                          defaultMessage="Select Workspace(s)"
                        />
                      }
                      isMulti={true}
                    />
                  </div>
                  <CustomButton
                    onClick={this.checkInvite}
                    btnType="success"
                    variant="contained"
                    query={btnQuery}
                    disabled={btnQuery === "progress" || emailList.length < 1}
                    style={{ height: 37 }}>
                    <FormattedMessage
                      id="team-settings.user-management.invite-members.form.send-invite-button.label"
                      defaultMessage="Send Invite"
                    />
                  </CustomButton>
                </div>
              </div>
            ) : (
              <div className={classes.upgradePlanCnt}>
                <div className={classes.upgradeLeftSide}>
                  <img src={upgradePro} alt="Pro Plan Image" className={classes.noPlanIcon} />
                </div>
                <div className={classes.upgradeRightSide}>
                  <Typography variant="h2" className={classes.upgradeHeadingText}>
                    Upgrade To Add Unlimited Team Members
                  </Typography>
                  <Typography variant="h5" className={classes.upgreadeNormalText}>
                    Your team has reached maximum limit ({invitedMembers} users) on nTask {planName}{" "}
                    plan.
                    <br />
                    Upgrade now to unleash full potential.
                  </Typography>
                  {this.props.isOwner && (
                    <CustomButton
                      onClick={this.showUpgradeDialog}
                      btnType="blue"
                      variant="outlined"
                      query={btnQuery}
                      style={{ height: 37 }}>
                      Upgrade Now
                    </CustomButton>
                  )}
                </div>
              </div>
            )}

            <div className={classes.workspaceMembersCnt}>
              <div className={classes.userListCnt}>
                <div style={{ display: "flex" }}>
                  <DefaultTextField
                    labelProps={{
                      shrink: true,
                    }}
                    errorState={teamError}
                    errorMessage={teamErrorText}
                    defaultProps={{
                      id: "teamMember",
                      onChange: this.handleInput("teamMember"),
                      value: teamMember,
                      placeholder: intl.formatMessage({
                        id: "common.search-assignee.label",
                        defaultMessage: "Search",
                      }),
                    }}
                    formControlStyles={{ width: "70%", padding: "0px" }}
                  />
                  <div style={{ width: 170, marginLeft: 20 }}>
                    <SelectSearchDropdown
                      data={() => {
                        return this.generateFilterDropDown();
                      }}
                      label=""
                      selectChange={this.handleOptionsSelect}
                      type="userFilter"
                      styles={{ margin: 0 }}
                      selectedValue={this.generateFilterDropDown().find(s => {
                        return s.value == userFilter.value;
                      })}
                      placeholder="Select Priority"
                      icon={false}
                      isMulti={false}
                    />
                  </div>
                </div>
                <ul className={classes.userList}>
                  {sortedInvitedMembers.map(x => {
                    let lastInvite = x.lastInvitedDate
                      ? " on " + new Date(x.lastInvitedDate).toDateString()
                      : "";
                    return (
                      <li key={x.userId}>
                        <div className={classes.listDiv}>
                          <div className="flex_center_start_row">
                            <CustomAvatar
                              otherMember={{
                                imageUrl: x.imageUrl,
                                fullName: x.fullName,
                                lastName: "",
                                email: x.email,
                                isOnline: x.isOnline,
                                isOwner: x.isOwner,
                              }}
                              size="small"
                              disableCard={true}
                            />
                            <div style={{ marginLeft: 10 }}>
                              <Typography
                                variant="h6"
                              >
                                <span
                                  className={classes.fontSizeLarge}
                                  style={{
                                    fontSize: "13px",
                                    fontWeight: theme.typography.fontWeightMedium,
                                  }}>
                                  {x.fullName}{" "}
                                </span>

                                <span className={classes.disabledUserText}>
                                  {x.isActive === false && !x.isOwner
                                    ? `· ${this.props.intl.formatMessage({
                                      id: "team-settings.user-management.user-status.disabled",
                                      defaultMessage: "User Disabled",
                                    })}`
                                    : ""}
                                </span>
                                <span className={classes.disabledUserText}>
                                  {x.status == "Rejected"
                                    ? `· ${this.props.intl.formatMessage({
                                      id: "team-settings.user-management.user-status.rejected",
                                      defaultMessage: "Rejected",
                                    })}`
                                    : ""}
                                </span>

                                <span className={classes.invitedUserText}>
                                  {x.isActive === null && !x.isOwner && x.status !== "Rejected"
                                    ? `· ${this.props.intl.formatMessage({
                                      id: "team-settings.user-management.user-status.invited",
                                      defaultMessage: "Invited",
                                    })}`
                                    : ""}
                                </span>
                                {/* <span className={classes.EnabledUserText}>
                                  {x.isActive && !x.isOwner ? "Enabled" : ""}
                                </span> */}
                                {/* <span className={classes.EnabledUserText}>
                                  {x.isOwner ? "Owner" : ""}
                                </span> */}
                              </Typography>
                              <Typography variant="caption"
                                className={classes.fontSizeSmall}
                                style={{ fontSize: "11px" }}>
                                {x.email}
                              </Typography>
                            </div>
                          </div>
                          <div className="flex_center_start_row">
                            <>
                              {!x.isOwner ? (
                                <>
                                  {/* {x.isActive === false &&
                                  permissions &&
                                  permissions.enableMember ? (
                                    <span
                                      className={classes.enableUserLink}
                                      onClick={this.enableMember.bind(
                                        this,
                                        x.userId
                                      )}
                                    >
                                      {x.isActive !== null ? "Enable User" : ""}
                                    </span>
                                  ) : null} */}

                                  {/* {x.isActive &&
                              permissions &&
                              permissions.disableMember ? (
                                <span
                                  className={classes.disableUserLink}
                                  onClick={}
                                >
                                  {x.isActive !== null ? "Disable User" : ""}
                                </span>
                              ) : null} */}

                                  {x.isActive === null ? (
                                    <>
                                      <CustomIconButton
                                        btnType="white"
                                        style={{
                                          marginRight: 0,
                                          fontSize: "12px",
                                          padding: 12,
                                          borderRightWidth: 0,
                                          borderTopRightRadius: 0,
                                          borderBottomRightRadius: 0,
                                          color: theme.palette.common.black,
                                        }}
                                        onClick={this.inviteMember.bind(this, x)}>
                                        <FormattedMessage
                                          id="team-settings.user-management.re-invite-button.label"
                                          defaultMessage="Re-invite"
                                        />
                                      </CustomIconButton>
                                      <CustomIconButton
                                        btnType="white"
                                        style={{
                                          borderTopLeftRadius: 0,
                                          borderBottomLeftRadius: 0,
                                        }}
                                        onClick={() => this.showHistoryMember(x)}>
                                        <Tooltip
                                          title={
                                            <FormattedMessage
                                              id="team-settings.user-management.invitation-history.label"
                                              defaultMessage="Invitation History"
                                            />
                                          }>
                                          <InfoIcon
                                            htmlColor={theme.palette.secondary.medDark}
                                            className={classes.infoIcon}
                                          />
                                        </Tooltip>
                                      </CustomIconButton>
                                    </>
                                  ) : null}
                                  {/* {x.isActive === null &&
                              permissions &&
                              permissions.removeMembers ? (
                                <IconButton
                                  btnType="smallFilledGray"
                                  style={{ padding: 0 }}
                                  onClick={}
                                >
                                  <CloseIcon
                                    fontSize="small"
                                    htmlColor={theme.palette.common.white}
                                  />
                                </IconButton>
                              ) : null} */}
                                </>
                              ) : null}
                            </>
                            {(x.isActive || x.isActive == null) &&
                              ((x.isOwner && x.isActive) || (x.isOwner && x.isActive === null)
                                ? null
                                : (x.role == "Admin" || x.role == "Member") &&
                                  currentTeamUser.role == "Admin"
                                  ? null
                                  : x.isActive !== null && (
                                    <div className={classes.adminCtrl}>
                                      {/* <SelectIconMenu
                                              handleSelectedRole={this.handleSelectedRole}
                                              iconType="status"
                                              style={{
                                                width: 200,
                                                marginBottom: 0,
                                                marginRight: 10
                                              }}
                                              isSimpleList={false}
                                              heading="Select Role"
                                              menuType="roles"
                                              isSingle={true}
                                              roles={x}
                                              reset={this.state.reset}
                                              closeSelectedRolePopup={
                                                this.closeSelectedRolePopup
                                              }
                                            /> */}
                                      <span className={classes.display}>
                                        <FormattedMessage
                                          id="workspace-settings.user-management.add-members.filter-dropdown.team-admin.label"
                                          defaultMessage="Team Admin"
                                        />
                                      </span>
                                      <div>
                                      <DefaultSwitch
                                        size="medium"
                                        checked={x.role == "Admin"}
                                        onChange={event =>
                                          this.handleSelectedRole(
                                            x.role == "Admin" ? "Member" : "Admin",
                                            x.userId
                                          )
                                        }
                                        value="Member Role"
                                      />
                                        </div>


                                      {/* <SelectSearchDropdown
                                        // data={() => { return generateTeamRoleData();  }}
                                        data={() => this.generateDropDownData(currentTeamUser.role == 'Admin')}

                                        styles={{
                                          margin: 0,
                                          marginRight: 20,
                                          flex: 1,
                                          width: 100
                                        }}
                                        isClearable={false}
                                        label=""
                                        selectChange={this.handleSelectedRole}
                                        type={x.userId}
                                        selectedValue={
                                          this.state[x.userId]
                                            ? this.state[x.userId]
                                            : generateTeamRoleData().find(r => {
                                              return r.value == x.role;
                                            })
                                        }
                                        placeholder={"Role"}
                                        isMulti={false}
                                      /> */}
                                      {/* <MemberListMenu
                                            handleSelectedRole={this.handleSelectedRole}
                                            isRoles={true}
                                            reset={this.state.reset}
                                            closeSelectedRolePopup={
                                              this.closeSelectedRolePopup
                                            }
                                            roles={x}
                                            /> */}
                                    </div>
                                  ))}
                            {x.isActive === false && !x.isOwner && (
                              <>
                                <span
                                  className={classes.EnabledUserText}
                                  onClick={() => this.openEnableConfirm(x.userId)}>
                                  <FormattedMessage
                                    id="team-settings.user-management.enable-user-button.label"
                                    defaultMessage="Enable User"
                                  />
                                </span>
                                <MoreActionDropdown
                                  isActive={x.isActive}
                                  enableMember={this.enableMember}
                                  disableMembers={this.disableMember}
                                  userId={x.userId}
                                  showSnackBar={this.props.showSnackBar}
                                />
                              </>
                            )}
                            {!x.isOwner ? (
                              x.role == "Admin" && currentTeamUser.role == "Admin" ? (
                                <div className="flex_center_start_row">
                                  <Typography variant="h6">
                                    <FormattedMessage
                                      id="workspace-settings.user-management.add-members.filter-dropdown.team-admin.label"
                                      defaultMessage="Team Admin"
                                    />
                                  </Typography>
                                </div>
                              ) : x.isActive == null || x.isActive == true ? (
                                <Fragment>
                                  {x.role == "Member" &&
                                    currentTeamUser.role == "Admin" &&
                                    x.isActive == true && (
                                      <div className="flex_center_start_row">
                                        <Typography variant="h6">
                                          <FormattedMessage
                                            id="workspace-settings.user-management.add-members.filter-dropdown.team-member.label"
                                            defaultMessage="Team Member"
                                          />
                                        </Typography>
                                      </div>
                                    )}
                                  <MoreActionDropdown
                                    isActive={x.isActive}
                                    enableMember={this.enableMember}
                                    disableMembers={this.disableMember}
                                    userId={x.userId}
                                    showSnackBar={this.props.showSnackBar}
                                  />
                                </Fragment>
                              ) : null
                            ) : (
                              <div className="flex_center_start_row">
                                <Typography variant="h6">
                                  {
                                    <FormattedMessage
                                      id="workspace-settings.user-management.add-members.filter-dropdown.team-owner.label"
                                      defaultMessage="Team Owner"
                                    />
                                  }
                                </Typography>
                              </div>
                            )}
                          </div>
                        </div>
                        {x.inviteHistory && inviteHistoryArr[x.userId] && (
                          <div className={classes.invitationHistory}>
                            <div className={classes.invitationHistoryTitle}>
                              <Typography variant="h4">
                                {
                                  <FormattedMessage
                                    id="team-settings.user-management.invitation-history.label"
                                    defaultMessage="Invitation History"
                                  />
                                }{" "}
                                ({x.inviteHistory.length})
                              </Typography>
                              <IconButton
                                btnType="transparent"
                                onClick={() => this.hideHistoryMember(x)}>
                                <CloseIcon htmlColor={theme.palette.secondary.medDark} />
                              </IconButton>
                            </div>
                            <div className={classes.invitationHistoryHeader}>
                              <Typography variant="body2"
                                className={classes.fontSizeLarge}
                                style={{ width: "40%", fontSize: "13px" }}>
                                <FormattedMessage
                                  id="team-settings.user-management.invitation-history.columns.date-time"
                                  defaultMessage="Date & Time"
                                />
                              </Typography>
                              <Typography variant="body2"
                                className={classes.fontSizeLarge}
                                style={{ width: "30%", fontSize: "13px" }}>
                                <FormattedMessage
                                  id="common.status.label"
                                  defaultMessage="Status"
                                />
                              </Typography>
                              <Typography variant="body2"
                                className={classes.fontSizeLarge}
                                style={{ width: "30%", fontSize: "13px" }}>
                                <FormattedMessage
                                  id="invitations.invite.title"
                                  defaultMessage="Invited By"
                                />
                              </Typography>
                            </div>
                            <ul className={classes.historyList}>
                              {x.inviteHistory.map(historyItem => {
                                let invitedByUser = profileState.data.teamMember.find(
                                  item => item.userId == historyItem.invitedBy
                                );
                                let date = moment(historyItem.inviteDate).format(
                                  "MMMM DD, YYYY. hh:mm:ss A"
                                );
                                return (
                                  <li>
                                    <div className={classes.historyListDiv}>
                                      <Typography
                                        variant="h6"
                                        className={classes.fontSizeLarge}
                                        style={{ width: "40%", fontSize: "13px" }}>
                                        {date}
                                      </Typography>
                                      <span
                                        style={{
                                          width: "30%",
                                          display: "flex",
                                          alignItems: "center",
                                        }}>
                                        {historyItem.status == "Error" ? (
                                          <>
                                            <ErrorIcon
                                              htmlColor={theme.palette.text.danger}
                                              className={classes.cssIcon}
                                            />
                                            <span
                                              style={{
                                                paddingLeft: 5,
                                                fontSize: "12px",
                                              }}>
                                              <FormattedMessage
                                                id="team-settings.user-management.invitation-history.columns.status.fail"
                                                defaultMessage="Failed"
                                              />
                                            </span>
                                          </>
                                        ) : historyItem.status == "Sent" ? (
                                          <>
                                            <CheckCirclrIcon
                                              htmlColor={theme.palette.text.brightGreen}
                                              className={classes.cssIcon}
                                            />
                                            <span
                                              style={{
                                                paddingLeft: 5,
                                                fontSize: "12px",
                                              }}>
                                              <FormattedMessage
                                                id="team-settings.user-management.invitation-history.columns.status.sent"
                                                defaultMessage="Sent"
                                              />
                                            </span>
                                          </>
                                        ) : historyItem.status == "Pending" ? (
                                          <>
                                            <WatchLaterIcon
                                              htmlColor={theme.palette.text.orange}
                                              className={classes.cssIcon}
                                            />
                                            <span
                                              style={{
                                                paddingLeft: 5,
                                                fontSize: "12px",
                                              }}>
                                              <FormattedMessage
                                                id="team-settings.user-management.invitation-history.columns.status.pending"
                                                defaultMessage="Pending"
                                              />
                                            </span>
                                          </>
                                        ) : null}
                                      </span>
                                      <div
                                        style={{
                                          width: "30%",
                                          display: "flex",
                                          alignItems: "center",
                                        }}>
                                        <CustomAvatar
                                          otherMember={{
                                            imageUrl: invitedByUser.imageUrl,
                                            fullName: invitedByUser.fullName,
                                            lastName: "",
                                            email: invitedByUser.email,
                                            isOnline: invitedByUser.isOnline,
                                            isOwner: invitedByUser.isOwner,
                                          }}
                                          size="small"
                                          disableCard={true}
                                        />
                                        <Typography variant="body2" style={{ marginLeft: 10 }}>
                                          {invitedByUser.fullName}
                                        </Typography>
                                      </div>
                                    </div>
                                  </li>
                                );
                              })}
                            </ul>
                          </div>
                        )}
                      </li>
                    );
                  })}
                </ul>
              </div>
            </div>
          </div>
          {showInvite ? (
            <div
              className={classes.infoMessageContainer}
              style={{ width: "25%", marginLeft: 10, marginTop: 0 }}>
              <NotificationMessage
                type="NewInfo"
                iconType="info"
                style={{
                  width: 265,
                  padding: "10px 15px 10px 15px",
                  backgroundColor: theme.palette.background.light,
                  borderRadius: 4,
                }}>
                <FormattedMessage
                  id="team-settings.user-management.invite-members.form.invite.hint"
                  defaultMessage="If you are inviting someone without selecting a Workspace, the invited person will be added to the team: "
                />
                "{companyInfo.companyName}".
                <FormattedMessage
                  id="team-settings.user-management.invite-members.form.invite.hint2"
                  defaultMessage="Selecting a Workspace prior to sending invitations will add users to that Workspace as a member. Don't worry, you can assign roles later!"
                />
              </NotificationMessage>
            </div>
          ) : null}
        </div>

        <ActionConfirmation
          open={confirmEnable}
          closeAction={this.closeEnableMember}
          cancelBtnText={
            <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
          }
          successBtnText={
            <FormattedMessage
              id="team-settings.user-management.confirmation.enable-user.enable-user-button.label"
              defaultMessage="Enable User"
            />
          }
          alignment="center"
          headingText={
            <FormattedMessage
              id="team-settings.user-management.confirmation.enable-user.title"
              defaultMessage="Enable"
            />
          }
          iconType="archive"
          msgText={
            <FormattedMessage
              id="team-settings.user-management.confirmation.enable-user.label"
              defaultMessage="Are you sure you want to enable the selected user?"
            />
          }
          successAction={this.enableMember}
          btnQuery={enableMemberBtnQuery}
        />
        <ActionConfirmation
          open={confirminvite}
          closeAction={this.closeinviteMember}
          cancelBtnText={
            <FormattedMessage id="common.action.cancel.label" defaultMessage="Cancel" />
          }
          successBtnText={
            <FormattedMessage
              id="team-settings.user-management.confirmation.resend-invite.resend-invite-button.label"
              defaultMessage="Resend Invite"
            />
          }
          alignment="center"
          headingText={
            <FormattedMessage
              id="team-settings.user-management.confirmation.resend-invite.title"
              defaultMessage="Resend Invite"
            />
          }
          iconType="archive"
          msgText={
            <FormattedMessage
              id="team-settings.user-management.confirmation.resend-invite.label"
              defaultMessage="Are you sure you want to resend invite to the selected user?"
            />
          }
          successAction={this.inviteMemberConfrimation}
          btnQuery={resendInviteEmailBtnQuery}
        />
        <ActionConfirmation
          open={confirmSelectedRole}
          closeAction={this.closeSelectedRolePopup}
          cancelBtnText={
            <FormattedMessage
              id="profile-settings-dialog.apps-integrations.personal-data.confiramtion-dialog.cancel-button.label"
              defaultMessage="Cancel"
            />
          }
          successBtnText={successBtnText}
          alignment="center"
          headingText={headingText}
          iconType="archive"
          msgText={msgText}
          note={note}
          successAction={this.updateSelectedRole}
          btnQuery={changeRoleBtnQuery}
        />
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    profileState: state.profile,
    isOwner: state.profile.data.teamMember.find(item => item.userId == state.profile.data.userId)
      .isOwner,
    companyInfo: state.profile.data.teams.find(
      item => item.companyId == state.profile.data.activeTeam
    ),
  };
};

export default injectIntl(
  withRouter(
    connect(mapStateToProps, {
      InviteTeamMembers,
      ResendInviteTeamMembers,
      ChangeTeamUserRole,
      validateEmailAddress,
      EnableTeamMember,
      showUpgradePlan,
    })
      (ManageMembers)
  )
);
