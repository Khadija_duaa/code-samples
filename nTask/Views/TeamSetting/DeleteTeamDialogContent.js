import React, { Component, Fragment } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { compose } from "redux";
import Typography from "@material-ui/core/Typography";
import DashIcon from "@material-ui/icons/Remove";
import workspaceSetting from "./styles";
import { withStyles } from "@material-ui/core/styles";
import DefaultTextField from "../../components/Form/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import IconButton from "@material-ui/core/IconButton";
import { FormattedMessage, injectIntl } from "react-intl";
class DeleteTeamDialogContent extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const {
      classes,
      theme,
      step,
      teamNameErrorMessage,
      teamNameError,
      handleTeamNameInput,
      teamName,
      activeTeam
    } = this.props;
    return (
      <div className={classes.deleteTeamDialogContent}>
        {step == 0 ? (
          <div>
            <Typography variant="body2">
              <FormattedMessage id="team-settings.general-information.delete-confirmation.delete-team-sentence.a" defaultMessage="Are you sure you want to permanantly delete this team?" />
              <br />
            </Typography>
            <Typography variant="body2">
              <FormattedMessage id="team-settings.general-information.delete-confirmation.delete-team-sentence.a-b" defaultMessage="Permanently deleting your team will automatically erase:" />
              <br />
            </Typography>
            <ul>
              <li>
                <DashIcon
                  htmlColor={theme.palette.error.light}
                  className={classes.dashIcon}
                />
                <Typography variant="body2"><FormattedMessage id="team-settings.general-information.delete-confirmation.delete-team-sentence.b" defaultMessage="All Workspaces" /> </Typography>
              </li>
              <li>
                <DashIcon
                  htmlColor={theme.palette.error.light}
                  className={classes.dashIcon}
                />

                <Typography variant="body2">
                  <FormattedMessage id="team-settings.general-information.delete-confirmation.delete-team-sentence.c" defaultMessage="All created and assigned tasks" />
                </Typography>
              </li>
              <li>
                <DashIcon
                  htmlColor={theme.palette.error.light}
                  className={classes.dashIcon}
                />

                <Typography variant="body2">
                  <FormattedMessage id="team-settings.general-information.delete-confirmation.delete-team-sentence.d" defaultMessage="All created and assigned projects" />
                </Typography>
              </li>
              <li>
                <DashIcon
                  htmlColor={theme.palette.error.light}
                  className={classes.dashIcon}
                />

                <Typography variant="body2">
                  <FormattedMessage id="team-settings.general-information.delete-confirmation.delete-team-sentence.e" defaultMessage="All timesheets (pending & approved)" />
                </Typography>
              </li>
              <li>
                <DashIcon
                  htmlColor={theme.palette.error.light}
                  className={classes.dashIcon}
                />

                <Typography variant="body2"><FormattedMessage id="team-settings.general-information.delete-confirmation.delete-team-sentence.f" defaultMessage="All comments history" /></Typography>
              </li>
              <li>
                <DashIcon
                  htmlColor={theme.palette.error.light}
                  className={classes.dashIcon}
                />

                <Typography variant="body2"><FormattedMessage id="team-settings.general-information.delete-confirmation.delete-team-sentence.g" defaultMessage="All file attachments" /></Typography>
              </li>
              <li>
                <DashIcon
                  htmlColor={theme.palette.error.light}
                  className={classes.dashIcon}
                />

                <Typography variant="body2"><FormattedMessage id="team-settings.general-information.delete-confirmation.delete-team-sentence.h" defaultMessage="All meetings" /></Typography>
              </li>
              <li>
                <DashIcon
                  htmlColor={theme.palette.error.light}
                  className={classes.dashIcon}
                />

                <Typography variant="body2">
                  <FormattedMessage id="team-settings.general-information.delete-confirmation.delete-team-sentence.i" defaultMessage="All created and assigned issues and risks" />
                </Typography>
              </li>
            </ul>
          </div>
        ) : step == 1 ? (
          <div>
            <Typography variant="body2">
              <FormattedMessage
                id="team-settings.general-information.delete-confirmation.text"
                values={{
                  w: <b><FormattedMessage id="team-settings.general-information.delete-confirmation.warning" defaultMessage="WARNING:" /></b>,
                  c: <b> <FormattedMessage id="team-settings.general-information.delete-confirmation.cannot" defaultMessage="CANNOT" /></b>
                }}
                defaultMessage={`{w} By deleting a team, you will permanently lose all
              your team's workspaces, tasks, projects, meetings, timesheets and
              files shared. You {c} undo this or regain access to your
              team once you proceed.`} /> <br />
              <br />
            </Typography>
            <Typography variant="body2">
              <FormattedMessage
                id="team-settings.general-information.delete-confirmation.text2"
                values={{ name: <b>{activeTeam.companyName}</b> }}
                defaultMessage={`Type {name} in the field below to permanently delete your team and everything in it.`} />
            </Typography>

            <DefaultTextField
              label={<FormattedMessage id="team-settings.general-information.delete-confirmation.form.team-name.label" defaultMessage="Team Name" />}
              fullWidth={true}
              errorState={teamNameError}
              errorMessage={teamNameErrorMessage}
              defaultProps={{
                id: "teamNameInput",
                type: "text",
                onChange: handleTeamNameInput,
                value: teamName,
                autoFocus: true,
                placeholder: this.props.intl.formatMessage({ id: "team-settings.general-information.delete-confirmation.form.team-name.placeholder", defaultMessage: "Enter your team name" })
              }}
            />

          </div>
        ) : null}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    profileState: state.profile
  };
};

export default compose(
  withRouter,
  injectIntl,
  connect(mapStateToProps),
  withStyles(workspaceSetting, { withTheme: true })
)(DeleteTeamDialogContent);
