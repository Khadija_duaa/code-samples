import Grid from "@material-ui/core/Grid";
import LinearProgress from "@material-ui/core/LinearProgress";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import ArrowRight from "@material-ui/icons/KeyboardArrowRight";
import { withSnackbar } from "notistack";
import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import { Route, withRouter } from "react-router-dom";
import { Elements, StripeProvider } from "react-stripe-elements";
import { compose } from "redux";
import Constants, { isAnyBusinessPlan, isBusinessPlan, isBusinessTrial, isEnterprisePlan } from "../../components/constants/planConstant";
import Payment from "../../payment/Payment";
import { calculateContentHeight } from "../../utils/common";
import Billing from "../billing/Billing";
import Apis from "./Apis/Apis";
import DataCenter from "./DataCenter/dataCenter";
import DataExport from "./DataExport/DataExport";
import GeneralSetting from "./GeneralSetting";
import ManageMembers from "./ManageMembers";
import workspaceSetting from "./styles";
import Configuration from "./Configurations";
import CustomButton from "../../components/Buttons/CustomButton";
import UpdatePlan from "./UpdateLifetimePlan/UpdatePlan";
import ActionConfirmation from "../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import { upgradeStorage } from "../../redux/actions/team";
import UpdateSuccess from "../billing/AddBillDetail/UpdateSuccess";
import TeamCalendar from "./TeamCalendar/TeamCalendar.view";
import { getStorageLimit } from "../../redux/actions/sidebarPannel";
// import CalendarSettingTeam from '../../components/CalendarModule/conatainer/CalendarSettingTeam';

const GBValue = 1000000000;
class TeamSetting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0,
      btnQuery: "",
      showConfirmation: false,
      successModal: false,
      storageSize: {
        totalStorage: 0,
        usedStorage: 0,
        percentageUsed: 0,
      }
    };
  }
  componentDidMount() {
    if (this.props.location.state == "inviteMember") {
      this.props.history.push("/team-settings/user-management");
    }
    if (this.props.location.pathname == "/team-settings/billing") {
      this.setState({ selectedIndex: 2 });
    }
    if (this.props.location.pathname == "/team-settings/upgrade-plan") {
      this.setState({ selectedIndex: null });
    }
    if (this.props.location.pathname == "/team-settings/user-management") {
      this.setState({ selectedIndex: 1 });
    }
    if (this.props.location.pathname == "/team-settings/work-calendar") {
      this.setState({ selectedIndex: 8 });
    }
    this.getStorage();
  }
  componentDidUpdate(prevProps, prevState) {
    if (this.props.profileState.data.loggedInTeam === "000000") {
      this.props.history.push("/tasks");
    }
  }

  tabClick = (event, index) => {
    if (index == 2) {
      this.props.history.push("/team-settings/billing");
      this.setState({ selectedIndex: index });
      return;
    }
    if (index == 1) {
      this.props.history.push("/team-settings/user-management");
      this.setState({ selectedIndex: index });
      return;
    }
    if (index === 8) {
      this.props.history.push("/team-settings/work-calendar");
      this.setState({ selectedIndex: index });
      return;
    }
    this.props.history.push("/team-settings");
    this.setState({ selectedIndex: index });
  };
  goBack = () => {
    this.props.history.goBack();
  };
  showSnackBar = (snackBarMessage, type) => {
    const { classes, enqueueSnackbar } = this.props;
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };
  upgradePlan = () => {
    this.props.history.push("/team-settings/upgrade-plan");
    this.setState({ selectedIndex: null });
  };

  openAdditionalRequest = () => {
    this.setState({ showConfirmation: true });
  };
  closeAdditionalRequest = () => {
    this.setState({ showConfirmation: false, btnQuery: "" });
  };
  GetAdditionalStorage = () => {
    this.setState({ btnQuery: "progress" });
    const { company } = this.props;
    const data = {
      TeamId: company.companyId,
      UpgradeDescription: "Get additional 1TB for $49",
    };
    upgradeStorage(
      data,
      (res) => {
        console.log("res", res);
        this.setState({ showConfirmation: false, btnQuery: "", successModal: true });
      },
      (err) => {
        console.log("err", err);
        this.setState({ showConfirmation: false, btnQuery: "" });
      }
    );
  };

  successfullyExit = () => {
    this.setState({ successModal: false });
    // handleBack()
  };
  // get storage details total storage limit and used storage

  getStorage = () => {
    getStorageLimit(
      DOCUMENTS_BASE_URL,
      succ => {
        this.setState({ storageSize: this.calculatePercentage(succ.data.totalStorage, succ.data.usedStorage) })
        // console.log(this.calculatePercentage(succ.data.totalStorage, succ.data.usedStorage))
      },
      fail => {
        this.setState({
          storageSize: {
            totalStorage: 0,
            usedStorage: 0,
            percentageUsed: 0,
          }
        })
      }
    );
  };
  calculatePercentage = (total, used) => {
    let percentageUsed = (used / total) * 100;
    percentageUsed = percentageUsed.toFixed(1);
    return {
      percentageUsed,
      usedStorage: (used / GBValue).toFixed(2),
      totalStorage: (total / GBValue).toFixed(2)
    }
  }
  render() {
    const { classes, theme, company, paidMembers, usedSpace, profileState, location } = this.props;
    const { storageLimit } = company.paymentPlan;
    const { paymentPlanTitle, packageType } = company.subscriptionDetails;
    let { allowedMembers } = company;

    const usedStorage = Constants.isPlanFree(paymentPlanTitle)
      ? usedSpace.toFixed(2)
      : (usedSpace / 1024).toFixed(2);
    const totalStorage = Constants.isPlanFree(paymentPlanTitle)
      ? storageLimit.toFixed(2)
      : (storageLimit / 1024).toFixed(2);
    const storageType = Constants.isPlanFree(paymentPlanTitle) ? "MB" : "GB";
    const storagePercent = Math.round((parseFloat(usedStorage) / parseFloat(totalStorage)) * 100);
    const usedMembers = paidMembers;
    let unlimited = Constants.isPlanPaid(paymentPlanTitle) ? false : true;
    if (Constants.isPlanNonPaid(paymentPlanTitle)) {
      let teamMembers = profileState.data.teamMember.filter(
        (item) => item.isDeleted == false && (item.isActive == null || item.isActive == true)
      ).length;
      allowedMembers = teamMembers > allowedMembers ? teamMembers : allowedMembers;
    }

    const totalMembers = allowedMembers;
    const membersPercent = Math.round((paidMembers / allowedMembers) * 100);
    let currentUser = profileState.data.teamMember.find((m) => {
      return profileState.data.userId == m.userId;
    });

    const { selectedIndex, successModal, showConfirmation, btnQuery, storageSize } = this.state;
    const isLifetimeDeal = () => {
      if (company.promotion == "Lifetime Deal 2020") {
        return true;
      } else {
        return false;
      }
    };
    const getPlanName = () => {
      if (packageType) {
        if (packageType.includes("Tier 5")) {
          return "Supreme Saver";
        } else if (packageType.includes("Tier 4")) {
          return "Ultimate Saver";
        } else if (packageType.includes("Tier 3")) {
          return "Big Saver";
        } else if (packageType.includes("Tier 2")) {
          return "Standard Saver";
        } else if (packageType.includes("Tier 1")) {
          return "Basic Saver";
        } else {
          return paymentPlanTitle;
        }
      } else {
        return paymentPlanTitle;
      }
    };
    return (
      <div
        className={classes.workspaceSettingCnt}
        style={{ height: calculateContentHeight() + 77, overflowY: "auto" }}>
        <div className={classes.workspaceSettingHeader}>
          <div className="flex_center_start_row">
            {/* <LeftArrow
              onClick={this.goBack}
              className={classes.backArrowIcon}
            /> */}

            <Typography variant="h4" classes={{ h1: classes.workspaceHeading }}>
              <FormattedMessage id="team-settings.title" defaultMessage="Team Settings" />
            </Typography>
          </div>
        </div>
        <div className={classes.workspaceSettingContentCnt}>
          <Grid
            container
            direction="row"
            justify="flex-start"
            alignItems="stretch">
            <Grid item classes={{ item: classes.workspaceSettingListCnt }}>
              <List component="nav" classes={{ root: classes.workspaceSettingSideList }}>
                <ListItem
                  selected={selectedIndex === 0}
                  onClick={(event) => this.tabClick(event, 0)}
                  classes={{
                    root: classes.listItem,
                    selected: classes.listItemSelected,
                  }}>
                  <ListItemText
                    primary={
                      <FormattedMessage
                        id="team-settings.general-information.label"
                        defaultMessage="General Information"
                      />
                    }
                    classes={{ primary: classes.workspaceSideListText }}
                  />
                  {selectedIndex === 0 ? <ArrowRight style={{ fontSize: "19px" }} /> : null}
                </ListItem>
                <ListItem
                  selected={location.pathname == "/team-settings/user-management"}
                  onClick={(event) => this.tabClick(event, 1)}
                  style={{ borderTop: "none" }}
                  classes={{
                    root: classes.listItem,
                    selected: classes.listItemSelected,
                  }}>
                  <ListItemText
                    primary={
                      <FormattedMessage
                        id="team-settings.user-management.label"
                        defaultMessage="User Management"
                      />
                    }
                    classes={{ primary: classes.workspaceSideListText }}
                  />
                  {selectedIndex === 1 ? <ArrowRight style={{ fontSize: "19px" }} /> : null}
                </ListItem>
                {company.isApiKeyAccess && (
                  <ListItem
                    selected={selectedIndex === 4}
                    onClick={(event) => this.tabClick(event, 4)}
                    style={{ borderTop: "none" }}
                    classes={{
                      root: classes.listItem,
                      selected: classes.listItemSelected,
                    }}>
                    <ListItemText
                      primary={
                        <FormattedMessage
                          id="team-settings.apikeys.label"
                          defaultMessage="Api Keys"
                        />
                      }
                      classes={{ primary: classes.workspaceSideListText }}
                    />
                    {selectedIndex === 4 ? <ArrowRight style={{ fontSize: "19px" }} /> : null}
                  </ListItem>
                )}
                {currentUser.isOwner && (
                  <>
                    <ListItem
                      selected={location.pathname == "/team-settings/billing"}
                      onClick={(event) => this.tabClick(event, 2)}
                      style={{ borderTop: "none" }}
                      classes={{
                        root: classes.listItem,
                        selected: classes.listItemSelected,
                      }}>
                      <ListItemText
                        primary={
                          <FormattedMessage
                            id="team-settings.billing.label"
                            defaultMessage="Billing"
                          />
                        }
                        classes={{ primary: classes.workspaceSideListText }}
                        theme={theme}
                      />
                      {selectedIndex === 2 ? <ArrowRight style={{ fontSize: "19px" }} /> : null}
                    </ListItem>
                    {!isLifetimeDeal() ? (
                      <ListItem
                        selected={selectedIndex === 3}
                        onClick={(event) => this.tabClick(event, 3)}
                        style={{ borderTop: "none" }}
                        classes={{
                          root: classes.listItem,
                          selected: classes.listItemSelected,
                        }}>
                        <ListItemText
                          primary={
                            <FormattedMessage
                              id="team-settings.payment-method.label"
                              defaultMessage="Payment Method"
                            />
                          }
                          classes={{ primary: classes.workspaceSideListText }}
                        />
                        {selectedIndex === 3 ? <ArrowRight style={{ fontSize: "19px" }} /> : null}
                      </ListItem>
                    ) : null}
                  </>
                )}
                <ListItem
                  selected={selectedIndex === 5}
                  onClick={(event) => this.tabClick(event, 5)}
                  style={{ borderTop: "none" }}
                  classes={{
                    root: classes.listItem,
                    selected: classes.listItemSelected,
                  }}>
                  <ListItemText
                    primary={
                      <FormattedMessage
                        id="team-settings.data-center.label"
                        defaultMessage="Data Center"
                      />
                    }
                    classes={{ primary: classes.workspaceSideListText }}
                  />
                  {selectedIndex === 5 ? <ArrowRight style={{ fontSize: "19px" }} /> : null}
                </ListItem>
                <ListItem
                  selected={selectedIndex === 6}
                  onClick={(event) => this.tabClick(event, 6)}
                  style={{ borderTop: "none" }}
                  classes={{
                    root: classes.listItem,
                    selected: classes.listItemSelected,
                  }}>
                  <ListItemText
                    primary={
                      <FormattedMessage
                        id="team-settings.data-center.label"
                        defaultMessage="Export Data"
                      />
                    }
                    classes={{ primary: classes.workspaceSideListText }}
                  />
                  {selectedIndex === 6 ? <ArrowRight style={{ fontSize: "19px" }} /> : null}
                </ListItem>

                {(isAnyBusinessPlan(paymentPlanTitle) || isEnterprisePlan(paymentPlanTitle)) &&
                  <ListItem
                    selected={selectedIndex === 8}
                    onClick={(event) => this.tabClick(event, 8)}
                    style={{ borderTop: "none" }}
                    classes={{
                      root: classes.listItem,
                      selected: classes.listItemSelected,
                    }}>
                    <ListItemText
                      primary={
                        <FormattedMessage
                          id="team-settings.data-center.label"
                          defaultMessage="Work Schedules"
                        />
                      }
                      classes={{ primary: classes.workspaceSideListText }}
                    />
                    {selectedIndex === 8 ? <ArrowRight style={{ fontSize: 19 }} /> : null}
                  </ListItem>}

                {this.props.profileState.data.userPlan !== "Free" &&
                  this.props.profileState.data.userPlan !== "free2018" &&
                  (currentUser.isOwner || currentUser.role == "Admin") ? (
                  <ListItem
                    selected={selectedIndex === 7}
                    onClick={(event) => this.tabClick(event, 7)}
                    style={{ borderTop: "none" }}
                    classes={{
                      root: classes.listItem,
                      selected: classes.listItemSelected,
                    }}>
                    <ListItemText
                      primary={"Configuration"}
                      classes={{ primary: classes.workspaceSideListText }}
                    />
                    {selectedIndex === 7 ? <ArrowRight style={{ fontSize: "19px" }} /> : null}
                  </ListItem>
                ) : null}
              </List>
              <div className={classes.teamStorageBody}>
                <Typography variant="h6"
                  className={classes.fontSize}
                  style={{ fontSize: "14px" }}>
                  <FormattedMessage
                    id="team-settings.team-storage.label"
                    defaultMessage="Team Storage"
                  />
                </Typography>
                <div className={classes.teamStorageCnt}>
                  <span>
                    {storageSize.usedStorage} / {storageSize.totalStorage} {storageType}{" "}
                    <FormattedMessage id="team-settings.team-storage.used" defaultMessage="used" />
                  </span>
                  <span>{storageSize.percentageUsed} %</span>
                </div>
                <LinearProgress
                  variant="determinate"
                  value={storageSize.percentageUsed}
                  classes={{
                    root: classes.progressBar,
                    barColorPrimary: classes.greenBar,
                  }}
                  style={{ marginTop: 10 }}
                />
                {isLifetimeDeal() ? (
                  <p className={classes.getMoreStorage} onClick={this.openAdditionalRequest}>
                    Get Additional 1TB for $49
                  </p>
                ) : null}
              </div>
              <div className={classes.teamMembersBody}>
                <Typography variant="h6"
                  className={classes.fontSize}
                  style={{ fontSize: "14px" }}>
                  <FormattedMessage
                    id="team-settings.members.team-member.label"
                    defaultMessage="Team Members"
                  />
                </Typography>
                <div className={classes.teamMembersCnt}>
                  <span>
                    <FormattedMessage
                      id="team-settings.members.active-member.label"
                      values={{ m: `${usedMembers} / ${totalMembers} ` }}
                      defaultMessage={`${usedMembers} / ${totalMembers} active members`}
                    />
                  </span>
                  <span>{membersPercent} %</span>
                </div>
                <LinearProgress
                  variant="determinate"
                  value={membersPercent}
                  classes={{
                    root: classes.progressBar,
                    barColorPrimary: classes.greenBar,
                  }}
                  style={{ marginTop: 10 }}
                />
              </div>
              {/* current plan starts here */}
              {isLifetimeDeal() ? (
                <div className={classes.teamCurrentPlan}>
                  <Typography variant="h6"
                    // className={classes.fontSize}
                    className={classes.currentPlan}
                    style={{ fontSize: "14px" }}>
                    Current Plan :
                  </Typography>
                  <Typography
                    variant="h6"
                    // className={classes.fontSize}
                    className={classes.lifeTimeDeal}
                    style={{ fontSize: "14px" }}>
                    Lifetime Deal by <span>Prime</span>
                  </Typography>
                  <Typography variant="h6"
                    // className={classes.fontSize}
                    className={classes.planName} style={{ fontSize: "14px" }}>
                    {getPlanName()}
                  </Typography>
                  <CustomButton
                    onClick={this.upgradePlan}
                    btnType="green"
                    variant="contained"
                    // query={loading && "progress"}
                    style={{
                      marginBottom: 0,
                      marginTop: 10,
                      borderRadius: 5,
                      padding: "7px 17px",
                      fontSize: "13px",
                      width: "100%",
                    }}>
                    Upgrade
                  </CustomButton>
                </div>
              ) : null}
            </Grid>
            <Grid item classes={{ item: classes.teamSettingContentCnt }}>
              {selectedIndex === 0 ? (
                <GeneralSetting
                  classes={classes}
                  theme={theme}
                  showSnackBar={this.showSnackBar}
                  showLoadingState={this.props.showLoadingState}
                  hideLoadingState={this.props.hideLoadingState}
                  planExpire={this.props.planExpire}
                />
              ) : selectedIndex === 1 ? (
                <>
                  {/*<ManageMembers*/}
                  {/*  classes={classes}*/}
                  {/*  theme={theme}*/}
                  {/*  showSnackBar={this.showSnackBar}*/}
                  {/*/>*/}
                </>
              ) : selectedIndex === 6 ? (
                <DataExport showSnackBar={this.showSnackBar} />
              ) : selectedIndex === 7 ? (
                <Configuration showSnackBar={this.showSnackBar} />
              ) : selectedIndex === 5 ? (
                <DataCenter showSnackBar={this.showSnackBar} />
              ) : selectedIndex === 4 ? (
                <Apis showSnackBar={this.showSnackBar} />
              ) : selectedIndex === 2 && currentUser.isOwner ? (
                <></>
              ) : // <Billing showSnackBar={this.showSnackBar} />
                selectedIndex === 3 && currentUser.isOwner ? (
                  <StripeProvider apiKey={STRIPE_PUBLISH_KEY}>
                    <Elements>
                      <Payment showSnackBar={this.showSnackBar} />
                    </Elements>
                  </StripeProvider>
                ) : null}
              {(isBusinessPlan(paymentPlanTitle) || isBusinessTrial(paymentPlanTitle) || isEnterprisePlan(paymentPlanTitle)) &&
                <Route
                  path="/team-settings/work-calendar"
                  render={() => (
                    <TeamCalendar />
                  )}
                />
              }
              <Route
                path="/team-settings/billing"
                render={() => {
                  return <Billing showSnackBar={this.showSnackBar} />;
                }}
              />
              <Route
                path="/team-settings/user-management"
                render={() => {
                  return (
                    <ManageMembers
                      classes={classes}
                      theme={theme}
                      showSnackBar={this.showSnackBar}
                    />
                  );
                }}
              />
              <Route
                path="/team-settings/upgrade-plan"
                render={() => {
                  return (
                    <UpdatePlan classes={classes} theme={theme} showSnackBar={this.showSnackBar} />
                    // <ManageMembers
                    //   classes={classes}
                    //   theme={theme}
                    //   showSnackBar={this.showSnackBar}
                    // />
                  );
                }}
              />
            </Grid>
          </Grid>
        </div>

        {successModal ? (
          <UpdateSuccess successfullyExit={this.successfullyExit}></UpdateSuccess>
        ) : null}
        <ActionConfirmation
          open={showConfirmation}
          closeAction={this.closeAdditionalRequest}
          cancelBtnText={"No, I changed my mind"}
          successBtnText={"Get, Additional 1TB"}
          alignment="left"
          headingText={"Get Additional Storage"}
          msgText={"Are you sure you want to get additional 1TB for $49?"}
          successAction={this.GetAdditionalStorage}
          btnQuery={btnQuery}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  let company = state.profile.data.teams.find(
    (item) => item.companyId == state.profile.data.activeTeam
  );
  let paidMembers = state.profile.data.teamMember.filter((item) => item.isDeleted == false).length;
  return {
    profileState: state.profile,
    company: company,
    paidMembers: paidMembers,
    usedSpace: company.usedSpace,
  };
};

export default compose(
  withRouter,
  connect(mapStateToProps),
  withSnackbar,
  withStyles(workspaceSetting, { withTheme: true })
)(TeamSetting);
