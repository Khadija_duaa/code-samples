import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import fileDownload from "js-file-download";
import { withSnackbar } from "notistack";
import React, { useState } from "react";
import { compose } from "redux";
import CustomButton from "../../../components/Buttons/CustomButton";
import IconActivityLog2 from "../../../components/Icons/IconActivityLog2";
import { exportTeamActivityLog } from "../../../redux/actions/team";
import Style from "./Style";

const DataExport = (props) => {
  const { classes, enqueueSnackbar } = props;
  const [loading, setLoading] = useState(false);

  const showSnackBar = (snackBarMessage = "", type = null) => {
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "top",
          horizontal: "centerdp",
        },
        variant: type ? type : "info",
      }
    );
  };

  const handleExportActivity = () => {
    setLoading(true);
    exportTeamActivityLog((res) => {
      fileDownload(res.data, "activity-log.xlsx");
      showSnackBar("Activity Log exported successfully", "success");
    }).finally(() => setLoading(false));
  };

  return (
    <div className={classes.container}>
      <div className={classes.card}>
        <div style={{ margin: "10px 10px" }}>
          <IconActivityLog2 />
        </div>
        <Typography variant="h4" style={{ fontWeight: "bold" }} className={classes.lineSpacing}>
          Team Activity Log Data
        </Typography>
        <Typography variant="h5" style={{ margin: "6px 0" , fontWeight: "bold"}}>
          Download your team activity log details from all the workspaces.
        </Typography>
        <CustomButton
          onClick={() => handleExportActivity()}
          btnType="green"
          variant="contained"
          query={loading && "progress"}
          style={{
            marginBottom: 0,
            marginTop: 10,
            borderRadius: 5,
            padding: "10px 25px",
            fontSize: "13px",
            lineHeight: "1.3",
            boxShadow: "0px 3px 6px #0000001A",
            ...(loading && {
              background: "none",
              boxShadow: "none",
            }),
          }}>
          Download
        </CustomButton>
      </div>
    </div>
  );
};
export default compose(withSnackbar, withStyles(Style, { withTheme: true }))(DataExport);
