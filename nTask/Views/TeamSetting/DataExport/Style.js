const Style = (theme) => ({
    container: {
      position: "relative",
      "& *": {
        fontFamily: theme.typography.fontFamilyLato,
      },
    },
    card: {
      padding: 20,
      textAlign: "center",
      width: 270,
      boxShadow: "0 1px 6px #0000001F",
      border: `1px solid ${theme.palette.background.grayLighter}`,
      borderRadius: 6,
    },
    snackBarHeadingCnt: {
      marginLeft: 10,
    },
    snackBarContent: {
      margin: 0,
      fontSize: "12px !important",
    },
  });
  
  export default Style;
  