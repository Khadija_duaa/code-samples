import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import workspaceSetting from "./styles";
import { connect } from "react-redux";
import { compose } from "redux";
import menuStyles from "../../assets/jss/components/menu";
import CustomIconButton from "../../components/Buttons/IconButton";
import SelectionMenu from "../../components/Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import combineStyles from "../../utils/mergeStyles";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import DeleteWorkspaceDialogContent from "../WorkspaceSetting/DeleteWorkspaceDialogContent";
import DeleteConfirmDialog from "../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import { DeleteWorkSpaceData } from "../../redux/actions/workspace";
import { FetchUserInfo } from "../../redux/actions/profile";
import { FormattedMessage, injectIntl } from "react-intl";
class WorkspaceActionDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      pickerOpen: false,
      pickerPlacement: "",
      archiveFlag: false,
      unArchiveFlag: false,
      deleteFlag: false,
      deleteWorkspaceDialogOpen: false,
      workspaceNameVerifyErrorMessage: "",
      step: 0,
      workspaceNameVerifyError: false,
      workspaceNameVerify: "",
      workspaceNameErrorState: false,
    };
  }
  //delete workspace confirmation dialog close
  handleDeleteWorkspaceDialogClose = () => {
    this.setState({ deleteWorkspaceDialogOpen: false });
  };
  //delete workspace confirmation dialog open
  deleteWorkspaceDialogOpen = () => {
    this.setState({ deleteWorkspaceDialogOpen: true, open: false });
  };
  //Handle Verify team name input for delete team dialog
  handleWorkspaceNameInput = (event) => {
    this.setState({
      workspaceNameVerify: event.target.value,
      workspaceNameVerifyError: false,
      workspaceNameVerifyErrorMessage: "",
    });
  };

  validWorkspaceDeleteInput = (key, value = "") => {
    const { loggedInTeam, workspaceNameVerify } = this.state;
    const { activeWorkspace } = this.props;
    let validationObj = value == "" ? false : true;
    if (!validationObj) {
      switch (key) {
        case "workspaceName":
          this.setState({
            workspaceNameVerifyError: true,
            workspaceNameVerifyErrorMessage: this.props.intl.formatMessage({
              id:
                "workspace-settings.general-information.edit-modal.form.validation.worksapce-name.empty",
              defaultMessage: "Oops! Please enter workspace name.",
            }),
          });
          break;

        default:
          break;
      }
    } else if (
      key == "workspaceName" &&
      activeWorkspace.teamName !== workspaceNameVerify
    ) {
      this.setState({
        workspaceNameVerifyError: true,
        workspaceNameVerifyErrorMessage: this.props.intl.formatMessage({
          id:
            "workspace-settings.general-information.edit-modal.form.validation.worksapce-name.doesntMatch",
          defaultMessage: "Oops! Workspace name does not match.",
        }),
      });
    } else return validationObj;
  };
  handleDeleteWorkspaceStep = (value) => {
    // Function that change the step
    this.setState({ step: value });
  };

  handleDeleteWorkspaceDialog = () => {
    const { profileState } = this.props;
    const { workspaceNameVerify } = this.state;
    let workspaceNameValidate = this.validWorkspaceDeleteInput(
      "workspaceName",
      workspaceNameVerify
    );
    if (workspaceNameValidate) {
      this.setState({ btnQuery: "progress" });
      this.props.DeleteWorkSpaceData(
        this.props.activeWorkspace.teamId,
        //Success Callback
        () => {
          //User Info
          this.handleDeleteWorkspaceDialogClose();
          this.props.FetchUserInfo((response) => {
            this.setState({ btnQuery: "" });
          });
        },
        //Failure Callback
        (error) => {
          this.setState({ btnQuery: "" });
        }
      );
    }
  };
  handleClose = (event) => {
    this.setState({ open: false, pickerOpen: false });
  };
  handlePickerClose = (event) => {
    this.setState({ pickerOpen: false });
  };

  handleClick = (event, placement) => {
    const { currentTarget } = event;
    this.setState((state) => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  };

  render() {
    const {
      classes,
      theme,
      deleteSchedule,
      editSchedule,
      activeWorkspace,
    } = this.props;
    const {
      open,
      placement,
      workspaceNameVerifyErrorMessage,
      workspaceNameVerifyError,
      workspaceNameVerify,
      deleteWorkspaceDialogOpen,
      step,
      btnQuery,
      workspaceNameErrorState,
      handleDeleteWorkspace,
    } = this.state;
    return (
      <Fragment>
        {deleteWorkspaceDialogOpen && (
          <DeleteConfirmDialog
            open={deleteWorkspaceDialogOpen}
            closeAction={this.handleDeleteWorkspaceDialogClose}
            cancelBtnText={step == 0 ? <FormattedMessage
              id="common.action.cancel.label"
              defaultMessage="Cancel"
            /> : <FormattedMessage
              id="workspace-settings.general-information.delete-workspace.delete-modal.form.changed-mind.label"
              defaultMessage="No, I've changed my mind"
            />}
            successBtnText={step == 0 ? <FormattedMessage
              id="common.action.delete.label"
              defaultMessage="Delete"
            /> : <FormattedMessage
              id="workspace-settings.general-information.delete-workspace.delete-modal.form.delete-workspace.label"
              defaultMessage="Yes, Delete Workspace"
            />}
            alignment="center"
            headingText={<FormattedMessage
              id="common.action.delete.label"
              defaultMessage="Delete"
            />}
            successAction={
              step == 0
                ? () => {
                  this.handleDeleteWorkspaceStep(1);
                }
                : this.handleDeleteWorkspaceDialog
            }
            btnQuery={btnQuery}
          >
            <DeleteWorkspaceDialogContent
              step={step}
              handlePasswordInput={this.handlePasswordInput}
              workspaceNameErrorMessage={workspaceNameVerifyErrorMessage}
              workspaceNameError={workspaceNameVerifyError}
              handleWorkspaceNameInput={this.handleWorkspaceNameInput}
              workspaceName={workspaceNameVerify}
              activeWorkspace={activeWorkspace}
            />
          </DeleteConfirmDialog>
        )}
        <ClickAwayListener onClickAway={this.handleClose}>
          <div>
            <CustomIconButton
              btnType="condensed"
              style={{ position: "absolute", right: 10, top: 10, zIndex: 11 }}
              onClick={(event) => {
                this.handleClick(event, "bottom-end");
              }}
              buttonRef={(node) => {
                this.anchorEl = node;
              }}
            >
              <MoreVerticalIcon
                htmlColor={theme.palette.secondary.medDark}
                style={{ fontSize: "24px !important" }}
              />
            </CustomIconButton>

            <SelectionMenu
              open={open}
              closeAction={this.handleClose}
              placement={placement}
              anchorRef={this.anchorEl}
              list={
                <List>
                  <ListItem disableRipple className={classes.headingItem}>
                    <ListItemText
                      primary={
                        <FormattedMessage
                          id="common.action.label"
                          defaultMessage="Select Action"
                        />
                      }
                      classes={{ primary: classes.headingText }}
                    />
                  </ListItem>
                  <ListItem
                    button
                    disableRipple
                    classes={{ selected: classes.statusItemText }}
                    onClick={handleDeleteWorkspace}
                  >
                    <ListItemText
                      primary={
                        <FormattedMessage
                          id="workspace-settings.general-information.delete-workspace.label"
                          defaultMessage="Delete Workspace"
                        />
                      }
                      onClick={this.deleteWorkspaceDialogOpen}
                      classes={{
                        primary: classes.statusItemText,
                      }}
                    />
                  </ListItem>
                </List>
              }
            />
          </div>
        </ClickAwayListener>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profileState: state.profile,
  };
};

export default compose(
  injectIntl,
  connect(mapStateToProps, { DeleteWorkSpaceData, FetchUserInfo }),
  withStyles(combineStyles(workspaceSetting, menuStyles), {
    withTheme: true,
  })
)(WorkspaceActionDropdown);
