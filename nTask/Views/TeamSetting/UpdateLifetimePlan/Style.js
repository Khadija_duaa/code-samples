const Style = (theme) => ({
  container: {
    position: "relative",
    width: 900,
    maxWidth: '100%',
    "& *": {
      fontFamily: theme.typography.fontFamilyLato,
    },
  },
  planHeader: {
    display: 'flex',
    marginBottom: 35,
  },
  arrowBack: {
    padding: "4px 0px 4px 4px",
    border: `1px solid ${theme.palette.background.light}`,
    borderRadius: "50%",
    background: theme.palette.background.light,
    cursor: "pointer",
    color: "#929292",
    zIndex: 1,
    width: 32,
    height: 32,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10,
    '& svg': {
      fontSize: "16px !important",
    },
    "&:hover": {
      backgroundColor: "#0090ff",
      border: "1px solid #0090ff",
      color: "white"
    },
  },
  headerTitle: {
    fontSize: "18px !important",
    fontWeight: theme.typography.fontWeightxLarge,
    marginBottom: 5,
  },
  headerDetails: {
    fontSize: "16px !important",
  },
  planCard: {
    padding: '40px 20px 20px 20px',
    textAlign: "center",
    width: '100%',
    position: 'relative',
    boxShadow: `2px 2px 0px ${theme.palette.background.contrast}`,
    border: `2px solid ${theme.palette.background.contrast}`,
    borderRadius: 6,
  },
  bestPlan: {
    boxShadow: `2px 2px 0px ${theme.palette.background.green}`,
    border: `2px solid ${theme.palette.border.greenBorder}`,
  },
  bestValue: {
    position: 'absolute',
    top: 0,
    left: '50%',
    transform: 'translate(-50%, -50%)',
    background: theme.palette.background.green,
    borderRadius: 50,
    padding: '4px 15px',
    color: theme.palette.background.default,
  },
  planTitle: {
    fontSize: "22px !important",
    // color: theme.palette.common.black,
    fontWeight: theme.typography.fontWeightLarge,
    fontWeight: theme.typography.fontWeightxLarge,
  },
  planMembers: {
    fontSize: "14px !important",
    margin: '10px 0',
    // color: theme.palette.common.black,
  },
  planPrice: {
    fontSize: "30px !important",
    // color: theme.palette.common.black,
    margin: '10px 0',
    fontWeight: theme.typography.fontWeightxLarge,
    '& span': {
      // fontWeight: theme.typography.fontWeightRegular,
      fontSize: "16px !important",
    }
  },
  planStorage: {
    fontWeight: theme.typography.fontWeightLarge,
    // color: theme.palette.common.black,
    marginBottom: 10,
    fontSize: "14px !important",
  },
  disabledPlan: {
    background: theme.palette.background.airy,
    '& $planTitle, $planMembers, $planPrice , $planStorage ': {
      color: theme.palette.secondary.lightGrey,
    }
  },
  upgradeBtn: {
    marginBottom: 0,
    marginTop: 10,
    width: '100%',
    fontSize: "16px !important",
    padding: "12px 17px",
    fontWeight: theme.typography.fontWeightxLarge,
    color: theme.palette.background.green,
    background: theme.palette.common.white,
    border: `2px solid ${theme.palette.background.green}`,
    
  },
  bestPlanBtn: {
    color: theme.palette.common.white,
    background: theme.palette.background.green,
  },
  currentPlanBtn: {
    color: theme.palette.text.light,
    borderColor: theme.palette.background.contrast,
  },
  disabledPlanBtn: {
    borderColor: `${theme.palette.background.grayLighter} !important`,
    background: theme.palette.background.grayLighter,
    color: `${theme.palette.text.light} !important`,
  }
});

export default Style;
