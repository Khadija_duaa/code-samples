import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { withSnackbar } from "notistack";
import { withRouter } from "react-router-dom";
import React, { useEffect, useState } from "react";
import { compose } from "redux";
import Grid from "@material-ui/core/Grid";
import Style from "./Style";
import ArrowBackIcon from "@material-ui/icons/ArrowBackIos";
import UpdateSuccess from "../../billing/AddBillDetail/UpdateSuccess";
import PlanCard from "./PlanCard";
import { useSelector } from "react-redux";

const UpdatePlan = (props) => {
  const { theme, classes, enqueueSnackbar, history } = props;
  const [successModal, setSuccessModal] = useState(false);
  const handleBack = () => {
    history.push("/team-settings/billing");
  };

  const [disableSession, setDisableSession] = useState(false);
  const { profileState } = useSelector(state => {
    return {
      profileState: state.profile.data,
    };
  });
  const { activeTeam, teams } = profileState;
  const company = teams.find(t => t.companyId === activeTeam) || {};
  useEffect(() => {
    setDisableSession(company.ltdPlanUpgradeRequested);
  }, [company.ltdPlanUpgradeRequested]);
  const successfullyOpen = () => {
    setSuccessModal(true)
  };
  const successfullyExit = () => {
    setSuccessModal(false);
    // handleBack()
  };
  const showSnackBar = (snackBarMessage = "", type = null) => {
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{snackBarMessage}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "top",
          horizontal: "centerdp",
        },
        variant: type ? type : "info",
      }
    );
  };
  return (
    <div className={classes.container}>
      <div className={classes.planHeader}>
        <div className={classes.arrowBack} onClick={handleBack}>
          <ArrowBackIcon />
        </div>
        <div className={classes.headerInfo}>
          <Typography variant="h3" className={classes.headerTitle}>
            Upgrade your Team Plan
          </Typography>
          <Typography variant="p" className={classes.headerDetails}>
            Select one of the <b>Lifetime Deal</b> plan to upgrade your team.
          </Typography>
        </div>
      </div>
      <Grid
        container
        direction="row"
        justify="flex-start"
        alignItems="stretch"
        spacing={24}
        style={{ height: "100%" }}>
        {/* 1st  */}
        <Grid item xs={12} sm={6} md={6}>
          <PlanCard
            bestValue={true}
            planTitle="Supreme Saver"
            planMembers="120"
            planPrice="499"
            planStorage="500GB storage"
            planId={5}
            openSuccess={successfullyOpen}
            handleDisable={setDisableSession}
            disableProps={disableSession}
          />
        </Grid>

        {/* 2nd  */}
        <Grid item xs={12} sm={6} md={6}>
          {/* <div className={classes.planCard}>
            <Typography variant="h2" className={classes.planTitle}>
              Ultimate Saver
            </Typography>
            <Typography variant="p" className={classes.planMembers}>
              50 team members
            </Typography>
            <Typography variant="h1" className={classes.planPrice}>
              <sup>$</sup>249/<span>lifetime</span>
            </Typography>
            <Typography variant="p" className={classes.planStorage}>
              1TB storage
            </Typography>
            <CustomButton
              // onClick={() => handleExportActivity()}
              btnType="green"
              variant="outlined"
              // query={loading && "progress"}
              className={classes.upgradeBtn}
              style={{
                background: '#fff',
                border: `2px solid ${theme.palette.background.green}`,
                color: theme.palette.background.green,
              }}>
              Upgrade for $150
            </CustomButton>
          </div> */}
          <PlanCard
            bestValue={false}
            planTitle="Ultimate Saver"
            planMembers="50"
            planPrice="249"
            planStorage="200GB storage"
            planId={4}
            openSuccess={successfullyOpen}
            handleDisable={setDisableSession}
            disableProps={disableSession}
          />
        </Grid>
      </Grid>
      <Grid
        container
        direction="row"
        justify="flex-start"
        alignItems="stretch"
        spacing={16}
        style={{ height: "100%", marginTop: 15 }}>
        {/* 3rd */}
        <Grid item xs={12} sm={6} md={6} lg={4}>
          {/* <div className={classes.planCard}>
            <Typography variant="h2" className={classes.planTitle}>
              Big Saver
            </Typography>
            <Typography variant="p" className={classes.planMembers}>
              20 team members
            </Typography>
            <Typography variant="h1" className={classes.planPrice}>
              <sup>$</sup>149/<span>lifetime</span>
            </Typography>
            <Typography variant="p" className={classes.planStorage}>
              1TB storage
            </Typography>
            <CustomButton
              // onClick={() => handleExportActivity()}
              btnType="green"
              variant="outlined"
              // query={loading && "progress"}
              className={classes.upgradeBtn}
              style={{
                background: '#fff',
                border: `2px solid ${theme.palette.background.green}`,
                color: theme.palette.background.green,
              }}>
              Upgrade
            </CustomButton>
          </div> */}
          <PlanCard
            bestValue={false}
            planTitle="Big Saver"
            planMembers="20"
            planPrice="149"
            planStorage="100GB storage"
            planId={3}
            openSuccess={successfullyOpen}
            handleDisable={setDisableSession}
            disableProps={disableSession}
          />
        </Grid>
        {/* 4th */}
        <Grid item xs={12} sm={6} md={6} lg={4}>
          {/* <div className={classes.planCard}>
            <Typography variant="h2" className={classes.planTitle}>
              Standard Saver
            </Typography>
            <Typography variant="p" className={classes.planMembers}>
              10 team members
            </Typography>
            <Typography variant="h1" className={classes.planPrice}>
              <sup>$</sup>99/<span>lifetime</span>
            </Typography>
            <Typography variant="p" className={classes.planStorage}>
              1TB storage
            </Typography>
            <CustomButton
              // onClick={() => handleExportActivity()}
              btnType="green"
              variant="outlined"
              // query={loading && "progress"}
              className={classes.upgradeBtn}
              style={{
                background: '#fff',
              }}>
              Current Plan
            </CustomButton>
          </div> */}
          <PlanCard
            bestValue={false}
            planTitle="Standard Saver"
            planMembers="10"
            planPrice="99"
            planStorage="40GB storage"
            planId={2}
            openSuccess={successfullyOpen}
            handleDisable={setDisableSession}
            disableProps={disableSession}
          />
        </Grid>
        {/* 5th */}
        <Grid item xs={12} sm={6} md={6} lg={4}>
          <PlanCard
            bestValue={false}
            planTitle="Basic Saver"
            planMembers="5"
            planPrice="49"
            planStorage="20GB storage"
            planId={1}
            openSuccess={successfullyOpen}
            handleDisable={setDisableSession}
            disableProps={disableSession}
          />
        </Grid>
      </Grid>

      {successModal ? <UpdateSuccess successfullyExit={successfullyExit}></UpdateSuccess> : null}
    </div >
  );
};
export default compose(
  withRouter, withSnackbar,
  withStyles(Style, { withTheme: true }))(UpdatePlan);
