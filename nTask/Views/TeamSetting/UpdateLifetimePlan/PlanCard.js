import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { withSnackbar } from "notistack";
import { withRouter } from "react-router-dom";
import React from "react";
import { compose } from "redux";
import clsx from "clsx";
import Style from "./Style";
import CustomButton from "../../../components/Buttons/CustomButton";
import { useDispatch, useSelector } from "react-redux";
import { updateLTDRequest, upgradeSubscription } from "../../../redux/actions/team";

const PlanCard = (props) => {
  const { theme, classes, enqueueSnackbar,
    bestValue,
    planTitle,
    planMembers,
    planPrice,
    planStorage,
    planId,
    openSuccess,
    handleDisable,
    disableProps
  } = props;

  const dispatch = useDispatch();
  const { profileState } = useSelector(state => {
    return {
      profileState: state.profile.data,
    };
  });
  const { activeTeam, teams } = profileState;
  const company = teams.find(t => t.companyId === activeTeam) || {};
  const currentPrice = company.paymentPlan.unitPriceAnually
  // const currentPrice = '99'
  const packageType = company.subscriptionDetails.packageType;
  // const packageType = 'Lifetime 2022 Tier 2';
  const getSelectedPlan = () => {
    if (packageType) {
      if (packageType.includes('Tier 5')) { return 5 }
      else if (packageType.includes('Tier 4')) { return 4 }
      else if (packageType.includes('Tier 3')) { return 3 }
      else if (packageType.includes('Tier 2')) { return 2 }
      else if (packageType.includes('Tier 1')) { return 1 }
    }
  }

  const planStatus = () => {
    if (getSelectedPlan() == planId) {
      return 'current'
    } else if (getSelectedPlan() > planId) {
      return 'disabled'
    } else {
      return 'available'
    }
  }
  const handleUpdatePlan = () => {
    if (planStatus() == 'current') return
    const data = {
      TeamId: company.companyId,
      UpgradeDescription: `${planTitle} with $${planPrice - currentPrice}`,
    }
    // handleDisable(packageType)
    upgradeSubscription(data,
      res => {
        openSuccess();
        dispatch(updateLTDRequest({
          companyId: company.companyId,
          ltdPlanUpgradeRequested: `Lifetime 2022 Tier ${planId}`,
        }))
      },
      err => {
        console.log('err', err);
      })
  }
  return (
    <div
      className={clsx({
        [classes.planCard]: true,
        [classes.bestPlan]: bestValue,
        [classes.disabledPlan]: planStatus() == 'disabled',
      })}
    >
      {bestValue ? <span className={classes.bestValue}>Best Value Offer </span> : null}
      <Typography variant="h2" className={classes.planTitle}>
        {planTitle}
      </Typography>
      <Typography variant="p" className={classes.planMembers}>
        {planMembers}{' '} team members
      </Typography>
      <Typography variant="h1" className={classes.planPrice}>
        <sup>$</sup>{planPrice}/<span>lifetime</span>
      </Typography>
      <Typography variant="p" className={classes.planStorage}>
        {planStorage}
      </Typography>
      <CustomButton
        onClick={handleUpdatePlan}
        btnType="green"
        variant={bestValue ? "contained" : "outlined"}
        // query={loading && "progress"}
        className={clsx({
          [classes.upgradeBtn]: true,
          [classes.bestPlanBtn]: bestValue,
          [classes.currentPlanBtn]: !bestValue && planStatus() == 'current' || planStatus() == disableProps,
          [classes.disabledPlanBtn]: planStatus() == 'disabled',
        })}
        disabled={planStatus() == 'disabled' || disableProps}
      >
        {planStatus() == 'current' ? 'Current Plan' :
          planStatus() == 'disabled' ? 'Not Available' :
            `Lifetime 2022 Tier ${planId}` == disableProps ? 'Already Requested' :
              `Lifetime 2022 Tier ${planId}` != disableProps ? `Upgrade for $${planPrice - currentPrice}` : ''}
        {/* Upgrade */}
      </CustomButton>
    </div>
  );
};
export default compose(
  withRouter, withSnackbar,
  withStyles(Style, { withTheme: true }))(PlanCard);
