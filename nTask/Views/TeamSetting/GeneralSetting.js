import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { compose } from "redux";
import DefaultTextField from "../../components/Form/TextField";
import Grid from "@material-ui/core/Grid";
import DeleteIcon from "@material-ui/icons/Cancel";
import Typography from "@material-ui/core/Typography";
import Avatar from "@material-ui/core/Avatar";
import CustomAvatar from "../../components/Avatar/Avatar";
import getErrorMessages from "../../utils/constants/errorMessages";
import {
  UpdateWorkSpaceData,
  DeleteWorkSpaceData,
  FetchWorkspaceInfo,
} from "../../redux/actions/workspace";
// import { SwitchToTeam } from "./../../redux/actions/teamMembers";
import { FetchUserInfo, updateTeam } from "../../redux/actions/profile";
import { PopulateTempData } from "../../redux/actions/tempdata";
import WorkspaceStatusDropdown from "./WorkspaceStatusDropdown";
import DeleteConfirmDialog from "../../components/Dialog/ConfirmationDialogs/DeleteConfirmation";
import DeleteTeamDialogContent from "./DeleteTeamDialogContent";
import {
  UpdateTeamImage,
  RemoveTeamLogo,
  deleteTeam,
} from "../../redux/actions/team";
import EditIcon from "@material-ui/icons/Edit";
import Hotkeys from "react-hot-keys";
import DefaultDialog from "../../components/Dialog/Dialog";
import EditTeamForm from "../AddNewForms/editTeamForm";
import Truncate from "react-truncate";
import CustomButton from "../../components/Buttons/CustomButton";
import { switchTeam, whiteLabelInfo, DefaultWhiteLabelInfo } from "../../redux/actions/teamMembers";
import WorkspaceActionDropdown from "./workspaceActionDropdown";
import DeleteWorkspaceDialogContent from "../WorkspaceSetting/DeleteWorkspaceDialogContent";
import ConstantsPlan from "../../components/constants/planConstant";
import moment from "moment";
import { translate } from "../../i18n/translate";
import { FormattedMessage, injectIntl } from "react-intl";

const hideBtn = {
  display: "none",
};

class GeneralSetting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inviteChecked: false,
      teamName: "",
      imageUrl: "",
      imageBasePath: "",
      fileName: "",
      teamNameError: "",
      teamNameErrorMessage: "",
      loggedInTeam: "",
      teamId: "",
      pictureThumbnail_70X70: "",
      confirmDeleteWorkspace: false,
      nameEdit: false,
      btnQuery: "",
      deleteTeamDialogOpen: false,
      step: 0,
      teamNameVerifyErrorMessage: "",
      teamNameVerifyError: false,
      teamNameVerify: "",
      teamNameErrorState: false,
      autoFocusField: true,
      openDialog: false,
      workspaceNameVerify: "",
      workspaceNameErrorState: false,
    };
    this.handleInput = this.handleInput.bind(this);
    this.handleInviteSwitch = this.handleInviteSwitch.bind(this);
    this.clickFile = this.clickFile.bind(this);
    this._handleImageChange = this._handleImageChange.bind(this);
    this.removeProfilePicture = this.removeProfilePicture.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
  }

  handleNameUpdate = (event) => {
    const { teamName, activeTeam } = this.state;

    if (event === "enter") {
      if (teamName == activeTeam.companyName) {
        this.setState({ nameEdit: false });
      } else {
        let teamNameValue = teamName.trim();
        let teamNameValidate = this.validInput("editTeamName", teamNameValue);
        let newActiveTeamObj = {
          ...this.state.activeTeam,
          companyName: teamName,
        };
        if (teamNameValidate) {
          // this.setState({
          //   activeTeam: newActiveTeamObj,
          // })
          this.props.updateTeam(
            newActiveTeamObj,
            //Success Callback
            (success) => {
              this.setState({
                nameEdit: false,
                teamNameErrorState: false,
                teamNameErrorMessage: "",
              });
            },
            (err) => {
              this.setState({
                // autoFocusField: false,
                teamNameErrorState: true,
                teamNameErrorMessage: err.data.message,
                // nameEdit: true
              });
            }
          );
        }
      }
    }
    if (event === "esc") {
      this.setState({
        nameEdit: false,
        teamNameErrorState: false,
        teamNameErrorMessage: "",
        teamName: "",
      });
    }
  };

  disableEdit = () => {
    this.setState({
      nameEdit: false,
      teamNameErrorState: false,
      teamNameErrorMessage: "",
      teamName: "",
    });
  };

  componentDidUpdate(prevProps, prevState) {
    const { profileState } = this.props;
    const loggedInTeam =
      profileState.data && profileState.data.teams
        ? profileState.data.teams.find(
            (x) => x.companyId === profileState.data.activeTeam
          )
        : {};

    if (
      JSON.stringify(prevProps.profileState.data.teams) !==
      JSON.stringify(profileState.data.teams)
    ) {
      if (this.props.profileState.activeTeam) {
        this.setState({
          teamId: this.props.profileState.data.activeTeam,
          activeTeam: loggedInTeam,
          teamName: loggedInTeam.companyName,
          loggedInTeam,
          pictureThumbnail_70X70: loggedInTeam.companyImage
            ? loggedInTeam.companyImage.pictureThumbnail_70X70
            : null,
          isEdit: false,
        });
      }
    }
  }

  componentDidMount() {
    const { profileState } = this.props;
    const activeTeam = profileState.data.activeTeam
      ? profileState.data.teams.find(
          (t) => t.companyId === profileState.data.activeTeam
        )
      : {};
    this.setState({ activeTeam, teamName: activeTeam.companyName });
  }
  clickFile() {
    let fileinputbtn = document.getElementById("teamImageUpload");
    fileinputbtn.click();
  }
  removeProfilePicture() {
    this.props.RemoveTeamLogo(
      this.props.profileState.data.activeTeam,
      (response) => {
        this.setState({
          activeTeam: response.data,
        });
      },
      (error) => {
        if (error.status === 500) {
          this.props.showSnackBar("Server throws error", "error");
        } else {
          this.props.showSnackBar(error.data.message, "error");
        }
      }
    );
  }
  _handleImageChange(e) {
    e.preventDefault();
    this.setState({
      imageUploadFlag: true,
      teamNameError: "",
      teamNameErrorMessage: "",
    });
    let reader = new FileReader();
    let file = e.target;
    if (file.files.length > 0) {
      let type = file.files[0].name.toLowerCase().match(/[0-9a-z]{1,5}$/gm)[0];
      if (
        (type != null && type.toLowerCase() == "jpg") ||
        type.toLowerCase() == "jpeg" ||
        type.toLowerCase() == "png" ||
        type.toLowerCase() == "gif" ||
        type.toLowerCase() == "bmp"
      ) {
        reader.onloadend = () => {
          this.setState({
            file: file.files[0],
            imagePreviewUrl: reader.result,
          });
        };
        reader.readAsDataURL(file.files[0]);
        let data = {
          companyId: this.props.profileState.data.activeTeam,
          ["PrevImageName"]: this.state.imageUrl,
          ["UploadedImage"]: file.files[0],
        };
        this.props.UpdateTeamImage(
          data,
          (response) => {
            this.setState({
              activeTeam: response.data,
              imageBasePath: response.data.imageBasePath,
              fileName: response.data.fileName,
              pictureThumbnail_70X70: response.data.companyImage
                ? response.data.companyImage.pictureThumbnail_70X70
                : null,
            });
            this.props.showSnackBar(
              response.data.message
                ? response.data.message
                : "Team image is successfully uploaded.",
              "success"
            );
          },
          (error) => {
            if (error.status === 500) {
              this.props.showSnackBar("Server throws error", "error");
            } else {
              this.props.showSnackBar(error.data.message, "error");
            }
          }
        );
      } else {
        this.props.showSnackBar(
          getErrorMessages().INVALID_IMAGE_EXTENSION,
          "error"
        );
      }
    }
  }
  handleInput = (name) => (event) => {
    this.setState({
      [name]: event.target.value,
      teamNameError: "",
      teamNameErrorMessage: "",
    });
  };
  handleInviteSwitch() {
    this.setState((prevState) => ({
      inviteChecked: !prevState.inviteChecked,
    }));
  }
  handleDeleteTeamStep = (value) => {
    this.setState({ step: value });
  };

  //Function to generate list of assignee avatars
  GenerateList = (workspace) => {
    const { profileState } = this.props;
    const membersObj = profileState.data.member.allMembers.filter((m) => {
      return workspace.members.indexOf(m.userId) >= 0 && !m.isDeleted;
    });
    return membersObj.slice(0, 3).map((Assignee, i) => {
      return (
        <li key={i} style={{ marginLeft: !i == 0 ? -5 : null }}>
          <CustomAvatar
            otherMember={{
              imageUrl: Assignee.imageUrl,
              fullName: Assignee.fullName,
              lastName: "",
              email: Assignee.email,
              isOnline: Assignee.isOnline,
              isOwner: Assignee.isOwner,
            }}
            size="xsmall"
            disableCard
          />
        </li>
      );
    });
  };

  validInput = (key, value = "") => {
    const { activeTeam, teamNameVerify } = this.state;
    let validationObj = value == "" ? false : true;
    if (!validationObj) {
      switch (key) {
        case "teamName":
          this.setState({
            teamNameVerifyError: true,
            teamNameVerifyErrorMessage: this.props.intl.formatMessage({
              id:
                "team-settings.general-information.delete-confirmation.form.validation.team-name.empty",
              defaultMessage: "Oops! Please enter team name.",
            }),
          });
          break;

        case "editTeamName":
          this.setState({
            teamNameError: true,
            teamNameErrorMessage: this.props.intl.formatMessage({
              id:
                "team-settings.general-information.delete-confirmation.form.validation.team-name.empty",
              defaultMessage: "Oops! Please enter team name.",
            }),
          });

        default:
          break;
      }
    } else if (
      key == "teamName" &&
      activeTeam.companyName.replace(/\s\s+/g, " ") !== teamNameVerify
    ) {
      this.setState({
        teamNameVerifyError: true,
        teamNameVerifyErrorMessage: this.props.intl.formatMessage({
          id:
            "team-settings.general-information.delete-confirmation.form.validation.team-name.notmatch",
          defaultMessage: "Oops! Team name does not match.",
        }),
      });
    } else return validationObj;
  };

  handleDeleteTeamDialog = () => {
    const { profileState } = this.props;
    const { teamNameVerify } = this.state;
    let teamNameValidate = this.validInput("teamName", teamNameVerify);

    if (teamNameValidate) {
      this.setState({ btnQuery: "progress" });

      this.props.deleteTeam(
        profileState.data.activeTeam,
        //Success Callback
        () => {
          //User Info
          this.props.FetchUserInfo((response) => {
            let activeWorkspace = response.data.loggedInTeam;
            let activeTeam = response.data.activeTeam;
            if (activeTeam && response.data.teams.length) {
              // Switch Team
              switchTeam(activeTeam, (response) => {
                if(response.data.team.isEnableWhiteLabeling && response.data.team.isEnableWhiteLabeling == true)
                this.props.whiteLabelInfo(activeTeam);
                else
                this.props.DefaultWhiteLabelInfo();
                this.handleDeleteTeamDialogClose();
                this.props.history.push(
                  `/teams/${response.data.team.companyUrl}`
                );
                this.setState({ btnQuery: "" });

                const { gracePeriod } = this.props;
                const {
                  paymentPlanTitle,
                  currentPeriondEndDate,
                } = response.data.team.subscriptionDetails;
                if (!ConstantsPlan.isPlanFree(paymentPlanTitle)) {
                  if (
                    ConstantsPlan.isPlanTrial(paymentPlanTitle) &&
                    ConstantsPlan.isPlanExpire(currentPeriondEndDate)
                  ) {
                    this.props.showLoadingState();
                    this.props.planExpire("Trial");
                    return;
                  } else if (
                    ConstantsPlan.isPlanPaid(paymentPlanTitle) &&
                    ConstantsPlan.isPlanExpire(
                      currentPeriondEndDate,
                      gracePeriod
                    )
                  ) {
                    this.props.showLoadingState();
                    this.props.planExpire("Paid");
                    return;
                  }
                }
                this.props.hideLoadingState();
              });
            } else {
              this.props.history.push("/no-team-found");
            }
          });
        }
      );
    }
  };
  //delete team confirmation dialog
  handleDeleteTeamDialogClose = () => {
    this.setState({ deleteTeamDialogOpen: false });
  };
  deleteTeamDialogOpen = () => {
    this.setState({ deleteTeamDialogOpen: true });
  };
  //Handle Verify team name input for delete team dialog
  handleTeamNameInput = (event) => {
    this.setState({
      teamNameVerify: event.target.value,
      teamNameVerifyError: false,
      teamNameVerifyErrorMessage: "",
    });
  };
  handleDialogClose = () => {
    this.setState({ openDialog: false });
  };

  handleEdit = () => {
    this.setState({ openDialog: true });
  };

  render() {
    const {
      teamName,
      teamNameError,
      teamNameErrorMessage,
      nameEdit,
      btnQuery,
      deleteTeamDialogOpen,
      activeTeam = {},
      step,
      teamNameVerifyErrorMessage,
      teamNameVerifyError,
      teamNameVerify,
      teamNameErrorState,
      autoFocusField,
      openDialog,
    } = this.state;
    const { classes, theme, profileState } = this.props;
    let workspaceCount = profileState.data.workspace.filter((w) => {
      return w.companyId !== null;
    }).length;

    let team = profileState.data.teams.find((n) => {
      return profileState.data.activeTeam == n.companyId;
    });
    let currentUser = profileState.data.teamMember.find((m) => {
      return m.userId == profileState.data.userId;
    });
    return (
      <>
        <DeleteConfirmDialog
          open={deleteTeamDialogOpen}
          closeAction={this.handleDeleteTeamDialogClose}
          cancelBtnText={
            step == 0 ? (
              <FormattedMessage
                id="common.action.delete.confirmation.cancel-button.label"
                defaultMessage="Cancel"
              />
            ) : (
              <FormattedMessage
                id="team-settings.general-information.delete-confirmation.form.changed-mind-button.label"
                defaultMessage="No, I've changed my mind"
              />
            )
          }
          successBtnText={
            step == 0 ? (
              <FormattedMessage
                id="common.action.delete.confirmation.delete-button.label"
                defaultMessage="Delete"
              />
            ) : (
              <FormattedMessage
                id="team-settings.general-information.delete-confirmation.form.delete-team-button.label"
                defaultMessage="Yes, Delete Team"
              />
            )
          }
          alignment="center"
          headingText={
            <FormattedMessage
              id="team-settings.general-information.delete-confirmation.title"
              defaultMessage="Delete"
            />
          }
          successAction={
            step == 0
              ? () => {
                  this.handleDeleteTeamStep(1);
                }
              : this.handleDeleteTeamDialog
          }
          btnQuery={btnQuery}
        >
          <DeleteTeamDialogContent
            step={step}
            teamNameErrorMessage={teamNameVerifyErrorMessage}
            teamNameError={teamNameVerifyError}
            handleTeamNameInput={this.handleTeamNameInput}
            teamName={teamNameVerify}
            activeTeam={activeTeam}
          />
        </DeleteConfirmDialog>

        <DefaultDialog
          title={
            <FormattedMessage
              id="team-settings.edit.label"
              defaultMessage="Edit Team"
            />
          }
          sucessBtnText={
            <FormattedMessage
              id="profile-settings-dialog.signin-security.change-password.form.save-button.label"
              defaultMessage="Save Changes"
            />
          }
          open={openDialog}
          onClose={this.handleDialogClose}
        >
          <EditTeamForm
            handleDialogClose={this.handleDialogClose}
            activeTeam={team}
          />
        </DefaultDialog>

        <div className={classes.personalInfoCnt}>
          <Grid
            container
            direction="row"
            justify="flex-start"
            alignItems="flex-start"
          >
            <div className={classes.profilePicCnt}>
              {activeTeam.companyImage ? (
                <div className={classes.profilePic}>
                  <img src={activeTeam.companyImage.pictureThumbnail_70X70} />
                  <DeleteIcon
                    onClick={this.removeProfilePicture}
                    htmlColor={theme.palette.error.main}
                    classes={{ root: classes.deleteProfileImageIcon }}
                  />
                </div>
              ) : activeTeam.companyName ? (
                <CustomAvatar userActiveTeam={true} size="large" />
              ) : (
                // <Avatar classes={{ root: classes.menuWorkspaceAvatar }}>
                //   {loggedInTeam.teamName.charAt(0).toUpperCase()}
                // </Avatar>
                "U"
              )}
              <p className={classes.uploadPhotoBtn} onClick={this.clickFile}>
                <FormattedMessage
                  id="profile-settings-dialog.upload-photo.label"
                  defaultMessage="Upload Photo"
                ></FormattedMessage>
              </p>

              <input
                style={hideBtn}
                className="fileInput"
                type="file"
                id="teamImageUpload"
                onChange={this._handleImageChange}
                accept={"png, jpeg, jpg, gif, PNG, JPG, JPEG, GIF, bmp, BMP"}
              />
            </div>
            {!nameEdit ? (
              <div>
                <Typography variant="h2" className={classes.teamName}>
                  {team.companyName}
                  <EditIcon
                    htmlColor={theme.palette.secondary.dark}
                    className={classes.editIcon}
                    onClick={nameEdit ? this.handleNameUpdate : this.handleEdit}
                  />
                </Typography>
                <Typography variant="body2" style={{ marginTop: 10 }}>
                  {window.location.origin + "/teams/" + team.companyUrl}
                </Typography>

                {currentUser.role == "Owner" && (
                  <CustomButton
                    btnType="gray"
                    variant="contained"
                    style={{ marginTop: 10 }}
                    onClick={this.deleteTeamDialogOpen}
                  >
                    <FormattedMessage
                      id="team-settings.general-information.delete-team-button.label"
                      defaultMessage="Delete Team"
                    />
                  </CustomButton>
                )}
              </div>
            ) : (
              <>
                <DefaultTextField
                  label=""
                  fullWidth={false}
                  errorState={teamNameErrorState}
                  errorMessage={teamNameErrorMessage}
                  formControlStyles={{ width: 350 }}
                  defaultProps={{
                    id: "teamNameInput",
                    onChange: this.handleInput("teamName"),
                    value: teamName,
                    inputProps: { maxLength: 80 },
                    disabled: !nameEdit,
                    autoFocus: true,
                    onBlur: this.disableEdit,
                  }}
                />
                <Hotkeys
                  keyName="enter,esc"
                  onKeyDown={this.handleNameUpdate}
                />
              </>
            )}
          </Grid>
        </div>
        <Typography variant="h5" className={classes.allWorkspacesList}>
          <FormattedMessage
            id="workspace-settings.all-workspaces.label"
            defaultMessage="All Workspaces"
          />{" "}
          <span className={classes.workspaceCount}>({workspaceCount})</span>
        </Typography>
        <Grid container spacing={2}>
          {profileState.data.workspace.map((w) => {
            return w.companyId ? (
              <Grid
                item
                classes={{ item: classes.workspaceListItem }}
                xs={6}
                lg={4}
                xl={3}
                // onClick={this.handleWorkspaceGridClick(w.teamId)}
              >
                <div className={classes.workspaceListItemInner}>
                  <WorkspaceActionDropdown activeWorkspace={w} />
                  <CustomAvatar
                    otherWorkspace={{
                      teamName: w.teamName,
                      pictureUrl: w.pictureUrl,
                      baseUrl: w.imageBasePath,
                    }}
                    size="medium"
                  />
                  <div title={w.teamName} style={{ display: "flex" }}>
                    <Truncate
                      lines={2}
                      width={225}
                      trimWhitespace={true}
                      ellipsis={<span>...</span>}
                      className={classes.workspaceItemHeading}
                    >
                      {w.teamName}
                    </Truncate>
                  </div>
                  {/* <Typography
                    variant="h5"
                    className={classes.workspaceItemHeading}
                  >
                    {w.teamName}
                  </Typography> */}
                  <div style={{ marginTop: 10 }}>
                    <WorkspaceStatusDropdown workspace={w} />
                  </div>
                  <div className={classes.memberListCnt}>
                    <Typography
                      variant="caption"
                      className={classes.membersHeading}
                    >
                      <FormattedMessage
                        id="common.members.label"
                        defaultMessage="Members:"
                      />
                    </Typography>
                    <ul className="AssigneeAvatarList">
                      {this.GenerateList(w)}
                      {w.members.length > 3 ? (
                        <li>
                          <Avatar classes={{ root: classes.TotalAssignee }}>
                            +{w.members.length - 3}
                          </Avatar>
                        </li>
                      ) : (
                        ""
                      )}
                    </ul>
                  </div>
                </div>
              </Grid>
            ) : null;
          })}
        </Grid>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profileState: state.profile,
    gracePeriod: state.profile.data.gracePeriod,
  };
};

export default compose(
  withRouter,
  injectIntl,
  connect(mapStateToProps, {
    UpdateTeamImage,
    UpdateWorkSpaceData,
    RemoveTeamLogo,
    DeleteWorkSpaceData,
    deleteTeam,
    PopulateTempData,
    updateTeam,
    // SwitchToTeam,
    FetchUserInfo,
    FetchWorkspaceInfo,
    whiteLabelInfo,
    DefaultWhiteLabelInfo
  })
)(GeneralSetting);
