import React, { useEffect, useState } from "react";
import "react-big-calendar/lib/css/react-big-calendar.css";
import { withStyles } from "@material-ui/core/styles";
import "react-big-calendar/lib/addons/dragAndDrop/styles.scss";
import { styles } from "../../../components/CalendarModule/Calendar.style";
import { compose } from "redux";
import { omit } from "lodash";
import { withSnackbar } from "notistack";
import CustomButton from "../../../components/Buttons/CustomButton";
import CustomTooltip from "../../../components/Tooltip/Tooltip";
import {
  getCalenderList,
  deleteCalender,
  saveCalender,
  updateCalender,
  copyCalender,
} from "../../../redux/actions/calendar";
import { updateCurrentWorkspaceCalender } from '../../../redux/actions/workspace';
import { useDispatch, useSelector } from "react-redux";
import CalendarForm from "../../../components/CalendarModule/CalendarForm.view";
import CalendarList from "../../../components/CalendarModule/CalendarList.view";
import DeleteWorkScheduleModal from "../../../components/CalendarModule/component/DeleteWorkScheduleModal";

const TeamCalendar = (props) => {
  const { classes, enqueueSnackbar } = props;

  const dispatch = useDispatch()
  const user = useSelector((store) => store.profile.data);

  const { activeTeam, teams } = user;
  const currentTeam = teams.find(t => t.companyId === activeTeam) || defaultTeamObj;
  const activeWorkspace = user.workspace.find((workspace) => workspace.teamId === user.loggedInTeam);

  const [data, setData] = useState(undefined);
  const [refresh, setRefresh] = useState(true);
  const [selected, setSelected] = useState({ data: undefined, type: undefined, loading: false });
  const role = user.teamMember.find((member) => member.userId == user.userId)?.role;

  useEffect(() => {
    if (refresh)
      getCalenderList((resp) => {
        const system = resp.entity.shift();
        const newval = [system, ...[...resp.entity].reverse()];
        setData(newval);
        setRefresh(false);
      });
  }, [refresh]);

  const showSnackBar = (message, type) => {
    enqueueSnackbar(
      <div className={classes.snackBarHeadingCnt}>
        <p className={classes.snackBarContent}>{message}</p>
      </div>,
      {
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
        variant: type ? type : "info",
      }
    );
  };

  const onSubmit = async (value) => {
    if (selected.data && selected.data.isSystemCalender === false)
      await updateCalender(
        { calenderId: selected.data.calenderId, ...value },
        (resp) => {
          setData((prev) =>
            prev.filter((item) => (item.calenderId === resp.calenderId ? resp : item))
          );
          setSelected({ data: undefined, type: undefined, loading: false });
          setRefresh(true);
          showSnackBar("Work Schedule updated successfully", "success");
          if (resp.calenderId === activeWorkspace.calendar.calenderId) dispatch(updateCurrentWorkspaceCalender(resp))
        },
        (error) => showSnackBar(error.data.message, "error")
      );
    else
      await saveCalender(
        {
          ...omit(
            value,
            "calenderId",
            "createdDate",
            "updatedDate",
            "createdBy",
            "updatedBy",
            "Id"
          ),
          isSystemCalender: false,
        },
        (resp) => {
          setData((prev) => {
            const system = prev.shift();
            const newval = [system, ...[resp, ...prev]];
            return newval;
          });
          setSelected({ data: undefined, type: undefined, loading: false });
          setRefresh(true);
          showSnackBar("Work Schedule created successfully", "success");
        },
        (error) => showSnackBar(error.data.message, "error")
      );
  };

  const onCopy = async (value) => {
    setSelected({ data: value, loading: true, type: "copy" });
    const body = {
      ...omit(value, "calenderId", "createdDate", "updatedDate", "createdBy", "updatedBy", "Id"),
      title: `Copy of ${value.title ?? ""}`,
    };
    setData((prev) => [body, ...prev]);
    await copyCalender(
      body,
      (resp) => {
        setSelected({ data: undefined, type: undefined, loading: false });
        showSnackBar("Work Schedule created successfully", "success");
      },
      (error) => showSnackBar(error.data.message, "error")
    );

    setRefresh(true);
    setSelected({ data: undefined, type: undefined, loading: false });
  };

  const onDelete = (value) => {
    setSelected({ data: value, loading: false, type: "delete" });
  };

  const onEdit = (value) => {
    setSelected({ data: value, loading: false, type: "edit" });
  };

  const confirmDelete = () => {
    setSelected((prev) => ({ ...prev, loading: true }));
    deleteCalender(
      selected.data.calenderId,
      () => {
        setData((prev) => prev.filter((item) => item.calenderId !== selected.data.calenderId)
        );
        setSelected({ data: undefined, type: undefined, loading: false });
        showSnackBar("Work Schedule deleted successfully", "success");
      },
      (error) => {
        setSelected((prev) => ({ ...prev, loading: false }));
        showSnackBar(error.data.message, "error");
      }
    );
  }

  if (!data)
    return (
      <div
        style={{
          height: window.innerHeight - 325,
          width: "100%",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}>
        <div className="loader" />
      </div>
    );

  if (selected.type === "edit")
    return (
      <div className={`${classes.CalendarMain} ${classes.configCnt}`}>
        <CalendarForm
          defaultValue={selected.data}
          onCancel={() => setSelected({ data: undefined, type: undefined, loading: false })}
          onSubmit={onSubmit}
          canCreate={data.length < currentTeam.allowedSchdules}
        />
      </div>
    );

  return (
    <div>
      <div className={classes.CalendarMain}>
        <div className={classes.editActionBtn}>
          <div className={classes.CreateScheduleText}>Schedules</div>
          {(role == "Owner" || role == "Admin") && (
            <CustomTooltip
              placement="top"
              helptext={currentTeam.allowedSchdules <= data.length ? "You've reached your Work Schedule creation quota limit, please contact support to increase your quota" : ''}>
              <CustomButton
                onClick={() => currentTeam.allowedSchdules <= data.length ? {} : setSelected({ data: undefined, type: "edit" })}
                btnType="success"
                variant="contained"
                // disabled={currentTeam.allowedSchdules <= data.length}
                style={{ marginBottom: 0, pointerEvents: 'auto' }}>
                Create New
              </CustomButton>
            </CustomTooltip>
          )}
        </div>

        <div className={classes.ListView}>
          <CalendarList
            active={data.find((item) => item.isSystemCalender)?.calenderId}
            onCopy={onCopy}
            onDelete={onDelete}
            onEdit={onEdit}
            onDefault={() => { }}
            canEdit={true}
            loading={selected.loading}
            data={data}
            canCreate={data.length < currentTeam.allowedSchdules}
          />
        </div>
      </div>

      <DeleteWorkScheduleModal
        open={selected.type === "delete"}
        loading={selected.loading}
        onClose={() => setSelected({ data: undefined, type: undefined, loading: false })}
        onSubmit={confirmDelete}
      />
    </div>
  );
};

export default compose(withSnackbar, withStyles(styles, { withTheme: true }))(TeamCalendar);
