import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import workspaceSetting from "./styles";
import { compose } from "redux";
import menuStyles from "../../assets/jss/components/menu";
import CustomIconButton from "../../components/Buttons/IconButton";
import SelectionMenu from "../../components/Menu/SelectionMenu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import combineStyles from "../../utils/mergeStyles";
import MoreVerticalIcon from "@material-ui/icons/MoreVert";
import { connect } from "react-redux";
import ActionConfirmation from "../../components/Dialog/ConfirmationDialogs/ActionConfirmation";
import {
  DisableTeamMember,
  EnableTeamMember,
  RemoveTeamMember,
} from "../../redux/actions/team";
import UpdateSubscription from "../billing/UpdateSubscription/UpdateSubscription";
import Constants from "../../components/constants/planConstant";
import { FormattedMessage } from "react-intl";
class MoreActionDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      placement: "",
      pickerOpen: false,
      pickerPlacement: "",
      disableConfirmDialog: false,
      disableMemberBtnQuery: "",
      enableMemberBtnQuery: "",
      enableConfirmDialog: false,
      removeConfirmDialog: "",
      removeMemberBtnQuery: "",
      showUpdateSubscription: false,
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handlePickerClose = this.handlePickerClose.bind(this);
  }

  handleClose(event) {
    this.setState({ open: false, pickerOpen: false });
  }
  handlePickerClose(event) {
    this.setState({ pickerOpen: false });
  }

  handleClick(event, placement) {
    const { currentTarget } = event;
    this.setState((state) => ({
      open: state.placement !== placement || !state.open,
      placement,
    }));
  }
  closeDisableMember = () => {
    this.setState({ disableConfirmDialog: false });
  };
  closeEnableMember = () => {
    this.setState({ enableConfirmDialog: false });
  };
  openDisableConfirm = () => {
    this.setState({ disableConfirmDialog: true, open: false });
  };
  openEnableConfirm = () => {
    // let allMembers = this.props.profileState.data.teamMember;
    let paidMembers = this.props.profileState.data.teamMember.filter(
      (item) =>
        item.isDeleted == false &&
        (item.isActive == null || item.isActive == true)
    ).length;
    let companyInfo = this.props.profileState.data.teams.find(
      (item) => item.companyId == this.props.profileState.data.activeTeam
    );
    const { allowedMembers, subscriptionDetails } = companyInfo;
    const { paymentPlanTitle } = subscriptionDetails;
    if (
      Constants.isPlanPaid(paymentPlanTitle) &&
      allowedMembers < paidMembers + 1
    ) {
      let minimumUser = paidMembers + 1 - allowedMembers;
      this.setState({ showUpdateSubscription: true, minimumUser: minimumUser });
    } else {
      this.setState({ enableConfirmDialog: true });
    }
  };
  openRemoveConfirm = () => {
    this.setState({ removeConfirmDialog: true, open: false });
  };
  closeRemoveMember = () => {
    this.setState({ removeConfirmDialog: false });
  };
  disableMember = () => {
    const { userId } = this.props;
    this.setState({ disableMemberBtnQuery: "progress" });
    let workspaceMemberObj = this.props.profileState.data.member.allMembers.find(
      (m) => {
        return m.userId == userId;
      }
    );
    let UpdatedWorkspaceMember = { ...workspaceMemberObj, isActive: false };
    this.props.DisableTeamMember(
      userId,
      UpdatedWorkspaceMember,
      (response) => {
        this.setState({
          disableMemberBtnQuery: "",
          disableConfirmDialog: false,
        });
      },
      (error) => {
        this.setState({
          disableMemberBtnQuery: "",
        });
        if (error && error.status === 500) {
          // self.showSnackBar('Server throws error','error');
        }
      }
    );
  };
  enableMember = () => {
    const { userId } = this.props;
    this.setState({ enableMemberBtnQuery: "progress" });
    let workspaceMemberObj = this.props.profileState.data.member.allMembers.find(
      (m) => {
        return m.userId == userId;
      }
    );
    let UpdatedWorkspaceMember = { ...workspaceMemberObj, isActive: true };
    this.props.EnableTeamMember(
      userId,
      UpdatedWorkspaceMember,
      (response) => {
        this.setState({
          enableMemberBtnQuery: "",
          enableConfirmDialog: false,
        });
      },
      (error) => {
        this.setState({
          enableMemberBtnQuery: "",
        });
        if (error && error.status === 500) {
          // self.showSnackBar('Server throws error','error');
        }
      }
    );
  };
  removeMember = () => {
    const { userId } = this.props;
    this.setState({ removeMemberBtnQuery: "progress" });
    this.props.RemoveTeamMember(
      userId,
      (response) => {
        this.setState({
          removeMemberBtnQuery: "",
          removeConfirmDialog: false,
        });
      },
      (error) => {
        this.setState({
          removeMemberBtnQuery: "",
        });
        this.closeRemoveMember()
        this.props.showSnackBar(error && error.data, 'error')
        if (error && error.status === 500) {
          // self.showSnackBar('Server throws error','error');
        }
      }
    );
  };
  exitUpdateSubscription = () => {
    this.setState({ showUpdateSubscription: false, minimumUser: 1 });
  };
  subscriptionSuccessfull = () => {
    this.setState({ showUpdateSubscription: false, minimumUser: 1 }, () => {
      this.enableMember();
    });
  };
  showSnackBar = (msg) => {
  };
  render() {
    const { classes, theme, permission, isActive, userId } = this.props;
    const {
      open,
      placement,
      disableConfirmDialog,
      enableConfirmDialog,
      disableMemberBtnQuery,
      enableMemberBtnQuery,
      removeConfirmDialog,
      removeMemberBtnQuery,
      minimumUser,
      showUpdateSubscription,
    } = this.state;
    return (
      <Fragment>
        {showUpdateSubscription && (
          <UpdateSubscription
            minimumUser={minimumUser}
            exitUpdateSubscription={this.exitUpdateSubscription}
            subscriptionSuccessfull={this.subscriptionSuccessfull}
            showSnackBar={this.showSnackBar}
          />
        )}
        <ActionConfirmation
          open={disableConfirmDialog}
          closeAction={this.closeDisableMember}
          cancelBtnText={
            <FormattedMessage
              id="common.action.cancel.label"
              defaultMessage="Cancel"
            />
          }
          successBtnText={
            <FormattedMessage
              id="team-settings.user-management.disable-user-button.label"
              defaultMessage="Disable User"
            />
          }
          alignment="center"
          headingText={
            <FormattedMessage
              id="common.action.disable.label"
              defaultMessage={`Disable`}
            />
          }
          iconType="archive"
          msgText={
            <FormattedMessage
              id="team-settings.user-management.disable-user-button.action"
              defaultMessage={`Are you sure you want to disable the selected user?`}
            />
          }
          successAction={this.disableMember}
          btnQuery={disableMemberBtnQuery}
        />
        <ActionConfirmation
          open={removeConfirmDialog}
          closeAction={this.closeRemoveMember}
          cancelBtnText={
            <FormattedMessage
              id="common.action.cancel.label"
              defaultMessage="Cancel"
            />
          }
          successBtnText={
            <FormattedMessage
              id="team-settings.user-management.delete-user-button.label"
              defaultMessage="Delete User"
            />
          }
          alignment="center"
          headingText={
            <FormattedMessage
              id="team-settings.user-management.confirmation.delete-user.title"
              defaultMessage="Delete"
            />
          }
          iconType="archive"
          msgText={
            <FormattedMessage
              id="team-settings.user-management.confirmation.delete-user.label"
              defaultMessage="Are you sure you want to delete the selected user?"
            />
          }
          successAction={this.removeMember}
          btnQuery={removeMemberBtnQuery}
          successBtnType="danger"
        />
        <ActionConfirmation
          open={enableConfirmDialog}
          closeAction={this.closeEnableMember}
          cancelBtnText={
            <FormattedMessage
              id="common.action.cancel.label"
              defaultMessage="Cancel"
            />
          }
          successBtnText={
            <FormattedMessage
              id="team-settings.user-management.confirmation.enable-user.enable-user-button.label"
              defaultMessage="Enable User"
            />
          }
          alignment="center"
          headingText={
            <FormattedMessage
              id="team-settings.user-management.confirmation.enable-user.title"
              defaultMessage="Enable"
            />
          }
          iconType="archive"
          msgText={
            <FormattedMessage
              id="team-settings.user-management.confirmation.enable-user.label"
              defaultMessage="Are you sure you want to enable the selected user?"
            />
          }
          successAction={this.enableMember}
          btnQuery={enableMemberBtnQuery}
        />
        <ClickAwayListener onClickAway={this.handleClose}>
          <div>
            <CustomIconButton
              btnType="condensed"
              onClick={(event) => {
                this.handleClick(event, "bottom-end");
              }}
              buttonRef={(node) => {
                this.anchorEl = node;
              }}
            >
              <MoreVerticalIcon
                htmlColor={theme.palette.secondary.medDark}
                style={{ fontSize: "28px" }}
              />
            </CustomIconButton>

            <SelectionMenu
              open={open}
              closeAction={this.handleClose}
              placement={placement}
              anchorRef={this.anchorEl}
              list={
                <List>
                  <ListItem disableRipple className={classes.headingItem}>
                    <ListItemText
                      primary={
                        <FormattedMessage
                          id="common.action.label"
                          defaultMessage="Select Action"
                        />
                      }
                      classes={{ primary: classes.headingText }}
                    />
                  </ListItem>
                  {isActive ==
                    false ? //   classes={{ selected: classes.statusMenuItemSelected }} //   disableRipple //   button // <ListItem // Checking if user is disabled
                    //   onClick={this.openEnableConfirm}
                    // >
                    //   <ListItemText
                    //     primary="Enable this user"
                    //     classes={{
                    //       primary: classes.statusItemText
                    //     }}
                    //   />
                    // </ListItem>
                    null : isActive == true ? ( //checking if user is active
                      <ListItem
                        button
                        disableRipple
                        classes={{ selected: classes.statusMenuItemSelected }}
                        onClick={this.openDisableConfirm}
                      >
                        <ListItemText
                          primary={
                            <FormattedMessage
                              id="team-settings.user-management.disable-user-button.label"
                              defaultMessage="Disable User"
                            />
                          }
                          classes={{
                            primary: classes.statusItemText,
                          }}
                        />
                      </ListItem>
                    ) : null}
                  <ListItem
                    button
                    disableRipple
                    classes={{ selected: classes.statusMenuItemSelected }}
                    onClick={this.openRemoveConfirm}
                  >
                    <ListItemText
                      primary={
                        <FormattedMessage
                          id="team-settings.user-management.delete-user-button.label"
                          defaultMessage="Delete User"
                        />
                      }
                      classes={{
                        primary: classes.statusItemTextDanger,
                      }}
                      style={{ color: theme.palette.text.danger }}
                    />
                  </ListItem>
                </List>
              }
            />
          </div>
        </ClickAwayListener>
      </Fragment>
    );
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    profileState: state.profile,
  };
};
export default compose(
  connect(mapStateToProps, {
    DisableTeamMember,
    EnableTeamMember,
    RemoveTeamMember,
  }),
  withStyles(combineStyles(workspaceSetting, menuStyles), {
    withTheme: true,
  })
)(MoreActionDropdown);
