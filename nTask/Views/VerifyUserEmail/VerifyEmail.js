import React, { Component } from "react";
import SvgIcon from "@material-ui/core/SvgIcon";
import Typography from "@material-ui/core/Typography";
import InviteIcon from "../../components/Icons/InviteIcon";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import { compose } from "redux";
import verifyEmailStyles from "./style";
import CustomButton from "../../components/Buttons/CustomButton";
import Paper from "@material-ui/core/Paper";
import { resendEmailVerification } from "../../redux/actions/profile";
import { Logout } from "../../redux/actions/logout";
import { connect } from "react-redux";

class VerifyUserEmail extends Component {
  constructor(props) {
    super(props);
    this.state = { btnQuery: "", step: 0 };
  }
  handleEmailSend = () => {
    this.setState({ btnQuery: "progress" });
    resendEmailVerification(() => {
      this.setState({ btnQuery: "", step: 1 });
    });
  };
  logoutUser = () => {
    this.props.Logout(null, this.props.history);
  };
  render() {
    const { theme, classes } = this.props;
    const { btnQuery, step } = this.state;

    return (
      <div className={classes.varticalAlignParent}>
        <div className={classes.varticalAlignChild}>
          <div className={classes.loginMainCnt}>
            <Paper className={classes.verifyEmailCnt}>
              <SvgIcon
                viewBox="0 0 458.6 434.9"
                className={classes.mailIcon}
                htmlColor={theme.palette.secondary.light}
              >
                <InviteIcon />
              </SvgIcon>
              {step == 0 ? (
                <>
                  <Typography
                    variant="h1"
                    classes={{ h1: classes.mainHeadingText }}
                  >
                    Verify your email address
                  </Typography>
                  <Typography
                    variant="body2"
                    align="center"
                    style={{ color: theme.palette.text.darkGray }}
                  >
                    We want you to be secure.
                  </Typography>
                  <Typography
                    variant="body2"
                    align="center"
                    style={{ color: theme.palette.text.darkGray }}
                  >
                    An email has been sent to your registered email.
                  </Typography>{" "}
                  <br />
                  <Typography
                    variant="body2"
                    align="center"
                    style={{
                      color: theme.palette.text.darkGray,
                      marginBottom: 25
                    }}
                  >
                    Didn't get the verification email? Click on button below
                  </Typography>
                  <div className={classes.btnCnt}>
                    <CustomButton
                      style={{ marginRight: "20px" }}
                      onClick={this.logoutUser}
                      btnType="plain"
                      variant="text"
                    >
                      Sign In
                    </CustomButton>
                    <CustomButton
                      onClick={this.handleEmailSend}
                      btnType="success"
                      variant="contained"
                      query={btnQuery}
                      disabled={btnQuery == "progress"}
                    >
                      Resend Verification Email
                    </CustomButton>{" "}
                  </div>
                </>
              ) : (
                <>
                  <Typography
                    variant="h1"
                    classes={{ h1: classes.mainHeadingText }}
                  >
                    You've got mail!
                  </Typography>
                  <Typography
                    variant="body2"
                    align="center"
                    style={{
                      color: theme.palette.text.darkGray,
                      marginBottom: 5
                    }}
                  >
                    Verification email has been sent successfully.
                  </Typography>
                  <Typography
                    variant="body2"
                    align="center"
                    style={{ color: theme.palette.text.darkGray }}
                  >
                    Please check your mail inbox.
                  </Typography>
                </>
              )}
            </Paper>
          </div>
        </div>
      </div>
    );
  }
}

export default compose(
  connect(null, { Logout }),
  withStyles(verifyEmailStyles, { withTheme: true }),
  withRouter
)(VerifyUserEmail);
