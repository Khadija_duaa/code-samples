import headerHeight from "./helper/getHeaderHeight";
const sideBarWidth = 221; //Width of sidebar when open

const appStyles = theme => ({
    root: {
      flexGrow: 1,
    //  height: 1000,
      zIndex: 1,
      width: "100%",
      overflow: 'hidden',
      position: 'relative',
      display: 'flex'
    },
    contentCollab: {
      flexGrow: 1,
      // background: "#fafafa",
      background: "white",
      padding: 0,
      marginTop: 88,
       minHeight: `calc(100vh - ${headerHeight}px)`,
       overflowX: "unset",   
       width: "100%",
    },
  contentShift: {
    flexGrow: 1,
    background: "#fafafa",
    padding: 0,
    paddingTop: headerHeight,
    minHeight: "100vh",
    width: "100%",
    overflowX: "auto",
    marginLeft: 73,
  },
    toolbar: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      padding: '0 8px',
      ...theme.mixins.toolbar
    },
    content: {
      flexGrow: 1,
      // background: "#fafafa",
      background: "white",
      padding: 0,
      marginTop: 88,
       minHeight: `calc(100vh - ${headerHeight}px)`,
       overflowX: "auto",   
       width: "100%",
    },
    newPaddingAdjustment:{
      marginTop: "46px",
       width: `calc(100% - ${sideBarWidth - 1}px) !important`,
      minHeight: `calc(100vh - 40px)`,
    },
    sidebarClose:{
      width: `calc(100% - 1px) !important`,
      marginLeft: -1
    },
      success: { color: "rgba(51, 51, 51, 1)", borderRadius: 4, backgroundColor:"white", borderLeft: "3px solid #4ac076" , minWidth: 400, fontFamily: `"Roboto", "Helvetica", "Arial", sans-serif !important`, fontSize: "14px !important" },
      error: { color: "rgba(51, 51, 51, 1)", borderRadius: 4, backgroundColor:"white", borderLeft: "3px solid #ed765e", minWidth: 400, fontFamily: `"Roboto", "Helvetica", "Arial", sans-serif !important`, fontSize: "14px !important" },
      warning: { color: "rgba(51, 51, 51, 1)", borderRadius: 4, backgroundColor:"white", borderLeft: "3px solid #fba848", minWidth: 400, fontFamily: `"Roboto", "Helvetica", "Arial", sans-serif !important`, fontSize: "14px !important" },
      info: { color: "rgba(51, 51, 51, 1)", borderRadius: 4, backgroundColor:"white", borderLeft: "3px solid rgba(81, 133, 252, 1)", minWidth: 400, fontFamily: `"Roboto", "Helvetica", "Arial", sans-serif !important`, fontSize: "14px !important" },
      dismissButton:{
        fontSize: "13px",
        fontFamily: "Roboto, Helvetica, Arial, sans-serif !important",
        color: "rgba(51, 51, 51, 1)",
        '&:hover':{
          background: "transparent"
        }
      },
      topNotificationBar: {
         background: theme.palette.background.blue,
         padding: "7px 20px",
         position: "fixed",
         top: 0,
         left: 0,
         right: 0,
         zIndex: 11,
         display: "flex",
        //  display: "none",
         justifyContent: "center",
         "& > p":{
          color: theme.palette.common.white,
          fontSize: "12px !important",
          marginRight: 10
         },
         "& + div": {
          transform: "translateY(51px)"
         }
      },
        snackBarHeadingCnt: {
        marginLeft: 10
      },
      snackBarContent: {
        margin: 0,
        fontSize: "12px !important"
      },
      surveyPageAdjustments:{
        height: "calc(100vh - 46px) !important",
        minHeight: 'auto !important',
      },
      surveyPageAdjustmentsWithHeight:{
        height: "calc(100vh - 100px) !important",
        minHeight: 'auto !important',
      }
  });

  export default appStyles
