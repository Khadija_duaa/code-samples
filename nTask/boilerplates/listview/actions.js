export function get_featureName_Action(data) {
  return {
    type: constants.SAVE_ALL_featureName_,
    payload: data,
  };
}
export function get_featureName_(param, dispatchfn, callback = () => {}, failure = () => {}) {
  if (dispatchfn) {
    instance()
      .get("api/_featureName_")
      .then(res => {
        dispatchfn(get_featureName_Action(res.data.entity));
        callback(res.data.entity);
      });
  } else {
    return dispatch =>
      instance()
        .get('api/_featureName_')
        .then(res => {
          dispatch(get_featureName_Action(res.data.entity));
          callback();
        });
  }
}