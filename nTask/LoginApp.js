import React, { Component } from "react";
import "./App.css";
import { withStyles } from "@material-ui/core/styles";
import appStyles from "./appStyles";
import { compose } from "redux";
import CustomRoutes from "./routes/LoginRoutes";
import Button from "@material-ui/core/Button";
import InfoIcon from "@material-ui/icons/Info";
import ErrorIcon from "@material-ui/icons/Error";
import WarningIcon from "@material-ui/icons/Warning";
import CheckedIcon from "@material-ui/icons/CheckCircle";
import CssBaseline from "@material-ui/core/CssBaseline";
import { MuiThemeProvider } from "@material-ui/core/styles";
import { defaultTheme } from "./assets/jss/theme";
import { SnackbarProvider } from "notistack";
import { validateToken } from "./redux/actions/authentication";
import { connect } from "react-redux";
import { dispatchTask } from "./redux/actions/tasks";
import helper from "./helper/index";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      authenticated: true
    };
  }
  componentDidMount() {
    const token = helper.getToken();
    if (token) {
      this.props.validateToken(
        (response) => { },
        (error) => {
            localStorage.clear();
            this.setState({ authenticated: false })

        });
    } else {
    }
  }
  render() {
    const { classes, auth } = this.props;
    const { authenticated } = this.state;
    const token = helper.getToken();
    return (
      <MuiThemeProvider theme={defaultTheme}>
        <SnackbarProvider
          maxSnack={3}
          iconVariant={{
            success: <CheckedIcon htmlColor="#4ac076" style={{ marginRight: 5 }} />,
            error: <ErrorIcon htmlColor="#ed765e" style={{ marginRight: 5 }} />,
            warning: <WarningIcon htmlColor="#fba848" style={{ marginRight: 5 }} />,
            info: <InfoIcon htmlColor="rgba(81, 133, 252, 1)" style={{ marginRight: 5 }} />
          }}
          action={[
            <Button
              color="secondary"
              className={classes.dismissButton}
              size="small"
            >
              Close
            </Button>
          ]}
          classes={{
            variantSuccess: classes.success,
            variantError: classes.error,
            variantWarning: classes.warning,
            variantInfo: classes.info
          }}
        >
          <CssBaseline />
          {auth || !token ? <CustomRoutes authorized={auth && authenticated} /> : null}
        </SnackbarProvider>
      </MuiThemeProvider>
    );
  }
}
function mapStateToProps(state) {
  return { auth: state.authenticationResponse.authState };
}
export default compose(
  withStyles(appStyles, { withTheme: true }),
  connect(
    mapStateToProps,
    { validateToken, dispatchTask }
  )
)(App);
