import findIndex from 'lodash/findIndex';

function GetMeetingAttendees(meetings, taskId) {
    let index = findIndex(meetings, function (o) { return o.taskId === taskId; });
    if (index > -1)
        return meetings[index]['attendees'];
    return []
}

export default GetMeetingAttendees