import cloneDeep from "lodash/cloneDeep";
import findIndex from "lodash/findIndex";

import { store } from '../index';

const getTeamsFromStore = () => {
    let newState = store.getState();
    return cloneDeep(newState.profile.data.workspace);
}

export const getTeamWithIndex = (index, teams) => {
    if (!teams)
        teams = getTeamsFromStore();
    if (teams.length && teams.length - 1 >= index)
        return teams[index];
    return null;
}

export const getTeamsExcluding = (teamId) => {
    let teams = getTeamsFromStore();
    const teamIndex = findIndex(teams || [], { teamId });
    if (teamIndex > -1)
        teams.splice(teamIndex, 1);
    return teams;
}

export const getAnyTeamIdExcept = (teamId) => {
    let team = getTeamWithIndex(0, getTeamsExcluding(teamId));
    if (team)
        return team.teamId;
    return null;
}