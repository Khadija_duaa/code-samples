

function GetUserTasks(tasks, userId){
    const filteredTasks = tasks.filter(task => {
        return task.assigneeList.indexOf(userId) > -1
    })

    return filteredTasks
}

export default GetUserTasks

