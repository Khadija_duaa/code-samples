import cloneDeep from "lodash/cloneDeep";

function updateCFSelectedOption(data, currentField) {
  const { fieldType, settings } = currentField;
  //For multiselect Select Dropdown
  if (fieldType == "dropdown" && settings.multiSelect) {
    const getSelectedOption = selectedId => {
      return currentField.values.data.find(a => a.id == selectedId);
    };
    const updateSelectedOption = selectedOptions =>
      selectedOptions.map(a => {
        const customFieldOption = getSelectedOption(a.id);
        const selectedOption = { ...a, ...customFieldOption, label: customFieldOption.value };
        return selectedOption;
      });
    const updatedData = data.reduce((r, cv) => {
      const updatedCfData =
        cv.customFieldData &&
        cv.customFieldData.map(c => {
          if (c.fieldId == currentField.fieldId) {
            return { ...c, fieldData: { data: updateSelectedOption(c.fieldData.data) } };
          } else {
            return c;
          }
        });
      r.push({ ...cv, customFieldData: updatedCfData || [] });
      return r;
    }, []);

    return updatedData;
  }
  //For Single Select Dropdown
  else if (fieldType == "dropdown" && !settings.multiSelect) {
    const getSelectedOption = selectedId => {
      return currentField.values.data.find(a => a.id == selectedId);
    };
    const updateSelectedOption = option => {
      const customFieldOption = getSelectedOption(option.id);
      const selectedOption = { ...option, ...customFieldOption, label: customFieldOption.value };
      return selectedOption;
    };
    const updatedData = data.reduce((r, cv) => {
      const updatedCfData =
        cv.customFieldData &&
        cv.customFieldData.map(c => {
          if (c.fieldId == currentField.fieldId) {
            return { ...c, fieldData: { data: updateSelectedOption(c.fieldData.data) } };
          } else {
            return c;
          }
        });
      r.push({ ...cv, customFieldData: updatedCfData || [] });
      return r;
    }, []);

    return updatedData;
  } else if (fieldType == "matrix") {
    const currentFieldCopy = cloneDeep(currentField);
    const updatedData = data.reduce((r, cv) => {
      const updatedCfData = cv.customFieldData.map(c => {
        if (c.fieldId == currentField.fieldId) {
          let allcolumn = currentField.settings.matrix.reduce((elem1, elem2) =>
            elem1.concat(elem2)
          ); /** merging all columns rows into one array */
          let selectedColum = allcolumn.find(
            sc => sc.cellName == c.fieldData.data[c.fieldData.data.length - 1].cellName
          ); /** finding the selected risk column option itno all columns */
          if (selectedColum) {
            const updatedValueClone = cloneDeep(c);
            updatedValueClone.fieldData.data[updatedValueClone.fieldData.data.length - 1] = {
              ...updatedValueClone.fieldData.data[updatedValueClone.fieldData.data.length - 1],
              ...selectedColum,
            };
            /** updating risk field data  */
            return updatedValueClone;
          } else return c;
        } else {
          return c;
        }
      });
      r.push({ ...cv, customFieldData: updatedCfData });
      return r;
    }, []);
    return updatedData;
  } else {
    return data;
  }
}

export default updateCFSelectedOption;
