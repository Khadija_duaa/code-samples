
function getRiskByImpactType(risks, impact) {
  const impactValue = impact == "null" ? null : impact
  return risks.filter(r => {
    return r.impact == impactValue;
  })
}

export default getRiskByImpactType