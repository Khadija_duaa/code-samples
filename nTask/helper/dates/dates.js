import { BluetoothAudioSharp } from "@material-ui/icons";
import moment from "moment";
import { useSelector } from "react-redux";
import { isAnyBusinessPlan } from "../../components/constants/planConstant";
import { uniqBy } from "lodash";
import { dayComparisonDateFormat } from "../../Views/Resources/constants";

export function getYesterday() {
  // yesterday date
  const yesterday = moment().subtract("1", "days").format();
  return yesterday;
}
export function getFiveDays() {
  // yesterday date
  const yesterday = moment().subtract("5", "days").format();
  return yesterday;
}
export function getCurrentWeek() {
  // starting day&date of current week
  const currentWeek = moment().startOf("isoWeek").format();;
  return currentWeek;
}
export function getNextWeek() {
  // starting date& day of the next week
  const nextWeek = moment()
    .add(1, "weeks")
    .startOf("isoWeek").format();;
  return nextWeek;
}

export function getNextMonth() {
  // get upComing Month
  const nextMonth = moment().add("1", "M").format();
  return nextMonth;
}

export function isDateEqual(date, type) {
  switch (type) {
    case "today":
      return moment().isSame(date, "day");
      break;
    case "yesterday":
      return moment(date).isSame(getYesterday(), "day");
      break;
    case "fiveDays":
      return moment(date).isSame(getFiveDays(), "day");
      break;
    case "currentWeek":
      return moment(date).isSame(getCurrentWeek(), "week");
      break;
    case "nextWeek":
      return moment(date).isSame(getNextWeek(), "week");
      break;
    case "currentMonth":
      return moment().isSame(date, "month");
      break;
    case "nextMonth":
      return moment(date).isSame(getNextMonth(), "month");
      break;
    default:
      break;
  }
}

export function inDateRange(date, start, end) {
  const parsedDate = date ? moment(date).format('l') : '';
  if (start && end) {
    return ((moment(parsedDate).isAfter(start) || moment(parsedDate).isSame(start)) &&
      (moment(parsedDate).isBefore(end) || moment(parsedDate).isSame(end)))
  } else if (start && !end) {
    return (moment(parsedDate).isAfter(start) || moment(parsedDate).isSame(start))
  } else if (!start && end) {
    return (moment(parsedDate).isBefore(end) || moment(parsedDate).isSame(end))
  }
  return false
}
// get Calendar from workspace or project 
export function getCalendar({ projectId = null, workspaceId = null }) {
  const state = useSelector(state => {
    return {
      workspaces: state.profile.data.workspace,
      loggedInTeam: state.profile.data.loggedInTeam,
      projects: state.projects.data || [],
      profileState: state.profile.data,
    }
  })
  const { workspaces, loggedInTeam, projects, profileState } = state;
  const { activeTeam, teams } = profileState;
  const currentTeam = teams.find(t => t.companyId === activeTeam) || defaultTeamObj;
  if (!isAnyBusinessPlan(currentTeam.subscriptionDetails.paymentPlanTitle)) {
    // console.log(currentTeam.subscriptionDetails.paymentPlanTitle);
    return null
  }
  if (projectId) {
    // console.log('get project calendar', projectId);
    const currentProject = projects.find(p => p.projectId === projectId);
    return currentProject?.calendar || null
  } else if (workspaceId) {
    // console.log('get workspaceId calendar', workspaceId);
    const currentWorkspace = workspaces.find(w => w.teamId === workspaceId);
    return currentWorkspace?.calendar || null
  } else {
    // console.log('get loggedInTeam calendar', loggedInTeam);
    const currentWorkspace = workspaces.find(w => w.teamId === loggedInTeam);
    return currentWorkspace?.calendar || null
  }
}
// handle weekend days
export function excludeOffdays(data, calendar) {
  const date = new Date(data);
  if (calendar == null) {
    return true
  }
  if (calendar.workingdays.some(day => day.id == date.getDay())) {
    return calendar.exceptions.every((exception => {
      if (exception.isRepeated && exception.type == 0) {
        if (inDateRange(date, moment(exception.fromDate).format('l'), moment(exception.repeatLimitDate).format('l'))) { // repeat limit
          if (exception.repeatType == 0) { // repeat weekly
            return date.getDay() != new Date(exception.fromDate).getDay();
          }
          if (exception.repeatType == 1) { // repeat Monthly
            return date.getDate() != new Date(exception.fromDate).getDate()
          }
          if (exception.repeatType == 2) { // repeat Yearly
            return moment(date).format('MM DD') != moment(exception.fromDate).format('MM DD')
          }
          return true
        }
      } else if (exception.type == 0) {
        return !inDateRange(date, moment(exception.fromDate).format('l'), moment(exception.toDate).format('l'))
      }
      return true
    }))
  }
}

// Rizwan ali


export function generateDateRange(periodStart, periodEnd) {
  if (!periodStart || !periodEnd) {
    return [];
  }
  const _periodStart = moment(periodStart),
    _periodEnd = moment(periodEnd);

  if (!_periodStart || !_periodStart.isValid() || !_periodEnd || !_periodEnd.isValid()) {
    return [];
  }

  let days = [],
    currentDay = _periodStart;

  const isEndOfPeriod = (currDay) => moment(currDay).isAfter(_periodEnd, "day");

  while (!isEndOfPeriod(currentDay)) {
    const cloned = currentDay.clone();
    days.push(cloned);

    currentDay.add(1, "day");
  }
  // console.log(
  //   "days:",
  //   days.map((k) => k.format(dayComparisonDateFormat))
  // );

  return days;
}

export function parseGeneratedDates(dateRange = []) {
  const sortedAsc = dateRange.sort((a, b) => {
    const _a = moment(a);
    const _b = moment(b);

    return _a.isBefore(_b) ? 0 : 1;
    //   return _a.year() - _b.year() && _a.month() - _b.month();
  });

  // console.log('sortedAsc:', sortedAsc)
  const months = sortedAsc.map((k) => {
    const day = moment(k);
    return {
      monthInx: day.month(),
      monthName: day.format("MMMM"),
      monthYear: day.year(),

      dates: [],
    };
  });

  const uniqueMonths = uniqBy(months, (e) => e.monthInx);

  const today = moment();
  const monthwise = sortedAsc.reduce((acc, curr, currInx, inputArr) => {
    let _acc = acc;

    const currMonthInx = curr.month();
    const selectedMonthInx = _acc.findIndex((k) => k.monthInx === currMonthInx);

    _acc[selectedMonthInx].dates.push({
      momentObj: curr,
      isToday: curr.isSame(today, "day"),
      weekday: curr.format("dddd"), // Fullname e.g. Thursday
      weekdayShort: curr.format("ddd"), // 3-letter name e.g. Thu
      dateOfMonth: curr.date(),
      // // below two values used for 'flattened list'
      // year: curr.year(),
      // monthIndex: curr.month(),
    });

    return _acc;
  }, uniqueMonths);

  // console.log('monthwise:', monthwise);
  // // sample output:
  // [
  //   {
  //     "monthInx": 10,
  //     "monthName": "November",
  //     "monthYear": 2022,
  //     "dates": [
  //         "2022-11-19T19:00:00.000Z",
  //         "2022-11-20T19:00:00.000Z",
  //         "2022-11-21T19:00:00.000Z",
  //         "2022-11-22T19:00:00.000Z",
  //         "2022-11-23T19:00:00.000Z",
  //         "2022-11-24T19:00:00.000Z",
  //         "2022-11-25T19:00:00.000Z"
  //     ]
  //   }
  // ]
  return monthwise;
}

export function getCurrentWeekDates() {
  const currentWeekStart = moment().startOf("week");
  const currentWeekEnd = moment().endOf("week");

  return parseGeneratedDates(generateDateRange(currentWeekStart, currentWeekEnd));
}

export function getCurrent2WeekDates() {
  const periodStart = moment().startOf("week");
  const periodEnd = moment().endOf("week").clone().add(7, "days");

  return parseGeneratedDates(generateDateRange(periodStart, periodEnd));
}

export function getCurrentMonthDates() {
  const currentMonthStart = moment().startOf("month");
  const currentMonthEnd = moment().endOf("month");

  return parseGeneratedDates(generateDateRange(currentMonthStart, currentMonthEnd));
}

export function getNextWeekDates(dayInLastWeek = moment()) {
  const dayInNextWeek = moment(dayInLastWeek).add(1, "week");
  const nextWeekStart = moment(dayInNextWeek).startOf("week");
  const nextWeekEnd = moment(dayInNextWeek).endOf("week");

  return parseGeneratedDates(generateDateRange(nextWeekStart, nextWeekEnd));
}

export function getNext2WeekDates(dayInLast2Weeks = moment()) {
  const dayInNext2Weeks = moment(dayInLast2Weeks).add(2, "weeks");
  const periodStart = moment(dayInNext2Weeks).startOf("week");
  const periodEnd = moment(dayInNext2Weeks).endOf("week").add(7, "days");

  return parseGeneratedDates(generateDateRange(periodStart, periodEnd));
}

export function getNextMonthDates(dayInLastMonth = moment()) {
  const dayInNextMonth = moment(dayInLastMonth).add(1, "month");
  const nextMonthStart = moment(dayInNextMonth).startOf("month");
  const nextMonthEnd = moment(dayInNextMonth).endOf("month");

  return parseGeneratedDates(generateDateRange(nextMonthStart, nextMonthEnd));
}

export const getMonthDaysCountByYearAndMonth = (year, monthIndex) => {
  const date = new Date(year, monthIndex);

  return moment(date).daysInMonth();
};

export const getCurrentMonthDaysCount = () => {
  return moment().daysInMonth();
};

export const parseDateToLocalMoment = (date = null) => {
  if (!date) {
    return;
  }
  return moment(date);
};

export const parseDateToUTCMoment = (date = null) => {
  if (!date) {
    return;
  }
  return moment.utc(date);
};

export const parseISODateString = (date = null) => parseDateToLocalMoment(date);
