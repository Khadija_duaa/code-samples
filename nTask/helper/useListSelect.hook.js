import { useEffect, useState } from "react";

function useListSelect(getList, accessor, searchQuery) {
  const [items, setItems] = useState([]);
  const [searchInput, setSearchInput] = useState('')
  const [selectAll, setSelectAll] = useState(false)
  const handleSelectItem = param => {
    let updatedItemsArr = items.map(i => {
      if (i[accessor] == param[accessor]) {
        return param;
      } else return i;
    });
    setItems(updatedItemsArr);
  };

 
  useEffect(() => {
    getList()
  }, []);

  const search = (value) => {
    setSearchInput(value)
  }

  const handleSelectAllItems = () => {
    let updatedItemsArr = items.map(i => {
     return {...i, checked: !selectAll}
    });
    setSelectAll(!selectAll)
    setItems(updatedItemsArr);
  }
  const filteredItems = items.filter(obj => obj[accessor].toLowerCase().indexOf(searchInput.toLowerCase()) > -1);
  const selectedItems = items.filter(it => it.checked);

  return { items: searchInput ? filteredItems : items, handleSelectItem, handleSelectAllItems, selectedItems, setItems, searchInput, search, selectAll };
}

export default useListSelect;
