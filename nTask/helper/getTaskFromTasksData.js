
import find from "lodash/find";
import cloneDeep from "lodash/cloneDeep";

import { store } from '../index';

const getTask = (tasks, taskId) => {
    let task = find(tasks, { taskId });
    if (task)
        return cloneDeep(task);
    else
        return null;
}

export const getTaskById = (taskId) => {
    let newState = store.getState();
    let task = newState.tasks ? newState.tasks.data.find(id => id.taskId == taskId) : null
    if (task) return cloneDeep(task)
    else return null
    // getTask(newState.tasks.data, taskId);
}

export const getTaskFromData = (tasks, taskId) => {
    return getTask(tasks, taskId);
}