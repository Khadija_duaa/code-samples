import { priorityData } from "../taskDropdownData";
import { priorityData as issuePriorityData, severity, typeData } from "../issueDropdownData";
import { statusData as projectStatusData } from "../projectDropdownData";
import { store } from "../../index";

export function getRowStyles(params, theme, type) {
  switch (params.node.field) {
    case "statusTitle": {
      return { background: params.node.allLeafChildren[0].data.statusColor + "2B" };
    }
    case "status":
      {
        if (type == "issue") {
          return {
            background:
              theme.palette.issueStatus[params.node.allLeafChildren[0].data.status] + "2B",
          };
        }
        if (type == "meeting") {
          return {
            background:
              theme.palette.meetingStatus[params.node.allLeafChildren[0].data.status] + "2B",
          };
        }
        if (type == "task") {
          return { background: params.node.allLeafChildren[0].data.statusColor + "2B" };
        }
        if (type == "project") {
          const projectColor = projectStatusData(theme).find(
            x => params.node.allLeafChildren[0].data.status == x.value
          ).bgColor;
          return { background: projectColor };
        }
        if (type == "risk") {
          return {
            background:
              theme.palette.riskStatus[
              params.node.allLeafChildren[0].data.status.replace(/\s/g, "")
              ] + "2B",
          };
        }
      }
      break;
    case "priority": {
      const priority = params.node.allLeafChildren[0].data.priority;
      if (type == "issue") {
        const priorityColor = issuePriorityData(theme).find(p => priority == p.value).bgColor;
        return { background: priorityColor };
      }
      if (type == "task") {
        const priorityColor = priorityData()[priority - 1].bgColor;
        return { background: priorityColor };
      }
    }
    case "project": {
      const projectId = params.node.allLeafChildren[0].data.projectId;
      const state = store.getState();
      const projects = store.getState().projects.data;
      const selectedProject = projects.find(p => p.projectId == projectId);
      if (type == "issue") {
        const projectColor = selectedProject && selectedProject.colorCode + "1a";
        return { background: projectColor };
      }
    }
    case "projects":
      {
        const projectId = params.node.allLeafChildren[0].data.projectId;
        const state = store.getState();
        const projects = store.getState().projects.data;
        const selectedProject = projects.find(p => p.projectId == projectId);
        if (type == "meeting") {
          const projectColor = selectedProject && selectedProject.colorCode + "1a";
          return { background: projectColor };
        }
      }
      break;

    case "severity":
      {
        const selectedSeverity = params.node.allLeafChildren[0].data.severity;
        const serverityColor = severity(theme).find(p => selectedSeverity == p.value).bgColor;
        return { background: serverityColor };
      }
      break;
    case "type":
      {
        if (type == "risk") {
          return {
            background: "transparent",
          };
        } else {
          const selectedType = params.node.allLeafChildren[0].data.type;
          const typeColor = typeData(theme).find(p => selectedType == p.value).bgColor;
          return { background: typeColor };
        }
      }
      break;
    case "impact":
      {
        return (
          params.node.allLeafChildren[0].data.impact && {
            background:
              theme.palette.riskImpact[
              params.node.allLeafChildren[0].data.impact.replace(/\s/g, "")
              ] + "2B",
          }
        );
      }
      break;
    default:
      return {}
  }
}

export function generateResult(a, b, operator) {
  switch (operator) {
    case 0:
      if (a !== "" && b !== "") return parseFloat(a) + parseFloat(b);
      return "";
      break;
    case 1:
      if (a !== "" && b !== "") return parseFloat(a) - parseFloat(b);
      return "";
      break;
    case 2:
      if (a !== "" && b !== "") return parseFloat(a) * parseFloat(b);
      return "";
      break;
    case 3:
      if (a !== "" && b !== "") return parseFloat(a) / parseFloat(b);
      return "";
      break;

    default:
      return "";
      break;
  }
};

export function handleCalculateValue(customFieldData = [], fieldData = {}) {
  const { fieldIdTo = "", fieldIdFrom = "", operation = 0 } = fieldData
  let fromField =
    customFieldData &&
    customFieldData.find(fromItem => fromItem.fieldId == fieldIdFrom);
  let toField =
    customFieldData &&
    customFieldData.find(toItem => toItem.fieldId == fieldIdTo);
  let result = "";
  if (fromField && toField) {
    result = generateResult(
      fromField.fieldData.data,
      toField.fieldData.data,
      operation
    );
  }
  return result;
};