

function GetUserMeetings(meetings, userId){
    const filteredMeetings = meetings.filter(meeting => {
        return meeting.attendeeList.indexOf(userId) > -1
    })

    return filteredMeetings
}

export default GetUserMeetings

