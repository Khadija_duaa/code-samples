
function getTaskByPriorityType(tasks, priority) {
  return tasks.filter(t => {
    return t.priority == priority
  })
}

export default getTaskByPriorityType