
function getTaskByStatusType(tasks, status) {
  return tasks.filter(t => {
    return t.status == status
  })
}

export default getTaskByStatusType