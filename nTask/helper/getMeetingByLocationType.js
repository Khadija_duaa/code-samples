
function getMeetingByLocationType(meetings, location) {
  const locationValue = location == "null" ? null : location
  return meetings.filter(m => {
    return m.location == locationValue
  })
}

export default getMeetingByLocationType