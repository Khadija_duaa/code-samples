import { store } from '../index';

export function generateActiveMembers() {  /** filtering members which are only active */
    let members = store.getState().profile.data.member.allMembers;
    const activeMembers = members.filter(m => {
        return m.isActive && !m.isDeleted
    })
    return activeMembers;
}