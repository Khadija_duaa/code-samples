import { getActiveFields } from "./getActiveCustomFields";
import React from 'react';
import CustomFieldIcon from "../Views/CustomFieldSettings/CustomFieldIcons/CustomFieldIcons";
export function fieldTypeData(options) {
  return options.map(o => {
    return { label: o.fieldName, value: o.fieldType, obj: o, icon:<CustomFieldIcon value={o.fieldType} color={"#969696"} /> };
  });
}
export function generateSectionData (options){
  return options.map(s=>{
    return{
      label:s.fieldName,
      value:s.fieldType,
      obj:s,
      color:s.settings.color ? s.settings.color :""
    }
  })
}
export function customFieldDropdownData(options) {
  return options?.map(o => {
    return {
      label: o.value,
      value: o.value,
      color: o.color ? o.color : "",
      id: o.id || "",
      obj: o,
    };
  });
}
export function customFieldColumnDropdownData(options) {
  return options.map(o => {
    return {
      label: o.value,
      value: o.value,
      color: o.color,
      id: o.id ? o.id : "",
      obj: o.obj ? o.obj : o,
    };
  });
}

export function groupBy(array, key) {
  // Accepts the array and key
  // Return the end result
  return array.reduce((result, currentValue) => {
    /** finding index if current value already exists  */
    let index = result.findIndex(x => x[key] == currentValue[key]);
    if (index < 0) result.push({ fieldType: currentValue[key], fields: [currentValue] });
    else result[index].fields.push(currentValue);
    return result;
  }, []); // empty array is the initial value for result object
}

export function getCustomFields(customFields, profileState, groupType, workspaceId) {
  let fields = getActiveFields(customFields.data, profileState, groupType);
  let currentWorkspaceId = workspaceId || profileState.loggedInTeam
  const filteredFields = fields.reduce((result, cv) => {
    const isExist = cv.workspaces && cv.workspaces.find(wf => wf.workspaceId == currentWorkspaceId && wf.groupType.includes(groupType));
    if (
      isExist ||
      (!isExist && cv.team && cv.team.groupType && cv.team.groupType.includes(groupType) && cv.team.teamId === profileState.activeTeam )
    )
      result.push(cv);
    return result;
  }, []);

  return filteredFields;
}
