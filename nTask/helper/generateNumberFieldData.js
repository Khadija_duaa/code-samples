/** thousand value seperator function */
export function handleThousandSeperator(x, numberField) {
  if (x === "" || !numberField.settings.useComma) return x;
  const givenNumber = x.replace(/,/g, "");
  const nfObject = new Intl.NumberFormat("en-US");
  const output = nfObject.format(givenNumber);
  return output;
}

//Generating data with respect to thousand seperater.
export function generateNumberValue(val, numberField) {
  const minusVal = val.indexOf("-") == 0 ? "-" : "";
  const number = val 
    .replace(/[^.\d]/g, "")
    .replace(/^(\d*\.?)|(\d*)\.?/g, "$1$2"); /** remove alphabets and commas from string */
  const indexOfDot = number.indexOf("."); /** finding the index of decimal point */
  if (indexOfDot < 0)
    return (
      minusVal + handleThousandSeperator(number, numberField)
    ); /** if string has no decimal point then return the value with thousand seperator logic */
  let beforeDecimalNum = number.slice(0, indexOfDot);
  let afterDecimalNum = number.slice(indexOfDot + 1, number.length);
  if (afterDecimalNum.length > parseInt(numberField.settings.decimalPlace)) return val;
  beforeDecimalNum = handleThousandSeperator(beforeDecimalNum, numberField);
  return minusVal + beforeDecimalNum + "." + afterDecimalNum;
}
