import { store } from "../index";

const defaultTeamObj = {
    config: {
        isProjectFieldMandatory: false,
        isCustomTime: false,
        timespan: 30
    },
};
export function ProjectMandatory(workspaceId = null) {
    const { loggedInTeam, workspace } = store.getState().profile.data;
    const currentWSId = workspaceId ? workspaceId : loggedInTeam;
    const currentWorkspace = workspace.find(t => t.teamId === currentWSId) || defaultTeamObj;
    const isAccessible = currentWorkspace.config.isProjectFieldMandatory;
    return isAccessible;
}
// check if task efforts mandortry in workspace settings the allow user to confirm status changing
export function TaskEffortMandatory(workspaceId = null) {
    const { loggedInTeam, workspace } = store.getState().profile.data;
    const currentWSId = workspaceId ? workspaceId : loggedInTeam;
    const currentWorkspace = workspace.find(t => t.teamId === currentWSId) || defaultTeamObj;
    return currentWorkspace.config.isUserTasksEffortMandatory;
}
// get workspace config global time increment of 30 mins
export function GlobalTimeCustom(workspaceId = null) {
    const { loggedInTeam, workspace } = store.getState().profile.data;
    const currentWSId = workspaceId ? workspaceId : loggedInTeam;
    const currentWorkspace = workspace.find(t => t.teamId === currentWSId) || defaultTeamObj;
    const configData = {
        isCustomTime: currentWorkspace.config.isCustomTime,
        timespan: currentWorkspace.config.timespan,
    };
    return configData;
}

