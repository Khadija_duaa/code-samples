

function GetProjectTasks(tasks, projectId, archived) {
    if (archived === undefined)
        archived = false;
    const filteredTasks = tasks.filter(task => {
        return task.projectId === projectId && task.isDeleted === archived
    })
    return filteredTasks
}

export default GetProjectTasks