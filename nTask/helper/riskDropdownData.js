import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";
import FlagIcon from "../components/Icons/FlagIcon";
import RoundIcon from "@material-ui/icons/Brightness1";
import RingIcon from "@material-ui/icons/RadioButtonUnchecked";
import { generateTaskData } from "./generateSelectData";
import { store } from "../index";

export function likelihoodData(t) {
  let id = "risk.common.likelihood.format";
  return [
    {
      label: "0-25 %",
      value: "0-25",
    },
    {
      label: "26-50 %",
      value: "26-50",
    },
    {
      label: "51-75 %",
      value: "51-75",
    },
    {
      label: "76-100 %",
      value: "76-100",
    },
  ];
}
export function impactData(theme, classes, t) {
  const impactColor = theme.palette.riskImpact;
  return [
    {
      label: t.formatMessage({ id: "risk.common.impact.dropdown.minor", defaultMessage: "Minor" }),
      value: "Minor",
      icon: <RingIcon htmlColor={impactColor.Minor} classes={{ root: classes.impactIcon }} />,
    },
    {
      label: t.formatMessage({
        id: "risk.common.impact.dropdown.moderate",
        defaultMessage: "Moderate",
      }),
      value: "Moderate",
      icon: <RingIcon htmlColor={impactColor.Moderate} classes={{ root: classes.impactIcon }} />,
    },
    {
      label: t.formatMessage({ id: "risk.common.impact.dropdown.major", defaultMessage: "Major" }),
      value: "Major",
      icon: <RingIcon htmlColor={impactColor.Major} classes={{ root: classes.impactIcon }} />,
    },
    {
      label: t.formatMessage({
        id: "risk.common.impact.dropdown.critical",
        defaultMessage: "Critical",
      }),
      value: "Critical",
      icon: <RingIcon htmlColor={impactColor.Critical} classes={{ root: classes.impactIcon }} />,
    },
  ];
}
export function statusData(theme, classes) {
  const statusColor = theme.palette.riskStatus;
  return [
    {
      label: "Identified",
      value: "Identified",
      icon: <RoundIcon htmlColor={statusColor.Identified} className={classes.statusIcon} />,
      color: statusColor.Identified,
      obj:{}
    },
    {
      label: "In Review",
      value: "In Review",
      icon: <RoundIcon htmlColor={statusColor.InReview} className={classes.statusIcon} />,
      color: statusColor.InReview,
      obj:{}
    },
    {
      label: "Agreed",
      value: "Agreed",
      icon: <RoundIcon htmlColor={statusColor.Agreed} className={classes.statusIcon} />,
      color: statusColor.Agreed,
      obj:{}
    },
    {
      label: "Rejected",
      value: "Rejected",
      icon: <RoundIcon htmlColor={statusColor.Rejected} className={classes.statusIcon} />,
      color: statusColor.Rejected,
      obj:{}
    },
  ];
}
export function priorityData(theme, classes) {
  const priorityColor = theme.palette.taskPriority;

  return [
    {
      label: "Critical",
      value: "Critical",
      icon: (
        <SvgIcon
          viewBox="0 0 24 32.75"
          htmlColor={priorityColor.Critical}
          className={classes.priorityIcon}>
          <FlagIcon />{" "}
        </SvgIcon>
      ),
    },
    {
      label: "High",
      value: "High",
      icon: (
        <SvgIcon
          viewBox="0 0 24 32.75"
          htmlColor={priorityColor.High}
          className={classes.priorityIcon}>
          <FlagIcon />{" "}
        </SvgIcon>
      ),
    },
    {
      label: "Medium",
      value: "Medium",
      icon: (
        <SvgIcon
          viewBox="0 0 24 32.75"
          htmlColor={priorityColor.Medium}
          className={classes.priorityIcon}>
          <FlagIcon />{" "}
        </SvgIcon>
      ),
    },
    {
      label: "Low",
      value: "Low",
      icon: (
        <SvgIcon
          viewBox="0 0 24 32.75"
          htmlColor={priorityColor.Low}
          className={classes.priorityIcon}>
          <FlagIcon />{" "}
        </SvgIcon>
      ),
    },
  ];
}

// Generate list of all projects for dropdown understandable form
export const generateTaskDropdownData = (risk = {}) => {
  const {
    tasks: { data },
  } = store.getState();
  let taskData =
    risk.projectId && risk.project !== "" ? data.filter(t => t.projectId === risk.projectId) : data;

  let tasksArr = generateTaskData(taskData);
  return tasksArr;
};

export default { statusData, impactData, likelihoodData, priorityData };
