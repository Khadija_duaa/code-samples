import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";
import FlagIcon from "../components/Icons/FlagIcon";
import RoundIcon from "@material-ui/icons/Brightness1";
import BugIcon from "../components/Icons/BugIcon";
import StarIcon from "../components/Icons/StarIcon";
import ImprovementIcon from "../components/Icons/ImprovementIcon";
import RingIcon from "@material-ui/icons/RadioButtonUnchecked";
import { generateTaskData } from "./generateSelectData";
import { store } from "../index";
// import translate from "../i18n/translate";
function translate(intl, id, value) {
  return intl ? intl.formatMessage({ id: id, defaultMessage: value }) : value;
}
export function typeData(theme, classes = {}, intl) {
  return [
    {
      label: translate(intl, "issue.common.issue-type.drop-down.bug", "Bug"),
      value: "Bug",
      bgColor: "rgb(251 228 223)",
      icon: (
        <SvgIcon
          viewBox="0 0 456.828 456.828"
          htmlColor={theme.palette.error.light}
          className={classes.bugIcon}
          style={{ fontSize: !classes.priority && 16 }}>
          <BugIcon />
        </SvgIcon>
      ),
      color: theme.palette.issueType.Bug,
    },
    {
      label: translate(intl, "issue.common.issue-type.drop-down.feature", "Feature"),
      value: "Feature",
      bgColor: "rgb(255 246 204)",
      icon: (
        <SvgIcon
          viewBox="0 0 19.481 19.481"
          classes={{ root: classes.ratingStarIcon }}
          htmlColor={theme.palette.background.star}
          style={{ fontSize: !classes.priority && 16 }}>
          <StarIcon />
        </SvgIcon>
      ),
      color: theme.palette.issueType.Feature,
    },
    {
      label: translate(intl, "issue.common.issue-type.drop-down.improvement", "Improvement"),
      value: "Improvement",
      bgColor: "rgb(227 244 207)",
      icon: (
        <SvgIcon
          viewBox="0 0 11 10.188"
          classes={{ root: classes.ImprovmentIcon }}
          htmlColor={theme.palette.primary.light}
          style={{ fontSize: !classes.priority && 16 }}>
          <ImprovementIcon />
        </SvgIcon>
      ),
      color: theme.palette.issueType.Improvement,
    },
  ];
}
export function statusData(theme, classes = {}, intl) {
  const statusColor = theme.palette.issueStatus;
  return [
    {
      label: translate(intl, "issue.common.status.drop-down.open", "Open"),
      value: "Open",
      color: statusColor.Open,

      icon: (
        <RoundIcon
          htmlColor={statusColor.Open}
          className={classes.statusIcon}
          style={{ fontSize: !classes.priority && 16 }}
        />
      ),
    },
    {
      label: translate(intl, "issue.common.status.drop-down.close", "Closed"),
      value: "Closed",
      color: statusColor.Closed,
      icon: (
        <RoundIcon
          htmlColor={statusColor.Closed}
          className={classes.statusIcon}
          style={{ fontSize: !classes.priority && 16 }}
        />
      ),
    },
  ];
}
export function priorityData(theme, classes = {}, intl) {
  const priorityColor = theme.palette.taskPriority;
  return [
    {
      label: translate(intl, "issue.common.priority.dropdown.critical", "Critical"),
      value: "Critical",
      color: priorityColor.Critical,
      bgColor: "rgba(255, 235, 235, 1)",
      icon: (
        <SvgIcon
          viewBox="0 0 24 32.75"
          htmlColor={priorityColor.Critical}
          className={classes.priorityIcon}
          style={{ fontSize: !classes.priority && 16 }}>
          <FlagIcon />{" "}
        </SvgIcon>
      ),
    },
    {
      label: translate(intl, "issue.common.priority.dropdown.high", "High"),
      value: "High",
      color: priorityColor.High,
      bgColor: "rgba(255, 235, 235, 1)",
      icon: (
        <SvgIcon
          viewBox="0 0 24 32.75"
          htmlColor={priorityColor.High}
          className={classes.priorityIcon}
          style={{ fontSize: !classes.priority && 16 }}>
          <FlagIcon />{" "}
        </SvgIcon>
      ),
    },
    {
      label: translate(intl, "issue.common.priority.dropdown.medium", "Medium"),
      value: "Medium",
      color: priorityColor.Medium,
      bgColor: "rgba(241, 251, 235, 1)",
      icon: (
        <SvgIcon
          viewBox="0 0 24 32.75"
          htmlColor={priorityColor.Medium}
          className={classes.priorityIcon}
          style={{ fontSize: !classes.priority && 16 }}>
          <FlagIcon />{" "}
        </SvgIcon>
      ),
    },
    {
      label: translate(intl, "issue.common.priority.dropdown.low", "Low"),
      value: "Low",
      color: priorityColor.Low,
      bgColor: "rgba(229, 247, 255, 1)",
      icon: (
        <SvgIcon
          viewBox="0 0 24 32.75"
          htmlColor={priorityColor.Low}
          className={classes.priorityIcon}
          style={{ fontSize: !classes.priority && 16 }}>
          <FlagIcon />{" "}
        </SvgIcon>
      ),
    },
  ];
}

export function severity(theme, classes = {}, intl) {
  const severityColor = theme.palette.severity;
  return [
    {
      label: translate(intl, "issue.common.severity.dropdown.minor", "Minor"),
      value: "Minor",
      color: severityColor.minor,
      bgColor: "rgba(229, 247, 255, 1)",
      icon: (
        <RingIcon
          htmlColor={severityColor.minor}
          className={classes.severityIcon}
          style={{ fontSize: !classes.priority && 16 }}
        />
      ),
      obj:{
        isDoneState: false
      }
    },
    {
      label: translate(intl, "issue.common.severity.dropdown.moderate", "Moderate"),
      value: "Moderate",
      color: severityColor.moderate,
      bgColor: "rgba(241, 251, 235, 1)",
      icon: (
        <RingIcon
          htmlColor={severityColor.moderate}
          className={classes.severityIcon}
          style={{ fontSize: !classes.priority && 16 }}
        />
      ),
      obj:{
        isDoneState: false
      }
    },
    {
      label: translate(intl, "issue.common.severity.dropdown.major", "Major"),
      value: "Major",
      color: severityColor.major,
      bgColor: "rgba(255, 235, 235, 1)",
      icon: (
        <RingIcon
          htmlColor={severityColor.major}
          className={classes.severityIcon}
          style={{ fontSize: !classes.priority && 16 }}
        />
      ),
      obj:{
        isDoneState: false
      }
    },
    {
      label: translate(intl, "issue.common.severity.dropdown.critical", "Critical"),
      value: "Critical",
      color: severityColor.critical,
      bgColor: "rgba(255, 235, 235, 1)",
      icon: (
        <RingIcon
          htmlColor={severityColor.critical}
          className={classes.severityIcon}
          style={{ fontSize: !classes.priority && 16 }}
        />
      ),
      obj:{
        isDoneState: false
      }
    },
  ];
}
// Generate list of all projects for dropdown understandable form
export const generateTaskDropdownData = (issue = {}) => {
  const {
    tasks: { data },
  } = store.getState();
  let tasksData =
    issue.projectId && issue.project !== ""
      ? data.filter(t => t.projectId === issue.projectId)
      : data;

  let tasksArr = generateTaskData(tasksData);
  return tasksArr;
};
export default { statusData, priorityData, typeData };
