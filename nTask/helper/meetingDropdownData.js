import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";
import FlagIcon from "../components/Icons/FlagIcon";
import RoundIcon from "@material-ui/icons/Brightness1";
import BugIcon from "../components/Icons/BugIcon";
import StarIcon from "../components/Icons/StarIcon";
import ImprovementIcon from "../components/Icons/ImprovementIcon";
import RingIcon from "@material-ui/icons/RadioButtonUnchecked";
import { generateTaskData } from "./generateSelectData";
import { store } from '../index';

export function statusData(theme, classes) {
  const statusColor = theme.palette.meetingStatus;

  return [
    {
      label: "Upcoming",
      value: "Upcoming",
      color: statusColor.Upcoming,
      icon: (
        <RoundIcon
          htmlColor={statusColor.Upcoming}
          className={classes.statusIcon}
        />
      )
    },
    {
      label: "In Review",
      value: "InReview",
      color: statusColor.InReview,
      icon: (
        <RoundIcon
          htmlColor={statusColor.InReview}
          className={classes.statusIcon}
        />
      )
    },
    {
      label: "Published",
      value: "Published",
      color: statusColor.Published,
      icon: (
        <RoundIcon
          htmlColor={statusColor.Published}
          className={classes.statusIcon}
        />
      )
    },
    {
      label: "Cancelled",
      value: "Cancelled",
      color: statusColor.Cancelled,
      icon: (
        <RoundIcon
          htmlColor={statusColor.Cancelled}
          className={classes.statusIcon}
        />
      )
    },
    {
      label: "OverDue",
      value: "OverDue",
      color: statusColor.OverDue,
      icon: (
        <RoundIcon
          htmlColor={statusColor.OverDue}
          classes={{ root: classes.statusIcon }}
        />
      )
    }
  ];
}
export const generateTaskDropdownData = (meeting = {}) => {
  const { tasks: { data } } = store.getState();
  let tasksData =
    meeting.projectId && meeting.project !== ""
      ? data.filter(t => t.projectId === meeting.projectId)
      : data;

  let tasksArr = generateTaskData(tasksData);
  return tasksArr;
};
export default statusData;
