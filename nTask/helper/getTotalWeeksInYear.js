import moment from "moment";

export const getTotalWeekInYear = (index, teams) => {
    let currentDate = new Date();
    let currentYear = currentDate.getFullYear()
    const totalNoOfWeeks = Math.max(moment(new Date(currentYear, 11, 31)).isoWeek(), moment(new Date(currentYear, 11, 31-7)).isoWeek())
return totalNoOfWeeks
}