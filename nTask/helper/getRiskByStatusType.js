
function getRiskByStatusType(risks, status) {
  return risks.filter(r => {
    return r.status == status
  })
}

export default getRiskByStatusType