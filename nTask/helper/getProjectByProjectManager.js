
function getProjectByProjectManager(projects, pm) {
  return projects.filter(p => {
    return p.projectManager === pm
  })
}

export default getProjectByProjectManager