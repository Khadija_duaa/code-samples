function GetUserPendingIssues(issues, userId){
    const filteredIssues = issues.filter(issue => {
        return issue.assignee.indexOf(userId) > -1 && issue.status !== "Closed"
    })

    return filteredIssues
}

export default GetUserPendingIssues

