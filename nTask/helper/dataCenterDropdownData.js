export const dataLocationData = [
    {
        label: "United States",
        value: "US",
        id: 'US',
    },
    {
        label: "European Union",
        value: "TE",
        id: 'IE',
    },
    {
        label: "Middle East",
        value: "BH",
        id: 'BH',
    },
    {
        label: "Asia Pacific",
        value: "SG",
        id: 'SG',
    }
]