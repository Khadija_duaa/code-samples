

function GetUserUpdatedIssue(tasks, userId){
    const filteredTasks = tasks.filter(task => {
        return task.updatedById == userId
    })

    return filteredTasks
}

export default GetUserUpdatedIssue

