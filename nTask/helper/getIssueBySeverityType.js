
function getIssueBySeverityType(issues, severity) {
  return issues.filter(i => {
    return i.severity == severity
  })
}

export default getIssueBySeverityType