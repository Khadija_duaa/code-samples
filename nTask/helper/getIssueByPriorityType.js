
function getIssueByPriorityType(issues, priority) {
  return issues.filter(i => {
    return i.priority == priority
  })
}

export default getIssueByPriorityType