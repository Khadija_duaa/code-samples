function GetProjectResources(projects, userId) {
  return projects.filter(project => {
    return project.resources &&  project.resources.indexOf(userId) >= 0;
  })
}

export default GetProjectResources;