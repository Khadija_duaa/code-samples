import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";
import IconFile from "../components/Icons/AttachmentTypeIcons/IconFile";
import IconImage from "../components/Icons/AttachmentTypeIcons/IconImage";
import IconVideo from "../components/Icons/AttachmentTypeIcons/IconVideo";
import IconZip from "../components/Icons/AttachmentTypeIcons/IconZip";
import FolderIcon from "../components/Icons/FolderIcon";

export function getIconByAttachmentType(
  theme = {},
  classes = {},
  attachmentType = ""
) {
  // const statusColor = theme.palette.projectStatus;
  if (attachmentType.includes("image")) {
    return (
      <>
        <SvgIcon
          viewBox="0 0 24 24"
          // htmlColor={theme.palette.border.brightBlue}
          className={classes.iconImage}
        >
          <IconImage />
        </SvgIcon>
      </>
    );
  } else if (attachmentType.includes("video")) {
    return (
      <>
        <SvgIcon
          viewBox="0 0 24 24"
          // htmlColor={theme.palette.border.brightBlue}
          className={classes.iconVideo}
        >
          <IconVideo />
        </SvgIcon>
      </>
    );
  } else if (
    (attachmentType.includes("application") ||
      attachmentType.includes("text")) &&
    !attachmentType.includes("zip")
  ) {
    return (
      <>
        <SvgIcon
          viewBox="0 0 24 24"
          // htmlColor={theme.palette.border.brightBlue}
          className={classes.iconFile}
        >
          <IconFile />
        </SvgIcon>
      </>
    );
  } else if (attachmentType.includes("zip")) {
    return (
      <>
        <SvgIcon
          viewBox="0 0 24 24"
          // htmlColor={theme.palette.border.brightBlue}
          className={classes.iconZip}
        >
          <IconZip />
        </SvgIcon>
      </>
    );
  }else if (attachmentType.includes("folder")) {
    return (
      <>
        <SvgIcon
          viewBox="0 0 28 28"
          // htmlColor={theme.palette.border.brightBlue}
          className={classes.iconFolder}
        >
          <FolderIcon />
        </SvgIcon>
      </>
    );
  } else return null;
}
