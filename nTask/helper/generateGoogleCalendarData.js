import { store } from "../index";

export function generateSelectData(
  label = "",
  value = "",
  id = "",
  uniqueId = "",
  obj = {}
) {
  return {
    label: label,
    value: uniqueId ? uniqueId : "" + value,
    uniqueId: uniqueId,
    id: id,
    obj: obj,
  };
}

export function generateAccountsData(accounts) {
  let ddData = accounts.map((acc, i) => {
    return generateSelectData(acc.mailId, acc.mailId, acc.mailId, null, acc);
  });
  return ddData;
}

export function generateCalendarsData(calendars) {
  let ddData = calendars.map((c, i) => {
    return generateSelectData(c.title, c.title, c.calendarId, null, c);
  });
  return ddData;
}

export function generateTeamsData(teams) {
  let ddData = teams.map((t, i) => {
    return generateSelectData(t.comapanyName, t.comapanyName, t.companyId, null, t);
  });
  return ddData;
}
export function generateWorkspacesData(workspaces,context,intl) {
  let workspace = workspaces;
  if(context != 'GOOGLE'){
    workspace.unshift({
      teamName : intl.formatMessage({id:"profile-settings-dialog.apps-integrations.common.workspace.allworkspace",defaultMessage:'All Workspaces'}),
      teamId: 1,
    });
  }
  let ddData = workspace.map((w, i) => {
    return generateSelectData(w.teamName, w.teamName, w.teamId, null, w);
  });
  return ddData;
}

export function generateProjectsData(projects, intl) {
  let data = [...projects];
  if(data.length>0 && data[0]['projectId'] !=1){
    data.unshift({
      projectName : intl.formatMessage({id:"profile-settings-dialog.apps-integrations.common.project.allproject",defaultMessage:"All Projects"}),
      projectId: 1,
    })
  }
  let ddData = data.map((p, i) => {
    return generateSelectData(p.projectName, p.projectId, p.projectId, null, p);
  });
  return ddData;
}

export function generateMeetingData(intl) {
  let data = [
    { label: intl.formatMessage({id:"profile-settings-dialog.apps-integrations.common.meeting.meeting-only",defaultMessage:"My meetings only"}), value: 1 },
    { label: intl.formatMessage({id:"profile-settings-dialog.apps-integrations.common.meeting.allmeetings",defaultMessage:"All meetings"}), value: 2 },
  ];
  let ddData = data.map((d, i) => {
    return generateSelectData(d.label, d.value, d.value, null, d);
  });
  return ddData;
}

export function generateTaskData(intl) {
  let data = [
    { label: intl.formatMessage({id:"profile-settings-dialog.apps-integrations.common.task.assigned-to-me",defaultMessage: "Tasks assigned to me"}), value: 1 },
    { label: intl.formatMessage({id:"profile-settings-dialog.apps-integrations.common.task.alltasks",defaultMessage :"All tasks"}), value: 2 },
  ];
  let ddData = data.map((d, i) => {
    return generateSelectData(d.label, d.value, d.value, null, d);
  });
  return ddData;
}

export function generateIssueData(intl) {
  let data = [
    { label: intl.formatMessage({id:"profile-settings-dialog.apps-integrations.common.issue.assigned",defaultMessage:"Issues assigned to me"}), value: 1 },
    { label: intl.formatMessage({id:"profile-settings-dialog.apps-integrations.common.issue.all-issues",defaultMessage:"All issues"}), value: 2 },
  ];
  let ddData = data.map((d, i) => {
    return generateSelectData(d.label, d.value, d.value, null, d);
  });
  return ddData;
}
