export function getActiveFields(customFields, profile, module = "") {
  /** fun that filter the fields on basis of isHidden and hiddenInWorkspaces key on user selected hide option */


  const fields = customFields.filter(cf => {
    const isHiddenInTeam = cf.settings.isHidden;
    const isHiddenInWorkspace = cf.settings.hiddenInWorkspaces.includes(profile.loggedInTeam);
    const isHiddenInGroup = cf.settings.hiddenInGroup.includes(module);


    return (!isHiddenInTeam && !isHiddenInGroup) ||
      (!isHiddenInWorkspace && !isHiddenInGroup) ||
      (!isHiddenInWorkspace && isHiddenInGroup && !isHiddenInTeam);

  });

  return fields;
}

export function getModuleActiveFields(customFields, profile, module = "") {
  /** fun that filter the fields on basis of isHidden and hiddenInGroup key on user selected hide option module wise */

  let fields;
  fields = customFields.filter(cf => {
    if (!cf.settings.isHidden && !cf.settings.hiddenInGroup.includes(module)) {
      return cf;
    }
  });
  return fields;
}
