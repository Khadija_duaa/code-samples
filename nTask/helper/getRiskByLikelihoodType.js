
function getRiskByLikelihoodType (risks, likelihood) {
  return risks.filter(r => {
    return r.likelihood == likelihood
  })
}

export default getRiskByLikelihoodType