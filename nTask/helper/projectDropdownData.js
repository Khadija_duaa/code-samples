import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";
import FlagIcon from "../components/Icons/FlagIcon";
import OnHoldIcon from "@material-ui/icons/PauseCircleFilled";
import InProgressIcon from "@material-ui/icons/Timelapse";
import CompletedIcon from "@material-ui/icons/CheckCircle";
import CancelIcon from "@material-ui/icons/NotInterested";
import RoundIcon from "@material-ui/icons/Brightness1Outlined";
import CircularIcon from "@material-ui/icons/Brightness1";

export function statusData(theme, classes, intl) {
  const statusColor = theme.palette.projectStatus;
  return [
    {
      label: intl
        ? intl.formatMessage({
            id: "project.dev.status.list.not-started",
            defaultMessage: "Not Started",
          })
        : "Not Started",
      value: 0,
      bgColor: statusColor.NotStartedBg,
      color: statusColor.NotStarted,
      // icon: <CircularIcon htmlColor={statusColor.NotStarted} style={{ fontSize: "20px !important" }} />,
      icon: (
        <RoundIcon
          htmlColor={statusColor.NotStarted}
          style={{ fontSize: "20px" }}
        />
      ),
      obj:{}
    },
    {
      label: intl
        ? intl.formatMessage({
            id: "project.dev.status.list.in-progress",
            defaultMessage: "In Progress",
          })
        : "In Progress",
      value: 1,
      bgColor: statusColor.InProgressBg,
      color: statusColor.InProgress,
      // icon: <CircularIcon htmlColor={statusColor.InProgress} style={{ fontSize: "20px !important" }} />,
      icon: <InProgressIcon htmlColor={statusColor.InProgress} style={{ fontSize: "20px" }} />,
      obj:{}
    },
    {
      label: intl
        ? intl.formatMessage({ id: "project.dev.status.list.on-hold", defaultMessage: "On Hold" })
        : "On Hold",
      value: 2,
      bgColor: statusColor.OnHoldBg,
      color: statusColor.OnHold,
      // icon: <CircularIcon htmlColor={statusColor.OnHold} style={{ fontSize: "20px !important" }} />,
      icon: <OnHoldIcon htmlColor={statusColor.OnHold} style={{ fontSize: "20px" }} />,
      obj:{}
    },
    {
      label: intl
        ? intl.formatMessage({
            id: "project.dev.status.list.completed",
            defaultMessage: "Completed",
          })
        : "Completed",
      value: 3,
      bgColor: statusColor.CompletedBg,
      color: statusColor.Completed,
      icon: <CompletedIcon htmlColor={statusColor.Completed} style={{ fontSize: "20px !important" }} />,
      obj:{}
      // icon: <CircularIcon htmlColor={statusColor.Completed} style={{ fontSize: "20px !important" }} />,
    },
    {
      label: intl
        ? intl.formatMessage({
            id: "project.dev.status.list.cancelled",
            defaultMessage: "Cancelled",
          })
        : "Cancelled",
      value: 4,
      bgColor: statusColor.CancelledBg,
      color: statusColor.Cancelled,
      icon: <CancelIcon htmlColor={statusColor.Cancelled} style={{ fontSize: "20px" }} />,
      obj:{}
      // icon: <CircularIcon htmlColor={statusColor.Cancelled} style={{ fontSize: "20px !important" }} />,
    },
  ];
}
export function priorityData(theme, classes) {
  const priorityColor = theme.palette.taskPriority;

  return [
    {
      label: "Critical",
      value: "Critical",
      icon: (
        <SvgIcon
          viewBox="0 0 24 32.75"
          htmlColor={priorityColor.Critical}
          className={classes.priorityIcon}>
          <FlagIcon />{" "}
        </SvgIcon>
      ),
    },
    {
      label: "High",
      value: "High",
      icon: (
        <SvgIcon
          viewBox="0 0 24 32.75"
          htmlColor={priorityColor.High}
          className={classes.priorityIcon}>
          <FlagIcon />{" "}
        </SvgIcon>
      ),
    },
    {
      label: "Medium",
      value: "Medium",
      icon: (
        <SvgIcon
          viewBox="0 0 24 32.75"
          htmlColor={priorityColor.Medium}
          className={classes.priorityIcon}>
          <FlagIcon />{" "}
        </SvgIcon>
      ),
    },
    {
      label: "Low",
      value: "Low",
      icon: (
        <SvgIcon
          viewBox="0 0 24 32.75"
          htmlColor={priorityColor.Low}
          className={classes.priorityIcon}>
          <FlagIcon />{" "}
        </SvgIcon>
      ),
    },
  ];
}

export function billingMethodData() {
  return [
    {
      label: "Fixed Fee",
      value: 0,
    },
    {
      label: "Fixed Fee per Task",
      value: 1,
    },
    {
      label: "Hourly Rate by Task",
      value: 2,
    },
    {
      label: "Hourly Rate by Resource",
      value: 3,
    },
  ];
}

export function currencyData(constants) {
  if (constants) {
    let currencyOptions = [];
    constants.data.currencies.forEach(function(obj) {
      /** logic for generating the currency drop down data in detail tab */
      const name = `${obj.isoCurrencySymbol} - ${obj.currencyEnglishName}`;
      currencyOptions.push({
        value: obj.isoCurrencySymbol,
        label: name,
      });
    });
    return currencyOptions || [];
  } else return [];
}

export function feeMethodData(billingMode = {}) {
  switch (billingMode.value) {
    case 0:
      return [
        {
          label: "",
          value: 0,
        },
        {
          label: "",
          value: 1,
        },
      ];
      break;
    case 1:
      return [
        {
          label: "Same Fee for Every Task",
          value: 0,
        },
        {
          label: "Different Fee per Task",
          value: 1,
        },
      ];
      break;
    case 2:
      return [
        {
          label: "Same Hourly Rate for Every Task",
          value: 0,
        },
        {
          label: "Different Hourly Rate per Task",
          value: 1,
        },
      ];
      break;
    case 3:
      return [
        {
          label: "Same Hourly Rate for Every Resource",
          value: 0,
        },
        {
          label: "Different Hourly Rate per Resource",
          value: 1,
        },
      ];
      break;

    default:
      return [];
      break;
  }
}

export function permissionData() {
  return [
    {
      label: "Project Manager",
      caption: "Full Access",
      value: "101",
    },
    {
      label: "Contributor (Full)",
      caption: "Can Add & Edit All",
      value: "102",
    },
    // {
    //   label: "Contributor (Limited)",
    //   caption: "Can Add & Edit Assigned",
    //   value: "104",
    // },
    {
      label: "Viewer (Full)",
      caption: "Can View & Add Comments All",
      value: "103",
    },
    // {
    //   label: "Viewer (Limited)",
    //   caption: "Can View & Add Comments Assigned",
    //   value: "105",
    // },
  ];
}
