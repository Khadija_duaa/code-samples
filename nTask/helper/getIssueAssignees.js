
function getIssueAssignees(issues, taskId) {
    return issues.filter(issue => issue.linkedTasks.indexOf(taskId) >= 0)
        .map(x => x.issueAssingnees) || [];
}

export default getIssueAssignees