import { store } from "../index";
const memberObject = {
  email: "-@-.com",
  fullName: "-",
  imageUrl: "",
  inviteHistory: [],
  isActive: true,
  isAdmin: false,
  isCustomRole: false,
  isDeleted: false,
  isMember: false,
  isOnline: false,
  jobTitle: "",
  lastInvitedDate: "",
  roleId: "",
  roleName: "",
  teamId: "",
  teamOwner: false,
  timeZone: "",
  userId: "",
  userName: "-@-.com",
};
export function getMembersById(memberIdArr = []) {
  /** filtering members which are only active */
  let members = store.getState().profile.data.member.allMembers;
  const membersList = members.filter(m => {
    return memberIdArr.indexOf(m.userId) > -1;
  });
  return membersList;
}

export function getMemberById(memberId = "") {
  /** filtering member which are only active */
  if (!memberId) return memberObject;
  let members = store.getState().profile.data.member.allMembers;
  const member = members.find(m => {
    return memberId === m.userId;
  });
  return member;
}
