function GetUserProjects(projects, userId) {
  const filteredProjects = projects.filter(project => {
    return project.projectManager.indexOf(userId) === 0;
  });

  return filteredProjects;
}

export default GetUserProjects;
///
