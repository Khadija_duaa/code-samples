function GetUserPendingRisks(risks, userId){
    const filteredRisks = risks.filter(risk => {
        return risk.riskOwner == userId && risk.status !== "Agreed" && risk.status !== "Rejected"
    })

    return filteredRisks
}

export default GetUserPendingRisks

