import React, { useEffect, useState, useContext } from "react";
import { updateProjectData } from "../../../redux/actions/projects";
import { useDispatch } from "react-redux";
import helper from "../../index";
import { grid } from "../../../components/CustomTable2/gridInstance";

function useUpdateProject() {
  const dispatch = useDispatch();
  const editProject = (project, type, value) => {
    let updatedValue;
    switch (type) {
      case "description":
        updatedValue = window.btoa(unescape(encodeURIComponent(value)));
        break;
      case "resources":
        updatedValue = value.map(resources => resources.userId);
        break;

      default:
    }
    //If value is some how updated/parsed than updatedValue is used else value
    let obj = { [type]: updatedValue || value };
    updateProjectData({ project, obj }, dispatch, res => {
      if (grid.grid) {
        const rowNode = grid.grid && grid.grid.getRowNode(res.id);
        rowNode && rowNode.setData(res);
      }
    },
      // failure
      (err) => {
        let errMessage = failureMessage;
        if (type === 'status' && err.data && err.data.message) errMessage = err.data.message;

      });
  };

  return {
    editProject,
  };
}

export default useUpdateProject;
