import { updateRiskData } from "../../../redux/actions/risks";
import { useDispatch } from "react-redux";
import { grid } from "../../../components/CustomTable2/gridInstance";

function useUpdateRisk() {
  const dispatch = useDispatch();
  const editRisk = (risk, type, value, success = () => {}, failure = () => {}) => {
    let obj = { [type]: value };
    updateRiskData(
      { risk, obj },
      dispatch,
      res => {
        success(res);
        if (grid.grid) {
          const rowNode = grid.grid && grid.grid.getRowNode(res.id);
          rowNode && rowNode.setData(res);
        }
      },
      // failure
      err => {
        let errMessage = err?.data?.message || "Oops! There seems to be an issue, please contact support@ntaskmanager.com.";
        failure(errMessage);
      }
    );
  };

  return {
    editRisk,
  };
}

export default useUpdateRisk;
