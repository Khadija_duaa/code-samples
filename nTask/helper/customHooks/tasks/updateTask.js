import React, { useEffect, useState, useContext } from "react";
import { updateTaskData } from "../../../redux/actions/tasks";
import { useDispatch } from "react-redux";
import helper from "../../index";
import { grid } from "../../../components/CustomTable2/gridInstance";
import { toastMessages } from "./toastMessages";
import { toast } from "react-toastify";

function useUpdateTask() {
  const dispatch = useDispatch();

  //Handle planned/actual date save
  const handleDateSave = (type, date, task, time, success = () => { }, failure = () => { }) => {
    const message = toastMessages[type];
    const defaultMessage = toastMessages.default;
    const loadingMessage = message ? message.loading : defaultMessage.loading;
    const successMessage = message ? message.success : defaultMessage.success;

    const taskUpdateToast = toast.loading(loadingMessage);
    const formatDate = helper.RETURN_CUSTOMDATEFORMAT(date);
    let obj;
    let key;
    switch (type) {
      case "actualStartDate":
        obj = { actualStartDate: formatDate, actualStartTime: time };
        key = "actualdate";
        break;
      case "actualDueDate":
        obj = { actualDueDate: formatDate, actualDueTime: time };
        key = "actualdate";
        break;
      case "startDate":
        obj = { startDate: formatDate, startTime: time };
        key = "planneddate";
        break;
      case "dueDate":
        obj = { dueDate: formatDate, dueTime: time };
        key = "planneddate";
        break;
      case "pStart":
        obj = { startDate: formatDate, startTime: time };
        break;
      case "pEnd":
        obj = { dueDate: formatDate, dueTime: time };
        break;
      case "aStart":
        obj = { actualStartDate: formatDate, actualStartTime: time };
        break;
      case "aEnd":
        obj = { actualDueDate: formatDate, actualDueTime: time };
        break;
      default:
    }
    updateTaskData({ task, obj }, 
      dispatch, 
      res => {
        success(res);
      // () => {
      // toast.update(taskUpdateToast, {
      //   render: successMessage,
      //   type: "success",
      //   isLoading: false,
      //   autoClose: 3000,
      //   hideProgressBar: false,
      //   pauseOnHover: true,
      //   closeOnClick: true,
      //   draggable: true,
      // });
    },
    err => {
      let errMessage = err?.data?.message || "Oops! Server throws error.";
      failure(errMessage);
      // let errMessage = failureMessage;
      // if (type === "status" && err.data && err.data.message) errMessage = err.data.message;
      // toast.update(taskUpdateToast, {
      //   render: errMessage,
      //   type: "error",
      //   isLoading: false,
      //   autoClose: 2000,
      //   hideProgressBar: false,
      //   pauseOnHover: true,
      //   closeOnClick: true,
      //   draggable: true,
      // });
    }
    );
  };

  const editTask = (task, type, value, success = () => { }, failure = () => { }) => {
    const loadingMessage = toastMessages[type]
      ? toastMessages[type].loading
      : toastMessages.default.loading;
    const successMessage = toastMessages[type]
      ? toastMessages[type].success
      : toastMessages.default.success;
    const failureMessage = toastMessages[type]
      ? toastMessages[type].failure
      : toastMessages.default.failure;
    const taskUpdateToast = toast.loading(loadingMessage);

    let updatedValue;
    switch (type) {
      case "description":
        updatedValue = window.btoa(unescape(encodeURIComponent(value)));
        break;
      case "assigneeList":
        updatedValue = value.map(assignee => assignee.userId);
        break;

      default:
    }
    //If value is some how updated/parsed than updatedValue is used else value
    let obj = { [type]: updatedValue || value };
    updateTaskData(
      { task, obj },
      dispatch,
      res => {
        success(res);
        // toast.update(taskUpdateToast, {
        //   render: successMessage,
        //   type: "success",
        //   isLoading: false,
        //   autoClose: 2000,
        //   hideProgressBar: false,
        //   pauseOnHover: true,
        //   closeOnClick: true,
        //   draggable: true,
        // });
        if (grid.grid) {
          const rowNode = grid.grid && grid.grid.getRowNode(res.id);
          rowNode && rowNode.setData(res);
        }
      },
      // failure
      err => {
        let errMessage = err?.data?.message || "Oops! Server throws error.";
        failure(errMessage);
        // let errMessage = failureMessage;
        // if (type === "status" && err.data && err.data.message) errMessage = err.data.message;
        // toast.update(taskUpdateToast, {
        //   render: errMessage,
        //   type: "error",
        //   isLoading: false,
        //   autoClose: 2000,
        //   hideProgressBar: false,
        //   pauseOnHover: true,
        //   closeOnClick: true,
        //   draggable: true,
        // });
      }
    );
  };

  return {
    editTask,
    handleDateSave,
  };
}

export default useUpdateTask;
