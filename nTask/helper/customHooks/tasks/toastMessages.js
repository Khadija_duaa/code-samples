import React from 'react'
export const toastMessages = {
  status: {
    loading: "Updating Status..!",
    success: "Task Status Updated Successfully!",
    failure: <>Task Status not Updated, Please contact <a href='mailto:support@ntaskmanager.com'> support@ntaskmanager.com</a></>
  },
  assigneeList: {
    loading: "Updating Task Assignee..!",
    success: "Task Assignee Updated Successfully!",
    failure: <>Task Assignment not Updated, Please contact <a href='mailto:support@ntaskmanager.com'> support@ntaskmanager.com</a></>
  },
  priority: {
    loading: "Updating Priority..!",
    success: "Priority Updated Successfully!",
    failure: <>Priority is not Updated, Please contact <a href='mailto:support@ntaskmanager.com'> support@ntaskmanager.com</a></>
  },
  startDate: {
    loading: "Updating Start Date..!",
    success: "Start Date Updated Successfully!",
    failure: <>Start Date is not Updated, Please contact <a href='mailto:support@ntaskmanager.com'> support@ntaskmanager.com</a></>
  },
  dueDate: {
    loading: "Updating Due Date..!",
    success: "Due Date  updated Successfully!",
    failure: <>Due Date is not Updated, Please contact <a href='mailto:support@ntaskmanager.com'> support@ntaskmanager.com</a></>
  },
  actualStartDate: {
    loading: "Updating Actual Start Date..!",
    success: "Actual Start Date Updated Successfully!",
    failure: <>Actual Start Date is not Updated, Please contact <a href='mailto:support@ntaskmanager.com'> support@ntaskmanager.com</a></>
  },
  actualDueDate: {
    loading: "Updating Actual Due Date..!",
    success: "Actual Due Date Updated Successfully!",
    failure: <>Actual Due Date is not Updated, Please contact <a href='mailto:support@ntaskmanager.com'> support@ntaskmanager.com</a></>
  },
  project: {
    loading: "Updating Task Project..!",
    success: "Task Project Updated Successfully!",
    failure: <>Task Project is not Updated, Please contact <a href='mailto:support@ntaskmanager.com'> support@ntaskmanager.com</a></>
  },
  description: {
    loading: "Updating Description..!",
    success: "Description Updated Successfully!",
    failure: <>Description is not Updated, Please contact <a href='mailto:support@ntaskmanager.com'> support@ntaskmanager.com</a></>
  },
  copy: {
    loading: "Updating Task Copy!",
    success: "Task Copy Updated Successfully!",
    failure: <>Task Copy is not Updated, Please contact <a href='mailto:support@ntaskmanager.com'> support@ntaskmanager.com</a></>
  },
  color: {
    loading: "Updating Task Color!",
    success: "Task Color Updated Successfully!",
    failure: <>Task Color is not Updated, Please contact <a href='mailto:support@ntaskmanager.com'> support@ntaskmanager.com</a></>
  },
  delete: {
    loading: "Deleteing Your Task!",
    success: "Task Deleted successfully!",
    failure: <>Task is not Deleted, Please contact <a href='mailto:support@ntaskmanager.com'> support@ntaskmanager.com</a></>
  },
  archive: {
    loading: "Updating Task Archive!",
    success: "Task Archived successfully!!",
    failure: <>Task is not Archived, Please contact <a href='mailto:support@ntaskmanager.com'> support@ntaskmanager.com</a></>
  },
  customField: {
    loading: "Updating",
    success: "Updated Successfully!",
    failure: <> Not Updated, Please contact <a href='mailto:support@ntaskmanager.com'> support@ntaskmanager.com</a></>
  },
  default: {
    loading: "Updating !",
    success: "Task Updated Successfully!",
    failure: <>Task is not Updated, Please contact <a href='mailto:support@ntaskmanager.com'> support@ntaskmanager.com</a></>
  },
};
