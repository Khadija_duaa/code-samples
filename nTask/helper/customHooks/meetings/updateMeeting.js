import { updateMeetingData } from "../../../redux/actions/meetings";
import { useDispatch } from "react-redux";
import helper from "../../index";
import { grid } from "../../../components/CustomTable2/gridInstance";
import { toastMessages } from "./toastMessages";
import { toast } from "react-toastify";

function useUpdateMeeting() {
  const dispatch = useDispatch();


  const editMeeting = (meeting, type, value) => {
    let updatedValue;
    switch (type) {
      case "description":
        updatedValue = window.btoa(unescape(encodeURIComponent(value)));
        break;
      case "attendeeIds":
        updatedValue = value.map(attendee => attendee.userId);
        break;

      default:
    }
    //If value is some how updated/parsed than updatedValue is used else value
    let obj = { [type]: updatedValue || value };
    updateMeetingData(
      { meeting, obj },
      dispatch,
      res => {
        if (grid.grid) {
          const rowNode = grid.grid && grid.grid.getRowNode(res.id);
          rowNode && rowNode.setData(res);
        }
      },
      // failure
      err => {
        // let errMessage = failureMessage;
        // if (type === "status" && err.data && err.data.message) errMessage = err.data.message;
      }
    );
  };

  return {
    editMeeting,
  };
}

export default useUpdateMeeting;
