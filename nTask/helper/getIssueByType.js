
function getIssueByType(issues, type) {
  const typeValue = type == "null" ? null : type == "undefined" ? undefined : type

  return issues.filter(i => {
    return i.type == typeValue
  })
}

export default getIssueByType