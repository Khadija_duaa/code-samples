import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";
import FlagIcon from "../components/Icons/FlagIcon";
import RoundIcon from "@material-ui/icons/Brightness1";
import OnHoldIcon from "@material-ui/icons/PauseCircleFilled";

import CompletedIcon from "@material-ui/icons/CheckCircle";
import CancelIcon from "@material-ui/icons/NotInterested";
import RoundIconOutlined from "@material-ui/icons/Brightness1Outlined";
import InReviewIcon from "@material-ui/icons/Help";
import CircularIcon from "@material-ui/icons/Brightness1";
import { defaultTheme } from "../assets/jss/theme";

export function statusDataNew(theme, classes) {
  const statusColor = theme.palette.taskStatusNew;

  return [
    {
      label: "Not Started",
      value: 0,
      icon: <CircularIcon htmlColor={statusColor.NotStarted} style={{ fontSize: "20px" }} />,
      color: statusColor.NotStarted
      // icon: (
      //   <RoundIconOutlined
      //     htmlColor={statusColor.NotStarted}
      //     // style={{ fontSize: "22px !important" }}
      //     className={classes.statusIcon}
      //   />
      // ),
    },
    {
      label: "In Progress",
      value: 1,
      icon: <CircularIcon htmlColor={statusColor.InProgress} style={{ fontSize: "20px" }} />,
      color: statusColor.InProgress
      // icon: (
      //   <InProgressIcon
      //     htmlColor={statusColor.InProgress}
      //     // style={{ fontSize: "22px !important" }}
      //     className={classes.statusIcon}
      //   />
      // ),
    },
    {
      label: "In Review",
      value: 2,
      icon: <CircularIcon htmlColor={statusColor.InReview} style={{ fontSize: "20px" }} />,
      color: statusColor.InReview
      // icon: (
      //   <InReviewIcon
      //     htmlColor={statusColor.InReview}
      //     // style={{ fontSize: "22px !important" }}
      //     className={classes.statusIcon}
      //   />
      // ),
    },
    {
      label: "Completed",
      value: 3,
      icon: <CircularIcon htmlColor={statusColor.Completed} style={{ fontSize: "20px" }} />,
      color: statusColor.Completed
      // icon: (
      //   <CompletedIcon
      //     htmlColor={statusColor.Completed}
      //     // style={{ fontSize: "22px !important" }}
      //     className={classes.statusIcon}
      //   />
      // ),
    },
    {
      label: "Cancelled",
      value: 4,
      icon: <CircularIcon htmlColor={statusColor.Cancelled} style={{ fontSize: "20px" }} />,
      color: statusColor.Cancelled
      // icon: (
      //   <CancelIcon
      //     htmlColor={statusColor.Cancelled}
      //     // style={{ fontSize: "22px !important" }}
      //     className={classes.statusIcon}
      //   />
      // ),
    },
  ];
}
export function statusData(theme, classes) {
  const statusColor = theme.palette.taskStatus;

  return [
    {
      label: "Not Started",
      value: "Not Started",
      icon: <RoundIcon htmlColor={statusColor.NotStarted} className={classes.statusIcon} />,
    },
    {
      label: "In Progress",
      value: "In Progress",
      icon: <RoundIcon htmlColor={statusColor.InProgress} className={classes.statusIcon} />,
    },
    {
      label: "In Review",
      value: "In Review",
      icon: <RoundIcon htmlColor={statusColor.InReview} className={classes.statusIcon} />,
    },
    {
      label: "Completed",
      value: "Completed",
      icon: <RoundIcon htmlColor={statusColor.Completed} className={classes.statusIcon} />,
    },
    {
      label: "Cancelled",
      value: "Cancelled",
      icon: (
        <RoundIcon htmlColor={statusColor.Cancelled} classes={{ root: classes.statusIcon }} />
      ),
    },
  ];
}
export function priorityData(theme, classes = {}, intl = null) {
  const priorityColor = theme ? theme.palette.taskPriority : defaultTheme.palette.taskPriority;
  return [
    {
      label: intl
        ? intl.formatMessage({
            id: "task.common.priority.dropdown.critical",
            defaultMessage: "Critical",
          })
        : "Critical",
      value: 1,
      bgColor: 'rgba(255, 235, 235, 1)',
      color:priorityColor.Critical,
      icon: (
        <SvgIcon
          viewBox="0 0 24 32.75"
          htmlColor={priorityColor.Critical}
          className={classes.priorityIcon}
          style={{fontSize: !classes.priority && 16 }}>
          <FlagIcon />{" "}
        </SvgIcon>
      ),
    },
    {
      label: intl
        ? intl.formatMessage({
            id: "task.common.priority.dropdown.high",
            defaultMessage: "High",
          })
        : "High",
      value: 2,
      bgColor: 'rgba(255, 235, 235, 1)',
      color:priorityColor.High,
      icon: (
        <SvgIcon
          viewBox="0 0 24 32.75"
          htmlColor={priorityColor.High}
          className={classes.priorityIcon}
          style={{fontSize: !classes.priority && 16 }}>
          
          <FlagIcon />{" "}
        </SvgIcon>
      ),
    },
    {
      label: intl
        ? intl.formatMessage({
            id: "task.common.priority.dropdown.medium",
            defaultMessage: "Medium",
          })
        : "Medium",
      value: 3,
      bgColor: 'rgba(241, 251, 235, 1)',
      color:priorityColor.Medium,
      icon: (
        <SvgIcon
          viewBox="0 0 24 32.75"
          htmlColor={priorityColor.Medium}
          className={classes.priorityIcon}
          style={{fontSize: !classes.priority && 16 }}>
          
          <FlagIcon />{" "}
        </SvgIcon>
      ),
    },
    {
      label: intl
        ? intl.formatMessage({
            id: "task.common.priority.dropdown.low",
            defaultMessage: "Low",
          })
        : "Low",
      value: 4,
      color:priorityColor.Low,
      bgColor: 'rgba(229, 247, 255, 1)',
      icon: (
        <SvgIcon
          viewBox="0 0 24 32.75"
          htmlColor={priorityColor.Low}
          className={classes.priorityIcon}
          style={{fontSize: !classes.priority && 16 }}
        >
          <FlagIcon />{" "}
        </SvgIcon>
      ),
    },
  ];
}
