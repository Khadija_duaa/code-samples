
function GetRiskOwner(risks, taskId) {
    return risks.filter(risk => risk.linkedTasks.indexOf(taskId) >= 0)
        .map(x => x.riskOwner) || [];
}

export default GetRiskOwner