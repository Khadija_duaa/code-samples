

function GetUserRisks(risks, userId){
    const filteredRisks = risks.filter(risk => {
        return risk.riskOwner == userId
    })

    return filteredRisks
}

export default GetUserRisks

