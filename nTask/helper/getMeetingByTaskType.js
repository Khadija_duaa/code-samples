
function getMeetingByTaskType(meetings, taskTitle) {
  const taskValue = taskTitle == "null" ? null : taskTitle == "undefined" ? undefined : taskTitle
  return meetings.filter(m => {
    return m.taskTitle == taskValue
  })
}

export default getMeetingByTaskType
