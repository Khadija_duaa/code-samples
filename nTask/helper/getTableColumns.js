function getTableColumns(type) {
  let ddData = [];
  switch (type) {
    case "task":
      ddData = [
        "ID",
        "Status",
        "Planned Start/End",
        "Actual Start/End",
        "Priority",
        "Project",
        "Progress",
        "Assignee",
        "Time Logged",
        "Comments",
        "Attachments",
        "Meetings",
        "Issues",
        "Risks",
        "Creation Date",
        "Created By",
      ];
      return ddData;
      break;
    case "risk":
      ddData = [
        "ID",
        "Status",
        // "Impact",
        // "Likelihood",
        "Risk Owner",
        // "Task",
        "Last Updated",
        "Created By",
        "Creation Date",
      ];
      return ddData;
      break;
    case "issue":
      ddData = [
        "ID",
        "Status",
        "Actual Start/End",
        "Planned Start/End",
        "Severity",
        "Priority",
        "Type",
        "Assignee",
        "Task",
        "Last Updated",
        "Created By",
        "Creation Date",
      ];
      return ddData;
      break;

    default:
      break;
  }
}

export default getTableColumns;
