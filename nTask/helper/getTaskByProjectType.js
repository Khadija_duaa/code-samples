
function getTaskByProjectType(tasks, projectId) {

  const projectValue = projectId == "null" ? null : projectId == "undefined" ? undefined : projectId
  return tasks.filter(t => {
    return t.project == projectValue
  })
}
export default getTaskByProjectType
