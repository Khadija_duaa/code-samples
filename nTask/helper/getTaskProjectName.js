
import find from "lodash/find";

import { store } from '../index';

const getProject = (projectId) => {
    let newState = store.getState();
    let project = find(newState.projects.data, { projectId });
    if (project)
        return project.projectName;
    else
        return '';
}

export const getTaskProjectName = (projectId) => {
    return getProject(projectId);
}