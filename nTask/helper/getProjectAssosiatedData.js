export const getProjectAssosiatedData = (projectId, tasks, issues, risks, meetings) => {

    let newTasks = tasks
        .filter(x => x.projectId === projectId) || [];

    let i = 0, newIssues = [], newRisks = [], newMeetings = [];


    for (i = 0; i < newTasks.length; i++) {

        // Filtering Assosiated issues
        newIssues = newIssues.concat(
            issues
                // .filter(y => y.isArchive)
                .filter(issue => issue.linkedTasks.indexOf(newTasks[i].taskId) >= 0) || []
        )

        // Filtering Assosiated risks
        newRisks = newRisks.concat(
            risks
                // .filter(y => y.isArchive)
                .filter(risk => risk.linkedTasks.indexOf(newTasks[i].taskId) >= 0) || []
        )

        // Filtering Assosiated meetings
        newMeetings = newMeetings.concat(
            meetings
                // .filter(y => y.isDelete)
                .filter(meeting => meeting.taskId === newTasks[i].taskId) || []
        )
    }

    return {
        tasks: newTasks,
        issues: newIssues,
        risks: newRisks,
        meetings: newMeetings
    }
}