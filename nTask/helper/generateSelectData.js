import React from "react";
import { store } from "../index";
import { getTotalWeekInYear } from "../helper/getTotalWeeksInYear";
import RemoveIcon from "@material-ui/icons/Remove";
import Multiply from "@material-ui/icons/Clear";
import AddIcon from "@material-ui/icons/Add";
import IconDivide from "../components/Icons/IconDivide";
import SvgIcon from "@material-ui/core/SvgIcon";

export function generateSelectData(label = "", value = "", id = "", uniqueId = "", obj = {}) {
  return {
    label: label,
    value: uniqueId ? uniqueId : "" + value,
    uniqueId: uniqueId,
    id: id,
    obj: obj,
  };
}

//Generating react select dropdown readable object for projects
export function generateProjectData(projects) {
  let ddData = projects.map((project, i) => {
    return generateSelectData(
      project.projectName,
      project.projectName,
      project.projectId,
      project.uniqueId,
      project
    );
  });
  return ddData;
}

//Generating react select dropdown readable object for assignees
export function generateAssigneeData(members) {
  let ddData = members
    .filter(m => {
      return m.isActive && !m.isDeleted;
    })
    .map((member, i) => {
      return generateSelectData(
        member.fullName,
        `${member.fullName} ${member.email}`,
        member.userId,
        null,
        member
      );
    });
  return ddData;
}

//Generating react select dropdown readable object for assignees
export function generateProjectAssigneeData(members) {
  let ddData = members.map((member, i) => {
    return generateSelectData(
      member.fullName,
      `${member.fullName} ${member.email}`,
      member.userId,
      null,
      member
    );
  });
  return ddData;
}

//Generating react select dropdown readable object for tasks
export function generateTaskData(tasks) {
  let ddData = tasks
    ? tasks.map((task, i) => {
      return generateSelectData(task.taskTitle, task.taskTitle, task.taskId, task.uniqueId, task);
    })
    : [];
  return ddData;
}

// Generating react select dropdown readable object for hours
export function generateIntergers() {
  let mins = [];
  for (let min = 1; min <= 9; min++) {
    mins.push(min);
  }
  let ddData = mins.map((m, i) => {
    return generateSelectData(m, m);
  });
  return ddData;
}
// Generating react select dropdown readable object for hours
export function generateTimeHoursData() {
  let mins = [];
  for (let min = 1; min <= 12; min++) {
    mins.push(min < 10 ? "0" + min : min);
  }
  let ddData = mins.map((m, i) => {
    return generateSelectData(m, m);
  });
  return ddData;
}

// Generating react select dropdown readable object for hours
export function generateDuration12HoursData() {
  let mins = [];
  for (let min = 0; min <= 12; min++) {
    mins.push(min);
  }
  let ddData = mins.map((m, i) => {
    return generateSelectData(m, m);
  });
  return ddData;
}
// Generating react select dropdown readable object for hours
export function generateDuration24HoursData() {
  let mins = [];
  for (let min = 0; min <= 23; min++) {
    mins.push(min);
  }
  let ddData = mins.map((m, i) => {
    return generateSelectData(m, m);
  });
  return ddData;
}
// Generating react select dropdown readable object for 100 hours
export function generateDuration100HoursData() {
  let mins = [];
  for (let min = 0; min <= 99; min++) {
    mins.push(min);
  }
  let ddData = mins.map((m, i) => {
    return generateSelectData(m, m);
  });
  return ddData;
}
// Generating react select dropdown readable object for 168 hours
export function generateDuration168HoursData() {
  let mins = [];
  for (let min = 0; min <= 167; min++) {
    mins.push(min);
  }
  let ddData = mins.map((m, i) => {
    return generateSelectData(m, m);
  });
  return ddData;
}

// Generating react select dropdown readable object for time mins
export function generateTimeMinsData() {
  let mins = [];
  for (let min = 0; min <= 59; min++) {
    mins.push(min < 10 ? "0" + min : min);
  }
  let ddData = mins.map((m, i) => {
    return generateSelectData(m, m);
  });
  return ddData;
}
// Generating react select dropdown readable object for time for duration mins
export function generateDurationMinsData(minsArray) {
  let mins = [];
  if (minsArray && minsArray.length) {
    mins = minsArray;
  } else {
    for (let min = 0; min <= 59; min++) {
      mins.push(min);
    }
  }
  let ddData = mins.map((m, i) => {
    return generateSelectData(m, m);
  });
  return ddData;
}

// Generating react select dropdown readable object for minutes
export function generateMinsData() {
  let mins = [];
  for (let min = 0; min <= 60; min++) {
    mins.push(min < 10 ? "0" + min : min);
  }
  let ddData = mins.map((m, i) => {
    return generateSelectData(m, m);
  });
  return ddData;
}
export function generateCompaniesData(companies) {
  let ddData = companies.map((company, i) => {
    return generateSelectData(
      company.companyName,
      company.companyName,
      company.companyId,
      null,
      company
    );
  });
  return ddData;
} //Generating react select dropdown readable object for teams

//Generating data for workspaces
export function generateWorkspaceData() {
  let workspaces = store.getState().profile.data.workspace.filter(w => {
    return w.isActive && w.companyId;
  });
  let ddData = workspaces.map((w, i) => {
    return generateSelectData(w.teamName, w.teamName, w.teamId, null, w);
  });
  return ddData;
}
//Generating data for roles
export function generateRoleData() {
  let roles = [
    { role: "Admin", id: "002" },
    { role: "Member", id: "003" },
    { role: "Limited Member", id: "004" },
  ];
  let ddData = roles.map((r, i) => {
    return generateSelectData(r.role, r.role, r.id, null, r);
  });
  return ddData;
}

export function generateWorkspaceRoleData(workspaceRoles) {
  let roles = workspaceRoles.map(r => {
    return { role: r.roleName, id: r.roleId };
  });
  // let roles = [
  //   { role: "Adminnnn", id: "002" },
  //   { role: "Member", id: "003" },
  //   { role: "Limited Member", id: "004" }
  // ];
  let ddData = roles.map((r, i) => {
    return generateSelectData(r.role, r.role, r.id, null, r);
  });
  return ddData;
}

//Generating stopBy data for repeat task/meeting view
export function generateRepeatStopByData() {
  let ddData = [
    { value: "Date", label: "Date" },
    { value: "After", label: "After" },
  ];
  return ddData;
}

//Generating calculation data
export function generateDMAS(classes) {
  let ddData = [
    { value: 0, label: "Addition", icon: <AddIcon className={classes.icon} /> },
    { value: 1, label: "Subtraction", icon: <RemoveIcon className={classes.icon} /> },
    { value: 2, label: "Multiplication", icon: <Multiply className={classes.icon} /> },
    {
      value: 3,
      label: "Division",
      icon: (
        <SvgIcon className={classes.iconDivide} viewBox="0 0 14 13.393">
          <IconDivide />
        </SvgIcon>
      ),
    },
  ];
  return ddData;
}

export function generateAdvancedOptionData() {
  let ddData = [
    { value: "Reviewed", label: "Submitted for Review" },
    { value: "Published", label: "Published" },
  ];
  return ddData;
}

//Generating month repeat type data for repeat task/meeting view
export function generateMonthRepeatTypeData() {
  let ddData = [
    { value: "Day", label: "Day" },
    { value: "The", label: "The" },
  ];
  return ddData;
}

// Generating react select dropdown readable object for Week Numbers in a year
export function generateWeekNoData() {
  let weeks = ["First", "Second", "Third", "Fourth", "Last"];
  let ddData = weeks.map((w, i) => {
    return generateSelectData(w, w, null, null, weeks);
  });

  return ddData;
}
// Generating react select dropdown readable object for Week Numbers in a year
export function generateWeekDaysData() {
  let weekDays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

  let ddData = weekDays.map((w, i) => {
    return generateSelectData(w, w);
  });
  return ddData;
}
// Generating react select dropdown readable object for Month Numbers
export function generateMonthNoData() {
  let months = [];
  for (let month = 1; month <= 12; month++) {
    months.push(
      month == 1
        ? { number: month, on: "1st" }
        : month == 2
          ? { number: month, on: "2nd" }
          : month == 3
            ? { number: month, on: "3rd" }
            : { number: month, on: `${month}th` }
    );
  }
  let ddData = months.map((m, i) => {
    return generateSelectData(m.on, m.on, null, null, m);
  });

  return ddData;
}
// Generating react select dropdown readable object for Month Numbers
export function generateMonthDaysNoData() {
  let days = [];
  for (let day = 1; day <= 31; day++) {
    if (day > 0) {
      days.push(
        day == 1
          ? { number: day, on: "1st" }
          : day == 2
            ? { number: day, on: "2nd" }
            : day == 3
              ? { number: day, on: "3rd" }
              : { number: day, on: `${day}th` }
      );
    }
  }
  let ddData = days.map((d, i) => {
    return generateSelectData(d.on, d.on, null, null, d);
  });

  return ddData;
}

//Generating data for workspace admin
export function generateWorkspaceAdminRoleData() {
  let roles = [
    { role: "Member", id: "003" },
    { role: "Limited Member", id: "004" },
  ];
  let ddData = roles.map((r, i) => {
    return generateSelectData(r.role, r.role, r.id, null, r);
  });
  return ddData;
}

//Generating data for team level roles
export function generateTeamRoleData() {
  let roles = [
    { role: "Admin", id: "001" },
    { role: "Member", id: "002" },
  ];
  let ddData = roles.map((r, i) => {
    return generateSelectData(r.role, r.role, r.id, null, r);
  });
  return ddData;
}

//Generating data for team admin level roles
export function generateTeamAdminRoleData() {
  let roles = [{ role: "Member", id: "002" }];
  let ddData = roles.map((r, i) => {
    return generateSelectData(r.role, r.role, r.id, null, r);
  });
  return ddData;
}
//Generating data for team admin level roles
export function generateTaskDelayDData() {
  let ddData = [
    { label: "On Track", value: "On Track" },
    { label: "Delayed", value: "Delayed" },
  ];
  return ddData;
}
