

const navHeight = localStorage.getItem('oldSidebarView') == "true" ? 38 : 0; // height of the navbar
const topHeaderHeight = 40 // Height of the topheader
const headerBorder = 1 //height of the border on top of navbar

const headerHeight = topHeaderHeight + navHeight + headerBorder // total value of height of header after adding nav height, header border and top header

export default headerHeight;