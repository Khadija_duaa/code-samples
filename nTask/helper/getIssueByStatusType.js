
function getIssueByStatusType(issues, status) {
  return issues.filter(i => {
    return i.status == status
  })
}

export default getIssueByStatusType