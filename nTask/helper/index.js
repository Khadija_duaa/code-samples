import Moment from "moment";
import validator from "validator";
import { store } from "../index";
import config from "./../config/index";
const userNameReg = /^(?=.{3,50}$)([a-zA-Z0-9]+(?:[_.-]?[a-zA-Z0-9]))*$/;
import { extendMoment } from "moment-range";

const getToken = () => {
  let sessionAccessToken = sessionStorage.token;
  let localAccessToken = localStorage.token;
  if (!sessionAccessToken || sessionAccessToken == undefined) {
    return localAccessToken;
  } else {
    return sessionAccessToken;
  }
};

const formatBytes = (bytes, decimals = 2, type = false) => {
  if (bytes === 0 && type) return "0 Bytes";
  if (bytes === 0 && !type) return 0;

  const k = 1024;
  const dm = decimals < 0 ? 0 : decimals;
  const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];

  const i = Math.floor(Math.log(bytes) / Math.log(k));
  if (type) return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
  else return parseFloat((bytes / Math.pow(k, i)).toFixed(dm));
};

let currentUser = {
  timeZone: {
    value: ``,
    utcValue: ``, // contains value like '-10:00'
  },
};

const moment = extendMoment(Moment);

const RETURN_REMAINING_DAYS_STATUS = (date, taskProgress) => {
  const now = moment().startOf("day");
  const end = moment(date, "MM/DD/YYYY 0:0:0");
  let days = parseInt(end.diff(now, "days"));

  if (taskProgress && parseInt(taskProgress) >= 100) return "Completed";

  if (date) {
    if (days >= 0) {
      if (days === 0) return "Due today";
      else if (days == 1) return "Due tomorrow";
      return days + " days remaining";
    } else {
      days = Math.abs(days); // -24 to 24
      return `${days} day${days > 1 ? "s" : ""} overdue`;
    }
  } else {
    return "-";
  }
};
const RETURN_REMAINING_DAYS_STATUS_NEW = (date, taskProgress, intl) => {
  const now = moment().startOf("day");
  const end = moment(date, "MM/DD/YYYY 0:0:0");
  let days = parseInt(end.diff(now, "days"));

  if (taskProgress && parseInt(taskProgress) >= 100)
    return intl.formatMessage({
      id: "common.grid.remaining-status.completed",
      defaultMessage: "Completed",
    });

  if (date) {
    if (days >= 0) {
      if (days === 0)
        return intl.formatMessage({
          id: "common.grid.remaining-status.duetoday",
          defaultMessage: "Due today",
        });
      else if (days == 1)
        return intl.formatMessage({
          id: "common.grid.remaining-status.duetomorrow",
          defaultMessage: "Due tomorrow",
        });
      return (
        days +
        " " +
        intl.formatMessage({
          id: "common.grid.remaining-status.dayremaining",
          defaultMessage: "days remaining",
        })
      );
    } else {
      days = Math.abs(days); // -24 to 24
      return `${days} ${intl.formatMessage({
        id:
          days > 1
            ? "common.grid.remaining-status.daysoverdue"
            : "common.grid.remaining-status.dayoverdue",
        defaultMessage: `day${days > 1 ? "s" : ""} overdue`,
      })}`;
    }
  } else {
    return "-";
  }
};
const standardTimeFormat = time => {
  let hours = "";
  let minutes = "";
  let timeSplit = time.split(":");
  if (timeSplit[0].length === 1) {
    hours = "0" + timeSplit[0];
  } else {
    hours = timeSplit[0];
  }
  if (timeSplit[1].length === 1) {
    minutes = "0" + timeSplit[1];
  } else {
    minutes = timeSplit[1];
  }
  return hours + ":" + minutes;
};

const RETURN_OVER_DUE_DAYS_WITH_PROGRESS = (endDate, progress) => {
  if (endDate) {
    let state = store.getState();
    let userTimeZone = state.profile.data.profile.timeZone;
    const timeZones = state.constants.data.timeZones || [];

    if (currentUser.timeZone.value !== userTimeZone) {
      currentUser.timeZone.value = userTimeZone;

      // timeZones = array of timezones coming in constansts object from backend

      timeZones.forEach((zone, index) => {
        if (zone.value === userTimeZone)
          // zone.label = '(UTC-10:00) Hawaii'
          currentUser.timeZone.utcValue = /UTC(.*)\)/.exec(zone.label)[1];
      });
    }

    // startOf('day') == setHours(0, 0, 0, 0)
    const today = moment
      .utc()
      .utcOffset(currentUser.timeZone.utcValue)
      .startOf("day");

    const end = moment(new Date(endDate).setHours(0, 0, 0, 0));
    let days = today.diff(end, "days");
    if (progress !== undefined) return progress === 100 || days <= 0 ? 0 : days;
    return days <= 0 ? 0 : days;
  }
  return 0;
};

const RETURN_REMAINING_DAYS_DIFFERENCE = date => {
  const now = moment(new Date().setHours(0, 0, 0, 0)); //todays date
  const end = moment(new Date(date).setHours(0, 0, 0, 0)); // another date
  let days = end.diff(now, "days");
  //

  return days;
};

const RETURN_Day_Name = date => {
  const dayName = moment(new Date(date).setHours(0, 0, 0, 0)).format("dddd"); // another date
  return dayName;
};

const RETURN_MONTH_NAME = date => {
  const monthName = moment(new Date(date).setHours(0, 0, 0, 0)).format("MMM"); // another date
  return monthName;
};

const RETURN_DAY_DATE = date => {
  const dayDate = new Date(date).getDate(); // another date
  return dayDate;
};

const RETURN_REMAINING_DAYS_DIFFERENCE_STATUS = date => {
  const now = moment(new Date().setHours(0, 0, 0, 0)); //todays date
  const end = moment(new Date(date).setHours(0, 0, 0, 0)); // another date
  let days = end.diff(now, "days");
  //
  if (days == 0) {
    return "Due today";
  }
  if (days > 1) {
    return "In " + days + " Days";
  } else if (days == 1) {
    return "In " + days + " Day";
  } else {
    return "Overdue";
  }
};

const RETURN_PROGRESS = (checklist, status) => {
  let doneCheckList = 0;
  let progress = 0;
  if (checklist && checklist.length > 0) {
    doneCheckList = checklist.filter(x => x.isDone).length;
    let checklistProgress = (doneCheckList / checklist.length) * 90;
    let statusValue = 0.0;
    switch (status) {
      case 0:
        break;
      case 1:
        statusValue = 0.25;
        break;
      case 2:
        statusValue = 0.5;
        break;
      case 3:
        statusValue = 1.0;
        break;
      default:
        statusValue = 0.0;
        break;
    }
    progress = checklistProgress + statusValue * 10;
  } else {
    switch (status) {
      case 0:
        break;
      case 1:
        progress = 25;
        break;
      case 2:
        progress = 50;
        break;
      case 3:
        progress = 100;
        break;
    }
  }
  return Math.floor(progress);
};

const ISZAPIERCALL = (email, Zapkey) => {
  //return 2 if came from zapier and credentials are okay
  //return 1 if came from zapier and credentials are not okay
  //return 0 if not came from zapier

  if (
    HELPER_GETPARAMS(window.location.href) &&
    HELPER_GETPARAMS(window.location.href).redirect_uri !== undefined &&
    HELPER_GETPARAMS(window.location.href) &&
    HELPER_GETPARAMS(window.location.href).state !== undefined &&
    HELPER_GETPARAMS(window.location.href) &&
    HELPER_GETPARAMS(window.location.href).response_type !== undefined &&
    HELPER_GETPARAMS(window.location.href) &&
    HELPER_GETPARAMS(window.location.href).client_id !== undefined
  ) {
    if ((email !== null || email !== "") && (Zapkey !== null || Zapkey !== "")) {
      return 2;
    } else {
      return 1;
    }
  } else {
    return 0;
  }
};

const ZAPIERREDIRECT = (token, email, zapkey) => {
  // var client_id = helper.HELPER_GETPARAMS(window.location.href).client_id;
  // var response_type = helper.HELPER_GETPARAMS(window.location.href).response_type;

  var state = HELPER_GETPARAMS(window.location.href).state;
  var redirect_uri = HELPER_GETPARAMS(window.location.href).redirect_uri;
  getToken() ? null : localStorage.setItem("token", `Bearer ${token}`);
  let url = redirect_uri + "?code=" + encodeURIComponent(email + "," + zapkey) + "&state=" + state;
  window.location.href = url;
};

const HELPER_ISALPHA_SPACE = (name, value) => {
  // Spaces Allowed
  let returnStatement = null;
  if (!validator.isAlpha(value.replace(/\s/g, "")))
    returnStatement = `${name} should only contain characters`;
  return returnStatement;
};

const HELPER_ALPHANUMERIC_SPACE = (name, value) => {
  // Spaces Allowed
  let returnStatement = null;
  if (value && !validator.isAlphanumeric(value.replace(/\s/g, ""))) {
    returnStatement = "Oops! " + name + " can only include alphanumeric, -";
  }
  return returnStatement;
};

const HELPER_USERNAME = value => {
  if (userNameReg.test(value)) return true;
  else return false;
};

const HELPER_EMAIL = value => {
  let returnStatement = null;
  if (!validator.isEmail(value)) returnStatement = "Email entered is not a valid email.";
  if (returnStatement === null && validator.contains(value, "+"))
    returnStatement = "Email entered is not a valid email.";
  return returnStatement;
};

const HELPER_EMPTY = (name, value) => {
  let returnStatement = null;
  if (!value || validator.isEmpty(value)) {
    returnStatement = "Please Enter " + name;
  }
  return returnStatement;
};

const RETURN_CECKSPACES = (value, name) => {
  let returnStatement = null;
  if (value && value.length && !value.replace(/\s/g, "").length) {
    returnStatement = name
      ? `${name} should only contain characters`
      : "Please enter value other than spaces";
  }
  return returnStatement;
};

const HELPER_ALPHANUMERIC = (name, value) => {
  let returnStatement = null;
  if (!validator.isAlphanumeric(value)) {
    returnStatement = "Oops! " + name + " can only include alphanumeric, -";
  }
  return returnStatement;
};

const HELPER_VALIDATEPHONENUM = num => {
  var phoneRe = /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g;
  if (phoneRe.test(num)) return null;
  return "Please enter a valid phone number";
};

const HELPER_CHARLIMIT40 = (name, value) => {
  let returnStatement = null;
  if (value && value.toString().trim().length > 40) {
    return "Oops! " + name + " must not exceed 40 characters limit";
  }
  return returnStatement;
};

const HELPER_CHARLIMIT50 = (name, value) => {
  let returnStatement = null;
  if (value && value.toString().trim().length > 50) {
    returnStatement = "Oops! " + name + " must not exceed 50 characters limit";
  }
  return returnStatement;
};

const HELPER_CHARLIMIT80 = (name, value) => {
  let returnStatement = null;
  if (value && value.toString().trim().length > 80) {
    returnStatement = "Oops! " + name + " must not exceed 80 characters limit";
  }
  return returnStatement;
};
const HELPER_CHARLIMIT250 = (name, value) => {
  let returnStatement = null;
  if (value && value.toString().trim().length > 250) {
    returnStatement = "Oops! " + name + " must not exceed 250 characters limit";
  }
  return returnStatement;
};

const HELPER_MINLENGTH_PASSWORD = value => {
  let returnStatement = null;
  if (value && value.toString().trim().length < 8) {
    returnStatement = "Oops! Password must be greater than 8 characters limit";
  }
  return returnStatement;
};

const HELPER_WHITESPACES = value => {
  let returnStatement = null;
  if (/\s/.test(value)) {
    returnStatement = "Oops! White spaces are not allowed.";
  }
  return returnStatement;
};
const HELPER_GETPARAMS = function (url) {
  var params = {};
  var parser = document.createElement("a");
  parser.href = url;
  var query = parser.search.substring(1);
  var vars = query.split("&");
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split("=");
    params[pair[0]] = decodeURIComponent(pair[1]);
  }
  return params;
};
const RETURN_CUSTOMDATEFORMAT = (date, time) => {
  if (date) {
    if (typeof date === "string") date = new Moment(date);
    if (moment(date).isValid()) {
      if (time) return date.format("MM/DD/YYYY") + " " + time;
      else return moment(date).format("MM/DD/YYYY hh:mm A");
    } else {
      return moment().format("MM/DD/YYYY hh:mm A");
    }
  } else return "";
};

const RETURN_MINIMUM_DATE = (array, attrib) => {
  return array.reduce((prev, curr) => {
    return moment(prev[attrib]) < moment(prev[attrib]) ? prev[attrib] : curr[attrib];
  });
};
const RETURN_MAXIMUM_DATE = (array, attrib) => {
  return array.reduce((prev, curr) =>
    moment(prev[attrib]) > moment(prev[attrib]) ? prev[attrib] : curr[attrib]
  );
};
const RETURN_CUSTOMDATEFORMATFORCHECKLIST = date => {
  return moment(date).format("MMM DD, hh:mm a");
};

const RETURN_CUSTOMDATEFORMATFORCREATEDATE = date => {
  if (moment(date).isValid()) return moment(date).format("MMMM DD, YYYY");
  return moment().format("MMMM DD, YYYY");
};

const RETURN_CUSTOMDATEFORMATFORLASTUPDATE = date => {
  return moment(date).format("MMM DD, YYYY hh:mm A");
};

const RETURN_CUSTOMDATEFORMATFORTIME = date => {
  if (moment(date).isValid()) return moment(date).format("hh:mm A");
  // return moment().format("hh:mm A");
  return date;
};
const RETURN_CHATDATEITEM_FORMAT = date => {
  if (moment(date).isValid()) return moment(date).format("MMM DD, YYYY");
  // return moment().format("hh:mm A");
  return date;
};
const RETURN_INITIALS = str => {
  return str ? str.charAt(0).toUpperCase() : "U";
};

const RETURN_DAYS_WITH_ARRAY = selectedDay => {
  let days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
  return selectedDay.map(x => days[x]);
};

const RETURN_DAYS_INDEX = selectedDays => {
  let days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
  return selectedDays.map(x => {
    return days.findIndex(day => day === x);
  });
};
const RETURN_BULKACTIONTYPES = value => {
  if (value === "Unarchive") {
    value = "UnArchive";
  }
  const actions = [
    "Unknown",
    "AddAssignee",
    "AddProject",
    "RemoveProject",
    "StartDate",
    "DueDate",
    "Priority",
    "Status",
    "Color",
    "Copy",
    "CopyToWorkspace",
    "Export",
    "Archive",
    "UnArchive",
    "Delete",
    "AddTask",
    "Severity",
    "AddOwner",
    "Impact",
    "Likelihood",
  ];
  return actions.indexOf(value) >= 0 || actions.map(x => x.toLowerCase()).indexOf(value) >= 0
    ? actions.indexOf(value)
    : 0;
};

const MEETING_BULK_TYPES = value => {
  const actions = ["Unknown", "Color", "Participant", "Status", "Archive", "Delete"];
  return actions.indexOf(value) >= 0 || actions.map(x => x.toLowerCase()).indexOf(value) >= 0
    ? actions.indexOf(value)
    : 0;
};
const PROJECT_BULK_TYPES = value => {
  const actions = [
    "Unknown",
    "AddAssignee",
    "AddProject",
    "RemoveProject",
    "StartDate",
    "DueDate",
    "Priority",
    "Status",
    "Color",
    "Copy",
    "CopyToWorkspace",
    "Export",
    "Archive",
    "Unarchive",
    "Delete",
  ];
  return actions.indexOf(value) >= 0 || actions.map(x => x.toLowerCase()).indexOf(value) >= 0
    ? actions.indexOf(value)
    : 0;
};
const RETURN_ITEMORDER = (data, id) => {
  let position = data.find(x => {
    if (x.itemId === id) {
      return x.position;
    }
  });
  if (position) return position.position;
};
const DOWNLOAD_DOCS = (url, fileName) => {
  var accessToken = sessionStorage.token || localStorage.token;
  if (!accessToken || accessToken == undefined) {
    return;
  }
  let URL = ENV == "production1" || ENV == "beta1" ? BASE_URL_API : BASE_URL;
  return (window.location =
    URL + //https://dev.naxxa.io
    "/api/docsfileuploader/downloaddocsfilesamazons3?url=" +
    url +
    "&fileName=" +
    fileName);
  // return  window.location = routeconfig.docDownloadUrlamazons3url + '?url=' + url + '&fileName=' + fileName;
  // downloadDocs(url, fileName, response=>{
  //   if(response.status===200){
  //
  //   }
  // })
};
const DOWNLOAD_EXCEL = (filename, text) => {
  var pom = document.createElement("a");
  pom.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(text));
  pom.setAttribute("download", filename);

  if (document.createEvent) {
    var event = document.createEvent("MouseEvents");
    event.initEvent("click", true, true);
    pom.dispatchEvent(event);
  } else {
    pom.click();
  }
};
const IMAGE_FORMAT = "IMAGE_FORMAT";
const DOC_FORMAT = "DOC_FORMAT";
const AUDIO_VIDEO_FORMAT = "AUDIO_VIDEO_FORMAT";
const COMPRESS_FORMAT = "COMPRESS_FORMAT";
const getFileIconType = fileExt => {
  let key = fileExt.toUpperCase();
  switch (key) {
    case "DOC":
    case "DOCX":
    case "XLSX":
    case "XLS":
    case "PPT":
    case "CSV":
    case "PDF": {
      return DOC_FORMAT;
    }
    case "PNG":
    case "JPG":
    case "JPEG":
    case "BMP":
    case "GIF": {
      return IMAGE_FORMAT;
    }
    case "PCM":
    case "WAV":
    case "AIFF":
    case "MP3":
    case "ACC":
    case "OGG":
    case "JPEG":
    case "WMA":
    case "FLAC":
    case "ALAC":
    case "WMA":
    case "MP4":
    case "M4A":
    case "M4V":
    case "F4V":
    case "F4A":
    case "M4B":
    case "M4R":
    case "F4B":
    case "MOV":
    case "3GP":
    case "3GP2":
    case "3G2":
    case "3GPP":
    case "3GPP2":
    case "OGG":
    case "OGA":
    case "OGV":
    case "OGX":
    case "WMV":
    case "WMA":
    case "ASF":
    case "WEBM":
    case "FLV":
    case "AVI":
    case "QUICKTIME": {
      return AUDIO_VIDEO_FORMAT;
    }
    case "ZIP":
    case "RAR": {
      return COMPRESS_FORMAT;
    }
    default: {
      return IMAGE_FORMAT;
    }
  }
};
export const getContrastYIQ = hexcolor => {
  if (!hexcolor) return "rgba(51, 51, 51, 1)";
  var r = parseInt(hexcolor.substr(1, 2), 16);
  var g = parseInt(hexcolor.substr(3, 2), 16);
  var b = parseInt(hexcolor.substr(5, 2), 16);
  var yiq = (r * 299 + g * 587 + b * 114) / 1000;
  return yiq >= 190 ? "rgba(51, 51, 51, 1)" : "white";
};
const DOWNLOAD_TEMPLATE = (data, fileName, type, downloadType) => {
  const link = document.createElement("a");
  link.style.display = "none";
  document.body.appendChild(link);

  let objectURL = ``;
  if (downloadType === "attachment")
    objectURL = `${ENV == "production1" || ENV == "beta1" ? BASE_URL_API : BASE_URL
      }api/docsfileuploader/downloaddocsfilesamazons3?url=${data}&fileName=${fileName}`;
  else objectURL = URL.createObjectURL(new Blob([data], { type }));

  link.href = objectURL;
  link.download = fileName;
  link.click();
};
const DOWNLOAD_FOLDER = (data, filename, showSnackBar) => {
  fetch(
    `${ENV == "production1" || ENV == "beta1" ? BASE_URL_API : BASE_URL
    }api/communication/DownloadFolderById?folderId=${data}`,
    {
      mode: "no-cors" /*{mode:'cors'}*/,
    }
  )
    .then(transfer => {
      return transfer.blob();
    })
    .then(bytes => {
      let elm = document.createElement("a");
      elm.href = URL.createObjectURL(bytes);
      elm.setAttribute("download", filename);
      elm.click();
      showSnackBar("Folder has been downloaded successfully");
    })
    .catch(error => { });
};
const CHANGETIMEFORMAT = time => {
  let newTime = "0h 0m";
  if (time) {
    newTime = time.split(":");
    let hour = newTime[0].charAt(0) === "0" ? newTime[0].slice(1, newTime[0].length) : newTime[0];
    let min = newTime[1].charAt(0) === "0" ? newTime[1].slice(1, newTime[1].length) : newTime[1];
    newTime = `${hour}h ${min}m`;
  }
  return newTime;
};
const CHECKDATEIFPRESENTINRANGE = (startDate, endDate, date) => {
  date = new Date(date.setHours(0, 0, 0, 0));
  if (endDate) {
    let range = moment().range(startDate, endDate);
    return range.contains(date); // false
  } else {
    return startDate.toDateString() === date.toDateString();
  }
};

const CALCULATEDURATIONDATETIME = (
  dateTime,
  data,
  isTime,
  durationHours,
  durationMins,
  endByDateString
) => {
  if (data) {
    durationHours = durationHours || data.durationHours ? data.durationHours : 1;
    durationMins = durationMins || data.durationMins ? data.durationMins : 0;
    let startDate = RETURN_CUSTOMDATEFORMAT(moment());
    if (isTime) {
      if (data.startDate) startDate = RETURN_CUSTOMDATEFORMAT(moment(data.startDate), dateTime);
    } else {
      startDate = RETURN_CUSTOMDATEFORMAT(dateTime, data.startTime);
    }
    let endDate = moment(startDate)
      .add(durationHours, "hours")
      .add(0, "seconds")
      .add(durationMins, "minutes");
    endDate = endByDateString
      ? RETURN_CUSTOMDATEFORMAT(moment(endByDateString), moment(endDate).format("hh:mm:ss A"))
      : RETURN_CUSTOMDATEFORMAT(endDate);
    (data.endDateString = endDate),
      (data.endTime = moment(endDate).format("hh:mm:ss A")),
      (data.startDate = startDate),
      (data.startTime = moment(startDate).format("hh:mm A")),
      (data.durationHours = durationHours || data.durationHours ? data.durationHours : 1),
      (data.durationMins = durationMins || data.durationMins ? data.durationMins : 0),
      (data.meetingId = data.meetingId);
    return data;
  } else {
    let { duration, location, meetingLocation, date, time, hours, mins, taskId, projectId, tasks } = dateTime;
    duration = duration.split(":");
    let durationInHours = hours;
    let durationInMinutes = mins;
    date = RETURN_CUSTOMDATEFORMAT(moment(date), time);
    let endDate = moment(date)
      .add(durationInHours, "hours")
      .add(0, "seconds")
      .add(durationInMinutes, "minutes");
    endDate = RETURN_CUSTOMDATEFORMAT(endDate);

    let saveObject = {
      startDate: date,
      startTime: moment(date).format("hh:mm A"),
      durationHours: durationInHours,
      durationMins: durationInMinutes,
      location : meetingLocation,
      meetingDisplayName: dateTime.meetingTitle.trim(),
      endDateString: endDate,
      endTime: moment(endDate).format("hh:mm:ss A"),
      meetingAgendasList: dateTime.agendaArray || [],
      attendeeList: dateTime.participant
        ? dateTime.participant.map(p => {
          return p.id;
        })
        : [],
    };
    if (meetingLocation) saveObject.location = meetingLocation;
    if (taskId) saveObject.taskId = taskId;
    if (projectId) saveObject.projectId = projectId;
    // if (dateTime.participant && dateTime.participant.length) {
    //   saveObject.attendeeList = dateTime.attendeeList || [];
    // }
    return saveObject;
  }
};
const startTime = startDate => {
  let h = startDate.getHours();
  let m = startDate.getMinutes();
  let meridian = h >= 12 ? "PM" : "AM";
  h = h % 12;
  h = h ? h : 12;
  h = h < 10 ? "0" + h : h;
  m = m < 10 ? "0" + m : m;

  return { hours: h, minutes: m, meridian: meridian };
};

const getDuration = (start, end) => {
  let endDate = moment(end);
  let startDate = moment(start);
  let minutes = endDate.diff(startDate, "minutes");
  var h = Math.floor(minutes / 60);
  var m = minutes % 60;
  return {
    hours: h < 10 ? "0" + h : h,
    minutes: m < 10 ? "0" + m : m,
  };
};
const SUMDURATIONS = list => {
  let durations = "";
  if (list.length > 0) {
    list.forEach(stamp => {
      durations = moment.duration(stamp).add(durations);
    });
    const totalTime = `${durations.hours() +
      (durations.days() * 24 + durations.months() * 30 * 24)}:${durations.minutes()}`;

    return totalTime;
  } else return "0:0";
};

const comparer = (a, b, isSorted, sortColumn) => {
  if (isSorted === "ASC") {
    if (typeof a[sortColumn] === "number" || typeof b[sortColumn] === "number") {
      return a[sortColumn] - b[sortColumn];
    }
  }
  if (isSorted === "DESC") {
    if (typeof a[sortColumn] === "number" || typeof b[sortColumn] === "number") {
      return b[sortColumn] - a[sortColumn];
    }
  }
  const first = a[sortColumn] ? a[sortColumn].toString().toLowerCase() : "Select";
  const second = b[sortColumn] ? b[sortColumn].toString().toLowerCase() : "Select";
  let result =
    isSorted === "ASC"
      ? first > second
        ? 1
        : first < second
          ? -1
          : 0
      : first > second
        ? -1
        : first < second
          ? 1
          : 0;
  if (result === 0) {
    result =
      a[sortColumn] && b[sortColumn]
        ? isSorted === "ASC"
          ? a[sortColumn] - b[sortColumn]
          : isSorted === "DESC"
            ? b[sortColumn] - a[sortColumn]
            : 0
        : 0;
  }
  return result;
};
const convertBytes = bytes => {
  /** func that converts the bytes into diff file sizes */
  const sizes = ["Bytes", "KB", "MB", "GB", "TB"];
  if (bytes == 0) {
    return 0;
  }
  const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
  if (i == 0) {
    return bytes + " " + sizes[i];
  }

  return (bytes / Math.pow(1024, i)).toFixed(1) + " " + sizes[i];
};
const hasClass = (element, className) => {
  do {
    if (element.classList && element.classList.contains(className)) {
      return true;
    }
    element = element.parentNode;
  } while (element);
  return false;
};

export default {
  getToken,
  formatBytes,
  HELPER_USERNAME,
  HELPER_EMAIL,
  HELPER_EMPTY,
  HELPER_ISALPHA_SPACE,
  HELPER_ALPHANUMERIC_SPACE,
  HELPER_ALPHANUMERIC,
  HELPER_CHARLIMIT40,
  HELPER_CHARLIMIT50,
  HELPER_CHARLIMIT80,
  HELPER_CHARLIMIT250,
  HELPER_VALIDATEPHONENUM,
  HELPER_MINLENGTH_PASSWORD,
  HELPER_WHITESPACES,
  HELPER_GETPARAMS,
  RETURN_REMAINING_DAYS_STATUS,
  RETURN_REMAINING_DAYS_STATUS_NEW,
  RETURN_PROGRESS,
  RETURN_CUSTOMDATEFORMAT,
  RETURN_CUSTOMDATEFORMATFORCHECKLIST,
  RETURN_CECKSPACES,
  RETURN_BULKACTIONTYPES,
  RETURN_INITIALS,
  RETURN_ITEMORDER,
  DOWNLOAD_DOCS,
  DOWNLOAD_EXCEL,
  DOWNLOAD_TEMPLATE,
  DOWNLOAD_FOLDER,
  getFileIconType,
  IMAGE_FORMAT,
  DOC_FORMAT,
  AUDIO_VIDEO_FORMAT,
  COMPRESS_FORMAT,
  CHANGETIMEFORMAT,
  RETURN_CUSTOMDATEFORMATFORCREATEDATE,
  CHECKDATEIFPRESENTINRANGE,
  MEETING_BULK_TYPES,
  ISZAPIERCALL,
  ZAPIERREDIRECT,
  CALCULATEDURATIONDATETIME,
  RETURN_REMAINING_DAYS_DIFFERENCE,
  RETURN_CUSTOMDATEFORMATFORTIME,
  RETURN_CHATDATEITEM_FORMAT,
  RETURN_REMAINING_DAYS_DIFFERENCE_STATUS,
  RETURN_CUSTOMDATEFORMATFORLASTUPDATE,
  RETURN_Day_Name,
  RETURN_MONTH_NAME,
  RETURN_DAY_DATE,
  RETURN_DAYS_WITH_ARRAY,
  SUMDURATIONS,
  PROJECT_BULK_TYPES,
  comparer,
  RETURN_DAYS_INDEX,
  startTime,
  getDuration,
  RETURN_MINIMUM_DATE,
  RETURN_MAXIMUM_DATE,
  RETURN_OVER_DUE_DAYS_WITH_PROGRESS,
  standardTimeFormat,
  convertBytes,
  hasClass,
};
