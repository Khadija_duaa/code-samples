export const filtersData = [
  {
    label: "View All Issues",
    value: "View All Issues",
    id: 'viewAllIssues',
  },
  {
    label: "Assigned to Me",
    value: "Assigned to Me",
    id: 'assignedToMe',
  },
  {
    label: "Due Today",
    value: "Due Today",
    id: 'dueToday',
  },
  {
    label: "Due in next 5 Days",
    value: "Due in next 5 Days",
    id: 'dueInFiveDays',
  },
  {
    label: "Overdue",
    value: "Overdue",
    id: 'overDueIssues',
  },
  {
    label: "Unscheduled",
    value: "Unscheduled",
    id: 'unscheduledIsses',
  },
  {
    label: "Unassigned Issues",
    value: "Unassigned Issues",
    id: 'unassignedIssues',
  },
];
//Generating data for workspace dropdown in tasks overview header
export const generateWorkspaceData = workspaces => {
  return workspaces.map(w => {
    return { label: w.teamName, value: w.teamName, obj: w };
  });
};
