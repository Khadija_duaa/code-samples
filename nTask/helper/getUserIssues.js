

function GetUserIssues(issues, userId){
    const filteredIssues = issues.filter(issue => {
        return issue.issueAssingnees.indexOf(userId) > -1
    })

    return filteredIssues
}

export default GetUserIssues

