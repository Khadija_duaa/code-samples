function GetUserPendingTask(tasks , userId){
    const filteredTasks = tasks.filter(task => {
        return task.assigneeList.indexOf(userId) > -1 && task.status !== 3 && task.status !== 4
    })

    return filteredTasks
}

export default GetUserPendingTask

