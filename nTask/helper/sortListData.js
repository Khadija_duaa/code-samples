
export const sortListData = (data, sortKey, sortDirection) => {
    const isDateColumn = sortKey.toLowerCase().includes('date');

    const comparer = (a, b) => {
        if (isDateColumn) {
            let str1 = a[sortKey] ? a[sortKey] : '1800-01-01T01:01:01';
            let str2 = b[sortKey] ? b[sortKey] : '1800-01-01T01:01:01';

            if (sortDirection === "ASC") {
                return str1 > str2 ? 1 : -1;
            } else if (sortDirection === "DESC") {
                return str1 < str2 ? 1 : -1;
            }
        }
        else if (typeof a[sortKey] === "string") {
            let str1 = a[sortKey].toLowerCase().trim();
            let str2 = b[sortKey] ? b[sortKey].toLowerCase().trim() : '';
            if (sortDirection === "ASC") {
                return str1 > str2 ? 1 : -1;
            } else if (sortDirection === "DESC") {
                return str1 < str2 ? 1 : -1;
            }
        }
        else if (sortDirection === "ASC") {
            return a[sortKey] > b[sortKey] ? 1 : -1;
        }
        else if (sortDirection === "DESC") {
            return a[sortKey] < b[sortKey] ? 1 : -1;
        }
    };

    let rows = [...data].sort(comparer);
    return rows;
};


export const getSortOrder = (workspaces, loggedInTeam, key) => {  //Getting sort order and direction from global state
    if (loggedInTeam) {
        let activeWorkspace = workspaces.find(w => {
            return w.teamId == loggedInTeam
        })
        if (activeWorkspace) {
            switch (key) {
                case 1:
                    let sortColumn = activeWorkspace.sortColumn.project[0];
                    let sortDirection = activeWorkspace.sortColumn.projectOrder;
                    return { sortColumn, sortDirection }
                    break;
                case 2:
                let taskColumn = activeWorkspace.sortColumn.task[0];
                let taskDirection = activeWorkspace.sortColumn.taskOrder;
                return { taskColumn, taskDirection }
                break;

                case 3:
                let meetingColumn = activeWorkspace.sortColumn.meeting[0];
                let meetingDirection = activeWorkspace.sortColumn.meetingOrder;
                return { meetingColumn, meetingDirection }
                break;

                case 4:
                let issueColumn = activeWorkspace.sortColumn.issue[0];
                let issueDirection = activeWorkspace.sortColumn.issueOrder;
                return { issueColumn, issueDirection }
                break;

                case 5:
                let riskColumn = activeWorkspace.sortColumn.risk[0];
                let riskDirection = activeWorkspace.sortColumn.riskOrder;
                return { riskColumn, riskDirection }
                break;

                default:
                return {}
                    break;
            }
        }
    }
    return {}
}