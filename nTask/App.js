import React, { Component, lazy, Suspense, useState } from "react";
import "./App.css";
import { withStyles } from "@material-ui/core/styles";
import appStyles from "./appStyles";
import { compose } from "redux";
// import CustomRoutes from "./routes/CustomRoutes";
import Button from "@material-ui/core/Button";
import InfoIcon from "@material-ui/icons/Info";
import ErrorIcon from "@material-ui/icons/Error";
import WarningIcon from "@material-ui/icons/Warning";
import CheckedIcon from "@material-ui/icons/CheckCircle";
import CssBaseline from "@material-ui/core/CssBaseline";
import { MuiThemeProvider } from "@material-ui/core/styles";
import { defaultTheme, getTheme } from "./assets/jss/theme";
import { SnackbarProvider } from "notistack";
import { validateToken } from "./redux/actions/authentication";
import { connect } from "react-redux";
import { dispatchTask } from "./redux/actions/tasks";
import CustomRoutes from "./routes/CustomRoutes";
import NoInternet from "./Views/NoInternet/NoInternet";
import { I18nProvider, LOCALES } from "./i18n";
import ToastNotification from "./components/Toast/Toaster";
import helper from "./helper/index";
import mixpanel from "mixpanel-browser";
import { MixPanelEvents } from "./mixpanel";
import loadable from '@loadable/component'
import { merge } from "lodash";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      authenticated: true,
      loginLoaded: true,
    };
    if (!window.ResizeObserver) {
      import("resize-observer-polyfill").then((k) => {
        window.ResizeObserver = k?.default || k;
      });
    }
  }
  componentDidMount() {
    mixpanel.init(MIXPANEL_API_KEY, { debug: true });
    mixpanel.track(MixPanelEvents.AppStarted);
    if (!localStorage.getItem("clearedSession2")) {
      localStorage.clear();
      sessionStorage.clear();
      localStorage.setItem("clearedSession2", true);
    }
    const token = helper.getToken();
    if (token) {
      this.props.validateToken(
        response => { },
        () => { }
      );
    }
  }

  render() {
    const { classes, auth, noInternet, language, whiteLabelTeam } = this.props;
    const { authenticated, loginLoaded } = this.state;
    const token = helper.getToken();
    let localLanguage = "";
    if (language != undefined || language != null) {
      if (language == "English") {
        localLanguage = "en-US";
      } else {
        localLanguage = language;
      }
    } else {
      localLanguage = "en-US";
    }
    return (
      <>
        {" "}
        {loginLoaded ? (
          <I18nProvider locale={localLanguage}>
            {/*<ToastNotification />*/}
            <MuiThemeProvider theme={getTheme(whiteLabelTeam?.customPrimaryColor &&
              { green: whiteLabelTeam?.customPrimaryColor })}>
              <SnackbarProvider
                maxSnack={3}
                iconVariant={{
                  success: <CheckedIcon style={{ marginRight: 5, color: "#4ac076" }} />,
                  error: <ErrorIcon style={{ marginRight: 5, color: "#ed765e" }} />,
                  warning: <WarningIcon style={{ marginRight: 5, color: "#fba848" }} />,
                  info: <InfoIcon style={{ marginRight: 5, color: "rgba(81, 133, 252, 1)" }} />,
                }}
                action={[
                  <Button color="secondary" className={classes.dismissButton} size="small">
                    Close
                  </Button>,
                ]}
                classes={{
                  variantSuccess: classes.success,
                  variantError: classes.error,
                  variantWarning: classes.warning,
                  variantInfo: classes.info,
                }}
              >
                <div id="root">
                  <CssBaseline />
                  {noInternet && <NoInternet />}
                  {auth || !token ? <CustomRoutes authorized={auth && authenticated} /> : null}
                </div>
              </SnackbarProvider>
            </MuiThemeProvider>
          </I18nProvider>
        ) : null
        }
      </>
    );
  }
}
App.defaultProps = {
  language: "en-US",
};
function mapStateToProps(state) {
  return {
    auth: state.authenticationResponse.authState,
    noInternet: state.noInternet,
    language: state.profile.data.profile ? state.profile.data.profile.language : "en-US",
    whiteLabelTeam: state.whiteLabelInfo.data,
  };
}
export default compose(
  withStyles(appStyles, { withTheme: true }),
  connect(mapStateToProps, { validateToken, dispatchTask })
)(App);
