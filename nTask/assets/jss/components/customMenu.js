const customMenuStyles = (theme) => ({
  popper: {
    zIndex: 1301,
    width: 246,
  },
  paperRoot: {
    background: theme.palette.background.default,
  },
  popperxsmall: {
    zIndex: 1301,
    width: 138,
  },
  poppersmall: {
    zIndex: 1301,
    width: 190,
  },
  poppersmall: {
    zIndex: 1301,
    width: 190,
  },
  poppermedium: {
    zIndex: 1301,
    width: 230,
  },
  popperxmedium: {
    zIndex: 1301,
    width: 260,
  },
  popperlarge: {
    zIndex: 1301,
    width: 300,
  },
  popperxlarge: {
    zIndex: 1301,
    width: 400,
  },
  popperxxlarge: {
    zIndex: 1301,
    width: 450,
  },
  popperextandable: {
    zIndex: 1301,
    minWidth: 300,
    width: 'auto',
  },
});

export default customMenuStyles;
