import betaDialogBgImg from "../../images/bg_1.png";
import CelebrationBg from "../../images/celebrationBg.png";

const dialogStyles = (theme) => ({
  dialogTitleWidth: {
    maxWidth: 500
  },
  dialogCnt: {
    // display: "flex",
    // alignItems: "center",
    // justifyContent: "center"
  },
  dialogPaperCnt: {
    overflowY: "visible",
    background: theme.palette.common.white,
    borderRadius: 6,
  },
  signInOptionText: {
    marginTop: 20,
    width: "100%",
    marginBottom: 20,
    fontSize: "12px !important",
    display: "flex",
    "&:after": {
      display: "inline-block",
      content: "''",
      borderTop: `1px solid ${theme.palette.text.light}`,
      flex: 1,
      transform: "translateY(7px)",
    },
    "&:before": {
      display: "inline-block",
      content: "''",
      flex: 1,
      borderTop: `1px solid ${theme.palette.text.light}`,
      transform: "translateY(7px)",
    },
    "& p": {
      width: 150,
      textAlign: "center",
      margin: 0,
      color: theme.palette.text.secondary,
    },
  },
  betaDialogPaperCnt: {
    overflowY: "visible",
    background: theme.palette.common.white,
    width: 450,
  },
  planDialogPaperCnt: {
    overflowY: "visible",
    background: theme.palette.common.white,
    width: 600,
  },
  MoveWorkspaceDialogPaperCnt: {
    overflowY: "visible",
    background: theme.palette.common.white,
    width: 450,
  },
  betaDialogPaperCnt: {
    overflowY: "visible",
    background: theme.palette.common.white,
    width: 520,
  },
  defaultDialogTitle: {
    padding: "15px 20px",
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    userSelect: 'none'
  },
  dialogContentCnt: {
    padding: "20px 20px 0 20px",
  },
  defaultDialogContent: {
    padding: "0",
    overflowY: "visible",
  },
  defaultDialogAction: {
    padding: "0 25px 25px",
  },
  centerAlignDialogContent: {
    padding: "30px 30px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
  },

  importBulkProgressCnt: {
    borderWidth: 2,
    borderColor: theme.palette.border.lightBorder,
    borderStyle: "solid",
    borderRadius: 5,
    padding: 50,
    background: theme.palette.background.paper,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginBottom: 30,
  },

  clProgressValue: {
    position: "absolute",
    right: -10,
    left: 0,
    textAlign: "center",
    top: 35,
    fontSize: "28px !important",
    fontWeight: theme.typography.fontWeightMedium,
  },
  DndDialogContent: {
    padding: "30px 0 0 0",
  },
  deleteConfirmationIconCnt: {
    border: `2px solid ${theme.palette.border.redBorder}`,
    borderRadius: "50%",
    width: 110,
    height: 110,
    paddingLeft: 29,
    paddingTop: 22,
    marginBottom: 30,
    background: theme.palette.background.items,
  },
  deleteConfirmationIcon: {
    fontSize: "68px !important",
  },
  recommendIcon: {
    fontSize: "130px !important",
  },
  archieveConfirmationIconCnt: {
    border: `2px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "50%",
    width: 110,
    height: 110,
    paddingLeft: 31,
    paddingTop: 29,
    marginBottom: 30,
    background: theme.palette.background.paper,
  },
  csvConfirmationIconCnt: {
    border: `2px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "50%",
    width: 110,
    height: 110,
    paddingLeft: 29,
    paddingTop: 29,
    marginBottom: 20,
    background: theme.palette.background.paper,
  },
  browseText: {
    // color: theme.palette.primary.light,
    color: theme.palette.border.blue,
    cursor: "pointer",
  },
  archieveConfirmationIcon: {
    fontSize: "52px !important",
  },
  dialogMessageText: {
    fontSize: "16px !important",
    fontWeight: theme.typography.fontWeightRegular,
    color: theme.palette.text.primary,
    margin: "0 0 20px 0",
    textAlign: "center",
  },
  ratingStarIcon: {
    fontSize: "36px !important",
    marginRight: 5,
  },
  feedbackDialogContentCnt: {
    padding: "20px 20px 30px 20px",
  },
  starsRatingTitle: {
    marginBottom: 10,
  },
  selectLabel: {
    transform: "translate(6px, -23px) scale(1)",
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular,
  },
  selectFormControl: {
    marginTop: 32,
    marginBottom: 20,
    lineHeight: 2,
  },
  dragnDropMessageText: {
    color: theme.palette.text.secondary,
    fontWeight: theme.typography.fontWeightLight,
    fontFamily: theme.typography.fontFamilyLato,
    margin: "10px 0 0 0",
    fontSize: "15px !important",
  },
  dialogFormActionsCnt: {
    paddingBottom: 40,
  },
  dialogActionsCnt: {
    padding: 20,
  },
  dialogMessageTextSecondary: {
    textAlign: "center",
  },
  betaDialogCnt: {
    padding: "30px 30px",
    background: `url(${betaDialogBgImg})`,
    backgroundRepeat: "no-repeat",
    backgroundSize: "contain",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  planDialogCnt: {
    padding: "60px 30px !important",
    background: `url(${CelebrationBg})`,
    backgroundRepeat: "no-repeat",
    borderRadius: 4,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  betaLogo: {
    marginBottom: 20,
    width: 130,
  },
  betaDialogMessage: {
    lineHeight: 1.8,
  },
  sessionDialogCnt: {
    padding: "30px 30px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  sessionDialogPaperCnt: {
    overflowY: "visible",
    background: theme.palette.common.white,
    width: 470,
  },
  profileAvatar: {
    width: 96,
    height: 96,
  },
  avatarWrap: {
    padding: 5,
    border: `1px solid ${theme.palette.border.greenBorder}`,
    borderRadius: "100%",
    margin: "20px 0",
  },
  uploadedImgName: {},
  deleteIconBtn: {
    background: theme.palette.background.dark,
    borderRadius: "100%",
  },
  deleteIcon: {
    fontSize: "14px !important",
  },

  //Public Link Styles

  publicLinkInputCnt: {
    display: "flex",
    marginBottom: 20,
  },

  //Move to workspace dialog styles
  riskIconCnt: {
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "50%",
    width: 90,
    height: 90,
    paddingLeft: 22,
    paddingTop: 20,
    marginBottom: 20,
    marginTop: 20,
    background: theme.palette.background.paper,
  },
  workspaceIconCnt: {
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "50%",
    width: 90,
    height: 90,
    paddingLeft: 23,
    paddingTop: 23,
    marginBottom: 20,
    marginTop: 20,
    background: theme.palette.background.paper,
  },
  moveIconCnt: {
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "50%",
    width: 90,
    height: 90,
    paddingLeft: 23,
    paddingTop: 23,
    marginBottom: 20,
    marginTop: 20,
    background: theme.palette.background.paper,
  },
  tickIconCnt: {
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "50%",
    width: 90,
    height: 90,
    paddingLeft: 23,
    paddingTop: 23,
    marginBottom: 20,
    marginTop: 20,
    background: theme.palette.background.paper,
  },
  tickIcon: {
    fontSize: "42px !important",
  },
  moveIcon: {
    fontSize: "42px !important",
  },
  workspaceIcon: {
    fontSize: "42px !important",
  },
  riskIcon: {
    fontSize: "42px !important",
  },
  boldText: {
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: 500,
  },
  moveWorkspaceMessage: {
    marginBottom: 30,
  },
  permissionCheckbox: {
    marginTop: 20,
  },
  urlAdornment: {
    marginRight: 0,
    width: 345,
  },
  workspaceUrlAdornment: {
    color: theme.palette.text.primary,
    fontSize: "14px !important",
    margin: 0,
  },
  mainHeading: {
    marginBottom: 20,
  },
  passwordVisibilityBtn: {
    padding: 0,
    "&:hover": {
      background: "transparent",
    },
  },
  planDialogSubText: {
    color: theme.palette.text.secondary,
  },
  planDialogHeading: {
    fontSize: "20px !important",
    color: theme.palette.text.azure,
  },
  avatarContainer: {
    margin: "20px 0",
  },
  fullScreenIcon: {
    fontSize: "19px !important",
  },
  subTitle:{
    color: "#7E7E7E",
    fontSize: "13px !important",
    fontWeight: 400,
    marginTop: 5
  },
  arrowBackCnt: {
    background: "#F3F3F3",
    height: "30px",
    width: "31px",
    borderRadius: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    marginRight: "10px",
    alignItems: "center",
    cursor: "pointer",
  },
  arrowBackIcon: {
    fontSize: "12px !important",
    marginLeft: 5,
  },
});

export default dialogStyles;
