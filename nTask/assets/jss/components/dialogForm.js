const dialogFormStyles = theme => ({
  dialogFormActionsCntGray: {
    padding: "20px 20px 20px 20px",
    background: theme.palette.background.items,
    marginTop: 30
  },
  dialogFormActionsCnt: {
    padding: 20
  },
  dialogFormCnt: {
    padding: 20
  },
  selectLabel: {
    transform: "translate(2px, -17px) scale(1)",
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular,

  },
  selectFormControl: {

  },
  workspaceUrlAdornment: {
    color: theme.palette.text.primary,
    fontSize: "14px !important",
    margin: 0
  },
  globeIcon: {
    marginRight: 5
  },
  urlAdornment: {
    marginRight: 6,
    // width: 345
  },
  radioBtnGroupLabel: {
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular,
    marginLeft: 3
  },
  timeInputCnt: {
    width: 210,

  },
  durationInputCnt: {
    width: 140,

  },
  addMeetingDetailList: {
    listStyleType: "none",
    marginBottom: 20,
    padding: 0,
    '& > li': {
      padding: "8px 10px",
      borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
      fontSize: "12px !important",
      color: theme.palette.text.primary,
      '& $CloseIconBtn': {
        marginLeft: 10
      },
      '&:hover $CloseIconBtn': {
        visibility: "visible"
      }
    }
  },
  CloseIconBtn: {
    visibility: "hidden",
  },
  listIcon: {
    fontSize: 8,
    color: theme.palette.secondary.light,
    marginRight: 10
  },
  errorText: {
    marginLeft: 7,
    marginTop: -10,
    color: theme.palette.error.main,
    '& span a': {
      color: theme.palette.primary.light,
      cursor: 'pointer'
    },
  },
})

export default dialogFormStyles;