const autoCompleteStyles = theme => ({
  dropDownAvatar: {
    width: 30,
    height: 30,
    fontSize: 16,
    marginRight: 10
  },
  dropDownAddAvatar: {
    width: 30,
    height: 30,
    fontSize: 28,
    marginRight:10,
    background: theme.palette.primary.main
  },
  selectError: {
      color: theme.palette.error.light
  },
  autoCompleteLabel: {
    transform: "translate(0px, -18px) scale(1)",
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular,
  },
  reactSelectFormControl:{
    marginTop: 22,
    marginBottom: 20
  }
});


export default autoCompleteStyles;
