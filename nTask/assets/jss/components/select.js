const selectStyles = theme => ({
  customRootMenuItem : {
   lineHeight: "0",
   minHeight: '28px',
   fontSize: "12px !important"
  },
  selectCnt: {
    width: 230
  },
  selectCmp: {
    borderRadius: 4,
    padding: "12px 28px 12px 15px",
    color: theme.palette.text.secondary,
    fontSize: "12px !important",
    "&:hover": {
      border: "none"
    },
    "&:focus": {
      background: "transparent"
    }
  },
  singleSelectCmp: {
    borderRadius: 4,
    padding: "12px 52px 12px 12px",
    color: theme.palette.text.secondary,
    fontSize: theme.typography.pxToRem(16),
    "&:hover": {
      border: "none"
    },
    "&:focus": {
      background: "transparent"
    }
  },
  selectCmpSingle: {
    borderRadius: 4,
    padding: "12px 28px 12px 28px",
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    "&:hover": {
      border: "none"
    },
    "&:focus": {
      background: "transparent"
    }
  },
  outlinedSelect: {
    borderRadius: 4,
    background: theme.palette.common.white,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    "&:hover": {
      border: `1px solid ${theme.palette.border.lightBorder}`
    }
  },
  outlinedSelectCnt: {
    fontSize: "12px !important",
    "&:hover $outlinedSelect": {
      border: `1px solid ${theme.palette.border.lightBorder} !important`
    }
  },
  selectIcon: {
    color: theme.palette.secondary.light,
    right: 11,
    color: "rgba(191, 191, 191, 1)",
    fontSize: "31px !important",
    top: "calc(50% - 15px)"
  },
  statusIcon: {
    fontSize: "11px !important"
  },
  statusItemText: {
    padding: "0 16px",
    fontSize: "12px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightRegular
  },
  statusItemTextMeeting: {
    fontSize: "12px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightRegular
  },
  statusMenuItemCnt: {
    padding: "0 0 0 16px",
    marginBottom: 2
  },
  rootUnassigned: {
    minHeight: "0px",
    fontSize: "12px !important"
  },
  customRootArchived: {
    minHeight: "46px",
    background: `${theme.palette.background.items}`,
    lineHeight: "0",
    fontSize: "12px !important",
    color: `${theme.palette.text.primary}`
  },
  plainMenuItemCnt: {},
  statusMenuItemSelected: {
    lineHeight: "0",
    minHeight: '28px',
    fontSize: "12px !important", 
    background: `${theme.palette.common.white} !important`,
    "&:hover": {
      background: `${theme.palette.background.items}`
    },
    "&:focus": {
      background: `${theme.palette.background.items} !important`
    }
  },
  selectInputLabelFixed: {
    transform: "translate(15px, 15px) scale(1)",
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular,
    zIndex: 1
  },
  menuHeadingItem: {
    lineHeight: "0",
    minHeight: '28px',
    padding: "0 16px",
    fontSize: "12px !important",
    marginTop: 10,
    fontWeight: theme.typography.fontWeightLight,
    textTransform: "capitalize",
    color: theme.palette.secondary.light,
    cursor: "default",
    pointerEvents: "none",
    "&:hover": {
      backgroundColor: "transparent",
      cursor: "unset"
    },
    "&:focus": {
      backgroundColor: "transparent",
      cursor: "unset"
    }
  },
  highlightItem: {
    lineHeight: "0",
    minHeight: '28px',
    background: theme.palette.background.items,
    padding: 10,
    fontSize: "12px !important",
    color: theme.palette.text.secondary,
    fontWeight: theme.typography.fontWeightRegular,
    borderTop: `1px solid ${theme.palette.border.lightBorder}`,
    "&:hover": {
      background: `${theme.palette.background.items} !important`
    },
    "&:focus": {
      background: `${theme.palette.background.items} !important`
    }
  },
  selectHighlightItemIconForProject: {
    fontSize: "1.5rem !important"
  },
  selectHighlightItemIcon: {
    // marginRight: 10,
    fontSize: "1.5rem !important",
  },
  selectHighlightItemIconArchive: {
    // marginRight: 10,
    fontSize: "1.5rem !important",
    marginRight: "10px"
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  },
  selectFormControl: {
    width: "100%"
  },
  defaultInputLabel: {
    transform: "translate(2px, -17px) scale(1);",
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular
  },
  ListItemAvatar: {
    height: 32,
    width: 32
  },
  MenuItem: {
    padding: "11px 20px"
  },
  AvatarMenuItem: {
    padding: "11px 20px",
    marginBottom: 2
  },
  MenuItemText: {
    fontSize: "12px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightLight
  },
  plainItemText: {
    fontSize: "12px !important",
    color: theme.palette.text.secondary,
    fontWeight: theme.typography.fontWeightLight
  },
  searchMenuList: {
    outline: "none"
  },
  flagIcon: {
    fontSize: "24px !important"
  },
  selectedValue: {
    //Adds left border to selected list item
    "&:before": {
      content: "''",
      position: "absolute",
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      width: 5,
      background: theme.palette.secondary.main
    }
  },
  priorityIcon: {
    fontSize: "16px !important"
  },
  bugIcon: {
    fontSize: "16px !important"
  },
  ratingStarIcon: {
    fontSize: "16px !important"
  },
  ImprovmentIcon: {
    fontSize: "16px !important"
  }
});

export default selectStyles;
