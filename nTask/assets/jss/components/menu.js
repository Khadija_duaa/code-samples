const menuStyles = theme => ({
  keyShortcut: {
    fontSize: "10px !important",
    width: 16,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    background: theme.palette.common.white,
    borderRadius: 5,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  menuItemStyle: {
    justifyContent: "space-between",
    display: "flex",
    width: "100%",
    "& > div": { display: "flex", alignItems: "center" },
  },
  paperRoot: {
    background: theme.palette.common.white,
    borderRadius: 4,
    boxShadow: "-1px 1px 5px -2px rgba(0,0,0,0.32);",
    fontFamily: theme.typography.fontFamilyLato,
  },
  popper: {
    zIndex: 11,
    width: 250,
    '&[x-placement*="bottom-start"] $arrow': {
      top: 0,
      left: 18,
      marginTop: -14,
      width: "3em",
      height: "1em",
      "&::before": {
        borderWidth: "0 1em 14px 1em",
        borderColor: `transparent transparent ${theme.palette.background.items} transparent`,
      },
    },
  },
  plainPopper: {
    zIndex: 9999,
    width: 220,
    '&[x-placement="bottom"] $arrow': {
      top: 0,
      right: 0,
      left: 0,
      marginTop: -14,
      width: 0,
      height: "1em",
      "&::before": {
        borderWidth: "0 1em 14px 1em",
        borderColor: `transparent transparent ${theme.palette.common.white} transparent`,
      },
    },
    '&[x-placement*="bottom-end"] $arrow': {
      top: 0,
      right: 36,
      marginTop: -14,
      width: 0,
      height: "1em",
      "&::before": {
        borderWidth: "0 1em 14px 1em",
        borderColor: `transparent transparent ${theme.palette.common.white} transparent`,
      },
    },
  },
  plainPopperDashboard: {
    extend: "plainPopper",
    width: 280,
    zIndex: 99999,
  },
  arrow: {
    position: "absolute",
    width: "3em",
    color: theme.palette.common.white,
    height: "3em",
    margin: "auto",
    display: "block",
    "&::before": {
      content: '""',
      margin: "auto",
      display: "block",
      width: 0,
      height: 0,
      borderStyle: "solid",
      fontSize: "10px !important",
    },
  },
  plainMenuItem: {
    padding: "6px 20px",
    fontSize: "12px !important",
    lineHeight: "normal",
    fontWeight: theme.typography.fontWeightRegular,
    color: theme.palette.secondary.medDark,
    "&:hover $plainMenuItemIcon": {
      color: theme.palette.primary.light,
      transition: "ease color 0.8s",
    },
  },
  subMenuCnt: {
    background: theme.palette.common.white,
    width: 200,
    position: "absolute",
    left: -201,
    zIndex: 111,
    boxShadow: "-1px 1px 5px -2px rgba(0,0,0,0.32)",
    borderRadius: 4,
  },
  subMenuItem: {
    padding: "6px 20px",
    overflow: "visible",
    fontSize: "12px !important",
    lineHeight: "normal",
    fontWeight: theme.typography.fontWeightRegular,
    color: theme.palette.secondary.medDark,
    "&:hover": {
      background: theme.palette.background.items,
      color: theme.palette.primary.light,
    },
    "&:hover $plainMenuItemIcon": {
      color: theme.palette.primary.light,
      transition: "ease color 0.8s",
    },
  },
  workspaceMenuList: {
    paddingBottom: 0,
  },
  plainMenuItemIcon: {
    marginRight: 10,
    fontSize: "18px !important",
  },
  menuItemIcon: {
    marginRight: 20,
    fontSize: "30px !important",
  },
  highlightItem: {
    background: theme.palette.background.items,
    fontSize: "12px !important",
    color: theme.palette.text.primary,
    padding: 12,
    "&:hover": {
      background: theme.palette.background.items,
    },
  },
  highlightItemTeamWork: {
    // background: theme.palette.background.items,
    fontSize: "12px !important",
    color: theme.palette.text.primary,
    padding: 12,
    "&:hover": {
      background: theme.palette.background.items,
    },
  },
  menuHeadingWorkName: {
    // background: theme.palette.background.items,
    fontSize: "12px !important",
    color: theme.palette.secondary.light,
    opacity: 1,
    padding: "10px 20px 0 20px",
    "& $wsMenuHeadingText": {
      maxWidth: 480,
      minWidth: 230,
      overflow: "hidden",
      color: theme.palette.text.primary,
      whiteSpace: "nowrap",
      textOverflow: "ellipsis",
    },
    "&:hover": {
      background: theme.palette.background.items,
    },
  },
  wsMenuHeadingText: {},
  menuButtonAvatar: {
    marginRight: 10,
  },

  itemTextRootFollowUp:{
    padding: "0 16px",
  // },
  // itemText: {
    // width: 134,
    overflow: "hidden",
    whiteSpace: "nowrap",
    lineHeight: "normal",
    textOverflow: "ellipsis"
  },
  
  menuHeading: {
    padding: "5px 20px 0 20px",
    // borderTop: `1px solid ${theme.palette.border.lightBorder}`,
    fontSize: "0.75em !important",
    fontWeight: theme.typography.fontWeightRegular,
    textTransform: "capitalize",
    color: theme.palette.secondary.light,
    "&:hover": {
      backgroundColor: "transparent",
      cursor: "unset",
    },
  },
  menuWorkspaceAvatar: {
    background: theme.palette.primary.main,
    marginRight: 10,
    width: 30,
    height: 30,
  },
  menuWorkspaceItem: {
    padding: 10,
    fontSize: "12px !important",
    justifyContent: "space-between",
    "&:hover": {
      transition: "ease background 1s",
    },
  },
  menuItemInner: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  menuButtonText: {
    // marginRight: 10,
    whiteSpace: "nowrap",
    lineHeight: "normal",
    textOverflow: "ellipsis",
    maxWidth: 150,
    overflow: "hidden",
    fontSize: "14px !important",
  },
  SearchInputMenuItem: {
    padding: "10px 16px",
    height: "auto",
    flexDirection: "column",
    alignItems: "stretch",
    "&:hover": {
      background: "none",
      cursor: "unset",
    },
  },
  menuHeadingItem: {
    padding: "0 20px",
    "&:hover": {
      backgroundColor: "transparent",
      cursor: "unset",
    },
  },
  menuHeadingItemText: {
    fontSize: "12px !important",
    marginTop: 5,
    fontWeight: theme.typography.fontWeightRegular,
    textTransform: "capitalize",
    color: theme.palette.secondary.light,
    fontFamily: theme.typography.fontFamilyLato,
  },
  menuHeadingListItemText: {
    fontSize: "12px !important",
    marginTop: 5,
    fontWeight: theme.typography.fontWeightRegular,
    textTransform: "capitalize",
    color: theme.palette.secondary.light,
  },
  ListItemAvatar: {
    height: 32,
    width: 32,
  },
  MenuItem: {
    padding: "8px 16px 8px 16px",
    marginBottom: 2,
  },
  AvatarMenuItem: {
    padding: "8px 16px 8px 16px",
    marginBottom: 2,
  },
  MenuItemText: {
    fontSize: "12px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightLight,
  },
  RemoveMenuItem: {
    padding: "11px 20px",
  },
  RemoveMenuItemText: {
    fontSize: "12px !important",
    color: theme.palette.error.light,
    fontWeight: theme.typography.fontWeightRegular,
  },
  MenuItemAvatar: {
    width: 36,
    height: 36,
    marginRight: 10,
  },
  listItemAvatar: {
    paddingTop: 5,
    paddingBottom: 5,
  },
  selectIcon: {
    color: theme.palette.secondary.light,
    right: 11,
    color: "rgba(191, 191, 191, 1)",
    fontSize: "31px !important",
    top: "calc(50% - 15px)",
  },
  statusIcon: {
    fontSize: "11px !important",
  },
  flagIcon: {
    fontSize: "24px !important",
  },
  statusItemText: {
    fontSize: "12px !important",
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightRegular,
    overflow: "hidden",
    width: 150,
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
  },
  statusItemTextDanger: {
    fontSize: "12px !important",
    color: theme.palette.text.danger,
    fontWeight: theme.typography.fontWeightRegular,
    overflow: "hidden",
    width: 150,
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
  },
  statusMenuItemCnt: {
    padding: "2px 0 2px 16px",
    fontSize: "12px !important",
    height: 20,
  },
  statusMenuItemSelected: {
    background: `transparent !important`,
    boxShadow: "none",
    "&:before": {
      content: "''",
      position: "absolute",
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      width: 5,
      background: theme.palette.secondary.main,
    },
    "&:hover": {
      background: `transparent !important`,
    },
  },

  statusMenuItemSelectedMUI4: {
    boxShadow:"none",
    background: `transparent !important`,
    "&:before": {
      content: "''",
      position: "absolute",
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      width: 5,
      background: theme.palette.secondary.main,
    },
    "&:hover": {
      background: `transparent !important`,
    },
  },
  colorPickerCnt: {
    position: "absolute",
    background: "white",
    right: -210,
    padding: 0,
    borderRadius: 7,
    boxShadow: "0px 0px 5px -1px rgba(0,0,0,0.07)",
  },
  colorPickerCntLeft: {
    position: "absolute",
    background: "white",
    left: -212,
    padding: 0,
    borderRadius: 7,
    boxShadow: "0px 0px 5px -1px rgba(0,0,0,0.07)",
  },
  submenuArrowBtn: {
    transform: "translateX(10px)",
  },
  subNavMainMenuCnt: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    flex: 1,
  },
  selectedValue: {
    //Adds left border to selected list item
    "&:before": {
      content: "''",
      position: "absolute",
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      width: 5,
      background: theme.palette.secondary.main,
    },
  },
  addBtnIcon: {
    marginRight: 5,
    fontSize: "18px !important",
  },
  TotalAssignee: {
    width: 40,
    height: 40,
    fontSize: "16px !important",
  },
  columnOrderButton: {
    position: "absolute",
    right: 20,
    zIndex: 1,
  },
  slackSelectedChannel: {
    maxWidth: 150,
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
  },
  menuHeadingBackItem: {
    padding: "0px 10px 8px 10px",
    cursor: "pointer",
    borderBottom: "1px solid #EAEAEA",
  },
  menuHeadingItemBackText: {
    fontSize: "12px !important",
    marginTop: 5,
    fontWeight: 600,
    textTransform: "capitalize",
    color: "#333333",
    fontFamily: theme.typography.fontFamilyLato,
  },
  menuHeadingItemBackTextRoot: {
    padding: "0px 7px",
  },
  labelImportExport: {
    paddingRight: 4,
    fontSize: "12px !important",
    color: "#7e7e7e",
    fontWeight: 500,
  },
});

export default menuStyles;
