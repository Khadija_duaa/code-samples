const switchStyles = theme => ({

  rootSmall: {
    width: 22,
  },
  rootSwitchMedium: {
    width: 43,
    height: 25,
    padding: 0,
    overflow: 'visible',
  },
  rootSwitch: {
    width: 30,
    height: 18,
    padding: 0,
    overflow: 'visible',
  },
  iOSSwitchBase: {
    top: 2,
    left: 2,
    padding: '0 !important',
    "&:hover": {
      backgroundColor: "transparent"
    },
    '&$iOSChecked': {
      transform: 'translateX(12px) !important',
      color: theme.palette.common.white,
      "&:hover": {
        backgroundColor: "transparent"
      },
      '& + $iOSBar': {
        opacity: 1
        // backgroundColor: theme.palette.primary.light,
      },
      '& + $iOSBarMedium': {
        opacity: 1,
        // backgroundColor: theme.palette.primary.light,
      },
      '& + $iOSBarSmallDark': {
        background: theme.palette.background.btnBlue,
      },
    },
  },
  iOSSwitchBaseSmall: {
    "&:hover": {
      backgroundColor: "transparent"
    }
  },
  iOSChecked: {
    transform: 'translateX(12px)',
    '& + $iOSBar': {
      opacity: 1,
      border: 'none',
    },
    '& + $iOSBarMedium': {
      opacity: 1,
      border: 'none',
    },
    '& + $iOSBarDark': {
      opacity: 1,
      border: 'none',
    },
    "&:hover": {
      backgroundColor: "transparent"
    }
  },

  iOSSwitchBaseMedium: {
    top: '5.5px',
    left: '5.5px',
    padding: '0 !important',
    "&:hover": {
      backgroundColor: "transparent"
    },
    '&$iOSChecked': {
      transform: 'translateX(17px)',
      color: theme.palette.common.white,
      "&:hover": {
        backgroundColor: "transparent"
      },
      '& + $iOSBar': {
        opacity: 1
        // backgroundColor: theme.palette.primary.light,
      },
      '& + $iOSBarMedium': {
        opacity: 1,
        // backgroundColor: theme.palette.primary.light,
      },
      '& + $iOSBarSmallDark': {
        background: theme.palette.background.btnBlue,
      },
    },
  },

  iOSCheckedSmall: {
    transform: 'translateX(12px)',
    "&:hover": {
      backgroundColor: "transparent"
    },
  },
  iOSBar: {
    borderRadius: 15,
    zIndex: -1,
    width: '100%',
    height: '100%',
    border: 'none',
    backgroundColor: theme.palette.background.light,
    opacity: 1,
    transition: theme.transitions.create(['background-color', 'border']),
    "&:hover": {
      backgroundColor: "transparent"
    }
  },
  iOSBarMedium: {
    borderRadius: 15,
    zIndex: -1,
    width: '100%',
    height: '100%',
    // marginTop: -12,
    // marginLeft: -21,
    border: 'none',
    backgroundColor: theme.palette.background.light,
    opacity: 1,
    transition: theme.transitions.create(['background-color', 'border']),
    "&:hover": {
      backgroundColor: "transparent"
    }
  },
  iOSBarSmall: {
    height: 18,
    top: '68%',
    left: '94%',
    width: 30
  },
  secondaryHover: {
    "&:hover": {
      backgroundColor: "transparent"
    }
  },
  iOSBarSmallDark: {
    height: 18,
    top: '68%',
    left: '94%',
    width: 30,
    background: theme.palette.background.btnBlue,
    opacity: 1
  },
  iOSBarDark: {
    borderRadius: 13,
    width: 42,
    height: 24,
    marginTop: -12,
    marginLeft: -21,
    border: 'none',
    backgroundColor: theme.palette.background.btnBlue,
    opacity: 1,
    transition: theme.transitions.create(['background-color', 'border']),
  },
  iOSIcon: {
    width: '14px !important',
    height: '14px !important',
    borderRadius: '50%',
    background: theme.palette.common.white
  },
  iOSIconChecked: {
    // boxShadow: theme.shadows[1],
  },
});

export default switchStyles;