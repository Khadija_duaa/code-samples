const sidebarWidth = 185;

const sidebarStyles = theme => ({
  menuButton: {
    marginLeft: 12,
    marginRight: 36
  },
  hide: {
    display: 'none'
  },
  newSidebarSwitchCnt: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '0 20px 0 10px',
    width: '100%',
    bottom: '90px',
    boxShadow: '0px 1px 0px #1d3544',
    '& p': {
      color: theme.palette.common.white,
      fontSize: "12px !important",
      fontWeight: theme.typography.fontWeightExtraLight
    }
  },
  betaTag: {
    padding: '2px 3px',
    borderRadius: 4,
    background: '#00ABED',
    color: theme.palette.common.white,
    fontSize: "10px !important",
    display: 'inline-block',
    marginLeft: 4
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: sidebarWidth,
    '& $toolbarOuterCnt': {
      background: "#292929",
      width: 184,
      height: 50,
      padding: '10px 50px 10px 10px',
      borderBottom: '1px solid rgba(0, 0, 0, 0.12)',
      // transition: theme
      //   .transitions
      //   .create('width', {
      //     easing: theme.transitions.easing.sharp,
      //     duration: theme.transitions.duration.leavingScreen
      //   }),
    },
    // transition: theme
    //   .transitions
    //   .create('width', {
    //     easing: theme.transitions.easing.sharp,
    //     duration: theme.transitions.duration.enteringScreen
    //   })
  },
  toolbarOuterCntWhite: {
    background: "#292929",
    width: 184,
    height: 50,
    paddingTop: '3px',
    paddingLeft: '5px',
    borderBottom: '1px solid rgba(0, 0, 0, 0.12)',
    // transition: theme
    //   .transitions
    //   .create('width', {
    //     easing: theme.transitions.easing.sharp,
    //     duration: theme.transitions.duration.leavingScreen
    //   }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    position: "fixed",
    // transition: theme
    //   .transitions
    //   .create('width', {
    //     easing: theme.transitions.easing.sharp,
    //     duration: theme.transitions.duration.leavingScreen
    //   }),
    width: theme.spacing.unit * 7,
    [
      theme
        .breakpoints
        .up('sm')
    ]: {
      width: theme.spacing.unit * 9
    },
    '& $toolbarOuterCnt': {
      width: 72,
      height: 50,
      // transition: theme
      //   .transitions
      //   .create('width', {
      //     easing: theme.transitions.easing.sharp,
      //     duration: theme.transitions.duration.leavingScreen
      //   }),
      overflow: "hidden"
    }
  },
  toolbarOuterCnt: {},
  toolbar: {
    padding: 24
  },
  sideBarLoad: {
    width: 20,
    height: 20,
    border: `2px solid rgba(243, 243, 243, 1)`,
    borderTop: `2px solid #00CC90`
  },
  sideBarLogoCntOpen: {
    visibility: 'visible',
    position: 'absolute',
    left: 0,
    top: 0,
    padding: '8px 20px'
  },
  sideBarLogoCntClose: {
    position: 'absolute',
    left: 0,
    top: 0,
    visibility: 'hidden',
    padding: 16
  },
  sideBarMenuIcon: {
    position: 'absolute',
    right: 0,
    top: 0,
    padding: '23px 24px',
    borderRadius: 0,
    color: localStorage.getItem("darkMode") == "true" ? theme.palette.secondary.light : null
  },
  itemRoot: {
    padding: "11px 16px 11px 26px",
    '&:hover': {
      backgroundColor: 'transparent',
      '& $listItemText, $listItemIcon': {
        color: theme.palette.text.primary,
        transition: 'ease-in-out color 0.5s'
      }
    },
  },
  itemSelected: {
    '& $listItemText, $listItemIcon': {
      color: theme.palette.primary.light
    },
    '& $listItemText, $listItemIconDark': {
      color: theme.palette.primary.light
    },
  },
  listItemText: {},
  listItemIcon: {
    fontSize: "20px !important",
    color: theme.palette.secondary.medDark
  },
  sideBarIcon: {
    fontSize: "20px !important",
  },
  listItemIconDark: {
    color: theme.palette.secondary.light
  },

  sidebarListItem: {
    padding: "5px 10px",
    display: "flex",
    justifyContent: "space-between",
  },
  sidebarFilterListItem: {
    padding: "5px 10px 5px 25px",
    display: "flex",
    justifyContent: "space-between",
  },
  savedFilterListCnt: {
    marginTop: 10
  },
  favProjectsHeading: {
    marginLeft: 10
  },
  favProjectListItemText: {
    fontSize: "12px !important",
    marginLeft: 10,
    color: theme.palette.common.white,
    width: 100,
    overflow: "hidden",
    whiteSpace: "nowrap",
    lineHeight: "normal",
    textOverflow: "ellipsis"
  },
  favProjectListItemTextDark: {
    fontSize: "12px !important",
    marginLeft: 10,
    color: theme.palette.common.black,
    width: 100,
    overflow: "hidden",
    whiteSpace: "nowrap",
    lineHeight: "normal",
    textOverflow: "ellipsis"
  },
  savedFilterListItemText: {
    fontSize: "12px !important",
    color: theme.palette.common.white,
    width: 100,
    overflow: "hidden",
    whiteSpace: "nowrap",
    lineHeight: "normal",
    textOverflow: "ellipsis"
  },
  projectColorIcon: {
    fontSize: "8px !important"
  },
  projectColorIconDark: {
    fontSize: "8px !important",
    color: theme.palette.common.black,
  },
  pushPinIcon: {
    fontSize: "14px !important"
  },
  allMembersCnt: {
    marginTop: 20,
  },
  allMembersHeading: {
    marginLeft: 10
  },
  viewAllHeading: {
    marginRight: 10,
    color: theme.palette.common.white,
    cursor: "pointer"
  },
  viewAllHeadingDark: {
    marginRight: 10,
    color: theme.palette.common.black,
    cursor: "pointer"
  },
  inviteMemberBtnCnt: {
    padding: "0 10px",
    margin: "20px 0 30px 0",
    display: 'none'
  },
  membersName: {
    fontSize: "12px !important",
    color: theme.palette.common.white,
    width: 140,
    margin: 0,
    overflow: "hidden",
    fontSize: "12px !important",
    whiteSpace: "nowrap",
    lineHeight: "normal",
    textOverflow: "ellipsis"
  },
  membersNameDark: {
    fontSize: "12px !important",
    color: theme.palette.common.black,
    width: 140,
    margin: 0,
    overflow: "hidden",
    fontSize: "12px !important",
    whiteSpace: "nowrap",
    lineHeight: "normal",
    textOverflow: "ellipsis"
  },
  teamMembersName: {
    fontSize: "12px !important",
    color: theme.palette.common.white,
    width: 100,
    overflow: "hidden",
    fontSize: "12px !important",
    whiteSpace: "nowrap",
    lineHeight: "normal",
    textOverflow: "ellipsis"
  },
  teamMembersNameDark: {
    fontSize: "12px !important",
    color: theme.palette.common.black,
    width: 100,
    overflow: "hidden",
    fontSize: "12px !important",
    whiteSpace: "nowrap",
    lineHeight: "normal",
    textOverflow: "ellipsis"
  },
  selectedFilter: {
    background: `${theme.palette.background.black} !important`
  },
  selectedValueIcon: { //Adds left border to selected list item
    fontSize: "10px !important",
    position: "absolute",
    left: 10
  },
  userDocsDetails: {
    whiteSpace: "initial",
    lineHeight: "normal",
    marginTop: 5
  },
  selectedMember: {
    //Adds left border to selected list item
    background: theme.palette.background.darkBlack,
    "&:before": {
      content: "''",
      position: "absolute",
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      width: 5,
      background: theme.palette.secondary.main
    },
    "&:hover": {
      background: `${theme.palette.background.darkBlack} !important`
    },
    "&:focus": {
      background: `${theme.palette.background.darkBlack} !important`
    },
  },
  menuItemSelected: {
    background: `${theme.palette.background.darkBlack} !important`,
    "&:hover": {
      background: `${theme.palette.background.darkBlack} !important`
    },
    "&:focus": {
      background: `${theme.palette.background.darkBlack} !important`
    }
  },
  memberListItem: {
    margin: 0,
    padding: 0
  },
  memberListItemInner: {
    padding: "7px 0",
    margin: "0 10px",
    borderBottom: `1px solid ${theme.palette.common.black}`
  },
  panel: {
    position: 'absolute',
    width: '100%',
  },
  panelHeader: {
    alignItems: "center",
    justifyContent: "center",
    display: "flex",
    backgroundColor: theme.palette.background.lightBlack,
    borderBottom: `1px solid ${theme.palette.common.black}`,
    cursor: "pointer"
  },
  arrowIcon: {
    fontSize: "26px !important"
  },
  panelBody: {
    display: 'flex',
    flexDirection: 'column',
    padding: '12px 10px 12px 10px',
    color: theme.palette.common.white,
    backgroundColor: theme.palette.background.lightBlack,
    fontSize: "12px !important",
    whiteSpace: 'normal'
  },
  panelTitle: {
    fontSize: "12px !important",
    color: theme.palette.text.light
  },
  planCnt: {
    display: 'flex',
    alignItems: 'center',
    marginTop: 2,
    marginBottom: 10,
    justifyContent: 'space-between',
    '& span': {
      lineHeight: 'normal'
    }
  },
  planDate: {
    fontSize: "11px !important",
    color: theme.palette.text.yellow,
  },
  recommend: {
    color: theme.palette.common.white,
    fontSize: "12px !important",
    display: 'flex',
    alignItems: 'center',
    padding: '0 19px',
    backgroundColor: theme.palette.background.lightBlack,
    borderTop: `1px solid ${theme.palette.common.black}`,
    justifyContent:'center',
    cursor: 'pointer'
  },
  displayNone:{
    display:'none'
  },
  open:{
    display: 'flex',
    justifyContent: 'space-between',
    width: '100%'
  }

});

export default sidebarStyles;