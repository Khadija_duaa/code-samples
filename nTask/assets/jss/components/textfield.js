const textFieldStyles = (theme) => ({
  plainTextField: {
    width: 300,
  },
  plainInputLabel: {
    fontSize: "15px !important",
  },
  plainInput: {
    padding: "7px 0",
  },
  inputAdormentUrl: {
    fontSize: "18px !important",
    background: theme.palette.common.white,
    color: theme.palette.secondary.black,
    lineHeight: "36px",
    zIndex: 1,
    margin: 0,
  },
  formFieldCnt: {
    marginBottom: 30,
  },
  formControl: {
    width: "100%",
  },
  autoCompleteLabel: {
    top: -13,
    fontSize: "15px !important",
  },
  shrinkLabel: {
    color: theme.palette.primary.light,
  },
  WideInputUnderline: {
    "&:hover::before": {
      borderBottom: "none",
    },
    "&::before": {
      borderBottom: `1px solid ${theme.palette.border.lightBorder}`,
    },
  },
  outlinedInputCnt: {
    borderRadius: 4,
    cursor: 'text',
    background: theme.palette.common.white,
    "&:hover $notchedOutlineCnt": {
      border: `1px solid ${theme.palette.border.lightBorder} !important`,
    },
  },
  outlinedInputDisabled: {
    background: theme.palette.background.items,
    cursor: "auto",
  },
  outlinedInputMultiline: {
    padding: 0,
    "& $notchedOutlineCnt": {
      borderRadius: 7,
    },
  },
  outlinedInputMultilineInput: {
    borderRadius: 7,
  },
 
  notchedOutlineCnt: {
    borderRadius: 4,
    border: `1px solid ${theme.palette.border.lightBorder} !important`,
  },
  notchedOutlineCntNB: {
    borderRadius: 0,
    boxShadow: "0px 3px 5px -1px rgba(0,0,0,0.06)",
    border: "none",
  },
  notchedOutlineCntBB: {
    borderRadius: 0,
    border: "none",
    borderBottom: `1px solid ${theme.palette.border.lightBorder} !important`,
  },
  notchedOutlineCntTransp : {
    border: 'none !important',
    borderRadius: 0,
    boxShadow: 'none'
  },

  notchedOutlineCntNRB: {
    borderRadius: "4px 0 0 4px",
    padding: "0 !important",
    border: `1px solid ${theme.palette.border.lightBorder} !important`,
    borderRight: "none",
  },
  notchedOutlineCntNLB: {
    borderRadius: "0 4px 4px 0",
    padding: "0 !important",
    border: `1px solid ${theme.palette.border.lightBorder} !important`,
    borderLeft: 'none'
  },
  outlinedInputError: {
    "& $notchedOutlineCnt": {
      border: `1px solid ${theme.palette.error.light} !important`,
    },
  },
  outlinedInput: {
    padding: "13px 14px",
    color: theme.palette.text.secondary,
    fontSize: "12px !important",
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 500,
    "&:hover": {
      cursor: "auto",
    },
  },
  outlinedInputsmall: {
    padding: "6px 6px",
    color: theme.palette.text.secondary,
    fontFamily: theme.typography.fontFamilyLato,
    fontWeight: 500,
    fontSize: "12px !important",
    "&:hover": {
      cursor: "auto",
    },
  },
  outlineInputFocus: {
    "& $notchedOutlineCnt": {
      border: `1px solid ${theme.palette.border.lightBorder} !important`,
    },
  },
  errorText: {
    marginLeft: 7,
    "& span a": {
      color: theme.palette.primary.light,
      cursor: "pointer",
    },
  },
  outlinedInputCntTransp:{
    background: 'transparent',
    '&:focus $notchedOutlineCntTransp': {
        border: 'none !important',
    }, 
    '&:hover $notchedOutlineCntTransp': {
      border: 'none !important',
  }
  },
  outlineInputFocusNoBorder: {
    '& $notchedOutlineCntTransp': {
      border: 'none !important'
    }
  },
  defaultInputLabel: {
    transform: "translate(2px, -17px) scale(1)",
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    display: "flex",
    alignItems: "center",
    fontWeight: theme.typography.fontWeightRegular,
    fontFamily:theme.typography.fontFamilyLato
  },
});

export default textFieldStyles;
