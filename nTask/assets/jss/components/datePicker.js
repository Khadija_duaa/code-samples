const datePickerStyles = theme => ({
  datePickerPopper: {
    zIndex: 2,
    minWidth: 560
  },
  onlyDatePopper: {
    zIndex: 1111111,
    width: 250
  },
  RangeDatePickerPopper: {
    zIndex: 6,
    minWidth: 236,
    top: "2px !important",
    left: "22px !important",
  },
  rangeDatePickerInner: {
    border: "1px solid rgba(221,221,221,1)",
    background: theme.palette.common.white
  },
  StaticDatePickerPopper: {
    zIndex: 6,
    left: "23px !important"
  },
  menuTab: {
    background: theme.palette.background.light,
    borderRadius: "10px 10px 0 0",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderBottom: "none",
    opacity: 1,
    minWidth: 141
  },
  staticDatePickerInputCnt: {
    margin: "20px 0"
  },
  menuTabSelected: {
    background: theme.palette.common.white,
    borderRadius: "10px 10px 0 0",
    border: `1px solid ${theme.palette.border.lightBorder}`
  },
  menuTabIndicator: {
    background: "transparent",
    height: 1
  },
  menuTimeCnt: {
    background: theme.palette.background.light,
    marginTop: 48,
    marginBottom: 4,
    border: "1px solid rgba(221,221,221,1)",
    borderRadius: "0 12px 12px 0",
    borderLeft: 0,
    width: 250
  },
  menuTimePickerCnt: {
    background: 'white',
    // border: "1px solid rgba(221,221,221,1)",
    boxShadow: "0px 1px 5px 0px rgba(0,0,0,0.2), 0px 2px 2px 0px rgba(0,0,0,0.14), 0px 3px 1px -2px rgba(0,0,0,0.12)",
    borderRadius: "4px",
    width: 250
  },
  dropdownsLabel: {
    transform: "translate(1px, -4px) scale(1)",
    display: 'block',
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    fontWeight: theme.typography.fontWeightRegular
  },
  dateTimeHeading: {
    background: theme.palette.background.default,
    padding: "10px 20px",
    margin: 0,
    textTransform: "uppercase",
    fontSize: "0.944rem",
    fontWeight: theme.typography.fontWeightRegular,
    borderRadius: "0 12px 0 0",
    color: theme.palette.text.secondary,
    borderBottom: `1px solid ${theme.palette.border.lightBorder}`
  },
  menuDateTimeInnerCnt: {
    padding: "15px 20px"
  },
  timeFieldsCnt: {
    padding: "0 20px"
  },
  timePickerFieldsCnt: {
    padding: 20
  },
  outlinedInputCnt: {
    marginRight: 15,
  },
  outlinedTimeInputCnt: {
    margin: 0,
    background: theme.palette.common.white,
    borderRadius: 4
  },
  timeInputCnt: {
    marginTop: 10
  },
  durationInputCnt: {
    marginTop: 11
  },
  outlinedMinInputCnt: {
    "&:hover $notchedOutlineMinCnt": {
      border: `1px solid ${theme.palette.border.lightBorder} !important`
    }
  },
  outlinedHoursInputCnt: {
    "&:hover $notchedOutlineHoursCnt": {
      border: `1px solid ${theme.palette.border.lightBorder} !important`,
      borderRight: "none !important"
    }
  },
  outlineInputFocus: {
    "& $notchedOutlineCnt": {
      border: `1px solid ${theme.palette.border.lightBorder}`
    },
    "& $notchedOutlineMinCnt": {
      border: `1px solid ${theme.palette.border.lightBorder}`,
    },
    "& $notchedOutlineHoursCnt": {
      border: `1px solid ${theme.palette.border.lightBorder}`,
      borderRight: 0
    },
    "& $notchedOutlineAMCnt": {
      border: `1px solid ${theme.palette.border.lightBorder}`
    },

  },
  outlinedInput: {
    padding: "13px 14px",
    "&:hover": {
      cursor: "auto"
    }
  },
  outlinedDateInput: {
    padding: "13.5px 14px",
    background: theme.palette.background.default,
    borderRadius: 4
  },
  outlinedTaskInput: {
    padding: "10px 14px",
    background: theme.palette.background.default,
    borderRadius: 4,
    fontSize: 22,
    fontWeight: theme.typography.fontWeightRegular
  },
  outlinedInputDisabled: {
    background: "transparent"
  },
  outlinedTimeInput: {
    padding: 14,
    textAlign: "center",
  },
  outlinedHoursInput: {
    background: theme.palette.background.default,
    textAlign: 'center',
    borderRadius: "4px 0 0 4px",
    padding: "12px 14px"
  },
  outlinedMinInput: {
    padding: "12px 14px",
    background: theme.palette.background.default,
    textAlign: 'center'
  },
  outlinedMinInputRound: {
    padding: "12px 14px",
    borderRadius: "0 4px 4px 0",
    borderLeft: 0,
    textAlign: 'center'
  },
  outlinedAmInput: {
    padding: "12px 14px",
    borderRadius: "0 4px 4px 0",
    borderLeft: 0,
    textAlign: 'center',
    "&:hover": {
      cursor: "pointer"
    }
  },
  notchedOutlineCnt: {
    borderRadius: 4,
    border: `1px solid ${theme.palette.border.lightBorder}`
  },
  notchedOutlineHoursCnt: {
    borderRadius: "4px 0 0 4px",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRight: 0

  },
  notchedOutlineMinCnt: {
    borderRadius: 0,
    border: `1px solid ${theme.palette.border.lightBorder}`
  },
  notchedOutlineMinCntRound: {
    borderRadius: 0,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: "0 4px 4px 0"
  },
  notchedOutlineAMCnt: {
    borderRadius: "0 4px 4px 0",
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderLeft: 0
  },
  addTimeBtnCnt: {
    display: "flex",
    padding: "5px 5px",
    cursor: "pointer",
    borderRadius: 20
  },
  addTimeActionBtnCnt: {
    textAlign: "right",
    marginTop: 10
  },
  addTimeBtnText: {
    color: theme.palette.text.primary,
    fontSize: "12px !important",
    lineHeight: "20px",
    marginLeft: 5,
    textDecoration: "underline"
  },
  timeCnt: {
    width: 250,
    background: theme.palette.common.white,
    borderTop: `1px solid ${theme.palette.border.lightBorder}`,
    padding: 10
  },
  timeInputCnt: {
    marginBottom: 5
  },
  timeInputLabel: {
    minWidth: 80,
    fontSize: 10,
    whiteSpace: "break-spaces"
  },
  amPmBtn: {
    position: "absolute",
    zIndex: 1,
    right: 11,
    borderRadius: "0 4px 4px 0",
    borderLeft: `1px solid ${theme.palette.border.lightBorder}`,
    padding: 5,
    cursor: "pointer",
    fontSize: 10

  },
  multipleDropdownCnt: {
    display: "flex"
  },
  errorText: {
    marginLeft: 7,
    color: theme.palette.error.main,
    '& span a': {
      color: theme.palette.primary.light,
      cursor: 'pointer'
    },
  },
  datePickerIconBtn:{
    background: theme.palette.common.white,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    padding: 7,
    marginRight: 4,
    borderRadius: "100%"
  }

})

export default datePickerStyles;