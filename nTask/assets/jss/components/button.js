const buttonStyles = theme => ({
  root: {
    fontFamily: "lato, sans-serif !important",
  },
  noPaddedButton: {
    padding: 0,
    "&:hover": {
      backgroundColor: "transparent"
    }
  },
  noPaddedBtnText: {
    textTransform: "capitalize",
    fontSize: "1.1em !important",
    fontWeight: theme.typography.fontWeightRegular
  },
  IconBtn: {
    background: theme.palette.status.success,
    color: theme.palette.common.white,
    borderRadius: 4,
    fontSize: "14px !important",
    padding: "8px 14px 8px 8px",
    fontWeight: theme.typography.fontWeightRegular,
    textTransform: "capitalize",
    "&:hover": {
      background: theme.palette.primary.dark
    },
    "& $BtnIcon": {
      marginRight: 5
    }
  },
  BtnIcon: {},
  DropdownButton: {
    background: "transparent",
    color: theme.palette.text.secondary,
    fontSize: "12px !important",
    padding: 0,
    minWidth: "unset",
    minHeight: "unset",
    boxShadow: 'none',
    fontWeight: theme.typography.fontWeightLight,
    textTransform: "capitalize",
    "&:hover": {
      background: "none"
    },
    '&:focus': {
      boxShadow: 'none'
    },
    "& $RightBtnIcon": {
      marginRight: 5
    }
  },
  grayBgIconBtn:{
    background: theme.palette.background.medium,
    border: "none",
    padding: 8
  },
  RightBtnIcon: {},
  smallIconBtn: {
    background: "transparent",
    color: theme.palette.text.secondary,
    fontSize: "13px !important",
    padding: 0,
    minWidth: "unset",
    minHeight: "unset",
    boxShadow: 'none',
    fontWeight: theme.typography.fontWeightRegular,
    textTransform: "capitalize",
    "&:hover": {
      background: "none"
    },
    '&:focus': {
      boxShadow: 'none'
    },
    "& $leftBtnIcon": {
      marginRight: 5
    }
  },
  leftBtnIcon: {},
  PlainBtn: {
    background: theme.palette.status.success,
    color: theme.palette.common.white,
    borderRadius: 4,
    boxShadow: "none",
    fontSize: "12px !important",
    padding: "8px 15px",
    fontWeight: theme.typography.fontWeightLight,
    textTransform: "capitalize",
    "&:hover": {
      background: theme.palette.primary.dark
    }
  },
  PublishBtn: {
    background: theme.palette.meetingStatus.Published,
    color: theme.palette.common.white,
    borderRadius: 4,
    boxShadow: "none",
    fontSize: "12px !important",
    padding: "8px 15px",
    fontWeight: theme.typography.fontWeightLight,
    textTransform: "capitalize",
    "&:hover": {
      background: theme.palette.meetingStatus.NewPublished
    }
  },
  PlainIconBtn: {
    background: "none",
    padding: 0,
    "&:hover": {
      background: 'none'
    }
  },
  grayBtn: {
    background: theme.palette.background.items,
    color: theme.palette.text.primary,
    boxShadow: "none",
    borderRadius: 4,
    fontSize: "14px !important",
    padding: "10px 30px",
    fontWeight: theme.typography.fontWeightRegular,
    textTransform: "capitalize",
    "&:hover": {
      background: theme.palette.background.items
    }
  },
dangerBtn: {
    background: theme.palette.background.danger,
    color: theme.palette.common.white,
    borderRadius: 4,
    fontSize: "12px !important",
    padding: "10px 20px",
    boxShadow: "none",
    fontWeight: theme.typography.fontWeightRegular,
    textTransform: "capitalize",
    "&:hover": {
      background: theme.palette.background.danger
    }
  },
  

  filledLightIconBtn: {
    background: theme.palette.background.light,
    border: theme.palette.background.darkBorder,
    borderRadius: 4,
    padding: 6,
    marginTop: 3,
    marginBottom: 3,
    border: `1px solid ${theme.palette.border.lightBorder}` 
  },
  filledWhiteIconBtn: {
  
    background: theme.palette.common.white,
    border: theme.palette.background.darkBorder,
    padding: 8,
    border: `1px solid ${theme.palette.border.lightBorder}`,
    borderRadius: 4
  },
  filledWhiteRoundIconBtn: {
    background: theme.palette.common.white,
    borderRadius: "50%",
    boxShadow: '0px 3px 6px #0000001A',
    padding: 8,
    "&:hover": {
    background: theme.palette.background.contrast,
    }
  },
  TransIconBtn: {
    background: "transparent",
    padding: 4,
    
  },
  smallFilledGray: {
    background: theme.palette.background.medium,
    padding: 2,
    "&:hover": {
      transition: "ease opacity 0.8s",
      background: theme.palette.background.dark,
      opacity:0.8
    }
  },
  TransparentBtn: {
    background: "transparent",
    color: theme.palette.secondary.light,
    borderRadius: 4,
    fontSize: "12px !important",
    padding: "8px 20px",
    fontWeight: theme.typography.fontWeightRegular,
    textTransform: "capitalize"
  }
});

export default buttonStyles;
