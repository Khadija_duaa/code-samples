module.exports = {
  "presets": [
    [
      "@babel/env",
      {
        "targets": {
          "node": "12"
        }
      }
    ],
    "@vue/cli-plugin-babel/preset",
  ],
  "plugins": [
    [
      "component",
      {
        "libraryName": "element-ui",
        "styleLibraryName": "theme-chalk"
      }
    ],
    "@babel/plugin-proposal-class-properties"
  ]
}