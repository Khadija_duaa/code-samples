<?php 

// The following command shows how to use the AWS CLI [3] to list all functions in a specific region using Node.js 12. To find all such functions in your account, repeat this command for each region:
// aws lambda list-functions --function-version ALL --region us-east-1 --output text --query "Functions[?Runtime=='nodejs12.x'].FunctionArn"

$envFileJson = file_get_contents('put_here_output_list-functions.json');
$envFile = json_decode($envFileJson, true);
$sufix = "-<environment_name>";

foreach($envFile as $lambda) {

        $envVariables = getEnvironmentVariables($lambda);

        if(!empty($envVariables)) {
            updateFunctionParameters($lambda, $sufix);
            updateCloudFormationTemplate($lambda, $sufix);
            updateTeamProviderInfo($lambda, $sufix);
        }

}

function updateTeamProviderInfo($lambda, $sufix)
{
    $envName = str_replace('-', '', $sufix);
    $envVariables = getEnvironmentVariables($lambda);
    $pathToTeamProviderInfo = findTeamProviderInfo();
    $teamProvider = file_get_contents($pathToTeamProviderInfo);
    $teamProvider = json_decode($teamProvider, true);

    $funcName = str_replace($sufix, '', $lambda['FunctionName']);
    $function = &$teamProvider[$envName]['categories']['function'][$funcName];
    foreach($envVariables as $key => $variable) {
        $tmp = [];
        $snakeKey = snakeToCamel($key);
        $tmp[$snakeKey] = $variable;
        $function = array_merge($function, $tmp);
    }


    $teamProvider = json_encode($teamProvider, JSON_PRETTY_PRINT | JSON_FORCE_OBJECT);
    $teamProvider = str_replace('    ', '  ', $teamProvider);
    $teamProvider = str_replace('\/', '/', $teamProvider);

    file_put_contents($pathToTeamProviderInfo, $teamProvider);
}

function updateCloudFormationTemplate($lambda, $sufix)
{
    $envVariables = getEnvironmentVariables($lambda);
    $pathToCloudFormationTemplate = findCloudFormationFile($lambda, $sufix);
    $cloudFormation = file_get_contents($pathToCloudFormationTemplate);

    $cloudFormation = json_decode($cloudFormation, true);
    $parameters = &$cloudFormation['Parameters'];

    $tmp = [];
    foreach($envVariables as $key => $variable) {
        $snakeKey = snakeToCamel($key);
        
        $tmp[$snakeKey] = [
            "Type" => "String"
        ];

        if(!array_key_exists($snakeKey, $parameters)) {
            $parameters = array_merge($parameters, $tmp);
        }
    }

    $resources = &$cloudFormation['Resources']['LambdaFunction']['Properties']['Environment']['Variables'];
    $tmp = [];
    foreach($envVariables as $key => $variable) {
        $snakeKey = snakeToCamel($key);
        
        $tmp[$key] = [
            "Ref" => $snakeKey
        ];

        if(!array_key_exists($key, $resources)) {
            $resources = array_merge($resources, $tmp);
        }
    }

    $cloudFormation = json_encode($cloudFormation, JSON_PRETTY_PRINT);
    $cloudFormation = str_replace('    ', '  ', $cloudFormation);
    $cloudFormation = str_replace('\/', '/', $cloudFormation);

    file_put_contents($pathToCloudFormationTemplate, $cloudFormation);
}

function updateFunctionParameters($lambda, $sufix) 
{
    $envVariables = getEnvironmentVariables($lambda);
    $pathToFuncParameters = findFuncFile($lambda, $sufix);
    $pathToFuncParametersJson = file_get_contents($pathToFuncParameters);

    $funcParameters = json_decode($pathToFuncParametersJson, true);

    $environmentVariableList = &$funcParameters['environmentVariableList'];

    foreach($envVariables as $key => $variable) {
        $tmp = [
            "cloudFormationParameterName" => snakeToCamel($key),
            "environmentVariableName" => $key
        ];

        if(!findExistingFuncParameter($environmentVariableList, $key)) {
            $environmentVariableList[] = $tmp;
        }
    }

    $funcParameters = json_encode($funcParameters, JSON_PRETTY_PRINT);
    $funcParameters = str_replace('    ', '  ', $funcParameters);

    file_put_contents($pathToFuncParameters, $funcParameters);
}

/*** Helpers ****/

function findExistingFuncParameter($environmentVariableList, $key) 
{

    if(!isset($environmentVariableList)) {
        return false;
    }

    foreach($environmentVariableList as $variable) {
        if($variable['environmentVariableName'] == $key) {
            return true;
        }
    }

    return false;
}

function snakeToCamel($input)
{
    return lcfirst(str_replace('_', '', ucwords(strtolower($input), '_')));
}

function findFuncFile($lambda, $sufix)
{
    $funcName = str_replace($sufix, '', $lambda['FunctionName']);

    $pathToFunc = 'amplify/backend/function/' . $funcName . '/function-parameters.json';

    if(file_exists($pathToFunc)) {
        return realpath($pathToFunc);
    }

    $outFile = '{
    "environmentVariableList": []
}';

    file_put_contents($pathToFunc, $outFile);

    return realpath($pathToFunc);
}

function findCloudFormationFile($lambda, $sufix)
{
    $funcName = str_replace($sufix, '', $lambda['FunctionName']);

    $pathToFunc = 'amplify/backend/function/' . $funcName . '/' . $funcName . '-cloudformation-template.json';

    if(file_exists($pathToFunc)) {
        return realpath($pathToFunc);
    }

    die("not found for 2" . json_encode($lambda));
}

function findTeamProviderInfo()
{
    return 'amplify/team-provider-info.json';
}

function getEnvironmentVariables($lambda)
{
    $env = $lambda['Environment']['Variables'];

    $env = array_filter($env, function($item) {
        return !in_array($item, [
            'ENV', 'REGION', 'API_HERA_GRAPHQLAPIENDPOINTOUTPUT', 'API_HERA_GRAPHQLAPIIDOUTPUT', 
            'STRIPE_SECRET_KEY', 
            'TELNYX_TOKEN',
            'API_SENDMESSAGE_APIID', 'API_STRIPESETUP_APINAME', 'API_STRIPESETUP_APIID',
            'VUE_APP_INTERCOM_TOKEN',
            'API_TELNYX',
            'GROWSURF_API_KEY',
            'GROWSURF_CAMPAIGN_ID'
        ]);
    }, ARRAY_FILTER_USE_KEY);

    return $env;
}