const response = require('./apiResponse.json')

const tables = [
    {
        table_pattern:[
            "drivers_ranked_by_overall_score__descending",
            "#"
         ],
        table_match_percent: 1,
        table_data:[],
        table_end_string: "Drivers ranked by overall score, descending.",
        min_data: 12,
        columns: [
            { 
                slug: "name",
                left: 57,
                right: 105.12
            },
            { 
                slug: "transporter_id",
                left: 105.12,
                right: 175.94
            },
            { 
                slug: "overal_tier",
                left: 175.94,
                right: 212.3
            },
            { 
                slug: "delivered",
                left: 212.3,
                right: 242.78,
            },
            { 
                slug: "key_focus_area",
                left: 242.78,
                right: 334.97
            },
            { 
                slug: "fico_score",
                left: 334.97,
                right: 380.21,
            },
            { 
                slug: "seatbelt_off_rate",
                left: 380.21,
                right: 414,
            },
            { 
                slug: "dcr",
                left: 414,
                right: 450,
            },
            { 
                slug: "dar",
                left: 450,
                right: 493,
            },
            { 
                slug: "swc_pod",
                left: 493,
                right: 531,
            },
            { 
                slug: "swc_cc",
                left: 531,
                right: 567,
            },
            { 
                slug: "swc_sc",
                left: 567,
                right: 600,
            },
            { 
                slug: "swc_ad",
                left: 600,
                right: 630,
            },
            { 
                slug: "dnrs",
                left: 630,
                right: 665,
            },
            { 
                slug: "pod_opps",
                left: 665,
                right: 690,
            },
            { 
                slug: "cc_opps",
                left: 690,
                right: 730,
            }
        ]
    },
    {
        table_pattern:[
                "drivers_ranked_by_overall_score__descending",
                "fant"
        ],
        table_match_percent: 1,
        table_data:[],
        min_data: 12,
        columns: [
            {
                slug: "name",
                left: 0.05198065936565399,
                right:0.17115876078605652
            },
            {
                slug: "transporter_id",
                left: 0.17115876078605652,
                right: 0.29660940170288086
            },
            {
                slug: "average_tier",
                left: 0.29660940170288086,
                right: 0.34888049960136414
            },
            {
                slug: "dcr",
                left: 0.34888049960136414,
                right: 0.40742412209510803
            },
            {
                slug: "dar",
                left: 0.40742412209510803,
                right: 0.46596774458885193
            },
            {
                slug: "swc_pod",
                left: 0.46596774458885193,
                right: 0.5224205255508423
            },
            {
                slug: "swc_cc",
                left: 0.5224205255508423,
                right: 0.5809641480445862
            },
            {
                slug: "swc_sc",
                left: 0.5809641480445862,
                right: 0.6395078301429749
            },
            {
                slug: "swc_ad",
                left: 0.6395078301429749,
                right: 0.6959605813026428
            },
            {
                slug: "high_low_performer",
                left: 0.6959605813026428,
                right: 0.7984119653701782
            },
            {
                slug: "fant",
                left: 0.798411965370178,
                right: 0.8318654298782349
            },
            {
                slug: "great",
                left: 0.8318654298782349,
                right: 0.8674097657203674
            },
            {
                slug: "fair",
                left: 0.8674097657203674,
                right: 0.8987724781036377
            },
            {
                slug: "poor",
                left: 0.8987724781036377,
                right: 0.9426801800727844
            },
        ]
    }
];


var test = parseTables(response, tables)

const fs = require('fs');

fs.writeFile('results.json', JSON.stringify(test), function(err){
    if(err) return console.log(err)
})



function parseTables(blocks, tables){
    var rowVariance = 6 //How much variation in the top value to be considered a new row
    var isHeader = false
    var tableIndex = null
    var positionLookup = {}
    var biggestIndex = 0;

    blocks.forEach((Block, index)=>{
        var slug = slugify(Block.str)

        //--------CHECK PATTERN TO SEE IF WE HAVE HIT A TABLE---------//
        let prevTableIndex = tableIndex
        tableIndex = findTableIndex(index, tableIndex, tables, blocks)
        //Reset Position Lookup if new table
        if( prevTableIndex !== tableIndex){
            positionLookup = {}
            biggestIndex = 0
        }

        //--------DETERMINE IF BLOCK IS A HEADER--------//
        if( tableIndex >= 0 && tableIndex != null){
            isHeader = tables[tableIndex].columns.find(header=> header.slug == slug)
        }else{
            isHeader = false
        }


        //---------DETERMINE IF BLOCK FALLS BETWEEN COLUMN--------//
        if(!isHeader && tableIndex !== null ){       
            var colIndex = getColumnIndex(Block, tableIndex, tables)

            
            //---------DETERMINE WHICH ROW THIS IS---------//
            var top = Block.transform[5]
            var roundedTop = Math.ceil(top/rowVariance)*rowVariance;
            var page = Block.page
            var position = `${page}-${roundedTop}`
            if( !positionLookup[position] ){
                positionLookup[position] = biggestIndex
                biggestIndex ++ 
            }
            var rowIndex = positionLookup[position]

            if( roundedTop == "804"){
                console.log({row:rowIndex, position: position, index: index, colIndex: colIndex})
            }


            //----------IF THE BLOCK IS WITHIN COLUMN BOUNDS, SAVE TO ROW-------------//
            if(colIndex >= 0){
                var column = tables[tableIndex].columns[colIndex]
                var value = Block.str
                if( column.transform ){
                    value = eval(column.transform)
                }

                //Setup table at correct position if none
                if(tables[tableIndex].table_data[rowIndex] === undefined ){
                    tables[tableIndex].table_data[rowIndex] = []
                }

                //set or append data to column
                if(tables[tableIndex].table_data[rowIndex][colIndex]){
                    tables[tableIndex].table_data[rowIndex][colIndex] += " " + value
                }else{
                    tables[tableIndex].table_data[rowIndex][colIndex] = value
                }
            }        
        }

    })

    
    //----------FORMAT RETURN DATA----------//
    var formattedTables = []
    return tables;
    tables.forEach(table => {
        var obj = {}
        var min_data = table.min_data
        obj.columns = table.columns.map(column=>column.slug)
        obj.table_data = []        
        table.table_data.forEach((row)=>{
            var rowObj = {}
            row.forEach((cell,index)=>{
                let key = obj.columns[index]
                rowObj[key] = cell
            })
            if(Object.keys(rowObj).length >= min_data){
                obj.table_data.push(rowObj)
            }
        })
        formattedTables.push(obj)
    })

    return formattedTables

}

/**
 * Determines if we have found a table or not and returns the current table index
 * @param {Number} i - Current index of blocks
 */
function findTableIndex(currentIndex, tableIndex, tables, blocks){
    //Determine the next table we are looking for
    var nextTableIndex = tableIndex == null ? 0 : tableIndex + 1;
    var nextTable = tables[nextTableIndex];
    if( nextTable == undefined ){
      return tableIndex;
    }


    //Get the next section of blocks to look through for matches
    var tablePattern = nextTable.table_pattern;
    var matchPercent = nextTable.table_match_percent;
    var patternLength = tablePattern.length;
    var isMatchRequiredCount = parseInt(patternLength * matchPercent )
    var checkArray = blocks.slice(currentIndex, currentIndex + patternLength );


    //Check how many of the blocks have matching text in our pattern
    var matchCount = 0;
    nextTable.table_pattern.forEach((header, index) => {
        var isInCheckArray = checkArray.some(block=>{
            var blockText = block.str;
            var blockSlug = slugify(blockText)
            return blockSlug == header
        })
        if( isInCheckArray ){
            matchCount++;
        }
    });

    
    //Check to see if we have found a table!
    if( matchCount >= isMatchRequiredCount ){
        console.log("FOUND TABLE!");
        console.log({matches:matchCount, index: currentIndex, newTableIndex: nextTableIndex});   
        return nextTableIndex
    }else{
        return tableIndex
    }
}


/**
 * Determine the index of the column based on the middle of the block
 * @param {Object} BoundingBox 
 */
function getColumnIndex(Block, tableIndex, tables){
    
    if( tableIndex >= 0 && tableIndex == null ){
        return -1
    }
    let left = Block.transform[4]
    let width = Block.width
    var middle = left + (width/2)
    let index = tables[tableIndex].columns.findIndex(header => {
        let blockLeft = header.left
        let blockRight = header.right
        return (blockLeft <= middle && blockRight >= middle)
    })
    return index
}


/**
 * Turn a string in a consistent slug
 * @param {String} str 
 */
function slugify (str) {   
    if(!str){
        return null
    }
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();
    
    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to   = "aaaaeeeeiiiioooouuuunc------";
    for (var i=0, l=from.length ; i<l ; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 - #\-]/g, '') // remove invalid chars
        .replace(/\s+/g, '_') // collapse whitespace and replace by _
        .replace(/-+/g, '_'); // collapse dashes

    return str;
}
