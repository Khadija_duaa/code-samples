const { defineConfig } = require('@vue/cli-service')
const webpack = require('webpack');

module.exports = {
  configureWebpack: {
    plugins: [
      new webpack.ProvidePlugin({
        // you must `npm install buffer` to use this.
        Buffer: ['buffer', 'Buffer']
      })
    ]
    ,
    resolve: {
      fallback: {
        crypto: require.resolve("crypto-browserify"),
        stream: require.resolve("stream-browserify"),
        util: false
      },
    },
  }
  // options...
 
}
