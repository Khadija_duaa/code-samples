import sys
import boto3
import botocore.exceptions as ex

#################################################################################################################################################
#HELPER
def get_dynamo_table(table_name):
    dynamodb = boto3.resource('dynamodb')
    return dynamodb.Table(table_name)
#################################################################################################################################################
#HELPER
#load tables
def scan_dynamo_table(table_name, scan_kwargs):
    
    table = get_dynamo_table(table_name)

    # Scanning
    complete = False
    items = []
    while not complete:
        try:
            response = table.scan(**scan_kwargs)
            # print('RESPONSE scan:', response)
        except ex.ClientError as error:
            raise Exception(f'Error Scanning Table: {table_name}: {error}')

        items.extend(response.get('Items', []))
        next_key = response.get('LastEvaluatedKey')
        scan_kwargs['ExclusiveStartKey'] = next_key

        # print(f"Items Collected {len(items)}")
        complete = True if next_key is None else False

    return items

#################################################################################################################################################
#HELPER
def get_record(table_name, scan_kwargs):
    
    table = get_dynamo_table(table_name)
    
    # print(table)
    # Updating a record
    try:
        response = table.get_item(**scan_kwargs)
        print('Success:', response)
    except ex.ClientError as error:
        raise Exception(f'Error Get Item of Table: {table_name}: {error}')

#################################################################################################################################################
#HELPER
def update_record(table_name, scan_kwargs):
    
    table = get_dynamo_table(table_name)
    
    # print(table)
    # Updating a record
    try:
        response = table.update_item(**scan_kwargs)
        print('Success:', response)
    except ex.ClientError as error:
        raise Exception(f'Error Scanning Table: {table_name}: {error}')

#################################################################################################################################################
#HELPER
def delete_record(table_name, scan_kwargs):
    
    table = get_dynamo_table(table_name)
    
    # print(table)
    # Updating a record
    try:
        response = table.delete_item(**scan_kwargs)
        print('Success:', response)
    except ex.ClientError as error:
        raise Exception(f'Error Delete a item of Table: {table_name}: {error}')

#################################################################################################################################################
#HELPER
def getCustomListItems(item):
    kwargs = {
        # 'IndexName': "gsi-OptionsCustomLists",
        'ExpressionAttributeNames': {
            "#opcl": "optionsCustomListsCustomListsId",
            "#g": "group",
            "#opt": "option"
        },
        'ExpressionAttributeValues': {
            ":opcl": item["id"],
            ':g': item["group"],
        },
        'FilterExpression': "#g = :g and #opcl = :opcl",
        'ProjectionExpression': 'id, #g, #opt',
    }
    # print('KARGS:', kwargs)
    vehicle_type_options = scan_dynamo_table(DYNAMODB_OPTIONS_CUSTOM_LIST_TABLE_NAME, kwargs)
    # print(f"Vehicle type Options: {len(vehicle_type_options)}")
    return vehicle_type_options

###############################################################################################################################################
#HELPER
def getCustomListbyType(group, type):
        kwargs = {
            'ExpressionAttributeNames': {
                '#g': 'group',
                '#t': 'type'
            },
            'ExpressionAttributeValues': {
                ':t': type,
                ':g': group
            },
            'FilterExpression': '#t = :t and #g = :g',
            'ProjectionExpression': 'id, #g, #t'
        }
        vehicle_type = scan_dynamo_table(DYNAMODB_CUSTOM_LIST_TABLE_NAME, kwargs)
        print(f"getCustomListbyType - Custom List Size: {len(vehicle_type)}")
        return vehicle_type
#################################################################################################################################################
#HELPER
def getVehicleTypeFromCustomList(group):
    customDelVanId = None
    standardParcelId = None
    tenkVanId = None
    standardParcelSmallId = None
    standardParcelLargeId = None
    standardParcelXLId = None

    try:
        vehicleCustomList = getCustomListbyType(group, 'vehicle-type')
        options = getCustomListItems(vehicleCustomList[0])
        for item in options:
            if item['option'] == '10,000lbs Van':
                tenkVanId = item['id']
            elif item['option'] == 'Custom Delivery Van':
                customDelVanId = item['id']
            elif item['option'] == 'Standard Parcel':
                standardParcelId = item['id']
            elif item['option'] == 'Standard Parcel Small':
                standardParcelSmallId = item['id']
            elif item['option'] == 'Standard Parcel Large':
                standardParcelLargeId = item['id']
            elif item['option'] == 'Standard Parcel XL':
                standardParcelXLId = item['id']
        result = {
            'tenkVanId': tenkVanId,
            'customDelVanId': customDelVanId,
            'standardParcelId': standardParcelId,
            'standardParcelSmallId': standardParcelSmallId,
            'standardParcelLargeId': standardParcelLargeId,
            'standardParcelXLId': standardParcelXLId
        }
        print("vehicle type id per tenant", result)
        return result
    except Exception as e:
        # print("[getVehicleTypeFromCustomList] Error in function getVehicleTypeFromCustomList", e)
        return {
            'error': e
        }

###############################################################################################################################################
#HELPER
def getAllTenants():
        kwargs = {
            'ExpressionAttributeNames': {
                '#g': 'group',
                '#t': 'type',
            },
            'ProjectionExpression': 'id, #g, #t'
        }
        tenant_list = scan_dynamo_table(DYNAMODB_TENANT_TABLE_NAME, kwargs)
        # print(f"tenant list: {len(tenant_list)}")
        return tenant_list

#################################################################################################################################################
