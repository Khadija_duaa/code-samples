import sys
import uuid
import json
import time
import boto3
import requests
import botocore.exceptions as ex
from boto3.dynamodb.conditions import Attr, Key
from datetime import datetime
from awsglue.utils import getResolvedOptions


DAYS_TO_REMOVE_NUMBER = 30

DYNAMODB_TENANT_TABLE_NAME = ""
DYNAMODB_MSG_SVS_PROVIDER_TABLE_NAME = ""
DYNAMODB_TELNYX_TABLE_NAME = ""


CLIENT_ID = ""
CLIENT_SECRET = ""
REFERESH_TOKEN = ""
ACCESS_TOKEN = ""
SSM_CRM_AUTH_PATH_PARAM_NAME = ""
MODEL_ACCOUNTS = "Accounts"
API_DOMAIN = "https://www.zohoapis.com"
ZOHO_OAUTH_DOMAIN = "https://accounts.zoho.com/oauth/v2/token"
ENDPOINT_ACCOUNTS = f"{API_DOMAIN}/crm/v5/{MODEL_ACCOUNTS}"
ENDPOINT_SEARCH = "{api_domain}/crm/v5/{module_name}/search"

def load_job_parameters():
    global DAYS_TO_REMOVE_NUMBER
    global DYNAMODB_TENANT_TABLE_NAME
    global DYNAMODB_MSG_SVS_PROVIDER_TABLE_NAME
    global DYNAMODB_TELNYX_TABLE_NAME
    global SSM_CRM_AUTH_PATH_PARAM_NAME

    args = getResolvedOptions( sys.argv, [
            'DYNAMODB_TENANT_TABLE_NAME', 
            'DYNAMODB_MSG_SVS_PROVIDER_TABLE_NAME',
            'DYNAMODB_TELNYX_TABLE_NAME',
            'SSM_CRM_AUTH_PATH_PARAM_NAME'
        ] )
    
    DAYS_TO_REMOVE_NUMBER = 30
    DYNAMODB_TENANT_TABLE_NAME = args['DYNAMODB_TENANT_TABLE_NAME']
    DYNAMODB_MSG_SVS_PROVIDER_TABLE_NAME = args['DYNAMODB_MSG_SVS_PROVIDER_TABLE_NAME']
    DYNAMODB_TELNYX_TABLE_NAME = args['DYNAMODB_TELNYX_TABLE_NAME']
    SSM_CRM_AUTH_PATH_PARAM_NAME = args['SSM_CRM_AUTH_PATH_PARAM_NAME']
    
###################################################***AWS API Methods***#######################################################################
def get_dynamo_table(table_name):
    dynamodb = boto3.resource('dynamodb')
    return dynamodb.Table(table_name)


def get_item(table_name, kwargs):
    table = get_dynamo_table(table_name)
    response = None
    try:
        response = table.get_item(**kwargs)
    except ex.ClientError as error:
        print(f'Error Get Item: {table_name}: {error}')

    return response


def scan_dynamo_table(table_name, scan_kwargs):
    
    table = get_dynamo_table(table_name)

    # Scanning
    complete = False
    items = []
    while not complete:
        try:
            response = table.scan(**scan_kwargs)
        except ex.ClientError as error:
            raise Exception(f'Error Scanning Table: {table_name}: {error}')

        items.extend(response.get('Items', []))
        next_key = response.get('LastEvaluatedKey')
        scan_kwargs['ExclusiveStartKey'] = next_key
        complete = True if next_key is None else False

    return items


def update_record(table_name, params):
    try:
        table = get_dynamo_table(table_name)
        response = table.update_item(**params)
        return response
    except ex.ClientError as error:
        print(f'Error Update Record: {table_name}: {error}')
        return None


def create_record(table_name, params):
    try:
        table = get_dynamo_table(table_name)
        response = table.put_item(**params)
        return response
    except ex.ClientError as error:
        print(f'Error Creating Record: {table_name}: {error}')
        return None


def query_dynamo_records(table_name, params):
    table = get_dynamo_table(table_name)
    try:
        response = table.query(**params)
        return response
    except ex.ClientError as error:
        print(f'Error Querying the Table: {table_name}: {error}')
        return []


def get_crm_auth_from_ssm(path_param):
    """
    """
    print(f"Getting CRM Secrets [{path_param}] from SSM")
    client = boto3.client('ssm')
    response = client.get_parameters_by_path(
        Path=path_param,
        Recursive=True
    )

    global CLIENT_ID
    global CLIENT_SECRET
    global REFERESH_TOKEN
    
    for param in response["Parameters"]:
        if "client_id" in param["Name"]:
            CLIENT_ID = param["Value"]
        elif "client_secret" in param["Name"]:
            CLIENT_SECRET = param["Value"]
        elif "refresh_token" in param["Name"]:
            REFERESH_TOKEN = param["Value"]

    return response

###################################################***ZOHO API Methods***######################################################################
def refresh_token():
    payload = {
        'client_id': CLIENT_ID,
        'refresh_token': REFERESH_TOKEN,
        'client_secret': CLIENT_SECRET,
        'grant_type': 'refresh_token'
    }
    response = requests.request("POST", ZOHO_OAUTH_DOMAIN, data=payload)
    
    global ACCESS_TOKEN
    global API_DOMAIN

    if 200 == response.status_code:
        ACCESS_TOKEN = response.json()["access_token"]
        API_DOMAIN = response.json()["api_domain"]
    
    return response


def api_call(method, endpoint, params={}, payload={}):
    if not method or not endpoint:
        print(f"Valid Method: {method} or Endpoint: {endpoint} Needed")
        return

    headers = {
        'Authorization': f"Bearer {ACCESS_TOKEN}",
        'Content-Type': 'application/json'
    }
    try:
        time.sleep(1)
        response = requests.request(method, endpoint, headers=headers, data=payload, params=params)
    except Exception as ex:
        print(f"Zoho API Exception: {ex}")

    print(f"[{method}]: {endpoint} --> {response.status_code}")

    return response


def update_record_in_zoho(input):
    """
    """
    payload = {"data": []}
    payload["data"].append(input)
    
    url = ENDPOINT_ACCOUNTS

    response = api_call("PUT", url, payload=json.dumps(payload))
    
    # Regenerating Access Token and Saving it in SSM
    if 401 == response.status_code:
        res = refresh_token()
        if 200 == res.status_code:
            # Sending Update API call again
            response = api_call("PUT", url, payload=json.dumps(payload))

    return response


def upsert_data_in_crm(company):
    print("Updating Tenant in Zoho CRM")

    # Check if Company Already Exist
    res = search_record({"criteria": f"((Tenant_ID:equals:{company['Tenant_ID']}))"}, MODEL_ACCOUNTS)
    if 200 == res.status_code:
        company.pop("Tenant_ID")
        company["id"] = res.json()["data"][0]["id"]
        update_record_in_zoho(company)
    else:
        print(f"Tenant {company['Tenant_ID']} doesn't exist in Zoho CRM")


def search_record(params, module_name):
    """
    Response code 204: 
        Record doesn't exist

    Response code 200    
        Record Found
    """
    url = ENDPOINT_SEARCH.format(api_domain=API_DOMAIN, module_name=module_name)
    response = api_call("GET", url, params=params)

    # Regenerating Access Token and Saving it in SSM
    if 401 == response.status_code:
        res = refresh_token()
        if 200 == res.status_code:
            # Sending Search API call again
            response = api_call("GET", url, params=params)
    
    return response

################################################################################################################################################
def get_tenant_status(tenant_plans, trialExpDate, ):
    status = 'Unknown'
        
    if 'trial' in tenant_plans:
        today = datetime.now()
        expDate = datetime.strptime(trialExpDate, "%Y-%m-%dT%H:%M:%S.%fZ")
        if trialExpDate != None and expDate < today:
            status = 'Lapsed Trial'
        else:
            status = 'Trial'
  
    if 'bundle' in tenant_plans:
        status = 'Active - Bundle'


    if 'standard' in tenant_plans:
        if len(tenant_plans) > 1:
            status = 'Active - Premium'
        else:
            status = 'Active - Standard'
  
    if 'none' in tenant_plans:
        status = 'Churned'

    return status    


def get_tenants():
   
    # Scanning Tenants based on trialExpDate
    now = datetime.now()
    today = now.strftime("%Y-%m-%dT%H:%M:%S")
    print("Today: ", today)
    kwargs = {
        'IndexName': 'byTrialExpDate',
        'FilterExpression': Attr('trialExpDate').lt(today),
        'ExpressionAttributeNames': { '#group': 'group' },
        'ProjectionExpression': 'id, #group, accountPremiumStatus, trialExpDate, messageServiceProvider, originationNumber'
    }
    tenant_data = scan_dynamo_table(DYNAMODB_TENANT_TABLE_NAME, kwargs)
    return tenant_data


def process_tenant(tenant_data):
    # Processing Tenants
    today = datetime.now()
    iso_datetime = today.isoformat("T", "milliseconds")+"Z"

    for tenant in tenant_data:
        tenant_update_params = {
            'Key': { "id": tenant["id"] },
            'ExpressionAttributeValues': {},
            'UpdateExpression': ''
        }
        tenant_plans = []
        for plan in tenant["accountPremiumStatus"]:
            tenant_plans.append(plan.lower())

        # Get Updated Customer Status
        status = get_tenant_status(tenant_plans, tenant["trialExpDate"])
        tenant_update_params['ExpressionAttributeValues'][':customerStatus'] = status
        tenant_update_params['UpdateExpression'] = 'SET customerStatus = :customerStatus'
    
        trialExpDate = datetime.strptime(tenant["trialExpDate"], "%Y-%m-%dT%H:%M:%S.%fZ")
        diff = today - trialExpDate
        days = diff.days

        print(f"================= Logging =====================")
        print(f"Days Elapsed: {days}")
        print(f"Group: {tenant['group']}")
        print(f"Trial Exp Date: {tenant['trialExpDate']}")
        print(f"Tenant Plans: {tenant_plans}")
        print(f"===============================================")

        company = {}
        
        # Update Message Service Provider in case of Trial Expired Tenant >= 30 Days
        if 'trial' in tenant_plans and 'Telnyx' == tenant["messageServiceProvider"]:            

            if days >= DAYS_TO_REMOVE_NUMBER:
                tenant_update_params['ExpressionAttributeValues'][':messageServiceProvider'] = 'None'
                tenant_update_params['UpdateExpression'] += ', messageServiceProvider = :messageServiceProvider '

                tenant_update_params['ExpressionAttributeValues'][':originationNumber'] = 'None'
                tenant_update_params['UpdateExpression'] += ', originationNumber = :originationNumber '

                company["Origination_Number"] = ""
                print(f"Plan: {tenant_plans}, Provider: {tenant['messageServiceProvider']}, Days: {days}, DAYS_TO_REMOVE_NUMBER: {DAYS_TO_REMOVE_NUMBER}")
                
                # Create Message Service Provider History
                create_params =  {
                    'Item': {
                        'id': str(uuid.uuid4()),
                        'group': tenant["group"],
                        'previousMessageServicerProvider': tenant['messageServiceProvider'],
                        'currentMessageServicerProvider': 'None',
                        'messageServiceProviderTenantId': tenant['id'], 
                        'date': iso_datetime,
                        'createdAt': iso_datetime,
                        'updatedAt': iso_datetime,
                        '__typename': 'MessageServiceProvider'
                    }
                }
                create_record(DYNAMODB_MSG_SVS_PROVIDER_TABLE_NAME, create_params)

                # Get Telnyx by Phone Number
                params = {
                    'IndexName': 'byPhoneNumber',
                    'KeyConditionExpression': Key('phoneNumber').eq(tenant['originationNumber']),
                    'ProjectionExpression': 'id, phone'
                }
                telnyx = query_dynamo_records(DYNAMODB_TELNYX_TABLE_NAME, params)

                # Update Telnyx
                if "Items" in telnyx and telnyx["Items"]:
                    telnyx_update_params = {
                        'Key': { "id": telnyx["Items"][0]["id"] },
                        'UpdateExpression': 'SET #group = :group, #updatedAt = :updatedAt',                     
                        'ExpressionAttributeNames': { 
                            '#group': 'group',
                            '#updatedAt': 'updatedAt'
                        },
                        'ExpressionAttributeValues': {
                            ':group': 'null',
                            ':updatedAt': iso_datetime
                        }
                    }
                    update_record(DYNAMODB_TELNYX_TABLE_NAME, telnyx_update_params)

        update_record(DYNAMODB_TENANT_TABLE_NAME, tenant_update_params)

        # Update Customer Status in Zoho
        company["Tenant_ID"] = tenant['id']
        company["Customer_Status"] = status
        upsert_data_in_crm(company)


if __name__ == "__main__":
    # init
    load_job_parameters()
    
    # Init Zoho
    get_crm_auth_from_ssm(SSM_CRM_AUTH_PATH_PARAM_NAME)
    refresh_token()

    tenant_data = get_tenants()
    
    # Processing Tenants
    process_tenant(tenant_data)