import sys
import hashlib
import boto3
import botocore.exceptions as ex
from boto3.dynamodb.conditions import Attr
from awsglue.utils import getResolvedOptions


DYNAMODB_TENANT_TABLE_NAME = ""
DYNAMODB_VALUE_LIST_TABLE_NAME = ""
DYNAMODB_VALUE_LIST_ITEM_TABLE_NAME = ""

def load_job_parameters():
    global DYNAMODB_TENANT_TABLE_NAME
    global DYNAMODB_VALUE_LIST_TABLE_NAME
    global DYNAMODB_VALUE_LIST_ITEM_TABLE_NAME

    args = getResolvedOptions( sys.argv, [
            'DYNAMODB_TENANT_TABLE_NAME',
            'DYNAMODB_VALUE_LIST_TABLE_NAME',
            'DYNAMODB_VALUE_LIST_ITEM_TABLE_NAME'
        ]
    )
    
    DYNAMODB_TENANT_TABLE_NAME = args['DYNAMODB_TENANT_TABLE_NAME']
    DYNAMODB_VALUE_LIST_TABLE_NAME = args['DYNAMODB_VALUE_LIST_TABLE_NAME']
    DYNAMODB_VALUE_LIST_ITEM_TABLE_NAME = args['DYNAMODB_VALUE_LIST_ITEM_TABLE_NAME']


###################################################***AWS API Methods***#######################################################################
def get_dynamo_table(table_name):
    dynamodb = boto3.resource('dynamodb')
    return dynamodb.Table(table_name)


def scan_dynamo_table(table_name, scan_kwargs):
    table = get_dynamo_table(table_name)

    # Scanning
    complete = False
    items = []
    while not complete:
        try:
            response = table.scan(**scan_kwargs)
        except ex.ClientError as error:
            raise Exception(f'Error Scanning Table: {table_name}: {error}')

        items.extend(response.get('Items', []))
        next_key = response.get('LastEvaluatedKey')
        scan_kwargs['ExclusiveStartKey'] = next_key

        complete = True if next_key is None else False

    print(f"Items Collected {len(items)}")
    return items


def query_dynamo_records(params):
    table_name = params['TableName']
    table = get_dynamo_table(table_name)
    try:
        response = table.query(**params)
        return response
    except ex.ClientError as error:
        print(f'Error Querying the Table: {table_name}: {error}')
        return []


def delete_record(table_name, params):
    table = get_dynamo_table(table_name)

    # Deleting a record
    try:
        response = table.delete_item(**params)
        return response
    except ex.ClientError as error:
        print(f'Error Delete a item of Table: {table_name}: {error}')
        return None
###############################################################################################################################################
def get_tenants():
    params = {
        'ExpressionAttributeNames': {
            '#id': 'id',
            '#group': 'group'
        },
        'TableName': DYNAMODB_TENANT_TABLE_NAME,
        'ProjectionExpression': '#id, #group',
    }
    tenants = scan_dynamo_table(DYNAMODB_TENANT_TABLE_NAME, params)
    return tenants


def get_value_list_by_tenant(tenantID):
    params = {
        'ExpressionAttributeValues': {
            ':valueListTenantId': tenantID,
        },
        'ExpressionAttributeNames': {
            '#id': 'id',
            '#group': 'group',
            '#key': 'key',
            '#valueListTenantId': 'valueListTenantId'
        },
        'TableName': DYNAMODB_VALUE_LIST_TABLE_NAME,
        'IndexName': 'gsi-TenantValueLists',
        'KeyConditionExpression': '#valueListTenantId = :valueListTenantId',
        'ProjectionExpression': '#id, #group, #key, #valueListTenantId',
    }
    valueList = query_dynamo_records(params)
    return valueList


def get_value_list_item(valueListItemID):
    params = {
        'ExpressionAttributeValues': {
            ':valueListItemValueListId': valueListItemID,
        },
        'ExpressionAttributeNames': {
            '#id': 'id',
            '#group': 'group',
            '#value': 'value',
            '#valueListItemValueListId': 'valueListItemValueListId'
        },
        'TableName': DYNAMODB_VALUE_LIST_ITEM_TABLE_NAME,
        'IndexName': 'gsi-ValueListItems',
        'KeyConditionExpression': '#valueListItemValueListId = :valueListItemValueListId',
        'ProjectionExpression': '#id, #group, #value, #valueListItemValueListId',
    }
    valueListItems = query_dynamo_records(params)
    return valueListItems


def find_duplicate(value_items):
    duplicate_value_items = []
    md5_list = []
    
    for item in value_items:
        md5_result = hashlib.md5(item["value"].encode("utf-8")).hexdigest()
        if md5_result not in md5_list:
            md5_list.append(md5_result)
        else:
            duplicate_value_items.append(item)
    
    return duplicate_value_items


def delete_duplicate_item(duplicates_value_items):
    for dupe in duplicates_value_items:
        params = {
            'Key': {
                "id": dupe['id']
            },
            'ConditionExpression': "#group = :group AND #value= :value",
            'ExpressionAttributeNames': {
                '#group': 'group',
                '#value': 'value'
            },
            'ExpressionAttributeValues': {
                ":group": dupe['group'],
                ":value": dupe['value']
            }
        }
        if( delete_record(DYNAMODB_VALUE_LIST_ITEM_TABLE_NAME, params) ):
            print(f"Deleted     -- ID: {dupe['id']}, Group: {dupe['group']}, Key: {value['key']}, Value: {dupe['value']}")
        else:
            print(f"Not Deleted -- ID: {dupe['id']}, Group: {dupe['group']}, Key: {value['key']}, Value: {dupe['value']}")



if __name__ == "__main__":
    # init
    load_job_parameters()
    
    tenants = get_tenants()

    # Processing Tenants
    for tenant in tenants:
        valueList = get_value_list_by_tenant(tenant["id"]).get('Items', [])

        for value in valueList:
            value_list_item = get_value_list_item(value["id"]).get('Items', [])
            duplicates_value_items = find_duplicate(value_list_item)
            
            # Delete duplicte entries from ValueListItem Table
            delete_duplicate_item(duplicates_value_items)
           
