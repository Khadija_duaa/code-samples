import re
import sys
import boto3
import botocore.exceptions as ex
from boto3.dynamodb.conditions import Attr
from awsglue.utils import getResolvedOptions


DYNAMODB_STAFF_TABLE_NAME = ""

def load_job_parameters():
    global DYNAMODB_STAFF_TABLE_NAME

    args = getResolvedOptions( sys.argv, ['DYNAMODB_STAFF_TABLE_NAME'] )
    
    DYNAMODB_STAFF_TABLE_NAME = args['DYNAMODB_STAFF_TABLE_NAME']


###################################################***AWS API Methods***#######################################################################
def get_dynamo_table(table_name):
    dynamodb = boto3.resource('dynamodb')
    return dynamodb.Table(table_name)


def scan_dynamo_table(table_name, scan_kwargs):
    
    table = get_dynamo_table(table_name)

    # Scanning
    complete = False
    items = []
    while not complete:
        try:
            response = table.scan(**scan_kwargs)
        except ex.ClientError as error:
            raise Exception(f'Error Scanning Table: {table_name}: {error}')

        items.extend(response.get('Items', []))
        next_key = response.get('LastEvaluatedKey')
        scan_kwargs['ExclusiveStartKey'] = next_key

        print(f"Items Collected {len(items)}")
        print(f"Last Evaluate Key: {next_key}")
        complete = True if next_key is None else False

    return items


def updateAltName(table_name, id, altNames):
    table = get_dynamo_table(table_name)
    response = table.update_item(
        Key={ 'id': id },
        UpdateExpression='SET alternateNames = :altNames',
        ExpressionAttributeValues={
            ':altNames': altNames
        }
    )
    print(response)
    return response
###############################################################################################################################################


if __name__ == "__main__":
    # init
    load_job_parameters()
    
    # Scanning Tenants based on trialExpDate
    kwargs = {
        'FilterExpression': Attr('alternateNames').exists(),
        'ProjectionExpression': 'id, alternateNames',
    }
    staff_data = scan_dynamo_table(DYNAMODB_STAFF_TABLE_NAME, kwargs)

    # Processing Staff
    for staff in staff_data:
        altsNames = staff["alternateNames"]
        altNamesSet = { re.sub(r'\s+', ' ', altsNames[0]) }
        
        # Removing Extra Spaces from the Alt Names
        for name in altsNames:
            altNamesSet.add(re.sub(r'\s+', ' ', name))

        updatedAltNames = list(altNamesSet)
        updateAltName(DYNAMODB_STAFF_TABLE_NAME, staff["id"], updatedAltNames)
