import boto3
import time
import sys
import threading
import json
from awsglue.utils import getResolvedOptions

def getTablePrimaryKeys(dynamodb, table_name):
    response = dynamodb.describe_table(TableName=table_name)
    key_schema = response['Table']['KeySchema']
    keys = []
    for key in key_schema:
        if key['KeyType'] == 'HASH':
            keys.append("{}".format(key['AttributeName']))
    print("Table {} PK Keys: {}".format(table_name, keys))
    return keys

def getDataSources(appsync_client, appsync_id):
    ds = []
    paginator = appsync_client.get_paginator('list_data_sources')
    page_iterator = paginator.paginate(apiId=appsync_id)
    print("Fetching list of datasources from source page by page")
    for page in page_iterator:
        for item in page["dataSources"]:
            if item["type"] != "NONE":
                ds.append(item)
    return ds

def fetchTenantsFromTable(dynamodb, table_name, tenant_attribute):
    print("------------------------------------------------------------------")
    print("Now Scanning Table: {}".format(table_name))
    tenants = []
    response = table_resource.scan(ProjectionExpression="#{}".format(tenant_attribute), 
                                        ExpressionAttributeNames = {"#{}".format(tenant_attribute): tenant_attribute})
    result = response['Items']
    while 'LastEvaluatedKey' in response:
        response = table_resource.scan(ProjectionExpression="#{}".format(tenant_attribute),
                                         ExpressionAttributeNames = {"#{}".format(tenant_attribute): tenant_attribute},
                                         ExclusiveStartKey=response['LastEvaluatedKey'])
        if "Items" in response:
            result.extend(response['Items'])
            
    for item in result:
        if tenant_attribute in item:
            if isinstance(item[tenant_attribute], str):
                tenants.append(item[tenant_attribute])
            elif isinstance(item[tenant_attribute], list):
                tenants.extend(item[tenant_attribute])
                
    tenants = set(tenants)
    print("Tenants found in the table: {}".format(tenants))
    return tenants



if __name__ == '__main__':
    # Get Glue Job Parameters
    try:
        JOB_PARAMETERS = ['region', 'appsync_id', 'attribute', 'target_tables']
        args = getResolvedOptions(sys.argv, JOB_PARAMETERS)
        print("Arguments:{}".format(args))
        region = args['region']
        appsync_id = args['appsync_id']
        attribute = args['attribute']
        target_tables = args['target_tables'] # Empty means all tables
        if not (region and appsync_id and attribute):
            print(
                "region, appsync_id and attribute are mandatory params to proceed.")
            exit(1)
    except Exception as ex:
        print(ex)
        print("Invalid or incomplete parameters provided for the glue job")
        exit(1)

    # Get a list of all tables in the AppSync API
    api_id = appsync_id
    session = boto3.Session(region_name=region)
    appsync = boto3.client('appsync')
    print("AppSync Client Initiated Successfully")
    dynamodb = boto3.client('dynamodb', region_name=region)
    print("DynamoDB Client Initiated Successfully")
    dataSources = getDataSources(appsync, api_id) # appsync.list_data_sources(apiId=api_id)['dataSources']
    print("List of Data Sources fetched from AppSync Id: {}".format(api_id))
    existing_tenants = []
    all_tenants = []
    for ds in dataSources:
        if not ds['name'].endswith("Table"):
            continue
        table_name = ds['dynamodbConfig']['tableName']
        table_resource = boto3.resource('dynamodb', region_name=region).Table(table_name)
        if target_tables == "ALL" or (target_tables != "ALL" and table_name in target_tables.split(",")):
            if table_name.startswith("Tenant-"):
                existing_tenants = fetchTenantsFromTable(dynamodb, table_name, attribute)
            else:
                all_tenants.extend(fetchTenantsFromTable(dynamodb, table_name, attribute))
                
    print("###############################>> LIST PARTIALLY REMOVED TENANTS <<###############################")
    all_tenants = set(all_tenants)
    for et in existing_tenants:
        if et in all_tenants:
            all_tenants.remove(et)
    if "system_admin" in all_tenants:
        all_tenants.remove("system_admin")
    if "admin" in all_tenants:
        all_tenants.remove("admin")
    i=1
    all_tenants = list(all_tenants)
    all_tenants = [value for value in all_tenants if value is not None]
    all_tenants.sort()
    for t in all_tenants:
        print("{}: {}".format(i, t))
        i=i+1