import boto3
import time
import sys
import threading
import json
from awsglue.utils import getResolvedOptions


def clean_s3(s3_arn, region, group):
    print("Clean S3 folder for tenant: {}".format(group))
    s3 = boto3.resource('s3', region_name=region)
    bucket_name = get_arn_resource_name(s3_arn)
    folder_name = "public/{}".format(group)
    print("Folder Path Is: {}".format(folder_name))
    bucket = s3.Bucket(bucket_name)

    # Recursively delete all objects in the folder
    for obj in bucket.objects.filter(Prefix=folder_name):
        print("Deleting S3 Object: {}".format(obj))
        obj.delete()

    # Delete the folder
    bucket.Object(folder_name).delete()


def getDataSources(appsync_client, appsync_id):
    ds = []
    paginator = appsync_client.get_paginator('list_data_sources')
    page_iterator = paginator.paginate(apiId=appsync_id)
    print("Fetching list of datasources from source page by page")
    for page in page_iterator:
        for item in page["dataSources"]:
            if item["type"] != "NONE":
                ds.append(item)
    return ds


def clean_cognito(cognito_user_pool_id, region, group):
    try:
        print("Clean Cognito User Pool for Group: {}".format(group))
        # Create a Cognito Identity Provider client
        client = boto3.client('cognito-idp', region_name=region)

        # List all the users in the group
        users = client.list_users_in_group(
            UserPoolId=cognito_user_pool_id,
            GroupName=group
        )['Users']

        # Remove each user from the group
        for user in users:
            print("Removing User [{}] From Group".format(user['Username']))
            client.admin_remove_user_from_group(
                UserPoolId=cognito_user_pool_id,
                GroupName=group,
                Username=user['Username']
            )
            print("Physically removing User [{}] From Cognito User Pool".format(
                user['Username']))
            response = client.admin_delete_user(
                UserPoolId=cognito_user_pool_id,
                Username=user['Username']
            )
        print("Remove Group {}".format(group))
        client.delete_group(UserPoolId=cognito_user_pool_id, GroupName=group)
    except Exception as ex:
        print(ex)


def get_arn_resource_name(arn):
    return arn.split(':')[-1].split('/')[-1]


def fetchTenantDataFromTable(dynamodb, table_name, attribute, attValue):
    items = []
    response = dynamodb.scan(
        TableName=table_name,
        ExpressionAttributeNames={"#{}".format(attribute): "{}".format(
            attribute)},  # define expression attribute name for 'group'
        # use expression attribute name in filter expression
        FilterExpression="#{} = :attValue".format(attribute),
        ExpressionAttributeValues={':attValue': {
            'S': attValue}},  # define filter value
    )

    items = response['Items']
    while 'LastEvaluatedKey' in response:
        response = dynamodb.scan(
            TableName=table_name,
            ExpressionAttributeNames={"#{}".format(attribute): "{}".format(
                attribute)},  # define expression attribute name for 'group'
            # use expression attribute name in filter expression
            FilterExpression="#{} = :attValue".format(attribute),
            ExpressionAttributeValues={':attValue': {
                'S': attValue}},  # define filter value
            ExclusiveStartKey=response['LastEvaluatedKey']
        )

        if "Items" in response and len(response['Items']) > 0:
            items.extend(response['Items'])

    return items


if __name__ == '__main__':
    # Get Glue Job Parameters
    try:
        JOB_PARAMETERS = ['region', 'cognito_user_pool_id',
                          'appsync_id', 'attribute', 'attribute_value', 's3_arn']
        args = getResolvedOptions(sys.argv, JOB_PARAMETERS)
        print("Arguments:{}".format(args))
        region = args['region']
        cognito_user_pool_id = args['cognito_user_pool_id']  # optional
        appsync_id = args['appsync_id']
        attribute = args['attribute']
        attribute_value = args['attribute_value']
        s3_arn = args['s3_arn']  # optional
        if not (region and appsync_id and attribute and attribute_value):
            print(
                "region, appsync_id, attribute and attribute value are mandatory params to proceed.")
            exit(1)
    except Exception as ex:
        print(ex)
        print("Invalid or incomplete parameters provided for the glue job")
        exit(1)

    # Get a list of all tables in the AppSync API
    api_id = appsync_id
    session = boto3.Session(region_name=region)
    appsync = boto3.client('appsync')
    dynamodb = boto3.client('dynamodb', region_name=region)
    print("AppSync Client Initiated Successfully")
    # appsync.list_data_sources(apiId=api_id)['dataSources']
    dataSources = getDataSources(appsync, api_id)
    print("List of Data Sources fetched from AppSync Id: {}".format(api_id))
    batch_size = 5
    for attValue in attribute_value.split(","):
        print(">>>>> Now Processing for Tenant Group: {}".format(attValue))
        for ds in dataSources:
            if not ds['name'].endswith("Table"):
                continue
            table_name = ds['dynamodbConfig']['tableName']
            print(
                "----------------------------------------------------------------------")
            print("Currently Processing Table: {}".format(table_name))
            table_resource = boto3.resource(
                'dynamodb', region_name=region).Table(table_name)
            maxItems = -1
            delete_iteration = 1
            while maxItems != 0:
                # Prep for batch
                items = fetchTenantDataFromTable(
                    dynamodb, table_name, attribute, attValue)
                maxItems = len(items)
                if maxItems == 0 and delete_iteration > 1:
                    print("All items deleted Successfully")
                    break

                print("Total records fetched: {}".format(maxItems))

                if maxItems == 0 and delete_iteration == 1:
                    break

                batches = [items[i:i+batch_size]
                           for i in range(0, len(items), batch_size)]

                print("Delete Iteration: {}".format(delete_iteration))
                delete_iteration = delete_iteration+1

                # Populate Primary Key values
                response = dynamodb.describe_table(TableName=table_name)
                key_schema = response['Table']['KeySchema']
                keys = []
                for key in key_schema:
                    if key['KeyType'] == 'HASH':
                        keys.append("{}".format(key['AttributeName']))
                print("Table PK Keys: {}".format(keys))

                # delete items in batches
                for batch in batches:
                    item_keys = []
                    for item in batch:
                        key = {}
                        for k in keys:
                            key[k] = "{}".format(item[k]['S'])
                        item_keys.append(key)
                    print("Item Keys to Delete: {}".format(item_keys))
                    with table_resource.batch_writer() as batch:
                        # Add the delete requests to the batch writer
                        for k in item_keys:
                            batch.delete_item(Key=k)
        clean_s3(s3_arn, region, attValue)
        clean_cognito(cognito_user_pool_id, region, attValue)
        print("########################################################################")
