import sys
import json
import requests
import boto3
import time
import botocore.exceptions as ex
from boto3.dynamodb.conditions import Attr
from datetime import datetime
from awsglue.utils import getResolvedOptions


###################################################***Utility Methods***#######################################################################
CLIENT_ID = ""
CLIENT_SECRET = ""
REFERESH_TOKEN = ""
ACCESS_TOKEN = ""

PROCESS_LOGS_FOR_DAYS = "1"
DYNAMODB_AUDITLOG_TABLE_NAME = ""
DYNAMODB_TENANT_TABLE_NAME = ""
DYNAMODB_USER_TABLE_NAME = ""
SSM_CRM_AUTH_PATH_PARAM_NAME = ""

API_DOMAIN = "https://www.zohoapis.com"
ZOHO_OAUTH_DOMAIN = "https://accounts.zoho.com/oauth/v2/token"

MODEL_CONTACTS = "Contacts"
MODEL_ACCOUNTS = "Accounts"

ENDPOINT_CONTACTS = f"{API_DOMAIN}/crm/v5/{MODEL_CONTACTS}"
ENDPOINT_ACCOUNTS = f"{API_DOMAIN}/crm/v5/{MODEL_ACCOUNTS}"
ENDPOINT_SEARCH = "{api_domain}/crm/v5/{module_name}/search"

def compare_dates(date1, date2):
    # convert string to date
    dt_obj1 = datetime.strptime(date1, "%Y-%m-%dT%H:%M:%S")
    dt_obj2 = datetime.strptime(date2, "%Y-%m-%dT%H:%M:%S")
    if dt_obj1 == dt_obj2:
        return 0
    elif dt_obj1 > dt_obj2:
        return 1 
    else:
        return 2

def load_job_parameters():
    global PROCESS_LOGS_FOR_DAYS
    global DYNAMODB_AUDITLOG_TABLE_NAME
    global DYNAMODB_TENANT_TABLE_NAME
    global DYNAMODB_USER_TABLE_NAME
    global SSM_CRM_AUTH_PATH_PARAM_NAME
    
    args = getResolvedOptions(sys.argv, [
            'PROCESS_LOGS_FOR_DAYS',
            'DYNAMODB_AUDITLOG_TABLE_NAME',
            'DYNAMODB_TENANT_TABLE_NAME',
            'DYNAMODB_USER_TABLE_NAME',
            'SSM_CRM_AUTH_PATH_PARAM_NAME']
    )
    
    PROCESS_LOGS_FOR_DAYS = args['PROCESS_LOGS_FOR_DAYS']
    DYNAMODB_AUDITLOG_TABLE_NAME = args['DYNAMODB_AUDITLOG_TABLE_NAME']
    DYNAMODB_TENANT_TABLE_NAME = args['DYNAMODB_TENANT_TABLE_NAME']
    DYNAMODB_USER_TABLE_NAME = args['DYNAMODB_USER_TABLE_NAME']
    SSM_CRM_AUTH_PATH_PARAM_NAME = args['SSM_CRM_AUTH_PATH_PARAM_NAME']
    
###############################################################################################################################################

###################################################***AWS API Methods***#######################################################################
def get_dynamo_table(table_name):
    dynamodb = boto3.resource('dynamodb')
    return dynamodb.Table(table_name)


def scan_dynamo_table(table_name, scan_kwargs):
    
    table = get_dynamo_table(table_name)

    # Scanning
    complete = False
    items = []
    while not complete:
        try:
            response = table.scan(**scan_kwargs)
        except ex.ClientError as error:
            raise Exception(f'Error Scanning Table: {table_name}: {error}')

        items.extend(response.get('Items', []))
        next_key = response.get('LastEvaluatedKey')
        scan_kwargs['ExclusiveStartKey'] = next_key

        print(f"Items Collected {len(items)}")
        complete = True if next_key is None else False

    return items


def get_item(table_name, kwargs):
    table = get_dynamo_table(table_name)
    response = None
    try:
        response = table.get_item(**kwargs)
    except ex.ClientError as error:
        print(f'Error Scanning Table: {table_name}: {error}')

    return response


def get_crm_auth_from_ssm(path_param):
    """
    """
    print(f"Getting CRM Secrets [{path_param}] from SSM")
    client = boto3.client('ssm')
    response = client.get_parameters_by_path(
        Path=path_param,
        Recursive=True
    )

    global CLIENT_ID
    global CLIENT_SECRET
    global REFERESH_TOKEN
    
    for param in response["Parameters"]:
        if "client_id" in param["Name"]:
            CLIENT_ID = param["Value"]
        elif "client_secret" in param["Name"]:
            CLIENT_SECRET = param["Value"]
        elif "refresh_token" in param["Name"]:
            REFERESH_TOKEN = param["Value"]

    return response


def set_auth_in_ssm(param_name, auth_dict):
    """
    """
    print(f"Setting [{param_name}] in SSM")
    client = boto3.client('ssm')
    response = client.put_parameter(
        Name=param_name,
        Value=json.dumps(auth_dict),
        Type='String'
    )
    return response
###############################################################################################################################################

#####################################################***CRM API Methods***#####################################################################
def refresh_token():
    payload = {
        'client_id': CLIENT_ID,
        'refresh_token': REFERESH_TOKEN,
        'client_secret': CLIENT_SECRET,
        'grant_type': 'refresh_token'
    }
    response = requests.request("POST", ZOHO_OAUTH_DOMAIN, data=payload)
    
    global ACCESS_TOKEN
    global API_DOMAIN

    if 200 == response.status_code:
        ACCESS_TOKEN = response.json()["access_token"]
        API_DOMAIN = response.json()["api_domain"]
    
    return response


def api_call(method, endpoint, params={}, payload={}):
    if not method or not endpoint:
        print(f"Valid Method: {method} or Endpoint: {endpoint} Needed")
        return

    headers = {
        'Authorization': f"Bearer {ACCESS_TOKEN}",
        'Content-Type': 'application/json'
    }
    try:
        time.sleep(1)
        response = requests.request(method, endpoint, headers=headers, data=payload, params=params)
    except Exception as ex:
        print(f"Zoho API Exception: {ex}")

    print(f"[{method}]: {endpoint} --> {response.status_code}")

    return response


def insert_record(input, module_name):
    """
    """
    payload = {"data": []}
    payload["data"].append(input)
    
    url = ENDPOINT_ACCOUNTS
    if MODEL_CONTACTS == module_name:
        url = ENDPOINT_CONTACTS

    response = api_call("POST", url, payload=json.dumps(payload))
    # Regenerating Access Token and Saving it in SSM
    if 401 == response.status_code:
        res = refresh_token()
        if 200 == res.status_code:
            # Sending Update API call again
            response = api_call("POST", url, payload=json.dumps(payload))

    return response


def update_record(input, module_name):
    """
    """
    payload = {"data": []}
    payload["data"].append(input)
    
    url = ENDPOINT_ACCOUNTS
    if MODEL_CONTACTS == module_name:
        url = ENDPOINT_CONTACTS

    response = api_call("PUT", url, payload=json.dumps(payload))
    
    # Regenerating Access Token and Saving it in SSM
    if 401 == response.status_code:
        res = refresh_token()
        if 200 == res.status_code:
            # Sending Update API call again
            response = api_call("PUT", url, payload=json.dumps(payload))

    return response


def search_record(params, module_name):
    """
    Response code 204: 
        Record doesn't exist

    Response code 200    
        Record Found
    """
    url = ENDPOINT_SEARCH.format(api_domain=API_DOMAIN, module_name=module_name)
    response = api_call("GET", url, params=params)

    # Regenerating Access Token and Saving it in SSM
    if 401 == response.status_code:
        res = refresh_token()
        if 200 == res.status_code:
            # Sending Search API call again
            response = api_call("GET", url, params=params)
    
    return response
###############################################################################################################################################

###################################################***Service***###############################################################################
def init():
    load_job_parameters()
    get_crm_auth_from_ssm(SSM_CRM_AUTH_PATH_PARAM_NAME)
    refresh_token()   

def process_dynamodb_data(data):
    """
    retVal: 
        user list,
        company list
    """
    # tenant_table_name = DYNAMODB_TENANT_TABLE_NAME
    # user_table_name = DYNAMODB_USER_TABLE_NAME
    users = {}
    companies = {}

    # Getting Unique Users from the Audit Logs
    for item in data:
        item["createdAt"] = item["createdAt"].split(".")[0]
        if item["email"] not in users:
            users[item["email"]] = { 
                "userID" : item["userID"],
                "createdAt": item["createdAt"]
            }
        else:
            res = compare_dates(item["createdAt"], users[item["email"]]["createdAt"])
            if res == 1:
                users[item["email"]]["createdAt"] = item["createdAt"]

        # Getting Unique Companies from the Audit Logs
        if item["tenantID"] not in companies:
            companies[item["tenantID"]]= {
                "tenantID" : item["tenantID"],
                "createdAt": item["createdAt"]
            }
        else:
            res = compare_dates(item["createdAt"], companies[item["tenantID"]]["createdAt"])
            if res == 1:
                companies[item["tenantID"]]["createdAt"] = item["createdAt"]

    return users, companies


def prepare_crm_payloads(audit_log_users, audit_log_companies):
    """
    Following is the mapping between AuditLog DynamoDB Table and CRM Modles

        Contact/Hera User       |   Audit Log Table	
        ===========================================
        Last_Active             | 	createdAt


        Account/Hera Tenant     |   Audit Log Table	
        ===========================================
        Last_Active             | 	createdAt

       Crossed Items in above table are not being synced
        These attributes will be used in future.
    """
    
    contacts = []
    companies = []

    try:
        user_keys = audit_log_users.keys()
        company_keys = audit_log_companies.keys()
    except ValueError as e:
        print(f"Error while preparing CRM Payloads: {e}")
        return

    for key in user_keys:
        user = audit_log_users[key]
        cont = {}
        cont["User_ID"] = user["userID"]
        cont["Last_Active"] = user["createdAt"]
        contacts.append(cont)

    for key in company_keys:
        company = audit_log_companies[key]
        comp = {}
        comp["Tenant_ID"] = company["tenantID"]
        comp["Last_Active"] = company["createdAt"]
        companies.append(comp)

    return companies, contacts


def upsert_data_in_crm(companies, contacts):
    print("Upserting Companies in Zoho CRM")
    for company in companies:
        # Check if Company Already Exist
        res = search_record({"criteria": f"((Tenant_ID:equals:{company['Tenant_ID']}))"}, MODEL_ACCOUNTS)
        if 200 == res.status_code:
            company.pop("Tenant_ID")
            company["id"] = res.json()["data"][0]["id"]
            update_record(company, MODEL_ACCOUNTS)
        else:
            # insert_record(company, MODEL_ACCOUNTS)
            print(f"{company['Tenant_ID']} doesn't exist in CRM")

    print("Upserting Contacts in CRM")
    for contact in contacts:
        # Check if Contact Already Exist
        res = search_record({"criteria": f"((Hera_User_ID:equals:{contact['User_ID']}))"}, MODEL_CONTACTS)
        if 200 == res.status_code:
            contact.pop("User_ID")
            contact["id"] = res.json()["data"][0]["id"]
            update_record(contact, MODEL_CONTACTS)
        else:
            # insert_record(contact, MODEL_CONTACTS)
            print(f"{contact['User_ID']} doesn't exist in CRM")
###############################################################################################################################################

if __name__ == "__main__":

    # Init:
    #   - Fetch Client_ID, CLient_SECRET & REFRESH_TOKEN from SSM
    #   - Generate fresh ACCESS_TOKEN
    init()

    # Get and process Dynamo Audit Log data for 
    #   the number of days provided in PROCESS_LOGS_FOR_DAYS env variables.
    days = int(PROCESS_LOGS_FOR_DAYS)
    if days > 0:
        now = datetime.now()
        start_time = datetime(now.year, now.month, now.day-days, 00, 00, 00).strftime("%Y-%m-%dT%H:%M:%S")
        end_time = datetime(now.year, now.month, now.day-days, 23, 59, 59).strftime("%Y-%m-%dT%H:%M:%S")
        print(f"Start DateTime: {start_time}")
        print(f"End DateTime {end_time}")
        kwargs = {
            'IndexName': 'byCreatedAt',
            'FilterExpression': Attr('createdAt').between(start_time, end_time),
            'ProjectionExpression': 'id, createdAt, userID, tenantID, email'
        }
        audit_log_data = scan_dynamo_table(DYNAMODB_AUDITLOG_TABLE_NAME, kwargs)
        print(f"Audit Log Data Size: {len(audit_log_data)}")

        # Process and get unique records from Audit logs 
        #   - Users With latest activity
        #   - Tenants / Companies
        audit_log_users, audit_log_companies = process_dynamodb_data(audit_log_data)
        print(f"Audit Log Processed User's Data Size: {len(audit_log_users)}")
        print(f"Audit Log Processed Company's Data Size: {len(audit_log_companies)}")

        # Prepare Payloads for CRM APIs
        companies, contacts = prepare_crm_payloads(audit_log_users, audit_log_companies)
    
        # For Logging
        print(f"Total Companies to update in CRM: {len(companies)}")
        print(f"Total Contacts to update in CRM : {len(contacts)}")

        # Upsert Data in CRM
        upsert_data_in_crm(companies, contacts)
    