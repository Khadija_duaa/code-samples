import sys
import boto3
import botocore.exceptions as ex
from awsglue.utils import getResolvedOptions
from boto3.dynamodb.conditions import Key, Attr
from pprint import pprint

###################################################*** Instructions & Global Variables***#######################################################################
#IF YOU WANT TO USE THIS SCRIPT IN ANOTHER TABLE. FOLLOW THIS STEPS:
# 1. CHANGE table name IN GLOBAL ENV in Job Details
# 2. Set the MODE of REPORT
# 3. Edit the REMOVE_FIELD_LIST or/and UPDATE_DICT

# ADITIONAL NOTES: IT'S IMPORTANT TO KNOW THAT THIS SCRIPT CAN NOT ERASE PARTITION KEYS, 
# FOR THE REST (SORT KEY AND COMMON FIELD), YES

DYNAMODB_TABLE_NAME = ""
#If you only want to print how many records will be updated write True
REPORT_MODE = True
#If you only want to execute one test, write True
TEST_MODE= True

#If you don't want to update any record leave in [] and {} respectively:
REMOVE_FIELD_LIST = ["licensePlateExp"]
UPDATE_DICT = {
    "status": "Inactive",
}

# #########################First script
# GET_INPUT = {
#     'ExpressionAttributeNames': {
#         '#g': 'group',
#         '#s': 'status',
#         '#lpe': 'licensePlateExp'
#     },
#     'ExpressionAttributeValues': {
#         ':empty_string' : '',
#         ':null' : None
#     },
#     'FilterExpression': "#s = :empty_string and #lpe = :null",
#     'ProjectionExpression': 'id, #g, #s, #lpe'
# }

# UPDATE_INPUT = {
#     'ExpressionAttributeNames': {
#         '#s': 'status',
#         '#lpe': 'licensePlateExp'
#     },
#     'ExpressionAttributeValues': {
#         ':temp' : 'Inactive',
#     },
#     'UpdateExpression': 'REMOVE #lpe SET #s = :temp',
# }

##########################################Second run
# GET_INPUT = {
#     'ExpressionAttributeNames': {
#         '#g': 'group',
#         '#s': 'status',
#         '#lpe': 'licensePlateExp'
#     },
#     'ExpressionAttributeValues': {
#         ':empty_string' : '',
#         ':null' : None
#     },
#     'FilterExpression': "#s = :empty_string or #s = :null",
#     'ProjectionExpression': 'id, #g, #s, #lpe'
# }

# UPDATE_INPUT = {
#     'ExpressionAttributeNames': {
#         '#s': 'status',
#     },
#     'ExpressionAttributeValues': {
#         ':temp' : 'Inactive',
#     },
#     'UpdateExpression': 'SET #s = :temp',
# }

##########################################Third run
# GET_INPUT = {
#     'ExpressionAttributeNames': {
#         '#g': 'group',
#         '#s': 'status',
#         '#lpe': 'licensePlateExp'
#     },
#     'ExpressionAttributeValues': {
#         ':empty_string' : '',
#         ':null' : None
#     },
#     'FilterExpression': "#lpe = :empty_string or #lpe = :null",
#     'ProjectionExpression': 'id, #g, #s, #lpe'
# }

# UPDATE_INPUT = {
#     'ExpressionAttributeNames': {
#         # '#s': 'status',
#         '#lpe': 'licensePlateExp'
#     },
#     'ExpressionAttributeValues': {
#         # ':temp' : 'Inactive',
#     },
#     'UpdateExpression': 'REMOVE #lpe ',
#     # 'UpdateExpression': 'SET #s = :temp',
# }

##########################################Check cases of vehicles without vehicles type
GET_INPUT = {
    'ExpressionAttributeNames': {
        '#g': 'group',
        '#s': 'status',
        '#lpe': 'licensePlateExp',
        '#vt': 'vehicleVehicleTypeId',
    },
    'FilterExpression': "attribute_not_exists(#vt)",
    'ProjectionExpression': 'id, #g, #s, #lpe, #vt'
}

UPDATE_INPUT = {
    # 'ExpressionAttributeNames': {
    #     # '#s': 'status',
    #     '#lpe': 'licensePlateExp'
    # },
    # 'ExpressionAttributeValues': {
    #     # ':temp' : 'Inactive',
    # },
    # 'UpdateExpression': 'REMOVE #lpe ',
    # 'UpdateExpression': 'SET #s = :temp',
}

###############################################################################################################################################
def generate_update_input(id):
    remove_query = ''
    update_query = ''
    if len(REMOVE_FIELD_LIST) > 0:
        remove_query = 'remove ' + ' and remove '.join(map(str, REMOVE_FIELD_LIST))
    if len(UPDATE_DICT) > 0:
        for key, value in UPDATE_DICT.items():
            update_query = update_query + f" and set {key} = {value}"
    # generate final query
    final_query = remove_query + update_query
    if len(UPDATE_DICT) > 0 and len(REMOVE_FIELD_LIST) <=0 :
        final_query = final_query[5:]

    kwargs = {
        'Key': {
            "id": id,
        },
        'UpdateExpression': final_query,
    }

    return kwargs

#################################################################################################################################################
#HELPER
def get_dynamo_table(table_name):
    dynamodb = boto3.resource('dynamodb')
    return dynamodb.Table(table_name)
#################################################################################################################################################
#HELPER
def scan_dynamo_table(table_name, scan_kwargs):
    
    table = get_dynamo_table(table_name)

    # Scanning
    complete = False
    items = []
    while not complete:
        try:
            response = table.scan(**scan_kwargs)
            # print('RESPONSE scan:', response)
        except ex.ClientError as error:
            raise Exception(f'Error Scanning Table: {table_name}: {error}')

        items.extend(response.get('Items', []))
        next_key = response.get('LastEvaluatedKey')
        scan_kwargs['ExclusiveStartKey'] = next_key

        # print(f"Items Collected {len(items)}")
        complete = True if next_key is None else False

    return items

#################################################################################################################################################
#HELPER
def update_record(table_name, scan_kwargs):
    
    table = get_dynamo_table(table_name)
    
    # print(table)
    # Updating a record
    try:
        response = table.update_item(**scan_kwargs)
        print('Success:', response)
    except ex.ClientError as error:
        raise Exception(f'Error Scanning Table: {table_name}: {error}')

#################################################################################################################################################
def load_job_parameters():
    global DYNAMODB_TABLE_NAME
    
    args = getResolvedOptions(sys.argv, [
            'DYNAMODB_TABLE_NAME',]
    )
    
    DYNAMODB_TABLE_NAME = args['DYNAMODB_TABLE_NAME']


###############################################################################################################################################
def init():
    print("****** START JOB ******")
    load_job_parameters()
    # print("DYNAMODB_TABLE_NAME:", DYNAMODB_TABLE_NAME)

###############################################################################################################################################
def generate_get_input():
        #******************************FilterExpression ************************************
        filter_expression = ''
        if len(REMOVE_FIELD_LIST) > 0:
            for item in REMOVE_FIELD_LIST:
                filter_expression = filter_expression + f" or {item} = :empty_string or {item} = :null"
        if len(UPDATE_DICT) > 0:
            for key, value in UPDATE_DICT.items():
                filter_expression = filter_expression + f" or {key} = :empty_string or {key} = :null "
        if len(REMOVE_FIELD_LIST) > 0 or len(UPDATE_DICT) > 0:
            filter_expression = filter_expression[4:]

        #****************************** Projection Expression ******************************
        projection_expression = ''
        if len(REMOVE_FIELD_LIST) > 0:
            for item in REMOVE_FIELD_LIST:
                projection_expression = projection_expression + f", {item}"
        if len(UPDATE_DICT) > 0:
            for key, value in UPDATE_DICT.items():
                projection_expression = projection_expression + f", {key}"
        projection_expression = 'id, #g' + projection_expression

        #****************************** Generate kwargs ************************************

        kwargs = {
            'ExpressionAttributeNames': {
                '#g': 'group',
            },
            'ExpressionAttributeValues': {
                ':empty_string' : '',
                ':null' : None
            },
            'FilterExpression': filter_expression,
            'ProjectionExpression': projection_expression,
        }

        return kwargs
###############################################################################################################################################

def load_vehicles_list():
        # kwargs = {
        #     'ExpressionAttributeNames': {
        #         '#g': 'group',
        #         '#dynamic_field': FIELD_TO_CLEAN,
        #     },
        #     'ExpressionAttributeValues': {
        #         ':empty_string' : '',
        #         ':null' : None
        #     },
        #     'FilterExpression': '#dynamic_field = :empty_string or #dynamic_field = :null',
        #     'ProjectionExpression': 'id, #g, #dynamic_field'
        # }
        kwargs = generate_get_input()
        if GET_INPUT is not "":
            kwargs = GET_INPUT
        print("------LOAD VEHICLES INPUT--------\n", kwargs)
        vehicle_list = scan_dynamo_table(DYNAMODB_TABLE_NAME, kwargs)
        print("\n=============VEHICLE LIST ================\n\n", vehicle_list)
        print(f"\n Vehicles List Size: {len(vehicle_list)}")
        return vehicle_list

###############################################################################################################################################

def update_vehicle(id):
    print('****** START TO UPDATE ******')
    try:
        # MODEL OF kwargs
        # kwargs = {
        #         'Key': {
        #             "id": id,
        #         },
        #         'ExpressionAttributeNames': {
        #             '#dynamic_field': FIELD_TO_CLEAN,
        #             ':temp' : NEW_VALUE
        #         },
        #         'UpdateExpression': 'set #dynamic_field = :temp',
        #         'UpdateExpression': 'remove #dynamic_field',
        #     }
        #in next fx
        kwargs = generate_update_input(id)
        if UPDATE_INPUT is not '':
            kwargs = {
                'Key': {
                    "id": id,
                },
                **UPDATE_INPUT,
            }
        print("------UPDATE INPUT--------\n", kwargs)
        if REPORT_MODE == False:
            update_record(DYNAMODB_TABLE_NAME, kwargs)
            print("\n\n------VEHICLE UPDATE SUCCESSFULLY--------\n\n")
    except Exception as e:
        print("[UPDATE-setVehicleType] Error 2 in function setVehicleType", e)

###############################################################################################################################################

def update_list_of_vehicle(cleanup_vehicles_list):
    for vehicle in cleanup_vehicles_list:
        update_vehicle(vehicle["id"])
        if TEST_MODE == True:
            return

#################################################################################################################################################

def clean_vehicles():
    cleanup_vehicles_list = load_vehicles_list()
    print(f"\n\n======FIELD TO CLEAN : {REMOVE_FIELD_LIST} ========\n\n")
    update_list_of_vehicle(cleanup_vehicles_list)

###############################################################################################################################################

if __name__ == "__main__":
    print(f"****** START JOB : UPDATING EPIC ******")
    # Init:
    #   - Fetch Client_ID, CLient_SECRET & REFRESH_TOKEN from SSM
    #   - Generate fresh ACCESS_TOKEN
    init()
    clean_vehicles()
###############################################################################################################################################