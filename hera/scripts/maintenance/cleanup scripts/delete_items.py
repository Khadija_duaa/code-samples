import sys
import boto3
import botocore.exceptions as ex
from awsglue.utils import getResolvedOptions
from boto3.dynamodb.conditions import Key, Attr
from pprint import pprint

###################################################*** Instructions & Global Variables***#######################################################################
#IF YOU WANT TO USE THIS SCRIPT IN ANOTHER TABLE. FOLLOW THIS STEPS:
# 1. CHANGE table name IN GLOBAL ENV in Job Details
# 2. Set the MODE of REPORT
# 3. Edit the REMOVE_FIELD_LIST or/and UPDATE_DICT

# ADITIONAL NOTES: IT'S IMPORTANT TO KNOW THAT THIS SCRIPT CAN NOT ERASE PARTITION KEYS, 
# FOR THE REST (SORT KEY AND COMMON FIELD), YES

DYNAMODB_TABLE_NAME = ""
#If you only want to print how many records will be updated write True
REPORT_MODE = True
#If you only want to execute one test, write True
TEST_MODE= True

#If you don't want to update any record leave in [] and {} respectively:
# please only make one change at time 
REMOVE_FIELD_LIST = ["licensePlateExp"]
UPDATE_DICT = {
    "status": "Inactive",
}


##########################################Check cases of vehicles without vehicles type
# GET_INPUT = {
#     'ExpressionAttributeNames': {
#         '#g': "group",
#         '#oclid': 'optionsCustomListsStaffOptionCustomListId',
#         '#oclstaff': 'optionsCustomListsStaffStaffId',
#     },
#     'ExpressionAttributeValues': {
#                 ':g' : 'solve-it-simply-17',
#             },
#     'FilterExpression': "#g = :g",
#     'ProjectionExpression': 'id, #g, #oclid, #oclstaff'
# }

# GET_INPUT = {
#     'ExpressionAttributeNames': {
#         '#g': "group",
#         '#oclid': 'optionsCustomListsStaffOptionCustomListId',
#         '#oclstaff': 'optionsCustomListsStaffStaffId',
#     },
#     'ProjectionExpression': 'id, #g, #oclid, #oclstaff'
# }

GET_INPUT = {
    'ExpressionAttributeNames': {
        '#g': "group",
    },
    'ProjectionExpression': 'id, #g'
}

#################################################################################################################################################
#HELPER
def get_dynamo_table(table_name):
    dynamodb = boto3.resource('dynamodb')
    return dynamodb.Table(table_name)
#################################################################################################################################################
#HELPER
def scan_dynamo_table(table_name, scan_kwargs):
    
    table = get_dynamo_table(table_name)

    # Scanning
    complete = False
    items = []
    while not complete:
        try:
            response = table.scan(**scan_kwargs)
            # print('RESPONSE scan:', response)
        except ex.ClientError as error:
            raise Exception(f'Error Scanning Table: {table_name}: {error}')

        items.extend(response.get('Items', []))
        next_key = response.get('LastEvaluatedKey')
        scan_kwargs['ExclusiveStartKey'] = next_key

        # print(f"Items Collected {len(items)}")
        complete = True if next_key is None else False

    return items

#################################################################################################################################################
#HELPER
def update_record(table_name, scan_kwargs):
    
    table = get_dynamo_table(table_name)
    
    # print(table)
    # Updating a record
    try:
        response = table.update_item(**scan_kwargs)
        print('Success:', response)
    except ex.ClientError as error:
        raise Exception(f'Error Scanning Table: {table_name}: {error}')

#################################################################################################################################################
#HELPER
def get_record(table_name, scan_kwargs):
    
    table = get_dynamo_table(table_name)
    
    # print(table)
    # Updating a record
    try:
        response = table.get_item(**scan_kwargs)
        print('Success:', response)
    except ex.ClientError as error:
        raise Exception(f'Error Get Item of Table: {table_name}: {error}')

#################################################################################################################################################
#HELPER
def delete_record(table_name, scan_kwargs):
    
    table = get_dynamo_table(table_name)
    
    # print(table)
    # Updating a record
    try:
        response = table.delete_item(**scan_kwargs)
        print('Success:', response)
    except ex.ClientError as error:
        raise Exception(f'Error Delete a item of Table: {table_name}: {error}')

#################################################################################################################################################

def load_job_parameters():
    global DYNAMODB_TABLE_NAME
    
    args = getResolvedOptions(sys.argv, [
        'DYNAMODB_TABLE_NAME',]
    )
    
    DYNAMODB_TABLE_NAME = args['DYNAMODB_TABLE_NAME']


###############################################################################################################################################
def init():
    print("****** START JOB ******")
    load_job_parameters()
    # print("DYNAMODB_TABLE_NAME:", DYNAMODB_TABLE_NAME)

###############################################################################################################################################

def load_vehicles_list():
        kwargs = GET_INPUT
        print("------LOAD ITEMSSS--------\n", kwargs)
        vehicle_list = scan_dynamo_table(DYNAMODB_TABLE_NAME, kwargs)
        print("\n=============VEHICLE LIST ================\n\n", vehicle_list)
        print(f"\n Vehicles List Size: {len(vehicle_list)}")
        return vehicle_list

###############################################################################################################################################

def delete_record_of_table(id):
    print('****** START TO UPDATE ******')
    try:
        # MODEL OF kwargs
        kwargs = {
                'Key': {
                    "id": id,
                }
            }
        print("------UPDATE INPUT--------\n", kwargs)
        if REPORT_MODE == False:
            delete_record(DYNAMODB_TABLE_NAME, kwargs)
            print("\n\n------VEHICLE UPDATE SUCCESSFULLY--------\n\n")
    except Exception as e:
        print("[UPDATE-setVehicleType] Error 2 in function setVehicleType", e)

###############################################################################################################################################

def delete_conections(cleanup_vehicles_list):
    for vehicle in cleanup_vehicles_list:
        delete_record_of_table(vehicle["id"])
        if TEST_MODE == True:
            return

#################################################################################################################################################

def clean_vehicles():
    cleanup_vehicles_list = load_vehicles_list()
    print(f"\n\n======FIELD TO CLEAN : {REMOVE_FIELD_LIST} ========\n\n")
    delete_conections(cleanup_vehicles_list)

###############################################################################################################################################

if __name__ == "__main__":
    print(f"****** START JOB : UPDATING EPIC ******")
    # Init:
    #   - Fetch Client_ID, CLient_SECRET & REFRESH_TOKEN from SSM
    #   - Generate fresh ACCESS_TOKEN
    init()
    clean_vehicles()
###############################################################################################################################################