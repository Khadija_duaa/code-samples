import sys
import boto3
import botocore.exceptions as ex
from awsglue.utils import getResolvedOptions
from boto3.dynamodb.conditions import Key, Attr
from pprint import pprint


# Specify the name of the DynamoDB table
DYNAMODB_TABLE_NAME = "LabelType-mn2kxb25mbbdpjsoqg7v7ig6hq-devqa"

#################################################################################################################################################
def delete_dynamo_item(table_name, id):
    table = get_dynamo_table(table_name)
    try:
        table.delete_item(Key={'id': id })
    except ex.ClientError as error:
        raise Exception(f'Error Delete Table Item: {table} - {id}: {error}')

#HELPER
def get_dynamo_table(table_name):
    dynamodb = boto3.resource('dynamodb')
    return dynamodb.Table(table_name)

#################################################################################################################################################
#HELPER
def scan_dynamo_table(table_name, scan_kwargs):
    
    table = get_dynamo_table(table_name)

    # Scanning
    complete = False
    items = []
    while not complete:
        try:
            response = table.scan(**scan_kwargs)
            print('RESPONSE scan:', response)
        except ex.ClientError as error:
            raise Exception(f'Error Scanning Table: {table_name}: {error}')

        items.extend(response.get('Items', []))
        next_key = response.get('LastEvaluatedKey')
        scan_kwargs['ExclusiveStartKey'] = next_key

        # print(f"Items Collected {len(items)}")
        complete = True if next_key is None else False

    return items

#################################################################################################################################################

def clean_table():
    kwargs = {
        'ExpressionAttributeNames': {
            '#g': 'group',
        },
        'ProjectionExpression': 'id, #g',
    }
    labelType_list = scan_dynamo_table(DYNAMODB_TABLE_NAME, kwargs)
    for labelType in labelType_list:
        # print("label type ===============", labelType)
        # print("label type id ===", labelType['id'])
        delete_dynamo_item(DYNAMODB_TABLE_NAME, labelType['id'])
        

#################################################################################################################################################
def load_job_parameters():
    global DYNAMODB_TABLE_NAME
    
    args = getResolvedOptions(sys.argv, [
            'DYNAMODB_TABLE_NAME',]
    )
    
    DYNAMODB_TABLE_NAME = args['DYNAMODB_TABLE_NAME']


###############################################################################################################################################
def init():
    print("****** START JOB ******")
    load_job_parameters()
    print("DYNAMODB_TABLE_NAME:", DYNAMODB_TABLE_NAME)

#################################################################################################################################################
#HELPER
if __name__ == "__main__":
    print(f"****** START JOB : UPDATING EPIC ******")
    # Init:
    #   - Fetch Client_ID, CLient_SECRET & REFRESH_TOKEN from SSM
    #   - Generate fresh ACCESS_TOKEN
    init()
    clean_table()
########################################################################################################################
