import sys
import boto3
import botocore.exceptions as ex
from boto3.dynamodb.conditions import Attr
from awsglue.utils import getResolvedOptions
from boto3.dynamodb.conditions import Key, Attr
from datetime import datetime
import uuid

###################################################*** Global Variables***#######################################################################
DYNAMODB_TENANT_TABLE_NAME = ""
DYNAMODB_CUSTOM_LIST_TABLE_NAME = ""
DYNAMODB_OPTIONS_CUSTOM_LIST_TABLE_NAME = ""
DYNAMODB_STAFF_TABLE_NAME = ""
DYNAMODB_OPTIONS_CUSTOM_LIST_STAFF_TABLE_NAME = ""

################################################################################################################################################
################################################################################################################################################
def load_job_parameters():
    global DYNAMODB_TENANT_TABLE_NAME
    global DYNAMODB_CUSTOM_LIST_TABLE_NAME
    global DYNAMODB_OPTIONS_CUSTOM_LIST_TABLE_NAME
    global DYNAMODB_STAFF_TABLE_NAME
    global DYNAMODB_OPTIONS_CUSTOM_LIST_STAFF_TABLE_NAME

    args = getResolvedOptions(sys.argv, [
            'DYNAMODB_TENANT_TABLE_NAME',
            'DYNAMODB_CUSTOM_LIST_TABLE_NAME',
            'DYNAMODB_OPTIONS_CUSTOM_LIST_TABLE_NAME',
            'DYNAMODB_STAFF_TABLE_NAME',
            'DYNAMODB_OPTIONS_CUSTOM_LIST_STAFF_TABLE_NAME']
    )

    
    DYNAMODB_TENANT_TABLE_NAME = args['DYNAMODB_TENANT_TABLE_NAME']
    DYNAMODB_CUSTOM_LIST_TABLE_NAME = args['DYNAMODB_CUSTOM_LIST_TABLE_NAME']
    DYNAMODB_OPTIONS_CUSTOM_LIST_TABLE_NAME = args['DYNAMODB_OPTIONS_CUSTOM_LIST_TABLE_NAME']
    DYNAMODB_STAFF_TABLE_NAME = args['DYNAMODB_STAFF_TABLE_NAME']
    DYNAMODB_OPTIONS_CUSTOM_LIST_STAFF_TABLE_NAME = args['DYNAMODB_OPTIONS_CUSTOM_LIST_STAFF_TABLE_NAME']
    # print(f"{args}")
#################################################################################################################################################
#HELPER
def get_dynamo_table(table_name):
    dynamodb = boto3.resource('dynamodb')
    return dynamodb.Table(table_name)
#################################################################################################################################################
#HELPER
def scan_dynamo_table(table_name, scan_kwargs):
    
    table = get_dynamo_table(table_name)

    # Scanning
    complete = False
    items = []
    while not complete:
        try:
            response = table.scan(**scan_kwargs)
            # print('RESPONSE scan:', response)
        except ex.ClientError as error:
            raise Exception(f'Error Scanning Table: {table_name}: {error}')

        items.extend(response.get('Items', []))
        next_key = response.get('LastEvaluatedKey')
        scan_kwargs['ExclusiveStartKey'] = next_key

        # print(f"Items Collected {len(items)}")
        complete = True if next_key is None else False

    return items

#################################################################################################################################################
#HELPER
def getCustomListItems(item):
    customDelVanId = ""
    standardParcelId = ""
    tenkVanId = ""
    standardParcelSmallId = ""
    standardParcelLargeId = ""
    standardParcelXLId = ""
    kwargs = {
        # 'IndexName': "gsi-OptionsCustomLists",
        'ExpressionAttributeNames': {
            "#opcl": "optionsCustomListsCustomListsId",
            "#g": "group",
            "#opt": "option"
        },
        'ExpressionAttributeValues': {
            ":opcl": item["id"],
            ':g': item["group"],
        },
        'FilterExpression': "#g = :g and #opcl = :opcl",
        'ProjectionExpression': 'id, #g, #opt',
    }
    vehicle_type_options = scan_dynamo_table(DYNAMODB_OPTIONS_CUSTOM_LIST_TABLE_NAME, kwargs)
    # print(f"Vehicle type Options: {len(vehicle_type_options)}")
    return vehicle_type_options

###############################################################################################################################################
#HELPER
def getCustomListbyType(group, type):
        kwargs = {
            'ExpressionAttributeNames': {
                '#g': 'group',
                '#t': 'type'
            },
            'ExpressionAttributeValues': {
                ':t': type,
                ':g': group
            },
            'FilterExpression': '#t = :t and #g = :g',
            'ProjectionExpression': 'id, #g, #t'
        }
        vehicle_type = scan_dynamo_table(DYNAMODB_CUSTOM_LIST_TABLE_NAME, kwargs)
        # print(f"getCustomListbyType - Custom List Size: {len(vehicle_type)}")
        return vehicle_type
#################################################################################################################################################
#HELPER
def getVehicleTypeFromCustomList(group):
    customDelVanId = None
    standardParcelId = None
    tenkVanId = None
    standardParcelSmallId = None
    standardParcelLargeId = None
    standardParcelXLId = None

    try:
        vehicleCustomList = getCustomListbyType(group, 'vehicle-type')
        # print({ 'vehicleCustomList': vehicleCustomList })
        options = getCustomListItems(vehicleCustomList[0])
        # print({ 'options': options })
        for item in options:
            if item['option'] == '10,000lbs Van':
                tenkVanId = item['id']
            elif item['option'] == 'Custom Delivery Van':
                customDelVanId = item['id']
            elif item['option'] == 'Standard Parcel':
                standardParcelId = item['id']
            elif item['option'] == 'Standard Parcel Small':
                standardParcelSmallId = item['id']
            elif item['option'] == 'Standard Parcel Large':
                standardParcelLargeId = item['id']
            elif item['option'] == 'Standard Parcel XL':
                standardParcelXLId = item['id']
        result = {
            'tenkVanId': tenkVanId,
            'customDelVanId': customDelVanId,
            'standardParcelId': standardParcelId,
            'standardParcelSmallId': standardParcelSmallId,
            'standardParcelLargeId': standardParcelLargeId,
            'standardParcelXLId': standardParcelXLId
        }
        return result
    except Exception as e:
        print("[getVehicleTypeFromCustomList] Error in function getVehicleTypeFromCustomList", e)

###############################################################################################################################################
#HELPER
def getAllTenants():
    kwargs = {
        'ExpressionAttributeNames': {
            '#g': 'group',
            '#t': 'type',
        },
        'ProjectionExpression': 'id, #g, #t'
    }
    tenant_list = scan_dynamo_table(DYNAMODB_TENANT_TABLE_NAME, kwargs)
    # print(f"tenant list: {len(tenant_list)}")
    return tenant_list
#################################################################################################################################################
#################################################################################################################################################

def getStaffByGroup(group):
    kwargs = {
        'ExpressionAttributeNames': {
            '#g': 'group',
            '#albs': 'authorizedLBS',
            '#cdv': 'customDeliveryVan'
        },
        'ExpressionAttributeValues': {
            ':g': group
        },
        'FilterExpression': "#g = :g",
        'ProjectionExpression' : 'id, #g, #albs, #cdv',
    }
    staff_list = scan_dynamo_table(DYNAMODB_STAFF_TABLE_NAME, kwargs)
    # print(f"staff list: {len(staff_list)}")
    return staff_list
#################################################################################################################################################
#################################################################################################################################################
def set_associates_authorize_to_drive():
    tenants = getAllTenants()
    # print(f"tenants: {tenants}")
    for tenant in tenants:
        try:
            vehicle_types_items = getVehicleTypeFromCustomList(tenant["group"])
            # print(f"vehicle_types_items: {len(vehicle_types_items)}")
            # print(f"vehicle_types_items: {vehicle_types_items}")
            staffs = getStaffByGroup(tenant["group"])
            print(f"staffs: {len(staffs)}")
            options_custom_list_staff_table = get_dynamo_table(DYNAMODB_OPTIONS_CUSTOM_LIST_STAFF_TABLE_NAME)
            # print(f"staff_table: {staff_table}")
            now = datetime.now()
            date_time = now.strftime("%Y-%m-%dT%H:%M:%SZ")
            # print(f"date_time: {date_time}")
            typeList = []
            for staff in staffs:
                print(f"staff: {staff}")

                try:
                    if (not staff.get('customDeliveryVan')) and (not staff.get('authorizedLBS')):
                        typeList = [vehicle_types_items["standardParcelId"], vehicle_types_items["standardParcelSmallId"], vehicle_types_items["standardParcelLargeId"], vehicle_types_items["standardParcelXLId"]]
                    elif staff.get('customDeliveryVan'):
                        typeList = [vehicle_types_items["customDelVanId"]]
                    elif staff.get('authorizedLBS'):
                        typeList = [vehicle_types_items["tenkVanId"]]
                    print(f"typeList: {typeList}")

                    for typeId in typeList:
                        print(f"typeId: {typeId}")
                        result = options_custom_list_staff_table.put_item(
                            Item={
                                'id': str(uuid.uuid4()),
                                'group': tenant["group"],
                                'optionsCustomListsStaffStaffId': staff["id"],
                                'optionsCustomListsStaffOptionCustomListId': typeId,
                                'createdAt': date_time,
                                'updatedAt': date_time
                                }
                        )
                        print(f"result: {result}")
                    # break
                except ex.ClientError as error:
                    raise Exception(f'Error Setting Authorize To Drive in a Staff: {staff}: {error}')
            # break
        except ex.ClientError as error:
            raise Exception(f'Error Setting Associates Authorize To Drive in a Tennat: {tenant}: {error}')
        
#################################################################################################################################################
#################################################################################################################################################

def init():
    load_job_parameters()
###############################################################################################################################################
############################################################# MAIN ############################################################################
if __name__ == "__main__":

    # Init:
    #   - Fetch Client_ID, CLient_SECRET & REFRESH_TOKEN from SSM
    print("****** START JOB ******")
    init()
    set_associates_authorize_to_drive()
###############################################################################################################################################