import sys
import datetime
import pytz
from awsglue.utils import getResolvedOptions
import boto3
import botocore.exceptions as ex
from boto3.dynamodb.conditions import Key, Attr
###################################################*** Global Variables***#######################################################################
DYNAMODB_TENANT_TABLE_NAME = ""
DYNAMODB_ACCIDENT_TABLE_NAME = ""
DYNAMODB_COUNSELING_TABLE_NAME = ""
DYNAMODB_INFRACTION_TABLE_NAME = ""
DYNAMODB_KUDOS_TABLE_NAME = ""
DYNAMODB_VEHICLE_MAINTENANCE_REMINDER_TABLE_NAME = ""
DYNAMODB_VEHICLE_TABLE_NAME = ""
DYNAMODB_DOCUMENT_TABLE_NAME = ""
DYNAMODB_DRUGTEST_TABLE_NAME = ""
DYNAMODB_INJURY_TABLE_NAME = ""
DYNAMODB_ONBOARD_TABLE_NAME = ""
DYNAMODB_PHYSICAL_TABLE_NAME = ""
DYNAMODB_STAFF_TABLE_NAME = ""
DYNAMODB_UNIFORM_TABLE_NAME = ""
#################################################################################################################################################
def get_dynamo_table(table_name):
    dynamodb = boto3.resource('dynamodb')
    return dynamodb.Table(table_name)
#################################################################################################################################################
def handle_timeout_error(last_evaluated_key, table_name):
    if last_evaluated_key:
        print(f"Last pagination before timeout: {last_evaluated_key}")
    else:
        print("No pagination information available before timeout")
    raise Exception(f'Timeout error while scanning the table: {table_name}')
#################################################################################################################################################
def scan_dynamo_table(table_name, scan_kwargs):
    table = get_dynamo_table(table_name)
    # Scanning
    complete = False
    items = []
    next_key_not_None = ''
    while not complete:
        try:
            response = table.scan(**scan_kwargs)
        except ex.ClientError as error:
            if error.response['Error']['Code'] == 'TimeoutException':
                handle_timeout_error(scan_kwargs.get('ExclusiveStartKey'), table_name)
            else:
                raise Exception(f'Error scanning the table: {table_name}: {error}')
        items.extend(response.get('Items', []))
        next_key = response.get('LastEvaluatedKey')
        scan_kwargs['ExclusiveStartKey'] = next_key
        if next_key is not None and len(items) > 0:
            next_key_not_None = next_key
        complete = True if next_key is None else False
    print(f"Items Collected {len(items)}")
    print(f"Last key: {next_key_not_None}")
    return items
#################################################################################################################################################
def update_record(table_name, scan_kwargs):
    table = get_dynamo_table(table_name)
    print(table)
    # Updating a record
    try:
        response = table.update_item(**scan_kwargs)
        print('Success:', response)
    except ex.ClientError as error:
        raise Exception(f'Error Scanning Table: {table_name}: {error}')
#################################################################################################################################################
def load_job_parameters():
    global DYNAMODB_TENANT_TABLE_NAME
    global DYNAMODB_ACCIDENT_TABLE_NAME
    global DYNAMODB_COUNSELING_TABLE_NAME
    global DYNAMODB_INFRACTION_TABLE_NAME
    global DYNAMODB_KUDOS_TABLE_NAME
    global DYNAMODB_VEHICLE_MAINTENANCE_REMINDER_TABLE_NAME
    global DYNAMODB_VEHICLE_TABLE_NAME
    global DYNAMODB_DOCUMENT_TABLE_NAME
    global DYNAMODB_DRUGTEST_TABLE_NAME
    global DYNAMODB_INJURY_TABLE_NAME
    global DYNAMODB_ONBOARD_TABLE_NAME
    global DYNAMODB_PHYSICAL_TABLE_NAME
    global DYNAMODB_STAFF_TABLE_NAME
    global DYNAMODB_UNIFORM_TABLE_NAME
    
    args = getResolvedOptions(sys.argv, [
            'DYNAMODB_TENANT_TABLE_NAME',
            'DYNAMODB_ACCIDENT_TABLE_NAME',
            'DYNAMODB_COUNSELING_TABLE_NAME',
            'DYNAMODB_INFRACTION_TABLE_NAME',
            'DYNAMODB_KUDOS_TABLE_NAME',
            'DYNAMODB_VEHICLE_MAINTENANCE_REMINDER_TABLE_NAME',
            'DYNAMODB_VEHICLE_TABLE_NAME',
            'DYNAMODB_DOCUMENT_TABLE_NAME',
            'DYNAMODB_DRUGTEST_TABLE_NAME',
            'DYNAMODB_INJURY_TABLE_NAME',
            'DYNAMODB_ONBOARD_TABLE_NAME',
            'DYNAMODB_PHYSICAL_TABLE_NAME',
            'DYNAMODB_STAFF_TABLE_NAME',
            'DYNAMODB_UNIFORM_TABLE_NAME'
        ]
    )
    
    DYNAMODB_TENANT_TABLE_NAME = args['DYNAMODB_TENANT_TABLE_NAME']
    DYNAMODB_ACCIDENT_TABLE_NAME = args['DYNAMODB_ACCIDENT_TABLE_NAME']
    DYNAMODB_COUNSELING_TABLE_NAME = args['DYNAMODB_COUNSELING_TABLE_NAME']
    DYNAMODB_INFRACTION_TABLE_NAME = args['DYNAMODB_INFRACTION_TABLE_NAME']
    DYNAMODB_KUDOS_TABLE_NAME = args['DYNAMODB_KUDOS_TABLE_NAME']
    DYNAMODB_VEHICLE_MAINTENANCE_REMINDER_TABLE_NAME = args['DYNAMODB_VEHICLE_MAINTENANCE_REMINDER_TABLE_NAME']
    DYNAMODB_VEHICLE_TABLE_NAME = args['DYNAMODB_VEHICLE_TABLE_NAME']
    DYNAMODB_DOCUMENT_TABLE_NAME = args['DYNAMODB_DOCUMENT_TABLE_NAME']
    DYNAMODB_DRUGTEST_TABLE_NAME = args['DYNAMODB_DRUGTEST_TABLE_NAME']
    DYNAMODB_INJURY_TABLE_NAME = args['DYNAMODB_INJURY_TABLE_NAME']
    DYNAMODB_ONBOARD_TABLE_NAME = args['DYNAMODB_ONBOARD_TABLE_NAME']
    DYNAMODB_PHYSICAL_TABLE_NAME = args['DYNAMODB_PHYSICAL_TABLE_NAME']
    DYNAMODB_STAFF_TABLE_NAME = args['DYNAMODB_STAFF_TABLE_NAME']
    DYNAMODB_UNIFORM_TABLE_NAME = args['DYNAMODB_UNIFORM_TABLE_NAME']
###############################################################################################################################################
def get_data_tenants():
        kwargs = {
            'ExpressionAttributeNames': {
                '#group': 'group',
                '#timeZone': 'timeZone',
            },
            'ProjectionExpression': 'id, #group, #timeZone'
        }
        tenants = scan_dynamo_table(DYNAMODB_TENANT_TABLE_NAME, kwargs)
        print(f"Tenant Size: {len(tenants)}")
        return tenants
###############################################################################################################################################
def setDateTablesTenantTimezone(table,field, vehicle_history_type=None):
    print("####################################################")
    print(f"Table: {table}")
    print(f"Field: {field}")
    if vehicle_history_type:
        print(f"Vehicle_History_Type: {vehicle_history_type}")
    tenants = get_data_tenants()
    for tenant in tenants:
        try:
            # Get all tenants
            print("---------------------------------------------------")
            print(f"Timezone: {tenant['timeZone']}")
            print(f"Group: {tenant['group']}")
            items = update_items_tenant(tenant, table, field, vehicle_history_type);
        except ex.ClientError as error:
            raise Exception(f'Error: {error}')
###############################################################################################################################################
def update_items_tenant(tenant, table, field, vehicle_history_type=None):
    validator_date = getLocalTimeForCurrentDay(tenant['timeZone'])
    print(f'Date: {validator_date}')
    kwargs = {
                'ExpressionAttributeNames': {
                    '#group': 'group',
                    f'#{field}': field
                },
                'ExpressionAttributeValues': {
                    ':group': tenant['group'],
                    ':empty': "",
                    ':null': None,
                    f':{field}': validator_date
                },
                'FilterExpression': f'#group = :group and not contains(#{field}, :{field}) and attribute_exists(#{field}) and #{field} <> :empty and #{field} <> :null',
                'ProjectionExpression': f'id, #group, #{field}'
            }

    if vehicle_history_type:
        kwargs['ExpressionAttributeNames']['#vehicleHistoryType'] = 'vehicleHistoryType'
        kwargs['ExpressionAttributeValues'][':vehicleHistoryType'] = vehicle_history_type
        kwargs['FilterExpression'] += ' and #vehicleHistoryType = :vehicleHistoryType'
    
    items = scan_dynamo_table(table, kwargs)
    if len(items) <= 0: 
        return []
            
    items_to_clean = clean_items(items, validator_date, table, field) 
    
    return items_to_clean
###############################################################################################################################################
def clean_items(items, validator_date, table, field):
    itemsToClean = []
    for item in items:
        if item.get(field):
            date = item[field].split('T')[0]
            item[field] = f"{date}T{validator_date}"

        itemsToClean.append(item)
        update_item(item, table, field)

    return itemsToClean
###############################################################################################################################################
def update_item(item, table, field):
    kwargs = {
                'Key': { "id": item["id"] },
                'ExpressionAttributeNames': {
                    f'#{field}': field,
                },
                'ExpressionAttributeValues': {
                    f':{field}': item[field]
                },
                'UpdateExpression': f'SET #{field} = :{field}',
            }
    update_record(table, kwargs)
###############################################################################################################################################
def getLocalTimeForCurrentDay(timezone):
    timestamp = datetime.datetime.now(pytz.timezone(timezone)).replace(hour=0, minute=0, second=0, microsecond=0)
    convertTimestamp = timestamp.astimezone(pytz.UTC).strftime("%H:%M:%S.%f")[:-3] + "Z"
    return convertTimestamp
###############################################################################################################################################
def maintenanceTableTenantTimezone():
    tables = [
        #MAINTENANCE HERA-2025
        {"table":DYNAMODB_KUDOS_TABLE_NAME,"field":"date"},
        {"table":DYNAMODB_INFRACTION_TABLE_NAME,"field":"date"},
        {"table":DYNAMODB_COUNSELING_TABLE_NAME,"field":"date"},
        #MAINTENANCE HERA-2026
        {"table":DYNAMODB_DOCUMENT_TABLE_NAME,"field":"documentDate"},
        {"table":DYNAMODB_DRUGTEST_TABLE_NAME,"field":"date"},
        {"table":DYNAMODB_INJURY_TABLE_NAME,"field":"dateOfDeath"},
        {"table":DYNAMODB_INJURY_TABLE_NAME,"field":"driverDOB"},
        {"table":DYNAMODB_INJURY_TABLE_NAME,"field":"driverHireDate"},
        {"table":DYNAMODB_INJURY_TABLE_NAME,"field":"injuryDate"},
        {"table":DYNAMODB_ONBOARD_TABLE_NAME,"field":"date3"},
        {"table":DYNAMODB_ONBOARD_TABLE_NAME,"field":"date4"},
        {"table":DYNAMODB_ONBOARD_TABLE_NAME,"field":"date5"},
        {"table":DYNAMODB_ONBOARD_TABLE_NAME,"field":"dateComplete"},
        {"table":DYNAMODB_ONBOARD_TABLE_NAME,"field":"dateStart"},
        {"table":DYNAMODB_PHYSICAL_TABLE_NAME,"field":"date"},
        {"table":DYNAMODB_PHYSICAL_TABLE_NAME,"field":"expirationDate"},
        {"table":DYNAMODB_STAFF_TABLE_NAME,"field":"dob"},
        {"table":DYNAMODB_STAFF_TABLE_NAME,"field":"dlExpiration"},
        {"table":DYNAMODB_STAFF_TABLE_NAME,"field":"hireDate"},
        {"table":DYNAMODB_UNIFORM_TABLE_NAME,"field":"issueDate"},
        {"table":DYNAMODB_UNIFORM_TABLE_NAME,"field":"returnedDate"},
        #MAINTENANCE HERA-2027
        {"table":DYNAMODB_ACCIDENT_TABLE_NAME,"field":"accidentDate","vehicle_history_type":"Vehicle Damage"},
        {"table":DYNAMODB_ACCIDENT_TABLE_NAME,"field":"accidentDate","vehicle_history_type":"Maintenance"},
        {"table":DYNAMODB_ACCIDENT_TABLE_NAME,"field":"accidentDate","vehicle_history_type":"Accident"},
        {"table":DYNAMODB_ACCIDENT_TABLE_NAME,"field":"drugTestDate","vehicle_history_type":"Accident"},
        {"table":DYNAMODB_ACCIDENT_TABLE_NAME,"field":"verifiedDate","vehicle_history_type":"Accident"},
        {"table":DYNAMODB_VEHICLE_MAINTENANCE_REMINDER_TABLE_NAME,"field":"dueByDate"},
        {"table":DYNAMODB_VEHICLE_TABLE_NAME,"field":"dateEnd"},
        {"table":DYNAMODB_VEHICLE_TABLE_NAME,"field":"dateStart"},
        {"table":DYNAMODB_VEHICLE_TABLE_NAME,"field":"licensePlateExp"}
    ]
    for table in tables:
        if table.get("vehicle_history_type"):
            setDateTablesTenantTimezone(table["table"],table["field"],table["vehicle_history_type"])
        else:
            setDateTablesTenantTimezone(table["table"],table["field"])
###############################################################################################################################################
def init():
    print("-----Start work------")
    load_job_parameters()
    maintenanceTableTenantTimezone()
###############################################################################################################################################
if __name__ == "__main__":
    # Init:
    #   - Fetch Client_ID, CLient_SECRET & REFRESH_TOKEN from SSM
    #   - Generate fresh ACCESS_TOKEN
    init()