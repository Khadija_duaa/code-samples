import boto3
import re
import uuid
import sys
from datetime import datetime
import botocore.exceptions as ex
from awsglue.utils import getResolvedOptions
from boto3.dynamodb.conditions import Key, Attr

###################################################*** Global Variables***#######################################################################
dynamodb = boto3.resource('dynamodb')

DYNAMODB_STAFF_TABLE_NAME = ""
DYNAMODB_MESSAGEPREFERENCESHISTORY_TABLE_NAME = ""

#SMS/EMAIL for MessagePreferenceType of the MessagePreferencesHistory table
SMS = 'SMS'
EMAIL = 'EMAIL'

#################################################################################################################################################
def load_job_parameters():
    global DYNAMODB_STAFF_TABLE_NAME
    global DYNAMODB_MESSAGEPREFERENCESHISTORY_TABLE_NAME
    args = getResolvedOptions(sys.argv, [
        'DYNAMODB_STAFF_TABLE_NAME',
        'DYNAMODB_MESSAGEPREFERENCESHISTORY_TABLE_NAME',
    ])    
    DYNAMODB_STAFF_TABLE_NAME = args['DYNAMODB_STAFF_TABLE_NAME']
    DYNAMODB_MESSAGEPREFERENCESHISTORY_TABLE_NAME = args['DYNAMODB_MESSAGEPREFERENCESHISTORY_TABLE_NAME']


#################################################################################################################################################
def is_valid_timestamp(timestamp):
    timestamp_pattern = r'^(\d{1,2}/\d{1,2}/\d{4}, \d{1,2}:\d{1,2}:\d{1,2}( [APap][Mm])?)$'
    if not re.match(timestamp_pattern, timestamp):
        return False

    try:
        datetime.strptime(timestamp, '%m/%d/%Y, %I:%M:%S %p')
        return True
    except ValueError:
        try:
            datetime.strptime(timestamp, '%m/%d/%Y, %H:%M:%S')
            return True
        except ValueError:
            return False

#################################################################################################################################################
def generate_uuid():
    return str(uuid.uuid4())

#################################################################################################################################################
def create_dynamo_record(params):
    try:
        table = dynamodb.Table(params['TableName'])
        table.put_item(Item=params['Item'])
    except Exception as e:
        print("Unable to create item. Error:", e)

#################################################################################################################################################
def create_message_preferences_history(params):
    try:
        formatted_datetime = datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z'
        input = {
            'TableName': DYNAMODB_MESSAGEPREFERENCESHISTORY_TABLE_NAME,
            'Item': {
                '__typename': 'MessagePreferencesHistory',
                'id': generate_uuid(),
                'group': params['group'],
                'messagePreferenceType': params['messagePreferenceType'],
                'messagePreferencesHistoryStaffId': params['messagePreferencesHistoryStaffId'],
                'description': params['description'],
                'datetime': params['datetime'],
                'createdAt': formatted_datetime,
                'updatedAt': formatted_datetime
            }
        }
        create_dynamo_record(input)
    except Exception as e:
        print('Error in create_message_preferences_history', e)

#################################################################################################################################################
def create_object_log(staff, log_selected, type):
    value = staff.get(log_selected, None)
    if value is None or (isinstance(value, str) and value == ''):
        return
    
    split_log = value.split('<br/>')
    log_filtered = [item for item in split_log if len(item) > 0]

    for log in log_filtered:
        arr = re.match(r'^(.*?) at (.*)$', log)
        if arr:
            description = arr.group(1).replace('null', '')
            timestamp = arr.group(2)

            if description and timestamp and is_valid_timestamp(timestamp):
                formatted_datetime = datetime.strptime(timestamp, '%m/%d/%Y, %I:%M:%S %p' if 'AM' in timestamp or 'PM' in timestamp else '%m/%d/%Y, %H:%M:%S').strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z'
                new_log = {
                    'datetime': formatted_datetime,
                    'description': description,
                    'messagePreferencesHistoryStaffId': staff['id'],
                    'group': staff['group'],
                    'messagePreferenceType': type
                }
                #createDynamoRecord
                create_message_preferences_history(new_log)
            else:
                print('skip: invalid date')
        else:
            print('skip')


#################################################################################################################################################
def get_staffs():
    table_name = DYNAMODB_STAFF_TABLE_NAME
    try:
        last_evaluated_key = None
        params = {
            'TableName': table_name,
            'ExpressionAttributeNames': {
                '#id': 'id',
                '#group': 'group'
            },
            'ProjectionExpression': '#id, #group, receiveTextMessagesChangeLog, receiveEmailMessagesChangeLog',
            'FilterExpression': 'attribute_exists(receiveTextMessagesChangeLog) or attribute_exists(receiveEmailMessagesChangeLog)',
        }
        table = dynamodb.Table(table_name)
       
        while True:
            response = table.scan(**params)
            last_evaluated_key = response.get('LastEvaluatedKey')
            params['ExclusiveStartKey'] = last_evaluated_key
            staffs = response.get('Items', [])
            print('LastEvaluatedKey:', last_evaluated_key)
            for staff in staffs:
                create_object_log(staff, 'receiveTextMessagesChangeLog', SMS)
                create_object_log(staff, 'receiveEmailMessagesChangeLog', EMAIL)
            if last_evaluated_key is None:
                break
    except Exception as e:
        print('Error in get_staffs', e)


###############################################################################################################################################
def handler_hera2429():
    try:
        # Uncomment the following line to run the script
        get_staffs()
    finally:
        print('***************************')
        print('Script Finished')
        print('***************************')

###############################################################################################################################################
def init():
    print("-----Start work------")
    load_job_parameters()
    # uncomment to run the script
    #handler_hera2429()

###############################################################################################################################################
if __name__ == "__main__":
    # Init:
    #   - Fetch Client_ID, CLient_SECRET & REFRESH_TOKEN from SSM
    #   - Generate fresh ACCESS_TOKEN
    init()