import sys
import boto3
import botocore.exceptions as ex
from boto3.dynamodb.conditions import Attr
from awsglue.utils import getResolvedOptions
from boto3.dynamodb.conditions import Key, Attr
from pprint import pprint

###################################################*** Global Variables***#######################################################################
DYNAMODB_VEHICLE_TABLE_NAME = ""
DYNAMODB_TENANT_TABLE_NAME = ""
DYNAMODB_CUSTOM_LIST_TABLE_NAME = ""
DYNAMODB_OPTIONS_CUSTOM_LIST_TABLE_NAME = ""

#################################################################################################################################################
#HELPER
def get_dynamo_table(table_name):
    dynamodb = boto3.resource('dynamodb')
    return dynamodb.Table(table_name)
#################################################################################################################################################
#HELPER
def scan_dynamo_table(table_name, scan_kwargs):
    
    table = get_dynamo_table(table_name)

    # Scanning
    complete = False
    items = []
    while not complete:
        try:
            response = table.scan(**scan_kwargs)
            # print('RESPONSE scan:', response)
        except ex.ClientError as error:
            raise Exception(f'Error Scanning Table: {table_name}: {error}')

        items.extend(response.get('Items', []))
        next_key = response.get('LastEvaluatedKey')
        scan_kwargs['ExclusiveStartKey'] = next_key

        # print(f"Items Collected {len(items)}")
        complete = True if next_key is None else False

    return items

#################################################################################################################################################
def load_job_parameters():
    global DYNAMODB_TENANT_TABLE_NAME
    global DYNAMODB_VEHICLE_TABLE_NAME
    global DYNAMODB_CUSTOM_LIST_TABLE_NAME
    global DYNAMODB_OPTIONS_CUSTOM_LIST_TABLE_NAME
    
    args = getResolvedOptions(sys.argv, [
            'DYNAMODB_TENANT_TABLE_NAME',
            'DYNAMODB_VEHICLE_TABLE_NAME',
            'DYNAMODB_CUSTOM_LIST_TABLE_NAME',
            'DYNAMODB_OPTIONS_CUSTOM_LIST_TABLE_NAME']
    )
    
    DYNAMODB_VEHICLE_TABLE_NAME = args['DYNAMODB_VEHICLE_TABLE_NAME']
    DYNAMODB_TENANT_TABLE_NAME = args['DYNAMODB_TENANT_TABLE_NAME']
    DYNAMODB_CUSTOM_LIST_TABLE_NAME = args['DYNAMODB_CUSTOM_LIST_TABLE_NAME']
    DYNAMODB_OPTIONS_CUSTOM_LIST_TABLE_NAME = args['DYNAMODB_OPTIONS_CUSTOM_LIST_TABLE_NAME']

###############################################################################################################################################
def get_vehicles_by_group(group):
        kwargs = {
            'ExpressionAttributeNames': {
                '#g': 'group',
                '#cdv': 'customDeliveryVan',
                '#ottp': 'overTenThousandPounds',
            },
            'ExpressionAttributeValues': {
                ':g': group
            },
            'FilterExpression': '#g = :g',
            'ProjectionExpression': 'id, #g, #cdv, #ottp'
        }
        vehicle_list = scan_dynamo_table(DYNAMODB_VEHICLE_TABLE_NAME, kwargs)
        # print("vehicle_list======", vehicle_list)
        # print(f"get_vehicles_by_group - Custom List Size: {len(vehicle_list)}")
        return vehicle_list
#################################################################################################################################################
#HELPER
def getCustomListItems(item):
    customDelVanId = ""
    standardParcelId = ""
    tenkVanId = ""
    standardParcelSmallId = ""
    standardParcelLargeId = ""
    standardParcelXLId = ""
    kwargs = {
        # 'IndexName': "gsi-OptionsCustomLists",
        'ExpressionAttributeNames': {
            "#opcl": "optionsCustomListsCustomListsId",
            "#g": "group",
            "#opt": "option"
        },
        'ExpressionAttributeValues': {
            ":opcl": item["id"],
            ':g': item["group"],
        },
        'FilterExpression': "#g = :g and #opcl = :opcl",
        'ProjectionExpression': 'id, #g, #opt',
    }
    # print('KARGS:', kwargs)
    vehicle_type_options = scan_dynamo_table(DYNAMODB_OPTIONS_CUSTOM_LIST_TABLE_NAME, kwargs)
    # print(f"Vehicle type Options: {len(vehicle_type_options)}")
    return vehicle_type_options

###############################################################################################################################################
#HELPER
def getCustomListbyType(group, type):
        kwargs = {
            'ExpressionAttributeNames': {
                '#g': 'group',
                '#t': 'type'
            },
            'ExpressionAttributeValues': {
                ':t': type,
                ':g': group
            },
            'FilterExpression': '#t = :t and #g = :g',
            'ProjectionExpression': 'id, #g, #t'
        }
        vehicle_type = scan_dynamo_table(DYNAMODB_CUSTOM_LIST_TABLE_NAME, kwargs)
        print(f"getCustomListbyType - Custom List Size: {len(vehicle_type)}")
        return vehicle_type
#################################################################################################################################################
#HELPER
def getVehicleTypeFromCustomList(group):
    customDelVanId = None
    standardParcelId = None
    tenkVanId = None
    standardParcelSmallId = None
    standardParcelLargeId = None
    standardParcelXLId = None

    try:
        vehicleCustomList = getCustomListbyType(group, 'vehicle-type')
        options = getCustomListItems(vehicleCustomList[0])
        for item in options:
            if item['option'] == '10,000lbs Van':
                tenkVanId = item['id']
            elif item['option'] == 'Custom Delivery Van':
                customDelVanId = item['id']
            elif item['option'] == 'Standard Parcel':
                standardParcelId = item['id']
            elif item['option'] == 'Standard Parcel Small':
                standardParcelSmallId = item['id']
            elif item['option'] == 'Standard Parcel Large':
                standardParcelLargeId = item['id']
            elif item['option'] == 'Standard Parcel XL':
                standardParcelXLId = item['id']
        result = {
            'tenkVanId': tenkVanId,
            'customDelVanId': customDelVanId,
            'standardParcelId': standardParcelId,
            'standardParcelSmallId': standardParcelSmallId,
            'standardParcelLargeId': standardParcelLargeId,
            'standardParcelXLId': standardParcelXLId
        }
        print("vehicle type id per tenant", result)
        return result
    except Exception as e:
        # print("[getVehicleTypeFromCustomList] Error in function getVehicleTypeFromCustomList", e)
        return {
            'error': e
        }

###############################################################################################################################################
#HELPER
def getAllTenants():
        kwargs = {
            'ExpressionAttributeNames': {
                '#g': 'group',
                '#t': 'type',
            },
            'ProjectionExpression': 'id, #g, #t'
        }
        tenant_list = scan_dynamo_table(DYNAMODB_TENANT_TABLE_NAME, kwargs)
        # print(f"tenant list: {len(tenant_list)}")
        return tenant_list

#################################################################################################################################################
#HELPER
def update_record(table_name, scan_kwargs):
    
    table = get_dynamo_table(table_name)
    
    # print(table)
    # Updating a record
    try:
        response = table.update_item(**scan_kwargs)
        print('Success:', response)
    except ex.ClientError as error:
        raise Exception(f'Error Scanning Table: {table_name}: {error}')

###############################################################################################################################################
def getAllVehicleTypeCustomList():
        kwargs = {
            'ExpressionAttributeNames': {
                '#g': 'group',
                '#t': 'type',
                '#v' : 'vin',
                '#s': 'status',
                '#lor': 'lastOdometerReadingDate',
            },
            'ExpressionAttributeValues': {
                ':t': 'vehicle-type'
            },
            'FilterExpression': '#t = :t',
            'ProjectionExpression': 'id, #g, #t, #v, #s, #lor'
        }
        vehicle_type = scan_dynamo_table(DYNAMODB_CUSTOM_LIST_TABLE_NAME, kwargs)
        # print(f"Custom List Size: {len(custom_list)}")
        return vehicle_type


###############################################################################################################################################
def set_vehicle_type_custom_list():
    tenants = getAllTenants()
    for tenant in tenants:
        print("tenant ===", tenant)
        # return
        try: 
            vehicle_type_ids = getVehicleTypeFromCustomList(tenant["group"])
            print("vehicle_type_item", vehicle_type_ids)
            
            vehicles = get_vehicles_by_group(tenant["group"])
            print("\n")
            print("Vehicles before loop:\n")
            print(vehicles)
            print("\n")
            # print("vehicles", vehicles)
        except Exception as e:
            print("[get vehicle type] Error 1 in function setVehicleType", e)
            continue        
        
        # 'tenkVanId': tenkVanId,
        # 'customDelVanId': customDelVanId,
        # 'standardParcelId': standardParcelId,
        # 'standardParcelSmallId': standardParcelSmallId,
        # 'standardParcelLargeId': standardParcelLargeId,
        # 'standardParcelXLId': standardParcelXLId

        standard_parcel_id = vehicle_type_ids['standardParcelId']
        custom_del_van_id = vehicle_type_ids['customDelVanId']
        tenk_van_id = vehicle_type_ids['tenkVanId']

        for vehicle in vehicles:
            print("====vehicle before update====", vehicle)
            type_id = standard_parcel_id
            if (vehicle.get('customDeliveryVan') and vehicle.get('overTenThousandPounds') and vehicle['customDeliveryVan'] and vehicle['overTenThousandPounds']):
                type_id = custom_del_van_id
                print("will be update at customDeliveryVan")
            elif (vehicle.get('customDeliveryVan') and vehicle["customDeliveryVan"]):
                type_id = custom_del_van_id
                print("will be update at customDeliveryVan")
            elif (vehicle.get('overTenThousandPounds') and vehicle["overTenThousandPounds"]):
                type_id = tenk_van_id
                print("will be update at overTenThousandPounds")
            else:
                print("will be update at standardParcel")

            print({"typeId": type_id})
            try:
                kwargs = {
                        'Key': {
                            "id": vehicle["id"],
                        },
                        'ExpressionAttributeNames': {
                            "#vtId": "vehicleVehicleTypeId",
                        },
                        'ExpressionAttributeValues': {
                            ":vtId": type_id
                        },
                        'UpdateExpression': "set #vtId=:vtId",
                    }

                print("------UPDATE--------", kwargs)
                update_record(DYNAMODB_VEHICLE_TABLE_NAME, kwargs)
                print("------VEHICLE UPDATE SUCCESSFULLY--------")
            except Exception as e:
                print("[UPDATE-setVehicleType] Error 2 in function setVehicleType", e)

###############################################################################################################################################
def init():
    print("****** START JOB ******")
    load_job_parameters()
    # print("DYNAMODB_VEHICLE_TABLE_NAME:", DYNAMODB_VEHICLE_TABLE_NAME)
    # print("DYNAMODB_TENANT_TABLE_NAME:", DYNAMODB_TENANT_TABLE_NAME)
    # print("DYNAMODB_CUSTOM_LIST_TABLE_NAME:", DYNAMODB_CUSTOM_LIST_TABLE_NAME)
    # print("DYNAMODB_OPTIONS_CUSTOM_LIST_TABLE_NAME:", DYNAMODB_OPTIONS_CUSTOM_LIST_TABLE_NAME)



###############################################################################################################################################

if __name__ == "__main__":

    # Init:
    #   - Fetch Client_ID, CLient_SECRET & REFRESH_TOKEN from SSM
    #   - Generate fresh ACCESS_TOKEN
    hi = {
        "hi": 233,
        "hello": 32432,
    }
    pprint(hi)
    init()
    set_vehicle_type_custom_list()
###############################################################################################################################################