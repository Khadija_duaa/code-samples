import sys
import boto3
import botocore.exceptions as ex
from boto3.dynamodb.conditions import Attr
from awsglue.utils import getResolvedOptions
from boto3.dynamodb.conditions import Key, Attr
from pprint import pprint

###################################################*** Global Variables***#######################################################################
DYNAMODB_ACCIDENT_TABLE_NAME = ""
DYNAMODB_TENANT_TABLE_NAME = ""
DYNAMODB_CUSTOM_LIST_TABLE_NAME = ""
DYNAMODB_OPTIONS_CUSTOM_LIST_TABLE_NAME = ""
TEST_MODE = False
ONLY_FOR_GROUP = ""

#################################################################################################################################################
#HELPER
def get_dynamo_table(table_name):
    dynamodb = boto3.resource('dynamodb')
    return dynamodb.Table(table_name)
#################################################################################################################################################
#HELPER
def scan_dynamo_table(table_name, scan_kwargs):
    
    table = get_dynamo_table(table_name)

    # Scanning
    complete = False
    items = []
    while not complete:
        try:
            response = table.scan(**scan_kwargs)
            # print('RESPONSE scan:', response)
        except ex.ClientError as error:
            raise Exception(f'Error Scanning Table: {table_name}: {error}')

        items.extend(response.get('Items', []))
        next_key = response.get('LastEvaluatedKey')
        scan_kwargs['ExclusiveStartKey'] = next_key

        # print(f"Items Collected {len(items)}")
        complete = True if next_key is None else False

    return items

#################################################################################################################################################
def load_job_parameters():
    global DYNAMODB_TENANT_TABLE_NAME
    global DYNAMODB_ACCIDENT_TABLE_NAME
    global DYNAMODB_CUSTOM_LIST_TABLE_NAME
    global DYNAMODB_OPTIONS_CUSTOM_LIST_TABLE_NAME
    
    args = getResolvedOptions(sys.argv, [
            'DYNAMODB_TENANT_TABLE_NAME',
            'DYNAMODB_ACCIDENT_TABLE_NAME',
            'DYNAMODB_CUSTOM_LIST_TABLE_NAME',
            'DYNAMODB_OPTIONS_CUSTOM_LIST_TABLE_NAME']
    )
    
    DYNAMODB_ACCIDENT_TABLE_NAME = args['DYNAMODB_ACCIDENT_TABLE_NAME']
    DYNAMODB_TENANT_TABLE_NAME = args['DYNAMODB_TENANT_TABLE_NAME']
    DYNAMODB_CUSTOM_LIST_TABLE_NAME = args['DYNAMODB_CUSTOM_LIST_TABLE_NAME']
    DYNAMODB_OPTIONS_CUSTOM_LIST_TABLE_NAME = args['DYNAMODB_OPTIONS_CUSTOM_LIST_TABLE_NAME']

###############################################################################################################################################
def get_accidents_by_group(group):
        kwargs = {
             'ExpressionAttributeNames': {
                "#id": "id",
                "#g": "group",
                "#type": "vehicleHistoryType",
                "#accidentId": "accidentOptionCustomListId",
                },
                'ExpressionAttributeValues': {
                    ":g": group,
                    ":type": "Accident",
                    ":null": None,
                    ":empty": "",
                },
                'FilterExpression': "#g = :g and #type=:type and (attribute_not_exists(#accidentId) or #accidentId=:null or #accidentId=:empty or attribute_not_exists(#accidentId))",
                'ProjectionExpression': '#id, #g',
        }
        accident_list = scan_dynamo_table(DYNAMODB_ACCIDENT_TABLE_NAME, kwargs)
        # print("=======accident_list======", accident_list)
        # print(f"get_accidents_by_group - Custom List Size: {len(accident_list)}")
        return accident_list
#################################################################################################################################################
#HELPER
def getOptionCustomListItems(item):

    kwargs = {
        # 'IndexName': "gsi-OptionsCustomLists",
        'ExpressionAttributeNames': {
            "#opcl": "optionsCustomListsCustomListsId",
            "#g": "group",
            "#opt": "option"
        },
        'ExpressionAttributeValues': {
            ":opcl": item["id"],
            ':g': item["group"],
        },
        'FilterExpression': "#g = :g and #opcl = :opcl",
        'ProjectionExpression': 'id, #g, #opt',
    }
    # print('KARGS:', kwargs)
    try:
        incident_type_options = scan_dynamo_table(DYNAMODB_OPTIONS_CUSTOM_LIST_TABLE_NAME, kwargs)
        
        # print(f"Incident type Options: {len(incident_type_options)}")

        return incident_type_options
    except Exception as e:
        print("[getOptionCustomListItems] Error in function getOptionCustomListItems", e)
        return {
            'error': e
        }

    

###############################################################################################################################################
#HELPER
def getCustomListbyType(group, type):
    try:
        kwargs = {
            'ExpressionAttributeNames': {
                '#g': 'group',
                '#t': 'type'
            },
            'ExpressionAttributeValues': {
                ':t': type,
                ':g': group
            },
            'FilterExpression': '#t = :t and #g = :g',
            'ProjectionExpression': 'id, #g, #t'
        }
        incident_type = scan_dynamo_table(DYNAMODB_CUSTOM_LIST_TABLE_NAME, kwargs)
        # print(f"getCustomListbyType - Custom List Size: {len(incident_type)}")
        return incident_type
    except Exception as e:
        print("[getIncidentTypeFromCustomList] Error in function getIncidentTypeFromCustomList", e)
        return []
#################################################################################################################################################
#HELPER
def getIncidentTypeFromCustomList(group):
    accident = None
    generalIncident = None

    try:
        incidentCustomList = getCustomListbyType(group, 'incident-type')
        options = getOptionCustomListItems(incidentCustomList[0])
        for item in options:
            if item['option'] == 'Accident':
                accident = item['id']
            elif item['option'] == 'General Incident':
                generalIncident = item['id']

        result = {
            'accident': accident,
            'generalIncident': generalIncident
        }
        print("result of accident type:", result)
        return result
    except Exception as e:
        print("[getIncidentTypeFromCustomList] Error in function getIncidentTypeFromCustomList", e)
        return {
            'error': e
        }

###############################################################################################################################################
#HELPER
def getAllTenants():
        kwargs = {
            'ExpressionAttributeNames': {
                '#g': 'group',
                '#t': 'type',
            },
            'ProjectionExpression': 'id, #g, #t'
        }
        tenant_list = scan_dynamo_table(DYNAMODB_TENANT_TABLE_NAME, kwargs)
        # print(f"tenant list: {len(tenant_list)}")
        return tenant_list

#################################################################################################################################################
#HELPER
def update_record(table_name, scan_kwargs):
    
    table = get_dynamo_table(table_name)
    
    # Updating a record
    try:
        response = table.update_item(**scan_kwargs)
        print('Success:', response)
    except ex.ClientError as error:
        raise Exception(f'Error Scanning Table: {table_name}: {error}')

###############################################################################################################################################
def getAllVehicleTypeCustomList():
        kwargs = {
            'ExpressionAttributeNames': {
                '#g': 'group',
                '#t': 'type',
                '#v' : 'vin',
                '#s': 'status',
                '#lor': 'lastOdometerReadingDate',
            },
            'ExpressionAttributeValues': {
                ':t': 'incident-type'
            },
            'FilterExpression': '#t = :t',
            'ProjectionExpression': 'id, #g, #t, #v, #s, #lor'
        }
        incident_type = scan_dynamo_table(DYNAMODB_CUSTOM_LIST_TABLE_NAME, kwargs)
        return incident_type

####################################################################################################################################
def update_accident_of_tenants(tenants):
    for tenant in tenants:
        print("group ===", tenant["group"])
        # get accidents
        
        try:
            incident_type_ids = getIncidentTypeFromCustomList(tenant["group"])
            accidents = get_accidents_by_group(tenant["group"])
            print("\n")
            print("accidents to update :", accidents)
            print("\n")
        except Exception as e:
            print("[get accidents] Error 1 in function get_accidents_by_group", e)
            continue      
        
        #set variables
        accident_id = incident_type_ids['accident']

        #update accident table
        for accident in accidents:
            type_id = accident_id
            print({"typeId": type_id})
            try:
                kwargs = {
                        'Key': {
                            "id": accident["id"],
                        },
                        'ExpressionAttributeNames': {
                            "#incidentTypeId": "accidentOptionCustomListId",
                        },
                        'ExpressionAttributeValues': {
                            ":incidentTypeId": type_id
                        },
                        'UpdateExpression': "set #incidentTypeId = :incidentTypeId",
                    }

                print("------UPDATE ACCIDENT = --------", accident)
                print("------UPDATE--------", kwargs)
                if TEST_MODE == False:
                    update_record(DYNAMODB_ACCIDENT_TABLE_NAME, kwargs)
                    print("------ACCIDENT UPDATE SUCCESSFULLY--------")
            except Exception as e:
                print("[UPDATE-setVehicleType] Error 2 in function setVehicleType", e)
###############################################################################################################################################
def scan_tenants_and_update_accidents():

    table_name = DYNAMODB_TENANT_TABLE_NAME
    scan_kwargs = {
            'ExpressionAttributeNames': {
                '#g': 'group',
                '#t': 'type',
            },
            'ProjectionExpression': 'id, #g, #t'
        }

    ##### GET TENANTS ######

    table = get_dynamo_table(table_name)
    complete = False
    items = []
    counter = 0

    ################# CASE FOR ONLY ONE TENANT ####################
    if not ONLY_FOR_GROUP == "":
        print(f"================== UPDATE ONLY FOR GROUP = {ONLY_FOR_GROUP} ==============")
        loopTenant = [{
            "group": ONLY_FOR_GROUP
        }]
        update_accident_of_tenants(loopTenant)
        return loopTenant
    
    ################# CASE FOR UPDATE ALL TENANTS ####################
    print("================== UPDATING FOR ALL GROUPS  ==============")
    while not complete:
        try:
            response = table.scan(**scan_kwargs)
            # print('RESPONSE scan:', response)
        except ex.ClientError as error:
            raise Exception(f'Error Scanning Table: {table_name}: {error}')

        loopTenant = response.get('Items', [])
        

        ######## UPDATE ###########
        update_accident_of_tenants(loopTenant)

        items.extend(loopTenant)
        next_key = response.get('LastEvaluatedKey')
        scan_kwargs['ExclusiveStartKey'] = next_key

        counter = counter + 1
        if TEST_MODE == True:
            if counter == 3:
                break
        complete = True if next_key is None else False

    return items

###############################################################################################################################################
def init():
    print("****** START JOB ******")
    load_job_parameters()

###############################################################################################################################################

if __name__ == "__main__":

    # Init:
    #   - Fetch Client_ID, CLient_SECRET & REFRESH_TOKEN from SSM
    #   - Generate fresh ACCESS_TOKEN
    init()
    items = scan_tenants_and_update_accidents()
    print("TENANTS THAT WERE UPDATED = ", items)
###############################################################################################################################################