import sys
import boto3
import botocore.exceptions as ex
from boto3.dynamodb.conditions import Attr
from awsglue.utils import getResolvedOptions
from boto3.dynamodb.conditions import Key, Attr

###################################################*** Global Variables***#######################################################################
DYNAMODB_ROUTE_TABLE_NAME = ""
DYNAMODB_CUSTOM_LIST_TABLE_TABLE_NAME = ""
DYNAMODB_OPTIONS_CUSTOM_LIST_TABLE_TABLE_TABLE_NAME = ""
#################################################################################################################################################
def get_dynamo_table(table_name):
    dynamodb = boto3.resource('dynamodb')
    return dynamodb.Table(table_name)
#################################################################################################################################################
def scan_dynamo_table(table_name, scan_kwargs):
    
    table = get_dynamo_table(table_name)

    # Scanning
    complete = False
    items = []
    while not complete:
        try:
            response = table.scan(**scan_kwargs)
            # print('RESPONSE scan:', response)
        except ex.ClientError as error:
            raise Exception(f'Error Scanning Table: {table_name}: {error}')

        items.extend(response.get('Items', []))
        next_key = response.get('LastEvaluatedKey')
        scan_kwargs['ExclusiveStartKey'] = next_key

        # print(f"Items Collected {len(items)}")
        complete = True if next_key is None else False

    return items
#################################################################################################################################################
def update_record(table_name, scan_kwargs):
    
    table = get_dynamo_table(table_name)
    
    # print(table)
    # Updating a record
    try:
        response = table.update_item(**scan_kwargs)
        print('Success:', response)
    except ex.ClientError as error:
        raise Exception(f'Error Scanning Table: {table_name}: {error}')

#################################################################################################################################################
def load_job_parameters():
    global DYNAMODB_ROUTE_TABLE_NAME
    global DYNAMODB_CUSTOM_LIST_TABLE_TABLE_NAME
    global DYNAMODB_OPTIONS_CUSTOM_LIST_TABLE_TABLE_TABLE_NAME
    
    args = getResolvedOptions(sys.argv, [
            'DYNAMODB_ROUTE_TABLE_NAME',
            'DYNAMODB_CUSTOM_LIST_TABLE_TABLE_NAME',
            'DYNAMODB_OPTIONS_CUSTOM_LIST_TABLE_TABLE_TABLE_NAME']
    )
    
    DYNAMODB_ROUTE_TABLE_NAME = args['DYNAMODB_ROUTE_TABLE_NAME']
    DYNAMODB_CUSTOM_LIST_TABLE_TABLE_NAME = args['DYNAMODB_CUSTOM_LIST_TABLE_TABLE_NAME']
    DYNAMODB_OPTIONS_CUSTOM_LIST_TABLE_TABLE_TABLE_NAME = args['DYNAMODB_OPTIONS_CUSTOM_LIST_TABLE_TABLE_TABLE_NAME']
#################################################################################################################################################
def getRoutesByGroup(group):
    kwargs = {
        'ExpressionAttributeValues': {
            ":g": group,
        },
        'ExpressionAttributeNames': {
            "#g": "group",
            "#ps": "parkingSpace",
        },
        'FilterExpression': "#g = :g and attribute_exists(parkingSpace)",
        'ProjectionExpression': 'id, #g, #ps'
    }

    # print('KARGS:', kwargs)
    routes = scan_dynamo_table(DYNAMODB_ROUTE_TABLE_NAME, kwargs)
    # print(f"Routes: {routes}")
    print(f"Routes: {len(routes)}")
    return routes

#################################################################################################################################################
def getParkingSpaceOptions(id):
    kwargs = {
        'IndexName': "gsi-OptionsCustomLists",
        'FilterExpression': "#oclid = :id",
        'ExpressionAttributeValues': {
            ":id": id,
        },
        'ExpressionAttributeNames': {
            "#oclid": "optionsCustomListsCustomListsId",
            "#g": "group",
            "#opt": "option"
        },
        'ProjectionExpression': 'id, #g, #opt, #oclid',
    }
    # print('KARGS:', kwargs)
    parking_space_options = scan_dynamo_table(DYNAMODB_OPTIONS_CUSTOM_LIST_TABLE_TABLE_TABLE_NAME, kwargs)
    print(f"Parking Space Options: {len(parking_space_options)}")
    return parking_space_options
    
###############################################################################################################################################
def getAllParkingSpaceCustomList():
        kwargs = {
            'ExpressionAttributeNames': {
                '#g': 'group',
                '#t': 'type',
            },
            'ExpressionAttributeValues': {
                ':t': 'parking-space'
            },
            'FilterExpression': '#t = :t',
            'ProjectionExpression': 'id, #g, #t'
        }
        custom_list = scan_dynamo_table(DYNAMODB_CUSTOM_LIST_TABLE_TABLE_NAME, kwargs)
        # print(f"Custom List Size: {len(custom_list)}")
        return custom_list
###############################################################################################################################################
def setParkingSpaceForRoutes():
    customLists = getAllParkingSpaceCustomList()
    # print("Custom List:", customLists)
    for customList in customLists:
        try:
            # Get all parking space options by gsi
            parkingSpaceList = getParkingSpaceOptions(customList["id"])
            print("----Parking Space List----:", parkingSpaceList)
            if(len(parkingSpaceList) <= 0): 
                continue
            
            #  Get routes by group
            routes = getRoutesByGroup(customList["group"])
            # print("Routes:", routes)
            if(len(routes) <= 0):
                continue
            
            for route in routes:
                # print(routes)
                try:
                    parkingSpaceId = ""
                    for item in parkingSpaceList:
                        # print("ITEM:", item)
                        # print("Route:", route)
                        # print("Validate: ", item["option"].lower() == route["parkingSpace"].lower())
                        if(route["parkingSpace"] == '' or route["parkingSpace"] == None):
                            print("CASE 1")
                            break
                        elif(item["option"].lower() == route["parkingSpace"].lower()):
                            parkingSpaceId = item["id"]
                            print("MATCH - PARKING SPACE ID:", parkingSpaceId)
                            break
                    if (parkingSpaceId == ""):
                        continue
                    kwargs = {
                        'Key': {
                            "id": route["id"],
                        },
                        'ExpressionAttributeNames': {
                            "#rps": "routeParkingSpaceId",
                        },
                        'ExpressionAttributeValues': {
                            ":rpsId": parkingSpaceId
                        },
                        'UpdateExpression': "SET #rps=:rpsId",
                    }
                    print("------UPDATE--------", kwargs)
                    updatedField = update_record(DYNAMODB_ROUTE_TABLE_NAME, kwargs)
                    print("------UPDATE SUCCESSFULLY--------", updatedField)
                except ex.ClientError as error:
                    raise Exception(f'Error: {error}')
        except ex.ClientError as error:
            raise Exception(f'Error: {error}')

###############################################################################################################################################
def init():
    print("****** START JOB ******")
    load_job_parameters()
    # print("DYNAMODB_ROUTE_TABLE_NAME:", DYNAMODB_ROUTE_TABLE_NAME)
    # print("DYNAMODB_CUSTOM_LIST_TABLE_TABLE_NAME:", DYNAMODB_CUSTOM_LIST_TABLE_TABLE_NAME)
    # print("DYNAMODB_OPTIONS_CUSTOM_LIST_TABLE_TABLE_TABLE_NAME:", DYNAMODB_OPTIONS_CUSTOM_LIST_TABLE_TABLE_TABLE_NAME)


###############################################################################################################################################

if __name__ == "__main__":

    # Init:
    #   - Fetch Client_ID, CLient_SECRET & REFRESH_TOKEN from SSM
    #   - Generate fresh ACCESS_TOKEN
    init()
    setParkingSpaceForRoutes()
###############################################################################################################################################