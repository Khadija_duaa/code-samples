import boto3
import pytz
import sys
import datetime
import botocore.exceptions as ex
from awsglue.utils import getResolvedOptions
from boto3.dynamodb.conditions import Key, Attr

###################################################*** Global Variables***#######################################################################
dynamodb = boto3.resource('dynamodb')

DYNAMODB_TENANT_TABLE_NAME = ""
DYNAMODB_INFRACTION_TABLE_NAME = ""
DYNAMODB_KUDOS_TABLE_NAME = ""

TENANTS_BY_GROUP = {}
TENANT_GROUPS_TO_UPDATE = [
    # If this list has values, only the records which groups are listed here will be fetched,
    # leave blank to update ALL records
]

#################################################################################################################################################
def get_dynamo_table(table_name):
    return dynamodb.Table(table_name)

#################################################################################################################################################
def handle_timeout_error(last_evaluated_key, table_name):
    if last_evaluated_key:
        print(f"Last pagination before timeout: {last_evaluated_key}")
    else:
        print("No pagination information available before timeout")
    raise Exception(f'Timeout error while scanning the table: {table_name}')

#################################################################################################################################################
def scan_dynamo_records(table, params):
    table_name = params['TableName']
    try:
        response = table.scan(**params)
        return response
    except ex.ClientError as error:
        if error.response['Error']['Code'] == 'TimeoutException':
            handle_timeout_error(next_key, table_name)
        else:
            raise Exception(f'Error scanning the table: {table_name}: {error}')

#################################################################################################################################################
def query_dynamo_records(table, params):
    table_name = params['TableName']
    try:
        response = table.query(**params)
        return response
    except ex.ClientError as error:
        if error.response['Error']['Code'] == 'TimeoutException':
            handle_timeout_error(next_key, table_name)
        else:
            raise Exception(f'Error querying the table: {table_name}: {error}')

#################################################################################################################################################
def update_dynamo_record(table, params):
    table_name = params['TableName']
    try:
        response = table.update_item(**params)
        print('Success:', response)
    except ex.ClientError as error:
        raise Exception(f'Error Scanning Table: {table_name}: {error}')

#################################################################################################################################################
def load_job_parameters():
    global DYNAMODB_TENANT_TABLE_NAME
    global DYNAMODB_INFRACTION_TABLE_NAME
    global DYNAMODB_KUDOS_TABLE_NAME
    args = getResolvedOptions(sys.argv, [
        'DYNAMODB_TENANT_TABLE_NAME',
        'DYNAMODB_INFRACTION_TABLE_NAME',
        'DYNAMODB_KUDOS_TABLE_NAME',
    ])    
    DYNAMODB_TENANT_TABLE_NAME = args['DYNAMODB_TENANT_TABLE_NAME']
    DYNAMODB_INFRACTION_TABLE_NAME = args['DYNAMODB_INFRACTION_TABLE_NAME']
    DYNAMODB_KUDOS_TABLE_NAME = args['DYNAMODB_KUDOS_TABLE_NAME']

###############################################################################################################################################
def load_all_tenants_into_map():
    global TENANTS_BY_GROUP
    table = get_dynamo_table(DYNAMODB_TENANT_TABLE_NAME)
    params = {
        'ExpressionAttributeNames': {
            '#id': 'id',
            '#group': 'group',
            '#timeZone': 'timeZone',
        },
        'TableName': DYNAMODB_TENANT_TABLE_NAME,
        'ProjectionExpression': '#id, #group, #timeZone',
    }
    last_evaluated_key = None
    while True:
        response = scan_dynamo_records(table, params)
        last_evaluated_key = response.get('LastEvaluatedKey')
        tenants = response.get('Items', [])
        for tenant in tenants:
            TENANTS_BY_GROUP[tenant.get('group')] = {
                "timezone": tenant.get('timeZone', ''),
                "offset_timestamp": get_local_time_for_current_day(tenant.get('timeZone', None)),
            }
        params["ExclusiveStartKey"] = last_evaluated_key
        print('LastEvaluatedKey', last_evaluated_key)
        if last_evaluated_key is None:
            break

###############################################################################################################################################
def apply_callback_to_all_records(table_name, table, cb):
    params = {
        'ExpressionAttributeValues': {
            ':dateFilter': 'T00:00:00',
        },
        'ExpressionAttributeNames': {
            '#id': 'id',
            '#group': 'group',
            '#date': 'date',
        },
        'TableName': table_name,
        'FilterExpression': 'contains (#date, :dateFilter)',
        'ProjectionExpression': '#id, #date, #group',
    }

    def fetch_records(params, resolver):
        last_evaluated_key = None
        while True:
            response = resolver(table, params)
            last_evaluated_key = response.get('LastEvaluatedKey')
            items = response.get('Items', [])
            cb(items)
            params["ExclusiveStartKey"] = last_evaluated_key
            print('Items fetched:', len(items))
            print('LastEvaluatedKey', last_evaluated_key)
            if last_evaluated_key is None:
                break

    if not len(TENANT_GROUPS_TO_UPDATE):
        return fetch_records(params, scan_dynamo_records)

    for group in TENANT_GROUPS_TO_UPDATE:
        params['IndexName'] = 'byGroup'
        params['ExpressionAttributeValues'][':group'] = group
        params['KeyConditionExpression'] = '#group = :group'
        fetch_records(params, query_dynamo_records)

###############################################################################################################################################
def update_item(table_name, table, item, new_date):
    params = {
        'TableName': table_name,
        'Key': { "id": item["id"] },
        'ExpressionAttributeNames': { '#date': 'date' },
        'ExpressionAttributeValues': { ':date': new_date },
        'UpdateExpression': 'set #date = :date',
    }
    update_dynamo_record(table, params)

###############################################################################################################################################
def get_local_time_for_current_day(timezone):
    try:
        timestamp = datetime.datetime.now(pytz.timezone(timezone)).replace(hour=0, minute=0, second=0, microsecond=0)
        convertTimestamp = timestamp.astimezone(pytz.UTC).strftime("%H:%M:%S.%f")[:-3] + "Z"
        return convertTimestamp
    except:
        return None

def get_date_with_timezone_offset(timestamp, offset):
    if not timestamp or not offset:
        return None
    date_only = timestamp.split('T')[0]
    return f'{date_only}T{offset}'

###############################################################################################################################################
def handle_print_report(lookup_dict):
    report_obj = {
      'summary': {
        'toBeUpdated': { 'infractions': 0, 'kudos': 0 },
        'toBeSkipped': { 'infractions': 0, 'kudos': 0 },
      },
      'toBeUpdated': {
        'infractions': { 'count': 0, 'items': [] },
        'kudos': { 'count': 0, 'items': [] },
      },
      'toBeSkipped': {
        'infractions': { 'count': 0, 'items': [] },
        'kudos': { 'count': 0, 'items': [] },
      },
    }
    table_names_dict = {
        DYNAMODB_INFRACTION_TABLE_NAME: 'infractions',
        DYNAMODB_KUDOS_TABLE_NAME: 'kudos',
    }

    table_names = [DYNAMODB_INFRACTION_TABLE_NAME, DYNAMODB_KUDOS_TABLE_NAME]
    for table_name in table_names:
        print('FETCHING DATA FROM TABLE:', table_name)
        table = get_dynamo_table(table_name)

        def callback(items):
            for item in items:
                group_obj = lookup_dict.get(item['group'], None)
                new_date = get_date_with_timezone_offset(item['date'], group_obj['offset_timestamp']) if group_obj else None
                table_name_value = table_names_dict[table_name]
                if not new_date:
                    report_obj['summary']['toBeSkipped'][table_name_value] += 1
                    report_obj['toBeSkipped'][table_name_value]['count'] += 1
                    report_obj['toBeSkipped'][table_name_value]['items'].append(item)
                    continue
                item['new_date'] = new_date
                report_obj['summary']['toBeUpdated'][table_name_value] += 1
                report_obj['toBeUpdated'][table_name_value]['count'] += 1
                report_obj['toBeUpdated'][table_name_value]['items'].append(item)

        apply_callback_to_all_records(table_name, table, callback)

    print('*** REPORT ***')
    print(report_obj)

###############################################################################################################################################
def handle_update_records(lookup_dict):
    table_names = [DYNAMODB_INFRACTION_TABLE_NAME, DYNAMODB_KUDOS_TABLE_NAME]
    for table_name in table_names:
        print('UPDATING DATA FROM TABLE:', table_name)
        table = get_dynamo_table(table_name)

        def callback(items):
            for item in items:
                group_obj = lookup_dict.get(item['group'], None)
                new_date = get_date_with_timezone_offset(item['date'], group_obj['offset_timestamp']) if group_obj else None
                if not new_date:
                    continue
                update_item(table_name, table, item, new_date)

        apply_callback_to_all_records(table_name, table, callback)

###############################################################################################################################################
def init():
    print("-----Start work------")
    global TENANTS_BY_GROUP
    load_job_parameters()
    load_all_tenants_into_map()
    handle_print_report(TENANTS_BY_GROUP)
    # handle_update_records(TENANTS_BY_GROUP)

###############################################################################################################################################
if __name__ == "__main__":
    # Init:
    #   - Fetch Client_ID, CLient_SECRET & REFRESH_TOKEN from SSM
    #   - Generate fresh ACCESS_TOKEN
    init()
