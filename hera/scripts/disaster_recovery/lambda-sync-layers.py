# This script will be used to attach same layers with lambda function which are associated in source lambda function.
import boto3
import sys
from awsglue.utils import getResolvedOptions

def getLambdaFunctionNames(client, suffix):
    functions = []
    next_marker = None
    while True:
        if next_marker:
            response = client.list_functions(Marker=next_marker)
        else:
            response = client.list_functions()

        functions += [f['FunctionName']
                      for f in response['Functions'] if f['FunctionName'].endswith(suffix)]

        if 'NextMarker' in response:
            next_marker = response['NextMarker']
        else:
            break
    return functions


if __name__ == '__main__':
    JOB_PARAMETERS = ['destination_region',
                      'destination_suffix', 'source_region', 'source_suffix']
    # Get Glue Job Parameters
    try:
        args = getResolvedOptions(sys.argv, JOB_PARAMETERS)
        print("Arguments: {}".format(args))

        source_region = args['source_region']
        source_suffix = "-"+args['source_suffix']
        destination_region = args['destination_region']
        destination_suffix = "-"+args['destination_suffix']

        # session = boto3.session.Session(profile_name='default')
        # source_region = 'us-east-1'
        # source_suffix = '-staging'
        # destination_region = 'us-east-2'
        # destination_suffix = '-dev'
    except Exception as ex:
        print("Invalid or incomplete parameters provided for the glue job %s" + str(ex))
        exit()

    source_client = boto3.client('lambda', region_name=source_region)
    destination_client = boto3.client('lambda', region_name=destination_region)
    print("Lambda clients initiated successfully!")
    # List of all function names in the primary region
    source_function_names = getLambdaFunctionNames(
        source_client, source_suffix)
    print("List of Source Lambda Functions fetched successfully")
    # Loop through each lambda function and update role permissions
    for fn_name in source_function_names:
        print("Now processing source lambda function ({})".format(fn_name))
        # Remove the suffix to get the original function name
        original_fn_name = fn_name[:-len(source_suffix)]
        # Add the destination suffix to get the function name in the destination region
        destination_fn_name = original_fn_name + destination_suffix
        # Get the environment variables of the source function
        source_func_config = source_client.get_function_configuration(
            FunctionName=fn_name)
        print("Retrieved source function ({}) config".format(fn_name))
        source_func_layers = source_func_config.get('Layers', [])
        print("Retrieved source function ({}) layers: {}".format(
            fn_name, source_func_layers))

        try:
            destination_func_config = destination_client.get_function_configuration(
            FunctionName=destination_fn_name)
        except Exception as ex:
            print("The function {} configuration does not exist.".format(destination_fn_name))
            print(ex)
            destination_func_config = None

        if destination_func_config:
            destination_func_layers = []
            # Iterate over the policies and attach to the destination role
            for source_layer in source_func_layers:
                print("Source Layer:", source_layer)
                dest_layer_arn = source_layer['Arn'].replace("aws:lambda:{}:".format(
                    source_region), "aws:lambda:{}:".format(destination_region))
                print("Destination Layer Arn:", dest_layer_arn)
                print("Layer Version:", dest_layer_arn.rsplit(':', 1)[-1])
                destination_func_layers.append(dest_layer_arn)

            # Update the configuration of the Lambda function with the new list of layers
            response = destination_client.update_function_configuration(
                FunctionName=destination_fn_name,
                Layers=destination_func_layers
            )
            print("Destination function ({}) updated with layers: {}".format(
                destination_fn_name, destination_func_layers))
        else:
            print("Destination function not found: ", destination_fn_name)
