import sys
import boto3
import json
from awsglue.utils import getResolvedOptions

def getAwsAccountId():
    sts_client = boto3.client('sts')
    account_id = sts_client.get_caller_identity().get('Account')
    print("AWS Account Number:", account_id)
    return account_id

def get_datasource_by_name(appsync_client, appsync_id, data_source_name):
    return appsync_client.get_data_source(apiId=appsync_id, name=data_source_name)

def update_role_policy_document(iam, account_id, role_name, table_name):
    # get role policies
    role_policies = iam.list_role_policies(RoleName=role_name)
    # iterate through the policies and filter by name
    for policy_name in role_policies['PolicyNames']:
        if policy_name.startswith('DynamoDBAccess'):
            print("Updating Policy Name:{}".format(policy_name))
            
            policy = iam.get_role_policy(RoleName=role_name, PolicyName=policy_name)
            policy_document = policy['PolicyDocument']
            print("Existing Policy Document: {}".format(policy_document))
            
            # update the resources section of the policy document
            policy_document['Statement'][0]['Resource'] = [
                'arn:aws:dynamodb:*:'+account_id+':table/'+table_name,
                'arn:aws:dynamodb:*:'+account_id+':table/'+table_name+'/*'
            ]
            
            # convert the policy document back to a JSON string
            updated_policy_document = json.dumps(policy_document)
            print("Modified Policy Document: {}".format(updated_policy_document))
            # update the policy
            iam.put_role_policy(RoleName=dest_role_name, PolicyName=policy_name, PolicyDocument=updated_policy_document)
            print("Role:{}; Policy:{}; Updated successfully".format(role_name, policy_name))

def get_data_sources(appsync_client, appsync_id):
    ds_dict = {}
    paginator = appsync_client.get_paginator('list_data_sources')
    page_iterator = paginator.paginate(apiId=appsync_id)
    print("Fetching list of datasources from source page by page")
    for page in page_iterator:
        for item in page["dataSources"]:
            if item["type"] != "NONE" and item["name"] != "MessageTable":
                ds_dict[item["name"]] = [item["dynamodbConfig"]
                                         ["tableName"], item['serviceRoleArn']]

    return ds_dict


def update_data_sources(appsync_client, appsync_id, data_source_name, role_arn, table_name, dest_region):
    try:
        appsync_client.update_data_source(
            apiId=appsync_id,
            name=data_source_name,
            type='AMAZON_DYNAMODB',
            serviceRoleArn=role_arn,
            dynamodbConfig={
                'tableName': table_name,
                'awsRegion': dest_region
            }
        )
        print("AppSync Datasource ({}/{}) updated with Role:{}; Table: {}".format(
            appsync_id, data_source_name, role_arn, table_name))
    except appsync_client.exceptions.NotFoundException:
        print("Datasoure {} not found in the destination App Sync {}".format(
            data_source_name, appsync_id))


if __name__ == '__main__':
    # Get Glue Job Parameters
    try:
        JOB_PARAMETERS = ['source_appsync_id',
                          'dest_appsync_id', 'source_region', 'dest_region']
        args = getResolvedOptions(sys.argv, JOB_PARAMETERS)
        print("Arguments: {}".format(args))

        source_appsync_id = args['source_appsync_id']
        dest_appsync_id = args['dest_appsync_id']
        source_region = args['source_region']
        dest_region = args['dest_region']
        account_id = getAwsAccountId()

        # session = boto3.session.Session(profile_name='default')
        # source_appsync_id = '4rgcvstf2zcmjelbnnzujwjjku'  # hera-staging
        # dest_appsync_id = 'rzqe4vpkqjc3did5e7zffeknbi'  # hera-stagingdr
        # source_region = 'us-east-2'
        # dest_region = 'us-east-1'
    except Exception as ex:
        print("Invalid or incomplete parameters provided for the glue job %s" + str(ex))
        exit()

    source_appsync = boto3.client('appsync', region_name=source_region)
    dest_appsync = boto3.client('appsync', region_name=dest_region)
    print("AppSync client initialized successfully!")
    iam = boto3.client('iam', region_name=dest_region)
    print("Iam client initialized successfully")
    source_data_sources_dict = get_data_sources(
        source_appsync, source_appsync_id)
        
    for data_source_name in source_data_sources_dict:
        print("Now processing datasource: {}".format(data_source_name))
        table_name = source_data_sources_dict[data_source_name][0]
        # role_arn = source_data_sources_dict[data_source_name][1]
        print("Source AppSync - Datasource: {}; Table:{};".format(data_source_name, table_name))
        try:
            dest_ds = get_datasource_by_name(dest_appsync, dest_appsync_id, data_source_name)
        except Exception as ex:
            print("Datasource not found in the destination region {}".format(data_source_name))
            print(ex)
            print("----------------------------------------------------------------")
            continue
        dest_role_arn = dest_ds['dataSource']['serviceRoleArn']
        dest_role_name = dest_role_arn.split('/')[-1]
        update_role_policy_document(iam, account_id, dest_role_name, table_name)

        update_data_sources(
            dest_appsync,
            dest_appsync_id,
            data_source_name,
            dest_role_arn,
            table_name,
            dest_region)
