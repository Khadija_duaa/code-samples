import boto3
import sys
from awsglue.utils import getResolvedOptions


def get_table_names(appsync_client, appsync_id):
    table_names = []
    paginator = appsync_client.get_paginator('list_data_sources')
    page_iterator = paginator.paginate(apiId=appsync_id)
    print("Fetching list of datasources from source page by page")
    for page in page_iterator:
        for item in page["dataSources"]:
            if item["type"] != "NONE" and item["name"] != "MessageTable":
                table_names.append(item["dynamodbConfig"]["tableName"])
    return table_names


def create_global_table(source_dynamodb, target_dynamodb, table_name, source_region, dest_region):

    table_description = source_dynamodb.describe_table(TableName=table_name)
    if table_description['Table']['ItemCount'] == 0:
        print("Table {} is empty.".format(table_name))
        attribute_definitions = [{'AttributeName': 'id', 'AttributeType': 'S'}]
    else:
        print("Table {} has {} records.".format(
            table_name, table_description['Table']['ItemCount']))
        attribute_definitions = table_description['Table']['AttributeDefinitions']

    key_schema = table_description['Table']['KeySchema']
    provisioned_throughput = table_description['Table']['ProvisionedThroughput']
    billing_mode = table_description['Table']['BillingModeSummary']['BillingMode']
    stream_specs = table_description['Table']['StreamSpecification']
    print("-----------------------------------------------")
    print("Table Description:{}".format(table_description['Table']))
    print("-----------------------------------------------")
    print("AttributeDefinition:{}".format(attribute_definitions))
    print("-----------------------------------------------")
    print("KeySchema:{}".format(key_schema))
    print("-----------------------------------------------")
    print("Billing Mode: {}".format(billing_mode))
    print("-----------------------------------------------")

    try:
        target_response = target_dynamodb.describe_table(TableName=table_name)
        print("Table already exists: ", target_response['Table']['TableName'])
    except target_dynamodb.exceptions.ResourceNotFoundException:
        print("Create New Table: {}".format(table_name))
        response = target_dynamodb.create_table(
            TableName=table_name,
            KeySchema=key_schema,
            AttributeDefinitions=attribute_definitions,
            BillingMode=billing_mode,
            StreamSpecification=stream_specs
        )
        print("Reponse of Table Creation: {}".format(response))

        response = source_dynamodb.create_global_table(
            GlobalTableName=table_name,
            ReplicationGroup=[{'RegionName': source_region},
                              {'RegionName': dest_region}]
        )
        print("Global Table Setup completed for table {}".format(table_name))
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
        return response


if __name__ == '__main__':

    JOB_PARAMETERS = ['dest_region', 'source_region', 'source_appsync_id']
    # Get Glue Job Parameters
    try:
        args = getResolvedOptions(sys.argv, JOB_PARAMETERS)
        print("Arguments: {}".format(args))

        source_region = args['source_region']
        source_appsync_id = args['source_appsync_id']
        dest_region = args['dest_region']
        excluded_tables = ['Message-mn2kxb25mbbdpjsoqg7v7ig6hq-devqa',
                           'AuditLog-mn2kxb25mbbdpjsoqg7v7ig6hq-devqa']

    except Exception as ex:
        print("Invalid or incomplete parameters provided for the glue job.")
        print(ex)
        exit()

    appsync_client = boto3.client('appsync', region_name=source_region)
    source_dynamodb = boto3.client('dynamodb', region_name=source_region)
    target_dynamodb = boto3.client('dynamodb', region_name=dest_region)
    print("Fetching table names from provided AppSync ID")
    table_names = get_table_names(appsync_client, source_appsync_id)
    print("Table names list: {}".format(table_names))

    for table in table_names:
        if table not in excluded_tables:
            try:
                print("Creating global table of: {}".format(table))
                result = create_global_table(
                    source_dynamodb, target_dynamodb, table, source_region, dest_region)
                print(">>> Table is in {} state.".format(
                    result['GlobalTableDescription']['ReplicationGroup']))
            except Exception as e:
                print(e)
