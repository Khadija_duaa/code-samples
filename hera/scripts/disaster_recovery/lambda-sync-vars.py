import boto3
import sys
import re
from awsglue.utils import getResolvedOptions


def getLambdaFunctionNames(client, suffix):
    functions = []
    next_marker = None
    while True:
        if next_marker:
            response = client.list_functions(Marker=next_marker)
        else:
            response = client.list_functions()

        functions += [f['FunctionName']
                      for f in response['Functions'] if f['FunctionName'].endswith(suffix)]

        if 'NextMarker' in response:
            next_marker = response['NextMarker']
        else:
            break
    return functions


def convertTableNames(source_vars, source_appsync_id, source_suffix, dest_appsync_id, dest_suffix):
    print("Converting Tables Names According to Destination Region")
    for key in source_vars:
        if key.endswith('_TABLE') or key=='TABLE':
            value = str(source_vars[key])
            new_value = re.sub("-{}{}".format(source_appsync_id, source_suffix),
                               "-{}{}".format(dest_appsync_id, dest_suffix), value)
            source_vars[key] = new_value
    return source_vars


if __name__ == '__main__':
    JOB_PARAMETERS = ['destination_region', 'destination_suffix', 'source_region', 'source_suffix',
                      'graphql_apiid_output', 'graphql_api_endpoint_output', 'graphql_apiid_output_source', 'bucket',
                      'user_pool', 'message_table', 'api_sendinternalmessage_apiid',
                      'api_sendmessage_apiid', 'api_stripesetup_apiid', 'statemachine_arn',
                      'snstopic_arn', 'convert_table_names_dest',
                      'origination_number','project_id','origin','hera_url',
                      'pin_point_region','bandwidth_application_id',
                      'telnyx_profile_name','metric_group_name']

    # Get Glue Job Parameters
    try:
        args = getResolvedOptions(sys.argv, JOB_PARAMETERS)
        print("Arguments: {}".format(args))

        source_region = args['source_region']
        source_suffix = "-"+args['source_suffix']
        destination_region = args['destination_region']
        destination_suffix = "-"+args['destination_suffix']
        source_appsync_id = args['graphql_apiid_output_source']
        dest_appsync_id = args['graphql_apiid_output']
        convert_table_names = args['convert_table_names_dest']

        # session = boto3.session.Session(profile_name='default')
        # source_region = 'us-east-1'
        # source_suffix = 'staging'
        # destination_region = 'us-east-2'
        # destination_suffix = 'dev'
    except Exception as ex:
        print("Invalid or incomplete parameters provided for the glue job")
        print(ex)
        exit()

    # Variables with default or region specific value.
    default_vars = {
        "API_HERA_GRAPHQLAPIIDOUTPUT": "{}".format(args['graphql_apiid_output']),
        "API_HERA_GRAPHQLAPIENDPOINTOUTPUT": "{}".format(args['graphql_api_endpoint_output']),
        "BUCKET": "{}".format(args['bucket']), # textractJobTrigger, ParsePerformanceData, ProcessPerformanceData, ProcessWeeklyData, addCounselingSignature, handleTextResponse, maintenanceDb, s3downloadlink, sendPinPointMessage
        "USERPOOL": "{}".format(args['user_pool']), # AdminQueriesad20795a, Signup, 
        "MESSAGE_TABLE":  "{}".format(args['message_table']), # stripeChargeTenants, stripeSetup, handleTextResponse, maintenanceDb, sendCoachingFeedback, sendMessages, sendPendingCoachingMessage
        "API_SENDINTERNALMESSAGE_APIID": "{}".format(args['api_sendinternalmessage_apiid']), # stripeChargeTenant
        "API_SENDMESSAGE_APIID": "{}".format(args['api_sendmessage_apiid']),
        "API_STRIPESETUP_APIID": "{}".format(args['api_stripesetup_apiid']),
        "STATEMACHINE_ARN": "{}".format(args['statemachine_arn']),
        "SNS_TOPIC_ARN": "{}".format(args['snstopic_arn']), # textractJobTrigger
        
        "AUTH_HERA4E362E32_USERPOOLID": "{}".format(args['user_pool']), # stripeChargeTenant
        "ORIGINATION_NUMBER": "{}".format(args['origination_number']), # handleTextResponse, sendPinPointMessage 
        "PROJECT_ID": "{}".format(args['project_id']), # handleTextResponse, sendPinPointMessage, ProcessPerformanceData, ProcessWeeklyData
        "ORIGIN": "{}".format(args['origin']), # sendCoachingFeedback, sendMessages, sendPendingCoachingMessage, 
        "HERA_URL": "{}".format(args['hera_url']), # ParsePerformanceData, 
        "WINDOW_ORIGIN": "{}".format(args['hera_url']), # sendPinPointMessage, ProcessPerformanceData
        "PIN_POINT_REGION": "{}".format(args['pin_point_region']), # sendPinPointMessage, ProcessWeeklyData, handleTextResponse, 
        "BANDWIDTH_APPLICATION_ID": "{}".format(args['bandwidth_application_id']), # handleTextResponse, sendPinPointMessage
        "TELNYX_PROFILE_NAME": "{}".format(args['telnyx_profile_name']), # handleTelnyxData
        "METRIC_GROUP_NAME": "{}".format(args['metric_group_name']) # sendCoachingFeedback, ParsePerformanceData
    }
    print("Default Variables are as follows: {}".format(default_vars))
    excluded_vars = ['env', 'region']

    source_client = boto3.client('lambda', region_name=source_region)
    destination_client = boto3.client('lambda', region_name=destination_region)
    print("Source and Destination clients are intilized successfully.")
    # List of all function names in the source region
    function_names = getLambdaFunctionNames(source_client, source_suffix)
    print("Source Region functions successfully fetched.")
    # print("Source region functions are:{}".format(function_names))

    # Loop through each function and update the environment variables
    for fn_name in function_names:
        print("Now processing function:{}".format(fn_name))
        # Remove the suffix to get the original function name
        original_fn_name = fn_name[:-len(source_suffix)]
        # Add the destination suffix to get the function name in the destination region
        destination_fn_name = original_fn_name + destination_suffix
        print("Destination Function Name is:{}".format(destination_fn_name))
        # Get the environment variables of the source function
        source_func_config = source_client.get_function_configuration(
            FunctionName=fn_name)
        if u'Environment' in source_func_config:
            source_env_vars = source_func_config['Environment']['Variables']
            print("Source Function ({}) Variables are: {}".format(
                fn_name, source_env_vars))
            # Create a new dictionary with non-excluded variables
            dest_env_vars = {
                k: v for k, v in source_env_vars.items() if k.lower() not in excluded_vars}
            # Check for specific variables and set their values
            for default_var_name, default_var_value in default_vars.items():
                if default_var_name in source_env_vars:
                    dest_env_vars[default_var_name] = default_var_value
            
            # Get the environment variables of the destination function
            try:
                destination_func_config = destination_client.get_function_configuration(
                    FunctionName=destination_fn_name)
            except Exception as ex:
                print("Function Not Found")
                print(ex)
                continue 
            existing_env_vars = {}
            if u'Environment' in destination_func_config:
                existing_env_vars = destination_func_config['Environment']['Variables']
                print("{} have the following variables:{}".format(
                    destination_fn_name, existing_env_vars))
            else:
                print("{} don't have existing variables.".format(
                    destination_fn_name))

            # Merge the new and existing dictionaries
            existing_env_vars.update(dest_env_vars)

            # Region for new environment preparation. Not for DR setup.
            if convert_table_names.lower() == "true" or convert_table_names == "1":
                existing_env_vars = convertTableNames(
                    existing_env_vars, source_appsync_id, source_suffix, dest_appsync_id, destination_suffix)

            # Update the environment variables of the destination function
            response = destination_client.update_function_configuration(
                FunctionName=destination_fn_name,
                Environment={'Variables': existing_env_vars}
            )
            print("Environment variables migrated from {} to {}".format(
                fn_name, destination_fn_name))
            print("-----------------------------------------------")
        else:
            print("Source Function ({}) don't have any variables".format(fn_name))
