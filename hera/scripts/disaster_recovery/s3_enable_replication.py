import boto3

# Todo: Need to create IAM roles using CloudFormation

# hera configs
session = boto3.session.Session(profile_name='hera')
source_region = 'us-east-2'
target_region = 'us-east-1'
source_buckets = ['hera2009fb1ba5504c8da9ba5f1fe34db738200210-staging']
target_buckets = ['hera2009fb1ba5504c8da9ba5f1fe34db738214923-stagingdr']
iam_role = 'arn:aws:iam::530079012632:role/s3-cross-region-replication-role' # Todo
iam_batch_role = 'arn:aws:iam::530079012632:role/s3_batch-replication_role'  # Todo


def bucket_versioning_status(bucket_name):
    s3_client = session.client('s3', region_name=source_region)
    response = s3_client.get_bucket_versioning( Bucket=bucket_name)
    if 'Status' not in response:
        return 'Suspended'
    return response['Status']

def enable_bucket_versioning(bucket_name):
    s3_client = session.client('s3', region_name=source_region)
    response = s3_client.put_bucket_versioning(
        Bucket=bucket_name,
        VersioningConfiguration={
            'Status': 'Enabled'
        },
    )
    return response

def enable_bucket_replication(source_bucket, target_bucket, id, iam_role):
    s3_client = session.client('s3', region_name=source_region)
    target_bucket_arn = 'arn:aws:s3:::' + target_bucket
    response = s3_client.put_bucket_replication(
        Bucket=source_bucket,
        ReplicationConfiguration={
            'Role': iam_role,
            'Rules': [
                {
                    'ID': id,
                    'Priority': 0,
                    'Status': 'Enabled',
                    'Destination': {
                        'Bucket': target_bucket_arn
                    },
                    "DeleteMarkerReplication":{
                        "Status":"Enabled"
                    },
                    "Filter": {"Prefix": ""}
                }
            ]
        }
    )
    return response

def bucket_replication_status(bucket_name, id):
    s3_client = session.client('s3', region_name=source_region)
    try:
        response = s3_client.get_bucket_replication(
            Bucket=bucket_name,
        )
    except Exception as e:
        print(e) 
        return 'Disabled'
    for rule in response['ReplicationConfiguration']['Rules']:
        if id in rule["ID"]:
            return 'Enabled'
    return 'Disabled'

def replicate_existing_objects(iam_batch_role, bucket_name):
    s3_control_client = session.client('s3control', region_name=source_region)

    accountId = session.client('sts').get_caller_identity().get('Account')
    response = s3_control_client.create_job(
        AccountId = accountId,
        ConfirmationRequired = False,
        Operation = {
            'S3ReplicateObject': {}
        },
        Report={
            'Enabled': False
        },
        ManifestGenerator= {
            "S3JobManifestGenerator": {
                "SourceBucket": "arn:aws:s3:::" + bucket_name,
                "Filter": {
                    "EligibleForReplication": True
                },
                "EnableManifestOutput": False
            }
        },
        Priority    = 10,
        RoleArn     = iam_batch_role,
    )
    return response

if __name__ == '__main__':

    for source_bucket, target_bucket in zip(source_buckets, target_buckets):
        print("Source bucket name: " + source_bucket)
        print("Destination bucket name: " + target_bucket)
        if bucket_versioning_status(source_bucket) == 'Suspended':
            print('Versioning is disabled on ' + source_bucket + ' bucket. Enabling versioning on it.')
            enable_bucket_versioning(source_bucket)
        if bucket_versioning_status(target_bucket) == 'Suspended':
            print('Versioning is disabled on ' + target_bucket + ' bucket. Enabling versioning on it.')
            enable_bucket_versioning(target_bucket)
        
        id = 'replicate-all-objects'
        if bucket_replication_status(source_bucket, id) == 'Disabled':
            print("Enabling replication on " + source_bucket + " bucket.")  
            enable_bucket_replication(source_bucket, target_bucket, id, iam_role)
            print("Replication " + str(bucket_replication_status(source_bucket, id)) + " on " + source_bucket + " bucket.") 
        else:
            print("Replication already " + str(bucket_replication_status(source_bucket, id)) + " on " + source_bucket + " bucket.") 

        print("Replicating existing objects of S3 bucket from source to target")
        replicate_existing_objects(iam_batch_role, source_bucket)
        print('Script completed')



# S3 CRR replication role

# {
#     "Version": "2012-10-17",
#     "Statement": [
#         {
#             "Action": [
#                 "s3:ListBucket",
#                 "s3:GetReplicationConfiguration",
#                 "s3:GetObjectVersionForReplication",
#                 "s3:GetObjectVersionAcl",
#                 "s3:GetObjectVersionTagging",
#                 "s3:GetObjectRetention",
#                 "s3:GetObjectLegalHold"
#             ],
#             "Effect": "Allow",
#             "Resource": [
#                 "arn:aws:s3:::delete-me-amplify",
#                 "arn:aws:s3:::delete-me-amplify/*",
#                 "arn:aws:s3:::delete-me-amplify-dr",
#                 "arn:aws:s3:::delete-me-amplify-dr/*"
#             ]
#         },
#         {
#             "Action": [
#                 "s3:ReplicateObject",
#                 "s3:ReplicateDelete",
#                 "s3:ReplicateTags",
#                 "s3:ObjectOwnerOverrideToBucketOwner"
#             ],
#             "Effect": "Allow",
#             "Resource": [
#                 "arn:aws:s3:::delete-me-amplify/*",
#                 "arn:aws:s3:::delete-me-amplify-dr/*"
#             ]
#         }
#     ]
# }

    

# Create Role with following permissions in CFN for batch job

# {
#     "Version": "2012-10-17",
#     "Statement": [
#         {
#             "Effect": "Allow",
#             "Action": [
#                 "s3:InitiateReplication",
#                 "s3:GetReplicationConfiguration",
#                 "s3:PutInventoryConfiguration"
#             ],
#             "Resource": [
#                 "arn:aws:s3:::delete-me-amplify",
#                 "arn:aws:s3:::delete-me-amplify/*"
#             ]
#         }
#     ]
# }


