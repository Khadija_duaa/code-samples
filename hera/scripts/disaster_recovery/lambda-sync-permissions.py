# This script will be used to replicate Lambda function role permissions similar to the primary region

import boto3
import sys
from awsglue.utils import getResolvedOptions

def getLambdaFunctionNames(client, suffix):
    functions = []
    next_marker = None
    while True:
        if next_marker:
            response = client.list_functions(Marker=next_marker)
        else:
            response = client.list_functions()

        functions += [f['FunctionName']
                      for f in response['Functions'] if f['FunctionName'].endswith(suffix)]

        if 'NextMarker' in response:
            next_marker = response['NextMarker']
        else:
            break
    return functions


if __name__ == '__main__':

    JOB_PARAMETERS = ['destination_region',
                    'destination_suffix', 'source_region', 'source_suffix']
    # Get Glue Job Parameters
    try:
        args = getResolvedOptions(sys.argv, JOB_PARAMETERS)
        print("Arguments: Source Region: {}; Source Suffix: {}, Destination Region: {}, Destination Suffix: {}".format(
            args['source_region'], args['source_suffix'], args['destination_region'], args['destination_suffix']))

        source_region = args['source_region']
        source_suffix = "-"+args['source_suffix']
        destination_region = args['destination_region']
        destination_suffix = "-"+args['destination_suffix']

        # session = boto3.session.Session(profile_name='default')
        # source_region = 'us-east-1'
        # source_suffix = '-staging'
        # destination_region = 'us-east-2'
        # destination_suffix = '-dev'
    except Exception as ex:
        print("Invalid or incomplete parameters provided for the glue job %s" + str(ex))
        exit()

    source_client = boto3.client('lambda', region_name=source_region)
    destination_client = boto3.client('lambda', region_name=destination_region)
    print("Lambda clients initiated successfully!")
    source_iam_client = boto3.client('iam', region_name=source_region)
    destination_iam_client = boto3.client('iam', region_name=destination_region)
    print("Iam clients initiated successfully!")
    # List of all function names in the primary region
    source_function_names = getLambdaFunctionNames(
        source_client, source_suffix)
    print("List of Lambda Functions fetched successfully")
    # Loop through each lambda function and update role permissions
    for fn_name in source_function_names:
        print("Processing the source function ({})".format(fn_name))
        # Remove the suffix to get the original function name
        original_fn_name = fn_name[:-len(source_suffix)]
        # Add the destination suffix to get the function name in the destination region
        destination_fn_name = original_fn_name + destination_suffix
        # Get the environment variables of the source function
        source_func = source_client.get_function_configuration(
            FunctionName=fn_name)
        print("Retrieved source function {} config".format(fn_name))

        
        try:
            dest_func = destination_client.get_function_configuration(
            FunctionName=destination_fn_name)
        except Exception as ex:
            print("The function {} configuration does not exist.".format(destination_fn_name))
            print(ex)
            dest_func = None        
        
        if dest_func:
            print("Retrieved destination function {} config".format(
                destination_fn_name))
            source_role_arn = source_func['Role']
            source_role_name = source_role_arn.split('/')[-1]
            print("Source Function ({}) RoleArn: {} & RoleName: {}".format(
                fn_name, source_role_arn, source_role_name))
            source_role_policies = source_iam_client.list_attached_role_policies(
                RoleName=source_role_name)
            dest_role_arn = dest_func['Role']
            dest_role_name = dest_role_arn.split('/')[-1]
            print("Destination Function ({}) RoleArn: {} & RoleName: {}".format(
                destination_fn_name, dest_role_arn, dest_role_name))
            print("Attaching the following policies into the destination function ({}) Role:{}".format(
                destination_fn_name, dest_role_name))

            # Iterate over the policies and attach to the destination role
            for policy in source_role_policies['AttachedPolicies']:
                if not policy['PolicyName'].startswith('AWSLambdaBasicExecutionRole-'):
                    destination_iam_client.attach_role_policy(
                        RoleName=dest_role_name, PolicyArn=policy['PolicyArn'])
                    print("Policy {} attached".format(policy['PolicyName']))
        else:
            print("Destination function ({}) not found: ".format(
                destination_fn_name))
    print("Successfully updated destination functions role policies from source functions")
