import boto3
import sys
from awsglue.utils import getResolvedOptions

def get_graphql_api(appsync_id, region):
    appsync_client = boto3.client('appsync', region_name=region)
    response = appsync_client.get_graphql_api(
        apiId=appsync_id
    )
    return response

def replace_cognito_userpool(appsync_client, appsync_api_id, graphql_api_name, user_pool_id, user_pool_region):
    appsync_client.update_graphql_api(
        apiId=appsync_api_id,
        name=graphql_api_name,
        authenticationType='AMAZON_COGNITO_USER_POOLS',
        userPoolConfig={
                'userPoolId': user_pool_id,
                'awsRegion': user_pool_region,
                'defaultAction': 'ALLOW'
        }
    )

if __name__ == '__main__':
    
    JOB_PARAMETERS = ['dest_region', 'dest_appsync_id', 
                      'source_region', 'source_appsync_id']
    # Get Glue Job Parameters
    try:
        args = getResolvedOptions(sys.argv, JOB_PARAMETERS)
        print("Arguments: {}".format(args))

        source_region = args['source_region']
        source_appsync_id = args['source_appsync_id']
        dest_region = args['dest_region']
        dest_appsync_id = args['dest_appsync_id']

        # session = boto3.session.Session(profile_name='default')
        # source_region = 'us-east-2'
        # dest_region = 'us-east-1'
        # source_appsync_id = '4rgcvstf2zcmjelbnnzujwjjku'    # hera-staging
        # dest_appsync_id = 'rzqe4vpkqjc3did5e7zffeknbi'   # hera-stagingdr
    except Exception as ex:
        print("Invalid or incomplete parameters provided for the glue job.")
        print(ex)
        exit()

    
    response = get_graphql_api(source_appsync_id, source_region)
    source_user_pool_id = response['graphqlApi']['userPoolConfig']['userPoolId']
    source_graphql_api_name = response['graphqlApi']['name']
    print("Source User Pool Id: {}; Source GraphQL API Name: {};".format(source_user_pool_id, source_graphql_api_name))

    response = get_graphql_api(dest_appsync_id, dest_region)
    dest_user_pool_id = response['graphqlApi']['userPoolConfig']['userPoolId']
    dest_graphql_api_name = response['graphqlApi']['name']
    appsync_client = boto3.client('appsync', region_name=dest_region)
    print("Destination User Pool Id: {}; Destination GraphQL API Name: {};".format(dest_user_pool_id, dest_graphql_api_name))
    
    print("Updating additional providers of " + dest_graphql_api_name + " API with primary user pool")
    replace_cognito_userpool(appsync_client, dest_appsync_id, dest_graphql_api_name, source_user_pool_id, source_region)
    print("Updated additional providers")
