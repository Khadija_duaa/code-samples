AWSTemplateFormatVersion: "2010-09-09"
Description: "Create a glue job to sync env vars from source Lambda function to target lambda function"

Parameters:
  GlueJobName:
    Type: String
    Description: Name of Glue Job for Data Migration
    Default: lambda-sync-vars

  GlueScriptLocation:
    Type: String
    Description: S3 path of the script
    Default: s3://hera2009fb1ba5504c8da9ba5f1fe34db738214923-stagingdr/dr-scripts/lambda-sync-vars.py

  GlueSourceRegion:
    Type: String
    Description: "The region where the source Lambda functions are located."
    Default: "us-east-2"

  GlueSourceSuffix:
    Type: String
    Description: "The suffix of the source Lambda functions to be updated."
    Default: "staging"

  GlueDestinationRegion:
    Type: String
    Description: "The region where the destination Lambda functions are located."
    Default: "us-east-1"

  GlueDestinationSuffix:
    Type: String
    Description: "The suffix of the destination Lambda functions to be updated."
    Default: "stagingdr"

  GlueGraphqlApiIdOutput:
    Type: String
    Description: "Destination AppSync Graphql Api Id"
    Default: ""

  GlueGraphqlApiIdOutputSource:
    Type: String
    Description: "Source AppSync Graphql Api Id"
    Default: ""

  GlueGraphqlApiEndPointOutput:
      Type: String
      Description: "Destination AppSync Graphql Api End Point"
      Default: ""

  GlueBucket:
      Type: String
      Description: "Destination Amplify App S3 Bucket name"
      Default: ""
  
  GlueUserPool:
    Type: String
    Description: "Primary Cognito UserPool Id"
    Default: ""

  GlueMessageTable:
    Type: String
    Description: "Destination Region Message Table Name"
    Default: ""
  
  GlueApiSendInteralMessageApiId:
    Type: String
    Description: "Destination Api Gateway: Api Id of SendInternalMessage Api"
    Default: ""
  
  GlueApiSendMessageApiId:
    Type: String
    Description: "Destination Api Gateway: Api Id of SendMessage Api"
    Default: ""

  GlueApiStripeSetupApiId:
    Type: String
    Description: "Destination Api Gateway: Api Id of StripeSetup Api"
    Default: ""
  
  GlueStateMachineArn:
    Type: String
    Description: "Destination Region Statemachine Arn"
    Default: ""

  GlueSnsTopicArn:
    Type: String
    Description: "Destination Region SNS Topic Arn"
    Default: ""
  
  GlueConvertTableNamesDest:
    Type: String
    Description: "Convert Table Names Values According to Destination Environment"
    Default: "false"

  GlueOriginationNumber:
    Type: String
    Description: "Originator Phone Number e.g. +1765432100. Used in handleTextResponse, sendPinPointMessage"
    Default: ""

  GlueProjectId:
    Type: String
    Description: "Project Id. Used in handleTextResponse, sendPinPointMessage, ProcessPerformanceData, ProcessWeeklyData"
    Default: ""

  GlueOrigin:
    Type: String
    Description: "Origin e.g. dev.herasolutions.app. Used in sendCoachingFeedback, sendMessages, sendPendingCoachingMessage"
    Default: ""

  GlueHeraUrl:
    Type: String
    Description: "Hera Application Url e.g. https://dev.herasolutions.app. Used in ParsePerformanceData, sendPinPointMessage, ProcessPerformanceData"
    Default: ""

  GluePinPointRegion:
    Type: String
    Description: "Pin Point Hosted Region e.g. us-east-1. Used in sendPinPointMessage, ProcessWeeklyData, handleTextResponse"
    Default: "us-east-1"

  GlueBandwidthApplicationId:
    Type: String
    Description: "Bandwidth Application Id (Deprecated) but still in use"
    Default: ""

  GlueTelnyxProfileName:
    Type: String
    Description: "Telnyx Profile Name e.g. Devqa, Test. Used in handleTelnyxData"
    Default: ""
  
  GlueMetricGroupName:
    Type: String
    Description: "Metric Group Name e.g. SendCoachingFeedback(DevQa), WeeklyDataImport. Used in sendCoachingFeedback, ParsePerformanceData"
    Default: ""

Resources:
  GlueJobSyncVarRole:
    Type: "AWS::IAM::Role"
    Properties:
      RoleName: "GlueJobSyncVarRole"
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
        - Effect: "Allow"
          Principal:
            Service: "glue.amazonaws.com"
          Action: "sts:AssumeRole"
      Path: "/"
      ManagedPolicyArns:
        - "arn:aws:iam::aws:policy/service-role/AWSGlueServiceRole"
        - "arn:aws:iam::aws:policy/AmazonS3ReadOnlyAccess"
        - "arn:aws:iam::aws:policy/CloudWatchLogsFullAccess"
      Policies:
      - PolicyName: "LambdaAccess"
        PolicyDocument:
          Version: "2012-10-17"
          Statement:
          - Effect: "Allow"
            Action:
            - "lambda:GetFunction"
            - "lambda:ListFunctions"
            - "lambda:GetFunctionConfiguration"
            - "lambda:UpdateFunctionConfiguration"
            Resource: "*"

  GlueJob:
    Type: "AWS::Glue::Job"
    Properties:
      Name: "UpdateLambdaEnvVarJob"
      Role: !GetAtt GlueJobSyncVarRole.Arn
      Command:
        Name: pythonshell
        ScriptLocation: !Ref GlueScriptLocation
        PythonVersion: 3
      DefaultArguments:
        "--source_region": !Ref GlueSourceRegion
        "--source_suffix": !Ref GlueSourceSuffix
        "--destination_region": !Ref GlueDestinationRegion
        "--destination_suffix": !Ref GlueDestinationSuffix
        "--graphql_apiid_output": !Ref GlueGraphqlApiIdOutput
        "--graphql_apiid_output_source": !Ref GlueGraphqlApiIdOutputSource
        "--graphql_api_endpoint_output": !Ref GlueGraphqlApiEndPointOutput
        "--bucket": !Ref GlueBucket
        "--user_pool": !Ref GlueUserPool
        "--message_table": !Ref GlueMessageTable
        "--api_sendinternalmessage_apiid": !Ref GlueApiSendInteralMessageApiId
        "--api_sendmessage_apiid": !Ref GlueApiSendMessageApiId
        "--api_stripesetup_apiid": !Ref GlueApiStripeSetupApiId
        "--statemachine_arn": !Ref GlueStateMachineArn
        "--snstopic_arn": !Ref GlueSnsTopicArn

        "--convert_table_names_dest": !Ref GlueConvertTableNamesDest
        "--origination_number": !Ref GlueOriginationNumber
        "--project_id": !Ref GlueProjectId
        "--origin": !Ref GlueOrigin
        "--hera_url": !Ref GlueHeraUrl
        "--pin_point_region": !Ref GluePinPointRegion
        "--bandwidth_application_id": !Ref GlueBandwidthApplicationId
        "--telnyx_profile_name": !Ref GlueTelnyxProfileName
        "--metric_group_name": !Ref GlueMetricGroupName
      ExecutionProperty:
        MaxConcurrentRuns: 1
