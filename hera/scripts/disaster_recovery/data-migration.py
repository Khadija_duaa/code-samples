import boto3
import time
import sys
import threading
from awsglue.utils import getResolvedOptions

# session = boto3.session.Session(profile_name='nbs-hera')
session = boto3
backup_arns = []
restore_list = []
# Create a list of destination environment lambda function names
dest_lambda_funcs = []
# create a list of threads
threads = []

# create a lock to synchronize access to the shared objects
lock = threading.Lock()


def getLambdaFunctionNames(lambda_client, env_name):
    functions = []
    next_marker = None
    while True:
        if next_marker:
            response = lambda_client.list_functions(Marker=next_marker)
        else:
            response = lambda_client.list_functions()

        functions += [f['FunctionName']
                      for f in response['Functions'] if f['FunctionName'].endswith(env_name)]

        if 'NextMarker' in response:
            next_marker = response['NextMarker']
        else:
            break
    return functions


def getTableStreamArn(dynamodb_client, table_name):
    # Fetch the key schema of the "Book" table
    response = dynamodb_client.describe_table(
        TableName=table_name,
    )
    table_stream_arn = None
    # Extract the stream ARN from the response
    if 'LatestStreamArn' in response['Table']:
        table_stream_arn = response['Table']['LatestStreamArn']
    print("Table {} Stream Arn is: {}".format(table_name, table_stream_arn))
    return table_stream_arn


def getLambdaFunctionAttachedToStream(lambda_client, stream_arn, lambda_functions):
    try:
        print("Getting List of Lambda functions Which are attached to Destination Table Stream")
        print("Lambda Functions: {}".format(lambda_functions))
        functions = []
        # loop through each Lambda function and check if it is triggered by the specified DynamoDB stream
        for function in lambda_functions:
            # Get the list of triggers for the Lambda function
            function_triggers = lambda_client.list_event_source_mappings(
                FunctionName=function)
            for trigger in function_triggers['EventSourceMappings']:
                if trigger['EventSourceArn'] == stream_arn:
                    functions.append(function)
        return functions
    except Exception as ex:
        print(ex)
        raise
    


def removeTriggerFromLambdaFunctions(lambda_client, stream_arn, lambda_functions):
    for function in lambda_functions:
        print("Removing Stream From Function {}".format(function))
        # Get the list of triggers for the Lambda function
        function_triggers = lambda_client.list_event_source_mappings(
            FunctionName=function)
        for trigger in function_triggers['EventSourceMappings']:
            if trigger['EventSourceArn'] == stream_arn:
                lambda_client.delete_event_source_mapping(UUID=trigger['UUID'])
        print("Dest table old stream arn triggers deleted from lambda functions")


def attachTriggerToLambdaFunctions(lambda_client, stream_arn, lambda_functions):
    for function in lambda_functions:
        # create a new trigger configuration for the DynamoDB table stream
        trigger_config = {
            'EventSourceArn': stream_arn,
            'FunctionName': function,
            'Enabled': True,
            'StartingPosition': 'LATEST',
            'BatchSize': 100
        }

        # add the trigger to the Lambda function
        response = lambda_client.create_event_source_mapping(**trigger_config)
        print("Created new trigger on function {} with new stream arn {}.".format(
            function, stream_arn))


def enableStreamOnTable(dynamo_client, table_name):
    # specify the type of data to include in the stream
    stream_view_type = 'NEW_AND_OLD_IMAGES'
    response = dynamo_client.update_table(
        TableName=table_name,
        StreamSpecification={
            'StreamEnabled': True,
            'StreamViewType': stream_view_type
        })
    print("Stream enabled on table {}".format(table_name))


def get_table_names(source_app_id, dest_app_id, source_appsync_client, dest_appsync_client):
    data_source_list = source_appsync_client.list_data_sources(
        apiId=source_app_id)
    for item in data_source_list["dataSources"]:
        if item["type"] != "NONE":
            source_table_names.append(item["dynamodbConfig"]["tableName"])

    data_source_list = dest_appsync_client.list_data_sources(apiId=dest_app_id)
    for item in data_source_list["dataSources"]:
        if item["type"] != "NONE":
            dest_table_names.append(item["dynamodbConfig"]["tableName"])

    return source_table_names, dest_table_names


def create_backup(dynamo_client, table_name):
    response = dynamo_client.create_backup(
        TableName=table_name,
        BackupName=table_name+'_backup'
    )
    return response["BackupDetails"]["BackupArn"], response["BackupDetails"]["BackupStatus"]


def backup_status(dynamo_client, backup_arn):
    response = dynamo_client.describe_backup(
        BackupArn=backup_arn
    )
    return response['BackupDescription']["BackupDetails"]["BackupStatus"]


def delete_table(dynamo_client, table_name):
    response = dynamo_client.delete_table(
        TableName=table_name
    )
    print("Table {} deletion is in progress. Please wait...".format(table_name))
    try:
        table_status = get_table_status(dynamo_client, table_name, 2)
        print(table_status)
        while table_status == 'DELETING':
            table_status = get_table_status(dynamo_client, table_name, 10)
        print(table_status)
    except Exception as ex:
        print(ex)
        print("Table deleted successfully: {}".format(table_name))

    return response


def restore_table(dynamo_client, table_name, backup_arn):
    response = dynamo_client.restore_table_from_backup(
        TargetTableName=table_name,
        BackupArn=backup_arn
    )
    print("Table {} Restoration is in progress. Please wait...".format(table_name))
    try:
        table_status = get_table_status(dynamo_client, table_name, 5)
        print(table_status)
        while table_status == 'CREATING':
            table_status = get_table_status(dynamo_client, table_name, 5)
        print(table_status)
    except Exception as ex:
        print(ex)
        print("Table created successfully: {}".format(table_name))

    return response


def table_exists(dynamo_client, table_name):
    table_list = dynamo_client.list_tables()
    if table_name in table_list['TableNames']:
        return True

    while 'LastEvaluatedTableName' in table_list:
        table_list = dynamo_client.list_tables(
            ExclusiveStartTableName=table_list['LastEvaluatedTableName'])
        if table_name in table_list['TableNames']:
            return True
    return False


def is_equal_table(source_table_names, dest_table_names):
    if (len(source_table_names) != len(dest_table_names)):
        if (len(source_table_names) > len(dest_table_names)):
            print("Source table list has more tables than destination table list. Both list should have equal number of tables.")
        else:
            print("Destinaion table list has more tables than source table list. Both list should have equal number of tables.")
        return False
    return True


def get_table_status(dynamo_client, table_name, delay=0):
    time.sleep(delay)
    table_status = dynamo_client.describe_table(TableName=table_name)[
        'Table']['TableStatus']
    return table_status


def restore_queue(restore_list, count):
    table_name = ''
    while (len(restore_list) == 4):
        print("There are already 4 concurrent restores. Waiting for at least one to complete to restore next table.")
        table_name = restore_list[count]
        if (table_exists(table_name) is True):
            table_status = get_table_status(table_name)
            if table_status == 'ACTIVE':
                print(table_name + ' table is in ' + table_status + ' state')
                break
            else:
                print(table_name + ' table is in ' + table_status + ' state')
        else:
            print('Unable to get the status of ' + table_name + " table.")
        if count == 3:
            count = 0
        else:
            count += 1
    if len(restore_list) == 4:
        print('Restore list: ' + str(restore_list))
        restore_list.remove(table_name)
    return restore_list


def restoreTableThread(source_table, dest_table, 
            dest_lambda_client, dest_lambda_funcs,
            source_dynamo_client, dest_dynamo_client,
            source_appsync_client, dest_appsync_client):
    
    print("...............................................................")
    print("Processing Table: {}".format(dest_table))
    # Replacing this with threads
    # count = 0
    # restore_queue(restore_list, count)
    dest_table_stream_arn = None
    dest_table_stream_lambda_functions = []
    if (table_exists(dest_dynamo_client, dest_table) is True):
        print("Dest Table {} Exists".format(dest_table))
        # Get Stream Arn of current destination tables
        dest_table_stream_arn = getTableStreamArn(
            dest_dynamo_client, dest_table)
        if dest_table_stream_arn != None:
            # Get List of Lambda Function which used dest table stream in triggers.
            dest_table_stream_lambda_functions = getLambdaFunctionAttachedToStream(
                dest_lambda_client, dest_table_stream_arn, dest_lambda_funcs)
            print("Destination Table Associated Lambda Functions: {}".format(
                dest_table_stream_lambda_functions))
        print("Deleting Dest table {}".format(dest_table))
        delete_table(dest_dynamo_client, dest_table)
    if (table_exists(source_dynamo_client, source_table) is True):
        print("Taking backup of " + source_table + " table")
        backup_arn, status = create_backup(source_dynamo_client, source_table)
        while (status != 'AVAILABLE'):
            status = backup_status(source_dynamo_client, backup_arn)
        print('Backup for ' + source_table + ' is in ' + status + ' state')
        global backup_arns
        with lock:
            backup_arns.append(backup_arn)

        if (table_exists(dest_dynamo_client, dest_table) is False):
            try:
                print("Restoring {} table".format(dest_table))
                restore_table(dest_dynamo_client, dest_table, backup_arn)
                try:
                    if dest_table_stream_arn != None and len(dest_table_stream_lambda_functions) != 0:
                        # Delete existing triggers of current stream from lambda functions
                        removeTriggerFromLambdaFunctions(
                            dest_lambda_client, dest_table_stream_arn, dest_table_stream_lambda_functions)
                        time.sleep(10)
                        # Enable Stream on restored table
                        enableStreamOnTable(dest_dynamo_client, dest_table)
                        print("Enabling Stream on Dest Table: {}, and wait for 10 seconds.".format(
                            dest_table))
                        time.sleep(10)
                        # Get New Stream Arn of restored table
                        print("Get new stream ARN")
                        dest_table_stream_arn = getTableStreamArn(
                            dest_dynamo_client, dest_table)
                        print("After restore stream arn of table {} is {}".format(
                            dest_table, dest_table_stream_arn))
                        if dest_table_stream_arn:
                            # Create Triggers with new stream id on Lambda Functions
                            attachTriggerToLambdaFunctions(
                                dest_lambda_client, dest_table_stream_arn, dest_table_stream_lambda_functions)
                    # restore_list.append(dest_table)
                except Exception as ex:
                    print(ex)
                    print("Unable to attach triggers for table {} stream on lambda functions due to above exception".format(
                        dest_table))
            except Exception as ex:
                print(ex)
                print(
                    "Table {} restoration failed due to above error.".format(dest_table))
        else:
            print("Can't restore table {} because its deletion still in progress.".format(
                dest_table))
    else:
        print("Source Table ({}) does not exist. Skiping this table from restoration process.".format(
            source_table))


def startAndWait():
    print("Starting {} Threads".format(len(threads)))
    # start all the threads
    for thread in threads:
        thread.start()
    print("Waiting Threads to Finish")
    # wait for all the threads to finish
    for thread in threads:
        try:
            thread.join()
        except Exception as ex:
            print(ex)
            raise
    print("All threads are finished now")


if __name__ == '__main__':
    # Get Glue Job Parameters
    try:
        JOB_PARAMETERS = ['source_table_names', 'source_appsync_id', 'dest_table_names',
                          'dest_appsync_id', 'source_region', 'dest_region', 'dest_env_name', 'sleep', 'tries']
        args = getResolvedOptions(sys.argv, JOB_PARAMETERS)
        print("Arguments:{}".format(args))
        source_table_names = args['source_table_names']
        dest_table_names = args['dest_table_names']
        if source_table_names == 'false':
            source_table_names = []
        else:
            source_table_names = source_table_names.split(",")
        if dest_table_names == 'false':
            dest_table_names = []
        else:
            dest_table_names = dest_table_names.split(",")

        source_appsync_id = args['source_appsync_id']
        dest_appsync_id = args['dest_appsync_id']

        source_region = args['source_region']
        dest_region = args['dest_region']

        dest_env_name = "-{}".format(args['dest_env_name'])

        tries = int(args['tries'])
        sleep = int(args['sleep'])
    except Exception as ex:
        print(ex)
        print("Invalid or incomplete parameters provided for the glue job")
        exit()

    if not source_table_names and not dest_table_names:
        print("Table names not provided. Fetching table names from provided AppSync IDs")
        source_table_names, dest_table_names = get_table_names(
            source_appsync_id, dest_appsync_id)
    else:
        print("Table names provided.")
    print("Source table names list: {}".format(source_table_names))
    print("Destination table names list: {}".format(dest_table_names))

    if (is_equal_table(source_table_names, dest_table_names) is False):
        exit()
    source_table_names = sorted(source_table_names)
    dest_table_names = sorted(dest_table_names)
    source_dynamo_client = session.client(
        'dynamodb', region_name=source_region)
    dest_dynamo_client = session.client('dynamodb', region_name=dest_region)

    source_appsync_client = session.client(
        'appsync', region_name=source_region)
    dest_appsync_client = session.client('appsync', region_name=dest_region)

    dest_lambda_client = session.client('lambda', region_name=dest_region)
    # Get List of all lambda functions of destination environment
    dest_lambda_funcs = getLambdaFunctionNames(
        dest_lambda_client, dest_env_name)
    print("Destination Environment Lambda Functions: {}".format(dest_lambda_funcs))

    concurrency = 5
    global threads
    threads = []
    for i in range(len(source_table_names)):
        t = threading.Thread(target=restoreTableThread, args=(
            source_table_names[i], dest_table_names[i], 
            dest_lambda_client, dest_lambda_funcs,
            source_dynamo_client, dest_dynamo_client,
            source_appsync_client, dest_appsync_client))
        threads.append(t)
        if i < len(source_table_names)-1 and len(threads) < concurrency:
            continue
        else:
            startAndWait()
            threads = []
            

    print("List of backup ARNS: {}".format(backup_arns))
