# hera

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Git Workflow
1. Make sure you are working in amplify dev env with `"amplify env checkout dev"`.
2. In VS Code click the branch name in the lower left corner. Then select "create new branch from…"  and enter in a name following this format: `"feature/new-feature-name"`.Prefix could also be `"bugfix"` or `"hotfix"`. Then select `"origin/dev"` as the repo to copy from.
3. Work on your dev feature branch. When the feature is finished, publish your changes.
4. Log in to BitBucket.com and create a pull request to merge into the `"dev"` branch.
5. Once the pull request is made, send QA a link to the AWS preview link auto created so they can test the new feature.
6. If approved the lead dev will merge the request into DEV which is now ready for customer testing.
7. You'll occasionally want to clean up old feature branches in VS Code. You can do so by doing CMD+ SHIFT+P and selecting "GIT: Delete Branch…" 


### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### Project Setup
Clone Repo
CD into folder and `npm install`
Install the amplify cli `sudo npm install -g @aws-amplify/cli`
run `amplify configure`
create aws user account with all defaults. Enter into terminal your access key and secret access key. Store them somewhere as you can't get the secret key again.
Save as your default profile.
run `amplify pull`
answer the questions thusly (Mostly defaults)

```
? Do you want to use an AWS profile? Yes
? Please choose the profile you want to use default
? Which app are you working on? d2utgk3kc5cfzv
? Pick a backend environment: dev
? Choose your default editor: Visual Studio Code
? Choose the type of app that you're building javascript
Please tell us about your project
? What javascript framework are you using vue
? Source Directory Path:  src
? Distribution Directory Path: dist
? Build Command:  npm run-script build
? Start Command: npm run-script serve
? Test Command: npm run test
```
Create a user in the dev cognito pool so you can login. 

### How to merge a pull request with Merge Conflicts
First do a a git fetch. Then you can do a `git merge feature/new-feature` on the dev branch.
From here you should probably do a `amplify pull` and `amplify codegen` so that stuff is all up to date. Then just merge in any conflicts and commit on the dev branch. Once you push the pull request should have been removed. You can now also delete the feature branch from your local system.

### How to run a local instance of the API and DynamoDB tables. 
There are 2 things you need to get before you start this process:
- User info
- Tenant info
1. Make sure you are using latest amplify-cli instance by running `npm install -g @aws-amplify/cli` (5.0.1).
2. Run `amplify mock` this will create a local instance of the api and a local instance of dynamodb. 
3. For the first time you will need to create your user and tenant using a mutation on the graphql playground run by amplify.
4. Happy coding!

```
mutation CreateTenant {
  createTenant(
    input: {
      id: "<Insert ID here>",
      companyName: "<Insert Company Name here>",
      trialExpDate: "2026-05-14T04:00:00.000Z",
      accountPremiumStatus: "trial",
      stripeBillingEmail: "<Insert Email>",
      group: "<Insert Group>"
    }
  )
  {
    id
  }
}

mutation CreateUser {
  createUser(
    input:{
      group: "<Insert Group>",
      cognitoSub: "<Insert Cognito Sub>",
      firstName: "<Insert First Name>",
      lastName: "<Insert Last Name>",
      email: "<Insert Email>",
      emailVerified: true,
      role: "admin",
      id:""<Insert User ID>"",
      userTenantId:"<Insert Tenant ID>"
    }
  ){
    id
  }
}
```

### How to run tests

set the following environment variables

```
COGNITO_SUPERADMIN=false
COGNITO_USERNAME=lmallmap2021+e2e@gmail.com
COGNITO_PASSWORD=yl240211
```

