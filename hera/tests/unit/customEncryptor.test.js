const { encrypt, decrypt } = require("../../amplify/backend/function/ParsePerformanceData/src/customEncryptor");

test("Custom Encryptor - Encrypt text", () => {
    let value = "Some text string";
    let encryptedValue = encrypt(value, 'example');
    console.debug({value, encryptedValue})
    expect(encryptedValue).toBe('a7596a0e702f8a2b73e5c80aec325e78beb4414b09062f8045faf4e1243d6929');
});

test("Custom Encryptor - Decrypt text", () => {
    let encryptedValue = "a7596a0e702f8a2b73e5c80aec325e78beb4414b09062f8045faf4e1243d6929";
    let value = decrypt(encryptedValue, 'example');
    console.debug({encryptedValue, value})
    expect(value).toBe('Some text string');
});

