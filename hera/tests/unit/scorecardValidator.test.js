const { scoreCardValidator } =  require('../../amplify/backend/function/ParsePerformanceData/src/scorecardValidator.js');
/** 
 * 1. If CC Opps. is > 0 then SWC-CC must be a decimal percentage with one decimal place (example 100.0%)
 * 
 * a) CC Opps = 25 & SWC-CC = 96.0%-> Success
*/
test("Score card validator", () => {
  let row = {
    name: 'Teneshia Swann',
    transporter_id: 'A149W B369GD99P',
    overal_tier: 'Fantastic',
    delivered: '202',
    key_focus_area: 'DAR',
    speeding_event_rate: '0.0',
    harsh_braking: '0.3',
    harsh_cornering: '0.0',
    seatbelt_off_rate: 'Coming Soon',
    distractions_rate: 'Coming Soon',
    following_distance_rate: 'Coming Soon',
    sign_signal_violations_rate: 'Coming Soon',
    dcr: '99.0%',
    dar: '0',
    swc_pod: '88.8%',
    swc_cc: '96.0%',
    star_rating: 'Coming Soon',
    dslp: '100.00%',
    dnrs: '2',
    pod_opps: '98',
    cc_opps: '25'
  }

  const toBeValue = []
  let result = scoreCardValidator(row)
  expect(result).toStrictEqual(toBeValue);
});

// b) CC Opps = 25 & SWC-CC = 96.00% -> Error
test("Score card validator", () => {
  let row = {
    name: 'Teneshia Swann',
    transporter_id: 'A149W B369GD99P',
    overal_tier: 'Fantastic',
    delivered: '202',
    key_focus_area: 'DAR',
    speeding_event_rate: '0.0',
    harsh_braking: '0.3',
    harsh_cornering: '0.0',
    seatbelt_off_rate: 'Coming Soon',
    distractions_rate: 'Coming Soon',
    following_distance_rate: 'Coming Soon',
    sign_signal_violations_rate: 'Coming Soon',
    dcr: '99.0%',
    dar: '0',
    swc_pod: '88.8%',
    swc_cc: '96.00%',
    star_rating: 'Coming Soon',
    dslp: '100.00%',
    dnrs: '2',
    pod_opps: '98',
    cc_opps: '25'
  }

  const toBeValue = [
    'ERROR_002_SCORECARD_VALIDATOR_001 (002.001)'
  ]
  let result = scoreCardValidator(row)
  expect(result).toStrictEqual(toBeValue);
});

// c) CC Opps = 25 & SWC-CC = 96% -> Error
test("Score card validator", () => {
  let row = {
    name: 'Teneshia Swann',
    transporter_id: 'A149W B369GD99P',
    overal_tier: 'Fantastic',
    delivered: '202',
    key_focus_area: 'DAR',
    speeding_event_rate: '0.0',
    harsh_braking: '0.3',
    harsh_cornering: '0.0',
    seatbelt_off_rate: 'Coming Soon',
    distractions_rate: 'Coming Soon',
    following_distance_rate: 'Coming Soon',
    sign_signal_violations_rate: 'Coming Soon',
    dcr: '99.0%',
    dar: '0',
    swc_pod: '88.8%',
    swc_cc: '96%',
    star_rating: 'Coming Soon',
    dslp: '100.00%',
    dnrs: '2',
    pod_opps: '98',
    cc_opps: '25'
  }

  const toBeValue = [
    'ERROR_002_SCORECARD_VALIDATOR_001 (002.001)'
  ]
  let result = scoreCardValidator(row)
  expect(result).toStrictEqual(toBeValue);
});

// d) CC Opps = 25 & SWC-CC =  -> Error
test("Score card validator", () => {
  let row = {
    name: 'Teneshia Swann',
    transporter_id: 'A149W B369GD99P',
    overal_tier: 'Fantastic',
    delivered: '202',
    key_focus_area: 'DAR',
    speeding_event_rate: '0.0',
    harsh_braking: '0.3',
    harsh_cornering: '0.0',
    seatbelt_off_rate: 'Coming Soon',
    distractions_rate: 'Coming Soon',
    following_distance_rate: 'Coming Soon',
    sign_signal_violations_rate: 'Coming Soon',
    dcr: '99.0%',
    dar: '0',
    swc_pod: '88.8%',
    swc_cc: '',
    star_rating: 'Coming Soon',
    dslp: '100.00%',
    dnrs: '2',
    pod_opps: '98',
    cc_opps: '25'
  }

  const toBeValue = [
    'ERROR_002_SCORECARD_VALIDATOR_001 (002.001)'
  ]
  let result = scoreCardValidator(row)
  expect(result).toStrictEqual(toBeValue);
});

/**
 * 2. If CC Opps. = 0 then SWC-CC must be empty 
 * a) CC Opps = 0 & SWC-CC =  -> SUCCESS
 */
test("Score card validator", () => {
  let row = {
    name: 'Teneshia Swann',
    transporter_id: 'A149W B369GD99P',
    overal_tier: 'Fantastic',
    delivered: '202',
    key_focus_area: 'DAR',
    speeding_event_rate: '0.0',
    harsh_braking: '0.3',
    harsh_cornering: '0.0',
    seatbelt_off_rate: 'Coming Soon',
    distractions_rate: 'Coming Soon',
    following_distance_rate: 'Coming Soon',
    sign_signal_violations_rate: 'Coming Soon',
    dcr: '99.0%',
    dar: '0',
    swc_pod: '88.8%',
    swc_cc: '',
    star_rating: 'Coming Soon',
    dslp: '100.00%',
    dnrs: '2',
    pod_opps: '98',
    cc_opps: '0'
  }

  const toBeValue = []
  let result = scoreCardValidator(row)
  expect(result).toStrictEqual(toBeValue);
});

/**
 * b) CC Opps = 0 & SWC-CC = null -> SUCCESS
 */
test("Score card validator", () => {
  let row = {
    name: 'Teneshia Swann',
    transporter_id: 'A149W B369GD99P',
    overal_tier: 'Fantastic',
    delivered: '202',
    key_focus_area: 'DAR',
    speeding_event_rate: '0.0',
    harsh_braking: '0.3',
    harsh_cornering: '0.0',
    seatbelt_off_rate: 'Coming Soon',
    distractions_rate: 'Coming Soon',
    following_distance_rate: 'Coming Soon',
    sign_signal_violations_rate: 'Coming Soon',
    dcr: '99.0%',
    dar: '0',
    swc_pod: '88.8%',
    swc_cc: null,
    star_rating: 'Coming Soon',
    dslp: '100.00%',
    dnrs: '2',
    pod_opps: '98',
    cc_opps: '0'
  }

  const toBeValue = []
  let result = scoreCardValidator(row)
  expect(result).toStrictEqual(toBeValue);
});

/**
 * b) CC Opps = 0 & SWC-CC = 52.0% -> ERROR
 */
 test("Score card validator", () => {
  let row = {
    name: 'Teneshia Swann',
    transporter_id: 'A149W B369GD99P',
    overal_tier: 'Fantastic',
    delivered: '202',
    key_focus_area: 'DAR',
    speeding_event_rate: '0.0',
    harsh_braking: '0.3',
    harsh_cornering: '0.0',
    seatbelt_off_rate: 'Coming Soon',
    distractions_rate: 'Coming Soon',
    following_distance_rate: 'Coming Soon',
    sign_signal_violations_rate: 'Coming Soon',
    dcr: '99.0%',
    dar: '0',
    swc_pod: '88.8%',
    swc_cc: '52.0%',
    star_rating: 'Coming Soon',
    dslp: '100.00%',
    dnrs: '2',
    pod_opps: '98',
    cc_opps: '0'
  }

  const toBeValue = [
    'ERROR_002_SCORECARD_VALIDATOR_002 (002.002)'
  ]
  let result = scoreCardValidator(row)
  expect(result).toStrictEqual(toBeValue);
});

/**
 * b) CC Opps = 0 & SWC-CC = 52% -> ERROR
 */
 test("Score card validator", () => {
  let row = {
    name: 'Teneshia Swann',
    transporter_id: 'A149W B369GD99P',
    overal_tier: 'Fantastic',
    delivered: '202',
    key_focus_area: 'DAR',
    speeding_event_rate: '0.0',
    harsh_braking: '0.3',
    harsh_cornering: '0.0',
    seatbelt_off_rate: 'Coming Soon',
    distractions_rate: 'Coming Soon',
    following_distance_rate: 'Coming Soon',
    sign_signal_violations_rate: 'Coming Soon',
    dcr: '99.0%',
    dar: '0',
    swc_pod: '88.8%',
    swc_cc: '52%',
    star_rating: 'Coming Soon',
    dslp: '100.00%',
    dnrs: '2',
    pod_opps: '98',
    cc_opps: '0'
  }

  const toBeValue = [
    'ERROR_002_SCORECARD_VALIDATOR_002 (002.002)'
  ]
  let result = scoreCardValidator(row)
  expect(result).toStrictEqual(toBeValue);
});

/** 
 * 3. If POD Opps. is > 0 then  SWC-POD must be a decimal percentage with one decimal place (example 100.0%)
 * 
 * a) POD Opps = 25 & SWC-POD = 88.8% -> Success
*/
test("Score card validator", () => {
  let row = {
    name: 'Teneshia Swann',
    transporter_id: 'A149WB369GD99P',
    overal_tier: 'Fantastic',
    delivered: '202',
    key_focus_area: 'DAR',
    speeding_event_rate: '0.0',
    harsh_braking: '0.3',
    harsh_cornering: '0.0',
    seatbelt_off_rate: 'Coming Soon',
    distractions_rate: 'Coming Soon',
    following_distance_rate: 'Coming Soon',
    sign_signal_violations_rate: 'Coming Soon',
    dcr: '99.0%',
    dar: '0',
    swc_pod: '88.8%',
    swc_cc: '96.6%',
    star_rating: 'Coming Soon',
    dslp: '100.00%',
    dnrs: '2',
    pod_opps: '98',
    cc_opps: '25'
  }

  const toBeValue = []
  let result = scoreCardValidator(row)
  expect(result).toStrictEqual(toBeValue);
});

// b) POD Opps = 0 & SWC-POD = 88.8% -> PASS
test("Score card validator", () => {
  let row = {
    name: 'Teneshia Swann',
    transporter_id: 'A149W B369GD99P',
    overal_tier: 'Fantastic',
    delivered: '202',
    key_focus_area: 'DAR',
    speeding_event_rate: '0.0',
    harsh_braking: '0.3',
    harsh_cornering: '0.0',
    seatbelt_off_rate: 'Coming Soon',
    distractions_rate: 'Coming Soon',
    following_distance_rate: 'Coming Soon',
    sign_signal_violations_rate: 'Coming Soon',
    dcr: '99.0%',
    dar: '0',
    swc_pod: '88.8%',
    swc_cc: '96.0%',
    star_rating: 'Coming Soon',
    dslp: '100.00%',
    dnrs: '2',
    pod_opps: '0',
    cc_opps: '25'
  }

  const toBeValue = []
  let result = scoreCardValidator(row)
  expect(result).toStrictEqual(toBeValue);
});


// c) POD Opps = -5 & SWC-POD = 960% -> PASS
test("Score card validator", () => {
  let row = {
    name: 'Teneshia Swann',
    transporter_id: 'A149W B369GD99P',
    overal_tier: 'Fantastic',
    delivered: '202',
    key_focus_area: 'DAR',
    speeding_event_rate: '0.0',
    harsh_braking: '0.3',
    harsh_cornering: '0.0',
    seatbelt_off_rate: 'Coming Soon',
    distractions_rate: 'Coming Soon',
    following_distance_rate: 'Coming Soon',
    sign_signal_violations_rate: 'Coming Soon',
    dcr: '99.0%',
    dar: '0',
    swc_pod: '88.8%',
    swc_cc: '960%',
    star_rating: 'Coming Soon',
    dslp: '100.00%',
    dnrs: '2',
    pod_opps: '-5',
    cc_opps: '25'
  }

  const toBeValue = [
    'ERROR_002_SCORECARD_VALIDATOR_001 (002.001)'
  ]
  let result = scoreCardValidator(row)
  expect(result).toStrictEqual(toBeValue);
});


// d) POD Opps = 5 & SWC-POD = 888% -> PASS
test("Score card validator", () => {
  let row = {
    name: 'Teneshia Swann',
    transporter_id: 'A149W B369GD99P',
    overal_tier: 'Fantastic',
    delivered: '202',
    key_focus_area: 'DAR',
    speeding_event_rate: '0.0',
    harsh_braking: '0.3',
    harsh_cornering: '0.0',
    seatbelt_off_rate: 'Coming Soon',
    distractions_rate: 'Coming Soon',
    following_distance_rate: 'Coming Soon',
    sign_signal_violations_rate: 'Coming Soon',
    dcr: '99.0%',
    dar: '0',
    swc_pod: '888%',
    swc_cc: '960%',
    star_rating: 'Coming Soon',
    dslp: '100.00%',
    dnrs: '2',
    pod_opps: '5',
    cc_opps: '25'
  }

  const toBeValue = [
    'ERROR_002_SCORECARD_VALIDATOR_001 (002.001)',
    'ERROR_002_SCORECARD_VALIDATOR_003 (002.003)'
  ]
  let result = scoreCardValidator(row)
  expect(result).toStrictEqual(toBeValue);
});

/** 
 * 4. If DNRs = 0 then DAR must = 100
 * 
 * a) DNRs = 0 & DAR = 100 -> Success
*/
test("Score card validator", () => {
  let row = {
    name: 'Teneshia Swann',
    transporter_id: 'A149WB369GD99P',
    overal_tier: 'Fantastic',
    delivered: '202',
    key_focus_area: 'DAR',
    speeding_event_rate: '0.0',
    harsh_braking: '0.3',
    harsh_cornering: '0.0',
    seatbelt_off_rate: 'Coming Soon',
    distractions_rate: 'Coming Soon',
    following_distance_rate: 'Coming Soon',
    sign_signal_violations_rate: 'Coming Soon',
    dcr: '99.0%',
    dar: '100',
    swc_pod: '88.8%',
    swc_cc: '96.6%',
    star_rating: 'Coming Soon',
    dslp: '100.00%',
    dnrs: '0',
    pod_opps: '98',
    cc_opps: '25'
  }

  const toBeValue = []
  let result = scoreCardValidator(row)
  expect(result).toStrictEqual(toBeValue);
});

 // b) DNRs = 0 & DAR = 88 -> ERROR
test("Score card validator", () => {
  let row = {
    name: 'Teneshia Swann',
    transporter_id: 'A149WB369GD99P',
    overal_tier: 'Fantastic',
    delivered: '202',
    key_focus_area: 'DAR',
    speeding_event_rate: '0.0',
    harsh_braking: '0.3',
    harsh_cornering: '0.0',
    seatbelt_off_rate: 'Coming Soon',
    distractions_rate: 'Coming Soon',
    following_distance_rate: 'Coming Soon',
    sign_signal_violations_rate: 'Coming Soon',
    dcr: '99.0%',
    dar: '88',
    swc_pod: '88.8%',
    swc_cc: '96.6%',
    star_rating: 'Coming Soon',
    dslp: '100.00%',
    dnrs: '0',
    pod_opps: '98',
    cc_opps: '25'
  }

  const toBeValue = [
    'ERROR_002_SCORECARD_VALIDATOR_004 (002.004)'
  ]
  let result = scoreCardValidator(row)
  expect(result).toStrictEqual(toBeValue);
});

/**
 * 4. If DNRs != 0 then DAR must be < 100
 */
 // a) DNRs = 5 & DAR = 88 -> ERROR
 test("Score card validator", () => {
  let row = {
    name: 'Teneshia Swann',
    transporter_id: 'A149WB369GD99P',
    overal_tier: 'Fantastic',
    delivered: '202',
    key_focus_area: 'DAR',
    speeding_event_rate: '0.0',
    harsh_braking: '0.3',
    harsh_cornering: '0.0',
    seatbelt_off_rate: 'Coming Soon',
    distractions_rate: 'Coming Soon',
    following_distance_rate: 'Coming Soon',
    sign_signal_violations_rate: 'Coming Soon',
    dcr: '99.0%',
    dar: '88',
    swc_pod: '88.8%',
    swc_cc: '96.6%',
    star_rating: 'Coming Soon',
    dslp: '100.00%',
    dnrs: '5',
    pod_opps: '98',
    cc_opps: '25'
  }

  const toBeValue = []
  let result = scoreCardValidator(row)
  expect(result).toStrictEqual(toBeValue);
});

 // a) DNRs = 1 & DAR = 101 -> ERROR
 test("Score card validator", () => {
  let row = {
    name: 'Teneshia Swann',
    transporter_id: 'A149WB369GD99P',
    overal_tier: 'Fantastic',
    delivered: '202',
    key_focus_area: 'DAR',
    speeding_event_rate: '0.0',
    harsh_braking: '0.3',
    harsh_cornering: '0.0',
    seatbelt_off_rate: 'Coming Soon',
    distractions_rate: 'Coming Soon',
    following_distance_rate: 'Coming Soon',
    sign_signal_violations_rate: 'Coming Soon',
    dcr: '99.0%',
    dar: '101',
    swc_pod: '88.8%',
    swc_cc: '96.6%',
    star_rating: 'Coming Soon',
    dslp: '100.00%',
    dnrs: '1',
    pod_opps: '98',
    cc_opps: '25'
  }

  const toBeValue = [
    'ERROR_002_SCORECARD_VALIDATOR_005 (002.005)'
  ]
  let result = scoreCardValidator(row)
  expect(result).toStrictEqual(toBeValue);
});

/**
 * 5. Overall Tier
 */
// 1. Overall Tier = 'Fantastic'
 test("Score card validator", () => {
  let row = {
    name: 'Teneshia Swann',
    transporter_id: 'A149WB369GD99P',
    overal_tier: 'Fantastic',
    delivered: '202',
    key_focus_area: 'DAR',
    speeding_event_rate: '0.0',
    harsh_braking: '0.3',
    harsh_cornering: '0.0',
    seatbelt_off_rate: 'Coming Soon',
    distractions_rate: 'Coming Soon',
    following_distance_rate: 'Coming Soon',
    sign_signal_violations_rate: 'Coming Soon',
    dcr: '99.0%',
    dar: '99',
    swc_pod: '88.8%',
    swc_cc: '96.6%',
    star_rating: 'Coming Soon',
    dslp: '100.00%',
    dnrs: '1',
    pod_opps: '98',
    cc_opps: '25'
  }

  const toBeValue = []
  let result = scoreCardValidator(row)
  expect(result).toStrictEqual(toBeValue);
});

// 2. Overall Tier = 'Great'
 test("Score card validator", () => {
  let row = {
    name: 'Teneshia Swann',
    transporter_id: 'A149WB369GD99P',
    overal_tier: 'Great',
    delivered: '202',
    key_focus_area: 'DAR',
    speeding_event_rate: '0.0',
    harsh_braking: '0.3',
    harsh_cornering: '0.0',
    seatbelt_off_rate: 'Coming Soon',
    distractions_rate: 'Coming Soon',
    following_distance_rate: 'Coming Soon',
    sign_signal_violations_rate: 'Coming Soon',
    dcr: '99.0%',
    dar: '99',
    swc_pod: '88.8%',
    swc_cc: '96.6%',
    star_rating: 'Coming Soon',
    dslp: '100.00%',
    dnrs: '1',
    pod_opps: '98',
    cc_opps: '25'
  }

  const toBeValue = []
  let result = scoreCardValidator(row)
  expect(result).toStrictEqual(toBeValue);
});

// 3. Overall Tier = 'Fair'
 test("Score card validator", () => {
  let row = {
    name: 'Teneshia Swann',
    transporter_id: 'A149WB369GD99P',
    overal_tier: 'Fair',
    delivered: '202',
    key_focus_area: 'DAR',
    speeding_event_rate: '0.0',
    harsh_braking: '0.3',
    harsh_cornering: '0.0',
    seatbelt_off_rate: 'Coming Soon',
    distractions_rate: 'Coming Soon',
    following_distance_rate: 'Coming Soon',
    sign_signal_violations_rate: 'Coming Soon',
    dcr: '99.0%',
    dar: '99',
    swc_pod: '88.8%',
    swc_cc: '96.6%',
    star_rating: 'Coming Soon',
    dslp: '100.00%',
    dnrs: '1',
    pod_opps: '98',
    cc_opps: '25'
  }

  const toBeValue = []
  let result = scoreCardValidator(row)
  expect(result).toStrictEqual(toBeValue);
});

// 4. Overall Tier = 'Poor'
 test("Score card validator", () => {
  let row = {
    name: 'Teneshia Swann',
    transporter_id: 'A149WB369GD99P',
    overal_tier: 'Poor',
    delivered: '202',
    key_focus_area: 'DAR',
    speeding_event_rate: '0.0',
    harsh_braking: '0.3',
    harsh_cornering: '0.0',
    seatbelt_off_rate: 'Coming Soon',
    distractions_rate: 'Coming Soon',
    following_distance_rate: 'Coming Soon',
    sign_signal_violations_rate: 'Coming Soon',
    dcr: '99.0%',
    dar: '99',
    swc_pod: '88.8%',
    swc_cc: '96.6%',
    star_rating: 'Coming Soon',
    dslp: '100.00%',
    dnrs: '1',
    pod_opps: '98',
    cc_opps: '25'
  }

  const toBeValue = []
  let result = scoreCardValidator(row)
  expect(result).toStrictEqual(toBeValue);
});

// 5. Overall Tier = 'Coming Soon'
 test("Score card validator", () => {
  let row = {
    name: 'Teneshia Swann',
    transporter_id: 'A149WB369GD99P',
    overal_tier: 'Coming Soon',
    delivered: '202',
    key_focus_area: 'DAR',
    speeding_event_rate: '0.0',
    harsh_braking: '0.3',
    harsh_cornering: '0.0',
    seatbelt_off_rate: 'Coming Soon',
    distractions_rate: 'Coming Soon',
    following_distance_rate: 'Coming Soon',
    sign_signal_violations_rate: 'Coming Soon',
    dcr: '99.0%',
    dar: '99',
    swc_pod: '88.8%',
    swc_cc: '96.6%',
    star_rating: 'Coming Soon',
    dslp: '100.00%',
    dnrs: '1',
    pod_opps: '98',
    cc_opps: '25'
  }

  const toBeValue = []
  let result = scoreCardValidator(row)
  expect(result).toStrictEqual(toBeValue);
});

// 6. Overall Tier = ''
 test("Score card validator", () => {
  let row = {
    name: 'Teneshia Swann',
    transporter_id: 'A149WB369GD99P',
    overal_tier: '',
    delivered: '202',
    key_focus_area: 'DAR',
    speeding_event_rate: '0.0',
    harsh_braking: '0.3',
    harsh_cornering: '0.0',
    seatbelt_off_rate: 'Coming Soon',
    distractions_rate: 'Coming Soon',
    following_distance_rate: 'Coming Soon',
    sign_signal_violations_rate: 'Coming Soon',
    dcr: '99.0%',
    dar: '99',
    swc_pod: '88.8%',
    swc_cc: '96.6%',
    star_rating: 'Coming Soon',
    dslp: '100.00%',
    dnrs: '1',
    pod_opps: '98',
    cc_opps: '25'
  }

  const toBeValue = [
    'ERROR_002_SCORECARD_VALIDATOR_006 (002.006)'
  ]
  let result = scoreCardValidator(row)
  expect(result).toStrictEqual(toBeValue);
});

// 7. Overall Tier = null
 test("Score card validator", () => {
  let row = {
    name: 'Teneshia Swann',
    transporter_id: 'A149WB369GD99P',
    overal_tier: null,
    delivered: '202',
    key_focus_area: 'DAR',
    speeding_event_rate: '0.0',
    harsh_braking: '0.3',
    harsh_cornering: '0.0',
    seatbelt_off_rate: 'Coming Soon',
    distractions_rate: 'Coming Soon',
    following_distance_rate: 'Coming Soon',
    sign_signal_violations_rate: 'Coming Soon',
    dcr: '99.0%',
    dar: '99',
    swc_pod: '88.8%',
    swc_cc: '96.6%',
    star_rating: 'Coming Soon',
    dslp: '100.00%',
    dnrs: '1',
    pod_opps: '98',
    cc_opps: '25'
  }

  const toBeValue = [
    'ERROR_002_SCORECARD_VALIDATOR_006 (002.006)'
  ]

  let result = scoreCardValidator(row)
  expect(result).toStrictEqual(toBeValue);
});