import { cleanVars } from './cleanVars'

test("HERA-1227: CleanVars Function Test", () => {
    const variables = {
        input: {
            input1: '\t\tHola      esto es un texto\n',
            input2: '\tHola      esto es \n\n    un texto\n',
            input3: '\tHola      esto es \n\n    un texto\n.    \u00a0',
            input4: '\tHola      esto es \n\n    un texto\n    \u00a0\u00a0\u00a0\n\u00a0.'
        }
    }

    const result = cleanVars(variables)

    const toBeValue = {
        input: {
            input1: 'Hola esto es un texto',
            input2: 'Hola esto es\nun texto',
            input3: 'Hola esto es\nun texto\n.',
            input4: 'Hola esto es\nun texto\n.'
        }
    }
    expect(result).toStrictEqual(toBeValue)
})