const cleanVars = ( variables ) => {
    //Trim fields   
    const keys = Object.keys(variables.input);
    for (const key of keys) {
      if (typeof(variables.input[key]) === 'string') {

        variables.input[key] = variables.input[key].replace(/\u00a0|\t/g, ' ');

        variables.input[key] = variables.input[key].replace(/ {0,}\n+ {0,}/g, '\n');
        variables.input[key] = variables.input[key].replace(/\n+/g, '\n');
        variables.input[key] = variables.input[key].replace(/  +/g, ' ');

        variables.input[key] = variables.input[key].trim()
      }
    }

    return variables
}

module.exports = {
    cleanVars
}