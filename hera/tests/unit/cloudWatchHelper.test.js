const { CWMetrics } = require("../../amplify/backend/function/ParsePerformanceData/src/cloudWatchMetricHelper");

test("CloudWatch", () => {
    const cwMe = new CWMetrics('UnitTest')
    cwMe.scorecard.metricGroup1({
        byError: "env"
    }).pushMetricData({
        MetricName: 'ERROR_002_SCORECARD_VALIDATOR_001 (002.001)',
        Value: 7,
        Unit: 'Count'
    })

    cwMe.scorecard.metricGroup2({
        bySeverity: "env"
    }).pushMetricData({
        MetricName: "E2E Testing",
        Value: 3,
        Unit: 'Count'
    })
    
    const metricData = cwMe.getMetrics()
    console.debug(metricData)

    expect(cwMe instanceof CWMetrics).toBe(true)
    expect(metricData.Namespace).toBe('UnitTest')
    expect(metricData.MetricData[0].MetricName).toBe("ERROR_002_SCORECARD_VALIDATOR_001 (002.001)")
    expect(metricData.MetricData[0].Unit).toBe("Count")
    expect(metricData.MetricData[0].Value).toBe(7)
    expect(metricData.MetricData[0].Dimensions[0].Name).toBe("byError")
    expect(metricData.MetricData[0].Dimensions[0].Value).toBe("env")
    expect(metricData.MetricData[1].MetricName).toBe("E2E Testing")
    expect(metricData.MetricData[1].Unit).toBe("Count")
    expect(metricData.MetricData[1].Value).toBe(3)
    expect(metricData.MetricData[1].Dimensions[0].Name).toBe("bySeverity")
    expect(metricData.MetricData[1].Dimensions[0].Value).toBe("env")
});
