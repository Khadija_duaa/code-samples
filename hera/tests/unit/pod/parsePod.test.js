const { parseTables } =  require('../../../amplify/backend/function/ParsePerformanceData/src/parseTables.js')
const blocks = require('./blocks.json')
const tables = require('./tables.json')
const farthestRight = require('./farthestRight.json')
const farthestLeft  = require('./farthestLeft.json')

test("Parse table", async () => {

    let version = '202215'
    let templateType = 'podQualityFeedback'

    let result = await parseTables(blocks, tables, farthestRight, farthestLeft, templateType, version)

    const daWithoutTransporterId = result[0].table_data.filter(da => da.transporter_id == null)
    expect(daWithoutTransporterId.length).toBe(0)

    const williamForrest = result[0].table_data.filter(da => da.employee_name == 'William Forrest')

    expect(williamForrest[0].transporter_id).toBe('AWNYFK48KHLM3')
    expect(williamForrest[0].package_not_clearly_visible).toBe('1')
    expect(williamForrest[0].other).toBe('0')

    expect(result[0].table_data.length).toBe(54)
    
})