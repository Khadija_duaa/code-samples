const { parseTables } =  require('../../../../amplify/backend/function/ParsePerformanceData/src/parseTables.js')
const blocks = require('./blocks.json')
const tables = require('./tables.json')
const farthestRight = require('./farthestRight.json')
const farthestLeft  = require('./farthestLeft.json')

xtest("Parse table", async () => {

    let version = '202220'
    let templateType = 'customerFeedback'

    let result = await parseTables(blocks, tables, farthestRight, farthestLeft, templateType, version)

    const daWithoutTransporterId = result[0].table_data.filter(da => da.transporter_id == null)
    expect(daWithoutTransporterId.length).toBe(0)

    const brandonReynolds = result[0].table_data.filter(da => da.driver_name == 'Brandon Reynolds')
    expect(brandonReynolds[0].transporter_id).toBe('ABIODPZZN51YA')
    expect(brandonReynolds[0].da_tier).toBe('Fantastic')
    expect(brandonReynolds[0].delivered_with_care).toBe('1')
    expect(brandonReynolds[0].on_time).toBe('2')

    const kyleJoyce = result[0].table_data.filter(da => da.driver_name == 'Kyle Joyce')
    expect(kyleJoyce[0].transporter_id).toBe('A1OH2D39L9UMA')
    expect(kyleJoyce[0].da_tier).toBe('Coming Soon')
    expect(kyleJoyce[0].friendly).toBe('5')
    expect(kyleJoyce[0].item_damaged).toBe(undefined)

    expect(result[0].table_data.length).toBe(60)
    
})