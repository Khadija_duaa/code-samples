const { parseTables } =  require('../../../../amplify/backend/function/ParsePerformanceData/src/parseTables.js')
const blocks = require('./blocks.json')
const tables = require('./tables.json')
const farthestRight = require('./farthestRight.json')
const farthestLeft  = require('./farthestLeft.json')

xtest("Parse table", async () => {

    let version = '202215'
    let templateType = 'customerFeedback'

    let result = await parseTables(blocks, tables, farthestRight, farthestLeft, templateType, version)

    const daWithoutTransporterId = result[0].table_data.filter(da => da.transporter_id == null)
    expect(daWithoutTransporterId.length).toBe(0)

    const graceGermanx = result[0].table_data.filter(da => da.driver_name == 'Grace German')
    expect(graceGermanx[0].transporter_id).toBe('A1D7IFB7QABI45')
    expect(graceGermanx[0].da_tier).toBe('Poor')
    expect(graceGermanx[0].delivery_was_not_so_great).toBe('4')

    const daJaniqueSmallwood = result[0].table_data.filter(da => da.driver_name == 'DaJanique Smallwood')
    expect(daJaniqueSmallwood[0].transporter_id).toBe('ALFP5C6R5B44F')
    expect(daJaniqueSmallwood[0].da_tier).toBe('Coming Soon')
    expect(daJaniqueSmallwood[0].no_feedback).toBe('89')
    expect(daJaniqueSmallwood[0].item_damaged).toBe('1')

    expect(result[0].table_data.length).toBe(51)
    
})