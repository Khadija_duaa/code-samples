const { generateToken, getShortLink, getFileByToken, getS3FileName } = require("../../amplify/backend/function/ParsePerformanceData/src/s3linkHelper");

let sampleToken

test("s3linkHelper - getS3FileName", () => {
    const key ="path/someExampleKey/abcdefg.yxz"
    const fileName = getS3FileName(key)
    console.debug({key, fileName})
    expect(fileName).toBe('abcdefg.yxz');
});

test("s3linkHelper - generateToken", () => {
    const token = generateToken("path/someExampleKey/abcdefg.yxz",{
        expires: 60 * 60 * 24 *3
    })
    console.debug({token})
    this.sampleToken = token
    expect(token).toContain('798f8feb30d5da71db9d8fb419c0a168af');
});

test("s3linkHelper - getShortLink", () => {
    const sLink = getShortLink("path/someExampleKey/abcdefg.yxz",{
        expires: 60 * 60 * 24 *3
    })
    console.debug({sLink})
    const heraUrl = process.env.HERA_URL
    expect(sLink).toContain(String(heraUrl));
    expect(sLink).toContain('/s3/');
    expect(sLink).toContain('798f8feb30d5da71db9d8fb419c0a168af');
});

test("s3linkHelper - getFileByToken", async () => {
    const token = this.sampleToken
    const file = await getFileByToken(token)
    console.debug({token, file})
    expect(file.s3Key).toBe('path/someExampleKey/abcdefg.yxz');
    expect(file.expirationDate instanceof Date).toBe(true);
    expect(file.URL).toBe('https://s3.amazonaws.com/');
    expect(file.fileName).toBe('abcdefg.yxz');

});
