/**
 * @jest-environment jsdom
 */
import { render, screen, cleanup } from "@testing-library/vue";
import '@testing-library/jest-dom';
import Vue from "vue";
import ElementUI from 'element-ui';
import '@/assets/css/element-variables.scss';
import ContainerFilter from "@/components/TableCard/ContainerFilter.vue";
import SearchBox from "@/components/TableCard/SearchBox.vue";
import CustomTable from "@/components/TableCard/CustomTable.vue";

Vue.use(ElementUI);

describe('Unit Test Components', () => {
  
  xtest( 'Show Component Title and Filters in Vehicle Maintenance Records', async () => {
    // The render method returns a collection of utilities to query your component.
    const { getByText } = render( ContainerFilter, {
      slots: {
        title: '<span>Vehicle Maintenance</span>',
        filters: '<span>Filters</span>',
      },
    })
    expect( getByText( 'Filters' ) ).toBeInTheDocument();
  });

  xtest( 'Show Component Search Bar', async () => {
    const { getByPlaceholderText } = render( SearchBox )
    expect( getByPlaceholderText( 'Type to search this list...' ) ).toBeInTheDocument();
  })
  
  test( 'Show Table Component', async () => {

    const mockColumns = [ 
      { name: "Date", col: "date", fixed: false, width: "120", sortable: false,  sortMethod: undefined }, 
      { name: "Name", col: "name", fixed: false, width: "120", sortable: false,  sortMethod: undefined }, 
      { name: "Address", col: "address", fixed: false, width: "120", sortable: false,  sortMethod: undefined }
    ];
  
    const mockTableData = [
      { date: '2016-05-03', name: 'Tom', address: 'No. 189, Grove St, Los Angeles' }, 
      { date: '2017-08-02', name: 'Omar', address: 'No. 200, Grove St, California' }, 
      { date: '2016-08-04', name: 'Mike', address: 'No. 189, Grove St, Las Vegas' }, 
      { date: '2017-02-01', name: 'Alex', address: 'No. 289, Grove St, Texas' }
    ];
  
    const mockOptionsRows = [
      { label: "View Vehicle", action: "view", divided: false },
      { label: "Add Vehicle Maintenance Record", action: "add", divided: false }
    ];
  
    const mockOptionsHeader = [
      { label: "Export as CSV", action: "exportCSV", divided: false },
    ];
  
    const footerTextTable = "Vehicle Maintenance Records";

    const { updateProps, debug} = render( CustomTable, {
      props: {
        columns: mockColumns,
        data: mockTableData,
        optionsRows: mockOptionsRows,
        optionsHeader: mockOptionsHeader,
        footerTable: footerTextTable
      },
    });

    await Vue.nextTick();
        
    // debug(undefined, Infinity);
    //! refactor this test: currently print two times the component
    expect( screen.getAllByText( /Date/i ) ).toHaveLength( 2 );
    expect( screen.getAllByText( "Export as CSV" ) ).toHaveLength( 2 );
    expect( screen.getByText( `Showing ${mockTableData.length} of 0 Vehicle Maintenance Records` ) ).toBeInTheDocument();
  });
})



