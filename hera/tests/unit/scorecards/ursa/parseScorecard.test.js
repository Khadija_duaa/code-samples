const { validateTransporterId, validateSilentError } = require('../validators.js')
const { parseTables } =  require('../../../../amplify/backend/function/ParsePerformanceData/src/parseTables.js')
const { scoreCardValidator } =  require('../../../../amplify/backend/function/ParsePerformanceData/src/scorecardValidator.js');
const blocks = require('./blocks.json')
const tables = require('./tables.json')
const farthestRight = require('./farthestRight.json')
const farthestLeft  = require('./farthestLeft.json')

test("Parse table", async () => {

    let version = '202139'
    let templateType = 'scorecard'
    
    let result = await parseTables(blocks, tables, farthestRight, farthestLeft, templateType, version, 'XL')

    //console.log('---result', JSON.stringify(result))

    const daWithoutTransporterId = result[0].table_data.filter(da => da.transporter_id == null)

    expect(daWithoutTransporterId.length).toBe(0)

    const scottBias = result[0].table_data.filter(da => da.name == 'Scott Bias')

    expect(scottBias[0].transporter_id).toBe('A1DBT291G71VUN')
    expect(scottBias[0].harsh_braking).toBe('0.3')
    expect(scottBias[0].pod_opps).toBe('671')

    let silentErrors = validateSilentError(result[0].table_data)
    expect(silentErrors.length).toBe(0)

    let transporterIdErrors = validateTransporterId(result[0].table_data)
    expect(transporterIdErrors.length).toBe(0)

    expect(result[0].table_data.length).toBe(82)
    
})