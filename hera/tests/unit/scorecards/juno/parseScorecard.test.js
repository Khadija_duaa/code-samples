const { validateTransporterId, validateSilentError } = require('../validators.js')
const { parseTables } =  require('../../../../amplify/backend/function/ParsePerformanceData/src/parseTables.js')
const blocks = require('./blocks.json')
const tables = require('./tables.json')
const farthestRight = require('./farthestRight.json')
const farthestLeft  = require('./farthestLeft.json')

test("Parse table", async () => {

    let version = '202005'
    let templateType = 'scorecard'
    
    let result = await parseTables(blocks, tables, farthestRight, farthestLeft, templateType, version)

    //console.log('---result', JSON.stringify(result))

    const daWithoutTransporterId = result[0].table_data.filter(da => da.transporter_id == null)

    expect(daWithoutTransporterId.length).toBe(0)

    // let silentErrors = validateSilentError(result[0].table_data)
    // expect(silentErrors.length).toBe(0)

    let transporterIdErrors = validateTransporterId(result[0].table_data)
    expect(transporterIdErrors.length).toBe(0)

    expect(result[0].table_data.length).toBe(82)
    
})