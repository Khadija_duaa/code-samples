const { scoreCardValidator } =  require('../../../amplify/backend/function/ParsePerformanceData/src/scorecardValidator.js');
const { checkTransporterId } = require('../../../amplify/backend/function/ParsePerformanceData/src/validate.js')

function validateTransporterId(das) {

    let transporterIdErrors = []
    das.forEach(element => {
        let result = checkTransporterId(element.transporter_id)
        if(!result) {
            transporterIdErrors.push(element)
        }
    });

    if(transporterIdErrors.length > 0) {
        console.log('---transporterId error', JSON.stringify(transporterIdErrors))
    }

    return transporterIdErrors
}

function validateSilentError(das) {
    let silentErrors = []
    das.forEach(element => {
        let result = scoreCardValidator(element)
        if(result.length > 0) {
            silentErrors.push(element)
        }
    });


    if(silentErrors.length > 0) {
        console.log('---silent error', JSON.stringify(silentErrors))
    }

    return silentErrors
}

module.exports = { validateTransporterId, validateSilentError };