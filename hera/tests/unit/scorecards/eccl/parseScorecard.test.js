const { validateTransporterId, validateSilentError } = require('../validators.js')
const { parseTables } =  require('../../../../amplify/backend/function/ParsePerformanceData/src/parseTables.js')
const { scoreCardValidator } =  require('../../../../amplify/backend/function/ParsePerformanceData/src/scorecardValidator.js');
const blocks = require('./blocks.json')
const tables = require('./tables.json')
const farthestRight = require('./farthestRight.json')
const farthestLeft  = require('./farthestLeft.json')

test("Parse table bslo", async () => {

    let version = '202220'
    let templateType = 'scorecard'
    
    let result = await parseTables(blocks, tables, farthestRight, farthestLeft, templateType, version)

    const daWithoutTransporterId = result[0].table_data.filter(da => da.transporter_id == null)
    expect(daWithoutTransporterId.length).toBe(0)

    const rudyLemoine = result[0].table_data.filter(da => da.name == 'Rudy Lemoine')

    expect(rudyLemoine[0].transporter_id).toBe('A2NN1RAA5ONW5L')
    expect(rudyLemoine[0].overal_tier).toBe('Fantastic')
    expect(rudyLemoine[0].delivered).toBe('225')
    expect(rudyLemoine[0].swc_cc).toBe(undefined)

    let silentErrors = validateSilentError(result[0].table_data)
    expect(silentErrors.length).toBe(0)

    let transporterIdErrors = validateTransporterId(result[0].table_data)
    expect(transporterIdErrors.length).toBe(0)

    expect(result[0].table_data.length).toBe(54)
    
})