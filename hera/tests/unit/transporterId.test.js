const { checkTransporterId } = require("../../amplify/backend/function/ParsePerformanceData/src/validate.js");

test("Validate transported Id", () => {
  let transporterId = "A3EWS8F7EQV9PO";
  let result = checkTransporterId(transporterId);
  expect(result).toBe(true);
});

test("Validate does not have lowercase letters", () => {
  let transporterId = "AewaFEQVP";
  let result = checkTransporterId(transporterId);
  expect(result).toBe(false);
});

test("Validate begins with the letter A", () => {
  let transporterId = "R3EWS8F7EQV9PO";
  let result = checkTransporterId(transporterId);
  expect(result).toBe(false);
});

test("Validate that transporter ID does not have spaces.", () => {
  let transporterId = "A3EW S8F7EQV9PO";
  let result = checkTransporterId(transporterId);
  expect(result).toBe(true);
});
