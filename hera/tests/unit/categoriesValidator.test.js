const { checkDPS } = require("../../amplify/backend/function/ParsePerformanceData/src/scorecardDpsLevel.js");

test("Validate DPS level:", () => {
    let overall = "Fantastic Plus";
    let safetyAndCompliance = "Fantastic";
    let team = "Fantastic";
    let quality = "Fantastic";
    
    let result = checkDPS(overall, safetyAndCompliance, team, quality);

    expect(result).toBe(true);
});
