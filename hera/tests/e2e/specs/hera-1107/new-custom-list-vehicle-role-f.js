describe('Vehicle Role For Route', function () {

    it('Go to Custom List Tab', function () {
        cy.get('[data-cy="user-info-dropdown"]').click()
        cy.wait(4000);
        cy.get('[data-cy="company-settings-option"]').click()
        cy.wait(4000);
        cy.get('[data-cy="custom-lists-tab"]').click()
        cy.wait(1000);
    })

    it('Go to Daily Rostering Tab', function () {
        cy.get('[data-cy="user-info-dropdown"]').click()
        cy.wait(4000);
        cy.get('[data-cy="company-settings-option"]').click()
        cy.wait(4000);
        cy.get('[data-cy="custom-lists-tab"]').click()
        cy.wait(4000);
        cy.get('[data-cy="daily-roster-tab"]').click()
        cy.wait(4000);
        cy.scrollTo(0, 2000);
        cy.wait(2000);
        cy.get('[data-cy="vehicle-role-for-route"]').click()
    })
})