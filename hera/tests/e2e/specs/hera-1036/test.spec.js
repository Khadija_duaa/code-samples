import MenuHelper from "../../support/Menu.helper"

describe('Add Vehicle Type to Vehicles: (4) On the DA Bulk Update page', function () {

    it('Test Bulk Updates for authorized to drive list', function () {
        // cy.get('[href="/da-management/dashboard"]').trigger('click')
        MenuHelper.Tab.goTo("Associate Management", '/da-management/dashboard')
        cy.wait(2000)
        cy.get('[data-cy="bulk-updates-tab"] > .el-radio-button__inner').click()
        cy.wait(2000)
       // cy.get(':nth-child(1) > .el-table_12_column_21 > .cell > [data-cy=tbu-authorized-to-drive-sw] > .el-input > .el-input__inner').click()
        cy.get('.el-select-dropdown__wrap:visible').find('li').contains('10,000lbs Van').click({force:true})
        cy.wait(3000)
        cy.get('.el-select-dropdown__wrap:visible').find('li').contains('Standard Parcel').click({force:true})
        cy.wait(3000)
        cy.get('.el-select-dropdown__wrap:visible').find('li').contains('Standard Parcel Large').click({force:true})
        cy.get('table > tbody > tr').first().find('.cell').first().click()
    })

})