import MenuHelper from "../../support/Menu.helper"
import ComponentHelper from "../../support/Component.helper2"

describe('Add Vehicle Type to Vehicles: (6) On the Vehicle Bulk Update page', function () {


    it('Test Bulk updates', function () {
        cy.wait(10000)
        cy.get('[href="/vehicles/dashboard"]').click({force:true})
        cy.wait(10000)
        cy.get('[data-cy="bulk-updates"]').click()
        cy.wait(5000)
        
        cy.get('[data-cy="bulk-updates-table"]')
            .find('[data-cy="tbu-vehicle-type-sw"]')
            .eq(0)
            .click()
        cy.get('.el-select-dropdown__wrap:visible').find('li').contains('Sedan').click({force:true})
        cy.wait(6000)

        cy.get('[data-cy="bulk-updates-table"]')
            .find('[data-cy="tbu-vehicle-type-sw"]')
            .eq(1)
            .click()
        cy.get('.el-select-dropdown__wrap:visible').find('li').contains('Sedan').click({force:true})
        cy.wait(6000)

        cy.get('[data-cy="bulk-updates-table"]')
            .find('[data-cy="tbu-vehicle-type-sw"]')
            .eq(2)
            .click()
        cy.get('.el-select-dropdown__wrap:visible').find('li').contains('Sedan').click({force:true})
        cy.wait(6000)

        cy.get('[data-cy="bulk-updates-table"]')
            .find('[data-cy="tbu-vehicle-type-sw"]')
            .eq(0)
            .click()
        cy.get('.el-select-dropdown__wrap:visible').find('li').contains('Custom Parcel').click({force:true})
        cy.wait(6000)

        cy.get('[data-cy="bulk-updates-table"]')
            .find('[data-cy="tbu-vehicle-type-sw"]')
            .eq(1)
            .click()
        cy.get('.el-select-dropdown__wrap:visible').find('li').contains('Custom Delivery Van').click({force:true})
        cy.wait(6000)

        cy.get('[data-cy="bulk-updates-table"]')
            .find('[data-cy="tbu-vehicle-type-sw"]')
            .eq(2)
            .click()
        cy.get('.el-select-dropdown__wrap:visible').find('li').contains('Standard Parcel').click({force:true})
        cy.wait(6000)

    })

})