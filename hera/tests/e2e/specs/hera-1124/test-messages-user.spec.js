let text = "Lorem ipsum dolor sit ametre \n Lorem";
let emptytext = " \r\n \r\n";

describe('Verify messages sending from messenger', function () {

    it('Test Messages with break lines in messenge', function () {
        cy.get('[data-cy="messenger-indicator-btn"]').click()
        cy.wait(4000);
        cy.get('.conversations > :nth-child(1) > .w-full').click({force:true})
        cy.wait(5000);
        cy.get('#twemoji-textarea').focus().type(text)
        cy.wait(5000);
        cy.get('#twemoji-textarea').focus().type(emptytext)
        cy.wait(5000);
        cy.get('[class="text-black-600"]').should('have.text', '0/918')
        cy.get('body').then($body=> {
            if ($body.find('.el-notification').length) {
                cy.get('body').find('.el-notification__closeBtn').click({multiple:true, force:true});
            }
        })
    })
})