import MenuHelper from "../../support/Menu.helper"
import ComponentHelper from "../../support/Component.helper2"
import FunctionHelper from "../../support/Function.helper";

const today = new FunctionHelper().getCurrentDate({
    format: 'Ymd',
    separator: '-'
});

describe('Route Status', function () {


    it('Go to Custom List Tab', function () {
        cy.get('[data-cy="user-info-dropdown"]').click()
        cy.wait(2000);
        cy.get('[data-cy="company-settings-option"]').click()
        cy.wait(2000);
        cy.get('[data-cy="custom-lists-tab"]').click()
        cy.wait(2000);
    })

    it('Go to Daily Rostering Tab', function () {
        cy.get('[data-cy="user-info-dropdown"]').click()
        cy.wait(2000);
        cy.get('[data-cy="company-settings-option"]').click()
        cy.wait(2000);
        cy.get('[data-cy="daily-roster-tab"]').click()
        cy.wait(2000);
        cy.scrollTo(0, 300);
        cy.wait(2000);
        cy.get('[data-cy="route-status"]').click()
        cy.wait(2000);
        cy.get('[data-cy="drawer-save-btn"]').click()
    })
})