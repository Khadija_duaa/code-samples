import ComponentHelper from "../../support/Component.helper2"

describe('Create a message template', function () {

    it('Test Add Associate Form', function () {

        let __helper = new ComponentHelper('message-template-table');

        cy.get('[data-cy="user-info-dropdown"]').click({force:true})
        cy.wait(5000);
        cy.get('[data-cy="company-settings-option"]').click({force:true})
        cy.wait(5000);
        cy.get('[data-cy="messenger-tab"]').click()
        cy.wait(5000);
        cy.get('.el-dropdown-menu__item').contains('Create Template').click({force:true})
        cy.get('[data-cy="drawer-cancel-btn"]').click()
    })
})