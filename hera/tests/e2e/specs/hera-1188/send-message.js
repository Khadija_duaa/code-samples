import MenuHelper from "../../support/Menu.helper"
import ComponentHelper from "../../support/Component.helper2"
import FunctionHelper from "../../support/Function.helper";

const today = new FunctionHelper().getCurrentDate({
    format: 'Ymd',
    separator: '-'
});

describe('Message Templates', function () {


    it.skip('Send Message using a message template', function () {
        let __helper = new ComponentHelper('message-template-content');

        cy.get('[data-cy="messenger-indicator-btn"]').click()
        cy.wait(8000);
        cy.get('[data-cy="message-template-dropdown"]').click()
        cy.wait(8000);
        cy.get('[data-cy="47be7c60-f74d-40e5-bacd-3e361dec2dd8"]').click({force:true})
        cy.wait(4000);
        cy.get('[data-cy="send-message"]').click()
        cy.wait(4000)
        cy.get('body').then($body=> {
            if ($body.find('.el-notification').length) {
                cy.get('body').find('.el-notification__closeBtn').click({multiple:true, force:true});
            }
        })
    })
})