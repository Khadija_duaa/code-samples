let text = "Option Test 1\n \n Option Test 2\n \n \n Option Test 3"

describe('HERA-1328', () => {
    context('Line breaks', () => {

        
        it('Line breaks', function () {
            cy.get('[data-cy="daily-roster-nav"]').click({multiple: true, force:true})
            cy.wait(4000);
            cy.get('[data-cy="edit"]').first().click()
            cy.wait(4000);
            cy.get('#twemoji-textarea').focus().type(text)
            cy.wait(4000);
            cy.get('[data-cy="update"]').first().click()
            cy.wait(4000);
            cy.get('[data-cy="edit"]').first().click()
            cy.wait(4000);
            cy.get('#twemoji-textarea').clear()
            cy.wait(4000);
            cy.get('[data-cy="update"]').first().click()
        })
    })
})