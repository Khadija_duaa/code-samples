import ComponentHelper from "../../../../support/Component.helper2"
import MenuHelper from "../../../../support/Menu.helper"
const {
    vehicle_management: {
        dashboard: {
            upcoming_maintenance_reminders:{
                Reminder
            }
        }
    }
} = Cypress.env('e2e');

describe("Vehicle Management > Dashboard  ",function(){
    context("View & Edit (Edit Maintenance Reminder)",function(){
        beforeEach(function () {
            MenuHelper.Tab.goTo("Vehicle Management", "/vehicles/dashboard", "dashboard-option")

            //To make sure if the selected option is Dashboard
            cy.get('[data-cy="vehicle-management-options"]')
                .children('.is-active').should('be.visible')
                .contains('.el-radio-button__inner','Dashboard')
            
        })

        it('The card allows you to cancel the reminder edit',function(){
            let clickFirst = (cyEl) => cyEl.then(x => x.length ? cy.wrap(x.length > 1 ? x.first() : x ).scrollIntoView().click() : null )

            let tabNames = {
                MileageReminders: "Mileage Reminders",
                DateReminders: "Date Reminders"
            }
            
            let testBtn = "View & Edit"

            let _helper = new ComponentHelper("card_upcoming_maintance")

            _helper.container().contains(tabNames.MileageReminders).scrollIntoView().click()
            clickFirst(_helper.container().find('[role="tabpanel"]:visible').find('.el-dropdown:visible'))

            cy.get(".el-dropdown-menu:visible").contains(testBtn).scrollIntoView().click()

            _helper = new ComponentHelper("modal_add_reminder")

            _helper.fillModalForm([
                {selector: "reminder-form-vehicle-sel", type: "select", value: [Reminder.Vehicle]},
                {selector: "reminder-form-services-sel", type: "select", value: Reminder.Services},
                {selector: "reminder-form-duebydate-dpk", type: "input", value: Reminder.DueByDate},
                {selector: "reminder-form-duebymileage-in", type: "input", value: Reminder.DueByMileage},
            ])

            _helper.modalButtonContain("Cancel").scrollIntoView().click()
        })

        it('The card allows you to update the reminder',function(){
            let clickFirst = (cyEl) => cyEl.then(x => x.length ? cy.wrap(x.length > 1 ? x.first() : x ).scrollIntoView().click() : null )

            let tabNames = {
                MileageReminders: "Mileage Reminders",
                DateReminders: "Date Reminders"
            }
            
            let testBtn = "View & Edit"

            let _helper = new ComponentHelper("card_upcoming_maintance")

            _helper.container().contains(tabNames.MileageReminders).scrollIntoView().click()
            clickFirst(_helper.container().find('[role="tabpanel"]:visible').find('.el-dropdown:visible'))

            cy.get(".el-dropdown-menu:visible").contains(testBtn).scrollIntoView().click()

            _helper = new ComponentHelper("modal_add_reminder")

            _helper.fillModalForm([
                {selector: "reminder-form-vehicle-sel", type: "select", value: [Reminder.Vehicle]},
                {selector: "reminder-form-services-sel", type: "select", value: Reminder.Services},
                {selector: "reminder-form-duebydate-dpk", type: "input", value: Reminder.DueByDate},
                {selector: "reminder-form-duebymileage-in", type: "input", value: Reminder.DueByMileage},
            ])

            _helper.modalButtonContain("Update").scrollIntoView().click()   
        })
    })
})