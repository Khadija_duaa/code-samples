
import ComponentHelper from "../../../../support/Component.helper2"
import MenuHelper from "../../../../support/Menu.helper"
const {
    vehicle_management: {
        dashboard: {
            upcoming_maintenance_reminders:{
                DropDown,
                Reminder
            }
        }
    }
} = Cypress.env('e2e');

describe("Vehicle Management > Dashboard  ",function(){
    context("Add reminder (Add Maintenance Reminder)",function(){
        
        beforeEach(function () {
            MenuHelper.Tab.goTo("Vehicle Management", "/vehicles/dashboard", "dashboard-option")

            //To make sure if the selected option is Dashboard
            cy.get('[data-cy="vehicle-management-options"]')
                .children('.is-active').should('be.visible')
                .contains('.el-radio-button__inner','Dashboard')
            
        })

        it('The card allows you add a new Dropdown item',function(){
            let _helper = new ComponentHelper("card_upcoming_maintance")
            _helper.actionButton("card_action_add").scrollIntoView().click()
            _helper.modalButton("reminder-form-new-service-btn").scrollIntoView().click()
            _helper.fillModalValue({selector: "reminder-form-service-in", type: "input", value: DropDown})
            _helper.modalButton("reminder-form-service-save-btn").scrollIntoView().click()
            cy.wait(4000)

            _helper.modalButtonContain("Cancel").click()
        })

        it('The card allows you to cancel the addition of the reminder',function(){

            let _helper = new ComponentHelper("card_upcoming_maintance")

            _helper.actionButton("card_action_add").scrollIntoView().click()

            _helper.fillModalForm([
                {selector: "reminder-form-vehicle-sel", type: "select", value: [Reminder.Vehicle]},
                {selector: "reminder-form-services-sel", type: "select", value: Reminder.Services},
                {selector: "reminder-form-duebydate-dpk", type: "input", value: Reminder.DueByDate},
                {selector: "reminder-form-duebymileage-in", type: "input", value: Reminder.DueByMileage},
            ])

            _helper.modalButtonContain("Cancel").scrollIntoView().click()

        })

        it('The card allows you to create a reminder',function(){

            let _helper = new ComponentHelper("card_upcoming_maintance")

            _helper.actionButton("card_action_add").scrollIntoView().click()

            _helper.fillModalForm([
                {selector: "reminder-form-vehicle-sel", type: "select", value: [Reminder.Vehicle]},
                {selector: "reminder-form-services-sel", type: "select", value: Reminder.Services},
                {selector: "reminder-form-duebydate-dpk", type: "input", value: Reminder.DueByDate},
                {selector: "reminder-form-duebymileage-in", type: "input", value: Reminder.DueByMileage},
            ])

            _helper.modalButtonContain("Create").scrollIntoView().click()

        })

    })
})