import ComponentHelper from "../../../../../support/Component.helper"
import MenuHelper from "../../../../../support/Menu.helper"

describe("Vehicle Management > Dashboard > Upcoming Maintenance Reminders",function(){
    context("View Vehicle > Add history record",function(){
        beforeEach(function () {
            MenuHelper.Tab.goTo("Vehicle Management", "/vehicles/dashboard", "dashboard-option")

            //To make sure if the selected option is Dashboard
            cy.get('[data-cy="vehicle-management-options"]')
                .children('.is-active').should('be.visible')
                .contains('.el-radio-button__inner','Dashboard')
            
        })

        it('Vehicle damage',function(){
            let time_piker_date = `${ new Date().getDate() }`

            let menuOp = {
                ViewEdit: "View & Edit Reminder",
                ViewVehicle: "View Vehicle",
                Delete: "Delete"
            }
            let historyOp = {
                VehicleDamage: "Vehicle Damage"
            }
            cy.get('[data-cy="card_upcoming_maintance"]').find('[data-cy="card_link_redirect"]').click()
            // Make sure the url is in /vehicles/reminders
            cy.location("pathname").should("eq","/vehicles/reminders")
            
            let __helper = new ComponentHelper('maintenance_reminders')
            __helper.actionButton('three_dot_button_row').first().realClick()
            cy.get('.el-dropdown-menu').contains(menuOp.ViewVehicle).click({force:true})
            
            cy.wait(1000)
            cy.waitForLoaderToDisappear()
            cy.scrollTo(0, '50%').wait(500)

            cy.get('[data-cy="history_record_options"]').find('thead').find('.el-dropdown').first().click({force: true})
            cy.get('[data-cy="history_record_options"]').find('thead').find('.el-dropdown-menu li').contains(historyOp.VehicleDamage).click({force: true}).invoke('hide')
            
            __helper = new ComponentHelper('vehicle_damage_form')

            __helper.actionButton('staff_sel').click()
            cy.get('.el-select-dropdown:visible li').first().click()
            
            cy.wrap([
                {type: 'input', selector: 'damage_in', value: 'Test...'},
                {type: 'input', selector: 'notes_in', value: 'Test notes...'},
                {type: 'file', selector: 'images_up', value: 'image_from_fixture.png'},

            ]).each(i=>__helper.fillValue(i))

            __helper.actionButton('severity_sel').click()
            cy.get('.el-select-dropdown:visible li').first().click()
            __helper.actionButton('damage_date_dpk').click()
            cy.get('.el-date-picker:visible tr').should('be.visible').contains(`${time_piker_date}`).click()
            __helper.actionButton('da_issue_chk').click()
            __helper.actionButton('counseling_chk').click()

            cy.get('body').find('.custom-drawer__footer').contains('Create').click({force: true})
        })
    })
})