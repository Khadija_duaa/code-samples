import ComponentHelper from "../../../../../support/Component.helper"
import MenuHelper from "../../../../../support/Menu.helper"

describe("Vehicle Management > Dashboard > Upcoming Maintenance Reminders",function(){
    context("View Vehicle > Add history record",function(){
        beforeEach(function () {
            MenuHelper.Tab.goTo("Vehicle Management", "/vehicles/dashboard", "dashboard-option")

            //To make sure if the selected option is Dashboard
            cy.get('[data-cy="vehicle-management-options"]')
                .children('.is-active').should('be.visible')
                .contains('.el-radio-button__inner','Dashboard')
        })

        it('Maintenance Record',function(){
            let time_piker_date = `${ new Date().getDate() }`

            let menuOp = {
                ViewEdit: "View & Edit Reminder",
                ViewVehicle: "View Vehicle",
                Delete: "Delete"
            }
            let historyOp = {
                MaintenanceRecord: "New Maintenance Record"
            }
            cy.get('[data-cy="card_upcoming_maintance"]').find('[data-cy="card_link_redirect"]').click()
            // Make sure the url is in /vehicles/reminders
            cy.location("pathname").should("eq","/vehicles/reminders")
            
            let __helper = new ComponentHelper('maintenance_reminders')
            __helper.actionButton('three_dot_button_row').first().realClick()
            cy.get('.el-dropdown-menu').contains(menuOp.ViewVehicle).click({force:true})
            
            cy.wait(1000)
            cy.waitForLoaderToDisappear()
            cy.scrollTo(0, '50%').wait(500)

            cy.get('[data-cy="history_record_options"]').find('thead').find('.el-dropdown').first().click({force: true})
            cy.get('[data-cy="history_record_options"]').find('thead').find('.el-dropdown-menu li').contains(historyOp.MaintenanceRecord).click({force: true}).invoke('hide')
            
            __helper = new ComponentHelper('vehicle-maintenance-form')
            
            __helper.actionButton('service-option-new-btn').click()
            __helper.actionButton('new-service-option-in').click().type('Hera test service 3')
            __helper.actionButton('service-option-save-btn').click()


            __helper.actionButton('location-option-new-btn').click()
            __helper.actionButton('new-location-option-in').click().type('Hera test location 3')
            __helper.actionButton('location-option-save-btn').click()


            __helper.actionButton('location-sel').click().get('.el-select-dropdown:visible li').then(x => x.length ? (x.length > 1 ? x.first() : x ).trigger('click')  : null )

            __helper.actionButton('status-sel').click().get('.el-select-dropdown:visible li').then(x => x.length ? (x.length > 1 ? x.first() : x ).trigger('click')  : null )
            
            __helper.actionButton('accident-date-dpk').click().get('.el-date-picker:visible tr').contains(`${time_piker_date}`).click()

            cy.wrap([
                {type: 'input', selector: 'mileage-in', value: '20'},
                {type: 'file', selector: 'images-up', value: 'image_from_fixture.png'}
            ]).each(e=>__helper.fillValue(e))
            
            __helper.actionButton('add-maintenace-reminder-btn').click()

            //Random 5 to 20 (Mileage)
            let rnd = Math.floor(Math.random() * 16) + 5

            __helper.actionButton('reminder-form-duebymileage-in').click().type(rnd)

            __helper.actionButton('reminder-form-duebydate-dpk').scrollIntoView().click().get('.el-date-picker:visible tr').contains(`${time_piker_date}`).click()

            const MAX_SERVICES = 3
            __helper.actionButton('services-sel').click().get('.el-select-dropdown:visible li').then(x=>cy.wrap(Cypress.$.grep(x,(n,i)=> i < MAX_SERVICES )).each(e=>cy.get(e).click()))
            cy.get('.el-select-dropdown:visible').then(x=>x.hide())

            const MAX_FUTURE_SERVICES = 3
            __helper.actionButton('reminder-form-services-sel').click().get('.el-select-dropdown:visible li').then(x=>cy.wrap(Cypress.$.grep(x,(n,i)=> i < MAX_FUTURE_SERVICES )).each(e=>cy.get(e).click()))
            cy.get('.el-select-dropdown:visible').then(x=>x.hide())
            cy.get('body').find('.custom-drawer__footer').contains('Create').click({force: true})
        })


    })
})