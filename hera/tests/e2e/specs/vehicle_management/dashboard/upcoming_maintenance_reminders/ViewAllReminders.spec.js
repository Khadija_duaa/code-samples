import ComponentHelper from "../../../../support/Component.helper"
import MenuHelper from "../../../../support/Menu.helper"

describe("Vehicle Management > Dashboard  ",function(){
    context("View All Reminders",function(){
        beforeEach(function () {
            MenuHelper.Tab.goTo("Vehicle Management","/vehicles/dashboard", "dashboard-option")

            //To make sure if the selected option is Dashboard
            cy.get('[data-cy="vehicle-management-options"]')
                .children('.is-active').should('be.visible')
                .contains('.el-radio-button__inner','Dashboard')
            
        })

        it('The card allows you to view all reminders',function(){
            let _helper = new ComponentHelper("card_upcoming_maintance")

            _helper.actionButton("card_link_redirect").scrollIntoView().click()
            
            cy.location("pathname").should("eq","/vehicles/reminders")
        })
    })
})