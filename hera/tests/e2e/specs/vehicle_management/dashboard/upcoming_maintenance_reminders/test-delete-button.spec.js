import MenuHelper from "../../../../support/Menu.helper"

describe('Vehicle Management / Dashboard',()=>{
    context('Upcoming Maintenance Reminders',()=>{
        beforeEach(function (){
            MenuHelper.Tab.goTo("Vehicle Management","/vehicles/dashboard", "dashboard-option")

            //To make sure if the selected option is Dashboard
            cy.get('[data-cy="vehicle-management-options"]')
                .children('.is-active').scrollIntoView().should('be.visible')
                .contains('.el-radio-button__inner','Dashboard')
             
        })

        it('Delete Reminder',function(){
            let clickFirst = (cyEl) => cyEl.then(x => x.length ? cy.wrap(x.length > 1 ? x.first() : x ).scrollIntoView().click() : null )

            let menuOp = {
                ViewEdit: "View & Edit Reminder",
                ViewVehicle: "View Vehicle",
                Delete: "Delete"
            }

            clickFirst(cy.get('[data-cy="card_upcoming_maintance"] [role="tabpanel"]').first().find('.el-dropdown:visible'))
            cy.get('.el-dropdown-menu:visible').contains(menuOp.Delete).scrollIntoView().click()

            cy.get('[role="dialog"]').contains('Delete').click()
            cy.wait(1000)
        })
    })
})