import ComponentHelper from "../../../../support/Component.helper"
import MenuHelper from "../../../../support/Menu.helper"

describe("vehicle management", function () {

    context("dashboard", function () {

        it("card upcoming maintance reminders links navigation", function () {
            MenuHelper.Tab.goTo("Vehicle Management", "/vehicles/dashboard", "dashboard-option")

            let _helper = new ComponentHelper("card_upcoming_maintance")

            _helper.container().find('[data-cy="card_title"]').contains("Upcoming Maintenance Reminders").should("be.visible")

            let tabsNames = [
                "Mileage Reminders",
                "Date Reminders"
            ]

            cy.get('[data-cy="card_tabs"]')
                .find('.el-tabs__item')
                .each(function ($el, index, $list) {
                    cy.get($el).contains(tabsNames[index]).should("be.visible")
                })

            //validate the link to take us to path route name /reminders
            _helper.actionButton("card_link_redirect").scrollIntoView().click()
            cy.location("pathname").should("eq", "/vehicles/reminders")

            MenuHelper.Tab.goTo("Vehicle Management", "/vehicles/dashboard")

            _helper.actionButton("card_action_add").click()
            cy.get('[data-cy="modal_add_reminder"]').should("be.visible")

            _helper.modalButtonContain("Cancel").click()
        })
    })
})