import MenuHelper from '../../../../support/Menu.helper'
import ComponentHelper from "../../../../support/Component.helper"
describe("Vehicle Management > Dashboard  ", function () {
    function ViewVehicle(tabName, dropdownValue) {
        let tabNames = {
            MileageReminders: "Mileage Reminders",
            DateReminders: "Date Reminders"
        }
        let __helper = new ComponentHelper("card_upcoming_maintance");
        let clickFirst = (cyEl) => cyEl.then(x => x.length ? cy.wrap(x.length > 1 ? x.first() : x).scrollIntoView().click() : null)

        __helper.container().contains(tabNames[tabName]).scrollIntoView().click()
        clickFirst(__helper.container().find('[role="tabpanel"]:visible').find('.el-dropdown:visible'))

        cy.get(".el-dropdown-menu:visible").contains(dropdownValue).scrollIntoView().click()

        __helper.waitXHR()
    }

    context("View Vehicle", function () {
        beforeEach(function () {
            MenuHelper.Tab.goTo("Vehicle Management", "/vehicles/dashboard", "dashboard-option")

            //To make sure if the selected option is Dashboard
            cy.get('[data-cy="vehicle-management-options"]')
                .children('.is-active').should('be.visible')
                .contains('.el-radio-button__inner', 'Dashboard')

        })

        it('(Tab: Mileage Reminders) The card allows you to display the vehicle details of the reminder', function () {
            ViewVehicle("MileageReminders", "View Vehicle")
        })

        it('(Tab: Date Reminders) The card allows you to display the vehicle details of the reminder', function () {
            ViewVehicle("DateReminders", "View Vehicle")
        })
    })
})