
import ComponentHelper from '../../../../support/Component.helper.js'
import MenuHelper from '../../../../support/Menu.helper.js'
import FunctionHelper from '../../../../support/Function.helper'
const {
    vehicle_management: {
        dashboard: {
            accidents_damage:{
                Accident
            }
        }
    }
} = Cypress.env('e2e');

describe("Vehicle Management > Dashboard > Accident & Damage",function(){
    context("Add accident (New Accident)",function(){
        beforeEach(function () {
            MenuHelper.Tab.goTo("Vehicle Management","/vehicles/dashboard", "dashboard-option")

            //To make sure if the selected option is Dashboard
            cy.get('[data-cy="vehicle-management-options"]')
                .children('.is-active').should('be.visible')
                .contains('.el-radio-button__inner','Dashboard')

            new ComponentHelper('card_accidents_damage').actionButton('addButtonText').click()
        })

        it("The card allows you to cancel the creation of a new accident", function() {
            let __helper = new ComponentHelper('vm_accident_form')
            __helper.actionButton('cancel_btn').scrollIntoView().click({force: true})
            cy.get(".el-message-box:visible").find("button:contains('Cancel')").wait(1000).click()
            __helper.actionButton('cancel_btn').scrollIntoView().click({force: true})
            
            cy.get(".el-message-box:visible").find("button:contains('Continue')").wait(1000).scrollIntoView().click()
        })

        it("The card allows you to create a new accident", function () {
            let today = new FunctionHelper().getCurrentDate({
                format: 'mdY',
                separator: '/'
            });

            cy.scrollTo('top')

            let __helper = new ComponentHelper('vm_accident_form')
            cy.waitForLoaderToDisappear()
            __helper.fillForm([
                {selector:"staff_op", type: "select", value: Accident.DeliveryAssociate },
                {selector:"accidentDate_dpk", type: "input", value: today },
                {selector:"vehicle_op", type: "select", value: Accident.Vehicle }])
            cy.scrollTo('top').wait(500)
            __helper.container().contains('.el-radio-button__inner', Accident.AtFault).click()
            cy.wrap([
                {selector: "street_in", type: "input", value: Accident.LocationStreet },
                {selector:"city_in", type: "input", value: Accident.LocationCity },
                {selector:"state_in", type: "input", value: Accident.LocationState },
                {selector:"zip_in", type: "input", value: Accident.LocationZip },
                {selector:"drugTestDate_dpk", type: "input", value: Accident.DrugTestDate },
                {selector:"drugTestResult_sel", type: "select", value: Accident.TestResults },
                {selector:"insuranceClaimNumber_in", type: "input", value: Accident.ClaimNumber },
                {selector:"policeDepartment_in", type: "input", value: Accident.PoliceDepartment },
                {selector:"policeOfficerName_in", type: "input", value: Accident.PoliceOfficerName },
                {selector:"policeReportNumber_in", type: "input", value: Accident.PoliceReportNumber },
                {selector:"verifiedDate_dpk", type: "input", value: today }
            ]).each((item) => {
                cy.scrollTo('top').wait(100)
                __helper.fillValue(item)
            })
            cy.scrollTo(0, '35%')
            cy.wait(500)
            __helper.fillForm([{ selector: "notes_txt", type: "input", value: Accident.Notes },
                {selector:"af-scanned-report-file", type: "file", value: Accident.ScannedReport },
                {selector:"af-accident-images-file", type: "file", value: Accident.AccidentImage },
            ])
            __helper.scroll({ x: 0, y: '95%' })
            cy.wait(500)

            __helper.actionButton('create_btn').click()

            __helper.modalButton('create_counseling_accicent_btn').click()
            __helper.alert('p').contains("success").should("be.visible")
        })
    })
})