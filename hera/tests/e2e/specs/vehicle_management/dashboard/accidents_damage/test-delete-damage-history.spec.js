import ComponentHelper from "../../../../support/Component.helper2"
import MenuHelper from "../../../../support/Menu.helper"

describe('Vehicle Management / Dashboard',()=>{
    context('Accidents & Damage > Vehicle Damage',()=>{
        beforeEach(function (){
            MenuHelper.Tab.goTo("Vehicle Management","/vehicles/dashboard", "dashboard-option")

            //To make sure if the selected option is Dashboard
            cy.get('[data-cy="vehicle-management-options"]')
                .children('.is-active').scrollIntoView().should('be.visible')
                .contains('.el-radio-button__inner','Dashboard')
             
        })

        it('Test - Delete Vehicle Damage',function(){
            let clickFirst = (cyEl) => cyEl.then(x => x.length ? cy.wrap(x.length > 1 ? x.first() : x ).scrollIntoView().click() : null )

            let tabs = {
                Accidents: {
                    text: "Accidents",
                    index: 0
                },
                VehicleDamage: {
                    text: "Vehicle Damage",
                    index: 1
                }
            }

            let __helper = new ComponentHelper('card_accidents_damage')
            __helper.container().contains(tabs.VehicleDamage.text).scrollIntoView().click()
            clickFirst(cy.get('[data-cy="ad_tabs"]').eq(tabs.VehicleDamage.index).find('[data-cy="cad-3dot-btn"]:visible'))

            let _3dotMenu = {
                ViewEdit: "View & Edit",
                ViewVehicle: "View Vehicle",
                Delete: "Delete"
            }

            cy.get('.el-dropdown-menu:visible').contains(_3dotMenu.ViewVehicle).click()

            cy.wait(500)
            cy.waitForLoaderToDisappear()
            cy.scrollTo(0, '50%').wait(500)
            __helper = new ComponentHelper('history_record_options')

            let rowOp = {
                ViewEdit: "View & Edit",
                Delete: "Delete",
            }
            __helper.table().find("tr").contains("Vehicle Damage Record").then(() => {
                cy.get(`[data-cy="three_dot_button_row"]`).first().click({force: true})
            })
            cy.get('.el-dropdown-menu').contains(rowOp.Delete).click({force: true})

            __helper.modalButtonContain('Delete').click().then(()=>{
                assert(true, '**Vehicle Damage deleted successfully:** the expected **"Vehicle Damage"** was deleted')
            })
        })
    })
})