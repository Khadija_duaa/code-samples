import ComponentHelper from "../../../../support/Component.helper"
import MenuHelper from "../../../../support/Menu.helper"

describe('Vehicle Management / Dashboard',()=>{
    context('Accidents & Damage',()=>{
        beforeEach(function (){
            MenuHelper.Tab.goTo("Vehicle Management","/vehicles/dashboard", "dashboard-option")

            //To make sure if the selected option is Dashboard
            cy.get('[data-cy="vehicle-management-options"]')
                .children('.is-active').scrollIntoView().should('be.visible')
                .contains('.el-radio-button__inner','Dashboard')
             
        })

        it('Delete button',function(){
            let clickFirst = (cyEl) => cyEl.then(x => x.length ? cy.wrap(x.length > 1 ? x.first() : x ).scrollIntoView().click() : null )

            let menuOp = {
                ViewEdit: "View & Edit Reminder",
                ViewVehicle: "View Vehicle",
                Delete: "Delete"
            }

            let _helper = new ComponentHelper('card_accidents_damage')

            clickFirst(_helper.actionButton("cad-3dot-btn"))
            cy.get('.el-dropdown-menu:visible').contains(menuOp.Delete).scrollIntoView().click()

            _helper.modal().find("input").scrollIntoView().click().type("DELETE")

            _helper.modal().find("button:contains('Confirm')").scrollIntoView().click()
        })
    })
})