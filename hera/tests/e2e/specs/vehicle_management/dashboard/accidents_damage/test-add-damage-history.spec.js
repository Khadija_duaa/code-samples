import ComponentHelper from "../../../../support/Component.helper"
import MenuHelper from "../../../../support/Menu.helper"
import FunctionHelper from "../../../../support/Function.helper";
const {
    vehicle_management: {
        dashboard: {
            accidents_damage:{
                DamageHistory
            }
        }
    }
} = Cypress.env('e2e');

describe('Vehicle Management / Dashboard',()=>{
    function FillForm(addVehicleName = false) {
        let today = new FunctionHelper().getCurrentDate({
            addDays: -10,   // 10 days older than to-date
            format: 'mdY',
            separator: '/'
        });

        let __helper = new ComponentHelper('vehicle_damage_form')
        if (addVehicleName) __helper.fillModalValue({type: 'select', selector: 'vehicle_sel', value: DamageHistory.Vehicle })
        __helper.fillModalForm([
            {type: 'select', selector: 'staff_sel', value: DamageHistory.DA },
            {type: 'input', selector: 'damage_in', value: DamageHistory.Damage },
            {type: 'select', selector: 'severity_sel', value: DamageHistory.Severity },
            {type: 'input', selector: 'damage_date_dpk', value: today },
            {type: 'input', selector: 'da_issue_chk', value: DamageHistory.DAIssue },
            {type: 'input', selector: 'counseling_chk', value: DamageHistory.Counseling },
            {type: 'input', selector: 'notes_in', value: DamageHistory.Notes },
            {type: 'file', selector: 'images_up', value: DamageHistory.Images }
        ])

        cy.scrollTo('bottom')

        __helper.modalButtonContain("Create").scrollIntoView().click().then(()=>{
            assert(true, '**Vehicle Damage created successfully:** the expected **"Vehicle Damage"** was created')
        })
        cy.waitForLoaderToDisappear()
    }

    context('Accidents & Damage > Vehicle Damage',()=>{
        beforeEach(function (){
            MenuHelper.Tab.goTo("Vehicle Management","/vehicles/dashboard", "dashboard-option")

            //To make sure if the selected option is Dashboard
            cy.get('[data-cy="vehicle-management-options"]')
                .children('.is-active').scrollIntoView().should('be.visible')
                .contains('.el-radio-button__inner','Dashboard')
             
        })

        it('Test - Vehicle Damage',function(){
            let tabs = {
                Accidents: {
                    text: "Accidents",
                    index: 0
                },
                VehicleDamage: {
                    text: "Vehicle Damage",
                    index: 1
                }
            }

            cy.get('[data-cy="card_accidents_damage"]').contains(tabs.VehicleDamage.text).scrollIntoView().click()
            cy.get('[data-cy="card_accidents_damage"] .link:contains("Add Vehicle Damage")').scrollIntoView().click()

            FillForm(true)
        })
        
        it('Test - Add History Record > Vehicle Damage',function(){
            let clickFirst = (cyEl) => cyEl.then(x => x.length ? cy.wrap(x.length > 1 ? x.first() : x ).scrollIntoView().click() : null )

            let tabs = {
                Accidents: {
                    text: "Accidents",
                    index: 0
                },
                VehicleDamage: {
                    text: "Vehicle Damage",
                    index: 1
                }
            }

            cy.get('[data-cy="card_accidents_damage"]').contains(tabs.VehicleDamage.text).scrollIntoView().click()

            clickFirst(cy.get('[data-cy="ad_tabs"]').eq(tabs.VehicleDamage.index).find('[data-cy="cad-3dot-btn"]:visible'))

            let _3dotMenu = {
                ViewEdit: "View & Edit",
                ViewVehicle: "View Vehicle",
                Delete: "Delete"
            }

            cy.get('.el-dropdown-menu:visible').contains(_3dotMenu.ViewVehicle).click()

            let historyRecordOp = {
                VehicleDamage: "New Vehicle Damage Record",
                MaintenanceRecord: "New Maintenance Record",
                AccidentRecord: "New Accident Record",
                OdometerReading: "New Odometer Reading"
            }

            cy.wait(500)
            cy.waitForLoaderToDisappear()
            cy.scrollTo(0, '50%').wait(500)
            cy.get('[data-cy="history_record_options"]').find('thead').as('menu').find('.el-dropdown').first().click({force: true})
            cy.get('@menu').find('.el-dropdown-menu li').contains(historyRecordOp.VehicleDamage).click({force: true})
            cy.get('@menu').find('.el-dropdown-menu li').invoke('hide')

            FillForm()
        })
    })
})