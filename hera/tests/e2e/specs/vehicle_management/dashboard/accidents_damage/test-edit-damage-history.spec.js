import FunctionHelper from "../../../../support/Function.helper"
import ComponentHelper from "../../../../support/Component.helper"
import MenuHelper from "../../../../support/Menu.helper"
const {
    vehicle_management: {
        dashboard: {
            accidents_damage:{
                DamageHistory_ED
            }
        }
    }
} = Cypress.env('e2e');

describe('Vehicle Management / Dashboard',()=>{
    context('Accidents & Damage > Vehicle Damage',()=>{
        
        beforeEach(function (){
            MenuHelper.Tab.goTo("Vehicle Management","/vehicles/dashboard", "dashboard-option")

            //To make sure if the selected option is Dashboard
            cy.get('[data-cy="vehicle-management-options"]')
                .children('.is-active').scrollIntoView().should('be.visible')
                .contains('.el-radio-button__inner','Dashboard')
             
        })

        it('Test - Edit Vehicle Damage',function(){
            let clickFirst = (cyEl) => cyEl.then(x => x.length ? cy.wrap(x.length > 1 ? x.first() : x ).scrollIntoView().click() : null )

            let tabs = {
                Accidents: {
                    text: "Accidents",
                    index: 0
                },
                VehicleDamage: {
                    text: "Vehicle Damage",
                    index: 1
                }
            }

            cy.get('[data-cy="card_accidents_damage"]').contains(tabs.VehicleDamage.text).scrollIntoView().click()
            clickFirst(cy.get('[data-cy="ad_tabs"]').eq(tabs.VehicleDamage.index).find('[data-cy="cad-3dot-btn"]:visible'))

            let _3dotMenu = {
                ViewEdit: "View & Edit",
                ViewVehicle: "View Vehicle",
                Delete: "Delete"
            }

            cy.get('.el-dropdown-menu:visible').contains(_3dotMenu.ViewEdit).click()

            cy.waitForLoaderToDisappear()

            let __helper = new ComponentHelper('vehicle-detail-form')
            
            let date = new FunctionHelper().getCurrentDate({
                format: 'mdY',
                separator: '/'
            })
            
            __helper.fillModalForm([
                {type: 'input', selector: 'damage_date_dpk', value: date},
                {type: 'select', selector: 'staff_sel', value: DamageHistory_ED.DA },
                {type: 'input', selector: 'damage_in', value: DamageHistory_ED.Damage },
                {type: 'select', selector: 'severity_sel', value: DamageHistory_ED.Severity },
                {type: 'input', selector: 'notes_in', value: DamageHistory_ED.Notes },
            ])

            __helper.modalButtonContain("Update").scrollIntoView().click().then(()=>{
                assert(true, '**Vehicle Damage updated successfully:** the expected **"Vehicle Damage"** was edited')
            })

        })
    })
})