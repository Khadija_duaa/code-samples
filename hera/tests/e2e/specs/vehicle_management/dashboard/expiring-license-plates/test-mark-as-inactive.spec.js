import ComponentHelper from "../../../../support/Component.helper"
import MenuHelper from "../../../../support/Menu.helper"

describe('Vehicle Management / Dashboard',()=>{
    context('Expiring License Plates',()=>{
        
        beforeEach(function (){
            MenuHelper.Tab.goTo("Vehicle Management","/vehicles/dashboard", "dashboard-option")

            //To make sure if the selected option is Dashboard
            cy.get('[data-cy="vehicle-management-options"]')
                .children('.is-active').scrollIntoView().should('be.visible')
                .contains('.el-radio-button__inner','Dashboard')
             
        })

        it('Mark as inactive',function(){
            let clickFirst = (cyEl) => cyEl.then(x => x.length ? cy.wrap(x.length > 1 ? x.first() : x ).scrollIntoView().click() : null )

            let menuOp = {
                ViewVehicle: "View Vehicle",
                MarkAsInactive: "Mark As Inactive"
            }

            let __helper = new ComponentHelper('expiring-licence-plates-form')

            __helper.container().find('[role="tab"]').last().click()
            clickFirst(__helper.container().find('[role="tabpanel"]:visible').find('.el-dropdown:visible'))

            cy.get(".el-dropdown-menu:visible").contains(menuOp.MarkAsInactive).scrollIntoView().click()

            cy.get('[role="dialog"]').contains('Update').click()
        })
    })
})