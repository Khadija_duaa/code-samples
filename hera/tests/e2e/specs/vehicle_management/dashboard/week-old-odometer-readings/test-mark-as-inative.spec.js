import ComponentHelper from "../../../../support/Component.helper"
import MenuHelper from "../../../../support/Menu.helper"

describe('Vehicle Management / Dashboard',()=>{
    context('Week-Old Odometer Readings',()=>{
        
        beforeEach(function (){
            MenuHelper.Tab.goTo("Vehicle Management","/vehicles/dashboard", "dashboard-option")

            //To make sure if the selected option is Dashboard
            cy.get('[data-cy="vehicle-management-options"]')
                .children('.is-active').should('be.visible')
                .contains('.el-radio-button__inner','Dashboard')
             
        })

        it('Mark as inactive',function(){
            let clickFirst = (cyEl) => cyEl.then(x => x.length ? cy.wrap(x.length > 1 ? x.first() : x ).scrollIntoView().click() : null )

            let menuOp = {
                AddOdometerReading: "Add Odometer Reading",
                ViewVehicle: "View Vehicle",
                MarkAsInactive: "Mark As Inactive"
            }

            clickFirst(new ComponentHelper('week-old-odometer-readings-form').actionButton('woor-3dot-btn'))
            cy.get('.el-dropdown-menu:visible').contains(menuOp.MarkAsInactive).scrollIntoView().click()

            cy.get('[role="dialog"]').contains('Update').click()
        })
    })
})