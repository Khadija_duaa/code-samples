import ComponentHelper from "../../../support/Component.helper"
import MenuHelper from '../../../support/Menu.helper'
const {
    vehicle_management: {
        bulk_updates: {
            BulkUpdates_VM
        }
    }
} = Cypress.env('e2e');

describe('Vehicle Management / Bulk Updates',()=>{
    context('Table',()=>{
        
        beforeEach(function (){
            MenuHelper.Tab.goTo("Vehicle Management","/vehicles/dashboard", "dashboard-option")

            //To make sure if the selected option is 'Bulk Updates'
            cy.get('[data-cy="vehicle-management-options"]')
            .contains('.el-radio-button__inner','Bulk Updates').click()
        })

        it('Edit Row',function(){
            let __helper = new ComponentHelper('bulk-updates-table');

            __helper.table().contains(BulkUpdates_VM.VehicleNameFilter).parents("tr").as("row")

            cy.get("@row").invoke("index").then(ROW_INDEX=>{
                __helper.fillTable(ROW_INDEX+1,[
                    {selector: "parking-space-sel", type:"select", value: BulkUpdates_VM.ParkingSpace},
                    {selector: "vehicle-device-sel", type:"select", value: BulkUpdates_VM.AssignedDevice},
                ])
            })

            cy.get("@row").find("[data-cy='status-btn']").scrollIntoView().click()

            __helper.fillModalForm([
                {selector: "new_status_sel", type:"select", value: BulkUpdates_VM.Status},
                {selector: "date_dpk", type:"input", value: BulkUpdates_VM.Date},
                {selector: "change_reason_in", type:"input", value:  BulkUpdates_VM.ReasonForChange},
            ])

            cy.wait(1000)

            __helper.modalButton('update_btn').scrollIntoView().click()

        })
    })
})