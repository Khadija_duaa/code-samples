import ComponentHelper from "../../../support/Component.helper"
import MenuHelper from "../../../support/Menu.helper"
const {
    vehicle_management: {
        vehicle_list: {
            Vehicle1,
            Vehicle2,
            Vehicle3
        }
    }
} = Cypress.env('e2e');

describe('Vehicle Management / Vehicle List',function () {

    function AddNewVehicle(vehicle) {
        let menuOp = {
            AddVehicle: "Add Vehicle",
            ExportVehicles: "Export Vehicles",
            ImportVehicles:"Import Vehicles"
        }
        let __helper = new ComponentHelper('vehicle_list_table')

        __helper.actionButton('vehicle_list_options').click()
        cy.get('.el-dropdown-menu:visible').contains(menuOp.AddVehicle).click()
        __helper = new ComponentHelper('vehicle_form')

        __helper.fillForm([
            {type: 'input', selector: 'name_in', value: vehicle.Name},
            {type: 'select', selector: 'parking_space_sel', value: vehicle.ParkingSpace},
            {type: 'select', selector: 'device_sel', value: vehicle.AssignedDevice},
            {type: 'input', selector: 'start_date_dpk', value: vehicle.StartDate},
            {type: 'input', selector: 'license_plate_in', value: vehicle.LicensePlate},
            {type: 'input', selector: 'end_date_dpk', value: vehicle.EndDate},
            {type: 'input', selector: 'license_plate_exp_dpk', value: vehicle.LicensePlateExpiration},
            {type: 'select', selector: 'ownership_sel', value: vehicle.Ownership},
            {type: 'select', selector: 'state_sel', value: vehicle.State},
            {type: 'input', selector: 'type_in', value: vehicle.Type},
            {type: 'input', selector: 'vin_in', value: vehicle.VIN},
            {type: 'input', selector: 'make_in', value: vehicle.Make},
            {type: 'input', selector: 'gas_card_in', value: vehicle.GasCardLast6},
            {type: 'input', selector: 'model_in', value: vehicle.Model},
            {type: 'select', selector: 'company_sel', value: vehicle.Company},
            {type: 'input', selector: 'year_in', value: vehicle.Year },
            {type: 'input', selector: 'rental_agreement_num_in', value: vehicle.RentalAgreementNumber},
            // {type: 'check', selector: 'over_limit_sw', value: vehicle.Over10kLBS},               // uncomment if this field is added again on UI
            // {type: 'check', selector: 'custom_delivery_sw', value: vehicle.CustomDeliveryVan},   // uncomment if this field is added again on UI
            {type: 'input', selector: 'notes_txt', value: vehicle.Notes},
        ])

        cy.get('[data-cy="create_btn"]').click()
        __helper.alert('p').contains("successfully created").should("be.visible")
        cy.waitForLoaderToDisappear()
        cy.scrollTo('top')
    }

    context('List options > Add Vehicle',function () {
        beforeEach(function () {
            MenuHelper.Tab.goTo("Vehicle Management","/vehicles/dashboard", "dashboard-option")

            //To make sure if the selected option is 'Vehicle List'
            cy.get('[data-cy="vehicle-management-options"]')
                .contains('.el-radio-button__inner','Vehicle List').click()
            
        })

        it('Test - Add a new vehicle (first vehicle)',function () {
            AddNewVehicle(Vehicle1)
        })

        it('Test - Add a new vehicle (second vehicle)',function () {
            AddNewVehicle(Vehicle2)
        })

        it('Test - Add a new vehicle (third vehicle)',function () {
            AddNewVehicle(Vehicle3)
        })
        
    })
})