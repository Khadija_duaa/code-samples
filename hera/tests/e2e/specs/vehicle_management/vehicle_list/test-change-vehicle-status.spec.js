import ComponentHelper from "../../../support/Component.helper"
import MenuHelper from "../../../support/Menu.helper"

describe('Vehicle Management / Vehicle List',function () {
    function UpdateStatus(currentStatus, statusToSet) {
        let menuOp = {
            ViewVehicle: "View Vehicle",
            AddOdometerReading: "Add Odometer Reading",
            ChangeVehicleStatus: "Change Vehicle Status"
        }

        let time_piker_date = `${new Date().getDate()}`

        cy.wait(1000)

        cy.get('[data-cy="filter_by_status_dd"]').should('be.visible').trigger('mouseover')
        cy.get('.el-dropdown-menu').contains(currentStatus).find('input').should('have.value', currentStatus)

        new ComponentHelper('vehicle_list_table').actionButton('three_dot_button_row').first().click()
        cy.get('.el-dropdown-menu:visible').contains(menuOp.ChangeVehicleStatus).click()
        let __helper = new ComponentHelper('status_form_modal')
        __helper.actionButton('new_status_sel').click()
        cy.get('.el-select-dropdown:visible li').contains(statusToSet).click()
        __helper.actionButton('date_dpk').should('be.visible').then(dpk => {
            cy.get(dpk).click()
            cy.wait(1000)
            cy.get('.el-date-picker:visible tr').should('be.visible').contains(`${time_piker_date}`).click()
            cy.get(".el-date-picker:visible").invoke("hide");
        })
        __helper.fillValue({ type: 'input', selector: 'change_reason_in', value: 'Testing', ttw: 100 })
        if (statusToSet != "Active") __helper.fillValue({ type: 'check', selector: 'toggle_new_maintenance_record', value: false })
        __helper.actionButton('update_btn').click()
        cy.waitForLoaderToDisappear()
        cy.scrollTo('top')
    }

    context('Vehicle options > Change vehicle status',function () {
        beforeEach(function () {
            MenuHelper.Tab.goTo("Vehicle Management","/vehicles/dashboard", "dashboard-option")

            //To make sure if the selected option is 'Vehicle List'
            cy.get('[data-cy="vehicle-management-options"]')
                .contains('.el-radio-button__inner','Vehicle List').click()
            
        })

        it('Test - Change Vehicle status (Active to Inative)',function () {
            let statusOp = {
                Active: "Active",
                Maintenance: "Inactive - Maintenance"
            }
            UpdateStatus(statusOp.Active, statusOp.Maintenance)
        })

        it('Test - Change Vehicle status (Inative to Active)',function () {
            let statusOp = {
                Active: "Active",
                Maintenance: "Inactive - Maintenance"
            }

            UpdateStatus(statusOp.Maintenance, statusOp.Active)
        })
    })
})