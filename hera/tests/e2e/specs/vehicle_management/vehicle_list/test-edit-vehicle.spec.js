import ComponentHelper from "../../../support/Component.helper"
import MenuHelper from "../../../support/Menu.helper"
const {
    filters:{
        Vehicle_Filter
    },
    vehicle_management: {
        vehicle_list: {
            Vehicle1_ED
        }
    }
} = Cypress.env('e2e');
describe('Vehicle Management / Vehicle List',function () {
    context('View Vehicle > Edit Vehicle',function () {
        beforeEach(function () {
            MenuHelper.Tab.goTo("Vehicle Management","/vehicles/dashboard", "dashboard-option")

            //To make sure if the selected option is 'Vehicle List'
            cy.get('[data-cy="vehicle-management-options"]')
                .contains('.el-radio-button__inner','Vehicle List').click()
            
        })

        it('Test - Edit vehicle',function () {
            cy.wait(1000)
            cy.get("[data-cy='vi-search-in']").scrollIntoView().type(Vehicle_Filter)

            cy.get(`[data-cy='vehicle_list_table'] tr:contains('${Vehicle_Filter}')`).find(" [data-cy='three_dot_button_row']").first().click()

            let menuOp = {
                ViewVehicle: "View Vehicle",
                AddOdometerReading: "Add Odometer Reading",
                ChangeVehicleStatus:"Change Vehicle Status"
            }

            cy.get(`.el-dropdown-menu:visible li:contains('${menuOp.ViewVehicle}')`).scrollIntoView().click()

            let _helper = new ComponentHelper("vehicle-detail-form")
            _helper.actionButton("vd-edit-btn").scrollIntoView().click()

            _helper.fillForm([
                {type: 'input', selector: 'name_in', value: Vehicle1_ED.Name},
                {type: 'select', selector: 'parking_space_sel', value: Vehicle1_ED.ParkingSpace},
                {type: 'select', selector: 'device_sel', value: Vehicle1_ED.AssignedDevice},
                {type: 'input', selector: 'start_date_dpk', value: Vehicle1_ED.StartDate},
                {type: 'input', selector: 'license_plate_in', value: Vehicle1_ED.LicensePlate},
                {type: 'input', selector: 'end_date_dpk', value: Vehicle1_ED.EndDate},
                {type: 'input', selector: 'license_plate_exp_dpk', value: Vehicle1_ED.LicensePlateExpiration},
                {type: 'select', selector: 'ownership_sel', value: Vehicle1_ED.Ownership},
                {type: 'select', selector: 'state_sel', value: Vehicle1_ED.State},
                {type: 'input', selector: 'type_in', value: Vehicle1_ED.Type},
                {type: 'input', selector: 'vin_in', value: Vehicle1_ED.VIN},
                {type: 'input', selector: 'make_in', value: Vehicle1_ED.Make},
                {type: 'input', selector: 'gas_card_in', value: Vehicle1_ED.GasCardLast6},
                {type: 'input', selector: 'model_in', value: Vehicle1_ED.Model},
                {type: 'select', selector: 'company_sel', value: Vehicle1_ED.Company},
                {type: 'input', selector: 'year_in', value: Vehicle1_ED.Year },
                {type: 'input', selector: 'rental_agreement_num_in', value: Vehicle1_ED.RentalAgreementNumber},
                // {type: 'check', selector: 'over_limit_sw', value: Vehicle1_ED.Over10kLBS},               // uncomment if this field is added again on UI
                // {type: 'check', selector: 'custom_delivery_sw', value: Vehicle1_ED.CustomDeliveryVan},   // uncomment if this field is added again on UI
                {type: 'input', selector: 'notes_txt', value: Vehicle1_ED.Notes},
            ])

            _helper.modalButtonContain("Update").scrollIntoView().click()
        })

        
    })
})