import ComponentHelper from "../../../support/Component.helper"
import MenuHelper from "../../../support/Menu.helper"
const {
    vehicle_management: {
        vehicle_list: {
            OdometerReading
        }
    }
} = Cypress.env('e2e');

describe('Vehicle Management / Vehicle List',function () {
    context('Vehicle options > Add Odometer Reading',function () {
        beforeEach(function () {
            MenuHelper.Tab.goTo("Vehicle Management","/vehicles/dashboard", "dashboard-option")

            //To make sure if the selected option is 'Vehicle List'
            cy.get('[data-cy="vehicle-management-options"]')
                .contains('.el-radio-button__inner','Vehicle List').click()
            
        })

        it('Test - Add Odometer Reading',function () {

            let menuOp = {
                ViewVehicle: "View Vehicle",
                AddOdometerReading: "Add Odometer Reading",
                ChangeVehicleStatus:"Change Vehicle Status"
            }

            new ComponentHelper('vehicle_list_table').actionButton('three_dot_button_row').first().click()
            cy.get('.el-dropdown-menu:visible').contains(menuOp.AddOdometerReading).click()
        
            let __helper = new ComponentHelper('odometer_reading_form')

            __helper.fillModalForm([
                {selector: "mileage_in", type: "input", value: OdometerReading.Mileage},
                {selector: "odometerDate_dpk", type: "input", value: OdometerReading.Date},
                {selector: "time_tpk", type: "input", value: OdometerReading.Time},
                {selector: "odometerType_sel", type: "select", value: OdometerReading.Type}
            ])

            cy.get('[data-cy="or-create-btn"]').click()
            cy.waitForLoaderToDisappear()
        })
    })
})