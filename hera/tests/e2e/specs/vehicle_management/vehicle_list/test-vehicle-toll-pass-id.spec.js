import ComponentHelper from "../../../support/Component.helper2"
import FunctionHelper from "../../../support/Function.helper"
import MenuHelper from "../../../support/Menu.helper"
const {
    vehicle_management: {
        vehicle_list: {
            Vehicle1
        }
    }
} = Cypress.env('e2e');

describe('Vehicle Management / Vehicle List',function () {
    context('Add-View-Edit Vehicle',function () {
        beforeEach(function () {
            MenuHelper.Tab.goTo("Vehicle Management","/vehicles/dashboard", "dashboard-option")

            //To make sure if the selected option is 'Vehicle List'
            cy.get('[data-cy="vehicle-management-options"]')
                .contains('.el-radio-button__inner','Vehicle List').click()
            
        })

        it('Test - Vehicle Toll pass ID',function () {
            cy.wait(1000)
            const vehicleName = Vehicle1.Name
            cy.get("[data-cy='vi-search-in']").type(vehicleName)

            const date = new FunctionHelper().getCurrentDate({
                format: 'mdY',
                separator: '/'
            })
            
            const toll_pass_id_message = `This is the "Toll Pass ID" field{selectAll}A test code will be entered{selectAll}`

            cy.get("table > tbody").wait(1000).then(()=>{
                if(Cypress.$(`tr:contains('${vehicleName}')`).length){
                    let menuOp = {
                        ViewVehicle: "View Vehicle",
                        AddOdometerReading: "Add Odometer Reading",
                        ChangeVehicleStatus:"Change Vehicle Status"
                    }
                    cy.get(`tr:contains('${vehicleName}')`).find(" [data-cy='three_dot_button_row']").first().scrollIntoView().click()
                    cy.get('.el-dropdown-menu:visible').contains(menuOp.ViewVehicle).click()
                }else{
                    let menuOp = {
                        AddVehicle: "Add Vehicle",
                        ExportVehicles: "Export Vehicles",
                        ImportVehicles:"Import Vehicles"
                    }
                    let __helper = new ComponentHelper('vehicle_list_table')
                    __helper.actionButton('vehicle_list_options').click()
                    cy.get('.el-dropdown-menu:visible').contains(menuOp.AddVehicle).click()

                    __helper = new ComponentHelper('vehicle_form')
                    __helper.fillValue({type: 'input', selector: 'toll_pass_id_in', value: `${toll_pass_id_message}TOLLPASSID`})

                    cy.wrap().then(()=>{
                        assert(true, '_**The "Toll Pass ID" field has been set**_')
                    })

                    cy.wait(1000)

                    __helper.fillForm([
                        {type: 'input', selector: 'name_in', value: Vehicle1.Name},
                        {type: 'select', selector: 'parking_space_sel', value: Vehicle1.ParkingSpace},
                        {type: 'select', selector: 'device_sel', value: Vehicle1.AssignedDevice},
                        {type: 'input', selector: 'start_date_dpk', value: Vehicle1.StartDate},
                        {type: 'input', selector: 'license_plate_in', value: Vehicle1.LicensePlate},
                        {type: 'input', selector: 'end_date_dpk', value: Vehicle1.EndDate},
                        {type: 'input', selector: 'license_plate_exp_dpk', value: Vehicle1.LicensePlateExpiration},
                        {type: 'select', selector: 'ownership_sel', value: Vehicle1.Ownership},
                        {type: 'select', selector: 'state_sel', value: Vehicle1.State},
                        {type: 'input', selector: 'type_in', value: Vehicle1.Type},
                        {type: 'input', selector: 'vin_in', value: Vehicle1.VIN},
                        {type: 'input', selector: 'make_in', value: Vehicle1.Make},
                        {type: 'input', selector: 'gas_card_in', value: Vehicle1.GasCardLast6},
                        {type: 'input', selector: 'model_in', value: Vehicle1.Model},
                        {type: 'select', selector: 'company_sel', value: Vehicle1.Company},
                        {type: 'input', selector: 'year_in', value: Vehicle1.Year },
                        {type: 'input', selector: 'rental_agreement_num_in', value: Vehicle1.RentalAgreementNumber},
                        {type: 'input', selector: 'notes_txt', value: Vehicle1.Notes}
                    ])
                    cy.get("[data-cy='create_btn']").scrollIntoView().click().then(()=>{
                        assert(true, '_**Vehicle was created**_')
                    })
                }
            })

            const __helper = new ComponentHelper('vehicle-detail-form')

            cy.get(`label:contains('Toll Pass ID'):visible`).should("be.visible").scrollIntoView({ offset: { top: -300} }).then(()=>{
                assert(true, '_**Toll Pass ID is Visible**_')
            })

            cy.get(`.el-form-item:contains('Toll Pass ID') .el-form-item__content`).invoke("text").then(text=>{
                assert(true, `_**Toll Pass ID value is: ${text.trim()}**_`)
            })

            cy.wait(500).log("_**[TEST - EDIT VEHICLE AND CHANGE TOLL PASS ID]()**_")

            __helper.actionButton("vd-edit-btn").scrollIntoView().click({force: true})
            
            __helper.fillModalValue({type: 'input', selector: 'toll_pass_id_in', value: `${toll_pass_id_message}TOLL-PASS-ID-EDITED`})

            cy.wait(500).then(()=>{
                assert(true, '_**The "Toll Pass ID" field has been edited**_')
            })

            __helper.modalButtonContain("Update").click()

            cy.get(`label:contains('Toll Pass ID'):visible`).scrollIntoView({ offset: { top: -300} })
            cy.get(`.el-form-item:contains('Toll Pass ID') .el-form-item__content`).invoke("text").then(text=>{
                assert(true, `_**Toll Pass ID is Visible**_`)
                assert(true, `_**Toll Pass ID value is: ${text.trim()}**_`)
            })
        })
    })
})