import ComponentHelper from "../../../support/Component.helper"
import MenuHelper from "../../../support/Menu.helper"

describe('Vehicle Management / Vehicle List',function () {
    context('List options > Export vehicles',function () {
        beforeEach(function () {
            MenuHelper.Tab.goTo("Vehicle Management","/vehicles/dashboard", "dashboard-option")

            //To make sure if the selected option is 'Vehicle List'
            cy.get('[data-cy="vehicle-management-options"]')
                .contains('.el-radio-button__inner','Vehicle List').scrollIntoView().click()
            
        })

        it('Test - Export vehicles',function () {
            cy.wait(1000)
            let menuOp = {
                AddVehicle: "Add Vehicle",
                ExportVehicles: "Export Vehicles",
                ImportVehicles:"Import Vehicles"
            }
            
            let __helper = new ComponentHelper('vehicle_list_table')

            __helper.actionButton('vehicle_list_options').click()
            cy.get('.el-dropdown-menu:visible').contains(menuOp.ExportVehicles).scrollIntoView().click()
            
            __helper.modal().find('.el-button--primary > span').contains('Export').click()
            
            let pad = s => s < 10 ? '0' + s : s
            let d = new Date()
            let dstr = [pad(d.getMonth()+1), pad(d.getDate()), d.getFullYear()].join('_')
            let ext = '.csv'
            let filename = 'vehicle-export--'+dstr+ext

            cy.readFile('cypress/downloads/'+filename).then(() => assert.exists(filename, '**EXPORT SUCCESS**') )
        })
    })
})