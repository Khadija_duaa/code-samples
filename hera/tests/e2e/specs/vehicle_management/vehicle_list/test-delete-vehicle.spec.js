import ComponentHelper from "../../../support/Component.helper"
import MenuHelper from "../../../support/Menu.helper"
const {
    filters:{
        Vehicle_Filter
    }
} = Cypress.env('e2e');

describe('Vehicle Management / Vehicle List',function () {
    context('View Vehicle > Delete Vehicle',function () {
        beforeEach(function () {
            MenuHelper.Tab.goTo("Vehicle Management","/vehicles/dashboard", "dashboard-option")

            //To make sure if the selected option is 'Vehicle List'
            cy.get('[data-cy="vehicle-management-options"]')
                .contains('.el-radio-button__inner','Vehicle List').click()
            
        })

        it('Test - Delete vehicle',function () {
            cy.wait(1000)
            cy.get("[data-cy='vi-search-in']").scrollIntoView().type(Vehicle_Filter)

            cy.get(`[data-cy='vehicle_list_table'] tr:contains('${Vehicle_Filter}')`).find(" [data-cy='three_dot_button_row']").first().click()

            let menuOp = {
                ViewVehicle: "View Vehicle",
                AddOdometerReading: "Add Odometer Reading",
                ChangeVehicleStatus:"Change Vehicle Status"
            }

            cy.get(`.el-dropdown-menu:visible li:contains('${menuOp.ViewVehicle}')`).scrollIntoView().click()

            let _helper = new ComponentHelper("vehicle-detail-form")
            _helper.actionButton("vd-edit-btn").scrollIntoView().click()
            _helper.modalButtonContain("Delete").scrollIntoView().then(($button) => {
                if ($button.prop('disabled')) {
                  assert(1, "Delete is not allowed");
                } else {
                    cy.get(".el-message-box:visible").contains("Delete").scrollIntoView().click()
                }
              });
        })
    })
})