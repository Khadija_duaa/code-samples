import ComponentHelper from "../../../support/Component.helper"
import MenuHelper from "../../../support/Menu.helper"

describe('Vehicle Management / Vehicle List',function () {
    context('List options > Import vehicles',function () {
        beforeEach(function () {
            MenuHelper.Tab.goTo("Vehicle Management","/vehicles/dashboard", "dashboard-option")

            //To make sure if the selected option is 'Vehicle List'
            cy.get('[data-cy="vehicle-management-options"]')
                .contains('.el-radio-button__inner','Vehicle List').scrollIntoView().click()
            
        })

        it('Test - Import vehicles',function () {

            let menuOp = {
                AddVehicle: "Add Vehicle",
                ExportVehicles: "Export Vehicles",
                ImportVehicles:"Import Vehicles"
            }
            
            let __helper = new ComponentHelper('vehicle_list_table')

            __helper.actionButton('vehicle_list_options').click()
            cy.get('.el-dropdown-menu:visible').contains(menuOp.ImportVehicles).click()
            
            cy.get('input[type="file"][accept=".csv"]').attachFile('vehicle-import-list.csv')

            /////
            let pad = s => s < 10 ? '0' + s : s
            let d = new Date()//today
            let dstr = [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/')

            let vehicle_name = `Test ${(d.getFullYear()+'').slice(-2)}${d.getMonth()+1}${d.getDay()}`

            //Random
            let rnd = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min

            let inputData = [
                {selector: 'name-in', value: vehicle_name},
                {selector: 'license-plate-in', value: 'ABC-XYZ'},
                {selector: 'type-in', value: 'CONVERTIBLE'},
                {selector: 'vin-in', value: 'TESTHERAVINP00000'},
                {selector: 'make-in', value: 'Hera'},
                {selector: 'gas-card-in', value: '000000'},
                // {selector: 'parking-space-in', value: '10000'},
                {selector: 'model-in', value: `Test ${d.getFullYear()}`},
                {selector: 'year-in', value: d.getFullYear() },
                {selector: 'rental-agreement-num-in', value: `HVL${rnd(10,90)}/${rnd(1000,9000)}/${d.getFullYear()}`}
            ]
            
            let time_piker_date = `${ d.getDate() }`

            __helper = new ComponentHelper('vehicle-import-form')

            __helper.actionButton('status-sel').last().then(x=>{
                cy.wrap(x).find('input').then( i => !i.first().val() ? cy.get(x).click().get('.el-select-dropdown:visible li').first().click() : null )
            })

            cy.wait(500)
            cy.get(".el-table__body-wrapper").last().then(($table) => {
                cy.wait(500)
                // Scroll the table 1500 pixels to the right
                $table[0].scrollLeft += 1500;
              });
            __helper.actionButton('start-date-dpk').last().then(x=>{
                cy.wrap(x).find('input').then( i => !i.first().val() ? cy.get(x).click().get('.el-date-picker:visible tr').contains(`${time_piker_date}`).click() : null )
            })
            
            cy.wait(500)
            __helper.actionButton('end-date-dpk').last().then(x=>{
                cy.wrap(x).find('input').then( i => !i.first().val() ? cy.get(x).click().get('.el-date-picker:visible tr').contains(`${time_piker_date}`).click() : null )
            })
            cy.get(".el-table__body-wrapper").last().then(($table) => {
                cy.wait(500)
                // Scroll the table 1500 pixels to the right
                $table[0].scrollLeft += 1500;
              });
            __helper.actionButton('license-plate-exp-dpk').last().scrollIntoView().then(x=>{
                cy.wrap(x).find('input').then( i => !i.first().val() ? i.first().val(dstr) : null )
            })

            __helper.actionButton('ownership-sel').last().then(x=>{
                cy.wrap(x).find('input').then( i => !i.first().val() ? cy.get(x).click().get('.el-select-dropdown:visible li').first().click() : null )
            })

            __helper.actionButton('authorized-to-drive').last().then(x=>{
                cy.wrap(x).find('input').then( i => !i.first().val() ? cy.get(x).click().get('.el-select-dropdown:visible li').first().click() : null )
            })

            __helper.actionButton('state-sel').last().then(x=>{
                cy.wrap(x).find('input').then( i => !i.first().val() ? cy.get(x).click().get('.el-select-dropdown:visible li').first().click() : null )
            })

            __helper.actionButton('company-sel').last().then(x=>{
                cy.wrap(x).find('input').then( i => !i.first().val() ? cy.get(x).click().get('.el-select-dropdown:visible li').first().click() : null )
            })

            cy.wrap(inputData).each(e =>{
                __helper.actionButton(e.selector).last().then( i => !i.first().val() ? cy.get(i).type(e.value, {force: true}) : null )
            })

            __helper.actionButton('submit-import-btn').click()
            __helper.alert('h2').contains("Vehicle Import Results").should("be.visible")
        })
    })
})