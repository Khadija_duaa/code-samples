import FunctionHelper from "../../../support/Function.helper";
import ComponentHelper from "../../../support/Component.helper";
import MenuHelper from "../../../support/Menu.helper";
const {
    daily_rostering: {
        stand_up_announcements: {
            StandUpAnnouncements
        }
    }
} = Cypress.env('e2e');

describe('Daily Roster Stand up Announcement', function () {

    context('Test send to Associate\'s button', function () {
        beforeEach(() => {
            cy.loginByUI(
                Cypress.env('cognito_username'),
                Cypress.env('cognito_password')
            );
            cy.location('pathname').should('eq', '/performance-and-coaching/performance');
            var today = new FunctionHelper().getCurrentDate({
                format: 'Ymd',
                separator: '-'
            });
            MenuHelper.Tab.goTo("Daily Rostering", '/daily-rostering/' + today + '/roster')
            cy.hideCookieInfo();
            cy.wait(4000);
        });

        it('Add text and send to Associate\'s', function () {
            let __helper = new ComponentHelper('standup-announcements');
            cy.wait(4000);
            __helper.actionButton('edit').scrollIntoView().click()
            cy.wait(4000);
            __helper.fillModalForm([
                { type: 'input', selector: 'message', value: StandUpAnnouncements.Message },
            ]);
            cy.wait(4000);
            __helper.modalButton('send-to-das').scrollIntoView().click()
            __helper.messageBoxConfirm()
            cy.wait(4000); //wait to confirm save
        });

        it('Remove text and send to DA\'s', function () {
            let __helper = new ComponentHelper('standup-announcements');
            cy.wait(4000);
            __helper.actionButton('edit').scrollIntoView().click()
            cy.wait(4000);
            __helper.fillModalForm([
                { type: 'input', selector: 'message', value: StandUpAnnouncements.Message },
            ]);
            cy.wait(4000);
            __helper.modalButton('send-to-das').scrollIntoView().click()
            __helper.messageBoxConfirm()
            cy.wait(4000); //wait to confirm save
        });

        it('Add text, add image, and send to Associate\'s', function () {
            let __helper = new ComponentHelper('standup-announcements');
            cy.wait(4000);
            __helper.actionButton('edit').scrollIntoView().click()
            __helper.detachIfExist();
            cy.wait(4000);
            __helper.modalButton('attachment').scrollIntoView().click()
            cy.wait(4000);
            __helper.fillModalForm([
                { type: 'input', selector: 'message', value: StandUpAnnouncements.Message },
                { type: 'file', selector: 'attachment-input', value: StandUpAnnouncements.File, ttw: 3000 },
            ]);
            cy.wait(4000);
            __helper.modalButton('send-to-das').scrollIntoView().click()
            __helper.messageBoxConfirm()
            cy.wait(4000); //wait to confirm save
        });

        it('delete image, delete expiration and send to DA\'s', function () {
            let __helper = new ComponentHelper('standup-announcements');
            cy.wait(4000);
            __helper.actionButton('edit').scrollIntoView().click()
            __helper.detachIfExist();
            cy.wait(4000);
            __helper.fillModalForm([
                { type: 'input', selector: 'message', value: '' },
            ]);
            cy.get('[data-cy=remove-img] > .uil').click({ force: true })
            cy.wait(4000);
            __helper.fillModalForm([
                { type: 'input', selector: 'message', value: StandUpAnnouncements.Message },
                { type: 'file', selector: 'attachment-input', value: StandUpAnnouncements.File, ttw: 3000 },
            ]);
            cy.wait(4000);
            __helper.modalButton('send-to-das').scrollIntoView().click()
            __helper.messageBoxConfirm()
            cy.wait(4000); //wait to confirm save
        });

    })
})





