import FunctionHelper from "../../../support/Function.helper";
import ComponentHelper from "../../../support/Component.helper";
import MenuHelper from "../../../support/Menu.helper";
const {
    daily_rostering: {
        stand_up_announcements: {
            StandUpAnnouncements
        }
    }
} = Cypress.env('e2e');

describe('Daily Roster Stand up Announcement', function () {

    context('Standup Anouncement Notes', function () {
        beforeEach(() => {
            cy.loginByUI(
                Cypress.env('cognito_username'),
                Cypress.env('cognito_password')
            );
            cy.location('pathname').should('eq', '/performance-and-coaching/performance');
            var today = new FunctionHelper().getCurrentDate({
                format: 'Ymd',
                separator: '-'
            });
            MenuHelper.Tab.goTo("Daily Rostering", '/daily-rostering/' + today + '/roster')
            cy.hideCookieInfo();
            cy.wait(4000);
        });

        it('Add text and cancel', function () {
            cy.wait(4000);
            let __helper = new ComponentHelper('standup-announcements');
            cy.wait(4000);
            __helper.actionButton('edit').scrollIntoView().click()
            cy.wait(4000);
            __helper.fillModalForm([
                { type: 'input', selector: 'message', value: StandUpAnnouncements.Message },
            ]);
            __helper.modalButton('cancel').scrollIntoView().click()
        });

        it('Remove text and cancel', function () {
            cy.wait(4000);
            let __helper = new ComponentHelper('standup-announcements');
            cy.wait(4000);
            __helper.actionButton('edit').scrollIntoView().click()
            cy.wait(4000);
            __helper.fillModalForm([
                { type: 'input', selector: 'message', value: StandUpAnnouncements.Message },
            ]);
            __helper.modalButton('cancel').scrollIntoView().click()
        });

        it('Add text, add image and cancel', function () {
            cy.wait(4000);
            let __helper = new ComponentHelper('standup-announcements');
            cy.wait(4000);
            __helper.actionButton('edit').scrollIntoView().click()
            cy.wait(4000);
            __helper.detachIfExist();
            cy.wait(4000);
            __helper.modalButton('attachment').scrollIntoView().click()
            cy.wait(4000);
            __helper.fillModalForm([
                { type: 'input', selector: 'message', value: StandUpAnnouncements.Message },
                { type: 'file', selector: 'attachment-input', value: StandUpAnnouncements.File, ttw: 3000 },
            ]);
            __helper.modalButton('cancel').scrollIntoView().click()
        });

        it('Add text, add image, delete image, delete expiration and cancel', function () {
            cy.wait(4000);
            let __helper = new ComponentHelper('standup-announcements');
            cy.wait(4000);
            __helper.actionButton('edit').scrollIntoView().click()
            cy.wait(4000);
            __helper.detachIfExist();
            cy.wait(4000);
            __helper.modalButton('attachment').scrollIntoView().click()
            cy.wait(4000);
            __helper.fillModalForm([
                { type: 'input', selector: 'message', value: StandUpAnnouncements.Message },
                { type: 'file', selector: 'attachment-input', value: StandUpAnnouncements.File, ttw: 3000 },
            ]);
            // __helper.detachIfExist();
            __helper.modalButton('cancel').scrollIntoView().click()
        });
    })
})



