import FunctionHelper from "../../../support/Function.helper";
import ComponentHelper from "../../../support/Component.helper";
import MenuHelper from "../../../support/Menu.helper";
const {
    daily_rostering: {
                stand_up_announcements: {
                    StandUpAnnouncements
                }
            }
} = Cypress.env('e2e');

describe('Daily Roster Stand up Announcement', function () {

    context('Test update button', function () {
        beforeEach(() => {
            cy.loginByUI(
                Cypress.env('cognito_username'),
                Cypress.env('cognito_password')
            );
            cy.location('pathname').should('eq', '/performance-and-coaching/performance');
            var today = new FunctionHelper().getCurrentDate({
                format: 'Ymd',
                separator: '-'
            });
            MenuHelper.Tab.goTo("Daily Rostering", '/daily-rostering/' + today + '/roster')
            cy.hideCookieInfo();
            cy.wait(4000);
        });

        it('Add text and update', function () {
            let __helper = new ComponentHelper('standup-announcements');
                        __helper.actionButton('edit').scrollIntoView().click()
                        cy.wait(4000);
                        __helper.fillModalForm([
                                {type: 'input', selector: 'message', value: 'This message will be update on announcements'},
                            ]);
                            cy.wait(4000);
                        __helper.modalButton('update').scrollIntoView().click()
                        cy.wait(4000); //wait to confirm save
        });

        it('Remove text and update', function () {
            let __helper = new ComponentHelper('standup-announcements');
            __helper.actionButton('edit').scrollIntoView().click()
            cy.wait(4000);
            __helper.fillModalForm([
                    {type: 'input', selector: 'message', value: 'This another message will be update on announcements but i remove it{selectall}{del}'},
                ]);
                cy.wait(4000);
            __helper.modalButton('update').scrollIntoView().click()
            cy.wait(4000); //wait to confirm save
        });

        it('Add text, add image and update', function () {
            let __helper = new ComponentHelper('standup-announcements');
            __helper.actionButton('edit').scrollIntoView().click()
            __helper.detachIfExist();
            cy.wait(4000);
            __helper.modalButton('attachment').scrollIntoView().click()
            cy.wait(4000);
            __helper.fillModalForm([
                {type: 'input', selector: 'message', value:  StandUpAnnouncements.Message},
                {type: 'file', selector: 'attachment-input', value:  StandUpAnnouncements.File, ttw: 3000},
            ]);
            cy.wait(4000);
            __helper.modalButton('update').scrollIntoView().click()
            cy.wait(4000); //wait to confirm save
        });

        it('Add text, add image, delete image, delete expiration and update', function () {
            let __helper = new ComponentHelper('standup-announcements');
            __helper.actionButton('edit').scrollIntoView().click()
            __helper.detachIfExist();
            cy.wait(4000);
            // __helper.modalButton('attachment').scrollIntoView().click()
            cy.get('[data-cy=remove-img] > .uil').click({ force: true })
            cy.wait(4000);
            __helper.fillModalForm([
                {type: 'input', selector: 'message', value:  StandUpAnnouncements.Message},
            ]);
            cy.wait(4000);
            __helper.modalButton('update').scrollIntoView().click()
            cy.wait(4000); //wait to confirm save
        });

    })
})



