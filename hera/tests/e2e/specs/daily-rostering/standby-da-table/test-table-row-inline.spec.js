import ComponentHelper from "../../../support/Component.helper";
import FunctionHelper from "../../../support/Function.helper";
import MenuHelper from "../../../support/Menu.helper";
const {
    daily_rostering: {
        standby_da_table: {
            StandbyDA_ED
        }
    }
} = Cypress.env('e2e');

describe('Daily Roster', function () {

    context('Standby delivery associates table', function () {
        beforeEach(() => {
            cy.loginByUI(
                Cypress.env('cognito_username'),
                Cypress.env('cognito_password')
            );
            cy.location('pathname').should('eq', '/performance-and-coaching/performance');
            var today = new FunctionHelper().getCurrentDate({
                format: 'Ymd',
                separator: '-'
            });
            MenuHelper.Tab.goTo("Daily Rostering", '/daily-rostering/' + today + '/roster')
            cy.hideCookieInfo();
            cy.wait(4000);
        });

        it('Test table row functionality', function () {
            (new ComponentHelper('standby-da-table'))
                .fillTable( 1, [
                    {type: 'select', selector: 'staffId', value: StandbyDA_ED.DeliveryAssociate, ttw: 2000},
                    {type: 'select', selector: 'status', value: StandbyDA_ED.Status, ttw: 2000},
                ]);
        });

        it('Test table row delete functionality', function () {
            cy.wait(3500)
            let __helper = new ComponentHelper('standby-da-table', {x: 0, y: 500}).scroll();
            // __helper.actionButton('delete').scrollIntoView().click();
            cy.get(':nth-child(1) > .el-table_11_column_17 > .cell > [data-cy=delete] > .icon-button').click({multiple:true, force:true})
            cy.wait(4000);
            cy.get('.el-message-box__btns > .el-button--primary').click({force:true});
        });
    })
})
