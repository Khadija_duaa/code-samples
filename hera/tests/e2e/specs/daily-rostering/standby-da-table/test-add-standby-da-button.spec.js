import ComponentHelper from "../../../support/Component.helper";
import FunctionHelper from "../../../support/Function.helper";
import MenuHelper from "../../../support/Menu.helper";
const {
    daily_rostering: {
        standby_da_table: {
            StandbyDA
        }
    }
} = Cypress.env('e2e');

describe('Daily Roster', function () {

    context('Standby delivery associates table', function () {
        beforeEach(() => {
            cy.loginByUI(
                Cypress.env('cognito_username'),
                Cypress.env('cognito_password')
            );
            cy.location('pathname').should('eq', '/performance-and-coaching/performance');
            var today = new FunctionHelper().getCurrentDate({
                format: 'Ymd',
                separator: '-'
            });
            MenuHelper.Tab.goTo("Daily Rostering", '/daily-rostering/' + today + '/roster')
            cy.hideCookieInfo();
            cy.wait(4000);
        });

        it('Add a standby DA', function () {
            let __helper = new ComponentHelper('standby-da-table');
            cy.get('[data-cy=add-standby-da] > span').click({force:true})
            __helper.fillModalForm([
                {type: 'select', selector: 'driver', value: 'The Joker', ttw: 2500},
                {type: 'select', selector: 'status', value: StandbyDA.Status, ttw: 2500},
            ]);
            __helper.modalButton('confirm').scrollIntoView().click();
            cy.log('Waiting for result');
            cy.wait(4000);
        });

        it('Add Single DA Assignment for a current date and cancel', function () {
            let __helper = new ComponentHelper('standby-da-table');
            cy.get('[data-cy=add-standby-da] > span').click({force:true})
            __helper.fillModalForm([
                {type: 'select', selector: 'driver', value: 'Tom Jerry', ttw: 2500},
                {type: 'select', selector: 'status', value: StandbyDA.Status, ttw: 2500},
            ]);
            __helper.modalButton('cancel').scrollIntoView().click();
        });
    })
})
