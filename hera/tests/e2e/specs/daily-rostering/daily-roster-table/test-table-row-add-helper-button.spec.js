import FunctionHelper from "../../../support/Function.helper";
import ComponentHelper from "../../../support/Component.helper";
import MenuHelper from "../../../support/Menu.helper"; 
const {
    daily_rostering: {
        daily_roster_table: {
            HelperDA
        }
    }
} = require('/e2e-cypress.config.js');

describe('Daily Roster', function () {

    context('Daily Roster table', function () {
        beforeEach(() => {
            cy.loginByUI(
                Cypress.env('cognito_username'),
                Cypress.env('cognito_password')
            );
            
            cy.hideCookieInfo();
            cy.wait(1000);
        });

        it('Test table row Add Helper', function () {
            var today = new FunctionHelper().getCurrentDate({
                format: 'Ymd',
                separator: '-'
            });
            
            MenuHelper.Tab.goTo("Daily Rostering", '/daily-rostering/'+ today + '/roster')
            cy.wait(4000);
            let __helper = new ComponentHelper('daily-roster-table');

          __helper.actionButton('status_3dot_button1').first().realClick()
          cy.get('.el-dropdown-menu').contains('Add Helper').scrollIntoView({ duration: 1000 }).click({force: true})
          cy.wait(4000) 

          __helper
                .fillForm([
                    {type: 'select', selector: 'choose-helper', value: HelperDA.Helper, ttw: 2500},
                    {type: 'select', selector: 'choose-helper-status', value: HelperDA.Status, ttw: 2500},
                ])
                
            cy.log('Waiting for result');
            cy.wait(4000)
        });

        it('Test table row Remove Helper', function () {
            let __helper = new ComponentHelper('daily-roster-table')
            __helper.actionButton('remove-helper').click({force: true});
            cy.log('Waiting for result');
            cy.wait(4000)
        });
    })
})