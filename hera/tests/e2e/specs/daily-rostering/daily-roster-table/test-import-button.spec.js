import ComponentHelper from "../../../support/Component.helper";
import FunctionHelper from "../../../support/Function.helper";
import MenuHelper from "../../../support/Menu.helper";  

const fs = require('fs');

describe('Daily Roster', function () {

    context('Daily Roster table', function () {
        beforeEach(() => {
            cy.loginByUI(
                Cypress.env('cognito_username'),
                Cypress.env('cognito_password')
            );

            cy.location('pathname').should('eq', '/performance-and-coaching/performance');
            cy.hideCookieInfo();

        });

        it('Test Import Daily Roaster', function () {
            cy.wait(1000);
            var today = new FunctionHelper().getCurrentDate({
                format: 'Ymd',
                separator: '-'
            });

            MenuHelper.Tab.goTo("Daily Rostering", '/daily-rostering/'+ today + '/roster');
            cy.wait(1000);
            let __helper = new ComponentHelper('daily-roster-table')
            __helper.actionButton('import').click({force: true});
            __helper.fillValue({
                type: 'file',
                selector: 'attach-file',
                value: 'daily-roster-import-example.xlsx',
                ttw: 3500
            });
            __helper.actionButton('import-cancel').click({force: true})
            __helper.messageBoxConfirm()
        });
    })
})
