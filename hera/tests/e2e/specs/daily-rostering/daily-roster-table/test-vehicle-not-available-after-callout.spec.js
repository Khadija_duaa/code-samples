import FunctionHelper from "../../../support/Function.helper"
import MenuHelper from "../../../support/Menu.helper"
import ComponentHelper from "../../../support/Component.helper2"
describe("Daily Roster", () => {
    context("Daily Roster table", () => {
        beforeEach(() => {
            cy.loginByUI(
                Cypress.env('cognito_username'),
                Cypress.env('cognito_password')
            );
            const today = new FunctionHelper().getCurrentDate({
                format: 'Ymd',
                separator: '-'
            })
            MenuHelper.Tab.goTo("Daily Rostering", '/daily-rostering/' + today + '/roster')
            cy.hideCookieInfo()
        })
        it("Test table row functionality - DA Vehicle Not Available After Callout", () => {
            const __helper = new ComponentHelper('daily-roster-table')
            __helper.actionButton('single-da').scrollIntoView().click()
            __helper.fillModalForm([
                { type: 'select', selector: 'staffId', value: 'Alan Kline' },
                { type: 'select', selector: 'deviceId', value: '10' },
                { type: 'input', selector: 'route', value: 'a best route ever' },
                { type: 'input', selector: 'staging', value: 'a 1st staging' },
                { type: 'select', selector: 'parkingSpace', value: 'HERA' },
                { type: 'select', selector: 'status', value: 'On Time' },
                { type: 'time', selector: 'time', value: { h: '09 am', m: '54' } },
                { type: 'input', selector: 'notes', value: 'a default note' },
            ])
            __helper.modalButton('confirm').scrollIntoView().click()
            cy.log('Waiting for result').then(() => {
                assert(true, '**A Roster Single DA created successfully**')
            })
            cy.wait(4000)
            __helper.actionButton('single-da').scrollIntoView().click()
            __helper.fillModalForm([
                { type: 'select', selector: 'staffId', value: 'Alan Kline' },
                { type: 'select', selector: 'deviceId', value: '10' },
                { type: 'input', selector: 'route', value: 'a best route ever' },
                { type: 'input', selector: 'staging', value: 'a 1st staging' },
                { type: 'select', selector: 'parkingSpace', value: 'HERA' },
                { type: 'select', selector: 'status', value: 'On Time' },
                { type: 'time', selector: 'time', value: { h: '09 am', m: '54' } },
                { type: 'input', selector: 'notes', value: 'a default note' },
            ])
            __helper.modalButton('confirm').scrollIntoView().click()
            cy.log('Waiting for result').then(() => {
                assert(true, '**Another Roster Single DA created successfully**')
            })
            cy.wait(4000)

            __helper.getTableRow(0, 'status').find(":text").invoke("val").should("eq", "On Time")

            cy.wait(1000).then(() => {
                assert(true, '**Status is in "On Time"**')
            })

            __helper.getTableRow(0, 'status').scrollIntoView().click()
            cy.get(".el-select-dropdown:visible").contains("Called Out").scrollIntoView().click()
            __helper.modalButtonContain("Confirm").scrollIntoView().click()
            cy.wait(1000).then(() => {
                assert(true, '**Status was changed to "Called Out"**')
            })
            cy.wait(5000)
        })

    })
})