import FunctionHelper from "../../../support/Function.helper";
import ComponentHelper from "../../../support/Component.helper";
import MenuHelper from "../../../support/Menu.helper";
const {
    daily_rostering: {
        daily_roster_table: {
            DailyRoster_ED
        }
    }
} = Cypress.env('e2e');

describe('Daily Roster', function () {

    context('Daily Roster table', function () {
        beforeEach(() => {
            cy.loginByUI(
                Cypress.env('cognito_username'),
                Cypress.env('cognito_password')
            );
            cy.location('pathname').should('eq', '/performance-and-coaching/performance');
            var today = new FunctionHelper().getCurrentDate({
                format: 'Ymd',
                separator: '-'
            });
            MenuHelper.Tab.goTo("Daily Rostering", '/daily-rostering/'+ today + '/roster')
            cy.hideCookieInfo();
            // cy.hideChatInfo()
        });

        it('Test table row functionality', function () {
            let __helper = new ComponentHelper('daily-roster-table')
            __helper.fillForm([
               // {type: 'time', selector: 'time', value: DailyRoster_ED.Time},
                {type: 'select', selector: 'staffId', value: DailyRoster_ED.DeliveryAssociate},
                {type: 'select', selector: 'vehicleId', value: DailyRoster_ED.Vehicle},
                {type: 'select', selector: 'deviceId', value: DailyRoster_ED.Device},
            //     {type: 'input', selector: 'route', value: DailyRoster_ED.Route},
           //    {type: 'input', selector: 'staging', value: DailyRoster_ED.Staging},
               {type: 'select', selector: 'parkingSpace', value: DailyRoster_ED.ParkingSpace},
               {type: 'select', selector: 'status', value: DailyRoster_ED.Status},
                
            ]);
        });
    })
})
