import FunctionHelper from "../../../support/Function.helper";
import ComponentHelper from "../../../support/Component.helper2";
import MenuHelper from "../../../support/Menu.helper";

const {
    daily_rostering: {
        daily_roster_table: {
            DailyRoster,
            ReplaceWithStandByDA
        }
    }
} = Cypress.env('e2e');

const today = new FunctionHelper().getCurrentDate({
    format: 'Ymd',
    separator: '-'
});



context('HERA-1024', function () {

    
    it('Test Add Driver Issue with counseling and check createdFrom field', function () {
        MenuHelper.Tab.goTo("Daily Rostering", '/daily-rostering/'+ today + '/roster')
        let __helper = new ComponentHelper('daily-roster-table');
        __helper.actionButton('single-da').scrollIntoView().click();
        __helper.fillModalForm([
            {type: 'select', selector: 'staffId', value: DailyRoster.DeliveryAssociate},
            {type: 'select', selector: 'vehicleId', value: DailyRoster.Vehicle},
        ]);
       // cy.get('[data-cy="deviceId"]').scrollIntoView().click().type(DailyRoster.Device).wait(100)
       // cy.get('.el-select-group:visible').contains(DailyRoster.Device).scrollIntoView().click()
        __helper.fillModalForm([
            {type: 'input', selector: 'route', value: DailyRoster.Route},
            {type: 'input', selector: 'staging', value: DailyRoster.Staging},
            {type: 'select', selector: 'parkingSpace', value: DailyRoster.ParkingSpace},
            {type: 'select', selector: 'status', value: DailyRoster.Status},
            {type: 'time', selector: 'time', value: DailyRoster.Time},
            {type: 'input', selector: 'notes', value: DailyRoster.Notes},
        ]);
        // __helper.modalButton('confirm').scrollIntoView().click();
        // cy.log('Waiting for result');
        // cy.wait(1000);

        // __helper.getTableRow(0,"status_3dot_button").scrollIntoView().click()
        // cy.get('.el-dropdown-menu:visible [data-cy="replace-standby-da"]').scrollIntoView().click()
        // __helper
        //     .fillModalForm([
        //         {type: 'select', selector: 'replacement-reason', value: ReplaceWithStandByDA.ReasonforReplacement, ttw: 1500},
        //         {type: 'check', selector: 'driver-issue-chk', value: true},
        //         {type: 'check', selector: 'driver-counseling-chk', value: true},
        //     ])
        new ComponentHelper('daily-roster-table')
            .modalButtonContain('Confirm')
            .scrollIntoView().click()
    
        cy.wait(1000)

        MenuHelper.Tab.goTo("Performance & Coaching", '/performance-and-coaching/performance')

        __helper = new ComponentHelper('performance-coaching-container');
        cy.contains('Counselings').scrollIntoView().click()
        //cy.contains('View All Counselings').click({force: true})
        cy.wait(4000)

        // __helper = new ComponentHelper('counseling-container');
        // __helper.fillValue({type: 'input', selector: 'filter', value: today})

        // __helper.getTableRow(0,"ci-elipsis-btn").scrollIntoView().click()
        // cy.get('.el-dropdown-menu:visible').contains('View Counseling').scrollIntoView().click()

        // cy.get('label:contains(Created From)').scrollIntoView({ offset: { top: -120 } })
        // cy.wait(1000)
    });

    // it('Delete Daily Roster created', function () {
    //     cy.visit('/daily-roster/' + today);
    //     cy.wait(5000)
    //     cy.get('body').find('[data-cy="status_3dot_button"]').click();
    //     cy.get('body').find('.el-dropdown-menu:visible').find('li').contains('Edit Associate Assignment').click({force: true})
    //     cy.get('body').find('[role="dialog"]:visible').find('button').contains('Delete').click()
    //     cy.get('body').find('.el-message-box:visible').find('button').contains('Delete').click()
    //     cy.wait(5000)
        
    // }) 
    
})