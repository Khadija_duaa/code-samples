import ComponentHelper from "../../../support/Component.helper";
import FunctionHelper from "../../../support/Function.helper";
import MenuHelper from "../../../support/Menu.helper";
const {
    daily_rostering: {
        daily_roster_table: {
            DailyRoster
        }
    }
} = Cypress.env('e2e');

describe('Daily Roster', function () {

    context('Daily Roster table', function () {
        beforeEach(() => {
            cy.loginByUI(
                Cypress.env('cognito_username'),
                Cypress.env('cognito_password')
            );

            cy.hideCookieInfo();
            cy.wait(4000);
        });

        it('Add Single DA Assignment for a current date', function () {
            var today = new FunctionHelper().getCurrentDate({
                format: 'Ymd',
                separator: '-'
            });
            
            MenuHelper.Tab.goTo("Daily Rostering", '/daily-rostering/'+ today + '/roster')
            cy.wait(4000);
            let __helper = new ComponentHelper('daily-roster-table');
            __helper.actionButton('single-da').scrollIntoView().click();
            cy.wait(4000);
            __helper.fillModalForm([
                {type: 'select', selector: 'staffId', value: DailyRoster.DeliveryAssociate},
                {type: 'select', selector: 'vehicleId', value: DailyRoster.Vehicle},
                {type: 'select', selector: 'deviceId', value: DailyRoster.Device},
                {type: 'input', selector: 'route', value: DailyRoster.Route},
                {type: 'input', selector: 'staging', value: DailyRoster.Staging},
                {type: 'select', selector: 'parkingSpace', value: DailyRoster.ParkingSpace},
                // {type: 'select', selector: 'status', value: DailyRoster.Status},
                // {type: 'time', selector: 'time', value: DailyRoster.Time},
                // {type: 'input', selector: 'notes', value: DailyRoster.Notes},
            ]);
            __helper.modalButton('confirm').scrollIntoView().click();
            cy.log('Waiting for result');
            cy.wait(4000);
        });

        // it('Add Single DA Assignment for a current date and cancel', function () {
        //     let __helper = new ComponentHelper('daily-roster-table');
        //     __helper.actionButton('single-da').scrollIntoView().click();
        //     __helper.fillModalForm([
        //         {type: 'select', selector: 'staffId', value: DailyRoster.DeliveryAssociate, ttw: 2000},
        //         {type: 'select', selector: 'vehicleId', value: DailyRoster.Vehicle, ttw: 2000},
        //         {type: 'select', selector: 'deviceId', value: DailyRoster.Device, ttw: 2000},
        //         {type: 'input', selector: 'route', value: DailyRoster.Route},
        //         {type: 'input', selector: 'staging', value: DailyRoster.Staging},
        //         {type: 'select', selector: 'parkingSpace', value: DailyRoster.ParkingSpace, ttw: 2000},
        //         {type: 'select', selector: 'status', value: DailyRoster.Status, ttw: 2000},
        //         {type: 'time', selector: 'time', value: DailyRoster.Time, ttw: 2000},
        //         {type: 'input', selector: 'notes', value: DailyRoster.Notes},
        //     ]);
        //     __helper.modalButton('cancel').scrollIntoView().click();
        // });
    })
})
