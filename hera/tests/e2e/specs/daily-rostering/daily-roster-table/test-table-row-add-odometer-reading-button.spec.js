import FunctionHelper from "../../../support/Function.helper";
import ComponentHelper from "../../../support/Component.helper";
import MenuHelper from "../../../support/Menu.helper"; 

describe('Daily Roster', function () {

    context('Daily Roster table', function () {
        beforeEach(() => {
            cy.loginByUI(
                Cypress.env('cognito_username'),
                Cypress.env('cognito_password')
            );
            cy.location('pathname').should('eq', '/performance-and-coaching/performance');
            var today = new FunctionHelper().getCurrentDate({
                format: 'Ymd',
                separator: '-'
            });
            MenuHelper.Tab.goTo("Daily Rostering", '/daily-rostering/'+ today + '/roster')
            
            cy.hideCookieInfo();
        });
        
        it('Test table row Add Rescuer', function () {
           
            let __helper = new ComponentHelper('daily-roster-table')
            __helper.actionButton('status_3dot_button1').first().realClick()
            cy.get('.el-dropdown-menu').contains('Add Rescuer').scrollIntoView({ duration: 1000 }).click({force: true})
          cy.wait(4000)
           
            __helper
                .fillForm([
                    {type: 'select-multi', selector: 'choose-rescuer', value: ['Alex Ar', 'Aladdin Ali', 'Aleeya Clark'], ttw: 2500},
                ])
            cy.log('Waiting for result');
            cy.wait(4000)
        });

        it('Test table row Remove Rescuer', function () {
            let __helper = new ComponentHelper('daily-roster-table')
            __helper.actionButton('status_3dot_button1').first().realClick()
            cy.get('.el-dropdown-menu').contains('Remove Rescuer').scrollIntoView({ duration: 1000 }).click({force: true})
            cy.log('Waiting for result');
            cy.wait(4000)
        });
    })
})