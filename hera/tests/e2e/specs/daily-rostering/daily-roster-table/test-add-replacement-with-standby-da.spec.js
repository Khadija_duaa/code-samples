import FunctionHelper from "../../../support/Function.helper";
import ComponentHelper from "../../../support/Component.helper";
import MenuHelper from "../../../support/Menu.helper"; 
const {
    daily_rostering: {
        daily_roster_table: {
            ReplaceWithStandByDA
        }
    }
} = Cypress.env('e2e');

describe('Daily Roster', function () {

    context('Daily Roster table', function () {
        beforeEach(() => {
            cy.loginByUI(
                Cypress.env('cognito_username'),
                Cypress.env('cognito_password')
            );

            cy.location('pathname').should('eq', '/performance-and-coaching/performance');
            cy.hideCookieInfo();
            cy.wait(1000);
        });
        
        it('Test Add replacement with Stand by DA', function () {
            cy.wait(1000);
            var today = new FunctionHelper().getCurrentDate({
                format: 'Ymd',
                separator: '-'
            });
            MenuHelper.Tab.goTo("Daily Rostering", '/daily-rostering/'+ today + '/roster')
            cy.wait(1000);

            let __helper = new ComponentHelper('standby-da-table')
            __helper.actionButton('add-standby-da').click({force: true});
            __helper
                .fillModalForm([
                    {type: 'select', selector: 'driver', value:'Alex Ar', ttw: 2500},
                    {type: 'select', selector: 'status', value: 'VTO', ttw: 2500},
                ])
            new ComponentHelper('standby-da-table')
                .modalButtonContain('Confirm')
                .scrollIntoView().click()
        });
        
        // it('Test remove replacement', function () {
        //     let __helper = new ComponentHelper('daily-roster-table')
        //     __helper.actionButton('replacement_3dot_button').scrollIntoView().click()
        //     __helper.actionButton('remove-replacement').scrollIntoView().click()
        //     new ComponentHelper('daily-roster-table').modalButtonContain('Yes').scrollIntoView().click()
        // });
    })
})
