import ComponentHelper from "../../../support/Component.helper";
import FunctionHelper from "../../../support/Function.helper";
import MenuHelper from "../../../support/Menu.helper";  
const fs = require('fs');

describe('Daily Roster', function () {

    context('Daily Roster table', function () {
        beforeEach(() => {
            cy.loginByUI(
                Cypress.env('cognito_username'),
                Cypress.env('cognito_password')
            );

            cy.location('pathname').should('eq', '/performance-and-coaching/performance');
            cy.hideCookieInfo();
            cy.wait(1000);
        });

        it('Test Export Daily Roaster', function () {
            var today = new FunctionHelper().getCurrentDate({
                format: 'Ymd',
                separator: '-'
            });
            MenuHelper.Tab.goTo("Daily Rostering", '/daily-rostering/'+ today + '/roster')
            cy.wait(1000);
           
            let __helper = new ComponentHelper('daily-roster-table')

            __helper.actionButton('export').click({force: true});

        });
    })
})
