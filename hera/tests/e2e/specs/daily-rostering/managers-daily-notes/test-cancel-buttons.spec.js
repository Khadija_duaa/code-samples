import FunctionHelper from "../../../support/Function.helper";
import ComponentHelper from "../../../support/Component.helper";
import MenuHelper from "../../../support/Menu.helper";
const {
    daily_rostering: {
        daily_roster_table: {
            DailyRoster_ED
        }
    }
} = Cypress.env('e2e');

describe('Daily Roster', function () {

    context('Managers Daily Notes', function () {
        beforeEach(() => {
            cy.loginByUI(
                Cypress.env('cognito_username'),
                Cypress.env('cognito_password')
            );
            cy.location('pathname').should('eq', '/performance-and-coaching/performance');
            var today = new FunctionHelper().getCurrentDate({
                format: 'Ymd',
                separator: '-'
            });
            MenuHelper.Tab.goTo("Daily Rostering", '/daily-rostering/' + today + '/roster')
            cy.hideCookieInfo();
            cy.wait(4000);
        });

        it('Add text and cancel', function () {
            cy.wait(4000);
            let __helper = new ComponentHelper('managers-daily-notes')
            cy.wait(4000)
            cy.get('[data-cy="edit"]').click({ multiple: true, force: true });
            cy.wait(4000);
            __helper.fillModalForm([
                { type: 'input', selector: 'message', value: 'testing' },
            ]);
            __helper.modalButton('cancel').scrollIntoView().click();
        });

        it('Remove text and cancel', function () {
            cy.wait(4000);
            let __helper = new ComponentHelper('managers-daily-notes');
            cy.wait(4000);
            cy.get('[data-cy="edit"]').click({ multiple: true, force: true });
            cy.wait(4000);
            __helper.fillModalForm([
                { type: 'input', selector: 'message', value: 'testing' },
            ]);
            __helper.modalButton('cancel').scrollIntoView().click();
        });
    })
})


