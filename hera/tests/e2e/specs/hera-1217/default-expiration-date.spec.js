describe('HERA-1217', () => {
    context('Default expiration date.', () => {
        
        it('Validate default expiration date.', function () {
            cy.get('[data-cy="messenger-indicator-btn"]').click()
            cy.wait(4000);
            cy.scrollTo('bottom',{ensureScrollable: false}) 
            cy.wait(4000);
            cy.get('.conversations > :nth-child(1)').click()
            cy.wait(4000);
            cy.get('[data-cy="attachment-button"]').click()
            cy.wait(3000);
            cy.get('[data-cy="cancel-button"]').click()
            cy.wait(1000);
        })
    })
})