import MenuHelper from "../../support/Menu.helper"
import ComponentHelper from "../../support/Component.helper"
import {ImportDAExampleData} from "../../../../e2e-cypress.config";

describe('Add Vehicle Type to Vehicles: (5) On the DA Import wizard', function () {

    // let authorizedToDrive = [
    //     '10,000lbs Van',
    //     'Custom Delivery Van',
    //     'Standard Parcel',
    //     'Standard Parcel Extra Large',
    //     'Standard Parcel Large',
    //     'Standard Parcel Small',
    // ]
    function findAssociate(associateName){
        cy.get('[href="/da-management"]').trigger('click')
        cy.wait(4000)
        cy.get('[data-cy="dalist-option"] > .el-radio-button__inner').click()
        cy.wait(2000)
        cy.get('[data-cy="search-in"]').type(associateName)
        cy.wait(3000)
        cy.get('table > tbody').find('.el-tooltip.icon-button').click( {multiple: true, force:true })
        cy.scrollTo(0, 0)
        cy.wait(1000)
    }

    function deleteUser(associateName) {
        findAssociate(associateName)
        cy.get('[data-cy="specific_da_dropdown"]').realMouseUp()
        cy.get('body').find('[data-cy="specific_da_delete_option"]').click({force:true})
        cy.get('[role="dialog"]:visible').find('input').type('delete')
        cy.get('[role="dialog"]:visible').find('button').contains('Confirm').click({force:true})
        cy.wait(1000)
    }

    it('Test Import DA Form', function () {
        cy.get('[href="/da-management"]').trigger('click')
        cy.wait(2000)
        cy.get('[data-cy="dalist-option"] > .el-radio-button__inner').click()
        cy.wait(2000)
        cy.get(".el-dropdown-menu").contains("Import Associates").click({force:true})
        cy.wait(5000)
        let __helper = new ComponentHelper('da-import-container')
        __helper.fillValue({type: 'file', selector: 'attach-file', value: 'Hera_Staff_Import_TemplateV2_filled.csv'});
        cy.wait(10000)
        cy.get('body').find('[data-cy="import-success"]').click()
        cy.wait(10000)
    })
    
    it('Delete users created by import file', function () {
        deleteUser('Dhonny')
        deleteUser('Melon')
        deleteUser('Sark')
        deleteUser('Cim')
    })

})