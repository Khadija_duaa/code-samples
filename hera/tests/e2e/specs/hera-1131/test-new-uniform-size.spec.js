import ComponentHelper from "../../support/Component.helper";
import MenuHelper from "../../support/Menu.helper";

describe('Verify new size of Uniform appears in list', function () {

    it('test new size in add uniform modal', function () {
        MenuHelper.Tab.goTo("Associate Management", '/da-management/dashboard')
        new ComponentHelper('damanagement-options').actionButton('dalist-option').scrollIntoView().click()
        let _helper = new ComponentHelper('da-list-content')
        cy.wait(9000)
        cy.wait(9000)
        cy.get('[data-cy=da_table_row_dropdown]').click({multiple: true,force:true})
        cy.get('.el-dropdown-menu').contains("View Associate").scrollIntoView().click({force:true})
        cy.wait(4000)
        cy.contains("UNIFORMS").scrollIntoView()
        cy.wait(4000)
        cy.contains("UNIFORMS").click()
        cy.get('[data-cy="da-uniform-add-btn"]').click()
        cy.wait(1000)
        cy.get(`[data-cy="uf-size-sel"]`).scrollIntoView().click()
        cy.get(".el-select-dropdown__item").contains("Short").should("exist").click()
        cy.get('[data-cy=uf-confirm-btn]').click()      
    })
})