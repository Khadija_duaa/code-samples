let text = `TEST \n TEST TEST \n TEST TEST TEST \n TEST TEST TEST TEST`
describe('HERA-1329', () => {
    context('Fix line break', () => {
        beforeEach(function() {
            cy.loginByUI(Cypress.env("cognito_username"), Cypress.env("cognito_password"));
        });
        
        it('Fix line break', function () {
            cy.wait(10000);
            cy.get('[data-cy="btn-kudos-issues"]').click()
            cy.wait(10000);
            cy.get('[data-cy="da-row-3dot-btn"]').first().click()
            cy.wait(10000);
            cy.get('[data-cy="view-issue-option"]').first().click({force: true})
            cy.wait(10000);
            cy.get('[data-cy="da-issue-detail-head-3dot-btn"]').click()
            cy.wait(10000);
            cy.get('[data-cy="btn-issue-edit"]').click()
            cy.wait(10000);
            cy.get('[data-cy="additional-info-in"]').focus().type(text)
            cy.wait(10000);
            cy.scrollTo('bottom')
            cy.wait(10000);
            cy.get('[data-cy="da-issue-create-btn"]').click()
            cy.wait(10000);
        })
    })
})