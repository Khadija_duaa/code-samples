import ComponentHelper from "../../support/Component.helper2";
import MenuHelper from "../../support/Menu.helper";

context("HERA-1135", function () {
 
    it("verify Dates in Associates issues", function () {
        MenuHelper.Tab.goTo('Performance & Coaching','/performance-and-coaching/performance')
        let __helper = new ComponentHelper('performance-coaching-container');
        cy.contains('Associate Kudos & Issues').scrollIntoView().click({force:true})
        cy.contains('View All Associate Issues').click({multiple:true, force: true})
        cy.wait(8000)
        __helper = new ComponentHelper('da-issue-list-card');
        __helper.getTableRow(0,"da-row-3dot-btn").scrollIntoView().click()
        cy.get('.el-dropdown-menu:visible').contains('View Associate Issue').scrollIntoView().click()
        cy.wait(4000)
    })
})