describe('If user deletes a Vehicle Type that is in use by Vehicles or DAs, warn the user', function () {


    it('Test delete Vehicle Type in Company Settings / Dropdown List', function () {
        cy.visit('/settings/drop-downs');
        cy.wait(5000)
        cy.scrollTo(0, '750px')
        cy.wait(2000)
        cy.get('.el-tag').contains('Element').click({force:true})
        cy.wait(2000)
        cy.get('[role="dialog"]:visible').find('button').contains('Delete').trigger('click')
        cy.wait(2000)
        cy.get('.el-message-box:visible').find('button').contains('No').trigger('click')
        cy.wait(2000)
        cy.get('[role="dialog"]:visible').find('button').contains('Cancel').trigger('click')

    })

})