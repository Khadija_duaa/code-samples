import ComponentHelper from "../../support/Component.helper2";

context("HERA-1086", function () {

    it("Should navigate to 'View Associate Issue' from 'Associate Kudos & Issues'", function () {
        cy.visit('/performance-and-coaching');
        cy.contains('Associate Kudos & Issues').scrollIntoView().click()
        cy.contains('View All Associate Issues').click({multiple:true, force: true})
        cy.wait(4000)
        const __helper = new ComponentHelper('da-issue-list-card');
        __helper.getTableRow(0,"da-row-3dot-btn").scrollIntoView().click()
        cy.get('.el-dropdown-menu:visible').contains('View Associate Issue').scrollIntoView().click()
    })
})