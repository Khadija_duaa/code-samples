import ComponentHelper from "../../../support/Component.helper";

describe('User Settings', function () {

    // COACHING TEXTS
    let parent_position = 5;
    let module_name = 'Coaching';
    let test_description = 'add a value that already exists in that dropdown list.';

    context('Company Settings', function () {

        beforeEach(() => {
            cy.loginByUI(
                Cypress.env('cognito_username'),
                Cypress.env('cognito_password')
            );
            cy.location('pathname').should('eq', '/performance-and-coaching/performance');
            cy.wait(10000);
            cy.hideCookieInfo();
            // cy.hideChatInfo()

            cy.menu_user_settings('Company Settings');
            cy.menu_company_settings(5);
        });

       // below test is related to driver-feedback , I investigate that current written test is not relavent so far.
       // thats why for time being skip this test.
        it.skip('mark as owner', function () {
            cy.location('pathname').should('eq', '/settings/driver-feedback');
            cy.get('[data-cy="users-container"]')
                .find('table')
                .find('tbody')
                .find('tr')
                .not(':contains("DSP Owner")')
                .first()
                .find('[role="button"].context-menu')
                .trigger('mouseenter')

            cy.get('body').find('.el-dropdown-menu:visible').find('li').contains('Mark as Owner').click({force:true})
        });

    })

})
