describe('HERA-1330', () => {
    context('Messenger header mobile', () => {
        beforeEach(function() {
            cy.viewport(390, 844)
            cy.loginByUI(Cypress.env("cognito_username"), Cypress.env("cognito_password"));
        });
        
        it('Mobile Messenger header', function () {
            cy.wait(10000);
            cy.visit({
                url: '/m/messenger/', method: 'GET',
            })
            cy.wait(10000);
            cy.get('[data-cy="lists-item-message"]').first().click()
        })
    })
})