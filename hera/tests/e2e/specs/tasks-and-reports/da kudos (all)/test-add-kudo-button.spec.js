import ComponentHelper from "../../../support/Component.helper2";
import MenuHelper from '../../../support/Menu.helper'
const {
    tasks_and_reports: {
        Kudo
    }
} = Cypress.env('e2e');

describe('Tasks & Reports', function () {
    context('Reports', function () {
        beforeEach(() => {
            MenuHelper.Tab.goTo("Tasks & Reports","/reports/")
            cy.hideCookieInfo();
            // cy.hideChatInfo()
        });
        
        it('DA Kudos (All) / Create DA Kudo', function () {
            MenuHelper.Top.selectFromCreate('Associate Kudo')
            cy.wait(3000)
            let __helper = new ComponentHelper('da-kudo-form')
            __helper.fillModalForm([
                {type: 'select', selector: 'da-list', value: Kudo.DA, ttw: 2000},
                {type: 'input', selector: 'date-of-occurrence', value: Kudo.DateOfOccurrence},
                {type: 'select', selector: 'kudo-type', value: Kudo.Type, ttw: 2000},
                {type: 'input', selector: 'notes', value: Kudo.Notes},
            ]);
            __helper.modalButtonContain('Create').scrollIntoView().click();
            __helper.alert('p').contains("Kudo created").should("be.visible")
        });
    })
})
