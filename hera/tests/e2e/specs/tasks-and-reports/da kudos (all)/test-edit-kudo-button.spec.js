import ComponentHelper from "../../../support/Component.helper2";
import MenuHelper from '../../../support/Menu.helper'
const {
    tasks_and_reports: {
        Kudo_ED
    }
} = Cypress.env('e2e');

describe('Tasks & Reports', function () {
    context('Reports', function () {
        beforeEach(() => {
            MenuHelper.Tab.goTo("Tasks & Reports","/reports/")
            cy.hideCookieInfo();
            // cy.hideChatInfo()
        });
        
        it('DA Kudos (All) / Edit DA Kudo', function () {
            let __helper = new ComponentHelper('performance-coaching', {x: 0, y: 500});
            __helper.container().find('.link').contains('Associate Kudos (All)').click()
            cy.location('pathname').should('eq', '/performance-and-coaching/da-kudos');
            cy.wait(3000)
            __helper = new ComponentHelper('da-kudo-table');
            __helper.getTableRow(0, 'edit-da-kudo').click({force: true})
            __helper.fillModalForm([
                {type: 'input', selector: 'date-of-occurrence', value: Kudo_ED.DateOfOccurrence},
                {type: 'input', selector: 'notes', value: Kudo_ED.Notes},
            ]);
            __helper.modalButtonContain('Update').scrollIntoView().click();
            cy.log('Waiting for result');
            cy.wait(4000);
        });

    })
})
