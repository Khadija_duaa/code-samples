import ComponentHelper from "../../../support/Component.helper2";
import MenuHelper from '../../../support/Menu.helper'
describe('Tasks & Reports', function () {
    context('Reports', function () {
        beforeEach(() => {
            MenuHelper.Tab.goTo("Tasks & Reports","/reports/")
            cy.hideCookieInfo();
            // cy.hideChatInfo()
        });

        it('DA Kudos (All) / Delete DA Kudo', function () {
            let __helper = new ComponentHelper('performance-coaching', {x: 0, y: 500});
            __helper.container().find('.link').contains('Associate Kudos (All)').click()
            cy.location('pathname').should('eq', '/performance-and-coaching/da-kudos');
            cy.wait(3000)
            __helper = new ComponentHelper('da-kudo-table');
            __helper.getTableRow(0, 'delete-da-kudo').click({force: true})
            __helper.modalButtonContain('Delete').scrollIntoView().click()
            __helper.alert('p').contains("Associate Kudo deleted").should("be.visible")
        });

    })
})
