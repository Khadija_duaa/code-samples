import MenuHelper from "../../support/Menu.helper"
import {Dashboard, ModalEditTask, ModalTask, PageTask, Alert} from "../../support/page-object-model.helper"

const dashboard = new Dashboard()
const Task = new PageTask()
const modalAddTask = new ModalTask()
const modalEditTask = new ModalEditTask()
const taskname = "test task name"
const tasknameEdited = "test edited"
const taskDescription = "test task description"

describe("Send notification to logged in users about upcoming/late tasks inside Hera.", function () {
    beforeEach(() => {
        MenuHelper.Tab.goTo("Tasks & Reports","/reports/")
        cy.hideCookieInfo();
    })

    it("Create/edit a task to be due within 5 days assigned to yourself", function () {
        cy.scrollTo('bottom').wait(500)
        cy.get("body .link").contains("Tasks (All)").scrollIntoView().click().wait(1000)
        Task.buttonAddTask().click({force: true})
        modalAddTask.verifyIfIsVisible()
        modalAddTask.inputAssignedTo().focus().click({force: true})
        modalAddTask.optionsSelect().find("li").contains("span", "Usama Khan").click({force: true})
        modalAddTask.inputDate().focus().click({force: true})
        modalAddTask.calendar().should('be.visible')
        const date = new Date()
        modalAddTask.calendar().contains("span", date.getDate() + 2).parent().click({force: true})
        modalAddTask.inputTaskName().focus().type(taskname)
        modalAddTask.inputCheckboxNotify().click({force: true})
        modalAddTask.inputTaskDescription().focus().type(taskDescription)
        modalAddTask.btnCreate().click({force: true})
        cy.wait(3000)
        Task.table().within(() => cy.contains(taskname).should("be.visible"))
        cy.log('created new Task, now we will edit this task ')
        Task.table().within(() => cy.contains(/test/g).parents("tr").find(".uil.uil-ellipsis-h").realHover())
        cy.wait(2000)
        cy.get(".el-dropdown-menu:visible").contains("Edit Task").scrollIntoView().click()
        cy.wait(5000)
        modalEditTask.inputTaskName().clear().type(tasknameEdited)
        modalEditTask.btnUpdate().click({force: true})
        Task.table().within(() => cy.contains(tasknameEdited).should("be.visible"))
        cy.log('Task Edited ')
        cy.wait(3000)
    })

    it("A new notification should be created.", function () {
        dashboard.BellIcon().realHover()
        cy.wait(3000)
        dashboard.ModalBellIcon().verifyNewNotification().should("be.visible")
        dashboard.ModalBellIcon().ViewAllNotification().click({force: true})
        cy.wait(3000)
    })

    it("Delete Task", function () {
        cy.scrollTo('bottom').wait(500)
        cy.get("body .link").contains("Tasks (All)").scrollIntoView().click()
        cy.wait(3000)
        Task.table().within(() => {
            return cy.contains(/test/g).parents("tr").find(".uil.uil-ellipsis-h").realHover()
        })
        cy.wait(2000)
        cy.get(".el-dropdown-menu:visible").contains("Delete Task").scrollIntoView().click()
        cy.wait(3000)
        cy.get('[role="dialog"] button').contains("Delete").click({force: true})
        Task.table().within(() => {
            return cy.get('tr').filter(':contains(/test/g)').should("have.length", 0)
        })
        cy.wait(3000)
    })
})