import ComponentHelper from "../../../support/Component.helper";
import MenuHelper from '../../../support/Menu.helper'
const {
    tasks_and_reports: {
        Injury_ED
    }
} = Cypress.env('e2e');

describe('Tasks & Reports', function () {
    context('Reports', function () {
        beforeEach(() => {
            MenuHelper.Tab.goTo("Tasks & Reports", "/reports/")
            cy.hideCookieInfo();
            // cy.hideChatInfo()
        });

        it('Injury Records (All) / Add Injury', function () {
            let __helper = new ComponentHelper('da-management', { x: 0, y: 1000 });
            __helper.container().find('.link').contains('Injury Records (All)').click({ force: true })
            cy.location('pathname').should('eq', '/da-management/injuries');
            cy.wait(3000)
            __helper = new ComponentHelper('injury-content')
            __helper.table().find('tr.table-row').eq(0).click()
            cy.wrap([
                { type: 'select', selector: 'da-list', value: Injury_ED.DeliveryAssociate },
                { type: 'input', selector: 'da-hire-date', value: Injury_ED.DAHireDate },
                { type: 'input', selector: 'address', value: Injury_ED.DAAddress_Street },
                { type: 'input', selector: 'city', value: Injury_ED.DAAddress_City },
                { type: 'input', selector: 'state', value: Injury_ED.DAAddress_State },
                { type: 'input', selector: 'zip', value: Injury_ED.DAAddress_Zip },
                { type: 'input', selector: 'date-of-birth', value: Injury_ED.DateOfBirth },
                { type: 'input', selector: 'completed-by-title', value: Injury_ED.CompletedByTitle },
                { type: 'input', selector: 'completed-by-phone', value: Injury_ED.CompletedByPhone },
            ]).each(formValue => {
                cy.scrollTo(0, 0).wait(500)
                new ComponentHelper('da-information-content').fillValue(formValue)
            })
            cy.scrollTo(0, 0).wait(500)
            cy.get('[data-cy="gender"]')
                .contains('.el-radio-button__inner', Injury_ED.Gender).click().wait(1500)

            cy.scrollTo(0, 500).wait(500)
            cy.wrap([
                { type: 'input', selector: 'injury-date', value: Injury_ED.InjuryDate },
                { type: 'input', selector: 'case-number', value: Injury_ED.CaseNumber },
                { type: 'input', selector: 'date-of-death', value: Injury_ED.DateOfDeath },
                { type: 'input', selector: 'physician', value: Injury_ED.Physician },
                { type: 'input', selector: 'physician-facility', value: Injury_ED.PhysicianFacility },
                { type: 'input', selector: 'facility-street', value: Injury_ED.FacilityAddress_Street },
                { type: 'input', selector: 'facility-city', value: Injury_ED.FacilityAddress_city },
                { type: 'input', selector: 'facility-state', value: Injury_ED.FacilityAddress_State },
                { type: 'input', selector: 'facility-zip', value: Injury_ED.FacilityAddress_Zip },
                { type: 'input', selector: 'time-shift-started', value: Injury_ED.TimeShiftStarted },
                { type: 'input', selector: 'injury-time', value: Injury_ED.InjuryTime },
                { type: 'check', selector: 'injury-time-is-unknown', value: Injury_ED.InjuryTimeIsUnknown },
            ]).each(formValue => {
                cy.scrollTo(0, 500).wait(500)
                new ComponentHelper('incident-information-content').fillValue(formValue)
            });

            cy.scrollTo(0, 750).wait(500)
            cy.wrap([
                { type: 'radio', selector: 'da-treated-in', value: Injury_ED.DATreatedInER },
                { type: 'radio', selector: 'da-hospitalized-overnight', value: Injury_ED.DAHospitalizedOvernight }
            ]).each(radioButton => {
                cy.get(`[data-cy="${radioButton.selector}"]`)
                    .contains('.el-radio-button__inner', radioButton.value).click().wait(1500)
            })
            new ComponentHelper('incident-information-content')
                .fillForm([
                    { type: 'input', selector: 'what-happened', value: Injury_ED.WhatHappened },
                    { type: 'input', selector: 'object-or-substance-harmed', value: Injury_ED.objectOrSubstance },
                    { type: 'input', selector: 'before-accident', value: Injury_ED.BeforeIncident },
                    { type: 'input', selector: 'injury-or-illness', value: Injury_ED.InjuryOrIllness },
                    { type: 'input', selector: 'internal-company-notes', value: Injury_ED.InternalCompanyNotes },
                ]);
            new ComponentHelper('new-injury-content').actionButtonContain('Update').scrollIntoView().click();
            cy.log('Waiting for result');
            cy.wait(4000);
        });
    })
})