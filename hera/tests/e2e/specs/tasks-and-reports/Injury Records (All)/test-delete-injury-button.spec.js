import ComponentHelper from "../../../support/Component.helper";
import MenuHelper from '../../../support/Menu.helper'
describe('Tasks & Reports', function () {
    context('Reports', function () {
        beforeEach(() => {
            MenuHelper.Tab.goTo("Tasks & Reports","/reports/")
            cy.hideCookieInfo();
            // cy.hideChatInfo()
        });

        it('Injury Records (All) / Add Injury', function () {
            let __helper = new ComponentHelper('da-management', {x: 0, y: 1000});
            __helper.container().find('.link').contains('Injury Records (All)').click({force: true})
            cy.location('pathname').should('eq', '/da-management/injuries');
            cy.wait(3000)
            __helper = new ComponentHelper('injury-content')
            __helper.table().find('tr.table-row').eq(0).click()
            cy.scrollTo(0, 0)
            __helper = new ComponentHelper('new-injury-content')
            __helper.actionButton('delete').click({force: true});
            __helper.modal().find('input[type="text"]').type('DELETE')
            __helper.modalButtonContain('Confirm').click({force: true})
            cy.log('Waiting for result');
            cy.wait(4000);
        });
    })
})