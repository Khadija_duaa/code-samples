import ComponentHelper from "../../../support/Component.helper";
import MenuHelper from '../../../support/Menu.helper'
const {
    tasks_and_reports: {
        Injury
    }
} = Cypress.env('e2e');

describe('Tasks & Reports', function () {
    context('Reports', function () {
        beforeEach(() => {
            MenuHelper.Tab.goTo("Tasks & Reports", "/reports/")
            cy.hideCookieInfo();
            // cy.hideChatInfo()
        });

        it('Injury Records (All) / Add Injury', function () {
            cy.scrollTo('bottom').wait(500)
            cy.get("body .link").contains('Injury Records (All)').click().wait(1000)
            cy.get('button > span').contains('Add Injury').click()
            cy.location('pathname').should('eq', '/da-management/injury/new');
            cy.wait(3000)

            cy.wrap([
                { type: 'select', selector: 'da-list', value: Injury.DeliveryAssociate },
                { type: 'input', selector: 'da-hire-date', value: Injury.DAHireDate },
                { type: 'input', selector: 'address', value: Injury.DAAddress_Street },
                { type: 'input', selector: 'city', value: Injury.DAAddress_City },
                { type: 'input', selector: 'state', value: Injury.DAAddress_State },
                { type: 'input', selector: 'zip', value: Injury.DAAddress_Zip },
                { type: 'input', selector: 'date-of-birth', value: Injury.DateOfBirth },
                { type: 'input', selector: 'completed-by-title', value: Injury.CompletedByTitle },
                { type: 'input', selector: 'completed-by-phone', value: Injury.CompletedByPhone },
            ]).each(formValue => {
                cy.scrollTo(0, 0).wait(500)
                new ComponentHelper('da-information-content').fillValue(formValue)
            })
            cy.scrollTo(0, 0).wait(500)
            cy.get('[data-cy="gender"]')
                .contains('.el-radio-button__inner', Injury.Gender).click().wait(1500)

            cy.wrap([
                { type: 'input', selector: 'injury-date', value: Injury.InjuryDate },
                { type: 'input', selector: 'case-number', value: Injury.CaseNumber },
                { type: 'input', selector: 'date-of-death', value: Injury.DateOfDeath },
                { type: 'input', selector: 'physician', value: Injury.Physician },
                { type: 'input', selector: 'physician-facility', value: Injury.PhysicianFacility },
                { type: 'input', selector: 'facility-street', value: Injury.FacilityAddress_Street },
                { type: 'input', selector: 'facility-city', value: Injury.FacilityAddress_city },
                { type: 'input', selector: 'facility-state', value: Injury.FacilityAddress_State },
                { type: 'input', selector: 'facility-zip', value: Injury.FacilityAddress_Zip },
                { type: 'input', selector: 'time-shift-started', value: Injury.TimeShiftStarted },
                { type: 'input', selector: 'injury-time', value: Injury.InjuryTime },
                { type: 'check', selector: 'injury-time-is-unknown', value: Injury.InjuryTimeIsUnknown },
            ]).each(formValue => {
                cy.scrollTo(0, 500).wait(500)
                new ComponentHelper('incident-information-content').fillValue(formValue)
            });

            cy.scrollTo(0, 750).wait(500)
            cy.wrap([
                { type: 'radio', selector: 'da-treated-in', value: Injury.DATreatedInER },
                { type: 'radio', selector: 'da-hospitalized-overnight', value: Injury.DAHospitalizedOvernight }
            ]).each(radioButton => {
                cy.get(`[data-cy="${radioButton.selector}"]`)
                    .contains('.el-radio-button__inner', radioButton.value).click().wait(1500)
            })
            new ComponentHelper('incident-information-content')
                .fillForm([
                    { type: 'input', selector: 'what-happened', value: Injury.WhatHappened },
                    { type: 'input', selector: 'object-or-substance-harmed', value: Injury.objectOrSubstance },
                    { type: 'input', selector: 'before-accident', value: Injury.BeforeIncident },
                    { type: 'input', selector: 'injury-or-illness', value: Injury.InjuryOrIllness },
                    { type: 'input', selector: 'internal-company-notes', value: Injury.InternalCompanyNotes },
                ]);
            new ComponentHelper('new-injury-content').actionButtonContain('Create').scrollIntoView().click();
            cy.log('Waiting for result');
            cy.wait(4000);
        });
    })
})