import MenuHelper from "../../../support/Menu.helper";
import ComponentHelper from "../../../support/Component.helper";
const {
    filters:{
        DeliveryAssociate_Filter
    },
    tasks_and_reports: {
        Counseling_ED
    }
} = Cypress.env('e2e');

describe('Tasks & Reports', function () {
    context('Reports', function () {
        beforeEach(() => {
            MenuHelper.Tab.goTo("Tasks & Reports","/reports/")
            cy.hideCookieInfo();
            // cy.hideChatInfo()
        });

        it('Counselings (All) / Edit Counseling', function () {
            let __helper = new ComponentHelper('performance-coaching', {x: 0, y: 1000});
            __helper.container().find('.link').contains('Counselings (All)').click({force: true})
            cy.location('pathname').should('eq', '/performance-and-coaching/counselings');
            cy.wait(4000)
            
            __helper = new ComponentHelper('counseling-container')
            __helper.fillValue({type: 'input', selector: 'filter', value: "Test Test"})
            __helper.getTableRow(0, 'view-counceling').click({force: true})

            __helper = new ComponentHelper('counseling-container')
            __helper.actionButton('edit').click({force: true})
            cy.wait(6000)
            cy.scrollTo(0, 0)
            new ComponentHelper('counseling-content')
                .fillForm([
                    {type: 'input', selector: 'date-of-occurrence', value: Counseling_ED.DateofOccurrence },
                    {type: 'select', selector: 'counseling-type', value: Counseling_ED.CounselingType },
                    {type: 'input', selector: 'problem', value: Counseling_ED.StatementOfTheproblem },
                    {type: 'input', selector: 'discussion', value: Counseling_ED.PriorDiscussionSummary },
                ]);
            cy.scrollTo(0, 725)
            new ComponentHelper('counseling-content')
                .fillForm([
                    {type: 'input', selector: 'summary', value: Counseling_ED.SummaryOfCorrectiveAction },
                    {type: 'input', selector: 'consequences', value: Counseling_ED.ConsequencesOfFailure },
                    // {type: 'input', selector: 'da-notes', value: Counseling_ED.DANotes },
                ]);
            cy.scrollTo('bottom')
            new ComponentHelper('counseling-content').actionButtonContain('Update').click({force: true});
            cy.log('Waiting for result');
            cy.wait(4000);
        });
    })
})