import MenuHelper from "../../../support/Menu.helper";
import ComponentHelper from "../../../support/Component.helper";
const {
    filters:{
        DeliveryAssociate_Filter
    }
} = Cypress.env('e2e');

describe('Tasks & Reports', function () {
    context('Reports', function () {
        beforeEach(() => {
            MenuHelper.Tab.goTo("Tasks & Reports","/reports/")
            cy.hideCookieInfo();
            // cy.hideChatInfo()
        });

        it('Counselings (All) / Delete Counseling', function () {
            let __helper = new ComponentHelper('performance-coaching', {x: 0, y: 1000});
            __helper.container().find('.link').contains('Counselings (All)').click({force: true})
            cy.location('pathname').should('eq', '/performance-and-coaching/counselings');
            cy.waitForLoaderToDisappear()
            
            __helper = new ComponentHelper('counseling-container')
            __helper.fillValue({type: 'input', selector: 'filter', value: "Test Test"})
            __helper.getTableRow(0, 'view-counceling').click({force: true})
            cy.wait(2000)
            
            __helper = new ComponentHelper('counseling-container')
            __helper.actionButton('delete').click({force: true})
            
            __helper.modal().find('input[type="text"]').type('delete')
            __helper.messageBoxConfirm()
            
            cy.log('Waiting for result');
            cy.wait(4000);
        });
    })
})