import MenuHelper from "../../../support/Menu.helper";
import ComponentHelper from "../../../support/Component.helper";
const {
    tasks_and_reports: {
        Counseling
    }
} = Cypress.env('e2e');

describe('Tasks & Reports', function () {
    context('Reports', function () {
        beforeEach(() => {
            MenuHelper.Tab.goTo("Tasks & Reports","/reports/")
            cy.hideCookieInfo();
            // cy.hideChatInfo()
        });

        it('Counseling (All) / Add Counseling', function () {
            MenuHelper.Top.selectFromCreate('Counseling')
            /* let __helper = new ComponentHelper('performance-coaching', {x: 0, y: 1000});
            __helper.container().find('.link').next().find('div').contains('Create Counseling').click({force: true}) */
            cy.location('pathname').should('eq', '/performance-and-coaching/counseling/new');
            cy.scrollTo(0, 0).wait(3000)
            new ComponentHelper('counseling-content')
                .fillForm([
                    // {type: 'select', selector: 'team-leader', value: Counseling.TeamLeader , ttw: 2000},
                    {type: 'input', selector: 'date-of-occurrence', value: Counseling.DateofOccurrence },
                    {type: 'select', selector: 'da-name', value: Counseling.DAName , ttw: 2000},
                    {type: 'select', selector: 'counseling-type', value: Counseling.CounselingType },
                    {type: 'input', selector: 'problem', value: Counseling.StatementOfTheproblem },
                    {type: 'input', selector: 'discussion', value: Counseling.PriorDiscussionSummary },
                ]);
            cy.scrollTo(0, 725)
            new ComponentHelper('counseling-content')
                .fillForm([
                    {type: 'input', selector: 'summary', value: Counseling.SummaryOfCorrectiveAction },
                    {type: 'input', selector: 'consequences', value: Counseling.ConsequencesOfFailure },
                    // {type: 'input', selector: 'da-notes', value: Counseling.DANotes },
                    {type: 'file', selector: 'counseling-img', value: Counseling.Image, ttw: 3000},
                ]);
            cy.scrollTo('bottom')
            new ComponentHelper('counseling-content').actionButtonContain('Create').realClick();
            cy.log('Waiting for result');
            cy.wait(4000);
        });
    })
})