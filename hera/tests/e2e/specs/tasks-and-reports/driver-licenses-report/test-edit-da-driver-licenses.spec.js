import MenuHelper from "../../../support/Menu.helper";
import FunctionHelper from "../../../support/Function.helper";

describe('Tasks & Reports', function () {
    context('Reports > Expiring Driver Licenses', function () {
        beforeEach(() => {
            MenuHelper.Tab.goTo("Tasks & Reports","/reports/")
            cy.hideCookieInfo();
            // cy.hideChatInfo()
        });

        it('Driver Licenses Report > Past Due Driver Licenses & Driver Licenses Due Soon (Within next 2 months)', function () {

            let _fh = new FunctionHelper()
            let date = (days) => _fh.getCurrentDate({
                format: 'mdY',
                separator: '/',
                addDays: days
            })

            cy.scrollTo(0, '85%').wait(1000)
            cy.get('[data-cy="da-management"] .link').contains('Expiring Driver Licenses').click()

            cy.location('pathname').should('eq', '/reports/da-driver-licenses');

            cy.get('[data-cy="da-driver-licenses-report"]').as('cards')
            cy.get('@cards').find('[data-cy="past-due-dl-list"] [data-cy="dl-row"]').as('past-due-list')

            cy.wrap([
                {value: date(-3)},
                {value: date(0)},
                {value: date(3)},
            ]).each((item, index) => {
                cy.get('@past-due-list').eq(index).then(row=>{
                    cy.wrap(row).find('[data-cy="dl-expiration-dpk"]').click().type(`{selectAll}{del}${item.value}`)
                    cy.get('.el-date-picker:visible').then(h=>h.hide())
                    cy.wrap(row).find('[data-cy="dl-save-btn"]').click()
                    cy.wait(4000)
                })
            })

            cy.get('@cards').find('[data-cy="dl-due-soon-list"] [data-cy="dl-row"]').as('due-soon-list')

            cy.wrap([
                {value: date(-3)},
                {value: date(0)},
                {value: date(3)},
            ]).each((item, index) => {
                cy.get('@due-soon-list').eq(index).then(row=>{
                    cy.wrap(row).find('[data-cy="dl-expiration-dpk"]').click().type(`{selectAll}{del}${item.value}`)
                    cy.get('.el-date-picker:visible').then(h=>h.hide())
                    cy.wrap(row).find('[data-cy="dl-save-btn"]').click()
                    cy.wait(4000)
                })
            })

            cy.wait(4000);
        });
    })
})
