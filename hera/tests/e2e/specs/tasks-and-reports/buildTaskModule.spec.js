import FunctionHelper from "../../support/Function.helper";
import ComponentHelper from "../../support/Component.helper2";
import MenuHelper from "../../support/Menu.helper";
const taskname = "Demo - Hera Test"
const tasknameEdited = "Demo - Hera Test edited"
const taskDescription = "Some description"

const {
    tasks_and_reports: {
        Task
    }
} = Cypress.env('e2e');


describe('Task Tests', function () {
    context('Build a task module for assigning simple tasks to users.', function () {
        beforeEach(function () {
            MenuHelper.Tab.goTo("Tasks & Reports","/reports/")
            cy.hideCookieInfo();
            // cy.hideChatInfo()
            
        })

        // it('Hit the "Add Task" Button. Opens a modal window where you can input basic task information', function () {
        //     let _helper = new ComponentHelper("tasks-and-reports")
        //     _helper.actionButton("add-task").should("be.visible").scrollIntoView().click()
        //     _helper.modal().should("be.visible").contains("Add Task")
        //     cy.wait(6000)
        // })

        // it('Hit the Cancel Button. Closes the modal Add Task from Windows', function () {
        //     let _helper = new ComponentHelper("tasks-and-reports")
        //     _helper.actionButton("add-task").should("be.visible").scrollIntoView().click()
        //     _helper.modal().should("be.visible").contains("Add Task")
        //     cy.wait(6000)
        //     _helper.modalButtonContain("Cancel").scrollIntoView().click()
        //     cy.wait(3000)
        // })

        // it('Hit "Create" without required fields. Error dialog will display', function () {
        //     let _helper = new ComponentHelper("tasks-and-reports")
        //     _helper.actionButton("add-task").should("be.visible").scrollIntoView().click()
        //     _helper.modal().should("be.visible").contains("Add Task")
        //     cy.wait(6000)
        //     _helper.modalButtonContain("Create").scrollIntoView().click()
        //     _helper.alert(":contains('Error')").should("be.visible")
        //     cy.wait(3000)
        // })

        it(`Fill in all information and hit "Create". The modal will close and the task will be made ( it will now show up on the list)`, function () {
            let _helper = new ComponentHelper("tasks-and-reports")
            _helper.actionButton("add-task").should("be.visible").scrollIntoView().click()
            _helper.modal().should("be.visible").contains("Add Task")

            _helper.fillModalForm([
                {selector:"due-date", type: "input", value: Task.DueDate},
                {selector:"assigned-to", type: "select", value: Task.AssignedTo},
                {selector:"status", type: "select", value: Task.Status},
                {selector:"taskname", type: "input", value: Task.TaskName},
                {selector:"notify-on-creation", type: "check", value: Task.NotifyAssignedTo},
                {selector:"date-reminder-chk", type: "check", value: Task.RemindAssignedTo},
                {selector:"date-reminder-dpk", type: "input", value: Task.RemindAssignedToDate},
                {selector:"description", type: "input", value: Task.Description},

            ])

            _helper.modalButtonContain("Create").scrollIntoView().click()
            
            cy.get("table > tbody:visible").contains(taskname).should("be.visible")
            cy.wait(3000)
        })

        it('Will open the modal again to edit an existing task.', function () {
            cy.scrollTo('bottom').wait(500)
            cy.get("body .link").contains("Tasks (All)").scrollIntoView().click()
            let _helper = new ComponentHelper("all-tasks")
            let ops = {
                EditTask: "Edit Task",
                CompleteTask: "Complete Task",
                DeleteTask: "Delete Task"
            }
            _helper.table().find(`tr:contains('${taskname}') [data-cy="3dot-button"]:visible`).scrollIntoView().click()
            cy.get(".el-dropdown-menu:visible").contains(ops.EditTask).scrollIntoView().click()
            cy.get(".el-dropdown-menu:visible").invoke("hide")

            let date_mdY = new FunctionHelper().getCurrentDate({
                format: 'mdY',
                separator: '/',
                addDays: 4
            })
            
            _helper.fillModalForm([
                {selector:"due-date", type: "input", value: date_mdY},
                {selector:"taskname", type: "input", value: tasknameEdited},
                {selector:"description", type: "input", value: taskDescription},

            ])

            _helper.modalButtonContain("Update").scrollIntoView().click()
            
            _helper.table().contains(tasknameEdited).should("be.visible")
            cy.wait(3000)
        })

        it(`It will complete the task. They will no longer show on other screens and the status will become "Complete".`, function () {
            cy.scrollTo('bottom').wait(500)
            cy.get("body .link").contains("Tasks (All)").scrollIntoView().click()
            let _helper = new ComponentHelper("all-tasks")
            let ops = {
                EditTask: "Edit Task",
                CompleteTask: "Complete Task",
                DeleteTask: "Delete Task"
            }
            _helper.table().find(`tr:contains('${tasknameEdited}') [data-cy="3dot-button"]:visible`).scrollIntoView().click()
            cy.get(".el-dropdown-menu:visible").contains(ops.CompleteTask).scrollIntoView().click()
            cy.get(".el-dropdown-menu:visible").invoke("hide")
            cy.wait(5000)
            cy.scrollTo('bottom').wait(500)
            cy.get("button > span").contains("Load More").click()
            _helper.table().find(`tr:contains('${tasknameEdited}') :contains('Complete')`).should('be.visible')
        })

        it("Delete Task", function () {
            cy.scrollTo('bottom').wait(500)
            cy.get("body .link").contains("Tasks (All)").scrollIntoView().click()
            let _helper = new ComponentHelper("all-tasks")
            let ops = {
                EditTask: "Edit Task",
                CompleteTask: "Complete Task",
                DeleteTask: "Delete Task"
            }
            cy.scrollTo('bottom').wait(500)
            cy.get("button > span").contains("Load More").click()
            _helper.table().find(`tr:contains('${tasknameEdited}') [data-cy="3dot-button"]:visible`).scrollIntoView().click()
            cy.get(".el-dropdown-menu:visible").contains(ops.DeleteTask).scrollIntoView().click()
            cy.get(".el-dropdown-menu:visible").invoke("hide")
            cy.wait(3000)
            _helper.modalButtonContain('Delete').scrollIntoView().click()
            _helper.table().find(`tr:contains('${tasknameEdited}')`).should("have.length", 0)
            cy.wait(3000)
        })

    })
})