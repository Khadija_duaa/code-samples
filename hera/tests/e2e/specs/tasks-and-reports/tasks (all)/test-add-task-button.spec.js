import ComponentHelper from "../../../support/Component.helper2";
import MenuHelper from "../../../support/Menu.helper";
const {
    tasks_and_reports: {
        Task
    }
} = Cypress.env('e2e');

describe('Tasks & Reports', function () {

    context('Task Dashboard', function () {
        beforeEach(() => {
            MenuHelper.Tab.goTo("Tasks & Reports","/reports/")
            cy.hideCookieInfo();
            // cy.hideChatInfo()
        });

        it('Fill task and create', function () {
            let __helper = new ComponentHelper("tasks-and-reports")
            __helper.actionButton("add-task").should("be.visible").scrollIntoView().click()
            __helper.modal().should("be.visible").contains("Add Task")
            __helper.fillModalForm([
                {selector:"due-date", type: "input", value: Task.DueDate},
                {selector:"assigned-to", type: "select", value: "Usama Khan"},
                {selector:"status", type: "select", value: Task.Status},
                {selector:"taskname", type: "input", value: Task.TaskName},
                {selector:"notify-on-creation", type: "check", value: Task.NotifyAssignedTo},
                {selector:"date-reminder-chk", type: "check", value: Task.RemindAssignedTo},
                {selector:"date-reminder-dpk", type: "input", value: Task.RemindAssignedToDate},
                {selector:"description", type: "input", value: Task.Description},
            ]);
            __helper.modalButtonContain('Create').scrollIntoView().click()
            cy.log('Waiting for result');
            __helper.alert('p').contains("Task created successfully").should("be.visible")
        });

        it('Fill task and cancel', function () {
            let __helper = new ComponentHelper('tasks-and-reports');
            __helper.actionButton('add-task').scrollIntoView().click()
            cy.location('pathname').should('eq', '/reports/tasks');
            __helper.fillModalForm([
                {selector:"due-date", type: "input", value: Task.DueDate},
                {selector:"assigned-to", type: "select", value: "Usama Khan"},
                {selector:"status", type: "select", value: Task.Status},
                {selector:"taskname", type: "input", value: Task.TaskName},
                {selector:"notify-on-creation", type: "check", value: Task.NotifyAssignedTo},
                {selector:"date-reminder-chk", type: "check", value: Task.RemindAssignedTo},
                {selector:"date-reminder-dpk", type: "input", value: Task.RemindAssignedToDate},
                {selector:"description", type: "input", value: Task.Description},
            ]);
            cy.wait(1000);
            __helper.modalButtonContain('Cancel').scrollIntoView().click()
        });
    })
})
