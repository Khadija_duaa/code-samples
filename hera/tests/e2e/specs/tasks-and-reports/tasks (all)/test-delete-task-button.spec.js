import ComponentHelper from "../../../support/Component.helper2";
import MenuHelper from "../../../support/Menu.helper";
describe('Tasks & Reports', function () {

    context('Task Dashboard', function () {
        beforeEach(() => {
            MenuHelper.Tab.goTo("Tasks & Reports","/reports/")
            cy.hideCookieInfo();
            // cy.hideChatInfo()
        });

        it('Select task and delete', function () {
            cy.wait(4000)
            cy.scrollTo('bottom');
            cy.get('.link')
                .contains("Tasks (All)")
                .click({force: true})
            cy.location('pathname').should('eq', '/reports/tasks');
            cy.wait(2000)
            let __helper = new ComponentHelper('all-tasks');
            __helper.select(`[data-cy='3dot-button']:visible:eq(0) > .context-menu`,"Delete Task")
            new ComponentHelper('all-tasks').modalButtonContain('Delete').click({force: true})
            cy.log('Waiting for result');
            cy.wait(4000);
        });
    })
})
