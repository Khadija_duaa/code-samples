import ComponentHelper from "../../../support/Component.helper2";
import MenuHelper from "../../../support/Menu.helper";
describe('Tasks & Reports', function () {

    context('Task Dashboard', function () {
        beforeEach(() => {
            MenuHelper.Tab.goTo("Tasks & Reports","/reports/")
            cy.hideCookieInfo();
            // cy.hideChatInfo()
        });

        it('Select task and change to complete', function () {
            cy.scrollTo('bottom').wait(500)
            cy.get('.link')
                .contains("Tasks (All)")
                .click()
            cy.location('pathname').should('eq', '/reports/tasks');
            cy.wait(2000)
            let __helper = new ComponentHelper('all-tasks');
            
            __helper.select(`[data-cy='3dot-button']:visible:eq(0) > .context-menu`,"Complete Task")
            cy.get(".el-dropdown-menu:visible").invoke("hide")
            cy.log('Waiting for result');
            cy.wait(4000);
        });
    })
})
