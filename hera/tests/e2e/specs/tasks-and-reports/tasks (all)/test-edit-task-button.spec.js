import ComponentHelper from "../../../support/Component.helper2";
import MenuHelper from "../../../support/Menu.helper";
const {
    tasks_and_reports: {
        Task_ED
    }
} = Cypress.env('e2e');

describe('Tasks & Reports', function () {

    context('Task Dashboard', function () {
        beforeEach(() => {
            MenuHelper.Tab.goTo("Tasks & Reports","/reports/")
            cy.hideCookieInfo();
            // cy.hideChatInfo()
        });

        it('Fill task and update', function () {
            cy.get('.dashboard-card-header')
                .eq(1)
                .parent()
                .find('.ellipsis')
                .eq(0)
                .click({force: true})
            let __helper = new ComponentHelper('tasks-and-reports');
            __helper.fillModalForm([
                {selector:"due-date", type: "input", value: Task_ED.DueDate},
                {selector:"assigned-to", type: "select", value: "Usama Khan"},
                {selector:"status", type: "select", value: Task_ED.Status},
                {selector:"taskname", type: "input", value: Task_ED.TaskName},
                // {selector:"notify-on-creation", type: "check", value: Task_ED.NotifyAssignedTo},
                {selector:"date-reminder-chk", type: "check", value: Task_ED.RemindAssignedTo},
                {selector:"date-reminder-dpk", type: "input", value: Task_ED.RemindAssignedToDate},
                {selector:"description", type: "input", value: Task_ED.Description},
            ]);
            __helper.modalButtonContain('Update').scrollIntoView().click()
            cy.log('Waiting for result');
            cy.wait(4000);
        });

        it('Fill task and cancel', function () {
            cy.get('.dashboard-card-header')
                .eq(1)
                .parent()
                .find('.ellipsis')
                .eq(0)
                .click({force: true})
            let __helper = new ComponentHelper('tasks-and-reports');
            __helper.fillModalForm([
                {selector:"due-date", type: "input", value: Task_ED.DueDate},
                {selector:"assigned-to", type: "select", value: "Usama Khan"},
                {selector:"status", type: "select", value: Task_ED.Status},
                {selector:"taskname", type: "input", value: Task_ED.TaskName},
                // {selector:"notify-on-creation", type: "check", value: Task_ED.NotifyAssignedTo},
                {selector:"date-reminder-chk", type: "check", value: Task_ED.RemindAssignedTo},
                {selector:"date-reminder-dpk", type: "input", value: Task_ED.RemindAssignedToDate},
                {selector:"description", type: "input", value: Task_ED.Description},
            ]);
            __helper.modalButtonContain('Cancel').scrollIntoView().click()
            cy.log('Task creation cancelled');
        });
    })
})
