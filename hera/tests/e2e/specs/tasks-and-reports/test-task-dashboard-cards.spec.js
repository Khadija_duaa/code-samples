import MenuHelper from "../../support/Menu.helper";


describe("DA Management > Dashboard  ",function(){

    context("Card  Tasks",function(){
       
      beforeEach(function () {
      MenuHelper.Tab.goTo("Associate Management", '/da-management/dashboard')
      cy.hideCookieInfo();
    })

    // This card doesn't exist on UI, as of Oct 13, 2023, so skipping this task
    it.skip('Card let me see task by tabs  and link to go to the page Task',function(){
        cy.wait(5000)
        cy.get('[data-cy="card_tasks"]').find('[data-cy="card_title"]').contains("Tasks").should("be.visible")


        let tabsNames = [
          'Assigned To Me',
          'Assigned By Me',
          'Other Tasks'
        ]

          cy.get('[data-cy="card_tasks"]')
            .find('.el-tabs__item')
            .each(function($el,index,$list){
                cy.get($el).contains(tabsNames[index]).should("be.visible")
            })

        //validate the link to  take us  DA Performance Report Page with path route name /coaching/da-performance-report

            cy.get('[data-cy="card_tasks"]').find('[data-cy="card_link-Tasks"]').click({force:true})
            cy.wait(3000)
            cy.location("pathname").should("eq","/tasks")

       })


  
    })

})