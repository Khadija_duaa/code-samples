let text = "Option Test 1"

describe('HERA-1076', () => {
    context('View route type options', () => {
        
        it('View route type options', function () {
            cy.get('[data-cy="user-info-dropdown"]').click()
            cy.wait(4000);
            cy.get('[data-cy="company-settings-option"]').click()
            cy.wait(4000);
            cy.get('[data-cy="custom-lists-tab"]').click()
            cy.wait(4000);
            cy.get('[data-cy="search-in"]').type('Incident Type')
            cy.get('[data-cy="three_dot_button_row"]').click({multiple:true, force:true})
            cy.wait(4000);
            cy.get('.el-dropdown-menu').contains('Edit this Custom List').scrollIntoView().click({force:true})
            cy.wait(4000);
           cy.get('[data-cy=new-option-btn]').click()
            cy.get('[data-cy="new-option-input"]').type(text)
            cy.wait(4000);
            cy.get('[data-cy="done-btn"]').click()
            cy.wait(4000);
            cy.get('[data-cy="drawer-save-btn"]').click()
            cy.wait(4000);
        })
    })
})