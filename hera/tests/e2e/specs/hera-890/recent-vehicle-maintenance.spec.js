import MenuHelper from "../../support/Menu.helper";

describe('HERA 890', () => {
  context('Go to Recent Vehicle Maitenance Records', () => {

    it('Go to Vehicle Dashboard and Tabs Vehicle Maintenance', function () {
      MenuHelper.Tab.goTo("Vehicle Management", '/vehicles/dashboard');
      cy.get( '[data-cy="vehicle-management-options"]' )
        .contains( '[data-cy="dashboard-option"]', 'Dashboard' ).click()
      
      cy.wait( 5000 );

      cy.get( '[data-cy="tab-title"]' )
        .contains( 'span', 'In Progress' ).click();

      cy.wait( 5000 );

      cy.get( '[data-cy="tab-title"]' )
        .contains( 'span', 'Waiting for Pickup' ).click();

      cy.wait( 5000 );

      cy.get( '[data-cy="tab-title"]' )
        .contains( 'span', 'Completed' ).click();
    });
  })
})