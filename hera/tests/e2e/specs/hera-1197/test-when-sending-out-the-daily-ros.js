import MenuHelper from "../../support/Menu.helper"
import ComponentHelper from "../../support/Component.helper2"
import FunctionHelper from "../../support/Function.helper";

const {
    daily_rostering: {
        daily_roster_table: {
            DailyRoster,
            ReplaceWithStandByDA
        }
    }
} = Cypress.env('e2e');

const today = new FunctionHelper().getCurrentDate({
    format: 'Ymd',
    separator: '-'
});

describe('Remove time validation when send rostered messages', function () {

    it('Test Add Driver Issue with counseling and check createdFrom field', function () {
        MenuHelper.Tab.goTo("Daily Rostering", '/daily-rostering/' + today +'/roster')

        let __helper = new ComponentHelper('daily-roster-table');
        __helper.actionButton('single-da').scrollIntoView().click();
        __helper.fillModalForm([
            {type: 'select', selector: 'staffId', value: DailyRoster.DeliveryAssociate},
            {type: 'select', selector: 'vehicleId', value: DailyRoster.Vehicle},
        ]);

        __helper.fillModalForm([
            {type: 'input', selector: 'route', value: DailyRoster.Route},
            {type: 'input', selector: 'staging', value: DailyRoster.Staging},
            {type: 'select', selector: 'parkingSpace', value: DailyRoster.ParkingSpace},
            {type: 'select', selector: 'status', value: 'On Time'},
            {type: 'time', selector: 'time', value: DailyRoster.Time},
            {type: 'input', selector: 'notes', value: DailyRoster.Notes},
        ]);
        __helper.modalButton('confirm').scrollIntoView().click();
        cy.log('Waiting for result');
        cy.wait(1000);

        MenuHelper.Tab.goTo("Performance & Coaching", '/performance-and-coaching/performance')

        __helper = new ComponentHelper('performance-coaching-container');
        cy.contains('Counselings').scrollIntoView().click()
        cy.wait(4000)
        __helper = new ComponentHelper('counseling-container');
    });

    it('Test Add DA Form', function () {
        MenuHelper.Tab.goTo("Daily Rostering", '/daily-rostering/' + today + '/roster')

        let __helper = new ComponentHelper('daily-roster-table');

        cy.get('body').find('[data-cy="status_3dot_button1"]').click({multiple:true, force:true});
        cy.get('body').find('[data-cy="status_3dot_button1"]').find('li').contains('Edit Associate Assignment').click({force: true})
        cy.get('body').find('[role="dialog"]:visible').find('button').contains('Delete').click()
        cy.get('body').find('.el-message-box:visible').find('button').contains('Delete').click()
        cy.wait(5000)
    })
})