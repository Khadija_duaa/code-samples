import ComponentHelper from "../../support/Component.helper"

describe('Add Vehicle Type to Vehicles: (8) On the Daily Roster', function () {


    function currentDate() {
        let today = new Date();
        return today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    }

    function findVehicle(vehicleName) {
        cy.get('[href="/vehicles/dashboard"]').click({force:true})
        cy.wait(4000)
        cy.get('[data-cy="vehicle-list"] > .el-radio-button__inner').click()
        cy.wait(2000)
        cy.get('[data-cy="search-in"]').type(vehicleName)
        cy.get('.el-dropdown-menu').contains(`View Vehicle`).click({force:true})
        cy.wait(1000)
    }
    
    function findAssociate(associateName){
        cy.get('[href="/da-management"]').trigger('click')
        cy.wait(4000)
        cy.get('[data-cy="dalist-option"] > .el-radio-button__inner').click()
        cy.wait(2000)
        cy.get('[data-cy="search-in"]').type(associateName)
        cy.wait(3000)
        cy.get('table > tbody').find('.el-tooltip.icon-button').click( {multiple: true, force:true })
        cy.scrollTo(0, 0)
        cy.wait(1000)
    }

    function deleteVehicle(vehicleName) {
        findVehicle(vehicleName)
        cy.get('body').find('[data-cy="vd-edit-btn"]').click()
        cy.get('[role="dialog"]:visible').find('button').contains('Delete').click({force:true})
        cy.get('.el-message-box:visible').find('button').contains('Delete').click({force:true})
        cy.wait(1000)
    }

    function deleteUser(associateName) {
        findAssociate(associateName)
        cy.get('[data-cy="specific_da_dropdown"]').realMouseUp()
        cy.get('body').find('[data-cy="specific_da_delete_option"]').click({force:true})
        cy.get('[role="dialog"]:visible').find('input').type('delete')
        cy.get('[role="dialog"]:visible').find('button').contains('Confirm').click({force:true})
        cy.wait(1000)
    }

    it('Add a new Associate', function () {
        cy.visit('/da-management/new');
        cy.wait(5000)
        cy.get('[data-cy="add-da-form"]').find('[data-cy="da-status-sel"]').click()
        cy.get('.el-select-dropdown:visible').find('li').contains('Active').click({force: true})
        cy.get('[data-cy="add-da-form"]').find('[data-cy="da-first-name-in"]').type('Yonnie')
        cy.get('[data-cy="add-da-form"]').find('[data-cy="da-last-name-in"]').type('Den')
        cy.get('[data-cy="add-da-form"]').find('[data-cy="da-email-in"]').type('YonnieDen@gmail.com')
        cy.get('[data-cy="add-da-form"]').find('[data-cy="da-authorized-to-drive-in"]').click()
        cy.get('.el-select-dropdown:visible').find('li').contains('Standard Parcel').click({force: true})
        cy.get('[data-cy="staffnew_create_btn"]').trigger('click')
        cy.wait(1000)
    })

    it('Add a new Vehicle', function () {
        cy.visit('/vehicles/new');
        cy.wait(5000)
        cy.get('[data-cy="vehicle_form"]').find('[data-cy="name_in"]').type('Dodge Viper')
        cy.get('[data-cy="vehicle_form"]').find('[data-cy="vehicleType"]').click()
        cy.get('.el-select-dropdown:visible').find('li').contains('Standard Parcel').click({force: true})
        cy.get('[data-cy="create_btn"]').click({force: true})
        cy.wait(1000)
    })

    it('Add a Daily Roster route', function () {
        cy.visit('/daily-rostering/' + currentDate() + '/roster');
        cy.wait(5000)
        cy.get('[data-cy="single-da"]').click()
        cy.get('[role="dialog"]:visible').find('[data-cy="staffId"]').click()
        cy.get('.el-select-dropdown:visible').find('li').contains('Yonnie Den').click({force: true})
        cy.get('[role="dialog"]:visible').find('[data-cy="vehicleId"]').click()
        cy.get('.el-select-dropdown:visible').find('li').contains('Dodge Viper').click({force: true})
        cy.get('[role="dialog"]:visible').find('[data-cy="confirm"]').click()
        cy.wait(1000)
    })

    it('Edit Vehicle changing Vehicle-Type', function () {
        findVehicle('Dodge Viper')
        cy.get('body').find('[data-cy="vd-edit-btn"]').click({force: true})
        cy.get('body').find('[role="dialog"]:visible').find('[data-cy="vehicleType"]').click({force: true})
        cy.get('.el-select-dropdown:visible').find('li').contains('Custom Delivery Van').click({force: true})
        cy.wait(2000)
        cy.get('[role="dialog"]:visible').find('button').contains('Update').click({force: true})
        cy.wait(1000)
    })

    it('Delete Daily Roster, user and Vehicle created', function () {
        cy.visit('/daily-rostering/' + currentDate() + '/roster');
        cy.wait(5000)
        cy.get('body').find('[data-cy="status_3dot_button1"]').click({ multiple: true, force: true });
        cy.get('body').find('[data-cy="status_3dot_button1"]').find('li').contains('Edit Associate Assignment').click({force: true})
        cy.get('body').find('[role="dialog"]:visible').find('button').contains('Delete').click()
        cy.get('body').find('.el-message-box:visible').find('button').contains('Delete').click()
        cy.wait(5000)
    }) 

})