import MenuHelper from "../../support/Menu.helper";
import ComponentHelper from "../../support/Component.helper";
import ComponentHelper2 from "../../support/Component.helper2";

describe('Verify if error message not appears under Intercom banner', function () {


    it('Test toaster message in Counseling', function () {
        MenuHelper.Top.selectFromCreate("Counseling");
        let __helper = new ComponentHelper('counseling-content');
            __helper.actionButtonContain('SELECT EXISTING ASSOCIATE ISSUES').click({force: true});
    })

    it('Test toaster message in Weekly Performance Data', function () {
        MenuHelper.Top.selectFromImport("Weekly Performance Data");
    
        const __helper = new ComponentHelper2('weekly-data-import');
        
        __helper.fillValue({ type: 'file', selector: 'attach-file', value: 'US_PRLO_DSC4_Week18_2022_DSPScorecard.pdf' });
    
        __helper.actionButtonContain('Upload All').scrollIntoView().click();
    
        cy.log('Waiting for result');
    });

})