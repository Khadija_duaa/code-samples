import ComponentHelper from "../../../support/Component.helper";
import MenuHelper from "../../../support/Menu.helper";
const {
    filters:{
        DeliveryAssociate_Filter
    },
    da_management: {
        bulk_updates: {
            BulkUpdates_DA
        }
    }
} = Cypress.env('e2e');

describe('Daily Roster', function () {

    context('Bulk updates', function () {
        beforeEach(() => {
            MenuHelper.Tab.goTo("Associate Management", '/da-management/dashboard', 'dashboard-tab')
        });

        it('Test edit table row', function () {
            let __helper = new ComponentHelper('damanagement-options')
            __helper.actionButton('bulk-updates-tab').click({force: true});
            cy.wait(3000)
            __helper = new ComponentHelper('bulk-updates-content')

            __helper.table().find(`tr:contains('${DeliveryAssociate_Filter}')`).as("row")


            cy.get("@row").invoke("index").then(ROW_INDEX=>{
                __helper.fillTable(ROW_INDEX+1,[
                    {selector: "tbu-authorizedLBS-sw", type:"text", value: BulkUpdates_DA.AuthorizedOver10kLBS, ttw: 2000},
                    {selector: "tbu-primary-vehicle-sel", type:"text", value: BulkUpdates_DA.PrimaryVehicle},
                    {selector: "tbu-secondary-vehicle-sel", type:"text", value: BulkUpdates_DA.SecondaryVehicle},
                    {selector: "tbu-tertiary-vehicle-sel", type:"text", value: BulkUpdates_DA.TertiaryVehicle},
                    {selector: "supervised-by", type:"text", value: BulkUpdates_DA.SupervisedBy},
                ])
            }).wait(2000)

            cy.log('[test-change-status-button.spec.js](./#/tests/integration\\da-management\\da-list\\test-change-status-button.spec.js)').then(()=>
                assert(!0,'**Row edited successfully**, go to test-change-status-button.spec.js to test Change status.')
            )

        });
    })
})
