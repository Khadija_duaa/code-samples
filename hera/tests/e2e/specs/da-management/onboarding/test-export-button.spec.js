import ComponentHelper from "../../../support/Component.helper";
import FunctionHelper from "../../../support/Function.helper";
import MenuHelper from "../../../support/Menu.helper";

describe('Associate Management', function () {

    context('Onboarding', function () {
        beforeEach(() => {
            MenuHelper.Tab.goTo("Associate Management", '/da-management/dashboard', 'dashboard-tab');
        });

        it('Test table export functionality', function () {
            let __helper = new ComponentHelper('damanagement-options')
            __helper.actionButton('onboarding-tab').click({ force: true });
            __helper = new ComponentHelper('onboarding-content')

            cy.waitForLoaderToDisappear()

            cy.get(".el-dropdown-menu").contains('Export Associates').click({ force: true });
            __helper.modalButtonContain('Export').click({ force: true });
            let ext = '.csv'
            let today = new FunctionHelper().getCurrentDate({
                format: 'mdY',
                separator: '_'
            });
            let filename = 'da-export-Active-' + today + ext
            cy.readFile('cypress/downloads/' + filename).then((content) => {
                assert(content, '**EXPORT SUCCESS**')
            });
        });
    })
})
