import ComponentHelper from "../../../support/Component.helper";
import MenuHelper from "../../../support/Menu.helper";

describe('Associate Management', function () {

    context('Onboarding', function () {
        beforeEach(() => {
            MenuHelper.Tab.goTo("Associate Management", '/da-management/dashboard', 'dashboard-tab');
        });

        it('Test table export functionality', function () {
            let __helper = new ComponentHelper('damanagement-options')
            __helper.actionButton('onboarding-tab').click({ force: true });
            __helper = new ComponentHelper('onboarding-content')

            cy.waitForLoaderToDisappear()

            cy.get(".el-dropdown-menu").contains('Import Associates').click({ force: true });
            cy.location('pathname').should('eq', '/da-management/import');

            __helper = new ComponentHelper('da-import-container')
            __helper.fillValue({
                type: 'file',
                selector: 'attach-file',
                value: 'da-export-Active-example.csv',
                ttw: 2000
            });
            __helper.actionButton('import-cancel').click({ force: true })
            __helper.messageBoxConfirm()
        });
    })
})
