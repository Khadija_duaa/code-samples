import ComponentHelper from "../../../../support/Component.helper";
import MenuHelper from "../../../../support/Menu.helper";

const {
    filters: {
        DeliveryAssociate_Filter
    },
    da_management: {
        da_list: {
            view_da: {
                Onboarding_DA,
                drug_tests: {
                    DrugTests
                },
                medical_card: {
                    MedicalCards
                }
            }
        }
    }
} = Cypress.env('e2e');
describe('Associate Management', () => {
    context('Onboarding Card Changes in Associate Record', () => {
        beforeEach(() => {
            MenuHelper.Tab.goTo("Associate Management", '/da-management/dashboard', 'dashboard-tab')
        });

        it('Vehicles Over 10,000 LBS: Fill form type', () => {
            new ComponentHelper('damanagement-options').actionButton('onboarding-tab').click({ force: true })
            new ComponentHelper('onboarding-content').getTableRow(2, "view-da").click({ force: true })
            const _helper = new ComponentHelper("staff-detail-container");

            cy.waitForLoaderToDisappear()

            _helper.container()
                .find('[data-cy="da-will-be-driving"] > [role="button"]')
                .click({ force: true })

            cy.get('body').find('ul.el-dropdown-menu:visible')
                .find('li.el-dropdown-menu__item').contains('Vehicles Over 10,000 LBS')
                .click({ force: true })

            cy.scrollTo(0, 150)


            const OnBoardingData = [
                { ...Onboarding_DA.Offer, value: "Signed" },
                { ...Onboarding_DA.AmazonProfile, value: "Profile Completed" },
                { ...Onboarding_DA.BackgroundCheck, value: "Amazon Cleared" },
                { ...Onboarding_DA._10kLBSApplication, value: "Completed" },
                { ...Onboarding_DA.TrainingVideos, value: "Completed" },
                { ...Onboarding_DA.DrugTest, value: "Completed: Cleared" },
                { ...Onboarding_DA.AddedtoPayroll, value: "Completed" },
                { ...Onboarding_DA.I9Verification, value: "Completed" },
                { ...Onboarding_DA.DSPOrientation, value: "Completed" },
                { ...Onboarding_DA._10kLBSOnlineTraining, value: "Completed" },
                { ...Onboarding_DA.MedicalCard, value: "Issued" },
                { ...Onboarding_DA.InPersonAmazonTraining, value: "Completed" },
                { ...Onboarding_DA._10kLBSRoadTesting, value: "Completed" },
                { ...Onboarding_DA.UniformsIssued, value: "Issued" },
                { ...Onboarding_DA.MentorAccount, value: "Account Created" },
                { ...Onboarding_DA.GasCardPIN, value: "Completed" },
                { ...Onboarding_DA.RideAlong, value: "Completed" },
            ]
            OnBoardingData.forEach((item) => {
                cy.get('[data-cy="' + item.selector + '"]')
                    .find('.el-dropdown-link')
                    .click({ force: true })

                cy.get('ul.el-dropdown-menu:visible')
                    .find('li.el-dropdown-menu__item').contains(new RegExp(`^${item.value}$`))  // Regex to match exact string as "Not Sent" also contains "Sent"
                    .click({ force: true })

                _helper.waitXHR()

                if (item.selector == "drug-test") {
                    const confirm_btn = '[data-cy="drug-tests-modal-confirm-btn"]'
                    _helper.modal()
                        .find(confirm_btn)
                        .click()

                    _helper.fillModalForm([
                        { selector: "dt-modal-date-dpk", type: "input", value: DrugTests.Date },
                        { selector: "dt-modal-location-in", type: "input", value: DrugTests.Location },
                        { selector: "dt-modal-result-dpk", type: "select", value: DrugTests.Results },
                        { selector: "dt-modal-file-up", type: "file", value: DrugTests.File }
                    ]);

                    _helper.modal()
                        .find(confirm_btn)
                        .click()
                    _helper.waitXHR()

                } else if (item.selector == "medical-card") {
                    _helper.modal()
                        .find('[data-cy="drug-tests-modal-confirm-btn"]')
                        .click()

                    _helper.fillModalForm([
                        { selector: "physical-form-date-dpk", type: "input", value: MedicalCards.ReceivedDate },
                        { selector: "physical-form-expiration-date-dpk", type: "input", value: MedicalCards.ExpirationDate },
                        { selector: "physical-form-file-up", type: "file", value: MedicalCards.File },
                    ]);

                    _helper.modal()
                        .find('[data-cy="form-confirm-btn"]')
                        .click()
                    _helper.waitXHR()
                }

                if (item.dates) {
                    for (let idx = 0; idx < item.dates.length; idx++) {
                        const date = item.dates[idx];
                        cy.get(`.el-timeline-item :contains('${item.label}') .el-date-editor`)
                            .eq(idx).find('.el-input__inner').type(`${date.value}`, { force: true }).blur({ force: true })
                    }
                }

                if (item.selector == "ride-along") {
                    _helper.fillValue({ selector: "ride-along-trainer", value: DeliveryAssociate_Filter, type: "select" })
                }
            })

            MenuHelper.Tab.goTo("Associate Management", '/da-management/onboarding')
            new ComponentHelper('damanagement-options').actionButton('onboarding-tab').click({ force: true })

        })
    })
})