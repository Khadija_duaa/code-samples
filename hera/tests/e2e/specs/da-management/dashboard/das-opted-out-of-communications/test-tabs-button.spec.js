import MenuHelper from "../../../../support/Menu.helper"

describe("Associate Management > Dashboard  ", function () {
  context("Opted Out of Communications ", function () {
    beforeEach(function () {
      MenuHelper.Tab.goTo("Associate Management", '/da-management/dashboard', 'dashboard-tab')
    })

    //TASK ONE
    it('Card let me see Active Associate Opted Out of Comms by tabs and link to go to the page "daOptedOut"', function () {
      cy.get('[data-cy="card_optedOut"]').scrollIntoView({ easing: 'linear', offset: { top: -80 } })

      //validate title card 
      cy.get('[data-cy="card_optedOut"]').find('[data-cy="card_title"]').contains("Active Associate Opted Out of Comms").should("be.visible")
      let tabsNames = [
        "Email Off",
        "SMS Off",
        "Both Off",
        "SMS On But No Phone"
      ]

      cy.get('[data-cy="card_optedOut"]')
        .find('[data-cy="card_tabs-title"]')
        .each(function ($el, index, $list) {
          cy.contains(tabsNames[index]).should("be.visible")
        })

      //validate the link to  take us  DA Performance Report Page with path route name /staff/daOptedOut
      cy.get('[data-cy="card_optedOut"]').find('[data-cy="card_link-optedout"]').click({ force: true })
      cy.location("pathname").should("eq", "/reports/daOptedOut")
    })
  })
})