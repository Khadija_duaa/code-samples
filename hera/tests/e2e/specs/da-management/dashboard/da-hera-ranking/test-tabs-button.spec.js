import MenuHelper from "../../../../support/Menu.helper"

describe("Associate Management >  Dashboard", function () {
    context("Card Associate Hera Rankings", function () {
        beforeEach(function () {
            MenuHelper.Tab.goTo("Associate Management", '/da-management/dashboard', 'dashboard-tab')
        })
        it("Card let me see Associate Hera ranking by tabs and link to go to the page 'DA PERFORMANCE REPORT'", function () {
            cy.get('[data-cy="card_hera-rankings"]').find('[data-cy="card_title"]').contains("Associate Hera Rankings").should("be.visible")
            let tabsNames = [
                'Highest Ranking',
                'Lowest Ranking'
            ]
    
            cy.get('[data-cy="card_hera-rankings"]')
                .find('.el-tabs__item')
                .each(function ($el, index, $list) {
                    cy.get($el).contains(tabsNames[index]).should("be.visible")
                })

            //validate the link to  take us  DA Performance Report Page with path route name /coaching/da-performance-report
            cy.get('[data-cy="card_hera-rankings"]').find('[data-cy="card_link-DaPerformanceReport"]').click({ force: true })
            cy.location("pathname").should("eq", "/performance-and-coaching/da-performance-report")
        })
    })
})