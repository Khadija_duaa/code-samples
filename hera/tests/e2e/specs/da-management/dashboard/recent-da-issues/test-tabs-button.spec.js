import MenuHelper from "../../../../support/Menu.helper"

describe("Associate Management > Dashboard  ", function () {

  context("Recent Associate Issues", function () {

    beforeEach(function () {
      MenuHelper.Tab.goTo("Associate Management", '/da-management/dashboard', 'dashboard-tab')
    })

    it('Card let me see Recent Associate Issues by tabs and link to go to the page  "Associate Management Issues"', function () {
      //validate title card 
      cy.get('[data-cy="card_da-issues"]').find('[data-cy="card_title"]').contains("Recent Associate Issues (Last 90 Days)").should("be.visible")

      let tabsNames = [
        "Amazon Violation/Defects",
        "Associate Issues"
      ]

      cy.get('[data-cy="card_da-issues"]')
        .find('.el-tabs__item')
        .each(function ($el, index, $list) {
          cy.get($el).contains(tabsNames[index]).should("be.visible")
        })

      //validate the link to  take us  DA Performance Report Page with path route name /coaching/da-performance-report
      cy.get('[data-cy="card_da-issues"]').find('[data-cy="card_link-da-issues"]').click({ force: true })
      cy.location("pathname").should("eq", "/performance-and-coaching/da-issues")

    })

  })

})