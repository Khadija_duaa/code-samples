import MenuHelper from "../../../../support/Menu.helper"

describe("Associate Management > Dashboard  ", function () {
  context("Netradyne Alerts", function () {
    beforeEach(function () {
      MenuHelper.Tab.goTo("Associate Management", '/da-management/dashboard', 'dashboard-tab')
    })

    it('Card let me see Netradyne Alerts by tabs and link to go to the page "Netradyne Alerts"', function () {
      cy.get('[data-cy="card_netradyne-alert"]').find('[data-cy="card_title"]').contains("Netradyne Alerts").should("be.visible")

      let tabsNames = [
        "Yesterday's Alerts",
        "This Week's Alert"
      ]

      cy.get('[data-cy="card_netradyne-alert"]')
      cy.get('[data-cy="yesterday-alert"]')
        .each(function ($el, index, $list) {
          cy.get($el).contains(tabsNames[index]).should("be.visible")
        })

      //validate the link to  take us  DA Performance Report Page with path route name /coaching/da-performance-report
      cy.get('[data-cy="card_netradyne-alert"]').find('[data-cy="card_link-netradyne"]').click({ force: true })
      cy.location("pathname").should("eq", "/performance-and-coaching/netradyne-alerts")
    })
  })
})