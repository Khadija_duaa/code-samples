import MenuHelper from "../../../../support/Menu.helper"

describe("Associate Management > Dashboard  ",function(){

    context("Field Assessments",function(){
      // uncomment beforeEach once the card is visible on the UI
      /* beforeEach(function () {
        MenuHelper.Tab.goTo("Associate Management", '/da-management/dashboard', 'dashboard-tab')
      }) */

          //TASK ONE
      // Card is not visible on the UI (as of Oct 23, 2033) so skipping this test case
       it.skip('Card let me see Field Assessments by tabs ',function(){
        //validate title card
        cy.get('[data-cy="card_field-assessment"]').find('[data-cy="card_title"]').contains("Field Assessments").should("be.visible")

        let tabsNames = [
          "Recommended",
          "Recently Completed"
        ]

          cy.get('[data-cy="card_field-assessment"]')
            .find('.el-tabs__item')
            .each(function($el,index,$list){
                cy.get($el).contains(tabsNames[index]).should("be.visible")
            })
       })
    })
})