import MenuHelper from "../../../../support/Menu.helper"

describe("Associate Management > Dashboard  ", function () {

  context("Recent Associate Issues", function () {

    beforeEach(function () {
      MenuHelper.Tab.goTo("Associate Management", '/da-management/dashboard', 'dashboard-tab')
    })
    //TASK ONE

    it('Card let me see Expiring Driver Licenses by tabs and link to go to the page "reports"', function () {
      cy.get('[data-cy="card_driver-licenses"]').scrollIntoView({ easing: 'linear', offset: { top: -80 } })
      
      //validate title card 
      cy.get('[data-cy="card_driver-licenses"]').find('[data-cy="card_title"]').contains("Expiring Driver Licenses").should("be.visible")

      let tabsNames = [
        "Expiring Soon",
        "Expired"
      ]

      cy.get('[data-cy="card_driver-licenses"]')
        .find('.el-tabs__item')
        .each(function ($el, index, $list) {
          cy.get($el).contains(tabsNames[index]).should("be.visible")
        })

      //validate the link to  take us  to path route name /reports
      cy.get('[data-cy="card_driver-licenses"]').find('[data-cy="card_link-driver-licenses"]').click({ force: true })
      cy.location("pathname").should("eq", "/reports/da-driver-licenses")

    })


  })


})