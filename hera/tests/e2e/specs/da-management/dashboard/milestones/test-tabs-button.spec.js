import MenuHelper from "../../../../support/Menu.helper"

describe("Associate Management > Dashboard  ", function () {

  context("Recent Associate Management Issues", function () {

    beforeEach(function () {
      MenuHelper.Tab.goTo("Associate Management", '/da-management/dashboard', 'dashboard-tab')
    })
    //TASK ONE

    it('Card let me see Birthdays and Work Anniversaries by tabs', function () {
      cy.get('[data-cy="card_milestones"]').scrollIntoView({ easing: 'linear', offset: { top: -80 } })

      //validate title card 
      cy.get('[data-cy="card_milestones"]').find('[data-cy="card_title"]').contains("Birthdays and Work Anniversaries").should("be.visible")

      let tabsNames = [
        "Birthdays",
        "30 Days",
        "3 Months",
        "6 Months",
        "9 Months",
        "1 Year",
        "2 Year",
        "3 Year",
        "4 Year"
      ]

      cy.get('[data-cy="card_milestones"]')
        .find('[data-cy="card_tabs-title"]')
        .each(function ($el, index, $list) {
          cy.get($el).contains(tabsNames[index])
        })

      //validate the link to  take us  to path route name /staff/milestones
      cy.get('[data-cy="card_milestones"]').find('[data-cy="card_link-milestones"]').click({ force: true })
      cy.location("pathname").should("eq", "/reports/milestones")

    })


  })


})