
import ComponentHelper from "../../../support/Component.helper";
import FunctionHelper from "../../../support/Function.helper";
import MenuHelper from "../../../support/Menu.helper";
const {
    filters:{
        DeliveryAssociate_Filter
    },
    da_management: {
        da_list: {
            Status_DA
        }
    }
} = Cypress.env('e2e');

describe('Associate Management',()=>{
    context('Associate List',()=>{
        beforeEach(() => {
            MenuHelper.Tab.goTo("Associate Management", '/da-management/dashboard', 'dashboard-tab')
        });
        it('Test - Change Status button',()=>{
            new ComponentHelper('damanagement-options').actionButton('dalist-option').scrollIntoView().click()

            cy.scrollTo('top')

            let _helper = new ComponentHelper('da-list-content')

            cy.waitForLoaderToDisappear()
            
            _helper.actionButton("status-filter-sel").click()

            cy.get(".el-dropdown-menu:visible").contains("All").click({force: true})
            cy.get(".el-dropdown-menu:visible").invoke("hide")

            _helper.fillValue({selector: "search-in", type: "input", value: DeliveryAssociate_Filter})

            _helper.table().find("tr").contains(DeliveryAssociate_Filter).find(`[data-cy="da_table_row_dropdown"]`).scrollIntoView().click()

            //
            let menuOp = {
                SendMessage: "Send Message",
                ViewDA: "View Associate",
                ChangeStatus: "Change Status"
            }
            cy.get('.el-dropdown-menu:visible').contains(menuOp.ChangeStatus).scrollIntoView().click()
            
            let today = new FunctionHelper().getCurrentDate({
                format: 'mdY',
                separator: '/'
            });

            _helper = new ComponentHelper("change-status-form")
            
            _helper.fillModalForm([
                {selector: "csd-new-status-sel", type:"select", value: Status_DA.Status},
                {selector: "csd-date-dpk", type:"input", value: today},
                {selector: "csd-status-change-reason-in", type:"input", value: Status_DA.Reason},
            ])

            _helper.actionButton("csd-update-btn").scrollIntoView().click()
        })
    })
})