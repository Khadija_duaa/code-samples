import ComponentHelper from "../../../support/Component.helper"
import MenuHelper from "../../../support/Menu.helper"
const {
    filters:{
        DeliveryAssociate_Filter
    }
} = Cypress.env('e2e');

describe("Associate Management", function () {
    context("Associate List", function () {
        beforeEach(function () {
            MenuHelper.Tab.goTo("Associate Management", '/da-management/dashboard', 'dashboard-tab')
            new ComponentHelper('damanagement-options').actionButton('dalist-option').scrollIntoView().click()
        })

        it("Input Filter by status", function () {

            cy.scrollTo('top')

            let _helper = new ComponentHelper('da-list-content')

            cy.waitForLoaderToDisappear()

            _helper.actionButton("status-filter-sel").click()

            cy.get(".el-dropdown-menu:visible").contains("All").click({force: true})
            cy.get(".el-dropdown-menu:visible").invoke("hide")

            _helper.fillValue({selector: "search-in", type: "input", value: DeliveryAssociate_Filter})

            _helper.table().find("tr").contains(DeliveryAssociate_Filter).its("length").then(length => {
                cy.get(`[data-cy="dalist-search-result-message"]:contains("Showing ${length}")`).should("be.visible")
            })

        })
    })
})