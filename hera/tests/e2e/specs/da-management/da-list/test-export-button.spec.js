import ComponentHelper from "../../../support/Component.helper";
import FunctionHelper from "../../../support/Function.helper";
import MenuHelper from "../../../support/Menu.helper";

describe('Associate Management', function () {
    context('Associate List', function () {
        beforeEach(() => {
            MenuHelper.Tab.goTo("Associate Management", '/da-management/dashboard', 'dashboard-tab')
        });

        it('Test table export functionality', function () {
            let __helper = new ComponentHelper('damanagement-options')
            __helper.actionButton('dalist-option').click({force: true});

            cy.scrollTo('top')

            __helper = new ComponentHelper('da-list-content')

            cy.waitForLoaderToDisappear()

            __helper.actionButton('export').click({force: true});
            __helper.modalButtonContain('Export').click({force: true});
            let ext = '.csv'
            let today = new FunctionHelper().getCurrentDate({
                format: 'mdY',
                separator: '_'
            });
            let filename = 'da-export-Active-' + today + ext
            cy.readFile('cypress/downloads/' + filename).then((content) => {
                assert(content,'**EXPORT SUCCESS**')
            });
        });
    })
})