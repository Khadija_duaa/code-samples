
import ComponentHelper from "../../../../support/Component.helper";
import MenuHelper from "../../../../support/Menu.helper";
const {
    filters:{
        DeliveryAssociate_Filter
    },
    da_management: {
        da_list: {
            view_da: {
                DrivingInfo_DA
            }
        }
    }
} = Cypress.env('e2e');

describe('Associate Management',()=>{
    context('Associate List > View Associate',()=>{
        beforeEach(() => {
            MenuHelper.Tab.goTo("Associate Management", '/da-management/dashboard', 'dashboard-tab')
        });
        it('Test - Update Driving Info',()=>{
            new ComponentHelper('damanagement-options').actionButton('dalist-option').scrollIntoView().click()

            cy.scrollTo('top')

            let _helper = new ComponentHelper('da-list-content')

            cy.waitForLoaderToDisappear()

            _helper.actionButton("status-filter-sel").click()

            cy.get(".el-dropdown-menu:visible").contains("All").click()
            cy.get(".el-dropdown-menu:visible").invoke("hide")

            _helper.fillValue({selector: "search-in", type: "input", value: DeliveryAssociate_Filter})

            _helper.table().find("tr").contains(DeliveryAssociate_Filter).find(`[data-cy="da_table_row_dropdown"]`).scrollIntoView().click()

            let menuOp = {
                SendMessage: "Send Message",
                ViewDA: "View Associate",
                ChangeStatus: "Change Status"
            }
            cy.get('.el-dropdown-menu:visible').contains(menuOp.ViewDA).scrollIntoView().click()
            
            cy.scrollTo('center')

            _helper = new ComponentHelper("driving-info-card");

            _helper.actionButton("edit-driving-info-btn").scrollIntoView().click()

            _helper.fillModalForm([
                {selector: "di-expiration-dpk", type: "input", value: DrivingInfo_DA.DLExpiration},
                {selector: "di-vehicle-dpk", type: "input", value: DrivingInfo_DA.MotorVehicleReport},
                {selector: "di-gas-card-pin-in", type: "input", value: DrivingInfo_DA.GasCardPIN},
                {selector: "di-default-vehicle-sel", type: "select", value: DrivingInfo_DA.PrimaryVehicle},
                {selector: "di-default-vehicle-2-sel", type: "select", value: DrivingInfo_DA.SecondaryVehicle},
                {selector: "di-default-vehicle-3-sel", type: "select", value: DrivingInfo_DA.TertiaryVehicle}
            ]);

            _helper.modalButtonContain("Update").scrollIntoView().click();

        })
    })
})