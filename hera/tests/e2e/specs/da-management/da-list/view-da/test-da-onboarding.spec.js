
import ComponentHelper from "../../../../support/Component.helper";
import MenuHelper from "../../../../support/Menu.helper";
import FunctionHelper from "../../../../support/Function.helper";
const {
    filters:{
        DeliveryAssociate_Filter
    },
    da_management: {
        da_list: {
            view_da: {
                Onboarding_DA
            }
        }
    }
} = Cypress.env('e2e');

describe('Associate Management',()=>{
    context('Associate List > View Associate',()=>{
        beforeEach(() => {
            MenuHelper.Tab.goTo("Associate Management", '/da-management/dashboard', 'dashboard-tab')
        });
        it('Test - Onboarding',()=>{
            new ComponentHelper('damanagement-options').actionButton('dalist-option').scrollIntoView().click()

            cy.scrollTo('top')

            let _helper = new ComponentHelper('da-list-content')

            cy.waitForLoaderToDisappear()

            _helper.actionButton("status-filter-sel").click()

            cy.get(".el-dropdown-menu:visible").contains("All").click()
            cy.get(".el-dropdown-menu:visible").invoke("hide")

            _helper.fillValue({selector: "search-in", type: "input", value: DeliveryAssociate_Filter})

            _helper.table().find("tr").contains(DeliveryAssociate_Filter).find(`[data-cy="da_table_row_dropdown"]`).scrollIntoView().click()

            let menuOp = {
                SendMessage: "Send Message",
                ViewDA: "View Associate",
                ChangeStatus: "Change Status"
            }
            cy.get('.el-dropdown-menu:visible').contains(menuOp.ViewDA).scrollIntoView().click()

            cy.scrollTo('bottom')

            _helper = new ComponentHelper("staff-detail-container");

            _helper.container().find('.el-collapse-item [role="button"]').contains("ONBOARDING").scrollIntoView().click()

            _helper.container()
                .find('[data-cy="da-will-be-driving"] > [role="button"]')
                .click({force: true})

            cy.wait(1000)

            cy.get('body').find('ul.el-dropdown-menu:visible')
                .find('li.el-dropdown-menu__item').contains('Standard Vehicles')
                .click({force: true})

            cy.wait(1000)

            cy.get('[data-cy="onboarding-form"]').should('be.visible')
        })
    })
})