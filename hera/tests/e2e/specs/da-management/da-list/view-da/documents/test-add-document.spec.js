import ComponentHelper from "../../../../../support/Component.helper2";
import MenuHelper from "../../../../../support/Menu.helper";
const {
    filters:{
        DeliveryAssociate_Filter
    },
    da_management: {
        da_list: {
            view_da: {
                documents:{
                    Documents
                }
            }
        }
    }
} = Cypress.env('e2e');

describe('Associate Management',()=>{
    context('Associate List > View Associate',()=>{
        beforeEach(() => {
            MenuHelper.Tab.goTo("Associate Management", '/da-management/dashboard', 'dashboard-tab')
        });
        it('Test - Documents > Add Document form',()=>{
            new ComponentHelper('damanagement-options').actionButton('dalist-option').scrollIntoView().click()

            let _helper = new ComponentHelper('da-list-content')

            cy.waitForLoaderToDisappear()

            _helper.actionButton("status-filter-sel").click()

            cy.get(".el-dropdown-menu:visible").contains("All").click()
            cy.get(".el-dropdown-menu:visible").invoke("hide")

            _helper.fillValue({selector: "search-in", type: "input", value: DeliveryAssociate_Filter})

            _helper.table().find("tr").contains(DeliveryAssociate_Filter).find(`[data-cy="da_table_row_dropdown"]`).scrollIntoView().click()

            let menuOp = {
                SendMessage: "Send Message",
                ViewDA: "View Associate",
                ChangeStatus: "Change Status"
            }
            cy.get('.el-dropdown-menu:visible').contains(menuOp.ViewDA).scrollIntoView().click().wait(500)

            cy.waitForLoaderToDisappear()
            cy.scrollTo(0, '90%')

            _helper = new ComponentHelper("staff-detail-container");

            _helper.container().find('.el-collapse-item [role="button"]').contains("DOCUMENTS").scrollIntoView().click()

            _helper.actionButton("da-document-add-btn").scrollIntoView().click();

            _helper.fillModalForm([
                {selector: "document-type-sel", type: "select", value: Documents.Type},
                {selector: "document-notes-in", type: "input", value: Documents.Notes},
                {selector: "document-date-dpk", type: "input", value: Documents.Date},
                {selector: "document-file-up", type: "file", value: Documents.File},
            ]);
            
            _helper.modalButton("form-confirm-btn").scrollIntoView().click();

        })
    })
})