
import ComponentHelper from "../../../../support/Component.helper";
import MenuHelper from "../../../../support/Menu.helper";
const {
    filters:{
        DeliveryAssociate_Filter
    },
    da_management: {
        da_list: {
            view_da: {
                DAPreferences
            }
        }
    }
} = Cypress.env('e2e');

describe('Associate Management',()=>{
    context('Associate List > View Associate',()=>{
        beforeEach(() => {
            MenuHelper.Tab.goTo("Associate Management", '/da-management/dashboard', 'dashboard-tab')
        });
        it('Test - Associate Preference button',()=>{
            new ComponentHelper('damanagement-options').actionButton('dalist-option').scrollIntoView().click()

            cy.scrollTo('top')

            let _helper = new ComponentHelper('da-list-content')

            cy.waitForLoaderToDisappear()

            _helper.actionButton("status-filter-sel").click()

            cy.get(".el-dropdown-menu:visible").contains("All").click({force: true})
            cy.get(".el-dropdown-menu:visible").invoke("hide")

            _helper.fillValue({selector: "search-in", type: "input", value: DeliveryAssociate_Filter})

            _helper.table().find("tr").contains(DeliveryAssociate_Filter).find(`[data-cy="da_table_row_dropdown"]`).scrollIntoView().click()

            let menuOp = {
                SendMessage: "Send Message",
                ViewDA: "View Associate",
                ChangeStatus: "Change Status"
            }
            cy.get('.el-dropdown-menu:visible').contains(menuOp.ViewDA).scrollIntoView().click()
            
            let staffdetailMenuOp = {
                Message: "Message",
                DAPreference: "Associate Preference",
                DeleteDA: "Delete Associate"
            }

            _helper = new ComponentHelper("staff-detail-container")
            _helper.actionButton("specific_da_dropdown").scrollIntoView().click()

            cy.get('.el-dropdown-menu:visible').contains(staffdetailMenuOp.DAPreference).scrollIntoView().click()

            _helper.modal().find('[role="tab"]').as("tabs")

            cy.get("@tabs").contains("Communication").scrollIntoView().click().then(()=>{


                _helper.fillModalForm([
                    {selector: "receive-email-messages-chk", type: "check", value: DAPreferences.Communication.EmailUpdates},
                    {selector: "receive-text-messages-chk", type: "check", value: DAPreferences.Communication.TextUpdates},
                ])

                // /* To send test messages */
                // _helper.modalButton("send-test-message-btn").scrollIntoView().click()
                // cy.wait(5000)

            })
            
            cy.get("@tabs").contains("Imports").scrollIntoView().click().then(()=>{

                _helper.modalButton("new-import-name-btn").scrollIntoView().click()
                _helper.fillModalValue({selector: "import-name-in", type: "input", value: DAPreferences.ImportName, ttw: 1000})
    
            })

            _helper.actionButtonContain("Done").scrollIntoView().click()

        })
    })
})