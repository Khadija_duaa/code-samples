
import ComponentHelper from "../../../../support/Component.helper";
import MenuHelper from "../../../../support/Menu.helper";
const {
    filters:{
        DeliveryAssociate_Filter
    },
    da_management: {
        da_list: {
            view_da: {
                ConpanyInfo_DA
            }
        }
    }
} = Cypress.env('e2e');

describe('Associate Management',()=>{
    context('Associate List > View Associate',()=>{
        beforeEach(() => {
            MenuHelper.Tab.goTo("Associate Management", '/da-management/dashboard', 'dashboard-tab')
        });
        it('Test - Update Company Info',()=>{
            new ComponentHelper('damanagement-options').actionButton('dalist-option').scrollIntoView().click()

            cy.scrollTo('top')

            let _helper = new ComponentHelper('da-list-content')

            cy.waitForLoaderToDisappear()

            _helper.actionButton("status-filter-sel").click()

            cy.get(".el-dropdown-menu:visible").contains("All").click()
            cy.get(".el-dropdown-menu:visible").invoke("hide")

            _helper.fillValue({selector: "search-in", type: "input", value: DeliveryAssociate_Filter})

            _helper.table().find("tr").contains(DeliveryAssociate_Filter).find(`[data-cy="da_table_row_dropdown"]`).scrollIntoView().click()

            let menuOp = {
                SendMessage: "Send Message",
                ViewDA: "View Associate",
                ChangeStatus: "Change Status"
            }
            cy.get('.el-dropdown-menu:visible').contains(menuOp.ViewDA).scrollIntoView().click()

            cy.scrollTo('center')

            _helper = new ComponentHelper("da-company-info-card");

            _helper.actionButton("da-company-info-edit-btn").scrollIntoView().click()

            _helper.fillModalForm([
                {selector: "ci-supervised-by-sel", type: "select", value: ConpanyInfo_DA.SupervisedBy},
                {selector: "ci-preferred-days-off-sel", type: "select", value: ConpanyInfo_DA.PreferredDaysOff},
                {selector: "ci-hourly-status-sel", type: "select", value: ConpanyInfo_DA.HourlyStatus},
                {selector: "ci-hired-dpk", type: "input", value: ConpanyInfo_DA.Hired},
                {selector: "ci-final-check-issued-dpk", type: "input", value: ConpanyInfo_DA.FinalCheckIssued},
            ]);
            
            _helper.modalButtonContain("Update").scrollIntoView().click();

        })
    })
})