
import ComponentHelper from "../../../../../support/Component.helper";
import MenuHelper from "../../../../../support/Menu.helper";

const {
    filters: {
        DeliveryAssociate_Filter
    },
    da_management: {
        da_list: {
            view_da: {
                Onboarding_DA
            }
        }
    }
} = Cypress.env('e2e');

describe('Associate Management', () => {
    context('Associate List > View Associate', () => {
        beforeEach(() => {
            MenuHelper.Tab.goTo("Associate Management", '/da-management/dashboard', 'dashboard-tab')
        });
        it('Test - Onboarding - Standard Vehicles: Fill form type', () => {

            new ComponentHelper('damanagement-options').actionButton('dalist-option').scrollIntoView().click()

            cy.scrollTo('top')

            let _helper = new ComponentHelper('da-list-content')

            cy.waitForLoaderToDisappear()

            _helper.actionButton("status-filter-sel").click()

            cy.get(".el-dropdown-menu:visible").contains("All").click({force: true})
            cy.get(".el-dropdown-menu:visible").invoke("hide")

            _helper.fillValue({ selector: "search-in", type: "input", value: DeliveryAssociate_Filter })

            _helper.table().find("tr").contains(DeliveryAssociate_Filter).find(`[data-cy="da_table_row_dropdown"]`).scrollIntoView().click()

            let menuOp = {
                SendMessage: "Send Message",
                ViewDA: "View Associate",
                ChangeStatus: "Change Status"
            }
            cy.get('.el-dropdown-menu:visible').contains(menuOp.ViewDA).scrollIntoView().click()

            cy.scrollTo('bottom')

            _helper = new ComponentHelper("staff-detail-container");

            _helper.container().find('.el-collapse-item [role="button"]').contains("ONBOARDING").click({ force: true })

            _helper.container()
                .find('[data-cy="da-will-be-driving"] > [role="button"]')
                .click({ force: true })

            cy.get('body').find('ul.el-dropdown-menu:visible')
                .find('li.el-dropdown-menu__item').contains('Standard Vehicles')
                .click({ force: true }).wait(500)

            cy.waitForLoaderToDisappear()
            cy.scrollTo(0, '95%')

            const OnBoardingData = [
                { ...Onboarding_DA.Offer, dates: Onboarding_DA.Offer.dates[0] },
                { ...Onboarding_DA.AmazonProfile, dates: Onboarding_DA.AmazonProfile.dates[0] },
                { ...Onboarding_DA.BackgroundCheck },
                { ...Onboarding_DA.TrainingVideos, dates: null },
                { ...Onboarding_DA.DrugTest, dates: Onboarding_DA.DrugTest.dates[0] },
                { ...Onboarding_DA.AddedtoPayroll, dates: null },
                { ...Onboarding_DA.I9Verification, dates: null },
                { ...Onboarding_DA.DSPOrientation, dates: Onboarding_DA.DSPOrientation.dates[0] },
                { ...Onboarding_DA.InPersonAmazonTraining, dates: Onboarding_DA.InPersonAmazonTraining.dates[0] },
                { ...Onboarding_DA.UniformsIssued },
                { ...Onboarding_DA.MentorAccount },
                { ...Onboarding_DA.GasCardPIN, dates: null },
                { ...Onboarding_DA.RideAlong, dates: null }
            ]

            OnBoardingData.forEach((item) => {
                _helper.container().find(".el-loading-mask")
                cy.get('[data-cy="' + item.selector + '"]')
                    .find('.el-dropdown-link')
                    .click({ force: true })

                cy.get('ul.el-dropdown-menu:visible')
                    .find('li.el-dropdown-menu__item').contains(new RegExp(`^${item.value}$`))  // Regex to match exact string as "Not Sent" also contains "Sent"
                    .click({ force: true })

                cy.wait(3500)

                if (item.dates) {
                    for (let idx = 0; idx < item.dates.length; idx++) {
                        const date = item.dates[idx];
                        cy.get(`.el-timeline-item :contains('${item.label}') .el-date-editor`)
                            .eq(idx).type(`{selectAll}${date.value}`).blur({ force: true })
                    }
                }
            })
        })
    })
})