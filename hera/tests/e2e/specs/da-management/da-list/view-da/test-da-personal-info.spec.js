
import ComponentHelper from "../../../../support/Component.helper";
import MenuHelper from "../../../../support/Menu.helper";
const {
    filters:{
        DeliveryAssociate_Filter
    },
    da_management: {
        da_list: {
            view_da: {
                PersonalInfo_DA
            }
        }
    }
} = Cypress.env('e2e');

describe('Associate Management',()=>{
    context('Associate List > View Associate',()=>{
        beforeEach(() => {
            MenuHelper.Tab.goTo("Associate Management", '/da-management/dashboard', 'dashboard-tab')
        });
        it('Test - Update Personal Info',()=>{
            new ComponentHelper('damanagement-options').actionButton('dalist-option').scrollIntoView().click()

            cy.scrollTo('top')

            let _helper = new ComponentHelper('da-list-content')

            cy.waitForLoaderToDisappear()

            _helper.actionButton("status-filter-sel").click()

            cy.get(".el-dropdown-menu:visible").contains("All").click({force: true})
            cy.get(".el-dropdown-menu:visible").invoke("hide")

            _helper.fillValue({selector: "search-in", type: "input", value: DeliveryAssociate_Filter})

            _helper.table().find("tr").contains(DeliveryAssociate_Filter).find(`[data-cy="da_table_row_dropdown"]`).scrollIntoView().click()

            let menuOp = {
                SendMessage: "Send Message",
                ViewDA: "View Associate",
                ChangeStatus: "Change Status"
            }
            cy.get('.el-dropdown-menu:visible').contains(menuOp.ViewDA).scrollIntoView().click()
            
            cy.wait(1000)

            _helper = new ComponentHelper("da-personal-info-card");

            _helper.actionButton("da-personal-info-edit-btn").scrollIntoView().click()

            _helper.fillModalForm([
                {selector: "pi-first-name-in", type: "input", value: PersonalInfo_DA.FirstName},
                {selector: "pi-last-name-in", type: "input", value: PersonalInfo_DA.LastName},
                {selector: "pi-email-in", type: "input", value: PersonalInfo_DA.Email},
                {selector: "pi-email-updates-swch", type: "check", value: PersonalInfo_DA.EmailUpdates},
                {selector: "pi-phone-in", type: "input", value: PersonalInfo_DA.Phone},
                {selector: "pi-test-updates-swch", type: "check", value: PersonalInfo_DA.TextUpdates},
                {selector: "pi-transporter-id-in", type: "input", value: PersonalInfo_DA.TranporterID},
                {selector: "pi-dob-dpk", type: "input", value: PersonalInfo_DA.DOB},
                {selector: "pi-gender-sel", type: "select", value: PersonalInfo_DA.Gender},
                {selector: "pi-pronouns-sel", type: "select", value: PersonalInfo_DA.Pronouns},
            ]);
            
            _helper.modalButtonContain("Update").scrollIntoView().click();

        })
    })
})