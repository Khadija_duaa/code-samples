import ComponentHelper from "../../../../../support/Component.helper";
import MenuHelper from "../../../../../support/Menu.helper";
const {
    filters:{
        DeliveryAssociate_Filter
    },
    da_management: {
        da_list: {
            view_da: {
                uniforms:{
                    Uniforms
                }
            }
        }
    }
} = Cypress.env('e2e');

describe('Associate Management',()=>{
    context('Associate List > View Associate',()=>{
        beforeEach(() => {
            MenuHelper.Tab.goTo("Associate Management", '/da-management/dashboard', 'dashboard-tab')
        });
        it('Test - Uniforms > Add Uniform Button',()=>{
            new ComponentHelper('damanagement-options').actionButton('dalist-option').scrollIntoView().click()

            cy.scrollTo('top')

            let _helper = new ComponentHelper('da-list-content')

            cy.waitForLoaderToDisappear()

            _helper.actionButton("status-filter-sel").click()

            cy.get(".el-dropdown-menu:visible").contains("All").click({force: true})
            cy.get(".el-dropdown-menu:visible").invoke("hide")

            _helper.fillValue({selector: "search-in", type: "input", value: DeliveryAssociate_Filter})

            _helper.table().find("tr").contains(DeliveryAssociate_Filter).find(`[data-cy="da_table_row_dropdown"]`).scrollIntoView().click()

            let menuOp = {
                SendMessage: "Send Message",
                ViewDA: "View Associate",
                ChangeStatus: "Change Status"
            }
            cy.get('.el-dropdown-menu:visible').contains(menuOp.ViewDA).scrollIntoView().click().wait(500)
            
            cy.scrollTo('bottom')

            _helper = new ComponentHelper("staff-detail-container");

            _helper.container().find('.el-collapse-item [role="button"]').contains("UNIFORMS").scrollIntoView().click()

            _helper.actionButton("da-uniform-add-btn").scrollIntoView().click()

            _helper.fillModalForm([
                {selector: "uf-type-sel", type: "select", value: Uniforms.Type},
                {selector: "uf-size-sel", type: "select", value: Uniforms.Size},
                {selector: "uf-qty-in", type: "input", value: Uniforms.QtyIssued},
                {selector: "uf-issue-date-dpk", type: "input", value: Uniforms.IssueDate},
                {selector: "uf-returned-date-dpk", type: "input", value: Uniforms.ReturnedDate},
            ]);
            
            _helper.modalButton("uf-confirm-btn").scrollIntoView().click();

        })
    })
})