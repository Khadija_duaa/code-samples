import ComponentHelper from "../../../../../support/Component.helper";
import MenuHelper from "../../../../../support/Menu.helper";
const {
    filters:{
        DeliveryAssociate_Filter
    },
    da_management: {
        da_list: {
            view_da: {
                drug_tests:{
                    DrugTests_ED
                }
            }
        }
    }
} = Cypress.env('e2e');

describe('Associate Management',()=>{
    context('Associate List > View Associate',()=>{
        beforeEach(() => {
            MenuHelper.Tab.goTo("Associate Management", '/da-management/dashboard', 'dashboard-tab')
        });
        it('Test - Documents > Edit a Drug Test',()=>{
            new ComponentHelper('damanagement-options').actionButton('dalist-option').scrollIntoView().click()

            let _helper = new ComponentHelper('da-list-content')

            cy.waitForLoaderToDisappear()

            _helper.actionButton("status-filter-sel").click()

            cy.get(".el-dropdown-menu:visible").contains("All").click({force: true})
            cy.get(".el-dropdown-menu:visible").invoke("hide")

            _helper.fillValue({selector: "search-in", type: "input", value: DeliveryAssociate_Filter})

            _helper.table().find("tr").contains(DeliveryAssociate_Filter).find(`[data-cy="da_table_row_dropdown"]`).scrollIntoView().click()

            let menuOp = {
                SendMessage: "Send Message",
                ViewDA: "View Associate",
                ChangeStatus: "Change Status"
            }
            cy.get('.el-dropdown-menu:visible').contains(menuOp.ViewDA).scrollIntoView().click().wait(500)
            
            cy.scrollTo('bottom')

            _helper = new ComponentHelper("staff-detail-container");

            _helper.container().find('.el-collapse-item [role="button"]').contains("DRUG TESTS").scrollIntoView().click()

            _helper.container().find('[data-cy="edit-drug-test-btn"]:visible').invoke('first').scrollIntoView().click()

            _helper.modalButton("dt-trash-file-btn").invoke("is",":visible").then(visible => {
                if(visible){
                    _helper.modalButton("dt-trash-file-btn").scrollIntoView().click();
                    _helper.modal().find(".el-message-box:visible").as("message_box").contains("Delete").scrollIntoView().click()
                    cy.get("@message_box").invoke("hide")
                    cy.wait(1000)
                }
            })

            _helper.fillModalForm([
                {selector: "dt-modal-date-dpk", type: "input", value: DrugTests_ED.Date},
                {selector: "dt-modal-location-in", type: "input", value: DrugTests_ED.Location},
                {selector: "dt-modal-result-dpk", type: "select", value: DrugTests_ED.Results},
                {selector: "dt-modal-file-up", type: "file", value: DrugTests_ED.File}
            ]);
            
            _helper.modalButton("drug-tests-modal-update-btn").scrollIntoView().click();

        })
    })
})