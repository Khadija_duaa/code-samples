import ComponentHelper from "../../../../../support/Component.helper";
import MenuHelper from "../../../../../support/Menu.helper";
const {
    filters:{
        DeliveryAssociate_Filter
    },
    da_management: {
        da_list: {
            view_da: {
                drug_tests:{
                    DrugTests
                }
            }
        }
    }
} = Cypress.env('e2e');

describe('Associate Management',()=>{
    context('Associate List > View Associate',()=>{
        beforeEach(() => {
            MenuHelper.Tab.goTo("Associate Management", '/da-management/dashboard', 'dashboard-tab')
        });
        it('Test - Documents > Add a new Drug Test',()=>{
            new ComponentHelper('damanagement-options').actionButton('dalist-option').scrollIntoView().click()

            cy.scrollTo('top')

            let _helper = new ComponentHelper('da-list-content')

            cy.waitForLoaderToDisappear()

            _helper.actionButton("status-filter-sel").click()

            cy.get(".el-dropdown-menu:visible").contains("All").click({force: true})
            cy.get(".el-dropdown-menu:visible").invoke("hide")

            _helper.fillValue({selector: "search-in", type: "input", value: DeliveryAssociate_Filter})

            _helper.table().find("tr").contains(DeliveryAssociate_Filter).find(`[data-cy="da_table_row_dropdown"]`).scrollIntoView().click()

            let menuOp = {
                SendMessage: "Send Message",
                ViewDA: "View Associate",
                ChangeStatus: "Change Status"
            }
            cy.get('.el-dropdown-menu:visible').contains(menuOp.ViewDA).scrollIntoView().click().wait(500)
            
            cy.scrollTo('bottom')
            
            _helper = new ComponentHelper("staff-detail-container");

            _helper.container().find('.el-collapse-item [role="button"]').contains("DRUG TESTS").scrollIntoView().click()

            _helper.actionButton("drug-tests-add-btn").scrollIntoView().click();

            _helper.fillModalForm([
                {selector: "dt-modal-date-dpk", type: "input", value: DrugTests.Date},
                {selector: "dt-modal-location-in", type: "input", value: DrugTests.Location},
                {selector: "dt-modal-result-dpk", type: "select", value: DrugTests.Results},
                {selector: "dt-modal-file-up", type: "file", value: DrugTests.File}
            ]);
            
            _helper.modalButton("drug-tests-modal-confirm-btn").scrollIntoView().click();

        })
    })
})