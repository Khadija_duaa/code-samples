import ComponentHelper from "../../../../../support/Component.helper";
import MenuHelper from "../../../../../support/Menu.helper";
const {
    filters:{
        DeliveryAssociate_Filter
    },
    da_management: {
        da_list: {
            view_da: {
                medical_card:{
                    MedicalCards
                }
            }
        }
    }
} = Cypress.env('e2e');

describe('Associate Management',()=>{
    context('Associate List > View Associate',()=>{
        beforeEach(() => {
            MenuHelper.Tab.goTo("Associate Management", '/da-management/dashboard', 'dashboard-tab')
        });
        it('Test - Medical Card > Add Medical Card Button',()=>{
            new ComponentHelper('damanagement-options').actionButton('dalist-option').scrollIntoView().click()

            cy.scrollTo('top')

            let _helper = new ComponentHelper('da-list-content')
            
            cy.waitForLoaderToDisappear()

            _helper.actionButton("status-filter-sel").click()

            cy.get(".el-dropdown-menu:visible").contains("All").click({force: true})
            cy.get(".el-dropdown-menu:visible").invoke("hide")

            _helper.fillValue({selector: "search-in", type: "input", value: DeliveryAssociate_Filter})

            _helper.table().find("tr").contains(DeliveryAssociate_Filter).find(`[data-cy="da_table_row_dropdown"]`).scrollIntoView().click()

            let menuOp = {
                SendMessage: "Send Message",
                ViewDA: "View Associate",
                ChangeStatus: "Change Status"
            }
            cy.get('.el-dropdown-menu:visible').contains(menuOp.ViewDA).scrollIntoView().click()
            
            cy.scrollTo('bottom')

            _helper = new ComponentHelper("staff-detail-container");

            _helper.container().find('.el-collapse-item [role="button"]').contains("MEDICAL CARDS").scrollIntoView().click()

            _helper.actionButton("da-medical-card-add-btn").scrollIntoView().click()

            _helper.fillModalForm([
                {selector: "physical-form-date-dpk", type: "input", value: MedicalCards.ReceivedDate},
                {selector: "physical-form-expiration-date-dpk", type: "input", value: MedicalCards.ExpirationDate},
                {selector: "physical-form-file-up", type: "file", value: MedicalCards.File},
            ]);
            
            _helper.modalButton("form-confirm-btn").scrollIntoView().click();

        })
    })
})