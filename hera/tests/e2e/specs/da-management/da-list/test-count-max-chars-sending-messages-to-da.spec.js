import MenuHelper from '../../../support/Menu.helper'
import ComponentHelper from "../../../support/Component.helper2"
describe("Associate Management",()=>{
    context("Associate list",()=>{
        beforeEach(function () {
            MenuHelper.Tab.goTo("Associate Management", '/da-management/dashboard', 'dashboard-tab')
        })

        it("Test - Allow to view count/Max. characters sending messages to Associates.",()=>{
            new ComponentHelper('damanagement-options').actionButton('dalist-option').scrollIntoView().click()

            cy.scrollTo('top')

            let _helper = new ComponentHelper('da-list-content')

            cy.waitForLoaderToDisappear()

            _helper.actionButton("status-filter-sel").click()

            cy.get(".el-dropdown-menu:visible").contains("All").click({force: true})
            cy.get(".el-dropdown-menu:visible").invoke("hide")

            const filter = "Hera TestDA"
            _helper.fillValue({selector: "search-in", type: "input", value: filter})
            _helper.table().contains("tr", filter).find(`.el-checkbox`).scrollIntoView().click()

            const menuOp = {
                MessageDAs: "Message Associates",
                AddDA:"Add Associate",
                ExportDAs : "Export Associates ",
                ImportDAs: "Import Associates"
            }

            cy.get("[data-cy='dropdown-da-header-table']").scrollIntoView().click()
            cy.get(".el-dropdown-menu:visible").contains("li", menuOp.MessageDAs).scrollIntoView().click().wait(100)
            cy.get(".el-dropdown-menu:visible").invoke("hide")

            _helper.modal("[data-cy='message-text-area']").as("helper_textarea")

            const fx_TypeMessage = function(displayName, message){
                cy.then(() => {
                    Cypress.log({
                        displayName,
                        message: `**[${message}]()**`,
                    })
                    Cypress.log({
                        displayName: 'Writing',
                        message: `**[_Trying to write a_ ${message.length} _character message._]()**`,
                    })
                    
                })
                cy.get("@helper_textarea", {log: false}).type(`{selectAll}${message}`, {log: false})
                _helper.modal("[data-cy='message-text-count']")
                    .find("span", {log: false})
                    .invoke({log: false},"text")
                    .then(text => assert(1,`Count: **${text}**`))
                cy.wait(1500)
            }

            fx_TypeMessage("MESSAGE 1","HELLO WORLD...")
            fx_TypeMessage("MESSAGE 2","These are test messages..")
            fx_TypeMessage("MESSAGE 3","123456789")

        })
    })
})