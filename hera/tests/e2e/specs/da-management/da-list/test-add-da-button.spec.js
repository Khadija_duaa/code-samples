import ComponentHelper from "../../../support/Component.helper";
import MenuHelper from "../../../support/Menu.helper";
const {
    da_management: {
        da_list: {
            DeliveryAssociate,
            DeliveryAssociate_Supervisor
        }
    }
} = Cypress.env('e2e');

describe('Associate Management', function () {

    function AddNewDA(DA){
        new ComponentHelper('damanagement-options').actionButton('dalist-option').scrollIntoView().click()

        cy.scrollTo('top')

        let _helperContent = new ComponentHelper('da-list-content')

        cy.waitForLoaderToDisappear()

        _helperContent.actionButton('dropdown-da-header-table').click()

        let menuOp = {
            AddDA:"Add Associate",
            ExportDAs : "Export Associates ",
            ImportDAs: "Import Associates"
        }
        
        cy.get(".el-dropdown-menu:visible").contains(menuOp.AddDA).scrollIntoView().click()
        cy.get(".el-dropdown-menu:visible").invoke("hide")
       
        cy.location('pathname').should('eq', '/da-management/associate-list');

        let __helperForm = new ComponentHelper("add-da-form");

        __helperForm.fillForm([
            {type: 'select', selector: 'da-status-sel', value: DA.Status},
            {type: 'input', selector: 'da-first-name-in', value: DA.FirstName},
            {type: 'input', selector: 'da-last-name-in', value: DA.LastName},
            {type: 'input', selector: 'da-email-in', value: DA.Email},
            {type: 'input', selector: 'da-phone-in', value: DA.Phone},
            {type: 'input', selector: 'da-transporter-in', value: DA.TranporterID},
            {type: 'input', selector: 'da-expiration-dpk', value: DA.DLExpiration},
            {type: 'input', selector: 'da-hire-date-dpk', value: DA.HireDate},
            {type: 'input', selector: 'da-dob', value: DA.DOB},
            {type: 'select', selector: 'da-authorized-to-drive-in', value: DA.AuthorizedToDrive},
        ])

        cy.get('[data-cy=staffnew_create_btn]').click()
        cy.wait(3500)
        cy.get('[data-cy=da-check-duplicate]').should('not.exist')
        _helperContent.alert('p').contains("successfully created").should("be.visible")
    }

    context('Associate List', function () {
        beforeEach(() => {
            MenuHelper.Tab.goTo("Associate Management", '/da-management/dashboard', 'dashboard-tab')
        });

        it('Test - Add a New Associate', function () {
            AddNewDA(DeliveryAssociate)
        });


        it('Test - Add a New Associate (Supervisor)', function () {
            AddNewDA(DeliveryAssociate_Supervisor)
        });

    })
})
