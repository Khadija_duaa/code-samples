import ComponentHelper from "../../../support/Component.helper"
import MenuHelper from "../../../support/Menu.helper"
const {
  filters:{
      DeliveryAssociate_Filter
  }
} = Cypress.env('e2e');

describe("Associate Management",function(){
    context("Associate list", function(){
        beforeEach(function () {
          MenuHelper.Tab.goTo("Associate Management", '/da-management/dashboard', 'dashboard-tab')
          })
          it("Delete Associate",function(){
            new ComponentHelper('damanagement-options').actionButton('dalist-option').scrollIntoView().click()

            cy.scrollTo('top')

            let _helper = new ComponentHelper('da-list-content')

            cy.waitForLoaderToDisappear()

            _helper.actionButton("status-filter-sel").click()

            cy.get(".el-dropdown-menu:visible").contains("Inactive - Terminated").click({force: true})
            cy.get(".el-dropdown-menu:visible").invoke("hide")

            _helper.fillValue({selector: "search-in", type: "input", value: DeliveryAssociate_Filter})

            _helper.table().find("tr").contains(DeliveryAssociate_Filter).find(`[data-cy="da_table_row_dropdown"]`).scrollIntoView().click()

            let menuOp = {
                SendMessage: "Send Message",
                ViewDA: "View Associate",
                ChangeStatus: "Change Status"
            }
            cy.get('.el-dropdown-menu:visible').contains(menuOp.ViewDA).scrollIntoView().click()
            
            let staffdetailMenuOp = {
                Message: "Message",
                DAPreference: "Associate Preference",
                DeleteDA: "Delete Associate"
            }

            _helper = new ComponentHelper("staff-detail-container")
            _helper.actionButton("specific_da_dropdown").scrollIntoView().click()

            cy.get('.el-dropdown-menu:visible').contains(staffdetailMenuOp.DeleteDA).scrollIntoView().click()

            _helper.modal().find("input[type='text']").type("delete").wait(1000)
            _helper.modalButtonContain("Confirm").scrollIntoView().click()
            _helper.alert('p').contains("Associate deleted.").should("be.visible")
          })
    })
})