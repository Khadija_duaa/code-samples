describe('HERA-1146', () => {
    context('Attach files and add expiration date', () => {
        
        it('Validate attach files', function () {
            cy.get('[data-cy="da-management-nav"]').click({ multiple: true, force:true })
            cy.wait(4000);
            cy.get('[data-cy="onboarding-tab"]').click()
            cy.waitForLoaderToDisappear();
            cy.get('.el-dropdown-menu__item').contains('Message Associates').first().click({force:true})
            cy.wait(9000);
            cy.get('[data-cy="include-attachment-button"]').click()
            cy.wait(4000);
            cy.get('[role="dialog"]:visible').find('button').contains('Cancel').trigger('click')
        })
    })
})