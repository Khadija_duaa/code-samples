describe('HERA-1146', () => {
    context('Attach files and add expiration date', () => {
        
        it('Validate attach files', function () {
            cy.get('[data-cy="da-management-nav"]').click({ multiple: true, force:true })
            cy.wait(4000);
            cy.get('[data-cy="dalist-option"]').click() 
            cy.waitForLoaderToDisappear();
            cy.get('.el-dropdown-menu__item').contains('Message Associates').click({force:true})
            cy.wait(4000);
            cy.get('[data-cy="include-attachment-button"]').click()
            cy.wait(2000);
            cy.get('[data-cy="btn-cancel-staff-message-form"]').click()
            cy.wait(1000);
        })
    })
})