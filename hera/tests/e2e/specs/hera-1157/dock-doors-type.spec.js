let text = "Option Test 1"

describe('HERA-1157', () => {
    context('View dock doors type options', () => {
        
        it('View dock doors type options', function () {
            cy.get('[data-cy="user-info-dropdown"]').click()
            cy.wait(4000);
            cy.get('[data-cy="company-settings-option"]').click()
            cy.wait(4000);
            cy.get('[data-cy="custom-lists-tab"]').click()
            cy.wait(4000);
            cy.get('[data-cy="search-in"]').type('Dock Door')
            cy.get('[data-cy="three_dot_button_row"]').click({multiple:true, force:true})
            cy.wait(4000);
            cy.get('.el-dropdown-menu').contains('Edit this Custom List').scrollIntoView().click({force:true})
            cy.wait(4000);
            cy.get('button').contains('New Dropdown Option').click({force:true})
            cy.get('[data-cy="new-option-input"]').type(text)
            cy.wait(4000);
            cy.get('[data-cy="done-btn"]').click({force:true})
            cy.wait(4000);
            cy.get('[data-cy="drawer-save-btn"]').click({force:true})
            cy.wait(4000);
        })
    })
})