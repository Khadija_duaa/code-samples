import ComponentHelper from "../../support/Component.helper";
import MenuHelper from "../../support/Menu.helper";
import FunctionHelper from "../../support/Function.helper";

// https://team-1618446135883.atlassian.net/jira/software/c/projects/HERA/boards/5?modal=detail&selectedIssue=HERA-447
describe('HERA 932', () => {
  context('Go to Vehicle Maitenance Records', () => {

    it('Go to Vehicle Maintenance Records Reports and Standard Maintenance Vehicles: Fill form type', function () {
      MenuHelper.Tab.goTo("Reports", '/reports/')
        
      console.log("element selected", cy.get('[data-cy="vehicle-management"]'));
      
      cy.get('[data-cy="vehicle-management"] .link').contains('Vehicle Maintenance Records').click({force: true});
      cy.wait(4000);
      cy.location('pathname').should('eq', '/reports/vehicle-maintenance-records');

      let menuOp = {
        ViewVehicle: "View Vehicle",
        AddVehicleMaintenanceRecord: "Add Vehicle Maintenance Record"
      };

      cy.wait(5000);

      new ComponentHelper('maintenance_records_table').actionButton('three_dot_button_row').first().click({force: true});

      cy.get('.el-dropdown-menu__item').contains(menuOp.AddVehicleMaintenanceRecord).click({force: true});

      let __helper = new ComponentHelper('vehicle-maintenance-form');

      __helper.fillModalForm([
        { selector: "services-sel", type: "select", value: "Camera" },
        { selector: "location-sel", type: "select", value:  "Jiffy Lube - 123 Main Street" },
        { selector: "status-sel", type: "select", value: "Complete" },
        { selector: "accident-date-dpk", type: "input", value: "05/11/2022" },
        { selector: "mileage-in", type: "input", value: "115000" },
        { selector: "notes-in", type: "input", value: "Notas de prueba" },
      ]);

      cy.get('[data-cy=or-create-btn]').click({force:true});

    });

  })
})