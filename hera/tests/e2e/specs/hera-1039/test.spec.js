import ComponentHelper from "../../support/Component.helper"
import MenuHelper from "../../support/Menu.helper";

describe('Add Vehicle Type to Vehicles: (7) On the Vehicle Import wizard', function () {


        
    function deleteVehicle(vehicleName){
        MenuHelper.Tab.goTo("Vehicle Management","/vehicles/dashboard")
        cy.wait(5000)
        cy.get('[data-cy="vehicle-list"] > .el-radio-button__inner').click()
        cy.wait(2000) 
        cy.get('[data-cy=vi-search-in]').type(vehicleName)
        cy.get('body').find('.el-dropdown-menu').find('li').contains('View Vehicle').click({force:true})
        cy.wait(2000)
        cy.get('body').find('[data-cy="vd-edit-btn"]').click()
        cy.get('[role="dialog"]:visible').find('button').contains('Delete').click()
        cy.get('.el-message-box:visible').find('button').contains('Delete').click()
        cy.wait(5000)
    }

    it('Test Import Vehicle Form', function () {
        MenuHelper.Tab.goTo("Vehicle Management","/vehicles/dashboard")
        cy.wait(5000)
        cy.get('[data-cy="vehicle-list"] > .el-radio-button__inner').click()
        cy.wait(2000)
        cy.get('body').find('.el-dropdown-menu').find('li').contains('Import Vehicles').click({force:true})
        cy.wait(5000)
        let __helper = new ComponentHelper('vehicle-import-form')
        __helper.fillValue({type: 'file', selector: 'attach-file', value: 'Hera_Vehicle_Import_TemplateV2_Filled.csv'});
        cy.wait(10000)
        cy.get('body').find('[data-cy="submit-import-btn"]').click()
        cy.wait(5000)
    })
    
    it('Delete users created by import file', function () {
        deleteVehicle('Adabat')
        deleteVehicle('Holoska')
        deleteVehicle('Apotos')
        deleteVehicle('Mazuri')
        deleteVehicle('Shamar')
    })

})