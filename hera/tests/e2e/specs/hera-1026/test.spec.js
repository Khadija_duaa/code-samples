import MenuHelper from "../../support/Menu.helper"
import ComponentHelper from "../../support/Component.helper2"

describe('Add Vehicle Type to Vehicles: (3) On the DA detail page', function () {


    // let authorizedToDrive = [
    //     '10,000lbs Van',
    //     'Custom Delivery Van',
    //     'Standard Parcel',
    //     'Standard Parcel Extra Large',
    //     'Standard Parcel Large',
    //     'Standard Parcel Small',
    // ]

    it('Test Add DA Form', function () {
        cy.get('[href="/da-management"]').trigger('click')
        cy.wait(5000)
        cy.get('[data-cy="dalist-option"] > .el-radio-button__inner').click()
        cy.wait(2000)
        /*cy.get('[data-cy="dropdown-da-header-table"] > div').first().realMouseUp()
        cy.wait(1000) */
        //cy.get('body').find('[data-cy="add_da_option"]').click()
        cy.get(".el-dropdown-menu").contains("Add Associate").click({force:true})
        cy.get('[data-cy="da-status-sel"]').click()
        cy.get('.el-select-dropdown__wrap:visible').find('li').contains('Active').click()
        cy.get('[data-cy="da-first-name-in"]').type('Melon')
        cy.get('[data-cy="da-last-name-in"]').type('Usk')
        cy.get('[data-cy="da-email-in"]').type('MelonUsk@gmail.com')
        cy.get('[data-cy="da-authorized-to-drive-in"]').click()
        cy.get('.el-select-dropdown__wrap:visible').find('li').contains('10,000lbs Van').click()
        cy.get('[data-cy="staffnew_create_btn"]').click()
        cy.wait(5000)
    })

    it('Test Edit DA Form', function () {
        cy.visit('/da-management')
        cy.wait(10000)
        cy.get('[data-cy="dalist-option"] > .el-radio-button__inner').click()
        cy.wait(2000)
        cy.get('[data-cy="search-in"]').type('Melon')
        //cy.get('table > tbody').find('.el-tooltip.icon-button').click()
        //cy.wait(10000)
        cy.wait(4000)
        cy.get(".el-dropdown-menu").contains("View Associate").click({force:true})
        cy.scrollTo(0, 400)
        cy.get('[data-cy="edit-driving-info-btn"]').click()
        cy.wait(4000)
        cy.get('[data-cy="da-authorized-to-drive-in"]').click()
        cy.get('.el-select-dropdown__wrap:visible').find('li').contains('Standard Parcel Large').click()
        cy.get('[role="dialog"]:visible').find('button').contains('Update').click()
        cy.wait(5000)
    })

    it('Delete user created Melon Usk', function () {
        cy.visit('/da-management')
        cy.wait(5000)
        cy.get('[data-cy="dalist-option"] > .el-radio-button__inner').click()
        cy.wait(2000)
        cy.get('[data-cy="search-in"]').type('Melon')
        //cy.get('table > tbody').find('.el-tooltip.icon-button').click()
        //cy.scrollTo(0, 0)
        cy.get(".el-dropdown-menu").contains("View Associate").click({force:true})
        cy.get('[data-cy="specific_da_dropdown"]').realMouseUp()
        cy.get('body').find('[data-cy="specific_da_delete_option"]').click()
        cy.get('[role="dialog"]:visible').find('input').type('delete')
        cy.get('[role="dialog"]:visible').find('button').contains('Confirm').click()
        cy.wait(5000)
    })

})