import ComponentHelper from "../../support/Component.helper";
import MenuHelper from "../../support/Menu.helper";

describe('HERA 865', () => {
    context('Onboarding Card Changes in Associate Record', () => {

        it('Standard Vehicles: Fill form type', () => {
          MenuHelper.Tab.goTo("Vehicle Management", '/vehicles/dashboard')

          cy.wait(4000)

          cy.get('[data-cy=vehicle-list] > .el-radio-button__inner').click({force:true})
          cy.get(':nth-child(1) > .el-table_14_column_32 > .cell > .flex > [data-cy=three_dot_button_row] > .context-menu').click();
          cy.get('.el-dropdown-menu').contains("Add Odometer Reading").first().click({force:true})
           let __helper = new ComponentHelper('odometer_reading_form')
          __helper.fillModalForm([
            { selector: "mileage_in", type: "input", value: "112000" },
            { selector: "odometerDate_dpk", type: "input", value:  "04/25/2022" },
            { selector: "time_tpk", type: "input", value: "18:00" },
            { selector: "odometerType_sel", type: "select", value: "Other" }
          ])

          cy.get('[data-cy="or-create-btn"]').click()
          cy.wait(4000)
          cy.scrollTo('top')
        
        })
    })
})

