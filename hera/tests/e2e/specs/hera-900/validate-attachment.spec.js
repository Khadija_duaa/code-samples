let date = "10/05/2022"

describe('HERA-900', () => {
    context('Attach files and add expiration date', () => {
        
        it('Validate attach files', function () {
            cy.get('[data-cy="messenger-indicator-btn"]').click()
            cy.wait(4000);
            cy.scrollTo('bottom',{ensureScrollable:false}) 
            cy.wait(4000);
            cy.get('.conversations > :nth-child(1)').click({force:true})
            cy.wait(4000);
            cy.get('[data-cy="attachment-button"]').click()
            cy.wait(4000);
            cy.get('[data-cy="expiration-date-input"]').click().type(date)
            cy.get('body').find('[role="dialog"]:visible').find('button').contains('Cancel').click({force:true})   
        })
    })
    
})