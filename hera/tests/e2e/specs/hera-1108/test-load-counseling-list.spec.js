describe('Perfomance for Recent Counseling Not Sent & Pending Signature', function () {
    it('Test Counseling List load', function () {
        cy.get('a[href="/performance-and-coaching"]').should("be.visible").realClick()
        cy.location("pathname").should("eq","/performance-and-coaching/performance")
        cy.get('[data-cy="pc-tab-control-rad"]').contains("Counselings").click({force:true})
        cy.location("pathname").should("eq","/performance-and-coaching/counselings")
    })
})