describe('HERA-1349', () => {
    context('Associete photo', () => {
        beforeEach(function() {
            cy.loginByUI(Cypress.env("cognito_username"), Cypress.env("cognito_password"));
        });
        
        it('Associete photo', function () {
            cy.get('[data-cy="da-management-nav"]').click({ multiple: true, force: true })
            cy.wait(10000);
            cy.get('[data-cy="dalist-option"]').click()
            cy.wait(10000);
            cy.get('[data-cy="da_table_row_dropdown"]').first().click()
            cy.wait(10000);
            cy.get('[data-cy="da_table_row_option_view"]').first().click({ force: true })
            cy.wait(10000);
            cy.get('[data-cy="select-photo"]').attachFile('profile.jpg')
            cy.wait(10000);
            cy.get('[data-cy="da-management-nav"]').click({ multiple: true, force: true })
            cy.wait(10000);
            cy.get('[data-cy="dalist-option"]').click()
            cy.wait(10000);
            cy.get('[data-cy="da_table_row_dropdown"]').first().click()
            cy.wait(10000);
            cy.get('[data-cy="da_table_row_option_view"]').first().click({ force: true })
        })
    })
})