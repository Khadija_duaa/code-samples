
import MenuHelper from '../../../../../support/Menu.helper'

const actions = {
    input: (selector, value) => {

        cy.get(`[data-cy="${selector}"]:visible`)
            .then(e=>{

                let cyEl;

                if(e.prop('tagName').toLowerCase()==='input'){

                    cyEl = cy.get(e)

                }else{

                    cyEl = cy.get(e).find('input')
                    
                }

                cyEl.click({force:true}).type(`{selectAll}{del}${value}`, { force: true })
                
                cy.get("@settings_search").realClick()

            })

    },
    inputNumber: (selector, value) => {
        cy.get(`[data-cy="${selector}"]:visible input`)
            .click({force:true})
            .type(`{selectAll}{del}${value}`, { force: true })
            .blur({force:true})
        cy.get("@settings_search").realClick()
    },
    switch: (selector) => {
        cy.get(`[data-cy="${selector}"]:visible`)
            .click({force:true})
            .blur({force:true})
    },
    slider: (selector, xPercent) =>  cy.get(`[data-cy="${selector}"]:visible .el-slider__bar`).then(slider=>{
        let posX = Math.floor(slider.width()*xPercent)/100
        let posY = Math.floor(slider.height()/2)
        cy.wrap(slider).click(posX, posY, {force:true})
    })
}

describe('Tasks & Reports', function () {
    context('Reports > PERFORMANCE & COACHING > DA Performance Report', function () {
        beforeEach(() => {
            cy.loginByUI(
                Cypress.env('cognito_username'),
                Cypress.env('cognito_password')
            );
            MenuHelper.Tab.goTo("Tasks & Reports","/reports")
            cy.hideCookieInfo();
            // cy.hideChatInfo()
        });

        it('Edit Setting button > Coaching > WEEKLY PERFORMANCE DATA', function () {

            cy.get('[data-cy="performance-coaching"] .link').contains('DA Performance').click()

            cy.location('pathname').should('eq', '/performance-and-coaching/da-performance-report')

            cy.get('[data-cy="da-performance-report-list"] [data-cy="da-pr-3dot-btn"]').click()

            let _3dotOp = {
                Editsettings: "Edit Settings"
            }

            cy.get('.el-dropdown-menu:visible').contains(_3dotOp.Editsettings).click()

            let setttings = {
                AccountDetails: "Account Details",
                Invoices: "Invoices",
                CompanyDetails: "Company Details",
                Users: "Users",
                Coaching: "Coaching",
                MessageLogs: "Message Logs",
                DropDownLists: "Drop-Down Lists"
            }

            cy.get('[data-cy="setting-ops"]').contains(setttings.Coaching).click()

            cy.location('pathname').should('eq', '/settings/driver-feedback')


            cy.wait(3000)

            let find = {
                first: "FICO® Score"
            }
            
            cy.get('[data-cy="settings-coaching-form"] [data-cy="df-settings-search-in"]').as("settings_search").click({force:true}).type(find.first, { force: true })

            cy.wrap([
                {selector: "df-issues-slider", type: "slider", value: 30 },
                {selector: "toggle-issue-in", type: "input", value: 150 },
                {selector: "toggle-issue-sw", type: "switch"},
                {selector: "toggle-co-in", type: "input", value: 100 },
                {selector: "toggle-co-sw", type: "switch"},
                {selector: "df-kudo-in", type: "input", value: 125 },
                {selector: "df-kudo-swch", type: "switch"},
                {selector: "df-pr-in", type: "input", value: 200 },
                {selector: "df-pr-swch", type: "switch"}
            ]).each(e => {
                actions[e.type](e.selector, e.value)
                cy.wait(3000)
            })

            cy.wait(3000)

            // cy.wrap([
            //     {selector: "df-issues-slider", type: "slider", value: 100 },
            //     {selector: "toggle-issue-in", type: "input", value: 750 },
            //     {selector: "toggle-issue-sw", type: "switch"},
            //     {selector: "toggle-co-in", type: "input", value: 800 },
            //     {selector: "toggle-co-sw", type: "switch"},
            //     {selector: "df-kudo-in", type: "input", value: 825 },
            //     {selector: "df-kudo-swch", type: "switch"},
            //     {selector: "df-pr-in", type: "input", value: 800 },
            //     {selector: "df-pr-swch", type: "switch"}
            // ]).each(e => {
            //     actions[e.type](e.selector, e.value)
            //     cy.wait(3000)
            // })

        
        });
        it('Edit Setting button > Coaching > DAILY PERFORMANCE DATA', function () {

            cy.get('[data-cy="performance-coaching"] .link').contains('DA Performance').click()

            cy.location('pathname').should('eq', '/performance-and-coaching/da-performance-report')

            cy.get('[data-cy="da-performance-report-list"] [data-cy="da-pr-3dot-btn"]').click()

            let _3dotOp = {
                Editsettings: "Edit Settings"
            }

            cy.get('.el-dropdown-menu:visible').contains(_3dotOp.Editsettings).click()

            let setttings = {
                AccountDetails: "Account Details",
                Invoices: "Invoices",
                CompanyDetails: "Company Details",
                Users: "Users",
                Coaching: "Coaching",
                MessageLogs: "Message Logs",
                DropDownLists: "Drop-Down Lists"
            }

            cy.get('[data-cy="setting-ops"]').contains(setttings.Coaching).click()

            cy.location('pathname').should('eq', '/settings/driver-feedback')


            cy.wait(3000)

            let find = {
                first: "DAILY FICO"
            }
            
            cy.get('[data-cy="settings-coaching-form"] [data-cy="df-settings-search-in"]').as("settings_search").click({force:true}).type(find.first, { force: true })


            cy.wrap([
                {selector: "df-mnm-issue-in", type: "input", value: 10 },//off
                {selector: "df-mnm-issue-sw", type: "switch"},
                {selector: "df-mnm-issue-slider", type: "slider", value: 90 },
                {selector: "df-mnm-kudo-in", type: "input", value: 20 },//off
                {selector: "df-mnm-kudo-sw", type: "switch"},
                {selector: "df-mnm-kudo-slider", type: "slider", value: 70 },
                {selector: "df-mnm-co-in", type: "input", value: 750 },
                {selector: "df-mnm-co-sw", type: "switch"},
                {selector: "df-mnm-pr-in", type: "input", value: 30 },
                {selector: "df-mnm-pr-sw", type: "switch"},
            ]).each(e => {
                actions[e.type](e.selector, e.value)
                cy.wait(3000)
            })

            cy.wait(3000)

            // cy.wrap([
            //     {selector: "df-mnm-issue-in", type: "input", value: 0 },//off
            //     {selector: "df-mnm-issue-sw", type: "switch"},
            //     {selector: "df-mnm-issue-slider", type: "slider", value: 80 },
            //     {selector: "df-mnm-kudo-in", type: "input", value: 0 },//off
            //     {selector: "df-mnm-kudo-sw", type: "switch"},
            //     {selector: "df-mnm-kudo-slider", type: "slider", value: 80 },
            //     {selector: "df-mnm-co-in", type: "input", value: 750 },
            //     {selector: "df-mnm-co-sw", type: "switch"},
            //     {selector: "df-mnm-pr-in", type: "input", value: 0 },
            //     {selector: "df-mnm-pr-sw", type: "switch"},
                
            // ]).each(e => {
            //     actions[e.type](e.selector, e.value)
            //     cy.wait(3000)
            // })

        
        });

        it('Edit Setting button > Coaching > CT DEVELOPERS!\'S CUSTOM DA ISSUES & KUDOS', function () {

            cy.get('[data-cy="performance-coaching"] .link').contains('DA Performance').click()

            cy.location('pathname').should('eq', '/performance-and-coaching/da-performance-report')

            cy.get('[data-cy="da-performance-report-list"] [data-cy="da-pr-3dot-btn"]').click()

            let _3dotOp = {
                Editsettings: "Edit Settings"
            }

            cy.get('.el-dropdown-menu:visible').contains(_3dotOp.Editsettings).click()

            let setttings = {
                AccountDetails: "Account Details",
                Invoices: "Invoices",
                CompanyDetails: "Company Details",
                Users: "Users",
                Coaching: "Coaching",
                MessageLogs: "Message Logs",
                DropDownLists: "Drop-Down Lists"
            }

            cy.get('[data-cy="setting-ops"]').contains(setttings.Coaching).click()

            cy.location('pathname').should('eq', '/settings/driver-feedback')


            cy.wait(3000)

            let find = {
                first: "Vehicle Damage",
                second: "Rescuer"
            }
            
            //first

            cy.get('[data-cy="settings-coaching-form"] [data-cy="df-settings-search-in"]').as("settings_search").click({force:true}).type(find.first, { force: true })


            cy.wrap([
                {selector: "df-custom-issue-slider", type: "slider", value: 50 }
            ]).each(e => {
                actions[e.type](e.selector, e.value)
                cy.wait(3000)
            })

            cy.wait(3000)

            cy.wrap([
                {selector: "df-custom-issue-slider", type: "slider", value: 0 }
            ]).each(e => {
                actions[e.type](e.selector, e.value)
                cy.wait(3000)
            })

            //second
            
            cy.get('[data-cy="settings-coaching-form"] [data-cy="df-settings-search-in"]').as("settings_search").click({force:true}).type(`{selectAll}${find.second}`, { force: true })


            cy.wrap([
                {selector: "df-custom-kudo-slider", type: "slider", value: 90 }
            ]).each(e => {
                actions[e.type](e.selector, e.value)
                cy.wait(3000)
            })

            cy.wait(3000)

            // cy.wrap([
            //     {selector: "df-custom-kudo-slider", type: "slider", value: 50 }
            // ]).each(e => {
            //     actions[e.type](e.selector, e.value)
            //     cy.wait(3000)
            // })

        });
    })
})
