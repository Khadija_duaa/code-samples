import ComponentHelper from "../../../../support/Component.helper2";
import {ImportWeeklyPerformanceData} from '/e2e-cypress.config.js'
describe('Performance & Coaching', function () {
    context('Performance Tracking', function () {
        beforeEach(() => {
            cy.loginByUI(
                Cypress.env('cognito_username'),
                Cypress.env('cognito_password')
            );
            cy.get('a[href="/performance-and-coaching"]').scrollIntoView().click()
            cy.location('pathname').should('eq', '/performance-and-coaching');
            cy.hideCookieInfo();
            // cy.hideChatInfo()
        });

        it('Imported performance data / Import performance data', function () {
            let __helper = new ComponentHelper('imported-performance-data-container');
            __helper.actionButton('import-performance-data').scrollIntoView().click()
            cy.location('pathname').should('eq', '/performance-and-coaching/weekly-performance-data-import');
            __helper = new ComponentHelper('weekly-data-import')
            __helper.fillValue({type: 'file', selector: 'attach-file', value: ImportWeeklyPerformanceData.File});
            __helper.fillValue({type: 'select', selector: 'filetype', value: ImportWeeklyPerformanceData.Type});
            __helper.fillValue({type: 'select', selector: 'week', value: ImportWeeklyPerformanceData.Week});
            // cy.scrollTo('bottom')
            __helper.actionButtonContain('Upload All').scrollIntoView().click()
            cy.log('Waiting for result');
            cy.wait(4000);
            __helper.actionButtonContain('Continue').scrollIntoView().click()
            cy.wait(4000);
            __helper.actionButtonContain('Import').scrollIntoView().click()
        });
    })
})