describe('Performance & Coaching / Most recent DA Issues (card)',function () {
    context('View DA Issue > Delete DA Issue',function () {
        beforeEach(function () {
            //$message.success
            
            cy.loginByUI(
                Cypress.env('cognito_username'),
                Cypress.env('cognito_password')
            )

            cy.get('a[href="/performance-and-coaching"]').should("be.visible").realClick()

            //To make sure the url is in /performance-and-coaching
            cy.location("pathname").should("eq","/performance-and-coaching/performance")
            cy.hideCookieInfo();
            
        })

        it('Test - Delete DA issue',function () {

            let tabs = {
                PerformanceTracking: "Performance Tracking",
                AutomatedCoaching: "Automated Coaching",
                DAKudosIssues: "Associate Kudos & Issues",
                Counselings: "Counselings"
            }

            cy.get('[data-cy="pc-tab-control-rad"]').contains(tabs.DAKudosIssues).click()

            let menuOp = {
                ViewDAIssue:"View Associate Issue",
                ViewCounseling: "View Linked Counseling",
                ViewDA: "View Associate"
            }

            let first = (cyEl) => cyEl.then(x => x.length ? cy.wrap(x.length > 1 ? x.first() : x ).scrollIntoView() : null )

            first(cy.get('[data-cy="da-issue-list-card"] [data-cy="da-row-3dot-btn"]')).click().get('.el-dropdown-menu:visible').contains(menuOp.ViewDAIssue).click()

            let menuOpDetail = {
                Edit:"Edit",
                Delete: "Delete",
                Appeal: "Appeal",
                CreateCounseling: "Create Counseling"
            }

            cy.get('[data-cy="da-issue-detail-head-3dot-btn"]').click().get('.el-dropdown-menu:visible').contains(menuOpDetail.Delete).click()

            cy.wait(1000)

            cy.get('[data-cy="btn-issue-delete"]').click()
            
            cy.wait(1500)

            cy.get('[role="alert"]').then( () => assert(true, '**DA Issue deleted successfully:** the expected **"DA Issue"** was deleted') )
            
            cy.wait(1500)

        })

    })
    

})