import ComponentHelper from "../../../../support/Component.helper2"
const {
    performance_coaching: {
        view_da_issue: {
            view_da_issue: {
                DAIssueInformation_ED
            }
        }
    }
} = Cypress.env('e2e');

describe('Performance & Coaching / Most recent DA Issues (card)',function () {
    context('View DA Issue > Edit DA Issue',function () {
        beforeEach(function () {
            
            cy.loginByUI(
                Cypress.env('cognito_username'),
                Cypress.env('cognito_password')
            )

            cy.get('a[href="/performance-and-coaching"]').should("be.visible").realClick()

            //To make sure the url is in /performance-and-coaching
            cy.location("pathname").should("eq","/performance-and-coaching/performance")
            cy.hideCookieInfo();
            
        })

        it('Test - Edit DA issue',function () {

            let tabs = {
                PerformanceTracking: "Performance Tracking",
                AutomatedCoaching: "Automated Coaching",
                DAKudosIssues: "Associate Kudos & Issues",
                Counselings: "Counselings"
            }

            cy.get('[data-cy="pc-tab-control-rad"]').contains(tabs.DAKudosIssues).click()

            let menuOp = {
                ViewDAIssue:"View Associate Issue",
                ViewCounseling: "View Counseling",
                ViewDA: "View Associate"
            }

            let first = (cyEl) => cyEl.then(x => x.length ? cy.wrap(x.length > 1 ? x.first() : x ).scrollIntoView() : null )

            first(cy.get('[data-cy="da-issue-list-card"] [data-cy="da-row-3dot-btn"]')).click().get('.el-dropdown-menu:visible').contains(menuOp.ViewDAIssue).click()

            let menuOpDetail = {
                Edit:"Edit",
                Delete: "Delete",
                Appeal: "Appeal",
                CreateCounseling: "Create Counseling"
            }

            cy.get('[data-cy="da-issue-detail-head-3dot-btn"]').click().get('.el-dropdown-menu:visible').contains(menuOpDetail.Edit).click()

            cy.wait(1000)

            let _helper =  new ComponentHelper('da-issue-information-form')

            cy.wait(1000).then(()=>{
                if(Cypress.$(`i[title="Delete POD"].uil-trash-alt`).length){
                    _helper.container(`i[title="Delete POD"].uil-trash-alt`).scrollIntoView().click()
                    _helper.modal().contains("Delete").scrollIntoView().click()
                }
            })

            _helper.fillForm([
                {type: 'select', selector: 'da-issue-type-sel', value: DAIssueInformation_ED.Type, ttw: 300},
                {type: 'input', selector: 'date-of-occurence-dpk', value: DAIssueInformation_ED.DateofOccurrence},
                {type: 'input', selector: 'time-in', value: DAIssueInformation_ED.Time},
                {type: 'input', selector: 'additional-info-in', value: DAIssueInformation_ED.AdditionalInformation},
                {type: 'select', selector: 'av-di-type-sel', value: DAIssueInformation_ED.Violation_Defect_Type, ttw: 300},
                {type: 'input', selector: 'station-id-in', value: DAIssueInformation_ED.Violation_Defect_Station},
                {type: 'input', selector: 'tracking-number-in', value: DAIssueInformation_ED.Violation_Defect_TrackingNumber},
                {type: 'select', selector: 'infraction-type-form-sel', value:DAIssueInformation_ED.Violation_Defect_InfractionType},
                {type: 'file', selector: 'picture-of-delivery-up', value:DAIssueInformation_ED.Violation_Defect_PictureOfDelivery, ttw: 300},
                {type: 'input', selector: 'pick-a-day-dpk', value:DAIssueInformation_ED.AppealInformation_Date},
                {type: 'input', selector: 'resolved-date-dpk', value:DAIssueInformation_ED.AppealInformation_ResolvedDate},
                {type: 'select', selector: 'appeal-status-sel', value:DAIssueInformation_ED.AppealInformation_Status, ttw: 300},
                {type: 'input', selector: 'appeal-notes-txa', value:DAIssueInformation_ED.AppealInformation_Notes},
            ])

            _helper.actionButton('da-issue-create-btn').click()
            
            cy.wait(1500)

            cy.get('[role="alert"]').then( () => assert(true, '**DA Issue updated successfully:** the expected **"DA Issue"** was edited') )
            
            cy.wait(1500)
        })

    })
    

})