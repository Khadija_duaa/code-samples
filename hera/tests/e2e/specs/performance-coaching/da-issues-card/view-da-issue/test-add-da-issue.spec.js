import ComponentHelper from "../../../../support/Component.helper"
const {
    performance_coaching: {
        view_da_issue: {
            view_da_issue: {
                DAIssueInformation
            }
        }
    }
} = Cypress.env('e2e');

describe('Performance & Coaching / Most recent DA Issues (card)',function () {
    context('Add DA Issue',function () {
        beforeEach(function () {
            
            cy.loginByUI(
                Cypress.env('cognito_username'),
                Cypress.env('cognito_password')
            )

            cy.get('a[href="/performance-and-coaching"]').should("be.visible").realClick()

            //To make sure the url is in /performance-and-coaching
            cy.location("pathname").should("eq","/performance-and-coaching/performance")
            cy.hideCookieInfo();
            
        })

        it('Test - add a new Associate issue',function () {

            let tabs = {
                PerformanceTracking: "Performance Tracking",
                AutomatedCoaching: "Automated Coaching",
                DAKudosIssues: "Associate Kudos & Issues",
                Counselings: "Counselings"
            }

            cy.get('[data-cy="pc-tab-control-rad"]').contains(tabs.DAKudosIssues).click()

            let menuOp = {
                AddDAIssue: "Add Associate Issue",
                ViewAllDAIssues: "View All Associate Issues"
            }

            cy.get('[data-cy="da-issue-list-card"] [data-cy="da-head-3dot-btn"]').scrollIntoView().click().get('.el-dropdown-menu:visible').contains(menuOp.AddDAIssue).click()

            cy.wait(1000)

            let _helper =  new ComponentHelper('da-issue-information-form')

            _helper.fillForm([
                {type: 'select', selector: 'da-issue-type-sel', value: DAIssueInformation.Type, ttw: 300},
                {type: 'select', selector: 'da-name-sel', value: DAIssueInformation.DAName, ttw: 300},
                {type: 'input', selector: 'date-of-occurence-dpk', value: DAIssueInformation.DateofOccurrence},
                {type: 'input', selector: 'time-in', value: DAIssueInformation.Time},
                {type: 'input', selector: 'additional-info-in', value: DAIssueInformation.AdditionalInformation},
                {type: 'select', selector: 'av-di-type-sel', value: DAIssueInformation.Violation_Defect_Type, ttw: 300},
                {type: 'input', selector: 'station-id-in', value: DAIssueInformation.Violation_Defect_Station},
                {type: 'input', selector: 'tracking-number-in', value: DAIssueInformation.Violation_Defect_TrackingNumber},
                {type: 'select', selector: 'infraction-type-form-sel', value:DAIssueInformation.Violation_Defect_InfractionType},
                {type: 'file', selector: 'picture-of-delivery-up', value:DAIssueInformation.Violation_Defect_PictureOfDelivery, ttw: 300},
                {type: 'input', selector: 'pick-a-day-dpk', value:DAIssueInformation.AppealInformation_Date},
                {type: 'input', selector: 'resolved-date-dpk', value:DAIssueInformation.AppealInformation_ResolvedDate},
                {type: 'select', selector: 'appeal-status-sel', value:DAIssueInformation.AppealInformation_Status, ttw: 300},
                {type: 'input', selector: 'appeal-notes-txa', value:DAIssueInformation.AppealInformation_Notes},
            ])

            _helper.actionButton('da-issue-create-btn').click()
            cy.wait(1500)
            
            cy.get('[role="dialog"] button').contains('Yes').scrollIntoView().click()

            cy.get('[role="alert"]').then( () => assert(true, '**DA Issue created successfully:** the expected **"DA Issue"** was created') )
            
            cy.wait(1500)
        })

    })
    

})