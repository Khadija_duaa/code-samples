import FunctionHelper from "../../support/Function.helper";
import ComponentHelper from "../../support/Component.helper2";
import MenuHelper from "../../support/Menu.helper";

function goToCompanySettings(){
    MenuHelper.Top.selectFromUser("Company Settings")
    let setttings = {
        AccountDetails: "Account Details",
        Invoices: "Invoices",
        CompanyDetails: "Company Details",
        Users: "Users",
        Coaching: "Coaching",
        Counseling: "Counseling",
        MessageLogs: "Message Logs",
        DropDownLists: "Drop-Down Lists"
    }
    cy.wait(2000)
    cy.get('[data-cy="setting-ops"]').contains(setttings.Counseling).click()
}

context("HERA-1025", function () {

    it("Add a new Counseling Template", function () {
        goToCompanySettings()
        let _helper = new ComponentHelper("counseling-templates")
        cy.get('[data-cy="three_dotted_menu"]').click({multiple:true, force:true})
        cy.wait(4000);
        cy.get('.el-dropdown-menu').contains('Create new Counseling Template').scrollIntoView().click({force:true})
        cy.wait(4000);

        _helper.fillForm([
            {selector: "template-name-in", type: "input", value: 'HERA-20251'},
            {selector: "counseling-notes-in", type: "input", value: 'counseling-notes'},
            {selector: "prior-discussion-summary-in", type: "input", value: 'prior-discussion-summary'},
            {selector: "corrective-action-summary-in", type: "input", value: 'corrective-action-summary'},
            {selector: "consequences-of-failure-in", type: "input", value: 'consequences-of-failure'},
        ])
        _helper.actionButton('drawer-save-btn').scrollIntoView().click()
        cy.wait(4000)
    })

    it("Try adding a new Counseling Template with existing name", function () {
        goToCompanySettings()
        let _helper = new ComponentHelper("counseling-templates")
        cy.get('[data-cy="three_dotted_menu"]').click({multiple:true, force:true})
        cy.wait(4000);
        cy.get('.el-dropdown-menu').contains('Create new Counseling Template').scrollIntoView().click({force:true})
        cy.wait(4000);
        _helper.fillForm([
            {selector: "template-name-in", type: "input", value: 'HERA-2025'}
        ])
        cy.get('.el-form-item__error:visible').contains('Template name already exists')
        cy.wait(4000)
        _helper.actionButton('drawer-cancel-btn').scrollIntoView().click()
    })

    it("Show Pre-Fill from template selector menu for Counselings", function () {
        MenuHelper.Top.selectFromCreate("Counseling")
        let _helper = new ComponentHelper("counseling-content")
        _helper.fillValue({selector: "counseling-templates-sel", type: "select", value: 'HERA-2025'})
        cy.wait(2000)
    })

    it("Edit Counseling Template", function () {
        goToCompanySettings()
        let _helper = new ComponentHelper("counseling-templates")
        cy.get('[data-cy="three_dotted_menu"]').click({multiple:true, force:true})
        cy.wait(4000);
        cy.get('.el-dropdown-menu').contains('Edit').scrollIntoView().click({force:true})
        cy.wait(4000);

        _helper.fillForm([
            {selector: "template-name-in", type: "input", value: 'HERA-2025 (Edited)'},
            {selector: "counseling-notes-in", type: "input", value: 'counseling-notes (Edited)'},
            {selector: "prior-discussion-summary-in", type: "input", value: 'prior-discussion-summary (Edited)'},
            {selector: "corrective-action-summary-in", type: "input", value: 'corrective-action-summary (Edited)'},
            {selector: "consequences-of-failure-in", type: "input", value: 'consequences-of-failure (Edited)'},
        ])
        _helper.actionButton('drawer-save-btn').scrollIntoView().click()
        cy.wait(4000)
    })

    it("Delete Counseling Template", function () {
        goToCompanySettings()
        let _helper = new ComponentHelper("counseling-templates")
        cy.get('[data-cy="three_dotted_menu"]').click({multiple:true, force:true})
        cy.wait(4000);
        cy.get('.el-dropdown-menu').contains('Delete').scrollIntoView().click({force:true})
        cy.wait(4000);
        _helper.modalButton('dialog-delete-btn').scrollIntoView().click()
        cy.wait(4000)
    })

})