import ComponentHelper from "../../../support/Component.helper2";
import MenuHelper from '../../../support/Menu.helper'
const {
    user_settings: {
        manage_devices: {
            Device
        }
    }
} = Cypress.env('e2e');

describe('User Menu', function () {

    context('Manage Devices', function () {
        beforeEach(() => {
            MenuHelper.Top.selectFromUser("Manage Devices")
            cy.location('pathname').should('eq', '/devices');
        });

        it('Test - Add Device', function () {
            let __helper = new ComponentHelper('device-list')
            __helper.actionButton("dl-add-device-btn").scrollIntoView().click()
            __helper.fillModalForm([
                {selector: "df-device-name-in", type: "input", value: Device.Name},
                {selector: "cf-status-sel", type: "select", value: Device.Status},
                {selector: "cf-phone-number-in", type: "input", value: Device.Phone},
                {selector: "cf-carrier-sel", type: "select", value: Device.Carrier},
                {selector: "df-notes-in", type: "input", value: Device.Notes},
            ])
            __helper.modalButtonContain("Confirm").scrollIntoView().click()
        });
    })
})
