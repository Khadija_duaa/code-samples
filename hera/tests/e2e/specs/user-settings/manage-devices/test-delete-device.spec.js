import ComponentHelper from "../../../support/Component.helper2";
import MenuHelper from '../../../support/Menu.helper'
const {
    filters:{
        Device_Filter
    }
} = Cypress.env('e2e');

describe('User Menu', function () {

    context('Manage Devices', function () {
        beforeEach(() => {
            MenuHelper.Top.selectFromUser("Manage Devices")
            cy.location('pathname').should('eq', '/devices');
        });

        it('Test - Delete Device', function () {

            let __helper = new ComponentHelper('device-list')

            __helper.fillValue({selector: "dl-search-in", type: "input", value: Device_Filter})
            __helper.table().find("tr").contains(Device_Filter).then(() => {
                cy.get(`[data-cy="dl-dropdown-device-btn"]`).first().click({force: true})
            })
            cy.get(".el-dropdown-menu").contains("Edit Device").click({force: true})

            __helper.modalButtonContain("Delete").then(($button) => {
                if ($button.prop('disabled')) {
                  assert(1, "Cannot delete this device!");
                } else {
                    cy.get(".el-message-box").contains("Delete").scrollIntoView().click()
                }
              });
        });
    })
})
