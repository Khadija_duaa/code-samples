import ComponentHelper from "../../../support/Component.helper2";
import MenuHelper from '../../../support/Menu.helper'
const {
    filters:{
        Device_Filter
    },
    user_settings: {
        manage_devices: {
            Device_ED
        }
    }
} = Cypress.env('e2e');

describe('User Menu', function () {

    context('Manage Devices', function () {
        beforeEach(() => {
            MenuHelper.Top.selectFromUser("Manage Devices")
            cy.location('pathname').should('eq', '/devices');
        });

        it('Test - Edit Device', function () {
            let __helper = new ComponentHelper('device-list')

            __helper.fillValue({selector: "dl-search-in", type: "input", value: Device_Filter})

            __helper.table().find("tr").contains(Device_Filter).then(() => {
                cy.get(`[data-cy="dl-dropdown-device-btn"]`).first().click({force: true})
            })
            cy.get(".el-dropdown-menu").contains("Edit Device").click({force: true})

            __helper.fillModalForm([
                {selector: "df-device-name-in", type: "input", value: Device_ED.Name},
                {selector: "cf-status-sel", type: "select", value: Device_ED.Status},
                {selector: "cf-phone-number-in", type: "input", value: Device_ED.Phone},
                {selector: "cf-carrier-sel", type: "select", value: Device_ED.Carrier},
                {selector: "df-notes-in", type: "input", value: Device_ED.Notes},
            ])
            __helper.modalButtonContain("Update").scrollIntoView().click()
            __helper.alert('p').contains("Device Updated").should("be.visible")
        });
    })
})
