
import ComponentHelper from '../../../../support/Component.helper2'
import MenuHelper from '../../../../support/Menu.helper'
import StripeHelper from '../../../../support/Stripe.helper'
const {
    user_settings: {
        account_details: {
            SubscriptionPlan
        }
    }
} = Cypress.env('e2e');

describe("User Menu / Company Settings", function () {
    context("Account Details", function () {
        beforeEach(function () {
            MenuHelper.Top.selectFromUser("Company Settings")

            let setttings = {
                AccountDetails: "Account Details",
                Invoices: "Invoices",
                CompanyDetails: "Company Details",
                Users: "Users",
                Coaching: "Coaching",
                MessageLogs: "Message Logs",
                DropDownLists: "Drop-Down Lists"
            }

            cy.get('[data-cy="setting-ops"]').contains(setttings.AccountDetails).click()

        })

        it("Test - Change Subscription Plan", function () {
            let _helper = new ComponentHelper("ad-subscription-plan-card")
            cy.wait(300).then(()=>{
                if(
                    !Cypress.$("[data-cy='hera-standard-card']:visible").length &&
                    Cypress.$("[data-cy='plan-edit-btn']:visible").length
                ){
                    _helper.actionButton("plan-edit-btn").scrollIntoView().click({force: true})
                }else{
                    _helper.actionButton("hera-standard-card").scrollIntoView().click()
                }
            })

            _helper.actionButton("f-performance-coaching").scrollIntoView().click()
            _helper.actionButton("f-daily-rostering").scrollIntoView().click()
            _helper.actionButton("f-da-management").scrollIntoView().click()
            _helper.actionButton("f-vechicle-management").scrollIntoView().click()

            _helper.actionButton("step1-continue-btn").scrollIntoView().click()
            _helper.actionButton("step2-continue-btn").scrollIntoView().click()

            _helper.fillForm([
                {selector: "stripe-first-name-in", type: "input", value: SubscriptionPlan.FirstName},
                {selector: "stripe-last-name-in", type: "input", value: SubscriptionPlan.LastName},
                {selector: "stripe-email-in", type: "input", value: SubscriptionPlan.Email},
                {selector: "stripe-address-in", type: "input", value: SubscriptionPlan.Address},
                {selector: "stripe-city-in", type: "input", value: SubscriptionPlan.City},
                {selector: "stripe-state-in", type: "input", value: SubscriptionPlan.State},
                {selector: "stripe-zip-in", type: "input", value: SubscriptionPlan.Zip},
            ])

            StripeHelper.getCardNumber("stripe-card-number-in").type(SubscriptionPlan.CardNumber).wait(1000)
            StripeHelper.getCardExpiry("stripe-card-expiry-in").type(SubscriptionPlan.Expiration).wait(1000)
            StripeHelper.getCardCvc("stripe-card-cvc-in").type(SubscriptionPlan.CVC).wait(1000)

            _helper.fillValue({selector: "stripe-authorization-chk", type: "check", value:"true", ttw:2000})

            _helper.actionButton("step3-continue-btn").then(($button) => {
                if ($button.prop('disabled')) {
                    assert(1, "Cannot update subscription for this user!");
                } else {
                    cy.wrap($button).click()
                }
            });

        })


    })
})