import ComponentHelper from '../../../../support/Component.helper2'
import MenuHelper from '../../../../support/Menu.helper'
const {
    user_settings: {
        company_details: {
            CompanyDetail
        }
    }
} = Cypress.env('e2e');

describe("User Menu / Company Settings", function () {
    context("Company Details", function () {
        beforeEach(function () {
            MenuHelper.Top.selectFromUser("Company Settings")
            let setttings = {
                AccountDetails: "Account Details",
                Invoices: "Invoices",
                CompanyDetails: "Company Details",
                Users: "Users",
                Coaching: "Coaching",
                MessageLogs: "Message Logs",
                DropDownLists: "Drop-Down Lists"
            }

            cy.get('[data-cy="setting-ops"]').contains(setttings.CompanyDetails).click()
        })

        it("Test - Edit Company Details form", function () {
            let _helper = new ComponentHelper("company-details-card")

            _helper.actionButton("cd-edit-btn").scrollIntoView().click()

            _helper.fillModalForm([
                // {selector: "cd-short-code-in", type: "input", value: CompanyDetail.ShortCode},
                {selector: "cd-dispatch-number-in", type: "input", value: CompanyDetail.DispatchPhoneNumber},
                {selector: "cd-stripe-billing-email-in", type: "input", value: CompanyDetail.BillingEmail},
                {selector: "cd-address-line-1-in", type: "input", value: CompanyDetail.Address_Line1},
                {selector: "cd-address-line-2-in", type: "input", value: CompanyDetail.Address_Line2},
                {selector: "cd-address-city-in", type: "input", value: CompanyDetail.Address_City},
                {selector: "cd-address-state-in", type: "input", value: CompanyDetail.Address_State},
                {selector: "cd-address-zip-in", type: "input", value: CompanyDetail.Address_Zip},
            ])

            _helper.modalButtonContain("Update").scrollIntoView().click({force:true})
            _helper.alert('p').contains("Company Updated").should("be.visible")
        })

    })
})