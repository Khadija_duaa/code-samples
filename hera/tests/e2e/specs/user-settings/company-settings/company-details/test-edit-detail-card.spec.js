import ComponentHelper from '../../../../support/Component.helper2'
import MenuHelper from '../../../../support/Menu.helper'
const {
    user_settings: {
        company_details: {
            CompanyDetailCard
        }
    }
} = Cypress.env('e2e');

describe("User Menu / Company Settings", function () {
    context("Company Details", function () {
        beforeEach(function () {
            MenuHelper.Top.selectFromUser("Company Settings")
            let setttings = {
                AccountDetails: "Account Details",
                Invoices: "Invoices",
                CompanyDetails: "Company Details",
                Users: "Users",
                Coaching: "Coaching",
                MessageLogs: "Message Logs",
                DropDownLists: "Drop-Down Lists"
            }

            cy.get('[data-cy="setting-ops"]').contains(setttings.CompanyDetails).click()
        })

        it("Test - Company Details Card / Change the Company Logo", function () {
            let _helper = new ComponentHelper("company-details-card")

            _helper.container().contains("Company Logo").parents("form").then(el => {
                
                if(el.find(":contains('Delete')").length){
                    cy.wrap(el.find(":contains('Delete')").last()).scrollIntoView().click()
                    _helper.modalButtonContain("OK").click()
                    cy.intercept('POST', '**/graphql', (req) => {
                        return req.body
                    })
                    cy.wait(2000)
                }
            })

            _helper.fillForm([
                {selector: "cd-company-logo-file", type: "file", value: CompanyDetailCard.AccidentRecordForm, ttw: 3000}
            ])
            cy.intercept('POST', '**/graphql', (req) => {
                return req.body
            })
        })

        it("Test - Company Details Cards / Set the Time Zone", function () {

            let _helper = new ComponentHelper("company-details-card")

            _helper.fillForm([
                {selector: "cd-time-zone-auto-sw", type: "check", value: CompanyDetailCard.AutomaticallyTimeZone}
            ])
        })

        it("Test - Company Details Cards / Change Accident Record Form file", function () {
            let _helper = new ComponentHelper("company-details-card")
            cy.wait(2000)
            _helper.container().contains("Accident Record Form").parents("form").then(el => {
                
                if(el.find(":contains('Delete')").length){
                    cy.wrap(el.find(":contains('Delete')").last()).scrollIntoView().click()
                    _helper.modalButtonContain("OK").click()
                }

            })
            _helper.fillForm([
                {selector: "cd-accident-report-file", type: "file", value: CompanyDetailCard.AccidentRecordForm}
            ])
        })

        it("Test - Company Details Cards / Change Workmans Comp Form file", function () {
            let _helper = new ComponentHelper("company-details-card")
            _helper.container().contains("Workmans Comp Form").parents("form").then(el => {

                if(el.find(":contains('Delete')").length){
                    cy.wrap(el.find(":contains('Delete')").last()).scrollIntoView().click()
                    _helper.modalButtonContain("OK").click()
                }
            })
            
            _helper.fillForm([
                {selector: "cd-workmans-comp-file", type: "file", value: CompanyDetailCard.WorkmansCompForm}
            ])
        })
    })
})