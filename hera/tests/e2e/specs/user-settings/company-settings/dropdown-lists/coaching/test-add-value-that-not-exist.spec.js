import MenuHelper from '../../../../../support/Menu.helper'
const {
    user_settings: {
        dropdown_lists: {
            coaching:{
                DropdownList_Coaching
            }
        }
    }
} = Cypress.env('e2e');

describe('User Settings', function () {

    // COACHING TEXTS
    let parent_position = 5;
    let module_name = 'Coaching';
    let test_description = 'add a value that not exists in that dropdown list.';

    context('Company Settings', function () {

        beforeEach(() => {
            MenuHelper.Top.selectFromUser("Company Settings")

            let setttings = {
                AccountDetails: "Account Details",
                Invoices: "Invoices",
                CompanyDetails: "Company Details",
                Users: "Users",
                Coaching: "Coaching",
                MessageLogs: "Message Logs",
                DropDownLists: "Drop-Down Lists"
            }

            cy.get('[data-cy="setting-ops"]').contains(setttings.DropDownLists).click()
        });

        it('test ' + module_name + ' / DA Issue Types: ' + test_description, function () {
            let child_position = 1;
            let value = DropdownList_Coaching.IssueType;
            let scrollTo = {x: 0, y: 2000};
            cy.test_dropdownLists_addValue(parent_position, child_position, value, scrollTo);
            cy.get('body').find('.el-notification[role="alert"]', {timeout: 2000}).should('have.length', 0);
        });

        it('test ' + module_name + ' / DA Kudo Types: ' + test_description, function () {
            let child_position = 2;
            let value = DropdownList_Coaching.KudoType;
            let scrollTo = {x: 0, y: 2000};
            cy.test_dropdownLists_addValue(parent_position, child_position, value, scrollTo);
            cy.get('body').find('.el-notification[role="alert"]', {timeout: 2000}).should('have.length', 0);
        });

    })

})
