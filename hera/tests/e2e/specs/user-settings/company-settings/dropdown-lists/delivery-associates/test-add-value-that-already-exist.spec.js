import MenuHelper from '../../../../../support/Menu.helper'
describe('User Settings', function () {

    // DELIVERY ASSOCIATES TEXTS
    let parent_position = 1;
    let module_name = 'Delivery Associates';
    let test_description = 'add a value that already exists in that dropdown list.';

    context('Company Settings', function () {

        beforeEach(() => {
            MenuHelper.Top.selectFromUser("Company Settings")

            let setttings = {
                AccountDetails: "Account Details",
                Invoices: "Invoices",
                CompanyDetails: "Company Details",
                Users: "Users",
                Coaching: "Coaching",
                MessageLogs: "Message Logs",
                DropDownLists: "Drop-Down Lists"
            }

            cy.get('[data-cy="setting-ops"]').contains(setttings.DropDownLists).click()
        });

        it('test ' + module_name + ' / Genders: ' + test_description, function () {
            let child_position = 1;
            let value = 'Female';
            let scrollTo = {x: 0, y: 0};
            cy.test_dropdownLists_addValue(parent_position, child_position, value, scrollTo);
            cy.get('body').find('.el-notification[role="alert"]').should('be.visible').find('p').contains('Item already exists');
        });

        it('test ' + module_name + ' / Hourly Status: ' + test_description, function () {
            let child_position = 2;
            let value = 'Full Time';
            let scrollTo = {x: 0, y: 0};
            cy.test_dropdownLists_addValue(parent_position, child_position, value, scrollTo);
            cy.get('body').find('.el-notification[role="alert"]').should('be.visible').find('p').contains('Item already exists');
        });

        it('test ' + module_name + ' / Pronouns: ' + test_description, function () {
            let child_position = 3;
            let value = 'He/Him';
            let scrollTo = {x: 0, y: 0};
            cy.test_dropdownLists_addValue(parent_position, child_position, value, scrollTo);
            cy.get('body').find('.el-notification[role="alert"]').should('be.visible').find('p').contains('Item already exists');
        });

        // it('test ' + module_name + ' / DA Status: ' + test_description, function () {
        //     let child_position = 5;
        //     let value = 'Active';
        //     let scrollTo = {x: 0, y: 200};
        //     cy.test_dropdownLists_addValue(parent_position, child_position, value, scrollTo);
        //     cy.get('body').find('.el-notification[role="alert"]').should('be.visible');
        // });

        it('test ' + module_name + ' / Uniform Types: ' + test_description, function () {
            let child_position = 6;
            let value = 'Hat';
            let scrollTo = {x: 0, y: 300};
            cy.test_dropdownLists_addValue(parent_position, child_position, value, scrollTo);
            cy.get('body').find('.el-notification[role="alert"]').should('be.visible').find('p').contains('Item already exists');
        });
    });
});
