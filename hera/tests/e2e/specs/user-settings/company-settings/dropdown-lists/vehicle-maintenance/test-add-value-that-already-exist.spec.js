import MenuHelper from '../../../../../support/Menu.helper'
describe('User Settings', function () {

    // VEHICLE MAINTENANCE TEXTS
    let parent_position = 3;
    let module_name = 'Vehicle Maintenance';
    let test_description = 'add a value that already exists in that dropdown list.';

    context('Company Settings', function () {

        beforeEach(() => {
            MenuHelper.Top.selectFromUser("Company Settings")

            let setttings = {
                AccountDetails: "Account Details",
                Invoices: "Invoices",
                CompanyDetails: "Company Details",
                Users: "Users",
                Coaching: "Coaching",
                MessageLogs: "Message Logs",
                DropDownLists: "Drop-Down Lists"
            }

            cy.get('[data-cy="setting-ops"]').contains(setttings.DropDownLists).click()
        });

        it('test ' + module_name + ' / Maintenance services: ' + test_description, function () {
            let child_position = 1;
            let value = 'Air Filter Replacement';
            let scrollTo = {x: 0, y: 1050};
            cy.test_dropdownLists_addValue(parent_position, child_position, value, scrollTo);
            cy.get('body').find('.el-notification[role="alert"]').should('be.visible').find('p').contains('Item already exists');
        });

        it('test ' + module_name + ' / Service Location: ' + test_description, function () {
            let child_position = 3;
            let value = 'Ohio';
            let scrollTo = {x: 0, y: 1300};
            cy.test_dropdownLists_addValue(parent_position, child_position, value, scrollTo);
            cy.get('body').find('.el-notification[role="alert"]').should('be.visible').find('p').contains('Item already exists');
        });

    })

})
