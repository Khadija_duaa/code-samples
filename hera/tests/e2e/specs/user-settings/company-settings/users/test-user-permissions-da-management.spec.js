
import ComponentHelper from "../../../../support/Component.helper";
import MenuHelper from "../../../../support/Menu.helper";
const {
    user_settings: {
        company_settings:{
            users:{
                User_Filter
            }
        }
    }
} = Cypress.env('e2e');

describe('User Settings', function () {

    context('Company Settings', function () {
        it('Test - Edit user and remove access to DA Management', function () {
            MenuHelper.Top.selectFromUser("Company Settings")

            let setttings = {
                AccountDetails: "Account Details",
                Invoices: "Invoices",
                CompanyDetails: "Company Details",
                Users: "Users",
                Coaching: "Coaching",
                MessageLogs: "Message Logs",
                DropDownLists: "Drop-Down Lists"
            }
            // Cypress gets an element with a standard jQuery selector
            cy.get(`[data-cy="setting-ops"]`).contains(setttings.Users).scrollIntoView().click()

            let __helper = new ComponentHelper('users-container')
            __helper.fillValue({type: 'input', selector: 'filter', value: User_Filter})
            cy.get(`tr:contains('${User_Filter}')`).find("[data-cy='three_dot_button_row']").first().click({force: true})

            let menu = {
                EditUser: "Edit User",
                DeleteUser: "Delete User"
            }
            cy.get(".el-dropdown-menu").contains(menu.EditUser).click({force:true})

            cy.waitForLoaderToDisappear()// To wait for data to load
            cy.scrollTo(0, 300)

            /** Full access section */
            let PHRASE_ACCESS = "Has Full Admin Access"
            cy.get(`[data-cy='uf-account-control-access-item']:contains('${PHRASE_ACCESS}')`).find(".el-switch").as("access_switch") // to create @access_switch variable

            let WILL_FULL_ACCESS = false
            cy.get("@access_switch").scrollIntoView().invoke("hasClass","is-checked").then(switch_checked=>{//
                if(
                    (switch_checked && !WILL_FULL_ACCESS) ||
                    (!switch_checked && WILL_FULL_ACCESS)
                ){
                    cy.get("@access_switch").scrollIntoView().click()
                }
            })
            /** end */

            /** Features section */
            let PERMISSION_FEATURE = "Associate Management"
            cy.get(`[data-cy='uf-permission-item']:contains('${PERMISSION_FEATURE}')`).find(".el-switch").as("feature_switch")

            cy.scrollTo('bottom')
            let FEATURE_WILL_BE_ENABLE = false
            cy.get("@feature_switch").invoke("hasClass","is-checked").then(switch_checked=>{//
                if(
                    (switch_checked && !FEATURE_WILL_BE_ENABLE) ||
                    (!switch_checked && FEATURE_WILL_BE_ENABLE)
                ){
                    cy.get("@feature_switch").click({multiple: true, force: true})
                }
            })
            /** end */

            __helper = new ComponentHelper('user-form')
            __helper.actionButtonContain('Update').scrollIntoView().click()

            cy.wait(500).then(()=>{
                if(Cypress.$(".el-message-box").is(":visible")){
                    __helper.modalButtonContain("Update").scrollIntoView().click()
                }
            })

            __helper.alert().contains("User Updated").should("be.visible").then(el=>{
                assert(el, `**Access permission for ${User_Filter} was changed**, expected user will not have access to ${PERMISSION_FEATURE}`)
            })
        });

        /* it("Test - Check if the user has not access to DA Management", function (){
            MenuHelper.Tab.goTo("Associate Management")

            cy.get("h4").contains("Please contact your administrator if you need access to Associate Management in Hera.").should("be.visible").then(el=>{
                assert(el, `**User ${User_Filter} cannot access DA Management**, expected  user does not have permission to access this section.`)
            })

            cy.wait(12000)
            
        }) */

        it('Test - Edit user and enable access to DA Management', function () {
            MenuHelper.Top.selectFromUser("Company Settings")

            let setttings = {
                AccountDetails: "Account Details",
                Invoices: "Invoices",
                CompanyDetails: "Company Details",
                Users: "Users",
                Coaching: "Coaching",
                MessageLogs: "Message Logs",
                DropDownLists: "Drop-Down Lists"
            }
            // Cypress gets an element with a standard jQuery selector
            cy.get(`[data-cy="setting-ops"]`).contains(setttings.Users).scrollIntoView().click()

            let __helper = new ComponentHelper('users-container')
            __helper.fillValue({type: 'input', selector: 'filter', value: User_Filter})
            cy.get(`tr:contains('${User_Filter}')`).find("[data-cy='three_dot_button_row']").first().click({force: true})

            let menu = {
                EditUser: "Edit User",
                DeleteUser: "Delete User"
            }
            cy.get(".el-dropdown-menu").contains(menu.EditUser).click({force: true})

            cy.waitForLoaderToDisappear()// To wait for data to load
            cy.scrollTo(0, 300)

            /** Features section */
            let PERMISSION_FEATURE = "Associate Management"
            cy.get(`[data-cy='uf-permission-item']:contains('${PERMISSION_FEATURE}')`).find(".el-switch").as("feature_switch")

            let FEATURE_WILL_BE_ENABLE = true
            cy.get("@feature_switch").invoke("hasClass","is-checked").then(switch_checked=>{//
                if(
                    (switch_checked && !FEATURE_WILL_BE_ENABLE) ||
                    (!switch_checked && FEATURE_WILL_BE_ENABLE)
                ){
                    cy.get("@feature_switch").click({multiple: true, force: true})
                }
            })
            /** end */

            /** Full access section */
            let PHRASE_ACCESS = "Has Full Admin Access"
            cy.get(`[data-cy='uf-account-control-access-item']:contains('${PHRASE_ACCESS}')`).find(".el-switch").as("access_switch") // to create @access_switch variable

            cy.waitForLoaderToDisappear()// To wait for data to load

            let WILL_FULL_ACCESS = true
            cy.get("@access_switch").scrollIntoView().invoke("hasClass","is-checked").then(switch_checked=>{//
                if(
                    (switch_checked && !WILL_FULL_ACCESS) ||
                    (!switch_checked && WILL_FULL_ACCESS)
                ){
                    cy.get("@access_switch").scrollIntoView().click()
                }
            })
            /** end */

            cy.scrollTo('bottom')
            __helper = new ComponentHelper('user-form')
            __helper.actionButtonContain('Update').scrollIntoView().click()

            cy.wait(1000).then(()=>{
                if(Cypress.$(".el-message-box").is(":visible")){
                    __helper.modalButtonContain("Update").scrollIntoView().click()
                }
            })

            __helper.alert().contains("User Updated").should("be.visible").then(el=>{
                assert(el, `**Access permission for ${User_Filter} was changed**, expected user will not have access to ${PERMISSION_FEATURE}`)
            })

        });

        it("Test - Check if the user has access to DA Management", function (){
            MenuHelper.Tab.goTo("Associate Management")

            cy.get("[data-cy='damanagement-options']").should("be.visible").then(el=>{
                assert(el, `**User ${User_Filter} access Associate Management**, expected  user does have permission to access this section.`)
            })
            
        })

    })
    
})