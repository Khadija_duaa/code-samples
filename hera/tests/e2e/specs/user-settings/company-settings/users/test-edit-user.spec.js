import ComponentHelper from "../../../../support/Component.helper2";
import MenuHelper from '../../../../support/Menu.helper'
const {
    user_settings: {
        company_settings:{
            users:{
                User_Filter,
                User_ED
            }
        }
    }
} = Cypress.env('e2e');

describe('User Settings', function () {

    context('Company Settings', function () {

        beforeEach(() => {
            MenuHelper.Top.selectFromUser("Company Settings")

            let setttings = {
                AccountDetails: "Account Details",
                Invoices: "Invoices",
                CompanyDetails: "Company Details",
                Users: "Users",
                Coaching: "Coaching",
                MessageLogs: "Message Logs",
                DropDownLists: "Drop-Down Lists"
            }

            cy.get('[data-cy="setting-ops"]').contains(setttings.Users).click()
        });

        it('test edit user', function () {
            let __helper = new ComponentHelper('users-container')
            __helper.fillValue({type: 'input', selector: 'filter', value: User_Filter})
            cy.get(`tr:contains('${User_Filter}')`).find("[data-cy='three_dot_button_row']").first().click({force: true})
            cy.get('.el-dropdown-menu').contains("Edit User").click({force:true})
            // __helper.getTableRow(0, 'edit-user').click({force: true})
            __helper = new ComponentHelper('user-form')
            __helper.fillForm([
                {type: 'input', selector: 'phone', value: User_ED.Phone},
            ])
            __helper.actionButtonContain('Update').click({force: true})
        });

    })

})
