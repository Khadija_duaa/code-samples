import ComponentHelper from "../../../../support/Component.helper2";
import MenuHelper from '../../../../support/Menu.helper'
const {
    user_settings: {
        company_settings:{
            users:{
                User
            }
        }
    }
} = Cypress.env('e2e');

describe('User Settings', function () {

    context('Company Settings', function () {

        beforeEach(() => {
            MenuHelper.Top.selectFromUser("Company Settings")

            let setttings = {
                AccountDetails: "Account Details",
                Invoices: "Invoices",
                CompanyDetails: "Company Details",
                Users: "Users",
                Coaching: "Coaching",
                MessageLogs: "Message Logs",
                DropDownLists: "Drop-Down Lists"
            }

            cy.get('[data-cy="setting-ops"]').contains(setttings.Users).click()
        });

        it('test add user', function () {
            let __helper = new ComponentHelper('users-container')
            __helper.actionButton('three_dot_button').first().click({force:true})
            cy.get('.el-dropdown-menu').contains("Add User").click({force:true})
            __helper = new ComponentHelper('user-form')
            __helper.fillForm([
                {type: 'input', selector: 'firstName', value: User.FirstName},
                {type: 'input', selector: 'lastName', value: User.LastName},
                {type: 'input', selector: 'email', value: User.Email},
                {type: 'input', selector: 'phone', value: User.Phone},
            ])
            __helper.actionButtonContain('Create').click({force:true})
        });

    })

})
