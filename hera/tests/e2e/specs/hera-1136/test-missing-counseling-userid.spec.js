import ComponentHelper from "../../support/Component.helper2";
import MenuHelper from "../../support/Menu.helper";

context('HERA-1136', function () {

    
    it('Test - Show the Team Leader Field on Counseling Form', function () {
        MenuHelper.Tab.goTo("Performance & Coaching", '/performance-and-coaching/performance')
        let __helper = new ComponentHelper('performance-coaching-container');
        cy.contains('Counselings').scrollIntoView().click()
        __helper = new ComponentHelper('counseling-container');
        __helper.getTableRow(0,"ci-elipsis-btn").scrollIntoView().click()
        cy.get('.el-dropdown-menu:visible').contains('View Counseling').scrollIntoView().click()

        cy.wait(1000)
    });
    
})