let text = "Testing Option"
describe('HERA-1154', () => {
    context('View drawer options', () => {
        
        it('View drawer options', function () {
            cy.get('[data-cy="user-info-dropdown"]').click()
            cy.wait(4000);
            cy.get('[data-cy="company-settings-option"]').click()
            cy.wait(4000);
            cy.get('[data-cy="custom-lists-tab"]').click()
            cy.wait(4000);
            cy.get('body').find('.el-dropdown-menu').contains('Edit this Custom List').first().click({force: true})
            cy.wait(2000)
            cy.get('[data-cy="new-option-btn"]').click()
            cy.wait(1000);
            cy.get('[data-cy="new-option-input"]').focus().type(text);
            cy.wait(4000);
            cy.get('[data-cy="done-btn"]').click({force: true}) 
            cy.wait(4000);
            cy.get('[data-cy="drawer-save-btn"]').click()
            cy.wait(4000);
        })
    })
})