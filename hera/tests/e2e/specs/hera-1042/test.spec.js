
describe('Add Vehicle Type to Vehicles: (10) During performance data and daily roster imports', function () {

    
    it('Daily Roster Import', function () {
        cy.visit('/daily-roster-import');
        cy.get('body').find('[data-cy="attach-file"]').attachFile('daily-roster-import-example.xlsx')
        cy.wait(1000)
    })

    it('Import Weekly Performance Data', function () {
        cy.visit('/performance-and-coaching/weekly-performance-data-import');
        cy.get('body').find('#FileUpload').attachFile('US_CLEO_HNY2_Week22_2022_DSPScorecard.pdf')
        cy.get('body').find('button').contains('Upload All').trigger('click')
        cy.wait(1000)
    })
})