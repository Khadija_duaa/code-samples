describe('HERA-1321', () => {
    context('Performance data imports', () => {
        
        it('Performance data imports', function () {
            cy.get('[data-cy="import-dropdown"]').click()
            cy.wait(2000);
            cy.get('[data-cy="import-weekly-performance-data"]').click()
            cy.wait(2000);
            cy.get('body').find('#FileUpload').attachFile('US_KDLL_DLN3_Week27_2023_customer_feedback20230710.pdf')
            cy.wait(10000);
            cy.get('[data-cy="btn-weekly-data-import"]').click({force: true})
            cy.wait(10000);
        })
    })
})