import FunctionHelper from "../../support/Function.helper";

describe('Parking Space', function () {

    it('Go to Custom List Tab', function () {
        cy.get('[data-cy="user-info-dropdown"]').click()
        cy.wait(6000);
        cy.get('[data-cy="company-settings-option"]').click()
        cy.wait(6000);
        cy.get('[data-cy="custom-lists-tab"]').click()
        cy.wait(8000);
    })

    it('Go to Daily Rostering Tab', function () {
        cy.get('[data-cy="user-info-dropdown"]').click()
        cy.wait(6000);
        cy.get('[data-cy="company-settings-option"]').click()
        cy.wait(6000);
        cy.get('[data-cy=custom-lists-tab]').click()
        cy.get('[data-cy=search-in]').type('Parking Space').click()
        cy.wait(8000)
        cy.get('[data-cy="three_dot_button_row"]').click({multiple:true, force:true})
        cy.wait(4000);
        cy.get('.el-dropdown-menu').contains('Edit this Custom List').scrollIntoView().click({force:true})
        cy.wait(4000);
        cy.get('[data-cy=drawer-save-btn]').click({force: true})
    })
})