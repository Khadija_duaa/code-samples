import FunctionHelper from "../../support/Function.helper";
import ComponentHelper from "../../support/Component.helper2";
import MenuHelper from "../../support/Menu.helper";

const {
    daily_rostering: {
        daily_roster_table: {
            DailyRoster,
            ReplaceWithStandByDA
        }
    }
} = Cypress.env('e2e');

const today = new FunctionHelper().getCurrentDate({
    format: 'Ymd',
    separator: '-'
});

context('HERA-1058', function () {

    
    it('Test send messages to all filtered DAs when roster status is Called Out, No Show, No Call or VTO', function () {
        MenuHelper.Tab.goTo("Daily Rostering", '/daily-roster/' + today)
        let __helper = new ComponentHelper('daily-roster-table');
        __helper.actionButton('single-da').scrollIntoView().click();
        __helper.fillModalForm([
            {type: 'select', selector: 'staffId', value: 'Hera Test'},
            {type: 'time', selector: 'time', value: DailyRoster.Time},
            {type: 'input', selector: 'notes', value: 'Hera test 1058'},
        ]);
        __helper.modalButton('confirm').scrollIntoView().click();

        cy.log('Waiting for result');

        cy.wait(1000);
        
        __helper.actionButton('single-da').scrollIntoView().click();
        __helper.fillModalForm([
            {type: 'select', selector: 'staffId', value: 'TestDA Supervisor'},
            {type: 'time', selector: 'time', value: DailyRoster.Time},
            {type: 'input', selector: 'notes', value: 'Hera test 1058'},
        ]);
        __helper.modalButton('confirm').scrollIntoView().click();

        cy.log('Waiting for result');

        cy.wait(1000);
   
        __helper.actionButton('single-da').scrollIntoView().click();
        __helper.fillModalForm([
            {type: 'select', selector: 'staffId', value: 'Hera TestDA'},
            {type: 'time', selector: 'time', value: DailyRoster.Time},
            {type: 'input', selector: 'notes', value: 'Hera test 1058'},
        ]);
        __helper.modalButton('confirm').scrollIntoView().click();

        cy.log('Waiting for result');

        cy.wait(1000);

        __helper.getTableRow(0, 'status').scrollIntoView().click()
        cy.get('.el-select-dropdown:visible').contains('Late, No Call').scrollIntoView().click()
        __helper.modalButtonContain('Cancel').scrollIntoView().click()
        cy.wait(2000)
        __helper.getTableRow(1, 'status').scrollIntoView().click()
        cy.get('.el-select-dropdown:visible').contains('Late, With Call').scrollIntoView().click()
        __helper.modalButtonContain('Cancel').scrollIntoView().click()
        cy.wait(2000)
        __helper.getTableRow(2, 'status').scrollIntoView().click()
        cy.get('.el-select-dropdown:visible').contains('On Time').scrollIntoView().click()
        cy.wait(2000)
        __helper.select('[data-cy="elipsis-top-level"]','Send to all rostered Associates')
        cy.contains('Are you sure you want to send out 3 messages?').should('be.visible')
        cy.wait(1000)
        __helper.modalButtonContain('Cancel').scrollIntoView().click()

        __helper.getTableRow(2, 'status').scrollIntoView().click()
        cy.get('.el-select-dropdown:visible').contains('VTO').scrollIntoView().click()
        cy.wait(2000)
        __helper.select('[data-cy="elipsis-top-level"]','Send to all rostered Associates')
        cy.contains('Are you sure you want to send out 2 messages?').should('be.visible')
        cy.wait(1000)
        __helper.modalButtonContain('Cancel').scrollIntoView().click()

        __helper.getTableRow(1, 'status').scrollIntoView().click()
        cy.get('.el-select-dropdown:visible').contains('Called Out').scrollIntoView().click()
        __helper.modalButtonContain('Cancel').scrollIntoView().click()
        cy.wait(2000)
        __helper.select('[data-cy="elipsis-top-level"]','Send to all rostered Associates')
        cy.contains('Are you sure you want to send out 1 messages?').should('be.visible')
        cy.wait(1000)
        __helper.modalButtonContain('Cancel').scrollIntoView().click()

        __helper.getTableRow(0, 'status').scrollIntoView().click()
        cy.get('.el-select-dropdown:visible').contains('No Show, No Call').scrollIntoView().click()
        __helper.modalButtonContain('Cancel').scrollIntoView().click()
        cy.wait(2000)
        
        __helper.actionButton('elipsis-top-level').scrollIntoView().click()
        cy.get('.el-dropdown-menu:visible').contains('li','Send to all rostered Associates').then(console.log)


    });

    it('Delete Daily Roster', function () {
        cy.visit('/daily-roster/' + today);
        cy.wait(5000)
        cy.get('body').find('[data-cy="status_3dot_button"]').each(($el, index) => {
            if(index < 2) {
                cy.wrap($el).click()
                cy.get('body').find('.el-dropdown-menu:visible').find('li').contains('Edit Associate Assignment').click({force: true})
                cy.get('body').find('[role="dialog"]:visible').find('button').contains('Delete').click()
                cy.get('body').find('.el-message-box:visible').find('button').contains('Delete').click()
            }
            
        })
        cy.get('body').find('[data-cy="status_3dot_button"]').first().click()
        cy.get('body').find('.el-dropdown-menu:visible').find('li').contains('Edit Associate Assignment').click({force: true})
        cy.get('body').find('[role="dialog"]:visible').find('button').contains('Delete').click()
        cy.get('body').find('.el-message-box:visible').find('button').contains('Delete').click()
        
    }) 
    
})