export default class ComponentHelper {

    constructor(cySelector, scrollAxis) {
        this.scroll(scrollAxis)
        this._containerSelector = cySelector ? `[data-cy="${ cySelector }"]` : 'body' 
        this._modalSelector = '[role="dialog"]:not(iframe)'
        this._graphql_intercept_url = '**/graphql'
        this._graphql_intercept_alias = null
        this._intercept_delay = 3500
    }

    scroll(scrollAxis) {
        cy.wait(2000)
        if (typeof scrollAxis !== "undefined") {
            cy.scrollTo(scrollAxis.x, scrollAxis.y)
        }
    }
    
    graphqlIntercept = (req) => cy.intercept('POST', this._graphql_intercept_url, req).as(this._graphql_intercept_alias = "GraphQL")

    waitXHR(action) {
        if(!this._graphql_intercept_alias){
            this.graphqlIntercept()
        }
        if(typeof action !== 'function'){
            action = (r) => r
        }
        return cy.wait(`@${this._graphql_intercept_alias}`).then(xhr => cy.wrap(action(xhr), {log: false}))
    }

    waitGraphqlXHR = (graphqlMethod) => {
        return this.waitXHR((xhr)=>{
            const {response: {body}} = xhr
            if(!graphqlMethod || !body){
               return xhr
            }
            const {data} = body
            const substr = graphqlMethod.substr(1)
            const graphqlResponse = data[graphqlMethod[0].toUpperCase() + substr] ?? data[graphqlMethod[0].toLowerCase() + substr]
            assert(graphqlResponse, `_@${this._graphql_intercept_alias}:_ **${graphqlMethod}**`)
            return graphqlResponse
        })
    }
    
    #defineContainter(querySelector, containerName){
        let container;
        if(containerName){
            container = this[containerName](querySelector)
        }else{
            container = cy.get(querySelector)
        }
        return container.then(c =>{
            const {left, top} = c.offset()
            const x = left + c.width()
            const y = top - 150
            c.scrollTo(x, y)
            return cy.wrap(c, {log: false })
        })
    }

    /** Containers */
    "container" = (childQuerySelector = '') => cy.get(`${ this._containerSelector } ${childQuerySelector}`)
    "table" = (childQuerySelector = '') => this.container(`table > tbody ${childQuerySelector}`)
    "modal" = (childQuerySelector = '') => cy.then(()=>{
        if(Cypress.$(this._modalSelector).is(":visible")){
            return cy.get(`${this._modalSelector}:visible ${childQuerySelector}`)
        }
        const modal = cy.get(`${this._modalSelector} > :visible`)
        const parent = modal.invoke("parent",this._modalSelector)
        if(childQuerySelector){
            return parent.invoke("find",childQuerySelector)
        }
        return parent
    })
    /** End containers */

    /**  Methods for form inputs */
    // {type: "time"}
    "time"(querySelector, value, containerName){
        this.#defineContainter(querySelector, containerName)
            .click()
        cy.get(`.el-popper:visible .el-scrollbar`).eq(0).contains('li',value.h)
            .click({force:true})
        cy.get(`.el-popper:visible .el-scrollbar`).eq(1).contains('li',value.m)
            .click({force:true})
        cy.get('.el-popper:visible').contains('button','OK').scrollIntoView().click()
        cy.get('.el-popper:visible').invoke('hide')
    }
    // {type: "datetime"}
    "datetime"(querySelector, value, containerName){
        this.#defineContainter(querySelector, containerName)
            .click()
        cy.get(".el-picker-panel:visible .el-input:eq(1)")
            .type(value.date)
        cy.get(".el-picker-panel:visible .el-input:eq(2)")
            .type(value.time)
        cy.get(`.el-picker-panel:visible button:contains(/OK/)`)
            .click({force:true})
    }
    // {type: "select"}
    "select"(querySelector, value, containerName){
        this.#defineContainter(querySelector, containerName)
            .then(select => {
                let aux = select.find(".el-dropdown-link")
                if(aux.length){
                    return cy.wrap(aux)
                }
                return cy.wrap(select)
            }).first()
            .click()

        const exact = (value) => RegExp(`^${value}$`, "g")

        cy.get('.el-popper:visible').as("helper_select").then(popper=>{
            if(popper.hasClass("is-multiple")){
                cy.wrap(value).each(v => {
                    cy.get("@helper_select")
                        .contains("li",exact(v))
                        .click({force:true})
                })
            }else{
                cy.get("@helper_select")
                    .contains("li", exact(value)).then(li => {
                        if(!li.hasClass("selected is-disabled")){
                            cy.wrap(li).click({force:true})
                        }
                    })
            }
            cy.get("@helper_select").invoke("hide")
        })
    }
    // {type: "input"}
    "input"(querySelector, value, containerName){
        this.#defineContainter(querySelector, containerName)
            .as("helper_input")
            .type(`{selectall}${value}`)
        cy.get("@helper_input").invoke('prop', 'tagName').then(tagName =>{
            if(tagName.toLowerCase() === 'div'){
                cy.get("@helper_input").invoke('prop', 'classList').then(classList =>{
                    if(classList.contains('el-date-editor')){
                        cy.get(".el-date-picker:visible").invoke("hide");
                    }
                })
            }
        })
    }
    // {type: "check"}
    "check"(querySelector, value, containerName){
        this.#defineContainter(querySelector, containerName)
            .as("helper_check")
        cy.get("@helper_check").invoke('prop', 'tagName').then(tagName=>{
            let checkbox;
            if(tagName.toLowerCase() === "input"){
                checkbox = cy.get("@helper_check")
            }else{
                checkbox = cy.get("@helper_check").invoke('find',':checkbox')
            }
            checkbox.invoke("prop","checked").then(checked=>{
                if(
                    (typeof value === 'undefined') ||
                    (checked && (value === "false" || value === false)) ||
                    (!checked && value && value !== "false")
                ){
                    cy.get("@helper_check").click({force:true})
                }
            })
        })
    }
    // {type: "radio"}
    "radio"(querySelector, value, containerName){
        this.#defineContainter(querySelector, containerName)
            .as("helper_radio")
        if(value){
            cy.get("@helper_radio").contains("label",value)
                .click({force:true})
        }else{
            cy.get("@helper_radio").invoke("find",":radio")
                .click({force:true})
        }
    }
    // {type: "file"}
    "file"(querySelector, value, containerName){
        this.#defineContainter(querySelector, containerName)
            .attachFile(value)
    }
    /** End methods*/

    getTableRow = (row, cySelector) => this.table(`tr:eq(${row}) [data-cy='${cySelector}']`)

    actionButton = (cySelector) => this.container(`[data-cy='${cySelector}']`)

    actionButtonContain = (containText) => this.container('button').contains(containText).parent()

    modalButton = (cySelector) => this.modal(`[data-cy='${cySelector}']`)

    modalButtonContain = (containText) => this.modal('button').contains(containText).parent()

    #fill({ type, value, ttw}, containerName, cySelector){

        // Call methods for form inputs
        this[type](cySelector, value, containerName)
        
        cy/*.wrap(Cypress.$("body"),{log:false}).focus({log:false}).blur({log:false})*/.then(() => {
            assert(true, `_${type}_ << **${ typeof value === 'object'? JSON.stringify(value) : value }**`)
        })
        
        if (typeof ttw !== "undefined") {
            cy.wait(ttw)
        }
    }

    fillValue = (item) => this.#fill(item, "container", `[data-cy='${item.selector}']`)

    fillModalValue = (item) => this.#fill(item, "modal", `[data-cy='${item.selector}']`)

    fillTableValue = (row, item) => this.#fill(item, "table", `tr.el-table__row:nth-child(${row}) [data-cy='${item.selector}']`)
    
    fillTable = (row, data) => cy.wrap(data).each((item) => this.fillTableValue(row, item))

    fillModalForm = (data) => cy.wrap(data).each(this.fillModalValue)

    fillForm = (data) => cy.wrap(data).each(this.fillValue)

    detachIfExist = () => {
        cy.get('body').then(($body) => {
            let isAttached = $body.find('[data-cy="remove-img"]')
            if (isAttached.length) {
                cy.wrap(isAttached).click()
            }
        })
    }

    messageBoxConfirm = async (confirm = true) => {
        cy.wait(1500)
        cy.get('body').then(($body) => {
            let modal = $body.find('div[role="dialog"]:visible').last()
            if (modal.length && typeof confirm === "boolean") {
                cy.wrap(modal).find(`button:nth-child(${confirm ? 2 : 1 })`).last().click()
            }
        })
    }

    alert = (selector = '') => cy.get(`[role="alert"]:visible ${selector}`)
    
}