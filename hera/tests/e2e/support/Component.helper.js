export default class ComponentHelper {

    constructor(cySelector, scrollAxis) {
        this.scroll(scrollAxis)
        this._containerSelector = cySelector
        this._modalSelector = 'div[role="dialog"]:visible'
        this._container = null;
        this._modal = null;
        this._intercept_url = '**/graphql'
        this._intercept_delay = 1000    // previous value = 3500
    }

    scroll(scrollAxis) {
        cy.wait(2000)
        if (typeof scrollAxis !== "undefined") {
            cy.scrollTo(scrollAxis.x, scrollAxis.y)
        }
    }

    //-
    get containerSelector() {
        return this._containerSelector;
    }

    get modalSelector() {
        return this._modalSelector
    }

    //-
    waitXHR() {
        cy.intercept({
            method: 'POST',
            url: this._intercept_url,
            statusCode: 200,
        }).then(() => {
            cy.wait(this._intercept_delay)
        })
    }

    container() {
        this._container = cy.get('[data-cy="' + this.containerSelector + '"]').should('be.visible')
        return this._container;
    }

    table() {
        this._table = this.container().find('table > tbody').should('be.visible')
        return this._table;
    }

    getTableRow(row, cySelector) {
        this._table = this.table()
            .find('tr').eq(row)
            .should('be.visible')
            .find('[data-cy="' + cySelector + '"]')
        return this._table;
    }

    modal() {
        this._modal = cy.get(this.modalSelector).should('be.visible')
        return this._modal
    }

    actionButton = (cySelector) => {
        let parent = this.container()
        return parent.find('[data-cy="' + cySelector + '"]')
    }

    actionButtonContain = (containText) => {
        let parent = this.container()
        return parent.find('button').contains(containText).parent()
    }

    modalButton = (cySelector) => {
        let modal = this.modal()
        return modal.find('[data-cy="' + cySelector + '"]')
    }

    modalButtonContain = (containText) => {
        let modal = this.modal()
        return modal.find('button').contains(containText).parent()
    }

    fillValue = (item) => {
        let selector = item.selector;
        let value = item.value;
        let type = item.type;
        let ttw = item.ttw;
        if (typeof ttw !== "undefined") {
            cy.wait(ttw)
        }
        if (type === 'time') {
            this.container()
                .find('[data-cy="' + selector + '"]')
                .scrollIntoView().click()
            cy.get('.el-time-spinner:visible > :nth-child(1)')
                .find('ul.el-time-spinner__list')
                .contains(value.h)
                .click({force: true})
            cy.get('.el-time-spinner:visible > :nth-child(2)')
                .find('ul.el-time-spinner__list')
                .contains(value.m)
                .click({force: true})
            cy.get('.el-time-panel:visible')
                .find('button.confirm')
                .click()
        }
        if (type === 'select') {

            this.container()
                .find('[data-cy="' + selector + '"]')
                .click({multiple: true, force: true})

            cy.get('.el-select-dropdown:visible').as("helper_select")

            cy.get("@helper_select").invoke("hasClass", "is-multiple").then(isMultiple => {

                if (isMultiple) {

                    cy.wrap(value).each(v => {
                        cy.get("@helper_select")
                            .find("li.el-select-dropdown__item")
                            .contains(v)
                            .click({force: true})
                    })

                } else {
                    cy.get("@helper_select")
                        .find("li.el-select-dropdown__item")
                        .contains(value)
                        .click({force: true})
                }
                cy.get("@helper_select").invoke("hide")

            })


        }
        if (type === 'dropdown') {
            this.container()
                .find('[data-cy="' + selector + '"]')
                .find('.el-dropdown-link')
                .click({force: true})
            cy.wait(2000)
            cy.get('body').find('ul.el-dropdown-menu:visible')
                .find('li.el-dropdown-menu__item').contains(value)
                .click({force: true})
        }
        if (type === 'input') {

            this.container()
                .find('[data-cy="' + selector + '"]').as("helper_input")
                .type('{selectall}{del}' + value)

            cy.get("@helper_input").invoke('prop', 'tagName').then(tagName => {

                if (tagName.toLowerCase() === 'div') {

                    cy.get("@helper_input").invoke('prop', 'classList').then(classList => {

                        if (classList.contains('el-date-editor')) {
                            cy.get(".el-date-picker:visible").invoke("hide");
                        }
                    })
                }
            })


        }
        if (type === 'check') {
            this.container()
                .find('[data-cy="' + selector + '"]').as("helper_check")
            cy.get("@helper_check").invoke('prop', 'tagName').then(tagName => {
                let checkbox;
                if (tagName.toLowerCase() === "input") {
                    checkbox = cy.get("@helper_check")
                } else {
                    checkbox = cy.get("@helper_check").find(':checkbox')
                }
                checkbox.invoke("prop", "checked").then(checked => {
                    if (
                        (typeof value === 'undefined') ||
                        (checked && (value === "false" || value === false)) ||
                        (!checked && value && value !== "false")
                    ) {
                        cy.get("@helper_check").scrollIntoView().click()
                    }
                })
            })
        }
        if (type === 'radio') {
            this.container()
                .find('[data-cy="' + selector + '"]')
                .find('input[type="radio"]')
                .click({force: true})
        }
        if (type === 'file') {
            this.container()
                .find('[data-cy="' + selector + '"]')
                .attachFile(value)
        }
        if (typeof ttw !== "undefined") {
            cy.wait(ttw)
        }
        cy.get('body').click({force: true})
        if (typeof item.dismissModal !== "undefined") {
            this.modal().find('.el-dialog__headerbtn').click({force: true})
        }        
    }

    fillModalValue = (item) => {
        let selector = item.selector;
        let value = item.value;
        let type = item.type;
        let ttw = item.ttw;
        if (typeof ttw !== "undefined") {
            cy.wait(ttw)
        }
        if (type === 'time') {
            this.modal()
                .find('[data-cy="' + selector + '"]')
                .scrollIntoView()
                .click()
            cy.get('.el-time-spinner:visible > :nth-child(1)')
                .find('ul.el-time-spinner__list')
                .contains(value.h)
                .scrollIntoView()
                .click()
            cy.get('.el-time-spinner:visible > :nth-child(2)')
                .find('ul.el-time-spinner__list')
                .contains(value.m)
                .scrollIntoView()
                .click()
            cy.get('.el-time-panel:visible')
                .find('.confirm')
                .scrollIntoView()
                .click()
        }
        if (type === 'select') {
            this.modal()
                .find('[data-cy="' + selector + '"]')
                .scrollIntoView().click()

            cy.get('.el-select-dropdown:visible').as("helper_select")

            cy.get("@helper_select").invoke("hasClass", "is-multiple").then(isMultiple => {

                if (isMultiple) {
                    for (const v of value) {
                        cy.get("@helper_select").find("li.el-select-dropdown__item").contains(v)
                            .click({force: true})
                    }
                } else {
                    cy.get("@helper_select").find("li.el-select-dropdown__item").contains(value)
                        .click({force: true})
                }
                cy.get("@helper_select").invoke("hide")

            })

        }
        if (type === 'input') {

            this.modal()
                .find('[data-cy="' + selector + '"]').as("helper_input")
                .type('{selectall}{del}' + value)

            cy.get("@helper_input").invoke('prop', 'tagName').then(tagName => {

                if (tagName.toLowerCase() === 'div') {

                    cy.get("@helper_input").invoke('prop', 'classList').then(classList => {

                        if (classList.contains('el-date-editor')) {
                            cy.get(".el-date-picker:visible").invoke("hide");
                        }
                    })
                }
            })
        }
        if (type === 'check') {
            this.modal()
                .find('[data-cy="' + selector + '"]').as("helper_check")
            cy.get("@helper_check").invoke('prop', 'tagName').then(tagName => {
                let checkbox;
                if (tagName.toLowerCase() === "input") {
                    checkbox = cy.get("@helper_check")
                } else {
                    checkbox = cy.get("@helper_check").find(':checkbox')
                }
                checkbox.invoke("prop", "checked").then(checked => {
                    if (
                        (typeof value === 'undefined') ||
                        (checked && (value === "false" || value === false)) ||
                        (!checked && value && value !== "false")
                    ) {
                        cy.get("@helper_check").scrollIntoView().click()
                    }
                })
            })
        }
        if (type === 'radio') {
            this.modal()
                .find('[data-cy="' + selector + '"]')
                .find('input[type="radio"]')
                .click({force: true})
        }
        if (type === 'file') {
            this.modal()
                .find('[data-cy="' + selector + '"]')
                .attachFile(value)
        }
        this.waitXHR()
        if (typeof ttw !== "undefined") {
            cy.wait(ttw)
        }
    }

    fillTableValue = (row, item) => {
        let selector = item.selector;
        let value = item.value;
        let type = item.type;
        let ttw = item.ttw;
        if (typeof ttw !== "undefined") {
            cy.wait(ttw)
        }
        if (type === 'time') {
            this.table()
                .find('tr.el-table__row:nth-child(' + row + ')')
                .find('[data-cy="' + selector + '"]')
                .scrollIntoView().click()
            cy.get('.el-time-spinner:visible > :nth-child(1)')
                .find('ul.el-time-spinner__list')
                .contains(value.h)
                .click({force: true})
            cy.get('.el-time-spinner:visible > :nth-child(2)')
                .find('ul.el-time-spinner__list')
                .contains(value.m)
                .click({force: true})
            cy.get('.el-time-panel:visible')
                .find('button.confirm')
                .click()
        }
        if (type === 'select') {
            this.table()
                .find('tr.el-table__row:nth-child(' + row + ')')
                .find('[data-cy="' + selector + '"]')
                .scrollIntoView()
            this.table()
                .find('tr.el-table__row:nth-child(' + row + ')')
                .find('[data-cy="' + selector + '"]')
                .scrollIntoView().click()

            cy.get('.el-select-dropdown:visible').as("helper_select")

            cy.get("@helper_select").invoke("hasClass", "is-multiple").then(isMultiple => {

                if (isMultiple) {
                    for (const v of value) {
                        cy.get("@helper_select").find("li.el-select-dropdown__item").contains(v)
                            .click({force: true})
                    }
                } else {
                    cy.get("@helper_select").find("li.el-select-dropdown__item").contains(value)
                        .click({force: true})
                }
                cy.get("@helper_select").invoke("hide")

            })

        }
        if (type === 'input') {
            this.table()
                .find('tr.el-table__row:nth-child(' + row + ')')
                .find('[data-cy="' + selector + '"]').as("helper_input")
                .type('{selectall}{del}' + value)

            cy.get("@helper_input").invoke('prop', 'tagName').then(tagName => {

                if (tagName.toLowerCase() === 'div') {

                    cy.get("@helper_input").invoke('prop', 'classList').then(classList => {

                        if (classList.contains('el-date-editor')) {
                            cy.get(".el-date-picker:visible").invoke("hide");
                        }

                    })
                }
            })
        }
        if (type === 'check') {
            this.table()
                .find('tr.el-table__row:nth-child(' + row + ')')
                .find('[data-cy="' + selector + '"]').as("helper_check")
            cy.get("@helper_check").invoke('prop', 'tagName').then(tagName => {
                let checkbox;
                if (tagName.toLowerCase() === "input") {
                    checkbox = cy.get("@helper_check")
                } else {
                    checkbox = cy.get("@helper_check").find(':checkbox')
                }
                checkbox.invoke("prop", "checked").then(checked => {
                    if (
                        (typeof value === 'undefined') ||
                        (checked && (value === "false" || value === false)) ||
                        (!checked && value && value !== "false")
                    ) {
                        cy.get("@helper_check").scrollIntoView().click()
                    }
                })
            })
        }
        if (type === 'radio') {
            this.table()
                .find('tr.el-table__row:nth-child(' + row + ')')
                .find('[data-cy="' + selector + '"]')
                .find('input[type="radio"]')
                .click({force: true})
        }
        if (type === 'file') {
            this.table()
                .find('tr.el-table__row:nth-child(' + row + ')')
                .find('[data-cy="' + selector + '"]')
                .attachFile(value)
        }
        cy.get('body').find('button').first().focus()
        this.waitXHR()
        if (typeof ttw !== "undefined") {
            cy.wait(ttw)
        }
    }

    fillTable = (row, data) => {
        this.table().scrollIntoView()
        cy.wrap(data).each((item, index, list) => {
            this.fillTableValue(row, item)
        })
    }

    fillModalForm = (data) => {
        cy.wrap(data).each((item, index, list) => {
            this.fillModalValue(item)
        })
    }

    fillForm = (data) => {
        cy.wrap(data).each((item, index, list) => {
            this.fillValue(item)
        })
    }

    detachIfExist = () => {
        cy.wait(300).then(()=>{
            let isAttached = Cypress.$('[data-cy="remove-img"]')
            if (isAttached.length) {
                cy.wrap(isAttached).click({force: true})
            }
        })
    }

    messageBoxConfirm = async (confirm = true) => {
        cy.wait(1500)
        cy.get('body').then(($body) => {
            let modal = $body.find('div[role="dialog"]:visible').last()
            if (modal.length) {
                if (confirm === true) {
                    cy.wrap(modal).find('button:nth-child(2)').last().click({force: true})
                }
                if (confirm === false) {
                    cy.wrap(modal).find('button:nth-child(1)').last().click({force: true})
                }
            }
        })
    }
    alert = function(selector = ''){
        return cy.get(`[role="alert"]:visible ${selector}`)
    }
}