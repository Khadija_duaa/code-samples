// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --

import Amplify, { Auth } from 'aws-amplify'
import aws_exports from './../../../src/aws-exports';
import 'cypress-file-upload';

import LoginUI from './LoginUI.command'
import Validate from './Validate.command'
import e2eConfig from '/e2e-cypress.config.js'
Cypress.env('e2e', e2eConfig)

Amplify.configure(aws_exports)

// Amazon Cognito
Cypress.Commands.add('loginByCognitoApi', (username, password) => {
  const log = Cypress.log({
    displayName: 'COGNITO LOGIN',
    message: [`🔐 Authenticating | ${username}`],
    // @ts-ignore
    autoEnd: false,
  })

  log.snapshot('before')

  const signIn = Auth.signIn({ username, password })

  cy.wrap(signIn, { log: false, timeout: 8000 }).then((cognitoResponse) => {
    const keyPrefixWithUsername = `${cognitoResponse.keyPrefix}.${cognitoResponse.username}`

    window.localStorage.setItem(
      `${keyPrefixWithUsername}.idToken`,
      cognitoResponse.signInUserSession.idToken.jwtToken
    )

    window.localStorage.setItem(
      `${keyPrefixWithUsername}.accessToken`,
      cognitoResponse.signInUserSession.accessToken.jwtToken
    )

    window.localStorage.setItem(
      `${keyPrefixWithUsername}.refreshToken`,
      cognitoResponse.signInUserSession.refreshToken.token
    )

    window.localStorage.setItem(
      `${keyPrefixWithUsername}.clockDrift`,
      cognitoResponse.signInUserSession.clockDrift
    )

    window.localStorage.setItem(
      `${cognitoResponse.keyPrefix}.LastAuthUser`,
      cognitoResponse.username
    )

    window.localStorage.setItem('amplify-authenticator-authState', 'signedIn')
    log.snapshot('after')
    log.end()
  })
})

Cypress.Commands.add('loginByUI', (username, password) => {
  LoginUI(username, password)
})

Cypress.Commands.add('Validate', (element) => {
  Validate(element)
})

Cypress.Commands.add('hideCookieInfo', () => {
  cy.get('#termly-code-snippet-support').invoke('hide')
  cy.get('#termly-code-snippet-support')
      .should('have.css', 'display', 'none')
  cy.log('Hide Cookie Message')
})

Cypress.Commands.add('hideChatInfo', () => {
  cy.get('.intercom-borderless-frame').invoke('hide');
  cy.get('.intercom-borderless-frame')
      .should('have.css', 'display', 'none')
  cy.log('Hide Chat Info')
})

Cypress.Commands.add('menu_user_settings', (value) => {
  // User settings / company settings
  cy.get('#top')
      .find('.el-dropdown-link > i.uil-user')
      .realMouseUp();
  cy.get('ul.el-dropdown-menu')
      .should('have.css', 'position', 'absolute');
  cy.get('ul.el-dropdown-menu:visible')
      .contains('li', value)
      .click({force: true})
  cy.get('#top')
      .find('.el-dropdown-link > i.uil-user')
      .realMouseDown();
});

Cypress.Commands.add('menu_company_settings', (value) => {
  // User settings / company settings / Dropdown-lists
  cy.get('ul.el-dropdown-menu').should('have.css', 'display', 'none');
  cy.get('ul.settings-menu > li.el-menu-item:nth-child('+value+')').click({force: true});
});


Cypress.Commands.add('test_dropdownLists_addValue', (parent_position, child_position, value, scrollTo) => {
  // User settings / company settings / Dropdown-lists
  cy.wait(2000);
  cy.scrollTo(scrollTo.x, scrollTo.y);
  cy.wait(2000);
  cy.get('div[role="tablist"]')
      .find('.el-collapse-item:nth-child('+parent_position+')')
      .find('div[role="tabpanel"]')
      .find('.el-collapse-item__content > div.mb-4:nth-child('+child_position+') > .button-new-tag')
      .click({force:true});

  cy.get('div[role="tablist"]')
      .find('.el-collapse-item:nth-child('+parent_position+')')
      .find('div[role="tabpanel"]')
      .find('.el-collapse-item__content > div.mb-4:nth-child('+child_position+') > .input-new-tag')
      .find('input')
      .trigger('focus');

  cy.get('div[role="tablist"]')
      .find('.el-collapse-item:nth-child('+parent_position+')')
      .find('div[role="tabpanel"]')
      .find('.el-collapse-item__content > div.mb-4:nth-child('+child_position+') > .input-new-tag')
      .find('input')
      .realType(value);

  cy.get('div[role="tablist"]')
      .find('.el-collapse-item:nth-child('+parent_position+')')
      .find('div[role="tabpanel"]')
      .find('.el-collapse-item__content > div.mb-4:nth-child('+child_position+') > .input-new-tag')
      .find('input')
      .trigger('blur');

  cy.wait(6000);
});

Cypress.Commands.add('waitForLoaderToDisappear', () => {
  cy.get('.el-loading-mask').should('have.css', 'display', 'none')
});

let LOCAL_STORAGE_MEMORY = {};

Cypress.Commands.add("saveLocalStorage", () => {
  Object.keys(localStorage).forEach(key => {
    LOCAL_STORAGE_MEMORY[key] = localStorage[key];
  });
});

Cypress.Commands.add("restoreLocalStorage", () => {
  Object.keys(LOCAL_STORAGE_MEMORY).forEach(key => {
    localStorage.setItem(key, LOCAL_STORAGE_MEMORY[key]);
  });
});


//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
