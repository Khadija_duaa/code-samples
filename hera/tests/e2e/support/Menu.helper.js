class Helper {
    
    constructor(containerSelector){

        this.containerSelector = containerSelector;

        this.getElement = (childSelector) => {
            return cy.get(`${this.containerSelector} ${childSelector}`);
        }
    
        this.showDropdownMenu = (dropdownBtnSelector, dropdownItemsSelector) => {
            this.getElement(dropdownBtnSelector).scrollIntoView().trigger('mouseenter');
            return cy.get(`.el-dropdown-menu:visible ${ dropdownItemsSelector }`);
        }
        
        this.clickAndLeave  = (cyEl) => {
            cyEl.click().trigger('mouseleave', {force: true});
            cy.get('.el-dropdown-menu').invoke('hide');
            cy.wait(2000)
        }
    
        this.regexp = (text) => new RegExp(text.trim().replace(/ /g, ".*\n*" ), 'gm');
    }

}

export class Top extends Helper{

    constructor(){

        super(`[data-cy="top-header"]`);

        this.getPopper = (selector, text) => {
            this.showDropdownMenu(selector, 'li.el-dropdown-menu__item').as('menu');
            if(text){
                this.clickAndLeave( cy.get("@menu").contains(text) );
            }else{
                return cy.get("@menu");
            }
        };

        this.getNotifications = (text, childSelector, btnSelector) => {
            const cyParent = '[data-cy="notification-row"]';
            this.showDropdownMenu(`[data-cy="notifications-dropdown"]`, cyParent).as('menu');
            if(text){

                if(childSelector){
                    cy.get("@menu").find(childSelector).contains(this.regexp(text)).parents(cyParent).invoke('index').then(index => {
                        this.clickAndLeave( cy.get("@menu").find(btnSelector).eq(index) )
                    })
                }else{
                    cy.get("@menu").contains(this.regexp(text)).parents(cyParent).invoke('index').then(index => {
                        this.clickAndLeave( cy.get("@menu").find(btnSelector).eq(index) )
                    })
                }

            }else{
                return cy.get("@menu");
            }
        };

        this.viewNotification = (text, childSelector) => {
            return this.getNotifications(text, childSelector, '[data-cy="n-perform-action-btn"]');
        };

        this.dismissNotification = (text, childSelector) => {
            return this.getNotifications(text, childSelector, '[data-cy="n-dismiss-btn"]');
        };

    }

    selectFromCreate = (text) => this.getPopper(`[data-cy="create-dropdown"]`, text);

    selectFromImport = (text) => this.getPopper(`[data-cy="import-dropdown"]`, text);

    goToMessenger = () => this.getElement(`[data-cy="messenger-indicator-btn"]`).scrollIntoView().click();

    notifications = {

        view: (text) => this.viewNotification( text),

        viewByTitle: (text) => this.viewNotification( text , `[data-cy="n-title"]`),

        viewByDescription: (text) => this.viewNotification( text, `[data-cy="n-description"]`),

        viewByReleaseNotes: (text) => this.viewNotification( text, `[data-cy="n-release-notes"]`),

        
        dismiss: (text) => this.dismissNotification( text),

        dismissByTitle: (text) => this.dismissNotification( text , `[data-cy="n-title"]`),

        dismissByDescription: (text) => this.dismissNotification( text, `[data-cy="n-description"]`),

        dismissByReleaseNotes: (text) => this.dismissNotification( text, `[data-cy="n-release-notes"]`),


        dismissAll: ()=>  this.clickAndLeave( this.showDropdownMenu(`[data-cy="notifications-dropdown"]`, '[data-cy="n-dismiss-all-btn"]').scrollIntoView() ),

        viewAll: ()=>  this.clickAndLeave( this.showDropdownMenu(`[data-cy="notifications-dropdown"]`, '[data-cy="n-view-all-btn"]').scrollIntoView() )

    }

    selectFromUser = (text) => this.getPopper(`[data-cy="user-info-dropdown"]`, text);

}

export class Tab extends Helper{
    constructor(){
        super(`[data-cy="main-nav"]`);
    }

    goTo = (text, locationPathName, subTabSelector = null) => {
        this.getElement("> a").contains(this.regexp(text)).click({force: true});
        cy.wait(2000);
        if (subTabSelector) {
            cy.wait(1000)
            cy.get(`[data-cy='${subTabSelector}']`).click({force: true});
        }
        if(locationPathName){
            cy.location('pathname').should('eq', locationPathName);
        }
    }

}

export default class MenuHelper {
    static Top = new Top();
    static Tab = new Tab();
}