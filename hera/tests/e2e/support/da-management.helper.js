export function go_to_dalist_from_damanagement() {
    cy.get('[href="/da-management"]').should("be.visible").click({force: true})
    cy.get('[data-cy="damanagement-options"]').find('[data-cy="dalist-option"]').click({force: true})
    return {
        write_in_inputfilter(text) {
            cy.window().then(win => {
                const component = win.StaffIndex
                const Vue = component.$root;
                Vue.$set(component, 'search', text);
            })
        },
        select_in_filter_by_status(option) {

            cy.window().then(win => {
                const component = win.StaffIndex
                const Vue = component.$root;

                Vue.$set(component, 'statusChecked', option)
            })
        },
        go_to_newDA_page() {
            cy.get('[data-cy="dropdown-da-header-table"] > div').realHover()
            cy.get('[data-cy="add_da_option"]').click()
        },
        go_to_view_specific_DA_page(firstname, lastname) {
            cy.get('[data-cy="dalist-table"] tbody tr')
                .contains("td", firstname)
                .parent().scrollIntoView()
            cy.wait(1000)
            cy.get('[data-cy="dalist-table"] tbody tr')
                .contains("td", firstname)
                .parent()
                .within(() => {
                    cy.get('[data-cy="da_table_row_dropdown" ]').realHover()
                    cy.get('[data-cy="da_table_row_option_view"]').click()
                    cy.wait(2000)
                })
            let fullname = firstname + " " + lastname
            cy.get('[data-cy="da_page_fullname"]').contains(fullname).should("be.visible")

        },
        open_modal_change_status_of_scpecific_DA() {
        }
    }
}

export function alert_error_was_fired() {
    cy.get('[role="alert"]')
        .contains("p", "Please fill out all required fields and correct any errors.")
        .should("be.visible")
}

export function alert_success_was_fired(message) {
    cy.get('[role="alert"]')
        .contains("p", message)
        .should("be.visible")
}

export function fillout_form_to_create_da() {
    cy.wait(1000)
    cy.window().then(win => {
        const component = win.StaffNew
        const Vue = component.$root
        Vue.$set(component.formFields, "firstName", "test name")
        Vue.$set(component.formFields, "lastName", "test lastname")
        Vue.$set(component.formFields, "email", "cristian@gmail.com")
        Vue.$set(component.formFields, "phone", "938403789")
        Vue.$set(component.formFields, "transporterId", "88888888")
        Vue.$set(component.formFields, "dlExpiration", "09/30/2021")
    })

    return {
        crear(callback) {
            cy.get('[data-cy="staffnew_create_btn"]').click()
            cy.get('[data-cy="da_page_fullname"]').contains("test name test lastname").should("be.visible")
            callback()
        },
        cancelar() {
        },
        with_empty_fields(array_fields) {
            cy.window().then(win => {
                const component = win.StaffNew
                const Vue = component.$root
                for (let i = 0; i < array_fields.length; i++) {
                    Vue.$set(component.formFields, array_fields[i], "")
                }
            })

            return {
                crear() {
                    cy.get('[data-cy="staffnew_create_btn"]').click()
                }
            }
        }
    }
}
  