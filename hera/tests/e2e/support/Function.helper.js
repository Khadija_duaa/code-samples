Date.prototype.addHours = function (h) {
    this.setHours(this.getHours() + h);
    return this;
}
Date.prototype.addDays = function (days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}
export default class FunctionHelper {
    getCurrentDate(option) {
        let opt = {
            format: (typeof option.format === "undefined") ? 'Ymd' : option.format,
            separator: (typeof option.separator === "undefined") ? '-' : option.separator,
            GMT: (typeof option.GMT === "undefined") ? -5 : option.GMT,
            addDays: (typeof option.addDays === "undefined") ? null : option.addDays,
        }
        var d = new Date().addHours(opt.GMT);
        var make_date = [];
        for (var i = 0; i < opt.format.length; i++) {
            if (opt.format.charAt(i) === 'Y') {
                make_date[i] = d.getUTCFullYear();
            }
            if (opt.format.charAt(i) === 'm') {
                make_date[i] = ("0" + (d.getUTCMonth() + 1)).slice(-2);
            }
            if (opt.format.charAt(i) === 'd') {
                if (opt.addDays !== null) {
                    make_date[i] = ("0" + (d.getUTCDate() + opt.addDays)).slice(-2);
                } else {
                    make_date[i] = ("0" + d.getUTCDate()).slice(-2);
                }
            }
        }
        if (opt.separator === false) {
            return make_date
        } else {
            return make_date.join(opt.separator);
        }
    }

    static match = (text) => {
        return new RegExp(text.trim().replace(/ /g, ".*\n*" ), 'gm');
    }

    static join = (object, ...keys)=> keys.map(k=>object[k]).join(" ")
}