export class PageTask {
    verifyRouteTask() {
        return cy.location("pathname").should("eq", "/reports")
    }

    tableTask() {
        this.verifyRouteTask()
        return cy.get(".dashboard-card-header")
            .eq(1)
            .next()
            .find(".cursor-pointer")
            .first()
    }

    table() {
        return cy.get("table")
    }

    drowpDown() {
        return cy.get("body > .el-dropdown-menu.el-popper").eq(1)
    }

    buttonAddTask() {
        return cy.contains("Add Task")
    }
}

export class ModalTask {
    verifyIfIsVisible() {
        return cy.get('[aria-label="Add Task"] .el-dialog__title').should("be.visible")
    }

    inputAssignedTo() {
        return cy.get('[aria-label="Add Task"] input[type="text"]').eq(1)
    }

    inputDate() {
        return cy.get('[aria-label="Add Task"] input[type="text"]').eq(2)
    }

    inputTaskName() {
        return cy.get('[aria-label="Add Task"] input[type="text"]').eq(4)
    }

    inputCheckboxNotify() {
        return cy.get('[aria-label="Add Task"] input[type="checkbox"]').eq(0)
    }

    inputTaskDescription() {
        return cy.get('[aria-label="Add Task"] textarea')
    }

    optionsSelect() {
        cy.wait(3000)
        return cy.get("div.el-select-dropdown ul").eq(2)
    }

    calendar() {
        return cy.get('.el-picker-panel')
    }

    btnCancel() {
        return cy.get('[aria-label="Add Task"]  button').contains("span", "Cancel")
    }

    btnCreate() {
        return cy.get('[aria-label="Add Task"]  button').contains("span", "Create")
    }
}

export class ModalEditTask {
    verifyIfIsVisible() {
        return cy.get('[aria-label="Edit Task"] .el-dialog__title').should("be.visible")
    }

    inputAssignedTo() {
        return cy.get('[aria-label="Edit Task"] input[type="text"]').eq(1)
    }

    inputDate() {
        return cy.get('[aria-label="Edit Task"] input[type="text"]').eq(2)
    }

    inputTaskName() {
        return cy.get('[aria-label="Edit Task"] input[type="text"]').eq(4)
    }

    inputTaskDescription() {
        return cy.get('[aria-label="Edit Task"] textarea')
    }

    optionsSelect() {
        cy.wait(3000)
        return cy.get("div.el-select-dropdown ul").eq(2)
    }

    calendar() {
        return cy.get('.el-picker-panel')
    }

    btnCancel() {
        return cy.get('[aria-label="Add Task"]  button').contains("span", "Cancel")
    }

    btnUpdate() {
        return cy.get('[aria-label="Edit Task"]  button').contains("span", "Update")
    }
}

export class Dashboard {
    #select = (opt) => {
        cy.wait(2500)
        return cy.get('ul.el-dropdown-menu').last().contains(opt).should("be.visible")
    }

    Create = () => cy.get('#top').contains("span", "Create").focus()
    Create_Menu = () => ({
        Couseling: () => this.#select("Counseling"),
        DAIssue: () => this.#select("DA Issue"),
        DAKudo: () => this.#select("DA Kudo"),
        AccidentReport: () => this.#select("Accident Report"),
        InjuryReport: () => this.#select("Injury Report"),
        Task: () => this.#select("Task")
    })

    Import = () => cy.get('#top').contains("span", "Import").focus()
    Import_Menu = () => ({
        DailyRosteExcel: () => this.#select("Daily Roster Excel"),
        DailyPerformanceData: () => this.#select("Daily Performance Data"),
        WeeklyPerformanceData: () => this.#select("Weekly Performance Data")
    })

    DropdownReports = () => cy.get('#top').contains("span", "Reports").focus()
    DropdownReports_Menu = () => ({
        DriverLicenses: () => this.#select("Driver Licenses"),
        Accidents: () => this.#select("Accidents"),
        Injuries: () => this.#select("Injuries"),
        NetradyneAlerts: () => this.#select("Netradyne Alerts"),
        DAsOptedOut: () => this.#select("DAs Opted Out"),
        DAPerformance: () => this.#select("DA Performance")
    })

    DropdownUserIcon = () => cy.get('#top .uil-user').parent().focus()
    DropdownUserIcon_Menu = () => ({
        AccountDetails: () => this.#select("Account Details"),
        CompanySettings: () => this.#select("Company Settings"),
        ManageUsers: () => this.#select("Manage Users"),
        ManageTask: () => this.#select("Manage Tasks"),
        ManageDevices: () => this.#select("Manage Devices"),
        HelpCenter: () => this.#select("Help Center"),
        ChangePassword: () => this.#select("Change Password"),
        Logout: () => this.#select("Logout"),
    })

    Performance = () => cy.get('[href="/performance"]').should("be.visible")

    DailyRoster = () => cy.get('[href="/daily-roster"]').should("be.visible")

    DAManagement = () => cy.get('[href="/da-management"]').should("be.visible")

    Coaching = () => cy.get('[href="/coaching"]').should("be.visible")

    Vehicles = () => cy.get('[href="/vehicles"]').should("be.visible")

    MessageIcon = () => cy.get("#top .uil-comment-alt-dots").eq(0).parent()

    BellIcon = () => cy.get("#top .uil-bell").eq(0).parent()

    ModalBellIcon = () => ({
        verifyNewNotification: () => cy.get('ul.el-dropdown-menu').last().contains(/Assigned Task:/g).eq(0),
        ViewAllNotification: () => this.#select("View All Notifications")
    })

    quitHover = (time = 0) => {
        cy.wait(time)
        cy.get("#top").eq(0).realClick()
    }

    DrowpdownUser(){
        return cy.get('#top i.uil-user').parent()
    }

    DrowpdownUserMenu(){
        return cy.get('ul.el-dropdown-menu > div.user-title').parent()
    }
}

export class Alert {
    title(){
        return cy.get(".el-notification__title")
    }
}

export default {ModalEditTask, ModalTask, PageTask, Dashboard, Alert}