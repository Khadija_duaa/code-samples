
import MenuHelper from './Menu.helper'
module.exports = function (username, password){

    cy.visit('/', {log: false})
    cy.get('#email:visible', {log: false}).invoke({log: false}, "val", username)
    cy.get('#password:visible', {log: false}).invoke({log: false}, "val", password)
    cy.get('form', {log: false}).submit({log: false})

    const superadmin = Cypress.env("cognito_superadmin")

    if(superadmin === 'true'){
    /** Make sure the user account has super admin privileges */

        let login_log = Cypress.log({
            displayName: "LOGIN UI",
            message: `🔐 **[_Authenticating as_ Super Administrator.]()**`,
            autoEnd: false
        })

        MenuHelper.Top.selectFromUser().then(menu =>{
            if(!menu.is(":contains(System Admin)")){
                throw new Error(`**The user account must have more privileges:** \`Make sure the user account ${username} has Super Administrator privileges.\``);
            }
            login_log.end()
        })

        MenuHelper.Top.selectFromUser("System Admin")
        
        cy.wait(1000, {log: false})
        
        const {
            super_admin_test: {
                Tenant
            }
        } = Cypress.env("e2e")

        const tabName ="Tenants"
        
        cy.get('[data-cy="system-admin-settings"]')
            .contains(".el-menu-item", tabName)
            .scrollIntoView({log: false})
            .click({log: false})
            .log( `Tenant: **[${Tenant}.]()**`)
        
        cy.get(`[data-cy="tenant-filter"]`)
            .scrollIntoView({log: false})
            .type(Tenant)
            .blur({log: false})
            .wait(500, {log: false})

        cy.get("table > tbody:visible", {log: false})
            .contains("tr", Tenant, {log: false})
            .scrollIntoView({log: false})
            .wait(1000, {log: false})
            .find(".el-dropdown:visible", {log: false})
            .scrollIntoView({log: false})
            .click({log: false})
            .wait(500, {log: false})

        const action = "Support Administrator Login"

        cy.get(".el-dropdown-menu:visible", {log: false})
            .contains("li", action, {log: false})
            .scrollIntoView({log: false})
            .wait(500, {log: false})
            .click({force: true, log: false})
            .then(()=>{
                login_log = Cypress.log({
                    displayName: "LOGIN",
                    message: `🔐 **[_Trying to log into_ ${Tenant} _as_ Support Administrator.]()**`,
                    autoEnd: false
                })
            })
            .wait(4000, {log: false})

        cy.log(`Search alert with message:`)
        cy.contains("You are currently logged into this DSP as Support Administrator.")

        cy.then(() => {
            login_log.end()
            assert(1,`_Session started on_ **${Tenant}** _as_ **Support Administrator**.`)
        })

        cy.get("[data-cy='user-info-dropdown']").trigger('mouseenter', {log: false})
        cy.get(".el-dropdown-menu:visible .user-title").then(usr => {
            assert(1,"**Support Administrator:**")
            cy.wrap(usr,{log: false}).each(el => assert(1, `${el.text()}`))
            cy.get('.el-dropdown-menu:visible', {log: false}).invoke({log: false}, 'hide')
        })
        cy.wait(4000, {log: false})

    }else{
        const login_log = Cypress.log({
            displayName: "LOGIN UI",
            message: `🔐 **[_Authenticating._]()**`,
            autoEnd: false
        })
        cy.then(() => login_log.end())
    }

    // cy.saveLocalStorage();
    
    // cy.visit('/')
    // cy.location('pathname').should('eq', '/')
    // cy.get('input[name="email"]').type(username)
    // cy.get('input[name="password"]').type(password)
    // cy.get('form').submit()
    // cy.wait(2000);
    // cy.saveLocalStorage();
}