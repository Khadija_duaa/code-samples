export default class StripeHelper{

    static getElement(cySelector, fieldName, datacy = true){

        if (Cypress.config('chromeWebSecurity')) {
            throw new Error('To get stripe element `chromeWebSecurity` must be disabled');
        }

        let selector;

        if(datacy){
            selector = `[data-cy='${cySelector}']`
        }else{
            selector = cySelector
        }

        return cy.get(`${selector} iframe`)
            .its('0.contentDocument.body')
            .should('not.be.empty')
            .then(cy.wrap)
            .find(`input[data-elements-stable-field-name="${fieldName}"]`)
    }

    static getCardNumber = (cySelector, datacy)=> this.getElement(cySelector, "cardNumber", datacy)
    static getCardExpiry = (cySelector, datacy)=> this.getElement(cySelector, "cardExpiry", datacy)
    static getCardCvc = (cySelector, datacy)=> this.getElement(cySelector, "cardCvc", datacy)
    static getPostalCode = (cySelector, datacy)=> this.getElement(cySelector, "postalCode", datacy)
}