//* User data to edit.
let email = "login"
let user = "user@ctdevelopers.com"
let password = "password"

describe('HERA-1158', () => {
    context('Add Counselings permission.', () => {
        beforeEach(function() {
            cy.loginByUI(Cypress.env("cognito_username"), Cypress.env("cognito_password"));
        });
        
        it('Add Counselings permission.', function () {
            cy.get('[data-cy="user-info-dropdown"]').click()
            cy.wait(4000);
            cy.get('[data-cy="company-settings-option"]').click()
            cy.wait(4000);
            cy.get('[data-cy="user-tab"]').click()
            cy.wait(4000);
            cy.get('[data-cy="filter"]').click()
            cy.wait(4000);
            cy.get('[data-cy="filter"]').focus().type(email)
            cy.wait(4000);
            cy.get('[data-cy="ellipsis-icon"]').click()
            cy.wait(4000);
            cy.get('[data-cy="edit-user"]').click()
            cy.wait(4000);
            cy.scrollTo(0, 600) 
            cy.wait(4000);
            cy.get('#fullAccess').click({force: true})
            cy.wait(4000);
            cy.scrollTo(0, 1200) 
            cy.wait(4000);
            cy.get('#dailyRostering').click({force: true})
            cy.wait(4000);
            cy.get('#customLists').click({force: true})
            cy.wait(4000);
            cy.get('#manageLabels').click({force: true})
            // cy.wait(4000);
            // cy.scrollTo(0, 10) 
            cy.wait(4000);
            cy.get('[data-cy="update-button"]').click()
            cy.wait(4000);
            cy.get('.el-message-box > .el-message-box__btns > .el-button--primary').click({force: true})
            cy.wait(4000);
            cy.get('[data-cy="user-info-dropdown"]').click()
            cy.wait(4000);
            cy.get('[data-cy="logout-option"]').click()
            cy.wait(4000);
            cy.loginByUI(user, password);
            cy.wait(8000);
            cy.get('[data-cy="user-info-dropdown"]').click()
            cy.wait(4000);
            cy.get('[data-cy="company-settings-option"]').click()
            cy.wait(4000);
        })
    })
})