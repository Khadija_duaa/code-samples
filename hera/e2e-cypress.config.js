import {join} from '/tests/e2e/support/Function.helper.js'

module.exports = (()=>{

    /** Create DA form */
    const DeliveryAssociate = {
        FirstName: "Hera",
        LastName: "TestDA",
        Status: "Active",
        Email: "test@add.associate",
        Phone: "2022222222",
        TranporterID: "0000444",
        DLExpiration: "12/11/2023", /* mm/dd/YYYY */
        HireDate: "12/25/2022", /* mm/dd/YYYY */
        DOB: "12/25/2001", /* mm/dd/YYYY */
        AuthorizedToDrive: ["Standard Parcel Small"],
        AuthorizedOver10kLBS: false,
        CustomDeliveryVan: true
    }

    /** Create DA form (a DA as supervisor) */
    const DeliveryAssociate_Supervisor = {
        FirstName: "TestDA",
        LastName: "Supervisor",
        Status: "Active",
        Email: "test@add.supervisorassociate",
        Phone: "2025555555",
        TranporterID: "00007777",
        DLExpiration: "08/11/2023", /* mm/dd/YYYY */
        HireDate: "12/25/2022", /* mm/dd/YYYY */
        DOB: "12/25/2001", /* mm/dd/YYYY */
        AuthorizedToDrive: ["Standard Parcel Large"],
        AuthorizedOver10kLBS: false,
        CustomDeliveryVan: true
    }

    /** Filter DA (for editing or deletion tests)*/
    const DeliveryAssociate_Filter = join(DeliveryAssociate, "FirstName", "LastName")

    const Status_DA = {
        Status: "Onboarding",
        Date: "12/22/2021",
        Reason: "Test test..."
    }

    /** Create Device form */
    const Device = {
        Name: "Test Device 2023",
        Status: "Active",
        Phone: "123456789",
        Carrier: "AT&T",
        Notes: "Some notes.."
    }

    /** Edit Device form */
    const Device_ED = {
        ...Device,
        Carrier: "Verizon",
        Notes: "Edited.."
    }

    /** Filter device (for editing or deletion tests)*/
    const Device_Filter = Device.Name

    /** Create vehicle form */
    const Vehicle1 = {
        Name: "Hera VehicleX1",
        ParkingSpace: "p1",
        AssignedDevice: Device_Filter,
        StartDate: "12/18/2022", /* mm/dd/YYYY */
        EndDate: "12/25/2023", /* mm/dd/YYYY */
        LicensePlate: "ABC-VX1",
        LicensePlateExpiration: "12/26/2023", /* mm/dd/YYYY */
        Ownership: "Rent",
        State: "Texas",
        Type: "CONVERTIBLE",
        VIN: "TESTHERAVINP11111",
        Make: "NBSDevelopers",
        Model: "Hera 2021",
        GasCardLast6: "111111",
        Company: "Enterprise",
        Year: "2021",
        RentalAgreementNumber: "HVL50/5000/2021",
        Over10kLBS: true,
        CustomDeliveryVan: true,
        Notes: "HERA test notes x1..."
    }
    const Vehicle2 = {
        Name: "Hera VehicleX2",
        ParkingSpace: "p2",
        AssignedDevice: Device_Filter,
        StartDate: "12/18/2022", /* mm/dd/YYYY */
        EndDate: "12/25/2023", /* mm/dd/YYYY */
        LicensePlate: "ABC-VX2",
        LicensePlateExpiration: "12/26/2023", /* mm/dd/YYYY */
        Ownership: "Rent",
        State: "New York",
        Type: "TestType",
        VIN: "TESTHERAVINP22222",
        Make: "NBSDevelopers",
        Model: "Hera 2021",
        GasCardLast6: "222222",
        Company: "Enterprise",
        Year: "2021",
        RentalAgreementNumber: "HVL50/5000/2021",
        Over10kLBS: true,
        CustomDeliveryVan: true,
        Notes: "HERA test notes x2..."
    }
    const Vehicle3 = {
        Name: "Hera VehicleX3",
        ParkingSpace: "p3",
        AssignedDevice: Device_Filter,
        StartDate: "12/18/2022", /* mm/dd/YYYY */
        EndDate: "12/25/2023", /* mm/dd/YYYY */
        LicensePlate: "ABC-VX3",
        LicensePlateExpiration: "12/26/2023", /* mm/dd/YYYY */
        Ownership: "Rent",
        State: "New York",
        Type: "Custom",
        VIN: "TESTHERAVINP33333",
        Make: "NBSDevelopers",
        Model: "Hera 2021",
        GasCardLast6: "333333",
        Company: "Enterprise",
        Year: "2021",
        RentalAgreementNumber: "HVL50/5000/2021",
        Over10kLBS: true,
        CustomDeliveryVan: true,
        Notes: "HERA test notes x3..."
    }

    /** Edit vehicle 1 form */
    const Vehicle1_ED = {
        ...Vehicle1,
        State: "New York",
        Over10kLBS: false,
        Notes: "vehicle 1 Edited.."
    }
    
    /** Filter vehicle (for editing or deletion tests)*/
    const Vehicle_Filter = Vehicle1.Name

    const BulkUpdates_DA = {
        DAsFullName: join(DeliveryAssociate, "FirstName", "LastName"),
        Status: "Active",
        AuthorizedOver10kLBS: true,
        PrimaryVehicle: Vehicle1_ED.Name,
        SecondaryVehicle: Vehicle2.Name,
        TertiaryVehicle: Vehicle3.Name,
        SupervisedBy: join(DeliveryAssociate_Supervisor, "FirstName", "LastName")
    }

    /** Create Document form */
    const Documents = {
        Type: "Accident Record",
        Notes: "Hera Test Document...",
        Date: "12/22/2021",
        File: "image_from_fixture.png"  /** tests/e2e/fixtures/{file} */
    }

    /** Edit Document form */
    const Documents_ED = {
        ...Documents,
        Type: "Injury Record",
        Notes: "Hera Test Document edited",
        Date: "12/25/2021"
    }

    /** Create DrugTest form */
    const DrugTests = {
        Date: "12/27/2021",
        Location: "Somewhere",
        Results: "Pass",
        File: "image_from_fixture.png"  /** tests/e2e/fixtures/{file} */
    }

    /** Edit DrugTest form */
    const DrugTests_ED = {
        ...DrugTests,
        Date: "12/28/2021",
        Location: "Metro",
        Results: "Fail",
        File: "image_from_fixture.png" 
    }

    /** Create MedicalCard form */
    const MedicalCards = {
        ReceivedDate: "12/27/2021",
        ExpirationDate:  "12/28/2021",
        File: "image_from_fixture.png"  /** tests/e2e/fixtures/{file} */
    }

    /** Edit MedicalCard form */
    const MedicalCards_ED = {
        ...MedicalCards,
        ReceivedDate: "12/28/2021",
        ExpirationDate:  "12/29/2021",
        File: "image_from_fixture.png" 
    }

    /** Create Uniform form */
    const Uniforms = {
        Type: "Mens Pants",
        Size: "L",
        QtyIssued: "3",
        IssueDate: "12/27/2021",
        ReturnedDate:  "12/28/2021"
    }

    /** Edit Uniform form */
    const Uniforms_ED = {
        ...Uniforms,
        Type: "Hat",
        Size: "M",
        QtyIssued: "1",
        IssueDate: "12/28/2021",
        ReturnedDate:  "12/29/2021"
    }

    const ConpanyInfo_DA = {
        SupervisedBy: join(DeliveryAssociate_Supervisor, "FirstName", "LastName"),
        PreferredDaysOff: ["Friday","Saturday","Sunday"],
        HourlyStatus: "Seasonal",
        Hired: "12/22/2021",
        FinalCheckIssued: "12/23/2021"
    }

    const DrivingInfo_DA = {
        DLExpiration: "12/25/2021",
        MotorVehicleReport: "12/20/2021",
        GasCardPIN: "0000001",
        PrimaryVehicle: Vehicle1_ED.Name,
        SecondaryVehicle: Vehicle2.Name,
        TertiaryVehicle: Vehicle3.Name,
        AuthorizedOver10kLBS: true,
        CustomDeliveryVan: true
    }

    const HeraScore_DA = {
        HeraScoreRange: "90"
    }

    const Onboarding_DA = {
        Offer: {label: "Offer", selector: "offer", type: "dropdown", value: "Sent", dates: [{ label: "Sent Date", value: "2023-08-10" }, { label: "Signed Date", value: "2023-08-11" }]},
        AmazonProfile: {label: "Amazon Profile", selector: "amazon-profile", type: "dropdown", value: "Profile Created", dates: [{ label: "Sent Date", value: "2023-08-10" }, { label: "Completed Date", value: "2023-08-11" }]},
        BackgroundCheck: {label: "Background Check", selector: "background-check", type: "dropdown", value: "Report Review", dates: [{label: "Initiated Date", value: "2023-08-07"}, {label: "Cleared Date", value: "2023-08-08"}]},
        _10kLBSApplication: {label: "10,000 LBS Application", selector: "10kLBS-application", type: "dropdown", value: "Link Sent", dates: [{ label: "Sent Date", value: "2023-08-10" }, { label: "Completed Date", value: "2023-08-11" }]},
        TrainingVideos: {label: "Training Videos", selector: "training-videos", type: "dropdown", value: "In Progress", dates: [{ label: "Completed Date", value: "2023-08-10" }]},
        DrugTest: {label: "Drug Test", selector: "drug-test", type: "dropdown", value: "Collected", dates: [{ label: "Ordered Date", value: "2023-08-10" }, {label: "Completed Date", value: "2023-08-12"}]},
        AddedtoPayroll: {label: "Added to Payroll", selector: "added-to-payroll", type: "dropdown", value: "In Progress", dates: [{ label: "Completed Date", value: "2023-08-10" }]},
        I9Verification: {label: "I9 Verification", selector: "I9-verification", type: "dropdown", value: "In Progress", dates: [{ label: "Completed Date", value: "2023-08-10" }]},
        DSPOrientation: {label: "DSP Orientation", selector: "dsp-orientation", type: "dropdown", value: "Scheduled", dates: [{ label: "Scheduled Date", value: "2023-08-10" },{ label: "Completed/Failed Date", value: "2023-08-10" },  { label: "Start Date", value: "2023-08-10" }, { label: "End Date", value: "2023-08-10" }]},
        InPersonAmazonTraining: {label: "In-Person Amazon Training", selector: "in-person-amazon-training", type: "dropdown", value: "Scheduled", dates: [{ label: "Scheduled Date", value: "2023-08-10" }, { label: "Completed/Failed Date", value: "2023-08-10" }, { label: "Start Date", value: "2023-08-10" }, { label: "End Date", value: "2023-08-10" }]},
        CustomDeliveryVanTraining: {label: "Custom Delivery Van Training", selector: "custom-delivery-van-training", type: "dropdown", value: "Scheduled", dates: [{ label: "Scheduled Date", value: "2023-08-10" }, { label: "Completed Date", value: "2023-08-10" }]},
        _10kLBSOnlineTraining: {label: "10,000 LBS Online Training", selector: "10kLBS-online-training", type: "dropdown", value: "Scheduled", dates: [{ label: "Scheduled Date", value: "2023-08-10" }, { label: "Completed Date", value: "2023-08-10" }]},
        UniformsIssued: {label: "Uniforms Issued", selector: "uniforms-issued", type: "dropdown", value: "Not Required", dates: null},
        _10kLBSRoadTesting: {label: "10,000 LBS Road Testing", selector: "10kLBS-road-testing", type: "dropdown", value: "Scheduled", dates: [{ label: "Scheduled Date", value: "2023-08-10" }, { label: "Completed Date", value: "2023-08-10" }]},
        MentorAccount: {label: "Mentor Account", selector: "mentor-account", type: "dropdown", value: "Invite Sent", dates: [{ label: "Invited Date", value: "2023-08-10" }]},
        GasCardPIN: {label: "Gas Card PIN Created", selector: "gas-card-pin-created", type: "dropdown", value: "Not Completed", dates: [{ label: "Completed Date", value: "2023-08-10" }]},
        RideAlong: {label: "Ride Along", selector: "ride-along", type: "dropdown", value: "Not Required", dates: [{ label: "Scheduled Date", value: "2023-08-10" }]},
        MedicalCard: {label: "Medical Card", selector: "medical-card", type: "dropdown", value: "Not Issued", dates: [{ label: "Issued Date",value: "2023-08-10" }]},
    }

    const PersonalInfo_DA = {
        ...DeliveryAssociate,
        EmailUpdates: true,
        TextUpdates: true,
        DOB: "12/22/2021",
        Gender: "Other",
        Pronouns: "They/Them"
    }

    const DAPreferences = {
        Communication:{
            EmailUpdates: false,
            TextUpdates: false
        },
        ImportName: "Hera test - some import name"
    }

    const ReplaceWithStandByDA = {
        ReasonforReplacement: "Late, With Call",
        DeliveryAssociate: "Hera Test",
    }

    const HelperDA = {
        Helper: join(DeliveryAssociate_Supervisor, "FirstName", "LastName"),
        Status: "On Time"
    }

    /** Create DA Assignment form */
    const DailyRoster = {
        DeliveryAssociate: DeliveryAssociate_Filter,
        Vehicle: Vehicle2.Name,
        Device: Device_Filter,
        Route: "a best route ever",
        Notes: "some note",
        Staging: "a 1st staging",
        ParkingSpace: "10000",
        Status: "Called Out",
        Time:  {h: '11 pm', m: '11'},
    }

    /** Edit DA Assignment form */
    const DailyRoster_ED = {
        ...DailyRoster,
        Vehicle: Vehicle3.Name,
        Device: Device_Filter,
        Route: "a best route ever x2",
        Notes: "some note...",
        Status: "On Time",
        Time:  {h: '10 pm', m: '10'}
    }

    const StandUpAnnouncements = {
        Message: "Message...",
        File: "image_from_fixture.png"
    }

    /** Create StandbyDA form */
    const StandbyDA = {
        DeliveryAssociate: DeliveryAssociate_Filter,
        Status: "Late, No Call"
    }
    
    /** Edit StandbyDA form */
    const StandbyDA_ED = {
        ...StandbyDA,
        DeliveryAssociate: join(DeliveryAssociate_Supervisor, "FirstName", "LastName"),
        Status: "On Time"
    }

    /** Create DA Issue Information form */
    const DAIssueInformation = {
        DAName: join(DeliveryAssociate, "FirstName", "LastName"),
        DateofOccurrence: "01/03/2022",
        Time: "07:07PM",
        AdditionalInformation: "Some note..",
        Type: "Amazon Violation/Defect",
        Violation_Defect_Type: "Defects",
        Violation_Defect_Station: "007",
        Violation_Defect_TrackingNumber: "0000000000",
        Violation_Defect_InfractionType: "Incorrect Scan",
        Violation_Defect_PictureOfDelivery: "image_from_fixture.png",
        AppealInformation_Date: "01/05/2022",
        AppealInformation_ResolvedDate: "01/05/2022",
        AppealInformation_Status: "Pending",
        AppealInformation_Notes: "Appeal notes.."
    }

    /** Edit DA Issue Information form */
    const DAIssueInformation_ED = {
        ...DAIssueInformation,
        AppealInformation_Status: "Approved",
    }

    /** Create Task form */
    const Task = {
        DueDate: "12/12/2023",
        AssignedTo: "Test User",
        Status: "Pending",
        TaskName: "Demo - Hera Test",
        NotifyAssignedTo: true,
        RemindAssignedTo: true,
        RemindAssignedToDate: {date:"2023-12-12", time: "10:30"},
        Description: "Some description"
    }

    /** Edit Task form */
    const Task_ED = {
        ...Task,
        TaskName: "Hera Test edited",
    }

    const ImportWeeklyPerformanceData = {
        File: 'weekly-import-example.pdf',
        Type: "Scorecard",
        Week: "10"
    }

    /** Create Counseling form */
    const Counseling = {
        TeamLeader: "Jose Jollja",
        DateofOccurrence: "01/12/2022",
        DAName: DeliveryAssociate_Filter,
        CounselingType: "Informal Warning",
        StatementOfTheproblem: "statement..",
        PriorDiscussionSummary: "DiscussionSummary",
        SummaryOfCorrectiveAction: "SummaryOfCorrectiveAction",
        ConsequencesOfFailure: "Consequences",
        DANotes: "Some notes...",
        Image: "image_from_fixture.png"
    }

    /** Edit Counseling form */
    const Counseling_ED = {
        ...Counseling,
        CounselingType: "Final Warning",
        StatementOfTheproblem: "statement..  -> EDITED",
        PriorDiscussionSummary: "DiscussionSummary  -> EDITED",
        SummaryOfCorrectiveAction: "SummaryOfCorrectiveAction  -> EDITED",
        ConsequencesOfFailure: "Consequences  -> EDITED",
    }

    
    /** Create Injury form */
    const Injury = {
        DeliveryAssociate: DeliveryAssociate_Filter,
        DAHireDate: "01/12/2022",
        DAAddress_Street: "Rockville 105, DawsonPort",
        DAAddress_City: "DawsonPort",
        DAAddress_State: "Rockville",
        DAAddress_Zip: "107428",
        Gender: "Male",
        CompletedByTitle: "Supervisor",
        CompletedByPhone: "987654321",
        DateOfBirth: "01/12/2022",
        InjuryDate: "01/12/2022",
        CaseNumber: "007",
        DateOfDeath: "01/12/2022",
        Physician: "Shaun Murphy",
        TimeShiftStarted: "07:07AM",
        PhysicianFacility: "good hope",
        InjuryTime: "07:07AM",
        InjuryTimeIsUnknown: false,
        FacilityAddress_Street: "rockport 105, NFSMW",
        FacilityAddress_city: "NFSMW",
        FacilityAddress_State: "NFS",
        FacilityAddress_Zip: "150150",
        DATreatedInER: "Yes",
        DAHospitalizedOvernight: "No",
        BeforeIncident: "only drive a vehicle",
        WhatHappened: "It was an accident",
        InjuryOrIllness: "none",
        InternalCompanyNotes: "company notes for injury",
        objectOrSubstance: "no registry"
    }

    /** Edit Injury form */
    const Injury_ED = {
        ...Injury,
        DAAddress_Zip: "107430",
        CompletedByTitle: "Kevin Burnout",
        CompletedByPhone: "+51 999 888 666"
    }

    /** Create Kudo form */
    const Kudo = {
        DA: DeliveryAssociate_Filter,
        DateOfOccurrence: "01/12/2022",
        Type: "Rescuer",
        Notes: "A default kudo text"
    }

    /** Edit Kudo form */
    const Kudo_ED = {
        ...Kudo,
        DateOfOccurrence: "01/13/2022",
        Notes: "A default kudo text EDITED"
    }

    const SubscriptionPlan = {
        FirstName: "Hera",
        LastName: "Subscription Test",
        Email: "smith@hera.app",
        Address: "185 Berry ST",
        City: "San Francisco",
        State: "CA",
        Zip: "94107",
        CardNumber: "4242424242424242",
        Expiration: "11/22",
        CVC: "123"
    }

    const CompanyDetail = {
        ShortCode: "e2e",
        DispatchPhoneNumber: "(123)456-7890",
        BillingEmail: "athena@herasolutions.app",
        Address_Line1: "23 Main St",
        Address_Line2: "Suite #123",
        Address_City: "Los Angeles",
        Address_State: "CA",
        Address_Zip: "90056"
    }

    const CompanyDetailCard = {
        Logo: "image_from_fixture.png",
        AutomaticallyTimeZone: true,
        AccidentRecordForm: "image_from_fixture.png",
        WorkmansCompForm: "image_from_fixture.png"
    }

    const DropdownList_Coaching = {
        IssueType: "Custom Issue Type",
        KudoType:  "Custom Kudo Type"
    }

    const DropdownList_DA = {
        Gender: "Female x2",
        HourlyStatus:  "Full Time x2",
        Pronoun: "He/Him x2",
        UniformTypes: "Custom pants x2"
    }

    const DropdownList_Device = {
        DeviceCarrier: "Movistar"
    }

    const DropdownList_VehicleMaintenance = {
        MaintenanceServices: "Air Filter Replacement x2",
        ServiceLocation: "Jiffy Lube - 123 Main Street x2"
    }

    const DropdownList_Vehicle = {
        VehicleCompany: "Alamo x2",
    }

    /** Create User form */
    const User = {
        FirstName: 'E2E',
        LastName: 'QA',
        Email: 'e2e.qa@hera.com',
        Phone: '2022222222'
    }

    /** Edit User form */
    const User_ED = {
        ...User,
        Phone: '2023333333'
    }

    const BulkUpdates_VM = {
        VehicleNameFilter: Vehicle1_ED.Name,
        ParkingSpace: "10000",
        AssignedDevice: Device_ED.Name,
        Status: "Active",
        Date: "01/07/2022",
        ReasonForChange: "Testing"
    }

    const Accident = {
        DeliveryAssociate: DeliveryAssociate_Filter,
        AccidentDate: "01/06/2022",
        Vehicle: Vehicle2.Name,
        AtFault: "Unknown",
        LocationStreet: "680 SUTTER ST #104 [HERA-Test]",
        LocationCity: "SAN FRANCISCO [HERA-Test]",
        LocationState: "CALIFORNIA [HERA-Test]",
        LocationZip: "107428",
        DrugTestDate: "01/07/2022",
        TestResults: "Pass",
        ClaimNumber: "00000000 [HERA-Test]",
        PoliceDepartment: "Phoenix [HERA-Test]",
        VerifiedDate: "01/07/2022",
        PoliceOfficerName: "Anonymous [HERA-Test]",
        Notes: "Some note [HERA-Test]",
        PoliceReportNumber: "+51-999999999 [HERA-Test]",
        ScannedReport: "image_from_fixture.png",
        AccidentImage:"image_from_fixture.png"
    }

    const DamageHistory = {
        Vehicle: Vehicle2.Name,
        DA: DeliveryAssociate_Filter,
        Damage: "Some damage",
        Severity: "Major",
        Date: "01/12/2022",
        DAIssue: true,
        Counseling: true,
        Notes: "Test notes...",
        Images: "image_from_fixture.png"
    }

    const DamageHistory_ED = {
        ...DamageHistory,
        Damage: "Some damage EDITED",
        Severity: "Minor",
        Date: "01/12/2022",
        Notes: "Some notes..."
    }

    const Reminder = {
        Vehicle: Vehicle2.Name,
        Services: [
            "Camera",
            "Check Engine Light Service",
            "Door lock"
        ],
        DueByDate: "01/12/2022",
        DueByMileage: "10000"
    }

    const OdometerReading = {
        Mileage: "10000",
        Date: "01/13/2022",
        Time: "07:30",
        Type: "End of route"
    }

//exports
    return {
        "super_admin_test":{
            Tenant: "E2E Testing"
        },
        "filters":{
            Device_Filter,
            DeliveryAssociate_Filter,
            Vehicle_Filter
        },
        "da_management":{
            "bulk_updates":{
                BulkUpdates_DA
            },
            "da_list":{
                "view_da":{
                    "documents":{
                        Documents,
                        Documents_ED
                    },
                    "drug_tests":{
                        DrugTests,
                        DrugTests_ED
                    },
                    "medical_card":{
                        MedicalCards,
                        MedicalCards_ED
                    },
                    "uniforms":{
                        Uniforms,
                        Uniforms_ED
                    },
                    ConpanyInfo_DA,
                    DrivingInfo_DA,
                    HeraScore_DA,
                    Onboarding_DA,
                    PersonalInfo_DA,
                    DAPreferences
                },
                DeliveryAssociate,
                DeliveryAssociate_Supervisor,
                Status_DA
            }
        },
        "daily_rostering":{
            "daily_roster_table":{
                ReplaceWithStandByDA,
                HelperDA,
                DailyRoster,
                DailyRoster_ED
            },
            "dispatchers_daily_notes":{
                DispatchersDailyNotes: "Some text notes"
            },
            "managers_daily_notes":{
                ManagersDailyNotes: "Some text notes"
            },
            "stand_up_announcements":{
                StandUpAnnouncements
            },
            "standby_da_table":{
                StandbyDA,
                StandbyDA_ED
            }
        },
        "performance_coaching":{
            "view_da_issue":{
                "view_da_issue":{
                    DAIssueInformation,
                    DAIssueInformation_ED
                }
            },
            "performance_tracking":{
                "imported_performance_data":{
                    ImportWeeklyPerformanceData
                }
            }
        },
        "tasks_and_reports":{
            Task,
            Task_ED,
            Kudo,
            Kudo_ED,
            Injury,
            Injury_ED,
            Counseling,
            Counseling_ED,
        },
        "user_settings":{
            "account_details":{
                SubscriptionPlan
            },
            "company_details":{
                CompanyDetail,
                CompanyDetailCard,
            },
            "dropdown_lists":{
                "coaching":{
                    DropdownList_Coaching
                },
                "delivery_associates":{
                    DropdownList_DA
                },
                "devices":{
                    DropdownList_Device
                },
                "vehicle_maintenance":{
                    DropdownList_VehicleMaintenance
                },
                "vehicles":{
                    DropdownList_Vehicle
                }
            },
            "company_settings":{
                "users":{
                    User_Filter: User.Email,
                    User,
                    User_ED
                }
            },
            "manage_devices":{
                Device,
                Device_ED
            }
        },
        "vehicle_management": {
            "bulk_updates": {
                BulkUpdates_VM
            },
            "dashboard":{
                "accidents_damage":{
                    Accident,
                    DamageHistory,
                    DamageHistory_ED
                },
                "upcoming_maintenance_reminders":{
                    DropDown: "Test DropDown",
                    Reminder
                }
            },
            "vehicle_list":{
                OdometerReading,
                Vehicle1,
                Vehicle2,
                Vehicle3,
                Vehicle1_ED
            }
        }
    }

})()

