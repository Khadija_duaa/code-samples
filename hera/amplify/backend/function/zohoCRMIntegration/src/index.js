const AWS = require('aws-sdk');
const fetch = require('node-fetch')
const ssm = new AWS.SSM({ region: "us-east-2" });

var CLIENT_ID = "";
var CLIENT_SECRET = "";
var REFERESH_TOKEN = "";
var ACCESS_TOKEN = "";
var API_DOMAIN = "https://www.zohoapis.com";

const MODEL_CONTACTS = "Contacts"
const MODEL_ACCOUNTS = "Accounts"


async function init() {
    // Get Auth Params from SSM
    await getCRMAuthFromSSM("/hera/prod/zoho/crm/")

    // Get Access Token
    await refresh_token()
}

function prepareResponse(statusCode, body) {
    response = {
        statusCode: statusCode,
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "*"
        }, 
        body: JSON.stringify(body)
    };
    console.log(response)
    return response
}

async function getCRMAuthFromSSM(pathParam) {
     try {
        const params = {
            Path: pathParam,
            Recursive: true
        };
        
        const response = await ssm.getParametersByPath(params).promise();
        const parameterValues = response.Parameters;
        for ( let i in parameterValues ) {
            if(parameterValues[i].Name.includes("client_id")) {
                CLIENT_ID = parameterValues[i].Value
            }
            else if(parameterValues[i].Name.includes("client_secret")) {
                CLIENT_SECRET = parameterValues[i].Value
            }
            else if(parameterValues[i].Name.includes("refresh_token")) {
                REFERESH_TOKEN = parameterValues[i].Value
            } else if(parameterValues[i].Name.includes("api_domains")) {
                API_DOMAIN = parameterValues[i].Value
            }
        }
    } catch (error) {
        console.error('Error retrieving parameter:', error);
    }
}

async function refresh_token() {
    const params = new URLSearchParams();
    params.append('client_id', CLIENT_ID);
    params.append('refresh_token', REFERESH_TOKEN);
    params.append('client_secret', CLIENT_SECRET);
    params.append('grant_type', 'refresh_token');
    try {
        const response = await fetch('https://accounts.zoho.com/oauth/v2/token', {method: 'POST', body: params});
        const json = await response.json();
        ACCESS_TOKEN = json["token_type"] + " " +json["access_token"]
        API_DOMAIN = json["api_domain"]
    } catch (error) {
        console.log(error);
    }
}

async function api_call(method, endpoint, params={}, payload={}) {
    var options = {
        method: method,
        headers: {
            'Authorization': ACCESS_TOKEN,
            'Content-Type': 'application/json'
        }
    }
    if(["PUT", "POST"].includes(method)) {
        options.body = payload
    }
    try {
        const response = await fetch(endpoint, options);
        return response
    } catch (error) {
        console.log(error);
        return error
    }
}

async function updateUser(body) {
    console.log("updateUser")
    endpoint = `${API_DOMAIN}/crm/v5/${MODEL_CONTACTS}`
    response = await api_call("PUT", endpoint, {}, JSON.stringify(body))
    
    console.log("Status Code: ", response.status)
    responseBody = await response.json()
    return prepareResponse(response.status, responseBody)
}

async function isUserExistinZoho(email) {
    console.log("isUserExistinZoho")
    encodedEmail = encodeURIComponent(email)
    console.log("Encoded Email: ", encodedEmail)
    endpoint = `${API_DOMAIN}/crm/v5/${MODEL_CONTACTS}/search?criteria=((Email:equals:${encodedEmail}))&fields=id`
    response = await api_call("GET", endpoint)
    if( response.status == 200 ) {
        json = await response.json()
        console.log(json)
        return json.data[0].id
    }
    return null
}

async function createUser(body) {
    console.log("createUser")

    console.log("User not found in Zoho, Creating New User")
    if( Object.keys(body.data[0]).includes("id") ) {
        delete body.data[0].id
    }

    console.log(JSON.stringify(body))
    endpoint = `${API_DOMAIN}/crm/v5/${MODEL_CONTACTS}`
    response = await api_call("POST", endpoint, {}, JSON.stringify(body))
    
    console.log("Status Code: ", response.status)
    responseBody = await response.json()
    return prepareResponse(response.status, responseBody)
}

async function upsertUser(event) {
    console.log("upsertUser")
    body = JSON.parse(event.body)
    if( !Object.keys(body.data[0]).includes("Email") ) {
        return prepareResponse(400, JSON.stringify( {error: "Required attributes Email is missing"} ))
    }
    userId = await isUserExistinZoho(body.data[0].Email)

    isUpdate = true

    if(event.httpMethod == "PUT") {
        if(userId) {
            isUpdate = true
            body.data[0].id = userId
        } else {
            if( Object.keys(body.data[0]).includes("id") ) {
                isUpdate = true
            } else {
                isUpdate = false
            }
        }
    } else if(event.httpMethod == "POST") {
        if (userId) {
            body.data[0].Contact_Status = "Active"
            body.data[0].id = userId
            isUpdate = true
        } else {
            isUpdate = false
        }
    }

    // Update Scenario
    if(isUpdate) {
        console.log("User found in Zoho, Updating User")
        return await updateUser(body)
    } else {
        console.log("Create new User in Zoho")
        return await createUser(body)
    }
}

async function updateTenant(event) {
    body = JSON.parse(event.body)
    if( !Object.keys(body.data[0]).includes("id") ) {
        return prepareResponse(400, "Required attributes Email is missing.")
    }

    endpoint = `${API_DOMAIN}/crm/v5/${MODEL_ACCOUNTS}`
    response = await api_call("PUT", endpoint, {}, JSON.stringify(body))

    console.log("Status Code: ", response.status)
    responseBody = await response.json()
    return prepareResponse(response.status, responseBody)
}

/**
 * @type {import('@types/aws-lambda').APIGatewayProxyHandler}
 */
exports.handler = async (event) => {
    console.log(`EVENT: ${JSON.stringify(event)}`);

    // Get Zoho Access Token
    await init()

    // Call the Zoho API
    if( event.pathParameters.module == "user" && ["PUT", "POST"].includes(event.httpMethod) ) {
        return await upsertUser(event)

    } else if(event.pathParameters.module == "tenant" && event.httpMethod == "PUT" ) {
        return await updateTenant(event)

    } else {
        return prepareResponse(405, `Method: ${event.httpMethod} Not Supported`)
    }
}
