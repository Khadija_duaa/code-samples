const createMessagePreferencesHistory = /* GraphQL */ `
  mutation CreateMessagePreferencesHistory(
    $input: CreateMessagePreferencesHistoryInput!
    $condition: ModelMessagePreferencesHistoryConditionInput
  ) {
    createMessagePreferencesHistory(input: $input, condition: $condition) {
      id
      group
      messagePreferenceType
      description
      datetime
      messagePreferencesHistoryStaffId
      createdAt
      updatedAt
      staff {
        id
      }
    }
  }
`;

const updateMessage = /* GraphQL */ `
  mutation UpdateMessage(
    $input: UpdateMessageInput!
    $condition: ModelMessageConditionInput
  ) {
    updateMessage(input: $input, condition: $condition) {
      id
      emailStatus
      emailSendInformation
    }
  }
`;

const updateStaff = /* GraphQL */ `
  mutation UpdateStaff(
    $input: UpdateStaffInput!
    $condition: ModelStaffConditionInput
  ) {
    updateStaff(input: $input, condition: $condition) {
      id,
      receiveEmailMessages
    }
  }
`;

const messageByDestinationEmail = /* GraphQL */ `
  query MessageByDestinationEmail(
    $destinationEmail: String
    $createdAt: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelMessageFilterInput
    $limit: Int
    $nextToken: String
  ) {
    messageByDestinationEmail(
      destinationEmail: $destinationEmail
      createdAt: $createdAt
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        createdAt
        staffId
        messageType
        channelType
        destinationNumber
        destinationEmail
        emailStatus
        emailSendInformation
        staff {
          id
          firstName
          lastName
          group
        }
      }
      nextToken
    }
  }
`;

module.exports = {
    createMessagePreferencesHistory,
    updateMessage,
    updateStaff,
    messageByDestinationEmail
}