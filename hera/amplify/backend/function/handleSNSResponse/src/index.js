/* Amplify Params - DO NOT EDIT
	API_HERA_GRAPHQLAPIENDPOINTOUTPUT
	API_HERA_GRAPHQLAPIIDOUTPUT
	API_HERA_MESSAGETABLE_ARN
	API_HERA_MESSAGETABLE_NAME
	ENV
	REGION
Amplify Params - DO NOT EDIT */
const {getMessageByDestinationEmail,updateMessageStatus,getDataNotification} = require('./util');

exports.handler = async (event, context) => {
    const records = event.Records;
    if(!records.length) return []
    const sns = records[0].Sns
    const dataNotification = getDataNotification(sns)
    
    try {
        //Obtain message data related to the destination and subject email
        const messageData = await getMessageByDestinationEmail(dataNotification);

        //Update message status and information
        const updatedMessageData = await updateMessageStatus(messageData,dataNotification);
        console.log({updatedMessageData})
            
    } catch (error) {
        console.log(error.message);
    }
};
