const { executeQuery,executeMutation } = require('./appSync.js');
const { updateMessage,updateStaff,messageByDestinationEmail,createMessagePreferencesHistory } = require('./graphql.js');
const EMAIL_STATUS_SUCCESS = 'Success'
const EMAIL_STATUS_ERROR = 'Error'

 /**
 * Update message record with send information 
 * 
 */
async function updateMessageStatus(messageData, dataNotification){
  
  const {id, staffId, staff} = messageData
  
  if(!id) throw new Error('Must have an ID.');
  if(!staff?.id && !staffId) throw new Error('Must have an Staff ID.');
  if(!dataNotification) throw new Error('Must have data notification.');
  
  const {notificationType,type,subtype} = dataNotification
  
  const input = {
    id,
    emailStatus: EMAIL_STATUS_SUCCESS,
    emailSendInformation: '',
  }
  
  if(notificationType === 'Bounce' || notificationType === 'Complaint'){
    input.emailStatus = `${EMAIL_STATUS_ERROR}` 
    input.emailSendInformation += `Email could not be delivered: ${notificationType}`
  }
  
  if(type) input.emailSendInformation += ` - ${type}` 
  if(subtype) input.emailSendInformation += ` - ${subtype}` 
  
  if(notificationType === 'Delivery'){
    delete input.emailSendInformation
  }
  
  if(notificationType === 'Bounce' && type === 'Permanent' && staff.id){
    
    const staffId = staff.id || staffId
    const receiveEmailMessages = false
    
    try {
      const responseStaff = await updateStaffReceiveEmailMessages(staffId,receiveEmailMessages)
      await createHistoryMessagePreferences(messageData,receiveEmailMessages)
      console.log({responseStaff})
    } catch (error) {
      console.log(`[updateStaffReceiveEmailMessages] : ${error.message}`) 
    }
    
  }
    
  try {
      const response = await executeMutation(updateMessage, {input})
      return response
  } catch (error) {
      throw new Error(`[updateMessageStatus] : ${error.message}`);
  }
  
}

async function createHistoryMessagePreferences(messageData,receiveEmailMessages){
  const {staffId,staff} = messageData
  const messagePreferencesHistoryStaffId = staffId || staff.id
  const input = {
    group:staff.group,
    messagePreferenceType: 'EMAIL',
    description: 'Hera changed the opt-in status to ' + receiveEmailMessages,
    datetime: new Date().toISOString(),
    messagePreferencesHistoryStaffId
  }
  
  try {
      await executeMutation(createMessagePreferencesHistory, {input})
  } catch (error) {
      throw new Error(`[createHistoryMessagePreferences] : ${error.message}`);
  }
}

async function updateStaffReceiveEmailMessages(staffId,receiveEmailMessages){
  
  const input = {
    id: staffId,
    receiveEmailMessages
  }
  
  try {
    const response = await executeMutation(updateStaff, {input})
    return response
  } catch (error) {
    throw new Error(`[updateStaffReceiveEmailMessages] : ${error.message}`);
  }
  
}

async function getMessageByDestinationEmail(dataNotification) {
  
  const {emailAddress,date} = dataNotification
  
  if(!emailAddress) throw new Error('Must have an destinationEmail.');
  
  const input = {
    destinationEmail: emailAddress,
    sortDirection: 'DESC',
    createdAt: { lt: date },
    limit: 1
  };
  
  try {
    const response = await executeQuery(messageByDestinationEmail, input);
    
    if (response.messageByDestinationEmail && response.messageByDestinationEmail.items.length > 0) {
      return response.messageByDestinationEmail.items[0];
    } else {
      throw new Error('No messages were found for the specified destination.');
    }
  } catch (error) {
    throw new Error(`[getMessageByDestinationEmail] : ${error.message}`);
  }
}

function getDataNotification(sns) {

  let type = null
  let subtype = null
  let recipient = null
  let date = null
  
  // Ensure it's a bounce notification
  if (sns.Type === 'Notification' && sns.Message) {
    const message = JSON.parse(sns.Message)
    const {notificationType,delivery,bounce,complaint,mail} = message
    
    if(delivery){
      const {recipients, timestamp} = delivery
      recipient = recipients[0] || ''
      date = timestamp
      
    }
    
    if(bounce){
      const {bounceType, bounceSubType, bouncedRecipients, timestamp} = bounce
      type = bounceType || ''
      subtype = bounceSubType || ''
      recipient = bouncedRecipients[0] || ''
      date = timestamp
      
    }
    
    if(complaint){
      const {complaintType, complaintSubType, complainedRecipients, timestamp} = complaint
      type = complaintType || ''
      subtype = complaintSubType || ''
      recipient = complainedRecipients[0] || ''
      date = timestamp
    }
    
    // Handle type notification
    const emailAddress = recipient.emailAddress || recipient
    return {emailAddress,date,notificationType,type,subtype}
  }
}

module.exports = {
    updateMessageStatus,
    getMessageByDestinationEmail,
    getDataNotification
}