/* Amplify Params - DO NOT EDIT
	API_HERA_GRAPHQLAPIENDPOINTOUTPUT
	API_HERA_GRAPHQLAPIIDOUTPUT
	API_HERA_INVOICELINEITEMTABLE_ARN
	API_HERA_INVOICELINEITEMTABLE_NAME
	API_HERA_INVOICETABLE_ARN
	API_HERA_INVOICETABLE_NAME
	API_HERA_MESSAGETABLE_ARN
	API_HERA_MESSAGETABLE_NAME
	API_HERA_STAFFTABLE_ARN
	API_HERA_STAFFTABLE_NAME
	API_HERA_TENANTTABLE_ARN
	API_HERA_TENANTTABLE_NAME
	ENV
	REGION
Amplify Params - DO NOT EDIT */
// eslint-disable-next-line import/no-extraneous-dependencies
const AWS = require("aws-sdk");
const region = process.env.REGION;
var ddb = new AWS.DynamoDB.DocumentClient({apiVersion: '2012-08-10'});

const {
  executeQuery,
} = require('./appSync.js');

const { 
  invoicesByGroupAndYearAndMonth,
} = require('./queries')


const { generateInvoiceTemplate } = require('./invoiceTemplate')
const { generateEmailTemplate } = require('./emailTemplate')

const today = new Date() // hardcode date here for testing - Ex: new Date(2021/03/01)
const year = today.getFullYear()
const month = today.getMonth()
const day = today.getDate()
const months = ['January','February','March','April','May','June','July','August','September','October','November','December']

//Month is 0 index but this function is 1 index, so adding 1 to month
const numOfDays = new Date(year, month + 1, 0).getDate();
console.log("number of days: " + numOfDays);

exports.handler = async (event, context, callback) => {
  console.log(event); // logging so you can see what gets passed when you invoke it
  
    // get tenants who have account premium status that isn't null
    var tenants = []
    var lastEvaluatedKey = null
    do{
      var params = {
        ExpressionAttributeNames: {
          '#g': 'group',
          '#a': 'accountPremiumStatus'
        },
        ExpressionAttributeValues: {
          ':n': 'None'
        },
        FilterExpression: "attribute_exists(#a) and NOT contains(#a, :n)",
        TableName: process.env.API_HERA_TENANTTABLE_NAME,
        ProjectionExpression: '#g,#a,id,companyName,trialExpDate, costStandard, costBundle, costPerformance, costRostering, costStaff, costVehicles, costMessaging, discountPercent, discountFixed, discountPercentLabel, discountFixedLabel, addressLine1, addressLine2, addressCity, addressState, addressZip, shortCode, stripeBillingEmail',
        ExclusiveStartKey: lastEvaluatedKey
      };
      var scanResult = await scanDynamoRecords(params);
      var items = scanResult.Items
      lastEvaluatedKey = scanResult.LastEvaluatedKey
      tenants = [...tenants, ...items]
    }while(lastEvaluatedKey)
    console.log({numberOfTenants: tenants.length})
    

    // loop through found tenants 
    for(const tenant of tenants){
      console.log({tenant: tenant.companyName})
      
      // skip tenant if plan is "None"
      if(tenant.accountPremiumStatus.includes('None')) {
        continue;
      } 

      // get number of active DAs for tenant
      var activeDas = []
      lastEvaluatedKey = null
      do{
        params = {
          ExpressionAttributeValues: {
            ':g': tenant.group,
            ':s': 'Active'
          },
          ExpressionAttributeNames: {
            '#g': 'group',
            '#s': 'status'
          },
          KeyConditionExpression: '#g = :g',
          FilterExpression: '#s = :s',
          ProjectionExpression: 'id',
          TableName: process.env.API_HERA_STAFFTABLE_NAME,
          IndexName: 'byGroup',
          ExclusiveStartKey: lastEvaluatedKey
        };
        var queryResult = await queryDynamoRecords(params);
        var items = queryResult.Items
        lastEvaluatedKey = queryResult.LastEvaluatedKey
        activeDas = [...activeDas, ...items]
      }while(lastEvaluatedKey)
      var numActiveDas = activeDas.length
      console.log({numActiveDas: numActiveDas})
      

      // determine if tenant still has an active trial
      let isTrial = false
      let todayMidnightUnixTime = new Date(today).setHours(0,0,0,0)
      let trialExpMidnightUnixTime = new Date(tenant.trialExpDate).setHours(0,0,0,0)
      let isTrialExpired =  trialExpMidnightUnixTime < todayMidnightUnixTime

      if(!isTrialExpired || tenant.accountPremiumStatus.includes('trial')){ // don't charge if they haven't upgraded yet
        var costStandardExt = 0
        var costBundleExt = 0
        var costPerformanceExt = 0
        var costRosteringExt = 0
        var costStaffExt = 0
        var costVehiclesExt = 0
        var totalCost = 0
        isTrial = true
      }
      else{
        var costStandardExt = ( tenant.costStandard * numActiveDas * tenant.accountPremiumStatus.includes('standard') ) / numOfDays;
        var costBundleExt = ( tenant.costBundle * numActiveDas * tenant.accountPremiumStatus.includes('bundle') ) / numOfDays;
        var costPerformanceExt = ( tenant.costPerformance * numActiveDas * tenant.accountPremiumStatus.includes('performance') ) / numOfDays;
        var costRosteringExt = ( tenant.costRostering * numActiveDas * tenant.accountPremiumStatus.includes('rostering') ) / numOfDays
        var costStaffExt = ( tenant.costStaff * numActiveDas * tenant.accountPremiumStatus.includes('staff') ) / numOfDays;
        var costVehiclesExt = ( tenant.costVehicles * numActiveDas * tenant.accountPremiumStatus.includes('vehicles') ) / numOfDays;
        var totalCost = costStandardExt + costBundleExt + costPerformanceExt + costRosteringExt + costStaffExt + costVehiclesExt;
      }
      console.log("Totals: " + costStandardExt + ", " + costBundleExt + ", " + costPerformanceExt + ", " + costRosteringExt + ", " + costStaffExt + ", " + costVehiclesExt + ", " + totalCost  )


      // get or create invoice record
      var invoice = {}
      var invoiceId = ''
      var input = {
        group: tenant.group,
        yearMonth:{
          eq: {
            month: month,
            year: year
          }
        }
      }
      
      var invoices = await executeQuery(invoicesByGroupAndYearAndMonth, input)
      invoices = invoices.invoicesByGroupAndYearAndMonth
      var loadLineItems = false
      
      // if invoice doesn't exist, create it
      if(!invoices.items.length){
        invoiceId = String(year) + '-' +  String(month + 1).padStart(2, '0') + tenant.id
        invoice = {
          id: invoiceId,
          group: tenant.group,
          month: month,
          year: year,
          createdAt: new Date().toISOString(),
          updatedAt: new Date().toISOString(),
          averageActiveDriverCount: numActiveDas,
          invoiceTenantId: tenant.id,
          discountPercent: tenant.discountPercent,
          discountFixed: tenant.discountFixed,
          discountPercentLabel: tenant.discountPercentLabel,
          discountFixedLabel: tenant.discountFixedLabel,
          status: 'Pending',
          'year#month': year + '#' + month
        }
        var createParams = {
          TableName: process.env.API_HERA_INVOICETABLE_NAME,
          Item: invoice
        };
        await createDynamoRecord(createParams);
   
      }
      else{
        loadLineItems = true
        invoice = invoices.items[0]
        invoiceId = invoices.items[0].id
      }

      // get all line items for current invoice
      var invoiceLineItems = []
      if(loadLineItems){
        lastEvaluatedKey = null
        do{
          params = {
            ExpressionAttributeValues: {
              ':i': invoiceId,
            },
            ExpressionAttributeNames: {
              '#i': 'invoiceLineItemInvoiceId',
              '#d': 'day',
              '#m': 'month',
              '#t': 'date'
            },
            KeyConditionExpression: '#i = :i',
            ProjectionExpression: 'id,standardCostExt,bundleCostExt,performanceCostExt,rosteringCostExt,staffCostExt,vehiclesCostExt,activeStaff,#d,#m, #t',
            TableName: process.env.API_HERA_INVOICELINEITEMTABLE_NAME,
            IndexName: 'gsi-InvoiceInvoiceLineItems',
            ExclusiveStartKey: lastEvaluatedKey
          };
          var queryResult = await queryDynamoRecords(params);
          var items = queryResult.Items
          lastEvaluatedKey = queryResult.LastEvaluatedKey
          invoiceLineItems = [...invoiceLineItems, ...items]
        }while(lastEvaluatedKey)
      }
      var lineItemIds = invoiceLineItems.map(item => {
        return item.id
      });
      
      
      // create or update invoice line item
      var todaysLineItemId = String(year) + '-' +  String(month + 1).padStart(2, '0') + '-' + String(day).padStart(2, '0') + tenant.group
      
      if(lineItemIds.includes(todaysLineItemId)){
        try {
          // update
          var updateParams = {
            TableName: process.env.API_HERA_INVOICELINEITEMTABLE_NAME,
            Key:{
              "id": todaysLineItemId
            },
            UpdateExpression: "set #a = :a, #b = :b, #c = :c, #d = :d, #e = :e, #f = :f, #g = :g, #h = :h",
            ExpressionAttributeNames:{
              "#a": "isTrial",
              "#b": "activeStaff",
              "#c": "standardCostExt",
              "#d": "bundleCostExt",
              "#e": "performanceCostExt",
              "#f": "rosteringCostExt",
              "#g": "staffCostExt",
              "#h": "vehiclesCostExt"
            },
            ExpressionAttributeValues:{
              ":a": isTrial,
              ":b": numActiveDas,
              ":c": costStandardExt,
              ":d": costBundleExt,
              ":e": costPerformanceExt,
              ":f": costRosteringExt,
              ":g": costStaffExt,
              ":h": costVehiclesExt
            },
            ReturnValues:"UPDATED_NEW"
          };

          await updateDynamoRecord(updateParams);
        } catch (error) {
          console.error(`[Update Invoice Line Item]`, error)
          continue
        }
      }
      else{
        try {
          // create
          var newLineItem = {
            id: todaysLineItemId,
            invoiceLineItemInvoiceId: invoiceId,
            group: tenant.group,
            date: new Date(todayMidnightUnixTime).toISOString(),
            month: month,
            year: year,
            day: day,
            createdAt: new Date().toISOString(),
            updatedAt: new Date().toISOString(),
            isTrial: isTrial,
            activeStaff: numActiveDas,
            standardCostExt: costStandardExt,
            bundleCostExt: costBundleExt,
            performanceCostExt: costPerformanceExt,
            rosteringCostExt: costRosteringExt,
            staffCostExt: costStaffExt,
            vehiclesCostExt: costVehiclesExt
          }
          var createParams = {
            TableName: process.env.API_HERA_INVOICELINEITEMTABLE_NAME,
            Item: newLineItem
          };
          if (tenant.costStandard) createParams.Item.standardCost = tenant.costStandard
          if (tenant.costBundle) createParams.Item.bundleCost = tenant.costBundle
          if (tenant.costPerformance) createParams.Item.performanceCost = tenant.costPerformance
          if (tenant.costRostering) createParams.Item.rosteringCost = tenant.costRostering
          if (tenant.costStaff) createParams.Item.staffCost = tenant.costStaff
          if (tenant.costVehicles) createParams.Item.vehiclesCost = tenant.costVehicles

          await createDynamoRecord(createParams);
        } catch (error) {
          console.error(`[Create Invoice Line Item]`, error);
          continue
        }
        
        // add new line item to line item array
        invoiceLineItems.push(newLineItem)
      }
      
      
      // sum up line items to get updated total
      var standardTotal = invoiceLineItems.reduce((accumulator, item) => ({standardCostExt: accumulator.standardCostExt + item.standardCostExt})) 
      var bundleTotal = invoiceLineItems.reduce((accumulator, item) => ({bundleCostExt: accumulator.bundleCostExt + item.bundleCostExt})) 
      var performanceTotal = invoiceLineItems.reduce((accumulator, item) => ({performanceCostExt: accumulator.performanceCostExt + item.performanceCostExt})) 
      var rosteringTotal = invoiceLineItems.reduce((accumulator, item) => ({rosteringCostExt: accumulator.rosteringCostExt + item.rosteringCostExt})) 
      var staffTotal = invoiceLineItems.reduce((accumulator, item) => ({staffCostExt: accumulator.staffCostExt + item.staffCostExt})) 
      var vehiclesTotal = invoiceLineItems.reduce((accumulator, item) => ({vehiclesCostExt: accumulator.vehiclesCostExt + item.vehiclesCostExt})) 
      var activeStaffTotal = invoiceLineItems.reduce((accumulator, item) => ({activeStaff: accumulator.activeStaff + item.activeStaff}))
      var averageActiveStaffTotal = (activeStaffTotal.activeStaff + numActiveDas) / (invoiceLineItems.length + 1)
      var invoiceTotal = standardTotal.standardCostExt + bundleTotal.bundleCostExt + performanceTotal.performanceCostExt + rosteringTotal.rosteringCostExt + staffTotal.staffCostExt + vehiclesTotal.vehiclesCostExt

      try {
        // create html invoice if its last day of the month
        var invoiceHtml = ''
        if(day == numOfDays){
          // calculate line item total
          invoiceLineItems = invoiceLineItems.map(item =>{
            var total = item.standardCostExt +
              item.bundleCostExt +
              item.performanceCostExt +
              item.rosteringCostExt +
              item.staffCostExt +
              item.vehiclesCostExt;
            item.invoiceTotal = total
            return item
          })

          // sort line items by date
          invoiceLineItems.sort(function (a,b){
            return new Date(a.date) - new Date(b.date);
          })

          invoice.discountPercent = tenant.discountPercent
          invoice.discountFixed = tenant.discountFixed
          invoice.discountPercentLabel = tenant.discountPercentLabel
          invoice.discountFixedLabel = tenant.discountFixedLabel

          // create html from line item array
          invoiceHtml = generateInvoiceTemplate(invoice, invoiceLineItems, tenant)

          // send invoice email if billing email is provided and not still on an active trial
          if(tenant.stripeBillingEmail && !isTrial){
            console.log({billingEmail: tenant.stripeBillingEmail})

            // send email with invoice
            var subject = "Hera: Invoice for " + months[month] + ' ' + year
            var header = "Below is your invoice for " + months[month] + ' ' + year
            var messageId = String(year) + '-' +  String(month + 1).padStart(2, '0') + tenant.group
            var messageItem = {
              id: messageId,
              group: tenant.group,
              messageTenantId: tenant.id,
              createdAt: new Date().toISOString(),
              updatedAt: new Date().toISOString(),
              isReadS: false,
              subject: subject,
              bodyText: 'Below is your invoice for ' + months[month] + ' ' + year,
              bodyHtml: generateEmailTemplate(header, invoiceHtml, tenant.companyName),
              channelType: 'EMAIL',
              messageType: 'invoice',
              destinationEmail: tenant.stripeBillingEmail
            }

            var createParams = {
              TableName: process.env.API_HERA_MESSAGETABLE_NAME,
              Item: messageItem
            };

            await createDynamoRecord(createParams);
          }

        }
      } catch (error) {
        console.error(`[Create Invoice Message]`, error);
        continue
      }

      try {
        // update invoice totals, averages, and html
        var updateParams = {
          TableName: process.env.API_HERA_INVOICETABLE_NAME,
          Key:{
            "id": invoiceId
          },
          UpdateExpression: "set #a = :a, #b = :b, #c = :c, #d = :d, #e = :e, #f = :f, #g = :g, #h = :h, #i = :i, #j = :j, #k = :k, #l = :l",
          ExpressionAttributeNames:{
            "#a": "invoiceTotal",
            "#b": "averageActiveDriverCount",
            "#c": "year",
            "#d": "createdAt",
            "#e": "month",
            "#f": "status",
            "#g": "group",
            "#h": "html",
            "#i": "discountPercent",
            "#j": "discountFixed",
            "#k": "discountPercentLabel",
            "#l": "discountFixedLabel"
          },
          ExpressionAttributeValues:{
            ":a": invoiceTotal,
            ":b": averageActiveStaffTotal,
            ":c": year,
            ":d": invoice.createdAt,
            ":e": month,
            ":f": 'Pending',
            ":g": tenant.group,
            ":h": invoiceHtml,
            ":i": tenant.discountPercent ? tenant.discountPercent : 0,
            ":j": tenant.discountFixed ? tenant.discountFixed : 0,
            ":k": tenant.discountPercentLabel ? tenant.discountPercentLabel : '',
            ":l": tenant.discountFixedLabel ? tenant.discountFixedLabel : ''
          },
          ReturnValues:"UPDATED_NEW"
        };

        await updateDynamoRecord(updateParams);
      } catch (error) {
        console.error(`[Update Invoice]`, error);
        continue
      }

    }
    // end loop
};

function scanDynamoRecords(params){
    return new Promise((resolve, reject) => {
        // Create DynamoDB service object
        ddb.scan(params, function(err, data) {
            if (err) {
                console.error("Unable to get items. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                console.log("Get Item succeeded:", JSON.stringify(data, null, 2));
                resolve(data)
            }
        });
    })
}

function queryDynamoRecords(params){
    return new Promise((resolve, reject) => {
        // Create DynamoDB service object
        ddb.query(params, function(err, data) {
            if (err) {
                console.error("Unable to get items. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                console.log("Get Item succeeded:", JSON.stringify(data, null, 2));
                resolve(data)
            }
        });
    })
}

function updateDynamoRecord(params){
    return new Promise((resolve, reject) => {
        ddb.update(params, function(err, data) {
            if (err) {
                console.log("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
                resolve(data)
            }
        });
  })
    
}

function createDynamoRecord(params){
    console.log("Creating Record");
    return new Promise((resolve, reject) => {
        ddb.put(params, function(err, data) {
          if (err) {
            console.log("Unable to put item. Error JSON:", JSON.stringify(err, null, 2));
            reject(err)
          } else {
            console.log("PutItem succeeded:", JSON.stringify(data, null, 2));
            resolve(data)
          }
        });
    
    
  })
}