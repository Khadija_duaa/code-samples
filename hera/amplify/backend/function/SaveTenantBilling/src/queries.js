const listTenants = /* GraphQL */ `
  query ListTenants(
    $filter: ModelTenantFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listTenants(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        group
        trialExpDate
        accountPremiumStatus
        costStandard
        costBundle
        costPerformance
        costRostering
        costStaff
        costVehicles
        costMessaging
        discountPercent
        discountFixed
        discountPercentLabel
        discountFixedLabel
        companyName
        addressLine1
        addressLine2
        addressCity
        addressState
        addressZip
        shortCode
        stripeBillingEmail
      }
      nextToken
    }
  }
`;

const staffsByGroup = /* GraphQL */ `
  query StaffsByGroup(
    $group: String
    $firstName: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelStaffFilterInput
    $limit: Int
    $nextToken: String
  ) {
    staffsByGroup(
      group: $group
      firstName: $firstName
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
      }
      nextToken
    }
  }
`;

const invoicesByGroupAndYearAndMonth = /* GraphQL */ `
  query InvoicesByGroupAndYearAndMonth(
    $group: String
    $yearMonth: ModelInvoiceByGroupYearMonthCompositeKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelInvoiceFilterInput
    $limit: Int
    $nextToken: String
  ) {
    invoicesByGroupAndYearAndMonth(
      group: $group
      yearMonth: $yearMonth
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        month
        year
        createdAt
        invoiceLineItems {
          items {
            id
            date
            activeStaff
            standardCostExt
            bundleCostExt
            performanceCostExt
            rosteringCostExt
            staffCostExt
            vehiclesCostExt
            messagingCostExt
            
          }
        }
      }
      nextToken
    }
  }
`;

const getInvoice = /* GraphQL */ `
  query GetInvoice($id: ID!) {
    getInvoice(id: $id) {
      id
      year
      month
      createdAt
      invoiceTotal
      discountPercent
      discountFixed
      discountPercentLabel
      discountFixedLabel
      invoiceLineItems {
        items {
          id
          date
          month 
          day
          activeStaff
          isTrial
          standardCostExt
          bundleCostExt
          performanceCostExt
          rosteringCostExt
          staffCostExt
          vehiclesCostExt
          messagingCostExt
        }
        nextToken
      }
     
    }
  }
`;

module.exports = {
  listTenants,
  staffsByGroup,
  invoicesByGroupAndYearAndMonth,
  getInvoice
};

