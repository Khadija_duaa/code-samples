const createInvoiceLineItem = /* GraphQL */ `
  mutation CreateInvoiceLineItem(
    $input: CreateInvoiceLineItemInput!
    $condition: ModelInvoiceLineItemConditionInput
  ) {
    createInvoiceLineItem(input: $input, condition: $condition) {
      id
      createdAt
      group
      date
      month
      year
      day
      activeStaff
      standardCost
      standardCostExt
      bundleCost
      bundleCostExt
      performanceCost
      performanceCostExt
      rosteringCost
      rosteringCostExt
      staffCost
      staffCostExt
      vehiclesCost
      vehiclesCostExt
      messagingCost
      messagingCostExt
      updatedAt
      
    }
  }
`;

const updateInvoiceLineItem = /* GraphQL */ `
  mutation UpdateInvoiceLineItem(
    $input: UpdateInvoiceLineItemInput!
    $condition: ModelInvoiceLineItemConditionInput
  ) {
    updateInvoiceLineItem(input: $input, condition: $condition) {
      id
      createdAt
      group
      date
      month
      year
      day
      isTrial
      activeStaff
      standardCost
      standardCostExt
      bundleCost
      bundleCostExt
      performanceCost
      performanceCostExt
      rosteringCost
      rosteringCostExt
      staffCost
      staffCostExt
      vehiclesCost
      vehiclesCostExt
      messagingCost
      messagingCostExt
      updatedAt
    }
  }
`;

const createInvoice = /* GraphQL */ `
  mutation CreateInvoice(
    $input: CreateInvoiceInput!
    $condition: ModelInvoiceConditionInput
  ) {
    createInvoice(input: $input, condition: $condition) {
      id
      group
      month
      year
      invoiceTotal
      averageActiveDriverCount
      discountPercent
      discountFixed
      discountPercentLabel
      discountFixedLabel
      status
    }
  }
`;

const updateInvoice = /* GraphQL */ `
  mutation UpdateInvoice(
    $input: UpdateInvoiceInput!
    $condition: ModelInvoiceConditionInput
  ) {
    updateInvoice(input: $input, condition: $condition) {
      id
      createdAt
      group
      month
      year
      invoiceTotal
      averageActiveDriverCount
      discountPercent
      discountFixed
      discountPercentLabel
      discountFixedLabel
      status
    }
  }`;

const createMessage = /* GraphQL */ `
  mutation CreateMessage(
    $input: CreateMessageInput!
    $condition: ModelMessageConditionInput
  ) {
    createMessage(input: $input, condition: $condition) {
      id
      group
      createdAt
      staffId
      messageType
      channelType
      destinationNumber
      destinationEmail
      subject
      bodyText
      bodyHtml
      smsStatus
      smsSendInformation
      emailStatus
      emailSendInformation
      updatedAt
    }
  }
`;

module.exports = {
  createInvoiceLineItem,
  updateInvoiceLineItem,
  createInvoice,
  updateInvoice,
  createMessage
};