/* Amplify Params - DO NOT EDIT
	API_HERA_GRAPHQLAPIENDPOINTOUTPUT
	API_HERA_GRAPHQLAPIIDOUTPUT
	API_HERA_STAFFTABLE_ARN
	API_HERA_STAFFTABLE_NAME
	ENV
	REGION
Amplify Params - DO NOT EDIT */

const appsyncUrl = process.env.API_HERA_GRAPHQLAPIENDPOINTOUTPUT
//const { request } = require('./appSyncRequest.js')
const {
    executeMutation, AWS
} = require('./appSync.js');

const { createMessage, updateStaff, createMessageReader } = require('./graphql.js')
const { ShortenUrlGenerator } = require('/opt/nodejs/short-url');
const { generateTemplate } = require('./emailTemplate.js')
const { updateDynamoRecord, buildUpdateParams, getDynamoRecord } = require("./dynamodbHelper.js");


exports.handler = async (event) => {
    
    try {

        await iterationForSendMessage(JSON.parse(event.body));
        
        console.log('done')
    
        // TODO implement
        const response = {
            statusCode: 200,
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "*"
            }, 
            body: JSON.stringify('success'),
        };
        return response;
    
    } catch(e) {
        console.log('something went wrong', e)
        const response = {
            statusCode: 200,
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "*"
            }, 
            body: JSON.stringify('failes'),
        };
    return response;
    }
};

async function iterationForSendMessage(data){
    let staffs = data.staffs;

    delete data["staffs"];
    for (var i=0 ; i < staffs.length ; i++){
        let staff = staffs[i];
        await sendMessage(staff, data );
    }
}

async function getStaffById(staffId) {
    let staff = {}
    try {
        let params = {
            Key: {
                id: staffId
            },
            ProjectionExpression: "id, firstName, lastName, email, phone, receiveTextMessages, receiveEmailMessages",
            TableName: process.env.API_HERA_STAFFTABLE_NAME
        }
        const result = await getDynamoRecord(params)
        staff = result.Item
    } catch (e) {
        console.log('Error in function getStaffById', e)
    } finally{
        return staff
    }
}


var sendMessage = async function(staff, data){

    //when the staff only has ID
    if(staff && !!staff.id && !staff.firstName){
        staff = await getStaffById(staff.id)
    }
    console.log(`Sending message to: ${staff.firstName} ${staff.lastName}`)
    console.log(`TenantId: ${data.tenantId} Group: ${data.group}`)
    
    //Check to see if staff has opted out of all communications
    if(!staff.receiveTextMessages && !staff.receiveEmailMessages){
        console.log("skipping " + staff.firstName)
        return {success: false, error: '', warning: "skipping " + staff.firstName }
    }
    
    let shortenUrl = null
    if(data.withShortenUrl){
        const vars = {
            SHORTEN_URL_DOMAIN:process.env.SHORTEN_URL_DOMAIN,
            API_HERA_GRAPHQLAPIENDPOINTOUTPUT: process.env.API_HERA_GRAPHQLAPIENDPOINTOUTPUT,
            REGION:process.env.REGION
        }
    
        const generator = new ShortenUrlGenerator(vars,AWS.config);
        shortenUrl = await generator.generateShortenUrl(data);
    
        if(!shortenUrl.response){
            console.log({success: shortenUrl.response, error: shortenUrl.result, warning: '' })
        } 
        
    }

    const bodyText = (data.bodyText) ? data.bodyText : msgCounselingToDA( 'text', data.firstName, (shortenUrl) ? shortenUrl.result.shortUrl : null );
    const bodyHtml = (data.bodyHtml) ? data.bodyHtml : msgCounselingToDA( 'html', data.firstName, (shortenUrl) ? shortenUrl.result.shortUrl : null );
    var markRead = data.type == "roster" || data.type == "broadcast" ? 'true' : 'false' 

    //Link Message to staff, route, and tenant
    if (data.route != null){
        var input = {
            staffId: staff.id,
            messageTenantId: data.tenantId,
            messageRouteId: data.route.id,
            group: data.group,
            senderName: data.senderName,
            senderId: data.senderId,
            isReadS: markRead,
            messageType: data.type
        }
        if(!data.route.isReplacement){
            input.messageRouteId = data.route.id
        }else if(data.route.isReplacement){
            input.messageReplaceByRouteId = data.route.id
        }
    }else{
        var input = {
            staffId: staff.id,
            messageTenantId: data.tenantId,
            group: data.group,
            senderName: data.senderName,
            isReadS:  markRead,
            messageType: data.type,
            senderId: data.user.id,
        }
    }
    if(data.attachmentId){
        input.messageAttachmentId = data.attachmentId
    } 

    //Setup SMS
    if(staff.receiveTextMessages){
        input.channelType = "SMS"
        input.bodyText = bodyText
        input.destinationNumber = staff.phone
    }

    //Setup EMAIL
    if(staff.receiveEmailMessages){
        input.channelType = "EMAIL"
        input.subject = data.subject
        input.bodyText = bodyText
        input.bodyHtml = generateTemplate(data.header, bodyHtml, staff.id, data.companyName, data.notificationPreferencesUrl)
        input.destinationEmail = staff.email
    }

    //See if they receive both SMS and EMAIL
    if(staff.receiveTextMessages && staff.receiveEmailMessages){
        input.channelType = "SMS/EMAIL"
    }

    //Create Record
    try{   
        
        data.owner && (input.owner = data.owner)
        input.destinationName = `${staff.firstName}  ${staff.lastName}`
        
        var response = await executeMutation(createMessage, {input})
        console.log('response', JSON.stringify(response))
        var messageId = response.createMessage.id

        const updateParams = {
            id: staff.id,
            smsLastMessage: bodyText,
            smsLastMessageTimestamp: response.createMessage.createdAt
        }
        const buildParams = buildUpdateParams(process.env.API_HERA_STAFFTABLE_NAME, updateParams, updateParams.id )
        await updateDynamoRecord(buildParams);

        // await executeMutation(createMessageReader, {input: {
        //     messageID: messageId,
        //     userID: data.user.id,
        //     group: data.group
        // }})

        //Update Route Record
        return {success: true, error: '', warning: '', messageId: messageId}
    }catch(e){
        console.log('error: ', staff.firstName)
        console.log('error: ', e)
        return {success: false, error: e, warning: '' }
    }

}

function msgCounselingToDA( msgType, userName, msgLink ){
    if(!userName || !msgLink) return null
    if( msgType == 'text') 
        return `${userName}, you have a counseling that requires your acknowledgment: ${msgLink}`
    else
        return `${userName}, you have a counseling that requires your acknowledgment:<br><br><a href="${msgLink}">${msgLink}</a>`
}
