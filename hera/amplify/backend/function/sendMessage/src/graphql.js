const createMessage = /* GraphQL */ `
  mutation CreateMessage(
    $input: CreateMessageInput!
    $condition: ModelMessageConditionInput
  ) {
    createMessage(input: $input, condition: $condition) {
      id
      group
      createdAt
      staffId
      messageType
      channelType
      destinationNumber
      destinationEmail
      subject
      bodyText
      bodyHtml
      senderName
      smsStatus
      smsSendInformation
      emailStatus
      emailSendInformation
      isReadS
      sentBy
      sentAt
      linkExpiryDate
      attachmentLink
      contentType
      senderId
      updatedAt
      attachment {
        id
        group
        createdAt
        s3Key
        expirationDate
        contentType
        fileSize
        updatedAt
        dailyRoster {
          nextToken
        }
        message {
          nextToken
        }
      }
      sender {
        id
      }
      staff {
        id
        status
      }
      isReadBy {
        items {
          id
          userID
          messageID
          group
          createdAt
          updatedAt
        }
        nextToken
      }
    }
  }
`;

const updateStaff = /* GraphQL */ `
  mutation UpdateStaff(
    $input: UpdateStaffInput!
    $condition: ModelStaffConditionInput
  ) {
    updateStaff(input: $input, condition: $condition) {
      id
      group
      transporterId
      firstName
      lastName
      alternateNames
      phone
      email
      keyFocusArea
      keyFocusAreaCompleted
      coachingOpportunity
      status
      dlExpiration
      vehicleReport
      gasCardPin
      defaultDevice
      dob
      gender
      pronouns
      assignedLead
      isALead
      hourlyStatus
      hireDate
      terminationDate
      finalCheckIssueDate
      returnedUniform
      latestScorecard
      smsLastMessageTimestamp
      smsLastMessage
      photo
      onboardingPinned
      netradyneDriverId
      heraScore
      heraRank
      prevHeraRank
      receiveTextMessages
      receiveEmailMessages
      notes
      customDeliveryVan
      authorizedLBS
      preferredDaysOff
      createdAt
      updatedAt
    }
  }
`;

const createMessageReader = /* GraphQL */ `
  mutation CreateMessageReader(
    $input: CreateMessageReaderInput!
    $condition: ModelMessageReaderConditionInput
  ) {
    createMessageReader(input: $input, condition: $condition) {
      id
      userID
      messageID
      group
      createdAt
      updatedAt
    }
  }
`;

module.exports = {
  createMessage,
  updateStaff,
  createMessageReader
};