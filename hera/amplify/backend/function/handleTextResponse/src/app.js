/* Amplify Params - DO NOT EDIT
	API_HERA_GRAPHQLAPIENDPOINTOUTPUT
	API_HERA_GRAPHQLAPIIDOUTPUT
	API_HERA_MESSAGEPREFERENCESHISTORYTABLE_ARN
	API_HERA_MESSAGEPREFERENCESHISTORYTABLE_NAME
	API_HERA_MESSAGETABLE_ARN
	API_HERA_MESSAGETABLE_NAME
	API_HERA_STAFFTABLE_ARN
	API_HERA_STAFFTABLE_NAME
	API_HERA_TENANTTABLE_ARN
	API_HERA_TENANTTABLE_NAME
	ENV
	REGION
	STORAGE_HERA_BUCKETNAME
Amplify Params - DO NOT EDIT *//*
Copyright 2017 - 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at
    http://aws.amazon.com/apache2.0/
or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
*/

var AWS = require("aws-sdk");
var express = require('express')
var bodyParser = require('body-parser')
var awsServerlessExpressMiddleware = require('aws-serverless-express/middleware')
AWS.config.region = process.env.region;

// declare a new express app
var app = express()
app.use(bodyParser.json())
app.use(awsServerlessExpressMiddleware.eventContext())

// Enable CORS for all methods
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "*")
  next()
});

const {
  getTenantByOriginationNumber,
  updateMessageRecord,
  createMessageRecord,
  handleUpdateAssociateReceiveNotifications,
  updateStaffLastSms,
} = require("./helpers.js");
const { getServiceConfig } = require('./message-providers');

app.post('/message', async function(req, res) {
  try {
    // Add your code here
    const serviceConfig = getServiceConfig(req);
    if (!serviceConfig.isValid) {
      throw new Error('Unexpected Request user-agent not found');
    }
    const response = await serviceConfig.config.messageHandler(req, {
      handleCreateMessagesForTenants,
    });
    res.end();
  }
  catch(e) {
    console.log('---error /message failed', e)
    res.json({success: 'Could not send message'})
  } 
});

app.post('/outboundMessage', async function(req, res) {
  try {
    let record = req.body && req.body[0];
    let messageId = record.message && record.message.tag;
    let recordMessageId = record.message && record.message.id;
    let type = record?.description;
    const serviceConfig = getServiceConfig(req);
    if (serviceConfig.isValid) {
      const response = await serviceConfig.config.outboundMessageHandler(req, {});
      record = response.record;
      recordMessageId = response.recordMessageId;
      type = response.type;
      messageId = response.messageId;
    }
    //updates the status on the message record if text sent succesfully
    await updateMessageRecord(messageId, type, recordMessageId);
    //updates the last SMS receieved string on staff
    console.log('logging Update Staff SMS')
    await updateStaffLastSms(messageId);
    //update dynamo with error status if error using tag in body / otherwise success
    //dynamo record id will be passed as a tag
  }
  catch(e) {
    console.log('outboundMessage error', JSON.stringify(e));
  }
  res.json({success: 'post call succeed!', url: req.url, body: req.body})
});

async function handleCreateMessagesForTenants(originationNumber, payload) {
  const tenants = originationNumber && (await getTenantByOriginationNumber(originationNumber));
  if(!tenants?.items) return;
  const { group } = tenants.items[0];
  const { number, messageBody, media } = payload;
  await handleUpdateAssociateReceiveNotifications(number, messageBody, tenants);
  const messageBodyLower = messageBody.toLowerCase().trim();
  const validCommands = ['stop','cancel','end','quit','stopall','stop all', 'unsubscribe','start', 'unstop'];
  if (!validCommands.includes(messageBodyLower)) {
    await createMessageRecord(number, messageBody, media, group);
  }
}

app.listen(3000, function() {
  console.log("App started")
});

// Export the app object. When executing the application local this does nothing. However,
// to port it to AWS Lambda we will create a wrapper around that will load the app from
// this file
module.exports = app
