function isTestEnv(){
  return !!process.env.ENV 
    && process.env.ENV === 'test'
}

function usingValidTestNumber(){
  return !!process.env.TEST_ENV_SMS_NUMBER 
    && process.env.TEST_ENV_SMS_NUMBER.includes('+1') 
    && process.env.TEST_ENV_SMS_NUMBER.length==12
}

function isValidTestEmail(email){
  return !!email 
    && email.includes('@herasolutions.info')
}

module.exports = {
  isTestEnv,
  usingValidTestNumber,
  isValidTestEmail
};
  