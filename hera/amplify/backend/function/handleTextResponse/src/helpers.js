const AWS = require("aws-sdk");
const s3 = new AWS.S3();
const { isTestEnv, isValidTestEmail } = require('./testEnvHelpers.js');
AWS.config.region = process.env.region;
const documentClient = new AWS.DynamoDB.DocumentClient({apiVersion: '2012-08-10'});
const axios = require('axios');
const { nanoid } = require('nanoid');
const { executeQuery, executeMutation } = require("./appSync");
const { updateDynamoRecord, buildUpdateParams, createDynamoRecord } = require("./dynamodbHelper");
const {
  messageByCarrierMessageId,
  getTenant,
  tenantByOriginationNumber,
  updateMessage,
  createNotification,
  createAttachment,
  createMessage,
  updateStaff
} = require("./graphql");

const pinpoint = new AWS.Pinpoint({region: process.env.PIN_POINT_REGION}); 
const projectId = process.env.PROJECT_ID;

async function getMessageByMessageCarrierId(carrierMessageId) {
  try {
    const input = { carrierMessageId };
    const response = await executeQuery(messageByCarrierMessageId, input);
    return response.messageByCarrierMessageId.items[0];
  }
  catch (error) {
    console.log('[getMessageByMessageCarrierId]', error);
    throw error;
  }
}

async function updateMessageStatus(messageToUpdate){
  return new Promise(async (resolve, reject) => {
    try {
      if(messageToUpdate.id) {
        const input = {
          id: messageToUpdate.id,
          smsStatus: messageToUpdate.smsStatus,
          smsSendInformation: messageToUpdate.smsSendInformation,
        }
        const response = await executeMutation(updateMessage, {input});
        console.log('[updateMessageStatus] response:', response);
        resolve(response);
      } 
      else {
        console.log('[updateMessageStatus] error', messageToUpdate);
        reject(messageToUpdate);
      }
    } catch (error) {
      console.log("Error in function updateMessageStatus", error);
      reject(error)
    }
  })
};

async function getTenantById(id) {
  const input = { id };
  try {
    const response = await executeQuery(getTenant, input);
    return response.getTenant;
  } catch (error) {
    console.log('[getTenantById]', error);
    throw new Error(error);
  }
}

async function getTenantByOriginationNumber(originationNumber) {
  const input = { originationNumber: originationNumber };
  try {
    const response = await executeQuery(tenantByOriginationNumber, input);
    return response.tenantByOriginationNumber;
  } catch (e) {
    console.log('[getTenantByOriginationNumber]', e);
    throw e;
  }
}

async function updateMessageRecord(messageId, status, recordMessageId) {
  //Updates the message record with the status
  return new Promise(async (resolve, reject) => {
    console.log({messageId, status});
    const textStatus = (status == 'ok') || (status == 'delivery-receipt-expired') || status.includes('Message delivered to carrier') ? 'Success' : 'Error'
    console.log({textStatus})
    try {
      if(messageId) {
        const input = {
          id: messageId,
          smsStatus: textStatus,
          smsSendInformation: status + ', messageId ' + recordMessageId,
        }
        const response = await executeMutation(updateMessage, {input})
        console.log('[updateMessageStatus] response:', response)
        resolve(response)
      }
      else {
        console.log('[updateMessageStatus] error', messageId)
        reject(messageId)
      }
    } catch (error) {
      console.log("Error in function updateMessageStatus", error);
      reject(error)
    }
  })
}

async function pushNotification(record) {
  try {
    const input = {
      group: record.group,
      owner: record.owner,
      title: 'Some messages could not be sent',
      description:
        'The following DAs did not receive the message:\n' +
        record.destinationName +
        '\nPlease resend your message to these DAs.',
      payload: JSON.stringify({}),
      clickAction: null,
      isRead: false,
    }
    await executeMutation(createNotification, { input });
  } catch (e) {
    console.log('error pushNotification', e);
  }
}

function getDynamoRecord(params) {
  return new Promise((resolve, reject) => {
    documentClient.get(params, function(err, data) {
      if (err) {
        console.error("Unable to get item. Error JSON:", JSON.stringify(err, null, 2));
        reject(err);
      } else {
        console.log("Get Item succeeded:", JSON.stringify(data, null, 2));
        resolve(data);
      }
    });
  })
}

function saveDataToS3(data, key) {
  const params = {
    Body: data,
    Bucket: process.env.STORAGE_HERA_BUCKETNAME,
    Key: 'public/' + key,
  };
  s3.putObject(params, function(err, data) {
    if (err) return console.log('error on s3', err, err.stack);
    console.log('success uploading to s3', data);
  });
}

async function createMessageRecord(number, messageBody, media, group) {
  console.log('[createMessageRecord] params:', number, messageBody, media);
  //Get the staff members record(s)
  const records = await getStaffRecords(group, number);
  const mediaUrls = (Array.isArray(media) && media.map(item => item.url)) || [];
  for (const record of records){
    //loop there everyone with this phone number
    const { id: staffId, group } = record;
    let i = 0;
    do{
      const attachmentID = nanoid(9);
      console.log(attachmentID);
      //send a message for each piece of media attached -- minimum 1
      if(media.length) {
        const fileName = mediaUrls[i].substr(mediaUrls[i].lastIndexOf('/') + 1);
        const currentKey = `${group}/messages/${staffId}/${(new Date()).getTime()}/${fileName}`;
        const contentType = getContentTypeFromName(fileName);
        const arraybuffer = await axios.get(mediaUrls[i], { responseType: 'arraybuffer' });
        const response = Buffer.from(arraybuffer.data, "utf-8");
        //Place Object in S3
        saveDataToS3(response, currentKey);
        //Create Attachment record
        const input = {
          id: attachmentID,
          group: group,
          s3Key: currentKey,
          expirationDate: new Date().setFullYear(2099),
          contentType: contentType,
        }
        await executeMutation(createAttachment, {input});
      }
      //Create Message record
      const input = {
        messageAttachmentId: attachmentID,
        bodyText: messageBody,
        channelType: "RESPONSE",
        messageType: "messenger",
        group: group,
        staffId: staffId,
        isReadS: 'false',
      }
      const response = await executeMutation(createMessage, {input});
      console.log(response);
      const updateParams = {
        id: staffId,
        smsLastMessageTimestamp: (new Date()).toISOString(),
        smsLastMessage: messageBody,
      }
      const buildParams = buildUpdateParams(process.env.API_HERA_STAFFTABLE_NAME, updateParams, updateParams.id);
      await updateDynamoRecord(buildParams);
      i = i + 1;
    }
    while(i < mediaUrls.length);
  }
}

async function handleUpdateTextStatus(number, receiveTexts, tenant) {
  try {
    //Get the staff members record(s)
    let records = [];
    const params = getFetchStaffParams(number, tenant);
    do{
      const scanResult = await findRecords(params);
      records = records.concat(scanResult.Items);
      params.ExclusiveStartKey = scanResult.LastEvaluatedKey;
    }
    while(params.ExclusiveStartKey);
    for(const record of records) {
      console.log(record);
      await updateStaffRecord(record, receiveTexts);
      await createMessagePreferenceHistoryRecord(record, receiveTexts);
    }
  }
  catch(e) {
    console.log(e);
  }
}

async function updateStaffMemberTextStatus(number, receiveTexts, tenants) {
  if (!tenants || !tenants.items || !tenants.items.length) {
    await handleUpdateTextStatus(number, receiveTexts);
    return;
  }
  for (const tenant of tenants.items) {
    await handleUpdateTextStatus(number, receiveTexts, tenant);
  }
}

async function handleUpdateAssociateReceiveNotifications(number, message, tenants) {
  const receiveNotifications = getReceiveMessageNotifications(message);
  if (receiveNotifications === null) return;
  await updateStaffMemberTextStatus(number, receiveNotifications, tenants);
}

function getFetchStaffParams(number, tenant) {
  const params = {
    TableName: process.env.API_HERA_STAFFTABLE_NAME,
    IndexName: "byPhone",
    KeyConditionExpression : "#phone = :phone",
    ExpressionAttributeNames : { "#phone" : 'phone' },
    ExpressionAttributeValues : { ":phone" : number },
    ExclusiveStartKey: null,
  };
  if (tenant && tenant.group) {
    params.KeyConditionExpression = '#phone = :phone AND #group = :group';
    params.ExpressionAttributeNames["#group"] = 'group';
    params.ExpressionAttributeValues[":group"] = tenant.group;
  }
  return params;
}

async function updateStaffRecord(record, receiveTexts){
  return new Promise((resolve, reject) => {
    const params = {
      TableName : process.env.API_HERA_STAFFTABLE_NAME,
      Key: { id : record.id },
      UpdateExpression: "set #b = :b",
      ExpressionAttributeNames:{
        "#b":"receiveTextMessages"
      },
      ExpressionAttributeValues:{
        ":b": receiveTexts
      },
      ReturnValues:"UPDATED_NEW"
    };
    console.log(params)
    documentClient.update(params, function(err, data) {
      if (err) {
        console.log(err)
        reject(err)
      }
      else {
        console.log(data)
        resolve(data)
      }
    });
  })
}

async function createMessagePreferenceHistoryRecord(record, receiveTexts){
  const timestamp = new Date().toLocaleString();
  const firstName = record.firstName || '';
  const lastName = record.lastName ? `${record.lastName}` : '';
  let log = '';
  if (receiveTexts) {
    log = `${firstName} ${lastName} opted in to text messages by sending a "START" message`;
  } else {
    log = `${firstName} ${lastName} opted out of text messages by sending a "STOP" message`;
  }
  //messagePreferenceType is enum: "EMAIL" OR "SMS"
  const messagePreferenceType = 'SMS';
  const createParams = {
    datetime: new Date().toISOString(),
    group: record.group,
    description: log,
    messagePreferenceType: messagePreferenceType,
    messagePreferencesHistoryStaffId: record.id,
  }
  await createMessagePreferencesHistory(createParams)
}

async function createMessagePreferencesHistory(params){
  try {
    const createParams = {
        TableName: process.env.API_HERA_MESSAGEPREFERENCESHISTORYTABLE_NAME,
        Item: {
          __typename: 'MessagePreferencesHistory',
          id: nanoid(),
          description: params.description,
          group: params?.group,
          datetime: params.datetime,
          messagePreferenceType: params.messagePreferenceType,
          messagePreferencesHistoryStaffId: params.messagePreferencesHistoryStaffId,
          createdAt: new Date().toISOString(),
          updatedAt: new Date().toISOString()
        }
    };

    if(!createParams.Item.messagePreferencesHistoryStaffId){
      return
    }
    await createDynamoRecord(createParams)
  } catch (error) {
    console.log('Error in function createMessagePreferencesHistory', error)
  }
}

async function getStaffRecords(group, number) {
  let records = [];
  let lastEvaluatedKey = null;
  //loop there everyone with this phone number
  do{
    const params = {
      TableName : process.env.API_HERA_STAFFTABLE_NAME,
      IndexName: 'byPhone',
      ExpressionAttributeNames: { '#p': 'phone', '#g': 'group' },
      ExpressionAttributeValues : { ':p': number, ':g': group },
      KeyConditionExpression : '#p=:p and #g=:g',
      ExclusiveStartKey: lastEvaluatedKey,
    };
    const scanResult = await findRecords(params);
    records = records.concat(scanResult.Items);
    lastEvaluatedKey = scanResult.LastEvaluatedKey;
  }
  while(lastEvaluatedKey);
  return records;
}

function getContentTypeFromName(fileName) {
  const MIME_TYPES = {
    '.jpg': 'image/jpeg',
    '.jpeg': 'image/jpeg',
    '.png': 'image/png',
    '.bmp': 'image/bmp',
    '.gif': 'image/gif',
  };
  const fileNameLower = fileName.toLowerCase();
  for (const key of Object.keys(MIME_TYPES)) {
    if (fileNameLower.includes(key)) {
      return MIME_TYPES[key];
    }
  }
  return 'other';
}

async function findRecords(params){
  console.log(params);
  return new Promise((resolve, reject) => {
    documentClient.query(params, function(err, data) {
      if (err) {
        console.log(err)
        reject(err)
      }
      else {
        console.log(data)
        resolve(data)
      }
    });
  })
}

function getReceiveMessageNotifications(message) {
  const messageLower = `${message || ''}`.toLowerCase().trim(); 
  const STOP_NOTIFICATIONS_SHORTCUT_LIST = ['stop','cancel','end','quit','stopall','stop all', 'unsubscribe'];
  const START_NOTIFICATIONS_SHORTCUT_LIST = ['start', 'unstop'];
  if (START_NOTIFICATIONS_SHORTCUT_LIST.includes(messageLower)) return true;
  if (STOP_NOTIFICATIONS_SHORTCUT_LIST.includes(messageLower)) return false;
  return null;
}

async function updateStaffLastSms(messageId) {
  //use messageId to query message record that got updated
  console.log('messageIdUpdateStaff',messageId)
  const params = {
    TableName : process.env.API_HERA_MESSAGETABLE_NAME,
    Key: { "id": messageId },
  };
  const findResult = await getDynamoRecord(params);
  const messageRecord = findResult.Item;
  console.log('staffUpdateMessageRecord', messageRecord);
  if(Object.keys(messageRecord).length === 0 || !messageRecord.staffId) {
    console.log('Could not find staff');
    return;
  }
  //update last message on staff record.
  const input = {
    id: messageRecord.staffId,
    smsLastMessageTimestamp: (new Date()).toISOString(),
    smsLastMessage: messageRecord.bodyText,
  }
  // mark as last message unread if receiving a response
  console.log('updateStaffInput', input);
  const response = await executeMutation(updateStaff, {input});
  console.log('updateStaffResponse',response);
  if(messageRecord.smsStatus == 'Error') {
    await pushNotification(messageRecord);
    const tenant = await getTenantById(messageRecord.messageTenantId);
    if (tenant) {
      messageRecord.companyName = tenant.companyName;
      await sendErrorEmail(messageRecord);
    }
  }
};

async function sendErrorEmail(record) {
  if (record.tenantId) {
    let subdomain = process.env.WINDOW_ORIGIN
    let tenantURL = `${subdomain}/system/tenants/${record.tenantId}`
    let serverTime = new Date(record.createdAt).toLocaleString()

    let content = `There was an error sending a message, here are the details: <br><br> 
  Error: ${record.smsSendInformation} <br>
  Origination number: ${record.originationNumber} <br>
  Destination number: ${record.destinationNumber} <br>
  Server time: ${serverTime} <br>
  Tenant: ${record.companyName} <br>
  Tenant page: <a href="${tenantURL}">${tenantURL}</a> 
  `

    if(record.error) {
      let details = `<br><br> <b>Error details:</b> <br> ${record.error}`
      content = content.concat(details)
    }

    let emailParams = {
      destinationEmail: process.env.SUPPORT_EMAIL,
      subject: 'Error sending message',
      bodyText: content,
      bodyHtml: content,
    }

    if (process.env.ENV === 'phasethree') {
      emailParams.subject = `(DEV) ${emailParams.subject}`
    }

    await sendEmail(emailParams)
  } else {
    console.log('[sendErrorEmail] Unable to send email. No tenant related.', JSON.stringify(record))
  }
}

/**
 * Send an email using aws pinpoint
 * @param(Object) record - DB Record with send info
 */
function sendEmail(record) {
  return new Promise((resolve, reject) => {
    
    ///------- ENSURE TEST ENVIRONMENT USES TEST EMAIL ------//
    if (isTestEnv() && !!record.destinationEmail && !isValidTestEmail(record.destinationEmail)){ 
      console.log('Exiting sendEmail from the test environment. Destination email does not appear to be a test address:', record)
      return reject("Error attempting to send a message with an invalid test address")
    }

    //--------VERIFY WE HAVE REQUIRED DATA--------//
    if(!record.subject || !record.bodyHtml || !record.bodyText || !record.destinationEmail){
      console.log("Error: Could not send email, missing required data")
      reject("Error: Could not send email, missing required data")
    }
    
    //----------CONSTRUCT EMAIL PARAMS----------//
    var charset = "UTF-8";
    var params = {
      ApplicationId: projectId,
      MessageRequest: {
        Addresses: {
          [record.destinationEmail]:{
            ChannelType: 'EMAIL'
          }
        },
        MessageConfiguration: {
          EmailMessage: {
            SimpleEmail: {
              Subject: {
                Charset: charset,
                Data: record.subject
              },
              HtmlPart: {
                Charset: charset,
                Data: record.bodyHtml
              },
              TextPart: {
                Charset: charset,
                Data: record.bodyText
              }
            }
          }
        }
      }
    };
    
    //----------SEND EMAIL-------------//
    pinpoint.sendMessages(params, function(err, data) {
      // If something goes wrong, print an error message.
      if(err) {
        console.log(err.message);
        reject(err)
      } else {
        console.log(data)
        console.log(data['MessageResponse']['Result'][record.destinationEmail])
        let result = "Email sent! Message ID: " + data['MessageResponse']['Result'][record.destinationEmail]['MessageId']
        console.log(result);
        resolve(result)
      }
    });
  
  })
}


module.exports = {
  getMessageByMessageCarrierId,
  updateMessageStatus,
  getTenantById,
  getTenantByOriginationNumber,
  updateMessageRecord,
  pushNotification,
  getDynamoRecord,
  saveDataToS3,
  createMessageRecord,
  handleUpdateTextStatus,
  updateStaffMemberTextStatus,
  handleUpdateAssociateReceiveNotifications,
  getFetchStaffParams,
  updateStaffRecord,
  getStaffRecords,
  getContentTypeFromName,
  findRecords,
  getReceiveMessageNotifications,
  updateStaffLastSms,
  sendErrorEmail,
  sendEmail,
};