const createMessage = /* GraphQL */ `
mutation CreateMessage(
  $input: CreateMessageInput!
  $condition: ModelMessageConditionInput
) {
  createMessage(input: $input, condition: $condition) {
    id
    group
    createdAt 
    channelType
    staffId
    bodyText
    isReadS
    messageType
    staff{
      id
      status
    }
    sender{
      id
    }
    isReadBy{
      items {
        userID
      }
    }
    attachment{
      id
      group
      s3Key
      expirationDate
      contentType
    }
  }
}
`;

const updateStaff = /* GraphQL */ `
  mutation UpdateStaff(
    $input: UpdateStaffInput!
    $condition: ModelStaffConditionInput
  ) {
    updateStaff(input: $input, condition: $condition) {
      id
      photo
      smsLastMessageTimestamp
      smsLastMessage
    }
  }
`;

const createAttachment = /* GraphQL */ `
  mutation CreateAttachment(
    $input: CreateAttachmentInput!
    $condition: ModelAttachmentConditionInput
  ) {
    createAttachment(input: $input, condition: $condition) {
      id
      group
      s3Key
      expirationDate
      contentType
    }
  }
`;

const createNotification = /* GraphQL */ `
  mutation CreateNotification(
    $input: CreateNotificationInput!
    $condition: ModelNotificationConditionInput
  ) {
    createNotification(input: $input, condition: $condition) {
      id
      group
      owner
      title
      createdAt
      description
      releaseNotes
      payload
      clickAction
      isRead
      updatedAt
    }
  }
`

const getTenant = /* GraphQL */ `
  query GetTenant($id: ID!) {
    getTenant(id: $id) {
      id
      originationNumber
      messageServiceProvider
      companyName
    }
  }
`

const tenantByOriginationNumber = /* GraphQL */ `
    query TenantByOriginationNumber(
        $originationNumber: String
        $sortDirection: ModelSortDirection
        $filter: ModelTenantFilterInput
        $limit: Int
        $nextToken: String
    ) {
        tenantByOriginationNumber(
            originationNumber: $originationNumber
            sortDirection: $sortDirection
            filter: $filter
            limit: $limit
            nextToken: $nextToken
        ) {
            items {
                id
                group
            }
        }
    }
`;

const messageByCarrierMessageId = /* GraphQL */ `
  query MessageByCarrierMessageId(
    $carrierMessageId: String
    $group: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelMessageFilterInput
    $limit: Int
    $nextToken: String
  ) {
    messageByCarrierMessageId(
      carrierMessageId: $carrierMessageId
      group: $group
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        carrierMessageId
        group
        smsStatus
        smsSendInformation
        destinationNumber
        tenant {
          id
          companyName
          originationNumber
          messageServiceProvider
        }
        staffId
      }
      nextToken
    }
  }
`

const updateMessage = /* GraphQL */ `
  mutation UpdateMessage(
    $input: UpdateMessageInput!
    $condition: ModelMessageConditionInput
  ) {
    updateMessage(input: $input, condition: $condition) {
      id
      group
      smsStatus
      smsSendInformation
      staffId
      createdAt
      channelType
      bodyText
      messageType
    }
  }
`;

module.exports = {
    createMessage,
    createAttachment,
    updateStaff,
    createNotification,
    getTenant,
    tenantByOriginationNumber,
    messageByCarrierMessageId,
    updateMessage
};