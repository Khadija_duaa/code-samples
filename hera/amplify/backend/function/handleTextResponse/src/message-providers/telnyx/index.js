const { getMessageByMessageCarrierId, updateMessageStatus, getTenantById, sendErrorEmail } = require("../../helpers");

async function messageHandler(req, handlerPayload) {
  const {
    handleCreateMessagesForTenants,
  } = handlerPayload;
  const eventType = req.body.data.event_type;
  const payload = req.body.data.payload;
  const carrierMessageId = payload.id;
  if (eventType === 'message.sent') {
    return console.log(`Message with ID: ${carrierMessageId} has been sent.`);
  }
  if (eventType === 'message.finalized') {
    console.log(`Message with ID: ${carrierMessageId} has been finalized.`);
    return (await handleMessageFinalized(carrierMessageId, payload));
  }
  if (eventType === 'message.received') {
    const record = req.body.data.payload;
    const timestamp = req.headers['telnyx-timestamp'];
    const messageBody = record.text;
    const media = record.media ? record.media : [];
    const number = record.from.phone_number;
    const originationNumber = record.to[0].phone_number;
    await handleCreateMessagesForTenants(originationNumber, {
      number,
      messageBody,
      media,
    })
  }
}

async function outboundMessageHandler(req) {
  console.log('app Data', req.body.data);
  console.log('app payload', req.body.data.payload);
  const type = req.body.data.event_type;
  const record = req.body.data.payload;
  return {
    type,
    record,
    messageId: record.organization_id,
    recordMessageId: record.id,
  };
}

async function handleMessageFinalized(carrierMessageId, payload) {
  const messageToUpdate = await getMessageByMessageCarrierId(carrierMessageId);
  if(payload.errors.length) {
    messageToUpdate.smsStatus = 'Error';
    messageToUpdate.smsSendInformation = `${payload.errors[0].detail} (${payload.errors[0].code})`;
  } else {
    messageToUpdate.smsStatus = 'Success';
    messageToUpdate.smsSendInformation = 'Text Message Sent!';
  }
  await updateMessageStatus(messageToUpdate);
  if(messageToUpdate.smsStatus == 'Error') {
    const tenant = await getTenantById(messageToUpdate.tenant.id);
    if (tenant && messageToUpdate.destinationNumber) {
      messageToUpdate.tenantId = tenant.id;
      messageToUpdate.companyName = tenant.companyName;
      messageToUpdate.originationNumber = tenant.originationNumber;
      messageToUpdate.error = JSON.stringify(payload.errors[0]);
      await sendErrorEmail(messageToUpdate);
    }
  }
}

module.exports = {
  config: {
    messageHandler,
    outboundMessageHandler,
  },
}
