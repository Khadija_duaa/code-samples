const telnyx = require('./telnyx');

/** Setup the config of each service provider based on user agent */
const PROVIDERS_CONFIG = {
  'telnyx-webhooks': telnyx.config,
};

function getServiceConfig(req) {
  const agent = Object.keys(PROVIDERS_CONFIG).find(agent => agent === req.headers['user-agent']);
  if (!agent) return { isValid: false, config: null };
  return {
    isValid: true,
    config: PROVIDERS_CONFIG[agent],
  };
}

module.exports = {
  getServiceConfig,
};
