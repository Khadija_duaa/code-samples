var AWS = require('aws-sdk');
var pinpoint = new AWS.Pinpoint({region: process.env.PIN_POINT_REGION}); 
var projectId = process.env.PROJECT_ID;
const awsServerlessExpress = require('aws-serverless-express');
const app = require('./app');
const server = awsServerlessExpress.createServer(app);
// var originationNumber = process.env.ORIGINATION_NUMBER;

AWS.config.region = process.env.region;
var documentClient = new AWS.DynamoDB.DocumentClient({apiVersion: '2012-08-10'})

const {
  executeMutation,
  executeQuery,
} = require('./appSync.js');

const {
  createMessage,
  updateStaff
} = require('./graphql.js');

const { updateDynamoRecord, buildUpdateParams, createDynamoRecord } = require('./dynamodbHelper.js');
const { createMessagePreferencesHistory } = require('./helpers.js')


exports.handler = async (event, context, callback) => {
  console.log(JSON.stringify(event, null, 2))
  if(event.path != '/message' && event.path != '/outboundMessage'){
    for(const record of event.Records){
      const message = JSON.parse(record.Sns.Message);
      console.log(message);

      await handleMessage(message);
    }

    return awsServerlessExpress.proxy(server, {}, context, 'PROMISE').promise;
  }
  else{
    //Bandwidth
    return awsServerlessExpress.proxy(server, event, context, 'PROMISE').promise;
  }
};

async function handleMessage(message={}) {
  const messageBody = message.messageBody;
  const number = message.originationNumber;
  const originationNumber = message.destinationNumber || process.env.ORIGINATION_NUMBER;

  if (!message.destinationNumber) {
    await handleSMSTextMessage({number, group: null}, messageBody, originationNumber);
    return;
  }

  const tenants = await getTenantsByOriginationNumber(message.destinationNumber);
  for (const { group } of tenants) {
    await handleSMSTextMessage({number, group}, messageBody, originationNumber);
  }
}

async function handleSMSTextMessage({number, group}, messageBody, originationNumber) {
  const messageBodyLower = messageBody.toLowerCase().trim();
  const validCommands = ['start', 'stop'];

  if (!validCommands.includes(messageBodyLower)) {
    await createMessageRecord({number, group}, messageBody);
    return;
  }

  const isStart = messageBodyLower === 'start';
  await updateStaffMemberTextStatus({number, group}, isStart, originationNumber);
}

async function getTenantsByOriginationNumber(originationNumber) {
  const params = {
    TableName : process.env.API_HERA_TENANTTABLE_NAME,
    IndexName: 'byOriginationNumber',
    KeyConditionExpression : '#number = :number',
    ExpressionAttributeNames : {
      '#number': 'originationNumber',
      '#group': 'group',
    },
    ExpressionAttributeValues : {':number' : originationNumber},
    ExclusiveStartKey: null,
    ProjectionExpression: '#group, #number',
  };

  let records = [];

  do {
    const scanResult = await findRecords(params);

    records = records.concat(scanResult.Items);
    params.ExclusiveStartKey = scanResult.LastEvaluatedKey;
  }
  while (params.ExclusiveStartKey);

  return records;
}

async function updateStaffMemberTextStatus({number, group}, receiveTexts, originationNumber){
  try {
    //Get the staff members record(s)
    const params = getFetchStaffParams({number, group});

    do{
      const scanResult = await findRecords(params);

      params.ExclusiveStartKey = scanResult.LastEvaluatedKey;
      await updateStaffsAndSendMessages(scanResult.Items, number, receiveTexts, originationNumber);
    }
    while(params.ExclusiveStartKey);
  }
  catch(e) {
    const message = "There was an error updating your text message preferences. Please contact an admin.";
    sendSMS(message, number, originationNumber);
    console.log(e);
  }
}

async function updateStaffsAndSendMessages(records, number, receiveTexts, originationNumber) {
  try {
    for (const record of records) {
      await updateStaffRecord(record, receiveTexts);
      
      const message = (receiveTexts === true) ?
        "You have been registered to receive messages. Please text STOP to stop receiving text updates.":
        "You have opted out of receiving messages. If you want to start again reply back with START.";

      sendSMS(message, number, originationNumber);
    }
  }
  catch (error) {
    throw error;
  }
}

async function updateStaffRecord(record, receiveTexts){

  try {
    const updateParams = {
      id: record.id,
      receiveTextMessages: receiveTexts
    }
    const buildParams = buildUpdateParams(process.env.API_HERA_STAFFTABLE_NAME, updateParams, updateParams.id);
    //update staff
    await updateDynamoRecord(buildParams);
  
    const firstName = record.firstName || '';
    const lastName = record.lastName || '';
    //messagePreferenceType is enum: "EMAIL" OR "SMS"
    const messagePreferenceType = 'SMS';
    const createParams = {
      datetime: new Date().toISOString(),
      group: record.group,
      description: `${firstName} ${lastName} changed the opt-in status to ${receiveTexts}`,
      messagePreferenceType: messagePreferenceType,
      messagePreferencesHistoryStaffId: record.id,
    }

    await createMessagePreferencesHistory(createParams)
  } catch (error) {
    console.log('Error in function updateStaffRecord', error)
  }

}



async function findRecords(params){
  return new Promise((resolve, reject) => {
    documentClient.query(params, function(err, data) {
      if (err) {
        console.log(err)
        reject(err)
      }
      else {
        console.log(data)
        resolve(data)
      }
    });
  })
}


function sendSMS(message, number, originationNumber) {
  var destinationNumber = number.replace(/[^0-9]/g, '');
  if (destinationNumber.length == 10) {
    destinationNumber = "+1" + destinationNumber;
  }
  
  var params = {
    ApplicationId: projectId,
    MessageRequest: {
      Addresses: {
        [destinationNumber]: {
          ChannelType: 'SMS'
        }
      },
      MessageConfiguration: {
        SMSMessage: {
          Body: message,
          MessageType: "TRANSACTIONAL",
          OriginationNumber: originationNumber
        }
      }
    }
  };

  pinpoint.sendMessages(params, function(err, data) {
    // If something goes wrong, print an error message.
    if(err) {
      console.log(err.message);
    // Otherwise, show the unique ID for the message.
    } else {
      console.log("Message sent! " 
          + data['MessageResponse']['Result'][destinationNumber]['StatusMessage']);
    }
  });
}

function getFetchStaffParams({number, group}) {
  const params = {
    TableName : process.env.API_HERA_STAFFTABLE_NAME,
    IndexName: "byPhone",
    KeyConditionExpression : "#phone = :phone",
    ExpressionAttributeNames : { "#phone" : 'phone' },
    ExpressionAttributeValues : { ":phone" : number },
    ExclusiveStartKey: null,
  };

  if (group) {
    params.KeyConditionExpression = '#phone = :phone AND #group = :group';
    params.ExpressionAttributeNames["#group"] = 'group';
    params.ExpressionAttributeValues[":group"] = group;
  }

  return params;
}

async function createMessageRecord({number, group}, messageBody){
  //Get the staff members ids
  console.log({number})
  console.log({messageBody})
    //Get the staff members record(s)

  const params = getFetchStaffParams({number, group});

  try {
    do {
      const scanResult = await findRecords(params);

      params.ExclusiveStartKey = scanResult.LastEvaluatedKey;
      await createMessageRecords(scanResult.Items, messageBody);
    }
    while (params.ExclusiveStartKey);
  }
  catch (error) {
    console.log(error);
  }
}

async function createMessageRecords(records, messageBody) {
  try {
    for(const record of records) {
      const input = {
        staffId: record.id,
        group: record.group,
        bodyText: messageBody,
        channelType: "RESPONSE",
        messageType: "messenger",
        isReadS: 'false'
      }
      
      const response = await executeMutation(createMessage, {input});
      console.log(response);
    }
  }
  catch (error) {
    throw error;
  }
}
