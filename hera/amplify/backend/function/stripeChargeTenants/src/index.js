/*
Use the following code to retrieve configured secrets from SSM:

const aws = require('aws-sdk');

const { Parameters } = await (new aws.SSM())
  .getParameters({
    Names: ["STRIPE_SECRET_KEY"].map(secretName => process.env[secretName]),
    WithDecryption: true,
  })
  .promise();

Parameters will be of the form { Name: 'secretName', Value: 'secretValue', ... }[]
*/
/* Amplify Params - DO NOT EDIT
	API_HERA_CARDTABLE_ARN
	API_HERA_CARDTABLE_NAME
	API_HERA_GRAPHQLAPIENDPOINTOUTPUT
	API_HERA_GRAPHQLAPIIDOUTPUT
	API_HERA_INVOICELINEITEMTABLE_ARN
	API_HERA_INVOICELINEITEMTABLE_NAME
	API_HERA_INVOICETABLE_ARN
	API_HERA_INVOICETABLE_NAME
	API_HERA_MESSAGETABLE_ARN
	API_HERA_MESSAGETABLE_NAME
	API_HERA_STRIPEQUEUETABLE_ARN
	API_HERA_STRIPEQUEUETABLE_NAME
	API_HERA_TENANTTABLE_ARN
	API_HERA_TENANTTABLE_NAME
	ENV
	REGION
Amplify Params - DO NOT EDIT *///************ NEW VERSION - PENDING MESSAGES TABLE ************//
// const { generateTemplate } = require('./emailTemplate')

// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
// Set the region
AWS.config.update({ region: process.env.REGION });

var ddb = new AWS.DynamoDB.DocumentClient({ apiVersion: '2012-08-10' });

const today = new Date()
const year = today.getFullYear()
const month = today.getMonth()

exports.handler = async (event) => {
    console.log(JSON.stringify(event, null, 2));

    // find tenants with a non-trial and not "None" account
    var tenants = []
    var lastEvaluatedKey = null
    do {
        var params = {
            ExpressionAttributeNames: {
                '#g': 'group',
            },
            ExpressionAttributeValues: {
                ':t': 'trial',
                ':n': 'None'
            },
            FilterExpression: "NOT contains(accountPremiumStatus, :t) and NOT contains(accountPremiumStatus, :n)",
            TableName: process.env.API_HERA_TENANTTABLE_NAME,
            ProjectionExpression: '#g,id,companyName,accountPremiumStatus,payOutstandingByDate,trialExpDate,discountPercent,discountFixed,discountPercentLabel,discountFixedLabel,stripeCustomerId,stripeBillingEmail,growSurfParticipantId,growSurfReferralComplete',
            ExclusiveStartKey: lastEvaluatedKey
        };
        var scanResult = await scanDynamoRecords(params);
        var items = scanResult.Items
        lastEvaluatedKey = scanResult.LastEvaluatedKey
        tenants = [...tenants, ...items]
    } while (lastEvaluatedKey)

    console.log('---tenants', tenants.length)

    try {

        let limit = 10
        let index = 0
        let tenantIndex = 0

        // loop through found tenants
        for (const tenant of tenants) {

            if(typeof(tenant)==='undefined' || !tenant || typeof(tenant.accountPremiumStatus) === 'undefined' || !tenant.accountPremiumStatus){
                console.log('Tenant has no accountPremiumStatus or does not exist, skipping', tenant)
                continue;
            }
            
            // skip tenant if plan is "None"
            if(!!tenant && tenant.accountPremiumStatus !== undefined && tenant.accountPremiumStatus.includes('None')) {
                console.log('Tenant has plan None, skipping')
                continue;
            }
            
            console.log('---tenant Index: ', tenantIndex)
            console.log('---tenant: ', JSON.stringify(tenant))

            let queueId = String(year) + '-' + String(month + 1).padStart(2, '0') + '-' + tenant.id

            let stripeQueue = {
                id: queueId,
                group: tenant.group,
                tenant: JSON.stringify(tenant),
                createdAt: new Date().toISOString()
            }

            var createParams = {
                TableName: process.env.API_HERA_STRIPEQUEUETABLE_NAME,
                Item: stripeQueue
            };

            await createDynamoRecord(createParams);

            index++
            tenantIndex++

            if (index >= limit) {
                console.log('---Sleeping...')
                await createCustomTimeout(5)
                index = 0
            }

        }

    } catch (e) {
        console.log('---Something went wrong', e)
    }
}

function createCustomTimeout(seconds) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log("---wakeup...")
            resolve();
        }, seconds * 1000);
    });
}

function scanDynamoRecords(params) {
    return new Promise((resolve, reject) => {
        // Create DynamoDB service object
        ddb.scan(params, function (err, data) {
            if (err) {
                console.error("Unable to get items. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                console.log("Get Item succeeded:", JSON.stringify(data, null, 2));
                resolve(data)
            }
        });
    })
}

function createDynamoRecord(params) {
    console.log("Creating Record");
    return new Promise((resolve, reject) => {
        ddb.put(params, function (err, data) {
            if (err) {
                console.log("Unable to put item. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                console.log("PutItem succeeded:", JSON.stringify(data, null, 2));
                resolve(data)
            }
        });


    })
}