const { createVehicleHistory } = require('./mutations')
const { tableFieldValueNames } = require('./tableFieldValueNames')

const updateVehicleHistory = async function(update, executeMutation) {
  update.changeType = 'Vehicle Details Updated'
  update.changes = update.changes.map(change => {
    let key, keys = Object.keys(change)
    if (keys.length) {
      key = keys[0]
      return JSON.stringify({
        dbFieldName: key,
        readableFieldName: tableFieldValueNames[key],
        oldValue: change[key]['oldValue'],
        newValue: change[key]['newValue'],
      })
    } else {
      return {}
    }
  })
  let updateResponse = null
  try {
    updateResponse = await executeMutation(createVehicleHistory, { input: update });
    console.log('VH upate', updateResponse)
  } catch (e) {
    console.log('updating vehicle history failed', e)
  }
  return updateResponse
}

module.exports = {
  updateVehicleHistory
}