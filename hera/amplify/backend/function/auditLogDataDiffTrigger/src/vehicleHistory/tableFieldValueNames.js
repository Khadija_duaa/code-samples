const tableFieldValueNames = {
    'type': 'Description',
    'name': 'Name',
    'make': 'Make',
    'model': 'Model',
    'year': 'Year',
    'notes': 'Notes',
    'accidentReports': 'Accident Reports',
    'category': 'Category',
    'company': 'Company',
    'customDeliveryVan': 'Custom Delivery Van',
    'dateEnd': 'Date End',
    'dateStart': 'Date Start',
    'gasCard': 'Gas card',
    'group': 'Group',
    'lastOdometerReadingDate': 'Last Odometer Reading Date',
    'licensePlate': 'License Plate',
    'licensePlateExp': 'License Plate Exp',
    'mileage': 'Mileage',
    'otherProvider': 'Other Provider',
    'overTenThousandPounds': 'Over Ten Thousand Pounds',
    'ownership': 'Ownership',
    'provider': 'Provider',
    'rentalAgreementNumber': 'Rental Agreement Number',
    'rentalContracts': 'Rental Contracts',
    'state': 'State',
    'status': 'Status',
    'tollPassId': 'Toll Pass Id',
    'vin': 'Vin',
    'device': 'Device',
    'parkingSpace': 'Parking Space',
    'vehicleType': 'Vehicle Type'
}

module.exports = {
    tableFieldValueNames
}