const createVehicleHistory = /* GraphQL */ `
  mutation CreateVehicleHistory(
    $input: CreateVehicleHistoryInput!
    $condition: ModelVehicleHistoryConditionInput
  ) {
    createVehicleHistory(input: $input, condition: $condition) {
      id
      group
      changeType
      date
      changes
      statusChangeReason
      tenantId
      userId
      vehicleId
      createdAt
      updatedAt
      vehicle {
        id
        group
      }
    }
  }
`;

module.exports = {
    createVehicleHistory
}