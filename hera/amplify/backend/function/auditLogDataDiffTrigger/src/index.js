const {
  executeQuery,
  executeMutation
} = require('./appSync')

const {
  logsByMutatedRecordIdAndCreatedAt,
  updateAuditLog
} = require('./graphql')

/**
* @type {import('@types/aws-lambda').APIGatewayProxyHandler}
*/
exports.handler = async event => {
  console.log(`EVENT: ${JSON.stringify(event)}`);
  for (const record of event.Records){
    const data = record.dynamodb.NewImage;
    const createdAt = new Date(data.createdAt.S);
    const mutatedRecordId = data.mutatedRecordId.S;
    const mutatedData = JSON.parse(data.mutatedData.S);
    const mutationName = data.mutationName.S;

    try {
      const input = {
        mutatedRecordId: mutatedRecordId,
        createdAt: {
          lt: createdAt
        },
        limit: 1,
        sortDirection: 'DESC'
      };
  
      const result = await executeQuery(logsByMutatedRecordIdAndCreatedAt, input);
      const prevAuditLog = result.logsByMutatedRecordIdAndCreatedAt.items[0];
      const prevMutatedData = JSON.parse(prevAuditLog.mutatedData);
      const differences = getMutatedInputDifferences(mutatedData, prevMutatedData);
      const updateInput = {
        id: data.id.S, 
        mutatedDataDiff: JSON.stringify(differences)
      };
      
      await executeMutation(updateAuditLog, { input: updateInput});

    
      // Check for updates to Vehicles and move to separate table
      // Exclude Odometer updates which go to accident
      if(
        mutationName === 'UpdateVehicle' 
        && differences.length 
        && !differences.filter(d=>typeof(d.mileage)!='undefined').length
      ){
        
        const { updateVehicleHistory } = require('./vehicleHistory/updateVehicleHistory')
  
        await updateVehicleHistory({  
          vehicleId: mutatedRecordId,
          date: createdAt,
          changes: differences,
          group:data.group.S,
          tenantId: data.tenantID.S,
          userId: data.userID.S,
          userFirstName: data.firstName.S,
          userLastName: data.lastName.S,
          userEmail: data.email.S,
          changeType: null
        }, executeMutation)
        
      }
    } catch (err) {
      console.log("==> error", err);
    }
    
  };
  return Promise.resolve('Successfully processed DynamoDB record');
};

function getMutatedInputDifferences(input, previousInput) {
  const keysNotIncluded = new Set(['updatedAt', 'createdAt', 'id', 'group']);
  const differences = [];

  for (const key in input) {
    if (previousInput.hasOwnProperty(key) && !keysNotIncluded.has(key)) {
      const oldValue = previousInput[key];
      const newValue = input[key];

      if (typeof oldValue === 'object' || typeof newValue === 'object') {
        if (oldValue?.id !== newValue?.id) {
          differences.push({ [key]: { oldValue: oldValue?.id ?? '', newValue: newValue?.id ?? '' } });
        }
      } else if (oldValue !== newValue) {
        differences.push({ [key]: { oldValue: oldValue ?? '', newValue: newValue ?? '' } });
      }
    }
  }

  return differences;
}
