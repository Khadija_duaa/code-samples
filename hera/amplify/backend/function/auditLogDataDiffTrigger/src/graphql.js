const logsByMutatedRecordIdAndCreatedAt = /* GraphQL */ `
  query LogsByMutatedRecordIdAndCreatedAt(
    $mutatedRecordId: String
    $createdAt: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelAuditLogFilterInput
    $limit: Int
    $nextToken: String
  ) {
    logsByMutatedRecordIdAndCreatedAt(
      mutatedRecordId: $mutatedRecordId
      createdAt: $createdAt
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        tenantID
        group
        email
        userID
        cognitoSub
        firstName
        lastName
        tenantName
        mutationName
        mutationNameText
        mutatedRecordId
        mutatedData
        mutatedDataDiff
        createdAt
        ipAddress
        ttl
        pageUrl
        logRocketSessionUrl
        updatedAt
      }
      nextToken
    }
  }
`;

const updateAuditLog = /* GraphQL */ `
  mutation UpdateAuditLog(
    $input: UpdateAuditLogInput!
    $condition: ModelAuditLogConditionInput
  ) {
    updateAuditLog(input: $input, condition: $condition) {
      id
      tenantID
      group
      email
      userID
      cognitoSub
      firstName
      lastName
      tenantName
      mutationName
      mutationNameText
      mutatedRecordId
      mutatedData
      mutatedDataDiff
      createdAt
      ipAddress
      ttl
      pageUrl
      logRocketSessionUrl
      updatedAt
    }
  }
`;

module.exports = {
    logsByMutatedRecordIdAndCreatedAt,
    updateAuditLog
}