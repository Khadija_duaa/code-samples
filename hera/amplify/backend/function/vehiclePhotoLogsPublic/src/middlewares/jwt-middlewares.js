const jwt = require('jsonwebtoken');
const {encrypt, decrypt} = require('../helpers/crypto-helpers');
const ShortenUrl = require('../entities/ShortenUrl');
const { getSecretValue } = require('/opt/nodejs/ssm');
let secretTokenKey
async function loadSecretTokenKey(){
  if(!secretTokenKey){
    secretTokenKey = await getSecretValue(process.env.SECRET_TOKEN_KEY);
  }
}

async function signToken(payloadToSign) {
  const json = JSON.stringify(payloadToSign);
  const encrypted = await encrypt(json);
  const payload = {
    iv: encrypted.iv,
    body: encrypted.body,
  };
  await loadSecretTokenKey();
  // Sign token
  return jwt.sign(payload, secretTokenKey, { expiresIn: '1d' });
}

async function generateTokens(req, res, next) {
  try {
    const payloadCopy = {...req.validPayload};
    const routes = payloadCopy.routes;
    delete payloadCopy.routes;

    const signedTokens = [];
    for (const route of routes) {
      try {
        const payloadToSign = {...payloadCopy, ...route};
        const token = await signToken(payloadToSign);
        signedTokens.push({id: route.id, payloadToSign, token});
      }
      catch (error) {
        console.log({error, route});
      }
    }

    if (!signedTokens.length) throw new Error('Bad Request');

    req.signedTokens = signedTokens;
    next();
  }
  catch (error) {
    console.log({error, req});
    res.sendStatus(400);
  }
}

async function validateToken(req, res, next) {
  try {
    const shortenUrl = await ShortenUrl.getByToken(req.params.token);
    if(!shortenUrl) throw new Error('Bad Request');
    await loadSecretTokenKey();
    const token = shortenUrl.urlToken;
    const { iv, body } = jwt.verify(token, secretTokenKey);
    const json = await decrypt(body, iv);
    req.shortenUrl = shortenUrl;
    req.validToken = req.params.token;
    req.validPayload = JSON.parse(json);
    next(); 
  }
  catch (error) {
    console.log({error, req});
    res.status(400).json({ type: 'INVALID_TOKEN' });
  }
}

module.exports = {
  generateTokens,
  validateToken,
};
