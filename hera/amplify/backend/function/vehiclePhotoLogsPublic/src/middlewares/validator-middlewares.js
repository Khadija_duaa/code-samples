const momentTz = require('moment-timezone');
const Tenant = require('../entities/Tenant');
const DailyRoster = require('../entities/DailyRoster');
const { getErrorPayload } = require('../utils');

const requiredPayloadPropsForToken = [
  'tenantId',
  'rosteredDayId',
  'sentByUserId',
  'routes',
];

const requiredPayloadPropsForRoute = [
  'id',
  'associateId',
  'vehicleId',
];

const requiredPayloadPropsForDocument = [
  'group',
  'name',
  'type',
  'key',
  'documentDate',
  'uploadDate',
];

function validatePayloadForDocument(req, res, next) {
  try {
    const payload = req.body;
    for (const prop of requiredPayloadPropsForDocument) {
      if (!payload.hasOwnProperty(prop)) throw new Error('Bad Request');
    }
    req.validDocumentPayload = {...payload};
    next();
  }
  catch (error) {
    console.log(JSON.stringify({req, error}));
    res.sendStatus(400);
  }
}

function validatePayloadForTokensCreation(req, res, next) {
  try {
    const payload = req.body;
    for (const prop of requiredPayloadPropsForToken) {
      if (!payload.hasOwnProperty(prop)) throw new Error('Bad Request');
    }

    if (!Array.isArray(payload.routes)) throw new Error('Bad Request');

    const validRoutes = [];
    for (const route of payload.routes) {
      let isValid = true;
      for (const prop of requiredPayloadPropsForRoute) {
        if (!route.hasOwnProperty(prop)) {
          isValid = false;
          break;
        }
      }
      if (isValid) {
        validRoutes.push({...route});
      }
    }

    if (!validRoutes.length) throw new Error('Bad Request');

    req.validPayload = {...payload, routes: validRoutes};
    next();
  }
  catch (error) {
    console.log(JSON.stringify({req, error}));
    res.sendStatus(400);
  }
}

async function validateTodayIsRosterDay(req, res, next) {
  try {
    const payload = req.validPayload;

    const tenant = await Tenant.getById(payload.tenantId);
    if (!tenant)  throw new Error('Bad Request');

    const dailyRoster = await DailyRoster.getById(payload.rosteredDayId);
    if (!dailyRoster) throw new Error('Bad Request');

    const rosterDay = dailyRoster.notesDate; // YYYY-MM-DD
    const tenantToday = momentTz.tz(tenant.timeZone).format('YYYY-MM-DD');

    if (rosterDay !== tenantToday) throw new Error('Invalid Roster Day');

    req.tenant = tenant;
    req.rosteredDay = dailyRoster;
    next();
  }
  catch (error) {
    console.log({req, error});
    if (!error.message.includes('Invalid Roster Day')) {
      return res.sendStatus(400);
    }

    try {
      const payload = await getErrorPayload(req.validPayload);
      res.status(400).json({
        type: 'INVALID_ROSTER_DAY',
        payload,
      });
    }
    catch (error) {
      console.log({req, error});
      res.sendStatus(400);
    }
  }
}

function validateVehiclePhotoLogS3Key(req, res, next) {
  try {
    if (!req.body || !req.body.s3Key) throw new Error('Bad Request');
    if (typeof req.body.s3Key !== 'string') throw new Error('Bad Request');

    const s3KeyPrefix = 'photo-log-images/' + req.tenant.group + '/vehicles/';
    if (!req.body.s3Key.startsWith(s3KeyPrefix)) throw new Error('Bad Request');

    if (!req.body.contentType) throw new Error('Bad Request');
    if (typeof req.body.contentType !== 'string') throw new Error('Bad Request');

    const safeImageMymeTypes = [
      'image/apng',
      'image/avif',
      'image/gif',
      'image/jpeg',
      'image/png',
      'image/svg+xml',
      'image/webp',
    ];
    if (!safeImageMymeTypes.includes(req.body.contentType)) throw new Error('Bad Request');

    req.s3Payload = {
      s3Key: req.body.s3Key,
      contentType: req.body.contentType,
    };
    next();
  }
  catch(error) {
    console.log({req, error});
    res.sendStatus(400);
  }
}

module.exports = {
  validateTodayIsRosterDay,
  validateVehiclePhotoLogS3Key,
  validatePayloadForTokensCreation,
  validatePayloadForDocument,
};
