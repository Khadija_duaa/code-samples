module.exports.dailyLogFragment = /* GraphQL */ `
fragment dailyLogFragment on DailyLog {
    id
    group
    type
    date
    notes
    vehicleId
    creationLinkSentDateTime
    dailyLogCreationLinkSentByUserId
    dailyLogCreationLinkAssociateId
    dailyLogTakenByUserId
    dailyLogTakenByAssociateId
    dailyLogRosteredDayId
    dailyLogVehicleId
    dailyLogCreatedByUserId
}`