const { dailyLogFragment } = require('./fragments')
module.exports.createDailyLog = dailyLogFragment + /* GraphQL */ `
  mutation CreateDailyLog(
    $input: CreateDailyLogInput!
    $condition: ModelDailyLogConditionInput
  ) {
    createDailyLog(input: $input, condition: $condition) {
      ...dailyLogFragment
    }
  }
`;

module.exports.createDocument = /* GraphQL */ `
  mutation CreateDocument(
    $input: CreateDocumentInput!
    $condition: ModelDocumentConditionInput
  ) {
    createDocument(input: $input, condition: $condition) {
      key
      group
      name
      type
    }
  }
`;

module.exports.createShortenUrl = /* GraphQL */ `
  mutation CreateShortenUrl(
    $input: CreateShortenUrlInput!
    $condition: ModelShortenUrlConditionInput
  ) {
    createShortenUrl(input: $input, condition: $condition) {
      id
      shortenId
    }
  }
`;

module.exports.updateShortenUrl = /* GraphQL */ `
  mutation UpdateShortenUrl(
    $input: UpdateShortenUrlInput!
    $condition: ModelShortenUrlConditionInput
  ) {
    updateShortenUrl(input: $input, condition: $condition) {
      id
      group
      type
      createdAt
    }
  }
`;
