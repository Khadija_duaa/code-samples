module.exports.getTenant = /* GraphQL */ `
  query GetTenant($id: ID!) {
    getTenant(id: $id) {
      id
      logo
      group
      timeZone
      companyName
      allowLibraryUpload
    }
  }
`;

module.exports.getVehicle = /* GraphQL */ `
  query GetVehicle($id: ID!) {
    getVehicle(id: $id) {
      id
      name
      group
    }
  }
`;

module.exports.getStaff = /* GraphQL */ `
  query GetStaff($id: ID!) {
    getStaff(id: $id) {
      id
      group
      firstName
      lastName
    }
  }
`;

module.exports.getDailyRoster = /* GraphQL */ `
  query GetDailyRoster($id: ID!) {
    getDailyRoster(id: $id) {
      id
      group
      notesDate
    }
  }
`;

module.exports.shortenUrlsByShortenId = /* GraphQL */ `
  query ShortenUrlsByShortenId(
    $shortenId: ID
    $sortDirection: ModelSortDirection
    $filter: ModelShortenUrlFilterInput
    $limit: Int
    $nextToken: String
  ) {
    shortenUrlsByShortenId(
      shortenId: $shortenId
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        type
        shortenId
        expirationTTL
        urlToken
        createdAt
        updatedAt
        dailyLog {
          id
          group
          type
          date
          documents {
            items {
              group
              name
              key
              type
            }
            nextToken
          }
        }
      }
      nextToken
    }
  }
`;

module.exports.dailyLogsByVehicleAndDate = /* GraphQL */ `
  query DailyLogsByVehicleAndDate(
    $vehicleId: String
    $date: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelDailyLogFilterInput
    $limit: Int
    $nextToken: String
  ) {
    dailyLogsByVehicleAndDate(
      vehicleId: $vehicleId
      date: $date
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        vehicleId
        rosteredDay {
          id
          notesDate
        }
        shortenUrls {
          items {
            id
            shortenId
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
