const { createShortenUrl, updateShortenUrl } = require("../graphql/mutations");
const { shortenUrlsByShortenId } = require("../graphql/queries");
const { executeQuery, executeMutation } = require("../helpers/appsync-helpers");
const { nanoid } = require('nanoid');

async function create(token, payload) {
  const input = {
    group: payload.group,
    type: 'DAILYLOG',
    shortenId: nanoid(12),
    urlToken: token,
    shortenUrlDailyLogId: payload.shortenUrlDailyLogId,
  };
  const data = await executeMutation(createShortenUrl, {input});
  return {
    id: data.createShortenUrl.id,
    shortenId: data.createShortenUrl.shortenId,
  };
}

async function update(id, payload) {
  const input = {
    id,
    ...payload,
  };
  const data = await executeMutation(updateShortenUrl, {input});
  return { id: data.updateShortenUrl.id };
}

async function getByToken(token) {
  const data = await executeQuery(shortenUrlsByShortenId, { shortenId: token });
  if (!data.shortenUrlsByShortenId || !data.shortenUrlsByShortenId.items || !data.shortenUrlsByShortenId.items.length) return null;
  
  const shortenUrls = data.shortenUrlsByShortenId.items;
  if(shortenUrls?.length > 1) throw new Error('Non unique token');

  const uniqueShortenUrl = shortenUrls[0];
  return {
    id: uniqueShortenUrl.id,
    urlToken: uniqueShortenUrl.urlToken,
    dailyLog: uniqueShortenUrl.dailyLog,
  }
}

module.exports = {
  create,
  update,
  getByToken,
};
