const { getVehicle } = require("../graphql/queries");
const { executeQuery } = require("../helpers/appsync-helpers");

async function getById(vehicleId) {
  const data = await executeQuery(getVehicle, { id: vehicleId });
  const vehicle = data.getVehicle;

  return {
    id: vehicle.id,
    name: vehicle.name,
  };
}

module.exports = {
  getById,
};
