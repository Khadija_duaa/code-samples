const { getTenant } = require("../graphql/queries");
const { executeQuery } = require("../helpers/appsync-helpers");

async function getById(tenantId) {
  const data = await executeQuery(getTenant, { id: tenantId });
  const tenant = data.getTenant;

  return {
    logo: tenant.logo,
    group: tenant.group,
    timeZone: tenant.timeZone,
    companyName: tenant.companyName,
    allowLibraryUpload: tenant.allowLibraryUpload
  };
}

module.exports = {
  getById,
};
