const { createDailyLog } = require("../graphql/mutations");
const { dailyLogsByVehicleAndDate } = require("../graphql/queries")
const { executeMutation, executeQuery } = require("../helpers/appsync-helpers");

async function create(payload) {
  const input = {
    type: 'Vehicle Photo Log',
    date: payload.date,
    group: payload.group,
    vehicleId: payload.vehicleId,
    dailyLogVehicleId: payload.vehicleId,
    creationLinkSentDateTime: payload.dateCreateLink,
    dailyLogCreationLinkSentByUserId: payload.sentByUserId,
    dailyLogCreationLinkAssociateId: payload.associateId,
    dailyLogRosteredDayId: payload.rosteredDayId,
  };
  const data = await executeMutation(createDailyLog, {input});
  return { id: data.createDailyLog.id };
}

async function getMostRecentDailyLog(roster) {

  const input = {
    vehicleId: roster.vehicleId,
    date: {
      lt: roster.dateCreateLink,
    },
    limit: 1,
    sortDirection: 'DESC',
  };

  const data = await executeQuery(dailyLogsByVehicleAndDate, input);
  if (!data.dailyLogsByVehicleAndDate || !data.dailyLogsByVehicleAndDate.items || !data.dailyLogsByVehicleAndDate.items.length) return null;
  
  const mostRecentDailyLogs = data.dailyLogsByVehicleAndDate.items;
  const mostRecentDailyLog = mostRecentDailyLogs[0];
  if (!mostRecentDailyLog) return;

  return {
    mostRecentDailyLog
  }
}


module.exports = {
  create,
  getMostRecentDailyLog
};
