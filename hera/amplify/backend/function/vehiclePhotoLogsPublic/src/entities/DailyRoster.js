const { getDailyRoster } = require("../graphql/queries");
const { executeQuery } = require("../helpers/appsync-helpers");

async function getById(rosterId) {
  const data = await executeQuery(getDailyRoster, { id: rosterId });
  const dailyRoster = data.getDailyRoster;

  return {
    notesDate: dailyRoster.notesDate,
  };
}

module.exports = {
  getById,
};
