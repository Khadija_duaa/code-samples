const { createDocument } = require("../graphql/mutations");
const { executeMutation } = require("../helpers/appsync-helpers");

async function create(dailyLogId, documentPayload) {
  const input = {
    group: documentPayload.group,
    name: documentPayload.name,
    type: documentPayload.type,
    key: documentPayload.key,
    documentDate: documentPayload.documentDate,
    uploadDate: documentPayload.uploadDate,
    documentDailyLogId: dailyLogId,
    documentTakenByAssociateId: documentPayload.takenByAssociateId,
  }

  const data = await executeMutation(createDocument, {input});
  const document = data.createDocument;

  return {
    key: document.key,
    type: document.type,
    name: document.name,
  };
}

module.exports = {
  create,
};
