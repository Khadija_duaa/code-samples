const { getStaff } = require("../graphql/queries");
const { executeQuery } = require("../helpers/appsync-helpers");

async function getById(associateId) {
  const data = await executeQuery(getStaff, { id: associateId });
  const associate = data.getStaff;

  return {
    firstName: associate.firstName,
    lastName: associate.lastName,
    id: associate.id
  };
}

module.exports = {
  getById,
};
