const AWS = require('aws-sdk');

/**
 * Creates a pre-signed url to authorize actions in S3 butcket objects for external users
 * @param {String} action -> Action to sign the generated url with
 * @param {String} key -> Object key to which the generated url will authorize permissions
 * @param {Object} config -> Additional configuration taht params may need
 * @returns {String} Presigned url with the specified permissions
 */
function generatePreSignedUrl(action, key, config) {
  const s3 = new AWS.S3({ signatureVersion: 'v4'});

  let params = {
    Bucket: process.env.STORAGE_HERA_BUCKETNAME,
    Key: 'public/' + key,
    Expires: 60 * 5, // 5mins
  };

  if (config) {
    params = {...params, ...config};
  }

  return s3.getSignedUrl(action, params);
}

/**
 * Generates a pre-signed url with GET permissions to S3 for external users
 * @param {String} key -> S3 key to locate the file
 * @returns {String} Presigned url with GET permissions
 */
function getPreSignedUrlForRead(key) {
  return generatePreSignedUrl('getObject', key);
}

/**
 * Generates a pre-signed url with PUT permissions to S3 for external users
 * @param {String} key -> S3 key to store the file
 * @param {String} contentType -> Content-Type value to sign the url
 * @returns {String} Presigned url with PUT permissions
 */
function getPreSignedUrlForUpload(key, contentType) {
  return generatePreSignedUrl('putObject', key, {
    ContentType: contentType,
  });
}

module.exports = {
  getPreSignedUrlForRead,
  getPreSignedUrlForUpload,
};
