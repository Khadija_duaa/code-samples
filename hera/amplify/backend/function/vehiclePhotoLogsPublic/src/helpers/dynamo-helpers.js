const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient();

function getDynamoRecord(params){
  return new Promise((resolve, reject) => {    
    ddb.get(params, function(err, data) {
      if (err) {
        console.error("Unable to Get Item. Error JSON:", JSON.stringify(err, null, 2));
        reject(err);
      } else {
        console.log("Get Item succeeded:", JSON.stringify(data, null, 2));
        resolve(data);
      }
    });
  });
}

function scanDynamoRecords(params) {
  return new Promise((resolve, reject) => {
    ddb.scan(params, function(err, data) {
      if (err) {
        console.error("Unable Scan Items. Error JSON:", JSON.stringify(err, null, 2));
        reject(err);
      } else {
        console.log("Scan Items succeeded:", JSON.stringify(data, null, 2));
        resolve(data);
      }
    });
  });
}

function queryDynamoRecords(params) {
  return new Promise((resolve, reject) => {
    ddb.query(params, function(err, data) {
      if (err) {
        console.error("Unable to Query Items. Error JSON:", JSON.stringify(err, null, 2));
        reject(err);
      } else {
        console.log("Query Items succeeded:", JSON.stringify(data, null, 2));
        resolve(data);
      }
    });
  });
}

function createDynamoRecord(params) {
  return new Promise((resolve, reject) => {
    ddb.put(params, function(err, data) {
      if (err) {
        console.error("Unable to Create Item. Error JSON:", JSON.stringify(err, null, 2));
        reject(err);
      } else {
        console.log("Create Item succeeded:", JSON.stringify(data, null, 2));
        resolve(data);
      }
    });
  });
}

function updateDynamoRecord(params) {
  return new Promise((resolve, reject) => {
    ddb.update(params, function(err, data) {
      if (err) {
        console.log("Unable to Update Item. Error JSON:", JSON.stringify(err, null, 2));
        reject(err);
      } else {
        console.log("Update Item succeeded:", JSON.stringify(data, null, 2));
        resolve(data);
      }
    });
  });
}

function deleteDynamoRecord(params) {
  return new Promise((resolve, reject) => {
    ddb.delete(params, function(err, data) {
      if (err) {
        console.log("Unable to Delete Item. Error JSON:", JSON.stringify(err, null, 2));
        reject(err);
      } else {
        console.log("Delete Item succeeded:", JSON.stringify(data, null, 2));
        resolve(data);
      }
    });
  });
}

module.exports = {
  getDynamoRecord,
  scanDynamoRecords,
  queryDynamoRecords,
  createDynamoRecord,
  updateDynamoRecord,
  deleteDynamoRecord,
};
