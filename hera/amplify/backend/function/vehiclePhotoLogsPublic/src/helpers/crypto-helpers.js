const crypto = require('crypto');
const { getSecretValue } = require('/opt/nodejs/ssm');
let secretHashKey
async function loadSecretHashKey(){
  if(!secretHashKey){
    secretHashKey = await getSecretValue(process.env.SECRET_HASH_KEY);
  }
}
/**
 * Encrypts a string sequence passed as param
 * @param {String} text -> Value to be encrypted
 * @returns {Object} Encrypted payload as object
 */
async function encrypt(text) {
  const iv = crypto.randomBytes(16);
  await loadSecretHashKey();
  const key = Buffer.from(secretHashKey, 'hex');
  const cipher = crypto.createCipheriv('aes-256-cbc', key, iv);
 
  let encrypted = cipher.update(text);
  encrypted = Buffer.concat([encrypted, cipher.final()]);

  return {
    iv: iv.toString('hex'),
    body: encrypted.toString('hex'),
  };
}

/**
 * Decrypts a encrypted payload and returns its value
 * @param {String} body -> Encrypted data as encoded string in 'hex' format
 * @param {String} initV -> Initialization Vector as encoded string in 'hex' format
 * @returns {String} Decrypted data as string
 */
async function decrypt(body, initV) {
 const iv = Buffer.from(initV, 'hex');
 await loadSecretHashKey();
 const key = Buffer.from(secretHashKey, 'hex');
 const encryptedText = Buffer.from(body, 'hex');
 const decipher = crypto.createDecipheriv('aes-256-cbc', key, iv);

 let decrypted = decipher.update(encryptedText);
 decrypted = Buffer.concat([decrypted, decipher.final()]);

 return decrypted.toString();
}

module.exports= {
  encrypt,
  decrypt,
};
