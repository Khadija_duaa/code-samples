const Tenant = require('../entities/Tenant');
const DailyRoster = require('../entities/DailyRoster');
const Vehicle = require('../entities/Vehicle');
const Associate = require('../entities/Associate');
const { getPreSignedUrlForRead } = require('../helpers/s3-helpers');

module.exports.getErrorPayload = async function (validPayload) {
  const tenant = await Tenant.getById(validPayload.tenantId);
  const rosteredDay = await DailyRoster.getById(validPayload.rosteredDayId);
  const vehicle = await Vehicle.getById(validPayload.vehicleId);
  const associate = await Associate.getById(validPayload.associateId);

  return {
    tenant: {
      group: tenant.group,
      logo: getPreSignedUrlForRead(tenant.logo),
      companyName: tenant.companyName,
    },
    associate: {
      firstName: associate.firstName,
      lastName: associate.lastName,
    },
    vehicle: { name: vehicle.name },
    rosteredDay: { notesDate: rosteredDay.notesDate },
  };
}
