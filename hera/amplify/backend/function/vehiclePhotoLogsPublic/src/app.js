/*
Use the following code to retrieve configured secrets from SSM:

const aws = require('aws-sdk');

const { Parameters } = await (new aws.SSM())
  .getParameters({
    Names: ["SECRET_HASH_KEY","SECRET_TOKEN_KEY"].map(secretName => process.env[secretName]),
    WithDecryption: true,
  })
  .promise();

Parameters will be of the form { Name: 'secretName', Value: 'secretValue', ... }[]
*/
/* Amplify Params - DO NOT EDIT
	API_HERA_DAILYLOGTABLE_ARN
	API_HERA_DAILYLOGTABLE_NAME
	API_HERA_DAILYROSTERTABLE_ARN
	API_HERA_DAILYROSTERTABLE_NAME
	API_HERA_DOCUMENTTABLE_ARN
	API_HERA_DOCUMENTTABLE_NAME
	API_HERA_GRAPHQLAPIENDPOINTOUTPUT
	API_HERA_GRAPHQLAPIIDOUTPUT
	API_HERA_TENANTTABLE_ARN
	API_HERA_TENANTTABLE_NAME
	ENV
	REGION
	STORAGE_HERA_BUCKETNAME
Amplify Params - DO NOT EDIT *//* Amplify Params - DO NOT EDIT
	API_HERA_GRAPHQLAPIENDPOINTOUTPUT
	API_HERA_GRAPHQLAPIIDOUTPUT
	ENV
	REGION
	STORAGE_HERA_BUCKETNAME
Amplify Params - DO NOT EDIT */

const express = require('express');
const bodyParser = require('body-parser');
const awsServerlessExpressMiddleware = require('aws-serverless-express/middleware');
const {
  validateTodayIsRosterDay,
  validatePayloadForDocument,
  validateVehiclePhotoLogS3Key,
  validatePayloadForTokensCreation,
} = require('./middlewares/validator-middlewares');
const { validateToken, generateTokens } = require('./middlewares/jwt-middlewares');
const controllers = require('./controllers');

const app = express();
app.use(bodyParser.json());
app.use(awsServerlessExpressMiddleware.eventContext());

// Enable CORS for all methods
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "*");
  next();
});


/**
 * Creates a token to create/view/edit a Vehicle Photo Log record
 */
app.post('/vehicle-photo-log/generate-token',
  validatePayloadForTokensCreation,
  validateTodayIsRosterDay,
  generateTokens,
  controllers.getSignedTokens
);


/**
 * Gets the data from a specific daily log if exists
 */
app.get('/u/vehicle-photo-logs/:token',
  validateToken,
  validateTodayIsRosterDay,
  controllers.getDataByToken
);


/**
 * Creates a new document for a specific daily log
 */
app.post('/u/vehicle-photo-logs/:token',
  validateToken,
  validatePayloadForDocument,
  validateTodayIsRosterDay,
  controllers.createDocument
);


/**
 * Generates a pre-signed url to allow users upload files to S3
 */
app.post('/u/vehicle-photo-logs/:token/get-upload-url',
  validateToken,
  validateTodayIsRosterDay,
  validateVehiclePhotoLogS3Key,
  controllers.getUploadUrl
);


app.listen(3000, function() {
  console.log("App started!");
});


module.exports = app;
