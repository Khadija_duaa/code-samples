const DailyLog = require('../entities/DailyLog');
const Document = require('../entities/Document');
const ShortenUrl = require('../entities/ShortenUrl');
const { getPreSignedUrlForRead } = require('../helpers/s3-helpers');

module.exports = async function(req, res) {
  try {
    const shortenUrl = req.shortenUrl;
    if (!shortenUrl) throw new Error('Not Found');

    let dailyLog = shortenUrl.dailyLog;
    if (!dailyLog) {
      const payload = {
        ...req.validPayload,
        date: (new Date()).toISOString(), // datetime for 'NOW' in Tenant's timezone
        group: req.tenant.group,
      };
      dailyLog = await DailyLog.create(payload);
      await ShortenUrl.update(req.shortenUrl.id, { group: req.tenant.group, shortenUrlDailyLogId: dailyLog.id });
    }

    const documentPayload = req.validDocumentPayload;
    const document =  await Document.create(dailyLog.id, documentPayload);

    res.status(201).json({
      url: getPreSignedUrlForRead(document.key),
      type: document.type,
    });
  }
  catch (error) {
    console.log({req, error});
    res.sendStatus(500);
  }
};
