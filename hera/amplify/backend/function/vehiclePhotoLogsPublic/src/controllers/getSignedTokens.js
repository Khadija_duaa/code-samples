const ShortenUrl = require('../entities/ShortenUrl');
const DailyLog = require('../entities/DailyLog');

module.exports = async function(req, res) {
  const tenant = req.tenant;

  const validTokens = [];
  for (const signed of req.signedTokens) {
    try {

      const payloadCopyToSign = {
        ...signed.payloadToSign,
        date: signed.payloadToSign.sentDateTime,
        group: tenant.group
      }

      let shortenUrl = null

      if(payloadCopyToSign.reusedLink){
        
        const { mostRecentDailyLog } = await DailyLog.getMostRecentDailyLog(payloadCopyToSign);
        
        const dailyLogId = mostRecentDailyLog?.id
        
        if(dailyLogId){

          shortenUrl = await ShortenUrl.create(signed.token, { group:  tenant.group, shortenUrlDailyLogId: dailyLogId });

        }else{

          const dailyLog = await DailyLog.create(payloadCopyToSign);
        
          shortenUrl = await ShortenUrl.create(signed.token, { group:  tenant.group, shortenUrlDailyLogId: dailyLog?.id  });

        }
        

      }else{

        const dailyLog = await DailyLog.create(payloadCopyToSign);
        
        shortenUrl = await ShortenUrl.create(signed.token, { group:  tenant.group, shortenUrlDailyLogId: dailyLog?.id  });

      }

      validTokens.push({...signed, token: shortenUrl.shortenId});
    }
    catch (error) {
      console.log({error});
    }
  }

  res
    .status(200)
    .json(validTokens);
};
