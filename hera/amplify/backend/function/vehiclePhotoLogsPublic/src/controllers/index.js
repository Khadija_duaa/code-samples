const getDataByToken = require('./getDataByToken');
const getSignedTokens = require('./getSignedTokens');
const createDocument = require('./createDocument');
const getUploadUrl = require('./getUploadUrl');

module.exports = {
  getDataByToken,
  getSignedTokens,
  createDocument,
  getUploadUrl,
};
