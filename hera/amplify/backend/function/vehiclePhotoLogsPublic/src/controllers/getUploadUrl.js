const { getPreSignedUrlForUpload } = require('../helpers/s3-helpers');

module.exports = async function(req, res) {
  try {
    const s3Key = req.s3Payload.s3Key;
    const contentType = req.s3Payload.contentType;

    const uploadUrl = getPreSignedUrlForUpload(s3Key, contentType);
    res.json({ uploadUrl });
  }
  catch (error) {
    console.log({req, error});
    res.sendStatus(500);
  }
};
