const Vehicle = require('../entities/Vehicle');
const Associate = require('../entities/Associate');
const DailyLog = require('../entities/DailyLog');
const { getPreSignedUrlForRead } = require("../helpers/s3-helpers");

function createResponseBody(payload) {
  const {tenant, vehicle, associate, rosteredDay, dailyLog} = payload;

  return {
    tenant: {
      logo: getPreSignedUrlForRead(tenant.logo),
      group: tenant.group,
      timeZone: tenant.timeZone,
      companyName: tenant.companyName,
      allowLibraryUpload: tenant.allowLibraryUpload,
    },
    vehicle: {
      id: vehicle.id, // Needed in front end to require signedURL for s3Key
      name: vehicle.name,
    },
    associate: {
      firstName: associate.firstName,
      lastName: associate.lastName,
      id: associate.id
    },
    rosteredDay: {
      notesDate: rosteredDay.notesDate, // YYYY-MM-DD
    },
    documents: dailyLog.documents.items.map(item => ({
      url: getPreSignedUrlForRead(item.key),
      type: item.type,
    })),
  };
}

module.exports = async function(req, res) {
  try {
    const { tenant, rosteredDay } = req;
    const vehicle = await Vehicle.getById(req.validPayload.vehicleId);
    const associate = await Associate.getById(req.validPayload.associateId);
    // If the log does not exist means that still has no photos
    const shortenUrl = req.shortenUrl;
    const dailyLog = ((shortenUrl && shortenUrl.dailyLog) || {documents: {items: []}});

    res.status(200).json({
      body: createResponseBody({tenant, rosteredDay, dailyLog, vehicle, associate}),
    });
  } catch (error) {
    console.log({req, error});
    res.sendStatus(500);
  }
};
