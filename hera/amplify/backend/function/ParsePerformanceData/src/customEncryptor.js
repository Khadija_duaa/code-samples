const Crypto = require('crypto')

const validate = function(text, key = ""){
    if(typeof text !== 'string'){
        throw {message: `The entered text is not valid. The expected text is a string, found '${text}'.`, from: "customEncryptor"}
    }
    if(typeof key !== 'string'){
        throw {message: `The entered key is not valid, The expected key is a string, found '${key}'.`, from: "customEncryptor"}
    }
    const keyBuffer = Buffer.from(key, "utf-8").toString("hex").padStart(32,"F");
    const iv = Buffer.from(keyBuffer,"hex");
    const algorithm = 'aes-256-cbc'
    return {keyBuffer, iv, algorithm}
}

const encrypt = function(text, key){
    try {
        const {keyBuffer, iv, algorithm} = validate(text, key)
        const cipher = Crypto.createCipheriv(algorithm, keyBuffer, iv)
        let encrypted = cipher.update(text, 'utf-8', 'hex')
        encrypted += cipher.final('hex')
        return encrypted
    } catch (error) {
        throw {errors: [error]}
    }

}

const decrypt = function(text, key){
    try {
        const {keyBuffer, iv, algorithm} = validate(text, key)
        const decipher = Crypto.createDecipheriv(algorithm, keyBuffer, iv)
        let decrypted = decipher.update(text, 'hex', 'utf-8')
        decrypted += decipher.final('utf-8')
        return decrypted
    } catch (error) {
        throw {errors: [error]}
    }
}

module.exports = {encrypt, decrypt}