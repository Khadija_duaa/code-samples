// Load the AWS SDK for Node.js
const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION});
AWS.config.region = process.env.region;
const s3 = new AWS.S3();
const { encrypt, decrypt } = require('./customEncryptor')

const getS3FileName = function(s3Key){
    return s3Key.substr( s3Key.lastIndexOf('/') + 1)
}

const generateToken = function(s3Key, params){

    try {

        const obj = {
            k: s3Key,
            e: undefined
        }
    
        if(params && (params.expires || params.Expires)){
            const expires = (params.expires || params.Expires)
            if(expires instanceof Date){
                obj.e = expires.getTime()
            }else{
                const expiration = new Date()
                expiration.setSeconds(expiration.getSeconds() + parseInt(expires))
                obj.e = expiration - 0
            }
        }
    
        const textString = JSON.stringify(obj)
        return encrypt(textString, process.env.ENCRYPTION_KEY)

    } catch (error) {
        console.log({error})
        throw error
    }

}

const getShortLink = function(s3Key, params){
    const token = generateToken(s3Key, params)
    return process.env.HERA_URL + '/s3/' + token
}

const getFileByToken = async function(token, params){

    let decryptedObj;

    try {
        decryptedObj = decrypt(token, process.env.ENCRYPTION_KEY)
    } catch (error) {
        throw {errors: [{message: "Invalid token."}]}
    }

    const obj = JSON.parse(decryptedObj)
    let expirationDate;

    if(obj.e){
        expirationDate = new Date(obj.e)
        if(obj.e < new Date()){
            const message = `The token expired on ${expirationDate}`
            throw {
                token,
                expirationDate,
                errors: [{message}]
            }
        }
    }

    try {
        
        const s3Key = obj.k
        params = {
            Bucket: process.env.STORAGE_HERA_BUCKETNAME,
            Key: s3Key,
            ...params
        };
        const url = s3.getSignedUrl('getObject', params)
        
        return {
            s3Key,
            expirationDate,
            token,
            URL: url,
            fileName: getS3FileName(s3Key),
        }

    } catch (error) {
        throw {errors: [error]}
    }

}

module.exports = {getS3FileName, generateToken, getFileByToken, getShortLink}