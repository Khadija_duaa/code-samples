const alternativeText = {
    '+': '(((PLUS_KEY)))',
    '&': '(((AMPERSAND_KEY)))',
    '$': '(((DOLLAR_KEY)))',
    '@': '(((AT_KEY)))',
    '=': '(((EQUALS_KEY)))',
    ';': '(((SEMICOLON_KEY)))',
    ':': '(((COLON_KEY)))',
    '\\': '(((BACKSLASH_KEY)))',
    '/': '(((FORWARD_SLASH_KEY)))',
    '|': '(((PIPE_KEY)))',
    '?': '(((QUESTION_MARK_KEY)'
}

const replaceAll = function(text, from, to){
    let results = text
    while(results.includes(from)){
        results = results.replace(from, to)
    }
    return results
}

const format = function(fileName, encode){
    let replaces
    if(encode){
        replaces = Object.entries(alternativeText)
    }else{
        replaces = []
        for (const key in alternativeText) {
            replaces.push([[alternativeText[key]], key])
        }
    }
    let result = fileName
        .replace(/[\s]+/g, ' ')
    return replaces.reduce((text, [from, to])=>{
        return replaceAll(text, from, to)
    }, result)
}

const S3FileName = {
    encode: (fileName) => format(fileName, true),
    decode: (fileName) => format(fileName, false)
}

module.exports = { S3FileName }