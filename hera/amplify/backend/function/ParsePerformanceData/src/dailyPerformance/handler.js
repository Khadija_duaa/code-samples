const {
    queryDynamoRecords,
    updateDynamoRecord,
} = require('../dynamodbHelper.js')

const { executeMutation } = require('../appSync.js');
const { createNotification } = require('../graphql.js');
const { workBookFromBuffer } = require('../util.js')
const { S3FileName } = require('../s3FileNameHelper')
const { parseMentor, parseNetradyne, parseEOC } = require('./parseHelper')
// Load the AWS SDK for Node.js
const AWS = require('aws-sdk')
//Configure AWS SDK
AWS.config.region = process.env.region;
const s3 = new AWS.S3()

const dailyHandler = async function(record){

    console.log(JSON.stringify(record))

    let results, status, error, type
    const s3Key = record.key.S;

    try {
        // download file from S3 and write to tmp path
        const params = {
            Bucket: process.env.STORAGE_HERA_BUCKETNAME,
            Key: 'public/' + s3Key
        }
        const s3Stream = await s3.getObject(params).promise()
        const workbook = workBookFromBuffer(s3Stream.Body)

        if(record.type.S === "mentor"){
            type = "Mentor"
            results = parseMentor(workbook)
        }
        else if(record.type.S === "netradyne"){
            type = "Netradyne"
            results = parseNetradyne(workbook)
        }
        else if(record.type.S === "eoc"){
            type = "EOC"
            results = parseEOC(workbook)
        }
        
        if(results){
            status = "SUCCEEDED"
            results.alreadyUploaded = !s3Stream.Expires
        }
    } catch (e) {
        console.log(error = e)
    } finally {

        const updateParams = {
            TableName: process.env.API_HERA_TEXTRACTJOBTABLE_NAME,
            Key:{
                "id": record.id.S
            },
            UpdateExpression: "set #j = :j, #r = :r",
            ExpressionAttributeNames:{
                "#j": "jobStatus",
                "#r": "results"
            },
            ExpressionAttributeValues:{
                ":j": status,
                ":r": JSON.stringify(results)
            }
        }

        //Make unique filename to prevent using same file from tmp
        const filePathArray = s3Key.split("/");
        const originalName = record.fileName.S || S3FileName.decode(filePathArray[filePathArray.length -1])

        let input = {
            group: record.group.S,
            owner: record.owner.S,
        }

        if(status === 'SUCCEEDED'){
            input.title = "JOB COMPLETE"
            input.description ="Your import job is complete"
            input.payload = JSON.stringify({name: "DailyDataImport", params: {id: record.id.S, importId: record.jobId.S, jobStatus: status, results} })
            input.clickAction = 'dismiss'
            input.isRead = true
        }else{
            //EventBus.$emit('daily-job-fail', pendingFile)
            status = "ERROR"
            input.title = "Error importing your daily performance data"
            input.description = `There was an error importing your ${type} file named <span class='break-all'>"${originalName}"</span>. Please download a fresh file from Amazon, and do not edit the filename or alter the file before importing it.`
            input.payload = JSON.stringify({name: 'DailyDataImport', params: {importId: record.jobId.S, jobStatus: status, error: error}})
            input.clickAction = 'navigate'
            input.isRead = false
        }

        await updateDynamoRecord(updateParams)
        await executeMutation(createNotification, {input})

        // check if all other jobs in import are done and create notification 
        // using graphQL to trigger subscription

        const jobs = []

        const queryParams = {
            TableName: process.env.API_HERA_TEXTRACTJOBTABLE_NAME,
            ExpressionAttributeNames: {
                '#ji': 'jobId',
                '#js': 'jobStatus',
            },
            ExpressionAttributeValues: {
                ':ji': record.jobId.S
            },
            IndexName: 'byJobId',
            KeyConditionExpression: '#ji = :ji',
            ProjectionExpression: '#ji, #js',
        }

        do{
            const queryResult = await queryDynamoRecords(queryParams);
            queryParams.LastEvaluatedKey = queryResult.LastEvaluatedKey
            jobs.push(...queryResult.Items)
        }while(queryParams.LastEvaluatedKey)


        let jobsFiltered = jobs.filter(job => !['SUCCEEDED', 'ERROR'].includes(job.jobStatus))

        if(!jobsFiltered.length){
            try {
                // create Notification using GraphQL to trigger subscription
                var uploadCompleteInput = {
                    id: record.jobId.S+'_d',
                    group: record.group.S,
                    owner: record.owner.S,
                    title: "Daily performance upload complete and ready to be imported",
                    description:  "Your Daily performance import upload started " + new Date(parseInt(record.jobId.S.split('_')[1])).toLocaleString('en-US', {timeZone: record.timeZone.S}) + " is complete. Click here to continue the import.",
                    payload: JSON.stringify({name: 'DailyDataImport', params: {importId: record.jobId.S}}),
                    clickAction: 'navigate',
                    isRead: false
                }
                await executeMutation(createNotification, {input: uploadCompleteInput})
            } catch (error) {
                console.log('Notification already exists', error)
            }
        }

    }
}

module.exports = {
    dailyHandler
};