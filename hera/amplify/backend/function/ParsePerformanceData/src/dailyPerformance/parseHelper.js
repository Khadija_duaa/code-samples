const {
    sheetToJson,
    tableDataFrom_Header_A_algorithm,
    getRangeFromSheet,
    getColumnRange,
    getHyperlinkValue
} = require('../util')

function matchSetsFrom(header){
    return {
        target: (row) => ({
            matchValue: (keyPattern) => row[header.find(k=>keyPattern.test(k))]
        })
    }
}

function checkValues(row, requiredFields) {
    return !requiredFields.some(f=>{
        return !row[f] && !( ['boolean','number','string'].includes(typeof row[f]))
    })
}

function filterByDriverNameValid( dataAlerts ) {
    const prop = '__EMPTY_1';
    const values = 'Unknown Driver';
    const arrayModified = dataAlerts.filter( alert => alert[ prop ] !== values );
    return arrayModified;
}

const parseMentor = function(workbook){
    const sheetName = 'Sheet1'
    const csvSheet = sheetToJson(workbook.Sheets[sheetName], { header: "A", sheetStubs: true, defval: null})
    const csvTableData = tableDataFrom_Header_A_algorithm(csvSheet)
    const { header, body } = csvTableData
    const matchSets = matchSetsFrom(header)
    let associateLevel = body.map( row => {
        const rowOfMatches = matchSets.target(row)
        return {
            driverName: rowOfMatches.matchValue(/Driver[ ]*Name/gi),
            station: rowOfMatches.matchValue(/STATION/gi ),
            trips: rowOfMatches.matchValue(/^Trips$/gi ),
            miles: rowOfMatches.matchValue(/Miles/gi ),
            time: rowOfMatches.matchValue(/Time/gi ),
            fico: rowOfMatches.matchValue(/Fico/gi ),
            accel: rowOfMatches.matchValue(/Accel/gi ),
            braking: rowOfMatches.matchValue(/Braking/gi ),
            cornering: rowOfMatches.matchValue(/Cornering/gi ),
            speeding: rowOfMatches.matchValue(/Speeding/gi ),
            distraction: rowOfMatches.matchValue(/Distraction/gi ),
            seatbelt: rowOfMatches.matchValue(/Seatbelt/gi ),
            backUp: rowOfMatches.matchValue(/Back[ ]*Up/gi ),
            mpg: rowOfMatches.matchValue(/Mpg/gi ),
            idling: Math.floor( (rowOfMatches.matchValue(/Idling/gi) || 0) * 100)+'%',
            engineOff: rowOfMatches.matchValue(/Engine[ ]*Off/gi ),
            preDvcr: rowOfMatches.matchValue(/Pre[ ]*DVCR/gi ),
            postDvcr: rowOfMatches.matchValue(/Post[ ]*DVCR/gi ),
            trainingAssigned: rowOfMatches.matchValue(/Training[ ]*Assigned/gi ),
            trainingCompleted: rowOfMatches.matchValue(/Training[ ]*Completed/gi ),
            sse: rowOfMatches.matchValue(/Sse/gi ),
            geotabTrips: rowOfMatches.matchValue(/^Geotab[ ]*Trips$/gi )
        }
    })

    const requiredFields = ['driverName']

    associateLevel = associateLevel.filter(row => checkValues(row, requiredFields))

    const data = { associateLevel }
    return data
}

const parseNetradyne = function(workbook){

    const sheetName = workbook.Workbook.Sheets[0].name
    const sheet = workbook.Sheets[sheetName]
    
    // remove first three cells (Timestamp Generated, Fleet, Day of Report)
    delete sheet.A1
    delete sheet.A2
    delete sheet.A3

    const A5 = sheet.A5

    const alerts = !Object.keys(A5).some(key => A5[key].includes("No Alerts"))

    for (const key in sheet) {
      if (sheet.hasOwnProperty(key) && typeof sheet[key] === 'object' && 'f' in sheet[key]) {
        sheet[key]['v'] = getHyperlinkValue(sheet[key]);
      }
    }

    // convert to json array
    let jsonSheet = sheetToJson(sheet)
    jsonSheet = filterByDriverNameValid( jsonSheet );

    // verfiy headers
    const headers = jsonSheet.shift()
    const cellKeys = Object.keys(headers);
    let headersString = cellKeys.map((key)=>{
        return headers[key]
    })
    headersString = headersString.join("").toLowerCase().replace(/\s+/g, '')
    headersString = headersString.replace(/timestamp\(.*\)a/g, 'timestampa')
    
// console.log('headerString', headersString)
    
    const oldHeaders = 's.nodrivernamedriveridgroupnamevehiclenumberalertidtimestampalerttypealertseveritydescriptionalertvideostatusduration(sec)startlatlongendlatlong'
    const oldHeaders2 = 's.nodrivernamedriveridgroupvehiclenumberalertidtimestampalerttypealertseveritydescriptionalertvideostatusduration(sec)startlatlongendlatlong'
    const latestHeaders = 's.nodrivernamedriveridgroupvehiclenumbervinalertidtimestampalerttypealertseveritydescriptionalertvideostatusduration(sec)startlatlongendlatlong'

    const validHeaders = [
        oldHeaders,
        oldHeaders2,
        latestHeaders
    ]

    const matchedHeader = validHeaders.findIndex(item => item == headersString)

// console.log('matchedHeader', matchedHeader)

    // header not found
    if(matchedHeader < 0 && alerts){
        throw 'Invalid header columns for Netradyne Excel file.'
    }
    
    let associateLevel = jsonSheet.map(row=>{
        const obj = {}
        obj.driverName = row['__EMPTY_1']
        obj.driverId = row['__EMPTY_2']
        if(matchedHeader === 2){
            obj.groupName = row['__EMPTY_3']
            obj.vehicleNumber = row['__EMPTY_4']
            obj.alertId = row['__EMPTY_6']
            obj.timestamp = row['__EMPTY_7']
            obj.alertType = row['__EMPTY_8']
            obj.alertSeverity = row['__EMPTY_9']
            obj.description = row['__EMPTY_10']
            obj.alertVideoStatus = row['__EMPTY_11']
            obj.duration = row['__EMPTY_12']
            obj.startLatLong = row['__EMPTY_13']
            obj.endLatLong = row['__EMPTY_14']
        }else{
            obj.groupName = row['__EMPTY_3']
            obj.vehicleNumber = row['__EMPTY_4']
            obj.alertId = row['__EMPTY_5']
            obj.timestamp = row['__EMPTY_6']
            obj.alertType = row['__EMPTY_7']
            obj.alertSeverity = row['__EMPTY_8']
            obj.description = row['__EMPTY_9']
            obj.alertVideoStatus = row['__EMPTY_10']
            obj.duration = row['__EMPTY_11']
            obj.startLatLong = row['__EMPTY_12']
            obj.endLatLong = row['__EMPTY_13']
        }
        return obj
    })
    
    const requiredFields = ['driverName', 'vehicleNumber', 'alertId', 'timestamp', 'alertType', 'alertSeverity']

    associateLevel = associateLevel.filter(row => checkValues(row, requiredFields))

    const data = { associateLevel }
    return data
}

const parseEOC = function(workbook){
    
    const sheetName = workbook.Workbook.Sheets[0].name
    const sheet = workbook.Sheets[sheetName]

    const { firstColumn, firstRow, lastColumn, lastRow } = getRangeFromSheet(sheet) // "A1:I280"

    const columnRange = getColumnRange(firstColumn, lastColumn)

    let dspLevelFirstColumn, associateFirstColumn
    let dspLevelRowIndex, associateLevelRowIndex

    rowLoop:
    for (let rowIndex = firstRow; rowIndex < lastRow; ++rowIndex) {

        columnLoop:
        for (const colLetter of columnRange) {
            const currentColumn = colLetter + rowIndex
            const cell = sheet[currentColumn]
            if(!cell){
                continue
            }
    
            if(/dsp_shortcode/gi.test(cell.v)){
                dspLevelFirstColumn = colLetter
                dspLevelRowIndex = rowIndex
                break columnLoop
            }
    
            else if(/transporter_id/gi.test(cell.v)){
                associateFirstColumn = colLetter
                associateLevelRowIndex = rowIndex
                break columnLoop
            }
    
            if(typeof associateLevelRowIndex === 'number'){
                break rowLoop
            }
        }

    }

    const dspRange = dspLevelFirstColumn + dspLevelRowIndex + ":" + lastColumn + (associateLevelRowIndex - 1) // "A??:I280"
    const associateRange = associateFirstColumn + associateLevelRowIndex + ":" + lastColumn + lastRow // "A???:I280"

    const dspJsonSheet = sheetToJson(sheet, { header: "A", sheetStubs: true, defval: null, range: dspRange})
    const associateJsonSheet = sheetToJson(sheet, { header: "A", sheetStubs: true, defval: null, range: associateRange})

    const dspLevel = tableDataFrom_Header_A_algorithm(dspJsonSheet).body

    const associateLevel = tableDataFrom_Header_A_algorithm(associateJsonSheet).body

    // Remove 'Grand Total' row from associateLevel table
    let grandTotalIndex = associateLevel.findIndex(row => /Grand[-|_|\s]?Total/gi.test(row['transporter_id']))
    if(grandTotalIndex > -1){
        let grandTotal = associateLevel.splice(grandTotalIndex, 1)
    }

    const data = {dspLevel, associateLevel}
    return data
}

module.exports = { parseMentor, parseNetradyne, parseEOC }