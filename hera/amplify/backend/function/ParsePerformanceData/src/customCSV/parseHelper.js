const KEY_ASSOCIATE_NAME = 'Associate Name'
const KEY_DSP_SHORT_CODE = 'DSP Short Code'
const KEY_TRANSPORTER_ID = 'Associate Transporter ID'
const { sheetToJson, tableDataFrom_Header_A_algorithm } = require('../util')
const parseCustomCSV = function(workbook){
    let sheetName;
    if(workbook.Workbook && workbook.Workbook.Sheets && workbook.Workbook.Sheets.length){
        const visibleSheet = workbook.Workbook.Sheets.find(sheet => !sheet.Hidden)
        if(visibleSheet){
            sheetName = visibleSheet.name
        }
    }
    if(!sheetName){
        sheetName = 'Sheet1'
    }
    const csvSheet = sheetToJson(workbook.Sheets[sheetName], { header: "A", sheetStubs: true, defval: null})

    let columns = {}
    let finalCsvSheet = csvSheet.filter(row => {
        let res = false
        Object.entries(row).forEach(([key, value]) => {
            if(value){
                columns[key] = null
                res = true
            }
        })
        return res
    })
    columns = Object.keys(columns)
    finalCsvSheet = finalCsvSheet.map(row=>{
        const obj = {}
        columns.forEach(c => {
            if(typeof row[c] === 'string'){
                obj[c] = row[c].trim()
            }else{
                obj[c] = row[c]
            }
        })
        return obj
    })

    const associateLevel = tableDataFrom_Header_A_algorithm(finalCsvSheet)

    const keyColumnNames = [KEY_ASSOCIATE_NAME, KEY_DSP_SHORT_CODE, KEY_TRANSPORTER_ID]

    const add = []
    const remove = []
    associateLevel.body.forEach(row => {
        associateLevel.header.forEach(column => {
            const cell = String(row[column]) || ''

            if(cell.includes("/") && cell.match(/([A-Za-z ]{4,})\/([A-Za-z0-9 ]{3,})\/([ ]*[A-Z][A-Z0-9]{10,})/g)){

                const staffInfo = cell.split("/")

                const [ associateName, shortCode, transporterId ] = staffInfo
                associateName && !row[KEY_ASSOCIATE_NAME] && (row[KEY_ASSOCIATE_NAME] = associateName.trim())
                shortCode && !row[KEY_DSP_SHORT_CODE] && (row[KEY_DSP_SHORT_CODE] = shortCode.trim())
                transporterId && !row[KEY_TRANSPORTER_ID] && (row[KEY_TRANSPORTER_ID] = transporterId.trim())

                keyColumnNames.forEach(key=>{
                    if(!add.includes(key)){
                        add.push(key)
                    }
                })

                if(!remove.includes(column)){
                    remove.push(column)
                }
                delete row[column]
            }
            else if(/^DSP\:[A-Z0-9]{3,}$/g.test(cell)){
                const { groups: { shortCode } } = /^DSP\:(?<shortCode>[A-Z0-9]{3,})$/g.exec(cell) || { groups: 0 }
                if(shortCode){
                    !row[KEY_DSP_SHORT_CODE] && (row[KEY_DSP_SHORT_CODE] = shortCode.trim())
                    if(!associateLevel.header.includes(KEY_DSP_SHORT_CODE)){
                        associateLevel.header.push(KEY_DSP_SHORT_CODE)
                    }
                    // if(!remove.includes(column)){
                    //     remove.push(column)
                    // }
                    // delete row[column]
                }
            }
        })
    })
    remove.forEach(r => {
        const colIndex = associateLevel.header.findIndex(h => h === r)
        if(colIndex > -1){
            associateLevel.header.splice(colIndex, 1)
        }
    })
    associateLevel.header.push(...add)
    
    const data = { associateLevel }
    return data
}

module.exports = { parseCustomCSV }