const {
    queryDynamoRecords,
} = require('../dynamodbHelper.js')

async function getStaffsByGroup(group) {
    try {
        let staffs = [];
        let lastEvaluatedKey = null;
        do {
            let params = {
                ExpressionAttributeNames: {
                    '#group': 'group',
                    '#status': 'status',
                },
                ExpressionAttributeValues: {
                    ':group': group,
                    ':active': 'Active',
                },
                TableName: process.env.API_HERA_STAFFTABLE_NAME,
                IndexName: 'byGroup',
                KeyConditionExpression: '#group = :group',
                ProjectionExpression: "id, #group, firstName, lastName, transporterId, #status",
                FilterExpression: "attribute_exists(transporterId) AND #status = :active",
                ExclusiveStartKey: lastEvaluatedKey,
            };
            let scanResult = await queryDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            staffs = [...staffs, ...items];
        } while (lastEvaluatedKey);

        return staffs;
    } catch (err) {
        console.log("[getStaffsByGroup] Error in function getStaffsByGroup", err);
        return {
            error: err,
        };
    }
}

function getTransporterIdByName(staffs, name) {
    let nameSplit, firstName, lastName
    let daMatch = { id: null }
    var transporterId = null

    nameSplit = name.split(" ")
    firstName = nameSplit[0].toLowerCase()
    lastName = nameSplit[nameSplit.length - 1].toLowerCase()

    daMatch = Object.values(staffs).find(da => {
        return (cleanInput(da.firstName) == cleanInput(firstName)) &&
            (cleanInput(da.lastName) == cleanInput(lastName))
    })

    if (daMatch) {
        transporterId = daMatch.transporterId
    }

    return transporterId

}

function cleanInput(str) {
    if (!str) {
        return null
    }
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();

    // remove accents, swap ñ for n, etc
    const from = 'àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõőṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;'
    const to = 'aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnoooooooooprrsssssttuuuuuuuuuwxyyzzz------'
    for (var i = 0, l = from.length; i < l; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 - #]/g, '') // remove invalid chars
        .replace(/\s+/g, '_') // collapse whitespace and replace by _
        .replace(/-+/g, '_'); // collapse dashes

    return str;
}

module.exports = {
    getStaffsByGroup,
    getTransporterIdByName,
}