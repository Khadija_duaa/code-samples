const { CWMetrics } = require('../cloudWatchMetricHelper.js')
const { pushToCloudWatch } = require('../cloudWatchLogger.js')
const { executeMutation } = require('../appSync.js');
const { createNotification } = require('../graphql.js')


async function failedValidations(job, error, metadataErrorText, originalName, tenantName, shortPdfURL, errorsCategories, errorTransporterId){

    const cwm = new CWMetrics(process.env.METRIC_GROUP_NAME)
    const metricDimension = process.env.ENV
    
    let input = null
    const typeFile = {
        'scorecard' : 'Scorecard',
        'customerFeedback': 'CX Feedback',
        'podQualityFeedback' : 'POD'
    }
    let file = typeFile[job.Item.type] || ''
    let hasMetric = false
    const jobId = job.Item.jobId
    const owner = job.Item.owner
    const group = job.Item.group
    const keyJob = job.Item.key
    let loggerBody = {
        'Title': `Fatal error importing week ${job.Item.week}-${job.Item.year} ${file} for ${tenantName}.`,
        'Tenant': tenantName,
        'Group': group,
        'PDF': shortPdfURL,
        'Year': job.Item.year,
        'Week': job.Item.week,
        'Severity': 'Fatal',
        'Reason': `There was an error importing your ${file} file named ${originalName}.`
    }
    
    if(error === "ERROR_METADATA"){          
        hasMetric = true
        loggerBody['ErrorCode'] = 'ERROR_001_METADATA'
        loggerBody['Description'] = `The file ${originalName} was created with a different ${metadataErrorText} than you selected.`
        loggerBody['Error'] = error
        
        input = {
          group: group,
          owner: owner,
          title: "Warning (001)",
          description: `The file <span class='break-all'>"${originalName}"</span> was created with a different ${metadataErrorText} than you selected.`,
          payload: JSON.stringify({name: 'WeeklyDataImport', params: {importId: jobId, key: keyJob, error: error}}),
          clickAction: 'navigate',
          isRead: false
        }
    }else if(error === "ERROR_TRANSPORTERID"){

        hasMetric = true
        loggerBody['ErrorCode'] = 'ERROR_001_TRANSPORTERID'
        loggerBody['Reason'] = `There was an error importing your ${file} file named ${originalName}. We were unable to properly import one or more of the Transporter IDs in this file. The Hera support team has been notified and they will be in touch as soon as possible with a solution.`
        loggerBody['Description'] = `transporterId: ${errorTransporterId}`
        loggerBody['Error'] = error

        input = {
          group: group,
          owner: owner,
          title: "Error importing your weekly performance data (002)",
          description: `There was an error importing your ${file} file named <span class='break-all'>"${originalName}"</span>. We were unable to properly import one or more of the Transporter IDs in this file. The Hera support team has been notified and they will be in touch as soon as possible with a solution.`,
          payload: JSON.stringify({name: 'WeeklyDataImport', params: {importId: jobId, key: keyJob, error: error}}),
          clickAction: 'navigate',
          isRead: false
        }
    }else if(error === "ERROR_DSP_LEVEL"){

        hasMetric = true
        loggerBody['ErrorCode'] = 'ERROR_DSP_LEVEL'
        loggerBody['Reason'] = `There was an error importing your Scorecard file named ${originalName}. We were unable to properly import one or more of the DSP-level metrics in this file. The Hera support team has been notified and they will be in touch as soon as possible with a solution.`
        loggerBody['Description'] = errorsCategories
        loggerBody['Error'] = error

        input = {
          group: group,
          owner: owner,
          title: "Error importing your weekly performance data (002)",
          description: `There was an error importing your Scorecard file named <span class='break-all'>"${originalName}"</span>. We were unable to properly import one or more of the DSP-level metrics in this file. The Hera support team has been notified and they will be in touch as soon as possible with a solution.`,
          payload: JSON.stringify({name: 'WeeklyDataImport', params: {importId: jobId, key: keyJob, error: error}}),
          clickAction: 'navigate',
          isRead: false
        }
    }else if(error === "ERROR_002_SCORECARD_VALIDATOR_007 (002.007)"){
        
        hasMetric = true
        loggerBody['ErrorCode'] = 'ERROR_002_SCORECARD_VALIDATOR_007 (002.007)'
        loggerBody['Reason'] = `Error importing your weekly performance data (002).`
        loggerBody['Description'] = `There was an error importing your ${file} file named ${originalName}. This file cannot be imported because it has been edited. Please download a new file and do not edit the file name, delete any pages or edit the file in any way before uploading it to Hera.`
        loggerBody['Error'] = error

        input = {
          group: group,
          owner: owner,
          title: "Error importing your weekly performance data (002)",
          description: `There was an error importing your ${file} file named <span class='break-all'>"${originalName}"</span>. This file cannot be imported because it has been edited. Please download a new file and do not edit the file name, delete any pages or edit the file in any way before uploading it to Hera.`,
          payload: JSON.stringify({name: 'WeeklyDataImport', params: {importId: jobId, key: keyJob, error: error}}),
          clickAction: 'navigate',
          isRead: false
        }
    }else if(error.validatorList){
        const loggerBody = {
            'Title': `Scorecard for ${originalName} week ${job.Item.week}-${job.Item.year}.`,
            'Tenant': tenantName,
            'Group': group,
            'PDF': shortPdfURL,
            'Year': job.Item.year,
            'Week': job.Item.week,
            'Severity': 'Fatal',
            'Reason': 'Failed validation.'
        }
        let ERROR_002_SCORECARD_VALIDATOR_001_OF_005 = false
        let ERROR_002_SCORECARD_VALIDATOR_001_OF_005_TEXT = new Set()
        let ERROR_002_SCORECARD_VALIDATOR_006 = false
        for(const item of error.validatorList){
            for(const errorCode of item.failedValidatorList){
                if(errorCode == 'ERROR_002_SCORECARD_VALIDATOR_006 (002.006)'){
                    ERROR_002_SCORECARD_VALIDATOR_006 = true
                }else{
                    ERROR_002_SCORECARD_VALIDATOR_001_OF_005 = true
                    ERROR_002_SCORECARD_VALIDATOR_001_OF_005_TEXT.add(errorCode)
                }
                cwm.scorecard.metricGroup1({
                    byError: metricDimension
                }).pushMetricData({
                    MetricName: errorCode,
                    Value: 1,
                    Unit: 'Count'
                })
    
                cwm.scorecard.metricGroup2({
                    bySeverity: metricDimension
                }).pushMetricData({
                    MetricName: 'Fatal',
                    Value: 1,
                    Unit: 'Count'
                })
                loggerBody['Error'] = errorCode
                const body = {
                    ErrorCode: errorCode,
                    ...loggerBody,
                    'Description': {
                    'TransporterID': item.transporter_id,
                    'Failed rule': errorCode
                    }
                }
                pushToCloudWatch.error(body)
                await cwm.sendToCloudWatch()
            }
        }

        let input = {
            group: group,
            owner: owner,
            title: "Error importing your weekly performance data (002)",
            description: '',
            payload: '',
            clickAction: 'navigate',
            isRead: false
        }

        if(ERROR_002_SCORECARD_VALIDATOR_006){
            input.description = `There was an error importing your ${file} file named <span class='break-all'>"${originalName}"</span>. We were unable to properly import one or more of the Overall Tier metric in this file. The Hera support team has been notified and they will be in touch as soon as possible with a solution.`
            input.payload = JSON.stringify({
                name: 'WeeklyDataImport', 
                params: {
                    importId: jobId, 
                    key: keyJob, 
                    error: 'ERROR_002_SCORECARD_VALIDATOR_006 (002.006)'
                }
            })
          
            await createNotificationError(input)
        }
        if(ERROR_002_SCORECARD_VALIDATOR_001_OF_005){
            input.description = 'Please contact Hera Support so that we can help resolve this issue.' 
            input.payload = JSON.stringify({
                name: 'WeeklyDataImport', 
                params: {
                    importId: jobId, 
                    key: keyJob, 
                    error: [...ERROR_002_SCORECARD_VALIDATOR_001_OF_005_TEXT].join(", ") 
                }
            })
            await createNotificationError(input)
        }
       
        return null
    }else{
        hasMetric = true
        loggerBody['ErrorCode'] = 'ERROR_003'
        loggerBody['Reason'] = `Error importing your weekly performance data (003).`
        loggerBody['Description'] = `There was an error importing your ${file} file named ${originalName}. Please download a fresh file from Amazon, and do not edit the filename or alter the file before importing it.`
        loggerBody['Error'] = error

        input = {
          group: group,
          owner: owner,
          title: "Error importing your weekly performance data (003)",
          description: `There was an error importing your ${file} file named <span class='break-all'>"${originalName}"</span>. Please download a fresh file from Amazon, and do not edit the filename or alter the file before importing it.`,
          payload: JSON.stringify({name: 'WeeklyDataImport', params: {importId: jobId, key: keyJob, error: error}}),
          clickAction: 'navigate',
          isRead: false
        }
    }

    pushToCloudWatch.error(loggerBody)

    if(hasMetric){
        cwm.scorecard.metricGroup1({
            byError: metricDimension
        }).pushMetricData({
            MetricName: loggerBody['ErrorCode'],
            Value: 1,
            Unit: 'Count'
        })

        cwm.scorecard.metricGroup2({
            bySeverity: metricDimension
        }).pushMetricData({
            MetricName: 'Fatal',
            Value: 1,
            Unit: 'Count'
        })
        await cwm.sendToCloudWatch()
    }
    return input
}


async function createNotificationError(input){
    try{
        await executeMutation(createNotification, {input})
    }catch(e){
        console.log('Error in create notifications',e)
    }
}

module.exports = { failedValidations }