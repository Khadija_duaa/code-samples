/* Amplify Params - DO NOT EDIT
	API_HERA_GRAPHQLAPIENDPOINTOUTPUT
	API_HERA_GRAPHQLAPIIDOUTPUT
	API_HERA_STAFFTABLE_ARN
	API_HERA_STAFFTABLE_NAME
	API_HERA_TENANTTABLE_ARN
	API_HERA_TENANTTABLE_NAME
	API_HERA_TEXTRACTJOBTABLE_ARN
	API_HERA_TEXTRACTJOBTABLE_NAME
	ENV
	REGION
	STORAGE_HERA_BUCKETNAME
Amplify Params - DO NOT EDIT */
var fs = require('fs')
var pdf = require('pdf-parse');

// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION});
const ddb = new AWS.DynamoDB.DocumentClient({apiVersion: '2012-08-10'});


const {
  executeMutation
} = require('./appSync.js');
const {
  buildQueryParams,
  queryDynamoRecords,
  updateDynamoRecord,
  getDynamoRecord
} = require('./dynamodbHelper.js');

const {
  createNotification
} = require('./graphql.js');

const {
  scoreCardValidatorsFailedList,
  pagesCounterValidator,
  newScorecardValidator
} = require('./scorecardValidator.js');

const {checkTransporterId} = require('./validate.js');

const {checkDPS} = require('./scorecardDpsLevel.js')

const { getShortLink } = require('./s3linkHelper')

const { CWMetrics } = require('./cloudWatchMetricHelper')
const { pushToCloudWatch } = require('./cloudWatchLogger')

const { S3FileName } = require('./s3FileNameHelper')

const { customCsvHandler } = require('./customCSV/handler')
const { dailyHandler } = require('./dailyPerformance/handler')
const { failedValidations } = require('./weeklyPerformance/validationErrors')

/*global text*/

exports.handler = async function(event, context, callback) {  
  context.callbackWaitsForEmptyEventLoop = true
  try {
    // await Promise.all(event.Records.map( async (dynoRecord) => {
    for (const dynoRecord of event.Records){
        const record = dynoRecord.dynamodb.NewImage;

        const createdAt = new Date(record.createdAt.S)
        const now = new Date()
        const max = 10 * 1000 * 60 // 10 min
        if(now - createdAt >= max){
          continue
        }
        
        let owner = record.owner.S
        if( dynoRecord.eventName == "INSERT"){
          // console.log(dynoRecord.eventName)
          const type = record.type.S
          switch(type){
              case 'customCsv':
                await customCsvHandler(record)
                break
              case 'mentor': case 'netradyne': case 'eoc':
                await dailyHandler(record)
                break
              case 'scorecard': case 'podQualityFeedback': case 'customerFeedback':
                await startJob(record)
                break
          }
        }

    }
    // }))
    
    // context.done(null, 'Successfully processed DynamoDB record'); 
    callback(null, 'Successfully processed DynamoDB record')
  } catch(e) {
    console.log('Job has an unrecoverable error', e)
    callback(null, 'Failed to processed DynamoDB record')
  }
};

async function startJob(record){
  const cwm = new CWMetrics(process.env.METRIC_GROUP_NAME)
  let scorecardError = false
  let customerFeedbackError = false
  let PODFeedbackError = false
  let { scorecardTemplates } = require('./scorecardTemplates')
  let { podTemplates } = require('./podTemplates')
  let { cxTemplates } = require('./cxTemplates')
  // make copies of the templates because code eventually mutates it and don't want the mutations to persists across invocations
  let copiedCxTemplate = JSON.parse(JSON.stringify(cxTemplates))
  let copiedPodTemplate = JSON.parse(JSON.stringify(podTemplates))
  let copiedScorecardTemplate = JSON.parse(JSON.stringify(scorecardTemplates))
  
  let templates = {
    scorecard: copiedScorecardTemplate,
    podQualityFeedback: copiedPodTemplate,
    customerFeedback: copiedCxTemplate
  }
  let { parseFields } = require('./parseFields')
  let { parseMetrics } = require('./parseMetrics')
  let { parseTables } = require('./parseTables')
  
  const renderParams = {
    blocks: [],
    page: 1
  }

  let id = record.id.S;
  
  
  //Get job data since relying on the streamed data seems spotty
  let getParams = {
    TableName : process.env.API_HERA_TEXTRACTJOBTABLE_NAME,
    Key: {
      "id": id
    }
  };
  
  let job = await getDynamoRecord(getParams)

  let filterParmas = {
    TableName: process.env.API_HERA_TENANTTABLE_NAME,
    Key: {
      "id": job.Item.textractJobTenantId
    }
  }

  let filterByJob = await getDynamoRecord(filterParmas)
  let tenantName = filterByJob.Item.companyName

  let userDefinedWeek = job.Item.week
  let userDefinedYear = job.Item.year
  let templateType = job.Item.type
  let usesXLCoaching = job.Item.tenantUsesXL
  let jobId = job.Item.jobId
  let owner = job.Item.owner
  let group = job.Item.group
  let timeZone = job.Item.timeZone
  let template = {...templates[templateType]}

  var keyJob = job.Item.key
  var fileYear
  var fileWeek           
  var fileMetaDataYear  = job.Item.metadataYear || ''
  var fileMetaDataWeek  = job.Item.metadataWeek || ''
  let errorsCategories = null
  let errorTransporterId = null
  let metricDimension = process.env.ENV
  if(record.key && jobId){

    
    let key = record.key.S;
    //Configure AWS SDK
    AWS.config.region = process.env.region;
    var s3 = new AWS.S3();
    
    let results = ''
    let metadataErrorText = 'week and year'
    let validatorList = null

    try{
      // download file from S3 and write to tmp path
      var params = {
        Bucket: process.env.STORAGE_HERA_BUCKETNAME,
        Key: 'public/' + key
      };
        
      //Make unique filename to prevent using same file from tmp
      var filePathArray = key.split("/");
      var originalName = record.fileName.S || S3FileName.decode(filePathArray[filePathArray.length -1])
      var fileName = new Date().getTime().toString() + originalName
      // console.log({fileName: fileName})

      let compareName = originalName.toLowerCase()
      const compareTypeValues = {
        'scorecard': 'scorecard',
        'customerFeedback': 'customer_feedback',
        'podQualityFeedback': '-pod-'
      }
      let compareType = compareTypeValues[templateType]
      
      let useEngTemplate = false
      if(compareName.includes('_en_')) {
        useEngTemplate = true
      }
        
      if(compareName.indexOf(compareType) === -1){
          throw "Error importing your weekly performance data"
      }
      
      var s3Stream = await s3.getObject(params).promise()
      var shortPdfURL = getShortLink(key)
      // console.log({contentLength: s3Stream.ContentLength})
      fs.writeFileSync('/tmp/' + fileName, s3Stream.Body, async(err) => {
        if (err) throw err;
        // console.log('The file has been saved!');
      });
    
      let options = {
          pagerender: render_page(renderParams)
      }
      
      let dataBuffer = fs.readFileSync('/tmp/' + fileName);

      let fieldsMetrics = {}
      if (useEngTemplate) {
        await pdf(dataBuffer, {}).then(async function (data) {
          fieldsMetrics = await parseMetrics(data)
        }).catch(error => {
          throw error
        })
      }

      await pdf(dataBuffer,options).then(async function(data) {
          //blocks is where the render function saves data

          //Remove file to prevent other sessions using the file
          await fs.unlink('/tmp/' + fileName, (err) => {
            if (err) throw err;
            // console.log('/tmp/' + fileName + ' was removed');
          });
          
          
          //---------GET VERSION INFO FROM PDF-----------
          let versionData = await parseFields([...renderParams.blocks], [...template.versionLookup])
          // console.log(JSON.parse(JSON.stringify({versionData: versionData})))

          //for scorecard >= 2022 44
          if(useEngTemplate){
            versionData.week = fieldsMetrics.week
            versionData.year = fieldsMetrics.year
          } 

          //---------DETERMINE WEEK---------
          var week = versionData.week ? versionData.week : userDefinedWeek
          
          week = week.padStart(2,'0')
          fileMetaDataWeek = fileMetaDataWeek.padStart(2,'0')
          userDefinedWeek = userDefinedWeek.padStart(2,'0')
          fileWeek = week
                  
          //---------DETERMINE YEAR---------
          var year = userDefinedYear ? userDefinedYear : versionData.year
        
          if(year){
            fileYear = year
          }else{
            fileYear = fileMetaDataYear
          }

          var version = parseInt( year + week )
          var correctTemplate = template.versions.find(templateVersion => {
            if(usesXLCoaching == true && templateType == "scorecard"){
              if(useEngTemplate) {
                return version >= templateVersion.start && templateVersion.vehicleType == "XL" && templateVersion.useEng == 'true'
              }
              return version >= templateVersion.start && templateVersion.vehicleType == "XL"
            }
            else if(!usesXLCoaching && templateType == "scorecard"){
              if(useEngTemplate) {
                return version >= templateVersion.start && templateVersion.vehicleType != "XL" && templateVersion.useEng == 'true'
              }
              return version >= templateVersion.start && templateVersion.vehicleType != "XL" && templateVersion.useEng != 'true'
            }
            else{
              return version >= templateVersion.start
            }
          })
          console.log({correctTemplate:correctTemplate})
          
          
          //--------DETERMINE FARTHEST LEFT/RIGHT FOR EACH PAGE-------------
          var farthestRight = [];
          var farthestLeft  = [];
          renderParams.blocks.forEach(block=>{
            let isVertical = block.transform[0] == 0
            let right = isVertical ? block.transform[5] - block.width : block.transform[4] + block.width
            let left  = isVertical ? block.transform[5] : block.transform[4]
            if(isVertical){ // left is down, right is up
              if( right < farthestRight[block.page - 1] || farthestRight[block.page - 1] == undefined){
                farthestRight[block.page - 1] = right;
              }
              if( left > farthestLeft[block.page - 1] || farthestLeft[block.page - 1] == undefined){
                farthestLeft[block.page - 1] = left;
              }
            }
            else{
              if( right > farthestRight[block.page - 1] || farthestRight[block.page - 1] == undefined){
                farthestRight[block.page - 1] = right;
              }
              if( left < farthestLeft[block.page - 1] || farthestLeft[block.page - 1] == undefined){
                farthestLeft[block.page - 1] = left;
              }
            }
           
          })
          
          const yearDifference = parseInt(fileMetaDataYear) - parseInt(fileYear);
          const weekDifference = parseInt(fileMetaDataWeek) - parseInt(fileWeek);
          const totalWeekDifference = yearDifference * 52 + weekDifference; 
          //---------VALIDATE THE DATE OF THE METADATA FILE---------
          //totalWeekDifference is the maximum week difference allowed between the metadata week and the file week
          if(userDefinedWeek != fileWeek || !(totalWeekDifference >= 0 && totalWeekDifference <= 2)){
            const errorInWeek = userDefinedWeek != fileWeek
            const errorInYear = fileMetaDataYear != fileYear
            const errorInYearWeek = errorInWeek && errorInYear
            const errorInYearOrWeek = errorInYearWeek 
              ? 1 : errorInWeek 
              ? 2 : errorInYear 
              ? 3 : 4
            const errorTextMap = {
              1 : 'week and year',
              2 : 'week',
              3 : 'year',
              4 : 'week or year'
            }
            metadataErrorText = errorTextMap[errorInYearOrWeek]
            throw "ERROR_METADATA"
          }
          
          //---------PARSE FIELDS AND TABLE DATA--------------
          // console.log({fields: correctTemplate.fields, startParsingAfter: correctTemplate.startParsingAfter})

          let vehicleType = 'standard'
          if(correctTemplate.vehicleType) {
            vehicleType = correctTemplate.vehicleType
          }
          if(version >= 202243 && useEngTemplate) {
            var fields = fieldsMetrics
          } else {
            var fields = await parseFields(renderParams.blocks, correctTemplate.fields, correctTemplate.startParsingAfter)
          }
          var tables = await parseTables(renderParams.blocks, correctTemplate.tables, farthestRight, farthestLeft, templateType, version, vehicleType, group)
                    

          let payload = ''
          let payloadSummary = ''

          if((templateType ===  'customerFeedback'  && correctTemplate.start >= 202239) ||
           (templateType === 'podQualityFeedback' && correctTemplate.start >= 202303)){
            if(tables[0].table_data.length == 0) throw "No table data found (summary tables)"
            if(tables[1].table_data.length == 0) throw "No table data found"
            
            // Clean transporter id when it came with .. at the end
            tables[1].table_data.map(row => {
              if(row.transporter_id && row.transporter_id.endsWith('..')){
                row.transporter_id = row.transporter_id.replace('..', '');
              }
            })
            const summaryTable = tables.slice(0,1)
            const normalResults = tables.slice(1)
            payloadSummary = {fields: fields, tables: summaryTable, alreadyUploaded: !s3Stream.Expires}
            
            payload = {fields: fields, tables: normalResults, alreadyUploaded: !s3Stream.Expires}
            results = JSON.stringify(payload)
          }else{
            if(tables[0].table_data.length == 0) throw "No table data found"
            // Clean transporter id when it came with .. at the end
            tables[0].table_data.map(row => {
              if(row.transporter_id && row.transporter_id.endsWith('..')){
                row.transporter_id = row.transporter_id.replace('..', '');
              }
            })
            payload = {fields: fields, tables: tables, alreadyUploaded: !s3Stream.Expires }
            if(templateType != 'scorecard'){
              results = JSON.stringify(payload)
            }
          }

          //---------VALIDATE TRANSPORTER ID--------------
          payload.tables[0].table_data.reduce((map, row) => {
            if(row.transporter_id){
              let checkTransporter = checkTransporterId(row.transporter_id) 
              if(!checkTransporter){
                errorTransporterId = row.transporter_id
                throw "ERROR_TRANSPORTERID"
              }
            }
          }, {})
          /**
           * ScoreCard Validator
          */
          if(job.Item.type === 'scorecard'){

            if(renderParams.page <= 3){
              scorecardError = true
              throw 'ERROR_002_SCORECARD_VALIDATOR_007 (002.007)'
            }
            let overall = fields.overall || ''
            let safetyAndCompliance = fields.safetyAndCompliance || ''
            let team = fields.team || ''
            let quality = fields.quality || ''
            
            let validateDpsLevel = checkDPS(overall.trim(), safetyAndCompliance.trim(), team.trim(), quality.trim())
            
            if(validateDpsLevel.validate == false){
              errorsCategories = validateDpsLevel.errorCategories
              throw "ERROR_DSP_LEVEL"
            }
            validatorList = newScorecardValidator(payload)
            results = JSON.stringify(payload)
            if(validatorList){
              const reports = validatorList || []
              if(reports.length){
                throw { validatorList: reports }
              }
            }
          }


          //--------VALIDATE COMPLETE CUSTOMER FEEDBACK PDF FILE-------------
          if(job.Item.type === 'customerFeedback'){
              if (renderParams.page <= 2) {
                throw 'ERROR_002_SCORECARD_VALIDATOR_007 (002.007)'
              }
              if(typeof fields.week === "undefined"){
                customerFeedbackError = true
                throw "Error importing your weekly performance data"
              }
          }

          //--------VALIDATE COMPLETE POD PDF FILE-------------
          if(job.Item.type === 'podQualityFeedback'){
              if (renderParams.page <= 2) {
                throw 'ERROR_002_SCORECARD_VALIDATOR_007 (002.007)'
              }
              if(typeof fields.week === "undefined"){
                PODFeedbackError = true
                throw "Error importing your weekly performance data"
              }
          }
          
          validatorList = validatorList ? JSON.stringify(validatorList) : null
          //---------UPDATE DYNAMO DB-------------
          const updatedParams = {
            TableName: process.env.API_HERA_TEXTRACTJOBTABLE_NAME,
            Key:{
                "id": id
            },
            UpdateExpression: "set jobStatus = :j, results = :r, week = :w, scoreCardFailedValidators = :scfv, summaryTableResults = :sr" ,
            ExpressionAttributeValues:{
                ":j": "SUCCEEDED",
                ":r": JSON.stringify(payload),
                ":w": week,
                ":scfv": validatorList,
                ":sr": JSON.stringify(payloadSummary)
            },
            ReturnValues:"UPDATED_NEW"
          };
          await updateDynamoRecord(updatedParams)
      }).catch(error=>{
        throw error
      })

      cwm.scorecard.metricGroup2({
        bySeverity: metricDimension
      }).pushMetricData({
        MetricName: 'Success',
        Value: 1,
        Unit: 'Count'
      })
      await cwm.sendToCloudWatch()
      const jobStatus = 'SUCCEEDED'
      
      await jobComplete(group, owner, id, jobStatus, results)
      await trigger(jobId, group, owner, timeZone)

    }catch(error) {
      
      const jobStatus = 'ERROR'
      let resultErrors = ''
      if(error.validatorList){
        const uniqueErrorCodes = new Set([
          ...error.validatorList.flatMap(item => item.failedValidatorList)
        ])
        resultErrors = [...uniqueErrorCodes].join(", ")
      }else{
        resultErrors = JSON.stringify(error, Object.getOwnPropertyNames(error))
      }
      validatorList = validatorList ? JSON.stringify(validatorList) : null
        
      //---------UPDATE DYNAMO DB-------------
      try {
        const updatedParams = {
          TableName: process.env.API_HERA_TEXTRACTJOBTABLE_NAME,
          Key:{
            "id": id
          },
          UpdateExpression: "set jobStatus = :j, results = :r, scoreCardFailedValidators = :scfv",
          ExpressionAttributeValues:{
            ":j": jobStatus,
            ":r": resultErrors,
            ":scfv": validatorList
          },
          ReturnValues:"UPDATED_NEW"
        }
        await updateDynamoRecord(updatedParams)
      } catch (e) {
        console.log('Error updating a textracjob item', e)
      }

      // save error notification
      const input = await failedValidations(job, error, metadataErrorText, originalName, tenantName, shortPdfURL, errorsCategories, errorTransporterId)

      try {
        if(input){
          await executeMutation(createNotification, {input})
        }
        await jobComplete(group, owner, id, jobStatus, resultErrors)
        await trigger(jobId, group, owner, timeZone)
      } catch (e) {
        console.log('Failed to create notifications', e)
      }
    }
  }
}

// default render callback
async function jobComplete(group, owner, id, jobStatus, results){
  try{
    let input = {
      group: group,
      owner: owner,
      title: "JOB COMPLETE",
      description:  "Your import job is complete",
      payload: JSON.stringify({ id: id, jobStatus: jobStatus, results: results }),
      clickAction: 'dismiss',
      isRead: true
    }
    await executeMutation(createNotification, {input})
  }catch(e){
    console.error("Error jobComplete: ", e)
  }
}

async function trigger(jobId, group, owner, timeZone){
  var jobs = []
  var lastEvaluatedKey = null
  try{
    do{
      let fields = ['jobId','jobStatus']
      let queryParams = buildQueryParams(process.env.API_HERA_TEXTRACTJOBTABLE_NAME, fields, jobId, 'byJobId', lastEvaluatedKey)
      let queryResult = await queryDynamoRecords(queryParams);
      let items = queryResult.Items
      lastEvaluatedKey = queryResult.LastEvaluatedKey
      jobs = [...jobs, ...items]
    }while(lastEvaluatedKey)
    
    let jobsFiltered = jobs.filter(job=>job.jobStatus != "SUCCEEDED" && job.jobStatus != "ERROR")
    if( jobsFiltered.length == 0){
      // create Notification using GraphQL to trigger subscription
      var input = {
        group: group,
        owner: owner,
        title: "Weekly performance upload complete and ready to be imported",
        description:  "Your weekly performance import upload started " + new Date(parseInt(jobId.split('_')[1])).toLocaleString('en-US', {timeZone: timeZone}) + " is complete. Click here to continue the import.",
        payload: JSON.stringify({name: 'WeeklyDataImport', params: {importId: jobId}}),
        clickAction: 'navigate',
        isRead: false
      }
      await executeMutation(createNotification, {input})
    }
  }catch(e){
    console.error("Error trigger: ", e)
  }
}

function render_page(renderParams){
  return function(pageData) {
    //check documents https://mozilla.github.io/pdf.js/
    let render_options = {
        //replaces all occurrences of whitespace with standard spaces (0x20). The default value is `false`.
        normalizeWhitespace: true,
        //do not attempt to combine same line TextItem's. The default value is `false`.
        disableCombineTextItems: false
    }

    return pageData.getTextContent(render_options)
    	.then(function(textContent) {
    		for (let item of textContent.items) {
	        item.page = renderParams.page;
          renderParams.blocks.push(item)
    		}
    		renderParams.page++;
    		return text;
    });
  }
}


