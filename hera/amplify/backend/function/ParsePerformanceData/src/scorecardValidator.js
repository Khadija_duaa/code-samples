//HERA-1049
const {
  checkTransporterId
} = require('./validate.js');

function scoreCardValidatorsFailedList(payload, blocks) {
  let report = []
  let pagesValidator = pagesCounterValidator(blocks)
  payload.tables[0].table_data.forEach(row => {
		let validator = { failedValidatorList: [] }
		let isValidTransporterID = false
		if(row.transporter_id) {
			let failedValidators = scoreCardValidator(row)
			isValidTransporterID = checkTransporterId(row.transporter_id)
			if(isValidTransporterID && failedValidators.length){
				validator['transporter_id'] = row.transporter_id
				validator['failedValidatorList'] = [...failedValidators]
				report.push(validator)
			}
		}
  });
  if(report.length) return { report: [...report], pagesValidator: pagesValidator }
  return null
}

function newScorecardValidator(payload) {
  let report = []
  const yearWeek = parseInt(payload.fields?.year + payload.fields?.week)
   payload.tables[0].table_data.forEach(row => {
		let validator = { failedValidatorList: [] }
		let isValidTransporterID = false
		if(row.transporter_id) {
			let failedValidators = scoreCardValidator(row, yearWeek)
			isValidTransporterID = checkTransporterId(row.transporter_id)
			if(isValidTransporterID && failedValidators.length){
				validator['transporter_id'] = row.transporter_id
				validator['failedValidatorList'] = [...failedValidators]
				report.push(validator)
			}
		}
  });
  return report
}

function scoreCardValidator(row, yearWeek) {
 
  let validator = []
  let overallTierValidator = [
    'Fantastic',
    'Great',
    'Fair',
    'Poor',
    'Coming Soon',
    'Coming  Soon',
    'Coming ',
    'Coming',
  ]

  const cc_opps = parseInt(row.cc_opps)
  const pod_opps = parseInt(row.pod_opps)
  const dnrs = parseInt(row.dnrs)
  const dar = parseInt(row.dar)

  let swcCcValidator = testPercentageValue(row.swc_cc)
  let swcPodValidator = testPercentageValue(row.swc_pod)


  // If CC Opps. is > 0 then SWC-CC must be a decimal percentage with one decimal place (example 100.0%)
  if(cc_opps > 0 && !swcCcValidator && isValid(row.cc_opps) && isValid(row.swc_cc) ) {
    validator.push('ERROR_002_SCORECARD_VALIDATOR_001 (002.001)')
  }
  // If CC Opps. = 0 then SWC-CC must be empty 
  if(cc_opps == 0 && swcCcValidator && isValid(row.cc_opps) && isValid(row.swc_cc) ) {
    validator.push('ERROR_002_SCORECARD_VALIDATOR_002 (002.002)')
  }
  // If POD Opps. is > 0 then  SWC-POD must be a decimal percentage with one decimal place (example 100.0%)
  if(pod_opps > 0 && !swcPodValidator && isValid(row.pod_opps) && isValid(row.swc_pod) ) {
    validator.push('ERROR_002_SCORECARD_VALIDATOR_003 (002.003)')
  }
  
  //new template for 202335
  if(yearWeek < 202335){
    const dnrsIsValid = isValidDnrsDar(row.dnrs, row.overal_tier, row.fico_score)
    const darIsValid =  isValidDnrsDar(row.dar, row.overal_tier, row.fico_score)

    // If DNRs = 0 then DAR must = 100
    if(!isNaN(dnrs) && dnrs == 0 && dar != 100 && !isNaN(dar)) {
      validator.push('ERROR_002_SCORECARD_VALIDATOR_004 (002.004)')
    }
    // If DNRs != 0 then DAR must be < 100
    if(!isNaN(dnrs) && dnrs !== 0 && !(dar < 100) && !isNaN(dar)) {
      validator.push('ERROR_002_SCORECARD_VALIDATOR_005 (002.005)')
    }
    if(!dnrsIsValid || !darIsValid){
      validator.push('ERROR_002_SCORECARD_VALIDATOR_005 (002.005)')
    }
  }
  
  
  // Ensure Overall Tier (FOR THE DA ON THE DA TABLE) is one of the following
  if(!overallTierValidator.includes(row.overal_tier)) {
    validator.push('ERROR_002_SCORECARD_VALIDATOR_006 (002.006)')
  }

  for(let property in row){
    if(property != 'name' && property != 'transporter_id' && checkComingSoonValues(row[property])){
      row[property] = null
    }
  }

  return validator
}

function isValidDnrsDar(value, overalTier, ficoScore) {
  if(!value && (checkComingSoonValues(overalTier) || checkComingSoonValues(ficoScore))){
    return true
  }
  if(checkComingSoonValues(value)){
    return true
  }
  if(value && isNaN(parseInt(value))){
     return false
  }
  if(!value) return false
  return true
}

function isValid(value){
  if((!value && value != 0) || checkComingSoonValues(value)){
    return false
  }
  return true
}

function checkComingSoonValues(value=''){
  const comingSoonValues = [
    'Coming Soon',
    'Coming'
  ]
  const text = value.trim().replace(/\s+/g, ' ')
  return comingSoonValues.includes(text)
}

function pagesCounterValidator(page, blocks, type) {

  const typePDF = ['scorecard', 'podQualityFeedback', 'customerFeedback'];
  if (!typePDF.includes(type)) {
    return { isValid: false }
  }
  if (type == "scorecard") {
    if (page <= 3) {
      return { isValid: false }
    }
    let pages = blocks.filter(block => block.str && block.str.includes('Page ') && /Page[0-9 ]*of[ 0-9]*/gi.test(block.str))
    let totalPDFPages = pages[0] ? (pages[0].str ? pages[0].str.split(' ') : null) : null
    if (!totalPDFPages) {
      return { isValid: false }
    }
    totalPDFPages = totalPDFPages[totalPDFPages.length - 1]
    let totalPagesReadLength = pages.length
    let lastPageRead = pages[pages.length - 1].page
    let isValid = (totalPDFPages == totalPagesReadLength) && (totalPDFPages == lastPageRead) && (totalPagesReadLength == lastPageRead)
    let message = `Read ${totalPagesReadLength}/${totalPDFPages} pages.`
    return { isValid, message }
    
  } else if (type == "podQualityFeedback") {
    if (page <= 2) {
      return { isValid: false }
    }
  } else if (type == "customerFeedback") {
    if (page <= 3) {
      return { isValid: false }
    }
  }
  return { isValid: true }
}

function testPercentageValue(value) {
  const regex = /^(100\.0|[1-9]?\d\.\d{1})(\%)$/gm
  return regex.test(value)
}

module.exports = { scoreCardValidatorsFailedList, scoreCardValidator, pagesCounterValidator, newScorecardValidator };
