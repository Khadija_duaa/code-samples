const XLSX = require('xlsx')

const workBookFromBuffer = function(buffer){
    return XLSX.read(buffer, {type: 'array'})
}

const workBookFromFile = function(file){
    return new Promise((resolve, reject)=>{
        const fr = new FileReader()
        fr.onload = (event) => {
            const arrayBuffer = new Uint8Array(event.target.result)
            const workbook = XLSX.read(arrayBuffer, {type: 'array'})
            resolve(workbook)
        }
        fr.onerror = () => reject(new Error("Error loading file."))
        fr.readAsArrayBuffer(file)
    })
}

const tableDataFrom_Header_A_algorithm = function(sheet){
    // const capitalize = (text) => text.replace(/\w\S*/g, (w) => w.length < 4 ? w.toUpperCase() : (w.replace(/^\w/, (c) => c.toUpperCase())))
    const aux = []
    const header = []
    const body = []
    const unnamedColumns = []
    let lastUnnamed = 0
    sheet.forEach( (importedRow, i) => {
        if(i === 0){//Header
            Object.keys(importedRow).forEach((letter, j)=>{
                let columnName = importedRow[letter]
                if(!columnName){
                    columnName = String(lastUnnamed++)
                    unnamedColumns.push(columnName)
                }
                // columnName = capitalize(columnName.toLowerCase())
                aux[j] = {letter, columnName}
                header[j] = columnName
            })
        }else{//Body
            const row = {}
            aux.forEach( (importedColumn) => { row[importedColumn.columnName] = importedRow[importedColumn.letter] })
            body.push(row)
        }
    })
    return {unnamedColumns, header, body}
}

const sheetToJson = (sheet, params) => XLSX.utils.sheet_to_json(sheet, params)

const  getHyperlinkValue = function(cell) {
    if (cell.f.startsWith('HYPERLINK')) {
      const match = /HYPERLINK\("([^"]+)", "([^"]+)"\)/.exec(cell.f);
      if (match) {
        return match[2];
      }
    }
    return cell.v;
}

const keyOf = (value) => value.replace(/\s+/g, '').toLowerCase()

const yesterday = function(){
    const yesterday = new Date()
    yesterday.setHours(0,0,0,0)
    yesterday.setDate(yesterday.getDate() - 1)
    return yesterday
}

const toTitleCase = function(value){
    return value.replace('-', ' ').split(' ').map(word => word[0].toUpperCase() + word.substr(1).toLowerCase()).join(' ')
}

const getRangeFromSheet = function(sheet, key = '!ref' ){
    let rangeRegexp = /(?<firstColumn>\w)(?<firstRow>\d*):(?<lastColumn>\w)(?<lastRow>\d*)/gi
    let range = rangeRegexp.exec(sheet[key])
    range.groups.firstRow = parseInt(range.groups.firstRow)
    range.groups.lastRow = parseInt(range.groups.lastRow)
    return range.groups
}

const colLetter = function(n){
    const ordA = 'A'.charCodeAt(0)
    const ordZ = 'Z'.charCodeAt(0)
    const len = ordZ - ordA + 1
    let s = ""
    while(n >= 0) {
        s = String.fromCharCode(n % len + ordA) + s
        n = Math.floor(n / len) - 1
    }
    return s
}

const getColumnRange = function (start, end){
    let i = 0
    let range, letter
    do{
        letter = colLetter(i++)
        if(letter === start){
            range = []
        }
        if(range){
            range.push(letter)
        }
    }while(letter !== end)
    return range
}

module.exports = { workBookFromBuffer, workBookFromFile, tableDataFrom_Header_A_algorithm, sheetToJson, getHyperlinkValue, keyOf, yesterday, toTitleCase, getRangeFromSheet, colLetter, getColumnRange }