let overallLevel = ["Fantastic Plus", "Fantastic", "Great", "Fair", "Poor", "Coming Soon"];
let safetyAndComplianceLevel = ["Fantastic", "Great", "Fair", "Poor", "Coming Soon"];
let teamLevel = ["Fantastic", "Great", "Fair", "Poor", "Coming Soon"];
let qualityLevel = ["Fantastic", "Great", "Fair", "Poor", "Coming Soon"];

function checkOverall(text) {
    let result = overallLevel.includes(text);

    return result;
}

function checkSafetyAndCompliance(text) {
    let result = safetyAndComplianceLevel.includes(text);

    return result;
}

function checkTeam(text) {
    let result = teamLevel.includes(text);

    return result;
}

function checkQuality(text) {
    let result = qualityLevel.includes(text);

    return result;
}

function checkDPS(overall, safety, team, quality) {
    let check = true;
    let errorCategories = []

    if (!checkOverall(overall)) {
        errorCategories.push(`Overall: ${overall}`);
    }

    if (!checkSafetyAndCompliance(safety)) {
        errorCategories.push(`Safety and compliance: ${safety}`);
    }

    if (!checkTeam(team)) {
        errorCategories.push(`Team: ${team}`);
    }

    if (!checkQuality(quality)) {
        errorCategories.push(`Quality: ${quality}`);
    }

    if (!checkOverall(overall) || !checkSafetyAndCompliance(safety) || !checkTeam(team) || !checkQuality(quality)) {
        check = false;
    }

    if (check) {
        return true;
    }else{
        let result = {
            'validate': check,
            'errorCategories': JSON.stringify(errorCategories)
        }
    
        return result;
    }
}

module.exports = { checkDPS };
