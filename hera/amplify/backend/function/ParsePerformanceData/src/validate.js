function checkTransporterId(transporterId) {
    let regex = /^(A)(?=.*[A-Z])[A-Z0-9]*$/gm;
    let result = regex.test(transporterId.replace(/\s+/g, ""));

    return result;
}

module.exports = { checkTransporterId };
