async function parseFields(blocks, fields, startParsingAfter = null){
    var startParsing = startParsingAfter == null ? true : false
    var multiplePossibleStartPoints = Array.isArray(startParsingAfter)
    // var fields = [...templateFields]
    // var blocks = [...templateBlocks]
    
    // if theres multiple start points and you can't find the first, fall back to the next
    if(multiplePossibleStartPoints){
        for(const startingPoint of startParsingAfter){
             blocks.forEach((Block, index)=>{
                //Because the first page can be different week to week, look for good place to start.
                if( Block.str == startingPoint && !startParsing){
                    startParsing = true
                    // console.log({startParsingAfter: index})
                }
                
                //Parse the field if we've seen startParsingAfter field.
                if(startParsing){
                    checkForPattern(fields, blocks, index)
                }
            })
            if(startParsing) break // if good starting point is found, skip the rest
        }
    }
    else{
        blocks.forEach((Block, index)=>{
            //Because the first page can be different week to week, look for good place to start.
            if( Block.str == startParsingAfter && !startParsing){
                startParsing = true
                // console.log({startParsingAfter: index})
            }
            
            //Parse the field if we've seen startParsingAfter field.
            if(startParsing){
                checkForPattern(fields, blocks, index)
            }
        })
    }
    
    
    //---------FORMAT RETURN DATA--------//
    var fieldsObject = {}
    fields.forEach(field => {
        fieldsObject[field.key] = field.value
    })
    return fieldsObject
}


//----------CHECK PATTERN FOR A MATCH------------//
function checkForPattern(fields, blocks, currentIndex){
    const validTeam = ['Fantastic', 'Great', 'Fair', 'Poor', 'Coming Soon']

    fields.forEach((field, index)=>{

        //-------SET INIT COUNT FOR EACH FIELD-----------//
        if(field.count == undefined){
            field.count = 0
        }

        //---------CHECK FIELD PATTERN AGAINST BLOCKS PATTERN-----------//
        var pattern = field.pattern
        var patternValue = blocks[currentIndex].str.trim()
        
        //If regex is passed, use that to capture group to test pattern against
        if(field.regex){
            let regex = RegExp(field.regex)
            var patternMatches = patternValue.match(regex)
            if( patternMatches == null) {
                // console.log('returning with null pattern match')
                return
            }
            //If we have a regex match, get the correct group
            patternValue = patternMatches[field.regexGroup]
        }
        var isMatch = pattern == patternValue

        //--------CHECK FOR NON MATCH-----------//
        if(!isMatch || patternValue == undefined){
            return
        }


        //---------SHIFT RELATIVE POSITION BASED ON IF TEXT IS BEFORE OR AFTER PATTERN-----------//
        var blockPos = currentIndex + field.relativePos 
        
        //--------CHECK MIN PAGE REQUIREMENTS----------//
        // var minPage = field.minPage
        // var page = blocks[blockPos].Page
        // if( page < minPage ){
        //     return
        // }

        //----------CHECK TO SEE IF THIS IS THE RIGHT INSTANCE OF THE PATTERN---------//
        field.count++
        if( field.instance !== field.count){
            return
        }
        
        //--------GET AND TRANSFORM FIELD VALUE----------//
        var value = blocks[blockPos].str            
        if( field.transform ){
            value = eval(field.transform);
        }

        if(field.key === 'team' && !validTeam.includes(value)) {
            let newRelativePosition = field.relativePos
            if(field.relativePos2) {
                newRelativePosition = field.relativePos2
                blockPos = currentIndex + newRelativePosition
                value = blocks[blockPos].str
            }

            if(!validTeam.includes(value) && field.relativePos3) {
                newRelativePosition = field.relativePos3
                blockPos = currentIndex + newRelativePosition
                value = blocks[blockPos].str
            }

            if(!validTeam.includes(value) && field.relativePos4) {
                newRelativePosition = field.relativePos4
                blockPos = currentIndex + newRelativePosition
                value = blocks[blockPos].str
            }
            
            if(!validTeam.includes(value) && field.relativePos5) {
                newRelativePosition = field.relativePos5
                blockPos = currentIndex + newRelativePosition
                value = blocks[blockPos].str
            }

            if(!validTeam.includes(value)) {
                value = ''
            }
        }

        //----------SET THE CORRECT FIELD VALUE----------//
        field.value = value  
        if( field.key == 'year'){
            // console.log({year: field})       
        }
        if( field.key == 'week'){
            // console.log({week: field})       
        }
    })
}

module.exports = {parseFields}