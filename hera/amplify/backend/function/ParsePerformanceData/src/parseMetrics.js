async function parseMetrics(payload) {
    let strConcatenated = payload.text.split("\n")
    const pageTwoIndex = strConcatenated.findIndex(text => text.startsWith('DSP Scorecard'))
    if(pageTwoIndex) {
        strConcatenated = strConcatenated.slice(pageTwoIndex)
    }

    let dspMetrics = {
        "week": getValueByIndex(strConcatenated, 'Week', 'Week'),
        "year": getYear(strConcatenated),
        "overall": getValueByIndex(strConcatenated, 'Overall Standing:', ':'),
        "safetyAndCompliance": getValueByIndex(strConcatenated, 'Safety and Compliance:', ':'),
        "safetyScore": getValueByIndex(strConcatenated, 'On-Road Safety Score*', '*'),
        "safeDriving": getValueByIndex(strConcatenated, 'Safe Driving Metric', 'Metric'),
        "safeDrivingText": getValueByIndex(strConcatenated, 'Safe Driving Metric', 'Metric', 1),
        "comprehensiveAudit": getValueByIndex(strConcatenated, 'Comprehensive Audit (CAS)', '(CAS)'),
        "seatbeltOff": getValueByIndex(strConcatenated, 'Seatbelt-Off Rate', 'Rate'),
        "seatbeltOffText": getValueByIndex(strConcatenated, 'Seatbelt-Off Rate', 'Rate', 1),
        "speedingEvent": getValueByIndex(strConcatenated, 'Speeding Event Rate', 'Rate'),
        "speedingEventText": getValueByIndex(strConcatenated, 'Speeding Event Rate', 'Rate', 1),
        "workingHoursCompliance": getValueByIndex(strConcatenated, 'Working Hour Compliance', 'Compliance'),
        "workingHoursComplianceText": getValueByIndex(strConcatenated, 'Working Hour Compliance', 'Compliance', 1),
        "customerDeliveryExperience": getValueByIndex(strConcatenated, 'Customer Delivery Experience', 'Experience'),
        "customerDeliveryFeedback": getValueByIndex(strConcatenated, 'Customer Delivery Feedback', 'Feedback'),
        "customerDeliveryFeedbackText": getValueByIndex(strConcatenated, 'Customer Delivery Feedback', 'Feedback', 1),
        "breachOfContract": getValueByIndex(strConcatenated, 'Breach of Contract', 'Contract'),
        "tenuredWorkforce": getValueByIndex(strConcatenated, 'Tenured Workforce', 'Workforce'),
        "tenuredWorkforceText": getValueByIndex(strConcatenated, 'Tenured Workforce', 'Workforce', 1),
        "customerEscalationDefectDPMO": getValueByIndex(strConcatenated, 'Customer Escalation Defect DPMO', 'DPMO'),
        "customerEscalationDefectDPMOText": getValueByIndex(strConcatenated, 'Customer Escalation Defect DPMO', 'DPMO', 1),
        "team": getValueByIndex(strConcatenated, 'Team:', ':'),
        "highPerformersShare": getValueByIndex(strConcatenated, 'High Performers Share', 'Share'),
        "highPerformersShareText": getValueByIndex(strConcatenated, 'High Performers Share', 'Share', 1),
        "lowPerformersShare": getValueByIndex(strConcatenated, 'Low Performers Share', 'Share'),
        "lowPerformersShareText": getValueByIndex(strConcatenated, 'Low Performers Share', 'Share', 1),
        "quality": getValueByIndex(strConcatenated, 'Quality:', ':'),
        "deliveryCompletion": getValueByIndex(strConcatenated, 'Delivery Completion Rate', 'Rate'),
        "deliveryCompletionText": getValueByIndex(strConcatenated, 'Delivery Completion Rate', 'Rate', 1),
        "deliveredAndReceived": getValueByIndex(strConcatenated, 'Delivered and Received', 'Received'),
        "deliveredAndReceivedText": getValueByIndex(strConcatenated, 'Delivered and Received', 'Received', 1),
        "standardWorkComplianceText": getValueByIndex(strConcatenated, 'Standard Work Compliance', 'Compliance'),
        "photoOnDelivery": getValueByIndex(strConcatenated, 'Photo-On-Delivery', 'Delivery'),
        "photoOnDeliveryText": getValueByIndex(strConcatenated, 'Photo-On-Delivery', 'Delivery', 1),
        "contactCompliance": getValueByIndex(strConcatenated, 'Contact Compliance', 'Compliance'),
        "contactComplianceText": getValueByIndex(strConcatenated, 'Contact Compliance', 'Compliance', 1),
        "signSignalViolationsRate": getValueByIndex(strConcatenated, 'Sign/Signal Violations Rate', 'Rate'),
        "signSignalViolationsRateText": getValueByIndex(strConcatenated, 'Sign/Signal Violations Rate', 'Rate', 1),
        "distractionsRate": getValueByIndex(strConcatenated, 'Distractions Rate', 'Rate'),
        "distractionsRateText": getValueByIndex(strConcatenated, 'Distractions Rate', 'Rate', 1),
        "followingDistanceRate": getValueByIndex(strConcatenated, 'Following Distance Rate', 'Rate'),
        "followingDistanceRateText": getValueByIndex(strConcatenated, 'Following Distance Rate', 'Rate', 1),
        "attendedDeliveryAccuracy": getValueByIndex(strConcatenated, 'Attended Delivery Accuracy', 'Accuracy'),
        "attendedDeliveryAccuracyText": getValueByIndex(strConcatenated, 'Attended Delivery Accuracy', 'Accuracy', 1),
        "deliverySlotPerformance": getValueByIndex(strConcatenated, 'Delivery Slot Performance', 'Performance'),
        "deliverySlotPerformanceText": getValueByIndex(strConcatenated, 'Delivery Slot Performance', 'Performance', 1),
        "deliverySuccessBehaviors": getValueByIndex(strConcatenated, 'Delivery Success Behaviors', 'Behaviors'),
        "deliverySuccessBehaviorsText": getValueByIndex(strConcatenated, 'Delivery Success Behaviors', 'Behaviors', 1)
    }
    return dspMetrics
}
function getYear(payload) {
    let regex = /^(19|20)\d{2}$/
    let year = payload.find(element => element.match(regex))
return year
}
function getValueByIndex(payload, pattern, lastWord, index = 0) {
    let line = payload.find(element => element.startsWith(pattern))
    if (line) {
        let attributeIndex = line.indexOf(lastWord) + lastWord.length
        return line.slice(attributeIndex).trim().split(' | ')[index]
    }
    return line
}
module.exports = { parseMetrics }