const { checkTransporterId } = require('validate')
const { getStaffsByGroup, getTransporterIdByName } = require('./weeklyPerformance/parseHelper')

async function parseTables(blocks, tables, farthestRight, farthestLeft, templateType, version, vehicleType, group){
    
    // const staffsByGroup = await getStaffsByGroup(group)
    var isHeader = false
    var tableIndex = null
    var positionLookup = {}
    var biggestIndex = 0;
    // var tables = [...templateTables]
    blocks.forEach((Block, index)=>{

        var slug = slugify(Block.str)
        

        //--------CHECK PATTERN TO SEE IF WE HAVE HIT A TABLE---------//
        let prevTableIndex = tableIndex
        tableIndex = findTableIndex(index, tableIndex, tables, blocks)

        //console.log('---tableIndex', tableIndex)

        //Reset Position Lookup if new table
        if( prevTableIndex !== tableIndex){
            positionLookup = {}
            biggestIndex = 0
            //console.log('---tableIndex', tableIndex)
        }

        isHeader = false
        
        //--------DETERMINE IF BLOCK IS A HEADER--------//
        if( tableIndex >= 0 && tableIndex != null){
            isHeader = tables[tableIndex].columns.find(header=> header.slug == slug)
            // deal with header slug being the same as a key focus area slug
            if(Block.str === "Seatbelt Off Rate" && isHeader.slug === 'seatbelt_off_rate' && tableIndex == 0) isHeader = false
            if(Block.str === "Harsh Braking" && isHeader.slug === 'harsh_braking' && tableIndex == 0) isHeader = false
            if(Block.str === "Harsh Cornering" && isHeader.slug === 'harsh_cornering' && tableIndex == 0) isHeader = false
            if(Block.str === "Speeding Event Rate" && isHeader.slug === 'speeding_event_rate' && tableIndex == 0) isHeader = false
            if(Block.str === "DCR" && isHeader.slug === 'dcr' && tableIndex == 0) isHeader = false
            if(Block.str === "DAR" && isHeader.slug === 'dar' && tableIndex == 0) isHeader = false
        }

        //---------DETERMINE IF BLOCK FALLS BETWEEN COLUMN--------//
        if(!isHeader && tableIndex !== null ){       
            var isVerticalTable =  Block.transform[0] == 0
            var colIndex = getColumnIndex(Block, tableIndex, tables, farthestRight, farthestLeft, isVerticalTable, templateType, version)

            //---------DETERMINE WHICH ROW THIS IS---------//
            var rowVariance = 7.5
            var top = isVerticalTable ? Block.transform[4] : Block.transform[5] // check the first value to determine if table is vertical instead of horizontal
            var roundedTop = Math.ceil(top/rowVariance)
            const isSummaryTable = !!tables[tableIndex].summaryTable

            if(templateType == 'podQualityFeedback' && !isSummaryTable){
                rowVariance =  3.999  //How much variation in the top value to be considered a new row
                roundedTop = Math.ceil(top/rowVariance)
            } else if (templateType == 'scorecard' && vehicleType == 'XL') {
                rowVariance = 4
                if(version >= 202233) {
                    rowVariance = 4.5
                }
                roundedTop = Math.ceil(top/rowVariance)
            } else if(templateType === 'customerFeedback' && isSummaryTable){
                rowVariance = 7.8
                roundedTop = Math.ceil(top/rowVariance)
            }

            //console.log('---rowVariance', rowVariance)
            
            var page = Block.page
            var position = `${page}-${roundedTop}`
            if( !positionLookup[position] ){
                positionLookup[position] = biggestIndex
                biggestIndex ++ 
            }
            var rowIndex = positionLookup[position]

            //----------IF THE BLOCK IS WITHIN COLUMN BOUNDS, SAVE TO ROW-------------//
            if(colIndex >= 0){
                var column = tables[tableIndex].columns[colIndex]
                var value = Block.str
                if( column.transform ){
                    value = eval(column.transform)
                }

                //Setup table at correct position if none
                if(tables[tableIndex].table_data[rowIndex] === undefined ){
                    tables[tableIndex].table_data[rowIndex] = []
                }

                //set or append data to column
                if(tables[tableIndex].table_data[rowIndex][colIndex]){
                    tables[tableIndex].table_data[rowIndex][colIndex] += " " + value
                }else{
                    tables[tableIndex].table_data[rowIndex][colIndex] = value
                }
            }        
        }

    })

    //console.log('---tables', JSON.stringify(tables))

    //----------FORMAT RETURN DATA----------//
    var formattedTables = []

    tables.forEach(table => {

        var obj = {}
        var min_data = table.min_data

        obj.columns = table.columns.map(column=>column.slug)
        obj.table_data = []      

        table.table_data.forEach((row)=>{

            var da = {}
            var min_data = table.min_data
            row.forEach((cell,index)=>{
                let key = obj.columns[index]
                da[key] = cell
            })

            if(da.transporter_id && checkTransporterId(da.transporter_id) && Object.keys(da).length >= min_data) {
                da.transporter_id = sanitizeTransporterId(da.transporter_id);
                obj.table_data.push(da)
            }else if(templateType === 'podQualityFeedback' && table.summaryTable && Object.keys(da).length >= min_data){
                if(da.category === "(Locker/Other Concealment)"){
                    da.category = 'Package Not Clearly Visible'
                }
                obj.table_data.push(da)
            }else if(templateType === 'customerFeedback' && table.summaryTable && Object.keys(da).length >= min_data){
                obj.table_data.push(da)
            }
            // [HERA-2176] This has been commented since files comes with TID again.            
            // else if (da.name && da.overal_tier) {
            //     da.transporter_id = getTransporterIdByName(staffsByGroup, da.name)
            //     if(da.transporter_id && checkTransporterId(da.transporter_id) && Object.keys(da).length >= min_data) {
            //         da.transporter_id = sanitizeTransporterId(da.transporter_id)
            //         obj.table_data.push(da)
            //     }
            // }

        })
        
        formattedTables.push(obj)

    })

    return formattedTables

}

function sanitizeTransporterId(transporterId) {
    return transporterId.replace(/\s+/g, "")
}

/**
 * Determines if we have found a table or not and returns the current table index
 * @param {Number} i - Current index of blocks
 */
function findTableIndex(currentIndex, tableIndex, tables, blocks){

    //Determine the next table we are looking for
    var nextTableIndex = tableIndex == null ? 0 : tableIndex + 1;
    var nextTable = tables[nextTableIndex];
    if( nextTable == undefined ){
        return tableIndex;
    }


    //Get the next section of blocks to look through for matches
    var tablePattern = nextTable.table_pattern;
    var matchPercent = nextTable.table_match_percent;
    var patternLength = tablePattern.length;
    var isMatchRequiredCount = parseInt(patternLength * matchPercent )
    var checkArray = blocks.slice(currentIndex, currentIndex + patternLength );


    //Check how many of the blocks have matching text in our pattern
    var matchCount = 0;
    nextTable.table_pattern.forEach((header, index) => {
        var isInCheckArray = checkArray.some(block=>{
            var blockText = block.str;
            var blockSlug = slugify(blockText)
            var headerSlug = header
            return blockSlug == headerSlug
        })
        if( isInCheckArray ){
            matchCount++;
        }
    });

    
    //Check to see if we have found a table!
    if( matchCount >= isMatchRequiredCount ) {
        // console.debug('---Found table', JSON.stringify({matches:matchCount, index: currentIndex, newTableIndex: nextTableIndex}))
        return nextTableIndex
    }

    return tableIndex
}


/**
 * Determine the index of the column based on the middle of the block
 * @param {Object} BoundingBox 
 */
function getColumnIndex(Block, tableIndex, tables, farthestRight, farthestLeft, isVerticalTable, templateType, version){
    if( tableIndex >= 0 && tableIndex == null ){
        return -1
    }
    
    let tableWidth = farthestRight[Block.page - 1] - farthestLeft[Block.page - 1]
    if(isVerticalTable) tableWidth *= -1

    var left = null
    if(isVerticalTable){
        left = (farthestLeft[Block.page - 1] - Block.transform[5]) / tableWidth
    }
    else{
        left = (Block.transform[4] - farthestLeft[Block.page - 1]) / tableWidth
    }
    
    let width = Block.width / tableWidth
    var middle = left + (width/2)
    
    if(version >= 202211 && Block.page > 2 && templateType == 'customerFeedback' && tables[tableIndex].columns2) {
        let index = tables[tableIndex].columns2.findIndex(header => {
            let blockLeft = header.left
            let blockRight = header.right
            return (blockLeft <= middle && blockRight >= middle)
        })
        return index
    }

    let index = tables[tableIndex].columns.findIndex(header => {
        let blockLeft = header.left
        let blockRight = header.right
        return (blockLeft <= middle && blockRight >= middle)
    })

    return index
}


/**
 * Turn a string in a consistent slug
 * @param {String} str 
 */
function slugify (str) {   
    if(!str){
        return null
    }
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();
    
    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to   = "aaaaeeeeiiiioooouuuunc------";
    for (var i=0, l=from.length ; i<l ; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 - #\-]/g, '') // remove invalid chars
        .replace(/\s+/g, '_') // collapse whitespace and replace by _
        .replace(/-+/g, '_'); // collapse dashes

    return str;
}



module.exports = {parseTables}