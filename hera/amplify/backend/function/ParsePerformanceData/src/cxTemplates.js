//Newer versions of template should be at top
var cxTemplates = {
    versionLookup: [
        {
            key: "week",
            instance: 1,
            relativePos: 0,
            regex: "(Week) (\\d\\d?)$",
            regexGroup: 1,
            pattern: "Week",
            transform: "patternMatches[2]",
        }
    ],
    versions: [
        {
            start: 202325,
            fields: [
                {
                    key: "week",
                    instance: 1,
                    relativePos: 0,
                    regex: "(Week) (\\d\\d?)$",
                    regexGroup: 1,
                    pattern: "Week",
                    transform: "patternMatches[2]",
                }
            ],
            tables: [
                {
                    table_pattern: [
                        "dsp_feedback_da_attributable",
                        ""
                    ],
                    table_match_percent: .8,
                    summaryTable: true,
                    table_data: [],
                    min_data: 2,
                    columns: [
                        {
                            slug: "category",
                            left: 0,
                            right: 0.5236
                        },
                        {
                            slug: "count",
                            left: 0.7555,
                            right: 1
                        }
                    ]
                },
                {
                    table_pattern: [
                        "rank",
                        "transporter_id",
                        "da_name",
                        "da_tier",
                        "friendly"
                    ],
                    table_match_percent: .8,
                    summaryTable: false,
                    table_data: [],
                    min_data: 4,
                    columns: [
                        {
                            slug: "rank",
                            left: 0.000,
                            right: 0.026
                        },
                        {
                            slug: "transporter_id",
                            left: 0.026,
                            right: 0.116
                        },
                        {
                            slug: "driver_name",
                            left: 0.116,
                            right: 0.200
                        },
                        {
                            slug: "da_tier",
                            left: 0.200,
                            right: 0.237
                        },
                        {
                            slug: "cdf_score",
                            left: 0.237,
                            right: 0.264
                        },
                        {
                            slug: "no_feedback",
                            left: 0.264,
                            right: 0.299
                        },
                        {
                            slug: "delivery_was_great",
                            left: 0.299,
                            right: 0.337
                        },
                        {
                            slug: "respectful_of_property",
                            left: 0.337,
                            right: 0.382
                        },
                        {
                            slug: "followed_instructions",
                            left: 0.382,
                            right: 0.423
                        },
                        {
                            slug: "friendly",
                            left: 0.423,
                            right: 0.456
                        },
                        {
                            slug: "went_above_and_beyond",
                            left: 0.456,
                            right: 0.493
                        },
                        {
                            slug: "delivered_with_care",
                            left: 0.493,
                            right: 0.532
                        },
                        {
                            slug: "thank_my_driver",
                            left: 0.532,
                            right: 0.571
                        },
                        {
                            slug: "delivery_was_not_so_great",
                            left: 0.571,
                            right: 0.607
                        },
                        {
                            slug: "mishandled_package",
                            left: 0.607,
                            right: 0.653
                        },
                        {
                            slug: "driver_unprofessional",
                            left: 0.653,
                            right: 0.693
                        },
                        {
                            slug: "not_follow_delivery_instructions",
                            left: 0.693,
                            right: 0.741
                        },
                        {
                            slug: "delivered_to_wrong_address",
                            left: 0.741,
                            right: 0.790
                        },
                        {
                            slug: "never_received_delivery",
                            left: 0.790,
                            right: 0.838
                        },
                        {
                            slug: "on_time",
                            left: 0.838,
                            right: 0.880
                        },
                        {
                            slug: "late_delivery",
                            left: 0.880,
                            right: 0.931
                        },
                        {
                            slug: "item_damaged",
                            left: 0.931,
                            right: 1.0
                        },
                    ]
                },
                {
                    table_pattern: [
                        "transporter_id",
                        "da_name",
                        "da_tier",
                        "cdf_score",
                        "driver_mishandled_package",
                        "driver_was_unprofessional"
                    ],
                    table_match_percent: .8,
                    summaryTable: false,
                    table_data: [],
                    min_data: 4,
                    columns: [
                        {
                            slug: "transporter_id",
                            left: 0.000,
                            right: 0.141
                        },
                        {
                            slug: "driver_name",
                            left: 0.141,
                            right: 0.308
                        },
                        {
                            slug: "da_tier",
                            left: 0.308,
                            right: 0.375
                        },
                        {
                            slug: "cdf_score",
                            left: 0.375,
                            right: 0.439
                        },
                        {
                            slug: "mishandled_package",
                            left: 0.439,
                            right: 0.502
                        },
                        {
                            slug: "driver_unprofessional",
                            left: 0.502,
                            right: 0.557
                        },
                        {
                            slug: "not_follow_delivery_instructions",
                            left: 0.557,
                            right: 0.609
                        },
                        {
                            slug: "delivery_to_wrong_address",
                            left: 0.609,
                            right: 0.675
                        },
                        {
                            slug: "never_received_delivery",
                            left: 0.675,
                            right: 0.782
                        },
                        {
                            slug: "tracking_id",
                            left: 0.782,
                            right: 0.920
                        },
                        {
                            slug: "delivery_rate",
                            left: 0.920,
                            right: 1.000
                        },
                    ]
                },
            ]
        },
        {
            start: 202314,
            fields: [
                {
                    key: "week",
                    instance: 1,
                    relativePos: 0,
                    regex: "(Week) (\\d\\d?)$",
                    regexGroup: 1,
                    pattern: "Week",
                    transform: "patternMatches[2]",
                }
            ],
            tables: [
                {
                    table_pattern: [
                        "dsp_feedback_da_attributable",
                        ""
                    ],
                    table_match_percent: .8,
                    summaryTable: true,
                    table_data: [],
                    min_data: 2,
                    columns: [
                        {
                            slug: "category",
                            left: 0,
                            right: 0.5236
                        },
                        {
                            slug: "count",
                            left: 0.7555,
                            right: 1
                        }
                    ]
                },
                {
                    table_pattern: [
                        "rank",
                        "transporter_id",
                        "da_name",
                        "da_tier",
                        "friendly"
                    ],
                    table_match_percent: .8,
                    summaryTable: false,
                    table_data: [],
                    min_data: 4,
                    columns: [
                        {
                            slug: "rank",
                            left: 0.000,
                            right: 0.029
                        },
                        {
                            slug: "transporter_id",
                            left: 0.029,
                            right: 0.123
                        },
                        {
                            slug: "driver_name",
                            left: 0.123,
                            right: 0.205
                        },
                        {
                            slug: "da_tier",
                            left: 0.205,
                            right: 0.249
                        },
                        {
                            slug: "cdf_score",
                            left: 0.249,
                            right: 0.278
                        },
                        {
                            slug: "no_feedback",
                            left: 0.278,
                            right: 0.314
                        },
                        {
                            slug: "delivery_was_great",
                            left: 0.314,
                            right: 0.353
                        },
                        {
                            slug: "respectful_of_property",
                            left: 0.353,
                            right: 0.402
                        },
                        {
                            slug: "followed_instructions",
                            left: 0.402,
                            right: 0.444
                        },
                        {
                            slug: "friendly",
                            left: 0.444,
                            right: 0.479
                        },
                        {
                            slug: "went_above_and_beyond",
                            left: 0.479,
                            right: 0.517
                        },
                        {
                            slug: "delivered_with_care",
                            left: 0.517,
                            right: 0.557
                        },
                        {
                            slug: "delivery_was_not_so_great",
                            left: 0.557,
                            right: 0.595
                        },
                        {
                            slug: "mishandled_package",
                            left: 0.595,
                            right: 0.643
                        },
                        {
                            slug: "driver_unprofessional",
                            left: 0.643,
                            right: 0.685
                        },
                        {
                            slug: "not_follow_delivery_instructions",
                            left: 0.685,
                            right: 0.737
                        },
                        {
                            slug: "delivered_to_wrong_address",
                            left: 0.737,
                            right: 0.787
                        },
                        {
                            slug: "never_received_delivery",
                            left: 0.787,
                            right: 0.838
                        },
                        {
                            slug: "on_time",
                            left: 0.838,
                            right: 0.881
                        },
                        {
                            slug: "late_delivery",
                            left: 0.881,
                            right: 0.934
                        },
                        {
                            slug: "item_damaged",
                            left: 0.934,
                            right: 1.0
                        },
                    ]
                },
                {
                    table_pattern: [
                        "transporter_id",
                        "da_name",
                        "da_tier",
                        "cdf_score",
                        "driver_mishandled_package",
                        "driver_was_unprofessional"
                    ],
                    table_match_percent: .8,
                    summaryTable: false,
                    table_data: [],
                    min_data: 4,
                    columns: [
                        {
                            slug: "transporter_id",
                            left: 0.000,
                            right: 0.141
                        },
                        {
                            slug: "driver_name",
                            left: 0.141,
                            right: 0.308
                        },
                        {
                            slug: "da_tier",
                            left: 0.308,
                            right: 0.375
                        },
                        {
                            slug: "cdf_score",
                            left: 0.375,
                            right: 0.439
                        },
                        {
                            slug: "mishandled_package",
                            left: 0.439,
                            right: 0.502
                        },
                        {
                            slug: "driver_unprofessional",
                            left: 0.502,
                            right: 0.557
                        },
                        {
                            slug: "not_follow_delivery_instructions",
                            left: 0.557,
                            right: 0.609
                        },
                        {
                            slug: "delivery_to_wrong_address",
                            left: 0.609,
                            right: 0.675
                        },
                        {
                            slug: "never_received_delivery",
                            left: 0.675,
                            right: 0.782
                        },
                        {
                            slug: "tracking_id",
                            left: 0.782,
                            right: 0.920
                        },
                        {
                            slug: "delivery_rate",
                            left: 0.920,
                            right: 1.000
                        },
                    ]
                },
            ]
        },
        {
            start: 202239,
            fields: [
                {
                    key: "week",
                    instance: 1,
                    relativePos: 0,
                    regex: "(Week) (\\d\\d?)$",
                    regexGroup: 1,
                    pattern: "Week",
                    transform: "patternMatches[2]",
                }
            ],
            tables: [
                {
                    table_pattern: [
                        "dsp_feedback_da_attributable",
                        ""
                    ],
                    table_match_percent: .8,
                    summaryTable: true,
                    table_data: [],
                    min_data: 2,
                    columns: [
                        {
                            slug: "category",
                            left: 0,
                            right: 0.5236
                        },
                        {
                            slug: "count",
                            left: 0.7555,
                            right: 1
                        }
                    ]
                },
                {
                    table_pattern: [
                        "rank",
                        "transporter_id",
                        "da_name",
                        "da_tier",
                        "friendly"
                    ],
                    table_match_percent: .8,
                    summaryTable: false,
                    table_data: [],
                    min_data: 4,
                    columns: [
                        {
                            slug: "rank",
                            left: 0.000,
                            right: 0.031
                        },
                        {
                            slug: "transporter_id",
                            left: 0.031,
                            right: 0.132
                        },
                        {
                            slug: "driver_name",
                            left: 0.132,
                            right: 0.227
                        },
                        {
                            slug: "da_tier",
                            left: 0.227,
                            right: 0.269
                        },
                        {
                            slug: "cdf_score",
                            left: 0.269,
                            right: 0.301
                        },
                        {
                            slug: "no_feedback",
                            left: 0.301,
                            right: 0.340
                        },
                        {
                            slug: "delivery_was_great",
                            left: 0.340,
                            right: 0.382
                        },
                        {
                            slug: "respectful_of_property",
                            left: 0.382,
                            right: 0.435
                        },
                        {
                            slug: "followed_instructions",
                            left: 0.435,
                            right: 0.481
                        },
                        {
                            slug: "friendly",
                            left: 0.481,
                            right: 0.518
                        },
                        {
                            slug: "went_above_and_beyond",
                            left: 0.518,
                            right: 0.559
                        },
                        {
                            slug: "delivered_with_care",
                            left: 0.559,
                            right: 0.603
                        },
                        {
                            slug: "delivery_was_not_so_great",
                            left: 0.603,
                            right: 0.644
                        },
                        {
                            slug: "mishandled_package",
                            left: 0.644,
                            right: 0.696
                        },
                        {
                            slug: "driver_unprofessional",
                            left: 0.696,
                            right: 0.741
                        },
                        {
                            slug: "not_follow_delivery_instructions",
                            left: 0.741,
                            right: 0.797
                        },
                        {
                            slug: "on_time",
                            left: 0.797,
                            right: 0.824
                        },
                        {
                            slug: "late_delivery",
                            left: 0.824,
                            right: 0.864
                        },
                        {
                            slug: "item_damaged",
                            left: 0.864,
                            right: 0.908
                        },
                        {
                            slug: "delivered_to_wrong_address",
                            left: 0.908,
                            right: 0.957
                        },
                        {
                            slug: "never_received_delivery",
                            left: 0.957,
                            right: 1.000
                        },
                    ]
                },
                {
                    table_pattern: [
                        "transporter_id",
                        "da_name",
                        "da_tier",
                        "cdf_score",
                        "driver_mishandled_package",
                        "driver_was_unprofessional"
                    ],
                    table_match_percent: .8,
                    summaryTable: false,
                    table_data: [],
                    min_data: 4,
                    columns: [
                        {
                            slug: "transporter_id",
                            left: 0.000,
                            right: 0.141
                        },
                        {
                            slug: "driver_name",
                            left: 0.141,
                            right: 0.308
                        },
                        {
                            slug: "da_tier",
                            left: 0.308,
                            right: 0.375
                        },
                        {
                            slug: "cdf_score",
                            left: 0.375,
                            right: 0.439
                        },
                        {
                            slug: "mishandled_package",
                            left: 0.439,
                            right: 0.502
                        },
                        {
                            slug: "driver_unprofessional",
                            left: 0.502,
                            right: 0.557
                        },
                        {
                            slug: "not_follow_delivery_instructions",
                            left: 0.557,
                            right: 0.609
                        },
                        {
                            slug: "delivery_to_wrong_address",
                            left: 0.609,
                            right: 0.675
                        },
                        {
                            slug: "never_received_delivery",
                            left: 0.675,
                            right: 0.782
                        },
                        {
                            slug: "tracking_id",
                            left: 0.782,
                            right: 0.920
                        },
                        {
                            slug: "delivery_rate",
                            left: 0.920,
                            right: 1.000
                        },
                    ]
                },
            ]
        },
        {
            start: 202217,
            fields: [
                {
                    key: "week",
                    instance: 1,
                    relativePos: 0,
                    regex: "(Week) (\\d\\d?)$",
                    regexGroup: 1,
                    pattern: "Week",
                    transform: "patternMatches[2]",
                }
            ],
            tables: [
                {
                    table_pattern: [
                        "rank",
                        "transporter_id",
                        "da_name",
                        "da_tier",
                        "friendly"
                    ],
                    table_match_percent: .8,
                    table_data: [],
                    min_data: 4,
                    columns: [
                        {
                            slug: "rank",
                            left: 0.050,
                            right: 0.080
                        },
                        {
                            slug: "transporter_id",
                            left: 0.080,
                            right: 0.154
                        },
                        {
                            slug: "driver_name",
                            left: 0.154,
                            right: 0.226
                        },
                        {
                            slug: "da_tier",
                            left: 0.226,
                            right: 0.268
                        },
                        {
                            slug: "cdf_score",
                            left: 0.268,
                            right: 0.299
                        },
                        {
                            slug: "no_feedback",
                            left: 0.299,
                            right: 0.337
                        },
                        {
                            slug: "delivery_was_great",
                            left: 0.337,
                            right: 0.381
                        },
                        {
                            slug: "respectful_of_property",
                            left: 0.381,
                            right: 0.432
                        },
                        {
                            slug: "followed_instructions",
                            left: 0.432,
                            right: 0.477
                        },
                        {
                            slug: "friendly",
                            left: 0.477,
                            right: 0.516
                        },
                        {
                            slug: "went_above_and_beyond",
                            left: 0.516,
                            right: 0.557
                        },
                        {
                            slug: "delivered_with_care",
                            left: 0.557,
                            right: 0.603
                        },
                        {
                            slug: "delivery_was_not_so_great",
                            left: 0.603,
                            right: 0.643
                        },
                        {
                            slug: "mishandled_package",
                            left: 0.643,
                            right: 0.695
                        },
                        {
                            slug: "driver_unprofessional",
                            left: 0.695,
                            right: 0.744
                        },
                        {
                            slug: "not_delivered_to_preferred_location",
                            left: 0.744,
                            right: 0.799
                        },
                        {
                            slug: "on_time",
                            left: 0.799,
                            right: 0.826
                        },
                        {
                            slug: "late_delivery",
                            left: 0.826,
                            right: 0.865
                        },
                        {
                            slug: "item_damaged",
                            left: 0.865,
                            right: 0.909
                        },
                        {
                            slug: "delivered_to_wrong_address",
                            left: 0.909,
                            right: 0.957
                        },
                        {
                            slug: "never_received_delivery",
                            left: 0.974,
                            right: 0.996
                        },
                    ],
                columns2: [
                        {
                            slug: "rank",
                            left: 0,
                            right: 0.032
                        },
                        {
                            slug: "transporter_id",
                            left: 0.032,
                            right: 0.110
                        },
                        {
                            slug: "driver_name",
                            left: 0.110,
                            right: 0.186
                        },
                        {
                            slug: "da_tier",
                            left: 0.186,
                            right: 0.231
                        },
                        {
                            slug: "cdf_score",
                            left: 0.231,
                            right: 0.263
                        },
                        {
                            slug: "no_feedback",
                            left: 0.263,
                            right: 0.303
                        },
                        {
                            slug: "delivery_was_great",
                            left: 0.303,
                            right: 0.348
                        },
                        {
                            slug: "respectful_of_property",
                            left: 0.348,
                            right: 0.403
                        },
                        {
                            slug: "followed_instructions",
                            left: 0.403,
                            right: 0.451
                        },
                        {
                            slug: "friendly",
                            left: 0.451,
                            right: 0.491
                        },
                        {
                            slug: "went_above_and_beyond",
                            left: 0.491,
                            right: 0.534
                        },
                        {
                            slug: "delivered_with_care",
                            left: 0.534,
                            right: 0.582
                        },
                        {
                            slug: "delivery_was_not_so_great",
                            left: 0.582,
                            right: 0.625
                        },
                        {
                            slug: "mishandled_package",
                            left: 0.625,
                            right: 0.679
                        },
                        {
                            slug: "driver_unprofessional",
                            left: 0.679,
                            right: 0.730
                        },
                        {
                            slug: "not_delivered_to_preferred_location",
                            left: 0.730,
                            right: 0.787
                        },
                        {
                            slug: "on_time",
                            left: 0.787,
                            right: 0.816
                        },
                        {
                            slug: "late_delivery",
                            left: 0.816,
                            right: 0.857
                        },
                        {
                            slug: "item_damaged",
                            left: 0.857,
                            right: 0.904
                        },
                        {
                            slug: "delivered_to_wrong_address",
                            left: 0.904,
                            right: 0.954
                        },
                        {
                            slug: "never_received_delivery",
                            left: 0.954,
                            right: 1
                        },
                    ]
                }
            ]
        },
        {
            start: 202213,
            fields: [
                {
                    key: "week",
                    instance: 1,
                    relativePos: 0,
                    regex: "(Week) (\\d\\d?)$",
                    regexGroup: 1,
                    pattern: "Week",
                    transform: "patternMatches[2]",
                }
            ],
            tables: [
                {
                    table_pattern: [
                        "rank",
                        "transporter_id",
                        "da_name",
                        "da_tier",
                        "cdf",
                        "score",
                        "feedback",
                        "friendly",
                        "delivery",
                        "was",
                        "great"
                    ],
                    table_match_percent: .8,
                    table_data: [],
                    min_data: 8,
                    columns: [
                        {
                            slug: "rank",
                            left: 0.050,
                            right: 0.080
                        },
                        {
                            slug: "transporter_id",
                            left: 0.080,
                            right: 0.154
                        },
                        {
                            slug: "driver_name",
                            left: 0.154,
                            right: 0.226
                        },
                        {
                            slug: "da_tier",
                            left: 0.226,
                            right: 0.268
                        },
                        {
                            slug: "cdf_score",
                            left: 0.268,
                            right: 0.299
                        },
                        {
                            slug: "no_feedback",
                            left: 0.299,
                            right: 0.337
                        },
                        {
                            slug: "delivery_was_great",
                            left: 0.337,
                            right: 0.381
                        },
                        {
                            slug: "respectful_of_property",
                            left: 0.381,
                            right: 0.432
                        },
                        {
                            slug: "followed_instructions",
                            left: 0.432,
                            right: 0.477
                        },
                        {
                            slug: "friendly",
                            left: 0.477,
                            right: 0.516
                        },
                        {
                            slug: "went_above_and_beyond",
                            left: 0.516,
                            right: 0.557
                        },
                        {
                            slug: "delivered_with_care",
                            left: 0.557,
                            right: 0.603
                        },
                        {
                            slug: "delivery_was_not_so_great",
                            left: 0.603,
                            right: 0.643
                        },
                        {
                            slug: "mishandled_package",
                            left: 0.643,
                            right: 0.695
                        },
                        {
                            slug: "driver_unprofessional",
                            left: 0.695,
                            right: 0.744
                        },
                        {
                            slug: "not_delivered_to_preferred_location",
                            left: 0.744,
                            right: 0.799
                        },
                        {
                            slug: "on_time",
                            left: 0.799,
                            right: 0.826
                        },
                        {
                            slug: "late_delivery",
                            left: 0.826,
                            right: 0.865
                        },
                        {
                            slug: "item_damaged",
                            left: 0.865,
                            right: 0.909
                        },
                        {
                            slug: "delivered_to_wrong_address",
                            left: 0.909,
                            right: 0.957
                        },
                        {
                            slug: "never_received_delivery",
                            left: 0.974,
                            right: 0.996
                        },
                    ],
                columns2: [
                        {
                            slug: "rank",
                            left: 0,
                            right: 0.032
                        },
                        {
                            slug: "transporter_id",
                            left: 0.032,
                            right: 0.110
                        },
                        {
                            slug: "driver_name",
                            left: 0.110,
                            right: 0.186
                        },
                        {
                            slug: "da_tier",
                            left: 0.186,
                            right: 0.231
                        },
                        {
                            slug: "cdf_score",
                            left: 0.231,
                            right: 0.263
                        },
                        {
                            slug: "no_feedback",
                            left: 0.263,
                            right: 0.303
                        },
                        {
                            slug: "delivery_was_great",
                            left: 0.303,
                            right: 0.348
                        },
                        {
                            slug: "respectful_of_property",
                            left: 0.348,
                            right: 0.403
                        },
                        {
                            slug: "followed_instructions",
                            left: 0.403,
                            right: 0.451
                        },
                        {
                            slug: "friendly",
                            left: 0.451,
                            right: 0.491
                        },
                        {
                            slug: "went_above_and_beyond",
                            left: 0.491,
                            right: 0.534
                        },
                        {
                            slug: "delivered_with_care",
                            left: 0.534,
                            right: 0.582
                        },
                        {
                            slug: "delivery_was_not_so_great",
                            left: 0.582,
                            right: 0.625
                        },
                        {
                            slug: "mishandled_package",
                            left: 0.625,
                            right: 0.679
                        },
                        {
                            slug: "driver_unprofessional",
                            left: 0.679,
                            right: 0.730
                        },
                        {
                            slug: "not_delivered_to_preferred_location",
                            left: 0.730,
                            right: 0.787
                        },
                        {
                            slug: "on_time",
                            left: 0.787,
                            right: 0.816
                        },
                        {
                            slug: "late_delivery",
                            left: 0.816,
                            right: 0.857
                        },
                        {
                            slug: "item_damaged",
                            left: 0.857,
                            right: 0.904
                        },
                        {
                            slug: "delivered_to_wrong_address",
                            left: 0.904,
                            right: 0.954
                        },
                        {
                            slug: "never_received_delivery",
                            left: 0.954,
                            right: 1
                        },
                    ]
                }
            ]
        },
        {
            start: 202212,
            fields: [
                {
                    key: "week",
                    instance: 1,
                    relativePos: 0,
                    regex: "(Week) (\\d\\d?)$",
                    regexGroup: 1,
                    pattern: "Week",
                    transform: "patternMatches[2]",
                }
            ],
            tables: [
                {
                    table_pattern: [
                        "rank",
                        "transporter_id",
                        "da_name",
                        "da_tier",
                        "cdf",
                        "score",
                        "feedback",
                        "friendly",
                        "delivery",
                        "was",
                        "great"
                    ],
                    table_match_percent: .8,
                    table_data: [],
                    min_data: 8,
                    columns: [
                        {
                            slug: "rank",
                            left: 0.050,
                            right: 0.080
                        },
                        {
                            slug: "transporter_id",
                            left: 0.080,
                            right: 0.154
                        },
                        {
                            slug: "driver_name",
                            left: 0.154,
                            right: 0.226
                        },
                        {
                            slug: "da_tier",
                            left: 0.226,
                            right: 0.268
                        },
                        {
                            slug: "cdf_score",
                            left: 0.268,
                            right: 0.299
                        },
                        {
                            slug: "no_feedback",
                            left: 0.299,
                            right: 0.337
                        },
                        {
                            slug: "delivery_was_great",
                            left: 0.337,
                            right: 0.381
                        },
                        {
                            slug: "respectful_of_property",
                            left: 0.381,
                            right: 0.432
                        },
                        {
                            slug: "followed_instructions",
                            left: 0.432,
                            right: 0.477
                        },
                        {
                            slug: "friendly",
                            left: 0.477,
                            right: 0.516
                        },
                        {
                            slug: "went_above_and_beyond",
                            left: 0.516,
                            right: 0.557
                        },
                        {
                            slug: "delivered_with_care",
                            left: 0.557,
                            right: 0.603
                        },
                        {
                            slug: "delivery_was_not_so_great",
                            left: 0.603,
                            right: 0.643
                        },
                        {
                            slug: "mishandled_package",
                            left: 0.643,
                            right: 0.695
                        },
                        {
                            slug: "driver_unprofessional",
                            left: 0.695,
                            right: 0.744
                        },
                        {
                            slug: "not_delivered_to_preferred_location",
                            left: 0.744,
                            right: 0.799
                        },
                        {
                            slug: "on_time",
                            left: 0.799,
                            right: 0.826
                        },
                        {
                            slug: "late_delivery",
                            left: 0.826,
                            right: 0.865
                        },
                        {
                            slug: "item_damaged",
                            left: 0.865,
                            right: 0.909
                        },
                        {
                            slug: "delivered_to_wrong_address",
                            left: 0.909,
                            right: 0.957
                        },
                        {
                            slug: "never_received_delivery",
                            left: 0.974,
                            right: 0.996
                        },
                    ],
                columns2: [
                        {
                            slug: "rank",
                            left: 0,
                            right: 0.032
                        },
                        {
                            slug: "transporter_id",
                            left: 0.032,
                            right: 0.109
                        },
                        {
                            slug: "driver_name",
                            left: 0.109,
                            right: 0.186
                        },
                        {
                            slug: "da_tier",
                            left: 0.186,
                            right: 0.230
                        },
                        {
                            slug: "cdf_score",
                            left: 0.230,
                            right: 0.262
                        },
                        {
                            slug: "no_feedback",
                            left: 0.262,
                            right: 0.303
                        },
                        {
                            slug: "delivery_was_great",
                            left: 0.303,
                            right: 0.348
                        },
                        {
                            slug: "respectful_of_property",
                            left: 0.348,
                            right: 0.402
                        },
                        {
                            slug: "followed_instructions",
                            left: 0.402,
                            right: 0.450
                        },
                        {
                            slug: "friendly",
                            left: 0.450,
                            right: 0.491
                        },
                        {
                            slug: "went_above_and_beyond",
                            left: 0.491,
                            right: 0.534
                        },
                        {
                            slug: "delivered_with_care",
                            left: 0.534,
                            right: 0.582
                        },
                        {
                            slug: "delivery_was_not_so_great",
                            left: 0.582,
                            right: 0.624
                        },
                        {
                            slug: "mishandled_package",
                            left: 0.624,
                            right: 0.679
                        },
                        {
                            slug: "driver_unprofessional",
                            left: 0.679,
                            right: 0.731
                        },
                        {
                            slug: "not_delivered_to_preferred_location",
                            left: 0.731,
                            right: 0.788
                        },
                        {
                            slug: "on_time",
                            left: 0.788,
                            right: 0.817
                        },
                        {
                            slug: "late_delivery",
                            left: 0.817,
                            right: 0.858
                        },
                        {
                            slug: "item_damaged",
                            left: 0.858,
                            right: 0.904
                        },
                        {
                            slug: "delivered_to_wrong_address",
                            left: 0.904,
                            right: 0.954
                        },
                        {
                            slug: "never_received_delivery",
                            left: 0.954,
                            right: 1
                        },
                    ]
                }
            ]
        },
        {
            start: 202211,
            fields: [
                {
                    key: "week",
                    instance: 1,
                    relativePos: 0,
                    regex: "(Week) (\\d\\d?)$",
                    regexGroup: 1,
                    pattern: "Week",
                    transform: "patternMatches[2]",
                }
            ],
            tables: [
                {
                    table_pattern: [
                        "transporter_id",
                        "driver_name",
                        "%_positive",
                        "feedback",
                        "%_negative",
                        "feedback",
                        "delivery",
                        "was",
                        "great",
                    ],
                    table_match_percent: .1,
                    table_data: [],
                    min_data: 15,
                    columns: [
                        {
                            slug: "rank",
                            left: 0.064,
                            right: 0.078
                        },
                        {
                            slug: "transporter_id",
                            left: 0.081,
                            right: 0.152
                        },
                        {
                            slug: "driver_name",
                            left: 0.152,
                            right: 0.223
                        },
                        {
                            slug: "da_tier",
                            left: 0.223,
                            right: 0.257
                        },
                        {
                            slug: "cdf_score",
                            left: 0.276,
                            right: 0.302
                        },
                        {
                            slug: "no_feedback",
                            left: 0.324,
                            right: 0.343
                        },
                        {
                            slug: "delivery_was_great",
                            left: 0.378,
                            right: 0.392
                        },
                        {
                            slug: "respectful_of_property",
                            left: 0.428,
                            right: 0.442
                        },
                        {
                            slug: "followed_instructions",
                            left: 0.474,
                            right: 0.483
                        },
                        {
                            slug: "friendly",
                            left: 0.512,
                            right: 0.521
                        },
                        {
                            slug: "went_above_and_beyond",
                            left: 0.551,
                            right: 0.56
                        },
                        {
                            slug: "delivered_with_care",
                            left: 0.591,
                            right: 0.604
                        },
                        {
                            slug: "delivery_was_not_so_great",
                            left: 0.641,
                            right: 0.65
                        },
                        {
                            slug: "mishandled_package",
                            left: 0.691,
                            right: 0.7
                        },
                        {
                            slug: "driver_unprofessional",
                            left: 0.746,
                            right: 0.755
                        },
                        {
                            slug: "not_delivered_to_preferred_location",
                            left: 0.793,
                            right: 0.802
                        },
                        {
                            slug: "on_time",
                            left: 0.824,
                            right: 0.837
                        },
                        {
                            slug: "late_delivery",
                            left: 0.867,
                            right: 0.876
                        },
                        {
                            slug: "item_damaged",
                            left: 0.908,
                            right: 0.917
                        },
                        {
                            slug: "delivered_to_wrong_address",
                            left: 0.95,
                            right: 0.959
                        },
                        {
                            slug: "never_received_delivery",
                            left: 0.991,
                            right: 1
                        },
                    ],
                    columns2: [
                        {
                            slug: "rank",
                            left: 0,
                            right: 0.031
                        },
                        {
                            slug: "transporter_id",
                            left: 0.031,
                            right: 0.106
                        },
                        {
                            slug: "driver_name",
                            left: 0.106,
                            right: 0.180
                        },
                        {
                            slug: "da_tier",
                            left: 0.180,
                            right: 0.227
                        },
                        {
                            slug: "cdf_score",
                            left: 0.227,
                            right: 0.266
                        },
                        {
                            slug: "no_feedback",
                            left: 0.266,
                            right: 0.309
                        },
                        {
                            slug: "delivery_was_great",
                            left: 0.309,
                            right: 0.360
                        },
                        {
                            slug: "respectful_of_property",
                            left: 0.360,
                            right: 0.413
                        },
                        {
                            slug: "followed_instructions",
                            left: 0.413,
                            right: 0.456
                        },
                        {
                            slug: "friendly",
                            left: 0.456,
                            right: 0.496
                        },
                        {
                            slug: "went_above_and_beyond",
                            left: 0.496,
                            right: 0.537
                        },
                        {
                            slug: "delivered_with_care",
                            left: 0.537,
                            right: 0.584
                        },
                        {
                            slug: "delivery_was_not_so_great",
                            left: 0.584,
                            right: 0.631
                        },
                        {
                            slug: "mishandled_package",
                            left: 0.631,
                            right: 0.684
                        },
                        {
                            slug: "driver_unprofessional",
                            left: 0.684,
                            right: 0.741
                        },
                        {
                            slug: "not_delivered_to_preferred_location",
                            left: 0.741,
                            right: 0.791
                        },
                        {
                            slug: "on_time",
                            left: 0.791,
                            right: 0.828
                        },
                        {
                            slug: "late_delivery",
                            left: 0.828,
                            right: 0.866
                        },
                        {
                            slug: "item_damaged",
                            left: 0.866,
                            right: 0.912
                        },
                        {
                            slug: "delivered_to_wrong_address",
                            left: 0.912,
                            right: 0.956
                        },
                        {
                            slug: "never_received_delivery",
                            left: 0.956,
                            right: 1
                        },
                    ]
                }
            ]
        },
        {
            start: 202144,
            fields: [
                {
                    key: "week",
                    instance: 1,
                    relativePos: 0,
                    regex: "(Week) (\\d\\d?)$",
                    regexGroup: 1,
                    pattern: "Week",
                    transform: "patternMatches[2]",
                }
            ],
            tables: [
                {
                    table_pattern: [
                        "transporter_id",
                        "driver_name",
                        "%_positive",
                        "feedback",
                        "%_negative",
                        "feedback",
                        "delivery",
                        "was",
                        "great",
                    ],
                    table_match_percent: .1,
                    table_data: [],
                    min_data: 15,
                    columns: [
                        {
                            slug: "transporter_id",
                            left: 0,
                            right: 0.092
                        },
                        {
                            slug: "driver_name",
                            left: 0.092,
                            right: 0.192
                        },
                        {
                            slug: "%_positive_feedback",
                            left: 0.192,
                            right: 0.246
                        },
                        {
                            slug: "$_negative_feedback",
                            left: 0.246,
                            right: 0.293
                        },
                        {
                            slug: "no_feedback",
                            left: 0.293,
                            right: 0.330
                        },
                        {
                            slug: "delivery_was_great",
                            left: 0.330,
                            right: 0.373
                        },
                        {
                            slug: "respectful_of_property",
                            left: 0.373,
                            right: 0.423
                        },
                        {
                            slug: "followed_instructions",
                            left: 0.423,
                            right: 0.474
                        },
                        {
                            slug: "friendly",
                            left: 0.474,
                            right: 0.515
                        },
                        {
                            slug: "went_above_and_beyond",
                            left: 0.515,
                            right: 0.554
                        },
                        {
                            slug: "delivered_with_care",
                            left: 0.554,
                            right: 0.595
                        },
                        {
                            slug: "delivery_was_not_so_great",
                            left: 0.595,
                            right: 0.640
                        },
                        {
                            slug: "mishandled_package",
                            left: 0.640,
                            right: 0.686
                        },
                        {
                            slug: "driver_unprofessional",
                            left: 0.686,
                            right: 0.737
                        },
                        {
                            slug: "not_delivered_to_preferred_location",
                            left: 0.737,
                            right: 0.798
                        },
                        {
                            slug: "on_time",
                            left: 0.798,
                            right: 0.834
                        },
                        {
                            slug: "late_delivery",
                            left: 0.834,
                            right: 0.874
                        },
                        {
                            slug: "item_damaged",
                            left: 0.874,
                            right: 0.914
                        },
                        {
                            slug: "delivered_to_wrong_address",
                            left: 0.914,
                            right: 0.957
                        },
                        {
                            slug: "never_received_delivery",
                            left: 0.957,
                            right: 1
                        },
                    ]
                }
            ]
        },
        {
            start: 202121,
            fields: [
                {
                    key: "week",
                    instance: 1,
                    relativePos: 0,
                    regex: "(Week) (\\d\\d?)$",
                    regexGroup: 1,
                    pattern: "Week",
                    transform: "patternMatches[2]",
                }
            ],
            tables: [
                {
                    table_pattern: [
                        "transporter_id",
                        "driver_name",
                        "%_positive",
                        "feedback",
                        "%_negative",
                        "feedback",
                        "delivery",
                        "was",
                        "great",
                    ],
                    table_match_percent: .8,
                    table_data: [],
                    min_data: 15,
                    columns: [
                        {
                            slug: "transporter_id",
                            left: 0,
                            right: 0.101
                        },
                        {
                            slug: "driver_name",
                            left: 0.101,
                            right: 0.200
                        },
                        {
                            slug: "%_positive_feedback",
                            left: 0.200,
                            right: 0.261
                        },
                        {
                            slug: "$_negative_feedback",
                            left: 0.261,
                            right: 0.318
                        },
                        {
                            slug: "delivery_was_great",
                            left: 0.318,
                            right: 0.360
                        },
                        {
                            slug: "delivery_was_not_so_great",
                            left: 0.360,
                            right: 0.421
                        },
                        {
                            slug: "total_deliveries_with_customer_feedback",
                            left: 0.421,
                            right: 0.505
                        },
                        {
                            slug: "respectful_of_property",
                            left: 0.505,
                            right: 0.564
                        },
                        {
                            slug: "followed_instructions",
                            left: 0.564,
                            right: 0.629
                        },
                        {
                            slug: "friendly",
                            left: 0.629,
                            right: 0.667
                        },
                        {
                            slug: "went_above_and_beyond",
                            left: 0.667,
                            right: 0.716
                        },
                        {
                            slug: "delivered_with_care",
                            left: 0.716,
                            right: 0.774
                        },
                        {
                            slug: "mishandled_package",
                            left: 0.774,
                            right: 0.840
                        },
                        {
                            slug: "driver_unprofessional",
                            left: 0.840,
                            right: 0.913
                        },
                        {
                            slug: "not_delivered_to_preferred_location",
                            left: 0.913,
                            right: 1.000
                        }
                    ]
                }
            ]
        },
        {
            start: 202029,
            fields: [
                {
                    key: "week",
                    instance: 1,
                    relativePos: 0,
                    regex: "(Week) (\\d\\d?)$",
                    regexGroup: 1,
                    pattern: "Week",
                    transform: "patternMatches[2]",
                }
            ],
            tables: [
                {
                    table_pattern: [
                        "transporter_id",
                        "driver_name",
                        "%_positive",
                        "feedback",
                        "%_negative",
                        "feedback",
                        "delivery",
                        "was",
                        "great",
                    ],
                    table_match_percent: .8,
                    table_data: [],
                    min_data: 16,
                    columns: [
                        {
                            slug: "transporter_id",
                            left: 0,
                            right: 0.086730414
                        },
                        {
                            slug: "driver_name",
                            left: 0.086730414,
                            right: 0.173488788
                        },
                        {
                            slug: "%_positive_feedback",
                            left: 0.173488788,
                            right: 0.225661243
                        },
                        {
                            slug: "$_negative_feedback",
                            left: 0.225661243,
                            right: 0.278001454
                        },
                        {
                            slug: "delivery_was_great",
                            left: 0.278001454,
                            right: 0.315243527
                        },
                        {
                            slug: "delivery_was_not_so_great",
                            left: 0.315243527,
                            right: 0.373497176
                        },
                        {
                            slug: "total_deliveries_with_customer_feedback",
                            left: 0.373497176,
                            right: 0.439761226
                        },
                        {
                            slug: "respectful_of_property",
                            left: 0.439761226,
                            right: 0.49595985
                        },
                        {
                            slug: "followed_instructions",
                            left: 0.49595985,
                            right: 0.547153721
                        },
                        {
                            slug: "friendly",
                            left: 0.547153721,
                            right: 0.580872896
                        },
                        {
                            slug: "went_above_and_beyond",
                            left: 0.580872896,
                            right: 0.629354694
                        },
                        {
                            slug: "delivered_with_care",
                            left: 0.629354694,
                            right: 0.682365934
                        },
                        {
                            slug: "care_for_others",
                            left: 0.682365934,
                            right: 0.729547615
                        },
                        {
                            slug: "mishandled_package",
                            left: 0.729547615,
                            right: 0.787256053
                        },
                        {
                            slug: "driving_unsafely",
                            left: 0.787256053,
                            right: 0.831879439
                        },
                        {
                            slug: "driver_unprofessional",
                            left: 0.831879439,
                            right: 0.898479002
                        },
                        {
                            slug: "not_delivered_to_preferred_location",
                            left: 0.898479002,
                            right: 0.968629425
                        },
                    ]
                }
            ]
        },
        {
            start: 200001,
            fields: [
                {
                    key: "week",
                    instance: 1,
                    relativePos: 0,
                    regex: "(Week) (\\d\\d?)$",
                    regexGroup: 1,
                    pattern: "Week",
                    transform: "patternMatches[2]",
                }
            ],
            tables: [
                {
                    table_pattern: [
                        "transporter_id",
                        "driver_name",
                        "%_positive",
                        "feedback",
                        "%_negative",
                        "feedback",
                        "delivery",
                        "was",
                        "great"
                    ],
                    table_match_percent: .8,
                    table_data: [],
                    min_data: 15,
                    columns: [
                        {
                            slug: "transporter_id",
                            left: 0,
                            right: 0.095144871
                        },
                        {
                            slug: "driver_name",
                            left: 0.095144871,
                            right: 0.190317709
                        },
                        {
                            slug: "%_positive_feedback",
                            left: 0.190317709,
                            right: 0.247538875
                        },
                        {
                            slug: "$_negative_feedback",
                            left: 0.247538875,
                            right: 0.304927844
                        },
                        {
                            slug: "delivery_was_great",
                            left: 0.304927844,
                            right: 0.345913972
                        },
                        {
                            slug: "delivery_was_not_so_great",
                            left: 0.345913972,
                            right: 0.402631726
                        },
                        {
                            slug: "total_deliveries_with_customer_feedback",
                            left: 0.402631726,
                            right: 0.482674236
                        },
                        {
                            slug: "respectful_of_property",
                            left: 0.482674236,
                            right: 0.54915259
                        },
                        {
                            slug: "followed_instructions",
                            left: 0.54915259,
                            right: 0.600332811
                        },
                        {
                            slug: "friendly",
                            left: 0.600332811,
                            right: 0.637249692
                        },
                        {
                            slug: "went_above_and_beyond",
                            left: 0.637249692,
                            right: 0.690275758
                        },
                        {
                            slug: "delivered_with_care",
                            left: 0.690275758,
                            right: 0.745021815
                        },
                        {
                            slug: "mishandled_package",
                            left: 0.745021815,
                            right: 0.80811612
                        },
                        {
                            slug: "driving_unsafely",
                            left: 0.80811612,
                            right: 0.85711489
                        },
                        {
                            slug: "driver_unprofessional",
                            left: 0.85711489,
                            right: 0.926781519
                        },
                        {
                            slug: "not_delivered_to_preferred_location",
                            left: 0.926781519,
                            right: 1
                        },
                    ]
                }
            ]
        }
    ]
}

module.exports = { cxTemplates }