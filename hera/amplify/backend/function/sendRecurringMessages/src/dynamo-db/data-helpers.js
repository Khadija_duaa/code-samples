
const {
    scanDynamoRecords,
    queryDynamoRecords
  } = require("./helper");
  
  // Get All Tenants
  async function getAllTenants() {
    try {
        let tenants = [];
        let lastEvaluatedKey = null;
        do {
            let params = {
                ExpressionAttributeNames: {
                    "#g": "group",
                    "#1": "automatedCoachingSendTime",
                    "#2": "timeZone",
                    "#3": "accountPremiumStatus",
                    "#4": "trialExpDate"
                },
                TableName: process.env.API_HERA_TENANTTABLE_NAME,
                ProjectionExpression: "id, companyName, #g, #1, #2, #3, #4",
                ExclusiveStartKey: lastEvaluatedKey,
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            tenants = tenants.concat(items)
        } while (lastEvaluatedKey);
  
        return tenants;
    } catch (err) {
        console.log("[getAllTenants] Error in function getAllTenants", err);
        return {
            error: err,
        };
    }
  }
  
  // Get Recurring Messages
  async function getRecurringMessages(group) {
    try {
        let recurringMessages = [];
        let lastEvaluatedKey = null
        do{
            let params = {
                ExpressionAttributeValues: {
                    ':g': group,
                },
                    ExpressionAttributeNames: {
                    '#g': 'group',
                },
                KeyConditionExpression: '#g = :g',
                ProjectionExpression: 'id,#g,channelType,messageBody,messageName,sendTime,recurringMessagesTenantId,senderId',
                TableName: process.env.API_HERA_RECURRINGMESSAGESTABLE_NAME,
                IndexName: 'byGroup',
                ExclusiveStartKey: lastEvaluatedKey
            };
            var queryResult = await queryDynamoRecords(params);
            var items = queryResult.Items
            lastEvaluatedKey = queryResult.LastEvaluatedKey
            recurringMessages = [...recurringMessages, ...items]
        }while(lastEvaluatedKey)
  
        return recurringMessages;
    } catch (err) {
        console.log("[getRecurringMessages] Error in function getRecurringMessages", err);
        return {
            error: err,
        };
    }
  }
  
  // Get Daily Rostered
  async function getDailyRosters(group, today) {
    console.log({ group, today });
    try {
        let dailyRosters = []
        let lastEvaluatedKey = null
        do{
            var params = {
                ExpressionAttributeValues: {
                ':g': group,
                ':d': today
                },
                  ExpressionAttributeNames: {
                '#g': 'group',
                '#d': 'notesDate'
                },
                KeyConditionExpression: '#g = :g',
                FilterExpression: '#d = :d',
                ProjectionExpression: 'id',
                TableName: process.env.API_HERA_DAILYROSTERTABLE_NAME,
                IndexName: 'byGroup',
                ExclusiveStartKey: lastEvaluatedKey
            };
            var queryResult = await queryDynamoRecords(params);
            var items = queryResult.Items
            lastEvaluatedKey = queryResult.LastEvaluatedKey
            dailyRosters = [...dailyRosters, ...items]
        }while(lastEvaluatedKey)
  
        return dailyRosters;
    } catch (err) {
        console.log("[getDailyRosters] Error in function getDailyRosters", err);
        return {
            error: err,
        };
    }
  }
  
  // Get Routes
  async function getRoutes(id) {
    try {
        var routeStaff = []
        var lastEvaluatedKey = null
        do{
           
          let params = {
              ExpressionAttributeValues: {
                  ':g': id,
              },
              ExpressionAttributeNames: {
                  '#g': 'routeDailyRosterId',
                  '#s': 'status'
              },
              KeyConditionExpression: '#g = :g',
              ProjectionExpression: 'routeStaffId, #s',
              TableName: process.env.API_HERA_ROUTETABLE_NAME,
              IndexName: 'gsi-RouteDailyRoster',   
            }
            let queryResult = await queryDynamoRecords(params);
            let items = queryResult.Items
            lastEvaluatedKey = queryResult.LastEvaluatedKey
            routeStaff = [...routeStaff, ...items]
        }while(lastEvaluatedKey)
  
        return routeStaff;
    } catch (err) {
        console.log("[getRoutes] Error in function getRoutes", err);
        return {
            error: err,
        };
    }
  }
  
  // Get Replacement by Routes
  async function getReplacementsByRoutes(id) {
    try {
        var replacementByRoutes = []
        var lastEvaluatedKey = null
        do{
  
          let params = {
              ExpressionAttributeValues: {
                  ':g': id,
              },
                  ExpressionAttributeNames: {
                  '#g': 'replaceByRouteDailyRosterId',
                  '#s': 'status'
              },
              KeyConditionExpression: '#g = :g',
              ProjectionExpression: 'replaceByRouteStaffId, #s',
              TableName: process.env.API_HERA_REPLACEBYROUTETABLE_NAME,
              IndexName: 'gsi-ReplaceRouteDailyRoster',   
          }
          
            let queryResult = await queryDynamoRecords(params);
            let items = queryResult.Items
            lastEvaluatedKey = queryResult.LastEvaluatedKey
            replacementByRoutes = [...replacementByRoutes, ...items]
        }while(lastEvaluatedKey)
  
        return replacementByRoutes;
    } catch (err) {
        console.log("[getReplacementsByRoutes] Error in function getReplacementsByRoutes", err);
        return {
            error: err,
        };
    }
  }
  
  
  module.exports = {
    getAllTenants,
    getRecurringMessages,
    getDailyRosters,
    getRoutes,
    getReplacementsByRoutes
  }