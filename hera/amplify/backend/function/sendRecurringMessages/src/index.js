/* Amplify Params - DO NOT EDIT
	API_HERA_DAILYROSTERTABLE_ARN
	API_HERA_DAILYROSTERTABLE_NAME
	API_HERA_GRAPHQLAPIIDOUTPUT
	API_HERA_MESSAGETABLE_ARN
	API_HERA_MESSAGETABLE_NAME
	API_HERA_RECURRINGMESSAGESTABLE_ARN
	API_HERA_RECURRINGMESSAGESTABLE_NAME
	API_HERA_REPLACEBYROUTETABLE_ARN
	API_HERA_REPLACEBYROUTETABLE_NAME
	API_HERA_ROUTETABLE_ARN
	API_HERA_ROUTETABLE_NAME
	API_HERA_STAFFTABLE_ARN
	API_HERA_STAFFTABLE_NAME
	API_HERA_TENANTTABLE_ARN
	API_HERA_TENANTTABLE_NAME
	ENV
	REGION
Amplify Params - DO NOT EDIT */

const { generateTemplate } = require('./resource/email-template')
const {
    createDynamoRecord,
    getDynamoRecord
} = require("./dynamo-db/helper");

const {
    getAllTenants,
    getRecurringMessages,
    getDailyRosters,
    getRoutes,
    getReplacementsByRoutes
} = require("./dynamo-db/data-helpers")

// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
// Set the region
AWS.config.update({region: process.env.REGION});

var moment = require('moment-timezone')

/**
 * @type {import('@types/aws-lambda').APIGatewayProxyHandler}
 */
exports.handler = async (event) => {
    console.log(JSON.stringify(event, null, 2));   
    
    // Find tenants configured to send messages at the current time
    var tenants = await getAllTenants();
    console.log({tenants})
    
    // Filters Tenants by Automated Coaching Send Time, Time Zone and Plan
    var filteredTenantsRecurringMessages = await filterTenantsByRecurringMessageTimeAndTz(tenants)
    console.log({ filteredTenantsRecurringMessages });

    await sendRecurringMessage(filteredTenantsRecurringMessages)

};

async function filterTenantsByRecurringMessageTimeAndTz(tenants) {
    let resultRecurringMessageTenant = []

    // loop through found tenants
    for (const tenant of tenants){
        // Tenant's company Name
        const { companyName, group }= tenant
        let resultRecurringMessage = []

        if(!tenant.timeZone){
            console.log(companyName + "'s messages skipped because Time Zone is not selected.");
            continue
        }
        
        var accountPremiumStatus = tenant.accountPremiumStatus
        if(!accountPremiumStatus) accountPremiumStatus = []
        var accountIsTrial = accountPremiumStatus.includes('trial')
        var accountHasPremiumRostering = accountPremiumStatus.includes('rostering') || accountPremiumStatus.includes('bundle')
        var trialExpDate = new Date(tenant.trialExpDate).getTime();

        // skip iteration if account doesn't have premium performance rostering or an active trial
        if((accountIsTrial && trialExpDate < new Date().getTime()) || (!accountIsTrial && !accountHasPremiumRostering)){
            console.log(companyName + "'s messages skipped because account doesn't have premium performance rostering or an active trial.");
            continue
        }
        
        let recurringMessages =  await getRecurringMessages(group)

        for(const recurringMessage of recurringMessages){
            
            if(!recurringMessage.sendTime){
                console.log(companyName + "'s messages skipped because Recurring Message Send Time is not selected.");
                continue
            }
                
            // compare tenant time zone and recurring message send time Local
            const tenantTz = moment.tz(tenant.timeZone).format("HHmm")
            var tenantRecurringMessageSendTime = moment(recurringMessage.sendTime, ["h:mm A"]).format('HHmm')
                
            console.log({tenantTz,tenantRecurringMessageSendTime})
                
            if(parseInt(tenantTz) != parseInt(tenantRecurringMessageSendTime)) continue
            
            resultRecurringMessage.push(recurringMessage)
                      

        }
        
        let output = {
            tenant : tenant,
            resultRecurringMessage : resultRecurringMessage
        }
        
        resultRecurringMessageTenant.push(output)
        
    }
    return resultRecurringMessageTenant
}

async function sendRecurringMessage(TenantsRecurringMessages) {
    
    // loop through found tenants
    for (const tenant of TenantsRecurringMessages){
                
        // Tenant's company Name
        var { companyName, group, timeZone }= tenant.tenant
        var todayMomentTz = moment.tz(timeZone).format('YYYY-MM-DD')
        
        
        // Only if the tenant has scheduled recurring messages
        if(!tenant.resultRecurringMessage?.length > 0){
            console.log(`The tenant '${companyName}' does not have scheduled recurring messages.`);
            continue
        }

        console.log(`Sending messages for "${companyName}"`)

        // only create message if DA is rostered
        try {
            var dailyRosters = await getDailyRosters(group, todayMomentTz)

            if(dailyRosters.length == 0){
                console.log("No daily roster found, skipping tenant " + companyName)
                continue
            }
        
            // Daily Roster ID
            var dailyRosterId = dailyRosters[0].id

        } catch (err) {
            console.log(`[getDailyRosters] Error in function getDailyRosters for Tenant: ${companyName} and group: ${group}`, err);
        }

        var routeStaff = []
        const excludedGroup = [
            'Called Out',
            'No Show, No Call',
            'VTO'
        ]

        // get daily roster id from the response
        try {
            var routes = await getRoutes(dailyRosterId)
            // Filter the routes with status different that the ones above
            routes.map((route) => {
                if(!excludedGroup.includes(route.status)) {
                    routeStaff.push(route.routeStaffId)
                }
            })

            if (routes.length == 0){
                console.log("No routes found, skipping tenant " + companyName)
                continue 
            }

        } catch (err) {
            console.log(`[getRoutes] Error in function getRoutes for Tenant: ${companyName} and group: ${group}`, err);
            continue
        }

        // // get replacements daily roster id from the response
        try {
            var replacementsByRoutes = await getReplacementsByRoutes(dailyRosterId)

            // throw 'ERROR getRoutes'
            replacementsByRoutes.map((replacement) => {
                if(!excludedGroup.includes(replacement.status)) {
                    routeStaff.push(replacement.replaceByRouteStaffId)
                }
            })

            if (!routeStaff.length){
                console.log("No routes with staff found, skipping tenant " + companyName)
                continue 
            }
        } catch (err) {
            console.log(`[getReplacementsByRoutes] Error in function getReplacementsByRoutes for Tenant: ${companyName} and group: ${group}`, err);
            continue
        }

        // Route Staff
        console.log({ routeStaff });
        
        for (const recurringMessage of tenant.resultRecurringMessage){
            
            var countStaff = 0
            
            for (const staffId of routeStaff){
                try {
                    let params = {
                        TableName : process.env.API_HERA_STAFFTABLE_NAME,
                          Key: {
                            "id": staffId
                          }
                    };
                    var staff = await getDynamoRecord(params)
                } catch (err) {
                    console.log(`[getDynamoRecord - Staff] Error in function getDynamoRecord - Staff for Tenant: ${companyName} and group: ${group}`, err);
                    continue
                }
                
                 // create messages from recurring messages
                var body = `${recurringMessage.messageBody}`
                var emailBody = body.replace(/\n/g, '<br>')
                var header = "Recurring Message"
                var subject = `Message from ${companyName}`
                var channelType
                if(staff.Item.receiveTextMessages && staff.Item.receiveEmailMessages && recurringMessage.channelType === 'SMS/EMAIL') channelType = 'SMS/EMAIL';
                else if( staff.Item.receiveTextMessages && (recurringMessage.channelType === 'SMS' || recurringMessage.channelType === 'SMS/EMAIL')) channelType = 'SMS';
                else if( staff.Item.receiveEmailMessages && (recurringMessage.channelType === 'EMAIL' || recurringMessage.channelType === 'SMS/EMAIL')) channelType = 'EMAIL';
                else channelType = ''
                var createdAt = new Date().toISOString()
                var todayMomentId = moment().format()
                countStaff++

                
                // Creating Message
                try {
                    var createParams = {
                        TableName: process.env.API_HERA_MESSAGETABLE_NAME,
                        Item: {
                            id: tenant.tenant.id + todayMomentId + '-' + countStaff + '/' +recurringMessage.id,
                            messageTenantId: tenant.tenant.id,
                            staffId: staffId,
                            createdAt: createdAt,
                            updatedAt: new Date().toISOString(),
                            group: group,
                            subject: subject,
                            bodyText: body,
                            bodyHtml: generateTemplate(header, emailBody, staffId, companyName),
                            channelType: channelType,
                            messageType: 'recurring',
                            isReadS: 'true',
                            'messageType#createdAt': 'recurring#' + createdAt,
                            'channelType#createdAt': channelType + '#' + createdAt,
                            'channelType#isReadS#createdAt': channelType + '#true#' + createdAt,
                            destinationNumber: staff.Item.phone,
                            destinationEmail: staff.Item.email,
                            senderId: recurringMessage.senderId,
                            senderName: recurringMessage.senderName
                        },
                        FilterExpression: "attribute_not_exists(createdAt)"
                    };
                    await createDynamoRecord(createParams);
                } catch (err) {
                    console.log(`[createDynamoRecord - Message]  Error in creating messages from recurring messages, for Tenant: ${companyName} and group: ${group}`, err);
                    continue 
                }
                
            }
        }
    }
}
