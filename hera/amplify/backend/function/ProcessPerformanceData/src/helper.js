
const {
    buildGetItemParams,
    getDynamoRecord,
    buildQueryAllDynamoParams,
    queryAllDynamoRecords,
    customBuildQueryParams
} = require('./dynamodbHelper.js')

const {
    getTableName
} = require('./util.js')

const { Tables } = require('./tables');

const getTenantById = async function(tenantId, fields = []){
    const getItemParams = buildGetItemParams(getTableName(Tables.TENANT_TABLE), fields, tenantId)
    return (await getDynamoRecord(getItemParams)).Item
}

const getTextractJobById = async function(id, fields = []){
    const getItemParams = buildGetItemParams(getTableName(Tables.TEXTRACT_JOB_TABLE), fields, id)
    return (await getDynamoRecord(getItemParams)).Item
}

const getAllJobsByJobId = async function(jobId, fields = []){
    const queryParams = buildQueryAllDynamoParams(getTableName(Tables.TEXTRACT_JOB_TABLE), fields, { jobId }, 'byJobId')
    return await queryAllDynamoRecords(queryParams)
}

const getAllAssociatesByGroup = async function(group, fields = []){
    const queryParams = buildQueryAllDynamoParams(getTableName(Tables.STAFF_TABLE), fields, { group }, 'byGroup')
    return await queryAllDynamoRecords(queryParams)
}

const getPendingMessageById = async function(id, fields = []){
    const getItemParams = buildGetItemParams(getTableName(Tables.PENDING_MESSAGE_TABLE), fields, id)
    return (await getDynamoRecord(getItemParams)).Item
}

const getAllVehiclesByGroup = async function(group, fields = []){
    const queryParams = buildQueryAllDynamoParams(getTableName(Tables.VEHICLE_TABLE), fields, { group }, 'byGroup')
    return await queryAllDynamoRecords(queryParams)
}

const getAllDevicesByGroup = async function(group, fields = []){
    const queryParams = buildQueryAllDynamoParams(getTableName(Tables.DEVICE_TABLE), fields, { group }, 'byGroup')
    return await queryAllDynamoRecords(queryParams)
}

const getAllCustomListsByGroup = async function(group, fields = []){
    const queryParams = buildQueryAllDynamoParams(getTableName(Tables.CUSTOM_LISTS_TABLE), fields, { group }, 'byGroup')
    return await queryAllDynamoRecords(queryParams)
}

const getAllOptionsCustomListsByGroup = async function(group, fields = []){
    const queryParams = buildQueryAllDynamoParams(getTableName(Tables.OPTIONS_CUSTOM_LISTS_TABLE), fields, { group }, 'byGroup')
    return await queryAllDynamoRecords(queryParams)
}

const getAllOptionsCustomListsByCustomListsId = async function(optionsCustomListsCustomListsId, fields = []){
    const queryParams = buildQueryAllDynamoParams(getTableName(Tables.OPTIONS_CUSTOM_LISTS_TABLE), fields, { optionsCustomListsCustomListsId }, 'gsi-OptionsCustomLists')
    return await queryAllDynamoRecords(queryParams)
}

const getAllValueListByGroup = async function(group, fields = []){
    const queryParams = buildQueryAllDynamoParams(getTableName(Tables.VALUE_LIST_TABLE), fields, { group }, 'byGroup')
    return await queryAllDynamoRecords(queryParams)
}

const getAllValueListItemByValueListId = async function(valueListItemValueListId, fields = []){
    const queryParams = buildQueryAllDynamoParams(getTableName(Tables.VALUE_LIST_ITEM_TABLE), fields, { valueListItemValueListId }, 'gsi-ValueListItems')
    return await queryAllDynamoRecords(queryParams)
}

const getCustomListByGroupAndType = async function(group, type, fields=[]){
    const queryParams = customBuildQueryParams(getTableName(Tables.CUSTOM_LISTS_TABLE), fields, { group, type }, 'byType')
    return await queryAllDynamoRecords(queryParams)
}

/**
 * Finds the index of the searched element on the specified array
 * @param {Array} arr - List of values(primitives|objects) from which to search
 * @param {Function} compareCb - Callback to perform the comparisson for the searched value against the array items,
 *  below are the values the callback should return
 *  -  0: If the value is found and no more comparissons are needed
 *  -  1: If the search should be performed on the right side of the array
 *  - -1: If the search should be performed on the left side of the arrray
 * @returns {Number} the index at which the searched value was found, or -1 if not found
 */
const findIndexByBinarySearch = (arr, compareCb) => {
    let [start, end] = [0, arr.length-1];
    let middle;
    while(start <= end) {
        middle = Math.floor((start + end) / 2);
        const value = compareCb(arr[middle]);
        if (value === 0) return middle;
        if (value > 0) { start = middle + 1; }
        else { end = middle - 1; }
    }
    return -1;
};

module.exports = {
    getTenantById,
    getTextractJobById,
    getAllJobsByJobId,
    getAllAssociatesByGroup,
    getPendingMessageById,
    getAllVehiclesByGroup,
    getAllDevicesByGroup,
    getAllCustomListsByGroup,
    getAllOptionsCustomListsByGroup,
    getAllOptionsCustomListsByCustomListsId,
    getAllValueListByGroup,
    getAllValueListItemByValueListId,
    getCustomListByGroupAndType,
    findIndexByBinarySearch,
}