const createNotification = /* GraphQL */ `
  mutation CreateNotification(
    $input: CreateNotificationInput!
    $condition: ModelNotificationConditionInput
  ) {
    createNotification(input: $input, condition: $condition) {
      id
      group
      owner
      title
      createdAt
      description
      releaseNotes
      payload
      clickAction
      isRead
      updatedAt
    }
  }
`;

const getTenant = /* GraphQL */ `
  query GetTenant($id: ID!) {
    getTenant(id: $id) {
      id
      originationNumber
      messageServiceProvider
      companyName
    }
  }
`;

const createCoachingHistory = /* GraphQL */ `
  mutation CreateCoachingHistory(
    $input: CreateCoachingHistoryInput!
    $condition: ModelCoachingHistoryConditionInput
  ) {
    createCoachingHistory(input: $input, condition: $condition) {
      id
      group
      type
      importId
      results
      createdAt
      updatedAt
    }
  }
`;

const listCoachingHistorys = /* GraphQL */ `
  query ListCoachingHistorys(
    $filter: ModelCoachingHistoryFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listCoachingHistorys(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        type
        importId
        results
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;

const createCoachingRecords = /* GraphQL */ `
  mutation CreateCoachingRecords(
    $input: CreateCoachingRecordsInput!
    $condition: ModelCoachingRecordsConditionInput
  ) {
    createCoachingRecords(input: $input, condition: $condition) {
      id
      group
      importId
      type
      results
      createdAt
      updatedAt
    }
  }
`; 

const updateCoachingHistory = /* GraphQL */ `
  mutation UpdateCoachingHistory(
    $input: UpdateCoachingHistoryInput!
    $condition: ModelCoachingHistoryConditionInput
  ) {
    updateCoachingHistory(input: $input, condition: $condition) {
      id
      group
      type
      importId
      results
      createdAt
      updatedAt
    }
  }
`;


//for kudo Subscriptions
const kudoFragment = /* GraphQL */ `
  fragment kudoFragment on Kudo {
    id
    group
    staffId
    kudoType
    date
    type {
      id
      option
    }
  }
`;

//for infraction Subscriptions
const infractionFragment = /* GraphQL */ `
  fragment infractionFragment on Infraction {
    id
    group
    staffId
    infractionType
    date
    type {
      id
      option
    }
  }
`;

const createKudo = kudoFragment + /* GraphQL */ `
  mutation CreateKudo(
    $input: CreateKudoInput!
    $condition: ModelKudoConditionInput
  ) {
    createKudo(input: $input, condition: $condition) {
      ...kudoFragment
    }
  }
`;

const updateKudo = kudoFragment + /* GraphQL */ `
  mutation UpdateKudo(
    $input: UpdateKudoInput!
    $condition: ModelKudoConditionInput
  ) {
    updateKudo(input: $input, condition: $condition) {
      ...kudoFragment
    }
  }
`;

const deleteKudo = kudoFragment + /* GraphQL */ `
  mutation DeleteKudo(
    $input: DeleteKudoInput!
    $condition: ModelKudoConditionInput
  ) {
    deleteKudo(input: $input, condition: $condition) {
      ...kudoFragment
    }
  }
`;

const createInfraction = infractionFragment + /* GraphQL */ `
  mutation CreateInfraction(
    $input: CreateInfractionInput!
    $condition: ModelInfractionConditionInput
  ) {
    createInfraction(input: $input, condition: $condition) {
      ...infractionFragment
    }
  }
`;

const updateInfraction = infractionFragment + /* GraphQL */ `
  mutation UpdateInfraction(
    $input: UpdateInfractionInput!
    $condition: ModelInfractionConditionInput
  ) {
    updateInfraction(input: $input, condition: $condition) {
      ...infractionFragment
    }
  }
`;

const deleteInfraction = infractionFragment + /* GraphQL */ `
  mutation DeleteInfraction(
    $input: DeleteInfractionInput!
    $condition: ModelInfractionConditionInput
  ) {
    deleteInfraction(input: $input, condition: $condition) {
      ...infractionFragment
    }
  }
`;

module.exports = {
  createNotification,
  getTenant,
  createCoachingHistory,
  listCoachingHistorys,
  createCoachingRecords,
  updateCoachingHistory,
  createKudo,
  updateKudo,
  deleteKudo,
  createInfraction,
  updateInfraction,
  deleteInfraction
};