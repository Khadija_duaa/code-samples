/* Amplify Params - DO NOT EDIT
	API_HERA_GRAPHQLAPIENDPOINTOUTPUT
	API_HERA_GRAPHQLAPIIDOUTPUT
	ENV
	REGION
	STORAGE_HERA_BUCKETNAME
Amplify Params - DO NOT EDIT */
const { customCsvHandler } = require('./customCsvImport/handler.js')
const { dailyHandler } = require('./dailyPerformance/handler.js')
const { weeklyHandler } = require('./weeklyPerformance/handler.js')

exports.handler = async ($event, context, callback) => {

    console.log(`EVENT: ${JSON.stringify($event)}`);

    let event;

    try {
        if(typeof $event.body === 'string'){
            event = JSON.parse($event.body)
            console.info('using $event.body')
        }else{
            event = $event
            console.info('using event')
        }

        if(!event) throw new Error('The event is undefined')
        if(!event.importType) throw new Error('The importType is undefined')

    } catch (error) {
        const { pushToCloudWatch } = require('./cloudWatchLogger')
        const loggerBody = {
            'ErrorCode': 'ERROR_003',
            'Title': `There was an error importing your file.`,
            'Severity': 'Fatal',
            'Reason': error,
        }
        pushToCloudWatch.error(loggerBody)
        const response = {
            statusCode: 200,
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "*"
            }, 
            body: JSON.stringify('failes'),
        };
        return response;
    }

    switch(event.importType){
        case 'customCsv': return await customCsvHandler(event, context, callback)
        case 'daily': return await dailyHandler(event, context, callback)
        case 'weekly': return await weeklyHandler(event, context, callback)
    }

};
