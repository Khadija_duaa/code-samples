const { pushToCloudWatch } =  require("./cloudWatchLogger")

const metricDimension = process.env.ENV

async function setCloudWatchMetrics(metrics, cwm) {
  
	let { 
		functionName,
		codeText,
		codeNumber,
		tenant,
		err,
		user,
		associate,
		issue
	} = metrics
	
	const loggerBody = {
		'Title': `[${functionName}] Error in function ${functionName}.`,
		'Function': `${functionName}`,
		'Severity': 'Moderate',
		'Tenant': tenant.companyName,
		'TenantId': tenant.id,
		'Group': tenant.group,
		'UserId': user.id,
		'UserName': user.userName,
		'AssociateId': associate.id,
		'AssociateName': associate.name,
		'IssueId': issue.id,
		'CounselingId': issue.counselingId,
		'Error': err
	}

	const errorCode = `${codeText}_DELETE_ISSUE_FAILED (${codeNumber})`

	const body = {
			ErrorCode: errorCode,
			...loggerBody,
			'Description': `${errorCode} was due to the following error: ${err}`
	}

	pushToCloudWatch.error(body)
	cwm.deleteIssue.metricGroup1({
			byError: metricDimension
	}).pushMetricData({
			MetricName: errorCode,
			Value: 1,
			Unit: 'Count'
	})

	cwm.deleteIssue.metricGroup2({
			bySeverity: metricDimension
	}).pushMetricData({
			MetricName: 'Fatal',
			Value: 1,
			Unit: 'Count'
	})

	await cwm.sendToCloudWatch()
}

module.exports = {
  setCloudWatchMetrics
}