const {
    cleanInput,
    cleanTransporterId,
    keyOf,
    AWSDateToDate
} = require('../util.js')

const {
    KEY_ASSOCIATE_NAME,
    KEY_TRANSPORTER_ID,
    KEY_ASSOCIATE_EMAIL,
    KEY_DSP_SHORT_CODE,
    getCustomColumns
} = require('./columnNames')

const colNoName = function(n){
    const ordA = 'A'.charCodeAt(0);
    const ordZ = 'Z'.charCodeAt(0);
    const len = ordZ - ordA + 1;
    let s = "";
    while(n >= 0) {
        s = String.fromCharCode(n % len + ordA) + s;
        n = Math.floor(n / len) - 1;
    }
    return s + '(No Name)';
}

const mergeColumnsFrom_Header_A_algorithm = function(columns){
    const header = []
    const body = []
    columns.forEach((sheet)=>{
        const newHeader = sheet.header.filter(column => !header.includes(column) )
        header.push(...newHeader)
        const unnamed = sheet.unnamedColumns.map( (column) => {
            const index = header.indexOf(column)
            const label = colNoName(index)
            header[index] = label
            return { name: column, label}
        })
        const safeRow = sheet.body.map( row =>{
            unnamed.forEach(column=>{
                const columnName = row[column.name]
                row[column.label] = columnName
                delete row[column.name]
            })
            return row
        })
        body.push(...safeRow)
    })
    return {header, body}
}

const shortCodeColumn = function(matchToDailyRosterColumn){
    const [key] = Object.entries(matchToDailyRosterColumn || {})
        .find(([key, value]) => value.userMatch === KEY_DSP_SHORT_CODE) || []
    if(key) return key
}

const getColumnTableData = function(columns){
    const mergedColumns = mergeColumnsFrom_Header_A_algorithm(columns)

    const rosterColumnNames = getCustomColumns()

    const columnsAlreadyMatched = {}

    const matchToDailyRosterColumn = mergedColumns.header.reduce((row, column) => {
        const importedColumn = column.toLowerCase()
        const match = rosterColumnNames.find( rosterColumn => {
            if(rosterColumn.label.toLowerCase() === importedColumn)
                return true
            return rosterColumn.suggestedColumnsToMatch && rosterColumn.suggestedColumnsToMatch.includes(importedColumn)
        })
        row[column] = {}
        if(match){
            const autoMatch = match.label
            if(!columnsAlreadyMatched[autoMatch]){
                row[column].automaticMatch = autoMatch
                row[column].userMatch = autoMatch
                columnsAlreadyMatched[autoMatch] = 1
            }
        }
        return row
    },{})

    const columnTableData = {
        header: mergedColumns.header,
        data: [
        {
            matchToDailyRosterColumn: true,
            ...matchToDailyRosterColumn
        },
        ...mergedColumns.body
        ]
    }
    return columnTableData
}

const processTableData = function(columnTableData){
    const [header, ...body] = columnTableData.data
    const newHeader = []
    const newBody = body.map(row=>{
        let newRow = {}
        Object.entries(row).map(([key, value])=>{
            if(!key || !header[key]) return
            const newKey = header[key].userMatch
            if(newKey){
                newHeader.push(newKey)
                newRow[newKey] = value
            }
        })
        return newRow
    })
    return {columns: newHeader, data: newBody}
}

const matchStaff = (Val, key) => staff => {
    if(key){
        return staff[key] === Val[key]
    }
    if(!Val.importNameKey && Val.importName === '(No Name Imported)'){
        return Val.importTransporterId ? staff.importTransporterId === Val.importTransporterId : false
    }
    return (staff.id === Val.id && !!Val.id) ||
        (staff.matchTransporterId === Val.matchTransporterId && !!Val.matchTransporterId)||
        staff.importNameKey === Val.importNameKey
}

const compileCustomCsv = function(succeededFile, storage, proxy, matchedStaffs){

    const matchStaffBy = proxy.matchStaffBy
    const customCsvData = storage.data.customCsvData
    const matchOptions = proxy.rosterColumnNames
        .filter(col=>col.label)

    const getValue = (row, matchBy) => (row[matchOptions.find(option=>option.label === matchBy).label] || '').trim()
    proxy.mergedData.data.forEach(row=>{
        row.date = AWSDateToDate(succeededFile.date)
        row.textractJobId = succeededFile.id
        let previouslyMatched = false
        let associateMatch;
        let suggestionFound = false
        let badNames = ['not available', 'coming soon', 'unknown driver', 'null']
        let driverName = getValue(row, KEY_ASSOCIATE_NAME)
        let transporterId = cleanTransporterId(getValue(row, KEY_TRANSPORTER_ID))
        let email = getValue(row, KEY_ASSOCIATE_EMAIL)
        let nameKey = ''
        switch(matchStaffBy){
            case KEY_ASSOCIATE_NAME: {
                    const nameSplit = driverName.split(" ")
                    const firstName = cleanInput(nameSplit[0].toLowerCase())
                    const lastName  = cleanInput(nameSplit[nameSplit.length - 1].toLowerCase())

                    // use name on suggested db record as hash key if matched
                    associateMatch = proxy.associates.find(associate =>{
                        return (cleanInput(associate.firstName) == firstName) && (cleanInput(associate.lastName) == lastName)
                    })
                    // mark as previously matched
                    // use name on matched db record as hash key if matched
                    if(associateMatch){
                        previouslyMatched = true
                        nameKey = associateMatch.firstName + associateMatch.lastName
                        transporterId = associateMatch.transporterId
                        email = associateMatch.email
                    }
                    // look for a suggestion if not matched
                    // find available match based on list of saved alt names
                    else{
                        associateMatch = proxy.associates.find(associate => {
                            return associate.alternateNames && associate.alternateNames.some(altName => {
                                return altName.toLowerCase() == driverName.toLowerCase()
                            })
                        })
                        if(associateMatch){
                            suggestionFound = true
                            proxy.suggestionFound && proxy.suggestionFound(true)
                            nameKey = associateMatch.firstName + associateMatch.lastName
                            transporterId = associateMatch.transporterId
                            email = associateMatch.email
                        }
                        previouslyMatched = false
                    }
                }
                break
            
            case KEY_TRANSPORTER_ID:{

                    // find available match based on exact transporter ID
                    if(proxy.associateLookupMap[transporterId]){
                        associateMatch = proxy.associateLookupMap[transporterId]
                        previouslyMatched = true
                        driverName = associateMatch.firstName + ' ' + associateMatch.lastName
                        nameKey = driverName
                        email = associateMatch.email
                    }
                }
                break
            
            case KEY_ASSOCIATE_EMAIL:{
                    if(email){
                        associateMatch = proxy.associates.find(associate =>{
                            return email === associate.email
                        })
                    }
                    if(associateMatch){
                        previouslyMatched = true
                        driverName = associateMatch.firstName + ' ' + associateMatch.lastName
                        nameKey = associateMatch.firstName + associateMatch.lastName
                        transporterId = associateMatch.transporterId
                    }
                }
                break
        }
                
        if(!driverName || badNames.includes(driverName.toLowerCase())) {
            driverName = "(No Name Imported)"
        }

        if(!associateMatch){
            // use name from import as hash key if no match or suggestion found in the db
            nameKey = driverName
            associateMatch = {id: null}
        }

        nameKey = keyOf(nameKey)
        let associateId = associateMatch.id
        let importTransporterId = transporterId ? cleanTransporterId(transporterId) : null
        let routeAssignment = {} 
        if(matchedStaffs){
            let alreadyMatched
            let assignmentType = ['notYetRostered', 'helper','rescuer','replaceBy','route'].find( key => {
                alreadyMatched = (matchedStaffs[key]||[]).find( staff => {
                    if(staff.importNameKey && nameKey) return staff.importNameKey === nameKey
                    if(staff.importTransporterId && transporterId) return staff.importTransporterId === transporterId
                })
                if(alreadyMatched){
                    return key
                }
            })

            if(!assignmentType) return

            const associateFound = proxy.associates.find(a => a.id === alreadyMatched.associateId )
            if(associateFound){
                if(!associateId){
                    associateId = associateFound.id
                }
                associateMatch = associateFound
                transporterId = associateFound.transporterId
                nameKey = alreadyMatched.importNameKey
                importTransporterId = alreadyMatched.importTransporterId
                
                routeAssignment.assignmentType = assignmentType
                alreadyMatched.routeId && (routeAssignment.routeId =  alreadyMatched.routeId)
                alreadyMatched.replaceById && (routeAssignment.replaceById =  alreadyMatched.replaceById)
                alreadyMatched.helperId && (routeAssignment.helperId =  alreadyMatched.helperId)
                alreadyMatched.rescuerId && (routeAssignment.rescuerId =  alreadyMatched.rescuerId)
            }
        }

        const previuosRegisteredDa = customCsvData.find(matchStaff({id: associateId, matchTransporterId: transporterId, importNameKey: nameKey, importName: driverName }))

        if(previuosRegisteredDa){
            previuosRegisteredDa.importData.push(row)
        }else{
            const associate = {
                importName: driverName,
                importNameKey: nameKey,
                importTransporterId,
                id: associateId,
                matchFirstName: associateMatch.firstName,
                matchLastName: associateMatch.lastName,
                matchTransporterId: transporterId,
                matchEmail: email,
                matchStatus: associateMatch.status,
                suggestionFound: suggestionFound,
                previouslyMatched: previouslyMatched,
                routeAssignment,
                importData: [row]
            }
            customCsvData.push(associate)
        }
        
    })
}

const getTableData = function(storage, checkRosterAssignment, shortCodeFilter){
    const {customCsvData} = storage.data
    // combine hash maps into array
    const table = [[],[]]
    if(!Object.keys(customCsvData).length) return table

    // loop through any remaining eoc data
    for(const csvRow of customCsvData){
        const {importData, ...staff} = csvRow
        if(shortCodeFilter && ( !importData || !importData.some(shortCodeFilter) )){
            continue
        }
        const prevMatch = staff.previouslyMatched
        checkRosterAssignment && checkRosterAssignment(staff)
        let tableRow = {
            id: csvRow.id,
            staff: staff,
            previouslyMatched: prevMatch
        }
        tableRow.customCsvData = [shortCodeFilter ? importData.filter(shortCodeFilter):importData]
        if(prevMatch)
            table[1].push(tableRow)
        else
            table[0].push(tableRow)
    }
    return table
}



module.exports = { mergeColumnsFrom_Header_A_algorithm, getColumnTableData, processTableData, matchStaff, compileCustomCsv, getTableData, shortCodeColumn }