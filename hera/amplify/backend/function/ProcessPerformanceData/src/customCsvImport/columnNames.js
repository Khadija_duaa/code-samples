const KEY_ASSOCIATE_NAME = 'Associate Name'
const KEY_TRANSPORTER_ID = 'Associate Transporter ID'
const KEY_ASSOCIATE_EMAIL = 'Associate Email'
const KEY_VEHICLE = 'Vehicle'
const KEY_VEHICLE_TYPE = 'Vehicle Type'
const KEY_PARKING_SPACE = 'Parking Space'
const KEY_ROSTER_STATUS = 'Roster Status'
const KEY_DSP_SHORT_CODE = 'DSP Short Code'
const KEY_DEVICE = 'Device'
const KEY_TIME = 'Time'
const KEY_ROUTE_NUMBER = 'Route'
const KEY_STAGING = 'Staging'
const KEY_NOTES = 'Notes'

const dspShortcodePopover = {
    message:(proxy)=> `Hera has detected that this column contains DSP Short Codes, and will automatically filter out any rows that do not contain your company’s Short Code (<b>${proxy.tenant.shortCode}</b>) so that you don’t accidentally import other DSPs’ routes.`,
    symbol: 'i',
    symbolClass: 'custom-primary uil uil-info',
    popperClass: 'popover-style popover-style-info'
}
const vehiclePopover = {
    message: "Because this column is tied to your Vehicles in Hera, your imported vehicle name must exactly match the name of a Vehicle in Hera. Values that don't match any of your Vehicles in Hera will be ignored during the import, but the rest of the row will still be imported if possible.",
    symbol: '?',
    symbolClass: 'el-badge__content--warning uil uil-question',
    popperClass: 'popover-style popover-style-warning'
}
const devicePopover = {
    message: "Because this column is tied to your Devices in Hera, your imported device name must exactly match the name of a Device in Hera. Values that don't match any of your Devices in Hera will be ignored during the import, but the rest of the row will still be imported if possible.",
    symbol: '?',
    symbolClass: 'el-badge__content--warning uil uil-question',
    popperClass: 'popover-style popover-style-warning'
}
const rosterStatusAndParkingSpacePopover = {
    message: "Because this column is tied to a Drop-Down List in Hera, your imported data must exactly match the options that are shown in “Company Settings”. Values that don't match the options in Hera will be ignored during the import, but the rest of the row will still be imported if possible.",
    symbol: '?',
    symbolClass: 'el-badge__content--warning uil uil-question',
    popperClass: 'popover-style popover-style-warning'
}

const { parseTimeValue } = require('../util')

const getCustomColumns = () =>{
    return [
        /**
         * {
         *  label: @type {String} <Column Name>,
         *  suggestedColumnsToMatch: @type {String[]} <Alternate Column Names>,
         *  popover: @type {String?} <Tooltip Text>, 
         *  parseValue: @type {Function?} <Format Function (@param {String} value * @return {String})>
         * },
         */
        {
            label: KEY_ASSOCIATE_NAME,
            suggestedColumnsToMatch: ['da', 'da name', 'delivery associate', 'associate', 'staff', 'staff name', 'employee', 'type']
        },
        {
            label: KEY_TRANSPORTER_ID,
            suggestedColumnsToMatch: ['transporter', 'transporter id', 'da transporter', 'da transporter id', 'associate transporter', 'dispatch order', 'tid', 'associate id', 'da id']
        },
        {
            label: KEY_ASSOCIATE_EMAIL,
            suggestedColumnsToMatch: ['email', 'da email', 'email address', 'da email address', 'email-address', 'da email-address', 'e-mail', 'da e-mail', 'e-mail-address', 'associate email','da’s email',"da's email",'associate’s email',"associate's email"]
        },
        {
            label: KEY_VEHICLE,
            suggestedColumnsToMatch: ['vehicle name', 'vehicle id', 'van', 'van name', 'van id'],
            popover: vehiclePopover
        },
        // {
        //     label: KEY_VEHICLE_TYPE,
        //     suggestedColumnsToMatch: ['service type']
        // }, 
        {
            label: KEY_DEVICE,
            suggestedColumnsToMatch: ['device name', 'device id', 'rabbit', 'rabbit name', 'rabbit id', 'phone', 'phone name', 'phone id'],
            popover: devicePopover
        }, 
        {
            label: KEY_PARKING_SPACE,
            suggestedColumnsToMatch: ['parking', 'park', 'parking spot'],
            popover: rosterStatusAndParkingSpacePopover
        },
        {
            label: KEY_ROSTER_STATUS,
            suggestedColumnsToMatch: ['status'],
            popover: rosterStatusAndParkingSpacePopover
        },
        {
            label: KEY_DSP_SHORT_CODE,
            suggestedColumnsToMatch: ['dsp', 'assigned dsp', 'suggested dsp'],
            popover: dspShortcodePopover
        },
        {
            label: KEY_TIME,
            suggestedColumnsToMatch: ['start time', 'scheduled time', 'roster time', 'rostered time', 'wave', 'dispatch time'],
            parseValue: parseTimeValue
        },
        {
            label: KEY_ROUTE_NUMBER,
            suggestedColumnsToMatch: ['route name', 'amazon route', 'route code']
        },
        {
            label: KEY_STAGING,
            suggestedColumnsToMatch: ['amazon staging', 'stage', 'staging location', 'location']
        },
        {
            label: KEY_NOTES,
            suggestedColumnsToMatch: ['comments']
        }
    ]
}

module.exports = {
    KEY_ASSOCIATE_NAME,
    KEY_TRANSPORTER_ID,
    KEY_ASSOCIATE_EMAIL,
    KEY_VEHICLE,
    KEY_VEHICLE_TYPE,
    KEY_PARKING_SPACE,
    KEY_ROSTER_STATUS,
    KEY_DSP_SHORT_CODE,
    KEY_DEVICE,
    KEY_TIME,
    KEY_ROUTE_NUMBER,
    KEY_STAGING,
    KEY_NOTES,
    getCustomColumns
}