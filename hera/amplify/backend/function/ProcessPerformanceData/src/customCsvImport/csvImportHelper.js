const {
    generateUUID,
    parseTimeValue,
    getTableName
}  = require('../util')
const {
    updateRecord,
    createDynamoRecord,
    buildUpdateParams
} = require('../dynamodbHelper')
const {
    getTableData,
    shortCodeColumn
} = require('./csvCompileHelper.js')
const {
    KEY_VEHICLE,
    KEY_VEHICLE_TYPE,
    KEY_PARKING_SPACE,
    KEY_ROSTER_STATUS,
    KEY_DSP_SHORT_CODE,
    KEY_DEVICE,
    KEY_TIME,
    KEY_ROUTE_NUMBER,
    KEY_STAGING,
    KEY_NOTES,
} = require('./columnNames')
const { Tables } = require('../tables');

const findElement = (list = [], value = '', properties = []) => {
    const lowerCaseValue = String(value).toLowerCase() || 0
    return list.find((element = {}) => {
        return element.id === lowerCaseValue || properties.some( key => String(element[key] || '').toLowerCase() === lowerCaseValue)
    })
}

const getVehicleValue = (vehicles, value) => {
    const item = findElement(vehicles, value, ['id', 'name'] ) || {}
    return item['id']
}

const getVehicleTypeValue = (optionCustomLists, value) => {
    const item = findElement(optionCustomLists, value, ['id', 'option'] ) || {}
    return item['id']
}

const getDeviceValue = (devices, value) => {
    const item = findElement(devices, value, ['id', 'deviceName'] ) || {}
    return item['id']
}

const getValueListItem = (ValueListItems, value) => {
    const item = findElement(ValueListItems, value, ['id', 'value'] ) || {}
    return item['value']
}

const getOptionCustomList = (optionCustomLists, value) => {
    const item = findElement(optionCustomLists, value, ['id', 'option'] ) || {}
    return item['id']
}

const importProcess = async function(storage, proxy){
    const tenant = proxy.tenant
    const shortCode = tenant.shortCode.toLowerCase()
    
    const matchToDailyRosterColumn = ((proxy.columnTableData||{}).data||[])[0]
    const importedShortCode = shortCodeColumn(matchToDailyRosterColumn)

    let tableData
    if(importedShortCode){
        tableData = getTableData(storage, null, (row)=> (row[KEY_DSP_SHORT_CODE]||'').toLowerCase() === shortCode)
    }else{
        tableData = getTableData(storage)
    }

    if(!tableData.length){
        console.log("Err: No Table Data found")
    }

    const matched = [...tableData[0], ...tableData[1]]

    for (const row of matched) {
        const { staff, customCsvData} = row
        await importDailyRoster(staff, customCsvData[0] || [], proxy)
    }

    
    const aux = Object.entries(proxy.importErrors.customCsv)
    proxy.importErrors.customCsvData = aux.reduce((map, [staffId, errors])=>{
        const staff = proxy.associates.find(associate => associate.id === staffId)
        Object.entries(errors).forEach(([errorType, value]) =>{
            return map.push({ staffId, staff: `${staff.firstName} ${staff.lastName}`, errorType, value })
        })
        return map
    }, [])
    delete proxy.importErrors.customCsv

    for (const [key, values] of Object.entries(proxy.tmpTable)) {
        for (const [staffId, item] of Object.entries(values)) {
            let status
            if(key === 'replaceBy'){
                status = await  createRecord(item, item.id, getTableName(Tables.REPLACE_BY_ROUTE_TABLE))
            }else if(key === 'route'){
                status = await  createRecord(item, item.id, getTableName(Tables.ROUTE_TABLE))
            }else if(key === 'notYetRostered'){
                item.group = tenant.group
                status = await  createRecord(item, generateUUID(), getTableName(Tables.ROUTE_TABLE))
            }
            status && (proxy.importCounts['customCsv'][status]++)
        }
    }

}

const importDailyRoster = async function(staff, customCsvData, proxy){
    if(!customCsvData || !customCsvData.length){
        return
    }
    const {
        associatesByIdLookupMap,
        vehiclesByIdLookupMap,
        vehiclesAlreadyAssignedLookupMap,
        importErrors,
        tmpTable,
        valueListItems,
        optionCustomLists,
        vehicles,
        devices,
        dailyrosterId,
        variant
    } = proxy

    const { customCsv: customCsvErrors} = importErrors

    let { routeAssignment, id: staffId } = staff

    if(routeAssignment){

        const importData = customCsvData.reduce((curr, value) => ({...curr, ...value}), {})

        let vehicleId, parkingSpaceId, deviceId, vehicleTypeId
        const item = {}

        if(importData[KEY_STAGING]){
            item.staging = importData[KEY_STAGING]
        }
        if(importData[KEY_ROUTE_NUMBER]){
            item.routeNumber = importData[KEY_ROUTE_NUMBER]
        }
        if(importData[KEY_NOTES]){
            item.notes = importData[KEY_NOTES]
        }

        if(importData[KEY_TIME]){
            const time = parseTimeValue(importData[KEY_TIME])
            if(time){
                item.time = time
            }else{
                !customCsvErrors[staffId] && (customCsvErrors[staffId] = {})
                customCsvErrors[staffId][KEY_TIME] = importData[KEY_TIME]
            }
        }
        if(importData[KEY_ROSTER_STATUS]){
            if(variant === 'DailyRosterV1'){
                const status = getValueListItem(valueListItems['roster-status'], importData[KEY_ROSTER_STATUS])
                if(status){
                    item.status = status
                }else{
                    !customCsvErrors[staffId] && (customCsvErrors[staffId] = {})
                    customCsvErrors[staffId][KEY_ROSTER_STATUS] = importData[KEY_ROSTER_STATUS]
                }
            }
        }
        if(importData[KEY_VEHICLE]){
            vehicleId = getVehicleValue(vehicles, importData[KEY_VEHICLE])
            if(!vehicleId){
                !customCsvErrors[staffId] && (customCsvErrors[staffId] = {})
                customCsvErrors[staffId][KEY_VEHICLE] = importData[KEY_VEHICLE]
            }
        }
        if(importData[KEY_PARKING_SPACE]){            
            parkingSpaceId = getOptionCustomList(optionCustomLists['parking-space'], importData[KEY_PARKING_SPACE])
            if(!parkingSpaceId){
                !customCsvErrors[staffId] && (customCsvErrors[staffId] = {})
                customCsvErrors[staffId][KEY_PARKING_SPACE] = importData[KEY_PARKING_SPACE]
            }
        }
        if(importData[KEY_DEVICE]){
            deviceId = getDeviceValue(devices, importData[KEY_DEVICE])
            if(!deviceId){
                !customCsvErrors[staffId] && (customCsvErrors[staffId] = {})
                customCsvErrors[staffId][KEY_DEVICE] = importData[KEY_DEVICE]
            }
        }
        if(importData[KEY_VEHICLE_TYPE]){
            vehicleTypeId = getVehicleTypeValue(devices, importData[KEY_VEHICLE_TYPE])
            if(!vehicleTypeId){
                !customCsvErrors[staffId] && (customCsvErrors[staffId] = {})
                customCsvErrors[staffId][KEY_VEHICLE_TYPE] = importData[KEY_VEHICLE_TYPE]
            }
        }

        // Set default vehicle
        if (!vehicleId) {
            const isVehicleActive = (id) => vehiclesByIdLookupMap[id]?.status === 'Active';
            const isVehicleAlreadyAssigned = (id) => !!vehiclesAlreadyAssignedLookupMap[id];
            const isVehicleEligibleForAssigment = (id) => id && isVehicleActive(id) && !isVehicleAlreadyAssigned(id);
            const staff = associatesByIdLookupMap[staffId];
            if (staff) {
                if (isVehicleEligibleForAssigment(staff.staffDefaultVehicleId)) {
                    vehicleId = staff.staffDefaultVehicleId;
                } else if (isVehicleEligibleForAssigment(staff.staffDefaultVehicle2Id)) {
                    vehicleId = staff.staffDefaultVehicle2Id;
                } else if (isVehicleEligibleForAssigment(staff.staffDefaultVehicle3Id)) {
                    vehicleId = staff.staffDefaultVehicle3Id;
                }
            }
        }
        vehicleId && (vehiclesAlreadyAssignedLookupMap[vehicleId] = true);

        if(routeAssignment.assignmentType === 'replaceBy'){
            item.id = routeAssignment.replaceById
            item.replaceByRouteStaffId = staffId
            vehicleId && (item.replaceByRouteVehicleId = vehicleId)
            parkingSpaceId && (item.replaceByRouteParkingSpaceId = parkingSpaceId)
            deviceId && (item.replaceByRouteDeviceId = deviceId)
            createOrUpdateItem(item, staffId, routeAssignment.assignmentType, tmpTable)
        }else{
            item.routeStaffId = staffId
            vehicleId && (item.routeVehicleId = vehicleId)
            parkingSpaceId && (item.routeParkingSpaceId = parkingSpaceId)
            deviceId && (item.routeDeviceId = deviceId)
            if(routeAssignment.assignmentType === 'route' &&  routeAssignment.routeId){
                item.id = routeAssignment.routeId
                createOrUpdateItem(item, staffId, routeAssignment.assignmentType, tmpTable)
            }else{
                item.routeDailyRosterId = dailyrosterId
                createOrUpdateItem(item, staffId, 'notYetRostered', tmpTable)
            }
        }
    }

}

const createOrUpdateItem = function (item, id, key, tmpTable){
    if(tmpTable[key][id]){
        tmpTable[key][id] = {...tmpTable[key][id], ...item}
    }else{
        tmpTable[key][id] = item
    }
}

async function createRecord(input, id, table){
    !input.updatedAt && (input.updatedAt = new Date().toISOString())
    try{
        const updateParams = buildUpdateParams(table, input, id)
        const wasUpdated = await updateRecord(updateParams, input)
        if(wasUpdated){
            return 'updated'
        }
    }
    catch(e){
        try{
            input.id = id
            input.createdAt = input.updatedAt
            const createParams = {
                TableName: table,
                Item: input
            }
            await createDynamoRecord(createParams)
            return 'created'
        }catch(e){
            console.log(e)
            return 'errors'
        }
    }
}

module.exports = { importProcess, createOrUpdateItem, createRecord }