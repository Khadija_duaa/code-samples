// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION})

// load AppSync and graphQL
const {
    executeMutation
} = require('../appSync.js');

const {
    createNotification,
    createCoachingHistory,
    createCoachingRecords,
} = require('../graphql.js')

const {
    cleanTransporterId,
    AWSDateToDate,
    generateUUID
} = require('../util.js')
const {
    getTenantById,
    getTextractJobById,
    getAllAssociatesByGroup,
    getAllVehiclesByGroup,
    getAllDevicesByGroup,
    getAllCustomListsByGroup,
    getAllOptionsCustomListsByCustomListsId,
    getAllValueListByGroup,
    getAllValueListItemByValueListId
} = require('../helper.js')
const {
    compileCustomCsv,
    processTableData,
    getColumnTableData
} = require('./csvCompileHelper')
const { importProcess } = require('./csvImportHelper')
const { getCustomColumns } = require('./columnNames')

// create a new s3 object
const s3 = new AWS.S3();

const customCsvHandler = async (event, context, callback)=>{

    console.log("*Custom Spreadsheet Data Import*")

    const storage = {
        data: {
            customCsvData: [],
        }
    }
    
    const importCounts = {
        customCsv: { created: 0, updated: 0, errors: 0, savedFiles: 0},
    }
    
    const importErrors = {
        customCsv: {}
    }
    
    const tmpTable = {
        notYetRostered: {},
        replaceBy: {},
        route: {}
    }

    const {
        rows: matchedStaffs,
        importId,
        group,
        clientTimeZone,
        owner,
        tenantId,
        textractJobId,
        collate,
        matchStaffBy,
        dailyrosterId,
        variant
    } = event
    const tenant = await getTenantById(tenantId)

    const textractJob = await getTextractJobById(textractJobId)

    textractJob.results = JSON.parse(textractJob.results)

    const associates = await getAllAssociatesByGroup(group)
    const associateLookupMap = {}
    const associatesByIdLookupMap = {};
    
    associates.forEach((associate) => {
        if(associate.transporterId){
            const key = cleanTransporterId(associate.transporterId)
            associateLookupMap[key] = {...associate, transporterId: key}
            associatesByIdLookupMap[associate.id] = associate;
        }
    })

    const vehicles = await getAllVehiclesByGroup(group, ['id', 'name', 'status'])
    const vehiclesByIdLookupMap = vehicles.reduce((acc, item) => (acc[item.id] = item, acc), {});
    const vehiclesAlreadyAssignedLookupMap = {}; // To populate when vehicles are assigned

    const devices = await getAllDevicesByGroup(group, ['id', 'deviceName'])
    let allValueList, allCustomLists

    const valueListItems = {}
    const optionCustomLists = {}

    if(variant === "DailyRosterV1"){
        allValueList = await getAllValueListByGroup(group, ['id', 'key'])

        let rosterStatusValueList = allValueList.find(vl => vl.key === 'roster-status' )
        if(rosterStatusValueList){
            let rosterStatusValueListItems = await getAllValueListItemByValueListId(rosterStatusValueList.id, ['id', 'value', 'deleted', 'hidden'])
            valueListItems['roster-status'] = rosterStatusValueListItems.filter(vli => !vli.hidden && !vli.deleted)
        }        
    }
    else if(variant === "DailyRosterV3"){
        allCustomLists = await getAllCustomListsByGroup(group, ['id', 'type', 'listName', 'listDisplay'])

        let parkingSpaceCustomLists = allCustomLists.find(cl => cl.type === 'parking-space')
        if(parkingSpaceCustomLists){
            optionCustomLists['parking-space'] = await getAllOptionsCustomListsByCustomListsId(parkingSpaceCustomLists.id, ['id', 'option', 'optionsCustomListsCustomListsId'])
        }
    }
    
    allCustomLists = await getAllCustomListsByGroup(group, ['id', 'type', 'listName', 'listDisplay'])
    let parkingSpaceCustomLists = allCustomLists.find(cl => cl.type === 'parking-space')
    
    if(parkingSpaceCustomLists){        
        optionCustomLists['parking-space'] = await getAllOptionsCustomListsByCustomListsId(parkingSpaceCustomLists.id, ['id', 'option', 'optionsCustomListsCustomListsId'])
    }

    const rosterColumnNames = getCustomColumns()

    const data = [{...textractJob.results.associateLevel}]
    const columnTableData = getColumnTableData(data)
    columnTableData.data[0] = { ...columnTableData.data[0], ...collate }

    const mergedData = processTableData(columnTableData)

    const proxy = {
        tenant,
        associates,
        associateLookupMap,
        associatesByIdLookupMap,
        vehiclesByIdLookupMap,
        vehiclesAlreadyAssignedLookupMap,
        dailyrosterId,
        importCounts,
        importErrors,
        tmpTable,
        clientTimeZone,
        rosterColumnNames,
        columnTableData,
        mergedData,
        collate,
        matchStaffBy,
        date: AWSDateToDate(textractJob.date),
        vehicles,
        devices,
        valueListItems,
        optionCustomLists,
        variant
    }

    compileCustomCsv(textractJob, storage, proxy, matchedStaffs)

    await importProcess(storage, proxy)

    if(!textractJob.alreadyUploaded){
        const copySource = `${process.env.STORAGE_HERA_BUCKETNAME}/public/${textractJob.key}`
        const params = {
            Bucket: process.env.STORAGE_HERA_BUCKETNAME,
            CopySource: encodeURI(copySource),
            Key: 'public/' + textractJob.key,
            Expires: null,
            MetadataDirective: 'REPLACE',
            TaggingDirective: 'REPLACE'
        }
        // Update file
        const response = await s3.copyObject(params).promise().catch((e) => console.error(e))
        if(response.CopyObjectResult){
            importCounts['customCsv']['savedFiles']++
        }
    }

    const importCount = importCounts['customCsv']
    if(importCount.created || importCount.updated || importCount.errors || importCount.savedFiles){
        try{
            let input = {
                id: generateUUID(),
                group: group,
                importId,
                type: 'customCsv',
                results: JSON.stringify(importCount),
            }
            
            await executeMutation(createCoachingHistory, { input })
        }catch(e){
            console.error(`coachingHistorysCustomCsvResult Error: `, e)
        }
    }
    
    try{
        let input = {
            id: generateUUID(),
            group: group,
            importId,
            type: 'dailyRosterCsvError',
            results: JSON.stringify({importErrors}),
        }
        
        await executeMutation(createCoachingRecords, { input })
    }catch(e){
        console.error(`dailyRosterCsvError createCoachingRecords Error: `, e)
    }
    
    
    try {
        const input = {
            group: group,
            owner: owner,
            title: "Custom Spreadsheet file import complete",
            description:  "Your import started " + new Date(parseInt(importId.split('_')[1])).toLocaleString('en-US', {timeZone: clientTimeZone}) + " is complete. Click here to view the results.",
            payload: JSON.stringify({name: 'CustomCSVImport', params: {importId: importId, importCounts, importErrors}}),
            clickAction: 'navigate',
            isRead: false
        }
        await executeMutation(createNotification, {input})

    } catch(e) {

        console.log('---error', e)

        const input = {
            group: group,
            owner: owner,
            title: "Custom Spreadsheet file import complete",
            description:  "Your import started is complete. Click here to view the results.",
            payload: JSON.stringify({name: 'CustomCSVImport', params: {importId: importId, importCounts, importErrors}}),
            clickAction: 'navigate',
            isRead: false
        }
        await executeMutation(createNotification, {input})

    } finally{
        const response = {
            statusCode: 200,
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "*"
            }, 
            body: JSON.stringify({importId: importId, importCounts, importErrors}),
        };
        return response;
    }
}

module.exports = { customCsvHandler }