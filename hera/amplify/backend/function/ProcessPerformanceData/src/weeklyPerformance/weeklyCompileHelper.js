
const { cleanTransporterId } = require('../util.js')
const { matchTransporterId } = require('./weeklyImportHelper.js')

const compileResults = function(jobs, group, tenantId, associateLookupMap){
    const associateLookupMapKeys = Object.keys(associateLookupMap)
        .sort((a, b) => `${a}`.localeCompare(`${b}`));
    var scorecardData = {}
    var podData = {}
    var cxData = {}

    jobs.forEach(async job =>{
        let importType = job.type

        if(job.jobStatus == 'SUCCEEDED'){

            // get table and field data
            var results = JSON.parse(job.results)
            var tables = results.tables 
            var table = tables[0]
            var fields = results.fields
            var week = fields.week ? fields.week : job.week
            var year = fields.year ? fields.year : job.year
            week = String(week).padStart(2, '0')
            fields.week = week

            if(importType == 'scorecard'){
                let data = table.table_data.reduce((map, row) => {
                    
                    let transporterId = cleanTransporterId(row.transporter_id)
                    row.transporter_id = transporterId = matchTransporterId(transporterId, associateLookupMap, associateLookupMapKeys)
                    
                    if(row.transporter_id && row.name == null) {
                        let targetDa = associateLookupMap[row.transporter_id]
                        
                        if(targetDa) {
                            row.name = targetDa.firstName +' '+targetDa.lastName
                        }
                    }
                    
                    // skip results that don't have transporter ID and name, week 7 2021 started having issues with header row
                    if(row.transporter_id && row.name && row.delivered){

                        let scoreCardId = tenantId + transporterId + year + week

                        var value = {
                            id: scoreCardId,
                            group: group,
                            week: week,
                            year: year,
                            name: row.name,
                            messageHasBeenSent: false,
                            transporterId: row.transporter_id,
                            overallTier: row.overal_tier,
                            delivered: row.delivered,
                            keyFocusArea: row.key_focus_area,
                            ficoScore: row.fico_score,
                            seatbeltOffRate: row.seatbelt_off_rate,
                            cdf: row.cdf,
                            ced: row.ced,
                            dcr: row.dcr,
                            dar: row.dar,
                            dsb: row.dsb,
                            swcPod: row.swc_pod,
                            swcCc: row.swc_cc,
                            swcSc: row.swc_sc,
                            swcAd: row.swc_ad,
                            dnrs: row.dnrs,
                            podOpps: row.pod_opps,
                            ccOpps: row.cc_opps,
                            speedingEventRate: row.speeding_event_rate,
                            distractionsRate: row.distractions_rate,
                            followingDistanceRate: row.following_distance_rate,
                            signSignalViolationsRate: row.sign_signal_violations_rate,
                            harshBrakingRate: row.harsh_braking, 
                            harshCorneringRate: row.harsh_cornering
                        }
                        scorecardData[scoreCardId] = value
                        return map
                    }
                    else{
                        return map
                    }
                }, {})
                
            } // endif scorecard

            else if(importType == 'podQualityFeedback'){
                let data = table.table_data.reduce((map, row) => {

                    let transporterId = cleanTransporterId(row.transporter_id)
                    row.transporter_id = transporterId = matchTransporterId(transporterId, associateLookupMap, associateLookupMapKeys)

                    if(row.transporter_id && row.employee_name == null) {
                        let targetDa = associateLookupMap[row.transporter_id]
                        
                        if(targetDa) {
                            row.employee_name = targetDa.firstName +' '+targetDa.lastName
                        }
                    }


                    if(row.transporter_id && row.employee_name){

                        let podQualityID = tenantId + transporterId + year + week

                        var value = {
                            id: podQualityID,
                            group: group,
                            week: week,
                            year: year,
                            employeeName: row.employee_name,
                            transporterId: row.transporter_id,
                            opportunities: row.opportunities,
                            success: row.success,
                            bypass: row.bypass,
                            packageInHand: row.package_in_hand,
                            notClearlyVisible: row.package_not_clearly_visible,
                            blurry: row.blurry_photo,
                            explicit: row.explicit,
                            mailSlot: row.mail_slot,
                            noPackage: row.no_package ? row.no_package : row.no_package_detected,
                            other: row.other,
                            packageTooClose: row.package_too_close,
                            personInPhoto: row.person_in_photo ? row.person_in_photo : row.human_in_the_picture,
                            photoTooDark: row.photo_too_dark,
                            takenFromCar: row.taken_from_car ? row.taken_from_car : row.package_in_car,
                            grandTotal: row.grand_total ? row.grand_total : row.rejects
                        }

                        podData[podQualityID] = value
                        return map
                    }
                    else{
                        return map
                    }
                }, {})

            } // endif POD

            else if(importType == 'customerFeedback'){
                let data = table.table_data.reduce((map, row) => {

                    let transporterId = cleanTransporterId(row.transporter_id)
                    row.transporter_id = transporterId = matchTransporterId(transporterId, associateLookupMap, associateLookupMapKeys)

                    if(row.transporter_id && row.driver_name == null) {
                        let targetDa = associateLookupMap[row.transporter_id]
                        
                        if(targetDa) {
                            row.driver_name = targetDa.firstName +' '+targetDa.lastName
                        }
                    }
                  
                    if(row.transporter_id && row.driver_name){
                        
                        let customerFeedbackID = tenantId + transporterId + year + week

                        var value = {
                            id: customerFeedbackID,
                            group: group,
                            week: week,
                            year: year,
                            messageHasBeenSent: false,
                            name: row.driver_name,
                            transporterId: row.transporter_id,
                            positiveFeedback: row['%_positive_feedback'],
                            negativeFeedback: row['$_negative_feedback'],
                            deliveryWasGreat: row.delivery_was_great,
                            careForOthers: row.care_for_others,
                            deliveryWasntGreat: row.delivery_was_not_so_great,
                            daTier: row.da_tier,
                            cdfScore:row.cdf_score,
                            totalDeliveries: row.total_deliveries_with_customer_feedback,
                            respectfulOfProperty: row.respectful_of_property,
                            followedInstructions: row.followed_instructions,
                            friendly: row.friendly,
                            aboveAndBeyond: row.went_above_and_beyond,
                            deliveredWithCare: row.delivered_with_care,
                            thankMyDriver: row.thank_my_driver,
                            mishandledPackage: row.mishandled_package,
                            drivingUnsafely: row.driving_unsafely,
                            driverUnprofessional: row.driver_unprofessional,
                            notDeliveredToPreferredLocation: row.not_follow_delivery_instructions,
                            noFeedback: row.no_feedback,
                            onTime: row.on_time,
                            lateDelivery: row.late_delivery,
                            itemDamaged: row.item_damaged,
                            deliveredToWrongAddress: row.delivered_to_wrong_address,
                            neverReceivedDelivery: row.never_received_delivery
                        }
                        cxData[customerFeedbackID] = value
                        return map
                    }
                    else{
                        return map
                    }
                }, {})
            } // endif CX
        }
    })
    return [scorecardData, podData, cxData]
}

module.exports = { compileResults }