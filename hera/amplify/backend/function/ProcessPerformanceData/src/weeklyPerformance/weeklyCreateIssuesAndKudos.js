const { getYearWeekDate, parseDateToTimezoneTimestamp, dateToYYYYMMDD } = require('../util.js')

function addCreateOrUpdateItem(item, id, type, proxy){
    const performanceImports = proxy.performanceImports
    if(!performanceImports[id]) performanceImports[id] = {}
    performanceImports[id][type] = item
}

const createOrUpdateItem = function (item, id, key, performanceImports){
    if(performanceImports[key][id]){
        performanceImports[key][id] = {...performanceImports[key][id], ...item}
    }else{
        performanceImports[key][id] = item
    }
}

const createIssuesAndKudos = async function(group, scorecard, cx, tenant, consecutiveScorecards, proxy){
    var staffId = null

    /**  
     * Don't Change the Order and the Value of levels, 
     * OverAll Tier Issues and Kudos creation depends on these values
     *   levels['Fantastic'] = 4
     *   levels['Great']     = 3
     *   levels['Fair']      = 2
     *   levels['Poor']      = 1
    */
    var levels = []
    levels['Fantastic'] = 4
    levels['Great']     = 3
    levels['Fair']      = 2
    levels['Poor']      = 1

    if(scorecard) staffId = scorecard.staffScoreCardStaffId
    else if(cx) staffId = cx.staffCxFeedbackStaffId
    else{
        return
    }

    // ------------ CUSTOMER FEEDBACK ------------
    if(cx){

        let issueDaTier = levels[tenant['coachingDaTierRatingIssue']]
        let cxDaTier = levels[cx.daTier]

        const date = getYearWeekDate(cx.year, cx.week);
        const dateStr = dateToYYYYMMDD(date);

        // --- Issue & Kudo for DA tier---
        //const infractionOrKudoType = 'DA tier'
        const infractionOrKudoType = 'CX Da Tier'
        
        // --- Issue ---
        if(cxDaTier && cxDaTier <= issueDaTier) {
            let id = cx.year + '-' + cx.week + '-cx-datier-' + staffId
            let input = {
                infractionStaffId: staffId, 
                staffId: staffId,
                date: parseDateToTimezoneTimestamp(dateStr, proxy.clientTimeZone),
                comment: 'Received ' + cx['daTier'] + ' (Created from CX import)',
                group: group,
                updatedAt: new Date().toISOString()
            }
            const infractionTypeId = proxy.issueTypeList.find(item => item.option === infractionOrKudoType)?.id
            if(infractionTypeId) input.infractionTypeId = infractionTypeId
            else input.infractionType = infractionOrKudoType
            
            //addCreateOrUpdateItem(input, id, 'issue', proxy)
            createOrUpdateItem(input, id, 'issues', proxy.performanceImports)
        }

        let kudoDaTier = levels[tenant['coachingDaTierRatingKudo']]
        // --- Kudo ---
        if(cxDaTier && cxDaTier >= kudoDaTier) {
            let id = cx.year + '-' + cx.week + '-cx-datier-' + staffId
            let input = {
                kudoStaffId: staffId, 
                staffId: staffId,
                date: parseDateToTimezoneTimestamp(dateStr, proxy.clientTimeZone),
                notes: 'Received ' + cx['daTier'] + ' (Created from CX import)',
                group: group,
                updatedAt: new Date().toISOString()
            }
            const kudoTypeId = proxy.kudoTypeList.find(item => item.option === infractionOrKudoType)?.id
            if(kudoTypeId) input.kudoTypeId = kudoTypeId
            else input.kudoType = infractionOrKudoType

            //addCreateOrUpdateItem(input, id, 'kudo', proxy)
            createOrUpdateItem(input, id, 'kudos', proxy.performanceImports)
        }

        let metrics = [
            //{ polarity: 1, name: 'CdfScore', fieldName: 'cdfScore', type: 'CdfScore', code: 'cds'},
            { polarity: 1, name: 'CdfScore', fieldName: 'cdfScore', type: 'CX Cdf Score', code: 'cds'}
        ]

        await Promise.all(metrics.map( async (metric) => {
            // --- Issues ---
            if((tenant['coaching' + metric.name + 'Issue']) && 
                (
                    ( metric.polarity  && parseFloat(cx[metric.fieldName]) < parseFloat(tenant['coaching' + metric.name + 'ThresholdIssue'])) ||
                    ( !metric.polarity && parseFloat(cx[metric.fieldName]) > parseFloat(tenant['coaching' + metric.name + 'ThresholdIssue']))
                ) 
            ){
                let id = cx.year + '-' + cx.week + '-cx-' + metric.code + '-' + staffId
                let input = {
                    infractionStaffId: staffId, 
                    staffId: staffId,
                    date: parseDateToTimezoneTimestamp(dateStr, proxy.clientTimeZone),
                    comment: 'Received ' + cx[metric.fieldName]+ ' (Created from CX import)',
                    group: group,
                    updatedAt: new Date().toISOString()
                }
                const infractionTypeId = proxy.issueTypeList.find(item => item.option === metric.type)?.id
                if(infractionTypeId) input.infractionTypeId = infractionTypeId
                else input.infractionType = metric.type

                //addCreateOrUpdateItem(input, id, 'issue', proxy)
                createOrUpdateItem(input, id, 'issues', proxy.performanceImports)
            }

            // --- Kudos ---
            if((tenant['coaching' + metric.name + 'Kudo']) && 
                (
                    ( metric.polarity  && parseFloat(cx[metric.fieldName]) >= parseFloat(tenant['coaching' + metric.name + 'ThresholdKudo'])) ||
                    ( !metric.polarity && parseFloat(cx[metric.fieldName]) <= parseFloat(tenant['coaching' + metric.name + 'ThresholdKudo']))
                )
            ){
                let id = cx.year + '-' + cx.week + '-cx-' + metric.code + '-' + staffId
                let input = {
                    kudoStaffId: staffId, 
                    staffId: staffId,
                    date: parseDateToTimezoneTimestamp(dateStr, proxy.clientTimeZone),
                    notes: 'Received ' + cx[metric.fieldName]+ ' (Created from CX import)',
                    group: group,
                    updatedAt: new Date().toISOString()
                }
                const kudoTypeId = proxy.kudoTypeList.find(item => item.option === metric.type)?.id
                if(kudoTypeId) input.kudoTypeId = kudoTypeId
                else input.kudoType = metric.type

                //addCreateOrUpdateItem(input, id, 'kudo', proxy)
                createOrUpdateItem(input, id, 'kudos', proxy.performanceImports)
            }
        }));
    }

    
    // ------------ SCORECARD ------------
    if(scorecard){ 
        let numberConsecutiveKudo = 0
        let numberConsecutiveIssue = 0
        try{
            // calculate consecutive tiers for issues and kudos
            let scoreCardsSorted = consecutiveScorecards[staffId]
            if(!scoreCardsSorted) scoreCardsSorted = []
            // add current scorecard if necessary and sort
            let currScorecardExists = scoreCardsSorted.find(el =>{
                return (el.week == scorecard.week) && (el.year == scorecard.year)
            })
            if(!currScorecardExists){
                let newScorecard = {
                    week: scorecard.week,
                    year: scorecard.year,
                    overallTier: scorecard.overallTier
                }
                scoreCardsSorted.push(newScorecard)
                scoreCardsSorted.sort((a,b) => {
                    return parseInt(b.year+b.week.padStart(2, 0)) - parseInt(a.year+a.week.padStart(2, 0))
                })
            }
            
            let breakKudo = false
            let breakIssue = false
            
            if(scoreCardsSorted.length){
                var issueAccumulator = 0
                var kudoAccumulator = 0
                let currentScorecardYearWeek = parseInt(String(scorecard.year) + String(scorecard.week))
                
                for(const sortedScorecard of scoreCardsSorted){
                    let sortedYearWeek = parseInt(sortedScorecard.year + sortedScorecard.week)
                    if(sortedYearWeek > currentScorecardYearWeek) continue // get to appropriate starting index
                    
                    if((sortedScorecard.overallTier == tenant.coachingConsecutiveTierRatingIssue) && !breakIssue){
                        issueAccumulator++
                        breakKudo = true
                    }
                    else if((sortedScorecard.overallTier == tenant.coachingConsecutiveTierRatingKudo) && !breakKudo){
                        kudoAccumulator++
                        breakIssue = true
                    }
                    else{
                        break // break if overall tier doesn't match issue or kudo criteria
                    }
                }
                
                numberConsecutiveIssue = issueAccumulator
                numberConsecutiveKudo = kudoAccumulator
            }
        }
        catch(e){
            console.log('---catch-14', {error: e})
            console.log('---catch-14', {staffId: staffId})
        }
        
        let metrics = [
            { code: 'f',   polarity: 1, name: 'Fico', fieldName: 'ficoScore', type: 'Scorecard FICO® Score'},
            { code: 'dcr', polarity: 1, name: 'Dcr', fieldName: 'dcr', type: 'Scorecard DCR'},
            { code: 'dar', polarity: 1, name: 'Dar', fieldName: 'dar', type: 'Scorecard DAR'},
            { code: 'dnr', polarity: 0, name: 'Dnr', fieldName: 'dnrs', type: 'Scorecard DNR'},
            { code: 'pod', polarity: 1, name: 'SwcPod', fieldName: 'swcPod', type: 'Scorecard SWC-POD'},
            { code: 'cc',  polarity: 1, name: 'SwcCc', fieldName: 'swcCc', type: 'Scorecard SWC-CC'},
            { code: 'sc',  polarity: 1, name: 'SwcSc', fieldName: 'swcSc', type: 'Scorecard SWC-SC'},
            { code: 'ad',  polarity: 1, name: 'SwcAd', fieldName: 'swcAd', type: 'Scorecard SWC-AD'},
            { code: 'so',  polarity: 0, name: 'SeatbeltOff', fieldName: 'seatbeltOffRate', type: 'Scorecard Seatbelt-Off Rate'},
            { code: 'se',  polarity: 0, name: 'SpeedingEvent', fieldName: 'speedingEventRate', type: 'Scorecard Speeding Event Rate'},
            { code: 'hb',  polarity: 0, name: 'HarshBrakingRate', fieldName: 'harshBrakingRate', type: 'Scorecard Harsh Braking Rate'},
            { code: 'hc',  polarity: 0, name: 'HarshCorneringRate', fieldName: 'harshCorneringRate', type: 'Scorecard Harsh Cornering Rate'},
            { code: 'dr',  polarity: 0, name: 'DistractionsRate', fieldName: 'distractionsRate', type: 'Scorecard Distractions Rate'},
            { code: 'fd',  polarity: 0, name: 'FollowingDistanceRate', fieldName: 'followingDistanceRate', type: 'Scorecard Following Distance Rate'},
            { code: 'ssv', polarity: 0, name: 'SignSignalViolationsRate', fieldName: 'signSignalViolationsRate', type: 'Scorecard Sign/Signal Violation Rate'},
            //{ code: 'ot',  polarity: 0, name: 'OverallTier', fieldName: 'overallTier', type: 'Overall Tier'},
            { code: 'ot',  polarity: 0, name: 'OverallTier', fieldName: 'overallTier', type: 'Scorecard Overall Tier'},
            //{ code: 'ct',  polarity: 0, name: 'ConsecutiveTier', type: 'Consecutive Tier'},
            { code: 'ct',  polarity: 0, name: 'ConsecutiveTier', type: 'Scorecard Consecutive Tier'},
            //{ code: 'dsb', polarity: 0, name:'Dsb', fieldName: 'dsb', type: 'Scorecard Delivery Success Behaviors'},
            { code: 'dsb', polarity: 0, name:'Dsb', fieldName: 'dsb', type: 'Scorecard DSB'},
        ]

        const date = getYearWeekDate(scorecard.year, scorecard.week);
        const dateStr = dateToYYYYMMDD(date);

        await Promise.all(metrics.map( async (metric) => {
            // --- Issues ---
            if(tenant['coaching' + metric.name + 'Issue']) {              
                const coachingOverallTierRatingIssueLevel = levels[tenant['coaching' + metric.name + 'RatingIssue']]
                const scorecardOverallTierLevel =  levels[scorecard[metric.fieldName]]
                
                if(
                    ( (metric.code === 'ot') && scorecardOverallTierLevel <= coachingOverallTierRatingIssueLevel ) ||
                    ( metric.code === 'ct' && numberConsecutiveIssue >= tenant['coaching' + metric.name + 'ThresholdIssue']) ||
                    ( metric.polarity && parseFloat(scorecard[metric.fieldName]) < parseFloat(tenant['coaching' + metric.name + 'ThresholdIssue'])) ||
                    ( !metric.polarity && parseFloat(scorecard[metric.fieldName]) > parseFloat(tenant['coaching' + metric.name + 'ThresholdIssue'])) 
                ) {
                    let comment = 'Received ' + scorecard[metric.fieldName]+ ' (Created from scorecard import)'
                    if(metric.code === 'ct') comment = `Received more than ${tenant.coachingConsecutiveTierThresholdIssue} consecutive ${tenant.coachingConsecutiveTierRatingIssue}${tenant.coachingConsecutiveTierThresholdIssue > 1 ? 's' : ''} on Scorecards (Created from scorecard import)`
                    let id = scorecard.year + '-' + scorecard.week + '-s-' + metric.code + '-' + staffId
                    let input = {
                        infractionStaffId: staffId, 
                        staffId: staffId,
                        date: parseDateToTimezoneTimestamp(dateStr, proxy.clientTimeZone),
                        comment: comment,
                        group: group,
                        updatedAt: new Date().toISOString()
                    }
                    const infractionTypeId = proxy.issueTypeList.find(item => item.option === metric.type)?.id
                    if(infractionTypeId) input.infractionTypeId = infractionTypeId
                    else input.infractionType = metric.type

                    //addCreateOrUpdateItem(input, id, 'issue', proxy)
                    createOrUpdateItem(input, id, 'issues', proxy.performanceImports)
                }
            }

            // --- Kudos ---
            if ( metric.code != 'ad' && tenant['coaching' + metric.name + 'Kudo'] ) {
                const coachingOverallTierRatingKudosLevel = levels[tenant['coaching' + metric.name + 'RatingKudo']]
                const scorecardOverallTierLevel =  levels[scorecard[metric.fieldName]]

                if (
                    ( (metric.code === 'ot') && scorecardOverallTierLevel >= coachingOverallTierRatingKudosLevel ) ||
                    ( metric.code === 'ct' && numberConsecutiveKudo >= tenant['coaching' + metric.name + 'ThresholdKudo']) ||
                    ( metric.polarity  && parseFloat(scorecard[metric.fieldName]) >= parseFloat(tenant['coaching' + metric.name + 'ThresholdKudo'])) ||
                    ( !metric.polarity && parseFloat(scorecard[metric.fieldName]) <= parseFloat(tenant['coaching' + metric.name + 'ThresholdKudo']))
                ) {
                    let notes = 'Received ' + scorecard[metric.fieldName]+ ' (Created from scorecard import)'
                    if(metric.code === 'ct') notes = `Received more than ${tenant.coachingConsecutiveTierThresholdKudo} consecutive ${tenant.coachingConsecutiveTierRatingKudo}${tenant.coachingConsecutiveTierThresholdKudo > 1 ? 's' : ''} on Scorecards (Created from scorecard import)`
                    let id = scorecard.year + '-' + scorecard.week + '-s-' + metric.code + '-' + staffId
                    let input = {
                        kudoStaffId: staffId, 
                        staffId: staffId,
                        date: parseDateToTimezoneTimestamp(dateStr, proxy.clientTimeZone),
                        notes: notes,
                        group: group,
                        updatedAt: new Date().toISOString()
                    }
                    const kudoTypeId = proxy.kudoTypeList.find(item => item.option === metric.type)?.id
                    if(kudoTypeId) input.kudoTypeId = kudoTypeId
                    else input.kudoType = metric.type

                    //addCreateOrUpdateItem(input, id, 'kudo', proxy)
                    createOrUpdateItem(input, id, 'kudos', proxy.performanceImports)
                }
            }
        }));
    }
    return
}

module.exports = { createIssuesAndKudos }