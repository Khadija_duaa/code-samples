// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION})

// load AppSync and graphQL
const {
    executeMutation
} = require('../appSync.js');

const {
    createNotification,
    createCoachingHistory,
    createCoachingRecords,
} = require('../graphql.js');

const {
    buildGetItemParams,
    buildQueryParams,
    buildUpdateParams,
    queryDynamoRecords,
    updateDynamoRecord,
    createDynamoRecord,
    getDynamoRecord,
    updateRecord
} = require('../dynamodbHelper.js')

const { Tables } = require('../tables');

// create a new s3 object
const s3 = new AWS.S3();

const { getTenantById, getAllJobsByJobId, getAllAssociatesByGroup, getCustomListByGroupAndType, getAllOptionsCustomListsByCustomListsId } = require('../helper.js')
const { cleanTransporterId, generateUUID, getTableName } = require('../util.js')
const { compileResults }  = require('./weeklyCompileHelper.js')
const { compileSummaryTablePod, compileSummaryTableCxFeedback } = require('./compileSummaryTable.js')
const { increaseCount, checkResults, createOrUpdateRecord, createUpdateIssueOrKudoRecord, deleteIssueOrKudoRecord}  = require('./weeklyImportHelper.js')
const { getKudosOrIssueRecords } = require('../kudosOrIssuesHelper.js')
const { createIssuesAndKudos }  = require('./weeklyCreateIssuesAndKudos.js')
const { createPendingMessageRecord }  = require('./weeklyCreatePendingMessage.js')
const { validateWeekForPendingMessages } = require('./validateGeneratePendingMessages.js');

Date.prototype.getWeekNumber = function(){
    var d = new Date(Date.UTC(this.getFullYear(), this.getMonth(), this.getDate()));
    var dayNum = d.getUTCDay() || 7;
    d.setUTCDate(d.getUTCDate() + 4 - dayNum);
    var yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
    return Math.ceil((((d - yearStart) / 86400000) + 1)/7)
}

const weeklyHandler = async (event, context, callback) => {

    console.log("*Weekly Data Import*")

    // global vars
    const pendingMessages = {}
    let performanceImports = {
        issues: {},
        kudos: {}
    }
    const importCounts = {
        scorecard: { created: 0, updated: 0, errors: 0, savedFiles: 0, failedFiles: 0, createdDas:{}, updatedDas:{}, errorsDas:{} },
        pod: { created: 0, updated: 0, errors: 0, savedFiles: 0, failedFiles: 0, createdDas:{}, updatedDas:{}, errorsDas:{} },
        cx: { created: 0, updated: 0, errors: 0, savedFiles: 0, failedFiles: 0, createdDas:{}, updatedDas:{}, errorsDas:{} },
    }
    const importErrors = {
        scorecard: [],
        pod: [],
        cx: []
    }
    const performanceCounts = {
        message: { created: 0, updated: 0, errors: 0, attempts: 0, errorMsgs: [], title: 'Pending Coaching Messages' },
        kudo: { created: 0, updated: 0, deleted: 0, errors: 0, createdDas:{}, updatedDas:{}, errorsDas:{}, errorMsgs: [], title: 'Associate Kudos' },
        issue: { created: 0, updated: 0, deleted: 0, errors: 0, createdDas:{}, updatedDas:{}, errorsDas:{}, errorMsgs: [], title: 'Associate Issues' }
    }
    let importFiles = {scorecard: false, customerFeedback: false, podQualityFeedback: false}
    
    var importId = String(event.importId)
    var rows = event.rows
    var tenantId = event.tenantId
    var owner = event.owner
    var clientTimeZone = event.clientTimeZone
    var handleImportsLastSaturday = event.handleImportsLastSaturday
    var associateLookupMap = {}
    let issueTypeList = []
    let kudoTypeList = []

    const proxy = {importCounts, importErrors, performanceCounts, performanceImports, pendingMessages, clientTimeZone, issueTypeList, kudoTypeList};

    var consecutiveScorecards = {}
    
    // load tenant info
    var tenant = await getTenantById(tenantId)
    var group = tenant.group
    
    // values for determining consecutive Scorecard metrics
    var weeksIssueRaw = tenant.coachingConsecutiveTierThresholdIssue == null ? 2 : tenant.coachingConsecutiveTierThresholdIssue;
    var weeksKudoRaw = tenant.coachingConsecutiveTierThresholdKudo == null ? 3 : tenant.coachingConsecutiveTierThresholdKudo;
    var weeksIssue = parseInt(weeksIssueRaw)
    var weeksKudo = parseInt(weeksKudoRaw)
    var numWeeksToLoad = weeksIssue > weeksKudo ? weeksIssue : weeksKudo
    var oldestYearWeek = 300053
    var newestYearWeek = 198001
    var containsScorecard = false

    // load DA lookup
    var associates = await getAllAssociatesByGroup(group)  
    associates.forEach((associate) => {
        if(associate.transporterId){
            let key = cleanTransporterId(associate.transporterId)
            associateLookupMap[key] = {id: associate.id, latestScorecard: associate.latestScorecard, keyFocusArea: associate.keyFocusArea, firstName: associate.firstName, lastName: associate.lastName, transporterId: key, alternateNames: associate.alternateNames}
        }
    })
  
    // load all jobs based on req job ID
    var jobs = await getAllJobsByJobId(importId)
    jobs = jobs.filter(job => job.jobStatus === 'SUCCEEDED')

    //get custom list issue-type and kudo-type
    const customListProjectedFields = ['id','type']
    const customListIssueType = await getCustomListByGroupAndType(group, 'issue-type', customListProjectedFields)
    const customListIssueTypeId = customListIssueType[0]?.id
    const customListKudoType = await getCustomListByGroupAndType(group, 'kudo-type', customListProjectedFields)
    const customListKudoTypeId = customListKudoType[0]?.id

    const optionCustomProjectedFields = ['id','option']
    if(customListIssueTypeId && customListKudoTypeId){
        issueTypeList = await getAllOptionsCustomListsByCustomListsId(customListIssueTypeId, optionCustomProjectedFields) || []
        kudoTypeList = await getAllOptionsCustomListsByCustomListsId(customListKudoTypeId, optionCustomProjectedFields) || []
        proxy.issueTypeList = issueTypeList
        proxy.kudoTypeList = kudoTypeList
    }

    try{
        // create/update CompanyScoreCard
        // await Promise.all(jobs.map( async (job) => {
        
        for(const job of jobs) {
            let results = JSON.parse(job.results)
            // summary table
            let summaryResults = JSON.parse(job.summaryTableResults)
            const isTypePodOrCx = job.type === 'podQualityFeedback' || job.type === 'customerFeedback'
            if(isTypePodOrCx && checkResults(results) && checkResults(summaryResults)){
                let fields = summaryResults.fields
                let week = fields.week ? fields.week : job.week
                week = String(week).padStart(2, '0')
               
                let year = fields.year ? fields.year : job.year
                let yearWeek = `${year}${week}`
                const summaryTableId = tenantId + `-${yearWeek}`
                let item = {
                    group: group,
                    year: year,
                    week: week,
                    yearWeek: yearWeek,
                    updatedAt: new Date().toISOString(),
                }

                if(job.type === 'podQualityFeedback'){
                    const data = compileSummaryTablePod(summaryResults.tables)
                    if(Object.keys(data).length){
                        let itemPod = {
                            ...item,
                            success: data.success,
                            bypass: data.bypass,
                            rejects: data.rejects,
                            opportunities: data.opportunities,
                            noPackageDetected: data.noPackageDetected,
                            packageNotClearlyVisible: data.packageNotClearlyVisible,
                            blurryPhoto: data.blurryPhoto,
                            packageTooClose: data.packageTooClose,
                            humanInThePicture: data.humanInThePicture,
                            packageInHand: data.packageInHand,
                            photoTooDark: data.photoTooDark,
                            packageInCar: data.packageInCar,
                            other: data.other,
                            grandTotal: data.grandTotal,
                            podQualitySummaryTextractJobId: job.id
                        }
                        
                        try{
                            //Update record, if that fails, try creating
                            let updateParams = buildUpdateParams(getTableName(Tables.POD_QUALITYSUMMARY_TABLE), itemPod, summaryTableId)
                            await updateDynamoRecord(updateParams);
                        }catch(e){
                            try{
                                // Create record, if that fails, log error
                                itemPod.createdAt = new Date().toISOString()
                                itemPod.id = summaryTableId
                                let createParams = {
                                    TableName: getTableName(Tables.POD_QUALITYSUMMARY_TABLE),
                                    Item: itemPod
                                };
                                await createDynamoRecord(createParams);
                            }
                            catch(e){
                                console.log('---catch POD_QUALITYSUMMARY_TABLE', e)
                            }
                        }
                    }
                }
                if(job.type === 'customerFeedback'){
                    const data = compileSummaryTableCxFeedback(summaryResults.tables)
                    if(Object.keys(data).length){
                        let itemCx = {
                            ...item,
                            dcfTier: data.dcfTier,
                            dcfScore: data.dcfScore,
                            positiveFeedback: data.positiveFeedback,
                            negativeFeedback: data.negativeFeedback,
                            deliveriesWithoutCF: data.deliveriesWithoutCF,
                            deliveryWasGreat: data.deliveryWasGreat,
                            respectfulOfProperty: data.respectfulOfProperty,
                            followedInstructions: data.followedInstructions,
                            friendly: data.friendly,
                            aboveAndBeyond: data.aboveAndBeyond,
                            deliveredWithCare: data.deliveredWithCare,
                            thankMyDriver: data.thankMyDriver,
                            deliveryWasntGreat: data.deliveryWasntGreat,
                            mishandledPackage: data.mishandledPackage,
                            driverUnprofessional: data.driverUnprofessional,
                            driverDidNotFollowDeliveryInstructions: data.driverDidNotFollowDeliveryInstructions,      
                            onTime: data.onTime,
                            lateDelivery: data.lateDelivery,
                            itemDamaged: data.itemDamaged,
                            deliveredToWrongAddress: data.deliveredToWrongAddress,
                            neverReceivedDelivery: data.neverReceivedDelivery,
                            cxFeedbackSummaryTextractJobId: job.id
                        }
                        
                        try{
                            //Update record, if that fails, try creating
                            let updateParams = buildUpdateParams(getTableName(Tables.CX_FEEDBACKSUMMARY_TABLE), itemCx, summaryTableId)
                            await updateDynamoRecord(updateParams);
                        }catch(e){
                            try{
                                // Create record, if that fails, log error
                                itemCx.createdAt = new Date().toISOString()
                                itemCx.id = summaryTableId
                                let createParams = {
                                    TableName: getTableName(Tables.CX_FEEDBACKSUMMARY_TABLE),
                                    Item: itemCx
                                };
                                await createDynamoRecord(createParams);
                            }
                            catch(e){
                                console.log('---catch CX_FEEDBACKSUMMARY_TABLE', e)
                            }
                        }
                    }
                }
                
            }//end summary tables

            if(checkResults(results)){

                let fields = results.fields
                let week = fields.week ? fields.week : job.week
                week = String(week).padStart(2, '0')
                fields.week = week
                let year = fields.year ? fields.year : job.year
                let yearWeek = parseInt(String(year) + week)
                if(yearWeek < oldestYearWeek) oldestYearWeek = yearWeek
                if(yearWeek > newestYearWeek) newestYearWeek = yearWeek
                let companyScoreCardId = tenantId + yearWeek
                let item = {
                    tenantId: tenantId,
                    yearWeek: yearWeek,
                    group: group,
                    updatedAt: new Date().toISOString(),
                    ...fields
                }

                if(job.type == 'scorecard'){
                    item.scorecardPdf = job.key
                    containsScorecard = true
                    importFiles.scorecard = true
                }
                if(job.type == 'podQualityFeedback'){
                    item.podQualityPdf = job.key
                    item.year = fields.year || job.year
                    importFiles.podQualityFeedback = true
                }
                if(job.type == 'customerFeedback'){
                    item.customerFeedbackPdf = job.key
                    importFiles.customerFeedback = true
                }

                try{
                    //Update record, if that fails, try creating
                    let updateParams = buildUpdateParams(getTableName(Tables.COMPANY_SCORECARD_TABLE), item, companyScoreCardId)
                    await updateDynamoRecord(updateParams);
                }catch(e){
                    try{
                        // Create record, if that fails, log error
                        item.createdAt = new Date().toISOString()
                        item.id = companyScoreCardId
                        let createParams = {
                            TableName: getTableName(Tables.COMPANY_SCORECARD_TABLE),
                            Item: item
                        };
                        await createDynamoRecord(createParams);
                    }
                    catch(e){
                        console.log('---catch-1', e)
                    }
                }
            }
        // })); // end create/update CompanyScoreCard
        }; // end create/update CompanyScoreCard
        
        if(containsScorecard){
            // load staffs scorecards based on threhsold of consecutives + 1
            let matchWeekLower = parseInt(String(oldestYearWeek).slice(-2)) - numWeeksToLoad
            let matchYearLower = parseInt(String(oldestYearWeek).slice(0,4))
            let matchWeekUpper = parseInt(String(newestYearWeek).slice(-2))
            let matchYearUpper = parseInt(String(newestYearWeek).slice(0,4))
            if(matchWeekLower < 0){ 
                matchWeekLower += 53 //53 weeks in an Amazon year
                matchYearLower--
            }
            let scorecards = []
            let lastEvaluatedKey = null
            const matchWeekLowerS = String(matchWeekLower).padStart(2,"0")
            do{
                let fields = ['group','week','year','overallTier','staffScoreCardStaffId']
                let queryParams = buildQueryParams(getTableName(Tables.STAFF_SCORECARD_TABLE), fields, group, 'byGroup', lastEvaluatedKey)
                queryParams.ExpressionAttributeNames['#sk'] = 'year#week'
                queryParams.ExpressionAttributeValues[':skl'] = `${matchYearLower}#${matchWeekLowerS}`
                queryParams.ExpressionAttributeValues[':sku'] = `${matchYearUpper}#${matchWeekUpper}`
                queryParams.KeyConditionExpression += ' AND #sk BETWEEN :skl AND :sku'
                queryParams.ExpressionAttributeNames['#f'] = 'matchedS'
                queryParams.ExpressionAttributeValues[':f'] = 'true'
                queryParams.FilterExpression = '#f = :f'
                let queryResult = await queryDynamoRecords(queryParams);
                let items = queryResult.Items
                lastEvaluatedKey = queryResult.LastEvaluatedKey
                scorecards = [...scorecards, ...items]
            }while(lastEvaluatedKey)
            
            // sort scorecards
            let scoreCardsSorted = []
            if(scorecards){
                await Promise.all(scorecards.map( async (scorecard) => {
                    let staffId = scorecard.staffScoreCardStaffId
                    if(!consecutiveScorecards[staffId]) consecutiveScorecards[staffId] = []
                    let obj = {
                        week: scorecard.week,
                        year: scorecard.year,
                        overallTier: scorecard.overallTier
                    }
                    consecutiveScorecards[staffId].push(obj)
                }));
                await Promise.all(Object.keys(consecutiveScorecards).map( async (key) => {
                    consecutiveScorecards[key].sort((a,b) => {
                        return parseInt(b.year+b.week.padStart(2, 0)) - parseInt(a.year+a.week.padStart(2, 0))
                    })
                }));
            }//endif scorecards
        }//endif containsScorecard
                
        let localeStringDate = new Date().toLocaleString('en-US', {timeZone: clientTimeZone})
        var today = new Date(localeStringDate) 
        var currentWeek = today.getWeekNumber() - 1
        var currentYear = today.getFullYear()
        // deal with week 53 / year switch over
        if(currentWeek === 0){
            // this may need to be dynamic based on how many weeks Amazon decides are in a year
            // in 2020 there were 53 weeks
            // in 2021 there were 52 weeks
            currentWeek = 52
            currentYear--
        }

        const [scorecardDictionary, podDictionary, cxDictionary] = compileResults(jobs, group, tenantId, associateLookupMap)

        await Promise.all(rows.map( async (row, index) => {
            if(row.staff.id != null){
                var daId = row.staff.id
                // Update DA with transporter ID if empty
                var noTransporterId = row.staff.matchTransporterId === 'No Transporter ID'
                var importTransporterId = row.staff.importTransporterId

                if((noTransporterId || (importTransporterId != row.staff.matchTransporterId)) && (row.staff.matchStatus && row.staff.matchStatus != "Inactive - Terminated") ){
                    // disabling Update DA transporter ID - HERA-1004
                    // let item = {
                    //     transporterId: importTransporterId,
                    //     updatedAt: new Date().toISOString()
                    // }
                    // try{
                    //     let updateParams = buildUpdateParams(process.env.STAFF_TABLE, item, daId)
                    //     await updateDynamoRecord(updateParams);
                    // }
                    // catch(e){
                    //     console.log('---catch-2', e)
                    // }
                    row.staff.matchTransporterId = importTransporterId

                    // get updated da and add to map
                    // get existing message
                    let fields = ['group', 'transporterId','latestScorecard','keyFocusArea','firstName','lastName','alternateNames']
                    let getItemParams = buildGetItemParams(getTableName(Tables.STAFF_TABLE), fields, daId)
                    let getItemResult = await getDynamoRecord(getItemParams);
                    let updatedDa = getItemResult.Item
                    associateLookupMap[importTransporterId] = {id: row.staff.id, latestScorecard: updatedDa.latestScorecard, keyFocusArea: updatedDa.keyFocusArea, firstName: row.staff.matchFirstName, lastName: row.staff.matchLastName, transporterId: importTransporterId, alternateNames: updatedDa.alternateNames}
                }
                // update DA with alternate names
                let altNamesUpdated = null
                let da = associateLookupMap[row.staff.matchTransporterId]
                if(da != null && da != undefined) {
                    altNamesUpdated = da.alternateNames
                }
                let altNamesAdded = 0
                let badNames = ['not available', 'coming soon', 'unknown driver', 'null']
                
                let cleanedAltName = row.staff.importName.replace(/\s+/g, ' ')
                // HERA-1011
                if(altNamesUpdated == null){
                    let altName = cleanedAltName
                    altName = altName.toLowerCase()
                    if(!badNames.includes(altName)) {
                        altNamesUpdated = [cleanedAltName]
                        altNamesAdded++
                    }
                }
                else{
                    let altNameExists = altNamesUpdated.some((name) =>{
                        return name == cleanedAltName
                    })

                    if(!altNameExists){
                        // HERA-1011
                        let altName = cleanedAltName
                        altName = altName.toLowerCase()
                        if(!badNames.includes(altName)) {
                            altNamesUpdated.push(cleanedAltName)
                            altNamesAdded++
                        }

                    }
                }
                if(altNamesAdded > 0){
                    let item = {
                        id: row.staff.id,
                        alternateNames: altNamesUpdated
                    }
                    try{
                        let updateParams = buildUpdateParams(getTableName(Tables.STAFF_TABLE), item, row.staff.id)
                        await updateDynamoRecord(updateParams);
                    }
                    catch(e){
                        console.log('---catch-3', e)
                    }
                }
                
                // create individual DA Scorecard, POD, and CX records
                var scorecards = row.scorecards
                var pods = row.pods
                var cxs = row.cxs
                var transporterId = cleanTransporterId(row.staff.matchTransporterId)
                
                
                if(scorecards.length){
                    await Promise.all(scorecards.map( async (id) => {
                        // get scorecard from dictionary
                        var scorecard = scorecardDictionary[id]
                        if(!scorecard){
                            console.log({scorecardDictionary, nullScorecard:{id}})
                            return
                        }
                        var daPerformanceCounts = {}
                        let scorecardWeek = String(scorecard.week).padStart(2, '0')
                        let scorecardYear = String(scorecard.year)
                        let yearWeek = scorecardYear + scorecardWeek
                        let scoreCardId = tenantId + transporterId + yearWeek
                        let associateLatestScorecard = associateLookupMap[transporterId]?.latestScorecard
                        if(typeof associateLatestScorecard === "undefined"){
                            associateLatestScorecard = 0;
                        }
                        
                        let scoreCardItem = {
                            group: group,
                            matched: true,
                            matchedS: 'true',
                            week: scorecardWeek,
                            year: scorecardYear,
                            'matchedS#year#week': 'true#' + scorecardYear + '#' + scorecardWeek,
                            'year#week': scorecardYear + '#' + scorecardWeek,
                            name: scorecard.name,
                            transporterId: scorecard.transporterId,
                            overallTier: scorecard.overallTier,
                            delivered: scorecard.delivered,
                            keyFocusArea: scorecard.keyFocusArea,
                            ficoScore: scorecard.ficoScore,
                            seatbeltOffRate: scorecard.seatbeltOffRate,
                            cdf: scorecard.cdf,
                            ced: scorecard.ced,
                            dcr: scorecard.dcr,
                            dar: scorecard.dar,
                            dsb: scorecard.dsb,
                            swcPod: scorecard.swcPod,
                            swcCc: scorecard.swcCc,
                            swcSc: scorecard.swcSc,
                            swcAd: scorecard.swcAd,
                            dnrs: scorecard.dnrs,
                            podOpps: scorecard.podOpps,
                            ccOpps: scorecard.ccOpps,
                            speedingEventRate: scorecard.speedingEventRate,
                            distractionsRate: scorecard.distractionsRate,
                            followingDistanceRate: scorecard.followingDistanceRate,
                            signSignalViolationsRate: scorecard.signSignalViolationsRate,
                            harshBrakingRate: scorecard.harshBrakingRate, 
                            harshCorneringRate: scorecard.harshCorneringRate,
                            staffScoreCardStaffId: daId,
                            updatedAt: new Date().toISOString()
                        }
                        
                        try{
                            // update record, if that fails try creating
                            let updateParams = buildUpdateParams(getTableName(Tables.STAFF_SCORECARD_TABLE), scoreCardItem, scoreCardId)
                            const wasUpdated = await updateRecord(updateParams, scoreCardItem)
                            if(wasUpdated){
                                importCounts.scorecard.updated++
                                importCounts.scorecard.updatedDas[daId] = true
                            }
                        }
                        catch(e){
                            try{
                                // create record, if that fails, log error
                                scoreCardItem.createdAt =  new Date().toISOString()
                                scoreCardItem.id = scoreCardId
                                let createParams = {
                                    TableName: getTableName(Tables.STAFF_SCORECARD_TABLE),
                                    Item: scoreCardItem
                                };
                                await createDynamoRecord(createParams);
                                importCounts.scorecard.created++
                                importCounts.scorecard.createdDas[daId] = true
                            }
                            catch(e){
                                console.log('---catch-4', e)
                                let message = ''
                                try{
                                    if(e.errors) message = e.errors[0].message
                                    else if(e.message) message = e.message
                                    else if(e.data) message = e.data.message
                                    else message = e
                                }
                                catch(syntaxError){
                                    message = JSON.stringify(e)
                                }
                                let error = {
                                    id: scoreCardItem.id,
                                    msg: message,
                                    name: scoreCardItem.name,
                                    transporterID: scoreCardItem.transporterId,
                                    year: scoreCardItem.year, 
                                    week: scoreCardItem.week
                                }
                                importErrors.scorecard.push(error)
                                importCounts.scorecard.errors++
                                importCounts.scorecard.errorsDas[daId] = true
                            }
                        }

                        // Update DA with latest ScoreCard yearweek
                        if(associateLatestScorecard <= yearWeek){
                            let item = {
                                latestScorecard: yearWeek,
                                keyFocusArea: scorecard.keyFocusArea,
                                keyFocusAreaCompleted: false,
                                updatedAt: new Date().toISOString()
                            }
                            try{
                                let updateParams = buildUpdateParams(getTableName(Tables.STAFF_TABLE), item, daId)
                                let record = await updateDynamoRecord(updateParams);
                                associateLookupMap[transporterId].latestScorecard = record.Attributes.latestScorecard
                            }
                            catch(e){
                                console.log('---catch-5', e)
                            }
                        }
                        
                        if( !row.staff.matchStatus?.startsWith('Inactive') ) {
                            // create or update pending message with latest scorecard if this week's data
                            if( validateWeekForPendingMessages( currentWeek, scorecardWeek, handleImportsLastSaturday ) && (currentYear == scorecardYear) ){
                                await createPendingMessageRecord(scoreCardItem, null, tenant, proxy)
                                performanceCounts.message.attempts++
                            }

                            //create da issues and kudos
                            await createIssuesAndKudos(group, scoreCardItem, null, tenant, consecutiveScorecards, proxy)
                        }
                    }))
                } //endif scorecards.length

                if(pods.length){
                    await Promise.all(pods.map( async (id) => {
                        // get pod from dictionary
                        var pod = podDictionary[id]
                        if(!pod){
                            console.log({podDictionary, nullPod:{id}})
                            return
                        }
                        let podWeek = String(pod.week).padStart(2, '0')
                        let podYear = String(pod.year)
                        let yearWeek = podYear + podWeek
                        let podId = tenantId + transporterId + yearWeek

                        let item = {
                            group: group,
                            matched: true,
                            matchedS: 'true',
                            week: podWeek,
                            year: podYear,
                            'matchedS#year#week': 'true#' + podYear + '#' + podWeek,
                            'year#week': podYear + '#' + podWeek,
                            employeeName: pod.employeeName,
                            transporterId: pod.transporterId,
                            opportunities: pod.opportunities,
                            success: pod.success,
                            bypass: pod.bypass,
                            packageInHand: pod.packageInHand,
                            notClearlyVisible: pod.notClearlyVisible,
                            blurry: pod.blurry,
                            explicit: pod.explicit,
                            mailSlot: pod.mailSlot,
                            noPackage: pod.noPackage,
                            other: pod.other,
                            packageTooClose: pod.packageTooClose,
                            personInPhoto: pod.personInPhoto,
                            photoTooDark: pod.photoTooDark,
                            takenFromCar: pod.takenFromCar,
                            grandTotal: pod.grandTotal,
                            podQualityStaffId: daId,
                            updatedAt: new Date().toISOString()
                        }

                        try{
                            // update PodQuality record, if that fails, create record
                            let updateParams = buildUpdateParams(getTableName(Tables.STAFF_POD_TABLE), item, podId)
                            const wasUpdated = await updateRecord(updateParams, item)
                            if(wasUpdated){
                                importCounts.pod.updated++
                                importCounts.pod.updatedDas[daId] = true
                            }
                        }
                        catch(e){
                            try{
                                // create record, if that fails, log error
                                item.createdAt =  new Date().toISOString()
                                item.id = podId
                                let createParams = {
                                    TableName: getTableName(Tables.STAFF_POD_TABLE),
                                    Item: item
                                };
                                await createDynamoRecord(createParams);
                                importCounts.pod.created++
                                importCounts.pod.createdDas[daId] = true
                            }
                            catch(e){
                                console.log('---catch-6', e)
                                let message = ''
                                try{
                                    if(e.errors) message = e.errors[0].message
                                    else if(e.message) message = e.message
                                    else if(e.data) message = e.data.message
                                    else message = e
                                }
                                catch(syntaxError){
                                    message = JSON.stringify(e)
                                }
                                let error = {
                                    id: item.id,
                                    msg: message,
                                    name: item.name,
                                    transporterID: item.transporterId,
                                    year: item.year, 
                                    week: item.week
                                }
                                importErrors.pod.push(error)
                                importCounts.pod.errors++
                                importCounts.pod.errorsDas[daId] = true
                            }
                        }
                    }))
                } //endif pods.length

                if(cxs.length){
                    await Promise.all(cxs.map( async (id) => {
                        // get cx from dictionary
                        var cx = cxDictionary[id]
                        if(!cx){
                            console.log({cxDictionary, nullCx:{id}})
                            return
                        }
                        var daPerformanceCounts = {}
                        let cxWeek = String(cx.week).padStart(2, '0')
                        let cxYear = String(cx.year)
                        let yearWeek = cxYear + cxWeek
                        let cxId = tenantId + transporterId + yearWeek
                        let associateLatestScorecard = associateLookupMap[transporterId]?.latestScorecard
                        if(typeof associateLatestScorecard === "undefined"){
                            associateLatestScorecard = 0;
                        }
                        
                        let item = {
                            group: group,
                            matched: true,
                            matchedS: 'true',
                            week: cxWeek,
                            year: cxYear,
                            'matchedS#year#week': 'true#' + cxYear + '#' + cxWeek,
                            'year#week': cxYear + '#' + cxWeek,
                            name: cx.name,
                            transporterId: cx.transporterId,
                            positiveFeedback: cx.positiveFeedback,
                            negativeFeedback: cx.negativeFeedback,
                            deliveryWasGreat: cx.deliveryWasGreat,
                            careForOthers: cx.careForOthers,
                            deliveryWasntGreat: cx.deliveryWasntGreat,
                            totalDeliveries: cx.totalDeliveries,
                            respectfulOfProperty: cx.respectfulOfProperty,
                            followedInstructions: cx.followedInstructions,
                            friendly: cx.friendly,
                            aboveAndBeyond: cx.aboveAndBeyond,
                            deliveredWithCare: cx.deliveredWithCare,
                            thankMyDriver: cx.thankMyDriver,
                            mishandledPackage: cx.mishandledPackage,
                            drivingUnsafely: cx.drivingUnsafely,
                            driverUnprofessional: cx.driverUnprofessional,
                            notDeliveredToPreferredLocation: cx.notDeliveredToPreferredLocation,
                            staffCxFeedbackStaffId: daId,
                            updatedAt: new Date().toISOString(),
                            daTier: cx.daTier,
                            cdfScore:cx.cdfScore,
                            noFeedback: cx.noFeedback,
                            onTime: cx.onTime,
                            lateDelivery: cx.lateDelivery,
                            itemDamaged: cx.itemDamaged,
                            deliveredToWrongAddress: cx.deliveredToWrongAddress,
                            neverReceivedDelivery: cx.neverReceivedDelivery
                        }

                        try{
                            // update StaffCxFeedback record, if that fails, create record
                            let updateParams = buildUpdateParams(getTableName(Tables.STAFF_CX_TABLE), item, cxId)
                            const wasUpdated = await updateRecord(updateParams, item)
                            if(wasUpdated){
                                importCounts.cx.updated++
                                importCounts.cx.updatedDas[daId] = true
                            }
                        }
                        catch(e){
                            try{
                                // create record, if that fails, log error
                                item.createdAt =  new Date().toISOString()
                                item.id = cxId
                                let createParams = {
                                    TableName: getTableName(Tables.STAFF_CX_TABLE),
                                    Item: item
                                };
                                await createDynamoRecord(createParams);
                                importCounts.cx.created++
                                importCounts.cx.createdDas[daId] = true

                            }
                            catch(e){
                                console.log('---catch-7', e)
                                let message = ''
                                try{
                                    if(e.errors) message = e.errors[0].message
                                    else if(e.message) message = e.message
                                    else if(e.data) message = e.data.message
                                    else message = e
                                }
                                catch(syntaxError){
                                    message = JSON.stringify(e)
                                }
                                let error = {
                                    id: item.id,
                                    msg: message,
                                    name: item.name,
                                    transporterID: item.transporterId,
                                    year: item.year, 
                                    week: item.week
                                }
                                importErrors.cx.push(error)
                                importCounts.cx.errors++
                                importCounts.cx.errorsDas[daId] = true
                            }
                        }

                        if( !row.staff.matchStatus?.startsWith('Inactive') ) {
                            // create or update pending message with latest cx if this week's data
                            if( validateWeekForPendingMessages( currentWeek, cxWeek, handleImportsLastSaturday ) && (currentYear == cxYear) ){
                                item.isNewerThanLatestScoreCard = associateLatestScorecard < yearWeek
                                await createPendingMessageRecord(null, item, tenant, proxy)
                                performanceCounts.message.attempts++
                            }
                            //create da issues and kudos
                            await createIssuesAndKudos(group, null, item, tenant, null, proxy)
                        }
                    }))
                }//endif cxs
            }//endif staffId
        }))//endif promise
    }
    catch(e){
        console.log('---catch-8', e)
    }
    
    for (const pendingMessageId in pendingMessages) {
        const pendingMessageItem = pendingMessages[pendingMessageId]
        await createOrUpdateRecord(pendingMessageItem, pendingMessageId, 'message', getTableName(Tables.PENDING_MESSAGE_TABLE), performanceCounts)
    }
 
    let extractDataForStaff = {}
    if (performanceImports.issues){
        for (let key in performanceImports.issues) {
          if (performanceImports.hasOwnProperty(key)) {
            let issue = performanceImports.issues[key];
            const { staffId, date } = issue;
            const entryKey = `${staffId}-${date}`;
            if (!extractDataForStaff[entryKey]) {
              extractDataForStaff[entryKey] = { staffId, date };
            }
          }
        }
    }
    if(performanceImports.kudos){
        for (let key in performanceImports.kudos) {
          if (performanceImports.kudos.hasOwnProperty(key)) {
            let kudo =performanceImports.kudos[key];
            const { staffId, date } = kudo;
            const entryKey = `${staffId}-${date}`;
            if (!extractDataForStaff[entryKey]) {
              extractDataForStaff[entryKey] = { staffId, date };
            }
          }
        }
    }
    const staffIdAndDate = Object.values(extractDataForStaff)
   
    let oldIssuesAndKudos = {
        issues:{},
        kudos:{}
    }
    for(let i of staffIdAndDate){
        //get Kudos and Issues
        const kudos = await getKudosOrIssueRecords(i.staffId, i.date, getTableName(Tables.DA_KUDO_TABLE))
        //filter for scorecards and cx
        kudos.filter(item => item.id.includes('-s-') && importFiles.scorecard || item.id.includes('-cx-') && importFiles.customerFeedback  ).forEach(kudo => {
            oldIssuesAndKudos.kudos[kudo.id] =  kudo
        });
          
        const issues = await getKudosOrIssueRecords(i.staffId, i.date, getTableName(Tables.DA_ISSUE_TABLE))
          
        issues.filter(item => item.id.includes('-s-') && importFiles.scorecard || item.id.includes('-cx-') && importFiles.customerFeedback ).forEach(issue => {
            oldIssuesAndKudos.issues[issue.id] = issue;
        });
        
    }
 
    
    for(const [key, values] of Object.entries(oldIssuesAndKudos)){
        for(const [keyId, item] of Object.entries(values)){
            if(!performanceImports[key].hasOwnProperty(keyId)){
                //remove issues or kudos before imported for this file
              	if(key==='kudos'){
              	    await deleteIssueOrKudoRecord(item, keyId, 'kudo', getTableName(Tables.DA_KUDO_TABLE), performanceCounts, tenant, event.user)
              	}
             	if(key==='issues'){
             	    await deleteIssueOrKudoRecord(item, keyId, 'issue',getTableName(Tables.DA_ISSUE_TABLE), performanceCounts, tenant, event.user)
             	}
             	
            }
        }
    }
    for (const [key, values] of Object.entries(performanceImports)) {
        for (const [id, item] of Object.entries(values)) {
            if(key === 'issues'){
                await createUpdateIssueOrKudoRecord(item, id,  'issue', getTableName(Tables.DA_ISSUE_TABLE), performanceCounts)
            }else if(key === 'kudos'){
                await createUpdateIssueOrKudoRecord(item, id, 'kudo', getTableName(Tables.DA_KUDO_TABLE), performanceCounts)
            }
           
        }
    }

    // create Notification using GraphQL to trigger subscription
    importCounts.scorecard.updatedDas = Object.keys(importCounts.scorecard.updatedDas).length
    importCounts.scorecard.createdDas = Object.keys(importCounts.scorecard.createdDas).length
    importCounts.scorecard.errorsDas = Object.keys(importCounts.scorecard.errorsDas).length
    importCounts.pod.updatedDas = Object.keys(importCounts.pod.updatedDas).length
    importCounts.pod.createdDas = Object.keys(importCounts.pod.createdDas).length
    importCounts.pod.errorsDas = Object.keys(importCounts.pod.errorsDas).length
    importCounts.cx.updatedDas = Object.keys(importCounts.cx.updatedDas).length
    importCounts.cx.createdDas = Object.keys(importCounts.cx.createdDas).length
    importCounts.cx.errorsDas = Object.keys(importCounts.cx.errorsDas).length
    
    performanceCounts.kudo.updatedDas = Object.keys(performanceCounts.kudo.updatedDas).length
    performanceCounts.kudo.createdDas = Object.keys(performanceCounts.kudo.createdDas).length
    performanceCounts.kudo.errorsDas = Object.keys(performanceCounts.kudo.errorsDas).length
    performanceCounts.issue.updatedDas = Object.keys(performanceCounts.issue.updatedDas).length
    performanceCounts.issue.createdDas = Object.keys(performanceCounts.issue.createdDas).length
    performanceCounts.issue.errorsDas = Object.keys(performanceCounts.issue.errorsDas).length
    
    for (const job of jobs) {
        const results = job.results
        if(checkResults(results)){
            const resultsObj = JSON.parse(results)
            if(!resultsObj.alreadyUploaded){
                const copySource = `${process.env.STORAGE_HERA_BUCKETNAME}/public/${job.key}`
                const params = {
                    Bucket: process.env.STORAGE_HERA_BUCKETNAME,
                    CopySource: encodeURI(copySource),
                    Key: 'public/' + job.key,
                    Expires: null,
                    MetadataDirective: 'REPLACE',
                    TaggingDirective: 'REPLACE'
                }
                // Update file
                const response = await s3.copyObject(params).promise().catch((e) => console.error(e))
                if(response.CopyObjectResult){
                    increaseCount(importCounts, job.type, 'savedFiles')
                }
            }
        }else{
            increaseCount(importCounts, job.type, 'failedFiles')
        }
    }
    
    const coachingHistory = [
        {name: 'scorecard', key: 'scorecard', query: createCoachingHistory, counter: importCounts, errorLog: 'coachingHistorysScorecardResult Error: '},
        {name: 'podQualityFeedback', key: 'pod', query: createCoachingHistory, counter: importCounts, errorLog: 'coachingHistorysPodResult Error: '},
        {name: 'customerFeedback', key: 'cx', query: createCoachingHistory, counter: importCounts, errorLog: 'coachingHistorysCustomerResult Error: '},
        {name: 'kudo', key: 'kudo', query: createCoachingRecords, counter: performanceCounts, errorLog: 'Kudo createCoachingRecords Error: '},
        {name: 'issue', key: 'issue', query: createCoachingRecords, counter: performanceCounts, errorLog: 'Issue createCoachingRecords Error: '},
        {name: 'pendingMessage', key: 'message', query: createCoachingRecords, counter: performanceCounts, errorLog: 'Message createCoachingRecords Error: '},
    ]
    
    for (const record of coachingHistory) {
        try{
            let input = {
                id: generateUUID(),
                group: group,
                importId: importId,
                type: record.name,
                results: JSON.stringify(record.counter[record.key]),
            }
            await executeMutation(record.query, { input })
        }catch(e){
            console.error(record.errorLog, e)
        }
    }
    
    try {
    
        let input = {
            group: group,
            owner: owner,
            title: "Weekly performance import complete",
            description:  "Your import started " + new Date(parseInt(importId.split('_')[1])).toLocaleString('en-US', {timeZone: clientTimeZone}) + " is complete. Click here to view the results.",
            payload: JSON.stringify({name: 'WeeklyDataImport', params: {importId: importId, importCounts: importCounts, importErrors: importErrors, performanceCounts: performanceCounts}}),
            clickAction: 'navigate',
            isRead: false
        }
        await executeMutation(createNotification, {input})

    } catch(e) {

        console.log('---error', e)

        let input = {
            group: group,
            owner: owner,
            title: "Weekly performance import complete",
            description:  "Your import started is complete. Click here to view the results.",
            payload: JSON.stringify({name: 'WeeklyDataImport', params: {importId: importId, importCounts: importCounts, importErrors: importErrors, performanceCounts: performanceCounts}}),
            clickAction: 'navigate',
            isRead: false
        }
        await executeMutation(createNotification, {input})

    } finally{
        const response = {
            statusCode: 200,
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "*"
            }, 
            body: JSON.stringify({importId, importCounts, importErrors, performanceCounts})
        };
        return response;
    }
}

module.exports = { weeklyHandler }