

const {
    buildUpdateParams,
    createDynamoRecord,
    updateRecord,
    deleteDynamoRecord,
    getDynamoRecord,
    updateDynamoRecord,
    queryDynamoRecords
} = require('../dynamodbHelper.js')
/*CloudWatch Loggers and Metricts */
const { setCloudWatchMetrics } = require('../cloudWatchFunctionHelper.js')
const { CWMetrics } = require('../cloudWatchMetricHelper.js')
const { executeMutation } = require('../appSync.js')
const { getTableName } = require('../util.js')
const {createKudo,
    updateKudo,
    deleteKudo,
    createInfraction,
    updateInfraction,
    deleteInfraction
} = require('../graphql.js')
const { findIndexByBinarySearch } = require('../helper.js')

const { Tables } = require('../tables');

/*******************************************************************************
 * Main helper functions
 ******************************************************************************/
const checkResults = function(results){
    if(!results) return false
    const allKnownErrors = [
        "ERROR_METADATA", "ERROR_DSP_LEVEL", "ERROR_TRANSPORTERID", "ERROR_001_TRANSPORTERID",
        "No table data found", "Error importing your weekly performance data", "No table data found (summary tables)", 
        "ERROR_002_SCORECARD_VALIDATOR_001 (002.001)",
        "ERROR_002_SCORECARD_VALIDATOR_002 (002.002)",
        "ERROR_002_SCORECARD_VALIDATOR_003 (002.003)",
        "ERROR_002_SCORECARD_VALIDATOR_004 (002.004)",
        "ERROR_002_SCORECARD_VALIDATOR_005 (002.005)",
        "ERROR_002_SCORECARD_VALIDATOR_006 (002.006)",
        "ERROR_002_SCORECARD_VALIDATOR_007 (002.007)"
    ]
    if(results.constructor === String){
        return !allKnownErrors.some(error => results.includes(error))
    }
    return true
}
const matchTransporterId = function(transporterId, associateLookupMap, associateLookupMapKeys){
    if(transporterId){
        const foundAssociateByTransporterID = associateLookupMap[transporterId];
        if (foundAssociateByTransporterID) return transporterId;
        const truncatedTransporterId =  transporterId.substr(0, transporterId.length - 1)
        // Fallback to lookup partial matches for TransporterID that are send incomplete
        const foundIndex = findIndexByBinarySearch(associateLookupMapKeys, (tID) => (
            tID.includes(truncatedTransporterId)
                ? 0 // TransporterID index found
                : (truncatedTransporterId.localeCompare(tID)) // Decide which side to search(1:right | -1:left)
        ));
        if (foundIndex !== -1) {
            return associateLookupMapKeys[foundIndex];
        }
    }
    return transporterId
}

const increaseCount = function (importCounts, type, key){
    switch(type){
        case 'scorecard': importCounts.scorecard[key]++; break
        case 'podQualityFeedback': importCounts.pod[key]++; break
        case 'customerFeedback': importCounts.cx[key]++; break
    }
}

/*******************************************************************************
 * Pending Messages, DA Kudo and Issues helper functions
 ******************************************************************************/
const createOrUpdateRecord = async function (item, id, type, table, performanceCounts){
    try{
        // update record, if that fails, create record
        const updateItem = {...item, id: undefined}
        let updateParams = buildUpdateParams(table, updateItem, id)
        const wasUpdated = await updateRecord(updateParams, updateItem)
        if(wasUpdated){
            performanceCounts[type].updated++
            if(performanceCounts[type].updatedDas){
                performanceCounts[type].updatedDas[updateItem.staffId] = true
            }
        }
    }catch(e){
        try{
            // create record, if that fails, log error
            item.createdAt = new Date().toISOString()
            item.updatedAt = new Date().toISOString()
            item.id = id
            let createParams = {
                TableName: table,
                Item: item
            };
            await createDynamoRecord(createParams);
            performanceCounts[type].created++
            if(performanceCounts[type].createdDas){
                performanceCounts[type].createdDas[item.staffId] = true
            }
        }catch(e){
            console.log('---catch-13', e)
            let message = ''
            try{
                if(e.errors) message = e.errors[0].message
                else if(e.message) message = e.message
                else if(e.data) message = e.data.message
                else message = e
            }
            catch(syntaxError){
                message = JSON.stringify(e)
            }
            let error = {
                id: id,
                msg: message,
                name: item.name,
                transporterID: item.transporterId,
                year: item.year, 
                week: item.week
            }
            performanceCounts[type].errorMsgs && performanceCounts[type].errorMsgs.push(error)
            performanceCounts[type].errors++
            if(performanceCounts[type].errorsDas) performanceCounts[type].errorsDas[item.staffId] = true
        }
    }
    return
}

/*******************************************************************************
 *  DA Kudo and Issues Create and Update
 ******************************************************************************/
const createUpdateIssueOrKudoRecord = async function(item, id, type, table, performanceCounts){
    
    try{
        //get
        let params = {
            Key: {
                id: id
            },
            TableName: table
        }
        const getResult = await getDynamoRecord(params)
       
        const oldValues = getResult.Item || {}
        if (Object.keys(oldValues).length != 0) {
            //exist result
            
            const fieldsThatShouldNotBeCompared = ['updatedAt']
           
            const fields = Object.keys(item).filter(field => !fieldsThatShouldNotBeCompared.includes(field))
            const needUpdated = fields.some(field => oldValues[field] !== item[field])
           
            if(needUpdated){
                //update
                //no need to "updatedAt" using GraphQL
                delete item.updatedAt
                const updateItem = {...item, id: id}
                const updateMutations = {
                    'issue': updateInfraction,
                    'kudo': updateKudo
                }
                const selectedMutation = updateMutations[type]
                //update issues or kudos using GraphQL to trigger subscription
                await executeMutation(selectedMutation, {input: updateItem})

                performanceCounts[type].updated++
                if(performanceCounts[type].updatedDas){
                    performanceCounts[type].updatedDas[updateItem.staffId] = true
                }
            }
        }else{
            // create new record
            item.id = id
            const createMutations = {
                'issue': createInfraction,
                'kudo': createKudo
            }
            const selectedMutation = createMutations[type]
            
            //no need to "updatedAt" using GraphQL
            delete item.updatedAt
            const input = {...item}
            //create issues or kudos using GraphQL to trigger subscription
            await executeMutation(selectedMutation, {input})
            
            performanceCounts[type].created++
            if(performanceCounts[type].createdDas){
                performanceCounts[type].createdDas[item.staffId] = true
            }
        }
    
    }catch(e){
        console.log('Error', e)
        let message = ''
        try{
            if(e.errors) message = e.errors[0].message
            else if(e.message) message = e.message
            else if(e.data) message = e.data.message
            else message = e
        }
        catch(syntaxError){
            message = JSON.stringify(e)
        }
        const [year, week] = id.split('-')
        let error = {
            id: id,
            msg: message,
            name: item.name,
            year: year, 
            week: week
        }
        performanceCounts[type].errorMsgs && performanceCounts[type].errorMsgs.push(error)
        performanceCounts[type].errors++
        if(performanceCounts[type].errorsDas) performanceCounts[type].errorsDas[item.staffId] = true
    }
    return
}

/*******************************************************************************
 *  DA Kudo and Issues delete
 ******************************************************************************/

const deleteIssueOrKudoRecord = async function(item, id, type, table, performanceCounts, tenant, user){
    const [year, week] = id.split('-')
    try{
        //get
        let params = {
            Key: {
                id: id
            },
            TableName: table
        }
        const getResult = await getDynamoRecord(params)
        const values = getResult.Item || {}
        if (Object.keys(values).length != 0) {
            //exist result
           
            if(values.infractionCounselingId){
                //not delete, count as error
                const da = await getAssociate(item.staffId)
                let error = {
                    id: id,
                    msg: 'Issue should not have previously been created, but cannot be automatically deleted because it has been linked to a Counseling.',
                    name: `${da.firstName} ${da.lastName}`,
                    year: year,
                    week: week
                }
               
                let metrics = {
                    functionName: 'deleteIssueOrKudoRecord',
                    codeText: 'ERROR_004',
                    codeNumber: '004.001',
                    tenant: { id:tenant.id, companyName: tenant.companyName, group: tenant.group},
                    err: error.msg,
                    issue: { id: id, counselingId: values.infractionCounselingId },
                    user: { id: user.id, userName: user.userName },
                    associate: { id: da.id, name: `${da.firstName} ${da.lastName}`}
                }
               
                performanceCounts[type].errorMsgs && performanceCounts[type].errorMsgs.push(error)
                performanceCounts[type].errors++
                if(performanceCounts[type].errorsDas) performanceCounts[type].errorsDas[item.staffId] = true
                const cwm = new CWMetrics(process.env.METRIC_GROUP_NAME)
                setCloudWatchMetrics(metrics, cwm)
                await cwm.sendToCloudWatch()
            }else{
                //delete
                const deleteMutations = {
                    'issue': deleteInfraction,
                    'kudo': deleteKudo
                }
                const selectedMutation = deleteMutations[type]
                //execute mutations for subscription of issues and kudos
                await executeMutation(selectedMutation, {input:{id: id}})
        
                performanceCounts[type].deleted++
            }
        }
    
    }catch(e){
        console.log('Error', e)
        let message = ''
        try{
            if(e.errors) message = e.errors[0].message
            else if(e.message) message = e.message
            else if(e.data) message = e.data.message
            else message = e
        }
        catch(syntaxError){
            message = JSON.stringify(e)
        }
        let error = {
            id: id,
            msg: message,
            year: year,
            week: week
        }
        performanceCounts[type].errorMsgs && performanceCounts[type].errorMsgs.push(error)
        performanceCounts[type].errors++
        if(performanceCounts[type].errorsDas) performanceCounts[type].errorsDas[item.staffId] = true
    }
    return
}

async function getAssociate(id){
    let associate = {
        id: '',
        firstName: '',
        lastName: ''
    }
    try {
        let params = {
            Key: {
                id: id
            },
            ProjectionExpression: "id, firstName, lastName",
            TableName: getTableName(Tables.STAFF_TABLE)
        }
        const result = await getDynamoRecord(params)
        associate = result.Item
    } catch (e) {
        console.log('Error in function getAssociate', e)
    } finally{
        return associate
    }
}


module.exports = { matchTransporterId, increaseCount, checkResults, createOrUpdateRecord, createUpdateIssueOrKudoRecord, deleteIssueOrKudoRecord }