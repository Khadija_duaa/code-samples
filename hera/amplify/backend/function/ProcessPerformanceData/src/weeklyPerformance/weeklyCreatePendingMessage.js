
const { getPendingMessageById } = require('../helper.js')
const { getStartAndEndOfWeek, shortDateFormat, getTableName } = require('../util.js')
/*******************************************************************************
 * Coaching messages helper functions
 ******************************************************************************/
 const {
    buildGetItemParams,
    buildQueryParams,
    queryDynamoRecords,
    getDynamoRecord,
} = require('../dynamodbHelper.js')

const { Tables } = require('../tables');

const createPendingMessageRecord = async function(scorecard, cx, tenant, proxy){
    var messageStringWeeklyCO = '';
    var messageStringWeeklyPR = '';
    var messageStringDailyCO = '';
    var messageStringDailyPR = '';
    var numberOfWeeklyCO = 0
    var numberOfWeeklyPR = 0
    var numberOfDailyCO = 0
    var numberOfDailyPR = 0
    var yesterday = new Date()

    var levels = []
    levels['Fantastic'] = 4
    levels['Great']     = 3
    levels['Fair']      = 2
    levels['Poor']      = 1

    var staffId = null
    if(scorecard) staffId = scorecard.staffScoreCardStaffId
    else if(cx) staffId = cx.staffCxFeedbackStaffId
    else{
        return {status: 'noop', id: 'noop'}
    }

    var cxFeedback = null
    yesterday.setDate(yesterday.getDate() - 1)
    yesterday = yesterday.toISOString().split('T')[0]


    //FIND EXISTING MESSAGE
    const pendingMessages = proxy.pendingMessages
    var pendingMessageId = tenant.id + staffId
    var existingMessage = {}
    try{
        // get existing message
        if(pendingMessages[pendingMessageId]){
            existingMessage = pendingMessages[pendingMessageId]
        }else{
            existingMessage = await getPendingMessageById(pendingMessageId)
        }
    }
    catch(e){
        console.log('---catch-9', e);
        return {status: 'noop', id: 'noop'}
    }


    // determine which phrases to include in message based on scorecard and CX data

    // ------------ CUSTOMER FEEDBACK ------------
    // will only fire if the CX feedback is the same week or newer as the latest scorecard for a staff member
    if(cx){
        // exit if all cx messages disabled
        // if(!tenant.coachingPositiveFeedbackCO && !tenant.coachingPositiveFeedbackPR){
        //     return {status: 'noop', id: 'noop'}
        // }

        // if the cx document is newer than the latest scorecard, create a new pending message
        if(cx.isNewerThanLatestScoreCard){
            // --- Coaching Opportunities ---

            let coDaTier = levels[tenant.coachingDaTierRatingCO]
            let prDaTier = levels[tenant.coachingDaTierRatingPR]
            let cxDaTier = levels[cx.daTier]
    
            if((tenant.coachingDaTierCO) && ((cx.daTier == 'Coming Soon') || cx.cdfScore == 'Comming Soon')) {
                console.log('---1x')
                messageStringWeeklyCO = messageStringWeeklyCO + `\n- Your Customer Feedback score is Coming Soon.\n`
                numberOfWeeklyCO++
            }
    
    
    
            if((tenant.coachingDaTierCO) && (cxDaTier <= coDaTier)) {
                console.log('---2x')
                messageStringWeeklyCO = messageStringWeeklyCO + `\n- Your overall Customer Feedback tier was ${cx.daTier}.\n`
                numberOfWeeklyCO++
            }
    
            if((tenant.coachingCdfScoreCO) && (parseFloat(cx.cdfScore) < tenant.coachingCdfScoreThreshold)) {
                console.log('---3x')
                messageStringWeeklyCO = messageStringWeeklyCO + `\n- Your Customer Feedback Score was ${cx.cdfScore}.\n`
                numberOfWeeklyCO++
            }
    
            if((tenant.coachingDaTierPR) && (cxDaTier >= prDaTier)) {
                console.log('---4x')
                messageStringWeeklyPR = messageStringWeeklyPR + `\n- Congratulations, your overall Customer Feedback tier was ${cx.daTier}.\n`
                numberOfWeeklyPR++
            }
    
            if((tenant.coachingCdfScorePR) && (parseFloat(cx.cdfScore) >= tenant.coachingCdfScoreThresholdIssue)) {
                console.log('---5x')
                messageStringWeeklyPR = messageStringWeeklyPR + `\n- Great job, your Customer Feedback Score was ${cx.cdfScore}.\n`
                numberOfWeeklyPR++
            }

            // Positive Feedback
            if(true || (tenant.coachingPositiveFeedbackPR)){
                let counterIncrease = false
                let partialMessage = ''
                if(cx.deliveryWasGreat > 0) {
                    partialMessage = partialMessage + `${cx.deliveryWasGreat} Delivery Was Great.\n`
                    counterIncrease = true
                }
                if(cx.respectfulOfProperty > 0) {
                    partialMessage = partialMessage + `${cx.respectfulOfProperty} Respectful of Property.\n`
                    counterIncrease = true
                }  
                if(cx.followedInstructions > 0) {
                    partialMessage = partialMessage + `${cx.followedInstructions} Followed Instructions.\n`
                    counterIncrease = true 
                }
                if(cx.friendly > 0) {
                    partialMessage = partialMessage + `${cx.friendly} Friendly.\n`
                    counterIncrease = true
                }
                if(cx.aboveAndBeyond > 0) {
                    partialMessage = partialMessage + `${cx.aboveAndBeyond} Above and Beyond.\n`
                    counterIncrease = true
                }
                if(cx.deliveredWithCare > 0) {
                    partialMessage = partialMessage + `${cx.deliveredWithCare} Delivered with Care.\n`
                    counterIncrease = true
                }
                if(cx.thankMyDriver > 0) {
                    partialMessage = partialMessage + `${cx.thankMyDriver} Thank My Driver.\n`
                    counterIncrease = true
                }
                if(counterIncrease){
                    messageStringWeeklyPR = messageStringWeeklyPR + `\n- ` + partialMessage
                    numberOfWeeklyPR++
                } 
            }

            // Negative Feedback
            if(true || (tenant.coachingPositiveFeedbackCO)){
                let counterIncrease = false
                let partialMessage = ''
                if(cx.deliveryWasntGreat > 0) {
                    partialMessage = partialMessage + `${cx.deliveryWasntGreat} Delivery was not so Great.\n`
                    counterIncrease = true
                }
                if(cx.mishandledPackage > 0) {
                    partialMessage = partialMessage + `${cx.mishandledPackage} DA Mishandled Package${cx.mishandledPackage > 1 ? 's' : ''}.\n`
                    counterIncrease = true
                }  
                if(cx.driverUnprofessional > 0) {
                    partialMessage = partialMessage + `${cx.driverUnprofessional} DA was Unprofessional.\n`
                    counterIncrease = true 
                }
                if(cx.notDeliveredToPreferredLocation > 0) {
                    partialMessage = partialMessage + `${cx.notDeliveredToPreferredLocation} DA did not follow my delivery instructions.\n`
                    counterIncrease = true
                }
                if(cx.deliveredToWrongAddress > 0) {
                    partialMessage = partialMessage + `${cx.deliveredToWrongAddress} Delivered to Wrong Address.\n`
                    counterIncrease = true
                }
                if(cx.neverReceivedDelivery > 0) {
                    partialMessage = partialMessage + `${cx.neverReceivedDelivery} Never Received Delivery.\n`
                    counterIncrease = true
                }
                if(counterIncrease){
                    messageStringWeeklyCO = messageStringWeeklyCO + `\n- ` + partialMessage
                    numberOfWeeklyCO++
                } 
            }
            
            // --- Positive reinforcement ---
            // Positive Feedback
            // if((tenant.coachingPositiveFeedbackPR)){
            //     numberOfWeeklyPR++
            // }

            // --- Positive reinforcement ---
            // Positive Feedback
            // if((tenant.coachingPositiveFeedbackPR) && (parseFloat(cx.positiveFeedback) >= tenant.coachingPositiveFeedbackThreshold)){
            //     messageStringWeeklyPR = messageStringWeeklyPR + `\n- Congratulations, you received ${cx.positiveFeedback} positive feedback from customers last week.\n`
            //     numberOfWeeklyPR++
            // }
            
        }
        // if the cx document is the same week as the latest scorecard, update the existing pending message
        else{
            // get the latest scorecard so the messages can be updated
            var existingScorecard = []
            var lastEvaluatedKey = null
            let queryParams = {}
            let fields = []
            do{
                fields = ['group','matched','matchedS','messageHasBeenSent','week','year','name','transporterId','overallTier','delivered','keyFocusArea','ficoScore','seatbeltOffRate','dcr','dar','swcPod','swcCc','swcSc','swcAd','dnrs','podOpps','ccOpps','speedingEventRate','distractionsRate','followingDistanceRate','signSignalViolationsRate','harshBrakingRate','harshCorneringRate']
                queryParams = buildQueryParams(getTableName(Tables.STAFF_SCORECARD_TABLE), fields, tenant.group, 'byGroupUnmatched', lastEvaluatedKey)
                // add filter and sort key expression
                queryParams.ExpressionAttributeNames['#sk'] = 'matchedS#year#week'
                queryParams.ExpressionAttributeNames['#f'] = 'transporterId'
                queryParams.ExpressionAttributeValues[':sk'] = `true#${cx.year}#${cx.week}`
                queryParams.ExpressionAttributeValues[':f'] = cx.transporterId
                queryParams.KeyConditionExpression += ' AND #sk = :sk'
                queryParams.FilterExpression = '#f = :f'
                let queryResult = await queryDynamoRecords(queryParams);
                let items = queryResult.Items
                lastEvaluatedKey = queryResult.LastEvaluatedKey
                existingScorecard = [...existingScorecard, ...items]
            }while(lastEvaluatedKey)
            scorecard = existingScorecard[0]

            // if that fails, try single digit week - this should only have to be temporary after all weeks are padded starting week 7 2021
            if(!scorecard){
                do{
                    queryParams.ExpressionAttributeValues[':sk'] = `true#${cx.year}#${String(parseInt(cx.week))}`
                    let queryResult = await queryDynamoRecords(queryParams);
                    let items = queryResult.Items
                    lastEvaluatedKey = queryResult.LastEvaluatedKey
                    existingScorecard = [...existingScorecard, ...items]
                }while(lastEvaluatedKey)
                scorecard = existingScorecard[0]
            }

            // if that fails, try using the first 13 characters of Transporter ID
            if(!scorecard){
                do{
                    queryParams.FilterExpression = 'begins_with(#f, :f)'
                    let queryResult = await queryDynamoRecords(queryParams);
                    let items = queryResult.Items
                    lastEvaluatedKey = queryResult.LastEvaluatedKey
                    existingScorecard = [...existingScorecard, ...items]
                }while(lastEvaluatedKey)
                scorecard = existingScorecard[0]
            }

            cxFeedback = cx
            if(!cxFeedback) {
                console.log('Staff Transporter ID malformed', {
                    tenantId: tenant.id, 
                    transporterId: cx.transporterId, 
                    week: cx.week, 
                    year: cx.year
                })
            }
        }
    }

    
    // ------------ SCORECARD ------------
    if(scorecard){
        // exit if all scorecard messages disabled
        if( !tenant.coachingFicoCO && !tenant.coachingDcrCO && !tenant.coachingDarCO &&
            !tenant.coachingFicoPR && !tenant.coachingDcrPR && !tenant.coachingDarPR&&
            !tenant.coachingSwcPodCO && !tenant.coachingSwcCcCO && !tenant.coachingSwcScCO &&
            !tenant.coachingSwcPodPR && !tenant.coachingSwcCcPR && !tenant.coachingSwcScPR &&
            !tenant.coachingSwcAdCO && !tenant.coachingSeatbeltOffCO && !tenant.coachingSpeedingEventCO && !tenant.coachingHarshBrakingRateCO && !tenant.coachingHarshCorneringRateCO &&
            !tenant.coachingSwcAdPR && !tenant.coachingSeatbeltOffPR && !tenant.coachingSpeedingEventPR && !tenant.coachingHarshBrakingRatePR && !tenant.coachingHarshCorneringRatePR && 
            !tenant.coachingSeatbeltCO && !tenant.coachingSseCO && !tenant.coachingPositiveFeedbackCO &&
            !tenant.coachingSeatbeltPR && !tenant.coachingSsePR && !tenant.coachingPositiveFeedbackPR && 
            !tenant.coachingDistractionsRateCO && !tenant.coachingDistractionsRatePR && !tenant.coachingFollowingDistanceRateCO &&
            !tenant.coachingFollowingDistanceRatePR && !tenant.coachingSignSignalViolationsRateCO && !tenant.coachingSignSignalViolationsRatePR &&
            !tenant.coachingDnrCO && !tenant.coachingDnrPR &&
            !tenant.coachingDaTierCO && !tenant.coachingDaTierPR &&
            !tenant.coachingCdfScoreCO && !tenant.coachingCdfScorePR){
            return {status: 'noop', id: 'noop'}
        }

        // --- Coaching Opportunities ---
        // FICO
        if((tenant.coachingFicoCO) && (parseFloat(scorecard.ficoScore) < parseFloat(tenant.coachingFicoThreshold) )){
            messageStringWeeklyCO = messageStringWeeklyCO + `\n- Your FICO score was ${scorecard.ficoScore} last week. ${tenant.coachingFicoThreshold} is the minimum score for "Fantastic".\n`
            numberOfWeeklyCO++
        }
        // DCR
        if((tenant.coachingDcrCO) && (parseFloat(scorecard.dcr) < parseFloat(tenant.coachingDcrThreshold))){
            messageStringWeeklyCO = messageStringWeeklyCO + `\n- Your Delivery Completion Rate didn't make it to "Fantastic" last week. You can fix this by delivering all of your packages.\n`
            numberOfWeeklyCO++
        }
        // DAR
        if((tenant.coachingDarCO) && (parseFloat(scorecard.dar) < parseFloat(tenant.coachingDarThreshold))){
            messageStringWeeklyCO = messageStringWeeklyCO + `\n- Too many customers did not receive their packages last week. To fix this, try to hide packages behind a planter or under a doormat.\n`
            numberOfWeeklyCO++
        }

        //DSB
        if((tenant.coachingDsbCO) && (parseFloat(scorecard.dsb) > parseFloat(tenant.coachingDsbThreshold))){
            messageStringWeeklyCO = messageStringWeeklyCO + `\n- Your Delivery Success Behaviors score was ${scorecard.dsb}, which is not Fantastic. Please remember to check name/address, deliver near geo-pin, take photos, follow CX instructions, never deliver to mailboxes, hide packages, scan individually, mark correct reason codes, and contact customer if necessary.\n`
            numberOfWeeklyCO++
        }

        // DNR${scorecard.dnrs} 
        if((tenant.coachingDnrCO) && (parseFloat(scorecard.dnrs) > parseFloat(tenant.coachingDnrThreshold))){
            messageStringWeeklyCO = messageStringWeeklyCO + `\n- You had ${scorecard.dnrs} DNR(s) - it is critical to follow delivery notes. Do your best to ensure packages aren’t visible to passers by & contact CX & Amazon Support if there are any delivery problems.\n`
            numberOfWeeklyCO++
        }

        // SWC-POD
        if((tenant.coachingSwcPodCO) && (parseFloat(scorecard.swcPod) < parseFloat(tenant.coachingSwcPodThreshold))){
            // get POD stats needed in message
            let podQualityID = tenant.id + scorecard.transporterId + scorecard.year + scorecard.week
            let pod = {}
            let fields = ['blurry','personInPhoto','noPackage','takenFromCar','packageInHand','notClearlyVisible','packageTooClose','photoTooDark','other','grandTotal']
            let getItemParams = buildGetItemParams(getTableName(Tables.STAFF_POD_TABLE), fields, podQualityID)
            let getItemResult = await getDynamoRecord(getItemParams);
            pod = getItemResult.Item
            
            
            messageStringWeeklyCO = messageStringWeeklyCO + `\n- You had ${(100 - parseFloat(scorecard.swcPod)).toFixed(2)}% total delivery photo rejects.\n`
            if(pod && (pod.blurry || pod.personInPhoto || pod.noPackage || pod.takenFromCar || pod.packageInHand || pod.notClearlyVisible || pod.packageTooClose || pod.photoTooDark || pod.other)){
                if(parseFloat(pod.blurry) > 0) messageStringWeeklyCO = messageStringWeeklyCO + ` ${pod.blurry} blurry.\n`
                if(parseFloat(pod.personInPhoto) > 0) messageStringWeeklyCO = messageStringWeeklyCO + ` ${pod.personInPhoto} human in the picture.\n`
                if(parseFloat(pod.noPackage) > 0) messageStringWeeklyCO = messageStringWeeklyCO + ` ${pod.noPackage} no package detected.\n`
                if(parseFloat(pod.takenFromCar) > 0) messageStringWeeklyCO = messageStringWeeklyCO + ` ${pod.takenFromCar} package in car.\n`
                if(parseFloat(pod.packageInHand) > 0) messageStringWeeklyCO = messageStringWeeklyCO + ` ${pod.packageInHand} package in hand.\n`
                if(parseFloat(pod.notClearlyVisible) > 0) messageStringWeeklyCO = messageStringWeeklyCO + ` ${pod.notClearlyVisible} package not clearly visible - locker/other concealment.\n`
                if(parseFloat(pod.packageTooClose) > 0) messageStringWeeklyCO = messageStringWeeklyCO + ` ${pod.packageTooClose} package too close.\n`
                if(parseFloat(pod.photoTooDark) > 0 ) messageStringWeeklyCO = messageStringWeeklyCO + ` ${pod.photoTooDark} photo too dark.\n`
                if(parseFloat(pod.other) > 0) messageStringWeeklyCO = messageStringWeeklyCO + ` ${pod.other} other.\n`
            }
            messageStringWeeklyCO = messageStringWeeklyCO + `Please ask for help to have less rejects.\n`
            numberOfWeeklyCO++
        }
        // SWC-CC
        if((tenant.coachingSwcCcCO) && (parseFloat(scorecard.swcCc) < parseFloat(tenant.coachingSwcCcThreshold)) && parseFloat(scorecard.ccOpps) > 0){
            messageStringWeeklyCO = messageStringWeeklyCO + `\n- Your Contact Compliance score was below "Fantastic". You can fix this by first marking the package "undeliverable" then the Rabbit will prompt you to call or text the customer.\n`
            numberOfWeeklyCO++
        }
        // SWC-SC
        if((tenant.coachingSwcScCO) && (parseFloat(scorecard.swcSc) < parseFloat(tenant.coachingSwcScThreshold))){
            messageStringWeeklyCO = messageStringWeeklyCO + `\n- Your Scan Compliance percentage was below "Fantastic". Please make sure you scan every package at the customer destination.\n`
            numberOfWeeklyCO++
        }
        // SWC-AD
        if((tenant.coachingSwcAdCO) && (parseFloat(scorecard.swcAd) > parseFloat(tenant.coachingSwcAdThreshold))){
            messageStringWeeklyCO = messageStringWeeklyCO + `\n- Amazon believes you marked "delivered to customer" too many times. Please be sure to only use this option when the customer actually takes the package in their hands.\n`
            numberOfWeeklyCO++
        }
        // // Seatbelt-Off Rate
        //originally compared to: parseFloat(tenant.coachingSeatbeltOffThreshold)
        if((tenant.coachingSeatbeltOffCO) && (parseFloat(scorecard.seatbeltOffRate) > parseFloat(tenant.coachingSeatbeltOffThreshold))){
            messageStringWeeklyCO = messageStringWeeklyCO + `\n- WARNING: You averaged ${scorecard.seatbeltOffRate} instances per route of not wearing your seatbelt while traveling last week. Please remember to buckle your seatbelt before putting the vehicle in drive.\n`
            numberOfWeeklyCO++
        }
        // // Speeding Event Rate
        //originally compared to: parseFloat(tenant.coachingSpeedingEventThreshold)
        if((tenant.coachingSpeedingEventCO) && (parseFloat(scorecard.speedingEventRate) > parseFloat(tenant.coachingSpeedingEventThreshold))){

            //Check for existing message for daily positive reinforcement, removes it if it exists
            if(existingMessage != null && existingMessage.bodyTextDailyPR && existingMessage.bodyTextDailyPR.includes(`\n- You practiced safe driving and followed all posted speed limits.\n`)){
                const regex = /\n- You practiced safe driving and had a "Fantastic" week by following all posted speed limits.\n/i;
                existingMessage.bodyTextDailyPR = existingMessage.bodyTextDailyPR.replace(regex, '') 
            }

            messageStringWeeklyCO = messageStringWeeklyCO + `\n- WARNING: You averaged ${scorecard.speedingEventRate} instances of speeding per route last week. It is required to follow all posted speed limits.\n`
            numberOfWeeklyCO++
        }

        // // Harsh Braking Rate
        //originally compared to: parseFloat(tenant.coachingHarshBrakingRateThreshold)
        if((tenant.coachingHarshBrakingRateCO) && (parseFloat(scorecard.harshBrakingRate) > parseFloat(tenant.coachingHarshBrakingRateThreshold))){
            messageStringWeeklyCO = messageStringWeeklyCO + `\n- WARNING: Your harsh breaking was not Fantastic on the scorecard. To improve your score ${scorecard.harshBrakingRate}, allow more distance between your vehicle and the vehicle in front of you, and be sure to break early enough for red lights and stop signs.\n`
            numberOfWeeklyCO++
        }

        // // Harsh Cornering Rate
        //originally compared to: parseFloat(tenant.coachingHarshCorneringRateThreshold)
        if((tenant.coachingHarshCorneringRateCO) && (parseFloat(scorecard.harshCorneringRate) > parseFloat(tenant.coachingHarshCorneringRateThreshold))){
            messageStringWeeklyCO = messageStringWeeklyCO + `\n- WARNING: Your harsh cornering was not Fantastic on the scorecard ${scorecard.harshCorneringRate}. To improve your score, be sure to slow down when you're taking corners.\n`
            numberOfWeeklyCO++
        }
        
        //Distractions Rate
        if((tenant.coachingTraningRemainingCO) && (parseFloat(scorecard.distractionsRate) > parseFloat(tenant.coachingDistractionsRateThreshold))){
            messageStringWeeklyCO = messageStringWeeklyCO + `\n- WARNING: You had distraction violations last week which lead to you not earning fantastic.\n`
            numberOfWeeklyCO++
        }

        //Following Distance Rate
        if((tenant.coachingFollowingDistanceRateCO) && (parseFloat(scorecard.followingDistanceRate) > parseFloat(tenant.coachingFollowingDistanceRateThreshold))){
            messageStringWeeklyCO = messageStringWeeklyCO + `\n- WARNING: You had following distance violations last week which lead to you not earning fantastic.\n`
            numberOfWeeklyCO++
        }

        //Sign Signal Violations Rate
        if((tenant.coachingSignSignalViolationsRateCO) && (parseFloat(scorecard.signSignalViolationsRate) > parseFloat(tenant.coachingSignSignalViolationsRateThreshold))){
            messageStringWeeklyCO = messageStringWeeklyCO + `\n- WARNING: You had sign/signal violations last week which lead to you not earning fantastic.\n`
            numberOfWeeklyCO++
        }
        
        // --- Positive reinforcement ---
        // FICO
        if((tenant.coachingFicoPR) && (parseFloat(scorecard.ficoScore) >= parseFloat(tenant.coachingFicoThreshold))){
            messageStringWeeklyPR = messageStringWeeklyPR + `\n- You have a FICO score of ${scorecard.ficoScore} last week, which is "Fantastic"!\n`
            numberOfWeeklyPR++
        }
        // DCR
        if((tenant.coachingDcrPR) && (parseFloat(scorecard.dcr) >= parseFloat(tenant.coachingDcrThreshold))){
            messageStringWeeklyPR = messageStringWeeklyPR + `\n- Your Delivery Completion Rate is "Fantastic" last week! Congratulations on delivering so many smiles.\n`
            numberOfWeeklyPR++
        }
        // DAR
        if((tenant.coachingDarPR) && (parseFloat(scorecard.dar) >= parseFloat(tenant.coachingDarThreshold))){
            messageStringWeeklyPR = messageStringWeeklyPR + `\n- ${scorecard.dar == '100' ? 'All' : 'Most'} customers have indicated that they received their packages last week, and your DAR is "Fantastic"!\n`
            numberOfWeeklyPR++
        }
        // DSB
        if((tenant.coachingDsbPR) && (parseFloat(scorecard.dsb) <= parseFloat(tenant.coachingDsbThresholdPR))){
            messageStringWeeklyPR = messageStringWeeklyPR + `\n- Outstanding score for Delivery Success Behaviors at ${scorecard.dsb}!\n`
            numberOfWeeklyPR++
        }
        // DNR
        if((tenant.coachingDnrPR) && (parseFloat(scorecard.dnrs) <= 0)){
            messageStringWeeklyPR = messageStringWeeklyPR + `\n- Congratulations on not getting any DNRs! \n`
            numberOfWeeklyPR++
        }
        // SWC-POD
        if((tenant.coachingSwcPodPR) && (parseFloat(scorecard.swcPod) >= parseFloat(tenant.coachingSwcPodThreshold))){
            messageStringWeeklyPR = messageStringWeeklyPR + `\n- You took excellent pictures, and your POD is "Fantastic"!\n`
            numberOfWeeklyPR++
        }
        // SWC-CC
        if((tenant.coachingSwcCcPR) && (parseFloat(scorecard.swcCc) >= tenant.coachingSwcCcThreshold)){
            messageStringWeeklyPR = messageStringWeeklyPR + `\n- Your Contact Compliance score was "Fantastic" last week.\n`
            numberOfWeeklyPR++
        }
        // SWC-SC
        if((tenant.coachingSwcScPR) && (parseFloat(scorecard.swcSc) >= parseFloat(tenant.coachingSwcScThreshold))){
            messageStringWeeklyPR = messageStringWeeklyPR + `\n- Your Scan Compliance was "Fantastic".\n`
            numberOfWeeklyPR++
        }
        // // Seatbelt-Off Rate
        //Orignally compared to parseFloat(tenant.coachingSeatbeltOffThreshold)
        if((tenant.coachingSeatbeltOffPR) && (parseFloat(scorecard.seatbeltOffRate) <= 0)){
            messageStringWeeklyPR = messageStringWeeklyPR + `\n- You earned a "Fantastic" score for wearing your seatbelt last week.\n`
            numberOfWeeklyPR++
        }
        // // Speeding Event Rate
        //Originally compared to parseFloat(tenant.coachingSpeedingEventThreshold)
        if((tenant.coachingSpeedingEventPR) && (parseFloat(scorecard.speedingEventRate) <= 0)){

            //Checks if existing message has a daily coaching, does not add speeding reinforcement
            if(existingMessage == null || (existingMessage.bodyTextDailyCO && !existingMessage.bodyTextDailyCO.includes( `of speeding during your last trip. It is required to follow all posted speed limits.\n`) && !existingMessage.bodyTextDailyCO.includes( `of Scorecard Speeding Event during your last trip. It is required to follow all posted speed limits.\n`))){
                messageStringWeeklyPR = messageStringWeeklyPR + `\n- You practiced safe driving and had a "Fantastic" week by following all posted speed limits.\n`
                numberOfWeeklyPR++
            }
        }
        // // Harsh Braking Rate
        //Originally compared to parseFloat(tenant.coachingHarshBrakingThreshold)
        if((tenant.coachingHarshBrakingRatePR) && (parseFloat(scorecard.harshBrakingRate) <= parseFloat(tenant.coachingHarshBrakingRateThresholdPR))){
            messageStringWeeklyPR = messageStringWeeklyPR + `\n- You successfully avoided harsh breaking and received a Fantastic on the scorcard.\n`
            numberOfWeeklyPR++
        }

        // // Harsh Cornering Rate
        //Originally compared to parseFloat(tenant.coachingHarshCorneringThreshold)
        if((tenant.coachingHarshCorneringRatePR) && (parseFloat(scorecard.harshCorneringRate) <= parseFloat(tenant.coachingHarshCorneringRateThresholdPR))){
            messageStringWeeklyPR = messageStringWeeklyPR + `\n- You successfully avoided harsh cornering and received a Fantastic on the scorcard.\n`
            numberOfWeeklyPR++
        }

        //Distractions Rate
        if((tenant.coachingDistractionsRatePR) && (parseFloat(scorecard.distractionsRate) <= parseFloat(tenant.coachingDistractionsRateThreshold))){
            messageStringWeeklyPR = messageStringWeeklyPR + `\n- You earned a "Fantastic" score for your distraction rate.\n`
            numberOfWeeklyPR++
        }

        //Following Distance Rate
        if((tenant.coachingFollowingDistanceRatePR) && (parseFloat(scorecard.followingDistanceRate) <= parseFloat(tenant.coachingFollowingDistanceRateThreshold))){
            messageStringWeeklyPR = messageStringWeeklyPR + `\n- You earned a "Fantastic" score for your following distance rate.\n`
            numberOfWeeklyPR++
        }

        //Sign Signal Violations Rate
        if((tenant.coachingSignSignalViolationsRatePR) && (parseFloat(scorecard.signSignalViolationsRate) <= parseFloat(tenant.coachingSignSignalViolationsRateThreshold))){
            messageStringWeeklyPR = messageStringWeeklyPR + `\n- You earned a "Fantastic" score for your sign/signal violation rate.\n`
            numberOfWeeklyPR++
        }

        // check for CX stats for same week as scorecard
        // if pending message creation was triggered by CX import, cxFeedback will already be defined
        if(!cxFeedback){
            try{
                let customerFeedbackID = tenant.id + scorecard.transporterId + scorecard.year + scorecard.week
                let fields = ['positiveFeedback','negativeFeedback','mishandledPackage','driverUnprofessional','daTier','cdfScore']
                let getItemParams = buildGetItemParams(getTableName(Tables.STAFF_CX_TABLE), fields, customerFeedbackID)
                let getItemResult = await getDynamoRecord(getItemParams);
                cxFeedback = getItemResult.Item
            }
            catch(e){
                console.log('---catch-10', e)
            }
        }

        // check for CX stats for same week as scorecard
        // if pending message creation was triggered by CX import, cxFeedback will already be defined
        // use first 13 characters of Transporter ID
        if(!cxFeedback){
            let truncatedScoreCard  = scorecard.transporterId.substring(0, scorecard.transporterId.length - 1)
            try{
                let customerFeedbackID = tenant.id + truncatedScoreCard + scorecard.year + scorecard.week
                let fields = ['positiveFeedback','negativeFeedback','mishandledPackage','driverUnprofessional','daTier','cdfScore']
                let getItemParams = buildGetItemParams(getTableName(Tables.STAFF_CX_TABLE), fields, customerFeedbackID)
                let getItemResult = await getDynamoRecord(getItemParams);
                cxFeedback = getItemResult.Item
            }
            catch(e){
                console.log('---catch-10-1', e)
            }
        }

        if(cxFeedback){
            // --- Coaching Opportunities ---

            let coDaTier = levels[tenant.coachingDaTierRatingCO]
            let prDaTier = levels[tenant.coachingDaTierRatingPR]
            let cxDaTier = levels[cxFeedback.daTier]
            
            if((tenant.coachingDaTierCO) && ((cxFeedback.daTier == 'Coming Soon') || cxFeedback.cdfScore == 'Comming Soon')) {
                console.log('---1')
                messageStringWeeklyCO = messageStringWeeklyCO + `\n- Your Customer Feedback score is Coming Soon.\n`
                numberOfWeeklyCO++
            }

            if((tenant.coachingDaTierCO) && (cxDaTier <= coDaTier)) {
                console.log('---2')
                messageStringWeeklyCO = messageStringWeeklyCO + `\n- Your overall Customer Feedback tier was ${cxFeedback.daTier}.\n`
                numberOfWeeklyCO++
            }

            if((tenant.coachingCdfScoreCO) && (parseFloat(cxFeedback.cdfScore) < tenant.coachingCdfScoreThreshold)) {
                console.log('---3')
                messageStringWeeklyCO = messageStringWeeklyCO + `\n- Your Customer Feedback Score was ${cxFeedback.cdfScore}.\n`
                numberOfWeeklyCO++
            }

            if((tenant.coachingDaTierPR) && (cxDaTier >= prDaTier)) {
                console.log('---4')
                messageStringWeeklyPR = messageStringWeeklyPR + `\n- Congratulations, your overall Customer Feedback tier was ${cxFeedback.daTier}.\n`
                numberOfWeeklyPR++
            }

            if((tenant.coachingCdfScorePR) && (parseFloat(cxFeedback.cdfScore) >= tenant.coachingCdfScoreThresholdIssue)) {
                console.log('---5')
                messageStringWeeklyPR = messageStringWeeklyPR + `\n- Great job, your Customer Feedback Score was ${cxFeedback.cdfScore}.\n`
                numberOfWeeklyPR++
            }

            // Positive Feedback
            if(true || (tenant.coachingPositiveFeedbackPR)){
                let counterIncrease = false
                let partialMessage = ''
                if(cxFeedback.deliveryWasGreat > 0) {
                    partialMessage = partialMessage + `${cxFeedback.deliveryWasGreat} Delivery Was Great.\n`
                    counterIncrease = true
                }
                if(cxFeedback.respectfulOfProperty > 0) {
                    partialMessage = partialMessage + `${cxFeedback.respectfulOfProperty} Respectful of Property.\n`
                    counterIncrease = true
                }  
                if(cxFeedback.followedInstructions > 0) {
                    partialMessage = partialMessage + `${cxFeedback.followedInstructions} Followed Instructions.\n`
                    counterIncrease = true 
                }
                if(cxFeedback.friendly > 0) {
                    partialMessage = partialMessage + `${cxFeedback.friendly} Friendly.\n`
                    counterIncrease = true
                }
                if(cxFeedback.aboveAndBeyond > 0) {
                    partialMessage = partialMessage + `${cxFeedback.aboveAndBeyond} Above and Beyond.\n`
                    counterIncrease = true
                }
                if(cxFeedback.deliveredWithCare > 0) {
                    partialMessage = partialMessage + `${cxFeedback.deliveredWithCare} Delivered with Care.\n`
                    counterIncrease = true
                }
                if(cxFeedback.thankMyDriver > 0) {
                    partialMessage = partialMessage + `${cxFeedback.thankMyDriver} Thank My Driver.\n`
                    counterIncrease = true
                }
                if(counterIncrease){
                    messageStringWeeklyPR = messageStringWeeklyPR + `\n- ` + partialMessage
                    numberOfWeeklyPR++
                } 
            }

            // Negative Feedback
            if(true || (tenant.coachingPositiveFeedbackCO)){
                let counterIncrease = false
                let partialMessage = ''
                if(cxFeedback.deliveryWasntGreat > 0) {
                    partialMessage = partialMessage + `${cxFeedback.deliveryWasntGreat} Delivery was not so Great.\n`
                    counterIncrease = true
                }
                if(cxFeedback.mishandledPackage > 0) {
                    partialMessage = partialMessage + `${cxFeedback.mishandledPackage} DA Mishandled Package${cxFeedback.mishandledPackage > 1 ? 's' : ''}.\n`
                    counterIncrease = true
                }  
                if(cxFeedback.driverUnprofessional > 0) {
                    partialMessage = partialMessage + `${cxFeedback.driverUnprofessional} DA was Unprofessional.\n`
                    counterIncrease = true
                }
                if(cxFeedback.notDeliveredToPreferredLocation > 0) {
                    partialMessage = partialMessage + `${cxFeedback.notDeliveredToPreferredLocation} DA did not follow my delivery instructions.\n`
                    counterIncrease = true
                }
                if(cxFeedback.deliveredToWrongAddress > 0) {
                    partialMessage = partialMessage + `${cxFeedback.deliveredToWrongAddress} Delivered to Wrong Address.\n`
                    counterIncrease = true
                }
                if(cxFeedback.neverReceivedDelivery > 0) {
                    partialMessage = partialMessage + `${cxFeedback.neverReceivedDelivery} Never Received Delivery.\n`
                    counterIncrease = true
                }
                if(counterIncrease){
                    messageStringWeeklyCO = messageStringWeeklyCO + `\n- ` + partialMessage
                    numberOfWeeklyCO++ 
                } 
            }
            
            // --- Positive reinforcement ---
            // Positive Feedback
            // if((tenant.coachingPositiveFeedbackPR)){
            //     numberOfWeeklyPR++
            // }
            
        }
    }
    
    // get staff data
    let fields = ['firstName','lastName','email','receiveTextMessages','receiveEmailMessages','phone','latestScorecard']
    let getItemParams = buildGetItemParams(getTableName(Tables.STAFF_TABLE), fields, staffId)
    let getItemResult = await getDynamoRecord(getItemParams);
    var staff = getItemResult.Item

    // determine sending method
    var channelType;
    if( staff.receiveTextMessages && staff.receiveEmailMessages) channelType = 'SMS/EMAIL';
    else if( staff.receiveTextMessages ) channelType = 'SMS';
    else if( staff.receiveEmailMessages ) channelType = 'EMAIL';
    else channelType = null

    let input = {
        updatedAt: new Date().toISOString(),
        pendingMessageTenantId: tenant.id,
        staffId,
        group: tenant.group
    }
    
    // update weekly message fields
    var newMessageCO = ''
    var newMessagePR = ''
    const fileToImport = scorecard || cx;
    let { week, year } = fileToImport;
    week = parseInt( week );
    year = parseInt( year );
    const { startOfWeek, endOfWeek } = getStartAndEndOfWeek( week, year );
    const startOfWeekFormatted = shortDateFormat( startOfWeek, "en-US", { month: "short", day: "numeric" });
    const endOfWeekFormatted = shortDateFormat( endOfWeek, "en-US", { month: "short", day: "numeric" });
    if(scorecard || cx){
        if(messageStringWeeklyCO) newMessageCO = "Here are the areas that need improvement from your latest scorecard for " + startOfWeekFormatted +" through " + endOfWeekFormatted + ":\n" + messageStringWeeklyCO + "\n"
        if(messageStringWeeklyPR) newMessagePR = "Here are the things you excelled at on your latest scorecard for " + startOfWeekFormatted + " through " + endOfWeekFormatted + ":\n" + messageStringWeeklyPR + "\n"
        if(existingMessage != null && existingMessage.bodyTextDailyCO) newMessageCO += "Here are the areas that need improvement from your " + (existingMessage.lastDailyImport || 'last') +" trip:\n" + existingMessage.bodyTextDailyCO
        if(existingMessage != null && existingMessage.bodyTextDailyPR) newMessagePR += "Here are the things you excelled at on your " + (existingMessage.lastDailyImport || 'last') +" trip:\n" + existingMessage.bodyTextDailyPR
    }

    if (messageStringWeeklyCO || messageStringWeeklyPR){
        if(!pendingMessages[pendingMessageId] && !existingMessage){
            input = {
                id: pendingMessageId,
                staffId: staffId,
                pendingMessageTenantId: tenant.id,
                group: tenant.group,
                numberOfWeeklyCO: numberOfWeeklyCO,
                numberOfWeeklyPR: numberOfWeeklyPR,
                numberOfDailyCO: numberOfDailyCO,
                numberOfDailyPR: numberOfDailyPR,
                numberOfCO: numberOfWeeklyCO + numberOfDailyCO,
                numberOfPR: numberOfWeeklyPR + numberOfDailyPR,
                bodyTextWeeklyCO: messageStringWeeklyCO,
                bodyTextWeeklyPR: messageStringWeeklyPR,
                bodyTextDailyCO: messageStringDailyCO,
                bodyTextDailyPR: messageStringDailyPR,
                channelType: channelType,
                startLastWeeklyImport: startOfWeekFormatted,
                endLastWeeklyImport: endOfWeekFormatted
            }
            input.bodyTextCO = ''
            input.bodyTextPR = ''
            if(messageStringWeeklyCO) input.bodyTextCO = "Here are the areas that need improvement from your latest scorecard for " + startOfWeekFormatted +" through " + endOfWeekFormatted + ":\n" + messageStringWeeklyCO + "\n"
            if(messageStringWeeklyPR) input.bodyTextPR = "Here are the things you excelled at on your latest scorecard for " + startOfWeekFormatted + " through " + endOfWeekFormatted + ":\n" + messageStringWeeklyPR + "\n"

            pendingMessages[pendingMessageId] = input
        }else{
            input.bodyTextWeeklyCO = messageStringWeeklyCO,
            input.bodyTextWeeklyPR = messageStringWeeklyPR,
            input.bodyTextCO = newMessageCO
            input.bodyTextPR = newMessagePR
            input.numberOfWeeklyCO = numberOfWeeklyCO,
            input.numberOfWeeklyPR = numberOfWeeklyPR,
            input.numberOfCO = parseInt(existingMessage ? existingMessage.numberOfDailyCO : '0') + numberOfWeeklyCO
            input.numberOfPR = parseInt(existingMessage ? existingMessage.numberOfDailyPR : '0') + numberOfWeeklyPR
            input.channelType = channelType
            input.startLastWeeklyImport = startOfWeekFormatted
            input.endLastWeeklyImport = endOfWeekFormatted

            pendingMessages[pendingMessageId] = {...existingMessage, ...input}
        }
    }
}
module.exports = { createPendingMessageRecord }