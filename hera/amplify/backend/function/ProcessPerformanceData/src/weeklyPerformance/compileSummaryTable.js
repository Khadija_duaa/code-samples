function findCategory(tableData=[], categoryName){
  const data = tableData.find(row => row.category.includes(categoryName))
  return data ? data.count : ''
}

function convertToObject(tableData=[]){
  const data = {}
  tableData.forEach(item => {
    data[item.category] = item.count
  })
  return data
}

function compileSummaryTablePod(table=[]){
  let data = {}
  if(!table.length || !table[0].table_data?.length){
    return data
  }
  const tableData = table[0].table_data
  const dataFound = convertToObject(tableData)
  const empty = ''
  data = {
    success: dataFound['Success'] || empty,
    bypass: dataFound['Bypass'] || empty,
    rejects: dataFound['Rejects'] || empty,
    opportunities: dataFound['Opportunities'] || empty,
    noPackageDetected: dataFound['No Package Detected']|| empty, 
    packageNotClearlyVisible: dataFound['Package Not Clearly Visible'] || empty,
    blurryPhoto: dataFound['Blurry Photo'] || empty,
    packageTooClose: dataFound['Package Too Close'] || empty,
    humanInThePicture: dataFound['Human In The Picture'] || empty,
    packageInHand: dataFound['Package In Hand'] || empty,
    photoTooDark: dataFound['Photo Too Dark'] || empty,
    packageInCar: dataFound['Package In Car'] || empty,
    other: dataFound['Other'] || empty,
    grandTotal: dataFound['Grand Total'] || empty
  }
  return data
    
}

function compileSummaryTableCxFeedback(table=[]){
  let data = {}
  if(!table.length || !table[0].table_data?.length){
    return data
  }
  const tableData = table[0].table_data
  data = {
    dcfTier: findCategory(tableData,'DSP Customer Feedback Tier'),
    dcfScore: findCategory(tableData,'DSP Customer Feedback Score'),
    positiveFeedback: findCategory(tableData,'Total Positive Feedback'),
    negativeFeedback: findCategory(tableData,'Total Negative Feedback'),
    deliveriesWithoutCF: findCategory(tableData,'Deliveries without Customer Feedback'),
    deliveryWasGreat: findCategory(tableData,'Delivery was Great'),
    respectfulOfProperty: findCategory(tableData,'Respectful of Property'),
    followedInstructions: findCategory(tableData,'Followed Instructions'),
    friendly: findCategory(tableData,'Friendly'),
    aboveAndBeyond: findCategory(tableData,'Above and Beyond'),
    deliveredWithCare: findCategory(tableData,'Delivered with Care'),
    thankMyDriver: findCategory(tableData,'Thank my Driver'),
    deliveryWasntGreat: findCategory(tableData,'Delivery was not so Great'),
    mishandledPackage: findCategory(tableData,'Driver Mishandled Package'),
    driverUnprofessional: findCategory(tableData,'Driver was Unprofessional'),
    driverDidNotFollowDeliveryInstructions: findCategory(tableData,'Driver did not follow my Delivery instructions'),
    onTime: findCategory(tableData,'On Time'),
    lateDelivery: findCategory(tableData,'Late Delivery'),
    itemDamaged: findCategory(tableData,'Item Damaged'),
    deliveredToWrongAddress: findCategory(tableData,'Delivered to Wrong Address'),
    neverReceivedDelivery: findCategory(tableData,'Never Received Delivery'),
  }
  return data
    
}
module.exports = {compileSummaryTablePod, compileSummaryTableCxFeedback }
