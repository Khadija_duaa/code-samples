// parameterWeek: type of coaching (Scorecad, Customer Feedback, etc)
// handleImportsLastSaturday: Treat import as last Saturday past, if so allow files from past import week
function validateWeekForPendingMessages( currentWeek, parameterWeek, handleImportsLastSaturday ) {

  return currentWeek == parameterWeek || (handleImportsLastSaturday && parameterWeek == currentWeek - 1);
}

module.exports = { validateWeekForPendingMessages };