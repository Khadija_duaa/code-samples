const AWS = require('aws-sdk');
const cloudWatch = new AWS.CloudWatch({apiVersion: '2010-08-01'})

const dimensionGroups = {
    deleteIssue: {
        metricGroup1: [
            ["byError"]
        ],
        metricGroup2: [
            ["bySeverity"]
        ]
    },

}

class CWMetrics {
    constructor(Namespace){
        if(!Namespace) throw {message: "Namespace is required", from:"cloudWatchHelper"}
        const _this = this
        this.clear = () => {
            _this.Namespace = Namespace
            _this.MetricData = []
        }
        this.clear()
        this.sendToCloudWatch = async () => {
            if(_this.Namespace && _this.MetricData && _this.MetricData.length){
                const metricData = this.getMetrics()
                await cloudWatch.putMetricData(metricData).promise()
                _this.clear()
            }
        }
        this.getMetrics = () => ({
            Namespace: _this.Namespace,
            MetricData: _this.MetricData
        })
        const dimensions = dimensionGroups
        const keys = Object.keys(dimensions)
        keys.forEach(key=>{
            const innerKeys = Object.keys(dimensions[key])
            _this[key] = {}
            innerKeys.forEach(iKey=>{
                _this[key][iKey] = function(dimension){
                    return {
                        pushMetricData: function(metric){
                            const dimensionContainer = dimensions[key][iKey].map(group => {
                                return group.map(name=>{
                                    return {
                                        Name: name,
                                        Value: dimension[name] || '-'
                                    }
                                })
                            })
                            dimensionContainer.forEach(Dimensions => {
                                const existingMetric = _this.MetricData.find(md=>md.MetricName === metric.MetricName && JSON.stringify(md.Dimensions) === JSON.stringify(Dimensions) )
                                if(existingMetric){
                                    existingMetric.Value += metric.Value || 0
                                }else{
                                    _this.MetricData.push({...metric, Dimensions})
                                }
                            })
                        }
                    }
                }
            })
        })
    }
}

module.exports = { CWMetrics }