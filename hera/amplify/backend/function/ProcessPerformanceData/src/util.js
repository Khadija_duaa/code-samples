

const moment = require('moment-timezone')

const keyOf = (value) => value.replace(/\s+/g, '').toLowerCase()

const cleanTransporterId = function(transporterId){
        return transporterId.replace(/\s+/g, '')
}

const cleanInput = function(str){
        if(!str){
                return null
        }
        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();
        
        // remove accents, swap ñ for n, etc
        const from = 'àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõőṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;'
        const to = 'aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnoooooooooprrsssssttuuuuuuuuuwxyyzzz------'
        for (var i=0, l=from.length ; i<l ; i++) {
                str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }
        
        str = str.replace(/[^a-z0-9 - #]/g, '') // remove invalid chars
        .replace(/\s+/g, '_') // collapse whitespace and replace by _
        .replace(/-+/g, '_'); // collapse dashes
        
        return str;
}

function getTableName(tableName){
  return tableName + "-" + process.env.API_HERA_GRAPHQLAPIIDOUTPUT + "-" + process.env.ENV;
}

/**
 * Parses a date string into a timestamp for a given timezone
 * @param {String} dateStr - Date in format YYYY-MM-DD
 * @param {String} timezone - Tenant timezone
 * @returns {String} timestamp in Tenant's timezone
 */
function parseDateToTimezoneTimestamp(dateStr, timezone) {
  const isValidTimezone = !!moment.tz.zone(timezone);
  const parsedDate = (isValidTimezone)? moment.tz(dateStr, timezone): moment(dateStr);
  return parsedDate.toDate().toISOString();
}

/**
 * Tranforms a date into a string in format YYYY-MM-DD
 * @param {Date} date - Date to be transformed to YYYY-MM-DD format
 * @returns {String} date in format YYYY-MM-DD
 */
function dateToYYYYMMDD(date) {
  return [
    `${date.getFullYear()}`.padStart(4, '0'),
    `${date.getMonth() + 1}`.padStart(2, '0'),
    `${date.getDate()}`.padStart(2, '0')
  ].join('-');
}

const AWSDateToDate = function(date){
        if(!date) return undefined
        if(date instanceof Date) return date
        const [year, month, day] = date.replace(/[a-z]*/gi,'').split('-')
        return new Date(year, month*1-1, day)
}

const beforeToday = function(clientTimeZone, days = 1){
        let result

        if(clientTimeZone){
                let localeStringDate = new Date().toLocaleString('en-US', {timeZone: clientTimeZone})
                result = new Date(localeStringDate)
        }else{
                result = new Date()
        }

        result.setHours(0,0,0,0)
        result.setDate(result.getDate() - days)
        return result
}

const toTitleCase = function(value){
        if(value === null || value === undefined) return value
        let formattedValue = typeof value === 'string' ? value : String(value)
        return formattedValue.replace('-', ' ').split(' ').map(word => word[0].toUpperCase() + word.substr(1).toLowerCase()).join(' ')
}

const generateUUID = function() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                const r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
        });
}

const weeksToMilliseconds = function(weeks) {
        return 1000 * 60 * 60 * 24 * 7 * (weeks - 1);
}

/**
 * Sets the given date as the first day of week of the first week of year.
 */
const firstWeekday = function(firstOfJanuaryDate) {
        // 0 correspond au dimanche et 6 correspond au samedi.
        var FIRST_DAY_OF_WEEK = 1; // Monday, according to iso8601
        var WEEK_LENGTH = 7; // 7 days per week
        var day = firstOfJanuaryDate.getDay();
        day = (day === 0) ? 7 : day; // make the days monday-sunday equals to 1-7 instead of 0-6
        var dayOffset=-day+FIRST_DAY_OF_WEEK; // dayOffset will correct the date in order to get a Monday
        if (WEEK_LENGTH-day+1<4) {
                // the current week has not the minimum 4 days required by iso 8601 => add one week
                dayOffset += WEEK_LENGTH;
        }
        return new Date(firstOfJanuaryDate.getTime()+dayOffset*24*60*60*1000);
}

const firstDayOfYear = function(date, year) {
        date.setYear(year);
        date.setDate(1);
        date.setMonth(0);
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        date.setMilliseconds(0);
        return date;
}

const firstWeekOfYear = function(year) {
        var date = new Date();
        date = firstDayOfYear(date,year);
        date = firstWeekday(date);
        return date;
}

const getYearWeekDate = function(yearIn, weekIn){
        let date = firstWeekOfYear(yearIn)
        let weekTime = weeksToMilliseconds(weekIn)
        let targetTime = date.getTime() + weekTime
        return new Date( date.setTime(targetTime) + ( 1000 * 60 * 60 * 24 * 9)); 
}

const parseTimeValue = function(value){
        if(value == null) return
        if(/\b((0)?[0-9]|(1[0-2])):[0-5][0-9][ ]?((a|p)[\.]?[ ]?m[\.]?)\b/gi.test(value)){
                const time = moment(value, ["hh:mm a"])
                if(time.isValid()){
                        return time.format("HH:mm")
                }
        }
        else if(/\b((0)?[0-9]|([0-2][0-4])|1[0-9]):[0-5][0-9]\b/gi.test(value)){
                return value
        }else if(/[0-1]{1}(\,|\.){1}[0-9]*/gi.test(value)){
                let delimiter
                if(String('0,1' * 1.0 ) !== 'NaN'){
                        delimiter = ['.',',']
                }else if(String('0.1' * 1.0 ) !== 'NaN'){
                        delimiter = [',','.']
                }else{
                        return
                }
                let time =  String(value).replace(...delimiter)
                let timeArr = String(24 * time).split(delimiter[1])
                let hrs = timeArr[0]
                let min = Math.round(60 * (0 + delimiter[1] + timeArr[1]))
                return `${hrs}:${min}`
        }
}

const getStartAndEndOfWeek = function (weekNumber, year) {
        // Create a date object for January 1st of the given year
        const januaryFirst = new Date(year, 0, 1);

        // Calculate the number of days between January 1st and the start of the given week
        const daysUntilFirstWeek = (7 - januaryFirst.getDay()) % 7;
        const startOfWeek = new Date(year, 0, 1 + (7 * (weekNumber - 1)) - daysUntilFirstWeek);

        // Calculate the end of the week by adding 6 days to the start of the week
        const endOfWeek = new Date(startOfWeek.getFullYear(), startOfWeek.getMonth(), startOfWeek.getDate() + 6);

        return { startOfWeek, endOfWeek };
}

const shortDateFormat = function ( date=new Date(), locale="en-US", options={ month: "short", day: "numeric" } ) {
        return new Date( date ).toLocaleString( locale, options )
}

module.exports = {
        keyOf,
        cleanTransporterId,
        cleanInput,
        getTableName,
        parseDateToTimezoneTimestamp,
        dateToYYYYMMDD,
        AWSDateToDate,
        toTitleCase,
        generateUUID,
        weeksToMilliseconds,
        firstWeekday,
        firstDayOfYear,
        firstWeekOfYear,
        getYearWeekDate,
        beforeToday,
        parseTimeValue,
        getStartAndEndOfWeek,
        shortDateFormat 
}