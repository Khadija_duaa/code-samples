const {
    queryDynamoRecords
} = require('./dynamodbHelper.js')

/*******************************************************************************
 *  get Kudo or Issues
 ******************************************************************************/

async function getKudosOrIssueRecords(staffId, date, tableName){
    let data = []
    try {
        let lastEvaluatedKey = null
       
       
        do {
            let params = {
                TableName: tableName,
                IndexName: 'byStaff',
                ExpressionAttributeValues: {
                    ':staffId': staffId,
                    ':date': date
                },
                ExpressionAttributeNames: {
                    '#staffId': 'staffId',
                    '#date': 'date',
                    '#g': 'group'
                },
                KeyConditionExpression: '#staffId = :staffId AND #date = :date',
                ProjectionExpression: "id, #g, #date, #staffId",
                ExclusiveStartKey: lastEvaluatedKey
            }
            const result = await queryDynamoRecords(params)
            lastEvaluatedKey = result.LastEvaluatedKey
            data = data.concat(result.Items)
        } while (lastEvaluatedKey)
    } catch (e) {
        console.log('Error in function getKudosOrIssueRecords', e)
    }finally{
        return data
    }
}



module.exports = { getKudosOrIssueRecords }