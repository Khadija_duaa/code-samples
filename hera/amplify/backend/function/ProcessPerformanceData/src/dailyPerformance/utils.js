const { AWSDateToDate }  = require('../util.js')
function getLastDayOfWeekFromColumnNames(restOfTheObject){
    let theLastDate, lastDayOfWeek
    const objKeys = Object.keys(restOfTheObject)
    objKeys.forEach( key => {
        if(key && !(key == false)){
            const currDate = AWSDateToDate(key)
            if(currDate && currDate.getTime()){
                if(!theLastDate || currDate > theLastDate){
                    lastDayOfWeek = key
                    theLastDate = currDate
                }
            }
        }
    })
    return {lastDayOfWeek, theLastDate}
}

function dateIsValid(date) {
    return date instanceof Date && !isNaN(date);
}

function getOnlyDate(date) {
    const dateObject = new Date(date); 
    const year = dateObject.getFullYear();
    const month = String(dateObject.getMonth() + 1).padStart(2, '0');
    const day = String(dateObject.getDate()).padStart(2, '0');
    return `${year}-${month}-${day}`;
}

function getValidDates(arrayDates){
    const datesList = [ ...arrayDates ]
    const datesValid =  datesList.filter( key => key.includes("-") && dateIsValid( new Date( key) ) );
    return datesValid
}

function getDaysWeekFromColumnName( restOfTheObject ) {
    const objKeys = Object.keys( restOfTheObject )
    const datesImport = getValidDates( objKeys );
    return datesImport
}

module.exports = {
    getDaysWeekFromColumnName,
    getLastDayOfWeekFromColumnNames,
    getOnlyDate
}


