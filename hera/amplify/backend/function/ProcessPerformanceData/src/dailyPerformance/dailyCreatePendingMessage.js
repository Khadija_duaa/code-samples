const { getPendingMessageById } = require('../helper.js')
const { shortDateFormat } = require('../util.js')

// removes hyphens and spaces and camel cases value
function formatNetradyneType(value){
    return value.replace('-', ' ').replace('-',' ').split(' ').map(word => word[0].toUpperCase() + word.substr(1).toLowerCase()).join(' ').replace(/\s+/g, '')
}

function joinMessages(...messages){
    if(!messages.length) return ''
    let str = ''
    messages.forEach(text => (str += (text||'')))
    return trimSpaces(str)
}

function trimSpaces(text){
    if(!text) return text
    return text.replace(/(\s){3,}/g,(a)=>{
        let count = 0
        let str = ''
        for (let c of Array.from(a)) {
            if(count > 1) break
            if(c.charCodeAt(0) == 10) count++
            str += c
        }
        return str
    })
}

function validValues(...values){
    if(!values.length) return false
    return values.some(value => (!value && ['boolean','number'].includes(typeof value)) || String(value||'').trim())
}

const createPendingMessagesMentor = async function(importId, mentor, staff, proxy){
    if(!mentor){
        return
    }
    const tenant = proxy.tenant
    const pendingMessages = proxy.tmpTable.pendingMessages
    const staffId = staff.id
    var numberOfWeeklyCO = ''
    var numberOfWeeklyPR = ''

    var messageStringDailyCO = '';
    var messageStringMentorCO = '';
    var messageStringNetradyneCO = '';
    var messageStringEocCO = '';
    var messageStringDailyPR = '';
    var messageStringMentorPR = '';
    var messageStringNetradynePR = '';
    var messageStringEocPR = '';
    var numberOfDailyCO = 0
    var numberOfMentorCO = 0
    var numberOfNetradyneCO = 0
    var numberOfEocCO = 0
    var numberOfDailyPR = 0
    var numberOfMentorPR = 0
    var numberOfNetradynePR = 0
    var numberOfEocPR = 0
    
    //FIND EXISTING MESSAGE
    var pendingMessageId = tenant.id + staffId
    let existingMessage = pendingMessages[pendingMessageId]
    if(!existingMessage){
        try{
            existingMessage = await getPendingMessageById(pendingMessageId)
        }
        catch(e){
            console.log(e);
            return
        }
    }

    
    // ------------ MENTOR ------------
    if(mentor){
        // exit if all mentor messages disabled
        if( !tenant.coachingDailyFicoCO && !tenant.coachingSeatbeltCO && !tenant.coachingSseCO &&
            !tenant.coachingDailyFicoPR && !tenant.coachingSeatbeltPR && !tenant.coachingSsePR &&
            !tenant.coachingTraningRemainingCO ){
            return
        }

        // --- Coaching Opportunities ---
        // FICO
        if((tenant.coachingDailyFicoCO) && (parseFloat(mentor.fico) < parseFloat(tenant.coachingDailyFicoThreshold ))){
            let accel = parseFloat(mentor.accel)
            let braking = parseFloat(mentor.braking)
            let cornering = parseFloat(mentor.cornering)
            let speeding = parseFloat(mentor.speeding) > parseFloat(mentor.sse) ? parseFloat(mentor.speeding) : parseFloat(mentor.sse)
            let distraction = parseFloat(mentor.distraction)
            let seatbelt = parseFloat(mentor.seatbelt)
            let backUp = parseFloat(mentor.backUp)
            let breakdown = false
            messageStringMentorCO = messageStringMentorCO + `\n- Your Amazon FICO score was ${mentor.fico} for your last trip.`
            if(accel && mentor.accel != "N/A" || 
                braking && mentor.braking != "N/A" || 
                cornering && mentor.cornering != "N/A" || 
                speeding && mentor.speeding != "N/A" || 
                speeding && mentor.sse != "N/A" ||
                distraction && mentor.distraction != "N/A" || 
                seatbelt && mentor.seatbelt != "N/A" || 
                backUp && mentor.backUp != "N/A"){
                    messageStringMentorCO = messageStringMentorCO + ` You had: `
                    breakdown = true
            }
            if(breakdown) {
                if(accel && mentor.accel != "N/A") messageStringMentorCO = messageStringMentorCO + ` ${accel} instance${accel > 1 ? 's' : ''} of accelerating too quickly,` 
                if(braking && mentor.braking != "N/A") messageStringMentorCO = messageStringMentorCO + ` ${braking} instance${braking > 1 ? 's' : ''} of braking too harshly,`
                if(cornering && mentor.cornering != "N/A") messageStringMentorCO = messageStringMentorCO + ` ${cornering} instance${cornering > 1 ? 's' : ''} of taking a turn too quickly,`
                if(speeding && (mentor.speeding != "N/A" ||mentor.sse != "N/A")) messageStringMentorCO = messageStringMentorCO + ` ${speeding} instance${speeding > 1 ? 's' : ''} of speeding,` 
                if(distraction && mentor.distraction != "N/A") messageStringMentorCO = messageStringMentorCO + ` ${distraction} instance${distraction > 1 ? 's' : ''} of using your phone while driving,`
                if(seatbelt && mentor.seatbelt != "N/A") messageStringMentorCO = messageStringMentorCO + ` ${seatbelt} instance${seatbelt > 1 ? 's' : ''} of not wearing your seatbelt while driving,` 
                if(backUp && mentor.backUp != "N/A") messageStringMentorCO = messageStringMentorCO + ` ${backUp} instance${backUp > 1 ? 's' : ''} of backing up. If you have no choice but to back up, do not exceed 5 mph in reverse.`
                messageStringMentorCO = messageStringMentorCO.substring(0, messageStringMentorCO.length - 1) + '. To improve your FICO score, reduce the number of instances listed above.\n'
            }
            numberOfMentorCO++
        }
        // Seatbelt
        if((tenant.coachingSeatbeltCO) && (parseFloat(mentor.seatbelt) > parseFloat(tenant.coachingSeatbeltThreshold))){
            messageStringMentorCO = messageStringMentorCO + `\n- WARNING: You had ${parseFloat(mentor.seatbelt)}${parseFloat(mentor.seatbelt) > 1 ? ' instances': ' instance'} of not wearing your seatbelt while traveling during your last trip. Please remember to buckle your seatbelt before putting the vehicle in drive.\n`
            numberOfMentorCO++
        }
        // Sse
        if((tenant.coachingSseCO) && (parseFloat(mentor.sse) > parseFloat(tenant.coachingSseThreshold) || parseFloat(mentor.speeding) > parseFloat(tenant.coachingSseThreshold))){

            //Checks if existing message had a weekly positive reinforcement for speeding, if so remove it.
            if(existingMessage && existingMessage.bodyTextWeeklyPR && existingMessage.bodyTextWeeklyPR.includes(`\n- You practiced safe driving and had a "Fantastic" week by following all posted speed limits.\n`)){
                const regex = /\n- You practiced safe driving and had a "Fantastic" week by following all posted speed limits.\n/i;
                existingMessage.bodyTextWeeklyPR = existingMessage.bodyTextWeeklyPR.replace(regex, '') 
            }

            //Set up number of instances for message, added in if-else statment to prevent message being on one really long line.
            var instanceMessage = null
            if( parseFloat(mentor.sse) > parseFloat(tenant.coachingSseThreshold) &&
                parseFloat(mentor.speeding) > parseFloat(tenant.coachingSseThreshold)
            ){
                instanceMessage = `${parseFloat(mentor.speeding)} ${parseFloat(mentor.speeding) > 1 ? 'instances': 'instance'} of speeding and ${parseFloat(mentor.sse)} ${parseFloat(mentor.sse) > 1 ? 'instances' : 'instance'} of Scorecard Speeding Event`
            }
            else if(
                !(parseFloat(mentor.sse) > parseFloat(tenant.coachingSseThreshold)) &&
                parseFloat(mentor.speeding) > parseFloat(tenant.coachingSseThreshold)
            ){
                instanceMessage = `${parseFloat(mentor.speeding)} ${parseFloat(mentor.speeding) > 1 ? 'instances': 'instance'} of speeding`
            }
            else if(
                parseFloat(mentor.sse) > parseFloat(tenant.coachingSseThreshold) &&
                !(parseFloat(mentor.speeding) > parseFloat(tenant.coachingSseThreshold))
            ){
                instanceMessage = `${parseFloat(mentor.sse)} ${parseFloat(mentor.sse) > 0 ? 'instances' : 'instance'} of Scorecard Speeding Event`
            }
            messageStringMentorCO = messageStringMentorCO + `\n- WARNING: You had ${instanceMessage} during your last trip. It is required to follow all posted speed limits.\n`
            numberOfMentorCO++
        }
        // DVCRs
        // if((tenant.coachingDvcrsCO) && (parseFloat(mentor.preDvcr) < 1 || parseFloat(mentor.postDvcr) < 1)){
        //     messageStringMentorCO = messageStringMentorCO + `\n- WARNING: You missed one or more of your vehicle inspections. Two are required every day.\n`
        //     numberOfMentorCO++
        // }
        // Trainings
        var trainingsRemaining = parseFloat(mentor.trainingAssigned) - parseFloat(mentor.trainingCompleted)
        if((tenant.coachingTraningRemainingCO) && (trainingsRemaining > 0)){
            messageStringMentorCO = messageStringMentorCO + `\n- You have ${trainingsRemaining} Mentor training video${trainingsRemaining > 1 ? 's' : ''} left to watch.\n`
            numberOfMentorCO++
        }
        
        
        // --- Positive reinforcement ---
        // FICO
        if((tenant.coachingDailyFicoPR) && (parseFloat(mentor.fico) >= parseFloat(tenant.coachingDailyFicoThreshold))){
            messageStringMentorPR = messageStringMentorPR + `\n- Your FICO was ${mentor.fico} for your last trip, which is over the threshold for "Fantastic"!`
            if(parseFloat(mentor.fico) != 850){
                messageStringMentorPR += ' Keep striving for 850!\n'
            }
            else messageStringMentorPR += '\n'
            numberOfMentorPR++
        }
        // Seatbelt
        if((tenant.coachingSeatbeltCO) && (parseFloat(mentor.seatbelt) <= parseFloat(tenant.coachingSeatbeltThreshold))){
            messageStringMentorPR = messageStringMentorPR + `\n- You kept yourself safe and wore your seatbelt during travel.\n`
            numberOfMentorPR++
        }
        // Sse
        if(
            (tenant.coachingSseCO) &&
            (parseFloat(mentor.sse) <= parseFloat(tenant.coachingSseThreshold)) && (parseFloat(mentor.speeding) <= parseFloat(tenant.coachingSseThreshold))
        ){
            //Checks if existing message has a weekly coaching for speed, doesn't add postive daily for speed.
            if(!existingMessage || !(existingMessage.bodyTextWeeklyCO||'').includes( `instances of speeding per route last week. It is required to follow all posted speed limits.\n`)){
                messageStringMentorPR = messageStringMentorPR + `\n- You practiced safe driving and followed all posted speed limits.\n`
                numberOfMentorPR++
            }
        }
    }

    // determine sending method
    var channelType;
    if( staff.receiveTextMessages && staff.receiveEmailMessages) channelType = 'SMS/EMAIL';
    else if( staff.receiveTextMessages ) channelType = 'SMS';
    else if( staff.receiveEmailMessages ) channelType = 'EMAIL';
    else channelType = null

    let input = {
        id: pendingMessageId,
        staffId: staffId,
        pendingMessageTenantId: tenant.id,
        group: tenant.group,
        updatedAt: new Date().toISOString()
    }

    // update weekly message fields
    var newMessageCO = ''
    var newMessagePR = ''

    // update daily message fields
    const { date: mentorDate } = mentor;
    const formatMentorDate = shortDateFormat(mentorDate, "en-US", { month: "short", day: "numeric", timeZone: 'UTC' });
    // copy weekly message parts from existing message
    if(existingMessage && existingMessage.bodyTextWeeklyCO){
        newMessageCO = "Here are the areas that need improvement from your latest scorecard for " + existingMessage.startLastWeeklyImport +" through " + existingMessage.endLastWeeklyImport + ":\n" + existingMessage.bodyTextWeeklyCO + "\n"
        numberOfWeeklyCO = parseInt(existingMessage.numberOfWeeklyCO)
    }
    if(existingMessage && existingMessage.bodyTextWeeklyPR){
        newMessagePR = "Here are the things you excelled at on your latest scorecard for " + existingMessage.startLastWeeklyImport +" through " + existingMessage.endLastWeeklyImport + ":\n" + existingMessage.bodyTextWeeklyPR + "\n"
        numberOfWeeklyPR = parseInt(existingMessage.numberOfWeeklyPR)
    }
    
    // copy or overwrite daily message parts from existing message
    if(existingMessage){
        
        // use past netradyne coaching message if none generated by this import
        if(existingMessage.bodyTextNetradyneCO && !messageStringNetradyneCO){
            messageStringNetradyneCO += existingMessage.bodyTextNetradyneCO
            numberOfNetradyneCO = existingMessage.numberOfNetradyneCO
        }   
        // use past netradyne reinforcement message if none generated by this import
        if(existingMessage.bodyTextNetradynePR && !messageStringNetradynePR){
            messageStringNetradynePR += existingMessage.bodyTextNetradynePR
            numberOfNetradynePR = existingMessage.numberOfNetradynePR
        }
        
        // use past eoc coaching message if none generated by this import
        if(existingMessage.bodyTextEocCO && !messageStringEocCO){
            messageStringEocCO += existingMessage.bodyTextEocCO
            numberOfEocCO = existingMessage.numberOfEocCO
        } 
        // use past eoc reinforcement message if none generated by this import
        if(existingMessage.bodyTextEocPR && !messageStringEocPR){
            messageStringEocPR += existingMessage.bodyTextEocPR
            numberOfEocPR = existingMessage.numberOfEocPR
        }

    }
    
    // add message parts together to form daily messages
    messageStringDailyCO = joinMessages(messageStringDailyCO, messageStringMentorCO, messageStringNetradyneCO, messageStringEocCO)
    messageStringDailyPR = joinMessages(messageStringDailyPR, messageStringMentorPR, messageStringNetradynePR, messageStringEocPR)
    if(validValues(messageStringDailyCO)) newMessageCO += "Here are the areas that need improvement from your " + formatMentorDate +" trip:\n" + messageStringDailyCO
    if(validValues(messageStringDailyPR)) newMessagePR += "Here are the things you excelled at on your " + formatMentorDate +" trip:\n" + messageStringDailyPR

    numberOfDailyCO = parseInt(numberOfMentorCO||0) + parseInt(numberOfNetradyneCO||0) + parseInt(numberOfEocCO||0)
    numberOfDailyPR = parseInt(numberOfMentorPR||0) + parseInt(numberOfNetradynePR||0) + parseInt(numberOfEocPR||0)

    if(!existingMessage){
        input = {
            id: pendingMessageId,
            staffId: staffId,
            pendingMessageTenantId: tenant.id,
            group: tenant.group,
            numberOfDailyCO: numberOfDailyCO,
            numberOfMentorCO: numberOfMentorCO,
            numberOfDailyPR: numberOfDailyPR,
            numberOfMentorPR: numberOfMentorPR,
            numberOfCO: numberOfWeeklyCO + numberOfDailyCO,
            numberOfPR: numberOfWeeklyPR + numberOfDailyPR,
            bodyTextDailyCO: messageStringDailyCO,
            bodyTextMentorCO: messageStringMentorCO,
            bodyTextDailyPR: messageStringDailyPR,
            bodyTextMentorPR: messageStringMentorPR,
            channelType: channelType,
            createdAt: input.updatedAt,
            updatedAt: input.updatedAt,
            lastDailyImport: formatMentorDate
        }
        input.bodyTextCO = ''
        input.bodyTextPR = ''
        if(importId){
            input.importId = importId
        }
        if(!validValues(messageStringDailyCO, messageStringDailyPR)){
            return
        }
        else{
            if(validValues(messageStringDailyCO)) input.bodyTextCO += "Here are the areas that need improvement from your " + formatMentorDate +" trip:\n" + messageStringDailyCO
            if(validValues(messageStringDailyPR)) input.bodyTextPR += "Here are the things you excelled at on your " + formatMentorDate +" trip:\n" + messageStringDailyPR
        }
        proxy.createOrUpdateItem(input, pendingMessageId, 'pendingMessages', proxy.tmpTable)
    }else{
        
        input.bodyTextDailyCO = messageStringDailyCO
        input.bodyTextMentorCO = messageStringMentorCO
        input.bodyTextCO = trimSpaces(newMessageCO)

        input.bodyTextDailyPR = messageStringDailyPR
        input.bodyTextMentorPR = messageStringMentorPR
        input.bodyTextPR = trimSpaces(newMessagePR)
        
        input.numberOfDailyCO = numberOfDailyCO
        input.numberOfMentorCO = numberOfMentorCO
        input.numberOfDailyPR = numberOfDailyPR
        input.numberOfMentorPR = numberOfMentorPR

        input.numberOfCO = numberOfDailyCO + numberOfWeeklyCO
        input.numberOfPR = numberOfDailyPR + numberOfWeeklyPR

        input.channelType = channelType
        input.importId = importId
        input.lastDailyImport = formatMentorDate

        proxy.createOrUpdateItem(input, pendingMessageId, 'pendingMessages', proxy.tmpTable)
    }

}

const createPendingMessagesNetradyne = async function (importId, netradyne, staff, proxy){
    if(!netradyne){
        return
    }
    const tenant = proxy.tenant
    const pendingMessages = proxy.tmpTable.pendingMessages
    const staffId = staff.id
    var numberOfWeeklyCO = ''
    var numberOfWeeklyPR = ''

    var messageStringDailyCO = '';
    var messageStringMentorCO = '';
    var messageStringNetradyneCO = '';
    var messageStringEocCO = '';
    var messageStringDailyPR = '';
    var messageStringMentorPR = '';
    var messageStringNetradynePR = '';
    var messageStringEocPR = '';
    var numberOfDailyCO = 0
    var numberOfMentorCO = 0
    var numberOfNetradyneCO = 0
    var numberOfEocCO = 0
    var numberOfDailyPR = 0
    var numberOfMentorPR = 0
    var numberOfNetradynePR = 0
    var numberOfEocPR = 0

    //FIND EXISTING MESSAGE
    var pendingMessageId = tenant.id + staffId
    let existingMessage = pendingMessages[pendingMessageId]
    if(!existingMessage){
        try{
            existingMessage = await getPendingMessageById(pendingMessageId)
        }
        catch(e){
            console.log(e);
            return
        }
    }

    // ------------ NETRADYNE ------------
    if(netradyne){
        // exit if all Netradyne messages disabled
        if( !tenant.coachingCameraObstructionCO && !tenant.coachingDriverDistractionCO && !tenant.coachingDriverDrowsinessCO &&
            !tenant.coachingDriverStarPR && !tenant.coachingFollowingDistanceCO &&
            !tenant.coachingHardAccelerationCO && !tenant.coachingHardBrakingCO && !tenant.coachingHardTurnCO &&
            !tenant.coachingHighGCO && !tenant.coachingLowImpactCO && !tenant.coachingSeatbeltComplianceCO &&
            !tenant.coachingSignViolationsCO && !tenant.coachingSpeedingViolationsCO && !tenant.coachingTrafficLightViolationCO && 
            !tenant.coachingUTurnCO && !tenant.coachingWeavingCO ){
            return
        }
        const netradyneCoachingMessages = {
            coachingCameraObstruction: ` %value% instance(s) of %severity% camera obstructions,`,
            coachingDriverDistraction: ` %value% instance(s) of %severity% driver distractions,`,
            coachingDriverDrowsiness: ` %value% instance(s) of %severity% driver drowsiness,`,
            coachingDriverStar: ` %value% instance(s) of %severity% driver stars,`,
            coachingFollowingDistance: ` %value% instance(s) of %severity% following distance,`,
            coachingHardAcceleration: ` %value% instance(s) of %severity% hard acceleration,`,
            coachingHardBraking: ` %value% instance(s) of %severity% hard braking,`,
            coachingHardTurn: ` %value% instance(s) of %severity% hard turning,`,
            coachingHighG: ` %value% instance(s) of %severity% high G,`,
            coachingLowImpact: ` %value% instance(s) of %severity% low impact,`,
            coachingSeatbeltCompliance: ` %value% instance(s) of %severity% seatbelt compliance,`,
            coachingSignViolations: ` %value% instance(s) of %severity% sign violations,`,
            coachingSpeedingViolations: ` %value% instance(s) of %severity% speeding violations,`,
            coachingTrafficLightViolation: ` %value% instance(s) of %severity% traffic light violations,`,
            coachingUTurn: ` %value% instance(s) of %severity% u-turns,`,
            coachingWeaving: ` %value% instance(s) of %severity% weaving`
        }
        
        // loop through alerts and compare to thresholds 
        var alertCounts = {}
        let driverStarOnly = true
        let disabledOnly = true
        for(const alert of netradyne.alerts){
            let type = alert.alertType
            let severity = alert.alertSeverity
            if(type != "DRIVER-STAR") driverStarOnly = false
            if(!alertCounts[type]) alertCounts[type] = {}
            if(!alertCounts[type][severity]) alertCounts[type][severity] = 1
            else alertCounts[type][severity]++

            let fieldPrefix = 'coaching' + formatNetradyneType(type) 
            let totalAlertCount = Object.keys(alertCounts[type]).reduce((sum,key)=>sum+parseFloat(alertCounts[type][key]||0),0)
            if(disabledOnly && tenant[fieldPrefix + 'CO'] && (totalAlertCount > parseFloat(tenant[fieldPrefix + 'Threshold'])) ){
                disabledOnly = false
            }
        }
        if(!driverStarOnly && !disabledOnly) {
            messageStringNetradyneCO = messageStringNetradyneCO + '\nYou had the following Netradyne alerts for your last trip:'
        }
        let messagePart = ''
        let messageSubPart = ''
        let existSubPart = false

        for(const type of Object.keys(alertCounts)) {
            let fieldPrefix = 'coaching' + formatNetradyneType(type) 
            if (fieldPrefix  == 'coachingDriverInitiated') continue
            let totalAlertCount = Object.keys(alertCounts[type]).reduce((sum,key)=>sum+parseFloat(alertCounts[type][key]||0),0);
            if(tenant[fieldPrefix + 'CO'] && (totalAlertCount > parseFloat(tenant[fieldPrefix + 'Threshold'])) ){
                if(Object.keys(alertCounts[type]).length > 1){
                    messageSubPart = `\n-`
                    existSubPart = true
                    let counterSubPart = 0
                    for(const severity of Object.keys(alertCounts[type])){
                        messageSubPart += netradyneCoachingMessages[fieldPrefix].replace('%value%', alertCounts[type][severity]).replace('%severity%', severity.toLowerCase())
                        counterSubPart++     
                    }
                    if(counterSubPart > 1){
                        messageSubPart = messageSubPart.slice(0, -1)
                        const lastIndex = messageSubPart.lastIndexOf(',')
                        messageSubPart = messageSubPart.substring(0, lastIndex) + ' &' + messageSubPart.substring(lastIndex + 1)
                    }
                }else{
                    for(const severity of Object.keys(alertCounts[type])){
                        messagePart += `\n-` + netradyneCoachingMessages[fieldPrefix].replace('%value%', alertCounts[type][severity]).replace('%severity%', severity.toLowerCase())
                    }
                }
                numberOfNetradyneCO++
            }
            else if(type == 'DRIVER-STAR' && tenant.coachingDriverStarPR && (totalAlertCount >= parseFloat(tenant.coachingDriverStarThresholdPR))){
                messageStringNetradynePR = messageStringNetradynePR + `\n- You earned ${totalAlertCount} Driver Star${totalAlertCount > 1 ? 's': ''} during your last trip!\n`
                numberOfNetradynePR++
            }
        }
        if(!driverStarOnly && !disabledOnly) { 
            messageStringNetradyneCO += `${existSubPart ? messagePart : messagePart.slice(0, -1)}${messageSubPart}. \nTo improve your scorecard ranking, reduce the number of Netradyne alerts you are receiving.\n`
        }

        if(messagePart==='') messageStringNetradyneCO = ''
    }
    
    // get staff data
    var input = {
        id: staffId
    }

    // determine sending method
    var channelType;
    if( staff.receiveTextMessages && staff.receiveEmailMessages) channelType = 'SMS/EMAIL';
    else if( staff.receiveTextMessages ) channelType = 'SMS';
    else if( staff.receiveEmailMessages ) channelType = 'EMAIL';
    else channelType = null

    // if(channelType){
        // try to update existing message in Dynamo

        input = {
            id: pendingMessageId,
            staffId: staffId,
            pendingMessageTenantId: tenant.id,
            group: tenant.group,
            updatedAt: new Date().toISOString()
        }


            // update weekly message fields
            var newMessageCO = ''
            var newMessagePR = ''
            const { date: netradyneDate } = netradyne;
            const formatNetradyneDate = shortDateFormat(netradyneDate, "en-US", { month: "short", day: "numeric", timeZone: 'UTC' });
            // update daily message fields
            if(netradyne){
                // copy weekly message parts from existing message
                if(existingMessage && existingMessage.bodyTextWeeklyCO){
                    newMessageCO = "Here are the areas that need improvement from your latest scorecard for " + existingMessage.startLastWeeklyImport +" through " + existingMessage.endLastWeeklyImport + ":\n" + existingMessage.bodyTextWeeklyCO + "\n"
                    numberOfWeeklyCO = parseInt(existingMessage.numberOfWeeklyCO)
                }
                if(existingMessage && existingMessage.bodyTextWeeklyPR){
                    newMessagePR = "Here are the things you excelled at on your latest scorecard for " + existingMessage.startLastWeeklyImport +" through " + existingMessage.endLastWeeklyImport + ":\n" + existingMessage.bodyTextWeeklyPR + "\n"
                    numberOfWeeklyPR = parseInt(existingMessage.numberOfWeeklyPR)
                }
                
                // copy or overwrite daily message parts from existing message
                if(existingMessage){

                    // use past mentor coaching message if none generated by this import
                    if(existingMessage.bodyTextMentorCO && !messageStringMentorCO){
                        messageStringMentorCO += existingMessage.bodyTextMentorCO
                        numberOfMentorCO = existingMessage.numberOfMentorCO
                    } 
                    // use past mentor reinforcement message if none generated by this import
                    if(existingMessage.bodyTextMentorPR && !messageStringMentorPR){
                        messageStringMentorPR += existingMessage.bodyTextMentorPR
                        numberOfMentorPR = existingMessage.numberOfMentorPR
                    }
                    
                    // use past eoc coaching message if none generated by this import
                    if(existingMessage.bodyTextEocCO && !messageStringEocCO){
                        messageStringEocCO += existingMessage.bodyTextEocCO
                        numberOfEocCO = existingMessage.numberOfEocCO
                    } 
                    // use past eoc reinforcement message if none generated by this import
                    if(existingMessage.bodyTextEocPR && !messageStringEocPR){
                        messageStringEocPR += existingMessage.bodyTextEocPR
                        numberOfEocPR = existingMessage.numberOfEocPR
                    }
                }
                
                
                // add message parts together to form daily messages
                messageStringDailyCO = joinMessages(messageStringDailyCO, messageStringMentorCO, messageStringNetradyneCO, messageStringEocCO)
                messageStringDailyPR = joinMessages(messageStringDailyPR, messageStringMentorPR, messageStringNetradynePR, messageStringEocPR)
                if(validValues(messageStringDailyCO)) newMessageCO += "Here are the areas that need improvement from your " + formatNetradyneDate +" trip:\n" + messageStringDailyCO
                if(validValues(messageStringDailyPR)) newMessagePR += "Here are the things you excelled at on your " + formatNetradyneDate + " trip:\n" + messageStringDailyPR
            }

            numberOfDailyCO = parseInt(numberOfMentorCO||0) + parseInt(numberOfNetradyneCO||0) + parseInt(numberOfEocCO||0)
            numberOfDailyPR = parseInt(numberOfMentorPR||0) + parseInt(numberOfNetradynePR||0) + parseInt(numberOfEocPR||0)

            if(!existingMessage){
                input = {
                    id: pendingMessageId,
                    staffId: staffId,
                    pendingMessageTenantId: tenant.id,
                    group: tenant.group,
                    numberOfWeeklyCO: numberOfWeeklyCO,
                    numberOfWeeklyPR: numberOfWeeklyPR,
                    numberOfDailyCO: numberOfDailyCO,
                    numberOfNetradyneCO: numberOfNetradyneCO,
                    numberOfDailyPR: numberOfDailyPR,
                    numberOfNetradynePR: numberOfNetradynePR,
                    numberOfCO: numberOfWeeklyCO + numberOfDailyCO,
                    numberOfPR: numberOfWeeklyPR + numberOfDailyPR,
                    bodyTextDailyCO: messageStringDailyCO,
                    bodyTextNetradyneCO: messageStringNetradyneCO,
                    bodyTextDailyPR: messageStringDailyPR,
                    bodyTextNetradynePR: messageStringNetradynePR,
                    channelType: channelType,
                    createdAt: input.updatedAt,
                    updatedAt: input.updatedAt,
                    lastDailyImport: formatNetradyneDate
                }
                input.bodyTextCO = ''
                input.bodyTextPR = ''
                if(importId){
                    input.importId = importId
                }
                if(!validValues(messageStringDailyCO, messageStringDailyPR)){
                    return
                }
                else{
                    if(validValues(messageStringDailyCO)) input.bodyTextCO += "Here are the areas that need improvement from your " + formatNetradyneDate +" trip:\n" + messageStringDailyCO
                    if(validValues(messageStringDailyPR)) input.bodyTextPR += "Here are the things you excelled at on your " + formatNetradyneDate +" trip:\n" + messageStringDailyPR
                }
                proxy.createOrUpdateItem(input, pendingMessageId, 'pendingMessages', proxy.tmpTable)
            }else{
                input.bodyTextDailyCO = messageStringDailyCO
                input.bodyTextNetradyneCO = messageStringNetradyneCO
                input.bodyTextCO = trimSpaces(newMessageCO)

                input.bodyTextDailyPR = messageStringDailyPR
                input.bodyTextNetradynePR = messageStringNetradynePR
                input.bodyTextPR = trimSpaces(newMessagePR)

                input.numberOfDailyCO = numberOfDailyCO
                input.numberOfNetradyneCO = numberOfNetradyneCO
                input.numberOfDailyPR = numberOfDailyPR
                input.numberOfNetradynePR = numberOfNetradynePR

                input.numberOfCO = numberOfDailyCO + numberOfWeeklyCO
                input.numberOfPR = numberOfDailyPR + numberOfWeeklyPR

                input.channelType = channelType
                input.importId = importId
                input.lastDailyImport = formatNetradyneDate

                proxy.createOrUpdateItem(input, pendingMessageId, 'pendingMessages', proxy.tmpTable)
            }
        
    // }
}

const createPendingMessagesEOC = async function(importId, eoc, staff, proxy){
    if(!eoc){
        return
    }
    const tenant = proxy.tenant
    const pendingMessages = proxy.tmpTable.pendingMessages
    const staffId = staff.id
    var numberOfWeeklyCO = ''
    var numberOfWeeklyPR = ''

    var messageStringDailyCO = '';
    var messageStringMentorCO = '';
    var messageStringNetradyneCO = '';
    var messageStringEocCO = '';
    var messageStringDailyPR = '';
    var messageStringMentorPR = '';
    var messageStringNetradynePR = '';
    var messageStringEocPR = '';
    var numberOfDailyCO = 0
    var numberOfMentorCO = 0
    var numberOfNetradyneCO = 0
    var numberOfEocCO = 0
    var numberOfDailyPR = 0
    var numberOfMentorPR = 0
    var numberOfNetradynePR = 0
    var numberOfEocPR = 0

    //FIND EXISTING MESSAGE
    var pendingMessageId = tenant.id + staffId
    let existingMessage = pendingMessages[pendingMessageId]
    if(!existingMessage){
        try{
            existingMessage = await getPendingMessageById(pendingMessageId)
        }
        catch(e){
            console.log(e);
            return
        }
    }

    
    // ------------ EOC ------------
    if(eoc){
        
        // exit if all eoc messages disabled
        if( !tenant.coachingDailyComplianceRateCO && !tenant.coachingDailyComplianceRatePR ){
            return
        }

        let eocScore = null
        
        if(typeof(eoc.dateToCheck)!='undefined' && typeof(eoc[eoc.dateToCheck]) !='undefined' && eoc[eoc.dateToCheck]){
            eocScore = (Math.floor(eoc[eoc.dateToCheck]*10000)/100) 
        }

        // --- Coaching Opportunities ---
        // Daily Compliance
        if( (tenant.coachingDailyComplianceRateCO) &&
            eocScore && (parseFloat(eocScore) < parseFloat(tenant.coachingDailyComplianceRateThreshold ))
        ){

            messageStringEocCO = messageStringEocCO + `\n- Your average Engine Off Compliance was ${eocScore}. Please remember to turn your engine off whenever you leave the vehicle.`
            numberOfEocCO++

        }

        // --- Positive reinforcement ---
        // Daily Compliance
        if(
            (tenant.coachingDailyComplianceRatePR) &&
            eocScore && (parseFloat(eocScore) >= parseFloat(tenant.coachingDailyComplianceRateThreshold))
        ){
            
            messageStringEocPR = messageStringEocPR + `\n- You did a great job with your Engine Off Compliance, averaging ${eocScore}. Nice work!`
            numberOfEocPR++

        }

    }

    // determine sending method
    var channelType;
    if( staff.receiveTextMessages && staff.receiveEmailMessages) channelType = 'SMS/EMAIL';
    else if( staff.receiveTextMessages ) channelType = 'SMS';
    else if( staff.receiveEmailMessages ) channelType = 'EMAIL';
    else channelType = null

    let input = {
        id: pendingMessageId,
        staffId: staffId,
        pendingMessageTenantId: tenant.id,
        group: tenant.group,
        updatedAt: new Date().toISOString()
    }

    // update weekly message fields
    var newMessageCO = ''
    var newMessagePR = ''

    const { date: eocDate } = eoc;
    const formatEOCDate = shortDateFormat(eocDate, "en-US", { month: "short", day: "numeric", timeZone: 'UTC' });
    // update daily message fields

    // copy weekly message parts from existing message
    if(existingMessage && existingMessage.bodyTextWeeklyCO){
        newMessageCO = "Here are the areas that need improvement from your latest scorecard for " + existingMessage.startLastWeeklyImport +" through " + existingMessage.endLastWeeklyImport + ":\n" + existingMessage.bodyTextWeeklyCO + "\n"
        numberOfWeeklyCO = parseInt(existingMessage.numberOfWeeklyCO)
    }
    if(existingMessage && existingMessage.bodyTextWeeklyPR){
        newMessagePR = "Here are the things you excelled at on your latest scorecard for " + existingMessage.startLastWeeklyImport +" through " + existingMessage.endLastWeeklyImport + ":\n" + existingMessage.bodyTextWeeklyPR + "\n"
        numberOfWeeklyPR = parseInt(existingMessage.numberOfWeeklyPR)
    }
    
    // copy or overwrite daily message parts from existing message
    if(existingMessage){

        // use past mentor coaching message if none generated by this import
        if(existingMessage.bodyTextMentorCO && !messageStringMentorCO){
            messageStringMentorCO += existingMessage.bodyTextMentorCO
            numberOfMentorCO = existingMessage.numberOfMentorCO
        } 
        // use past mentor reinforcement message if none generated by this import
        if(existingMessage.bodyTextMentorPR && !messageStringMentorPR){
            messageStringMentorPR += existingMessage.bodyTextMentorPR
            numberOfMentorPR = existingMessage.numberOfMentorPR
        }
        
        // use past netradyne coaching message if none generated by this import
        if(existingMessage.bodyTextNetradyneCO && !messageStringNetradyneCO){
            messageStringNetradyneCO += existingMessage.bodyTextNetradyneCO
            numberOfNetradyneCO = existingMessage.numberOfNetradyneCO
        }   
        // use past netradyne reinforcement message if none generated by this import
        if(existingMessage.bodyTextNetradynePR && !messageStringNetradynePR){
            messageStringNetradynePR += existingMessage.bodyTextNetradynePR
            numberOfNetradynePR = existingMessage.numberOfNetradynePR
        } 

    }
    
    // add message parts together to form daily messages
    messageStringDailyCO = joinMessages(messageStringDailyCO, messageStringMentorCO, messageStringNetradyneCO, messageStringEocCO)
    messageStringDailyPR = joinMessages(messageStringDailyPR, messageStringMentorPR, messageStringNetradynePR, messageStringEocPR)

    if(validValues(messageStringDailyCO)) newMessageCO += "Here are the areas that need improvement from your " + formatEOCDate +" trip:\n" + messageStringDailyCO
    if(validValues(messageStringDailyPR)) newMessagePR += "Here are the things you excelled at on your " + formatEOCDate +" trip:\n" + messageStringDailyPR

    numberOfDailyCO = parseInt(numberOfMentorCO||0) + parseInt(numberOfNetradyneCO||0) + parseInt(numberOfEocCO||0)
    numberOfDailyPR = parseInt(numberOfMentorPR||0) + parseInt(numberOfNetradynePR||0) + parseInt(numberOfEocPR||0)
    
    if(!existingMessage){
        input = {
            id: pendingMessageId,
            staffId: staffId,
            pendingMessageTenantId: tenant.id,
            group: tenant.group,
            numberOfDailyCO: numberOfDailyCO,
            numberOfEocCO: numberOfEocCO,
            numberOfDailyPR: numberOfDailyPR,
            numberOfEocPR: numberOfEocPR,
            numberOfCO: numberOfWeeklyCO + numberOfDailyCO,
            numberOfPR: numberOfWeeklyPR + numberOfDailyPR,
            bodyTextDailyCO: messageStringDailyCO,
            bodyTextEocCO: messageStringEocCO,
            bodyTextDailyPR: messageStringDailyPR,
            bodyTextEocPR: messageStringEocPR,
            channelType: channelType,
            createdAt: input.updatedAt,
            updatedAt: input.updatedAt,
            lastDailyImport: formatEOCDate
        }
        input.bodyTextCO = ''
        input.bodyTextPR = ''
        if(importId){
            input.importId = importId
        }
        if(!validValues(messageStringDailyCO, messageStringDailyPR)){
            return
        }
        else{
            if(validValues(messageStringDailyCO)) input.bodyTextCO += "Here are the areas that need improvement from your " + formatEOCDate +" trip:\n" + messageStringDailyCO
            if(validValues(messageStringDailyPR)) input.bodyTextPR += "Here are the things you excelled at on your " + formatEOCDate +" trip:\n" + messageStringDailyPR
        }
        proxy.createOrUpdateItem(input, pendingMessageId, 'pendingMessages', proxy.tmpTable)
    }else{
        
        input.bodyTextDailyCO = messageStringDailyCO
        input.bodyTextEocCO = messageStringEocCO
        input.bodyTextCO = trimSpaces(newMessageCO)

        input.bodyTextDailyPR = messageStringDailyPR
        input.bodyTextEocPR = messageStringEocPR
        input.bodyTextPR = trimSpaces(newMessagePR)

        input.numberOfDailyCO = numberOfDailyCO
        input.numberOfEocCO = numberOfEocCO
        input.numberOfDailyPR = numberOfDailyPR
        input.numberOfEocPR = numberOfEocPR

        input.numberOfCO = numberOfDailyCO + numberOfWeeklyCO
        input.numberOfPR = numberOfDailyPR + numberOfWeeklyPR

        input.channelType = channelType
        input.importId = importId
        input.lastDailyImport = formatEOCDate

        proxy.createOrUpdateItem(input, pendingMessageId, 'pendingMessages', proxy.tmpTable)
    }

}

module.exports = { createPendingMessagesMentor ,createPendingMessagesNetradyne, createPendingMessagesEOC };