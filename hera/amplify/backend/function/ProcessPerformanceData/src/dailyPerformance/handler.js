// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION})

// load AppSync and graphQL
const {
    executeMutation
} = require('../appSync.js');

const {
    createNotification,
    createCoachingHistory,
    createCoachingRecords,
} = require('../graphql.js');

const { cleanTransporterId, generateUUID } = require('../util.js')
const { getTenantById, getAllJobsByJobId, getAllAssociatesByGroup, getCustomListByGroupAndType, getAllOptionsCustomListsByCustomListsId} = require('../helper.js')
const { compileFile } = require('./dailyCompileHelper.js')
const { importProcess } = require('./dailyImportHelper')
// create a new s3 object
const s3 = new AWS.S3();

const dailyHandler = async (event, context, callback)=>{

    console.log("*Daily Data Import*")

    // Global vars
    const storage = {
        associateLevel: {
            mentorData: [],
            netradyneData: [],
            eocData: []
        },
        dspLevel: {
            eocData: []
        }
    }

    const importCounts = {
        mentor: { created: 0, updated: 0, errors: 0, savedFiles: 0 },
        netradyne: { created: 0, updated: 0, errors: 0, savedFiles: 0 },
        eoc: { created: 0, updated: 0, errors: 0, savedFiles: 0 }
    }

    const performanceCounts = {
        message: { created: 0, updated: 0, errors: 0 },
        issue: { created: 0, updated: 0, deleted: 0, errors: 0 },
        kudo: { created: 0, updated: 0, deleted: 0, errors: 0 }
    }

    const importErrors = {
        mentor: [],
        netradyne: [],
        eoc: []
    }

    const tmpTable = {
        associates:{},
        pendingMessages:{},
        issues:{},
        kudos:{}
    }

    const { importId: jobId, rows: matchedStaffs, group, clientTimeZone, owner, tenantId, user, daysToImport } = event
    const tenant = await getTenantById(tenantId)

    const succeededFiles = (await getAllJobsByJobId(jobId)).filter(job => {
        if(job.jobStatus === 'SUCCEEDED'){
            job.results = JSON.parse(job.results)
            return true
        }
    })

    const associates = await getAllAssociatesByGroup(group)
    const associateLookupMap = {}
    
    associates.forEach((associate) => {
        if(associate.transporterId){
            const key = cleanTransporterId(associate.transporterId)
            associateLookupMap[key] = {...associate, transporterId: key}
        }
    })

    //get custom list issue-type and kudo-type
    const customListProjectedFields = ['id','type']
    const customListIssueType = await getCustomListByGroupAndType(group, 'issue-type', customListProjectedFields)
    const customListIssueTypeId = customListIssueType[0]?.id
    const customListKudoType = await getCustomListByGroupAndType(group, 'kudo-type', customListProjectedFields)
    const customListKudoTypeId = customListKudoType[0]?.id

    const optionCustomProjectedFields = ['id','option']
    let issueTypeList = []
    let kudoTypeList = []
    if(customListIssueTypeId && customListKudoTypeId){
        issueTypeList = await getAllOptionsCustomListsByCustomListsId(customListIssueTypeId, optionCustomProjectedFields) || []
        kudoTypeList = await getAllOptionsCustomListsByCustomListsId(customListKudoTypeId, optionCustomProjectedFields) || []
    }

    const proxy = {
        tenant,
        associates,
        associateLookupMap,
        performanceCounts,
        importCounts,
        importErrors,
        tmpTable,
        clientTimeZone,
        user,
        issueTypeList,
        kudoTypeList,
        daysToImport
    }

    succeededFiles.forEach(job => compileFile(job, storage, proxy, matchedStaffs) )

    await importProcess(storage, proxy)

    for (const job of succeededFiles) {
        if(!job.results.alreadyUploaded){
            const copySource = `${process.env.STORAGE_HERA_BUCKETNAME}/public/${job.key}`
            const params = {
                Bucket: process.env.STORAGE_HERA_BUCKETNAME,
                CopySource: encodeURI(copySource),
                Key: 'public/' + job.key,
                Expires: null,
                MetadataDirective: 'REPLACE',
                TaggingDirective: 'REPLACE'
            }
            // Update file
            const response = await s3.copyObject(params).promise().catch((e) => console.error(e))
            if(response.CopyObjectResult){
                importCounts[job.type]['savedFiles']++
            }
        }
    }

    for (const type of ['mentor', 'netradyne', 'eoc']) {
        const importCount = importCounts[type]
        if(importCount.created || importCount.updated || importCount.errors || importCount.savedFiles){
            try{
                let input = {
                    id: generateUUID(),
                    group: group,
                    importId: jobId,
                    type,
                    results: JSON.stringify(importCount),
                }
                
                await executeMutation(createCoachingHistory, { input })
            }catch(e){
                console.error(`coachingHistorys${type[0].toUpperCase()+type.substring(1)}Result Error: `, e)
            }
        }
    }

    for (const type of ['kudo','issue','message']) {
        const performanceCount = performanceCounts[type]
        if(performanceCount.created || performanceCount.updated || performanceCount.errors){
            try{
                let input = {
                    id: generateUUID(),
                    group: group,
                    importId: jobId,
                    type: type === 'message'? 'pendingMessage' : type,
                    results: JSON.stringify(performanceCount),
                }
                
                await executeMutation(createCoachingRecords, { input })
            }catch(e){
                console.error(`${type[0].toUpperCase()+type.substring(1)} createCoachingRecords Error: `, e)
            }
        }
    }
    
    try {
        const input = {
            group: group,
            owner: owner,
            title: "Daily performance import complete",
            description:  "Your import started " + new Date(parseInt(jobId.split('_')[1])).toLocaleString('en-US', {timeZone: clientTimeZone}) + " is complete. Click here to view the results.",
            payload: JSON.stringify({name: 'DailyDataImport', params: {importId: jobId, importCounts, importErrors, performanceCounts}}),
            clickAction: 'navigate',
            isRead: false
        }
        await executeMutation(createNotification, {input})

    } catch(e) {

        console.log('---error', e)

        const input = {
            group: group,
            owner: owner,
            title: "Daily performance import complete",
            description:  "Your import started is complete. Click here to view the results.",
            payload: JSON.stringify({name: 'DailyDataImport', params: {importId: jobId, importCounts, importErrors, performanceCounts}}),
            clickAction: 'navigate',
            isRead: false
        }
        await executeMutation(createNotification, {input})

    } finally{
        const response = {
            statusCode: 200,
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "*"
            }, 
            body: JSON.stringify({importId: jobId, importCounts, importErrors, performanceCounts}),
        };
        return response;
    }

}

module.exports = { dailyHandler }