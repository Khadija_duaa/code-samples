const {
    cleanInput,
    cleanTransporterId,
    keyOf,
    AWSDateToDate
} = require('../util.js')

const searchBySavedNames = (proxy, firstName, lastName) => proxy.associates.find(associate =>{
    return (cleanInput(associate.firstName) == cleanInput(firstName)) && (cleanInput(associate.lastName) == cleanInput(lastName))
})
const searchByAlternateNames = (proxy, driverName) => proxy.associates.find(associate => {
    return associate.alternateNames && associate.alternateNames.some(altName => {
        return altName.toLowerCase() == driverName.toLowerCase()
    })
})
const searchByNetradyneDriverId = (proxy, driverId) => proxy.associates.find(associate => {
    return associate.netradyneDriverId && (associate.netradyneDriverId == driverId)
})

const postFiltering = function(associateList, matchedStaffs, nameKey, transporterId){
    const alreadyMatched = matchedStaffs.find( staff => {
        if(staff.importNameKey && nameKey) return staff.importNameKey === nameKey
        if(staff.importTransporterId && transporterId) return staff.importTransporterId === transporterId
    })
    let associateMatch
    if(alreadyMatched){
        associateMatch = associateList.find(a => a.id === alreadyMatched.id )
    }
    return {matched: alreadyMatched, associate: associateMatch}
}

const matchStaff = (Val, key) => staff => {
    if(key){
        return staff[key] === Val[key]
    }
    if(!Val.importNameKey && Val.importName === '(No Name Imported)'){
        return Val.importTransporterId ? staff.importTransporterId === Val.importTransporterId : false
    }
    return (staff.id === Val.id && !!Val.id) ||
        (staff.matchTransporterId === Val.matchTransporterId && !!Val.matchTransporterId)||
        staff.importNameKey === Val.importNameKey
}

const compileFile = function(succeededFile, storage, proxy, matchedStaffs){
    if(succeededFile.type === "mentor"){
        compileMentor(succeededFile, storage, proxy, matchedStaffs)
    }
    else if(succeededFile.type === "netradyne"){
        compileNetradyne(succeededFile, storage, proxy, matchedStaffs)
    }
    else if(succeededFile.type === "eoc"){
        compileEOC(succeededFile, storage, proxy, matchedStaffs)
    }
}

const compileMentor = function(succeededFile, storage, proxy, matchedStaffs){
    const associateMentorData = storage.associateLevel.mentorData

    if(!succeededFile.results || !succeededFile.results.associateLevel){
        return
    }

    succeededFile.results.associateLevel.forEach(row =>{
        row.date = AWSDateToDate(succeededFile.date)
        row.textractJobId = succeededFile.id
        let previouslyMatched = false
        let associateMatch = {id: null}
        let suggestionFound = false
        var nameKey = ''
        var transporterId = null
        let badNames = ['not available', 'coming soon', 'unknown driver', 'null']
        var driverName = !row.driverName ? "(No Name Imported)" : row.driverName
        const driverNameCheck = driverName.toLowerCase()

        if(badNames.includes(driverNameCheck)) {
            driverName = "(No Name Imported)"
        }
                            
        const nameSplit = driverName.split(" ")        
        const firstName = nameSplit[0].toLowerCase()
        const lastName  = nameSplit[nameSplit.length - 1].toLowerCase()

        // use name on suggested db record as hash key if matched
        associateMatch = searchBySavedNames(proxy, firstName, lastName)

        // mark as previously matched
        // use name on matched db record as hash key if matched
        if(associateMatch){
            previouslyMatched = true
            nameKey = associateMatch.firstName + associateMatch.lastName
            transporterId = associateMatch.transporterId
        }
        // look for a suggestion if not matched
        // find available match based on list of saved alt names
        else{
            associateMatch = searchByAlternateNames(proxy, driverName)
            if(associateMatch){
                suggestionFound = true
                proxy.suggestionFound && proxy.suggestionFound(true)
                nameKey = associateMatch.firstName + associateMatch.lastName
                transporterId = associateMatch.transporterId
            }
            else{
                // use name from import as hash key if no match or suggestion found in the db
                nameKey = driverName
                associateMatch = {id: null}
            }
            previouslyMatched = false
        }

        nameKey = keyOf(nameKey)
        
        let importTransporterId = transporterId ? cleanTransporterId(transporterId) : null

        if(matchedStaffs){
            const {associate, matched} = postFiltering(proxy.associates, matchedStaffs, nameKey, transporterId)
            if(associate){
                associateMatch = associate
                transporterId = associate.transporterId
                nameKey = matched.importNameKey
                importTransporterId = matched.importTransporterId
            }else{
                return
            }
        }

        const associateId = associateMatch.id
        const previuosRegisteredDa = associateMentorData.find(matchStaff({id: associateId, matchTransporterId: transporterId, importNameKey: nameKey, importName: driverName }))

        if(previuosRegisteredDa){
            previuosRegisteredDa.importData.push(row)
        }else{
            const associate = {
                importName: driverName,
                importNameKey: nameKey,
                importTransporterId: importTransporterId,
                id: associateId,
                matchFirstName: associateMatch.firstName,
                matchLastName: associateMatch.lastName,
                matchTransporterId: transporterId,
                matchStatus: associateMatch.status,
                suggestionFound: suggestionFound,
                previouslyMatched: previouslyMatched,
                importData: [row]
            }
            associateMentorData.push(associate)
        }
    })
}

const compileNetradyne = function(succeededFile, storage, proxy, matchedStaffs){
    
    const associateNetradyneData = storage.associateLevel.netradyneData
    
    if(!succeededFile.results || !succeededFile.results.associateLevel){
        return
    }

    const netradynes = succeededFile.results.associateLevel.reduce((map, row)=>{
        const {driverName, driverId, ...alert} = row
        const namekey = keyOf(driverName)
        const record = map.find(m => m.driverId === driverId || keyOf(m.driverName) === namekey)
        if(record){
            record.alerts.push(alert)
        }else{
            map.push({
                driverName,
                driverId,
                alerts: [alert]
            })
        }
        return map
    },[])
    
    netradynes.forEach(row =>{
        row.date = AWSDateToDate(succeededFile.date)
        row.textractJobId = succeededFile.id
        let previouslyMatched = false
        let associateMatch = {id: null}
        let suggestionFound = false
        var nameKey = ''
        var transporterId = null
        let badNames = ['not available', 'coming soon', 'unknown driver', 'null']
        var driverName = !row.driverName ? "(No Name Imported)" : row.driverName
        const driverNameCheck = driverName.toLowerCase()
        const driverId = row.driverId

        if(badNames.includes(driverNameCheck)) {
            driverName = "(No Name Imported)"
        }
                            
        const nameSplit = driverName.split(" ")        
        const firstName = nameSplit[0].toLowerCase()
        const lastName  = nameSplit[nameSplit.length - 1].toLowerCase()

        // find available match based on list of saved driver ids
        associateMatch = searchByNetradyneDriverId(proxy, driverId)

        // mark as previously matched
        // use name on matched db record as hash key if matched
        if(associateMatch){
            previouslyMatched = true
            nameKey = associateMatch.firstName + associateMatch.lastName
            transporterId = associateMatch.transporterId
        }
        // find available match based on list of saved alt names if driver id fails
        else{
            associateMatch = searchBySavedNames(proxy, firstName, lastName)
            if(associateMatch){
                previouslyMatched = true
                nameKey = associateMatch.firstName + associateMatch.lastName
                transporterId = associateMatch.transporterId
            }
            // look for a suggestion if not matched
            // use name on suggested db record as hash key if matched
            else{
                associateMatch = searchByAlternateNames(proxy, driverName)

                if(associateMatch){
                    suggestionFound = true
                    proxy.suggestionFound && proxy.suggestionFound(true)
                    nameKey = associateMatch.firstName + associateMatch.lastName
                    transporterId = associateMatch.transporterId
                }
                else{
                    // use name from import as hash key if no match or suggestion found in the db
                    nameKey = driverName
                    associateMatch = {id: null}
                }
                previouslyMatched = false
            }

        }

        nameKey = keyOf(nameKey)

        let importTransporterId = transporterId ? cleanTransporterId(transporterId) : null

        if(matchedStaffs){
            const {associate, matched} = postFiltering(proxy.associates, matchedStaffs, nameKey, transporterId)
            if(associate){
                associateMatch = associate
                transporterId = associate.transporterId
                nameKey = matched.importNameKey
                importTransporterId = matched.importTransporterId
            }else{
                return
            }
        }

        const associateId = associateMatch.id
        const previuosRegisteredDa = associateNetradyneData.find(matchStaff({id: associateId, matchTransporterId: transporterId, importNameKey: nameKey, importName: driverName }))

        if(previuosRegisteredDa){
            previuosRegisteredDa.importData.push(row)
        }else{
            const associate = {
                importName: driverName,
                importNameKey: nameKey,
                importTransporterId: importTransporterId,
                id: associateId,
                matchFirstName: associateMatch.firstName,
                matchLastName: associateMatch.lastName,
                matchTransporterId: transporterId,
                matchStatus: associateMatch.status,
                suggestionFound: suggestionFound,
                previouslyMatched: previouslyMatched,
                importData: [row]
            }
            associateNetradyneData.push(associate)
        }
    })
}

const compileEOC = function(succeededFile, storage, proxy, matchedStaffs){

    const dspEocData = storage.dspLevel.eocData
    const associateEocData = storage.associateLevel.eocData

    if( !succeededFile.results || !succeededFile.results.dspLevel || !succeededFile.results.associateLevel ){
        return
    }

    const dspLevel = succeededFile.results.dspLevel

    const record = dspLevel[0]
    record.date = AWSDateToDate(succeededFile.date)
    record.textractJobId = succeededFile.id
    record.fileName = succeededFile.fileName
    dspEocData.push(record)

    const associateLevel = succeededFile.results.associateLevel
    associateLevel.forEach(row => {
        
        row.date = AWSDateToDate(succeededFile.date)
        row.textractJobId = succeededFile.id
        let previouslyMatched = false
        let associateMatch = {id: null}
        let suggestionFound = false 
        var nameKey = ''
        var transporterId = null
        let badNames = ['not available', 'coming soon', 'unknown driver', 'null']
        var driverName = !row['da_name'] ? "(No Name Imported)" : row['da_name']
        const driverNameCheck = driverName.toLowerCase()

        if(badNames.includes(driverNameCheck)) {
            driverName = "(No Name Imported)"
        }
                            
        const nameSplit = driverName.split(" ")        
        const firstName = nameSplit[0].toLowerCase()
        const lastName  = nameSplit[nameSplit.length - 1].toLowerCase()

        if(row.transporter_id){

            //Format transporter id for better lookup results
            transporterId = cleanTransporterId(row.transporter_id)

            // find available match based on exact transporter ID
            if(proxy.associateLookupMap[transporterId]){
                associateMatch = proxy.associateLookupMap[transporterId]
                previouslyMatched = true
                driverName = associateMatch.firstName + ' ' + associateMatch.lastName
                nameKey = driverName
            }
        }else if(driverName !== "(No Name Imported)"){
             // use name on suggested db record as hash key if matched
            associateMatch = searchBySavedNames(proxy, firstName, lastName)

            // mark as previously matched
            // use name on matched db record as hash key if matched
            if(associateMatch){
                previouslyMatched = true
                nameKey = associateMatch.firstName + associateMatch.lastName
                transporterId = associateMatch.transporterId
            }
            // look for a suggestion if not matched
            // find available match based on list of saved alt names
            else{
                associateMatch = searchByAlternateNames(proxy, driverName)
                if(associateMatch){
                    suggestionFound = true
                    proxy.suggestionFound && proxy.suggestionFound(true)
                    nameKey = associateMatch.firstName + associateMatch.lastName
                    transporterId = associateMatch.transporterId
                }
                else{
                    // use name from import as hash key if no match or suggestion found in the db
                    nameKey = driverName
                    associateMatch = {id: null}
                }
                previouslyMatched = false
            }
        }else{
            return
        }
        nameKey = keyOf(nameKey)

        let importTransporterId = transporterId ? cleanTransporterId(transporterId) : null

        if(matchedStaffs){
            const {associate, matched} = postFiltering(proxy.associates, matchedStaffs, nameKey, transporterId)
            if(associate){
                associateMatch = associate
                transporterId = associate.transporterId
                nameKey = matched.importNameKey
                importTransporterId = matched.importTransporterId
            }else{
                return
            }
        }

        const associateId = associateMatch.id
        const previuosRegisteredDa = associateEocData.find(matchStaff({id: associateId, matchTransporterId: transporterId }))

        if(previuosRegisteredDa){
            previuosRegisteredDa.importData.push(row)
        }else{
            const associate = {
                importName: driverName,
                importNameKey: nameKey,
                importTransporterId: importTransporterId,
                id: associateId,
                matchFirstName: associateMatch.firstName,
                matchLastName: associateMatch.lastName,
                matchTransporterId: transporterId,
                matchStatus: associateMatch.status,
                suggestionFound: suggestionFound,
                previouslyMatched: previouslyMatched,
                importData: [row]
            }
            associateEocData.push(associate)
        }
        
    })

}

const getTableData = function(storage){
    const {mentorData, netradyneData, eocData} = storage.associateLevel
    // combine hash maps into array
    const table = [[],[]]
    if(!Object.keys(mentorData).length && !Object.keys(netradyneData).length && !Object.keys(eocData).length) return table

    // loop through mentor data
    for(const mentorRow of mentorData){
        const netradyneIndex = netradyneData.findIndex(matchStaff(mentorRow))
        const eocIndex = eocData.findIndex(matchStaff(mentorRow))
        const netradyne = netradyneData?.[netradyneIndex]
        const eoc = eocData?.[eocIndex]
        const {importData, ...staff} = mentorRow
        const prevMatch = staff.previouslyMatched
        const tableRow = {
            id: mentorRow.id,
            staff: staff,
            previouslyMatched: prevMatch,
        }
        tableRow.mentorData = importData
        tableRow.netradyneData = netradyne?.importData || []
        tableRow.eocData = eoc?.importData || []
        if(prevMatch)
            table[1].push(tableRow)
        else
            table[0].push(tableRow)
        if(tableRow.netradyneData.length)
            netradyneData.splice(netradyneIndex, 1)
        if(tableRow.eocData.length)
            eocData.splice(eocIndex, 1)
        
    }
    
    // loop through any remaining netradyne data
    for(const netradyneRow of netradyneData){
        const eocIndex = eocData.findIndex(matchStaff(netradyneRow))
        const eoc = eocData?.[eocIndex]
        const {importData, ...staff} = netradyneRow
        const prevMatch = staff.previouslyMatched
        let tableRow = {
            id: netradyneRow.id,
            staff: staff,
            previouslyMatched: prevMatch,
            mentorData: []
        }
        
        tableRow.netradyneData = importData
        tableRow.eocData = eoc?.importData || []
        if(prevMatch)
            table[1].push(tableRow)
        else
            table[0].push(tableRow)
        if(tableRow.eocData.length)
            eocData.splice(eocIndex, 1)
    }

    // loop through any remaining eoc data
    for(const eocRow of eocData){
        const {importData, ...staff} = eocRow
        const prevMatch = staff.previouslyMatched
        let tableRow = {
            id: eocRow.id,
            staff: staff,
            previouslyMatched: prevMatch,
            mentorData: [],
            netradyneData: []
        }
        tableRow.eocData = importData
        if(prevMatch)
            table[1].push(tableRow)
        else
            table[0].push(tableRow)
    }
    return table
}


module.exports = { matchStaff, compileFile, compileMentor, compileNetradyne, compileEOC, getTableData }