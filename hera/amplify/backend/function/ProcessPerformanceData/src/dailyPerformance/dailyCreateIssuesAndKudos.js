const {parseDateToTimezoneTimestamp, getTableName} = require('../util');

// removes hyphens and spaces and camel cases value
function formatNetradyneType(value){
    return value.replace('-', ' ').replace('-',' ').split(' ').map(word => word[0].toUpperCase() + word.substr(1).toLowerCase()).join(' ').replace(/\s+/g, '')
}

function toTitleCase(value){
    return value.replace('-', ' ').replace('-',' ').split(' ').map(word => word.charAt(0).toUpperCase() + word.substr(1).toLowerCase()).join(' ')
}

const createIssuesAndKudosMentor = async function(mentor, proxy){
    if(!mentor){
        return
    }
    const tenant = proxy.tenant
    const staffId = mentor.staffMentorStaffId

    let metrics = [
        { code: 'f', polarity: 1, name: 'DailyFico', fieldName: 'fico', type: 'Mentor Daily FICO®'},
        { code: 'msr', polarity: 0, name: 'Seatbelt', fieldName: 'seatbelt', type: 'Mentor Seatbelt Rate'},
        { code: 'sse', polarity: 0, name: 'Sse', fieldName: 'sse', type: 'Mentor Sse'},
        { code: 'speeding', polarity: 0, name: 'Sse', fieldName: 'speeding', type: 'Mentor Sse'},
        { code: 'tr', polarity: 1, name: 'TraningRemaining', fieldName: 'trainings', type: 'Mentor Trainings Remaining'}
    ]
    let trainingsRemaining = parseFloat(mentor.trainingAssigned) - parseFloat(mentor.trainingCompleted)

    metrics.forEach(metric => {
        let compareVal = metric.code === 'tr' ? trainingsRemaining : parseFloat(mentor[metric.fieldName])
        let date = mentor.date
        let day = String(date.getDate()).padStart(2, '0')
        let month = String(date.getMonth() + 1).padStart(2, '0')
        let year = date.getFullYear()
        date = `${year}-${month}-${day}`

        // --- Issues ---
        if((tenant['coaching' + metric.name + 'Issue']) && 
            (
                ( metric.polarity  && compareVal < parseFloat(tenant['coaching' + metric.name + 'ThresholdIssue'])) ||
                ( !metric.polarity && compareVal > parseFloat(tenant['coaching' + metric.name + 'ThresholdIssue'])) ||
                ( metric.code === 'tr' && compareVal > 0)
            )
        ){
            let input = {
                id: date + '-m-' + metric.code + '-' + staffId,
                infractionStaffId: staffId, 
                staffId: staffId,
                date: parseDateToTimezoneTimestamp(date, proxy.clientTimeZone),
                comment: 'Received ' + compareVal + ' (Created from Mentor import)',
                group: tenant.group,
                updatedAt: new Date().toISOString()
            }
            const infractionTypeId = proxy.issueTypeList.find(item => item.option ===  metric.type)?.id
            if(infractionTypeId) input.infractionTypeId = infractionTypeId
            else input.infractionType = metric.type

            // createRecord(input, process.env.DA_ISSUE_TABLE)
            proxy.createOrUpdateItem(input, input.id, 'issues', proxy.tmpTable)
        }

        // --- Kudos ---
        if(metric.code != 'tr' && (tenant['coaching' + metric.name + 'Kudo']) && 
            (
                ( metric.polarity  && compareVal >= parseFloat(tenant['coaching' + metric.name + 'ThresholdKudo'])) ||
                ( !metric.polarity && compareVal <= parseFloat(tenant['coaching' + metric.name + 'ThresholdKudo']))
            )
        ){
            let input = {
                id: date + '-m-' + metric.code + '-' + staffId,
                kudoStaffId: staffId, 
                staffId: staffId,
                date: parseDateToTimezoneTimestamp(date, proxy.clientTimeZone),
                notes: 'Received ' + compareVal + ' (Created from Mentor import)',
                group: tenant.group,
                updatedAt: new Date().toISOString()
            }
            const kudoTypeId = proxy.kudoTypeList.find(item => item.option === metric.type)?.id
            if(kudoTypeId) input.kudoTypeId = kudoTypeId
            else input.kudoType = metric.type

            // createRecord(input, process.env.DA_KUDO_TABLE)
            proxy.createOrUpdateItem(input, input.id, 'kudos', proxy.tmpTable)
        }
    })
}

const createIssuesAndKudosNetradyne = async function(netradyne, proxy){
    if(!netradyne){
        return
    }
    const tenant = proxy.tenant
    const staffId = netradyne.staff.id
    
    let date = netradyne.alerts[0].timestamp
    date = date.split(' ')[0]

    // loop through alerts and compare to thresholds 
    var alertCounts = {}
    let driverStarOnly = true
    for(const alert of netradyne.alerts){
        let type = alert.alertType
        let severity = alert.alertSeverity
        if(type != "DRIVER-STAR") driverStarOnly = false
        if(!alertCounts[type]) alertCounts[type] = {}
        if(!alertCounts[type][severity]) alertCounts[type][severity] = 1
        else alertCounts[type][severity]++
    }

    for(const type of Object.keys(alertCounts)) {

        let fieldPrefix = 'coaching' + formatNetradyneType(type)
        let totalAlertCount = Object.keys(alertCounts[type]).reduce((sum,key)=>sum+parseFloat(alertCounts[type][key]||0),0);
        // --- Issues ---
        if(tenant[fieldPrefix + 'Issue'] && (totalAlertCount > parseFloat(tenant[fieldPrefix + 'ThresholdIssue'])) ){
            let input = {
                id: date + '-n-' + type + '-' + staffId,
                infractionStaffId: staffId, 
                staffId: staffId,
                date: parseDateToTimezoneTimestamp(date, proxy.clientTimeZone),
                comment: 'Received ' + totalAlertCount + ' (Created from Netradyne import)',
                group: tenant.group,
                updatedAt: new Date().toISOString()
            }
            const infractionTypeId = proxy.issueTypeList.find(item => item.option ===  'Netradyne ' + toTitleCase(type))?.id
            if(infractionTypeId) input.infractionTypeId = infractionTypeId
            else input.infractionType = 'Netradyne ' + toTitleCase(type)

            proxy.createOrUpdateItem(input, input.id, 'issues', proxy.tmpTable)
        }
        // --- Kudos ---
        else if(type == 'DRIVER-STAR' && tenant.coachingDriverStarKudo && (totalAlertCount >= parseFloat(tenant.coachingDriverStarThresholdKudo))){
            let input = {
                id: date + '-n-' + type + '-' + staffId,
                kudoStaffId: staffId, 
                staffId: staffId,
                date: parseDateToTimezoneTimestamp(date, proxy.clientTimeZone),
                notes: 'Received ' + totalAlertCount + ' (Created from Netradyne import)',
                group: tenant.group,
                updatedAt: new Date().toISOString()
            }
            const kudoTypeId = proxy.kudoTypeList.find(item => item.option === 'Netradyne ' + toTitleCase(type))?.id
            if(kudoTypeId) input.kudoTypeId = kudoTypeId
            else input.kudoType = 'Netradyne ' + toTitleCase(type)

            proxy.createOrUpdateItem(input, input.id, 'kudos', proxy.tmpTable)
        }
    }
    
    
}

const createIssuesAndKudosEOC = async function(eoc, proxy){
    if(!eoc){
        return
    }
    const tenant = proxy.tenant
    //const staffId = eoc.eocScoreStaffId // eocScoreStaffId never exists
    const staffId = eoc.staffId

    let date = eoc.date.split('T')[0]

    let metrics = [
        //{ code: 'dc', polarity: 1, name: 'DailyComplianceRate', fieldName: 'average', type: 'Average Daily Compliance'}
        { code: 'dc', polarity: 1, name: 'DailyComplianceRate', fieldName: 'average', type: 'EOC Daily Compliance Rate'}
    ]
    
    if(typeof(eoc['average'])==='undefined' && typeof(eoc['Current Average'] !== 'undefined')){
        eoc['average'] = eoc[date]
    }
    
    metrics.forEach(metric => {

        let value = 100 * (parseFloat(eoc[metric.fieldName]))

        // --- Issues ---
        if((tenant['coaching' + metric.name + 'Issue']) && 
            (
                ( metric.polarity  && value < parseFloat(tenant['coaching' + metric.name + 'ThresholdIssue'])) ||
                ( !metric.polarity && value > parseFloat(tenant['coaching' + metric.name + 'ThresholdIssue']))
            )
        ){
            let input = {
                id: date + '-e-' + metric.code + '-' + staffId,
                infractionStaffId: staffId, 
                staffId: staffId,
                date: parseDateToTimezoneTimestamp(date, proxy.clientTimeZone),
                comment: 'Received ' + value + '% (Created from EOC import)',
                group: tenant.group,
                updatedAt: new Date().toISOString()
            }
            const infractionTypeId = proxy.issueTypeList.find(item => item.option ===  metric.type)?.id
            if(infractionTypeId) input.infractionTypeId = infractionTypeId
            else input.infractionType = metric.type

            proxy.createOrUpdateItem(input, input.id, 'issues', proxy.tmpTable)
        }

        // --- Kudos ---
        if((tenant['coaching' + metric.name + 'Kudo']) && 
            (
                ( metric.polarity  && value >= parseFloat(tenant['coaching' + metric.name + 'ThresholdKudo'])) ||
                ( !metric.polarity && value <= parseFloat(tenant['coaching' + metric.name + 'ThresholdKudo']))
            )
        ){
            let input = {
                id: date + '-e-' + metric.code + '-' + staffId,
                kudoStaffId: staffId, 
                staffId: staffId,
                date: parseDateToTimezoneTimestamp(date, proxy.clientTimeZone),
                notes: 'Received ' + value + '% (Created from EOC import)',
                group: tenant.group,
                updatedAt: new Date().toISOString()
            }
            const kudoTypeId = proxy.kudoTypeList.find(item => item.option === metric.type)?.id
            if(kudoTypeId) input.kudoTypeId = kudoTypeId
            else input.kudoType = metric.type

            proxy.createOrUpdateItem(input, input.id, 'kudos', proxy.tmpTable)
        }
    })
}


module.exports = { createIssuesAndKudosMentor, createIssuesAndKudosNetradyne, createIssuesAndKudosEOC }