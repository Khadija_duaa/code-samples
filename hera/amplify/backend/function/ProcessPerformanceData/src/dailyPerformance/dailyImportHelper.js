

const { createPendingMessagesMentor ,createPendingMessagesNetradyne, createPendingMessagesEOC } = require('./dailyCreatePendingMessage')
const { createIssuesAndKudosMentor, createIssuesAndKudosNetradyne, createIssuesAndKudosEOC } = require('./dailyCreateIssuesAndKudos')
const { toTitleCase, AWSDateToDate, beforeToday }  = require('../util')
const { updateRecord, createDynamoRecord, buildUpdateParams, getDynamoRecord, queryDynamoRecords } = require('../dynamodbHelper.js')
const { getTableData } = require('./dailyCompileHelper.js')
const { getKudosOrIssueRecords } = require('../kudosOrIssuesHelper.js')

/*CloudWatch Loggers and Metricts */
const { setCloudWatchMetrics } = require('../cloudWatchFunctionHelper.js')
const { CWMetrics } = require('../cloudWatchMetricHelper.js')

const { executeMutation } = require('../appSync.js')
const { getLastDayOfWeekFromColumnNames, getDaysWeekFromColumnName, getOnlyDate } = require('./utils.js')
const { getTableName } = require('../util.js')
const {
    createKudo,
    updateKudo,
    deleteKudo,
    createInfraction,
    updateInfraction,
    deleteInfraction 
} = require('../graphql.js');
const { Tables } = require('../tables');


const getRecordsEOCScore = async function(group, level, date) {
    let recordsEOCScore = []
    try {
        let lastEvaluatedKey = null
        do {
            let params = {
                TableName: getTableName(Tables.EOC_SCORE_TABLE),
                IndexName: 'byGroupLevelAndDate',
                ExpressionAttributeValues: {
                    ':group': group,
                    ':levelDate': `${level}#${date}`,
                },
                ExpressionAttributeNames: { 
                    '#group': 'group',
                    '#levelDate': 'level#date',
                    '#date': 'date',
                },
                KeyConditionExpression: '#group = :group AND begins_with(#levelDate, :levelDate)',
                ProjectionExpression: "id, #group, #date",
                ExclusiveStartKey: lastEvaluatedKey
            }
            const result = await queryDynamoRecords(params)
            lastEvaluatedKey = result.LastEvaluatedKey
            recordsEOCScore = recordsEOCScore.concat(result.Items)
        } while (lastEvaluatedKey)
    } catch (error) {
        console.log('Error in function accidentsByGroupAndAccidentDate', error)
    }finally{
        return recordsEOCScore
    }
}

const extractDateFromFileName = ( fileName ) => {
    const regex = /(\d{8})/;
    const match = fileName.match(regex);
    let date = null;
    let dateFormated = '';
    if (match && match.length > 1) {
        date = match[1];
    } else {
        console.log('No se encontró una fecha válida.');
    }

    if (date) {
        let year = date.slice(0, 4);
        let month = date.slice(4, 6);
        let day = date.slice(6)
        dateFormated = `${year}-${month}-${day}`
    }

    return dateFormated;
} 

const orderDSPBYNameFile = ( data ) => {
    const dspOrdered = data.map( record => {
        return {
            ...record,
            dateFormated: extractDateFromFileName( record.fileName )
        }
    }).sort(( a, b ) => new Date( a.dateFormated ) - new Date( b.dateFormated ));
    return dspOrdered;
}

function percentage(float){
    // float to percentage, E.g.: 0.25 -> '25%'
    if(float){
        return (Math.floor(float*10000)/100) + '%'
    }
    return undefined
}

function mentorDateFormatted(date){
    const year = date.getFullYear()
    const month = String(date.getMonth() + 1)
    const day = String(date.getDate())
    return `${month.padStart(2, '0')}/${day.padStart(2, '0')}/${year}`
}

function groupByTime(data){
    if(!data.length){
        return []
    }
    const groupedData = data.reduce((obj, row)=>{
        const time = row.date.getTime()
        !obj[time] && ( obj[time] = [] )
        obj[time].push(row)
        return obj
    }, {})
    return Object.entries(groupedData)
}

function groupByTimeAndStaff(type, groupedByTime, staffId, map){
    return groupedByTime.forEach(([time, record])=>{
        !map[time] && ( map[time] = {} )
        !map[time][staffId] && ( map[time][staffId] = {} )
        map[time][staffId][type] = record
    })
}

const importProcess = async function(storage, proxy){
    let importFiles = {mentor: false, netradyne: false, eoc: false}

    console.log({proxy});

    const { daysToImport  } = proxy;

    if (daysToImport.length > 0) {
        await importDspEOCForDaysNotImported(storage, proxy);
    } else {
        return;
    }


    const tabledata = getTableData(storage)

    const matched = [...tabledata[0], ...tabledata[1]]

    const groupedByTimeAndStaffId = matched.reduce((map, row)=>{
        const staff = row.staff
        for (const type of ['mentor','netradyne','eoc']) {
            const data = row[type+'Data']
            const byTime = groupByTime(data)
            groupByTimeAndStaff(type, byTime, staff.id, map)
        }
        proxy.associateLookupMap[staff.id] = staff
        return map
    }, {})
    
    const timeGroup = Object.entries(groupedByTimeAndStaffId)
    await Promise.all(timeGroup.map(async ([time, group])=>{
        const staffIdGroup = Object.entries(group)
        await Promise.all(staffIdGroup.map(async ([staffId, record])=>{
            const staff = proxy.associateLookupMap[staffId]
            let importId = time + '-'
            if(record.mentor && record.mentor.length){
                importId += 'm'
                importFiles.mentor = true
            }
            if(record.netradyne && record.netradyne.length){
                importId += 'n'
                importFiles.netradyne = true
            }
            if(record.eoc && record.eoc.length){
                importId += 'e'
                importFiles.eoc = true
            }
            await importStaffMentor(importId, staff, record.mentor, proxy)
            await importStaffNetradyne(importId, staff, record.netradyne, proxy)
            if (daysToImport.length > 0) {
                await importStaffEOCDaily(importId, staff, record.eoc, proxy)
            }
        }))
    }))
    
    let extractDataForStaff = {}
    if (proxy.tmpTable.issues){
        for (let key in proxy.tmpTable.issues) {
          if (proxy.tmpTable.issues.hasOwnProperty(key)) {
           
            let issue = proxy.tmpTable.issues[key];
          
            const { staffId, date } = issue;
            const entryKey = `${staffId}-${date}`;
            if (!extractDataForStaff[entryKey]) {
              extractDataForStaff[entryKey] = { staffId, date };
            }
          }
        }
    }
    if(proxy.tmpTable.kudos){
        for (let key in proxy.tmpTable.kudos) {
          if (proxy.tmpTable.kudos.hasOwnProperty(key)) {
           
            let kudo = proxy.tmpTable.kudos[key];
            const { staffId, date } = kudo;
            const entryKey = `${staffId}-${date}`;
            if (!extractDataForStaff[entryKey]) {
              extractDataForStaff[entryKey] = { staffId, date };
            }
            
          }
        }
    }
    const staffIdAndDate = Object.values(extractDataForStaff)
    
    let oldIssuesAndKudos = {
        issues:{},
        kudos:{}
    }
    for(let i of staffIdAndDate){
        //get Kudos and Issues
        const kudos = await getKudosOrIssueRecords(i.staffId, i.date, getTableName(Tables.DA_KUDO_TABLE))
        //filter for mentor, netradyne and eoc
        kudos.filter(item => item.id.includes('-m-') && importFiles.mentor ||  item.id.includes('-n-') && importFiles.netradyne || item.id.includes('-e-') && importFiles.eoc).forEach(kudo => {
            oldIssuesAndKudos.kudos[kudo.id] =  kudo;
        });
          
        const issues = await getKudosOrIssueRecords(i.staffId, i.date, getTableName(Tables.DA_ISSUE_TABLE))
          
        issues.filter(item => item.id.includes('-m-') && importFiles.mentor ||  item.id.includes('-n-') && importFiles.netradyne || item.id.includes('-e-') && importFiles.eoc).forEach(issue => {
            oldIssuesAndKudos.issues[issue.id] = issue;
        });
        
    }
    
    for(const [key, values] of Object.entries(oldIssuesAndKudos)){
        for(const [keyId, item] of Object.entries(values)){
            if(!proxy.tmpTable[key].hasOwnProperty(keyId)){
                let status, type
              	if(key==='kudos'){
              	    type = 'kudo'
              	    status = await deleteIssueOrKudoRecord(item, keyId, type, getTableName(Tables.DA_KUDO_TABLE), proxy.user, proxy.tenant)
              	}
             	if(key==='issues'){
             	    type = 'issue'
             	    status = await deleteIssueOrKudoRecord(item, keyId, type, getTableName(Tables.DA_ISSUE_TABLE), proxy.user, proxy.tenant)
             	}
             	status && (proxy.performanceCounts[type][status]++)
            }
        }
    }
    
    for (const [key, values] of Object.entries(proxy.tmpTable)) {
        for (const [id, item] of Object.entries(values)) {
            let status, type
            if(key === 'associates'){
                await createRecord(item, id, getTableName(Tables.STAFF_TABLE))
            }else if(key === 'pendingMessages'){
                type = 'message'
                status = await createRecord(item, id, getTableName(Tables.PENDING_MESSAGE_TABLE))
            }else if(key === 'issues'){
                type = 'issue'
                status = await createUpdateIssueOrKudoRecord(item, id, type, getTableName(Tables.DA_ISSUE_TABLE))
            }else if(key === 'kudos'){
                type = 'kudo'
                status = await createUpdateIssueOrKudoRecord(item, id, type,getTableName(Tables.DA_KUDO_TABLE))
            }
            status && (proxy.performanceCounts[type][status]++)
        }
    }

}

const importDspEOCForDaysNotImported = async function(storage, proxy){
    proxy.createOrUpdateItem = createOrUpdateItem;
    const dspEocData = storage.dspLevel.eocData;
    const dspEocDataOrdered = orderDSPBYNameFile( dspEocData );
    const { daysToImport, tenant: { group } } = proxy;
    for (const [index,eoc] of dspEocDataOrdered.entries()) {
        const { dsp_shortcode, 'Current Average': current_average, textractJobId, ...restOfTheObject } = eoc;
        const { date } = restOfTheObject;
        const { lastDayOfWeek } = getLastDayOfWeekFromColumnNames( restOfTheObject );
        for (const day of daysToImport[ index ]) {
            
            if (!day || day > lastDayOfWeek || day > getOnlyDate(date)) continue;
            const existingRecord = await getRecordsEOCScore( group, 'dsp', day);
            if (existingRecord.length) {
                continue;
            }
            const average = percentage(eoc[day]);
            const eocItem = {
                id: proxy.tenant.id+'-'+new Date( day ).getTime(),
                group: proxy.tenant.group,
                level: "dsp",
                average,
                textractJobId,
                updatedAt: new Date().toISOString()
            }
            
            if (day === lastDayOfWeek) {
                eocItem.averageDailyCompliance = percentage(current_average);
            }

            try{
                // //If fails, create record
                eocItem.date = new Date(day).toISOString();
                eocItem['level#date'] = eocItem.level+"#"+eocItem.date;
                eocItem.createdAt = eocItem.updatedAt;
                const createParams = {
                    TableName: getTableName(Tables.EOC_SCORE_TABLE),
                    Item: eocItem
                }
                await createDynamoRecord(createParams);
                proxy.importCounts.eoc.created++;
            }catch(e){
                console.log(e);
                proxy.importCounts.eoc.errors++;
            }


        }

    }

}

const importDspEOC = async function(storage, proxy){
    proxy.createOrUpdateItem = createOrUpdateItem
    const dspEocData = storage.dspLevel.eocData
    for (const eoc of dspEocData) {
        const {dsp_shortcode, 'Current Average': current_average, textractJobId, ...restOfTheObject} = eoc
        let {lastDayOfWeek, theLastDate} = getLastDayOfWeekFromColumnNames(restOfTheObject)
        const average = percentage(eoc[lastDayOfWeek])

        const eocItem = {
            id: proxy.tenant.id+'-'+theLastDate.getTime(),
            group: proxy.tenant.group,
            level: "dsp",
            average,
            averageDailyCompliance: percentage(current_average),
            textractJobId,
            updatedAt: new Date().toISOString()
        }

        try{
            // create eoc record
            eocItem.date = theLastDate.toISOString()
            eocItem['level#date'] = eocItem.level+"#"+eocItem.date
            const { id, ...item } = eocItem
            const updateParams = buildUpdateParams(getTableName(Tables.EOC_SCORE_TABLE), item, id)
            await updateRecord(updateParams, eocItem)
        }catch(e){
            try{
                // //If fails, create record
                eocItem.createdAt = eocItem.updatedAt
                const createParams = {
                    TableName: getTableName(Tables.EOC_SCORE_TABLE),
                    Item: eocItem
                }
                await createDynamoRecord(createParams)
                proxy.importCounts.eoc.created++
            }catch(e){
                console.log(e)
                proxy.importCounts.eoc.errors++
            }
        }
    }
} 

const importStaffMentor = async function(importId, staff, mentorData, proxy){
    if(!mentorData || !mentorData.length){
        return
    }
    const {id: staffId, importName, matchStatus} = staff
    for (const mentor of mentorData) {
        const date = mentor.date
        const mentorItem = {
            ...mentor,
            id: mentorDateFormatted(mentor.date) + staffId,
            staffMentorStaffId: staffId,
            matched: true,
            name: importName,
            messageHasBeenSent: false,
            group: proxy.tenant.group,
            updatedAt: new Date().toISOString()
        }

        delete mentorItem.driverName

        // create mentor record
        try{
            // //Update staff mentor record
            mentorItem.date = date.toISOString()
            const {id, ...item} = mentorItem
            const updateParams = buildUpdateParams(getTableName(Tables.STAFF_MENTOR_TABLE), item, id)
            const fieldsThatShouldNotBeCompared = ['updatedAt','textractJobId']
            const wasUpdated = await updateRecord(updateParams, item, fieldsThatShouldNotBeCompared)
            if(wasUpdated){
                proxy.importCounts.mentor.updated++
            }

        }catch(e){
            try{
                // //If fails, create record
                mentorItem.createdAt = mentorItem.updatedAt
                const createParams = {
                    TableName: getTableName(Tables.STAFF_MENTOR_TABLE),
                    Item: mentorItem
                }
                await createDynamoRecord(createParams)
                proxy.importCounts.mentor.created++
            }catch(e){
                console.log(e)
                proxy.importCounts.mentor.errors++
            }
        }

        if( !matchStatus.startsWith('Inactive') ) {
            // create or update pending message record if yesterday's data
            mentorItem.date = date
            if(mentor.date.getTime() >= beforeToday(proxy.clientTimeZone).getTime()){
                await createPendingMessagesMentor(importId, mentorItem, staff, proxy)
            }
            // create da issues and kudos
            await createIssuesAndKudosMentor(mentorItem, proxy)
        }
    }
}

const importStaffNetradyne = async function(importId, staff, netradyneData, proxy){
    if(!netradyneData || !netradyneData.length){
        return
    }
    const {id: staffId, importName, matchStatus} = staff
    for (const netradyne of netradyneData) {
        for (const alert of netradyne.alerts) {
            const netradyneAlertItem = {
                ...alert,
                date: netradyne.date.toISOString(),
                id: alert.alertId + "-" + staffId,
                driverId: netradyne.driverId,
                group: proxy.tenant.group,
                staffId: staffId,
                staffNetradyneAlertStaffId: staffId,
                matched: true,
                matchedS: 'true',
                driverName: importName,
                alertType: toTitleCase(alert.alertType),
                alertSeverity: toTitleCase(alert.alertSeverity),
                alertVideoStatus: toTitleCase(alert.alertVideoStatus),
                sortKey: `${staffId}#${toTitleCase(alert.alertType)}`,
                textractJobId: netradyne.textractJobId,
                updatedAt: new Date().toISOString()
            }
            // create netradyne record
            try{
                // //Create new staff netradyne record
                const {id, ...item} = netradyneAlertItem
                const updateParams = buildUpdateParams(getTableName(Tables.STAFF_NETRADYNE_ALERT_TABLE), item, id)
                const fieldsThatShouldNotBeCompared = ['updatedAt','textractJobId']
                const wasUpdated = await updateRecord(updateParams, item, fieldsThatShouldNotBeCompared)
                if(wasUpdated){
                    proxy.importCounts.netradyne.updated++
                }
            }catch(e){
                try{
                    // //If fails, update record
                    netradyneAlertItem.createdAt = netradyneAlertItem.updatedAt
                    const createParams = {
                        TableName: getTableName(Tables.STAFF_NETRADYNE_ALERT_TABLE),
                        Item: netradyneAlertItem
                    }
                    await createDynamoRecord(createParams)
                    proxy.importCounts.netradyne.created++
                }catch(e){
                    console.log(e)
                    proxy.importCounts.netradyne.errors++
                }
            }
        }

        if( !matchStatus.startsWith('Inactive') ) {
            netradyne.staff = staff
            // create or update pending message record if yesterday's data for Netradyne
            if(netradyne.date.getTime() >= beforeToday(proxy.clientTimeZone).getTime()){
                await createPendingMessagesNetradyne(importId, netradyne, staff, proxy)
            }
            //create or update da issues and kudos
            await createIssuesAndKudosNetradyne(netradyne, proxy)
        }

        createOrUpdateItem({netradyneDriverId: netradyne.driverId}, staff.id, 'associates', proxy.tmpTable)
    }
}

const importStaffEOCDaily = async function(importId, staff, eocData, proxy){
    if(!eocData || !eocData.length){
        return
    }
    const { daysToImport, tenant: { group } } = proxy;
    const {id: staffId, importName, matchStatus} = staff;

    const datesToImport = [ ...new Set( daysToImport.flat() ) ];
    
    if ( datesToImport.length == 0 ) {
        return;
    }

    let latestDate
    for (const eoc of eocData) {
        const { transporter_id, 'Current Average': current_average, date, textractJobId, ...restOfTheObject } = eoc
        const { lastDayOfWeek,  theLastDate } = getLastDayOfWeekFromColumnNames( restOfTheObject );
        const validDate = getOnlyDate(date)

        for (const date of datesToImport) {
            latestDate = date
            let itemsToImport = {}
            if (!eoc.hasOwnProperty( date )) continue;
            if (!date || date > lastDayOfWeek || date > validDate) continue;
            if ( date === lastDayOfWeek ) {
                itemsToImport.averageDailyCompliance = percentage( current_average )
            }

            itemsToImport.id = `${new Date( date ).getTime()}-${staffId}`;
            itemsToImport.eocScoreStaffId = staffId;
            itemsToImport.date = new Date(date).toISOString();
            itemsToImport.group = group;
            itemsToImport.level = "associate";
            itemsToImport.average = percentage( eoc[ date ] );
            itemsToImport.textractJobId = textractJobId;
            itemsToImport.updatedAt = new Date().toISOString();
            try {
                itemsToImport['level#date'] = `${itemsToImport.level}#${itemsToImport.date}`;
                itemsToImport.createdAt = itemsToImport.updatedAt;
                const createParams = {
                    TableName: getTableName(Tables.EOC_SCORE_TABLE),
                    Item: itemsToImport
                }
                await createDynamoRecord(createParams)
                proxy.importCounts.eoc.created++
            }catch(e) {
                console.log(e)
                proxy.importCounts.eoc.errors++
            }
        }

        // eoc.date = `${eoc.date}`; 
        // Above date was previously a full new Date() string 
        // instead of an ISO string
        eoc.date = new Date(eoc.date).toISOString()
        eoc.dateToCheck = (latestDate === validDate) ? latestDate : validDate
        eoc.staffId = staffId
        
        if( !matchStatus.startsWith('Inactive') ) {
            // create or update pending message record if yesterday's data
            if(theLastDate.getTime() >= beforeToday(proxy.clientTimeZone, 2).getTime()){
                await createPendingMessagesEOC(importId, eoc, staff, proxy)
            }

            // create da issues and kudos
            await createIssuesAndKudosEOC(eoc, proxy);
        }
    }
}

const importStaffEOC = async function(importId, staff, eocData, proxy){
    if(!eocData || !eocData.length){
        return
    }
    const tenant = proxy.tenant
    const {id: staffId, importName, matchStatus} = staff
    for (const eoc of eocData) {
        const {transporter_id, 'Current Average': current_average, date, textractJobId, ...restOfTheObject} = eoc

        let {lastDayOfWeek, theLastDate} = getLastDayOfWeekFromColumnNames(restOfTheObject)
        const datesToImport = getDaysWeekFromColumnName(restOfTheObject);
        
        const itemsToImport = datesToImport.map( date => {
            const average = percentage( eoc[ date ] );
            return {
                id: `${new Date( date ).getTime()}-${staffId}`,
                eocScoreStaffId: staffId,
                date: new Date(date).toISOString(),
                group: proxy.tenant.group,
                level: "associate",
                average,
                averageDailyCompliance: percentage( current_average ),
                textractJobId,
                updatedAt: new Date().toISOString()
            }
        })

        // create eoc record
        for (let index = 0; index < itemsToImport.length; index++) {
            const eocItem = itemsToImport[index];
            try{
                eocItem['level#date'] = `${eocItem.level}#${eocItem.date}`;
                const {id, ...item} = eocItem;
                const updateParams = buildUpdateParams(getTableName(Tables.EOC_SCORE_TABLE), item, id)
                const fieldsThatShouldNotBeCompared = ['updatedAt','textractJobId']
                const wasUpdated = await updateRecord(updateParams, item, fieldsThatShouldNotBeCompared)
                if(wasUpdated){
                    proxy.importCounts.eoc.updated++
                }
    
            }catch(e){
                try{
                    // //If fails, create record
                    eocItem.createdAt = eocItem.updatedAt
                    const createParams = {
                        TableName: getTableName(Tables.EOC_SCORE_TABLE),
                        Item: eocItem
                    }
                    await createDynamoRecord(createParams)
                    proxy.importCounts.eoc.created++
                }catch(e){
                    console.log(e)
                    proxy.importCounts.eoc.errors++
                }
            }
            if( !matchStatus.startsWith('Inactive') ) {
                // create or update pending message record if yesterday's data
                if(theLastDate.getTime() >= beforeToday(proxy.clientTimeZone, 2).getTime()){
                    await createPendingMessagesEOC(importId, eocItem, staff, proxy)
                }
        
                // create da issues and kudos
                await createIssuesAndKudosEOC(eocItem, proxy)
            }
        }


    }
}

const createOrUpdateItem = function (item, id, key, tmpTable){
    if(tmpTable[key][id]){
        tmpTable[key][id] = {...tmpTable[key][id], ...item}
    }else{
        tmpTable[key][id] = item
    }
}

async function createRecord(input, id, table){
    !input.updatedAt && (input.updatedAt = new Date().toISOString())
    try{
        const updateParams = buildUpdateParams(table, input, id)
        const wasUpdated = await updateRecord(updateParams, input)
        if(wasUpdated){
            return 'updated'
        }
    }
    catch(e){
        try{
            input.id = id
            input.createdAt = input.updatedAt
            const createParams = {
                TableName: table,
                Item: input
            }
            await createDynamoRecord(createParams)
            return 'created'
        }catch(e){
            console.log(e)
            return 'errors'
        }
    }
}




/*******************************************************************************
 *  DA Kudo and Issues Create and Update
 ******************************************************************************/
const createUpdateIssueOrKudoRecord = async function(item, id, type, table){
    try{
        let params = {
            Key: {
                id: id
            },
            TableName: table
        }
        const getResult  = await getDynamoRecord(params)
     
        const oldValues = getResult.Item || {}
        if (Object.keys(oldValues).length != 0) {
            //exist result
            const fieldsThatShouldNotBeCompared = ['updatedAt']
            const fields = Object.keys(item).filter(field => !fieldsThatShouldNotBeCompared.includes(field))
            const needUpdated = fields.some(field => oldValues[field] !== item[field])
           
            if(needUpdated){
                //update
                //no need to "updatedAt" using GraphQL
                delete item.updatedAt
                const updateItem = {...item, id: id}
                const updateMutations = {
                    'issue': updateInfraction,
                    'kudo': updateKudo
                }
                const selectedMutation = updateMutations[type]
                //update issues or kudos using GraphQL to trigger subscription
                await executeMutation(selectedMutation, {input: updateItem})
                return 'updated'
            }
        }else{
            // create new record
            item.id = id
            const createMutations = {
                'issue': createInfraction,
                'kudo': createKudo
            }
            const selectedMutation = createMutations[type]
            
            //no need to "updatedAt" using GraphQL
            delete item.updatedAt
            const input = {...item}
            //create issues or kudos using GraphQL to trigger subscription
            await executeMutation(selectedMutation, {input})
            return 'created'
        }
    
    }catch(e){
        console.log('Error in function createUpdateIssueOrKudoRecord', e)
        return 'errors'
    }
}


/*******************************************************************************
 *  delete Kudo or Issues for dailys files
 ******************************************************************************/

async function deleteIssueOrKudoRecord(item, id, type, table, user, tenant){
    
    try{
        let params = {
            Key: {
                id: id
            },
            TableName: table
        }
        const getResult = await getDynamoRecord(params)
        const values = getResult.Item || {}
        if (Object.keys(values).length != 0) {
            //exist result
            if(values.infractionCounselingId){
                //not delete, count as error
                const da = await getAssociate(item.staffId)
                let metrics = {
                    functionName: 'deleteIssueOrKudoRecord',
                    codeText: 'ERROR_004',
                    codeNumber: '004.001',
                    tenant: { id:tenant.id, companyName: tenant.companyName, group: tenant.group},
                    err: 'Issue should not have previously been created, but cannot be automatically deleted because it has been linked to a Counseling.',
                    issue: { id: id, counselingId: values.infractionCounselingId },
                    user: { id: user.id, userName: user.userName },
                    associate: { id: da.id, name: `${da.firstName} ${da.lastName}`}
                }
                const cwm = new CWMetrics(process.env.METRIC_GROUP_NAME)
                setCloudWatchMetrics(metrics, cwm)
                await cwm.sendToCloudWatch()
                return 'errors'
            }else{
                //delete
                const deleteMutations = {
                    'issue': deleteInfraction,
                    'kudo': deleteKudo
                }
                const selectedMutation = deleteMutations[type]
                //execute mutations for subscription of issues and kudos
                await executeMutation(selectedMutation, {input:{id: id}})
                return 'deleted'
            }
        }
    }catch(e){
        console.log('Error in function deleteIssueOrKudoRecord', e)
        return 'errors'
    }
}

async function getAssociate(id){
    let associate = {
        id: '',
        firstName: '',
        lastName: ''
    }
    try {
        let params = {
            Key: {
                id: id
            },
            ProjectionExpression: "id, firstName, lastName",
            TableName: getTableName(Tables.STAFF_TABLE)
        }
        const result = await getDynamoRecord(params)
        associate = result.Item
    } catch (e) {
        console.log('Error in function getAssociate', e)
    } finally{
        return associate
    }
}

module.exports = { importProcess, createRecord, createOrUpdateItem }