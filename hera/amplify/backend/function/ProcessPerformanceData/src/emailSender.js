//HERA-1049

// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
// Load S3 Storage
var S3 = new AWS.S3();

// Global variables
var pinpoint = new AWS.Pinpoint({region: process.env.PIN_POINT_REGION}); 
var projectId = process.env.PROJECT_ID;
var senderEmail = process.env.SENDER_EMAIL;

const {
  executeQuery,
} = require('./appSync');

const {
  getTenant
} = require('./graphql.js');

const { isTestEnv, isValidTestEmail } = require('./testEnvHelpers.js');

// /*******************************************************************************
//  * Create body to send an email using AWS
//  ******************************************************************************/
async function sendErrorEmail(record) {

    if (record.tenant_id) {

        //Generate a link from S3
        var params = {
          Bucket: process.env.STORAGE_HERA_BUCKETNAME,
          Key: 'public/' + record.key,
          Expires: 432000 //Expire in 5 days
        };

        var pdfURL = S3.getSignedUrl('getObject', params)

        // Get Tenant Company Name
        let tenantName = await getTenantById(record.tenant_id)

        // list validators
        let validatorList = ''

        // Page counter validator
        if(!record.validatorPageCounter.isValid) validatorList = `<li> Page validator: 'ERROR_002_SCORECARD_VALIDATOR_007 (002.007). ${record.validatorPageCounter.message}</li>`
        
        record.validatorsFailedList.forEach(item => {
          let rules = ''
          item.rulesFailed.forEach(rule => {
            rules = rules + `<li>${rule}</li>`
          })
          item = `<li> Transporter ID: ${item.transporter_id}<br>
            Failed rules:<br>
              <ul>
                ${rules}
              </ul>
            </li>`
          validatorList = validatorList + item
        })
    
        let content = `<br>
            PDF: <a href="${pdfURL}"> Link to PDF </a> <br>
            Reason: The following validations failed: <br>
            <ul>
               ${validatorList}
            </ul>
        `
        let emailParams = {
            destinationEmail: process.env.SUPPORT_EMAIL,
            subject: `Silent error importing week ${record.week}-${record.year} Scorecard for ${tenantName.companyName}`,
            bodyText: content,
            bodyHtml: content,
        }

        if (process.env.ENV === 'phasethree') {
          emailParams.subject = `(DEV) ${emailParams.subject}`
        }
    
        await sendEmail(emailParams)
    } else {
        console.log('[sendErrorEmail] Unable to send email. No tenant related.', JSON.stringify(record))
    }
}

/*******************************************************************************
 * Send an email using aws pinpoint
 * @param(Object) record - DB Record with send info
 ******************************************************************************/
 function sendEmail(record) {
  return new Promise((resolve, reject) => {
    
    //------- ENSURE TEST ENVIRONMENT USES TEST EMAIL ------//
    if (isTestEnv()) {
      if(!!record.destinationEmail && !isValidTestEmail()){ 
        console.log('Exiting sendEmail from the test environment. Destination email does not appear to be a test address:', record)
        return reject("Error attempting to send a message with an invalid test address")
      }
    }

    //--------VERIFY WE HAVE REQUIRED DATA--------//
    if(!record.subject || !record.bodyHtml || !record.bodyText || !record.destinationEmail){
      console.log("Error: Could not send email, missing required data")
      reject("Error: Could not send email, missing required data")
    }
    
    //----------CONSTRUCT EMAIL PARAMS----------//
    var charset = "UTF-8";
    var params = {
      ApplicationId: projectId,
      MessageRequest: {
        Addresses: {
          [record.destinationEmail]:{
            ChannelType: 'EMAIL'
          }
        },
        MessageConfiguration: {
          EmailMessage: {
            FromAddress: senderEmail,
            SimpleEmail: {
              Subject: {
                Charset: charset,
                Data: record.subject
              },
              HtmlPart: {
                Charset: charset,
                Data: record.bodyHtml
              },
              TextPart: {
                Charset: charset,
                Data: record.bodyText
              }
            }
          }
        }
      }
    };
    
    //----------SEND EMAIL-------------//
    pinpoint.sendMessages(params, function(err, data) {
      // If something goes wrong, print an error message.
      if(err) {
        console.log(err.message);
        reject(err)
      } else {
        console.log(data)
        console.log(data['MessageResponse']['Result'][record.destinationEmail])
        let result = "Email sent! Message ID: " + data['MessageResponse']['Result'][record.destinationEmail]['MessageId']
        console.log(result);
        resolve(result)
      }
    });
  
  })
}

async function getTenantById(id) {
  let input = {
    id: id,
  }
  try {
    let response = await executeQuery(getTenant, input)
    return response.getTenant
  } catch (e) {
    console.log('[getTenantById]', e)
    return e
  }
}

module.exports = { sendErrorEmail };
