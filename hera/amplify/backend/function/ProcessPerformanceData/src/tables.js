class Tables {
    static COMPANY_SCORECARD_TABLE = 'CompanyScoreCard';
    static CUSTOM_LISTS_TABLE = 'CustomLists';
    static CX_FEEDBACKSUMMARY_TABLE = 'CxFeedbackSummary';
    static DA_ISSUE_TABLE = 'Infraction';
    static DA_KUDO_TABLE = 'Kudo';
    static DEVICE_TABLE = 'Device';
    static EOC_SCORE_TABLE = 'EocScore';
    static OPTIONS_CUSTOM_LISTS_TABLE = 'OptionsCustomLists';
    static PENDING_MESSAGE_TABLE = 'PendingMessage';
    static POD_QUALITYSUMMARY_TABLE = 'PodQualitySummary';
    static REPLACE_BY_ROUTE_TABLE = 'ReplaceByRoute';
    static ROUTE_TABLE = 'Route';
    static STAFF_CX_TABLE = 'StaffCxFeedback';
    static STAFF_MENTOR_TABLE = 'StaffMentor';
    static STAFF_NETRADYNE_ALERT_TABLE = 'StaffNetradyneAlert';
    static STAFF_POD_TABLE = 'PodQuality';
    static STAFF_SCORECARD_TABLE = 'StaffScoreCard';
    static STAFF_TABLE = 'Staff';
    static TENANT_TABLE = 'Tenant';
    static TEXTRACT_JOB_TABLE = 'TextractJob';
    static VALUE_LIST_ITEM_TABLE = 'ValueListItem';
    static VALUE_LIST_TABLE = 'ValueList';
    static VEHICLE_TABLE = 'Vehicle';
  }

  module.exports = { Tables };