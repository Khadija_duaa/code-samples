/* Amplify Params - DO NOT EDIT
	API_HERA_GRAPHQLAPIIDOUTPUT
	API_HERA_STAFFTABLE_ARN
	API_HERA_STAFFTABLE_NAME
	ENV
	REGION
Amplify Params - DO NOT EDIT */
exports.handler = event => {
  //eslint-disable-line
  console.log(JSON.stringify(event, null, 2));
  event.Records.forEach(record => {
    console.log(record.eventID);
    console.log(record.eventName);
    console.log('DynamoDB Record: %j', record.dynamodb);

    //Update staff record if updating/modifying record.
    if( record.eventName === "MODIFY" || record.eventName === "INSERT" ){
      var data = record.dynamodb.NewImage
      var year = data.year.S
      var week = data.week.S.padStart(2, '0')
      var yearWeek = year + week
      
      //check to see if record is related to a staff member
      var staffId = data.staffScoreCardStaffId.S
      if( staffId != undefined ){
        updateStaffWithLatestScoreCard(staffId, yearWeek)
      }
    }

  });
  return Promise.resolve('Successfully processed DynamoDB record');
};

/**
 * Determines if the yearWeek needs to be updated then updates it if needed
 * @param {String} staffId 
 * @param {Int} yearWeek 
 */
async function updateStaffWithLatestScoreCard(staffId, yearWeek){
  try{
    var staffRecord = await getDynamoRecord(staffId)
    console.log(staffRecord)
  }catch(e){
    console.log(e)
  }
}



function getDynamoRecord(id){
  return new Promise((resolve, reject) => {
      var params = {
        TableName : process.env.API_HERA_STAFFTABLE_NAME,
        Key: {
          "id": id
        }
      };
      
      var documentClient = new AWS.DynamoDB.DocumentClient();
      
      documentClient.get(params, function(err, data) {
          if (err) {
                console.error("Unable to get item. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                console.log("Get Item succeeded:", JSON.stringify(data, null, 2));
                resolve(data)
            }
      });
  })
}