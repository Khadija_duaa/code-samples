/* Amplify Params - DO NOT EDIT
	API_HERA_AUDITLOGARCHIVETABLE_ARN
	API_HERA_AUDITLOGARCHIVETABLE_NAME
	API_HERA_GRAPHQLAPIIDOUTPUT
	ENV
	REGION
Amplify Params - DO NOT EDIT */
var AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION})

/**
 * @type {import('@types/aws-lambda').APIGatewayProxyHandler}
 */
exports.handler = async event => {
  console.log(`EVENT: ${JSON.stringify(event)}`);
  
  var recordCounter = 0
  
  // Create the DynamoDB service object
  const dynamodb = new AWS.DynamoDB({apiVersion: '2012-08-10'});
  const auditLogArchiveTableName = process.env.API_HERA_AUDITLOGARCHIVETABLE_NAME

  for (const record of event.Records) {
    console.log(record.eventName);
    
    if("REMOVE" == record.eventName && "Service" == record.userIdentity.type) {
      var deletedItem = record.dynamodb.OldImage

      // Preparing Payload to insert in AuditLog Archive Table
      var params = {
        TableName: auditLogArchiveTableName,
        Item: {
          'id':  {S: deletedItem.id.S},
          'tenantID':  {S: deletedItem.tenantID.S},
          'group':  {S: deletedItem.group.S},
          'email':  {S: deletedItem.email.S},
          'userID':  {S: deletedItem.userID.S},
          'cognitoSub':  {S: deletedItem.cognitoSub.S},
          'firstName':  {S: deletedItem.firstName.S},
          'lastName':  {S: deletedItem.lastName.S},
          'tenantName':  {S: deletedItem.tenantName.S},
          'mutationName':  {S: deletedItem.mutationName.S},
          'mutationNameText':  {S: deletedItem.mutationNameText.S},
          'mutatedRecordId':  {S: deletedItem.mutatedRecordId.S},
          'mutatedData':  {S: deletedItem.mutatedData.S},
          'ipAddress':  {S: deletedItem.ipAddress.S},   
          'pageUrl':  {S: deletedItem.pageUrl.S}        
        }
      };
      try {
        await dynamodb.putItem(params).promise();
        console.log("PutItem Params: %j", params)
        recordCounter++;
      } catch(err) {
        return { error: err }
      }
    }
  }

  return Promise.resolve('Successfully Processed DynamoDB record: '+recordCounter);
};
