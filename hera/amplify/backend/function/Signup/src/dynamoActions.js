var AWS = require("aws-sdk");
var ddb = new AWS.DynamoDB.DocumentClient({apiVersion: '2012-08-10'});

function putDynamoRecord(params){
    return new Promise((resolve, reject) => {
        ddb.put(params, function(err, data) {
            if (err) {
                console.log("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
                resolve(data)
            }
        });
  })
}

function generateUUID() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

module.exports = {
  putDynamoRecord,
  generateUUID
};