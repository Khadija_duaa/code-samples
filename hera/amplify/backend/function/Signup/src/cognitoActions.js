const { CognitoIdentityServiceProvider } = require('aws-sdk');
const cognitoIdentityServiceProvider = new CognitoIdentityServiceProvider();
const userPoolId = process.env.AUTH_HERA4E362E32_USERPOOLID;

async function createGroup(groupname) {
  const params = {
    GroupName: groupname,
    UserPoolId: userPoolId
  };

  console.log(`Attempting to create ${groupname}`);

  try {
    const result = await cognitoIdentityServiceProvider.createGroup(params).promise();
    console.log(`Success creating ${groupname}`);
    return {
      message: `Success creating ${groupname}`,
    };
  } catch (err) {
    console.log(err);
    throw err;
  }
}


async function updateUserAttributes(username, userId) {
  var params = {
  UserAttributes: [ 
    {
        Name: 'custom:user_id', 
        Value: userId
    }
  ],
  UserPoolId: userPoolId,
  Username: username, 

};

  console.log(`Attempting to update ${username}`);

  try {
    const result = await cognitoIdentityServiceProvider.adminUpdateUserAttributes(params).promise();
    console.log(`Success updating ${username}`);
    return {
      message: `Success updating ${username}`,
    };
  } catch (err) {
    console.log(err);
    throw err;
  }
}


async function addUserToGroup(username, groupname) {
  const params = {
    GroupName: groupname,
    UserPoolId: userPoolId,
    Username: username,
  };

  console.log(`Attempting to add ${username} to ${groupname}`);

  try {
    const result = await cognitoIdentityServiceProvider.adminAddUserToGroup(params).promise();
    console.log(`Success adding ${username} to ${groupname}`);
    return {
      message: `Success adding ${username} to ${groupname}`,
    };
  } catch (err) {
    console.log(err);
    throw err;
  }
}


async function getUser(username) {
  const params = {
    Username: username,
    UserPoolId: userPoolId
  };

  console.log(`Attempting to get ${username}`);

  try {
    const result = await cognitoIdentityServiceProvider.adminGetUser(params).promise();
    console.log({user: result})
    console.log(`Success getting ${username}`);
    return result;
  } catch (err) {
    console.log(err);
    throw err;
  }
}


module.exports = {
  createGroup,
  updateUserAttributes,
  addUserToGroup,
  getUser
};