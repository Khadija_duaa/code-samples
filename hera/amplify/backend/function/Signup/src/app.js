/* Amplify Params - DO NOT EDIT
	API_HERA_GRAPHQLAPIIDOUTPUT
	API_HERA_TENANTTABLE_ARN
	API_HERA_TENANTTABLE_NAME
	API_HERA_USERTABLE_ARN
	API_HERA_USERTABLE_NAME
	AUTH_HERA4E362E32_USERPOOLID
	ENV
	REGION
Amplify Params - DO NOT EDIT *//*
Copyright 2017 - 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at
    http://aws.amazon.com/apache2.0/
or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
*/




var express = require('express')
var bodyParser = require('body-parser')
var awsServerlessExpressMiddleware = require('aws-serverless-express/middleware')
var generatGroupName = require('./generateGroupName')
const {
  createGroup,
  updateUserAttributes,
  addUserToGroup,
  getUser
} = require('./cognitoActions');

const {
  putDynamoRecord,
  generateUUID
} = require('./dynamoActions');

// declare a new express app
var app = express()
app.use(bodyParser.json())
app.use(awsServerlessExpressMiddleware.eventContext())

// Enable CORS for all methods
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
});


/****************************
* SIGNUP USER *
****************************/

app.post('/signup', async function(req, res) {
  console.log(req.body)
  
  //--------GET COGNITO USER ID------------
  var userData = await getUser(req.body.email)
  var cognitoUserId = userData.Username
  console.log(cognitoUserId)
  
  
  //---------DETERMINE GROUP NAME-------
  let companyName = req.body.companyName
  let groupName = generatGroupName(companyName)
  console.log({groupName})
  
  
  //-----------CREATE COGNITO GROUP-----------
  var createGroupResults = await createGroup(groupName)
  console.log({createGroupResults})
  
  
  //---------CREATE TENANT----------
  let tenantId = generateUUID();
  let newTenant = {
    TableName: process.env.API_HERA_TENANTTABLE_NAME,
    Item: {
      id: tenantId,
      __typename: 'Tenant',
      companyName: req.body.companyName,
      group: groupName,
      coachingFicoThreshold: '750',
      coachingDcrThreshold: '98.66',
      coachingDarThreshold: '70',
      coachingSwcPodThreshold: '97',
      coachingSwcCcThreshold: '100',
      coachingSwcScThreshold: '99.9',
      coachingSwcAdThreshold: '0',
      coachingPositiveFeedbackThreshold: '100',
      coachingDailyFicoThreshold: '750',
      createdAt: new Date().toISOString(),
      updatedAt: new Date().toISOString()
    }
  };
  await putDynamoRecord(newTenant)
  
  
  //---------CREATE USER----------
  let userId = generateUUID();
  let newUser = {
    TableName: process.env.API_HERA_USERTABLE_NAME,
    Item: {
      id: userId,
      cognitoSub: cognitoUserId,
      userTenantId: tenantId,
      __typename: 'User',
      group: [groupName, 'admin'],
      role: 'admin',
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      phone: req.body.phone,
      email: req.body.email,
      createdAt: new Date().toISOString(),
      updatedAt: new Date().toISOString()
    }
  };
  await putDynamoRecord(newUser)
  
  
  //----------UPDATE COGNITO USER WITH DB ID----------------
  var cognitoUser = await updateUserAttributes(req.body.email, userId)
  console.log({cognitoUser})
  
  
  //----------ADD USER TO GROUP------------
  await addUserToGroup(req.body.email, 'admin')
  await addUserToGroup(req.body.email, groupName )
  
  
  //----------RETURN COGNITO, USER AND GROUP INFO-----------
  let response = {
    cognitoUser: userData,
    user: newUser.Item,
    tenant: newTenant.Item
  }
  
  res.json(response)
});



app.listen(3000, function() {
    console.log("App started")
});

// Export the app object. When executing the application local this does nothing. However,
// to port it to AWS Lambda we will create a wrapper around that will load the app from
// this file
module.exports = app
