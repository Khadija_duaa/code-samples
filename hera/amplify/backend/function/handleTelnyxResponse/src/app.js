/* Amplify Params - DO NOT EDIT
	API_HERA_GRAPHQLAPIENDPOINTOUTPUT
	API_HERA_GRAPHQLAPIIDOUTPUT
	ENV
	REGION
Amplify Params - DO NOT EDIT */

/*
Copyright 2017 - 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at
    http://aws.amazon.com/apache2.0/
or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
*/

var express = require('express')
var bodyParser = require('body-parser')
const { getSecretValue } = require('/opt/nodejs/ssm')
var awsServerlessExpressMiddleware = require('aws-serverless-express/middleware')

const axios = require('axios');
const COUNTRIES_CODES_VALID = ['US', 'PR', 'VI'];

async function initInstance(){
    console.log("Initializing Telnyx Instance.");
    const accessToken = await getSecretValue(process.env.TELNYX_TOKEN);
    console.log("Retrieved access token from parameter store.");
    const instance = await axios.create({
          baseURL: 'https://api.telnyx.com/v2',
          headers: {
            Authorization: `Bearer ${accessToken}`,
            'Content-Type': 'application/json',
          },
    });
    return instance;
}


// declare a new express app
var app = express()
app.use(bodyParser.json())
app.use(awsServerlessExpressMiddleware.eventContext())

// Enable CORS for all methods
app.use(function(req, res, next) {

  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "*")
  next()
});

/**********************
 * Example get method *
 **********************/

app.get('/validator/:phone', async function(req, res) {
  
  const instance = await initInstance();
 
  try {
    const phone_number = req.params.phone;
    console.log('phone number from lambda: ' + phone_number);
    const  { data }  =  await instance.get(`number_lookup/${ phone_number }`);
    const { data: phone } = data
    if ( !COUNTRIES_CODES_VALID.includes( phone.country_code ) ) return res.send(false);
    return res.send(true)
  }
  catch (e){
      return res.send(false);
  }
});

/**********************
 * Example post method *
 **********************/

app.post('/validator/telnyx', async function(req, res) {
  let result = []
  const records = req.body.records
  try {
      result = await validateRecordTelnyx(records)
      return res.json({success: 'get call succeed!', result });
  } catch (e) {
      console.log(e)
      result = []
      return res.json({error: e ? e : `Error getting records!`, result }); 
  }
});

async function validateRecordTelnyx(records){
  const instance = await initInstance();
  const promises = [];
  for (const item of records) {
    let validated = false
    try {
      if(item.phone && item.phone !== '' && item.validated){
        const phone_number = item.phone;
        console.log('phone number from lambda: ' + phone_number);
        const { data } = await instance.get(`number_lookup/${phone_number}`);
        const { data: phone } = data;
        validated = COUNTRIES_CODES_VALID.includes(phone.country_code)
      }
      const record = {
        ...item,
        validated
      };
      promises.push(record);
      await new Promise(resolve => setTimeout(resolve, 0));
    } catch (e){
      console.log({error: e})
    }
  }
  const validateRecords = await Promise.all(promises);
  return validateRecords
}

app.listen(3000, function() {
    console.log("App started")
});

// Export the app object. When executing the application local this does nothing. However,
// to port it to AWS Lambda we will create a wrapper around that will load the app from
// this file
module.exports = app
