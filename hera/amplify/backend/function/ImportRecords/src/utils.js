const seedCustomListOption = {
  accidents: { items: [] },
  associates: { items: [] },
  canBeDeleted: true,
  canBeEdited: true,
  canBeReorder: true,
  daysCount: 0,
  default: false,
  parkingSpace: { items: [] },
  replaceByRouteParkingSpace: { items: [] },
  routeParkingSpace: { items: [] },
  tempAdded: true,
  usedFor: 0,
  vehicles: { items: [] }
}

function cleanImportInput ( Input ) {
  Object.keys(Input).forEach((k) => !Input[k] && delete Input[k]);
}

function cleanLicenseAndState( records ) {
  let items = [...records];
  items = items.map( item => {
    if (!item.licensePlate || !item.state) {
      item.licensePlate = null;
      item.state = null;
    }
    if (!item.licensePlateExp || item.licensePlateExp == "Invalid Date") {
      item.licensePlateExp = null;
    }
    return item;
  })

  return items;
}

function determineAction( recordsExisting, data ) {
  const existRecord = recordsExisting.find( record => record.vin === data.vin );
  if (existRecord) {
    return { ...data, action: 'Update' };
  }
  return { ...data, action: 'Create' };
}

function existRecordsWithErrors( array ) {
  return array.some( item => item.fieldsWithErrors.length > 0 )
}

function findVINDuplicates( array, property ) {
  const valuesSet = new Set();
  const duplicates = new Set();
  
  for (const obj of array) {
    const value = obj[property];
    if (valuesSet.has(value)) {
      duplicates.add(value); // Agregar el objeto duplicado al array de duplicados
    } else {
      valuesSet.add(value);
    }
  }

  return duplicates;
}

function generateUUID() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
  });
}

function groupRecordsByMatchColumn( records, recordsExisting, column ) {
  const matchingRecords = [];
  const recordsBackup = [];
  records.forEach(record => {
    const criteriaValue = record[column];
    const vehicle = recordsExisting.find( record => ( record[column] === criteriaValue ) );
    if (vehicle) {
      matchingRecords.push([record, vehicle])
      recordsBackup.push( vehicle );
    }
  });
  return [matchingRecords, recordsBackup];
}

function recordsCreate( recordsImport ) {
  return recordsImport.filter(record => record.action === 'Create');
}

function recordsUpdate( recordsImport ) {
  return recordsImport.filter(record => record.action === 'Update');
}

function registeredLicensePlates( records ) {
  return records.filter( record =>  record.licensePlate && record.state ).map( record => `${record.licensePlate}-${record.state}` );
}

function registeredVIN( records ) {
  return records.filter( record => record.vin ).map( record => record.vin );
}

function simulateFieldsNull( data ) {
  const simulateData = [];
  data.forEach(record => {
    let [ recordImport, recordDB ] = [ ...record ];
    let tempRecord = { ...recordDB }
    for (const key in recordImport) {
      if ( recordImport[key] && key in recordDB ) {
        tempRecord[key] = null
      }
    }
    simulateData.push(tempRecord);
  });

  return simulateData;
}

function updateRecordsDuplicates( records, listDuplicates ) {
  const recordsToImport = [ ...records ];
  recordsToImport.forEach((element, index, array) => {
    if ( listDuplicates.has( element.vin ) ) {
      array[index] = { ...element, fieldsWithErrors: [ ...element.fieldsWithErrors, "vin" ] }
    }
  });
  return recordsToImport
}

function validateRecordsByVIN( array, property ) {
  const dataToImport = [ ...array ];
  const valuesSet = new Set();
  dataToImport.forEach((element, index, array) => {
    const value = element[property];
    if (valuesSet.has( value )) {
      array[index] = { ...element, fieldsWithErrors: [ ...element.fieldsWithErrors, "vin" ] }
    } else {
      valuesSet.add( value );
    }
  });
  return dataToImport;
}

function validateRecordsByLicensePlate( array, properties ) {
  const dataToImport = [ ...array ];
  const [licensePlate, state] = properties;
  const valuesSet = new Set();

  dataToImport.forEach((element, index, array) => {
    const licensePlateState = `${element[licensePlate]}-${element[state]}`;
    if (valuesSet.has(licensePlateState)) {
      array[index] = { ...element, fieldsWithErrors: [ ...element.fieldsWithErrors, "licensePlate", "state" ] } // Agregar el objeto duplicado al array de duplicados
    } else {
      valuesSet.add(licensePlateState);
    }
  });
  return dataToImport;
}

function buildRecordsToImport( data ) {
  const dataSaveToDynamo = [];
  data.forEach(record => {
    const [ recordImport, recordDB ] = [ ...record ];
    const tempRecord = { ...recordDB, ...recordImport }
    const newTempRecord = removeEmptyFields( tempRecord );
    dataSaveToDynamo.push( newTempRecord );
  });
  return dataSaveToDynamo;
}

function removeEmptyFields( record ) {
  let cleanRecord = { ...record };
  for (const property in cleanRecord) {
    if ( Object.hasOwnProperty.call( cleanRecord, property ) ) {
      if ( !cleanRecord[property] ) {
        delete cleanRecord[ property ];
      }
    }
  }
  return cleanRecord;
}

function removeExtraFields( records, fields ) {
  const recordsUpdate = records.map( record => {
    let tempRecord = { ...record };
    for (const field of fields) {
      delete tempRecord[ field ];
    }
    return tempRecord;
  });
  return recordsUpdate;
}

function standarizeFields( record, templateObject ) {
  const tempRecord = { ...record };
  for (const field in templateObject) {
    if (Object.hasOwnProperty.call( templateObject, field )) {
      const value = templateObject[ field ];
      tempRecord[ value ] = tempRecord[ field ];
    }
    delete tempRecord[ field ];
  }
  return tempRecord;
}

function standardizeRecordFields( records, templateObject) {
  return records.map(record => standarizeFields(record, templateObject))
}


module.exports = {
  buildRecordsToImport,
  cleanImportInput,
  cleanLicenseAndState,
  determineAction,
  existRecordsWithErrors,
  findVINDuplicates,
  generateUUID,
  groupRecordsByMatchColumn,
  recordsCreate,
  recordsUpdate,
  registeredLicensePlates,
  registeredVIN,
  removeExtraFields,
  seedCustomListOption,
  simulateFieldsNull,
  standardizeRecordFields,
  updateRecordsDuplicates,
  validateRecordsByLicensePlate,
  validateRecordsByVIN
}