const { buildUpdateParams, scanDynamoRecords, updateDynamoRecord, deleteDynamoRecord, createDynamoRecord, queryDynamoRecords } = require('../dynamodbHelper');
const { cleanImportInput, generateUUID } = require('../utils');

async function getStaffByGroup(group) {
  try {
    let staffs = [];
    let lastEvaluatedKey = null;
    
    do {
      let params = {
        TableName: process.env.API_HERA_STAFFTABLE_NAME,
        ExpressionAttributeNames: {
          '#g': 'group',
          '#status': 'status',
          '#name': 'firstName',
          '#lname': 'lastName',
          '#tid': 'transporterId',
          '#email': 'email',
          '#phone': 'phone',
          '#hiredate': 'hireDate',
          '#gender': 'gender',
          '#pronouns': 'pronouns',
          '#dob': 'dob',
          '#gcp': 'gasCardPin', 
          '#dle': 'dlExpiration', 
          '#vr': 'vehicleReport', 
          '#hs': 'hourlyStatus'
        },
        ExpressionAttributeValues: {
          ':g': group,
        },
        FilterExpression: "#g = :g",
        ProjectionExpression: 'id, #g, #status, #name, #lname, #tid, #email, #phone, #hiredate, #gender, #pronouns, #dob, #gcp, #dle, #vr, #hs',
        ExclusiveStartKey: lastEvaluatedKey
      };
      let scanResult = await scanDynamoRecords(params);
      let items = scanResult.Items
      lastEvaluatedKey = scanResult.LastEvaluatedKey
      staffs = [...staffs, ...items]
    } while ( lastEvaluatedKey )

    return staffs
  } catch (err) {
      console.log("[getAllStaffs] Error in function getStaffByGroup", err);
      return {
        error: err,
      };
  }
}

async function updateRecords(records, backupRecords) {
  let totalRecordsUpdate = 0;

  try {
    for (const item of records ) {
      const { authorizedToDrive } = item;

      delete item.action;
      delete item.authorizedToDrive;
      delete item.authorizedToDriveItems;

      let updateParams = buildUpdateParams( process.env.API_HERA_STAFFTABLE_NAME, { ...item, updatedAt: new Date().toISOString() }, item.id );
      await updateDynamoRecord( updateParams );

      if (authorizedToDrive.items?.length) {

        const options = await getOptionsByStaff(item);
        
        if (options.length) {
          await removeOldVehiclesToDrive(options);
        }

        await saveAuthorizeToDriveByStaff(authorizedToDrive.items, item)
      }

      totalRecordsUpdate++;
    }
    
    return totalRecordsUpdate;
  } catch (e) {
    console.log({e});
  }
}

async function createRecords(records) {

  let totalRecordsCreate = 0;
  try {
    for (const item of records ) {
      const { email, group, transporterId, phone, authorizedToDrive } = item;

      if (email) {
        const foundStaffByEmail = await getStaffByEmail(email, group);
        if (foundStaffByEmail.length) {
          item.email = '';
        }
      }

      if (transporterId) {
        const foundStaffByTransporterId = await getStaffByTransporterID(transporterId, group);
        if (foundStaffByTransporterId.length) {
          item.transporterId = '';
        }
      }

      if (phone) {
        const foundStaffByPhone = await getStaffByPhone(phone, group);
        if (foundStaffByPhone.length) {
          item.phone = '';
        }
      }

      delete item.action;
      delete item.authorizedToDrive;
      delete item.authorizedToDriveItems;

      cleanImportInput( item );
      let itemRecord = {
        TableName: process.env.API_HERA_STAFFTABLE_NAME,
        Item: {
          id: generateUUID(),
          __typename: 'Staff',
          ...item,
          createdAt: new Date().toISOString(),
          updatedAt: new Date().toISOString()
        }
      }

      await createDynamoRecord(itemRecord);

      if (authorizedToDrive.items?.length) {
        await saveAuthorizeToDriveByStaff(authorizedToDrive.items, itemRecord.Item)
      }

      totalRecordsCreate++;
    }

    return totalRecordsCreate;

  } catch (e) {
    return {
      error: true,
      data: [],
      message: e.message
    }
  }
}

async function getStaffByEmail(email, group) {
  try {
    let staffs = []
    let lastEvaluatedKey = null
    do {
      const queryParams = {
        TableName: process.env.API_HERA_STAFFTABLE_NAME,
        IndexName: "byEmail",
        KeyConditionExpression: '#email = :email AND #group = :group',
        ExpressionAttributeNames: {
          '#group': 'group',
          '#email': 'email'
        },
        ExpressionAttributeValues: {
          ':email': email,
          ':group': group
        },
        ExclusiveStartKey: lastEvaluatedKey,
        ProjectionExpression: '#group, firstName, lastName, #email',
      }
      const queryResult = await queryDynamoRecords(queryParams)
      const items = queryResult.Items
      lastEvaluatedKey = queryResult.LastEvaluatedKey
      staffs = [...staffs, ...items]
    } while (lastEvaluatedKey)
    return staffs
  } catch (error) {
    console.log('[getStaffByEmail] Error in function getStaffByEmail', error)
    return {
      error: error,
    }
  }
}

async function getStaffByTransporterID(transporterId, group) {
  try {
    let staffs = []
    let lastEvaluatedKey = null
    do {
      const queryParams = {
        TableName: process.env.API_HERA_STAFFTABLE_NAME,
        IndexName: "byTransporterId",
        KeyConditionExpression: '#ti = :transporterId AND #group = :group',
        ExpressionAttributeNames: {
          '#group': 'group',
          '#ti': 'transporterId'
        },
        ExpressionAttributeValues: {
          ':group': group,
          ':transporterId': transporterId
        },
        ExclusiveStartKey: lastEvaluatedKey,
        ProjectionExpression: '#group, firstName, lastName, #ti',
      }
      const queryResult = await queryDynamoRecords(queryParams)
      const items = queryResult.Items
      lastEvaluatedKey = queryResult.LastEvaluatedKey
      staffs = [...staffs, ...items]
    } while (lastEvaluatedKey)
    return staffs
  } catch (error) {
    console.log('[getStaffByEmail] Error in function getStaffByEmail', error)
    return {
      error: error,
    }
  }
}

async function getStaffByPhone(phone, group) {
  try {
    let staffs = []
    let lastEvaluatedKey = null
    do {
      const queryParams = {
        TableName: process.env.API_HERA_STAFFTABLE_NAME,
        IndexName: "byPhone",
        KeyConditionExpression: '#phone = :phone AND #group = :group',
        ExpressionAttributeNames: {
          '#group': 'group',
          '#phone': 'phone'
        },
        ExpressionAttributeValues: {
          ':phone': phone,
          ':group': group
        },
        ExclusiveStartKey: lastEvaluatedKey,
        ProjectionExpression: '#group, firstName, lastName, #phone',
      }
      const queryResult = await queryDynamoRecords(queryParams)
      const items = queryResult.Items
      lastEvaluatedKey = queryResult.LastEvaluatedKey
      staffs = [...staffs, ...items]
    } while (lastEvaluatedKey)
    return staffs
  } catch (error) {
    console.log('[getStaffByPhone] Error in function getStaffByPhone', error)
    return {
      error: error,
    }
  }
}

async function saveAuthorizeToDriveByStaff(optionIds, staff) {
  try {
    for (const optionId of optionIds) {
      let itemRecord = {
        TableName: process.env.API_HERA_OPTIONSCUSTOMLISTSSTAFFTABLE_NAME,
        Item: {
          id: generateUUID(),
          __typename: 'OptionsCustomListsStaff',
          group: staff.group,
          optionsCustomListsStaffOptionCustomListId: optionId,
          optionsCustomListsStaffStaffId: staff.id,
          createdAt: new Date().toISOString(),
          updatedAt: new Date().toISOString()
        }
      }
      await createDynamoRecord( itemRecord );
    }
  } catch (error) {
    console.log(error)
  }
}

async function getOptionsByStaff(staff) {
  try {
    let options = []
    let lastEvaluatedKey = null
    do {
      const queryParams = {
        TableName: process.env.API_HERA_OPTIONSCUSTOMLISTSSTAFFTABLE_NAME,
        IndexName: "gsi-StaffOptionsCustomListsStaff",
        KeyConditionExpression: '#staffId = :optionsCustomListsStaffStaffId',
        ExpressionAttributeNames: {
          '#staffId': 'optionsCustomListsStaffStaffId'
        },
        ExpressionAttributeValues: {
          ':optionsCustomListsStaffStaffId': staff.id,
        },
        ExclusiveStartKey: lastEvaluatedKey,
        ProjectionExpression: 'id',
      }
      const queryResult = await queryDynamoRecords(queryParams)
      const items = queryResult.Items
      lastEvaluatedKey = queryResult.LastEvaluatedKey
      options = [...options, ...items]
    } while (lastEvaluatedKey)
    return options
  } catch (error) {
    console.log('[getOptionsByStaff] Error in function getOptionsByStaff', error)
    return {
      error: error,
    }
  }
}

async function removeOldVehiclesToDrive(vehicles) {
  for (const vehicleId of vehicles) {
    const { id } = vehicleId
    const deleteParams = {
      Key:{ id },
      TableName: process.env.API_HERA_OPTIONSCUSTOMLISTSSTAFFTABLE_NAME,
    }
    await deleteDynamoRecord(deleteParams);
  }
}

module.exports = {
  getStaffByGroup,
  updateStaffRecords: updateRecords,
  createStaffRecords: createRecords
}