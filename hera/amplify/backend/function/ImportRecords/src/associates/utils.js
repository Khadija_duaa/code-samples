function determineActionByTransporterID(dictionaryTransporterID, data) {
  return data.map( element => {
    return {
      ...element,
      action: dictionaryTransporterID[element.transporterId] ? 'Update' : 'Create'
    }
  })
}

function determineActionByEmail(dictionaryEmails, data) {
  return data.map( element => {
    return {
      ...element,
      action: dictionaryEmails[element.email] ? 'Update' : 'Create'
    }
  })
}


function hashTableTransporterID(data) {
  const dictionaryTransporterID = {};
  data.forEach(element => {
    const { transporterId } = element
    if (transporterId) {
      dictionaryTransporterID[ transporterId ] = element;
    }
  });
  return dictionaryTransporterID;
}

function hashTablePhone(data) { 
  const dictionaryPhones = {};
  data.forEach(element => {
    const { phone } = element;
    if (phone) {
      dictionaryPhones[ phone ] = element;
    }
  });
  return dictionaryPhones;
}

function hashTableEmail(data) {
  const dictionaryEmails = {};
  data.forEach(element => {
    const { email } = element;
    if (email) {
      dictionaryEmails[ email ] = element;
    }
  });
  return dictionaryEmails;
}

module.exports = {
  determineActionByEmail,
  determineActionByTransporterID,
  hashTableEmail,
  hashTableTransporterID,
  hashTablePhone
}