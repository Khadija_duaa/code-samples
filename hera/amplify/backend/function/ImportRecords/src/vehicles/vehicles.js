const { buildUpdateParams, scanDynamoRecords, updateDynamoRecord, createDynamoRecord } = require('../dynamodbHelper');
const { removeExtraFields, cleanImportInput, generateUUID } = require('../utils');

async function getVehiclesByGroup( group ) {
  try {
    let vehicles = [];
    let lastEvaluatedKey = null;
    
    do {
      let params = {
        TableName: process.env.API_HERA_VEHICLETABLE_NAME,
        ExpressionAttributeNames: {
          '#g': 'group',
          '#status': 'status',
          '#name': 'name',
          '#lp': 'licensePlate',
          '#lpe': 'licensePlateExp',
          '#state': 'state',
          '#vin': 'vin',
          '#gc': 'gasCard',
          '#m': 'mileage',
          '#pe': 'parkingSpace',
          '#ds': 'dateStart', 
          '#de': 'dateEnd', 
          '#own': 'ownership', 
          '#vt': 'type',
          '#desc': 'notes',
          '#mk': 'make',
          '#md': 'model',
          '#y': 'year',
          '#comp': 'company',
          '#ran': 'rentalAgreementNumber',
          '#vpsId': 'vehicleParkingSpaceId',
          '#vvtId': 'vehicleVehicleTypeId'
        },
        ExpressionAttributeValues: {
          ':g': group,
        },
        FilterExpression: "#g = :g",
        ProjectionExpression: 'id, #g, #status, #name, #lp, #lpe, #state, #vin, #gc, #m, #pe, #ds, #de, #own, #vt, #desc, #mk, #md, #y, #comp, #ran, #vpsId, #vvtId',
        ExclusiveStartKey: lastEvaluatedKey
      };
      let scanResult = await scanDynamoRecords(params);
      let items = scanResult.Items
      lastEvaluatedKey = scanResult.LastEvaluatedKey
      vehicles = [...vehicles, ...items]
    } while ( lastEvaluatedKey )

    return vehicles
  } catch (err) {
      console.log("[getAllVehicles] Error in function getAllVehicles", err);
      return {
        error: err,
      };
  }
}

async function updateRecords( records ) {
  let totalRecordsUpdate = 0;
  try {
    const data = removeExtraFields( records, [ "nameOptionParking", "nameOptionVehicle", "fieldsWithErrors", "action" ] );
    for (const item of data ) {
      let updateParams = buildUpdateParams( process.env.API_HERA_VEHICLETABLE_NAME, { ...item, updatedAt: new Date().toISOString() }, item.id );
      await updateDynamoRecord( updateParams );
      totalRecordsUpdate++;
    }
    
    return totalRecordsUpdate;
     
  } catch (e) {
    return {
      error: true,
      data: [],
      message: e.message
    }
  }
}

async function createRecords( records ) {

  let totalRecordsCreate = 0;
  try {
    const data = removeExtraFields( records, [ "nameOptionParking", "nameOptionVehicle", "fieldsWithErrors", "action" ] );
    for (const item of data ) {
      cleanImportInput( item );
      let itemRecord = {
        TableName: process.env.API_HERA_VEHICLETABLE_NAME,
        Item: {
          id: generateUUID(),
          __typename: 'Vehicle',
          ...item,
          createdAt: new Date().toISOString(),
          updatedAt: new Date().toISOString()
        }
      }
      await createDynamoRecord( itemRecord );
      totalRecordsCreate++;
    }

    return totalRecordsCreate;

  } catch (e) {
    return {
      error: true,
      data: [],
      message: e.message
    }
  }
}

module.exports = {
  getVehiclesByGroup,
  updateRecords,
  createRecords
}