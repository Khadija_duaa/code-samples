function hashTableVIN(data) { 
  const dictionaryVIN = {};
  data.forEach(element => {
    const { vin } = element;
    if (vin) {
      dictionaryVIN[vin] = element;
    }
  });
  return dictionaryVIN;
}

function hashTableLicenseByState(data) {
  const dictionaryLicenseByState = {};
  data.forEach(element => {
    const { licensePlate, state } = element;
    if (licensePlate && state) {
      dictionaryLicenseByState[`${licensePlate}-${state}`] = element;
    }
  });
  return dictionaryLicenseByState;
}

module.exports = {
  hashTableVIN,
  hashTableLicenseByState
}