const AWS = require('aws-sdk');

const pushToCloudWatch = (
    this.pushToCloudWatch=["error","log","info","warn","debug"],
    this.pushToCloudWatch = this.pushToCloudWatch.reduce(function(obj, method){
        obj[method] = function(...params){
            const _params = params.map(p => {
                const param = p instanceof Error ? {message: p.message, stack: p.stack} : p
                Object.keys(param).forEach(k=>{
                    const prop = param[k]
                    if(prop instanceof Error) param[k] = {message: prop.message, stack: prop.stack}
                })
                return param
            })
            const log = {
                LineNumber: getLineNumber(1, stack => stack.includes(method)),
                Date: formattedDate(),
                    Type: method.toUpperCase(),
                    Body: _params.length > 1? _params : _params[0]
                }
                console[method](JSON.stringify(log))
            }
        return obj
    },{}),
    this.pushToCloudWatch
)
  
function getLineNumber(stackIndex = 0, predicate){
    let line
    try {
        throw new Error()
    } catch (error) {
        const stack = String(error.stack).split('\n')
        const at = predicate ? stack.findIndex(predicate) : 2
        const [x,y,...z] = stack[at+stackIndex].match(/:[0-9]*/g)
        line = [x,y].join('').substring(1)
    } finally {
        return line
    }
}
  
function formattedDate(){
    const today = new Date()
    const year = today.getFullYear()
    const month = String(today.getMonth() + 1)
    const date = String(today.getDate())
    const hours = String(today.getHours())
    const minutes = String(today.getMinutes())
    return `${year}/${month.padStart(2, '0')}/${date.padStart(2, '0')} ${hours.padStart(2,'0')}:${minutes.padStart(2,'0')}`
}

module.exports = { formattedDate, getLineNumber, pushToCloudWatch }