/* Amplify Params - DO NOT EDIT
	API_HERA_CUSTOMLISTSTABLE_ARN
	API_HERA_CUSTOMLISTSTABLE_NAME
	API_HERA_GRAPHQLAPIIDOUTPUT
	API_HERA_OPTIONSCUSTOMLISTSSTAFFTABLE_ARN
	API_HERA_OPTIONSCUSTOMLISTSSTAFFTABLE_NAME
	API_HERA_OPTIONSCUSTOMLISTSTABLE_ARN
	API_HERA_OPTIONSCUSTOMLISTSTABLE_NAME
	API_HERA_STAFFTABLE_ARN
	API_HERA_STAFFTABLE_NAME
	API_HERA_VEHICLETABLE_ARN
	API_HERA_VEHICLETABLE_NAME
	ENV
	REGION
	STORAGE_HERA_BUCKETNAME
Amplify Params - DO NOT EDIT *//* Amplify Params - DO NOT EDIT
	ENV
	REGION
	STORAGE_HERA_BUCKETNAME
Amplify Params - DO NOT EDIT *//*
Copyright 2017 - 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at
    http://aws.amazon.com/apache2.0/
or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
*/




const express = require('express')
const bodyParser = require('body-parser')
const awsServerlessExpressMiddleware = require('aws-serverless-express/middleware')
const { getVehiclesByGroup, updateRecords, createRecords } = require('./vehicles/vehicles')
const { hashTableVIN, hashTableLicenseByState } = require('./vehicles/utils');
const { getStaffByGroup, createStaffRecords, updateStaffRecords } = require('./associates/staff')
const { hashTableEmail, hashTableTransporterID, hashTablePhone  } = require('./associates/utils');
const { getOptionsCustomListByGroup, getCustomListbyType, saveOptionCustomList } = require('./optionCustomList')
const { buildRecordsToImport, cleanLicenseAndState, groupRecordsByMatchColumn, recordsCreate, recordsUpdate, simulateFieldsNull, standardizeRecordFields } = require('./utils')
const { generateCsvBuffer, columnsCsvStaff } = require('./parseCSV');
const { saveFileIntoS3Bucket } = require('./aws-helpers/s3Helpers');

// declare a new express app
const app = express()
app.use(bodyParser.json())
app.use(awsServerlessExpressMiddleware.eventContext())

// Enable CORS for all methods
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "*")
  next()
});


/**********************
 * Example get method *
 **********************/

app.get('/item', function(req, res) {
  // Add your code here
  res.json({success: 'get call succeed!', url: req.url});
});

app.get('/item/*', function(req, res) {
  // Add your code here
  res.json({success: 'get call succeed!', url: req.url});
});

/****************************
* Example post method *
****************************/

app.post('/import-records', async function(req, res) {
  // Add your code here
  let recordsToImport = req.body.recordsToImport;
  const group = req.body.group;
  const matchColumn = req.body.matchColumn;
  const type = req.body.type;
  const module = req.body.module;

  try {

    if (module === 'vehicles') {
      
      let vinExisting = {};
      let licenseByStateExisting = {};
      const vehicles = await getVehiclesByGroup(group);
      
      if ( type === 'validation' ) {

        vinExisting = hashTableVIN(vehicles);
        licenseByStateExisting = hashTableLicenseByState(vehicles);

        return res.json({
          error: false,
          message: '',
          data: {
            records: recordsToImport,
            vins: vinExisting,
            licenseByState: licenseByStateExisting
          }
        })
  
      } else {
  
        let countRecordsUpdate = 0;
        let countRecordsCreate = 0;
  
        const optionsCustomListByGroup = await getOptionsCustomListByGroup(group);
        const customListVehicle = await getCustomListbyType(group, "vehicle-type");
        const [customListVehicleType] = customListVehicle;
  
        const customListParking = await getCustomListbyType(group, "parking-space");
        const [customListParkingSpace] = customListParking;
  
        const optionsVehicleType = optionsCustomListByGroup.filter( option => option.optionsCustomListsCustomListsId === customListVehicleType.id );
        optionsVehicleType.sort( (firstItem, secondItem) => firstItem.order - secondItem.order );
  
        const optionsParkingSpace = optionsCustomListByGroup.filter( option => option.optionsCustomListsCustomListsId === customListParkingSpace.id );
        optionsParkingSpace.sort( (firstItem, secondItem) => firstItem.order - secondItem.order );
  
        // add options custom list
        const optionVehicleTypeToAdd = [];
        const optionParkingSpaceToAdd = [];
  
        const optionsVehicleTypeToImport = recordsToImport.filter( record => record.vehicleVehicleTypeId );
        const optionsParkingSpaceToImport = recordsToImport.filter( record => record.vehicleParkingSpaceId );
  
        const optionsVehicleTypeToImportMapping = optionsVehicleTypeToImport.map( record => {
          return {
            id: record.vehicleVehicleTypeId,
            option: record.nameOptionVehicle,
            canBeReorder: true,
            canBeEdited: true,
            group: group,
            optionsCustomListsCustomListsId: customListVehicleType.id,
            canBeDeleted: true,
            daysCount: 0,
            default: false,
            usedFor: 0
          }
        })
  
        for (const element of optionsVehicleTypeToImportMapping) {
          const elementFound = optionsVehicleType.find( recordOption => recordOption.option.toLowerCase() == element.option.toLowerCase() );
          if (!elementFound) {
            const newOptionToAdd = { ...element, order: !optionsVehicleType.length ? 0 : optionsVehicleType[ optionsVehicleType.length - 1 ].order + 1 }
            optionVehicleTypeToAdd.push( newOptionToAdd );
            optionsVehicleType.push( newOptionToAdd );
          }
        }
  
        const optionsParkingSpaceToImportMapping = optionsParkingSpaceToImport.map( record => {
          return {
            id: record.vehicleParkingSpaceId,
            option: record.nameOptionParking,
            canBeReorder: true,
            canBeEdited: true,
            group: group,
            optionsCustomListsCustomListsId: customListParkingSpace.id,
            canBeDeleted: true,
            daysCount: 0,
            default: false,
            usedFor: 0
          }
        });
  
        for (const element of optionsParkingSpaceToImportMapping) {
          const elementFound = optionsParkingSpace.find( recordOption => recordOption.option.toLowerCase() == element.option.toLowerCase() );
          if (!elementFound) {
            const newOptionToAdd = { ...element, order: !optionsParkingSpace.length ? 0 : optionsParkingSpace[ optionsParkingSpace.length - 1 ].order + 1 }
            optionParkingSpaceToAdd.push( newOptionToAdd );
            optionsParkingSpace.push( newOptionToAdd );
          }
        }
  
        const optionsCustomListToSave = [ ...optionVehicleTypeToAdd, ...optionParkingSpaceToAdd ];
  
        if (optionsCustomListToSave.length) {
          await saveOptionCustomList( optionsCustomListToSave );
        }
  
        let simulateRecords = [];
        let recordsToSave = [];
        let recordsCreateToSave = [];
  
        recordsToImport = cleanLicenseAndState( recordsToImport );
        const recordsToUpdate = recordsUpdate( recordsToImport );
        const recordsToCreate = recordsCreate( recordsToImport );
        if ( recordsToUpdate.length > 0 ) {
          let [ groupRecordsByColumnMatch, backupRecords ] = groupRecordsByMatchColumn( recordsToUpdate, vehicles, matchColumn );
          // simulateRecords = simulateFieldsNull( groupRecordsByColumnMatch );
          recordsToSave = buildRecordsToImport( groupRecordsByColumnMatch );
          recordsToSave = standardizeRecordFields( recordsToSave, {
            vehicleName: 'name', 
            licensePlateExpiration: 'licensePlateExp' ,
            startDate: 'dateStart',
            endDate: 'dateEnd'
          });
          countRecordsUpdate = await updateRecords( recordsToSave );
        }
  
        if ( recordsToCreate.length > 0 ) {
          recordsCreateToSave = standardizeRecordFields( recordsToCreate, { 
            vehicleName: 'name', 
            licensePlateExpiration: 'licensePlateExp' ,
            startDate: 'dateStart',
            endDate: 'dateEnd'
          })
          countRecordsCreate = await createRecords( recordsCreateToSave );
        }
  
        return res.json({
          data: {
            countRecordsUpdate,
            countRecordsCreate
          }
        })
  
      } 
    } else {
      
      let phonexExisting = {};
      let transportersExisting = {};
      let emailsExisting = {};
      const staffs = await getStaffByGroup(group);
      
      if (type === 'validation') {

        phonexExisting = hashTablePhone( staffs );
        transportersExisting = hashTableTransporterID( staffs );
        emailsExisting = hashTableEmail( staffs );

        return res.json({
          error: false,
          message: '',
          data: {
            records: recordsToImport,
            transporters: transportersExisting,
            emails: emailsExisting,
            phones: phonexExisting
          }
        })
      } else {
        const optionsCustomListByGroup = await getOptionsCustomListByGroup(group);
        const customListVehicle = await getCustomListbyType(group, "vehicle-type");
        const [ customListVehicleType ] = customListVehicle;

        const optionsVehicleType = optionsCustomListByGroup.filter( option => option.optionsCustomListsCustomListsId === customListVehicleType.id );
        optionsVehicleType.sort( (firstItem, secondItem) => firstItem.order - secondItem.order );
        
        const optionVehicleTypeToAdd = [];

        const optionsVehicleTypeToImport = recordsToImport.filter( record => record.authorizedToDriveItems.length );
  
        for (const item of optionsVehicleTypeToImport) {
          const { authorizedToDriveItems } = item;
          authorizedToDriveItems.forEach(item => {
            const optionFound = optionsVehicleType.find( _item => _item.option === item.option );
            if (!optionFound) {
              const newOptionToAdd = { 
                ...item, 
                canBeDeleted: true,
                canBeEdited: true, 
                canBeReorder: true,
                daysCount: 0,
                default: false,
                group: group,
                order: !optionsVehicleType.length ? 0 : optionsVehicleType[ optionsVehicleType.length - 1 ].order + 1,
                optionsCustomListsCustomListsId: customListVehicleType.id,
                usedFor: 0
              }
              optionVehicleTypeToAdd.push( newOptionToAdd );
              optionsVehicleType.push( newOptionToAdd );
            }
          });
        }

        if (optionVehicleTypeToAdd.length) {
          await saveOptionCustomList( optionVehicleTypeToAdd );
        }
        
        let countRecordsUpdate = 0;
        let countRecordsCreate = 0;
        let simulateRecords = [];
        let recordsToSave = [];
        const recordsToUpdate = recordsUpdate( recordsToImport );
        const recordsToCreate = recordsCreate( recordsToImport );

        if ( recordsToUpdate.length > 0 ) {
          let [ groupRecordsByColumnMatch, backupRecords ] = groupRecordsByMatchColumn(recordsToUpdate, staffs, matchColumn);
          recordsToSave = buildRecordsToImport(groupRecordsByColumnMatch);
          countRecordsUpdate = await updateStaffRecords(recordsToSave, backupRecords);
        }

        if ( recordsToCreate.length > 0 ) {
          countRecordsCreate = await createStaffRecords( recordsToCreate );
        }

        return res.json({
          data: {
            countRecordsCreate,
            countRecordsUpdate
          }
        })
      }
    }

  } catch (e) {
    res.json({ message: 'Import Record Error', error: true, ...e })
    console.log("Error: ", e);
  }

});

app.post('/item', function(req, res) {
  // Add your code here
  res.json({success: 'post call succeed!', url: req.url, body: req.body})
});

app.post('/item/*', function(req, res) {
  // Add your code here
  res.json({success: 'post call succeed!', url: req.url, body: req.body})
});

/****************************
* Example put method *
****************************/

app.put('/item', function(req, res) {
  // Add your code here
  res.json({success: 'put call succeed!', url: req.url, body: req.body})
});

app.put('/item/*', function(req, res) {
  // Add your code here
  res.json({success: 'put call succeed!', url: req.url, body: req.body})
});

/****************************
* Example delete method *
****************************/

app.delete('/item', function(req, res) {
  // Add your code here
  res.json({success: 'delete call succeed!', url: req.url});
});

app.delete('/item/*', function(req, res) {
  // Add your code here
  res.json({success: 'delete call succeed!', url: req.url});
});

app.listen(3000, function() {
    console.log("App started")
});

// Export the app object. When executing the application local this does nothing. However,
// to port it to AWS Lambda we will create a wrapper around that will load the app from
// this file
module.exports = app
