const AWS = require('aws-sdk');

/**
 * Uploads a file to S3
 * @param {ReadStream} readStream -> Data to be saved in S3
 * @param {String} filename -> Name of the file to be saved in S3
 * @returns {String} url to download file
 */
 async function saveFileIntoS3Bucket(readStream, filename) {
  const s3 = new AWS.S3();

  const uploadParams = {
    Bucket: process.env.STORAGE_HERA_BUCKETNAME,
    ACL: 'public-read',
    Key: `csv-import/${filename}.csv`,
    Body: readStream,
    Expires: Date.now() + (1000 * 60 * 60),
  };

  // call S3 to retrieve uploaded file url
  const url = await (new Promise((resolve, reject) => {
    s3.upload (uploadParams, function (err, data) {
      if (err) return reject(err);
      if (data) return resolve(data.Location);

      reject(new Error('No data available'));
    });
  }));

  return url;
}

/**
 * Reads a file from S3
 * @param {String} s3Key -> S3 key to locate the file in S3
 * @returns {Buffer} Data as buffer
 */
async function readFileFromS3Bucket(s3Key) {
  const s3 = new AWS.S3();

  const keyPrefix = 'public/';

  const uploadParams = {
    Bucket: process.env.STORAGE_HERA_BUCKETNAME,
    Key: keyPrefix + s3Key,
  };

  // call S3 to retrieve stream
  const buffer = await (new Promise((resolve, reject) => {
    s3.getObject (uploadParams, function (err, data) {
      if (err) return reject(err);
      if (data) return resolve(data.Body);

      reject(new Error('No data available'));
    });
  }));

  return buffer;
}

module.exports = {
  saveFileIntoS3Bucket,
  readFileFromS3Bucket,
};
