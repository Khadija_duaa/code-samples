const columnsCsvStaff = [
  'id',
  'dlExpiration',
  'dob',
  'email',
  'firstName',
  'lastName',
  'transporterId',
  'phone',
  'hireDate',
  'gender',
  'pronouns',
  'gasCardPin',
  'vehicleReport',
  'status',
  'hourlyStatus'
]

function escapeCommas (data) {
  return data.map(row => {
    for (const [key, value] of Object.entries(row)) {
      row[key] = value.includes(",") ? `"${value}"` : value;
    }
    return row;
  });
}

function buildData(data) {
  const dataToStandarize = [ ...data ];
  const arrayMaps = []
  dataToStandarize.forEach((record) => {
    let map = new Map();
    columnsCsv.forEach(column => {
      if ( !(column in record) ) {
        map.set(column, null)
      }
      map.set(column, record[column])
    })
    arrayMaps.push( map );
    map = null;
  });
  return arrayMaps;
}

function arrayToCSV(data, columnsCsv) {
  
  const dataToSave = buildData( data );
  const valueArrays = dataToSave.map((map) => Array.from(map.values()));
  const flattenedValues = valueArrays.flat();
  const dataRows = [];
  
  for (let i = 0; i < flattenedValues.length; i += columnsCsv.length) {
    const rowData = flattenedValues.slice(i, i + columnsCsv.length).join(",");
    dataRows.push(rowData);
  }
  
  const csvString = [columnsCsv, ...dataRows].join('\n');
  return csvString;
}


function  generateCsvBuffer( data, columns ) {
  const escapedData = escapeCommas( data );
  const CSV = arrayToCSV( escapedData, columns );
  const buffer = Buffer.from(CSV, "binary");
  return buffer;
}

module.exports = {
  generateCsvBuffer,
  columnsCsvStaff
}