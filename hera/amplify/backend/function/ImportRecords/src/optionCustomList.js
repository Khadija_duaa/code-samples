const { scanDynamoRecords, createDynamoRecord } = require('./dynamodbHelper');

async function getOptionsCustomListByGroup(group) {
  try {
    let optionsCustomList = [];
    let lastEvaluatedKey = null;
    
    do {
      let params = {
        TableName: process.env.API_HERA_OPTIONSCUSTOMLISTSTABLE_NAME,
        ExpressionAttributeNames: {
          '#g': 'group',
          '#canBeDeleted': 'canBeDeleted',
          '#canBeEdited': 'canBeEdited',
          '#canBeReorder': 'canBeReorder',
          '#daysCount': 'daysCount',
          '#option': 'option',
          '#order': 'order',
          '#optCLId': 'optionsCustomListsCustomListsId',
        },
        ExpressionAttributeValues: {
          ':g': group,
        },
        FilterExpression: "#g = :g",
        ProjectionExpression: 'id, #g, #canBeDeleted, #canBeEdited, #canBeReorder, #daysCount, #option, #order, #optCLId',
        ExclusiveStartKey: lastEvaluatedKey
      };
      let scanResult = await scanDynamoRecords(params);
      let items = scanResult.Items
      lastEvaluatedKey = scanResult.LastEvaluatedKey
      optionsCustomList = [...optionsCustomList, ...items]
    } while ( lastEvaluatedKey )

    return optionsCustomList
  } catch (err) {
      console.log("[getOptionCustomListByGroup] Error in function getOptionsCustomListByGroup", err);
      return {
        error: err,
      };
  }
}

async function getCustomListbyType(group, type) {
  try {
    let customLists = []
    let lastEvaluatedKey = null
    do {
      let params = {
        ExpressionAttributeNames: {
          '#group': 'group',
          '#type': 'type',
        },
        ExpressionAttributeValues: {
          ':type': type,
          ':g': group
        },
        FilterExpression: "#type = :type and #group = :g",
        TableName: process.env.API_HERA_CUSTOMLISTSTABLE_NAME,
        ProjectionExpression: 'id, #group, #type',
        ExclusiveStartKey: lastEvaluatedKey
      };
      let scanResult = await scanDynamoRecords(params);
      let items = scanResult.Items
      lastEvaluatedKey = scanResult.LastEvaluatedKey
      customLists = [...customLists, ...items]
    } while (lastEvaluatedKey)
    return customLists
  } catch (err) {
    console.log("[getCustomListbyType] Error in function getCustomListbyType", err);
    return {
        error: err,
    };
  }
}

async function saveOptionCustomList( customList ) {
  try {
    for (const option of customList) {
      let createOptionCustomList = {
        TableName: process.env.API_HERA_OPTIONSCUSTOMLISTSTABLE_NAME,
        Item: {
          id: option.id,
          __typename: 'OptionsCustomLists',
          order: option.order,
          option: option.option,
          default: option.default,
          usedFor: option.usedFor,
          daysCount: option.daysCount,
          canBeEdited: option.canBeEdited,
          canBeDeleted: option.canBeDeleted,
          canBeReorder: option.canBeReorder,
          group: option.group,
          createdAt: new Date().toISOString(),
          updatedAt: new Date().toISOString(),
          optionsCustomListsCustomListsId: option.optionsCustomListsCustomListsId
        }
      };
      await createDynamoRecord(createOptionCustomList);
    }
  } catch (err) {
    console.log("[saveOptionCustomList] Error in function saveOptionCustomList", err);
    return {
      error: err,
    };
  }
}

module.exports = {
  getOptionsCustomListByGroup,
  getCustomListbyType,
  saveOptionCustomList
}