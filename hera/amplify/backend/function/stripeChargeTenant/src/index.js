/* Amplify Params - DO NOT EDIT
	API_HERA_CARDTABLE_ARN
	API_HERA_CARDTABLE_NAME
	API_HERA_GRAPHQLAPIENDPOINTOUTPUT
	API_HERA_GRAPHQLAPIIDOUTPUT
	API_HERA_INVOICELINEITEMTABLE_ARN
	API_HERA_INVOICELINEITEMTABLE_NAME
	API_HERA_INVOICETABLE_ARN
	API_HERA_INVOICETABLE_NAME
	API_HERA_MESSAGETABLE_ARN
	API_HERA_MESSAGETABLE_NAME
	API_HERA_STRIPEQUEUETABLE_ARN
	API_HERA_STRIPEQUEUETABLE_NAME
	API_HERA_TENANTTABLE_ARN
	API_HERA_TENANTTABLE_NAME
	API_SENDINTERNALMESSAGE_APIID
	API_SENDINTERNALMESSAGE_APINAME
	API_SENDMESSAGE_APIID
	API_SENDMESSAGE_APINAME
	API_STRIPESETUP_APIID
	API_STRIPESETUP_APINAME
	ENV
	REGION
Amplify Params - DO NOT EDIT *//*
Use the following code to retrieve configured secrets from SSM:

const aws = require('aws-sdk');

const { Parameters } = await (new aws.SSM())
  .getParameters({
    Names: ["STRIPE_SECRET_KEY"].map(secretName => process.env[secretName]),
    WithDecryption: true,
  })
  .promise();

Parameters will be of the form { Name: 'secretName', Value: 'secretValue', ... }[]
*/
// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');

// Set the region
AWS.config.update({ region: process.env.REGION });
//const { generateTemplate } = require('./emailTemplate')

var ddb = new AWS.DynamoDB.DocumentClient({ apiVersion: '2012-08-10' });

// Set your secret key. Remember to switch to your live secret key in production!
// See your keys here: https://dashboard.stripe.com/account/apikeys
const { getSecretValue } = require('/opt/nodejs/ssm')
let stripe;
const { generateInvoiceTemplate } = require('./invoiceTemplate')
const { generateEmailTemplate } = require('./emailTemplate')
const { pushToCloudWatch } = require('./cloudWatchLogger')
const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

async function initStripe(){
    if(!stripe){
        console.log("Initializing Stripe Module");
        const stripeKey = await getSecretValue(process.env.STRIPE_SECRET_KEY)
		stripe = require('stripe')(stripeKey);
    }
}

exports.handler = async (event) => {
	
	// validate request authoriztion
	
	// get tenant from request
	let reqBody = JSON.parse(event.body)
	let tenant = reqBody?.tenant
	let chargeTenant = { status: "Not charged" }
	
	try {
		console.log('---event', JSON.stringify(event))
		console.log('+-----------------------+')
		chargeTenant = await charge(tenant)
		console.log('+-----------------------+')
	} catch (e) {
		console.log('---Something went wrong', e)
		const loggerBody = {
    		'Title': `Error charging tenant, tenant group: ${tenant.group}`,
    		"Reason": e
		}
		pushToCloudWatch.error(loggerBody)
		chargeTenant = { "status": "error", e: e }
	}

	return {
		statusCode: 200,
		// Uncomment below to enable CORS requests
		headers: {
			"Access-Control-Allow-Origin": "*",
			"Access-Control-Allow-Headers": "*"
		}, 
		body: JSON.stringify(chargeTenant),
	};
};


async function charge(tenant) {
	
	console.log('---charge tenant', tenant)
	// get Stripe customer id
	const customerId = tenant.stripeCustomerId
	if (!customerId) {
		// not set up in stripe, throw error and email tenant
		console.log("Error: Customer not set up in Stripe. Customer: ", tenant.companyName)

		// email Hera if tenant billing email not set up
		// console.log("Error: Customer billing email not set up.")
		return { status: "Error: Customer billing email not set up.", "tenant": tenant }
	}

	// get invoice for CURRENT month
	var group = tenant.group;
	var today = new Date()
	var year = today.getFullYear()
	var month = today.getMonth()
	var monthName = months[month]

	let params = {
		ExpressionAttributeValues: {
			':g': group,
			':y': String(year) + '#' + String(month),
		},
		ExpressionAttributeNames: {
			'#g': 'group',
			'#y': 'year#month',
			'#s': 'status',
			'#mm': 'month',
			'#yyyy': 'year',
		},
		KeyConditionExpression: '#g = :g AND #y = :y',
		ProjectionExpression: 'id, #y, #g, #yyyy, #mm, invoiceTotal, discountPercent, discountFixed, #s',
		TableName: process.env.API_HERA_INVOICETABLE_NAME,
		IndexName: 'byGroupYearMonth',
	};
	var queryResult = await queryDynamoRecords(params);
	var items = queryResult.Items
	var invoice = items[0]

	// throw error if no invoice from previous month
	if (!invoice) {
		console.log("Error: no invoice from last month.", tenant.companyName)
		return { status: "Error: no invoice from last month.", tenant: tenant.companyName }	
	}

	// skip if already paid
	if (invoice.status == 'Paid') {
		console.log("Skipping " + tenant.companyName + ": Invoice already paid.")
		return { status: "Error: This month's invoice is already marked as paid. Check stripe for full details for the tenant.", invoice: invoice }
	}
	
	// return { status: "found invoice", invoice: invoice, params: params }

	// set up totals
	const taxRate = 0
	const fixedDiscount = invoice.discountFixed ? invoice.discountFixed : 0
	const percentDiscount = invoice.discountPercent ? invoice.discountPercent / 100 * invoice.invoiceTotal : 0
	const subtotalLessDiscount = invoice.invoiceTotal - fixedDiscount - percentDiscount
	const subtotalPlusTaxes = (subtotalLessDiscount + subtotalLessDiscount * taxRate).toFixed(2)
	const invoiceTotal = parseInt(subtotalPlusTaxes.replace(/[^0-9]/g, '')) // pass total in cents to Stripe
	console.log({ invoiceTotal: invoiceTotal })

	// throw error if total is 0
	if (invoiceTotal <= 0) {
		console.log("Error: $0 balance on invoice.", tenant.companyName)
		return { status: "Error: $0 balance on invoice.", invoice: invoice }
	}

	// get customer's cards from Dynamo
	params = {
		ExpressionAttributeValues: {
			':g': group,
		},
		ExpressionAttributeNames: {
			'#g': 'group',
		},
		KeyConditionExpression: '#g = :g',
		ProjectionExpression: 'id, stripePaymentMethodId, active',
		TableName: process.env.API_HERA_CARDTABLE_NAME,
		IndexName: 'byGroup',
	};
	var queryResult = await queryDynamoRecords(params);
	var cards = queryResult.Items

	// find active card
	var activeCard = cards.find(card => {
		return card.active == true
	})
	const activeCardDynamoId = activeCard.id
	const paymentMethodId = activeCard.stripePaymentMethodId

	// create a payment intent in Stripe
	let status = ''
	let queueStatus = ''
	let paymentIntentId = ''
	let paymentIntent
	let lastFour
	await initStripe();
	try {
		if(invoiceTotal > 0){
			// get payment method from Stripe
			const paymentMethod = await stripe.paymentMethods.retrieve(paymentMethodId);
		
			// check if card is set to expire soon
			const expMonth = paymentMethod.card.exp_month
			const expYear = paymentMethod.card.exp_year
			lastFour = paymentMethod.card.last4
			const exp = String(expYear) + String(expMonth).padStart(2, '0')
			const currMonth = String(year) + String(month + 1).padStart(2, '0')
			const monthsLeft = (parseInt(exp) - parseInt(currMonth) - 1) % 88 // mod it to account for transition between years, -1 because invoice is for previous month
			console.log({ monthsLeft: monthsLeft })
			
			paymentIntent = await stripe.paymentIntents.create({
				amount: invoiceTotal,
				currency: 'usd',
				customer: customerId,
				payment_method: paymentMethodId,
				off_session: true,
				confirm: true,
			});
			
			console.log('---paymentItents Paid', JSON.stringify({
				group: tenant.group,
				paymentIntentId: paymentIntent.id,
				amount: invoiceTotal,
				currency: 'usd',
				customer: customerId,
				payment_method: paymentMethodId,
				off_session: true,
				confirm: true,
			}))
		}

		status = 'Paid'
		queueStatus = 'Paid'
		paymentIntentId = paymentIntent.id
	} catch (err) {
		// Error code will be authentication_required if authentication is needed
		console.log('---Error code is: ', err.code);
		console.log('---Status code is: ', err.statusCode)
		console.log({ error: err })
		
		const loggerBody = {
			"ErrorCode": err.code,
			"StatusCode": err.statusCode,
    		"Title": `Payment intent error, tenant group: ${group}`,
    		"Reason": err
		}
		pushToCloudWatch.error(loggerBody)

		// return { status: 'Error paying', error: err }
		
		if (err.statusCode != 429 && err.statusCode != 503) {
			
			const paymentIntentRetrieved = await stripe.paymentIntents.retrieve(err.raw.payment_intent.id);
			console.log('PI retrieved: ', paymentIntentRetrieved.id);

			// add error message to card in Dynamo
			var updateCardParams = {
				TableName: process.env.API_HERA_CARDTABLE_NAME,
				Key: {
					"id": activeCardDynamoId
				},
				UpdateExpression: "set #a = :a",
				ExpressionAttributeNames: {
					"#a": "chargeError",

				},
				ExpressionAttributeValues: {
					":a": err.raw.message,
				},
				ReturnValues: "UPDATED_NEW"
			};
			await updateDynamoRecord(updateCardParams);

			// update Tenant with unpaid invoice status
			let currentAccountStatus = tenant.accountPremiumStatus
			if (!currentAccountStatus.includes('unpaid')) currentAccountStatus.push('unpaid')

			let date = new Date();
			date.setDate(date.getDate() + 7);
			console.log({ payOutstandingByDate: tenant.payOutstandingByDate })
			console.log({ date: date })

			var payOutstandingByDate
			if (!tenant.payOutstandingByDate || JSON.stringify(tenant.payOutstandingByDate) == '{}') payOutstandingByDate = date
			else payOutstandingByDate = tenant.payOutstandingByDate

			console.log({ payOutstandingByDate: payOutstandingByDate })
			let updateParams = {
				TableName: process.env.API_HERA_TENANTTABLE_NAME,
				Key: {
					"id": tenant.id
				},
				UpdateExpression: "set #a = :a, #b = :b",
				ExpressionAttributeNames: {
					"#a": "accountPremiumStatus",
					"#b": "payOutstandingByDate"
				},
				ExpressionAttributeValues: {
					":a": currentAccountStatus,
					":b": payOutstandingByDate
				},
				ReturnValues: "UPDATED_NEW"
			};
			await updateDynamoRecord(updateParams);

			status = 'Payment Error'
			queueStatus = 'Payment Error'

		} else {

			console.log('---Stripe error 429, 503', err)
			status = 'Pending'
			queueStatus = 'Retry'
			return { status: 'Error: ---Stripe error 429, 503', error: err }
			
		}
	}
	
	// update status of invoice
	var updateParams = {
		TableName: process.env.API_HERA_INVOICETABLE_NAME,
		Key: {
			"id": invoice.id
		},
		UpdateExpression: "set #a = :a, #b = :b",
		ExpressionAttributeNames: {
			"#a": "status",
			"#b": "cardLastFourCharged"
		},
		ExpressionAttributeValues: {
			":a": status,
			":b": lastFour
		},
		ReturnValues: "UPDATED_NEW"
	};
	
	await updateDynamoRecord(updateParams);

	// email receipt
	let emailStatus = 'Failed to send email. ';
	if(status === 'Paid'){
		console.log('creating invoice email to tenant')
		try{
			await emailReceiptToTenant(tenant, invoice, status)
			emailStatus = "Final invoice successfully emailed to tenant. "
		}catch(e){
			console.log('payment successful, but error emailing tenant confirmation:', e, 'tenant', tenant)
			emailStatus = "Payment succeeded but an error occurred when emailing the tenant's final invoice. "
			const loggerBody = {
				'Title': `Payment successful, but error emailing tenant confirmation: ${tenant.group}`,
				"Reason": e
			}
			pushToCloudWatch.error(loggerBody)
		}
	}


	return {
		status: status,
		emailStatus: emailStatus,
		invoice: invoice,		
		group: tenant,
		paymentIntentId: paymentIntentId,
		amount: invoiceTotal,
		currency: 'usd',
		customer: customerId,
		payment_method: paymentMethodId		
	}
}

async function emailReceiptToTenant(tenant, invoice, status) {

	// get invoice line items
	var invoiceLineItems = []
	let lastEvaluatedKey = null
	do {
		let params = {
			ExpressionAttributeValues: {
				':i': invoice.id,
			},
			ExpressionAttributeNames: {
				'#i': 'invoiceLineItemInvoiceId',
				'#d': 'day',
				'#m': 'month',
				'#t': 'date'
			},
			KeyConditionExpression: '#i = :i',
			ProjectionExpression: 'id,standardCostExt,bundleCostExt,performanceCostExt,rosteringCostExt,staffCostExt,vehiclesCostExt,activeStaff,#d,#m, #t',
			TableName: process.env.API_HERA_INVOICELINEITEMTABLE_NAME,
			IndexName: 'gsi-InvoiceInvoiceLineItems',
			ExclusiveStartKey: lastEvaluatedKey
		};
		var queryResult = await queryDynamoRecords(params);
		var items = queryResult.Items
		lastEvaluatedKey = queryResult.LastEvaluatedKey
		invoiceLineItems = [...invoiceLineItems, ...items]
	} while (lastEvaluatedKey)

	var lineItemIds = invoiceLineItems.map(item => {
		return item.id
	});

	// sum up line items to get updated total
	var standardTotal = invoiceLineItems.reduce((accumulator, item) => ({ standardCostExt: accumulator.standardCostExt + item.standardCostExt }))
	var bundleTotal = invoiceLineItems.reduce((accumulator, item) => ({ bundleCostExt: accumulator.bundleCostExt + item.bundleCostExt }))
	var performanceTotal = invoiceLineItems.reduce((accumulator, item) => ({ performanceCostExt: accumulator.performanceCostExt + item.performanceCostExt }))
	var rosteringTotal = invoiceLineItems.reduce((accumulator, item) => ({ rosteringCostExt: accumulator.rosteringCostExt + item.rosteringCostExt }))
	var staffTotal = invoiceLineItems.reduce((accumulator, item) => ({ staffCostExt: accumulator.staffCostExt + item.staffCostExt }))
	var vehiclesTotal = invoiceLineItems.reduce((accumulator, item) => ({ vehiclesCostExt: accumulator.vehiclesCostExt + item.vehiclesCostExt }))
	var activeStaffTotal = invoiceLineItems.reduce((accumulator, item) => ({ activeStaff: accumulator.activeStaff + item.activeStaff }))
	//var averageActiveStaffTotal = (activeStaffTotal.activeStaff + numActiveDas) / (invoiceLineItems.length + 1)
	//var invoiceTotal = standardTotal.standardCostExt + bundleTotal.bundleCostExt + performanceTotal.performanceCostExt + rosteringTotal.rosteringCostExt + staffTotal.staffCostExt + vehiclesTotal.vehiclesCostExt

	// create html invoice if its last day of the month
	var invoiceHtml = ''

	// calculate line item total
	invoiceLineItems = invoiceLineItems.map(item => {
		var total = item.standardCostExt +
			item.bundleCostExt +
			item.performanceCostExt +
			item.rosteringCostExt +
			item.staffCostExt +
			item.vehiclesCostExt;
			item.invoiceTotal = total
		return item
	})

	// sort line items by date
	invoiceLineItems.sort(function (a, b) {
		return new Date(a.date) - new Date(b.date);
	})

	invoice.discountPercent = tenant.discountPercent
	invoice.discountFixed = tenant.discountFixed
	invoice.discountPercentLabel = tenant.discountPercentLabel
	invoice.discountFixedLabel = tenant.discountFixedLabel

	// create html from line item array
	invoiceHtml = generateInvoiceTemplate(invoice, invoiceLineItems, tenant)
	console.log('invoice email ----------------', invoiceHtml)

	// send invoice email if billing email is provided and not still on an active trial
	if (tenant.stripeBillingEmail) {
		console.log('sending email to:', { billingEmail: tenant.stripeBillingEmail })
		var today = new Date()
		var year = today.getFullYear()
		var month = today.getMonth()
		var year = today.getFullYear()
		// send email with invoice
		var subject = "Hera: Invoice for " + months[month] + ' ' + year
		var header = "Below is your invoice for " + months[month] + ' ' + year
		var messageId = String(year) + '-' + String(month + 1).padStart(2, '0') + tenant.group
		var messageItem = {
			id: messageId,
			group: tenant.group,
			messageTenantId: tenant.id,
			createdAt: new Date().toISOString(),
			updatedAt: new Date().toISOString(),
			isReadS: false,
			subject: subject,
			bodyText: 'Below is your invoice for ' + months[month] + ' ' + year,
			bodyHtml: generateEmailTemplate(header, invoiceHtml, tenant.companyName),
			channelType: 'EMAIL',
			messageType: 'invoice',
			destinationEmail: tenant.stripeBillingEmail
		}

		var createParams = {
			TableName: process.env.API_HERA_MESSAGETABLE_NAME,
			Item: messageItem
		};

		let messageQuery = await createDynamoRecord(createParams);
		console.log('message query details', messageQuery)
	}else{
		console.log('no tenant billing email, no email sent')
	}
}

function queryDynamoRecords(params) {
	return new Promise((resolve, reject) => {
		// Create DynamoDB service object
		ddb.query(params, function (err, data) {
			if (err) {
				console.error("Unable to get items. Error JSON:", JSON.stringify(err, null, 2));
				reject(err)
			} else {
				console.log("Get Item succeeded:", JSON.stringify(data, null, 2));
				resolve(data)
			}
		});
	})
}

function updateDynamoRecord(params) {
	return new Promise((resolve, reject) => {
		ddb.update(params, function (err, data) {
			if (err) {
				console.log("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
				reject(err)
			} else {
				console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
				resolve(data)
			}
		});
	})

}

function createDynamoRecord(params) {
	console.log("Creating Record");
	return new Promise((resolve, reject) => {
		ddb.put(params, function (err, data) {
			if (err) {
				console.log("Unable to put item. Error JSON:", JSON.stringify(err, null, 2));
				reject(err)
			} else {
				console.log("PutItem succeeded:", JSON.stringify(data, null, 2));
				resolve(data)
			}
		});
	})
}