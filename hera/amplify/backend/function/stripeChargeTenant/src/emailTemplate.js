function generateEmailTemplate(header, body, companyName){
    let sent = new Date().toLocaleString();

    return `<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'> 
    <html xmlns='http://www.w3.org/1999/xhtml' style='margin: 0;padding: 0;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> 
    
    <head style='margin: 0;padding: 0;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> 
        <meta name='viewport' content='width=device-width' style='margin: 0;padding: 0;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> 
        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' style='margin: 0;padding: 0;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> 
        <title style='margin: 0;padding: 0;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'>${header}</title> 
    </head> 
    
    <body style='margin: 0;padding: 0;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;-webkit-font-smoothing: antialiased;-webkit-text-size-adjust: none;height: 100%;line-height: 1.6;background-color: #f6f6f6;width: 100% !important;'> 
        <table class='body-wrap' style='margin: 0;padding: 0;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;background-color: #f6f6f6;width: 100%;'> 
            <tr style='margin: 0;padding: 0;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> 
                <td style='margin: 0;padding: 0;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;'></td> 
                <td class='container' width='600' style='margin: 0 auto !important;padding: 0;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;display: block !important;max-width: 600px !important;clear: both !important;'> 
                    <div class='content' style='margin: 0 auto;padding: 20px;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;max-width: 600px;display: block;'> 
                        <table class='main' width='100%' cellpadding='0' cellspacing='0' style='margin: 0;padding: 0;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;background: #fff;border: 1px solid #e9e9e9;border-radius: 3px;'> 
                            <tr style='margin: 0;padding: 0;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> 
                                <td class='content-wrap aligncenter' style='margin: 0;padding: 20px;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;text-align: center;'> 
                                    <table width='100%' cellpadding='0' cellspacing='0' style='margin: 0;padding: 0;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> 
                                        <tr style='margin: 0;padding: 0;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> 
                                            <td class='content-block' style='margin: 0;padding: 0 0 20px;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;'> 
                                                <img style="display: inline-block; width: 200px;" src='https://dsp.herasolutions.app/img/logo-no-stroke.afacc59e.png'> 
                                                <p><a href='https://herasolutions.app'>herasolutions.app</a></p>
                                            </td> 
                                        </tr> 
                                        <tr style='margin: 0;padding: 0;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> 
                                            <td class='content-block' style='margin: 0;padding: 0 0 20px;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;'> 
                                                <h2 style='margin: 40px 0 0;padding: 0;font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;box-sizing: border-box;font-size: 24px;color: #000;line-height: 1.2;font-weight: 400;'>${header}</h2> 
                                            </td> 
                                        </tr> 
                                        <tr style='margin: 0;padding: 0;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> 
                                            <td class='content-block' style='margin: 0;padding: 0 0 20px;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;'> 
                                                <table class='invoice' style='margin: 40px auto;padding: 0;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;text-align: left;width: 80%;'> 
                                                    <tr style='margin: 0;padding: 0;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> 
                                                        <td style='margin: 0;padding: 5px 0;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;'> 
                                                            ${body} 
                                                        </td> 
                                                    </tr> 
                                                </table> 
                                            </td> 
                                        </tr> 
                                        <tr style='margin: 0;padding: 0;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> 
    
                                        </tr> 
                                        <tr style='margin: 0;padding: 0;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> 
                                            <td class='content-block' style='margin: 0;padding: 0 0 20px;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;'> 
                                                Sent ${sent} UTC
                                            </td> 
                                        </tr> 
                                    </table> 
                                </td> 
                            </tr> 
                        </table> 
                        <div class='footer' style='margin: 0;padding: 20px;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;width: 100%;clear: both;color: #999;'> 
                            <table width='100%' style='margin: 0;padding: 0;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> 
                                <tr style='margin: 0;padding: 0;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;'> 
                                    <td class='aligncenter content-block' style='margin: 0;padding: 0 0 20px;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 12px;vertical-align: top;text-align: center;'>Powered by Hera</td> 
                                </tr>
                            </table> 
                        </div> 
                    </div> 
                </td> 
                <td style='margin: 0;padding: 0;font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;vertical-align: top;'></td> 
            </tr> 
        </table> 
    
    </body> 
    
    </html>`
}

module.exports = {generateEmailTemplate};
