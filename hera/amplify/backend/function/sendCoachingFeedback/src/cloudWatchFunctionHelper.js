const { pushToCloudWatch } =  require("./cloudWatchLogger")

const metricDimension = process.env.ENV

async function setCloudWatchMetrics(metrics, cwm) {
  
  let { 
    functionName,
    codeText,
    codeNumber,
    tenant,
    err
  } = metrics

	const loggerBody = {
	'Title': `[${functionName}] Error in function ${functionName}.`,
	'Function': `${functionName}`,
	'Severity': 'Fatal',
  'Tenant': tenant.companyName,
  'Group': tenant.group,
	'Error': err
	}

	const errorCode = `${codeText}_SEND_COACHING_FEEDBACK (${codeNumber})`

	const body = {
			ErrorCode: errorCode,
			...loggerBody,
			'Description': `${errorCode} was due to the following error: ${err}`
	}

	pushToCloudWatch.error(body)
	cwm.pendingMessage.metricGroup1({
			byError: metricDimension
	}).pushMetricData({
			MetricName: errorCode,
			Value: 1,
			Unit: 'Count'
	})

	cwm.pendingMessage.metricGroup2({
			bySeverity: metricDimension
	}).pushMetricData({
			MetricName: 'Fatal',
			Value: 1,
			Unit: 'Count'
	})

	await cwm.sendToCloudWatch()
}

module.exports = {
  setCloudWatchMetrics
}