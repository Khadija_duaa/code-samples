/* Amplify Params - DO NOT EDIT
	API_HERA_DAILYROSTERTABLE_ARN
	API_HERA_DAILYROSTERTABLE_NAME
	API_HERA_GRAPHQLAPIENDPOINTOUTPUT
	API_HERA_GRAPHQLAPIIDOUTPUT
	API_HERA_MESSAGETABLE_ARN
	API_HERA_MESSAGETABLE_NAME
	API_HERA_PENDINGMESSAGETABLE_ARN
	API_HERA_PENDINGMESSAGETABLE_NAME
	API_HERA_PODQUALITYTABLE_ARN
	API_HERA_PODQUALITYTABLE_NAME
	API_HERA_REPLACEBYROUTETABLE_ARN
	API_HERA_REPLACEBYROUTETABLE_NAME
	API_HERA_ROUTETABLE_ARN
	API_HERA_ROUTETABLE_NAME
	API_HERA_STAFFCXFEEDBACKTABLE_ARN
	API_HERA_STAFFCXFEEDBACKTABLE_NAME
	API_HERA_STAFFMENTORTABLE_ARN
	API_HERA_STAFFMENTORTABLE_NAME
	API_HERA_STAFFSCORECARDTABLE_ARN
	API_HERA_STAFFSCORECARDTABLE_NAME
	API_HERA_STAFFTABLE_ARN
	API_HERA_STAFFTABLE_NAME
	API_HERA_TENANTTABLE_ARN
	API_HERA_TENANTTABLE_NAME
	ENV
	REGION
Amplify Params - DO NOT EDIT */
const { generateTemplate } = require('./emailTemplate')
const {
    createDynamoRecord,
    getDynamoRecord,
    deleteDynamoRecord,
} = require("./dynamodbHelper.js");

const {
    getAllTenants,
    getPendingMessages,
    getDailyRosters,
    getRoutes,
    getReplacementsByRoutes
} = require("./getDatahelpers")

/**CloudWatch Loggers and Metricts */
const { setCloudWatchMetrics } = require('./cloudWatchFunctionHelper')

const { CWMetrics } = require('./cloudWatchMetricHelper')
const cwm = new CWMetrics(process.env.METRIC_GROUP_NAME)

// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
// Set the region
AWS.config.update({region: process.env.REGION});

var moment = require('moment-timezone')

exports.handler = async (event) => {
    console.log(JSON.stringify(event, null, 2));   

    // set up time vars
    var today = new Date();
    var todayMoment = moment(today).format('YYYY-MM-DD')
    console.log({ todayMoment })
    
    // Find tenants configured to send messages at the current time
    var tenants = await getAllTenants();
    
    // Filters Tenants by Automated Coaching Send Time, Time Zone and Plan
    var filteredTenants = await filterTenantsByCoachingTimeAndTz(tenants)
    console.log({ filteredTenants });
    console.log({ length: filteredTenants.length });

    if(!filteredTenants.length) return

    await sendCoachingFeedback(filteredTenants, todayMoment)

	await cwm.sendToCloudWatch()
}

async function filterTenantsByCoachingTimeAndTz(tenants) {
    let results = []
    // loop through found tenants
    for (const tenant of tenants){
        // Tenant's company Name
        var { companyName, group }= tenant

        if(!tenant.timeZone){
            console.log(companyName + "'s messages skipped because Time Zone is not selected.");
            continue
        }

        if(!tenant.automatedCoachingSendTime){
            console.log(companyName + "'s messages skipped because Coaching Send Time is not selected.");
            continue
        }

        // compare tenant time zone and coaching send time Local
        var tenantTz = moment.tz(tenant.timeZone).format("HHmm")
        var tenantCoachingSendTime = moment(tenant.automatedCoachingSendTime, ["hh:mm:ss"]).format('HHmm')
        
        console.log({ companyName, group, currentTenantTz: tenantTz, tenantTimeZone: tenant.timeZone, automatedCoachingSendTime: tenant.automatedCoachingSendTime })

        if(parseInt(tenantTz) != parseInt(tenantCoachingSendTime)) continue
        
        var accountPremiumStatus = tenant.accountPremiumStatus
        if(!accountPremiumStatus) accountPremiumStatus = []
        var accountIsTrial = accountPremiumStatus.includes('trial')
        var accountHasPremiumCoaching = accountPremiumStatus.includes('performance') || accountPremiumStatus.includes('bundle')
        var trialExpDate = new Date(tenant.trialExpDate).getTime();

        // skip iteration if account doesn't have premium performance coaching or an active trial
        if((accountIsTrial && trialExpDate < new Date().getTime()) || (!accountIsTrial && !accountHasPremiumCoaching)){
            console.log(companyName + "'s messages skipped because account doesn't have premium performance coaching or an active trial.");
            continue
        }

        // Adding Tenants after all filters
        results.push(tenant)
    }
    return results
}

async function sendCoachingFeedback(tenants, todayMoment) {
    
    // loop through found tenants
    for (const tenant of tenants){
        // Tenant's company Name
        var { companyName, group }= tenant

        console.log(`Sending messages for "${companyName}"`)

        /**
         * NEW - PHASE 2 WITH PENDING MESSAGE TABLE
         */

        // get all pending messages
        try {
            var pendingMessages = await getPendingMessages(group)
            console.log({ totalPendingMessages: pendingMessages.length });
        } catch (err) {
            console.log(`[getPendingMessages] Error in function getPendingMessages for Tenant: ${companyName} and group: ${group}`, err);
            let metrics = {
                functionName: 'getPendingMessages',
                codeText: 'ERROR_003',
                codeNumber: '003.001',
                tenant,
                err
            }
            setCloudWatchMetrics(metrics, cwm)
            continue
        }

        // only create message if DA is rostered
        try {
            var dailyRosters = await getDailyRosters(group, todayMoment)

            if(dailyRosters.length == 0){
                console.log("No daily roster found, skipping tenant " + companyName)
                continue
            }
    
            // Daily Roster ID
            var dailyRosterId = dailyRosters[0].id

        } catch (err) {
            console.log(`[getDailyRosters] Error in function getDailyRosters for Tenant: ${companyName} and group: ${group}`, err);
            let metrics = {
                functionName: 'getDailyRosters',
                codeText: 'ERROR_003',
                codeNumber: '003.002',
                tenant,
                err
            }
            setCloudWatchMetrics(metrics, cwm)
            continue
        }

        var routeStaff = []
        const excludedGroup = [
            'Called Out',
            'No Show, No Call',
            'VTO'
        ]

        // get daily roster id from the response
        try {
            var routes = await getRoutes(dailyRosterId)
            // Filter the routes with status different that the ones above
            routes.map((route) => {
                if(!excludedGroup.includes(route.status)) {
                    routeStaff.push(route.routeStaffId)
                }
            })

            if (routes.length == 0){
                console.log("No routes found, skipping tenant " + companyName)
                continue 
            }

        } catch (err) {
            console.log(`[getRoutes] Error in function getRoutes for Tenant: ${companyName} and group: ${group}`, err);
            let metrics = {
                functionName: 'getRoutes',
                codeText: 'ERROR_003',
                codeNumber: '003.003',
                tenant,
                err
            }
            setCloudWatchMetrics(metrics, cwm)
            continue
        }

        // // get replacements daily roster id from the response
        try {
            var replacementsByRoutes = await getReplacementsByRoutes(dailyRosterId)

            // throw 'ERROR getRoutes'
            replacementsByRoutes.map((replacement) => {
                if(!excludedGroup.includes(replacement.status)) {
                    routeStaff.push(replacement.replaceByRouteStaffId)
                }
            })

            if (!routeStaff.length){
                console.log("No routes with staff found, skipping tenant " + companyName)
                continue 
            }
        } catch (err) {
            console.log(`[getReplacementsByRoutes] Error in function getReplacementsByRoutes for Tenant: ${companyName} and group: ${group}`, err);
            let metrics = {
                functionName: 'getReplacementsByRoutes',
                codeText: 'ERROR_003',
                codeNumber: '003.004',
                tenant,
                err
            }
            setCloudWatchMetrics(metrics, cwm)
            continue
        }

        // Route Staff
        console.log({ routeStaff });
        
        for (const pendingMessage of pendingMessages){

            try {
                let params = {
                      TableName : process.env.API_HERA_STAFFTABLE_NAME,
                      Key: {
                        "id": pendingMessage.pendingMessageStaffId ? pendingMessage.pendingMessageStaffId : pendingMessage.staffId
                      }
                };
                var staff = await getDynamoRecord(params)
            } catch (err) {
                console.log(`[getDynamoRecord - Staff] Error in function getDynamoRecord - Staff for Tenant: ${companyName} and group: ${group}`, err);
                let metrics = {
                    functionName: 'getDynamoRecord - Staff',
                    codeText: 'ERROR_003',
                    codeNumber: '003.005',
                    tenant,
                    err
                }
                setCloudWatchMetrics(metrics, cwm)
                continue
            }
            
            if(JSON.stringify(staff) == "{}" || staff.Item.status.toLowerCase() != "active"){
                console.log("Deleting pending message if staff is differente than Active, without creating message")
                console.log({ pendingMesagesDeletedFromStaff: staff })
                // delete pending message
                try {
                    let params = {
                        Key:{
                            "id": pendingMessage.id
                        },
                        TableName: process.env.API_HERA_PENDINGMESSAGETABLE_NAME,
                    };
                    await deleteDynamoRecord(params);
                } catch (err) {
                    console.log(`[deleteDynamoRecord - PendingMessage]  Error in deleting pending message without creating message for Tenant: ${companyName} and group: ${group}`, err);
                    let metrics = {
                        functionName: 'deleteDynamoRecord - PendingMessage',
                        codeText: 'ERROR_003',
                        codeNumber: '003.006',
                        tenant,
                        err
                    }
                    setCloudWatchMetrics(metrics, cwm)                 
                }
                //skip rest of iteration
                continue
            }
            
            // if it's included, do the following
            if(routeStaff.includes(pendingMessage.pendingMessageStaffId) || routeStaff.includes(pendingMessage.staffId)){
                console.log({ id: staff.Item.id , creatingMessagesFor: `${staff.Item.firstName} ${staff.Item.lastName}` })
                 // create messages from pending messages
                var body = `${pendingMessage.bodyTextCO ? pendingMessage.bodyTextCO + "\n\n" : '' } ${pendingMessage.bodyTextPR ? pendingMessage.bodyTextPR : ''}`
                var emailBody = body.replace(/\n/g, '<br>')
                var header = "Your Daily Performance Feedback"
                var subject = "Your Daily Performance Feedback"
                var channelType
                if(staff.Item.receiveTextMessages && staff.Item.receiveEmailMessages) channelType = 'SMS/EMAIL';
                else if( staff.Item.receiveTextMessages ) channelType = 'SMS';
                else if( staff.Item.receiveEmailMessages ) channelType = 'EMAIL';
                else channelType = ''
                var createdAt = new Date().toISOString()

                // Creating Message
                try {
                    var createParams = {
                        TableName: process.env.API_HERA_MESSAGETABLE_NAME,
                        Item: {
                            id: tenant.id + todayMoment + (pendingMessage.pendingMessageStaffId ? pendingMessage.pendingMessageStaffId : pendingMessage.staffId),
                            messageTenantId: tenant.id,
                            staffId: (pendingMessage.pendingMessageStaffId ? pendingMessage.pendingMessageStaffId : pendingMessage.staffId),
                            createdAt: createdAt,
                            updatedAt: new Date().toISOString(),
                            group: group,
                            subject: subject,
                            bodyText: body,
                            bodyHtml: generateTemplate(header, emailBody, pendingMessage.pendingMessageStaffId ? pendingMessage.pendingMessageStaffId : pendingMessage.staffId, companyName),
                            channelType: channelType,
                            messageType: 'coaching',
                            isReadS: 'true',
                            'messageType#createdAt': 'coaching#' + createdAt,
                            'channelType#createdAt': channelType + '#' + createdAt,
                            'channelType#isReadS#createdAt': channelType + '#true#' + createdAt,
                            destinationNumber: staff.Item.phone,
                            // destinationNumber: '3475938464',
                            // destinationNumber: '7656984731',
                            // destinationNumber: '3176490422',
                            destinationEmail: staff.Item.email
                            // destinationEmail: 'heratest@junotransit.com'
                            // destinationEmail: 'akaiser@dbservices.com'
                            // destinationEmail: 'ddrake@dbservices.com'
                        }
                    };
                    await createDynamoRecord(createParams);
                } catch (err) {
                    console.log(`[createDynamoRecord - Message]  Error in creating messages from pending messages, for Tenant: ${companyName} and group: ${group}`, err);
                    let metrics = {
                        functionName: 'createDynamoRecord - Message',
                        codeText: 'ERROR_003',
                        codeNumber: '003.007',
                        tenant,
                        err
                    }
                    setCloudWatchMetrics(metrics, cwm)
                    continue 
                }

                // Delete Pending Message
                try {
                    let params = {
                        Key:{
                            "id": pendingMessage.id
                        },
                        TableName: process.env.API_HERA_PENDINGMESSAGETABLE_NAME,
                    };
                    await deleteDynamoRecord(params);

                } catch (err) {
                    console.log(`[deleteDynamoRecord - Pending Message]  Error in deleting pending messages, for Tenant: ${companyName} and group: ${group}`, err);
                    let metrics = {
                        functionName: 'deleteDynamoRecord - Pending Message',
                        codeText: 'ERROR_003',
                        codeNumber: '003.008',
                        tenant,
                        err
                    }
                    setCloudWatchMetrics(metrics, cwm)
                    continue 
                }
            }
         }
        
    }
}