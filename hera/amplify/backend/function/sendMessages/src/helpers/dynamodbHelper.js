const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient();

function createDynamoRecord(params) {
    return new Promise((resolve, reject) => {
        ddb.put(params, function(err, data) {
            if (err) {
                console.error("Unable to create item. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                console.log("Create item succeeded:", JSON.stringify(data, null, 2));
                resolve(data)
            }
        });
    });
}

function getDynamoRecord(params){
    return new Promise((resolve, reject) => {
        ddb.get(params, function(err, data) {
            if (err) {
                  console.error("Unable to get item. Error JSON:", JSON.stringify(err, null, 2));
                  reject(err)
              } else {
                  console.log("Get Item succeeded:", JSON.stringify(data, null, 2));
                  resolve(data)
              }
        });
    });
}


function updateDynamoRecord(params) {
    return new Promise((resolve, reject) => {
        ddb.update(params, function(err, data) {
            if (err) {
                console.log("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                console.log("Update Item succeeded:", JSON.stringify(data, null, 2));
                resolve(data)
            }
        });
    })

}

function queryDynamoRecords(params) {
    return new Promise((resolve, reject) => {
        // Create DynamoDB service object
        ddb.query(params, function(err, data) {
            if (err) {
                console.error("Unable to query items. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                resolve(data)
            }
        });
    });
}

module.exports = {
    createDynamoRecord,
    getDynamoRecord,
    queryDynamoRecords,
    updateDynamoRecord
};