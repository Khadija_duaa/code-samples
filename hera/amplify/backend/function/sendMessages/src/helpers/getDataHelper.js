const { queryDynamoRecords, getDynamoRecord } = require("./dynamodbHelper");

/**
 * Get daily roster by id
 * @param {*} id 
 * @returns daily roster data
 */
async function getDailyRosterById(id){
    let dailyRosterData = {}
    try {
        const params = {
            TableName : process.env.API_HERA_DAILYROSTERTABLE_NAME,
            Key: {
                "id": id
            },
            ExpressionAttributeNames: {
                '#g': 'group',
            },
            ProjectionExpression: "id, #g, notesDate, standUpNotes",
        }
        const response = await getDynamoRecord(params)
        dailyRosterData = response.Item
    } catch (error) {
        console.log("Error in getDailyRosterById function", error)
    } finally {
        return dailyRosterData
    }
}

/**
 * It queries the DynamoDB Route table for all records that match the routeDailyRosterId. 
 * 
 * @param id - the routeDailyRosterId (is id of the Daily Roster)
 * @returns Routes -> return example:
 * [
 *   {
 *     "id": "xxxxxxxx-xxxx-...",
 *     "routeStaffId": "xxxxxxxx-xxxx-...", --> id of the staff
 *     "status": "Called Out",
 *     "isNotActive": false,
 *     "receivedAnnouncement": false,
 *     "routeHelperId": "xxxxxxxx-xxxx-...", --> id of the staff helper
 *     "helperStatus": "On Time"
 *   }
 *  ]
 */
async function getRoutesByDailyRosterId(id){
    let routes = [];
    try {
        let lastEvaluatedKey = null;
        do{
            let params = {
                TableName: process.env.API_HERA_ROUTETABLE_NAME,
                IndexName: 'gsi-RouteDailyRoster',
                ExpressionAttributeNames: {
                    '#dailyRosterId': 'routeDailyRosterId',
                    '#s': 'status'
                },
                ExpressionAttributeValues: {
                    ':dailyRosterId': id,
                },
                KeyConditionExpression: '#dailyRosterId = :dailyRosterId',
                ProjectionExpression: 'id, routeStaffId, #s, isNotActive, receivedAnnouncement, routeHelperId, helperStatus',
                ExclusiveStartKey: lastEvaluatedKey
            };
            const queryResult = await queryDynamoRecords(params);
            lastEvaluatedKey = queryResult.LastEvaluatedKey;
            routes = routes.concat(queryResult.Items);
        }while(lastEvaluatedKey)
        
    } catch (err) {
        console.log("Error in getRoutesByDailyRosterId function", err);
    } finally {
        return routes;
    }
}

/**
 * It queries the DynamoDB ReplaceByRoute table for all records that match 
 * the replaceByRouteDailyRosterId. 
 * @param id - the id of the Route
 * @returns Replacement of Routes -> return example:
 *  [
 *   {
 *     "id": "xxxxxxxx-xxxx-..."
 *     "replaceByRouteStaffId": "xxxxxxxx-xxxx-..", --> id of the staff
 *     "status": "Called Out",
 *     "isNotActive": false,
 *     "replaceByRouteRouteId": "xxxxxxxx-xxxx-...", --> id of the route replaced
 *     "receivedAnnouncement": false
 *   },
 *  ]
 */
async function getReplacementsByDailyRosterId(id){
    let replacementByRoutes = [];
    try {
        let lastEvaluatedKey = null;
        do{
            let params = {
                TableName: process.env.API_HERA_REPLACEBYROUTETABLE_NAME,
                IndexName: 'gsi-ReplaceRouteDailyRoster',   
                ExpressionAttributeValues: {
                    ':dailyRosterId': id,
                },
                ExpressionAttributeNames: {
                    '#dailyRosterId': 'replaceByRouteDailyRosterId',
                    '#s': 'status',
                },
                KeyConditionExpression: '#dailyRosterId = :dailyRosterId',
                ProjectionExpression: 'id, #s, #dailyRosterId, replaceByRouteRouteId, replaceByRouteStaffId, isNotActive, receivedAnnouncement',
                ExclusiveStartKey: lastEvaluatedKey
            };
            const queryResult = await queryDynamoRecords(params);
            lastEvaluatedKey = queryResult.LastEvaluatedKey;
            replacementByRoutes = replacementByRoutes.concat(queryResult.Items);
        }while(lastEvaluatedKey)
  
    } catch (err) {
        console.log("Error in getReplacementsByDailyRosterId function", err);
    } finally {
        return replacementByRoutes;
    }
}
  

module.exports = {
    getDailyRosterById,
    getRoutesByDailyRosterId,
    getReplacementsByDailyRosterId,
}