const AWS = require('aws-sdk');
const {
    getDailyRosterById,
    getRoutesByDailyRosterId,
    getReplacementsByDailyRosterId,
    
} = require('../helpers/getDataHelper');

const {
    createDynamoRecord,
    getDynamoRecord, 
    updateDynamoRecord
} = require("../helpers/dynamodbHelper");

const { generateTemplate } = require('../templates/emailTemplate');
const { nanoid } = require('nanoid');
const { ShortenUrlGenerator } = require('/opt/nodejs/short-url');

/**
 * It gets the daily roster for a given date, gets the route staff for that daily roster, 
 * then calls the function that sends the stand up announcements to the route staff.
 * 
 * @param data - { userInfo, notesDate, formattedNotesDate, attachmentId, sendToAll }
 * @returns returns the same as the function sendMessagesAnnouncements
 */
async function sendStandUpAnnouncements(data){
    try {
        const { dailyRosterId } = data;

        let result = {
            numSent: 0,
            numError: 0,
            numUpdated: 0,
            numGenerated: 0
        }; 
        if(!dailyRosterId) return result;
  
        //get daily roster data
        const dailyRosterData = await getDailyRosterById(dailyRosterId)
        if(!dailyRosterData.id) return result;

        const announcement = dailyRosterData.standUpNotes;
        const routes = await getActiveRoute(dailyRosterData.id);

        const params = {
            data,
            announcement,
            routes
        }
        result = await sendMessagesAnnouncements(params);
        return result;
        
    } catch (e) {
        console.log('[sendStandUpAnnouncements] Error Sending Messages function', e);
        throw e.showNotification ? e : { message: e.message };
    }
}

/**
 * Gets Routes and ReplaceByRoutes data with Daily Roster id
 * 
 * @param id - Daily Roster id
 * @returns  returns an array of filterd objects containing the idRoute, idStaff and receivedAnnouncement.
 */
async function getActiveRoute(dailyRosterId){
   
    const excludedGroup = [
        'Called Out',
        'No Show, No Call',
        'VTO'
    ];

    let activeRoutes = [];
    let idOfAllRoutes = [];
    const routes = await getRoutesByDailyRosterId(dailyRosterId);
   
    routes.forEach((route) => {
        if(route.routeStaffId && !excludedGroup.includes(route.status) && !route.isNotActive) {
            activeRoutes.push({
                routeId: route.id,
                staffId: route.routeStaffId,
                receivedAnnouncement: route.receivedAnnouncement,
                isReplacement: false,
                replacementId: null
            });
            //add helpers
            if(route.routeHelperId && !excludedGroup.includes(route.helperStatus)){
                activeRoutes.push({
                    routeId: route.id,
                    staffId: route.routeHelperId,
                    receivedAnnouncement: route.receivedAnnouncement,
                    isReplacement: false,
                    replacementId: null
                });
            }
        }
        idOfAllRoutes.push(route.id)
    });

    const replacementsByRoutes = await getReplacementsByDailyRosterId(dailyRosterId);
    replacementsByRoutes.forEach((replacement) => {
        if(replacement.replaceByRouteStaffId && !excludedGroup.includes(replacement.status) && !replacement.isNotActive && idOfAllRoutes.includes(replacement.replaceByRouteRouteId)) {
            activeRoutes.push({
                routeId: replacement.replaceByRouteRouteId,
                staffId: replacement.replaceByRouteStaffId,
                receivedAnnouncement: replacement.receivedAnnouncement,
                isReplacement: true,
                replacementId: replacement.id
            });
        }
    });
    return activeRoutes;
}


/**
 * Receives parameters that contain ids of the staff (routeStaff), announcement,
 * data (userInfo, formattedNotesDate, attachmentId, sendToAll) and sends them an announcement
 * @param params {data, announcement, routeStaff }, routeStaff is array of ids of the staff
 * @returns number of messages sent, number of errors and number of records updated after sending messages {numSent, numError, numUpdated}
 */
async function sendMessagesAnnouncements(params){
    const { data, announcement, routes } = params;
    const { userInfo, formattedNotesDate, attachmentId, sendToAll, withShortenUrl, expirationDate, notificationPreferencesBaseUrl } =  data;
    const { tenant } = userInfo;
    const { companyName, group } = tenant;
    
   
    let result = {
        numSent: 0,
        numError: 0,
        numUpdated: 0,
        numGenerated: 0
    };
    let filterdRoutes = routes;
   
    
    if(!sendToAll){
        filterdRoutes = filterdRoutes.filter(route => route.receivedAnnouncement != true);
    }
   
    for(const route of filterdRoutes){
        let staff = {};
        //get staff info
        if (!route.staffId)
            continue;
        try{
            let params = {
                TableName : process.env.API_HERA_STAFFTABLE_NAME,
                Key: {
                    "id": route.staffId
                },
                ProjectionExpression: 'id, firstName, lastName, receiveTextMessages, receiveEmailMessages, phone, email',
            };
            staff = await getDynamoRecord(params);
        }catch (e){
            console.log(`[getDynamoRecord-Staff Table] Error when getting Staff Info from DB - Tenant's group ${group}, Staff id: ${route.staffId} `, e);
            result['numError']++;
            continue;
        }
        
        //message body
        const header = `Announcements for ${formattedNotesDate}`;
        const subject = `Announcements for ${formattedNotesDate}`;
        const messageType = 'standUpAnnouncements';
        const bodyText = announcement;
        const body = announcement;
        const messageRouteId = route.routeId;
        
        let channelType;
        if( staff.Item.receiveTextMessages && staff.Item.receiveEmailMessages ) channelType = 'SMS/EMAIL';
        else if( staff.Item.receiveTextMessages ) channelType = 'SMS';
        else if( staff.Item.receiveEmailMessages ) channelType = 'EMAIL';
        else channelType = '';
        
        const createdAt = new Date().toISOString();
        const updatedAt = new Date().toISOString();
        const senderName= `${userInfo.firstName} ${userInfo.lastName}`;
        const destinationName = `${staff.Item.firstName} ${staff.Item.lastName}`;
       
        
        try{
            //parameters to create message
            let createParams = {
                TableName: process.env.API_HERA_MESSAGETABLE_NAME,
                Item: {
                    id: nanoid(),
                    __typename: 'Message',
                    staffId: staff.Item.id,
                    createdAt,
                    updatedAt,
                    group,
                    subject,
                    bodyText,
                    bodyHtml: generateTemplate(header, body, staff.Item.id, companyName, notificationPreferencesBaseUrl),
                    channelType,
                    messageType,
                    isReadS: 'false',
                    'messageType#createdAt': messageType + '#' + createdAt,
                    'channelType#createdAt': channelType + '#' + createdAt,
                    'channelType#isReadS#createdAt': channelType + '#false#' + createdAt,
                    destinationNumber: staff.Item.phone,
                    destinationEmail: staff.Item.email,
                    destinationName,
                    owner: userInfo.cognitoSub,
                    senderName,
                    senderId: userInfo.id,
                    messageTenantId: tenant.id,
                    messageRouteId,
                }
            };

            if(route.isReplacement && route.replacementId){
                //is Replacement
                createParams.Item.messageReplaceByRouteId = route.replacementId;
            }

            if(attachmentId) {
                createParams.Item.messageAttachmentId = attachmentId;
            }
            //create message
            await createDynamoRecord(createParams);
            result['numSent']++;

            //create shorten url
            if(attachmentId && withShortenUrl){
                
                 const vars = {
                    SHORTEN_URL_DOMAIN: process.env.SHORTEN_URL_DOMAIN,
                    API_HERA_GRAPHQLAPIENDPOINTOUTPUT: process.env.API_HERA_GRAPHQLAPIENDPOINTOUTPUT,
                    REGION: process.env.REGION
                }

                const generator = new ShortenUrlGenerator(vars, AWS.config);
                
                const input = {group,attachmentId,expirationDate};

                if(result.numGenerated === 0){
                
                    const shortenUrl = await generator.generateShortenUrl(input);

                    if(!shortenUrl.response) {
                        throw 'The shortened url was not generated.' 
                    }else{
                        result.numGenerated++
                    } 
                       
                }
            }
            
            //upadate Staff
            let updateParamsStaff = {
                TableName : process.env.API_HERA_STAFFTABLE_NAME,
                Key: {
                    "id": staff.Item.id
                },
                ExpressionAttributeNames: {
                    '#smsLast': 'smsLastMessage',
                    '#smsLastTime': 'smsLastMessageTimestamp',
                },
                ExpressionAttributeValues: {
                    ':smsLast': bodyText,
                    ':smsLastTime': updatedAt
                },
                UpdateExpression: 'set #smsLast = :smsLast, #smsLastTime = :smsLastTime'
            };
            await updateDynamoRecord(updateParamsStaff);
            
            //update Route or ReplaceByRoute
            let updateParams = {
                ExpressionAttributeNames: {
                    '#ra': 'receivedAnnouncement',
                },
                ExpressionAttributeValues: {
                    ':ra': true,
                },
                UpdateExpression: 'set #ra = :ra'
            }
            if(route.isReplacement && route.replacementId){
                //update ReplaceByRoute
                updateParams.TableName = process.env.API_HERA_REPLACEBYROUTETABLE_NAME
                updateParams.Key = {
                    "id": route.replacementId
                }
                await updateDynamoRecord(updateParams);
                result['numUpdated']++;
            }
            //update Route
            updateParams.TableName = process.env.API_HERA_ROUTETABLE_NAME
            updateParams.Key = {
                "id": route.routeId
            }
            await updateDynamoRecord(updateParams);
            result['numUpdated']++;
        }catch(e){
            console.log('[createDynamoRecord/updateDynamoRecord] ', e);
            result['numError']++;
        }
      
    }
    
    return result;
}


module.exports = {
    sendStandUpAnnouncements
}