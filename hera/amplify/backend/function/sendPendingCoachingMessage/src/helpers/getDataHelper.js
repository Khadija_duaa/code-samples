
const {
    createDynamoRecord,
    getDynamoRecord,
    scanDynamoRecords,
    queryDynamoRecords,
    deleteDynamoRecord,
  } = require("./dynamodbHelper.js");
    
  // Get Pending Messages
  async function getPendingMessages(group) {
    try {
        let pendingMessages = [];
        let lastEvaluatedKey = null
        do{
            let params = {
                ExpressionAttributeValues: {
                    ':g': group,
                },
                    ExpressionAttributeNames: {
                    '#g': 'group',
                },
                KeyConditionExpression: '#g = :g',
                ProjectionExpression: 'id,#g,channelType,destinationNumber,destinationEmail,bodyText,subject,bodyHtml,smsSendInformation,emailSendInformation,staffId, pendingMessageStaffId,bodyTextCO,bodyTextPR',
                TableName: process.env.API_HERA_PENDINGMESSAGETABLE_NAME,
                IndexName: 'byGroup',
                ExclusiveStartKey: lastEvaluatedKey
            };
            var queryResult = await queryDynamoRecords(params);
            var items = queryResult.Items
            lastEvaluatedKey = queryResult.LastEvaluatedKey
            pendingMessages = [...pendingMessages, ...items]
        }while(lastEvaluatedKey)
  
        return pendingMessages;
    } catch (err) {
        console.log("[getPendingMessages] Error in function getPendingMessages", err);
        throw err
    }
  }
  
  // Get Daily Rostered
  async function getDailyRosters(group, today) {
    console.log({ group, today });
    try {
        let dailyRosters = []
        let lastEvaluatedKey = null
        do{
            var params = {
                ExpressionAttributeValues: {
                ':g': group,
                ':d': today
                    
                },
                  ExpressionAttributeNames: {
                '#g': 'group',
                '#d': 'notesDate'
                },
                KeyConditionExpression: '#g = :g',
                FilterExpression: '#d = :d',
                ProjectionExpression: 'id',
                TableName: process.env.API_HERA_DAILYROSTERTABLE_NAME,
                IndexName: 'byGroup',
                ExclusiveStartKey: lastEvaluatedKey
            };
            var queryResult = await queryDynamoRecords(params);
            var items = queryResult.Items
            lastEvaluatedKey = queryResult.LastEvaluatedKey
            dailyRosters = [...dailyRosters, ...items]
        }while(lastEvaluatedKey)
  
        return dailyRosters;
    } catch (err) {
        console.log("[getDailyRosters] Error in function getDailyRosters", err);
        throw err
    }
  }
  
  // Get Routes
  async function getRoutes(id) {
    try {
        var routeStaff = []
        var lastEvaluatedKey = null
        do{
           
          let params = {
              ExpressionAttributeValues: {
                  ':g': id,
              },
              ExpressionAttributeNames: {
                  '#g': 'routeDailyRosterId',
                  '#s': 'status'
              },
              KeyConditionExpression: '#g = :g',
              ProjectionExpression: 'routeStaffId, #s',
              TableName: process.env.API_HERA_ROUTETABLE_NAME,
              IndexName: 'gsi-RouteDailyRoster',   
            }
            let queryResult = await queryDynamoRecords(params);
            let items = queryResult.Items
            lastEvaluatedKey = queryResult.LastEvaluatedKey
            routeStaff = [...routeStaff, ...items]
        }while(lastEvaluatedKey)
  
        return routeStaff;
    } catch (err) {
        console.log("[getRoutes] Error in function getRoutes", err);
        throw err
    }
  }
  
  // Get Replacement by Routes
  async function getReplacementsByRoutes(id) {
    try {
        var replacementByRoutes = []
        var lastEvaluatedKey = null
        do{
  
          let params = {
              ExpressionAttributeValues: {
                  ':g': id,
              },
                  ExpressionAttributeNames: {
                  '#g': 'replaceByRouteDailyRosterId',
                  '#s': 'status'
              },
              KeyConditionExpression: '#g = :g',
              ProjectionExpression: 'replaceByRouteStaffId, #s',
              TableName: process.env.API_HERA_REPLACEBYROUTETABLE_NAME,
              IndexName: 'gsi-ReplaceRouteDailyRoster',   
          }
          
            let queryResult = await queryDynamoRecords(params);
            let items = queryResult.Items
            lastEvaluatedKey = queryResult.LastEvaluatedKey
            replacementByRoutes = [...replacementByRoutes, ...items]
        }while(lastEvaluatedKey)
  
        return replacementByRoutes;
    } catch (err) {
        console.log("[getReplacementsByRoutes] Error in function getReplacementsByRoutes", err);
        throw err
    }
  }
  
  
  module.exports = {
    getPendingMessages,
    getDailyRosters,
    getRoutes,
    getReplacementsByRoutes
  }