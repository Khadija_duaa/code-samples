
const {
    createDynamoRecord,
    getDynamoRecord,
    scanDynamoRecords,
    queryDynamoRecords,
    deleteDynamoRecord,
  } = require("./dynamodbHelper.js");

const {
    getPendingMessages,
    getDailyRosters,
    getRoutes,
    getReplacementsByRoutes
} = require('./getDataHelper')

const {
    generateTemplate
} = require('../templates/emailTemplate')

const {
    executeMutation
} = require('../appSync')

const { 
    updateStaff
} = require('./graphql')

var { nanoid } = require('nanoid')
const { updateDynamoRecord, buildUpdateParams } = require('./dynamodbHelper.js')

/**
 * Validating Data
 */
async function validateExistingPendingMessages(data) {
    try {
        const { userInfo , pendingMessages } = data

        // Get all Pending Messages by group
        let fetchedPendingMessages = await getPendingMessages(userInfo.tenant.group)

        // Compare values
        let comparizon = await compareLocalAndDynamoDBMessages(pendingMessages, fetchedPendingMessages)

        // Throw unsynchronized error if they are not equal
        if(!comparizon){
            throw {
                message: "Your Pending Messages are not synchronized. Please refresh the page with the button below to resolve this error.",
                showRefreshModal: true
            }
        }
        return 
    } catch (e) {
        console.log('[validateExistingPendingMessages] Error Validating Existing Pending Messages', e);
        throw e.showRefreshModal ? e : { message: e.message }
    }
}

/**
 * Comparing Local Data vs Fetched Data
 */
async function compareLocalAndDynamoDBMessages(localMessages, fetchedMessages) {
    let mapFetchedMessages = fetchedMessages.map(item => item.id)
    return await localMessages.every(item => mapFetchedMessages.includes(item));
}

/**
 * Sending Pending Messages
 */
async function sendPendingMessages(data){
    try {
        const { userInfo, radioSendTo, todayMoment } = data

        /**
         * 1. Daily Roster
         */
        let dailyRosters = await getDailyRosters(userInfo.tenant.group, todayMoment)

        if(!dailyRosters.length && radioSendTo == 'rostered') throw { showNotification: true, message: 'No Associates rostered today.' }

        /**
         * 2. Routes
        */
        var routeStaff = []
        if(dailyRosters.length) {

            let dailyRosterId = dailyRosters[0].id
            routeStaff = await getRouteStaff(dailyRosterId)
        }

        if(!routeStaff.length && radioSendTo == 'rostered') throw { showNotification: true, message: 'No Associates rostered today.' }

        /**
         * 3. Send Pending Messages
        */
        let input = {
            data,
            routeStaff
        }
        let results = await sendMessages(input)
        return results

    } catch (e) {
        console.log('[sendPendingMessages] Error Sending Messages function', e);
        throw e.showNotification ? e : { message: e.message }
    }
}

// Get Route Staff
async function getRouteStaff(id) {

    let routeStaff = []
    const excludedGroup = [
        'Called Out',
        'No Show, No Call',
        'VTO'
    ]
    var routes = await getRoutes(id)
    // Filter the routes with status different that the ones above
    routes.map((route) => {
        if(!excludedGroup.includes(route.status)) {
            routeStaff.push(route.routeStaffId)
        }
    })

    var replacementsByRoutes = await getReplacementsByRoutes(id)

    // Filter ReplaceBYROute staffs and status
    replacementsByRoutes.map((replacement) => {
        if(!excludedGroup.includes(replacement.status)) {
            routeStaff.push(replacement.replaceByRouteStaffId)
        }
    })
    return routeStaff
}

async function sendMessages(input) {

    const { data, routeStaff } = input
    const { pendingMessages, radioSendTo, userInfo } =  data
    const { tenant } = userInfo
    const { companyName, group } = tenant

    let result = {
        numSent: 0,
        numUnsubscribed: 0,
        numNotRostered: 0,
        numError: 0
    }

    for (const nmessageId of pendingMessages){

        // Getting Pending Message
        try {
            let params = {
                  TableName : process.env.API_HERA_PENDINGMESSAGETABLE_NAME,
                  Key: {
                    "id": nmessageId
                  }
            };
            var pendingMessage = await getDynamoRecord(params)
            pendingMessage = pendingMessage.Item
        } catch (err) {
            console.log(`[getDynamoRecord-Pending Message Table] Error when getting Pending Message from DB - Tenant's group ${group}, Pending Message id: ${nmessageId} `, err);
            result['numError']++
            continue
        }
        let staffId = pendingMessage.pendingMessageStaffId ? pendingMessage.pendingMessageStaffId : pendingMessage.staffId

        if(radioSendTo == 'rostered'){
            // check if DA is rostered today
            if(!routeStaff.includes(staffId)) {
                result['numNotRostered']++
                continue
            }   
        }

        // Getting Staff Info
        try {
            let params = {
                  TableName : process.env.API_HERA_STAFFTABLE_NAME,
                  Key: {
                    "id": staffId
                  }
            };
            var staff = await getDynamoRecord(params)
        } catch (err) {
            console.log(`[getDynamoRecord-Staff Table] Error when getting Pending Message from DB - Tenant's group ${group}, Staff id: ${staffId} `, err);
            result['numError']++
            continue
        }

        // Message Body
        var body = `${pendingMessage.bodyTextCO ? pendingMessage.bodyTextCO + "\n\n" : '' } ${pendingMessage.bodyTextPR ? pendingMessage.bodyTextPR : ''}`
        var emailBody = body.replace(/\n/g, '<br>')
        var header = 'Your Daily Performance Feedback'
        var subject = 'Your Daily Performance Feedback'
        var channelType
        if(staff.Item.receiveTextMessages && staff.Item.receiveEmailMessages) channelType = 'SMS/EMAIL';
        else if( staff.Item.receiveTextMessages ) channelType = 'SMS';
        else if( staff.Item.receiveEmailMessages ) channelType = 'EMAIL';
        else channelType = ''
        var createdAt = new Date().toISOString()
        var updatedAt = new Date().toISOString()
        var senderName = `${userInfo.firstName} ${userInfo.lastName}`
        // Creating Message
        try {
            var createParams = {
                TableName: process.env.API_HERA_MESSAGETABLE_NAME,
                Item: {
                    id: nanoid(),
                    messageTenantId: tenant.id,
                    staffId,
                    createdAt,
                    updatedAt,
                    group,
                    subject,
                    bodyText: body,
                    bodyHtml: generateTemplate(header, emailBody, staffId, companyName),
                    channelType,
                    messageType: 'coaching',
                    isReadS: 'true',
                    'messageType#createdAt': 'coaching#' + createdAt,
                    'channelType#createdAt': channelType + '#' + createdAt,
                    'channelType#isReadS#createdAt': channelType + '#true#' + createdAt,
                    destinationNumber: staff.Item.phone,
                    destinationEmail: staff.Item.email,
                    owner: userInfo.cognitoSub,
                    senderName,
                    senderId: userInfo.id
                }
            };
            await createDynamoRecord(createParams);
    
            const updateParams = {
                id: staffId,
                smsLastMessage: body,
                smsLastMessageTimestamp: createdAt
            }
            const buildParams = buildUpdateParams(process.env.API_HERA_STAFFTABLE_NAME, updateParams, updateParams.id)
            await updateDynamoRecord(buildParams);

            result['numSent']++
        } catch (err) {
            console.log(err);
            result['numError']++
            continue 
        }

        // Delete Pending Message
        try {
            let params = {
                Key:{
                    "id": pendingMessage.id
                },
                TableName: process.env.API_HERA_PENDINGMESSAGETABLE_NAME,
            };
            await deleteDynamoRecord(params);

        } catch (err) {
            console.log(err);
            result['numError']++
            continue 
        }
    }
    return result
}

module.exports = {
    validateExistingPendingMessages,
    sendPendingMessages
}