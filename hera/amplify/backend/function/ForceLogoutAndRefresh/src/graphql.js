const createMessage = /* GraphQL */ `
mutation CreateMessage(
  $input: CreateMessageInput!
  $condition: ModelMessageConditionInput
) {
  createMessage(input: $input, condition: $condition) {
    id
    group
    createdAt 
    channelType
    staffId
    bodyText
    isReadS
    messageType
    staff{
      id
      status
    }
    sender{
      id
    }
    isReadBy{
      items {
        userID
      }
    }
    attachment{
      id
      group
      s3Key
      expirationDate
      contentType
    }
  }
}
`;

const updateStaff = /* GraphQL */ `
  mutation UpdateStaff(
    $input: UpdateStaffInput!
    $condition: ModelStaffConditionInput
  ) {
    updateStaff(input: $input, condition: $condition) {
      id
    }
  }
`;

const createAttachment = /* GraphQL */ `
  mutation CreateAttachment(
    $input: CreateAttachmentInput!
    $condition: ModelAttachmentConditionInput
  ) {
    createAttachment(input: $input, condition: $condition) {
      id
      group
      s3Key
      expirationDate
      contentType
    }
  }
`;

const createNotification = /* GraphQL */ `
  mutation CreateNotification(
    $input: CreateNotificationInput!
    $condition: ModelNotificationConditionInput
  ) {
    createNotification(input: $input, condition: $condition) {
      id
      group
      owner
      title
      createdAt
      description
      releaseNotes
      payload
      clickAction
      isRead
      updatedAt
    }
  }
`

const getTenant = /* GraphQL */ `
  query GetTenant($id: ID!) {
    getTenant(id: $id) {
      id
      originationNumber
      messageServiceProvider
      companyName
    }
  }
`

module.exports = {
    createMessage,
    createAttachment,
    updateStaff,
    createNotification,
    getTenant,
};