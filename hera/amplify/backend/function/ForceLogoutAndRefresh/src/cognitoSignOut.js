const AWS = require("aws-sdk");

exports.cognitoSignOutAll = async (UserPoolId) => {
    try{
        const cognito = new AWS.CognitoIdentityServiceProvider();
        const params = { UserPoolId };
        const users = [];
    
        do {
            const result = await cognito.listUsers(params).promise();
            users.push(...result.Users);
            params.PaginationToken = result.PaginationToken;
        } while (params.PaginationToken);
    
        for (const user of users) {
            let Username = user.Username;
            await cognito.adminUserGlobalSignOut({ UserPoolId, Username }).promise();
        }
        return { cognitoSignOut: true }
    }catch(e){
        console.error("Error cognitoSignOut: ", e)
        return { cognitoSignOut: false, error: e }
    }
};