const {
    scanDynamoRecords,
    buildQueryParams,
    queryDynamoRecords,
    buildUpdateParams,
    updateDynamoRecord,
    createDynamoRecord,
  } = require('./dynamodbHelper')
  async function getAllClients() {
    try {
      let users = []
      let lastEvaluatedKey = null
      do {
        let params = {
          TableName: process.env.API_HERA_USERTABLE_NAME,
          ExclusiveStartKey: lastEvaluatedKey,
        }
        let scanResult = await scanDynamoRecords(params)
        let items = scanResult.Items
        lastEvaluatedKey = scanResult.LastEvaluatedKey
        users = [...users, ...items]
      } while (lastEvaluatedKey)
  
      return users
    } catch (err) {
      console.log('Error in function getAllClients', err)
      return {
        error: err,
      }
    }
  }
  
  module.exports = {
    getAllClients
    
  }