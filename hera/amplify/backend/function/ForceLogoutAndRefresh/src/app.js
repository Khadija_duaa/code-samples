/* Amplify Params - DO NOT EDIT
	API_HERA_GRAPHQLAPIENDPOINTOUTPUT
	API_HERA_GRAPHQLAPIIDOUTPUT
	API_HERA_USERTABLE_ARN
	API_HERA_USERTABLE_NAME
	AUTH_HERA4E362E32_USERPOOLID
	ENV
	REGION
Amplify Params - DO NOT EDIT *//*
Copyright 2017 - 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at
http://aws.amazon.com/apache2.0/
or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
*/

const express = require('express')
const bodyParser = require('body-parser')
const awsServerlessExpressMiddleware = require('aws-serverless-express/middleware')
const { getAllClients } = require('./users')
const { createNotification } = require("./graphql.js")
const {
  executeMutation
} = require('./appSync.js');
const { cognitoSignOutAll } = require('./cognitoSignOut')

// declare a new express app
const app = express()
app.use(bodyParser.json())
app.use(awsServerlessExpressMiddleware.eventContext())

// Enable CORS for all methods
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
});

app.post('/forcelogoutandrefresh',async function(req, res) {
  try {
    let input = {
      title: "NEW VERSION",
      clickAction: req.body.refreshType,
      isRead: false,
      releaseNotes: req.body.releaseNotes,
      group: 'system_admin'
    }
    const userPoolId = process.env.AUTH_HERA4E362E32_USERPOOLID;
    let cognitoSignOut = await cognitoSignOutAll(userPoolId) //This function closes all cognito sessions.
    if(cognitoSignOut.error){  
      let error = { message: cognitoSignOut.error }
      input.owner = req.body.cognitoSub
      input.description = cognitoSignOut.error
      await pushNotification(input)
      return res.json({ error: 'Cognito signout failed', url: req.url, body: error })
    }
    
    const users = await getAllClients()
    if(users.error || !users.length){  
      let error = {message: users.error ? users.error : 'No users found'}
      return res.json({error: 'post call failed', url: req.url, body: error})
    }

    input.description = req.body.newVersionNumber
    for(const user of users){
      input.owner = user.cognitoSub ? user.cognitoSub : null
      if(input.owner) await pushNotification(input)
    }

    res.json({success: 'post call succeed!', url: req.url, body: req.body})
  } catch (e) {
    res.json({error: 'post call failed', url: req.url, body: e})
  }
  
});
async function pushNotification(input) {
  try {
    await executeMutation(createNotification, { input })
  } catch (e) {
    console.log('error pushNotification', e)
  }
}

app.listen(3000, function() {
  console.log("App started")
});

// Export the app object. When executing the application local this does nothing. However,
// to port it to AWS Lambda we will create a wrapper around that will load the app from
// this file
module.exports = app
