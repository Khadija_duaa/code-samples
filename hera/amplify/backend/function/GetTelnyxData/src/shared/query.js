const AWS = require('aws-sdk');
const documentClient = new AWS.DynamoDB.DocumentClient({apiVersion: '2012-08-10'})

async function scanDynamoRecords(params) {
    return new Promise((resolve, reject) => {
        // Create DynamoDB service object
        documentClient.scan(params, function(err, data) {
            if (err) {
                console.error("Unable to get items. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                //console.log("Get Item succeeded:", JSON.stringify(data, null, 2));
                resolve(data)
            }
        });
    })
}

module.exports = { scanDynamoRecords } 