const { scanDynamoRecords } = require("./query")

async function getDataTelnyxEnviroment(){
  
    let records = []
    const table = 'TELNYX_TABLE'
    const arrayEnviroment = getAllTypeEnviroment(table)
    
    try {
      for (const env of arrayEnviroment) {
        const environment = getSplitEnv(env)
        let lastEvaluatedKey = null
        do {
          let params = {
            TableName: env,
            ExclusiveStartKey: lastEvaluatedKey,
          }
          let scanResult = await scanDynamoRecords(params)
          let items = scanResult.Items
            for (const item of items) {
              item.enviroment = environment;
            }
          lastEvaluatedKey = scanResult.LastEvaluatedKey
          if(lastEvaluatedKey) console.log(lastEvaluatedKey)
          records = records.concat(items)
        } while (lastEvaluatedKey);
      }
      return records
    } catch (err) {
      console.log('Error in function getDataTelnyxEnviroment', err);
      return []
    }
  }
  
  async function getDataTenantsEnviroment(){
    let records = []
    const table = 'TENANT_TABLE'
    const arrayEnviroment = getAllTypeEnviroment(table)
    
    try {
      for (const env of arrayEnviroment) {
        let lastEvaluatedKey = null
        do {
          let params = {
            ExpressionAttributeNames: {
              '#group': 'group',
            },
            TableName: env,
            ProjectionExpression: 'id, #group, createdAt, companyName, trialExpDate, accountPremiumStatus, usesXLcoaching',
            ExclusiveStartKey: lastEvaluatedKey,
          }
          let scanResult = await scanDynamoRecords(params)
          let items = scanResult.Items
          lastEvaluatedKey = scanResult.LastEvaluatedKey
          if(lastEvaluatedKey) console.log(lastEvaluatedKey)
          records = records.concat(items)
        } while (lastEvaluatedKey);
      }
      return records
    } catch (err) {
      console.log('Error in function getDataTelnyxEnviroment', err);
      return []
    }
  }
  
  function getAllTypeEnviroment(type){
      let arrayEnviroment = [];
      let i = 1;
    
      while (process.env.hasOwnProperty(`${type}_${i}`)) {
        const hastTable = process.env[`${type}_${i}`];
        arrayEnviroment.push(hastTable);
        i++;
      }
    
      return arrayEnviroment;
  }
  
  function getSplitEnv(env){
    const splitWord = env.split("-");
    const wordEnv = splitWord[splitWord.length - 1]
    return wordEnv;
  }
  
  module.exports = {
    getDataTelnyxEnviroment,
    getDataTenantsEnviroment
  }