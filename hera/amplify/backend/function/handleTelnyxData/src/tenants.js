const {
  scanDynamoRecords,
  buildQueryParams,
  queryDynamoRecords,
  buildUpdateParams,
  updateDynamoRecord,
  createDynamoRecord,
} = require('./dynamodbHelper')

async function getAllTenants() {
  try {
    let tenants = []
    let lastEvaluatedKey = null
    do {
      let params = {
        ExpressionAttributeNames: {
          '#group': 'group',
        },
        TableName: process.env.API_HERA_TENANTTABLE_NAME,
        ProjectionExpression:
          'id, #group, originationNumber, messageServiceProvider',
        ExclusiveStartKey: lastEvaluatedKey,
      }
      let scanResult = await scanDynamoRecords(params)
      let items = scanResult.Items
      lastEvaluatedKey = scanResult.LastEvaluatedKey
      tenants = [...tenants, ...items]
    } while (lastEvaluatedKey)

    return tenants
  } catch (err) {
    console.log('Error in function getAllTenants', err)
    return {
      error: err,
    }
  }
}

async function getStoredTelnyxList() {
  try {
    let numbers = []
    let lastEvaluatedKey = null
    do {
      let params = {
        TableName: process.env.API_HERA_TELNYXTABLE_NAME,
        ExclusiveStartKey: lastEvaluatedKey,
      }
      let scanResult = await scanDynamoRecords(params)
      let items = scanResult.Items
      lastEvaluatedKey = scanResult.LastEvaluatedKey
      numbers = [...numbers, ...items]
    } while (lastEvaluatedKey)

    let localList = []
    numbers.forEach(item => {
      localList.push(item.phoneNumber)
    })

    return localList
  } catch (err) {
    console.log('Error in function getStoredTelnyxList', err)
    return {
      error: err,
    }
  }
}

module.exports = {
  getAllTenants,
  getStoredTelnyxList,
}
