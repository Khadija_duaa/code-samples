const axios = require('axios')
const { getSecretValue } = require('/opt/nodejs/ssm')
const { executeMutation } = require('./appSync')
const {
  createTelnyx,
  updateTenantMessageService,
  createMessageServiceProvider,
  updateTelnyx,
} = require('./graphql')
let instance;
async function initInstance(){
  if(!instance){
    console.log("Initializing Telnyx Instance.");
    const accessToken = await getSecretValue(process.env.TELNYX_TOKEN);
    console.log("Retrieved access token from parameter store.");
    instance = axios.create({
          baseURL: 'https://api.telnyx.com/v2',
          headers: {
            Authorization: `Bearer ${accessToken}`,
            'Content-Type': 'application/json',
          },
    });  
  }
  return instance;
}
async function getTelnyxNumbers() {
  let numbers = []
  let pageNumber = 1
  let totalPages = 0
  try {
    await initInstance();
    do {
      const response = await instance.get('phone_numbers', {
        params: {
          'page[number]': pageNumber,
        },
      })

      totalPages = response.data.meta.total_pages
      pageNumber++

      response.data.data.forEach(item => {
        numbers.push(item)
      })
    } while (totalPages >= pageNumber)

    return numbers
  } catch (error) {
    console.error('[getTelnyxNumbers]', error)
  }
}

async function saveNumberOnDB(number) {
  let input = {
    group: 'null',
    phoneNumber: number.phone_number,
    telnyxId: number.id,
    status: number.status,
    date: new Date().toISOString(),
  }
  try {
    return await executeMutation(createTelnyx, { input })
  } catch (error) {
    console.log('[saveNumberOnDB]', error)
  }
}

async function updateServiceProvider(tenant, telnyxId) {
  let inputTenant = {
    id: tenant.id,
    group: tenant.group,
    messageServiceProvider: 'Telnyx',
  }
  await executeMutation(updateTenantMessageService, { input: inputTenant })

  let inputForHistory = {
    previousMessageServicerProvider: tenant.messageServiceProvider,
    currentMessageServicerProvider: inputTenant.messageServiceProvider,
    date: new Date().toISOString(),
    messageServiceProviderTenantId: tenant.id,
    group: tenant.group,
  }
  await executeMutation(createMessageServiceProvider, {
    input: inputForHistory,
  })

  let inputTelnyx = {
    id: telnyxId,
    group: tenant.group,
  }
  await executeMutation(updateTelnyx, { input: inputTelnyx })
}

module.exports = {
  getTelnyxNumbers,
  saveNumberOnDB,
  updateServiceProvider,
}
