const createTelnyx = /* GraphQL */ `
  mutation CreateTelnyx(
    $input: CreateTelnyxInput!
    $condition: ModelTelnyxConditionInput
  ) {
    createTelnyx(input: $input, condition: $condition) {
      id
      group
      phoneNumber
      telnyxId
      status
      date
      createdAt
      updatedAt
    }
  }
`

const updateTelnyx = /* GraphQL */ `
  mutation UpdateTelnyx(
    $input: UpdateTelnyxInput!
    $condition: ModelTelnyxConditionInput
  ) {
    updateTelnyx(input: $input, condition: $condition) {
      id
      group
    }
  }
`

const listTelnyxs = /* GraphQL */ `
  query ListTelnyxs(
    $filter: ModelTelnyxFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listTelnyxs(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        group
        phoneNumber
        telnyxId
        status
        date
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`

const updateTenantMessageService = /* GraphQL */ `
  mutation UpdateTenant(
    $input: UpdateTenantInput!
    $condition: ModelTenantConditionInput
  ) {
    updateTenant(input: $input, condition: $condition) {
      id
      group
      messageServiceProvider
    }
  }
`

const createMessageServiceProvider = /* GraphQL */ `
  mutation CreateMessageServiceProvider(
    $input: CreateMessageServiceProviderInput!
    $condition: ModelMessageServiceProviderConditionInput
  ) {
    createMessageServiceProvider(input: $input, condition: $condition) {
      group
      date
      previousMessageServicerProvider
      currentMessageServicerProvider
    }
  }
`

module.exports = {
  createTelnyx,
  updateTelnyx,
  listTelnyxs,
  updateTenantMessageService,
  createMessageServiceProvider,
}
