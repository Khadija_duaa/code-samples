/* Amplify Params - DO NOT EDIT
	API_HERA_GRAPHQLAPIENDPOINTOUTPUT
	API_HERA_GRAPHQLAPIIDOUTPUT
	API_HERA_TELNYXTABLE_ARN
	API_HERA_TELNYXTABLE_NAME
	API_HERA_TENANTTABLE_ARN
	API_HERA_TENANTTABLE_NAME
	ENV
	REGION
Amplify Params - DO NOT EDIT */
const { getTelnyxNumbers, saveNumberOnDB, updateServiceProvider } = require('./telnyx')
const { getAllTenants, getStoredTelnyxList } = require('./tenants')

exports.handler = async event => {
  const tenants = await getAllTenants()
  const localList = await getStoredTelnyxList()
  const telnyxNumbers = await getTelnyxNumbers()

  for (const number of telnyxNumbers) {
    if (localList.includes(number.phone_number)) { continue }
    if (number.status != 'active') { continue }
    if (number.messaging_profile_name != process.env.TELNYX_PROFILE_NAME) { continue }
    const savedNumber = await saveNumberOnDB(number)

    const tenant = tenants.find(tenant => {
      return tenant.originationNumber == number.phone_number
    })

    if (tenant) {
      let telnyxId = savedNumber.createTelnyx.id
      await updateServiceProvider(tenant, telnyxId)
    }
  }
  
  const response = {
    statusCode: 200,
    body: JSON.stringify('handleTelnyxData done!'),
  }

  return response
}
