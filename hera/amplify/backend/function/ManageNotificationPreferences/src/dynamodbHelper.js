const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient();


function buildUpdateParams(table, item, id) {
    let updateExpression = 'set '
    let expressionAttributeNames = {}
    let expressionAttributeValues = {}
    let i = 0
    delete item.id // dont update id or createdAt
    delete item.createdAt
    for (const [key, value] of Object.entries(item)) {
        if (value != undefined) {
            updateExpression += `#${i} = :${i},`
            expressionAttributeNames[`#${i}`] = key
            expressionAttributeValues[`:${i}`] = value
            i++
        } else continue
    }
    updateExpression = updateExpression.slice(0, -1)

    let updateParams = {
        TableName: table,
        Key: {
            "id": id
        },
        ConditionExpression: 'attribute_exists(id)',
        UpdateExpression: updateExpression,
        ExpressionAttributeNames: expressionAttributeNames,
        ExpressionAttributeValues: expressionAttributeValues,
        ReturnValues: "UPDATED_NEW"
    };

    return updateParams
}


function getDynamoRecord(params){
    return new Promise((resolve, reject) => {
        ddb.get(params, function(err, data) {
            if (err) {
                console.log("Unable to get item. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                // console.log("Get Item succeeded:", JSON.stringify(data, null, 2));
                resolve(data)
            }
      });
  })
}

function createDynamoRecord(params) {
    return new Promise((resolve, reject) => {
        ddb.put(params, function(err, data) {
            if (err) {
                console.error("Unable to create item. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                console.log("Create item succeeded:", JSON.stringify(data, null, 2));
                resolve(data)
            }
        });
    })
}

function updateDynamoRecord(params) {
    return new Promise((resolve, reject) => {
        ddb.update(params, function(err, data) {
            if (err) {
                console.log("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                console.log("Update Item succeeded:", JSON.stringify(data, null, 2));
                resolve(data)
            }
        });
    })

}


module.exports = {
    buildUpdateParams,
    createDynamoRecord,
    updateDynamoRecord,
    getDynamoRecord
}