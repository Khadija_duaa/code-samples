/* Amplify Params - DO NOT EDIT
	API_HERA_GRAPHQLAPIIDOUTPUT
	API_HERA_MESSAGEPREFERENCESHISTORYTABLE_ARN
	API_HERA_MESSAGEPREFERENCESHISTORYTABLE_NAME
	API_HERA_STAFFTABLE_ARN
	API_HERA_STAFFTABLE_NAME
	ENV
	REGION
Amplify Params - DO NOT EDIT *//*
Copyright 2017 - 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at
    http://aws.amazon.com/apache2.0/
or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
*/


var express = require('express')
var bodyParser = require('body-parser')
var awsServerlessExpressMiddleware = require('aws-serverless-express/middleware')
const { buildUpdateParams,
  createDynamoRecord,
  updateDynamoRecord,
  getDynamoRecord } = require("./dynamodbHelper.js")
const { nanoid } = require('nanoid')
// declare a new express app
var app = express()
app.use(bodyParser.json())
app.use(awsServerlessExpressMiddleware.eventContext())

// Enable CORS for all methods
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
});


/*************************
 * Get Staff Information *
 *************************/
app.get('/staff/:id', async function(req, res) {
  try{
    const staffId = req.params.id
    const params = {
      TableName : process.env.API_HERA_STAFFTABLE_NAME,
      Key: {
        "id": staffId
      }
    }
    const staffData = await getDynamoRecord(params)
    
    //Check for no record found
    if(Object.keys(staffData).length === 0){
      throw new Error("Staff member not found")
    }
    
    //Clean the data so we only show comm prefs
    var cleanedData = {
      receiveTextMessages: staffData.Item.receiveTextMessages,
      receiveEmailMessages: staffData.Item.receiveEmailMessages,
      firstName: staffData.Item.firstName,
      lastName: staffData.Item.lastName,
      group: staffData.Item.group
    }
    res.json({success: 'success', staffData: cleanedData });
  }catch(e){
    console.log(e)
    res.status(500)
    res.json({error: 'Could not get staff information', message: e.message})
  }
});



/*****************************************
* update staff communication preferences *
******************************************/
app.put('/staff/:id', async function(req, res) {
  // Add your code here
  var staffId = req.params.id

  const staffParams = {
    id: staffId,
    receiveTextMessages: req.body.staffData.receiveTextMessages,
    receiveEmailMessages: req.body.staffData.receiveEmailMessages
  }

  try{

    const response = await updateNotificationPreferences(staffParams)
    if(req.body.preferenceTypeSms){
      await createMessagePreferencesHistory(req.body.preferenceTypeSms)
    }
    if(req.body.preferenceTypeEmail){
      await createMessagePreferencesHistory(req.body.preferenceTypeEmail)
    }
    console.log(response)
    res.json({success: 'put call succeed!', url: req.url, body: req.body})
  }catch(e){
    console.log(e)
    res.status(500)
    res.json({error: 'Could not update notification preferences', message: e.message})
  }
  
});


async function updateNotificationPreferences(params){
  try {
    const updateParams = buildUpdateParams(process.env.API_HERA_STAFFTABLE_NAME, params, params.id)
    return await updateDynamoRecord(updateParams)
  } catch (error) {
    console.log('Error in function updateNotificationPreferences', error)
  }
}



async function createMessagePreferencesHistory(params){
  try {
    const createParams = {
        TableName: process.env.API_HERA_MESSAGEPREFERENCESHISTORYTABLE_NAME,
        Item: {
          __typename: 'MessagePreferencesHistory',
          id: nanoid(),
          description: params.description,
          group: params.group,
          datetime: params.datetime,
          messagePreferenceType: params.messagePreferenceType,
          messagePreferencesHistoryStaffId: params.messagePreferencesHistoryStaffId,
          createdAt: new Date().toISOString(),
          updatedAt: new Date().toISOString()
        }
    };
    if(!createParams.Item.messagePreferencesHistoryStaffId){
      return
    }
    await createDynamoRecord(createParams)
  } catch (error) {
    console.log('Error in function createMessagePreferencesHistory', error)
  }
}


app.listen(3000, function() {
    console.log("App started")
});

// Export the app object. When executing the application local this does nothing. However,
// to port it to AWS Lambda we will create a wrapper around that will load the app from
// this file
module.exports = app
