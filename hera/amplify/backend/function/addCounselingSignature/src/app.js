/* Amplify Params - DO NOT EDIT
	API_HERA_COUNSELINGTABLE_ARN
	API_HERA_COUNSELINGTABLE_NAME
	API_HERA_DOCUMENTTABLE_ARN
	API_HERA_DOCUMENTTABLE_NAME
	API_HERA_GRAPHQLAPIIDOUTPUT
	API_HERA_INFRACTIONTABLE_ARN
	API_HERA_INFRACTIONTABLE_NAME
	API_HERA_OPTIONSCUSTOMLISTSTABLE_ARN
	API_HERA_OPTIONSCUSTOMLISTSTABLE_NAME
	API_HERA_STAFFTABLE_ARN
	API_HERA_STAFFTABLE_NAME
	API_HERA_TENANTTABLE_ARN
	API_HERA_TENANTTABLE_NAME
	API_HERA_USERTABLE_ARN
	API_HERA_USERTABLE_NAME
	ENV
	REGION
	STORAGE_HERA_BUCKETNAME
Amplify Params - DO NOT EDIT *//*
Copyright 2017 - 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at
    http://aws.amazon.com/apache2.0/
or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
*/

var AWS = require("aws-sdk");
var express = require('express')
var bodyParser = require('body-parser')
var awsServerlessExpressMiddleware = require('aws-serverless-express/middleware')
var s3 = new AWS.S3();

// declare a new express app
var app = express()
app.use(bodyParser.json())
app.use(awsServerlessExpressMiddleware.eventContext())

// Enable CORS for all methods
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "*")
  next()
});


/**********************
 * Example get method *
 **********************/
app.get('/counseling', function(req, res) {
// Add your code here
res.json({success: 'get call succeed!', url: req.url});
  // Add your code here
  
});


app.get('/counseling/:id', async function(req, res) {
  try{
    //Get the counseling Record
    var counselingId = req.params.id;
    var params = {
      TableName : process.env.API_HERA_COUNSELINGTABLE_NAME,
      Key: {
        "id": counselingId
      }
    };
    var counselingData = await getDynamoRecord(params)

    if(Object.keys(counselingData).length === 0){
      throw new Error("Counseling Record not found")
    }
    
    var staffID = counselingData.Item.counselingStaffId;
    var userID = counselingData.Item.counselingUserId;
    var tenantID = counselingData.Item.tenantId
    const severityID = counselingData.Item.counselingSeverityId;

    counselingData.Item.severity = null;
    if (severityID) {
      const params = {
        TableName : process.env.API_HERA_OPTIONSCUSTOMLISTSTABLE_NAME,
        Key: {"id": severityID},
      };
      const severityOption = await getDynamoRecord(params);
      counselingData.Item.severity = severityOption?.Item;
    }

    // Get Images of Counseling from Document Table
    var params = {
      TableName: process.env.API_HERA_DOCUMENTTABLE_NAME,
      KeyConditionExpression: "#i = :i",
      IndexName: "gsi-imagesCounseling",
      ExpressionAttributeNames:{
        '#i': 'documentImageCounselingId',
        '#k': 'key',
        '#t': 'type',
        '#d': 'isDocVisibleToAssociate'
      },
      ExpressionAttributeValues: {
        ':i': counselingId,
      },
      ProjectionExpression: '#k,#t,#d'
    };
    var imagesData = await queryDynamoRecords(params)

    counselingData.Item.images = {}
    if (imagesData) {
      counselingData.Item.images = imagesData.Items
      counselingData.Item.images.forEach(function(image, index) {
          signedUrl = s3.getSignedUrl('getObject', {
            Bucket: process.env.STORAGE_HERA_BUCKETNAME,
            Key: 'public/' + image["key"],
            Expires: 60
          });
          image["docSignedUrl"] = signedUrl
      });
    }
    
    //Get the staff Record
    var params = {
      TableName : process.env.API_HERA_STAFFTABLE_NAME,
      Key: {
        "id": staffID
      }
    };
    
    var staffData = await getDynamoRecord(params)
    
    if(Object.keys(staffData).length === 0){
      throw new Error("Counseling Record not found")
    }
    
    //Get the user Record
    var userData = {}
    if(userID){
        var params = {
          TableName: process.env.API_HERA_USERTABLE_NAME,
          Key:{
            "id": userID
          }
        };
        
        userData = await getDynamoRecord(params)
        
        if(Object.keys(userData).length === 0){
          userData = {}
        }
        if(!tenantID){
          tenantID = userData.Item.userTenantId
        }
    }
    
    var companyData = {}
    var signedUrl = null
    if(tenantID){
      var params = {
        TableName: process.env.API_HERA_TENANTTABLE_NAME,
        Key:{
          "id": tenantID
        },
        ProjectionExpression: 'logo,companyName',
      };
      
      companyData = await getDynamoRecord(params)
      var companyLogo = companyData.Item.logo
      var companyName = companyData.Item.companyName
      console.log(companyData)
      
      // get copmany logo url
       signedUrl = s3.getSignedUrl('getObject', {
        Bucket: process.env.STORAGE_HERA_BUCKETNAME,
        Key: 'public/' + companyLogo,
        Expires: 60
      });
    }

    //Get the infractions
     params = {
        ExpressionAttributeValues: {
            ':i': counselingId,
        },
         ExpressionAttributeNames: {
            '#i': 'infractionCounselingId',
        },
        KeyConditionExpression: '#i = :i',
        IndexName: 'gsi-CounselingInfraction',
        TableName: process.env.API_HERA_INFRACTIONTABLE_NAME,
    };
    var infractionData = await queryDynamoRecords(params);
    console.log(infractionData)
    var infractions = infractionData.Items
  
  res.json({success: 'get call succeed!', counselingData: counselingData, staffData: staffData, userData: userData, infractionData: infractionData, companyLogo:signedUrl, companyName:companyName});
  }
  catch(e){
    console.log(e)
    res.status(500)
    res.json({error: 'Could not get counseling information', message: e.message})
  }
});

/****************************
* Example post method *
****************************/

app.post('/counseling', function(req, res) {
  // Add your code here
  res.json({success: 'post call succeed!', url: req.url, body: req.body})
});

app.post('/counseling/*', function(req, res) {
  // Add your code here
  res.json({success: 'post call succeed!', url: req.url, body: req.body})
});

/****************************
* Example put method *
****************************/
app.put('/counseling/:id', async function(req, res) {
  // Add your code here
  var counselingID = req.params.id;
  var signatureAcknowledge = req.body.signatureAcknowledge
  var refusedToSign = req.body.refusedToSign
  var status = req.body.status
  var signature = req.body.signature
  var dateSigned = req.body.dateSigned
  var employeeNotes = req.body.employeeNotes
  
  try{
    var response = await updateCounseling(counselingID, signatureAcknowledge, refusedToSign, status, signature, dateSigned, employeeNotes)
    console.log(response)
    res.json({success: 'put call succeed!', url: req.url, body: req.body})
  }
  catch(e){
    console.log(e);
    res.status(500);
    res.json({error: 'Could not update counseling record', message: e.message})
  }
  
});

function getDynamoRecord(params){
  return new Promise((resolve, reject) => {
      
      var documentClient = new AWS.DynamoDB.DocumentClient();
      
      documentClient.get(params, function(err, data) {
          if (err) {
                console.error("Unable to get item. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                console.log("Get Item succeeded:", JSON.stringify(data, null, 2));
                resolve(data)
            }
      });
  })
}

function updateCounseling(id, signatureAcknowledge, refusedToSign, status, signature, dateSigned, employeeNotes){
  return new Promise((resolve, reject) =>{
    AWS.config.region = process.env.region;
    var ddb = new AWS.DynamoDB.DocumentClient({apiVersion: '2012-08-10'})
    var updateRecord = {
      TableName: process.env.API_HERA_COUNSELINGTABLE_NAME,
      Key: {
        "id": id
      },
      UpdateExpression: " set #a = :a, #b = :b, #c = :c, #d = :d, #e = :e, #n = :n",
      ExpressionAttributeNames:{
        "#a": "signatureAcknowledge",
        "#b": "refusedToSign",
        "#c": "status",
        "#d": "signature",
        "#e": "dateSigned",
        "#n": "employeeNotes"
      },
      ExpressionAttributeValues:{
        ":a": signatureAcknowledge,
        ":b": refusedToSign,
        ":c": status,
        ":d": signature,
        ":e": dateSigned,
        ":n": employeeNotes
      },
      ReturnValues:"UPDATED_NEW"
    };
    
    ddb.update(updateRecord, function(err,data){
      if(err){
        console.error("Unable to update item. Error JSON: ", JSON.stringify(err, null, 2));
        reject(err);
      } else{
        console.log("Updated item succeed", JSON.stringify(data, null, 2));
        resolve(data);
      }
    });
  })
};

function queryDynamoRecords(params){
    return new Promise((resolve, reject) => {
        // Create DynamoDB service object
        var ddb = new AWS.DynamoDB.DocumentClient({apiVersion: '2012-08-10'})
        ddb.query(params, function(err, data) {
            if (err) {
                console.error("Unable to get items. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                console.log("Get Item succeeded:", JSON.stringify(data, null, 2));
                resolve(data)
            }
        });
    })
}

function scanDynamoRecords(params) {
  return new Promise((resolve, reject) => {
      // Create DynamoDB service object
      var ddb = new AWS.DynamoDB.DocumentClient({apiVersion: '2012-08-10'})
      ddb.scan(params, function(err, data) {
          if (err) {
              err[`['params' of scanDynamoRecords]`] = params
              console.error("Unable to get items. Error JSON:", JSON.stringify(err, null, 2));
              reject(err)
          } else {
              console.log("Scan Item succeeded:", JSON.stringify(data, null, 2));
              resolve(data)
          }
      });
  })
}

app.listen(3000, function() {
    console.log("App started")
});

// Export the app object. When executing the application local this does nothing. However,
// to port it to AWS Lambda we will create a wrapper around that will load the app from
// this file
module.exports = app
