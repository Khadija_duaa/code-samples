const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient({apiVersion: '2012-08-10'})

function scanDynamoRecords(params) {
    return new Promise((resolve, reject) => {
        // Create DynamoDB service object
        ddb.scan(params, function(err, data) {
            if (err) {
                console.error("Unable to get items. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                //console.log("Get Item succeeded:", JSON.stringify(data, null, 2));
                resolve(data)
            }
        });
    })
}

function queryDynamoRecords(params){
    return new Promise((resolve, reject) => {
        ddb.query(params, function(err, data) {
            if (err) {
                err[`['params' of queryDynamoRecords]`] = params
                console.error("Unable to query items. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                // console.log("Get items succeeded:", JSON.stringify(data, null, 2));
                resolve(data)
            }
        });
    })
}

module.exports = { 
    scanDynamoRecords,
    queryDynamoRecords
} 