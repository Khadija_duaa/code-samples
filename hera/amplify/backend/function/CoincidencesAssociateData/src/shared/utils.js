
const { scanDynamoRecords, queryDynamoRecords } = require("./query")

async function nickNamesByName(name){
  let records = [];
  try {
    let lastEvaluatedKey = null;
    do {
      let params = {
        TableName: process.env.API_HERA_NICKNAMETABLE_NAME,
        IndexName: "byName",
        ExpressionAttributeNames: {
          "#name": "name",
        },
        ExpressionAttributeValues: {
          ":name": name
        },
        KeyConditionExpression: "#name = :name",
        ProjectionExpression: "#name, nicknames",
        ExclusiveStartKey: lastEvaluatedKey,
      };
      let results = await queryDynamoRecords(params);
      let items = results.Items;
      lastEvaluatedKey = results.LastEvaluatedKey;
      if (lastEvaluatedKey) console.log("lastEvaluatedKey: ", lastEvaluatedKey);
      records = records.concat(items);
    } while (lastEvaluatedKey);

  } catch (err) {
    console.log("Error in function nickNamesByName", err);
    records = []
  } finally {
    return records
  }
}

async function getDataAsocciate(group){
  let staffs = [];
  try {
    let lastEvaluatedKey = null;
    do {
      let params = {
        TableName: process.env.API_HERA_STAFFTABLE_NAME,
        IndexName: "byGroup",
        ExpressionAttributeNames: {
          "#group": "group",
          "#status": "status",
        },
        ExpressionAttributeValues: {
          ":group": group,
        },
        KeyConditionExpression: "#group = :group",
        ProjectionExpression: "id, #group, firstName, lastName, transporterId, dob, #status, email, phone, authorizedToDrive, defaultVehicle, defaultVehicle2, defaultVehicle3, keyFocusArea, keyFocusAreaCompleted",
        ExclusiveStartKey: lastEvaluatedKey,
      };
      let results = await queryDynamoRecords(params);
      let items = results.Items;
      lastEvaluatedKey = results.LastEvaluatedKey;
      if (lastEvaluatedKey) console.log("lastEvaluatedKey: ", lastEvaluatedKey);
      staffs = staffs.concat(items);
    } while (lastEvaluatedKey);

  } catch (err) {
    console.log("[getDataAsociate] Error in function getDataAsociate", err);
    staffs = []
  } finally {
    return staffs
  }
}

function capitalizeFirstLetter(string='') {
  if(!string){
    return ''
  }
  return string?.charAt(0).toUpperCase() + string?.slice(1).toLowerCase()
}

async function checkDuplicate(data) {
  
    const { firstName,lastName, group } = data
    const capitalizeFirstName = capitalizeFirstLetter(firstName)

    // Get nicknames by firstName
    const nicknames = await nickNamesByName(capitalizeFirstName)
    // Get staff by Group
    const associates = await getDataAsocciate(group)
    // Array to store the found duplicates
    let duplicates = []; 
    const matchingNickname = nicknames.find(person => person.name === capitalizeFirstName)
  
    associates.forEach(associate => {
      if(matchingNickname){
        const name = associate.firstName // string
        const nicknames = matchingNickname.nicknames // array
        const validate = nicknames.includes(name)
        if(validate){
          duplicates.push(associate)
        }
      }

      //Check for spelling errors
      const validateFirstName = getMissingLetters(firstName, associate.firstName)
      const validateLastName = getMissingLetters(lastName, associate.lastName)
      //Check for accent errors
      const validateFirstName2 = compareStrings(firstName, associate.firstName)
      if((validateFirstName + validateLastName) <= 2 || validateFirstName2 === true){
        duplicates.push(associate)
      }
    })
    
    const uniqueDuplicates = [...new Set(duplicates)];
    return uniqueDuplicates

}
  
function getMissingLetters(s1, s2) {
    const m = s1.length;
    const n = s2.length;
  
  
    const dp = new Array(m + 1).fill(null).map(() => new Array(n + 1).fill(0));
  
    for (let i = 0; i <= m; i++) {
      dp[i][0] = i;
    }
    for (let j = 0; j <= n; j++) {
      dp[0][j] = j;
    }
  
    for (let i = 1; i <= m; i++) {
      for (let j = 1; j <= n; j++) {
        const cost = s1[i - 1] === s2[j - 1] ? 0 : 1;
        dp[i][j] = Math.min(dp[i - 1][j] + 1, dp[i][j - 1] + 1, dp[i - 1][j - 1] + cost);
      }
    }

    return dp[m][n];
}
  
function removeAccents(str) {
    return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
}
  
function compareStrings(str1, str2) {
    let isValid = false;
  
    if (str1.length === str2.length) {
      const normalizedStr1 = removeAccents(str1.toLowerCase());
      const normalizedStr2 = removeAccents(str2.toLowerCase());
      isValid = normalizedStr1 === normalizedStr2;
    }
    
    return isValid;
}
  
  module.exports = { checkDuplicate } 