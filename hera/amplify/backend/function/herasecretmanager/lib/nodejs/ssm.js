const AWS = require('aws-sdk');
const ssm = new AWS.SSM({ region: process.env.REGION });

async function getSecretValue(secretName) {
  try {

    const params = {
      Name: secretName,
      WithDecryption: true // Set this to true if the parameter is stored as a SecureString
    };
    
    const response = await ssm.getParameter(params).promise();
    const parameterValue = response.Parameter.Value;
    
    return parameterValue;
  } catch (error) {
    console.error('Error retrieving parameter:', error);
    return null;
  }
}

module.exports = {
  getSecretValue
}
