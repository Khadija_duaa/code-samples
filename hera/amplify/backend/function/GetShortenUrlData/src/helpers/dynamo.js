const AWS = require('aws-sdk');
const documentClient = new AWS.DynamoDB.DocumentClient({apiVersion: '2012-08-10'})

async function queryDynamoRecords(params) {
    return new Promise((resolve, reject) => {
        // Create DynamoDB service object
        documentClient.query(params, function(err, data) {
            if (err) {
                console.error("Unable to query items. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                console.log("Get items succeeded:", JSON.stringify(data, null, 2));
                resolve(data)
            }
        });
    })
}

module.exports = { queryDynamoRecords } 