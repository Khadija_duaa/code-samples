/*
Copyright 2017 - 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at
    http://aws.amazon.com/apache2.0/
or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
*/

// Import the fs module to read the configuration file
const fs = require('fs');

// Determine the current environment
const env = process.env.NODE_ENV || 'messages';

// Access the environment variables
const SHORTEN_URL_TABLE = process.env.API_HERA_SHORTENURLTABLE_NAME;
const { queryDynamoRecords } = require("./helpers/dynamo")

const express = require('express')
const bodyParser = require('body-parser')
const awsServerlessExpressMiddleware = require('aws-serverless-express/middleware')

// declare a new express app
const app = express()
app.use(bodyParser.json())
app.use(awsServerlessExpressMiddleware.eventContext())

// Enable CORS for all methods
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "*")
  next()
});

app.get('/u/shorten-url/:shortenId', async function(req, res) {
 
  let id = null
  const shortenId = req.params.shortenId
  
  try{
      //Get an shorten url record's data
      const params = {
        TableName: SHORTEN_URL_TABLE,
        IndexName: 'byShortenId',
        KeyConditionExpression: 'shortenId = :shortenId',
        ExpressionAttributeValues: {
          ':shortenId': shortenId 
        }
      };

      const result = await queryDynamoRecords(params)
      if(result.Items.length === 0) return res.json({error: `No items found`, type: 'ERROR'}) 
      const item = result.Items[0]
      if(item.type === 'ATTACHMENT' && item.shortenUrlAttachmentId) id = item.shortenUrlAttachmentId
      if(item.type === 'COUNSELING' && item.shortenUrlCounselingId) id = item.shortenUrlCounselingId
      if(item.type === 'DAILYLOG' && item.urlToken) id = item.urlToken

      if(!id) return res.json({error: `Error getting the id of the ${item.type}`, shortenId})
      return res.json({success: 'get call succeed!', id, type: item.type});

  }catch(e){
      return res.json({error: e ? e : `Error getting data!`, shortenId}); 
  }
});


app.listen(3000, function() {
    console.log("App started")
});

// Export the app object. When executing the application local this does nothing. However,
// to port it to AWS Lambda we will create a wrapper around that will load the app from
// this file
module.exports = app
