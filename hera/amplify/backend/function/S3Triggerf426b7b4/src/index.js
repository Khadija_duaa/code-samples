var AWS = require("aws-sdk");


// eslint-disable-next-line
exports.handler = async function(event, context) {
  console.log('Received S3 event:', JSON.stringify(event, null, 2));

  // Get the object from the event and show its content type
  const bucket = event.Records[0].s3.bucket.name; //eslint-disable-line
  const key = event.Records[0].s3.object.key; //eslint-disable-line
  console.log(`Bucket: ${bucket}`, `Key: ${key}`);  


  //Configure Textract
  const region = event.Records[0].awsRegion
  AWS.config.region = region;
  var textract = new AWS.Textract();
  console.log(textract);


  var params = {
    DocumentLocation: {
      S3Object: {
        Bucket: bucket,
        Name: key
      }
    },
    FeatureTypes: [ 
      "TABLES", "FORMS"
    ],
  };

  // const data = await textract.startDocumentAnalysis(params).promise();
  // console.log(data);
  
  console.log('skipping textract job with params:', params)

  context.done(null, 'Successfully processed S3 event'); // SUCCESS with message
};
