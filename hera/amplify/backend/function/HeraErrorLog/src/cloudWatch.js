const AWS = require("aws-sdk");

const cloudwatchLogs = new AWS.CloudWatchLogs({apiVersion:"2014-03-28"})
const logGroupName = process.env.LOG_GROUP_NAME || null

async function createLogGroupError(error){
  
    let {name, message, email, action, stack} = error

    name = (name) ? name : '-'
    message = (message) ? message : '-'
    email = (email) ? email : '-'
    action = (action) ? action : '-'
 
    if(!logGroupName){
        return { statusCode: 500, body: 'Missing environment variable LOG_GROUP_NAME defined'}
    }

    try {

    const currentDateMiliseconds = Date.now().toString()
    const currentDateFormat = getCurrentDateFormat()
    const logStreamName = `${currentDateFormat}/[$LATEST]${currentDateMiliseconds}/${action}/${email}`
    
    const createStreamParams = {
      logGroupName,
      logStreamName
    };
    
    await cloudwatchLogs.createLogStream(createStreamParams).promise()

    const errorMessage = `[Error.Login]\nEmail: ${email}\nError Name: ${name}\nError Message: ${message}\nError Stack: ${stack}` 
    const errorEvent = {
      message: errorMessage,
      timestamp: Date.now()
    };

    const logEvent = {
      logGroupName,
      logStreamName,
      logEvents: [errorEvent]
    };

    await cloudwatchLogs.putLogEvents(logEvent).promise()

    return { statusCode: 200, body: `Sucess!, Log Group was sent: ${logGroupName}`}
  } catch (error) {
    return { statusCode: 500, body: `Error!, Error to sent a Log Group: ${error}` }
  }
}

function getCurrentDateFormat(){
  const currentDate = new Date();
  return `${currentDate.getFullYear()}/${(currentDate.getMonth() + 1).toString().padStart(2, '0')}/${currentDate.getDate().toString().padStart(2, '0')}`
}

module.exports = {
    createLogGroupError
}