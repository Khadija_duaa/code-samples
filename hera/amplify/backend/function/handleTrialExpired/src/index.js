/* Amplify Params - DO NOT EDIT
	API_HERA_GRAPHQLAPIENDPOINTOUTPUT
	API_HERA_GRAPHQLAPIIDOUTPUT
	ENV
	REGION
Amplify Params - DO NOT EDIT */

const { getTrialTenantsWithTelnyx, getTelnyxByPhone } = require('./tenants')
const { updateServiceProvider } = require('./telnyx')

exports.handler = async event => {
  const tenants = await getTrialTenantsWithTelnyx()
  const today = new Date().getTime()
  const daysToRemoveNumber = 30

  for (const tenant of tenants) {
    let trialExpDate = new Date(tenant.trialExpDate).getTime()
    let diff = today - trialExpDate
    let days = diff / (1000 * 60 * 60 * 24)
    if (days >= daysToRemoveNumber) {
      const telnyx = await getTelnyxByPhone(tenant.originationNumber)
      await updateServiceProvider(tenant, telnyx.id)
    }
  }

  const response = {
    statusCode: 200,
    body: 'handleTrialExpired done.',
  }

  return response
}
