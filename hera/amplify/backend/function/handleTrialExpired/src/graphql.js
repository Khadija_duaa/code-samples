const updateTelnyx = /* GraphQL */ `
  mutation UpdateTelnyx(
    $input: UpdateTelnyxInput!
    $condition: ModelTelnyxConditionInput
  ) {
    updateTelnyx(input: $input, condition: $condition) {
      id
      group
    }
  }
`

const updateTenantMessageService = /* GraphQL */ `
  mutation UpdateTenant(
    $input: UpdateTenantInput!
    $condition: ModelTenantConditionInput
  ) {
    updateTenant(input: $input, condition: $condition) {
      id
      group
      messageServiceProvider
    }
  }
`

const createMessageServiceProvider = /* GraphQL */ `
  mutation CreateMessageServiceProvider(
    $input: CreateMessageServiceProviderInput!
    $condition: ModelMessageServiceProviderConditionInput
  ) {
    createMessageServiceProvider(input: $input, condition: $condition) {
      group
      date
      previousMessageServicerProvider
      currentMessageServicerProvider
    }
  }
`

module.exports = {
  updateTelnyx,
  updateTenantMessageService,
  createMessageServiceProvider,
}
