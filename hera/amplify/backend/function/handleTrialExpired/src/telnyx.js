const { executeMutation } = require('./appSync')
const {
  updateTenantMessageService,
  createMessageServiceProvider,
  updateTelnyx,
} = require('./graphql')

async function updateServiceProvider(tenant, telnyxId) {
  let inputTenant = {
    id: tenant.id,
    group: tenant.group,
    messageServiceProvider: 'None',
    originationNumber: '',
  }
  await executeMutation(updateTenantMessageService, { input: inputTenant })

  let inputForHistory = {
    previousMessageServicerProvider: tenant.messageServiceProvider,
    currentMessageServicerProvider: inputTenant.messageServiceProvider,
    date: new Date().toISOString(),
    messageServiceProviderTenantId: tenant.id,
    group: tenant.group,
  }
  await executeMutation(createMessageServiceProvider, { input: inputForHistory })

  let inputTelnyx = {
    id: telnyxId,
    group: 'null',
  }
  await executeMutation(updateTelnyx, { input: inputTelnyx })
}

module.exports = {
  updateServiceProvider,
}
