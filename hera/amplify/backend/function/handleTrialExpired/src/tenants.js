const {
  scanDynamoRecords,
  buildQueryParams,
  queryDynamoRecords,
  buildUpdateParams,
  updateDynamoRecord,
  createDynamoRecord,
} = require('./dynamodbHelper')

async function getTrialTenantsWithTelnyx() {
  try {
    let tenants = []
    let lastEvaluatedKey = null
    let accountPremiumStatus = 'trial'
    let messageServiceProvider = 'Telnyx'
    do {
      let params = {
        TableName: process.env.API_HERA_TENANTTABLE_NAME,
        FilterExpression:
          'contains(#accountPremiumStatus, :accountPremiumStatus) AND #messageServiceProvider = :messageServiceProvider',
        ExpressionAttributeNames: {
          '#accountPremiumStatus': 'accountPremiumStatus',
          '#messageServiceProvider': 'messageServiceProvider',
        },
        ExpressionAttributeValues: {
          ':accountPremiumStatus': accountPremiumStatus,
          ':messageServiceProvider': messageServiceProvider,
        },
        ExclusiveStartKey: lastEvaluatedKey,
      }
      let scanResult = await scanDynamoRecords(params)
      let items = scanResult.Items
      lastEvaluatedKey = scanResult.LastEvaluatedKey
      tenants = [...tenants, ...items]
    } while (lastEvaluatedKey)

    return tenants
  } catch (err) {
    console.log('Error in function getTrialTenantsWithTelnyx', err)
    return {
      error: err,
    }
  }
}

async function getTelnyxByPhone(phoneNumber) {
  try {
    let telnyx = []
    let lastEvaluatedKey = null
    do {
      let params = {
        TableName: process.env.API_HERA_TELNYXTABLE_NAME,
        FilterExpression: 'phoneNumber = :phoneNumber',
        ExpressionAttributeValues: { ':phoneNumber': phoneNumber },
        ExclusiveStartKey: lastEvaluatedKey,
        Limit: 1,
      }
      let scanResult = await scanDynamoRecords(params)
      let items = scanResult.Items
      lastEvaluatedKey = scanResult.LastEvaluatedKey
      telnyx = [...telnyx, ...items]
    } while (lastEvaluatedKey)

    return telnyx[0]
  } catch (err) {
    console.log('Error in function getTelnyxByPhone', err)
    return {
      error: err,
    }
  }
}

module.exports = {
  getTrialTenantsWithTelnyx,
  getTelnyxByPhone,
}
