/*
Use the following code to retrieve configured secrets from SSM:

const aws = require('aws-sdk');

const { Parameters } = await (new aws.SSM())
  .getParameters({
    Names: ["STRIPE_SECRET_KEY"].map(secretName => process.env[secretName]),
    WithDecryption: true,
  })
  .promise();

Parameters will be of the form { Name: 'secretName', Value: 'secretValue', ... }[]
*/
/* Amplify Params - DO NOT EDIT
	API_HERA_CARDTABLE_ARN
	API_HERA_CARDTABLE_NAME
	API_HERA_GRAPHQLAPIENDPOINTOUTPUT
	API_HERA_GRAPHQLAPIIDOUTPUT
	API_HERA_INVOICETABLE_ARN
	API_HERA_INVOICETABLE_NAME
	API_HERA_MESSAGETABLE_ARN
	API_HERA_MESSAGETABLE_NAME
	API_HERA_TENANTTABLE_ARN
	API_HERA_TENANTTABLE_NAME
	ENV
	REGION
Amplify Params - DO NOT EDIT *//*
Copyright 2017 - 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at
    http://aws.amazon.com/apache2.0/
or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
*/

// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
// Set the region
AWS.config.update({region: process.env.REGION});
var ddb = new AWS.DynamoDB.DocumentClient({apiVersion: '2012-08-10'});
const { generateTemplate } = require('./emailTemplate')

var express = require('express')
var bodyParser = require('body-parser')
var awsServerlessExpressMiddleware = require('aws-serverless-express/middleware')

// declare a new express app
var app = express()
app.use(bodyParser.json())
app.use(awsServerlessExpressMiddleware.eventContext())

// Enable CORS for all methods
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
});

const { pushToCloudWatch } = require('./cloudWatchLogger')

// Set up Stripe
// Set your secret key. Remember to switch to your live secret key in production!
// See your keys here: https://dashboard.stripe.com/account/apikeys
const { getSecretValue } = require('/opt/nodejs/ssm')
let stripe;
const months = ['January','February','March','April','May','June','July','August','September','October','November','December']


async function initStripe(){
  if(!stripe){
    console.log("Initializing Stripe Module");
    const stripeKey = await getSecretValue(process.env.STRIPE_SECRET_KEY)
    stripe = require('stripe')(stripeKey);
  } 
}
/**********************
 * Get Customer *
 **********************/
app.get('/customer', async function(req, res) {
  await initStripe();
  const customers = await stripe.customers.list({});
  res.json({success: 'get call succeed!', url: req.url, customers: customers});
});

app.get('/customer/:id', async function(req, res) {
  var customerId = req.params.id
  await initStripe();
  try{
    const customer = await stripe.customers.retrieve(
      customerId
    );
    const paymentMethods = await stripe.paymentMethods.list({
      customer: customerId,
      type: 'card',
    });

    res.json({success: 'get call succeed!', url: req.url, customer: customer, paymentMethods: paymentMethods});

  }catch(e){
    const loggerBody = {
      'Title': `Failed getting customer from Stripe`,
      "Reason": e  
    }
    pushToCloudWatch.error(loggerBody)
    res.status(500).json({ success: 'Failed. Error getting customer from Stripe', error: e })
  }
});


/****************************
* Create Customer *
****************************/
app.post('/customer', async function(req, res) {
  // Add your code here
  const body = req.body
  let existingCustomers = false
  await initStripe();
  // Ensure customer with given email does not already exist
  try{
    existingCustomers = await stripe.customers.list({
      limit: 1,
      email: body.email
    });    
  }catch(e){
    res.status(500).json({ success: 'Failed. Error creating customer from Stripe', error: e })
  }

  if(existingCustomers && existingCustomers.data !== undefined && existingCustomers.data.length){
    console.log('found existing customer(s)', existingCustomers)
    res.json({success: 'Success. Existing customer found.', url: req.url, body: req.body, customer: existingCustomers.data[0] })
  }
  
  try{
    const createCustomer = await stripe.customers.create({
      name: body.name,
      email: body.email.trim(),
      address: body.address,
      description: body.name
    });
    console.log(JSON.stringify(createCustomer))
    res.json({success: 'post call succeed!', url: req.url, body: req.body, customer: createCustomer})
  }catch(e){
    const loggerBody = {
      'Title': `Failure creating Stripe customer`,
      "Reason": e  
    }
    pushToCloudWatch.error(loggerBody)
    res.status(500).json({ success: 'Failed. Error creating customer from Stripe', error: e })
  }
});

app.post('/customer/*', function(req, res) {
  // Add your code here
  res.json({success: 'post call succeed!', url: req.url, body: req.body})
});


/****************************
* Update Customer *
****************************/
app.put('/customer', function(req, res) {
  // Add your code here
  res.json({success: 'put call succeed!', url: req.url, body: req.body})
});

app.put('/customer/:id', async function(req, res) {
  const body = req.body
  var customerId = req.params.id
  var update = {
    name: body.name,
    email: body.email.trim(),
    address: body.address,
    description: body.name,
  }
  await initStripe();
  // if(!!body.defaultPaymentId){
	//   update.invoice_settings = {
	//     "default_payment_method": body.defaultPaymentId
	//   }
  // }
  try{
    const updateCustomer = await stripe.customers.update(customerId, update);
    console.log(JSON.stringify(updateCustomer))
    res.json({success: 'put call succeed!', url: req.url, body: req.body})
  }catch(e){
	  const loggerBody = {
	    'Title': `Failure updating Stripe customer`,
	    "Reason": e  
	  }
	  pushToCloudWatch.error(loggerBody)
	  res.status(500).json({ success: 'Failed. Error updating Stripe customer', error: e })
  }
});

/****************************
* Delete Payment Method *
****************************/
app.delete('/customer', function(req, res) {
  // Add your code here
  res.json({success: 'delete call succeed!', url: req.url});
});

app.delete('/customer/:customerId/:paymentMethodId', async function(req, res) {
  var customerId = req.params.customerId
  var paymentMethodId = req.params.paymentMethodId
  await initStripe();
  try{
    const paymentMethod = await stripe.paymentMethods.detach(
      paymentMethodId
    );
    console.log(paymentMethod)
    res.json({success: 'delete call succeed!', url: req.url, paymentMethod: paymentMethod});
  }catch(e){
    const loggerBody = {
      'Title': `Error detaching customer from Stripe`,
      "Reason": e  
    }
    pushToCloudWatch.error(loggerBody)
    res.status(500).json({ success: 'Failed. Error detaching customer from Stripe', error: e })
  }
});


/****************************
* Create Setup Intent *
****************************/
app.post('/intent', async function(req, res) {
  // const express = require('express');
  // const expressHandlebars = require('express-handlebars');
  // const app = express();
  await initStripe();
  const body = req.body
  try{
    console.log({body: body})
    var intent = await stripe.setupIntents.create({
      customer: req.body.customer.id,
    });

    
    // app.engine('.hbs', expressHandlebars({ extname: '.hbs' }));
    // app.set('view engine', '.hbs');
    // app.set('views', './views');
    
    // app.get('/card-wallet', async (req, res) => {
    //   intent = await stripe.setupIntents.create({
    //     customer: req.body.customer.id,
    //   });
    // });
    
    // app.listen(3000, () => {
    //   console.log('Running on port 3000');
    // });
    
    console.log({intent: intent})
    res.json({success: 'post call succeed!', url: req.url, body: req.body, intent: intent})

  }catch(e){
    const loggerBody = {
      'Title': `Failure creating Stripe setup intent`,
      "Reason": e  
    }
    pushToCloudWatch.error(loggerBody)
    res.status(500).json({ success: 'Failed. Error creating Stripe setup intent', error: e })
  }
});


/****************************
* Create Payment Intent *
****************************/
app.post('/payment-intent/:tenantId', async function(req, res) {
  // collect parameters
  const tenantId = req.params.tenantId
  
  // get tenant from Dyanmo
  var params = {
    ExpressionAttributeNames: {
      '#id': 'id',
      '#g': 'group'
    },
    ExpressionAttributeValues: {
      ':id' : tenantId
    },
    TableName: process.env.API_HERA_TENANTTABLE_NAME,
    KeyConditionExpression: '#id = :id',
    ProjectionExpression: 'id,#g,companyName,accountPremiumStatus,trialExpDate,discountPercent,discountFixed,discountPercentLabel,discountFixedLabel,stripeCustomerId,stripeBillingEmail',
  };
  var queryResult = await queryDynamoRecords(params);
  var tenant = queryResult.Items[0]
  const group = tenant.group
  console.log({tenant: tenant})
  
  // get oustanding invoices from Dynamo
  params = {
    ExpressionAttributeValues: {
      ':g': group,
      ':s': "Payment Error",
    },
     ExpressionAttributeNames: {
      '#g': 'group',
      '#s': 'status',
      '#m': 'month',
      '#y': 'year'
    },
    KeyConditionExpression: '#g = :g AND #s = :s',
    ProjectionExpression: 'id,invoiceTotal,discountPercent,discountFixed,#m,#y',
    TableName: process.env.API_HERA_INVOICETABLE_NAME,
    IndexName: 'byGroupAndStatus',
  };
  queryResult = await queryDynamoRecords(params);
  var invoices = queryResult.Items
  console.log({invoices: invoices})

  // get customer's cards from Dynamo
  params = {
    ExpressionAttributeValues: {
      ':g': group,
    },
    ExpressionAttributeNames: {
      '#g': 'group',
    },
    KeyConditionExpression: '#g = :g',
    ProjectionExpression: 'id, stripePaymentMethodId, active',
    TableName: process.env.API_HERA_CARDTABLE_NAME,
    IndexName: 'byGroup',
  };
  queryResult = await queryDynamoRecords(params);
  var cards = queryResult.Items
  console.log({cards: cards})
  
  // find active card
  var activeCard = cards.find(card =>{
      return card.active == true
  })
  const activeCardDynamoId = activeCard.id
  const paymentMethodId = activeCard.stripePaymentMethodId
  console.log({paymentMethodId: paymentMethodId})
  
  
  // get payment method from Stripe
  await initStripe();
  const paymentMethod = await stripe.paymentMethods.retrieve(
    paymentMethodId
  );
  console.log({paymentMethod: paymentMethod})
 
  // create a payment intent in Stripe for each outstanding invoice
  // invoices.forEach(async invoice =>{
  var allInvoicesPaid = true
  for(const invoice of invoices){
    var status = ''
    const month = invoice.month
    const monthName = months[invoice.month]
    const year = invoice.year
    const customerId = tenant.stripeCustomerId
    
    // set up totals
    const taxRate = 0
    const fixedDiscount = invoice.discountFixed ? invoice.discountFixed : 0
    const percentDiscount = invoice.discountPercent ? invoice.discountPercent / 100 * invoice.invoiceTotal : 0
    const subtotalLessDiscount = invoice.invoiceTotal - fixedDiscount - percentDiscount
    const subtotalPlusTaxes = (subtotalLessDiscount + subtotalLessDiscount * taxRate).toFixed(2)
    const invoiceTotal = parseInt(subtotalPlusTaxes.replace(/[^0-9]/g, '')) // pass total in cents to Stripe
    console.log({invoiceTotal: invoiceTotal})
    
    try {
      const paymentIntent = await stripe.paymentIntents.create({
        amount: invoiceTotal, 
        currency: 'usd',
        customer: customerId,
        payment_method: paymentMethodId,
        off_session: true,
        confirm: true,
      });
      status = 'Paid'
    } catch (err) {
      // Error code will be authentication_required if authentication is needed
      console.log('Error code is: ', err.code);
      console.log({error: err})
      const paymentIntentRetrieved = await stripe.paymentIntents.retrieve(err.raw.payment_intent.id);
      console.log('PI retrieved: ', paymentIntentRetrieved.id);
        
      // send error to billing email if necessary
      const subject = `Hera: Payment Issue for ${tenant.companyName} for ${monthName} ${year}`
      const header = "Payment Issue"
      const time = new Date().getTime()
      var emailBody = `There was an issue charging your active card for ${monthName} ${year}. Please resolve the errors listed below by managing your saved cards through your <a href="dsp.herasolutions.app/settings/account-details">Hera account settings</a>.<br><br>${err}`
      var createParams = {
        TableName: process.env.API_HERA_MESSAGETABLE_NAME,
        Item: {
          id: `${year}-${month+1}-${tenant.id}-err-${time}`,
          messageTenantId: tenant.id,
          createdAt: new Date().toISOString(),
          updatedAt: new Date().toISOString(),
          group: group,
          subject: subject,
          bodyText: emailBody,
          bodyHtml: generateTemplate(header, emailBody, tenant.companyName),
          channelType: 'EMAIL',
          messageType: 'billingError',
          destinationEmail: tenant.stripeBillingEmail
        }
      };
      var create = await createDynamoRecord(createParams);

      // add error message to card in Dynamo
      var updateCardParams = {
        TableName: process.env.API_HERA_CARDTABLE_NAME,
        Key:{
            "id": activeCardDynamoId
        },
        UpdateExpression: "set #a = :a",
        ExpressionAttributeNames:{
            "#a": "chargeError",
           
        },
        ExpressionAttributeValues:{
            ":a": err.raw.message,
        },
        ReturnValues:"UPDATED_NEW"
      };
      await updateDynamoRecord(updateCardParams);

      status = 'Payment Error'
      allInvoicesPaid = false
    }
    
    // update status of invoice
    var updateParams = {
      TableName: process.env.API_HERA_INVOICETABLE_NAME,
      Key:{
          "id": invoice.id
      },
      UpdateExpression: "set #a = :a",
      ExpressionAttributeNames:{
          "#a": "status",
         
      },
      ExpressionAttributeValues:{
          ":a": status,
      },
      ReturnValues:"UPDATED_NEW"
    };
    await updateDynamoRecord(updateParams);
  // })
  } 
  
  // update tenant to not be marked as overdue
  if(allInvoicesPaid){
    let currentAccountStatus = tenant.accountPremiumStatus
    currentAccountStatus = currentAccountStatus.filter(e => e !== 'unpaid'); 
    let payOutstandingByDate = ''
    
    let updateParams = {
      TableName: process.env.API_HERA_TENANTTABLE_NAME,
      Key:{
        "id": tenant.id
      },
      UpdateExpression: "set #a = :a, #b = :b",
      ExpressionAttributeNames:{
        "#a": "accountPremiumStatus",
        "#b": "payOutstandingByDate"
      },
      ExpressionAttributeValues:{
        ":a": currentAccountStatus,
        ":b": payOutstandingByDate
      },
      ReturnValues:"UPDATED_NEW"
    };
    await updateDynamoRecord(updateParams);
  }
  
  res.json({success: 'post call succeed!', url: req.url, body: req.body, invoices: invoices, allInvoicesPaid: allInvoicesPaid})
});


app.listen(3000, function() {
    console.log("App started")
});

// Export the app object. When executing the application local this does nothing. However,
// to port it to AWS Lambda we will create a wrapper around that will load the app from
// this file
module.exports = app


function queryDynamoRecords(params){
  return new Promise((resolve, reject) => {
    // Create DynamoDB service object
    ddb.query(params, function(err, data) {
      if (err) {
        console.error("Unable to get items. Error JSON:", JSON.stringify(err, null, 2));
        reject(err)
      } else {
        console.log("Get Item succeeded:", JSON.stringify(data, null, 2));
        resolve(data)
      }
    });
  })
}

function createDynamoRecord(params){
    console.log("Creating Record");
    return new Promise((resolve, reject) => {
        ddb.put(params, function(err, data) {
          if (err) {
            console.log("Unable to put item. Error JSON:", JSON.stringify(err, null, 2));
            reject(err)
          } else {
            console.log("PutItem succeeded:", JSON.stringify(data, null, 2));
            resolve(data)
          }
        });
    
    
  })
}

function updateDynamoRecord(params){
    return new Promise((resolve, reject) => {
        ddb.update(params, function(err, data) {
            if (err) {
                console.log("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
                resolve(data)
            }
        });
  })
    
}