const { getSecretValue } = require('/opt/nodejs/ssm')
let stripe;
const { buildUpdateParams, scanDynamoRecords, updateDynamoRecord } = require("./dynamodbHelper");

async function initStripe(){
    if(!stripe){
        console.log("Initializing Stripe Module");
        const stripeKey = await getSecretValue(process.env.STRIPE_SECRET_KEY)
        stripe = require('stripe')(stripeKey);
    }
}

async function hera632(){
    console.log("hera632")
    await initStripe();
    let allTenants = await getAllTenants()
    let tenants = allTenants.filter(tenant => tenant.stripeCustomerId)
    await Promise.all(tenants.map(async (tenant) => {
        try{
            const customer = await stripe.customers.retrieve(tenant.stripeCustomerId);
            if(customer.id){
                await stripe.customers.update(
                    tenant.stripeCustomerId,
                    {
                        name: tenant.companyName,
                        email: tenant.stripeBillingEmail.trim(),
                        description: tenant.companyName
                    }
                );
            }
        }catch(e){
            console.log(e)
        }
    }))
}

async function getAllTenants() {
    try {
        let tenants = [];
        let lastEvaluatedKey = null;
        do {
            let params = {   
                TableName: process.env.API_HERA_TENANTTABLE_NAME,
                ProjectionExpression: "id, companyName, stripeCustomerId, stripeBillingEmail",
                ExclusiveStartKey: lastEvaluatedKey,
                
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            tenants = [...tenants, ...items];
        } while (lastEvaluatedKey);

        return tenants;
    } catch (err) {
        console.log("[getAllTenants] Error in function getAllTenants", err);
        return {
            error: err,
        };
    }
}

module.exports = {
    hera632,
};