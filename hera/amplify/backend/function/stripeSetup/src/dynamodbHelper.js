const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient();

function buildUpdateParams(table, item, id) {
    let updateExpression = 'set '
    let expressionAttributeNames = {}
    let expressionAttributeValues = {}
    let i = 0
    delete item.id // dont update id or createdAt
    delete item.createdAt
    for (const [key, value] of Object.entries(item)) {
        if (value != undefined) {
            updateExpression += `#${i} = :${i},`
            expressionAttributeNames[`#${i}`] = key
            expressionAttributeValues[`:${i}`] = value
            i++
        } else continue
    }
    updateExpression = updateExpression.slice(0, -1)

    let updateParams = {
        TableName: table,
        Key: {
            "id": id
        },
        ConditionExpression: 'attribute_exists(id)',
        UpdateExpression: updateExpression,
        ExpressionAttributeNames: expressionAttributeNames,
        ExpressionAttributeValues: expressionAttributeValues,
        ReturnValues: "UPDATED_NEW"
    };

    return updateParams
}

function buildQueryParams(table, fields, value, index, startKey) {
    let projectionExpression = 'id,'
    let expressionAttributeNames = {}
    let expressionAttributeValues = {}
    let i = 0
    for (const field of fields) {
        projectionExpression += `#${i},`
        expressionAttributeNames[`#${i}`] = field
        i++
    }
    projectionExpression = projectionExpression.slice(0, -1)

    let queryParams = {
        ExpressionAttributeValues: {
            ':0': value
        },
        ExpressionAttributeNames: expressionAttributeNames,
        KeyConditionExpression: '#0 = :0',
        ProjectionExpression: projectionExpression,
        TableName: table,
        IndexName: index,
        ExclusiveStartKey: startKey
    };

    return queryParams
}

function createDynamoRecord(params) {
    return new Promise((resolve, reject) => {
        ddb.put(params, function(err, data) {
            if (err) {
                console.error("Unable to create item. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                console.log("Create item succeeded:", JSON.stringify(data, null, 2));
                resolve(data)
            }
        });
    })
}

function updateDynamoRecord(params) {
    return new Promise((resolve, reject) => {
        ddb.update(params, function(err, data) {
            if (err) {
                console.log("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                console.log("Update Item succeeded:", JSON.stringify(data, null, 2));
                resolve(data)
            }
        });
    })

}

function scanDynamoRecords(params) {
    return new Promise((resolve, reject) => {
        // Create DynamoDB service object
        ddb.scan(params, function(err, data) {
            if (err) {
                console.error("Unable to get items. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                //console.log("Get Item succeeded:", JSON.stringify(data, null, 2));
                resolve(data)
            }
        });
    })
}

function queryDynamoRecords(params) {
    return new Promise((resolve, reject) => {
        // Create DynamoDB service object
        ddb.query(params, function(err, data) {
            if (err) {
                console.error("Unable to query items. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                // console.log("Get items succeeded:", JSON.stringify(data, null, 2));
                resolve(data)
            }
        });
    })
}

function dynamoBatchWriter(params) {
    return ddb.batchWrite(params, function (err, data) {
        if (err) console.log('---ERROR batchExecuteStatement', err, err.stack); // an error occurred
        else console.log('---SUCCESS batchExecuteStatement', data);           // successful response
    }).promise();
}


function deleteDynamoRecord(params){
    console.log("Creating Record");
    return new Promise((resolve, reject) => {
        ddb.delete(params, function(err, data) {
          if (err) {
            console.log("Unable to delete item. Error JSON:", JSON.stringify(err, null, 2));
            reject(err)
          } else {
            console.log("DeleteItem succeeded:", JSON.stringify(data, null, 2));
            resolve(data)
          }
        });
    
    
  })
}

module.exports = {
    buildUpdateParams,
    buildQueryParams,
    createDynamoRecord,
    updateDynamoRecord,
    scanDynamoRecords,
    queryDynamoRecords,
    dynamoBatchWriter,
    deleteDynamoRecord
}