function validateInput(data) {

    if (!data.group) {
        return stateResponse('need to define the group.')
    }

    if (!data.type) {
        return stateResponse('need to define the type.')
    }
    
    const input = {
        group: data.group,
        type: data.type,
        shortenId: data.shortenId
    };

    if (data.shortenUrlCounselingId) input.shortenUrlCounselingId = data.shortenUrlCounselingId
    if (data.shortenUrlAttachmentId){
        input.shortenUrlAttachmentId = data.shortenUrlAttachmentId
        input.attachmentId = data.shortenUrlAttachmentId
    }
    if (data.shortenUrlDailyLogId) input.shortenUrlDailyLogId = data.shortenUrlDailyLogId
    if (data.urlToken) input.urlToken = data.urlToken
    if (data.expirationTTL) input.expirationTTL = data.expirationTTL
    
    return stateResponse(input, true)
}

function createUrlWithDomain(shortenId, type, domain) {
    const baseUrl = domain
    const pathAuthorization = 'u'
    const shortUrl = `${baseUrl}/${pathAuthorization}/${shortenId}`
    return { shortUrl,shortenId }
}

function stateResponse(message, state = false) {
    return {
        result: (!state) ? `Error function, ${message}` : message,
        response: state
    }
}

module.exports = {
    validateInput,
    createUrlWithDomain,
    stateResponse
}