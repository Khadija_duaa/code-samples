// Import required packages
require('es6-promise').polyfill();
require('isomorphic-fetch');
const AWSAppSyncClient = require('aws-appsync').default;
const { AUTH_TYPE } = require('aws-appsync');
const gql = require('graphql-tag');

class AppSyncClient {
  constructor(vars,config) {
    // Initialize AWS config
    config.update({
      region: vars.REGION
    });

    // Initialize the AppSync client
    this.client = new AWSAppSyncClient({
      url: vars.API_HERA_GRAPHQLAPIENDPOINTOUTPUT,
      region: vars.REGION,
      auth: {
        type: AUTH_TYPE.AWS_IAM,
        credentials: config.credentials,
      },
      disableOffline: true,
    });
  }

  async executeMutation(mutation, variables) {
    try {
      console.log(gql(mutation));
      console.log(variables);
      const response = await this.client.mutate({
        mutation: gql(mutation),
        variables,
        fetchPolicy: 'no-cache',
      });
      return response.data;
    } catch (error) {
      console.log('Error while trying to mutate data');
      console.log(error);
      throw JSON.stringify(error);
    }
  }

  async executeQuery(query, variables) {
    try {
      console.log(gql(query));
      const response = await this.client.query({
        query: gql(query),
        variables,
        fetchPolicy: 'network-only',
      });
      return response.data;
    } catch (error) {
      console.log('Error while trying to fetch data');
      throw JSON.stringify(error);
    }
  }
}

module.exports = { AppSyncClient };