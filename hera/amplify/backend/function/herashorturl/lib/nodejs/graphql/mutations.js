module.exports.createShortenUrl = /* GraphQL */ `
  mutation CreateShortenUrl(
    $input: CreateShortenUrlInput!
    $condition: ModelShortenUrlConditionInput
  ) {
    createShortenUrl(input: $input, condition: $condition) {
      id
      shortenId
    }
  }
`;