module.exports.shortenUrlsByShortenId = /* GraphQL */ `
  query ShortenUrlsByShortenId(
    $shortenId: ID
    $sortDirection: ModelSortDirection
    $filter: ModelShortenUrlFilterInput
    $limit: Int
    $nextToken: String
  ) {
    shortenUrlsByShortenId(
      shortenId: $shortenId
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        type
        shortenId
        expirationTTL
        urlToken
        createdAt
        updatedAt
        attachment {
            id
            group
            createdAt
            s3Key
            expirationDate
            contentType
            fileSize
            updatedAt
        }
        counseling {
            id
            group
            date
            severity {
              id
              option
            }
            refusedToSign
            signatureAcknowledge
            signature
            counselingNotes
            employeeNotes
            correctiveActionSummary
            priorDiscussionSummary
            consequencesOfFailure
            dateSent
            status
            dateSigned
            tenantId
            createdFrom
            createdAt
            updatedAt
        }
        dailyLog {
          id
            group
            type
            date
            documents {
            items {
                group
                name
                key
                type
            }
            nextToken
            }
        }
      }
      nextToken
    }
  }
`;

module.exports.shortenUrlsByAttachmentId = /* GraphQL */`
query ShortenUrlsByAttachmentId(
  $attachmentId: ID
  $sortDirection: ModelSortDirection
  $filter: ModelShortenUrlFilterInput
  $limit: Int
  $nextToken: String
) {
  shortenUrlsByAttachmentId(
    attachmentId: $attachmentId
    sortDirection: $sortDirection
    filter: $filter
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      attachmentId
    }
    nextToken
  }
}
`;