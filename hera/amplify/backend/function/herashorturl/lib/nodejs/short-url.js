const { AppSyncClient } = require('./helpers/appSyncClient.js');

const {
    validateInput,
    createUrlWithDomain,
    stateResponse
} = require("./helpers/shortenUrl.js")

const {
    nanoid
} = require("nanoid")

const momentTz = require("moment-timezone")

const {
    createShortenUrl
} = require("./graphql/mutations.js")

const {
    shortenUrlsByShortenId,shortenUrlsByAttachmentId
} = require("./graphql/queries.js")

const TIME_UTC = '05:00:00Z'

class ShortenUrlGenerator {

    /**
     * Constructor function for the ShortenUrl class.
     * @param {Object} vars - An object containing variables needed to set up the AppSyncClient and domain for shortened URLs.
     * @param {string} vars.SHORTEN_URL_DOMAIN - A string representing the domain used for shortened URLs.
     * @param {string} vars.API_HERA_GRAPHQLAPIENDPOINTOUTPUT - A string representing the route API used for AppSyncClient.
     * @param {string} vars.REGION - A string representing the region used for AppSyncClient.
     */
    constructor(vars,config) {
        this.client = new AppSyncClient(vars,config);
        this.domain = vars.SHORTEN_URL_DOMAIN;
    }

    /**
     * Generates a shortened URL from the provided data and returns the shortened URL with a success state.
     * @param {Object} dataInput - An object containing variables needed to generate shorten url.
     * @param {string} dataInput.group - A string representing the tenant group.
     * @param {string} dataInput.type - A string representing the place of application (COUNSELING, ATTACHMENT, DAILYLOG).
     * @param {string} [dataInput.shortenUrlCounselingId] - A string representing the Counseling Id.
     * @param {string} [dataInput.shortenUrlAttachmentId] - A string representing the Attachment Id.
     * @param {string} [dataInput.shortenUrlDailyLogId] - A string representing the DailyLog Id.
     * @param {string} [dataInput.urlToken] - A string representing the token.
     * @returns {Object} The shortened URL, shortenId with a success state.
     */
    async generateShortenUrl(dataInput) {
        let typeShortenUrl = null
        if (dataInput.type === 'COUNSELING' || dataInput.counselingId) typeShortenUrl = 'COUNSELING'
        if (dataInput.type === 'ATTACHMENT' || dataInput.attachmentId) typeShortenUrl = 'ATTACHMENT'
        if (dataInput.type === 'DAILYLOG' || dataInput.dailyLogId) typeShortenUrl = 'DAILYLOG'

        const data = {
            group: dataInput.group,
            type: typeShortenUrl,
            shortenUrlCounselingId: (dataInput.counselingId) ? dataInput.counselingId : null,
            shortenUrlAttachmentId: (dataInput.attachmentId) ? dataInput.attachmentId : null,
            shortenUrlDailyLogId: (dataInput.dailyLogId) ? dataInput.dailyLogId : null,
            urlToken: (dataInput.urlToken) ? dataInput.urlToken : null,
            expirationTTL: (dataInput.expirationDate) ? this.convertDateToUnixEpochTime(dataInput.expirationDate) : null
        }

         const shortenId = nanoid(12);
         const shortenIdValidated = await this.checkDuplicateShortenId(shortenId);
         data.shortenId = shortenIdValidated;

        try {
            const inputShorteUrl = validateInput(data);
            if (!inputShorteUrl.response) return stateResponse(inputShorteUrl.result);
            const input = inputShorteUrl.result;
            await this.client.executeMutation(createShortenUrl, {
                input
            });
            const result = createUrlWithDomain(shortenId, data.type, this.domain);
            return stateResponse(result, true);

        } catch (err) {
            console.log("[generateShortenUrl] Error in function generateShortenUrl", err);
            return stateResponse("Did not register successfully.");
        }
    }

    /**
    Checks if a shortenId is already in use by querying the database. If it is, generates a new shortenId and recursively calls itself until an unused shortenId is found.
    @param {string} shortenId - The shortenId to check.
    @returns {string} - The shortenId that is not in use.
    @throws {Error} - If an error occurs while querying the database.
    */

    async checkDuplicateShortenId(shortenId) {
        try {
            const result = await this.client.executeQuery(shortenUrlsByShortenId, {
                shortenId: shortenId
            });

            if (result.shortenUrlsByShortenId.items.length > 0) {
                const otherShortenId = nanoid(12);
                return this.checkDuplicateShortenId(otherShortenId);
            } else {
                return shortenId;
            }

        } catch (error) {
            console.log("[checkDuplicateShortenId] Error in function checkDuplicateShortenId", error);
            throw error;
        }
    }

    async checkDuplicateAttachment(input) {

        const { attachmentId  } = input

        try {

            const result = await this.client.executeQuery(shortenUrlsByAttachmentId, { attachmentId:attachmentId  });
            const resultValidate = result.shortenUrlsByAttachmentId.items.length > 0;
            return resultValidate;

        } catch (error) {
            console.log("[checkDuplicateAttachment] Error in function checkDuplicateAttachment", error);
            throw error;
        }
    }

    convertDateToUnixEpochTime(expirationDate){
        const containsSlashAndDash = /[\/-]/.test(expirationDate);
        let date = null;

        if (containsSlashAndDash) {
            date = new Date(expirationDate); 

        } else {
            const validatedExpirationDate = expirationDate.length === 10
            ? parseInt(expirationDate) * 1000
            : parseInt(expirationDate);
            date = momentTz
                .duration(validatedExpirationDate, "milliseconds")
                .asMilliseconds();
        }

        const formatDate = momentTz(date).format("YYYY-MM-DD");
        const convertExpirationDate = new Date(`${formatDate} ${TIME_UTC}`);
        return Math.floor(convertExpirationDate.getTime() / 1000);
    }
}

module.exports = { ShortenUrlGenerator };