const { nanoid } = require("nanoid");
const { createDynamoRecord, updateDynamoRecord, buildUpdateParams } = require("./dynamodbHelper");
const { executeMutation } = require('./appSync.js');
const { updateMessage } = require('./graphql.js');

function isCodificationGSM7(text='') {
  const regex = /^[0-9A-Za-z\r\n !"#\$%&'’()*+,\-.\/:;<=>?@\[\\\]^_{}|~]+$/;
  return regex.test(text);
}

function calculateBytesGSM7(message='') {
  const specialCharacters = {
    '\u000C': true,  //line break
    '\u005E': true,  //Circumflex
    '\u007B': true,  // "{"
    '\u007D': true,  // "{"
    '\u005C': true,  // "\"
    '\u005B': true,  // "["
    '\u007E': true,  // ´
    '\u005D': true,  // "]"
    '\u007C': true,  // |
    '\u20AC': true   // Euro
  };

  let bytes = 0;
  for (let i = 0; i < message.length; i++) {
    const char = message.charAt(i);
    if (specialCharacters[char]) {
      bytes += 2;
    } else {
      bytes += 1;
    }
  }
  return bytes;
}

function calculateBytesUTF16(message='') {
  const encoder = new TextEncoder();
  const bytes = encoder.encode(message);
  //when the message is encoded as UTF-16, twice as many bytes are used.
  return bytes.length * 2;
}

function detectEncodingAndCalculateMaxBytes(message) {
  //134 characters x 10 parts UTF-16 encoding
  let maxBytesPerPart = 1340
  let encoding = "UTF-16"
  
  if (isCodificationGSM7(message)) {
    //153 characters x 10 parts
    maxBytesPerPart = 1530
    encoding = "GSM-7"
    return { maxBytesPerPart,  encoding }
  }
  //UTF-16 encoding
  return { maxBytesPerPart, encoding }
}

function calculateBytes(message, encoding=''){
  if(encoding == "GSM-7"){
    const totalBytes = calculateBytesGSM7(message);
    return totalBytes
  }
  //UTF-16
  const totalBytes = calculateBytesUTF16(message);
  return totalBytes
}


function splitMessage(message, exceededParts=0) {
  let { maxBytesPerPart, encoding } = detectEncodingAndCalculateMaxBytes(message)
  let parts = [];
  let currentPart = '';
  if (exceededParts > 0) {
    //134 or 153 bytes per part
    const decrement = encoding === "GSM-7" ? 153 : 134;
    maxBytesPerPart = maxBytesPerPart - (decrement * exceededParts);
    maxBytesPerPart = maxBytesPerPart < decrement ? decrement : maxBytesPerPart;
  }

  const lines = message.split(/\r?\n/);
  
  for (let i = 0; i < lines.length; i++) {
    const line = lines[i]; 
    const bytes = calculateBytes(line, encoding);
    if (bytes <= maxBytesPerPart) {
      if (calculateBytes(currentPart + line, encoding) > maxBytesPerPart && currentPart !== '') {
        parts.push(currentPart.trim());
        currentPart = '';
      }
      currentPart += line + '\n';
     
    } else {
      const words = line.split(' ');
      for (let j = 0; j < words.length; j++) {
        const word = words[j];
        if (calculateBytes(currentPart + word + ' ', encoding) > maxBytesPerPart && currentPart !== '') {
          parts.push(currentPart.trim());
          currentPart = '';
        }
        currentPart += word + ' ';
      }
    }
  }

  if (currentPart.trim() !== '') {
    parts.push(currentPart.trim());
  }
  return parts;
}


/**
 * Create message record
 * 
 */
async function createMessage(messageInfo, table) {
  try {
    const createdAt = new Date().toISOString();
    const updatedAt = new Date().toISOString();
    const channelType = messageInfo.channelType;
    const messageType =  messageInfo.messageType;

    //parameters to create message
    let createParams = {
      TableName: table,
      Item: {
        id: nanoid(),
        __typename: "Message",
        staffId: messageInfo.staffId,
        createdAt,
        updatedAt,
        group: messageInfo.group,
        bodyText: messageInfo.bodyText,
        channelType,
        messageType,
        isReadS: "false",
        "messageType#createdAt": messageType + "#" + createdAt,
        "channelType#createdAt": channelType + "#" + createdAt,
        "channelType#isReadS#createdAt": channelType + "#false#" + createdAt,
        destinationNumber: messageInfo.destinationNumber,
        destinationEmail: messageInfo.destinationEmail,
        destinationName: messageInfo.destinationName,
        owner: messageInfo.owner,
        senderName: messageInfo.senderName,
        senderId: messageInfo.senderId,
        messageTenantId: messageInfo.messageTenantId
      }
    };
    if(messageInfo.hasParts){
      createParams.Item.hasParts = messageInfo.hasParts;
    }

    //create message
    await createDynamoRecord(createParams);
  } catch (error) {
    console.log("Error in function createMessage", error);
  }
}


/**
 * Update message record with send information 
 * 
 */
async function updateMessageStatus(record, status, hasParts=false){
  try {
    if(record.id) {
      let input = {
        id: record.id,
        group: record.group,
        smsStatus: status.smsStatus,
        smsSendInformation: status.smsSendInformation,
        emailStatus: status.emailStatus,
        emailSendInformation: status.emailSendInformation,
        carrierMessageId: status.carrierMessageId || 'No Carrier Message ID'
      }
      if(hasParts){
        input.bodyText = record.bodyText;
        input.hasParts = record.hasParts;
      }
      let response = await executeMutation(updateMessage, {input})
      console.log('[updateMessageStatus] response:', response)
    } 
    else {
      console.log('[updateMessageStatus] error', record)
    }
  } catch (error) {
    console.log("Error in function updateMessageStatus", error);
  }
}

module.exports = { splitMessage, createMessage, updateMessageStatus};
