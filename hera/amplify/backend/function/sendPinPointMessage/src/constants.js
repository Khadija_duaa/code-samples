const PENDING_STATUS = 'Pending'
const ERROR_STATUS = 'Error'
const SUCCESS_STATUS = 'Success'

module.exports = {
    PENDING_STATUS,
    ERROR_STATUS,
    SUCCESS_STATUS
};