/* Amplify Params - DO NOT EDIT
	API_HERA_ATTACHMENTTABLE_ARN
	API_HERA_ATTACHMENTTABLE_NAME
	API_HERA_GRAPHQLAPIENDPOINTOUTPUT
	API_HERA_GRAPHQLAPIIDOUTPUT
  API_HERA_STAFFTABLE_ARN
	API_HERA_STAFFTABLE_NAME
	ENV
	REGION
	STORAGE_HERA_BUCKETNAME
Amplify Params - DO NOT EDIT */
//Init AWS SDK
var AWS = require('aws-sdk');

var s3 = new AWS.S3({apiVersion: '2006-03-01'});
var documentClient = new AWS.DynamoDB.DocumentClient({apiVersion: '2012-08-10'})
var pinpoint = new AWS.Pinpoint({region: process.env.PIN_POINT_REGION}); 

// Make sure the SMS channel is enabled for the projectId that you specify.
// See: https://docs.aws.amazon.com/pinpoint/latest/userguide/channels-sms-setup.html
var projectId = process.env.PROJECT_ID;
var senderEmail = process.env.SENDER_EMAIL;

// You need a dedicated long code in order to use two-way SMS. 
// See: https://docs.aws.amazon.com/pinpoint/latest/userguide/channels-voice-manage.html#channels-voice-manage-request-phone-numbers
// var originationNumber = process.env.ORIGINATION_NUMBER;

const { executeMutation, executeQuery } = require('./appSync.js');
const { updateStaff, getTenant, createNotification } = require('./graphql.js');
const { isTestEnv, usingValidTestNumber, isValidTestEmail } = require('./testEnvHelpers.js');
const { splitMessage, createMessage, updateMessageStatus } = require('./utils.js');
const { PENDING_STATUS, ERROR_STATUS, SUCCESS_STATUS } = require('./constants.js');
const { nanoid } = require('nanoid');
const { updateDynamoRecord, buildUpdateParams } = require("./dynamodbHelper.js");

exports.handler = function(event, context) {

  try {

    //eslint-disable-line
    console.log(JSON.stringify(event, null, 2));
    // let interval = 1100 // delays each iteration by 1.1 seconds
    event.Records.forEach((record, i) => {
      // setTimeout( () => {
        console.log(record.eventID);
        console.log(record.eventName);
        console.log('DynamoDB Record: %j', record.dynamodb);
        let eventSourceARN = record.eventSourceARN
        let table = eventSourceARN.substring(
            eventSourceARN.lastIndexOf("table/") + 6, 
            eventSourceARN.lastIndexOf("/stream")
        )
        if(record.eventName == "INSERT"){
          sendMessage(record.dynamodb, table)
        }
      // }, i * interval)
    });

  } catch(e) {
    console.log('sendPinPointMessage error', e)
  }

}

let messageServiceProvider = '';

async function sendMessage(dynamoRecord, table) {
  
  //Parse Dynamo Record
  let subject = dynamoRecord.NewImage.subject ==                         undefined ? null : dynamoRecord.NewImage.subject.S
  let bodyText = dynamoRecord.NewImage.bodyText ==                       undefined ? null : dynamoRecord.NewImage.bodyText.S
  let bodyHtml = dynamoRecord.NewImage.bodyHtml ==                       undefined ? null : dynamoRecord.NewImage.bodyHtml.S
  let messageAttachmentId = dynamoRecord.NewImage.messageAttachmentId == undefined ? null : dynamoRecord.NewImage.messageAttachmentId.S
  let channelType = dynamoRecord.NewImage.channelType ==                 undefined ? ""   : dynamoRecord.NewImage.channelType.S ? dynamoRecord.NewImage.channelType.S.toUpperCase() : ""
  let destinationNumber = dynamoRecord.NewImage.destinationNumber ==     undefined ? null : dynamoRecord.NewImage.destinationNumber.S
  let destinationEmail = dynamoRecord.NewImage.destinationEmail ==       undefined ? null : dynamoRecord.NewImage.destinationEmail.S
  let messageStaffId = dynamoRecord.NewImage.staffId ==           undefined ? null : dynamoRecord.NewImage.staffId.S
  let messageTenantId = dynamoRecord.NewImage.messageTenantId ==         undefined ? null : dynamoRecord.NewImage.messageTenantId.S
  let tenant = null
  let group = dynamoRecord.NewImage.group ==         undefined ? null : dynamoRecord.NewImage.group.S
  let owner = dynamoRecord.NewImage.owner ==         undefined ? null : dynamoRecord.NewImage.owner.S
  let destinationName = dynamoRecord.NewImage.destinationName ==         undefined ? null : dynamoRecord.NewImage.destinationName.S
  let createdAt = dynamoRecord.NewImage.createdAt ==         undefined ? null : dynamoRecord.NewImage.createdAt.S

  let senderName = dynamoRecord.NewImage.senderName ? dynamoRecord.NewImage.senderName.S : null
  let senderId = dynamoRecord.NewImage.senderId ? dynamoRecord.NewImage.senderId.S : null
  let messageType = dynamoRecord.NewImage.messageType ? dynamoRecord.NewImage.messageType.S: null

  // Exit if type is response, to prevent infinite loop
  if (channelType == "RESPONSE") {
    console.log("Type is response, exiting.")
    return
  }

  console.log({subject:subject})
  console.log({messageTenantId: messageTenantId})

  if(messageTenantId){
    tenant = await getTenantById(messageTenantId)
    console.log({tenant: tenant})
    var originationNumber = tenant.originationNumber;
    messageServiceProvider = tenant.messageServiceProvider;
  }
  
  if(!originationNumber){
    console.error("Error: Could not send SMS, missing originationNumber")
    return
  }

  
  console.log({originationNumber: originationNumber})
  
  //Get an attachment record's data
  if(messageAttachmentId){
     var params = {
      TableName : process.env.API_HERA_ATTACHMENTTABLE_NAME,
      Key: {
        "id": messageAttachmentId
      }
    };
    var findResult = await getDynamoRecord(params)
    console.log('findResult', JSON.stringify(findResult))   
    var contentType = findResult.Item.contentType
    var fileSize = findResult.Item.fileSize
    var attachmentLink = findResult.Item.s3Key
    
    //appends link to the end of text body
    if (contentType){
      var nonImageUrl = process.env.WINDOW_ORIGIN + '/f/' + messageAttachmentId
      console.log('---nonImageUrl', nonImageUrl)
      bodyText = ( bodyText ? bodyText + ' ' : '' ) + nonImageUrl     
    } 
  }

  var record = {
    id: dynamoRecord.NewImage.id.S,
    group: group,
    subject: subject,
    bodyText: bodyText,
    bodyHtml: bodyHtml,
    contentType: contentType,
    fileSize: fileSize,
    attachmentLink: attachmentLink,
    channelType: channelType,
    originationNumber: originationNumber,
    destinationNumber: destinationNumber,
    destinationEmail: destinationEmail,
    messageStaffId: messageStaffId,
    table: table,
    createdAt: createdAt
  }
  console.log('record', JSON.stringify(record))
  
  var sendResults = {
    smsStatus: "Not Sent",
    smsSendInformation: "User is opted out",
    emailStatus: "Not Sent",
    emailSendInformation: "User is opted out"
  }
  
  let messagesPendingToSend = []
  let messageSend = null

  //channelType can be "SMS", "EMAIL", or "SMS/EMAIL"
  if(channelType.includes("SMS")){
    try{
      const { response, messages } = await sendSMS(record, originationNumber, tenant)
      await updateStaffLastSms(record, channelType)
      sendResults.smsStatus = PENDING_STATUS
      sendResults.smsSendInformation = "This message has been sent to the phone carrier and we are waiting for confirmation that they received it."
      sendResults.carrierMessageId = response && response.data ? response.data.id : null
      messageSend = messages && messages.messageSend ? messages.messageSend : null
      messagesPendingToSend = messages && messages.messagesPending ? messages.messagesPending : []
    }catch(e){
      console.log('channelType error', e)
      sendResults.smsStatus = ERROR_STATUS
      sendResults.smsSendInformation = e
      // Only show user friendly error if error from Bandwidth
      if(typeof e === 'object' && e !== null && e.errorMessage) {
        sendResults.smsSendInformation = e.errorMessage
      } else if (e.response && e.response.request.host === 'api.telnyx.com' && e.response.data.errors.length) {
        sendResults.smsSendInformation = `${e.response.data.errors[0].detail} (${e.response.data.errors[0].code})`
        record.error = JSON.stringify(e.response.data.errors[0])
      } else if (e.message) {
        sendResults.smsSendInformation = e.message
      }
      
      if(owner) {
        console.log('---owner', JSON.stringify(owner))
        record.group = group
        record.owner = owner
        record.destinationName = destinationName
  
        await pushNotification(record)
      }

      if (tenant && record.destinationNumber) {
        record.tenantId = tenant.id
        record.companyName = tenant.companyName
        record.smsSendInformation = sendResults.smsSendInformation
        
        await sendErrorEmail(record)
      }
    }
  }
  if(channelType.includes("EMAIL")){
    try{
      var emailResults = await sendEmail(record)
      sendResults.emailStatus = PENDING_STATUS
      sendResults.emailSendInformation = emailResults
    }catch(e){
      sendResults.emailStatus = ERROR_STATUS
      sendResults.emailSendInformation = e
    }
  }

  const partId = nanoid()
  let totalParts = messagesPendingToSend.length + 1
  let partOrden = 1
  let hasParts = false
  if(messagesPendingToSend.length && messageSend){
    hasParts = true
    record.bodyText = messageSend
    record.hasParts = `${partId}-part-${partOrden}/${totalParts}`
  }
  
  //UPDATE MESSAGE STATUS
  await updateMessageStatus(record, sendResults, hasParts)

  if(messagesPendingToSend.length){
    let messageInfo = {
      staffId: messageStaffId,
      senderName: senderName,
      senderId: senderId,
      destinationName: destinationName,
      destinationNumber: destinationNumber,
      destinationEmail: destinationEmail,
      channelType: 'SMS',
      owner: owner,
      messageType: messageType,
      messageTenantId: tenant.id ? tenant.id : messageTenantId,
      group: tenant.group ? tenant.group: group
    }
    for(const sms of messagesPendingToSend){
      partOrden++
      messageInfo.hasParts = `${partId}-part-${partOrden}/${totalParts}`
      messageInfo.bodyText = sms
      //create missing parts
      await createMessage(messageInfo, table)
    }
  }
}

async function sendErrorEmail(record) {
  if (record.tenantId) {
    let subdomain = process.env.WINDOW_ORIGIN
    let tenantURL = `${subdomain}/system/tenants/${record.tenantId}`
    let serverTime = new Date(record.createdAt).toLocaleString()

    let content = `There was an error sending a message, here are the details: <br><br> 
  Error: ${record.smsSendInformation} <br>
  Origination number: ${record.originationNumber} <br>
  Destination number: ${record.destinationNumber} <br>
  Server time: ${serverTime} <br>
  Tenant: ${record.companyName} <br>
  Tenant page: <a href="${tenantURL}">${tenantURL}</a> 
  `

    if(record.error) {
      let details = `<br><br> <b>Error details:</b> <br> ${record.error}`
      content = content.concat(details)
    }

    let emailParams = {
      destinationEmail: process.env.SUPPORT_EMAIL,
      subject: 'Error sending message',
      bodyText: content,
      bodyHtml: content,
    }

    if (process.env.ENV === 'phasethree') {
      emailParams.subject = `(DEV) ${emailParams.subject}`
    }

    await sendEmail(emailParams)
  } else {
    console.log('[sendErrorEmail] Unable to send email. No tenant related.', JSON.stringify(record))
  }
}

async function pushNotification(record) {
  try {
    
    let description = "The following Associates did not receive the message:\n" + record.destinationName + "\nPlease resend your message to these Associates."
    let title = "Some messages could not be sent"
    
    if (!record.destinationNumber) {
      title = "Associate does not have a phone number"
      description = `${record.destinationName} does not have a phone number in their Associate profile. ${record.destinationName} did not receive this message. Please add their phone number to Hera then send this message again.`
    }

    var input = {
      group: record.group,
      owner: record.owner,
      title: title,
      description:  description,
      payload: JSON.stringify({}),
      clickAction: null,
      isRead: false
      
  }
  await executeMutation(createNotification, {input})

  } catch(e) {
    console.log('error pushNotification', e)
  }
}

async function sendSMS(record, originationNumber, tenant) {
  
    // TEST_TENANT_ID: 
    // A tenant that is allowed to use SMS in test environment
    let testTenant = !!process.env.TEST_TENANT_ID && !!tenant && tenant.id === process.env.TEST_TENANT_ID
    let useTestNumber = isTestEnv() && !testTenant
    var url = false;


    //--------VERIFY WE HAVE REQUIRED DATA--------//
    if(!record.destinationNumber){
      console.error("Error: Could not send SMS, missing required data")
      throw "Error: Could not send SMS, missing required data"
    }
    
    if(false && record.attachmentLink ){
      //Get file from S3
        if(record.contentType.includes("image") && record.fileSize < 525000  ){
            var params = {Bucket: process.env.STORAGE_HERA_BUCKETNAME, Key: 'public/' + record.attachmentLink, ResponseContentDisposition: 'inline', ResponseContentType: record.contentType};
            var url = s3.getSignedUrl('getObject', params);
            console.log('S3 signed URL', url)
        }
    }
        
    //---------FORMAT DATA----------//

    var destinationNumber = record.destinationNumber.replace(/[^0-9]/g, '');
    if (destinationNumber.length == 10) {
      destinationNumber = "+1" + destinationNumber;
    }
    
    console.log('mmmProvider', messageServiceProvider, originationNumber, destinationNumber);

    // Only replace number if we are in test environment and NOT using a test tenant
    if (useTestNumber) {          
      if(usingValidTestNumber()){
        destinationNumber = process.env.TEST_ENV_SMS_NUMBER
      }else{
        console.log('Error: Unable to send SMS message from test environment because a test number is not set up or configured correctly using the ENV variable TEST_ENV_SMS_NUMBER and the format +1##########.')
        return reject('Error: Unable to send SMS message from test environment because a test number is not set up or configured correctly using the ENV variable TEST_ENV_SMS_NUMBER and the format +1##########.')
      }
    }

    //---------BRANCHES BASED ON PIN POINT / BANDWIDTH ----------//
    if(messageServiceProvider === 'Telnyx'){
      destinationNumber = destinationNumber.includes('+')?destinationNumber:'+'+destinationNumber;

      // if in test environment, prepend company name to SMS
      let smsText = record.bodyText

      if (useTestNumber) {
        smsText = `TEST: ${tenant.companyName}. MSG: ${smsText}`
      }

      var sms = {
        "from": originationNumber,
        "to": destinationNumber,
        "text": smsText
      }
      
      if(url){
        sms.media_urls = [url]
      }
      console.log('Telnyx SMS input', JSON.stringify(sms))

      try {
        const messageParts = splitMessage(sms.text) || []
        if(messageParts.length > 1){
          const smsFirstPart = messageParts[0]
          const messagesPending = messageParts.slice(1)
          sms.text = smsFirstPart

          return await sendByTelnyx(sms, smsFirstPart, messagesPending)
        }
        return await sendByTelnyx(sms, sms.text, [])    
      } catch (error) { 
        if(error.response.data.errors && error.response.data.errors[0].code === '40302'){
          console.log('Error 40302: ', error.response.data.errors)
          const detail = error.response.data.errors[0].detail
          const parts = detail.match(/divided into (\d+)/i);

          let exceededParts = 0
          if(parts && parts[1]){
            exceededParts = parseInt(parts[1]) - 10
          }
          
          let messageParts = splitMessage(sms.text, exceededParts) || []
          const smsFirstPart = messageParts[0]
          const messagesPending = messageParts.slice(1)
          sms.text = smsFirstPart
          return await sendByTelnyx(sms, smsFirstPart, messagesPending)
        }
        throw error
      }
    }
}

async function sendByTelnyx(sms, messageSend, messagesPending){
  const axios = require('axios');
  const apiTelnyx = process.env.API_TELNYX
  const instance = axios.create({
      baseURL: 'https://api.telnyx.com/v2/'
  });
  instance.defaults.headers.common['Authorization'] = apiTelnyx;

  try{
    const response = await instance.post('messages', sms)
    console.log(`Telnyx Response:`, response.data)
    return {
      response: response.data,
      messages: {
        messageSend: messageSend,
        messagesPending: messagesPending
      }
    }
  }catch(error){
    console.error('Telnyx Error Response', error.response.data.errors);
    throw error
  }
}

/**
 * Send an email using aws pinpoint
 * @param(Object) record - DB Record with send info
 */
function sendEmail(record) {
  return new Promise((resolve, reject) => {
    
    //--------VERIFY WE HAVE REQUIRED DATA--------//
    if(!record.subject || !record.bodyHtml || !record.bodyText || !record.destinationEmail){
      console.log("Error: Could not send email, missing required data")
      reject("Error: Could not send email, missing required data")
    }
    
    //------- ENSURE TEST ENVIRONMENT USES TEST EMAIL ------//
    if (isTestEnv()) {      
      if(!!record.destinationEmail && !isValidTestEmail(record.destinationEmail)){ 
        console.log('Exiting sendEmail from the test environment. Destination email does not appear to be a test address:', record)
        return reject("Error attempting to send a message with an invalid test address")
      }
    }

    //----------CONSTRUCT EMAIL PARAMS----------//
    var charset = "UTF-8";
    var params = {
      ApplicationId: projectId,
      MessageRequest: {
        Addresses: {
          [record.destinationEmail]:{
            ChannelType: 'EMAIL'
          }
        },
        MessageConfiguration: {
          EmailMessage: {
            FromAddress: senderEmail,
            SimpleEmail: {
              Subject: {
                Charset: charset,
                Data: record.subject
              },
              HtmlPart: {
                Charset: charset,
                Data: record.bodyHtml
              },
              TextPart: {
                Charset: charset,
                Data: record.bodyText
              }
            }
          }
        }
      }
    };

    // also send to admin if billing error
    if(record.subject.toLowerCase().includes('payment issue')){
        params.MessageRequest.Addresses['admin@herasolutions.info'] = {
            ChannelType: 'EMAIL'
        }
    }
    
    //----------SEND EMAIL-------------//
    pinpoint.sendMessages(params, function(err, data) {
      // If something goes wrong, print an error message.
      if(err) {
        console.log(err.message);
        reject(err)
      } else {
        console.log(data)
        console.log(data['MessageResponse']['Result'][record.destinationEmail])
        let result = "Email sent! Message ID: " + data['MessageResponse']['Result'][record.destinationEmail]['MessageId']
        console.log(result);
        resolve(result)
      }
    });
  
  })
}




/**
 * Update the staff records last SMS time and last message 
 * */
async function updateStaffLastSms(record, channelType){
  
  if(record.messageStaffId) {
    const updateParams = {
      id: record.messageStaffId,
      smsLastMessageTimestamp: new Date().toISOString(),
      smsLastMessage: record.bodyText
    }
    const buildParams = buildUpdateParams(process.env.API_HERA_STAFFTABLE_NAME, updateParams, updateParams.id)
    await updateDynamoRecord(buildParams);
  } else {
    console.log('---updateStaffLastSms error', JSON.stringify(record))
  }
};

async function getTenantById(id){
  let input = {
    id: id
  }
  try{
    let response = await executeQuery(getTenant, input);
    return response.getTenant
  }
  catch(e){
    console.log('[getTenantById]', e)
    return e
  }

};


function getDynamoRecord(params){
  return new Promise((resolve, reject) => {
      documentClient.get(params, function(err, data) {
          if (err) {
                console.error("Unable to get item. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                console.log("Get Item succeeded:", JSON.stringify(data, null, 2));
                resolve(data)
            }
      });
  })
}