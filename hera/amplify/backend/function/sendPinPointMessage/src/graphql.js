const updateStaff = /* GraphQL */ `
  mutation UpdateStaff(
    $input: UpdateStaffInput!
    $condition: ModelStaffConditionInput
  ) {
    updateStaff(input: $input, condition: $condition) {
      id
    }
  }
`;

const getTenant = /* GraphQL */`
  query GetTenant($id: ID!){
    getTenant(id: $id){
      id
      originationNumber
      messageServiceProvider
      companyName
    }
  }
`;

const createNotification = /* GraphQL */ `
  mutation CreateNotification(
    $input: CreateNotificationInput!
    $condition: ModelNotificationConditionInput
  ) {
    createNotification(input: $input, condition: $condition) {
      id
      group
      owner
      title
      createdAt
      description
      releaseNotes
      payload
      clickAction
      isRead
      updatedAt
    }
  }
`;

const updateMessage = /* GraphQL */ `
  mutation UpdateMessage(
    $input: UpdateMessageInput!
    $condition: ModelMessageConditionInput
  ) {
    updateMessage(input: $input, condition: $condition) {
      id
      group
      smsStatus
      smsSendInformation
      emailStatus
      emailSendInformation
      carrierMessageId
      bodyText
      hasParts
      staffId
      staff{
        id
      }
    }
  }
`;
                        
module.exports = {
    updateStaff,
    getTenant,
    createNotification,
    updateMessage
};