const { buildUpdateParams, scanDynamoRecords, updateDynamoRecord } = require("./dynamodbHelper.js");

async function hera1180_clean() {
    var vehicles = await getAllVehicles();
   // console.log(vehicles)
    await cleanEmptyVIN(vehicles);
}

async function getAllVehicles() {
    try {
        let vehicles = [];
        let lastEvaluatedKey = null;
        do {
            let params = {
                ExpressionAttributeNames: {
                    "#group": "group",
                },
                
                TableName: process.env.VEHICLE_TABLE,
                ProjectionExpression: "id, vin, #group",
                ExclusiveStartKey: lastEvaluatedKey,
                FilterExpression: "attribute_exists(vin) and vin = :empty",
                ExpressionAttributeValues: {
                    ":empty": ""
                }
                
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            vehicles = [...vehicles, ...items];
        } while (lastEvaluatedKey);

        return vehicles;
    } catch (err) {
        console.log("[getAllVehicles] Error in function getAllVehicles", err);
        return {
            error: err,
        };
    }
}


async function cleanEmptyVIN(list){

    for (const vehicle of list) {
        const {
            ExpressionAttributeNames,
            UpdateExpression,
        } = getUpdateExpression(vehicle);
        try {
            console.log(vehicle)
            await updateDynamoRecord({
                TableName: process.env.VEHICLE_TABLE,
                Key: {"id": vehicle.id},
                ExpressionAttributeNames,
                UpdateExpression,
                ReturnValues: "UPDATED_NEW"
            })
            console.log("success Vehicle id:", vehicle.id)
            return
        } catch (err) {
            console.log("[hera-1180 cleanEmptyVIN] Error cleaning vin", err);
            return {
                error: err,
            };
        }
        
    }
}

function getUpdateExpression(vehicle){
    let UpdateExpression = ''
    const ExpressionAttributeNames = { '#vin': 'vin'}
    const removeArr = [];
    const GSIs = ['licensePlate','licensePlateExp', 'vin', 'status'];
    GSIs.forEach(field => {
        if (!vehicle[field]) {
            const key = `#${field}`;
            ExpressionAttributeNames[key] = field;
            removeArr.push(key);
        }
    });
    
    if (removeArr.length > 0) {
        const removeExpr = ` remove ${removeArr.join(', ')}`;
        UpdateExpression += removeExpr;
    }
    
    //console.log(ExpressionAttributeNames)
    //console.log(UpdateExpression)
    
    return {
        ExpressionAttributeNames,
        UpdateExpression
    }
}

module.exports = {
    hera1180_clean,
};