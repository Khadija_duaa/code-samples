const {scanDynamoRecords, buildUpdateParams, updateDynamoRecord, createDynamoRecord} = require("./dynamodbHelper");

async function handleHera964 () {
    try {
        let tenants = await getAllTenants()

        for (const tenant of tenants) {
            for (const option of optionsData) {
                let createOptionColumn = {
                    TableName: 'DailyRostersColumns-znhsfld3srhgdbamacqgjtk5nq-phasethree',
                    Item: {
                        id: generateUUID(),
                        group: tenant.group,
                        ...option,
                        createdAt: new Date().toISOString(),
                        updatedAt: new Date().toISOString()
                    }
                };
                await createDynamoRecord(createOptionColumn);
            }
        }

    } catch (error) {
        console.log("Error pe: ", error);
    }
}

async function getAllTenants() {
    try {
        let tenants = []
        let lastEvaluatedKey = null
        do {
            let params = {
                ExpressionAttributeNames: {
                    '#group': 'group',
                },
                TableName: process.env.TENANT_TABLE,
                ProjectionExpression: 'id, #group, CustomLists',
                ExclusiveStartKey: lastEvaluatedKey
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items
            lastEvaluatedKey = scanResult.LastEvaluatedKey
            tenants = [...tenants, ...items]
        } while (lastEvaluatedKey)
        return tenants
    } catch (err) {
        console.log("[getAllTenants] Error in function getAllTenants", err);
        return {
            error: err,
        };
    }
}

async function getAllOptions() {
    try {
        let optionColumns = []
        let lastEvaluatedKey = null
        do {
            let params = {
                TableName: 'DailyRostersColumns-znhsfld3srhgdbamacqgjtk5nq-phasethree',
                ProjectionExpression: 'id',
                ExclusiveStartKey: lastEvaluatedKey
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items
            lastEvaluatedKey = scanResult.LastEvaluatedKey
            optionColumns = [...optionColumns, ...items]
        } while (lastEvaluatedKey)
        return optionColumns
    } catch (err) {
        console.log("[getAllTenants] Error in function getAllTenants", err);
        return {
            error: err,
        };
    }
}

function generateUUID() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

const optionsData = [
    {
        order: 0,
        label: 'Route ID',
        isFixed: true,
        isDisplayed: true,
        displayedFor: 'for-routes',
        btnEditCustomList: false
    },
    {
        order: 1,
        label: 'Route Code',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-routes',
        btnEditCustomList: false
    },
    {
        order: 2,
        label: 'Route Type',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-routes',
        btnEditCustomList: true,
        customListType: 'route-type',
        isCustomizable: true
    },
    {
        order: 3,
        label: 'Route Status',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-routes',
        btnEditCustomList: true,
        customListType: 'route-status',
        isCustomizable: false
    },
    {
        order: 4,
        label: 'Vehicles & Associates',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-routes',
        btnEditCustomList: false
    },
    {
        order: 5,
        label: 'Wave Time',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-routes',
        btnEditCustomList: false
    },
    {
        order: 6,
        label: 'Staging',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-routes',
        btnEditCustomList: false
    },
    {
        order: 7,
        label: 'Route Notes',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-routes',
        btnEditCustomList: false
    },
    {
        order: 8,
        label: 'Dock Door',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-routes',
        btnEditCustomList: true,
        customListType: 'dock-door',
        isCustomizable: true
    },
    {
        order: 9,
        label: 'Total Stops',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-routes',
        btnEditCustomList: false
    },
    {
        order: 10,
        label: 'Total Packages',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-routes',
        btnEditCustomList: false
    },
    {
        order: 11,
        label: 'Completed Stops',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-routes',
        btnEditCustomList: false
    },
    {
        order: 12,
        label: 'Completed Packages',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-routes',
        btnEditCustomList: false
    },
    {
        order: 13,
        label: 'Undelivered Packages - Total',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-routes',
        btnEditCustomList: false
    },
    {
        order: 14,
        label: 'First Stop Time',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-routes',
        btnEditCustomList: false
    },
    {
        order: 15,
        label: 'Last Stop Time',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-routes',
        btnEditCustomList: false
    },
    {
        order: 16,
        label: 'Scheduled Start Time',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-routes',
        btnEditCustomList: false
    },
    {
        order: 17,
        label: 'Scheduled End Time',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-routes',
        btnEditCustomList: false
    },
    {
        order: 18,
        label: 'Actual Start Time',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-routes',
        btnEditCustomList: false
    },
    {
        order: 19,
        label: 'Actual End Time',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-routes',
        btnEditCustomList: false
    },
    {
        order: null,
        label: 'Undelivered Packages - Business Closed',
        isFixed: false,
        isDisplayed: false,
        displayedFor: 'for-routes',
        btnEditCustomList: false
    },
    {
        order: null,
        label: 'Undelivered Packages - Unable to Access',
        isFixed: false,
        isDisplayed: false,
        displayedFor: 'for-routes',
        btnEditCustomList: false
    },
    {
        order: null,
        label: 'Undelivered Packages - Weather',
        isFixed: false,
        isDisplayed: false,
        displayedFor: 'for-routes',
        btnEditCustomList: false
    },
    {
        order: null,
        label: 'Undelivered Packages - No Safe Location',
        isFixed: false,
        isDisplayed: false,
        displayedFor: 'for-routes',
        btnEditCustomList: false
    },
    {
        order: null,
        label: 'Undelivered Packages - Unable to Locate',
        isFixed: false,
        isDisplayed: false,
        displayedFor: 'for-routes',
        btnEditCustomList: false
    },
    {
        order: null,
        label: 'Undelivered Packages - Station Closure',
        isFixed: false,
        isDisplayed: false,
        displayedFor: 'for-routes',
        btnEditCustomList: false
    },
    {
        order: null,
        label: 'Undelivered Packages - Order Canceled',
        isFixed: false,
        isDisplayed: false,
        displayedFor: 'for-routes',
        btnEditCustomList: false
    },
    {
        order: null,
        label: 'Undelivered Packages - Out of Drive Time',
        isFixed: false,
        isDisplayed: false,
        displayedFor: 'for-routes',
        btnEditCustomList: false
    },
    {
        order: null,
        label: 'Picked Up Returned Packages',
        isFixed: false,
        isDisplayed: false,
        displayedFor: 'for-routes',
        btnEditCustomList: false
    },


    {
        order: 0,
        label: 'Vehicle',
        isFixed: true,
        isDisplayed: true,
        displayedFor: 'for-vehicles',
        btnEditCustomList: false
    },
    {
        order: 1,
        label: 'Primary Associate',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-vehicles',
        btnEditCustomList: false
    },
    {
        order: 2,
        label: 'Other Associates',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-vehicles',
        btnEditCustomList: false
    },
    {
        order: 3,
        label: 'Rescue Vehicles',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-vehicles',
        btnEditCustomList: false
    },
    {
        order: 4,
        label: 'Parking Space',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-vehicles',
        btnEditCustomList: true,
        customListType: 'parking-space',
        isCustomizable: true
    },
    {
        order: 5,
        label: 'Inspections',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-vehicles',
        btnEditCustomList: false
    },
    {
        order: 6,
        label: 'Primary Device',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-vehicles',
        btnEditCustomList: false
    },
    {
        order: null,
        label: 'Other Devices',
        isFixed: false,
        isDisplayed: false,
        displayedFor: 'for-vehicles',
        btnEditCustomList: false
    },


    {
        order: 0,
        label: 'Associate',
        isFixed: true,
        isDisplayed: true,
        displayedFor: 'for-associates',
        btnEditCustomList: false
    },
    {
        order: 1,
        label: 'Roster Status',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-associates',
        btnEditCustomList: false
    },
    {
        order: 2,
        label: 'Work Start Time',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-associates',
        btnEditCustomList: false
    },
    {
        order: 3,
        label: 'Work End Time',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-associates',
        btnEditCustomList: false
    },
    {
        order: 4,
        label: 'Station Departure Time',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-associates',
        btnEditCustomList: false
    },
    {
        order: 5,
        label: 'Station Return Time',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-associates',
        btnEditCustomList: false
    },
    {
        order: 6,
        label: 'Lunch Start Time',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-associates',
        btnEditCustomList: false
    },
    {
        order: 7,
        label: 'Lunch End Time',
        isFixed: false,
        isDisplayed: true,
        displayedFor: 'for-associates',
        btnEditCustomList: false
    },

]

module.exports = {
    handleHera964
}
