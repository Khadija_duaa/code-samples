const { parseMetrics } = require('./parseMetrics') 
const fs = require('fs')
const pdf = require('pdf-parse');

var AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });

async function readScorecardPDF(key){
    let fieldsMetrics = {}
    let s3 = new AWS.S3();
    let params = {
        Bucket: process.env.BUCKET,
        Key: 'public/' + key
    };

    
    let filePathArray = key.split("/");
    let originalName = filePathArray[filePathArray.length - 1]
    let fileName = new Date().getTime().toString() + originalName
    let s3Stream = await s3.getObject(params).promise()

    fs.writeFileSync('/tmp/' + fileName, s3Stream.Body, async (err) => {
        if (err) throw err;
    });

       
    const dataBuffer = fs.readFileSync('/tmp/' + fileName);

    await pdf(dataBuffer, {}).then(async function (data) {
        await fs.unlink('/tmp/' + fileName, (err) => {
            if (err) throw err;
        });

        fieldsMetrics = await parseMetrics(data)
    }).catch(error => {
        throw error
    })

    return fieldsMetrics
}


module.exports = { readScorecardPDF }
