const {
    scanDynamoRecords,
    createDynamoRecord,
    updateDynamoRecord,
    buildUpdateParams,
    queryDynamoRecords
} = require("../dynamodbHelper.js");
const { readCxPDF } = require("./parseCxFeedback.js");

async function handlerCxFeedback(){
    //get customerFeedbacks
    const type = 'customerFeedback'
    try {
        let lastEvaluatedKey = null;
        let uniqueCxFeedback = new Set();
        do {
            let params = {
                TableName: process.env.TEXTRACTJOB_TABLE,
                ExpressionAttributeValues: {
                  ':t': type,
                  ':y': '2023',
                  ':isProcessedS': 'true',
                  ':jobStatus': 'SUCCEEDED'
                },
                ExpressionAttributeNames: {
                  '#g': 'group',
                  '#t': 'type',
                  '#y': 'year',
                  '#w': 'week',
                  '#k': 'key',
                  '#isProcessedS': 'isProcessedS',
                  '#jobStatus': 'jobStatus',
                  '#xl': 'tenantUsesXLS',
                  '#textractJobTenantId': 'textractJobTenantId',
                  '#ca': 'createdAt'
                },
                ProjectionExpression: "id, #g, #t, #y, #w, #k, #isProcessedS, #jobStatus, #xl, #textractJobTenantId, #ca",
                FilterExpression: '#t = :t and #y = :y and #isProcessedS = :isProcessedS and #jobStatus = :jobStatus',
                ExclusiveStartKey: lastEvaluatedKey
            };
            const result = await scanDynamoRecords(params);
            lastEvaluatedKey = result.LastEvaluatedKey;
            const data = result.Items || []
            console.log('cx lastEvaluatedKey =>', lastEvaluatedKey);
            
            data.sort((a, b) => {
              return new Date(b.createdAt) - new Date(a.createdAt);
            });
            for(let item of data){
                const entryKey = `${item.group}-${item.year}-${item.week}`;
                if(!uniqueCxFeedback.has(entryKey)) {
                  uniqueCxFeedback.add(entryKey);
                  try{
                    await processCustomerFeedback(item);
                  }catch(e){
                    console.log('Cannot read the CX Feedback file', e)
                  }
                }
            }
        } while (lastEvaluatedKey);
       
    } catch (err) {
        console.log('[hera2544] Error in function handlerCxFeedback', err);
    }
}


async function processCustomerFeedback(cx){
    const key = cx.key
    const {fields, summaryTable, table} = await readCxPDF(key, cx)
    if(table && table.length > 0 && summaryTable && summaryTable.length>0){
        //updated
        await fillCxSummaryTable(summaryTable, fields, cx)
        await matchCxStaff(table, fields, cx)
    }else{
        console.log(`skip for: group: ${cx.group} customerFeedback, week: ${cx.week} year: ${cx.year}`)
    }
}

function cleanTransporterId(transporterId){
    return transporterId.replace(/\s+/g, '')
}

function findCategory(table, categoryName){
  const data = table.find(row => {
    return row.category.includes(categoryName)
  })
  return data ? data.count : ''
}
async function compileSummaryTableCxFeedback(tableData=[]){
  let data = {}
  if(!tableData.length){
    return data
  }
  
  data = {
    dcfTier: findCategory(tableData,'DSP Customer Feedback Tier'),
    dcfScore: findCategory(tableData,'DSP Customer Feedback Score'),
    positiveFeedback: findCategory(tableData,'Total Positive Feedback'),
    negativeFeedback: findCategory(tableData,'Total Negative Feedback'),
    deliveriesWithoutCF: findCategory(tableData,'Deliveries without Customer Feedback'),
    deliveryWasGreat: findCategory(tableData,'Delivery was Great'),
    respectfulOfProperty: findCategory(tableData,'Respectful of Property'),
    followedInstructions: findCategory(tableData,'Followed Instructions'),
    friendly: findCategory(tableData,'Friendly'),
    aboveAndBeyond: findCategory(tableData,'Above and Beyond'),
    deliveredWithCare: findCategory(tableData,'Delivered with Care'),
    deliveryWasntGreat: findCategory(tableData,'Delivery was not so Great'),
    mishandledPackage: findCategory(tableData,'Driver Mishandled Package'),
    driverUnprofessional: findCategory(tableData,'Driver was Unprofessional'),
    driverDidNotFollowDeliveryInstructions: findCategory(tableData,'Driver did not follow my Delivery instructions'),
    onTime: findCategory(tableData,'On Time'),
    lateDelivery: findCategory(tableData,'Late Delivery'),
    itemDamaged: findCategory(tableData,'Item Damaged'),
    deliveredToWrongAddress: findCategory(tableData,'Delivered to Wrong Address'),
    neverReceivedDelivery: findCategory(tableData,'Never Received Delivery'),
  }
  return  data
    
}

async function fillCxSummaryTable(summaryTable, resultFields, jobItem){
    const data = await compileSummaryTableCxFeedback(summaryTable)
    if(Object.keys(data || {}).length !== 0){
       
        let week = resultFields.week ? resultFields.week : jobItem.week
        week = String(week).padStart(2, '0')
               
        const year = jobItem.year
        const yearWeek = `${year}${week}`
        const summaryTableId = jobItem.textractJobTenantId + `-${yearWeek}`
        let item = {
            group: jobItem.group,
            year: year,
            week: week,
            yearWeek: yearWeek,
            updatedAt: new Date().toISOString(),
            cxFeedbackSummaryTextractJobId: jobItem.id,
            ...data
        }
        try{
            //Update record, if that fails, try creating
            let updateParams = buildUpdateParams(process.env.CX_FEEDBACK_SUMMARY_TABLE, item, summaryTableId)
            delete updateParams.ReturnValues
            await updateDynamoRecord(updateParams);
            await updateCompanyScorecard(year, week, jobItem)
        }catch(e){
            try{
                // Create record, if that fails, log error
                item.createdAt = new Date().toISOString()
                item.id = summaryTableId
                let createParams = {
                    TableName: process.env.CX_FEEDBACK_SUMMARY_TABLE,
                    Item: item
                };
                await createDynamoRecord(createParams);
            }catch(e){
                console.log('Cannot Create (function fillCxSummaryTable)', `for: group: ${item.group}, StaffCxFeedback week: ${item.week} year: ${item.year}`,  e)
                return
            }
            await updateCompanyScorecard(year, week, jobItem)
        }
    }
    return
}
async function updateCompanyScorecard(year, week, jobItem){
    const yearWeek = parseInt(String(year) + String(week))
    const companyScoreCardId = `${jobItem.textractJobTenantId}${yearWeek}`
    
    let companyScoreCardItem = {
        week: week,
        year: year,
        yearWeek: yearWeek,
        tenantId: jobItem.textractJobTenantId,
        group: jobItem.group,
        customerFeedbackPdf: jobItem.key
    }
    try{
        //update updateCompanyScorecard
        let updateParams = buildUpdateParams(process.env.COMPANYSCORECARD_TABLE, companyScoreCardItem, companyScoreCardId)
        delete updateParams.ReturnValues
        await updateDynamoRecord(updateParams);
    }catch(e){
        try{
            // Create record, if that fails, log error
            companyScoreCardItem.createdAt = new Date().toISOString()
            companyScoreCardItem.id = companyScoreCardId
            let createParams = {
                TableName: process.env.COMPANYSCORECARD_TABLE,
                Item: companyScoreCardItem
            };
            await createDynamoRecord(createParams);
        }catch(e){
            console.log('Cannot Create companyScorecard (function fillCxSummaryTable)', `for: group: ${companyScoreCardItem.group}, companyScorecard week: ${companyScoreCardItem.week} year: ${companyScoreCardItem.year}`,  e)
        }
    }
    return
}

async function matchCxStaff(tableCx, resultFields, jobItem){
    const staffs = await staffByGroup(jobItem.group)
   
    let matchedStaff = []
    for(let staff of staffs){
        const findResult = tableCx.find(tableItem => tableItem.driver_name && tableItem.transporter_id === staff.transporterId)
        if(findResult){
          matchedStaff.push(findResult)
        }
    }
    
    const year = jobItem.year
    let week = resultFields.week ? resultFields.week : jobItem.week
    week = String(week).padStart(2, '0')
    const group = jobItem.group
   
    const cxData = Object.values(matchedStaff.reduce((acc, row) => {
        let transporterId = cleanTransporterId(row.transporter_id);
        const customerFeedbackID = jobItem.textractJobTenantId + transporterId + year + week;
    
        if (!acc[customerFeedbackID]) {
            acc[customerFeedbackID] = {
                id: customerFeedbackID,
                group: group,
                week: week,
                year: year,
                messageHasBeenSent: false,
                name: row.driver_name,
                transporterId: row.transporter_id,
                positiveFeedback: row['%_positive_feedback'],
                negativeFeedback: row['$_negative_feedback'],
                deliveryWasGreat: row.delivery_was_great,
                careForOthers: row.care_for_others,
                deliveryWasntGreat: row.delivery_was_not_so_great,
                daTier: row.da_tier,
                cdfScore:row.cdf_score,
                totalDeliveries: row.total_deliveries_with_customer_feedback,
                respectfulOfProperty: row.respectful_of_property,
                followedInstructions: row.followed_instructions,
                friendly: row.friendly,
                aboveAndBeyond: row.went_above_and_beyond,
                deliveredWithCare: row.delivered_with_care,
                mishandledPackage: row.mishandled_package,
                drivingUnsafely: row.driving_unsafely,
                driverUnprofessional: row.driver_unprofessional,
                notDeliveredToPreferredLocation: row.not_follow_delivery_instructions,
                noFeedback: row.no_feedback,
                onTime: row.on_time,
                lateDelivery: row.late_delivery,
                itemDamaged: row.item_damaged,
                deliveredToWrongAddress: row.delivered_to_wrong_address,
                neverReceivedDelivery: row.never_received_delivery,
                updatedAt: new Date().toISOString(),
            };
        }
    
        return acc;
    }, {}));
   
    for(let cx of cxData){
        //update
        await updateStaffCxFeedback(cx)
    }
    return

}

async function updateStaffCxFeedback(cxItem){
    try{
        let updateParams = buildUpdateParams(process.env.STAFF_CX_TABLE, cxItem, cxItem.id)
        delete updateParams.ReturnValues
        await updateDynamoRecord(updateParams)
    }catch(error){
        console.log('Cannot update (function updateStaffCxFeedback)', `for: group: ${cxItem.group}, StaffCxFeedback week: ${cxItem.week} year: ${cxItem.year}`,  error)
    }
    return
}


async function staffByGroup(group){
    let staffByGroup = []
    try {
        let lastEvaluatedKey = null
       
        do {
            let params = {
                TableName: process.env.STAFF_TABLE,
                IndexName: 'byGroupStatus',
                ExpressionAttributeValues: {
                    ':g': group,
                },
                ExpressionAttributeNames: {
                    '#g': 'group'
                },
                KeyConditionExpression: '#g = :g',
                 FilterExpression: 'attribute_exists(transporterId)',
                ProjectionExpression: "id, #g, transporterId, firstName, lastName",
                ExclusiveStartKey: lastEvaluatedKey
            }
            const result = await queryDynamoRecords(params)
            lastEvaluatedKey = result.LastEvaluatedKey
            staffByGroup = staffByGroup.concat(result.Items)
        } while (lastEvaluatedKey)
    } catch (error) {
        console.log('Error in function staffByGroup', error)
    }finally{
        return staffByGroup
    }
}


module.exports = { handlerCxFeedback }