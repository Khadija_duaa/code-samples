const {
    scanDynamoRecords,
    updateDynamoRecord,
    buildUpdateParams
} = require("../dynamodbHelper.js");
const { readScorecardPDF } = require("./parseScorecard.js");

async function handlerScorecard(){
    //get scorecard imported
    const type = 'scorecard'
   
    try {
        let lastEvaluatedKey = null;
        let uniqueScorecards = new Set();
        do {
            let params = {
                TableName: process.env.TEXTRACTJOB_TABLE,
                ExpressionAttributeValues: {
                  ':t': type,
                  ':y': '2023',
                  ':isProcessedS': 'true',
                  ':jobStatus': 'SUCCEEDED'
                },
                ExpressionAttributeNames: {
                  '#g': 'group',
                  '#t': 'type',
                  '#y': 'year',
                  '#w': 'week',
                  '#k': 'key',
                  '#isProcessedS': 'isProcessedS',
                  '#jobStatus': 'jobStatus',
                  '#xl': 'tenantUsesXLS',
                  '#textractJobTenantId': 'textractJobTenantId',
                  '#ca': 'createdAt'
                },
                ProjectionExpression: "id, #g, #t, #y, #w, #k, #isProcessedS, #jobStatus, #xl, #textractJobTenantId, #ca",
                FilterExpression: '#t = :t and #y = :y and #isProcessedS = :isProcessedS and #jobStatus = :jobStatus',
                ExclusiveStartKey: lastEvaluatedKey
            };
            const result = await scanDynamoRecords(params);
            lastEvaluatedKey = result.LastEvaluatedKey;
            const data = result.Items || []
            console.log('sc lastEvaluatedKey =>', lastEvaluatedKey);
            
            data.sort((a, b) => {
              return new Date(b.createdAt) - new Date(a.createdAt);
            });
            for(let item of data){
                const entryKey = `${item.group}-${item.year}-${item.week}`;
                if(!uniqueScorecards.has(entryKey)) {
                  uniqueScorecards.add(entryKey);
                  
                  try{
                    await processScorecard(item);
                  }catch(e){
                    console.log('Cannot read the Scorecard file', e)
                  }
                }
            }
            
        } while (lastEvaluatedKey);
       
    } catch (err) {
        console.log('[hera2544] Error in function handlerScorecard', err);
    }
  
}

async function processScorecard(sc){
    const key = sc.key
    const fields = await readScorecardPDF(key)
    
    if(Object.keys(fields || {}).length !== 0){
        await updateScorecard(sc, fields)
    }else{
        console.log(`skip for: group: ${sc.group}, Scorecard week: ${sc.week} year: ${sc.year}`)
    }
}


async function updateScorecard(scorecard, fields){
    const year = fields.year ? fields.year : scorecard.year
    let week = fields.week ? fields.week : scorecard.week
    week =  String(week).padStart(2, '0')
    try{
        const yearWeek = parseInt(String(year) + String(week))
        const companyScoreCardId = `${scorecard.textractJobTenantId}${yearWeek}`
        fields['week'] = week
        fields['year'] = year
        fields['scorecardPdf'] = scorecard.key
        fields['yearWeek'] = yearWeek
        fields['tenantId'] = scorecard.textractJobTenantId
        fields['group'] = scorecard.group
        fields['updatedAt'] =  new Date().toISOString()
        let updateParams = buildUpdateParams(process.env.COMPANYSCORECARD_TABLE, fields, companyScoreCardId)
        delete updateParams.ReturnValues
        await updateDynamoRecord(updateParams)
    }catch(err){
        console.log('Cannot update (function updateScorecard)', `for: group: ${fields.group}, Scorecard week: ${week} year: ${year}`,  err)
    }
    return
}

module.exports = { handlerScorecard }