async function parseFields(blocks, fields) {
   
    for (const block of blocks) {
        const value = checkForPattern(fields, block);
        if (value !== null) {
          return { week: value }
        }
    }
    
    return { week: null };
}
  
function checkForPattern(fields, block) {
    for (const field of fields) {
      if (field.regex && block.str.match(new RegExp(field.regex))) {
        const matches = block.str.match(new RegExp(field.regex));
        if (matches && matches[field.regexGroup]) {
          const value = matches[field.regexGroup].trim();
          if (/^\d+$/.test(value)) {
            const week = parseInt(value, 10);
            if (week >= 1 && week <= 53) {
              return value;
            }
          }
        }
      }
    }
 
    return null
}

module.exports = {parseFields}