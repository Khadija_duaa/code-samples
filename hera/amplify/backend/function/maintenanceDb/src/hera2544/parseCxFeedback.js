const fs = require('fs')
const pdf = require('pdf-parse');

var AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });

const { cxTemplates } = require('./cxTemplates.js')
const { parseFields } = require('./parseFields.js')
const { parseTables } = require('./parseTables.js')


async function readCxPDF(key, job){
    let result = {}
    let template = {...cxTemplates}
    let s3 = new AWS.S3();
    let params = {
        Bucket: process.env.BUCKET,
        Key: 'public/' + key
    };

    
    let filePathArray = key.split("/");
    let originalName = filePathArray[filePathArray.length - 1]
    let fileName = new Date().getTime().toString() + originalName
    let s3Stream = await s3.getObject(params).promise()

    fs.writeFileSync('/tmp/' + fileName, s3Stream.Body, async (err) => {
        if (err) throw err;
    });

    const dataBuffer = fs.readFileSync('/tmp/' + fileName);

    const renderParams = {
        blocks: [],
        page: 1
    }
    let options = {
        pagerender: render_page(renderParams)
    }
    
   
    await pdf(dataBuffer, options).then(async function (data) {
        await fs.unlink('/tmp/' + fileName, (err) => {
            if (err) throw err;
        });
        let templateType = 'customerFeedback'
      
        let versionData = await parseFields([...renderParams.blocks], [...template.versionLookup])
        
        let week = versionData.week ? versionData.week : job.week
        week = week.padStart(2,'0')
        const year = job.year
        const yearWeek = parseInt(String(year)+String(week))
        //versionTemplate
        
        const version = yearWeek
        
        const correctTemplate = template.versions.find(templateVersion => {
            return version >= templateVersion.start
        })
     
        //--------DETERMINE FARTHEST LEFT/RIGHT FOR EACH PAGE-------------
        let farthestRight = [];
        let farthestLeft  = [];
        renderParams.blocks.forEach(block=>{
            let isVertical = block.transform[0] == 0
            let right = isVertical ? block.transform[5] - block.width : block.transform[4] + block.width
            let left  = isVertical ? block.transform[5] : block.transform[4]
            if(isVertical){ // left is down, right is up
              if( right < farthestRight[block.page - 1] || farthestRight[block.page - 1] == undefined){
                farthestRight[block.page - 1] = right;
              }
              if( left > farthestLeft[block.page - 1] || farthestLeft[block.page - 1] == undefined){
                farthestLeft[block.page - 1] = left;
              }
            }
            else{
              if( right > farthestRight[block.page - 1] || farthestRight[block.page - 1] == undefined){
                farthestRight[block.page - 1] = right;
              }
              if( left < farthestLeft[block.page - 1] || farthestLeft[block.page - 1] == undefined){
                farthestLeft[block.page - 1] = left;
              }
            }
        })
        
        let vehicleType = 'standard'
        if(correctTemplate.vehicleType) {
          vehicleType = correctTemplate.vehicleType
        }
      
      
        const tables = await parseTables(renderParams.blocks, correctTemplate.tables,farthestRight, farthestLeft, templateType, version, vehicleType)
        
        if(tables[1].table_data.length == 0){
          //return for skip process
          throw 'skip process'
        }
        if(tables[0].table_data.length == 0){
          //return for skip process
          throw 'skip process'
        }
        

        tables[1].table_data.map(row => {
          if(row.transporter_id && row.transporter_id.endsWith('..')){
            row.transporter_id = row.transporter_id.replace('..', '');
          }
        })
        
        result = {fields: {week:week, year: year}, summaryTable: tables[0].table_data,  table: tables[1].table_data}
       
    }).catch(error => {
      console.log(error)
      return {fields: {}, summaryTable: [], table: []}
    })
 
    return result
}

function render_page(renderParams){
    return function(pageData) {
      //check documents https://mozilla.github.io/pdf.js/
      let render_options = {
          //replaces all occurrences of whitespace with standard spaces (0x20). The default value is `false`.
          normalizeWhitespace: true,
          //do not attempt to combine same line TextItem's. The default value is `false`.
          disableCombineTextItems: false
      }
  
      return pageData.getTextContent(render_options)
          .then(function(textContent) {
              for (let item of textContent.items) {
              item.page = renderParams.page;
            renderParams.blocks.push(item)
              }
              renderParams.page++;
              return text;
      });
    }
  }


module.exports = { readCxPDF }