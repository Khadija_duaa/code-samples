const {
    convertDataTenantTimeZone
} = require("../helpers")

async function runHera1924Staff() {
    const TYPE = 'Staff'
    const FIELDS = [
        ['dob'],
        ['dlExpiration'],
        ['hireDate']
    ]

    for (const field of FIELDS) {
        await convertDataTenantTimeZone(TYPE, field);
    }
}

module.exports = {
    runHera1924Staff
}