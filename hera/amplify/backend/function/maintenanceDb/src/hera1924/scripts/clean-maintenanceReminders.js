const {
    convertDataTenantTimeZone
} = require("../helpers")

async function runHera1924VehicleMaintenanceReminder() {
    const TYPE = 'VehicleMaintenanceReminder'
    const FIELDS = [
        ['dueByDate']
    ]

    for (const field of FIELDS) {
        await convertDataTenantTimeZone(TYPE, field);
    }
}

module.exports = {
    runHera1924VehicleMaintenanceReminder
}