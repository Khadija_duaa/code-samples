const {
    convertDataTenantTimeZone
} = require("../helpers")

async function runHera1924Accidents() {
    const TYPE = 'Accident'
    const FIELDS = [
        {vehicleHistoryType:'Vehicle Damage',field:['accidentDate']},
        {vehicleHistoryType:'Maintenance',field:['accidentDate']},
        {vehicleHistoryType:'Accident',field:['accidentDate']},
        {vehicleHistoryType:'Accident',field:['drugTestDate']},
        {vehicleHistoryType:'Accident',field:['verifiedDate']}
    ]

    for (const field of FIELDS) {
        await convertDataTenantTimeZone(TYPE, field.field, field.vehicleHistoryType);
    }
}

module.exports = {
    runHera1924Accidents
}