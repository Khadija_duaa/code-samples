const {
    getDataTenants,
    getAllItems,
    cleanItems
} = require("../helpers")

const {
    getTableHash
} = require('../utils')

async function runHera1924Issues() {

    let result = []
    const tableName = 'Infraction'
    const fieldsName = ['date']
    const tenants = await getDataTenants()
    const tableHash = getTableHash(tableName);
    
    for (const tenant of tenants) {
         const items = await getAllItems(tenant, tableHash, fieldsName);
         const itemsToClean = await cleanItems(items, tableHash, fieldsName);
         result = result.concat(itemsToClean)
    }
    
    console.log(result)
    console.log("count: "+result.length)

}

module.exports = {
    runHera1924Issues
}