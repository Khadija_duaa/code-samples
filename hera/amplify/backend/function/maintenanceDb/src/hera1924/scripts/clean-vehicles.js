const {
    convertDataTenantTimeZone
} = require("../helpers")

async function runHera1924Vehicles() {
    const TYPE = 'Vehicle'
    const FIELDS = [
        ['dateEnd'],
        ['dateStart'],
        ['licensePlateExp']
    ]

    for (const field of FIELDS) {
        await convertDataTenantTimeZone(TYPE, field);
    }
}

module.exports = {
    runHera1924Vehicles
}