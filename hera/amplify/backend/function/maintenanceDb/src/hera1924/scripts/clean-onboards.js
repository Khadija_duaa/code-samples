const {
    convertDataTenantTimeZone
} = require("../helpers")

async function runHera1924OnBoard() {
    const TYPE = 'OnBoard'
    const FIELDS = [
        ['date3'],
        ['date4'],
        ['date5'],
        ['dateComplete'],
        ['dateStart']
    ]

    for (const field of FIELDS) {
        await convertDataTenantTimeZone(TYPE, field);
    }
}

module.exports = {
    runHera1924OnBoard
}