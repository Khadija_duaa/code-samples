const {
    convertDataTenantTimeZone
} = require("../helpers")

async function runHera1924Drugs() {
    const TYPE = 'DrugTest'
    const FIELDS = [
        ['date']
    ]

    for (const field of FIELDS) {
        await convertDataTenantTimeZone(TYPE, field);
    }
}

module.exports = {
    runHera1924Drugs
}