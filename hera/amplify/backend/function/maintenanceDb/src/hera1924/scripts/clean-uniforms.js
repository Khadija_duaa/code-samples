const {
    convertDataTenantTimeZone
} = require("../helpers")

async function runHera1924Uniforms() {
    const TYPE = 'Uniform'
    const FIELDS = [
        ['issueDate'],
        ['returnedDate']
    ]

    for (const field of FIELDS) {
        await convertDataTenantTimeZone(TYPE, field);
    }
}

module.exports = {
    runHera1924Uniforms
}