const {
    convertDataTenantTimeZone
} = require("../helpers")

async function runHera1924Injuries() {
    const TYPE = 'Injury'
    const FIELDS = [
        ['dateOfDeath'],
        ['driverDOB'],
        ['driverHireDate'],
        ['injuryDate']
    ]

    for (const field of FIELDS) {
        await convertDataTenantTimeZone(TYPE, field);
    }
}

module.exports = {
    runHera1924Injuries
}