const {
    convertDataTenantTimeZone
} = require("../helpers")

async function runHera1924Documents() {
    const TYPE = 'Document'
    const FIELDS = [
        ['documentDate']
    ]

    for (const field of FIELDS) {
        await convertDataTenantTimeZone(TYPE, field);
    }
}

module.exports = {
    runHera1924Documents
}