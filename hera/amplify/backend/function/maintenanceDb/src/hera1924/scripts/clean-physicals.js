const {
    convertDataTenantTimeZone
} = require("../helpers")

async function runHera1924Physicals() {
    const TYPE = 'Physical'
    const FIELDS = [
        ['date'],
        ['expirationDate']
    ]

    for (const field of FIELDS) {
        await convertDataTenantTimeZone(TYPE, field);
    }
}

module.exports = {
    runHera1924Physicals
}