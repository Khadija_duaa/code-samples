const {
    scanDynamoRecords,
    updateDynamoRecord
} = require("../dynamodbHelper")

const {
    getLocalTimeForCurrentDay,
    getTableHash
} = require("./utils")

async function getDataTenants() {
    let tenants = []
    let lastEvaluatedKey = null
    try {
        do {
            let params = {
                ExpressionAttributeNames: {
                    '#group': 'group',
                    '#timeZone': 'timeZone',
                },
                TableName: process.env.TENANT_TABLE,
                ProjectionExpression: 'id, #group, #timeZone',
                ExclusiveStartKey: lastEvaluatedKey
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items
            lastEvaluatedKey = scanResult.LastEvaluatedKey
            tenants = tenants.concat(items)
        } while (lastEvaluatedKey)
        return tenants
    } catch (err) {
        console.log("[getAllTenants] Error in function getAllTenants", err);
        return {
            error: err,
        };
    }
}

async function getAllItems(tenant, hashTable, fields, vehicleHistoryType = null) {
    
    const validatorDate = getLocalTimeForCurrentDay(tenant.timeZone)
    const table = hashTable.hashTableName
    
    try {
        let arrayItems = [];
        let lastEvaluatedKey = null;
        let conditionFilterExpressionOne = ""
        let conditionFilterExpressionTwo = ""
        let conditionFilterExpressionThree = ""
        let conditionFilterExpressionFour = ""
        let conditionProjectionExpression = ""
        do {
            let params = {
                TableName: table,
                ExpressionAttributeNames: {
                    "#group": "group"
                },
                ExpressionAttributeValues: {
                    ':group': tenant.group,
                    ':empty': "",
                    ':null': null
                },
                ExclusiveStartKey: lastEvaluatedKey
            };
            for (const field of fields) {
                conditionFilterExpressionOne += ` not contains(#${field},:${field}) or`
                conditionFilterExpressionTwo += ` attribute_exists(#${field}) or`
                conditionFilterExpressionThree += ` #${field} <> :empty or`
                conditionFilterExpressionFour += ` #${field} <> :null or`
                conditionProjectionExpression += `,#${field}`
                params.ExpressionAttributeNames[`#${field}`] = field
                params.ExpressionAttributeValues[`:${field}`] = validatorDate
            }
            conditionFilterExpressionOne = removeString(conditionFilterExpressionOne,"or")
            conditionFilterExpressionOne = removeDuplicate(conditionFilterExpressionOne," ")
            conditionFilterExpressionTwo = removeString(conditionFilterExpressionTwo,"or")
            conditionFilterExpressionTwo = removeDuplicate(conditionFilterExpressionTwo," ")
            conditionFilterExpressionThree = removeString(conditionFilterExpressionThree,"or")
            conditionFilterExpressionThree = removeDuplicate(conditionFilterExpressionThree," ")
            conditionFilterExpressionFour = removeString(conditionFilterExpressionFour,"or")
            conditionFilterExpressionFour = removeDuplicate(conditionFilterExpressionFour," ")
            conditionProjectionExpression = removeDuplicate(conditionProjectionExpression,",")           
            params.FilterExpression = `#group = :group and (${conditionFilterExpressionOne}) and (${conditionFilterExpressionTwo}) and (${conditionFilterExpressionThree}) and (${conditionFilterExpressionFour})`

            if(vehicleHistoryType){
                params.ExpressionAttributeNames["#vehicleHistoryType"] = 'vehicleHistoryType'
                params.ExpressionAttributeValues[":vehicleHistoryType"] = vehicleHistoryType
                params.FilterExpression += "and #vehicleHistoryType = :vehicleHistoryType"
            }

            params.ProjectionExpression = "id,#group" + conditionProjectionExpression
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            arrayItems = arrayItems.concat(items)
        } while (lastEvaluatedKey);
        return { arrayItems,validatorDate }
    } catch (err) {
        console.log(`[getAllItems] Error in function getAllItems (Table: ${table})`, err);
        return {
            error: err,
        };
    }
}

async function cleanItems(data, hashTable, fields) {
    const table = hashTable.hashTableName
    const { arrayItems, validatorDate } = data
    const itemsToClean = [];
    for (const item of arrayItems) {
        for (const field of fields) {
            if(!item[field]) { 
                console.log(`[cleanItems]The field ${field} does not exist (Table: ${table})`);
                break
            }else{
                const date = item[field].split('T')[0]
                item[field] = `${date}T${validatorDate}`
            }
        }
        itemsToClean.push(item)
        await updateItemsDynamo(item, table, fields)
    }

    return itemsToClean;
}

async function updateItemsDynamo(item, table, fields) {
    
    let conditionUpdateExpression = ""
    
    try {
        const params = {
         TableName: table,
         ExpressionAttributeNames: {},
         ExpressionAttributeValues: {},
         Key: {
            "id": item.id
         },
         ReturnValues: "UPDATED_NEW"
        }
        
        for (const field of fields) {
            conditionUpdateExpression += ` #${field} = :${field},`
            params.ExpressionAttributeNames[`#${field}`] = field
            params.ExpressionAttributeValues[`:${field}`] = item[field]
        }
        
        params.UpdateExpression = "set " + removeString(conditionUpdateExpression,",") 
        await updateDynamoRecord(params)
    } catch (err) {
        console.log(`[hera-1924] Error updating items (Table: ${table})`, err);
        return {
            error: err,
        };
    }
}

async function convertDataTenantTimeZone(tableName, fieldsName, vehicleHistoryType = null) {
    let arrayClean = []
    const tenants = await getDataTenants()
    const tableHash = getTableHash(tableName);

    for (const tenant of tenants) {
        const items = await getAllItems(tenant, tableHash, fieldsName, vehicleHistoryType);
        const itemsToClean = await cleanItems(items, tableHash, fieldsName);
        arrayClean = arrayClean.concat(itemsToClean)
    }

    console.log(arrayClean)
    console.log(`count: ${arrayClean.length} ${tableHash.hashTableName}|${fieldsName}`)
}

function removeString(expression,word){
    const lastOrIndex = expression.lastIndexOf(word);
    if (lastOrIndex !== -1) {
      expression = expression.slice(0, lastOrIndex).trim();
    }
    return expression
}

function removeDuplicate(expression, target){
  const words = expression.split(target);
  const uniqueWords = [...new Set(words)];
  const result = uniqueWords.join(target);
  return result;
}

module.exports = {
    getDataTenants,
    getAllItems,
    cleanItems,
    convertDataTenantTimeZone
}