const moment = require('moment-timezone')

/**
 * Returns a table object containing a table name and ID, based on the environment provided.
 * @param {string} table - A table name to use as a base for the generated ID.
 * @returns {object} A table object containing a name and ID.
 */
function getTableHash(tableName) {
    const env = process.env.ENV;
    const envHashTable = process.env.ENV_HASH_TABLE;
    const hashTable = {
        tableName: tableName,
        hashTableName: `${tableName}-${envHashTable}-${env}`
    };
    return hashTable;
}

/**
 * Returns the local time of the specified timezone for the current day.
 * @param {string} timezone - The timezone for which to calculate the local time.
 * @returns {string} The local time formatted as "HH:mm:ss".
 */
function getLocalTimeForCurrentDay(timezone) {
    const timestamp = moment().startOf('day').format("YYYY-MM-DD HH:mm:ss");
    const convertTimestamp = moment.tz(timestamp, timezone).utcOffset(0).format("HH:mm:ss.SSS[Z]")
    return convertTimestamp
}

module.exports = {
    getLocalTimeForCurrentDay,
    getTableHash
};