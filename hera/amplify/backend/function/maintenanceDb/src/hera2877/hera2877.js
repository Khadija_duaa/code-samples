const {
    createDynamoRecord,
  } = require('./shared/query')
  
  const {
    createNickname,
  } = require('./shared/data')


  async function runHera2877() {

      let result = await insertUserNickNames()
      console.log({
        result
      });
  }

  async function insertUserNickNames() {
      const countInsert = {
          errors: 0,
          success: 0
      }
      for (const item of createNickname) {
          try {
              const createNickname = {
              TableName: process.env.NICKNAME_TABLE,
              Item: {
                  id: generateUUID(),
                  name: item.name,
                  nicknames: item.nickNames,
                  createdAt: new Date().toISOString(),
                  updatedAt: new Date().toISOString()
                }
              };
        await createDynamoRecord(createNickname);  
              countInsert.success++
          } catch (e) {
              console.log('error insertUserNickNames', e)
              countInsert.errors++
          }
      }
      return countInsert
  }
  
  function generateUUID() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  module.exports = {
      runHera2877
  }