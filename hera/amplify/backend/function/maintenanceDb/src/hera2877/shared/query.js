const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient();

function createDynamoRecord(params) {
    return new Promise((resolve, reject) => {
        ddb.put(params, function(err, data) {
            if (err) {
                console.error("Unable to create item. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                console.log("Create item succeeded:", JSON.stringify(data, null, 2));
                resolve(data)
            }
        });
    })
}

module.exports = {
    createDynamoRecord
};