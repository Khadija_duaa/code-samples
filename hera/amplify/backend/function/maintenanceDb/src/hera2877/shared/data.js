const createNickname = 
[{
        "name": "Matthew",
        "nickNames": ["Matt", "Matty"]
    },
    {
        "name": "Aaron",
        "nickNames": ["Ron", "Ronnie", "Ronny"]
    },
    {
        "name": "Aron",
        "nickNames": ["Ron, Ronnie, Ronny"]
    },
    {
        "name": "Abel",
        "nickNames": ["Abe","Abie"]
    },
    {
        "name": "Abner",
        "nickNames": ["Ab","Abbie"]
    },
    {
        "name": "Abraham",
        "nickNames": ["Abe","Abie","Bram"]
    },
    {
        "name": "Abram",
        "nickNames": ["Abe","Abie","Bram"]
    },
    {
        "name": "Adam",
        "nickNames": ["Ad","Addie","Addy","Ade"]
    },
    {
        "name": "Adelbert",
        "nickNames": ["Ad","Ade","Al","Bert","Bertie","Del"]
    },
    {
        "name": "Adalbert",
        "nickNames": ["Ad","Ade","Al","Bert","Bertie","Del"]
    },
    {
        "name": "Adrian",
        "nickNames": ["Ade"]
    },
    {
        "name": "Alan",
        "nickNames": ["Al"]
    },
    {
        "name": "Allen",
        "nickNames": ["Al"]
    },
    {
        "name": "Allan",
        "nickNames": ["Al"]
    },
    {
        "name": "Albert",
        "nickNames": ["Al","Bert","Bertie"]
    },
    {
        "name": "Alexander",
        "nickNames": ["Al","Alex","Alec","Aleck","Lex","Sandy","Sander"]
    },
    {
        "name": "Alfred",
        "nickNames": ["Al","Alf","Alfie","Fred","Freddie","Freddy"]
    },
    {
        "name": "Algernon",
        "nickNames": ["Algie","Algy","Alger"]
    },
    {
        "name": "Allister",
        "nickNames": ["Al"]
    },
    {
        "name": "Alistair",
        "nickNames": ["Al"]
    },
    {
        "name": "Alastair",
        "nickNames": ["Al"]
    },
    {
        "name": "Alaster",
        "nickNames": ["Al"]
    },
    {
        "name": "Alister",
        "nickNames": ["Al"]
    },
    {
        "name": "Alonzo",
        "nickNames": ["Al","Lon","Lonnie","Lonny"]
    },
    {
        "name": "Alonso",
        "nickNames": ["Al","Lon","Lonnie","Lonny"]
    },
    {
        "name": "Alfonso",
        "nickNames": ["Al","Alf","Alfie","Alonso","Lon"]
    },
    {
        "name": "Alphonso",
        "nickNames": ["Al","Alf","Alfie","Alonso","Lon"]
    },
    {
        "name": "Alvah",
        "nickNames": ["Al"]
    },
    {
        "name": "Alva",
        "nickNames": ["Al"]
    },
    {
        "name": "Alvan",
        "nickNames": ["Al"]
    },
    {
        "name": "Alvin",
        "nickNames": ["Al","Vin","Vinny","Win"]
    },
    {
        "name": "Alwyn",
        "nickNames": ["Al","Vin","Vinny","Win"]
    },
    {
        "name": "Alwin",
        "nickNames": ["Al","Vin","Vinny","Win"]
    },
    {
        "name": "Ambrose",
        "nickNames": ["Ambie","Brose"]
    },
    {
        "name": "Andrew",
        "nickNames": ["Andy","Drew"]
    },
    {
        "name": "Angus",
        "nickNames": ["Gus"]
    },
    {
        "name": "Anselm",
        "nickNames": ["Anse"]
    },
    {
        "name": "Ansel",
        "nickNames": ["Anse"]
    },
    {
        "name": "Anthony",
        "nickNames": ["Tony"]
    },
    {
        "name": "Anton",
        "nickNames": ["Tony"]
    },
    {
        "name": "Antony",
        "nickNames": ["Tony"]
    },
    {
        "name": "Archibald",
        "nickNames": ["Arch","Archie","Baldie"]
    },
    {
        "name": "Arnold",
        "nickNames": ["Arnie"]
    },
    {
        "name": "Arthur",
        "nickNames": ["Art","Artie"]
    },
    {
        "name": "Augustus",
        "nickNames": ["Augie","Gus","Gussy","Gust","Gustus"]
    },
    {
        "name": "August",
        "nickNames": ["Augie","Gus","Gussy","Gust","Gustus"]
    },
    {
        "name": "Augustine",
        "nickNames": ["Augie","Austin","Gus","Gussy","Gust"]
    },
    {
        "name": "Augustin",
        "nickNames": ["Augie","Austin","Gus","Gussy","Gust"]
    },
    {
        "name": "Austin",
        "nickNames": ["Augie","Austin","Gus","Gussy","Gust"]
    },
    {
        "name": "Avery",
        "nickNames": ["Avy"]
    },
    {
        "name": "Baldwin",
        "nickNames": ["Baldie","Win"]
    },
    {
        "name": "Barrett",
        "nickNames": ["Barry","Barrie"]
    },
    {
        "name": "Bartholomew",
        "nickNames": ["Bart","Barty","Bartlett","Bartley","Bat","Batty"]
    },
    {
        "name": "Basil",
        "nickNames": ["Baz","Basie"]
    },
    {
        "name": "Benedict",
        "nickNames": ["Ben","Bennie","Benny"]
    },
    {
        "name": "Benjamin",
        "nickNames": ["Ben","Bennie","Benny","Benjy","Benjie"]
    },
    {
        "name": "Bennett",
        "nickNames": ["Ben","Bennie","Benny"]
    },
    {
        "name": "Bennet",
        "nickNames": ["Ben","Bennie","Benny"]
    },
    {
        "name": "Bernard",
        "nickNames": ["Bernie","Berney","Barney","Barnie"]
    },
    {
        "name": "Barnard",
        "nickNames": ["Bernie","Berney","Barney","Barnie"]
    },
    {
        "name": "Bert",
        "nickNames": ["Bertie"]
    },
    {
        "name": "Berthold",
        "nickNames": ["Bert","Bertie"]
    },
    {
        "name": "Bertram",
        "nickNames": ["Bert","Bertie"]
    },
    {
        "name": "Bertrand",
        "nickNames": ["Bert","Bertie"]
    },
    {
        "name": "Bill",
        "nickNames": ["Billy","Billie","William","Willis"]
    },
    {
        "name": "Bradford",
        "nickNames": ["Brad","Ford"]
    },
    {
        "name": "Bradley",
        "nickNames": ["Brad"]
    },
    {
        "name": "Brandon",
        "nickNames": ["Brand","Brandy"]
    },
    {
        "name": "Branden",
        "nickNames": ["Brand","Brandy"]
    },
    {
        "name": "Brenton",
        "nickNames": ["Brent,Bret,Brett"]
    },
    {
        "name": "Brian",
        "nickNames": ["Bryan,Bryant"]
    },
    {
        "name": "Broderick",
        "nickNames": ["Brodie","Brody","Brady","Rick","Ricky"]
    },
    {
        "name": "Bruce",
        "nickNames": ["Bruno"]
    },
    {
        "name": "Burton",
        "nickNames": ["Burt"]
    },
    {
        "name": "Byron",
        "nickNames": ["Ron","Ronnie","Ronny"]
    },
    {
        "name": "Caleb",
        "nickNames": ["Cal"]
    },
    {
        "name": "Calvin",
        "nickNames": ["Cal","Vin","Vinny"]
    },
    {
        "name": "Cameron",
        "nickNames": ["Cam","Ron","Ronny"]
    },
    {
        "name": "Carey",
        "nickNames": ["Cary","Carry"]
    },
    {
        "name": "Carl",
        "nickNames": ["Karl"]
    },
    {
        "name": "Carol",
        "nickNames": ["Carrol","Carroll"]
    },
    {
        "name": "Casey",
        "nickNames": ["Kasey"]
    },
    {
        "name": "Caspar",
        "nickNames": ["Cas","Cass"]
    },
    {
        "name": "Casper",
        "nickNames": ["Cas","Cass"]
    },
    {
        "name": "Cassius",
        "nickNames": ["Cas","Cass"]
    },
    {
        "name": "Cecil",
        "nickNames": ["Cis"]
    },
    {
        "name": "Cedric",
        "nickNames": ["Ced","Rick","Ricky"]
    },
    {
        "name": "Charles",
        "nickNames": ["Charlie","Charley","Chuck","Chad","Chas"]
    },
    {
        "name": "Chester",
        "nickNames": ["Chet"]
    },
    {
        "name": "Christian",
        "nickNames": ["Chris","Christy","Kit"]
    },
    {
        "name": "Christopher",
        "nickNames": ["Chris","Kris","Cris","Christy","Kit","Kester","Kristof","Toph","Topher"]
    },
    {
        "name": "Kristopher",
        "nickNames": ["Chris","Kris","Cris","Christy","Kit","Kester","Kristof","Toph","Topher"]
    },
    {
        "name": "Clarence",
        "nickNames": ["Clare","Clair"]
    },
    {
        "name": "Clare",
        "nickNames": ["Clair"]
    },
    {
        "name": "Clark",
        "nickNames": ["Clarke"]
    },
    {
        "name": "Claude",
        "nickNames": ["Claud"]
    },
    {
        "name": "Clayton",
        "nickNames": ["Clay"]
    },
    {
        "name": "Clement",
        "nickNames": ["Clem"]
    },
    {
        "name": "Clifford",
        "nickNames": ["Cliff","Ford"]
    },
    {
        "name": "Clinton",
        "nickNames": ["Clint"]
    },
    {
        "name": "Colin",
        "nickNames": ["Cole"]
    },
    {
        "name": "Collin",
        "nickNames": ["Cole"]
    },
    {
        "name": "Conrad",
        "nickNames": ["Con","Connie","Conny"]
    },
    {
        "name": "Cornelius",
        "nickNames": ["Connie","Conny","Corny","Corney","Cory"]
    },
    {
        "name": "Curtis",
        "nickNames": ["Curt"]
    },
    {
        "name": "Cyril",
        "nickNames": ["Cy"]
    },
    {
        "name": "Cyrus",
        "nickNames": ["Cy"]
    },
    {
        "name": "Daniel",
        "nickNames": ["Dan","Danny"]
    },
    {
        "name": "Darrell",
        "nickNames": ["Darry"]
    },
    {
        "name": "Darryl",
        "nickNames": ["Darry"]
    },
    {
        "name": "Daryl",
        "nickNames": ["Darry"]
    },
    {
        "name": "Darrel",
        "nickNames": ["Darry"]
    },
    {
        "name": "David",
        "nickNames": ["Dave","Davey","Davie","Davy"]
    },
    {
        "name": "Delbert",
        "nickNames": ["Del","Bert","Bertie"]
    },
    {
        "name": "Dennis",
        "nickNames": ["Den","Denny"]
    },
    {
        "name": "Denis",
        "nickNames": ["Den","Denny"]
    },
    {
        "name": "Derrick",
        "nickNames": ["Derry","Rick","Ricky"]
    },
    {
        "name": "Derek",
        "nickNames": ["Derry","Rick","Ricky"]
    },
    {
        "name": "Desmond",
        "nickNames": ["Des"]
    },
    {
        "name": "Dexter",
        "nickNames": ["Dex"]
    },
    {
        "name": "Domenick",
        "nickNames": ["Dom","Nick","Nicky"]
    },
    {
        "name": "Domenic",
        "nickNames": ["Dom","Nick","Nicky"]
    },
    {
        "name": "Dominick",
        "nickNames": ["Dom","Nick","Nicky"]
    },
    {
        "name": "Dominic",
        "nickNames": ["Dom","Nick","Nicky"]
    },
    {
        "name": "Don",
        "nickNames": ["Donnie","Donny"]
    },
    {
        "name": "Donald",
        "nickNames": ["Don","Donnie","Donny"]
    },
    {
        "name": "Donovan",
        "nickNames": ["Don","Donnie","Donny"]
    },
    {
        "name": "Douglas",
        "nickNames": ["Doug"]
    },
    {
        "name": "Douglass",
        "nickNames": ["Doug"]
    },
    {
        "name": "Dudley",
        "nickNames": ["Dud","Duddy"]
    },
    {
        "name": "Duncan",
        "nickNames": ["Dunny","Dunk"]
    },
    {
        "name": "Dustin",
        "nickNames": ["Dusty"]
    },
    {
        "name": "Edgar",
        "nickNames": ["Ed","Eddie","Eddy","Ned"]
    },
    {
        "name": "Edmund",
        "nickNames": ["Ed","Eddie","Eddy","Ned","Ted"]
    },
    {
        "name": "Edmond",
        "nickNames": ["Ed","Eddie","Eddy","Ned","Ted"]
    },
    {
        "name": "Edward",
        "nickNames": ["Ed","Eddie","Eddy","Ned","Ted","Teddy"]
    },
    {
        "name": "Edwin",
        "nickNames": ["Ed","Eddie","Eddy","Ned"]
    },
    {
        "name": "Egbert",
        "nickNames": ["Bert","Bertie"]
    },
    {
        "name": "Elbert",
        "nickNames": ["El","Bert","Bertie"]
    },
    {
        "name": "Eldred",
        "nickNames": ["El"]
    },
    {
        "name": "Elijah",
        "nickNames": ["Eli","Lige"]
    },
    {
        "name": "Elias",
        "nickNames": ["Eli","Lige"]
    },
    {
        "name": "Elliott",
        "nickNames": ["El"]
    },
    {
        "name": "Elliot",
        "nickNames": ["El"]
    },
    {
        "name": "Ellis",
        "nickNames": ["El"]
    },
    {
        "name": "Elmer",
        "nickNames": ["El"]
    },
    {
        "name": "Alton",
        "nickNames": ["El","Al"]
    },
    {
        "name": "Elton",
        "nickNames": ["El","Al"]
    },
    {
        "name": "Elwyn",
        "nickNames": ["El","Vin","Vinny","Win"]
    },
    {
        "name": "Elwin",
        "nickNames": ["El","Vin","Vinny","Win"]
    },
    {
        "name": "Elvin",
        "nickNames": ["El","Vin","Vinny","Win"]
    },
    {
        "name": "Elvis",
        "nickNames": ["El"]
    },
    {
        "name": "Elwood",
        "nickNames": ["El","Woody"]
    },
    {
        "name": "Emory",
        "nickNames": ["Em"]
    },
    {
        "name": "Emmery",
        "nickNames": ["Em"]
    },
    {
        "name": "Emery",
        "nickNames": ["Em"]
    },
    {
        "name": "Emile",
        "nickNames": ["Em"]
    },
    {
        "name": "Emil",
        "nickNames": ["Em"]
    },
    {
        "name": "Emanuel",
        "nickNames": ["Manny","Mannie"]
    },
    {
        "name": "Immanuel",
        "nickNames": ["Manny","Mannie"]
    },
    {
        "name": "Manuel",
        "nickNames": ["Manny","Mannie"]
    },
    {
        "name": "Emmanuel",
        "nickNames": ["Manny","Mannie"]
    },
    {
        "name": "Emmett",
        "nickNames": ["Em"]
    },
    {
        "name": "Emmet",
        "nickNames": ["Em"]
    },
    {
        "name": "Erick",
        "nickNames": ["Rick","Ricky"]
    },
    {
        "name": "Erik",
        "nickNames": ["Rick","Ricky"]
    },
    {
        "name": "Eric",
        "nickNames": ["Rick","Ricky"]
    },
    {
        "name": "Ernest",
        "nickNames": ["Ernie"]
    },
    {
        "name": "Earnest",
        "nickNames": ["Ernie"]
    },
    {
        "name": "Erwin",
        "nickNames": ["Erv","Vin","Win"]
    },
    {
        "name": "Irvin",
        "nickNames": ["Erv","Vin","Win"]
    },
    {
        "name": "Irvine",
        "nickNames": ["Erv","Vin","Win"]
    },
    {
        "name": "Irving",
        "nickNames": ["Erv","Vin","Win"]
    },
    {
        "name": "Irwin",
        "nickNames": ["Erv","Vin","Win"]
    },
    {
        "name": "Ervin",
        "nickNames": ["Erv","Vin","Win"]
    },


    {
        "name": "Eugene",
        "nickNames": ["Gene"]
    },

    {
        "name": "Eustace",
        "nickNames": ["Stacy","Stacey"]
    },

    {
        "name": "Evan",
        "nickNames": ["Ev"]
    },

    {
        "name": "Everard",
        "nickNames": ["Ev"]
    },

    {
        "name": "Everett",
        "nickNames": ["Ev"]
    },

    {
        "name": "Fabian",
        "nickNames": ["Fabe","Fab"]
    },

    {
        "name": "Felix",
        "nickNames": ["Lix"]
    },

    {
        "name": "Ferdinand",
        "nickNames": ["Ferdie","Fred","Freddie"]
    },

    {
        "name": "Ferguson",
        "nickNames": ["Fergie"]
    },
    {
        "name": "Fergus",
        "nickNames": ["Fergie"]
    },

    {
        "name": "Floyd",
        "nickNames": ["Floy","Lloyd"]
    },


    {
        "name": "Francis",
        "nickNames": ["Frank","Frankie","Franky","Fran"]
    },

    {
        "name": "Franklyn",
        "nickNames": ["Frank","Frankie","Franky"]
    },
    {
        "name": "Franklin",
        "nickNames": ["Frank","Frankie","Franky"]
    },

    {
        "name": "Fredric",
        "nickNames": ["Fred","Freddie","Freddy","Rick","Ricky"]
    },
    {
        "name": "Fredrick",
        "nickNames": ["Fred","Freddie","Freddy","Rick","Ricky"]
    },
    {
        "name": "Frederic",
        "nickNames": ["Fred","Freddie","Freddy","Rick","Ricky"]
    },
    {
        "name": "Frederick",
        "nickNames": ["Fred","Freddie","Freddy","Rick","Ricky"]
    },


    {
        "name": "Gabriel",
        "nickNames": ["Gabe","Gabby"]
    },

    {
        "name": "Garret",
        "nickNames": ["Gary","Garry"]
    },
    {
        "name": "Garrett",
        "nickNames": ["Gary","Garry"]
    },

    {
        "name": "Jeffery",
        "nickNames": ["Jeff"]
    },
    {
        "name": "Jeffrey",
        "nickNames": ["Jeff"]
    },
    {
        "name": "Geoffrey",
        "nickNames": ["Jeff"]
    },

    {
        "name": "George",
        "nickNames": ["Georgie","Geordie"]
    },

    {
        "name": "Gerard",
        "nickNames": ["Gerry","Jerry"]
    },
    {
        "name": "Gerald",
        "nickNames": ["Gerry","Jerry"]
    },

    {
        "name": "Gilbert",
        "nickNames": ["Gil","Bert"]
    },


    {
        "name": "Gordon",
        "nickNames": ["Gordy","Don"]
    },



    {
        "name": "Gregor",
        "nickNames": ["Greg","Gregg"]
    },
    {
        "name": "Gregory",
        "nickNames": ["Greg","Gregg"]
    },

    {
        "name": "Griffin",
        "nickNames": ["Griff"]
    },
    {
        "name": "Griffith",
        "nickNames": ["Griff"]
    },


    {
        "name": "Harold",
        "nickNames": ["Hal","Harry"]
    },

    {
        "name": "Harrison",
        "nickNames": ["Harry"]
    },
    {
        "name": "Harris",
        "nickNames": ["Harry"]
    },

    {
        "name": "Harvey",
        "nickNames": ["Harve"]
    },


    {
        "name": "Henry",
        "nickNames": ["Harry","Hank","Hal"]
    },

    {
        "name": "Herbert",
        "nickNames": ["Herb","Bert","Bertie"]
    },

    {
        "name": "Herman",
        "nickNames": ["Manny","Mannie"]
    },

    {
        "name": "Hillary",
        "nickNames": ["Hill","Hillie","Hilly"]
    },
    {
        "name": "Hilary",
        "nickNames": ["Hill","Hillie","Hilly"]
    },



    {
        "name": "Howard",
        "nickNames": ["Howie"]
    },

    {
        "name": "Hubert",
        "nickNames": ["Hugh","Bert","Bertie","Hube"]
    },

    {
        "name": "Hugh",
        "nickNames": ["Hughie","Hugo"]
    },

    {
        "name": "Humphry",
        "nickNames": ["Humph"]
    },
    {
        "name": "Humphrey",
        "nickNames": ["Humph"]
    },


    {
        "name": "Ignatius",
        "nickNames": ["Iggy","Nate"]
    },

    {
        "name": "Immanuel",
        "nickNames": ["Manny","Mannie"]
    },

    {
        "name": "Irwin",
        "nickNames": ["Ervin"]
    },
    {
        "name": "Irving",
        "nickNames": ["Ervin"]
    },
    {
        "name": "Irvine",
        "nickNames": ["Ervin"]
    },
    {
        "name": "Irvin",
        "nickNames": ["Ervin"]
    },

    {
        "name": "Isaak",
        "nickNames": ["Ike"]
    },
    {
        "name": "Isaac",
        "nickNames": ["Ike"]
    },

    {
        "name": "Isador",
        "nickNames": ["Izzy"]
    },
    {
        "name": "Isadore",
        "nickNames": ["Izzy"]
    },
    {
        "name": "Isidor",
        "nickNames": ["Izzy"]
    },
    {
        "name": "Isidore",
        "nickNames": ["Izzy"]
    },


    {
        "name": "Jack",
        "nickNames": ["Jackie","Jacky"]
    },

    {
        "name": "Jacob",
        "nickNames": ["Jake","Jay"]
    },

    {
        "name": "James",
        "nickNames": ["Jim","Jimmy","Jimmie","Jamie","Jem"]
    },

    {
        "name": "Jared",
        "nickNames": ["Jerry"]
    },

    {
        "name": "Jervis",
        "nickNames": ["Jerry"]
    },
    {
        "name": "Jarvis",
        "nickNames": ["Jerry"]
    },

    {
        "name": "Jason",
        "nickNames": ["Jay"]
    },

    {
        "name": "Jasper",
        "nickNames": ["Jay"]
    },

    {
        "name": "Jefferson",
        "nickNames": ["Jeff"]
    },

    {
        "name": "Jeffery",
        "nickNames": ["Jeff"]
    },
    {
        "name": "Geoffrey",
        "nickNames": ["Jeff"]
    },
    {
        "name": "Jeffrey",
        "nickNames": ["Jeff"]
    },

    {
        "name": "Jeremiah",
        "nickNames": ["Jerry"]
    },
    {
        "name": "Jeremy",
        "nickNames": ["Jerry"]
    },

    {
        "name": "Jerome",
        "nickNames": ["Jerry"]
    },

    {
        "name": "Jesse",
        "nickNames": ["Jess","Jessie","Jessy"]
    },

    {
        "name": "Joel",
        "nickNames": ["Joe"]
    },

    {
        "name": "John",
        "nickNames": ["Jack","Jackie","Jacky","Johnny"]
    },

    {
        "name": "Jonathan",
        "nickNames": ["Jon","Jonny"]
    },

    {
        "name": "Joseph",
        "nickNames": ["Joe","Joey","Jo","Jos","Jody"]
    },

    {
        "name": "Joshua",
        "nickNames": ["Josh"]
    },

    {
        "name": "Judson",
        "nickNames": ["Jud","Sonny"]
    },

    {
        "name": "Julius",
        "nickNames": ["Jule","Jules"]
    },
    {
        "name": "Julian",
        "nickNames": ["Jule","Jules"]
    },

    {
        "name": "Justin",
        "nickNames": ["Jus","Just"]
    },



    {
        "name": "Kelly",
        "nickNames": ["Kelley"]
    },

    {
        "name": "Kelvin",
        "nickNames": ["Kel","Kelly"]
    },

    {
        "name": "Kendall",
        "nickNames": ["Ken","Kenny"]
    },

    {
        "name": "Kendrick",
        "nickNames": ["Ken","Kenny","Rick","Ricky"]
    },

    {
        "name": "Kenneth",
        "nickNames": ["Ken","Kenny"]
    },

    {
        "name": "Kent",
        "nickNames": ["Ken","Kenny"]
    },

    {
        "name": "Kevin",
        "nickNames": ["Kev"]
    },


    {
        "name": "Kristofer",
        "nickNames": ["Kris","Kit","Kester","Christopher"]
    },
    {
        "name": "Kristopher",
        "nickNames": ["Kris","Kit","Kester","Christopher"]
    },

    {
        "name": "Kurt",
        "nickNames": ["Curt"]
    },


    {
        "name": "Lambert",
        "nickNames": ["Bert"]
    },

    {
        "name": "Lamont",
        "nickNames": ["Monty","Monte"]
    },

    {
        "name": "Launcelot",
        "nickNames": ["Lance"]
    },
    {
        "name": "Lancelot",
        "nickNames": ["Lance"]
    },

    {
        "name": "Lorenzo",
        "nickNames": ["Larry","Lars","Laurie","Lawrie","Loren","Lauren"]
    },
    {
        "name": "Lorence",
        "nickNames": ["Larry","Lars","Laurie","Lawrie","Loren","Lauren"]
    },
    {
        "name": "Lawrence",
        "nickNames": ["Larry","Lars","Laurie","Lawrie","Loren","Lauren"]
    },
    {
        "name": "Laurence",
        "nickNames": ["Larry","Lars","Laurie","Lawrie","Loren","Lauren"]
    },

    {
        "name": "Lee",
        "nickNames": ["Leigh"]
    },

    {
        "name": "Leo",
        "nickNames": ["Leon"]
    },

    {
        "name": "Leonard",
        "nickNames": ["Leo","Leon","Len","Lenny","Lennie"]
    },

    {
        "name": "Leopold",
        "nickNames": ["Leo","Poldie"]
    },

    {
        "name": "Leeroy",
        "nickNames": ["Lee","Roy"]
    },
    {
        "name": "Leroy",
        "nickNames": ["Lee","Roy"]
    },

    {
        "name": "Lesley",
        "nickNames": ["Les"]
    },
    {
        "name": "Leslie",
        "nickNames": ["Les"]
    },

    {
        "name": "Lester",
        "nickNames": ["Les"]
    },

    {
        "name": "Lewis",
        "nickNames": ["Lew","Lewie"]
    },

    {
        "name": "Lincoln",
        "nickNames": ["Lin","Linc","Lynn"]
    },

    {
        "name": "Lindon",
        "nickNames": ["Lin","Lynn"]
    },

    {
        "name": "Lindsey",
        "nickNames": ["Lin","Lynn"]
    },
    {
        "name": "Lindsay",
        "nickNames": ["Lin","Lynn"]
    },


    {
        "name": "Lionel",
        "nickNames": ["Leo","Leon"]
    },

    {
        "name": "Llewellyn",
        "nickNames": ["Llew","Lyn"]
    },

    {
        "name": "Floyd",
        "nickNames": ["Loy","Floy"]
    },
    {
        "name": "Loyde",
        "nickNames": ["Loy","Floy"]
    },
    {
        "name": "Loyd",
        "nickNames": ["Loy","Floy"]
    },
    {
        "name": "Lloyd",
        "nickNames": ["Loy","Floy"]
    },


    {
        "name": "Lonny",
        "nickNames": ["Alonso"]
    },
    {
        "name": "Lonnie",
        "nickNames": ["Alonso"]
    },

    {
        "name": "Louis",
        "nickNames": ["Lou","Louie"]
    },


    {
        "name": "Lucius",
        "nickNames": ["Lu","Luke"]
    },
    {
        "name": "Lucian",
        "nickNames": ["Lu","Luke"]
    },

    {
        "name": "Lucas",
        "nickNames": ["Luke"]
    },
    {
        "name": "Luke",
        "nickNames": ["Luke"]
    },

    {
        "name": "Luther",
        "nickNames": ["Loot","Luth"]
    },



    {
        "name": "Malcolm",
        "nickNames": ["Mal","Malc","Mac"]
    },

    {
        "name": "Manuel",
        "nickNames": ["Manny","Mannie"]
    },


    {
        "name": "Marcus",
        "nickNames": ["Mark","Marc"]
    },
    {
        "name": "Marc",
        "nickNames": ["Mark","Marc"]
    },
    {
        "name": "Mark",
        "nickNames": ["Mark","Marc"]
    },


    {
        "name": "Martin",
        "nickNames": ["Mart","Marty"]
    },

    {
        "name": "Mervin",
        "nickNames": ["Marv","Merv"]
    },
    {
        "name": "Marvin",
        "nickNames": ["Marv","Merv"]
    },

    {
        "name": "Matthew",
        "nickNames": ["Matt","Mat","Matty","Mattie"]
    },

    {
        "name": "Matthias",
        "nickNames": ["Matt","Mat","Matty","Mattie"]
    },

    {
        "name": "Morris",
        "nickNames": ["Morry","Morey","Moe"]
    },
    {
        "name": "Maurice",
        "nickNames": ["Morry","Morey","Moe"]
    },

    {
        "name": "Maximilian",
        "nickNames": ["Max"]
    },

    {
        "name": "Maxwell",
        "nickNames": ["Max"]
    },


    {
        "name": "Melvin",
        "nickNames": ["Mel"]
    },

    {
        "name": "Merlin",
        "nickNames": ["Merle"]
    },


    {
        "name": "Michael",
        "nickNames": ["Mike","Mikey","Mick","Mickey","Micky"]
    },

    {
        "name": "Myles",
        "nickNames": ["Milo"]
    },
    {
        "name": "Miles",
        "nickNames": ["Milo"]
    },

    {
        "name": "Milo",
        "nickNames": ["Myles","Miles"]
    },

    {
        "name": "Milton",
        "nickNames": ["Milt"]
    },

    {
        "name": "Mitchell",
        "nickNames": ["Mitch"]
    },


    {
        "name": "Montague",
        "nickNames": ["Monty","Monte"]
    },

    {
        "name": "Montgomery",
        "nickNames": ["Monty","Monte"]
    },

    {
        "name": "Morgan",
        "nickNames": ["Mo"]
    },

    {
        "name": "Mortimer",
        "nickNames": ["Mort","Morty"]
    },

    {
        "name": "Morton",
        "nickNames": ["Mort","Morty"]
    },

    {
        "name": "Moses",
        "nickNames": ["Mo","Moe","Mose","Moss"]
    },


    {
        "name": "Nathaniel",
        "nickNames": ["Nat","Nate","Natty"]
    },
    {
        "name": "Nathan",
        "nickNames": ["Nat","Nate","Natty"]
    },


    {
        "name": "Nelson",
        "nickNames": ["Nel","Nell","Nels"]
    },

    {
        "name": "Neville",
        "nickNames": ["Nev"]
    },
    {
        "name": "Nevile",
        "nickNames": ["Nev"]
    },
    {
        "name": "Nevil",
        "nickNames": ["Nev"]
    },
    {
        "name": "Nevill",
        "nickNames": ["Nev"]
    },

    {
        "name": "Newton",
        "nickNames": ["Newt"]
    },

    {
        "name": "Nicolas",
        "nickNames": ["Nick","Nicky","Nicol","Cole","Colin"]
    },
    {
        "name": "Nicholas",
        "nickNames": ["Nick","Nicky","Nicol","Cole","Colin"]
    },

    {
        "name": "Nigel",
        "nickNames": ["Nige"]
    },



    {
        "name": "Norbert",
        "nickNames": ["Bert"]
    },

    {
        "name": "Norris",
        "nickNames": ["Nor","Norrie"]
    },

    {
        "name": "Norman",
        "nickNames": ["Norm","Normie","Nor","Norrie"]
    },

    {
        "name": "Norton",
        "nickNames": ["Nort"]
    },

    {
        "name": "Oliver",
        "nickNames": ["Ollie","Noll","Nollie","Nolly"]
    },


    {
        "name": "Orville",
        "nickNames": ["Orv","Ollie"]
    },

    {
        "name": "Osbert",
        "nickNames": ["Ossy","Ozzie","Ozzy","Bert"]
    },

    {
        "name": "Osborne",
        "nickNames": ["Ossy","Ozzie","Ozzy"]
    },
    {
        "name": "Osborn",
        "nickNames": ["Ossy","Ozzie","Ozzy"]
    },

    {
        "name": "Oscar",
        "nickNames": ["Os","Ossy"]
    },

    {
        "name": "Osmund",
        "nickNames": ["Ossy","Ozzie","Ozzy"]
    },
    {
        "name": "Osmond",
        "nickNames": ["Ossy","Ozzie","Ozzy"]
    },

    {
        "name": "Oswold",
        "nickNames": ["Os","Ossy","Oz","Ozzie","Ozzy"]
    },
    {
        "name": "Oswald",
        "nickNames": ["Os","Ossy","Oz","Ozzie","Ozzy"]
    },



    {
        "name": "Patrick",
        "nickNames": ["Pat","Paddy","Patsy"]
    },

    {
        "name": "Paul",
        "nickNames": ["Pauly"]
    },

    {
        "name": "Perceval",
        "nickNames": ["Percy","Perce"]
    },
    {
        "name": "Percival",
        "nickNames": ["Percy","Perce"]
    },


    {
        "name": "Peter",
        "nickNames": ["Pete","Petie","Petey"]
    },

    {
        "name": "Phillip",
        "nickNames": ["Phil","Pip"]
    },
    {
        "name": "Philip",
        "nickNames": ["Phil","Pip"]
    },

    {
        "name": "Quinton",
        "nickNames": ["Quinn"]
    },

    {
        "name": "Quenton",
        "nickNames": ["Quinn"]
    },
    {
        "name": "Quintin",
        "nickNames": ["Quinn"]
    },
    {
        "name": "Quentin",
        "nickNames": ["Quinn"]
    },

    {
        "name": "Quincey",
        "nickNames": ["Quinn"]
    },
    {
        "name": "Quincy",
        "nickNames": ["Quinn"]
    },

    {
        "name": "Ralph",
        "nickNames": ["Raff","Rafe","Ralphy"]
    },

    {
        "name": "Randal",
        "nickNames": ["Rand","Randy"]
    },
    {
        "name": "Randall",
        "nickNames": ["Rand","Randy"]
    },

    {
        "name": "Randolph",
        "nickNames": ["Rand","Randy","Dolph"]
    },

    {
        "name": "Rafael",
        "nickNames": ["Raff","Rafe"]
    },
    {
        "name": "Raphael",
        "nickNames": ["Raff","Rafe"]
    },

    {
        "name": "Raymund",
        "nickNames": ["Ray"]
    },
    {
        "name": "Raymond",
        "nickNames": ["Ray"]
    },

    {
        "name": "Reginald",
        "nickNames": ["Reg","Reggie","Renny","Rex"]
    },


    {
        "name": "Rubin",
        "nickNames": ["Rube","Ruby"]
    },
    {
        "name": "Ruben",
        "nickNames": ["Rube","Ruby"]
    },
    {
        "name": "Reuben",
        "nickNames": ["Rube","Ruby"]
    },

    {
        "name": "Reynold",
        "nickNames": ["Ray"]
    },

    {
        "name": "Richard",
        "nickNames": ["Dick","Rick","Ricky","Rich","Richie"]
    },

    {
        "name": "Rick",
        "nickNames": ["Ricky","Cedric","Derek","Eric","Frederick","Richard","Roderic","Broderick","Kendrick"]
    },

    {
        "name": "Robert",
        "nickNames": ["Bob","Bobbie","Bobby","Dob","Rob","Robbie","Robby","Robin","Bert"]
    },

    {
        "name": "Roderick",
        "nickNames": ["Rod","Roddy","Rick","Ricky"]
    },
    {
        "name": "Roderic",
        "nickNames": ["Rod","Roddy","Rick","Ricky"]
    },

    {
        "name": "Rodney",
        "nickNames": ["Rod","Roddy"]
    },

    {
        "name": "Rodger",
        "nickNames": ["Rod","Roddy","Rodge","Roge"]
    },
    {
        "name": "Roger",
        "nickNames": ["Rod","Roddy","Rodge","Roge"]
    },

    {
        "name": "Rowland",
        "nickNames": ["Rolly","Roly","Rowly","Orlando"]
    },
    {
        "name": "Roland",
        "nickNames": ["Rolly","Roly","Rowly","Orlando"]
    },

    {
        "name": "Rolf",
        "nickNames": ["Rudy","Rolf","Rolph","Dolph","Dolf"]
    },
    {
        "name": "Rolfe",
        "nickNames": ["Rudy","Rolf","Rolph","Dolph","Dolf"]
    },
    {
        "name": "Rolph",
        "nickNames": ["Rudy","Rolf","Rolph","Dolph","Dolf"]
    },

    {
        "name": "Roman",
        "nickNames": ["Rom","Romy"]
    },

    {
        "name": "Ronald",
        "nickNames": ["Ron","Ronnie","Ronny"]
    },

    {
        "name": "Ron",
        "nickNames": ["Ronnie","Ronny","Aaron","Byron","Cameron","Ronald"]
    },

    {
        "name": "Roscoe",
        "nickNames": ["Ross"]
    },



    {
        "name": "Rudolf",
        "nickNames": ["Rudy","Rolf","Rolph","Dolph","Dolf"]
    },
    {
        "name": "Rudolph",
        "nickNames": ["Rudy","Rolf","Rolph","Dolph","Dolf"]
    },

    {
        "name": "Rufus",
        "nickNames": ["Rufe"]
    },


    {
        "name": "Russel",
        "nickNames": ["Russ"]
    },
    {
        "name": "Russell",
        "nickNames": ["Russ"]
    },


    {
        "name": "Sampson",
        "nickNames": ["Sam","Sammy"]
    },
    {
        "name": "Samson",
        "nickNames": ["Sam","Sammy"]
    },

    {
        "name": "Samuel",
        "nickNames": ["Sam","Sammy"]
    },

    {
        "name": "Sanford",
        "nickNames": ["Sandy","Ford"]
    },


    {
        "name": "Scott",
        "nickNames": ["Scotty"]
    },

    {
        "name": "Sean",
        "nickNames": ["Shaun","Shawn","Shane"]
    },

    {
        "name": "Sebastian",
        "nickNames": ["Seb","Bass"]
    },



    {
        "name": "Seymour",
        "nickNames": ["Morey","Sy"]
    },

    {
        "name": "Shannon",
        "nickNames": ["Shanon"]
    },

    {
        "name": "Sheldon",
        "nickNames": ["Shelly","Shel","Don"]
    },

    {
        "name": "Shellie",
        "nickNames": ["Shel"]
    },
    {
        "name": "Shelly",
        "nickNames": ["Shel"]
    },
    {
        "name": "Shelley",
        "nickNames": ["Shel"]
    },


    {
        "name": "Shelton",
        "nickNames": ["Shelly","Shel","Tony"]
    },

    {
        "name": "Sydney",
        "nickNames": ["Sid","Syd"]
    },
    {
        "name": "Sidney",
        "nickNames": ["Sid","Syd"]
    },

    {
        "name": "Silas",
        "nickNames": ["Si","Sy"]
    },

    {
        "name": "Sylvester",
        "nickNames": ["Syl","Vester"]
    },
    {
        "name": "Silvester",
        "nickNames": ["Syl","Vester"]
    },

    {
        "name": "Simeon",
        "nickNames": ["Sim","Simie","Simmy"]
    },

    {
        "name": "Simon",
        "nickNames": ["Si","Sy","Sim","Simie","Simmy"]
    },

    {
        "name": "Solomon",
        "nickNames": ["Sol","Solly","Sal"]
    },

    {
        "name": "Sonny",
        "nickNames": ["Son"]
    },


    {
        "name": "Stacey",
        "nickNames": ["Eustace"]
    },
    {
        "name": "Stacy",
        "nickNames": ["Eustace"]
    },

    {
        "name": "Stanley",
        "nickNames": ["Stan"]
    },

    {
        "name": "Steven",
        "nickNames": ["Steve","Stevie","Steph","Steff","Stef"]
    },
    {
        "name": "Stephan",
        "nickNames": ["Steve","Stevie","Steph","Steff","Stef"]
    },
    {
        "name": "Steffan",
        "nickNames": ["Steve","Stevie","Steph","Steff","Stef"]
    },
    {
        "name": "Stefan",
        "nickNames": ["Steve","Stevie","Steph","Steff","Stef"]
    },
    {
        "name": "Stephen",
        "nickNames": ["Steve","Stevie","Steph","Steff","Stef"]
    },

    {
        "name": "Stewart",
        "nickNames": ["Stu","Stew"]
    },
    {
        "name": "Stuart",
        "nickNames": ["Stu","Stew"]
    },

    {
        "name": "Terrence",
        "nickNames": ["Terry"]
    },
    {
        "name": "Terrance",
        "nickNames": ["Terry"]
    },
    {
        "name": "Terence",
        "nickNames": ["Terry"]
    },

    {
        "name": "Thadeus",
        "nickNames": ["Tad","Thad"]
    },
    {
        "name": "Thaddeus",
        "nickNames": ["Tad","Thad"]
    },

    {
        "name": "Theodor",
        "nickNames": ["Ted","Teddy","Theo","Terry"]
    },
    {
        "name": "Theodore",
        "nickNames": ["Ted","Teddy","Theo","Terry"]
    },

    {
        "name": "Thomas",
        "nickNames": ["Tom","Tommy"]
    },

    {
        "name": "Timothy",
        "nickNames": ["Tim","Timmy"]
    },

    {
        "name": "Tobias",
        "nickNames": ["Toby","Tobi","Tobie"]
    },


    {
        "name": "Tony",
        "nickNames": ["Anthony"]
    },

    {
        "name": "Tracy",
        "nickNames": ["Tracey"]
    },

    {
        "name": "Travis",
        "nickNames": ["Trav"]
    },

    {
        "name": "Trenton",
        "nickNames": ["Trent"]
    },

    {
        "name": "Trevor",
        "nickNames": ["Trev"]
    },

    {
        "name": "Tristan",
        "nickNames": ["Tris"]
    },
    {
        "name": "Tristam",
        "nickNames": ["Tris"]
    },
    {
        "name": "Tristram",
        "nickNames": ["Tris"]
    },


    {
        "name": "Tyler",
        "nickNames": ["Ty"]
    },

    {
        "name": "Tyron",
        "nickNames": ["Ty"]
    },
    {
        "name": "Tyrone",
        "nickNames": ["Ty"]
    },

    {
        "name": "Ulysses",
        "nickNames": ["Uly","Uli","Lyss"]
    },

    {
        "name": "Urias",
        "nickNames": ["Uri","Uria"]
    },
    {
        "name": "Uriah",
        "nickNames": ["Uri","Uria"]
    },

    {
        "name": "Valentin",
        "nickNames": ["Val"]
    },
    {
        "name": "Valentine",
        "nickNames": ["Val"]
    },

    {
        "name": "Valerius",
        "nickNames": ["Val"]
    },
    {
        "name": "Valerian",
        "nickNames": ["Val"]
    },

    {
        "name": "Van",
        "nickNames": ["Vance"]
    },

    {
        "name": "Vance",
        "nickNames": ["Van"]
    },

    {
        "name": "Vaughan",
        "nickNames": ["Vaughn"]
    },

    {
        "name": "Vernon",
        "nickNames": ["Vern","Verne"]
    },

    {
        "name": "Victor",
        "nickNames": ["Vic","Vick"]
    },

    {
        "name": "Vincent",
        "nickNames": ["Vince","Vin","Vinny"]
    },

    {
        "name": "Vergil",
        "nickNames": ["Virge"]
    },
    {
        "name": "Virgil",
        "nickNames": ["Virge"]
    },

    {
        "name": "Wallis",
        "nickNames": ["Wally","Wallie"]
    },
    {
        "name": "Wallace",
        "nickNames": ["Wally","Wallie"]
    },


    {
        "name": "Walter",
        "nickNames": ["Walt","Wally","Wallie"]
    },



    {
        "name": "Wesley",
        "nickNames": ["Wes"]
    },

    {
        "name": "Wendell",
        "nickNames": ["Dell","Del"]
    },

    {
        "name": "Wilbert",
        "nickNames": ["Will","Willie","Willy","Bert"]
    },

    {
        "name": "Wilber",
        "nickNames": ["Will","Willie","Willy"]
    },
    {
        "name": "Wilbur",
        "nickNames": ["Will","Willie","Willy"]
    },

    {
        "name": "Wiley",
        "nickNames": ["Will","Willie","Willy"]
    },

    {
        "name": "Wilfrid",
        "nickNames": ["Will","Willie","Willy","Fred","Freddie","Freddy"]
    },
    {
        "name": "Wilfred",
        "nickNames": ["Will","Willie","Willy","Fred","Freddie","Freddy"]
    },

    {
        "name": "Willard",
        "nickNames": ["Will","Willie","Willy"]
    },

    {
        "name": "William",
        "nickNames": ["Bill","Billy","Billie","Will","Willie","Willy","Liam"]
    },

    {
        "name": "Willis",
        "nickNames": ["Bill","Billy","Billie","Will","Willie","Willy"]
    },

    {
        "name": "Wilson",
        "nickNames": ["Will","Willie","Willy"]
    },

    {
        "name": "Winfrid",
        "nickNames": ["Win","Winnie","Winny","Fred","Freddie","Freddy"]
    },
    {
        "name": "Winfred",
        "nickNames": ["Win","Winnie","Winny","Fred","Freddie","Freddy"]
    },

    {
        "name": "Winston",
        "nickNames": ["Win","Winnie","Winny"]
    },

    {
        "name": "Woodrow",
        "nickNames": ["Wood","Woody"]
    },

    {
        "name": "Xavier",
        "nickNames": ["Zave"]
    },

    {
        "name": "Zachary",
        "nickNames": ["Zack","Zacky","Zach"]
    },
    {
        "name": "Zacharias",
        "nickNames": ["Zack","Zacky","Zach"]
    },
    {
        "name": "Zachariah",
        "nickNames": ["Zack","Zacky","Zach"]
    }
]

module.exports = {
    createNickname
};