const {
    scanDynamoRecords,
    createDynamoRecord,
    queryDynamoRecords
} = require('../dynamodbHelper.js')


const HIDDEN_ISSUE_TYPE_CREATE_ISSUE = "HIDDEN_ISSUE_TYPE_CREATE_ISSUE";
const HIDDEN_KUDO_TYPE_CREATE_KUDO = "HIDDEN_ISSUE_TYPE_CREATE_ISSUE";


async function handlerHera3113(){
    //uncomment the following line to run the script
    //await loadAllTenants()
}


async function loadAllTenants(){
    try {
        let lastEvaluatedKey = null;
        do{
            let params = {
                TableName: process.env.TENANT_TABLE,
                ExpressionAttributeNames: {
                    '#g': 'group'
                },
                ProjectionExpression: "id, #g",
                ExclusiveStartKey: lastEvaluatedKey
            }
            const result = await scanDynamoRecords(params);
            lastEvaluatedKey = result.LastEvaluatedKey;
            console.log('lastEvaluatedKey: ', lastEvaluatedKey);

            await createCustomList(result.Items);

        }while(lastEvaluatedKey);
    } catch (error) {
        console.log("[loadAllTenants] Error in function loadAllTenants", error);
    } finally {
        console.log('***************************');
        console.log('Update Finished');
        console.log('***************************');
    }
}



async function createCustomList(tenants){

    const newCustomLists = [
        {
            type: 'issue-type',
            listCategory: 'Performance & Coaching',
            listName: 'Associate Issue Type',
            listDisplay: 'Text Only',
            canDeleteAllOptions: false,
        },
        {
            type: 'kudo-type',
            listCategory: 'Performance & Coaching',
            listName: 'Associate Kudo Type',
            listDisplay: 'Text Only',
            canDeleteAllOptions: false,
        }
    ]

    for(let customList of newCustomLists){
        for(const tenant of tenants){

            let customListCreateParams = {
                TableName: process.env.CUSTOM_LIST_TABLE,
                Item: {
                    __typename: 'CustomLists',
                    id: generateUUID(),
                    group: tenant.group,
                    type: customList.type,
                    listCategory: customList.listCategory,
                    listName: customList.listName,
                    listDisplay: customList.listDisplay,
                    canDeleteAllOptions: customList.canDeleteAllOptions,
                    createdAt: new Date().toISOString(),
                    updatedAt: new Date().toISOString(),
                }
            };
            
            try {
                //CREATE CUSTOM LIST
                await createDynamoRecord(customListCreateParams);
            } catch (error) {
                console.log('Error in create custom list', error)
                continue;
            }
        

            //key for value list
            const key =  customList.type === 'issue-type' ? 'infraction-type' : 'kudo-type';
           
            const valueListId  = await getValueList(tenant.group, key)
            let valueListItems = []
            if(valueListId){
                valueListItems = await getValueListItems(valueListId)
            }
            const optionsCustomLists = valueListItems.sort(sortByValue)


            let order = 1;
            for(const option of optionsCustomLists){
                
                let optionsCustomListCreateParams = {
                    TableName: process.env.OPTIONS_CUSTOM_LIST_TABLE,
                    Item: {
                        __typename: 'OptionsCustomLists',
                        id: generateUUID(),
                        group: tenant.group,
                        optionsCustomListsCustomListsId: customListCreateParams.Item.id,
                        order: order,
                        option: option.value,
                        default: false,
                        isEnabled: true,
                        isCustom: option.custom ? true : false,
                        usedFor: 0,
                        daysCount: 0,
                        canBeEdited: false,
                        canBeDeleted: false,
                        canBeReorder: true,
                        driverReportSetting: option.driverReportSetting ? option.driverReportSetting : 0,
                        createdAt: new Date().toISOString(),
                        updatedAt: new Date().toISOString(),
                    }
                }

                //is hidden option
                if(!!option.hidden){
                    const hiddenValue = customList.type === 'issue-type' ? [ HIDDEN_ISSUE_TYPE_CREATE_ISSUE ] : [ HIDDEN_KUDO_TYPE_CREATE_KUDO ];
                    optionsCustomListCreateParams.Item.isHiddenForSections = hiddenValue;
                }
                
                try {  
                    //CREATE OPTIONS CUSTOM LIST
                    await createDynamoRecord(optionsCustomListCreateParams);
                    order++;
                } catch (error) {
                    console.log('Error in create option custom list', error)
                    continue;
                }
            }
        }
    }
}


async function getValueList(group, key){
   
    try {
        let lastEvaluatedKey = null;
        let queryParams = {
            TableName: process.env.VALUELIST_TABLE,
            IndexName: 'byGroup',
            ExpressionAttributeNames: {
                '#g': 'group',
                '#k': 'key'
            },
            ExpressionAttributeValues: {
                ':g': group,
                ':k': key
            },
           
            KeyConditionExpression: '#g = :g AND #k = :k',
            ProjectionExpression: "id, #g, #k",
            ExclusiveStartKey: lastEvaluatedKey
        }
        const result = await queryDynamoRecords(queryParams);
        lastEvaluatedKey = result.LastEvaluatedKey;
      
        return result.Items[0]?.id
    } catch (error) {
        console.log('error in getValueList function', error)
    }
}

async function getValueListItems(valueListId){
    let valueListItems = []
    
    try {
        let lastEvaluatedKey = null;
        do {
            let queryParams = {
                TableName: process.env.VALUELIST_ITEM_TABLE,
                IndexName: 'gsi-ValueListItems',
                ExpressionAttributeNames: {
                    '#valueListId': 'valueListItemValueListId',
                    '#g' : 'group',
                    '#v' : 'value',
                    '#h' : 'hidden',
                    '#c' : 'custom',
                    '#d' : 'driverReportSetting'
                },
                ExpressionAttributeValues: {
                    ':valueListId': valueListId 
                },
                KeyConditionExpression: '#valueListId = :valueListId',
                ProjectionExpression: "id, #g, #v, #h, #c, #d",
                ExclusiveStartKey: lastEvaluatedKey
            }
            const result = await queryDynamoRecords(queryParams);
            lastEvaluatedKey = result.LastEvaluatedKey;
            valueListItems = valueListItems.concat(result.Items);
        } while (lastEvaluatedKey);
        
    } catch (error) {
        console.log('error in getValueListItems function', error)
    } finally {
        return valueListItems
    }
}


function generateUUID() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

function sortByValue(a, b) {
    return a?.value.toLowerCase().localeCompare(b?.value.toLowerCase())
};

module.exports = { handlerHera3113 }; 