async function updateStaffScoreCard(staffcorecards) {

    for (var element of staffcorecards) {
        let params = {
            TableName: process.env.STAFFSCORECARD_TABLE,
            Key: {
                "id": element.id
            },
            ExpressionAttributeValues: {
                ":v": element.matchedS + '#' + element.year + '#' + element.week + '#' + element.name,
            },
            ExpressionAttributeNames: {
                "#a": 'matchedS#year#week#name'
            },
            UpdateExpression: "set #a = :v",
            ReturnValues: "UPDATED_NEW"
        };

        await ddb.update(params, function(err, data) {
            if (err) {
                console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
            } else {
                console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
            }
        }).promise();

    }
    console.log('done')
    return 'done'
}

module.exports = {
    updateStaffScoreCard
}