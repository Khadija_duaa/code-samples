const {
  scanDynamoRecords,
  queryDynamoRecords,
} = require("./dynamodbHelper");

const {
  executeQuery,
} = require('./appSync/appSync');

const { 
  invoicesByGroupAndYearAndMonth,
} = require('./helpers/queryHelpers')

const today = new Date() // hardcode date here for testing - Ex: new Date(2021/03/01)
const year = today.getFullYear()
const month = today.getMonth()-1 //previous month
const day = today.getDate()
const months = ['January','February','March','April','May','June','July','August','September','October','November','December']
const numOfDays = new Date(year, month + 1, 0).getDate();
console.log("number of days: " + numOfDays);

async function runHera2689 () {
  //  Get all Tenants
  let tenants = await getAllTenants()
  console.log({ numberOfTenants: tenants.length });
  console.log({ tenants });
  if(!tenants.length) return
  await calculateAmountToCharge(tenants)
}

async function getAllTenants() {
  try {
      let tenants = []
      let lastEvaluatedKey = null
      do {
          let params =  {
            ExpressionAttributeNames: {
              '#g': 'group',
              '#a': 'accountPremiumStatus'
            },
            ExpressionAttributeValues: {
              ':n': 'None'
            },
            FilterExpression: "attribute_exists(#a) and NOT contains(#a, :n)",
            TableName: process.env.TENANT_TABLE,
            ProjectionExpression: '#g,#a,id,companyName,trialExpDate, costStandard, costBundle, costPerformance, costRostering, costStaff, costVehicles, costMessaging, discountPercent, discountFixed, discountPercentLabel, discountFixedLabel, addressLine1, addressLine2, addressCity, addressState, addressZip, shortCode, stripeBillingEmail',
            ExclusiveStartKey: lastEvaluatedKey
          };
          let scanResult = await scanDynamoRecords(params);
          let items = scanResult.Items
          lastEvaluatedKey = scanResult.LastEvaluatedKey
          tenants = [...tenants, ...items]
      } while (lastEvaluatedKey)
      return tenants
  } catch (e) {
      console.log("[getAllTenants] Error in function getAllTenants", e);
      return {
          error: e,
      };
  }
}

async function calculateAmountToCharge(tenants) {
  let tenantData = []
  for(const tenant of tenants) {
    try {
      // skip tenant if plan is "None"
      if(tenant.accountPremiumStatus.includes('None')) continue 

      let activeAssociates = await getAllActiveAssociates(tenant.group)
      // console.log({ totalActiveAssociates: activeAssociates.length });

      if(!activeAssociates.length) continue

      let numActiveDas = activeAssociates.length

      // determine if tenant still has an active trial
      let isTrial = false
      let todayMidnightUnixTime = new Date(today).setHours(0,0,0,0)
      let trialExpMidnightUnixTime = new Date(tenant.trialExpDate).setHours(0,0,0,0)
      let isTrialExpired =  trialExpMidnightUnixTime < todayMidnightUnixTime

      if(!isTrialExpired || tenant.accountPremiumStatus.includes('trial')){ // don't charge if they haven't upgraded yet
        var costStandardExt = 0
        var costBundleExt = 0
        var costPerformanceExt = 0
        var costRosteringExt = 0
        var costStaffExt = 0
        var costVehiclesExt = 0
        var totalCost = 0
        isTrial = true
      }
      else{
        var costStandardExt = ( tenant.costStandard * numActiveDas * tenant.accountPremiumStatus.includes('standard') ) / numOfDays;
        var costBundleExt = ( tenant.costBundle * numActiveDas * tenant.accountPremiumStatus.includes('bundle') ) / numOfDays;
        var costPerformanceExt = ( tenant.costPerformance * numActiveDas * tenant.accountPremiumStatus.includes('performance') ) / numOfDays;
        var costRosteringExt = ( tenant.costRostering * numActiveDas * tenant.accountPremiumStatus.includes('rostering') ) / numOfDays
        var costStaffExt = ( tenant.costStaff * numActiveDas * tenant.accountPremiumStatus.includes('staff') ) / numOfDays;
        var costVehiclesExt = ( tenant.costVehicles * numActiveDas * tenant.accountPremiumStatus.includes('vehicles') ) / numOfDays;
        var totalCost = costStandardExt + costBundleExt + costPerformanceExt + costRosteringExt + costStaffExt + costVehiclesExt;
      }

      // Get Invoice record
      let invoices = await getAllInvoices(tenant.group)

      if(!invoices.items.length) {
        let input = {
          ['Tenant Name']: tenant.companyName,
          ['Group']: tenant.group,
          // ['Account Premium Status']: tenant.accountPremiumStatus,
          ['Total Active Associates']: activeAssociates.length,
          ['Total expected charge by today']: totalCost,
          // ['Invoices Line Items']: 0,
          ['Total To Charge']: 31 * totalCost
        }
        tenantData.push(input)
        // console.log({
        //   tile: 'CHARGE X 31 DAYS',
        //   group: tenant.group,
        //   status: tenant.accountPremiumStatus,
        //   totalActiveAssociates: activeAssociates.length,
        //   totalCost,
        //   totals: costStandardExt + ", " + costBundleExt + ", " + costPerformanceExt + ", " + costRosteringExt + ", " + costStaffExt + ", " + costVehiclesExt + ", " + totalCost,
        //   invoices: invoices.items[0],
        //   totalToCharge: 31 * totalCost
        // });
      } else {
        let invoiceLineItems = await getAllInvoiceLineItems(invoices.items[0].id,)

        let input = {
          // ['Title']: 'CHARGE DIFFERENCE',
          ['Tenant Name']: tenant.companyName,
          ['Group']: tenant.group,
          // ['Account Premium Status']: tenant.accountPremiumStatus,
          ['Total Active Associates']: activeAssociates.length,
          ['Total expected charge by today']: totalCost,
          // ['Invoices Line Items']: invoiceLineItems.length,
          ['Total To Charge']: (31 - invoiceLineItems.length) * totalCost
        }
        tenantData.push(input)
        // console.log({
        //   tile: 'CHARGE DIFFERENCE',
        //   group: tenant.group,
        //   status: tenant.accountPremiumStatus,
        //   totalActiveAssociates: activeAssociates.length,
        //   totalCost,
        //   totals: costStandardExt + ", " + costBundleExt + ", " + costPerformanceExt + ", " + costRosteringExt + ", " + costStaffExt + ", " + costVehiclesExt + ", " + totalCost,
        //   invoices: invoices.items[0],
        //   invoiceId: invoices.items[0].id,
        //   invoiceLineItemsLength: invoiceLineItems.length,
        //   invoiceLineItems,
        //   totalToCharge: (31 - invoiceLineItems.length) * totalCost
        // });
      }
    } catch (e) {
      console.log('[Error Tenant] inner for loop:', e);
    }
  }
  const filteredData = tenantData.filter(item  => item['Total To Charge'] > 0)
  console.log(JSON.stringify(filteredData));
}

async function getAllActiveAssociates(group){
  try {
    let activeAssociates = []
    let lastEvaluatedKey = null
    do{
      let params = {
        ExpressionAttributeValues: {
          ':g': group,
          ':s': 'Active'
        },
        ExpressionAttributeNames: {
          '#g': 'group',
          '#s': 'status'
        },
        KeyConditionExpression: '#g = :g',
        FilterExpression: '#s = :s',
        ProjectionExpression: 'id',
        TableName: process.env.STAFF_TABLE,
        IndexName: 'byGroup',
        ExclusiveStartKey: lastEvaluatedKey
      };
      var queryResult = await queryDynamoRecords(params);
      var items = queryResult.Items
      lastEvaluatedKey = queryResult.LastEvaluatedKey
      activeAssociates = [...activeAssociates, ...items]
    }while(lastEvaluatedKey)
    return activeAssociates
  } catch (e) {
    console.log("[getAllActiveAssociates] Error in function getAllActiveAssociates", e);    
  }
}

async function getAllInvoices(group) {

  let invoices = []
  try {
    let input = {
      group,
      yearMonth:{
        eq: {
          month: month,
          year: year
        }
      }
    }
    let result = await executeQuery(invoicesByGroupAndYearAndMonth, input)
    invoices = result.invoicesByGroupAndYearAndMonth
    return invoices
  } catch (e) {
    console.log("[getAllInvoices] Error in function getAllInvoices", e);
  }
}

async function getAllInvoiceLineItems(invoiceId) {
  try {
    let invoiceLineItems = []
    let lastEvaluatedKey = null
    do{
      let params = {
        ExpressionAttributeValues: {
          ':i': invoiceId,
        },
        ExpressionAttributeNames: {
          '#i': 'invoiceLineItemInvoiceId',
          '#d': 'day',
          '#m': 'month',
          '#t': 'date'
        },
        KeyConditionExpression: '#i = :i',
        ProjectionExpression: 'id,standardCostExt,bundleCostExt,performanceCostExt,rosteringCostExt,staffCostExt,vehiclesCostExt,activeStaff,#d,#m, #t',
        TableName: process.env.INVOICE_LINE_ITEM_TABLE,
        IndexName: 'gsi-InvoiceInvoiceLineItems',
        ExclusiveStartKey: lastEvaluatedKey
      };
      var queryResult = await queryDynamoRecords(params);
      var items = queryResult.Items
      lastEvaluatedKey = queryResult.LastEvaluatedKey
      invoiceLineItems = [...invoiceLineItems, ...items]
    }while(lastEvaluatedKey)
    return invoiceLineItems
  } catch (e) {
    console.log("[getAllInvoiceLineItems] Error in function getAllInvoiceLineItems", e);    
  }
}

module.exports = {
  runHera2689
}