const { runHera1372 } = require('./hera1372.js')

exports.handler = async (event, context) => {
    await runHera1372()
    const response = {
      statusCode: 200,
      body: JSON.stringify('runHera1372 done'),
  };

  return response;
}