/**
 * This script sets 'documentImageAccidentId' and 'documentAccidentId' values to 'documentIncidentId'
 * and removes 'documentImageAccidentId' and 'documentAccidentId' fields so every media file attached
 * to an Accident record is on the same field
 */

const {
    scanDynamoRecords,
    updateDynamoRecord,
  } = require("./dynamodbHelper");
  
  /**
   * Main function
   */
  async function handleHera3651() {
    try {


      /** Accident Table */

          /** Print Accident Report */
          await handlePrintReportAccidents()

          /** Update Accidents */
          //await handleUpdateAccidentRecords()

      /** **************************** */

      /** Document Table */

          /** Print Document Report */
          // await handlePrintReportDocuments();

          /** Update Documents */
          //await handleUpdateDocumentRecords()

      /** ******************************* */

    } catch (error) {
      console.log(error);
    }
  }
  
  /**
   * Handles printing the report of the changes that this script will cause
   */
  async function handlePrintReportDocuments() {
    const reportObj = {
      summary: { toRemoveTraces: 0, toMigrate: 0 },
      toRemoveTraces: { count: 0, items: [] },
      toMigrate: { count: 0, items: [] },
    };
    await applyCallbackToAllFilteredDocuments((documentList) => {
      for (const document of documentList) {
        const accidentId = document.documentAccidentId || document.documentImageAccidentId;
        if(accidentId){
          if (document.documentIncidentId === accidentId) {
            reportObj.summary.toRemoveTraces += 1;
            reportObj.toRemoveTraces.count += 1;
            reportObj.toRemoveTraces.items.push({
              ...document,
              toSet_documentIncidentId: document.documentAccidentId || document.documentImageAccidentId,
            });
          }
          else {
            reportObj.summary.toMigrate += 1;
            reportObj.toMigrate.count += 1;
            reportObj.toMigrate.items.push(document);
          }
        }
      }
    });
    console.log('Report', JSON.stringify(reportObj, null, 2));
  }

  
  /**
   * Handles printing the report of the changes that this script will cause
   */
  async function handlePrintReportAccidents() {
    const reportObj = {
      summary: { toUpdate: 0, toSkip: 0 },
      toUpdate: { count: 0, items: [] },
      toSkip: { count: 0, items: [] },
    };
    await applyCallbackToAllFilteredAccidents((accidentList) => {
      for (const accident of accidentList) {
        if (typeof accident.scannedReport == 'string') {
          reportObj.summary.toUpdate += 1;
          reportObj.toUpdate.count += 1;
          reportObj.toUpdate.items.push(accident);
        }
        else {
          reportObj.summary.toSkip += 1;
          reportObj.toSkip.count += 1;
          reportObj.toMigrate.items.push(accident);
        }
      }
    });
    console.log('Report', JSON.stringify(reportObj, null, 2));
  }

  /**
   * Handles the update of Accident that have 'scannedReport' attribute set
   */
  async function handleUpdateDocumentRecords() {
    const MAX_COUNT_PER_BATCH = 20;
    const sendUpdates = async (documentList) => {
      const promises = documentList.map(async (document) => {
        const accidentId = document.documentAccidentId || document.documentImageAccidentId;
        if(accidentId){
          if(document.documentIncidentId === accidentId){
            await updateDocumentToRemoveTraces(document);
          }else{
            await updateDocumentToMigrate(document, accidentId)
          }
        }
      });
      await Promise.all(promises);
    };
    await applyCallbackToAllFilteredDocuments(async (documentList) => {
      let batchUpdateList = [];
      for (const document of documentList) {
        batchUpdateList.push(document);
        if (batchUpdateList.length === MAX_COUNT_PER_BATCH) {
          await sendUpdates(batchUpdateList);
          batchUpdateList = [];
        }
      }
      if (batchUpdateList.length) {
        await sendUpdates(batchUpdateList);
      }
    });
  }
  
  /**
   * Handles the update of Accident that have 'scannedReport' attribute set
   */
  async function handleUpdateAccidentRecords() {
    const MAX_COUNT_PER_BATCH = 20;
    const sendUpdates = async (accidentList) => {
      const promises = accidentList.map(async (accident) => {
        await updateAccident(accident.id)
      });
      await Promise.all(promises);
    };
    await applyCallbackToAllFilteredAccidents(async (accidentList) => {
      let batchUpdateList = [];
      for (const accident of accidentList) {
        if (typeof accident.scannedReport == 'string'){
          batchUpdateList.push(accident);
        }
        if (batchUpdateList.length === MAX_COUNT_PER_BATCH) {
          await sendUpdates(batchUpdateList);
          batchUpdateList = [];
        }
      }
      if (batchUpdateList.length) {
        await sendUpdates(batchUpdateList);
      }
    });
  }

  /**
   * Handles the update of Accident that have 'imageAccidentId' attribute set
   */
  async function handleUpdateDocumentRecords() {
    const MAX_COUNT_PER_BATCH = 20;
    const sendUpdates = async (documentList) => {
      const promises = documentList.map(async (document) => {
        const accidentId = document.documentAccidentId || document.documentImageAccidentId;
        if(accidentId){
          if(document.documentIncidentId === accidentId){
            await updateDocumentToRemoveTraces(document);
          }else{
            await updateDocumentToMigrate(document, accidentId)
          }
        }
      });
      await Promise.all(promises);
    };
    await applyCallbackToAllFilteredDocuments(async (documentList) => {
      let batchUpdateList = [];
      for (const document of documentList) {
        batchUpdateList.push(document);
        if (batchUpdateList.length === MAX_COUNT_PER_BATCH) {
          await sendUpdates(batchUpdateList);
          batchUpdateList = [];
        }
      }
      if (batchUpdateList.length) {
        await sendUpdates(batchUpdateList);
      }
    });
  }
  
  /**
   * Updates a Document record, sets 'documentImageAccidentId' value to 'documentAccidentId'
   * and removes 'documentImageAccidentId'
   * @param {Boolean} accidentId - Accident ID value to set to 'documentImageAccidentId' field
  */
  async function updateAccident(accidentId) {
    try {
      const params = {
        TableName: process.env.ACCIDENT_TABLE,
        Key: { id: accidentId },
        ExpressionAttributeNames: {
          "#scannedReport": "scannedReport",
        },
        UpdateExpression: "remove #scannedReport",
        ReturnValues: "UPDATED_NEW",
      };
      await updateDynamoRecord(params);
    }
    catch (error) {
      console.log(`Error updating document [ID=${document.id}]`);
    }
  }
  
  /**
   * Updates a Document record, sets 'documentImageAccidentId' value to 'documentAccidentId'
   * and removes 'documentImageAccidentId'
   * @param {Object} document - Document record to update
   * @param {Boolean} accidentId - Accident ID value to set to 'documentImageAccidentId' field
  */
  async function updateDocumentToMigrate(document, accidentId) {
    try {
      const params = {
        TableName: process.env.DOCUMENT_TABLE,
        Key: { id: document.id },
        ExpressionAttributeValues: {
          ":accidentId": accidentId,
        },
        ExpressionAttributeNames: {
          "#documentIncidentId": "documentIncidentId",
        },
        UpdateExpression: "set #documentIncidentId = :accidentId",
        ReturnValues: "UPDATED_NEW",
      };
      if(document.documentAccidentId != null){
        params.ExpressionAttributeValues[':documentAccidentId'] = null
        params.ExpressionAttributeNames['#documentAccidentId'] = 'documentAccidentId'
        params.UpdateExpression += ', #documentAccidentId = :documentAccidentId'
      }
      if(document.documentImageAccidentId != null){
        params.ExpressionAttributeValues[':documentImageAccidentId'] = null
        params.ExpressionAttributeNames['#documentImageAccidentId'] = 'documentImageAccidentId'
        params.UpdateExpression += ', #documentImageAccidentId = :documentImageAccidentId'
      }
      await updateDynamoRecord(params);
    }
    catch (error) {
      console.log(`Error updating document [ID=${document.id}]`);
    }
  }



  /**
   * Updates a Document record, sets 'documentImageAccidentId' value to 'documentAccidentId'
   * and removes 'documentImageAccidentId'
   * @param {Object} document - Document record to update
  */
  async function updateDocumentToRemoveTraces(document) {
    try {
      const params = {
        TableName: process.env.DOCUMENT_TABLE,
        Key: { id: document.id },
        ExpressionAttributeValues: {},
        ExpressionAttributeNames: {},
        ReturnValues: "UPDATED_NEW",
      };
      if(document.hasOwnProperty('documentAccidentId')){
        params.ExpressionAttributeNames['#documentAccidentId'] = 'documentAccidentId'
        params.UpdateExpression = `remove #documentAccidentId`
      }
      else if(document.hasOwnProperty('documentImageAccidentId')){
        params.ExpressionAttributeNames['#documentImageAccidentId'] = 'documentImageAccidentId'
        params.UpdateExpression = `remove #documentImageAccidentId`
      }
      await updateDynamoRecord(params);
    }
    catch (error) {
      console.log(`Error updating document [ID=${document.id}]`);
    }
    
  }
  
  /**
   * Applies a callback to a list of Documents records that have 'scannedReport' set as attribute
   * @param {Function} cb - Callback to execute for each group of Accident records retrieved from DynamoDB
   */
  async function applyCallbackToAllFilteredAccidents(cb) {
    let lastEvaluatedKey = null;
    try {
      do {
        const params = {
          ExpressionAttributeNames: {
            '#id': 'id',
            '#group': 'group',
            '#scannedReport': 'scannedReport'
          },
          TableName: process.env.ACCIDENT_TABLE,
          ProjectionExpression: '#id, #group, #scannedReport',
          FilterExpression: 'attribute_exists(#scannedReport)',
          ExclusiveStartKey: lastEvaluatedKey,
        };
        const scanResult = await scanDynamoRecords(params);
        await cb(scanResult.Items);
        lastEvaluatedKey = scanResult.LastEvaluatedKey;
        console.log({lastEvaluatedKey});
      }
      while (lastEvaluatedKey);
    }
    catch (error) {
      console.log("[applyCallbackToAllFilteredDocuments] Error", error);
    }
  }
  
  /**
   * Applies a callback to a list of Documents records that have 'documentImageAccidentId' set as attribute
   * @param {Function} cb - Callback to execute for each group of Document records retrieved from DynamoDB
   */
  async function applyCallbackToAllFilteredDocuments(cb) {
    let lastEvaluatedKey = null;
    try {
      do {
        const params = {
          ExpressionAttributeNames: {
            '#id': 'id',
            '#group': 'group',
            '#documentAccidentId': 'documentAccidentId',
            '#documentImageAccidentId': 'documentImageAccidentId',
            "#documentIncidentId": 'documentIncidentId'
          },
          TableName: process.env.DOCUMENT_TABLE,
          ProjectionExpression: '#id, #group, #documentAccidentId, #documentImageAccidentId, #documentIncidentId',
          FilterExpression: 'attribute_exists(#documentAccidentId) or attribute_exists(#documentImageAccidentId)',
          ExclusiveStartKey: lastEvaluatedKey,
        };
        const scanResult = await scanDynamoRecords(params);
        await cb(scanResult.Items);
        lastEvaluatedKey = scanResult.LastEvaluatedKey;
        console.log({lastEvaluatedKey});
      }
      while (lastEvaluatedKey);
    }
    catch (error) {
      console.log("[applyCallbackToAllFilteredDocuments] Error", error);
    }
  }
  
  module.exports = {
    handleHera3651,
  };
  