const {
    updateDynamoRecord,
    scanDynamoRecords
} = require('./dynamodbHelper')

const {
    getAllTenants,
    getVehicleTypeFromCustomList
} = require('./customList/helper')

async function runHera995() {
    let tenants = await getAllTenants()
    await setVehicleTypeCustomList(tenants)
}

async function setVehicleTypeCustomList(tenants){
    try {
        for(let tenant of tenants){
            const { tenkVanId, customDelVanId, standardParcelId } = await getVehicleTypeFromCustomList(tenant.group)
            let vehicles = await getVehiclesByGroup(tenant.group)

            let counter = 0
            for (let vehicle of vehicles) {
                /**Counter : Remove when testing globally */
                counter++
                if(counter == 10) break
                /*************************************** */
                console.log({ vehicle });
                let typeId = standardParcelId
                if (vehicle.customDeliveryVan && vehicle.overTenThousandPounds) {
                    typeId = customDelVanId
                } if (vehicle.customDeliveryVan) {
                    typeId = customDelVanId
                } if (vehicle.overTenThousandPounds) {
                    typeId = tenkVanId
                } 
                console.log({ typeId });
                try {
                    await updateDynamoRecord({
                        TableName: process.env.VEHICLE_TABLE,
                        Key: {"id": vehicle.id},
                        ExpressionAttributeNames: {"#vtId": 'vehicleVehicleTypeId'},
                        ExpressionAttributeValues: {":vtId": typeId},
                        UpdateExpression: "set #vtId = :vtId",
                        ReturnValues: "UPDATED_NEW"
                    })
                } catch(e) {
                    console.log("[UPDATE-setVehicleType] Error 2 in function setVehicleType", e);

                }
            }

        }
    } catch (e) {
        console.log("[setVehicleType] Error in function setVehicleType", e);
        return {
            error: e,
        };
        
    }
}

async function getVehiclesByGroup(group) {
    try {
        let vehicles = []
        let lastEvaluatedKey = null
        do {
            let params = {
                TableName: process.env.VEHICLE_TABLE,
                ExpressionAttributeNames: {
                    '#g': 'group',
                    '#o10k': 'overTenThousandPounds',
                    '#cdv': 'customDeliveryVan'
                },
                ExpressionAttributeValues: {
                    ':g': group,
                },
                FilterExpression: "#g = :g",
                ProjectionExpression: 'id, #g, #o10k, #cdv',
                ExclusiveStartKey: lastEvaluatedKey
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items
            lastEvaluatedKey = scanResult.LastEvaluatedKey
            vehicles = [...vehicles, ...items]
        } while (lastEvaluatedKey)
        return vehicles
    } catch (err) {
        console.log("[getAllVehicles] Error in function getAllVehicles", err);
        return {
            error: err,
        };
    }
}

module.exports = {
    runHera995
}