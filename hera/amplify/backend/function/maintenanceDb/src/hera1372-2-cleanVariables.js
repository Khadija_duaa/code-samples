const { scanDynamoRecords, updateDynamoRecord } = require("./dynamodbHelper.js");
var AWS = require("aws-sdk");

async function runHera1372() {
    // var tenants = await getAllTenants();
    // var tenants = await getTenantsWithEmptyField();
    // var tenants = await getTenantsWithEmptyAndOrifinationNumber();
    // var tenants = await getTenantsWithTzAuto();
    var tenants = await getTenantsWithTzAutoAndValidOriginaNumber();
    console.log(JSON.stringify(tenants));
    // await deletePrevCoachingSendTimeVariables(tenants);
    // await deletePrevCoachingSendTimeVariablesAndOmitOrignationNumber(tenants);
    // await deletePrevCoachingSendTimeVariablesTz(tenants);
    await deletePrevCoachingSendTimeVariablesTzAndKeepOrigNum(tenants)
    console.log({ TotalTenantsUpdated: tenants.length });
}

async function getTenantsWithTzAutoAndValidOriginaNumber() {
    try {
        let tenants = [];
        let lastEvaluatedKey = null;
        do {
            let params = {
                ExpressionAttributeNames: {
                    "#g": "group",
                    "#1": "coachingSendTime",
                    "#2": "coachingSendTimeLocal",
                    "#3": "coachingSendTimeUtcOffset",
                    "#4": "timeZoneSetAutomatically",
                    '#5': "automatedCoachingSendTime",
                    '#6': "originationNumber",
                    "#tz": "timeZone"
                },
                TableName: process.env.TENANT_TABLE,
                ProjectionExpression: "id, companyName, #g, #1, #2, #3, #4, #5, #6, #tz",
                FilterExpression: "attribute_exists(#4)",
                ExclusiveStartKey: lastEvaluatedKey,
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            tenants = [...tenants, ...items];
        } while (lastEvaluatedKey);

        return tenants;
    } catch (err) {
        console.log("[getAllTenants] Error in function getAllTenants", err);
        return {
            error: err,
        };
    }
}

async function getTenantsWithTzAuto() {
    try {
        let tenants = [];
        let lastEvaluatedKey = null;
        do {
            let params = {
                ExpressionAttributeNames: {
                    "#g": "group",
                    "#1": "coachingSendTime",
                    "#2": "coachingSendTimeLocal",
                    "#3": "coachingSendTimeUtcOffset",
                    "#4": "timeZoneSetAutomatically",
                    '#5': "automatedCoachingSendTime",
                    '#6': "originationNumber",
                    "#tz": "timeZone"
                },
                ExpressionAttributeValues: {
                    ":empty": "",
                },
                TableName: process.env.TENANT_TABLE,
                ProjectionExpression: "id, companyName, #g, #1, #2, #3, #4, #5, #6, #tz",
                FilterExpression: "attribute_exists(#4) and #6 =:empty",
                ExclusiveStartKey: lastEvaluatedKey,
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            tenants = [...tenants, ...items];
        } while (lastEvaluatedKey);

        return tenants;
    } catch (err) {
        console.log("[getAllTenants] Error in function getAllTenants", err);
        return {
            error: err,
        };
    }
}

async function getTenantsWithEmptyAndOrifinationNumber() {
    try {
        let tenants = [];
        let lastEvaluatedKey = null;
        do {
            let params = {
                ExpressionAttributeNames: {
                    "#g": "group",
                    "#1": "coachingSendTime",
                    "#2": "coachingSendTimeLocal",
                    "#3": "coachingSendTimeUtcOffset",
                    "#4": "timeZoneSetAutomatically",
                    '#5': "automatedCoachingSendTime",
                    '#6': "originationNumber",
                    "#tz": "timeZone"
                },
                TableName: process.env.TENANT_TABLE,
                ProjectionExpression: "id, companyName, #g, #1, #2, #3, #4, #5, #6, #tz",
                FilterExpression: "attribute_exists(#1)",
                ExclusiveStartKey: lastEvaluatedKey,
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            tenants = [...tenants, ...items];
        } while (lastEvaluatedKey);

        return tenants;
    } catch (err) {
        console.log("[getAllTenants] Error in function getAllTenants", err);
        return {
            error: err,
        };
    }
}

async function getTenantsWithEmptyField() {
    try {
        let tenants = [];
        let lastEvaluatedKey = null;
        do {
            let params = {
                ExpressionAttributeNames: {
                    "#g": "group",
                    "#1": "coachingSendTime",
                    "#2": "coachingSendTimeLocal",
                    "#3": "coachingSendTimeUtcOffset",
                    "#4": "timeZoneSetAutomatically",
                    '#5': "automatedCoachingSendTime",
                    '#6': "originationNumber",
                    "#tz": "timeZone"
                },
                ExpressionAttributeValues: {
                    ":empty": "",
                },
                TableName: process.env.TENANT_TABLE,
                ProjectionExpression: "id, companyName, #g, #1, #2, #3, #4, #5, #6, #tz",
                FilterExpression: "attribute_exists(#1) and #6 =:empty",
                ExclusiveStartKey: lastEvaluatedKey,
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            tenants = [...tenants, ...items];
        } while (lastEvaluatedKey);

        return tenants;
    } catch (err) {
        console.log("[getAllTenants] Error in function getAllTenants", err);
        return {
            error: err,
        };
    }
}

async function getAllTenants() {
    try {
        let tenants = [];
        let lastEvaluatedKey = null;
        do {
            let params = {
                ExpressionAttributeNames: {
                    "#g": "group",
                    "#1": "coachingSendTime",
                    "#2": "coachingSendTimeLocal",
                    "#3": "coachingSendTimeUtcOffset",
                    "#4": "timeZoneSetAutomatically",
                    '#5': "automatedCoachingSendTime",
                    '#6': "originationNumber",
                    "#tz": "timeZone"
                },
                ExpressionAttributeValues: {
                    ":empty": "",
                },
                TableName: process.env.TENANT_TABLE,
                ProjectionExpression: "id, companyName, #g, #1, #2, #3, #4, #5, #6, #tz",
                FilterExpression: "attribute_exists(#1) and #1 <>:empty and attribute_exists(#2) and #2 <>:empty and attribute_exists(#3) and #3 <>:empty and attribute_exists(#4)",
                ExclusiveStartKey: lastEvaluatedKey,
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            tenants = [...tenants, ...items];
        } while (lastEvaluatedKey);

        return tenants;
    } catch (err) {
        console.log("[getAllTenants] Error in function getAllTenants", err);
        return {
            error: err,
        };
    }
}

async function deletePrevCoachingSendTimeVariablesTzAndKeepOrigNum(tenants) {
    console.log('Deleting Previous Coaching Send Time Variables');
    let counter = 0
    for (const tenant of tenants) {
        try {
            await updateDynamoRecord({
                TableName: process.env.TENANT_TABLE,
                Key: {"id": tenant.id},
                ExpressionAttributeNames: {
                    "#tz": 'timeZoneSetAutomatically',
                },
                UpdateExpression: "remove #tz",
                ReturnValues: "UPDATED_NEW"
            })
            counter++;
        } catch (error) {
            console.log("[updateDynamoRecord] Error in function updateDynamoRecord", error);
            console.log("[updateDynamoRecord] Tenant", { tenant });
        }
    }
    console.log({ totalDeletedTenantsCoachingTimes: counter });
}

async function deletePrevCoachingSendTimeVariablesTz(tenants) {
    console.log('Deleting Previous Coaching Send Time Variables');
    let counter = 0
    for (const tenant of tenants) {
        try {
            await updateDynamoRecord({
                TableName: process.env.TENANT_TABLE,
                Key: {"id": tenant.id},
                ExpressionAttributeNames: {
                    "#tz": 'timeZoneSetAutomatically',
                },
                UpdateExpression: "remove #tz, originationNumber",
                ReturnValues: "UPDATED_NEW"
            })
            counter++;
        } catch (error) {
            console.log("[updateDynamoRecord] Error in function updateDynamoRecord", error);
            console.log("[updateDynamoRecord] Tenant", { tenant });
        }
    }
    console.log({ totalDeletedTenantsCoachingTimes: counter });
}

async function deletePrevCoachingSendTimeVariablesAndOmitOrignationNumber(tenants) {
    console.log('Deleting Previous Coaching Send Time Variables');
    let counter = 0
    for (const tenant of tenants) {
        try {
            await updateDynamoRecord({
                TableName: process.env.TENANT_TABLE,
                Key: {"id": tenant.id},
                ExpressionAttributeNames: {
                    "#timeZoneSetAutomatically": 'timeZoneSetAutomatically',
                    "#coachingSendTimeUtcOffset": 'coachingSendTimeUtcOffset',
                    '#coachingSendTimeLocal': 'coachingSendTimeLocal',
                    "#coachingSendTime": 'coachingSendTime',
                },
                UpdateExpression: "remove #timeZoneSetAutomatically, #coachingSendTimeUtcOffset, #coachingSendTime, #coachingSendTimeLocal",
                ReturnValues: "UPDATED_NEW"
            })
            counter++;
        } catch (error) {
            console.log("[updateDynamoRecord] Error in function updateDynamoRecord", error);
            console.log("[updateDynamoRecord] Tenant", { tenant });
        }
    }
    console.log({ totalDeletedTenantsCoachingTimes: counter });
}

async function deletePrevCoachingSendTimeVariables(tenants) {
    console.log('Deleting Previous Coaching Send Time Variables');
    let counter = 0
    for (const tenant of tenants) {
        try {
            await updateDynamoRecord({
                TableName: process.env.TENANT_TABLE,
                Key: {"id": tenant.id},
                ExpressionAttributeNames: {
                    "#timeZoneSetAutomatically": 'timeZoneSetAutomatically',
                    "#coachingSendTimeUtcOffset": 'coachingSendTimeUtcOffset',
                    '#coachingSendTimeLocal': 'coachingSendTimeLocal',
                    "#coachingSendTime": 'coachingSendTime',
                },
                UpdateExpression: "remove #timeZoneSetAutomatically, #coachingSendTimeUtcOffset, #coachingSendTime, #coachingSendTimeLocal, originationNumber",
                ReturnValues: "UPDATED_NEW"
            })
            counter++;
        } catch (error) {
            console.log("[updateDynamoRecord] Error in function updateDynamoRecord", error);
            console.log("[updateDynamoRecord] Tenant", { tenant });
        }
    }
    console.log({ totalDeletedTenantsCoachingTimes: counter });
}

module.exports = {
  runHera1372,
};