const {scanDynamoRecords, buildUpdateParams, updateDynamoRecord, createDynamoRecord} = require("./dynamodbHelper");

async function runHera1244 () {
    //  Set Custom List
    await setVehicleTypeCustomListToTenant()
    //  Set Options Custom List
    await setOptionsToVehicleTypeCustomList()
}

async function setOptionsToVehicleTypeCustomList() {
    try {
        let customLists = await getAllVehicleTypeCustomList()
        for (const customList of customLists) {
            console.log({ customList });
            for (const option of optionsCustomList) {
                let createOptionCustomList = {
                    TableName: process.env.OPTIONS_CUSTOM_LIST_TABLE,
                    Item: {
                        id: generateUUID(),
                        order: option.order,
                        option: option.option,
                        default: option.default,
                        usedFor: option.usedFor,
                        daysCount: option.daysCount,
                        canBeEdited: option.canBeEdited,
                        canBeDeleted: option.canBeDeleted,
                        canBeReorder: option.canBeReorder,
                        group: customList.group,
                        createdAt: new Date().toISOString(),
                        updatedAt: new Date().toISOString(),
                        optionsCustomListsCustomListsId: customList.id
                    }
                };
                await createDynamoRecord(createOptionCustomList);
            }
        }
        return true;
    } catch (error) {
        console.log("Error: ", error);
    }
}

async function getAllVehicleTypeCustomList() {
    try {
        let customLists = []
        let lastEvaluatedKey = null
        do {
            let params = {
                ExpressionAttributeNames: {
                    '#group': 'group',
                    '#type': 'type',
                },
                ExpressionAttributeValues: {
                    ':type': 'vehicle-type'
                },
                FilterExpression: "#type = :type",
                TableName: process.env.CUSTOM_LIST_TABLE,
                ProjectionExpression: 'id, #group, #type',
                ExclusiveStartKey: lastEvaluatedKey
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items
            lastEvaluatedKey = scanResult.LastEvaluatedKey
            customLists = [...customLists, ...items]
        } while (lastEvaluatedKey)
        return customLists
    } catch (err) {
        console.log("[getAllVehicleTypeCustomList] Error in function getAllVehicleTypeCustomList", err);
        return {
            error: err,
        };
    }
}

async function setVehicleTypeCustomListToTenant() {
    try {
        let tenants = await getAllTenants()
        for (const tenant of tenants) {
            console.log({ tenant });
            let createCustomList = {
                TableName: process.env.CUSTOM_LIST_TABLE,
                Item: {
                    id: generateUUID(),
                    type: 'vehicle-type',
                    listCategory: 'Daily Rostering',
                    listName: 'Vehicle Type',
                    listDisplay: 'Text Only',
                    group: tenant.group,
                    canDeleteAllOptions: true,
                    createdAt: new Date().toISOString(),
                    updatedAt: new Date().toISOString()
                }
            };
            await createDynamoRecord(createCustomList);
        }
        return true;
    } catch (error) {
        console.log("Error: ", error);
    }
}

async function getAllTenants() {
    try {
       let tenants = []
        let lastEvaluatedKey = null
        do {
            let params = {
                ExpressionAttributeNames: {
                    '#group': 'group',
                },
                TableName: process.env.TENANT_TABLE,
                ProjectionExpression: 'id, #group',
                ExclusiveStartKey: lastEvaluatedKey
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items
            lastEvaluatedKey = scanResult.LastEvaluatedKey
            tenants = [...tenants, ...items]
        } while (lastEvaluatedKey)
        return tenants
    } catch (err) {
        console.log("[getAllTenants] Error in function getAllTenants", err);
        return {
            error: err,
        };
    }
}

function generateUUID() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

const optionsCustomList = [
    {
        order: 1,
        option: '10,000lbs Van',
        default: false,
        usedFor: 0,
        daysCount: 0,
        canBeEdited: true,
        canBeDeleted: true,
        canBeReorder: true
    },
    {
        order: 2,
        option: 'Custom Delivery Van',
        default: false,
        usedFor: 0,
        daysCount: 0,
        canBeEdited: true,
        canBeDeleted: true,
        canBeReorder: true
    },
    {
        order: 3,
        option: 'Standard Parcel Small',
        default: false,
        usedFor: 0,
        daysCount: 0,
        canBeEdited: true,
        canBeDeleted: true,
        canBeReorder: true
    },
    {
        order: 4,
        option: 'Standard Parcel',
        default: false,
        usedFor: 0,
        daysCount: 0,
        canBeEdited: true,
        canBeDeleted: true,
        canBeReorder: true
    },
    {
        order: 5,
        option: 'Standard Parcel Large',
        default: false,
        usedFor: 0,
        daysCount: 0,
        canBeEdited: true,
        canBeDeleted: true,
        canBeReorder: true
    },
    {
        order: 6,
        option: 'Standard Parcel XL',
        default: false,
        usedFor: 0,
        daysCount: 0,
        canBeEdited: true,
        canBeDeleted: true,
        canBeReorder: true
    },
]

module.exports = {
    runHera1244
}