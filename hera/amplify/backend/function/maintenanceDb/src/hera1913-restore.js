const { buildUpdateParams, scanDynamoRecords, updateDynamoRecord } = require("./dynamodbHelper.js");

async function runHera1913Restore() {
    var vehicles = await getAllVehiclesFromRestoreDB();
    // console.log('Total:',vehicles.length)
    // console.log(vehicles)
    await setDataBackUp(vehicles)
}

async function getAllVehiclesFromRestoreDB() {
    try {
        let vehicles = [];
        let lastEvaluatedKey = null;
        do {
            let params = {
                ExpressionAttributeNames: {
                    "#g": "group",
                    "#s": "status",
                },
                
                TableName: 'Vehicle-zeobggbnyva4padyiddojnmnqy-production-01052023-restored',
                ProjectionExpression: "id, vin, #g, #s, licensePlate, licensePlateExp",
                ExclusiveStartKey: lastEvaluatedKey,
                FilterExpression: "attribute_exists(vin) and vin = :empty",
                ExpressionAttributeValues: {
                    ":empty": ""
                }
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            vehicles = [...vehicles, ...items];
        } while (lastEvaluatedKey);

        return vehicles;
    } catch (err) {
        console.log("[getAllVehicles] Error in function getAllVehicles", err);
        return {
            error: err,
        };
    }
}

async function setDataBackUp(list) {
    for(const vehicle of list) {
        const {
            ExpressionAttributeNames,
            ExpressionAttributeValues,
            UpdateExpression
        } = getUpdateExpression(vehicle)

        try {
            await updateDynamoRecord({
                TableName: process.env.VEHICLE_TABLE,
                Key: {"id": vehicle.id},
                ExpressionAttributeValues,
                ExpressionAttributeNames,
                UpdateExpression,
                ReturnValues: "UPDATED_NEW"
            })
            console.log("Updated Vehicle in PROD id:", vehicle.id)
            // return
        } catch (err) {
            console.log(`[hera-1913-restore setDataBackUp] Error Updating Vehicle status with id: ${vehicle.id} `, err);
            return {
                error: err,
            };
        }
    }
}

function getUpdateExpression(vehicle){

    let UpdateExpression = ''
    let ExpressionAttributeNames = {}
    let ExpressionAttributeValues = {}
    const setArr = [];
    const GSIs = ['licensePlate','licensePlateExp', 'status'];
    GSIs.forEach(field => {
        if (vehicle[field] && vehicle[field] != '') {
            const key = `#${field}`;
            const value = `:${field}`;
            ExpressionAttributeNames[key] = field;
            ExpressionAttributeValues[value] = vehicle[field]
            const data = `${key} = ${value}`
            setArr.push(data);
        }
    });
    
    if (setArr.length > 0) {
        const setExpr = `set ${setArr.join(', ')}`;
        UpdateExpression += setExpr;
    }
    
    return {
        ExpressionAttributeNames,
        ExpressionAttributeValues,
        UpdateExpression
    }
}

module.exports = {
    runHera1913Restore,
};