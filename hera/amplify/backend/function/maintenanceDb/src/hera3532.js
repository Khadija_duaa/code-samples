/**
 * This script removes the DEFAULT options set for the following types
 *  issue-type
 *  kudo-type
 *  parking-space
 */

const {
  queryDynamoRecords,
  scanDynamoRecords,
  updateDynamoRecord,
} = require("./dynamodbHelper");

const FORBIDDEN_CUSTOM_LISTS_TO_SET_DEFAULT_SELECTIONS_TO = [
  { listName: 'Associate Issue Type', type: 'issue-type' },
  { listName: 'Associate Kudo Type', type: 'kudo-type' },
  { listName: 'Parking Space', type: 'parking-space' },
];

const VALUES_TO_REMOVE_FROM_IS_DEFAULT_FOR_SECTIONS = {
  'ISSUE_TYPE_CREATE_ISSUE': true,
  'KUDO_TYPE_CREATE_KUDO': true,
  'PARKING_SPACE_CREATE_VEHICLE': true,
};

/**
 * Main function
 */
async function handleHera3532() {
  try {
    // Print Report
    await handlePrintReport();
    // Update
    // await handleUpdateRecords();
  } catch (error) {
    console.log(error);
  }
}

/**
 * Handles printing the report of the changes that this script will cause
 */
async function handlePrintReport() {
  const reportObj = {
    count: 0,
    items: [],
  };
  await loadCustomListsAndApplyCallbakcToAllOptionsCustomLists((optionsCustomListArray) => {
    for (const optionCustomList of optionsCustomListArray) {
      const isDefaultForSections = getNewValueForIsDefaultForSections(optionCustomList);
      reportObj.count += 1;
      reportObj.items.push({
        ...optionCustomList,
        toSet_isDefaultForSections: isDefaultForSections,
      });
    }
  });
  console.log('Report', JSON.stringify(reportObj, null, 2));
}

/**
 * Handles the update of OptionCustomList
 */
async function handleUpdateRecords() {
  const MAX_COUNT_PER_BATCH = 20;
  const sendUpdates = async (optionsCustomListsArray) => {
    const promises = optionsCustomListsArray.map(async (optionCustomList) => {
      const isDefaultForSections = getNewValueForIsDefaultForSections(optionCustomList);
      return await updateOptionCustomList(optionCustomList, isDefaultForSections);
    });
    await Promise.all(promises);
  };
  await loadCustomListsAndApplyCallbakcToAllOptionsCustomLists(async (optionsCustomListsArray) => {
    let batchUpdateList = [];
    for (const item of optionsCustomListsArray) {
      batchUpdateList.push(item);
      if (batchUpdateList.length === MAX_COUNT_PER_BATCH) {
        await sendUpdates(batchUpdateList);
        batchUpdateList = [];
      }
    }
    if (batchUpdateList.length) {
      await sendUpdates(batchUpdateList);
    }
  });
}

/**
 * Calculates the new value for 'isDefaultForSections' by removing the tagetted section values
 * @param {Object} optionCustomList - OptionCustomList to calculate the new isDefaultForSections value from
 * @returns {Array} New value for 'isDefaultForSections'
 */
function getNewValueForIsDefaultForSections(optionCustomList) {
  return optionCustomList
    .isDefaultForSections
    ?.filter(section => !VALUES_TO_REMOVE_FROM_IS_DEFAULT_FOR_SECTIONS[section])
    || [];
}

/**
 * Update an Option CustomList record setting the new value for 'isDefaultForSections'
 * @param {Object} optionCustomList - OptionCustomList record to be updated
 * @param {List} isDefaultForSections - New value for 'isDefaultForSections' field
 */
async function updateOptionCustomList(optionCustomList, isDefaultForSections) {
  try {
    const params = {
      TableName: process.env.OPTIONS_CUSTOM_LIST_TABLE,
      Key: { id: optionCustomList.id },
      ExpressionAttributeValues: {
        ":isDefaultForSections": isDefaultForSections,
      },
      ExpressionAttributeNames: {
        '#isDefaultForSections': 'isDefaultForSections',
      },
      UpdateExpression: "set #isDefaultForSections = :isDefaultForSections",
      ReturnValues: "UPDATED_NEW",
    };
    await updateDynamoRecord(params);
  }
  catch (error) {
    console.log(`Error updating OptionCustomList [ID=${optionCustomList.id}]`);
  }
}

/**
 * Loads every CustomList that is target as FORBIDDEN CUSTOM LISTS TO SET DEFAULT SELECTIONS TO
 * @param {Function} cb - Callback to apply on a list of OptionsCustomList
 */
async function loadCustomListsAndApplyCallbakcToAllOptionsCustomLists(cb) {
  try {
    for (const customList of FORBIDDEN_CUSTOM_LISTS_TO_SET_DEFAULT_SELECTIONS_TO) {
      let lastEvaluatedKey = null;
      do {
        const params = {
          ExpressionAttributeNames: {
            '#id': 'id',
            '#group': 'group',
            '#listName': 'listName',
            '#type': 'type',
          },
          ExpressionAttributeValues: {
            ':listName': customList.listName,
            ':type': customList.type,
          },
          TableName: process.env.CUSTOM_LIST_TABLE,
          ProjectionExpression: '#id, #group',
          ExclusiveStartKey: lastEvaluatedKey,
          FilterExpression: '#listName=:listName and #type=:type',
        };
        const scanResult = await scanDynamoRecords(params);
        await applyCallbackToAllOptionsCustomLists(scanResult.Items, cb);
        lastEvaluatedKey = scanResult.LastEvaluatedKey;
        console.log('CustomLists', {lastEvaluatedKey});
      }
      while (lastEvaluatedKey);
    }
  }
  catch (error) {
    console.log("[loadCustomListsAndApplyCallbakcToAllOptionsCustomLists] Error", error);
  }
}

/**
 * Fetches all 'Options Custom Lists' for the passed 'CustomLists' that have 'isDefaultForSections' attribute set
 * @param {Array} customLists - List of CustomLists records
 * @param {Function} cb - Callback to apply on a list of the OptionCustomLists fetched from the query
 */
async function applyCallbackToAllOptionsCustomLists(customLists, cb) {
  const containsExpression = Object.keys(VALUES_TO_REMOVE_FROM_IS_DEFAULT_FOR_SECTIONS)
    .map((section) => `contains(#isDefaultForSections, :${section})`)
    .join(' or ');
  const sectionAttributeValues = Object.keys(VALUES_TO_REMOVE_FROM_IS_DEFAULT_FOR_SECTIONS)
    .reduce((acc, section) => (acc[`:${section}`]=section, acc), {});
  try {
    for (const customList of customLists) {
      let lastEvaluatedKey = null;
      do {
        const params = {
          IndexName: 'gsi-OptionsCustomLists',
          KeyConditionExpression: '#optionsCustomListsCustomListsId = :optionsCustomListsCustomListsId',
          ExpressionAttributeNames: {
            '#id': 'id',
            '#group': 'group',
            '#isDefaultForSections': 'isDefaultForSections',
            '#optionsCustomListsCustomListsId': 'optionsCustomListsCustomListsId',
          },
          ExpressionAttributeValues: {
            ':optionsCustomListsCustomListsId': customList.id,
            ...sectionAttributeValues
          },
          TableName: process.env.OPTIONS_CUSTOM_LIST_TABLE,
          ProjectionExpression: '#id, #group, #isDefaultForSections',
          FilterExpression: `attribute_exists(#isDefaultForSections) and (${containsExpression})`,
          ExclusiveStartKey: lastEvaluatedKey,
        };
        const queryResult = await queryDynamoRecords(params);
        await cb(queryResult.Items);
        lastEvaluatedKey = queryResult.LastEvaluatedKey;
      }
      while (lastEvaluatedKey);
    }
  }
  catch (error) {
    console.log("[applyCallbackToAllOptionsCustomLists] Error", error);
  }
}

module.exports = {
  handleHera3532,
};
