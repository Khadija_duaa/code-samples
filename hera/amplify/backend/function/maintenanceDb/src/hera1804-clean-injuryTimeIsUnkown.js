const {
  scanDynamoRecords,
  updateDynamoRecord,
} = require("./dynamodbHelper.js");
async function handleHera1804CleanInjuryTimeIsUnkown() {
  try {
    let lastEvaluatedKey = null;
    do {
      let params = {
        TableName: process.env.INJURY_TABLE,
        ProjectionExpression: "id, injuryTimeIsUnkown",
        ExclusiveStartKey: lastEvaluatedKey,
        FilterExpression: "attribute_exists(injuryTimeIsUnkown)",
      };
      let scanResult = await scanDynamoRecords(params);
      lastEvaluatedKey = scanResult.LastEvaluatedKey;
      console.log('lastEvaluatedKey =>', lastEvaluatedKey);
      let injuries = scanResult.Items;
      //**********Uncomment the following line to remove the "injuryTimeIsUnkown" field*******
      //await removeInjuryTimeIsUnkown(injuries);
    } while (lastEvaluatedKey);
  } catch (err) {
    console.log("[handleHera1804CleanInjuryTimeIsUnkown] Error in function handleHera1804CleanInjuryTimeIsUnkown", err);
    return {
      error: err,
    };
  }
}
async function removeInjuryTimeIsUnkown(list) {
  for (const item of list) {
    try {
      await updateDynamoRecord({
        TableName: process.env.INJURY_TABLE,
        Key: { "id": item.id },
        UpdateExpression: "remove #injuryTimeIsUnkown",
        ExpressionAttributeNames: { "#injuryTimeIsUnkown": "injuryTimeIsUnkown" },
        ReturnValues: "UPDATED_NEW"
      });
    } catch (err) {
      console.log("[hera-1804] Error updating injuryTimeIsUnkown", err);
      return {
        error: err,
      };
    }
  }
}
module.exports.handleHera1804CleanInjuryTimeIsUnkown = handleHera1804CleanInjuryTimeIsUnkown