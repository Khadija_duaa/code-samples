const { buildUpdateParams, scanDynamoRecords, updateDynamoRecord } = require("./dynamodbHelper.js");

async function runHera1120() {
    var staffs = await getAllStaffs();
    
    //Sort the list by group
    var sortedlist = staffs.sort(function (a, b) {
        return (a.group < b.group) ? -1 : 1 ;
    });
    
    //Group Staff by same group
    var groupedbyGroup = await subgroupArraysByGroup(staffs);
    
    var groupFiltered = [];
    //Get only duplicated emails by group
    for(var group of groupedbyGroup) {
         const duplicatedList = group.map(v => v.phone).filter((v, i, vIds) => vIds.indexOf(v) !== i);
         const duplicates = group.filter(obj => duplicatedList.includes(obj.phone));
         
         var sortedlistb = duplicates.sort(function (a, b) {
            return (a.phone < b.phone) ? -1 : 1 ;
         });
         if(sortedlistb.length>0){
            groupFiltered.push(sortedlistb);
         }
    }
    
    //Group staff with same phone in subarrays
    var groupedbyPhone = await subgroupArraysByEmail(groupFiltered);
    
    //cleanEmails
    var phonestoClean = await cleanEmails(groupedbyPhone);
    
    console.log(JSON.stringify(phonestoClean));
    return true;
}

async function getAllStaffs() {
    try {
        let staffs = [];
        let lastEvaluatedKey = null;
        do {
            let params = {
                ExpressionAttributeNames: {
                    "#status": "status",
                    "#group": "group",
                },
                
                TableName: process.env.STAFF_TABLE,
                ProjectionExpression: "id, phone, #status, #group, transporterId",
                ExclusiveStartKey: lastEvaluatedKey,
                FilterExpression: "attribute_exists(phone) and phone<> :empty",
                ExpressionAttributeValues: {
                    ":empty": "",
                }
                
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            staffs = [...staffs, ...items];
        } while (lastEvaluatedKey);

        return staffs;
    } catch (err) {
        console.log("[getAllStaffs] Error in function getAllStaffs", err);
        return {
            error: err,
        };
    }
}


async function subgroupArraysByGroup(list){
    var main = []
    var subarray = []
   
    var initialValue = list[0]['group'];
        for(var i=0; i<list.length;i++) {
  	        if(list[i].group == initialValue) {
                subarray.push(list[i])
            } else {
     			initialValue = list[i].group;
                main.push(subarray);
                subarray = [];
                subarray.push(list[i]);
            }
     
             if(i==list.length-1){
     	        main.push(subarray);
            }
        }     

    return main;
}

async function subgroupArraysByEmail(list){
    
    var main = [];
    
    for (const grouped of list) {
        
        var submain = [];  
        var subarray = [];
  
        var initialValue = grouped[0]['email'];
       
        for(var i=0; i<grouped.length;i++) {
  	        if(grouped[i].email == initialValue) {
                subarray.push(grouped[i])
            } else {
     			initialValue = grouped[i].email;
                submain.push(subarray);
                subarray = [];
                subarray.push(grouped[i]);
            }
     
             if(i==grouped.length-1) {
     	        submain.push(subarray);
            }
        } 
        main.push(submain);
    }
    
    return main;
}

async function cleanEmails(list){
    
    var emailstoClean = [];
 
    for (const grouped of list) {
        
        var subgroup = [];
        
        for(const staff of grouped){
            //sort the subarray by status
            var objsorted = staff.sort(function (a, b) {
                return (a.status < b.status) ? -1 : 1 ;
            });
        
            var pos = await getActive(staff);
            if(pos==-1){
                pos = objsorted.map(function(e) { return e.status; }).indexOf('Active');
            }
            if(pos==-1){
                pos = objsorted.map(function(e) { return e.status; }).indexOf('Inactive - Terminated');
            }
            
            if(pos!=-1) {
                objsorted.splice(pos, 1);
            } else {
                objsorted.splice(0, 1);
            }
            
            if(objsorted.length>0){
                subgroup.push(objsorted);
            }
            
            //await updateEmailDynamo(objsorted);
        }
        emailstoClean.push(subgroup);
    }
    
    return emailstoClean;
}

async function updateEmailDynamo(list){
    try {
        for (const staff of list) {
        
        await updateDynamoRecord({
            TableName: process.env.STAFF_TABLE,
            Key: {"id": staff.id},
            ExpressionAttributeValues: {":email": staff.id + '@changethisemail.com'},
            ExpressionAttributeNames: {"#email": 'email'},
            UpdateExpression: "set #email = :email",
            ReturnValues: "UPDATED_NEW"
        })
            
        }
        
        
    } catch (err) {
        console.log("[hera-1120] Error updating phones", err);
        return {
            error: err,
        };
    }
}

async function getActive(staff){
  	var pos = -1;
    for(var i=0; i<staff.length;i++) {
    	if(staff[i].status == 'Active' && staff[i].transporterId && staff[i].transporterId!=''){
      	pos = i;
        break;
      }
    }
    return pos;
}

module.exports = {
    runHera1120,
};