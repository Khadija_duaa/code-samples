const { buildUpdateParams, scanDynamoRecords, updateDynamoRecord } = require("./dynamodbHelper.js");

async function runHera1182() {
    var vehicles = await getAllVehicles();

    //Sort the list by group
    var sortedlist = vehicles.sort(function (a, b) {
        return (a.group < b.group) ? -1 : 1 ;
    });
    
    //Group Vehicle by same group
    var groupedbyGroup = await subgroupArraysByGroup(sortedlist);
    
    var groupFiltered = [];
    //Get only duplicated emails by group
    for(var group of groupedbyGroup) {
         const duplicatedList = group.map(v => v.vin).filter((v, i, vIds) => vIds.indexOf(v) !== i);
         const duplicates = group.filter(obj => duplicatedList.includes(obj.vin));
         
         var sortedlistb = duplicates.sort(function (a, b) {
            return (a.vin < b.vin) ? -1 : 1 ;
         });
         if(sortedlistb.length>0){
            groupFiltered.push(sortedlistb);
         }
    }
    
    //Group vehicle with same phone in subarrays
    var groupedbyVin = await subgroupArraysByVin(groupFiltered);
    
    //cleanEmails
    var vehiclesToClean = await cleanVINs(groupedbyVin);
    console.log(JSON.stringify(vehiclesToClean));
}

async function getAllVehicles() {
    try {
        let vehicles = [];
        let lastEvaluatedKey = null;
        do {
            let params = {
                ExpressionAttributeNames: {
                    "#status": "status",
                    "#group": "group",
                },
                TableName: process.env.VEHICLE_TABLE,
                ProjectionExpression: "id, #status, #group, vin",
                ExclusiveStartKey: lastEvaluatedKey,
                FilterExpression: "attribute_exists(vin) and vin<> :empty and vin<> :null",
                ExpressionAttributeValues: {
                    ':empty': "",
                    ':null': null
                }
                
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            vehicles = [...vehicles, ...items];
        } while (lastEvaluatedKey);
        return vehicles;
    } catch (err) {
        console.log("[getAllVehicles] Error in function getAllVehicles", err);
        return {
            error: err,
        };
    }
}

async function subgroupArraysByGroup(list){
    var main = []
    var subarray = []
   
    var initialValue = list[0]['group'];
        for(var i=0; i<list.length;i++) {
  	        if(list[i].group == initialValue) {
                subarray.push(list[i])
            } else {
     			initialValue = list[i].group;
                main.push(subarray);
                subarray = [];
                subarray.push(list[i]);
            }
     
             if(i==list.length-1){
     	        main.push(subarray);
            }
        }     

    return main;
}

async function subgroupArraysByVin(list){
    
    var main = [];
    
    for (const grouped of list) {
        
        var submain = [];  
        var subarray = [];
  
        var initialValue = grouped[0]['vin'];
       
        for(var i=0; i<grouped.length;i++) {
  	        if(grouped[i].vin == initialValue) {
                subarray.push(grouped[i])
            } else {
     			initialValue = grouped[i].vin;
                submain.push(subarray);
                subarray = [];
                subarray.push(grouped[i]);
            }
     
             if(i==grouped.length-1) {
     	        submain.push(subarray);
            }
        } 
        main.push(submain);
    }
    
    return main;
}

async function cleanVINs(list){
    
    var vehiclesToClean = [];
 
    for (const grouped of list) {
        
        var subgroup = [];
        
        for(const vehicle of grouped) {
            //sort the subarray by status
            var objsorted = vehicle.sort(function (a, b) {
                return (a.status < b.status) ? -1 : 1 ;
            });
        
            var pos = objsorted.map(function(e) { return e.status; }).indexOf('Active');
            
            if(pos==-1){
                pos = objsorted.map(function(e) { return e.status; }).indexOf('Inactive - Maintenance');
            }
            if(pos==-1){
                pos = objsorted.map(function(e) { return e.status; }).indexOf('Inactive - Grounded');
            }
            
            if(pos!=-1) {
                objsorted.splice(pos, 1);
            } else {
                objsorted.splice(0, 1);
            }
            
            if(objsorted.length>0){
                subgroup.push(objsorted);
            }
            //await updateVinDynamo(objsorted);
        }
        vehiclesToClean.push(subgroup);
    }
    
    return vehiclesToClean;
}

async function updateVinDynamo(list){
    try {
        for (const vehicle of list) {
        
        await updateDynamoRecord({
            TableName: process.env.VEHICLE_TABLE,
            Key: {"id": vehicle.id},
            UpdateExpression: 'remove #vin',
            ExpressionAttributeNames: { '#vin': 'vin' },
            ReturnValues: "UPDATED_NEW"
        })
        }
        
        
    } catch (err) {
        console.log("[hera-1182] Error updating vehicles", err);
        return {
            error: err,
        };
    }
}

module.exports = {
    runHera1182,
};