const {
    scanDynamoRecords,
    updateDynamoRecord,
    queryDynamoRecords,
    createDynamoRecord
} = require('./dynamodbHelper.js')

function needUpdate(tenant, defaultSettings, keys) {
    return keys.some(key => defaultSettings[key] !== tenant[key])
}

async function iterateTenants(tenants, defaultSettings, keys) {
    for (const tenant of tenants) {
        if (needUpdate(tenant, defaultSettings, keys)) {
            const updateParams = {
                TableName: process.env.TENANT_TABLE,
                Key: { id: tenant.id },
                ExpressionAttributeNames: {
                    "#a": "coachingDsbThresholdIssue",
                    "#b": "coachingDsbThreshold",
                    "#c": "coachingDsbThresholdPR",
                    "#d": "coachingDsbThresholdKudo",
                    "#e": "coachingDsbIssue",
                    "#f": "coachingDsbCO",
                    "#g": "coachingDsbPR",
                    "#h": "coachingDsbKudo",
                },
                ExpressionAttributeValues: {
                    ":a": defaultSettings.coachingDsbThresholdIssue,
                    ":b": defaultSettings.coachingDsbThreshold,
                    ":c": defaultSettings.coachingDsbThresholdPR,
                    ":d": defaultSettings.coachingDsbThresholdKudo,
                    ":e": defaultSettings.coachingDsbIssue,
                    ":f": defaultSettings.coachingDsbCO,
                    ":g": defaultSettings.coachingDsbPR,
                    ":h": defaultSettings.coachingDsbKudo
                },
                UpdateExpression: "set #a=:a,#b=:b,#c=:c,#d=:d,#e=:e,#f=:f,#g=:g,#h=:h",
                ReturnValues: "UPDATED_NEW",
            }
            const response = await updateDynamoRecord(updateParams)
            console.log({ group: tenant.group, response })

            let arrayList = [
                'infraction-type',
                'kudo-type'
            ]

            for (const key of arrayList) {
                let valueListId = await getValueListIdByKey(tenant.group, key)
                let driverValue = key === arrayList[0] ? 60 : 10

                if (valueListId) {
                    let item = {
                        id: generateUUID(),
                        createdAt: new Date().toISOString(),
                        custom: false,
                        driverReportSetting: driverValue,
                        group: tenant.group,
                        hidden: true,
                        updatedAt: new Date().toISOString(),
                        value: 'Scorecard DSB',
                        valueListItemValueListId: valueListId,
                        __typename: 'ValueListItem'
                    }

                    console.log({ item });

                    await populateValueListItem(item)
                } else {
                    console.log(tenant.group, key, ' --skip due to missing ValueList');
                }
            }
        } else {
            console.log(tenant.group + ' --skip, tenant already has the fields.')
        }
    }
}

async function populateValueListItem(item) {
    let createValueListItem = {
        TableName: process.env.VALUELIST_ITEM_TABLE,
        Item: { ...item }
    };
    await createDynamoRecord(createValueListItem);
}

async function handleAllTenants() {
    const defaultSettings = {
        "coachingDsbThresholdIssue": "584",
        "coachingDsbThreshold": "259",
        "coachingDsbThresholdPR": "258",
        "coachingDsbThresholdKudo": "100",
        "coachingDsbIssue": true,
        "coachingDsbCO": true,
        "coachingDsbPR": true,
        "coachingDsbKudo": true
    }
    const keys = Object.keys(defaultSettings)
    try {
        const scanParams = {
            ExpressionAttributeNames: {
                "#a": "coachingDsbThresholdIssue",
                "#b": "coachingDsbThreshold",
                "#c": "coachingDsbThresholdPR",
                "#d": "coachingDsbThresholdKudo",
                "#e": "coachingDsbIssue",
                "#f": "coachingDsbCO",
                "#g": "coachingDsbPR",
                "#h": "coachingDsbKudo",
                "#0": "group"
            },
            TableName: process.env.TENANT_TABLE,
            ProjectionExpression: "id,#a,#b,#c,#d,#e,#f,#g,#h,#0",
        }
        do {
            const scanResult = await scanDynamoRecords(scanParams)
            console.log('LastEvaluatedKey', scanResult.LastEvaluatedKey);
            await iterateTenants(scanResult.Items, defaultSettings, keys)
            scanParams.ExclusiveStartKey = scanResult.LastEvaluatedKey
        } while (scanParams.ExclusiveStartKey)
    } catch (err) {
        console.log('[handleAllTenants] Error in function', err)
        return {
            error: err
        }
    }
}

async function getValueListIdByKey(group, key) {
    try {
        const queryParams = {
            ExpressionAttributeNames: {
                '#g': 'group',
                '#k': 'key'
            },
            ExpressionAttributeValues: {
                ':g': group,
                ':k': key
            },
            TableName: process.env.VALUELIST_TABLE,
            IndexName: 'byGroup',
            KeyConditionExpression: '#g = :g AND #k = :k',
            ProjectionExpression: 'id'
        }
        const queryResult = await queryDynamoRecords(queryParams)

        return queryResult.Items[0]?.id
    } catch (err) {
        console.log('[getValueListIdByKey] Params:', group, key)
    }
}

function generateUUID() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

async function handleHera3210() {
    await handleAllTenants()
}

module.exports = {
    handleHera3210
}