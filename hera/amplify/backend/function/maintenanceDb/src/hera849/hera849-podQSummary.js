const {
    scanDynamoRecords,
    updateDynamoRecord,
} = require("../dynamodbHelper.js");

async function handlerHera849PodQSummary(){
    const data = await getPodQualitySummary();
    for(let pod of data){
        await updatePodQualitySummary(pod);
    }
}

async function getPodQualitySummary(){
    try{
        let lastEvaluatedKey = null;
        let data = [];
        do{
             let params = {
                TableName: process.env.POD_QUALITY_SUMMARY_TABLE,
                ExpressionAttributeNames: {
                    '#w': 'week',
                    '#y': 'year'
                },
                ProjectionExpression: "id, #w, #y",
                ExclusiveStartKey: lastEvaluatedKey,
            };
            const result = await scanDynamoRecords(params);
            lastEvaluatedKey = result.LastEvaluatedKey;
            console.log('lastEvaluatedKey =>', lastEvaluatedKey);
            data = data.concat(result.Items);
        }while(lastEvaluatedKey);
        return data;
    }catch(err){
        console.log("[hera849-getPodQualitySummary] Error in function getPodQualitySummary", err);
    }
    
}


async function updatePodQualitySummary(pod){

    try {
        const updatedAt =  new Date().toISOString();
        let updatedParams = {
            TableName : process.env.POD_QUALITY_SUMMARY_TABLE,
            Key: {
                "id": pod.id
            },
            ExpressionAttributeNames: {
                '#updatedAt': 'updatedAt',
                '#yw': 'yearWeek'
            },
            ExpressionAttributeValues: {
                ':updatedAt': updatedAt,
                ':yw': `${pod.year}${pod.week}`
            },
            UpdateExpression: 'set #updatedAt = :updatedAt, #yw = :yw'
        };
        await updateDynamoRecord(updatedParams);
    } catch (err) {
        console.log("[hera849-updatePodQualitySummary] Error updating updatePodQualitySummary", err);
    }
}

module.exports = { handlerHera849PodQSummary };