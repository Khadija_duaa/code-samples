const {
    scanDynamoRecords,
    updateDynamoRecord,
} = require("../dynamodbHelper.js");

async function handlerHera849CxSummary(){
    const data = await getCxFeedbackSummary();
    for(let cx of data){
        await updateCxFeedbackSummary(cx);
    }
}

async function getCxFeedbackSummary(){
    try{
        let lastEvaluatedKey = null;
        let data = [];
        do{
             let params = {
                TableName: process.env.CX_FEEDBACK_SUMMARY_TABLE,
                ExpressionAttributeNames: {
                    '#w': 'week',
                    '#y': 'year'
                },
                ProjectionExpression: "id, #w, #y",
                ExclusiveStartKey: lastEvaluatedKey,
            };
            const result = await scanDynamoRecords(params);
            lastEvaluatedKey = result.LastEvaluatedKey;
            console.log('lastEvaluatedKey =>', lastEvaluatedKey);
            data = data.concat(result.Items);
        }while(lastEvaluatedKey);
        return data;
    }catch(err){
        console.log("[hera849-getCxFeedbackSummary] Error in function getCxFeedbackSummary", err);
    }
    
}


async function updateCxFeedbackSummary(cx){

    try {
        const updatedAt =  new Date().toISOString();
        let updatedParams = {
            TableName : process.env.CX_FEEDBACK_SUMMARY_TABLE,
            Key: {
                "id": cx.id
            },
            ExpressionAttributeNames: {
                '#updatedAt': 'updatedAt',
                '#yw': 'yearWeek'
            },
            ExpressionAttributeValues: {
                ':updatedAt': updatedAt,
                ':yw': `${cx.year}${cx.week}`
            },
            UpdateExpression: 'set #updatedAt = :updatedAt, #yw = :yw'
        };
        await updateDynamoRecord(updatedParams);
    } catch (err) {
        console.log("[hera849-updateCxFeedbackSummary] Error updating updateCxFeedbackSummary", err);
    }
}

module.exports = { handlerHera849CxSummary };