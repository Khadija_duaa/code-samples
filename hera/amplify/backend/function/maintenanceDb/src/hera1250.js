const {
    scanDynamoRecords,
    updateDynamoRecord,
} = require("./dynamodbHelper.js");

async function handlerHera1250(){
    try {
        let lastEvaluatedKey = null;
        do{
            let params = {
                TableName: process.env.EOC_SCORE_TABLE,
                ExpressionAttributeNames: {
                    '#l': 'level',
                    '#d': 'date'
                },
                ProjectionExpression: "id, #l, #d",
                ExclusiveStartKey: lastEvaluatedKey,
            };
            const result = await scanDynamoRecords(params);
            lastEvaluatedKey = result.LastEvaluatedKey;
            console.log('lastEvaluatedKey =>', lastEvaluatedKey);
            const eocDspLevel = result.Items

            //await updateDspLevel(eocDspLevel)

        }while(lastEvaluatedKey)
    } catch (err) {
        console.log("[handlerHera1250] Error in function handlerHera1250", err);
    }
}

async function updateDspLevel(eocDspLevel){

    for(let eoc of eocDspLevel){
        try {
            const updatedAt =  new Date().toISOString()
            let updatedParams = {
                TableName : process.env.EOC_SCORE_TABLE,
                Key: {
                    "id": eoc.id
                },
                ExpressionAttributeNames: {
                    '#l': 'level',
                    '#updatedAt': 'updatedAt',
                    '#levelAndDate': 'level#date'
                },
                ExpressionAttributeValues: {
                    ':l': eoc.level,
                    ':updatedAt': updatedAt,
                    ':levelAndDate':  eoc.level + '#' + eoc.date,
                },
                UpdateExpression: 'set #l = :l, #updatedAt = :updatedAt, #levelAndDate = :levelAndDate'
            };
            await updateDynamoRecord(updatedParams);
        } catch (err) {
            console.log("[hera-1250] Error updating updateDspLevel", err);
        }
    }
}

module.exports = { handlerHera1250 }