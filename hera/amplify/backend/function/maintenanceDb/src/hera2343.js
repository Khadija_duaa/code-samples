const {
  buildUpdateParams,
  scanDynamoRecords,
  updateDynamoRecord,
} = require("./dynamodbHelper.js");

async function runHera2343() {
  let validValues = [
    'INFORMAL_WARNING',
    'FORMAL_VERBAL_WARNING',
    'FINAL_WARNING',
    'DISMISSAL'
  ]

  let counselings = await getAllCounselings();

  for (const counseling of counselings) {
    try {
      if (!validValues.includes(counseling.warningType)) {
        let newWarningType = parseCounselingTypeLabelToValue(counseling.warningType)

        let updateParams = buildUpdateParams(
          process.env.COUNSELING_TABLE,
          { warningType: newWarningType },
          counseling.id
        );
        await updateDynamoRecord(updateParams);
      }
    } catch (err) {
      console.log("[runHera2343] Error in function runHera2343", err);
    }
  }
}

async function getAllCounselings() {
  try {
    let counselings = [];
    let lastEvaluatedKey = null;
    do {
      let params = {
        ExpressionAttributeNames: {
          "#group": "group",
        },
        TableName: process.env.COUNSELING_TABLE,
        FilterExpression: 'attribute_exists(warningType) and warningType <> :empty',
        ProjectionExpression: "id, #group, warningType",
        ExpressionAttributeValues:{
          ":empty": "",
        },
        ExclusiveStartKey: lastEvaluatedKey,
      };
      let scanResult = await scanDynamoRecords(params);
      let items = scanResult.Items;
      lastEvaluatedKey = scanResult.LastEvaluatedKey;
      counselings = [...counselings, ...items];
    } while (lastEvaluatedKey);

    return counselings;
  } catch (err) {
    console.log("[getAllCounselings] Error in function getAllCounselings", err);
    return {
      error: err,
    };
  }
}

function parseCounselingTypeLabelToValue(typeLabel) {
  const toUpper = word => word.toUpperCase();
  return typeLabel.split(' ').map(toUpper).join('_');
}

module.exports = {
  runHera2343,
};
