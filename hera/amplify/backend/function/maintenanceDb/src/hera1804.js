const {
  scanDynamoRecords,
  updateDynamoRecord,
} = require("./dynamodbHelper.js");

async function handleHera1804() {
  try {
    let lastEvaluatedKey = null;
    do {
      let params = {
        TableName: process.env.INJURY_TABLE,
        ProjectionExpression: "id, injuryTimeIsUnkown, injuryTime",
        ExclusiveStartKey: lastEvaluatedKey,
      };
      let scanResult = await scanDynamoRecords(params);
      lastEvaluatedKey = scanResult.LastEvaluatedKey;
      console.log('lastEvaluatedKey =>', lastEvaluatedKey);
      const injuries = scanResult.Items
      let list = searchInjuryTimeIsUnkown(injuries);
      let report = getReport(list);
      console.log(JSON.stringify(report));
      //**********Uncomment the following line to update the "injuryTimeIsUnknown" field*******
      // await updatesInjuryTimeIsUnknown(list);
    } while (lastEvaluatedKey);
  } catch (err) {
    console.log("[handleHera1804] Error in function handleHera1804", err);
    return {
      error: err,
    };
  }
}
function searchInjuryTimeIsUnkown(list) {
  return list.filter(value => value.injuryTimeIsUnkown == 'true' || value.injuryTimeIsUnkown == 'false').map(elem => {
    const { injuryTimeIsUnkown, injuryTime } = elem;
    let newInjuryTimeIsUnknown
    // cases when injuryTImeIsUnknown = ''
    if (!injuryTime && !injuryTimeIsUnkown) injuryTimeIsUnkown = true
    if (injuryTime && !injuryTimeIsUnkown) injuryTimeIsUnkown = false
    // cases when injuryTImeIsUnknown != ''
    newInjuryTimeIsUnknown = injuryTimeIsUnkown == "true" ? true : false
    return {
      ...elem,
      newInjuryTimeIsUnknown
    }
  })
}
function getReport(list) {
  return list.map(({ id, injuryTimeIsUnkown, newInjuryTimeIsUnknown }) => {
    return {
      id: id,
      injuryTimeIsUnkown: injuryTimeIsUnkown.toString().replace(/,/g, ", "),
      newInjuryTimeIsUnknown: newInjuryTimeIsUnknown.toString().replace(/,/g, ", "),
    };
  });
}
async function updatesInjuryTimeIsUnknown(list) {
  for (let item of list) {
    try {
      await updateDynamoRecord({
        TableName: process.env.INJURY_TABLE,
        Key: { id: item.id },
        ExpressionAttributeValues: {
          ":injuryTimeIsUnknown": item.newInjuryTimeIsUnknown,
        },
        ExpressionAttributeNames: { "#injuryTimeIsUnknown": "injuryTimeIsUnknown" },
        UpdateExpression: "set #injuryTimeIsUnknown = :injuryTimeIsUnknown",
        ReturnValues: "UPDATED_NEW",
      });
    } catch (err) {
      console.log("[hera-1804] Error updating injuryTimeIsUnknown", err);
      return {
        error: err,
      };
    }
  }
}
module.exports.handleHera1804 = handleHera1804