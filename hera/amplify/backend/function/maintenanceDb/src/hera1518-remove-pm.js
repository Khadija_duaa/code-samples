const {
  scanDynamoRecords,
  queryDynamoRecords,
  deleteDynamoRecord
} = require("./dynamodbHelper.js");

async function runHera1518() {
    var pendingMessages = await getAllPendingMessagesWithOutStaffID()
    await deletePendingMessages(pendingMessages)
    
    console.log({ pendingMessages});
    console.log({ totalPendingMessages: pendingMessages.length });
}

// // Get all Tenants
async function getAllPendingMessagesWithOutStaffID() {
    try {
        let pendingMessages = [];
        let lastEvaluatedKey = null;
        do {
            let params = {
                ExpressionAttributeNames: {
                    "#g": "group",
                },
                ExpressionAttributeValues: {
                  ':start': '2022-08-17',
                  ':end': '2022-10-03'
                },
                TableName: process.env.PENDING_MESSAGE_TABLE,
                FilterExpression: "attribute_not_exists(staffId) AND createdAt BETWEEN :start AND :end",
                ProjectionExpression: "id, #g, createdAt, staffId, pendingMessageStaffId",
                ExclusiveStartKey: lastEvaluatedKey,
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            pendingMessages = [...pendingMessages, ...items];
        } while (lastEvaluatedKey);

        return pendingMessages;
    } catch (err) {
        console.log("[getAllPendingMessagesWithOutStaffID] Error in function getAllPendingMessagesWithOutStaffID", err);
        return {
            error: err,
        };
    }
}

// Delete Pending Message
async function deletePendingMessages(messages) {
    try {
        for(let pendingMessage of messages) {
            let params = {
              Key:{
                  "id": pendingMessage.id
              },
              TableName: process.env.PENDING_MESSAGE_TABLE,
          };
          await deleteDynamoRecord(params);
        }
    } catch (err) {
        console.log("[deletePendingMessages] Error updating deletePendingMessages", err);
        return {
            error: err,
        };
    }
}

module.exports = {
  runHera1518,
};