const { queryDynamoRecords, scanDynamoRecords } = require('./dynamodbHelper.js')
const { CognitoIdentityServiceProvider } = require('aws-sdk')
const cognitoIdentityServiceProvider = new CognitoIdentityServiceProvider()
const userPoolId = process.env.USERPOOL

async function getAllUsers() {
  try {
    let users = []
    let lastEvaluatedKey = null
    do {
      let params = {
        ExpressionAttributeNames: {
          '#group': 'group',
        },
        TableName: process.env.USER_TABLE,
        ProjectionExpression:
          'id, #group, userTenantId, email, createdAt, permissionLogin',
        ExclusiveStartKey: lastEvaluatedKey,
      }
      let scanResult = await scanDynamoRecords(params)
      let items = scanResult.Items
      lastEvaluatedKey = scanResult.LastEvaluatedKey
      users = [...users, ...items]
    } while (lastEvaluatedKey)

    return users
  } catch (err) {
    console.log('[HERA-1144] Error in function getAllUsers', err)
    return {
      error: err,
    }
  }
}

async function getUsersByTenantId(tenantId) {
  try {
    let users = []
    let lastEvaluatedKey = null
    const queryParams = {
      ExpressionAttributeNames: {
        '#id': 'id',
        '#userTenantId': 'userTenantId',
        '#g': 'group',
      },
      ExpressionAttributeValues: {
        ':userTenantId': tenantId,
      },
      TableName: process.env.USER_TABLE,
      IndexName: 'gsi-TenantUsers',
      KeyConditionExpression: '#userTenantId = :userTenantId',
      ProjectionExpression: '#id, #userTenantId, #g, email, isOwner',
    }
    do {
      const queryResult = await queryDynamoRecords(queryParams)
      const items = queryResult.Items
      lastEvaluatedKey = queryResult.LastEvaluatedKey
      users = [...users, ...items]
    } while (lastEvaluatedKey)
    return users
  } catch (error) {
    console.log('[HERA-1144] Error in function getUsersByTenantId', error)
    return {
      error: error,
    }
  }
}

// Signs out from all devices, as an administrator.
async function signUserOut(username) {
  const params = {
    UserPoolId: userPoolId,
    Username: username,
  }

  console.log(`Attempting to signout ${username}`)

  try {
    const result = await cognitoIdentityServiceProvider
      .adminUserGlobalSignOut(params)
      .promise()
    console.log(`Signed out ${username} from all devices`)
    console.log({ result })
    return {
      message: `Signed out ${username} from all devices`,
    }
  } catch (err) {
    console.log(err)
    throw err
  }
}

module.exports = {
  getAllUsers,
  getUsersByTenantId,
  signUserOut,
}
