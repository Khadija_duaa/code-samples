const { buildUpdateParams, scanDynamoRecords, updateDynamoRecord } = require("./dynamodbHelper.js");

async function runHera1120() {
    var users = await getAllUsers();
    
    for(let i=0; i<users.length;i++) {
  	    if(users[i]['group']){
  		    let validGroup = users[i]['group'][0];
            users[i]['validGroup'] = validGroup;
  	    }
    }
    
    //Sort the list by group
    var sortedlist = users.sort(function (a, b) {
        return (a.validGroup < b.validGroup) ? -1 : 1 ;
    });
    
    var groupedbyGroup = await subgroupArraysByGroup(sortedlist);
    
    var groupFiltered = [];
    //Get only duplicated phones by group
    for(var group of groupedbyGroup) {
         const duplicatedList = group.map(v => v.phone).filter((v, i, vIds) => vIds.indexOf(v) !== i);
         const duplicates = group.filter(obj => duplicatedList.includes(obj.phone));
         
         var sortedlistb = duplicates.sort(function (a, b) {
            return (a.phone < b.phone) ? -1 : 1 ;
         });
         if(sortedlistb.length>0){
            groupFiltered.push(sortedlistb);
         }
    }
    
    //Group staff with same phone in subarrays
    var groupedbyPhone = await subgroupArraysByPhone(groupFiltered);
    
    //cleanEmails
    var phonestoClean = await cleanPhones(groupedbyPhone);
    
    console.log(JSON.stringify(phonestoClean));
    return true;
}

async function getAllUsers() {
    try {
        let users = [];
        let lastEvaluatedKey = null;
        do {
            let params = {
                ExpressionAttributeNames: {
                    "#group": "group",
                },
                TableName: process.env.USER_TABLE,
                ProjectionExpression: "id, phone, #group, permissionLogin",
                ExclusiveStartKey: lastEvaluatedKey,
                FilterExpression: "attribute_exists(phone) and phone<> :empty",
                ExpressionAttributeValues: {
                    ":empty": "",
                }
                
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            users = [...users, ...items];
        } while (lastEvaluatedKey);

        return users;
    } catch (err) {
        console.log("[getAllUsers] Error in function getAllUsers", err);
        return {
            error: err,
        };
    }
}

async function subgroupArraysByGroup(list){
    var main = []
    var subarray = []
   
    var initialValue = list[0]['validGroup'];
        for(var i=0; i<list.length;i++) {
  	        if(list[i].validGroup == initialValue) {
                subarray.push(list[i])
            } else {
     			initialValue = list[i].validGroup;
                main.push(subarray);
                subarray = [];
                subarray.push(list[i]);
            }
     
             if(i==list.length-1){
     	        main.push(subarray);
            }
        }     

    return main;
}

async function subgroupArraysByPhone(list){
    
    var main = [];
    
    for (const grouped of list) {
        
        var submain = [];  
        var subarray = [];
  
        var initialValue = grouped[0]['phone'];
       
        for(var i=0; i<grouped.length;i++) {
  	        if(grouped[i].phone == initialValue) {
                subarray.push(grouped[i])
            } else {
     			initialValue = grouped[i].phone;
                submain.push(subarray);
                subarray = [];
                subarray.push(grouped[i]);
            }
     
             if(i==grouped.length-1) {
     	        submain.push(subarray);
            }
        } 
        main.push(submain);
    }
    
    return main;
}

async function cleanPhones(list){
    
    var phonestoClean = [];
 
    for (const grouped of list) {
        
        var subgroup = [];
        
        for(const user of grouped) {
            //sort the subarray by status
            var objsorted = user.sort(function (a, b) {
                return (a.status < b.status) ? -1 : 1 ;
            });
        
            var pos = await getActive(user);

            if(pos!=-1) {
                objsorted.splice(pos, 1);
            }
            
            if(objsorted.length>0){
                subgroup.push(objsorted);
            }
            
            //await updatePhonesDynamo(objsorted);
        }
        phonestoClean.push(subgroup);
    }
    
    return phonestoClean;
}

async function updatePhonesDynamo(list){
    try {
        for (const user of list) {
        
        await updateDynamoRecord({
            TableName: process.env.USER_TABLE,
            Key: {"id": user.id},
            ExpressionAttributeValues: {":phone": null },
            ExpressionAttributeNames: {"#phone": 'phone'},
            UpdateExpression: "set #phone = :phone",
            ReturnValues: "UPDATED_NEW"
        })
            
        }
        
        
    } catch (err) {
        console.log("[hera-1120] Error updating phones", err);
        return {
            error: err,
        };
    }
}

async function getActive(user){
  	var pos = -1;
    for(var i=0; i<user.length;i++) {
    	if(user[i].permissionLogin){
      	    pos = i;
            break;
      }
    }
    return pos;
}

module.exports = {
    runHera1120,
};