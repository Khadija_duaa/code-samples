const { scanDynamoRecords, updateDynamoRecord } = require("./dynamodbHelper.js");
const axios = require('axios');

const accessToken = process.env.TELNYX_TOKEN;

const instance = axios.create({
  baseURL: 'https://api.telnyx.com/v2',
  headers: {
    Authorization: `Bearer ${accessToken}`,
    'Content-Type': 'application/json',
  },
});

async function runHera1119User() {
    var users = await getAllUsers();
    var list = await cleanPhones(users);
    console.log(JSON.stringify(list));
}

async function validateTelnyx(phone_number) {
    try {
        const  { data }  =  await instance.get(`number_lookup/${ phone_number }`);
        const { data: phone } = data;

        if (phone.country_code == 'US') {
            return phone.phone_number;
        }
        
        return '';
     
        
    } catch (error){
        if(error.response && error.response.data.errors.length) {
            let detail = JSON.stringify(error.response.data.errors[0].detail)
            if (detail.includes('Canadian')) {
                return '' // then update with empty value
            }
        }
        console.error(error)
        process.exit(0)
    }
}

async function getAllUsers() {
    try {
        let users = [];
        let lastEvaluatedKey = null;
        do {
            let params = {
                TableName: process.env.USER_TABLE,
                ExpressionAttributeNames: {"#phone": 'phone'},
                ExpressionAttributeValues: {
                ':phone': ""
            },
                FilterExpression: "#phone <> :phone",
                ProjectionExpression: "id, phone ",
                ExclusiveStartKey: lastEvaluatedKey,
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            users = [...users, ...items];
        } while (lastEvaluatedKey);

        return users;
    } catch (err) {
        console.log("[HERA-1119] Error in function getAllusers", err);
        return {
            error: err,
        };
    }
}


async function cleanPhones(list) {
    var cleanedList = [];
    const reg = /^\+[1]\d{10}$/;
    let counter = 1 // TEMP
     for (const user of list) {
        const validate = reg.test(user.phone);
        if(user.phone && !validate){
            user.old = user.phone;
            const clean = user.phone.replace(/\D/g,'');
            const len = clean.length;
            
            user.phone = '';
             
            if (len == 10) {
                user.phone = `1${ clean }`;
            } 
            
            if (len == 11) {
                user.phone = clean;
            }
            
            if (user.phone) {
                user.phone = await validateTelnyx(user.phone);
            }
            
            cleanedList.push(user);
            console.log(counter, JSON.stringify(cleanedList)) // TEMP
            counter++ // TEMP
            //await updatePhone(user) // single
        }
    }
    return cleanedList;
}

async function updatePhone(item) {
    try {

        if (item.phone) {
            await updateDynamoRecord({
                TableName: process.env.USER_TABLE,
                Key: {"id": item.id},
                ExpressionAttributeValues: {":phone": item.phone},
                ExpressionAttributeNames: {"#phone": 'phone'},
                UpdateExpression: "set #phone = :phone",
                ReturnValues: "UPDATED_NEW"
            })
        } else {
            console.log("remove phone in user")
            await updateDynamoRecord({
                TableName: process.env.USER_TABLE,
                Key: {"id": item.id},
                ExpressionAttributeNames: {"#phone": 'phone'},
                UpdateExpression: "remove #phone",
                ReturnValues: "UPDATED_NEW"
            })
        }
        
    } catch (err) {
        console.log("[HERA-1119] Error updating phone", err);
        return {
            error: err,
        };
    }
}



module.exports = {
    runHera1119User,
};