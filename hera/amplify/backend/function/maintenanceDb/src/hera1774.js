/**
 * THIS SCRIPT REMOVES "originationNumber" FIELD ON "TENANT TABLE" WHEN EMPTY
 */

const {
  scanDynamoRecords,
  updateDynamoRecord,
} = require("./dynamodbHelper.js");

/**
 * Main function
 */
async function handleHera1774() {
  const tenantRecordsToUpdate = await getRecordsWithEmptyOriginationNumber();
  // PRINT REPORT BEFORE UPDATING
  printReport(tenantRecordsToUpdate);
  // await removeOriginationNumber(tenantRecordsToUpdate);
}

/**
 * Print a list of Tenant IDs of records wich "originationNumber" field is empty
 * and thus should be removed, from table
 * @param {Array} tenantList
 */
function printReport(tenantList) {
  console.log(`***** TENANT IDs WHICH 'originationNumber' FIELD WILL BE DELETED *****`);
  console.log(`UPDATING ${tenantList.length} RECORDS ...`);
  const listAsJson = JSON.stringify(tenantList, null, 2);
  console.log(listAsJson);
}

/**
 * Scans dynamo records and retrieves the ones with empty "originationNumber"
 * @returns {Array}: List of items which "originationNumber" is empty
 */
async function getRecordsWithEmptyOriginationNumber() {
  let tenantList = [];
  let lastEvaluatedKey = null;

  try {
    do {
      let params = {
        FilterExpression: "originationNumber = :empty",
        ExpressionAttributeValues: { ":empty": "" },
        TableName: process.env.TENANT_TABLE,
        ProjectionExpression: "id, originationNumber",
        ExclusiveStartKey: lastEvaluatedKey,
      };

      let scanResult = await scanDynamoRecords(params);
      lastEvaluatedKey = scanResult.LastEvaluatedKey;
      tenantList = tenantList.concat(scanResult.Items);

    } while(lastEvaluatedKey);

    return tenantList;

  } catch (error) {
    console.log("[HERA-1774 ] Error in function getRecordsWithEmptyOriginationNumber", error);
    return { error };
  }
}

/**
 * Removes "originationNumber" field from a list of records
 * @param {Array} tenantList: List of records which "originationNumber" field will be removed
 */
async function removeOriginationNumber(tenantList) {
  for (let tenant of tenantList) {
    try {
      await updateDynamoRecord({
        TableName: process.env.TENANT_TABLE,
        Key: { id: tenant.id },
        ExpressionAttributeNames: { "#field": "originationNumber" },
        UpdateExpression: "remove #field"
      });
    } catch (error) {
      console.log(`[HERA-1774] Error removing 'originationNumber' (ID:${tenant.id})`, error);
    }
  }
}

module.exports = { handleHera1774 };
