const {scanDynamoRecords, buildUpdateParams, updateDynamoRecord, createDynamoRecord} = require("./dynamodbHelper");

async function handleHera1107 () {
    await setCustomListToTenant()
    await setOptionsToCustomList()
}

async function setOptionsToCustomList() {
    try {
        let customLists = await getAllVehicleRoleForRouteCustomList()
        for (const customList of customLists) {
            for (const option of optionsCustomList) {
                let createOptionCustomList = {
                    TableName: 'OptionsCustomLists-znhsfld3srhgdbamacqgjtk5nq-phasethree',
                    Item: {
                        id: generateUUID(),
                        order: option.order,
                        option: option.option,
                        default: option.default,
                        usedFor: option.usedFor,
                        daysCount: option.daysCount,
                        canBeEdited: option.canBeEdited,
                        canBeDeleted: option.canBeDeleted,
                        canBeReorder: option.canBeReorder,
                        group: customList.group,
                        createdAt: new Date().toISOString(),
                        updatedAt: new Date().toISOString(),
                        optionsCustomListsCustomListsId: customList.id
                    }
                };
                await createDynamoRecord(createOptionCustomList);
            }
        }
        return true;
    } catch (error) {
        console.log("Error pe: ", error);
    }
}

async function getAllVehicleRoleForRouteCustomList() {
    try {
        let customLists = []
        let lastEvaluatedKey = null
        do {
            let params = {
                ExpressionAttributeNames: {
                    '#group': 'group',
                    '#type': 'type',
                },
                ExpressionAttributeValues: {
                    ':type': 'vehicle-role-for-route'
                },
                FilterExpression: "#type = :type",
                TableName: 'CustomLists-znhsfld3srhgdbamacqgjtk5nq-phasethree',
                ProjectionExpression: 'id, #group, #type',
                ExclusiveStartKey: lastEvaluatedKey
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items
            lastEvaluatedKey = scanResult.LastEvaluatedKey
            customLists = [...customLists, ...items]
        } while (lastEvaluatedKey)
        return customLists
    } catch (err) {
        console.log("[getAllVehicleRoleForRouteCustomList] Error in function getAllVehicleRoleForRouteCustomList", err);
        return {
            error: err,
        };
    }
}

async function setCustomListToTenant() {
    try {
        let tenants = await getAllTenants()
        for (const tenant of tenants) {
            let createCustomList = {
                TableName: 'CustomLists-znhsfld3srhgdbamacqgjtk5nq-phasethree',
                Item: {
                    id: generateUUID(),
                    type: 'vehicle-role-for-route',
                    listCategory: 'Daily Rostering',
                    listName: 'Vehicle Role For Route',
                    listDisplay: 'Status Pills',
                    group: tenant.group,
                    createdAt: new Date().toISOString(),
                    updatedAt: new Date().toISOString()
                }
            };
            await createDynamoRecord(createCustomList);
        }
        return true;
    } catch (error) {
        console.log("Error pe: ", error);
    }
}

async function getAllTenants() {
    try {
       let tenants = []
        let lastEvaluatedKey = null
        do {
            let params = {
                ExpressionAttributeNames: {
                    '#group': 'group',
                },
                TableName: process.env.TENANT_TABLE,
                ProjectionExpression: 'id, #group, CustomLists',
                ExclusiveStartKey: lastEvaluatedKey
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items
            lastEvaluatedKey = scanResult.LastEvaluatedKey
            tenants = [...tenants, ...items]
        } while (lastEvaluatedKey)
        return tenants
    } catch (err) {
        console.log("[getAllTenants] Error in function getAllTenants", err);
        return {
            error: err,
        };
    }
}

function generateUUID() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

const optionsCustomList = [
    {
        order: 1,
        option: 'Primary',
        default: true,
        usedFor: 0,
        daysCount: 0,
        canBeEdited: false,
        canBeDeleted: false,
        canBeReorder: false
    },
    {
        order: 2,
        option: 'Split With',
        default: false,
        usedFor: 0,
        daysCount: 0,
        canBeEdited: true,
        canBeDeleted: true,
        canBeReorder: true
    },
    {
        order: 3,
        option: 'Sweeper',
        default: false,
        usedFor: 0,
        daysCount: 0,
        canBeEdited: true,
        canBeDeleted: true,
        canBeReorder: true
    },
]

module.exports = {
    handleHera1107
}