const {
    updateDynamoRecord,
    buildUpdateParams,
    queryDynamoRecords,
    scanDynamoRecords
} = require('./dynamodbHelper.js');

async function handlerHera3577(){
    //uncomment the following line to run the script
    //await loadAllTenants();
}

async function loadAllTenants(){
    try {
        let lastEvaluatedKey = null;
        do{
            let params = {
                TableName: process.env.TENANT_TABLE,
                ExpressionAttributeNames: {
                    '#g': 'group'
                },
                ProjectionExpression: "id, #g",
                ExclusiveStartKey: lastEvaluatedKey
            }
            const result = await scanDynamoRecords(params);
            lastEvaluatedKey = result.LastEvaluatedKey;
            console.log('lastEvaluatedKey: ', lastEvaluatedKey);

            await getCustomList(result.Items);

        }while(lastEvaluatedKey);
    } catch (error) {
        console.log("[loadAllTenants] Error in function loadAllTenants", error);
    } finally {
        console.log('***************************');
        console.log('Update Finished');
        console.log('***************************');
    }
}

async function getCustomList(tenants=[]){
    const types = ['kudo-type', 'issue-type'];

    for(let tenant of tenants){
        for(let type of types){
            const customListId = await getCustomListByType(tenant.group, type);
            let optionsCustomList = [];
            if(customListId){
                optionsCustomList = await getOptionsCustomListById(customListId) || [];
            }
        
            for(let item of optionsCustomList){
                await updateOptionCustomList(item)
            }
        }
    }
}


async function getCustomListByType(group, type){
    try {
        let lastEvaluatedKey = null;

        let queryParams = {
            TableName: process.env.CUSTOM_LIST_TABLE,
            IndexName: 'byType',
            ExpressionAttributeNames: {
                '#g': 'group',
                '#t': 'type'
            },
            ExpressionAttributeValues: {
                ':g': group,
                ':t': type
            },
            KeyConditionExpression: '#g =:g AND #t = :t',
            ProjectionExpression: "id, #g, #t",
            ExclusiveStartKey: lastEvaluatedKey
        }
        const result = await queryDynamoRecords(queryParams);
        lastEvaluatedKey = result.LastEvaluatedKey;

        return result.Items[0]?.id
    } catch (error) {
        console.log("Error in function getCustomListByType", error);
    }
}


async function getOptionsCustomListById(id){
    let records = [];
    try {
        let lastEvaluatedKey = null;
        do {
            let queryParams = {
                TableName: process.env.OPTIONS_CUSTOM_LIST_TABLE,
                IndexName: 'gsi-OptionsCustomLists',
                ExpressionAttributeNames: {
                    '#customListId': 'optionsCustomListsCustomListsId',
                    '#isCustom': 'isCustom',
                    '#option': 'option'
                },
                ExpressionAttributeValues: {
                    ':customListId': id,
                    ':isCustom': true
                },
                KeyConditionExpression: '#customListId = :customListId',
                FilterExpression: '#isCustom = :isCustom',
                ProjectionExpression: "id, isCustom, canBeEdited, canBeDeleted, #option, #customListId",
                ExclusiveStartKey: lastEvaluatedKey
            }
            const result = await queryDynamoRecords(queryParams);
            lastEvaluatedKey = result.LastEvaluatedKey;
            records = records.concat(result.Items);
            
        } while (lastEvaluatedKey);
    } catch(error) {
        console.log("Error in function getOptionsCustomListById", error);
        records = [];
    } finally {
        return records
    }
}

async function updateOptionCustomList(item){
    try{
        const input = {
            id: item.id,
            canBeEdited: item.isCustom ? true : false,
            canBeDeleted: item.isCustom ? true : false,
            updatedAt: new Date().toISOString()
        }
        if(!item.isCustom) return;
        
        const buildParams = buildUpdateParams(process.env.OPTIONS_CUSTOM_LIST_TABLE, input, input.id);
        await updateDynamoRecord(buildParams);
    }catch(error){
        console.log("Error in function updateOptionCustomList", error);
    }
}

module.exports = { handlerHera3577 };