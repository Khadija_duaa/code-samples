/**
 * This script sets 'permissionManageCounselings' to true for every user that has full admin Access(permissionFullAccess) otherwise
 * sets the permission to false
 */

const {
  scanDynamoRecords,
  updateDynamoRecord,
} = require("./dynamodbHelper");

/**
 * Main function
 */
async function handleHera2216() {
  try {
    // Print Report
    await handlePrintReport();
    // Update Users
    // await handleUpdateRecords();    
  } catch (error) {
    console.log(error);
  }
}

/**
 * Handles printing the report of the changes that this script will cause
 */
async function handlePrintReport() {
  const reportObj = {
    summary: { false: 0, true: 0, },
    false: { count: 0, items: []},
    true: { count: 0, items: []},
  };
  await applyCallbackToAllUsers((userList) => {
    for (const user of userList) {
      const permissionManageCounseling = !!user.permissionFullAccess;
      reportObj.summary[permissionManageCounseling] += 1;
      reportObj[permissionManageCounseling].count += 1;
      reportObj[permissionManageCounseling].items.push({
        ...user,
        toSet_permissionManageCounseling: permissionManageCounseling,
      });
    }
  });
  console.log('Report', JSON.stringify(reportObj, null, 2));
}

/**
 * Handles the update of every User record that doe not have 'permissionFullAccess', by setting the field 'permissionCounselings' to false
 */
async function handleUpdateRecords() {
  const MAX_COUNT_PER_BATCH = 20;
  const sendUpdates = async (userList) => {
    const promises = userList.map(async (user) => (await updateUser(user, !!user.permissionFullAccess)));
    await Promise.all(promises);
  };
  await applyCallbackToAllUsers(async (userList) => {
    let batchUpdateList = [];
    for (const user of userList) {
      batchUpdateList.push(user);
      if (batchUpdateList.length === MAX_COUNT_PER_BATCH) {
        await sendUpdates(batchUpdateList);
        batchUpdateList = [];
      }
    }
    if (batchUpdateList.length) {
      await sendUpdates(batchUpdateList);
    }
  });
}

/**
 * Updates a User record, sets permissionCounselings to false
 * @param {Object} user - Current user record to update
 * @param {Boolean} isActive - Value to set to permissionManageCounselings
*/
async function updateUser(user, isActive) {
  try {
    const params = {
      TableName: process.env.USER_TABLE,
      Key: { id: user.id },
      ExpressionAttributeValues: {
        ":permissionManageCounselings": isActive,
      },
      ExpressionAttributeNames: {
        "#permissionManageCounselings": "permissionManageCounselings",
      },
      UpdateExpression: "set #permissionManageCounselings = :permissionManageCounselings",
      ReturnValues: "UPDATED_NEW",
    };
    await updateDynamoRecord(params);
  }
  catch (error) {
    console.log(`Error updating user [ID=${user.id}]`);
  }
}

/**
 * Applies a callback to a list of Users that does not have 'permissionFullAccess'
 * @param {Function} cb - Callback to execute for each group of Users retrieved from DynamoDB
 */
async function applyCallbackToAllUsers(cb) {
  let lastEvaluatedKey = null;
  try {
    do {
      const params = {
        ExpressionAttributeNames: {
          '#id': 'id',
          '#group': 'group',
          '#permissionFullAccess': 'permissionFullAccess',
        },
        TableName: process.env.USER_TABLE,
        ProjectionExpression: '#id, #group, #permissionFullAccess',
        ExclusiveStartKey: lastEvaluatedKey,
      };
      const scanResult = await scanDynamoRecords(params);
      await cb(scanResult.Items);
      lastEvaluatedKey = scanResult.LastEvaluatedKey;
      console.log({lastEvaluatedKey});
    }
    while (lastEvaluatedKey);
  }
  catch (error) {
    console.log("[applyCallbackToAllUsers] Error", error);
  }
}

module.exports = {
  handleHera2216,
};
