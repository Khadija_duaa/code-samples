const {
    getBackup,
    populateBackup,
} = require('./helper')

const GROUP = 'juno-transit-25'
const RESTORED_TABLE = 'Infraction-zeobggbnyva4padyiddojnmnqy-production-restored-DEC1-22'

async function restoreInfraction() {
    const backup = await getBackup(GROUP, RESTORED_TABLE)
    console.log('[HERA-1873] Infraction', JSON.stringify(backup))

    await populateBackup(backup, process.env.INFRACTION_TABLE)
}

module.exports = {
    restoreInfraction,
}
