const {
    getBackup,
    populateBackup,
} = require('./helper')

const GROUP = 'juno-transit-25'
const RESTORED_TABLE = 'DrugTest-zeobggbnyva4padyiddojnmnqy-production-restored-DEC1-22'

async function restoreDrugTest() {
    const backup = await getBackup(GROUP, RESTORED_TABLE)
    console.log('[HERA-1873] DrugTest', JSON.stringify(backup))

    await populateBackup(backup, process.env.DRUGTEST_TABLE)
}

module.exports = {
    restoreDrugTest,
}
