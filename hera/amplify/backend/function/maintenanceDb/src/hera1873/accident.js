const {
    getBackup,
    populateBackup,
} = require('./helper')

const GROUP = 'juno-transit-25'
const RESTORED_TABLE = 'Accident-zeobggbnyva4padyiddojnmnqy-production-restored-DEC1-22'

async function restoreAccident() {
    const backup = await getBackup(GROUP, RESTORED_TABLE)
    console.log('[HERA-1873] Accident', JSON.stringify(backup))

    await populateBackup(backup, process.env.ACCIDENT_TABLE)
}

module.exports = {
    restoreAccident,
}
