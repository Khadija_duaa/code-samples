const {
    getBackup,
    populateBackup,
} = require('./helper')

const GROUP = 'juno-transit-25'
const RESTORED_TABLE = 'Document-zeobggbnyva4padyiddojnmnqy-production-restored-DEC1-22'

async function restoreDocument() {
    const backup = await getBackup(GROUP, RESTORED_TABLE)
    console.log('[HERA-1873] Document', JSON.stringify(backup))

    await populateBackup(backup, process.env.DOCUMENT_TABLE)
}

module.exports = {
    restoreDocument,
}
