const {
    getBackup,
    populateBackup,
} = require('./helper')

const GROUP = 'juno-transit-25'
const RESTORED_TABLE = 'Card-zeobggbnyva4padyiddojnmnqy-production-restored-DEC1-22'

async function restoreCard() {
    const backup = await getBackup(GROUP, RESTORED_TABLE)
    console.log('[HERA-1873] Card', JSON.stringify(backup))

    await populateBackup(backup, process.env.CARD_TABLE)
}

module.exports = {
    restoreCard,
}
