const {
    getBackup,
    populateBackup,
} = require('./helper')

const GROUP = 'juno-transit-25'
const RESTORED_TABLE = 'DailyRoster-zeobggbnyva4padyiddojnmnqy-production-restored-DEC1-22'

async function restoreDailyRoster() {
    const backup = await getBackup(GROUP, RESTORED_TABLE)
    console.log('[HERA-1873] DailyRoster', JSON.stringify(backup))

    await populateBackup(backup, process.env.DAILYROSTER_TABLE)
}

module.exports = {
    restoreDailyRoster,
}
