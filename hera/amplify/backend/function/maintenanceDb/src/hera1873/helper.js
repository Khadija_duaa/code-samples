const {
    queryDynamoRecords,
    createDynamoRecord,
} = require('../dynamodbHelper')

async function getBackup(group, table) {
    try {
        let injuries = [];
        let lastEvaluatedKey = null
        do {
            let params = {
                ExpressionAttributeValues: {
                    ':group': group,
                },
                ExpressionAttributeNames: {
                    '#group': 'group',
                },
                KeyConditionExpression: '#group = :group',
                TableName: table,
                IndexName: 'byGroup',
                ExclusiveStartKey: lastEvaluatedKey
            };
            var queryResult = await queryDynamoRecords(params);
            var items = queryResult.Items
            lastEvaluatedKey = queryResult.LastEvaluatedKey
            injuries = [...injuries, ...items]
        } while (lastEvaluatedKey)

        return injuries;
    } catch (err) {
        console.log("[HERA-1873] Error in function getBackup", err);
        throw err
    }
}

async function populateBackup(payload, table) {
    for (const item of payload) {
        let input = {
            TableName: table,
            Item: item,
        }
        await createDynamoRecord(input)
    }
}

module.exports = {
    getBackup,
    populateBackup,
}