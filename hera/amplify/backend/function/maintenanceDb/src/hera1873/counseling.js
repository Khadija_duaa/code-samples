const {
    getBackup,
    populateBackup,
} = require('./helper')

const GROUP = 'juno-transit-25'
const RESTORED_TABLE = 'Counseling-zeobggbnyva4padyiddojnmnqy-production-restored-DEC1-22'

async function restoreCounseling() {
    const backup = await getBackup(GROUP, RESTORED_TABLE)
    console.log('[HERA-1873] Counseling', JSON.stringify(backup))

    await populateBackup(backup, process.env.COUNSELING_TABLE)
}

module.exports = {
    restoreCounseling,
}
