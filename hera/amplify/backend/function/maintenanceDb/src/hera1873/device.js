const {
    getBackup,
    populateBackup,
} = require('./helper')

const GROUP = 'juno-transit-25'
const RESTORED_TABLE = 'Device-zeobggbnyva4padyiddojnmnqy-production-restored-DEC1-22'

async function restoreDevice() {
    const backup = await getBackup(GROUP, RESTORED_TABLE)
    console.log('[HERA-1873] Device', JSON.stringify(backup))

    await populateBackup(backup, process.env.DEVICE_TABLE)
}

module.exports = {
    restoreDevice,
}
