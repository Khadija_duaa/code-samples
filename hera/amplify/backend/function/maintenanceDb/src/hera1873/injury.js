const {
    getBackup,
    populateBackup,
} = require('./helper')

const GROUP = 'juno-transit-25'
const RESTORED_TABLE = 'Injury-zeobggbnyva4padyiddojnmnqy-production-restored-DEC1-22'

async function restoreInjury() {
    const backup = await getBackup(GROUP, RESTORED_TABLE)
    console.log('[HERA-1873] Injury', JSON.stringify(backup))

    await populateBackup(backup, process.env.INJURY_TABLE)
}

module.exports = {
    restoreInjury,
}
