const {
    getBackup,
    populateBackup,
} = require('./helper')

const GROUP = 'juno-transit-25'
const RESTORED_TABLE = 'CompanyScoreCard-zeobggbnyva4padyiddojnmnqy-production-restored-DEC1-22'

async function restoreCompanyScoreCard() {
    const backup = await getBackup(GROUP, RESTORED_TABLE)
    console.log('[HERA-1873] CompanyScoreCard', JSON.stringify(backup))

    await populateBackup(backup, process.env.COMPANYSCORECARD_TABLE)
}

module.exports = {
    restoreCompanyScoreCard,
}
