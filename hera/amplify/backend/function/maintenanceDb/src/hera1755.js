const {scanDynamoRecords, buildUpdateParams, updateDynamoRecord, createDynamoRecord} = require("./dynamodbHelper");

async function handleHera1755 () {
    //  Set Custom List
    //await setCounselingSeverityCustomListToTenant()

    //  Update Custom List
    await updateCustomList()
}

async function updateCustomList() {
    try {
        let customLists = await getAllCounselingSeverityCustomList()
        for (const customList of customLists) {
            const item = {
                id:customList.id,
                createdAt:customList.createdAt,
                canDeleteAllOptions: customList.type == 'counseling-severity'? false:true
            }
                const updateParams = buildUpdateParams(process.env.CUSTOM_LIST_TABLE, item, item.id)
               console.log({updateParams})
               await updateDynamoRecord(updateParams)
            }
        return true;
    } catch (error) {
        console.log("Error pe: ", error);
    }
}

async function getAllCounselingSeverityCustomList() {
    try {
        let customLists = []
        let lastEvaluatedKey = null
        do {
            let params = {
                ExpressionAttributeNames: {
                    '#group': 'group',
                    '#type': 'type',
                },
                TableName: process.env.CUSTOM_LIST_TABLE,
                ProjectionExpression: 'id, #group, #type',
                ExclusiveStartKey: lastEvaluatedKey
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items
            lastEvaluatedKey = scanResult.LastEvaluatedKey
            customLists = [...customLists, ...items]
        } while (lastEvaluatedKey)
        return customLists
    } catch (err) {
        console.log("[getAllCounselingSeverityCustomList] Error in function getAllCounselingSeverityCustomList", err);
        return {
            error: err,
        };
    }
}




module.exports = {
    handleHera1755
}