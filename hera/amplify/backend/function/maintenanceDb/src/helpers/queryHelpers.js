
const invoicesByGroupAndYearAndMonth = /* GraphQL */ `
  query InvoicesByGroupAndYearAndMonth(
    $group: String
    $yearMonth: ModelInvoiceByGroupYearMonthCompositeKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelInvoiceFilterInput
    $limit: Int
    $nextToken: String
  ) {
    invoicesByGroupAndYearAndMonth(
      group: $group
      yearMonth: $yearMonth
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        month
        year
        createdAt
        invoiceLineItems {
          items {
            id
            date
            activeStaff
            standardCostExt
            bundleCostExt
            performanceCostExt
            rosteringCostExt
            staffCostExt
            vehiclesCostExt
            messagingCostExt
            
          }
        }
      }
      nextToken
    }
  }
`;

module.exports = {
  invoicesByGroupAndYearAndMonth,
};