const {
  scanDynamoRecords,
  updateDynamoRecord,
} = require("./dynamodbHelper.js");

async function handleHera1642() {
  let injuries = await getAllInjuries();
  let list = searchInjuryTimeIsUnkown(injuries);
  let report = getReport(list);
  console.log(JSON.stringify(report));
  //await updatesInjuryTimeIsUnkown(list);

}

async function getAllInjuries() {
  try {
    let injuries = [];
    let lastEvaluatedKey = null;
    do {
      let params = {
        TableName: process.env.INJURY_TABLE,
        ProjectionExpression: "id, injuryTimeIsUnkown",
        ExclusiveStartKey: lastEvaluatedKey,
      };
      let scanResult = await scanDynamoRecords(params);
      let items = scanResult.Items;
      lastEvaluatedKey = scanResult.LastEvaluatedKey;
      injuries = [...injuries, ...items];
    } while (lastEvaluatedKey);
    return injuries;
  } catch (err) {
    console.log("[getAllInjuries] Error in function getAllInjuries", err);
    return {
      error: err,
    };
  }
}

function searchInjuryTimeIsUnkown(list) {
  return list.filter( value => value.injuryTimeIsUnkown == 'Yes' || value.injuryTimeIsUnkown == 'No').map( elem => {
    const { injuryTimeIsUnkown } = elem;
    const newInjuryTimeIsUnkown = injuryTimeIsUnkown == "Yes" ? true : false;
    return {
      ...elem,
      newInjuryTimeIsUnkown
    }
  })
}

function getReport(list) {
  return list.map(({id, injuryTimeIsUnkown, newInjuryTimeIsUnkown}) => {
    return {
      id: id,
      injuryTimeIsUnkown: injuryTimeIsUnkown.toString().replace(/,/g, ", "),
      newInjuryTimeIsUnkown: newInjuryTimeIsUnkown.toString().replace(/,/g, ", "),
    };
  });
}

async function updatesInjuryTimeIsUnkown(list) {
  try {
    for (let item of list) {
      await updateDynamoRecord({
        TableName: process.env.INJURY_TABLE,
        Key: { id: item.id },
        ExpressionAttributeValues: {
          ":injuryTimeIsUnkown": item.newInjuryTimeIsUnkown,
        },
        ExpressionAttributeNames: { "#injuryTimeIsUnkown": "injuryTimeIsUnkown" },
        UpdateExpression: "set #injuryTimeIsUnkown = :injuryTimeIsUnkown",
        ReturnValues: "UPDATED_NEW",
      });
    }
  } catch (err) {
    console.log("[hera-1642] Error updating injuryTimeIsUnkown", err);
    return {
      error: err,
    };
  }
}

module.exports.handleHera1642 = handleHera1642