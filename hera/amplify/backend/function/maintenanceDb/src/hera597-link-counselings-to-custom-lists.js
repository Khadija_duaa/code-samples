/**
 * This script creates a field 'counselingSeverityId' from every Counseling record to link it to
 * Counseling Severity Options of Options Custom Lists
 */

const {
  scanDynamoRecords,
  updateDynamoRecord,
} = require("./dynamodbHelper");

const warningTypeToSeverityMap = {
  INFORMAL_WARNING: '1) Informal Warning',
  FORMAL_VERBAL_WARNING: '2) Formal Verbal Warning',
  FINAL_WARNING: '3) Final Warning',
  DISMISSAL: '4) Dismissal',
};

// Object to handled cached values of Counseling Severity Options by Group
const severityOptionsByGroup = {};

/**
 * Main function
 */
async function handleHera597() {
  try {
    // Load all Counseling Severity Options into 'severityOptionsByGroup'
    await loadAllSeverityOptions();
    // Print Report
    await handlePrintReport();
    // Link counselings to Severity Options Custom Lists
    // await handleUpdateRecords();    
  } catch (error) {
    console.log(error);
  }
}

/**
 * Find the corresponding Counseling Severity ID for a given Cousenling record
 * @param {Object} counseling - Record to which find the corresponding Counseling Severity ID
 * @returns {String|null} The id of the found Option CUstom List, otherwise null
 */
function getSeverityOptionId(counseling) {
  const severityOptions = severityOptionsByGroup[counseling.group]
  if (!severityOptions) return null;
  const option = warningTypeToSeverityMap[counseling.warningType];
  const searchCb = (counseling.warningType)?
    (item) => {return item.option === option}:
    (item) => {return !!item.default};
  const foundOption = severityOptions.find(searchCb);
  if (!foundOption) return null;
  return foundOption.id;
};

/**
 * Handles printing the report of the changes that this script will cause
 */
async function handlePrintReport() {
  const reportObj = {
    toBeUpdated: {count: 0, items: []},
    toBeSkipped: {count: 0, items: []},
  };
  await applyCallbackToAllCounselings((counselingList) => {
    for (const counseling of counselingList) {
      const severityId = getSeverityOptionId(counseling);
      if (!severityId || counseling.counselingSeverityId) {
        reportObj.toBeSkipped.count += 1;
        reportObj.toBeSkipped.items.push({...counseling});
        continue;
      };
      reportObj.toBeUpdated.count += 1;
      reportObj.toBeUpdated.items.push({
        ...counseling,
        counselingSeverityId: severityId,
      });
    }
  });
  console.log('Report', JSON.stringify(reportObj, null, 2));
}

/**
 * Handles the update of every Counseling record, by creating the field 'counselingSeverityId'
 */
async function handleUpdateRecords() {
  await applyCallbackToAllCounselings(async (counselingList) => {
    for (const counseling of counselingList) {
      const severityId = getSeverityOptionId(counseling);
      if (!severityId || counseling.counselingSeverityId) continue;
      await updateCounseling(counseling, severityId);
    }
  });
}

/**
 * Updates a Counseling record, sets counselingSeverityId
 * @param {Object} counseling - Current counseling record to update
 * @param {String} severityId - ID of the found Option Custom list to link as Counseling Severity
*/
async function updateCounseling(counseling, severityId) {
  try {
    const params = {
      TableName: process.env.COUNSELING_TABLE,
      Key: { id: counseling.id },
      ExpressionAttributeValues: {
        ":counselingSeverityId": severityId,
      },
      ExpressionAttributeNames: {
        "#counselingSeverityId": "counselingSeverityId",
      },
      UpdateExpression: "set #counselingSeverityId = :counselingSeverityId",
      ReturnValues: "UPDATED_NEW",
    };
    await updateDynamoRecord(params);
  }
  catch (error) {
    console.log(`Error updating counseling [ID=${counseling.id}]`);
  }
}

/**
 * Applies a callback to a list of Counselings
 * @param {Function} cb - Callback to execute for each group of Counselings retrieved from DynamoDB
 */
async function applyCallbackToAllCounselings(cb) {
  let lastEvaluatedKey = null;
  try {
    do {
      const params = {
        ExpressionAttributeNames: {
          '#id': 'id',
          '#group': 'group',
          '#warningType': 'warningType',
          '#counselingSeverityId': 'counselingSeverityId',
        },
        TableName: process.env.COUNSELING_TABLE,
        ProjectionExpression: '#id, #warningType, #group, #counselingSeverityId',
        FilterExpression: 'attribute_not_exists(#counselingSeverityId)',
        ExclusiveStartKey: lastEvaluatedKey,
      };
      const scanResult = await scanDynamoRecords(params);
      await cb(scanResult.Items);
      lastEvaluatedKey = scanResult.LastEvaluatedKey;
      console.log({lastEvaluatedKey});
    }
    while (lastEvaluatedKey);
  }
  catch (error) {
    console.log("[applyCallbackToAllCounselings] Error", error);
  }
}

/**
 * Fetches all Options for Counseling Severity
 */
async function loadAllSeverityOptions() {
  let lastEvaluatedKey = null;
  do {
    try {
      const params = {
        ExpressionAttributeValues: {
          ':opt1': '1) Informal Warning',
          ':opt2': '2) Formal Verbal Warning',
          ':opt3': '3) Final Warning',
          ':opt4': '4) Dismissal',
        },
        ExpressionAttributeNames: {
          '#id': 'id',
          '#group': 'group',
          '#option': 'option',
          '#default': 'default',
        },
        FilterExpression: '#option in (:opt1, :opt2, :opt3, :opt4)',
        TableName: process.env.OPTIONS_CUSTOM_LIST_TABLE,
        ProjectionExpression: '#id, #group, #option, #default',
        ExclusiveStartKey: lastEvaluatedKey,
      };
      const scanResult = await scanDynamoRecords(params);
      // Load Severity Options into global map
      for (const item of scanResult.Items) {
        severityOptionsByGroup[item.group] = severityOptionsByGroup[item.group] || [];
        severityOptionsByGroup[item.group].push({...item});
      }
      lastEvaluatedKey = scanResult.LastEvaluatedKey;
    }
    catch (error) {
      console.log('[loadAllSeverityOptions] Error loading options', error);
    }
  }
  while (lastEvaluatedKey);
}

module.exports = {
  handleHera597,
};
