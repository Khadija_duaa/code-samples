const {
    createDynamoRecord,
    scanDynamoRecords
} = require('./dynamodbHelper.js')

const {
    getAllTenants,
    getVehicleTypeFromCustomList
} = require('./customList/helper')

var { nanoid } = require('nanoid')

async function runHera1026() {
    let tenants = await getAllTenants()
    await setAuthToDriveCustomList(tenants)
}

async function setAuthToDriveCustomList(tenants){
    try {
        for(let tenant of tenants){
            const { 
                tenkVanId,
                customDelVanId,
                standardParcelId,
                standardParcelSmallId,
                standardParcelLargeId,
                standardParcelXLId
            }  = await getVehicleTypeFromCustomList(tenant.group)
            let staffs = await getStaffByGroup(tenant.group)
            console.log({ group: tenant.group, staffs });

            let counter = 0
            for (let staff of staffs) {
                /**Counter : Remove when testing globally */
                counter++
                if(counter == 2) break
                /*************************************** */
                console.log({ staff });
                let typeList = []
                if (!staff.customDeliveryVan && !staff.authorizedLBS) {
                    typeList = [standardParcelId, standardParcelSmallId, standardParcelLargeId, standardParcelXLId]
                }
                if (staff.customDeliveryVan) {
                    typeList = [customDelVanId]
                }
                if (staff.authorizedLBS) {
                    typeList = [tenkVanId]
                }
                console.log({ typeList });
                console.log('CREATING OPTION STAFF LIST');
                for(const typeId of typeList) {
                    const input = {
                        TableName: process.env.OPTIONS_CUSTOM_LIST_STAFF_TABLE,
                        Item: {
                            id: nanoid(),
                            group: staff.group,
                            optionsCustomListsStaffStaffId: staff.id,
                            optionsCustomListsStaffOptionCustomListId: typeId,
                            createdAt: new Date().toISOString(),
                            updatedAt: new Date().toISOString()
                        }
                    };
                    console.log({ input });
                    await createDynamoRecord(input);
                }
            }
        }
    } catch (e) {
        console.log("[setAuthToDriveCustomList] Error in function setAuthToDriveCustomList", e);
        return {
            error: e,
        };
        
    }
}

async function getStaffByGroup(group) {

    try {
        let staffs = []
        let lastEvaluatedKey = null
        do {
            let params = {
                TableName: process.env.STAFF_TABLE,
                ExpressionAttributeNames: {
                    '#g': 'group',
                    '#albs': 'authorizedLBS',
                    '#cdv': 'customDeliveryVan'
                },
                ExpressionAttributeValues: {
                    ':g': group,
                },
                FilterExpression: "#g = :g",
                ProjectionExpression: 'id, #g, #albs, #cdv',
                ExclusiveStartKey: lastEvaluatedKey
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items
            lastEvaluatedKey = scanResult.LastEvaluatedKey
            staffs = [...staffs, ...items]
        } while (lastEvaluatedKey)
        return staffs
    } catch (err) {
        console.log("[getStaffByGroup] Error in function getStaffByGroup", err);
        return {
            error: err,
        };
    }
}

module.exports = {
    runHera1026
}