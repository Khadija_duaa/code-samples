const {
  queryDynamoRecords,
  scanDynamoRecords,
  updateDynamoRecord,
} = require('./dynamodbHelper.js')

async function getAllTenants() {
  try {
    let tenants = []
    let lastEvaluatedKey = null
    do {
      let params = {
        ExpressionAttributeNames: {
          '#group': 'group',
        },
        TableName: process.env.TENANT_TABLE,
        ProjectionExpression: 'id, #group',
        ExclusiveStartKey: lastEvaluatedKey,
      }
      let scanResult = await scanDynamoRecords(params)
      let items = scanResult.Items
      lastEvaluatedKey = scanResult.LastEvaluatedKey
      tenants = [...tenants, ...items]
    } while (lastEvaluatedKey)

    return tenants
  } catch (err) {
    console.log('[HERA-1126] Error in function getAllTenants', err)
    return {
      error: err,
    }
  }
}

async function getUsersByTenantId(tenantId) {
  try {
    let users = []
    let lastEvaluatedKey = null
    const queryParams = {
      ExpressionAttributeNames: {
        '#id': 'id',
        '#userTenantId': 'userTenantId',
        '#g': 'group',
      },
      ExpressionAttributeValues: {
        ':userTenantId': tenantId,
      },
      TableName: process.env.USER_TABLE,
      IndexName: 'gsi-TenantUsers',
      KeyConditionExpression: '#userTenantId = :userTenantId',
      ProjectionExpression:
        '#id, #userTenantId, #g, email, isOwner, isCreator, createdAt',
    }
    do {
      const queryResult = await queryDynamoRecords(queryParams)
      const items = queryResult.Items
      lastEvaluatedKey = queryResult.LastEvaluatedKey
      users = [...users, ...items]
    } while (lastEvaluatedKey)
    return users
  } catch (error) {
    console.log('[HERA-1126] Error in function getUsersByTenantId', error)
    return {
      error: error,
    }
  }
}

async function updateUser(user) {
  try {
    const updateParams = {
      TableName: process.env.USER_TABLE,
      Key: {
        id: user.id,
      },
      ExpressionAttributeNames: {
        '#isOwner': 'isOwner',
        '#isCreator': 'isCreator',
      },
      ExpressionAttributeValues: {
        ':isOwner': user.isOwner,
        ':isCreator': user.isCreator,
      },
      UpdateExpression: 'set #isOwner = :isOwner, #isCreator = :isCreator',
      ReturnValues: 'UPDATED_NEW',
    }

    return await updateDynamoRecord(updateParams)
  } catch (error) {
    console.log('[HERA-1126] Error in function updateUser', error)
    return {
      error: error,
    }
  }
}

async function handleHera1126() {
  const tenants = await getAllTenants()
  for (const tenant of tenants) {
    const users = await getUsersByTenantId(tenant.id)
    const filteredUsers = users.filter(
      user =>
        !user.email.startsWith('sysadmin-') &&
        !user.email.endsWith('@herasolution.app')
    )

    if (!filteredUsers.length) {
      continue
    }

    let hasOwner = filteredUsers.find(user => user.isOwner)
    let hasCreator = filteredUsers.find(user => user.isCreator)

    if (!hasOwner) {
      let dates = filteredUsers.map(user => {
        return new Date(user.createdAt)
      })
      let oldDate = new Date(Math.min(...dates))
      let firstUser = filteredUsers.find(
        user => oldDate.getTime() === new Date(user.createdAt).getTime()
      )
      firstUser.isOwner = true
      if (!hasCreator) {
        firstUser.isCreator = true
      }
      await updateUser(firstUser)
    }
  }
}

module.exports = {
  handleHera1126,
}
