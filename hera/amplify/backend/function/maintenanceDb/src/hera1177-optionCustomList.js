const { 
    scanDynamoRecords,
    createDynamoRecord,
    queryDynamoRecords
} = require("./dynamodbHelper");

var { nanoid } = require('nanoid')

const filterExpression = [ 
    {
        expression: {  },
        filter: 'attribute_not_exists(#d) and attribute_not_exists(#h)'
    },
    {
        expression: {
            ":true": true
        },
        filter: 'attribute_exists(#d) and attribute_exists(#h) and #d <> :true and #h <> :true'
    },
    {
        filter: 'attribute_not_exists(#d) and attribute_exists(#h)'
    }
]

async function runHera1177 () {

    //  Set Options Custom List
    await setOptionsToParkingSpaceCustomList()
}

async function setOptionsToParkingSpaceCustomList() {
    const customLists = await getAllParkingSpaceCustomList()
    // console.log({ customLists });
    for (const customList of customLists) {
        console.log({ customList });
        const { id, group } = customList
        try {
            const valueList = await getAllDropdownParkingSpaceValueList(group)
            
            console.log({ valueList });
            console.log({ length: valueList.length });
            if(!valueList.length) continue

            const parkingSpaceItem = valueList[0] // There is always 1 single value per Value List groups-> parking-space, vehicle-type, etc

            let valueListItems = await getAllDropdownParkingSpaceValueListItems(parkingSpaceItem.id)
            
            console.log({ valueListItems });
            console.log({ length: valueListItems.length });
            console.log("------------");
            if (!valueListItems.length) continue
            const uniqueValueListItems = [... new Map(valueListItems.map(item => [item.value, item])).values()]
            console.log({ uniqueValueListItems });
            console.log({ length: uniqueValueListItems.length });

            let order = 1
            for(const option of uniqueValueListItems) {
                console.log("------------");
                console.log({ option });
                console.log("----------");
                try {
                    let createOptionCustomList = {
                        TableName: process.env.OPTIONS_CUSTOM_LIST_TABLE,
                        Item: {
                            id: nanoid(36),
                            order: order,
                            option: option.value,
                            default: 'false',
                            usedFor: 0,
                            daysCount: 0,
                            canBeEdited: true,
                            canBeDeleted: true,
                            canBeReorder: true,
                            group: group,
                            createdAt: new Date().toISOString(),
                            updatedAt: new Date().toISOString(),
                            optionsCustomListsCustomListsId: id
                        }
                    };
                    console.log({ createOptionCustomList });
                    await createDynamoRecord(createOptionCustomList);
                    order++
                } catch (e) {
                    console.log("[For of] Error in dropdownParkingSpaceValueListItems: ", e);
                }
            }
        } catch (e) {
            console.log("[setOptionsToParkingSpaceCustomList] Error in function setOptionsToParkingSpaceCustomList: ", e);
        }
    }
}

async function getAllParkingSpaceCustomList() {
    try {
        let customLists = []
        let lastEvaluatedKey = null
        do {
            let params = {
                ExpressionAttributeNames: {
                    '#group': 'group',
                    '#type': 'type',
                },
                ExpressionAttributeValues: {
                    ':type': 'parking-space'
                },
                FilterExpression: "#type = :type",
                TableName: process.env.CUSTOM_LIST_TABLE,
                ProjectionExpression: 'id, #group, #type',
                ExclusiveStartKey: lastEvaluatedKey
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items
            lastEvaluatedKey = scanResult.LastEvaluatedKey
            customLists = [...customLists, ...items]
        } while (lastEvaluatedKey)
        return customLists
    } catch (err) {
        console.log("[getAllParkingSpaceCustomList] Error in function getAllParkingSpaceCustomList", err);
        return {
            error: err,
        };
    }
}

async function getAllDropdownParkingSpaceValueList(group) {
    try {
        let customLists = []
        let lastEvaluatedKey = null
        do {
            let params = {
                ExpressionAttributeNames: {
                    '#g': 'group',
                    '#k': 'key',
                },
                ExpressionAttributeValues: {
                    ':k': 'parking-space',
                    ':g': group,
                },
                FilterExpression: "#k = :k and #g = :g",
                TableName: process.env.VALUE_LIST_TABLE,
                ProjectionExpression: 'id, #g, #k',
                ExclusiveStartKey: lastEvaluatedKey
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items
            lastEvaluatedKey = scanResult.LastEvaluatedKey
            customLists = [...customLists, ...items]
        } while (lastEvaluatedKey)
        return customLists
    } catch (err) {
        console.log("[getAllDropdownParkingSpaceValueList] Error in function getAllDropdownParkingSpaceValueList", err);
        return {
            error: err,
        };
    }
}

async function getAllDropdownParkingSpaceValueListItems(valueListId) {
    let customLists = []
    let lastEvaluatedKey = null
    
    for(const item of filterExpression) {
        do {
            try {
                let params = {
                    TableName: process.env.VALUE_LIST_ITEM_TABLE,
                    IndexName: "gsi-ValueListItems",
                    KeyConditionExpression: "#vlItemvlId = :vlItemvlId",
                    ExpressionAttributeValues: {
                        ":vlItemvlId": valueListId,
                        ...item.expression
                    },
                    ExpressionAttributeNames: {
                        "#vlItemvlId": "valueListItemValueListId",
                        '#d': 'deleted',
                        '#h': 'hidden',
                    },
                    FilterExpression: item.filter,
                    ExclusiveStartKey: lastEvaluatedKey,
                };
                console.log({ params });
                let queryResult = await queryDynamoRecords(params);
                let items = queryResult.Items;
                lastEvaluatedKey = queryResult.LastEvaluatedKey;
                customLists = [...customLists, ...items];
            } catch (e) {
                console.log("[getAllDropdownParkingSpaceValueListItems] Error in function getAllDropdownParkingSpaceValueListItems", e);
            }
        } while (lastEvaluatedKey)
    }
    return customLists 
}

module.exports = {
    runHera1177
}