const telnyxUtils = require('./telnyxUtils');
const { scanDynamoRecords } = require('./../dynamodbHelper.js');

async function generateNumberReport() {
  try {
    const profileName = process.env.TELNYX_PROFILE_NAME;
    const profileId = await telnyxUtils.getProfileIdByProfileName(profileName);
    const telnyxNumbers = await telnyxUtils.getTelnyxProfileNumbers(profileId);

    const tableName = process.env.TELNYX_TABLE;
    const dynamoNumbers = await scanDynamoRecords({ TableName: tableName });

    const telnyxPhoneNumbers = telnyxNumbers.map(({ phone_number }) => phone_number);
    const dynamoPhoneNumbers = dynamoNumbers.Items.map(({ phoneNumber }) => phoneNumber);

    // 1. Telnyx numbers not in DynamoDB
    const numbersInTelnyxNotInDynamo = findNumbersNotInArray(telnyxPhoneNumbers, dynamoPhoneNumbers);
    printReport('Telnyx numbers not in DynamoDB:', numbersInTelnyxNotInDynamo);

    // 2. DynamoDB numbers not in Telnyx
    const numbersInDynamoNotInTelnyx = findNumbersNotInArray(dynamoPhoneNumbers, telnyxPhoneNumbers);
    printReport('DynamoDB numbers not in Telnyx:', numbersInDynamoNotInTelnyx);

    // 3. Duplicate numbers in DynamoDB
    const duplicateNumbers = findDuplicateNumbers(dynamoPhoneNumbers);
    printReport('Duplicate numbers in DynamoDB:', duplicateNumbers);
  } catch (error) {
    console.error('An error occurred while generating the number report:', error);
  }
}

function findNumbersNotInArray(sourceArray, targetArray) {
  return sourceArray.filter((num) => !targetArray.includes(num));
}

function findDuplicateNumbers(numbersArray) {
  const numberFrequency = {};
  const duplicateNumbers = [];

  numbersArray.forEach((number) => {
    numberFrequency[number] = (numberFrequency[number] || 0) + 1;

    if (numberFrequency[number] === 2) {
      duplicateNumbers.push(number);
    }
  });

  return duplicateNumbers;
}

function printReport(title, numbers) {
  console.log(title);
  numbers.forEach((number) => {
    console.log(number);
  });
}

module.exports = {
  generateNumberReport,
};
