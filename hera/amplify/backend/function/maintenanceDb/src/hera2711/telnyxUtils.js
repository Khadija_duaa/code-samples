const telnyxAxios = require('./axios');

async function getTelnyxProfileNumbers(profileId) {
    try {
        let numbers = []
        let pageNumber = 1
        let totalPages = 0

        do {
            const response = await telnyxAxios.get(`/messaging_profiles/${profileId}/phone_numbers`, {
                params: {
                    'page[number]': pageNumber,
                },
            });

            totalPages = response.data.meta.total_pages
            pageNumber++

            response.data.data.forEach(item => {
                numbers.push(item)
            })
        } while (totalPages >= pageNumber)

        return numbers;
    } catch (error) {
        console.error('An error occurred while retrieving numbers from Telnyx profile:', error);
        throw error;
    }
}

async function getProfileIdByProfileName(profileName) {
    try {
        const profilesResponse = await telnyxAxios.get('/messaging_profiles');

        if (profilesResponse.status !== 200) {
            throw new Error('Error retrieving Telnyx profiles');
        }

        const profiles = profilesResponse.data.data;
        const profile = profiles.find((p) => p.name === profileName);

        if (!profile) {
            throw new Error(`Profile "${profileName}" not found in Telnyx`);
        }

        return profile.id;
    } catch (error) {
        console.error('An error occurred while retrieving the profile ID from Telnyx:', error);
        throw error;
    }
}

module.exports = {
    getTelnyxProfileNumbers,
    getProfileIdByProfileName,
};
