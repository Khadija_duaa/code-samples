const {
    updateDynamoRecord,
    scanDynamoRecords
} = require('../dynamodbHelper.js');

const TENANT_TEST_TABLE = 'Tenant-ygx7qox3gfgtzlz434jwtgokzu-test'; // Harcoded because we only want to clean this table with this script

async function cleanTestTenantFields() {
    try {
        const testTenants = await getTestTenants();

        for (const tenant of testTenants) {
            const excludedGroups = ['awesome-graphs-inc-6', 'juno-demo-mqa-53', 'solve-it-simply-17', 'hera-demo-mqa-47'];
            if (excludedGroups.includes(tenant.group)) {
                console.log(`Skipping tenant ${tenant.id} in excluded group ${tenant.group}`);
                continue;
            }
            await clearTenantFields(tenant.id);
        }

        console.log('Cleaning of test Tenants completed successfully.');
    } catch (error) {
        console.error('An error occurred during the cleaning of test Tenants:', error);
    }
}

async function getTestTenants() {
    try {
        const tableName = TENANT_TEST_TABLE;
        const queryParams = {
            TableName: tableName,
            ExpressionAttributeNames: { "#group": "group" },
            ProjectionExpression: 'id, originationNumber, messageServiceProvider, #group',
            FilterExpression: 'attribute_exists(originationNumber) AND attribute_exists(messageServiceProvider) AND (#group <> :group1 AND #group <> :group2 AND #group <> :group3 AND #group <> :group4)',
            ExpressionAttributeValues: {
                ':group1': 'awesome-graphs-inc-6',
                ':group2': 'juno-demo-mqa-53',
                ':group3': 'solve-it-simply-17',
                ':group4': 'hera-demo-mqa-47'
            }
        };

        let testTenants = [];
        let scanResult;

        do {
            scanResult = await scanDynamoRecords(queryParams);
            testTenants = testTenants.concat(scanResult.Items);
            queryParams.ExclusiveStartKey = scanResult.LastEvaluatedKey;
        } while (scanResult.LastEvaluatedKey);

        return testTenants;
    } catch (error) {
        console.error('An error occurred while retrieving test Tenants:', error);
        throw error;
    }
}

async function clearTenantFields(tenantId) {
    const tableName = TENANT_TEST_TABLE
    const updateParams = {
        TableName: tableName,
        Key: { id: tenantId },
        UpdateExpression: 'REMOVE originationNumber SET messageServiceProvider = :none',
        ExpressionAttributeValues: {
            ':none': 'None',
        },
    };

    try {
        await updateDynamoRecord(updateParams);
        console.log(`Cleared originationNumber and set messageServiceProvider to "None" for tenant ${tenantId}`);
    } catch (error) {
        console.error(`Error updating tenant ${tenantId}:`, error);
        throw error;
    }
}

module.exports = {
    cleanTestTenantFields
};
