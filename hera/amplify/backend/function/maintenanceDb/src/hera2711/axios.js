const axios = require('axios');

const telnyxApiBaseUrl = 'https://api.telnyx.com/v2';

const telnyxAxios = axios.create({
    baseURL: telnyxApiBaseUrl,
    headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${process.env.TELNYX_TOKEN}`,
    },
});

module.exports = telnyxAxios;