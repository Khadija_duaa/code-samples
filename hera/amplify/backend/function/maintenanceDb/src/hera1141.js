const {
  scanDynamoRecords,
  updateDynamoRecord,
} = require("./dynamodbHelper.js");

async function handleHera1141() {
  let staffs = await getAllStaffs();
  let list = searchAlternateNames(staffs);
  let report = getReport(list);
  console.log(JSON.stringify(report));
  //await updatesAlternateNames(list);
}

async function getAllStaffs() {
  try {
    let staffs = [];
    let lastEvaluatedKey = null;
    do {
      let params = {
        TableName: process.env.STAFF_TABLE,
        ProjectionExpression: "id, alternateNames",
        ExclusiveStartKey: lastEvaluatedKey,
      };
      let scanResult = await scanDynamoRecords(params);
      let items = scanResult.Items;
      lastEvaluatedKey = scanResult.LastEvaluatedKey;
      staffs = [...staffs, ...items];
    } while (lastEvaluatedKey);
    return staffs;
  } catch (err) {
    console.log("[getAllStaffs] Error in function getAllStaffs", err);
    return {
      error: err,
    };
  }
}

function searchAlternateNames(list) {
  const alterNames = list.filter((value) => value.alternateNames);
  const newArray = alterNames.map((element) => {
    const { alternateNames } = element;
    const newAlternateNames = alternateNames.filter(
      (item) =>
        item.toLowerCase() != 'null' &&
        !item.includes("No Name Imported") &&
        !item.includes("Not Available") &&
        !item.includes("Coming") &&
        !item.includes("Soon") &&
        !item.includes("Fair") &&
        !item.includes("Poor") &&
        !item.includes("Fantastic") &&
        !item.includes("Great") &&
        !item.includes("%") &&
        checkForTransporterId(item)
    );
    return {
      id: element.id,
      alternateNames: alternateNames,
      newAlternateNames: newAlternateNames,
    };
  });
  return newArray.filter(
    (item) => item["alternateNames"].length != item["newAlternateNames"].length
  );
}

function getReport(list) {
  return list.map((staff) => {
    return {
      id: staff.id,
      alternateNames: staff.alternateNames.toString().replace(/,/g, ", "),
      newAlternateNames: staff.newAlternateNames.toString().replace(/,/g, ", "),
    };
  });
}

function checkForTransporterId(alternateNames) {
  let regex = /^(A)(?=.*[A-Z])[A-Z0-9]*$/gm;
  const words = alternateNames.split(" ");
  for (const word of words) {
    let result = regex.test(word.replace(/\s+/g, ""));
    if (result && (word.length == 14 || word.length == 13)) return false;
  }
  return true;
}

async function updatesAlternateNames(list) {
  try {
    for (let item of list) {
      await updateDynamoRecord({
        TableName: process.env.STAFF_TABLE,
        Key: { id: item.id },
        ExpressionAttributeValues: {
          ":alternateNames": item.newAlternateNames,
        },
        ExpressionAttributeNames: { "#alternateNames": "alternateNames" },
        UpdateExpression: "set #alternateNames = :alternateNames",
        ReturnValues: "UPDATED_NEW",
      });
    }
  } catch (err) {
    console.log("[hera-1141] Error updating alternateNames", err);
    return {
      error: err,
    };
  }
}

module.exports = {
  handleHera1141,
};
