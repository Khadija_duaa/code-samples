const {
    scanDynamoRecords,
    buildUpdateParams, 
    updateDynamoRecord
} = require('./dynamodbHelper.js')

async function hera1660 () {

    //  update Users('permissionMessageTemplate'), if Permission 'Full Admin Access' is true
    return await updateUserHera1660()
}

async function getAllUsers() {
    try {
        let users = []
        let lastEvaluatedKey = null
        do {
            let params = {
                ExpressionAttributeNames: {
                    '#group': 'group',
                },
                ExpressionAttributeValues: {
                  ':permissionFullAccess': true,
                },
                ProjectionExpression: 'id,#group,permissionFullAccess,permissionMessageTemplate, email',
                FilterExpression: 'permissionFullAccess = :permissionFullAccess',
                TableName: process.env.USER_TABLE,
                ExclusiveStartKey: lastEvaluatedKey
            }
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items
            lastEvaluatedKey = scanResult.LastEvaluatedKey
            users = [...users, ...items]
        } while (lastEvaluatedKey)
        return users

    } catch (err) {
        console.log('Error in function [getAllUsers]', err)
        return {
            error: err
        }
    }
}

async function updateUserHera1660() {
        let users = await getAllUsers()
        for (const user of users) {
            const item = {
                id: user.id,
                updatedAt:new Date().toISOString(),
                permissionMessageTemplate: true
            }
            const updateParams = buildUpdateParams(process.env.USER_TABLE, item, item.id)
            try {
                console.log(updateParams)
               //await updateDynamoRecord(updateParams)
            } catch (e) {
                console.log("Error pe: ", e);
            }
        }
        return true;
}

module.exports = {
    hera1660
}