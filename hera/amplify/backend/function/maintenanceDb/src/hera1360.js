const {
  scanDynamoRecords,
  createDynamoRecord
} = require("./dynamodbHelper.js");

async function handleHera1360() {
  let staffs = await getAllStaffs();
  let messages = await getAllMessages();
  let historyMessages = await getHistoryMessages();
  const dataSaveToTable = await structureDataToSave( staffs, messages, historyMessages );
  await setRecordVisitedChatStaff( dataSaveToTable );
}

async function getAllStaffs() {
  try {
    let staffs = [];
    let lastEvaluatedKey = null;
    do {
      let params = {
        TableName: process.env.STAFF_TABLE,
        ExpressionAttributeNames: {
          '#group': 'group'
        },
        ProjectionExpression: "id, #group",
        ExclusiveStartKey: lastEvaluatedKey,
      };
      let scanResult = await scanDynamoRecords(params);
      let items = scanResult.Items;
      lastEvaluatedKey = scanResult.LastEvaluatedKey;
      staffs = [...staffs, ...items];
    } while (lastEvaluatedKey);
    return staffs;
  } catch (err) {
    console.log("[getAllStaffs] Error in function getAllStaffs", err);
    return {
      error: err,
    };
  }
}

async function getAllMessages() {
  try {
    let messages = [];
    let lastEvaluatedKey = null;
    do {
      let params = {
        TableName: process.env.MESSAGE_TABLE,
        FilterExpression: "#channelType = :channelType AND #messageType = :messageType",
        ExpressionAttributeNames: {"#channelType": "channelType", "#messageType":"messageType" },
        ExpressionAttributeValues: {
          ':channelType': 'RESPONSE',
          ':messageType': 'messenger'
        },
        ProjectionExpression: "id, messageType, channelType, staffId, updatedAt",
        ExclusiveStartKey: lastEvaluatedKey,
      };
      let scanResult = await scanDynamoRecords(params);
      let items = scanResult.Items;
      lastEvaluatedKey = scanResult.LastEvaluatedKey;
      messages = [...messages, ...items];
    } while (lastEvaluatedKey);
    return messages;
  } catch (err) {
    console.log("[getAllMessages] Error in function getAllMessages", err);
    return {
      error: err,
    };
  }
}

async function getHistoryMessages() {
  try {
    let historyMessages = [];
    let lastEvaluatedKey = null;
    do {
      let params = {
        TableName: process.env.HISTORY_MESSAGE_TABLE,
        ExpressionAttributeNames: {
          '#group': 'group'
        },
        ProjectionExpression: "id, createdAt, #group, messageID, userID",
        ExclusiveStartKey: lastEvaluatedKey,
      };
      let scanResult = await scanDynamoRecords(params);
      let items = scanResult.Items;
      lastEvaluatedKey = scanResult.LastEvaluatedKey;
      historyMessages = [...historyMessages, ...items];
    } while (lastEvaluatedKey);
    return historyMessages;
  } catch (err) {
    console.log("[getHistoryMessages] Error in function getHistoryMessages", err);
    return {
      error: err,
    };
  }
}

async function getRecordMessageReadStatus() {
  try {
    let recordMessageReadStatus = [];
    let lastEvaluatedKey = null;
    do {
      let params = {
        TableName: process.env.MESSAGE_READ_STATUS_TABLE,
        ProjectionExpression: "staffID, userID",
        ExclusiveStartKey: lastEvaluatedKey,
      };
      let scanResult = await scanDynamoRecords(params);
      let items = scanResult.Items;
      lastEvaluatedKey = scanResult.LastEvaluatedKey;
      recordMessageReadStatus = [...recordMessageReadStatus, ...items];
    } while (lastEvaluatedKey);
    return recordMessageReadStatus;
  } catch (err) {
    console.log("[getHistoryMessages] Error in function getHistoryMessages", err);
    return {
      error: err,
    };
  }
}

async function setRecordVisitedChatStaff( records ) {
  try {
    const recordsMessageReadStatus = await getRecordMessageReadStatus();
    for (const record of records) {
      const existRecordInTable = existRecord( recordsMessageReadStatus, record );
      if (!existRecordInTable) {
        let createVisitRecord = {
          TableName: process.env.MESSAGE_READ_STATUS_TABLE,
          Item: {
            id: generateUUID(),
            staffID: record.staffID,
            userID: record.userID,
            "staffID#userID": `${record.staffID}#${record.userID}`,
            readUpToDateTime: record.readUpToDateTime,
            group: record.group,
            createdAt: record.createdAt,
            updatedAt: record.createdAt
          }
        };
        await createDynamoRecord(createVisitRecord);  
      }
    }
    return true;
  } catch (err) {
    console.log('[setRecordVisitedChatStaff] Error in function setRecordVisitedChatStaff', err)
  }
}

const filterMessages = ( arrayMessages, staffID ) => {
  const messagesFiltered = arrayMessages.filter( message => {
    return message.staffId === staffID;
  })
  return messagesFiltered;
}

const lastMessageSentByStaff = ( messages ) => {
  if (messages.length) {
    return messages[0];
  } else {
    return undefined;
  }
}

const usersListViewMessage = ( historyMessages, messageID ) => { 
  return historyMessages.filter( message => message.messageID === messageID );
}

const orderByDate = (a, b) => {
  const firstDate = new Date( a.updatedAt ).getTime();
  const nextDate = new Date( b.updatedAt ).getTime();
  return nextDate - firstDate; 
}

const deleteDuplicateRecords = ( listRecords ) => {
  const listIDs = [];
  const nonDuplicateRecords = [];
  for (let index = 0; index < listRecords.length; index++) {
    const record = listRecords[index];
    if (!listIDs.includes(record.userID)) {
      listIDs.push( record.userID );
      nonDuplicateRecords.push( record );
    }
  }
  return nonDuplicateRecords;
}

function structureDataToSave( listStaff, messages, historyMessageReader ) {
  const dataSaveToMessageReadStatusTable = [];
  for (let index = 0; index < listStaff.length; index++) {
    const staff = listStaff[index];
    const { id, group } = staff;
      const messagesSendStaff = filterMessages( messages, id ).sort( orderByDate );
      const lastMessageOfStaff = lastMessageSentByStaff( messagesSendStaff )?.id;
      if ( lastMessageOfStaff ) {
        let listRecords = usersListViewMessage( historyMessageReader, lastMessageOfStaff );
        listRecords = deleteDuplicateRecords( listRecords );
        if (listRecords.length) {
          for (let index = 0; index < listRecords.length; index++) {
            const { userID, createdAt } = listRecords[index];
            dataSaveToMessageReadStatusTable.push( { createdAt: new Date( createdAt ).toISOString(), group, readUpToDateTime: new Date( createdAt ).toISOString(), staffID: id, userID } );
          }
        }
      }
  }
  return dataSaveToMessageReadStatusTable;
}

function generateUUID() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

function existRecord( listRecord, record ) {
  const { staffID, userID } = record;
  return listRecord.find( item =>  item.staffID === staffID && item.userID === userID );
}

module.exports = {
  handleHera1360,
};