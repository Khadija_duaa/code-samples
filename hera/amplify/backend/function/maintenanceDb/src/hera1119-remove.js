const { buildUpdateParams, scanDynamoRecords, updateDynamoRecord } = require("./dynamodbHelper.js");

async function runHera1119Remove() {
    var staffs = await getAllStaffs();
    await removeEmptyPhones(staffs);
}

async function getAllStaffs() {
    try {
        let staffs = [];
        let lastEvaluatedKey = null;
        do {
            let params = {
                ExpressionAttributeNames: {
                    "#status": "status",
                    "#group": "group",
                },
                
                TableName: process.env.STAFF_TABLE,
                ProjectionExpression: "id, phone, #status, #group, transporterId",
                ExclusiveStartKey: lastEvaluatedKey,
                FilterExpression: "attribute_exists(phone) and phone = :empty",
                ExpressionAttributeValues: {
                    ":empty": ""
                }
                
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            staffs = [...staffs, ...items];
        } while (lastEvaluatedKey);

        return staffs;
    } catch (err) {
        console.log("[getAllStaffs] Error in function getAllStaffs", err);
        return {
            error: err,
        };
    }
}


async function removeEmptyPhones(list){
    try {
        for (const staff of list) {
        
        await updateDynamoRecord({
            TableName: process.env.STAFF_TABLE,
            Key: {"id": staff.id},
            UpdateExpression: 'remove #phone ',
            ExpressionAttributeNames: { '#phone': 'phone' },
            ReturnValues: "UPDATED_NEW"
        })
        
        console.log("Staff id:", staff.id)
        return     
        }
        
        
    } catch (err) {
        console.log("[hera-1119 remove] Error updating phones", err);
        return {
            error: err,
        };
    }
}

module.exports = {
    runHera1119Remove,
};