const { scanDynamoRecords, createDynamoRecord } = require("./dynamodbHelper");
const createDefaultLabelTypes = async( ) => {
    try {
        let tenants = [];
        let lastEvaluatedKey = null;
        do {
        let params = {
            ExpressionAttributeNames: {
            "#group": "group",
            },
            TableName: process.env.TENANT_TABLE,
            ProjectionExpression: "id, #group",
            ExclusiveStartKey: lastEvaluatedKey,
        };
        let scanResult = await scanDynamoRecords(params);
        let items = scanResult.Items;
        lastEvaluatedKey = scanResult.LastEvaluatedKey;
        for( const tenant of items ){
            await setTypes( tenant.group )
        }
        tenants = [...tenants, ...items];
        } while (lastEvaluatedKey);

        return tenants;
    } catch (err) {
        console.log("error2 tenant, lastevaluatedkey". lastEvaluatedKey)
        console.log("[getTenants] Error in function getTenants", err);
        return {
        error: err,
        };
    }
}

async function setTypes( group ){
    /** Set Label Types for the group */
    const defaultsType = ['Associates', 'Vehicles']
    for( const name of defaultsType ){
        try{
            let options = {
                TableName: process.env.LABELTYPE_TABLE,
                Item: {
                    id: generateUUID(),
                    group,
                    name,
                    __typename: 'LabelType',
                    createdAt: new Date().toISOString(),
                    updatedAt: new Date().toISOString(),
                }
            };
            console.log('create:', options )
            await createDynamoRecord(options);
        } catch(e) {
            console.log("Error1 setType in tenant", group)
            console.log(e);

        }
    }
}
function generateUUID() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}
module.exports = { createDefaultLabelTypes }
