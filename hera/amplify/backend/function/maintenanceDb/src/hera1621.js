const {
  scanDynamoRecords,
  updateDynamoRecord,
} = require('./dynamodbHelper')

const {
    getUser,
    createUser,
    addUserToGroup,
    listUsersInGroup
} = require('./cognito/cognitoActions')

async function runHera1621() {
    let usersWithOutCognitoSub = await getAllUsers()
    console.log({ usersWithOutCognitoSub });
    if(!usersWithOutCognitoSub.length) return

    const { usersWithCognitoUser, usersWithOutCognitoUser } = await checkCognitoUserExists(usersWithOutCognitoSub)
    console.log({ usersWithCognitoUser });
    if(usersWithCognitoUser.length) await updateUserCognitoSub(usersWithCognitoUser)
    console.log({ usersWithOutCognitoUser });
    if(usersWithOutCognitoUser.length) await createCognitoUser(usersWithOutCognitoUser)

}

// Get users with email and missing cognitoSub
async function getAllUsers() {
  try {
      let users = [];
      let lastEvaluatedKey = null;
      do {
          let params = {
              ExpressionAttributeNames: {
                  "#group": "group",
              },
              TableName: process.env.USER_TABLE,
              ProjectionExpression: "id, #group, email, permissionFullAccess",
              ExclusiveStartKey: lastEvaluatedKey,
              FilterExpression: "attribute_not_exists(cognitoSub) and attribute_exists(email)",
          };
          let scanResult = await scanDynamoRecords(params);
          let items = scanResult.Items;
          lastEvaluatedKey = scanResult.LastEvaluatedKey;
          users = [...users, ...items];
      } while (lastEvaluatedKey);

      return users;
  } catch (err) {
      console.log("[getAllUsers] Error in function getAllUsers", err);
      return {
          error: err,
      };
  }
}

// Check if they have cognito user
async function checkCognitoUserExists(users){
    let usersWithCognitoUser = []
    let usersWithOutCognitoUser = []
    for(const user of users){
        let result
        try {
            result = await getUser(user.email)
            usersWithCognitoUser.push({ cognitoSub: result.Username, id: user.id })
        } catch (e) {
            usersWithOutCognitoUser.push({ resend: false, ...user })
            console.log('Cognito User wasnt found');
        }
    }
    return { usersWithCognitoUser, usersWithOutCognitoUser }
}

async function updateUserCognitoSub(users) {
    try {
        for(const user of users) {
            console.log({ updated: user });
            await updateUser(user)
        }
    } catch (e) {
        console.log("[updateUserCognitoSub] Error updating updateUserCognitoSub", e);
        return {
            error: e,
        };
    }
}
async function updateUser(user){
    try {
        await updateDynamoRecord({
          TableName: process.env.USER_TABLE,
          Key: { "id": user.id },
          ExpressionAttributeValues: { ":cognitoSub": user.cognitoSub },
          ExpressionAttributeNames: { "#cognitoSub": 'cognitoSub' },
          UpdateExpression: "set #cognitoSub = :cognitoSub",
          ReturnValues: "UPDATED_NEW"
        })
    } catch (e) {
        console.log("[updateUser] Error updating updateUser", e);
        return {
            error: e,
        };
    }
}

async function createCognitoUser(users) {
    // console.log({ users });
    try {
        let adminList = []
        adminList = await listUsersinTheGroup('admin')
        console.log({ adminList });
        for(const user of users) {
            // console.log({ user });
            let userList = []
            const response = await createUser(user.email, user.id, user.resend);
            console.log({ createdCognitoUser: response });

            // Add user to the tenant's group
            userList = await listUsersinTheGroup(user.group[0])

            console.log({ email: user.email });
            console.log({ userList });

            if(!userList.length) return

            let userInGroup = userList.includes(user.email)

            console.log({ userInGroup });
            // Add user to the Tenant's group
            if(!userInGroup) {
                console.log(`Adding user ${user.email} to Tenant's group ${user.group[0]}`);
                try {
                    await addUserToGroup(user.email, user.group[0])
                } catch (e) {
                    console.log(`Error adding user ${user.email} to Tenant's group`);
                }
            }

            if(!adminList.length) return

            let userAdminInGroup = adminList.includes(user.email)

            // Add user to the addmin group
            if(!userAdminInGroup) {
                if(!user.permissionFullAccess) return
                console.log(`Adding user ${user.email} to Admin's group`);
                try {
                    await addUserToGroup(user.email, 'admin')
                } catch (e) {
                    console.log(`Error adding user ${user.email} to Admins's group`);
                }
            }

            // Update CognitoSub
            try {
                let result = await getUser(user.email)
                await updateUser({ cognitoSub: result.Username, id: user.id })
            } catch (e) {
                console.log('Cognito User wasnt found');
            }
        }
    } catch (e) {
        console.log('Cognito User couldnt be created', e);
    }

}

async function listUsersinTheGroup(group){
    let userList = []
    let lastEvaluatedKey = null
    let records = []
    try {
        do{
            let userExistsInGroup = await listUsersInGroup(group, 60, lastEvaluatedKey)
            var items = userExistsInGroup.Users
            lastEvaluatedKey = userExistsInGroup.NextToken
            records = [...records, ...items]
        }while(lastEvaluatedKey)
        console.log({ records });
    
        if(!records.length) return
    
        records.forEach( record => {
            let email = record.Attributes.find(item  => item.Name == 'email' )
            userList.push(email.Value)
        })

    } catch (e) {
        console.log(`[listUsersInGroup] Error listing Users`);
        return {
            error: e
        }
    }
    return userList
}

module.exports = {
    runHera1621,
};