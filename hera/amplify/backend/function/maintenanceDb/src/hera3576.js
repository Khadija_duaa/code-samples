const {
    scanDynamoRecords,
    deleteDynamoRecord,
    queryDynamoRecords
} = require('./dynamodbHelper.js')

async function handlerHera3576(){
    //uncomment the following line to run the script
    //await loadAllTenants()
}


async function loadAllTenants(){
    try {
        let lastEvaluatedKey = null;
        do{
            let params = {
                TableName: process.env.TENANT_TABLE,
                ExpressionAttributeNames: {
                    '#g': 'group'
                },
                ProjectionExpression: "id, #g",
                ExclusiveStartKey: lastEvaluatedKey
            }
            const result = await scanDynamoRecords(params);
            lastEvaluatedKey = result.LastEvaluatedKey;
            console.log('lastEvaluatedKey: ', lastEvaluatedKey);

            await removeValueList(result.Items);

        }while(lastEvaluatedKey);
    } catch (error) {
        console.log("[loadAllTenants] Error in loadAllTenants function", error);
    } finally {
        console.log('***************************');
        console.log('Delete Finished');
        console.log('***************************');
    }
}

async function removeValueList(tenants=[]){
    //values list for delete
    const valueListTypes = ['infraction-type', 'kudo-type'];

    for(let key of valueListTypes){
        for(let tenant of tenants){
            const valueList  = await getValueList(tenant.group, key);
            for(let vlItem of valueList){
                let valueListId = vlItem.id;
                let valueListItems = [];
                if(valueListId){
                    valueListItems = await getValueListItems(valueListId);
                }

                let tableName = process.env.VALUELIST_ITEM_TABLE;
                for(let item of valueListItems){
                    //delete value list items
                    await deleteRecord(tableName, item.id);
                }

                tableName = process.env.VALUELIST_TABLE;
                if(valueListId){
                    //delete value list
                    await deleteRecord(tableName, valueListId);
                }
            }
            
        }
    }
}


async function getValueList(group, key){
   let valuesList = []
    try {
        let lastEvaluatedKey = null;
        let queryParams = {
            TableName: process.env.VALUELIST_TABLE,
            IndexName: 'byGroup',
            ExpressionAttributeNames: {
                '#g': 'group',
                '#k': 'key'
            },
            ExpressionAttributeValues: {
                ':g': group,
                ':k': key
            },
           
            KeyConditionExpression: '#g = :g AND #k = :k',
            ProjectionExpression: "id, #g, #k",
            ExclusiveStartKey: lastEvaluatedKey
        }
        const result = await queryDynamoRecords(queryParams);
        lastEvaluatedKey = result.LastEvaluatedKey;
        valuesList = valuesList.concat(result.Items);
    } catch (error) {
        console.log('error in getValueList function', error);
        valuesList = [];
    } finally {
        return valuesList;
    }
}

async function getValueListItems(valueListId){
    let valueListItems = []
    
    try {
        let lastEvaluatedKey = null;
        do {
            let queryParams = {
                TableName: process.env.VALUELIST_ITEM_TABLE,
                IndexName: 'gsi-ValueListItems',
                ExpressionAttributeNames: {
                    '#valueListId': 'valueListItemValueListId',
                    '#v' : 'value',
                },
                ExpressionAttributeValues: {
                    ':valueListId': valueListId 
                },
                KeyConditionExpression: '#valueListId = :valueListId',
                ProjectionExpression: "id, #v",
                ExclusiveStartKey: lastEvaluatedKey
            }
            const result = await queryDynamoRecords(queryParams);
            lastEvaluatedKey = result.LastEvaluatedKey;
            valueListItems = valueListItems.concat(result.Items);
        } while (lastEvaluatedKey);
        
    } catch (error) {
        console.log('Error in getValueListItems function', error)
    } finally {
        return valueListItems
    }
}

async function deleteRecord(tableName, id){
    try {
        const deleteParams = {
            Key:{ id },
            TableName: tableName,
        }
        await deleteDynamoRecord(deleteParams)
    } catch (error) {
        console.log('Error in deleteRecord function', error)
    }
}


module.exports = { handlerHera3576 };