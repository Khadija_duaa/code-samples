const { 
    deleteDynamoRecord,
    queryDynamoRecords
} = require('./dynamodbHelper.js')

async function handleValueListItem(valueList, valueListItems){
    let text = ''
    if(valueListItems.length){
        text += valueListItems.reduce((text, valueListItem)=>{
            return text + valueList.group+','+
                valueList.key+','+
                valueListItem.value.replace(/,/g,'‚')+','+
                !!valueListItem.custom+','+
                !!valueListItem.deleted+','+
                valueListItem.id+'||'
        },'')
        // for (const valueListItem of valueListItems) {
        //     // delete valueListItem
        //     const deleteParams = {
        //         Key:{
        //             id: valueListItem.id
        //         },
        //         TableName: process.env.VALUE_LIST_ITEM_TABLE,
        //     }
        //     // const deleteResult = await deleteDynamoRecord(deleteParams);
        //     console.log({
        //         [valueList.group+'/'+valueList.key]:{
        //             valueListItem,
        //             deleteParams: JSON.stringify(deleteParams),
        //             // deleteResult
        //         }
        //     })
        //     text += valueList.group+','+
        //             valueList.key+','+
        //             valueListItem.value.replace(/,/g,' ~')+','+
        //             !!valueListItem.custom+','+
        //             !!valueListItem.deleted+','+
        //             valueListItem.id+'||'
        // }
    }
    if(text) console.log({text})
    return text
}

async function handleValueList(valueLists){
    let text = ''
    if(valueLists.length){
        const queryParams = {
            ExpressionAttributeNames: {
                '#i': 'id',
                '#vlid': 'valueListItemValueListId',
                '#v': 'value',
                '#c': 'custom',
                '#d': 'deleted'
            },
            ExpressionAttributeValues: {
                ':c': true
            },
            TableName: process.env.VALUE_LIST_ITEM_TABLE,
            IndexName: 'gsi-ValueListItems',
            KeyConditionExpression: '#vlid = :vlid',
            FilterExpression: '#c = :c',
            ProjectionExpression: '#i, #v, #c, #d'
        }
        for (const valueList of valueLists) {
            queryParams.ExpressionAttributeValues[':vlid'] = valueList.id
            do{
                const valueListItemResult = await queryDynamoRecords(queryParams)
                const valueListItems = valueListItemResult.Items
                text += await handleValueListItem(valueList, valueListItems)
                queryParams.LastEvaluatedKey = valueListItemResult.LastEvaluatedKey
            }while(queryParams.LastEvaluatedKey)
        }
    }
    return text
}

async function getAllTenantCustomDropDown(){
    let text = 'Group,ValueList,ValueListItem,Custom,Deleted,Id||'
    const notAllowedCustomDropdowns = ['staff-status','roster-status','vehicle-status','device-status']
    const queryParams = {
        ExpressionAttributeNames: {
            '#i': 'id',
            '#g': 'group',
            '#k': 'key'
        },
        ExpressionAttributeValues: {},
        TableName: process.env.VALUE_LIST_TABLE,
        IndexName: 'byKey',
        KeyConditionExpression: '#k = :k',
        ProjectionExpression: '#i, #g, #k'
    }
    for (const key of notAllowedCustomDropdowns) {
        queryParams.ExpressionAttributeValues[':k'] = key
        do{
            const valueListResult = await queryDynamoRecords(queryParams)
            const str = await handleValueList(valueListResult.Items)
            text += str
            queryParams.LastEvaluatedKey = valueListResult.LastEvaluatedKey
        }while(queryParams.LastEvaluatedKey)
    }
    console.log({String: text})
}

module.exports = {
    getAllTenantCustomDropDown
}