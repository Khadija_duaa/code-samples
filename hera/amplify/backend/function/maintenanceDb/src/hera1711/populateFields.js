const { buildUpdateParams, updateDynamoRecord } = require("./../dynamodbHelper");

async function populateCompanyScorecard(companyScoreCardId, item) {
    try {
        let updateParams = buildUpdateParams(process.env.COMPANYSCORECARD_TABLE, item, companyScoreCardId)
        await updateDynamoRecord(updateParams);

    } catch (e) {
        console.log('Warning: failed to update scorecard', companyScoreCardId)
    }
}

async function populateTextractJob(textractJobId, item) {
    try {
        let updateParams = buildUpdateParams(process.env.TEXTRACTJOB_TABLE, item, textractJobId)
        await updateDynamoRecord(updateParams);

    } catch (e) {
        console.log('Warning: failed to update textractjob', textractJobId)
    }
}

module.exports = {
    populateCompanyScorecard,
    populateTextractJob
}
