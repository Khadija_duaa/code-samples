const { getMetricsFromPDF, getTextractJobByYearWeek } = require("./retrieveData");
const { populateCompanyScorecard, populateTextractJob } = require("./populateFields");
const YEAR = 2022
const WEEK = 44

async function runHera1711() {
    const jobs = await getTextractJobByYearWeek(YEAR, WEEK);

    const filtered = jobs.filter((job) => {
        let results = JSON.parse(job.results)
        let indexes = Object.keys(results.fields).length
        return indexes <= 7
    })

    if (!filtered.length) {
        console.log('There is not pending jobs.')
    }

    for (const job of filtered) {
        // 1. Get new metrics
        let key = job.key
        let fields = await getMetricsFromPDF(key)

        // 2. Re-populate CompanyScorecard table with new DSP metrics.
        let companyScoreCardId = job.textractJobTenantId + String(job.year) + String(job.week)
        let item = {
            ...fields
        }
        await populateCompanyScorecard(companyScoreCardId, item)

        // 3. Re-populate TextractJob table with correct values.
        let results = JSON.parse(job.results)
        results.fields = fields
        let textractJobItem = { results: JSON.stringify(results) }
        await populateTextractJob(job.id, textractJobItem)
        // process.exit(0)
    }
}

module.exports = {
    runHera1711,
};