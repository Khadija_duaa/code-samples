const { scanDynamoRecords } = require("./../dynamodbHelper");
const fs = require('fs')
const pdf = require('pdf-parse');
const { parseMetrics } = require('./parseMetrics')

// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });

async function getMetricsFromPDF(key) {
    let fieldsMetrics = {}
    let s3 = new AWS.S3();
    let params = {
        Bucket: process.env.BUCKET,
        Key: 'public/' + key
    };

    //Make unique filename to prevent using same file from tmp
    let filePathArray = key.split("/");
    let originalName = filePathArray[filePathArray.length - 1]
    let fileName = new Date().getTime().toString() + originalName
    let s3Stream = await s3.getObject(params).promise()

    fs.writeFileSync('/tmp/' + fileName, s3Stream.Body, async (err) => {
        if (err) throw err;
    });

    // parse file using pdf-parse      
    let dataBuffer = fs.readFileSync('/tmp/' + fileName);

    await pdf(dataBuffer, {}).then(async function (data) {

        await fs.unlink('/tmp/' + fileName, (err) => {
            if (err) throw err;
        });

        fieldsMetrics = await parseMetrics(data)

    }).catch(error => {
        throw error
    })

    return fieldsMetrics
}

async function getTextractJobByYearWeek(year, week) {
    try {
        let jobs = [];
        let lastEvaluatedKey = null;
        do {
            let params = {
                ExpressionAttributeNames: {
                    '#key': 'key',
                    '#group': 'group',
                    '#textractJobTenantId': 'textractJobTenantId',
                    '#year': 'year',
                    '#week': 'week',
                    '#jobStatus': 'jobStatus',
                    '#type': 'type',
                },
                ExpressionAttributeValues: {
                    ':year': year.toString(),
                    ':week': week.toString(),
                    ':flag': '_en_',
                    ':jobStatus': 'SUCCEEDED',
                    ':type': 'scorecard'
                },
                TableName: process.env.TEXTRACTJOB_TABLE,
                ProjectionExpression: "id, #key, #group, #year, #week, #jobStatus, results, #type, #textractJobTenantId",
                ExclusiveStartKey: lastEvaluatedKey,
                FilterExpression: ` #year = :year and 
                                    #week = :week and 
                                    contains(#key, :flag) and
                                    #jobStatus = :jobStatus and
                                    #type = :type`
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            jobs = [...jobs, ...items];
        } while (lastEvaluatedKey);

        return jobs;
    } catch (err) {
        console.log("[getTextractJobByYearWeek] Error in function getTextractJobByYearWeek", err);
        return {
            error: err,
        };
    }
}

module.exports = {
    getMetricsFromPDF,
    getTextractJobByYearWeek,
}