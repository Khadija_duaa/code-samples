/**
 * This script updates all Counseling Severity Options
 */
  
const {
  scanDynamoRecords,
  updateDynamoRecord,
  buildUpdateParams,
} = require("./dynamodbHelper");
    
/**
  * Main function
  */
async function handleHera3662() {
  try {
    // Get all Counseling Severity Options
    await loadAllSeverityOptions();
  } catch (error) {
    console.log(error);
  }
}
    
/**
  * Fetches all Options for Counseling Severity
  */
async function loadAllSeverityOptions() {
  let lastEvaluatedKey = null;
  let records = []
    do {
      try {
        const params = {
          ExpressionAttributeValues: {
            ':opt1': '1) Informal Warning',
            ':opt2': '2) Formal Verbal Warning',
            ':opt3': '3) Final Warning',
            ':opt4': '4) Dismissal',
          },
          ExpressionAttributeNames: {
            '#id': 'id',
            '#option': 'option'
          },
          FilterExpression: '#option in (:opt1, :opt2, :opt3, :opt4)',
          TableName: process.env.OPTIONS_CUSTOM_LIST_TABLE,
          ProjectionExpression: '#id, #option',
          ExclusiveStartKey: lastEvaluatedKey,
        };
        const scanResult = await scanDynamoRecords(params);
        lastEvaluatedKey = scanResult.LastEvaluatedKey;
        records = records.concat(scanResult.Items)
        console.log({CountKey:scanResult.Items.length,lastEvaluatedKey});
        await updateOptionsName(scanResult.Items)
      }
      catch (error) {
        console.log('[loadAllSeverityOptions] Error loading and update options', error);
      }
    }while (lastEvaluatedKey);  
  console.log({Count:records.length})
}
    
/**
  * Updates a Option Custom list record, sets OptionCustomList Id
  * @param {Array} options 
  */
async function updateOptionsName(options=[]){
  const reportUpdate = {
    sucess: 0,
    error: 0
  }
        
  for(const item of options){
    const optionName = removeDefaultNumbering(item.option)
    let updatedParams = {
      id: item.id,
      option: optionName
    };
          
    const buildParams = buildUpdateParams(process.env.OPTIONS_CUSTOM_LIST_TABLE, updatedParams, updatedParams.id);
    delete buildParams.ReturnValues;
          
    try {
      await updateDynamoRecord(buildParams);
      reportUpdate.sucess++
    } catch (error) {
      console.log("Error in function updateOptionsName", error);
      reportUpdate.error++
    }
  }
      
  console.log({reportUpdate})
}
    
function removeDefaultNumbering(option){
  const regex = /^\d+\)\s+/;
  return option.replace(regex, '');
}
    
module.exports = {
  handleHera3662,
};
    