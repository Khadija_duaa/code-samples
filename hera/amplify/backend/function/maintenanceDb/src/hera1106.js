const {
    queryDynamoRecords,
    scanDynamoRecords,
    updateDynamoRecord
} = require('./dynamodbHelper.js')

async function handleCounselingsThatHAveNoUserId(counselings, userId){
    for (const counseling of counselings) {

        const updateParams = {
            TableName: process.env.COUNSELING_TABLE,
            Key: {
                "id": counseling.id
            },
            ExpressionAttributeNames: {
                "#cuid": 'counselingUserId'
            },
            ExpressionAttributeValues: {
                ":cuid": userId
            },
            UpdateExpression: "set #cuid = :cuid",
            ReturnValues: "UPDATED_NEW"
        }

        console.log({
            counseling,
            OwnerUserId: userId,
            updateParams
        })
        
        // const response = await updateDynamoRecord(updateParams)
        // console.log({response})
        // process.exit(0)
    }
}

async function getCounselingsByGroup(user){
    const group = (user.group||[]).find(g=>!g.includes('system_admin'))
    if(!group)
        return
    try {
        const queryParams = {
            ExpressionAttributeNames: {
                '#i': 'id',
                '#g': 'group',
                '#cuid': 'counselingUserId'
            },
            ExpressionAttributeValues: {
                ':g': group
            },
            TableName: process.env.COUNSELING_TABLE,
            IndexName: 'byGroup',
            KeyConditionExpression: '#g = :g',
            ProjectionExpression: '#i,#cuid'
        }
        do {
            const queryResult = await queryDynamoRecords(queryParams)
            const filteredCounselings = queryResult.Items.filter(counseling=>!counseling.counselingUserId)
            await handleCounselingsThatHAveNoUserId(filteredCounselings, user.id)
            queryParams.ExclusiveStartKey = queryResult.LastEvaluatedKey
        } while (queryParams.ExclusiveStartKey )
    } catch (err) {
        console.log('[getCounselingsByGroup] Error in function', err)
        return {
            error: err
        }
    }
}

async function iterateUsers(users){
    for (const user of users) {
        if(user.isOwner){
            await getCounselingsByGroup(user)
        }
    }
}

async function handleAllUsers() {
    try {
        const params = {
            ExpressionAttributeNames: {
                '#g': 'group',
                '#o': 'isOwner'
            },
            TableName: process.env.USER_TABLE,
            ProjectionExpression:
            'id, #g, #o'
        }
        do {
            const scanResult = await scanDynamoRecords(params)
            await iterateUsers(scanResult.Items)
            params.ExclusiveStartKey = scanResult.LastEvaluatedKey
        } while (params.ExclusiveStartKey)
    } catch (err) {
        console.log('[handleAllUsers] Error in function', err)
        return {
            error: err
        }
    }
}

async function handleHera1106(){
    await handleAllUsers()
}

module.exports = {
    handleHera1106
}