/**
 * This script updates the 'date' fields that have 'T00:00:00' saved on Infraction and Kudo tables
 * and sets the correct offset to the timestamps according to the Tenant's timezone
 */
const moment = require('moment-timezone');
const {
  scanDynamoRecords,
  updateDynamoRecord,
} = require("./dynamodbHelper");

const tenantsByGroup = {};

/**
 * Main function
 */
async function handleHera3112() {
  try {
    await loadAllTenantsIntoMap();
    // Print Report
    await handlePrintReport();
    // Link counselings to Severity Options Custom Lists
    // await handleUpdateRecords();
  } catch (error) {
    console.log(error);
  }
}

/**
 * Handles printing the report of the changes that this script will cause
 */
async function handlePrintReport() {
  const reportObj = {
    toBeUpdated: {
      infractions: {count: 0, items: []},
      kudos: {count: 0, items: []},
    },
    toBeSkipped: {
      infractions: {count: 0, items: []},
      kudos: {count: 0, items: []},
    },
  };
  const tableNames = {
    [process.env.INFRACTION_TABLE]: 'infractions',
    [process.env.KUDO_TABLE]: 'kudos',
  };
  const tables = [process.env.INFRACTION_TABLE, process.env.KUDO_TABLE];
  for (const table of tables) {
    await applyCallbackToAllRecords(table, async (recordList) => {
      for (const record of recordList) {
        const newTimestamp = await getUpdatedTimestamp(record.group, record.date);
        const tableName = tableNames[table];
        if (!newTimestamp) {
          reportObj.toBeSkipped[tableName].count += 1;
          reportObj.toBeSkipped[tableName].items.push({...record});
          continue;
        }
        reportObj.toBeUpdated[tableName].count += 1;
        reportObj.toBeUpdated[tableName].items.push({...record, newDate: newTimestamp});        
      }
    });
  }
  console.log('Report Summary', JSON.stringify({
    toBeUpdated: {
      infractions: reportObj.toBeUpdated.infractions.count,
      kudos: reportObj.toBeUpdated.kudos.count,
    },
    toBeSkipped: {
      infractions: reportObj.toBeSkipped.infractions.count,
      kudos: reportObj.toBeSkipped.kudos.count,
    },
  }, null, 2));
  console.log('Full Report', JSON.stringify(reportObj, null, 2));
}

/**
 * Handles the update of Infraction and Kudo records
 */
async function handleUpdateRecords() {
  const tables = [process.env.INFRACTION_TABLE, process.env.KUDO_TABLE];
  for (const table of tables) {
    await applyCallbackToAllRecords(table, async (recordList) => {
      const promises = recordList.map(async (record) => {
        const newTimestamp = await getUpdatedTimestamp(record.group, record.date);
        if (!newTimestamp) return Promise.resolve();
        return (await updateRecord(table, record, newTimestamp));
      });
      await Promise.all(promises);
    });
  }
}

/**
 * Generates the timestamp of a date for a Tenant timezone
 * @param {String} group - Group of the Tenant to get it's timezone 
 * @param {String} date - Timestamp of the record
 * @returns {String|null} Timestamp in ISO format or null if invalid timezone is found
 */
async function getUpdatedTimestamp(group, date) {
  const timezone = tenantsByGroup[group]?.timeZone;
  if (!timezone) return null;
  const isValidTimezone = !!moment.tz.zone(timezone);
  if (!isValidTimezone) return null;
  const dateStr = date.split('T')[0];
  return moment.tz(dateStr, timezone).toDate().toISOString();
}

/**
 * Updates the specified record
 * @param {String} table - Table name to which to make the query to
 * @param {Object} record - Record object which will be updated
 * @param {String} newTimestamp - Updated timestamp in Tenant's timezone
 */
async function updateRecord(table, record, newTimestamp) {
  try {
    const params = {
      TableName: table,
      Key: { id: record.id },
      ExpressionAttributeValues: { ":date": newTimestamp },
      ExpressionAttributeNames: { '#date': 'date' },
      UpdateExpression: "set #date = :date",
      ReturnValues: "UPDATED_NEW",
    };
    await updateDynamoRecord(params);
  }
  catch (error) {
    console.log(`Error updating record [ID=${record.id}]`);
  }
}

/**
 * Applies a callback to a list of Records
 * @param {String} table - Table name to which to make the query
 * @param {Function} cb - Callback to execute for each group of records retrieved from DynamoDB
 */
async function applyCallbackToAllRecords(table, cb) {
  let lastEvaluatedKey = null;
  try {
    do {
      const params = {
        ExpressionAttributeValues: {
          ':dateFilter': 'T00:00:00',
        },
        ExpressionAttributeNames: {
          '#id': 'id',
          '#group': 'group',
          '#date': 'date',
        },
        TableName: table,
        ProjectionExpression: '#id, #date, #group',
        FilterExpression: 'contains (#date, :dateFilter)',
        ExclusiveStartKey: lastEvaluatedKey,
      };
      const scanResult = await scanDynamoRecords(params);
      await cb(scanResult.Items);
      lastEvaluatedKey = scanResult.LastEvaluatedKey;
      console.log({lastEvaluatedKey});
    }
    while (lastEvaluatedKey);
  }
  catch (error) {
    console.log("[applyCallbackToAllRecords] Error", error);
  }
}

/**
 * Fetches all Tenants
 */
async function loadAllTenantsIntoMap() {
  let lastEvaluatedKey = null;
  do {
    try {
      const params = {
        ExpressionAttributeNames: {
          '#id': 'id',
          '#group': 'group',
          '#timeZone': 'timeZone',
        },
        TableName: process.env.TENANT_TABLE,
        ProjectionExpression: '#id, #group, #timeZone',
        ExclusiveStartKey: lastEvaluatedKey,
      };
      const scanResult = await scanDynamoRecords(params);
      // Load Severity Options into global map
      for (const item of scanResult.Items) {
        tenantsByGroup[item.group] = {...item};
      }
      lastEvaluatedKey = scanResult.LastEvaluatedKey;
    }
    catch (error) {
      console.log('[loadAllTenantsIntoMap] Error loading options', error);
    }
  }
  while (lastEvaluatedKey);
}

module.exports = {
  handleHera3112,
};
