/* eslint-disable no-console */
require("es6-promise").polyfill();
require("isomorphic-fetch");


// eslint-disable-next-line import/no-extraneous-dependencies
const AWS = require("aws-sdk");
const AWSAppSyncClient = require("aws-appsync").default;
const { AUTH_TYPE } = require("aws-appsync");

const region = process.env.REGION;
AWS.config.update({
  region
});
const appsyncUrl = process.env.API_HERA_GRAPHQLAPIENDPOINTOUTPUT;
const gql = require("graphql-tag");

// graphql client.  We define it outside of the lambda function in order for it to be reused during subsequent calls
let client;


// initializes our graphql client
function initializeClient() {
  client = new AWSAppSyncClient({
    url: appsyncUrl,
    region,
    auth: {
      type: AUTH_TYPE.AWS_IAM,
      credentials: AWS.config.credentials
    },
    disableOffline: true
  });
}

// generic mutation function.  A way to quickly reuse mutation statements
async function executeMutation(mutation, variables, silent) {
  if (!client) {
    initializeClient();
  }

  try {
    // console.log(gql(mutation));
    // console.log(variables);
    const response = await client.mutate({
      mutation: gql(mutation),
      variables,
      fetchPolicy: "no-cache"
    });
    return response.data;
  } catch (err) {
    if(!silent){
      console.log("Error while trying to mutate data");
      console.log(err);
    }
    throw JSON.stringify(err);
  }
}

// generic query function.  A way to quickly reuse query statements
async function executeQuery(query, variables) {
  if (!client) {
    initializeClient();
  }
  try {
    // console.log(gql(query));
    const response = await client.query({
      query: gql(query),
      variables,
      fetchPolicy: "network-only"
    });
    return response.data;
  } catch (err) {
    console.log("Error while trying to fetch data");
    throw JSON.stringify(err);
  }
}

module.exports = {
    executeMutation,
    executeQuery
};