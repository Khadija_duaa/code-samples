const { buildUpdateParams, scanDynamoRecords, updateDynamoRecord } = require("./dynamodbHelper.js");

async function runHera978() {
    var tenants = await getAllTenants();
    for (const tenant of tenants) {
        if (tenant.originationNumber === "+11111111111" && tenant.messageServiceProvider === "None") {
            let updateParams = buildUpdateParams(process.env.TENANT_TABLE, { originationNumber: "" }, tenant.id);
            await updateDynamoRecord(updateParams)
        }
    }
    return true;
}

async function getAllTenants() {
    try {
        let tenants = [];
        let lastEvaluatedKey = null;
        do {
            let params = {
                ExpressionAttributeNames: {
                    "#group": "group",
                },
                TableName: process.env.TENANT_TABLE,
                ProjectionExpression: "id, companyName,  #group, messageServiceProvider, originationNumber",
                ExclusiveStartKey: lastEvaluatedKey,
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            tenants = [...tenants, ...items];
        } while (lastEvaluatedKey);

        return tenants;
    } catch (err) {
        console.log("[getAllTenants] Error in function getAllTenants", err);
        return {
            error: err,
        };
    }
}

module.exports = {
    runHera978,
};
