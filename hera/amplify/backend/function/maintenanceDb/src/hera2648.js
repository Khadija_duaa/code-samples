const { scanDynamoRecords, updateDynamoRecord, queryDynamoRecords } = require("./dynamodbHelper.js");

async function runHera2648() {
    await updateTenantsAndUsers()
}

async function updateTenantsAndUsers() {

    let lastEvaluatedKey = null;

    try {
        do {
            const params = {
                ExpressionAttributeNames: {
                    "#group": "group",
                },
                TableName: process.env.TENANT_TABLE,
                ProjectionExpression: "id, #group",
                ExclusiveStartKey: lastEvaluatedKey,

            };

            const scanResult = await scanDynamoRecords(params);
            const tenants = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            console.log('lastEvaluatedKey:', lastEvaluatedKey)

            for (const tenant of tenants) {
                await updateTenant(tenant)
                const users = await getAllUsers(tenant.id)
                for (const user of users) {
                    try {
                        await updateUser(user)
                    } catch (err) {
                        console.log(err)
                    }
                }

            }

        } while (lastEvaluatedKey);

        return 'Script complete';

    } catch (err) {
        console.log("[updateTenantsAndUsers] Error in function updateTenantsAndUsers", err);
        return {
            error: err,
        };
    }
}

async function getAllUsers(tenantId) {
    const users = [];
    let lastEvaluatedKey = null;

    try {
        do {
            const params = {
                ExpressionAttributeNames: {
                    '#id': 'id',
                    '#g': 'group',
                    '#pfa': 'permissionFullAccess',
                    '#userTenantId': 'userTenantId'
                },
                ExpressionAttributeValues: {
                    ':userTenantId': tenantId,
                },
                TableName: process.env.USER_TABLE,
                IndexName: 'gsi-TenantUsers',
                KeyConditionExpression: '#userTenantId = :userTenantId',
                ProjectionExpression: '#id, #pfa, #g',

            };

            const queryResult = await queryDynamoRecords(params);
            const items = queryResult.Items;
            lastEvaluatedKey = queryResult.LastEvaluatedKey;
            users.push(...items);

        } while (lastEvaluatedKey);
        return users;
    } catch (err) {
        console.log("[getAllUsers] Error in function getAllUsers", err);
        return {
            error: err,
        };
    }
}

async function updateTenant(tenant) {
    const { id } = tenant
    const allowLibraryUpload = false
    const params = {
        TableName: process.env.TENANT_TABLE,
        Key: { 'id': id },
        ExpressionAttributeValues: {
            ":allowLibraryUpload": allowLibraryUpload
        },
        ExpressionAttributeNames: {
            "#allowLibraryUpload": "allowLibraryUpload"
        },
        UpdateExpression: "set #allowLibraryUpload = :allowLibraryUpload",
        ReturnValues: "UPDATED_NEW",
    }

    try {
        await updateDynamoRecord(params);
    } catch (e) {

        console.log("[updateTenant] Error updating updateTenant", e);
        return {
            error: e,
        };

    }
}

async function updateUser(user) {
    const { id, permissionFullAccess } = user
    const permissionVehiclePhotoLogs = (permissionFullAccess) ? true : false
    const params = {
        TableName: process.env.USER_TABLE,
        Key: { 'id': id },
        ExpressionAttributeValues: {
            ":permissionVehiclePhotoLogs": permissionVehiclePhotoLogs
        },
        ExpressionAttributeNames: {
            "#permissionVehiclePhotoLogs": "permissionVehiclePhotoLogs"
        },
        UpdateExpression: "set #permissionVehiclePhotoLogs = :permissionVehiclePhotoLogs",
        ReturnValues: "UPDATED_NEW",
    }

    try {
        await updateDynamoRecord(params);
    } catch (e) {
        console.log("[updateUser] Error updating updateUser", e);
        return {
            error: e,
        };
    }
}

module.exports = {
    runHera2648,
};
