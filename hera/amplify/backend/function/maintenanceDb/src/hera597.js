const {scanDynamoRecords, buildUpdateParams, updateDynamoRecord, createDynamoRecord} = require("./dynamodbHelper");

async function handleHera597 () {
    //  Set Custom List
    await setCounselingSeverityCustomListToTenant()

    //  Set Options Custom List
    await setOptionsToCounselingSeverityCustomList()
}

async function setOptionsToCounselingSeverityCustomList() {
    try {
        let customLists = await getAllCounselingSeverityCustomList()
        for (const customList of customLists) {
            for (const option of optionsCustomList) {
                let createOptionCustomList = {
                    TableName: process.env.OPTIONS_CUSTOM_LIST_TABLE,
                    Item: {
                        id: generateUUID(),
                        order: option.order,
                        option: option.option,
                        default: option.default,
                        usedFor: option.usedFor,
                        daysCount: option.daysCount,
                        canBeEdited: option.canBeEdited,
                        canBeDeleted: option.canBeDeleted,
                        canBeReorder: option.canBeReorder,
                        group: customList.group,
                        createdAt: new Date().toISOString(),
                        updatedAt: new Date().toISOString(),
                        optionsCustomListsCustomListsId: customList.id
                    }
                };
                await createDynamoRecord(createOptionCustomList);
            }
        }
        return true;
    } catch (error) {
        console.log("Error pe: ", error);
    }
}

async function getAllCounselingSeverityCustomList() {
    try {
        let customLists = []
        let lastEvaluatedKey = null
        do {
            let params = {
                ExpressionAttributeNames: {
                    '#group': 'group',
                    '#type': 'type',
                },
                ExpressionAttributeValues: {
                    ':type': 'counseling-severity'
                },
                FilterExpression: "#type = :type",
                TableName: process.env.CUSTOM_LIST_TABLE,
                ProjectionExpression: 'id, #group, #type',
                ExclusiveStartKey: lastEvaluatedKey
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items
            lastEvaluatedKey = scanResult.LastEvaluatedKey
            customLists = [...customLists, ...items]
        } while (lastEvaluatedKey)
        return customLists
    } catch (err) {
        console.log("[getAllCounselingSeverityCustomList] Error in function getAllCounselingSeverityCustomList", err);
        return {
            error: err,
        };
    }
}

async function setCounselingSeverityCustomListToTenant() {
    try {
        let tenants = await getAllTenants()
        for (const tenant of tenants) {
            let createCustomList = {
                TableName: process.env.CUSTOM_LIST_TABLE,
                Item: {
                    id: generateUUID(),
                    type: 'counseling-severity',
                    listCategory: 'Associates',
                    listName: 'Counseling Severity',
                    listDisplay: 'Text Only',
                    group: tenant.group,
                    createdAt: new Date().toISOString(),
                    updatedAt: new Date().toISOString()
                }
            };
            await createDynamoRecord(createCustomList);
        }
        return true;
    } catch (error) {
        console.log("Error pe: ", error);
    }
}

async function getAllTenants() {
    try {
       let tenants = []
        let lastEvaluatedKey = null
        do {
            let params = {
                ExpressionAttributeNames: {
                    '#group': 'group',
                },
                TableName: process.env.TENANT_TABLE,
                ProjectionExpression: 'id, #group, CustomLists',
                ExclusiveStartKey: lastEvaluatedKey
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items
            lastEvaluatedKey = scanResult.LastEvaluatedKey
            tenants = [...tenants, ...items]
        } while (lastEvaluatedKey)
        return tenants
    } catch (err) {
        console.log("[getAllTenants] Error in function getAllTenants", err);
        return {
            error: err,
        };
    }
}

function generateUUID() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

const optionsCustomList = [
    {
        order: 1,
        option: '1) Informal Warning',
        default: true,
        usedFor: 0,
        daysCount: 0,
        canBeEdited: true,
        canBeDeleted: true,
        canBeReorder: true
    },
    {
        order: 2,
        option: '2) Formal Verbal Warning',
        default: false,
        usedFor: 0,
        daysCount: 0,
        canBeEdited: true,
        canBeDeleted: true,
        canBeReorder: true
    },
    {
        order: 3,
        option: '3) Final Warning',
        default: false,
        usedFor: 0,
        daysCount: 0,
        canBeEdited: true,
        canBeDeleted: true,
        canBeReorder: true
    },
    {
        order: 4,
        option: '4) Dismissal',
        default: false,
        usedFor: 0,
        daysCount: 0,
        canBeEdited: true,
        canBeDeleted: true,
        canBeReorder: true
    }
]

module.exports = {
    handleHera597
}