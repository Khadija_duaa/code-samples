const { scanDynamoRecords, updateDynamoRecord } = require("./dynamodbHelper.js");
var AWS = require("aws-sdk");

async function runHera2351() {
    let users = await getAllUsers();
    for (let i in users) {
        await updateUser(users[i].id)
    }
}

async function getAllUsers() {
    const users = [];
    let lastEvaluatedKey = null;

    try {
        do {
            const params = {
                ExpressionAttributeNames: {
                    "#group": "group",
                },
                TableName: process.env.USER_TABLE,
                ProjectionExpression: "id, #group",
                ExclusiveStartKey: lastEvaluatedKey,
            };

            const scanResult = await scanDynamoRecords(params);
            const items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            console.log('lastEvaluatedKey:', lastEvaluatedKey)
            users.push(...items);
        } while (lastEvaluatedKey);

        return users;
    } catch (err) {
        console.log("[getAllUsers] Error in function getAllUsers", err);
        return {
            error: err,
        };
    }
}

async function updateUser(id) {
    try {
        await updateDynamoRecord({
            TableName: process.env.USER_TABLE,
            Key: { 'id': id },
            ExpressionAttributeValues: { 
                ":permissionTasksReports": true, 
                ":permissionVehicleManagement": true 
            },
            ExpressionAttributeNames: { 
                "#permissionTasksReports": "permissionTasksReports", 
                "#permissionVehicleManagement": "permissionVehicleManagement" 
            },
            UpdateExpression: "set #permissionTasksReports = :permissionTasksReports, #permissionVehicleManagement = :permissionVehicleManagement",
            ReturnValues: "UPDATED_NEW",
        });
    } catch (e) {
        console.log("[updateUser] Error updating updateUser", e);
        return {
            error: e,
        };
    }
}

module.exports = {
    runHera2351,
};
