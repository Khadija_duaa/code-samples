const {
    scanDynamoRecords,
    createDynamoRecord
} = require("./dynamodbHelper");
var { nanoid } = require('nanoid')

async function runHera1177 () {
    //  Create Parking Space Custom List
    let tenants = await getAllTenants()
    // console.log({ tenants });
    await createParkingSpaceCustomList(tenants)
}

async function createParkingSpaceCustomList(tenants) {
    for (const tenant of tenants) {
        try {
            let createCustomList = {
                TableName: process.env.CUSTOM_LIST_TABLE,
                Item: {
                    id: nanoid(36),
                    type: 'parking-space',
                    listCategory: 'Daily Rostering',
                    listName: 'Parking Space',
                    listDisplay: 'Text Only',
                    group: tenant.group,
                    canDeleteAllOptions: true,
                    createdAt: new Date().toISOString(),
                    updatedAt: new Date().toISOString()

                }
            };
            console.log({ createCustomList  });
            // await createDynamoRecord(createCustomList);
        } catch (error) {
            console.log("[createParkingSPaceCustomList] Error in function createParkingSPaceCustomList", error);
        }
    }
}

async function getAllTenants() {
    try {
       let tenants = []
        let lastEvaluatedKey = null
        do {
            let params = {
                ExpressionAttributeNames: {
                    '#group': 'group',
                },
                TableName: process.env.TENANT_TABLE,
                ProjectionExpression: 'id, #group',
                ExclusiveStartKey: lastEvaluatedKey
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items
            lastEvaluatedKey = scanResult.LastEvaluatedKey
            tenants = [...tenants, ...items]
        } while (lastEvaluatedKey)
        return tenants
    } catch (err) {
        console.log("[getAllTenants] Error in function getAllTenants", err);
        return {
            error: err,
        };
    }
}

module.exports = {
    runHera1177
}