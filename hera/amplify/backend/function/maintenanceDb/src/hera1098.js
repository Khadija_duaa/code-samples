const { scanDynamoRecords, updateDynamoRecord } = require("./dynamodbHelper.js");

async function runHera1098() {
    var staffs = await getAllStaffs();
    var list = await cleanPhones(staffs);
    await updatesPhones(list);
    return true;
}

async function getAllStaffs() {
    try {
        let staffs = [];
        let lastEvaluatedKey = null;
        do {
            let params = {
                TableName: process.env.STAFF_TABLE,
                ProjectionExpression: "id, phone ",
                ExclusiveStartKey: lastEvaluatedKey,
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            staffs = [...staffs, ...items];
        } while (lastEvaluatedKey);

        return staffs;
    } catch (err) {
        console.log("[getAllStaffs] Error in function getAllStaffs", err);
        return {
            error: err,
        };
    }
}

async function cleanPhones(list) {
    var cleanedList = [];
     for (const staff of list) {
        if(staff.phone && (staff.phone.trim().startsWith('(1') || staff.phone.trim()=='(')){
            cleanedList.push(staff);
        }
    }
    return cleanedList;
}

async function updatesPhones(list) {
    try {
        for (let item of list) {
            await updateDynamoRecord({
                TableName: process.env.STAFF_TABLE,
                Key: {"id": item.id},
                ExpressionAttributeValues: {":phone": ''},
                ExpressionAttributeNames: {"#phone": 'phone'},
                UpdateExpression: "set #phone = :phone",
                ReturnValues: "UPDATED_NEW"
            })
        }
    } catch (err) {
        console.log("[hera-1098] Error updating phones", err);
        return {
            error: err,
        };
    }
}

module.exports = {
    runHera1098,
};