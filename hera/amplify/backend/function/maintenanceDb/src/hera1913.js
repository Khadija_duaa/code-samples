const { buildUpdateParams, scanDynamoRecords, updateDynamoRecord } = require("./dynamodbHelper.js");

async function runHera1913() {
    var vehicles = await getAllVehicles();
    // console.log('Total:',vehicles.length)
    // console.log(vehicles)
    // await cleanEmptyStatus(vehicles);
}

async function getAllVehicles() {
    try {
        let vehicles = [];
        let lastEvaluatedKey = null;
        do {
            let params = {
                ExpressionAttributeNames: {
                    "#g": "group",
                    "#s": "status",
                },
                TableName: process.env.VEHICLE_TABLE,
                ProjectionExpression: "id, #s, #g",
                ExclusiveStartKey: lastEvaluatedKey,
                FilterExpression: "attribute_not_exists(#s)",
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            vehicles = [...vehicles, ...items];
        } while (lastEvaluatedKey);

        return vehicles;
    } catch (err) {
        console.log("[getAllVehicles] Error in function getAllVehicles", err);
        return {
            error: err,
        };
    }
}


async function cleanEmptyStatus(list){

    for (const vehicle of list) {
        try {
            console.log(vehicle)
            await updateDynamoRecord({
                TableName: process.env.VEHICLE_TABLE,
                Key: {"id": vehicle.id},
                ExpressionAttributeValues: {":i": 'Inactive-Archived'},
                ExpressionAttributeNames: {"#s": 'status'},
                UpdateExpression: "set #s = :i",
                ReturnValues: "UPDATED_NEW"
            })
            console.log("success Vehicle id:", vehicle.id)
            // return
        } catch (err) {
            console.log(`[hera-1913 cleanEmptyStatus] Error Updating Vehicle status with id: ${vehicle.id} `, err);
            return {
                error: err,
            };
        }
        
    }
}

function getUpdateExpression(vehicle){
    let UpdateExpression = ''
    const ExpressionAttributeNames = { '#vin': 'vin'}
    const removeArr = [];
    const GSIs = ['licensePlate','licensePlateExp', 'vin', 'status'];
    GSIs.forEach(field => {
        if (!vehicle[field]) {
            const key = `#${field}`;
            ExpressionAttributeNames[key] = field;
            removeArr.push(key);
        }
    });
    
    if (removeArr.length > 0) {
        const removeExpr = ` remove ${removeArr.join(', ')}`;
        UpdateExpression += removeExpr;
    }
    
    //console.log(ExpressionAttributeNames)
    //console.log(UpdateExpression)
    
    return {
        ExpressionAttributeNames,
        UpdateExpression
    }
}

module.exports = {
    runHera1913,
};