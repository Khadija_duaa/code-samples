/**
 * THIS SCRIPT UPDATES 'status' FIELD ON "COUNSELING TABLE" FOLLOWING THESE RULES:
 *  1.) NOT_SENT          -> if status = 'Sent' and dateSent is empty
 *  2.) PENDING_SIGNATURE -> if status = 'Sent' and dateSent is not empty
 *  3.) NOT_SENT          -> if status = 'Pending' and dateSent is empty
 *  4.) PENDING_SIGNATURE -> if status = 'Pending' and dateSent is not empty
 *  5.) SIGNED            -> if status = 'Pending Signature' and dateSigned is not empty
 * 
 * ADDITIONALY UPDATES 'status' FIELD VALUE USING THESE RULES:
 *  Signed            -> SIGNED
 *  Not Sent          -> NOT_SENT
 *  Refused To Sign   -> REFUSED_TO_SIGN
 *  Pending Signature -> PENDING_SIGNATURE
 *
 * FINALLY IT REMOVES "empty" FIELDS THAT ARE SETUP AS GSIs
 */

const {
  scanDynamoRecords,
  updateDynamoRecord,
} = require("./dynamodbHelper.js");

const COUNSELING_STATUS_NOT_SENT = 'NOT_SENT';
const COUNSELING_STATUS_PENDING_SIGNATURE = 'PENDING_SIGNATURE';
const COUNSELING_STATUS_REFUSED_TO_SIGN = 'REFUSED_TO_SIGN';
const COUNSELING_STATUS_SIGNED = 'SIGNED';

/**
 * Main function
 */
async function handleHera1823() {
  // PRINT REPORT BEFORE UPDATING
  await printReport();
  // await scanAndApplyCallbackToAllCounselingRecords(updateCounselingStatus);
}

/**
 * Print a report indicating which records will be updated to wich 'status' value
 */
async function printReport() {
  const reportObj = {
    count: 0,
    status: {
      SIGNED: { count: 0, items: [] },
      NOT_SENT: {count: 0, items: []},
      PENDING_SIGNATURE: {count: 0, items: []},
      REFUSED_TO_SIGN: {count: 0, items: []},
    }
  };

  await scanAndApplyCallbackToAllCounselingRecords(counselingList => {
    reportObj.count += counselingList.length || 0;

    counselingList = counselingList.forEach(counseling => {
      const newStatus = getValueForCounselingStatus(counseling);
      
      if (!reportObj.status[newStatus]) {
        reportObj.status[newStatus] = { count: 0, items: [] };
      }

      reportObj.status[newStatus].count += 1;
      reportObj.status[newStatus].items.push({ ...counseling });
    });
  });

  /** PRINT REPORT */
  const reportAsJson = JSON.stringify(reportObj);
  console.log(reportAsJson);
}

/**
 * Scans all dynamo Counseling records and executes a callback passing
 * the returned result Items as param
 * @param {Function} cb: callback to execute passing the returned Items as param
 */
async function scanAndApplyCallbackToAllCounselingRecords(cb) {
  let lastEvaluatedKey = null;
  try {
    do {
      let params = {
        TableName: process.env.COUNSELING_TABLE,
        ProjectionExpression: "#id, #status, #date, #dateSent, #dateSigned",
        ExpressionAttributeNames: {
          "#id": "id",
          "#status": "status",
          "#date": "date",
          "#dateSent": "dateSent",
          "#dateSigned": "dateSigned",
        },
        ExclusiveStartKey: lastEvaluatedKey,
      };
      let scanResult = await scanDynamoRecords(params);
      lastEvaluatedKey = scanResult.LastEvaluatedKey;

      await cb(scanResult.Items);

    } while(lastEvaluatedKey);
  } catch (error) {
    console.log("[HERA-1823 ] Error in function scanAndApplyCallbackToAllCounselingRecords", error);
  }
}

/**
 * Maps a counseling 'status' to it's corresponding value
 * @param {Object} counseling 
 * @returns {String} representation of counseling status value
 */
function getValueForCounselingStatus(counseling) {
  const { status, dateSent, dateSigned } = counseling;

  // Apply first set of rules
  if (status === 'Sent' && !dateSent) return COUNSELING_STATUS_NOT_SENT;
  if (status === 'Sent' && dateSent) return COUNSELING_STATUS_PENDING_SIGNATURE;
  if (status === 'Pending' && !dateSent) return COUNSELING_STATUS_NOT_SENT;
  if (status === 'Pending' && dateSent) return COUNSELING_STATUS_PENDING_SIGNATURE;
  if (status === 'Pending Signature' && dateSigned) return COUNSELING_STATUS_SIGNED;

  // For counselings with empty status
  if (!status && !dateSent) return COUNSELING_STATUS_NOT_SENT;
  if (!status && !dateSigned) return COUNSELING_STATUS_PENDING_SIGNATURE;

  // Transform Remaining values
  const statusDictionary = {
    'Signed': COUNSELING_STATUS_SIGNED,
    'Not Sent': COUNSELING_STATUS_NOT_SENT,
    'Refused To Sign': COUNSELING_STATUS_REFUSED_TO_SIGN,
    'Pending Signature': COUNSELING_STATUS_PENDING_SIGNATURE,
  };

  const mappedStatus = statusDictionary[status];
  if (mappedStatus) return mappedStatus;

  return status;
}

/**
 * Generates Update params for each Counseling record
 * @param {Object} counseling
 * @returns {Object} attributes to be used for record update
 */
function getUpdateParams(counseling) {
  const ExpressionAttributeValues = {":newStatus": getValueForCounselingStatus(counseling)};
  const ExpressionAttributeNames = {"#status": "status"};
  let UpdateExpression = "set #status = :newStatus";

  const removeArr = [];
  const GSIs = ['date', 'dateSigned'];
  GSIs.forEach(field => {
    if (!counseling[field]) {
      const key = `#${field}`;
      ExpressionAttributeNames[key] = field;
      removeArr.push(key);
    }
  });

  if (removeArr.length > 0) {
    const removeExpr = ` remove ${removeArr.join(', ')}`;
    UpdateExpression += removeExpr;
  }

  return {
    ExpressionAttributeValues,
    ExpressionAttributeNames,
    UpdateExpression
  }
}

/**
 * Updates Cousenling records 'status' field to it's corresponding value
 * @param {Array} counselingList: List of Counseling records
 */
async function updateCounselingStatus(counselingList) {
  for (let counseling of counselingList) {
    const {
      ExpressionAttributeValues,
      ExpressionAttributeNames,
      UpdateExpression,
    } = getUpdateParams(counseling);

    try {
      await updateDynamoRecord({
        TableName: process.env.COUNSELING_TABLE,
        Key: { id: counseling.id },
        ExpressionAttributeValues,
        ExpressionAttributeNames,
        UpdateExpression,
      });
    } catch (error) {
      console.log(`[HERA-1823] Error updating counseling 'status' (ID:${counseling.id})`, error);
    }
  }
}

module.exports = { handleHera1823 };
