const { buildUpdateParams, scanDynamoRecords, updateDynamoRecord } = require("../dynamodbHelper.js");

async function runHera1116Counseling() {
    const counselings = await getAllCounselings();
    console.log(JSON.stringify(counselings));
    //cleanEmails
    var counselingstoClean = await cleanCounselings(counselings);
    
    console.log(JSON.stringify(counselingstoClean));
    
}

async function getAllCounselings() {
    try {
        let counselings = [];
        let lastEvaluatedKey = null;
        do {
            let params = {   
                TableName: process.env.COUNSELING_TABLE,
                ProjectionExpression: "id, consequencesOfFailure, correctiveActionSummary, counselingNotes, signature",
                ExclusiveStartKey: lastEvaluatedKey,
                
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            counselings = [...counselings, ...items];
        } while (lastEvaluatedKey);

        return counselings;
    } catch (err) {
        console.log("[getAllCounselings] Error in function getAllCounselings", err);
        return {
            error: err,
        };
    }
}

async function cleanCounselings(list){
    
    const  counselingstoClean = [];
 
    for(const counseling of list) {
        if (counseling.consequencesOfFailure) counseling.consequencesOfFailure = counseling.consequencesOfFailure.trim();
        if (counseling.correctiveActionSummary) counseling.correctiveActionSummary = counseling.correctiveActionSummary.trim();
        if (counseling.counselingNotes) counseling.counselingNotes = counseling.counselingNotes.trim();
        if (counseling.signature) counseling.signature = counseling.signature.trim();

        counselingstoClean.push(counseling)
        //updateCounselingDynamo(counseling)
    }
    
    return counselingstoClean;
}

async function updateCounselingDynamo(counseling){
    try {
        
        await updateDynamoRecord({
            TableName: process.env.COUNSELING_TABLE,
            Key: {"id": counseling.id},
            ExpressionAttributeValues: {
                ":consequencesOfFailure": counseling.consequencesOfFailure,
                ":correctiveActionSummary": counseling.correctiveActionSummary,
                ":counselingNotes": counseling.counselingNotes,
                ":signature": counseling.signature
            },
            ExpressionAttributeNames: {
                "#consequencesOfFailure": 'consequencesOfFailure',
                "#correctiveActionSummary" : 'correctiveActionSummary',
                "#counselingNotes": 'counselingNotes',
                "#signature": 'signature'
            },
            UpdateExpression: "set #consequencesOfFailure = :consequencesOfFailure, #correctiveActionSummary = :correctiveActionSummary, #counselingNotes = :counselingNotes, #signature = :signature",
            ReturnValues: "UPDATED_NEW"
        })
            
        
        
    } catch (err) {
        console.log("[hera-1116] Error updating counseling", err);
        return {
            error: err,
        };
    }
}



module.exports = {
    runHera1116Counseling,
};