const { buildUpdateParams, scanDynamoRecords, updateDynamoRecord } = require("../dynamodbHelper");

async function runHera1116Staff() {
    const staffs = await getAllStaffs();
    console.log(JSON.stringify(staffs));
    //cleanEmails
    var staffstoClean = await cleanStaffs(staffs);
    
    console.log(JSON.stringify(staffstoClean));
    
}

async function getAllStaffs() {
    try {
        let staffs = [];
        let lastEvaluatedKey = null;
        do {
            let params = {   
                TableName: process.env.STAFF_TABLE,
                ProjectionExpression: "id, firstName, lastName, notes",
                ExclusiveStartKey: lastEvaluatedKey,
                
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            staffs = [...staffs, ...items];
        } while (lastEvaluatedKey);

        return staffs;
    } catch (err) {
        console.log("[getAllstaffs] Error in function getAllstaffs", err);
        return {
            error: err,
        };
    }
}

async function cleanStaffs(list){
    
    const  staffstoClean = [];
 
    for(const staff of list) {
        if (staff.firstName) staff.firstName = staff.firstName.trim();
        if (staff.lastName) staff.lastName = staff.lastName.trim();
        if (staff.notes) staff.notes = staff.notes.trim();

        staffstoClean.push(staff)
        //updateStaffDynamo(staff)
    }
    
    return staffstoClean;
}

async function updateStaffDynamo(staff){
    try {
        
        await updateDynamoRecord({
            TableName: process.env.STAFF_TABLE,
            Key: {"id": staff.id},
            ExpressionAttributeValues: {
                ":firstName": staff.firstName,
                ":lastName": staff.lastName,
                ":notes": staff.notes,
            },
            ExpressionAttributeNames: {
                "#firstName": 'firstName',
                "#lastName": 'lastName',
                "#notes": 'notes'
            },
            UpdateExpression: "set #firstName = :firstName, #lastName = :lastName, #notes = :notes",
            ReturnValues: "UPDATED_NEW"
        })
            
        
        
    } catch (err) {
        console.log("[hera-1116] Error updating staffs", err);
        return {
            error: err,
        };
    }
}



module.exports = {
    runHera1116Staff,
};