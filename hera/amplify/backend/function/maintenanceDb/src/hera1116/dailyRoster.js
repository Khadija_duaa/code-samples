const { buildUpdateParams, scanDynamoRecords, updateDynamoRecord } = require("../dynamodbHelper.js");

async function runHera1116DailyRoster() {
    const dailyRosters = await getAllDailyRosters();
    console.log(JSON.stringify(dailyRosters));
    //cleanEmails
    var dailyRosterstoClean = await cleanDailyRosters(dailyRosters);
    
    console.log(JSON.stringify(dailyRosterstoClean));
    
}

async function getAllDailyRosters() {
    try {
        let dailyRosters = [];
        let lastEvaluatedKey = null;
        do {
            let params = {   
                TableName: process.env.DAILY_ROSTER_TABLE,
                ProjectionExpression: "id, amNotes,pmNotes,standUpNotes",
                ExclusiveStartKey: lastEvaluatedKey,
                
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            dailyRosters = [...dailyRosters, ...items];
        } while (lastEvaluatedKey);

        return dailyRosters;
    } catch (err) {
        console.log("[getAlldailyRosters] Error in function getAlldailyRosters", err);
        return {
            error: err,
        };
    }
}

async function cleanDailyRosters(list){
    
    const  dailyRosterstoClean = [];
 
    for(const dailyRoster of list) {
        if (dailyRoster.amNotes) dailyRoster.amNotes = dailyRoster.amNotes.trim();
        if (dailyRoster.pmNotes) dailyRoster.pmNotes = dailyRoster.pmNotes.trim();
        if (dailyRoster.standUpNotes) dailyRoster.standUpNotes = dailyRoster.standUpNotes.trim();

        dailyRosterstoClean.push(dailyRoster)
        //updateDailyRosterDynamo(dailyRoster)
    }
    
    return dailyRosterstoClean;
}

async function updateDailyRosterDynamo(dailyRoster){
    try {
        
        await updateDynamoRecord({
            TableName: process.env.DAILY_ROSTER_TABLE,
            Key: {"id": dailyRoster.id},
            ExpressionAttributeValues: {
                ":amNotes": dailyRoster.amNotes,
                ":pmNotes": dailyRoster.pmNotes,
                ":standUpNotes": dailyRoster.standUpNotes

            },
            ExpressionAttributeNames: {
                "#amNotes": 'amNotes',
                "#pmNotes": 'pmNotes',
                "#standUpNotes": 'standUpNotes',

            },
            UpdateExpression: "set #amNotes = :amNotes, #pmNotes = :pmNotes,#standUpNotes = :standUpNotes",
            ReturnValues: "UPDATED_NEW"
        })
            
        
        
    } catch (err) {
        console.log("[hera-1116] Error updating dailyRosters", err);
        return {
            error: err,
        };
    }
}



module.exports = {
    runHera1116DailyRoster,
};