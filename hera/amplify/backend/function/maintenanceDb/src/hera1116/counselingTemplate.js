const { buildUpdateParams, scanDynamoRecords, updateDynamoRecord } = require("../dynamodbHelper.js");

async function runHera1116CounselingTemplate() {
    const counselingTemplates = await getAllCounselingTemplates();
    console.log(JSON.stringify(counselingTemplates));
    //cleanEmails
    var counselingTemplatestoClean = await cleanCounselingTemplates(counselingTemplates);
    
    console.log(JSON.stringify(counselingTemplatestoClean));
    
}

async function getAllCounselingTemplates() {
    try {
        let counselingTemplates = [];
        let lastEvaluatedKey = null;
        do {
            let params = {   
                TableName: process.env.COUNSELING_TEMPLATE_TABLE,
                ProjectionExpression: "id, consequencesOfFailure, correctiveActionSummary, counselingNotes, priorDiscussionSummary, templateName",
                ExclusiveStartKey: lastEvaluatedKey,
                
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            counselingTemplates = [...counselingTemplates, ...items];
        } while (lastEvaluatedKey);

        return counselingTemplates;
    } catch (err) {
        console.log("[getAllCounselingTemplates] Error in function getAllCounselingTemplates", err);
        return {
            error: err,
        };
    }
}

async function cleanCounselingTemplates(list){
    
    const  counselingTemplatestoClean = [];
 
    for(const counselingTemplate of list) {
        if (counselingTemplate.consequencesOfFailure) counselingTemplate.consequencesOfFailure = counselingTemplate.consequencesOfFailure.trim();
        if (counselingTemplate.correctiveActionSummary) counselingTemplate.correctiveActionSummary = counselingTemplate.correctiveActionSummary.trim();
        if (counselingTemplate.counselingNotes) counselingTemplate.counselingNotes = counselingTemplate.counselingNotes.trim();
        if (counselingTemplate.priorDiscussionSummary) counselingTemplate.priorDiscussionSummary = counselingTemplate.priorDiscussionSummary.trim();
        if (counselingTemplate.templateName) counselingTemplate.templateName = counselingTemplate.templateName.trim();
        
        counselingTemplatestoClean.push(counselingTemplate)
        //updateCounselingTemplateDynamo(counselingTemplate)
    }
    
    return counselingTemplatestoClean;
}

async function updateCounselingTemplateDynamo(counselingTemplate){
    try {
        
        await updateDynamoRecord({
            TableName: process.env.COUNSELING_TEMPLATE_TABLE,
            Key: {"id": counselingTemplate.id},
            ExpressionAttributeValues: {
                ":consequencesOfFailure": counselingTemplate.consequencesOfFailure,
                ":correctiveActionSummary": counselingTemplate.correctiveActionSummary,
                ":counselingNotes": counselingTemplate.counselingNotes,
                ":priorDiscussionSummary": counselingTemplate.priorDiscussionSummary,
                ":templateName": counselingTemplate.templateName
            },
            ExpressionAttributeNames: {
                "#consequencesOfFailure": 'consequencesOfFailure',
                "#correctiveActionSummary" : 'correctiveActionSummary',
                "#counselingNotes": 'counselingNotes',
                "#priorDiscussionSummary": 'priorDiscussionSummary',
                "#templateName": 'templateName'
            },
            UpdateExpression: "set #consequencesOfFailure = :consequencesOfFailure, #correctiveActionSummary = :correctiveActionSummary, #counselingNotes = :counselingNotes, #priorDiscussionSummary = :priorDiscussionSummary, #templateName = :templateName",
            ReturnValues: "UPDATED_NEW"
        })
            
        
        
    } catch (err) {
        console.log("[hera-1116] Error updating counselingTemplates", err);
        return {
            error: err,
        };
    }
}



module.exports = {
    runHera1116CounselingTemplate,
};