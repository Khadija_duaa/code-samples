const { buildUpdateParams, scanDynamoRecords, updateDynamoRecord } = require("../dynamodbHelper");

async function runHera1116Document() {
    const documents = await getAllDocuments();
    console.log(JSON.stringify(documents));
    //cleanEmails
    var documentstoClean = await cleanDocuments(documents);
    
    console.log(JSON.stringify(documentstoClean));
    
}

async function getAllDocuments() {
    try {
        let documents = [];
        let lastEvaluatedKey = null;
        do {
            let params = {   
                TableName: process.env.DOCUMENT_TABLE,
                ProjectionExpression: "id,notes",
                ExclusiveStartKey: lastEvaluatedKey,
                
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            documents = [...documents, ...items];
        } while (lastEvaluatedKey);

        return documents;
    } catch (err) {
        console.log("[getAlldocuments] Error in function getAlldocuments", err);
        return {
            error: err,
        };
    }
}

async function cleanDocuments(list){
    
    const  documentstoClean = [];
 
    for(const document of list) {
        if (document.notes) document.notes = document.notes.trim();

        documentstoClean.push(document)
        //updateDocumentDynamo(document)
    }
    
    return documentstoClean;
}

async function updateDocumentDynamo(document){
    try {
        
        await updateDynamoRecord({
            TableName: process.env.DOCUMENT_TABLE,
            Key: {"id": document.id},
            ExpressionAttributeValues: {
                ":notes": document.notes
            },
            ExpressionAttributeNames: {
                "#notes": 'notes',
            },
            UpdateExpression: "set #notes = :notes",
            ReturnValues: "UPDATED_NEW"
        })
            
        
        
    } catch (err) {
        console.log("[hera-1116] Error updating documents", err);
        return {
            error: err,
        };
    }
}



module.exports = {
    runHera1116Document,
};