const { buildUpdateParams, scanDynamoRecords, updateDynamoRecord } = require("../dynamodbHelper");

async function runHera1116Route() {
    const routes = await getAllRoutes();
    console.log(JSON.stringify(routes));
    //cleanEmails
    var routestoClean = await cleanRoutes(routes);
    
    console.log(JSON.stringify(routestoClean));
    
}

async function getAllRoutes() {
    try {
        let routes = [];
        let lastEvaluatedKey = null;
        do {
            let params = {   
                TableName: process.env.ROUTE_TABLE,
                ProjectionExpression: "id, staging",
                ExclusiveStartKey: lastEvaluatedKey,
                
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            routes = [...routes, ...items];
        } while (lastEvaluatedKey);

        return routes;
    } catch (err) {
        console.log("[getAllroutes] Error in function getAllroutes", err);
        return {
            error: err,
        };
    }
}

async function cleanRoutes(list){
    
    const  routestoClean = [];
 
    for(const route of list) {
        if (route.staging) route.staging = route.staging.trim();

        routestoClean.push(route)
        //updaterouteDynamo(route)
    }
    
    return routestoClean;
}

async function updaterouteDynamo(route){
    try {
        
        await updateDynamoRecord({
            TableName: process.env.ROUTE_TABLE,
            Key: {"id": route.id},
            ExpressionAttributeValues: {
                ":staging": route.staging,
            },
            ExpressionAttributeNames: {
                "#staging": 'staging',
            },
            UpdateExpression: "set #staging = :staging",
            ReturnValues: "UPDATED_NEW"
        })
            
        
        
    } catch (err) {
        console.log("[hera-1116] Error updating routes", err);
        return {
            error: err,
        };
    }
}



module.exports = {
    runHera1116Route,
};