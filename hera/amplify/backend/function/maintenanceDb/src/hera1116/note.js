const { buildUpdateParams, scanDynamoRecords, updateDynamoRecord } = require("../dynamodbHelper");

async function runHera1116Note() {
    const notes = await getAllNotes();
    console.log(JSON.stringify(notes));
    //cleanEmails
    var notestoClean = await cleanNotes(notes);
    
    console.log(JSON.stringify(notestoClean));
    
}

async function getAllNotes() {
    try {
        let notes = [];
        let lastEvaluatedKey = null;
        do {
            let params = {   
                TableName: process.env.note_TABLE,
                ProjectionExpression: "id, text",
                ExclusiveStartKey: lastEvaluatedKey,
                
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            notes = [...notes, ...items];
        } while (lastEvaluatedKey);

        return notes;
    } catch (err) {
        console.log("[getAllnotes] Error in function getAllnotes", err);
        return {
            error: err,
        };
    }
}

async function cleanNotes(list){
    
    const  notestoClean = [];
 
    for(const note of list) {
        if (note.text) note.text = note.text.trim();

        notestoClean.push(note)
        //updateNoteDynamo(note)
    }
    
    return notestoClean;
}

async function updateNoteDynamo(note){
    try {
        
        await updateDynamoRecord({
            TableName: process.env.note_TABLE,
            Key: {"id": note.id},
            ExpressionAttributeValues: {
                ":text": note.text,
            },
            ExpressionAttributeNames: {
                "#text": 'text',
            },
            UpdateExpression: "set #text = :text",
            ReturnValues: "UPDATED_NEW"
        })
            
        
        
    } catch (err) {
        console.log("[hera-1116] Error updating notes", err);
        return {
            error: err,
        };
    }
}



module.exports = {
    runHera1116Note,
};