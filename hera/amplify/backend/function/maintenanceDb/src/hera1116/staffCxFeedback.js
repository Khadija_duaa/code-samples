const { buildUpdateParams, scanDynamoRecords, updateDynamoRecord } = require("../dynamodbHelper");

async function runHera1116StaffCxFeedback() {
    const staffCxFeedbacks = await getAllStaffCxFeedbacks();
    console.log(JSON.stringify(staffCxFeedbacks));
    //cleanEmails
    var staffCxFeedbackstoClean = await cleanStaffCxFeedbacks(staffCxFeedbacks);
    
    console.log(JSON.stringify(staffCxFeedbackstoClean));
    
}

async function getAllStaffCxFeedbacks() {
    try {
        let staffCxFeedbacks = [];
        let lastEvaluatedKey = null;
        do {
            let params = {   
                TableName: process.env.STAFF_CX_FEEDBACK_TABLE,
                ProjectionExpression: "id, name",
                ExclusiveStartKey: lastEvaluatedKey,
                
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            staffCxFeedbacks = [...staffCxFeedbacks, ...items];
        } while (lastEvaluatedKey);

        return staffCxFeedbacks;
    } catch (err) {
        console.log("[getAllStaffCxFeedbacks] Error in function getAllStaffCxFeedbacks", err);
        return {
            error: err,
        };
    }
}

async function cleanStaffCxFeedbacks(list){
    
    const  staffCxFeedbackstoClean = [];
 
    for(const staffCxFeedback of list) {
        if (staffCxFeedback.name) staffCxFeedback.name = staffCxFeedback.name.trim();

        staffCxFeedbackstoClean.push(staffCxFeedback)
        //updateStaffCxFeedbackDynamo(staffCxFeedback)
    }
    
    return staffCxFeedbackstoClean;
}

async function updateStaffCxFeedbackDynamo(staffCxFeedback){
    try {
        
        await updateDynamoRecord({
            TableName: process.env.STAFF_CX_FEEDBACK_TABLE,
            Key: {"id": staffCxFeedback.id},
            ExpressionAttributeValues: {
                ":name": staffCxFeedback.name
            },
            ExpressionAttributeNames: {
                "#name": 'name'
            },
            UpdateExpression: "set #name = :name",
            ReturnValues: "UPDATED_NEW"
        })
            
        
        
    } catch (err) {
        console.log("[hera-1116] Error updating staffCxFeedbacks", err);
        return {
            error: err,
        };
    }
}



module.exports = {
    runHera1116StaffCxFeedback,
};