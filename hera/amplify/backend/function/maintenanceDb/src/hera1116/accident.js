const { buildUpdateParams, scanDynamoRecords, updateDynamoRecord } = require("../dynamodbHelper.js");

async function runHera1116Accident() {
    const accidents = await getAllAccidents();
    console.log(JSON.stringify(accidents));
    //cleanEmails
    var accidentstoClean = await cleanAccidents(accidents);
    
    console.log(JSON.stringify(accidentstoClean));
    
}

async function getAllAccidents() {
    try {
        let accidents = [];
        let lastEvaluatedKey = null;
        do {
            let params = {   
                TableName: process.env.ACCIDENT_TABLE,
                ProjectionExpression: "id, address, addressCity, addressState, addressZip, insuranceClaimNumber, notes, policeDepartment, policeOfficerName, policeReportNumber",
                ExclusiveStartKey: lastEvaluatedKey,
                
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            accidents = [...accidents, ...items];
        } while (lastEvaluatedKey);

        return accidents;
    } catch (err) {
        console.log("[getAllAccidents] Error in function getAllAccidents", err);
        return {
            error: err,
        };
    }
}

async function cleanAccidents(list){
    
    const  AccidentstoClean = [];
 
    for(const accident of list) {
        if (accident.address) accident.address = accident.address.trim();
        if (accident.addressCity) accident.addressCity = accident.addressCity.trim();
        if (accident.addressState) accident.addressState = accident.addressState.trim();
        if (accident.addressZip) accident.addressZip = accident.addressZip.trim();
        if (accident.insuranceClaimNumber) accident.insuranceClaimNumber = accident.insuranceClaimNumber.trim();
        if (accident.notes) accident.notes = accident.notes.trim();
        if (accident.policeDepartment) accident.policeDepartment = accident.policeDepartment.trim();
        if (accident.policeOfficerName) accident.policeOfficerName = accident.policeOfficerName.trim();
        if (accident.policeReportNumber) accident.policeReportNumber = accident.policeReportNumber.trim();

        AccidentstoClean.push(accident)
        //updateAccidentDynamo(accident)
    }
    
    return AccidentstoClean;
}

async function updateAccidentDynamo(accident){
    try {
        
        await updateDynamoRecord({
            TableName: process.env.ACCIDENT_TABLE,
            Key: {"id": accident.id},
            ExpressionAttributeValues: {
                ":address": accident.address,
                ":addressCity": accident.addressCity,
                ":addressState": accident.addressState,
                ":addressZip": accident.addressZip,
                ":insuranceClaimNumber": accident.insuranceClaimNumber,
                ":notes": accident.notes,
                ":policeDepartment": accident.policeDepartment,
                ":policeOfficerName": accident.policeOfficerName,
                ":policeReportNumber": accident.policeReportNumber
            },
            ExpressionAttributeNames: {
                "#address": 'address',
                "#addressCity" : 'addressCity',
                "#addressState": 'addressState',
                "#addressZip": 'addressZip',
                "#insuranceClaimNumber": 'insuranceClaimNumber',
                "#notes": 'notes',
                "#policeDepartment": 'policeDepartment',
                "#policeOfficerName": 'policeOfficerName',
                "#policeReportNumber": 'policeReportNumber'
            },
            UpdateExpression: "set #address = :address, #addressCity = :addressCity, #addressState = :addressState, #addressZip = :addressZip, #insuranceClaimNumber = :insuranceClaimNumber, #notes = :notes, #policeDepartment = :policeDepartment, #policeOfficerName = :policeOfficerName, #policeReportNumber = :policeReportNumber",
            ReturnValues: "UPDATED_NEW"
        })
            
        
        
    } catch (err) {
        console.log("[hera-1116] Error updating accidents", err);
        return {
            error: err,
        };
    }
}



module.exports = {
    runHera1116Accident,
};