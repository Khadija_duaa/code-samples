const { buildUpdateParams, scanDynamoRecords, updateDynamoRecord } = require("../dynamodbHelper");

async function runHera1116Injury() {
    const injuries = await getAllInjuries();
    console.log(JSON.stringify(injuries));
    //cleanEmails
    var injuriestoClean = await cleanInjuries(injuries);
    
    console.log(JSON.stringify(injuriestoClean));
    
}

async function getAllInjuries() {
    try {
        let injuries = [];
        let lastEvaluatedKey = null;
        do {
            let params = {   
                TableName: process.env.INJURY_TABLE,
                ProjectionExpression: "id, caseNumber, completedByTitle, descriptionBeforeAccident, descriptionDirectHarmCause, descriptionIncident, descriptionInjury, driverAddress, driverCity, driverState, driverZip, notes, physicianAddress, physicianCity, physicianFacility, physicianName, physicianState, physicianZip",
                ExclusiveStartKey: lastEvaluatedKey,
                
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            injuries = [...injuries, ...items];
        } while (lastEvaluatedKey);

        return injuries;
    } catch (err) {
        console.log("[getAllInjuries] Error in function getAllInjuries", err);
        return {
            error: err,
        };
    }
}

async function cleanInjuries(list){
    
    const  injuriestoClean = [];
 
    for(const injury of list) {
        if (injury.caseNumber) injury.caseNumber = injury.caseNumber.trim();
        if (injury.completedByTitle) injury.completedByTitle = injury.completedByTitle.trim();
        if (injury.descriptionBeforeAccident) injury.descriptionBeforeAccident = injury.descriptionBeforeAccident.trim();
        if (injury.descriptionDirectHarmCause) injury.descriptionDirectHarmCause = injury.descriptionDirectHarmCause.trim();
        if (injury.descriptionIncident) injury.descriptionIncident = injury.descriptionIncident.trim();
        if (injury.descriptionInjury) injury.descriptionInjury = injury.descriptionInjury.trim();
        if (injury.driverAddress) injury.driverAddress = injury.driverAddress.trim();
        if (injury.driverCity) injury.driverCity = injury.driverCity.trim();
        if (injury.driverState) injury.driverState = injury.driverState.trim();
        if (injury.driverZip) injury.driverZip = injury.driverZip.trim();
        if (injury.notes) injury.notes = injury.notes.trim();
        if (injury.physicianAddress) injury.physicianAddress = injury.physicianAddress.trim();
        if (injury.physicianCity) injury.physicianCity = injury.physicianCity.trim();
        if (injury.physicianFacility) injury.physicianFacility = injury.physicianFacility.trim();
        if (injury.physicianName) injury.physicianName = injury.physicianName.trim();
        if (injury.physicianState) injury.physicianState = injury.physicianState.trim();
        if (injury.physicianZip) injury.physicianZip = injury.physicianZip.trim();


        injuriestoClean.push(injury)
        //updateInjuryDynamo(Injury)
    }
    
    return injuriestoClean;
}

async function updateInjuryDynamo(injury){
    try {
        
        await updateDynamoRecord({
            TableName: process.env.INJURY_TABLE,
            Key: {"id": injury.id},
            ExpressionAttributeValues: {
                ":caseNumber": injury.caseNumber,
                ":completedByTitle": injury.completedByTitle,
                ":descriptionBeforeAccident": injury.descriptionBeforeAccident,
                ":descriptionDirectHarmCause": injury.descriptionDirectHarmCause,
                ":descriptionIncident": injury.descriptionIncident,
                ":descriptionInjury": injury.descriptionInjury,
                ":driverAddress": injury.driverAddress,
                ":driverCity": injury.driverCity,
                ":driverState": injury.driverState,
                ":driverZip": injury.driverZip,
                ":notes": injury.notes,
                ":physicianAddress": injury.physicianAddress,
                ":physicianCity": injury.physicianCity,
                ":physicianFacility": injury.physicianFacility,
                ":physicianName": injury.physicianName,
                ":physicianState": injury.physicianState,
                ":physicianZip": injury.physicianZip
            },
            ExpressionAttributeNames: {
                "#caseNumber": 'caseNumber',
                "#completedByTitle": 'completedByTitle',
                "#descriptionBeforeAccident": 'descriptionBeforeAccident',
                "#descriptionDirectHarmCause": 'descriptionDirectHarmCause',
                "#descriptionIncident": 'descriptionIncident',
                "#descriptionInjury": 'descriptionInjury',
                "#driverAddress": 'driverAddress',
                "#driverCity": 'driverCity',
                "#driverState": 'driverState',
                "#driverZip": 'driverZip',
                "#notes": 'notes',
                "#physicianAddress": 'physicianAddress',
                "#physicianCity": 'physicianCity',
                "#physicianFacility": 'physicianFacility',
                "#physicianName": 'physicianName',
                "#physicianState": 'physicianState',
                "#physicianZip": 'physicianZip'
            },
            UpdateExpression: `set #caseNumber = :caseNumber,
                                    #completedByTitle = :completedByTitle,
                                    #descriptionBeforeAccident = :descriptionBeforeAccident,
                                    #descriptionDirectHarmCause = :descriptionDirectHarmCause,
                                    #descriptionIncident = :descriptionIncident,
                                    #descriptionInjury = :descriptionInjury,
                                    #driverAddress = :driverAddress,
                                    #driverCity = :driverCity,
                                    #driverState = :driverState,
                                    #driverZip = :driverZip,
                                    #notes = :notes,
                                    #physicianAddress = :physicianAddress,
                                    #physicianCity = :physicianCity,
                                    #physicianFacility = :physicianFacility,
                                    #physicianName = :physicianName,
                                    #physicianState = :physicianState,
                                    #physicianZip = :physicianZip`,
            ReturnValues: "UPDATED_NEW"
        })
            
        
        
    } catch (err) {
        console.log("[hera-1116] Error updating injuries", err);
        return {
            error: err,
        };
    }
}



module.exports = {
    runHera1116Injury,
};