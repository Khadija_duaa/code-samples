const { buildUpdateParams, scanDynamoRecords, updateDynamoRecord } = require("../dynamodbHelper");

async function runHera1116Vehicle() {
    const vehicles = await getAllVehicles();
    console.log(JSON.stringify(vehicles));
    //cleanEmails
    var vehiclestoClean = await cleanVehicles(vehicles);
    
    console.log(JSON.stringify(vehiclestoClean));
    
}

async function getAllVehicles() {
    try {
        let vehicles = [];
        let lastEvaluatedKey = null;
        do {
            let params = {   
                TableName: process.env.VEHICLE_TABLE,
                ProjectionExpression: "id, gasCard, licensePlate, make, model, name, notes, vin",
                ExclusiveStartKey: lastEvaluatedKey,
                
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            vehicles = [...vehicles, ...items];
        } while (lastEvaluatedKey);

        return vehicles;
    } catch (err) {
        console.log("[getAllvehicles] Error in function getAllvehicles", err);
        return {
            error: err,
        };
    }
}

async function cleanVehicles(list){
    
    const  vehiclestoClean = [];
 
    for(const vehicle of list) {
        if (vehicle.gasCard) vehicle.gasCard = vehicle.gasCard.trim();
        if (vehicle.licensePlate) vehicle.licensePlate = vehicle.licensePlate.trim();
        if (vehicle.make) vehicle.make = vehicle.make.trim();
        if (vehicle.model) vehicle.model = vehicle.model.trim();
        if (vehicle.name) vehicle.name = vehicle.name.trim();
        if (vehicle.notes) vehicle.notes = vehicle.notes.trim();
        if (vehicle.vin) vehicle.vin = vehicle.vin.trim();

        vehiclestoClean.push(vehicle)
        //updateVehicleDynamo(vehicle)
    }
    
    return vehiclestoClean;
}

async function updateVehicleDynamo(vehicle){
    try {
        
        await updateDynamoRecord({
            TableName: process.env.VEHICLE_TABLE,
            Key: {"id": vehicle.id},
            ExpressionAttributeValues: {
                ":gasCard": vehicle.gasCard,
                ":licensePlate": vehicle.licensePlate,
                ":make": vehicle.make,
                ":model": vehicle.model,
                ":name": vehicle.name,
                ":notes": vehicle.notes,
                ":vin": vehicle.vin
            },
            ExpressionAttributeNames: {
                "#gasCard": 'gasCard',
                "#licensePlate": 'licensePlate',
                "#make": 'make',
                "#model": 'model',
                "#name": 'name',
                "#notes": 'notes',
                "#vin": 'vin'
            },
            UpdateExpression: "set #gasCard = :gasCard, #licensePlate = :licensePlate, #make = :make, #model = :model, #name = :name, #notes = :notes, #vin = :vin",
            ReturnValues: "UPDATED_NEW"
        })
            
        
        
    } catch (err) {
        console.log("[hera-1116] Error updating vehicles", err);
        return {
            error: err,
        };
    }
}



module.exports = {
    runHera1116Vehicle,
};