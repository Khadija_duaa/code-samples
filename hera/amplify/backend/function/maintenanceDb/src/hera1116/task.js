const { buildUpdateParams, scanDynamoRecords, updateDynamoRecord } = require("../dynamodbHelper");

async function runHera1116Task() {
    const tasks = await getAllTasks();
    console.log(JSON.stringify(tasks));
    //cleanEmails
    var taskstoClean = await cleanTasks(tasks);
    
    console.log(JSON.stringify(taskstoClean));
    
}

async function getAllTasks() {
    try {
        let tasks = [];
        let lastEvaluatedKey = null;
        do {
            let params = {   
                TableName: process.env.TASK_TABLE,
                ProjectionExpression: "id, description, taskName",
                ExclusiveStartKey: lastEvaluatedKey,
                
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            tasks = [...tasks, ...items];
        } while (lastEvaluatedKey);

        return tasks;
    } catch (err) {
        console.log("[getAlltasks] Error in function getAlltasks", err);
        return {
            error: err,
        };
    }
}

async function cleanTasks(list){
    
    const  taskstoClean = [];
 
    for(const task of list) {
        if (task.description) task.description = task.description.trim();
        if (task.taskName) task.taskName = task.taskName.trim();

        taskstoClean.push(task)
        //updateTaskDynamo(task)
    }
    
    return taskstoClean;
}

async function updateTaskDynamo(task){
    try {
        
        await updateDynamoRecord({
            TableName: process.env.TASK_TABLE,
            Key: {"id": task.id},
            ExpressionAttributeValues: {
                ":description": task.description,
                ":taskName": task.taskName
            },
            ExpressionAttributeNames: {
                "#description": 'description',
                "#taskName": 'taskName'
            },
            UpdateExpression: "set #description = :description, #taskName = :taskName",
            ReturnValues: "UPDATED_NEW"
        })
            
        
        
    } catch (err) {
        console.log("[hera-1116] Error updating tasks", err);
        return {
            error: err,
        };
    }
}



module.exports = {
    runHera1116Task,
};