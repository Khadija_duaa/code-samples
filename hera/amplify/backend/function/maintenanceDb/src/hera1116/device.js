const { buildUpdateParams, scanDynamoRecords, updateDynamoRecord } = require("../dynamodbHelper.js");

async function runHera1116Device() {
    const devices = await getAllDevices();
    console.log(JSON.stringify(devices));
    //cleanEmails
    var devicestoClean = await cleanDevices(devices);
    
    console.log(JSON.stringify(devicestoClean));
    
}

async function getAllDevices() {
    try {
        let devices = [];
        let lastEvaluatedKey = null;
        do {
            let params = {   
                TableName: process.env.DEVICE_TABLE,
                ProjectionExpression: "id, deviceName,notes",
                ExclusiveStartKey: lastEvaluatedKey,
                
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            devices = [...devices, ...items];
        } while (lastEvaluatedKey);

        return devices;
    } catch (err) {
        console.log("[getAlldevices] Error in function getAlldevices", err);
        return {
            error: err,
        };
    }
}

async function cleanDevices(list){
    
    const  devicestoClean = [];
 
    for(const device of list) {
        if (device.deviceName) device.deviceName = device.deviceName.trim();
        if (device.notes) device.notes = device.notes.trim();

        devicestoClean.push(device)
        //updatedeviceDynamo(device)
    }
    
    return devicestoClean;
}

async function updatedeviceDynamo(device){
    try {
        
        await updateDynamoRecord({
            TableName: process.env.DEVICE_TABLE,
            Key: {"id": device.id},
            ExpressionAttributeValues: {
                ":deviceName": device.deviceName,
                ":notes": device.notes
            },
            ExpressionAttributeNames: {
                "#deviceName": 'deviceName',
                "#notes": 'notes',

            },
            UpdateExpression: "set #deviceName = :deviceName,#notes = :notes",
            ReturnValues: "UPDATED_NEW"
        })
            
        
        
    } catch (err) {
        console.log("[hera-1116] Error updating devices", err);
        return {
            error: err,
        };
    }
}



module.exports = {
    runHera1116Device,
};