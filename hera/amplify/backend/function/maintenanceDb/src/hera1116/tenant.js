const { buildUpdateParams, scanDynamoRecords, updateDynamoRecord } = require("../dynamodbHelper");

async function runHera1116Tenant() {
    const tenants = await getAllTenants();
    console.log(JSON.stringify(tenants));
    //cleanEmails
    var tenantstoClean = await cleanTenants(tenants);
    
    console.log(JSON.stringify(tenantstoClean));
    
}

async function getAllTenants() {
    try {
        let tenants = [];
        let lastEvaluatedKey = null;
        do {
            let params = {   
                TableName: process.env.TENANT_TABLE,
                ProjectionExpression: "id, addressCity, addressLine1, addressLine2, addressState, addressZip, shortCode",
                ExclusiveStartKey: lastEvaluatedKey,
                
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            tenants = [...tenants, ...items];
        } while (lastEvaluatedKey);

        return tenants;
    } catch (err) {
        console.log("[getAllTenants] Error in function getAllTenants", err);
        return {
            error: err,
        };
    }
}

async function cleanTenants(list){
    
    const  tenantstoClean = [];
 
    for(const tenant of list) {
        if (tenant.addressCity) tenant.addressCity = tenant.addressCity.trim();
        if (tenant.addressLine1) tenant.addressLine1 = tenant.addressLine1.trim();
        if (tenant.addressLine2) tenant.addressLine2 = tenant.addressLine2.trim();
        if (tenant.addressState) tenant.addressState = tenant.addressState.trim();
        if (tenant.addressZip) tenant.addressZip = tenant.addressZip.trim();
        if (tenant.shortCode) tenant.shortCode = tenant.shortCode.trim();


        tenantstoClean.push(tenant)
        //updateTenantDynamo(tenant)
    }
    
    return tenantstoClean;
}

async function updateTenantDynamo(tenant){
    try {
        
        await updateDynamoRecord({
            TableName: process.env.TENANT_TABLE,
            Key: {"id": tenant.id},
            ExpressionAttributeValues: {
                ":addressCity": tenant.addressCity,
                ":addressLine1": tenant.addressLine1,
                ":addressLine2": tenant.addressLine2,
                ":addressState": tenant.addressState,
                ":addressZip": tenant.addressZip,
                ":shortCode": tenant.shortCode
            },
            ExpressionAttributeNames: {
                "#addressCity": 'addressCity',
                "#addressLine1": 'addressLine1',
                "#addressLine2": 'addressLine2',
                "#addressState": 'addressState',
                "#addressZip": 'addressZip',
                "#shortCode": 'shortCode'
            },
            UpdateExpression: "set #addressCity = :addressCity, #addressLine1 = :addressLine1, #addressLine2 = :addressLine2, #addressState = :addressState, #addressZip = :addressZip, #shortCode = :shortCode",
            ReturnValues: "UPDATED_NEW"
        })
            
        
        
    } catch (err) {
        console.log("[hera-1116] Error updating tenants", err);
        return {
            error: err,
        };
    }
}



module.exports = {
    runHera1116Tenant,
};