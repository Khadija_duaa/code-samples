const { buildUpdateParams, scanDynamoRecords, updateDynamoRecord } = require("../dynamodbHelper");

async function runHera1116User() {
    const users = await getAllUsers();
    console.log(JSON.stringify(users));
    //cleanEmails
    var userstoClean = await cleanUsers(users);
    
    console.log(JSON.stringify(userstoClean));
    
}

async function getAllUsers() {
    try {
        let users = [];
        let lastEvaluatedKey = null;
        do {
            let params = {   
                TableName: process.env.USER_TABLE,
                ProjectionExpression: "id, firstName, lastName",
                ExclusiveStartKey: lastEvaluatedKey,
                
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            users = [...users, ...items];
        } while (lastEvaluatedKey);

        return users;
    } catch (err) {
        console.log("[getAllusers] Error in function getAllusers", err);
        return {
            error: err,
        };
    }
}

async function cleanUsers(list){
    
    const  userstoClean = [];
 
    for(const user of list) {
        if (user.firstName) user.firstName = user.firstName.trim();
        if (user.lastName) user.lastName = user.lastName.trim();

        userstoClean.push(user)
        //updateUserDynamo(user)
    }
    
    return userstoClean;
}

async function updateUserDynamo(user){
    try {
        
        await updateDynamoRecord({
            TableName: process.env.USER_TABLE,
            Key: {"id": user.id},
            ExpressionAttributeValues: {
                ":firstName": user.firstName,
				":lastName": user.lastName
            },
            ExpressionAttributeNames: {
                "#firstName": 'firstName',
				"#lastName": 'lastName'
            },
            UpdateExpression: "set #firstName = :firstName, #lastName = :lastName",
            ReturnValues: "UPDATED_NEW"
        })
            
        
        
    } catch (err) {
        console.log("[hera-1116] Error updating users", err);
        return {
            error: err,
        };
    }
}



module.exports = {
    runHera1116User,
};