const { buildUpdateParams, scanDynamoRecords, updateDynamoRecord } = require("../dynamodbHelper");

async function runHera1116Kudo() {
    const kudos = await getAllKudos();
    console.log(JSON.stringify(kudos));
    //cleanEmails
    var kudostoClean = await cleanKudos(kudos);
    
    console.log(JSON.stringify(kudostoClean));
    
}

async function getAllKudos() {
    try {
        let kudos = [];
        let lastEvaluatedKey = null;
        do {
            let params = {   
                TableName: process.env.KUDO_TABLE,
                ProjectionExpression: "id,notes",
                ExclusiveStartKey: lastEvaluatedKey,
                
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            kudos = [...kudos, ...items];
        } while (lastEvaluatedKey);

        return kudos;
    } catch (err) {
        console.log("[getAllkudos] Error in function getAllkudos", err);
        return {
            error: err,
        };
    }
}

async function cleanKudos(list){
    
    const  kudostoClean = [];
 
    for(const kudo of list) {
        if (kudo.notes) kudo.notes = kudo.notes.trim();

        kudostoClean.push(kudo)
        //updateKudoDynamo(kudo)
    }
    
    return kudostoClean;
}

async function updateKudoDynamo(kudo){
    try {
        
        await updateDynamoRecord({
            TableName: process.env.KUDO_TABLE,
            Key: {"id": kudo.id},
            ExpressionAttributeValues: {
                ":notes": kudo.notes
            },
            ExpressionAttributeNames: {
                "#notes": 'notes',
            },
            UpdateExpression: "set #notes = :notes",
            ReturnValues: "UPDATED_NEW"
        })
            
        
        
    } catch (err) {
        console.log("[hera-1116] Error updating kudos", err);
        return {
            error: err,
        };
    }
}



module.exports = {
    runHera1116Kudo,
};