const { buildUpdateParams, scanDynamoRecords, updateDynamoRecord } = require("../dynamodbHelper");

async function runHera1116ReplaceByRoute() {
    const replaceByRoutes = await getAllReplaceByRoutes();
    console.log(JSON.stringify(replaceByRoutes));
    //cleanEmails
    var replaceByRoutestoClean = await cleanReplaceByRoutes(replaceByRoutes);
    
    console.log(JSON.stringify(replaceByRoutestoClean));
    
}

async function getAllReplaceByRoutes() {
    try {
        let replaceByRoutes = [];
        let lastEvaluatedKey = null;
        do {
            let params = {   
                TableName: process.env.REPLACE_BY_ROUTE_TABLE,
                ProjectionExpression: "id, staging",
                ExclusiveStartKey: lastEvaluatedKey,   
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            replaceByRoutes = [...replaceByRoutes, ...items];
        } while (lastEvaluatedKey);

        return replaceByRoutes;
    } catch (err) {
        console.log("[getAllreplaceByRoutes] Error in function getAllreplaceByRoutes", err);
        return {
            error: err,
        };
    }
}

async function cleanReplaceByRoutes(list){
    
    const  replaceByRoutestoClean = [];
 
    for(const replaceByRoute of list) {
        if (replaceByRoute.staging) replaceByRoute.staging = replaceByRoute.staging.trim();

        replaceByRoutestoClean.push(replaceByRoute)
        //updateReplaceByRouteDynamo(replaceByRoute)
    }
    
    return replaceByRoutestoClean;
}

async function updateReplaceByRouteDynamo(replaceByRoute){
    try {
        
        await updateDynamoRecord({
            TableName: process.env.REPLACE_BY_ROUTE_TABLE,
            Key: {"id": replaceByRoute.id},
            ExpressionAttributeValues: {
                ":staging": replaceByRoute.staging,
            },
            ExpressionAttributeNames: {
                "#staging": 'staging',
            },
            UpdateExpression: "set #staging = :staging",
            ReturnValues: "UPDATED_NEW"
        })
            
        
        
    } catch (err) {
        console.log("[hera-1116] Error updating replaceByRoutes", err);
        return {
            error: err,
        };
    }
}



module.exports = {
    runHera1116ReplaceByRoute,
};