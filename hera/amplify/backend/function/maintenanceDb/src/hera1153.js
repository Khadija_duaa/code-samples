/**
 * This script sets 'accidentOptionCustomListId' to it's corresponding value
 * to link an Accident with OptionsCustomList
 */

const {
  scanDynamoRecords,
  updateDynamoRecord,
} = require("./dynamodbHelper.js");


/**
 * Main function
 */
async function handleHera1153() {
  const groups = await generateGroupsObject();

  // PRINT REPORT BEFORE UPDATE
  await printReport(groups);
  // await handleAccidentsUpdate(groups);
}

/**
 * Prints a report indicating the groups considered to update the items,
 * the items that will be updated, the items that will be excluded and
 * the counst for each of them
 * 
 * @param {Object} groups -> Object that stores all found groups for Custom Lists
 * in the form: { [group]: { optionId: "123-abcd-abc123" } }
 */
async function printReport(groups) {
  const reportObj = {
    groups,
    toUpdate: { count: 0, items: [] },
    toExclude: { count: 0, items: [] },
  };

  await fetchAllAccidentsByChunks(accidentList => {
    const [itemsToUpdate, itemsToExclude] = createReportItems(groups, accidentList);

    reportObj.toUpdate.count += itemsToUpdate.length;
    reportObj.toUpdate.items = reportObj.toUpdate.items.concat(itemsToUpdate);

    reportObj.toExclude.count += itemsToExclude.length;
    reportObj.toExclude.items = reportObj.toExclude.items.concat(itemsToExclude);
  });

  // PRINT REPORT
  const reportAsJson = JSON.stringify(reportObj);
  console.log(reportAsJson);
}

/**
 * Handles Accident records update by setting accidentOptionCustomListId to the corresponding value
 * 
 * @param {Object} groups -> Object that stores all found groups for Custom Lists
 * in the form: { [group]: { optionId: "123-abcd-abc123" } }
 */
async function handleAccidentsUpdate(groups) {
  const handler = createUpdateHandler(groups);
  await fetchAllAccidentsByChunks(handler);
}

/**
 * Creates two lists which will be used to generate the report before updating
 * 
 * @param {Object} groups -> Object that stores all found groups for Custom Lists
 * in the form: { [group]: { optionId: "123-abcd-abc123" } }
 * @param {Array} accidentList -> List of Accident records
 * @returns {Array} list of lists that will be used to generate the final report
 */
function createReportItems(groups, accidentList) {
  const itemsToExclude = [];
  const itemsToUpdate = [];

  for (const accident of accidentList) {
    const { id, group } = accident;
    const option = groups[group];

    if (!option) {
      itemsToExclude.push({ id, group });
      continue;
    }

    itemsToUpdate.push({
      id: accident.id,
      accidentOptionCustomListId_NEW: option.optionId,
    });
  }

  return [itemsToUpdate, itemsToExclude];
}

/**
 * Creates a callback to handle the update of a list of Accident records
 * 
 * @param {Object} groups -> Object that stores all found groups for Custom Lists
 * in the form: { [group]: { optionId: "123-abcd-abc123" } }
 * @returns {Function} callback to be used as handler to update a list of Accident records
 */
function createUpdateHandler(groups) {
  return async (accidentList=[]) =>  {
    for (const accident of accidentList) {
      const { id, group } = accident;
      const option = groups[group];

      if (!option) continue;

      await updateAccident(id, option.optionId);
    }
  };
}

/**
 * Creates a reducer callback and injects a lookup Object to check if the evaluated option ID
 * should be included as valid ID in groups
 * 
 * @param {Object} customListsDict -> Lookup object with Custom List IDs as keys
 * to check if the option should be included in the final groups object
 * @returns {Function} callback used as reducer which handles the creation
 * of groups lookup object
 */
function createAccidentOptionReducer(customListsDict) {
  return (acc, item) => {
    const optionId = item.id;
    const customListId = item.optionsCustomListsCustomListsId;

    const customList = customListsDict[customListId];
    const listExists = !!customList;

    if (listExists) {
      const group = customList.group;
      acc[group] = { optionId };
    }

    return acc;
  };
}

/**
 * Handles the creation of groups object which will be used as lookup for Accident update
 * 
 * @returns {Object} lookup object to check if an Accident should be updated based on it's group
 */
async function generateGroupsObject() {
  const customLists = await fetchAllCustomLists();
  const accidentOptions = await fetchAllAccidentOptions();

  const customListsReducer = (acc, item) => ((acc[item.id] = { group: item.group }), acc);;
  const customListsDict = customLists.reduce(customListsReducer, {});

  const accidentOptionsReducer = createAccidentOptionReducer(customListsDict);
  const groups = accidentOptions.reduce(accidentOptionsReducer, {});

  return groups;
}

/**
 * Fetches all Incident Type records found in CustomLists
 * 
 * @returns {Array} list of all found Incident Type lists records
 */
async function fetchAllCustomLists() {
  let customLists = [];

  try {
    let lastEvaluatedKey = null;

    do {
      const params = {
        TableName: process.env.CUSTOM_LISTS_TABLE,
        ExpressionAttributeNames: {
          "#id": "id",
          "#group": "group",
          "#listCategory": "listCategory",
          "#listName": "listName",
          "#type": "type",
        },
        ExpressionAttributeValues: {
          ":listCategory": "Associates",
          ":listName": "Incident Type",
          ":type": "incident-type",
        },
        FilterExpression: "#listCategory=:listCategory and #listName=:listName and #type=:type",
        ProjectionExpression: "#id, #group",
        ExclusiveStartKey: lastEvaluatedKey,
      };
  
      const scanResult = await scanDynamoRecords(params);

      lastEvaluatedKey = scanResult.LastEvaluatedKey;
      customLists = customLists.concat(scanResult.Items);
    }
    while (lastEvaluatedKey);

    return customLists;
  }
  catch (error) {
    console.log("[HERA-1153] Error in function 'fetchAllCustomLists'", error);
    throw error;
  }
}

/**
 * Fetches all records with 'option' set to 'Accident' found in OptionsCustomList
 * 
 * @returns {Array} list of all found Accident option records
 */
async function fetchAllAccidentOptions() {
  let accidentOptions = [];

  try {
    let lastEvaluatedKey = null;

    do {
      const params = {
        TableName: process.env.OPTIONS_CUSTOM_LISTS_TABLE,
        ExpressionAttributeNames: {
          "#id": "id",
          "#option": "option",
          "#customListId": "optionsCustomListsCustomListsId",
        },
        ExpressionAttributeValues: {
          ":option": "Accident",
        },
        FilterExpression: "#option = :option",
        ProjectionExpression: "#id, #customListId",
        ExclusiveStartKey: lastEvaluatedKey,
      };
  
      const scanResult = await scanDynamoRecords(params);

      lastEvaluatedKey = scanResult.LastEvaluatedKey;
      accidentOptions = accidentOptions.concat(scanResult.Items);      
    }
    while (lastEvaluatedKey);

    return accidentOptions;
  }
  catch (error) {
    console.log("[HERA-1153] Error in function 'fetchAllAccidentOptions'", error);
    throw error;
  }
}

/**
 * Fetches all(by chunks) Accidents records with 'vehicleHistoryTye' of 'Accident' and don't have
 * relation with OptionsCustomLists
 * 
 * @param {Funciotn} cb -> Callback to execute for each chunk of records
 * retrieved by dynamo query
 */
async function fetchAllAccidentsByChunks(cb) {
  try {
    let lastEvaluatedKey = null;

    do {
      const params = {
        TableName: process.env.ACCIDENT_TABLE,
        ExpressionAttributeNames: {
          "#id": "id",
          "#group": "group",
          "#type": "vehicleHistoryType",
          "#incidentTypeId": "accidentOptionCustomListId",
        },
        ExpressionAttributeValues: {
          ":type": "Accident",
          ":null": null,
          ":empty": "",
        },
        FilterExpression: "#type=:type and (#incidentTypeId=:null or #incidentTypeId=:empty or attribute_not_exists(#incidentTypeId))",
        ProjectionExpression: "#id, #group",
        ExclusiveStartKey: lastEvaluatedKey,
      };
  
      const scanResult = await scanDynamoRecords(params);

      lastEvaluatedKey = scanResult.LastEvaluatedKey;      
      await cb(scanResult.Items);
    }
    while (lastEvaluatedKey);
  }
  catch (error) {
    console.log("[HERA-1153] Error in function 'fetchAllAccidentsByChunks'", error);
    throw error;
  }
}

/**
 * Updates an Accident record by executing a mutation on dynamoDB, sets accidentOptionCustomListId
 * tio it's corresponding value to link Accidents with OptionsCustomLists
 * 
 * @param {String} accidentId -> ID of the record to be UPDATED
 * @param {String} optionId -> ID of the option that corresponds to Accident in OptionsCustomLists
 */
async function updateAccident(accidentId, optionId) {
  try {
    await updateDynamoRecord({
      TableName: process.env.ACCIDENT_TABLE,
      Key: { id: accidentId },
      ExpressionAttributeNames: {
        "#incidentTypeId": "accidentOptionCustomListId",
      },
      ExpressionAttributeValues: {
        ":incidentTypeId": optionId
      },
      UpdateExpression: "set #incidentTypeId = :incidentTypeId",
    });
  } catch (error) {
    console.log(`[HERA-1153] Error updating accident record (ID:${accidentId})`, error);
  }
}

module.exports = { handleHera1153 };
