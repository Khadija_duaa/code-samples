const {
  scanDynamoRecords,
  queryDynamoRecords,
  updateDynamoRecord
} = require("./dynamodbHelper.js");

async function runHera1518() {
    var tenants = await getAllTenants();
    var pendingMessages = await getPenginMessagesByTenant(tenants)
    await createOrUpdateStaffId(pendingMessages)
    
    console.log({ pendingMessages});
    console.log({ totalPendingMessages: pendingMessages.length });
    console.log({ TotalTenantsUpdated: tenants.length });
}

// Get all Tenants
async function getAllTenants() {
    try {
        let tenants = [];
        let lastEvaluatedKey = null;
        do {
            let params = {
                ExpressionAttributeNames: {
                    "#g": "group"
                },
                TableName: process.env.TENANT_TABLE,
                ProjectionExpression: "id, companyName, #g",
                ExclusiveStartKey: lastEvaluatedKey,
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            tenants = [...tenants, ...items];
        } while (lastEvaluatedKey);

        return tenants;
    } catch (err) {
        console.log("[getAllTenants] Error in function getAllTenants", err);
        return {
            error: err,
        };
    }
}

async function getPenginMessagesByTenant(tenants){
    let pendingMessages = []
    for(let tenant of tenants) {
      let messages = await getPendingMessages(tenant.group)
      if(!messages.length) continue
      pendingMessages.push({ id: tenant.id, messages })
    }
    return pendingMessages
}

// Get Pending Messages
async function getPendingMessages(group) {
  try {
      let pendingMessages = [];
      let lastEvaluatedKey = null
      do{
          let params = {
              ExpressionAttributeValues: {
                ':g': group,
                ':date': '2022-10-03'
              },
              ExpressionAttributeNames: {
                '#g': 'group',
              },
              KeyConditionExpression: '#g = :g and createdAt >= :date',
              ProjectionExpression: 'id,#g, createdAt, staffId, pendingMessageStaffId',
              FilterExpression: "attribute_not_exists(staffId)",
              TableName: process.env.PENDING_MESSAGE_TABLE,
              IndexName: 'byGroup',
              ExclusiveStartKey: lastEvaluatedKey
          };
          var queryResult = await queryDynamoRecords(params);
          var items = queryResult.Items
          lastEvaluatedKey = queryResult.LastEvaluatedKey
          pendingMessages = [...pendingMessages, ...items]
      }while(lastEvaluatedKey)

      return pendingMessages;
  } catch (err) {
      console.log("[getPendingMessages] Error in function getPendingMessages", err);
      return {
          error: err,
      };
  }
}

// Fix Issue
async function createOrUpdateStaffId(tenants) {
    try {
        for(let tenant of tenants) {
          for(let pendingMessage of tenant.messages) {
            let staffId = pendingMessage.id.split(tenant.id)[1]
            console.log({ id: pendingMessage.id });
            console.log({ staffId });

            let staff = await getStaffById({ id: staffId, group: pendingMessage.group })
            console.log({ staff });

            if(!staff.length) continue
      
            await updateDynamoRecord({
              TableName: process.env.PENDING_MESSAGE_TABLE,
              Key: { "id": pendingMessage.id },
              ExpressionAttributeValues: { ":staffId": staffId },
              ExpressionAttributeNames: { "#staffId": 'staffId' },
              UpdateExpression: "set #staffId = :staffId",
              ReturnValues: "UPDATED_NEW"
            })
          }
        }
    } catch (err) {
        console.log("[createOrUpdateStaffId] Error updating pendingMessage", err);
        return {
            error: err,
        };
    }
}

// Get all Tenants
async function getStaffById(staff) {
    try {
      let staffs = []
      let lastEvaluatedKey = null
      const queryParams = {
        ExpressionAttributeNames: {
          '#id': 'id',
          '#g': 'group',
        },
        ExpressionAttributeValues: {
            ':id': staff.id,
        },
        TableName: process.env.STAFF_TABLE,
        KeyConditionExpression: '#id = :id',
        ProjectionExpression:
          '#id, #g, firstName, lastName',
      }
      do {
        const queryResult = await queryDynamoRecords(queryParams)
        const items = queryResult.Items
        lastEvaluatedKey = queryResult.LastEvaluatedKey
        staffs = [...staffs, ...items]
      } while (lastEvaluatedKey)
      return staffs
    } catch (error) {
      console.log('[getStaffById] Error in function getStaffById', error)
      return {
        error: error,
      }
    }
}

module.exports = {
  runHera1518,
};