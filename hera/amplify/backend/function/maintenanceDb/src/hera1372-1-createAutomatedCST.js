const { scanDynamoRecords, updateDynamoRecord } = require("./dynamodbHelper.js");
var AWS = require("aws-sdk");

async function runHera1372() {
    var tenants = await getAllTenants();
    await createAutomatedCoachingSendTime(tenants)
    console.log({ TotalTenantsUpdated: tenants.length });
}

async function getAllTenants() {
    try {
        let tenants = [];
        let lastEvaluatedKey = null;
        do {
            let params = {
                ExpressionAttributeNames: {
                    "#g": "group",
                    "#1": "coachingSendTime",
                    "#2": "coachingSendTimeLocal",
                    "#3": "coachingSendTimeUtcOffset",
                    "#4": "timeZoneSetAutomatically",
                    '#5': "automatedCoachingSendTime",
                    "#tz": "timeZone"
                },
                TableName: process.env.TENANT_TABLE,
                ProjectionExpression: "id, companyName, #g, #1, #2, #3, #4, #5, #tz",
                ExclusiveStartKey: lastEvaluatedKey,
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            tenants = [...tenants, ...items];
        } while (lastEvaluatedKey);

        return tenants;
    } catch (err) {
        console.log("[getAllTenants] Error in function getAllTenants", err);
        return {
            error: err,
        };
    }
}

async function createAutomatedCoachingSendTime(tenants){
    console.log('Creating Automated Coaching Time');
    let counter = 0
    for (const tenant of tenants) {
        if(tenant.coachingSendTimeLocal && tenant.coachingSendTimeLocal.includes('T')) {
            let automatedCoachingSendTime = (tenant.coachingSendTimeLocal.split('T')[1]).split('-')[0]
            await updateDynamoRecord({
                TableName: process.env.TENANT_TABLE,
                Key: {"id": tenant.id},
                ExpressionAttributeValues: {
                    ":automatedCoachingSendTime": automatedCoachingSendTime,
                },
                ExpressionAttributeNames: {
                    "#automatedCoachingSendTime": 'automatedCoachingSendTime',
                },
                UpdateExpression: "set #automatedCoachingSendTime = :automatedCoachingSendTime",
                ReturnValues: "UPDATED_NEW"
            })
            counter++;
        }
        else {
            console.log({ tenantWithNOTCoachingSendTimeLocal: tenant });
        }
    }
    console.log({ totalCreatedTenantsAutomatedCoachingTime: counter });
}

module.exports = {
  runHera1372,
};