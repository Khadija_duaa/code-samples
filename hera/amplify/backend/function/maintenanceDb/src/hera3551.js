const {
    scanDynamoRecords,
    updateDynamoRecord
} = require('./dynamodbHelper.js')

async function handlerHera3551() {
    // Uncomment the following line to run the script
    await loadFilteredStaffs()
}

async function loadFilteredStaffs() {
    try {
        let lastEvaluatedKey = null;
        do {
            let params = {
                TableName: process.env.STAFF_TABLE,
                ExpressionAttributeNames: { "#phone": 'phone' },
                ExpressionAttributeValues: { ':parenthesis': "(" },
                FilterExpression: "attribute_exists(#phone) and begins_with(#phone, :parenthesis)",
                ProjectionExpression: "id, #phone",
                ExclusiveStartKey: lastEvaluatedKey
            }
            const result = await scanDynamoRecords(params);
            lastEvaluatedKey = result.LastEvaluatedKey;
            console.log('lastEvaluatedKey: ', lastEvaluatedKey);

            await fixFormatNumber(result.Items)

        } while (lastEvaluatedKey);
    } catch (error) {
        console.log("[loadFilteredStaffs] Error in function loadFilteredStaffs", error);
    } finally {
        console.log('***************************');
        console.log('Update Finished');
        console.log('***************************');
    }
}

async function fixFormatNumber(items) {
    try {
        const updatedItems = [];

        for (const item of items) {
            const originalPhone = item.phone;
            const cleanedPhone = originalPhone.replace(/[^0-9]/g, ''); // Remove non-numeric characters
            const formattedPhone = `+1${cleanedPhone}`; // Add '+1' prefix

            updatedItems.push({
                id: item.id,
                phone: formattedPhone,
            });
        }

        // console.log('Updated items:', updatedItems);

        for (const item of updatedItems) {
            await updatePhone(item)
        }
    } catch (error) {
        console.log('[fixFormatNumber] Error in function fixFormatNumber', error);
    }
}

async function updatePhone(item) {
    try {
        await updateDynamoRecord({
            TableName: process.env.STAFF_TABLE,
            Key: { "id": item.id },
            ExpressionAttributeValues: { ":phone": item.phone },
            ExpressionAttributeNames: { "#phone": 'phone' },
            UpdateExpression: "set #phone = :phone",
            ReturnValues: "UPDATED_NEW"
        })
    } catch (err) {
        console.log("[updatePhone] Error in function updatePhone", err);
        return {
            error: err,
        };
    }
}

module.exports = { handlerHera3551 }; 