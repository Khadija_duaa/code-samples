const AWS = require("aws-sdk");

const env = process.env.ENV
const cloudwatch = new AWS.CloudWatch({apiVersion:"2014-03-28"});
const cloudwatchlogs = new AWS.CloudWatchLogs();
const logGroupName = process.env.LOG_GROUP_NAME || null
const emailSns = process.env.EMAIL_SNS || null
const topicArn = process.env.TOPIC_ARN || null
const namespace = `HeraLogError(${env})`; // Replace with the name of the existing metric in CloudWatch

async function createAlarmError(action){
  
  const alarmName = `${env} - Alarm by Log Errors ${action}`;
  const metricName = `Error${action}Log`; // Replace with the namespace of the metric
  const comparisonOperator = 'GreaterThanOrEqualToThreshold'; // Replace with the desired comparison operator
  const threshold = 1; // Replace with the desired threshold
  const evaluationPeriods = 1; // Replace with the desired number of evaluation periods
  
  try {
    await cloudwatch.putMetricAlarm({
    AlarmName: alarmName,
    ComparisonOperator: comparisonOperator,
    EvaluationPeriods: evaluationPeriods,
    MetricName: metricName,
    Namespace: namespace,
    Threshold: threshold,
    AlarmActions: [topicArn], // Replace with the desired actions for the alarm (SNS ARNs, etc.)
    OKActions: [topicArn],
    Statistic: 'Sum', // Replace with the desired statistic ('SampleCount', 'Average', 'Sum', etc.)
    Period: 60, // Replace with the desired period in seconds
    TreatMissingData: 'notBreaching' // Replace with the desired behavior for missing data
    }).promise();
    
    await new AWS.SNS({ apiVersion: '2010-03-31' }).subscribe({
      TopicArn: topicArn,
      Protocol: 'email',
      Endpoint: emailSns
    }).promise();
    
    console.log(`Alarm "${alarmName}" and notification created successfully.`);
    return { response: true, body: 'Alarm created' };
  } catch (error) {
    console.error('Error creating the alarm:', error);
    return { response: false, body: 'Error creating the alarm and notification' };
  }
  
}

async function createCustomMetric(action){
    
  const metricName = `Error${action}Log`;

  try {
    await cloudwatch.putMetricData({
      Namespace: namespace,
      MetricData: [
        {
          MetricName: metricName,
          Unit: 'Count',
          Value: 1
        },
      ],
    }).promise();

    console.log(`Custom metric created successfully.`);
    return { response: true, body: 'Custom metric created' };
  } catch (error) {
    console.error('Error creating the custom metric:', error);
    return { response: false, body: 'Error creating the the custom metric' };
  }
};

async function createMetricFilter(action){
  
  if(!emailSns){
    return { response: false, body: 'Missing environment variable EMAIL_SNS defined'}
  }
    
  if(!topicArn){
    return { response: false, body: 'Missing environment variable TOPIC_ARN defined'}
  }
  
  const filterName = `Error${action}Filter`; // Replace with the desired name for the metric filter
  const filterPattern = `Error.${action}` // Replace with the desired filter pattern

  try {
    await cloudwatchlogs.putMetricFilter({
      logGroupName: logGroupName,
      filterName: filterName,
      filterPattern: filterPattern,
      metricTransformations: [
        {
          metricNamespace: namespace, // Replace with the desired namespace for the metric
          metricName: `Error${action}Log`, // Replace with the desired name for the metric
          metricValue: '1', // Replace with the desired value for the metric
        },
      ],
    }).promise();

    console.log(`Metric filter "${filterName}" created successfully for log group "${logGroupName}".`);
    return { response: true, body: 'Metric filter created' };
  } catch (error) {
    console.error('Error creating the metric filter:', error);
    return { response: false, body: 'Error creating the metric filter' };
  }
  
}

async function runHera2772(){

    const action = 'Login'

    try {
        const response_metric = await createCustomMetric(action)
        if(!response_metric.response) return response_metric.body
        const response_filter = await createMetricFilter(action)
        if(!response_filter.response) return response_filter.body
        const response_alarm = await createAlarmError(action)
        if(!response_alarm.response) return response_alarm.body
    } catch (e) {
        console.log(e)
    }

}

module.exports = {
    runHera2772
}