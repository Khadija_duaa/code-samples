const { buildUpdateParams, scanDynamoRecords, updateDynamoRecord } = require("./dynamodbHelper.js");

async function runHera1472() {
    var tenants = await getAllTenants();
    
    await setCustomerStatus(tenants)
    console.log(tenants)
}

async function getAllTenants() {
    try {
        let tenants = [];
        let lastEvaluatedKey = null;
        do {
            let params = {
                //ExpressionAttributeNames: {
                //    "#group": "group",
                //},
                TableName: process.env.TENANT_TABLE,
                ProjectionExpression: "id, accountPremiumStatus,  trialExpDate",
                ExclusiveStartKey: lastEvaluatedKey,
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            tenants = [...tenants, ...items];
        } while (lastEvaluatedKey);

        return tenants;
    } catch (err) {
        console.log("[getAllTenants] Error in function getAllTenants", err);
        return {
            error: err,
        };
    }
}

async function setCustomerStatus(tenants){
    let status = 'Unknown'
    for (const tenant of tenants) {
        if(tenant.accountPremiumStatus.length > 0){
            status = await calculateCustomerStatus(tenant.accountPremiumStatus, tenant.trialExpDate)
        }
        let updateParams = buildUpdateParams(process.env.TENANT_TABLE, { customerStatus: status }, tenant.id);
        await updateDynamoRecord(updateParams)
    }
}

async function calculateCustomerStatus(accountPremiumStatus, trialExpDate){
      let plans = []
      let status = 'Unknown'

      for (let plan of accountPremiumStatus) {
        plans.push(plan.toLowerCase());
      }
      
      
      if (plans.includes('trial')) {
        const date = new Date()
        if( new Date(trialExpDate) < date && trialExpDate !== null) {
          status = 'Lapsed Trial'
        }
        status = 'Trial'
      }

      if (plans.includes('bundle')) {
        status = 'Active - Bundle'
      }

      if (plans.includes('standard')) {
        if (plans.length > 1) {
          status = 'Active - Premium'
        }

        status = 'Active - Standard'
      }

      if (plans.includes('none')) {
        status = 'Churned'
      }

      return status
}

module.exports = {
    runHera1472,
};
