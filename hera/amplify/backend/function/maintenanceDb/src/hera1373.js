const {
    scanDynamoRecords,
    updateDynamoRecord,
    getDynamoRecord,
    buildUpdateParams
} = require("./dynamodbHelper.js");


const PARKING_SPACE_CREATE_VEHICLE = 'PARKING_SPACE_CREATE_VEHICLE';
const VEHICLE_TYPE_CREATE_VEHICLE = 'VEHICLE_TYPE_CREATE_VEHICLE';
const AUTHORIZED_TO_DRIVE_CREATE_ASSOCIATE = 'AUTHORIZED_TO_DRIVE_CREATE_ASSOCIATE';
const COUNSELING_SEVERITY_CREATE_COUNSELING = 'COUNSELING_SEVERITY_CREATE_COUNSELING';
const INCIDENT_TYPE_CREATE_INCIDENT = 'INCIDENT_TYPE_CREATE_INCIDENT';

async function handlerHera1373(){
    
    try {
        let lastEvaluatedKey = null;
        do{
            let params = {
                TableName: process.env.OPTIONS_CUSTOM_LIST_TABLE,
                ExpressionAttributeNames: {
                    '#d': 'default'
                },
                ProjectionExpression: "id, #d, optionsCustomListsCustomListsId",
                ExclusiveStartKey: lastEvaluatedKey
            };
            const result = await scanDynamoRecords(params);
            lastEvaluatedKey = result.LastEvaluatedKey;
            console.log('lastEvaluatedKey: ', lastEvaluatedKey);
            
            //await updateOptions(result.Items);

        }while(lastEvaluatedKey);
        
    } catch (error) {
        console.log("[handlerHera1373] Error in function handlerHera1373", error);
    } finally {
        console.log('***************************');
        console.log('Update Finished');
        console.log('***************************');
    }
}


async function updateOptions(options=[]){
    
    for(const item of options){
        let isDefaultForSectionSelected = null;
        if(item.default){
            let getParams = {
                TableName : process.env.CUSTOM_LIST_TABLE,
                Key: {
                    "id": item.optionsCustomListsCustomListsId
                }
            };
            const getResult = await getDynamoRecord(getParams);
            const customList = getResult.Item || {};
            
            const isDefaultForSections = {
                'counseling-severity': COUNSELING_SEVERITY_CREATE_COUNSELING,
                'parking-space': PARKING_SPACE_CREATE_VEHICLE,
                'vehicle-type': VEHICLE_TYPE_CREATE_VEHICLE,
                'incident-type': INCIDENT_TYPE_CREATE_INCIDENT
            };
            
            if(customList.type && isDefaultForSections[customList.type]){
                isDefaultForSectionSelected = [isDefaultForSections[customList.type]];
            }
        
        }
        
        let updatedParams = {
            id: item.id,
            isEnabled: true,
            isDefaultForSections: isDefaultForSectionSelected,
            updatedAt:  new Date().toISOString(),
        };
        
        
        const buildParams = buildUpdateParams(process.env.OPTIONS_CUSTOM_LIST_TABLE, updatedParams, updatedParams.id);
        delete buildParams.ReturnValues;
        
        try {
            await updateDynamoRecord(buildParams);
        } catch (error) {
            console.log("Error in function updateOptions", error);
        }
    }
}

module.exports = { handlerHera1373 };