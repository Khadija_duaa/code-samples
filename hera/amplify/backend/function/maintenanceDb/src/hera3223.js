/**
 * This script sets 'documentImageAccidentId' and 'documentAccidentId' values to 'documentIncidentId'
 * and removes 'documentImageAccidentId' and 'documentAccidentId' fields so every media file attached
 * to an Accident record is on the same field
 */

const {
  scanDynamoRecords,
  updateDynamoRecord,
} = require("./dynamodbHelper");

/**
 * Main function
 */
async function handleHera3223() {
  try {
    // Print Report
    await handlePrintReport();
    // Update Users
    // await handleUpdateRecords();
  } catch (error) {
    console.log(error);
  }
}

/**
 * Handles printing the report of the changes that this script will cause
 */
async function handlePrintReport() {
  const reportObj = {
    summary: { toUpdate: 0, toSkip: 0, },
    toUpdate: { count: 0, items: [] },
    toSkip: { count: 0, items: [] },
  };
  await applyCallbackToAllFilteredDocuments((documentList) => {
    for (const document of documentList) {
      if (document.documentAccidentId && document.documentImageAccidentId) {
        reportObj.summary.toSkip += 1;
        reportObj.toSkip.count += 1;
        reportObj.toSkip.items.push(document);
      }
      else {
        reportObj.summary.toUpdate += 1;
        reportObj.toUpdate.count += 1;
        reportObj.toUpdate.items.push({
          ...document,
          toSet_documentIncidentId: document.documentAccidentId || document.documentImageAccidentId,
        });
      }
    }
  });
  console.log('Report', JSON.stringify(reportObj, null, 2));
}

/**
 * Handles the update of Document records that have 'documentImageAccidentId' attribute set
 */
async function handleUpdateRecords() {
  const MAX_COUNT_PER_BATCH = 20;
  const sendUpdates = async (documentList) => {
    const promises = documentList.map(async (document) => {
      const accidentId = document.documentAccidentId || document.documentImageAccidentId;
      return (await updateDocument(document, accidentId));
    });
    await Promise.all(promises);
  };
  await applyCallbackToAllFilteredDocuments(async (documentList) => {
    let batchUpdateList = [];
    for (const document of documentList) {
      batchUpdateList.push(document);
      if (batchUpdateList.length === MAX_COUNT_PER_BATCH) {
        await sendUpdates(batchUpdateList);
        batchUpdateList = [];
      }
    }
    if (batchUpdateList.length) {
      await sendUpdates(batchUpdateList);
    }
  });
}

/**
 * Updates a Document record, sets 'documentImageAccidentId' value to 'documentAccidentId'
 * and removes 'documentImageAccidentId'
 * @param {Object} document - Document record to update
 * @param {Boolean} accidentId - Accident ID value to set to 'documentImageAccidentId' field
*/
async function updateDocument(document, accidentId) {
  try {
    const params = {
      TableName: process.env.DOCUMENT_TABLE,
      Key: { id: document.id },
      ExpressionAttributeValues: {
        ":accidentId": accidentId,
      },
      ExpressionAttributeNames: {
        "#documentIncidentId": "documentIncidentId",
      },
      UpdateExpression: "set #documentIncidentId = :accidentId",
      ReturnValues: "UPDATED_NEW",
    };
    await updateDynamoRecord(params);
  }
  catch (error) {
    console.log(`Error updating document [ID=${document.id}]`);
  }
}

/**
 * Applies a callback to a list of Documents records that have 'documentImageAccidentId' set as attribute
 * @param {Function} cb - Callback to execute for each group of Document records retrieved from DynamoDB
 */
async function applyCallbackToAllFilteredDocuments(cb) {
  let lastEvaluatedKey = null;
  try {
    do {
      const params = {
        ExpressionAttributeNames: {
          '#id': 'id',
          '#group': 'group',
          '#documentAccidentId': 'documentAccidentId',
          '#documentImageAccidentId': 'documentImageAccidentId',
        },
        TableName: process.env.DOCUMENT_TABLE,
        ProjectionExpression: '#id, #group, #documentAccidentId, #documentImageAccidentId',
        FilterExpression: 'attribute_exists(#documentAccidentId) or attribute_exists(#documentImageAccidentId)',
        ExclusiveStartKey: lastEvaluatedKey,
      };
      const scanResult = await scanDynamoRecords(params);
      await cb(scanResult.Items);
      lastEvaluatedKey = scanResult.LastEvaluatedKey;
      console.log({lastEvaluatedKey});
    }
    while (lastEvaluatedKey);
  }
  catch (error) {
    console.log("[applyCallbackToAllFilteredDocuments] Error", error);
  }
}

module.exports = {
  handleHera3223,
};
