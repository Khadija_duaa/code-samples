const {scanDynamoRecords} = require("./dynamodbHelper");

async function handleHera1790 () {
    let tenants = await getAllTenants()
    let grouped = getDuplicates(tenants)
    console.log("🚀 ~ grouped", grouped)
}

async function getDuplicates (arr) {
    return arr.reduce(function (r, a) {
        r[a.group] = r[a.group] || [];
        r[a.group].push(a);
        return r;
    }, Object.create(null));
}

async function getAllTenants() {
    try {
       let tenants = []
        let lastEvaluatedKey = null
        do {
            let params = {
                ExpressionAttributeNames: {
                    '#group': 'group',
                },
                TableName: process.env.TENANT_TABLE,
                ProjectionExpression: 'id, #group, companyName',
                ExclusiveStartKey: lastEvaluatedKey
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items
            lastEvaluatedKey = scanResult.LastEvaluatedKey
            tenants = [...tenants, ...items]
        } while (lastEvaluatedKey)
        return tenants
    } catch (err) {
        console.log("[getAllTenants] Error in function getAllTenants", err);
        return {
            error: err,
        };
    }
}

async function getMessagesByTenant(tenantId) {
    try {
       let messages = []
        let lastEvaluatedKey = null
        do {
            let params = {
                ExpressionAttributeNames: {
                    '#group': 'group',
                },
                ExpressionAttributeValues: {
                    ':messageTenantId': tenantId
                },
                FilterExpression: "messageTenantId = :messageTenantId",
                TableName: process.env.MESSAGE_TABLE,
                ProjectionExpression: 'id, #group, messageTenantId',
                ExclusiveStartKey: lastEvaluatedKey
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items
            lastEvaluatedKey = scanResult.LastEvaluatedKey
            messages = [...messages, ...items]
        } while (lastEvaluatedKey)
        return messages
    } catch (err) {
        console.log("[getAllTenants] Error in function getAllTenants", err);
        return {
            error: err,
        };
    }
}

module.exports = {
    handleHera1790
}