const { 
    scanDynamoRecords,
    queryDynamoRecords,
    updateDynamoRecord
} = require("./dynamodbHelper");

async function runHera1177 () {

    //  Set Options Custom List
    await setParkingSpaceForVehicles()
}

async function setParkingSpaceForVehicles() {
    const customLists = await getAllParkingSpaceCustomList()
    // console.log({ customLists });
    for (const customList of customLists) {
        // console.log({ customList });
        const { id, group } = customList
        // if(group != 'fantastic-deliveries-20-43') continue

        console.log({ group });
        try {
            // Get all parking space options by gsi
            const parkingSpaceList = await getParkingSpaceOptions(id)
            
            console.log({ parkingSpaceList });
            // console.log({ length: parkingSpaceList.length });
            if(!parkingSpaceList.length) continue

            // Get Vehicles by group
            const vehicles = await getVehiclesByGroup(group)

            // console.log({ vehicles });
            // console.log({ length: vehicles.length });

            if(!vehicles.length) continue
            for (const vehicle of vehicles) {
                // console.log({ vehicle });
                try {
                    let parkingSpaceId = null
                    for(const item of parkingSpaceList) {
                        console.log({ item: item.option });
                        console.log({ vehicle: vehicle.parkingSpace });
                        if(vehicle.parkingSpace == '') {
                            break
                        }
                        else if(item.option.toLowerCase() == vehicle.parkingSpace.toLowerCase()) {
                            parkingSpaceId = item.id
                            break
                        }
                    }
                    // console.log({ parkingSpaceId });

                    if(!parkingSpaceId) continue

                    const updateParams = {
                        TableName: process.env.VEHICLE_TABLE,
                        Key: { id: vehicle.id },
                        ExpressionAttributeNames: {
                            "#vps": "vehicleParkingSpaceId",
                        },
                        ExpressionAttributeValues: {
                            ":vpsId": parkingSpaceId
                        },
                        UpdateExpression: "set #vps=:vpsId",
                        ReturnValues: "UPDATED_NEW",
                    }
                    console.log({ updateParams });
                    await updateDynamoRecord(updateParams)

                } catch (e) {
                    console.log("[For of] Error in function getVehiclesByGroup", e);
                }
            }
        } catch (e) {
            console.log("[setOptionsToParkingSpaceCustomList] Error in function setOptionsToParkingSpaceCustomList: ", e);
        }
    }
}

async function getAllParkingSpaceCustomList() {
    try {
        let customLists = []
        let lastEvaluatedKey = null
        do {
            let params = {
                ExpressionAttributeNames: {
                    '#group': 'group',
                    '#type': 'type',
                },
                ExpressionAttributeValues: {
                    ':type': 'parking-space'
                },
                FilterExpression: "#type = :type",
                TableName: process.env.CUSTOM_LIST_TABLE,
                ProjectionExpression: 'id, #group, #type',
                ExclusiveStartKey: lastEvaluatedKey
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items
            lastEvaluatedKey = scanResult.LastEvaluatedKey
            customLists = [...customLists, ...items]
        } while (lastEvaluatedKey)
        return customLists
    } catch (err) {
        console.log("[getAllParkingSpaceCustomList] Error in function getAllParkingSpaceCustomList", err);
        return {
            error: err,
        };
    }
}

async function getParkingSpaceOptions(id) {
    let optionsList = []
    let lastEvaluatedKey = null
    do {
        try {
            let params = {
                TableName: process.env.OPTIONS_CUSTOM_LIST_TABLE,
                IndexName: "gsi-OptionsCustomLists",
                KeyConditionExpression: "#oclid = :id",
                ExpressionAttributeValues: {
                    ":id": id,
                },
                ExpressionAttributeNames: {
                    "#oclid": "optionsCustomListsCustomListsId",
                    "#g": "group",
                    "#opt": "option"
                },
                ProjectionExpression: 'id, #g, #opt, #oclid',
                ExclusiveStartKey: lastEvaluatedKey,
            };
            let queryResult = await queryDynamoRecords(params);
            let items = queryResult.Items;
            lastEvaluatedKey = queryResult.LastEvaluatedKey;
            optionsList = [...optionsList, ...items];
        } catch (e) {
            console.log("[getParkingSpaceOptions] Error in function getParkingSpaceOptions", e);
        }
    } while (lastEvaluatedKey)
    return optionsList 
}

async function getVehiclesByGroup(group) {
    let vehicles = []
    let lastEvaluatedKey = null
    do {
        try {
            let params = {
                TableName: process.env.VEHICLE_TABLE,
                IndexName: "byGroup",
                KeyConditionExpression: "#g = :g",
                ExpressionAttributeValues: {
                    ":g": group,
                },
                ExpressionAttributeNames: {
                    "#g": "group",
                    "#ps": "parkingSpace",
                    "#n": "name",
                },
                FilterExpression: "attribute_exists(#ps)",
                ProjectionExpression: 'id, #g, #n, #ps',
                ExclusiveStartKey: lastEvaluatedKey,
            };
            let queryResult = await queryDynamoRecords(params);
            let items = queryResult.Items;
            lastEvaluatedKey = queryResult.LastEvaluatedKey;
            vehicles = [...vehicles, ...items];
        } catch (e) {
            console.log("[getVehiclesByGroup] Error in function getVehiclesByGroup", e);
        }
    } while (lastEvaluatedKey)
    return vehicles 
}
module.exports = {
    runHera1177
}