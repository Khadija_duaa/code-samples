const { scanDynamoRecords, updateDynamoRecord } = require("./dynamodbHelper.js");

async function runHera1322() {
	const pendingMessages = await getAllPendingMessages();

	for (const pendingMessage of pendingMessages) {
		await addUpdatedAt(pendingMessage)
	}
}

async function getAllPendingMessages() {
	try {
		let staffs = [];
		let lastEvaluatedKey = null;
		do {
			let params = {
				ExpressionAttributeNames: { "#group": "group", },
				TableName: process.env.PENDING_MESSAGE_TABLE,
				ProjectionExpression: "id, createdAt, #group",
				ExclusiveStartKey: lastEvaluatedKey,
				FilterExpression: "attribute_not_exists(updatedAt)",
			};
			let scanResult = await scanDynamoRecords(params);
			let items = scanResult.Items;
			lastEvaluatedKey = scanResult.LastEvaluatedKey;
			staffs = [...staffs, ...items];
		} while (lastEvaluatedKey);

		return staffs;
	} catch (err) {
		console.log("[getAllPendingMessages] Error in function getAllPendingMessages", err);
		return {
			error: err,
		};
	}
}

async function addUpdatedAt(pendingMessage) {
	try {
		await updateDynamoRecord({
			TableName: process.env.PENDING_MESSAGE_TABLE,
			Key: { "id": pendingMessage.id },
			ExpressionAttributeValues: { ":updatedAt": pendingMessage.createdAt },
			ExpressionAttributeNames: { "#updatedAt": 'updatedAt' },
			UpdateExpression: "set #updatedAt = :updatedAt",
			ReturnValues: "UPDATED_NEW"
		})
	} catch (err) {
		console.log("[addUpdatedAt] Error updating pendingMessage", err);
		return {
			error: err,
		};
	}
}

module.exports = {
	runHera1322,
};