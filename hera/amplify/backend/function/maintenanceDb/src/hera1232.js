const {
    scanDynamoRecords,
    updateDynamoRecord
} = require('./dynamodbHelper.js')

function needUpdate(tenant, defaultSettings, keys){
    return keys.some( key => defaultSettings[key] !== tenant[key] )
}

async function iterateTenants(tenants, defaultSettings, keys){
    for (const tenant of tenants) {
        if(needUpdate(tenant, defaultSettings, keys)){
            const updateParams = {
                TableName: process.env.TENANT_TABLE,
                Key: { id: tenant.id },
                ExpressionAttributeNames: {
                    "#a": "coachingDailyComplianceRateThresholdIssue",
                    "#b": "coachingDailyComplianceRateThreshold",
                    "#c": "coachingDailyComplianceRateThresholdPR",
                    "#d": "coachingDailyComplianceRateThresholdKudo",
                    "#e": "coachingDailyComplianceRateIssue",
                    "#f": "coachingDailyComplianceRateCO",
                    "#g": "coachingDailyComplianceRatePR",
                    "#h": "coachingDailyComplianceRateKudo",
                },
                ExpressionAttributeValues: {
                    ":a": defaultSettings.coachingDailyComplianceRateThresholdIssue,
                    ":b": defaultSettings.coachingDailyComplianceRateThreshold,
                    ":c": defaultSettings.coachingDailyComplianceRateThresholdPR,
                    ":d": defaultSettings.coachingDailyComplianceRateThresholdKudo,
                    ":e": defaultSettings.coachingDailyComplianceRateIssue,
                    ":f": defaultSettings.coachingDailyComplianceRateCO,
                    ":g": defaultSettings.coachingDailyComplianceRatePR,
                    ":h": defaultSettings.coachingDailyComplianceRateKudo
                },
                UpdateExpression: "set #a=:a,#b=:b,#c=:c,#d=:d,#e=:e,#f=:f,#g=:g,#h=:h",
                ReturnValues: "UPDATED_NEW",
            }
            const response = await updateDynamoRecord(updateParams)
            console.log({group: tenant.group, response})
            // process.exit(0)
        }else{
            console.log(tenant.group+' --skip')
        }
    }
}

async function handleAllTenants() {
    const defaultSettings = {
        "coachingDailyComplianceRateThresholdIssue": "75",
        "coachingDailyComplianceRateThreshold": "95",
        "coachingDailyComplianceRateThresholdPR": "100",
        "coachingDailyComplianceRateThresholdKudo": "100",
        "coachingDailyComplianceRateIssue": true,
        "coachingDailyComplianceRateCO": true,
        "coachingDailyComplianceRatePR": true,
        "coachingDailyComplianceRateKudo": true
    }
    const keys = Object.keys(defaultSettings)
    try {
        const scanParams = {
            ExpressionAttributeNames: {
                "#a": "coachingDailyComplianceRateThresholdIssue",
                "#b": "coachingDailyComplianceRateThreshold",
                "#c": "coachingDailyComplianceRateThresholdPR",
                "#d": "coachingDailyComplianceRateThresholdKudo",
                "#e": "coachingDailyComplianceRateIssue",
                "#f": "coachingDailyComplianceRateCO",
                "#g": "coachingDailyComplianceRatePR",
                "#h": "coachingDailyComplianceRateKudo",
                "#0": "group"
            },
            TableName: process.env.TENANT_TABLE,
            ProjectionExpression: "id,#a,#b,#c,#d,#e,#f,#g,#h,#0",
        }
        do {
            const scanResult = await scanDynamoRecords(scanParams)
            await iterateTenants(scanResult.Items, defaultSettings, keys)
            scanParams.ExclusiveStartKey = scanResult.LastEvaluatedKey
        } while (scanParams.ExclusiveStartKey)
    } catch (err) {
        console.log('[handleAllTenants] Error in function', err)
        return {
            error: err
        }
    }
}

async function handleHera1232(){
    await handleAllTenants()
}

module.exports = {
    handleHera1232
}