const { buildUpdateParams, scanDynamoRecords, updateDynamoRecord } = require("./dynamodbHelper.js");

async function runHera1119RemoveUser() {
    const users = await getAllUsers()
    await removeEmptyPhones(users)
}

async function getAllUsers() {
    try {
        let staffs = [];
        let lastEvaluatedKey = null;
        do {
            let params = {
                ExpressionAttributeNames: {
                    "#group": "group",
                },
                
                TableName: process.env.USER_TABLE,
                ProjectionExpression: "id, phone, #group",
                ExclusiveStartKey: lastEvaluatedKey,
                FilterExpression: "attribute_exists(phone) and phone = :empty",
                ExpressionAttributeValues: {
                    ":empty": ""
                }
                
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            staffs = [...staffs, ...items];
        } while (lastEvaluatedKey);

        return staffs;
    } catch (err) {
        console.log("[getAllStaffs] Error in function getAllStaffs", err);
        return {
            error: err,
        };
    }
}


async function removeEmptyPhones(list){
    try {
        for (const user of list) {
        
        await updateDynamoRecord({
            TableName: process.env.USER_TABLE,
            Key: {"id": user.id},
            UpdateExpression: 'remove #phone ',
            ExpressionAttributeNames: { '#phone': 'phone' },
            ReturnValues: "UPDATED_NEW"
        })
        
        console.log("User id:", user.id)
        // return
        }
        
        
    } catch (err) {
        console.log("[hera-1119 remove] Error updating phones", err);
        return {
            error: err,
        };
    }
}

module.exports = {
    runHera1119RemoveUser,
};