const { 
    scanDynamoRecords,
    buildQueryParams,
    queryDynamoRecords,
    buildUpdateParams,
    updateDynamoRecord,
    createDynamoRecord
    } = require('./dynamodbHelper.js')

async function searchAndRepopulateMissingScoreCards(tenants) {
    
    process.exit(0); // script locked.
    
    let year = 2021
    for (const tenant of tenants) {
        for (let week = 40; week <= 45; week++) {

            const staffScoreCards = await getStaffScorecardsByGroupAndYearWeek(tenant.group, year, week)

            if (staffScoreCards.length) {
                const companyScoreCard = await getCompanyScorecardByGroupAndYearWeek(tenant.group, year, week)
                if (!companyScoreCard.length) {
                    console.log('[searchAndRepopulateMissingScoreCards] ERROR: MISSING COMPANY SCORECARD', {
                        tenantId: tenant.id,
                        group: tenant.group,
                        year,
                        week,
                    })
                    
                    await repopulateCompanyScorecards(tenant.id, tenant.group, year.toString(), week.toString())
                    
                }
            }
        }
    }
    
    // let year = 2021
    // let week = 52
    // let group = 'amw-transport-16'
    // let tenantId = '2a9e319c-48ad-42af-81dc-02023144b008'
    
    // await repopulateCompanyScorecards(tenantId, group, year.toString(), week.toString())
    
}

async function getAllTenants() {
    try {
        let tenants = []
        let lastEvaluatedKey = null
        do {
            let params = {
                ExpressionAttributeNames: {
                    '#group': 'group',
                },
                TableName: process.env.TENANT_TABLE,
                ProjectionExpression: 'id, #group',
                ExclusiveStartKey: lastEvaluatedKey
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items
            lastEvaluatedKey = scanResult.LastEvaluatedKey
            tenants = [...tenants, ...items]
        } while (lastEvaluatedKey)

        return tenants

    } catch (err) {
        console.log('[searchAndRepopulateMissingScoreCards] Error in function getAllTenants', err)
        return {
            error: err
        }
    }
}

async function getStaffScorecardsByGroupAndYearWeek(group, year, week) {
    try {
        let staffScorecards = []
        let fields = ['group', 'week', 'year']
        let lastEvaluatedKey = null

        do {
            let queryParams = buildQueryParams(process.env.STAFFSCORECARD_TABLE, fields, group, 'byGroup', lastEvaluatedKey)
            queryParams.ExpressionAttributeNames = {
                ...queryParams.ExpressionAttributeNames,
                '#sk': 'year#week',
            }

            queryParams.ExpressionAttributeValues = {
                ...queryParams.ExpressionAttributeValues,
                ':sk': `${year}#${week}`,
            }

            queryParams.KeyConditionExpression += ' AND #sk = :sk'

            let queryResult = await queryDynamoRecords(queryParams);

            let items = queryResult.Items
            lastEvaluatedKey = queryResult.LastEvaluatedKey
            staffScorecards = [...staffScorecards, ...items]
        } while (lastEvaluatedKey)

        return staffScorecards

    } catch (err) {

        let funcParams = {
            group, 
            year, 
            week
        }
        console.log('[searchAndRepopulateMissingScoreCards] Error in function getStaffScorecardsByGroupAndYearWeek', JSON.stringify(funcParams))
        console.log('[searchAndRepopulateMissingScoreCards] Error in function getStaffScorecardsByGroupAndYearWeek', err)

        return {
            error: err
        }
    }
}

async function getCompanyScorecardByGroupAndYearWeek(group, year, week) {
    try {
        let companyScorecard = []
        let fields = ['group', 'tenantId']
        let lastEvaluatedKey = null

        do {
            let queryParams = buildQueryParams(process.env.COMPANYSCORECARD_TABLE, fields, group, 'byGroup', lastEvaluatedKey)
            queryParams.ExpressionAttributeNames = {
                ...queryParams.ExpressionAttributeNames,
                '#sk': 'yearWeek',
            }

            queryParams.ExpressionAttributeValues = {
                ...queryParams.ExpressionAttributeValues,
                ':sk': parseInt(`${year}${week}`),
            }

            queryParams.KeyConditionExpression += ' AND #sk = :sk'

            let queryResult = await queryDynamoRecords(queryParams);

            let items = queryResult.Items
            lastEvaluatedKey = queryResult.LastEvaluatedKey
            companyScorecard = [...companyScorecard, ...items]
        } while (lastEvaluatedKey)

        return companyScorecard

    } catch (err) {
        let funcParams = {
            group, 
            year, 
            week
        }
        console.log('[searchAndRepopulateMissingScoreCards] Error in function getCompanyScorecardByGroupAndYearWeek', JSON.stringify(funcParams))
        console.log('[searchAndRepopulateMissingScoreCards] Error in function getCompanyScorecardByGroupAndYearWeek', err)

        return {
            error: err
        }
    }
}

async function repopulateCompanyScorecards(tenantId, group, year, week) {
    try {
        let jobs = []
        let fields = ['group', 'jobId', 'key', 'isProcessed', 'isProcessedS', 'results', 'createdAt']
        let lastEvaluatedKey = null
        let oldestYearWeek = 300053
        let newestYearWeek = 198001

        // Settings
        // let tenantId = '5fecf3a8-1d03-4186-8867-1680c88da95b'
        // let group = 'ct-developers-81'
        // let year = '2021'
        // let week = '44'
        let type = 'scorecard'
        let jobStatus = 'SUCCEEDED'
        let isProcessedS = 'true'
        
        do {
            let queryParams = buildQueryParams(process.env.TEXTRACTJOB_TABLE, fields, group, 'byGroup', lastEvaluatedKey)
            
            queryParams.ExpressionAttributeNames = {
                ...queryParams.ExpressionAttributeNames,
                '#jobStatus': 'jobStatus',
                '#year': 'year',
                '#week': 'week',
                '#type': 'type',
                '#isProcessedS': 'isProcessedS'
            }

            queryParams.ExpressionAttributeValues = {
                ...queryParams.ExpressionAttributeValues,
                ':jobStatus': jobStatus,
                ':year': year,
                ':week': week,
                ':type': type,
                ':isProcessedS': isProcessedS
            }

            queryParams.KeyConditionExpression += ' AND #jobStatus = :jobStatus'
            queryParams.FilterExpression = '#year = :year AND #week = :week AND #type = :type AND #isProcessedS = :isProcessedS'

            let queryResult = await queryDynamoRecords(queryParams);

            let items = queryResult.Items
            lastEvaluatedKey = queryResult.LastEvaluatedKey
            jobs = [...jobs, ...items]
        } while (lastEvaluatedKey)

        let jobToProcess = null;
        let currentDate = 0; // 1970

        for (const job of jobs) {

            let jobCreatedAt = new Date(job.createdAt)
            let jobCreatedAtTime = jobCreatedAt.getTime()
            
            if(jobCreatedAtTime > currentDate) {
                jobToProcess = job
                currentDate  =  jobCreatedAtTime
            }

        }

        if(!jobToProcess) {
            console.log('[searchAndRepopulateMissingScoreCards] JobToProcess is null', JSON.stringify(jobs))
            process.exit(0);
        }

        console.log('[searchAndRepopulateMissingScoreCards] JobToProcess', JSON.stringify(jobToProcess))

        let jobsToProcess = [];
        jobsToProcess.push(jobToProcess)

        for (const job of jobsToProcess) {
            let results = JSON.parse(job.results)
            let fields = results.fields
            let week = fields.week ? fields.week : job.week
            week = String(week).padStart(2, '0')
            fields.week = week
            let year = fields.year ? fields.year : job.year
            let yearWeek = parseInt(String(year) + week)
            if (yearWeek < oldestYearWeek) oldestYearWeek = yearWeek
            if (yearWeek > newestYearWeek) newestYearWeek = yearWeek
            let companyScoreCardId = tenantId + yearWeek
            let item = {
                tenantId: tenantId,
                yearWeek: yearWeek,
                group: group,
                scorecardPdf: job.key,
                updatedAt: new Date().toISOString(),
                ...fields
            }

            try {
                // Update record, if that fails, try creating
                let updateParams = buildUpdateParams(process.env.COMPANYSCORECARD_TABLE, item, companyScoreCardId)
                
                await updateDynamoRecord(updateParams);

                let logMessage = {
                    item
                }

                console.log('[searchAndRepopulateMissingScoreCards] Warning unexpected item updated', JSON.stringify(logMessage))
                process.exit(0);

            } catch (e) {

                console.log('[searchAndRepopulateMissingScoreCards] CATCH Warning: failed to update scorecard', JSON.stringify({
                    tenantId: tenantId,
                    yearWeek: yearWeek,
                    group: group
                }))

                try {
                    // Create record, if that fails, log error
                    item.createdAt = new Date().toISOString()
                    item.id = companyScoreCardId
                    let createParams = {
                        TableName: process.env.COMPANYSCORECARD_TABLE,
                        Item: item
                    };
                    
                    await createDynamoRecord(createParams);

                    let logMessage = {
                        item
                    }

                    console.log('[searchAndRepopulateMissingScoreCards] Success Item created', JSON.stringify(logMessage))

                } catch (e) {
                    console.log('[searchAndRepopulateMissingScoreCards] Failed item', JSON.stringify(item))
                    console.log('[searchAndRepopulateMissingScoreCards] Failed to create Company Scorecard.', e)
                    process.exit(0);
                }
            }
        };

    } catch (err) {
        let funcParams = {
            tenantId, 
            group, 
            year, 
            week
        }
        console.log('[searchAndRepopulateMissingScoreCards] Error in function repopulateCompanyScorecards', JSON.stringify(funcParams))
        console.log('[searchAndRepopulateMissingScoreCards] Error in function repopulateCompanyScorecards', err)
    }
}

module.exports = {
    searchAndRepopulateMissingScoreCards,
    getAllTenants
}