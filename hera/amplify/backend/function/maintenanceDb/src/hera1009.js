const {
  buildUpdateParams,
  scanDynamoRecords,
  updateDynamoRecord,
} = require('./dynamodbHelper.js')

async function runHera1009() {
  var users = await getAllUsers()

  for (const user of users) {
    if (user.lastLogin && user.lastLogin.includes('1970')) {
        let updateParams = buildUpdateParams(process.env.USER_TABLE, {lastLogin: ''}, user.id)
        await updateDynamoRecord(updateParams);
    }
  }
}

async function getAllUsers() {
  try {
    let users = []
    let lastEvaluatedKey = null
    do {
      let params = {
        ExpressionAttributeNames: {
          '#group': 'group',
        },
        TableName: process.env.USER_TABLE,
        ProjectionExpression:
          'id, #group, userTenantId, email, createdAt, updatedAt, lastLogin',
        ExclusiveStartKey: lastEvaluatedKey,
      }
      let scanResult = await scanDynamoRecords(params)
      let items = scanResult.Items
      lastEvaluatedKey = scanResult.LastEvaluatedKey
      users = [...users, ...items]
    } while (lastEvaluatedKey)

    return users
  } catch (err) {
    console.log('[getAllUsers] Error in function getAllTenants', err)
    return {
      error: err,
    }
  }
}

module.exports = {
  runHera1009,
}
