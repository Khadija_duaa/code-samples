const { buildUpdateParams, scanDynamoRecords, updateDynamoRecord } = require("./dynamodbHelper.js");

async function runHera1180() {

    //1.- Get all Vehicles
    var vehicles = await getAllVehicles();
    
     //Sort the list by group
    var sortedlist = vehicles.sort(function (a, b) {
        return (a.group < b.group) ? -1 : 1 ;
    });

    //2.- Group Vehicles by Tennant
    var groupedbyGroup = await subgroupArraysByGroup(sortedlist);
    
    var groupFiltered = [];
    
    for(var group of groupedbyGroup) {
         const duplicatedList = group.map(v => v.licensePlate + v.state ).filter((v, i, vIds) => vIds.indexOf(v) !== i);
         const duplicates = group.filter(obj => duplicatedList.includes(obj.licensePlate + obj.state));
         
         var sortedlistb = duplicates.sort(function (a, b) {
            return (a.licensePlate + a.state < b.licensePlate + b.state) ? -1 : 1 ;
         });
         if(sortedlistb.length>0){
            groupFiltered.push(sortedlistb);
         }
    }
    
    //Group vehicle with same phone in subarrays
    var groupedbyPlateAndState = await subgroupArraysByPlateAndState(groupFiltered);
    
    //cleanEmails
    var vehiclesToClean = await cleanVehicles(groupedbyPlateAndState);
    
    console.log(JSON.stringify(vehiclesToClean));
    
}

async function getAllVehicles() {

    try {
        let vehicles = [];
        let lastEvaluatedKey = null;
        do {
            let params = {
                ExpressionAttributeNames: {
                    "#status": "status",
                    "#group": "group",
                    "#state": "state",
                },

                TableName: process.env.VEHICLE_TABLE,
                ProjectionExpression: "id, #group, licensePlate, #state, #status",
                FilterExpression: "attribute_exists(licensePlate) and licensePlate<> :empty",
                FilterExpression: "attribute_exists(#state) and #state<> :empty",
                ExclusiveStartKey: lastEvaluatedKey,
                ExpressionAttributeValues: {
                    ":empty": "",
                }
            };

            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items;
            lastEvaluatedKey = scanResult.LastEvaluatedKey;
            vehicles = [...vehicles, ...items];
        } while (lastEvaluatedKey);
        return vehicles;

    } catch (err) {
        console.log("[getAllVehicles] Error in function getAllVehicles", err);
        return {
            error: err,
        };
    }
}

async function subgroupArraysByGroup(vehicles) {

    //Sort the list by group
    var sortedlist = vehicles.sort(function (a, b) {
        return (a.group < b.group) ? -1 : 1 ;
    });

    var main = []
    var subarray = []

    var initialValue = sortedlist[0]['group'];
        for(var i=0; i<sortedlist.length;i++) {
  	        if(sortedlist[i].group == initialValue) {
                subarray.push(sortedlist[i])
            } else {
     			initialValue = sortedlist[i].group;
                main.push(subarray);
                subarray = [];
                subarray.push(sortedlist[i]);
            }

            if(i==sortedlist.length-1){
                main.push(subarray);
            }
        }     
    return main;
}

async function subgroupArraysByPlateAndState(list){

    var main = [];

    for (const grouped of list) {
        var submain = [];  
        var subarray = [];
        var initialValue = grouped[0]['licensePlate'] + grouped[0]['state'];

        for(var i=0; i<grouped.length;i++) {
  	        if(grouped[i].licensePlate + grouped[i].state == initialValue) {
                subarray.push(grouped[i])
            } else {
     			initialValue = grouped[i].licensePlate + grouped[i].state;
                submain.push(subarray);
                subarray = [];
                subarray.push(grouped[i]);
            }

            if(i==grouped.length-1) {
     	        submain.push(subarray);
            }
        } 

        main.push(submain);
    }

    return main;
}

async function cleanVehicles(list){

    var vehiclesToClean = [];

    for (const grouped of list) {
        var subgroup = [];

        for(const vehicle of grouped) {
            //sort the subarray by status
            var objsorted = vehicle.sort(function (a, b) {
                return (a.status < b.status) ? -1 : 1 ;
            });

            var pos = objsorted.map(function(e) { return e.status; }).indexOf('Active');

            if(pos==-1){
                pos = objsorted.map(function(e) { return e.status; }).indexOf('Inactive - Maintenance');
            }

            if(pos==-1){
                pos = objsorted.map(function(e) { return e.status; }).indexOf('Inactive - Grounded');
            }

            if(pos!=-1) {
                objsorted.splice(pos, 1);
            } else {
                objsorted.splice(0, 1);
            }

            if(objsorted.length>0){
                subgroup.push(objsorted);
            }
            
            await updateVinDynamo(objsorted);
        }
        vehiclesToClean.push(subgroup);
    }

    return vehiclesToClean;

}


async function updateVinDynamo(list){

    try {
        for (const vehicle of list) {

        await updateDynamoRecord({
            TableName: process.env.VEHICLE_TABLE,
            Key: {"id": vehicle.id},
            ExpressionAttributeValues: {":state": null, ":licensePlate": null},
            ExpressionAttributeNames: {"#state": 'state', "#licensePlate": 'licensePlate'},
            UpdateExpression: "set #state = :state, #licensePlate = :licensePlate",
            ReturnValues: "UPDATED_NEW"
        })
        }
    } catch (err) {
        console.log("[hera-1182] Error updating vehicles", err);
        return {
            error: err,
        };
    }
}

module.exports = {
    runHera1180,
};