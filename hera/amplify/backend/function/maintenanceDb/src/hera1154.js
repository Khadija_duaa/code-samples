const {scanDynamoRecords, buildUpdateParams, updateDynamoRecord, createDynamoRecord} = require("./dynamodbHelper");

async function runHera1154 () {
    await setCustomListToTenant()
    await setOptionsToCustomList()
}

async function setOptionsToCustomList() {
    try {
        let customLists = await getAllIncidentTypeCustomList()
        console.log({ customListsLength : customLists.length });
        for (const customList of customLists) {
            console.log({ customList });
            for (const option of optionsCustomList) {
                console.log({ option });
                let createOptionCustomList = {
                    TableName: process.env.OPTIONS_CUSTOM_LIST_TABLE,
                    Item: {
                        id: generateUUID(),
                        group: customList.group,
                        order: option.order,
                        option: option.option,
                        default: option.default,
                        usedFor: option.usedFor,
                        daysCount: option.daysCount,
                        canBeEdited: option.canBeEdited,
                        canBeDeleted: option.canBeDeleted,
                        canBeReorder: option.canBeReorder,
                        createdAt: new Date().toISOString(),
                        updatedAt: new Date().toISOString(),
                        optionsCustomListsCustomListsId: customList.id
                    }
                };
                await createDynamoRecord(createOptionCustomList);
            }
        }
        return true;
    } catch (error) {
        console.log("Error pe: ", error);
    }
}

async function getAllIncidentTypeCustomList() {
    try {
        let customLists = []
        let lastEvaluatedKey = null
        do {
            let params = {
                ExpressionAttributeNames: {
                    '#group': 'group',
                    '#type': 'type',
                },
                ExpressionAttributeValues: {
                    ':type': 'incident-type'
                },
                FilterExpression: "#type = :type",
                TableName: process.env.CUSTOM_LIST_TABLE,
                ProjectionExpression: 'id, #group, #type',
                ExclusiveStartKey: lastEvaluatedKey
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items
            lastEvaluatedKey = scanResult.LastEvaluatedKey
            customLists = [...customLists, ...items]
        } while (lastEvaluatedKey)
        return customLists
    } catch (err) {
        console.log("[getAllIncidentTypeCustomList] Error in function getAllIncidentTypeCustomList", err);
        return {
            error: err,
        };
    }
}

async function setCustomListToTenant() {
    try {
        let tenants = await getAllTenants()
        console.log({ totalTenants: tenants.length });
        for (const tenant of tenants) {
            console.log({ tenant });
            let createCustomList = {
                TableName: process.env.CUSTOM_LIST_TABLE,
                Item: {
                    id: generateUUID(),
                    group: tenant.group,
                    type: 'incident-type',
                    listCategory: 'Associates',
                    listName: 'Incident Type',
                    listDisplay: 'Text Only',
                    canDeleteAllOptions: true,
                    createdAt: new Date().toISOString(),
                    updatedAt: new Date().toISOString()
                }
            };
            await createDynamoRecord(createCustomList);
        }
        return true;
    } catch (error) {
        console.log("Error pe: ", error);
    }
}

async function getAllTenants() {
    try {
       let tenants = []
        let lastEvaluatedKey = null
        do {
            let params = {
                ExpressionAttributeNames: {
                    '#group': 'group',
                },
                TableName: process.env.TENANT_TABLE,
                ProjectionExpression: 'id, #group',
                ExclusiveStartKey: lastEvaluatedKey
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items
            lastEvaluatedKey = scanResult.LastEvaluatedKey
            tenants = [...tenants, ...items]
        } while (lastEvaluatedKey)
        return tenants
    } catch (err) {
        console.log("[getAllTenants] Error in function getAllTenants", err);
        return {
            error: err,
        };
    }
}

function generateUUID() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

const optionsCustomList = [
    {
      order: 1,
      option: "General Incident",
      default: true,
      usedFor: 0,
      daysCount: 0,
      canBeEdited: false,
      canBeDeleted: false,
      canBeReorder: true,
    },
    {
      order: 2,
      option: "Accident",
      default: false,
      usedFor: 0,
      daysCount: 0,
      canBeEdited: false,
      canBeDeleted: false,
      canBeReorder: true,
    },
];

module.exports = {
    runHera1154
}