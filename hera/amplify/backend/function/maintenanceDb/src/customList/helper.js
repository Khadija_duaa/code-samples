const {
    updateDynamoRecord,
    scanDynamoRecords
} = require('../dynamodbHelper')

async function getAllTenants() {
    try {
        let tenants = []
        let lastEvaluatedKey = null
        do {
            let params = {
                ExpressionAttributeNames: {
                    '#group': 'group',
                },
                TableName: process.env.TENANT_TABLE,
                ProjectionExpression: 'id, #group',
                ExclusiveStartKey: lastEvaluatedKey
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items
            lastEvaluatedKey = scanResult.LastEvaluatedKey
            tenants = [...tenants, ...items]
        } while (lastEvaluatedKey)
        return tenants
    } catch (err) {
        console.log("[getAllTenants] Error in function getAllTenants", err);
        return {
            error: err,
        };
    }
}


async function getCustomListbyType(group, type) {
    try {
        let customLists = []
        let lastEvaluatedKey = null
        do {
            let params = {
                ExpressionAttributeNames: {
                    '#group': 'group',
                    '#type': 'type',
                },
                ExpressionAttributeValues: {
                    ':type': type,
                    ':g': group
                },
                FilterExpression: "#type = :type and #group = :g",
                TableName: process.env.CUSTOM_LIST_TABLE,
                ProjectionExpression: 'id, #group, #type',
                ExclusiveStartKey: lastEvaluatedKey
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items
            lastEvaluatedKey = scanResult.LastEvaluatedKey
            customLists = [...customLists, ...items]
        } while (lastEvaluatedKey)
        return customLists
    } catch (err) {
        console.log("[getCustomListbyType] Error in function getCustomListbyType", err);
        return {
            error: err,
        };
    }
}

async function getCustomListItems(item) {
    try {
        let types = []
        let lastEvaluatedKey = null
        do {
            let params = {
                ExpressionAttributeNames: {
                    '#g': 'group',
                    '#opcl': 'optionsCustomListsCustomListsId',
                    '#opt' : 'option'
                },
                ExpressionAttributeValues: {
                    ':g': item.group,
                    ':opcl': item.id,
                },
                FilterExpression: "#g = :g and #opcl = :opcl",
                TableName: process.env.OPTIONS_CUSTOM_LIST_TABLE,
                ProjectionExpression: 'id, #g, #opt',
                ExclusiveStartKey: lastEvaluatedKey
            };
            let scanResult = await scanDynamoRecords(params);
            let items = scanResult.Items
            lastEvaluatedKey = scanResult.LastEvaluatedKey
            types = [...types, ...items]
        } while (lastEvaluatedKey)
        return types
    } catch (err) {
        console.log("[getCustomListItems] Error in function getCustomListItems", err);
        return {
            error: err,
        };
    }
}

async function getVehicleTypeFromCustomList(group) {
    
    let customDelVanId = null
    let standardParcelId = null
    let tenkVanId = null
    let standardParcelSmallId = null
    let standardParcelLargeId = null
    let standardParcelXLId = null

    try {
        let vehicleCustomList = await getCustomListbyType(group, 'vehicle-type')
        console.log({ vehicleCustomList });
        let options = await getCustomListItems(vehicleCustomList[0])
        console.log({ options });
        for(let item of options) {
            if(item.option == '10,000lbs Van') tenkVanId = item.id
            else if(item.option == 'Custom Delivery Van') customDelVanId = item.id
            else if(item.option == 'Standard Parcel') standardParcelId = item.id
            else if(item.option == 'Standard Parcel Small') standardParcelSmallId = item.id
            else if(item.option == 'Standard Parcel Large') standardParcelLargeId = item.id
            else if(item.option == 'Standard Parcel XL') standardParcelXLId = item.id
        }
        return { tenkVanId, customDelVanId, standardParcelId, standardParcelSmallId, standardParcelLargeId, standardParcelXLId }
    } catch (e) {
        console.log("[getVehicleTypeFromCustomList] Error in function getVehicleTypeFromCustomList", e);
        return {
            error: e,
        };
        
    }
}


module.exports = {
    getAllTenants,
    getCustomListbyType,
    getCustomListItems,
    getVehicleTypeFromCustomList
}