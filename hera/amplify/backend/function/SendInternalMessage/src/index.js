var AWS = require('aws-sdk');

var pinpoint = new AWS.Pinpoint({region: process.env.PIN_POINT_REGION});
var projectId = process.env.PROJECT_ID;
var senderEmail = process.env.SENDER_EMAIL;
const { isTestEnv, isValidTestEmail } = require('./testEnvHelpers.js');

exports.handler = async (event) => {

  try {
    
    let request = JSON.parse(event.body);

    let record = {
      destinationEmail: request.to,
      subject: request.subject,
      bodyHtml: request.bodyHtml,
      bodyText: request.bodyText
    }

    await sendEmail(record)

    const response = {
        statusCode: 200,
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "*"
        }, 
        body: JSON.stringify('Send internal message success'),
    };
    return response;

  } catch(e) {
    console.log(e);
  }
  
};

/**
 * Send an email using aws pinpoint
 * @param(Object) record - DB Record with send info
 */
 function sendEmail(record) {
    return new Promise((resolve, reject) => {
      
      //------- ENSURE TEST ENVIRONMENT USES TEST EMAIL ------//
      if (isTestEnv()) {
        if(!!record.destinationEmail && !isValidTestEmail(record.destinationEmail)){ 
          console.log('Exiting sendEmail from the test environment. Destination email does not appear to be a test address:', record)
          return reject("Error attempting to send a message with an invalid test address")
        }
      }      

      //--------VERIFY WE HAVE REQUIRED DATA--------//
      if(!record.subject || !record.bodyHtml || !record.bodyText || !record.destinationEmail){
        console.log("Error: Could not send email, missing required data")
        reject("Error: Could not send email, missing required data")
      }
      
      //----------CONSTRUCT EMAIL PARAMS----------//
      var charset = "UTF-8";
      var params = {
        ApplicationId: projectId,
        MessageRequest: {
          Addresses: {
            [record.destinationEmail]:{
              ChannelType: 'EMAIL'
            }
          },
          MessageConfiguration: {
            EmailMessage: {
              FromAddress: senderEmail,
              SimpleEmail: {
                Subject: {
                  Charset: charset,
                  Data: record.subject
                },
                HtmlPart: {
                  Charset: charset,
                  Data: record.bodyHtml
                },
                TextPart: {
                  Charset: charset,
                  Data: record.bodyText
                }
              }
            }
          }
        }
      };
    
      //----------SEND EMAIL-------------//
      pinpoint.sendMessages(params, function(err, data) {
        // If something goes wrong, print an error message.
        if(err) {
          console.log(err.message);
          reject(err)
        } else {
          console.log(data)
          console.log(data['MessageResponse']['Result'][record.destinationEmail])
          let result = "Email sent! Message ID: " + data['MessageResponse']['Result'][record.destinationEmail]['MessageId']
          console.log(result);
          resolve(result)
        }
      });
    
    })
  }
