/* Amplify Params - DO NOT EDIT
	ENV
	REGION
	STORAGE_HERA_BUCKETNAME
Amplify Params - DO NOT EDIT */

const { generateReadableStream } = require('./scripts/generateStreams');
const { saveFileIntoS3Bucket } = require('./aws-helpers/s3Helpers');

const headers = {
  "Access-Control-Allow-Origin": "*",
  "Access-Control-Allow-Headers": "*",
};

/**
 * Checks if staffData Object has all required attributes
 * @param {Object} staffData data object to validate
 * @returns {Boolean} response to check if data has all required attributes
 */
function hasRequiredAttributes(staffData) {
  if (!staffData || typeof staffData !== 'object') return false;

  const hasAllProps = (obj, props) => props.every(prop => obj.hasOwnProperty(prop));
  const hasSome = (obj, props) => props.some(prop => obj.hasOwnProperty(prop));

  const requiredPropsForStaffData = [
    'companyName',
    'firstName',
    'lastName',
    'counselings',
  ];
  if (!hasAllProps(staffData, requiredPropsForStaffData)) return false;

  if (!Array.isArray(staffData.counselings)) return false;

  const requiredPropsForCounseling = [
    'date',
    'dateSent',
    'status',
    'signature',
    'dateSigned',
    'timeSigned',
    'severity',
    'counselingNotes',
    'consequencesOfFailure',
    'priorDiscussionSummary',
    'correctiveActionSummary',
    'user',
  ];
  const requiredPropsForInfraction = [ 'date', 'time', 'comment', 'infractionType'];
  const requiredPropsForImageAttachment = ['url', 'key'];
  const requiredPropsForDocumentAttachment = ['key', 'name'];

  for (const counseling of staffData.counselings) {
    if (!hasAllProps(counseling, requiredPropsForCounseling)) return false;

    if(!Array.isArray(counseling.infractions)) return false;

    if (counseling.infractions?.length > 0) { // infractions are optional
      for (const infraction of counseling.infractions) {
        if (!hasAllProps(infraction, requiredPropsForInfraction)) return false;
      }
    }

    if (counseling.attachments?.images?.length > 0) { // image attachments are optional
      for (const image of counseling.attachments.images) {
        if (!hasSome(image, requiredPropsForImageAttachment)) return false;
      }
    }
    if (counseling.attachments?.documents?.length > 0) { // document attachments are optional
      for (const document of counseling.attachments.documents) {
        if (!hasAllProps(document, requiredPropsForDocumentAttachment)) return false;
      }
    }
  }

  return true;
}

/**
 * @type {import('@types/aws-lambda').APIGatewayProxyHandler}
 */
exports.handler = async (event) => {
  const staffData = JSON.parse(event.body);

  const isValidBody = hasRequiredAttributes(staffData);
  if (!isValidBody) {
    return {
      statusCode: 400,
      headers,
    };
  }

  const { stream, filename } = await generateReadableStream(staffData);
  const downloadUrl = await saveFileIntoS3Bucket(stream, filename);

  return {
    statusCode: 200,
    body: JSON.stringify({ url: downloadUrl }),
    headers,
  };
};
