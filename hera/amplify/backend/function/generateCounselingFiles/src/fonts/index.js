const path = require('path');

module.exports = {
  Roboto: {
    normal: path.join(__dirname, 'Roboto-Regular.ttf'),
    bold: path.join(__dirname, 'Roboto-Bold.ttf'),
    italics: path.join(__dirname, 'Roboto-Italic.ttf'),
    bolditalics: path.join(__dirname, 'Roboto-BoldItalic.ttf'),
  },
  RobotoBlack: {
    normal: path.join(__dirname, 'Roboto-Black.ttf'),
    bold: path.join(__dirname, 'Roboto-Bold.ttf'),
    italics: path.join(__dirname, 'Roboto-Italic.ttf'),
    bolditalics: path.join(__dirname, 'Roboto-BlackItalic.ttf'),
  },
};
