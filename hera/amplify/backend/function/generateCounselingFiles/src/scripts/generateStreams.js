const moment = require('moment-timezone');

const { getPdfStreamForCounseling } = require('./createPdf');
const { getZipStream } = require('./createZip');
const { readFileFromS3Bucket } = require('../aws-helpers/s3Helpers');
const { TYPE_ZIP, TYPE_PDF } = require('../utils/constants');

/**
 * Creates a filename based on file type
 * @param {Object} staffData
 * @param {String} type type of file to generate the name
 * @returns String
 */
 const getFilename = (staffData, type) => {
  let today = moment().format('MM_DD_YYYY');

  const { firstName, lastName } = staffData;

  if (type === TYPE_ZIP) {
    return `${firstName}_${lastName}-counselings-${today}.zip`;
  }

  if (type === TYPE_PDF) {
    const date = staffData.counselings[0]?.date?.replace(/[/-]/ig, '_');
    return `${firstName}_${lastName}-counseling-${date}-${today}.pdf`;
  }

  return 'unknown';
};

/**
 * Handles the file creation for counselings pdf/zip
 * @param {Object} staffData
 * @returns ReadStream
 */
async function generateReadableStream(staffData) {
  const hasMultipleCounselings = staffData.counselings.length > 1;
  const counseling = staffData.counselings[0];
  const counselingHasAttachments = counseling.attachments?.documents?.length > 0;
  const zipSinglePdfWithAttachments = !hasMultipleCounselings && counselingHasAttachments;

  // Create a zip file if there are multiple Counselings or
  // if there is just one counseling and has attachments
  if (hasMultipleCounselings || counselingHasAttachments) {
    const zipStream = await getZipStream(staffData, zipSinglePdfWithAttachments);
    const zipName = getFilename(staffData, TYPE_ZIP);
    return {
      filename: zipName,
      stream: zipStream,
    };
  }

  if(!staffData.companyLogo && staffData.companyLogoS3Key && staffData.companyLogoS3Key !== '—'){
    staffData.companyLogo = await readFileFromS3Bucket(staffData.companyLogoS3Key)
  }

  const stream = await getPdfStreamForCounseling(staffData, counseling);
  const pdfName = getFilename(staffData, TYPE_PDF);
  return {
    stream,
    filename: pdfName,
  };
}

module.exports = { generateReadableStream };
