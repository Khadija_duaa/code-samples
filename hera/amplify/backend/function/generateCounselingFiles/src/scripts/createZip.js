const JSZip = require('jszip');
const moment = require('moment-timezone');
const { readFileFromS3Bucket } = require('../aws-helpers/s3Helpers');

const { getPdfStreamForCounseling } = require('./createPdf');

/**
 * Handles calculation of correlative numbers for repeated filenames
 * @param {Map} map -> Dictionary to lookup for repeated values
 * @param {String} filename -> File name to lookup for and create a correlative if needed
 * @returns {String} sufix to append if repeated
 */
function correlative(map, filename) {
  if (!map.has(filename)) {
    map.set(filename, 1);
    return '';
  }

  const count = map.get(filename);
  map.set(filename, count + 1);
  return `${count}`;
}

/**
 * Suffixes a string with a count suffix if needed
 * @param {String} filename -> Name of the file to be suffixed if needed
 * @param {String} countSuffix -> Suffix to append to filename
 * @returns {String} Transformed filename
 */
function getSuffixedName(filename='', countSuffix='') {
  if (!countSuffix) return filename;

  const regex = /\.(.*)$/ig;
  const hasExtension = regex.test(filename);

  if (!hasExtension) return `${filename}_${countSuffix}`;

  const splitted = filename.split('.');
  const extension = splitted.pop();

  return splitted.join('.') + `_${countSuffix}.${extension}`;
}

/**
 * Embeds files into the zip or into a folder if 'folderName' is provided
 * @param {JSZip} zip -> JSZip object to embed the files into
 * @param {Array} attachmentList -> List of attachments to be included in the zip file
 * @param {String} pdfName -> Name of the main pdf in the folder
 * @param {String} folderName -> Optional folder to group files
 */
async function embedAttachmentsIntoZip(zip=new JSZip(), attachmentList=[], pdfName='', folderName='') {
  let count = 1;

  const filenamesMap = new Map();
  correlative(filenamesMap, pdfName); // register the main name in case of duplicates

  for (const document of attachmentList) {
    const buffer = await readFileFromS3Bucket(document.key);

    const filename = document.name || `Counseling Attachment #${count++}`;
    const countSuffix = correlative(filenamesMap, filename);

    const fileNameWithSuffix = getSuffixedName(filename, countSuffix);
    const filePath = folderName? `${folderName}/${fileNameWithSuffix}`: fileNameWithSuffix;

    zip.file(filePath, buffer);
  }
}

/**
 * Zip pdf files
 * @param {Object} staffData -> Staff data object
 * @param {Array} pdfFiles -> List of objects with filename and stream properties
 * @returns {stream} readable stream to upload to S3
 */
async function compressPdfsIntoZip(staffData, pdfFiles=[], zipSinglePdfWithAttachments=false) {
  const { firstName, lastName } = staffData;

  const zip = new JSZip();
  const filenamesMap = new Map();

  let today = moment().format('MM_DD_YYYY');

  for(const pdf of pdfFiles) {
    const date = pdf.date.replace(/[/-]/ig, '_');
    const filename = `${firstName}_${lastName}-counseling-${date}-${today}`;

    const pdfHasAttachments = pdf.attachments.length > 0;
    const nameToCompare = pdfHasAttachments? `${filename}.pdf`: filename;

    const countSuffix = correlative(filenamesMap, nameToCompare);
    const fileNameWithSuffix = getSuffixedName(filename, countSuffix);

    const folderPath = zipSinglePdfWithAttachments? '': `${fileNameWithSuffix}/`;
    let pdfFilename = `${fileNameWithSuffix}.pdf`;

    if (pdfHasAttachments) { // Add attachments to zip folder
      pdfFilename = `${folderPath}${filename}.pdf`;
      await embedAttachmentsIntoZip(zip, pdf.attachments, `${filename}.pdf`, folderPath);
    }

    // Add pdf stream to zip
    zip.file(pdfFilename, pdf.stream);
  }

  return await zip.generateNodeStream({
    type:'nodebuffer',
    streamFiles:true
  });
}

/**
 * Create a zip of PDF counselings
 * @param {Array} counselings -> List of objects with counseling data
 * @returns {stream} readable stream to upload to S3
 */
async function getZipStream(staffData, zipSinglePdfWithAttachments=false) {
  const { counselings } = staffData;

  const pdfFiles = await Promise.all(
    counselings.map(async (counseling) => {
      const stream = await getPdfStreamForCounseling(staffData, counseling);
      const attachments = counseling.attachments?.documents || [];

      return {
        stream,
        attachments,
        date: counseling.date,
      };
    })
  );

  const zipStream = await compressPdfsIntoZip(staffData, pdfFiles, zipSinglePdfWithAttachments);
  return zipStream;
}

module.exports = {
  getZipStream,
};
