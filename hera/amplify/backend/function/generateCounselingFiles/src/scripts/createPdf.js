const PdfPrinter = require('pdfmake');
const fonts = require('../fonts');
const { readFileFromS3Bucket } = require('../aws-helpers/s3Helpers');

/**
 * Create the object configuration to embed a Title with an underline the PDF file
 * @param {String} text
 * @returns {Object} configuration to be used to embed titles in PDF
 */
function objectConfigForSectionTitle(text='') {
  return {
    table: {
      widths: ['*'],
      body: [
        [
          {
            text,
            style: 'sectionTitle',
            border: [false, false, false, true],
          },
        ],
      ],
    },
    layout: {
      hLineWidth: function (i, node) {
        return 0.25;
      },
      hLineColor: function (i, node) {
        return '#7c726c';
      },
    }
  };
}

/**
 * Creates an object configuration to embed a Table into PDF file
 * @param {Array} headers -> List of column headers
 * @param {Array} data -> List of column content
 * @param {Object} obj -> Custom styles for column header and content
 * @throws {Error} If headers and data lengths mismatch
 * @returns {Object} Configuration to embed a table into PDF file
 */
function objectConfigurationForTable(headers=[], data=[], {headerStyle, dataStyle, marginHeader}={}) {
  if (headers?.length !== data?.length) {
    const message = `Column count mismatch ${headers.length} !== ${data.length}`;
    throw new Error(message);
  }

  return {
    layout: 'noBorders',
    table: {
      widths: '*'.repeat(headers.length).split(''),
      body: [
        headers.map(text => ({
          text,
          style: headerStyle || 'sectionSubTitle',
          margin: marginHeader || [0, 5, 0, 0],
        })),
        data.map(text => ({
          text,
          style: dataStyle || 'body',
        })),
      ]
    },
  };
}

/**
 * Creates a configuration object list to embed Text into PDF file
 * @param {Array} subsections -> List of objects with title and body attributes
 * @returns {Array} Configuration object list to embed Text into PDF file
 */
function objectConfigListForSubSections(subsections=[]) {
  const contentList = subsections.flatMap(({ title, body }) => [
    {
      text: title,
      style: 'sectionSubTitle',
    },
    {
      text: body,
      style: 'body',
    },
  ]);

  contentList[0].margin = [0, 5, 0, 5];

  return contentList;
}

/**
 * Embeds Counseling Infractions into PDF file as tables
 * @param {Array} infractions -> List of counseling infractions to be embeded into PDF file
 * @returns {Array} COnfiguration object list to embed Tables into PDF file
 */
function objectConfigListForInfractions(infractions=[]) {
  return infractions.map(infraction => [
    objectConfigurationForTable([
      'Infraction Type',
      'Date',
      'Time',
    ],
    [
      infraction.infractionType,
      infraction.date,
      infraction.time,
    ]),
    {
      margin: [0, 0, 0, 10],
      table: {
        widths: ['*'],
        body: [
          [
            {
              text: 'Notes',
              style: 'sectionSubTitle',
              border: [false, false, false, false],
              margin: [-5, 0, 0, 0],
            },
          ],
          [
            {
              text: infraction.comment,
              style: 'body',
              border: [false, false, false, true],
              margin: [-5, 0, 0, 10],
            },
          ],
        ],
      },
      layout: {
        hLineWidth: function (i, node) {
          return 0.25;
        },
        hLineColor: function (i, node) {
          return '#7c726c';
        },
      }
    },
  ]);
}

/**
 * Creates a list of configuration objects to embed images into the PDF file
 * @param {Array} images -> List of images to be embedded into the PDF file
 * @returns {Array} List of configuration objects of each image
 */
async function objectConfigListForImageAttachments(images=[]) {
  const imagesList = await Promise.all(images.map(async img => {
    let image
    if(img.url){
      image = img.url
    }else if(img.key){
      try {
        image = await readFileFromS3Bucket(img.key)
      } catch (error) {
        console.log('objectConfigListForImageAttachments', error)
      }
    }
    return {
      width: 500,
      image,
      margin: [16, 30, 0, 0],
    }
  }));

  // Add a new page to start appending images
  if (imagesList.length > 0) {
    imagesList[0].pageBreak = 'before';
  }

  return imagesList;
}

/**
 * Insert optional fields if any of them have data to display
 * @param {Object} docDefinition -> Configuration object to generate the PDF file
 * @param {Object} counseling -> Object from which to exxtract data for the PDF file
 */
async function appendOptionalFields(docDefinition, counseling) {
  const attachmentImages = counseling.attachments?.images || [];

  /****************************************************************
   * ASSOCIATE ISSUES
   ****************************************************************/
  if (counseling.infractions?.length > 0) {
    docDefinition.content.push(objectConfigForSectionTitle('Associate Issues'));
    docDefinition.content.push(...objectConfigListForInfractions(counseling.infractions));
  }

  /****************************************************************
   * COUNSELING IMAGE ATTACHMENTS
   ****************************************************************/
  if (attachmentImages.length > 0) {
    try {
      const configList = await objectConfigListForImageAttachments(attachmentImages)
      docDefinition.content.push(...configList)
    } catch (error) {
      console.log('appendOptionalFields - error getting config', error)
    }
  }
}


/**
 * Generates a PDF file as readable stream
 * @param {Object} staffData -> Contains staff data to embed into PDF file
 * @param {Object} counseling -> Contains counseling data to embed into PDF file
 * @returns {ReadStream} PDF document as a readable stream
 */
async function getPdfStreamForCounseling(staffData, counseling) {
  let logoImage = { text: '' };

  const { companyLogo } = staffData;
  if (companyLogo && companyLogo !== '—') {
    logoImage = { image: companyLogo, fit: [180, 80] };
  }

  const docDefinition = {
    pageSize: 'LETTER',
    content: [
      logoImage,
      {
        style: 'headLine',
        text: staffData.companyName,
        margin: [0, 10, 0, 15],
      },
      {
        style: 'headLine',
        text: `COUNSELING FOR: ${staffData.firstName} ${staffData.lastName}`,
      },
      {
        style: 'headLine',
        text: `DATE OF OCURRENCE: ${counseling.date}`,
        margin: [0, 3, 0, 7],
      },
      objectConfigurationForTable([
        'Team Leader',
        'Counseling Severity',
      ],
      [
        `${counseling.user.firstName} ${counseling.user.lastName}`,
          counseling.severity,
      ],
      {
        headerStyle: 'sectionSubTitle',
        dataStyle: 'contentHeading',
        marginHeader: [0, 20, 0, 5],
      }),
      /****************************************************************
       * COUNSELING INFORMATION
       ****************************************************************/
      objectConfigForSectionTitle('Counseling Information'),
      ...objectConfigListForSubSections([
        { // DATE LAST SENT
          title: 'Date Last Sent',
          body: counseling.dateSent,
        },
        { // STATEMENT OF PROBLEM
          title: 'Statement of Problem',
          body: counseling.counselingNotes,
        },
        { // PRIOR DISCUSSION SUMMARY
          title: 'Prior Discussion Summary',
          body: counseling.priorDiscussionSummary,
        },
        { // SUMMARY OF CORRECTIVE ACTION
          title: 'Summary of Corrective Action Discussed',
          body: counseling.correctiveActionSummary,
        },
        { // CONSEQUENCES OF FAILURE
          title: 'Consequences of Failure to Improve Performance or Corrective Behaviour',
          body: counseling.consequencesOfFailure,
        },
      ]),
      /****************************************************************
       * SIGNATURE INFORMATION
       ****************************************************************/
      objectConfigForSectionTitle('Signature Information'),
      objectConfigurationForTable([
        'Status',
        'Signature',
        'Date Signed',
        'Time Signed',
      ],
      [
        counseling.status,
        counseling.signature,
        counseling.dateSigned,
        counseling.timeSigned,
      ]),
    ],
    defaultStyle: {
      font: 'Roboto',
      fontSize: 13,
      bold: false,
    },
    styles: {
      headLine: {
        font: 'RobotoBlack',
        color: '#313842',
      },
      sectionTitle: {
        fontSize: 12.5,
        color: '#000',
        margin: [-5, 10, 0, 0],
      },
      sectionSubTitle: {
        fontSize: 11,
        color: '#7c726c',
        margin: [0, 20, 0, 5],
      },
      contentHeading: {
        font: 'RobotoBlack',
        fontSize: 14,
        color: '#000',
        margin: [0, -5, 0, 0],
      },
      externalLink: {
        font: 'Roboto',
        fontSize: 11,
        color: '#0000ee',
      },
      body: {
        fontSize: 10.3,
        lineHeight: .925,
        characterSpacing: .5,
        alignment: 'justify',
        color: '#000',
        bold: true,
      },
    },
  };

  try {
    await appendOptionalFields(docDefinition, counseling);
    const printer = new PdfPrinter(fonts);
    const pdfDoc = printer.createPdfKitDocument(docDefinition);
    pdfDoc.end();
    return pdfDoc;
  } catch (error) {
    console.log('getPdfStreamForCounseling - error getting stream data', error)
    throw error
  }

}

module.exports = {
  getPdfStreamForCounseling,
};
