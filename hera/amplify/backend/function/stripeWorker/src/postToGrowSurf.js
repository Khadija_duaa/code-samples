const https = require('https');
const { getSecretValue } = require('/opt/nodejs/ssm');
/** 
 * Promise wrapper for GrowSurf request. 
 * https://api.growsurf.com/v2/campaign/:csff71/participant/:{participantId}/ref
 *
 * @async
 * @function postToGrowSurf
 * @param {string} growSurfParticipantId - GrowSurf participant ID linking tenant to referrer.
 * @return {Promise<string>} - Data from GrowSurf API response.
 */

module.exports = async function postToGrowSurf(growSurfParticipantId){	
	const growSurfApikey = await getSecretValue(process.env.GROWSURF_API_KEY);
	const growSurfCampaignId = process.env.GROWSURF_CAMPAIGN_ID;
    const growSurfPath = `/v2/campaign/${growSurfCampaignId}/participant/${growSurfParticipantId}/ref`;
	
	let options = {
		method: 'POST',
		host: 'api.growsurf.com',
		path: growSurfPath,
		headers: { 
			Authorization: `Bearer ${growSurfApikey}`
		}
	};
	
	return new Promise((resolve, reject) => {
		const req = https.request(options, res => {
			let rawData = '';
			res.on('data', chunk => rawData += chunk);
			res.on('end', () => {
		    	try {
					resolve(JSON.parse(rawData));
				} catch (err) {
					reject(new Error(err));
				}
			});
		});
		
		req.on('error', err => reject(new Error(err)) );
		req.write('');
		req.end();
	});
}