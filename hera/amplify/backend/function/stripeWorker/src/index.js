/* Amplify Params - DO NOT EDIT
	API_HERA_CARDTABLE_ARN
	API_HERA_CARDTABLE_NAME
	API_HERA_GRAPHQLAPIIDOUTPUT
	API_HERA_INVOICETABLE_ARN
	API_HERA_INVOICETABLE_NAME
	API_HERA_MESSAGETABLE_ARN
	API_HERA_MESSAGETABLE_NAME
	API_HERA_STRIPEQUEUETABLE_ARN
	API_HERA_STRIPEQUEUETABLE_NAME
	API_HERA_TENANTTABLE_ARN
	API_HERA_TENANTTABLE_NAME
	ENV
	REGION
Amplify Params - DO NOT EDIT *//*
Use the following code to retrieve configured secrets from SSM:

const aws = require('aws-sdk');

const { Parameters } = await (new aws.SSM())
  .getParameters({
    Names: ["GROWSURF_API_KEY","STRIPE_SECRET_KEY"].map(secretName => process.env[secretName]),
    WithDecryption: true,
  })
  .promise();

Parameters will be of the form { Name: 'secretName', Value: 'secretValue', ... }[]
*/
// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
const postToGrowSurf = require('./postToGrowSurf.js')

// Set the region
AWS.config.update({ region: process.env.REGION });
const { generateTemplate } = require('./emailTemplate')

var ddb = new AWS.DynamoDB.DocumentClient({ apiVersion: '2012-08-10' });

// Set your secret key. Remember to switch to your live secret key in production!
// See your keys here: https://dashboard.stripe.com/account/apikeys
const { getSecretValue } = require('/opt/nodejs/ssm')
let stripe;

const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

exports.handler = async (event) => {

    try {

        console.log('---event', JSON.stringify(event))

        for (const dynoRecord of event.Records) {
            const record = dynoRecord.dynamodb.NewImage
            if (dynoRecord.eventName == "INSERT") {
                console.log('---new Invoice Insert Record', JSON.stringify(record))

                let stripeQueueId = record.id.S
                let tenantS = record.tenant.S

                let tenant = JSON.parse(tenantS)
                console.log('+-----------------------+')
                await charge(tenant, stripeQueueId)
                console.log('+-----------------------+')
            }
        }

    } catch (e) {
        console.log('---Something went wrong', e)
    }

    console.log('Done!')

    // TODO implement
    const response = {
        statusCode: 200,
        body: JSON.stringify('Done!'),
    };
    return response;
};

async function initStripe(){    
    if(!stripe){
        console.log("Initializing Stripe Module");
        const stripeKey = await getSecretValue(process.env.STRIPE_SECRET_KEY)
        stripe = require('stripe')(stripeKey);
    }
    return stripe;
}
async function charge(tenant, stripeQueueId) {

    console.log('---charge tenant', tenant)
    if (!!tenant.accountPremiumStatus?.includes('None')) {
        console.log('Tenant has plan of None, skipping', tenant)
        return;
    }

    // get Stripe customer id
    const customerId = tenant.stripeCustomerId
    if (!customerId) {
        // not set up in stripe, throw error and email tenant
        console.log("Error: Customer not set up in Stripe. Customer: ", tenant.companyName)

        // email Hera if tenant billing email not set up
        // console.log("Error: Customer billing email not set up.")
        return;
    }

    stripe = await initStripe();
    
    // get invoice for last month
    var group = tenant.group;
    var today = new Date()
    //var now = new Date();
    //var today = new Date(now.getFullYear(), now.getMonth()+1, 1)
    var year = today.getFullYear()
    var month = today.getMonth()

    // deal with new year
    if (month == 0) {
        month = 11
        year--
    }
    else {
        month--
    }
    var monthName = months[month]

    let params = {
        ExpressionAttributeValues: {
            ':g': group,
            ':y': String(year) + '#' + String(month),

        },
        ExpressionAttributeNames: {
            '#g': 'group',
            '#y': 'year#month',
            '#s': 'status'
        },
        KeyConditionExpression: '#g = :g AND #y = :y',
        ProjectionExpression: 'id, #y, #g, invoiceTotal, discountPercent, discountFixed, #s',
        TableName: process.env.API_HERA_INVOICETABLE_NAME,
        IndexName: 'byGroupYearMonth',
    };
    var queryResult = await queryDynamoRecords(params);
    var items = queryResult.Items
    var invoice = items[0]

    // throw error if no invoice from previous month
    if (!invoice) {
        console.log("Error: no invoice from last month.", tenant.companyName)
        return
    }

    // skip if already paid
    if (invoice.status == 'Paid') {
        console.log("Skipping " + tenant.companyName + ": Invoice already paid.")
        return
    }

    // set up totals
    const taxRate = 0
    const fixedDiscount = invoice.discountFixed ? invoice.discountFixed : 0
    const percentDiscount = invoice.discountPercent ? invoice.discountPercent / 100 * invoice.invoiceTotal : 0
    const subtotalLessDiscount = invoice.invoiceTotal - fixedDiscount - percentDiscount
    const subtotalPlusTaxes = (subtotalLessDiscount + subtotalLessDiscount * taxRate).toFixed(2)
    const invoiceTotal = parseInt(subtotalPlusTaxes.replace(/[^0-9]/g, '')) // pass total in cents to Stripe
    console.log({ invoiceTotal: invoiceTotal })

    // throw error if total is 0
    if (invoiceTotal <= 0) {
        console.log("Error: $0 balance on invoice.", tenant.companyName)
        return
    }



    // get customer's cards from Dynamo
    params = {
        ExpressionAttributeValues: {
            ':g': group,
        },
        ExpressionAttributeNames: {
            '#g': 'group',
        },
        KeyConditionExpression: '#g = :g',
        ProjectionExpression: 'id, stripePaymentMethodId, active',
        TableName: process.env.API_HERA_CARDTABLE_NAME,
        IndexName: 'byGroup',
    };
    var queryResult = await queryDynamoRecords(params);
    var cards = queryResult.Items

    // find active card
    var activeCard = cards.find(card => {
        return card.active == true
    })
    const activeCardDynamoId = activeCard.id
    const paymentMethodId = activeCard.stripePaymentMethodId

    // get payment method from Stripe
    const paymentMethod = await stripe.paymentMethods.retrieve(
        paymentMethodId
    );

    // check if card is set to expire soon
    const expMonth = paymentMethod.card.exp_month
    const expYear = paymentMethod.card.exp_year
    const lastFour = paymentMethod.card.last4
    const exp = String(expYear) + String(expMonth).padStart(2, '0')
    const currMonth = String(year) + String(month + 1).padStart(2, '0')
    const monthsLeft = (parseInt(exp) - parseInt(currMonth) - 1) % 88 // mod it to account for transition between years, -1 because invoice is for previous month
    console.log({ monthsLeft: monthsLeft })
    if (monthsLeft <= 3) {
        if (monthsLeft <= 0) {
            var subject = "Hera: Active Payment Method Expired"
            var header = "Active Payment Method Expired"
            var emailBody = `Your active payment method has expired. Please update your active payment method through your <a href="dsp.herasolutions.app/settings/account-details">Hera account settings</a> to not lose access to Hera.<br><br>Exp: ${expMonth}/${expYear}`
        }
        else {
            var subject = "Hera: Active Payment Method Expiring Soon"
            var header = "Active Payment Method Expiring Soon"
            var emailBody = `Your active payment method will be expiring soon. Please make sure to update your active payment method through your <a href="dsp.herasolutions.app/settings/account-details">Hera account settings</a>.<br><br>Exp: ${expMonth}/${expYear}`
        }
        // send email about card expiration
        var createParams = {
            TableName: process.env.API_HERA_MESSAGETABLE_NAME,
            Item: {
                id: `${year}-${month + 1}-${tenant.id}-exp`,
                messageTenantId: tenant.id,
                createdAt: new Date().toISOString(),
                updatedAt: new Date().toISOString(),
                group: group,
                subject: subject,
                bodyText: emailBody,
                bodyHtml: generateTemplate(header, emailBody, tenant.companyName),
                channelType: 'EMAIL',
                messageType: 'billingError',
                destinationEmail: tenant.stripeBillingEmail
            }
        };
        var create = await createDynamoRecord(createParams);
    }

    // create a payment intent in Stripe
    var status = ''
    var queueStatus = ''
    try {
        const paymentIntent = await stripe.paymentIntents.create({
            amount: invoiceTotal,
            currency: 'usd',
            customer: customerId,
            payment_method: paymentMethodId,
            off_session: true,
            confirm: true,
        });
        
        console.log('---paymentItents Paid', JSON.stringify({
            group: tenant.group,
            paymentIntentId: paymentIntent.id,
            amount: invoiceTotal,
            currency: 'usd',
            customer: customerId,
            payment_method: paymentMethodId,
            off_session: true,
            confirm: true,
        }))
        status = 'Paid'
        queueStatus = 'Paid'

        try{
            if('growSurfParticipantId' in tenant && 'growSurfReferralComplete' in tenant){

				// Only proceed if the referral is NOT complete 
				if( !!tenant.growSurfParticipantId && !tenant.growSurfReferralComplete){
					
					// post to GrowSurf
					// `api.growsurf.com/v2/campaign/csff71/participant/${data.growSurfParticipantId}/ref`					
					const growSurfReferralRequest = await postToGrowSurf(tenant.growSurfParticipantId)
					
					if(growSurfReferralRequest.success === true){
						// update tenant
						let updateGrowSurfParams = {
							TableName: process.env.API_HERA_TENANTTABLE_NAME,
							Key: {
								"id": tenant.id
							},
							UpdateExpression: "set #a = :a",
							ExpressionAttributeNames: {
								"#a": "growSurfReferralComplete"
							},
							ExpressionAttributeValues: {
								":a": true
							},
							ReturnValues: "UPDATED_NEW"
						};
						await updateDynamoRecord(updateGrowSurfParams);
						console.log('GrowSurf referral success for tenant', tenant.id)
					}else{
						console.log('GrowSurf referral failed. GS ParticipantId:', tenant.growSurfParticipantId, 'GS fail message: ', growSurfReferralRequest.message)
					}
                }else{
					console.log('growsurf exits, but has been updated')
				}
			}
        }catch(e){
            console.log('Error Posting to GrowSurf:', e, 'Tenant: ', tenant, 'Payment ID: ', paymentIntent.id, 'Invoice total: ', invoiceTotal, 'customerID: ', customerId)

        }

    } catch (err) {
        // Error code will be authentication_required if authentication is needed
        console.log('---Error code is: ', err.code);
        console.log('---Status code is: ', err.statusCode)
        console.log({ error: err })
        
        if (err.statusCode != 429 && err.statusCode != 503) {
            
            const paymentIntentRetrieved = await stripe.paymentIntents.retrieve(err.raw.payment_intent.id);
            console.log('PI retrieved: ', paymentIntentRetrieved.id);

            // send error to billing email if necessary
            const subject = `Hera: Payment Issue for ${tenant.companyName} for ${monthName} ${year}`
            const header = "Payment Issue"
            var emailBody = `There was an issue charging your active card for ${monthName} ${year}. Please resolve the errors listed below by managing your saved cards through your <a href="dsp.herasolutions.app/settings/account-details">Hera account settings</a>.<br><br>${err}`
            var createParams = {
                TableName: process.env.API_HERA_MESSAGETABLE_NAME,
                Item: {
                    id: `${year}-${month + 1}-${tenant.id}-err`,
                    messageTenantId: tenant.id,
                    createdAt: new Date().toISOString(),
                    updatedAt: new Date().toISOString(),
                    group: group,
                    subject: subject,
                    bodyText: emailBody,
                    bodyHtml: generateTemplate(header, emailBody, tenant.companyName),
                    channelType: 'EMAIL',
                    messageType: 'billingError',
                    destinationEmail: tenant.stripeBillingEmail
                }
            };
            var create = await createDynamoRecord(createParams);

            // add error message to card in Dynamo
            var updateCardParams = {
                TableName: process.env.API_HERA_CARDTABLE_NAME,
                Key: {
                    "id": activeCardDynamoId
                },
                UpdateExpression: "set #a = :a",
                ExpressionAttributeNames: {
                    "#a": "chargeError",

                },
                ExpressionAttributeValues: {
                    ":a": err.raw.message,
                },
                ReturnValues: "UPDATED_NEW"
            };
            await updateDynamoRecord(updateCardParams);

            // update Tenant with unpaid invoice status
            let currentAccountStatus = tenant.accountPremiumStatus
            if (!currentAccountStatus.includes('unpaid')) currentAccountStatus.push('unpaid')

            let date = new Date();
            date.setDate(date.getDate() + 7);
            console.log({ payOutstandingByDate: tenant.payOutstandingByDate })
            console.log({ date: date })

            var payOutstandingByDate
            if (!tenant.payOutstandingByDate || JSON.stringify(tenant.payOutstandingByDate) == '{}') payOutstandingByDate = date.toISOString()
            else payOutstandingByDate = tenant.payOutstandingByDate

            console.log({ payOutstandingByDate: payOutstandingByDate })
            let updateParams = {
                TableName: process.env.API_HERA_TENANTTABLE_NAME,
                Key: {
                    "id": tenant.id
                },
                UpdateExpression: "set #a = :a, #b = :b",
                ExpressionAttributeNames: {
                    "#a": "accountPremiumStatus",
                    "#b": "payOutstandingByDate"
                },
                ExpressionAttributeValues: {
                    ":a": currentAccountStatus,
                    ":b": payOutstandingByDate
                },
                ReturnValues: "UPDATED_NEW"
            };
            await updateDynamoRecord(updateParams);

            status = 'Payment Error'
            queueStatus = 'Payment Error'

        } else {

            console.log('---Stripe error 429, 503', err)
            status = 'Pending'
            queueStatus = 'Retry'

        }
    }
    // update status of invoice
    var updateParams = {
        TableName: process.env.API_HERA_INVOICETABLE_NAME,
        Key: {
            "id": invoice.id
        },
        UpdateExpression: "set #a = :a, #b = :b",
        ExpressionAttributeNames: {
            "#a": "status",
            "#b": "cardLastFourCharged"
        },
        ExpressionAttributeValues: {
            ":a": status,
            ":b": lastFour
        },
        ReturnValues: "UPDATED_NEW"
    };
    await updateDynamoRecord(updateParams);

    var updateParams = {
        TableName: process.env.API_HERA_STRIPEQUEUETABLE_NAME,
        Key: {
            "id": stripeQueueId
        },
        UpdateExpression: "set #a = :a, #b = :b",
        ExpressionAttributeNames: {
            "#a": "invoiceId",
            "#b": "result"
        },
        ExpressionAttributeValues: {
            ":a": invoice.id,
            ":b": queueStatus
        },
        ReturnValues: "UPDATED_NEW"
    };
    await updateDynamoRecord(updateParams);

    return invoice.id

}

function queryDynamoRecords(params) {
    return new Promise((resolve, reject) => {
        // Create DynamoDB service object
        ddb.query(params, function (err, data) {
            if (err) {
                console.error("Unable to get items. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                console.log("Get Item succeeded:", JSON.stringify(data, null, 2));
                resolve(data)
            }
        });
    })
}

function updateDynamoRecord(params) {
    return new Promise((resolve, reject) => {
        ddb.update(params, function (err, data) {
            if (err) {
                console.log("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
                resolve(data)
            }
        });
    })

}

function createDynamoRecord(params) {
    console.log("Creating Record");
    return new Promise((resolve, reject) => {
        ddb.put(params, function (err, data) {
            if (err) {
                console.log("Unable to put item. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                console.log("PutItem succeeded:", JSON.stringify(data, null, 2));
                resolve(data)
            }
        });


    })
}