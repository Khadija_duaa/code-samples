/* Amplify Params - DO NOT EDIT
	API_HERA_ATTACHMENTTABLE_ARN
	API_HERA_ATTACHMENTTABLE_NAME
	API_HERA_GRAPHQLAPIIDOUTPUT
	ENV
	REGION
	STORAGE_HERA_BUCKETNAME
Amplify Params - DO NOT EDIT *//*
Copyright 2017 - 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at
    http://aws.amazon.com/apache2.0/
or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
*/




var express = require('express')
var bodyParser = require('body-parser')
var awsServerlessExpressMiddleware = require('aws-serverless-express/middleware')
var AWS = require('aws-sdk');
const XLSX = require('xlsx')
var documentClient = new AWS.DynamoDB.DocumentClient({apiVersion: '2012-08-10'})
var s3 = new AWS.S3({apiVersion: '2006-03-01'});

// declare a new express app
var app = express()
app.use(bodyParser.json())
app.use(awsServerlessExpressMiddleware.eventContext())

// Enable CORS for all methods
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "*")
  next()
});


/**********************
 * Example get method *
 **********************/

app.get('/download', function(req, res) {
  // Add your code here
  res.json({success: 'get call succeed!', url: req.url});
});

app.get('/download/:id', async function(req, res) {
  // Add your code here
  var messageAttachmentId = req.params.id
  
  try{
      //Get an attachment record's data
      var params = {
        TableName : process.env.API_HERA_ATTACHMENTTABLE_NAME,
        Key: {
          "id": messageAttachmentId
        }
      };
      var findResult = await getDynamoRecord(params)
      if(!findResult.Item){
         return res.json({error: `Sorry, this link is invalid or has expired. Please speak with your company manager to get a new link.`}) 
       }
      var s3Key = findResult.Item.s3Key
      var fileName = s3Key.substr( s3Key.lastIndexOf('/') + 1)
      var now = Math.round((new Date()).getTime() / 1000)
      var createdAt = new Date(findResult.Item.createdAt)
      var expiry  = (findResult.Item.expirationDate) ? findResult.Item.expirationDate : createdAt.setDate(createdAt.getDate() + 7)
      
      let log = {
        "date": now,
        "expirationDate": expiry
      }

      console.info(log);

      if(now > expiry){
         return res.json({error: `Sorry, this link is invalid or has expired. Please speak with your company manager to get a new link.`}) 
      }
      
      //Generate a link from S3
      var params = {Bucket: process.env.STORAGE_HERA_BUCKETNAME, Key: 'public/' + s3Key};
      var url = s3.getSignedUrl('getObject', params)
      
      res.json({success: 'get call succeed!', url: url, fileName: fileName });
  }catch(e){
      res.json({error: e ? e : "There was an error retrieving the file", fileName: fileName});
  }

  

});

/****************************
* Example post method *
****************************/

app.post('/download', function(req, res) {
  // Add your code here
  
  //TEMP DOWNLOAD PAGE
  
  
  
  res.json({success: 'post call succeed!', url: req.url, body: req.body})
});

app.post('/download/*', function(req, res) {
  // Add your code here
  res.json({success: 'post call succeed!', url: req.url, body: req.body})
});

/****************************
* Example put method *
****************************/

app.put('/download', function(req, res) {
  // Add your code here
  res.json({success: 'put call succeed!', url: req.url, body: req.body})
});

app.put('/download/*', function(req, res) {
  // Add your code here
  res.json({success: 'put call succeed!', url: req.url, body: req.body})
});


/****************************
* Example delete method *
****************************/

app.delete('/download', function(req, res) {
  // Add your code here
  res.json({success: 'delete call succeed!', url: req.url});
});

app.delete('/download/*', function(req, res) {
  // Add your code here
  res.json({success: 'delete call succeed!', url: req.url});
});

/****************************
* Download Hera Templates *
****************************/

app.get('/download-templates', async function(req, res) {
  try {
    const params = {Bucket: req.query.bucket, Key: req.query.key};
  
    const csvFile = await s3.getObject(params).promise()
    const workbook = XLSX.read(csvFile.Body, {type: 'array'})
    const csvBody = XLSX.utils.sheet_to_csv(workbook.Sheets.Sheet1);

    res.json({success: 'Get call succeed!', csvBody});
  } catch (e) {
    res.status(500)
    res.json({error: "There was an error retrieving the file"});
  }
});

app.listen(3000, function() {
    console.log("App started")
});



// Export the app object. When executing the application local this does nothing. However,
// to port it to AWS Lambda we will create a wrapper around that will load the app from
// this file
module.exports = app

function getDynamoRecord(params){
  return new Promise((resolve, reject) => {
      documentClient.get(params, function(err, data) {
          if (err) {
                console.error("Unable to get item. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                console.log("Get Item succeeded:", JSON.stringify(data, null, 2));
                resolve(data)
            }
      });
  })
}