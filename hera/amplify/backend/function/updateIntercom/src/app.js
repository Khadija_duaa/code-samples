/*
Use the following code to retrieve configured secrets from SSM:

const aws = require('aws-sdk');

const { Parameters } = await (new aws.SSM())
  .getParameters({
    Names: ["VUE_APP_INTERCOM_TOKEN"].map(secretName => process.env[secretName]),
    WithDecryption: true,
  })
  .promise();

Parameters will be of the form { Name: 'secretName', Value: 'secretValue', ... }[]
*/
/* Amplify Params - DO NOT EDIT
	API_HERA_GRAPHQLAPIENDPOINTOUTPUT
	API_HERA_GRAPHQLAPIIDOUTPUT
	API_HERA_TENANTTABLE_ARN
	API_HERA_TENANTTABLE_NAME
	API_HERA_USERTABLE_ARN
	API_HERA_USERTABLE_NAME
	ENV
	REGION
Amplify Params - DO NOT EDIT *//*
Copyright 2017 - 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at
http://aws.amazon.com/apache2.0/
or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
*/

const express = require('express')
const bodyParser = require('body-parser')
const awsServerlessExpressMiddleware = require('aws-serverless-express/middleware')

// declare a new express app
const app = express()
app.use(bodyParser.json())
app.use(awsServerlessExpressMiddleware.eventContext())

const { getCompaniesByTenantId, updateCompany } = require('./intercom')

// Enable CORS for all methods
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
});

app.post('/intercom-update/',async function(req, res) {
    try {
        let body = req.body
        let intercomCompany = await getCompaniesByTenantId(body.id)    
        let intercomCompanyId = intercomCompany.data.id    
        let updateIntercomCompany = await updateCompany(intercomCompanyId, { name: body.name })    
        if(updateIntercomCompany.status != 200) throw "ERROR_UPDATE_INTERCOM_COMPANY"     
        console.log("Result Intercon Update: ", { success: 'post call succeed!', updateIntercomCompany }) 
        res.json({ success: 'Company Update' }) 
    } catch (e) {
        console.log("updateIntercomCompany", e)    
        res.json({error: 'post call failed', url: req.url, body: e})
    }
});

app.listen(3000, function() {
    console.log("App started")
});

// Export the app object. When executing the application local this does nothing. However,
// to port it to AWS Lambda we will create a wrapper around that will load the app from
// this file
module.exports = app