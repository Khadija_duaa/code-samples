const { queryDynamoRecords, scanDynamoRecords } = require("./dynamodbHelper.js");
const { executeQuery } = require('./appSync.js')
const {
  vehicleIds,
  staffIds,
  invoicesByGroup,
} = require('./queries')

async function getTenants() {
  try {
    let tenants = [];
    let lastEvaluatedKey = null;
    do {
      let params = {
        ExpressionAttributeNames: {
          "#group": "group",
        },
        TableName: process.env.API_HERA_TENANTTABLE_NAME,
        ProjectionExpression: "id, companyName, #group, shortCode, accountPremiumStatus, trialExpDate, customerStatus",
        ExclusiveStartKey: lastEvaluatedKey,
      };
      let scanResult = await scanDynamoRecords(params);
      let items = scanResult.Items;
      lastEvaluatedKey = scanResult.LastEvaluatedKey;
      tenants = [...tenants, ...items];
    } while (lastEvaluatedKey);

    return tenants;
  } catch (err) {
    console.log("[getTenants] Error in function getTenants", err);
    return {
      error: err,
    };
  }
}

async function getUsersByTenantId(tenantId) {
  try {
    let users = []
    let lastEvaluatedKey = null
    const queryParams = {
      ExpressionAttributeNames: {
        '#id': 'id',
        '#userTenantId': 'userTenantId',
        '#g': 'group',
      },
      ExpressionAttributeValues: {
        ':userTenantId': tenantId,
      },
      TableName: process.env.API_HERA_USERTABLE_NAME,
      IndexName: 'gsi-TenantUsers',
      KeyConditionExpression: '#userTenantId = :userTenantId',
      ProjectionExpression:
        '#id, #userTenantId, #g, email, firstName, lastName',
    }
    do {
      const queryResult = await queryDynamoRecords(queryParams)
      const items = queryResult.Items
      lastEvaluatedKey = queryResult.LastEvaluatedKey
      users = [...users, ...items]
    } while (lastEvaluatedKey)
    return users
  } catch (error) {
    console.log('[getUsersByTenantId] Error in function getUsersByTenantId', error)
    return {
      error: error,
    }
  }
}

async function getCompanyInput(tenant) {
  //Vehicle count
  let vehicleInput = { group: tenant.group, filter: { status: { eq: "Active" } } }
  const vehicleResponse = await gLoadListAll(vehicleIds, vehicleInput, 'vehiclesByGroup')
  const vehicleCount = vehicleResponse.length

  // Staff count
  let staffInput = { group: tenant.group, filter: { status: { eq: "Active" } } }
  const staffResponse = await gLoadListAll(staffIds, staffInput, 'staffsByGroup')
  const staffCount = staffResponse.length

  // // Months invoiced
  let invoicesInput = { group: tenant.group }
  const invoicesResponse = await gLoadListAll(invoicesByGroup, invoicesInput, 'invoicesByGroup')

  let updateInput = {
    name: tenant.companyName,
    plan: formatPlan(tenant),
    company_id: tenant.id,
    custom_attributes: {
      active_vehicles: vehicleCount,
      active_das: staffCount,
      trial_expiration_date: trialExpirationDate(tenant),
      dsp_short_code: tenant.shortCode,
      months_invoiced: invoicesResponse.length,
      total_invoiced: calculateTotalInvoiced(invoicesResponse),
      customer_status: tenant.customerStatus
    },
  }

  return updateInput
}

function calculateTotalInvoiced(invoices) {
  let total = 0
  invoices.forEach(invoice => {
    total += invoice.invoiceTotal
  })

  return total.toFixed(2)
}

function formatPlan(tenant) {
  if (!tenant.accountPremiumStatus) {
    return ''
  }

  let accountPremiumStatus = tenant.accountPremiumStatus

  if (
    accountPremiumStatus.length > 1 &&
    accountPremiumStatus.includes('standard')
  ) {
    let filtered = accountPremiumStatus.filter(status => status != 'standard')

    let abbreviations = {
      staff: 'DA',
      performance: 'Performance',
      rostering: 'Roster',
      vehicles: 'Vehicles',
    }
    const abbreviated = filtered.map(item => abbreviations[item])

    return `Standard+${abbreviated.join('-')}`
  }

  if (
    accountPremiumStatus.includes('trial') &&
    tenant.trialExpDate < this.currentDateFormatted
  ) {
    return 'Expired'
  }

  return toTitleCase(accountPremiumStatus[0])
}

function toTitleCase(value) {
  return value
    .split(' ')
    .map(word => word.charAt(0).toUpperCase() + word.substr(1).toLowerCase())
    .join(' ')
}

function trialExpirationDate(tenant) {
  if (!tenant.trialExpDate) {
    return ''
  }

  return tenant.trialExpDate.toString().split('T')[0]
}

async function gLoadListAll(query, input, queryName) {
  var nextToken = null
  var list = []
  do {
    input.nextToken = nextToken
    var response
    try {
      response = await executeQuery(query, input)
      list = [...list, ...response[queryName].items]
      nextToken = response[queryName].nextToken
    } catch (err) {
      console.log(err)
    }
  } while (nextToken)
  return list
}

module.exports = {
  getCompanyInput,
  getTenants,
  getUsersByTenantId
}
