const axios = require('axios')
const { getSecretValue } = require('/opt/nodejs/ssm')
let accessToken;
let instance;
async function loadAccessToken(){
  if(!accessToken){
    console.log("Retrieve AccessToken from Value");
    accessToken = await getSecretValue(process.env.VUE_APP_INTERCOM_TOKEN)
  }
}

async function initIntercom(){
  if(!instance){
    await loadAccessToken();
    instance = axios.create({
      baseURL: 'https://api.intercom.io',
      headers: {
        Authorization: `Bearer ${accessToken}`,
        'Content-Type': 'application/json',
      },
    })
  }
  
}

async function getCompanies(fromPage, toPage) {
  let params = { per_page: 50, page: fromPage }
  let companies = []
  let next = false
  await initIntercom();
  do {
    try {
      let response = await instance.get('/companies', { params })
      next = response.data.pages.next

      response.data.data.forEach(company => {
        companies.push(company)
      })

      params = {
        per_page: response.data.pages.per_page,
        page: response.data.pages.page + 1,
      }
    } catch (error) {
      console.error('[getCompanies]', error)
    }
  } while (next && params.page <= toPage)

  return companies
}

async function getContacts(companyId) {
  let params = { per_page: 50, page: 1 }
  let contacts = []
  let next = false
  await initIntercom();
  do {
    try {
      let response = await instance.get(`/companies/${companyId}/contacts`, {
        params,
      })

      next = response.data.pages.next

      response.data.data.forEach(contact => {
        contacts.push(contact)
      })

      params = {
        per_page: response.data.pages.per_page,
        page: response.data.pages.page + 1,
      }
    } catch (error) {
      console.error('[getContacts]', error)
    }
  } while (next)

  return contacts
}

function getPageRange() {
  const FIRST_HOUR = 2
  const SECOND_HOUR = 3
  let range = {
    from: 1,
    to: 3,
  }
  let hours = new Date().getHours()
  let minutes = new Date().getMinutes()

  if (hours == FIRST_HOUR && minutes >= 0 && minutes <= 14) {
    range = {
      from: 1,
      to: 3,
    }
  } else if (hours == FIRST_HOUR && minutes >= 15 && minutes <= 29) {
    range = {
      from: 4,
      to: 6,
    }
  } else if (hours == FIRST_HOUR && minutes >= 30 && minutes <= 44) {
    range = {
      from: 7,
      to: 9,
    }
  } else if (hours == FIRST_HOUR && minutes >= 45 && minutes <= 59) {
    range = {
      from: 10,
      to: 12,
    }
  } else if (hours == SECOND_HOUR && minutes >= 0 && minutes <= 14) {
    range = {
      from: 13,
      to: 15,
    }
  } else if (hours == SECOND_HOUR && minutes >= 15 && minutes <= 29) {
    range = {
      from: 16,
      to: 18,
    }
  } else if (hours == SECOND_HOUR && minutes >= 30 && minutes <= 44) {
    range = {
      from: 19,
      to: 21,
    }
  } else if (hours == SECOND_HOUR && minutes >= 45 && minutes <= 59) {
    range = {
      from: 22,
      to: 24,
    }
  }

  return range
}

async function updateCompany(id, input) {
  await initIntercom();
  return await instance.put(`/companies/${id}`, input)
}

async function updateContact(id, input) {
  await initIntercom();
  return await instance.put(`/contacts/${id}`, input)
}

async function deleteContact(id) {
  await initIntercom();
  return await instance.delete(`/contacts/${id}`)
}

async function archiveContact(id) {
  await initIntercom();
  return await instance.post(`/contacts/${id}/archive`)
}

async function deleteCompany(id) {
  await initIntercom();
  return await instance.delete(`/companies/${id}`)
}

async function archiveCompany(id) {
  await loadAccessToken();
  return await axios.post(
    'https://api.intercom.io/tags',
    {
      'name': 'To Be Archived',
      'companies': [
        {
          'id': id
        }
      ]
    },
    {
      headers: {
        'Authorization': `Bearer ${accessToken}`,
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    }
  );
}

async function getCompaniesByTenantId(id) {
  await loadAccessToken();
  return await axios.get('https://api.intercom.io/companies', {
    params: {
      'company_id': id
    },
    headers: {
      'Authorization': `Bearer ${accessToken}`,
      'Accept': 'application/json'
    }
  });
}

module.exports = {
  getCompanies,
  getContacts,
  getPageRange,
  updateCompany,
  updateContact,
  deleteContact,
  deleteCompany,
  archiveContact,
  archiveCompany,
  getCompaniesByTenantId
}
