/* Amplify Params - DO NOT EDIT
	API_HERA_GRAPHQLAPIENDPOINTOUTPUT
	API_HERA_GRAPHQLAPIIDOUTPUT
	ENV
	REGION
Amplify Params - DO NOT EDIT */

const {
  getCompanies,
  getContacts,
  getPageRange,
  updateCompany,
  updateContact,
  deleteContact,
  deleteCompany,
  archiveContact,
  archiveCompany,
  getCompaniesByTenantId,
} = require('./intercom')

const { getTenants, getCompanyInput, getUsersByTenantId } = require('./tenants')

const awsServerlessExpress = require('aws-serverless-express');
const app = require('./app');

/** 
 * @type {import('http').Server} 
 */
const server = awsServerlessExpress.createServer(app);

/** 
 * @type {import('@types/aws-lambda').APIGatewayProxyHandler} 
 */

exports.handler = async (event, context) => {
  let path = event.path

  if(path == '/intercom-update/'){
    console.log(`EVENT: ${JSON.stringify(event)}`);
    return awsServerlessExpress.proxy(server, event, context, 'PROMISE').promise;
  }else{
    let range = getPageRange()
    const companies = await getCompanies(range.from, range.to)
    const tenants = await getTenants()
    
    if(!tenants.length) {
      console.error("Not tenants found.")
      return
    }
    
    const tenantsIds = tenants.map(tenant => {
      return tenant.id
    })
    
    let adminList = [
      'matthew@junotransit.com', 
      'luisdavidgd+heraprod@gmail.com'
    ]
    let companyMatched = 0
    let companyUnmatched = 0
    let usersMatched = 0
    let usersUnmatched = 0
    
    for (const company of companies) {
      if (tenantsIds.includes(company.company_id)) {
        companyMatched++
        const tenant = tenants.find(tenant => tenant.id === company.company_id)
        console.log("---Tenant: ", tenant)
        const companyInput = await getCompanyInput(tenant)
        console.log("---CompanyInput: ", companyInput)
    
        try {
          const companyResponse = await updateCompany(company.id, companyInput)
          console.log('Company Updated', JSON.stringify(companyResponse.data))
        } catch (error) {
          console.log('[Error updating Company]', { companyInput }, { company }, error)
        }
    
        const contacts = await getContacts(company.id)
        const users = await getUsersByTenantId(tenant.id)
        const userIds = users.map(user => {
          return user.id
        })
    
        for (const contact of contacts) {
          if (userIds.includes(contact.external_id)) {
            usersMatched++
            const user = users.find(user => user.id === contact.external_id)
            let name = user.firstName + ' ' + user.lastName
            if (contact.name != name) {
              const contactInput = {
                name: name,
              }
              
              try {
                const contactResponse = await updateContact(contact.id, contactInput)
                console.log('Contact updated', JSON.stringify(contactResponse.data))
              } catch (error) {
                console.log('[Error updating Contact]', { contactInput }, { contact }, error)
              }
            }
          } else {
            usersUnmatched++
            if(adminList.includes(contact.email)) {
              console.log('Skip delete admin email', contact.email)
            } else {
              console.log('Contact for delete', JSON.stringify(contact))
              try {
                const contactResponse = await archiveContact(contact.id)
                console.log('Contact archive', JSON.stringify(contactResponse.data))
                console.log('contact archive')
              } catch (error) {
                console.log('[Error archive Contact]', contact, error)
              }
            }
          }
        }
      } else {
        companyUnmatched++
        const contacts = await getContacts(company.id)
    
        for (const contact of contacts) {
          try {
            const contactResponse = await archiveContact(contact.id)
            console.log('Contact archive', JSON.stringify(contactResponse.data))
          } catch (error) {
            console.log('[Error archive Contact]', contact, error)
          }
        }
    
        try {
          const companyResponse = await archiveCompany(company.id)
          console.log('Tag created', JSON.stringify(companyResponse.data))
        } catch (error) {
          console.log('[Error created tag Company]', { company }, error)
        }
      }
    }
    
    const response = {
      statusCode: 200,
      companies: companies.length,
      tenants: tenants.length,
      companyMatched: companyMatched,
      companyUnmatched: companyUnmatched,
      usersMatched: usersMatched,
      usersUnmatched: usersUnmatched,
      range: range
    }

    return response
  }
}
