const createMessage = /* GraphQL */ `
mutation CreateMessage(
  $input: CreateMessageInput!
  $condition: ModelMessageConditionInput
) {
  createMessage(input: $input, condition: $condition) {
    id
    group
    createdAt 
    channelType
    staffId
    bodyText
    isReadS
    messageType
    staff{
      id
      status
    }
  }
}
`;

const createNotification = /* GraphQL */ `
  mutation CreateNotification(
    $input: CreateNotificationInput!
    $condition: ModelNotificationConditionInput
  ) {
    createNotification(input: $input, condition: $condition) {
      id
      group
      owner
      title
      createdAt
      description
      releaseNotes
      payload
      clickAction
      isRead
      updatedAt
    }
  }
`;
                    

module.exports = {
    createMessage,
    createNotification
};