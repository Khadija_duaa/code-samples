/* Amplify Params - DO NOT EDIT
	API_HERA_GRAPHQLAPIENDPOINTOUTPUT
	API_HERA_GRAPHQLAPIIDOUTPUT
	API_HERA_MESSAGETABLE_ARN
	API_HERA_MESSAGETABLE_NAME
	API_HERA_NOTIFICATIONTABLE_ARN
	API_HERA_NOTIFICATIONTABLE_NAME
	API_HERA_TASKTABLE_ARN
	API_HERA_TASKTABLE_NAME
	ENV
	REGION
Amplify Params - DO NOT EDIT */
/*
Copyright 2017 - 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at
    http://aws.amazon.com/apache2.0/
or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
*/




var express = require('express')
var bodyParser = require('body-parser')
var awsServerlessExpressMiddleware = require('aws-serverless-express/middleware')

const AWS = require('aws-sdk');
var ddb = new AWS.DynamoDB.DocumentClient({apiVersion: '2012-08-10'});
const stepfunctions = new AWS.StepFunctions();
const stateMachineArn = process.env.STATEMACHINE_ARN;

// declare a new express app
var app = express()
app.use(bodyParser.json())
app.use(awsServerlessExpressMiddleware.eventContext())

// Enable CORS for all methods
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "*")
  next()
});

const {
  executeMutation,
} = require('./appSync.js');

const {
  createMessage,
  createNotification
} = require('./mutations.js');


/**********************
 * Example get method *
 **********************/

app.get('/notification', function(req, res) {
  // Add your code here
  res.json({success: 'get call succeed!', url: req.url});
});

app.get('/notification/*', function(req, res) {
  // Add your code here
  res.json({success: 'get call succeed!', url: req.url});
});

/****************************
* Example post method *
****************************/

app.post('/notification', async function(req, res) {
  // Add your code here
  console.log({req: req})
  const body = req.body
  const reminderDate = body.reminderDate
  const notificationType = body.notificationType
  const taskID = body.taskID
  const table = process.env.API_HERA_MESSAGETABLE_NAME
  const message = body.message
  const notification = body.notification
  const notificationTable = process.env.API_HERA_NOTIFICATIONTABLE_NAME
  const apiEndpoint = process.env.API_ENDPOINT
  const apiStage = process.env.API_STAGE
  
  //no need to save ARN if the notification is immidiate
  //only save execution ARN if the reminder is in the future
  if(new Date().getTime() < new Date(reminderDate).getTime() || notificationType == "create"){
      const result = await stepfunctions.startExecution({
        stateMachineArn,
        input: JSON.stringify({
            reminderDate, table, message, notificationTable, notification, apiEndpoint, apiStage
        }),
      }).promise();
      console.log(result)
      var executionARN = result.executionArn
    
    
      var updateParams = {
          TableName: process.env.API_HERA_TASKTABLE_NAME,
          Key:{
              "id": taskID
          },
          UpdateExpression: "set #a = :a",
          ExpressionAttributeNames:{
            "#a": "executionARN",
          },
          ExpressionAttributeValues:{
            ":a": executionARN,
          },
          ReturnValues:"UPDATED_NEW"
      };
      await updateDynamoRecord(updateParams);
  }


  
  res.json({success: 'post call succeed!', url: req.url, body: req.body})
});

app.post('/notification/*', function(req, res) {
  // Add your code here
  res.json({success: 'post call succeed!', url: req.url, body: req.body})
});

/****************************
* Example put method *
****************************/

app.put('/notification', async function(req, res) {
  // Add your code here
  
  //If the PUT is called, we need to terminate the step function and start a new one.
  const body = req.body
  const removalARN = body.executionARN
  const reminderDate = body.reminderDate
  const taskID = body.taskID
  const table = process.env.API_HERA_MESSAGETABLE_NAME
  const message = body.message
  const notification = body.notification
  const notificationTable = process.env.API_HERA_NOTIFICATIONTABLE_NAME

  //terminate the old step function, if there is one
  if (removalARN){
    var result = await stepfunctions.stopExecution({
      'executionArn': removalARN
    }).promise();
    console.log(result)   
  }
  
  //no need to save ARN if the notification is immidiate
  //only save execution ARN if the reminder is in the future
  if(new Date().getTime() < new Date(reminderDate).getTime()){
      
      //start a new step function
      var result = await stepfunctions.startExecution({
        stateMachineArn,
        input: JSON.stringify({
            reminderDate, table, message, notificationTable, notification
        }),
      }).promise();
      console.log(result)
      const executionARN = result.executionArn
    
      var updateParams = {
          TableName: process.env.API_HERA_TASKTABLE_NAME,
          Key:{
              "id": taskID
          },
          UpdateExpression: "set #a = :a",
          ExpressionAttributeNames:{
            "#a": "executionARN",
          },
          ExpressionAttributeValues:{
            ":a": executionARN,
          },
          ReturnValues:"UPDATED_NEW"
      };
      await updateDynamoRecord(updateParams);
 
  }
  
  
  res.json({success: 'put call succeed!', url: req.url, body: req.body})
});

app.put('/notification/*', function(req, res) {
  // Add your code here
  res.json({success: 'put call succeed!', url: req.url, body: req.body})
});

/****************************
* Example delete method *
****************************/

app.delete('/notification', async function(req, res) {
  // Add your code here
  
  const body = req.body
  const executionARN = body.executionARN
  
  const result = await stepfunctions.stopExecution({
    'executionArn': executionARN
  }).promise();
  console.log(result)
  
  res.json({success: 'delete call succeed!', url: req.url});
});

app.delete('/notification/*', function(req, res) {
  // Add your code here
  res.json({success: 'delete call succeed!', url: req.url});
});

app.post('/sendNotification', async function(req, res) {
  // Add your code here
  const body = req.body
  const message = body.message
  const notification = body.notification
  
  // create in-app notification
  let input = {...notification}
  let response = await executeMutation(createNotification, {input})
  console.log(response)
  
  // create SMS/Email message notification
  input = {...message}
  response = await executeMutation(createMessage, {input})
  console.log(response)
  
  
  
  res.json({success: 'post call succeed!', url: req.url, body: req.body})
});

app.listen(3000, function() {
    console.log("App started")
});

// Export the app object. When executing the application local this does nothing. However,
// to port it to AWS Lambda we will create a wrapper around that will load the app from
// this file
module.exports = app

function updateDynamoRecord(params){
    return new Promise((resolve, reject) => {
        ddb.update(params, function(err, data) {
            if (err) {
                console.log("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
                resolve(data)
            }
        });
  })
}



