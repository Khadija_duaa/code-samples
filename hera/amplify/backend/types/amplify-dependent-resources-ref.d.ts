export type AmplifyDependentResourcesAttributes = {
    "function": {
        "hera4e362e32PostConfirmation": {
            "Name": "string",
            "Arn": "string",
            "LambdaExecutionRole": "string",
            "Region": "string"
        },
        "hera4e362e32PreTokenGeneration": {
            "Name": "string",
            "Arn": "string",
            "LambdaExecutionRole": "string",
            "Region": "string"
        },
        "AdminQueriesad20795a": {
            "Name": "string",
            "Arn": "string",
            "Region": "string",
            "LambdaExecutionRole": "string"
        },
        "test": {
            "Name": "string",
            "Arn": "string",
            "Region": "string",
            "LambdaExecutionRole": "string"
        },
        "S3Triggerf426b7b4": {
            "Name": "string",
            "Arn": "string",
            "Region": "string",
            "LambdaExecutionRole": "string"
        },
        "sendPinPointMessage": {
            "Name": "string",
            "Arn": "string",
            "Region": "string",
            "LambdaExecutionRole": "string"
        },
        "handleTextResponse": {
            "Name": "string",
            "Arn": "string",
            "Region": "string",
            "LambdaExecutionRole": "string"
        },
        "ManageNotificationPreferences": {
            "Name": "string",
            "Arn": "string",
            "Region": "string",
            "LambdaExecutionRole": "string"
        },
        "updateStaffsLatestScorecard": {
            "Name": "string",
            "Arn": "string",
            "Region": "string",
            "LambdaExecutionRole": "string"
        },
        "sendCoachingFeedback": {
            "Name": "string",
            "Arn": "string",
            "Region": "string",
            "LambdaExecutionRole": "string",
            "CloudWatchEventRule": "string"
        },
        "Signup": {
            "Name": "string",
            "Arn": "string",
            "Region": "string",
            "LambdaExecutionRole": "string"
        },
        "SaveTenantBilling": {
            "Name": "string",
            "Arn": "string",
            "Region": "string",
            "LambdaExecutionRole": "string",
            "CloudWatchEventRule": "string"
        },
        "addCounselingSignature": {
            "Name": "string",
            "Arn": "string",
            "Region": "string",
            "LambdaExecutionRole": "string"
        },
        "stripeSetup": {
            "Name": "string",
            "Arn": "string",
            "Region": "string",
            "LambdaExecutionRole": "string"
        },
        "stripeChargeTenants": {
            "Name": "string",
            "Arn": "string",
            "Region": "string",
            "LambdaExecutionRole": "string",
            "CloudWatchEventRule": "string"
        },
        "ScheduleNotifications": {
            "Name": "string",
            "Arn": "string",
            "Region": "string",
            "LambdaExecutionRole": "string"
        },
        "s3downloadlink": {
            "Name": "string",
            "Arn": "string",
            "Region": "string",
            "LambdaExecutionRole": "string"
        },
        "ParsePerformanceData": {
            "Name": "string",
            "Arn": "string",
            "Region": "string",
            "LambdaExecutionRole": "string"
        },
        "maintenanceDb": {
            "Name": "string",
            "Arn": "string",
            "Region": "string",
            "LambdaExecutionRole": "string"
        },
        "sendMessage": {
            "Name": "string",
            "Arn": "string",
            "Region": "string",
            "LambdaExecutionRole": "string"
        },
        "SendInternalMessage": {
            "Name": "string",
            "Arn": "string",
            "Region": "string",
            "LambdaExecutionRole": "string"
        },
        "updateIntercom": {
            "Name": "string",
            "Arn": "string",
            "Region": "string",
            "LambdaExecutionRole": "string",
            "CloudWatchEventRule": "string"
        },
        "handleTelnyxResponse": {
            "Name": "string",
            "Arn": "string",
            "Region": "string",
            "LambdaExecutionRole": "string"
        },
        "handleTelnyxData": {
            "Name": "string",
            "Arn": "string",
            "Region": "string",
            "LambdaExecutionRole": "string"
        },
        "handleTrialExpired": {
            "Name": "string",
            "Arn": "string",
            "Region": "string",
            "LambdaExecutionRole": "string"
        },
        "stripeWorker": {
            "Name": "string",
            "Arn": "string",
            "Region": "string",
            "LambdaExecutionRole": "string"
        },
        "stripeChargeTenant": {
            "Name": "string",
            "Arn": "string",
            "Region": "string",
            "LambdaExecutionRole": "string"
        },
        "ForceLogoutAndRefresh": {
            "Name": "string",
            "Arn": "string",
            "Region": "string",
            "LambdaExecutionRole": "string"
        },
        "sendMessages": {
            "Name": "string",
            "Arn": "string",
            "Region": "string",
            "LambdaExecutionRole": "string"
        },
        "sendPendingCoachingMessage": {
            "Name": "string",
            "Arn": "string",
            "Region": "string",
            "LambdaExecutionRole": "string"
        },
        "ProcessPerformanceData": {
            "Name": "string",
            "Arn": "string",
            "Region": "string",
            "LambdaExecutionRole": "string"
        },
        "heraappsync": {
            "Arn": "string"
        },
        "heraawsserverlessexpress": {
            "Arn": "string"
        },
        "heraaxioslayer": {
            "Arn": "string"
        },
        "herabandwidth": {
            "Arn": "string"
        },
        "heraexpress": {
            "Arn": "string"
        },
        "heraexpresshandlebars": {
            "Arn": "string"
        },
        "herajsonwebtoken": {
            "Arn": "string"
        },
        "herajszip": {
            "Arn": "string"
        },
        "heramoment": {
            "Arn": "string"
        },
        "herananoid": {
            "Arn": "string"
        },
        "herapdflib": {
            "Arn": "string"
        },
        "herapdfmakelib": {
            "Arn": "string"
        },
        "herapdfparse": {
            "Arn": "string"
        },
        "herasecretmanager": {
            "Arn": "string"
        },
        "herashorturl": {
            "Arn": "string"
        },
        "herastripe": {
            "Arn": "string"
        },
        "heraxlsxlayer": {
            "Arn": "string"
        }      
    },
    "auth": {
        "hera4e362e32": {
            "IdentityPoolId": "string",
            "IdentityPoolName": "string",
            "UserPoolId": "string",
            "UserPoolName": "string",
            "AppClientIDWeb": "string",
            "AppClientID": "string",
            "AppClientSecret": "string"
        }
    },
    "api": {
        "hera": {
            "GraphQLAPIIdOutput": "string",
            "GraphQLAPIEndpointOutput": "string"
        },
        "AdminQueries": {
            "RootUrl": "string",
            "ApiName": "string"
        },
        "ManageNotificationPreferences": {
            "RootUrl": "string",
            "ApiName": "string",
            "ApiId": "string"
        },
        "Signup": {
            "RootUrl": "string",
            "ApiName": "string",
            "ApiId": "string"
        },
        "addCounselingSignature": {
            "RootUrl": "string",
            "ApiName": "string",
            "ApiId": "string"
        },
        "stripeSetup": {
            "RootUrl": "string",
            "ApiName": "string",
            "ApiId": "string"
        },
        "bandwidth": {
            "RootUrl": "string",
            "ApiName": "string",
            "ApiId": "string"
        },
        "ScheduleNotifications": {
            "RootUrl": "string",
            "ApiName": "string",
            "ApiId": "string"
        },
        "s3downloadlink": {
            "RootUrl": "string",
            "ApiName": "string",
            "ApiId": "string"
        },
        "sendMessage": {
            "RootUrl": "string",
            "ApiName": "string",
            "ApiId": "string"
        },
        "SendInternalMessage": {
            "RootUrl": "string",
            "ApiName": "string",
            "ApiId": "string"
        },
        "handleTelnyxResponse": {
            "RootUrl": "string",
            "ApiName": "string",
            "ApiId": "string"
        },
        "stripeCharge": {
            "RootUrl": "string",
            "ApiName": "string",
            "ApiId": "string"
        },
        "ForceLogoutAndRefresh": {
            "RootUrl": "string",
            "ApiName": "string",
            "ApiId": "string"
        },
        "sendMessages": {
            "RootUrl": "string",
            "ApiName": "string",
            "ApiId": "string"
        },
        "sendPendingCoachingMessage": {
            "RootUrl": "string",
            "ApiName": "string",
            "ApiId": "string"
        },
        "ProcessPerformanceData": {
            "RootUrl": "string",
            "ApiName": "string",
            "ApiId": "string"
        }
    },
    "storage": {
        "hera": {
            "BucketName": "string",
            "Region": "string"
        }
    }
}