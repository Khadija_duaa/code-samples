{
  "AWSTemplateFormatVersion": "2010-09-09",
  "Parameters": {
    "env": {
      "Type": "String"
    },
    "AmplifyAppId": {
      "Type": "String"
    },
    "UserName": {
      "Type": "String"
    },
    "DatabaseName": {
      "Type": "String"
    },
    "DatabaseMinCapacity": {
      "Type": "Number"
    },
    "DatabaseMaxCapacity": {
      "Type": "Number"
    }
  },
  "Resources": {
    "DBClusterParameterGroup": {
      "Type": "AWS::RDS::DBClusterParameterGroup",
      "Properties": {
        "Description": {
          "Fn::Sub": "hera-DBClusterParameterGroup-${AmplifyAppId}-${env}"
        },
        "Family": "aurora-mysql8.0",
        "Parameters": {
          "character_set_server": "utf8",
          "character_set_client": "utf8",
          "character_set_connection": "utf8",
          "character_set_results": "utf8",
          "character_set_database": "utf8",
          "performance_schema": "1"
        }
      }
    },
    "DBParameterGroup": {
      "Type": "AWS::RDS::DBParameterGroup",
      "Properties": {
        "DBParameterGroupName": {
          "Fn::Sub": "hera-DBParameterGroup-${AmplifyAppId}-${env}"
        },
        "Description": "Hera DBParameterGroup",
        "Family": "aurora-mysql8.0",
        "Parameters": {
          "general_log": "0",
          "slow_query_log": "0",
          "long_query_time": "5000",
          "log_output": "FILE"
        }
      }
    },
    "RdsMasterUserSecret": {
      "Type": "AWS::SecretsManager::Secret",
      "Properties": {
        "Name": {
          "Fn::Sub": "hera-secret-serverless-v2-${AmplifyAppId}-${env}"
        },
        "Description": "Secret with dynamically generated password.",
        "GenerateSecretString": {
          "RequireEachIncludedType": true,
          "SecretStringTemplate": {
            "Fn::Sub": "{\"username\": \"${UserName}\"}"
          },
          "GenerateStringKey": "password",
          "PasswordLength": 25,
          "ExcludePunctuation": true
        }
      }
    },
    "RdsReaderUserSecret": {
      "Type": "AWS::SecretsManager::Secret",
      "Properties": {
        "Name": {
          "Fn::Sub": "hera-rds-user-reader-serverless-v2-${AmplifyAppId}-${env}"
        },
        "Description": "Secret with IAM password.",
        "SecretString": "{\"username\":\"HeraRDSReader\",\"password\":\"RDS\"}"
      }
    },
    "RdsWriterUserSecret": {
      "Type": "AWS::SecretsManager::Secret",
      "Properties": {
        "Name": {
          "Fn::Sub": "hera-rds-user-writer-serverless-v2-${AmplifyAppId}-${env}"
        },
        "Description": "Secret with IAM password.",
        "SecretString": "{\"username\":\"HeraRDSWriter\",\"password\":\"RDS\"}"
      }
    },
    "RdsServerlessDB": {
      "Type": "AWS::RDS::DBCluster",
      "Properties": {
        "EnableIAMDatabaseAuthentication": true,
        "DBClusterIdentifier": {
          "Fn::Sub": "hera-${AmplifyAppId}-${env}"
        },
        "Engine": "aurora-mysql",
        "EngineVersion": "8.0.mysql_aurora.3.04.0",
        "MasterUsername": {
          "Fn::Join": [
            "",
            [
              "{{resolve:secretsmanager:",
              {
                "Ref": "RdsMasterUserSecret"
              },
              ":SecretString:username}}"
            ]
          ]
        },
        "MasterUserPassword": {
          "Fn::Join": [
            "",
            [
              "{{resolve:secretsmanager:",
              {
                "Ref": "RdsMasterUserSecret"
              },
              ":SecretString:password}}"
            ]
          ]
        },
        "DBClusterParameterGroupName": {
          "Ref": "DBClusterParameterGroup"
        },
        "PreferredMaintenanceWindow": "Thu:18:35-Thu:19:05",
        "PreferredBackupWindow": "18:05-18:35",
        "BackupRetentionPeriod": 1,
        "DatabaseName": {
          "Fn::Sub": "${DatabaseName}${env}"
        },
        "VpcSecurityGroupIds": [
          {
            "Fn::GetAtt": [
              "RDSSecurityGroup",
              "GroupId"
            ]
          }
        ],
        "ServerlessV2ScalingConfiguration": {
          "MinCapacity": {
            "Fn::Sub": "${DatabaseMinCapacity}"
          },
          "MaxCapacity": {
            "Fn::Sub": "${DatabaseMaxCapacity}"
          }
        }
      }
    },
    "DBInstance1": {
      "Type": "AWS::RDS::DBInstance",
      "Properties": {
        "DBClusterIdentifier": {
          "Ref": "RdsServerlessDB"
        },
        "DBParameterGroupName": {
          "Ref": "DBParameterGroup"
        },
        "DBInstanceClass": "db.serverless",
        "Engine": "aurora-mysql",
        "AvailabilityZone": "us-east-2b",
        "PubliclyAccessible": true
      }
    },
    "SecretRDSInstanceAttachment": {
      "Type": "AWS::SecretsManager::SecretTargetAttachment",
      "Properties": {
        "SecretId": {
          "Ref": "RdsMasterUserSecret"
        },
        "TargetId": {
          "Ref": "RdsServerlessDB"
        },
        "TargetType": "AWS::RDS::DBCluster"
      }
    },
    "RDSSecurityGroup": {
      "Type": "AWS::EC2::SecurityGroup",
      "Properties": {
        "GroupName": {
          "Fn::Sub": "rds-sg-${AmplifyAppId}-${env}"
        },
        "GroupDescription": "RDS Serverless Security Group",
        "SecurityGroupIngress": [
          {
            "IpProtocol": "tcp",
            "FromPort": "3306",
            "ToPort": "3306",
            "CidrIp": "0.0.0.0/0"
          }
        ]
      }
    },
    "RDSSecretReaderRole": {
      "Type": "AWS::IAM::Role",
      "Properties": {
        "RoleName": {
          "Fn::Sub": "rds-secret-reader-${AmplifyAppId}-${env}"
        },
        "AssumeRolePolicyDocument": {
          "Version": "2012-10-17",
          "Statement": [
            {
              "Effect": "Allow",
              "Principal": {
                "Service": "rds.amazonaws.com"
              },
              "Action": "sts:AssumeRole"
            }
          ]
        },
        "Policies": [
          {
            "PolicyName": "SecretsManagerReadPolicy",
            "PolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Effect": "Allow",
                  "Action": "secretsmanager:GetSecretValue",
                  "Resource": "*"
                },
                {
                  "Sid": "DecryptSecretValue",
                  "Action": [
                    "kms:Decrypt"
                  ],
                  "Effect": "Allow",
                  "Resource": [
                    "*"
                  ],
                  "Condition": {
                    "StringEquals": {
                      "kms:ViaService": {
                        "Fn::Sub": "secretsmanager.${AWS::Region}.amazonaws.com"
                      }
                    }
                  }
                },
                {
                  "Effect": "Allow",
                  "Action": [
                    "rds-db:connect"
                  ],
                  "Resource": [
                    "*"
                  ]
                }
              ]
            }
          }
        ]
      }
    },
    "RDSProxy": {
      "Type": "AWS::RDS::DBProxy",
      "DependsOn": [
        "RDSSecretReaderRole",
        "RdsMasterUserSecret"
      ],
      "Properties": {
        "DebugLogging": false,
        "DBProxyName": {
          "Fn::Sub": "rds-proxy-${AmplifyAppId}-${env}"
        },
        "EngineFamily": "MYSQL",
        "IdleClientTimeout": 120,
        "RequireTLS": true,
        "RoleArn": {
          "Fn::GetAtt": [
            "RDSSecretReaderRole",
            "Arn"
          ]
        },
        "Auth": [
          {
            "AuthScheme": "SECRETS",
            "SecretArn": {
              "Ref": "RdsMasterUserSecret"
            },
            "IAMAuth": "REQUIRED"
          },
          {
            "AuthScheme": "SECRETS",
            "SecretArn": {
              "Ref": "RdsWriterUserSecret"
            },
            "IAMAuth": "REQUIRED"
          },
          {
            "AuthScheme": "SECRETS",
            "SecretArn": {
              "Ref": "RdsReaderUserSecret"
            },
            "IAMAuth": "REQUIRED"
          }
        ],
        "VpcSubnetIds": [
          "subnet-2fc60344",
          "subnet-051e2d7f",
          "subnet-841a81c8"
        ]
      }
    },
    "ProxyTargetGroup": {
      "Type": "AWS::RDS::DBProxyTargetGroup",
      "DependsOn": [
        "RdsServerlessDB",
        "DBInstance1"
      ],
      "Properties": {
        "DBProxyName": {
          "Ref": "RDSProxy"
        },
        "DBClusterIdentifiers": [
          {
            "Ref": "RdsServerlessDB"
          }
        ],
        "TargetGroupName": "default",
        "ConnectionPoolConfigurationInfo": {
          "MaxConnectionsPercent": 100,
          "MaxIdleConnectionsPercent": 50,
          "ConnectionBorrowTimeout": 120
        }
      }
    }
  },
  "Outputs": {},
  "Description": "{\"createdOn\":\"Mac\",\"createdBy\":\"Amplify\",\"createdWith\":\"12.7.1\",\"stackType\":\"custom-customCloudformation\",\"metadata\":{}}"
}