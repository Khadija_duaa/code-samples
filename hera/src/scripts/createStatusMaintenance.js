import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'
import { MessageBox } from 'element-ui'
import { accidentsByVehicleAndHistoryType } from '@/views/Vehicles/queries'
import { API, graphqlOperation } from 'aws-amplify'
locale.use(lang)

/**
 * 
 * @param {Object} vehicleSelect properties status and name necessary.  
 * @param {Object} maintenanceFormFields properties maintenanceStatus neccessary.
 * @returns (boolean) Confirm Yes = true, No = false
 */

export async function confirmChangeStatusVehicle(vehicleSelect, maintenanceFormFields) {
    var response  = {
        opc: null,
        valueConfirmChangeStatusVehicle: null
    };
    var vehicleStatus = vehicleSelect.status;
    var vehicleName = vehicleSelect.name;
    var maintenanceStatus = maintenanceFormFields.maintenanceStatus;
    const validated = await validateOption(vehicleStatus, maintenanceFormFields);
    response.opc = validated;

    if (response.opc !== 0) {
        const title = `Would you like to change the status of vehicle "${vehicleName}" to "${(response.opc === 1) ? 'Inactive - Maintenance' : 'Active'}"?`
        const body = (response.opc === 1) ? `Since you've set a Maintenance Record to "${maintenanceStatus}", you may wish to change this Vehicle's status to show that the Vehicle is currently not available for use due to maintenance. The Vehicle's current status is "${vehicleStatus}".` : `Since this Vehicle has no incomplete Maintenance Records, you may wish to change this Vehicle's status to show that it's available for use. The Vehicle's current status is "${vehicleStatus}".`
        response.valueConfirmChangeStatusVehicle = await MessageBox.confirm(body, title, {
            confirmButtonText: 'Yes, Change Status',
            cancelButtonText: 'No, Leave Status',
            dangerouslyUseHTMLString: true,
            showClose: false,
        }).then(async (e) => {
            if (e == 'confirm') {
                return true;
            }
        }).catch((e) => {
            console.log(e);
        });
    }

    return response;
}

async function validateOption(vehicleStatus, maintenanceFormFields) {
    var opc = 0;
    const countItemsRecordDiffComplete = await countRecordDiffComplete(maintenanceFormFields.vehicleId);
    opc = (vehicleStatus !== 'Inactive - Maintenance' && maintenanceFormFields.maintenanceStatus !== 'Complete') ? 1 : opc;
    opc = (vehicleStatus !== 'Active' && maintenanceFormFields.maintenanceStatus === 'Complete' && countItemsRecordDiffComplete === 0) ? 2 : opc;
    return opc;
}

//Waiting for pickup, In Progress
async function countRecordDiffComplete(id) {
    var recordDiffComplete = 0;
    var input = {
        vehicleId: id,
        vehicleHistoryTypeAccidentDate: {
            beginsWith: {
                vehicleHistoryType: "Maintenance"
            }
        },
        filter: {
            or: [{
                    maintenanceStatus: {
                        eq: "In Progress"
                    }
                },
                {
                    maintenanceStatus: {
                        eq: "Waiting for Pickup"
                    }
                }
            ]
        }
    }
    try {
        recordDiffComplete = await API.graphql(graphqlOperation(accidentsByVehicleAndHistoryType, input))
    } catch (e) {
        console.log(e);
    }

    return recordDiffComplete.data.accidentsByVehicleAndHistoryType.items.length;
}