import { API } from 'aws-amplify';

async function createLogGroupError(error){
    const apiName = 'heraPublicApi'
    const path = '/create-log-error'     
    const post = {
      body: { error }
    }
    try {
      const {response} = await API.post(apiName, path, post)
      return response
    } catch (error) {
      console.log(error)
      return error
    }
}

export {createLogGroupError}