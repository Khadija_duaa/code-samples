import { API, graphqlOperation } from 'aws-amplify';
import store from '@/store/index.js'
import generateTemplate from '@/scripts/emailTemplate.js'
import { createMessage, updateStaff, createMessageReader } from '@/graphql/mutations'
import { updateRoute, updateMessage } from '@/graphql/mutations'
import { updateDailyRoster } from '@/graphql/mutations'
import router from '@/router/index';

const getTenantId = () => store.state.userInfo.tenant.id

var sendMessage = async function(parameters){

    if(false && process.env.VUE_APP_USE_SENDMESSAGE_LAMBDA == 0) {
        return sendMessageFromFrontend(parameters)
    } else {
        const BATCH_SIZE = 20
        //spliting parameters.staff array to chunks for load balancing and prohibit db exception DynamoDB:DynamoDbException
        if(Array.isArray(parameters.staff)){
            const associatesChunks = splitArray(parameters.staff, BATCH_SIZE)
            associatesChunks.forEach((associatesChunk) => {
                setTimeout(() => {
                    parameters.staff = associatesChunk
                    return sendMessageFromBackend(parameters)
                }, 100);
            })
        }
        else {
            return await sendMessageFromBackend(parameters);
        }
    }

}
// splits array into chunks with defined batch size
var splitArray = function(array,batchSize){
    return [...Array(Math.ceil(array.length/batchSize))].map((_,i)=>array.slice(i*batchSize,(i+1)*batchSize));
}
// {staff, companyName, route, bodyText, bodyHtml, header=null, subject = null, type = "roster", rosterID, time, attachmentId, contentType, user = null }
var sendMessageFromBackend = async function(parameters) {
    console.log('sendMessageFromBackend')
    try {
        let staffs = Array.isArray(parameters.staff) ? parameters.staff : [parameters.staff]

        let apiName = 'sendMessage';
        let path = '/send';

        let notificationPreferencesUrl = window.location.origin + "/notification-preferences/" + staffs[0].id

        let post = {
            body: {
            staffs,
            companyName: store.state.userInfo.tenant.companyName,
            route: parameters.route,
            bodyText:  parameters.bodyText,
            bodyHtml: parameters.bodyHtml ?? parameters.bodyHTML,
            firstName: parameters.firstName,
            type: parameters.type ?? "roster",
            attachmentId: parameters.attachmentId,
            expirationDate: parameters.expirationDate,
            counselingId: parameters.counselingId,
            contentType: parameters.contentType,
            user: store.state.userInfo,
            group: store.state.userInfo.tenant.group,
            owner: store.state.userInfo.cognitoSub,
            senderName: store.state.userInfo.firstName + ' ' + store.state.userInfo.lastName,
            senderId: store.state.userInfo.id,
            tenantId: getTenantId(),
            isTrialAccount: store.getters.isTrialAccount,
            isTrialExpired: store.getters.isTrialExpired,
            subject: parameters.subject,
            header: parameters.header,
            notificationPreferencesUrl: notificationPreferencesUrl,
            withShortenUrl:  parameters.withShortenUrl ?? false
            }
        }
        
        await API.post(apiName, path, post)
        return {success: true, error: '', warning: ''}
    } catch (e) {
        throw e
    }  
}

var sendMessageFromFrontend = async function({staff, companyName, route, bodyText, bodyHtml, header=null, subject = null, type = "roster", rosterID, time, attachmentId, contentType, user = null } ) {
    console.log('sendMessageFromFrontend')

    var markRead = type == "roster" || type == "broadcast" ? 'true' : 'false' 
    //Check to see if staff has opted out of all communications
    if(!staff.receiveTextMessages && !staff.receiveEmailMessages){
        console.log("skipping " + staff.firstName)
        return {success: false, error: '', warning: "skipping " + staff.firstName }
    }
    //Link Message to staff, route, and tenant
    if (route != null){
        var input = {
            staffId: staff.id,
            messageTenantId: getTenantId(),
            messageRouteId: route.id,
            group: store.state.userInfo.tenant.group,
            senderName: store.state.userInfo.firstName + ' ' + store.state.userInfo.lastName,
            senderId: store.state.userInfo.id,
            isReadS: markRead,
            messageType: type
        }
        if(!route.isReplacement){
            input.messageRouteId = route.id
        }else if(route.isReplacement){
            input.messageReplaceByRouteId = route.id
        }
    }else{
        var input = {
            staffId: staff.id,
            messageTenantId: getTenantId(),
            group: store.state.userInfo.tenant.group,
            senderName: store.state.userInfo.firstName + ' ' + store.state.userInfo.lastName,
            isReadS:  markRead,
            messageType: type,
            senderId: store.state.userInfo.id,
        }
    }
    if(attachmentId){
        input.messageAttachmentId = attachmentId
    } 

    //Setup SMS
    if(staff.receiveTextMessages){
        input.channelType = "SMS"
        input.bodyText = bodyText
        input.destinationNumber = staff.phone
    }

    //Setup EMAIL
    if(staff.receiveEmailMessages){
        input.channelType = "EMAIL"
        input.subject = subject
        input.bodyText = bodyText
        input.bodyHtml = generateTemplate(header, bodyHtml, staff.id, companyName)
        input.destinationEmail = staff.email
    }

    //See if they receive both SMS and EMAIL
    if(staff.receiveTextMessages && staff.receiveEmailMessages){
        input.channelType = "SMS/EMAIL"
    }

    //Create Record
    try{
        // resrict access for expired trialsS
        if(store.getters.isTrialAccount && store.getters.isTrialExpired){
            throw {
                message: 'Please select a purchase option to regain access to Hera.'
            }
        }
        // create messsage record
        var response = await API.graphql(graphqlOperation(createMessage, {input} ));
        var messageId = response.data.createMessage.id
        var staffResponse = await API.graphql(graphqlOperation(updateStaff, {input: {
            id: staff.id,
            smsLastMessage: bodyText,
            smsLastMessageTimestamp: response.data.createMessage.createdAt
        }}))
        // const messageReader = await API.graphql(graphqlOperation(createMessageReader, {input: {
        //     messageID: messageId,
        //     userID: store.state.userInfo.id,
        //     group: store.state.userInfo.tenant.group
        // }}))
        //Update Route Record
        return {success: true, error: '', warning: '', messageId: messageId}
    }catch(e){
        console.log(e)
        return {success: false, error: e, warning: '' }
    }
}

export default sendMessage