export const createPodQuality = /* GraphQL */ `
  mutation CreatePodQuality(
    $input: CreatePodQualityInput!
    $condition: ModelPodQualityConditionInput
  ) {
    createPodQuality(input: $input, condition: $condition) {
      id
      createdAt
      updatedAt
    }
  }
`;
export const updatePodQuality = /* GraphQL */ `
  mutation UpdatePodQuality(
    $input: UpdatePodQualityInput!
    $condition: ModelPodQualityConditionInput
  ) {
    updatePodQuality(input: $input, condition: $condition) {
      id
      createdAt
      updatedAt
    }
  }
`;