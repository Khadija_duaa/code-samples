export const createStaffCxFeedback = /* GraphQL */ `
  mutation CreateStaffCxFeedback(
    $input: CreateStaffCxFeedbackInput!
    $condition: ModelStaffCxFeedbackConditionInput
  ) {
    createStaffCxFeedback(input: $input, condition: $condition) {
      id      
      createdAt
      updatedAt
    }
  }
`;

export const updateStaffCxFeedback = /* GraphQL */ `
  mutation UpdateStaffCxFeedback(
    $input: UpdateStaffCxFeedbackInput!
    $condition: ModelStaffCxFeedbackConditionInput
  ) {
    updateStaffCxFeedback(input: $input, condition: $condition) {
      id
      createdAt
      updatedAt
    }
  }
`;
