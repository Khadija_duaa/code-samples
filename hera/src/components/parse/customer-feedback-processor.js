import { API, graphqlOperation } from 'aws-amplify'
import {createStaffCxFeedback, updateStaffCxFeedback} from './customer-feedback-mutations'
import {updateTextractJob, createCompanyScoreCard, updateCompanyScoreCard, updateStaff } from '@/graphql/mutations.js'
import { getStaffTransporterIds, getTenantCoachingMessageSettings } from '@/api/queries.js'
import {createNotification} from '@/graphql/mutations'
import createPendingMessageRecord from '@/scripts/createPendingMessage'
import createIssuesAndKudos from '@/scripts/createIssuesAndKudos'
import store from '@/store/index'

function cleanTransporterId(transporterId){
    // var clean = transporterId.replace(/o|O/g, '0')
    // clean = clean.replace(/i|l/g, '1')
    // clean = clean.replace(/Q/g, '0')
    var clean = transporterId.replace(/\s+/g, '')
    return clean
}

async function process(job){
    // Throw error to user
    if(job.jobStatus == "ERROR"){
        //Mark textract job as processed
        let input = { 
            id: job.id, 
            isProcessed: true,
            isProcessedS: 'true', // need the string version since booleans can't be sort fields
            group: store.state.userInfo.tenant.group
        }
        await API.graphql(graphqlOperation(updateTextractJob, {input} ));
        
         //Create notification
        input = {
            title: "Unable to process Customer Feedback. Please try again.",
            description: JSON.parse(job.results).filename ,
            isRead: false,
            group: store.state.userInfo.tenant.group,
            clickAction: "dismiss",
            payload: ""
        }
        await API.graphql(graphqlOperation(createNotification, {input} ))
        return
    }

    var jobId = job.id
    var results = JSON.parse(job.results)
    var fields = results.fields
    var tables = results.tables
    var key = job.key
    var week = fields.week ? fields.week : job.week
    week = week.padStart(2, '0')
    var year = job.year
    var yearWeek = year + week
    let companyScoreCardId = store.state.userInfo.tenant.id + yearWeek

    var today = new Date()
    var currentWeek = today.getWeekNumber() - 1
    var currentYear = today.getFullYear()


    // add CX Feedback pdf to company scorecard
    var input = {
        id: companyScoreCardId,
        tenantId: store.state.userInfo.tenant.id,
        yearWeek: yearWeek,
        week: week,
        year: year,
        group: store.state.userInfo.tenant.group,
        customerFeedbackPdf: key
    }
    try{
        //Create new company scorecard record
        await API.graphql(graphqlOperation(createCompanyScoreCard, {input} ));
    }catch(e){
        console.log(e)
        try{
            //If fails, update record, if that fails cry
            await API.graphql(graphqlOperation(updateCompanyScoreCard, {input} ));
        }catch(e){
            console.log(e)
        }
    }


    //Create Staff Lookup Table
    input = {
        group: store.state.userInfo.tenant.group,
        limit: 500
    }
    var staff = await API.graphql(graphqlOperation(getStaffTransporterIds, input));
    var staffRecords = staff.data.staffsByGroup.items
    var staffLookup = {}
    staffRecords.forEach((staff) => {
        if(staff.transporterId){
            let key = cleanTransporterId(staff.transporterId)
            staffLookup[key] = {id: staff.id, latestScorecard: staff.latestScorecard}
        }
    })

    // retrieve tenant coaching information to create messages
    input = {
        id: store.state.userInfo.tenant.id
    }
    var tenant = await API.graphql(graphqlOperation(getTenantCoachingMessageSettings, input));
    tenant = tenant.data.getTenant


    //Create customer feedback records
    var table = tables[0]
    var noMatchCount = 0;
    // table.table_data.forEach(async (row) => {        
    await Promise.all(table.table_data.map(async (row) => {        
        let customerFeedbackID = store.state.userInfo.tenant.id + row.transporter_id + year + week
        // console.log(row)

        //Format transporter id for better lookup results
        if( row.transporter_id ){
            var transporterId = cleanTransporterId(row.transporter_id)
        }else{
            var transporterId = null
        }


        //Get Staff ID info
        if(staffLookup[transporterId]){
            var staffId = staffLookup[transporterId].id
            var staffLatestScorecard = staffLookup[transporterId].latestScorecard
        }
        else{ 
            var staffId = null
        }
        let isMatched = staffId != null
        
        //Track how many have no staff id
        if(!isMatched){
            noMatchCount++
        }

        var input = {
            id: customerFeedbackID,
            group: store.state.userInfo.tenant.group,
            matched: isMatched,
            matchedS: isMatched ? 'true' : 'false',
            week: week,
            year: year,
            messageHasBeenSent: false,
            name: row.driver_name,
            transporterId: row.transporter_id,
            positiveFeedback: row['%_positive_feedback'],
            negativeFeedback: row['$_negative_feedback'],
            deliveryWasGreat: row.delivery_was_great,
            careForOthers: row.care_for_others,
            deliveryWasntGreat: row.delivery_was_not_so_great,
            totalDeliveries: row.total_deliveries_with_customer_feedback,
            respectfulOfProperty: row.respectful_of_property,
            followedInstructions: row.followed_instructions,
            friendly: row.friendly,
            aboveAndBeyond: row.went_above_and_beyond,
            deliveredWithCare: row.delivered_with_care,
            mishandledPackage: row.mishandled_package,
            drivingUnsafely: row.driving_unsafely,
            driverUnprofessional: row.driver_unprofessional,
            notDeliveredToPreferredLocation: row.not_delivered_to_preferred_location
        }
         //Add staffid to input if available
         if( staffId ){
            input.staffCxFeedbackStaffId = staffId
        }
        try{
            //Create new record
            await API.graphql(graphqlOperation(createStaffCxFeedback, {input} ));
        }catch(e){
            try{
                delete input.messageHasBeenSent // dont update if a message has been sent
                //If fails, update record, if that fails cry
                await API.graphql(graphqlOperation(updateStaffCxFeedback, {input} ));
            }catch(e){
                console.log(e)
            }
        }

        // // check if CX feedback is for the latest week
        // if( isMatched && staffLatestScorecard <= yearWeek){
        //     // CX was imported before scorecard or is the same as the current week
        //     // create or update pending coaching message with latest cx feedback
        //     input.isNewerThanLatestScoreCard = staffLatestScorecard < yearWeek
        //     await createPendingMessageRecord(null, input, null, tenant)
        // }

        if( isMatched && (currentWeek == week) && (currentYear == year)){
        // if( isMatched ){
            // create or update pending coaching message with latest cx feedback
            input.isNewerThanLatestScoreCard = staffLatestScorecard < yearWeek
            await createPendingMessageRecord(null, input, null, tenant)
        }

        //create da issues and kudos
        if(isMatched){
            await createIssuesAndKudos(null, input, null, tenant, null)
        }

    }))


    //Mark textract job as processed
    input = { 
        id: jobId, 
        isProcessed: true,
        isProcessedS: `true`, // need the string version since booleans can't be sort fields
        group: store.state.userInfo.tenant.group 
    }
    await API.graphql(graphqlOperation(updateTextractJob, {input} ));


    //Create notification
    if(noMatchCount == 0){
        input = {
            title: "Customer Feedback Week " + week + "-" + year + " parsing completed",
            description: "Click here to dismiss",
            isRead: false,
            clickAction: "dismiss",
            group: store.state.userInfo.tenant.group,
            payload: ""
        }
    }else{
        input = {
            title: "Customer Feedback Week " + week + "-" + year + " parsing completed with " + noMatchCount + " errors",
            description: "Click here to manually fix issues",
            isRead: false,
            clickAction: "navigate",
            group: store.state.userInfo.tenant.group,
            payload: JSON.stringify({name: 'CustomerFeedbackIndex'})
        }
    }
    await API.graphql(graphqlOperation(createNotification, {input} ))
}

Date.prototype.getWeekNumber = function(){
    var d = new Date(Date.UTC(this.getFullYear(), this.getMonth(), this.getDate()));
    var dayNum = d.getUTCDay() || 7;
    d.setUTCDate(d.getUTCDate() + 4 - dayNum);
    var yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
    return Math.ceil((((d - yearStart) / 86400000) + 1)/7)
};

export default process