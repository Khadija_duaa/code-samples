import { API, graphqlOperation } from 'aws-amplify';
import {createPodQuality, updatePodQuality} from './pod-quality-mutations'
import { updateTextractJob,  createCompanyScoreCard, updateCompanyScoreCard} from '@/graphql/mutations.js'
import { getStaffTransporterIds } from '@/api/queries.js'
import {createNotification} from '@/graphql/mutations'
import store from '@/store/index'

function cleanTransporterId(transporterId){
    // var clean = transporterId.replace(/o|O/g, '0')
    // clean = clean.replace(/i|l/g, '1')
    // clean = clean.replace(/Q/g, '0')
    var clean = transporterId.replace(/\s+/g, '')
    return clean
}

async function process(job){
    // Throw error to user
    if(job.jobStatus == "ERROR"){
        //Mark textract job as processed
        let input = { 
            id: job.id, 
            isProcessed: true,
            isProcessedS: 'true', // need the string version since booleans can't be sort fields
            group: store.state.userInfo.tenant.group
        }
        await API.graphql(graphqlOperation(updateTextractJob, {input} ));
        
         //Create notification
        input = {
            title: "Unable to process POD Quality Report. Please try again.",
            description: JSON.parse(job.results).filename ,
            isRead: false,
            group: store.state.userInfo.tenant.group,
            clickAction: "dismiss",
            payload: ""
        }
        await API.graphql(graphqlOperation(createNotification, {input} ))
        return
    }

    var jobId = job.id
    var results = JSON.parse(job.results)
    // console.log(results)
    var fields = results.fields
    var tables = results.tables
    var key = job.key
    var week = fields.week ? fields.week : job.week
    week = week.padStart(2, '0')
    var year = job.year
    var yearWeek = year + week
    let companyScoreCardId = store.state.userInfo.tenant.id + yearWeek

    // add POD pdf to company scorecard
    var input = {
        id: companyScoreCardId,
        tenantId: store.state.userInfo.tenant.id,
        yearWeek: yearWeek,
        week: week,
        year: year,
        group: store.state.userInfo.tenant.group,
        podQualityPdf: key
    }
    try{
        //Create new company scorecard record
        await API.graphql(graphqlOperation(createCompanyScoreCard, {input} ));
    }catch(e){
        console.log(e)
        try{
            //If fails, update record, if that fails cry
            await API.graphql(graphqlOperation(updateCompanyScoreCard, {input} ));
        }catch(e){
            console.log(e)
        }
    }

    //Create Staff Lookup Table
    input = {
        group: store.state.userInfo.tenant.group,
        limit: 500
    }
    var staff = await API.graphql(graphqlOperation(getStaffTransporterIds, input));
    var staffRecords = staff.data.staffsByGroup.items
    var staffLookup = {}
    staffRecords.forEach((staff) => {
        if(staff.transporterId){
            let key = cleanTransporterId(staff.transporterId)
            staffLookup[key] = staff.id
        }
    })
    // console.log(staffLookup)

    //Create PodQuality records
    var table = tables[0]
    var noMatchCount = 0;
    table.table_data.forEach(async (row) => {        
        let podQualityID = store.state.userInfo.tenant.id + row.transporter_id + year + week
        // console.log(row)

        //Format transporter id for better lookup results
        if( row.transporter_id ){
            var transporterId = cleanTransporterId(row.transporter_id)
        }else{
            var transporterId = null
        }


        // let staffId = staffLookup[transporterId]
        //Get Staff ID info
        if(staffLookup[transporterId]){
            var staffId = staffLookup[transporterId]
        }
        else{ 
            var staffId = null
        }
        let isMatched = staffId != null
        
        //Track how many have no staff id
        if(!isMatched){
            noMatchCount++
        }

        var input = {
            id: podQualityID,
            group: store.state.userInfo.tenant.group,
            matched: isMatched,
            matchedS: isMatched ? 'true' : 'false',
            week: week,
            year: year,
            employeeName: row.employee_name,
            transporterId: row.transporter_id,
            opportunities: row.opportunities,
            success: row.success,
            bypass: row.bypass,
            packageInHand: row.package_in_hand,
            notClearlyVisible: row.package_not_clearly_visible,
            blurry: row.blurry_photo,
            explicit: row.explicit,
            mailSlot: row.mail_slot,
            noPackage: row.no_package ? row.no_package : row.no_package_detected,
            other: row.other,
            packageTooClose: row.package_too_close,
            personInPhoto: row.person_in_photo ? row.person_in_photo : row.human_in_the_picture,
            photoTooDark: row.photo_too_dark,
            takenFromCar: row.taken_from_car ? row.taken_from_car : row.package_in_car,
            grandTotal: row.grand_total ? row.grand_total : row.rejects
        }
        //Add staffid to input if available
        if( staffId ){
            input.podQualityStaffId = staffId
        }
        try{
            //Create new record
            await API.graphql(graphqlOperation(createPodQuality, {input} ));
        }catch(e){
            console.log(e)
            try{
                //If fails, update record, if that fails cry
                await API.graphql(graphqlOperation(updatePodQuality, {input} ));
            }catch(e){
                console.log(e)
                console.log("Error: Could not update/create POD record")
            }
        }
    })


    //Mark textract job as processed
    input = { 
        id: jobId, 
        isProcessed: true, 
        isProcessedS: 'true', // need the string version since booleans can't be sort fields
        group: store.state.userInfo.tenant.group
    }
    await API.graphql(graphqlOperation(updateTextractJob, {input} ));


    //Create notification
    if(noMatchCount == 0){
        input = {
            title: "POD Quality Report Week " + week + "-" + year + " parsing completed",
            description: "Click here to dismiss",
            isRead: false,
            clickAction: "dismiss",
            payload: "",
            group: store.state.userInfo.tenant.group
        }
    }else{
        input = {
            title: "POD Quality Report Week " + week + "-" + year + " parsing completed with " + noMatchCount + " errors",
            description: "Click here to manually fix issues",
            isRead: false,
            clickAction: "navigate",
            payload: JSON.stringify({name: 'PodQualityIndex'}),
            group: store.state.userInfo.tenant.group
        }
    }
    await API.graphql(graphqlOperation(createNotification, {input} ))
}


export default process