
export const createStaff = /* GraphQL */ `
  mutation CreateStaff(
    $input: CreateStaffInput!
    $condition: ModelStaffConditionInput
  ) {
    createStaff(input: $input, condition: $condition) {
      id
      firstName
      lastName
      alternateNames
      transporterId
      keyFocusArea
      latestScorecard
      netradyneDriverId
      status
    }
  }
`;