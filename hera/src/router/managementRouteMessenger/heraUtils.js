export const HeraUtils = {
    hasProperty(obj, property) {
        return Object.prototype.hasOwnProperty.call(obj, property)
    }
}
