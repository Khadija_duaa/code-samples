export const vehicleFragment = /* GraphQL */`
  fragment vehicleFragment on Vehicle {
    id
    group
    name
    vehicle
    type
    vehicleType {
      id
      group
      order
      option
      default
      usedFor
      daysCount
      canBeEdited
      canBeDeleted
      canBeReorder
      createdAt
      updatedAt
    }
    labels {
      items {
        id
        labelId
        createdAt
        label {
          id
          name
          status
        }
      }
    }
    state
    licensePlateExp
    dateStart
    dateEnd
    ownership
    provider
    otherProvider
    category
    vin
    make
    model
    year
    rentalAgreementNumber
    images
    rentalContracts
    accidentReports
    licensePlate
    mileage
    gasCard
    status
    company
    notes
    parkingSpace {
      id
      group
      order
      option
      default
      usedFor
      daysCount
      canBeEdited
      canBeDeleted
      canBeReorder
      createdAt
      updatedAt
    }
    lastOdometerReadingDate
    replaceByRoute {
      id
      group
      notes
      createdAt
      routeNumber
      staging
      status
      helperStatus
      isNotActive
      standby
      time
      messageSentTime
      messageSentError
      receivedAnnouncement
      rescued
      updatedAt
    }
    accidents {
      items{
        id
      }
      nextToken
    }
    maintenanceRecords{
      items{
        id
        vehicleHistoryType
        services
        location
        maintenanceStatus
        accidentDate
        accidentType
      }
    }
    vehicleDamage {
      nextToken
    }
    odometerReadings {
      items{
        id
      }
      nextToken
    }
    maintenance {
      nextToken
    }
    reminders {
      nextToken
    }
    vehicleStatusHistory {
      items {
        id
        group
        reason
        date
        previousStatus
        currentStatus
        createdAt
        updatedAt
      }
      nextToken
    }
    overTenThousandPounds
    customDeliveryVan
    tollPassId
    createdAt
    updatedAt
    device {
      id
      group
      deviceName
      phoneNumber
      carrier
      status
      notes
      createdAt
      updatedAt
    }
    # vehicleRecordHistory {
    #   items {
    #     id
    #     changes
    #     createdAt
    #     date
    #     userFirstName
    #     userLastName
    #   }
    #   nextToken
    # }
    device2 {
      id
      group
      deviceName
      phoneNumber
      carrier
      status
      notes
      createdAt
      updatedAt
    }
    dailyLogs {
      nextToken
    }
    documents {
      nextToken
    }
    route {
      nextToken
      scannedCount
    }
    defaultStaff{
      items{
        id
        group
        firstName
        lastName
        authorizedToDrive{
          items{
            id
            optionCustomList{
              id
              option
            }
          }
        }
      }
      nextToken
    }
    defaultStaff2{
      items{
        id
        group
        firstName
        lastName
        authorizedToDrive{
          items{
            id
            optionCustomList{
              id
              option
            }
          }
        }
      }
      nextToken
    }
    defaultStaff3{
      items{
        id
        group
        firstName
        lastName
        authorizedToDrive{
          items{
            id
            optionCustomList{
              id
              option
            }
          }
        }
      }
      nextToken
    }
  }
`;

export const associateFragment = /* GraphQL */ `
  fragment associateFragment on Staff {
    id
    group
    vehicleType
    transporterId
    firstName
    lastName
    keyFocusArea
    dob
    gender
    hireDate
    phone
    status
    createdAt
    updatedAt
    email
    dlExpiration
    vehicleReport
    gasCardPin
    hireDate
    receiveTextMessages
    receiveEmailMessages
    authorizedToDrive {
      items {
        id
        group
        staff {
          id
        }
        optionCustomList {
          id
          option
        }
      }
      nextToken
    }
    defaultVehicle{
      id
      status
      name
    }
    defaultVehicle2 {
      id
      status
      name
    }
    defaultVehicle3 {
      id
      status
      name
    }
    onBoarding {
      items {
        id
        group
        name
        isComplete
        status
        dateComplete
        dateStart
        createdAt
        updatedAt
      }
      nextToken
    }
    scoreCards {
      items {
        id
        group
        overallTier
        year
        week
      }
      nextToken
    }
    route {
      items {
        id
        group
        rescued
      }
    }
    routeRescuer {
      items {
        id
      }
    }
    labels {
      items {
        id
        createdAt
        group
        labelId
        label {
          id
          name
          status
        }
      }
      nextToken
    }
    pronouns
    isALead
    assignedLead
    hourlyStatus
    hireDate
    finalCheckIssueDate
    photo
    smsLastMessageTimestamp
    smsLastMessage
    preferredDaysOff
  }
`;

export const deviceFragment = /* GraphQL */ `
  fragment deviceFragment on Device {
    id
    group
    deviceName
    phoneNumber
    carrier
    imei
    status
    notes
    replaceByRoute{
      nextToken
    }
    route {
        items{
            id
        }
      nextToken
    }
    vehicle {
        items{
            id
        }
      nextToken
    }
  }
`;

export const userFragment = /* GraphQL */ `
  fragment userFragment on User {
    id
    group
    cognitoSub
    firstName
    lastName
    phone
    email
    emailVerified
    role
    isOwner
    isCreator
    receiveSmsTaskReminders
    receiveEmailTaskReminders
    receiveSmsTaskAssignments
    receiveEmailTaskAssignments
    permissionLogin
    permissionFullAccess
    permissionDocuments
    permissionCounselings
    permissionAccidents
    permissionInjuries
    permissionDrugTests
    permissionDailyRostering
    permissionMessenger
    permissionPerformanceCoaching
    permissionDAManagement
    permissionCustomLists
    permissionManageLabels
    permissionVehicleManagement
    permissionTasksReports
    permissionMessageTemplate
    permissionVehiclePhotoLogs
    lastLogin
    tenant{
      id
    }
    counseling{
      id
    }
    injuriesCompletedBy{
      nextToken
    }
    assignerTasks{
      nextToken
    }
    accidentsVerified{
      nextToken
    }
    assigneeTasks{
      nextToken
    }
    completedReminders{
      nextToken
    }
    messagesRead{
      nextToken
    }
    messagesSent{
      nextToken
    }
    recurringMessagesSent{
      nextToken
    }
    messageReadStatus{
      nextToken
    }
  }
`;

export const optionsCustomListsStaffFragment = /* GraphQL */ `
  fragment optionsCustomListsStaffFragment on OptionsCustomListsStaff{
    id
    group
    optionCustomList {
      id
      group
      customLists {
        id
        group
        type
        listCategory
        listName
        listDisplay
        createdAt
        updatedAt
      }
      order
      option
    }
    createdAt
    updatedAt
    staff {
      id
    }
  }
`;

export const infractionFragment = /* GraphQL */ `
  fragment infractionFragment on Infraction {
    id
    group
    staffId
    infractionType
    date
    type {
      id
      option
    }
  }
`;

export const kudoFragment = /* GraphQL */ `
  fragment kudoFragment on Kudo {
    id
    group
    staffId
    kudoType
    date
    type {
      id
      option
    }
  }
`;

export const tenantFragment = /* GraphQL */ `
  fragment tenantFragment on Tenant {
    id
    group
    coachingDriverRankRange
    coachingFicoIssue
    coachingFicoKudo
    coachingDcrIssue
    coachingDcrKudo
    coachingDarIssue
    coachingDarKudo
    coachingDnrIssue
    coachingDnrKudo
    coachingDsbIssue
    coachingDsbKudo
    coachingSwcPodIssue
    coachingSwcPodKudo
    coachingSwcCcIssue
    coachingSwcCcKudo
    coachingSwcScIssue
    coachingSwcScKudo
    coachingSwcAdIssue
    coachingSwcAdKudo
    coachingSeatbeltOffIssue
    coachingSeatbeltOffKudo
    coachingSpeedingEventIssue
    coachingSpeedingEventKudo
    coachingDistractionsRateIssue
    coachingDistractionsRateKudo
    coachingFollowingDistanceRateIssue
    coachingFollowingDistanceRateKudo
    coachingSignSignalViolationsRateIssue
    coachingSignSignalViolationsRateKudo
    coachingConsecutiveTierIssue
    coachingConsecutiveTierKudo
    coachingOverallTierIssue
    coachingOverallTierKudo
    coachingPositiveFeedbackIssue
    coachingPositiveFeedbackKudo
    coachingCdfScoreIssue
    coachingCdfScoreKudo
    coachingDaTierIssue
    coachingDaTierKudo
    coachingDailyFicoIssue
    coachingDailyFicoKudo
    coachingSeatbeltIssue
    coachingSeatbeltKudo
    coachingSseIssue
    coachingSseKudo
    coachingDvcrsIssue
    coachingTraningRemainingIssue
    coachingCameraObstructionIssue
    coachingDriverDistractionIssue
    coachingDriverDrowsinessIssue
    coachingDriverInitiatedIssue
    coachingDriverStarKudo
    coachingFaceMaskComplianceIssue
    coachingFollowingDistanceIssue
    coachingHardAccelerationIssue
    coachingHardBrakingIssue
    coachingHardTurnIssue
    coachingHighGIssue
    coachingLowImpactIssue
    coachingSeatbeltComplianceIssue
    coachingSignViolationsIssue
    coachingSpeedingViolationsIssue
    coachingTrafficLightViolationIssue
    coachingUTurnIssue
    coachingWeavingIssue
    coachingDailyComplianceRateIssue
    coachingDailyComplianceRateKudo
  }
`;


export const replaceByRouteFragment = /* GraphQL */ `
  fragment replaceByRouteFragment on ReplaceByRoute{
    id
    group
    notes
    routeNumber
    status
    helperStatus
    parkingSpace {
        id
        order
        option
    }
    staging
    standby
    time
    isNotActive
    messageSentTime
    messageSentError
    receivedAnnouncement
    rescued
    createdAt
    updatedAt
    route{
      id
    }
    dailyRoster{
      id
    }
    staff {
      id
    }
    document{
      items{
        id
        name
        key
        type
        route{
            id
        }
      }
    }
    odometerReadings {
      items {
        id
      }
      nextToken
    }
    vehicle{
        id
        name
    }
    device{
        id
        deviceName
    }
    messages {
      items{
        id
        bodyText
        createdAt
        emailStatus
        smsStatus
        smsSendInformation
        emailSendInformation
        messageType
        staff {
            id
            firstName
        }
      }
    }
  }
`;

export const routeFragment = replaceByRouteFragment + /* GraphQL */ `
fragment routeFragment on Route{
  id
  routeDailyRosterId
  group
  notes
  routeNumber
  previousStatus
  status
  helperStatus
  parkingSpace {
      id
      order
      option
  }
  staging
  standby
  time
  isNotActive
  messageSentTime
  messageSentError
  receivedAnnouncement
  createdAt
  updatedAt
  replaceByRoute (sortDirection: ASC){
    items{
      ...replaceByRouteFragment
    }
  }
  odometerReadings {
      items {
      id
      group
      }
      nextToken
  }
  vehicleDamage {
      items {
        id
      }
      nextToken
  }
  staff {
    id
  }
  helper {
    id
  }
  rescuer{
    id
  }
  rescuers {
    items {
      id
      staff{
          id
      }
    }
  }
  messages {
      items{
      id
      bodyText
      createdAt
      emailStatus
      smsStatus
      smsSendInformation
      emailSendInformation
      messageType
      staff {
          id
          firstName
      }
      }
  }
  vehicle {
      id
  }
  document{
      items{
      id
      name
      key
      type
      route{
          id
      }
      }
  }
  device {
      id
  }
  kudos{
      items{
        id
        staffId
      }
  }
}
`;

export const dailyRosterFragment = routeFragment + /* GraphQL */ `
fragment dailyRosterFragment on DailyRoster{
    id
    group
    notesDate
    amNotes
    pmNotes
    standUpNotes
    lastStandUpSentTime
    createdAt
    updatedAt
    attachment {
      id
      group
      s3Key
      expirationDate
      contentType
      createdAt
      updatedAt
    }
    route {
      items {
        ...routeFragment
      }
      nextToken
    }
}
`;

export const dailyLogFragment = /* GraphQL */ `
fragment dailyLogFragment on DailyLog {
  id
  group
  type
  date
  notes
  vehicleId
  creationLinkSentDateTime
  dailyLogCreationLinkSentByUserId
  dailyLogCreationLinkAssociateId
  dailyLogTakenByUserId
  dailyLogTakenByAssociateId
  dailyLogRosteredDayId
  dailyLogVehicleId
  dailyLogCreatedByUserId
  history {
    items {
      id
      date
      group
      previousValue
      currentValue
      dailyLogId
      createdAt
      updatedAt
    }
    nextToken
  }
  shortenUrls {
    items {
      id
      group
      type
      shortenId
      expirationTTL
      urlToken
      attachmentId
      isOpenLink
      createdAt
      updatedAt
    }
    nextToken
  }
  documents {
    items {
      id
      group
      name
      key
      type
      uploadDate
      notes
      documentDate
      isDocVisibleToAssociate
      createdAt
      updatedAt
    }
    nextToken
  }
}`;

export const accidentFragment = /* GraphQL */ `
fragment accidentFragment on Accident {
  id
  group
  createdAt
  updatedAt
  accidentOdometerReadingRouteId
  accidentReplaceOdometerReadingRouteId
  replaceOdometerReadingRoute{
    id
  }
  odometerReadingRoute{
    id
  }
  odometerReadingVehicle{
    id
  }
}`;

export const messageFragment = /* GraphQL */ `
fragment messageFragment on Message {
  id
  group
  createdAt 
  attachment{
    id
    s3Key
    contentType
  }
  sender {
    id
  }
  isReadBy {
    items {
      userID
    }
  }
  channelType
  staffId
  senderName
  bodyText
  isReadS
  messageType
  smsStatus
  smsSendInformation
  staff{
    id
    status
  }
}`;

export const messageReadStatusFragment = /* GraphQL */ `
  fragment messageReadStatusFragment on MessageReadStatus {
    id
    group
    userID
    staffID
    readUpToDateTime
    createdAt
    updatedAt
  }
`;