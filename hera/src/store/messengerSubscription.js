import { API, graphqlOperation } from 'aws-amplify';
import {onCreateMessageByGroup, onUpdateMessageByGroup} from '@/api/subscriptions';
import store from '@/store'
import EventBus from '@/eventBus'
import { loadStaff, matchReadChatStaff, totalUnreadMessages } from '@/views/Messenger/countUnreadMessages';

var subscriptionCreate = null
var subscriptionUpdate = null
window.messengerSubscriptionDisconnect = false

var messengerLoadNotifications = async function(){
    const listStaff = await loadStaff();
    const staffsMatched = await matchReadChatStaff( listStaff );
    const notificationsUnreadMessages = totalUnreadMessages( staffsMatched );
    store.commit('setMessengerNotifications', notificationsUnreadMessages)
}

var messengerSubscribe = async function(){
    try {
        console.log("listening for new messages")
        messengerSubscriptionDisconnect = false

        //First Load all unread notifications
        await messengerLoadNotifications()
    
    
        //Now subscribe to any new additions
        var input = {
            group: store.state.userInfo.tenant.group
        }
        subscriptionCreate = API.graphql(
            graphqlOperation(onCreateMessageByGroup, input)
        ).subscribe({
            next: async(newMessage) => {
                let payload = newMessage.value.data.onCreateMessageByGroup;
                // only let in if group matches logged in user
                if( payload != null && (payload.group == store.state.userInfo.tenant.group && payload.staff.status.toLowerCase() == "active") ){
                    EventBus.$emit('messenger-new-message-conversation', payload );
                    //Only create notification if type is response
                    if( payload.channelType === "RESPONSE" && payload.senderName === null ){
                        store.dispatch( "marNewkMessageRecieved" );
                    }
                }
            },
            error: async(messengerSubscribeError)=>{
                messengerSubscriptionDisconnect = true
                console.log({messengerSubscribeError})
            }
        });
        
        subscriptionUpdate = API.graphql(
            graphqlOperation(onUpdateMessageByGroup, input)
        ).subscribe({
            next: async(updateMessage) => {
                // Handle onUpdateMessage subscription logic
                let updateMessPayload = updateMessage.value.data.onUpdateMessageByGroup;
                // only let in if group matches logged in user
                if( updateMessPayload != null ){
                  EventBus.$emit('messenger-update-message-conversation', updateMessPayload );
                }
            },
            error: async(messengerSubscribeError)=>{
                console.log({messengerSubscribeError})
            }
        });

    } catch (messengerSubscribeError) {
        messengerSubscriptionDisconnect = true
        if(messengerSubscribeError.message === 'No current user'){
            messengerUnsubscribe()
        }else{
            console.log({messengerSubscribeError})
        }
    }
}



var messengerUnsubscribe = async function(){
    subscriptionCreate?.unsubscribe();
    subscriptionUpdate?.unsubscribe();
}


export {messengerSubscribe, messengerUnsubscribe, messengerLoadNotifications}