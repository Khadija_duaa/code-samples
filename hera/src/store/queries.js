import {
  vehicleFragment,
  associateFragment,
  deviceFragment,
  userFragment,
  dailyRosterFragment,
  dailyLogFragment
} from './fragments'

export const vehiclesByGroup = vehicleFragment + /* GraphQL */ `
  query VehiclesByGroup(
    $group: String
    $name: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelVehicleFilterInput
    $limit: Int
    $nextToken: String
  ) {
    vehiclesByGroup(
      group: $group
      name: $name
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        ...vehicleFragment
      }
      nextToken
    }
  }
`;

export const staffsByGroup = associateFragment + /* GraphQL */ `
query StaffsByGroup(
  $group: String
  $firstName: ModelStringKeyConditionInput
  $sortDirection: ModelSortDirection
  $filter: ModelStaffFilterInput
  $limit: Int
  $nextToken: String
) {
  staffsByGroup(
    group: $group
    firstName: $firstName
    sortDirection: $sortDirection
    filter: $filter
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      ...associateFragment
    }
    nextToken
  }
}
`;

export const devicesByGroup = deviceFragment + /* GraphQL */ `
  query DevicesByGroup(
    $group: String
    $deviceName: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelDeviceFilterInput
    $limit: Int
    $nextToken: String
  ) {
    devicesByGroup(
      group: $group
      deviceName: $deviceName
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        ...deviceFragment
      }
      nextToken
    }
  }
`;

export const usersByTenant = userFragment + /* GraphQL */ `
  query UsersByTenant(
    $userTenantId: ID,
    $sortDirection: ModelSortDirection,
    $filter: ModelUserFilterInput,
    $limit: Int,
    $nextToken: String
    ) {
    usersByTenant(
      userTenantId: $userTenantId
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ){
      items{
        ...userFragment
      }
      nextToken
    }
  }
`;

export const getDailyRoster = dailyRosterFragment + /* GraphQL */ `
  query GetDailyRoster($id: ID!) {
    getDailyRoster(id: $id) {
      ...dailyRosterFragment
    }
  }
`;
export const dailyLogsByDate = dailyLogFragment + /* GraphQL */ `
  query DailyLogsByDate(
    $group: String
    $date: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelDailyLogFilterInput
    $limit: Int
    $nextToken: String
  ) {
    dailyLogsByDate(
      group: $group
      date: $date
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        ...dailyLogFragment
      }
      nextToken
    }
  }
`;