import { API, graphqlOperation } from 'aws-amplify';
import { listTextractJobs, textractJobsByOwnerAndIsProcessedS } from '@/api/queries.js'
import scorecardProcess from '@/components/parse/scorecard-processor.js'
import podProcess from '@/components/parse/pod-quality-processor.js'
import customerFeebackProcess from '@/components/parse/customer-feedback-processor.js'
import store from '@/store/index'

var checkJobsStatus = async function(){
    const WEEKLY_IMPORT = process.env.VUE_APP_WEEKLY_IMPORT === "1"
    
    var nextToken = null
    var jobs = []
    if(!store.state.userInfo) return []
    
    do{
        var input = {
            // filter: {
            //     isProcessed: {
            //         eq: false 
            //     }
            // },
            isProcessedS: {
                eq: "false"
            },
            group: store.state.userInfo.tenant.group,
            nextToken: nextToken,
            owner: store.state.userInfo.cognitoSub
        }
        var response = await API.graphql(graphqlOperation(textractJobsByOwnerAndIsProcessedS, input ));
        jobs = [ ...jobs, ...response.data.textractJobsByOwnerAndIsProcessedS.items]
        nextToken = response.data.textractJobsByOwnerAndIsProcessedS.nextToken
    }while(nextToken)

    // get arr of unique jobs, ran into issues with duplicate jobs loading
    var uniqueJobs = []
    const map = new Map()
    jobs.forEach(job =>{
        if(!map.has(job.id)){
            map.set(job.id, true)
            uniqueJobs.push(job)
        }
    })
    console.log(uniqueJobs)

    uniqueJobs.forEach(job => {
        if( job.type == 'scorecard' && ( job.jobStatus == "SUCCEEDED" || job.jobStatus == "ERROR")){
            if(!WEEKLY_IMPORT) scorecardProcess(job)
        }
        else if ( job.type == 'podQualityFeedback' && ( job.jobStatus == "SUCCEEDED" || job.jobStatus == "ERROR")){
            if(!WEEKLY_IMPORT) podProcess(job)
        }
        else if ( job.type == 'customerFeedback' && ( job.jobStatus == "SUCCEEDED" || job.jobStatus == "ERROR")){
            if(!WEEKLY_IMPORT) customerFeebackProcess(job)
        }
    });

    return uniqueJobs
}


export default checkJobsStatus