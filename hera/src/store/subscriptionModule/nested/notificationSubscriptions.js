import { mixinMethods } from '@/main'
import store from '@/store'
import EventBus from '@/eventBus'
import {notificationsByOwner} from '@/graphql/queries'
import {onCreateNotification} from '@/graphql/subscriptions';
import {updateNotification} from '@/store/mutations'
import { initializeSubscription, disconnectSubscription } from '../helper'

"use strict";
let subscriptionNotification = null

const loadNotifications = async function(){
    const input = { 
        filter: {
            isRead: { eq: false  } 
        },
        sortDirection: 'DESC',
        owner: store.state.userInfo.cognitoSub
    }
    const notifications = await mixinMethods.gLoadListAll(notificationsByOwner, input, "notificationsByOwner")
    store.commit('setNotifications', notifications)
}

export const notificationSubscribe = function(instance){
    const queries = {
        onCreateNotification,
    }
    const input = {
        owner: store.state.userInfo.cognitoSub
    }
    initializeSubscription(subscriptionNotification, queries, input, "subscriptionStore/handleNotification", instance)
}

export const config = function(subscriptions){
    const subscription = {}
    subscriptionNotification = subscriptions['notification'] = {
        loadList: loadNotifications,
        subscribe: notificationSubscribe,
        unsubscribe: (instance) => disconnectSubscription(subscription, instance),
        callback: notificationEventCallback,
        subscription
    }
}

const loadNewVersion = async function(payload){
    //Check to see if we already have the latest version
    if(notification.description === store.state.version ){
        console.log("Already updated")
        return
    }

    //See if we need to logout
    if(payload.clickAction == 'LOGOUT'){
        try{
            console.log("LOGOUT")
            store.commit('setSessionInfo', null)
            store.commit('setUserInfo', null)
            document.location.href="/";
            location.reload();
        }catch(e){
            console.log('Logout Error', { e, payload });
        }
    }else{
        //Reload page
        location.reload();
    }
}

const importJobComplete = function (payload, type, importName = 'WeeklyDataImport'){
    const events = {
        'JOB COMPLETE':{
            'CustomCSVImport': 'mark-custom-csv-job-complete',
            'DailyDataImport': 'mark-daily-job-complete',
            'WeeklyDataImport': 'mark-job-complete'
        },
        'IMPORT RESULTS':{
            'CustomCSVImport': 'load-custom-csv-import-results',
            'DailyDataImport': 'load-daily-import-results',
            'WeeklyDataImport': 'load-import-results'
        }
    }
    EventBus.$emit(events[type][importName], payload)
}

export const notificationEventCallback = async function(subscriptionState, { data: notification, eventName }){
    const notificationPayload = JSON.parse(notification.payload)
    switch(eventName){
        case 'onCreate':{
            const pathname = window.location.pathname;
            if ( pathname === "/performance-and-coaching/weekly-performance-data-import" ) {
                try {
                    notification.isRead = true
                    let input = {
                        id: notification.id,
                        isRead: true
                    }
                    await mixinMethods.api(updateNotification, {input})
                } catch (e) {
                    console.log('Update Notification Error', { e, payload })
                }
            }

            if(notification.title == 'NEW VERSION' || notification.title == 'DEACTIVATE USER' || notification.title == 'Outstanding Invoices Paid'){
                await loadNewVersion(notification)
                return
            }
            else if(notification.title == 'JOB COMPLETE'){
                notificationPayload.notificationId = notification.id
                importJobComplete(notificationPayload, notification.title, notificationPayload.name)
                return
            }
            else if(notification.title.toLowerCase().includes('import complete')){
                const params = notificationPayload.params
                importJobComplete(params, 'IMPORT RESULTS', notificationPayload.name)
            }
            else if(notification.title.includes('Error importing your weekly performance data') || notification.title.includes('Warning')){
                EventBus.$emit('weekly-performance-fails', notification)
            }
            else if(notification.title.includes('does not have a phone number')){
                EventBus.$emit('missing-phone-number', notification)
            }
            store.commit('addNotification', notification)
            break
        }
    }
}