import {
    onUpdateTenantById,
} from '../../subscriptions';
import { recalculateDaPerformanceData } from '@/views/DaPerformanceReport/calculateDaPerformanceReport'
import store from '@/store'
import { initializeSubscription, disconnectSubscription } from '../helper'

"use strict";
let subscriptionTenant = null

export const tenantSubscribe = function(instance){
    const queries = {
        onUpdateTenantById,
    }
    const input = {
        id: store.state.userInfo.tenant.id
    }
    initializeSubscription(subscriptionTenant, queries, input, "subscriptionStore/handleTenant", instance)
}

export const config = function(subscriptions){
    const subscription = {}
    subscriptionTenant = subscriptions['tenant'] = {
        loadList: null,
        subscribe: tenantSubscribe,
        unsubscribe: (instance) => disconnectSubscription(subscription, instance),
        callback: tenantEventCallback,
        subscription
    }
}

export const tenantEventCallback = async function(subscriptionState, { data: tenant }){
    store.commit('setTenantCoachingInfo', tenant)
    await recalculateDaPerformanceData(true)
}