import {
    subscriptions,
    configureActionsForLists,
    configureActionsForNestedRecords,
    setList,
    mutateElement,
    handleElement,
    loadList,
    configureSubscriptions,
} from './helper'

import {
    lookupMapById,
    lookupMapByKey
} from '@/utilities/helpers'

configureSubscriptions([
    
    require('./lists/vehicleSubscriptions').config,
    require('./lists/associateSubscriptions').config,
    require('./lists/deviceSubscriptions').config,
    require('./lists/userSubscriptions').config,

    require('./nested/routeSubscriptions').config,
    require('./nested/replaceByRouteSubscriptions').config,
    require('./nested/dailyRosterSubscriptions').config,
    require('./nested/optionsCustomListsStaffSubscriptions').config,
    require('./nested/tenantSubscriptions').config,
    require('./nested/infractionSubscriptions').config,
    require('./nested/kudoSubscriptions').config,
    require('./nested/messengerSubscriptions').config,
    require('./nested/notificationSubscriptions').config,
    require('./nested/dailyLogSubscriptions').config,
    require('./nested/accidentSubscriptions').config,
    require('./nested/messageReadStatusSubscriptions').config

])

export const subscriptionSetup = {
    subscribe: ()=>{
        const entries = Object.entries(subscriptions)
        entries.forEach(async([key, pubs])=>{
            loadList(pubs)
            const instances = Object.entries(pubs.subscription)
            if(!instances.length || instances.some(([i, event]) => !event.socket)){
                pubs.subscribe()
            }
        })
        console.log("%clistening for mutation events.","background-color:rgba(200,250,0,.5)")
    },
    unsubscribe: ()=>{
        const entries = Object.entries(subscriptions)
        entries.forEach(([key,
            value])=>{
            if(instances.some(([i,
                event]) => event.socket)){
                value.unsubscribe()
            }
        })
    }
}

export const subscriptionStore = {//Store Module
    namespaced: true,
    state:()=>({
        vehicleList: [],
        associateList: [],
        messengerList: [],
        messageTemplates: [],
        deviceList: [],
        userList: [],
        infractionList: [],
        kudoList: [],
        dailyRosterCache: [],
        dailyLogCache: {},
        temporalRouteByDailyRosterId: {},
        temporalReplaceByRouteId: {}
    }),
    getters:{

        getVehicles: state => state.vehicleList,
        getVehiclesLicensePlate90days: (state) => {
            const currentDate = new Date();
            // Calculate the date 90 days more
            const ninetyDaysMore = new Date(currentDate);
            ninetyDaysMore.setDate(currentDate.getDate() + 90);

            return state.vehicleList.filter(obj => {
                const expDate = new Date(obj.licensePlateExp);
                // Check if the expDate is within the last 90 days
                return expDate >= currentDate  && expDate <= ninetyDaysMore;
            });
        },
        getVehiclesWeekOldOdometerReadings: (state) => {
            const currentDate = new Date();
            // Calculate the date 7 days ago
            const weekAgo = new Date(currentDate);
            weekAgo.setDate(currentDate.getDate() - 7);

            return state.vehicleList.filter(obj => {
                const expDate = new Date(obj.lastOdometerReadingDate);
                return expDate >= weekAgo  && expDate <= currentDate;
            });
        },
        getVehiclesTwoWeekOldOdometerReadings: (state) => {
            const currentDate = new Date();
            // Calculate the date 7 days ago
            const weekAgo = new Date(currentDate);
            weekAgo.setDate(currentDate.getDate() - 14);

            return state.vehicleList.filter(obj => {
                const expDate = new Date(obj.lastOdometerReadingDate);
                return expDate >= weekAgo  && expDate <= currentDate;
            });
        },
        getVehiclesHighMileageOdometerReadings: (state) => {
            return state.vehicleList.filter( vehicle => {
                return parseInt(vehicle.mileage) >= 50000
            })
        },
        getAssociates: state => state.associateList,
        getDevices: state => state.deviceList,
        getUsers: state => state.userList,
        getDailyRoster: state => state.dailyRosterCache,
        
        getVehicleLookupMap: state => lookupMapById(state.vehicleList),
        getAssociateLookupMap: state => lookupMapById(state.associateList),
        getDeviceLookupMap: state => lookupMapById(state.deviceList),
        getUserLookupMap: state => lookupMapById(state.userList),
        getDailyRosterLookupMap: state => lookupMapById(state.dailyRosterCache),
        getMessengerLookupMap: state => lookupMapByKey(state.messengerList, 'staffId'),

        getActiveVehicles: state => state.vehicleList.filter(vehicle => vehicle.status?.toLowerCase() === 'active'),
        getActiveAssociates: state => state.associateList.filter(associate => associate.status?.toLowerCase() === 'active'),
        getActiveDevices: state => state.deviceList.filter(device => device.status?.toLowerCase() === 'active'),

        getInactiveAssociates: state => state.associateList.filter(vehicle => vehicle.status && vehicle.status.toLowerCase() !== 'active'),
        getInactiveVehicles: state => state.vehicleList.filter(associate => associate.status && associate.status.toLowerCase() !== 'active'),
        getInactiveDevices: state => state.deviceList.filter(device => device.status && device.status.toLowerCase() !== 'active'),

        getInfractionList: state => state.infractionList,
        getKudoList: state => state.kudoList,

        getMessageTemplates(state){
            if(state.messageTemplates.lastRenderKey && (new Date) - state.messageTemplates.lastRenderKey < 15000 ){
                return state.messageTemplates
            }
            return null
        }
    },
    mutations: {
        setList,
        mutateElement,
        handleElement,
        clearTemporalRouteByDailyRosterId: (state, dailyRosterId) => {
            state.temporalRouteByDailyRosterId[dailyRosterId] = []
        },
        clearTemporalReplaceByRouteId: (state, routeId) => {
            state.temporalReplaceByRouteId[routeId] = []
        }
    },
    actions: {
        ...configureActionsForLists([
            { setList: 'setVehicleList', mutateElement: 'mutateVehicle', listName: 'vehicleList', entity: "vehicle" },
            { setList: 'setAssociateList', mutateElement: 'mutateAssociate', listName: 'associateList', entity: "associate" },
            { setList: 'setDeviceList', mutateElement: 'mutateDevice', listName: 'deviceList', entity: "device" },
            { setList: 'setUserList', mutateElement: 'mutateUser', listName: 'userList', entity: "user" },
        ]),
        ...configureActionsForNestedRecords([
            { handleElement: 'handleMessenger', entity: "messenger" },
            { handleElement: 'handleNotification', entity: "notification" },
            { handleElement: 'handleOptionsCustomListsStaff', entity: "optionsCustomListsStaff" },
            { handleElement: 'handleTenant', entity: "tenant" },
            { handleElement: 'handleInfraction', entity: "infraction" },
            { handleElement: 'handleKudo', entity: "kudo" },
            { handleElement: 'handleRoute', entity: "route" },
            { handleElement: 'handleReplaceByRoute', entity: "replaceByRoute" },
            { handleElement: 'handleDailyRoster', entity: "dailyRoster" },
            { handleElement: 'handleDailyLog', entity: "dailyLog" },
            { handleElement: 'handleAccident', entity: "accident" },
            { handleElement: 'handleMessageReadStatus', entity: "messageReadStatus" },
        ]),
        setMessengerList(context, list){
            context.commit('setList', { list, listName: "messengerList" })
        },
        setMessageTemplates(context, list){
            context.commit('setList', { list, listName: "messageTemplates" })
        }
    }
}