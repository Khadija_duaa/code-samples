import { mixinMethods } from '@/main'
import {
    onCreateStaffByGroup,
    onUpdateStaffByGroup,
    onDeleteStaffByGroup
} from '../../subscriptions';
import store from '@/store'
import { staffsByGroup } from '../../queries'
import { initializeSubscription, disconnectSubscription } from '../helper'
import { recalculateDaPerformanceData } from '@/views/DaPerformanceReport/calculateDaPerformanceReport'
import { messengerLoadNotifications } from '../nested/messengerSubscriptions'
import { updateAssociateMessengerPhotoUrl } from '@/views/Messenger/countUnreadMessages';

"use strict";
let subscriptionAssociates = null

export const loadAssociates = async function(){
    const input = {
        group: store.state.userInfo.tenant.group,
        limit:100
    }
    const associates = await mixinMethods.gLoadListAll(staffsByGroup, input, 'staffsByGroup')
    store.dispatch("subscriptionStore/setAssociateList", associates)
    messengerLoadNotifications()
}

export const associateSubscribe = function(instance){
    const queries = {
        onCreateStaffByGroup,
        onUpdateStaffByGroup,
        onDeleteStaffByGroup
    }
    const input = {
        group: store.state.userInfo.tenant.group
    }
    initializeSubscription(subscriptionAssociates, queries, input, "subscriptionStore/mutateAssociate", instance)
}

export const config = function(subscriptions){
    const subscription = {}
    subscriptionAssociates = subscriptions['associate'] = {
        loadList: loadAssociates,
        subscribe: associateSubscribe,
        unsubscribe: (instance) => disconnectSubscription(subscription, instance),
        callback: associateEventCallback,
        subscription
    }
}

export const associateEventCallback = async function(subscriptionState, {data: associate, eventName}){
    switch(eventName){
        case 'onUpdate':{
            updateAssociateMessengerPhotoUrl(associate)
            break
        }
    }
    await recalculateDaPerformanceData(false)
}