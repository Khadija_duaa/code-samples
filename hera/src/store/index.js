import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate"
// import checkJobsStatus from './checkJobsStatus'
import { API, graphqlOperation } from 'aws-amplify';
import {updateMessage} from '@/api/mutations'
import { nanoid } from 'nanoid'
import { labelTypeByNameAndGroup } from "@/mixins/labelsQueries";
import { mixinMethods } from "@/main";
import { getAccident, listSystems } from '@/graphql/queries';
import { updateSystem } from "@/graphql/mutations";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    version: '3.1.11.1',
    sessionInfo: null,
    userInfo: null,
    companyLogo: null,
    signUpCredentials: null,
    valuelists: {},
    vehicleTab: "Dashboard",
    daTab: "Dashboard",
    performanceCoachingTab: "Performance Tracking",
    vehicleCount: null,
    states: ["Alabama", "Alaska", "American Samoa", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "District Of Columbia", "Federated States Of Micronesia", "Florida", "Georgia", "Guam", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Marshall Islands", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Carolina", "North Dakota", "Northern Mariana Islands", "Ohio", "Oklahoma", "Oregon", "Palau", "Pennsylvania", "Puerto Rico", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virgin Islands", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"],
    jobs: [],
    monitoringJobs: false,
    notifications: [],
    messengerNotifications: 0,
    settingsView: 'SettingsNoSelection',
    messengerStaff: [],
    staffList: [],
    vehicleList: [],
    messengerUnreadToggle: 'All Messages',
    netradyneReportDateRange: [],
    reminderMileageDueInSortOrder: null,
    pendingImportJobs: [],
    importId: '',
    tempStatusCheckedDAlist: ['Active'],
    tempStatusCheckedVehiclelist: ['Active'],
    tempStatusCheckedReports:{},
    supportAdministrator: false,
    environment: process.env.VUE_APP_HERA_ENV,
    isDevEnvironment: process.env.VUE_APP_HERA_ENV === 'development',
    isProdEnvironment: process.env.VUE_APP_HERA_ENV === 'production',
    labelTypes: [],
    labelFiltersList: [],
    statusFiltersList: [],
    hasUserPermissions: process.env.VUE_APP_USER_PERMISSIONS === "1",
    filtersAssociateStatus: ['Active'],
    filtersCounselingStatus:[],
    filtersCounselingSeverity: [],
    dateOfOccurrenceCounseling: '',
    dateSendCounseling: '',
    filterStatusVehicleMaintenance: [],
    filterMaintStatusVehicleMaintenance: [],
    filterDateVehicleMaintenance: '',
    filterTypeOfActivityDailyReport: [],
    filterRosterStatusDailyReport: [],
    isGeneratingCounselingFiles: false,
    readOnlyAccess: {},
    customList: [],
    customListOption: {
      default: false,
      isEnabled: true,
      isCustom: true,
      usedFor: 0,
      daysCount: 0,
      canBeEdited: true,
      canBeDeleted: true,
      canBeReorder: true,
      accidents: { items: [] },
      associates: { items: [] },
      vehicles: { items: [] },
      parkingSpace: { items: [] },
      replaceByRouteParkingSpace: { items: [] },
      routeParkingSpace: { items: [] },
      counselings: { items: [] },
      issues: { items: [] },
      kudos: { items: [] },
      tempAdded: true
    },
    filtersStatusVehicleByType: [],
    filtersTypeVehicleByType: [],
    radioAssociateManagement: 'dashboard',
    tenantCoachingInfo: {},
    daPerformanceData: [],
    daScore: {},
    updatingDailyRosterRoute: false,
    customCSVImportCompleted: false,
    systemList: [],
    filtersVehicleList: {
      status: [],
      vehicleType: [],
      labels: [],
      company: [],
    },
    filterAssociateList: {
      status: [],
      labels: [],
      prefferedDaysOff: []
    }
  },
  getters: {
    isLoggedIn: state => {
      return !!state.userInfo?.id
    },
    isMobile() {
      return (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))
    },
    groups: state => {
      var groups
      if(state.sessionInfo){
        groups = state.sessionInfo.signInUserSession.accessToken.payload['cognito:groups']
      }
      else{
        groups = undefined
      }
      return groups
    },

    // account status
    isStandardAccount: (state, getters) => {
      return state?.userInfo?.tenant.accountPremiumStatus 
      && (
        state.userInfo.tenant.accountPremiumStatus.includes("standard")
        && !getters.isPlanNone
      )
    },
    isBundleAccount: (state, getters) => {
      return state?.userInfo?.tenant.accountPremiumStatus 
      && (
        state.userInfo.tenant.accountPremiumStatus.includes("bundle")
        && !getters.isPlanNone
      )
    },
    isTrialAccount: (state, getters) => {
      return state?.userInfo?.tenant.accountPremiumStatus 
      && (
        state.userInfo.tenant.accountPremiumStatus.includes("trial")
        && !getters.isPlanNone
      )
    },
    isUnpaidAccount: (state, getters) => {
      return state?.userInfo?.tenant.accountPremiumStatus && state.userInfo.tenant.accountPremiumStatus.includes("unpaid")
    },
    isTrialExpired: (state, getters) => {
      return  new Date(state?.userInfo?.tenant.trialExpDate).getTime() <  new Date().getTime()
    },
    isOustandingExpired: (state, getters) => {
      return getters.isUnpaidAccount && ( new Date(state.userInfo.tenant.payOutstandingByDate).getTime() <  new Date().getTime())
    },
    isPlanNone: (state, getters) => {
      return state?.userInfo?.tenant.accountPremiumStatus && state.userInfo.tenant.accountPremiumStatus.includes("None")
    },

    //PREMIUM TENANT PLANS/TENANT PERMISSIONS
    hasPerformanceCoaching: (state, getters) => {
      return (state?.userInfo?.tenant.accountPremiumStatus && state?.userInfo?.tenant.accountPremiumStatus.includes("performance") || (!getters.isTrialExpired) || getters.isBundleAccount)
      && !getters.isPlanNone
    },
    hasDailyRostering: (state, getters) => {
      return (state?.userInfo?.tenant.accountPremiumStatus && state?.userInfo?.tenant.accountPremiumStatus.includes("rostering") || (!getters.isTrialExpired) || getters.isBundleAccount)
      && !getters.isPlanNone
    },
    hasStaffManagement: (state, getters) => {
      return (state?.userInfo?.tenant.accountPremiumStatus && state?.userInfo?.tenant.accountPremiumStatus.includes("staff") || (!getters.isTrialExpired) || getters.isBundleAccount)
      && !getters.isPlanNone
    },
    hasVehicleManagement: (state, getters) => {
      return (state?.userInfo?.tenant.accountPremiumStatus && state?.userInfo?.tenant.accountPremiumStatus.includes("vehicles") || (!getters.isTrialExpired) || getters.isBundleAccount)
      && !getters.isPlanNone
    },
    hasReadOnlyAccess: (state, getters) => (permission) => {
      if(state.readOnlyAccess.hasOwnProperty(permission) && !getters.isPlanNone) {
        return state.readOnlyAccess[permission]
      }
      return false
    },

    //USER PERMISSIONS
    hasIncidentAccess: (state, getters) => {
      const tenant = state?.userInfo?.tenant;
      const accountPremiumStatus = state?.userInfo?.tenant.accountPremiumStatus;

      return (
        accountPremiumStatus.includes('bundle') ||
        accountPremiumStatus.includes('staff') ||
        accountPremiumStatus.includes('vehicles') ||
        (accountPremiumStatus.includes('trial') && !tenant.trialExpDate) ||
        (accountPremiumStatus.includes('trial') && !getters.isTrialExpired)
      );
    },
    hasCounselingManagement: (state, getters) => {
      return (
        !state?.hasUserPermissions ||
        (state?.hasUserPermissions && (state?.userInfo?.permissionFullAccess || state?.userInfo?.permissionManageCounselings))
      );
    },
    hasCounselingSettings: (state, getters) => {
      return !state?.hasUserPermissions || (state?.hasUserPermissions && state?.userInfo?.permissionCounselings);
    },
    hasMessengerManagement: (state, getters) => {
      return (state?.userInfo?.permissionMessenger ?? true) && !getters.isPlanNone
    },
    hasDailyRosteringManagement: (state, getters) => {
      return state?.userInfo?.permissionDailyRostering ?? true
    },
    hasPerformanceCoachingManagement: (state, getters) => {
      return state?.userInfo?.permissionPerformanceCoaching ?? true
    },
    hasDAManagement: (state, getters) => {
      return state?.userInfo?.permissionDAManagement ?? true
      
    },
    hasMessageTemplateManagement: (state, getters) => {
      return state?.userInfo?.permissionMessageTemplate ?? true      
    },
    hasVehicleManagementPermission: (state, getters) => {
      return state?.userInfo?.permissionVehicleManagement ?? true      
    },
    hasTasksReports: (state, getters) => {
      return state?.userInfo?.permissionTasksReports ?? true      
    },
    hasVehiclePhotoLogs: (state, getters) => {
      return state?.userInfo?.permissionVehiclePhotoLogs ?? true      
    },
    hasCustomListsManagement: (state, getters) => {
      return (
        !state?.hasUserPermissions ||
        (state?.hasUserPermissions && (state?.userInfo?.permissionFullAccess || state?.userInfo?.permissionCustomLists))
      );
    },
    // hasMessaging: (state, getters) => {
    //   return state.userInfo.tenant.accountPremiumStatus.includes("messaging") || (getters.isTrialAccount && !getters.isTrialExpired) || getters.isBundleAccount
    // },

    // user roles
    isSystemAdmin: (state, getters) => {
      return getters.groups && getters.groups.includes("system_admin")
    },
    isTeamLead: (state, getters) => {
      return state?.userInfo?.role.includes("teamlead")
    },
    isAdmin: (state, getters) => {
      if(!getters.groups) return false
      let isSystemAdmin = getters.groups && getters.groups.includes("system_admin")
      let isAdmin = getters.groups && getters.groups.includes("admin") || state?.userInfo?.role == "admin"
      return isSystemAdmin || isAdmin
    },
    hasLogguedSupportAdministrator: (state) => {
      return state.supportAdministrator
    },
    currEnv: (state) => {
      return state.environment
    },
    isDevEnv: (state) => {
      return state.isDevEnvironment
    },
    isProdEnv: (state) => {
      return state.isProdEnvironment
    },
    getCustomList: (state) => {
      return state.customList
    },
    vehicleTypeList: (state) => {
      const vehicleTypes = state.customList.find(item => item.type === 'vehicle-type')
      return vehicleTypes ? vehicleTypes.options : []
    },
    parkingSpaceList: (state) => {
      const parkingSpaces = state.customList.find(item => item.type === 'parking-space')
      return parkingSpaces ? parkingSpaces.options : []
    },
    incidentTypeList: (state) => {
      const incidentTypes = state.customList.find(item => item.type === 'incident-type')
      return incidentTypes ? incidentTypes.options : []
    },
    counselingSeverityTypeList: (state) => {
      const counselingSeverityTypes = state.customList.find(item => item.type === 'counseling-severity')
      return counselingSeverityTypes ? counselingSeverityTypes.options.sort((a, b) => a.order - b.order) : []
    },
    issueTypeList: (state) => {
      const issueTypes = state.customList.find(item => item.type === 'issue-type')
      return issueTypes ? issueTypes.options : []
    },
    kudoTypeList: (state) => {
      const kudoTypes = state.customList.find(item => item.type === 'kudo-type')
      return kudoTypes ? kudoTypes.options : []
    },
    issueTypeObject: (state) => {
      const issueTypes = state.customList.find(item => item.type === 'issue-type')
      const options = issueTypes ? issueTypes.options : []
      const object = {}
      options.forEach(item => {
        if(!object[item.option]) object[item.option] = item
      })
      return object
    },
    kudoTypeObject: (state) => {
      const kudoTypes = state.customList.find(item => item.type === 'kudo-type')
      const options = kudoTypes ? kudoTypes.options : []
      const object = {}
      options.forEach(item => {
        if(!object[item.option]) object[item.option] = item
      })
      return object
    },
    getCustomListByType: (state) => (type) => {
      const customList = state.customList.find(item => item.type == type)
      return customList ? customList : {}
    },
    //for filters of Counseling
    getFiltersAssociateStatus: (state) => {
      return state.filtersAssociateStatus;
    },
    getFiltersCounselingStatus: (state) => {
      return state.filtersCounselingStatus
    },
    getFiltersCounselingSeverity: (state) => {
      state.filtersCounselingSeverity=[]
      const customList = state.customList
      for(let custom of customList) {
        if (custom.type === "counseling-severity") {
          for (let opt of custom.options) {
            state.filtersCounselingSeverity.push(opt.option)
          }
        }
      }
      return state.filtersCounselingSeverity
    },
    getDateOfOccurrenceCounseling: (state) => {
      return state.dateOfOccurrenceCounseling
    },
    getDateSendCounseling: (state) => {
      return state.dateSendCounseling
    }, 
     //for filters of vehicle maintenance
    getFilterStatusVehicleMaintenance: (state) => {
      return state.filterStatusVehicleMaintenance
    },
    getFilterMaintStatusVehicleMaintenance: (state) => {
      return state.filterMaintStatusVehicleMaintenance
    },
    getFilterDateVehicleMaintenance: (state) => {
      return state.filterDateVehicleMaintenance
    },
    //filter of daily report
    getFilterTypeOfActivityDailyReport:(state) => {
      return state.filterTypeOfActivityDailyReport
    },
    getFilterRosterStatusDailyReport:(state) => {
      return state.filterRosterStatusDailyReport
    },
    getIpAddress:(state) => {
      return state.ipAddress;
    },
    getMutationNameMap:() => {
      return {
        CreateValueListItem: "Create Value List Item",
      };
    },
    // Custom List 
    existsInStore: (state, getters) => (value, type) => {
      let customList = getters.getCustomListByType(type)
      if(!Object.keys(customList).length) return
      const existsOption = customList.options.some(item => item.option.toLowerCase() === value.toLowerCase())
      if(existsOption) return
      const order = !customList.options.length? 0 : customList.options[customList.options.length -1].order +1

      const inputOption = JSON.parse(JSON.stringify(state.customListOption))
      const input = {
        id: mixinMethods.generateUUID(),
        group: customList.group,
        order: order,
        option: value,
        optionsCustomListsCustomListsId: customList.id,
        ...inputOption,
      }
      if(type === 'issue-type' || type === 'kudo-type'){
        input.driverReportSetting = 0
      }
      customList.options.push(input)
    },
    changeStatusOptionCustomList: (state, getters) => (row, type) => {
      let customList = getters.getCustomListByType(type);
      if(!Object.keys(customList).length || !customList.options?.length) return;
      customList.options.find(option => {
        if(option.id === row.id){
          option.isEnabled = row.isEnabled;
        }
      })
    },
    //for filters of report Vehicle By Type
    getFiltersStatusVehicleByType: (state) => {
      return state.filtersStatusVehicleByType
    },
    getFiltersTypeVehicleByType: (state) => {
      return state.filtersTypeVehicleByType
    },
    getAssociateManagementRadio: (state) => {
      return state.radioAssociateManagement
    },
    getTenantCoachingInfo: (state) => {
      return state.tenantCoachingInfo
    },
    getDaPerformanceData: (state) => {
      return state.daPerformanceData
    },
    getDaScore: (state) => {
      return state.daScore
    },
    getUpdatingDailyRosterRoute: (state) => {
      return state.updatingDailyRosterRoute
    },
    //labels
    getLabelTypes:(state) => {
      let labelTypesList = state.labelTypes.map(obj => { 
        const {id, name, ...rest} = obj 
        return {id, name};
      })
      return labelTypesList
    },
    getAssociateLabels:(state) => {
      let associateLabelList = state.labelTypes.find(labelType => labelType.name === "Associates")
      if(!Object.keys(associateLabelList).length) return []
      return associateLabelList.labelList?.items.filter(obj => obj.label.status == true).map(obj => obj.label)
    },
    getVehicleLabels:(state) => {
      let vehicleLabelList = state.labelTypes.find(labelType => labelType.name === "Vehicles").labelList.items
      vehicleLabelList = vehicleLabelList.filter(obj => obj.label.status == true).map(obj => { 
        const {id, label} = obj 
        return label;
      })
      return vehicleLabelList
    },
    getUpdatingDailyRosterRoute: (state) => {
      return state.updatingDailyRosterRoute
    },
    isZohoCrmDisabled: (state) => {
      return state.systemList[0].isZohoCrmDisabled
    },
    // vehicle list
    getVehicleListStatusFilter:(state) => {
      return state.filtersVehicleList.status ? state.filtersVehicleList.status : []
    },
    getVehicleListTypeFilter:(state) => {
      return state.filtersVehicleList.vehicleType ? state.filtersVehicleList.vehicleType : []
    },
    getVehicleListLabelsFilter:(state) => {
      return state.filtersVehicleList.labels ? state.filtersVehicleList.labels : []
    },
    getVehicleListCompanyFilter:(state) => {
      return state.filtersVehicleList.company ? state.filtersVehicleList.company : []
    },
  },
  mutations: {
    setSessionInfo(state, payload){     
      state.sessionInfo =  payload
    },
    setUserInfo(state, payload){     
      state.userInfo =  payload
    },
    setTenantName(state, payload){
      state.userInfo.tenant.companyName = payload
    },
    setTimeZone(state, payload) {
      state.userInfo.tenant.timeZone= payload
    },
    setCompanyLogo(state, payload){     
      state.companyLogo =  payload
    },
    setSettingsView(state, view){
      state.settingsView = view
    },
    setMonitoringJobs(state, payload){
      state.monitoringJobs = payload
    },
    setValuelists(state, payload){
      var key = payload.key
      var items = payload.items
      var id = payload.id
      let valueListObj = {
        ...state.valuelists,
        [key]: {
          id,
          items
        }
      }
      state.valuelists = valueListObj
    },
    setCustomList(state, payload) {
      state.customList = payload
    },
    setCustomListOption (state, { type, option }) {
      const customList = this.getters.getCustomListByType(type)
      const index = customList.options.findIndex(item => item.id == option.id)
      if(index == -1) return

      state.customList = state.customList.map(item => {
        if(item.type == type) {
          item.options[index] = option
        }
        return item
      })
    },
    setFilterAssociateList(state, { filter, value } ) {
      state.filterAssociateList[filter] = value
    },

    setNotifications(state, payload){
      state.notifications = payload
    },
    addNotification(state, payload){
      state.notifications.unshift(payload)
    },
    setMessengerNotifications(state, payload){
      state.messengerNotifications = payload
    },
    addMessengerNotification(state, payload){
      console.log(payload)
      state.messengerNotifications.unshift(payload)
    },
    setMessengerStaff(state, payload){
      state.messengerStaff = payload
    },
    setStaffList(state, payload){
      state.staffList = payload
    },
    setSignUpCredentials(state, payload){
      state.signUpCredentials = payload
    },
    setNetradyneDateRange(state, payload){
      state.netradyneReportDateRange = [...payload]
    },
    setVehicleList(state, payload){
      state.vehicleList = [...payload]
    },
    setStatusChecked(state, payload) {
      state.tempStatusCheckedDAlist = payload;
    },
    setStatusCheckedForVehicle(state, payload) {
      state.tempStatusCheckedVehiclelist = payload;
    },
    setStatusCheckedForReports(state, payload) {
      state.tempStatusCheckedReports = {...state.tempStatusCheckedReports, ...payload};
    },
    setSupportAdministrator(state, payload) {
      state.supportAdministrator = payload
    },
    //for filters of Counseling
    setFiltersAssociateStatus(state, list) {
      state.filtersAssociateStatus = list;
    },
    setFiltersCounselingSeverity(state, list){
      state.filtersCounselingSeverity = list
    },
    setFiltersCounselingStatus(state, list){
      state.filtersCounselingStatus = list
    },
    setDateOfOccurrenceCounseling(state, value){
      state.dateOfOccurrenceCounseling = value
    },
    setDateSendCounseling(state, value){
      state.dateSendCounseling = value
    },
    //for filters of vehicle maintenance
    setFilterStatusVehicleMaintenance(state, list) {
      state.filterStatusVehicleMaintenance = list
    },
    setFilterMaintStatusVehicleMaintenance(state, list) {
      state.filterMaintStatusVehicleMaintenance = list
    },
    setFilterDateVehicleMaintenance(state, value){
      state.filterDateVehicleMaintenance = value
    },
    //filter of daily report
    setFilterTypeOfActivityDailyReport(state, value){
      state.filterTypeOfActivityDailyReport = value
    },
    setFilterRosterStatusDailyReport(state, value){
      state.filterRosterStatusDailyReport = value
    },
    setIsGeneratingCounselingFiles(state, value) {
      state.isGeneratingCounselingFiles = value;
    },
    setReadOnlyAccess: (state, value) => {
      state.readOnlyAccess = value
    },
    setTemporaryValueInStore(state, payload){
      state.valuelists[payload.storeId].items.push({
        value: payload.value
      })
    },
    //for filters of report Vehicle By Type
    setFiltersStatusVehicleByType(state, value){
      state.filtersStatusVehicleByType = value
    },
    setFiltersTypeVehicleByType(state, value){
      state.filtersTypeVehicleByType = value
    },
    setSelectedRadioOption(state, value) {
      state.radioAssociateManagement = value;
    },
    setTenantCoachingInfo: (state, value) => {
      state.tenantCoachingInfo = value
    },
    setDaPerformanceData: (state, value) => {
      state.daPerformanceData = value
    },
    setDaScore: (state, value) => {
      state.daScore = value
    },
    // for labels epic
    setLabelTypes(state, list) {
      state.labelTypes = list;
    },
    // for labels filters
    setLabelFiltersList( state, list ){
      state.labelFiltersList = list
    },
    setStatusFiltersList( state, list ){
      state.statusFiltersList = list 
    },
    setVehicleListFilters(state, { filter, value } ) {
      state.filtersVehicleList[filter] = value
    },
    //end of labels
    setSelectedRadioOption(state, value) {
      state.radioAssociateManagement = value;
    },
    setTenantCoachingInfo: (state, value) => {
      state.tenantCoachingInfo = value
    },
    setDaPerformanceData: (state, value) => {
      state.daPerformanceData = value
    },
    setDaScore: (state, value) => {
      state.daScore = value
    },
    setUpdatingDailyRosterRoute: (state, value) => {
      state.updatingDailyRosterRoute = value
	},
    setSystemList: (state, value) => {
      state.systemList = value
    },
    setUpdateSystemList: (state, value) => {
      state.systemList = [value.data.updateSystem]
	},
  },
  actions: {
    // async monitorJobsStatus(context){
    //   console.log("monitoring job status'")
    //   context.commit('setMonitoringJobs', true)
    //   var timer = setInterval(async ()=>{
    //     var jobs = await checkJobsStatus()        
    //     context.commit('setJobs', jobs)
    //     jobs.forEach(job =>{
    //         let payload = {
    //             importId: job.jobId,
    //             jobId: job.id,
    //             job: job
    //         }
    //         context.commit('setImportJobs', payload)
    //     })
    //     var jobsFiltered = jobs.filter(job=>job.jobStatus != "SUCCEEDED")
    //     if( jobs.length == 0){
    //       context.commit('setMonitoringJobs', false)
    //       clearInterval(timer)
    //     }
    //   }, 5000)
    // },
    async markMessagesAsRead(context, payload){
      let notifications = context.state.messengerNotifications;
      let { messageUnread = 0 } = payload;
      if(notifications > 0) {
        context.commit ('setMessengerNotifications', notifications - messageUnread )
      }
    },
    async marNewkMessageRecieved(context) {
      let notifications = context.state.messengerNotifications;
      context.commit( 'setMessengerNotifications', notifications + 1 );
    },
    setCustomListOption: async ( { commit }, payload) => commit('setCustomListOption', payload),
    setVehicleListFilters: ( { commit }, payload) => commit('setVehicleListFilters', payload),
    async loadLabelTypes({ commit, state, event }) {
        try{
          const input = {
            group: state.userInfo.tenant.group
          } 

          let labelTypeList = await mixinMethods.gLoadListAll(labelTypeByNameAndGroup, input, "labelTypeByNameAndGroup");
          commit('setLabelTypes', labelTypeList);
        }catch(e){
            console.log(e)
      }
    },
    loadSystemList({ commit }) {
      new Promise((resolve, reject) => {
        mixinMethods
          .gLoadListAll(listSystems, {}, "listSystems")
          .then((data) => {
            commit("setSystemList", data);
            resolve(data);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    updateSystemList({ commit, state }, payload) {
      const id = state.systemList[0].id;
      const input = { id, ...payload };
      new Promise((resolve, reject) => {
        mixinMethods
          .api(updateSystem, { input })
          .then((data) => {
            commit("setUpdateSystemList", data);
            resolve(data);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
    setFilterAssociateList: ( { commit }, payload) => commit('setFilterAssociateList', payload)
  },
  modules: {
    subscriptionStore: require('./subscriptionModule').subscriptionStore
  },
  plugins: [
    createPersistedState({
      paths: ['userInfo', 'jobs', 'signUpCredentials', 'netradyneReportDateRange', 'customList', 'filtersVehicleList', 'filterAssociateList']
    })
  ]
})
