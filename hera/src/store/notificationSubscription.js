import { API, Auth, graphqlOperation } from 'aws-amplify';
import {notificationsByOwner} from '@/graphql/queries'
import {onCreateNotification} from '@/graphql/subscriptions';
import {updateNotification} from './mutations'
import store from '@/store'
import EventBus from '@/eventBus'

var subscription = null
window.subscriptionDisconnect = false

var loadNotifications = async function(){
    var input = { 
        filter: {
            isRead: { eq: false  } 
        },
        sortDirection: 'DESC',
        owner: store.state.userInfo.cognitoSub
    }
    
    var nextToken = null
    var notifications = []
    do{
        input.nextToken = nextToken
        var response = await API.graphql(graphqlOperation(notificationsByOwner, input ));
        notifications = [ ...notifications, ...response.data.notificationsByOwner.items]
        nextToken = response.data.notificationsByOwner.nextToken
    }while(nextToken)

    store.commit('setNotifications', notifications)
}

var subscribe = async function(retry = 0){


    try {


    console.log("listening for notifications")

    subscriptionDisconnect = false

    //First Load all unread notifications
    await loadNotifications()

    // setTimeout(() => {
        //Now subscribe to any new additions
        var input = {
            owner: store.state.userInfo.cognitoSub
        }
        subscription = API.graphql(
            graphqlOperation(onCreateNotification, input)
        ).subscribe({
            next: async (notification) => {
                retry = 0
                let payload = notification.value.data.onCreateNotification
                let obj = JSON.parse(payload.payload)
                const pathname = window.location.pathname;
                if ( pathname === "/performance-and-coaching/weekly-performance-data-import" ) {
                    try {
                        payload = { ...payload, isRead: true };
                        let input = {
                            id: payload.id,
                            isRead: true
                        }

                        await API.graphql(graphqlOperation(updateNotification, {input} ));
                    } catch (e) {
                        console.log('Update Notification Error', { e, payload });
                    }
                }

                //Check to see if notification type is a new version or invoices paid.
                //if new version, the version number is set in the description
                if(payload.title == 'NEW VERSION' || payload.title == 'DEACTIVATE USER' || payload.title == 'Outstanding Invoices Paid'){
                    await loadNewVersion(payload)
                    return
                }
                else if(payload.title == 'JOB COMPLETE'){
                    if(obj.name === 'CustomCSVImport'){
                        EventBus.$emit('mark-custom-csv-job-complete', obj)
                    }
                    if(obj.name === 'DailyDataImport'){
                        EventBus.$emit('mark-daily-job-complete', obj)
                    }else{
                        obj.notificationId = payload.id
                        EventBus.$emit('mark-job-complete', obj)
                    }
                    return
                }
                else if(payload.title.toLowerCase().includes('import complete')){
                    // show results if still on import wizard
                    let params = obj.params
                    if(obj.name === 'CustomCSVImport'){
                        EventBus.$emit('load-custom-csv-import-results', params)
                    }
                    if(obj.name === 'DailyDataImport'){
                        EventBus.$emit('load-daily-import-results', params)
                    }else{
                        EventBus.$emit('load-import-results', params)
                    }
                }else if(payload.title.includes('Error importing your weekly performance data') || payload.title.includes('Warning')){
                    EventBus.$emit('weekly-performance-fails', payload)
                } else if(payload.title.includes('does not have a phone number')){
                    EventBus.$emit('missing-phone-number', payload)
                }

                store.commit('addNotification', payload)
            },
            error: (errors) => {
                subscriptionDisconnect = true
                subscribe()
            },
        }, (error) => {
            subscriptionDisconnect = true
            subscribe()
        });
    // }, retry)

    } catch(e) {
        subscriptionDisconnect = true
        if(e.message === 'No current user'){
            unsubscribe()
        }else{
            console.log({e})
        }
    }
}


var loadNewVersion = async function(payload){
    //mark notification as read.
    // let input = { 
    //     id: payload.id, 
    //     isRead: true
    // }
    // await API.graphql(graphqlOperation(updateNotification, {input} ));


    //Check to see if we already have the latest version
    if(payload.description === store.state.version ){
        console.log("Already updated")
        return
    }

    //See if we need to logout
    if(payload.clickAction == 'LOGOUT'){
        try{
            console.log("LOGOUT")
            store.commit('setSessionInfo', null)
            store.commit('setUserInfo', null)
            document.location.href="/";
            location.reload();
        }catch(e){
            console.log('Logout Error', { e, payload });
        }
    }else{
        //Reload page
        location.reload();
    }

}

var unsubscribe = async function(){
    subscription?.unsubscribe();
}


export {subscribe, unsubscribe, loadNotifications}