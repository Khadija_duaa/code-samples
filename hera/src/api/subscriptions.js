export const onCreateMessageByGroup = /* GraphQL */ `
  subscription OnCreateMessageByGroup($group: String!) {
    onCreateMessageByGroup(group: $group) {
      id
      group
      createdAt 
      attachment{
        id
        s3Key
        contentType
      }
      sender {
        id
      }
      isReadBy {
        items {
          userID
        }
      }
      channelType
      staffId
      senderName
      bodyText
      isReadS
      messageType
      staff{
        status
      }
    }
  }
`;
export const onUpdateMessageByGroup = /* GraphQL */ `
  subscription OnUpdateMessageByGroup($group: String!) {
    onUpdateMessageByGroup(group: $group) {
      id
      group
      smsStatus
      smsSendInformation
    }
  }
`;