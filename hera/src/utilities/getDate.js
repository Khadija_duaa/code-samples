import moment from 'moment'
import momentTz from 'moment-timezone'
import store from '@/store/index'
/**
 * This function is used to send by parameter the current date in a specific format and is used for * daily rostering
 * @returns date format: YYYY-MM-DD
 */
const getTenantTimeZone = ()=> store.state.userInfo?.tenant.timeZone || ''

export function date(onlyDate = null){
    var today = new Date()
    const tenantTimeZone = getTenantTimeZone()
    if(onlyDate && tenantTimeZone){
        const dateTenantTimeZone = moment().tz(tenantTimeZone).format()
        today = dateTenantTimeZone.split("T")[0]
    }
    return today
}

export function dateTimeZone(date) {
    const todayConvert = getUtcTimestampInTenantTimeZone(date)
    return todayConvert.split("T")[0]
}

function getUtcTimestampInTenantTimeZone(timestamp) {
    const parseZone = moment.tz(timestamp, getTenantTimeZone()).utc().format();
    return parseZone;
}

export function getDateLimitsInTenantTimeZone(date) {
    const tenantTimeZone = getTenantTimeZone()
    const isValidTimezone = !!momentTz.tz.zone(tenantTimeZone);
    const dateStr = moment(date).format('YYYY-MM-DD');
    if (!isValidTimezone) {
        return [
            moment(dateStr, 'YYYY-MM-DD').toISOString(),
            moment(dateStr, 'YYYY-MM-DD').add(1, 'day').toISOString(),
        ];
    }
    const start = momentTz.tz(dateStr, tenantTimeZone).toISOString();
    const end = momentTz.tz(dateStr, tenantTimeZone).add(1, 'day').toISOString();
    return [start, end];
}

export const betweenDateTime = function(date, start, end){
    const a = new Date(date).getTime()
    const b = new Date(start).getTime()
    const c = new Date(end).getTime()
    return a > b && a < c
}


export const isAfterDateTime = function(dateA, dateB){
    const a = new Date(dateA).getTime()
    const b = new Date(dateB).getTime()
    return a > b
}