export const lookupMapByKey = function(list, key){
    return list.reduce((map, entity) => (map[entity[key]] = entity, map), {})
}

export const lookupMapById = function(list){
    return lookupMapByKey(list, 'id')
}

export const lookupItem = function(lookupMap, itemA, keyOrCallback){
    //match by Id
    const match = lookupMap[itemA.id] || itemA
    if(typeof keyOrCallback === 'string'){
        return match[keyOrCallback]
    }
    if(typeof keyOrCallback === 'function'){
        return keyOrCallback(match)
    }
    return match
}