import momentTz from 'moment-timezone';

/**
 * Takes an array of objects and parses the specified date attributes listed on 'dateFields'
 * to a date in certain timezone
 * @param {Object|Array} data -> Object or List of objects which 'dateFields' will be parsed
 * @param {String} timezone -> Timezone in which the dates should be parsed to
 * @param {Array} dateFields -> List of date attributes that should be parsed for each object in 'data'
 * @returns {Array} If 'data' is an Object returns a shallow copy of 'data' with object date attributes parsed
 * if 'data' is an Array then it returns a list objectcs with parsed 'dateFields'
 */
export function parseDatesToTimezone(data = [], timezone = '', dateFields = []) {
  const isValidTimezone = !!momentTz.tz.zone(timezone);
  const isoFormat = 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]';

  const parseObjectDateFields = (obj) => {
    const objShallowCopy = { ...obj };

    dateFields.forEach(dateField => {
      const timestamp = objShallowCopy[dateField.field] ?? objShallowCopy[dateField];
      if (timestamp && isValidTimezone) {
        if (!dateField.format) {
          objShallowCopy[dateField] = momentTz(timestamp).tz(timezone).format(isoFormat);
        } else {
          objShallowCopy[dateField.field] = momentTz(timestamp).tz(timezone).format(dateField.format);
        }
      }
    });

    return objShallowCopy;
  };

  if (Array.isArray(data)) {
    return data.map(obj => parseObjectDateFields(obj));
  }

  return parseObjectDateFields(data);
}
