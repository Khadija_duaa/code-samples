export function isEmailAllowed(userEmail=''){
    if(!userEmail) return false
    const emailsExcluded = ["@herasolutions.info", "@herasolutions.app", "@ctdevelopers.com","@solveitsimly.com", "@northbaysolutions.net", "@northbaysolutions.com"]
    const domain = '@'+userEmail.split("@").pop()
    return !emailsExcluded.includes(domain)
}