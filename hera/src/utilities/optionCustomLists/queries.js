export const getOptionsCustomList = /* GraphQL */ `
  query GetOptionsCustomLists($id: ID!) {
    getOptionsCustomLists(id: $id) {
      id
      group
    }
  }
`;
