import { API, graphqlOperation } from 'aws-amplify';
import store from '@/store/index.js'
import { staffsByEmail,staffsByTransporterId, staffsByPhone } from '@/views/Staff/queries'
import { usersByPhone } from '@/views/Settings/Users/query'
import { vehiclesByVin, vehiclesByGroup } from "@/views/Vehicles/queries"
import { validatePhoneNumberFormatSingle, cleanPhoneNumber } from '@/utilities/phoneNumberFormat'
import {i18n} from '@/main'
export default {

    async validatePhoneNumberDA(rule, value, callback)
    {
      if (!value) {
        callback()
      } else {
        const validator = await validatePhoneNumberFormatSingle(value);
        if (!validator) {
          callback(new Error('Please enter a valid US mobile number.'))
        }
        else {
            value = cleanPhoneNumber(value, 'US')
            var input = { group: { eq: store.state.userInfo.tenant.group }, phone: value }
            var existingPhone = await API.graphql(graphqlOperation(staffsByPhone, input))
            var daIds = existingPhone.data.staffsByPhone.items.map(da => { return da.id })
            if(rule.daId) var isCurrentDa = daIds.includes(rule.daId)
            if(daIds.length && !isCurrentDa) {
              callback(new Error('The phone number entered already exists.'))
            } else {
              callback();
            }
        }
      }
    },

    async validatePhoneNumberUser(rule, value, callback)
    {
      if (!value) {
        callback()
      } else {
        const validator = await validatePhoneNumberFormatSingle(value);
        if (!validator) {
          callback(new Error('Please enter a valid US mobile number.'))
        }
        else {
            value = cleanPhoneNumber(value, 'US')
            var input = { phone: value }
            var existingPhone = await API.graphql(graphqlOperation(usersByPhone, input))
            var isPhoneInSameTennant = false;
            var userTennats = existingPhone.data.usersByPhone.items.map(da => { return da.group })
            for(var user of userTennats) {
              isPhoneInSameTennant = user.includes(rule.params.group)
              if(isPhoneInSameTennant) break;
            }
            var daIds = existingPhone.data.usersByPhone.items.map(da => { return da.id })
            if(rule.params.daId) var isCurrentDa = daIds.includes(rule.params.daId)
            if(daIds.length && !isCurrentDa && isPhoneInSameTennant) {
              callback(new Error('The phone number entered already exists.'))
            } else {
              callback();
            }
        }
      }
    },
    
    async uniqueVIN(rule, value, callback){
      if(value){
        var input = { group: { eq: store.state.userInfo.tenant.group }, vin: value }
        var existingVIN = await API.graphql(graphqlOperation(vehiclesByVin, input))
        var vehicleIds = existingVIN.data.vehiclesByVin.items.map(vehicle => { return vehicle.id })
        if(rule.daId) var isCurrentDa = vehicleIds.includes(rule.daId)
        if(vehicleIds.length && !isCurrentDa){
          let message = `A VIN already exists with this vehicle.`
          callback(new Error(message))
        }
      }
      callback()
    },  

    async uniqueEmailDA(rule, value, callback){
      if(value){
        var input = { group: { eq: store.state.userInfo.tenant.group }, email: value }
        var existingEmail = await API.graphql(graphqlOperation(staffsByEmail, input))
        var daIds = existingEmail.data.staffsByEmail.items.map(da => { return da.id })
        if(rule.daId) var isCurrentDa = daIds.includes(rule.daId)
        if(daIds.length && !isCurrentDa){
          let message = `${i18n.t('label.An_associate')} already exists with this email.`
          callback(new Error(message))
        }
      }
      else if(rule.required && !value){
        let message = "Please input an email address"
        callback(new Error(message))
      }
      callback()
    },  

    async validateLicensePlate(rule, value, callback) {
      let isDuplicated = false;
      if( (rule.formField.licensePlate == '' || rule.formField.licensePlate == null ) && rule.formField.state!=''){
        let message = `License Plate is required when you provide a state.`
        callback(new Error(message))
      } else if(rule.formField.licensePlate == '' && rule.formField.state == ''){
        callback();
      } else {
        let currentCombination = rule.formField.licensePlate+rule.formField.state;
        let input = { group: store.state.userInfo.tenant.group, limit: 1000 }
        let response = await API.graphql(graphqlOperation(vehiclesByGroup, input))

        let currentValue
        let isValueChanged = false
        for(let vehicle of response.data.vehiclesByGroup.items) {
          let combination = vehicle.licensePlate+vehicle.state;
          if(currentCombination.trim() == combination){
            isDuplicated = true;
          }
          if(vehicle.id == rule.formField.id){
            currentValue = vehicle.licensePlate
          }
        }
        
        if(currentValue != value) isValueChanged = true
        if(isValueChanged && isDuplicated) {
          callback(new Error('This set for License Plate and State already exists'))
        }
      }
    },

    async validateState(rule, value, callback) {
      let isDuplicated = false;
      if( (rule.formField.state == '' || rule.formField.state == null) && rule.formField.licensePlate!='') {
        let message = `License State is required if you add a License Plate.`
        callback(new Error(message))
      } else if(rule.formField.licensePlate == '' && rule.formField.state == ''){
        callback();
      } else {
        
        let currentCombination = rule.formField.licensePlate+rule.formField.state;
        let input = { group: store.state.userInfo.tenant.group, limit: 1000 }
        let response = await API.graphql(graphqlOperation(vehiclesByGroup, input))

        let currentValue
        let isValueChanged = false
        for(let vehicle of response.data.vehiclesByGroup.items) {
          let combination = vehicle.licensePlate+vehicle.state;
          if(currentCombination.trim() == combination){
            isDuplicated = true;
          }
          if(vehicle.id == rule.formField.id) {
            currentValue = vehicle.state
          }
        }
        
        if(currentValue != value) isValueChanged = true
        if(isValueChanged && isDuplicated) {
          callback(new Error('This set for License Plate and State already exists'))
        }
      }
    },
    
    async uniqueTransporterIdDA(rule, value, callback){
      if(value){
        var input = { group: { eq: store.state.userInfo.tenant.group }, transporterId: value }
        var existingTransporterId = await API.graphql(graphqlOperation(staffsByTransporterId, input))
        var daIds = existingTransporterId.data.staffsByTransporterId.items.map(da => { return da.id })
        if(rule.daId) var isCurrentDa = daIds.includes(rule.daId)
        if(daIds.length && !isCurrentDa){
          let message = `${i18n.t('label.An_associate')} already exists with this Transporter ID.`
          callback(new Error(message))
        }
      }
      else if(rule.required && !value){
        let message = "Please input a Transporter ID"
        callback(new Error(message))
      }
      callback()
    },  

    /**
     * Custom validator that ensures something resembling an email is present.
     * @param {Object} rule 
     * @param {String} value 
     * @param {Function} callback 
     */
    email(rule, value, callback){
        var old_re = /\S+@\S+\.\S+/
        var re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
        var isEmail = re.test(value) && old_re.test(value)
        if( !isEmail || value === '' ) {
            let message = rule.message || "A valid email is required"
            callback(new Error(message))
        }
        callback()
    },

    vehicleNameFormatAndRequired(rule, value, callback){
      if(!value){
        let message = rule.message || "A vehicle name is required"
        callback(new Error(message))
      }
      /**
       * 
       *Must be between 3 and 63 characters long.
        Can consist only of letters, numbers, dots (.), and hyphens (-).
        Must begin and end with a letter or number.
        Can't begin with "xn--".
       */
      var re = /^(?!xn--)[a-zA-Z0-9]{1}[a-zA-Z0-9-.]{1,61}[a-zA-Z0-9]{1}$/
      var isValid = re.test(value);

      if(!isValid){
        let message = rule.message || "A value name is required. Use rules listed below"
        callback(new Error(message))
      }
      callback()
    },

    time(rule, value, callback){
        var re = /^((1[0-2])|(0?[0-9])):[0-5][0-9](a|p|A|P)(m|M)$/g
        if (!value) {
          callback(new Error('Invalid Time. Use format HH:MMAM/PM'));
        }
        else if (!value.match(re)) {
          callback(new Error('Invalid Time. Use format HH:MMAM/PM'));
        }
        else {
          callback();
        }
    },

    timeNotConfirmed(rule, value, callback){
      var re = /^((1[0-2])|(0?[0-9])):[0-5][0-9](a|p|A|P)(m|M)$/g
      if (!value) {
        callback();
      }
      else if (!value.match(re)) {
        callback(new Error('Invalid Time. Use format HH:MMAM/PM'));
      }
      else {
        callback();
      }
  },

    staff(rule, value, callback){
      if(!value || !value.id){
        let message = rule.message || `Please select ${i18n.t('label.an_associate')}`
        callback(new Error(message))
      }
      else {
        callback();
      }
    },

    incidentType(rule, value, callback){
      if(!value.id){
        let message = rule.message || "Please select a incidentType"
        callback(new Error(message))
      }
      else {
        callback();
      }
    },

    verifiedBy(rule, value, callback){
      if(!value.id){
        let message = rule.message || "Please select a verifiedBy"
        callback(new Error(message))
      }
      else {
        callback();
      }
    },

    user(rule, value, callback){
      if(!value.id){
        let message = rule.message || "Please select a user"
        callback(new Error(message))
      }
      else{
        callback();
      }
    },
    
    odometerType(rule, value, callback){
      if(!value){
        let message = rule.message || "Please select a type"
        callback(new Error(message))
      }
      else{
        callback();
      }
    },

    delete(rule, value, callback){
      if(value) value = value.toLowerCase().trim()
      if( value != 'delete' ) {
          let message = rule.message || "Please enter the correct phrase to confirm"
          callback(new Error(message))
      }
      callback()
  },


  vehicle(rule, value, callback){
    if(!value || !value.id){
      let message = rule.message || "Please select a vehicle"
      callback(new Error(message))
    }
    else {
      callback();
    }
  },

  assigneeCheck(rule, value, callback){
    if(value.id == null || value.id == undefined || value.id == ""){
      let message = rule.message || "Please select an assignee"
      callback(new Error(message))
    }
    else {
      callback();
    }
  },

  maintenanceReminder(rule, value, callback){
    if(!rule.dueByDate && !rule.dueByMileage){
      let message = rule.message || "Date or mileage must be filled in"
      callback(new Error(message))
    }
    else{
      callback();
    }
  },

  maintenanceServices(rule, value, callback){
    if(!value || value.length == 0){
      let message = rule.message || "A service must be selected"
      callback(new Error(message))
    }
    else{
      callback();
    }
  },

  maintenanceVehicles(rule, value, callback){
    if(!value || value.length == 0){
      let message = rule.message || "A Vehicle must be selected"
      callback(new Error(message))
    }
    else{
      callback();
    }
  }
}