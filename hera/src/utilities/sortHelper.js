const sortByName = (a, b) => {
  return a.name.toLowerCase().localeCompare(b.name.toLowerCase(), undefined, {numeric: true, sensitivity: 'base'})
};

const sortByRecords = ( a, b) => {
  return a.numberOfRecords - b.numberOfRecords;
}

export { sortByName, sortByRecords };