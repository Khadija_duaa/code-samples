export const INCIDENT_ROUTE_NEW = 'IncidentNew';
export const INCIDENT_ROUTE_EDIT = 'IncidentEdit';
export const INCIDENT_ROUTE_INDEX = 'AccidentIndex';

export const VEHICLE_DAMAGE_ROUTE_INDEX = 'VehicleDamageIndex';
export const VEHICLE_DAMAGE_ROUTE_RECENT = 'RecentVehicleDamage';
