//type for MessagePreferenceType of the MessagePreferencesHistory table
export const SMS = 'SMS';
export const EMAIL = 'EMAIL';

//format for datetime of MessagePreferencesHistory records
export const datetimeFormat = 'M/D/YYYY h:m:ss A';
