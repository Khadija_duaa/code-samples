export default function msgCounselingToDA( msgType, userName, msgLink ){
    if( msgType == 'text') 
        return `${userName}, you have a counseling that requires your acknowledgment: ${msgLink}`
    else
        return `${userName}, you have a counseling that requires your acknowledgment:<br><br><a href="${msgLink}">${msgLink}</a>`
}