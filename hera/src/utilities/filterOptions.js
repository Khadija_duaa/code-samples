/**
 * Creates and returns a list of date options to feed filters
 * @param {String} selectedOption -> Selected option by default
 * @returns {Array} List of default options for date filters
 */
export function getDateFilterOptions(selectedOption='') {
  const optionList = [
    { value: "all dates", label: "All Dates", selected: true },
    { value: "current week", label: "Current Week", selected: false },
    { value: "last week", label: "Last Week", selected: false },
    { value: "last 2 weeks", label: "Last 2 Weeks", selected: false },
    { value: "last 3 weeks", label: "Last 3 Weeks", selected: false },
    { value: "previous month", label: "Previous Month", selected: false },
    { value: "last 30 days", label: "Last 30 Days", selected: false },
    { value: "last 60 days", label: "Last 60 Days", selected: false },
    { value: "last 90 days", label: "Last 90 Days", selected: false },
    { value: "current year", label: "Current Year", selected: false },
    { value: "last year", label: "Last Year", selected: false },
  ];

  if (!selectedOption) return optionList;

  return optionList.map(item => ({
    ...item,
    selected: item.value === selectedOption,
  }));
}
