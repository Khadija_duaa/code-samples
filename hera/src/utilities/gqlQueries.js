export const literalTypeDef /* GraphQL */ = function(strings, ...values){
    const fieldNames = strings.reduce((map, str, index) => {
        let aux
        if(str){
            aux = str.trim().split('\n')
            aux = aux[aux.length-1]
            const rawName = aux?.replace(/[\}\)\(\{]/g, '')?.trim()
            if(rawName){
                const { field, type } = /\$?(?<field>[\w\d]*):\s*(?<type>[\w\d]*!?)/g.exec(rawName)?.groups || {}
                map.push({
                    field,
                    type,
                    value: values[index]
                })
            }
        }
        return map
    }, [])
    return fieldNames
}

const extractFragments = function(fragments, proxy){
    Object.entries(fragments)?.forEach(([key, fragment]) => {
        const matchNestedFragments = fragment.match(/fragment\s*[\w\d]*\s*on\s*[\w\d]*\s*\{/g)
        if(matchNestedFragments){
            const lastNestedFragmentIndex = matchNestedFragments.length - 1
            matchNestedFragments.forEach((mnf, index) => {
                let nestedFragment
                if(index === lastNestedFragmentIndex){
                    nestedFragment = fragment.substring(fragment.indexOf(mnf))
                }else{
                    nestedFragment = fragment.substring(fragment.indexOf(mnf), fragment.indexOf(matchNestedFragments[index+1]))
                }
                const { fragmentName, tableName } = /fragment\s*(?<fragmentName>[\w\d]*)\s*on\s*(?<tableName>[\w\d]*)\s*\{/g.exec(nestedFragment)?.groups || {}
                if(fragmentName && tableName && !proxy.fragments[fragmentName+tableName]){
                    proxy.fragments[fragmentName+tableName] = nestedFragment.replace(/( )+/g, ' ').trim()
                }
            })
        }
    })
}

const parseBody = function(item, parameters, proxy){
    let res = item.body.replace(/(^\{|^\(|\)$|\}$| *)/g, '').trim()
    let { header, queryAlias, queryName } = /(?<header>((?<queryAlias>[\w\d]*):)?(?<queryName>[\w\d]*)\{)/g.exec(res)?.groups || {}
    let key = queryAlias || queryName
    if(queryAlias){
        const alias = proxy.usedKeys[queryAlias] ? (queryAlias + proxy.usedKeys[key]) : queryAlias
        proxy.body += res.replace(header, `${alias?alias+':':''} ${queryName}(\n${parameters}\n){`).trim() + '\n'
    }else{
        const alias = proxy.usedKeys[queryName] ? (queryName + proxy.usedKeys[queryName]) : queryName
        proxy.body += res.replace(header, `${alias!==queryName?alias+':':''} ${queryName}(\n${parameters}\n){`).trim() + '\n'
    }
    !proxy.usedKeys[key] ? (proxy.usedKeys[key] = 1) : (proxy.usedKeys[key]++)
}

const prepareQuery = function(item, index, proxy){
    let localParameters = ''
    item.parameters?.forEach(({field, type, value}) => {
        const localField = field + index
        proxy.variables[localField] = value
        proxy.inputParameters += `$${localField}: ${type}\n`
        localParameters += `${field}: $${localField}\n`
    })
    parseBody(item, localParameters.trim(), proxy)
}

export const createMultipleQuery = function({fragments=[], expressions=[], name = "CustomQuery"}){
    const proxy = {
        fragments: {},
        inputParameters: '',
        usedKeys: {},
        query: undefined,
        body: '',
        variables:{}
    }
    extractFragments(fragments, proxy)
    expressions.forEach((item, index) => prepareQuery(item, index, proxy))
    if(proxy.inputParameters){
        proxy.query = /* GraphQL */ `query ${name}(\n${proxy.inputParameters.trim()}\n){\n${proxy.body}\n}`
    }else{
        proxy.query = /* GraphQL */ `query ${name}{\n${proxy.body}\n}`
    }
    Object.entries(proxy.fragments).forEach(([k, fragmentStr]) => (proxy.query = `${fragmentStr}\n${proxy.query}`) )
    return {
        query: proxy.query,
        variables: proxy.variables
    }
}

  