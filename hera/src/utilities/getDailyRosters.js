import { API, graphqlOperation } from 'aws-amplify'

const dailyRostersByGroupAndNotesDate = /* GraphQL */ `
  query DailyRostersByGroupAndNotesDate(
    $group: String
    $notesDate: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelDailyRosterFilterInput
    $limit: Int
    $nextToken: String
  ) {
    dailyRostersByGroupAndNotesDate(
      group: $group
      notesDate: $notesDate
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        notesDate
        route {
            items {
                id
                isNotActive
                standby
                staff {
                    id
                    firstName
                }
                replaceByRoute {
                  items {
                    id
                    isNotActive
                    staff {
                        id
                        firstName
                    } 
                    status
                  } 
                }
                status
            }
        }
      }
      nextToken
    }
  }
`;

function getRouteStaff(routes) {
  let routeStaff = [];
  const excludedGroup = [
    'Called Out',
    'No Show, No Call',
    'VTO'
  ];
  routes.forEach((route) => {
    if (route.id && !excludedGroup.includes(route.status) && !route.isNotActive && !route.standby) {
      routeStaff.push(route);
    } else if (route.id && excludedGroup.includes(route.status) && route.isNotActive && !route.standby) {
      route.replaceByRoute.items.forEach((replacement) => {
        if (replacement.id && !excludedGroup.includes(replacement.status) && !replacement.isNotActive) {
          routeStaff.push(replacement);
        }
      });
    }
  });
  return routeStaff;
}

export default async function getDailyRosters(group, date) {
  let response = false
  try {
    let input = {
      notesDate: {
        eq: date
      },
      group: group
    }

    const { data } = await API.graphql(graphqlOperation(dailyRostersByGroupAndNotesDate, input))
    const dailyRosterResponse = data.dailyRostersByGroupAndNotesDate.items
    let routes = []
    if(dailyRosterResponse[0]) routes = dailyRosterResponse[0].route.items
    response = getRouteStaff(routes).length ? true : false;
  }
  catch (e) {
    console.error('Something went wrong');
  }
  finally {
    return response
  }
}