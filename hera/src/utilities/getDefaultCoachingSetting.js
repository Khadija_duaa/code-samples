export function getDefaultCoachingSetting(weeklyList=[], dailyList=[]){
    let metrics = {
      'Rescuer': { defaults: { kudoSlider: 50 } },
      'CalledOut': { defaults: { issueSlider: 70 } },
      'LateWithCall': { defaults: { issueSlider: 50 } },
      'LateNoCall': { defaults: { issueSlider: 50 } },
      'NoShowNoCall': { defaults: { issueSlider: 100 } },
      'VehicleDamage': { defaults: { issueSlider: 100 } },
      'RescuedDA':{ defaults: { issueSlider: 0 } },
      'AmazonViolationDefect':{ defaults: { issueSlider: 100 } },
    }
    weeklyList.forEach(item => {
        if(!metrics[item.sliderField]) metrics[item.sliderField] = item
    })
    dailyList.forEach(item => {
        if(!metrics[item.sliderField]) metrics[item.sliderField] = item
    })
    return metrics
}