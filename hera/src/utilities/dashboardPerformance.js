import moment from 'moment';
/**
 * Help with the calculation of the color of the icon depending on the type of table
 * @param [data] - an array of objects
 * @param typeTable - thisWeek, lastWeek, yesterday, beforeYesterday, thisQuarter, thisWeekDaily, lastWeekDaily
 * @return true or false
 */
export async function calculateIconColor(data = [], typeTable){
   
    if (typeTable === "thisWeek" || typeTable === "lastWeek" || typeTable == "yesterday" || typeTable == "beforeYesterday") {
        //weekly and daily files
        let iconTab = true
        for (let item of data) {
            if (item.importData.importStatus === false) {
                iconTab = false
            }
        }
        return iconTab
    }else if(typeTable === "thisQuarter") {
        //weekly files
        let iconTab = true
        for (let item of data) {
            if (item.scorecard.importStatus === false || item.pod.importStatus === false || item.cfeedback.importStatus === false) {
                iconTab = false
            }
        }
        return iconTab
    }else if (typeTable === "thisWeekDaily" || typeTable === "lastWeekDaily") {
        //daily files
        let iconTab = true
        for (let dailyData of data) {
            if (dailyData.mentor.importStatus === false || dailyData.netradyne.importStatus === false || dailyData.eoc.importStatus === false) {
                iconTab = false
            }
        }
        return iconTab
    }
    return false

}

function returnWeeklyData(item){
    return {
        actionButtonText: 'Download Saved File',
        importData: {
            importStatus: true, 
            createdAt: item.createdAt, 
            date: item.week,
            key: item.key,
            id: item.id
        }
    }
}

function returnDailyData(item){
    return {
        actionButtonText: 'Download Saved File',
        importData: {
            importStatus: true, 
            createdAt: item.createdAt, 
            date: item.date,
            key: item.key,
            id: item.id
        }
    }
}


/**
 * 
 * The first array of objects is called data, and the second array of objects is called weekData.
 * 
 * The function loops through the data array, and if the week and year of the object in the data array
 * matches the week and year of the object in the weekData array, it will update the object in the
 * weekData array with the data from the object in the data array.
 * 
 * The function returns the weekData array.

 * @param [data] - array of objects
 * @param [weekData] - [{fileType: 'scorecard', actionButtonText: 'Import File Now', importData:
 * {importStatus: false, createdAt: null, date: null, key: null}}]
 * @param selectWeek - '01' ... '53'
 * @param year - 2023 (example)
 * @returns [weekData] - array of objects
 */
export async function fillSpecificWeekOrDayTable(data=[], weekData=[], selectWeekOrDay, year, typeFiles){
    const dailys = typeFiles === 'dailys'
    const dataFound = {
        actionButtonText: 'Import File Now',
        importData: {
            importStatus: false, 
            createdAt: null, 
            date: null,
            key: null
        }
    }
    let scorecardData = dataFound
    let podData = dataFound
    let cxFeedbackData = dataFound
    let mentorData = dataFound
    let netradyneData = dataFound
    let eocData = dataFound
    for(let item of data) {
        let itemWeekOrDate = item.week === selectWeekOrDay && item.year === year
        if(dailys) itemWeekOrDate = item.date === selectWeekOrDay

        if(itemWeekOrDate){
            const typeFile = {
                scorecard: () => {
                    scorecardData = returnWeeklyData(item)
                },
                podQualityFeedback: () => {
                    podData = returnWeeklyData(item)
                },
                customerFeedback: () => {
                    cxFeedbackData = returnWeeklyData(item)
                },
                mentor: () => {
                    mentorData = returnDailyData(item)
                },
                netradyne: () => {
                    netradyneData = returnDailyData(item)
                },
                eoc: () => {
                    eocData = returnDailyData(item)
                },
            }
            if(!!typeFile[item.type]) typeFile[item.type]()
        }
    }

    for(let fileData of weekData){
        const typeFile = {
            scorecard: () => {
                fileData.actionButtonText = scorecardData.actionButtonText
                fileData.importData = scorecardData.importData
            },
            podQualityFeedback: () => {
                fileData.actionButtonText = podData.actionButtonText
                fileData.importData = podData.importData
            },
            customerFeedback: () => {
                fileData.actionButtonText = cxFeedbackData.actionButtonText
                fileData.importData = cxFeedbackData.importData
            }, 
            mentor: () => {
                fileData.actionButtonText = mentorData.actionButtonText
                fileData.importData = mentorData.importData
            },
            netradyne: () => {
                fileData.actionButtonText = netradyneData.actionButtonText
                fileData.importData = netradyneData.importData
            },
            eoc: () => {
                fileData.actionButtonText = eocData.actionButtonText
                fileData.importData = eocData.importData
            },
            error: () => {
                console.error('fileType '+fileData.fileType+' not defined in function fillSpecificWeekOrDayTable(): fileType is only of type: scorecard, podQualityFeedback, customerFeedback, mentor or eoc')
            }            
        }
        if(!!typeFile[fileData.fileType]) typeFile[fileData.fileType]()   
        else typeFile['error']()
    }
    return weekData
}

/**
 * It takes an array of objects, an array of dates, a year, and a string, and returns an array of
 * objects.
 * @param [data] - array of objects 
 * @param [weekOrDayRange] - [ '01', ..., '53'] or [ '2023-01-01', ..., '2023-12-31']
 * @param year - 2023 (example)
 * @param typeFiles - 'dailys' or 'weeklys'
 * @returns [data] - array of objects 
 * 
 */
export async function fillWeeksOrDaysTable(data=[], weekOrDayRange=[], year, typeFiles){
    let dataResult = []
    const dailys = typeFiles === 'dailys'
    const dataFound = {
        actionButtonText: 'Import File Now',
        importData: {
            importStatus: false,
            createdAt: null,
            date: null,
            key: null
        }
    }
    for(let weekOrDay of weekOrDayRange) {
        let scorecardData = dataFound
        let podData = dataFound
        let cxFeedbackData = dataFound
        let mentorData = dataFound
        let netradyneData = dataFound
        let eocData = dataFound
        for(let item of data){
            let itemWeekOrDate = item.week
            if(dailys) itemWeekOrDate = item.date
            if(itemWeekOrDate === weekOrDay){
                const typeFile = {
                    scorecard: () => {
                        scorecardData = returnWeeklyData(item)
                    },
                    podQualityFeedback: () => {
                        podData = returnWeeklyData(item)
                    },
                    customerFeedback: () => {
                        cxFeedbackData = returnWeeklyData(item)
                    },
                    mentor: () => {
                        mentorData = returnDailyData(item)
                    },
                    netradyne: () => {
                        netradyneData = returnDailyData(item)
                    },
                    eoc: () => {
                        eocData = returnDailyData(item)
                    }
                }
                if(!!typeFile[item.type]) typeFile[item.type]()
            }
        }
        let input = {}
        if(dailys) {
            input = {
                weekOrDay: moment(weekOrDay, 'YYYY-MM-DD').format('MMM D, YYYY'),
                mentor: mentorData.importData,
                netradyne: netradyneData.importData,
                eoc: eocData.importData,                
            }
        }else{
            //typeFiles is a 'weeklys'
            input = {
                weekOrDay: `Week ${weekOrDay} ${year}`,
                scorecard: scorecardData.importData,
                pod: podData.importData,
                cfeedback: cxFeedbackData.importData  
            }
        }
        dataResult.push(input)
    }
    return dataResult
}