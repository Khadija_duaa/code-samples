import { Storage } from 'aws-amplify'
import { encrypt, decrypt } from './customEncryptor'
import moment from 'moment'

export const getS3FileName = function(s3Key){
    return s3Key.substr( s3Key.lastIndexOf('/') + 1)
}

export const generateToken = function(s3Key, params){

    try {

        const obj = {
            k: s3Key,
            e: undefined
        }
    
        if(params && (params.expires || params.Expires)){
            const expires = (params.expires || params.Expires)
            if(expires instanceof Date){
                obj.e = expires.getTime()
            }else{
                const expiration = new Date()
                expiration.setSeconds(expiration.getSeconds() + parseInt(expires))
                obj.e = expiration - 0
            }
        }
    
        const textString = JSON.stringify(obj)
        return encrypt(textString, process.env.VUE_APP_ENCRYPTION_KEY)

    } catch (error) {
        console.log({error})
        throw error
    }

}

export const getShortLink = function(s3Key, params){
    const token = generateToken(s3Key, params)
    return window.location.origin + '/s3/' + token
}

export const downloadS3BlobFile = function(s3Key, options = {}){
    options.download = true
    Storage.get(s3Key, options).then((blob)=>{
        const a = document.createElement("a")
        a.href = window.URL.createObjectURL(blob.Body)
        a.download = getS3FileName(s3Key)
        a.click()
        a.remove()
    })
}

export const getFileByToken = async function(token, params){

    let decryptedObj;

    try {
        decryptedObj = decrypt(token, process.env.VUE_APP_ENCRYPTION_KEY)
    } catch (error) {
        throw {errors: [{message: "Invalid token."}]}
    }

    const obj = JSON.parse(decryptedObj)
    let expirationDate;

    if(obj.e){
        expirationDate = new Date(obj.e)
        if(obj.e < new Date()){
            const message = `The token expired on ${moment(expirationDate).format("YYYY-MM-DD HH:mm:ss")}`
            throw {
                token,
                expirationDate,
                errors: [{message}],
                download: () => console.log(message)
            }
        }
    }

    try {
        
        const s3Key = obj.k
        const url = await Storage.get(s3Key, params)
        
        return {
            s3Key,
            expirationDate,
            token,
            URL: url,
            fileName: getS3FileName(s3Key),
            download: () => downloadS3BlobFile(s3Key, params)
        }

    } catch (error) {
        throw {errors: [error]}
    }

}


export default {getS3FileName, downloadS3BlobFile, generateToken, getFileByToken, getShortLink}