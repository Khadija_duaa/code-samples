import moment from 'moment-timezone';

/**
 * Parses a datetime string representation to date with format MM/DD/YYYY
 * @param {String} datetime ISO format datetime
 * @param {String} timezone String representing a timezone to parse the datetime
 * ie. America/Lima
 * @returns {String} date with format 'MONTH/DAY/YEAR'
 */
export function parseDatetimeToDate(datetime, timezone) {
  if (!datetime || isNaN(Date.parse(datetime))) return '—';

  const isValidTimezone = !!moment.tz.zone(timezone);
  if (isValidTimezone) {
    return moment.tz(datetime, timezone).format("MM/DD/YYYY");
  }

  return moment(datetime).format("MM/DD/YYYY");
};

/**
 * Parses a datetime string representation to time with format hh:mm AM/PM
 * @param {String} datetime ISO format datetime
 * @param {String} timezone String representing a timezone to parse the datetime
 * ie. America/Lima
 * @returns {String} time with format 'HOUR:MINUTES PM|AM'
 */
export function parseDatetimeToTimeAmPm(datetime, timezone) {
  if (!datetime || isNaN(Date.parse(datetime))) return '--:--';

  const time = datetime.split('T')[1];
  if (!time) return '--:--';

  let [hh, mm] = time.split('.')[0].split(':');

  const isValidTimezone = !!moment.tz.zone(timezone);
  if (isValidTimezone) {
    [hh, mm] = moment.tz(datetime, timezone).format("HH:mm").split(':');
  }

  if (isNaN(Number(hh)) || isNaN(Number(mm))) return '--:--';

  const abbrev = (hh > 12)? 'PM': 'AM';

  if (hh > 12) hh -= 12;

  hh = `${hh}`.padStart(2, '0');
  mm = `${mm}`.padStart(2, '0');

  return `${hh}:${mm} ${abbrev}`;
};
