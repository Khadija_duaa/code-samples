import { 
	createOptionsCustomListsStaff,
	deleteOptionsCustomListsStaff,
	createOptionsCustomLists,
	updateStaff
}from '@/graphql/mutations'
import { mapState, mapActions } from 'vuex'

export default {

	data() {
		return {
      vehiclesToDelete: []
		}
	},
	created() {
	},
	computed: {
		...mapState([
			'userInfo'
		]),
	},
	methods: {
		...mapActions([
			'setCustomListOption'
		]),

		/**
		 * set authorized to drive to an associate
		 * @param {Object} staff objects with items of id of vehicle type
		 * @returns {Object} new authorize to drive body
		 */
		setAuthorizeToDriveItems(staff) {
			return staff.authorizedToDrive.items.map(item => {
				return {
					id: item.optionCustomList.id,
					optionsCustomListsStaff: item.id,
					staffId: item.staff.id,
					option: item.optionCustomList.option,
					group: item.group
				}
			})
		},

		/**
		 * add authorized to drive to an associate
		 * @param {Object} items objects with items of id of vehicle type
		 * @param {ID} staffId of staff
		 * @returns 
		 */
		async addAuthorizeToDrive(items, staffId) {
			let newList = []
			if (!items || !items.length) return newList

			await Promise.all(items.map(async item => {
				try {
					const createInput = {
						group: this.userInfo.tenant.group,
						optionsCustomListsStaffStaffId: staffId,
						optionsCustomListsStaffOptionCustomListId: item,
					}
					let result = await this.api(createOptionsCustomListsStaff, { input: createInput })
					result = result.data.createOptionsCustomListsStaff
					const finalItem = {
						createdAt: result.createdAt,
						group: result.group,
						id: result.id,
					}
					newList.push(finalItem)
				} catch (e) {
					throw e
				}
			}));
			return newList
		},

		/**
		 * delete authorized to drive to an associate
		 * @param {Object} items objects with items of id of options custom list
		 * @returns 
		 */
		async deleteAuthorizeToDrive(items) {

			await Promise.all(items.map(async item => {
				try {
					const deleteInput = {
						id: item.optionsCustomListsStaff,
					}
					await this.api(deleteOptionsCustomListsStaff, { input: deleteInput })
				} catch (e) {
					throw e
				}
			}));
		},

		/**
		 * Validate Vehicle can still kept their default associates
		 * @params vehicle, newVehicleTypeId, calledFrom data
		 * @returns 
		*/
		async validateAssociateList(input) {

			const {
				vehicle, 
				initVehicleData, 
				newVehicleTypeId,
				calledFrom, 
			} = input

			const defaultAssociates = []
			const defaultAssociateList = [
					vehicle.defaultStaff,
					vehicle.defaultStaff2,
					vehicle.defaultStaff3
			]

			defaultAssociateList.map( defaultStaff => {
				if(typeof(defaultStaff?.items)!=='undefined'){
					defaultStaff.items.map( associate => {
							const authorizedToDrive = associate.authorizedToDrive.items.map(item => item.optionCustomList.id)
							if(!authorizedToDrive.includes(newVehicleTypeId)) {
									defaultAssociates.push(associate)
							}
					})
				}
			})
			
			if(!defaultAssociates.length) return true

			const h = this.$createElement;
			const text = `This Vehicle has been assigned to ${defaultAssociates.length} ${this.pluralize(defaultAssociates.length, 'Associate', 'Associates')} 
					who ${this.pluralize(defaultAssociates.length, 'is', 'are')} not authorized to drive the ${calledFrom == 'bulkUpdates' ? 'type of Vehicle' : 'types of Vehicles'} selected:`

			return await this.$msgbox({
					title: 'Message',
					message: h('div', null, [
							h('p', null, text),
							h('ul', {class: 'vehicleMessage'}, defaultAssociates.map( associate => h('li', null, `${associate.firstName} ${associate.lastName}`))),
							h('p', null, `They will be unassigned if you continue.`), 
							h('p', null, `Click Cancel to undo your change to this vehicle.`),    
					]),
					showCancelButton: true,
					confirmButtonText: 'Confirm',
					cancelButtonText: 'Cancel',
			})
			.then(() => {
				if(calledFrom == 'vehicleDetail') {
					this.$emit('vehicles-to-delete-from-associates', defaultAssociates)
				} else if (calledFrom == 'bulkUpdates') {
					this.vehiclesToDelete = defaultAssociates
				}	
				return true
			}).catch(() => {
				vehicle.vehicleType.id = initVehicleData.vehicleType.id
				this.vehiclesToDelete = []
				return false
			}).finally(() => {
				this.$refs.vehicleTypeDropdown.$refs.select.blur();
			})
		},

    async removeDefaultVehiclesFromAssociates(vehiclesToDeleteFromAssociates, vehicleId) {

      if(!vehiclesToDeleteFromAssociates.length) return

      await Promise.all(vehiclesToDeleteFromAssociates.map(async (associate) => { 
        await Promise.all(['', 2, 3].map(vehicleNumber => this.removeDefaultVehicleFromAssociate(associate, vehicleId, vehicleNumber)));
      }));
    },

    async removeDefaultVehicleFromAssociate(associate, vehicleId, vehicleNumber) {
      const defaultVehicleProp = `defaultVehicle${vehicleNumber}`
      if (associate[defaultVehicleProp] && associate[defaultVehicleProp].id == vehicleId) {

        try {
					const input = {
						id: associate.id,
						[`staffDefaultVehicle${vehicleNumber}Id`]: null
					}
          await this.api(updateStaff, { input })
        } catch (e) { 
          console.log(e)
          this.displayUserError(e)
        }
      }
    },

		/**
		 * create vehicle type to Option Custom List
		 * @param {Object} input object containing staff, vehicles, authorizedToDrive
		 * @returns 
		 */
		async validateDefaultVehicles(removedId, input, { removedAuthorizedToDrive } = {}){
			const { 
				staff,
        vehicles,
        authorizedToDrive
			} = input

			const unauthorizedVehicleList = this.validDefaultVehicles(staff, vehicles, authorizedToDrive)

			if(!unauthorizedVehicleList.length) return { removedAuthorizedToDrive, updateDefaultVehicles: false }

			const h = this.$createElement;
			const text = `The following default ${this.pluralize(unauthorizedVehicleList.length, 'Vehicle', 'Vehicles')} 
							for this ${this.$t('label.associate')} ${this.pluralize(unauthorizedVehicleList.length, 'is', 'are')} 
							no longer authorized to drive, so will now be removed:`
			return await this.$msgbox({
				title: 'Message',
				message: h('div', null, [
					h('p', null, text),
					h('ul', {class: 'vehicleMessage'}, unauthorizedVehicleList.map((vehicle)=> h('li', null, `${vehicle.name}` ))),
					h('p', null, `We recommend choosing a new default Vehicle for this Associate.`), 
				]),
				showCancelButton: true,
				confirmButtonText: 'Confirm',
				cancelButtonText: 'Cancel',
			})
			.then(() => {
				unauthorizedVehicleList.map( vehicle => {
					if(staff.defaultVehicle.id == vehicle.id) staff.defaultVehicle = { id: null, name: null }
					if(staff.defaultVehicle2.id == vehicle.id) staff.defaultVehicle2 = { id: null, name: null }
					if(staff.defaultVehicle3.id == vehicle.id) staff.defaultVehicle3 = { id: null, name: null }
				})
				return { removedAuthorizedToDrive: true, updateDefaultVehicles: true }
			}).catch(() => {
				staff.authorizedToDrive.items = [...authorizedToDrive, removedId]
				return { removedAuthorizedToDrive: false, updateDefaultVehicles: false }
			})
		},

		validDefaultVehicles(staff, vehicles, list) {
			let unauthorizedVehicleList = []

			const defaultVehicles = [
				staff.defaultVehicle.id,
				staff.defaultVehicle2.id,
				staff.defaultVehicle3.id,
			]
			defaultVehicles.map(vehicleId => this.checkIfAuthorizeToDrive(vehicles, list, vehicleId, unauthorizedVehicleList) )
			return unauthorizedVehicleList
		},

		checkIfAuthorizeToDrive(vehicleList, authorizeToDriveList, vehicleId, list) {
			const vehicle = vehicleList.find(item => item.id == vehicleId)
			if(vehicle && vehicle.vehicleType && !authorizeToDriveList.includes(vehicle.vehicleType.id)) list.push(vehicle)
		},
	},
}
