import { API } from 'aws-amplify';

export default {
    methods: {
        async downloadTemplate(downloadParams) {
            try {
                let apiName = 's3downloadlink';
                let path = '/download-templates';
                let params = {
                    queryStringParameters: {
                      "bucket": downloadParams.bucket,
                      "key": downloadParams.key
                    }, 
                }
        
                const result = await API.get(apiName, path, params);
                const csvParse = this.$papa.parse(result.csvBody)
        
                this.$papa.download(csvParse.data, downloadParams.fileName) 
              } catch (e) {
                throw e
              } 
        }
    }
}