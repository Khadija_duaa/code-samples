import moment from 'moment'
import { $screen } from '@/utilities/breakpoints'
//Patterns to identify file type and get dates from filename - for Mentor, Netradyne, and EOC files.
export const fileTypes = [
    {
        name: $screen.xs || $screen.sm ? "Custom File" : 'Custom Spreadsheet',
        shortName: "customCsv",
        storage: "customCsvData",
        resultTitle: "Daily Roster Associate Assignments",
        domClass: "custom-csv-style",
        fileNamePatterns: [
            ".?(csv|xls|xlsx)",
        ]
    },
]

const checkByExtensions = function(fileName){
    //Confirm a csv file was selected
    const fileMatch = fileName.match(/(csv|xls|xlsx)/)
    if(!fileMatch){
        return
    }
    return true
}

const matchByPatterns = function(fileName){
    return fileTypes.find(p =>{
        return p['fileNamePatterns'].find(regexp => {
            return new RegExp(regexp,"ig").test(fileName)
        })
    })
}

export const getDetailsFromFileName = function(fileName, formattedDate){

    if(!checkByExtensions(fileName)){
        return
    }

    const match = matchByPatterns(fileName)
    if(!match){
        return
    }

    const results = {
        type: match
    }

    if(formattedDate){
        const matchDate = /(?<day>[0-9]{2})\/(?<month>[0-9]{2})\/(?<year>[0-9]{4})/g.exec(formattedDate)
        let {groups: {day, month, year}} = matchDate
        month = parseInt(month) ? month : moment().month(month).format("M")
        let date = new Date(year, month - 1, day)
        results.year = date.getFullYear()
        results.week = date.getWeekNumber()
        if(/.*\.xls(x)?$/gi.test(fileName)){
            results.dateString = `Custom Excel File for ${formattedDate}`
        }else{
            results.dateString = `Custom CSV File for ${formattedDate}`
        } 
    }

    return results
}

export default { getDetailsFromFileName, fileTypes }
