export default [
  {
    "id": "dabb762a-8c04-47ec-ac75-a451581ccbef",
    "group": "sample-group",
    "atFault": "Unknown",
    "accidentDate": "2023-03-25T05:00:00.000Z",
    "accidentType": null,
    "notes": "Sample Incident record",
    "staffId": "4ff4-974a",
    "vehicleId": null,
    "vehicleHistoryType": "Incident",
    "damage": null,
    "damageSeverity": null,
    "updatedAt": "2023-03-24T20:55:12.680Z",
    "staff": {
      "group": "sample-group",
      "transporterId": null,
      "firstName": "Peter",
      "lastName": "Mendez"
    },
    "verifiedBy": {
      "group": [
        "sample-group",
        "system_admin"
      ],
      "firstName": "Mark",
      "lastName": "Caspian"
    },
    "vehicle": null,
    "optionCustomList": {
      "option": "General Incident"
    }
  },
  {
    "id": "836b-b645",
    "group": "sample-group",
    "atFault": "",
    "accidentDate": "2023-03-21T05:00:00.000Z",
    "accidentType": null,
    "notes": "Sample Accident record",
    "staffId": "5ddd-4027",
    "vehicleId": "b1bc-482e",
    "vehicleHistoryType": "Incident",
    "damage": null,
    "damageSeverity": null,
    "updatedAt": "2023-03-22T23:56:03.882Z",
    "staff": {
      "group": "sample-group",
      "transporterId": null,
      "firstName": "Andre",
      "lastName": "TersTegen"
    },
    "verifiedBy": {
      "group": [
          "sample-group",
        "system_admin"
      ],
      "firstName": "Roger",
      "lastName": "Martin"
    },
    "vehicle": null,
    "optionCustomList": {
      "option": "Accident"
    }
  },
  {
    "id": "c0d9-4a97",
    "group": "sample-group",
    "atFault": "No",
    "accidentDate": "2023-02-05T05:00:00.000Z",
    "accidentType": null,
    "notes": "There is a witness that states ...",
    "staffId": "4ff4-974a",
    "vehicleId": "b1bc-482e",
    "vehicleHistoryType": "Incident",
    "damage": null,
    "damageSeverity": null,
    "updatedAt": "2023-03-22T23:55:17.594Z",
    "staff": {
      "group": "sample-group",
      "transporterId": null,
      "firstName": "Michelle",
      "lastName": "Garcia"
    },
    "verifiedBy": {
      "group": [
          "sample-group",
        "system_admin"
      ],
      "firstName": "Mark",
      "lastName": "Caspian"
    },
    "vehicle": {
      "group": "sample-group",
      "name": "Lamborghini - Murcielago",
      "vehicle": null
    },
    "optionCustomList": {
      "option": "General Incident"
    }
  },
  {
  "id": "4b86-8a5b",
    "group": "sample-group",
    "atFault": "",
    "accidentDate": "2022-10-22T05:00:00.000Z",
    "accidentType": null,
    "notes": "We are still investigating the causes ...",
    "staffId": "4d6c-86ef",
    "vehicleId": null,
    "vehicleHistoryType": "Incident",
    "damage": null,
    "damageSeverity": null,
    "updatedAt": "2023-03-22T23:55:26.484Z",
    "staff": {
      "group": "sample-group",
      "transporterId": null,
      "firstName": "Mark",
      "lastName": "Ruffalo"
    },
    "verifiedBy": {
      "group": [
          "sample-group",
        "system_admin"
      ],
      "firstName": "Mark",
      "lastName": "Caspian"
    },
    "vehicle": null,
    "optionCustomList": {
      "option": "General Incident"
    }
  },
  {
    "id": "7b0f-8b26",
    "group": "sample-group",
    "atFault": "Yes",
    "accidentDate": "2022-03-23T05:00:00.000Z",
    "accidentType": null,
    "notes": "",
    "staffId": "4d6c-86ef",
    "vehicleId": null,
    "vehicleHistoryType": "Incident",
    "damage": null,
    "damageSeverity": null,
    "updatedAt": "2023-03-22T23:55:37.121Z",
    "staff": {
      "group": "sample-group",
      "transporterId": null,
      "firstName": "Said",
      "lastName": "Murrugarra"
    },
    "verifiedBy": {
      "group": [
          "sample-group",
        "system_admin"
      ],
      "firstName": "Mark",
      "lastName": "Caspian"
    },
    "vehicle": null,
    "optionCustomList": {
      "option": "Accident"
    }
  },
  {
    "id": "4c59-963a",
    "group": "sample-group",
    "atFault": "Unknown",
    "accidentDate": "2023-03-24T05:00:00.000Z",
    "accidentType": null,
    "notes": "This was a weird experience",
    "staffId": "5ddd-4027",
    "vehicleId": "47bc-a62c",
    "vehicleHistoryType": "Accident",
    "damage": null,
    "damageSeverity": null,
    "updatedAt": "2023-03-28T22:55:15.686Z",
    "staff": {
      "group": "sample-group",
      "transporterId": null,
      "firstName": "Christina",
      "lastName": "Flores"
    },
    "verifiedBy": {
      "group": [
          "sample-group",
        "system_admin"
      ],
      "firstName": "Mark",
      "lastName": "Caspian"
    },
    "vehicle": {
      "group": "sample-group",
      "name": "Toyota - Tercel",
    },
    "optionCustomList": {
      "option": "Accident"
    }
  },
  {
    "id": "2b25-4d03-adcc",
    "group": "sample-group",
    "atFault": "Unknown",
    "accidentDate": "2023-03-19T05:00:00.000Z",
    "accidentType": null,
    "notes": "Accident sample",
    "staffId": "4d6c-86ef",
    "vehicleId": "b1bc-482e",
    "vehicleHistoryType": "Accident",
    "damage": null,
    "damageSeverity": null,
    "updatedAt": "2023-03-28T22:56:22.039Z",
    "staff": {
      "group": "sample-group",
      "transporterId": null,
      "firstName": "Erik",
      "lastName": "Gonzales"
    },
    "verifiedBy": {
      "group": [
          "sample-group",
        "system_admin"
      ],
      "firstName": "Mark",
      "lastName": "Caspian"
    },
    "vehicle": {
      "group": "sample-group",
      "name": "Ferrari - Roma",
    },
    "optionCustomList": {
      "option": "Accident"
    }
  },
  {
    "id": "e588-9445",
    "group": "sample-group",
    "atFault": "No",
    "accidentDate": "2023-03-14T05:00:00.000Z",
    "accidentType": null,
    "notes": "Sample Accident record",
    "staffId": "4d6c-86ef",
    "vehicleId": "b1bc-482e",
    "vehicleHistoryType": "Accident",
    "damage": null,
    "damageSeverity": null,
    "updatedAt": "2023-03-22T23:55:07.598Z",
    "staff": {
      "group": "sample-group",
      "transporterId": null,
      "firstName": "Karen",
      "lastName": "Burdeau"
    },
    "verifiedBy": {
      "group": [
          "sample-group",
        "system_admin"
      ],
      "firstName": "Mark",
      "lastName": "Caspian"
    },
    "vehicle": {
      "group": "sample-group",
      "name": "BMW - Z4",
    },
    "optionCustomList": {
      "option": "Accident"
    }
  },
  {
    "id": "c999-4200-8b71",
    "group": "sample-group",
    "atFault": "",
    "accidentDate": "2023-02-23T05:00:00.000Z",
    "accidentType": null,
    "notes": "Regitered Accident",
    "staffId": "4ff4-974a",
    "vehicleId": "b1bc-482e",
    "vehicleHistoryType": "Accident",
    "damage": null,
    "damageSeverity": null,
    "updatedAt": "2023-03-28T22:56:49.204Z",
    "staff": {
      "group": "sample-group",
      "transporterId": null,
      "firstName": "Claude",
      "lastName": "Flaubert"
    },
    "verifiedBy": {
      "group": [
        "sample-group",
        "system_admin"
      ],
      "firstName": "Mark",
      "lastName": "Caspian"
    },
    "vehicle": {
      "group": "sample-group",
      "name": "Chevrolet - Camaro",
    },
    "optionCustomList": {
      "option": "Accident"
    }
  },
  {
    "id": "47c9-9934",
    "group": "sample-group",
    "atFault": "Yes",
    "accidentDate": "2023-01-20T05:00:00.000Z",
    "accidentType": null,
    "notes": "Margaritte had an Accident and crashed her car",
    "staffId": "4ff4-974a",
    "vehicleId": "47bc-a62c",
    "vehicleHistoryType": "Accident",
    "damage": null,
    "damageSeverity": null,
    "updatedAt": "2023-03-22T23:58:19.732Z",
    "staff": {
      "group": "sample-group",
      "transporterId": null,
      "firstName": "Margaritte",
      "lastName": "Gautier"
    },
    "verifiedBy": {
      "group": [
          "sample-group",
        "system_admin"
      ],
      "firstName": "Mark",
      "lastName": "Caspian"
    },
    "vehicle": {
      "group": "sample-group",
      "name": "Mazda - RX8",
    },
    "optionCustomList": {
      "option": "Accident"
    }
  }
];
