export const updateStaffOnboardingNotes = /* GraphQL */ `
  mutation updateStaff(
    $input: UpdateStaffInput!
    $condition: ModelStaffConditionInput
  ) {
    updateStaff(input: $input, condition: $condition) {
      id
      onboardingNotes
      updatedAt
    }
  }
`;