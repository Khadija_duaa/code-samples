export default {
    data() {
        return {
            associateIsOpen: false,
            associateLoading: false,
            associateFormTitle: 'Add Associate'
        };
    },
    methods: {
        associateOpenCreate() {
            this.associateIsOpen = true;
            this.associateLoading = true;
            this.associateFormTitle = 'Add Associate';
        },

        associateCancelSave(another,id) {
            if(another === 'create' || another === 'redirect' || !another){
                this.associateIsOpen = false
                
                if(another === 'create') {
                    this.loadData()
                }
                
                if(another === 'redirect'){
                    this.$router.push({name: 'StaffDetail', params: { id }})
                }
                return
            }
            
            this.associateIsOpen = true
        }
    }
};