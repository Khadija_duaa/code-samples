import { createStaff } from '@/graphql/mutations'
import { cleanPhoneNumber } from '@/utilities/phoneNumberFormat'
import {mapState} from "vuex"
import vehicleType from '@/mixins/vehicleType'

export default {
  mixins: [ vehicleType ],
  data() {
    return {
      addStaffModalOpen: false,
      formFields:{
        status: '',
        firstName: '',
        lastName: '',
        email: '',
        phone: '',
        transporterId: '',
        dlExpiration: '',
        hireDate: '',
        authorizedToDrive: {}
      },
      loading: false,
      addStaffModalOpen: false,
      associateIsOpen: false,
      associateLoading: false,
      associateFormTitle: 'Add Associate'
    }
  },
  computed: {
    ...mapState(['userInfo']),
  },
  methods: {

    /**
     * Main function to validate, create and return object
     * @param {String} addAnother string to create another staff in the same form
     * @returns {Object} json of the new staff
     */
    async validateForm(addAnother, isValidated = true){
      try{

        if(isValidated) await this.$refs.addDaForm.$refs.form.validate()
        

        // Validate list of Associate's Authorize To Drive
        const isValid = await this.validateAuthorizeToDriveItems(this.formFields.authorizedToDrive.items)
        if((!isValid.isEmpty && !isValid.response) || isValid.isCloseViewCoincidence) return
        return await this.createStaff(addAnother, isValidated)

      }catch(e){
        console.log(e)
        this.displayUserError("Please fill out all required fields and correct any errors.")
      }
    },

    /**
     * Create a new record of Staff and authorized to drive
     * function part of validateForm
     * @param {String} addAnother string to create another staff in the same form
     * @returns {Object} json of the new staff
     */
    async createStaff(addAnother, isValidated = true, isCloseViewCoincidence = false){
      try{
        this.addStaffModalOpen = false
        this.loading = true
        var input = {
          ...this.formFields,
          receiveTextMessages: true,
          receiveEmailMessages: true,
          group: this.userInfo.tenant.group
        }

        if(!input.transporterId) delete input.transporterId
        if(!input.email) delete input.email
        input.phone = cleanPhoneNumber(input.phone, 'US')
        if(input.phone ==='') delete input.phone
        
        const authorizedToDrive = input.authorizedToDrive
        delete input.authorizedToDrive

        const result = await this.api(createStaff, { input })
        let newStaff = result.data.createStaff
        /**
         * Create AuthorizeToDrive for this Associate
         */
        newStaff.authorizedToDrive.items = await this.addAuthorizeToDrive(authorizedToDrive.items, newStaff.id)
        this.notification() 
        if(addAnother){
          // cleanup form to create another associate later
          if(isValidated) this.$refs.addDaForm.$refs.form.resetFields()
        }else{
          //option2: create only 1 associate and then, go to "associate detail page"
          const staffId = result.data.createStaff.id
          this.$router.push({name: 'StaffDetail', params:{ id: staffId } })
        }
        let resumeNewStaff = {
          id: newStaff.id,
          name: `${newStaff.firstName} ${newStaff.lastName}`,
          firstName: newStaff.firstName,
          lastName: newStaff.lastName,
          authorizedToDrive: newStaff.authorizedToDrive,
          defaultVehicle: newStaff.defaultVehicle,
          defaultVehicle2: newStaff.defaultVehicle2,
          defaultVehicle3: newStaff.defaultVehicle3,
          keyFocusArea: newStaff.keyFocusArea,
          keyFocusAreaCompleted: newStaff.keyFocusAreaCompleted,
          transporterId: newStaff.transporterId,
        }

        if(isCloseViewCoincidence) resumeNewStaff.isCloseViewCoincidence = true

        return resumeNewStaff
      }catch(e){
        if(e != 'cancel'){          
          console.log(e)
          this.displayUserError(e)
        }
      }
      finally{
        this.loading = false
      }
      
    },

    async validateAuthorizeToDriveItems(items){

      if(items && items.length) return { isEmpty: true, response: false }

      const text = 'This Associate is not yet authorized to drive any type of Vehicles. This means they will not be able to be linked to a Vehicle on the Daily Roster.<br><span class="font-bold"> We recommend updating the "Authorized to Drive" field before continuing.<span>'
      
      return await this.$confirm(text, 'Warning', {
          confirmButtonText: 'Continue Anyway',
          cancelButtonText: 'Go Back',
          type: 'warning',
          closeOnPressEscape: false,
          closeOnClickModal: false,
          showClose: false,
          showConfirmButton: true,
          closeOnHashChange: true,
          dangerouslyUseHTMLString: true
      })
      .then(() => {
        return { isEmpty: false, response: true, isCloseViewCoincidence: false }
      }).catch(() => {
        return { isEmpty: false, response: false, isCloseViewCoincidence: true  }
      })
    },

    /** notification code 200 success */
    notification(){
      this.displayUserNotification({title: 'Success', message: 'The new Associate was successfully created.', type: 'success'})
    },

    async closeDaForm(view) {
      this.$refs.addDaForm.$refs.form.resetFields()
      this.addStaffModalOpen = false
    },

    cleanFields(){
      this.formFields = {
        status: 'Active',
        firstName: '',
        lastName: '',
        email: '',
        phone: '',
        transporterId: '',
        dlExpiration: '',
        hireDate: '',
        authorizedToDrive: {}
      }
    },

    associateOpenCreate() {
      this.associateIsOpen = true;
      this.associateLoading = true;
      this.associateFormTitle = 'Add Associate';
    },

    associateCancelSave(another,id) {
        if(another === 'create' || another === 'redirect' || !another){
            this.associateIsOpen = false
            
            if(another === 'create') {
                this.loadData()
            }
            
            if(another === 'redirect'){
                this.$router.push({name: 'StaffDetail', params: { id }})
            }
            return
        }
        
        this.associateIsOpen = true
    }

  },
}