export const getInjury = /* GraphQL */ `
  query GetInjury($id: ID!) {
    getInjury(id: $id) {
      id
      group
      caseNumber
      injuryDate
      injuryTime
      injuryTimeIsUnknown
      timeStaffStartedWork
      completedBy
      completedByTitle
      completedByPhone
      driverHireDate
      driverDOB
      driverGender
      driverAddress
      driverCity
      driverState
      driverZip
      physicianName
      physicianFacility
      physicianAddress
      physicianCity
      physicianState
      physicianZip
      wasTreatedInER
      wasHospitalizedOvernight
      descriptionBeforeAccident
      descriptionInjury
      descriptionIncident
      descriptionDirectHarmCause
      injuryType
      notes
      dateOfDeath
      caseNumberFromLog
      staffId
      documents {
        items {
          id
          group
          name
          key
          type
          uploadDate
        }
      }
      createdAt
      updatedAt
      staff {
        id
        group
        transporterId
        firstName
        lastName
      }
      completedByUser {
        id
        group
        cognitoSub
        firstName
        lastName
        role
        createdAt
        updatedAt
        injuriesCompletedBy {
          nextToken
        }
      }
    }
  }
`;