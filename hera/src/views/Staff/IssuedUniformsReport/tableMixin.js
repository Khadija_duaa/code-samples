import momentTz from 'moment-timezone';
import { sortByStringProp } from '@/utilities/sortHelpers';
import { exportCSV } from '@/utilities/exportCSV';

function sortUniforms(uniforms) {
  return uniforms.sort((u1, u2) => {
    if (!u1.returnedDate && !u2.returnedDate) return u1.uniformType?.value.localeCompare(u2.uniformType?.value);
    if (!u1.returnedDate) return -1;
    if (!u2.returnedDate) return 1;
    return (
      u2.returnedDate.localeCompare(u1.returnedDate) ||
      u1.uniformType?.value.localeCompare(u2.uniformType?.value)
    );
  });
}

function mapUniformsAsListOfItems(uniforms, timezone) {
  return uniforms.map(u => {
    const issueDate = momentTz.tz(u.issueDate, timezone).format('MM/DD/YYYY');
    const returnedDate = momentTz.tz(u.returnedDate, timezone).format('MM/DD/YYYY');
    const text = `${u.uniformType?.value} (${u.size?.value}) x ${u.qty}, Issued ${issueDate}`;
    return (u.returnedDate)? `${text}, Returned ${returnedDate}`: text;
  });
}

function parseDataForExport(dataList, tableColumns) {
  return dataList
    .map(item => ({...item, listOfItems: item.listOfItems.join('')}))
    .map(item => tableColumns.reduce((exportItem, column) => {
      exportItem[column.name] = item[column.col];
      return exportItem;
    }, {}));
}

export default {
  data() {
    return {
      uniformTableCols: [
        { name: "First Name", col: "firstName", fixed: false, width: "170", sortable: true,  sortMethod: sortByStringProp('firstName')},
        { name: "Last Name", col: "lastName", fixed: false, width: "170", sortable: true,  sortMethod: sortByStringProp('lastName')},
        { name: "Associate Status", col: "status", fixed: false, width: "200", sortable: true,  sortMethod: sortByStringProp('status')},
        { name: "Items Issued", col: "itemsIssued", fixed: false, width: "150", sortable: true,  sortMethod: sortByStringProp('itemsIssued')},
        { name: "Items Returned", col: "itemsReturned", fixed: false, width: "180", sortable: true,  sortMethod: sortByStringProp('itemsReturned')},
        { name: "Items In Posession", col: "itemsInPosession", fixed: false, width: "200", sortable: true,  sortMethod: sortByStringProp('itemsInPosession')},
        { name: "List of Items", col: "listOfItems", fixed: false, width: "350", sortable: false,  sortMethod: undefined},
      ],
      uniformTableOptions: [
        { label: "Export as CSV", action: "exportCSV", divided:  false},
      ],
      uniformTableRowOptions: [
        { label: "View Associate", action: "viewAssociate", divided:  false},
      ],
    };
  },

  methods: {
    uniformTableMapCalculatedFields(data) {
      return data.map(item => {
        const itemsIssued = item.uniforms?.items || [];
        const itemReturned = itemsIssued.filter(u => !!u.returnedDate) || [];
        const sortedUniforms = sortUniforms(itemsIssued);
        return {
          ...item,
          itemsIssued: `${itemsIssued.length}`,
          itemsReturned: `${itemReturned.length}`,
          itemsInPosession: `${itemsIssued.length - itemReturned.length}`,
          listOfItems: mapUniformsAsListOfItems(sortedUniforms, this.getTenantTimeZone()),
        };
      });
    },

    uniformTableHandleCommand(command, payload) {
      const {action, row} = command;
      switch(action) {
        // TABLE ACTIONS
        case 'exportCSV':
          this.uniformTableHandleExportCSV(payload.dataList);
          break;
        // ROW ACTIONS
        case 'viewAssociate':
          this.uniformTableHandleViewAssociate(row.id);
          break;
      }
    },

    uniformTableHandleExportCSV(dataList) {
      const arrData = parseDataForExport(dataList, this.uniformTableCols);
      const fileName = `issued-uniforms-export-${this.$moment().format('MM/DD/YYYY')}.csv`
      exportCSV({ arrData, fileName });
    },

    uniformTableHandleViewAssociate(associateID) {
      this.$router.push({
        name: 'StaffDetail',
        params: {
          id: associateID,
        },
      });
    },
  },
};
