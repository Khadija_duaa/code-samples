export const updateStaffPreferences = /* GraphQL */ `
  mutation UpdateStaff(
    $input: UpdateStaffInput!
    $condition: ModelStaffConditionInput
  ) {
    updateStaff(input: $input, condition: $condition) {
      id
      group
      firstName
      lastName
      receiveTextMessages
      receiveEmailMessages
      alternateNames
      updatedAt
      messagePreferencesHistory {
        items {
          id
          messagePreferenceType
          description
          datetime
          messagePreferencesHistoryStaffId
        }
        nextToken
      }
    }
}
`;

export const updateStaffDlExpiration = /* GraphQL */ `
  mutation UpdateStaff(
    $input: UpdateStaffInput!
    $condition: ModelStaffConditionInput
  ) {
    updateStaff(input: $input, condition: $condition) {
      id
      group
      dlExpiration
    }
  }
`;

export const updateStaffExpirationVehicleReport = /* GraphQL */ `
  mutation UpdateStaff(
    $input: UpdateStaffInput!
    $condition: ModelStaffConditionInput
  ) {
    updateStaff(input: $input, condition: $condition) {
      id
      group
      vehicleReport
    }
  }
`;

export const updatePhysicalExpirationDate = /* GraphQL */ `
  mutation UpdatePhysical(
    $input: UpdatePhysicalInput!
    $condition: ModelPhysicalConditionInput
  ) {
    updatePhysical(input: $input, condition: $condition) {
      id
      group
      expirationDate
    }
  }
`;

export const createOptionsCustomLists = /* GraphQL */ `
  mutation CreateOptionsCustomLists(
    $input: CreateOptionsCustomListsInput!
    $condition: ModelOptionsCustomListsConditionInput
  ) {
    createOptionsCustomLists(input: $input, condition: $condition) {
      id
      group
      customLists {
        id
        group
        type
        listCategory
        listName
        listDisplay
        canDeleteAllOptions
        options {
          nextToken
        }
        createdAt
        updatedAt
      }
      order
      option
      default
      usedFor
      daysCount
      canBeEdited
      canBeDeleted
      canBeReorder
      accidents {
        items {
          id
          group
          atFault
          drugTestDate
          drugTestResult
          accidentDate
          accidentType
          address
          addressCity
          addressState
          addressZip
          fileName
          verifiedDate
          notes
          insuranceClaimNumber
          policeDepartment
          policeOfficerName
          policeReportNumber
          staffId
          vehicleId
          vehicleHistoryType
          services
          location
          maintenanceStatus
          maintenanceDateCompleted
          mileageAsOfMaintenance
          mileage
          time
          damage
          damageSeverity
          vehicleDamageDate
          odometerDate
          odometerType
          createdAt
          updatedAt
        }
        nextToken
      }
      associates {
        items {
          id
          group
          createdAt
          updatedAt
        }
        nextToken
      }
      replaceByRouteParkingSpace {
        items {
          id
          group
          notes
          createdAt
          routeNumber
          staging
          status
          helperStatus
          isNotActive
          standby
          time
          messageSentTime
          messageSentError
          receivedAnnouncement
          rescued
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
      routeParkingSpace {
        items {
          id
          group
          notes
          routeNumber
          staging
          previousStatus
          statusChangedTime
          status
          helperStatus
          standby
          time
          isNotActive
          messageSentTime
          messageSentError
          receivedAnnouncement
          rescued
          routeStaffId
          totalStops
          totalPackages
          completedStops
          completedPackages
          undeliveredPackagesTotal
          undeliveredPackagesBusinessClose
          undeliveredPackagesUnableToAccess
          undeliveredPackagesOtherAWS
          pickUpReturnPackage
          additionalPackagesFromRescue
          splitWithRosterAssignment
          additionalPackagesFromSplit
          firstStopTime
          lastStopTime
          daWorkStartTime
          daWorkEndTime
          daRouteStartTime
          daRouteEndTime
          rtsTime
          lunchStartTime
          lunchEndTime
          inspectionFueled
          inspectionCleaned
          inspectionFlashers
          inspectionCreditCard
          inspectionKeys
          inspectionDeviceCharger
          inspectionDeviceBattery
          inspectionNotes
          createdAt
          updatedAt
        }
        nextToken
        scannedCount
      }
      vehicles {
        items {
          id
          group
          name
          vehicle
          type
          state
          licensePlateExp
          dateStart
          dateEnd
          ownership
          provider
          otherProvider
          category
          vin
          make
          model
          year
          rentalAgreementNumber
          images
          rentalContracts
          accidentReports
          licensePlate
          mileage
          gasCard
          status
          company
          notes
          lastOdometerReadingDate
          tollPassId
          createdAt
          updatedAt
        }
        nextToken
      }
      parkingSpace {
        items {
          id
          group
          name
          vehicle
          type
          state
          licensePlateExp
          dateStart
          dateEnd
          ownership
          provider
          otherProvider
          category
          vin
          make
          model
          year
          rentalAgreementNumber
          images
          rentalContracts
          accidentReports
          licensePlate
          mileage
          gasCard
          status
          company
          notes
          lastOdometerReadingDate
          tollPassId
          createdAt
          updatedAt
        }
        nextToken
      }
    }
  }
`;

export const updateStaff = /* GraphQL */ `
  mutation UpdateStaff(
    $input: UpdateStaffInput!
    $condition: ModelStaffConditionInput
  ) {
    updateStaff(input: $input, condition: $condition) {
      id
      group
      firstName
      lastName
      status
      createdAt
      updatedAt
      staffStatusHistory {
        items {
          id
          group
          reason
          date
          previousStatus
          currentStatus
          createdAt
          updatedAt
        }
        nextToken
      }
    }
  }
`;

export const createMessagePreferencesHistory = /* GraphQL */ `
  mutation CreateMessagePreferencesHistory(
    $input: CreateMessagePreferencesHistoryInput!
    $condition: ModelMessagePreferencesHistoryConditionInput
  ) {
    createMessagePreferencesHistory(input: $input, condition: $condition) {
      id
      group
      messagePreferenceType
      description
      datetime
      messagePreferencesHistoryStaffId
      createdAt
      updatedAt
      staff {
        id
        firstName
        lastName
      }
    }
  }
`;