export const staffsByGroup = /* GraphQL */ `
query StaffsByGroup(
  $group: String
  $firstName: ModelStringKeyConditionInput
  $sortDirection: ModelSortDirection
  $filter: ModelStaffFilterInput
  $limit: Int
  $nextToken: String
) {
  staffsByGroup(
    group: $group
    firstName: $firstName
    sortDirection: $sortDirection
    filter: $filter
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      group
      vehicleType
      transporterId
      firstName
      lastName
      keyFocusArea
      dob
      gender
      hireDate
      phone
      status
      createdAt
      updatedAt
      email
      dlExpiration
      vehicleReport
      gasCardPin
      hireDate
      updatedAt
      authorizedToDrive {
        items {
          id
          group
          staff {
            id
          }
          optionCustomList {
            id
            option
          }
        }
        nextToken
      }
      labels {
        items {
          id
          labelId
          label {
            id
            name
            status
          }
          createdAt
          updatedAt
        }
      }
      defaultVehicle{
        id
        name
      }
      defaultVehicle2 {
        id
        name
      }
      defaultVehicle3 {
        id
        name
      }
      onBoarding {
        items {
          id
          group
          name
          isComplete
          status
          dateComplete
          dateStart
          createdAt
          updatedAt
        }
        nextToken
      }
      pronouns
      isALead
      assignedLead
      hourlyStatus
      hireDate
      finalCheckIssueDate
    }
    nextToken
  }
}
`;