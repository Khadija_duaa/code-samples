export const staffsByGroup = /* GraphQL */ `
query StaffsByGroup(
  $group: String
  $firstName: ModelStringKeyConditionInput
  $sortDirection: ModelSortDirection
  $filter: ModelStaffFilterInput
  $limit: Int
  $nextToken: String
) {
  staffsByGroup(
    group: $group
    firstName: $firstName
    sortDirection: $sortDirection
    filter: $filter
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      group
      firstName
      lastName
      status
      updatedAt
      authorizedToDrive {
        items {
          id
          group
          staff {
            id
          }
          optionCustomList {
            id
            option
          }
        }
        nextToken
      }
      defaultVehicle{
        id
        name
      }
      defaultVehicle2 {
        id
        name
      }
      defaultVehicle3 {
        id
        name
      }
      isALead
      assignedLead
    }
    nextToken
  }
}
`;
export const staffsUniformsByGroup = /* GraphQL */ `
  query StaffsByGroup(
    $group: String
    $firstName: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelStaffFilterInput
    $limit: Int
    $nextToken: String
  ) {
    staffsByGroup(
      group: $group
      firstName: $firstName
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        firstName
        lastName
        status
        uniforms {
          items {
            id
            qty
            issueDate
            returnedDate
            size {
              id
              value
            }
            uniformType {
              id
              value
            }
          }
        }
      }
    }
  }
`;
