import { mapState } from 'vuex'
import { updateStaff } from '@/graphql/mutations'
import addDaFormMixin from './components/AddDaForm/AddDaFormMixin'
export default {

  mixins: [addDaFormMixin],
  data() {
    return {

    }
  },
  created() {
  },
  computed: {
    ...mapState([
      'userInfo'
    ]),

    ...mapState('subscriptionStore', [
      'associateList',
  ]),
  },
  methods: {
    async updateRowValue(item){

      const { row, key, vehicleList, tableData } = item

      let field = key
      let value = row[key]
      /**
       * Check if User is updating a default vehicle
       */
      if (key.toLowerCase().includes('default')) {
        field = `staff${this.transformKey(key)}Id`
        value = row[key].id
        const vehicleName = vehicleList.find(vehicle => vehicle.id == value)
        row[key].name = vehicleName ? vehicleName.name : null
      }

      /**
       * Update Staff
       */
      try {
        const input = {
          id: row.id,
          [`${field}`]: value ? value : null
        }

        await this.api(updateStaff, { input });

        // Transform Assigned Lead into a matched Staff
        if(key == 'assignedLead') {
          const matchedStaff = tableData.find(item => item.id == value)
          row[key] = matchedStaff ? `${matchedStaff.firstName} ${matchedStaff.lastName}` : null
        }

        const message = this.keyMessageValues.find(item => {
          if (item.key == field) return item.message
        }).message

        this.displayUserNotification({
          message: `${row.firstName} ${row.lastName} ${message}`,
          type: "success",
          title: `${this.$t('label.associate')} Updated`
        })
      } catch (e) {
        throw e
      }
    },

    async removeAuthorizeToDriveItem(calledFrom, staff, vehicleList, removedId, items) {
      try {
        /**
         *  Validate list of Associate's Authorized To Drive has at least 1 item
         * */
        let response = {
          removedAuthorizedToDrive: false,
          updateDefaultVehicles: false
        }
      
        const removedAllAuthorizeToDriveItems = await this.validateAuthorizeToDriveItems(items)

        if(removedAllAuthorizeToDriveItems.isEmpty){
          response.removedAuthorizedToDrive = calledFrom == 'remove-tag'? true : false
        } else if (!removedAllAuthorizeToDriveItems.response) {
          items.push(removedId)
          response.removedAuthorizedToDrive = false
          return response
        } else {
          response.removedAuthorizedToDrive = true
        }

        /**
         *  Validate if Associate is still authorized to drive their default vehicles
        */
        const input = {
          staff,
          vehicles: vehicleList,
          authorizedToDrive: items,
        }
        return await this.validateDefaultVehicles(removedId, input, response)
      } catch (e) {
        throw e
      }
    },

    async updateStaff(staff) {

      try {
        const input = {
          id: staff.id,
          staffDefaultVehicleId: staff.defaultVehicle.id,
          staffDefaultVehicle2Id: staff.defaultVehicle2.id,
          staffDefaultVehicle3Id: staff.defaultVehicle3.id
        }
        await this.api(updateStaff, { input })
      } catch(e){
        this.printUserError(e)
      }
    },

    transformKey(value){
      return value.split(' ').map(word => word.charAt(0).toUpperCase() + word.substr(1)).join(' ')
    },

    setAssignedLead(id){
        const associate = this.associateList.find(associate => associate.id == id)
        return !associate ? null : `${associate.firstName} ${associate.lastName}`
    }
  }
}
