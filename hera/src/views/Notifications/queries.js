export const notificationsByOwner = /* GraphQL */ `
  query NotificationsByOwner(
    $owner: String
    $createdAt: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelNotificationFilterInput
    $limit: Int
    $nextToken: String
  ) {
    notificationsByOwner(
      owner: $owner
      createdAt: $createdAt
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        owner
        title
        createdAt
        payload
        clickAction
        isRead
        releaseNotes
        description
      }
      nextToken
    }
  }
`;