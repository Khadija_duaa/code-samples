
export const staffsByGroup = /* GraphQL */ `
query StaffsByGroup(
  $group: String
  $firstName: ModelStringKeyConditionInput
  $sortDirection: ModelSortDirection
  $filter: ModelStaffFilterInput
  $limit: Int
  $nextToken: String
) {
  staffsByGroup(
    group: $group
    firstName: $firstName
    sortDirection: $sortDirection
    filter: $filter
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      group
      transporterId
      firstName
      lastName
      keyFocusArea
      dob
      gender
      hireDate
      phone
      status
      createdAt
      updatedAt
      email
      dlExpiration
      vehicleReport
      gasCardPin
      hireDate
      updatedAt
      scoreCards {
        items {
          overallTier
          year
          week
        }
      }
      defaultVehicle{
        id
        name
      }
      defaultVehicle2 {
        id
        name
      }
      defaultVehicle3 {
        id
        name
      }
      route {
        items {
          rescued
        }
      }
      routeRescuer {
        items {
          id
        }
      }
      onBoarding {
        items {
          id
          group
          name
          isComplete
          status
          dateComplete
          dateStart
          createdAt
          updatedAt
        }
        nextToken
      }
      pronouns
      isALead
      assignedLead
      hourlyStatus
      hireDate
      finalCheckIssueDate
      counselings {
        items {
          id
          signature
        }
      }
    }
    nextToken
  }
}
`;

export const vehiclesByGroup = /* GraphQL */ `
  query VehiclesByGroup(
    $group: String
    $name: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelVehicleFilterInput
    $limit: Int
    $nextToken: String
  ) {
    vehiclesByGroup(
      group: $group
      name: $name
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        name
        status
        mileage
        vin
        lastOdometerReadingDate
        licensePlate
        licensePlateExp
        gasCard
        company
        make
        model
        year
        state
        dateStart
        notes
        dateEnd
        ownership
        type
        rentalAgreementNumber
        tollPassId
        parkingSpace {
          id
          group
          order
          option
        }
        updatedAt
        device {
          id
          deviceName
        }
        device2 {
          id
          deviceName
        }
        updatedAt
        defaultStaff {
          nextToken       
        }
        defaultStaff2 {
          nextToken
        }
        defaultStaff3 {
          nextToken
        }
      }
      nextToken
    }
  }
`;

export const devicesByGroup = /* GraphQL */ `
  query DevicesByGroup(
    $group: String
    $deviceName: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelDeviceFilterInput
    $limit: Int
    $nextToken: String
  ) {
    devicesByGroup(
      group: $group
      deviceName: $deviceName
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        deviceName
        status
        route {
            items{
                id
            }
          nextToken
        }
        vehicle {
            items{
                id
            }
          nextToken
        }
      }
      nextToken
    }
  }
`;

export const getStaff = /* GraphQL */ `
  query GetStaff($id: ID!) {
    getStaff(id: $id) {
      id
      group
      transporterId
      firstName
      lastName
      alternateNames
      phone
      email
      coachingOpportunity
      status
      dlExpiration
      vehicleReport
      gasCardPin
      dob
      gender
      pronouns
      assignedLead
      isALead
      hourlyStatus
      hireDate
      terminationDate
      finalCheckIssueDate
      returnedUniform
      latestScorecard
      notes
      defaultVehicle {
        id
        group
        name
        status
        updatedAt
      }
      defaultVehicle2 {
        id
        group
        name
        status
        updatedAt
      }
      defaultVehicle3 {
        id
        group
        name
        status
      }
      photo
      onboardingPinned
      accidents {
        items {
          group
          id
          accidentDate
          accidentType
          drugTestDate
          drugTestResult
          updatedAt
          verifiedBy{
            id
            firstName
            lastName
          }
          staff{
            id
            firstName
            lastName
          }
          vehicle{
            id
            name
          }
        }
        nextToken
      }
      documents {
        items {
          id
          group
          name
          key
          type
          uploadDate
          notes
          documentDate
          createdAt
          updatedAt
        }
        nextToken
      }
      counselings{
        items{
          id
          date
          severity {
            id
            option
          }
          refusedToSign
          counselingNotes
          updatedAt
          staff{
            id
            firstName
            lastName
          }
          user{
            id
            firstName
            lastName
          }
        }
      }
      drugTests {
        items {
          group
          id
          date
          location
          results
          file
          fileName
          createdAt
          updatedAt
        }
        nextToken
      }
      injuries {
        items {
          group
          id
          injuryDate
          injuryType
          physicianName
          notes
          updatedAt
        }
        nextToken
      }
      infractions{
        items{
          id
          updatedAt
        }
        nextToken
      }
      messages{
        items{
          id
        }
        nextToken
      }
      onBoarding {
        items {
          id
          group
          name
          isComplete
          status
          dateComplete
          dateStart
          createdAt
          updatedAt
        }
        nextToken
      }
      physicals {
        items {
          id
          group
          date
          expirationDate
          file
          fileName
          createdAt
          updatedAt
        }
        nextToken
      }
      route{
        items{
          id
          rescued
          updatedAt
          dailyRoster{
            notesDate
          }
          rescuer{
            id
            firstName
            lastName
          }
          rescuers {
            items {
              id
              staff {
                firstName
                lastName
              }
            }
          }
        }
      }
      routeHelper{
        items{
          id
        }
        nextToken
      }
      routeRescuer{
        items{
          id
          dailyRoster{
            notesDate
          }
          staff{
            id
            firstName
            lastName
          }
        }
      }
      routeRescuerStaff{
        items{
          id
          staff{
            id
            firstName
            lastName
          }
          route {
            id
            dailyRoster{
              notesDate
            }
            staff {
              id
              firstName
              lastName
            }
          }
        }
      }
      uniforms {
        items {
          id
          group
          qty
          issueDate
          returnedDate
          createdAt
          updatedAt
          size{
            id
            value
          }
          uniformType{
            id
            value
          }
        }
        nextToken
      }
      scoreCards {
        items {
          id
          group
          matched
          matchedS
          week
          year
          name
          transporterId
          overallTier
          delivered
          keyFocusArea
          ficoScore
          seatbeltOffRate
          dcr
          dar
          swcPod
          swcCc
          swcSc
          swcAd
          dnrs
          podOpps
          ccOpps
          speedingEventRate
          distractionsRate
          followingDistanceRate
          signSignalViolationsRate
          createdAt
          updatedAt
        }
        nextToken
      }
      mentor {
        items {
          id
          group
          name
          date
          station
          trips
          miles
          time
          fico
          accel
          braking
          cornering
          speeding
          distraction
          seatbelt
          backUp
          sse
          mpg
          idling
          engineOff
          preDvcr
          postDvcr
          trainingAssigned
          trainingCompleted
          createdAt
          updatedAt
        }
        nextToken
      }
      cxFeedback {
        items {
          id
          group
          matched
          matchedS
          week
          year
          name
          transporterId
          positiveFeedback
          negativeFeedback
          deliveryWasGreat
          deliveryWasntGreat
          totalDeliveries
          respectfulOfProperty
          followedInstructions
          friendly
          aboveAndBeyond
          deliveredWithCare
          careForOthers
          mishandledPackage
          drivingUnsafely
          driverUnprofessional
          notDeliveredToPreferredLocation
          createdAt
          updatedAt
        }
        nextToken
      }
      staffStatusHistory {
        items {
          id
          group
          reason
          date
          previousStatus
          currentStatus
          createdAt
          updatedAt
        }
        nextToken
      }
      receiveTextMessages
      receiveEmailMessages
      podQualities {
        items {
          id
          group
          matched
          matchedS
          week
          year
          employeeName
          transporterId
          blurry
          explicit
          mailSlot
          noPackage
          other
          opportunities
          success
          bypass
          packageInHand
          notClearlyVisible
          packageTooClose
          personInPhoto
          photoTooDark
          takenFromCar
          grandTotal
          createdAt
          updatedAt
        }
        nextToken
      }
      preferredDaysOff
      createdAt
      updatedAt
    }
  }
`;

export const dailyRostersByGroupAndNotesDate = /* GraphQL */ `
  query DailyRostersByGroupAndNotesDate(
    $group: String
    $notesDate: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelDailyRosterFilterInput
    $limit: Int
    $nextToken: String
  ) {
    dailyRostersByGroupAndNotesDate(
      group: $group
      notesDate: $notesDate
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        notesDate
        generalNotes
        fleetNotes
        lastStandUpSentTime
        updatedAt
        attachment {
          id
          group
          s3Key
          expirationDate
          contentType
        }
        note {
          items {
            id
            group
            type
            text
            vehiclesTagged {
              items {
                id
                counter
                vehicle {
                  id
                }
              }
            }
            staffsTagged {
              items {
                id
                counter
                staff {
                  id
                }
              }
            }
          }
        }
        route {
          items {
            id
            group
            notes
            routeNumber
            status
            helperStatus
            parkingSpace
            staging
            standby
            sweeper
            time
            isNotActive
            messageSentTime
            messageSentError
            receivedAnnouncement
            createdAt
            updatedAt
            totalStops
            totalPackages
            completedStops
            completedPackages
            undeliveredPackagesTotal
            undeliveredPackagesBusinessClose
            undeliveredPackagesUnableToAccess
            undeliveredPackagesOtherAWS
            pickUpReturnPackage
            additionalPackagesFromRescue
            splitWithRosterAssignment
            additionalPackagesFromSplit
            firstStopTime
            lastStopTime
            daWorkStartTime
            daWorkEndTime
            daRouteStartTime
            daRouteEndTime
            rtsTime
            lunchStartTime
            lunchEndTime
            inspectionFueled
            inspectionCleaned
            inspectionFlashers
            inspectionCreditCard
            inspectionKeys
            inspectionDeviceCharger
            inspectionDeviceBattery
            inspectionNotes
            replaceByRoute (sortDirection: ASC){
              items{
                id
                group
                notes
                routeNumber
                status
                helperStatus
                parkingSpace
                staging
                standby
                time
                isNotActive
                messageSentTime
                messageSentError
                receivedAnnouncement
                updatedAt
                staff {
                  id
                  firstName
                  lastName
                  receiveTextMessages
                  receiveEmailMessages
                  phone
                  email
                  keyFocusArea
                  keyFocusAreaCompleted
                }
                document{
                  items{
                    id
                    name
                    key
                    type
                    route{
                      id
                    }
                  }
                }
                odometerReadings {
                  items {
                    id
                    group
                  }
                  nextToken
                }
                vehicle{
                  id
                  name
                  parkingSpace {
                    id
                    group
                    order
                    option
                  }
                  status
                  device {
                    id
                    deviceName
                  }
                }
                device{
                  id
                  deviceName
                }
                messages {
                  items{
                    id
                    bodyText
                    createdAt
                    emailStatus
                    smsStatus
                    smsSendInformation
                    emailSendInformation
                    messageType
                    staff {
                      id
                      firstName
                    }
                  }
                }
              }
            }
            replacedByStandbyByRoute (sortDirection: DESC){
              items {
                id
                group
                createdAt
                routeNumber
                status
                standby
                time
                route {
                  id
                }
                staff {
                  id
                  firstName
                  lastName
                }
              }
            }
            odometerReadings {
              items {
                id
                group
              }
              nextToken
            }
            staff {
              id
              firstName
              lastName
              receiveTextMessages
              receiveEmailMessages
              phone
              email
              keyFocusArea
              keyFocusAreaCompleted
            }
            rescuers {
              items {
                id
                staff{
                  id
                  firstName
                  lastName
                }
                group
                routeId
                staffId
                route {
                  id
                }
                startTime
                endTime
                createdAt
                updatedAt
                totalRescuedPackages
              }
            }
            helpers {
              items {
                id
                staff{
                  id
                  firstName
                  lastName
                }
                group
                routeId
                route {
                  id
                }
                staffId
                status
                startTime
                endTime
                createdAt
                updatedAt
                daIssueCreated
                counselingCreated
              }
            }
            messages {
              items{
                id
                bodyText
                createdAt
                emailStatus
                smsStatus
                smsSendInformation
                emailSendInformation
                messageType
                staff {
                  id
                  firstName
                }
              }
            }
            vehicle {
              id
              name
              parkingSpace {
                id
                group
                order
                option
              }
              status
              device {
                id
                deviceName
              }
            }
            document{
              items{
                id
                name
                key
                type
                route{
                  id
                }
              }
            }
            device {
              id
              deviceName
            }
            device2 {
              id
              deviceName
            }
          }
          nextToken
        }
      }
      nextToken
    }
  }
`;

export const getRoute = /* GraphQL */ `
  query GetRoute($id: ID!) {
    getRoute(id: $id) {
      id
      staff {
        id
        firstName
        lastName
      }
      rescuers {
        items {
          id
          staff{
            id
            firstName
            lastName
          }
          group
          routeId
          staffId
          route {
            id
          }
          startTime
          endTime
          createdAt
          updatedAt
          totalRescuedPackages
        }
      }
      helpers {
        items {
          id
          staff{
            id
            firstName
            lastName
          }
          group
          routeId
          route {
            id
          }
          staffId
          status
          startTime
          endTime
          createdAt
          updatedAt
        }
      }
    }
  }
`;

export const listStaffsDailyRoster = /* GraphQL */ `
  query StaffsByGroup(
    $group: String
    $firstName: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelStaffFilterInput
    $limit: Int
    $nextToken: String
  ) {
    staffsByGroup(
      group: $group
      firstName: $firstName
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        firstName
        lastName
        alternateNames
        status
        phone
        email
        receiveTextMessages
        receiveEmailMessages
        defaultVehicle{
          id
          name
          status
          device {
            id
            deviceName
            status
          }
          device2 {
            id
            deviceName
            status
          }
          parkingSpace {
            id
            group
            order
            option
          }
        }
        defaultVehicle2{
          id
          name
          status
          device {
            id
            deviceName
            status
          }
          device2 {
            id
            deviceName
            status
          }
          parkingSpace {
            id
            group
            order
            option
          }
        }
        defaultVehicle3{
          id
          name
          status
          device {
            id
            deviceName
            status
          }
          device2 {
            id
            deviceName
            status
          }
          parkingSpace {
            id
            group
            order
            option
          }
        }
      }
      nextToken
    }
  }
`;
