export const createRoute = /* GraphQL */ `
  mutation CreateRoute(
    $input: CreateRouteInput!
    $condition: ModelRouteConditionInput
  ) {
    createRoute(input: $input, condition: $condition) {
      id
      group
      notes
      parkingSpace
      routeNumber
      staging
      status
      helperStatus
      standby
      time
      isNotActive
      messageSentTime
      messageSentError
      receivedAnnouncement
      rescued
      totalStops
      totalPackages
      completedStops
      completedPackages
      undeliveredPackagesTotal
      undeliveredPackagesBusinessClose
      undeliveredPackagesUnableToAccess
      undeliveredPackagesOtherAWS
      pickUpReturnPackage
      additionalPackagesFromRescue
      splitWithRosterAssignment
      additionalPackagesFromSplit
      firstStopTime
      lastStopTime
      daWorkStartTime
      daWorkEndTime
      daRouteStartTime
      daRouteEndTime
      rtsTime
      lunchStartTime
      lunchEndTime
      inspectionFueled
      inspectionCleaned
      inspectionFlashers
      inspectionCreditCard
      inspectionKeys
      inspectionDeviceCharger
      inspectionDeviceBattery
      inspectionNotes
      createdAt
      updatedAt
    }
  }
`;

export const updateRoute = /* GraphQL */ `
  mutation UpdateRoute(
    $input: UpdateRouteInput!
    $condition: ModelRouteConditionInput
  ) {
    updateRoute(input: $input, condition: $condition) {
      id
      group
      notes
      parkingSpace
      routeNumber
      staging
      status
      helperStatus
      standby
      time
      isNotActive
      messageSentTime
      messageSentError
      receivedAnnouncement
      rescued
      createdAt
      updatedAt
    }
  }
`;

export const deleteRoute = /* GraphQL */ `
  mutation DeleteRoute(
    $input: DeleteRouteInput!
    $condition: ModelRouteConditionInput
  ) {
    deleteRoute(input: $input, condition: $condition) {
      id
      group
      notes
      parkingSpace
      routeNumber
      staging
      status
      helperStatus
      standby
      time
      isNotActive
      messageSentTime
      messageSentError
      receivedAnnouncement
      rescued
      createdAt
      updatedAt
    }
  }
`;


export const createReplacedByStandbyByRoute = /* GraphQL */ `
  mutation CreateReplacedByStandbyByRoute(
    $input: CreateReplacedByStandbyByRouteInput!
    $condition: ModelReplacedByStandbyByRouteConditionInput
  ) {
    createReplacedByStandbyByRoute(input: $input, condition: $condition) {
      id
      group
      createdAt
      routeNumber
      status
      time
      updatedAt
      dailyRoster {
        id
        group
        createdAt
        updatedAt
        replaceByRoute {
          nextToken
        }
        replacedByStandbyByRoute {
          nextToken
        }
      }
      route {
        id
        group
      }
      staff {
        id
        group
        firstName
        lastName
        status
        createdAt
        updatedAt
      }
    }
  }
`;