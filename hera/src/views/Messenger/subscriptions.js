export const onUpdateStaff = /* GraphQL */ `
  subscription OnUpdateStaff {
    onUpdateStaff {
      id
      firstName
      lastName
      smsLastMessageTimestamp
      smsLastMessage  
    }
  }
`;
