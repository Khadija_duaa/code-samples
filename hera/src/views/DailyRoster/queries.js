import { dailyLogFragment } from '@/store/fragments'
export const importListStaffs = /* GraphQL */ `
  query StaffsByGroup(
    $group: String
    $firstName: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelStaffFilterInput
    $limit: Int
    $nextToken: String
  ) {
    staffsByGroup(
      group: $group
      firstName: $firstName
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        firstName
        lastName
        transporterId
        keyFocusArea
        keyFocusAreaCompleted
        defaultVehicle {
          id
          status
          parkingSpace {
            id
            group
            order
            option
          }
          vehicleType {
            id
            group
            order
            option
          }
          device{
            id
            status
          }
        }
        defaultVehicle2 {
          id
          status
          parkingSpace {
            id
            group
            order
            option
          }
          vehicleType {
            id
            group
            order
            option
          }
          device{
            id
            status
          }
        }
        defaultVehicle3 {
          id
          status
          parkingSpace {
            id
            group
            order
            option
          }
          vehicleType {
            id
            group
            order
            option
          }
          device{
            id
            status
          }
        }
        authorizedToDrive {
          items {
            id
            optionCustomList {
              id
              option
            }
          }
          nextToken
        }
      }
      nextToken
    }
  }
`;

export const vehiclesByGroup = /* GraphQL */ `
  query VehiclesByGroup(
    $group: String
    $name: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelVehicleFilterInput
    $limit: Int
    $nextToken: String
  ) {
    vehiclesByGroup(
      group: $group
      name: $name
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        name
        vehicle
        status
        parkingSpace {
          id
          group
          order
          option
        }
        vehicleType {
          id
          group
          option
        }
        defaultStaff {
          nextToken
        }
        defaultStaff2 {
          nextToken
        }
        device {
          id
          group
          deviceName
          status
          notes
        }
        route {
          nextToken
        }
      }
      nextToken
    }
  }
`;

export const devicesByGroup = /* GraphQL */ `
  query DevicesByGroup(
    $group: String
    $deviceName: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelDeviceFilterInput
    $limit: Int
    $nextToken: String
  ) {
    devicesByGroup(
      group: $group
      deviceName: $deviceName
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        deviceName
        imei
        status
        route {
            items{
                id
            }
          nextToken
        }
        vehicle {
            items{
                id
            }
          nextToken
        }
      }
      nextToken
    }
  }
`;

export const dailyRostersByGroup = /* GraphQL */ `
  query DailyRostersByGroup(
    $group: String
    $id: ModelIDKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelDailyRosterFilterInput
    $limit: Int
    $nextToken: String
  ) {
    dailyRostersByGroup(
      group: $group
      id: $id
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        notesDate
        amNotes
        pmNotes
        standUpNotes
        lastStandUpSentTime
        route {
          items {
            id
            group
            notes
            routeNumber
            status
            parkingSpace {
              id
              group
              order
              option
            }
            staging
            standby
            time
            messageSentTime
            messageSentError
            receivedAnnouncement
            rescued
            staff {
              id
              firstName
              lastName
              receiveTextMessages
              receiveEmailMessages
              keyFocusArea
              keyFocusAreaCompleted
              phone
              email
            }
            helper {
              id
              firstName
              lastName
              receiveTextMessages
              receiveEmailMessages
              phone
              email
            }
            rescuer{
              id
              firstName
              lastName
            }
            messages {
              items{
                id
                bodyText
                createdAt
                emailStatus
                smsStatus
                smsSendInformation
                emailSendInformation
                messageType
                staff {
                  id
                  firstName
                }
              }
            }
            vehicle {
              id
              name
              parkingSpace {
                id
                group
                order
                option
              }
              vehicleType {
                id
                group
                option
              }
              device {
                id
                deviceName
              }
            }
            document{
              items{
                id
                name
                key
                type
                route{
                  id
                }
              }
            }
            device {
              id
              deviceName
            }
          }
          nextToken
        }
      }
      nextToken
    }
  }
`;

export const dailyRostersByGroupAndNotesDate = /* GraphQL */ `
  query DailyRostersByGroupAndNotesDate(
    $group: String
    $notesDate: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelDailyRosterFilterInput
    $limit: Int
    $nextToken: String
  ) {
    dailyRostersByGroupAndNotesDate(
      group: $group
      notesDate: $notesDate
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        notesDate
        amNotes
        pmNotes
        standUpNotes
        lastStandUpSentTime
        updatedAt
        attachment {
          id
          group
          s3Key
          expirationDate
          contentType
        }
        route {
          items {
            id
            group
            notes
            routeNumber
            previousStatus
            status
            helperStatus
            parkingSpace {
              id
              group
              order
              option
            }
            staging
            standby
            time
            isNotActive
            messageSentTime
            messageSentError
            receivedAnnouncement
            updatedAt
            replaceByRoute (sortDirection: ASC){
              items{
                id
                group
                notes
                routeNumber
                status
                helperStatus
                parkingSpace {
                  id
                  group
                  order
                  option
                }
                staging
                standby
                time
                isNotActive
                messageSentTime
                messageSentError
                receivedAnnouncement
                updatedAt
                route{
                  id
                }
                staff {
                  id
                  firstName
                  lastName
                  receiveTextMessages
                  receiveEmailMessages
                  phone
                  email
                  keyFocusArea
                  keyFocusAreaCompleted
                  authorizedToDrive {
                    items {
                      id
                      optionCustomList {
                        id
                        option
                      }
                    }
                    nextToken
                  }
                }
                document{
                  items{
                    id
                    name
                    key
                    type
                    route{
                      id
                    }
                  }
                }
                odometerReadings {
                  items {
                    id
                    group
                  }
                  nextToken
                }
                vehicle{
                  id
                  name
                  parkingSpace {
                    id
                    group
                    order
                    option
                  }
                  vehicleType {
                    id
                    group
                    option
                  }
                  status
                  device {
                    id
                    deviceName
                  }
                }
                device{
                  id
                  deviceName
                }
                messages {
                  items{
                    id
                    bodyText
                    createdAt
                    emailStatus
                    smsStatus
                    smsSendInformation
                    emailSendInformation
                    messageType
                    staff {
                      id
                      firstName
                    }
                  }
                }
              }
            }
            odometerReadings {
              items {
                id
                group
              }
              nextToken
            }
            vehicleDamage {
              items {
                id
              }
              nextToken
            }
            staff {
              id
              firstName
              lastName
              receiveTextMessages
              receiveEmailMessages
              phone
              email
              keyFocusArea
              keyFocusAreaCompleted
              defaultDevice
              defaultVehicle {
                id
                name
                status
                device {
                  id
                  deviceName
                  status
                }
                parkingSpace {
                  id
                  group
                  order
                  option
                }
                vehicleType {
                  id
                  group
                  option
                }
              }
              defaultVehicle2 {
                id
                name
                status
                device {
                  id
                  deviceName
                  status
                }
                parkingSpace {
                  id
                  group
                  order
                  option
                }
                vehicleType {
                  id
                  group
                  option
                }
              }
              defaultVehicle3 {
                id
                name
                status
                device {
                  id
                  deviceName
                  status
                }
                parkingSpace {
                  id
                  group
                  order
                  option
                }
                vehicleType {
                  id
                  group
                  option
                }
              }
              authorizedToDrive {
                items {
                  id
                  optionCustomList {
                    id
                    option
                  }
                }
                nextToken
              }
            }
            helper {
              id
              firstName
              lastName
              receiveTextMessages
              receiveEmailMessages
              phone
              email
            }
            rescuer{
              id
              firstName
              lastName
            }
            rescuers {
              items {
                id
                staff{
                  id
                  firstName
                  lastName
                }
              }
            }
            messages {
              items{
                id
                bodyText
                createdAt
                emailStatus
                smsStatus
                smsSendInformation
                emailSendInformation
                messageType
                staff {
                  id
                  firstName
                }
              }
            }
            vehicle {
              id
              name
              parkingSpace {
                id
                group
                order
                option
              }
              vehicleType {
                id
                group
                option
              }
              status
              device {
                id
                deviceName
              }
            }
            document{
              items{
                id
                name
                key
                type
                route{
                  id
                }
              }
            }
            device {
              id
              deviceName
            }
            kudos{
              items{
                id
                staffId
              }
            }
          }
          nextToken
        }
      }
      nextToken
    }
  }
`;

export const dailyRostersByGroupPM = /* GraphQL */ `
  query DailyRostersByGroup(
    $group: String
    $id: ModelIDKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelDailyRosterFilterInput
    $limit: Int
    $nextToken: String
  ) {
    dailyRostersByGroup(
      group: $group
      id: $id
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        notesDate
        pmNotes
      }
      nextToken
    }
  }
`;

export const getVehicle = /* GraphQL */ `
  query GetVehicle($id: ID!) {
    getVehicle(id: $id) {
      id
      group
      name
      status
      lastOdometerReadingDate
      licensePlateExp
    }
  }
`;

export const dailyLogsByDate = dailyLogFragment + /* GraphQL */ `
  query DailyLogsByDate(
    $group: String
    $date: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelDailyLogFilterInput
    $limit: Int
    $nextToken: String
  ) {
    dailyLogsByDate(
      group: $group
      date: $date
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        ...dailyLogFragment
      }
      nextToken
    }
  }
`;

export const getDailyLogDocuments = /* GraphQL */ `
  query GetDailyLog($id: ID!) {
    getDailyLog(id: $id) {
      id
      documents {
        items {
          id
          group
          name
          key
          type
          uploadDate
          notes
          documentDate
          createdAt
          updatedAt
        }
        nextToken
      }
    }
  }
`;