import store from '@/store'
import { mixinMethods } from '@/main'
import { createMultipleQuery, literalTypeDef } from '@/utilities/gqlQueries'
import { dailyLogFragment, dailyRosterFragment } from '@/store/fragments'

let dailyLogCounter = 0
const MAX_DAILY_ROSTER_SIZE = 30

export const clearCachedDailyRoster = function (dailyRosterId){
    const dailyRosterCache = store.state.subscriptionStore.dailyRosterCache
    const index = dailyRosterCache.findIndex(d => d.id === dailyRosterId)
    if(index > -1){
        const deleted = dailyRosterCache.splice(index, 1)
    }
}

export const getDailyRosterData = async function (payload, times = 0){

    if(times > 0) return

    const { dailyRosterId, previuosDailyRosterId, dateLimitsInTenantTimeZone, fallback } = payload

    const { dailyRosterCache, dailyLogCache} = store.state.subscriptionStore
    const dailyRosterLookupMap = store.getters["subscriptionStore/getDailyRosterLookupMap"]
    
    const keyDate = dateLimitsInTenantTimeZone.join('|')
    const matchToday = dailyRosterLookupMap[dailyRosterId]
    const matchYesterday = dailyRosterLookupMap[previuosDailyRosterId]
    const matchDailyLog = dailyLogCache[keyDate]

    const input = {
        dailyRosterId,
        previuosDailyRosterId,
        group: store.state.userInfo.tenant.group,
        date: { between: dateLimitsInTenantTimeZone }
    }

    const currentDailyRosterExpression = {
        parameters: literalTypeDef /* GraphQL */ `(
            id: ID! ${ input.dailyRosterId }
        )`,
        body: /* GraphQL */ `{
            getCurrentDailyRoster: getDailyRoster{
                ...dailyRosterFragment
            }
        }`,
    }
    const previousDailyRosterExpression = {
        parameters: literalTypeDef /* GraphQL */ `(
            id: ID! ${ input.previuosDailyRosterId }
        )`,
        body: /* GraphQL */ `{
            getPreviousDailyRoster: getDailyRoster{
                ...dailyRosterFragment
            }
        }`,
    }
    const dailyLogsExpression = {
        parameters: literalTypeDef /* GraphQL */ `(
            group: String ${ input.group }
            date: ModelStringKeyConditionInput ${ input.date }
            sortDirection: ModelSortDirection ${ "DESC" }
            limit: Int ${ 900 }
        )`,
        body: /* GraphQL */ `{
            dailyLogsByDate{
                items {
                    ...dailyLogFragment
                }
                nextToken
            }
        }`,
    }
    const messageTemplatesExpression = {
        parameters: literalTypeDef /* GraphQL */ `(
            group: String ${ input.group }
            limit: Int ${ 900 }
        )`,
        body: /* GraphQL */ `{
            messageTemplatesByGroup{
                items {
                    id
                    group
                    name
                    message
                    createdAt
                    updatedAt
                }
                nextToken
            }
        }`,
    }

    const preparedFragments = {}
    const preparedExpressions = [
        messageTemplatesExpression
    ]

    if(!matchToday || !matchYesterday){
        preparedFragments.dailyRosterFragment = dailyRosterFragment
        if(!matchToday){
            preparedExpressions.push(currentDailyRosterExpression)
        }
        if(!matchYesterday){
            preparedExpressions.push(previousDailyRosterExpression)
        }
    }
    if(!matchDailyLog){
        preparedFragments.dailyLogFragment = dailyLogFragment
        preparedExpressions.push(dailyLogsExpression)
    }

    const { query, variables } = createMultipleQuery({
        fragments: preparedFragments,
        expressions: preparedExpressions
    })

    const responseMulti = await mixinMethods.api(query, variables)

    if(responseMulti.data.dailyLogsByDate){
        dailyLogCache[keyDate] = responseMulti.data.dailyLogsByDate.items
        dailyLogCache[keyDate].counter = ++dailyLogCounter
    }else{
        responseMulti.data.dailyLogsByDate = { items: matchDailyLog }
    }

    if(responseMulti.data.getCurrentDailyRoster){
        dailyRosterCache.push(responseMulti.data.getCurrentDailyRoster)
    }else if(matchToday){
        responseMulti.data.getCurrentDailyRoster = matchToday
    }else{
        const result = await fallback()
        if(result.errors?.[0]?.errorType?.includes("DynamoDB:ConditionalCheckFailedException")) {
            await new Promise(r => setTimeout(r, 500)) //sleep
            const retry = await getDailyRosterData(payload, 1)
            if(retry){
                return retry
            } 
        }else if(result.data?.createDailyRoster){
            const currentDailyRoster = result.data.createDailyRoster
            dailyRosterCache.push(responseMulti.data.getCurrentDailyRoster = currentDailyRoster)
        }
        else{
            throw {message: "Error when creating a new Daily Roster", responseMulti, result}
        }
    }

    if(responseMulti.data.getPreviousDailyRoster){
        dailyRosterCache.push(responseMulti.data.getPreviousDailyRoster)
    }else if(matchYesterday){
        responseMulti.data.getPreviousDailyRoster = matchYesterday
    }

    const listCount = dailyRosterCache.length
    if(listCount > MAX_DAILY_ROSTER_SIZE){
        for(let i = listCount - MAX_DAILY_ROSTER_SIZE; i-- > 0;){
            dailyRosterCache.shift()
        }
    }

    const dailyLogEntries = Object.entries(dailyLogCache)
    const dailyLogEntriesCount = dailyLogEntries.length
    if(dailyLogEntriesCount >= MAX_DAILY_ROSTER_SIZE){
        const sortedDailyLogEntries = dailyLogEntries.sort((a, b)=> a.counter - b.counter)
        for(let i = dailyLogEntriesCount - MAX_DAILY_ROSTER_SIZE; i >= 0; --i){
            const [key, value] = sortedDailyLogEntries[i]
            delete dailyLogCache[key]
        }
    }

    return responseMulti
}