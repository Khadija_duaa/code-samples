export function hasNullOrEmptyStatus(route) {
  const replaceByRoute = route.replaceByRoute?.items;
  if(replaceByRoute?.length)
    return !replaceByRoute[replaceByRoute.length-1].status;

  return !route.status;
}

export function hasVehicleSelected (route) {
  const replaceByRoute = route.replaceByRoute?.items;
  if(replaceByRoute?.length)
    return !!replaceByRoute[replaceByRoute.length-1].vehicle?.id;

  return !!route.vehicle?.id;
}
