
/**
 * The function returns the last object in an array of route replacements.
 * @param [replaceByRoute].
 * @returns the last object in the array
 */
export function routeReplacement(replaceByRoute=[]){
    let activeReplacementAssociate = {}
    const lastIndex = replaceByRoute.length - 1

    if(replaceByRoute.length){
      activeReplacementAssociate = replaceByRoute[lastIndex]
    }
    return activeReplacementAssociate
}
