export const staffAttendanceStatistics = /* GraphQL */ `
  query DailyRostersByGroupAndNotesDate(
    $group: String
    $notesDate: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelDailyRosterFilterInput
    $limit: Int
    $nextToken: String
    $routeFilter: ModelRouteFilterInput
  ) {
    dailyRostersByGroupAndNotesDate(
      group: $group
      notesDate: $notesDate
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        notesDate
        updatedAt
        route(
          filter: $routeFilter
        ){
          items {
            id
            status
            time
            rescued
            staff {
              id
              firstName
              lastName
              status
            }
            replaceByRoute{
              items{
                id
                status
                staff{
                  id
                  firstName
                  lastName
                  status
                }
              }
            }
          }
        }
      }
      nextToken
    }
  }
`;
export const dailyRostersByGroupAndNotesDate = /* GraphQL */ `
  query DailyRostersByGroupAndNotesDate(
    $group: String
    $notesDate: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelDailyRosterFilterInput
    $limit: Int
    $nextToken: String
    $routeFilter: ModelRouteFilterInput
  ) {
    dailyRostersByGroupAndNotesDate(
      group: $group
      notesDate: $notesDate
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        notesDate
        updatedAt
        route(
          filter: $routeFilter
        ){
          items {
            id
            status
            time
            rescued
            staff {
              id
              firstName
              lastName
              status
            }
            rescuers {
              items {
                id
                updatedAt
                startTime
                staff{
                  id
                  firstName
                  lastName
                  status
                }
              }
            }
            infractions{
              items{
                id
                infractionType
                infractionDescription
                date
                time
                counseling{
                  id
                  severity {
                    id
                    option
                  }
                  counselingNotes
                  status
                  employeeNotes
                }
                staffId
              }
            }
            kudos{
              items{
                id
                date
                group
                kudoType
                notes
                staffId
                updatedAt
              }
            }
            replaceByRoute (sortDirection: ASC){
              items{
                id
                status
                isNotActive
                updatedAt
                staff {
                  id
                  firstName
                  lastName
                  status
                }
              }
            }
          }
        }
      }
      nextToken
    }
  }
`;