export const vehicleStatusByGroup = /* GraphQL */ `
  query VehicleStatusByGroup(
    $group: String
    $date: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelVehicleStatusFilterInput
    $limit: Int
    $nextToken: String
  ) {
    vehicleStatusByGroup(
      group: $group
      date: $date
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        date
        previousStatus
        currentStatus
        createdAt
        updatedAt
        vehicle {
          id
          group
          name
          status
        }
      }
      nextToken
    }
  }
`;

export const staffStatussByGroup = /* GraphQL */ `
  query StaffStatussByGroup(
    $group: String
    $date: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelStaffStatusFilterInput
    $limit: Int
    $nextToken: String
  ) {
    staffStatussByGroup(
      group: $group
      date: $date
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        date
        previousStatus
        currentStatus
        createdAt
        updatedAt
        staff {
          id
          group
          firstName
          lastName
          status
        }
      }
      nextToken
    }
  }
`;
export const counselingsByGroupAndDate = /* GraphQL */ `
  query CounselingsByGroupAndDate(
    $group: String
    $date: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelCounselingFilterInput
    $limit: Int
    $nextToken: String
  ) {
    counselingsByGroupAndDate(
      group: $group
      date: $date
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        date
        status
        dateSigned
        createdAt
        updatedAt
        severity {
          id
          option
        }
        staff {
          id
          group
          firstName
          lastName
        }
      }
      nextToken
    }
  }
`;

export const counselingsByGroupAndDateSigned = /* GraphQL */ `
  query CounselingsByGroupAndDateSigned(
    $group: String
    $dateSigned: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelCounselingFilterInput
    $limit: Int
    $nextToken: String
  ) {
    counselingsByGroupAndDateSigned(
      group: $group
      dateSigned: $dateSigned
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        date
        status
        dateSigned
        createdAt
        updatedAt
        severity {
          id
          option
        }
        staff {
          id
          group
          firstName
          lastName
        }
      }
      nextToken
    }
  }
`;

export const accidentsByGroupAndAccidentDate = /* GraphQL */ `
  query AccidentsByGroupAndAccidentDate(
    $group: String
    $accidentDate: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelAccidentFilterInput
    $limit: Int
    $nextToken: String
  ) {
    accidentsByGroupAndAccidentDate(
      group: $group
      accidentDate: $accidentDate
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        accidentDate
        vehicleHistoryType
        location
        damage
        mileage  
        createdAt
        updatedAt
        staff {
          id
          firstName
          lastName
        }
        verifiedBy {
          id
          firstName
          lastName
        }
        vehicle {
          id
          name
        }
      }
      nextToken
    }
  }
`;
export const injuriesByGroupAndCreatedAt = /* GraphQL */ `
  query InjuriesByGroupAndCreatedAt(
    $group: String
    $createdAt: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelInjuryFilterInput
    $limit: Int
    $nextToken: String
  ) {
    injuriesByGroupAndCreatedAt(
      group: $group
      createdAt: $createdAt
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        injuryDate
        injuryTime
        completedBy
        createdAt
        updatedAt
        staff {
          id
          firstName
          lastName
        }
        completedByUser {
          id
          firstName
          lastName
        }
      }
      nextToken
    }
  }
`;


export const dailyRostersByGroupAndNotesDate = /* GraphQL */ `
  query DailyRostersByGroupAndNotesDate(
    $group: String
    $notesDate: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelDailyRosterFilterInput
    $limit: Int
    $nextToken: String
  ) {
    dailyRostersByGroupAndNotesDate(
      group: $group
      notesDate: $notesDate
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        notesDate
        createdAt
        updatedAt
        route {
          items {
            id
            group
            previousStatus
            statusChangedTime
            status
            time
            createdAt
            updatedAt
            rescued
            staff {
              id
              firstName
              lastName
              email
              
            }
            rescuers {
              items {
                id
                createdAt
                updatedAt
                staff{
                  id
                  firstName
                  lastName
                }
              }
            }
            vehicle {
              id
              name
              status
            }
            replaceByRoute (sortDirection: ASC){
              items{
                id
                status
                isNotActive
                updatedAt
                staff {
                  id
                  firstName
                  lastName
                }
                vehicle{
                  id
                  name
                }
              }
            }
          }
          nextToken
        }
      }
      nextToken
    }
  }
`;

export const dailyLogsByDate = /* GraphQL */ `
  query DailyLogsByDate(
    $group: String
    $date: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelDailyLogFilterInput
    $limit: Int
    $nextToken: String
  ) {
    dailyLogsByDate(
      group: $group
      date: $date
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        type
        date
        notes
        documents {
          items {
            id
          }
          nextToken
        }
        vehicle {
          id
          name
        }
      }
    }
  }
`;
