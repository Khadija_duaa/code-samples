import EventBus from '@/eventBus'
export default {

    data(){
        return {
            ensureUpdate: {
                init: ()=>{
                    const prevCount = this.getDailyRosterCount()
                    return {
                        finish: ()=>{
                            setTimeout(_this=>{
                                const nextCount = _this.getDailyRosterCount()
                                if(
                                    prevCount.routesAndReplaceBys === nextCount.routesAndReplaceBys &&
                                    prevCount.standByRoutes === nextCount.standByRoutes
                                ){
                                    EventBus.$emit('reload-daily-rostering')
                                }
                            }, 300, this)
                        }
                    }
                }
            }
        }
    },

    methods: {
        getDailyRosterCount(){
            const routesAndReplaceBys = this.dailyRosterTableData.reduce((count, route) => {
                const replaceByCount = route.replaceByRoute?.items?.length || 0
                return count + replaceByCount + 1
            }, 0)
            const standByRoutes = this.standbyTableData.length
            return {
                routesAndReplaceBys,
                standByRoutes
            }
        },
    }

}