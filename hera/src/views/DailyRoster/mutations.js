import { routeFragment, replaceByRouteFragment } from '@/store/fragments.js'
export const createReplaceByRoute = replaceByRouteFragment + /* GraphQL */ `
  mutation CreateReplaceByRoute(
    $input: CreateReplaceByRouteInput!
    $condition: ModelReplaceByRouteConditionInput
  ) {
    createReplaceByRoute(input: $input, condition: $condition) {
      ...replaceByRouteFragment
    }
  }
`;

export const updateReplaceByRoute = replaceByRouteFragment + /* GraphQL */ `
  mutation UpdateReplaceByRoute(
    $input: UpdateReplaceByRouteInput!
    $condition: ModelReplaceByRouteConditionInput
  ) {
    updateReplaceByRoute(input: $input, condition: $condition) {
      ...replaceByRouteFragment
    }
  }
`;

export const deleteReplaceByRoute = replaceByRouteFragment + /* GraphQL */ `
  mutation DeleteReplaceByRoute(
    $input: DeleteReplaceByRouteInput!
    $condition: ModelReplaceByRouteConditionInput
  ) {
    deleteReplaceByRoute(input: $input, condition: $condition) {
      ...replaceByRouteFragment
    }
  }
`;

export const createRoute = routeFragment + /* GraphQL */ `
  mutation CreateRoute(
    $input: CreateRouteInput!
    $condition: ModelRouteConditionInput
  ) {
    createRoute(input: $input, condition: $condition) {
      ...routeFragment
    }
  }
`;

export const deleteRoute = routeFragment + /* GraphQL */ `
  mutation DeleteRoute(
    $input: DeleteRouteInput!
    $condition: ModelRouteConditionInput
  ) {
    deleteRoute(input: $input, condition: $condition) {
      ...routeFragment
    }
  }
`;

export const updateRoute = routeFragment + /* GraphQL */ `
  mutation UpdateRoute(
    $input: UpdateRouteInput!
    $condition: ModelRouteConditionInput
  ) {
    updateRoute(input: $input, condition: $condition) {
      ...routeFragment
    }
  }
`;