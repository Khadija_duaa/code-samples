export const sampleDataRepeatOffenders =  [
    {
        firstName: "Alexander",
        lastName: "Rivas",
        status: "Active",
        issueType: "Called Out",
        occurrencesThisMonth: 2,
        occurrencesLastMonth: 0,
        occurrencesVariance: 2
    },
    {
        firstName: "Carlos",
        lastName: "Taylor",
        status: "Active",
        issueType: "Rescued DA",
        occurrencesThisMonth: 1,
        occurrencesLastMonth: 1,
        occurrencesVariance: 0
    },
    {
        firstName: "Anthony",
        lastName: "Smith",
        status: "Active",
        issueType: "Vehicle Damage",
        occurrencesThisMonth: 2,
        occurrencesLastMonth: 0,
        occurrencesVariance: 2
    },
    {
        firstName: "Carlos",
        lastName: "Taylor",
        status: "Active",
        issueType: "Called Out",
        occurrencesThisMonth: 1,
        occurrencesLastMonth: 0,
        occurrencesVariance: 1
    },
    {
        firstName: "Carlos",
        lastName: "Taylor",
        status: "Active",
        issueType: "No Show, No Call",
        occurrencesThisMonth: 1,
        occurrencesLastMonth: 0,
        occurrencesVariance: 1
    },
    {
        firstName: "Anthony",
        lastName: "Smith",
        status: "Active",
        issueType: "No Show, No Call",
        occurrencesThisMonth: 3,
        occurrencesLastMonth: 0,
        occurrencesVariance: 3
    },
    {
        firstName: "Brian",
        lastName: "Miller",
        status: "Active",
        issueType: "Vehicle Damage",
        occurrencesThisMonth: 1,
        occurrencesLastMonth: 0,
        occurrencesVariance: 1
    },
    {
        firstName: "Anthony",
        lastName: "Smith",
        status: "Active",
        issueType: "Late, No Call",
        occurrencesThisMonth: 0,
        occurrencesLastMonth: 2,
        occurrencesVariance: -2
    }
]