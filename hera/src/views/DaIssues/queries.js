export const getInfraction = /* GraphQL */ `
  query GetInfraction($id: ID!) {
    getInfraction(id: $id) {
      id
      group
      infractionType
      comment
      date
      time
      staffId
      station
      trackingNumber
      infractionTier
      infractionDescription
      appealDate
      resolvedDate
      appealStatus
      appealNotes
      miscNotes
      createdAt
      updatedAt
      counseling {
        id
        group
        date
        severity {
          id
          option
        }
        refusedToSign
        signatureAcknowledge
        signature
        counselingNotes
        employeeNotes
        correctiveActionSummary
        priorDiscussionSummary
        consequencesOfFailure
        dateSent
        status
        createdAt
        updatedAt
        infractions {
          nextToken
        }
        staff {
          id
          group
          firstName
          lastName
          alternateNames
          phone
          email
        }
        user {
          id
          group
          firstName
          lastName
          phone
          email
        }
      }
      documents {
        items {
            id
            group
            name
            key
            type
            uploadDate
            notes
            createdAt
            updatedAt
          }
        nextToken
      }
      staff {
        id
        group
        transporterId
        firstName
        lastName
        alternateNames
        phone
        email
      }
      type {
        id
        option
      }
    }
  }
`;

export const infractionsByStaff = /* GraphQL */ `
  query InfractionsByStaff(
    $staffId: String
    $date: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelInfractionFilterInput
    $limit: Int
    $nextToken: String
  ) {
    infractionsByStaff(
      staffId: $staffId
      date: $date
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        infractionType
        comment
        date
        time
        staffId
        infractionTier
        infractionDescription
        appealDate
        resolvedDate
        appealStatus
        appealNotes
        miscNotes
        counseling {
          id
          status
        }
        staff {
          id
          firstName
          lastName
        }
        type {
          id
          option
        }
      }
      nextToken
    }
  }
`;

export const infractionsByGroupAndDate = /* GraphQL */ `
  query InfractionsByGroupAndDate(
    $group: String
    $date: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelInfractionFilterInput
    $limit: Int
    $nextToken: String
  ) {
    infractionsByGroupAndDate(
      group: $group
      date: $date
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        infractionType
        comment
        date
        time
        infractionTier
        infractionDescription
        counseling {
          id
          status
        }
        staff {
          id
          firstName
          lastName
          status
        }
        type {
          id
          option
        }
      }
      nextToken
    }
  }
`;

export const getRoute = /* GraphQL */ `
  query GetRoute(
    $id: ID!
    $infractionSortDirection: ModelSortDirection
    $infractionLimit: Int
    $infractionNextToken: String
  ){
    getRoute(id: $id) {
      id
      status
      time
      rescued
      staff {
        id
        firstName
        lastName
        status
      }
      infractions(
        sortDirection: $infractionSortDirection
        limit: $infractionLimit
        nextToken: $infractionNextToken
      ){
        items{
          id
          group
          infractionType
          comment
          date
          time
          staffId
          infractionTier
          infractionDescription
          appealDate
          resolvedDate
          appealStatus
          appealNotes
          miscNotes
          counseling {
            id
            status
          }
          staff {
            id
            firstName
            lastName
          }
        }
        nextToken
      }
      

    }
  }
`;
