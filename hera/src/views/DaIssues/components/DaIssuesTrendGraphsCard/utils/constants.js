export const CHART_MIN_LABELS_COUNT = 5;
export const CHART_VALID_TIMEFRAMES = ['years', 'months', 'weeks', 'days'];
export const CHART_DATE_FORMATS = {
  years: 'YYYY',
  months: 'MMM YYYY',
  weeks: '[Week] ww, YYYY',
  days: 'MM/DD/YYYY',
};
