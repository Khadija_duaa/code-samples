import timeframeOptions from '@/utilities/constants/filterOptions/timeframeOptions';
import { HIDDEN_ISSUE_TYPE_CREATE_ISSUE } from '@/utilities/constants/hiddenSecctionForCustomList';
import { mapState, mapGetters } from 'vuex';

function activeValueListsOptions(item) {
  return (
    (item.custom && !item.deleted) ||
    (!item.custom && !item.deleted && !item.hidden)
  )
}

export default {
  data() {
    return {
      filtersLoading: false,
      filtersAssociateStatusOptions: [],
      filtersAssociateStatusSelected: [],
      filtersInfractionTypeOptions: [],
      filtersInfractionTypeSelected: '',
      filtersTimeFrameOptions: [],
      filtersTimeFrameSelected: '',
    };
  },

  created() {
    this.loadFilters();
  },

  computed: {
    ...mapState([
      'valuelists',
    ]),

    ...mapGetters([
      'issueTypeList',
    ]),

    filtersCount() {
      let count = 1; // Roster Status Filter will always be active
      count += (this.filtersAssociateStatusOptions.length === this.filtersAssociateStatusSelected.length)? 0: 1;
      count += (this.filtersTimeFrameSelected === 'all dates')? 0: 1;
      return count;
    },
  },

  methods: {
    async loadFilters() {
      try {
        this.filtersLoading = true;
        this.loadAssociateStatusFilters();
        this.loadTimeFrameFilters();
        await this.loadInfractionTypeFilters();
      }
      finally {
        this.filtersLoading = false;
      }
    },

    loadAssociateStatusFilters() {
      this.filtersAssociateStatusOptions = this.valuelists['staff-status'].items
        .filter(activeValueListsOptions)
        .map(s => s.value);
      this.filtersAssociateStatusSelected = this.filtersAssociateStatusOptions;
    },

    async loadInfractionTypeFilters() {
      await this.loadCustomList();
      this.filtersInfractionTypeOptions = this.customListEnabled(this.issueTypeList, true, null, null, HIDDEN_ISSUE_TYPE_CREATE_ISSUE)
        .map(item => ({label: item.option, value: item.option}));
      this.$_setInfractionTypeSelected('Called Out');
    },

    loadTimeFrameFilters() {
      this.filtersTimeFrameOptions = timeframeOptions;
      this.$_setTimeFrameSelected('last 90 days');
    },

    $_setAssociateStatusSelected(filters) {
      this.filtersAssociateStatusSelected = filters;
    },

    $_setInfractionTypeSelected(selected) {
      this.filtersInfractionTypeSelected = selected;
      this.filtersInfractionTypeOptions = this.filtersInfractionTypeOptions.map(item => ({...item, selected: (item.value === selected)}));
    },

    $_setTimeFrameSelected(selected) {
      this.filtersTimeFrameSelected = selected;
      this.filtersTimeFrameOptions = this.filtersTimeFrameOptions.map(item => ({...item, selected: (item.value === selected)}));
    },
  },
};
