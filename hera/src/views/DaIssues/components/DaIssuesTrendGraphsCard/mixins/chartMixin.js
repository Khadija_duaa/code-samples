import {
  CHART_DATE_FORMATS,
  CHART_MIN_LABELS_COUNT,
  CHART_VALID_TIMEFRAMES,
} from "../utils/constants";

/**
 * Creates a configuration object for labels to store the format in which the labels have been parsed
 * and the array of generated labels
 * @param {Moment} start - Moment date to define start range date
 * @param {Moment} end - Moment date to define end range date
 * @param {String} timeframeDisplay - Timeframe display to calculate the labels from
 * @returns {Object} labelsConfig
 *   {String} labelsConfig.format - Format in which the labels have been parsed
 *   {Array} labelsConfig.labels - List of string labels
 */
function generateLabelsByTimeFrame(start, end, timeframeDisplay) {
  let currentDate = start;
  const fmt = CHART_DATE_FORMATS[timeframeDisplay];
  const labelsConfig = { format: fmt, labels: [] };
  do {
    labelsConfig.labels.push(currentDate.format(fmt));
    currentDate = currentDate.add(1, timeframeDisplay);
  }
  while(currentDate <= end);
  const currentLabel = currentDate.format(fmt);
  const endLabel = end.format(fmt);
  if (currentLabel === endLabel) { // Add last label in case the 'currentDate' have passed the 'end' by the timeframe offset
    labelsConfig.labels.push(currentDate.format(fmt));
  }
  return labelsConfig;
}

export default {
  methods: {
    /**
     * Handles the generation of X axis labels depending on the selected TimeFrame
     * @param {Date} start - Start date range
     * @param {Date} end - End date range
     * @param {String} timeframeDisplay - Timeframe display to calculate the labels from
     * @returns {Object} labelsConfig
     *   {String} labelsConfig.format - Format in which the labels have been parsed
     *   {Array} labelsConfig.labels - List of string labels
     */
    $_getChartLabelsConfigObject(start, end, timeframeDisplay) {
      const [startDate, endDate] = [this.$moment(start), this.$moment(end)];
      if (timeframeDisplay === 'days') return generateLabelsByTimeFrame(startDate, endDate, 'days');
      const foundIndex = CHART_VALID_TIMEFRAMES.findIndex(t => t === timeframeDisplay);
      if (foundIndex === -1) throw new Error(`Invalid time frame "${timeframeDisplay}"`);
      const diff = endDate.diff(startDate, timeframeDisplay);
      if (diff < CHART_MIN_LABELS_COUNT) return this.$_getChartLabelsConfigObject(start, end, CHART_VALID_TIMEFRAMES[foundIndex+1]);
      return generateLabelsByTimeFrame(startDate, endDate, timeframeDisplay);
    },
  },
};
