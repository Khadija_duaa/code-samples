export const tasksByGroup = /* GraphQL */ `
  query TasksByGroup(
    $group: String
    $sortDirection: ModelSortDirection
    $filter: ModelTaskFilterInput
    $limit: Int
    $nextToken: String
  ) {
    tasksByGroup(
      group: $group
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        owner
        creator
        date
        status
        taskName
        reminderDate
        isNotifyOnCreation
        isDateReminder
        executionARN
        description
        updatedAt
        assigner{
          id
          firstName
          lastName
        }
        assignee{
          id
          firstName
          lastName
          email
          phone
          receiveSmsTaskReminders
          receiveEmailTaskReminders
          receiveSmsTaskAssignments
          receiveEmailTaskAssignments
        }
      }
      nextToken
    }
  }
`;

export const tasksByGroupAndStatus = /* GraphQL */ `
  query TasksByGroupAndStatus(
    $group: String
    $sortDirection: ModelSortDirection
    $filter: ModelTaskFilterInput
    $limit: Int
    $nextToken: String
  ) {
    tasksByGroupAndStatus(
      group: $group
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        owner
        creator
        date
        status
        taskName
        reminderDate
        isNotifyOnCreation
        isDateReminder
        executionARN
        description
        updatedAt
        assigner{
          id
          firstName
          lastName
        }
        assignee{
          id
          firstName
          lastName
        }
      }
      nextToken
    }
  }
`;

export const tasksByOwner = /* GraphQL */ `
  query TasksByOwner(
    $owner: String
    $statusDate: ModelTaskByOwnerCompositeKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelTaskFilterInput
    $limit: Int
    $nextToken: String
  ) {
    tasksByOwner(
      owner: $owner
      statusDate: $statusDate
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        owner
        creator
        date
        status
        taskName
        reminderDate
        isNotifyOnCreation
        isDateReminder
        executionARN
        description
        updatedAt
        assigner{
          id
          firstName
          lastName
        }
        assignee{
          id
          firstName
          lastName
        }
      }
      nextToken
    }
  }
`;

export const tasksByCreator = /* GraphQL */ `
  query TasksByCreator(
    $creator: String
    $statusDate: ModelTaskByCreatorCompositeKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelTaskFilterInput
    $limit: Int
    $nextToken: String
  ) {
    tasksByCreator(
      creator: $creator
      statusDate: $statusDate
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        owner
        creator
        date
        status
        taskName
        reminderDate
        isNotifyOnCreation
        isDateReminder
        executionARN
        description
        updatedAt
        assigner{
          id
          firstName
          lastName
        }
        assignee{
          id
          firstName
          lastName
        }
      }
      nextToken
    }
  }
`;

export const getUser = /* GraphQL */ `
  query GetUser($id: ID!) {
    getUser(id: $id) {
      id
      cognitoSub
      firstName
      lastName
      phone
      email
      role
      receiveSmsTaskReminders
      receiveEmailTaskReminders
      receiveSmsTaskAssignments
      receiveEmailTaskAssignments
    }
  }
`;

export const getTask = /* GraphQL */ `
  query GetTask($id: ID!) {
    getTask(id: $id) {
      id
      owner
      creator
      date
      status
      taskName
      reminderDate
      isNotifyOnCreation
      isDateReminder
      executionARN
      description
      updatedAt
      assigner{
        id
        firstName
        lastName
      }
      assignee{
        id
        firstName
        lastName
        email
        phone
        receiveSmsTaskReminders
        receiveEmailTaskReminders
        receiveSmsTaskAssignments
        receiveEmailTaskAssignments
      }
    }
  }
`;