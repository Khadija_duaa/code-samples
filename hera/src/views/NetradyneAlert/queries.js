export const staffNetradyneAlertsByAlertsByGroupInDateRange = /* GraphQL */ `
  query StaffNetradyneAlertsByAlertsByGroupInDateRange(
    $group: String
    $timestamp: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelStaffNetradyneAlertFilterInput
    $limit: Int
    $nextToken: String
  ) {
    staffNetradyneAlertsByAlertsByGroupInDateRange(
      group: $group
      timestamp: $timestamp
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        staffId
        matched
        matchedS
        driverName
        driverId
        groupName
        vehicleNumber
        alertId
        timestamp
        alertType
        alertSeverity
        description
        alertVideoStatus
        duration
        startLatLong
        endLatLong
        sortKey
        createdAt
        updatedAt
        staff {
          id
          group
          transporterId
          firstName
          lastName
          alternateNames
        }
      }
      nextToken
    }
  }
`;