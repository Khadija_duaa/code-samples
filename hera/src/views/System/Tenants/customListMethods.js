import { createCustomLists, createOptionsCustomLists } from '@/graphql/mutations'
import * as customList from './optionsCustomList'
import { sortByStringProp } from '@/utilities/sortHelpers';
import { weeklyPerformanceMetrics, dailyPerformanceMetrics } from '@/utilities/defaultCoachingSettings';
import { getDefaultCoachingSetting } from '@/utilities/getDefaultCoachingSetting';

const vehicleRoleForRoutesOptionsCustomList = customList.vehicleRoleForRoutesOptions
const routeStatusOptionsCustomList = customList.routeStatusOptions
const routeTypeOptionsCustomList = customList.routeTypeOptions
const incidentTypeOptionsCustomList = customList.incidentTypeOptions
const vehicleTypeOptionsCustomList = customList.vehicleTypeOptions
const counselingSeverityCustomList = customList.counselingSeverityOptions
const issueTypeOptionsCustomList = customList.issueTypeOptions
const kudoTypeOptionsCustomList = customList.kudoTypeOptions

  export const createVehicleRoleForRouteCustomList = async (group, api) => {
    const inputCreateCustomList = {
      type: 'vehicle-role-for-route',
      listCategory: 'Daily Rostering',
      listName: 'Vehicle Role For Route',
      listDisplay: 'Status Pills',
      group,
      canDeleteAllOptions: true
    }

    const resultCustomList = await api(createCustomLists, {input: inputCreateCustomList})

    for (const obj of vehicleRoleForRoutesOptionsCustomList) {
      const inputCreateOptionCustomList = {
        ...obj,
        group: resultCustomList.data.createCustomLists.group,
        optionsCustomListsCustomListsId: resultCustomList.data.createCustomLists.id
      }

      await api(createOptionsCustomLists, {input: inputCreateOptionCustomList})
    }
  }

  export const createRouteStatusCustomList = async (group, api) => {
    const inputCreateCustomList = {
      type: 'route-status',
      listCategory: 'Daily Rostering',
      listName: 'Route Status',
      listDisplay: 'Status Pills',
      group,
      canDeleteAllOptions: true
    }

    const resultCustomList = await api(createCustomLists, {input: inputCreateCustomList})

    for (const obj of routeStatusOptionsCustomList) {
      const inputCreateOptionCustomList = {
        ...obj,
        group: resultCustomList.data.createCustomLists.group,
        optionsCustomListsCustomListsId: resultCustomList.data.createCustomLists.id
      }

      await api(createOptionsCustomLists, {input: inputCreateOptionCustomList})
    }
  }

  export const createParkingSpaceCustomList = async (group, api) => {
    const inputCreateCustomList = {
      type: 'parking-space',
      listCategory: 'Daily Rostering',
      listName: 'Parking Space',
      listDisplay: 'Text Only',
      group,
      canDeleteAllOptions: true
    }

    await api(createCustomLists, {input: inputCreateCustomList})
  }

  export const createRouteTypeCustomList = async (group, api) => {
    const inputCreateCustomList = {
      type: 'route-type',
      listCategory: 'Daily Rostering',
      listName: 'Route Type',
      listDisplay: 'Text Only',
      group,
      canDeleteAllOptions: true
    }

    const resultCustomList = await api(createCustomLists, {input: inputCreateCustomList})

    for (const obj of routeTypeOptionsCustomList) {
      const inputCreateOptionCustomList = {
        ...obj,
        group: resultCustomList.data.createCustomLists.group,
        optionsCustomListsCustomListsId: resultCustomList.data.createCustomLists.id
      }

      await api(createOptionsCustomLists, {input: inputCreateOptionCustomList})
    }
  }

  export const createIncidentTypeCustomList = async (group, api) => {

    try {
      const input = {
        type: 'incident-type',
        listCategory: 'Associates',
        listName: 'Incident Type',
        listDisplay: 'Text Only',
        group,
        canDeleteAllOptions: true
      }
      const resultCustomList = await api(createCustomLists, { input })

      for (const item of incidentTypeOptionsCustomList) {
        const option = {
          ...item,
          group,
          optionsCustomListsCustomListsId: resultCustomList.data.createCustomLists.id
        }
        await api(createOptionsCustomLists, { input: option })
      }
    } catch (e) {
      throw e
    }
  }

  export const createDockDoorCustomList = async (group, api) => {
    const inputCreateCustomList = {
      type: 'dock-door',
      listCategory: 'Daily Rostering',
      listName: 'Dock Door',
      listDisplay: 'Text Only',
      group,
      canDeleteAllOptions: true
    }

    await api(createCustomLists, {input: inputCreateCustomList})
  }

  export const createVehicleTypeCustomList = async (group, api) => {

    try {
      const input = {
        type: 'vehicle-type',
        listCategory: 'Daily Rostering',
        listName: 'Vehicle Type',
        listDisplay: 'Text Only',
        group,
        canDeleteAllOptions: true
      }

      const resultCustomList = await api(createCustomLists, { input })
  
      for (const item of vehicleTypeOptionsCustomList) {
        const option = {
          ...item,
          group,
          optionsCustomListsCustomListsId: resultCustomList.data.createCustomLists.id
        }
  
        await api(createOptionsCustomLists, { input: option })
      }
      
    } catch (e) {
      throw e
    }
  }

  export const createCounselingSeverityCustomList = async (group, api) => {
    const inputCreateCustomList = {
      type: 'counseling-severity',
      listCategory: 'Associates',
      listName: 'Counseling Severity',
      listDisplay: 'Text Only',
      group,
      canDeleteAllOptions: false
    }

    const resultCustomList = await api(createCustomLists, {input: inputCreateCustomList})

    for (const obj of counselingSeverityCustomList) {
      const inputCreateOptionCustomList = {
        ...obj,
        group: resultCustomList.data.createCustomLists.group,
        optionsCustomListsCustomListsId: resultCustomList.data.createCustomLists.id
      }

      await api(createOptionsCustomLists, {input: inputCreateOptionCustomList})
    }
  }

export const createIssueTypeCustomList = async (group, api) => {
  const inputCreateCustomList = {
    type: 'issue-type',
    listCategory: 'Performance & Coaching',
    listName: 'Associate Issue Type',
    listDisplay: 'Text Only',
    group,
    canDeleteAllOptions: false
  }
  const response = await api(createCustomLists, {input: inputCreateCustomList});

  const issueTypeOptionsSortedAlphabetically = issueTypeOptionsCustomList.sort(sortByStringProp('option'));
  const sliderDefaults = getDefaultCoachingSetting(weeklyPerformanceMetrics, dailyPerformanceMetrics)
  let order = 1;
  for(const item of issueTypeOptionsSortedAlphabetically){
    const optionName = item.option.replace(/ /g, '').replace(/-/g, '').replace(/\//g, '').replace(/®/g, '').replace(/,/g, '')
    const defaultsValues = sliderDefaults[optionName]
    const driverReportSetting = defaultsValues?.defaults.issueSlider || 0
    const inputCreateOptionCustomList = {
      ...item,
      order,
      driverReportSetting,
      group: response.data.createCustomLists.group,
      optionsCustomListsCustomListsId: response.data.createCustomLists.id
    }
    await api(createOptionsCustomLists, {input: inputCreateOptionCustomList});
    order++;
  }
}


export const createKudoTypeCustomList = async (group, api) => {
  
  const inputCreateCustomList = {
    type: 'kudo-type',
    listCategory: 'Performance & Coaching',
    listName: 'Associate Kudo Type',
    listDisplay: 'Text Only',
    group,
    canDeleteAllOptions: false
  }
  const response = await api(createCustomLists, {input: inputCreateCustomList});
  
  const kudoTypeOptionsSortedAlphabetically = kudoTypeOptionsCustomList.sort(sortByStringProp('option'));
  const sliderDefaults = getDefaultCoachingSetting(weeklyPerformanceMetrics, dailyPerformanceMetrics)
  let order = 1;
  for(const item of kudoTypeOptionsSortedAlphabetically){
    const optionName = item.option.replace(/ /g, '').replace(/-/g, '').replace(/\//g, '').replace(/®/g, '').replace(/,/g, '')
    const defaultsValues = sliderDefaults[optionName]
    const driverReportSetting = defaultsValues?.defaults.kudoSlider || 0

    const inputCreateOptionCustomList = {
      ...item,
      order,
      driverReportSetting,
      group: response.data.createCustomLists.group,
      optionsCustomListsCustomListsId: response.data.createCustomLists.id
    }
    await api(createOptionsCustomLists, {input: inputCreateOptionCustomList});
    order++;
  }
}
