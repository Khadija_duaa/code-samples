const setTenantStatus = (accountPremiumStatus, trialExpDate) => {
	let plans = []
	let status = 'Unknown'

	for (let plan of accountPremiumStatus) {
	  plans.push(plan.toLowerCase());
	}

	if (plans.includes('trial')) {
	  const date = new Date()
	  if( new Date(trialExpDate) < date && trialExpDate !== null) {
		status = 'Lapsed Trial'
	  }
	  else{
		status = 'Trial'
	  }
	}

	if (plans.includes('bundle')) {
	  status = 'Active - Bundle'
	}

	if (plans.includes('standard')) {
	  if (plans.length > 1) {
		status = 'Active - Premium'
	  }
	  else{
		status = 'Active - Standard'
	  }
	}

	if (plans.includes('none')) {
	  status = 'Churned'
	}

	return status
}
 
module.exports = {
	setTenantStatus
}