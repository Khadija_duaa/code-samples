export const podQualitysByGroup = /* GraphQL */ `
  query PodQualitysByGroup(
    $group: String
    $yearWeek: ModelPodQualityByGroupCompositeKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelPodQualityFilterInput
    $limit: Int
    $nextToken: String
  ) {
    podQualitysByGroup(
      group: $group
      yearWeek: $yearWeek
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        matched
        week
        year
        employeeName
        transporterId
        blurry
        explicit
        mailSlot
        noPackage
        other
        packageTooClose
        personInPhoto
        takenFromCar
        grandTotal
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;

export const podQualityReportsByGroupUnmatched = /* GraphQL */ `
  query PodQualityReportsByGroupUnmatched(
    $group: String
    $matchedSYearWeek: ModelPodQualityByGroupUnmatchedCompositeKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelPodQualityFilterInput
    $limit: Int
    $nextToken: String
  ) {
    podQualityReportsByGroupUnmatched(
      group: $group
      matchedSYearWeek: $matchedSYearWeek
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        matched
        matchedS
        week
        year
        employeeName
        transporterId
        blurry
        explicit
        mailSlot
        noPackage
        other
        opportunities
        success
        bypass
        packageInHand
        notClearlyVisible
        packageTooClose
        personInPhoto
        photoTooDark
        takenFromCar
        grandTotal
        updatedAt
      }
      nextToken
    }
  }
`;

export const podQualityReportsByGroupAndTransporterId = /* GraphQL */ `
  query PodQualityReportsByGroupAndTransporterId(
    $group: String
    $transporterId: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelPodQualityFilterInput
    $limit: Int
    $nextToken: String
  ) {
    podQualityReportsByGroupAndTransporterId(
      group: $group
      transporterId: $transporterId
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        week
        year
        updatedAt
      }
      nextToken
    }
  }
`;

export const staffIndex = /* GraphQL */ `
query StaffsByGroup(
  $group: String
  $firstName: ModelStringKeyConditionInput
  $sortDirection: ModelSortDirection
  $filter: ModelStaffFilterInput
  $limit: Int
  $nextToken: String
) {
  staffsByGroup(
    group: $group
    firstName: $firstName
    sortDirection: $sortDirection
    filter: $filter
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      group
      transporterId
      firstName
      lastName
    }
    nextToken
  }
}
`;