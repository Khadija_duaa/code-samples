export const kudosByGroupAndDate = /* GraphQL */ `
  query KudosByGroupAndDate(
    $group: String
    $date: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelKudoFilterInput
    $limit: Int
    $nextToken: String
  ) {
    kudosByGroupAndDate(
      group: $group
      date: $date
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        kudoType
        date
        notes
        updatedAt
        staff {
          id
          firstName
          lastName
        }
        type {
          id
          option
        }
      }
      nextToken
    }
  }
`;

export const kudosByStaff = /* GraphQL */ `
  query KudosByStaff(
    $staffId: String
    $date: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelKudoFilterInput
    $limit: Int
    $nextToken: String
  ) {
    kudosByStaff(
      staffId: $staffId
      date: $date
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        kudoType
        notes
        date
        updatedAt
        staff {
          id
          firstName
          lastName
        }
        type {
          id
          option
        }
      }
      nextToken
    }
  }
`;

export const getTenantSendTime = /* GraphQL */ `
  query GetTenant($id: ID!) {
    getTenant(id: $id) {
      automatedCoachingSendTime
      timeZone
    }
  }
`;