import moment from 'moment'
import { API, graphqlOperation } from 'aws-amplify';
import { workBookFromFile }  from '@/../amplify/backend/function/ParsePerformanceData/src/util.js'
import { parseEOC }  from '@/../amplify/backend/function/ParsePerformanceData/src/dailyPerformance/parseHelper'
import { getLastDayOfWeekFromColumnNames, getDaysWeekFromColumnName }  from '@/../amplify/backend/function/ProcessPerformanceData/src/dailyPerformance/utils'
import { eocScoresByGroupLevelAndDate } from '@/views/Performance&Coaching/Performance/components/DashboardPerformanceDataImports/queries.js';
import store from '@/store/index'
import { shortDateFormat } from '@/../amplify/backend/function/ProcessPerformanceData/src/util.js'

//Patterns to identify file type and get dates from filename - for Mentor, Netradyne, and EOC files.
export const fileTypes = [
    {
        name: "Mentor",
        shortName: "mentor",
        storage: "mentorData",
        domClass: "text-orange-600 bg-orange-500 bg-opacity-10",
        fileNamePatterns: [
            "([\( \)0-9-a-z]*)?Daily[-| |_]?[0-9]{4}[-| |_]?[0-9]{2}[-| |_]?[0-9]{2}([\( \)0-9-_a-z\.]*)?csv"
        ],
        datePatterns: [
            "(?<year>[0-9]{4})[-| |_]?(?<month>[0-9]{2})[-| |_]?(?<day>[0-9]{2})"
        ],
    },
    {
        name: "Weekly Mentor",
        shortName: "weeklyMentor",
        fileNamePatterns: [
            "([\( \)0-9-a-z]*)?Weekly[-| |_]?[0-9]{4}[-| |_]?[0-9]{2}[-| |_]?[0-9]{2}([\( \)0-9-_a-z\.]*)?csv"
        ],
    },
    {
        name: "EOC",
        shortName: "eoc",
        storage: "eocData",
        domClass: "text-purple-600 bg-orange-500 bg-opacity-10",
        fileNamePatterns: [
            "(([\( \)0-9-_a-z])*)?[0-9]{8}[_| |-]?EOC[_| |-]?Last[_| |-]?7[_| |-]?Days(([\( \)0-9-_a-z\.])*)?xlsx"
        ],
        datePatterns: [
            "(?<year>[0-9]{4})(?<month>[0-9]{2})(?<day>[0-9]{2})[_| |-]?EOC",
        ]
    },
    {
        name: "Netradyne",
        shortName: "netradyne",
        storage: "netradyneData",
        resultTitle: "Netradyne Alerts",
        domClass: "text-teal-600 bg-orange-500 bg-opacity-10",
        fileNamePatterns: [
            "([\( \)0-9-_a-z]{3,})?\([ ]?([A-Za-z])*[ ]?[0-9]{2}[ ]?[0-9]{4}[ ]?\)([\( \)0-9-_a-z\.]*)?xlsx",
        ],
        datePatterns: [
            "(?<month>([A-Za-z]){3,})[-| |_]?(?<day>[0-9]{2})[-| |_]?(?<year>[0-9]{4})",
        ]
    },
]

const getEOCScoresByGroup = async function ( initialDate, endDate ) {
    const initialDateTransform = `${initialDate}T00:00:00.000Z`;
    const endDateTransform = `${endDate}T00:00:00.000Z`;
    const input = {
        group: store.state.userInfo.tenant.group,
        levelDate: {
            between: [
                { level: 'dsp' ,date: initialDateTransform } ,
                { level: 'dsp', date: endDateTransform } 
            ]
        },
    }

    try {
        const results = await API.graphql( graphqlOperation( eocScoresByGroupLevelAndDate, input ));
        const { data: { eocScoresByGroupLevelAndDate: eocScoresRecords } = { items: [] } } = results;
        return eocScoresRecords.items;
    } catch (error) {
        throw error;
    }
}

const checkByExtensions = function(fileName){
    //Confirm a xlsx or csv was selected
    const fileMatch = fileName.match(/(xlsx|csv)/)
    if(!fileMatch){
        return
    }
    return true
}

const matchByPatterns = function(fileName){
    return fileTypes.find(p =>{
        return p['fileNamePatterns'].find(regexp => {
            return new RegExp(regexp,"ig").test(fileName)
        })
    })
}

export const getDetailsFromFile = async function(file){

    const fileName = file.name

    if(!checkByExtensions(fileName)){
        return
    }

    const match = matchByPatterns(fileName)
    if(!match){
        return
    }

    if(match.shortName == 'weeklyMentor'){
        return { type: match, error: true, errorMessage: "Hera cannot import the weekly Mentor file. Please download the daily Mentor file and reattempt the upload." }
    }

    const matchDate = match.datePatterns.map(m=>new RegExp(m,"g").exec(fileName))
    const matchGroups = matchDate.find(results => !!results)

    if(!matchGroups){
        return
    }

    let date
    let daystoImport = [];

    if(match.shortName === 'eoc'){ // Rule - in EOC files the date will be one day earlier than the filename indicates
        const sharedEocError = { type: match, error: true, errorMessage: "Error getting dates from " + fileName + " EOC file." }

        if(file.results){
            const fileColumns = file.results.dspLevel[0]
            const { theLastDate } = getLastDayOfWeekFromColumnNames(fileColumns)
            const datesToImport = getDaysWeekFromColumnName(fileColumns);
            const [ initialDate ] = datesToImport;
            const endDate = datesToImport[datesToImport.length - 1];
            const recordExisting = await getEOCScoresByGroup( initialDate, endDate );
            const datesExistingImported = recordExisting.map( record => record.date.split('T')[0]);
            const daysFinallyImported = datesToImport.filter( day => !datesExistingImported.includes( day ) );
            date = theLastDate;
            daystoImport = daysFinallyImported;
        }else{
            try {    
                let workbook = await workBookFromFile(file)
                const eocResults = parseEOC(workbook)
                const fileColumns = eocResults.dspLevel[0]
                const { theLastDate } = getLastDayOfWeekFromColumnNames(fileColumns)
                const datesToImport = getDaysWeekFromColumnName(fileColumns);
                const [ initialDate ] = datesToImport;
                const endDate = datesToImport[datesToImport.length - 1];
                const recordExisting = await getEOCScoresByGroup( initialDate, endDate );
                const datesExistingImported = recordExisting.map( record => record.date.split('T')[0]);
                
                const daysFinallyImported = datesToImport.filter( day => !datesExistingImported.includes( day ) );
                date = theLastDate
                daystoImport = daysFinallyImported;
            } catch (error) {
                return sharedEocError
            }
        }
    }else{
        let {groups: {day, month, year}} = matchGroups
        month = parseInt(month) ? month : moment().month(month).format("M")
        date = new Date(year, month - 1, day)
        daystoImport = [formatDate(date)]
    }
    const dateString = summaryDaysToImport( daystoImport, match );
    return { type: match, date, year: date.getFullYear(),week: date.getWeekNumber(), dateString, daystoImport }
}

const summaryDaysToImport = ( daysToImport, match ) => {
    let summaryImport = '';
    if ( daysToImport.length === 0 ) {
        summaryImport = "You already imported this file";
    } else if ( daysToImport.length === 1 ) {
        summaryImport = `Importing ${match.name} data for ${ shortDateFormat( daysToImport[0], "en-US", { timeZone: 'UTC',year: 'numeric', month: 'long', day: 'numeric' } ) }`;
    } else {
        summaryImport = `Importing ${match.name} data for multiples dates`;
    }
    return summaryImport;
}
const formatDate = (date) => {
    return `${date.getFullYear()}-${(date.getMonth() + 1).toString().padStart(2, '0')}-${date.getDate().toString().padStart(2, '0')}`
}

export default { getDetailsFromFile, fileTypes }
