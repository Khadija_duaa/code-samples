export const textractJobsByGroupAndDate = /* GraphQL */ `
  query TextractJobsByGroupAndDate(
    $group: String
    $date: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelTextractJobFilterInput
    $limit: Int
    $nextToken: String
  ) {
    textractJobsByGroupAndDate(
      group: $group
      date: $date
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        key
        week
        year
        type
        date
        jobStatus
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;

export const textractJobsByGroupAndWeek = /* GraphQL */ `
  query TextractJobsByGroupAndWeek(
    $group: String
    $week: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelTextractJobFilterInput
    $limit: Int
    $nextToken: String
  ) {
    textractJobsByGroupAndWeek(
      group: $group
      week: $week
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        key
        week
        year
        type
        date
        isProcessedS
        jobStatus
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;

export const companyScoreCardsByGroup = /* GraphQL */ `
  query CompanyScoreCardsByGroup(
    $group: String
    $yearWeek: ModelIntKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelCompanyScoreCardFilterInput
    $limit: Int
    $nextToken: String
  ) {
    companyScoreCardsByGroup(
      group: $group
      yearWeek: $yearWeek
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        week
        year
        overall
        safetyAndCompliance
        team
        quality
        scorecardPdf
        podQualityPdf
        customerFeedbackPdf
        customerEscalationDefectDPMOText
        tenantId
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;

export const podQualitySummaryByGroupAndWeek = /* GraphQL */ `
  query PodQualitySummaryByGroupAndWeek(
    $group: String
    $week: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelPodQualitySummaryFilterInput
    $limit: Int
    $nextToken: String
  ) {
    podQualitySummaryByGroupAndWeek(
      group: $group
      week: $week
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        week
        year
        noPackageDetected
        packageNotClearlyVisible
        humanInThePicture
        textractJob {
          id
          group
          key
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;

export const cxFeedbackSummaryByGroupAndWeek = /* GraphQL */ `
  query CxFeedbackSummaryByGroupAndWeek(
    $group: String
    $week: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelCxFeedbackSummaryFilterInput
    $limit: Int
    $nextToken: String
  ) {
    cxFeedbackSummaryByGroupAndWeek(
      group: $group
      week: $week
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        week
        year
        deliveryWasGreat
        respectfulOfProperty
        followedInstructions
        textractJob {
          id
          group
          key
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;

export const eocScoresByGroupLevelAndDate = /* GraphQL */ `
  query EocScoresByGroupLevelAndDate(
    $group: String
    $levelDate: ModelEocScoreByGroupLevelAndDateCompositeKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelEocScoreFilterInput
    $limit: Int
    $nextToken: String
  ) {
    eocScoresByGroupLevelAndDate(
      group: $group
      levelDate: $levelDate
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        date
        level
        average
        averageDailyCompliance
        textractJobId
        createdAt
      }
      nextToken
    }
  }
`;

export const eocScoresByGroup = /* GraphQL */ `
  query EocScoresByGroup(
    $group: String
    $sortDirection: ModelSortDirection
    $filter: ModelEocScoreFilterInput
    $limit: Int
    $nextToken: String
  ) {
    eocScoresByGroup(
      group: $group
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        date
        level
        average
        averageDailyCompliance
        textractJobId
        createdAt
      }
      nextToken
    }
  }
`;


export const getTextractJob = /* GraphQL */ `
  query GetTextractJob($id: ID!) {
    getTextractJob(id: $id) {
      id
      group
      key
    }
  }
`;