export const dspMetricOptions = [
    {
        value: 'scorecardMetrics',
        label: 'SCORECARD METRICS',
        typeGraph: 'scorecard',
        typeFile: 'Weekly',
        levelChildren: 1,
        children:[
            { 
                value: 'safetyScore',
                label: 'On-Road Safety Score'
            },
            {
                value: 'safeDriving',
                label: 'Safe Driving Metric'
            },
            {
                value: 'seatbeltOff',
                label: 'Seatbelt-Off Rate'
            },
            {
                value: 'speedingEvent',
                label: 'Speeding Event'
            },
            {
                value: 'signSignalViolationsRate',
                label: 'Sign/Signal Violations Rate'
            },
            {
                value: 'distractionsRate',
                label: 'Distractions Rate'
            },
            {
                value: 'followingDistanceRate',
                label: 'Following Distance Rate'
            },
            {
                value: 'workingHoursCompliance',
                label: 'Working Hours Compliance'
            },
            {
                value: 'customerDeliveryExperience',
                label: 'Customer Delivery Experience'
            },
            {
                value: 'customerEscalationDefectDPMO',
                label: 'Customer Escalation Defect DPMO'
            },
            {
                value: 'customerDeliveryFeedback',
                label: 'Customer Delivery Feedback'
            },
            {
                value: 'deliveryCompletion',
                label: 'Delivery Completion Rate'
            },
            {
                value: 'deliveredAndReceived',
                label: 'Delivered and Received'
            },
            {
                value: 'standardWorkComplianceText',
                label: 'Standard Work Compliance'
            },
            {
                value: 'photoOnDelivery',
                label: 'Photo-On-Delivery'
            },
            {
                value: 'contactCompliance',
                label: 'Contact Compliance'
            },
            {
                value: 'attendedDeliveryAccuracy',
                label: 'Attended Delivery Accuracy'
            },
            {
                value: 'highPerformersShare',
                label: 'High Performers Share'
            },
            {
                value: 'lowPerformersShare',
                label: 'Low Performers Share'
            },
            {
                value: 'tenuredWorkforce',
                label: 'Tenured Workforce'
            }
        ]
    },
    {
        value: 'podMetrics',
        label: 'POD METRICS',
        typeGraph: 'pod',
        typeFile: 'Weekly',
        levelChildren: 2,
        children:[
            {
                value: 'podSummary',
                label: 'POD Summary',
                children: [
                    {
                        value: 'success',
                        label: 'Success',
                    },
                    {
                        value: 'bypass',
                        label: 'Bypass',
                    },
                    {
                        value: 'rejects',
                        label: 'Rejects',
                    },
                ]
            },
            { 
                value: 'rejectsCategoryBreakdown',
                label: 'Rejects Category Breakdown',
                children: [
                    {
                        value: 'noPackageDetected',
                        label: 'No Package Detected',
                    },
                    {
                        value: 'packageNotClearlyVisible',
                        label: 'Package Not Clearly Visible',
                    },
                    {
                        value: 'blurryPhoto',
                        label: 'Blurry Photo',
                    },
                    {
                        value: 'packageTooClose',
                        label: 'Package Too Close',
                    },
                    {
                        value: 'humanInThePicture',
                        label: 'Human In The Picture',
                    },
                    {
                        value: 'packageInHand',
                        label: 'Package in Hand',
                    },
                    {
                        value: 'photoTooDark',
                        label: 'Photo Too Dark',
                    },
                    {
                        value: 'other',
                        label: 'Other',
                    },
                    {
                        value: 'packageInCar',
                        label: 'Package In Car',
                    },
                ]
            }
        ]
    },
    {
        value: 'cxMetrics',
        label: 'CX METRICS',
        typeGraph: 'cxFeedback',
        typeFile: 'Weekly',
        levelChildren: 1,
        children:[
            { 
                value: 'dcfTier',
                label: 'DSP Customer Feedback Tier'
            },
            { 
                value: 'dcfScore',
                label: 'DSP Customer Feedback Score'
            },
            { 
                value: 'positiveFeedback',
                label: 'Total Positive Feedback (Delivery was Great)'
            },
            { 
                value: 'negativeFeedback',
                label: 'Total Negative Feedback (Delivery was not so Great)'
            },
            { 
                value: 'deliveriesWithoutCF',
                label: 'Deliveries without Customer Feedback'
            },
            {
                value: 'deliveryWasGreat',
                label: 'Delivery was Great (DA attributable)'
            },
            { 
                value: 'respectfulOfProperty',
                label: 'Respectful of Property'
            },
            { 
                value: 'followedInstructions',
                label: 'Followed Instructions'
            },
            { 
                value: 'friendly',
                label: 'Friendly'
            },
            { 
                value: 'aboveAndBeyond',
                label: 'Above and Beyond'
            },
            { 
                value: 'deliveredWithCare',
                label: 'Delivered with Care'
            },
            { 
                value: 'deliveryWasntGreat',
                label: 'Delivery was not so Great (DA attributable)'
            },
            { 
                value: 'mishandledPackage',
                label: 'Driver Mishandled Package'
            },
            { 
                value: 'driverUnprofessional',
                label: 'Driver was Unprofessional'
            },
            { 
                value: 'driverDidNotFollowDeliveryInstructions',
                label: 'Driver did not follow my Delivery instructions'
            },
            {
                value: 'onTime',
                label: 'On Time'
            },
            {
                value: 'lateDelivery',
                label: 'Late Delivery'
            },
            {
                value: 'itemDamaged',
                label: 'Item Damaged'
            },
            {
                value: 'deliveredToWrongAddress',
                label: 'Delivered to Wrong Address'
            },
            {
                value: 'neverReceivedDelivery',
                label: 'Never Received Delivery'
            }
        ]
    },
    
]

export const daMetricOptions = [
    {
        value: 'daScorecardMetrics',
        label: 'SCORECARD METRICS',
        typeGraph: 'daScorecard',
        typeFile: 'Weekly',
        levelChildren: 1,
        children:[
            { 
                value: 'overallTier',
                label: 'Overall Tier'
            },
            {
                value: 'delivered',
                label: 'Number of Delivered Packages'
            },
            {
                value: 'ficoScore',
                label: 'FICO'
            },
            {
                value: 'seatbeltOffRate',
                label: 'Seatbelt Off Rate'
            },
            {
                value: 'speedingEventRate',
                label: 'Speeding Event Rate'
            },
            {
                value: 'distractionsRate',
                label: 'Distractions Rate'
            },
            {
                value: 'followingDistanceRate',
                label: 'Following Distance Rate'
            },
            {
                value: 'signSignalViolationsRate',
                label: 'Sign/Signal Violations Rate'
            },
            {
                value: 'cdf',
                label: 'CDF'
            },
            {
                value: 'ced',
                label: 'CED'
            },
            {
                value: 'dcr',
                label: 'DCR'
            },
            {
                value: 'dar',
                label: 'DAR'
            },
            {
                value: 'swcPod',
                label: 'SWC-POD'
            },
            {
                value: 'swcCc',
                label: 'SWC-CC'
            },
            {
                value: 'swcAd',
                label: 'SWC-AD'
            },
            {
                value: 'dnrs',
                label: 'DNRs'
            },
        ]
    },
    {
        value: 'daPodSummary',
        label: 'POD: Summary',
        typeGraph: 'daPod',
        typeFile: 'Weekly',
        levelChildren: 1,
        children:[
            {
                value: 'opportunities',
                label: 'Opportunities'
            },
            {
                value: 'success',
                label: 'Success',
            },
            {
                value: 'bypass',
                label: 'Bypass',
            },
            {
                value: 'grandTotal',
                label: 'Rejects',
            },
        ]
    },
    {
        value: 'daPodRCB',
        label: 'POD: Rejects Category Breakdown',
        typeGraph: 'daPod',
        typeFile: 'Weekly',
        levelChildren: 1,
        children: [
            {
                value: 'blurry',
                label: 'Blurry Photo',
            },
            {
                value: 'personInPhoto',
                label: 'Human In The Picture',
            },
            {
                value: 'noPackage',
                label: 'No Package Detected',
            },
            {
                value: 'takenFromCar',
                label: 'Package In Car',
            },
            {
                value: 'packageInHand',
                label: 'Package in Hand',
            },
            {
                value: 'notClearlyVisible',
                label: 'Package Not Clearly Visible - Locker/Other Concealment',
            },
            {
                value: 'packageTooClose',
                label: 'Package Too Close',
            },
            {
                value: 'photoTooDark',
                label: 'Photo Too Dark',
            },
            {
                value: 'other',
                label: 'Other',
            },
            
        ]
    },
    {
        value: 'daCxMetricsDS',
        label: 'CX Metrics: Demographic & Summary',
        typeGraph: 'daCxFeedback',
        typeFile: 'Weekly',
        levelChildren: 1,
        children: [
            { 
                value: 'daTier',
                label: 'DA Tier'
            },
            {
                value: 'cdfScore',
                label: 'CDF Score'
            },
        ]
    },
    {
        value: 'daCxMetricsDF',
        label: 'CX Metrics: Detailed Feedback (DA Attributable)',
        typeGraph: 'daCxFeedback',
        typeFile: 'Weekly',
        levelChildren: 1,
        children: [
            {
                value: 'noFeedback',
                label: 'No Feedback'
            },
            {
                value: 'deliveryWasGreat',
                label: 'Delivery Was Great'
            },
            {
                value: 'respectfulOfProperty',
                label: 'Respectful of Property'
            },
            {
                value: 'followedInstructions',
                label: 'Followed Instructions'
            },
            {
                value: 'friendly',
                label: 'Friendly'
            },
            {
                value: 'aboveAndBeyond',
                label: 'Above and Beyond'
            },
            {
                value: 'deliveredWithCare',
                label: 'Delivered with Care'
            },
            {
                value: 'deliveryWasntGreat',
                label: 'Delivery was not so Great'
            },
            {
                value: 'mishandledPackage',
                label: 'DA Mishandled Package'
            },
            {
                value: 'driverUnprofessional',
                label: 'DA was Unprofessional'
            },
            {
                value: 'notDeliveredToPreferredLocation',
                label: 'DA Did not follow my delivery Instructions'
            },
        ]
    },
    {
        value: 'daCxMetricsDFnotDA',
        label: 'CX Metrics: Detailed Feedback (Non DA Attributable)',
        typeGraph: 'daCxFeedback',
        typeFile: 'Weekly',
        levelChildren: 1,
        children:[
            {
                value: 'onTime',
                label: 'On Time'
            },
            {
                value: 'lateDelivery',
                label: 'Late Delivery'
            },
            {
                value: 'itemDamaged',
                label: 'Item Damaged'
            },
            {
                value: 'deliveredToWrongAddress',
                label: 'Delivered to Wrong Address'
            },
            {
                value: 'neverReceivedDelivery',
                label: 'Never Received Delivery'
            }
        ]
    },
    {
        value: 'daMentor',
        label: 'Mentor',
        typeGraph: 'daMentor',
        typeFile: 'Daily',
        levelChildren: 1,
        children:[
            {
                value: 'trips',
                label: 'Trip'
            },
            {
                value: 'geotabTrips',
                label: 'Geotab Trips'
            },
            {
                value: 'miles',
                label: 'Miles'
            },
            {
                value: 'fico',
                label: 'Fico'
            },
            {
                value: 'accel',
                label: 'Accel'
            },
            {
                value: 'braking',
                label: 'Braking'
            },
            {
                value: 'cornering',
                label: 'Cornering'
            },
            {
                value: 'speeding',
                label: 'Speeding'
            },
            {
                value: 'distraction',
                label: 'Distraction'
            },
            {
                value: 'seatbelt',
                label: 'Seatbelt'
            },
            {
                value: 'backUp',
                label: 'Back Up'
            },
            {
                value: 'sse',
                label: 'Sse (Scorecard Speeding Event)'
            },
            {
                value: 'mpg',
                label: 'Mpg'
            },
            {
                value: 'idling',
                label: 'Idling'
            },
            {
                value: 'engineOff',
                label: 'Engine Off'
            },
            {
                value: 'trainingAssigned',
                label: 'Training Assigned/Completed'
            },
        ]
    },
    {
        value: 'daNetradyne',
        label: 'Netradyne',
        typeGraph: 'netradyne',
        typeFile: 'Daily',
        levelChildren: 1,
        children:[
            {
                value: 'total',
                label: 'Total'
            },
            {
                value: 'severeAlerts',
                label: 'Severe'
            },
            {
                value: 'moderateAlerts',
                label: 'Moderate'
            },
        ]
    }
]
