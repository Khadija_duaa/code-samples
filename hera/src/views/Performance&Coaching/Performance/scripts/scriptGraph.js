import moment from 'moment'

function findLabelAndValue(options=[], value=""){
    return options.find(option => option.value === value)
}

export function selectedOptionData(selectedOption='', data=[]){
    let searchResult = {
        typeGraph: null,
        typeFile: null,
        value: null,
        label: null
    }
    for(let dspOption of data){
        const optionsChildren = dspOption.children
        const levelChildren = dspOption.levelChildren
        searchResult.typeGraph = dspOption.typeGraph
        searchResult.typeFile = dspOption.typeFile
        if(levelChildren === 1){
          const result = findLabelAndValue(optionsChildren, selectedOption)
          if(result){
            searchResult.label = result.label
            searchResult.value = result.value
            return searchResult
          }
        }else if(levelChildren === 2){
          for(let optc of optionsChildren){
            const result = findLabelAndValue(optc.children, selectedOption)
            if(result){
              searchResult.label = result.label
              searchResult.value = result.value
              return searchResult
            }
            
          }
        }
    }
}

function getMinAndMaxValue(array=[]){
    let minValue = 0
    let maxValue = 4
    let sortedArray = array.filter(value => { return !(value == null || value == undefined) }).sort((a, b) => a - b)

    if(sortedArray.length){
        minValue = Math.round(sortedArray[0] * .98)
        minValue = minValue < 0 ? 0 : minValue
        maxValue = Math.round(sortedArray.at(-1) * 1.01)
        maxValue = maxValue < 2 ? 2 : maxValue
    }
    return { minValue, maxValue }
}


function getValue(foundData, selectedOption){
    const stringValues = ['Fantastic Plus','Fantastic','Great','Fair','Poor']
    const scaleOfValues = {
        'Poor': 1,
        'Fair': 2,
        'Great': 3,
        'Fantastic': 4,
        'Fantastic Plus': 5,
    }
    let value = null
    let valueChanged = false

    if(stringValues.includes(foundData[selectedOption])){
        value = scaleOfValues[foundData[selectedOption]] || null
        valueChanged = true
    }else{
        const floatNumber = parseFloat(foundData[selectedOption])
        value = foundData[selectedOption] ? (isNaN(floatNumber) ? null : floatNumber) : null
    }
  
    if(!Object.keys(foundData).includes(selectedOption)) {
        value = null
    }

    return { value, valueChanged}
}


export function changeDataFromGraph(data=[], showWeeks=10, selectedData, selectedOption, dateStart, dateEnd, staffAverage=false, staffData=[], typeFile){
    let array = []
    let labels = []
    let staffAverageData= []
    let changeValue = false
    let averageValueChanged = false
    for (let index = 0; index < showWeeks; index++) {
        const date = moment(dateEnd).subtract(index,'weeks')
        let week = moment(date).format('ww')
        const yearWeek = moment(date).format('YYYYww')
        let itemData = null
        let itemStaffAverage = null
        const foundData = data.find(item => `${item.year}${item.week}` == yearWeek)
        if(foundData){
            const { value, valueChanged } = getValue(foundData, selectedOption)
            itemData = value
            changeValue = valueChanged
        }
       
        if(staffAverage){
            const result = calculateAverage(staffData, selectedData, selectedOption, typeFile, dateStart, dateEnd)
            const foundStaffAverage = result.find(item => `${item.year}${item.week}` == yearWeek)
            itemStaffAverage = foundStaffAverage?.average ?? null
            if(!!foundStaffAverage?.valueChanged){
                averageValueChanged = foundStaffAverage?.valueChanged ?? false
            } 
        }
        array[index] = itemData
        labels[index] = `Week ${week}`
        staffAverageData[index] = itemStaffAverage
    }
    
    const { minValue, maxValue } = getMinAndMaxValue(array)

    return {
        labels: labels.reverse(),
        label: selectedData.label,
        selectedOption,
        data: array.reverse(),
        staffAverage: staffAverageData.reverse(),
        yMinValue: minValue,
        yMaxValue: maxValue,
        changeValue: staffAverage ? averageValueChanged : changeValue
    }
}



export function changeDailyDataFromGraph(data=[], showWeeks=10, selectedData, selectedOption, dateStart, dateEnd, staffAverage=false, staffData=[], typeFile){
    let array = []
    let labels = []
    let staffAverageData= []
    let changeValue = false
    let averageValueChanged = false
    for (let index = 0; index < showWeeks; index++) {
        const date = moment(dateEnd).subtract(index,'days')
        let compareDate = moment.utc(date).format('YYYY-MM-DD')
        let itemData = null
        let itemStaffAverage = null
        const foundData = data.find(item => moment.utc(item.date).format('YYYY-MM-DD') === compareDate)
        if(foundData){
            const { value, valueChanged } = getValue(foundData, selectedOption)
            itemData = value
            changeValue = valueChanged
        }
        if(staffAverage){
            const result = calculateAverage(staffData, selectedData, selectedOption, typeFile, dateStart, dateEnd)
            const foundStaffAverage = result.find(item => item.date == compareDate)
            itemStaffAverage = foundStaffAverage?.average ?? null
            if(!!foundStaffAverage?.valueChanged){
                averageValueChanged = foundStaffAverage?.valueChanged ?? false
            }
        }

        array[index] = itemData
        labels[index] = moment(compareDate).format('MMM DD')
        staffAverageData[index] = itemStaffAverage
    }
    const { minValue, maxValue } = getMinAndMaxValue(array)

    return {
        labels: labels.reverse(),
        label: selectedData.label,
        selectedOption,
        data: array.reverse(),
        staffAverage: staffAverageData.reverse(),
        yMinValue: minValue,
        yMaxValue: maxValue,
        changeValue: staffAverage ? averageValueChanged : changeValue
    }
}

export function calculateAverage(staffData=[], selectedData, selectedOption, typeFile, dateStart, dateEnd){
   
    let data = []
    for(let staff of staffData){
        const selectData = {
            daScorecard: () => {
              data = data.concat(staff.scoreCards.items)
            },
            daPod: () => {
              data = data.concat(staff.podQualities.items)
            },
            daCxFeedback: () => {
              data = data.concat(staff.cxFeedback.items)
            },
            daMentor: () => {
              data = data.concat(staff.mentor.items)
            },
            netradyne:() => {
                data = data.concat(staff.netrdadyneAlerts.items)
            },
        }
        selectData[selectedData.typeGraph]()
    }
    
    if(selectedData.typeGraph==="netradyne"){
        data = calculateNetradyneValue(data)
    } 
    let result = []
    const prop = selectedOption
    if(typeFile === 'Weekly'){
        data = data.filter(item => {
            const date = moment(`${item?.year}${item?.week}`,'YYYYww').format('YYYY-MM-DD')
            return moment(date).isBetween(dateStart, dateEnd, undefined, '[]')
        })
        result = calculateAverageWeeklys(data, prop)
    }else{
        data = data.filter(item => {
            const date = moment.utc(item?.date).format('YYYY-MM-DD')
            return moment(date).isBetween(dateStart, dateEnd, undefined, '[]')
        })
        result = calculateAverageDailys(data, prop)
    }

    return result
   
}

function calculateAverageWeeklys(data=[], prop){
    let numberDecimal = 0
    let valueChangedFinal = false
    return data.reduce((acc, curr) => {
        //const { [prop]:value, week, year } = curr
        const { week, year } = curr
        const { value, valueChanged } = getValue(curr, prop)
        if(valueChanged){
            numberDecimal = valueChanged ? 0 : 2
            valueChangedFinal = valueChanged
        }
        const index = acc.findIndex(el => el.week === week && el.year === year)
        if (index === -1) {
            acc.push({ week, year, [prop]: [value] })
        } else {
            acc[index][prop].push(value)
        }
        return acc
    }, []).map(item => {
        let average = item[prop].reduce((acc, curr) => acc + curr, 0) / item[prop].length
        average = parseFloat(average.toFixed(numberDecimal))
    
        return {
          week: item.week,
          year: item.year,
          valueChanged: valueChangedFinal,
          average: average
        }
    })
}

function calculateAverageDailys(data=[], prop){
    let numberDecimal = 0
    let valueChangedFinal = false
    return data.reduce((acc, curr) => {
        //const { [prop]:value, date } = curr
        let { date } = curr
        date = moment.utc(date).format('YYYY-MM-DD')
        const { value, valueChanged } = getValue(curr, prop)
        if(valueChanged){
            numberDecimal = valueChanged ? 0 : 2
            valueChangedFinal = valueChanged
        }
        const index = acc.findIndex(el => moment.utc(el.date).format('YYYY-MM-DD') === date)
        if (index === -1) {
            acc.push({ date, [prop]: [value] })
        } else {
            acc[index][prop].push(value)
        }
        return acc
    }, []).map(item => {
        let average = item[prop].reduce((acc, curr) => acc + curr, 0) / item[prop].length
        average = parseFloat(average.toFixed(numberDecimal))

        return {
            date: item.date,
            valueChanged: valueChangedFinal,
            average: average
        }
    })
}


export function calculateNetradyneValue(data=[]){
    const netradyneData =  data.reduce((acc, alert) => {
      const { date, alertSeverity } = alert
      const existData = acc.find(item => item.date === date)
        if (existData) {
          if (alertSeverity.toLowerCase().includes('severe')) {
            existData.severeAlerts++
          }
          if (alertSeverity.toLowerCase().includes('moderate')) {
            existData.moderateAlerts++
          }
          existData.total = existData.severeAlerts + existData.moderateAlerts;
        } else {
          const newEntry = {
            date: date,
            severeAlerts: alertSeverity.toLowerCase().includes('severe') ? 1 : 0,
            moderateAlerts: alertSeverity.toLowerCase().includes('moderate') ? 1 : 0,
            total: 1,
          }
          acc.push(newEntry)
        }
        return acc
    }, [])
    return netradyneData
}