export const scorecardsByTenantAndYearWeek = /* GraphQL */ `
  query scorecardsByTenantAndYearWeek(
    $tenantId: ID
    $sortDirection: ModelSortDirection
    $limit: Int
    $nextToken: String
  ) {
    scorecardsByTenantAndYearWeek(
      tenantId: $tenantId
      sortDirection: $sortDirection
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        week
        year
        yearWeek
        overall
        safetyAndCompliance
        comprehensiveAudit
        safetyScore
        safeDriving
        safeDrivingText
        fico
        seatbeltOff
        seatbeltOffText
        speedingEvent
        speedingEventText
        dvcrCompliance
        dvcrComplianceText
        onTimeCompliance
        onTimeComplianceText
        complianceScoreText
        workingHoursCompliance
        workingHoursComplianceText
        dspAudit
        dspAuditText
        thirtyDaysNoticeText
        customerEscalationDefectDPMO
        customerEscalationDefectDPMOText
        team
        highPerformersShare
        highPerformersShareText
        lowPerformersShare
        lowPerformersShareText
        attritionRate
        attritionRateText
        quality
        deliveryCompletion
        deliveryCompletionText
        deliveredAndReceived
        deliveredAndReceivedText
        standardWorkComplianceText
        photoOnDelivery
        photoOnDeliveryText
        contactCompliance
        contactComplianceText
        scanCompliance
        scanComplianceText
        attendedDeliveryAccuracy
        attendedDeliveryAccuracyText
        distractionsRate
        followingDistanceRate
        signSignalViolationsRate
        distractionsRateText
        followingDistanceRateText
        signSignalViolationsRateText
        scorecardPdf
        podQualityPdf
        customerFeedbackPdf
      }
      nextToken
    }
  }
`;


export const listStaffs = /* GraphQL */ `
  query StaffsByGroup(
    $group: String
    $firstName: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelStaffFilterInput
    $limit: Int
    $nextToken: String
  ) {
    staffsByGroup(
      group: $group
      firstName: $firstName
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        firstName
        lastName
        status
        latestScorecard 
      } 
        nextToken
    }
  }
`;

export const cxFeedbackSummaryByGroupAndYearWeek = /* GraphQL */ `
  query CxFeedbackSummaryByGroupAndYearWeek(
    $group: String
    $yearWeek: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelCxFeedbackSummaryFilterInput
    $limit: Int
    $nextToken: String
  ) {
    cxFeedbackSummaryByGroupAndYearWeek(
      group: $group
      yearWeek: $yearWeek
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        week
        year
        yearWeek
        dcfTier
        dcfScore
        positiveFeedback
        negativeFeedback
        deliveriesWithoutCF
        deliveryWasGreat
        respectfulOfProperty
        followedInstructions
        friendly
        aboveAndBeyond
        deliveredWithCare
        deliveryWasntGreat
        mishandledPackage
        driverUnprofessional
        driverDidNotFollowDeliveryInstructions
        onTime
        lateDelivery
        itemDamaged
        deliveredToWrongAddress
        neverReceivedDelivery
        textractJob {
          id
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;

export const podQualitySummaryByGroupAndYearWeek = /* GraphQL */ `
  query PodQualitySummaryByGroupAndYearWeek(
    $group: String
    $yearWeek: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelPodQualitySummaryFilterInput
    $limit: Int
    $nextToken: String
  ) {
    podQualitySummaryByGroupAndYearWeek(
      group: $group
      yearWeek: $yearWeek
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        week
        year
        yearWeek
        success
        bypass
        rejects
        opportunities
        noPackageDetected
        packageNotClearlyVisible
        blurryPhoto
        packageTooClose
        humanInThePicture
        packageInHand
        photoTooDark
        packageInCar
        other
        grandTotal
        textractJob {
          id
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;


export const staffsByGroupStatus = /* GraphQL */ `
  query StaffsByGroupStatus(
    $group: String
    $status: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelStaffFilterInput
    $limit: Int
    $nextToken: String
  ) {
    staffsByGroupStatus(
      group: $group
      status: $status
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group 
        firstName
        lastName  
        status
        createdAt
        updatedAt 
        scoreCards(limit: 106) {
          items {
            id
            group
            matched
            matchedS
            week
            year
            name
            transporterId
            overallTier
            delivered
            keyFocusArea
            ficoScore
            seatbeltOffRate
            cdf
            ced
            dcr
            dar
            swcPod
            swcCc
            swcSc
            swcAd
            dnrs
            podOpps
            ccOpps
            speedingEventRate
            distractionsRate
            followingDistanceRate
            signSignalViolationsRate
            createdAt
            updatedAt
          }
          nextToken
        }
        mentor(limit: 730) {
          items {
            id
            group
            name
            date
            station
            trips
            miles
            time
            fico
            accel
            braking
            cornering
            speeding
            distraction
            geotabTrips
            seatbelt
            backUp
            sse
            mpg
            idling
            engineOff
            preDvcr
            postDvcr
            trainingAssigned
            trainingCompleted
            createdAt
            updatedAt
          }
          nextToken
        }
        cxFeedback(limit: 106) {
          items {
            id
            group
            matched
            matchedS
            week
            year
            name
            messageHasBeenSent
            transporterId
            positiveFeedback
            negativeFeedback
            deliveryWasGreat
            deliveryWasntGreat
            totalDeliveries
            respectfulOfProperty
            followedInstructions
            friendly
            aboveAndBeyond
            deliveredWithCare
            careForOthers
            mishandledPackage
            drivingUnsafely
            driverUnprofessional
            notDeliveredToPreferredLocation
            noFeedback
            onTime
            lateDelivery
            itemDamaged
            deliveredToWrongAddress
            neverReceivedDelivery
            daTier
            cdfScore
            createdAt
            updatedAt
            
          }
          nextToken
        }
        podQualities(limit: 106) {
          items {
            id
            group
            matched
            matchedS
            week
            year
            employeeName
            transporterId
            blurry
            explicit
            mailSlot
            noPackage
            other
            opportunities
            success
            bypass
            packageInHand
            notClearlyVisible
            packageTooClose
            personInPhoto
            photoTooDark
            takenFromCar
            grandTotal
            createdAt
            updatedAt
          }
          nextToken
        }
        netrdadyneAlerts(limit: 730) {
          items {
            id
            group
            staffId
            matched
            matchedS
            driverName
            driverId
            groupName
            vehicleNumber
            alertId
            timestamp
            alertType
            alertSeverity
            description
            alertVideoStatus
            duration
            startLatLong
            endLatLong
            sortKey
            date
            textractJobId
            createdAt
            updatedAt
          }
          nextToken
        }
      }
      nextToken
    }
  }
`;



export const companyScoreCardsByGroup = /* GraphQL */ `
  query CompanyScoreCardsByGroup(
    $group: String
    $yearWeek: ModelIntKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelCompanyScoreCardFilterInput
    $limit: Int
    $nextToken: String
  ) {
    companyScoreCardsByGroup(
      group: $group
      yearWeek: $yearWeek
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        week
        year
        yearWeek
        overall
        safetyAndCompliance
        comprehensiveAudit
        safetyScore
        safeDriving
        safeDrivingText
        fico
        seatbeltOff
        seatbeltOffText
        speedingEvent
        speedingEventText
        dvcrCompliance
        dvcrComplianceText
        onTimeCompliance
        onTimeComplianceText
        complianceScoreText
        workingHoursCompliance
        workingHoursComplianceText
        dspAudit
        dspAuditText
        thirtyDaysNoticeText
        customerEscalationDefectDPMO
        customerEscalationDefectDPMOText
        team
        highPerformersShare
        highPerformersShareText
        lowPerformersShare
        lowPerformersShareText
        attritionRate
        attritionRateText
        quality
        deliveryCompletion
        deliveryCompletionText
        deliveredAndReceived
        deliveredAndReceivedText
        standardWorkComplianceText
        photoOnDelivery
        photoOnDeliveryText
        contactCompliance
        contactComplianceText
        scanCompliance
        scanComplianceText
        attendedDeliveryAccuracy
        attendedDeliveryAccuracyText
        distractionsRate
        followingDistanceRate
        signSignalViolationsRate
        distractionsRateText
        followingDistanceRateText
        signSignalViolationsRateText
        scorecardPdf
        podQualityPdf
        customerFeedbackPdf
        harshBrakingEvent
        harshCorneringEvent
        harshBrakingEventText
        harshCorneringEventText
        deliverySlotPerformance
        deliverySlotPerformanceText
        tenantId
        customerDeliveryExperience
        customerDeliveryFeedback
        tenuredWorkforce
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;