export const staffsByGroup = /* GraphQL */ `
query StaffsByGroup(
  $group: String
  $sortDirection: ModelSortDirection
  $filter: ModelStaffFilterInput
  $limit: Int
  $nextToken: String
  $scoreCardSortDirection: ModelSortDirection
  $scoreCardFilter: ModelStaffScoreCardFilterInput
) {
  staffsByGroup(
    group: $group
    sortDirection: $sortDirection
    filter: $filter
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      firstName
      lastName
      scoreCards(
        filter: $scoreCardFilter
        sortDirection: $scoreCardSortDirection
      ){
        items{
          id
          week
          year
          name
          overallTier
          podOpps
          swcPod
          swcCc
          swcSc
          swcAd
          updatedAt
        }
      }
      status
      updatedAt
    }
    nextToken
  }
}
`;