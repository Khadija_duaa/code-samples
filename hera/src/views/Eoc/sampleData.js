export const sampleDataEocMetrics =  [
    {
        firstName: "Alexander",
        lastName: "Johnson",
        status: "Active",
        rank: 1,
        average: "99.2%",
        averageDailyCompliance: "99.2%",
        previousAverage: "98.2%"
    },
    {
        firstName: "Carlos",
        lastName: "Taylor",
        status: "Active",
        rank: 2,     
        average: "98.1%",
        averageDailyCompliance: "99.1%",
        previousAverage: "98.1%"
    },
    {
        firstName: "Anthony",
        lastName: "Smith",
        status: "Active",
        rank: 3,
        average: "97.1%",
        averageDailyCompliance: "96.1%",
        previousAverage: "97.1%"
    },
    {
        firstName: "Henry",
        lastName: "Wilson",
        status: "Active",
        rank: 4,    
        average: "96.2%",
        averageDailyCompliance: "96.2%",
        previousAverage: "96.2%"
    },
    {
        firstName: "Carlos",
        lastName: "Williams",
        status: "Active",
        rank: 5,
        average: "95.1%",
        averageDailyCompliance: "95.1%",
        previousAverage: "95.1%"
    },
    {
        firstName: "Anthony",
        lastName: "Jones",
        status: "Active",
        rank: 6,
        average: "94.3%",
        averageDailyCompliance: "94.3%",
        previousAverage: "94.3%"
    },
    {
        firstName: "Brian",
        lastName: "Miller",
        status: "Active",
        rank: 7,        
        average: "93.5%",
        averageDailyCompliance: "93.5%",
        previousAverage: "93.5%"
    },
    {
        firstName: "Anthony",
        lastName: "Johnson",
        status: "Active",
        rank: 8,
        average: "93.2%",
        averageDailyCompliance: "93.2%",
        previousAverage: "93.2%"
    }
]