export const eocScoresByGroupLevelAndDate = /* GraphQL */ `
  query EocScoresByGroupLevelAndDate(
    $group: String
    $levelDate: ModelEocScoreByGroupLevelAndDateCompositeKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelEocScoreFilterInput
    $limit: Int
    $nextToken: String
  ) {
    eocScoresByGroupLevelAndDate(
      group: $group
      levelDate: $levelDate
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        date
        level
        average
        averageDailyCompliance
        textractJobId
        createdAt
        updatedAt
        staff {
          id
          firstName
          lastName
          status
        }
      }
      nextToken
    }
  }
`;

export const eocScoresByGroupLevelDSPAndDate = /* GraphQL */ `
  query EocScoresByGroupLevelAndDate(
    $group: String
    $levelDate: ModelEocScoreByGroupLevelAndDateCompositeKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelEocScoreFilterInput
    $limit: Int
    $nextToken: String
  ) {
    eocScoresByGroupLevelAndDate(
      group: $group
      levelDate: $levelDate
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        date
        level
      }
      nextToken
    }
  }
`;

export const staffsByGroupStatus = /* GraphQL */ `
  query StaffsByGroupStatus(
    $group: String
    $status: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelStaffFilterInput
    $limit: Int
    $nextToken: String
  ) {
    staffsByGroupStatus(
      group: $group
      status: $status
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        status
      }
      nextToken
    }
  }
`;