const rosterColumns = [
    {
        // id: 0,
        order: 0,
        label: "Delivery Associate",
        isChecked: true,
        list: 1,
        fixed: true,
        value: "Delivery-Associate"
    },
    {
        // id: 1,
        order: 1,
        label: "Wave Time",
        isChecked: false,
        list: 2,
        fixed: false,
        value: "Wave-Time"
    },
    {
        // id: 2,
        order: 2,
        label: "Vehicle",
        isChecked: false,
        list: 2,
        fixed: false,
        value: "Vehicle"
    },
    {
        // id: 3,
        order: 3,
        label: "Device",
        isChecked: false,
        list: 2,
        fixed: false,
        value: "Device"
    },
    {
        // id: 4,
        order: 4,
        label: "Route & Staging",
        isChecked: false,
        list: 2,
        fixed: false,
        value: "Route-Staging"
    },
    {
        // id: 5,
        order: 5,
        label: "Parking",
        isChecked: false,
        list: 2,
        fixed: false,
        value: "Parking"
    },
    {
        // id: 6,
        order: 6,
        label: "Inspection - Notes",
        isChecked: false,
        list: 2,
        fixed: false,
        value: "Inspection-Notes"
    },
    {
        // id: 7,
        order: 7,
        label: "Notes",
        isChecked: false,
        list: 2,
        fixed: false,
        value: "Notes"
    },
    {
        // id: 8,
        order: 8,
        label: "Completed Packages",
        isChecked: false,
        list: 2,
        fixed: false,
        value: "Completed-Packages"
    },
    {
        // id: 9,
        order: 9,
        label: "Undelivered packages - Total",
        isChecked: false,
        list: 2,
        fixed: false,
        value: "Undelivered-packages-Total"
    },
    {
        // id: 10,
        order: 10,
        label: "Undelivered packages - Business Closed",
        isChecked: false,
        list: 2,
        fixed: false,
        value: "Undelivered-packages-Business-Closed"
    },
    {
        // id: 11,
        order: 11,
        label: "Undelivered packages - Unable to Access",
        isChecked: false,
        list: 2,
        fixed: false,
        value: "Undelivered-packages-Unable-to-Access"
    },
    {
        // id: 12,
        order: 12,
        label: "Pick up Returned Packages",
        isChecked: false,
        list: 2,
        fixed: false,
        value: "Pick-up-Returned-Packages"
    },
    {
        // id: 13,
        order: 13,
        label: "First Stop Time",
        isChecked: false,
        list: 2,
        fixed: false,
        value: "First-Stop-Time"
    },
    {
        // id: 14,
        order: 14,
        label: "Last Stop Time",
        isChecked: false,
        list: 2,
        fixed: false,
        value: "Last-Stop-Time"
    },
    {
        // id: 15,
        order: 15,
        label: "DA Work Start Time",
        isChecked: false,
        list: 2,
        fixed: false,
        value: "DA-Work-Start-Time"
    },
    {
        // id: 16,
        order: 16,
        label: "DA Work End Time",
        isChecked: false,
        list: 2,
        fixed: false,
        value: "DA-Work-End-Time"
    },
    {
        // id: 17,
        order: 17,
        label: "Total Stops",
        isChecked: false,
        list: 2,
        fixed: false,
        value: "Total-Stops"
    },
    {
        // id: 18,
        order: 18,
        label: "Lunch Start time",
        isChecked: false,
        list: 2,
        fixed: false,
        value: "Lunch-Start-Time"
    },
    {
        // id: 19,
        order: 19,
        label: "Lunch End Time",
        isChecked: false,
        list: 2,
        fixed: false,
        value: "Lunch-End-Time"
    },
    {
        // id: 20,
        order: 20,
        label: "Inspection - Fueled",
        isChecked: false,
        list: 2,
        fixed: false,
        value: "Inspection-Fueled"
    },
    {
        // id: 21,
        order: 21,
        label: "Inspection - Cleaned",
        isChecked: false,
        list: 2,
        fixed: false,
        value: "Inspection-Cleaned"
    },
    {
        // id: 22,
        order: 22,
        label: "Inspection - Flashers",
        isChecked: false,
        list: 2,
        fixed: false,
        value: "Inspection-Flashers"
    },
    {
        // id: 23,
        order: 23,
        label: "Inspection - Credit Card",
        isChecked: false,
        list: 2,
        fixed: false,
        value: "Inspection-Credit-Card"
    },
    {
        // id: 24,
        order: 24,
        label: "Inspection - Flashers",
        isChecked: false,
        list: 2,
        fixed: false,
        value: "Inspection-Flashers"
    },
    {
        // id: 25,
        order: 25,
        label: "Inspection - Keys",
        isChecked: false,
        list: 2,
        fixed: false,
        value: "Inspection-Keys"
    },
    {
        // id: 26,
        order: 26,
        label: "Inspection - Device Charger",
        isChecked: false,
        list: 2,
        fixed: false,
        value: "Inspection-Device-Charger"
    },
    {
        // id: 27,
        order: 27,
        label: "Inspection - Device Battery",
        isChecked: false,
        list: 2,
        fixed: false,
        value: "Inspection-Device-Battery"
    },
    {
        // id: 28,
        order: 28,
        label: "Status",
        isChecked: true,
        list: 1,
        fixed: true,
        value: "Status"
    },
];
const result = []

const getRosterColumns = (id, group) => {
    const data = {
        dailyRostersColumnsTenantId: id,
        group: group,
        columnStatus: true,
    }
    rosterColumns.forEach((column) => {
        let newColumn = { ...column, ...data };
        result.push(newColumn);
    })
    return result;
}

export default getRosterColumns;