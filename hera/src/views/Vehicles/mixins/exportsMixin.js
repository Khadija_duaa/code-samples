import { exportCSV } from '@/utilities/exportCSV';
import moment from 'moment';

/**
 * Handles the export of data
 * @param {Array} data - List of data that will be exported
 * @param {Array} tableColumns - List of columns being displayed
 * @param {String} fileName - Name of the file exported
 */
function exportData(data, tableColumns, fileName) {
    validateExportData(tableColumns, data);
    const mappedData = mapData(data)
    const arrData = getArrayDataToExport(tableColumns, mappedData);
    exportCSV({arrData, fileName});
}

/**
 * Transforms the data to be exported 
 * @param {Array} data - List of records to be exported
 * @returns {Array} Transformed list with all fields needed to export
 */
function mapData(data) {
    return data.map(item => ({
        ...item,
        date: formatDate(item.date),
        details: item.details.replace('•', '-'),
    }))
}

/**
 * Transforms a date in format UTC into MM_DD_YYYY
 * @param {String} date -> Date in string format UTC
 * @returns {String} Value of the transformed date
 */
function formatDate(date) {
    return moment(date, 'YYYY-MM-DD').format('MM/DD/YYYY')
}

/**
 * Maps a list of values into items that will be used for export
 * @param {Array} tableColumns -> List of columns that a CustomTable displays
 * @param {Array} dataList -> List of values being displayed in a CustomTable
 * @returns {Array} Transformed values that will be used o generate the exported file
 */
function getArrayDataToExport(tableColumns, dataList) {
    // Consider ONLY columns that have 'name' set
    const validTableColumns = tableColumns.filter(tableColumn => !!tableColumn.name);
    return dataList.map((item) =>
      validTableColumns.reduce((exportItem, tableColumn) => {
        exportItem[tableColumn.name] = item[tableColumn.col];
        return exportItem;
      }, {})
    );
}

/**
 * Validates if the requirements for export data are being fulfilled
 * @param {Array} tableColumns - List of columns being displayed
 * @param {Array} data - List of data that will be exported
 */
function validateExportData(tableColumns, data) {
    if (!tableColumns) throw new Error('"tableColumns" is required');
    if (!data) throw new Error('"data" is required');
    if (!tableColumns.length || !data.length) throw new Error('No data to export');
}

export default {
    methods: {
        $_exportsHandleDataExport(filteredTableData, tableColumns) {
            try {
                const fileName = 'vehicle-history-records.csv'   
                exportData(filteredTableData, tableColumns, fileName)
            } catch (error) {
                this.printUserError(error)
                this.displayUserError(error)
            }
        }
    }
}