export const getDocument = /* GraphQL */ `
  query GetDocument($id: ID!) {
    getDocument(id: $id) {
      id
      key
      type
      notes
      uploadDate
      documentDate      
    }
  }
`;

export const listDocuments = /* GraphQL */ `
  query ListDocuments(
    $filter: ModelDocumentFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listDocuments(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        key
        type
        notes
        uploadDate
        documentDate
      }
      nextToken
    }
  }
`;

export const getDailyLog = /* GraphQL */ `
  query GetDailyLog($id: ID!) {
    getDailyLog(id: $id) {
      id
      group
      type
      date
      notes
      vehicleId
      creationLinkSentDateTime
      takenByUser {
        id
        firstName
        lastName
      }
      takenByAssociate {
        id
        firstName
        lastName
      }
      rosteredDay {
        id
        notesDate
      }
      creationLinkSentByUser {
        id
        firstName
        lastName
      }
      creationLinkAssociate {
        id
        firstName
        lastName
      }
      documents {
        items {
          id
          group
          name
          key
          type
          uploadDate
          notes
          documentDate
          createdAt
          updatedAt
          takenByUser {
            id
            firstName
            lastName
          }
          takenByAssociate {
            id
            firstName
            lastName
          }
        }
        nextToken
      }
      shortenUrls {
        items {
          id
          isOpenLink
        }
        nextToken
      }
      history {
        items {
          id
          date
          previousValue
          currentValue
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;

export const dailyLogsByVehicleAndDate = /* GraphQL */ `
  query DailyLogsByVehicleAndDate(
    $vehicleId: String
    $date: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelDailyLogFilterInput
    $limit: Int
    $nextToken: String
  ) {
    dailyLogsByVehicleAndDate(
      vehicleId: $vehicleId
      date: $date
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        type
        date
        notes
        vehicleId
        creationLinkSentDateTime
        takenByUser {
          id
          firstName
          lastName
        }
        takenByAssociate {
          id
          firstName
          lastName
        }
        rosteredDay {
          id
          notesDate
        }
        creationLinkSentByUser {
          id
          firstName
          lastName
        }
        creationLinkAssociate {
          id
          firstName
          lastName
        }
        documents {
          items {
            id
            group
            name
            key
            type
            uploadDate
            notes
            documentDate
            createdAt
            updatedAt
            takenByUser {
              id
              firstName
              lastName
            }
            takenByAssociate {
              id
              firstName
              lastName
            }
          }
          nextToken
        }
        history {
          items {
            id
            date
            previousValue
            currentValue
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;

export const getDailyLogDates = /* GraphQL */ `
  query GetDailyLog($id: ID!) {
    getDailyLog(id: $id) {
      id
      date
      createdAt
      updatedAt
    }
  }
`;

export const listUsers = /* GraphQL */ `
  query ListUsers(
    $filter: ModelUserFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listUsers(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        group
        firstName
        lastName
      }
      nextToken
    }
  }
`;

export const staffsByGroup = /* GraphQL */ `
  query StaffsByGroup(
    $group: String
    $firstName: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelStaffFilterInput
    $limit: Int
    $nextToken: String
  ) {
    staffsByGroup(
      group: $group
      firstName: $firstName
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        firstName
        lastName
        status
        alternateNames
      }
      nextToken
    }
  }
`;

export const getVehicle = /* GraphQL */ `
  query GetVehicle($id: ID!) {
    getVehicle(id: $id) {
      id
      group
      name
    }
  }
`;
