import { mapState, mapGetters } from 'vuex';
import { Storage } from 'aws-amplify';
import moment from 'moment-timezone';
import {
  createDocument,
  updateDocument,
  deleteDocument,
  createDailyLogHistory,
} from '@/graphql/mutations';
import { 
  createDailyLog,
  updateDailyLog
} from '@/store/mutations'
import {
  dailyLogsByVehicleAndDate,
  getDailyLog,
  getDailyLogDates,
  getVehicle,
} from './queries';
import store from '@/store'

const mapToOption = item => ({
  value: item.id,
  label: `${item.firstName || ''} ${item.lastName || ''}`,
});

const filterBySearch = (value='', search='') => {
  const lower = value.toLowerCase();
  const lowerSearch = search.toLowerCase();

  return lower && lower.includes(lowerSearch);
};

function mapLogListDates(logList=[]) {
  if (!logList?.length) return [];

  const formatDate = (date) => moment(date, 'YYYY-MM-DD').format('MM/DD/YYYY');

  return logList.map(item => ({
    ...item,
    previousValue: formatDate(item.previousValue),
    currentValue: formatDate(item.currentValue),
  }));
}

function getState(item){

  const state = { value : '', color : '' }

  const { documents,shortenUrls } = item

  const documentItems = documents.items
  const isOpenLink = shortenUrls.items[0]?.isOpenLink

  if(!isOpenLink){
    state.value = 'Not Opened'
    state.color = 'bg-red-500'
    return state
  }

  const documentByAssociate = documentItems.filter((document) => document?.takenByAssociate)

  if(documentByAssociate.length > 0){
    const all = documentByAssociate.length > 1 ? 'Photos' : 'Photo'
    state.value = `${documentByAssociate.length} ${all}`
    state.color = 'bg-green-500'
  }

  if(isOpenLink && documentByAssociate.length === 0){
    state.value = 'Opened, No Photos'
    state.color = 'bg-orange-500'
  }

  return state

}

function getFullName(image, takenBy = null){
  const newTakenBy = image.takenByAssociate || image.takenByUser || takenBy;
  if (newTakenBy?.firstName && newTakenBy?.lastName) return `${newTakenBy.firstName} ${newTakenBy.lastName}`;
  if (newTakenBy?.firstName) return newTakenBy.firstName;
  if (newTakenBy?.lastName) return newTakenBy.lastName;

  return '—';
}

function getAssociateOrUserIDFromImage (image){
  const takenBy = image.takenByAssociate || image.takenByUser;
  return takenBy?.id || null;
}

function getCreationNotesFromDailyLog(dailyLog) {

  const tenantTimeZone = store.state.userInfo?.tenant.timeZone || ''
  const {rosteredDay, creationLinkSentDateTime} = dailyLog;

  if (rosteredDay && !creationLinkSentDateTime) {
    const formattedDate = moment(rosteredDay.notesDate, 'YYYY-MM-DD').format('MM/DD/YYYY');
    return `This Vehicle Photo Log was originally created from the Daily Roster for ${formattedDate}.`;
  }

  if (rosteredDay && creationLinkSentDateTime) {
    const {creationLinkAssociate: associate, creationLinkSentByUser: user} = dailyLog;
    const associateFullName = associate? `${associate.firstName || ''} ${associate.lastName || ''}`: '—';
    const userFullName = user? `${user.firstName || ''} ${user.lastName || ''}`: '—';
    const formattedTime = moment.tz(creationLinkSentDateTime, tenantTimeZone).format('hh:mm A')

    return `This Vehicle Photo Log was originally created from a creation link sent to ${associateFullName} at ${formattedTime} by ${userFullName}.`;
  }

  return '';
}

function getDailyLogHistoryInitialData() {
  return {
    title: "Vehicle Photo Log Change Log",
    logType: "photo log",
    logList: [],
    dailyLogStoredDate: '',
    dailyLogUploadDate: '',
  };
}

function getPlainDate(date=new Date(), format) {
  if (typeof date === 'string') return date.replace(/\//ig, '-');
  if (!(date instanceof Date)) return date;

  const dateFormat = format || 'YYYY-MM-DD';

  return moment(date).format(dateFormat);
}

async function addSrcToImages(imagesList=[], takenBy = null) {
  return await Promise.all(
    imagesList?.map(
      async (img) => {
        try {
          const src = await Storage.get(img.key);
          const takenByFullName = getFullName(img, takenBy)
          const takenById = getAssociateOrUserIDFromImage(img) || takenBy?.id || null
          return { ...img, src, takenByFullName, takenById };
        } catch {
          return { ...img, src: '', takenByFullName: '', takenById: ''};
        }
      })
    );
}

export default {
  computed: {
    ...mapState([
      'userInfo',
    ]),
    ...mapGetters([
      'hasVehicleManagement'
    ]),

    ...mapState('subscriptionStore', [
      'userList',
      'associateList',
      'vehicleList'
    ]),

    VehicleDailyLog_UsersList(){
      return this.userList
    },

    VehicleDailyLog_AssociatesList(){
      return this.associateList
    },

    VehicleDailyLog_PrimarySectionTitle() {
      const date = this.VehicleDailyLog_FormFields.date;
      const dateFormat = 'MM/DD/YYYY';
      const plainDate = getPlainDate(date, dateFormat) || '';

      return `PHOTOS FOR ${plainDate}`;
    },

    VehicleDailyLog_CreatedByOptions() {

      const optionsList = this.reactiveOptionList
      const search = this.VehicleDailyLog_TakenBySearch;
      
      if (!search && this.VehicleDailyLog_FormFields.takenById) return optionsList;

      const filterByFullName = (item) => {
        const fullName = `${item.firstName || ''} ${item.lastName || ''}`;
        return filterBySearch(fullName, search);
      };

      const filteredUsers = this.userList.filter(filterByFullName).map(mapToOption);

      const filteredActiveAssociates = store.getters["subscriptionStore/getActiveAssociates"]
        .filter(filterByFullName)
        .map(mapToOption);

      const filteredInactiveAssociates = store.getters["subscriptionStore/getInactiveAssociates"]
        .filter(filterByFullName)
        .map(mapToOption);

      const updateReactiveList = (a, b, valueWhenNoneFound) => {
        a.length = 0
        if(b.length > 0){
          a.push(...b)
        }else{
          a.push({ value: valueWhenNoneFound, label: 'None Found', disabled: true } )
        }
      }

      // Load Users
      updateReactiveList(optionsList[0].options, filteredUsers, 1)
      // Load Active Users
      updateReactiveList(optionsList[1].options, filteredActiveAssociates, 2)
      // Load Inactive Users
      updateReactiveList(optionsList[2].options, filteredInactiveAssociates, 3)

      return optionsList;
    },
  },

  data() { 
    return {
      VehicleDailyLog_VehicleId: null,

      VehicleDailyLog_IsOpen: false,
      VehicleDailyLog_Loading: false,
      VehicleDailyLog_FormTitle: '',
      VehicleDailyLog_FormFields: {},
      VehicleDailyLog_Vehicle: {},

      VehicleDailyLog_SecondarySectionTitle: 'MOST RECENT PREVIOUS PHOTOS',

      VehicleDailyLog_Images: [],
      VehicleDailyLog_ImagesToCreate: [],
      VehicleDailyLog_ImagesToDelete: [],

      VehicleDailyLog_Links: [],

      VehicleDailyLog_CachedDailyLogToEdit: null,

      VehicleDailyLog_MostRecentDailyLog: null,
      VehicleDailyLog_MostRecentDailyLogImages: [],

      // Private Props
      VehicleDailyLog_TakenBySearch: '',
      reactiveOptionList: [
        {
          label: 'HERA USERS',
          options: [],
        },
        {
          label: 'ACTIVE ASSOCIATES',
          options: [],
        },
        {
          label: 'INACTIVE ASSOCIATES',
          options: [],
        },
      ]
    };
  },

  methods: {
    $_VehicleDailyLog_initFormFields(date, time) {
      let now = new Date();
      if(date instanceof Date) { // Override 'now' with 'date', keep 'time'
        const dateDate = moment(date).format('YYYY-MM-DD');
        const nowTime = moment(now).format('HH:mm:ss.SSSS');
        now = moment(`${dateDate} ${nowTime}`).toDate();
      }
      const timezone = this.userInfo?.tenant.timeZone;
      const isValidTimezone = !!moment.tz.zone(timezone);
      if (isValidTimezone) {
        const parsedDate = moment.tz(now, timezone);
        now = new Date(parsedDate.format('YYYY/MM/DD'));
      }
      
      return {
        notes: '',
        date: now,
        time: (time) ? time : '00:00:00',
        takenById: '',
        creationNotes: '',
        history: getDailyLogHistoryInitialData(),
      };
    },

    $_VehicleDailyLog_open(payload) {
      const { mode } = payload;

      if(mode === 'create') return this.$_VehicleDailyLog_openInCreateMode(payload);
      if(mode === 'edit') return this.$_VehicleDailyLog_openInEditMode(payload);

      // throw error on invalid mode
      throw new Error('To open a Vehicle Photo Log, a mode(create|edit) must be set in the payload');
    },

    async $_VehicleDailyLog_openInCreateMode(payload) {
      const { vehicleId=null, date, time } = payload || {};

      if (!vehicleId) {
        throw new Error('No vehicle ID was selected for this Photo Log');
      }

      this.$_VehicleDailyLog_clearFormFields(date, time);

      try {
        this.VehicleDailyLog_IsOpen = true;
        this.VehicleDailyLog_Loading = true;

        this.VehicleDailyLog_FormTitle = 'Add a new Vehicle Photo Log';

        this.VehicleDailyLog_TakenBySearch = '';
        const { id, firstName='', lastName='' } = this.userInfo;
        // Trigger search, to load the current logged in user as default for createdBy
        this.VehicleDailyLog_TakenBySearch = `${firstName || ''} ${lastName || ''}`;
        this.VehicleDailyLog_FormFields.takenById = id;

        this.VehicleDailyLog_VehicleId = vehicleId;
        this.VehicleDailyLog_Vehicle = await this.$_VehicleDailyLog_loadVehicle(vehicleId);
      }
      catch (error) {
        console.log(error);
        this.displayUserError(error);
        this.VehicleDailyLog_IsOpen = false;
      }
      finally {
        this.VehicleDailyLog_Loading = false;
      }
    },

    async $_VehicleDailyLog_openInEditMode(payload) {
      const { dailyLogId } = payload;

      this.$_VehicleDailyLog_clearFormFields();

      this.VehicleDailyLog_IsOpen = true;
      this.VehicleDailyLog_Loading = true;

      const input = { id: dailyLogId };

      try {
        const {
          data: { getDailyLog: dailyLog }
        } = await this.api(getDailyLog, input);

        if (!dailyLog?.vehicleId) {
          this.VehicleDailyLog_IsOpen = false;
          throw new Error('This Vehicle Photo Log does not exist or does not have a vehicle associated to it');
        }

        this.VehicleDailyLog_FormTitle = 'Vehicle Photo Log';

        this.VehicleDailyLog_VehicleId = dailyLog.vehicleId;
        this.VehicleDailyLog_Vehicle = await this.$_VehicleDailyLog_loadVehicle(dailyLog.vehicleId);

        const [date, time] = this.$_VehicleDailyLog_getDateAndTimeInTenantTimezone(dailyLog.date);

        const history = {
          ...getDailyLogHistoryInitialData(),
          logList: mapLogListDates(dailyLog.history?.items),
          dailyLogStoredDate: dailyLog.date,
          dailyLogUploadDate: dailyLog.createdAt,
        };

        this.VehicleDailyLog_TakenBySearch = '';

        const { takenByUser, takenByAssociate } = dailyLog;
        const takenBy = takenByUser || takenByAssociate || null;
        if (takenBy) {// Trigger search, to load user/associate who created the Daily Log record
          this.VehicleDailyLog_TakenBySearch = `${takenBy.firstName || ''} ${takenBy.lastName || ''}`;
        }

        this.VehicleDailyLog_FormFields = {
          id: dailyLog.id,
          group: dailyLog.group,
          notes: dailyLog.notes,
          takenById: takenBy?.id || '',
          creationNotes: getCreationNotesFromDailyLog(dailyLog),
          history,
          date,
          time,
        };

        this.VehicleDailyLog_CachedDailyLogToEdit = { ...dailyLog };

        //LOAD LINKS ASSOCIATE
        this.VehicleDailyLog_Links = this.$_VehicleDailyLog_showAssociateLinks([dailyLog])
        
        // LOAD STORED IMAGES
        this.VehicleDailyLog_Images = await addSrcToImages(dailyLog.documents.items,takenBy);
        
        // Load Most Recent Daily Log
        await this.$_VehicleDailyLog_loadMostRecentDailyLog({ dailyLogToEdit: dailyLog });
      }
      catch (error) {
        console.log(error);
        this.displayUserError(error);
        this.VehicleDailyLog_IsOpen = false;
      }
      finally {
        this.VehicleDailyLog_Loading = false;
      }
    },

    $_VehicleDailyLog_clearFormFields(date, time) {
      this.VehicleDailyLog_VehicleId = null;

      this.VehicleDailyLog_FormFields = this.$_VehicleDailyLog_initFormFields(date, time);

      this.VehicleDailyLog_Vehicle = {};

      this.VehicleDailyLog_Images = [];
      this.VehicleDailyLog_ImagesToCreate = [];
      this.VehicleDailyLog_ImagesToDelete = [];

      this.VehicleDailyLog_CachedDailyLogToEdit = null;

      this.VehicleDailyLog_MostRecentDailyLog = null;
      this.VehicleDailyLog_MostRecentDailyLogImages = [];
    },

    $_VehicleDailyLog_cancelSave() {
      this.VehicleDailyLog_IsOpen = false;
      this.$_VehicleDailyLog_clearFormFields();
    },

    async $_VehicleDailyLog_loadVehicle(vehicleId) {
      try {
        const storedVehicle = this.vehicleList.find(vehicle => vehicle.id === vehicleId)
        if(storedVehicle){
          return storedVehicle
        }
        const response = await this.api(getVehicle, { id: vehicleId});
        return response.data.getVehicle;
      }
      catch (error) {
        throw error;
      }
    },

    $_VehicleDailyLog_parsePlainDateToTenantTimezoneMomentDate(date, time) {
      const timezone = this.userInfo.tenant.timeZone;
      const plainDate = getPlainDate(date);

      return moment.tz(`${plainDate} ${time}`, timezone);
    },

    $_VehicleDailyLog_getDateAndTimeInTenantTimezone(timestamp, onlyDate = null) {
      const timezone = this.userInfo.tenant.timeZone;

      const [dateStr, timeStr] = moment.tz(timestamp, timezone)
        .format('YYYY/MM/DD HH:mm:ss')
        .split(' ');

      if(onlyDate) return [dateStr, timeStr]

      const date = new Date(`${dateStr} ${timeStr}`);
      const time = date.toTimeString().split(' ')[0];

      return [date, time];
    },

    async $_VehicleDailyLog_loadMostRecentDailyLog(payload) {
      this.VehicleDailyLog_MostRecentDailyLog = null;
      this.VehicleDailyLog_MostRecentDailyLogImages = [];

      let { dailyLogToEdit } = payload || {};

      if (!dailyLogToEdit && !this.VehicleDailyLog_CachedDailyLogToEdit) return;
      if (!dailyLogToEdit) {
        const { date, time } = this.VehicleDailyLog_FormFields;
        const dateMomentObject = this.$_VehicleDailyLog_parsePlainDateToTenantTimezoneMomentDate(date, time);

        dailyLogToEdit = {...this.VehicleDailyLog_CachedDailyLogToEdit};
        dailyLogToEdit.date = dateMomentObject.toISOString();
      }

      try {
        const input = {
          vehicleId: dailyLogToEdit.vehicleId,
          date: {
            lt: dailyLogToEdit.date,
          },
          limit: 1,
          sortDirection: 'DESC',
        };

        const {
          data: { dailyLogsByVehicleAndDate: previousDailyLogs }
        } = await this.api(dailyLogsByVehicleAndDate, input, 'dailyLogsByVehicleAndDate');

        const mostRecentDailyLog = previousDailyLogs?.items?.at(0);

        if (!mostRecentDailyLog || mostRecentDailyLog.id === dailyLogToEdit.id) return;

        const [date, time] = this.$_VehicleDailyLog_getDateAndTimeInTenantTimezone(mostRecentDailyLog.date);

        const history = {
          ...getDailyLogHistoryInitialData(),
          logList: mapLogListDates(mostRecentDailyLog.history?.items),
          dailyLogStoredDate: mostRecentDailyLog.date,
          dailyLogUploadDate: mostRecentDailyLog.createdAt,
        };

        const { takenByUser, takenByAssociate } = mostRecentDailyLog;
        const takenBy = takenByUser || takenByAssociate || null;
        const takenByFullName = (!takenBy) ? '—': `${takenBy.firstName || ''} ${takenBy.lastName || ''}`;

        this.VehicleDailyLog_MostRecentDailyLog = {
          history,
          date,
          rawDate: mostRecentDailyLog.date,
          time,
          takenByFullName,
          notes: mostRecentDailyLog.notes || '—',
        };

        // LOAD STORED IMAGES
        this.VehicleDailyLog_MostRecentDailyLogImages = await addSrcToImages(mostRecentDailyLog.documents.items);
      }
      catch (error) {
        throw error;
      }
    },

    async $_VehicleDailyLog_savePlusAddDamage(payload) {
      if(!this.hasVehicleManagement){
        this.displayUserNotification({title: 'Save', type: 'info', message:'Premium Required'})
        return
      }
      const { openVehicleDamage } = payload;

      try {
        // Load data before is cleared out by '$_VehicleDailyLog_saveData' call
        const vehicleId = this.VehicleDailyLog_VehicleId;
        const { notes, date, time } = this.VehicleDailyLog_FormFields;

        const dateInTenantTimeZone = this.$_VehicleDailyLog_parsePlainDateToTenantTimezoneMomentDate(date, time);

        const imgIdsMarkedForDeletion = this.VehicleDailyLog_ImagesToDelete.reduce(
          (acc, img) => ((acc[img.id] = true), acc),
          {}
        );

        const notMarkedForDeletion = img => !imgIdsMarkedForDeletion[img.id];

        const storedImages = this.VehicleDailyLog_Images
          .filter(notMarkedForDeletion)
          .map(img => ({
            ...img,
            fileName: `${Date.now()}-${img.name}`,
            fileType: img.type,
          }));
        const toCreateImages = this.VehicleDailyLog_ImagesToCreate.map(img => ({ ...img }));

        const imagesToCreate = await addSrcToImages([...storedImages, ...toCreateImages]);

        // Save Vehicle Daily Photo Log
        const payload = {throwError: true};
        await this.$_VehicleDailyLog_saveData(payload);

        // Wait to show success notification before opening VehicleDamage
        await new Promise(resolve => setTimeout(resolve, 50));

        // Open Vehicle Damage Drawer in Create Mode
        const damagePayload = {
          mode: 'create',
          formTitle: 'Create Vehicle Damage',
          formAction: 'Create',
          formFields: {
            vehicleId,
            notes,
            vehicleDamageDate: dateInTenantTimeZone,
          },
          imagesToCreate,
          prefixCounselingCreatedFrom: 'Vehicle Photo Log',
        };

        openVehicleDamage(damagePayload);
      } catch (error) {
        console.log(error);
        this.displayUserError(error);
      }
    },

    $_VehicleDailyLog_injectRosteredDayId(rosteredDayId) {
      this.VehicleDailyLog_FormFields.rosteredDayId = rosteredDayId;
    },

    async $_VehicleDailyLog_saveData(payload={}) {
      if(!this.hasVehicleManagement){
        this.displayUserNotification({title: 'Save', type: 'info', message:'Premium Required'})
        return
      }
      const { throwError=false } = payload;

      this.VehicleDailyLog_Loading = true;

      const documents = [];
      let photoLogID = this.VehicleDailyLog_FormFields.id;
      const isUpdate = !!photoLogID;

      try {
        if (
          (!this.VehicleDailyLog_ImagesToCreate.length && !this.VehicleDailyLog_Images.length) ||
          (!this.VehicleDailyLog_ImagesToCreate.length &&
            this.VehicleDailyLog_ImagesToDelete.length === this.VehicleDailyLog_Images.length)
        ) {
          throw new Error('Please select or take some photos for this Daily Photo Log');
        }

        const { rosteredDayId, notes, date, time } = this.VehicleDailyLog_FormFields;
        const plainDate = getPlainDate(date);
        const dateInTenantTimeZone = this.$_VehicleDailyLog_parsePlainDateToTenantTimezoneMomentDate(date, time);

        const input = {
          notes,
          type: 'Vehicle Photo Log',
          date: dateInTenantTimeZone,
          group: this.userInfo.tenant.group,
          vehicleId: this.VehicleDailyLog_VehicleId,
          dailyLogVehicleId: this.VehicleDailyLog_VehicleId,
        };

        if (photoLogID) { input.id = photoLogID; }
        else { input.id = photoLogID = this.generateUUID() }
        if(rosteredDayId){
          input.dailyLogRosteredDayId = rosteredDayId
        }else{
          const formattedDate = this.$moment(this.date).format("YYYY-MM-DD")
          input.dailyLogRosteredDayId = `${formattedDate}${this.userInfo.tenant.group}`
        }

        // Update user o associate 
        for (const imageDocument of this.VehicleDailyLog_Images) {
        
          const imageTakenBy = imageDocument.takenByUser || imageDocument.takenByAssociate

          if( imageTakenBy?.id && imageTakenBy?.id !== imageDocument.takenById ){

            const takenByAssociateOrUser = this.VehicleDailyLog_ValidateTakenBy(imageDocument.takenById)

            const documentInput = {
              id : imageDocument.id
            }

            if(takenByAssociateOrUser.isUser){
              documentInput.documentTakenByUserId = imageDocument.takenById
              documentInput.documentTakenByAssociateId = null
            }else{
              documentInput.documentTakenByAssociateId = imageDocument.takenById
              documentInput.documentTakenByUserId = null
            }

            await this.api(updateDocument, { input: documentInput });
          }
        }

        // Save added images
        for (const imageDocument of this.VehicleDailyLog_ImagesToCreate) {
          const savedDocument = await this.$_VehicleDailyLog_createAndSaveDocument(
            photoLogID,
            imageDocument,
            plainDate,
            dateInTenantTimeZone,
          );

          documents.push(savedDocument);
        }

        // Remove images marked for deletion
        await this.$_VehicleDailyLog_deleteRelatedPhotoLogData(this.VehicleDailyLog_ImagesToDelete);

        if (isUpdate) {
          await this.$_VehicleDailyLog_createDailyLogHistoryRecordIfNeeded({...input});
          await this.api(updateDailyLog, { input });
        }
        else {
          await this.api(createDailyLog, { input });
        }

        this.displayUserNotification({
          title: 'Success',
          type: "success",
          message: `Vehicle Daily Photo Log successfuly ${isUpdate? 'updated': 'created'}.`,
        });

        this.$_VehicleDailyLog_clearFormFields();
        this.VehicleDailyLog_IsOpen = false;
      }
      catch (error) {
        // Remove partially saved images for Daily Log
        await this.$_VehicleDailyLog_deleteRelatedPhotoLogData(documents);

        if(throwError) {
          throw error;
        }

        console.log(error);
        this.displayUserError(error);
      }
      finally {
        this.VehicleDailyLog_Loading = false;
      }
    },

    async $_VehicleDailyLog_createDailyLogHistoryRecordIfNeeded(input) {
      const { id, date } = input;

      const {
        data: { getDailyLog: dailyLogDates }
      } = await this.api(getDailyLogDates, { id }, 'getDailyLog');

      // Compare dates ONLY
      const dateFormat = 'YYYY-MM-DD';
      const timezone = this.userInfo.tenant.timeZone;

      const formDateStr = date.toISOString();
      const formDate = moment(formDateStr).tz(timezone).format(dateFormat);

      const recordedDateStr = dailyLogDates.date;
      const recordedDate = moment(recordedDateStr).tz(timezone).format(dateFormat);

      const isNewDate = formDate !== recordedDate;
      if (isNewDate) { // Create new Daily Log History record
        const historyLogInput = {
          date: (new Date()).toISOString(),
          group: this.userInfo.tenant.group,
          previousValue: recordedDate, // save date only YYYY-MM-DD
          currentValue: formDate, // save date only YYYY-MM-DD
          dailyLogId: id,
          dailyLogHistoryDailyLogId: id,
        };
        await this.api(createDailyLogHistory, {input: historyLogInput});
      }
    },

    async $_VehicleDailyLog_createAndSaveDocument(photoLogId, document, plainDate, dateTime) {
      try {
        const millisNow = (new Date()).getTime();
        // Upload file to S3
        const group = this.userInfo.tenant.group;

        const userId = this.userInfo.id

        const { file, fileType, fileName } = document;

        const s3Key = 'photo-log-images/' +
          group +
          '/vehicles/' +
          this.VehicleDailyLog_VehicleId.toLowerCase() +
          `/${plainDate}/${millisNow}-${fileName}`;

        const object = await Storage.put(s3Key, file);

        const input = {
          group,
          name: fileName,
          type: fileType,
          key: object.key,
          documentDate: dateTime,
          uploadDate: (new Date()).toISOString(),
          documentDailyLogId: photoLogId,
          documentTakenByUserId: userId
        }

        return await this.api(createDocument, { input });
      }
      catch(error) {
        console.log(error);
        this.displayUserError(error);
      }
    },

    async $_VehicleDailyLog_deleteRelatedPhotoLogData(documents=[]) {
      if(documents.length > 0) {
        await Promise.allSettled(documents.map(async (document) => {
          await Storage.remove(document.key);
          await this.api(deleteDocument, {input: { id: document.id }});
        }));
      }
    },

    $_VehicleDailyLog_getPhotosTakenByConnectionField(takenById) {
      const user = this.userList.find(user => user.id === takenById);
      if (user) return {
        connectionField: 'dailyLogTakenByUserId',
        oldField: 'dailyLogTakenByAssociateId',
      };

      const associate = this.associateList.find(associate => associate.id === takenById);
      if (associate) return {
        connectionField: 'dailyLogTakenByAssociateId',
        oldField: 'dailyLogTakenByUserId',
      };

      return null;
    },

    $_VehicleDailyLog_handleSearchCreatedBy(search) {
      this.VehicleDailyLog_TakenBySearch = search;
    },

    async $_VehicleDailyLog_fetchList(payload, retry=3) {
      const { query, queryName, variables={} } = payload || {};

      try {
        const items = await this.gLoadListAll(query, variables, queryName);
        return items;
      }
      catch (error) {
        console.log(error);

        if (retry <= 0) return [];

        return await this.$_VehicleDailyLog_fetchList(payload, retry - 1);
      }
    },

    async $_VehicleDailyLog_loadUsers() {
      const payload = {
        query: listUsers,
        queryName: 'listUsers',
      };

      this.VehicleDailyLog_UsersList = await this.$_VehicleDailyLog_fetchList(payload);
    },

    async $_VehicleDailyLog_loadAssociates() {
      const payload = {
        query: staffsByGroup,
        queryName: 'staffsByGroup',
        variables: { group: this.userInfo.tenant.group },
      };

      this.VehicleDailyLog_AssociatesList = await this.$_VehicleDailyLog_fetchList(payload);
    },

    VehicleDailyLog_ValidateTakenBy(id){

      const validateTakenBy = {
        isUser: false,
        isAssociate: false
      }

      const filteredUsers = this.VehicleDailyLog_UsersList
        .filter(item => item.id === id)

      const filteredAssociates = this.VehicleDailyLog_AssociatesList
        .filter(associate => associate.id === id)


      if (filteredUsers.length > 0) {
        validateTakenBy.isUser = true
      }

      if (filteredAssociates.length > 0) {
        validateTakenBy.isAssociate = true
      }

      return validateTakenBy

    },

    $_VehicleDailyLog_showAssociateLinks(dailyLogs) {

      const dailyLogsItems = dailyLogs.flatMap((item) => {
        const [date, time] = this.$_VehicleDailyLog_getDateAndTimeInTenantTimezone(item.creationLinkSentDateTime, true);
        
        return item.shortenUrls.items.map(url => ({
          url,
          sentToAssociate: `${item.creationLinkAssociate?.firstName || ''} ${item.creationLinkAssociate?.lastName || ''}`,
          sentDateTime: `${date} ${time}`,
          sentByUser: `${item.creationLinkSentByUser?.firstName || ''} ${item.creationLinkSentByUser?.lastName || ''}`,
          status: getState(item),
        }));
      });
      return dailyLogsItems

    },
  },

  created() {
    this.VehicleDailyLog_FormFields = this.$_VehicleDailyLog_initFormFields();
  },
};
