import { mapState, mapGetters, mapMutations } from "vuex";
import {
  createAccident,
  deleteAccident,
  // updateStaff,
  // createVehicleStatus,
  // createDocument,
  deleteInfraction, deleteCounseling,
  deleteDocument, updateAccident,
  createVehicleMaintenanceReminder,
  deleteVehicleMaintenanceReminder
} from '@/graphql/mutations'
import {getAccidentUpdatedTimestamp, getVehicleUpdatedTimestamp} from '@/utilities/timestampQueries'
import { confirmChangeStatusVehicle } from "@/scripts/createStatusMaintenance"


export default {
    data(){
      return {
        Maintenance_DrawerVisibility: false,
        MaintenanceForm_formFields: {
          location: null,
          services: [],
          accidentDate: null,
          maintenanceStatus: null,
          mileage: null,
          vehicleId: null, 
          accidentVehicleId: null,
          notes: null
        },
        MaintenanceForm_Reminders: [],
        MaintenanceForm_Documents: [],
        MaintenanceForm_DocumentsCreated:[],
        MaintenanceForm_DocumentsDeleted:[],
        MaintenanceForm_vehicleSelect:{
          name:'',
          status:'',
          vehicle:null
        },
      }
    },
    computed: {
      ...mapState(['userInfo']),
      ...mapGetters([
        'hasVehicleManagement',
      ]),
    },
    methods: {
      MaintenanceDrawer_open(payload, vehicle){
        this.MaintenanceForm_formFields = {
          group: this.userInfo.tenant.group,
          mileage: null,
          vehicleId: vehicle.id, 
          accidentVehicleId: vehicle.id,
          vehicleHistoryType: "Maintenance",
          maintenanceStatus: (payload.valueActionToggle != undefined && payload.valueActionToggle) ? "In Progress" : null,
          //default
          location: null,
          services: [],
          accidentDate: null,
          notes: null
        },
        this.MaintenanceForm_Reminders = []
        this.MaintenanceForm_Documents = []
        this.MaintenanceForm_DocumentsCreated = []
        this.MaintenanceForm_DocumentsDeleted = []
        this.MaintenanceForm_vehicleSelect = {
          name: vehicle.name,
          status: vehicle.status,
          vehicle: vehicle,
        },
        this.Maintenance_DrawerVisibility = true
      },
      /**
       * Handles caneling maintenance creation
       */
      handleMaintenanceCancel(){
        this.$refs.maintenanceForm.$refs.form.clearValidate();
        this.Maintenance_DrawerVisibility = false
        this.MaintenanceForm_formFields = {
          location: null,
          services: [],
          accidentDate: null,
          maintenanceStatus: null,
          mileage: null,
          accidentVehicleId: null,
          vehicleId: null,
          notes: null
        }
        this.MaintenanceForm_Reminders = []
        this.MaintenanceForm_Documents = []
        this.MaintenanceForm_DocumentsCreated = []
        this.MaintenanceForm_DocumentsDeleted = []
      },
      /**
       * Creates Maintenance Type Accident Record
       */
      async saveMaintenanceData(){
        if(!this.hasVehicleManagement){
          this.displayUserNotification({title: 'Create', type: 'info', message:'Premium Required'})
          return
        }
        try{
          // this.loading = true
          var reminderIDs = []
          var documentIDs = []
          var vehicleMaintenanceID = this.MaintenanceForm_formFields.id
          var recordNeedsUpdated = vehicleMaintenanceID ? true : false
          //Validate form
          try {
            await this.$refs.maintenanceForm.$refs.form.validate();
          } catch (error) {
            throw new Error('Please fill out all required fields.');
          }

          //check timestamp
          if(recordNeedsUpdated){
            var timestampResult = await this.api(getAccidentUpdatedTimestamp, {id: this.MaintenanceForm_formFields.id})
            if(!timestampResult.data.getAccident){
                throw "Vehicle maintenance is missing or deleted by another user."
            }
            var timestamp = timestampResult.data.getAccident.updatedAt
            if(this.MaintenanceForm_formFields.updatedAt != timestamp){
                throw "Record was updated by another user. Please copy any changes and refresh the page."
            }
          }

          //Create Reminders
          if(this.MaintenanceForm_Reminders.length > 0){
            for(const [index , maintenanceReminder] of this.MaintenanceForm_Reminders.entries()){
              //Validate reminder
              await this.$refs.maintenanceForm.$refs.reminder[index].$refs.form.validate();

              //Create input object
              var input = {...maintenanceReminder}
              input.vehicleId = this.MaintenanceForm_formFields.vehicleId
              input.vehicleMaintenanceReminderVehicleId = this.MaintenanceForm_formFields.vehicleId

              //Save dueBySort to object
              if(input.dueByMileage){
                input.dueByMileage = this.cleanMileage(input.dueByMileage)
                input.dueBySort = parseFloat(this.cleanMileage(input.dueByMileage))
              }
              if(!input.dueByMileage){
                input.dueBySort = (new Date(input.dueByDate).getTime() / 1000000) 
              }

              input.status = "Pending"
              //Create Reminder
              const reminder = await this.api(createVehicleMaintenanceReminder, {input})
              var reminderID = reminder.data.createVehicleMaintenanceReminder.id
              reminderIDs.push(reminderID)
            }
          }

          var input = {...this.MaintenanceForm_formFields}
          input.accidentMaintenanceVehicleId = this.MaintenanceForm_formFields.vehicleId
          input.mileage = this.cleanMileage(input.mileage)
          this.cleanAccidentInput(input)
          if(!recordNeedsUpdated){
            const result = await this.api(createAccident, {input})
            var vehicleMaintenance = result.data.createAccident
            var vehicleMaintenanceID = vehicleMaintenance.id
          }

          //Create Documents
          if(this.MaintenanceForm_Documents.length > 0){
            for(const maintenanceDoc of this.MaintenanceForm_Documents){
              var documentRecord = await this.createDocument(vehicleMaintenanceID, maintenanceDoc, this.MaintenanceForm_formFields.vehicleId, 'vehicle-maintenance')
              var documentID = documentRecord.data.createDocument.id
              documentIDs.push(documentID)
            }
          }

          //Delete Documents
          if(this.MaintenanceForm_DocumentsDeleted.length > 0){
            for(const maintenanceDoc of this.MaintenanceForm_DocumentsDeleted){
                  var result = await Storage.remove(maintenanceDoc.key)
                  var deleteResult = await this.api(deleteDocument, {input:{ id: maintenanceDoc.id}} )
            }
          }

          //Update Accident
          if(recordNeedsUpdated){
            const updateResult = await this.api(updateAccident, {input})
          }

          if(this.statusVehicleForm.newStatus !== 'Inactive - Maintenance'){
            const response = await confirmChangeStatusVehicle(this.MaintenanceForm_vehicleSelect,this.MaintenanceForm_formFields);
            if(response.valueConfirmChangeStatusVehicle){
              this.statusVehicleForm.newStatus = (response.opc === 1) ? 'Inactive - Maintenance' : 'Active';
              this.statusVehicleForm.date = new Date();
              this.statusVehicleForm.statusChangeReason = (response.opc === 1) ? 'Opened Maintenance Record' : 'Completed Maintenance Record';
              this.statusVehicleForm.vehicle = this.MaintenanceForm_vehicleSelect.vehicle
              await this.updateStatus();
            }
          }
          
          //Wrap up
          this.$refs.maintenanceForm.$refs.form.clearValidate();
          this.Maintenance_DrawerVisibility = false
          this.MaintenanceForm_formFields = {
            location: null,
            services: [],
            accidentDate: null,
            maintenanceStatus: null,
            mileage: null,
          }
          this.MaintenanceForm_Reminders = []
          this.MaintenanceForm_Documents = []
          this.MaintenanceForm_DocumentsCreated = []
          this.MaintenanceForm_DocumentsDeleted = []
          this.reminderNextToken = null
          this.accidentNextToken = null
          this.reminders = []
          this.vehicleHistory = []
          this.loadData()
        }
        catch(e){
          console.log(e)
          if(e == false){
            e = "Please fill out all required fields and correct any errors."
          }
          if(recordNeedsUpdated){
            this.deleteRelatedAccidentData(reminderIDs, documentIDs, null, null, null)
          }
          else{
            this.deleteRelatedAccidentData(reminderIDs, documentIDs, vehicleMaintenanceID, null, null)
          }
          this.displayUserError(e)
        }
        finally{
          this.loading = false
        }
      },

      /**
       * Delete the reminders and documents that the user tried to create with their maintenance record.
       */
      async deleteRelatedAccidentData(reminders, documents, accidentID, counselingID, infractionID, ){
        //Delete Reminders
        if(reminders.length > 0){
          reminders.forEach((reminderID)=>{
            this.api(deleteVehicleMaintenanceReminder, {input:{ id: reminderID }} )
          })
        }

        if(documents.length > 0){
          documents.forEach((documentID)=>{
            this.api(deleteDocument, {input:{ id: documentID }} )
          })
        }

        if(accidentID){
          this.api(deleteAccident, {input:{ id: accidentID}} )
        }

        if(counselingID){
          this.api(deleteCounseling, {input:{ id: counselingID}} )
        }

        if(infractionID){
          this.api(deleteInfraction, {input:{ id: infractionID}} )
        }
      },
    }
}