import { updateAccident } from '@/graphql/mutations'
import { accidentsByVehicleAndHistoryType } from '../../queries'
import { API,graphqlOperation } from 'aws-amplify'

/**
 * Update All Status Maintenance Records 
 * @param {String} id vehicle identification
 */
export async function updateHistoryMaintenanceComplete(id) {

    var input = {
        vehicleId: id,
        vehicleHistoryTypeAccidentDate: {
            beginsWith: {
                vehicleHistoryType: "Maintenance"
            }
        },
        filter: {
            maintenanceStatus: {
                ne: "Complete"
            }
        }
    }

    try {
        var result = await API.graphql(graphqlOperation(accidentsByVehicleAndHistoryType, input))
    } catch (e) {
        console.log(e);
    }

    const items = result.data.accidentsByVehicleAndHistoryType.items

    for (const item of items) {
        input = {
            id: item.id,
            maintenanceStatus: 'Complete'
        }
        try {
            await API.graphql(graphqlOperation(updateAccident, {
                input
            }));
        } catch (e) {
            console.log(e);
        }
    }
}