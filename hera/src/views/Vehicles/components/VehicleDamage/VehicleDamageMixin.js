import { mapState, mapGetters } from 'vuex';
import { Storage } from 'aws-amplify';
import { COUNSELING_SEVERITY_CREATE_COUNSELING } from '@/utilities/constants/defaultSectionsByCustomList';
import { COUNSELING_STATUS_NOT_SENT } from '@/utilities/constants/counseling';
import { getAccidentUpdatedTimestamp } from '@/utilities/timestampQueries';
import { getAccident } from '@/graphql/queries';
import { staffsByGroup, vehiclesByGroup } from './queries';
import {
  createAccident,
  updateAccident,
  deleteAccident,
  createDocument,
  deleteDocument,
  createInfraction,
  createCounseling,
} from '@/graphql/mutations';

function setdocumentURLToNull(documents) {
  return documents.map(document => ({
    ...document,
    url: null,
    markedForDeletion: false,
  }));
}

function loadAndSetImageSrcFromBlob(imgObj) {
  if (imgObj.file) {
    imgObj.src = URL.createObjectURL(imgObj.file);
  }

  return imgObj;
}

function initialFormFields() {
  return {
    vehicleId: null,
    damage: null,
    vehicleDamageDate: null,
    damageSeverity: null,
    notes: null,
    staff: { id: '' },
    createDaIssue: null,
    createCounseling: null,
    accidentVehicleId: null,
  };
}

export default {
  computed: {
    ...mapState([
      'userInfo',
    ]),
    ...mapGetters([
      'hasVehicleManagement',
      'counselingSeverityTypeList',
      'issueTypeList'
    ]),
    
    ...mapState('subscriptionStore', [
      'vehicleList',
      'associateList'
    ]),

    VehicleDamage_VehicleList(){
      return this.vehicleList
    },

    VehicleDamage_StaffList(){
      return this.associateList
    },
  },

  data() {
    return {
      VehicleDamage_IsOpen: false,
      VehicleDamage_Loading: false,
      VehicleDamage_FormTitle: '',
      VehicleDamage_FormAction: '',
      VehicleDamage_FormFields: initialFormFields(),
      VehicleDamage_PrefixCounselingCreatedFrom: '',
      VehicleDamage_CounselingData: {},
      VehicleDamage_DeleteModalOpen: false,

      VehicleDamage_LoadingStaffList: false,

      VehicleDamage_LoadingVehicleList: false,
      VehicleDamage_DisableVehicleSelection: false,

      VehicleDamage_DamageImages: [],
      VehicleDamage_ImagesToCreate: [],
      VehicleDamage_ImagesToDelete: [],
    }
  },

  methods: {
    $_VehicleDamage_open(payload={}) {
      const { mode } = payload;

      if (mode === 'create') return this.$_VehicleDamage_openInCreateMode(payload);
      if (mode === 'edit') return this.$_VehicleDamage_openInEditMode(payload);

      // throw error on invalid mode
      throw new Error('To open a Vehicle Damage, a mode(create|edit) must be set in the payload');
    },

    $_VehicleDamage_openInCreateMode(payload={}) {
      const {
        formTitle='Create Vehicle Damage',
        formAction='Create',
        formFields={},
        counselingData={},
        imagesToCreate=[],
        prefixCounselingCreatedFrom='',
        disableVehicleSelection=false,
      } = payload;

      this.$_VehicleDamage_clearFormFields();

      this.VehicleDamage_FormAction = formAction;
      this.VehicleDamage_FormTitle = formTitle;

      this.VehicleDamage_DisableVehicleSelection = disableVehicleSelection;

      this.VehicleDamage_FormFields = {
        ...initialFormFields(),
        group: this.userInfo.tenant.group,
        vehicleHistoryType: "Vehicle Damage",
        ...formFields,
      };

      this.VehicleDamage_PrefixCounselingCreatedFrom = prefixCounselingCreatedFrom;
      this.VehicleDamage_CounselingData = {...counselingData};

      if (Array.isArray(imagesToCreate) && imagesToCreate.length > 0) {
        this.VehicleDamage_ImagesToCreate = this.VehicleDamage_ImagesToCreate.concat(imagesToCreate);
        this.VehicleDamage_ImagesToCreate = this.VehicleDamage_ImagesToCreate.map(loadAndSetImageSrcFromBlob);
      }

      this.VehicleDamage_IsOpen = true;
    },

    async $_VehicleDamage_openInEditMode(payload={}) {
      const {
        damageId,
        formTitle='Update Vehicle Damage',
        formAction='Update',
        disableVehicleSelection=true,
      } = payload;

      this.VehicleDamage_IsOpen = true;
      this.VehicleDamage_Loading = true;

      const input = { id: damageId };

      try{
        const {
          data: { getAccident: accident }
        } = await this.api(getAccident, input);

        if(!accident.vehicleId) {
          const msg = "Cannot edit vehicle damage record without vehicle. Vehicle record is missing.";
          throw new Error(msg);
        }

        this.$_VehicleDamage_clearFormFields();

        this.VehicleDamage_FormAction = formAction;
        this.VehicleDamage_FormTitle = formTitle;

        this.VehicleDamage_DisableVehicleSelection = disableVehicleSelection;

        this.VehicleDamage_FormFields = {
          ...initialFormFields(),
          vehicleHistoryType: "Vehicle Damage",
          id: accident.id,
          vehicleId: accident.vehicleId,
          accidentVehicleId: accident.vehicleId,
          group: accident.group,
          damage: accident.damage,
          notes: accident.notes,
          damageSeverity: accident.damageSeverity,
          vehicleDamageDate: accident.vehicleDamageDate,
          staff: { id: accident.staff?.id },
          updatedAt: accident.updatedAt,
        };

        // This makes the images be reactive in the vehicle damage form and show up.
        this.VehicleDamage_DamageImages = setdocumentURLToNull(accident.vehicleDamageImages.items);
      }
      catch(e) {
        console.log(e);
        this.VehicleDamage_IsOpen = false;
        this.displayUserError(e);
      }
      finally {
        this.VehicleDamage_Loading = false;
      }
    },

    $_VehicleDamage_openDeleteDialog() {
      this.VehicleDamage_DeleteModalOpen = true;
    },

    $_VehicleDamage_closeDeleteDialog() {
      this.VehicleDamage_DeleteModalOpen = false;
    },

    $_VehicleDamage_clearFormFields() {
      this.VehicleDamage_CounselingData = {};

      this.VehicleDamage_FormFields = initialFormFields();

      this.VehicleDamage_DamageImages = [];
      this.VehicleDamage_ImagesToCreate = [];
      this.VehicleDamage_ImagesToDelete = [];
    },

    $_VehicleDamage_print() {
      const accidentID = this.VehicleDamage_FormFields.id;

      const route = this.$router.resolve({
        name: 'PrintVehicleDamage',
        params:{ accidentID }
      });

      window.open(route.href, '_blank');
    },

    async $_VehicleDamage_delete() {
      this.VehicleDamage_Loading = true;

      const input = { id: this.VehicleDamage_FormFields.id };

      try {
        const {
          data: { getAccident: accident }
        } = await this.api(getAccident, input);

        let documents = [];
        // Documents
        documents = documents.concat(accident.incidentDocuments?.items || []);
        // Vehicle Damage Images
        documents = documents.concat(accident.vehicleDamageImages?.items || []);
        // Maintenance Documents
        documents = documents.concat(accident.maintenanceDocuments?.items || []);

        // Delete Documents
        for(const document of documents) {
          await Storage.remove(document.key);
          await this.api(deleteDocument, {input: { id: document.id }});
        }

        //Delete Accident
        await this.api(deleteAccident, {input: { id: accident.id}});

        this.VehicleDamage_IsOpen = false;
        this.$_VehicleDamage_clearFormFields();
      }
      catch(e) {
        console.log(e);
        this.displayUserError(e);
      }
      finally{
        this.VehicleDamage_DeleteModalOpen = false;
        this.VehicleDamage_Loading = false;
      }
    },

    $_VehicleDamage_cancelSave() {
      this.$_VehicleDamage_clearFormFields();
      this.VehicleDamage_IsOpen = false;
    },

    async $_VehicleDamage_checkTimestamps(vehicleDamageID) {
      const timestampResult = await this.api(getAccidentUpdatedTimestamp, {id: vehicleDamageID});
      if(!timestampResult.data.getAccident) {
        throw new Error("Vehicle damage is missing or deleted by another user.");
      }

      const timestamp = timestampResult.data.getAccident.updatedAt;
      if(this.VehicleDamage_FormFields.updatedAt !== timestamp) {
        throw new Error("Record was updated by another user. Please copy any changes and refresh the page.");
      }
    },

    $_VehicleDamage_getQueryInput(staffID) {
      const input = {
        ...this.VehicleDamage_FormFields,
        accidentVehicleId: this.VehicleDamage_FormFields.vehicleId,
        accidentDamageVehicleId: this.VehicleDamage_FormFields.vehicleId,
        accidentDate: this.VehicleDamage_FormFields.vehicleDamageDate,
      };

      if(staffID) {
        input.staffId = staffID;
        input.accidentStaffId = staffID;
      };

      return input;
    },

    async $_VehicleDamage_handleVehicleDamageImages(documentIDs, vehicleDamageID, vehicleId) {
      // Remove deleted images
      if (this.VehicleDamage_ImagesToDelete.length > 0) {
        for(const image of this.VehicleDamage_ImagesToDelete) {
          await Storage.remove(image.key);
          await this.api(deleteDocument, { input: { id: image.id }});
        }
      }

      // Upload new added images
      if(this.VehicleDamage_ImagesToCreate.length > 0) {
        for(const image of this.VehicleDamage_ImagesToCreate) {
          const documentRecord = await this.$_VehicleDamage_createDocument(vehicleDamageID, image, vehicleId, 'vehicle-damage');
          const documentID = documentRecord.data.createDocument.id;
          documentIDs.push(documentID);
        }
      }
    },

    async $_VehicleDamage_handleCreateAditionalDocuments(isInEditMode, staffID, input) {
      if (isInEditMode) return [null, null];

      const {
        createCounseling: isCreateCounselingChecked,
        createDaIssue: isCreateInfractionChecked,
      } = input;
      
      // Create counseling and/or infraction
      let counselingID = null;
      let infractionID = null;

      const issue = `Vehicle Damage: ${input.damage}`;

      const payload = {
        staffID,
        date: input.vehicleDamageDate,
        notes: issue,
      };

      if(isCreateCounselingChecked) {
        const counselingRecord = await this.$_VehicleDamage_createCounselingForStaff(payload);
        counselingID = counselingRecord.data.createCounseling.id;
      }

      if(isCreateInfractionChecked) {
        payload.counselingID = counselingID;

        const infractionRecord = await this.$_VehicleDamage_createInfractionForStaff(payload);
        infractionID = infractionRecord.data.createInfraction.id;
      }

      return [counselingID, infractionID];
    },

    async $_VehicleDamage_saveData() {
      if(!this.hasVehicleManagement){
        this.displayUserNotification({title: 'Save', type: 'info', message:'Premium Required'})
        return
      }
      this.VehicleDamage_Loading = true;

      const documentIDs = [];
      let counselingID = null;
      let infractionID = null;
      let vehicleDamageID = this.VehicleDamage_FormFields.id;
      const recordNeedsUpdate = !!vehicleDamageID;

      try {
        if(recordNeedsUpdate) {
          await this.$_VehicleDamage_checkTimestamps(vehicleDamageID);
        }

        const staffID = this.VehicleDamage_FormFields.staff?.id;
        const input = this.$_VehicleDamage_getQueryInput(staffID);
        const inputCopy = {...input};
        this.cleanAccidentInput(input);

        if(recordNeedsUpdate) { // Update Accident
          await this.api(updateAccident, {input});
        }
        else { // Create Accident
          const result = await this.api(createAccident, {input});
          const vehicleDamage = result.data.createAccident;
          vehicleDamageID = vehicleDamage.id;
        }

        await this.$_VehicleDamage_handleVehicleDamageImages(documentIDs, vehicleDamageID, input.vehicleId);

        const isInEditMode = recordNeedsUpdate;
        [counselingID, infractionID] = await this.$_VehicleDamage_handleCreateAditionalDocuments(isInEditMode, staffID, inputCopy);

        this.VehicleDamage_IsOpen = false;
        this.$_VehicleDamage_clearFormFields();
      }
      catch(e) {
        console.log(e);
        this.displayUserError(e);
        this.$_VehicleDamage_deleteRelatedAccidentData(documentIDs, counselingID, infractionID);
      }
      finally {
        this.VehicleDamage_Loading = false;
      }
    },

    async $_VehicleDamage_deleteRelatedAccidentData(documentIDs, counselingID, infractionID) {
      try {
        if(documentIDs.length > 0) {
          Promise.all(documentIDs.map(async (id) =>
            await this.api(deleteDocument, {input: { id }})
          ));
        }

        if(counselingID) {
          await this.api(deleteCounseling, {input:{ id: counselingID}});
        }

        if(infractionID) {
          await this.api(deleteInfraction, {input:{ id: infractionID}});
        }
      } catch (error) {
        this.displayUserError(error);
      }
    },

    async $_VehicleDamage_createDocument(accidentID, document, vehicleID, pathType) {
      const { group } = this.userInfo.tenant;
      const { path, fileType, fileName } = document;

      const timestamp = String(Date.now());

      const s3Key = group +
        '/vehicle/' +
        vehicleID.toLowerCase() +
        `/${pathType}/` +
        accidentID.toLowerCase() +
        path + timestamp + "/" + fileName;

      // Upload file to S3
      if(!document.file && document.key) {
        const result = await Storage.get(document.key, { download: true });
        document.file = result.Body;
      }
      const object = await Storage.put(s3Key, document.file);

      const input = {
        group,
        uploadDate: timestamp,
        name: fileName,
        type: fileType,
        key: object.key,
        documentImagevehicleDamageId: accidentID,
      };

      const result = await this.api(createDocument, {input});
      return result;
    },

    async $_VehicleDamage_createCounselingForStaff(payload) {
      const {staffID, date, notes} = payload;

      const createdFrom = `${this.VehicleDamage_PrefixCounselingCreatedFrom} ${notes? '(' + notes + ')': ''}`;

      const input = {
        date,
        createdFrom,
        counselingNotes: notes,
        counselingStaffId: staffID,
        counselingSeverityId: this.counselingSeverityTypeList.find(item => item.isDefaultForSections?.includes(COUNSELING_SEVERITY_CREATE_COUNSELING))?.id,
        counselingUserId: this.userInfo.id,
        tenantId: this.userInfo.tenant.id,
        group: this.userInfo.tenant.group,
        status: COUNSELING_STATUS_NOT_SENT,
        ...this.VehicleDamage_CounselingData,
      };

      const counseling = await this.api(createCounseling, {input});
      return counseling;
    },

    async $_VehicleDamage_createInfractionForStaff(payload) {
      const {staffID, date, notes, counselingID} = payload;

      const input = {
        date,
        infractionTypeId: this.issueTypeList.find(item => item.option === 'Vehicle Damage')?.id,
        staffId: staffID,
        infractionStaffId: staffID,
        comment: notes,
        group: this.userInfo.tenant.group,
      };

      if(counselingID) {
        input.infractionCounselingId = counselingID;
      }

      const infraction = await this.api(createInfraction, {input});
      return infraction;
    },
  },
}
