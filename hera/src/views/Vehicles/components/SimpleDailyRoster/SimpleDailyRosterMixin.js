import moment from 'moment-timezone';

function getPlainDate(date=new Date(), format) {
    if (typeof date === 'string') return date.replace(/\//ig, '-');
    if (!(date instanceof Date)) return date;
  
    const dateFormat = format || 'YYYY-MM-DD';
  
    return moment(date).format(dateFormat);
}

export default {

  data() { 
    return {

      // Photo Logs vars to dialog modal
      VehicleDailyLog_IsOpenPhotoLog: false,
      VehicleDailyLog_FormTitlePhotoLog: '',
      VehicleDailyLog_FormFieldsPhotoLog: {},

      // Photo Logs vars to drawer
      VehicleDailyLog_IsOpenSimpleDailyRoster: false,
      VehicleDailyLog_FormTitleSimpleDailyRoster: '',
      VehicleDailyLog_AssociatesLinks: [],
       
    };
  },

  methods: {
   
    $_VehicleDailyLog_OpenPhotoLog(payload, showDialog) {

        const { vehicle } = payload;
        this.VehicleDailyLog_FormFieldsPhotoLog = payload
        this.VehicleDailyLog_IsOpenPhotoLog = showDialog
        this.VehicleDailyLog_FormTitlePhotoLog = `Add Vehicle Photo Log for ${vehicle.name}`
  
    },
  
    $_VehicleDailyLog_CancelSimpleDailyRoster(){
        this.VehicleDailyLog_IsOpenSimpleDailyRoster = false
    },
  
    $_VehicleDailyLog_OpenTableAssociateLinks(photoLogs, date) {

        const dateFormat = 'MMM D, YYYY';
        const plainDate = getPlainDate(date, dateFormat) || '';
        this.VehicleDailyLog_IsOpenSimpleDailyRoster = true
        this.VehicleDailyLog_FormTitleSimpleDailyRoster = `Vehicle Photo Logs for ${plainDate}`
        const photoLogModify = photoLogs.map((item) => {
          const newItem = JSON.parse(JSON.stringify(item))
          newItem.time_photolog = this.intlFormatDate(item.date, { hour: "numeric", minute: "numeric", second: "numeric" });
          newItem.associate_links = newItem.shortenUrls?.items.length || 0
          newItem.dailyLog_photos = newItem.documents?.items.length || 0
          return newItem
        })
  
        this.VehicleDailyLog_AssociatesLinks = photoLogModify
  
    },
  
    $_VehicleDailyLog_CancelPhotoLog(){
        this.VehicleDailyLog_IsOpenPhotoLog = false
    }

  },
};
