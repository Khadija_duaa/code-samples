export const REMINDER_STATUS_PENDING = 'Pending';
export const REMINDER_STATUS_COMPLETE = 'Complete';

export const DUE_BY_OPTION_NO_DUE_BY_DATE = 'No Due By Date';
export const DUE_BY_OPTION_PAST_DUE = 'Past Due';
export const DUE_BY_OPTION_NEXT_7_DAYS = 'Due In Next 7 Days';
export const DUE_BY_OPTION_NEXT_14_DAYS = 'Due In Next 14 Days';
export const DUE_BY_OPTION_OVER_14_DAYS = 'Due In Over 14 Days';

export const HISTORY_TYPE_VEHICLE_DAMAGE = "Vehicle Damage Record";
export const HISTORY_TYPE_VEHICLE_PHOTO_LOG = "Vehicle Photo Log";
export const HISTORY_TYPE_MAINTENANCE = "Maintenance Record";
export const HISTORY_TYPE_INCIDENT = "Incident Record";
export const HISTORY_TYPE_ODOMETER_READING = "Odometer Reading";
export const HISTORY_TYPE_VEHICLE_DETAILS_UPDATE = "Vehicle Details Updated"
