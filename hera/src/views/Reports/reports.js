import moment from 'moment';

import {i18n} from '@/main'
import {date} from '@/utilities/getDate'

import { INICIDENT_TYPE_ACCIDENT } from '@/utilities/constants/incidents';

import {
    INCIDENT_ROUTE_INDEX,
    INCIDENT_ROUTE_NEW,
    VEHICLE_DAMAGE_ROUTE_INDEX,
    VEHICLE_DAMAGE_ROUTE_RECENT,
} from '@/utilities/constants/routeNames';

//PLEASE KEEP IN ALPHABETICALLY ORDER BY NAME
export const reportsArr = [
    {
        name: "Accident Records",
        description: "View Incident Records that have an Incident Type of \"Accident\".",
        neededPermission: 'permissionAccidents',
        neededPremium: ['hasVehicleManagement', 'hasStaffManagement'],
        sections: [
            "VehicleManagement"
        ],
        route: INCIDENT_ROUTE_INDEX,
        routeQuery: {
            type: INICIDENT_TYPE_ACCIDENT,
        },
        actions: [
            {
                actionName: "View Report",
                actionRoute: INCIDENT_ROUTE_INDEX,
                actionQuery: {
                    type: INICIDENT_TYPE_ACCIDENT,
                },
                report: "Recent Accident Records"
            },
            {
                actionName: "Create Accident",
                actionRoute: INCIDENT_ROUTE_NEW,
                actionParams: [
                    {
                        paramName: "loadAsAccidentType",
                        paramValue: true
                    }
                ],
                report: "Recent Accident Records"
            },
            {
                actionName: "Go to Dashboard",
                actionRoute: "VehicleIndex",
                report: "Recent Accident Records"
            }           
        ]
    },
    {
        name: "Counselings (All)",
        description: "View all counselings.",
        neededPermission: ['permissionCounselings', 'permissionManageCounselings'],
        neededPremium: 'hasPerformanceCoaching',
        sections: [
            "PerformanceAndCoaching"
        ],
        route: "CounselingIndex",
        actions: [
            {
                actionName: "View Report",
                actionRoute: "CounselingIndex",
                report: "Counselings"
            },
            {
                actionName: "Create Counseling",
                actionRoute: "CounselingNew",
                report: "Counselings"
            },
            {
                actionName: "Go to Dashboard",
                actionRoute: "Dashboard",
                report: "Counselings"
            }
        ]
    },    
    {
        name: "Counselings Pending Signature",
        description: `View all counselings that are awaiting signature from the ${i18n.t('label.associate')}.`,
        neededPermission: ['permissionCounselings', 'permissionManageCounselings'],
        neededPremium: 'hasPerformanceCoaching',
        sections: [
            "PerformanceAndCoaching"
        ],
        route: "PendingCounselingsReport",
        actions: [
            {
                actionName: "View Report",
                actionRoute: "PendingCounselingsReport",
                report: "Counselings"
            },
            {
                actionName: "Create Counseling",
                actionRoute: "CounselingNew",
                report: "Counselings"
            },
            {
                actionName: "Go to Dashboard",
                actionRoute: "Dashboard",
                report: "Counselings"
            }
        ]
    },
    {
        name: `Birthdays for Active ${i18n.t('label.associate')}`,
        description: `View all Active ${i18n.t('label.associate')} birthdays.`,
        sections: [
            "DaManagement"
        ],
        route: "AnniversariesSummary",
        routeParams: [
            {
                paramName: "milestoneOption",
                paramValue: "birthday"
            }
        ],
        actions: [
            {
                actionName: "View Report",
                actionRoute: "AnniversariesSummary",
                report: "DA Birthdays",
                actionParams: [
                    {
                        paramName: "milestoneOption",
                        paramValue: "birthday"
                    }
                ]
            }
        ]
    },
    {
        name: `${i18n.t('label.associate')} Issues (All)`,
        description: `View all ${i18n.t('label.associate')} Issues.`,
        neededPremium: 'hasPerformanceCoaching',
        sections: [
            "PerformanceAndCoaching"
        ],
        route: "DaIssueIndex",
        actions: [
            {
                actionName: "View Report",
                actionRoute: "DaIssueIndex",
                report: "DA Issues"
            },
            {
                actionName: "Create DA Issue",
                actionRoute: "DaIssueNew",
                report: "DA Issues"
            },
            {
                actionName: "Go to Dashboard",
                actionRoute: "Dashboard",
                report: "DA Issues"
            }
        ]
    },
    {
        name: `${i18n.t('label.associate')} Issues (Last 90 Days)`,
        description: `View all ${i18n.t('label.associate')} issues in the last 90 days.`,
        neededPremium: 'hasPerformanceCoaching',
        sections: [
            "PerformanceAndCoaching"
        ],
        route: "RecentDaIssuesReport",
        actions: [
            {
                actionName: "View Report",
                actionRoute: "RecentDaIssuesReport",
                report: "DA Issues"
            },
            {
                actionName: "Create DA Issue",
                actionRoute: "DaIssueNew",
                report: "DA Issues"
            },
            {
                actionName: "Go to Dashboard",
                actionRoute: "Dashboard",
                report: "DA Issues"
            }
        ]
    },
    {
        name: `${i18n.t('label.associate')} Kudos (All)`,
        description: `View all ${i18n.t('label.associate')} Kudos.`,
        neededPremium: 'hasPerformanceCoaching',
        sections: [
            "PerformanceAndCoaching"
        ],
        route: "DaKudoIndex",
        actions: [
            {
                actionName: "View Report",
                actionRoute: "DaKudoIndex",
                report: "DA Issues"
            },
            {
                actionName: "Create DA Kudo",
                actionRoute: "DaKudoIndex",
                actionParams: [
                    {
                        paramName: "addKudo",
                        paramValue: true
                    }
                ],
                report: "DA Kudos"
            },
            {
                actionName: "Go to Dashboard",
                actionRoute: "Dashboard",
                report: "DA Kudos"
            }
        ]
    },
    {
        name: `${i18n.t('label.associate')} Kudos (Last 90 Days)`,
        description: `View all ${i18n.t('label.associate')} kudos in the last 90 days.`,
        neededPremium: 'hasPerformanceCoaching',
        sections: [
            "PerformanceAndCoaching"
        ],
        route: "RecentDaKudosReport",
        routeParams: [
            {
                paramName: 'range',
                paramValue: 'recent'
            },
        ],
        actions: [
            {
                actionName: "View Report",
                actionRoute: "RecentDaKudosReport",
                report: "DA Kudos",
                actionParams: [
                    {
                        paramName: 'range',
                        paramValue: 'recent'
                    }
                ]
            },
            {
                actionName: "Create DA Kudo",
                actionRoute: "DaKudoIndex",
                actionParams: [
                    {
                        paramName: "addKudo",
                        paramValue: true
                    }
                ],
                report: "DA Kudos"
            },
            {
                actionName: "Go to Dashboard",
                actionRoute: "Dashboard",
                report: "DA Kudos"
            }
        ]
    },
    {
        name: `${i18n.t('label.associates')} Opted Out of Communications`,
        description: `View all ${i18n.t('label.associates')} who have opted out of SMS or email communication`,
        neededPremium: 'hasStaffManagement',
        sections: [
            "DaManagement"
        ],
        route:"StaffOptedOut",
        actions: [
            {
                actionName: "View Report",
                actionRoute: "StaffOptedOut",
                report: "DA Opted Out of Communications",
                actions: [
                    {
                        actionName: "View Report",
                        actionRoute: "StaffOptedOut"
                    }
                ]
            }
        ]
    },
    {
        name: `30-Day ${i18n.t('label.associate')} Issue Trends`,
        description: `View ${i18n.t('label.associate')} issues trends in the 30 days.`,
        neededPremium: 'hasPerformanceCoaching',
        sections: [
            "PerformanceAndCoaching"
        ],
        route: "DaIssueTrendReport",
        actions: [
            {
                actionName: "View Report",
                actionRoute: "DaIssueTrendReport",
                report: "DA Issues Trend"
            },
            {
                actionName: "Manage Types of Associate Issues",
                actionRoute: "SettingsDropDowns",
                report: "DA Issues Trend"
            },
            {
                actionName: "Go to Dashboard",
                actionRoute: "StaffIndex",
                report: "DA Issues Trend"
            }
        ]
    },
    {
        name: `${i18n.t('label.associate')} Performance`,
        description: `View the performance of all ${i18n.t('label.associates')} and their performance compared to others.`,
        neededPremium: 'hasPerformanceCoaching',
        sections: [
            "PerformanceAndCoaching"
        ],
        route: "DaPerformanceReport",
        actions: [
            {
                actionName: "View Report",
                actionRoute: "DaPerformanceReport",
                report: "DA Performance"
            },
            {
                actionName: "Go to Dashboard",
                actionRoute: "StaffIndex",
                report: "DA Performance"
            }
        ]
    },
    {
        name: "Incident Records",
        description: "View all Incident Records, which includes Accidents.",
        neededPermission: 'permissionAccidents',
        neededPremium: ['hasVehicleManagement', 'hasStaffManagement'],
        sections: [
            "DaManagement"
        ],
        route: INCIDENT_ROUTE_INDEX,
        actions: [
            {
                actionName: "View Report",
                actionRoute: INCIDENT_ROUTE_INDEX,
                report: "Accidents"
            },
            {
                actionName: "Create Incident",
                actionRoute: INCIDENT_ROUTE_NEW,
                report: "Accidents"
            },
            {
                actionName: "Go to Dashboard",
                actionRoute: "VehicleIndex",
                report: "Accidents"
            }
        ]
    },
    {
        name: "Injury Records (All)",
        description: "View all injury records.",
        neededPermission: 'permissionInjuries',
        neededPremium: 'hasStaffManagement',
        sections: [
            "DaManagement"
        ],
        route: "InjuryIndex",
        actions: [
            {
                actionName: "View Report",
                actionRoute: "InjuryIndex",
                report: "Injuries"
            },
            {
                actionName: "Add Injury",
                actionRoute: "InjuryNew",
                report: "Injuries"
            }
        ]
    },   
    {
        name: "Issued Uniforms",
        description: "",
        sections: [
            "DaManagement"
        ],
        route: "IssuedUniformsReport",
        actions: [
            {
                actionName: "View Report",
                actionRoute: "IssuedUniformsReport",
                report: "Issued Uniforms Rep"
            },
        ]
    },
    {
        name: "Expiring Driver Licenses",
        description: `View all ${i18n.t('label.associates')} with expiring or expired driver licenses.`,
        neededPremium: 'hasStaffManagement',
        sections: [
            "DaManagement"
        ],
        route: "DriverLicensesReport",
        actions: [
            {
                actionName: "View Report",
                actionRoute: "DriverLicensesReport",
                report: "Expiring Driver Licenses"
            },
            {
                actionName: "Go to Dashboard",
                actionRoute: "StaffIndex",
                report: "Expiring Driver Licenses"
            }
        ]
    },
    {
        name: `${i18n.t('label.associates')} Missing a Phone Number`,
        description: `View all ${i18n.t('label.associates')} with expiring or expired driver licenses.`,
        neededPremium: 'hasStaffManagement',
        sections: [
            "DaManagement"
        ],
        route: "MissingPhoneNumberReport",
        actions: [
            {
                actionName: "View Report",
                actionRoute: "MissingPhoneNumberReport",
                report: "Associates Missing a Phone Number"
            }
        ]
    },
    {
        name: "Expiring License Plates",
        description: "View all vehicles with expiring or expired license plates.",
        neededPremium: 'hasVehicleManagement',
        sections: [
            "VehicleManagement"
        ],
        route: "VehicleLicensePlate",
        actions: [
            {
                actionName: "View Report",
                actionRoute: "VehicleLicensePlate",
                report: "Expiring License Plates"
            },
            {
                actionName: "Go to Dashboard",
                actionRoute: "VehicleIndex",
                report: "Expiring License Plates"
            }
        ]
    },
    {
        name: "Maintenance Reminders (All)",
        description: "View all maintenance reminders.",
        neededPremium: 'hasVehicleManagement',
        sections: [
            "VehicleManagement"
        ],
        route: "ReminderIndex",
        actions: [
            {
                actionName: "View Report",
                actionRoute: "ReminderIndex",
                report: "Maintenance Reminders"
            },
            {
                actionName: "Add Reminder",
                actionRoute: "ReminderIndex",
                report: "Maintenance Reminders",
                actionParams: [
                    {
                        paramName: "addReminder",
                        paramValue: true
                    }
                ]
            },
            {
                actionName: "Go to Dashboard",
                actionRoute: "VehicleIndex",
                report: "Maintenance Reminders"
            },
        ]
    },
    {
        name: "Associate Issue Trends Graphs",
        description: "",
        sections: [
            "PerformanceAndCoaching"
        ],
        route: "DaIssueTrendGraphsReport",
        actions: [
            {
                actionName: "View Report",
                actionRoute: "DaIssueTrendGraphsReport",
                report: "Issue Trends Graphs",
            },
            {
                actionName: "Manage Types of Associate Issues",
                actionRoute: "SettingsCustomLists",
                report: "Dropdowns List",
                neededPermission: 'permissionFullAccess',
            },
            {
                actionName: "Go to Dashboard",
                actionRoute: "StaffIndex",
                report: "Assocaite Dashboard",
            },
        ]
    },
    {
        name: "Netradyne Alerts (Last 30 Days)",
        description: "View all Netradyne alerts created in the last 30 days.",
        neededPremium: 'hasPerformanceCoaching',
        sections: [
            "PerformanceAndCoaching"
        ],
        route: "NetradyneIndex",
        routeParams: [
            {
                paramName: 'dashboardStartDate',
                paramValue: new Date(moment().subtract(30, 'days'))
            },
            {
                paramName: 'dashboardEndDate',
                paramValue: new Date(moment().subtract(1, 'days'))
            }
        ],
        actions: [
            {
                actionName: "View Report",
                actionRoute: "NetradyneIndex",
                report: "Netradyne Alerts",
                actionParams: [
                    {
                        paramName: 'dashboardStartDate',
                        paramValue: new Date(moment().subtract(30, 'days'))
                    },
                    {
                        paramName: 'dashboardEndDate',
                        paramValue: new Date(moment().subtract(1, 'days'))
                    }
                ],
            },
            {
                actionName: "Import Netradyne",
                actionRoute: "DailyDataImport",
                report: "Netradyne Alerts"
            }
        ]
    },
    {
        name: "Netradyne Alerts (This Week)",
        description: "View all Netradyne alerts created during the current week.",
        neededPremium: 'hasPerformanceCoaching',
        sections: [
            "PerformanceAndCoaching"
        ],
        route: "NetradyneIndex",
        routeParams: [
            {
                paramName: 'dashboardStartDate',
                paramValue: new Date(moment().startOf('week'))
            },
            {
                paramName: 'dashboardEndDate',
                paramValue: new Date(moment().endOf('week'))
            }
        ],
        actions: [
            {
                actionName: "View Report",
                actionRoute: "NetradyneIndex",
                report: "Netradyne Alerts",
                actionParams: [
                    {
                        paramName: 'dashboardStartDate',
                        paramValue: new Date(moment().startOf('week'))
                    },
                    {
                        paramName: 'dashboardEndDate',
                        paramValue: new Date(moment().endOf('week'))
                    }
                ],
            },
            {
                actionName: "Import Netradyne",
                actionRoute: "DailyDataImport",
                report: "Netradyne Alerts"
            },
            {
                actionName: "Go to Dashboard",
                actionRoute: "StaffIndex",
                report: "Netradyne Alerts"
            }
        ]
    },
    {
        name: "Netradyne Alerts (Yesterday)",
        description: "View all Netradyne alerts created for yesterday.",
        neededPremium: 'hasPerformanceCoaching',
        sections: [
            "PerformanceAndCoaching"
        ],
        route: "NetradyneIndex",
        routeParams: [
            {
                paramName: 'dashboardStartDate',
                paramValue: new Date(moment().subtract(1, 'days'))
            },
            {
                paramName: 'dashboardEndDate',
                paramValue: new Date(moment().subtract(1, 'days'))
            }
        ],
        actions: [
            {
                actionName: "View Report",
                actionRoute: "NetradyneIndex",
                report: "Netradyne Alerts",
                actionParams: [
                    {
                        paramName: 'dashboardStartDate',
                        paramValue: new Date(moment().subtract(1, 'days'))
                    },
                    {
                        paramName: 'dashboardEndDate',
                        paramValue: new Date(moment().subtract(1, 'days'))
                    }
                ],
            },
            {
                actionName: "Import Netradyne",
                actionRoute: "DailyDataImport",
                report: "Netradyne Alerts"
            },
            {
                actionName: "Go to Dashboard",
                actionRoute: "StaffIndex",
                report: "Netradyne Alerts"
            }
        ]
    },
    {
        name: "DSP-Level Performance Data: Weekly Imports",
        // description: "View all Netradyne alerts created for yesterday.",
        neededPremium: 'hasPerformanceCoaching',
        sections: [
            "PerformanceAndCoaching"
        ],
        route: "DaPerformanceImportedData",
        actions: [
            {
                actionName: "View Report",
                actionRoute: "DaPerformanceImportedData",
            },
        ]
    },
    {
        name: `Total Fantastics by ${i18n.t('label.associate')} with Most Recent Streak`,
        description: null,
        neededPremium: 'hasPerformanceCoaching',
        sections: [
            "PerformanceAndCoaching"
        ],
        route:"FantasticsByStaff",
        actions: [
            {
                actionName: "View Report",
                actionRoute: "FantasticsByStaff",
                report: "Total Fantastics by DA with Most Recent Streak"
            }
        ],
    },
    {
        name: 'Engine Off Compliance (EOC) Metrics',
        description: null,
        neededPremium: 'hasPerformanceCoaching',
        sections: [
            "PerformanceAndCoaching"
        ],
        route: "EOCMetricsReport",
        actions: [
            {
                actionName: "View Report",
                actionRoute: "EOCMetricsReport",
            },
            {
                actionName: "Go to Dashboard",
                actionRoute: "dashboard",
            }
        ]
    },
    {
        name: '30-Day Repeat Offenders',
        description: null,
        neededPremium: 'hasPerformanceCoaching',
        sections: [
            "PerformanceAndCoaching"
        ],
        route: "RepeatOffendersReport",
        actions: [
            {
                actionName: "View Report",
                actionRoute: "RepeatOffendersReport",
            },
            {
                actionName: "Go to Dashboard",
                actionRoute: "dashboard",
            }
        ]
    },
    {
        name: `${i18n.t('label.associates')} Who Rescued Others`,
        description: `View ${i18n.t('label.associates')} Who Rescued Others`,
        neededPremium: 'hasPerformanceCoaching',
        sections: [
            "DailyRostering"
        ],
        route:"StaffsWhoRescuedOthers",
        actions: [
            {
                actionName: "View Report",
                actionRoute: "StaffsWhoRescuedOthers",
                report: "DAs Who Rescued Others"
            }
        ]
    },
    {
        name: `${i18n.t('label.associates')} Who Were Rescued`,
        description: `View ${i18n.t('label.associates')} Who Were Rescued`,
        neededPremium: 'hasPerformanceCoaching',
        sections: [
            "DailyRostering"
        ],
        route:"StaffsWhoWereRescued",
        actions: [
            {
                actionName: "View Report",
                actionRoute: "StaffsWhoWereRescued",
                report: "DA Who Were Rescued"
            }
        ]
    },
    {
        name: `${i18n.t('label.associates')} Attendance Statistics`,
        description: `View ${i18n.t('label.associate')} Attendance Statistics`,
        neededPremium: 'hasPerformanceCoaching',
        sections: [
            "DailyRostering"
        ],
        route:"StaffAttendanceStatistics",
        actions: [
            {
                actionName: "View Report",
                actionRoute: "StaffAttendanceStatistics",
                report: "DA Attendance Statistics"
            }
        ]
    },
    {
        name: "Daily Roster Standbys",
        description: "View Daily Roster Standbys",
        neededPremium: 'hasPerformanceCoaching',
        sections: [
            "DailyRostering"
        ],
        route:"DailyRosterStandbys",
        actions: [
            {
                actionName: "View Report",
                actionRoute: "DailyRosterStandbys",
                report: "Daily Roster Standbys"
            }
        ]
    },
    {
        name: 'Daily Report',
        description: 'See all the most important activity for a day in one report',
        neededPremium: 'hasPerformanceCoaching',
        sections: [
            "DailyRostering"
        ],
        route:"DailyReport",
        routeParams: [
            {
                paramName: "dateId",
                paramValue: date()
            }
        ],
        actions: [
            {
                actionName: "View Report on Daily Roster",
                actionRoute: "DailyReport",
                report: "Daily Report",
                actionParams: [
                    {
                        paramName: "dateId",
                        paramValue: date()
                    }
                ],
                
            }
        ]
    },
    {
        name: "Tasks (All)",
        description: "View all created tasks.",
        neededPremium: 'hasStaffManagement',
        sections: [
            "UserManagement"
        ],
        route: "Task",
        actions: [
            {
                actionName: "View Report",
                actionRoute: "Task",
                report: "Tasks"
            },
            {
                actionName: "Add Task",
                actionRoute: "Task",
                report: "Tasks",
                actionParams: [
                    {
                        paramName: "addTask",
                        paramValue: true
                    }
                ]
            },
        ]
    },
    {
        name: "Upcoming Maintenance Reminders",
        description: "View all incomplete maintenance reminders.",
        neededPremium: 'hasVehicleManagement',
        sections: [
            "VehicleManagement"
        ],
        route: "UpcomingMaintenanceReminders",
        actions: [
            {
                actionName: "View Report",
                actionRoute: "ReminderIndex",
                report: "Upcoming Maintenance Reminders",
            },
            {
                actionName: "Add Maintenance Reminder",
                actionRoute: "ReminderIndex",
                report: "Upcoming Maintenance Reminders",
                actionParams: [
                    {
                        paramName: "addReminder",
                        paramValue: true
                    }
                ]
            },
            {
                actionName: "Go to Dashboard",
                actionRoute: "VehicleIndex",
                report: "Upcoming Maintenance Reminders"
            },
        ]
    },
    {
        name: "Vehicle Damage Records (All)",
        description: "View all vehicle damage records.",
        neededPremium: 'hasVehicleManagement',
        sections: [

            "VehicleManagement"
        ],
        route: VEHICLE_DAMAGE_ROUTE_INDEX,
        actions: [
            {
                actionName: "View Report",
                actionRoute: VEHICLE_DAMAGE_ROUTE_INDEX,
                report: "Vehicle Damages",
            },
            {
                actionName: "Add Vehicle Damage",
                actionRoute: VEHICLE_DAMAGE_ROUTE_INDEX,
                report: "Vehicle Damages",
                actionParams: [
                    {
                        paramName: 'addVehicleDamage',
                        paramValue: true
                    }
                ]
            },
            {
                actionName: "Go to Dashboard",
                actionRoute: "VehicleIndex",
                report: "Vehicle Damages"
            },
        ]
    },
    {
        name: "Vehicle Damage Records (Last 90 Days)",
        description: "View all the vehicle damage records from the last 90 days",
        neededPremium: 'hasVehicleManagement',
        sections: [
            "VehicleManagement"
        ],
        route: VEHICLE_DAMAGE_ROUTE_RECENT,
        actions: [
            {
                actionName: "View Report",
                actionRoute: VEHICLE_DAMAGE_ROUTE_RECENT,
                report: "Recent Vehicle Damage Records",
            },
            {
                actionName: "Add Vehicle Damage",
                actionRoute: VEHICLE_DAMAGE_ROUTE_INDEX,
                report: "Recent Vehicle Damage Records",
                actionParams: [
                    {
                        paramName: 'addVehicleDamage',
                        paramValue: true
                    }
                ]
            },
            {
                actionName: "Go to Dashboard",
                actionRoute: "VehicleIndex",
                report: "Recent Vehicle Damage Records"
            },        
        ]
    },
    {
        name: "Week-Old Odometer Readings",
        description: "View all vehicles that have gone without an odometer reading for longer than a week.",
        neededPremium: 'hasVehicleManagement',
        sections: [
            "VehicleManagement"
        ],
        route: "VehicleWeekOldOdometerReadings",
        actions: [
            {
                actionName: "View Report",
                actionRoute: "VehicleWeekOldOdometerReadings",
                report: "Week-Old Odometer Readings",
            },
            {
                actionName: "Add Odometer Reading",
                actionRoute: "VehicleWeekOldOdometerReadings",
                report: "Week-Old Odometer Readings",
                actionParams: [
                    {
                        paramName: 'addOdometerReading',
                        paramValue: true
                    }
                ]
            },
            {
                actionName: "Go to Dashboard",
                actionRoute: "VehicleIndex",
                report: "Week-Old Odometer Readings"
            },
        ]
    },
    {
        name: "Vehicle Maintenance Records",
        description: "Vehicle Maintenance Records",
        neededPremium: 'hasVehicleManagement',
        sections: [
            "VehicleManagement"
        ],
        route: "VehicleMaintenanceRecords",
        actions: [
            {
                actionName: "View Report",
                actionRoute: "VehicleMaintenanceRecords",
                report: "Vehicle Maintenance Records",
            },
            {
                actionName: "Add Maintenance Record",
                actionRoute: "VehicleMaintenanceRecords",
                report: "Vehicle Maintenance Records",
                actionParams: [
                    {
                        paramName: 'addMaintenanceRecord',
                        paramValue: true
                    }
                ]
            },
            {
                actionName: "Go to Dashboard",
                actionRoute: "VehicleIndex",
                report: "Vehicle Maintenance Records"
            },
        ]
    },
    {
        name: "Vehicles by Type",
        description: "View all vehicles by type.",
        neededPremium: 'hasVehicleManagement',
        sections: [
            "VehicleManagement"
        ],
        route: "VehicleByType",
        actions: [
            {
                actionName: "View Report",
                actionRoute: "VehicleByType",
                report: "Vehicles by Type",
            },
            {
                actionName: "Administer Vehicle Types",
                actionRoute: "SettingsDropDowns",
                report: "Vehicles by Type",
                requiredPermissionFullAccess: true
            },
            {
                actionName: "Go to Dashboard",
                actionRoute: "vehicle-dashboard",
                report: "Vehicles by Type",
            },
        ]
    }
]