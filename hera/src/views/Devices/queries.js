export const devicesByGroup = /* GraphQL */ `
  query DevicesByGroup(
    $group: String
    $deviceName: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelDeviceFilterInput
    $limit: Int
    $nextToken: String
  ) {
    devicesByGroup(
      group: $group
      deviceName: $deviceName
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        deviceName
        phoneNumber
        carrier
        status
        imei
        notes
        updatedAt
        route {
            items{
                id
            }
          nextToken
        }
        vehicle {
            items{
                id
            }
          nextToken
        }
      }
      nextToken
    }
  }
`;