import moment from 'moment';
import { API, Storage } from 'aws-amplify';
import { mapState, mapMutations } from 'vuex';
import { parseDatetimeToDate } from '@/utilities/dateTimeParsers';
import { parseCounselingStatusValueToLabel } from '@/views/Counseling/utils';
import { getStaffNames } from '../queries';


const mapInfractions = (infractions=[]) => infractions.map(infraction => ({
  date: infraction.date?.trim(),
  time: infraction.time?.trim(),
  comment: infraction.comment?.trim(),
  infractionType: infraction.infractionType?.trim(),
}));

async function filterAndMapDocuments(documentList=[]) {
  const hasS3Key = item => !!item.key;

  documentList = documentList.filter(hasS3Key).map(item => ({
    key: item.key,
    type: item.type,
    name: item.name,
  }));

  const isImage = item => item.type?.toLowerCase().includes('image');

  const documents = documentList.filter(item => !isImage(item));
  const images = await Promise.all(documentList.filter(isImage).map(async (item) => {
    const { width=1, height=1 } = await getImageDimensions(item.key);
    return { ...item, width, height };
  }));

  const sortedImages = images.sort((a, b) => {
    const ratioA = a.height / a.width;
    const ratioB = b.height / b.width;
    return ratioA - ratioB;
  });

  return {
    documents,
    images: sortedImages,
  };
}

function setEmDashOnEmptyNullFields(obj) {
  Object.entries(obj).forEach(([key, value]) => {
    obj[key] = value || '—';
  });
  return obj;
}

async function mapOnlyNeededAttributesForCounseling(counseling) {
  const {images, documents} = await filterAndMapDocuments(counseling.images?.items || []);

  return {
    date: counseling.date?.trim(),
    dateSent: counseling.dateSent?.trim(),
    status: counseling.status?.trim(),
    signature: counseling.signature?.trim(),
    dateSigned: counseling.dateSigned?.trim(),
    severity: counseling.severity?.option?.trim(),
    counselingNotes: counseling.counselingNotes?.trim(),
    consequencesOfFailure: counseling.consequencesOfFailure?.trim(),
    priorDiscussionSummary: counseling.priorDiscussionSummary?.trim(),
    correctiveActionSummary: counseling.correctiveActionSummary?.trim(),
    user: {
      firstName: counseling.user?.firstName?.trim(),
      lastName: counseling.user?.lastName?.trim(),
    },
    infractions: mapInfractions(counseling.infractions?.items || []),
    attachments: {
      images,
      documents,
    },
  };
}

function normalizeStaffData(staffData, timezone) {
  const isoFormat = 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]';

  staffData.counselings = staffData.counselings.map((counseling) => {
    const { date, dateSent, dateSigned, status } = counseling;

    counseling.status = parseCounselingStatusValueToLabel(status);

    counseling.date = date? moment(date, isoFormat).format("MM/DD/YYYY") : '';
    counseling.dateSent = dateSent? moment(dateSent, isoFormat).format("MM/DD/YYYY") : '';
    counseling.dateSigned = dateSigned? moment(dateSigned, isoFormat).format("MM/DD/YYYY") : '';
    counseling.timeSigned = dateSigned? moment(dateSigned, isoFormat).format('hh:mm A') : '--:--';

    counseling.user = setEmDashOnEmptyNullFields(counseling.user);

    counseling.infractions = counseling.infractions.map((infraction) => {
      infraction.date = parseDatetimeToDate(infraction.date, timezone);
      infraction.time = infraction.time ? infraction.time : '--:--';
      setEmDashOnEmptyNullFields(infraction);
      return infraction;
    });

    setEmDashOnEmptyNullFields(counseling);
    return counseling;
  });

  setEmDashOnEmptyNullFields(staffData);
  return staffData;
}

async function getImageDimensions(s3Key) {
  if (!s3Key) return '';

  const { width, height } = await (new Promise(async (resolve) => {
    const url = await Storage.get(s3Key);

    const image = new Image();
    image.crossOrigin = 'Anonymous';
    image.src = url;

    image.onload = function() {
      const canvas = document.createElement('canvas');
      const context = canvas.getContext('2d');
      canvas.height = this.naturalHeight;
      canvas.width = this.naturalWidth;
      context.fillStyle = "#fff";
      context.fillRect(0, 0, canvas.width, canvas.height);

      context.drawImage(this, 0, 0);

      resolve({
        width: canvas.width,
        height: canvas.height,
      });
    };
  }));

  return { width, height };
}

function createLinkAndClickToDownloadFile(url) {
  const a = document.createElement('a');
  a.target = 'blank';
  a.href = url;

  a.click();
}


export default {
  computed: {
    ...mapState([
      'userInfo',
      'isGeneratingCounselingFiles',
    ]),
  },

  methods: {
    ...mapMutations([
      'setIsGeneratingCounselingFiles',
    ]),

    async $_Counseling_download(staffID, counselingList=[]) {
      if (this.isGeneratingCounselingFiles) {
        this.displayUserNotification({
          title: `Counseling files`,
          type: "info",
          message: `Some counseling files are still being processed ...`
        });
        return;
      }

      try {
        const hasCounselings = (counselingList?.length > 0);
        if (!hasCounselings) {
          const msg = 'No counselings available to generate files';
          throw new Error(msg);
        }

        const addPluralS = (counselingList.length > 1)? 's': '';
        this.displayUserNotification({
          title: `Generating file${addPluralS}`,
          type: "info",
          message: `Generating counseling file${addPluralS} ...`
        });

        this.setIsGeneratingCounselingFiles(true);

        const input = { id: staffID };
        const {data: {getStaff}} = await this.api(getStaffNames, input);

        getStaff.companyName = this.userInfo.tenant?.companyName;
        getStaff.companyLogoS3Key = this.userInfo.tenant?.logo
        getStaff.counselings = await Promise.all(counselingList.map(mapOnlyNeededAttributesForCounseling));

        const timezone = this.userInfo.tenant?.timeZone;
        const staffData = normalizeStaffData(getStaff, timezone);

        const apiName = 'generateCounselingFilesApi';
        const path = '/generate-counseling-files';
        const options = {
          body: staffData,
        };

        const { url } = await API.post(apiName, path, options);
        createLinkAndClickToDownloadFile(url);
      }
      catch(e) {
        this.displayUserError(e);
      }
      finally {
        this.setIsGeneratingCounselingFiles(false);
      }
    },
  },
}
