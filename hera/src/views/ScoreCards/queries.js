export const staffScorecardsGroupUnmatched = /* GraphQL */ `
  query StaffScorecardsGroupUnmatched(
    $group: String
    $matchedSYearWeek: ModelStaffScoreCardByGroupUnmatchedCompositeKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelStaffScoreCardFilterInput
    $limit: Int
    $nextToken: String
  ) {
    staffScorecardsGroupUnmatched(
      group: $group
      matchedSYearWeek: $matchedSYearWeek
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        matched
        matchedS
        week
        year
        name
        transporterId
        overallTier
        delivered
        keyFocusArea
        ficoScore
        seatbeltOffRate
        dcr
        dar
        swcPod
        swcCc
        swcSc
        swcAd
        dnrs
        podOpps
        ccOpps
        speedingEventRate
        harshBrakingRate
        harshBrakingEvent
        distractionsRate
        followingDistanceRate
        signSignalViolationsRate
        updatedAt
      }
      nextToken
    }
  }
`;

export const staffScorecardsByGroupAndTransporterId = /* GraphQL */ `
  query StaffScorecardsByGroupAndTransporterId(
    $group: String
    $transporterId: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelStaffScoreCardFilterInput
    $limit: Int
    $nextToken: String
  ) {
    staffScorecardsByGroupAndTransporterId(
      group: $group
      transporterId: $transporterId
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        week
        year
        keyFocusArea
        updatedAt
      }
      nextToken
    }
  }
`;

export const staffIndex = /* GraphQL */ `
query StaffsByGroup(
  $group: String
  $firstName: ModelStringKeyConditionInput
  $sortDirection: ModelSortDirection
  $filter: ModelStaffFilterInput
  $limit: Int
  $nextToken: String
) {
  staffsByGroup(
    group: $group
    firstName: $firstName
    sortDirection: $sortDirection
    filter: $filter
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      group
      transporterId
      firstName
      lastName
      latestScorecard
    }
    nextToken
  }
}
`;
export const getStaffScoreCard = /* GraphQL */ `
  query GetStaffScoreCard($id: ID!) {
    getStaffScoreCard(id: $id) {
      id
      group
      matched
      matchedS
      messageHasBeenSent
      week
      year
      name
      transporterId
      overallTier
      delivered
      keyFocusArea
      ficoScore
      seatbeltOffRate
      dcr
      dar
      swcPod
      swcCc
      swcSc
      swcAd
      dnrs
      podOpps
      ccOpps
      speedingEventRate
      harshBrakingRate
      harshBrakingEvent
      createdAt
      updatedAt
    }
  }
`;