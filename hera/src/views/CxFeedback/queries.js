export const cxFeebackByGroupUnmatched = /* GraphQL */ `
  query CxFeebackByGroupUnmatched(
    $group: String
    $matchedSYearWeek: ModelStaffCxFeedbackByGroupUnmatchedCompositeKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelStaffCxFeedbackFilterInput
    $limit: Int
    $nextToken: String
  ) {
    cxFeebackByGroupUnmatched(
      group: $group
      matchedSYearWeek: $matchedSYearWeek
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        group
        matched
        matchedS
        week
        year
        name
        transporterId
        positiveFeedback
        negativeFeedback
        deliveryWasGreat
        deliveryWasntGreat
        totalDeliveries
        respectfulOfProperty
        followedInstructions
        friendly
        aboveAndBeyond
        deliveredWithCare
        careForOthers
        mishandledPackage
        drivingUnsafely
        driverUnprofessional
        notDeliveredToPreferredLocation
        updatedAt
      }
      nextToken
    }
  }
`;

export const cxFeebackByGroupAndTransporterId = /* GraphQL */ `
  query CxFeebackByGroupAndTransporterId(
    $group: String
    $transporterId: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelStaffCxFeedbackFilterInput
    $limit: Int
    $nextToken: String
  ) {
    cxFeebackByGroupAndTransporterId(
      group: $group
      transporterId: $transporterId
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        week
        year
        updatedAt
      }
      nextToken
    }
  }
`;

export const staffIndex = /* GraphQL */ `
query StaffsByGroup(
  $group: String
  $firstName: ModelStringKeyConditionInput
  $sortDirection: ModelSortDirection
  $filter: ModelStaffFilterInput
  $limit: Int
  $nextToken: String
) {
  staffsByGroup(
    group: $group
    firstName: $firstName
    sortDirection: $sortDirection
    filter: $filter
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      group
      transporterId
      firstName
      lastName
    }
    nextToken
  }
}
`;

export const getStaffCxFeedback = /* GraphQL */ `
  query GetStaffCxFeedback($id: ID!) {
    getStaffCxFeedback(id: $id) {
      id
      group
      matched
      matchedS
      week
      year
      name
      messageHasBeenSent
      transporterId
      positiveFeedback
      negativeFeedback
      deliveryWasGreat
      deliveryWasntGreat
      totalDeliveries
      respectfulOfProperty
      followedInstructions
      friendly
      aboveAndBeyond
      deliveredWithCare
      careForOthers
      mishandledPackage
      drivingUnsafely
      driverUnprofessional
      notDeliveredToPreferredLocation
      createdAt
      updatedAt
    }
  }
`;

export const getStaffLatestScorecard = /* GraphQL */ `
  query GetStaff($id: ID!) {
    getStaff(id: $id) {
      latestScorecard
    }
  }
`;