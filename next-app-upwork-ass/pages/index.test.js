import React from 'react';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import Form from './index';

test('Form renders correctly', () => {
  render(<Form />);
});


// test('Submitting the form with valid data', async () => {
//   // Render the form
//   render(<Form />);

//   // Fill in form fields
//   userEvent.type(screen.getByLabelText('User name'), 'Test User');
//   userEvent.type(screen.getByLabelText('Country'), 'USA');
//   userEvent.type(screen.getByLabelText('Tax identifier'), '1234-ABC-56789');

//   // Submit the form
//   fireEvent.click(screen.getByText('Submit'));

//   // Assert that the alert function was called with the correct message
//   expect(alert).toHaveBeenCalledWith('Form submitted successfully!');
// });


test('displays error message if username is less than 3 characters', async () => {
  render(<Form />);
  const submitButton = screen.getByRole('button', { name: 'Submit' });

  fireEvent.click(submitButton);

  await waitFor(() => {
    expect(screen.getByText('Username must be at least 3 characters long')).toBeInTheDocument();
  });
});


test('displays error message if an invalid country is selected', async () => {
  render(<Form />);
  const countryInput = screen.getByLabelText('Country');
  const submitButton = screen.getByRole('button', { name: 'Submit' });

  userEvent.type(countryInput, 'InvalidCountry');
  fireEvent.click(submitButton);

  await waitFor(() => {
    expect(screen.getByText('Please select a valid country')).toBeInTheDocument();
  });
});



