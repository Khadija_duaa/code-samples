import React, { useState, useCallback } from 'react';
import debounce from 'lodash.debounce';
import UserInput from './components/UserInput';
import { countries } from './utils/countryList';

const Form = ({ onSubmit }) => {
  const [username, setUsername] = useState('');
  const [country, setCountry] = useState('');
  const [taxIdentifier, setTaxIdentifier] = useState('');
  const [errors, setErrors] = useState({});

  // Debounce after 500ms
  const debouncedSetUsername = debounce((value) => setUsername(value), 500); 
  const debouncedSetTaxIdentifier = debounce((value) => setTaxIdentifier(value), 100); 

  // Memoize handleUsernameChange using useCallback
  const handleUsernameChange = useCallback((e) => {
    debouncedSetUsername(e.target.value);
  }, []);

  // Memoize handleCountryChange using useCallback
  const handleCountryChange = useCallback((value) => {
    setCountry(value);
  }, []);

  // Memoize handleTaxChange using useCallback
  const handleTaxChange = useCallback((e) => {
    debouncedSetTaxIdentifier(e.target.value);
  }, []);

  // Define array of UserInput props
  const userInputProps = [
    {
      label: "User name",
      type: "text",
      isInputSearchable: false,
      value: username,
      onChange: handleUsernameChange,
      errorMessage: errors.username
    },
    {
      label: "Country",
      type: "text",
      value: country,
      isInputSearchable: true,
      onChange: handleCountryChange,
      errorMessage: errors.country
    },
    {
      label: "Tax identifier",
      type: "text",
      value: taxIdentifier,
      countryName: country,
      onChange: handleTaxChange,
      isInputSearchable: false,
      errorMessage: errors.taxIdentifier
    }
  ];

  const handleSubmit = async (e) => {
    e.preventDefault();
    // Perform client-side validation
    const validationErrors = {};
    if (!username || username.length < 3) {
      validationErrors.username = 'Username must be at least 3 characters long';
    }

    if (!countries.includes(country)) {
      validationErrors.country = 'Please select a valid country';
    }

    // Validate tax identifier based on country
    if (country === 'USA') {
      if (!/^\d{4}-[A-Za-z]{3}-\d{5,7}$/.test(taxIdentifier)) {
        validationErrors.taxIdentifier = 
        'Tax identifier must be in the format [4 digits]-[3 letters]-[5 or 7 digits]';
       }
      } else if (country === 'Canada') {
        if (!/^(?:\d{10}|[A-Za-z]{10})-[A-Za-z]{2}$/.test(taxIdentifier)) {
          validationErrors.taxIdentifier =
            'Tax identifier must be in the format [10 digits or letters A,B,D]-[2 letters]'
       }
      }

    setErrors(validationErrors);
    if (Object.keys(validationErrors).length === 0) {
      try {
        const response = await fetch('/api/submit-form', {
          method: 'POST',
          body: JSON.stringify({ username, country, taxIdentifier }),
          headers: {
            'Content-Type': 'application/json'
          }
        });
        alert('Form submitted successfully!');
        // Clear form fields on success
        setUsername('');
        setCountry('');
        setTaxIdentifier('');
      } catch (error) {
        console.error('Error:', error);
        alert('Failed to submit form', error);
      }
    }
  };

  return (
    <form onSubmit={handleSubmit} className='flex flex-row mt-10'>
      {/* Map over userInputProps array to render UserInput components */}
      {userInputProps.map((props, index) => (
        <UserInput key={index} {...props} />
      ))}
      <button
        type="submit"
        className="bg-blue-500 text-white px-4 py-2 rounded-md hover:bg-blue-600 self-center mt-4"
      >
        Submit
      </button>
    </form>
  );
};

export default Form;
