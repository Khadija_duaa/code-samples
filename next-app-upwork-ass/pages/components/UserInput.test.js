import React from 'react';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import UserInput from './UserInput';
import Form from '../index';


test('Renders without errors', () => {
  render(<UserInput label="Test Label" />);
});

test('Displays label correctly', () => {
    const { getByText } = render(<UserInput label="Test Label" />);
    const labelElement = getByText('Test Label');
    expect(labelElement).toBeInTheDocument();
  });
  

test('renders error message when passed', () => {
  render(<UserInput errorMessage="Invalid input" />);
  const error = screen.getByText('Invalid input');
  expect(error).toBeInTheDocument();
});

test('renders empty placeholder text for countries other than Canada and USA', () => {
    render(<UserInput label="Tax identifier" countryName="Mexico" />);
    const input = screen.getByLabelText('Tax identifier');
    expect(input).toHaveAttribute('placeholder', ''); // Assert placeholder is empty string
  });


test('handles errors gracefully', () => {
    // Render UserInput components for each input with appropriate props
    render(<UserInput label="User name" errorMessage="Invalid username" />);
    render(<UserInput label="Country" errorMessage="Invalid country" />);
    render(<UserInput label="Tax identifier" errorMessage="Invalid input" />);

    // Simulate providing invalid input for each input
    const userInput = screen.getByLabelText('User name');
    userEvent.type(userInput, 'invalid-username');

    const countryInput = screen.getByLabelText('Country');
    userEvent.type(countryInput, 'invalid-country');

    const taxIdentifierInput = screen.getByLabelText('Tax identifier');
    userEvent.type(taxIdentifierInput, 'invalid-data');

    // Ensure error messages are displayed for each input
    expect(screen.getByText('Invalid username')).toBeInTheDocument();
    expect(screen.getByText('Invalid country')).toBeInTheDocument();
    expect(screen.getByText('Invalid input')).toBeInTheDocument();
});

test('clears error message on valid input', () => {
    render(<Form />);
    
    // Invalid input for username field
    const usernameInput = screen.getByLabelText('User name');
    userEvent.type(usernameInput, 'invalid-username');
  
    // Invalid input for country field
    const countryInput = screen.getByLabelText('Country');
    userEvent.type(countryInput, 'invalid-country');
  
    // Invalid input for tax identifier field
    const taxIdentifierInput = screen.getByLabelText('Tax identifier');
    userEvent.type(taxIdentifierInput, 'invalid-data');
  
    // Clear input fields
    userEvent.clear(usernameInput);
    userEvent.clear(countryInput);
    userEvent.clear(taxIdentifierInput);
  
    // Provide valid input for each field
    userEvent.type(usernameInput, 'valid-username');
    userEvent.type(countryInput, 'valid-country');
    userEvent.type(taxIdentifierInput, 'XXXXXX-XX'); // Valid Canadian tax identifier
  
    // Find and click the submit button
    const submitButton = screen.getByRole('button', { name: 'Submit' });
    fireEvent.click(submitButton);
  
    // Error message should disappear for all fields
    expect(screen.queryByText('Invalid username')).toBeNull();
    expect(screen.queryByText('Invalid country')).toBeNull();
    expect(screen.queryByText('Invalid input')).toBeNull();
  });

// test('renders options when input changes and is searchable', async () => {
//     render(<UserInput label="Country" isInputSearchable countries={countries} />);
//     const input = screen.getByRole('textbox');
//     userEvent.type(input, 'USA');
  
//     // Wait for the options to appear
//     await waitFor(() => {
//         const options = screen.getAllByText(/USA/i);
//         expect(options).toHaveLength(1);
//       }, { timeout: 5000 });
//   });


// test('Selects option correctly when clicked', () => {
//     const onChange = jest.fn();
//     const { getByRole, getByText } = render(
//       <UserInput label="Country" isInputSearchable onChange={onChange} />
//     );
//     const inputElement = getByRole('textbox');

//     fireEvent.change(inputElement, { target: { value: 'test' } });
//     const optionElement = getByText('test option');
//     fireEvent.click(optionElement);
//     expect(onChange).toHaveBeenCalledWith('test option');
//   });
  
test('renders placeholder text for USA tax identifier', () => {
    render(<UserInput label="Tax identifier" countryName="USA" />);
    const input = screen.getByRole('textbox');
    expect(input).toHaveAttribute('placeholder', 'XXXX-XXX-XXXXX or XXXX-XXX-XXXXXXX');
  });
  
test('renders placeholder text for Canada tax identifier', () => {
    render(<UserInput label="Tax identifier" countryName="Canada" />);
    const input = screen.getByRole('textbox');
    expect(input).toHaveAttribute('placeholder', 'XXXXXXXXXX-XX');
  });
  
  

  