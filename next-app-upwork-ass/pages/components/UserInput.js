import React, { memo, useState, useEffect, useCallback } from 'react';
import debounce from 'lodash.debounce';
import { countries } from '../utils/countryList';

const UserInput = ({ label, type, value, onChange, isInputSearchable, countryName,  errorMessage }) => {

  console.log("Input component rendered")
  const [inputValue, setInputValue] = useState('');
  const [showOptions, setShowOptions] = useState(false);
  const [placeholderText, setPlaceholderText] = useState('');

  useEffect(() => {
    // Update placeholder text when the country changes
    setPlaceholderText(getPlaceholder());
  }, [countryName]);

  const debouncedSetInputValue = debounce((value) => setInputValue(value), 100);

  const handleInputChange = useCallback((e) => {
    const value = e.target.value;
      debouncedSetInputValue(value);
      onChange(value);
      setShowOptions(true);
  }, [inputValue, onChange]);

  const handleSelectOption = (country) => {
    setInputValue(country);
    onChange(country);
    setShowOptions(false);
  };

  const getPlaceholder = () => {
    if (label === 'Tax identifier') {
      // No placeholder if input value is empty
       if (inputValue.trim() === '' && (countryName !== "USA" && countryName !== "Canada")) return '';
      if (countryName === "USA") {
         return 'XXXX-XXX-XXXXX or XXXX-XXX-XXXXXXX';
      } else if (countryName === "Canada") {
        return 'XXXXXXXXXX-XX';
      }
      else {
        return ''; 
      }
    }
    return '';
  };
  
  return (
    <div className="mb-4 mr-5">
      <label className="block text-gray-700" htmlFor={label}>{label}</label>
      <div className="relative">
       <input
        type={type}
        id={label}
        placeholder={placeholderText}
        className="border border-gray-300 rounded-md px-2 py-2 mt-1 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 block"        
        onChange={isInputSearchable ? handleInputChange : onChange}
       />
         {errorMessage && (
          <p className="text-red-500 mt-1 absolute left-0 -bottom-15 w-full">{errorMessage}</p>
        )}
      </div>
      {isInputSearchable && showOptions && (
        <div className='z-10 absolute max-h-40 overflow-y-auto'>
          {countries && countries.filter((country) =>
              country.toLowerCase().includes(inputValue.toLowerCase())
            ).map((country, index) => (
              <div
                key={index}
                className="px-4 py-2 cursor-pointer hover:bg-blue-50 bg-gray-100"
                onClick={() => handleSelectOption(country)}
              >
                {country}
              </div>
            ))}
        </div>
        )}
    </div>
  );
};

export default memo(UserInput);