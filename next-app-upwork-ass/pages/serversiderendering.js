import React from 'react';

const ApiExample = ({ posts, loading, error }) => (
  <div>
    {loading && <p>Loading...</p>}
    {error && <p>Error: {error.message}</p>}
    {posts && posts.length > 0 && (
      <div>
        <h1>Posts from JSONPlaceholder API</h1>
        <ul>
          {posts.map(post => (
            <li key={post.id}>{post.title}</li>
          ))}
        </ul>
      </div>
    )}
  </div>
);

export async function getServerSideProps() {
  // Function to fetch data from the JSONPlaceholder API
  try {
    const response = await fetch('https://jsonplaceholder.typicode.com/posts');
    if (!response.ok) {
      throw new Error('Network response was not ok');
    }
    const posts = await response.json();

    return {
      props: {
        posts,
        loading: false,
        error: null,
      },
    };
  } catch (error) {
    return {
      props: {
        posts: [],
        loading: false,
        error: { message: 'Error fetching data' },
      },
    };
  }
}

export default ApiExample;
