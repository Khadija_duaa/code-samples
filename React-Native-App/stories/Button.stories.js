import React from 'react';
import { storiesOf } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';
import {CustomButton} from '../components/CustomButton';
import { BufferView } from './decorators';

storiesOf('Button', module)
    .addDecorator(BufferView)
    .add('default', () => (
        <CustomButton onPress={action('tapped-default')}>Press Me</CustomButton>
    ))
    .add('outline', () => (
        <CustomButton onPress={action('tapped-outline')} outline ={true}>
            Press Me
        </CustomButton>
    ));