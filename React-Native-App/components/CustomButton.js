import React from 'react';
import {View, TouchableOpacity, Text, TextInput, StyleSheet, SafeAreaView, Dimensions} from 'react-native';
import colors from '../utils/colors';

const screen = Dimensions.get('window');

const styles = StyleSheet.create({

    button: {
        paddingVertical: 10,
        width: screen.width * 0.85,
        backgroundColor: colors.border,
        marginVertical: screen.height * 0.010,
    },
    buttonOutline: {
        paddingVertical: 10,
        width: screen.width * 0.85,
        backgroundColor: colors.white,
        borderColor: colors.border,
        marginVertical: screen.height * 0.010,
    },
    buttonText: {
        color:'#4F6D7A',
        textAlign: 'center'
    },
    buttonTextOutline: {
        color:'black',
        textAlign: 'center'
    },
});

export const CustomButton = (props) => {


    return (
        <View >
            <TouchableOpacity  style={props.outline ? styles.buttonOutline : styles.button}
                               onPress={props.onPress} disabled={props.disabled} {...props}>
                <Text style={props.outline ? styles.buttonTextOutline: styles.buttonText} {...props}>{props.text}</Text>
            </TouchableOpacity>
        </View>
    );
};