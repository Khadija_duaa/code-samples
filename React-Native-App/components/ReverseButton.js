import React from 'react';
import { TouchableOpacity, Image, StyleSheet, Text } from 'react-native';
import colors from '../utils/colors';

const styles = StyleSheet.create({
    button: {
        marginVertical: 7,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonIcon: {
        marginRight: 10,
        width: 18,
        height: 18,
    },
    buttonText: {
        color: colors.white,
        fontSize: 14,
    },
});

export const ReverseButton = (props) => {
    return (
        <TouchableOpacity onPress={props.onPress} style={styles.button}>
            <Image
                source={require('../assets/images/reverse.png')}
                style={styles.buttonIcon}
                resizeMode="contain"
            />
            <Text style={styles.buttonText}>{props.text}</Text>
        </TouchableOpacity>
    );
};