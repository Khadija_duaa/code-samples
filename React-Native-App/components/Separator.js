import React from 'react';
import { View} from 'react-native';


export const Separator = (props) => <View style={props.style} />;