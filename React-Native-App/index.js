import {AppRegistry} from 'react-native';
import React from 'react';
import App from './App';
import {name as appName} from './app.json';
export default from './storybook';
AppRegistry.registerComponent(appName, () => App);
