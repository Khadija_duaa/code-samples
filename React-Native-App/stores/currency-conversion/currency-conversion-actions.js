import axios from 'axios';
import {Alert} from 'react-native'
import * as actions from './currency-conversion-action-types';
import {handleError} from "../store-utils";


export const loadCurrencyRates = (currency) => {

    return (dispatch) => {
        dispatch(setProcessing(true));

        axios.get(`http://fixer.handlebarlabs.com/latest?base=${currency}`)
            .then((response) => {

                console.log('its response', response);
                let currencyRateList= [];
                Object.entries(response && response.data && response.data.rates).forEach
                (([key, value]) => currencyRateList.push({title: key, rate:value}));

                console.log('its currencyRateList', currencyRateList);


                dispatch(setCurrencyRates(currencyRateList));
                dispatch(setProcessing(false));

            })
            .catch(err => {
                console.log('its error', err);

                let message = handleError(err);
                Alert.alert(message);
                dispatch(setCurrencyRates({
                    error: message
                }));
                dispatch(setProcessing(false));
            });
    };
};


const setCurrencyRates = (payload) => {
    return {
        type: actions.CURRENCY_RATES,
        payload
    }
};

export const setFavoriteCurrencyList = (payload) => {
    return {
        type: actions.FETCH_FAVORITE_CURRENCY_LIST,
        payload
    }
};

export const setThemeColor = (payload) => {
    return {
        type: actions.THEME_COLOR,
        payload
    }
};

export const setUserAuthenticated = (payload) => {
    return {
        type: actions.SET_USER_AUTHENTICATED,
        payload
    }
};

const setProcessing = (processing) => {
    return {
        type: actions.PROCESSING,
        payload: processing
    };
};

