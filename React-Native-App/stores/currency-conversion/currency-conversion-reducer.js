import AsyncStorage from "@react-native-community/async-storage";
import * as actions from './currency-conversion-action-types';
import currencies from '../../utils/currencies';

const loadStateFromLocalStorage = () => {
    let currencyConversionState = AsyncStorage.getItem('state');

    if (currencyConversionState && currencyConversionState.length > 0) {
        return JSON.parse(currencyConversionState);
    }

    return null;
};

const setStoreData = async (key, data) =>{
    try {
        if(data)
        {
            await AsyncStorage.setItem(key, data);
        }
        else
        {
            await AsyncStorage.removeItem(key);
        }
    } catch (err)
    {

    }

};

export const initState = () => {
    return {
        currencyRateList: [],
        processing:false,
        color: '#4f6d7a',
        authenticated: false,
        baseCurrency:'USD',
        quoteCurrency:'GBP',
        baseCurrencyUserValue:'1' ,
        conversionRate: 0.89824,
        currencyList : currencies
    };
};

const _initState = () => {

    const prevState = loadStateFromLocalStorage();
    if (!prevState) {
        return ({...initState()})
    }
    return prevState;
};

const reducer = (state = _initState(), action) => {
    let newState = {...state};

    switch (action.type) {
        case actions.CURRENCY_RATES:
            setCurrencyRates(newState, action.payload);
            break;
        case actions.THEME_COLOR:
            setThemeColor(newState, action.payload);
            break;
        case actions.SET_USER_AUTHENTICATED:
            setUserAuthenticated(newState, action.payload);
            break;
        case actions.FETCH_FAVORITE_CURRENCY_LIST:
            setFavoriteCurrencyList(newState, action.payload);
            break;
        case actions.PROCESSING:
            setProcessing(newState, action.payload);
            break;

        default : {
            // do nothing
        }
    }
    return newState;
};


export const setCurrencyRates = (state, data) => {
    state.currencyRateList = data
};

const setFavoriteCurrencyList = (state, data) => {

    let _currencyList = [...state.currencyList];

    for(let i = 0 ; i < _currencyList.length ; i++)
    {
        for ( let k = 0 ; k < data.length ; k++) {
            if (_currencyList[i].title === data[k].title) {

                _currencyList[i].iconName = 'heart';
                break;
            }
        }
    }
    state.currencyList = _currencyList;
};

export const setThemeColor = (state, data) => {
    state.color = data;
    setStoreData('color', JSON.stringify(data)).then(r => r);
};

export const setUserAuthenticated = (state, data) => {
    state.authenticated = data
};

export const setProcessing = (state, processing) => {
    state.processing = processing;
};



export default reducer;
