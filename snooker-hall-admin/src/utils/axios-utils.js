import axios from "./axios";
import {NotificationManager} from "react-notifications";
import {handleError} from "../stores/store-utils";

export const membershipSetAsDefault = (_id) => {
  return new Promise((resolve, reject) => {
    let url = `/membership-default/${_id}`;

    axios.put(url)
      .then((resp) => {
        NotificationManager.success(resp.data['_message'], '', 3000);
        resolve();
      })
      .catch(err => {
        let message = handleError(err);
        NotificationManager.error(message, '', 3000);
        reject();
      });
  })
};
