export const getObjectValue = (obj, path) => {
  let val = null;

  if (path.indexOf('.') > -1) {
    let paths = path.split('.');
    val = obj;
    paths.forEach(_path => {
      val = val[_path];
    });
  }
  else {
    val = obj[path];
  }

  return val;
};

export const capitalize = (str) => {
  if (str) {
    str = str.toLowerCase();
    let words = str.split(' ');

    const capitalizedWords = [];

    words.forEach(word => {
      capitalizedWords.push(`${word.charAt(0).toUpperCase()}${word.length > 1 ? word.substr(1) : ''}`);
    });

    return capitalizedWords.join(' ');
  }

  return str;
};
