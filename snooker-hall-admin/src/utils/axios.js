import axios from 'axios';
import {store} from '../index';
import serverConfig from './config.json';

const getOptions = () => {
  return {
    headers: {
      'X-Auth': `${store.getState().auth.token}`,
    }
  };
};

const prepareUrl = (api) => (serverConfig.serverUrl + api);

const wrapper = {
  get: (api) => axios.get(prepareUrl(api), getOptions()),
  post: (api, formData = {}) => axios.post(prepareUrl(api), formData, getOptions()),
  put: (api, formData = {}) => axios.put(prepareUrl(api), formData, getOptions()),
  delete: (api) => axios.delete(prepareUrl(api), getOptions()),
};

export default wrapper;
