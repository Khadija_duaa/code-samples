import * as actions from './users-action-types';

const initState = () => {
  return {
    user: null,
    users: [],
    pageNumbers: [],
    processing: false,
    error: null,
    recordsPerPage: 10,
    currentPage: 1,
    indexOfLastTodo: null,
    indexOfFirstTodo: null,
    activeUsers: []
  };
};

const reducer = (state = initState(), action) => {
  let newState = {...state};

  switch (action.type) {
    case actions.USERS:
      setUsers(newState, action.payload);
      break;
    case actions.PROCESSING:
      setProcessing(newState, action.payload);
      break;
    case actions.Pagination:
      setPagination(newState, action.payload);
      break;
    case actions.USER:
      setUser(newState, action.payload);
      break;
    case actions.EDIT_USERPROFILE:
      updateUserProfile(newState, action.payload);
      break;
    case actions.ADD_TOPUP_BALANCE:
      topupBalance(newState, action.payload);
      break;
    case actions.RESET_REDUX:
      resetRedux(newState);
      break;
    default : {
      // do nothing
    }

  }

  return newState;
};

const resetRedux = (state) => {
  state.user = null;
  state.error = null;
  state.message = null;
};

const updateUserProfile = (state, data) => {
  state.message = data.message;
};
const topupBalance = (state, data) => {
  state.message = data.message;
};

const setUsers = (state, data) => {
  console.log("User Reducer: ", data)
  state._users = data._users;
  state.users = data.users;
  state.error = data.error;

  state.activeUsers = [];
  // Update Pagination data
  if (state.users && state.users.length) {
    state.totalPages = Math.ceil(state.users.length / state.recordsPerPage);
    setPagination(state, 1);
  }
};
const setUser = (state, data) => {
  state.user = data;
};

const setProcessing = (state, processing) => {
  state.processing = processing;
};

const setPagination = (state, currentPage) => {
  let allUsers = [...state.users];

  let firstIndex = (currentPage - 1) * state.recordsPerPage;

  let lastIndex = firstIndex + state.recordsPerPage;
  if (lastIndex > allUsers.length) {
    lastIndex = allUsers.length;
  }

  state.activeUsers = allUsers.slice(firstIndex, lastIndex);
  state.currentPage = currentPage;


};

export default reducer;
