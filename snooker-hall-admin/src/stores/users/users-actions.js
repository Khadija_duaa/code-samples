import axios from '../../utils/axios';
import * as actions from './users-action-types';
import {NotificationManager} from 'react-notifications';
import {getObjectValue} from '../../utils/common-utils';
import {handleError} from "../store-utils";

export const loadUsers = (membershipUsers) => {
  return (dispatch, getState) => {
    dispatch(setProcessing(true));

    let url = (membershipUsers && getState().membership.membership) ? `membership-users/${getState().membership.membership._id}` : 'users';

    axios.get(`/${url}`)
      .then((response) => {
        console.log('response', response);
        let {users} = response.data;

        dispatch(setUsers({users: users.sort((a, b) => a.firstName.localeCompare(b.firstName))}));
        dispatch(setProcessing(false));
      })
      .catch(err => {
        let message = handleError(err);

        NotificationManager.error(message, '', 3000);

        dispatch(setUsers({error: message, users: []}));
        dispatch(setProcessing(false));
      });
  };
};

export const updateUserProfile = (data, callback) => {
  return (dispatch, getState) => {
    dispatch(setProcessing(true));

    axios.post(`/user-profile/${getState().users.user._id}`, data)
      .then((response) => {
        console.log('response', response);
        let {data} = response;

        console.log(data);
        NotificationManager.success(data._message, '', 3000);

        if (data) {
          dispatch(updateUserprofile({
            message: data._message,
          }));
        }
        dispatch(setProcessing(false));

        callback && callback();
      })
      .catch(err => {
        let message = handleError(err);

        NotificationManager.error(message, '', 3000);

        dispatch(updateUserprofile({error: message}));
        dispatch(setProcessing(false));
      });
  };
};
export const setTopUpBalance = (data, callback) => {
  return (dispatch, getState) => {
    dispatch(setProcessing(true));

    axios.post(`/top-up/${getState().users.user._id}`, data)
      .then((response) => {
        console.log('response', response);

        let {data} = response;

        NotificationManager.success(data._message, '', 3000);

        if (data) {
          dispatch(topupBalance({message: data._message}));
          dispatch(setProcessing(false));

          callback && callback();
        }
      })
      .catch(err => {
        let message = handleError(err);
        NotificationManager.error(message, '', 3000);

        dispatch(updateUserprofile({error: message}));
        dispatch(setProcessing(false));
      });
  };
};


export const searchUsers = (searchStr, searchParams = []) => {
  return (dispatch, getState) => {
    const usersState = getState().users;

    let users = usersState._users || usersState.users;
    users = [...users];

    if (searchStr) {
      let filteredUsers = [];
      users.forEach(user => {

        for (let index = 0; index < searchParams.length; index++) {
          let param = searchParams[index];

          const _val = getObjectValue(user, param);
          if (_val && _val.toLowerCase().indexOf(searchStr.toLowerCase()) > -1) {
            filteredUsers.push(user);
            break;
          }
        }
      });

      dispatch(setUsers({_users: users, users: filteredUsers}));
    }
    else {
      dispatch(setUsers({_users: undefined, users}));
    }
  };

};

const setUsers = (users) => {
  return {
    type: actions.USERS,
    payload: users,

  }
};
const topupBalance = (balance) => {
  return {
    type: actions.ADD_TOPUP_BALANCE,
    payload: balance,

  }
};

export const setUser = (user) => {
  return {
    type: actions.USER,
    payload: user,

  }
};

const updateUserprofile = (payload) => {
  return {
    type: actions.EDIT_USERPROFILE,
    payload
  }
};

const setProcessing = (processing) => {
  return {
    type: actions.PROCESSING,
    payload: processing
  };
};

export const setPagination = (payload) => {
  return {
    type: actions.Pagination,
    payload
  };
};

export const resetRedux = () => {
  return {
    type: actions.RESET_REDUX
  };
};

