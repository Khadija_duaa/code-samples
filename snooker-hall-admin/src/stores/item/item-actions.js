import * as actions from "./item-action-types";
import {NotificationManager} from 'react-notifications';
import axios from "../../utils/axios";
import {handleError} from '../store-utils';

export const addItemCategory = (data, callback) => {
  return (dispatch) => {
    dispatch(setProcessing(true));

    axios.post('/item-category', data)
      .then((response) => {
        console.log('response', response);

        let {data} = response;

        NotificationManager.success(data._message, '', 3000);

        if (data) {
          dispatch(addItemcategory({message: data._message}));
          dispatch(setProcessing(false));

          callback && callback();
        }
      })
      .catch(err => {
        let message = handleError(err);
        NotificationManager.error(message, '', 3000);

        dispatch(setProcessing(false));
      });
  };
};

export const refreshItemCategory = () => {
  return (dispatch, getState) => {
    dispatch(setProcessing(true));

    axios.get(`/item-category/${getState().items.category._id}`)
      .then((response) => {
        let {itemCategory} = response.data;
        dispatch(setCategory(itemCategory));
        dispatch(setProcessing(false));
      })
      .catch(err => {
        let message = handleError(err);
        NotificationManager.error(message, '', 3000);
        dispatch(setProcessing(false));
      });
  };
};

export const loadCategories = () => {
  return (dispatch, getState) => {
    dispatch(setProcessing(true));

    axios.get('/item-category')
      .then((response) => {
        let {data} = response;

        const items = [];

        data.itemCategories.forEach(category => {
          category.items.forEach(item => {
            items.push({...item, categoryName: category.categoryName});
          });
        });

        console.log('items', items);

        dispatch(setCategories(data.itemCategories));
        dispatch(setItems(items));
        dispatch(setProcessing(false));
      })
      .catch(err => {
        let message = handleError(err);

        dispatch(setCategories({error: message}));
        dispatch(setProcessing(false));
      });
  };
};

export const updateItem = (data, callback) => {
  return (dispatch, getState) => {
    dispatch(setProcessing(true));

    axios.put(`/item/${data._id}`, data)
      .then((response) => {
        let {data} = response;
        NotificationManager.success(data._message, '', 3000);
        dispatch(setProcessing(false));
        callback && callback();
      })
      .catch(err => {
        let message = handleError(err);
        NotificationManager.error(message, '', 3000);
        dispatch(setProcessing(false));
      });
  };
};

export const updateCategory = (_id, data, callback) => {
  return (dispatch) => {
    dispatch(setProcessing(true));

    axios.put(`/item-category/${_id}`, data)
      .then((response) => {
        console.log('response', response);
        let {data} = response;

        console.log(data);

        NotificationManager.success(data._message, '', 3000);

        dispatch(setProcessing(false));

        callback && callback();
      })
      .catch(err => {
        let message = handleError(err);

        NotificationManager.error(message, '', 3000);
        dispatch(setProcessing(false));
      });
  };
};

export const addItem = (_id, data, callback) => {
  return (dispatch) => {
    dispatch(setProcessing(true));

    delete data._id;

    axios.post(`/item/${_id}`, data)
      .then((response) => {
        console.log('response', response);

        let {data} = response;

        NotificationManager.success(data._message, '', 3000);

        if (data) {
          dispatch(addItemAgainstCategory({message: data._message}));
          dispatch(setProcessing(false));
          callback && callback();
        }
      })
      .catch(err => {
        let message = handleError(err);
        NotificationManager.error(message, '', 3000);
        dispatch(setProcessing(false));
      });
  };
};

export const deleteItemCategory = (id, index) => {
  return (dispatch) => {

    dispatch(setProcessing(true));

    axios.delete(`/item-category/${id}`)
      .then((response) => {
        console.log('response', response);

        let {data} = response;

        NotificationManager.success(data['_message'], '', 3000);

        dispatch(_deleteItemCategory(index));
        dispatch(setProcessing(false));
      })
      .catch(err => {
        let message = handleError(err);

        NotificationManager.error(message, 'Error', 3000);
        dispatch(setProcessing(false));
      });
  };
};


export const deleteItem = (id, index) => {
  return (dispatch) => {
    dispatch(setProcessing(true));

    axios.delete(`/item/${id}`)
      .then((response) => {
        console.log('response', response);
        let {data} = response;

        NotificationManager.success(data['_message'], '', 3000);

        console.log('deleting item at index')

        dispatch(_deleteItem(index));
        dispatch(setProcessing(false));
      })
      .catch(err => {
        let message = handleError(err);

        NotificationManager.error(message, 'Error', 3000);
        dispatch(setProcessing(false));
      });
  };
};

const _deleteItem = (index) => {
  return {
    type: actions.DELETE_ITEM,
    payload: index,
  }
};

const _deleteItemCategory = (payload) => {
  return {
    type: actions.DELETE_ITEM_CAT,
    payload,
  }
};


const addItemAgainstCategory = (payload) => {
  return {
    type: actions.ADD_ITEM,
    payload

  }
};

const setCategories = (categories) => {
  return {
    type: actions.LOAD_CATEGORIES,
    payload: categories,

  }
};

const setItems = (payload) => {
  return {
    type: actions.LOAD_ITEMS,
    payload,

  }
};

const _updateItem = (payload) => {
  return {
    type: actions.EDIT_ITEM,
    payload
  }
};

export const setItem = (item) => {
  return {
    type: actions.ITEM,
    payload: item,

  }
};

export const setCategory = (category) => {
  return {
    type: actions.CATEGORY,
    payload: category,

  }
};


const addItemcategory = (payload) => {
  return {
    type: actions.ADD_ITEM_CATEGORY,
    payload

  }
};
const setProcessing = (processing) => {
  return {
    type: actions.PROCESSING,
    payload: processing
  };
};

export const resetRedux = () => {
  return {
    type: actions.RESET_REDUX
  };
};
