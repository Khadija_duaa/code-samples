import * as actions from './item-action-types';

const initState = () => {
  return {
    processing: false,
    itemCategories: [],
    items: [],
    item: null,
    category: null,
    error: null,

  };
};


const reducer = (state = initState(), action) => {
  let newState = {...state};

  switch (action.type) {
    case actions.PROCESSING:
      setProcessing(newState, action.payload);
      break;
    case actions.ADD_ITEM_CATEGORY:
      addItemCategory(newState, action.payload);
      break;
    case actions.LOAD_CATEGORIES:
      setCategories(newState, action.payload);
      break;
    case actions.LOAD_ITEMS:
      setItems(newState, action.payload);
      break;
    case actions.ITEM:
      setItem(newState, action.payload);
      break;
    case actions.CATEGORY:
      setCategory(newState, action.payload);
      break;
    case actions.ADD_ITEM:
      addItemAgainstCategory(newState, action.payload);
      break;
    case actions.EDIT_ITEM:
      _updateItem(newState, action.payload);
      break;
    case actions.DELETE_ITEM:
      deleteItem(newState, action.payload);
      break;
    case actions.DELETE_ITEM_CAT:
      deleteItemCat(newState, action.payload);
      break;
    case actions.RESET_REDUX:
      resetRedux(newState);
      break;
    default : {
      // do nothing
    }
  }

  return newState;
};

const addItemAgainstCategory = (state, data) => {
  state.message = data.message;

};

const setCategories = (state, data) => {
  state.itemCategories = data;
};

const setItems = (state, data) => {
  state.items = data;
};

const resetRedux = (state) => {
  state.category = null;
  state.item = null;
  state.error = null;
  state.message = null;
  state.itemCategories = [];
  state.items = [];
};


const _updateItem = (state, data) => {
  state.message = data.message;
};

const setItem = (state, data) => {
  state.item = data;
};

const setCategory = (state, data) => {
  state.category = data;
};


const addItemCategory = (state, data) => {
  state.message = data.message;
};

const setProcessing = (state, processing) => {
  state.processing = processing;
};

const deleteItem = (state, index) => {
  let newItems = [...state.category.items];
  newItems.splice(index, 1);
  state.category.items = newItems;
};

const deleteItemCat = (state, index) => {
  let newItems = [...state.itemCategories];
  newItems.splice(index, 1);
  state.itemCategories = newItems;
};

export default reducer;

