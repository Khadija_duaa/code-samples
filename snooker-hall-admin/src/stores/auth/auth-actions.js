import axios from '../../utils/axios';
import * as actions from './auth-action-types';
import {handleError} from '../store-utils';

const loginUser = (args) => {
  return {
    type: actions.LOGIN,
    payload: args
  };
};

const logoutUser = () => {
  return {
    type: actions.LOGOUT
  };
};

const setProcessing = (processing) => {
  return {
    type: actions.PROCESSING,
    payload: processing
  };
};

export const login = (username, password) => {
  return (dispatch) => {
    console.log(`Attempting login with following credentials [${username}], [${password}]`);
    dispatch(setProcessing(true));

    axios.post(`/admin-login`, {username, password})
      .then((response) => {
        console.log('response', response);

        const {data} = response;
        const jwt = response.headers['x-auth'];

        if (data && jwt) {
          dispatch(loginUser({
            token: jwt,
            message: data._message,
            remember: data.remember
          }));
        }
        else {
          dispatch(loginUser({
            token: null,
            message: null,
            error: 'Invalid Credentials'
          }));
        }

        dispatch(setProcessing(false));

      })
      .catch((err) => {
        let message = handleError(err);

        dispatch(loginUser({error: message}));
        dispatch(setProcessing(false));
      });
  };
};


export const logout = () => {
  return (dispatch) => {
    dispatch(logoutUser());
    dispatch({type: 'RESET_REDUX'});
  }
};
