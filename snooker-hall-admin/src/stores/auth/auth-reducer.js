import * as actions from './auth-action-types';

const loadAuthStateFromLocalStorage = () => {
  let authState = sessionStorage.getItem("authState");
  if (!authState) {
    authState = localStorage.getItem("authState");
  }

  if (authState && authState.length > 0) {
    return JSON.parse(authState);
  }

  return null;
};

const _initState = {
  token: null,
  authenticated: false,
  processing: false,
  message: null,
  error: null
};

const initState = () => {
  const prevState = loadAuthStateFromLocalStorage();
  if (!prevState) {
    return {..._initState};
  }
  return prevState;
};

const reducer = (state = initState(), action) => {
  let newState = {...state};

  switch (action.type) {
    case actions.LOGIN:
      login(newState, action.payload);
      break;

    case actions.LOGOUT:
      logout(newState, action.payload);
      break;

    case actions.PROCESSING:
      setProcessing(newState, action.payload);
      break;

    case actions.RESET_REDUX:
      console.log('Resetting Auth redux');
      newState = {..._initState};
      break;
    default : {
      // do nothing
    }
  }
  return newState;
};

const setProcessing = (state, processing) => {
  state.processing = processing;
};

const login = (state, data) => {
  state.token = data.token;
  state.message = data._message;
  state.error = data.error;

  if (!state.error && state.token) {
    state.authenticated = true;
    if (data.remember) {
      localStorage.setItem('authState', JSON.stringify(state));
    }
    else {
      sessionStorage.setItem('authState', JSON.stringify(state));
    }
  }

};

const logout = (state) => {
  state.authenticated = false;
  state.token = null;
  state.message = null;
  state.error = null;
  state.processing = false;

  localStorage.removeItem("authState");
  sessionStorage.removeItem("authState");
};

export default reducer;


