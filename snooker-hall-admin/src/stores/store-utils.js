export const handleError = (error) => {
  let _message = 'Some error occurred, Please check internet connectivity';

  console.error('error', error);

  if (error.response) console.error('error.response', error.response);

  if (error.response && error.response.data) {
    console.error('error.response.data', error.response.data);
    _message = error.response.data._message;
  }

  console.error('Error Message', _message);

  return _message;
};
