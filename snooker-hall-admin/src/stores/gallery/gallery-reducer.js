import * as actions from './gallery-action-types';

const initState = () => {
  return {
    album: null,
    albums: [],
    processing: false,
    error: null,
  };
};

const reducer = (state = initState(), action) => {
  let newState = {...state};

  switch (action.type) {
    case actions.ALBUMS:
      setAlbums(newState, action.payload);
      break;
    case actions.ALBUM:
      setAlbum(newState, action.payload);
      break;
    case actions.DELETE:
      deleteAlbum(newState, action.payload);
      break;
    case actions.PROCESSING:
      setProcessing(newState, action.payload);
      break;
    case actions.RESET:
      resetRedux(newState);
      break;
    default : {
      // do nothing
    }
  }
  return newState;
};

const resetRedux = (state) => {
  state.albums = [];
  state.album = null;
  state.error = null;
};

const setAlbums = (state, data) => {
  state.albums = data;
  state.album = null;
};

const setAlbum = (state, _id) => {
  if (typeof _id === 'object') {
    state.album = _id;
  }
  else {
    if (_id) {
      let filteredAlbums = state.albums.filter(album => album._id === _id);
      if (filteredAlbums && filteredAlbums.length) {
        state.album = filteredAlbums[0];
      }
    }
    else {
      state.album = null;
    }
  }
};

const setProcessing = (state, processing) => {
  state.processing = processing;
};

const deleteAlbum = (state, index) => {
  let newAlbums = [...state.albums];
  newAlbums.splice(index, 1);

  setAlbums(state, newAlbums);
};

export default reducer;
