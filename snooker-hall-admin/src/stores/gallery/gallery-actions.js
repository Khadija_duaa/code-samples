import * as actions from "../gallery/gallery-action-types";
import {NotificationManager} from 'react-notifications';
import axios from "../../utils/axios";
import {handleError} from '../store-utils';

const _deleteImages = async (index, images) => {
  if (index < images.length) {
    let image = images[index];
    await axios.delete(`/gallery/${image.album}/${image.url}`);
    await _deleteImages(index + 1, images);
  }

  return true;
};

export const deleteImages = (images, callback) => {
  return (dispatch) => {
    if (images && images.length) {
      dispatch(setProcessing(true));

      _deleteImages(0, images)
        .then(() => {

          NotificationManager.success('Selected images deleted successfully', '', 3000);

          callback && callback();

          dispatch(setProcessing(false));
        })
        .catch((error) => {
          let message = handleError(error);
          NotificationManager.error(message, '', 3000);
          dispatch(setProcessing(false));
        });
    }
  };
};

export const loadGallery = () => {

  return (dispatch) => {
    dispatch(setProcessing(true));

    axios.get(`/gallery`)
      .then((response) => {
        console.log(response);
        let {galleries} = response.data;
        dispatch(setAlbums(galleries));
        dispatch(setProcessing(false));
      })
      .catch(err => {
        let message = handleError(err);

        NotificationManager.error(message, '', 3000);

        dispatch(setAlbums({error: message}));
        dispatch(setProcessing(false));
      });
  };
};

const setAlbums = (payload) => {
  return {
    type: actions.ALBUMS,
    payload,
  }
};

const setProcessing = (processing) => {
  return {
    type: actions.PROCESSING,
    payload: processing
  };
};

const setAlbum = (payload) => {
  return {
    type: actions.SET_ALBUM,
    payload

  }
};

export const saveAlbum = (name, callback) => {
  return (dispatch, getState) => {
    dispatch(setProcessing(true));

    let url = `/gallery`;

    let axiosPromise = null;
    let existingAlbum = getState().gallery.album;

    let existingId = existingAlbum ? existingAlbum._id : null;

    if (existingId) {
      url += `/${existingId}`;
      axiosPromise = axios.put(url, {name});
    }
    else {
      axiosPromise = axios.post(url, {name});
    }

    axiosPromise.then((response) => {
      let {data} = response;

      NotificationManager.success(data['_message'], '', 3000);

      dispatch(setProcessing(false));
      callback && callback();
    })
      .catch(err => {
        let message = handleError(err);

        NotificationManager.error(message, '', 3000);

        dispatch(setAlbum({error: message}));
        dispatch(setProcessing(false));
      });
  };
};

export const specificAlbum = (payload) => {
  return {
    type: actions.ALBUM,
    payload,
  }
};


export const updateAlbum = (album) => {
  return (dispatch, getState) => {
    let existingAlbums = [...getState().gallery.albums];

    let albumIndex = getIndex(album._id, existingAlbums);

    if (albumIndex > -1) {
      existingAlbums[albumIndex] = album;
      dispatch(setAlbums(existingAlbums));
    }
  };
};

const getIndex = (_id, albums) => {
  let index = -1;

  if (albums && albums.length) {
    albums.forEach((album, i) => {
      if (album._id === _id) {
        index = i;
      }
    });
  }

  return index;
};

export const deleteAlbum = (album) => {
  return (dispatch, getState) => {
    dispatch(setProcessing(true));

    axios.delete(`/gallery/${album._id}`)
      .then((response) => {
        console.log('response', response);
        let {data} = response;
        let index = getIndex(album._id, getState().gallery.albums);

        dispatch(_deleteAlbum(index));
        dispatch(setProcessing(false));

        NotificationManager.success(data['_message'], '', 3000);
      })
      .catch(err => {
        let message = handleError(err);

        NotificationManager.error(message, 'Error', 3000);

        dispatch(setProcessing(false));
      });
  };
};

const _deleteAlbum = (payload) => {
  return {
    type: actions.DELETE,
    payload
  }
};

export const resetRedux = () => {
  return {
    type: actions.RESET
  }
};
