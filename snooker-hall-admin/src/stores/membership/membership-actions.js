import axios from '../../utils/axios';
import * as actions from './membership-action-types';
import {NotificationManager} from 'react-notifications';
import {handleError} from '../store-utils';

export const resetRedux = () => {
  return {
    type: actions.RESET
  }
};

const setMemberships = (memberships) => {
  return {
    type: actions.MEMBERSHIPS,
    payload: memberships,

  }
};

export const setProcessing = (processing) => {
  return {
    type: actions.PROCESSING,
    payload: processing
  };
};

export const setMembership = (membership) => {
  return {
    type: actions.MEMBERSHIP,
    payload: membership,

  }
};

const _deleteMembership = (index) => {
  return {
    type: actions.DELETE,
    payload: index,
  }
};


export const loadMemberships = () => {
  return (dispatch, getState) => {
    dispatch(setProcessing(true));

    axios.get(`/memberships`)
      .then((response) => {
        let {data} = response;

        dispatch(setMemberships(data.memberships));
        dispatch(setProcessing(false));
      })
      .catch(err => {
        let message = handleError(err);

        dispatch(setMemberships({error: message}));
        dispatch(setProcessing(false));
      });
  };
};

export const saveMembership = (membership, callback) => {
  return (dispatch, getState) => {
    dispatch(setProcessing(true));

    let url = `/memberships`;

    let axiosPromise = null;

    let existingId = getState().membership.membership ? getState().membership.membership._id : null;

    if (existingId) {
      url += `/${existingId}`;
      axiosPromise = axios.put(url, membership);
    }
    else {
      axiosPromise = axios.post(url, membership);
    }

    axiosPromise.then((response) => {
      let {data} = response;

      let responseData = {
        ...data.membership,
        message: data['_message']
      };

      NotificationManager.success(responseData.message, '', 3000);

      dispatch(setMembership(responseData));
      dispatch(setProcessing(false));

      callback && callback();
    }).catch(err => {
      let message = handleError(err);

      NotificationManager.error(message, '', 3000);

      dispatch(setMembership({error: message}));
      dispatch(setProcessing(false));
    });
  };
};

export const saveMembershipDetails = (specialRates, callback) => {
  return (dispatch, getState) => {
    dispatch(setProcessing(true));

    let existingId = getState().membership.membership ? getState().membership.membership._id : null;

    axios.put(`/membership-details/${existingId}`, {specialRates})
      .then((response) => {
        let {data} = response;

        let responseData = {
          ...data.membership,
          message: data['_message']
        };

        NotificationManager.success(responseData.message, '', 3000);

        dispatch(setMembership(responseData));
        dispatch(setProcessing(false));

        callback && callback();
      })
      .catch(err => {
        let message = handleError(err);

        NotificationManager.error(message, '', 3000);

        dispatch(setMembership({error: message}));
        dispatch(setProcessing(false));
      });
  };
};

export const deleteMembership = (id, index) => {
  return (dispatch, getState) => {

    dispatch(setProcessing(true));

    axios.delete(`/memberships/${id}`)
      .then((response) => {
        console.log('response', response);
        let {data} = response;

        NotificationManager.success(data['_message'], '', 3000);

        dispatch(_deleteMembership(index));
        dispatch(setProcessing(false));
      })
      .catch(err => {
        let message = handleError(err);

        NotificationManager.error(message, 'Error', 3000);

        dispatch(setMembership({error: message}));
        dispatch(setProcessing(false));
      });
  };
};
