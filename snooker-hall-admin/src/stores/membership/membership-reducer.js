import * as actions from './membership-action-types';

const initState = () => {
  return {
    membership: null,
    memberships: [],
    processing: false,
    error: null,
  };
};

const reducer = (state = initState(), action) => {
  let newState = {...state};

  switch (action.type) {
    case actions.MEMBERSHIPS:
      setMemberships(newState, action.payload);
      break;
    case actions.PROCESSING:
      setProcessing(newState, action.payload);
      break;
    case actions.MEMBERSHIP:
      setMembership(newState, action.payload);
      break;
    case actions.DELETE:
      deleteMembership(newState, action.payload);
      break;
    case actions.RESET:
      resetData(newState);
      break;
    default : {
      // do nothing
    }
  }

  return newState;
};

const resetData = (state) => {
  state.message = null;
  state.error = null;
  state.membership = null;
};

const setMemberships = (state, data) => {
  state.memberships = data;
  state.error = data.error;
  state.user = data.user;
};

const setProcessing = (state, processing) => {
  state.processing = processing;
};

const setMembership = (state, data) => {
  if (data.error) {
    state.error = data.error;
  } else {
    state.membership = data;
    state.message = data.message;
  }
};

const deleteMembership = (state, index) => {
  let newMemberships = [...state.memberships];
  newMemberships.splice(index, 1);
  state.memberships = newMemberships;

};

export default reducer;
