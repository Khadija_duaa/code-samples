import React, {Component} from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultFooter extends Component {
  render() {

    // eslint-disable-next-line
    const {children, ...attributes} = this.props;

    return (
      <React.Fragment>
        <span><a target='_blank' rel="noopener noreferrer"
                 href="https://www.facebook.com/Snooker-247-235371880410642">Snooker 247</a> &copy; 2018.</span>
        <span className="ml-auto">Powered by <a href="https://www.synavos.com" target='_blank' rel="noopener noreferrer">Synavos</a></span>
      </React.Fragment>
    );
  }
}

DefaultFooter.propTypes = propTypes;
DefaultFooter.defaultProps = defaultProps;

export default DefaultFooter;
