import React, { Component } from 'react';
import {DropdownItem, DropdownMenu, DropdownToggle, Nav } from 'reactstrap';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import { logout} from '../../stores/auth/auth-actions';
import {withRouter} from "react-router-dom";

import {AppHeaderDropdown, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {
  render() {

    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full={{ src: '/Logo_Color.jpg', padding:0,margin:0,width:'30vw' , alt: 'Snooker247 Logo' }}
          minimized={{ src: '/Logo_Color.jpg', padding:0,margin:0,width:'30vw', alt: 'Snooker247 Logo' }}
        />

        <AppSidebarToggler className="d-md-down-none" display="lg" />
        <strong style={{fontSize:"150%"}}> SNOOKER 247</strong>

        <Nav className="ml-auto" navbar>
          <AppHeaderDropdown direction="down">
            <DropdownToggle nav>
              <img  src="/Snooker_Display.png" className="img-avatar" alt="admin@bootstrapmaster.com" />
            </DropdownToggle>
            <DropdownMenu right style={{ right: 'auto' }}>

              <DropdownItem onClick={this.props.logout}><i className="fa fa-lock" /> Logout</DropdownItem>
            </DropdownMenu>
          </AppHeaderDropdown>
        </Nav>

        {/*<AppAsideToggler className="d-lg-none" mobile />*/}
      </React.Fragment>
    );
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => dispatch(logout())
  }
};
DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

const connectedComponent = connect(null, mapDispatchToProps)(DefaultHeader);
export default withRouter(connectedComponent);

