export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',
    },


    {
      name: 'Users',
      url: '/views/users',
      icon: 'icon-user',
    },


    {
      name: 'Memberships',
      url: '/views/memberships',
      icon: 'icon-badge',
    },

    {
      name: 'Gallery',
      url: '/views/gallery',
      icon: 'icon-star',
    },
    {
      name: 'Item Categories',
      url: '/views/item-categories',
      icon: 'icon-menu',
    },
  ],
};
