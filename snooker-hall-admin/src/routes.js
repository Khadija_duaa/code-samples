import React from 'react';
import Loadable from 'react-loadable';

import DefaultLayout from './containers/DefaultLayout';


function Loading() {
  return <div>Loading...</div>;
}

const Breadcrumbs = Loadable({
  loader: () => import('./views/Base/Breadcrumbs'),
  loading: Loading,
});


const UserDetail = Loadable({
  loader: () => import('./views/UserDetail/UserDetail'),
  loading: Loading,
});


const Cards = Loadable({
  loader: () => import('./views/Base/Cards'),
  loading: Loading,
});

const Carousels = Loadable({
  loader: () => import('./views/Base/Carousels'),
  loading: Loading,
});

const Collapses = Loadable({
  loader: () => import('./views/Base/Collapses'),
  loading: Loading,
});

const Dropdowns = Loadable({
  loader: () => import('./views/Base/Dropdowns'),
  loading: Loading,
});

const UserProfileForm = Loadable({
  loader: () => import('./views/Base/UserProfileForm/UserProfileForm'),
  loading: Loading,
});

const MembershipInfo = Loadable({
  loader: () => import('./views/Base/UserProfileForm/MembershipInfo'),
  loading: Loading,
});

const Jumbotrons = Loadable({
  loader: () => import('./views/Base/Jumbotrons'),
  loading: Loading,
});

const Queue = Loadable({
  loader: () => import('./views/Base/Queue/Queue'),
  loading: Loading,
});

const Navbars = Loadable({
  loader: () => import('./views/Base/Navbars'),
  loading: Loading,
});

const Navs = Loadable({
  loader: () => import('./views/Base/Navs'),
  loading: Loading,
});

const Paginations = Loadable({
  loader: () => import('./views/Base/Paginations'),
  loading: Loading,
});

const Popovers = Loadable({
  loader: () => import('./views/Base/Popovers'),
  loading: Loading,
});

const ProgressBar = Loadable({
  loader: () => import('./views/Base/ProgressBar'),
  loading: Loading,
});

const Switches = Loadable({
  loader: () => import('./views/Base/Switches'),
  loading: Loading,
});

const Topup = Loadable({
  loader: () => import('./views/Base/Topup'),
  loading: Loading,
});


const Tooltips = Loadable({
  loader: () => import('./views/Base/Tooltips'),
  loading: Loading,
});

const BrandButtons = Loadable({
  loader: () => import('./views/Buttons/BrandButtons'),
  loading: Loading,
});

const ButtonDropdowns = Loadable({
  loader: () => import('./views/Buttons/ButtonDropdowns'),
  loading: Loading,
});

const ButtonGroups = Loadable({
  loader: () => import('./views/Buttons/ButtonGroups'),
  loading: Loading,
});

const Buttons = Loadable({
  loader: () => import('./views/Buttons/Buttons'),
  loading: Loading,
});

const Charts = Loadable({
  loader: () => import('./views/Charts'),
  loading: Loading,
});

const Dashboard = Loadable({
  loader: () => import('./views/Dashboard'),
  loading: Loading,
});

const Users = Loadable({
  loader: () => import('./views/Users/Users'),
  loading: Loading,
});
const Tables = Loadable({
  loader: () => import('./views/Tables/Tables'),
  loading: Loading,
});

const Membership = Loadable({
  loader: () => import('./views/Membership/Membership'),
  loading: Loading,
});

const Items = Loadable({
  loader: () => import('./views/Item/Item'),
  loading: Loading,
});

const CategoryForm = Loadable({
  loader: () => import('./views/Components/CategoryForm'),
  loading: Loading,
});

const Tax = Loadable({
  loader: () => import('./views/Tax/tax'),
  loading: Loading,
});

const ItemsForm = Loadable({
  loader: () => import('./views/Components/ItemForm'),
  loading: Loading,
});

const CoreUIIcons = Loadable({
  loader: () => import('./views/Icons/CoreUIIcons'),
  loading: Loading,
});

const  Categories= Loadable({
  loader: () => import('./views/Categories/Categories'),
  loading: Loading,
});

const Flags = Loadable({
  loader: () => import('./views/Icons/Flags'),
  loading: Loading,
});

const FontAwesome = Loadable({
  loader: () => import('./views/Icons/FontAwesome'),
  loading: Loading,
});


const SimpleLineIcons = Loadable({
  loader: () => import('./views/Icons/SimpleLineIcons'),
  loading: Loading,
});

const Alerts = Loadable({
  loader: () => import('./views/Notifications/Alerts'),
  loading: Loading,
});

const Badges = Loadable({
  loader: () => import('./views/Notifications/Badges'),
  loading: Loading,
});

const Modals = Loadable({
  loader: () => import('./views/Notifications/Modals'),
  loading: Loading,
});

const Colors = Loadable({
  loader: () => import('./views/Theme/Colors'),
  loading: Loading,
});

const Typography = Loadable({
  loader: () => import('./views/Theme/Typography'),
  loading: Loading,
});

const Widgets = Loadable({
  loader: () => import('./views/Widgets/Widgets'),
  loading: Loading,
});

const MembershipDetails = Loadable({
  loader: () => import('./views/Components/MembershipDetailsWrapper'),
  loading: Loading,
});

const MembershipForm = Loadable({
  loader: () => import('./views/Components/MembershipForm'),
  loading: Loading,
});

const Gallery = Loadable({
  loader: () => import('./views/Gallery/Gallery'),
  loading: Loading,
});

const AddAlbum = Loadable({
  loader: () => import('./views/Gallery/GalleryForm'),
  loading: Loading,
});

const membershipUsers = () => <Users membershipUsers/>;

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  {path: '/', exact: true, name: 'Home', component: DefaultLayout},
  {path: '/dashboard', exact: true, name: 'Dashboard', component: Dashboard},

  {path: '/views/users', exact: true, name: 'Users', component: Users},
  {path: '/views/users/user-detail', exact: true, name: 'User Details', component: UserDetail},
  {path: '/views/users/profile', exact: true, name: 'User Profile', component: UserProfileForm},
  {path: '/views/users/top-up', exact: true, name: 'Top-Up', component: Topup},

  {path: '/views/membership-users', exact: true, name: 'Users', component: membershipUsers},

  {path: '/views/memberships', exact: true, name: 'Memberships', component: Membership},
  {path: '/views/item-categories/category', exact: true, name: 'Category', component: CategoryForm},
  {path: '/views/item-categories/items/item', exact: true, name: 'Item', component: ItemsForm},
  {path: '/views/item-categories/items', exact: true, name: 'Items', component: Items},
  {path: '/views/item-categories', exact: true, name: 'Item Categories', component: Categories},

  {path: '/views/tax', exact: true, name: 'Tax', component: Tax},
  {path: '/views/memberships/details', exact: true, name: 'Membership Details', component: MembershipDetails},
  {path: '/views/memberships/membership', exact: true, name: 'Membership', component: MembershipForm},

  {path: '/views/gallery', exact: true, name: 'Gallery', component: Gallery},
  {path: '/views/gallery/addAlbum', exact: true, name: 'Create Album', component: AddAlbum},

  {path: '/views/table-view', name: 'Tables', component: Tables},
  {path: '/theme', exact: true, name: 'Theme', component: Colors},
  {path: '/theme/colors', name: 'Colors', component: Colors},
  {path: '/theme/typography', name: 'Typography', component: Typography},
  {path: '/base', exact: true, name: 'Base', component: Cards},
  {path: '/base/cards', name: 'Cards', component: Cards},
  {path: '/membershipInfo', name: 'Info', component: MembershipInfo},
  {path: '/base/switches', name: 'Switches', component: Switches},
  {path: '/base/breadcrumbs', name: 'Breadcrumbs', component: Breadcrumbs},
  {path: '/base/carousels', name: 'Carousel', component: Carousels},
  {path: '/base/collapses', name: 'Collapse', component: Collapses},
  {path: '/base/dropdowns', name: 'Dropdowns', component: Dropdowns},
  {path: '/base/jumbotrons', name: 'Jumbotrons', component: Jumbotrons},
  {path: '/base/view-queue', name: 'Queue', component: Queue},
  {path: '/base/navbars', name: 'Navbars', component: Navbars},
  {path: '/base/navs', name: 'Navs', component: Navs},
  {path: '/base/paginations', name: 'Paginations', component: Paginations},
  {path: '/base/popovers', name: 'Popovers', component: Popovers},
  {path: '/base/progress-bar', name: 'Progress Bar', component: ProgressBar},
  {path: '/base/tooltips', name: 'Tooltips', component: Tooltips},
  {path: '/buttons', exact: true, name: 'Buttons', component: Buttons},
  {path: '/buttons/buttons', name: 'Buttons', component: Buttons},
  {path: '/buttons/button-dropdowns', name: 'Button Dropdowns', component: ButtonDropdowns},
  {path: '/buttons/button-groups', name: 'Button Groups', component: ButtonGroups},
  {path: '/buttons/brand-buttons', name: 'Brand Buttons', component: BrandButtons},
  {path: '/icons', exact: true, name: 'Icons', component: CoreUIIcons},
  {path: '/icons/coreui-icons', name: 'CoreUI Icons', component: CoreUIIcons},
  {path: '/icons/flags', name: 'Flags', component: Flags},
  {path: '/icons/font-awesome', name: 'Font Awesome', component: FontAwesome},
  {path: '/icons/simple-line-icons', name: 'Simple Line Icons', component: SimpleLineIcons},
  {path: '/notifications', exact: true, name: 'Notifications', component: Alerts},
  {path: '/notifications/alerts', name: 'Alerts', component: Alerts},
  {path: '/notifications/badges', name: 'Badges', component: Badges},
  {path: '/notifications/modals', name: 'Modals', component: Modals},
  {path: '/widgets', name: 'Widgets', component: Widgets},
  {path: '/charts', name: 'Charts', component: Charts},
];

export default routes;
