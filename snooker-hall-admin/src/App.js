import React, {Component} from 'react';
import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import 'react-notifications/lib/notifications.css';

import './App.css';
// Styles
// CoreUI Icons Set
import '@coreui/icons/css/coreui-icons.min.css';
// Import Flag Icons Set
import 'flag-icon-css/css/flag-icon.min.css';
// Import Font Awesome Icons Set
import 'font-awesome/css/font-awesome.min.css';
// Import Simple Line Icons Set
import 'simple-line-icons/css/simple-line-icons.css';
// Import Main styles for this application
import './scss/style.css'

import 'react-notifications/lib/notifications.css';

// Containers
import {DefaultLayout} from './containers';
// Pages
import {Login} from './views/Pages';


class App extends Component {

  getAuthRedirect = () => {
    return this.props.auth.authenticated ? <Redirect to={'/dashboard'}/> : <Redirect to={'/login'}/>;
  };

  getHomePage = () => {
    let jsx = null;
    if (this.props.auth.authenticated) {
      jsx = <Route path="/" name="Home" component={DefaultLayout}/>
    }
    return jsx;
  };


  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/login" name="Login Page" component={Login}/>
          {this.getHomePage()}
          {this.getAuthRedirect()}
        </Switch>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    errorMsg: state.auth.error
  };
};

export default connect(mapStateToProps)(App);
