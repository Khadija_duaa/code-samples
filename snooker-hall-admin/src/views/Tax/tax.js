import React, {Component} from 'react';
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  FormGroup,
  Input,
  Label,
  Row,
} from 'reactstrap';

class Tax extends Component {

  state = {
    amount: ''
  };


  onInputChange = (e) => {

    let {target} = e;
    let state = {...this.state};
    if((target.value.length) <= 5) {
      if (!isNaN(target.value)) {
        state[target.name] = Math.abs(target.value);
      }
      this.setState(state);
    }
  };

  getFormButtons = () => {
    return (
      <div className="form-actions">
        <Row>
          <Col xs={1}>
            <Button type="submit" color="primary" disabled={!this.state.amount}>
              Submit
            </Button>
          </Col>

        </Row>
      </div>
    );
  };

  getTopUpField = () => {
    return (
      <FormGroup>
        <Input
          value={this.state.amount}
          type="number"
          name="amount"
          placeholder="Enter %Tax"
          required
          onChange={this.onInputChange}/>
      </FormGroup>
    );
  };

  render() {
    return (
      <div className="animated fadeIn">
          <Row>
            <Col xs="12">
              <Card>
                <CardHeader>
                  <strong>Percentage Tax</strong>
                </CardHeader>
                <CardBody>

                  <Row>
                    <Col xs={12}>
                      {this.getTopUpField()}
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12}>
                      {this.getFormButtons()}
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
      </div>
    );
  }
}



export default Tax;

