import React from 'react';
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Container,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
  Table
} from 'reactstrap';

import TableHeaders from '../Components/TableHeads';
import {deleteItemCategory, loadCategories, resetRedux, setCategory, setItem} from "../../stores/item/item-actions";
import {connect} from "react-redux";
import {withRouter, Link} from "react-router-dom";
import Loader from "../../utils/loader";
import serverConfig from '../../utils/config';

const header = ["Name", "Icon", "Actions"];

class Categories extends React.Component {

  state = {
    modalOpen: false,
    categoryDeleteInfo: {}
  };

  componentDidMount() {
    this.props.resetRedux();
    this.props.loadCategories();
  }

  openDeleteModal = (category) => this.setState({modalOpen: !this.state.modalOpen, categoryDeleteInfo: category});

  getModalContent = () => {
    return (
      <Modal isOpen={this.state.modalOpen} toggle={this.openDeleteModal}
             className={'modal-danger ' + this.props.className}>

        <ModalHeader toggle={this.openDeleteModal}>
          Are you sure ?
        </ModalHeader>

        <ModalBody>
          Are you sure you want to delete this item ?
        </ModalBody>

        <ModalFooter>
          <Button color="danger" onClick={() => {
            let {categoryDeleteInfo} = this.state;
            this.props.deleteItemCategory(categoryDeleteInfo._id, categoryDeleteInfo.index);
            this.openDeleteModal(null);
          }}>
            Delete
          </Button>{' '}

          <Button color="secondary" onClick={this.openDeleteModal}>
            Cancel
          </Button>

        </ModalFooter>
      </Modal>
    );
  };

  getCardHeader = () => {
    return (
      <Row>
        <Col xs={10} style={{textAlign: 'left'}}>
          <i className="fa fa-align-justify"/> Categories
        </Col>


        <Col xs={2} lg={{size: 2}} style={{textAlign: 'right'}}>
          <Button color="primary" size="md" onClick={() => {
            this.props.history.push('/views/item-categories/category');
          }}>
            Add Category
          </Button>
        </Col>

      </Row>
    );
  };

  getCategoriesActions = (category, index) => {
    return (
      <Container>
        <Row>
          <Col xs={12} xl={'auto'} style={{marginBottom: '12px'}}>
            <Button color="primary" size="md"
                    onClick={() => {
                      this.props.setCategory(category);
                      this.props.history.push('/views/item-categories/category');
                    }}>
              Edit
            </Button>
          </Col>


          <Col xs={12} xl={'auto'} style={{marginBottom: '12px'}}>
            <Button color="danger" size="md"
                    onClick={() => {
                      this.openDeleteModal({_id: category._id, index});
                    }}>
              Delete
            </Button>
          </Col>
        </Row>
      </Container>
    );
  };

  getCategoryData = (category, index) => {
    return (
      <tr key={category._id}>
        <td>
          <Link to='/views/item-categories/items'
                onClick={() => this.props.setCategory(category)}>
            {category.categoryName}
          </Link>
        </td>

        <td>
          <img style={{height: '36px', width: '36px'}}
               src={`${serverConfig.serverUrl}/photos/${category.icon}?time=${Date.now()}`}/>
        </td>

        <td>
          {this.getCategoriesActions(category, index)}
        </td>
      </tr>
    );
  };

  getCategoryRows = () => {
    let jsx = null;
    let {itemCategories} = this.props;

    if (itemCategories) {
      jsx = itemCategories.map(this.getCategoryData);
    }

    return jsx;
  };


  getTable = () => {
    return (
      <Table bordered hover striped responsive>
        <thead>
        <TableHeaders>
          {header}
        </TableHeaders>
        </thead>

        <tbody>
        {this.getCategoryRows()}
        </tbody>
      </Table>
    );
  };


  render() {
    return (
      <div className="animated fadeIn">
        {this.getModalContent()}

        <Row>
          <Col xs={12}>
            {this.props.processing ? <Loader/> :
              <Card>
                <CardHeader>
                  {this.getCardHeader()}
                </CardHeader>
                <CardBody>
                  {this.getTable()}
                </CardBody>
              </Card>
            }
          </Col>
        </Row>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    processing: state.items.processing,
    itemCategories: state.items.itemCategories,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadCategories: () => dispatch(loadCategories()),
    setItem: (item) => dispatch(setItem(item)),
    setCategory: (category) => dispatch(setCategory(category)),
    deleteItemCategory: (id, index) => dispatch(deleteItemCategory(id, index)),
    resetRedux: () => dispatch(resetRedux()),
  }
};

const connectedComponent = connect(mapStateToProps, mapDispatchToProps)(Categories);
export default withRouter(connectedComponent)
