import React from 'react';
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
} from 'reactstrap';
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {saveMembership} from "../../stores/membership/membership-actions";
import Loader from "../../utils/loader";

const _ = require('lodash');

class MembershipForm extends React.Component {

  state = {
    formName: "Add New Membership",
    name: '',
    rate: 0,
    price: '',
    minutesOnSubscription: 0
  };

  componentDidMount() {
    if (this.props.membership) {
      console.log("Membership:", this.props.membership);
      let {name, rate, price, minutesOnSubscription} = this.props.membership;
      this.setState({rate, price, name, minutesOnSubscription, formName: `Update Membership ${name}`});
    }
  }

    onInputChange = (e, str, maxLength = 6) => {
    let {type, value, name} = e.target;

    if (value.toString().length <= maxLength) {
      let state = {...this.state};

      if (type === 'number') {
        if (!isNaN(value)) {
          state[name] = Math.abs(value);
        }
      }
      else {
        state[name] = value;
      }
      this.setState(state);
    }

  };

  handleNumbers = (e) => {
    const value = +e.target.value;
    const name = e.target.name;
    let _state = {...this.state};
    if (_.isFinite(value) && value >= 0 && value <= 100000) {
      _state[name] = value;
      this.setState(_state);
    }
  };

  onSaveChanges = () => {
    const {rate, name, price, minutesOnSubscription} = this.state;
    this.props.saveMembership({name, rate, price, minutesOnSubscription}, () => {
      this.props.history.push('/views/memberships');
    });
  };

  getForm = () => {
    return (
      <Form action="" method="post" className="form-horizontal">

        <FormGroup row>
          <Col md="3">
            <Label>Membership Name</Label>
          </Col>
          <Col xs="12" md="9">
            <Input type="text" name="name" placeholder="Membership Name"
                   required value={this.state.name} onChange={(e) => this.onInputChange(e, 12)} autoFocus/>
          </Col>
        </FormGroup>

        <FormGroup row>
          <Col md="3">
            <Label>Basic Rate / Hour</Label>
          </Col>
          <Col xs="12" md="9">
            <Input type="text" id="rate" name="rate" placeholder="Basic Rate"
                   required value={this.state.rate} onChange={this.handleNumbers}/>
          </Col>
        </FormGroup>

        <FormGroup row>
          <Col md="3">
            <Label>Subscription Price / year</Label>
          </Col>
          <Col xs="12" md="9">
            <Input type="text" id="price" name="price" placeholder="Subscription Price"
                   required value={this.state.price} onChange={this.handleNumbers}/>
          </Col>
        </FormGroup>

        <FormGroup row>
          <Col md="3">
            <Label>Free Minutes on Subscription</Label>
          </Col>
          <Col xs="12" md="9">
            <Input type="text" id="minutesOnSubscription" name="minutesOnSubscription" placeholder="Free Minutes"
                   required value={this.state.minutesOnSubscription} onChange={this.handleNumbers}/>
          </Col>
        </FormGroup>
      </Form>
    );
  };

  getCardFooter = () => {
    return (
      <Row>
        <Col xs="1">
          <Button type="submit" size="md" color="primary" onClick={this.onSaveChanges}>
            Submit
          </Button>
        </Col>
        <Col xs="1">
          <Button type="submit" size="md" color="secondary"
                  onClick={() => {
                    this.props.history.push('/views/memberships');
                  }}>
            Cancel
          </Button>
        </Col>
      </Row>
    );
  };

  render() {
    return (
      <div>
        {this.props.processing ? <Loader/> :
          <Card>
            <CardHeader>
              <strong>{this.state.formName}</strong>
            </CardHeader>

            <CardBody>
              {this.getForm()}
            </CardBody>

            <CardFooter>
              {this.getCardFooter()}
            </CardFooter>
          </Card>
        }
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    membership: state.membership.membership,
    message: state.membership.message,
    processing: state.membership.processing
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    saveMembership: (membership, callback) => dispatch(saveMembership(membership, callback))
  }
};

const connectedComponent = connect(mapStateToProps, mapDispatchToProps)(MembershipForm);
export default withRouter(connectedComponent);
