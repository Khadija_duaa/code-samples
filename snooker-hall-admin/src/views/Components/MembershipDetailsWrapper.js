import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import Details from './MembershipDetails';
import {saveMembershipDetails} from '../../stores/membership/membership-actions';

class Wrapper extends React.Component {

  componentWillMount() {
    if (!this.props.membership) {
      this.props.history.push('/views/memberships');
    }
  }


  render() {
    let initialValues = {...this.props.membership};

    return (
      <Details
        initialValues={initialValues}
        enableReinitialize={true}
        onSubmit={(data) => {
          this.props.updateDetails(data.specialRates, () => {
            this.props.history.push('/views/memberships');
          });
        }}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    membership: state.membership.membership,
    message: state.membership.message,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateDetails: (data, callback) => dispatch(saveMembershipDetails(data, callback))
  }
};

const connectedComponent = connect(mapStateToProps, mapDispatchToProps)(Wrapper);
export default withRouter(connectedComponent);
