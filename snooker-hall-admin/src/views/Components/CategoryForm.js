import React from 'react';
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
} from 'reactstrap';
import {addItemCategory, loadCategories, updateCategory} from "../../stores/item/item-actions";
import connect from "react-redux/es/connect/connect";
import {withRouter} from "react-router-dom"
import Loader from "../../utils/loader";
import {capitalize} from "../../utils/common-utils";

const _ = require('lodash');

class CategoryForm extends React.Component {

  state = {
    _id: '',
    formName: "Add Category",
    categoryName: '',
    icon: '',
    tax: 0
  };

  componentDidMount() {
    if (this.props.category) {
      let {categoryName, _id, tax} = this.props.category;
      console.log('this.props.category', this.props.category);
      this.setState({categoryName, _id, tax, formName: "Update Category"});
    }
  }

  handleName = event => {
    this.setState({categoryName: capitalize(event.target.value)});
  };

  onSaveChanges = () => {
    let formData = new FormData();

    formData.append('name', this.state.categoryName);
    formData.append('tax', this.state.tax.toString());

    formData.append('file', this.state.icon, this.state.icon.name);

    if (this.state._id) {
      this.props.updateCategory(this.state._id, formData, () => {
        this.props.history.push('/views/item-categories');
      });
    }
    else {
      this.props.addItemCategory(formData, () => {
        this.props.history.push('/views/item-categories');
      });
    }
  };

  onDrop = (e) => {
    if (e.target.files.length) {
      const file = e.target.files[0];
      const fileExtension = file.name.split('.').pop();
      if (['svg', 'png'].indexOf(fileExtension) > -1) {
        this.setState({icon: file});
      } else {
        // TODO show error
      }
    }
  };

  handleTax = (e) => {
    const value = +e.target.value;
    if (_.isFinite(value) && value >= 0 && value <= 100) {
      this.setState({tax: value});
    }
  };

  getTaxField = () => {
    return (
      <FormGroup row>
        <Col xs="1">
          <Label>Tax %</Label>
        </Col>
        <Col xs="5" md={'auto'}>
          <Input type="text" name="tax" placeholder="Tax %"
                 value={this.state.tax} ={this.handleTax}/>
        </Col>
      </FormGroup>
    );
  };


  getNameField = () => {
    return (
      <FormGroup row>
        <Col xs="1">
          <Label>Name</Label>
        </Col>
        <Col xs="5" md={'auto'}>
          <Input type="text" name="categoryName" placeholder="Category Name"
                 value={this.state.categoryName} ={this.handleName} autoFocus/>
        </Col>
      </FormGroup>
    );
  };

  getIconField = () => {
    return (
      <FormGroup row>
        <Col xs="1">
          <Label>Icon</Label>
        </Col>

        <Col xs="5" md={'auto'}>
          <input type={'file'}
                 ref={uploadElement => this.uploadElement = uploadElement}
                 accept={'.svg,.png'}
                 hidden
                 onChange={this.onDrop}/>

          <Button type="submit" size="md" color="primary"
                  onClick={e => {
                    e.preventDefault();
                    this.uploadElement.click()
                  }}>
            Upload Icon
          </Button>
          <br />
          <span>{this.state.icon && this.state.icon.name}</span>
        </Col>
      </FormGroup>
    );
  };


  getCardHeader = () => {
    return (
      <Row>
        <Col xs={2} style={{textAlign: 'left'}}>
          <strong>{this.state.formName}</strong>
        </Col>
      </Row>
    );
  };

  getForm = () => {
    return (
      <Form action="" method="post" className="form-horizontal">

        {this.getNameField()}
        {this.getTaxField()}
        {this.getIconField()}
      </Form>
    );
  };

  formDisabled = () => {
    // edit
    if (this.state._id) {
      return !this.state.categoryName && !this.state.icon
    }
    else {
      return !this.state.categoryName || !this.state.icon
    }
  };


  getCardFooter = () => {
    return (
      <Row>
        <Col xs="1">
          <Button type="submit" size="md" color="primary" onClick={this.onSaveChanges}
                  disabled={this.formDisabled()}>
            Submit
          </Button>
        </Col>
        <Col xs="1">
          <Button type="submit" size="md" color="secondary"
                  onClick={() => {
                    this.props.history.push('/views/item-categories');
                  }}>
            Cancel
          </Button>
        </Col>
      </Row>
    );
  };

  render() {
    return (
      <div>
        {this.props.processing ? <Loader/> :
          <Card>
            <CardHeader>
              {this.getCardHeader()}
            </CardHeader>

            <CardBody>
              {this.getForm()}
            </CardBody>

            <CardFooter>
              {this.getCardFooter()}
            </CardFooter>
          </Card>
        }
      </div>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    processing: state.items.processing,
    category: state.items.category,


  }
};
const mapDispatchToProps = (dispatch) => {
  return {
    loadCategories: () => dispatch(loadCategories()),
    addItemCategory: (data, callback) => dispatch(addItemCategory(data, callback)),
    updateCategory: (_id, data, callback) => dispatch(updateCategory(_id, data, callback)),

  }
};

const connectedComponent = connect(mapStateToProps, mapDispatchToProps)(CategoryForm);
export default withRouter(connectedComponent);
