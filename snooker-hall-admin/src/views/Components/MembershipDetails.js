import React from 'react';
import 'antd/dist/antd.css'
import {Select, Input, Button, Icon} from 'antd';
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  FormGroup,
  Label,
  Row
} from 'reactstrap';
import {Field, reduxForm, FieldArray} from 'redux-form'
import {withRouter} from 'react-router-dom';


const Option = Select.Option;
const daysArray = ["Sat", "Sun", "Mon", "Tue", "Wed", "Thu", "Fri"];
const days = daysArray.map((type) => {
  return (<Option key={type} id={type}>
      {type}
    </Option>
  )
});


// const renderSelect = ({input, label, children, placeholder, meta: {touched, error}, ...props}) => {
//   return (
//     <FormGroup label={label}>
//       <Label>{label}</Label>
//       <Select {...input} {...props} placeholder={"Select"}>
//         {children}
//       </Select>
//     </FormGroup>
//   );
// };

const renderSelect = ({ input, label, children,placeholder, meta: { touched, error }, ...props }) => {
  console.log("Input1",input.value);

  let validateStatus = '';
  if (touched && error) {
    validateStatus = 'error';
  }

  (props.mode == 'multiple' || props.mode == 'tag') && !input.value ?input.value = []:""
  console.log("Input2",input.value);

  return (
    <FormGroup label={label}>
      <Label>{label}</Label>
      <Select {...input} {...props} placeholder={"Select"}>
        {children}
      </Select>
    </FormGroup>
  );
};

const renderText = ({input, label, children, mode, meta: {touched, error}, ...props}) => {
  return (
    <FormGroup label={label}>
      <Label>{label}</Label>
      <Input  {...input} {...props}/>
    </FormGroup>
  );
};

const renderTime = ({input, label,placeholder, children, mode, meta: {touched, error}, ...props}) => {
  return (
    <span>
      <Label>{label}</Label>
      <Input {...input} placeholder={placeholder} type={'time'} {...props}/>
    </span>
  );
};

const renderMembers = ({fields, meta: { error, submitFailed} } ) => {
  return (
    <div>

      <Row>
        <Col xs={2} md={12}>
          <FormGroup>
            <Button type="dashed" onClick={ () => {fields.push({})}} icon={"plus"}>
              Add Timings
            </Button>
          </FormGroup>
        </Col>
      </Row>

      {fields.map((member, index) => (
        <Row key={index}>

          <Col xs={3} md={2}>
            <Field label={"Days"} mode={"multiple"} component={renderSelect} name={`${member}.days`}
                   placeholder={"Days"}>
              {days}
            </Field>
          </Col>

          <Col xs={5} md={2}>
            <Field
              label={"Rate"}
              component={renderText}
              name={`${member}.rate`}
              placeholder={"Rate"}
              type={"number"}
              min={0}/>
          </Col>

          <Col xs={6} md={4}>
            <FormGroup>
              <Row>
                <Col xs={6}>
                  <Field name={`${member}.startTime`} placeholder="From" component={renderTime} label={"Timings From"}
                         format={(value) => value || null}/>
                </Col>
                <Col xs={6}>
                  <Field name={`${member}.endTime`} placeholder="To" component={renderTime} label={"Timings To"}
                         format={(value) => value || null}/>
                </Col>
              </Row>
            </FormGroup>
          </Col>

          <Col xs={2} md={1} style={{textAlign: 'center'}}>
            <table style={{height: '100%'}}>
              <tbody>
              <tr>
                <td className="align-middle">
                  <Icon type={'close'} className={'hover'} style={{color: 'red'}} onClick={() => fields.remove(index)}/>
                </td>
              </tr>
              </tbody>
            </table>
          </Col>
        </Row>
      ))}
    </div>
  )
};

class Details extends React.Component {

  render() {
    const {handleSubmit, submitting, pristine, reset} = this.props;

    return (
      <form onSubmit={handleSubmit}>
        <Row>
          <Col xs={12} md={12}>
            <Card>
              <CardHeader>
                <strong>Membership Details</strong>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs={6} md={4}>
                    <Field
                      label={"Membership Name"}
                      component={renderText}
                      name={"name"}
                      placeholder={"Membership Name"}
                      type={"text"}
                      disabled={true}
                    />
                  </Col>

                  <Col xs={6} md={4}>
                    <Field
                      label={"Basic Rate / Hour"}
                      component={renderText}
                      name={"rate"}
                      placeholder={"Basic Rate / Hour"}
                      type={"number"}
                      disabled={true}/>
                  </Col>

                  <Col xs={6} md={4}>
                    <Field
                      label={"Annual Subscription"}
                      component={renderText}
                      name={"price"}
                      placeholder={"Annual Subscription"}
                      type={"number"}
                      disabled={true}/>
                  </Col>

                </Row>

                <FieldArray name={"specialRates"} component={renderMembers}/>

                <Row>
                  <Col xs={2} xl={1}>
                    <Button type="primary" disabled={submitting} htmlType="submit">
                      Save
                    </Button>
                  </Col>

                  <Col xs={2} xl={1}>
                    <Button disabled={pristine || submitting} onClick={reset}>
                      Undo
                    </Button>
                  </Col>

                  <Col xs={2} xl={1}>
                    <Button enable={(pristine || submitting) ? "true" : "false"}
                            onClick={() => this.props.history.push('/views/memberships')}>
                      Cancel
                    </Button>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </form>
    )
  }
}

const membershipDetails = reduxForm({form: 'membershipDetails'})(Details);
export default withRouter(membershipDetails);
