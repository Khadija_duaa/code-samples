import React from 'react';
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
} from 'reactstrap';
import {addItem, loadCategories, updateItem} from "../../stores/item/item-actions";
import {withRouter} from "react-router-dom";
import Loader from "../../utils/loader";
import connect from "react-redux/es/connect/connect";
import {capitalize} from "../../utils/common-utils";

const _ = require('lodash');

class ItemForm extends React.Component {

  state = {
    formName: "Add Item",
    name: '',
    price: 0,
    _id: ''
  };

  componentWillMount() {
    if (!this.props.category) {
      this.props.history.push('/views/item-categories');
    }
  }

  componentDidMount() {
    if (this.props.item) {
      let {name, price, _id} = this.props.item;
      this.setState({price, name, _id, formName: "Update Item"});
    }
  }

  handlePrice = (e) => {
    const value = +e.target.value;
    if (_.isFinite(value) && value >= 0 && value < 10000) {
      this.setState({price: value});
    }
  };

  handleName = (e) => {
    this.setState({name: capitalize(e.target.value)});
  };

  onSaveChanges = () => {
    if (this.state._id) {
      this.props.updateItem({...this.state}, () => {
        this.props.history.push('/views/item-categories/items');
      });
    }
    else {
      this.props.addItem(this.props.category._id, {...this.state}, () => {
        this.props.history.push('/views/item-categories/items');
      });
    }
  };

  getNameField = () => {
    return (
      <FormGroup row>
        <Col md="3">
          <Label>Name</Label>
        </Col>
        <Col xs="12" md="9">
          <Input type="text" name="name" placeholder="Item Name"
                 required value={this.state.name} onChange={this.handleName} autoFocus/>
        </Col>
      </FormGroup>
    );
  };

  getTypeField = () => {
    return (
      <FormGroup row>
        <Col md="3">
          <Label>Category</Label>
        </Col>
        <Col xs="12" md="9">
          <Input type="text" name="name" placeholder="Item Name"
                 value={this.props.category ? this.props.category.categoryName : ''} disabled/>
        </Col>
      </FormGroup>
    );
  };

  getPriceField = () => {
    return (
      <FormGroup row>
        <Col md="3">
          <Label>Price</Label>
        </Col>
        <Col xs="12" md="9">
          <Input type="text" name="price" placeholder="Item Price"
                 required value={this.state.price} onChange={this.handlePrice}/>
        </Col>
      </FormGroup>
    );
  };


  getForm = () => {
    return (
      <Form action="" method="post" className="form-horizontal">
        {this.getNameField()}
        {this.getTypeField()}
        {this.getPriceField()}
      </Form>
    );
  };

  getCardFooter = () => {
    return (
      <Row>
        <Col xs="1">
          <Button type="submit" size="md" color="primary" onClick={this.onSaveChanges}
                  disabled={!this.state.name || !this.state.price}>
            Submit
          </Button>
        </Col>
        <Col xs="1">
          <Button type="submit" size="md" color="secondary"
                  onClick={() => {
                    this.props.history.push('/views/item-categories/items');
                  }}>
            Cancel
          </Button>
        </Col>
      </Row>
    );
  };

  render() {
    return (
      <div>
        {this.props.processing ? <Loader/> :
          <Card>
            <CardHeader>
              <strong>{this.state.formName}</strong>
            </CardHeader>

            <CardBody>
              {this.getForm()}
            </CardBody>

            <CardFooter>
              {this.getCardFooter()}
            </CardFooter>
          </Card>
        }
      </div>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    category: state.items.category,
    item: state.items.item,
    processing: state.items.processing,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadCategories: () => dispatch(loadCategories()),
    addItem: (_id, data, callback) => dispatch(addItem(_id, data, callback)),
    updateItem: (data, callback) => dispatch(updateItem(data, callback)),
  }
};

const connectedComponent = connect(mapStateToProps, mapDispatchToProps)(ItemForm);
export default withRouter(connectedComponent);

