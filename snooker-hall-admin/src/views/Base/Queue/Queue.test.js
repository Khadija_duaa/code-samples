import React from 'react';
import ReactDOM from 'react-dom';
import Queue from './Queue';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Queue />, div);
  ReactDOM.unmountComponentAtNode(div);
});
