import React, {Component} from 'react';
import {newPerson} from './Utils'
import {
  Badge,
  Card,
  CardBody,
  CardHeader,
  Col,
  ListGroup,
  ListGroupItem,
  Row,
} from 'reactstrap';

class Queue extends Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: 1
    };
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }



  render() {

    return (
      <div className="animated fadeIn">
        <Row>
          <Col sm="12" xl="6">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i><strong>Waiting List</strong>
              </CardHeader>
              <CardBody>
                <ListGroup>
                  {
                    newPerson.names1.map((data, i) => {
                      return (
                        <ListGroupItem key={i}>{data.name} {data.time} <a
                          href="https://www.w3schools.com/html/"><Badge
                          className="float-right" pill>X</Badge></a></ListGroupItem>
                      )
                    })
                  }
                </ListGroup>
              </CardBody>
            </Card>
          </Col>

          <Col sm="12" xl="6">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i><strong>Guest List</strong>
              </CardHeader>
              <CardBody>
                <ListGroup>
                  {
                    newPerson.names1.map((data, i) => {
                      return (
                        <ListGroupItem key={i} className="justify-content-between">{data.name} {data.time}<a
                          href="https://www.w3schools.com/html/"><Badge
                          className="float-right" pill>X</Badge></a></ListGroupItem>
                      )
                    })
                  }

                </ListGroup>
              </CardBody>
            </Card>
          </Col>
        </Row>


      </div>
    );
  }
}

export default Queue;
