import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {AppSwitch} from '@coreui/react';
import {Button, Card, CardBody, CardHeader, Col, FormGroup, Input, Label, Row,} from 'reactstrap';

import {loadMemberships} from "../../../stores/membership/membership-actions";
import {loadUsers, updateUserProfile} from "../../../stores/users/users-actions";

import connect from "react-redux/es/connect/connect";
import Loader from "../../../utils/loader";
const _ = require('lodash');

class UserProfileForm extends Component {

  state = {
    membership: '',
    freeMinutes: '',
    negativeBalanceLimit: null,
    isNegativeBalanceAllowed: true
  };

  componentWillMount() {
    if (!this.props.user) {
      this.props.history.push('/views/users');
    }
  }

  componentDidMount() {
    this.props.loadMemberships();

    if (this.props.user) {
      let {isNegativeBalanceAllowed, freeMinutes, negativeBalanceLimit} = this.props.user.profile;
      let membership = this.props.user.membership['_id'];
      this.setState({isNegativeBalanceAllowed, freeMinutes, membership, negativeBalanceLimit : {}});
    }
  }


  handleChange = () => {
    let newState = {};
    newState.isNegativeBalanceAllowed = !this.state.isNegativeBalanceAllowed;

    if (!newState.isNegativeBalanceAllowed) {
      newState.negativeBalanceLimit = '';
    }

    this.setState(newState);
  };

  onInputChange = (e) => {
    let {value, type, name} = e.target;

    let state = {...this.state};

    if ('number' === type) {
      let absValue = isNaN(value) ? null : Math.abs(value);
       if (_.isFinite(absValue) && absValue >= 0 && absValue <= 1000000) {
        state[name] = 'negativeBalanceLimit' === name ? (-1 * absValue) : absValue;
      }
    }
    else {
      state[name] = value;
    }

    this.setState(state);
  };

  onSaveChanges = () => {
    this.props.updateUserProfile({...this.state}, () => {
      this.props.history.push('/views/users')
    });
  };

  getMembershipField = () => {
    return (
      <FormGroup>

        <Label htmlFor="membership-select">Membership</Label>

        <Input type="select"
               defaultValue={this.props.user.membership._id}
               name="membership"
               autoFocus
               id="membership-select"
               ={this.onInputChange}>
          {this.props.memberships.map(membership => {
              return (
                <option key={membership._id} value={membership._id}>
                  {membership.name}
                </option>
              )
            }
          )}
        </Input>
      </FormGroup>
    );
  };

  getFreeMinutesField = () => {
    return (
      <FormGroup>
        <Label htmlFor="name">Free Minutes</Label>
        <Input type="number" name='freeMinutes' value={this.state.freeMinutes} id="name"
               placeholder="Set Free Minutes" required
               ={this.onInputChange}/>
      </FormGroup>
    );
  };


  getWalletField = () => {
    return (
      <FormGroup>
        <Label htmlFor="name">Wallet</Label>
        <Input type="number" disabled name='wallet' id="name" value={this.props.user.profile.wallet}/>
      </FormGroup>
    );
  };

  getNegativeBalanceField = () => {
    return (
      <FormGroup>
        <Label htmlFor="ccnumber">Negative Balance Limit</Label>
        <Row>
          <Col xs={{size: 10}}>
            <Input type="number" name="negativeBalanceLimit" id={'ccnumber'} placeholder="Set Limit"
                   required
                   ={this.onInputChange}
                   value={this.state.negativeBalanceLimit}
                   disabled={!this.state.isNegativeBalanceAllowed}
            />
          </Col>

          <Col xs={{size: 2}}>
            <div style={{float: 'left'}}>
              <AppSwitch className={'float-right mb-0'}
                         label
                         color={'info'}
                         defaultChecked={this.state.isNegativeBalanceAllowed}
                         size={'sm'}
                         ={this.handleChange}/>
            </div>
          </Col>
        </Row>
      </FormGroup>
    );
  };

  getFormButtons = () => {
    return (
      <div className="form-actions">
        <Row>
          <Col xs={{size: 12}} md={{size: 1}}>
            <Button type="submit" color="primary" onClick={this.onSaveChanges}>
              Save changes
            </Button>
          </Col>

          <Col xs={{size: 12}} md={{size: 1}}>
            <Button color="secondary"
                    onClick={() => this.props.history.push('/views/users')}>
              Cancel
            </Button>
          </Col>
        </Row>
      </div>
    );
  };

  getCardBody = () => {
    if (!this.props.user) return null;

    return (
      <CardBody>

        <Row>
          <Col xs={12}>
            {this.getMembershipField()}
          </Col>
        </Row>

        <Row>
          <Col xs="12">
            {this.getFreeMinutesField()}
          </Col>
        </Row>

        <Row>
          <Col xs="12">
            {this.getWalletField()}
          </Col>
        </Row>

        <Row>
          <Col xs={{size: 12}}>
            {this.getNegativeBalanceField()}
          </Col>
        </Row>

        <Row>
          <Col xs={12}>
            {this.getFormButtons()}
          </Col>
        </Row>

      </CardBody>
    );
  };

  render() {
    return (
      <div className="animated fadeIn">
        {this.props.processing ? <Loader/> :
          <Row>
            <Col xs="12" lg="12">
              <Card>

                <CardHeader>
                  <strong>Profile</strong>
                </CardHeader>

                {this.getCardBody()}

              </Card>
            </Col>
          </Row>
        }
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.users.user,
    memberships: state.membership.memberships,
    message: state.users.message,
    processing: state.users.processing
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadMemberships: () => dispatch(loadMemberships()),
    updateUserProfile: (data, callback) => dispatch(updateUserProfile(data, callback)),
    loadUsers: () => dispatch(loadUsers()),
  }
};

const connectedComponent = connect(mapStateToProps, mapDispatchToProps)(UserProfileForm);
export default withRouter(connectedComponent);
