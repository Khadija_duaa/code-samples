import React, {Component} from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Label,
  Row,
} from 'reactstrap';
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";

class MembershipInfo extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleFade = this.toggleFade.bind(this);
    this.state = {
      collapse: true,
      fadeIn: true,
      timeout: 300,
      name: '',
      rate: '',
      price: '',
    };
  }

  toggle() {
    this.setState({collapse: !this.state.collapse});
  }

  toggleFade() {
    this.setState((prevState) => {
      return {fadeIn: !prevState}
    });
  }

  componentDidMount() {
    if (this.props.membership) {
      let {name, rate, price} = this.props.membership;
      this.setState({name, rate, price});
    }
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" lg="12">
            <Card>
              <CardHeader>
                <strong>Membership Info</strong>
              </CardHeader>
              <CardBody>

                <Row>
                  <Col lg="2">
                    <Label htmlFor="select">Membership Name:</Label>

                  </Col>
                  <Col>
                    <Label>
                      {this.state.name}
                    </Label>
                  </Col>
                </Row>

                <br />
                <Row>
                  <Col lg="2">
                    <Label htmlFor="name">Basic Rate:</Label>
                  </Col>
                  <Col>
                    <Label>
                      {this.state.rate}
                    </Label>
                  </Col>
                </Row>
                <br></br>
                <Row>

                  <Col lg="2">
                    <Label htmlFor="ccnumber">Subscription Price:</Label>
                  </Col>
                  <Col>
                    <Label>
                      {this.state.price}
                    </Label>
                  </Col>
                </Row>
                <br></br>
              </CardBody>
            </Card>
          </Col>
        </Row>

      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    membership: state.membership.membership
  }
};

const connectedComponent = connect(mapStateToProps)(MembershipInfo);
export default withRouter(connectedComponent)
