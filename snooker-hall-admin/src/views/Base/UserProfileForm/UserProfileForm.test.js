import React from 'react';
import ReactDOM from 'react-dom';
import UserProfileForm from './';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<UserProfileForm />, div);
  ReactDOM.unmountComponentAtNode(div);
});
