import React from 'react';
import ReactDOM from 'react-dom';
import Topup from './Topup';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Topup />, div);
  ReactDOM.unmountComponentAtNode(div);
});
