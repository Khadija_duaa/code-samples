import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  FormGroup,
  Input,
  Label,
  Row,
} from 'reactstrap';
import {setTopUpBalance} from "../../../stores/users/users-actions";
import connect from "react-redux/es/connect/connect";
import Loader from "../../../utils/loader";

class Topup extends Component {

  state = {
    amount: ''
  };

  componentWillMount() {
    if (!this.props.user) {
      this.props.history.push('/views/users');
    }
  }

  onInputChange = (e) => {

    let {target} = e;
    let state = {...this.state};
      if((target.value.length) <= 5) {
        if (!isNaN(target.value)) {
          state[target.name] = Math.abs(target.value);
        }
      this.setState(state);
    }
  };

  onSaveChanges = () => {
    console.log('formData', this.state);
    this.props.setTopUpBalance({...this.state}, () => {
      this.props.history.push('/views/users')
    });
  };

  getFormButtons = () => {
    return (
      <div className="form-actions">
        <Row>
          <Col xs={1}>
            <Button type="submit" color="primary" disabled={!this.state.amount}
                    onClick={this.onSaveChanges}>
              Save changes
            </Button>
          </Col>

          <Col xs={1}>
            <Button color="secondary"
                    onClick={() => this.props.history.push('/views/users')}>
              Cancel
            </Button>
          </Col>
        </Row>
      </div>
    );
  };

  getTopUpField = () => {
    if (!this.props.user) return null;
    
    return (
      <FormGroup>
        <Label style={{color: '#20a8d8'}}
               htmlFor="name">
          {`Current Wallet Balance : ${this.props.user.profile.wallet}`}
        </Label>

        <Input
          value={this.state.amount}
          type="number"
               name="amount"
               placeholder="Top-Up Amount"
               required
               onChange={this.onInputChange}/>
      </FormGroup>
    );
  };

  render() {
    return (
      <div className="animated fadeIn">
        {this.props.processing ? <Loader/> :
          <Row>
            <Col xs="12">
              <Card>
                <CardHeader>
                  <strong>Top-up Balance</strong>
                </CardHeader>
                <CardBody>

                  <Row>
                    <Col xs={12}>
                      {this.getTopUpField()}
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12}>
                      {this.getFormButtons()}
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        }
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.users.user,
    message: state.users.message,
    processing: state.users.processing
  }
};
const mapDispatchToProps = (dispatch) => {
  return {
    setTopUpBalance: (balance, callback) => dispatch(setTopUpBalance(balance, callback)),
  }
};

const connectedComponent = connect(mapStateToProps, mapDispatchToProps)(Topup);
export default withRouter(connectedComponent);

