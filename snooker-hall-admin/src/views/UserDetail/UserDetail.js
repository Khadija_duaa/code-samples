import React, {Component} from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Label,
  Row,
  Table,
} from 'reactstrap';
import moment from 'moment';
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";


class UserDetail extends Component {

  componentWillMount() {
    if (!this.props.user) {
      this.props.history.push('/views/users');
    }
  }

  getPersonalInfo = () => {
    let data = [];

    if (this.props.user) {
      let {user} = this.props;

      data.push({name: 'First Name', value: user.firstName});
      data.push({name: 'Last Name', value: user.lastName});
      data.push({name: 'Email', value: user.email});
      data.push({name: 'Phone Number', value: user.phoneNumber});
      data.push({name: 'Security Number', value: user.ssn});
      data.push({name: 'City', value: user.city});
      data.push({name: 'Street', value: user.street});
      data.push({name: 'Postal Code', value: user.postalCode});
      data.push({name: 'Gender', value: user.ssnDetails ? user.ssnDetails.sex : 'N/A'});
      data.push({name: 'Highest Break', value: user.highestBreak});

    }

    return data;
  };

  getMembershipHistoryRows = () => {
    let jsx = null;
    let {user} = this.props;

    if (user && user.membershipHistory && user.membershipHistory.length) {
      let history = [...user.membershipHistory];
      jsx = history.reverse().map(history => {
        return (
          <tr key={history._id}>
            <td>{history.name}</td>
            <td>{history.rate}</td>
            <td>{history.price}</td>
            <td>{moment(history.subscriptionDate).format('MMMM Do YYYY, h:mm:ss a')}</td>
            <td>{moment(history.expirationDate).format('MMMM Do YYYY, h:mm:ss a')}</td>
          </tr>
        );
      });
    }

    return jsx;
  };

  getMembershipHistory = () => {
    return (
      <Row>
        <Col xs={12}>
          <Label>
            <h2>
              Membership History
            </h2>
          </Label>
        </Col>

        <Col xs={12}>
          <Table bordered hover striped responsive>
            <thead>
            <tr>
              <th>Name</th>
              <th>Rate/Hour</th>
              <th>Subscription Price</th>
              <th>Subscription Date</th>
              <th>Expiration Date</th>
            </tr>
            </thead>

            <tbody>
            {this.getMembershipHistoryRows()}
            </tbody>
          </Table>
        </Col>
      </Row>
    );
  };

  personalInfo = () => {
    return (
      <Row>
        <Col xs={12}>
          <Label>
            <h2>
              Personal Information
            </h2>
          </Label>
        </Col>

        <Col xs={6}>
          <Table bordered hover striped responsive>
            <tbody>
            {this.getPersonalInfo().map((row, index) => {
              return (
                <tr key={index}>
                  <th>{row.name}</th>
                  <td>
                    {row.value}
                  </td>
                </tr>
              );
            })}
            </tbody>
          </Table>
        </Col>
      </Row>
    );
  };

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs={12}>
            <Card>
              <CardHeader>
                <strong>User Details</strong>

              </CardHeader>

              <CardBody>

                {this.personalInfo()}

                {this.getMembershipHistory()}

              </CardBody>
            </Card>
          </Col>
        </Row>

      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.users.user
  }
};

const connectedComponent = connect(mapStateToProps)(UserDetail);
export default withRouter(connectedComponent)
