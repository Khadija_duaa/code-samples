import React from 'react';
import ReactDOM from 'react-dom';
import UserDetail from './UserDetail';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<UserDetail />, div);
  ReactDOM.unmountComponentAtNode(div);
});
