import React from 'react';
import {
  Row,
  Col,
  Card,
  Button,
  CardBody,
  CardHeader,
  Container,
} from 'reactstrap';
import Loader from "../../utils/loader";
import 'react-images-uploader/styles.css';
import 'react-images-uploader/font.css';
import PhotoUploader from '../../components/PhotoUploader';
import serverConfig from '../../utils/config.json';
import {connect} from "react-redux";
import {loadGallery, specificAlbum, resetRedux, deleteImages} from "../../stores/gallery/gallery-actions";
import {withRouter} from "react-router-dom";
import GridGallery from 'react-grid-gallery';
import EditDeleteAlbum from "../../components/EditDeleteAlbum";

class Gallery extends React.Component {

  state = {
    selectedImages: []
  };

  getIndex = (img) => {
    let index = -1;
    const images = this.state.selectedImages;

    for (let i = 0; i < images.length; i++) {
      let image = images[i];
      if (image.url === img.url) {
        index = i;
        break;
      }
    }

    return index;
  };

  addInSelectedImages = (index) => {
    let selectedImages = [...this.state.selectedImages];
    let displayingImages = this.prepareGalleryImages();

    let img = displayingImages[index];
    let _img = {url: img.url, album: img.album};

    let _index = this.getIndex(_img);

    if (_index > -1) {
      selectedImages.splice(_index, 1);
    }
    else {
      selectedImages.push(_img);
    }

    this.setState({selectedImages});
  };

  isSelected = (img) => this.getIndex(img) > -1;

  initData = () => {
    this.setState({selectedImages: []});
    this.props.resetRedux();
    this.props.loadGallery();
  };

  componentDidMount() {
    this.initData();
  }

  mapAlbumPhotoToGalleryObject = (id, url) => {
    let src = `${serverConfig.serverUrl}/photos/${id}/${url}`;
    return {
      url,
      album: id,
      src,
      thumbnail: src,
      thumbnailWidth: 320,
      thumbnailHeight: 212,
      isSelected: this.isSelected({url, album: id})
    };
  };

  prepareGalleryImages = () => {
    let photos = [];
    let {album, albums} = this.props;
    if (album) {
      photos = album['photos'].map(url => {
        return this.mapAlbumPhotoToGalleryObject(album._id, url);
      });
    }
    else if (albums) {
      albums.forEach(_album => {
        _album['photos'].forEach(photo => {
          photos.push(this.mapAlbumPhotoToGalleryObject(_album._id, photo));
        });
      });
    }

    return photos;
  };

  onSelectImage = (e) => {
    this.setState({selectedImages: []});
    this.props.specificAlbum(e.target.value || null);
  };

  editDeleteAlbum = () => {
    let jsx = this.deleteSelected();

    if (!jsx && this.props.album) {
      jsx = <EditDeleteAlbum/>;
    }

    return jsx;
  };


  deleteSelected = () => {
    return this.state.selectedImages && this.state.selectedImages.length ?
      <Button color="danger" size="md"
              onClick={() => {
                this.props.deleteImages(this.state.selectedImages, () => {
                  this.initData();
                });
              }}>
        Delete Selected Images
      </Button>
      : null;
  };


  getCardHeader = () => {
    return (
      <Row>
        <Col xs={2} style={{textAlign: 'left'}}>
          <strong>Gallery</strong>
        </Col>
        <Col xs={10}>
          <Row className={'pull-right'}>
            <Col xs={'auto'}>
              <PhotoUploader/>
            </Col>
            <Col xs={'auto'}>
              <Button color="primary" size="md"
                      onClick={() => {
                        this.props.specificAlbum(null);
                        this.props.history.push('/views/gallery/addAlbum');
                      }}>
                Create Album
              </Button>
            </Col>
          </Row>
        </Col>
      </Row>
    );
  };

  getSelectField = () => {
    return (
      <select className="form-control" onChange={this.onSelectImage}>
        <option value="">All Albums</option>
        {
          this.props.albums && this.props.albums.map((album) => {
            return <option key={album._id} value={album._id}>
              {album.name}
            </option>
          })
        }
      </select>
    );
  };

  getCardBody = () => {
    const IMAGES = this.prepareGalleryImages();

    return (
      <Container>
        <Row>
          <Col sm={6} md={3}>
            {this.getSelectField()}
          </Col>

          <Col sm={6} md={6}>
            {this.editDeleteAlbum()}
          </Col>
        </Row>

        <Row style={{marginTop: '70px'}}>
          <Col sm={12}>
            <GridGallery
              enableImageSelection={true}
              images={IMAGES}
              onSelectImage={(index) => this.addInSelectedImages(index)}/>
          </Col>
        </Row>
      </Container>
    );
  };

  render() {
    return (
      <div className="animated fadeIn">
        {this.props.processing ? <Loader/> :
          <Row>
            <Col xs={12}>
              <Card>
                <CardHeader>
                  {this.getCardHeader()}
                </CardHeader>

                <CardBody>
                  {this.getCardBody()}
                </CardBody>
              </Card>
            </Col>
          </Row>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    albums: state.gallery.albums,
    album: state.gallery.album,
    processing: state.gallery.processing
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadGallery: () => dispatch(loadGallery()),
    specificAlbum: (id) => dispatch(specificAlbum(id)),
    deleteImages: (images, callback) => dispatch(deleteImages(images, callback)),
    resetRedux: () => dispatch(resetRedux()),
  }
};

const connectedComponent = connect(mapStateToProps, mapDispatchToProps)(Gallery);
export default withRouter(connectedComponent)
