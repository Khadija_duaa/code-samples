import React from 'react';
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
} from 'reactstrap';
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {saveAlbum} from "../../stores/gallery/gallery-actions";
import Loader from "../../utils/loader"


class MembershipForm extends React.Component {

  state = {
    formName: "Create Album",
    name: '',
  };

  componentDidMount() {
    if (this.props.album) {
      let {name} = this.props.album;
      this.setState({name, formName: `Update Album`});
    }
  }

  onInputChange = (e) => {
    let {target} = e;
    let state = {...this.state};
    state[target.name] = target.value;
    this.setState(state);
  };

  keyDown = (e) => {
    if(e.keyCode === 13) {
    e.preventDefault();
    }
  };

  onSaveChanges = () => {
    let {name} = this.state;
    this.props.saveAlbum(name, () => {
      this.props.history.push('/views/gallery');
    });
  };

  getCardBody = () => {
    return (
      <Form action="" method="post" className="form-horizontal">

        <FormGroup row>
          <Col md="2">
            <Label htmlFor="hf-email">Album Name</Label>
          </Col>
          <Col xs="12" md="9">
            <Input type="text" id="name" name="name" placeholder="Name"
                   required value={this.state.name} onChange={this.onInputChange} onKeyDown={this.keyDown}/>
          </Col>
        </FormGroup>

      </Form>
    );
  };

  getCardFooter = () => {
    return (
      <Row>
        <Col xs="1">
          <Button type="submit" size="md" color="primary" onClick={this.onSaveChanges}>
            Submit
          </Button>
        </Col>
        <Col xs="1">
          <Button type="submit" size="md" color="secondary"
                  onClick={() => {
                    this.props.history.push('/views/gallery');
                  }}>
            Cancel
          </Button>
        </Col>
      </Row>
    );
  };

  render() {
    return (
      <div>
        {this.props.processing ? <Loader/> :
          <Card>
            <CardHeader>
              <strong>{this.state.formName}</strong>
            </CardHeader>
            <CardBody>
              {this.getCardBody()}
            </CardBody>

            <CardFooter>
              {this.getCardFooter()}
            </CardFooter>
          </Card>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    message: state.gallery.message,
    album: state.gallery.album,
    processing: state.gallery.processing
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    saveAlbum: (album, callback) => dispatch(saveAlbum(album, callback))
  }
};

const connectedComponent = connect(mapStateToProps, mapDispatchToProps)(MembershipForm);
export default withRouter(connectedComponent);
