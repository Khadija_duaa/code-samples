import React from 'react';
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Container,
  Row,
  Table,
  Modal, ModalBody, ModalFooter, ModalHeader
} from 'reactstrap';

import {withRouter} from 'react-router-dom';
import TableHeaders from '../Components/TableHeads'
import {
  loadMemberships,
  setMembership,
  resetRedux,
  deleteMembership,
  setProcessing
} from "../../stores/membership/membership-actions";
import {connect} from "react-redux";
import Loader from "../../utils/loader";
import {membershipSetAsDefault} from "../../utils/axios-utils";

const header = ["Membership", "Basic Rate/Hour", "Subscription Price", "Default", "Users", "Actions"];

class Membership extends React.Component {

  componentDidMount() {
    this.props.resetRedux();
    this.props.loadMemberships();
  }

  state = {
    modalOpen: false
  };

  openDeleteModal = (membership) => this.setState({modalOpen: !this.state.modalOpen, membershipDeleteInfo: membership});

  getModalContent = () => {
    return (<Modal isOpen={this.state.modalOpen} toggle={this.openDeleteModal}
                   className={'modal-danger ' + this.props.className}>

        <ModalHeader toggle={this.openDeleteModal}>
          Are you sure ?
        </ModalHeader>

        <ModalBody>
          Are you sure you want to delete this Membership ?
        </ModalBody>

        <ModalFooter>
          <Button color="danger" onClick={() => {
            let {membershipDeleteInfo} = this.state;
            this.props.deleteMembership(membershipDeleteInfo._id, membershipDeleteInfo.index);
            this.openDeleteModal(null);
          }}>
            Delete
          </Button>{' '}

          <Button color="secondary" onClick={this.openDeleteModal}>
            Cancel
          </Button>

        </ModalFooter>
      </Modal>
    );
  };

  getCardHeader = () => {
    return (
      <Row>
        <Col xs={6} style={{textAlign: 'left'}}>
          <i className="fa fa-align-justify"/> Memberships
        </Col>

        <Col xs={6} lg={{size: 3, offset: 3}} style={{textAlign: 'right'}}>
          <Button color="primary" size="md"
                  onClick={() => this.props.history.push('/views/memberships/membership')}>
            Add Membership
          </Button>
        </Col>

      </Row>
    );
  };

  getMembershipActions = (membership, index) => {
    return (
      <Container>
        <Row>
          <Col xs={12} xl={'auto'} style={{marginBottom : '12px'}}>
            <Button color="primary" size="md"
                    onClick={() => {
                      this.props.setMembership(membership);
                      this.props.history.push('/views/memberships/membership');
                    }}>
              Edit
            </Button>
          </Col>

          <Col xs={12} xl={'auto'} style={{marginBottom : '12px'}}>
            <Button color="primary" size="md"
                    onClick={() => {
                      this.props.setMembership(membership);
                      this.props.history.push('/views/memberships/details');
                    }}>
              Membership Detail
            </Button>
          </Col>

          <Col xs={12} xl={'auto'} style={{marginBottom : '12px'}}>
            <Button color="primary" size="md"
                    onClick={() => {
                      this.props.setMembership(membership);
                      this.props.history.push('/views/membership-users');
                    }}>
              Users
            </Button>
          </Col>

          <Col xs={12} xl={'auto'} style={{marginBottom : '12px'}}>
            {!membership['isBasic'] &&
            <Button color="primary" size="md"
                    onClick={() => {
                      this.setAsDefault(membership._id);
                    }}>
              Set Default
            </Button>
            }
          </Col>

          <Col xs={12} xl={'auto'} style={{marginBottom : '12px'}}>
            {!membership['isBasic'] &&
            <Button color="danger" size="md"
                    onClick={() => {
                      this.openDeleteModal({_id: membership._id, index});
                    }}>
              Delete
            </Button>
            }
          </Col>
        </Row>
      </Container>
    );
  };

  getMembershipRow = (membership, index) => {
    return (
      <tr key={membership._id}>
        <td>
          {membership.name}
        </td>

        <td>
          {membership.rate}
        </td>

        <td>
          {membership.price}
        </td>

        <td>
          {membership['isBasic'] ? 'Yes' : 'No'}
        </td>

        <td>
          {membership.users ? membership.users.length : 0}
        </td>

        <td>
          {this.getMembershipActions(membership, index)}
        </td>
      </tr>
    );
  };

  getMembershipRows = () => {
    let jsx = null;
    let {memberships} = this.props;

    if (memberships && memberships.length) {
      jsx = this.props.memberships.map((data, i) => this.getMembershipRow(data, i));
    }

    return jsx;
  };


  getTable = () => {
    return (
      <Table bordered responsive striped>
        <thead>
        <TableHeaders>
          {header}
        </TableHeaders>
        </thead>

        <tbody>
        {this.getMembershipRows()}
        </tbody>
      </Table>
    );
  };

  setAsDefault = (_id) => {
    this.props.setProcessing(true);

    membershipSetAsDefault(_id)
      .then(() => {
        this.props.loadMemberships();
      })
      .catch(() => {
      })
  };

  render() {
    const membership = this.props;
    console.log("Membership: ", membership);
    return (
      <div className="animated fadeIn">
        {this.getModalContent()}

        <Row>
          <Col xs={12}>
            {this.props.processing ? <Loader/> :
              <Card>
                <CardHeader>
                  {this.getCardHeader()}
                </CardHeader>
                <CardBody>
                  {this.getTable()}
                </CardBody>
              </Card>
            }
          </Col>
        </Row>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    memberships: state.membership.memberships,
    processing: state.membership.processing
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadMemberships: () => dispatch(loadMemberships()),
    setMembership: (membership) => dispatch(setMembership(membership)),
    deleteMembership: (id, index) => dispatch(deleteMembership(id, index)),
    resetRedux: () => dispatch(resetRedux()),
    setProcessing: (processing) => dispatch(setProcessing(processing))
  }
};

const connectedComponent = connect(mapStateToProps, mapDispatchToProps)(Membership);
export default withRouter(connectedComponent)
