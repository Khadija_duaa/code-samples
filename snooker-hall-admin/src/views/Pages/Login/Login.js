import React, {Component} from 'react';
import {
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row
} from 'reactstrap';
import {connect} from 'react-redux';
import {login, logout} from '../../../stores/auth/auth-actions';
import {withRouter} from 'react-router-dom';
import Loader from "../../../utils/loader";

class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      popoverOpen: false,
      username: '',
      password: '',
    };
  }

  toggle() {
    this.setState({ //the error happens here
      popoverOpen: true
    });
  }

  componentDidMount() {
    if (this.props.userAuthenticated) {
      this.props.logout();
    }
  }

  componentDidUpdate() {
    if (this.props.userAuthenticated) {
      this.props.history.push('/');
    }
  }


  onInputChange = (e) => {
    let state = {...this.state};
    state[e.target.name] = e.target.value;
    console.log("I am state");
    console.log(state);
    this.setState(state);

  };

  onFormSubmit = (e) => {
    e.preventDefault();
    console.log('loginForm', this.state);

    this.props.login(this.state.username, this.state.password);

  };

  usernameField = () => {
    return (
      <InputGroup className="mb-3">

        <InputGroupAddon addonType="prepend">
          <InputGroupText>
            <i className="icon-user"/>
          </InputGroupText>
        </InputGroupAddon>

        <Input type="text" placeholder="Username" name='username' autoComplete="username"
               onChange={this.onInputChange}/>

      </InputGroup>
    );
  };

  passwordField = () => {
    return (
      <InputGroup className="mb-4">
        <InputGroupAddon addonType="prepend">
          <InputGroupText>
            <i className="icon-lock"/>
          </InputGroupText>
        </InputGroupAddon>

        <Input type="passwor  d" placeholder="Password" autoComplete="current-password"
               onChange={this.onInputChange} name='password'/>

      </InputGroup>
    );
  };


  form = () => {
    if (this.props.processing) {
      return (
        <div style={{textAlign : 'center'}}>
          <Loader/>

        </div>
      );
    }
    else {
      return (
        <Form>
          <h1>Login</h1>

          <p className="text-muted">Snooker247 Admin Panel</p>

          {this.props.errorMsg ? <p style={{color: 'red'}}>{this.props.errorMsg}</p> : null}

          {this.usernameField()}
          {this.passwordField()}

          <Row>
            <Col xs="6">
              <Button id="Popover1" type='submit' disabled={!this.state.username || !this.state.password}
                      color="primary"
                      onClick={this.onFormSubmit} className="px-4">Login</Button>
            </Col>
          </Row>

        </Form>
      );
    }
  };

  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col xs="4">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    {this.form()}
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userAuthenticated: state.auth.authenticated,
    processing: state.auth.processing,
    errorMsg: state.auth.error
  }
};

const mapDispatchToProps = (dispatch) => {
  console.log('mapDispatchToProps');
  return {
    login: (username, password) => dispatch(login(username, password)),
    logout: () => dispatch(logout())
  }
};

const connectedComponent = connect(mapStateToProps, mapDispatchToProps)(Login);
export default withRouter(connectedComponent);

