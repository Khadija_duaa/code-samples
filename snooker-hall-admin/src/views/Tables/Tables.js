import React, {Component} from 'react';
import {newPerson} from './Utils'
import {
  Badge,
  Card,
  CardBody,
  CardHeader,
  Col,
  ListGroup,
  ListGroupItem,
  Row,
} from 'reactstrap';

class Tables extends Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: 1
    };
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  dragStart(e) {
    this.dragged = e.currentTarget;
    e.dataTransfer.effectAllowed = 'move';
    e.dataTransfer.setData('text/html', this.dragged);
  }

  dragEnd(e) {
    this.dragged.style.display = 'block';
    // this.dragged.parentnode.removeChild(placeholder);

    // update state
    let data = newPerson.names1;
    let from = Number(this.dragged.dataset.id);
    let to = Number(this.over.dataset.id);
    if (from < to) to--;
    data.splice(to, 0, data.splice(from, 1)[0]);
    this.setState({newPerson: data});
  }

  dragOver(e) {
    e.preventDefault();
    this.dragged.style.display = "none";
    if (e.target.className === 'placeholder') return;
    this.over = e.target;
  }

  render() {

    return (
      <div className="animated fadeIn">
        <Row>
          <Col sm="12" xl="6">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i><strong>Table 1</strong>
              </CardHeader>
              <CardBody>
                <ListGroup>
                  {
                    newPerson.names1 && newPerson.names1.map((data, i) => {
                      return (
                        <ListGroupItem data-id={i} key={i} draggable='true' onDragEnd={this.dragEnd.bind(this)}
                                       onDragStart={this.dragStart.bind(this)}
                                        onDragOver={this.dragOver.bind(this)}>{data.name} {data.time} <a
                          href="https://www.w3schools.com/html/"><Badge
                          className="float-right" pill>X</Badge></a></ListGroupItem>
                      )
                    })
                  }
                </ListGroup>
              </CardBody>
            </Card>
          </Col>

          <Col sm="12" xl="6">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i><strong>Table 2</strong>
              </CardHeader>
              <CardBody>
                <ListGroup>
                  {
                    newPerson.names1 && newPerson.names1.map((data, i) => {
                      return (
                        <ListGroupItem className="justify-content-between"data-id={i} key={i} draggable='true' onDragEnd={this.dragEnd.bind(this)}
                                       onDragStart={this.dragStart.bind(this)}
                                       onDragOver={this.dragOver.bind(this)}>{data.name} {data.time}<a
                          href="https://www.w3schools.com/html/"><Badge
                          className="float-right" pill>X</Badge></a></ListGroupItem>
                      )
                    })
                  }

                </ListGroup>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Row>
          <Col sm="12" xl="6">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i><strong>Table 3</strong>
              </CardHeader>
              <CardBody>
                <ListGroup>
                  {
                    newPerson.names1  && newPerson.names1.map((data, i) => {
                      return (
                        <ListGroupItem key={i}>{data.name} {data.time} <a
                          href="https://www.w3schools.com/html/"><Badge
                          className="float-right" pill>X</Badge></a></ListGroupItem>
                      )
                    })
                  }
                </ListGroup>
              </CardBody>
            </Card>
          </Col>

          <Col sm="12" xl="6">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i><strong>Table 4</strong>
              </CardHeader>
              <CardBody>
                <ListGroup>
                  {
                    newPerson.names1  && newPerson.names1.map((data, i) => {
                      return (
                        <ListGroupItem key={i} className="justify-content-between">{data.name} {data.time}<a
                          href="https://www.w3schools.com/html/"><Badge
                          className="float-right" pill>X</Badge></a></ListGroupItem>
                      )
                    })
                  }

                </ListGroup>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Tables;
