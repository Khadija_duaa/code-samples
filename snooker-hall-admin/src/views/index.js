import {
  Breadcrumbs,
  Cards,
  Carousels,
  Collapses,
  Dropdowns,
  UserProfileForm,
  Jumbotrons,
  ListGroups,
  Navbars,
  Navs,
  Paginations,
  Popovers,
  ProgressBar,
  Switches,
  Tabs,
  Tooltips,
  Topup
} from './Base';

import { ButtonDropdowns, ButtonGroups, Buttons, BrandButtons } from './Buttons';
import Charts from './Charts';
import UserDetail from './UserDetail/UserDetail';
import Dashboard from './Dashboard';
import Users from './Users/Users';
import Tables from './Tables/Tables';
import { CoreUIIcons, Flags, FontAwesome, SimpleLineIcons } from './Icons';
import { Alerts, Badges, Modals } from './Notifications';
import { Login, Page404, Page500, Register } from './Pages';
import { Colors, Typography } from './Theme';
import Widgets from './Widgets';

export {
  Badges,
  Users,
  UserDetail,
  Tables,
  Typography,
  Colors,
  CoreUIIcons,
  Page404,
  Page500,
  Register,
  Login,
  Modals,
  Alerts,
  Flags,
  SimpleLineIcons,
  FontAwesome,
  ButtonDropdowns,
  ButtonGroups,
  BrandButtons,
  Buttons,
  Tooltips,
  Tabs,
  Topup,
  Charts,
  Dashboard,
  Widgets,
  Jumbotrons,
  Switches,
  ProgressBar,
  Popovers,
  Navs,
  Navbars,
  UserProfileForm,
  Dropdowns,
  Collapses,
  Carousels,
  Cards,
  Breadcrumbs,
  Paginations,
};

