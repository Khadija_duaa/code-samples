import React, {Component} from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row,
  Table,
  Button,
  Container,
  Input,
  InputGroup,
  FormGroup,
  InputGroupAddon,
  Form,
} from 'reactstrap';

import {connect} from 'react-redux';
import TableHeaders from '../Components/TableHeads';
import {loadUsers, setUser, resetRedux, searchUsers} from '../../stores/users/users-actions';
// import {UserDetail} from '../UserDetail/UserDetail'

import {withRouter, Link} from 'react-router-dom';
import {setPagination} from "../../stores/users/users-actions";
import Loader from "../../utils/loader";

const searchFields = ['firstName', 'lastName', 'email', 'phoneNumber', 'ssn', 'membership.name'];
const header = ["First Name", "Last Name", "Phone Number", "Wallet", "FreeMinutes", "Membership Name", "Actions"];

class Users extends Component {

  componentWillMount() {
    let membershipUsers = this.props['membershipUsers'];
    if (membershipUsers && !this.props.membership) {
      this.props.history.push('/views/memberships');
    }
  }

  componentDidMount() {
    this.props.resetRedux();
    this.props.loadUsers(this.props['membershipUsers']);
  }

  getPagination = () => {
    let jsx = [];

    for (let index = 0; index < this.props.totalPages; index++) {
      jsx.push(
        <PaginationItem key={index}>
          <PaginationLink onClick={() => this.props.setPagination(index + 1)}>
            {index + 1}
          </PaginationLink>
        </PaginationItem>
      );
    }


    return <Pagination>
      {jsx}
    </Pagination>;
  };

  navigate = (user, path) => {
    this.props.setUser(user);
    this.props.history.push(path);
  };

  searchField = () => {
    return (
      <Form className="form-horizontal">
        <FormGroup>
          <InputGroup>

            <InputGroupAddon addonType="prepend">
              <Button type="button" disabled color="primary"><i className="fa fa-search"/> Search</Button>
            </InputGroupAddon>

            <Input type="text" id="input1-group2" name="input1-group2" placeholder="Search"
                   onChange={(e) => this.props.searchUsers(e.target.value, searchFields)}/>

          </InputGroup>
        </FormGroup>
      </Form>
    );
  };


  getTableHeaders = () => {
    return (
      <TableHeaders>
        {header}
      </TableHeaders>
    );
  };

  getUserActions = (user) => {
    return (
      <Container>
        <Row>
          <Col xs={12} xl={'auto'} style={{marginBottom: '12px'}}>
            <Button type="submit" size="md" color="primary"
                    onClick={() => this.navigate(user, '/views/users/profile')}>
              Edit
            </Button>
          </Col>

          <Col xs={12} xl={'auto'}>
            <Button type="submit" size="md" color="primary"
                    onClick={() => this.navigate(user, '/views/users/top-up')}>
              Top-up Balance
            </Button>
          </Col>
        </Row>
      </Container>
    );
  };

  getUserData = (user) => {
    return (
      <tr key={user._id}>
        <td>
          <Link to='/views/users/user-detail'
                onClick={() => this.props.setUser(user)}>
            {user.firstName}
          </Link>
        </td>

        <td>
          {user.lastName}
        </td>

        <td>
          {user.phoneNumber}
        </td>

        <td>
          {user.profile.wallet}
        </td>

        <td>
          {user.profile.freeMinutes}
        </td>

        <td>
          {user.membership.name}
        </td>

        <td>
          {this.getUserActions(user)}
        </td>
      </tr>
    );
  };

  getUsersData = () => {

    let jsx = [];
    let {activeUsers} = this.props;
    if (activeUsers && activeUsers.length) {
      jsx = activeUsers.map(user => this.getUserData(user));
    }

    return jsx;
  };

  getTable = () => {
    return (
      <Table bordered hover striped responsive>
        <thead>
        {this.getTableHeaders()}
        </thead>

        <tbody>
        {this.getUsersData()}
        </tbody>

      </Table>
    );
  };

  getCardBody = () => {
    if (!this.props.activeUsers) return null;
    return (
      <div>
        <Row>
          <Col sm={12}>
            {this.searchField()}
          </Col>
        </Row>

        <Row style={{marginTop: '10px'}}>
          <Col sm={12}>
            {this.getTable()}
          </Col>
        </Row>

        {this.getPagination()}
      </div>
    );
  };

  render() {
    return (
      <div className="animated fadeIn">
        {this.props.processing ? <Loader/> :
          <Row>
            <Col xs="12">
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"/> Users
                </CardHeader>

                <CardBody>
                  {this.getCardBody()}
                </CardBody>
              </Card>
            </Col>
          </Row>
        }
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    processing: state.users.processing,
    activeUsers: state.users.activeUsers,
    totalPages: state.users.totalPages,
    membership: state.membership.membership
  }
};
const mapDispatchToProps = (dispatch) => {
  return {
    loadUsers: (membershipUsers) => dispatch(loadUsers(membershipUsers)),
    setPagination: (paging) => dispatch(setPagination(paging)),
    setUser: (user) => dispatch(setUser(user)),
    resetRedux: () => dispatch(resetRedux()),
    searchUsers: (searchStr, searchParams) => dispatch(searchUsers(searchStr, searchParams))
  }
};

const connectedComponent = connect(mapStateToProps, mapDispatchToProps)(Users);
export default withRouter(connectedComponent);

