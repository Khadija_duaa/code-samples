import React from 'react';
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Container,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
  Table
} from 'reactstrap';

import TableHeaders from '../Components/TableHeads';
import {loadCategories, setItem, deleteItem, resetRedux} from "../../stores/item/item-actions";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import Loader from "../../utils/loader";
import {refreshItemCategory} from "../../stores/item/item-actions";

const header = ["Name", "Category", "Price", "Actions"];

class Items extends React.Component {

  state = {
    modalOpen: false,
    itemDeleteInfo: null
  };

  componentWillMount() {
    if (!this.props.category) {
      this.props.history.push('/views/item-categories');
    }
  }

  componentDidMount() {
    if (this.props.category) {
      this.props.setItem(null);
      this.props.refreshItemCategory();
    }
  }

  openDeleteModal = (item) => this.setState({modalOpen: !this.state.modalOpen, itemDeleteInfo: item});

  getModalContent = () => {
    return (
      <Modal isOpen={this.state.modalOpen} toggle={this.openDeleteModal}
             className={'modal-danger ' + this.props.className}>

        <ModalHeader toggle={this.openDeleteModal}>
          Are you sure ?
        </ModalHeader>

        <ModalBody>
          Are you sure you want to delete this item ?
        </ModalBody>

        <ModalFooter>
          <Button color="danger" onClick={() => {
            let {itemDeleteInfo} = this.state;
            this.props.deleteItem(itemDeleteInfo._id, itemDeleteInfo.index);
            this.openDeleteModal(null);
          }}>
            Delete
          </Button>{' '}

          <Button color="secondary" onClick={this.openDeleteModal}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    );
  };

  getCardHeader = () => {
    return (
      <Row>
        <Col xs={10} style={{textAlign: 'left'}}>
          <i className="fa fa-align-justify"/> Items
        </Col>

        <Col xs={2} style={{textAlign: 'right'}}>
          <Button color="primary" size="md" onClick={() => {
            this.props.history.push('/views/item-categories/items/item');
          }}>
            Add Item
          </Button>
        </Col>

      </Row>
    );
  };

  getItemsActions = (item, index) => {
    return (
      <Container>
        <Row>
          <Col xs={12} xl={'auto'} style={{marginBottom: '12px'}}>
            <Button color="primary" size="md"
                    onClick={() => {
                      this.props.setItem(item);
                      this.props.history.push('/views/item-categories/items/item');
                    }}>
              Edit
            </Button>
          </Col>


          <Col xs={12} xl={'auto'} style={{marginBottom: '12px'}}>
            <Button color="danger" size="md"
                    onClick={() => {
                      this.openDeleteModal({_id: item._id, index});
                    }}>
              Delete
            </Button>
          </Col>
        </Row>
      </Container>
    );
  };

  getItemData = (item, index) => {
    return (
      <tr key={item._id}>
        <td>
          {item.name}
        </td>

        <td>
          {this.props.category.categoryName}
        </td>

        <td>
          {item.price}
        </td>

        <td>
          {this.getItemsActions(item, index)}
        </td>
      </tr>
    );
  };


  getItemRows = () => {
    if (!this.props.category) return null;

    let jsx = null;

    let {items} = this.props.category;

    if (items) {
      jsx = items.map(this.getItemData);
    }

    return jsx;
  };


  getTable = () => {
    return (
      <Table bordered hover striped responsive>
        <thead>
        <TableHeaders>
          {header}
        </TableHeaders>
        </thead>

        <tbody>
        {this.getItemRows()}
        </tbody>
      </Table>
    );
  };

  render() {
    return (
      <div className="animated fadeIn">
        {this.getModalContent()}

        <Row>
          <Col xs={12}>
            {this.props.processing ? <Loader/> :
              <Card>
                <CardHeader>
                  {this.getCardHeader()}
                </CardHeader>
                <CardBody>
                  {this.getTable()}
                </CardBody>
              </Card>
            }
          </Col>
        </Row>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    processing: state.items.processing,
    category: state.items.category,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadCategories: () => dispatch(loadCategories()),
    setItem: (item) => dispatch(setItem(item)),
    deleteItem: (id, index) => dispatch(deleteItem(id, index)),
    resetRedux: () => dispatch(resetRedux()),
    refreshItemCategory: () => dispatch(refreshItemCategory())
  }
};

const connectedComponent = connect(mapStateToProps, mapDispatchToProps)(Items);
export default withRouter(connectedComponent)
