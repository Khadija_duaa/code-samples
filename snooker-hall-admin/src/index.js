import './polyfill'
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {Provider} from 'react-redux';
import App from './App';
import thunk from 'redux-thunk';
import {createStore, applyMiddleware, combineReducers} from 'redux';
import auth from './stores/auth/auth-reducer';
import users from './stores/users/users-reducer';
import items from './stores/item/item-reducer'
import membership from './stores/membership/membership-reducer';
import gallery from './stores/gallery/gallery-reducer'
import { reducer as formReducer } from 'redux-form'

const rootReducer = combineReducers({
  auth,
  users,
  membership,
  gallery,
  items,
  form: formReducer
});

export const store = createStore(rootReducer, applyMiddleware(thunk));

ReactDOM.render(
  <Provider store={store}>
    <App/>
  </Provider>, document.getElementById('root'));
