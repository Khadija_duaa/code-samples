import React from 'react';
import {
  Row,
  Col,
  Button,
  Modal, ModalBody, ModalFooter, ModalHeader
} from 'reactstrap';
import {Link, withRouter} from 'react-router-dom';
import {connect} from "react-redux";
import {deleteAlbum} from "../stores/gallery/gallery-actions";


class EditDeleteAlbum extends React.Component {

  state = {
    modalOpen: false
  };

  openDeleteModal = () => {
    this.setState({modalOpen: !this.state.modalOpen});
  };

  getModalContent = () => {
    return (
      <Modal isOpen={this.state.modalOpen} toggle={this.openDeleteModal}
             className={'modal-danger ' + this.props.className}>
        <ModalHeader toggle={this.openDeleteModal}>Are you sure ?</ModalHeader>

        <ModalBody>
          Are you sure you want to delete this Album ?
        </ModalBody>

        <ModalFooter>
          <Button color="danger" onClick={() => {
            this.props.deleteAlbum(this.props.album);
            this.openDeleteModal(null);
          }}>
            Delete
          </Button>{' '}
          <Button color="secondary" onClick={this.openDeleteModal}>Cancel</Button>
        </ModalFooter>
      </Modal>
    );
  };

  render() {
    return (
      <div>
        {this.getModalContent()}

        <Row>
          <Col xs={2}>
            <Link to='/views/gallery/addAlbum'>
              <Button>
                Edit
              </Button>
            </Link>
          </Col>

          <Col xs={6} style={{textAlign: 'left'}}>
            <Button color="danger" onClick={() => this.openDeleteModal()}>
              Delete Album
            </Button>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    album: state.gallery.album,
    processing: state.membership.processing
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    deleteAlbum: (album) => dispatch(deleteAlbum(album)),
  }
};

const connectedComponent = connect(mapStateToProps, mapDispatchToProps)(EditDeleteAlbum);
export default withRouter(connectedComponent)
