import React from 'react';
import axios from '../utils/axios';
import {connect} from 'react-redux';
import {specificAlbum, updateAlbum} from '../stores/gallery/gallery-actions';
import {NotificationManager} from "react-notifications";
import Loader from "../utils/loader";
import {Button} from 'reactstrap';

const validatePhotos = (photos) => {
  const SUPPORTED_FILE_TYPES = ['jpg', 'jpeg', 'png'];
  const MAX_FILE_SIZE_MB = 20;
  const MAX_FILE_SIZE = MAX_FILE_SIZE_MB * 1024 * 1024; // 10 MB

  let validPhotos = [];
  if (photos && photos.length) {
    for (let index = 0; index < photos.length; index++) {
      const photo = photos[index];

      let fileType = photo.name.split('.').pop().toLowerCase();

      if (SUPPORTED_FILE_TYPES.indexOf(fileType) < 0) {
        NotificationManager.error(`Invalid File ${photo.name}`, '', 3000);
      }
      else if (photo.size > MAX_FILE_SIZE) {
        NotificationManager.error(`File ${photo.name} is too large, Max file size is ${MAX_FILE_SIZE_MB} MB`, '', 3000);
      }
      else {
        validPhotos.push(photo);
      }
    }
  }
  return validPhotos;
};

class PhotoUploader extends React.Component {

  state = {
    processing: false
  };


  onDrop = (photos) => {
    let validPhotos = validatePhotos(photos);
    this.uploadPhotos(0, validPhotos, () => {
      this.setState({processing: false});
    });
  };

  uploadPhotos = (index, photos, callback) => {
    if (index < photos.length) {
      let photo = photos[index];

      this.setState({processing: true}, () => {
        let formData = new FormData();
        formData.append('file', photo, photo.name);

        axios.post(`/gallery/${this.props.album._id}`, formData)
          .then(response => {
            let album = response.data.gallery;

            this.props.updateInAlbums(album);
            this.props.setAlbum(album);

            NotificationManager.success(response.data['_message'], '', 3000);

            // if last photo, callback
            if (index + 1 === photos.length) {
              callback && callback();
            }
            else {
              this.uploadPhotos(index + 1, photos, callback);
            }
          })
          .catch(err => {
            this.setState({processing: false});
            console.error(err);
            NotificationManager.error(err.response.data['_message'], '', 3000);
          });
      });
    }
  };

  render() {
    if (!this.props.album) {
      return null;
    }

    if (this.state.processing) {
      return (
        <Loader
          type="Triangle"
          color="Black"
          height="5vh"
          width="5vw"/>
      );
    }
    else {
      return (
        <div>
          <input type={'file'}
                 multiple
                 ref={uploadElement => {
                   this.uploadElement = uploadElement
                 }}
                 accept={'.jpg,.jpeg,.png'}
                 hidden
                 onChange={(e) => {
                   this.onDrop(e.target.files)
                 }}/>
          <Button type="submit" size="md" color="primary" onClick={() => this.uploadElement.click()}>
            Upload Photos
          </Button>
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    jwt: state.auth.token,
    album: state.gallery.album,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setAlbum: (album) => dispatch(specificAlbum(album)),
    updateInAlbums: (album) => dispatch(updateAlbum(album))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PhotoUploader);
